Contributing and development guidelines
=======================================

.. toctree::
   :maxdepth: 1

   CONTRIBUTING
   distrib/README
   distrib/README-install
   docs/README
   docs/README-otherdoc
   AUTHORS
   CODE_OF_CONDUCT
