Release notes
=============

Starting with Version 3.17, this file keeps track of what has been done from one version to the next one. 

Minor version 3.17
* Asyncrhonous file dumping of ad stack with prefetch for reads (compile adstack with -D_ADSTACKPREFETCH)
* Profiling tool for suggesting checkpointing strategies (Added through the -profile option in tapenade command)
* Bugfix: specific input language when more than one file as input (-inputlanguage LANGUAGE filename)

