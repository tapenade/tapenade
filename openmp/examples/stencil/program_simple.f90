MODULE MM
   type testtype
      integer, dimension(:), pointer :: idx
   end type


CONTAINS

  ! expected: xb is safe
  SUBROUTINE func_a(x,y,n)
    integer :: n, i
    real *8, dimension(:) :: x,y

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(i) = x(i)
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_a_un(x,y,n)
    integer :: n, i
    real *8, dimension(:) :: x,y

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(i) = x(i) + x(i+1)
    END DO
  END SUBROUTINE
  
  ! expected: xb is safe
  SUBROUTINE func_b(x,y,c,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    integer, dimension(:) :: c

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i)) = x(c(i))
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_b_un(x,y,c,d,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    integer, dimension(:) :: c,d

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i)) = x(d(i))
    END DO
  END SUBROUTINE

  ! expected: xb is safe
  SUBROUTINE func_c(x,y,c,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    integer, dimension(:) :: c

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i)) = x(c(i)+10)
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_c_un(x,y,c,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    integer, dimension(:) :: c

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i)) = x(c(i+10))
    END DO
  END SUBROUTINE

  ! expected: xb is safe
  SUBROUTINE func_d(x,y,tt,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    type(testtype) :: tt

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(tt%idx(i)) = x(tt%idx(i))
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_d_un(x,y,tt,n)
    integer :: n, i
    real *8, dimension(:) :: x,y
    type(testtype) :: tt

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(tt%idx(i)) = x(tt%idx(i+10))
    END DO
  END SUBROUTINE

  ! expected: xb is safe
  SUBROUTINE func_e(x,y,n)
    integer :: n, i
    real *8, dimension(:,:) :: x,y

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(i,1) = x(3,i) + x(i,3)
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_e_un(x,y,n)
    integer :: n, i
    real *8, dimension(:,:) :: x,y

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(i,1) = x(3,i) + x(i,2)
    END DO
  END SUBROUTINE

  ! expected: xb is safe
  SUBROUTINE func_f(x,y,c,n)
    integer :: n, i
    real *8, dimension(:,:) :: x,y
    integer, dimension(:,:) :: c

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i,1)) = x(c(i,1))
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_f_un(x,y,c,n)
    integer :: n, i
    real *8, dimension(:,:) :: x,y
    integer, dimension(:,:) :: c

    !$OMP parallel do schedule(static)
    DO i=1,n
      y(c(i,1)) = x(c(1,i))
    END DO
  END SUBROUTINE

  ! expected: xb is safe
  SUBROUTINE func_g(x,y,n,cond)
    integer :: n, i
    real *8, dimension(:) :: x,y
    real :: temp
    logical :: cond

    !$OMP parallel do private(temp) schedule(static)
    DO i=1,n
      temp = x(i)
      if(cond) then
        y(i) = temp
      end if
    END DO
  END SUBROUTINE

  ! expected: xb is unsafe
  SUBROUTINE func_g_un(x,y,c,n,cond)
    integer :: n, i
    real *8, dimension(:) :: x,y
    integer, dimension(:) :: c
    real :: temp
    logical :: cond

    !$OMP parallel do private(temp) schedule(static)
    DO i=1,n
      temp = x(c(i))
      if(cond) then
        y(c(i)) = temp
      end if
    END DO
  END SUBROUTINE
END MODULE MM

