#define index3d(ny,nz,i,j,k) (1 + ((i)*(ny) + (j))*(nz) + (k))

MODULE MM

CONTAINS

  SUBROUTINE cpu_stencil(c0,c1,A0,Anext,nx,ny,nz,niter)
    REAL*8 :: c0,c1
    REAL *8, DIMENSION(:) :: A0, Anext
    INTEGER :: nx,ny,nz,niter
    INTEGER i,j,k,iter

    DO iter=1,niter 
      !$OMP parallel do private(i,j,k) schedule(static)
      DO i=1,nx-2
         DO j=1,ny-2
            DO k=1,nz-2
               Anext(index3d(ny,nz,i,j,k)) =     &
  &             (A0(index3d(ny,nz,i,j,k+1))      &
  &             + A0(index3d(ny,nz,i,j,k-1))     &
  &             + A0(index3d(ny,nz,i,j+1,k))     &
  &             + A0(index3d(ny,nz,i,j-1,k))     &
  &             + A0(index3d(ny,nz,i+1,j,k))     &
  &             + A0(index3d(ny,nz,i-1,j,k)))*c1 &
  &             -A0(index3d(ny,nz,i,j,k))*c0
            END DO
         END DO
      END DO
    END DO
  END SUBROUTINE cpu_stencil
  
END MODULE MM

PROGRAM MAIN
  USE MM
  INTEGER, PARAMETER :: nx=32, ny=32, nz=32, niter = 100000
  REAL *8, DIMENSION(nx*ny*nz) :: A0, A1
  REAL*8 :: c0, c1
  INTEGER i,j,k
  INTEGER, external :: omp_get_max_threads
  REAL*8 :: t0
  REAL*8, external :: omp_get_wtime

  print *, 'Total OMP threads: ', omp_get_max_threads()

  DO i = 1,SIZE(A0)
     A0(i) = 5.0*SIN(i*0.2)
     A1(i) = 3.0*COS(i*0.5)
  ENDDO
  c0 = 1.5
  c1 = 0.6
  t0 = omp_get_wtime()
  CALL CPU_STENCIL(c0,c1,A0,A1,nx,ny,nz,niter)
  t0 = omp_get_wtime() - t0
  print *,'pri_time:',t0,'res:',SUM(A1)
END PROGRAM MAIN
