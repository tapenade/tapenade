        INTEGER ISIZE1OFDrfcr
        PARAMETER(ISIZE1OFDrfcr=2)
        INTEGER ISIZE2OFDrfcr
        PARAMETER(ISIZE2OFDrfcr=2048)
        INTEGER ISIZE3OFDrfcr
        PARAMETER(ISIZE3OFDrfcr=132)
        INTEGER ISIZE1OFDrfcl
        PARAMETER(ISIZE1OFDrfcl=2)
        INTEGER ISIZE2OFDrfcl
        PARAMETER(ISIZE2OFDrfcl=2048)
        INTEGER ISIZE3OFDrfcl
        PARAMETER(ISIZE3OFDrfcl=132)
        INTEGER ISIZE1OFDrfmss_sign
        PARAMETER(ISIZE1OFDrfmss_sign=512)
        INTEGER ISIZE2OFDrfmss_sign
        PARAMETER(ISIZE2OFDrfmss_sign=66)
        INTEGER ISIZE1OFDrfmsss_sign
        PARAMETER(ISIZE1OFDrfmsss_sign=257)
        INTEGER ISIZE2OFDrfmsss_sign
        PARAMETER(ISIZE2OFDrfmsss_sign=222)
        INTEGER ISIZE1OFDrfnsexch_sign
        PARAMETER(ISIZE1OFDrfnsexch_sign=2048)
        INTEGER ISIZE2OFDrfnsexch_sign
        PARAMETER(ISIZE2OFDrfnsexch_sign=66)
