#!/bin/sh
export OMP_STACKSIZE=128M
ulimit -s unlimited
export OMP_PLACES='sockets(1)'
export LD_LIBRARY_PATH=/home/hueckelh/simd_adjoint_stencils/
export OMP_NUM_THREADS=1
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=2
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=4
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=8
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=16
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=28
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
export OMP_NUM_THREADS=56
./stassuij
DBAD_PHASE=2 ./stassuij-tgt
./stassuij-adj-atomics
./stassuij-adj-reduction
