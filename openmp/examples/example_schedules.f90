! Example program that uses the push/pop stack from inside a parallel loop
! in a safe manner. Three variants are implemented. All of them initialize
! an array with natural numbers (1,2,3,4,...) and push this to the stack,
! before killing and finally restoring (popping) it.
! The three variants:
! 1) Push not only the value, but also the iteration counter. Each reverse
!    iteration pops the original counter, to ensure that the derivative of
!    the correct forward iteration is being worked on, regardless of OpenMP
!    schedule. This is simple to implement, but requires a push/pop in each
!    iteration, and O(n) storage.
! 2) Static schedule, which replaces OpenMP's schedule, and can be replayed
!    in reverse order during the backward pass. No overhead, but also no
!    choice in schedule.
! 3) Dynamic recording, which uses a method to record the start and end of
!    chunks. Needs 2*num_chunks storage, so as long as the chunk size is
!    at least 3, this method saves space.

subroutine foo_counter(n)
! Variant 1, counter pushing
implicit none
integer i, n, i_b
double precision b(n)
!$omp parallel do
do i=1,n
  b(i) = i
  call pushreal8(b(i))
  call pushinteger4(i)
end do
b = 0
!$omp parallel do private(i)
do i_b=1,n
  call popinteger4(i)
  call popreal8(b(i))
end do
print *, b
end subroutine

subroutine foo_static(n)
! Variant 2, static schedule
implicit none
integer i, n, threadstart, threadend
double precision b(n)
!$omp parallel private(i,threadstart,threadend)
call getstaticschedule(1,n,1,threadstart,threadend)
do i=threadstart,threadend
  b(i) = i
  call pushreal8(b(i))
end do
!$omp end parallel
b = 0
!$omp parallel private(i,threadstart,threadend)
call getstaticschedule(1,n,1,threadstart,threadend)
do i=threadend,threadstart,-1
  call popreal8(b(i))
end do
!$omp end parallel
print *, b
end subroutine

subroutine foo_dynamic(n)
! Variant 3, dynamic schedule recording
implicit none
integer i, n, ichunk, threadstart, threadend, num_chunks
double precision b(n)
!$omp parallel
call initdynamicschedule
!$omp do schedule(dynamic,2)
do i=1,n
  b(i) = i
  call recorddynamicschedule(i,1)
  call pushreal8(b(i))
end do
call finalizedynamicschedule
!$omp end parallel
b = 0
!$omp parallel private(i,ichunk,threadstart,threadend,num_chunks)
call popinteger4(num_chunks)
do ichunk=1,num_chunks
  call popinteger4(threadend)
  call popinteger4(threadstart)
  do i=threadend,threadstart,-1
    call popreal8(b(i))
  end do
end do
!$omp end parallel
print *, b
end subroutine

program main
  integer, parameter :: n = 10
  call foo_counter(n)
  call foo_static(n)
  call foo_dynamic(n)
end program

