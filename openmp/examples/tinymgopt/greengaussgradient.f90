subroutine gggrad(color_ia, edge2nodes, dv, sij, numEdges, numNodes, numColors, gradient)
  integer :: numNodes, numEdges, i, j, ie, ic
  integer, dimension(numColors) :: color_ia
  real, dimension(numNodes) :: dv
  integer, dimension(2,numEdges) :: edge2nodes
  real, dimension(numNodes) :: gradient
  real, dimension(numEdges) :: sij
  real :: dvFace
  do ic=1,size(color_ia,1)-1
    !$OMP PARALLEL DO SHARED(color_ia, dv, gradient, sij, edge2nodes) PRIVATE(ie, i, j, dvFace)
    do ie=color_ia(ic),color_ia(ic+1)-1
      i = edge2nodes(1,ie)
      j = edge2nodes(2,ie)
      if (i .ne. j) then
        dvFace=0.5d0*(dv(i)+dv(j))
        gradient(i) = gradient(i) + dvFace*sij(ie)
        gradient(j) = gradient(j) - dvFace*sij(ie)
      end if
    end do
  end do
end subroutine

subroutine createAndRun(numNodes, numIters, numEdges, numColors)
  integer :: numNodes, numEdges, numColors
  integer, dimension(numColors+1) :: color_ia
  real, dimension(numNodes) :: dv
  integer, dimension(2,numEdges) :: edge2nodes
  real, dimension(numNodes) :: gradient
  real, dimension(numEdges) :: sij
  integer :: ie, ic, iter
  ! build structure for 1D mesh with colored edges
  do ie = 1, numEdges/2 + 1
    edge2nodes(1, ie) = ie * 2 - 1
    edge2nodes(2, ie) = ie * 2
  end do
  do ie = 1, numEdges/2
    edge2nodes(1, ie + numEdges/2 + 1) = ie * 2
    edge2nodes(2, ie + numEdges/2 + 1) = ie * 2 + 1
  end do
  color_ia(1) = 1
  color_ia(2) = numEdges/2 + 2
  color_ia(3) = numEdges + 1

  ! set arbitrary values
  dv = 0.5
  sij = 0.5
  gradient = 0
  do iter = 1, numIters
    call gggrad(color_ia, edge2nodes, dv, sij, numEdges, numNodes, numColors, gradient)
  end do
  write(*,*) sum(gradient)
end subroutine

program main
  integer :: numNodes, numIters, numEdges, numColors
  character(len=255) :: numNodesChar, numItersChar
  call get_command_argument(1,numNodesChar)
  call get_command_argument(2,numItersChar)
  read(numNodesChar,'(i10)') numNodes
  read(numItersChar,'(i10)') numIters
  ! warning: numEdges must be even number
  numEdges = numNodes - 1
  numColors = 2
  call createAndRun(numNodes, numIters, numEdges, numColors)
end program
