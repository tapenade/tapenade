/* $Id: main.c,v 1.1 2008/03/04 17:30:03 stratton Exp $ */

/*############################################################################*/

#include "main.h"
#include "lbm_1d_array.h"
#include "lbm_diff.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <omp.h>

/*############################################################################*/

static LBM_GridPtr srcGrid, dstGrid, srcGridd, dstGridd, srcGridb, dstGridb;

/*############################################################################*/

struct pb_TimerSet timers;
int main( int nArgs, char* arg[] ) {
	int na;
	for(na = 0; na<nArgs; na++) printf("%s ",arg[na]);
	printf("\n");
	MAIN_Param param;
	int t;

        struct pb_Parameters* params;
        params = pb_ReadParameters(&nArgs, arg);

	MAIN_parseCommandLine( nArgs, arg, &param, params );
	MAIN_printInfo( &param );
	MAIN_initialize( &param );
	double time;

	for( t = 1; t <= param.nTimeSteps/4; t++ ) {
          lbm_sc_atomic_(*srcGrid, *dstGrid);
	}
	time = omp_get_wtime();
	for( t = 1; t <= param.nTimeSteps; t++ ) {
          lbm_sc_atomic_(*srcGrid, *dstGrid);
	}
	printf("time primal %f\n", omp_get_wtime()-time);

	for( t = 1; t <= param.nTimeSteps/4; t++ ) {
          lbm_sc_atomic_d_(*srcGrid, *srcGridd, *dstGrid, *dstGridd);
	}
	time = omp_get_wtime();
	for( t = 1; t <= param.nTimeSteps; t++ ) {
          lbm_sc_atomic_d_(*srcGrid, *srcGridd, *dstGrid, *dstGridd);
	}
	printf("time tangent %f\n", omp_get_wtime()-time);

	for( t = 1; t <= param.nTimeSteps/4; t++ ) {
          lbm_sc_atomic_b_(*srcGrid, *srcGridb, *dstGrid, *dstGridb);
	}
	time = omp_get_wtime();
	for( t = 1; t <= param.nTimeSteps; t++ ) {
          lbm_sc_atomic_b_(*srcGrid, *srcGridb, *dstGrid, *dstGridb);
	}
	printf("time atomic %f\n", omp_get_wtime()-time);

	for( t = 1; t <= param.nTimeSteps/4; t++ ) {
          lbm_sc_reduction_b_(*srcGrid, *srcGridb, *dstGrid, *dstGridb);
	}
	time = omp_get_wtime();
	for( t = 1; t <= param.nTimeSteps; t++ ) {
          lbm_sc_reduction_b_(*srcGrid, *srcGridb, *dstGrid, *dstGridb);
	}
	printf("time reduction %f\n", omp_get_wtime()-time);

        pb_FreeParameters(params);
	return 0;
}

/*############################################################################*/

void MAIN_parseCommandLine( int nArgs, char* arg[], MAIN_Param* param, struct pb_Parameters * params) {
	struct stat fileStat;
	
	if( nArgs < 2 ) {
		printf( "syntax: lbm <time steps>\n" );
		exit( 1 );
	}

	param->nTimeSteps     = atoi( arg[1] );

	if( params->inpFiles[0] != NULL ) {
		param->obstacleFilename = params->inpFiles[0];

		if( stat( param->obstacleFilename, &fileStat ) != 0 ) {
			printf( "MAIN_parseCommandLine: cannot stat obstacle file '%s'\n",
			         param->obstacleFilename );
			exit( 1 );
		}
		if( fileStat.st_size != SIZE_X*SIZE_Y*SIZE_Z+(SIZE_Y+1)*SIZE_Z ) {
			printf( "MAIN_parseCommandLine:\n"
			        "\tsize of file '%s' is %i bytes\n"
					    "\texpected size is %i bytes\n",
			        param->obstacleFilename, (int) fileStat.st_size,
			        SIZE_X*SIZE_Y*SIZE_Z+(SIZE_Y+1)*SIZE_Z );
			exit( 1 );
		}
	}
	else param->obstacleFilename = NULL;

	param->resultFilename = params->outFile;
	param->action         = STORE;
	param->simType        = LDC;
}

/*############################################################################*/

void MAIN_printInfo( const MAIN_Param* param ) {
	const char actionString[3][32] = {"nothing", "compare", "store"};
	const char simTypeString[3][32] = {"lid-driven cavity", "channel flow"};
	printf( "MAIN_printInfo:\n"
	        "\tgrid size      : %i x %i x %i = %.2f * 10^6 Cells\n"
	        "\tnTimeSteps     : %i\n"
	        "\tresult file    : %s\n"
	        "\taction         : %s\n"
	        "\tsimulation type: %s\n"
	        "\tobstacle file  : %s\n\n",
	        SIZE_X, SIZE_Y, SIZE_Z, 1e-6*SIZE_X*SIZE_Y*SIZE_Z,
	        param->nTimeSteps, param->resultFilename, 
	        actionString[param->action], simTypeString[param->simType],
	        (param->obstacleFilename == NULL) ? "<none>" :
	                                            param->obstacleFilename );
}

/*############################################################################*/

void MAIN_initialize( const MAIN_Param* param ) {
	LBM_allocateGrid( (float**) &srcGrid );
	LBM_allocateGrid( (float**) &dstGrid );

	LBM_initializeGrid( *srcGrid );
	LBM_initializeGrid( *dstGrid );

	LBM_allocateGrid( (float**) &srcGridd );
	LBM_allocateGrid( (float**) &dstGridd );

	LBM_initializeGrid( *srcGridd );
	LBM_initializeGrid( *dstGridd );

	LBM_allocateGrid( (float**) &srcGridb );
	LBM_allocateGrid( (float**) &dstGridb );

	LBM_initializeGrid( *srcGridb );
	LBM_initializeGrid( *dstGridb );

	if( param->obstacleFilename != NULL ) {
		LBM_loadObstacleFile( *srcGrid, param->obstacleFilename );
		LBM_loadObstacleFile( *dstGrid, param->obstacleFilename );
	}

	if( param->simType == CHANNEL ) {
		LBM_initializeSpecialCellsForChannel( *srcGrid );
		LBM_initializeSpecialCellsForChannel( *dstGrid );
	}
	else {
		LBM_initializeSpecialCellsForLDC( *srcGrid );
		LBM_initializeSpecialCellsForLDC( *dstGrid );
	}

	LBM_showGridStatistics( *srcGrid );
}

