/* $Id: lbm_1d_array.h,v 1.1 2008/03/04 17:30:03 stratton Exp $ */

#ifndef _LBM_MACROS_H_
#define _LBM_MACROS_H_

#define OMEGA (1.95)
#define SIZE   (120)
#define SIZE_X (1*SIZE)
#define SIZE_Y (1*SIZE)
#define SIZE_Z (150)
#define C 0
#define N 1
#define S 2
#define E 3
#define W 4
#define T 5
#define B 6
#define NE 7
#define NW 8
#define SE 9
#define SW 10
#define NT 11
#define NB 12
#define ST 13
#define SB 14
#define ET 15
#define EB 16
#define WT 17
#define WB 18
#define FLAGS 19
#define N_CELL_ENTRIES 20
#define OBSTACLE 1
#define ACCEL 2
#define IN_OUT_FLOW 4
#define DFL1 (1.0/ 3.0)
#define DFL2 (1.0/18.0)
#define DFL3 (1.0/36.0)

#define CALC_INDEX(x,y,z,e) (e)+N_CELL_ENTRIES*((x)+(y)*SIZE_X+(z)*SIZE_X*SIZE_Y)

#define SWEEP_VAR integer :: i
#define TEST_VAR  logical :: cond

#define SWEEP_START(x1,y1,z1,x2,y2,z2) \
  do i = CALC_INDEX(x1, y1, z1, 0), \
         CALC_INDEX(x2, y2, z2, 0)-1, \
         N_CELL_ENTRIES

#define SWEEP_END end do

#define GRID_ENTRY_SWEEP(g,dx,dy,dz,e) g(CALC_INDEX(dx, dy, dz, e)+(i))

#define LOCAL(g,e)       GRID_ENTRY_SWEEP( g,  0,  0,  0, e )
#define NEIGHBOR_C(g,e)  GRID_ENTRY_SWEEP( g,  0,  0,  0, e )
#define NEIGHBOR_N(g,e)  GRID_ENTRY_SWEEP( g,  0, +1,  0, e )
#define NEIGHBOR_S(g,e)  GRID_ENTRY_SWEEP( g,  0, -1,  0, e )
#define NEIGHBOR_E(g,e)  GRID_ENTRY_SWEEP( g, +1,  0,  0, e )
#define NEIGHBOR_W(g,e)  GRID_ENTRY_SWEEP( g, -1,  0,  0, e )
#define NEIGHBOR_T(g,e)  GRID_ENTRY_SWEEP( g,  0,  0, +1, e )
#define NEIGHBOR_B(g,e)  GRID_ENTRY_SWEEP( g,  0,  0, -1, e )
#define NEIGHBOR_NE(g,e) GRID_ENTRY_SWEEP( g, +1, +1,  0, e )
#define NEIGHBOR_NW(g,e) GRID_ENTRY_SWEEP( g, -1, +1,  0, e )
#define NEIGHBOR_SE(g,e) GRID_ENTRY_SWEEP( g, +1, -1,  0, e )
#define NEIGHBOR_SW(g,e) GRID_ENTRY_SWEEP( g, -1, -1,  0, e )
#define NEIGHBOR_NT(g,e) GRID_ENTRY_SWEEP( g,  0, +1, +1, e )
#define NEIGHBOR_NB(g,e) GRID_ENTRY_SWEEP( g,  0, +1, -1, e )
#define NEIGHBOR_ST(g,e) GRID_ENTRY_SWEEP( g,  0, -1, +1, e )
#define NEIGHBOR_SB(g,e) GRID_ENTRY_SWEEP( g,  0, -1, -1, e )
#define NEIGHBOR_ET(g,e) GRID_ENTRY_SWEEP( g, +1,  0, +1, e )
#define NEIGHBOR_EB(g,e) GRID_ENTRY_SWEEP( g, +1,  0, -1, e )
#define NEIGHBOR_WT(g,e) GRID_ENTRY_SWEEP( g, -1,  0, +1, e )
#define NEIGHBOR_WB(g,e) GRID_ENTRY_SWEEP( g, -1,  0, -1, e )


#define COLLIDE_STREAM
#ifdef COLLIDE_STREAM

#define SRC_C(g)  LOCAL( g, C  )
#define SRC_N(g)  LOCAL( g, N  )
#define SRC_S(g)  LOCAL( g, S  )
#define SRC_E(g)  LOCAL( g, E  )
#define SRC_W(g)  LOCAL( g, W  )
#define SRC_T(g)  LOCAL( g, T  )
#define SRC_B(g)  LOCAL( g, B  )
#define SRC_NE(g) LOCAL( g, NE )
#define SRC_NW(g) LOCAL( g, NW )
#define SRC_SE(g) LOCAL( g, SE )
#define SRC_SW(g) LOCAL( g, SW )
#define SRC_NT(g) LOCAL( g, NT )
#define SRC_NB(g) LOCAL( g, NB )
#define SRC_ST(g) LOCAL( g, ST )
#define SRC_SB(g) LOCAL( g, SB )
#define SRC_ET(g) LOCAL( g, ET )
#define SRC_EB(g) LOCAL( g, EB )
#define SRC_WT(g) LOCAL( g, WT )
#define SRC_WB(g) LOCAL( g, WB )

#define DST_C(g)  NEIGHBOR_C ( g, C  )
#define DST_N(g)  NEIGHBOR_N ( g, N  )
#define DST_S(g)  NEIGHBOR_S ( g, S  )
#define DST_E(g)  NEIGHBOR_E ( g, E  )
#define DST_W(g)  NEIGHBOR_W ( g, W  )
#define DST_T(g)  NEIGHBOR_T ( g, T  )
#define DST_B(g)  NEIGHBOR_B ( g, B  )
#define DST_NE(g) NEIGHBOR_NE( g, NE )
#define DST_NW(g) NEIGHBOR_NW( g, NW )
#define DST_SE(g) NEIGHBOR_SE( g, SE )
#define DST_SW(g) NEIGHBOR_SW( g, SW )
#define DST_NT(g) NEIGHBOR_NT( g, NT )
#define DST_NB(g) NEIGHBOR_NB( g, NB )
#define DST_ST(g) NEIGHBOR_ST( g, ST )
#define DST_SB(g) NEIGHBOR_SB( g, SB )
#define DST_ET(g) NEIGHBOR_ET( g, ET )
#define DST_EB(g) NEIGHBOR_EB( g, EB )
#define DST_WT(g) NEIGHBOR_WT( g, WT )
#define DST_WB(g) NEIGHBOR_WB( g, WB )

#else /* COLLIDE_STREAM */

#define SRC_C(g)  NEIGHBOR_C ( g, C  )
#define SRC_N(g)  NEIGHBOR_S ( g, N  )
#define SRC_S(g)  NEIGHBOR_N ( g, S  )
#define SRC_E(g)  NEIGHBOR_W ( g, E  )
#define SRC_W(g)  NEIGHBOR_E ( g, W  )
#define SRC_T(g)  NEIGHBOR_B ( g, T  )
#define SRC_B(g)  NEIGHBOR_T ( g, B  )
#define SRC_NE(g) NEIGHBOR_SW( g, NE )
#define SRC_NW(g) NEIGHBOR_SE( g, NW )
#define SRC_SE(g) NEIGHBOR_NW( g, SE )
#define SRC_SW(g) NEIGHBOR_NE( g, SW )
#define SRC_NT(g) NEIGHBOR_SB( g, NT )
#define SRC_NB(g) NEIGHBOR_ST( g, NB )
#define SRC_ST(g) NEIGHBOR_NB( g, ST )
#define SRC_SB(g) NEIGHBOR_NT( g, SB )
#define SRC_ET(g) NEIGHBOR_WB( g, ET )
#define SRC_EB(g) NEIGHBOR_WT( g, EB )
#define SRC_WT(g) NEIGHBOR_EB( g, WT )
#define SRC_WB(g) NEIGHBOR_ET( g, WB )

#define DST_C(g)  LOCAL( g, C  )
#define DST_N(g)  LOCAL( g, N  )
#define DST_S(g)  LOCAL( g, S  )
#define DST_E(g)  LOCAL( g, E  )
#define DST_W(g)  LOCAL( g, W  )
#define DST_T(g)  LOCAL( g, T  )
#define DST_B(g)  LOCAL( g, B  )
#define DST_NE(g) LOCAL( g, NE )
#define DST_NW(g) LOCAL( g, NW )
#define DST_SE(g) LOCAL( g, SE )
#define DST_SW(g) LOCAL( g, SW )
#define DST_NT(g) LOCAL( g, NT )
#define DST_NB(g) LOCAL( g, NB )
#define DST_ST(g) LOCAL( g, ST )
#define DST_SB(g) LOCAL( g, SB )
#define DST_ET(g) LOCAL( g, ET )
#define DST_EB(g) LOCAL( g, EB )
#define DST_WT(g) LOCAL( g, WT )
#define DST_WB(g) LOCAL( g, WB )

#endif /* COLLIDE_STREAM */

#define MAGIC_CAST(v) ((unsigned int*) ((void*) (&(v))))
#define FLAG_VAR(v) unsigned int* const _aux_ = MAGIC_CAST(v)

#define TEST_FLAG_SWEEP(g,f)     cond(LOCAL(g, FLAGS),f)
#ifdef TAPENADE
function cond(g, f)
  real :: g
  integer :: i, f
  logical :: cond
  cond = g .gt. f
end function
#else
#ifndef CONDNONE
function cond(g, f)
  real :: g
  integer :: i, f
  logical :: cond
  cond = (0 .ne. iand(transfer(g,i), f))
end function
#endif
#endif




#endif /* _LBM_MACROS_H_ */
