#!/bin/sh
export OMP_STACKSIZE=256M
ulimit -s unlimited
export OMP_PLACES='sockets(1)'
export LD_LIBRARY_PATH=/home/hueckelh/simd_adjoint_stencils/
export OMP_NUM_THREADS=1
./main_serial.exe -o out.bin -i 120_120_150_ldc.of -- 20
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=2
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=4
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=8
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=16
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=28
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
export OMP_NUM_THREADS=56
./main.exe -o out.bin -i 120_120_150_ldc.of -- 20
