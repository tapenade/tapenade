#ifndef _LBM_DIFF_H_
#define _LBM_DIFF_H_
void lbm_sc_(LBM_Grid srcGrid, LBM_Grid dstGrid);
void lbm_sc_d_(LBM_Grid srcGrid, LBM_Grid srcGridd, LBM_Grid dstGrid, LBM_Grid dstGridd);
void lbm_sc_b_(LBM_Grid srcGrid, LBM_Grid srcGridb, LBM_Grid dstGrid, LBM_Grid dstGridb);
#endif /* _LBM_DIFF_H_ */
