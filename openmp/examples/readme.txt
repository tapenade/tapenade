compile the scoping example with
> gfortran example_scopes.f90 ../OMP_FirstAidKit/adStack.c ../OMP_FirstAidKit/adOMP.c -fopenmp -o exscope
and compile the schedule example with
> gfortran example_schedules.f90 ../OMP_FirstAidKit/adStack.c ../OMP_FirstAidKit/adOMP.c -fopenmp -o exsched

finally, compile the benchmark example using the old or new stack with
> gfortran benchmark_stacks.f90 -fopenmp -O3 ../../ADFirstAidKit/adStack.c ../../ADFirstAidKit/adBuffer.f -o bench_tp
> gfortran benchmark_stacks.f90 -fopenmp -O3 ../OMP_FirstAidKit/adStack.c -o bench_cl
and run with
> ./bench_tp 100000000 0
> ./bench_cl 100000000 0

Notes on useful tricks regarding openmp:
> ulimit -s unlimited
 relaxes some size limit somewhere otherwise execution may segfault
> export OMP_STACKSIZE=10M
 larger stack for each omp thread otherwise execution may segfault
> export OMP_NUM_THREADS=3
 specify the number of threads

About the stencilrange test cases:
> cd tapenade/openmp/examples/stencilrange
> python3 generate_tests.py
> cd generated
> . compile_all.sh
