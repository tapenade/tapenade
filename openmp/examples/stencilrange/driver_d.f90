program driver
  use omp_lib
  implicit none
  integer :: n, iters, i
  double precision, allocatable, dimension(:) :: uold0, unew0
  double precision, allocatable, dimension(:) :: uold0d, unew0d
  double precision, allocatable, dimension(:) :: uold1, unew1
  double precision, allocatable, dimension(:) :: uold1d, unew1d
  double precision, dimension(STENCIL) :: weightsl, weightsr
  character(len=32) :: num1char, num2char
  double precision :: weightc
  double precision :: time

  if(command_argument_count() .lt. 2) then
    print *, 'Usage: driver <domainsize> <iteration>'
    stop
  endif
  call get_command_argument(1, num1char)
  read(num1char,*) n
  call get_command_argument(2, num2char)
  read(num2char,*) iters
  print *, "threads/stencilsize/domainsize/reps/iters:", &
          omp_get_max_threads(), STENCIL, n, REP, iters

  allocate(uold0(n), unew0(n))
  allocate(uold0d(n), unew0d(n))
  allocate(uold1(n), unew1(n))
  allocate(uold1d(n), unew1d(n))

  do i=1,n
    uold0(i) = sin(dble(i))
    uold0d(i) = sin(dble(i))
    unew0(i) = 0
    unew0d(i) = 0
    uold1(i) = sin(dble(i))
    uold1d(i) = sin(dble(i))
    unew1(i) = 0
    unew1d(i) = 0
  end do
  do i=1,STENCIL
    weightsl(i) = 1.0/i
    weightsr(i) = 1.0/i
  end do
  weightc = -2.0*sum(weightsl)

  ! warmup loop
  do i=1,iters/10
    call conv1dplain_d(unew0, unew0d, uold0, uold0d, &
                       weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dplain_d(unew0, unew0d, uold0, uold0d, &
                       weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "plaind time ", time, " sum ", sum(unew0d)

  ! warmup loop
  do i=1,iters/10
    call conv1dcompact_d(unew1, unew1d, uold1, uold1d, &
                         weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dcompact_d(unew1, unew1d, uold1, uold1d, &
                         weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "cmpctd time ", time, " sum ", sum(unew1d)

  deallocate(unew1, uold1)
  deallocate(unew1d, uold1d)
  deallocate(unew0, uold0)
  deallocate(unew0d, uold0d)
end program
