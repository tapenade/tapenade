import sys

# Get the file name that contains the timings
if(len(sys.argv)<4):
  print("Usage: filtertimes.py <input_file0> <input_file1> <input_file2> [column=filtervalue]")
  exit()
filename0 = sys.argv[1]
filename1 = sys.argv[2]
filename2 = sys.argv[3]

# Get the columns to filter, e.g. threads=16 to show only results with that thread count.
# Multiple filters can be given, e.g. threads=16 domainsize=1000
keys = ("threads", "stencilsize", "domainsize", "reps", "iters",
        "plain_time", "compact_time", "plaind_time", "compactd_time",
        "plainbr_time", "plainba_time", "compactb_time")
filters = list()
if(len(sys.argv)>3):
  for arg in sys.argv[4:]:
    key,val = arg.split("=")
    if(not key in keys):
      print("Error: column must be one of "+str(keys))
      exit()
    filters.append((key,val))

# Initialize
counter = 0
threads = None
stencilsize = None
domainsize = None
reps = None
iters = None
plain_time = None
compact_time = None
plaind_time = None
compactd_time = None
plainbr_time = None
plainba_time = None
compactb_time = None
rec = None
# The input files contain a sequence of lines that belong to one setting.
# The first line is always the setting, the next one is the primal time,
# and so forth. A setting has 9 lines, after that the counter is reset
# and the subsequent line is interpreted as the start of a new setting.
with open(filename0) as fp0, open(filename1) as fp1, open(filename2) as fp2:
  print("# " + ", ".join(keys))
  for line0, line1, line2 in zip(fp0, fp1, fp2):
    # The first line is the test case specification, the subsequent lines
    # contain the timings for this test case. The specification consists
    # of the number of threads, stencil radius, etc.
    if(counter == 0):
      rec = dict()
      _, rec["threads"], rec["stencilsize"], rec["domainsize"], rec["reps"], rec["iters"] = line0.split()
    # Here come the timings
    if(counter == 1):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["plain_time"] = min(res0,res1,res2)
    if(counter == 2):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["compact_time"] = min(res0,res1,res2)
    if(counter == 4):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["plaind_time"] = min(res0,res1,res2)
    if(counter == 5):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["compactd_time"] = min(res0,res1,res2)
    if(counter == 7):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["plainbr_time"] = min(res0,res1,res2)
    if(counter == 8):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["plainba_time"] = min(res0,res1,res2)
    if(counter == 9):
      _, _, res0, _, _ = line0.split()
      _, _, res1, _, _ = line1.split()
      _, _, res2, _, _ = line2.split()
      rec["compactb_time"] = min(res0,res1,res2)
    counter = counter + 1
    # We have reached the end of this setting. It is time to check our
    # filters, and if required, print the setting and its results.
    if(counter == 10):
      printThis = True
      for fcol,fval in filters:
        if(rec[fcol] != fval):
          printThis = False
      if(printThis):
        print(", ".join([rec[x] for x in keys]))
      # Now reset the counter, so that the next line will be interpreted
      # as a new setting again.
      counter = 0
