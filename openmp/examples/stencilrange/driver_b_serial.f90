program driver
  use omp_lib
  implicit none
  integer :: n, iters, i
  double precision, allocatable, dimension(:) :: uold0, unew0
  double precision, allocatable, dimension(:) :: uold0b, unew0b
  double precision, allocatable, dimension(:) :: uold1, unew1
  double precision, allocatable, dimension(:) :: uold1b, unew1b
  double precision, allocatable, dimension(:) :: uold2, unew2
  double precision, allocatable, dimension(:) :: uold2b, unew2b
  double precision, dimension(STENCIL) :: weightsl, weightsr
  character(len=32) :: num1char, num2char
  double precision :: weightc
  double precision :: time

  if(command_argument_count() .lt. 2) then
    print *, 'Usage: driver <domainsize> <iteration>'
    stop
  endif
  call get_command_argument(1, num1char)
  read(num1char,*) n
  call get_command_argument(2, num2char)
  read(num2char,*) iters
  print *, "threads/stencilsize/domainsize/reps/iters:", &
          omp_get_max_threads(), STENCIL, n, REP, iters

  allocate(uold0(n), unew0(n))
  allocate(uold0b(n), unew0b(n))
  allocate(uold1(n), unew1(n))
  allocate(uold1b(n), unew1b(n))
  allocate(uold2(n), unew2(n))
  allocate(uold2b(n), unew2b(n))

  do i=1,n
    uold0(i) = sin(dble(i))
    uold0b(i) = 0
    unew0(i) = 0
    unew0b(i) = cos(dble(i))
    uold1(i) = sin(dble(i))
    uold1b(i) = 0
    unew1(i) = 0
    unew1b(i) = cos(dble(i))
    uold2(i) = sin(dble(i))
    uold2b(i) = 0
    unew2(i) = 0
    unew2b(i) = cos(dble(i))
  end do
  do i=1,STENCIL
    weightsl(i) = 1.0/i
    weightsr(i) = 1.0/i
  end do
  weightc = -2.0*0.99*sum(weightsl)

  ! warmup loop
  do i=1,iters/10
    call conv1dplain_b(unew0, unew0b, uold0, uold0b, &
                       weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dplain_b(unew0, unew0b, uold0, uold0b, &
                       weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "plainb time ", time, " sum ", sum(uold0b)

  ! warmup loop
  do i=1,iters/10
    call conv1dcompact_b(unew1, unew1b, uold1, uold1b, &
                         weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dcompact_b(unew1, unew1b, uold1, uold1b, &
                         weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "cmpctb time ", time, " sum ", sum(uold1b)

  deallocate(unew2, uold2)
  deallocate(unew2b, uold2b)
  deallocate(unew1, uold1)
  deallocate(unew1b, uold1b)
  deallocate(unew0, uold0)
  deallocate(unew0b, uold0b)
end program
