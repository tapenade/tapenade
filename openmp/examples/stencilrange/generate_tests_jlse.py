import textwrap
from string import Template

def expandTerm(term, repetitions):
  """Inflates the amount of work in an expression. Given some term (e.g. a*b),
     this function returns an expanded term (w1*a*b+w2*a*b+w3*a*b+...) where
     (w1+w2+... = 1.0), so the result is unaffected up to machine precision."""
  sumWeights = (repetitions)*(repetitions+1)/2
  terms = list()
  for id in range(1,repetitions+1):
    terms.append(Template(term).substitute(expansionfactor=str(id/sumWeights)))
  return "".join(terms)
  

def getConvPlain(stencilsize, repetitions):
  centerpoint = "inval = uold(i)\noutval = 0.0\n" + expandTerm("outval = outval + ${expansionfactor}*weightc*inval\n",repetitions) + "unew(i) = unew(i) + outval\n"
  rightpoints = list()
  leftpoints = list()
  for s in range(stencilsize,0,-1):
    rightpoints.append("inval = uold(i+%d)\noutval = 0.0\n"%s + expandTerm("outval = outval + ${expansionfactor}*weightsr(%d)*inval\n"%s,repetitions) + "unew(i) = unew(i) + outval\n")
    leftpoints.append("inval = uold(i-%d)\noutval = 0.0\n"%s + expandTerm("outval = outval + ${expansionfactor}*weightsl(%d)*inval\n"%s,repetitions) + "unew(i) = unew(i) + outval\n")
  leftpoints.reverse()
  loopbody = "".join(rightpoints)+centerpoint+"".join(leftpoints)
  ccode = ""
  with open("templates/plain.template.f90") as fp:
    ccode = Template(fp.read())
  return ccode.substitute(stencilsize=stencilsize, loopbody=textwrap.indent(loopbody, " "*10))


def getConvCompact(stencilsize, repetitions):
  centerpoint = "inval = uold(i)\noutval = 0.0\n" + expandTerm("outval = outval + ${expansionfactor}*weightc*inval\n",repetitions) + "unew(i) = unew(i) + outval\n"
  rightpoints = list()
  leftpoints = list()
  for s in range(stencilsize,0,-1):
    rightpoints.append("inval = uold(i-%d)\noutval = 0.0\n"%s + expandTerm("outval = outval + ${expansionfactor}*weightsl(%d)*inval\n"%(s),repetitions) + "unew(i) = unew(i) + outval\n")
    leftpoints.append("outval = 0.0\n" + expandTerm("outval = outval + ${expansionfactor}*weightsr(%d)*inval\n"%(s),repetitions) + "unew(i-%d) = unew(i-%d) + outval\n"%(s,s))
  loopbody = "".join(rightpoints)+centerpoint+"".join(leftpoints)
  rightpull = "inval = uold(i+s)\noutval = 0.0\n" + expandTerm("outval = outval + ${expansionfactor}*weightsr(s)*inval\n",repetitions) + "unew(i) = unew(i) + outval\n"
  leftpull = "inval = uold(i-s)\noutval = 0.0\n" + expandTerm("outval = outval + ${expansionfactor}*weightsl(s)*inval\n",repetitions) + "unew(i) = unew(i) + outval\n"
  ccode = ""
  with open("templates/compact.template.f90") as fp:
    ccode = Template(fp.read())
  return ccode.substitute(stencilsize=stencilsize,
                          loopbody=textwrap.indent(loopbody, " "*10),
                          leftpull=textwrap.indent(leftpull, " "*10),
                          rightpull = textwrap.indent(rightpull, " "*10),
                          centerpoint = textwrap.indent(centerpoint, " "*10))


maxiter = 8*64*1e7
with open("generated/compile_all.sh", 'w') as fp_compile, open('generated/compile_serial.sh', 'w') as fp_compile_serial:
  with open("generated/differentiate_all.sh", 'w') as fp_diff:
    with open("generated/run_all.sh", 'w') as fp_run, open("generated/run_serial.sh", 'w') as fp_run_serial:
      fp_run.write("#!/bin/sh\nexport OMP_STACKSIZE=128M\nulimit -s unlimited\nexport OMP_PLACES='sockets(1)'\nexport LD_LIBRARY_PATH=/home/hueckelh/simd_adjoint_stencils/\n")
      fp_run_serial.write("#!/bin/sh\nulimit -s unlimited\nexport OMP_PLACES='sockets(1)'\nexport LD_LIBRARY_PATH=/home/hueckelh/simd_adjoint_stencils/\n")
      for stencilsize in (1,2,8):
        for repetitions in (1,8,64):
          filename = "%d_%d.f90"%(stencilsize,repetitions)
          with open("generated/"+filename, 'w') as fp, open("generated/serial_"+filename, 'w') as fp_serial:
            fp.write(getConvPlain(stencilsize,repetitions))
            fp.write(getConvCompact(stencilsize,repetitions))
            fp_serial.write(getConvPlain(stencilsize,repetitions).replace("!$omp", "!omp"))
            fp_serial.write(getConvCompact(stencilsize,repetitions).replace("!$omp", "!omp"))
          # the "-nooptim mergediff" flag prevents Tapenade from simplifying the derivative terms,
          # which would remove the artificially inflated computation that is used in our test case
          # to adjust the arithmetic intensity.
          fp_diff.write('../../adSIMD.py %s "conv1dPlain(unew,uold)/(uold) conv1dDLT(unew,uold)/(uold) conv1dCompact(unew,uold)/(uold) conv1dCompactDLT(unew,uold)/(uold)" "tapenade -nooptim mergediff"\n'%filename) 
          fp_compile.write("STENCIL=%d REP=%d ADFIRSTAIDKIT=/home/hueckelh/ADFirstAidKit/ ADOMPFIRSTAIDKIT=/home/hueckelh/tap_omp_tests/OMP_FirstAidKit make -f ../Makefile_template\n"%(stencilsize,repetitions))
          fp_compile_serial.write("STENCIL=%d REP=%d ADFIRSTAIDKIT=/home/hueckelh/ADFirstAidKit/ ADOMPFIRSTAIDKIT=/home/hueckelh/tap_omp_tests/OMP_FirstAidKit/ make -f ../Makefile_template_serial\n"%(stencilsize,repetitions))
          for domainsize in (1e3,1e4,1e5,1e6,1e7):
            iters = maxiter / domainsize / min(repetitions,8) / min(stencilsize,2)
            fp_run_serial.write("./execs_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
            fp_run_serial.write("./execds_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
            fp_run_serial.write("./execbs_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
            for threads in (1,2,4,8,16,28,56):
              fp_run.write("export OMP_NUM_THREADS=%d\n"%(threads))
              iters = maxiter / domainsize / min(repetitions,8) / min(stencilsize,2)
              fp_run.write("./exec_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
              fp_run.write("./execd_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
              fp_run.write("./execb_%d_%d %d %d\n"%(stencilsize,repetitions,domainsize,iters))
