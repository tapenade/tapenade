subroutine conv1dplain(unew, uold, weightsl, weightsr, weightc, n)
  implicit none
  integer :: n, i
  double precision, dimension(0:n-1) :: unew, uold
  double precision, dimension(${stencilsize}) :: weightsl, weightsr
  double precision :: weightc, inval, outval
  !$$omp parallel do private(inval,outval) schedule(static)
  do i = ${stencilsize}, n-${stencilsize}-1
${loopbody}
  end do
end subroutine
