subroutine conv1dcompact(unew, uold, weightsl, weightsr, weightc, n)
  implicit none
  integer :: n, i, s, ir, offset, from
  double precision, dimension(0:n-1) :: unew, uold
  double precision, dimension(${stencilsize}) :: weightsl, weightsr
  double precision :: weightc, inval, outval
  do offset=0,${stencilsize}
    from = 2*${stencilsize}+offset
    !$$omp parallel do private(inval,outval) schedule(static)
    do i = from, n-${stencilsize}-1, ${stencilsize}+1
${loopbody}
    end do
  end do

  do ir=0, ${stencilsize}-1
    ! left remainder cells ...
    i = ir + ${stencilsize}
    ! ... center
${centerpoint}
    ! ... left neighbors
    do s=1, ${stencilsize}
${leftpull}
    end do
    ! ... right neighbors
    do s=1, ${stencilsize}-ir-1
${rightpull}
    end do

    ! right remainder cells ...
    i = n - 2*${stencilsize}+ ir
    ! ... right neighbors
    do s=${stencilsize}-ir,${stencilsize}
${rightpull}
    end do
  end do
end subroutine
