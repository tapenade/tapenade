import sys

if(len(sys.argv)<3):
  print("Usage: postprocess.py <input_file> <output_file>")
  exit()

filename = sys.argv[1]
filename_post = sys.argv[2]
inPlain = False
inCompact = False
reductionVersion = list()
atomicVersion = list()
with open(filename) as fp:
  with open(filename_post, 'w') as fp_post:
    for line in fp:
      if("SUBROUTINE CONV1DPLAIN_B" in line):
        inPlain = True
      if("END SUBROUTINE CONV1DPLAIN_B" in line):
        inPlain = False
        reductionVersion.append(line)
        line = line.replace("CONV1DPLAIN_B", "CONV1DPLAIN_B_ATOMIC")
        atomicVersion.append(line)
      if("SUBROUTINE CONV1DCOMPACT_B" in line):
        inCompact = True
      if("END SUBROUTINE CONV1DCOMPACT_B" in line):
        inCompact = False
        fp_post.write(line)
      if(inCompact):
        line = line.replace("REDUCTION(+:","SHARED(")
        fp_post.write(line)
      if(inPlain):
        reductionVersion.append(line)
        line = line.replace("REDUCTION(+:","SHARED(")
        line = line.replace("CONV1DPLAIN_B", "CONV1DPLAIN_B_ATOMIC")
        if("uoldb(" in line):
          atomicVersion.append("!$omp atomic update\n")
        atomicVersion.append(line)
      if("_d" in filename):
        line = line.replace("DO , SCHEDULE", "DO SCHEDULE")
        fp_post.write(line)
    fp_post.write("".join(reductionVersion))
    fp_post.write("".join(atomicVersion))
