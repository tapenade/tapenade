program driver
  use omp_lib
  implicit none
  integer :: n, iters, i
  double precision, allocatable, dimension(:) :: uold0, unew0
  double precision, allocatable, dimension(:) :: uold1, unew1
  double precision, dimension(STENCIL) :: weightsl, weightsr
  character(len=32) :: num1char, num2char
  double precision :: weightc
  double precision :: time

  if(command_argument_count() .lt. 2) then
    print *, 'Usage: driver <domainsize> <iteration>'
    stop
  endif
  call get_command_argument(1, num1char)
  read(num1char,*) n
  call get_command_argument(2, num2char)
  read(num2char,*) iters
  print *, "threads/stencilsize/domainsize/reps/iters:", &
          omp_get_max_threads(), STENCIL, n, REP, iters

  allocate(uold0(n), unew0(n))
  allocate(uold1(n), unew1(n))

  do i=1,n
    uold0(i) = sin(dble(i))
    unew0(i) = 0
    uold1(i) = sin(dble(i))
    unew1(i) = 0
  end do
  do i=1,STENCIL
    weightsl(i) = 1.0/i
    weightsr(i) = 1.0/i
  end do
  weightc = -2.0*sum(weightsl)

  ! warumup loop
  do i=1,iters/10
    call conv1dplain(unew0, uold0, weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dplain(unew0, uold0, weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "plain time ", time, " sum ", sum(unew0)

  ! warumup loop
  do i=1,iters/10
    call conv1dcompact(unew1, uold1, weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime()
  ! timing loop
  do i=1,iters
    call conv1dcompact(unew1, uold1, weightsl, weightsr, weightc, n)
  end do
  time = omp_get_wtime() - time
  print *, "cmpct time ", time, " sum ", sum(unew1)

  deallocate(unew1, uold1)
  deallocate(unew0, uold0)
end program
