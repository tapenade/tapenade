subroutine stack_serial(n)
implicit none
integer i, n
double precision b
b = 3.1415
do i=1,n
  call pushreal8(b)
end do
do i=1,n
  call popreal8(b)
end do
end subroutine

subroutine stack_parallel(n)
implicit none
integer i, n
double precision b
b = 3.1415
!$omp parallel do
do i=1,n
  call pushreal8(b)
end do
!$omp parallel do private(b)
do i=1,n
  call popreal8(b)
end do
end subroutine

program main
  use omp_lib
  character(32) :: niterchar
  integer :: n, p
  real (kind = 8), dimension(3) :: t
  call get_command_argument(1,niterchar)
  read(niterchar,*)n
  call get_command_argument(2,niterchar)
  read(niterchar,*)p

  t(1) = omp_get_wtime()
  call stack_serial(n)
  t(2) = omp_get_wtime()
  if(p .eq. 1) then
    call stack_parallel(n)
  end if
  t(3) = omp_get_wtime()
  print *, 'time ser: ', t(2)-t(1)
  print *, 'time par: ', t(3)-t(2)
end program

