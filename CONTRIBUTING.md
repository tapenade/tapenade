Gitlab and workflow
-------------------

Tapenade source repository is located here:
[https://gitlab.inria.fr/tapenade/tapenade](https://gitlab.inria.fr/tapenade/tapenade).

Development uses the [gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow).
The `master` branch stores the official release history, and the `develop` 
branch serves as an integration branch for features.

Continuous integration and tests
--------------------------------

Each pull from a git branch is verified by the automated tests on
the [gitlab-ci](https://gitlab.inria.fr/tapenade/tapenade/-/pipelines) 
and [Jenkins](https://ci.inria.fr/test-tapenade/)
continuous integration servers.
For `master` and `develop` git branches, all tests must succeed.

Cf `.gitlab-ci.yml` configuration file for gitlab pipeline and
[../local/README-jenkins.md](https://gitlab.inria.fr/tapenade/local/)
for Jenkins jobs.

Java conventions
----------------
We recomment no specific coding convention other than "make life easy for the person who will read your code". If you develop a new component in Tapenade, feel free to use the convention that you feel most readable. If you just browse through an existing piece of code, please refrain from enforcing your own favorite conventions. If you need to apply just a few changes to the source, use your judgement. Please assume that the previous contributor had good reasons for their stylistic choices.

We strongly discourage using automatic reformatting tools to the source.

Convention, nomenclature on Tapenade variable names
---------------------------------------------------
 - "source", "src", should be used (rather than "orig", "origin") to refer
        to things that belong to the original, non-differentiated program.<br>
 - "copy" should be used to refer to copies of the source objects that
        will be placed into the reverse differentiated code.<br>
 - "reverse", "rev", should be used to refer to the reverse mode of AD, e.g.
        to the complete adjoint code and not only its backwards sweep.<br>
 - "diff" should be used to relate to the general differentiated code, or
        specifically to its backward sweep if the forward sweep has a different
        thing for itself (ADJOINT_SPLIT_MODE). In this last case, the specific thing for
        the split forward sweep is referred to by "fwdDiff" instead of "diff".<br>
 - "origin", "orig", should be used to refer to the origin of a directed arrow,
        such as the "origin" of a FGArrow.<br>
 - "destination", "dest", should be used to refer to the destination of a directed arrow,
        such as the "destination" of a FGArrow.<br>
 - "forward", "fwd", should be used to refer to the forward sweep
        of a reverse-differentiated code.<br>
 - "backward", "bwd", should be used to refer to the reverse sweep
        of a reverse-differentiated code.<br>
 - "downstream", "ds", should be used to refer to the source execution
        direction that goes from entry to exit in the "source" program.
        In the bwd sweep, code is executed "upstream".<br>
 - "upstream", "us", should be used to refer to the opposite of the source
        execution direction that goes from exit to entry in the "source"
        program, and therefore goes along the execution direction of the bwd sweep.<br>
 - "entry" refers to the true, final execution order of an object.<br>
 - "exit" refers to the true, final execution order of an object. Therefore a Block or
        a sweep, regardless fwd or bwd, is always executed from entry to exit.


Comments
--------
Javadoc comments in the Java source files are delimited by 
the `/** ... */` delimiters. 
These comments are processed by 
[Javasphinx](https://bronto-javasphinx.readthedocs.io/en/latest/#)
to generate the API documentation and by
the Javadoc tool to generate the complete implementation documentation. 


Documentation
-------------
README*.md files 
([Mardown syntax](https://www.markdownguide.org/cheat-sheet/))
and index.rst file
([reStructuredText](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html))
are used to generate the Tapenade Developer Documentation
using
[Sphinx documentation generator](https://www.sphinx-doc.org).
