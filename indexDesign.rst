Tapenade Design
===============

.. toctree::
   :maxdepth: 2

   src/README
   src/main/java/fr/inria/tapenade/toplevel/index
   src/main/java/fr/inria/tapenade/frontend/index
   docs/syntaxIL
   src/main/java/fr/inria/tapenade/representation/classes
   src/main/java/fr/inria/tapenade/representation/index
   src/main/java/fr/inria/tapenade/representation/dataflow
   src/main/java/fr/inria/tapenade/analysis/README
   src/main/java/fr/inria/tapenade/differentiation/index
   src/main/java/fr/inria/tapenade/ir2tree/README
   src/main/java/fr/inria/tapenade/prettyprint/README
   src/main/html/README
