
/*********************************************************************/
/*********************************************************************/
/*     Determine circumferentially averaged mean values along        */
/*********************************************************************/
/*  last edit: 05.05.2004   A. Weber, E. Kuegeler                    */
/*********************************************************************/
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>  

//#include "post.h"


#define ON      1
#define OFF     0

/**********************************************************/
/* values for determination of boundary layer edge by Khalid-method. */
/* added by Dragan, 02.04.2004 */
#define VALUE_CUTOFF     0.60
#define FS_BLOCK_PART    0.20
#define PART_OF_LNORM    0.20

/**********************************************************/

/**********************************************************/

#define RADIAL_MACHINE   OFF 
/* #define ROTATIONAL       ON */
#define MAX_BLOCKS       999
#define MAX_STAGES       50
#define MAX_RADIAL_DIM   300
#define OUTPUT_TECPLOT10 OFF

/**********************************************************/

#define    MAX_CHARACTER   160
#define    PI             (4.0 * atan(1.0) )
#define    EPSILON         1.e-12
#define    sqr(x)          ((x)*(x))
#define    sgn(x)          ((x) < 0.0 ? (-1.0) : (x) > 0.0 ? 1.0 : 0.0)
#define    max(x,y)        ((x) <= (y) ? (y) : (x))
#define    min(x,y)        ((x) <= (y) ? (x) : (y))
#define    OUT printf
#define    NEW(a,b) if((a=(void*)malloc(b))==NULL){\
           OUT("[%s:%d]*****SERIOUS****malloc for %lu bytes failed\n", __FILE__, __LINE__, (unsigned long)b);exit  (-1);}

/**********************************************************/






typedef double  Ffloat;
typedef int     Fint;
typedef char    Fmsg[256];
typedef char    char33[33] ;

typedef struct
{
    Ffloat  re, im;
}   Fcomplex;

typedef struct
{
    Fcomplex  val0, val1, val2;
}   Feigen;

typedef struct
{
    Ffloat  col0[3], col1[3], col2[3];
}   Fmatrix3x3;

typedef enum
{
    structured,
    unstructured
} MeshTopology;

typedef enum
{
    hexahedra = 8,
    tetrahedra = 4,
    prism = 6,
    pyramid = 5,
    quadrilateral = 4,
    triangular = 3,
    mixed      = 0
} FelementType;


typedef	struct{
    Fint	num_cells;
    Fint	*cellIndex;    
    Ffloat      *dist;
} Funstruct;

typedef	struct{
    Fint	 *vertex;
    FelementType type;    
} Fconnect;

typedef struct
{
    Ffloat      x, y, z;
} Fcoord;

typedef enum
{
    STEADY,
    UNSTEADY
}       Fsimulation;

typedef struct {
    Fcomplex rho,u,v,w,p,eddyVisc;
    Fcomplex rho_u,rho_v,rho_w;
    Ffloat SPL;
    Ffloat x,y,z;    
}FnodeFourier;

typedef struct
{
    Ffloat	x, y, z;
}	
FvecCartesian;

typedef enum
{
  AMPLITUDE,
  PHASE,
  FLUCT,
  REAL,
  IMAG,
  MODAL,
  PHYS,
  RMS,
  BLADE_SURFACES,
  WAVE_SPLIT
}
FfourierOutputMode;

typedef struct
{
    Ffloat x,y,z,r,phi;
    Ffloat rho,u,v,w,p,eddyVisc,a,M,entropy,T;
    Ffloat vR,vPhi;
    FvecCartesian    vort;
}
FannulusData;

typedef struct
{
    Ffloat         r_mean;

    Fint           m,n;

    Ffloat         x,radius,SPL;
    Fcomplex       rho, u, v, w, p;
    Fcomplex       eddyVisc, M,T;
    Fcomplex       vorticity,entropy;
    Fcomplex       vPhi, vM, vR;
    Fcomplex       vort_x,vort_y,vort_z;
    Fcomplex       a;    
}
FMatchVal;

typedef struct {
    Fint timeSteps,noOfHarmonics,globalTimeSteps,timeStepsBPF;
    Ffloat phi0,timeShift,timeScale,delta_t;
    Fint mode[30];
}FfourierGlobal;


typedef enum
{
        ENABLED,
        DISABLED
}
FmessageMode;

typedef struct
{
    Ffloat       rho, u, v, w, p;
    Ffloat       eddyVisc, M;
    Ffloat       tke, tls;
    Ffloat       vorticity;
    /* new for matching TUe */
    Ffloat      x,r,phi;
    Ffloat       vR,vPhi,vM;
    Ffloat       T,entropy,a;
    FvecCartesian    vort;
}
FphysVal;

typedef struct
{
    Ffloat      x, y, z, A_norm;
    Ffloat      radius, J;
    Ffloat      xc;  /* dimensionslose Sehnenlaenge */
    Ffloat      x_av, y_av, z_av; /* Werte in Zell-Mitte */
}
FgeomVal;

typedef struct
{
    Ffloat       uu, uv, vv, wu, wv, ww, D, Spur, RKE, RKE_Phi;
    Ffloat       vM_vM, vM_vR, vM_vPhi, vR_vR, vR_vPhi, vPhi_vPhi;
}
FstressVal;

typedef struct
{
    FphysVal    phys;
    FgeomVal    geom;
    Funstruct   *unstruct;
    FstressVal  stresses;
    FphysVal    fluct;
}
Fnode;

typedef	struct{
    Fint	num_cells;
    Fint	*cellIndex;    
    Ffloat      *dist;
    FphysVal    *phys;
    Fint        *donorFlag;
} FvertexOnPanel;

typedef	struct{
    FvertexOnPanel *vertex;
    Fint           *pointList;
    Fint           *pointListDonor;
    Fint           num_vertices;   
    Fint           donorZoneId;
    char33         donorZoneName;
    char33         panelGroup;
    char33         panelGroupDonor;
    Fint           panelIdDonor;
} FpanelInterior;

typedef enum
{
    LEFT,
    RIGHT,
    LOWER,
    UPPER,
    EARTH,
    SPACE
}
BdSide;

typedef struct
{
    Fint         bladeId;
    Fint         nBlades;
    BdSide       side;
} FaeSurfaceData;

typedef struct
{
    Ffloat      amplitude, omega;
    Fint         circumferentialMode, radialMode;
} FacousticData;


typedef struct
{
    char33                  name;
    char33                  BCFamName;    
    int                     npnts;  /* number of faces */
    int                     *pnts;  /* ptr to faceId */  
}
FzoneBCInfo;


typedef struct
{
    FphysVal                phys;
    FgeomVal                geom;
}
FvertexData;


typedef struct
{  
    /*------- */
    FzoneBCInfo        zoneBC;
    /*------- */
    Fconnect           *faceConnect;
    /*------- */
    Fint               num_vertices;
    FvertexData        *vertex;
    /*------- */
    Fint               outputFlag;
}
UpanelBd;


typedef struct
{
    Fnode        *pNode;
    int          nodeJdim, nodeKdim, nodeLdim;
    Ffloat       pitch; 
    int          Segments, stageId;
    int          stokesLower, stokesUpper, stokesEarth, stokesSpace;
    int          solidLower, solidUpper, solidLeft, solidRight, solidEarth, solidSpace;
    int          defaultStokes;
    int          interfaceLeft, interfaceRight;
    Ffloat       RPMleft, RPMright;
    
    FfourierGlobal global_fourier;
    FnodeFourier *fNode;
    FannulusData *annulus;

    Ffloat omega;

    /* --- AE Module --- */
    FaeSurfaceData ae;
    /* ----------------- */

    /* --- Acoustic --- */
    FacousticData    acoustic;
    /* ----------------- */

    /* --- hybrid mesh --- */
    MeshTopology   topology;
    Fconnect       *pConnect;
    Fint           cellDim;
    Fint           num_interiorPanels;
    FpanelInterior *interiorPanel;
    char33         zoneName;
    Fint           num_bdPanelsU;
    UpanelBd      *bdPanelU;
    /* -------------------- */
}
Fblock;

typedef struct
{
    Ffloat  F1, F2, F3, F4, F5;
    Ffloat  H, Sum;
    Ffloat  a_x, a_y, a_z, a_phi, a_r;
}
FfluxVal;

typedef struct
{
    Fcomplex    rho, u, v, w, p, eddyVisc, M;
}
FnodeFFT;

typedef struct
{
    FphysVal    phys;
    Fcomplex    func;
}
FindexDataFFT;

typedef struct
{
    int            J, K, L, BLK;
    FindexDataFFT *indexData;
}
FindexFFT;

typedef struct
{
    FnodeFFT    *pNodeFFT;
}
FblockFFT;

typedef struct 
{
    Ffloat      gamma;
    Ffloat      p_total;
    Ffloat      t_total;
    Ffloat      rho_0,a_0, r_gas;
    Ffloat      length;
    Ffloat      reynolds_no;
    Ffloat      delta_t, rpm, omega, dy_dt;

    Ffloat     rpm_ref,segments_ref,r_inner,r_outer,freq_ref;
    Fint        timeSteps_ref; 
}  
RefVals;

typedef enum
{
    ENSEMBLE,
    CONSERVATIVE
}
FaverageType;

typedef enum
 {
     rotational,
     cartesian
}
FreferenceFrame;

typedef enum
 {
     COMPRESSOR,
     TURBINE
}
FmachineType;

typedef enum
{
    NORMED,
    UN_NORMED
} 
FmeshMode;

typedef struct 
{
    Ffloat     entropy;
    Ffloat     massflow, alpha_rel, alpha_abs, beta_rel, beta_abs;
    Ffloat     p_total_rel, p_total_abs, p_static, p_static_area;
    Ffloat     t_total_rel, t_total_abs, t_total_rot, t_static, etaIs, zeta;
    Ffloat     vAx, vRad, vTanAbs, vTanRel;
    Ffloat     density, mach_rel, mach_abs, mach_mer, tke, tls, eddyVisc, diffusionFactor;
    Ffloat     dy_dt, omega, rpm, radius, radiusReal, chordlength;
    Ffloat     rot_energy, massCool, mTotalCool, massLocal;
    int        block_number, segments, timestep;
    BdSide     side;
} 
BdVal;

typedef struct
{
    BdVal  radLeft[MAX_RADIAL_DIM], radRight[MAX_RADIAL_DIM];
    BdVal  globalLeft, globalRight;
}
BdStageVal;


typedef struct
{
    BdVal  left, right; 
    Fint   timestep;
    Ffloat massLoss, entropy, pressureLoss, etaIs;
}
StageVal;

typedef struct 
{
    StageVal *stage;
    Fint  noOfLines, timesteps_per_period;
    Fmsg  fname_left, fname_right;
}
BdPerformance;

typedef struct
{
    FmessageMode tm_wf;
}
Fturbulence;

typedef struct
{
    Fint       jE, kE, lE;             /* freestream condition */
    Ffloat     rhoE, uE, rhoE_uE;      /* freestream condition */
    Ffloat     theta;                  /* boundary layer thickness */
    Ffloat     theta_1;                /* displacement layer thickness */
    Ffloat     theta_2;                /* momentum layer thickness     */
    Ffloat     H_12;                   /* form parameter H_12 = theta_1/theta_2 */
    Ffloat     cF;                     /* skin friction */
    Ffloat     leadingEdge, trailingEdge, chordLength;
    Ffloat     yPlus, yPlusWF, machIs, eddyMax;
    Ffloat     NuX, Heat_Coeff;        /* Heat-Transfer Values*/
    Ffloat     linIntF;                /* factor for linear interpolation of boundary layer edge*/
}
TmTransition;

typedef enum
{
    ONE_EQUATION,
    TWO_EQUATION
}
TmModelType;

typedef enum
{
    TECPLOT,
    NORWAY,
    TRACE,
    TRACE_S,
    TRACE_GRID,
    ASCII,
    ENSIGHT,
    TDSR,
    FOURIER,
    FOURIER_CGNS,
    CGNS,
    CGNS_COMPLETE,
    CGNS_2D,
    MTU
}
FDataType;

typedef enum
{
    GLOBAL_PERFORMANCE,
    GLOBAL_EFFICIENCY,
    GLOBAL_STAGE_DATA,
    FOURIER_FIELD,
    FOURIER_SPECTRUM,
    AVERAGE,
    AVERAGE_GLOBAL,
    FLUCTUATION,
    AVERAGE_RMS,
    SHOW_GRID_SIZE,
    TRANSFORM_FIELD,
    CHECK_JACOBIANS,
    MAXIMUM_VALUES,
    BALANCE_GRID,
    OPERATE,
    EXTRACT_DATA,
    CHECK4CORRUPT,
    BD_PERFORMANCE,
    DEF,
    FFT_TIME_SPACE,
    FULL_SPECTRUM,
    REL2ABS,
    FREEZE_ROTOR,
    LAMBDA2,
    QCRITERION,
    DETERMINISTIC_STRESSES,
    BUILD_SURF_DATA
}
FPostType;

typedef struct
{
    int planes_j, planes_k, planes_l;
    Ffloat planes_z;
    Ffloat  xEntry, xExit, radius;
}
FPostData;

typedef struct
{
    char        finput[MAX_CHARACTER];
    char        foutput[MAX_CHARACTER];
    char        path_name[MAX_CHARACTER];
    char        av_fname[MAX_CHARACTER];
    char        operate_fname[MAX_CHARACTER];
    char        global_fname[MAX_CHARACTER];
    char        blow_fname[MAX_CHARACTER];
    int         duplicate;
    TmModelType	turbulence_model;
    FDataType   input_format, output_format;
    FDataType   panel_input_format;
    FPostType   post;
    FPostData   postData;
    Fint        current_fourier_harmonic;
    Ffloat      current_fourier_time;
    FfourierOutputMode fourier_output;
    Fsimulation mode;
    Fint        NoOutput4UserDefinedPanels;
}
Fcontrol;



typedef struct
{
    float       variables[20][MAX_CHARACTER];
    float       color,repeat,zone[6];
    float       numvar, version, zero;
    float       marker1, marker2, format;
}
Ftecplot;


/*
typedef struct
{
    float       variables[20][MAX_CHARACTER];
    float       color,repeat,*zone;
    float       numvar, version, zero;
    float       marker1, marker2, format;
}
FtecplotU;
*/

typedef struct
{
    Fblock      *block;
    RefVals     ref;
    BdVal       entry, exit;
    Fcontrol    control;
    Ftecplot    tec;
    int         dimension;
    int         nBlocks;
    int         nStages;
    int         nBladeRows;
    Ffloat      pitch; 
    int         segments;
    int         num_of_processors;
    int         num_of_records;
    char        fft_variable[MAX_CHARACTER];
    FindexFFT   indexFFT;
    FaverageType    average;
    FreferenceFrame referenceFrame;
    FmachineType machineType;
    BdPerformance bdperformance;
    Fturbulence     tm;
    BdStageVal   stageVal[MAX_STAGES];

    /* vars for acoustic matching */
    Fint        fft_jpos,fft_block,fft_nrad;
    Ffloat      fft_axial_pos;
    Fint        n_circ,n_time;
    FmeshMode   meshMode;
} 
Fglobal;



typedef struct
{
    Fint noOfNodes;
    Fint noOfBlocks;
    Fint blockNumber[MAX_BLOCKS];
}
Fproc;

typedef struct
{
    Fint noOfNodes;
    Fint blkId;
    Fint nodeJdim, nodeKdim, nodeLdim;
    Fnode *adress;
}
Fblk;

typedef struct
{
    Ffloat x, y, z;
}
FdefgeomVal;

typedef struct
{
    FdefgeomVal   geom;
    FdefgeomVal   phi;
    Ffloat        weight;
}
Fdefnode;

typedef struct
{
    int          nNodes;
    int          nodeJdim, nodeKdim, nodeLdim;
    Fdefnode     *pNode;
}
Fdefblock;

typedef struct
{
    int         nBlocks;
    Fdefblock   *block;    
}
Fdefglobal;


Fglobal  global;





#define  NBLOCKS      global.nBlocks
#define  NODE_JDIM    global.block[blk].nodeJdim
#define  NODE_KDIM    global.block[blk].nodeKdim
#define  NODE_LDIM    global.block[blk].nodeLdim
#define  TOTAL_DIM    NODE_JDIM * NODE_KDIM * NODE_LDIM

#define  TOPOLOGY      global.block[blk].topology
#define  P_CONNECT     global.block[blk].pConnect
#define  CELL_DIM      global.block[blk].cellDim
#define  ZONE_NAME_1ST global.block[blk].zoneName
#define  NUM_PANELS    global.block[blk].num_interiorPanels
#define  INT_PANELS    global.block[blk].interiorPanel
#define  INT_PANELS_D  global.block[blkDonor].interiorPanel
#define  P_NODE_D      global.block[blkDonor].pNode

#define  P_NODE       global.block[blk].pNode
#define  STAGE        global.stageVal
#define  TEC          global.tec

#define	 TURB_MODEL   global.control.turbulence_model

#define  DUPLICATE    global.control.duplicate
#define  DIMENSION    global.dimension
#define  IN_FNAME     global.control.finput
#define  OUT_FNAME    global.control.foutput
#define  PATH_FNAME   global.control.path_name
#define  AV_FNAME     global.control.av_fname

#define  IN_FORMAT    global.control.input_format
#define  OUT_FORMAT   global.control.output_format

#define  PITCH        global.pitch
#define  STAGE_ID     global.block[blk].stageId
#define  SEGMENTS     global.segments
#define  SEGMENTS_BLK global.block[blk].Segments

#define  TIME_RECORDS global.num_of_records
#define  FFT_VARIABLE global.fft_variable
#define  PROCESSORS   global.num_of_processors

#define  AV_TYPE      global.average
#define  REF_FRAME    global.referenceFrame
#define  MESH_DEFAULT global.meshMode

/**********************************************************/

#define  RPM          global.ref.rpm
#define  P_TOTAL      global.ref.p_total
#define  T_TOTAL      global.ref.t_total
#define  GAMMA        global.ref.gamma
#define  GAMMA_1      (GAMMA - 1.)
#define  R_GAS        global.ref.r_gas
#define  L_NORM       global.ref.length
#define  C_P                (GAMMA * R_GAS / (GAMMA - 1.0))
#define  SUTHERLAND_CONST   (110.0/T_TOTAL)  
#define  RE           global.ref.reynolds_no
#define  A_PLUS       26.

/**********************************************************/

#define  RPM          global.ref.rpm
#define  R_INNER      global.ref.r_inner
#define  R_OUTER      global.ref.r_outer

#define  RPM_REF       global.ref.rpm_ref
#define  SEG_REF       global.ref.segments_ref
#define  FREQ_REF      global.ref.freq_ref
#define  TIMESTEPS_REF global.ref.timeSteps_ref

#define  F_NODE      (global.block[blk].fNode)
#define  ANNULUS     (global.block[blk].annulus)

#define  PITCH_BLK    global.block[blk].pitch
#define  OMEGA        global.block[blk].omega
#define  TIME_STEPS   global.block[blk].global_fourier.timeSteps
#define  GLOBAL_TIMESTEPS global.block[blk].global_fourier.globalTimeSteps
#define  DELTA_T      global.block[blk].global_fourier.delta_t
#define  PHI0         global.block[blk].global_fourier.phi0
#define  TIME_LAG     global.block[blk].global_fourier.timeShift
#define  TIME_SCALE   global.block[blk].global_fourier.timeScale
#define  TIMESTEPS_BPF        global.block[blk].global_fourier.timeStepsBPF
#define  FOURIER_HARMONICS    global.block[blk].global_fourier.noOfHarmonics
#define  MODE                 global.block[blk].global_fourier.mode
#define  HARMONIC             global.control.current_fourier_harmonic
#define  TIME                 global.control.current_fourier_time
#define  FOURIER_OUTPUT       global.control.fourier_output

#define  FFT_JPOS     global.fft_jpos
#define  FFT_BLOCK    global.fft_block
#define  FFT_XPOS     global.fft_axial_pos
#define  FFT_NRAD     global.fft_nrad

#define  N_CIRC       10 /* number of spatial harmonics */
#define  N_TIME       7 /* number of omega-harmonics */

#define  MUE          1.822517E-05





//CEO/**********************************************************/

//CEO#define  SERIOUS(function, message)      passMessage( function, message);

//CEO/**********************************************************/
















/**********************************************************
 *
 */
Fnode    rel2abs4Data(Fnode rel, BdVal *bd)
{
    Fnode abs = rel;

    Ffloat dy_dt = bd->dy_dt;

    if (DIMENSION == 2) {

//CEO	if ((dy_dt < -100.0) || (dy_dt > 100)) SERIOUS("rel2abs4Data","... specify correct dyDt ...");
    
	abs.phys.v = rel.phys.v + dy_dt;
	abs.phys.w = 0.0;
	abs.phys.M = sqrt( (sqr(abs.phys.u) + sqr(abs.phys.v)) /
			   ( GAMMA * abs.phys.p / abs.phys.rho));	
    }


    else if (DIMENSION == 3) {

	abs.phys.v     = rel.phys.v - rel.geom.z * bd->omega;
	abs.phys.w     = rel.phys.w + rel.geom.y * bd->omega;

	abs.phys.M     = sqrt( (sqr(abs.phys.u) + sqr(abs.phys.v) + sqr(abs.phys.w)) / 
			       ( GAMMA * abs.phys.p / abs.phys.rho));
    }

//CEO    else  SERIOUS("rel2abs4Data"," ... Wrong dimensions for DataSet ...\n");

    return(abs);
}
/**********************************************************
 *
 */
 
 
Fnode    average4Data(int blk, int index0, int index1, int index2, int index3)
{
    Fnode av;
    
    av.geom.x   = 0.25 * (P_NODE[index0].geom.x + P_NODE[index1].geom.x + 
			  P_NODE[index2].geom.x + P_NODE[index3].geom.x);
    av.geom.y   = 0.25 * (P_NODE[index0].geom.y + P_NODE[index1].geom.y + 
			  P_NODE[index2].geom.y + P_NODE[index3].geom.y);
    av.geom.z   = 0.25 * (P_NODE[index0].geom.z + P_NODE[index1].geom.z + 
			  P_NODE[index2].geom.z + P_NODE[index3].geom.z);
    av.geom.radius = sqrt( sqr(av.geom.y) + sqr(av.geom.z));

    av.phys.rho = 0.25 * (P_NODE[index0].phys.rho + P_NODE[index1].phys.rho + 
			  P_NODE[index2].phys.rho + P_NODE[index3].phys.rho);
    av.phys.u   = 0.25 * (P_NODE[index0].phys.u + P_NODE[index1].phys.u + 
			  P_NODE[index2].phys.u + P_NODE[index3].phys.u);
    av.phys.v   = 0.25 * (P_NODE[index0].phys.v + P_NODE[index1].phys.v + 
			  P_NODE[index2].phys.v + P_NODE[index3].phys.v);
    av.phys.w   = 0.25 * (P_NODE[index0].phys.w + P_NODE[index1].phys.w + 
			  P_NODE[index2].phys.w + P_NODE[index3].phys.w);
    av.phys.p   = 0.25 * (P_NODE[index0].phys.p + P_NODE[index1].phys.p + 
			  P_NODE[index2].phys.p + P_NODE[index3].phys.p);
    av.phys.M   = 0.25 * (P_NODE[index0].phys.M + P_NODE[index1].phys.M + 
			  P_NODE[index2].phys.M + P_NODE[index3].phys.M);
    av.phys.eddyVisc   = 0.25 * (P_NODE[index0].phys.eddyVisc + P_NODE[index1].phys.eddyVisc + 
				 P_NODE[index2].phys.eddyVisc + P_NODE[index3].phys.eddyVisc);    

    return (av);
}
/**********************************************************
 *
 */
FgeomVal  calc4Area(int blk, int index, int indexKp1, int indexLp1, int indexKp1Lp1)
{
    FgeomVal    A;

    /* calc areas ... */
    if (DIMENSION == 2) {

	A.x = P_NODE[indexKp1].geom.y - P_NODE[index].geom.y;
	A.y = P_NODE[index].geom.x - P_NODE[indexKp1].geom.x;
	A.z = 0.;
	A.radius = 0.;
    }

    else if (DIMENSION == 3){
	
	FgeomVal    diag_1, diag_2;
	
        Ffloat      x_av = 0.25 * (P_NODE[index].geom.x +
                                   P_NODE[indexKp1].geom.x +
                                   P_NODE[indexLp1].geom.x +
                                   P_NODE[indexKp1Lp1].geom.x);

	Ffloat      y_av = 0.25 * (P_NODE[index].geom.y +
				   P_NODE[indexKp1].geom.y +
				   P_NODE[indexLp1].geom.y +
				   P_NODE[indexKp1Lp1].geom.y);
				  
	Ffloat      z_av = 0.25 * (P_NODE[index].geom.z +
				   P_NODE[indexKp1].geom.z +
				   P_NODE[indexLp1].geom.z +
				   P_NODE[indexKp1Lp1].geom.z);
	
	diag_1.x = P_NODE[indexKp1Lp1].geom.x - P_NODE[index].geom.x;
	diag_1.y = P_NODE[indexKp1Lp1].geom.y - P_NODE[index].geom.y;
	diag_1.z = P_NODE[indexKp1Lp1].geom.z - P_NODE[index].geom.z;

	diag_2.x = P_NODE[indexLp1].geom.x - P_NODE[indexKp1].geom.x;
	diag_2.y = P_NODE[indexLp1].geom.y - P_NODE[indexKp1].geom.y;
	diag_2.z = P_NODE[indexLp1].geom.z - P_NODE[indexKp1].geom.z;

	A.x = 0.5 * (diag_1.y*diag_2.z - diag_2.y*diag_1.z);
	A.y = 0.5 * (diag_1.z*diag_2.x - diag_2.z*diag_1.x);
	A.z = 0.5 * (diag_1.x*diag_2.y - diag_2.x*diag_1.y);
	A.radius = sqrt( sqr(y_av) + sqr(z_av));

        A.x_av = x_av;
        A.y_av = y_av;
        A.z_av = z_av;

	/* switch to Cartesian System */
	if (REF_FRAME == cartesian)
	    A.radius = z_av;
    }


//CEO    else SERIOUS("getStageId"," ... Wrong dimensions for DataSet ...\n");

    A.A_norm = sqrt(sqr(A.x) + sqr(A.y) + sqr(A.z));
    return  (A);
}


void  add2Flux(Fnode av, FgeomVal A, FfluxVal *F)
{

    Ffloat  vX, vPhi, vR, HLocal, cosPhi, sinPhi;
    Ffloat  a_x, a_y, a_z, a_r, a_phi, rho_vn, ASum;


    a_x = A.x;
    a_y = A.y;
    a_z = A.z;

    if (DIMENSION == 2) {

	vX   = av.phys.u;
	vPhi = av.phys.v;
	vR   = 0.0;

	cosPhi = 1.0;
	sinPhi = 0.0;

	a_x   = A.x;
	a_r   = 0.0;
	a_phi = A.y;
	ASum   = sqrt( sqr(a_x) + sqr(a_phi) + sqr(a_r));
    }

    else if (DIMENSION == 3){

	/* switch to Cartesian System */
	if (REF_FRAME == rotational) {

	    cosPhi = av.geom.z / av.geom.radius;
	    sinPhi = av.geom.y / av.geom.radius;

	}
	else {

	    cosPhi = 1.0;
	    sinPhi = 0.0;
	}

	vX   = av.phys.u;
	vPhi = av.phys.v * cosPhi - av.phys.w * sinPhi;
	vR   = av.phys.w * cosPhi + av.phys.v * sinPhi;

	a_x   = a_x;
	a_r   = a_z * cosPhi + a_y * sinPhi;
	a_phi = a_y * cosPhi - a_z * sinPhi;
	ASum  = sqrt( sqr(a_x) + sqr(a_phi) + sqr(a_r));
    }
	    
//CEO    else SERIOUS("getStageId"," ... Wrong dimensions for DataSet ...\n");


    rho_vn = av.phys.rho * (a_x * vX + a_phi * vPhi + a_r * vR);

    HLocal = (GAMMA/GAMMA_1 * av.phys.p/av.phys.rho + 0.5 * (sqr(av.phys.u) + sqr(vPhi) + sqr(vR)));

    F->F1 += (rho_vn);
    F->F2 += (rho_vn * vX   + a_x   * av.phys.p);
    F->F3 += (rho_vn * vPhi + a_phi * av.phys.p);
    F->F4 += (rho_vn * vR   + a_r   * av.phys.p);
    F->F5 += (rho_vn * HLocal);

    F->Sum += ASum;
    F->H   += (HLocal * ASum);

    F->a_x   += a_x;
    F->a_y   += a_y;
    F->a_z   += a_z;
    F->a_phi += a_phi;
    F->a_r   += a_r;
}


FphysVal  Flux2Phys(FfluxVal Flux)
{

    FphysVal  FluxAv;
    Ffloat    INV_GAMMA_P_1 = 1.0/(GAMMA + 1.0);
    Ffloat    abbreviation, FN;

    Flux.Sum = sqrt( sqr(Flux.a_x) + sqr(Flux.a_phi) + sqr(Flux.a_r));

    Flux.F1 /= Flux.Sum;
    Flux.F2 /= Flux.Sum;
    Flux.F3 /= Flux.Sum;
    Flux.F4 /= Flux.Sum;
    Flux.F5 /= Flux.Sum;
    Flux.H  /= Flux.Sum;

    Flux.a_x /= Flux.Sum;
    Flux.a_y /= Flux.Sum;
    Flux.a_z /= Flux.Sum;
    Flux.a_r /= Flux.Sum;
    Flux.a_phi /= Flux.Sum;

    FN = Flux.a_x * Flux.F2 + Flux.a_phi * Flux.F3 + Flux.a_r * Flux.F4;

    abbreviation = sqr(FN) + (GAMMA*GAMMA-1) * 
	(sqr(Flux.F2) + sqr(Flux.F3) + sqr(Flux.F4) - 2.0 * Flux.F1 * Flux.F5);

    FluxAv.p   = INV_GAMMA_P_1 * (FN + sqrt(abbreviation));
    FluxAv.u   = (Flux.F2 - Flux.a_x * FluxAv.p) / Flux.F1;
    FluxAv.v   = (Flux.F3 - Flux.a_phi * FluxAv.p) / Flux.F1;
    FluxAv.w   = (Flux.F4 - Flux.a_r * FluxAv.p) / Flux.F1;
    FluxAv.rho = Flux.F1 / (Flux.a_x * FluxAv.u + Flux.a_phi * FluxAv.v + Flux.a_r * FluxAv.w);

    FluxAv.M   = sqrt(( sqr(FluxAv.u) + sqr(FluxAv.v) + sqr(FluxAv.w)) /
		      ( GAMMA * FluxAv.p / FluxAv.rho));

    return(FluxAv);
}



void     calcStageData(BdVal *bd, char *fileName, Fint stage, char *mode, BdVal *radVal)
{
    Fmsg        msg;
    int         blk = bd->block_number;
    Fnode       av_rel, av_abs;
    int         j, k, l, index, indexKp1, indexLp1, indexKp1Lp1;
    float       localMass, sumMass = 0.0, t_av, globalArea = 0., xLocation = 0.0;
    FgeomVal    Area;
    FfluxVal    FluxAbs, FluxRel;

    int         endPtK      = NODE_KDIM - 1;
    int         endPtL      = NODE_LDIM - 1;

    
    if      (DIMENSION == 2) {
//CEO	SERIOUS("","");
	endPtL = NODE_LDIM;
    }

    if      (bd->side == LEFT)  j = 0;
    else if (bd->side == RIGHT) j = NODE_JDIM - 1;
//CEO    else    SERIOUS("calcStageData", "Side Not Known");


    /* initialize... */
    bd->p_total_abs = 0.;
    bd->p_total_rel = 0.;
    bd->t_total_abs = 0.;
    bd->t_total_rel = 0.;
    bd->t_total_rot = 0.;
    bd->p_static    = 0.;
    bd->p_static_area = 0.;
    bd->t_static    = 0.;
    bd->massflow    = 0.;
    bd->mach_rel    = 0.;
    bd->mach_abs    = 0.;
    bd->alpha_rel   = 0.;
    bd->alpha_abs   = 0.;
    bd->beta_rel    = 0.;
    bd->beta_abs    = 0.;
    bd->rot_energy  = 0.;

    initFlux(&FluxAbs);
    initFlux(&FluxRel);

    for ( l = 0; l < endPtL; l++)
	for ( k = 0; k < endPtK; k++){

	    index       = l * NODE_JDIM * NODE_KDIM + k * NODE_JDIM + j;
	    indexKp1    = l * NODE_JDIM * NODE_KDIM + (k+1) * NODE_JDIM + j;
	    indexLp1    = (l+1) * NODE_JDIM * NODE_KDIM + k * NODE_JDIM + j;
	    indexKp1Lp1 = (l+1) * NODE_JDIM * NODE_KDIM + (k+1) * NODE_JDIM + j;

	    
	    if (DIMENSION == 2) {
		
		indexLp1    = index;
		indexKp1Lp1 = indexKp1;
	    }
//CEO#if 0   /* input data in absolute frame */
//CEO	    av_abs        = average4Data(blk, index, indexKp1, indexLp1, indexKp1Lp1);
//CEO	    av_rel        = abs2rel4Data(av_abs, bd);
//CEO#else   /* input data in relative frame */
	    av_rel        = average4Data(blk, index, indexKp1, indexLp1, indexKp1Lp1);
   	    av_abs        = rel2abs4Data(av_rel, bd);
//CEO#endif
	    Area          = calc4Area( blk, index, indexKp1, indexLp1, indexKp1Lp1);
	    localMass     = av_abs.phys.rho * (av_abs.phys.u * Area.x + 
					       av_abs.phys.v * Area.y + 
					       av_abs.phys.w * Area.z);	    
	    xLocation    += av_rel.geom.x;


	    av_rel.phys.M = sqrt( (sqr(av_rel.phys.u) + sqr(av_rel.phys.v) + sqr(av_rel.phys.w)) /
			   ( GAMMA * av_rel.phys.p / av_rel.phys.rho));

	    
	    /* calc averaged fluxes */
	    add2Flux( av_abs, Area, &FluxAbs);	    
	    add2Flux( av_rel, Area, &FluxRel);	    

	    /* add global values to passage averaged values [not uesed for flux averaging] */
	    t_av  = GAMMA * av_rel.phys.p / av_rel.phys.rho;
	    
	    bd->p_static    += (localMass * av_rel.phys.p);
	    bd->p_static_area += (Area.x * av_rel.phys.p);
	    bd->t_static    += (localMass * t_av);
	    bd->massflow    += localMass;

	    bd->p_total_rel += (localMass * av_rel.phys.p * pow((1. + 0.5*GAMMA_1*sqr(av_rel.phys.M)),(GAMMA/GAMMA_1)));
	    bd->p_total_abs += (localMass * av_rel.phys.p * pow((1. + 0.5*GAMMA_1*sqr(av_abs.phys.M)),(GAMMA/GAMMA_1)));

	    bd->t_total_rel += (localMass * t_av * (1. + 0.5*GAMMA_1*sqr(av_rel.phys.M)));
	    bd->t_total_abs += (localMass * t_av * (1. + 0.5*GAMMA_1*sqr(av_abs.phys.M)));
	    bd->t_total_rot += (localMass * t_av * (1. + 0.5*GAMMA_1*((sqr(av_rel.phys.u) + 
								       sqr(av_rel.phys.v) + 
								       sqr(av_rel.phys.w) - 
								       sqr(bd->omega * av_rel.geom.radius)) / t_av)));

	    bd->mach_rel    += (localMass * av_rel.phys.M);
	    bd->mach_abs    += (localMass * av_abs.phys.M);

	    bd->alpha_rel   += (localMass * atan(av_rel.phys.v / av_rel.phys.u) * 180. / PI);
	    bd->alpha_abs   += (localMass * atan(av_abs.phys.v / av_abs.phys.u) * 180. / PI);
	    bd->beta_rel    += (localMass * atan(av_rel.phys.w / av_rel.phys.u) * 180. / PI);
	    bd->beta_abs    += (localMass * atan(av_abs.phys.w / av_abs.phys.u) * 180. / PI);

	    bd->rot_energy  += (Area.x * sqr(bd->omega * Area.radius));
	    globalArea      += Area.x;
	}
    

    if (AV_TYPE == CONSERVATIVE) { /**** FLUX AVERAGING ****/
 
	FphysVal  PhysAbs, PhysRel;
	
	PhysAbs = Flux2Phys(FluxAbs);
	PhysRel = Flux2Phys(FluxRel);

	/* static values */
	bd->p_static = PhysAbs.p;
	bd->p_static_area /= globalArea;
	bd->t_static = GAMMA * PhysAbs.p / PhysAbs.rho;

	/* total values */
	bd->t_total_abs = bd->t_static * (1. + 0.5*GAMMA_1*sqr(PhysAbs.M));
	bd->p_total_abs = bd->p_static * pow((1. + 0.5*GAMMA_1*sqr(PhysAbs.M)),(GAMMA/GAMMA_1));
	bd->mach_abs    = PhysAbs.M;

	bd->rot_energy /= globalArea;
	bd->t_total_rel = bd->t_static * (1. + 0.5*GAMMA_1*sqr(PhysRel.M));
	bd->p_total_rel = bd->p_static * pow((1. + 0.5*GAMMA_1*sqr(PhysRel.M)),(GAMMA/GAMMA_1));
	bd->t_total_rot = bd->t_static * (1. + 0.5 * GAMMA_1 * ((sqr(PhysRel.u) + 
								 sqr(PhysRel.v) +
								 sqr(PhysRel.w) - bd->rot_energy) / bd->t_static));
	bd->mach_rel    = PhysRel.M;

	/* angle values */
	bd->alpha_abs   = atan(PhysAbs.v / PhysAbs.u) * 180. / PI;
	bd->alpha_rel   = atan(PhysRel.v / PhysRel.u) * 180. / PI;
	bd->beta_abs    = atan(PhysAbs.w / PhysAbs.u) * 180. / PI;
	bd->beta_rel    = atan(PhysRel.w / PhysRel.u) * 180. / PI;

	/* massflow */
/* 	bd->massflow    = PhysAbs.rho * PhysAbs.u * FluxAbs.Sum; */
	xLocation       /= ( (Ffloat) (NODE_KDIM - 1) * (NODE_LDIM - 1));

	printf("        flux-averaging \n");
    }
    
    else if (AV_TYPE == ENSEMBLE) { /**** PASSAGE AVERAGING ****/
    
	bd->p_total_abs /= bd->massflow;
	bd->p_total_rel /= bd->massflow;
	bd->t_total_abs /= bd->massflow;
	bd->t_total_rel /= bd->massflow;
	bd->t_total_rot /= bd->massflow;
	bd->p_static    /= bd->massflow;
	bd->p_static_area /= globalArea;
	bd->t_static    /= bd->massflow;
	bd->mach_rel    /= bd->massflow;
	bd->mach_abs    /= bd->massflow;
	bd->alpha_rel   /= bd->massflow;
	bd->alpha_abs   /= bd->massflow;
	bd->beta_rel    /= bd->massflow;
	bd->beta_abs    /= bd->massflow;
	bd->rot_energy  /= bd->massflow;
	xLocation       /= ( (Ffloat) (NODE_KDIM - 1) * (NODE_LDIM - 1));
	printf("        passage-averaging \n");
    }

//CEO    else  SERIOUS("calcStageData","no AverageType specified");


    /* add dimensions */
    if (DIMENSION == 3) {

	bd->massflow *= (global.ref.rho_0 * global.ref.a_0 * sqr(global.ref.length));
	bd->massflow *= (Ffloat) calcSegments(bd);
    }

/*CEO
#if 0
    printf("[BLOCK %d] : Pt_abs = %g,  Pt_rel = %g, Tt_abs = %g,  Tt_rel = %g, Tt_rot = %g\n",blk,
	   bd->p_total_abs*GAMMA*P_TOTAL,bd->p_total_rel*GAMMA*P_TOTAL,
	   bd->t_total_abs*T_TOTAL,bd->t_total_rel*T_TOTAL,bd->t_total_rot*T_TOTAL);

    printf("\t    P_stat = %g, P_stat_area = %g, T_stat = %g\n",
	   bd->p_static*GAMMA*P_TOTAL, bd->p_static_area*GAMMA*P_TOTAL, bd->t_static*T_TOTAL);

    printf("\t    M_abs = %g,  M_rel = %g, Alpha_abs = %g,  Beta_abs = %g\n\n",
	   bd->mach_abs, bd->mach_rel, bd->alpha_abs, bd->beta_abs);

    printf("\t    Massflow = %g\n\n",bd->massflow);
    printf("segm %g, lref = %g, a_0 = %g, rho_0 = %g, omega = %g\n", 
	   (Ffloat) calcSegments(bd), global.ref.length, global.ref.a_0, global.ref.rho_0, bd->omega);

#endif
CEO*/
    /* Radial Distribution */
    {
	FILE        *fp;
	Fint        indexHub = 0 * NODE_JDIM * NODE_KDIM + 0 * NODE_JDIM + j;
	Fint        indexTip = (NODE_LDIM - 1) * NODE_JDIM * NODE_KDIM + 0 * NODE_JDIM + j;
        Ffloat      xHub      = P_NODE[indexHub].geom.x;
        Ffloat      xTip      = P_NODE[indexTip].geom.x;
	Ffloat      radiusHub = sqrt(sqr(P_NODE[indexHub].geom.y) +
				     sqr(P_NODE[indexHub].geom.z));
	Ffloat      radiusTip = sqrt(sqr(P_NODE[indexTip].geom.y) +
				     sqr(P_NODE[indexTip].geom.z));

//CEO	if (endPtL > MAX_RADIAL_DIM)
//CEO	    SERIOUS("calcStageData", " Please increase No. of RADIAL Points in post.h.");
	
	
	/* switch to Cartesian System */
	if (REF_FRAME == cartesian) {

	    radiusHub = P_NODE[indexHub].geom.z;
	    radiusTip = P_NODE[indexTip].geom.z;
	}

	/* open tecplot file */
	if ((fp = fopen(fileName, mode)) == NULL) {
	    
	    sprintf(msg, "Cannot open Radial Distribution File [%s].",fileName);
//CEO	    SERIOUS("calcStageData", msg);
	}
	
	/* write Tecplot-Header 4 radial.dat */
	if (bd->side == LEFT) {

	    fprintf( fp,"\nZONE T=\"Entry Row %d - Block= %d\", F=POINT\n\n", stage, blk);
            fprintf( fp,"#....r/H.......m/M......pt_abs.....pt_rel.....Tt_abs....Tt_rel.....M_abs....M_rel......Vax.......Vrad....Vt_abs....Vt_rel.....alfa_abs...alfa_rel.....beta.....p.......rho.......muet..\n");

	}
	else if (bd->side == RIGHT) {

	    fprintf( fp,"\nZONE T=\"Exit Row %d - Block= %d\", F=POINT\n\n", stage, blk);
            fprintf( fp,"#....r/H.......m/M......pt_abs.....pt_rel.....Tt_abs....Tt_rel.....M_abs....M_rel......Vax.......Vrad....Vt_abs....Vt_rel.....alfa_abs...alfa_rel.....beta......p.......rho.......muet..\n");

	}
//CEO	else
//CEO		SERIOUS("calcStageData", "Side Not Known");
	

	for ( l = 0; l < endPtL; l++){ 
          // BUG ICI
	    Fint   indexRadius = l * NODE_JDIM * NODE_KDIM + 0 * NODE_JDIM + j;
	    Fint   indexRadiusLp1 = (l+1) * NODE_JDIM * NODE_KDIM + 0 * NODE_JDIM + j;	    
 
	    Ffloat tanPressure, tanTemperature, tanDensity, tanMachAbs, tanMachRel, tanPressureA = 0, AreaA = 0;
	    Ffloat tanTTAbs, tanTTRel, tanPTRel, tanPTAbs, tanTTRot, globalArea = 0, rot_energy = 0;
	    Ffloat tanAlphaRel, tanAlphaAbs, tanBetaRel, tanBetaAbs, radius, tanMass = 0.0, tanEddy = 0.0;
	    Ffloat eta_is, radiusReal;
	    Ffloat tanV_rel, tanV_rel_tan, tanV_rel_mer, tanV_rel_rad, tanV_rel_ax;
	    Ffloat tanV_abs, tanV_abs_tan, tanV_abs_mer, tanV_abs_rad, tanV_abs_ax;
	    FphysVal  PhysAbs, PhysRel;

	    initFlux(&FluxAbs);
	    initFlux(&FluxRel);
	    
	    for ( k = 0; k < endPtK; k++){
		
		index       = l * NODE_JDIM * NODE_KDIM + k * NODE_JDIM + j;
		indexKp1    = l * NODE_JDIM * NODE_KDIM + (k+1) * NODE_JDIM + j;
		indexLp1    = (l+1) * NODE_JDIM * NODE_KDIM + k * NODE_JDIM + j;
		indexKp1Lp1 = (l+1) * NODE_JDIM * NODE_KDIM + (k+1) * NODE_JDIM + j;

		/* input data in relative frame */
		av_rel        = average4Data(blk, index, indexKp1, indexLp1, indexKp1Lp1);
		av_abs        = rel2abs4Data(av_rel, bd);

		Area          = calc4Area( blk, index, indexKp1, indexLp1, indexKp1Lp1);
		localMass     = av_abs.phys.rho * (av_abs.phys.u * Area.x + 
						   av_abs.phys.v * Area.y + 
						   av_abs.phys.w * Area.z);	    

		av_rel.phys.M = sqrt( (sqr(av_rel.phys.u) + sqr(av_rel.phys.v) + sqr(av_rel.phys.w)) /
				      ( GAMMA * av_rel.phys.p / av_rel.phys.rho));
		
		/* calc averaged fluxes */
		add2Flux( av_abs, Area, &FluxAbs);	    
		add2Flux( av_rel, Area, &FluxRel);	    

		tanPressureA    += (av_abs.phys.p * Area.A_norm);
		AreaA           += Area.A_norm;
		tanMass         += localMass;
		tanEddy         += (Area.A_norm * av_abs.phys.eddyVisc);

                rot_energy      += (Area.x * sqr(bd->omega * Area.radius));
                globalArea      += Area.x;
	    } /* for (k) */

	    PhysAbs = Flux2Phys(FluxAbs);
	    PhysRel = Flux2Phys(FluxRel);
	
	    /* static values */
	    tanPressure    = PhysAbs.p * GAMMA * P_TOTAL;
	    tanPressureA   = tanPressureA / AreaA * GAMMA * P_TOTAL;
	    tanTemperature = GAMMA * PhysAbs.p / PhysAbs.rho * T_TOTAL;
	    tanDensity     = PhysAbs.rho * global.ref.rho_0;
	    tanMachAbs     = PhysAbs.M;
	    tanMachRel     = PhysRel.M;
	    
	    /* total values */
            rot_energy /= globalArea;
	    tanTTRel = tanTemperature * (1. + 0.5*GAMMA_1 * sqr(tanMachRel));
	    tanTTAbs = tanTemperature * (1. + 0.5*GAMMA_1 * sqr(tanMachAbs));
	    tanPTRel = tanPressure * pow((1. + 0.5*GAMMA_1 * sqr(tanMachRel)), (GAMMA/GAMMA_1));
	    tanPTAbs = tanPressure * pow((1. + 0.5*GAMMA_1 * sqr(tanMachAbs)), (GAMMA/GAMMA_1));
            tanTTRot = tanTemperature / T_TOTAL * (1. + 0.5*GAMMA_1 * ((sqr(PhysRel.u) +
                                                                        sqr(PhysRel.v) +
                                                                        sqr(PhysRel.w) - rot_energy) / tanTemperature * T_TOTAL));
	    
            /* new relative channel height calculated by x, r */
	    radius   = sqrt((sqr(Area.x_av - xHub) + sqr(Area.radius - radiusHub)) /
                            (sqr(xTip - xHub) + sqr(radiusTip - radiusHub)));

            radiusReal = global.ref.length * Area.radius;

	    tanMass *= (global.ref.rho_0 * global.ref.a_0 * sqr(global.ref.length) * ((Ffloat) calcSegments(bd)));
	    tanEddy /= AreaA;
	    sumMass += tanMass;

            /* velocities and angles */
            tanV_rel     = sqrt(sqr(PhysRel.u) + sqr(PhysRel.v) + sqr(PhysRel.w) );
            tanV_rel_ax  = PhysRel.u;
            tanV_rel_tan = PhysRel.v;
            tanV_rel_rad = PhysRel.w;
            tanV_rel_mer = sqrt(sqr(PhysRel.u) + sqr(PhysRel.w));

	    
	    if (REF_FRAME == rotational) {
		tanAlphaRel  = atan(tanV_rel_tan / tanV_rel_mer) * 180. / PI;
		tanBetaRel   = asin(tanV_rel_rad / tanV_rel_mer) * 180. / PI;
	    }
	    else {
		tanAlphaRel = atan(PhysRel.v / PhysRel.u) * 180. / PI; 
		tanBetaRel  = atan(PhysRel.w / PhysRel.u) * 180. / PI; 
	    }


            tanV_abs     = sqrt(sqr(PhysAbs.u) + sqr(PhysAbs.v) + sqr(PhysAbs.w) );
            tanV_abs_ax  = PhysAbs.u;
            tanV_abs_rad = PhysAbs.w;
            tanV_abs_tan = PhysAbs.v;
            tanV_abs_mer = sqrt(sqr(PhysAbs.u) + sqr(PhysAbs.w));

	    if (REF_FRAME == rotational) {
		tanAlphaAbs  = atan(tanV_abs_tan / tanV_abs_mer) * 180. / PI;
		tanBetaAbs   = asin(tanV_abs_rad / tanV_abs_mer) * 180. / PI;
	    }
	    else {
		tanAlphaAbs = atan(PhysAbs.v / PhysAbs.u) * 180. / PI; 
		tanBetaAbs  = atan(PhysAbs.w / PhysAbs.u) * 180. / PI; 
	    }

	    eta_is = 100.;

	    radVal[l].radius      = radius;
	    radVal[l].radiusReal  = radiusReal;
	    radVal[l].p_total_abs = tanPTAbs;
	    radVal[l].p_total_rel = tanPTRel;
	    radVal[l].t_total_abs = tanTTAbs;
	    radVal[l].t_total_rel = tanTTRel;
            radVal[l].t_total_rot = tanTTRot;
	    radVal[l].mach_abs    = tanMachAbs;
	    radVal[l].mach_rel    = tanMachRel;
	    radVal[l].alpha_abs   = tanAlphaAbs;
	    radVal[l].alpha_rel   = tanAlphaRel;
	    radVal[l].beta_abs    = tanBetaAbs;
 	    radVal[l].p_static    = tanPressure;
	    radVal[l].p_static_area = tanPressureA;
	    radVal[l].density     = tanDensity;
	    radVal[l].eddyVisc    = tanEddy;
	    radVal[l].massflow    = sumMass / bd->massflow;
            radVal[l].massLocal   = tanMass / bd->massflow;	    

	    radVal[l].vAx         = tanV_rel_ax;
	    radVal[l].vRad        = tanV_rel_rad;
	    radVal[l].vTanAbs     = tanV_abs_tan;
	    radVal[l].vTanRel     = tanV_rel_tan;
	    radVal[l].omega       = bd->omega;

	    radVal[l].segments    = global.block[blk].Segments;
	} /* for (l) */


	/* Now write out whole radial distribution */
	for ( l = 0; l < endPtL; l++){	
	
 	    fprintf(fp,"%9.4f %9.4f %10.1f %10.1f %10.2f %9.2f %9.4f %8.4f %9.2f %9.2f %9.2f %9.2f %11.2f %10.2f %9.2f %9.1f %8.4f %8.1f \n",
		    radVal[l].radius,
		    radVal[l].massflow,
		    radVal[l].p_total_abs,
		    radVal[l].p_total_rel,
		    radVal[l].t_total_abs,
		    radVal[l].t_total_rel,
		    radVal[l].mach_abs,
		    radVal[l].mach_rel,
		    radVal[l].vAx     * global.ref.a_0,
		    radVal[l].vRad    * global.ref.a_0,
		    radVal[l].vTanAbs * global.ref.a_0,
		    radVal[l].vTanRel * global.ref.a_0,
		    radVal[l].alpha_abs,
		    radVal[l].alpha_rel,
		    radVal[l].beta_abs,
		    radVal[l].p_static,
		    radVal[l].density,
		    radVal[l].eddyVisc);
	}
	
	fclose(fp);

	printf("        written to Radial Distribution File [%s] for blk %d\n",fileName,blk);
   	printf("        analysis plane at x = %g \n",xLocation);
    }
}

