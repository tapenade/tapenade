/*        Generated by TAPENADE     (INRIA, Ecuador team)
    Tapenade 3.14 (r7130) - 30 Oct 2018 16:49
*/
float bar(float x);
float foo(float x);

float top(float x) {
    x = foo(x);
    x = bar(x);
    return x;
}

int main() {
    float myres = 3;
    myres = foo(myres);
    printf("%f\n", myres);
}
