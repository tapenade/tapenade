Tapenade Authors
================

Main developer: Ecuador Team, Inria Sophia Antipolis - Méditerranée - tapenade@inria.fr
- Laurent Hascoët
- Valérie Pascual

Continuous integration and distribution, Tapenade web server
- Valérie Pascual
