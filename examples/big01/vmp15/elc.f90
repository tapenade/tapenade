
    subroutine elcont19(aflg,sflg,ctyp,nnc,nctno,conpar,dlamb,xlamb,find,iter,oflag,cnflag,matr,aug,corfl,ist)
!   Obtains the elemental stresses, assembles the internal equivalent forces and the tangent matrix
!   new contact formulation
!   aflg        - flag for axisymmetry (true=axi)
!   sflg        - flag for unsymmetric stiffness matrix (true=unsym)
!   ctyp        - contact element type
!   nnc         - number of contact element nodes
!   nctno       - number of cotnact elements
!   conpar      - contact parameters
!	dlamb       - load factor increment
!   xlamb       - load factor
!   find        - vertical total contact load
!   iter        - current Newton-Raphson iteration
!   oflg        - contact output flag
!   cnflag      - active contact flag, i.e. contact established
!   matr        - stiffness matrix flag, .true. -> calculate stiffness matrix
!   aug         - current augmentation number
!   corfl       - correction step flag
!   ist         - call purpose: 0 -> contact load
!                               1 -> sensitivity load in preprocessing
!                               2 -> sensitivity load in postproccessing
!-----------------------------------------------------------------------
    use nazi
    use genpar
	use pit
	use glob
	use inter
	use constif
	use fempar,only:nurw,rx,mnurp,crn,ncon,ftco,form,cstat,conout,aucou,aucoux,    &
	                csvar,snode,ndime,nnode,x,x0,conno,ldgof
    use gvec, only:resid1
!
	logical aflg,sflg,oflag,cnflag,matr,corfl
	character(5) ctyp
	integer mcn
	parameter (mcn=20)
	integer ic,i,j,nnc,aug,ist,nctno,iter
	integer ano
	real(8) conpar(8),cc(2),cn(2),vn(2),vnl,gp,kc(2,2)
    real(8) dx(2),eps,eps1,rr
	real(8) dlamb,xlamb,find,ffac
	real(8) kcf,ang,avgp,navgp,fmax,vdxm
	real(8) cs(2),cm1(2),cm2(2),fs(2),chi
	real(8) t3(3),dxs(3),dfac
	real(8) si,bet,lami,dtdg
!
	real(8) cm3(2)
	integer nriter
	real(8) yy(1:2),yyp(1:2),yypp(1:2),fun,dfdchi,chi1
	real(8) cm(1:2,1:mcn),sf(1:mcn),dsf(1:mcn),ddsf(1:mcn)
	real(8) chil,chir
	real(8) tt(2,2),nn(2,2),tn(2,2),nt(2,2),t(1:2),n(1:2)
	real(8) qf1,qf2,qf3,dqf1,dqf2,dqf3
	real(8) nf1,nf2,nf3,dnf1,dnf2,dnf3,ddnf1,ddnf2,ddnf3
	real(8) sc1,sc2
	real(8) mdfs(2,mcn),mdrho(mcn)
	real(8) mdfm1(2,mcn),mdfm2(2,mcn),mdfm3(2,mcn)
	real(8) fovec(mcn),tamat(mcn,mcn)
	real(8) q1,q2,q3
	real(8) chitl
!
	real(8) xxfa,xxfb
	real(8) asurf,xxxsc1,xxxsc2
	real(8) pifa
	real(8) xxl,xxr
!
	real(8) dnurd,ddnurd,dddnurd,wei(3),ftco2,cp1,cp2,cp3,s2al
	real(8) rad,delp,fbc(mcn)
	real(8) gtol,dnrm2,ddot
	integer imax,cpr(mcelem),cpcou,actlist(mcelem),nact
!
! 	external dnrm2
!
	pifa=datan(1.d0)
	imax=100
	ncon=0
	ftco=0.d0
	ftco2=0.d0
	cnflag=.false.
    fbc=0.d0
!   set active set storage
    asel=.false.  
!   get contact parameters
	eps=10.d-8
!	eps=10.d-15
    eps1=1.d-15
	gtol=1.d-5
	dfac=dsqrt(10.d0)
    rr=conpar(1)
	cc=x(1:2,nnode+1)
! barrier method-specific terms
!	if(iter.eq.0)idat1(:,9)=1.d0
	si=1.d0
!	bet=0.1d0
	bet=0.2d0
	lami=1.d0
	actlist=0
	nact=0
!	if(iter.le.5)cstif=cstif/10.d0
! compute slave node surface (model specific)
!	compute average penetration
!	avgp=0.d0
!	navgp=0
!	fmax=0.d0
!	do i=1,nctno
!		if(idat0(i,1).lt.0.d0)then
!			avgp=avgp+idat0(i,1)
!			navgp=navgp+1
!		endif
!		if(idat0(i,4).lt.fmax)then
!			fmax=idat0(i,4)
!		endif
!	enddo
!	if(avgp.ne.0.d0)then
!		avgp=avgp/navgp
!	endif
!	open(75)
	write(75,*)'NEW ****************'
!	write(*,'(a,f12.8)')'AVERAGE GAP ',avgp
!	write(*,'(a,f20.4)')'MAX NODE FORCE ',fmax
	find=0.d0
    

	if(ctyp.eq.'nsmop'.or.ctyp.eq.'nsmol'.or.ctyp.eq.'smope'.or.ctyp.eq.'smolo')then
! *********************************************************************************************
! ******************************************************************************
! ******************************************************************************
!		contact formulation for flexible-flexible sperical/cylindrical contact
!		10-noded element, 1 slave, 7 master, smoothed master curve between 3 nodes
!		max. 4 outer nodes needed for continuous tangent vector calculation
!		1 slave node plus two adjacent nodes on slave surface for area caculation
!		The three slave nodes have to be gioven with R_csl<R_cs<R_csr
!		calculate stiffness matrix, penalty formulation with modified augmented
!		lagrangian algorithm (penalty increased during iterations), or modified
!		augmented modified logarithmic barrier method
!		node order:cml2,cml1,cm1,cm2,cm3,cmr1,cmr2,csl,cs,csr
!       loop over contact elements
        do ic=1,nctno
            if(cstat(ic))then
                xxfa=0.d0
                xxfb=0.d0
                xxl=0.d0
                xxr=0.d0
                cm=0.d0
!               get master nodes current coordinates
                cm(1:2,3)=x(1:2,conno(ic,3))
                cm(1:2,4)=x(1:2,conno(ic,4))
                cm(1:2,5)=x(1:2,conno(ic,5))
!               get slave node current coordinates
                ano=conno(ic,snode)
                cs(1:2)=x(1:2,ano)
!               get tangent definition points
                if(conno(ic,1).ne.0)then
                    cm(1:2,1)=x(1:2,conno(ic,1))			
                    cm(1:2,2)=x(1:2,conno(ic,2))
                    xxfa=1.d0
                endif
                if(conno(ic,7).ne.0)then
                    cm(1:2,6)=x(1:2,conno(ic,6))			
                    cm(1:2,7)=x(1:2,conno(ic,7))
                    xxfb=1.d0
                endif
!               calculate first estimate for chi
                chitl=ddot(2,cs-cm(:,4),1,cm(:,5)-cm(:,4),1)/ddot(2,cm(:,5)-cm(:,4),1,cm(:,5)-cm(:,4),1)
!               calculate chi
                nriter=0
                chi=10.d0
                chi1=chitl
                do while(nriter.lt.imax.and.dabs(chi1-chi).gt.eps1)
!               do while(nriter.lt.imax.and.dabs(chi1-chi).gt.1.d-20)
!                   update parameters
                    nriter=nriter+1
                    chi=chi1
!                   calculate local tangent vector
                    call co7qh(7,chi,sf(1:7),xxfa,xxfb)
                    call dco7qh(7,chi,dsf(1:7),xxfa,xxfb)
                    call ddco7qh(7,chi,ddsf(1:7),xxfa,xxfb)
                    call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,sf(1:7),1,0.d0,yy(1:2),1)
                    call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,dsf(1:7),1,0.d0,yyp(1:2),1)
                    call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,ddsf(1:7),1,0.d0,yypp(1:2),1)
                    fun=ddot(2,cs(1:2)-yy(1:2),1,yyp(1:2),1)
                    dfdchi=ddot(2,cs(1:2)-yy(1:2),1,yypp(1:2),1)-ddot(2,yyp(1:2),1,yyp(1:2),1)
                    chi1=chi-fun/dfdchi
                enddo
                chi=chi1
!               fudge for slave node on axis of symmetry
                if(aflg.and.cs(1).lt.eps.and.xxfa.eq.0)then
                    chi=-1.d0
                elseif(aflg.and.cs(1).lt.eps.and.xxfb.eq.0)then
                    chi=1.d0
                endif
                call co7qh(7,chi,sf(1:7),xxfa,xxfb)
                call dco7qh(7,chi,dsf(1:7),xxfa,xxfb)
                call ddco7qh(7,chi,ddsf(1:7),xxfa,xxfb)
                call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,sf(1:7),1,0.d0,yy(1:2),1)
                call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,dsf(1:7),1,0.d0,yyp(1:2),1)
                call dgemv('N',2,7,1.d0,cm(1:2,1:7),2,ddsf(1:7),1,0.d0,yypp(1:2),1)
!               calculate local tangent and normal
                t=0.d0
                n=0.d0
                if(chi.ge.-1.d0.and.chi.le.1.d0)then
                    t(1:2)=yyp(1:2)/dnrm2(2,yyp(1:2),1)
                    n(1)=-t(2)
                    n(2)= t(1)
    				asel(ic)=.true.
    				nact=nact+1
    				actlist(nact)=ic
                endif
!               calculate gap
                gp=ddot(2,cs(1:2)-yy(1:2),1,n(1:2),1)
!               write(*,*)'GAP ',ic,chi,gp
	            idat1(ic,1)=gp
                idat1(ic,2)=chi
                idat1(ic,3:4)=yy(1:2)
                idat1(ic,5:6)=yyp(1:2)
                idat1(ic,7:8)=yypp(1:2)
	    		crn(ic,1)=conno(ic,snode)
		    endif
		enddo
!
!	    check duplicate nodes
        call codu(nact,actlist,snode,cnflag)
!       new loop for load and stiffness matrix determination
        do j=1,nact
            ic=actlist(j)
			if(cstat(ic).and.asel(ic))then
	            gp=idat1(ic,1)
                chi=idat1(ic,2)
    			yy(1:2)=idat1(ic,3:4)
	    		yyp(1:2)=idat1(ic,5:6)
	    		yypp(1:2)=idat1(ic,7:8)
	    		xxfa=0.d0
	    		xxfb=0.d0
!               get tangent definition points
                if(conno(ic,1).ne.0)then
                    xxfa=1.d0
                endif
                if(conno(ic,7).ne.0)then
                    xxfb=1.d0
                endif
                call co7qh(7,chi,sf(1:7),xxfa,xxfb)
                call dco7qh(7,chi,dsf(1:7),xxfa,xxfb)
                call ddco7qh(7,chi,ddsf(1:7),xxfa,xxfb)
!		    	check if projection is on master segment
			    if(asel(ic))then
			    	t(1:2)=yyp(1:2)/dnrm2(2,yyp(1:2),1)
				    n(1)=-t(2)
    				n(2)= t(1)
!                   calculate element surface
	                call consurf(aflg,csvar,snode,ic,asurf)
	    			call ctrac(corfl,iter,aug,form,cstif,gp,n,asurf,bet,idat1(ic,9),ffac,dtdg,fs,aselo(ic),asel(ic))
!                 
                    
!                   define contact force vector matrix
                    fovec(1:2) =fs(1:2)*sf(1)
                    fovec(3:4) =fs(1:2)*sf(2)
                    fovec(5:6) =fs(1:2)*sf(3)
                    fovec(7:8) =fs(1:2)*sf(4)
                    fovec(9:10)=fs(1:2)*sf(5)
                    fovec(11:12) =fs(1:2)*sf(6)
                    fovec(13:14) =fs(1:2)*sf(7)
                    fovec(2*snode-1:2*snode)=-fs
                    !tamat(2*snode-1:2*snode,1:mcn)=-mdfs
!                   assemble global stiffness matrix and load vector *****************************
	                !call cmattra(nnc,ic,snode,tamat(1:2*nnc,1:2*nnc),fovec(1:2*nnc),iter,dlamb,find,sflg,matr,ist)
                endif
                continue
            endif
        enddo
 endif
 end
!
