
	subroutine ctrac(cfl,iter,aug,form,stif,gp,n,surf,bet,oldff,ffac,dtdg,fc,cacto,cact)
!	calculate contact force 
!	penal - penalty formulation
!	lobar - modified logarithmic barrier formulation
!   cact  - contact element active in current increment
!   cacto - contact element active in previous increment
!   cfl   - correction flag : false> force calculation, true> correction term calculation
	use fempar,only:aucou
	character(5) form
	logical cact,cacto,cfl
	integer iter,aug
	real(8) stif,gp,oldff,ffac,dtdg,fc(2),n(2),bet,surf
	real(8) s,a,b,lami
!
    if(cfl)then
!   calculate correction term according to Dussault for logarithmic barrier method
        if(form.eq.'lobar')then
		    lami=1.d0
		    s=1.d0
		    a=-lami*stif/(s*(1-bet))**2
		    b= lami*(1.d0-2.d0*bet)/s/(1.d0-bet)**2
            if(gp.lt.-bet*s/stif)then
!			    ffac=a*gp+b
			    ffac=lami/(s*(1-bet))**2*gp*stif**2
		    else
!			    ffac=lami/(s+stif*gp)
			    ffac=lami*gp/(s+stif*gp)**2*stif**2
		    endif
		    fc(1:2)=ffac*n(1:2)*surf
		    cact=.true.
	    endif
    else
!       calculate contact traction and stiffness
	    if(form.eq.'penal')then
    !	    *** contact ***
    !		compute force vector
		    ffac=-stif*gp
		    dtdg=-stif
    !		compute force vectors
		    fc(1:2)=ffac*n(1:2)*surf
		    cact=.true.
		    if(gp.ge.0.d0)then
    !		    *** no contact ***
    !			set to zero
			    ffac=0.d0
			    dtdg=0.d0
    !!			if(oldff.ne.0.d0.and.iter.le.1)then
    !			if(oldff.ne.0.d0.and.iter.le.1.and.aucou.eq.1)then
			    if(cacto.and.iter.le.1.and.aucou.eq.1)then
				    fc =-fc
    !				write(*,*)'UNLOADING'
			    else
				    fc =0.d0
				    cact=.false.
			    endif
		    endif
	    elseif(form.eq.'lobar')then
		    lami=1.d0
		    s=1.d0
		    a=-lami*stif/(s*(1-bet))**2
		    b= lami*(1.d0-2.d0*bet)/s/(1.d0-bet)**2
            if(gp.lt.-bet*s/stif)then
			    ffac=a*gp+b
			    dtdg=a
		    else
			    ffac=lami/(s+stif*gp)
			    dtdg=-lami*stif/(s+stif*gp)**2
		    endif
    !		if(aug.eq.1)then
    !		    if(ffac.ge.5000.d0)then
    !		        ffac=5000.d0
    !		        dtdg=ffac/gp
    !!		        write(*,*)'CUT ',iter
    !		    endif
    !		endif
    !	    compute force vectors
		    fc(1:2)=ffac*n(1:2)*surf
		    cact=.true.
	    else
		    write(*,*)'Contact formulation not implemented'
		    stop
	    endif
	endif
	end
!
	subroutine consurf(aflg,svar,snode,elem,surf)
!	calculate nodal contact surface
!	aflg - axisymmetry flag: true=axi, false=plane strain
!   svar - surface falg: true = slave surface defined by two additional nodes, false = one slave node only per element
!   snode - position of slave node in contact element definition list
!   elem - contact element number
!   surf - contact surface
!
    use fempar, only:x,conno
    use pit
    logical aflg,svar
    integer snode
    integer elem
    real(8) surf,xxl,xxr
    real(8) cs(1:2),csl(1:2),csr(1:2),dxl(1:2),dxr(1:2),ls,lr
    real(8) dnrm2
    !
    if(svar)then
!       get slave node current coordinates
        cs(1:2)=x(1:2,conno(elem,snode))
!   	get slave surface definition points
	    if(conno(elem,snode-1).ne.0)then
		    csl(1:2)=x(1:2,conno(elem,snode-1))			
		    xxl=1.d0
        else
	        csl=0.d0
		    xxl=0.d0
	    endif
	    if(conno(elem,snode+1).ne.0)then
    	    csr(1:2)=x(1:2,conno(elem,snode+1))			
	        xxr=1.d0
	    else
		    csr=0.d0
		    xxr=0.d0
        endif
!       calculate element surface
	    dxl=csl-cs
	    dxr=csr-cs
	    ls=dnrm2(2,dxl(1:2),1)
	    lr=dnrm2(2,dxr(1:2),1)
	    if(aflg)then
!   	    axisymmetry
		    surf=pi/3.d0*(xxl*ls*(2.d0*cs(1)+csl(1))+xxr*lr*(2.d0*cs(1)+csr(1)))
        else
!           plane strain (not checked yet !!!)                    
	        surf=0.5d0*(xxl*ls+xxr*lr)
	    endif
    else
	    if(aflg)then
!           get slave node current coordinates
            cs(1:2)=x(1:2,conno(elem,snode))
!   	    axisymmetry
		    surf=2.d0*pi*cs(1)
        else
!           plane strain
	        surf=1.d0
	    endif
    endif
    end
!
	subroutine cmatsurf(aflg,svar,snode,elem,ffac,n,dfs)
!	calculate nodal contact surface
!	aflg - axisymmetry flag: true=axi, false=plane strain
!   svar - surface falg: true = slave surface defined by two additional nodes, false = one slave node only per element
!   snode - position of slave node in contact element definition list
!   elem - contact element number
!   ffac - contact traction magnitude
!   n    - normal vector
!   dfs  - stiffness matrix contribution of slave node
!
    use fempar, only:x,conno
    use pit
    logical aflg,svar
    integer snode,elem,i
    real(8) xxl,xxr
    real(8) cs(1:2),csl(1:2),csr(1:2),dxl(1:2),dxr(1:2),n(2),mat(2,2),ffac,ls,lr
    real(8) dfs(2,20)
    real(8) dnrm2
    if(svar)then
!       variable surface defined by 3 nodes ***
!       get slave node current coordinates
        cs(1:2)=x(1:2,conno(elem,snode))
!   	get slave surface definition points
	    if(conno(elem,snode-1).ne.0)then
		    csl(1:2)=x(1:2,conno(elem,snode-1))			
		    xxl=1.d0
        else
	        csl=0.d0
		    xxl=0.d0
	    endif
	    if(conno(elem,snode+1).ne.0)then
    	    csr(1:2)=x(1:2,conno(elem,snode+1))			
	        xxr=1.d0
	    else
		    csr=0.d0
		    xxr=0.d0
        endif
!       calculate element surface
	    dxl=csl-cs
	    dxr=csr-cs
	    ls=dnrm2(2,dxl(1:2),1)
	    lr=dnrm2(2,dxr(1:2),1)
!       part of left slave node                            
        do i=1,2
	        mat(i,1:2)=n(i)*dxl/ls
        enddo
        if(aflg)then
!           contribution of csl                         
            dfs(1:2,2*snode-3:2*snode-2)=xxl*ffac*mat*pi/3.d0*(2.d0*cs(1)+csl(1))
            dfs(1:2,2*snode-3)=dfs(1:2,2*snode-3)+xxl*ffac*n*pi/3.d0*ls
!           contribution of cs
            dfs(1:2,2*snode-1:2*snode)=dfs(1:2,2*snode-1:2*snode)-xxl*ffac*mat*pi/3.d0*(2.d0*cs(1)+csl(1))
            dfs(1:2,2*snode-1)=dfs(1:2,2*snode-1)+xxl*ffac*n*pi/3.d0*ls*2.d0
        else
!           contribution of csl
	        dfs(1:2,2*snode-3:2*snode-2)=xxl*0.5d0*ffac*mat
!           contribution of cs
            dfs(1:2,2*snode-1:2*snode)=dfs(1:2,2*snode-1:2*snode)-xxl*0.5d0*ffac*mat
        endif
!       part of right slave node
        do i=1,2
	        mat(i,1:2)=n(i)*dxr/lr
        enddo
        if(aflg)then
!           contribution of cs
            dfs(1:2,2*snode-1:2*snode)=dfs(1:2,2*snode-1:2*snode)-xxr*ffac*mat*pi/3.d0*(2.d0*cs(1)+csr(1))
            dfs(1:2,2*snode-1)=dfs(1:2,2*snode-1)+xxr*ffac*n*pi/3.d0*lr*2.d0
!           contribution of csr
            dfs(1:2,2*snode+1:2*snode+2)=xxr*ffac*mat*pi/3.d0*(2.d0*cs(1)+csr(1))
            dfs(1:2,2*snode+1)=dfs(1:2,2*snode+1)+xxr*ffac*n*pi/3.d0*lr
        else
!           contribution of cs
            dfs(1:2,2*snode-1:2*snode)=dfs(1:2,2*snode-1:2*snode)-xxr*0.5d0*ffac*mat
!           contribution of csr
            dfs(1:2,2*snode+1:2*snode+2)=xxr*0.5d0*ffac*mat
        endif
    else
!       slave surface relates to slave node only ***
	    if(aflg)then
!           get slave node current coordinates
!           contribution of cs
            dfs(1:2,2*snode-1)=dfs(1:2,2*snode-1)+ffac*n*2.d0*pi
        else
!           no contribution
        endif
    endif
    end
!
!! ***		            test alternative calculation of contact traction		    
!                        if(matr)then  
!				            fftest=0.d0
!				            itest=ldgof(1,conno(ic,1))
!				            if(itest.gt.0)then
!				                fftest=fftest-resid1(itest)*n(1)
!				            endif
!				            itest=ldgof(2,conno(ic,1))
!				            if(itest.gt.0)then
!				                fftest=fftest-resid1(itest)*n(2)
!    			            endif
!    			            if(ffac.gt.0.d0)then
!    				            write(*,*)ic,ffac,fftest
!    				        endif
!    				    endif
!! ***    				    
!

!
	subroutine codu(nact,actlist,snode,cnflag)
!	chack for duplicate contact slave nodes and absence of contact
!	nnc     - number of nodes in contact element
!   ic      - contact element number
!   snode   - slave node poistion in elemental node list
!   tamat   - element stiffness matrix
!   fovec   - element force vector
!   iter    - NR iteration number
!   dlamb   - incremental load increase factor
!   sflg    - symmetry flag for stiffness matrix
!   matr    - stiffness amtrix construction flag
!   ist     - load vector flag: 0->load vector, 1->=sensitivity load vector preproc, 2->=sensitivity load vector postproc 
!
    use genpar,only:mcelem
    use fempar, only:conno,cstat
    use inter,only:asel,idat1
    logical cnflag
    integer snode,i,j,ic,ic2,nact,actlist(mcelem),ncon,ano
    real(8) gp,gp2,ftco,ftco2
!
!	check duplicate nodes
    ncon=0
    ftco=0.d0
    ftco2=0.d0
    do i=1,nact-1
        ic=actlist(i)
	    if(cstat(ic).and.asel(ic))then
		    gp =idat1(ic,1)
    		ano=conno(ic,snode)
    		ncon=ncon+1
	    	ftco=ftco+dabs(gp)		
			ftco2=ftco2+dabs(gp)		
	    	do j=ic+1,nact
    	        ic2=actlist(j)
		    	if(cstat(ic2).and.asel(ic2))then
			    	gp2 =idat1(ic2,1)
					if(conno(ic2,snode).eq.ano)then
	    				if(dabs(gp).le.dabs(gp2))then
                            asel(ic2)=.false.
						else
                            asel(ic)=.false.
	    				endif
					endif
    			endif
	    	enddo
	    endif
    enddo
!
    ic=actlist(nact)
    if(cstat(ic).and.asel(ic))then
        gp=idat1(ic,1)
		ncon=ncon+1
    	ftco=ftco+dabs(gp)		
		ftco2=ftco2+dabs(gp)		
	endif
	if(ftco2.eq.0.d0)then
	    cnflag=.true.
	endif
	end
!
