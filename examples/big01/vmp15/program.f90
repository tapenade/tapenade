
      MODULE GENPAR
! define general program parameters
!     mpoin  --> maximum number of nodes
!     melem  --> maximum number of elements
!     mdgof  --> maxiumu number of degrees of freedom
!     mprof  --> maximum number of off-diagonal terms in tangent matrix
!     mnode  --> maximum number of nodes per element
!     mgaus  --> maximum number of Gauss points per element
!     mmats  --> maximum number of materials 
!     mbpel  --> maximum number of pressure elements
!     mis    --> maximum number of added load increments in computation
!     mcelem --> maximum number of contact elements
      logical poif
      integer mpoin,melem,mdgof,mprof,mnode,mgaus,mmats,mbpel,mis,npar
      integer mcelem,mpnode
      parameter(mpoin=2300,melem=1900,mdgof=5000,mcelem=1000,mpnode=4)
      parameter(mprof=100000)
      parameter(mnode=12  ,mgaus=   9,mmats=  15,mbpel=   100)
      parameter(mis  =50,npar=9)
!**********************************************************************
!     Define some communication flag parameters.
      integer           initial, eval_fc, eval_gA, eval_w, eval_x0,finished, newpoint, terminate
      parameter        (initial      =  0,      &
                        eval_fc      =  1,      &
                        eval_gA      =  2,      &
                        eval_w       =  3,      &
                        eval_x0      =  4,      &
                        finished     =  5,      &
                        newpoint     =  6,      &
                        terminate    = 99)
!     These parameters can be made smaller or larger to adjust for
!     application problem and host machine memory sizes. Please see
!     the recommended sizes below.
      integer  max_n, max_m, max_nnzj, max_nnz_w
!**********************************************************************
!     Recommended maximum array sizes  
!        n: number of variables
!        m: number of constraints
!     *  very small scale problems   n/m <=     200
!**********************************************************************
!     Very small scale problems
      parameter (max_n       = 200)
      parameter (max_m       = 200)
      parameter (max_nnzj    = 40000)
      parameter (max_nnz_w   = 40000)
      END
!
      module gmat
! module for sharing global stiffness matrix, condensed form
    use genpar,only:mdgof,mprof
	real(8) csrk(2*mprof+mdgof)
	integer columns(2*mprof+mdgof),rowindex(mdgof)
    end
!
    module newmat
! module for storing stiffness matrix in skyline format
    use genpar,only:mdgof
!   for pardiso format
    integer pariparm(64),pperm(mdgof),pt(64)
    end
!
    module gvec
! module for sharing global solution and load vectors
    use genpar,only:mdgof
    real(8) eload(mdgof),pdisp(mdgof),resid(mdgof),displ(mdgof),react(mdgof),tload(mdgof)
    real(8) resid1(mdgof),residc(mdgof)
    end
!
    module glob
! module for sharing global stiffness matrix, full matrix and global displacement and force vector
    use genpar,only:mdgof
    real(8) stifgcsr(200,200)
    end
!
    module nazi
! defines all variable types for debugging
    implicit none
    end
!      
    module plast
! module for sharing mechanical variables
    use genpar, only:mgaus,melem
    real(8) stresv0(4,mgaus,melem),ivv0(5,mgaus,melem),eplv0(4,mgaus,melem),stresv1(4,mgaus,melem),   &
           ivv1(5,mgaus,melem),eplv1(4,mgaus,melem),defg0(5,mgaus,melem),defg1(5,mgaus,melem),        &
           rotm0(5,mgaus,melem),rotm1(5,mgaus,melem),rotm12(5,mgaus,melem),bev0(4,mgaus,melem),       &
           bev1(4,mgaus,melem)
    end
!
    module rotz
! total rotation matrices of integration points
    use genpar, only:mgaus,melem
    real(8) rot0(3,3,mgaus,melem),rot1(3,3,mgaus,melem)
    real(8) strr(3,3,mgaus,melem),alrr(3,3,mgaus,melem)
    real(8) eprr(3,3,mgaus,melem),err(3,3,mgaus,melem)
    end
!
    module pit
! set parameter 2*pi
      real(8) pi2,pi
      parameter (pi2=2.d0*3.1415926535897932384626433832795d0)
      parameter (pi=3.1415926535897932384626433832795d0)
      end
!
      module defgr0
! derivatives of shape functions with respect to initial configuration
!     elecd0 -->  Cartesian derivatives
!                | dN1/dx, dN2/dx,... |
!                | dN1/dy, dN2/dy,... | x  ngaus x nelem
!                | dN1/dz, dN2/dz,... |
      use genpar, only:mnode,mgaus,melem
      real(8) elecd0(3,mnode,mgaus,melem)
      end
!
      module defgr
! derivatives of shape functions with respect to initial configuration
!     elecd0 -->  Cartesian derivatives
!                | dN1/dx, dN2/dx,... |
!                | dN1/dy, dN2/dy,... | x  ngaus x nelem
!                | dN1/dz, dN2/dz,... |
      use genpar, only:mnode,mgaus,melem
      real(8) elecdt(3,mnode,mgaus,melem),vinct(mgaus,melem)
      end
!
      module pval
!     values of pressure values at integration points
      use genpar, only:mgaus,melem,mnode,mpnode
      real(8) pelm(mgaus,melem),pnval(mpnode,melem)
      end
!
      module mproj
!     von Mises projection operator and 4didentity tensor
      real(8) mpr(3,3,3,3),mprs(3,3,3,3),imat(3,3,3,3),idy4(3,3,3,3),imats(3,3,3,3)
      real(8) del(3,3)
!!	parameter (del=(/1.d0,0.d0,0.d0,0.d0,1.d0,0.d0,0.d0,0.d0,1.d0/))
      end
!
      module inter
!     interface element data at integration points
      use genpar, only:melem,mis,mcelem
      logical asethist(2,mcelem),glact,asel(mcelem),aselo(mcelem)
      real(8) idat1(melem*3,20),idat0(melem*3,20)
      real(8) idat2(melem,20,mis)
      end
!
      module grads0
! module for initial gradients with respect to material parameters
! in rotation-neutralized coordinate frame
      use genpar, only:mgaus,melem,npar
      real(8), save, allocatable:: dbev0dk(:,:,:,:),dstresv0dk(:,:,:,:),divv0dk(:,:,:,:)
      end
!
      module grads1
! module for initial gradients with respect to material parameters
! in rotation-neutralized coordinate frame
      use genpar, only:mgaus,melem,npar
      real(8), save, allocatable:: dbev1dk(:,:,:,:),dstresv1dk(:,:,:,:),divv1dk(:,:,:,:)
      end
!
      module gradresid
! module for calculating gradient of load vector and displacement vector
      use genpar, only:mgaus,melem,mdgof,npar
      real(8) df0dk(mdgof,npar)
      real(8), save, allocatable:: dudk(:,:),dfdk(:,:),dreactdk(:,:),du0dk(:,:)
      end
!
      module presid
! module for calculating gradient of pressure vector for mixed formulation
      use genpar, only:melem,mpnode,npar
!      real(8) dpn0dk(npar,mpnode,melem),ddpndk(npar,mpnode,melem)
      real(8),save,allocatable :: dpn0dk(:,:,:),ddpndk(:,:,:)
      end
!
      module fempar
! module for finite element model parameters (usualy passed as arguments in hepperdang
      use nazi
      use genpar, only:mgaus,melem,mpoin,mnode,mdgof,mmats,mis,mcelem
!
      character(5) ctyp,form
      character(3) stptyp
      logical asym,axi,cont,ada,iflag,nflag,flcon,imp,stpflg,mflag,csvar
      logical cstat(mcelem)
      integer ngaus,ndime,nelem,npoin,nnode,pnode,ndgof,negdf,nctno,nnc,snode
      integer itot
      integer ldgof(2,mpoin),lnods(mnode,melem),conno(mcelem,10)
      integer tits(10,mis)
      integer matyp(mmats),matno(melem),suseg(100,3),elset(mcelem)
      integer miter,nincr,nprs,nprs0,nprof,rateeq,nimpel
      real(8) cnorm,xlamb,x(3,mpoin),x0(3,mpoin),delu(3,mpoin)
      real(8) pp(mnode,melem),delp(mnode,melem)
      reaL(8) eledb(4,mnode+1,mgaus)
      real(8) props(12,mmats),conpar(8),gap(mcelem,mis),inden(0:30,mis)
      real(8) udis(200,mis),ccontr(4),xint(2,mpoin),deluint(2,mpoin),pint(mnode,melem)
      integer cconti(4)
      real(8) rx0(1:2,mpoin),rx(1:2,mpoin),rxi(2),nurw(3,mcelem),ftco
      integer nnurp,mnurp,crn(mcelem,10),ncon,aucou,aucoux
      real(8) dtmin,dtmax
	real(8) allgrd(9,10,mis,3),allval(9,mis),allval0(9,mis),num
! new for spring elements
      logical spri
      integer spno(mpoin,2),nspr
      real(8) sppar(2)
! new for loadstep control
      integer lsipar(4,100)
      real(8) lsrpar(6,100)

      real(8) conout(mcelem,10)
      real(8) searc
      end
!
      module kin1
!   module for storing state variables in rotation neutralized coordinates
!   kinematic quantities and kinematic fourth order tensors
      use genpar
      real(8),save,allocatable :: dehs(:,:,:,:),s1hs(:,:,:,:),al1hs(:,:,:,:),s0hs(:,:,:,:),al0hs(:,:,:,:)
      end
!
      module kin2
!   module for storing state variables in rotation neutralized coordinates
!   kinematic quantities and kinematic fourth order tensors
      use genpar
      real(8),save,allocatable :: ttens(:,:,:,:,:,:),tt41(:,:,:,:,:,:),tt42(:,:,:,:,:,:)
      end
!
      module fix
!     fixed material parameters
      real(8) fmu,fee,fhkin,fhnl
      end
!
      module fdata
!     store experimental data and computed values
      use genpar, only:mis,npar
      integer nexp,nimp
      real(8) fexp(1:mis),fcom(1:mis),udat(1:mis)
      real(8) ximp(1:mis),dimp(1:mis),dcom(1:mis)
      real(8) ddcomdk(1:mis,1:npar),sca
      integer nact(mis),actelset(mis,20)
      end
!
      module store
      use genpar,only:npar
!     store model parameters and error function gradients
      real(8) xstore(npar),gstore(npar),hstore(npar,npar),fstore
      end
!
      module hist
!     stores state variables and geometry for all increments
      use genpar, only:mgaus,melem,mis,mpoin,mpnode
      real(8) sthis(4,mgaus,melem,0:mis),ivhis(5,mgaus,melem,0:mis)
      real(8) fhis(5,mgaus,melem,0:mis),rhis(5,mgaus,melem,0:mis)
      real(8) r12his(5,mgaus,melem,0:mis),behis(4,mgaus,melem,0:mis)
      real(8) xhis(2,mpoin,0:mis),deluhis(2,mpoin,0:mis)
	real(8) phis(mpnode,melem,0:mis)
      end
!
      module fnod
!     stores nodes which are summed over to get reaction force
      use genpar, only:melem,mpoin
      integer ngrel,ngrnod
      integer nlist(mpoin),elist(melem),hnode
      end
!
      module globfl
!     global flag for first iteration in optimization process
      logical glfl
      end
!
      module disgr
      use genpar,only:npar,mdgof
!     store displacement gradient
      real(8) sdudk(mdgof,npar)
      end
!
! ********************************************************************
! copied
!
      module grafu
      use genpar,only:npar
!     parameters set for fudging gradient calculations by activating or inactivating parameters
      real(8) gfu(10),hfu(10,10)
      end
!
      module calc
!     parameter for recalculating gradient and function
      logical fcalc
      end
!
      module scale
!     parameter vector for scaling material parameters
      use genpar
      real(8) oscale(10)
      end
!
! **** new
      module constif
!     parameters for contact stiffness
      use genpar,only:mcelem
      real(8) cstif,csmin,csmax,csold,afac
      integer aucous,aucxx
      end
! **** new
!
! **** new
      module inks
!     number of increments
      integer nincs,sgnincs
      end
! **** new
!
! **** new
      module test
!     test data for debugging
      use genpar
      real(8) pold(8,mpoin),told(2,mpoin),chiold(mpoin)
      end
! **** new

! **** new
      module nodegrad
!     store contact traction gradient
      use genpar
      real(8) cgrad(mpoin,npar),c2grad(mpoin,16,npar)
      end
! **** new

! **** new
      module getsig
!     transfer stress gradient
      use genpar
      real(8) dds1(3,3,npar),ss1(3,3)
      end
! **** new

! **** new
      module test2
!     define test element, gausspoint and component
      use genpar
      integer tig,tie,tco,tco2
      end
! **** new
!
    module connect
!   treats arry for connctivity calculations
    integer,save,allocatable :: connec(:,:)
    end

