

! 
! =======================================================================
! 
module Common
! 
! -----------------------------------------------------------------------
! Purpose: 
!   The common module is used to define global constants, user options,
!   namelists, etc.  It should never be used for variables.
! 
! Author: J.E. Hicken, Feb 2006
! -----------------------------------------------------------------------
   
  use mpi

  implicit none

! _______________________
! -- type kind definitions
  !-- int8 is used for stencils which need at least 25 bits
  integer, parameter :: int8 = selected_int_kind(8)
  !-- dp is the default double precision real
  integer, parameter :: dp = kind(1.d0)
  
! _______________________
! -- global constants

  integer, parameter :: &
       nVar = 5, &       !-- number of variables at each node
       asStencil = 13, & !-- maximum number of nbrs in stencil of as
       paStencil = 7     !-- maximum number of nbrs in stencil of pa
  real(kind=dp), parameter :: &
       large = 1.d+15, small = 1.d-15, &
       gamma = 1.4d0, gami = 0.4d0, &
       pi = 3.14159265358979d0

  !-- debugging flag; probably doesn't do what you think it does
  logical :: debug = .false.

! _______________________
! -- MPI information
  integer :: &
       myrank, & !-- the rank of this process
       nProc     !-- the total number of processors

  !-- list of tags for various operations
  integer, parameter :: EXCHANGE_TAG = 1, IO_TAG = 2

! _______________________
! -- file names
  integer, parameter :: slen = 128
  character(11), parameter :: input_file = 'input.param'
  character(len=40) :: grid_file_prefix, map_file_prefix, &
       output_file_prefix
  namelist /files/ grid_file_prefix, map_file_prefix, &
       output_file_prefix
  
! _______________________
! -- file units
  integer, parameter :: input_unit = 10, grid_unit = 20, map_unit = 30, &
       con_unit = 40, junk_unit = 50, out_unit = 60, res_unit = 70, &
       q_unit = 80, his_unit = 90, cp_unit = 100, Jmat_test_unit = 110, &
       newgrid_unit = 120, patch_unit = 130, femsurf_unit = 140, &
       femgrid_unit = 150
  
! _______________________
! -- user specified options

  type GridOpts !-- mesh options
    logical :: &
         explicit_map !-- use an explicit B-spline mesh
    integer :: &
         order(3), &  !-- the B-spline order in each coordinate direction
         ncpts(3)     !-- number of B-spline control points in each dir.
  end type GridOpts
  type(GridOpts) :: grid

  namelist /mesh/ grid

  !-----------------------------------
  integer :: nhalo
  real(kind=dp) :: & !-- Matrix dissipation paramters
         Vl,  &      !-- spectral radius scaling for linear waves
         Vn          !-- spectral radius scaling for nonlinear waves
  type SolverOpts !-- solver options
    logical :: &
         restart, &  !-- is this a restart?
         viscous, &  !-- is the flow viscous?
         p_switch, & !-- is the pressure switch on?
         frz_coef24  !-- are the dissipation coefficients frozen?
    integer :: &
         idmodel,  & !-- type of dissipation model
         istream,  & !-- coordinate direction of streamwise flow (xyz)
         iground,  & !-- coordinate direction of vertical direction
         reorder,  & !-- type of reordering to use
         updat_frq,& !-- # interations between approx. Jac. updates
         nk_strtup,& !-- type of quasi-Newton start-up to use
         nk_time,  & !-- sets how the reference time step is calculated
         nk_pfrz,  & !-- iteration at which to freeze the preconditioner
         nk_its,   & !-- maximum number of Newton-Krylov iterations
         lev_fil,  & !-- fill-level used for the preconditioner
         gmres_its,& !-- GMRES iterations used during the linear solve
         schur_its   !-- maximum number of approximate Schur iterations
    real(kind=dp) :: &
         dt_min,   & !-- minimum time step used during start-up
         A, B,     & !-- terms in the geometric time step for start-up
         beta,     & !-- the exponent used for the reference time step
         alpha,    & !-- the factor used for the reference time step
         min_res,  & !-- tolerance with which to converge the solver
         prec_dlf, & !-- prec. 4th-difference dissipation lumping factor
         drop_tol, & !-- drop tolerance at which to switch to full Newton
         strtup_tol,&!-- tolerance before switching from quasi-Newton
         newton_tol,&!-- linear solver tolerance once switched to Newton 
         schur_tol   !-- approximate schur preconditioner tolerance
    real(kind=dp), dimension(3) :: &
         dis2,     & !-- 2nd-difference dissipation coefficient
         dis4        !-- 4th-difference dissipation coefficient
  end type SolverOpts
  type (SolverOpts) :: diablo

  namelist /solver/ nhalo, Vl, Vn, diablo

  !-----------------------------------
  type OptimizeOpts  !-- optimizer options
    character(len=4) :: &
         obj_fun     !-- 4 character expression for the desired objective
    real(kind=dp) :: &
         adj_tol     !-- tolerance to solve the adjoint equations
  end type OptimizeOpts

  type(OptimizeOpts) :: jtstrm
  character(len=10) :: opt_method

  namelist /optimizer/ opt_method, jtstrm

! _______________________
! -- global strucutres

  type FarFieldVar !-- far-field state
    real(kind=dp) :: &
         alpha, &     !-- angle of attack
         rho, e, h, & !-- free-stream density, energy, enthalpy
         p, a, &      !-- free-stream pressure and sound-speed
         Mach, Re     !-- free-stream Mach number and Reynolds number
    real(kind=dp), dimension(3) :: &
         uvw          !-- free-stream velocity
  end type FarFieldVar
  type(FarFieldVar) :: fsvar
  real(kind=dp)     :: qfs(nVar) !-- free-stream conservative variables

  
! _______________________
! module subroutines
contains
! 
! =======================================================================
! 
  subroutine startMPI() 
! 
! -----------------------------------------------------------------------
! Purpose: Initializes MPI, finds this processor's rank and the total
!   number of processors
! 
! Post: 1) myrank is initialized to rank of calling process
!       2) nProc is initialized to the number of processors
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- local variables
    integer :: ierr

! _____________________
! begin main execution

    !-- initialize, get number of processes, and myrank
    !-- check for problems
    call mpi_init(ierr)
    if (ierr /= MPI_SUCCESS) then
      write (*,*) 'Error in startMPI :: mpi_init failed'
      stop 'startMPI'
    end if
    call mpi_comm_size(MPI_COMM_WORLD, nProc, ierr)
    if (ierr /= MPI_SUCCESS) then
      write (*,*) 'Error in startMPI :: mpi_comm_size failed'
      stop 'startMPI'
    end if
    call mpi_comm_rank(MPI_COMM_WORLD, myrank, ierr)
    if (ierr /= MPI_SUCCESS) then
      write (*,*) 'Error in startMPI :: mpi_comm_rank failed'
      stop 'startMPI'
    end if

10  format('[',I2,'] : number of processors = ',I3)
!    write (*,10) myrank,nProc

    !-- set the Psparse modules variables corresponding to myrank, nProc
    !call setProcInfo(nProc,myrank)
    
  end subroutine startMPI
! 
! =======================================================================
! 
  subroutine endMPI() 
! 
! -----------------------------------------------------------------------
! Purpose: calls mpi_finalize,
! -----------------------------------------------------------------------
! _______________________
! variable specification
    
    !-- local variables
    integer :: ierr
    
! _____________________
! begin main execution
    
    !-- finalize and check for problems
    call mpi_finalize(ierr)
    if (ierr /= MPI_SUCCESS) then
      write (*,*) 'Error in endMPI :: mpi_finalize failed'
      stop 'endMPI'
    end if

  end subroutine endMPI
! 
! =======================================================================
! 
  function nbrpos(stcl, pos)
! 
! -----------------------------------------------------------------------
! Purpose: Counts the number of positions occupied in the stencil (stcl)
!   up to, but not including, pos
! -----------------------------------------------------------------------

    integer :: nbrpos, pos
    integer(kind=int8) :: stcl
    integer :: i
    integer(kind=int8) :: tmp

    tmp = stcl
    nbrpos = 0
    do i=1,pos
      nbrpos = nbrpos + iand(tmp,1) 
      tmp = ishft(tmp,-1)
    end do    
  end function nbrpos
! 
! =======================================================================
! 
  function numbits(int, size)
! 
! -----------------------------------------------------------------------
! Purpose: Counts the number of bits in int set to 1 from position 0 to
!   position size-1; Yes, this is essentially the same as nbrpos, except
!   that the default integer is used
! -----------------------------------------------------------------------

    integer, intent(in) :: int, size
    integer :: numbits
    integer :: i, tmp

    tmp = int
    numbits = 0
    do i=1,size
      numbits = numbits + iand(tmp,1) 
      tmp = ishft(tmp,-1)
    end do    

  end function numbits
! 
! =======================================================================
! 
  pure function crossProd(x,y)
! 
! -----------------------------------------------------------------------
! Purpose: Cacluates the vector cross product
! -----------------------------------------------------------------------

    real(kind=dp), dimension(3) :: crossProd
    real(kind=dp), intent(in), dimension(3) :: x, y

    crossProd(1) = x(2)*y(3) - y(2)*x(3)
    crossProd(2) = x(3)*y(1) - y(3)*x(1)
    crossProd(3) = x(1)*y(2) - y(1)*x(2)

  end function crossProd
! 
! =======================================================================
! 
  pure subroutine getPrimitive(q,press,Jac,rho,u,v,w,e,p)
! 
! -----------------------------------------------------------------------
! Purpose: Finds the primitive variables based on the given transformed
!   conservative variables and pressure
! -----------------------------------------------------------------------

    real(kind=dp), intent(in) :: q(5), press, Jac
    real(kind=dp), intent(out) :: rho, u, v, w, e, p

    rho = q(1)
    u = q(2)/rho
    v = q(3)/rho
    w = q(4)/rho
    !rho = rho*Jac
    e = q(5) !*Jac
    p = press !*Jac

  end subroutine getPrimitive
! 
! =======================================================================
! 
  pure function getM(rho,u,v,w)
! 
! -----------------------------------------------------------------------
! Purpose: Returns the transformation matrix from conservative to
!   primitive variables
! -----------------------------------------------------------------------

    real(kind=dp), intent(in) :: rho, u, v, w
    real(kind=dp), dimension(5,5) :: getM
    real(kind=dp) :: fac

    fac = 1.d0/rho
    getM = 0.0d0
    getM(1,1) = 1.d0
    getM(2,1) = -u*fac
    getM(2,2) = fac
    getM(3,1) = -v*fac
    getM(3,3) = fac
    getM(4,1) = -w*fac
    getM(4,4) = fac
    getM(5,1) = .5d0*(u*u + v*v + w*w)*gami
    getM(5,2) = -u*gami
    getM(5,3) = -v*gami
    getM(5,4) = -w*gami
    getM(5,5) = gami

  end function getM
! 
! =======================================================================
! 
  pure function DpressDq(q) result(dpdq)
! 
! -----------------------------------------------------------------------
! Purpose: Returns the derivative of pressure with respect to q(1:5)
! -----------------------------------------------------------------------

    real(kind=dp), intent(in) :: q(5)
    real(kind=dp) :: dpdq(5)
    real(kind=dp) :: fac, u, v, w

    fac = 1.d0/q(1)
    u = q(2)*fac; v = q(3)*fac; w = q(4)*fac
    dpdq(1) = 0.5d0*(u*u + v*v + w*w)*gami
    dpdq(2) = -u*gami
    dpdq(3) = -v*gami
    dpdq(4) = -w*gami
    dpdq(5) = gami

  end function DpressDq
! 
! =======================================================================
! 
  pure function cmax(c1, c2) 
! 
! -----------------------------------------------------------------------
! Purpose: returns the complex number (c1 or c2) with the larger real
!   part
!
! Inputs:
!   c1, c2 = two complex numbers
! Outputs:
!   cmax = c1, if (real(c1) >= real(c2)), c2 otherwise
! 
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- function arguments
    complex(kind=dp), intent(in) :: c1, c2
    complex(kind=dp) :: cmax

! _____________________
! begin main execution 

    if (real(c1,kind=dp) >= real(c2,kind=dp)) then
      cmax = c1
    else
      cmax = c2
    end if

  end function cmax
! 
! =======================================================================
! 
  pure function cabs(c)
! 
! -----------------------------------------------------------------------
! Purpose: returns c if real(c) >= 0, returns -c otherwise
!
! Inputs:
!   c = complex number
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- function arguments
    complex(kind=dp), intent(in) :: c
    complex(kind=dp) :: cabs

! _____________________
! begin main execution 

    if (real(c,kind=dp) >= 0) then
      cabs = c
    else
      cabs = -c
    end if

  end function cabs
! 
! =======================================================================
! 
  pure function Btrans(i, N, m, r, s)
! 
! -----------------------------------------------------------------------
! Purpose: The smooth transition operator for the SBP dissipation 
!
! Inputs:
!   i = the node index ( i >= 1, i <= N)
!   N = the number of nodes ( N >= 1 )
!   m = the number of nodes in the transition region ( m >= 1, m <= N/2)
!   r = the value at the end point of Btrans is (1/(N-1))^r
!   s = the value of Btrans in the interior
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- function arguments
    integer, intent(in) :: i, N, m, r
    real(kind=dp), intent(in) :: s
    real(kind=dp) :: Btrans

    !-- local variables
    real(kind=dp) :: x, x2, trs, tmp, h

! _____________________
! begin main execution 

    tmp = 1.d0/real(N-1,dp)
    !-- symmetrize x with respect to 0.5
    x = 0.5d0 - abs(real(i-1,dp)*tmp - 0.5d0)
    trs = real(m-1,dp)*tmp
    if (x > trs) then  !-- integer comparison between i and m better?
      !-- if x is in the interior, Btrans = s
      Btrans = s
    else
      !-- if x is in the transition region, find the appropriate value
      h = tmp**r
      tmp = (1-h)/(trs*trs)
      x2 = x*x
      Btrans = h + 3.d0*tmp*x2 - 2.d0*tmp*x2*x/trs
    end if

  end function Btrans
! 
! =======================================================================
!   
end module Common

!
! =======================================================================
!
module EulerRHS_Mod
!
! -----------------------------------------------------------------------
! Purpose:
! Holds the routines used to calcuate the right-hand-side of Newton's
! method applied to the equations of motion. Note, some of these may
! be reused in the LHS_Mod.
!
! Author: J.E. Hicken, Apr 2006
! -----------------------------------------------------------------------

  use mpi
  use Common
  use Block_Mod
  use Interface_Mod
  use Boundary_Mod
  use IO_Mod

  implicit none

  !-- constants
  real(kind=dp), private, parameter :: &
       d0_0 = 0.d0, d0_25 = 0.25d0, d0_5 = 0.5d0, d1_0 = 1.d0, &
       d1_5 = 1.5d0, d2_0 = 2.d0
  !real(kind=dp), parameter :: &
  ! Vl = 0.025d0, &
  ! Vn = 0.025d0 !Vn = 0.025d0

! _______________________
! module subroutines
contains
!
! =======================================================================
!
  subroutine getEulerRHS(blk)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the inviscid fluxes and numerical dissipation for
! all nodes and accumulates these fluxes in s. The contravariant
! velocity (U), the spectral radii (spect) and, the dissipation
! coefficients (coef2, coef4) are also calculated for future use in
! getLHS.
!
! Pre: 1) blk%press and blk%sndsp are consistent with blk%q
! Post 1) the rhs based on blk%q (and blk%press, blk%sndsp) is in blk%s
! 2) blk%U, blk%spect, blk%coef2, and blk%coef4 are consistent with
! blk%q
!
! InOuts:
! blk = the block structure whose rhs is being calculated based on
! blk%q, blk%press, and blk%sndsp
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: di, ierr

! _____________________
! begin main execution

    !-- loop over each coordinate direction
    diloop : do di = 1,3

      !-- check for 2-D flow solve
      if (blk%jkmmax(di) == 1) cycle diloop

      !-- get the Euler fluxes and add them to blk%s
      call getEulerFluxesRHS(blk,di)

      !-- get the appropriate numerical dissipation
      if (diablo% idmodel == 1) then
        !-- use scalar dissipation model
        call getScalarDissRHS(blk,di)
      else if (diablo% idmodel == 2) then
        !-- use matrix dissipation model
        call getMatrixDissRHS(blk,di)
      else if (diablo% idmodel == 3) then
        !-- use SBP scalar dissipation
        call getSBPDissRHS(blk,di)
      else
        write(*,*) myrank,': Error in RHS_Mod:getEulerRHS :: ', &
             'invalid dissipation model.'
        call mpi_abort(MPI_COMM_WORLD,0,ierr)
      end if

    end do diloop

  end subroutine getEulerRHS
!
! =======================================================================
!
  subroutine getEulerFluxesRHS(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the inviscid fluxes for the flow given by blk%q,
! and adds these fluxes to blk%s.
!
! Pre: 1) blk%press and blk%sndsp are consistent with blk%q
! Post 1) the inviscid fluxes based on blk%q (and blk%press, blk%sndsp)
! are added to is in blk%s
! 2) blk%U, blk%spect, blk%coef2, and blk%coef4 are consistent with
! blk%q
!
! Inputs:
! di = the coordinate direction in which we want the fluxes.
!
! InOuts:
! blk = the block structure whose rhs is being calculated based on
! blk%q, blk%press, and blk%sndsp
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer, dimension(3) :: jkms, jkmf, bit, jkm
    integer :: j, k, m, jm, km, mm, jp, kp, mp, n
    integer :: it1, it2, jit1, jit2
    real(kind=dp) :: flux(5)

! _____________________
! begin main execution

    !-- find the index ranges
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    bit(:) = 0
    jkms(di) = jkms(di)+1
    jkmf(di) = jkmf(di)-1
    bit(di) = 1

    !-- calculate the flux at each internal node, and add/subtract it
    !-- from the appropriate nbrs
    do m = jkms(3), jkmf(3)
      mm = m - bit(3)
      mp = m + bit(3)
      do k = jkms(2), jkmf(2)
        km = k - bit(2)
        kp = k + bit(2)
        do j = jkms(1), jkmf(1)
          jm = j - bit(1)
          jp = j + bit(1)

          !-- if the complier is not inlining this, we should move
          !-- the code in here explicitly
          flux = getFlux(di,j,k,m)
          do n = 1,5
            blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - flux(n)
            blk%s(jm,km,mm,n) = blk%s(jm,km,mm,n) + flux(n)
          end do

        end do
      end do
    end do

    !-- add fluxes to get correct block side treatment
    it1 = mod(di,3)+1
    it2 = mod(di+1,3)+1
    do jit1 = jkms(it1), jkmf(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2)
        jkm(it2) = jit2

        !-- get the indices for the node at the low side in direction di,
        !-- and its internal nbr
        jkm(di) = 1
        j = jkm(1); k = jkm(2); m = jkm(3)
        jkm(di) = jkm(di)+1
        jp = jkm(1); kp = jkm(2); mp = jkm(3)

        !-- subtract the flux at the interior node, and subtract the
        !-- flux at the boundary/interface node; the factor of 2
        !-- accounts for the factor of 0.5 used in getFlux()
        flux = getFlux(di,j,k,m)
        do n = 1,5
          blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - flux(n)
          blk%s(j,k,m,n) = blk%s(j,k,m,n) - d2_0*flux(n)
        end do

        !-- the flux contribution from (jp,kp,mp) to (j,k,m) is not
        !-- correct due to the 0.5 factor; add some more
        flux = getFlux(di,jp,kp,mp)
        do n = 1,5
          blk%s(j,k,m,n) = blk%s(j,k,m,n) + flux(n)
        end do

        !-- get the indices for the node at the high side in direction
        !-- di, and its internal nbr
        jkm(di) = blk%jkmmax(di)
        j = jkm(1); k = jkm(2); m = jkm(3)
        jkm(di) = jkm(di) - 1
        jm = jkm(1); km = jkm(2); mm = jkm(3)

        !-- add the flux at the interior node, and add the flux at the
        !-- boundary/interface node; the factor of 2 accounts for the
        !-- factor of 0.5 used in getFlux()
        flux = getFlux(di,j,k,m)
        do n = 1,5
          blk%s(jm,km,mm,n) = blk%s(jm,km,mm,n) + flux(n)
          blk%s(j,k,m,n) = blk%s(j,k,m,n) + d2_0*flux(n)
        end do

        !-- the flux contribution from (jm,km,mm) to (j,k,m) is not
        !-- correct due to the 0.5 factor; subtract some more
        flux = getFlux(di,jm,km,mm)
        do n = 1,5
          blk%s(j,k,m,n) = blk%s(j,k,m,n) - flux(n)
        end do

      end do !-- jit2 loop
    end do !-- jit1 loop

  contains
    !--------------------------------
    function getFlux(di,j,k,m)
      !-- Purpose: returns the Euler flux in direction di at node
      !-- (j,k,m)
      integer, intent(in) :: di, j, k, m
      real(kind=dp) :: getFlux(5)

      !-- local variables
      real(kind=dp) :: dxidx, dxidy, dxidz, q1, q2, q3, q4, q5, fac, &
           u, v, w, Uxi, prs

      !-- get the metric terms for quick access; d0_5 term is for
      !-- the second-order accuracte derivative
      dxidx = d0_5*blk%dxidx(j,k,m,di,1)
      dxidy = d0_5*blk%dxidx(j,k,m,di,2)
      dxidz = d0_5*blk%dxidx(j,k,m,di,3)

      !-- get the flow variables for quick access
      q1 = blk%q(j,k,m,1)
      q2 = blk%q(j,k,m,2)
      q3 = blk%q(j,k,m,3)
      q4 = blk%q(j,k,m,4)
      q5 = blk%q(j,k,m,5)
      prs = blk%press(j,k,m)

      !-- calculate the contravariant velocity
      fac = 1.d0/q1
      u = q2*fac; v = q3*fac; w = q4*fac
      Uxi = u*dxidx + v*dxidy + w*dxidz !+ xit(j,k,m)
      blk%U(j,k,m,di) = d2_0*Uxi !-- need the factor of 2 to correct
                                 !-- the 0.5 factor in dxidx, etc

      !-- calculate the flux
      getFlux(1) = q1*Uxi
      getFlux(2) = q2*Uxi + dxidx*prs
      getFlux(3) = q3*Uxi + dxidy*prs
      getFlux(4) = q4*Uxi + dxidz*prs
      getFlux(5) = (q5 + prs)*Uxi !- xit(j,k,m)*pressj

    end function getFlux

  end subroutine getEulerFluxesRHS
!
! =======================================================================
!
  subroutine getScalarDissRHS(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the numerical dissipation in the direction di using
! the JST scalar dissipation scheme.
!
! Inputs:
! di = the coordiate direction on block blk for which we want to add
! dissipation to the right-hand-side array blk%s
!
! InOuts:
! blk = the block which we want to add dissipation to blk%s
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables

! _____________________
! begin main execution

    !-- calculate and store the spectral radius at each node; the result
    !-- is stored in blk%spect
    call spectralRadius(blk,di)

    if (.not. diablo% frz_coef24) then
      !-- calculate the smoothed pressure switch in direction di; the
      !-- result is stored in blk%pcoef
      call calcSmoothUpsilon(blk,di)

      !-- find the 2nd and 4th difference dissipation coefficients, which
      !-- are stored in blk%coef2 and blk%coef4, respectively
      call calcCoef24(blk,di)
    end if

    !-- find the scalar dissipation in direction di and add it to blk%s
    call calcScalarDiss(blk,di)

  end subroutine getScalarDissRHS
!
! =======================================================================
!
  subroutine getMatrixDissRHS(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the numerical dissipation in the direction di using
! the Swanson and Turkel maxtrix dissipation scheme.
!
! Inputs:
! di = the coordiate direction on block blk for which we want to add
! dissipation to the right-hand-side array blk%s
!
! InOuts:
! blk = the block which we want to add dissipation to blk%s
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables

! _____________________
! begin main execution

    if (.not. diablo% frz_coef24) then
      !-- calculate the smoothed pressure switch in direction di; the
      !-- result is stored in blk%pcoef
      call calcSmoothUpsilon(blk,di)

      !-- find the 2nd and 4th difference dissipation coefficients, which
      !-- are stored in blk%coef2 and blk%coef4, respectively
      call calcCoef24(blk,di)
    end if

    !-- find the matrix dissipation in direction di and add it to blk%s
    call calcMatrixDiss(blk,di)

  end subroutine getMatrixDissRHS
!
! =======================================================================
!
  subroutine spectralRadius(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the spectral radius for the flux Jacobian matrix
! in the coordinate direction di.
!
! Pre: 1) blk%U should hold the contravariant velocities (scaled by J)
! 2) blk%sndsp should hold the sound speed based on blk%q
! Post: 1) the spectral radius in direction di is stored in blk%spect
!
! Inputs:
! di = the direction of the flux Jacobian whose spectral radius is
! desired
!
! InOuts:
! blk = the block structure whose blk%spect component in direction di
! is being calculated
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: j, k, m
    real(kind=dp) :: dA

! _____________________
! begin main execution

    if (.not. debug) then

      do m = 1, blk%jkmmax(3)
        do k = 1, blk%jkmmax(2)
          do j = 1, blk%jkmmax(1)

            !-- get the norm of the gradient of dxi(di)dx(i); note,
            !-- the metric components are scaled by J (hence, they are
            !-- independent of J)
            dA = sqrt(blk%dxidx(j,k,m,di,1)*blk%dxidx(j,k,m,di,1) + &
                 blk%dxidx(j,k,m,di,2)*blk%dxidx(j,k,m,di,2) + &
                 blk%dxidx(j,k,m,di,3)*blk%dxidx(j,k,m,di,3))

            blk%spect(j,k,m,di) = abs(blk%U(j,k,m,di)) + &
                 blk%sndsp(j,k,m)*dA
          end do
        end do
      end do

    else

      !-- constant spect for debugging
      do m = 1, blk%jkmmax(3)
        do k = 1, blk%jkmmax(2)
          do j = 1, blk%jkmmax(1)

            dA = sqrt(blk%dxidx(j,k,m,di,1)*blk%dxidx(j,k,m,di,1) + &
                 blk%dxidx(j,k,m,di,2)*blk%dxidx(j,k,m,di,2) + &
                 blk%dxidx(j,k,m,di,3)*blk%dxidx(j,k,m,di,3))

            blk%spect(j,k,m,di) = dA

          end do
        end do
      end do

    end if

  end subroutine spectralRadius
!
! =======================================================================
!
  subroutine calcSmoothUpsilon(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates smoothed pressure switch (Upsilon) coefficient, in
! the direction di.
!
! Pre: 1) the pressure in blk%press is based on the flow in blk%q
! Post: 1) the pressure switch in direction di is stored in blk%pcoef
!
! Inputs:
! di = the direction of the switch
!
! InOuts:
! blk = the block structure whose pressure switch in direction di is
! wanted.
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), bit(3)
    integer :: j, k, m, jm, km, mm, jp, kp, mp
    real(kind=dp) :: r1, r2

! _____________________
! begin main execution

    if (.not. diablo% p_switch) then
      !-- constant pcoef of 1.0
      blk%pcoef(:,:,:) = d1_0
      return
    end if

    !--------------------------------
    !-- initialize pcoef
    blk%pcoef(:,:,:) = d0_0

    !-- return if 2nd-difference coefficient is zero
    if (diablo% dis2(di) == 0.d0) return

    !-- find Upsilon
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    bit(:) = 0
    bit(di) = 1
    jkms(di) = 2
    jkmf(di) = jkmf(di)-1
    do m = jkms(3), jkmf(3)
      mm = m - bit(3)
      mp = m + bit(3)
      do k = jkms(2), jkmf(2)
        km = k - bit(2)
        kp = k + bit(2)
        do j = jkms(1), jkmf(1)
          jm = j - bit(1)
          jp = j + bit(1)

          r1 = blk%press(jp,kp,mp) - d2_0*blk%press(j,k,m) + &
               blk%press(jm,km,mm)
          r2 = blk%press(jp,kp,mp) + d2_0*blk%press(j,k,m) + &
               blk%press(jm,km,mm)
          blk%pcoef(j,k,m) = abs(r1/r2)
        end do
      end do
    end do

    !-- deal with Upsilon at boundaries
    call copyBlockSides(di,blk,1,1,blk%pcoef)

    !--------------------------------
    !-- smooth out pressure coefficient (store in work(:,:,:,1))
    do m = jkms(3), jkmf(3)
      mm = m - bit(3)
      mp = m + bit(3)
      do k = jkms(2), jkmf(2)
        km = k - bit(2)
        kp = k + bit(2)
        do j = jkms(1), jkmf(1)
          jm = j - bit(1)
          jp = j + bit(1)

          blk%work(j,k,m,1) = max(blk%pcoef(jm,km,mm), &
               blk%pcoef(j,k,m),blk%pcoef(jp,kp,mp))
        end do
      end do
    end do

    !-- deal with smoothed coefficient at boundaries
    call copyBlockSides(di,blk,MAX_WRK,1,blk%work)

    !--------------------------------
    !-- filter the smoothed pressure coefficient (store in pcoef)
    do m = jkms(3), jkmf(3)
      mm = m - bit(3)
      mp = m + bit(3)
      do k = jkms(2), jkmf(2)
        km = k - bit(2)
        kp = k + bit(2)
        do j = jkms(1), jkmf(1)
          jm = j - bit(1)
          jp = j + bit(1)

          blk%pcoef(j,k,m) = d0_5*blk%work(j,k,m,1) + &
               d0_25*(blk%work(jm,km,mm,1) + blk%work(jp,kp,mp,1))
        end do
      end do
    end do

    !-- deal with filtered-smoothed coefficient at boundaries
    call copyBlockSides(di,blk,1,1,blk%pcoef)

  end subroutine calcSmoothUpsilon
!
! =======================================================================
!
  subroutine calcCoef24(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the second- and fourth-difference dissipation
! coefficients at the half nodes; the half node j+1/2 is stored at j.
!
! Pre: 1) the pressure coefficient (if needed) is in blk%pcoef
! Post: 1) the 2nd- and 4th-difference dissipation coefficients are
! stored in blk%coef2 and blk%coef4
!
! Inputs:
! di = the direction of the desired dissipation coefficients
!
! InOuts:
! blk = the block structure for which we are calculating the
! dissipation coefficients in direction di
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), bit(3)
    integer :: j, k, m, jp, kp, mp
    real(kind=dp) :: c2, c4

! _____________________
! begin main execution

    !-- find the index ranges; note, there are only jkmmax(di)-1 half
    !-- nodes in direction di
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    bit(:) = 0
    jkmf(di) = jkmf(di) - 1
    bit(di) = 1

    do m = jkms(3), jkmf(3)
      mp = m + bit(3)
      do k = jkms(2), jkmf(2)
        kp = k + bit(2)
        do j = jkms(1), jkmf(1)
          jp = j + bit(1)

          c2 = diablo% dis2(di)*(blk%pcoef(jp,kp,mp) + blk%pcoef(j,k,m))
          c4 = diablo% dis4(di)
          c4 = max(d0_0,c4-c2)

          blk%coef2(j,k,m,di) = c2
          blk%coef4(j,k,m,di) = c4

        end do
      end do
    end do

    !-- we could copy the internal values at jkmmax(di)-1 into the nodes
    !-- at jkmmax(di), but these values are never needed

  end subroutine calcCoef24
!
! =======================================================================
!
  subroutine calcScalarDiss(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the second- and fouth-difference scalar dissipation
! terms in the direction di, based on the coefficients coef2, coef4,
! and spect.
!
! Pre: 1) the flux Jacobian spectral radius is in blk%spect
! 2) the 2nd- and 4th-difference dissipation coefficients are in
! blk%coef2 and blk%coef4
!
! Inputs:
! di = the coordinate direction for the dissipation
!
! InOuts:
! blk = the block structure whose equations we want to add dissipation
! to.
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), jkm(3)
    integer :: j, k, m, jp, kp, mp, n
    real(kind=dp) :: spect, c4, c2, diff4, diff2, diss

! _____________________
! begin main execution

    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di)-1

    !-- loop over conservative variables; note, we use 5 instead of nVar
    !-- in case additional variables should not have dissipation
    do n = 1,5

      !-- find the forward-difference at the half-nodes, and the
      !-- 2nd-difference the nodes; the forward diffenence is stored in
      !-- blk%work(:,:,:,1), and the 2nd-difference in blk%work(:,:,:,2)
      call calcDissDifferences(blk,di,n)

      !-- calculate the scalar dissipation contributions
      do m = jkms(3), jkmf(3)
        do k = jkms(2), jkmf(2)
          do j = jkms(1), jkmf(1)
            jkm = (/ j, k, m /)
            jkm(di) = jkm(di) + 1
            jp = jkm(1); kp = jkm(2); mp = jkm(3)

            spect = d0_5*(blk%spect(j,k,m,di) + blk%spect(jp,kp,mp,di))
            c4 = blk%coef4(j,k,m,di)
            c2 = blk%coef2(j,k,m,di)
            diff4 = blk%work(jp,kp,mp,2) - blk%work(j,k,m,2)
            diff2 = blk%work(j,k,m,1)
            !-- calculate the dissipation contribution at the half node
            diss = spect*(c4*diff4 - c2*diff2)

            !-- add/subtract the dissipation contribution
            blk%s(j,k,m,n) = blk%s(j,k,m,n) + diss
            blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - diss

          end do
        end do
      end do

    end do !-- n loop

  end subroutine calcScalarDiss
!
! =======================================================================
!
  subroutine calcDissDifferences(blk, di, n)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the forward-difference of the nth flow variable in
! the direction di, and stores the result in blk%work(:,:,:,1). The
! forward differences are relevant only for nodes 1,2,..jkmmax(di)-1.
! Also, Calculates the second-difference of the nth flow variable and
! stores the result in blk%work(:,:,:,2).
!
! Post: the forward- and second-differences are stored in
! blk%work(:,:,:,1) and blk%work(:,:,:,2), respectively.
!
! Inputs:
! di = the coordinate direction for the dissipation
! n = the variable of blk%q that we want the differences
!
! InOuts:
! blk = the block structure whose forward and second difference are
! being calculated
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di, n

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), jkm(3)
    integer :: j, k, m, jp, kp, mp, it1, it2, jit1, jit2
    real(kind=dp) :: dq

! _____________________
! begin main execution

    !-- initialize work(:,:,:,1) and work(:,:,:,2)
    blk%work(:,:,:,1) = d0_0
    blk%work(:,:,:,2) = d0_0

    !-- calculate the first-order difference on the internal faces, and
    !-- add the contribution to the second-difference
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di) - 1
    do m = jkms(3), jkmf(3)
      do k = jkms(2), jkmf(2)
        do j = jkms(1), jkmf(1)
          jkm = (/ j, k, m /)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)

          dq = blk%q(jp,kp,mp,n) - blk%q(j,k,m,n)
          blk%work(j,k,m,1) = dq
          blk%work(j,k,m,2) = blk%work(j,k,m,2) + dq
          blk%work(jp,kp,mp,2) = blk%work(jp,kp,mp,2) - dq

        end do
      end do
    end do

    !-- at block ends, the second-difference is set to zero
    it1 = mod(di,3)+1
    it2 = mod(di+1,3)+1
    do jit1 = jkms(it1), jkmf(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2)
        jkm(it2) = jit2

        !-- low end
        jkm(di) = 1
        blk%work(jkm(1),jkm(2),jkm(3),2) = d0_0

        !-- high end
        jkm(di) = blk%jkmmax(di)
        blk%work(jkm(1),jkm(2),jkm(3),2) = d0_0

      end do
    end do

  end subroutine calcDissDifferences
!
! =======================================================================
!
  subroutine calcMatrixDiss(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the second- and fouth-difference matrix dissipation
! terms in the direction di, based on the coefficients coef2 and coef4.
!
! Pre: 1) the 2nd- and 4th-difference dissipation coefficients are in
! blk%coef2 and blk%coef4
!
! Inputs:
! di = the coordinate direction for the dissipation
!
! InOuts:
! blk = the block structure whose equations we want to add dissipation
! to.
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), jkm(3)
    integer :: j, k, m, jp, kp, mp, n
    real(kind=dp) :: c4, c2, diss(5), dq(5), qavg(5), dxidx(3)

! _____________________
! begin main execution

    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di)-1

    !-- find the forward-difference at the half-nodes, and the
    !-- 2nd-difference at the nodes
    call calcMatrixDissDiffs(blk,di)

    !-- calculate the matrix dissipation contributions, by looping over
    !-- the half-nodes
    do m = jkms(3), jkmf(3)
      do k = jkms(2), jkmf(2)
        do j = jkms(1), jkmf(1)
          jkm = (/ j, k, m /)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)

          !-- find the matrix dissipation term at the half node
          !-- jkm(di) + 1/2
          c4 = blk%coef4(j,k,m,di)
          c2 = blk%coef2(j,k,m,di)
          dq = c4*(blk%work(jp,kp,mp,6:10) - blk%work(j,k,m,6:10)) &
               - c2*blk%work(j,k,m,1:5)
          qavg = d0_5*(blk%q(jp,kp,mp,1:5) + blk%q(j,k,m,1:5))
          dxidx = d0_5*(blk%dxidx(jp,kp,mp,di,:) + blk%dxidx(j,k,m,di,:))
          call matDiss(dxidx,qavg,dq,diss)

          !-- add/subtract the dissipation to/from the adjacent nodes
          blk%s(j,k,m,1:5) = blk%s(j,k,m,1:5) + diss(:)
          blk%s(jp,kp,mp,1:5) = blk%s(jp,kp,mp,1:5) - diss(:)

        end do
      end do
    end do

  end subroutine calcMatrixDiss
!
! =======================================================================
!
  subroutine calcMatrixDissDiffs(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the forward-difference of the flow variables in the
! direction di, and stores the result in blk%work(:,:,:,1:5). The
! forward differences are relevant only for nodes 1,2,..jkmmax(di)-1.
! Also, Calculates the second-difference of the flow variables and
! stores the result in blk%work(:,:,:,6:10).
!
! Post: the forward- and second-differences are stored in
! blk%work(:,:,:,1:5) and blk%work(:,:,:,6:10), respectively.
!
! Inputs:
! di = the coordinate direction for the dissipation
!
! InOuts:
! blk = the block structure whose variables are being forward and
! second-difference differenced (nice gammar, huh)
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), jkm(3)
    integer :: j, k, m, jp, kp, mp, it1, it2, jit1, jit2
    real(kind=dp) :: dq(5)

! _____________________
! begin main execution

    !-- initialize work(:,:,:,1:10)
    blk%work(:,:,:,1:10) = d0_0

    !-- calculate the first-order difference on the internal faces, and
    !-- add the contribution to the second-difference
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di) - 1
    do m = jkms(3), jkmf(3)
      do k = jkms(2), jkmf(2)
        do j = jkms(1), jkmf(1)
          jkm = (/ j, k, m /)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)

          dq = blk%q(jp,kp,mp,:) - blk%q(j,k,m,:)
          blk%work(j,k,m,1:5) = dq
          blk%work(j,k,m,6:10) = blk%work(j,k,m,6:10) + dq
          blk%work(jp,kp,mp,6:10) = blk%work(jp,kp,mp,6:10) - dq

        end do
      end do
    end do

    !-- at the block ends, the second-difference is set to zero
    it1 = mod(di,3)+1
    it2 = mod(di+1,3)+1
    do jit1 = jkms(it1), jkmf(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2)
        jkm(it2) = jit2

        !-- low end
        jkm(di) = 1
        blk%work(jkm(1),jkm(2),jkm(3),6:10) = d0_0

        !-- high end
        jkm(di) = blk%jkmmax(di)
        blk%work(jkm(1),jkm(2),jkm(3),6:10) = d0_0

      end do
    end do

  end subroutine calcMatrixDissDiffs
!
! =======================================================================
!
  subroutine matDiss(dxidx, q, dq, diss)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the matrix dissipation terms based on the state
! q (assumed to be some sort of averaged state), the differences dq,
! and the metric terms dxidx (assumed to be independent of J)
! Note: this implementation is based on the matrix dissipation model
! of Swanson and Turkel.
!
! Inputs:
! dxidx = the metric terms in the desired direction (indpendent of J)
! at the half node
! q = the averaged flow state at the half node
! dq = flow variables differences (first and third difference)
!
! Outs:
! diss = the matrix dissipation terms
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    real(kind=dp), intent(in) :: dxidx(3), q(5), dq(5)

    real(kind=dp), intent(out) :: diss(5)

    !-- local variables
    real(kind=dp) :: nx, ny, nz, dA, q1, q2, q3, q4, q5, fac, press, a, &
         u, v, w, Un, lambda1, lambda2, lambda3, rhoA, phi, H, &
         dq1, dq2, dq3, dq4, dq5, tmp1, tmp2, tmp3
    real(kind=dp) :: E1dq(5), E2dq(5)

! _____________________
! begin main execution

    !-- get some flow and geometric quantities for quick reference
    nx = dxidx(1); ny = dxidx(2); nz = dxidx(3)
    dA = sqrt(nx*nx + ny*ny + nz*nz)

    q1 = q(1); q2 = q(2); q3 = q(3); q4 = q(4); q5 = q(5)
    fac = d1_0/q1
    press = gami*(q5 - d0_5*(q2*q2 + q3*q3 + q4*q4)*fac)
    a = sqrt(gamma*press*fac)
    u = q2*fac; v = q3*fac; w = q4*fac
    Un = u*nx + v*ny + w*nz

    lambda1 = Un + dA*a
    lambda2 = Un - dA*a
    lambda3 = Un
    rhoA = abs(Un) + dA*a
    lambda1 = max(abs(lambda1),Vn*rhoA)
    lambda2 = max(abs(lambda2),Vn*rhoA)
    lambda3 = max(abs(lambda3),Vl*rhoA)

    phi = d0_5*(u*u + v*v + w*w)
    H = (q5 + press)*fac

    dq1 = dq(1); dq2 = dq(2); dq3 = dq(3); dq4 = dq(4); dq5 = dq(5)

    !-- diagonal matrix multiply
    diss(1) = lambda3*dq1
    diss(2) = lambda3*dq2
    diss(3) = lambda3*dq3
    diss(4) = lambda3*dq4
    diss(5) = lambda3*dq5

    !-- get E1*dq
    E1dq(1) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E2*dq
    E2dq(1) = d0_0
    E2dq(2) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to diss
    tmp1 = d0_5*(lambda1 + lambda2) - lambda3
    tmp2 = gami/(a*a)
    tmp3 = d1_0/(dA*dA)
    diss(:) = diss(:) + tmp1*(tmp2*E1dq(:) + tmp3*E2dq(:))

    !-- get E3*dq
    E1dq(1) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E4*dq
    E2dq(1) = d0_0
    E2dq(2) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to diss
    tmp1 = d0_5*(lambda1 - lambda2)/(dA*a)
    diss(:) = diss(:) + tmp1*(E1dq(:) + gami*E2dq(:))

  end subroutine matDiss
!
! =======================================================================
!
  subroutine getSBPDissRHS(blk, di)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the second- and fouth-difference SBP dissipation
! terms in the direction di
!
! Inputs:
! di = the coordinate direction for the dissipation
!
! InOuts:
! blk = the block structure whose equations we want to add dissipation
! to.
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: it1, it2, jdi, jit1, jit2
    integer :: jkms(3), jkmf(3), jkm(3)
    integer :: j, k, m, jp, kp, mp, n
    real(kind=dp) :: B, spect, c4, c2, diff4, diff2, diss

! _____________________
! begin main execution

    it1 = mod(di,3) + 1
    it2 = mod(di+1,3) + 1
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di)-1

    !-- loop over conservative variables; note, we use 5 instead of nVar
    !-- in case additional variables should not have dissipation
    do n = 1,5

      !-- find the forward-difference at the half-nodes, and the
      !-- 2nd-difference the nodes; the forward diffenence is stored in
      !-- blk%work(:,:,:,1), and the 2nd-difference in blk%work(:,:,:,2)
      call calcSBPDissDiffs(blk,di,n)

      !-- apply the diagonal transition operator
      do jdi = 1, blk%jkmmax(di)
        jkm(jdi) = jdi
        B = Btrans(jdi,blk%jkmmax(di),5,1,diablo% dis4(1)) !1.d0)
        do jit1 = 1, blk%jkmmax(it1)
          jkm(it1) = jit1
          do jit2 = 1, blk%jkmmax(it2)
            jkm(it2) = jit2

            j = jkm(1); k = jkm(2); m = jkm(3)
            blk%work(j,k,m,2) = B*blk%work(j,k,m,2)
          end do
        end do
      end do

      !-- calculate the dissipation contributions in the interior
      do m = jkms(3), jkmf(3)
        do k = jkms(2), jkmf(2)
          do j = jkms(1), jkmf(1)
            jkm = (/ j, k, m /)
            jkm(di) = jkm(di) + 1
            jp = jkm(1); kp = jkm(2); mp = jkm(3)

            diff4 = blk%work(jp,kp,mp,2) - blk%work(j,k,m,2)

            !-- calculate the dissipation contribution at the half node
            diss = diff4

            !-- add/subtract the dissipation contribution
            blk%s(j,k,m,n) = blk%s(j,k,m,n) + diss
            blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - diss

          end do
        end do
      end do

      !-- add corrections at the boundaries
      do jit1 = jkms(it1), jkmf(it1)
        jkm(it1) = jit1
        do jit2 = jkms(it2), jkmf(it2)
          jkm(it2) = jit2

          !-- low end
          jkm(di) = 1
          j = jkm(1); k = jkm(2); m = jkm(3)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)

          blk%s(j,k,m,n) = blk%s(j,k,m,n) + 3.d0*blk%work(j,k,m,2) + &
               blk%work(jp,kp,mp,2)
          blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - 3.d0*blk%work(j,k,m,2)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)
          blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) + blk%work(j,k,m,2)

          !-- high end
          jkm(di) = blk%jkmmax(di)
          j = jkm(1); k = jkm(2); m = jkm(3)
          jkm(di) = jkm(di) - 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)
          blk%s(j,k,m,n) = blk%s(j,k,m,n) + 3.d0*blk%work(j,k,m,2) + &
               blk%work(jp,kp,mp,2)
          blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) - 3.d0*blk%work(j,k,m,2)
          jkm(di) = jkm(di) - 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)
          blk%s(jp,kp,mp,n) = blk%s(jp,kp,mp,n) + blk%work(j,k,m,2)

        end do
      end do

    end do !-- n loop

  end subroutine getSBPDissRHS
!
! =======================================================================
!
  subroutine calcSBPDissDiffs(blk, di, n)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the forward-difference of the nth flow variable in
! the direction di, and stores the result in blk%work(:,:,:,1). The
! forward differences are relevant only for nodes 1,2,..jkmmax(di)-1.
! Also, Calculates the second-difference of the nth flow variable and
! stores the result in blk%work(:,:,:,2).
!
! Post: the forward- and second-differences are stored in
! blk%work(:,:,:,1) and blk%work(:,:,:,2), respectively.
!
! Inputs:
! di = the coordinate direction for the dissipation
! n = the variable of blk%q that we want the differences
!
! InOuts:
! blk = the block structure whose forward and second difference are
! being calculated
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    integer, intent(in) :: di, n

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: jkms(3), jkmf(3), jkm(3), jkm1(3)
    integer :: j, k, m, jp, kp, mp, it1, it2, jit1, jit2
    real(kind=dp) :: dq

! _____________________
! begin main execution

    !-- initialize work(:,:,:,1) and work(:,:,:,2)
    blk%work(:,:,:,1) = d0_0
    blk%work(:,:,:,2) = d0_0

    !-- calculate the first-order difference on the internal faces, and
    !-- add the contribution to the second-difference
    jkms(:) = 1
    jkmf(:) = blk%jkmmax(:)
    jkmf(di) = jkmf(di) - 1
    do m = jkms(3), jkmf(3)
      do k = jkms(2), jkmf(2)
        do j = jkms(1), jkmf(1)
          jkm = (/ j, k, m /)
          jkm(di) = jkm(di) + 1
          jp = jkm(1); kp = jkm(2); mp = jkm(3)

          dq = blk%q(jp,kp,mp,n) - blk%q(j,k,m,n)
          blk%work(j,k,m,1) = dq
          blk%work(j,k,m,2) = blk%work(j,k,m,2) + dq
          blk%work(jp,kp,mp,2) = blk%work(jp,kp,mp,2) - dq

        end do
      end do
    end do

    !-- at block ends, the second-difference is set to the interior
    !-- neighbour's value
    it1 = mod(di,3)+1
    it2 = mod(di+1,3)+1
    do jit1 = jkms(it1), jkmf(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2)
        jkm(it2) = jit2

        !-- low end
        jkm(di) = 1
        jkm1 = jkm
        jkm1(di) = 2
        blk%work(jkm(1),jkm(2),jkm(3),2) = &
             blk%work(jkm1(1),jkm1(2),jkm1(3),2)

        !-- high end
        jkm(di) = blk%jkmmax(di)
        jkm1 = jkm
        jkm1(di) = jkm1(di) - 1
        blk%work(jkm(1),jkm(2),jkm(3),2) = &
             blk%work(jkm1(1),jkm1(2),jkm1(3),2)

      end do
    end do

  end subroutine calcSBPDissDiffs
!
! =======================================================================
!
  subroutine getInternalSATRHS(iface, blk1, blk2)
!
! -----------------------------------------------------------------------
! Purpose: Adds the SAT interface terms on the interface iface to the
! local blocks, blk1 and blk2, that border the interface.
!
! Inputs:
! iface = an interface structure which specifies the subface info
!
! InOuts:
! blk1, blk2 = the two process-local block structures to which the SAT
! terms are being added to the interface nodes
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Interface), intent(in) :: iface

    type(Block), intent(inout) :: blk1, blk2

    !-- local variables
    integer :: ierr, di1, it11, it12, di2, it21, it22, jit1, jit2
    integer :: j1, k1, m1, j2, k2, m2
    integer, dimension(3) :: bit1, jkm1, bit2, jkm2
    real(kind=dp) :: q1(5), q2(5), dxidx(3), sgn1, sgn2, sat(5)

! _____________________
! begin main execution

    !-- check that iface is an internal interface
    if (iface%isExternal) then
      write(*,*) myrank, &
           ':Error in EulerRHS_Mod:getInternalSATRHS :: ', &
           'iface is external.'
      call mpi_abort(MPI_COMM_WORLD,0,ierr)
    end if

    !-- find the normal direction index for blk1
    di1 = (iface%side(1)+1)/2
    if (mod(iface%side(1),2) == 1) then
      jkm1(di1) = 1
      bit1(di1) = 1
      sgn1 = d1_0
    else
      jkm1(di1) = blk1%jkmmax(di1)
      bit1(di1) = -1
      sgn1 = -d1_0
    end if

    !-- find the normal direction index for blk2
    di2 = (iface%side(2)+1)/2
    if (mod(iface%side(2),2) == 1) then
      jkm2(di2) = 1
      bit2(di2) = 1
      sgn2 = d1_0
    else
      jkm2(di2) = blk2%jkmmax(di2)
      bit2(di2) = -1
      sgn2 = -d1_0
    end if

    !-- find the first tangential direction information
    it11 = abs(iface%dit1(1,1))
    it21 = abs(iface%dit2(1,1))

    !-- find the second tangential direction information
    it12 = abs(iface%dit1(2,1))
    it22 = abs(iface%dit2(2,1))

    do jit1 = 0, iface%dit1(1,3)-1
      jkm1(it11) = iface%dit1(1,2) + sign(jit1,iface%dit1(1,1))
      jkm2(it21) = iface%dit2(1,2) + sign(jit1,iface%dit2(1,1))
      do jit2 = 0, iface%dit1(2,3)-1
        jkm1(it12) = iface%dit1(2,2) + sign(jit2,iface%dit1(2,1))
        jkm2(it22) = iface%dit2(2,2) + sign(jit2,iface%dit2(2,1))

        j1 = jkm1(1); k1 = jkm1(2); m1 = jkm1(3)
        j2 = jkm2(1); k2 = jkm2(2); m2 = jkm2(3)

        q1 = blk1%q(j1,k1,m1,1:5)
        q2 = blk2%q(j2,k2,m2,1:5)

        !-- get the SATs for the variables of block 1
        dxidx = blk1%dxidx(j1,k1,m1,di1,:)
        call eulerSAT(sgn1,dxidx,q1,q2,sat)

        !-- add the SAT terms to the node on block 1
        !-- the factor of 2 is for the diagonal norm
        blk1%s(j1,k1,m1,1:5) = blk1%s(j1,k1,m1,1:5) + d2_0*sat(1:5)

        !-- get the SATs for the variables of block2
        dxidx = blk2%dxidx(j2,k2,m2,di2,:)
        call eulerSAT(sgn2,dxidx,q2,q1,sat)

        !-- add the SAT terms to the node on block 2
        !-- the factor of 2 is for the diagonal norm
        blk2%s(j2,k2,m2,1:5) = blk2%s(j2,k2,m2,1:5) + d2_0*sat(1:5)

      end do
    end do

  end subroutine getInternalSATRHS
!
! =======================================================================
!
  subroutine getExternalSATRHS(iface, blk, gblk)
!
! -----------------------------------------------------------------------
! Purpose: Adds the SAT interface terms on the interface iface to the
! process-local block blk.
!
! Inputs:
! iface = an interface structure which specifies the subface info
! gblk = the external ghost block on the given interface
!
! InOuts:
! blk = the block structure to which the SAT terms are being added on
! the nodes cooresponding to iface
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Interface), intent(in) :: iface
    type(Block), intent(in) :: gblk

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: ierr, di, gdi, it1, git1, it2, git2, jit1, jit2
    integer :: jkmi(3), bit(3), jkmg(3), gbit(3)
    integer :: j, k, m, jg, kg, mg
    real(kind=dp) :: q(5), qg(5), qf(5), dq(5), dxidx(3), sgn, sat(5)

! _____________________
! begin main execution

    !-- check that iface is an external interface
    if (.not. iface%isExternal) then
      write(*,*) myrank, &
           ':Error in EulerRHS_Mod:getExternalSATRHS :: ', &
           'iface is not external.'
      call mpi_abort(MPI_COMM_WORLD,0,ierr)
    end if

    !-- find the normal direction index for the local block
    di = (iface%side(1)+1)/2
    if (mod(iface%side(1),2) == 1) then
      jkmi(di) = 1
      bit(di) = 1
      sgn = d1_0
    else
      jkmi(di) = blk%jkmmax(di)
      bit(di) = -1
      sgn = -d1_0
    end if

    !-- find the normal direction index for the ghost block
    gdi = (iface%side(2)+1)/2
    if (mod(iface%side(2),2) == 1) then
      jkmg(gdi) = 1
      gbit(gdi) = 1
    else
      jkmg(gdi) = gblk%jkmmax(gdi)
      gbit(gdi) = -1
    end if

    !-- find the first tangential direction information
    it1 = abs(iface%dit1(1,1))
    git1 = abs(iface%dit2(1,1))

    !-- find the second tangential direction information
    it2 = abs(iface%dit1(2,1))
    git2 = abs(iface%dit2(2,1))

    !-- loop over the nodes on this interface
    do jit1 = 0, iface%dit1(1,3)-1
      jkmi(it1) = iface%dit1(1,2) + sign(jit1,iface%dit1(1,1))
      jkmg(git1) = iface%dit2(1,2) + sign(jit1,iface%dit2(1,1))
      do jit2 = 0, iface%dit1(2,3)-1
        jkmi(it2) = iface%dit1(2,2) + sign(jit2,iface%dit1(2,1))
        jkmg(git2) = iface%dit2(2,2) + sign(jit2,iface%dit2(2,1))

        j = jkmi(1); k = jkmi(2); m = jkmi(3)
        jg = jkmg(1); kg = jkmg(2); mg = jkmg(3)

        q = blk%q(j,k,m,1:5)
        qg = gblk%q(jg,kg,mg,1:5)
        dxidx = blk%dxidx(j,k,m,di,:)
        call eulerSAT(sgn,dxidx,q,qg,sat)

        !-- add the SAT terms; the factor of 2 is for the diagonal norm
        blk%s(j,k,m,1:5) = blk%s(j,k,m,1:5) + d2_0*sat(1:5)

      end do
    end do

  end subroutine getExternalSATRHS
!
! =======================================================================
!
  subroutine eulerSAT(sgn, dxidx, q, qg, sat)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the (Euler) SAT interface terms based on the states
! q and qg, and the metric terms dxidx (assumed to be independent of J)
! Note: this implementation is based closely on the matrix dissipation
! model of Swanson and Turkel.
!
! Inputs:
! sgn = +1 if interface is at low side of block, -1 otherwise
! dxidx = the metric terms in the desired direction (indpendent of J)
! q = the flow state of the "local" node
! qg = the flow state of the "ghost" node
!
! Outs:
! sat = the Euler SAT interface penalty term
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    real(kind=dp), intent(in) :: sgn, dxidx(3), q(5), qg(5)

    real(kind=dp), intent(out) :: sat(5)

    !-- local variables
    real(kind=dp) :: nx, ny, nz, dA, q1, q2, q3, q4, q5, fac, press, a, &
         u, v, w, Un, lambda1, lambda2, lambda3, rhoA, phi, H, &
         dq1, dq2, dq3, dq4, dq5, tmp1, tmp2, tmp3
    real(kind=dp) :: E1dq(5), E2dq(5)

! _____________________
! begin main execution

    !-- get some flow and geometric quantities for quick reference
    nx = dxidx(1); ny = dxidx(2); nz = dxidx(3)
    dA = sqrt(nx*nx + ny*ny + nz*nz)

    q1 = d0_5*(q(1) + qg(1))
    q2 = d0_5*(q(2) + qg(2))
    q3 = d0_5*(q(3) + qg(3))
    q4 = d0_5*(q(4) + qg(4))
    q5 = d0_5*(q(5) + qg(5))
    fac = 1.d0/q1
    press = gami*(q5 - 0.5*(q2*q2 + q3*q3 + q4*q4)*fac)
    a = sqrt(gamma*press*fac)
    u = q2*fac; v = q3*fac; w = q4*fac
    Un = u*nx + v*ny + w*nz

    lambda1 = Un + dA*a
    lambda2 = Un - dA*a
    lambda3 = Un
    rhoA = abs(Un) + dA*a
    lambda1 = d0_5*(max(abs(lambda1),Vn*rhoA) + sgn*lambda1)
    lambda2 = d0_5*(max(abs(lambda2),Vn*rhoA) + sgn*lambda2)
    lambda3 = d0_5*(max(abs(lambda3),Vl*rhoA) + sgn*lambda3)

    phi = 0.5d0*(u*u + v*v + w*w)
    H = (q5 + press)*fac

    dq1 = q(1) - qg(1)
    dq2 = q(2) - qg(2)
    dq3 = q(3) - qg(3)
    dq4 = q(4) - qg(4)
    dq5 = q(5) - qg(5)

    !-- diagonal matrix multiply
    sat(1) = lambda3*dq1
    sat(2) = lambda3*dq2
    sat(3) = lambda3*dq3
    sat(4) = lambda3*dq4
    sat(5) = lambda3*dq5

    !-- get E1*dq
    E1dq(1) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E2*dq
    E2dq(1) = 0.d0
    E2dq(2) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to sat
    tmp1 = 0.5d0*(lambda1 + lambda2) - lambda3
    tmp2 = gami/(a*a)
    tmp3 = 1.d0/(dA*dA)
    sat(:) = sat(:) + tmp1*(tmp2*E1dq(:) + tmp3*E2dq(:))

    !-- get E3*dq
    E1dq(1) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E4*dq
    E2dq(1) = 0.d0
    E2dq(2) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to sat
    tmp1 = 0.5d0*(lambda1 - lambda2)/(dA*a)
    sat(:) = sat(:) + tmp1*(E1dq(:) + gami*E2dq(:))

  end subroutine eulerSAT
!
! =======================================================================
!
  subroutine getBCFarRHS(bface, blk, qfs)
!
! -----------------------------------------------------------------------
! Purpose: Adds the SAT terms for a far-field boundary to the nodes on
! the given boundary subface.
!
! Inputs:
! bface = an boundary structure which specifies the subface info
! qfs = the free-stream conservative variables
!
! InOuts:
! blk = a block structure whose nodes on bface are getting SAT terms
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Boundary), intent(in) :: bface
    real(kind=dp), intent(in) :: qfs(5)

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: di, it1, it2, jit1, jit2, j, k, m
    integer :: jkm(3), jkms(3), jkmf(3), bit(3)
    real(kind=dp) :: q(5), dq(5), dxidx(3), sgn, sat(5)

! _____________________
! begin main execution

    !-- find the normal direction index and bit
    di = (bface%side+1)/2
    if (mod(bface%side,2) == 1) then
      jkm(di) = 1
      bit(di) = 1
      sgn = d1_0
    else
      jkm(di) = blk%jkmmax(di)
      bit(di) = -1
      sgn = -d1_0
    end if

    !-- find the first tangential direction information
    it1 = abs(bface%dit(1,1))
    bit(it1) = sign(1,bface%dit(1,1))
    jkms(it1) = bface%dit(1,2)
    jkmf(it1) = jkms(it1) + sign(bface%dit(1,3)-1,bit(it1))

    !-- find the second tangential direction information
    it2 = abs(bface%dit(2,1))
    bit(it2) = sign(1,bface%dit(2,1))
    jkms(it2) = bface%dit(2,2)
    jkmf(it2) = jkms(it2) + sign(bface%dit(2,3)-1,bit(it2))

    !-- loop over the boundary nodes
    do jit1 = jkms(it1), jkmf(it1), bit(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2), bit(it2)
        jkm(it2) = jit2

        j = jkm(1); k = jkm(2); m = jkm(3)

        q = blk%q(j,k,m,1:5)
        dq = q - qfs
        dxidx = blk%dxidx(j,k,m,di,:)
        call boundarySAT(sgn,dxidx,q,dq,sat)

        !-- add the SAT terms; the factor of 2 is for the diagonal norm
        blk%s(j,k,m,1:5) = blk%s(j,k,m,1:5) + d2_0*sat(1:5)

      end do
    end do

  end subroutine getBCFarRHS
!
! =======================================================================
!
  subroutine getBCWallRHS(bface, blk)
!
! -----------------------------------------------------------------------
! Purpose: Adds the SAT terms for a slip-wall boundary to the nodes on
! the given boundary subface.
!
! Inputs:
! bface = an boundary structure which specifies the subface info
!
! InOuts:
! blk = a block structure whose nodes on bface are getting SAT terms
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Boundary), intent(in) :: bface

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: di, it1, it2, jit1, jit2, j, k, m
    integer :: jkm(3), jkms(3), jkmf(3), bit(3)
    real(kind=dp) :: q(5), dxidx(3), qwall(5), dq(5), P(5,5), sgn, sat(5)

! _____________________
! begin main execution

    !-- find the normal direction index and bit
    di = (bface%side+1)/2
    if (mod(bface%side,2) == 1) then
      jkm(di) = 1
      bit(di) = 1
      sgn = d1_0
    else
      jkm(di) = blk%jkmmax(di)
      bit(di) = -1
      sgn = -d1_0
    end if

    !-- find the first tangential direction information
    it1 = abs(bface%dit(1,1))
    bit(it1) = sign(1,bface%dit(1,1))
    jkms(it1) = bface%dit(1,2)
    jkmf(it1) = jkms(it1) + sign(bface%dit(1,3)-1,bit(it1))

    !-- find the second tangential direction information
    it2 = abs(bface%dit(2,1))
    bit(it2) = sign(1,bface%dit(2,1))
    jkms(it2) = bface%dit(2,2)
    jkmf(it2) = jkms(it2) + sign(bface%dit(2,3)-1,bit(it2))
    !-- loop over the boundary nodes
    do jit1 = jkms(it1), jkmf(it1), bit(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2), bit(it2)
        jkm(it2) = jit2

        j = jkm(1); k = jkm(2); m = jkm(3)

        q = blk%q(j,k,m,1:5)
        dxidx = blk%dxidx(j,k,m,di,:)
        !-- get the appropriate penalty for the wall
        call wallQ(dxidx,q,qwall,P)
        !qwall(5) = qwall(5) - d0_5*q(1)*(q(2)*q(2) + q(3)*q(3) + q(4)*q(4) &
        ! - qwall(2)*qwall(2) - qwall(3)*qwall(3) - qwall(4)*qwall(4))
        dq = q - qwall
        !call farfieldSAT(sgn,dxidx,q,dq,sat)
        call boundarySAT(sgn,dxidx,q,dq,sat)

        !-- add the SAT terms; the factor of 2 is for the diagonal norm
        blk%s(j,k,m,1:5) = blk%s(j,k,m,1:5) + d2_0*sat(1:5)
      end do
    end do

  end subroutine getBCWallRHS
!
! =======================================================================
!
  subroutine getBCDirichletRHS(bface, blk, qfs)
!
! -----------------------------------------------------------------------
! Purpose: Adds the SAT terms for a Dirichlet boundary to the nodes on
! the given boundary subface.
!
! Inputs:
! bface = an boundary structure which specifies the subface info
! fsvar = the free-stream variables (a structure)
!
! InOuts:
! blk = a block structure whose nodes on bface are getting SAT terms
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    type(Boundary), intent(in) :: bface
    real(kind=dp), intent(in) :: qfs(5)

    type(Block), intent(inout) :: blk

    !-- local variables
    integer :: di, it1, it2, jit1, jit2, j, k, m
    integer :: jkm(3), jkms(3), jkmf(3), bit(3)
    real(kind=dp) :: q(5), dq(5), dxidx(3), sgn, sat(5)

! _____________________
! begin main execution

    !-- find the normal direction index and bit
    di = (bface%side+1)/2
    if (mod(bface%side,2) == 1) then
      jkm(di) = 1
      bit(di) = 1
      sgn = d1_0
    else
      jkm(di) = blk%jkmmax(di)
      bit(di) = -1
      sgn = -d1_0
    end if

    !-- find the first tangential direction information
    it1 = abs(bface%dit(1,1))
    bit(it1) = sign(1,bface%dit(1,1))
    jkms(it1) = bface%dit(1,2)
    jkmf(it1) = jkms(it1) + sign(bface%dit(1,3)-1,bit(it1))

    !-- find the second tangential direction information
    it2 = abs(bface%dit(2,1))
    bit(it2) = sign(1,bface%dit(2,1))
    jkms(it2) = bface%dit(2,2)
    jkmf(it2) = jkms(it2) + sign(bface%dit(2,3)-1,bit(it2))

    !-- loop over the boundary nodes
    do jit1 = jkms(it1), jkmf(it1), bit(it1)
      jkm(it1) = jit1
      do jit2 = jkms(it2), jkmf(it2), bit(it2)
        jkm(it2) = jit2

        j = jkm(1); k = jkm(2); m = jkm(3)

        q = blk%q(j,k,m,1:5)
        dq = q - qfs

        !-- add the SAT terms; the factor of 2 is for the diagonal norm
        blk%s(j,k,m,1:5) = dq(1:5) !blk%s(j,k,m,1:5) + d2_0*dq(1:5)

      end do
    end do

  end subroutine getBCDirichletRHS
!
! =======================================================================
!
  subroutine boundarySAT(sgn, dxidx, q, dq, sat)
!
! -----------------------------------------------------------------------
! Purpose: Calculates the (Euler) SAT boundary terms based on the state
! q (assumed to be the average state), the difference dq, and the
! metric terms dxidx (assumed to be independent of J).
! Note: this implementation is based closely on the matrix dissipation
! model of Swanson and Turkel.
! Note: this differs from eulerSAT (the one for interfaces) in that
! it does not provide an entropy fix.
!
! Inputs:
! sgn = +1 if interface is at low side of block, -1 otherwise
! dxidx = the metric terms in the desired direction (indpendent of J)
! q = an averaged flow state
! dq = the difference in flow states
!
! Outs:
! sat = the Euler SAT interface penalty term
!
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    real(kind=dp), intent(in) :: sgn, dxidx(3), q(5), dq(5)

    real(kind=dp), intent(out) :: sat(5)

    !-- local variables
    real(kind=dp) :: nx, ny, nz, dA, q1, q2, q3, q4, q5, fac, press, a, &
         u, v, w, Un, lambda1, lambda2, lambda3, rhoA, phi, H, &
         dq1, dq2, dq3, dq4, dq5, tmp1, tmp2, tmp3
    real(kind=dp) :: E1dq(5), E2dq(5)

! _____________________
! begin main execution

    !-- get some flow and geometric quantities for quick reference
    nx = dxidx(1); ny = dxidx(2); nz = dxidx(3)
    dA = sqrt(nx*nx + ny*ny + nz*nz)

    q1 = q(1)
    q2 = q(2)
    q3 = q(3)
    q4 = q(4)
    q5 = q(5)
    fac = d1_0/q1
    press = gami*(q5 - d0_5*(q2*q2 + q3*q3 + q4*q4)*fac)
    a = sqrt(gamma*press*fac)
    u = q2*fac; v = q3*fac; w = q4*fac
    Un = u*nx + v*ny + w*nz

    lambda1 = Un + dA*a
    lambda2 = Un - dA*a
    lambda3 = Un
    rhoA = abs(Un) + dA*a

    lambda1 = d0_5*(abs(lambda1) + sgn*lambda1)
    lambda2 = d0_5*(abs(lambda2) + sgn*lambda2)
    lambda3 = d0_5*(abs(lambda3) + sgn*lambda3)







    phi = d0_5*(u*u + v*v + w*w)
    H = (q5 + press)*fac

    dq1 = dq(1); dq2 = dq(2); dq3 = dq(3); dq4 = dq(4); dq5 = dq(5)

    !-- diagonal matrix multiply
    sat(1) = lambda3*dq1
    sat(2) = lambda3*dq2
    sat(3) = lambda3*dq3
    sat(4) = lambda3*dq4
    sat(5) = lambda3*dq5

    !-- get E1*dq
    E1dq(1) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E2*dq
    E2dq(1) = d0_0
    E2dq(2) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to sat
    tmp1 = d0_5*(lambda1 + lambda2) - lambda3
    tmp2 = gami/(a*a)
    tmp3 = d1_0/(dA*dA)
    sat(:) = sat(:) + tmp1*(tmp2*E1dq(:) + tmp3*E2dq(:))

    !-- get E3*dq
    E1dq(1) = -Un*dq1 + nx*dq2 + ny*dq3 + nz*dq4
    E1dq(2) = E1dq(1)*u
    E1dq(3) = E1dq(1)*v
    E1dq(4) = E1dq(1)*w
    E1dq(5) = E1dq(1)*H

    !-- get E4*dq
    E2dq(1) = d0_0
    E2dq(2) = phi*dq1 - u*dq2 - v*dq3 - w*dq4 + dq5
    E2dq(3) = E2dq(2)*ny
    E2dq(4) = E2dq(2)*nz
    E2dq(5) = E2dq(2)*Un
    E2dq(2) = E2dq(2)*nx

    !-- add to sat
    tmp1 = d0_5*(lambda1 - lambda2)/(dA*a)
    sat(:) = sat(:) + tmp1*(E1dq(:) + gami*E2dq(:))

  end subroutine boundarySAT
!
! =======================================================================
!
  subroutine wallQ(dxidx, q, qwall, M)
!
! -----------------------------------------------------------------------
! Purpose:
! Calculates the appropriate wall velcoity such that the normal
! velocity of qwall is 0. This is used to get the wall data for the
! SAT method applied to an Euler slip boundary condition. Here's how
! it works: the velocity at a coordinate plane (xi,eta, or zeta =
! constant) is found, using a local orthogonal basis composed of the
! normal to the surface (dxidx/|dxidx|), and two tangent vectors to the
! surface. The transponse of the orthogonal matrix, A^{T}, satisfies
!
! | u | | a(1,1) a(1,2) a(1,3) | |vn |
! | v | = | a(2,1) a(2,2) a(2,3) |*|vt1|
! | w | | a(3,1) a(3,2) a(3,3) | |vt2|
!
! where
! a(*,1) = c1*dXi(di)/dx(*), c1 = 1/norm(dXi(di)/dx(*))
! a(*,2) = a unit vector, orthogonal to a(*,1)
! a(*,3) = crossProd(a(*,2),a(*,1))
! The matrix system thus constructed is orthogonal and therefore
! trivial to invert. The actual manipulation that occurs is given by
!
! (u,v,w)^{T}_{wall} = A^{T} \hat{A} (u,v,w)^{T}
!
! where
! \hat{A} = A with first row of set to zero.
!
!
! Inputs:
! dxidx = the grid metrics in the direction normal to the surface
! q = the flow variables at the wall, before the correction
!
! Outputs:
! qwall = the flow variables at the wall, after the correction
! M = the transformation matrix, P^{T} \hat{P}
!
! Author: J.E. Hicken, October 2005
! -----------------------------------------------------------------------
! _______________________
! variable specification

    !-- subroutine arguments
    real(kind=dp), intent(in) :: dxidx(3), q(5)

    real(kind=dp), intent(out) :: qwall(5), M(5,5)

    !-- local variables
    integer :: it1, it2, i
    real(kind=dp) :: A(3,3), fac, vec1(3), vec2(3)
    real(kind=dp) :: q1, q2, q3, q4, vn, vt1, vt2

! _____________________
! begin main execution

    A(:,:) = 0.d0

    !-- get the boundary normal vector
    A(1,:) = dxidx(:)
    fac = d1_0/sqrt(A(1,1)*A(1,1) + A(1,2)*A(1,2) + A(1,3)*A(1,3))
    A(1,:) = fac*A(1,:)

    !-- find the first tangential normal vector
    it1 = 1
    if (abs(A(1,2)) < abs(A(1,it1))) it1 = 2
    if (abs(A(1,3)) < abs(A(1,it1))) it1 = 3
    A(2,it1) = 1.d0
    A(2,:) = A(2,:) - A(1,it1)*A(1,:)
    fac = d1_0/sqrt(A(2,1)*A(2,1) + A(2,2)*A(2,2) + A(2,3)*A(2,3))
    A(2,:) = fac*A(2,:)

    !-- find the second tangential normal vector
    vec1 = A(1,:)
    vec2 = A(2,:)
    A(3,:) = crossProd(vec1,vec2)

    !-- calculate the momentums in the new basis
    q2 = q(2); q3 = q(3); q4 = q(4)
    vn = d0_0 !A(1,1)*q2 + A(1,2)*q3 + A(1,3)*q4
    vt1 = A(2,1)*q2 + A(2,2)*q3 + A(2,3)*q4
    vt2 = A(3,1)*q2 + A(3,2)*q3 + A(3,3)*q4

    !-- convert back to the old basis
    qwall(1) = q(1)
    qwall(2) = A(2,1)*vt1 + A(3,1)*vt2 !-- + A(1,1)*vn
    qwall(3) = A(2,2)*vt1 + A(3,2)*vt2 !-- + A(1,2)*vn
    qwall(4) = A(2,3)*vt1 + A(3,3)*vt2 !-- + A(1,3)*vn
    qwall(5) = q(5)
    fac = d1_0/q(1)
    qwall(5) = q(5) - d0_5*(q2*q2 + q3*q3 + q4*q4 &
         - vt1*vt1 - vt2*vt2)*fac

    !-- find the transformation matrix
    M(:,:) = d0_0
    forall (i = 1:5) M(i,i) = d1_0
    M(2:4,2) = M(2:4,2) - A(1,1)*A(1,1:3)
    M(2:4,3) = M(2:4,3) - A(1,2)*A(1,1:3)
    M(2:4,4) = M(2:4,4) - A(1,3)*A(1,1:3)
    M(5,1) = (q(5) - qwall(5))*fac
    M(5,2) = (vt1*A(2,1) + vt2*A(3,1) - q2)*fac
    M(5,3) = (vt1*A(2,2) + vt2*A(3,2) - q3)*fac
    M(5,4) = (vt1*A(2,3) + vt2*A(3,3) - q4)*fac

  end subroutine wallQ
!
! =======================================================================
!
end module EulerRHS_Mod
