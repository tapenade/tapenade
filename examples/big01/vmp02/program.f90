PROGRAM forces
IMPLICIT none
!!$!***************************************
!!$!Module Files
!!$!**************************************
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * File:          block.f90                                       *
!!$!      * Author:        Edwin van der Weide, Steve Repsher,             *
!!$!      *                Seonghyeon Hahn                                 *
!!$!      * Starting date: 12-19-2002                                      *
!!$!      * Last modified: 10-11-2005                                      *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$!       module block
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * This module contains the definition of the derived data type   *
!!$!      * for block, which is the basic building block for this code.    *
!!$!      *                                                                *
!!$!      * Apart from the derived data type for block, this module also   *
!!$!      * contains the actual array for storing the blocks and the       *
!!$!      * number of blocks stored on this processor.                     *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$!       use constants
!!$!       implicit none
!!$!       save
!!$
!!$       ! Parameters used for coarsening definition.
!!$
!!$       integer(kind=porType), parameter :: leftStarted  = -1_porType
!!$       integer(kind=porType), parameter :: regular      =  0_porType
!!$       integer(kind=porType), parameter :: rightStarted =  1_porType
!!$
!!$       ! Parameters used for subsonic inlet bc treatment.
!!$
!!$       integer(kind=intType), parameter :: noSubInlet      = 0_intType
!!$       integer(kind=intType), parameter :: totalConditions = 1_intType
!!$       integer(kind=intType), parameter :: massFlow        = 2_intType
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * The definition of the derived data type visc_subface_type,     *
!!$!      * which stores the viscous stress tensor and heat flux vector.   *
!!$!      * In this way it is avoided that these quantities must be        *
!!$!      * recomputed for the viscous forces and postprocessing. This     *
!!$!      * saves both time and a considerable amount of code.             *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$       type viscSubfaceType
!!$
!!$         ! tau(:,:,6): The 6 components of the viscous stress tensor.
!!$         !             The first 2 dimensions of these arrays are equal
!!$         !             to the dimenions of the cell subface without any
!!$         !             halo cell. Consequently the starting index is
!!$         !             arbitrary, such that no offset computation is
!!$         !             needed when the arrays are accessed.
!!$         ! q(:,:,3):   Same story for the heat flux vector.
!!$         ! uTau(:,:):  And for the friction velocity.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: tau, q
!!$         real(kind=realType), dimension(:,:),   pointer :: uTau
!!$
!!$       end type viscSubfaceType
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * The definition of the derived data type BCDataType, which      *
!!$!      * stores the prescribed data of boundary faces as well as unit   *
!!$!      * normals. For all the arrays the first two dimensions equal the *
!!$!      * dimensions of the subface, possibly extended with halo cells.  *
!!$!      * Consequently the starting index is arbitrary, such that no     *
!!$!      * offset computation is needed when the array is accessed.       *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$       type BCDataType
!!$
!!$         ! inBeg, inEnd: Node range in the first direction of the subface
!!$         ! jnBeg, jnEnd: Idem in the second direction.
!!$         ! icBeg, icEnd: Cell range in the first direction of the subface
!!$         ! jcBeg, jcEnd: Idem in the second direction.
!!$
!!$         integer(kind=intType) :: inBeg, inEnd, jnBeg, jnEnd
!!$         integer(kind=intType) :: icBeg, icEnd, jcBeg, jcEnd
!!$
!!$         ! norm(:,:,3):  The unit normal; it points out of the domain.
!!$         ! rface(:,:):   Velocity of the face in the direction of the
!!$         !               outward pointing normal. only allocated for
!!$         !               the boundary conditions that need this info.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: norm
!!$         real(kind=realType), dimension(:,:),   pointer :: rface
!!$
!!$         ! subsonicInletTreatment: which boundary condition treatment
!!$         !                         to use for subsonic inlets; either
!!$         !                         totalConditions or massFlow.
!!$
!!$         integer(kind=intType) :: subsonicInletTreatment
!!$
!!$         ! uSlip(:,:,3):  the 3 components of the velocity vector on
!!$         !                a viscous wall. 
!!$         ! TNS_Wall(:,:): Wall temperature for isothermal walls.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: uSlip
!!$         real(kind=realType), dimension(:,:),   pointer :: TNS_Wall
!!$
!!$         ! ptInlet(:,:):       Total pressure at subsonic inlets.
!!$         ! ttInlet(:,:):       Total temperature at subsonic inlets.
!!$         ! htInlet(:,:):       Total enthalpy at subsonic inlets.
!!$         ! flowXDirInlet(:,:): X-direction of the flow for subsonic
!!$         !                     inlets.
!!$         ! flowYDirInlet(:,:): Idem in y-direction.
!!$         ! flowZDirInlet(:,:): Idem in z-direction.
!!$
!!$         real(kind=realType), dimension(:,:), pointer :: ptInlet
!!$         real(kind=realType), dimension(:,:), pointer :: ttInlet
!!$         real(kind=realType), dimension(:,:), pointer :: htInlet
!!$         real(kind=realType), dimension(:,:), pointer :: flowXDirInlet
!!$         real(kind=realType), dimension(:,:), pointer :: flowYDirInlet
!!$         real(kind=realType), dimension(:,:), pointer :: flowZDirInlet
!!$
!!$         ! turbInlet(:,:,nt1:nt2): Turbulence variables at inlets,
!!$         !                         either subsonic or supersonic.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: turbInlet
!!$
!!$         ! rho(:,:):  density; used for multiple bc's.
!!$         ! velX(:,:): x-velocity; used for multiple bc's.
!!$         ! velY(:,:): y-velocity; used for multiple bc's.
!!$         ! velZ(:,:): z-velocity; used for multiple bc's.
!!$         ! ps(:,:):   static pressure; used for multiple bc's.
!!$
!!$         real(kind=realType), dimension(:,:), pointer :: rho
!!$         real(kind=realType), dimension(:,:), pointer :: velX
!!$         real(kind=realType), dimension(:,:), pointer :: velY
!!$         real(kind=realType), dimension(:,:), pointer :: velZ
!!$         real(kind=realType), dimension(:,:), pointer :: ps
!!$
!!$       end type BCDataType
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * The definition of the derived data type block_type, which      *
!!$!      * stores dimensions, coordinates, solution, etc.                 *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$       type blockType
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Block dimensions and orientation.                            *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         !  nx, ny, nz - Block integer dimensions for no halo cell based
!!$         !               quantities.
!!$         !  il, jl, kl - Block integer dimensions for no halo node based
!!$         !               quantities.
!!$         !  ie, je, ke - Block integer dimensions for single halo
!!$         !               cell-centered quantities.
!!$         !  ib, jb, kb - Block integer dimensions for double halo
!!$         !               cell-centered quantities.
!!$         ! rightHanded - Whether or not the block is a right handed.
!!$         !               If not right handed it is left handed of course.
!!$
!!$         integer(kind=intType) :: nx, ny, nz
!!$         integer(kind=intType) :: il, jl, kl
!!$         integer(kind=intType) :: ie, je, ke
!!$         integer(kind=intType) :: ib, jb, kb
!!$
!!$         logical :: rightHanded
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Block boundary conditions.                                   *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         !  nSubface             - Number of subfaces on this block.
!!$         !  n1to1                - Number of 1 to 1 block boundaries.
!!$         !  nBocos               - Number of physical boundary subfaces.
!!$         !  nViscBocos           - Number of viscous boundary subfaces.
!!$         !  BCType(:)            - Boundary condition type for each
!!$         !                         subface. See the module BCTypes for
!!$         !                         the possibilities.
!!$         !  BCFaceID(:)          - Block face location of each subface.
!!$         !                         possible values are: iMin, iMax, jMin,
!!$         !                         jMax, kMin, kMax. see also module
!!$         !                         BCTypes.
!!$         !  cgnsSubface(:)       - The subface in the corresponding cgns
!!$         !                         block. As cgns distinguishes between
!!$         !                         boundary and internal boundaries, the
!!$         !                         BCType of the subface is needed to
!!$         !                         know which one to take.
!!$         !  inBeg(:), inEnd(:)   - Lower and upper limits for the nodes
!!$         !  jnBeg(:), jnEnd(:)     in each of the index directions on a
!!$         !  knBeg(:), knEnd(:)     given subface. Note that one of these
!!$         !                         indices will not change since we will
!!$         !                         be moving on a face.
!!$         !  dinBeg(:), dinEnd(:) - Lower and upper limits for the nodes
!!$         !  djnBeg(:), djnEnd(:)   in the each of the index directions
!!$         !  dknBeg(:), dknEnd(:)   of the donor subface for this
!!$         !                         particular subface. Note that one of
!!$         !                         these indices will not change since we
!!$         !                         will be moving on a face.
!!$         !  icBeg(:), icEnd(:)   - Lower and upper limits for the cells
!!$         !  jcBeg(:), jcEnd(:)     in each of the index directions for
!!$         !  kcBeg(:), kcEnd(:)     the subface. The cells indicated by
!!$         !                         this range are halo cells (the 
!!$         !                         constant index) adjacent to the face.
!!$         !                         a possible overlap outside the block
!!$         !                         is stored.
!!$         !  neighBlock(:)        - Local block number to which this
!!$         !                         subface connects. This value is set to
!!$         !                         zero if this subface is not connected
!!$         !                         to another block.
!!$         !  neighProc(:)         - Processor number where the neighbor
!!$         !                         block is stored. This value is set to
!!$         !                         -1 if this subface is not connected
!!$         !                         to another block.
!!$         !  l1(:), l2(:),        - Short hand for the transformation
!!$         !  l3(:)                  matrix between this subface and the
!!$         !                         neighbor block. These values are set
!!$         !                         to zero if this subface is not
!!$         !                         connected to another block.
!!$         !  groupNum(:)          - Group number to which this subface
!!$         !                         belongs. If this subface does not
!!$         !                         belong to any group, the corresponding
!!$         !                         entry in this array is zeroed out. If
!!$         !                         the subface belongs to a sliding mesh
!!$         !                         interface the absolute value of 
!!$         !                         group_num contains the number of the
!!$         !                         sliding mesh interface. One side of
!!$         !                         the interface gets a positive number,
!!$         !                         the other side a negative one.
!!$
!!$         integer(kind=intType) :: nSubface, n1to1, nBocos, nViscBocos
!!$
!!$         integer(kind=intType), dimension(:), pointer :: BCType
!!$         integer(kind=intType), dimension(:), pointer :: BCFaceID
!!$         integer(kind=intType), dimension(:), pointer :: cgnsSubface
!!$
!!$         integer(kind=intType), dimension(:), pointer :: inBeg, inEnd
!!$         integer(kind=intType), dimension(:), pointer :: jnBeg, jnEnd
!!$         integer(kind=intType), dimension(:), pointer :: knBeg, knEnd
!!$
!!$         integer(kind=intType), dimension(:), pointer :: dinBeg, dinEnd
!!$         integer(kind=intType), dimension(:), pointer :: djnBeg, djnEnd
!!$         integer(kind=intType), dimension(:), pointer :: dknBeg, dknEnd
!!$
!!$         integer(kind=intType), dimension(:), pointer :: icBeg, icEnd
!!$         integer(kind=intType), dimension(:), pointer :: jcBeg, jcEnd
!!$         integer(kind=intType), dimension(:), pointer :: kcBeg, kcEnd
!!$
!!$         integer(kind=intType), dimension(:), pointer :: neighBlock
!!$         integer(kind=intType), dimension(:), pointer :: neighProc
!!$         integer(kind=intType), dimension(:), pointer :: l1, l2, l3
!!$         integer(kind=intType), dimension(:), pointer :: groupNum
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Overset boundary (fringe) cells and blanked cells.           *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         !  iblank(0:Ib,0:jb,0:kb) - stores an integer for every cell of
!!$         !                           this block, including halos. The
!!$         !                           following convention is used:
!!$         !                           + field = 1
!!$         !                           + hole = 0
!!$         !                           + fringe >= 9 preprocessing
!!$         !                                     = 0 solver
!!$         !                           + oversetOuterBound boco = -1
!!$         !                           + any other boco halos = 2
!!$         !  nHoles                 - number of owned hole cells.
!!$         !  nCellsOverset          - number of owned overset cells with
!!$         !                           donors.
!!$         !  nCellsOversetAll       - total number of overset cells
!!$         !                           including fringe from 1-to-1 halos
!!$         !                           and orphans.
!!$         !  nOrphans               - number of orphans (boundary cells
!!$         !                           without donors).
!!$         !  ibndry(3,..)           - indices for each overset cell.
!!$         !  idonor(3,..)           - donor indices for each overset cell.
!!$         !  overint(3,..)          - interpolants for the donor stencil.
!!$         !  neighBlockOver(..)     - local block number to which donor
!!$         !                           cell belongs.
!!$         !  neighProcOver(..)      - processor number where the neighbor
!!$         !                           block is stored.
!!$
!!$         integer(kind=intType) :: nCellsOverset, nCellsOversetAll
!!$         integer(kind=intType) :: nHoles, nOrphans
!!$
!!$         integer(kind=intType), dimension(:,:,:), pointer :: iblank
!!$
!!$         integer(kind=intType), dimension(:,:), pointer :: ibndry
!!$         integer(kind=intType), dimension(:,:), pointer :: idonor
!!$         real(kind=realType),   dimension(:,:), pointer :: overint
!!$
!!$         integer(kind=intType), dimension(:), pointer :: neighBlockOver
!!$         integer(kind=intType), dimension(:), pointer :: neighProcOver
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Boundary data for the boundary subfaces.                     *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! BCData(nBocos): The boundary data for each of the boundary
!!$         !                 subfaces.
!!$
!!$         type(BCDataType), dimension(:), pointer :: BCData
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * The stress tensor and heat flux vector at viscous wall faces *
!!$!        * as well as the face pointers to these viscous wall faces.    *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! viscSubface(nViscBocos):    Storage for the viscous stress
!!$         !                             tensor and heat flux vector for
!!$         !                             the viscous subfaces.
!!$         ! viscIMinPointer(2:jl,2:kl): Pointer to viscous subface for 
!!$         !                             the iMin block face. If the face
!!$         !                             is not part of a viscous subface
!!$         !                             this value is set to 0.
!!$         ! viscIMaxPointer(2:jl,2:kl): Idem for iMax block face.
!!$         ! viscJMinPointer(2:il,2:kl): Idem for jMin block face.
!!$         ! viscJMaxPointer(2:il,2:kl): Idem for jmax block face.
!!$         ! viscKMinPointer(2:il,2:jl): Idem for kMin block face.
!!$         ! viscKMaxPointer(2:il,2:jl): Idem for kMax block face.
!!$
!!$         type(viscSubfaceType), dimension(:), pointer :: viscSubface
!!$
!!$         integer(kind=intType), dimension(:,:), pointer :: viscIMinPointer
!!$         integer(kind=intType), dimension(:,:), pointer :: viscIMaxPointer
!!$         integer(kind=intType), dimension(:,:), pointer :: viscJMinPointer
!!$         integer(kind=intType), dimension(:,:), pointer :: viscJMaxPointer
!!$         integer(kind=intType), dimension(:,:), pointer :: viscKMinPointer
!!$         integer(kind=intType), dimension(:,:), pointer :: viscKMaxPointer
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Mesh related variables.                                      *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         !  x(0:ie,0:je,0:ke,3)  - xyz locations of grid points in block.
!!$         !  xOld(nOld,:,:,:,:)   - Coordinates on older time levels;
!!$         !                         only needed for unsteady problems on
!!$         !                         deforming grids. Only allocated on
!!$         !                         the finest grid level. The blank
!!$         !                         dimensions are equal to the dimensions
!!$         !                         of x.
!!$         !  sI(0:ie,1:je,1:ke,3) - Projected areas in the i-coordinate
!!$         !                         direction. Normals point in the
!!$         !                         direction of increasing i.
!!$         !  sJ(1:ie,0:je,1:ke,3) - Projected areas in the j-coordinate
!!$         !                         direction. Normals point in the
!!$         !                         direction of increasing j.
!!$         !  sK(1:ie,1:je,0:ke,3) - Projected areas in the k-coordinate
!!$         !                         direction. Normals point in the
!!$         !                         direction of increasing k.
!!$         !  vol(0:ib,0:jb,0:kb)  - Cell volumes. The second level halo
!!$         !                         is present for a multigrid option.
!!$         !  volOld(nold,2:il,..) - Volumes on older time levels; only
!!$         !                         needed for unsteady problems on
!!$         !                         deforming grids. Only allocated on
!!$         !                         the finest grid level.
!!$         !  porI(1:il,2:jl,2:kl) - Porosity in the i direction.
!!$         !  porJ(2:il,1:jl,2:kl) - Porosity in the j direction.
!!$         !  porK(2:il,2:jl,1:kl) - Porosity in the k direction.
!!$         !
!!$         !  blockIsMoving      - Whether or not the block is moving.
!!$         !  addGridVelocities  - Whether or not the face velocities
!!$         !                       are allocated and set.
!!$         !  sFaceI(0:ie,je,ke) - Dot product of the face velocity and
!!$         !                       the normal in i-direction.
!!$         !  sFaceJ(ie,0:je,ke) - Idem in j-direction.
!!$         !  sFaceK(ie,je,0:ke) - Idem in k-direction.
!!$
!!$         real(kind=realType), dimension(:,:,:,:),   pointer :: x
!!$         real(kind=realType), dimension(:,:,:,:,:), pointer :: xOld
!!$
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: sI, sJ, sK
!!$         real(kind=realType), dimension(:,:,:),   pointer :: vol
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: volOld
!!$
!!$         integer(kind=porType), dimension(:,:,:), pointer :: porI
!!$         integer(kind=porType), dimension(:,:,:), pointer :: porJ
!!$         integer(kind=porType), dimension(:,:,:), pointer :: porK
!!$
!!$         logical :: blockIsMoving, addGridVelocities
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: sFaceI
!!$         real(kind=realType), dimension(:,:,:), pointer :: sFaceJ
!!$         real(kind=realType), dimension(:,:,:), pointer :: sFaceK
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Flow variables.                                              *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! w(0:ib,0:jb,0:kb,1:nw)       - The set of independent variables
!!$         !                                w(i,j,k,1:nwf) flow field
!!$         !                                variables, which are rho, u, 
!!$         !                                v, w and rhoE. In other words
!!$         !                                the velocities  are stored and
!!$         !                                not the momentum!!!!
!!$         !                                w(i,j,k,nt1:nt2) turbulent 
!!$         !                                variables; also the primitive
!!$         !                                variables are stored.
!!$         ! wOld(nOld,2:il,2:jl,2:kl,nw) - Solution on older time levels,
!!$         !                                needed for the time integration
!!$         !                                for unsteady problems. In
!!$         !                                constrast to w, the conservative
!!$         !                                variables are stored in wOld for
!!$         !                                the flow variables; the turbulent
!!$         !                                variables are always the
!!$         !                                primitive ones.
!!$         !                                Only allocated on the finest
!!$         !                                mesh.
!!$         ! p(0:ib,0:jb,0:kb)            - Static pressure.
!!$         ! gamma(0:ib,0:jb,0:kb)        - Specific heat ratio; only
!!$         !                                allocated on the finest grid.
!!$         ! rlv(0:ib,0:jb,0:kb)          - Laminar viscosity; only
!!$         !                                allocated on the finest mesh
!!$         !                                and only for viscous problems.
!!$         ! rev(0:ib,0:jb,0:kb)          - Eddy viscosity; only
!!$         !                                allocated rans problems with
!!$         !                                eddy viscosity models.
!!$         ! s(1:ie,1:je,1:ke,3)          - Mesh velocities of the cell
!!$         !                                centers; only for moving mesh
!!$         !                                problems.
!!$
         real(kind=realType), dimension(:,:,:,:),   pointer :: w
         real(kind=realType), dimension(:,:,:,:,:), pointer :: wOld
         real(kind=realType), dimension(:,:,:),     pointer :: p, gamma
         real(kind=realType), dimension(:,:,:),     pointer :: rlv, rev
         real(kind=realType), dimension(:,:,:,:),   pointer :: s
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Residual and multigrid variables.                            *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! dw(0:ib,0:jb,0:kb,1:nw)   - Values of convective and combined
!!$         !                             flow residuals. Only allocated on
!!$         !                             the finest mesh.
!!$         ! fw(0:ib,0:jb,0:kb,1:nwf)  - values of artificial dissipation
!!$         !                             and viscous residuals.
!!$         !                             Only allocated on the finest mesh.
!!$
!!$         ! w1(1:ie,1:je,1:ke,1:nMGVar) - Values of the mg variables
!!$         !                               upon first entry to a coarser
!!$         !                               mesh; only allocated on the
!!$         !                               coarser grids. The variables
!!$         !                               used to compute the multigrid
!!$         !                               corrections are rho, u, v, w
!!$         !                               and p; the rhoE value is used
!!$         !                               for unsteady problems only.
!!$         ! p1(1:ie,1:je,1:ke)          - Value of the pressure upon
!!$         !                               first entry to a coarser grid;
!!$         !                               only allocated on the coarser
!!$         !                               grids.
!!$         ! wr(2:il,2:jl,2:kl,1:nMGVar) - Multigrid forcing terms; only 
!!$         !                               allocated on the coarser grids.
!!$         !                               The forcing term of course
!!$         !                               contains conservative residuals,
!!$         !                               at least for the flow variables.
!!$
!!$         real(kind=realType), dimension(:,:,:),   pointer :: p1
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: dw, fw
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: w1, wr
!!$
!!$         ! mgIFine(2:il,2) - The two fine grid i-cells used for the
!!$         !                   restriction of the solution and residual to
!!$         !                   the coarse grid. Only on the coarser grids.
!!$         ! mgJFine(2:jl,2) - Idem for j-cells.
!!$         ! mgKFine(2:kl,2) - Idem for k-cells.
!!$
!!$         ! mgIWeight(2:il) - Weight for the residual restriction in
!!$         !                   in i-direction. Value is either 0.5 or 1.0,
!!$         !                   depending whether mgIFine(,1) is equal to
!!$         !                   or differs from mgIFine(,2).
!!$         ! mgJWeight(2:jl) - Idem for weights in j-direction.
!!$         ! mgKWeight(2:kl) - Idem for weights in k-direction.
!!$
!!$         ! mgICoarse(2:il,2) - The two coarse grid i-cells used for the
!!$         !                     interpolation of the correction to the
!!$         !                     fine grid. Not on the coarsest grid.
!!$         ! mgJCoarse(2:jl,2) - Idem for j-cells.
!!$         ! mgKCoarse(2:kl,2) - Idem for k-cells.
!!$
!!$         integer(kind=intType), dimension(:,:), pointer :: mgIFine
!!$         integer(kind=intType), dimension(:,:), pointer :: mgJFine
!!$         integer(kind=intType), dimension(:,:), pointer :: mgKFine
!!$
!!$         real(kind=realType),   dimension(:),   pointer :: mgIWeight
!!$         real(kind=realType),   dimension(:),   pointer :: mgJWeight
!!$         real(kind=realType),   dimension(:),   pointer :: mgKWeight
!!$
!!$         integer(kind=intType), dimension(:,:), pointer :: mgICoarse
!!$         integer(kind=intType), dimension(:,:), pointer :: mgJCoarse
!!$         integer(kind=intType), dimension(:,:), pointer :: mgKCoarse
!!$
!!$         ! iCoarsened - How this block was coarsened in i-direction.
!!$         ! jCoarsened - How this block was coarsened in j-direction.
!!$         ! kCoarsened - How this block was coarsened in k-direction.
!!$
!!$         integer(kind=porType) :: iCoarsened, jCoarsened, kCoarsened
!!$
!!$         ! iCo: Indicates whether or not i grid lines are present on the
!!$         !      coarse grid; not allocated for the coarsest grid.
!!$         ! jCo: Idem in j-direction.
!!$         ! kCo: Idem in k-direction.
!!$
!!$         logical, dimension(:), pointer :: iCo, jCo, kCo
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Time-stepping and spectral radii variables.                  *
!!$!        * only allocated on the finest grid.                           *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! wn(2:il,2:jl,2:kl,1:nMGVar) - Values of the update variables
!!$         !                               at the beginning of the RungeKutta
!!$         !                               iteration. Only allocated for
!!$         !                               RungeKutta smoother.
!!$         ! pn(2:il,2:jl,2:kl)          - The pressure for the RungeKutta
!!$         !                               smoother.
!!$         ! dtl(1:ie,1:je,1:ke)         - Time step
!!$         ! radI(1:ie,1:je,1:ke)        - Spectral radius in i-direction.
!!$         ! radJ(1:ie,1:je,1:ke)        - Spectral radius in j-direction.
!!$         ! radK(1:ie,1:je,1:ke)        - Spectral radius in k-direction.
!!$
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: wn
!!$         real(kind=realType), dimension(:,:,:),   pointer :: pn
!!$         real(kind=realType), dimension(:,:,:),   pointer :: dtl
!!$         real(kind=realType), dimension(:,:,:),   pointer :: radI
!!$         real(kind=realType), dimension(:,:,:),   pointer :: radJ
!!$         real(kind=realType), dimension(:,:,:),   pointer :: radK
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Turbulence model variables.                                  *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! d2Wall(2:il,2:jl,2:kl) - Distance from the center of the cell
!!$         !                          to the nearest viscous wall.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: d2Wall
!!$
!!$         ! bmti1(je,ke,nt1:nt2,nt1:nt2): Matrix used for the implicit
!!$         !                               boundary condition treatment of
!!$         !                               the turbulence equations at the
!!$         !                               iMin boundary. Only allocated on
!!$         !                               the finest level and for the 1st
!!$         !                               spectral solution.
!!$         ! bmti2(je,ke,nt1:nt2,nt1:nt2): Idem for the iMax boundary.
!!$         ! bmtj1(ie,ke,nt1:nt2,nt1:nt2): Idem for the jMin boundary.
!!$         ! bmtj2(ie,ke,nt1:nt2,nt1:nt2): Idem for the jMax boundary.
!!$         ! bmtk1(ie,je,nt1:nt2,nt1:nt2): Idem for the kMin boundary.
!!$         ! bmtk2(ie,je,nt1:nt2,nt1:nt2): Idem for the kMax boundary.
!!$
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmti1
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmti2
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmtj1
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmtj2
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmtk1
!!$         real(kind=realType), dimension(:,:,:,:), pointer :: bmtk2
!!$
!!$         ! bvti1(je,ke,nt1:nt2): RHS vector used for the implicit
!!$         !                       boundary condition treatment of the
!!$         !                       turbulence equations at the iMin
!!$         !                       boundary. Only allocated on the finest
!!$         !                       level and for the 1st spectral solution.
!!$         ! bvti2(je,ke,nt1:nt2): Idem for the iMax boundary.
!!$         ! bvtj1(ie,ke,nt1:nt2): Idem for the jMin boundary.
!!$         ! bvtj2(ie,ke,nt1:nt2): Idem for the jMax boundary.
!!$         ! bvti2(je,ke,nt1:nt2): Idem for the iMax boundary.
!!$         ! bvtk1(ie,ke,nt1:nt2): Idem for the kMin boundary.
!!$         ! bvtk2(ie,ke,nt1:nt2): idem for the kMax boundary.
!!$
!!$         real(kind=realType), dimension(:,:,:), pointer :: bvti1, bvti2
!!$         real(kind=realType), dimension(:,:,:), pointer :: bvtj1, bvtj2
!!$         real(kind=realType), dimension(:,:,:), pointer :: bvtk1, bvtk2
!!$!
!!$!        ****************************************************************
!!$!        *                                                              *
!!$!        * Relation to the original cgns grid.                          *
!!$!        *                                                              *
!!$!        ****************************************************************
!!$!
!!$         ! sectionID      - The section of the grid this block belongs to.
!!$         ! cgnsBlockID    - Block/zone number of the cgns grid to which
!!$         !                  this block is related.
!!$         ! iBegOr, iEndOr - Range of points of this block in the
!!$         ! jBegOr, jEndOr   corresponding cgns block, i.e. for this block
!!$         ! kBegOr, kEndOr   iBegOr <= i <= iEndOr, jBegOr <= j <= jEndOr, 
!!$         !                  kBegOr <= k <= kEndOr.
!!$         !                  It is of course possible that the entire
!!$         !                  block is stored.
!!$
!!$         integer(kind=intType) :: cgnsBlockID, sectionID
!!$         integer(kind=intType) :: iBegOr, iEndOr, jBegOr, jEndOr
!!$         integer(kind=intType) :: kBegOr, kEndOr
!!$
!!$       end type blockType
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * Array of all blocks at all multigrid levels and spectral sols. *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$       ! nDom:            total number of computational blocks.
!!$       ! flowDoms(:,:,:): array of blocks. Dimensions are
!!$       !                  (nDom,nLevels,nTimeIntervalsSpectral)
!!$
!!$       integer(kind=intType) :: nDom
!!$
!!$       type(blockType), allocatable, dimension(:,:,:) :: flowDoms
!!$!
!!$!      ******************************************************************
!!$!      *                                                                *
!!$!      * Additional info needed in the flow solver.                     *
!!$!      *                                                                *
!!$!      ******************************************************************
!!$!
!!$       ! nCellGlobal(nLev) - Global number of cells on every mg level.
!!$
!!$       integer(kind=intType), allocatable, dimension(:) :: nCellGlobal
!!$
!!$!       end module block
!!$   
!!$
!!$
!!$!***************************************


contains
!
!      ******************************************************************
!      *                                                                *
!      * File:          forcesAndMoments.f90                            *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 04-01-2003                                      *
!      * Last modified: 06-12-2005                                      *
!      *                                                                *
!      ******************************************************************
!
       subroutine forcesAndMoments(w,p, cFp, cFv, cMp, cMv, yplusMax)
!
!      ******************************************************************
!      *                                                                *
!      * forcesAndMoments computes the contribution of the block        *
!      * given by the pointers in blockPointers to the force and        *
!      * moment coefficients of the geometry. A distinction is made     *
!      * between the inviscid and viscous parts. In case the maximum    *
!      * yplus value must be monitored (only possible for rans), this   *
!      * value is also computed.                                        *
!      *                                                                *
!      ******************************************************************
!
!       use blockPointers
!       use BCTypes
!       use flowVarRefState
!       use inputPhysics
       implicit none
!
!      Subroutine arguments
!
!****************************************************************
! Maximum numbers of characters in a string and in a cgns name

!!$       integer, parameter :: maxStringLen   = 256
!!$       integer, parameter :: maxCGNSNameLen =  32
!!$
!!$       ! Numerical constants
!!$
!!$       real(kind=realType), parameter :: pi    = 3.1415926535897931_realType
!!$       real(kind=realType), parameter :: eps   = 1.e-25_realType
!!$       real(kind=realType), parameter :: large = 1.e+37_realType
!!$
!!$       ! Constant in Sutherland's law; SI-units.
!!$
!!$       real(kind=realType), parameter :: SSuthDim  = 110.55_realType
!!$       real(kind=realType), parameter :: muSuthDim = 1.716e-5_realType
!!$       real(kind=realType), parameter :: TSuthDim  = 273.15_realType
!!$
!!$       ! Constants to define the porosity values
!!$
!!$       integer(kind=porType), parameter :: noFlux     = -1_porType
!!$       integer(kind=porType), parameter :: boundFlux  =  0_porType
!!$       integer(kind=porType), parameter :: normalFlux =  1_porType
!!$
!!$       ! Indices in the array of independent variables
!!$
!!$       integer, parameter :: irho    =  1  ! Density
!!$       integer, parameter :: ivx     =  2  ! x-Velocity
!!$       integer, parameter :: ivy     =  3  ! y-velocity
!!$       integer, parameter :: ivz     =  4  ! z-Velocity
!!$       integer, parameter :: irhoE   =  5  ! Energy
!!$
!!$       integer, parameter :: itu1    =  6  ! Turbulent kinetic energy,
!!$                                           ! SA viscosity
!!$       integer, parameter :: itu2    =  7  ! Dissipation rate, time scale 
!!$       integer, parameter :: itu3    =  8  ! Scalar V2 
!!$       integer, parameter :: itu4    =  9  ! Scalar F2 
!!$       integer, parameter :: itu5    = 10  ! Eddy-viscosity used for
!!$                                           ! wall functions.
!!$
!!$       ! Parameters to indicate the position in the work array dw for
!!$       ! turbulence models.
!!$
!!$       integer, parameter :: idvt    = 1  ! Tmp RHS storage; at max a
!!$                                          ! 2x2 subsystem is solved.
!!$       integer, parameter :: ivort   = 3  ! Tmp vort storage
!!$       integer, parameter :: istrain = 3  ! Tmp strain storage
!!$       integer, parameter :: iprod   = 3  ! Tmp prod storage
!!$       integer, parameter :: icd     = 4  ! Tmp cross term storage
!!$       integer, parameter :: if1SST  = 5  ! Tmp F1 (for SST) storage
!!$       integer, parameter :: isct    = 4  ! Tmp time scale (for v2f) storage
!!$       integer, parameter :: iscl2   = 5  ! Tmp length scale (for v2f) storage
!!$
!!$       ! Indices in the array of conservative flow residuals for the
!!$       ! momentum variables.
!!$
!!$       integer, parameter :: imx   = ivx ! x-Momentum
!!$       integer, parameter :: imy   = ivy ! y-Momentum
!!$       integer, parameter :: imz   = ivz ! z-Momentum
!!$
!!$       ! Floating point parameters.
!!$
       real(kind=realType), parameter :: zero  = 0.0_realType
       real(kind=realType), parameter :: one   = 1.0_realType
       real(kind=realType), parameter :: two   = 2.0_realType
       real(kind=realType), parameter :: three = 3.0_realType
       real(kind=realType), parameter :: four  = 4.0_realType
       real(kind=realType), parameter :: five  = 5.0_realType
       real(kind=realType), parameter :: six   = 6.0_realType
       real(kind=realType), parameter :: eight = 8.0_realType

       real(kind=realType), parameter :: half   = 0.5_realType
       real(kind=realType), parameter :: third  = one/three
       real(kind=realType), parameter :: fourth = 0.25_realType
       real(kind=realType), parameter :: sixth  = one/six
       real(kind=realType), parameter :: eighth = 0.125_realType

!!$       ! Threshold parameter for real types; the value depends
!!$       ! whether single or double precision is used.
!!$
!!$!#ifdef USE_SINGLE_PRECISION
!!$!       real(kind=realType), parameter :: thresholdReal = 1.e-5_realType
!!$!#else
!!$!       real(kind=realType), parameter :: thresholdReal = 1.e-10_realType
!!$!#endif
!!$
!!$       ! Definition of the tab and carriage return character.
!!$
!!$       character(len=1), parameter :: tabChar = achar(9)
!!$       character(len=1), parameter :: retChar = achar(13)

!****************************************************************

       real(kind=realType), dimension(3), intent(out) :: cFp, cFv
       real(kind=realType), dimension(3), intent(out) :: cMp, cMv

       real(kind=realType), dimension(:,:,:), intent(in) :: p,w



       real(kind=realType), intent(out) :: yplusMax
!
!      Local variables.
!
       integer(kind=intType) :: nn, i, j

       real(kind=realType) :: pm1, fx, fy, fz, fn
       real(kind=realType) :: xc, yc, zc
       real(kind=realType) :: fact, rho, mul, yplus, dwall

       real(kind=realType) :: tauXx, tauYy, tauZz
       real(kind=realType) :: tauXy, tauXz, tauYz

       real(kind=realType), dimension(3) :: refPoint

       real(kind=realType), dimension(:,:),   pointer :: pp2, pp1
       real(kind=realType), dimension(:,:),   pointer :: rho2, rho1
       real(kind=realType), dimension(:,:),   pointer :: rlv2, rlv1
       real(kind=realType), dimension(:,:),   pointer :: dd2Wall
       real(kind=realType), dimension(:,:,:), pointer :: ss, xx
       real(kind=realType), dimension(:,:,:), pointer :: norm

       logical :: viscousSubface
!
!      ******************************************************************
!      *                                                                *
!      * Begin execution                                                *
!      *                                                                *
!      ******************************************************************
!
       ! Determine the reference point for the moment computation in
       ! meters.

       refPoint(1) = LRef*pointRef(1)
       refPoint(2) = LRef*pointRef(2)
       refPoint(3) = LRef*pointRef(3)

       ! Initialize the force and moment coefficients to 0 as well as
       ! yplusMax.

       cFp(1) = zero; cFp(2) = zero; cFp(3) = zero
       cFv(1) = zero; cFv(2) = zero; cFv(3) = zero
       cMp(1) = zero; cMp(2) = zero; cMp(3) = zero
       cMv(1) = zero; cMv(2) = zero; cMv(3) = zero

       yplusMax = zero

       ! Loop over the boundary subfaces of this block.

       bocos: do nn=1,nBocos
!
!        ****************************************************************
!        *                                                              *
!        * Integrate the inviscid contribution over the solid walls,    *
!        * either inviscid or viscous. The integration is done with     *
!        * cp. For closed contours this is equal to the integration     *
!        * of p; for open contours this is not the case anymore.        *
!        * Question is whether a force for an open contour is           *
!        * meaningful anyway.                                           *
!        *                                                              *
!        ****************************************************************
!
         invForce: if(BCType(nn) == EulerWall        .or. &
                       BCType(nn) == NSWallAdiabatic .or. &
                       BCType(nn) == NSWallIsothermal) then

           ! Subface is a wall. Check if it is a viscous wall.

           viscousSubface = .true.
           if(BCType(nn) == EulerWall) viscousSubface = .false.

           ! Set a bunch of pointers depending on the face id to make
           ! a generic treatment possible. The routine setBcPointers
           ! is not used, because quite a few other ones are needed.

           select case (BCFaceID(nn))

             case (iMin)
               pp2  => p(2,1:,1:);      pp1  => p(1,1:,1:)
               rho2 => w(2,1:,1:,irho); rho1 => w(1,1:,1:,irho)
               ss   => si(1,:,:,:);     xx   => x(1,:,:,:)
               fact = -one

               if(equations == RANSEquations) dd2Wall => d2Wall(2,:,:)
               if( viscousSubface ) then
                 rlv2 => rlv(2,1:,1:); rlv1 => rlv(1,1:,1:)
               endif

             !===========================================================

             case (iMax)
               pp2  => p(il,1:,1:);      pp1  => p(ie,1:,1:)
               rho2 => w(il,1:,1:,irho); rho1 => w(ie,1:,1:,irho)
               ss   => si(il,:,:,:);     xx   => x(il,:,:,:)
               fact = one

               if(equations == RANSEquations) dd2Wall => d2Wall(il,:,:)
               if( viscousSubface ) then
                 rlv2 => rlv(il,1:,1:); rlv1 => rlv(ie,1:,1:)
               endif

             !===========================================================

             case (jMin)
               pp2  => p(1:,2,1:);      pp1  => p(1:,1,1:)
               rho2 => w(1:,2,1:,irho); rho1 => w(1:,1,1:,irho)
               ss   => sj(:,1,:,:);     xx   => x(:,1,:,:)
               fact = -one

               if(equations == RANSEquations) dd2Wall => d2Wall(:,2,:)
               if( viscousSubface ) then
                 rlv2 => rlv(1:,2,1:); rlv1 => rlv(1:,1,1:)
               endif

             !===========================================================

             case (jMax)
               pp2  => p(1:,jl,1:);      pp1  => p(1:,je,1:)
               rho2 => w(1:,jl,1:,irho); rho1 => w(1:,je,1:,irho)
               ss   => sj(:,jl,:,:);     xx   => x(:,jl,:,:)
               fact = one

               if(equations == RANSEquations) dd2Wall => d2Wall(:,jl,:)
               if( viscousSubface ) then
                 rlv2 => rlv(1:,jl,1:); rlv1 => rlv(1:,je,1:)
               endif

             !===========================================================

             case (kMin)
               pp2  => p(1:,1:,2);      pp1  => p(1:,1:,1)
               rho2 => w(1:,1:,2,irho); rho1 => w(1:,1:,1,irho)
               ss   => sk(:,:,1,:);     xx   => x(:,:,1,:)
               fact = -one

               if(equations == RANSEquations) dd2Wall => d2Wall(:,:,2)
               if( viscousSubface ) then
                 rlv2 => rlv(1:,1:,2); rlv1 => rlv(1:,1:,1)
               endif

             !===========================================================

             case (kMax)
               pp2  => p(1:,1:,kl);      pp1  => p(1:,1:,ke)
               rho2 => w(1:,1:,kl,irho); rho1 => w(1:,1:,ke,irho)
               ss   => sk(:,:,kl,:);     xx   => x(:,:,kl,:)
               fact = one

               if(equations == RANSEquations) dd2Wall => d2Wall(:,:,kl)
               if( viscousSubface ) then
                 rlv2 => rlv(1:,1:,kl); rlv1 => rlv(1:,1:,ke)
               endif

           end select

           ! Loop over the quadrilateral faces of the subface. Note
           ! that the nodal range of BCData must be used and not the
           ! cell range, because the latter may include the halo's in i
           ! and j-direction. The offset +1 is there, because inBeg and
           ! jnBeg refer to nodal ranges and not to cell ranges.

           do j=(BCData(nn)%jnBeg+1),BCData(nn)%jnEnd
             do i=(BCData(nn)%inBeg+1),BCData(nn)%inEnd

               ! Compute the average pressure minus 1 and the coordinates
               ! of the centroid of the face relative from from the
               ! moment reference point. Due to the usage of pointers for
               ! the coordinates, whose original array starts at 0, an
               ! offset of 1 must be used. The pressure is multipled by
               ! fact to account for the possibility of an inward or
               ! outward pointing normal.

               pm1 = fact*(half*(pp2(i,j) + pp1(i,j)) - pInf)

               xc = fourth*(xx(i,j,  1) + xx(i+1,j,  1) &
                  +         xx(i,j+1,1) + xx(i+1,j+1,1)) - refPoint(1)
               yc = fourth*(xx(i,j,  2) + xx(i+1,j,  2) &
                  +         xx(i,j+1,2) + xx(i+1,j+1,2)) - refPoint(2)
               zc = fourth*(xx(i,j,  3) + xx(i+1,j,  3) &
                  +         xx(i,j+1,3) + xx(i+1,j+1,3)) - refPoint(3)

               ! Compute the force components.

               fx = pm1*ss(i,j,1)
               fy = pm1*ss(i,j,2)
               fz = pm1*ss(i,j,3)

               ! Update the inviscid force and moment coefficients.

               cFp(1) = cFp(1) + fx
               cFp(2) = cFp(2) + fy
               cFp(3) = cFp(3) + fz

               cMp(1) = cMp(1) + yc*fz - zc*fy
               cMp(2) = cMp(2) + zc*fx - xc*fz
               cMp(3) = cMp(3) + xc*fy - yc*fx

             enddo
           enddo
!
!          **************************************************************
!          *                                                            *
!          * Integration of the viscous forces.                         *
!          * Only for viscous boundaries.                               *
!          *                                                            *
!          **************************************************************
!
           visForce: if( viscousSubface ) then

             ! Initialize dwall for the laminar case and set the pointer
             ! for the unit normals.

             dwall = zero
             norm => BCData(nn)%norm

             ! Loop over the quadrilateral faces of the subface and
             ! compute the viscous contribution to the force and
             ! moment and update the maximum value of y+.

             do j=(BCData(nn)%jnBeg+1),BCData(nn)%jnEnd
               do i=(BCData(nn)%inBeg+1),BCData(nn)%inEnd

                 ! Store the viscous stress tensor a bit easier.

                 tauXx = viscSubface(nn)%tau(i,j,1)
                 tauYy = viscSubface(nn)%tau(i,j,2)
                 tauZz = viscSubface(nn)%tau(i,j,3)
                 tauXy = viscSubface(nn)%tau(i,j,4)
                 tauXz = viscSubface(nn)%tau(i,j,5)
                 tauYz = viscSubface(nn)%tau(i,j,6)

                 ! Compute the viscous force on the face. A minus sign
                 ! is now present, due to the definition of this force.

                 fx = -fact*(tauXx*ss(i,j,1) + tauXy*ss(i,j,2) &
                    +        tauXz*ss(i,j,3))
                 fy = -fact*(tauXy*ss(i,j,1) + tauYy*ss(i,j,2) &
                    +        tauYz*ss(i,j,3))
                 fz = -fact*(tauXz*ss(i,j,1) + tauYz*ss(i,j,2) &
                    +        tauZz*ss(i,j,3))

                 ! Compute the coordinates of the centroid of the face
                 ! relative from the moment reference point. Due to the
                 ! usage of pointers for xx and offset of 1 is present,
                 ! because x originally starts at 0.

                 xc = fourth*(xx(i,j,  1) + xx(i+1,j,  1) &
                    +         xx(i,j+1,1) + xx(i+1,j+1,1)) - refPoint(1)
                 yc = fourth*(xx(i,j,  2) + xx(i+1,j,  2) &
                    +         xx(i,j+1,2) + xx(i+1,j+1,2)) - refPoint(2)
                 zc = fourth*(xx(i,j,  3) + xx(i+1,j,  3) &
                    +         xx(i,j+1,3) + xx(i+1,j+1,3)) - refPoint(3)

                 ! Update the viscous force and moment coefficients.

                 cFv(1) = cFv(1) + fx
                 cFv(2) = cFv(2) + fy
                 cFv(3) = cFv(3) + fz

                 cMv(1) = cMv(1) + yc*fz - zc*fy
                 cMv(2) = cMv(2) + zc*fx - xc*fz
                 cMv(3) = cMv(3) + xc*fy - yc*fx

                 ! Compute the tangential component of the stress tensor,
                 ! which is needed to monitor y+. The result is stored
                 ! in fx, fy, fz, although it is not really a force.
                 ! As later on only the magnitude of the tangential
                 ! component is important, there is no need to take the
                 ! sign into account (it should be a minus sign).

                 fx = tauXx*norm(i,j,1) + tauXy*norm(i,j,2) &
                    + tauXz*norm(i,j,3)
                 fy = tauXy*norm(i,j,1) + tauYy*norm(i,j,2) &
                    + tauYz*norm(i,j,3)
                 fz = tauXz*norm(i,j,1) + tauYz*norm(i,j,2) &
                    + tauZz*norm(i,j,3)

                 fn = fx*norm(i,j,1) + fy*norm(i,j,2) + fz*norm(i,j,3)

                 fx = fx - fn*norm(i,j,1)
                 fy = fy - fn*norm(i,j,2)
                 fz = fz - fn*norm(i,j,3)

                 ! Compute the local value of y+. Due to the usage
                 ! of pointers there is on offset of -1 in dd2Wall..

                 if(equations == RANSEquations) dwall = dd2Wall(i-1,j-1)

                 rho   = half*(rho2(i,j) + rho1(i,j))
                 mul   = half*(rlv2(i,j) + rlv1(i,j))
                 yplus = sqrt(rho*sqrt(fx*fx + fy*fy + fz*fz))*dwall/mul

                 ! Store this value if this value is larger than the
                 ! currently stored value.

                 yplusMax = max(yplusMax, yplus)

               enddo
             enddo

           endif visForce
         endif invForce

       enddo bocos

       ! Currently the coefficients only contain the surface integral
       ! of the pressure tensor. These values must be scaled to
       ! obtain the correct coefficients.

       fact = two/(gammaInf*pInf*MachCoef*MachCoef &
            *      surfaceRef*LRef*LRef)
       cFp(1) = cFp(1)*fact; cFp(2) = cFp(2)*fact; cFp(3) = cFp(3)*fact
       cFv(1) = cFv(1)*fact; cFv(2) = cFv(2)*fact; cFv(3) = cFv(3)*fact

       fact = fact/(lengthRef*LRef)
       cMp(1) = cMp(1)*fact; cMp(2) = cMp(2)*fact; cMp(3) = cMp(3)*fact
       cMv(1) = cMv(1)*fact; cMv(2) = cMv(2)*fact; cMv(3) = cMv(3)*fact

       end subroutine forcesAndMoments

end program forces
