!Compile: pgf90 -c eigvvcgm.f90
!*******************************************************************************
subroutine cg ( n, ar, ai, wr, wi, matz, zr, zi, ierr )
!*******************************************************************************
!
!! CG gets eigenvalues and eigenvectors of a complex general matrix.
!
!
!  Discussion:
!
!    This subroutine calls the recommended sequence of EISPACK subroutines
!    to find the eigenvalues and eigenvectors (if desired)
!    of a complex general matrix.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input/output, real AR(N,N), AI(N,N).  On input, the real and 
!    imaginary parts of the complex matrix.  On output, AR and AI
!    have been overwritten by other information.
!
!    Output, real WR(N), WI(N), the real and imaginary parts
!    of the eigenvalues.
!
!    Input, integer MATZ, is 0 if only eigenvalues are desired, and
!    nonzero if both eigenvalues and eigenvectors are to be computed.
!
!    Output, real ZR(N,N), ZI(N,N), the real and imaginary parts,
!    respectively, of the eigenvectors, if MATZ is not zero.
!
!    Output, integer IERR, an error completion code described in the 
!    documentation for COMQR and COMQR2.  The normal completion code is zero.
!
  implicit none
!
  integer n
!
  real ai(n,n)
  real ar(n,n)
  real fv1(n)
  real fv2(n)
  real fv3(n)
  integer ierr
  integer is1
  integer is2
  integer matz
  real wi(n)
  real wr(n)
  real zi(n,n)
  real zr(n,n)
!
  call cbal ( n, ar, ai, is1, is2, fv1 )

  call corth ( n, is1, is2, ar, ai, fv2, fv3 )

  if ( matz == 0 ) then

    call comqr ( n, is1, is2, ar, ai, wr, wi, ierr )

    if ( ierr /= 0 ) then
      return
    end if
  
  else

    call comqr2 ( n, is1, is2, fv2, fv3, ar, ai, wr, wi, zr, zi, ierr )

    if ( ierr /= 0 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'CG - Fatal error!'
      write ( *, '(a)' ) '  Nonzero error return from COMQR2.'
      return
    end if

    call cbabk2 ( n, is1, is2, fv1, n, zr, zi )

  end if

  return
end

!*******************************************************************************
subroutine cbal ( n, ar, ai, low, igh, scale )
!*******************************************************************************
!
!! CBAL balances a complex matrix before eigenvalue calculations.
!
!
!  Discussion:
!
!    This subroutine balances a complex matrix and isolates
!    eigenvalues whenever possible.
!
!    Suppose that the principal submatrix in rows low through igh
!    has been balanced, that P(J) denotes the index interchanged
!    with J during the permutation step, and that the elements
!    of the diagonal matrix used are denoted by D(I,J).  Then
!      SCALE(J) = P(J),    for J = 1,...,LOW-1
!               = D(J,J)       J = LOW,...,IGH
!               = P(J)         J = IGH+1,...,N.
!    The order in which the interchanges are made is N to IGH+1,
!    then 1 to LOW-1.
!
!    Note that 1 is returned for IGH if IGH is zero formally.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input/output, real AR(N,N), AI(N,N).  On input, the real and
!    imaginary parts of the complex matrix to be balanced.  On output,
!    the real and imaginary parts of the balanced matrix.
!
!    Output, integer LOW, IGH, are values such that AR(I,J) and AI(I,J)
!    are zero if I is greater than J and either J=1,...,LOW-1 or 
!    I=IGH+1,...,N.
!
!    Output, real SCALE(N), information determining the
!    permutations and scaling factors used.
!
  implicit none
!
  integer n
!
  real ai(n,n)
  real ar(n,n)
  real b2
  real c
  real f
  real g
  integer i
  integer iexc
  integer igh
  integer j
  integer jj
  integer k
  integer l
  integer low
  integer m
  logical noconv
  real r
  real radix
  real s
  real scale(n)
!
  radix = 16.0E+00

  iexc = 0
  j = 0
  m = 0

  b2 = radix * radix
  k = 1
  l = n
  go to 100

20 continue

  scale(m) = j

  if ( j /= m ) then

    do i = 1, l
      call r_swap ( ar(i,j), ar(i,m) )
      call r_swap ( ai(i,j), ai(i,m) )
    end do

    do i = k, n
      call r_swap ( ar(j,i), ar(m,i) )
      call r_swap ( ai(j,i), ai(m,i) )
    end do

  end if

  if ( iexc == 2 ) then
    go to 130
  end if
!
!  Search for rows isolating an eigenvalue and push them down.
!
80 continue

  if ( l == 1 ) then
    go to 280
  end if

  l = l - 1

100 continue

  do jj = 1, l

     j = l + 1 - jj

     do i = 1, l
       if ( i /= j ) then
         if ( ar(j,i) /= 0.0E+00 .or. ai(j,i) /= 0.0E+00 ) go to 120
       end if
     end do

     m = l
     iexc = 1
     go to 20

120  continue

  end do

  go to 140
!
!  Search for columns isolating an eigenvalue and push them left.
!
130 continue

  k = k + 1

140 continue

   do j = k, l

     do i = k, l
       if ( i /= j ) then
         if ( ar(i,j) /= 0.0E+00 .or. ai(i,j) /= 0.0E+00 ) go to 170
       end if
     end do

     m = k
     iexc = 2
     go to 20
170  continue

  end do
!
!  Now balance the submatrix in rows k to l.
!
  scale(k:l) = 1.0E+00
!
!  Iterative loop for norm reduction.
!
190 continue

  noconv = .false.

  do i = k, l

    c = 0.0E+00
    r = 0.0E+00

    do j = k, l
      if ( j /= i ) then
        c = c + abs ( ar(j,i) ) + abs ( ai(j,i) )
        r = r + abs ( ar(i,j) ) + abs ( ai(i,j) )
      end if
    end do
!
!  Guard against zero C or R due to underflow.
!
     if ( c == 0.0E+00 .or. r == 0.0E+00 ) go to 270

     g = r / radix
     f = 1.0E+00
     s = c + r

     do while ( c < g )
       f = f * radix
       c = c * b2
     end do

     g = r * radix

     do while  ( c >= g )
       f = f / radix
       c = c / b2
     end do
!
!  Now balance.
!
     if ( ( c + r ) / f < 0.95E+00 * s ) then

       g = 1.0E+00 / f
       scale(i) = scale(i) * f
       noconv = .true.

       ar(i,k:n) = ar(i,k:n) * g
       ai(i,k:n) = ai(i,k:n) * g

       ar(1:l,i) = ar(1:l,i) * f
       ai(1:l,i) = ai(1:l,i) * f

     end if

270  continue

  end do

  if ( noconv ) go to 190

  280 continue

  low = k
  igh = l

  return
end

!*******************************************************************************
subroutine r_swap ( x, y )
!*******************************************************************************
!
!! R_SWAP switches two real values.
!
!
!  Modified:
!
!    04 February 2003
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input/output, real X, Y.  On output, the values of X and
!    Y have been interchanged.
!
  implicit none
!
  real x
  real y
  real z
!
  z = x
  x = y
  y = z

  return
end

!*******************************************************************************
subroutine corth ( n, low, igh, ar, ai, ortr, orti )
!*******************************************************************************
!
!! CORTH transforms a complex general matrix to upper Hessenberg form.
!
!
!  Discussion:
!
!    Given a complex general matrix, this subroutine
!    reduces a submatrix situated in rows and columns
!    LOW through IGH to upper Hessenberg form by
!    unitary similarity transformations.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input, integer LOW, IGH, are determined by the balancing routine CBAL.
!    If CBAL is not used, set LOW = 1 and IGH = N.
!
!    Input/output, real AR(N,N), AI(N,N).  On input, the real and imaginary
!    parts of the complex input matrix.  On output, the real and imaginary
!    parts of the Hessenberg matrix.  Information about the unitary
!    transformations used in the reduction is stored in the remaining 
!    triangles under the Hessenberg matrix.
!
!    Output, real ORTR(IGH), ORTI(IGH), further information about the
!    transformations.
!
  implicit none
!
  integer igh
  integer n
!
  real ai(n,n)
  real ar(n,n)
  real f
  real fi
  real fr
  real g
  real h
  integer i
  integer ii
  integer j
  integer jj
  integer la
  integer m,mp,low
  real orti(igh)
  real ortr(igh)
  real pythag
  real scale
!
  la = igh - 1

  if ( igh - 1 < low + 1 ) then
    return
  end if

  do m = low + 1, la

    h = 0.0E+00
    ortr(m) = 0.0E+00
    orti(m) = 0.0E+00
    scale = 0.0E+00
!
!  Scale column.
!
    do i = m, igh
      scale = scale + abs ( ar(i,m-1) ) + abs ( ai(i,m-1) )
    end do

    if ( scale == 0.0E+00 ) then
      cycle
    end if

    mp = m + igh

    do ii = m, igh
      i = mp - ii
      ortr(i) = ar(i,m-1) / scale
      orti(i) = ai(i,m-1) / scale
      h = h + ortr(i) * ortr(i) + orti(i) * orti(i)
    end do

    g = sqrt ( h )
    f = pythag ( ortr(m), orti(m) )

    if ( f /= 0.0E+00 ) then
      h = h + f * g
      g = g / f
      ortr(m) = ( 1.0E+00 + g ) * ortr(m)
      orti(m) = ( 1.0E+00 + g ) * orti(m)
    else
      ortr(m) = g
      ar(m,m-1) = scale
    end if
!
!  Form (I-(U*Ut)/h) * A.
!
    do j = m, n

      fr = 0.0E+00
      fi = 0.0E+00

      do ii = m, igh
        i = mp - ii
        fr = fr + ortr(i) * ar(i,j) + orti(i) * ai(i,j)
        fi = fi + ortr(i) * ai(i,j) - orti(i) * ar(i,j)
      end do

      fr = fr / h
      fi = fi / h

      ar(m:igh,j) = ar(m:igh,j) - fr * ortr(m:igh) + fi * orti(m:igh)
      ai(m:igh,j) = ai(m:igh,j) - fr * orti(m:igh) - fi * ortr(m:igh)

    end do
!
!  Form (I-(U*Ut)/h) * A * (I-(U*Ut)/h)
!
    do i = 1, igh

      fr = 0.0E+00
      fi = 0.0E+00

      do jj = m, igh
        j = mp - jj
        fr = fr + ortr(j) * ar(i,j) - orti(j) * ai(i,j)
        fi = fi + ortr(j) * ai(i,j) + orti(j) * ar(i,j)
      end do

      fr = fr / h
      fi = fi / h

      ar(i,m:igh) = ar(i,m:igh) - fr * ortr(m:igh) - fi * orti(m:igh)
      ai(i,m:igh) = ai(i,m:igh) + fr * orti(m:igh) - fi * ortr(m:igh)

    end do

    ortr(m) = scale * ortr(m)
    orti(m) = scale * orti(m)
    ar(m,m-1) = - g * ar(m,m-1)
    ai(m,m-1) = - g * ai(m,m-1)

  end do

  return
end

!*******************************************************************************
subroutine comqr ( n, low, igh, hr, hi, wr, wi, ierr )
!*******************************************************************************
!
!! COMQR gets eigenvalues of a complex upper Hessenberg matrix.
!
!
!  Discussion:
!
!    This subroutine finds the eigenvalues of a complex
!    upper Hessenberg matrix by the QR method.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input, integer LOW, IGH, are determined by the balancing routine CBAL.
!    If CBAL is not used, set LOW = 1 and IGH = N.
!
!    Input/output, real HR(N,N), HI(N,N).  On input, the real and imaginary 
!    parts of the complex upper Hessenberg matrix.  Their lower triangles
!    below the subdiagonal contain information about the unitary 
!    transformations used in the reduction by CORTH, if performed.  On output,
!    the upper Hessenberg portions of HR and HI have been destroyed.  
!    Therefore, they must be saved before calling COMQR if subsequent
!    calculation of eigenvectors is to be performed.
!
!    Output, real WR(N), WI(N), the real and imaginary parts of the 
!    eigenvalues.  If an error exit is made, the eigenvalues should be
!    correct for indices IERR+1,...,N.
!
!    Output, integer IERR, error flag.
!    0, for normal return,
!    J, if the limit of 30*N iterations is exhausted while the J-th 
!       eigenvalue is being sought.
!
  implicit none
!
  integer n
!
  integer en
  integer enm1
  real hi(n,n)
  real hr(n,n)
  integer i
  integer ierr
  integer igh
  integer itn
  integer its
  integer j
  integer l
  integer ll
  integer low
  real norm
  real pythag
  real si
  real sr
  real ti
  real tr
  real tst1
  real tst2
  real wi(n)
  real wr(n)
  real xi
  real xr
  real yi
  real yr
  real zzi
  real zzr
!
  ierr = 0
!
!  Create real subdiagonal elements.
!
  l = low + 1

  do i = l, igh

     ll = min ( i+1, igh )

     if ( hi(i,i-1) /= 0.0E+00 ) then

     norm = pythag ( hr(i,i-1), hi(i,i-1) )
     yr = hr(i,i-1) / norm
     yi = hi(i,i-1) / norm
     hr(i,i-1) = norm
     hi(i,i-1) = 0.0E+00

     do j = i, igh
       si = yr * hi(i,j) - yi * hr(i,j)
       hr(i,j) = yr * hr(i,j) + yi * hi(i,j)
       hi(i,j) = si
     end do

     do j = low, ll
       si = yr * hi(j,i) + yi * hr(j,i)
       hr(j,i) = yr * hr(j,i) - yi * hi(j,i)
       hi(j,i) = si
     end do

    end if

  end do
!
!  Store roots isolated by CBAL.
!
  do i = 1, n
    if ( i < low .or. i > igh ) then
      wr(i) = hr(i,i)
      wi(i) = hi(i,i)
    end if
  end do

  en = igh
  tr = 0.0E+00
  ti = 0.0E+00
  itn = 30 * n
!
!  Search for next eigenvalue.
!
  220 continue

  if ( en < low ) then
    return
  end if

  its = 0
  enm1 = en - 1
!
!  Look for single small sub-diagonal element.
!
  240 continue

  do ll = low, en
    l = en + low - ll
    if ( l == low ) then
      exit
    end if
    tst1 = abs ( hr(l-1,l-1) ) + abs ( hi(l-1,l-1) ) + abs ( hr(l,l) ) &
      + abs ( hi(l,l) )
    tst2 = tst1 + abs ( hr(l,l-1) )
    if ( tst2 == tst1 ) then
      exit
    end if
  end do
!
!  Form shift.
!
  if ( l == en ) then
    go to 660
  end if

  if ( itn == 0 ) go to 1000

  if ( its == 10 .or. its == 20 ) go to 320
  sr = hr(en,en)
  si = hi(en,en)
  xr = hr(enm1,en) * hr(en,enm1)
  xi = hi(enm1,en) * hr(en,enm1)
  if ( xr == 0.0E+00 .and. xi == 0.0E+00 ) go to 340
  yr = (hr(enm1,enm1) - sr) / 2.0E+00
  yi = (hi(enm1,enm1) - si) / 2.0E+00

  call csroot ( yr**2-yi**2+xr, 2.0E+00*yr*yi+xi, zzr, zzi )

  if ( yr * zzr + yi * zzi < 0.0E+00 ) then
    zzr = -zzr
    zzi = -zzi
  end if

  call cdiv ( xr, xi, yr+zzr, yi+zzi, xr, xi )
  sr = sr - xr
  si = si - xi
  go to 340
!
!  Form exceptional shift.
!
320 continue

  sr = abs ( hr(en,enm1) ) + abs ( hr(enm1,en-2) )
  si = 0.0E+00

340 continue

  do i = low, en
    hr(i,i) = hr(i,i) - sr
    hi(i,i) = hi(i,i) - si
  end do

  tr = tr + sr
  ti = ti + si
  its = its + 1
  itn = itn - 1
!
!  Reduce to triangle (rows).
!
  do i = l+1, en

     sr = hr(i,i-1)
     hr(i,i-1) = 0.0E+00
     norm = pythag ( pythag ( hr(i-1,i-1), hi(i-1,i-1) ), sr )
     xr = hr(i-1,i-1) / norm
     wr(i-1) = xr
     xi = hi(i-1,i-1) / norm
     wi(i-1) = xi
     hr(i-1,i-1) = norm
     hi(i-1,i-1) = 0.0E+00
     hi(i,i-1) = sr / norm

     do j = i, en
        yr = hr(i-1,j)
        yi = hi(i-1,j)
        zzr = hr(i,j)
        zzi = hi(i,j)
        hr(i-1,j) = xr * yr + xi * yi + hi(i,i-1) * zzr
        hi(i-1,j) = xr * yi - xi * yr + hi(i,i-1) * zzi
        hr(i,j) = xr * zzr - xi * zzi - hi(i,i-1) * yr
        hi(i,j) = xr * zzi + xi * zzr - hi(i,i-1) * yi
    end do

  end do

  si = hi(en,en)

  if ( si /= 0.0E+00 ) then
    norm = pythag ( hr(en,en), si )
    sr = hr(en,en) / norm
    si = si / norm
    hr(en,en) = norm
    hi(en,en) = 0.0E+00
  end if
!
!  Inverse operation (columns).
!
  do j = l+1, en

     xr = wr(j-1)
     xi = wi(j-1)

     do i = l, j

        yr = hr(i,j-1)
        yi = 0.0E+00
        zzr = hr(i,j)
        zzi = hi(i,j)
        if ( i /= j ) then
          yi = hi(i,j-1)
          hi(i,j-1) = xr * yi + xi * yr + hi(j,j-1) * zzi
        end if
        hr(i,j-1) = xr * yr - xi * yi + hi(j,j-1) * zzr
        hr(i,j) = xr * zzr + xi * zzi - hi(j,j-1) * yr
        hi(i,j) = xr * zzi - xi * zzr - hi(j,j-1) * yi

     end do

  end do

  if ( si /= 0.0E+00 ) then

    do i = l, en
      yr = hr(i,en)
      yi = hi(i,en)
      hr(i,en) = sr * yr - si * yi
      hi(i,en) = sr * yi + si * yr
    end do

  end if

  go to 240
!
!  A root found.
!
660 continue

  wr(en) = hr(en,en) + tr
  wi(en) = hi(en,en) + ti
  en = enm1
  go to 220
!
!  Set error: all eigenvalues have not converged after 30*n iterations.
!
1000 continue

  ierr = en
  return
end

!*******************************************************************************
subroutine comqr2 ( n, low, igh, ortr, orti, hr, hi, wr, wi, zr, zi, ierr )
!*******************************************************************************
!
!! COMQR2 gets eigenvalues/vectors of a complex upper Hessenberg matrix.
!
!
!  Discussion:
!
!    This subroutine finds the eigenvalues and eigenvectors
!    of a complex upper Hessenberg matrix by the QR
!    method.  The eigenvectors of a complex general matrix
!    can also be found if CORTH has been used to reduce
!    this general matrix to Hessenberg form.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input, integer LOW, IGH, are determined by the balancing routine CBAL.
!    If CBAL is not used, set LOW = 1 and IGH = N.
!
!    Input/output, real ORTR(N), ORTI(N).  On input, information about the 
!    unitary transformations used in the reduction by CORTH, if performed.
!    If the eigenvectors of the Hessenberg matrix are desired, set ORTR(J) and
!    ORTI(J) to 0.0E+00 for these elements.  On output, these arrays
!    have been overwritten.
!
!    Input/output, real HR(N,N), HI(N,N).  On input, the real and imaginary
!    parts of the complex upper Hessenberg matrix.  Their lower triangles
!    below the subdiagonal contain further information about the 
!    transformations which were used in the reduction by CORTH, if performed.
!    If the eigenvectors of the Hessenberg matrix are desired, these elements
!    may be arbitrary.
!
!    Output, real WR(N), WI(N), the real and imaginary parts of the 
!    eigenvalues.  If an error exit is made, the eigenvalues should be
!    correct for indices IERR+1,...,N.
!
!    Output, real ZR(N,N), ZI(N,N), the real and imaginary parts of the 
!    eigenvectors.  The eigenvectors are unnormalized.  If an error exit
!    is made, none of the eigenvectors has been found.
!
!    Output, integer IERR, error flag.
!    0, for normal return,
!    J, if the limit of 30*N iterations is exhausted while the J-th 
!      eigenvalue is being sought.
!
  implicit none
!
  integer igh
  integer n
!
  integer en
  integer enm1
  real hi(n,n)
  real hr(n,n)
  integer i
  integer iend
  integer ierr
  integer ii
  integer itn
  integer its
  integer j
  integer jj
  integer k
  integer l
  integer ll
  integer low
  integer m
  integer nn
  real norm
  real orti(igh)
  real ortr(igh)
  real pythag
  real si
  real sr
  real ti
  real tr
  real tst1
  real tst2
  real wi(n)
  real wr(n)
  real xi
  real xr
  real yi
  real yr
  real zi(n,n)
  real zr(n,n)
  real zzi 
  real zzr
!
  ierr = 0
!
!  Initialize eigenvector matrix.
!
  call rmat_ident ( n, n, zr )

  zi(1:n,1:n) = 0.0E+00
!
!  Form the matrix of accumulated transformations from the information
!  left by CORTH.
!
  iend = igh - low - 1
  if ( iend ) 180, 150, 105

105 continue

  do ii = 1, iend

     i = igh - ii
     if ( ortr(i) == 0.0E+00 .and. orti(i) == 0.0E+00 ) go to 140
     if ( hr(i,i-1) == 0.0E+00 .and. hi(i,i-1) == 0.0E+00 ) go to 140
!
!  Norm below is negative of H formed in CORTH.
!
     norm = hr(i,i-1) * ortr(i) + hi(i,i-1) * orti(i)

     do k = i+1, igh
       ortr(k) = hr(k,i-1)
       orti(k) = hi(k,i-1)
     end do

     do j = i, igh

        sr = 0.0E+00
        si = 0.0E+00

        do k = i, igh
          sr = sr + ortr(k) * zr(k,j) + orti(k) * zi(k,j)
          si = si + ortr(k) * zi(k,j) - orti(k) * zr(k,j)
        end do

        sr = sr / norm
        si = si / norm

        do k = i, igh
          zr(k,j) = zr(k,j) + sr * ortr(k) - si * orti(k)
          zi(k,j) = zi(k,j) + sr * orti(k) + si * ortr(k)
        end do

      end do

140 continue

  end do
!
!  Create real subdiagonal elements.
!
150 continue

  l = low + 1

  do i = l, igh

     ll = min ( i+1, igh )

     if ( hi(i,i-1) == 0.0E+00 ) then
       go to 170
     end if

     norm = pythag ( hr(i,i-1), hi(i,i-1) )
     yr = hr(i,i-1) / norm
     yi = hi(i,i-1) / norm
     hr(i,i-1) = norm
     hi(i,i-1) = 0.0E+00

     do j = i, n
       si = yr * hi(i,j) - yi * hr(i,j)
       hr(i,j) = yr * hr(i,j) + yi * hi(i,j)
       hi(i,j) = si
     end do

     do j = 1, ll
       si = yr * hi(j,i) + yi * hr(j,i)
       hr(j,i) = yr * hr(j,i) - yi * hi(j,i)
       hi(j,i) = si
     end do

     do j = low, igh
       si = yr * zi(j,i) + yi * zr(j,i)
       zr(j,i) = yr * zr(j,i) - yi * zi(j,i)
       zi(j,i) = si
     end do

170 continue

  end do
!
!  Store roots isolated by CBAL.
!
180 continue

  do i = 1, n
    if ( i < low .or. i > igh) then
      wr(i) = hr(i,i)
      wi(i) = hi(i,i)
    end if
  end do

  en = igh
  tr = 0.0E+00
  ti = 0.0E+00
  itn = 30 * n
!
!  Search for next eigenvalue.
!
220 continue

  if ( en < low ) go to 680
  its = 0
  enm1 = en - 1
!
!  Look for single small sub-diagonal element.
!
240 continue

  do ll = low, en
    l = en + low - ll
    if ( l == low ) then
      exit
    end if
    tst1 = abs ( hr(l-1,l-1) ) + abs ( hi(l-1,l-1) ) + abs ( hr(l,l) ) &
      + abs ( hi(l,l) )
    tst2 = tst1 + abs ( hr(l,l-1) )
    if ( tst2 == tst1 ) then
      exit
    end if
  end do
!
!  Form shift.
!
  if ( l == en ) go to 660
  if ( itn == 0 ) go to 1000
  if ( its == 10 .or. its == 20 ) go to 320
  sr = hr(en,en)
  si = hi(en,en)
  xr = hr(enm1,en) * hr(en,enm1)
  xi = hi(enm1,en) * hr(en,enm1)
  if ( xr == 0.0E+00 .and. xi == 0.0E+00 ) go to 340
  yr = ( hr(enm1,enm1) - sr ) / 2.0E+00
  yi = ( hi(enm1,enm1) - si ) / 2.0E+00

  call csroot ( yr**2-yi**2+xr, 2.0E+00*yr*yi+xi, zzr, zzi )

  if ( yr * zzr + yi * zzi < 0.0E+00 ) then
    zzr = -zzr
    zzi = -zzi
  end if

  call cdiv ( xr, xi, yr+zzr, yi+zzi, xr, xi )
  sr = sr - xr
  si = si - xi
  go to 340
!
!  Form exceptional shift.
!
320 continue

  sr = abs ( hr(en,enm1) ) + abs ( hr(enm1,en-2) )
  si = 0.0E+00

340 continue

  do i = low, en
    hr(i,i) = hr(i,i) - sr
    hi(i,i) = hi(i,i) - si
  end do

  tr = tr + sr
  ti = ti + si
  its = its + 1
  itn = itn - 1
!
!  Reduce to triangle (rows).
!
  do i = l+1, en

     sr = hr(i,i-1)
     hr(i,i-1) = 0.0E+00
     norm = pythag ( pythag ( hr(i-1,i-1), hi(i-1,i-1) ), sr )
     xr = hr(i-1,i-1) / norm
     wr(i-1) = xr
     xi = hi(i-1,i-1) / norm
     wi(i-1) = xi
     hr(i-1,i-1) = norm
     hi(i-1,i-1) = 0.0E+00
     hi(i,i-1) = sr / norm

     do j = i, n
        yr = hr(i-1,j)
        yi = hi(i-1,j)
        zzr = hr(i,j)
        zzi = hi(i,j)
        hr(i-1,j) = xr * yr + xi * yi + hi(i,i-1) * zzr
        hi(i-1,j) = xr * yi - xi * yr + hi(i,i-1) * zzi
        hr(i,j) = xr * zzr - xi * zzi - hi(i,i-1) * yr
        hi(i,j) = xr * zzi + xi * zzr - hi(i,i-1) * yi
     end do

  end do

  si = hi(en,en)

  if ( si /= 0.0E+00 ) then

    norm = pythag ( hr(en,en), si )
    sr = hr(en,en) / norm
    si = si / norm
    hr(en,en) = norm
    hi(en,en) = 0.0E+00

    do j = en+1, n
      yr = hr(en,j)
      yi = hi(en,j)
      hr(en,j) = sr * yr + si * yi
      hi(en,j) = sr * yi - si * yr
    end do

  end if
!
!  Inverse operation (columns).
!
  do j = l+1, en

     xr = wr(j-1)
     xi = wi(j-1)

     do i = 1, j

       yr = hr(i,j-1)
       yi = 0.0E+00
       zzr = hr(i,j)
       zzi = hi(i,j)

       if ( i /= j ) then
         yi = hi(i,j-1)
         hi(i,j-1) = xr * yi + xi * yr + hi(j,j-1) * zzi
       end if

       hr(i,j-1) = xr * yr - xi * yi + hi(j,j-1) * zzr
       hr(i,j) = xr * zzr + xi * zzi - hi(j,j-1) * yr
       hi(i,j) = xr * zzi - xi * zzr - hi(j,j-1) * yi

     end do

     do i = low, igh
       yr = zr(i,j-1)
       yi = zi(i,j-1)
       zzr = zr(i,j)
       zzi = zi(i,j)
       zr(i,j-1) = xr * yr - xi * yi + hi(j,j-1) * zzr
       zi(i,j-1) = xr * yi + xi * yr + hi(j,j-1) * zzi
       zr(i,j) = xr * zzr + xi * zzi - hi(j,j-1) * yr
       zi(i,j) = xr * zzi - xi * zzr - hi(j,j-1) * yi
     end do

  end do

  if ( si /= 0.0E+00 ) then

    do i = 1, en
      yr = hr(i,en)
      yi = hi(i,en)
      hr(i,en) = sr * yr - si * yi
      hi(i,en) = sr * yi + si * yr
    end do

    do i = low, igh
      yr = zr(i,en)
      yi = zi(i,en)
      zr(i,en) = sr * yr - si * yi
      zi(i,en) = sr * yi + si * yr
    end do

  end if

  go to 240
!
!  A root found.
!
660 continue

  hr(en,en) = hr(en,en) + tr
  wr(en) = hr(en,en)
  hi(en,en) = hi(en,en) + ti
  wi(en) = hi(en,en)
  en = enm1
  go to 220
!
!  All roots found.
!  Backsubstitute to find vectors of upper triangular form.
!
680 continue

  norm = 0.0E+00

  do i = 1, n
    do j = i, n
      tr = abs ( hr(i,j) ) + abs ( hi(i,j) )
      norm = max ( norm, tr )
    end do
  end do

  if ( n == 1 ) then
    return
  end if

  if ( norm == 0.0E+00 ) then
    return
  end if

  do nn = 2, n

     en = n + 2 - nn
     xr = wr(en)
     xi = wi(en)
     hr(en,en) = 1.0E+00
     hi(en,en) = 0.0E+00
     enm1 = en - 1

     do ii = 1, enm1

        i = en - ii
        zzr = 0.0E+00
        zzi = 0.0E+00

        do j = i+1, en
          zzr = zzr + hr(i,j) * hr(j,en) - hi(i,j) * hi(j,en)
          zzi = zzi + hr(i,j) * hi(j,en) + hi(i,j) * hr(j,en)
        end do

        yr = xr - wr(i)
        yi = xi - wi(i)

        if ( yr == 0.0E+00 .and. yi == 0.0E+00 ) then

           tst1 = norm
           yr = tst1
           do
             yr = 0.01E+00 * yr
             tst2 = norm + yr
             if ( tst2 <= tst1 ) then
               exit
             end if
           end do

        end if

        call cdiv ( zzr, zzi, yr, yi, hr(i,en), hi(i,en) )
!
!  Overflow control.
!
        tr = abs ( hr(i,en) ) + abs ( hi(i,en) )

        if ( tr /= 0.0E+00 ) then

          tst1 = tr
          tst2 = tst1 + 1.0E+00 / tst1

          if ( tst2 <= tst1 ) then

            do j = i, en
              hr(j,en) = hr(j,en)/tr
              hi(j,en) = hi(j,en)/tr
            end do

          end if

       end if

     end do

  end do
!
!  End backsubstitution.
!
  enm1 = n - 1
!
!  Vectors of isolated roots.
!
  do i = 1, n - 1

    if ( i < low .or. i > igh ) then

      do j = i+1, n
        zr(i,j) = hr(i,j)
        zi(i,j) = hi(i,j)
      end do

    end if

  end do
!
!  Multiply by transformation matrix to give vectors of original full matrix.
!
  do jj = low, n - 1

     j = n + low - jj
     m = min ( j, igh )

     do i = low, igh

        zzr = 0.0E+00
        zzi = 0.0E+00
        do k = low, m
          zzr = zzr + zr(i,k) * hr(k,j) - zi(i,k) * hi(k,j)
          zzi = zzi + zr(i,k) * hi(k,j) + zi(i,k) * hr(k,j)
        end do

        zr(i,j) = zzr
        zi(i,j) = zzi

      end do

  end do

  return
!
!  Set error: all eigenvalues have not converged after 30*n iterations.
!
1000 continue

  ierr = en
  return
end


!*******************************************************************************
subroutine csroot ( xr, xi, yr, yi )
!*******************************************************************************
!
!! CSROOT computes the complex square root of a complex quantity.
!
!
!  Discussion:
!
!    The branch of the square function is chosen so that
!      YR >= 0.0E+00
!    and
!      sign ( YI ) == sign ( XI )
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, real XR, XI, the real and imaginary parts of the quantity
!    whose square root is desired.
!
!    Output, real YR, YI, the real and imaginary parts of the square root.
!
  implicit none
!
  real pythag
  real s
  real ti
  real tr
  real xi
  real xr
  real yi
  real yr
!
  tr = xr
  ti = xi
  s = sqrt ( 0.5E+00 * ( pythag ( tr, ti ) + abs ( tr ) ) )

  if ( tr >= 0.0E+00 ) yr = s
  if ( ti < 0.0E+00 ) s = -s
  if ( tr <= 0.0E+00 ) yi = s

  if ( tr < 0.0E+00 ) then
    yr = 0.5E+00 * ( ti / yi )
  else if ( tr > 0.0E+00 ) then
    yi = 0.5E+00 * ( ti / yr )
  end if

  return
end

!*******************************************************************************
subroutine cdiv ( ar, ai, br, bi, cr, ci )
!*******************************************************************************
!
!! CDIV emulates complex division, using real arithmetic.
!
!
!  Discussion:
!
!    This routine performs complex division:
!
!      (CR,CI) = (AR,AI) / (BR,BI)
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, real AR, AI, the real and imaginary parts of the numerator.
!
!    Input, real BR, BI, the real and imaginary parts of the denominator.
!
!    Output, real CR, CI, the real and imaginary parts of the result.
!
  implicit none
!
  real ai
  real ais
  real ar
  real ars
  real bi
  real bis
  real br
  real brs
  real ci
  real cr
  real s
!
  s = abs ( br ) + abs ( bi )

  ars = ar / s
  ais = ai / s
  brs = br / s
  bis = bi / s

  s = brs**2 + bis**2
  cr = ( ars * brs + ais * bis ) / s
  ci = ( ais * brs - ars * bis ) / s

  return
end

!*******************************************************************************
subroutine rmat_ident ( lda, n, a )
!*******************************************************************************
!
!! RMAT_IDENT sets the square matrix A to the identity.
!
!
!  Modified:
!
!    04 February 2003
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer LDA, the leading dimension of A.
!
!    Input, integer N, the order of A.
!
!    Output, real A(LDA,N), the matrix which has been
!    set to the identity.
!
  implicit none
!
  integer lda
  integer n
!
  real a(lda,n)
  integer i
  integer j
!
  do i = 1, n
    do j = 1, n
      if ( i == j ) then
        a(i,j) = 1.0E+00
      else
        a(i,j) = 0.0E+00
      end if
    end do
  end do

  return
end

!*******************************************************************************
subroutine cbabk2 ( n, low, igh, scale, m, zr, zi )
!*******************************************************************************
!
!! CBABK2 finds eigenvectors by undoing the CBAL transformation.
!
!
!  Discussion:
!
!    This subroutine forms the eigenvectors of a complex general
!    matrix by back transforming those of the corresponding
!    balanced matrix determined by CBAL.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Parameters:
!
!    Input, integer N, the order of the matrix.
!
!    Input, integer LOW, IGH, values determined by CBAL.
!
!    Input, real SCALE(N), information determining the permutations
!    and scaling factors used by CBAL.
!
!    Input, integer M, the number of eigenvectors to be back transformed.
!
!    Input/output, real ZR(N,M), ZI(N,M).  On input, the real and imaginary 
!    parts, respectively, of the eigenvectors to be back transformed in
!    their first M columns.  On output, the transformed eigenvectors.
!
  implicit none
!
  integer m
  integer n
!
  integer i
  integer igh
  integer ii
  integer j
  integer k
  integer low
  real s
  real scale(n)
  real zi(n,m)
  real zr(n,m)
!
  if ( m == 0 ) then
    return
  end if

  if ( igh /= low ) then

    do i = low, igh

      s = scale(i)

      zr(i,1:m) = zr(i,1:m) * s
      zi(i,1:m) = zi(i,1:m) * s

    end do

  end if

  do ii = 1, n

    i = ii

    if ( i < low .or. i > igh ) then

      if ( i < low ) then
        i = low - ii
      end if

      k = scale(i)

      if ( k /= i ) then

        do j = 1, m
          call r_swap ( zr(i,j), zr(k,j) )
          call r_swap ( zi(i,j), zi(k,j) )
        end do

      end if

    end if

  end do

  return
end

!*******************************************************************************
function pythag ( a, b )
!*******************************************************************************
!
!! PYTHAG computes SQRT ( A**2 + B**2 ) carefully.
!
!
!  Discussion:
!
!    The formula
!
!      PYTHAG = sqrt ( A**2 + B**2 )
!
!    is reasonably accurate, but can fail if, for example, A**2 is larger
!    than the machine overflow.  The formula can lose most of its accuracy
!    if the sum of the squares is very large or very small.
!
!  Modified:
!
!    04 February 2003
!
!  Reference:
!
!    J H Wilkinson and C Reinsch,
!    Handbook for Automatic Computation,
!    Volume II, Linear Algebra, Part 2,
!    Springer Verlag, 1971.
!
!    B Smith, J Boyle, J Dongarra, B Garbow, Y Ikebe, V Klema, C Moler,
!    Matrix Eigensystem Routines, EISPACK Guide,
!    Lecture Notes in Computer Science, Volume 6,
!    Springer Verlag, 1976.
!
!  Modified:
!
!    04 February 2003
!
!  Parameters:
!
!    Input, real A, B, the two legs of a right triangle.
!
!    Output, real PYTHAG, the length of the hypotenuse.
!
  implicit none
!
  real a
  real b
  real p
  real pythag
  real r
  real s
  real t
  real u
!
  p = max ( abs ( a ), abs ( b ) )

  if ( p /= 0.0E+00 ) then

    r = ( min ( abs ( a ), abs ( b ) ) / p )**2

    do

      t = 4.0E+00 + r

      if ( t == 4.0E+00 ) then
        exit
      end if

      s = r / t
      u = 1.0E+00 + 2.0E+00 * s
      p = u * p
      r = ( s / u )**2 * r

    end do

  end if

  pythag = p

  return
end
!*******************************************************************************
