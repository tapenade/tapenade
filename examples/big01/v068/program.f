*--------------------------------------------------------------------------*
* Function subroutine. 
*--------------------------------------------------------------------------*
      SUBROUTINE ADJ_FCN(T,Y,YP,RESULT,RP)
      IMPLICIT NONE
      DOUBLE PRECISION PI
      PARAMETER (PI=3.14159265358979323846)
      INTEGER NEQ
      PARAMETER (NEQ=61)
      DOUBLE PRECISION T,Y,YP,RESULT,RP
      DOUBLE PRECISION tlPo
      DOUBLE PRECISION tlPt
      DOUBLE PRECISION tlRo
      DOUBLE PRECISION tlRt
      DOUBLE PRECISION tlRv
      DOUBLE PRECISION umPo
      DOUBLE PRECISION umPt
      DOUBLE PRECISION umRo
      DOUBLE PRECISION umRt
      DOUBLE PRECISION umRv
      DOUBLE PRECISION upPop
      DOUBLE PRECISION upPopC
      DOUBLE PRECISION upPopp
      DOUBLE PRECISION upPoppC
      DOUBLE PRECISION upPonp
      DOUBLE PRECISION upPonpCn
      DOUBLE PRECISION upPonpp
      DOUBLE PRECISION upPonppCn
      DOUBLE PRECISION upPtp
      DOUBLE PRECISION upPtpC
      DOUBLE PRECISION upPtnp
      DOUBLE PRECISION upPtnpCn
      DOUBLE PRECISION upPo
      DOUBLE PRECISION upPoC
      DOUBLE PRECISION upPt
      DOUBLE PRECISION upPtC
      DOUBLE PRECISION upRo
      DOUBLE PRECISION upRon
      DOUBLE PRECISION upRt
      DOUBLE PRECISION upRtn
      DOUBLE PRECISION upRv
      DOUBLE PRECISION upRvRv
      DOUBLE PRECISION upRvn
      DOUBLE PRECISION upRvnRvn
      DOUBLE PRECISION acPo
      DOUBLE PRECISION acPop
      DOUBLE PRECISION acPopp
      DOUBLE PRECISION acPopRo
      DOUBLE PRECISION acPopRt
      DOUBLE PRECISION acPoppRo
      DOUBLE PRECISION acPoppRt
      DOUBLE PRECISION acPonp
      DOUBLE PRECISION acPonpp
      DOUBLE PRECISION acPonpRon
      DOUBLE PRECISION acPonpRtn
      DOUBLE PRECISION acPonppRon
      DOUBLE PRECISION acPonppRtn
      DOUBLE PRECISION acPt
      DOUBLE PRECISION acPtp
      DOUBLE PRECISION acPtpRo
      DOUBLE PRECISION acPtpRt
      DOUBLE PRECISION acPtnp
      DOUBLE PRECISION acPtnpRon
      DOUBLE PRECISION acPtnpRtn
      DOUBLE PRECISION dcPoC
      DOUBLE PRECISION dcPopC
      DOUBLE PRECISION dcPoppC
      DOUBLE PRECISION dcPopCRo
      DOUBLE PRECISION dcPopCRt
      DOUBLE PRECISION dcPoppCRo
      DOUBLE PRECISION dcPoppCRt
      DOUBLE PRECISION dcPonpCn
      DOUBLE PRECISION dcPonppCn
      DOUBLE PRECISION dcPonpCRo
      DOUBLE PRECISION dcPonpCRt
      DOUBLE PRECISION dcPonppCRo
      DOUBLE PRECISION dcPonppCRt
      DOUBLE PRECISION dcPtC
      DOUBLE PRECISION dcPtpC
      DOUBLE PRECISION dcPtpCRo
      DOUBLE PRECISION dcPtpCRt
      DOUBLE PRECISION dcPtnpCn
      DOUBLE PRECISION dcPtnpCRo
      DOUBLE PRECISION dcPtnpCRt
      DOUBLE PRECISION arPopRo
      DOUBLE PRECISION arPopRt
      DOUBLE PRECISION arPoppRo
      DOUBLE PRECISION arPoppRt
      DOUBLE PRECISION arPopCRo
      DOUBLE PRECISION arPopCRt
      DOUBLE PRECISION arPoppCRo
      DOUBLE PRECISION arPoppCRt
      DOUBLE PRECISION arPonpRon
      DOUBLE PRECISION arPonpRtn
      DOUBLE PRECISION arPonppRon
      DOUBLE PRECISION arPonppRtn
      DOUBLE PRECISION arPonpCRo
      DOUBLE PRECISION arPonpCRt
      DOUBLE PRECISION arPonppCRo
      DOUBLE PRECISION arPonppCRt
      DOUBLE PRECISION arPtpRo
      DOUBLE PRECISION arPtpRt
      DOUBLE PRECISION arPtpCRo
      DOUBLE PRECISION arPtpCRt
      DOUBLE PRECISION arPtnpRon
      DOUBLE PRECISION arPtnpRtn
      DOUBLE PRECISION arPtnpCRo
      DOUBLE PRECISION arPtnpCRt
      DOUBLE PRECISION arRvRv
      DOUBLE PRECISION arRvnRvn
      DOUBLE PRECISION drPopRo
      DOUBLE PRECISION drPopRt
      DOUBLE PRECISION drPoppRo
      DOUBLE PRECISION drPoppRt
      DOUBLE PRECISION drPopCRo
      DOUBLE PRECISION drPopCRt
      DOUBLE PRECISION drPoppCRo
      DOUBLE PRECISION drPoppCRt
      DOUBLE PRECISION drPonpRon
      DOUBLE PRECISION drPonpRtn
      DOUBLE PRECISION drPonppRon
      DOUBLE PRECISION drPonppRtn
      DOUBLE PRECISION drPonpCRo
      DOUBLE PRECISION drPonpCRt
      DOUBLE PRECISION drPonppCRo
      DOUBLE PRECISION drPonppCRt
      DOUBLE PRECISION drPtpRo
      DOUBLE PRECISION drPtpRt
      DOUBLE PRECISION drPtpCRo
      DOUBLE PRECISION drPtpCRt
      DOUBLE PRECISION drPtnpRon
      DOUBLE PRECISION drPtnpRtn
      DOUBLE PRECISION drPtnpCRo
      DOUBLE PRECISION drPtnpCRt
      DOUBLE PRECISION drRvRv
      DOUBLE PRECISION drRvnRvn
      DOUBLE PRECISION nlPop
      DOUBLE PRECISION nlPopRo
      DOUBLE PRECISION nlPopRt
      DOUBLE PRECISION nlPopC
      DOUBLE PRECISION nlPopCRo
      DOUBLE PRECISION nlPopCRt
      DOUBLE PRECISION nlPtp
      DOUBLE PRECISION nlPtpRo
      DOUBLE PRECISION nlPtpRt
      DOUBLE PRECISION nlPtpC
      DOUBLE PRECISION nlPtpCRo
      DOUBLE PRECISION nlPtpCRt
      DOUBLE PRECISION nlRv
      DOUBLE PRECISION nlRvRv
      DOUBLE PRECISION nePonp
      DOUBLE PRECISION nePonpp
      DOUBLE PRECISION nePonpRon
      DOUBLE PRECISION nePonpRtn
      DOUBLE PRECISION nePonppRon
      DOUBLE PRECISION nePonppRtn
      DOUBLE PRECISION nePonpCn
      DOUBLE PRECISION nePonppCn
      DOUBLE PRECISION nePonpCRo
      DOUBLE PRECISION nePonpCRt
      DOUBLE PRECISION nePonppCRo
      DOUBLE PRECISION nePonppCRt
      DOUBLE PRECISION nePtnp
      DOUBLE PRECISION nePtnpRon
      DOUBLE PRECISION nePtnpRtn
      DOUBLE PRECISION nePtnpCn
      DOUBLE PRECISION nePtnpCRo
      DOUBLE PRECISION nePtnpCRt
      DOUBLE PRECISION neRvn
      DOUBLE PRECISION neRvnRvn
      DOUBLE PRECISION hoPoC
      DOUBLE PRECISION hoPtC
      DOUBLE PRECISION htPopC
      DOUBLE PRECISION htPopCRo
      DOUBLE PRECISION htPopCRt
      DOUBLE PRECISION htPonpCn
      DOUBLE PRECISION htPonpCRo
      DOUBLE PRECISION htPonpCRt
      DOUBLE PRECISION Ct
      DOUBLE PRECISION bin
      DOUBLE PRECISION unbin
      DOUBLE PRECISION binRv
      DOUBLE PRECISION unbinRv
      DOUBLE PRECISION trPo
      DOUBLE PRECISION trPt
      DOUBLE PRECISION trRo
      DOUBLE PRECISION trRt
      DOUBLE PRECISION trRv
      DOUBLE PRECISION tmcMnPo
      DOUBLE PRECISION tmcMnPt
      DOUBLE PRECISION tmcMnRo
      DOUBLE PRECISION tmcMnRt
      DOUBLE PRECISION tmcMnRv
      DOUBLE PRECISION Nf
      DOUBLE PRECISION L
      DOUBLE PRECISION C
      DOUBLE PRECISION Rn
      DOUBLE PRECISION G
      DOUBLE PRECISION GRv
      DOUBLE PRECISION MnRo
      DOUBLE PRECISION McRo
      DOUBLE PRECISION MnRt
      DOUBLE PRECISION McRt
      DOUBLE PRECISION MnPo
      DOUBLE PRECISION McPo
      DOUBLE PRECISION MnPt
      DOUBLE PRECISION McPt
      DOUBLE PRECISION MnRv
      DOUBLE PRECISION McRv
      DOUBLE PRECISION Rv
      DOUBLE PRECISION Rvn
      DOUBLE PRECISION RvRv
      DOUBLE PRECISION RvnRvn
      DOUBLE PRECISION Po
      DOUBLE PRECISION Pt
      DOUBLE PRECISION PoC
      DOUBLE PRECISION PtC
      DOUBLE PRECISION PopC
      DOUBLE PRECISION PtpC
      DOUBLE PRECISION Pop
      DOUBLE PRECISION Ptp
      DOUBLE PRECISION PoppC
      DOUBLE PRECISION PopRo
      DOUBLE PRECISION PtpRo
      DOUBLE PRECISION PopRt
      DOUBLE PRECISION PtpRt
      DOUBLE PRECISION PoppRo
      DOUBLE PRECISION PoppRt
      DOUBLE PRECISION Popp
      DOUBLE PRECISION PopCRo
      DOUBLE PRECISION PtpCRo
      DOUBLE PRECISION PopCRt
      DOUBLE PRECISION PtpCRt
      DOUBLE PRECISION PoppCRo
      DOUBLE PRECISION PoppCRt
      DOUBLE PRECISION Ro
      DOUBLE PRECISION Rt
      DOUBLE PRECISION PonpCn
      DOUBLE PRECISION PtnpCn
      DOUBLE PRECISION Ponp
      DOUBLE PRECISION Ptnp
      DOUBLE PRECISION PonppCn
      DOUBLE PRECISION PonpRon
      DOUBLE PRECISION PtnpRon
      DOUBLE PRECISION PonpRtn
      DOUBLE PRECISION PtnpRtn
      DOUBLE PRECISION PonppRon
      DOUBLE PRECISION PonppRtn
      DOUBLE PRECISION Ponpp
      DOUBLE PRECISION PonpCnRon
      DOUBLE PRECISION PtnpCnRon
      DOUBLE PRECISION PonpCnRtn
      DOUBLE PRECISION PtnpCnRtn
      DOUBLE PRECISION PonppCnRon
      DOUBLE PRECISION PonppCnRtn
      DOUBLE PRECISION Ron
      DOUBLE PRECISION Rtn
      DOUBLE PRECISION Cn
      DIMENSION Y(NEQ),YP(NEQ),RESULT(*),RP(*)
*
* Equation set-up. 
*


      tlPo = RP(1)
      tlPt = RP(2)
      tlRo = RP(3)
      tlRt = RP(4)
      tlRv = RP(5)
      umPo = RP(6)
      umPt = RP(7)
      umRo = RP(8)
      umRt = RP(9)
      umRv = RP(10)
      upPop = RP(11)
      upPopC = RP(12)
      upPopp = RP(13)
      upPoppC = RP(14)
      upPonp = RP(15)
      upPonpCn = RP(16)
      upPonpp = RP(17)
      upPonppCn = RP(18)
      upPtp = RP(19)
      upPtpC = RP(20)
      upPtnp = RP(21)
      upPtnpCn = RP(22)
      upPo = RP(23)
      upPoC = RP(24)
      upPt = RP(25)
      upPtC = RP(26)
      upRo = RP(27)
      upRon = RP(28)
      upRt = RP(29)
      upRtn = RP(30)
      upRv = RP(31)
      upRvRv = RP(32)
      upRvn = RP(33)
      upRvnRvn = RP(34)
      acPo = RP(35)
      acPop = RP(36)
      acPopp = RP(37)
      acPopRo = RP(38)
      acPopRt = RP(39)
      acPoppRo = RP(40)
      acPoppRt = RP(41)
      acPonp = RP(42)
      acPonpp = RP(43)
      acPonpRon = RP(44)
      acPonpRtn = RP(45)
      acPonppRon = RP(46)
      acPonppRtn = RP(47)
      acPt = RP(48)
      acPtp = RP(49)
      acPtpRo = RP(50)
      acPtpRt = RP(51)
      acPtnp = RP(52)
      acPtnpRon = RP(53)
      acPtnpRtn = RP(54)
      dcPoC = RP(55)
      dcPopC = RP(56)
      dcPoppC = RP(57)
      dcPopCRo = RP(58)
      dcPopCRt = RP(59)
      dcPoppCRo = RP(60)
      dcPoppCRt = RP(61)
      dcPonpCn = RP(62)
      dcPonppCn = RP(63)
      dcPonpCRo = RP(64)
      dcPonpCRt = RP(65)
      dcPonppCRo = RP(66)
      dcPonppCRt = RP(67)
      dcPtC = RP(68)
      dcPtpC = RP(69)
      dcPtpCRo = RP(70)
      dcPtpCRt = RP(71)
      dcPtnpCn = RP(72)
      dcPtnpCRo = RP(73)
      dcPtnpCRt = RP(74)
      arPopRo = RP(75)
      arPopRt = RP(76)
      arPoppRo = RP(77)
      arPoppRt = RP(78)
      arPopCRo = RP(79)
      arPopCRt = RP(80)
      arPoppCRo = RP(81)
      arPoppCRt = RP(82)
      arPonpRon = RP(83)
      arPonpRtn = RP(84)
      arPonppRon = RP(85)
      arPonppRtn = RP(86)
      arPonpCRo = RP(87)
      arPonpCRt = RP(88)
      arPonppCRo = RP(89)
      arPonppCRt = RP(90)
      arPtpRo = RP(91)
      arPtpRt = RP(92)
      arPtpCRo = RP(93)
      arPtpCRt = RP(94)
      arPtnpRon = RP(95)
      arPtnpRtn = RP(96)
      arPtnpCRo = RP(97)
      arPtnpCRt = RP(98)
      arRvRv = RP(99)
      arRvnRvn = RP(100)
      drPopRo = RP(101)
      drPopRt = RP(102)
      drPoppRo = RP(103)
      drPoppRt = RP(104)
      drPopCRo = RP(105)
      drPopCRt = RP(106)
      drPoppCRo = RP(107)
      drPoppCRt = RP(108)
      drPonpRon = RP(109)
      drPonpRtn = RP(110)
      drPonppRon = RP(111)
      drPonppRtn = RP(112)
      drPonpCRo = RP(113)
      drPonpCRt = RP(114)
      drPonppCRo = RP(115)
      drPonppCRt = RP(116)
      drPtpRo = RP(117)
      drPtpRt = RP(118)
      drPtpCRo = RP(119)
      drPtpCRt = RP(120)
      drPtnpRon = RP(121)
      drPtnpRtn = RP(122)
      drPtnpCRo = RP(123)
      drPtnpCRt = RP(124)
      drRvRv = RP(125)
      drRvnRvn = RP(126)
      nlPop = RP(127)
      nlPopRo = RP(128)
      nlPopRt = RP(129)
      nlPopC = RP(130)
      nlPopCRo = RP(131)
      nlPopCRt = RP(132)
      nlPtp = RP(133)
      nlPtpRo = RP(134)
      nlPtpRt = RP(135)
      nlPtpC = RP(136)
      nlPtpCRo = RP(137)
      nlPtpCRt = RP(138)
      nlRv = RP(139)
      nlRvRv = RP(140)
      nePonp = RP(141)
      nePonpp = RP(142)
      nePonpRon = RP(143)
      nePonpRtn = RP(144)
      nePonppRon = RP(145)
      nePonppRtn = RP(146)
      nePonpCn = RP(147)
      nePonppCn = RP(148)
      nePonpCRo = RP(149)
      nePonpCRt = RP(150)
      nePonppCRo = RP(151)
      nePonppCRt = RP(152)
      nePtnp = RP(153)
      nePtnpRon = RP(154)
      nePtnpRtn = RP(155)
      nePtnpCn = RP(156)
      nePtnpCRo = RP(157)
      nePtnpCRt = RP(158)
      neRvn = RP(159)
      neRvnRvn = RP(160)
      hoPoC = RP(161)
      hoPtC = RP(162)
      htPopC = RP(163)
      htPopCRo = RP(164)
      htPopCRt = RP(165)
      htPonpCn = RP(166)
      htPonpCRo = RP(167)
      htPonpCRt = RP(168)
      Ct = RP(169)
      bin = RP(170)
      unbin = RP(171)
      binRv = RP(172)
      unbinRv = RP(173)
      trPo = RP(174)
      trPt = RP(175)
      trRo = RP(176)
      trRt = RP(177)
      trRv = RP(178)
      tmcMnPo = RP(179)
      tmcMnPt = RP(180)
      tmcMnRo = RP(181)
      tmcMnRt = RP(182)
      tmcMnRv = RP(183)
      Nf = RP(184)
      L = RP(185)

      G = Y(1)
      GRv = Y(2)
      MnRo = Y(3)
      McRo = Y(4)
      MnRt = Y(5)
      McRt = Y(6)
      MnPo = Y(7)
      McPo = Y(8)
      MnPt = Y(9)
      McPt = Y(10)
      MnRv = Y(11)
      McRv = Y(12)
      Rv = Y(13)
      Rvn = Y(14)
      RvRv = Y(15)
      RvnRvn = Y(16)
      Po = Y(17)
      Pt = Y(18)
      PoC = Y(19)
      PtC = Y(20)
      PopC = Y(21)
      PtpC = Y(22)
      Pop = Y(23)
      Ptp = Y(24)
      PoppC = Y(25)
      PopRo = Y(26)
      PtpRo = Y(27)
      PopRt = Y(28)
      PtpRt = Y(29)
      PoppRo = Y(30)
      PoppRt = Y(31)
      Popp = Y(32)
      PopCRo = Y(33)
      PtpCRo = Y(34)
      PopCRt = Y(35)
      PtpCRt = Y(36)
      PoppCRo = Y(37)
      PoppCRt = Y(38)
      Ro = Y(39)
      Rt = Y(40)
      PonpCn = Y(41)
      PtnpCn = Y(42)
      Ponp = Y(43)
      Ptnp = Y(44)
      PonppCn = Y(45)
      PonpRon = Y(46)
      PtnpRon = Y(47)
      PonpRtn = Y(48)
      PtnpRtn = Y(49)
      PonppRon = Y(50)
      PonppRtn = Y(51)
      Ponpp = Y(52)
      PonpCnRon = Y(53)
      PtnpCnRon = Y(54)
      PonpCnRtn = Y(55)
      PtnpCnRtn = Y(56)
      PonppCnRon = Y(57)
      PonppCnRtn = Y(58)
      Ron = Y(59)
      Rtn = Y(60)
      Cn = Y(61)

      C=Ct-(PoC+PtC+PopC+PtpC+PoppC+PopCRo+PopCRt+PtpCRo+PtpCRt+PoppCRo
     +   +PoppCRt+PonpCn+PtnpCn+PonppCn+PonpCnRon+PonpCnRtn+PtnpCnRon+
     +   PtnpCnRtn+PonppCnRon+PonppCnRtn+Cn)
      Rn=Ron+PonpRon+PonppRon+PonpCnRon+PonppCnRon+PtnpRon+PtnpCnRon+
     +   Rtn+PonpRtn+PonppRtn+PonpCnRtn+PonppCnRtn+PtnpRtn+PtnpCnRtn

      RESULT(1) =bin*Rn*(1.0000000000d+000-G)-unbin*G
      RESULT(2) =binRv*RvnRvn*(1.0000000000d+000-GRv)-unbinRv*GRv
      RESULT(3) =trRo*(1.0000000000d+000-G)*(1.0000000000d+000-GRv)**
     +   3.0000000000d+000-tmcMnRo*MnRo
      RESULT(4) =tmcMnRo*MnRo-umRo*McRo
      RESULT(5) =trRt*(1.0000000000d+000-G)-tmcMnRt*MnRt
      RESULT(6) =tmcMnRt*MnRt-umRt*McRt
      RESULT(7) =trPo*(1.0000000000d+000-G)**5.0000000000d+000-tmcMnPo*
     +   MnPo+L
      RESULT(8) =tmcMnPo*MnPo-umPo*McPo
      RESULT(9) =trPt*(1.0000000000d+000-G)**5.0000000000d+000-tmcMnPt*
     +   MnPt+L
      RESULT(10) =tmcMnPt*MnPt-umPt*McPt
      RESULT(11) =trRv*(1.0000000000d+000-G)**3.0000000000d+000-tmcMnRv
     +   *MnRv
      RESULT(12) =tmcMnRv*MnRv-umRv*McRv
      RESULT(13) =tlRv*McRv-2.0000000000d+000*arRvRv*Rv*Rv+
     +   2.0000000000d+000*drRvRv*RvRv-nlRv*Rv+neRvn*Rvn-upRv*Rv
      RESULT(14) =-2.0000000000d+000*Nf*arRvnRvn*Rvn*Rvn+
     +   2.0000000000d+000*drRvnRvn*RvnRvn+nlRv*Rv-neRvn*Rvn-upRvn*Rvn
      RESULT(15) =arRvRv*Rv*Rv-drRvRv*RvRv-nlRvRv*RvRv+neRvnRvn*RvnRvn-
     +   2.0000000000d+000*upRvRv*RvRv
      RESULT(16) =Nf*arRvnRvn*Rvn*Rvn-drRvnRvn*RvnRvn+nlRvRv*RvRv-
     +   neRvnRvn*RvnRvn-2.0000000000d+000*upRvnRvn*RvnRvn
      RESULT(17) =tlPo*McPo-acPo*Po*C+dcPoC*PoC-upPo*Po
      RESULT(18) =tlPt*McPt-acPt*Pt*C+dcPtC*PtC-upPt*Pt
      RESULT(19) =acPo*Po*C-dcPoC*PoC-hoPoC*PoC-upPoC*PoC
      RESULT(20) =acPt*Pt*C-dcPtC*PtC-hoPtC*PtC-upPtC*PtC
      RESULT(21) =hoPoC*PoC+acPop*Pop*C-dcPopC*PopC-upPopC*PopC-htPopC*
     +   PopC-nlPopC*PopC+nePonpCn*PonpCn-arPopCRo*PopC*Ro+drPopCRo*
     +   PopCRo-arPopCRt*PopC*Rt+drPopCRt*PopCRt
      RESULT(22) =hoPtC*PtC+acPtp*Ptp*C-dcPtpC*PtpC-upPtpC*PtpC-nlPtpC*
     +   PtpC+nePtnpCn*PtnpCn-arPtpCRo*PtpC*Ro+drPtpCRo*PtpCRo-
     +   arPtpCRt*PtpC*Rt+drPtpCRt*PtpCRt
      RESULT(23) =-acPop*Pop*C+dcPopC*PopC-upPop*Pop-arPopRo*Pop*Ro+
     +   drPopRo*PopRo-arPopRt*Pop*Rt+drPopRt*PopRt-nlPop*Pop+nePonp*
     +   Ponp
      RESULT(24) =-acPtp*Ptp*C+dcPtpC*PtpC-upPtp*Ptp-arPtpRo*Ptp*Ro+
     +   drPtpRo*PtpRo-arPtpRt*Ptp*Rt+drPtpRt*PtpRt-nlPtp*Ptp+nePtnp*
     +   Ptnp
      RESULT(25) =htPopC*PopC-upPoppC*PoppC+acPopp*Popp*C-dcPoppC*PoppC
     +   +nePonppCn*PonppCn-arPoppCRo*PoppC*Ro+drPoppCRo*PoppCRo-
     +   arPoppCRt*PoppC*Rt+drPoppCRt*PoppCRt
      RESULT(26) =arPopRo*Pop*Ro-drPopRo*PopRo-acPopRo*PopRo*C+dcPopCRo
     +   *PopCRo-nlPopRo*PopRo+nePonpRon*PonpRon
      RESULT(27) =arPtpRo*Ptp*Ro-drPtpRo*PtpRo-acPtpRo*PtpRo*C+dcPtpCRo
     +   *PtpCRo-nlPtpRo*PtpRo+nePtnpRon*PtnpRon
      RESULT(28) =arPopRt*Pop*Rt-drPopRt*PopRt-acPopRt*PopRt*C+dcPopCRt
     +   *PopCRt-nlPopRt*PopRt+nePonpRtn*PonpRtn
      RESULT(29) =arPtpRt*Ptp*Rt-drPtpRt*PtpRt-acPtpRt*PtpRt*C+dcPtpCRt
     +   *PtpCRt-nlPtpRt*PtpRt+nePtnpRtn*PtnpRtn
      RESULT(30) =arPoppRo*Popp*Ro-drPoppRo*PoppRo-acPoppRo*PoppRo*C+
     +   dcPoppCRo*PoppCRo+nePonppRon*PonppRon
      RESULT(31) =arPoppRt*Popp*Rt-drPoppRt*PoppRt-acPoppRt*PoppRt*C+
     +   dcPoppCRt*PoppCRt+nePonppRtn*PonppRtn
      RESULT(32) =-acPopp*Popp*C+dcPoppC*PoppC+nePonpp*Ponpp-arPoppRo*
     +   Popp*Ro+drPoppRo*PoppRo-arPoppRt*Popp*Rt+drPoppRt*PoppRt-
     +   upPopp*Popp
      RESULT(33) =arPopCRo*PopC*Ro-drPopCRo*PopCRo+acPopRo*PopRo*C-
     +   dcPopCRo*PopCRo-nlPopCRo*PopCRo+nePonpCRo*PonpCnRon-htPopCRo*
     +   PopCRo
      RESULT(34) =arPtpCRo*PtpC*Ro-drPtpCRo*PtpCRo+acPtpRo*PtpRo*C-
     +   dcPtpCRo*PtpCRo-nlPtpCRo*PtpCRo+nePtnpCRo*PtnpCnRon
      RESULT(35) =arPopCRt*PopC*Rt-drPopCRt*PopCRt+acPopRt*PopRt*C-
     +   dcPopCRt*PopCRt-nlPopCRt*PopCRt+nePonpCRt*PonpCnRtn-htPopCRt*
     +   PopCRt
      RESULT(36) =arPtpCRt*PtpC*Rt-drPtpCRt*PtpCRt+acPtpRt*PtpRt*C-
     +   dcPtpCRt*PtpCRt-nlPtpCRt*PtpCRt+nePtnpCRt*PtnpCnRtn
      RESULT(37) =arPoppCRo*PoppC*Ro-drPoppCRo*PoppCRo+acPoppRo*PoppRo*
     +   C-dcPoppCRo*PoppCRo+nePonppCRo*PonppCnRon+htPopCRo*PopCRo
      RESULT(38) =arPoppCRt*PoppC*Rt-drPoppCRt*PoppCRt+acPoppRt*PoppRt*
     +   C-dcPoppCRt*PoppCRt+nePonppCRt*PonppCnRtn+htPopCRt*PopCRt
      RESULT(39) =-arPopRo*Ro*Pop-arPoppRo*Ro*Popp-arPopCRo*Ro*PopC-
     +   arPoppCRo*Ro*PoppC+drPopRo*PopRo+drPoppRo*PoppRo+drPopCRo*
     +   PopCRo+drPoppCRo*PoppCRo-arPtpRo*Ro*Ptp-arPtpCRo*Ro*PtpC+
     +   drPtpRo*PtpRo+drPtpCRo*PtpCRo+tlRo*McRo-upRo*Ro
      RESULT(40) =-arPopRt*Rt*Pop-arPoppRt*Rt*Popp-arPopCRt*Rt*PopC-
     +   arPoppCRt*Rt*PoppC+drPopRt*PopRt+drPoppRt*PoppRt+drPopCRt*
     +   PopCRt+drPoppCRt*PoppCRt-arPtpRt*Rt*Ptp-arPtpCRt*Rt*PtpC+
     +   drPtpRt*PtpRt+drPtpCRt*PtpCRt+tlRt*McRt-upRt*Rt
      RESULT(41) =acPonp*Nf*Ponp*Cn-dcPonpCn*PonpCn-htPonpCn*PonpCn+
     +   nlPopC*PopC-nePonpCn*PonpCn-arPonpCRo*Nf*PonpCn*Ron+drPonpCRo
     +   *PonpCnRon-arPonpCRt*Nf*PonpCn*Rtn+drPonpCRt*PonpCnRtn-
     +   upPonpCn*PonpCn
      RESULT(42) =acPtnp*Nf*Ptnp*Cn-dcPtnpCn*PtnpCn+nlPtpC*PtpC-
     +   nePtnpCn*PtnpCn-arPtnpCRo*Nf*PtnpCn*Ron+drPtnpCRo*PtnpCnRon-
     +   arPtnpCRt*Nf*PtnpCn*Rtn+drPtnpCRt*PtnpCnRtn-upPtnpCn*PtnpCn
      RESULT(43) =-acPonp*Nf*Ponp*Cn+dcPonpCn*PonpCn-arPonpRon*Nf*Ponp*
     +   Ron+drPonpRon*PonpRon-arPonpRtn*Nf*Ponp*Rtn+drPonpRtn*PonpRtn
     +   +nlPop*Pop-nePonp*Ponp-upPonp*Ponp
      RESULT(44) =-acPtnp*Nf*Ptnp*Cn+dcPtnpCn*PtnpCn-arPtnpRon*Nf*Ptnp*
     +   Ron+drPtnpRon*PtnpRon-arPtnpRtn*Nf*Ptnp*Rtn+drPtnpRtn*PtnpRtn
     +   +nlPtp*Ptp-nePtnp*Ptnp-upPtnp*Ptnp
      RESULT(45) =htPonpCn*PonpCn+acPonpp*Nf*Ponpp*Cn-dcPonppCn*PonppCn
     +   -nePonppCn*PonppCn-arPonppCRo*Nf*PonppCn*Ron+drPonppCRo*
     +   PonppCnRon-arPonppCRt*Nf*PonppCn*Rtn+drPonppCRt*PonppCnRtn-
     +   upPonppCn*PonppCn
      RESULT(46) =arPonpRon*Nf*Ponp*Ron-drPonpRon*PonpRon-acPonpRon*Nf*
     +   PonpRon*Cn+dcPonpCRo*PonpCnRon+nlPopRo*PopRo-nePonpRon*
     +   PonpRon
      RESULT(47) =arPtnpRon*Nf*Ptnp*Ron-drPtnpRon*PtnpRon-acPtnpRon*Nf*
     +   PtnpRon*Cn+dcPtnpCRo*PtnpCnRon+nlPtpRo*PtpRo-nePtnpRon*
     +   PtnpRon
      RESULT(48) =arPonpRtn*Nf*Ponp*Rtn-drPonpRtn*PonpRtn-acPonpRtn*Nf*
     +   PonpRtn*Cn+dcPonpCRt*PonpCnRtn+nlPopRt*PopRt-nePonpRtn*
     +   PonpRtn
      RESULT(49) =arPtnpRtn*Nf*Ptnp*Rtn-drPtnpRtn*PtnpRtn-acPtnpRtn*Nf*
     +   PtnpRtn*Cn+dcPtnpCRt*PtnpCnRtn+nlPtpRt*PtpRt-nePtnpRtn*
     +   PtnpRtn
      RESULT(50) =arPonppRon*Nf*Ponpp*Ron-drPonppRon*PonppRon-
     +   acPonppRon*Nf*PonppRon*Cn+dcPonppCRo*PonppCnRon-nePonppRon*
     +   PonppRon
      RESULT(51) =arPonppRtn*Nf*Ponpp*Rtn-drPonppRtn*PonppRtn-
     +   acPonppRtn*Nf*PonppRtn*Cn+dcPonppCRt*PonppCnRtn-nePonppRtn*
     +   PonppRtn
      RESULT(52) =-acPonpp*Nf*Ponpp*Cn+dcPonppCn*PonppCn-nePonpp*Ponpp-
     +   arPonppRon*Nf*Ponpp*Ron+drPonppRon*PonppRon-arPonppRtn*Nf*
     +   Ponpp*Rtn+drPonppRtn*PonppRtn-upPonpp*Ponpp
      RESULT(53) =arPonpCRo*Nf*PonpCn*Ron-drPonpCRo*PonpCnRon+acPonpRon
     +   *Nf*PonpRon*Cn-dcPonpCRo*PonpCnRon+nlPopCRo*PopCRo-nePonpCRo*
     +   PonpCnRon-htPonpCRo*PonpCnRon
      RESULT(54) =arPtnpCRo*Nf*PtnpCn*Ron-drPtnpCRo*PtnpCnRon+acPtnpRon
     +   *Nf*PtnpRon*Cn-dcPtnpCRo*PtnpCnRon+nlPtpCRo*PtpCRo-nePtnpCRo*
     +   PtnpCnRon
      RESULT(55) =arPonpCRt*Nf*PonpCn*Rtn-drPonpCRt*PonpCnRtn+acPonpRtn
     +   *Nf*PonpRtn*Cn-dcPonpCRt*PonpCnRtn+nlPopCRt*PopCRt-nePonpCRt*
     +   PonpCnRtn-htPonpCRt*PonpCnRtn
      RESULT(56) =arPtnpCRt*Nf*PtnpCn*Rtn-drPtnpCRt*PtnpCnRtn+acPtnpRtn
     +   *Nf*PtnpRtn*Cn-dcPtnpCRt*PtnpCnRtn+nlPtpCRt*PtpCRt-nePtnpCRt*
     +   PtnpCnRtn
      RESULT(57) =arPonppCRo*Nf*PonppCn*Ron-drPonppCRo*PonppCnRon+
     +   acPonppRon*Nf*PonppRon*Cn-dcPonppCRo*PonppCnRon-nePonppCRo*
     +   PonppCnRon+htPonpCRo*PonpCnRon
      RESULT(58) =arPonppCRt*Nf*PonppCn*Rtn-drPonppCRt*PonppCnRtn+
     +   acPonppRtn*Nf*PonppRtn*Cn-dcPonppCRt*PonppCnRtn-nePonppCRt*
     +   PonppCnRtn+htPonpCRt*PonpCnRtn
      RESULT(59) =-arPonpRon*Nf*Ron*Ponp-arPonppRon*Nf*Ron*Ponpp-
     +   arPonpCRo*Nf*Ron*PonpCn-arPonppCRo*Nf*Ron*PonppCn+drPonpRon*
     +   PonpRon+drPonppRon*PonppRon+drPonpCRo*PonpCnRon+drPonppCRo*
     +   PonppCnRon-arPtnpRon*Nf*Ron*Ptnp-arPtnpCRo*Nf*Ron*PtnpCn+
     +   drPtnpRon*PtnpRon+drPtnpCRo*PtnpCnRon-upRon*Ron
      RESULT(60) =-arPonpRtn*Nf*Rtn*Ponp-arPonppRtn*Nf*Rtn*Ponpp-
     +   arPonpCRt*Nf*Rtn*PonpCn-arPonppCRt*Nf*Rtn*PonppCn+drPonpRtn*
     +   PonpRtn+drPonppRtn*PonppRtn+drPonpCRt*PonpCnRtn+drPonppCRt*
     +   PonppCnRtn-arPtnpRtn*Nf*Rtn*Ptnp-arPtnpCRt*Nf*Rtn*PtnpCn+
     +   drPtnpRtn*PtnpRtn+drPtnpCRt*PtnpCnRtn-upRtn*Rtn
      RESULT(61) =-acPonp*Nf*Cn*Ponp-acPonpp*Nf*Cn*Ponpp-acPonpRon*Nf*
     +   Cn*PonpRon-acPonppRon*Nf*Cn*PonppRon+dcPonpCn*PonpCn+
     +   dcPonppCn*PonppCn+dcPonpCRo*PonpCnRon+dcPonppCRo*PonppCnRon-
     +   acPtnp*Nf*Cn*Ptnp-acPtnpRon*Nf*Cn*PtnpRon+dcPtnpCn*PtnpCn+
     +   dcPtnpCRo*PtnpCnRon-acPonpRtn*Nf*Cn*PonpRtn-acPonppRtn*Nf*Cn*
     +   PonppRtn+dcPonpCRt*PonpCnRtn+dcPonppCRt*PonppCnRtn-acPtnpRtn*
     +   Nf*Cn*PtnpRtn+dcPtnpCRt*PtnpCnRtn+upPonpCn*PonpCn+upPonppCn*
     +   PonppCn+upPtnpCn*PtnpCn
      RETURN
      END

*
*--------------------------------------------------------------------------*
* Problem subroutine. 
*--------------------------------------------------------------------------*
      SUBROUTINE ADJ_RES(T,Y,YP,CJ,DELTA,IRES,RP,IP,SENPAR)
      IMPLICIT NONE
      DOUBLE PRECISION RESULT
      DOUBLE PRECISION T, Y, YP, CJ, DELTA, RP, SENPAR
      INTEGER IRES, IP
      INTEGER NEQ
      PARAMETER (NEQ=61)
      DIMENSION Y(NEQ),YP(NEQ),DELTA(NEQ),RP(*),IP(*),SENPAR(*)
      DIMENSION RESULT(61)

      CALL ADJ_FCN(T,Y,YP,RESULT,RP)
      DELTA(1) = YP(1) - RESULT(1)
      DELTA(2) = YP(2) - RESULT(2)
      DELTA(3) = YP(3) - RESULT(3)
      DELTA(4) = YP(4) - RESULT(4)
      DELTA(5) = YP(5) - RESULT(5)
      DELTA(6) = YP(6) - RESULT(6)
      DELTA(7) = YP(7) - RESULT(7)
      DELTA(8) = YP(8) - RESULT(8)
      DELTA(9) = YP(9) - RESULT(9)
      DELTA(10) = YP(10) - RESULT(10)
      DELTA(11) = YP(11) - RESULT(11)
      DELTA(12) = YP(12) - RESULT(12)
      DELTA(13) = YP(13) - RESULT(13)
      DELTA(14) = YP(14) - RESULT(14)
      DELTA(15) = YP(15) - RESULT(15)
      DELTA(16) = YP(16) - RESULT(16)
      DELTA(17) = YP(17) - RESULT(17)
      DELTA(18) = YP(18) - RESULT(18)
      DELTA(19) = YP(19) - RESULT(19)
      DELTA(20) = YP(20) - RESULT(20)
      DELTA(21) = YP(21) - RESULT(21)
      DELTA(22) = YP(22) - RESULT(22)
      DELTA(23) = YP(23) - RESULT(23)
      DELTA(24) = YP(24) - RESULT(24)
      DELTA(25) = YP(25) - RESULT(25)
      DELTA(26) = YP(26) - RESULT(26)
      DELTA(27) = YP(27) - RESULT(27)
      DELTA(28) = YP(28) - RESULT(28)
      DELTA(29) = YP(29) - RESULT(29)
      DELTA(30) = YP(30) - RESULT(30)
      DELTA(31) = YP(31) - RESULT(31)
      DELTA(32) = YP(32) - RESULT(32)
      DELTA(33) = YP(33) - RESULT(33)
      DELTA(34) = YP(34) - RESULT(34)
      DELTA(35) = YP(35) - RESULT(35)
      DELTA(36) = YP(36) - RESULT(36)
      DELTA(37) = YP(37) - RESULT(37)
      DELTA(38) = YP(38) - RESULT(38)
      DELTA(39) = YP(39) - RESULT(39)
      DELTA(40) = YP(40) - RESULT(40)
      DELTA(41) = YP(41) - RESULT(41)
      DELTA(42) = YP(42) - RESULT(42)
      DELTA(43) = YP(43) - RESULT(43)
      DELTA(44) = YP(44) - RESULT(44)
      DELTA(45) = YP(45) - RESULT(45)
      DELTA(46) = YP(46) - RESULT(46)
      DELTA(47) = YP(47) - RESULT(47)
      DELTA(48) = YP(48) - RESULT(48)
      DELTA(49) = YP(49) - RESULT(49)
      DELTA(50) = YP(50) - RESULT(50)
      DELTA(51) = YP(51) - RESULT(51)
      DELTA(52) = YP(52) - RESULT(52)
      DELTA(53) = YP(53) - RESULT(53)
      DELTA(54) = YP(54) - RESULT(54)
      DELTA(55) = YP(55) - RESULT(55)
      DELTA(56) = YP(56) - RESULT(56)
      DELTA(57) = YP(57) - RESULT(57)
      DELTA(58) = YP(58) - RESULT(58)
      DELTA(59) = YP(59) - RESULT(59)
      DELTA(60) = YP(60) - RESULT(60)
      DELTA(61) = YP(61) - RESULT(61)
      RETURN
      END