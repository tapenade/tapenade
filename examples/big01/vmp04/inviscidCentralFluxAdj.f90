!
!      ******************************************************************
!      *                                                                *
!      * File:          inviscidCentralFluxAdj.f90                      *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 03-20-2006                                      *
!      * Last modified: 03-20-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       subroutine inviscidCentralFluxAdj(wAdj,  pAdj,  dwAdj, &
                                         iCell, jCell, kCell)
!
!      ******************************************************************
!      *                                                                *
!      * inviscidCentralFluxAdj computes the Euler fluxes using a       *
!      * central discretization for the cell iCell, jCell, kCell of the *
!      * block to which the variables in blockPointers currently point  *
!      * to.                                                            *
!      *                                                                *
!      ******************************************************************
!
       use blockPointers
       use flowVarRefState
       use inputPhysics
       implicit none
!
!      Subroutine arguments
!
       integer(kind=intType) :: iCell, jCell, kCell

       real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                      intent(in) :: wAdj
       real(kind=realType), dimension(-2:2,-2:2,-2:2),    &
                                                      intent(in) :: pAdj
       real(kind=realType), dimension(nw), intent(inout) :: dwAdj
!
!      Local variables.
!
       integer(kind=intType) :: i, j, k, ii, jj, kk

       real(kind=realType) :: qsp, qsm, rqsp, rqsm, porVel, porFlux
       real(kind=realType) :: pa, fs, sFace, vnp, vnm, fact
!
!      ******************************************************************
!      *                                                                *
!      * Begin execution                                                *
!      *                                                                *
!      ******************************************************************
!
       ! Initialize sFace to zero. This value will be used if the
       ! block is not moving.

       sFace = 0.0
!
!      ******************************************************************
!      *                                                                *
!      * Advective fluxes in the i-direction.                           *
!      *                                                                *
!      ******************************************************************
!
       i    = iCell-1; j = jCell; k = kCell
       fact = -one

       ! Loop over the two faces which contribute to the residual of
       ! the cell considered.

       do ii=-1,0

         ! Set the dot product of the grid velocity and the
         ! normal in i-direction for a moving face.
          
         if( addGridVelocities ) sFace = sFaceI(i,j,k)

         ! Compute the normal velocities of the left and right state.

         vnp = wAdj(ii+1,0,0,ivx)*sI(i,j,k,1) &
             + wAdj(ii+1,0,0,ivy)*sI(i,j,k,2) &
             + wAdj(ii+1,0,0,ivz)*sI(i,j,k,3)
         vnm = wAdj(ii,  0,0,ivx)*sI(i,j,k,1) &
             + wAdj(ii,  0,0,ivy)*sI(i,j,k,2) &
             + wAdj(ii,  0,0,ivz)*sI(i,j,k,3)

         ! Set the values of the porosities for this face.
         ! porVel defines the porosity w.r.t. velocity;
         ! porFlux defines the porosity w.r.t. the entire flux.
         ! The latter is only zero for a discontinuous block
         ! boundary that must be treated conservatively.
         ! The default value of porFlux is 0.5, such that the
         ! correct central flux is scattered to both cells.
         ! In case of a boundFlux the normal velocity is set
         ! to sFace.

         porVel  = one
         porFlux = half
         if(porI(i,j,k) == noFlux)    porFlux = 0.0
         if(porI(i,j,k) == boundFlux) then
            porVel = 0.0
            vnp    = sFace
            vnm    = sFace
         endif
          
         ! Incorporate porFlux in porVel.

         porVel = porVel*porFlux

         ! Compute the normal velocities relative to the grid for
         ! the face as well as the mass fluxes.

         qsp = (vnp - sFace)*porVel
         qsm = (vnm - sFace)*porVel

         rqsp = qsp*wAdj(ii+1,0,0,irho)
         rqsm = qsm*wAdj(ii,  0,0,irho)
 
         ! Compute the sum of the pressure multiplied by porFlux.
         ! For the default value of porFlux, 0.5, this leads to
         ! the average pressure.
          
         pa = porFlux*(pAdj(ii+1,0,0) + pAdj(ii,0,0))
          
         ! Compute the fluxes through this face.
          
         fs = rqsp + rqsm
          
         dwAdj(irho) = dwAdj(irho) + fact*fs
          
         fs = rqsp*wAdj(ii+1,0,0,ivx) + rqsm*wAdj(ii,0,0,ivx) &
            + pa*sI(i,j,k,1)
         dwAdj(imx) = dwAdj(imx) + fact*fs
          
         fs = rqsp*wAdj(ii+1,0,0,ivy) + rqsm*wAdj(ii,0,0,ivy) &
            + pa*sI(i,j,k,2)
         dwAdj(imy) = dwAdj(imy) + fact*fs
          
         fs = rqsp*wAdj(ii+1,0,0,ivz) + rqsm*wAdj(ii,0,0,ivz) &
            + pa*sI(i,j,k,3)
         dwAdj(imz) = dwAdj(imz) + fact*fs
          
         fs = qsp*wAdj(ii+1,0,0,irhoE) + qsm*wAdj(ii,0,0,irhoE) &
            + porFlux*(vnp*pAdj(ii+1,0,0) + vnm*pAdj(ii,0,0))
         dwAdj(irhoE) = dwAdj(irhoE) + fact*fs

         ! Update i and set fact to 1 for the second face.

         i    = i + 1
         fact = one
       enddo
!
!      ******************************************************************
!      *                                                                *
!      * Advective fluxes in the j-direction.                           *
!      *                                                                *
!      ******************************************************************
!
       i    = iCell; j = jCell-1; k = kCell
       fact = -one

       ! Loop over the two faces which contribute to the residual of
       ! the cell considered.

       do jj=-1,0

         ! Set the dot product of the grid velocity and the
         ! normal in j-direction for a moving face.
          
         if( addGridVelocities ) sFace = sFaceJ(i,j,k)

         ! Compute the normal velocities of the left and right state.

         vnp = wAdj(0,jj+1,0,ivx)*sJ(i,j,k,1) &
             + wAdj(0,jj+1,0,ivy)*sJ(i,j,k,2) &
             + wAdj(0,jj+1,0,ivz)*sJ(i,j,k,3)
         vnm = wAdj(0,jj,  0,ivx)*sJ(i,j,k,1) &
             + wAdj(0,jj,  0,ivy)*sJ(i,j,k,2) &
             + wAdj(0,jj,  0,ivz)*sJ(i,j,k,3)

         ! Set the values of the porosities for this face.
         ! porVel defines the porosity w.r.t. velocity;
         ! porFlux defines the porosity w.r.t. the entire flux.
         ! The latter is only zero for a discontinuous block
         ! boundary that must be treated conservatively.
         ! The default value of porFlux is 0.5, such that the
         ! correct central flux is scattered to both cells.
         ! In case of a boundFlux the normal velocity is set
         ! to sFace.

         porVel  = one
         porFlux = half
         if(porJ(i,j,k) == noFlux)    porFlux = 0.0
         if(porJ(i,j,k) == boundFlux) then
            porVel = 0.0
            vnp    = sFace
            vnm    = sFace
         endif
          
         ! Incorporate porFlux in porVel.

         porVel = porVel*porFlux

         ! Compute the normal velocities relative to the grid for
         ! the face as well as the mass fluxes.

         qsp = (vnp - sFace)*porVel
         qsm = (vnm - sFace)*porVel

         rqsp = qsp*wAdj(0,jj+1,0,irho)
         rqsm = qsm*wAdj(0,jj,  0,irho)
 
         ! Compute the sum of the pressure multiplied by porFlux.
         ! For the default value of porFlux, 0.5, this leads to
         ! the average pressure.
          
         pa = porFlux*(pAdj(0,jj+1,0) + pAdj(0,jj,0))
          
         ! Compute the fluxes through this face.
          
         fs = rqsp + rqsm
          
         dwAdj(irho) = dwAdj(irho) + fact*fs
          
         fs = rqsp*wAdj(0,jj+1,0,ivx) + rqsm*wAdj(0,jj,0,ivx) &
            + pa*sJ(i,j,k,1)
         dwAdj(imx) = dwAdj(imx) + fact*fs
          
         fs = rqsp*wAdj(0,jj+1,0,ivy) + rqsm*wAdj(0,jj,0,ivy) &
            + pa*sJ(i,j,k,2)
         dwAdj(imy) = dwAdj(imy) + fact*fs
          
         fs = rqsp*wAdj(0,jj+1,0,ivz) + rqsm*wAdj(0,jj,0,ivz) &
            + pa*sJ(i,j,k,3)
         dwAdj(imz) = dwAdj(imz) + fact*fs
          
         fs = qsp*wAdj(0,jj+1,0,irhoE) + qsm*wAdj(0,jj,0,irhoE) &
            + porFlux*(vnp*pAdj(0,jj+1,0) + vnm*pAdj(0,jj,0))
         dwAdj(irhoE) = dwAdj(irhoE) + fact*fs

         ! Update j and set fact to 1 for the second face.

         j    = j + 1
         fact = one
       enddo
!
!      ******************************************************************
!      *                                                                *
!      * Advective fluxes in the k-direction.                           *
!      *                                                                *
!      ******************************************************************
!
       i    = iCell; j = jCell; k = kCell-1
       fact = -one

       ! Loop over the two faces which contribute to the residual of
       ! the cell considered.

       do kk=-1,0

         ! Set the dot product of the grid velocity and the
         ! normal in k-direction for a moving face.
          
         if( addGridVelocities ) sFace = sFaceK(i,j,k)

         ! Compute the normal velocities of the left and right state.

         vnp = wAdj(0,0,kk+1,ivx)*sK(i,j,k,1) &
             + wAdj(0,0,kk+1,ivy)*sK(i,j,k,2) &
             + wAdj(0,0,kk+1,ivz)*sK(i,j,k,3)
         vnm = wAdj(0,0,kk,  ivx)*sK(i,j,k,1) &
             + wAdj(0,0,kk,  ivy)*sK(i,j,k,2) &
             + wAdj(0,0,kk,  ivz)*sK(i,j,k,3)

         ! Set the values of the porosities for this face.
         ! porVel defines the porosity w.r.t. velocity;
         ! porFlux defines the porosity w.r.t. the entire flux.
         ! The latter is only zero for a discontinuous block
         ! boundary that must be treated conservatively.
         ! The default value of porFlux is 0.5, such that the
         ! correct central flux is scattered to both cells.
         ! In case of a boundFlux the normal velocity is set
         ! to sFace.

         porVel  = one
         porFlux = half
         if(porK(i,j,k) == noFlux)    porFlux = 0.0
         if(porK(i,j,k) == boundFlux) then
            porVel = 0.0
            vnp    = sFace
            vnm    = sFace
         endif
          
         ! Incorporate porFlux in porVel.

         porVel = porVel*porFlux

         ! Compute the normal velocities relative to the grid for
         ! the face as well as the mass fluxes.

         qsp = (vnp - sFace)*porVel
         qsm = (vnm - sFace)*porVel

         rqsp = qsp*wAdj(0,0,kk+1,irho)
         rqsm = qsm*wAdj(0,0,kk,  irho)
 
         ! Compute the sum of the pressure multiplied by porFlux.
         ! For the default value of porFlux, 0.5, this leads to
         ! the average pressure.
          
         pa = porFlux*(pAdj(0,0,kk+1) + pAdj(0,0,kk))
          
         ! Compute the fluxes through this face.
          
         fs = rqsp + rqsm
          
         dwAdj(irho) = dwAdj(irho) + fact*fs
          
         fs = rqsp*wAdj(0,0,kk+1,ivx) + rqsm*wAdj(0,0,kk,ivx) &
            + pa*sK(i,j,k,1)
         dwAdj(imx) = dwAdj(imx) + fact*fs
          
         fs = rqsp*wAdj(0,0,kk+1,ivy) + rqsm*wAdj(0,0,kk,ivy) &
            + pa*sK(i,j,k,2)
         dwAdj(imy) = dwAdj(imy) + fact*fs
          
         fs = rqsp*wAdj(0,0,kk+1,ivz) + rqsm*wAdj(0,0,kk,ivz) &
            + pa*sK(i,j,k,3)
         dwAdj(imz) = dwAdj(imz) + fact*fs
          
         fs = qsp*wAdj(0,0,kk+1,irhoE) + qsm*wAdj(0,0,kk,irhoE) &
            + porFlux*(vnp*pAdj(0,0,kk+1) + vnm*pAdj(0,0,kk))
         dwAdj(irhoE) = dwAdj(irhoE) + fact*fs

         ! Update k and set fact to 1 for the second face.

         k    = k + 1
         fact = one
       enddo

       end subroutine inviscidCentralFluxAdj
