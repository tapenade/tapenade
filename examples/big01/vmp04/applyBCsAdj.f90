!
!      ******************************************************************
!      *                                                                *
!      * File:          applyBCsAdj.f90                                 *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 03-20-2006                                      *
!      * Last modified: 03-21-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       subroutine applyBCsAdj(wAdj, pAdj, iCell, jCell, kCell)
!
!      ******************************************************************
!      *                                                                *
!      * applyBCsAdj applies the possible boundary conditions for the   *
!      * halo cells adjacent to the cell for which the residual needs   *
!      * to be computed.                                                *
!      *                                                                *
!      ******************************************************************
!
       use BCTypes
       use blockPointers
       use flowVarRefState
       implicit none
!
!      Subroutine arguments.
!
       integer(kind=intType) :: iCell, jCell, kCell

       real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                   intent(inout) :: wAdj
       real(kind=realType), dimension(-2:2,-2:2,-2:2),    &
                                                   intent(inout) :: pAdj
!
!      Local variables.
!
       integer(kind=intType) :: nn
       integer(kind=intType) :: iSBeg, jSBeg, kSBeg
       integer(kind=intType) :: iSEnd, jSEnd, kSEnd
       integer(kind=intType) :: iBBeg, jBBeg, kBBeg
       integer(kind=intType) :: iBEnd, jBEnd, kBEnd
       integer(kind=intType) :: iRBeg, jRBeg, kRBeg
       integer(kind=intType) :: iREnd, jREnd, kREnd
       integer(kind=intType) :: iOffset, jOffset, kOffset

       real(kind=realType), dimension(-2:2,-2:2,nw) :: wAdj0, wAdj1
       real(kind=realType), dimension(-2:2,-2:2,nw) :: wAdj2, wAdj3
       real(kind=realType), dimension(-2:2,-2:2)    :: pAdj0, pAdj1
       real(kind=realType), dimension(-2:2,-2:2)    :: pAdj2, pAdj3

       logical :: iOverlap, jOverlap, kOverlap, secondHalo
!
!      ******************************************************************
!      *                                                                *
!      * Begin execution                                                *
!      *                                                                *
!      ******************************************************************
!
       ! Determine the range of the stencil for the given cell.

       iSBeg = iCell - 2; iSEnd = iCell + 2
       jSBeg = jCell - 2; jSEnd = jCell + 2
       kSBeg = kCell - 2; kSEnd = kCell + 2

       ! Loop over the number of physical boundary subfaces of the block.

       bocoLoop: do nn=1,nBocos

         ! Determine the range of halo cells which this boundary subface
         ! will change.

         select case (BCFaceID(nn))
           case (iMin)
             iBBeg = 0;                iBEnd = 1
             jBBeg = BCData(nn)%icBeg; jBEnd = BCData(nn)%icEnd
             kBBeg = BCData(nn)%jcBeg; kBEnd = BCData(nn)%jcEnd

           !=============================================================

           case (iMax)
             iBBeg = ie;               iBEnd = ib
             jBBeg = BCData(nn)%icBeg; jBEnd = BCData(nn)%icEnd
             kBBeg = BCData(nn)%jcBeg; kBEnd = BCData(nn)%jcEnd

           !=============================================================

           case (jMin)
             iBBeg = BCData(nn)%icBeg; iBEnd = BCData(nn)%icEnd
             jBBeg = 0;                jBEnd = 1
             kBBeg = BCData(nn)%jcBeg; kBEnd = BCData(nn)%jcEnd

           !=============================================================

           case (jMax)
             iBBeg = BCData(nn)%icBeg; iBEnd = BCData(nn)%icEnd
             jBBeg = je;               jBEnd = jb
             kBBeg = BCData(nn)%jcBeg; kBEnd = BCData(nn)%jcEnd

           !=============================================================

           case (kMin)
             iBBeg = BCData(nn)%icBeg; iBEnd = BCData(nn)%icEnd
             jBBeg = BCData(nn)%jcBeg; jBEnd = BCData(nn)%jcEnd
             kBBeg = 0;                kBEnd = 1

           !=============================================================

           case (kMax)
             iBBeg = BCData(nn)%icBeg; iBEnd = BCData(nn)%icEnd
             jBBeg = BCData(nn)%jcBeg; jBEnd = BCData(nn)%jcEnd
             kBBeg = ke;               kBEnd = kb

         end select

         ! Check for an overlap between the stencil range and the
         ! halo cells influenced by this boundary subface.

         iOverlap = .false.
         if(iSBeg <= iBEnd .and. iSEnd >= iBBeg) iOverlap = .true.

         jOverlap = .false.
         if(jSBeg <= jBEnd .and. jSEnd >= jBBeg) jOverlap = .true.

         kOverlap = .false.
         if(kSBeg <= kBEnd .and. kSEnd >= kBBeg) kOverlap = .true.

         checkOverlap: if(iOverlap .and. jOverlap .and. kOverlap) then

           ! There is an overlap between the two ranges.
           ! Determine the actual overlap region.

           iRBeg = max(iSBeg, iBBeg); iREnd = min(iSEnd, iBEnd)
           jRBeg = max(jSBeg, jBBeg); jREnd = min(jSEnd, jBEnd)
           kRBeg = max(kSBeg, kBBeg); kREnd = min(kSEnd, kBEnd)

           ! Determine the offset values to be used in the computation
           ! of the halo cells. Either the cells with index -2 or with
           ! -1 corresponds to the starting cell ID of the subrange
           ! to be considered.

           iOffset = iRBeg - 1; if(iSBeg == iRBeg) iOffset = iOffset - 1
           jOffset = jRBeg - 1; if(jSBeg == jRBeg) jOffset = jOffset - 1
           kOffset = kRBeg - 1; if(kSBeg == kRBeg) kOffset = kOffset - 1

           ! Set some pointers depending on the situation.

           select case (BCFaceID(nn))
             case (iMin)

               icBeg = jRBeg; icEnd = jREnd
               jcBeg = kRBeg; jcEnd = kREnd

               iOffset = jOffset
               jOffset = kOffset

               secondHalo = .true.
               if(iRBeg == iREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj(-2,-2:2,-2:2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-1,-2:2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj( 0,-2:2,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj( 1,-2:2,-2:2,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj(-2,-2:2,-2:2)
                 pAdj1(-2:2,-2:2) = pAdj(-1,-2:2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj( 0,-2:2,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj( 1,-2:2,-2:2)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2,-2:2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-1,-2:2,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj( 0,-2:2,-2:2,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj(-2,-2:2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj(-1,-2:2,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj( 0,-2:2,-2:2)
               endif

             !===========================================================

             case (iMax)

               icBeg = jRBeg; icEnd = jREnd
               jcBeg = kRBeg; jcEnd = kREnd

               iOffset = jOffset
               jOffset = kOffset

               secondHalo = .true.
               if(iRBeg == iREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj( 2,-2:2,-2:2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj( 1,-2:2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj( 0,-2:2,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-1,-2:2,-2:2,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj( 2,-2:2,-2:2)
                 pAdj1(-2:2,-2:2) = pAdj( 1,-2:2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj( 0,-2:2,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj(-1,-2:2,-2:2)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj( 2,-2:2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj( 1,-2:2,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj( 0,-2:2,-2:2,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj( 2,-2:2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj( 1,-2:2,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj( 0,-2:2,-2:2)
               endif

             !===========================================================

             case (jMin)

               icBeg = iRBeg; icEnd = iREnd
               jcBeg = kRBeg; jcEnd = kREnd

               jOffset = kOffset

               secondHalo = .true.
               if(jRBeg == jREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj(-2:2,-2,-2:2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-1,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2, 0,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2, 1,-2:2,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj(-2:2,-2,-2:2)
                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-1,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2, 0,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2, 1,-2:2)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2,-1,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2, 0,-2:2,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2,-1,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2, 0,-2:2)
               endif

             !===========================================================

             case (jMax)

               icBeg = iRBeg; icEnd = iREnd
               jcBeg = kRBeg; jcEnd = kREnd

               jOffset = kOffset

               secondHalo = .true.
               if(jRBeg == jREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj(-2:2, 2,-2:2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2, 1,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2, 0,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2,-1,-2:2,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj(-2:2, 2,-2:2)
                 pAdj1(-2:2,-2:2) = pAdj(-2:2, 1,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2, 0,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2,-1,-2:2)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2, 2,-2:2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2, 1,-2:2,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2, 0,-2:2,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj(-2:2, 2,-2:2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2, 1,-2:2)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2, 0,-2:2)
               endif

             !===========================================================

             case (kMin)

               icBeg = iRBeg; icEnd = iREnd
               jcBeg = jRBeg; jcEnd = jREnd

               secondHalo = .true.
               if(kRBeg == kREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2,-2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2,-1,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 0,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 1,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj(-2:2,-2:2,-2)
                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-2:2,-1)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2,-2:2, 0)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2,-2:2, 1)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2,-2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2,-1,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 0,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-2:2,-2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2,-2:2,-1)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2,-2:2, 0)
               endif

             !===========================================================

             case (kMax)

               icBeg = iRBeg; icEnd = iREnd
               jcBeg = jRBeg; jcEnd = jREnd

               secondHalo = .true.
               if(kRBeg == kREnd) secondHalo = .false.

               if( secondHalo ) then
                 wAdj0(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 2,1:nw)
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 1,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 0,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2,-1,1:nw)

                 pAdj0(-2:2,-2:2) = pAdj(-2:2,-2:2, 2)
                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-2:2, 1)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2,-2:2, 0)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2,-2:2,-1)
               else
                 wAdj1(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 2,1:nw)
                 wAdj2(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 1,1:nw)
                 wAdj3(-2:2,-2:2,1:nw) = wAdj(-2:2,-2:2, 0,1:nw)

                 pAdj1(-2:2,-2:2) = pAdj(-2:2,-2:2, 2)
                 pAdj2(-2:2,-2:2) = pAdj(-2:2,-2:2, 1)
                 pAdj3(-2:2,-2:2) = pAdj(-2:2,-2:2, 0)
               endif

           end select

           ! Determine the boundary condition and call the
           ! appropriate routine.

           select case (BCType(nn))
             case (EulerWall)
               call bcEulerWallAdj(nn,icBeg, icEnd, jcBeg, jcEnd,   &
                                   iOffset, jOffset, secondHalo,    &
                                   wAdj0, wAdj1, wAdj2, wAdj3,      &
                                   pAdj0, pAdj1, pAdj2, pAdj3)

             !===========================================================

             case (FarField)
               call bcFarfieldAdj(nn,icBeg, icEnd, jcBeg, jcEnd, &
                                  iOffset, jOffset, secondHalo,  &
                                  wAdj0, wAdj1, wAdj2,           &
                                  pAdj0, pAdj1, pAdj2)



           end select

           ! Copy the information back to the original arrays wAdj and pAdj.

           select case (BCFaceID(nn))
             case (iMin)

               if( secondHalo ) then
                 wAdj(-2,-2:2,-2:2,1:nw) = wAdj0(-2:2,-2:2,1:nw) 
                 wAdj(-1,-2:2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj(-2,-2:2,-2:2) = pAdj0(-2:2,-2:2) 
                 pAdj(-1,-2:2,-2:2) = pAdj1(-2:2,-2:2) 
               else
                 wAdj(-2,-2:2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj(-2,-2:2,-2:2)      = pAdj1(-2:2,-2:2)
               endif

             !===========================================================

             case (iMax)

               if( secondHalo ) then
                 wAdj( 2,-2:2,-2:2,1:nw) = wAdj0(-2:2,-2:2,1:nw)
                 wAdj( 1,-2:2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj( 2,-2:2,-2:2) = pAdj0(-2:2,-2:2)
                 pAdj( 1,-2:2,-2:2) = pAdj1(-2:2,-2:2)
               else
                 wAdj( 2,-2:2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj( 2,-2:2,-2:2)      = pAdj1(-2:2,-2:2)
               endif

             !===========================================================

             case (jMin)

               if( secondHalo ) then
                 wAdj(-2:2,-2,-2:2,1:nw) = wAdj0(-2:2,-2:2,1:nw)
                 wAdj(-2:2,-1,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj(-2:2,-2,-2:2) = pAdj0(-2:2,-2:2)
                 pAdj(-2:2,-1,-2:2) = pAdj1(-2:2,-2:2)
               else
                 wAdj(-2:2,-2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj(-2:2,-2,-2:2)      = pAdj1(-2:2,-2:2)
               endif

             !===========================================================

             case (jMax)

               if( secondHalo ) then
                 wAdj(-2:2, 2,-2:2,1:nw) = wAdj0(-2:2,-2:2,1:nw)
                 wAdj(-2:2, 1,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj(-2:2, 2,-2:2) = pAdj0(-2:2,-2:2)
                 pAdj(-2:2, 1,-2:2) = pAdj1(-2:2,-2:2)
               else
                 wAdj(-2:2, 2,-2:2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj(-2:2, 2,-2:2)      = pAdj1(-2:2,-2:2)
               endif

             !===========================================================

             case (kMin)

               if( secondHalo ) then
                 wAdj(-2:2,-2:2,-2,1:nw) = wAdj0(-2:2,-2:2,1:nw)
                 wAdj(-2:2,-2:2,-1,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj(-2:2,-2:2,-2) = pAdj0(-2:2,-2:2)
                 pAdj(-2:2,-2:2,-1) = pAdj1(-2:2,-2:2)
               else
                 wAdj(-2:2,-2:2,-2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj(-2:2,-2:2,-2)      = pAdj1(-2:2,-2:2)
               endif

             !===========================================================

             case (kMax)

               if( secondHalo ) then
                 wAdj(-2:2,-2:2, 2,1:nw) = wAdj0(-2:2,-2:2,1:nw)
                 wAdj(-2:2,-2:2, 1,1:nw) = wAdj1(-2:2,-2:2,1:nw)

                 pAdj(-2:2,-2:2, 2) = pAdj0(-2:2,-2:2)
                 pAdj(-2:2,-2:2, 1) = pAdj1(-2:2,-2:2)
               else
                 wAdj(-2:2,-2:2, 2,1:nw) = wAdj1(-2:2,-2:2,1:nw)
                 pAdj(-2:2,-2:2, 2)      = pAdj1(-2:2,-2:2)
               endif

           end select

         endif checkOverlap

       enddo bocoLoop

       end subroutine applyBCsAdj
