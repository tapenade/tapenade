!
!      ******************************************************************
!      *                                                                *
!      * File:          residualAdj.f90                                 *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 03-19-2006                                      *
!      * Last modified: 03-20-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       subroutine residualAdj(wAdj, dwAdj, iCell, jCell, kCell)
!
!      ******************************************************************
!      *                                                                *
!      * Simple routine to compute the residual in a single cell of the *
!      *  multiblock mesh solver SUmb.  The state in a stencil that is  *
!      *  5 cells wide in each coordinate direction (centered about the *
!      *  central cell) is given by wAdj(-2:2,-2:2,-2:2,nw) and the     *
!      *  residual is returned in the array dwAdj(nw).                  *
!      *                                                                *
!      ******************************************************************
!
       use inputDiscretization
       use flowVarRefState
       implicit none
!
!      Subroutine arguments
!
       integer(kind=intType) :: iCell, jCell, kCell

       real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                   intent(inout) :: wAdj
       real(kind=realType), dimension(nw), intent(out) :: dwAdj
!
!      Local variables
!
       integer(kind=intType) :: l
       real(kind=realType), dimension(-2:2,-2:2,-2:2) :: pAdj
!
!      ******************************************************************
!      *                                                                *
!      * Begin execution                                                *
!      *                                                                *
!      ******************************************************************
!
       ! Initialize the residual to zero.

       do l=1,nw
         dwAdj(l) = zero
       enddo

       ! Given the state, compute the pressure according to the ideal
       ! gas law.

       call computePressureAdj(wAdj, pAdj)

       ! Apply the boundary conditions.

       call applyBCsAdj(wAdj, pAdj, iCell, jCell, kCell)

       ! Calculate the inviscid convective residual using a central
       ! discretization.

       call inviscidCentralFluxAdj(wAdj,  pAdj,  dwAdj, &
                                   iCell, jCell, kCell)

       ! Calculate the dissipative fluxes. The routine called depends
       ! on the discretization used.

       if(spaceDiscr == upwind) then
          call inviscidUpwindFluxAdj(wAdj,  pAdj,  dwAdj, &
                                     iCell, jCell, kCell)
 !      !else
 !      !  call terminate("residualAdj", &
 !      !                 "Dissipation model not implemented yet.")
       endif


       end subroutine residualAdj
