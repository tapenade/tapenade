!
!      ******************************************************************
!      *                                                                *
!      * File:          bcFarfieldAdj.f90                               *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 03-21-2006                                      *
!      * Last modified: 03-21-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       subroutine bcFarfieldAdj(nn,icBeg, icEnd, jcBeg, jcEnd, &
                                iOffset, jOffset, secondHalo,  &
                                wAdj0, wAdj1, wAdj2, &
                                pAdj0, pAdj1, pAdj2)
!
!      ******************************************************************
!      *                                                                *
!      * bcFarfieldAdj applies the farfield boundary condition to       *
!      * subface nn of the block to which the pointers in blockPointers *
!      * currently point.                                               *
!      *                                                                *
!      ******************************************************************
!
       use blockPointers, only : BCData
       use constants
       use flowVarRefState
       use iteration
       implicit none
!
!      Subroutine arguments.
!
       integer(kind=intType), intent(in) :: nn
       integer(kind=intType), intent(in) :: icBeg, icEnd, jcBeg, jcEnd
       integer(kind=intType), intent(in) :: iOffset, jOffset

       logical, intent(in) :: secondHalo

       real(kind=realType), dimension(-2:2,-2:2,nw) :: wAdj0, wAdj1
       real(kind=realType), dimension(-2:2,-2:2,nw) :: wAdj2
       real(kind=realType), dimension(-2:2,-2:2)    :: pAdj0, pAdj1
       real(kind=realType), dimension(-2:2,-2:2)    :: pAdj2
!
!      Local variables.
!
       integer(kind=intType) :: i, j, l, ii, jj

       real(kind=realType) :: nnx, nny, nnz
       real(kind=realType) :: gm1, ovgm1, gm53, factK, ac1, ac2
       real(kind=realType) :: r0, u0, v0, w0, qn0, vn0, c0, s0
       real(kind=realType) :: re, ue, ve, we, qne, ce
       real(kind=realType) :: qnf, cf, uf, vf, wf, sf, cc, qq
!
!      ******************************************************************
!      *                                                                *
!      * Begin execution                                                *
!      *                                                                *
!      ******************************************************************
!
       ! Some constants needed to compute the riemann invariants.

       gm1   = gammaInf - one
       ovgm1 = one/gm1
       gm53  =  gammaInf - five*third
       factK = -ovgm1*gm53

       ! Compute the three velocity components, the speed of sound and
       ! the entropy of the free stream.

       r0  = one/wInf(irho)
       u0  = wInf(ivx)
       v0  = wInf(ivy)
       w0  = wInf(ivz)
       c0  = sqrt(gammaInf*pInfCorr*r0)
       s0  = wInf(irho)**gammaInf/pInfCorr

       ! Loop over the generic subface to set the state in the
       ! halo cells.

       do j=jcBeg, jcEnd
         do i=icBeg, icEnd

           ii = i - iOffset
           jj = j - jOffset

           ! Store the three components of the unit normal a
           ! bit easier.

           nnx = BCData(nn)%norm(i,j,1)
           nny = BCData(nn)%norm(i,j,2)
           nnz = BCData(nn)%norm(i,j,3)

           ! Compute the normal velocity of the free stream and
           ! substract the normal velocity of the mesh.

           qn0 = u0*nnx + v0*nny + w0*nnz
           vn0 = qn0 - BCData(nn)%rface(i,j)

           ! Compute the three velocity components, the normal
           ! velocity and the speed of sound of the current state
           ! in the internal cell.

           re  = one/wAdj2(ii,jj,irho)
           ue  = wAdj2(ii,jj,ivx)
           ve  = wAdj2(ii,jj,ivy)
           we  = wAdj2(ii,jj,ivz)
           qne = ue*nnx + ve*nny + we*nnz
           ce  = sqrt(gammaInf*pAdj2(ii,jj)*re)

           ! Compute the new values of the riemann invariants in
           ! the halo cell. Either the value in the internal cell
           ! is taken (positive sign of the corresponding
           ! eigenvalue) or the free stream value is taken
           ! (otherwise).

           if(vn0 > -c0) then       ! Outflow or subsonic inflow.
             ac1 = qne + two*ovgm1*ce
           else                     ! Supersonic inflow.
             ac1 = qn0 + two*ovgm1*c0
           endif

           if(vn0 > c0) then        ! Supersonic outflow.
             ac2 = qne - two*ovgm1*ce
           else                     ! Inflow or subsonic outflow.
             ac2 = qn0 - two*ovgm1*c0
           endif

           qnf = half*  (ac1 + ac2)
           cf  = fourth*(ac1 - ac2)*gm1

           if(vn0 > zero) then            ! Outflow.

             uf = ue + (qnf - qne)*nnx
             vf = ve + (qnf - qne)*nny
             wf = we + (qnf - qne)*nnz
             sf = wAdj2(ii,jj,irho)**gammaInf/pAdj2(ii,jj)

             do l=nt1MG,nt2MG
               wAdj1(ii,jj,l) = wAdj2(ii,jj,l)
             enddo

           else                           ! Inflow

             uf = u0 + (qnf - qn0)*nnx
             vf = v0 + (qnf - qn0)*nny
             wf = w0 + (qnf - qn0)*nnz
             sf = s0

             do l=nt1MG,nt2MG
               wAdj1(ii,jj,l) = wInf(l)
             enddo

           endif

           ! Compute the density, velocity and pressure in the
           ! halo cell.

           cc = cf*cf/gammaInf
           qq = uf*uf + vf*vf + wf*wf
           wAdj1(ii,jj,irho) = (sf*cc)**ovgm1
           wAdj1(ii,jj,ivx)  = uf
           wAdj1(ii,jj,ivy)  = vf
           wAdj1(ii,jj,ivz)  = wf
           pAdj1(ii,jj)      = wAdj1(ii,jj,irho)*cc

           ! Compute the total energy.

           wAdj1(ii,jj,irhoE) = ovgm1*pAdj1(ii,jj)     &
                              + half*wAdj1(ii,jj,irho) &
                              *      (uf**2 + vf**2 + wf**2)

           if( kPresent )                            &
             wAdj1(ii,jj,irhoE) = wAdj1(ii,jj,irhoE) &
                                - factK*wAdj1(ii,jj,irho) &
                                *       wAdj1(ii,jj,itu1)
         enddo
       enddo

       ! Extrapolate the state vectors in case a second halo
       ! is needed.

       if( secondHalo )                                             &
         call extrapolate2ndHaloAdj(nn,icBeg, icEnd, jcBeg, jcEnd,  &
                                    iOffset, jOffset, wAdj0, wAdj1, &
                                    wAdj2, pAdj0, pAdj1, pAdj2)

       end subroutine bcFarfieldAdj
