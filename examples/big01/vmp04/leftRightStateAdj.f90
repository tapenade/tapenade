!      ==================================================================

         subroutine leftRightStateAdj(du1, du2, du3, left, right,nwInt,omk,opk,factminmod,firstOrderK)
!
!        ****************************************************************
!        *                                                              *
!        * leftRightState computes the differences in the left and      *
!        * right state compared to the first order interpolation. For a *
!        * monotonic second order discretization the interpolations     *
!        * need to be nonlinear. The linear second order scheme can be  *
!        * stable (depending on the value of kappa), but it will have   *
!        * oscillations near discontinuities.                           *
!        *                                                              *
!        ****************************************************************
!
         use precision
         use constants
         use inputDiscretization

         implicit none
!
!        Local parameter.
!
         real(kind=realType), parameter :: epsLim = 1.e-10_realType
!
!        Subroutine arguments.
!
         integer(kind=intType) :: nwInt
         real(kind=realType) :: omk, opk, factMinmod
         real(kind=realType), dimension(*), intent(in)  :: du1, du2, du3
         real(kind=realType), dimension(*) :: left, right
         logical :: firstOrderK
!
!        Local variables.
!
         integer(kind=intType) :: l

         real(kind=realType) :: rl1, rl2, rr1, rr2, tmp
!
!        ****************************************************************
!        *                                                              *
!        * Begin execution.                                             *
!        *                                                              *
!        ****************************************************************
!
             ! Linear interpolation; no limiter.
             ! Loop over the number of variables to be interpolated.

             do l=1,nwInt
               left(l)  =  omk*du1(l) + opk*du2(l)
               right(l) = -omk*du3(l) - opk*du2(l)
             enddo
         ! In case only a first order scheme must be used for the
         ! turbulent transport equations, set the correction for the
         ! turbulent kinetic energy to 0.

         if( firstOrderK ) then
           left(itu1)  = zero
           right(itu1) = zero
         endif

         end subroutine leftRightStateAdj
