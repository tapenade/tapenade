module diffsizes
     integer, parameter :: ISIZE1OFicend = 4
     integer, parameter :: ISIZE1OFDrficend = 4
     integer, parameter :: ISIZE1OFjcend = 4
     integer, parameter :: ISIZE1OFDrfjcend = 4
     integer, parameter :: ISIZE1OFicbeg = 4
     integer, parameter :: ISIZE1OFDrficbeg = 4
     integer, parameter :: ISIZE1OFjcbeg = 4
     integer, parameter :: ISIZE1OFDrfjcbeg = 4
     integer, parameter :: nbdirsmax = 4
end module diffsizes
