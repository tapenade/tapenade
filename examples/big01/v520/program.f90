! Time-stamp: <*** mod_control.f90: 2016/09/17 13:01 (Xijun LAI) ***>

module mod_control

  real*8, allocatable, dimension(:) :: cod0,cod_node0
  real*8, allocatable, dimension(:) ::   cod_k1,cod_r1, cod_k1_lak, cod_r1_lak

end module mod_control

subroutine cost_function ( t, phi_node, phi, cost )
  !
  ! ======================================================================
  !
  !
  ! ======================================================================
  !
  use cmndat
  use mod_spaceobs
  !
  implicit none
  real(8),             intent(in)     :: t
  real(8),              intent(in)    :: phi_node(:,:),phi(:,:)
  real(8),             intent(out)    :: cost
  !
  integer     :: i, j, k
  real*8     :: cost1, cost2
  !
  !
  
  cost1 = 0d0
  cost2 = 0d0
  do j = 1, nts_obs
    if ( abs(tim_obs(j)-t) < 1.0d-12 ) then
      ! Discrepancy of simulated and observed lake water quality
      do k = 1, nobs_node
        do i = 1, num_plt
          cost1 = cost1 + (plt_node_obs (j, i,k) - phi_node(nodeid_obs(k),i))**2
        end do
      end do
      ! Discrepancy of simulated and observed water quality in a river reach
      do k = 1, nobs_vol
        do i = 1, num_plt
          cost2 = cost2 + (plt_obs (j, i,k) - phi(volid_obs(k),i))**2
        end do
      end do
      
    end if
  end do
  
  cost = cost1 + cost2
  !
end subroutine cost_function


! Time-stamp: <*** get_boundary.f90: 2016/09/17 13:00 (Xijun LAI) ***>
!
subroutine get_boundary(tcur, phi_node)
  !
  use cmndat
  implicit none
  real*8,  intent(in)    :: tcur
  real*8,  intent(inout) :: phi_node(:,:)
  !
  real*8 :: wrk1,wrk2
  integer :: jt, k, i, iv, kts, ir, il

  !    Get given boundary concentration
  do iv = 1, nv
    if(s(iv).knd == 1) then
      k = 0
      do i = 1, num_plt
        select case ( name_plt(i) )
          !    
        case ('COD')
          k = k + 1
          
          kts = s(iv).its
          do jt = 1, max_index_cod(kts)
            if(tcur > bc_cod_tim(jt, kts)) exit
          end do
          wrk1 = bc_cod_val(jt+1,kts)-bc_cod_val(jt,kts)
          wrk2 = bc_cod_tim(jt+1,kts)-bc_cod_tim(jt,kts)
          phi_node(iv,k) = bc_cod_val(jt,kts)+(tcur-bc_cod_tim(jt,kts))*wrk1/wrk2
          
        case('NH3')
          k = k + 1
!!!
        end select
      end do
    end if
  end do

  !    Get given polutant load
  do jt = 1, nts_load
    if(tcur > bctim(jt)) exit
  end do
  wrk2 = bctim(jt+1)-bctim(jt)

  !
  k = 0
  do i = 1, num_plt
    select case ( name_plt(i) )
      !    
    case ('COD')
      k = k + 1
      do ir = 1, nr
        wrk1 = cod_load_riv(ir,jt+1) - cod_load_riv(ir,jt)
        loadriv(ir,k) = cod_load_riv(ir,jt)+(tcur-bctim(jt))*wrk1/wrk2
        loadriv(ir,k) = loadriv(ir,k) + cod_load_fix(ir) 
      end do
      do il = 1, klake
        wrk1 = cod_load_lak(il,jt+1) - cod_load_lak(il,jt)
        loadlak(il,k) = cod_load_lak(il,jt)+(tcur-bctim(jt))*wrk1/wrk2
      end do
      
    case('NH3')
      k = k + 1
!!!
    end select
  end do

end subroutine get_boundary

! Description: 
!*****************************************************************************
!
!  ***   1-order upwind scheme including central scheme for diffusion term
!
!*****************************************************************************
!
!  Time-stamp: <*** Solvers.f90: 2016/09/17 13:02 (Xijun LAI) ***>
!
subroutine solvers(ndim,dtim,phi_node,phi)
  !  use mod_common
  USE cmndat
  use mod_control
  !
  implicit none

  integer, intent(in) :: ndim   ! number of transport variables
  real*8,  intent(in) :: dtim   !, diff(:),s1rd(:,:),sconst(:,:),rbeta(:,:)
  real*8,  intent(inout) :: phi_node(:,:),phi(:,:)
  !

  ! local variables
  real*8  :: qnlr(100),clr(100,ndim)
  !  real*8  :: ap(ndim), qnlr(100), awrk,aslr(100),clr(100,ndim)
  !  real*8  :: a1(300,ndim),rhs1(100,ndim)
  !  integer :: ia1(100),ja1(300)
  integer :: k, i,j, ir, knb, kvb, kve, iwrk,ig, idir,ics,i1, ilak
  integer :: kv, kcs,kvol,krow, icsb, icse, num_reach, nvals_cur, nvals
  integer :: nlr, kvolb, kvole, vol2cs(100), nzs, lgnb
  !  integer :: ia(size(phi,dim=1)+nv+1), ja(size(phi,dim=1)*4), nrows
  !  real*8  :: avalues(size(phi,dim=1)*4,ndim), rhs(size(phi,dim=1)+nv,ndim)
  !  real*8  :: uflow(size(qflow))
  !  real*8  :: xx(size(phi,dim=1)+nv)
  real*8,dimension(ndim) :: phil, phir, massin, flux
  !  real*8 :: sc0(size(phi,dim=1),ndim) 
  real*8 :: decay(ndim), release(ndim), wrk(ndim)
  real*8 :: vol, vel, qout,qin,vol1

  !  sc0 = sconst

  do k = 1, nv

    if(s(k).knd == 1) cycle   

    nlr = 0
    do i = 1, s(k).rnum
      ir = s(k).rindex(i)
      if ( ir <= nr ) then
        kcs = s(k).csindex(i)
        !vol_cs: for storing index of control volume corresponding to the index of cross-section
        !       which need to be given values before time looping

        nlr = nlr + 1
        !       vol2cs(nlr) =  vol_cs(kcs) + nv
        qnlr(nlr) = qcs(kcs)*s(k).direct(i)
        clr (nlr,:) = phi(vol_cs(kcs),:)
        !     aslr(nlr) = acs(kcs)
      else
        nlr = nlr + 1
        !  treatment for gate node 
        ig = ir-nr
        if ( s(k).n == gnode_beg(ig) )then
          knb = index_node(gnode_end(ig))
          qnlr(nlr) =   qgate(ig)
          clr (nlr,:) = phi_node(knb,:)
        else if ( s(k).n == gnode_end(ig) )then
          knb = index_node(gnode_beg(ig))
          qnlr(nlr) = - qgate(ig)
          clr (nlr,:) = phi_node(knb,:)
        end if
      end if
      !     end if
    end do

    ! sum inflow and outflow
    qout = 0d0
    qin  = 0d0
    massin = 0d0
    do j = 1, nlr
      if(qnlr(j)>0d0)then
        qout = qout+qnlr(j)
      else
        massin = massin-qnlr(j)*clr(j,:)
        qin = qin - qnlr(j)
      end if
    end do

    ! ½ÚµãÈ¡ÅÅË®    
    if (s(k).qcon<0d0) then
      qout = qout - s(k).qcon
    else if(s(k).qcon>0d0)then
      qin = qin + s(k).qcon
      !!      Å¨¶ÈÔÝÓÃ±¾½ÚµãÅ¨¶ÈÌæ´ú
      massin = massin+s(k).qcon*phi_node(k,1:ndim)
    end if

    ! lake node
    ilak = s(k).ilak
    if (ilak>0) then
      vol = s(k).area*(znode(k)-s(k).zb)
      j = 0
      do i = 1, num_plt
        select case ( name_plt(i) )
          !    
        case ('COD')
          j = j + 1
          decay(j) = cod_k1_lak(ilak)
          release(j) = cod_r1_lak(ilak)*s(k).area
        case ('NH3')
        case default
        end select
      end do
      vol1 = vol+(qin-qout)*dtim
      decay = decay*0.5*(vol+vol1)*phi_node(k,:)
      decay = (decay-release)*dtim
      phi_node(k,:) = (phi_node(k,:)*vol + (massin-qout*phi_node(k,:))*dtim -decay(:)+ loadlak(ilak,:)*dtim)/vol1
    else
      if(qout>1.0d-12)then
        ! Correct water balance for non-storage node    
        if (abs(qin - qout)>0) then
          if ((qin-qout)/qout>1.d-2) then
            !     write(*,*) k,qin-qout
            !      stop 'Unbalanced water of node'
          end if
          qout = qout+(qin-qout)
        end if
        if (qout>1.0d-12)then
          phi_node(k,1:ndim) = massin/qout
        end if
      else
        !no change
      end if
    end if

  end do
  ! end node looping

  do ir = 1, nr

    icsb = cs_beg(ir); icse= cs_end(ir)
    kvb = index_node(rnode_beg(ir))
    kve = index_node(rnode_end(ir))

    num_reach = icse-icsb

    kvolb = vol_cs(icsb)
    kvole = vol_cs(icse)

    !    ! ²àÏò»ãÁ÷,¼ÙÉè»ãÁ÷Å¨¶ÈÒÔ¸Ãµ¥ÔªµÄÅ¨¶È´øÈë¼ÆËã
    !    do i = 0, num_reach-1
    !      if (inb(icsb+i).eq.2) then
    !        sc0(kvolb+i,:) = sc0(kvolb+i,:) + QLL(icsb+i)*phi(kvolb+i,:)    !/acs(icsb+i)
    !      end if
    !    end do

    do i = 1, num_reach
      kvol = kvolb+i-1
      phil = phi(kvol,:)
      ics  = icsb + i-1
      flux = 0d0
      do idir = 1, -1, -2
        knb = kvol+(idir+1)/2
        kcs = icsb + i-1+(idir+1)/2
        vel  = qcs(kcs)*idir
        !           vel  = vel*idir
        if(kcs==icsb)then
          phir = phi_node(kvb,:)
        else if(kcs==icse)then
          phir = phi_node(kve,:)
        else
          phir = phi(knb,:)
        end if

        !  1st-order upwind , advection part
        flux = flux + min(vel,0.d0)*phir + max(0.d0,vel)*phil
      end do

      !  source terms
      j = 0
      do i1 = 1, num_plt
        select case ( name_plt(i1) )
          !    
        case ('COD')
          j = j + 1
          decay(j) = cod_k1(ir)
          release(j) = cod_r1(ir)*dxe(ics)*(wb(ir)+slop(ir)*depth(ir))
        case ('NH3')
        case default
        end select
      end do

      vol = 0.5*(acs(ics)+acs(ics+1))*dxe(ics)
      vol1 = vol+(qcs(ics)-qcs(ics+1))*dtim
      decay = decay*0.5*(vol+vol1)*phil
      decay = (decay-release)*dtim
      phi(kvol,1:ndim) = (phi(kvol,1:ndim)* vol - dt1*flux - decay(:) + loadriv(ir,:)*dtim)/vol1
    end do
  end do
  ! end river looping

end subroutine solvers



! Time-stamp: <*** wqual_model.f90: 2016/09/17 13:03 (Xijun LAI) ***>
!
subroutine wqual_model(cost)

  use cmndat
  use mod_control
  implicit none
  real*8, intent(out) :: cost

  !  real*8, allocatable, dimension(:,:) :: plt, plt_node
  real*8, dimension(nvols,num_vars) :: plt  !, beta_plt, src0, src1
  !  real*8 :: diff(nvols)
  real*8 :: plt_node(nv,num_vars)
  real*8 :: tcur
  integer :: iday, jt, k, i, nct, ir, ivol, jvol

  interface

    subroutine solvers(ndim,dtim,phi_node,phi)
      !subroutine solvers(ndim,dtim,phi_node,phi,diff,sconst,s1rd,rbeta)
      integer, intent(in) :: ndim   ! number of transport variables
      real*8,  intent(in) :: dtim   !, diff(:),s1rd(:,:),sconst(:,:),rbeta(:,:)
      real*8,  intent(inout) :: phi_node(:,:),phi(:,:)
    end subroutine solvers

    subroutine cost_function ( t, phi_node, phi, cost )
      real(8),             intent(in)     :: t
      real(8),              intent(in)    :: phi_node(:,:),phi(:,:)
      real(8),             intent(out)    :: cost
    end subroutine cost_function

    subroutine get_boundary(tcur, phi_node)
      real*8,  intent(in)    :: tcur
      real*8,  intent(inout) :: phi_node(:,:)
    end subroutine get_boundary

  end interface

  !  diff = 0
  !  beta_plt = 1
  nct = int(86400/dt1)
  
 ! open(2,file=trim(prj1d)//'\out\COD_Node_day.txt')
 ! write(2,'(2000i12)') (s(k).n,k=1,nv)
 ! write(2,'(2000f12.3)') cod_node0(:)
  !
  ! Initialize
  k = 0
  do i = 1, num_plt
    
    select case ( name_plt(i) )
      !    
    case ('COD')
      k = k + 1
      plt_node(:,k) = cod_node0(:)
      plt(:,k) = cod0(:)
      
    case('NH3')
      k = k + 1
      !      plt_node(1:nv,k) = nh3_node(1:nv)
      !      plt(1:nvols,k) = nh3(1:nvols)
      
    end select
    !--------------------------------
  end do
  !
  cost = 0d0
  !³õÊ¼±³¾°³¡
  !
  do iday = day_beg+1, day_end

    write(*,*) 'Days:', iday
    !  get flow condition of each cross section
    do ir = 1, nr
      do i = cs_beg(ir), cs_end(ir)
        depth(i) = zcs_day(i,iday)-zb(ir)
        qcs(i)   = qcs_day(i,iday)
        acs(i) = 0.5*(2*wb(ir)+slop(ir)*depth(i))*depth(i)
      end do
    end do
    qgate(:) = qgate_day(:,iday)
    znode(:) = znode_day(:,iday)       

    !    znode0(:) = znode(:)
    do jt = 1, nct
      !    print*,iday, jt
      tcur = jt*dt1+(iday-1)*86400.
      call get_boundary(tcur,plt_node(:,:))

      !        call solvers(num_vars,dt1, plt_node(:,:),plt(:,:),diff,src0,src1,beta_plt(:,:))
      call solvers(num_vars,dt1, plt_node(:,:),plt(:,:))

      call cost_function(tcur, plt_node, plt, cost)
      !        k = 0
      !        do i = 1, num_plt
      !!           
      !           select case ( name_plt(i) )
      !           case ('COD')
      !              k = k + 1
      !              cod_node(:) = plt_node(:,k)
      !              cod(:)      = plt(:,k)
      !           case('NH3')
      !              k = k + 1
      !              
      !           end select
      !        end do
      !!
    end do
    ! end day-time loop

    !        k = 0
    !        do i = 1, num_plt
    !!           
    !           select case ( name_plt(i) )
    !           case ('COD')
    !              k = k + 1
    !!              write(2,'(2000f12.3)') cod_node(:)
    !              write(2,'(2000f12.3)') plt_node(:,k)
    !           case('NH3')
    !              k = k + 1
    !!              write(2,*) cod_node(:)
    !           end select
    !        end do
  end do

end subroutine wqual_model
