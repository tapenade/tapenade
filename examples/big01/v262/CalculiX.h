#define ARCH Linux

#define Linux 1
#define IRIX 2
#define IRIX64 3
#define HP 4

#if ARCH == Linux
#define FORTRAN(A,B) A##_  B
#elif ARCH == IRIX || ARCH == IRIX64
#define FORTRAN(A,B) A##_##B
#elif ARCH == HP
#define FORTRAN(A,B) A##B
#endif

#if ARCH == Linux
#define CEE(A,B) A##_  B
#elif ARCH == IRIX || ARCH == IRIX64
#define CEE(A,B) A##_##B
#elif ARCH == HP
#define CEE(A,B) A##B
#endif

#define NNEW(a,b,c) a=calloc((c),sizeof(b));
//#define NNEW(a,b,c) a=(b *)u_calloc((c),sizeof(b),__FILE__,__LINE__,#a)

// #define RENEW(a,b,c) a=realloc((b *)(a),(c)*sizeof(b))

#define RENEW(a,b,c) free(a); a=calloc((b *)(a),(c)*sizeof(b))


//#define RENEW(a,b,c) a=(b *)u_realloc((b *)(a),(c)*sizeof(b),__FILE__,__LINE__,#a)


#define SFREE(a) free(a)
//#define SFREE(a) u_free(a,__FILE__,__LINE__,#a)

// #define RENEW(a,b,c) a=(b *) realloc((b *)(a),(c)*sizeof(b))

#define DMEMSET(a,b,c,d) for(im=b;im<c;im++)a[im]=d

#ifdef LONGLONG
#define ITG long long
#define ITGFORMAT "lld"
#else
#define ITG int
#define ITGFORMAT "d"
#endif

void linstatic(double *co,ITG *nk,ITG **konp,ITG **ipkonp,char **lakonp,
	     ITG *ne, 
	     ITG *nodeboun,ITG *ndirboun,double *xboun,ITG *nboun, 
	     ITG *ipompc,ITG *nodempc,double *coefmpc,char *labmpc,
             ITG *nmpc, 
	     ITG *nodeforc,ITG *ndirforc,double *xforc,ITG *nforc, 
	     ITG *nelemload,char *sideload,double *xload,
	     ITG *nload, ITG *nactdof, 
	     ITG **icolp,ITG *jq,ITG **irowp,ITG *neq,ITG *nzl, 
	     ITG *nmethod,ITG *ikmpc,ITG *ilmpc,ITG *ikboun, 
	     ITG *ilboun,
	     double *elcon,ITG *nelcon,double *rhcon,ITG *nrhcon,
	     double *alcon,ITG *nalcon,double *alzero,ITG **ielmatp,
	     ITG *ielorien,ITG *norien,double *orab,ITG *ntmat_,
	     double *t0,double *t1,double *t1old, 
	     ITG *ithermal,double *prestr,ITG *iprestr, 
	     double *vold,ITG *iperturb,double *sti,ITG *nzs, 
	     ITG *kode,char *filab,double *eme,
             ITG *iexpl,double *plicon,ITG *nplicon,double *plkcon,
             ITG *nplkcon,
             double **xstatep,ITG *npmat_,char *matname,ITG *isolver,
	     ITG *mi,ITG *ncmat_,ITG *nstate_,double *cs,ITG *mcs,
             ITG *nkon,double **enerp,double *xbounold,
	     double *xforcold,double *xloadold,
             char *amname,double *amta,ITG *namta,
             ITG *nam,ITG *iamforc,ITG *iamload,
             ITG *iamt1,ITG *iamboun,double *ttime,char *output, 
             char *set,ITG *nset,ITG *istartset,
             ITG *iendset,ITG *ialset,ITG *nprint,char *prlab,
             char *prset,ITG *nener,double *trab, 
             ITG *inotr,ITG *ntrans,double *fmpc,char *cbody,ITG *ibody,
	     double *xbody,ITG *nbody,double *xbodyold,double *timepar, 
	     double *thicke, char *jobnamec,char *tieset,ITG *ntie,
	     ITG *istep,ITG *nmat,ITG *ielprop,double *prop,char *typeboun,
	     ITG *mortar,ITG *mpcinfo,double *tietol,ITG *ics,ITG *icontact,
		 ITG* maxvm_p, double* maxvm_pnorm);


void FORTRAN(mafillsm,(double *co,ITG *nk,ITG *kon,ITG *ipkon,char *lakon,
	       ITG *ne,ITG *nodeboun,ITG *ndirboun,double *xboun, 
	       ITG *nboun,ITG *ipompc,ITG *nodempc,double *coefmpc, 
	       ITG *nmpc,ITG *nodeforc,ITG *ndirforc,
	       double *xforc,ITG *nforc,ITG *nelemload,char *sideload,
	       double *xload,ITG *nload,double *xbody,ITG *ipobody,
	       ITG *nbody,double *cgr,
	       double *ad,double *au,double *bb,ITG *nactdof, 
	       ITG *icol,ITG *jq,ITG *irow,ITG *neq,ITG *nzl, 
	       ITG *nmethod,ITG *ikmpc,ITG *ilmpc,ITG *ikboun, 
	       ITG *ilboun,
	       double *elcon,ITG *nelcon,double *rhcon,ITG *nrhcon,
	       double *alcon,ITG *nalcon,double *alzero,ITG *ielmat,
	       ITG *ielorien,ITG *norien,double *orab,ITG *ntmat_,
	       double *t0,double *t1,ITG *ithermal,
	       double *prestr,ITG *iprestr,double *vold,
	       ITG *iperturb,double *sti,ITG *nzs,double *stx,
	       double *adb,double *aub,ITG *iexpl,
               double *plicon,ITG *nplicon,double *plkcon,ITG *nplkcon,
               double *xstiff, 
	       ITG *npmat_,double *dtime,char *matname,ITG *mi,
               ITG *ncmat_,ITG *mass,ITG *stiffness,ITG *buckling,ITG *rhs,
               ITG *intscheme,double *physcon,double *shcon,ITG *nshcon,
               double *cocon,ITG *ncocon,double *ttime,double *time,
               ITG *istep,ITG *kinc,ITG *coriolis,ITG *ibody,
	       double *xloadold,double *reltime,double *veold,
               double *springarea,ITG *nstate_,double *xstateini,
	       double *xstate,double *thicke,
               ITG *integerglob,double *doubleglob,char *tieset,
	       ITG *istartset,ITG *iendset,ITG *ialset,ITG *ntie,
	       ITG *nasym,double *pslavsurf,double *pmastsurf,ITG *mortar,
	       double *clearini,ITG *ielprop,double *prop));



void results(double *co,ITG *nk,ITG *kon,ITG *ipkon,char *lakon,
	     ITG *ne,double *v,double *stn,ITG *inum, 
	     double *stx,
	     double *elcon,ITG *nelcon,double *rhcon,ITG *nrhcon,
	     double *alcon,ITG *nalcon,double *alzero,ITG *ielmat,
	     ITG *ielorien,ITG *norien,double *orab,ITG *ntmat_,
	     double *t0,double *t1,ITG *ithermal,double *prestr, 
             ITG *iprestr,char *filab,double *eme,double *emn,
             double *een,ITG *iperturb,double *f,double *fn,ITG *nactdof,
             ITG *iout,double *qa,
	     double *vold,double *b,ITG *nodeboun,ITG *ndirboun,
	     double *xboun,ITG *nboun,ITG *ipompc,ITG *nodempc,
	     double *coefmpc,char *labmpc,ITG *nmpc,ITG *nmethod, 
             double *vmax,ITG *neq,double *veold,double *accold,
	     double *beta,double *gamma,double *dtime,double *time,
             double *ttime,double *plicon,
             ITG *nplicon,double *plkcon,ITG *nplkcon,
             double *xstateini,double *xstiff,double *xstate,ITG *npmat_,
	     double *epl,char *matname,ITG *mi,ITG *ielas,
	     ITG *icmd,ITG *ncmat_,ITG *nstate_,double *stiini,
	     double *vini,ITG *ikboun,ITG *ilboun,double *ener,
	     double *enern,double *emeini,double *xstaten,double *eei,
             double *enerini,double *cocon,ITG *ncocon,char *set, 
             ITG *nset,ITG *istartset,
             ITG *iendset,ITG *ialset,ITG *nprint,char *prlab,
             char *prset,double *qfx,double *qfn,double *trab,
             ITG *inotr,ITG *ntrans,double *fmpc,ITG *nelemload,
	     ITG *nload,ITG *ikmpc,ITG *ilmpc,ITG *istep,ITG *iinc,
	     double *springarea,double *reltime,ITG *ne0,double *xforc,
             ITG *nforc,double *thicke,
             double *shcon,ITG *nshcon,char *sideload,double *xload,
             double *xloadold,ITG *icfd,ITG *inomat,double *pslavsurf,
             double *pmastsurf,ITG *mortar,ITG *islavact,double *cdn,
             ITG *islavnode,ITG *nslavnode,ITG *ntie,double *clearini,
             ITG *islavsurf,ITG *ielprop,double *prop);

void FORTRAN(resultsini,(ITG *nk,double *v,ITG *ithermal,char *filab,
       ITG *iperturb,double *f,double *fn,ITG *nactdof,ITG *iout,
       double *qa,double *vold,double *b,ITG *nodeboun,ITG *ndirboun,
       double *xboun,ITG *nboun,ITG *ipompc,ITG *nodempc,double *coefmpc,
       char *labmpc,ITG *nmpc,ITG *nmethod,double *cam,ITG *neq,
       double *veold,double *accold,double *bet,double *gam,double *dtime,
       ITG *mi,double *vini,ITG *nprint,char *prlab,ITG *intpointvar,
       ITG *calcul_fn,ITG *calcul_f,ITG *calcul_qa,ITG *calcul_cauchy,
       ITG *iener,ITG *ikin,ITG *intpointvart,double *xforc,ITG *nforc));



void  FORTRAN(resultsprint,(double *co,ITG *nk,ITG *kon,ITG *ipkon,
       char *lakon,ITG *ne,double *v,double *stn,ITG *inum,double *stx,
       ITG *ielorien,ITG *norien,double *orab,double *t1,ITG *ithermal,
       char *filab,double *een,ITG *iperturb,double *fn,ITG *nactdof,
       ITG *iout,double *vold,ITG *nodeboun,ITG *ndirboun,ITG *nboun,
       ITG *nmethod,double *ttime,double *xstate,double *epn,ITG *mi,
       ITG *nstate_,double *ener,double *enern,double *xstaten,double *eei,
       char *set,ITG *nset,ITG *istartset,ITG *iendset,ITG *ialset,ITG *nprint,
       char *prlab,char *prset,double *qfx,double *qfn,double *trab,ITG *inotr,
       ITG *ntrans,ITG *nelemload,ITG *nload,ITG *ikin,ITG *ielmat,
       double *thicke,double *eme,double *emn,double *rhcon,ITG *nrhcon,
       double *shcon,ITG *nshcon,double *cocon,ITG *ncocon,ITG *ntmat_,
       char *sideload,ITG *icfd,ITG *inomat,double *pslavsurf,
       ITG *islavact,double *cdn,ITG *mortar,ITG *islavnode,ITG *nslavnode,
       ITG *ntie,ITG *islavsurf,double *time,ITG *ielprop,double *prop,
       double *veold));


void FORTRAN(stop,());

void resultsvm(double *co,ITG *nk,ITG *kon,ITG *ipkon,char *lakon,
	     ITG *ne,double *v,double *stn,ITG *inum,
	     double *stx,
	     double *elcon,ITG *nelcon,double *rhcon,ITG *nrhcon,
	     double *alcon,ITG *nalcon,double *alzero,ITG *ielmat,
	     ITG *ielorien,ITG *norien,double *orab,ITG *ntmat_,
	     double *t0,double *t1,ITG *ithermal,double *prestr,
             ITG *iprestr,char *filab,double *eme,double *emn,
             double *een,ITG *iperturb,double *f,double *fn,ITG *nactdof,
             ITG *iout,double *qa,
	     double *vold,double *b,ITG *nodeboun,ITG *ndirboun,
	     double *xboun,ITG *nboun,ITG *ipompc,ITG *nodempc,
	     double *coefmpc,char *labmpc,ITG *nmpc,ITG *nmethod,
             double *vmax,ITG *neq,double *veold,double *accold,
	     double *beta,double *gamma,double *dtime,double *time,
             double *ttime,double *plicon,
             ITG *nplicon,double *plkcon,ITG *nplkcon,
             double *xstateini,double *xstiff,double *xstate,ITG *npmat_,
	     double *epl,char *matname,ITG *mi,ITG *ielas,
	     ITG *icmd,ITG *ncmat_,ITG *nstate_,double *stiini,
	     double *vini,ITG *ikboun,ITG *ilboun,double *ener,
	     double *enern,double *emeini,double *xstaten,double *eei,
             double *enerini,double *cocon,ITG *ncocon,char *set,
             ITG *nset,ITG *istartset,
             ITG *iendset,ITG *ialset,ITG *nprint,char *prlab,
             char *prset,double *qfx,double *qfn,double *trab,
             ITG *inotr,ITG *ntrans,double *fmpc,ITG *nelemload,
	     ITG *nload,ITG *ikmpc,ITG *ilmpc,ITG *istep,ITG *iinc,
	     double *springarea,double *reltime,ITG *ne0,double *xforc,
             ITG *nforc,double *thicke,
             double *shcon,ITG *nshcon,char *sideload,double *xload,
             double *xloadold,ITG *icfd,ITG *inomat,double *pslavsurf,
             double *pmastsurf,ITG *mortar,ITG *islavact,double *cdn,
             ITG *islavnode,ITG *nslavnode,ITG *ntie,double *clearini,
             ITG *islavsurf,ITG *ielprop,double *prop,ITG* maxvm_p, double* maxvm_pnorm);
