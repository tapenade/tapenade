* COMMON du maillage pastille : fichier cmogmai.inc
************************************************************************
*
      COMMON /MOGMA1/ xmo_RPAST(mo_M31MAX,mo_IGRMAX,mo_IFEMAX),
     &                xmo_HHREF(mo_M31MAX),
     &                xmo_PRODIS(mo_M31MAX)
*
      COMMON /MOGMA2/ mo_NBNOZO(mo_M31MAX,mo_IGRMAX),
     &                mo_NB_ZONE(mo_M31MAX),
     &                mo_NB_TRAX
*