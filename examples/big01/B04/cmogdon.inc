* COMMON des donn�es : fichier cmogdon.inc
************************************************************************
*
      COMMON /MOGDO1/ 
     &       xmo_AGG3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_VTPORI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_VTPORJI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_CPORI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_CPORJI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_ATPOR3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_ATPORJ3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_CXEPORI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_CXEPORJI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_MAVOLI3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       xmo_AGG, 
     &       xmo_VTPORI, 
     &       xmo_VTPORJI, 
     &       xmo_CPORI, 
     &       xmo_CPORJI, 
     &       xmo_ATPOR, 
     &       xmo_ATPORJ,
     &       xmo_CXEPORI, 
     &       xmo_CXEPORJI,
     &       xmo_MAVOLI,
     &       xmo_PREFRI(mo_M31MAX), 
     &       xmo_TEMFRI(mo_M31MAX)
*
      COMMON /MOGDO2/ 
     &       mo_MATER3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &       mo_MATER