C        Generated by TAPENADE     (INRIA, Ecuador team)
C  Tapenade 3.13 (r6968M) - 18 Jul 2018 18:14
C
      DOUBLE PRECISION xmo_tvseqr
      DOUBLE PRECISION xmo_tmseqr
      DOUBLE PRECISION xmo_tvgg
      DOUBLE PRECISION xmo_timrseq
      DOUBLE PRECISION xmo_timqseq
      DOUBLE PRECISION xmo_timseqr
      DOUBLE PRECISION xmo_timpgg
      DOUBLE PRECISION xmo_timpgg_etat2
      DOUBLE PRECISION xmo_timrgg
      DOUBLE PRECISION xmo_timseq
      DOUBLE PRECISION xmo_tmseq
      DOUBLE PRECISION xmo_tmgg
      DOUBLE PRECISION xmo_timqseqr
      DOUBLE PRECISION xmo_tvseq
      DOUBLE PRECISION xmo_timgg
      DOUBLE PRECISION xmo_timrseqr
      DOUBLE PRECISION xmo_timqgg
C
C     ------------------------------------------------------------------
C common des variables
C fichier cmogmat.inc
C***********************************************************************
C-----7--0---------0---------0---------0---------0---------0---------0--
C     ------------------------------------------------------------------
C
C
      COMMON /mogmat/ xmo_tmgg(mo_nnsphmx, mo_nnsphmx), xmo_timgg(
     +mo_nnsphmx, mo_nnsphmx), xmo_timrgg(mo_nnsphmx, mo_nnsphmx), 
     +xmo_tvgg(mo_nnsphmx), xmo_timqgg(mo_nnsphmx), xmo_timpgg(
     +mo_nnsphmx), xmo_timpgg_etat2(mo_nnsphmx), xmo_tmseq(mo_nnsphmx, 
     +mo_nnsphmx), xmo_timseq(mo_nnsphmx, mo_nnsphmx), xmo_timrseq(
     +mo_nnsphmx, mo_nnsphmx), xmo_tvseq(mo_nnsphmx), xmo_timqseq(
     +mo_nnsphmx), xmo_tmseqr(mo_nnsphmx, mo_nnsphmx), xmo_timseqr(
     +mo_nnsphmx, mo_nnsphmx), xmo_timrseqr(mo_nnsphmx, mo_nnsphmx), 
     +xmo_tvseqr(mo_nnsphmx), xmo_timqseqr(mo_nnsphmx)
      DOUBLE PRECISION xmo_tvggd(mo_nnsphmx)
      DOUBLE PRECISION xmo_tvseqd(mo_nnsphmx)

