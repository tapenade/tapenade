* common des grandeurs informatiques
* fichier cmoginf.inc
************************************************************************
*     ------------------------------------------------------------------             
      COMMON /MOGIF1/ 
     &                xmo_TPSAFF(2),
     &                xmo_TPSREP(mo_NREPRISEMX), 
     &                xmo_TPSINI
*
      COMMON /MOGIF2/ 
     &         mo_IMPR_ANA(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_IFICHMX), 
     &         mo_NORDR_ANA(mo_M31MAX,mo_IGRMAX,mo_IFEMAX),
     &         mo_NBPTANA,
     &         mo_NTPSREP(mo_NREPRISEMX),
     &         mo_TRANCHE_DEB,
     &         mo_TRANCHE_FIN, 
     &                mo_NLEC, 
     &                mo_NSOR,
     &                mo_DEBUG,
     &                mo_NNRES1(2), 
     &                mo_NNRES2(2), 
     &                mo_NNRES3(2), 
     &                mo_NNRES4(2),
     &                mo_NNRES5(2), 
     &                mo_NNSYNT(2), 
     &                mo_NRES1, 
     &                mo_NRES2, 
     &                mo_NRES3, 
     &                mo_NRES4, 
     &                mo_NRES5, 
     &                mo_NSYNT, 
     &                mo_NMAC,
     &                mo_NINTEG,
     &                mo_NREPW, 
     &                mo_NREPR, 
     &                mo_NREPRISE,
     &                mo_IMPR1,
     &                mo_IMPR2,
     &                mo_IMPR3,
     &                mo_IMPR4,
     &                mo_IMPR5,
     &                mo_IMPRSYNT,
     &                mo_DEPART_SUR_REPRISE, 
     &                mo_NREPRISELU,
     &                mo_NTPSINI, 
     &                mo_PASSAGE, 
     &                mo_FREQIMPR  
*     ------------------------------------------------------------------
      CHARACTER*80  NOM_FICH_REPRISE 
*            
      COMMON /MOGIF3/ 
     &               NOM_FICH_REPRISE    
*     ==================================================================