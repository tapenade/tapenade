* common des paramètres secondaires
* fichier cmogpas.inc
************************************************************************
*     ------------------------------------------------------------------
      COMMON /MOGPAS/ xmo_BETAC, xmo_DXE, xmo_DXEJ, xmo_DXETHMIX,
     &                xmo_DXEJTHMIX, xmo_BJ, 
     &                xmo_KG, xmo_DUS, xmo_DS, xmo_DBVOL,
     &                xmo_DBSUR, xmo_DUJ, xmo_DU, xmo_VBSUR, xmo_VBVOL,
     &                xmo_DBUCH,  xmo_DBJVO, xmo_DBJSU,
     &                xmo_VBJVOL, xmo_VBJSUR, xmo_KGJ, xmo_FACGCJ,
     &                xmo_FACPBJ, xmo_OMSKT, xmo_PVI,  
     &                xmo_PVIJ1_B, xmo_PVIJ2,
     &                xmo_PVIJ1_POR,   
     &                xmo_FACRHO, xmo_GAMMA, 
     &                xmo_DISTCOAL,xmo_DISTCOALJ,
     &                xmo_KGJ1, xmo_KGJ2
*
*     ------------------------------------------------------------------