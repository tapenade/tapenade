C       *********************************
C       *         @(#)crednbr.inc	1.1     7/27/98           *
C       *********************************
      COMMON / REDNBR / DFLOT,NFIXE,ITYPE,NBERR,NCAR
      DOUBLE PRECISION DFLOT
C
C DFLOT  = reel lu (R*8)
C NFIXE  = entier lu
C ITYPE  = type de variable u (1:ENTIER , 2:REEL ,
C           3:MOT , 4:TEXTE , <0:ERREUR , 0:ERREUR DE FICHIER)
C NBERR  = incremente les erreurs
C NCAR   = nombre de caracteres de la variable lue

