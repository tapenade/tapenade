* common des constantes physiques
* fichier cmogphy.inc
*
*     ------------------------------------------------------------------
* Attention les valeurs de xmo_XNAVOG, xmo_BOLTZ, xmo_PIZAHL
* doivent �tre �gales aux valeurs XNAVOG, BOLTZ, PIZAHL de METEOR

*
      COMMON /MOGPHY/ 
     &               xmo_XNAVOG,
     &               xmo_BOLTZ,
     &               xmo_PIZAHL,
     &               xmo_OMEGA,
     &               xmo_RAT, 
     &               xmo_ZXE, 
     &               xmo_ELEC, 
     &               xmo_EPSI0, 
     &               xmo_TCXE, 
     &               xmo_PCXE, 
     &               xmo_BVDW, 
     &               xmo_AVDW, 
     &               xmo_ZD
*
*     ------------------------------------------------------------------