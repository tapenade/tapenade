C Example from CEA Cadarache. Code Mogador, cf Bertrand IOOSS, 2/2003
      SUBROUTINE MOFDER_GEAR(NEQ,T,Y,YDOT)
C     ====================================
C
C
C     ******************************************************************
C     MOFDER : MOgador calcul de la Fonction DERiv�e des vecteurs des 
C              variables de gaz et de d�faut                                    
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogpas.inc'
C
C NEQ est le nombre d'equations effectives � r�soudre
C NEQ prend la valeur de mo_G_NEQ � l'appel du solveur lsoda
C Y a pour longueur utile G_NSEQ (= NEQ)
C xmo_YEXP a pour longueur utile mo_NGG+3+mo_NSEQ_C+3+7 (cas classique)
************************************************************************
* cmogvar.inc : mo_IET mo_NVG mo_DIF_SEQ en entr�e 
* cmogpnu.inc : mo_NGG mo_NSEQ  mo_NSEQR en entr�e
* cmogpte.inc : xmo_TEST0 xmo_TEST4 xmo_TEST7 xmo_TEST8  xmo_TEST9 en entr�e
* cmogear.inc : xmo_YEXP mo_NEQG mo_NEQD mo_IGTAB mo_IDTAB en entr�e et sortie
* cmogpas.inc : xmo_FACRHO en entr�e
************************************************************************
*
      DOUBLE PRECISION Y(*),YDOT(*)
*
      DOUBLE PRECISION xmo_DERG(mo_NVGAMX),
     &                 xmo_DERD(mo_NVDEMX),
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX),
     &                 xmo_XE_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XE_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XE_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XE_CREATION(mo_NNSPHMX),
     &                 xmo_XE_REMJOINT(mo_NNSPHMX),
     &                 xmo_XE_REMBUL(mo_NNSPHMX),
     &                 xmo_XE_REMPOR(mo_NNSPHMX),
     &                 xmo_XE_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XE_SORTIEGRAIN(mo_NNSPHMX),
     &                 xmo_XE_CLIM(mo_NNSPHMX),
     &                 xmo_XERBLOC_EVOL(mo_NNSPHMX),
     &                 xmo_XEJ_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XEJ_SOURCE(mo_NNSPHMX),
     &                 xmo_XEJ_REMJOINT(mo_NNSPHMX),
     &                 xmo_XEJ_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEJ_CLIM3(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGTUN(mo_NNSPHMX),
     &                 xmo_XEJ_SORTIEJ(mo_NNSPHMX),
     &                 xmo_XEHOM_CREATION(mo_NNSPHMX),
     &                 xmo_XEHOM_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEHOM_APPORT(mo_NNSPHMX),
     &                 xmo_XEHOM_CLIM3(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOFDER_GEAR')
*
CDEBUG
CDEBUG        WRITE(20,*) 'debut mofder_gear mo_NEQG = ',mo_NEQG
CDEBUG        WRITE(20,*) 'debut mofder_gear Y(1+mo_NEQG) = ',
CDEBUG     &               Y(1+mo_NEQG)
CDEBUG
*
       CALL MOCORC_GEAR(NEQ,T,Y)
*
CDEBUG
CDEBUG        WRITE(20,*) 'mofder_gear apres MOCORC_GEAR mo_NVG = ',mo_NVG
CDEBUG        WRITE(20,*) 
CDEBUG     &   'mofder_gear apres MOCORC_GEAR xmo_YEXP(1+mo_NVG) = ',
CDEBUG     &               xmo_YEXP(1+mo_NVG)
CDEBUG
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
*
************************************************************************
* ETAT CLASSIQUE (0)
************************************************************************
* On trouve dans le tableau 'gaz', dans l'ordre :
* - gaz dissous intra
* - bulles (nombre gaz et volume) intra
* - gaz dissous inter
* - bulles (nombre gaz et volume) inter
*
* On trouve dans le tableau 'd�faut', dans l'ordre :
* - pores (nombre gaz et volume) intra
* - pores (nombre gaz et volume) inter
* - densit� de dislocation 
************************************************************************
      IF (mo_IET .EQ. 0) THEN
*
* Tranfert dans les tableaux 'gaz' et 'd�fauts'
*
        DO I = 1,mo_NVG
          xmo_VG1(I) = xmo_YEXP(I)
        ENDDO
*
        DO I = 1,mo_NVD
          xmo_VD1(I) = xmo_YEXP(I+mo_NVG)
        ENDDO
*
CDEBUG
CDEBUG        WRITE(20,*) 'dans mofder_gear mo_NVG = ',mo_NVG
CDEBUG        WRITE(20,*) 'dans mofder_gear xmo_VD1(1) = ',xmo_VD1(1)
CDEBUG
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
*
* Calcul de termes des d�riv�es des variables intra
*
        CALL MOTERM(
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
* essaiLOC
     &      T,
* essaiLOC
     &
     &      xmo_XE_NUCLHOMO,
     &      xmo_XE_PIEGBUL,xmo_XE_PIEGPOR,xmo_XE_CREATION,
     &      xmo_XE_REMJOINT,xmo_XE_REMBUL,xmo_XE_REMPOR,
     &      xmo_XE_DIFFUSION,xmo_XE_SORTIEGRAIN,xmo_XE_CLIM,
     &      xmo_XERBLOC_EVOL,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,xmo_B_COALEVB,xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,xmo_XEB_PIEGBPOR,xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,xmo_VTB_GAZ,xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,xmo_POR_COALEVPOR,xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,xmo_XEPOR_PIEGBPOR,xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,xmo_VTPOR_GAZ,xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,xmo_VTPOR_SORTIEGRAIN,
     &      xmo_CXECR  )
*
* Calcul des termes des d�riv�es des variables inter dans le cas 
* o� l'�tat du combustible est "classique" (0)
*
      CALL MOTRM0(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEJ_NUCLHOMO,xmo_XEJ_PIEGBUL,xmo_XEJ_PIEGPOR,
     &      xmo_XEJ_SOURCE,xmo_XEJ_REMJOINT,xmo_XEJ_DIFFUSION,
     &      xmo_XEJ_CLIM3,xmo_XEJ_SORTIEJ,
     &      xmo_BJ_NUCLHOMO,xmo_BJ_APPORT,xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,xmo_BJ_REMBUL,xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,xmo_XEBJ_APPORT,xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,xmo_XEBJ_REMBUL,xmo_XEBJ_SORTIEJ,
     &      xmo_XEBJ_PERCBPOR,xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,xmo_VTBJ_GAZ,xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,xmo_VTBJ_APPORT,xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,xmo_XEPORJ_APPORT,xmo_XEPORJ_SORTIEJ,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,xmo_VTPORJ_GAZ,xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,xmo_VTPORJ_SORTIEJ,
     &      
     &      xmo_SOURXE,xmo_HJ,xmo_HJPOR,xmo_BBJ,xmo_BPORJ,
     &      xmo_OUTSE,xmo_OUTEJEC,xmo_CXEJM,xmo_PINTBJ)
*
*
* CALCUL DES DERIVEES TEMPORELLES***************************************
************************************************************************
* d�riv�e temporelle de la concentration du vecteur des concentrations 
* en gaz dissous xmo_CXE
*
        DO IVGA = 1, mo_NGG
          xmo_DERG(IVGA) = 
     &        xmo_XE_NUCLHOMO(IVGA)
     &      + xmo_XE_PIEGBUL(IVGA)
     &      + xmo_XE_PIEGPOR(IVGA)
     &      + xmo_XE_CREATION(IVGA)
     &      + xmo_XE_REMJOINT(IVGA) * xmo_TEST7
     &      + xmo_XE_REMBUL(IVGA)
     &      + xmo_XE_REMPOR(IVGA)
     &      + xmo_XE_DIFFUSION(IVGA)
     &      + xmo_XE_CLIM(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration  en gaz dissous au voisinage des bulles
        DO IVGA = 1, mo_NGG
          xmo_DERG(IVGA + mo_NGG) = 
     &        xmo_XERBLOC_EVOL(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration de bulles intra xmo_CB
*
        xmo_DERG(2*mo_NGG+1) =
     &        xmo_B_NUCLHOMO
     &      + xmo_B_COALEDB
     &      + xmo_B_COALEVB
     &      + xmo_B_COALEGEOM
     &      + xmo_B_PIEGBPOR
     &      + xmo_B_REMBUL
     &      + xmo_B_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration du gaz de bulles intra xmo_CXEB
*
        xmo_DERG(2*mo_NGG+2) =
     &        xmo_XEB_NUCLHOMO
     &      + xmo_XEB_PIEGBUL
     &      + xmo_XEB_PIEGBPOR
     &      + xmo_XEB_REMBUL
     &      + xmo_XEB_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle du volume total de bulles intra xmo_VTB
*
        xmo_DERG(2*mo_NGG+3) =
     &        xmo_VTB_LACUNE
     &      + xmo_VTB_GAZ
     &      + xmo_VTB_PIEGBPOR
     &      + xmo_VTB_REMBUL
     &      + xmo_VTB_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz inter dissous
*
        DO IVGA = 1, mo_NSEQ_C
          xmo_DERG(2*mo_NGG+3+IVGA) =
     &         xmo_XEJ_NUCLHOMO(IVGA)
     &       + xmo_XEJ_PIEGBUL(IVGA) 
     &       + xmo_XEJ_PIEGPOR(IVGA)
     &       + xmo_XEJ_REMJOINT(IVGA)
     &       + xmo_XEJ_SOURCE(IVGA) * xmo_TEST0
     &       + xmo_XEJ_DIFFUSION(IVGA)
     &       + xmo_XEJ_CLIM3(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles inter xmo_CBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+1) = 
     &        xmo_BJ_NUCLHOMO
     &      + xmo_BJ_APPORT * xmo_TEST0 * xmo_TEST8
     &      + xmo_BJ_COALEDBJ
     &      + xmo_BJ_COALEVBJ
     &      + xmo_BJ_COALEGEOMJ
     &      + xmo_BJ_PIEGBPOR
     &      + xmo_BJ_REMBUL
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de bulles inter xmo_CXEBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+2) = 
     &        xmo_XEBJ_NUCLHOMO
     &      + xmo_XEBJ_APPORT * xmo_TEST0 * xmo_TEST8
     &      + xmo_XEBJ_PIEGBUL
     &      + xmo_XEBJ_PIEGBPOR
     &      + xmo_XEBJ_REMBUL
     &      + xmo_XEBJ_PERCBPOR
     &      + xmo_XEBJ_PERCPLEN
*
************************************************************************
* d�riv�e temporelle du volume total de bulles inter xmo_VTBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3) =
     &        xmo_VTBJ_LACUNE
     &      + xmo_VTBJ_GAZ 
     &      + xmo_VTBJ_PIEGBPOR
     &      + xmo_VTBJ_REMBUL
     &      + xmo_VTBJ_APPORT * xmo_TEST0 * xmo_TEST8 
*
*
* DEBUT TABLEAU DEFAUT**************************************************
* d�riv�e temporelle de la concentration en pores intra xmo_CPOR
*
        xmo_DERD(1) =
     &        xmo_POR_COALEDPOR
     &      + xmo_POR_COALEVPOR
     &      + xmo_POR_REMPOR
     &      + xmo_POR_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores intra xmo_CXEPOR
*
        xmo_DERD(2) =
     &        xmo_XEPOR_PIEGPOR
     &      + xmo_XEPOR_PIEGBPOR
     &      + xmo_XEPOR_REMPOR
     &      + xmo_XEPOR_SORTIEGRAIN  
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores intra xmo_VTPOR
*
*
        xmo_DERD(3) =
     &        xmo_VTPOR_LACUNE
     &      + xmo_VTPOR_GAZ
     &      + xmo_VTPOR_PIEGBPOR
     &      + xmo_VTPOR_REMPOR
     &      + xmo_VTPOR_SORTIEGRAIN 
*
* Pour test o� le volume des pores est fixe
        xmo_DERD(3) = xmo_TEST4 * xmo_DERD(3)
*
*
*
************************************************************************
* d�riv�e temporelle de la concentration en pores inter xmo_CPORJ
*
        xmo_DERD(4) = 0.D0
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores inter xmo_CXEPORJ
*
        xmo_DERD(5) = 
     &        xmo_XEPORJ_PIEGPOR
     &      + xmo_XEPORJ_PIEGBPOR
     &      + xmo_XEPORJ_REMPOR
     &      + xmo_XEPORJ_APPORT * xmo_TEST0 
     &      + xmo_XEPORJ_PERCBPOR
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores inter xmo_VTPORJ
*
        xmo_DERD(6) =
     &        xmo_VTPORJ_LACUNE
     &      + xmo_VTPORJ_GAZ
     &      + xmo_VTPORJ_PIEGBPOR
     &      + xmo_VTPORJ_APPORT * xmo_TEST0 
*  
************************************************************************
* d�riv�e temporelle de la densit� de dislocation : 
*
        xmo_DERD(7) = xmo_FACRHO * xmo_RHOD
*
************************************************************************
************************************************************************
      ENDIF
************************************************************************
* FIN ETAT CLASSIQUE (0)
************************************************************************
*
*
*
************************************************************************
* ETAT 'EN RESTRUCTURATION' (2) 
************************************************************************
* On trouve dans le tableau 'gaz', dans l'ordre :
* - gaz dissous zones saines
* - bulles (nombre gaz et volume) de zones saines
* - gaz dissous zones restructur�es
* - bulles de rim (nombre gaz et volume) de zones restructur�es
*
* On trouve dans le tableau 'd�faut', dans l'ordre :
* - pores (nombre gaz et volume) de zones saines
* - pores de rim (nombre gaz et volume) de zones restructur�es
* - densit� de dislocation des zones saines
************************************************************************
      IF (mo_IET .EQ. 2) THEN
*
* Tranfert dans les tableaux 'gaz' et 'd�fauts'
*
        DO I = 1,mo_NVG
          xmo_VG1(I) = xmo_YEXP(I)
        ENDDO
*
        DO I = 1,mo_NVD
          xmo_VD1(I) = xmo_YEXP(I+mo_NVG)
        ENDDO
*
*
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
* Calcul de termes des d�riv�es des variables zones saines
* les entr�es 0.D0,0.D0,0.D0,0.D0 ne sont pas utilisees dans moterm
* lorsqu'on se trouve dans l'etat (2), comme c'est le cas ici
*
        CALL MOTERM(
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
* essaiLOC
     &      T,
* essaiLOC
     &      
     &      xmo_XE_NUCLHOMO,
     &      xmo_XE_PIEGBUL,xmo_XE_PIEGPOR,xmo_XE_CREATION,
     &      xmo_XE_REMJOINT,xmo_XE_REMBUL,xmo_XE_REMPOR,
     &      xmo_XE_DIFFUSION,xmo_XE_SORTIEGRAIN,xmo_XE_CLIM,
     &      xmo_XERBLOC_EVOL,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,xmo_B_COALEVB,xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,xmo_XEB_PIEGBPOR,xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,xmo_VTB_GAZ,xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,xmo_POR_COALEVPOR,xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,xmo_XEPOR_PIEGBPOR,xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,xmo_VTPOR_GAZ,xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,xmo_VTPOR_SORTIEGRAIN,
     &      xmo_CXECR  )
*
* Calcul des termes des d�riv�es des variables de joint de grain
*
      CALL MOTRM0(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEJ_NUCLHOMO,xmo_XEJ_PIEGBUL,xmo_XEJ_PIEGPOR,
     &      xmo_XEJ_SOURCE,xmo_XEJ_REMJOINT,xmo_XEJ_DIFFUSION,
     &      xmo_XEJ_CLIM3,xmo_XEJ_SORTIEJ,
     &      xmo_BJ_NUCLHOMO,xmo_BJ_APPORT,xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,xmo_BJ_REMBUL,xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,xmo_XEBJ_APPORT,xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,xmo_XEBJ_REMBUL,xmo_XEBJ_SORTIEJ,
     &      xmo_XEBJ_PERCBPOR,xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,xmo_VTBJ_GAZ,xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,xmo_VTBJ_APPORT,xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,xmo_XEPORJ_APPORT,xmo_XEPORJ_SORTIEJ,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,xmo_VTPORJ_GAZ,xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,xmo_VTPORJ_SORTIEJ,
     &      
     &      xmo_SOURXE,xmo_HJ,xmo_HJPOR,xmo_BBJ,xmo_BPORJ,
     &      xmo_OUTSE,xmo_OUTEJEC,xmo_CXEJM,xmo_PINTBJ)
*
* Calcul des termes des d�riv�es des variables des zones resructur�es
* dans le cas o� l'�tat du combustible est "en restructuration" (2)
*
      CALL MOTRM2(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_APPORT,
     &      xmo_BRIM_APPORT,
     &      xmo_XEBRIM_APPORT,
     &      xmo_VTBRIM_APPORT,
     &      xmo_PORRIM_APPORT,
     &      xmo_XEPORRIM_APPORT,
     &      xmo_VTPORRIM_APPORT )
*
*
      CALL MOTRM3(
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_CREATION,xmo_XEHOM_DIFFUSION,
     &      xmo_XEHOM_CLIM3,
     &      
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ,
     &      
     &      xmo_CXEHOMM,xmo_OUTSE,xmo_OUTEJEC_RIM )
*
*
* CALCUL DES DERIVEES TEMPORELLES***************************************
************************************************************************
*
*
* d�riv�e temporelle de la concentration du vecteur des concentrations 
* en gaz dissous xmo_CXE dans les zones saines
*
        DO IVGA = 1, mo_NGG
          xmo_DERG(IVGA) = 
     &        xmo_XE_NUCLHOMO(IVGA)
     &      + xmo_XE_PIEGBUL(IVGA)
     &      + xmo_XE_PIEGPOR(IVGA)
     &      + xmo_XE_CREATION(IVGA)
     &      + xmo_XE_REMJOINT(IVGA)
     &      + xmo_XE_REMBUL(IVGA)
     &      + xmo_XE_REMPOR(IVGA)
     &      + xmo_XE_DIFFUSION(IVGA)
     &      + xmo_XE_CLIM(IVGA)
     &      + xmo_XE_SORTIEGRAIN(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration  en gaz dissous au voisinage des bulles
        DO IVGA = 1, mo_NGG
          xmo_DERG(IVGA + mo_NGG) = 
     &        xmo_XERBLOC_EVOL(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration de bulles xmo_CB dans les zones
* saines
*
        xmo_DERG(2*mo_NGG+1) =
     &        xmo_B_NUCLHOMO
     &      + xmo_B_COALEDB
     &      + xmo_B_COALEVB
     &      + xmo_B_COALEGEOM
     &      + xmo_B_PIEGBPOR
     &      + xmo_B_REMBUL
     &      + xmo_B_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration du gaz de bulles xmo_CXEB
* dans les zones saines
*
        xmo_DERG(2*mo_NGG+2) =
     &        xmo_XEB_NUCLHOMO
     &      + xmo_XEB_PIEGBUL
     &      + xmo_XEB_PIEGBPOR
     &      + xmo_XEB_REMBUL
     &      + xmo_XEB_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle du volume total de bulles xmo_VTB dans les zones saines
*
        xmo_DERG(2*mo_NGG+3) =
     &        xmo_VTB_LACUNE
     &      + xmo_VTB_GAZ
     &      + xmo_VTB_PIEGBPOR
     &      + xmo_VTB_REMBUL
     &      + xmo_VTB_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz inter dissous
*
        DO IVGA = 1, mo_NSEQ_C
          xmo_DERG(2*mo_NGG+3+IVGA) =
     &         xmo_XEJ_NUCLHOMO(IVGA)
     &       + xmo_XEJ_PIEGBUL(IVGA) 
     &       + xmo_XEJ_PIEGPOR(IVGA)
     &       + xmo_XEJ_REMJOINT(IVGA)
     &       + xmo_XEJ_SOURCE(IVGA) * xmo_TEST0
     &       + xmo_XEJ_DIFFUSION(IVGA)
     &       + xmo_XEJ_CLIM3(IVGA)
     &       + xmo_XEJ_SORTIEJ(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles inter xmo_CBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+1) = 
     &        xmo_BJ_NUCLHOMO
     &      + xmo_BJ_APPORT * xmo_TEST0 * xmo_TEST8
     &      + xmo_BJ_COALEDBJ
     &      + xmo_BJ_COALEVBJ
     &      + xmo_BJ_COALEGEOMJ
     &      + xmo_BJ_PIEGBPOR
     &      + xmo_BJ_REMBUL
     &      + xmo_BJ_SORTIEJ
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de bulles inter xmo_CXEBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+2) = 
     &        xmo_XEBJ_NUCLHOMO
     &      + xmo_XEBJ_APPORT * xmo_TEST0 * xmo_TEST8
     &      + xmo_XEBJ_PIEGBUL
     &      + xmo_XEBJ_PIEGBPOR
     &      + xmo_XEBJ_REMBUL
     &      + xmo_XEBJ_SORTIEJ
*essaiPERC2
     &      + xmo_XEBJ_PERCBPOR
     &      + xmo_XEBJ_PERCPLEN
*essaiPERC2
*
************************************************************************
* d�riv�e temporelle du volume total de bulles inter xmo_VTBJ
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3) =
     &        xmo_VTBJ_LACUNE
     &      + xmo_VTBJ_GAZ 
     &      + xmo_VTBJ_PIEGBPOR
     &      + xmo_VTBJ_REMBUL
     &      + xmo_VTBJ_APPORT * xmo_TEST0 * xmo_TEST8
     &      + xmo_VTBJ_SORTIEJ 
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz dissous dans le milieu 
* restructur�, consid�r� comme homog�ne xmo_CXEHOM
*
        DO IVGA = 1, mo_NSEQR
          xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3+IVGA) =
     &         xmo_XEHOM_CREATION(IVGA)
     &       + xmo_XEHOM_DIFFUSION(IVGA)
     &       + xmo_XEHOM_CLIM3(IVGA)
     &       + xmo_XEHOM_APPORT(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles de rim xmo_CBRIM
* dans le milieu restructur�
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3+mo_NSEQR+1) = xmo_BRIM_APPORT
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de bulles de rim xmo_CXEBRIM
* dans le milieu restructur�
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3+mo_NSEQR+2) =
     &        xmo_XEBRIM_APPORT
     &      + xmo_XEBRIM_ALIM_DIRECTE
     &      + xmo_XEBRIM_ALIM_ECHAP
     &      + xmo_XEBRIM_REMBRIM
*
************************************************************************
* d�riv�e temporelle du volume total de bulles de rim xmo_VTBRIM
* dans le milieu restructur�
*
        xmo_DERG(2*mo_NGG+3+mo_NSEQ_C+3+mo_NSEQR+3) =
     &        xmo_VTBRIM_APPORT 
     &      + xmo_VTBRIM_LACUNE
     &      + xmo_VTBRIM_GAZ
*
*
*DEBUT TABLEAU DEFAUT***************************************************
* d�riv�e temporelle de la concentration en pores xmo_CPOR dans les zones
* saines
*
        xmo_DERD(1) =
     &        xmo_POR_COALEDPOR
     &      + xmo_POR_COALEVPOR
     &      + xmo_POR_REMPOR
     &      + xmo_POR_SORTIEGRAIN 
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores xmo_CXEPOR dans les zones saines
*
        xmo_DERD(2) =
     &        xmo_XEPOR_PIEGPOR
     &      + xmo_XEPOR_PIEGBPOR
     &      + xmo_XEPOR_REMPOR
     &      + xmo_XEPOR_SORTIEGRAIN  
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores xmo_VTPOR dans les zones
* saines
*
*
        xmo_DERD(3) =
     &        xmo_VTPOR_LACUNE
     &      + xmo_VTPOR_GAZ
     &      + xmo_VTPOR_PIEGBPOR
     &      + xmo_VTPOR_REMPOR
     &      + xmo_VTPOR_SORTIEGRAIN 
*
*
************************************************************************
* d�riv�e temporelle de la concentration en pores inter xmo_CPORJ
*
        xmo_DERD(4) = xmo_PORJ_SORTIEJ
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores inter xmo_CXEPORJ
*
        xmo_DERD(5) = 
     &        xmo_XEPORJ_PIEGPOR
     &      + xmo_XEPORJ_PIEGBPOR
     &      + xmo_XEPORJ_REMPOR
     &      + xmo_XEPORJ_APPORT * xmo_TEST0 
     &      + xmo_XEPORJ_SORTIEJ
*essaiPERC2
     &      + xmo_XEPORJ_PERCBPOR
*essaiPERC2
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores inter xmo_VTPORJ
*
        xmo_DERD(6) =
     &        xmo_VTPORJ_LACUNE
     &      + xmo_VTPORJ_GAZ
     &      + xmo_VTPORJ_PIEGBPOR
     &      + xmo_VTPORJ_APPORT * xmo_TEST0 
     &      + xmo_VTPORJ_SORTIEJ
*  
*
*
************************************************************************
* d�riv�e temporelle de la concentration en pores de rim xmo_CPORRIM
*
        xmo_DERD(7) = xmo_PORRIM_APPORT
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores de rim xmo_CXEPORRIM
*
        xmo_DERD(8) = 
     &        xmo_XEPORRIM_ALIM_DIRECTE
     &      + xmo_XEPORRIM_ALIM_ECHAP
     &      + xmo_XEPORRIM_REMPORRIM
     &      + xmo_XEPORRIM_APPORT
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores de rim xmo_VTPORRIM
*
        xmo_DERD(9) =
     &        xmo_VTPORRIM_LACUNE
     &      + xmo_VTPORRIM_GAZ
     &      + xmo_VTPORRIM_APPORT
*  
************************************************************************
* d�riv�e temporelle de la densit� de dislocation : 
*
        xmo_DERD(10) = xmo_FACRHO * xmo_RHOD
*
************************************************************************
************************************************************************
      ENDIF
************************************************************************
* FIN ETAT 'EN RESTRUCTURATION' (2)
************************************************************************
*
************************************************************************
* ETAT 'RESTRUCTURE' (3)
************************************************************************
* On trouve dans le tableau 'gaz', dans l'ordre :
* - gaz dissous zones restructur�es
* - bulles nanom�triques (nombre gaz et volume) de zones restructur�es
* - bulles de rim (nombre gaz et volume) de zones restructur�es
*
* On trouve dans le tableau 'd�faut', dans l'ordre :
* - pores de rim (nombre gaz et volume) de zones restructur�es
************************************************************************
      IF (mo_IET .EQ. 3) THEN
*
* Tranfert dans les tableaux 'gaz' et 'd�fauts'
*
        DO I = 1,mo_NVG
          xmo_VG1(I) = xmo_YEXP(I)
        ENDDO
*
        DO I = 1,3
          xmo_VD1(I) = xmo_YEXP(I+mo_NVG)
        ENDDO
*
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL3(xmo_VG1,xmo_VD1,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM)
*
*
* Calcul des termes des d�riv�es des variables des zones resructur�es
* 
*
      CALL MOTRM3(
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_CREATION,xmo_XEHOM_DIFFUSION,
     &      xmo_XEHOM_CLIM3,
     &      
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ,
     &      
     &      xmo_CXEHOMM,xmo_OUTSE,xmo_OUTEJEC_RIM )
*
*
* CALCUL DES DERIVEES TEMPORELLES***************************************
************************************************************************
* d�riv�e temporelle de la concentration en gaz dissous dans le milieu 
* restructur�, consid�r� comme homog�ne xmo_CXEHOM
*
        DO IVGA = 1, mo_NSEQR
          xmo_DERG(IVGA) =
     &         xmo_XEHOM_CREATION(IVGA)
     &       + xmo_XEHOM_DIFFUSION(IVGA)
     &       + xmo_XEHOM_CLIM3(IVGA)
        ENDDO
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles de rim xmo_CBRIM
* dans le milieu restructur�
*
        xmo_DERG(mo_NSEQR+1) = 0.D0
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de bulles de rim xmo_CXEBRIM
* dans le milieu restructur�
*
        xmo_DERG(mo_NSEQR+2) =
     &        xmo_XEBRIM_ALIM_DIRECTE
     &      + xmo_XEBRIM_ALIM_ECHAP
     &      + xmo_XEBRIM_REMBRIM
*
************************************************************************
* d�riv�e temporelle du volume total de bulles de rim xmo_VTBRIM
* dans le milieu restructur�
*
        xmo_DERG(mo_NSEQR+3) =
     &        xmo_VTBRIM_LACUNE
     &      + xmo_VTBRIM_GAZ
*
*
*
*
*DEBUT TABLEAU DEFAUT***************************************************
************************************************************************
* d�riv�e temporelle de la concentration en pores de rim xmo_CPORRIM
*
        xmo_DERD(1) = 0.D0
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores de rim xmo_CXEPORRIM
*
        xmo_DERD(2) = 
     &        xmo_XEPORRIM_ALIM_DIRECTE
     &      + xmo_XEPORRIM_ALIM_ECHAP
     &      + xmo_XEPORRIM_REMPORRIM
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores de rim xmo_VTPORRIM
*
        xmo_DERD(3) =
     &        xmo_VTPORRIM_LACUNE
     &      + xmo_VTPORRIM_GAZ
*  
*
************************************************************************
      ENDIF
************************************************************************
* FIN ETAT 'RESTRUCTURE' (3)
************************************************************************
*
*
      DO I = 1,mo_NEQG
        YDOT(I) = xmo_DERG(mo_IGTAB(I))
      ENDDO
      DO I = 1,mo_NEQD
        YDOT(I+mo_NEQG) = xmo_DERD(mo_IDTAB(I))
      ENDDO
*
CDEBUG
CDEBUG        WRITE(20,*) 'fin mofder_gear mo_NEQG = ',mo_NEQG
CDEBUG        WRITE(20,*) 'fin mofder_gear Y(1+mo_NEQG) = ',
CDEBUG     &               Y(1+mo_NEQG)
CDEBUG
*
      RETURN
      END
      double precision function bnorm (n, a, nra, ml, mu, w)
clll. optimize
c-----------------------------------------------------------------------
c this function computes the norm of a banded n by n matrix,
c stored in the array a, that is consistent with the weighted max-norm
c on vectors, with weights stored in the array w.
c ml and mu are the lower and upper half-bandwidths of the matrix.
c nra is the first dimension of the a array, nra .ge. ml+mu+1.
c in terms of the matrix elements a(i,j), the norm is given by..
c   bnorm = max(i=1,...,n) ( w(i) * sum(j=1,...,n) abs(a(i,j))/w(j) )
c-----------------------------------------------------------------------
      integer n, nra, ml, mu
      integer i, i1, jlo, jhi, j
      double precision a, w
      double precision an, sum
      dimension a(nra,n), w(n)
      an = 0.0d0
      do 20 i = 1,n
        sum = 0.0d0
        i1 = i + mu + 1
        jlo = max0(i-ml,1)
        jhi = min0(i+mu,n)
        do 10 j = jlo,jhi
 10       sum = sum + dabs(a(i1-j,j))/w(j)
        an = dmax1(an,sum*w(i))
 20     continue
      bnorm = an
      return
c----------------------- end of function bnorm -------------------------
      end
      subroutine cfode (meth, elco, tesco)
clll. optimize
      integer meth
      integer i, ib, nq, nqm1, nqp1
      double precision elco, tesco
      double precision agamq, fnq, fnqm1, pc, pint, ragq,
     1   rqfac, rq1fac, tsign, xpin
      dimension elco(13,12), tesco(3,12)
c-----------------------------------------------------------------------
c cfode is called by the integrator routine to set coefficients
c needed there.  the coefficients for the current method, as
c given by the value of meth, are set for all orders and saved.
c the maximum order assumed here is 12 if meth = 1 and 5 if meth = 2.
c (a smaller value of the maximum order is also allowed.)
c cfode is called once at the beginning of the problem,
c and is not called again unless and until meth is changed.
c
c the elco array contains the basic method coefficients.
c the coefficients el(i), 1 .le. i .le. nq+1, for the method of
c order nq are stored in elco(i,nq).  they are given by a genetrating
c polynomial, i.e.,
c     l(x) = el(1) + el(2)*x + ... + el(nq+1)*x**nq.
c for the implicit adams methods, l(x) is given by
c     dl/dx = (x+1)*(x+2)*...*(x+nq-1)/factorial(nq-1),    l(-1) = 0.
c for the bdf methods, l(x) is given by
c     l(x) = (x+1)*(x+2)* ... *(x+nq)/k,
c where         k = factorial(nq)*(1 + 1/2 + ... + 1/nq).
c
c the tesco array contains test constants used for the
c local error test and the selection of step size and/or order.
c at order nq, tesco(k,nq) is used for the selection of step
c size at order nq - 1 if k = 1, at order nq if k = 2, and at order
c nq + 1 if k = 3.
c-----------------------------------------------------------------------
      dimension pc(12)
c
      go to (100, 200), meth
c
 100  elco(1,1) = 1.0d0
      elco(2,1) = 1.0d0
      tesco(1,1) = 0.0d0
      tesco(2,1) = 2.0d0
      tesco(1,2) = 1.0d0
      tesco(3,12) = 0.0d0
      pc(1) = 1.0d0
      rqfac = 1.0d0
      do 140 nq = 2,12
c-----------------------------------------------------------------------
c the pc array will contain the coefficients of the polynomial
c     p(x) = (x+1)*(x+2)*...*(x+nq-1).
c initially, p(x) = 1.
c-----------------------------------------------------------------------
        rq1fac = rqfac
        rqfac = rqfac/dfloat(nq)
        nqm1 = nq - 1
        fnqm1 = dfloat(nqm1)
        nqp1 = nq + 1
c form coefficients of p(x)*(x+nq-1). ----------------------------------
        pc(nq) = 0.0d0
        do 110 ib = 1,nqm1
          i = nqp1 - ib
 110      pc(i) = pc(i-1) + fnqm1*pc(i)
        pc(1) = fnqm1*pc(1)
c compute integral, -1 to 0, of p(x) and x*p(x). -----------------------
        pint = pc(1)
        xpin = pc(1)/2.0d0
        tsign = 1.0d0
        do 120 i = 2,nq
          tsign = -tsign
          pint = pint + tsign*pc(i)/dfloat(i)
 120      xpin = xpin + tsign*pc(i)/dfloat(i+1)
c store coefficients in elco and tesco. --------------------------------
        elco(1,nq) = pint*rq1fac
        elco(2,nq) = 1.0d0
        do 130 i = 2,nq
 130      elco(i+1,nq) = rq1fac*pc(i)/dfloat(i)
        agamq = rqfac*xpin
        ragq = 1.0d0/agamq
        tesco(2,nq) = ragq
        if (nq .lt. 12) tesco(1,nqp1) = ragq*rqfac/dfloat(nqp1)
        tesco(3,nqm1) = ragq
 140    continue
      return
c
 200  pc(1) = 1.0d0
      rq1fac = 1.0d0
      do 230 nq = 1,5
c-----------------------------------------------------------------------
c the pc array will contain the coefficients of the polynomial
c     p(x) = (x+1)*(x+2)*...*(x+nq).
c initially, p(x) = 1.
c-----------------------------------------------------------------------
        fnq = dfloat(nq)
        nqp1 = nq + 1
c form coefficients of p(x)*(x+nq). ------------------------------------
        pc(nqp1) = 0.0d0
        do 210 ib = 1,nq
          i = nq + 2 - ib
 210      pc(i) = pc(i-1) + fnq*pc(i)
        pc(1) = fnq*pc(1)
c store coefficients in elco and tesco. --------------------------------
        do 220 i = 1,nqp1
 220      elco(i,nq) = pc(i)/pc(2)
        elco(2,nq) = 1.0d0
        tesco(1,nq) = rq1fac
        tesco(2,nq) = dfloat(nqp1)/elco(1,nq)
        tesco(3,nq) = dfloat(nq+2)/elco(1,nq)
        rq1fac = rq1fac/fnq
 230    continue
      return
c----------------------- end of subroutine cfode -----------------------
      end



      DOUBLE PRECISION FUNCTION D1MACH(I)
      INTEGER I
C
C  DOUBLE-PRECISION MACHINE CONSTANTS
C  D1MACH( 1) = B**(EMIN-1), THE SMALLEST POSITIVE MAGNITUDE.
C  D1MACH( 2) = B**EMAX*(1 - B**(-T)), THE LARGEST MAGNITUDE.
C  D1MACH( 3) = B**(-T), THE SMALLEST RELATIVE SPACING.
C  D1MACH( 4) = B**(1-T), THE LARGEST RELATIVE SPACING.
C  D1MACH( 5) = LOG10(B)
C
      INTEGER SMALL(2)
      INTEGER LARGE(2)
      INTEGER RIGHT(2)
      INTEGER DIVER(2)
      INTEGER LOG10(2)
      INTEGER SC, CRAY1(38), J
      COMMON /D9MACH/ CRAY1
      SAVE SMALL, LARGE, RIGHT, DIVER, LOG10, SC
      DOUBLE PRECISION DMACH(5)
      EQUIVALENCE (DMACH(1),SMALL(1))
      EQUIVALENCE (DMACH(2),LARGE(1))
      EQUIVALENCE (DMACH(3),RIGHT(1))
      EQUIVALENCE (DMACH(4),DIVER(1))
      EQUIVALENCE (DMACH(5),LOG10(1))
C  THIS VERSION ADAPTS AUTOMATICALLY TO MOST CURRENT MACHINES.
C  R1MACH CAN HANDLE AUTO-DOUBLE COMPILING, BUT THIS VERSION OF
C  D1MACH DOES NOT, BECAUSE WE DO NOT HAVE QUAD CONSTANTS FOR
C  MANY MACHINES YET.
C  TO COMPILE ON OLDER MACHINES, ADD A C IN COLUMN 1
C  ON THE NEXT LINE
      DATA SC/0/
C  AND REMOVE THE C FROM COLUMN 1 IN ONE OF THE SECTIONS BELOW.
C  CONSTANTS FOR EVEN OLDER MACHINES CAN BE OBTAINED BY
C          mail netlib@research.bell-labs.com
C          send old1mach from blas
C  PLEASE SEND CORRECTIONS TO dmg OR ehg@bell-labs.com.
C
C     MACHINE CONSTANTS FOR THE HONEYWELL DPS 8/70 SERIES.
C      DATA SMALL(1),SMALL(2) / O402400000000, O000000000000 /
C      DATA LARGE(1),LARGE(2) / O376777777777, O777777777777 /
C      DATA RIGHT(1),RIGHT(2) / O604400000000, O000000000000 /
C      DATA DIVER(1),DIVER(2) / O606400000000, O000000000000 /
C      DATA LOG10(1),LOG10(2) / O776464202324, O117571775714 /, SC/987/
C
C     MACHINE CONSTANTS FOR PDP-11 FORTRANS SUPPORTING
C     32-BIT INTEGERS.
C      DATA SMALL(1),SMALL(2) /    8388608,           0 /
C      DATA LARGE(1),LARGE(2) / 2147483647,          -1 /
C      DATA RIGHT(1),RIGHT(2) /  612368384,           0 /
C      DATA DIVER(1),DIVER(2) /  620756992,           0 /
C      DATA LOG10(1),LOG10(2) / 1067065498, -2063872008 /, SC/987/
C
C     MACHINE CONSTANTS FOR THE UNIVAC 1100 SERIES.
C      DATA SMALL(1),SMALL(2) / O000040000000, O000000000000 /
C      DATA LARGE(1),LARGE(2) / O377777777777, O777777777777 /
C      DATA RIGHT(1),RIGHT(2) / O170540000000, O000000000000 /
C      DATA DIVER(1),DIVER(2) / O170640000000, O000000000000 /
C      DATA LOG10(1),LOG10(2) / O177746420232, O411757177572 /, SC/987/
C
C     ON FIRST CALL, IF NO DATA UNCOMMENTED, TEST MACHINE TYPES.
      IF (SC .NE. 987) THEN
         DMACH(1) = 1.D13
         IF (      SMALL(1) .EQ. 1117925532
     *       .AND. SMALL(2) .EQ. -448790528) THEN
*           *** IEEE BIG ENDIAN ***
            SMALL(1) = 1048576
            SMALL(2) = 0
            LARGE(1) = 2146435071
            LARGE(2) = -1
            RIGHT(1) = 1017118720
            RIGHT(2) = 0
            DIVER(1) = 1018167296
            DIVER(2) = 0
            LOG10(1) = 1070810131
            LOG10(2) = 1352628735
         ELSE IF ( SMALL(2) .EQ. 1117925532
     *       .AND. SMALL(1) .EQ. -448790528) THEN
*           *** IEEE LITTLE ENDIAN ***
            SMALL(2) = 1048576
            SMALL(1) = 0
            LARGE(2) = 2146435071
            LARGE(1) = -1
            RIGHT(2) = 1017118720
            RIGHT(1) = 0
            DIVER(2) = 1018167296
            DIVER(1) = 0
            LOG10(2) = 1070810131
            LOG10(1) = 1352628735
         ELSE IF ( SMALL(1) .EQ. -2065213935
     *       .AND. SMALL(2) .EQ. 10752) THEN
*               *** VAX WITH D_FLOATING ***
            SMALL(1) = 128
            SMALL(2) = 0
            LARGE(1) = -32769
            LARGE(2) = -1
            RIGHT(1) = 9344
            RIGHT(2) = 0
            DIVER(1) = 9472
            DIVER(2) = 0
            LOG10(1) = 546979738
            LOG10(2) = -805796613
         ELSE IF ( SMALL(1) .EQ. 1267827943
     *       .AND. SMALL(2) .EQ. 704643072) THEN
*               *** IBM MAINFRAME ***
            SMALL(1) = 1048576
            SMALL(2) = 0
            LARGE(1) = 2147483647
            LARGE(2) = -1
            RIGHT(1) = 856686592
            RIGHT(2) = 0
            DIVER(1) = 873463808
            DIVER(2) = 0
            LOG10(1) = 1091781651
            LOG10(2) = 1352628735
         ELSE IF ( SMALL(1) .EQ. 1120022684
     *       .AND. SMALL(2) .EQ. -448790528) THEN
*           *** CONVEX C-1 ***
            SMALL(1) = 1048576
            SMALL(2) = 0
            LARGE(1) = 2147483647
            LARGE(2) = -1
            RIGHT(1) = 1019215872
            RIGHT(2) = 0
            DIVER(1) = 1020264448
            DIVER(2) = 0
            LOG10(1) = 1072907283
            LOG10(2) = 1352628735
         ELSE IF ( SMALL(1) .EQ. 815547074
     *       .AND. SMALL(2) .EQ. 58688) THEN
*           *** VAX G-FLOATING ***
            SMALL(1) = 16
            SMALL(2) = 0
            LARGE(1) = -32769
            LARGE(2) = -1
            RIGHT(1) = 15552
            RIGHT(2) = 0
            DIVER(1) = 15568
            DIVER(2) = 0
            LOG10(1) = 1142112243
            LOG10(2) = 2046775455
         ELSE
            DMACH(2) = 1.D27 + 1
            DMACH(3) = 1.D27
            LARGE(2) = LARGE(2) - RIGHT(2)
            IF (LARGE(2) .EQ. 64 .AND. SMALL(2) .EQ. 0) THEN
               CRAY1(1) = 67291416
               DO 10 J = 1, 20
                  CRAY1(J+1) = CRAY1(J) + CRAY1(J)
 10               CONTINUE
               CRAY1(22) = CRAY1(21) + 321322
               DO 20 J = 22, 37
                  CRAY1(J+1) = CRAY1(J) + CRAY1(J)
 20               CONTINUE
               IF (CRAY1(38) .EQ. SMALL(1)) THEN
*                  *** CRAY ***
                  CALL I1MCRY(SMALL(1), J, 8285, 8388608, 0)
                  SMALL(2) = 0
                  CALL I1MCRY(LARGE(1), J, 24574, 16777215, 16777215)
                  CALL I1MCRY(LARGE(2), J, 0, 16777215, 16777214)
                  CALL I1MCRY(RIGHT(1), J, 16291, 8388608, 0)
                  RIGHT(2) = 0
                  CALL I1MCRY(DIVER(1), J, 16292, 8388608, 0)
                  DIVER(2) = 0
                  CALL I1MCRY(LOG10(1), J, 16383, 10100890, 8715215)
                  CALL I1MCRY(LOG10(2), J, 0, 16226447, 9001388)
               ELSE
                  WRITE(*,9000)
                  STOP 779
                  END IF
            ELSE
               WRITE(*,9000)
               STOP 779
               END IF
            END IF
         SC = 987
         END IF
*    SANITY CHECK
      IF (DMACH(4) .GE. 1.0D0) STOP 778
      IF (I .LT. 1 .OR. I .GT. 5) THEN
         WRITE(*,*) 'D1MACH(I): I =',I,' is out of bounds.'
         STOP
         END IF
      D1MACH = DMACH(I)
      RETURN
 9000 FORMAT(/' Adjust D1MACH by uncommenting data statements'/
     *' appropriate for your machine.')
* /* Standard C source for D1MACH -- remove the * in column 1 */
*#include <stdio.h>
*#include <float.h>
*#include <math.h>
*double d1mach_(long *i)
*{
*	switch(*i){
*	  case 1: return DBL_MIN;
*	  case 2: return DBL_MAX;
*	  case 3: return DBL_EPSILON/FLT_RADIX;
*	  case 4: return DBL_EPSILON;
*	  case 5: return log10(FLT_RADIX);
*	  }
*	fprintf(stderr, "invalid argument: d1mach(%ld)\n", *i);
*	exit(1); return 0; /* some compilers demand return values */
*}
      END
      SUBROUTINE I1MCRY(A, A1, B, C, D)
**** SPECIAL COMPUTATION FOR OLD CRAY MACHINES ****
      INTEGER A, A1, B, C, D
      A1 = 16777216*B + C
      A = 16777216*A1 + D
      END
      subroutine daxpy(n,da,dx,incx,dy,incy)
c
c     constant times a vector plus a vector.
c     uses unrolled loops for increments equal to one.
c     jack dongarra, linpack, 3/11/78.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision dx(*),dy(*),da
      integer i,incx,incy,ix,iy,m,mp1,n
c
      if(n.le.0)return
      if (da .eq. 0.0d0) return
      if(incx.eq.1.and.incy.eq.1)go to 20
c
c        code for unequal increments or equal increments
c          not equal to 1
c
      ix = 1
      iy = 1
      if(incx.lt.0)ix = (-n+1)*incx + 1
      if(incy.lt.0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dy(iy) = dy(iy) + da*dx(ix)
        ix = ix + incx
        iy = iy + incy
   10 continue
      return
c
c        code for both increments equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,4)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dy(i) = dy(i) + da*dx(i)
   30 continue
      if( n .lt. 4 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,4
        dy(i) = dy(i) + da*dx(i)
        dy(i + 1) = dy(i + 1) + da*dx(i + 1)
        dy(i + 2) = dy(i + 2) + da*dx(i + 2)
        dy(i + 3) = dy(i + 3) + da*dx(i + 3)
   50 continue
      return
      end
      double precision function ddot(n,dx,incx,dy,incy)
c
c     forms the dot product of two vectors.
c     uses unrolled loops for increments equal to one.
c     jack dongarra, linpack, 3/11/78.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision dx(*),dy(*),dtemp
      integer i,incx,incy,ix,iy,m,mp1,n
c
      ddot = 0.0d0
      dtemp = 0.0d0
      if(n.le.0)return
      if(incx.eq.1.and.incy.eq.1)go to 20
c
c        code for unequal increments or equal increments
c          not equal to 1
c
      ix = 1
      iy = 1
      if(incx.lt.0)ix = (-n+1)*incx + 1
      if(incy.lt.0)iy = (-n+1)*incy + 1
      do 10 i = 1,n
        dtemp = dtemp + dx(ix)*dy(iy)
        ix = ix + incx
        iy = iy + incy
   10 continue
      ddot = dtemp
      return
c
c        code for both increments equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,5)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dtemp = dtemp + dx(i)*dy(i)
   30 continue
      if( n .lt. 5 ) go to 60
   40 mp1 = m + 1
      do 50 i = mp1,n,5
        dtemp = dtemp + dx(i)*dy(i) + dx(i + 1)*dy(i + 1) +
     *   dx(i + 2)*dy(i + 2) + dx(i + 3)*dy(i + 3) + dx(i + 4)*dy(i + 4)
   50 continue
   60 ddot = dtemp
      return
      end
      subroutine dgbfa(abd,lda,n,ml,mu,ipvt,info)
      integer lda,n,ml,mu,ipvt(1),info
      double precision abd(lda,1)
c
c     dgbfa factors a double precision band matrix by elimination.
c
c     dgbfa is usually called by dgbco, but it can be called
c     directly with a saving in time if  rcond  is not needed.
c
c     on entry
c
c        abd     double precision(lda, n)
c                contains the matrix in band storage.  the columns
c                of the matrix are stored in the columns of  abd  and
c                the diagonals of the matrix are stored in rows
c                ml+1 through 2*ml+mu+1 of  abd .
c                see the comments below for details.
c
c        lda     integer
c                the leading dimension of the array  abd .
c                lda must be .ge. 2*ml + mu + 1 .
c
c        n       integer
c                the order of the original matrix.
c
c        ml      integer
c                number of diagonals below the main diagonal.
c                0 .le. ml .lt. n .
c
c        mu      integer
c                number of diagonals above the main diagonal.
c                0 .le. mu .lt. n .
c                more efficient if  ml .le. mu .
c     on return
c
c        abd     an upper triangular matrix in band storage and
c                the multipliers which were used to obtain it.
c                the factorization can be written  a = l*u  where
c                l  is a product of permutation and unit lower
c                triangular matrices and  u  is upper triangular.
c
c        ipvt    integer(n)
c                an integer vector of pivot indices.
c
c        info    integer
c                = 0  normal value.
c                = k  if  u(k,k) .eq. 0.0 .  this is not an error
c                     condition for this subroutine, but it does
c                     indicate that dgbsl will divide by zero if
c                     called.  use  rcond  in dgbco for a reliable
c                     indication of singularity.
c
c     band storage
c
c           if  a  is a band matrix, the following program segment
c           will set up the input.
c
c                   ml = (band width below the diagonal)
c                   mu = (band width above the diagonal)
c                   m = ml + mu + 1
c                   do 20 j = 1, n
c                      i1 = max0(1, j-mu)
c                      i2 = min0(n, j+ml)
c                      do 10 i = i1, i2
c                         k = i - j + m
c                         abd(k,j) = a(i,j)
c                10    continue
c                20 continue
c
c           this uses rows  ml+1  through  2*ml+mu+1  of  abd .
c           in addition, the first  ml  rows in  abd  are used for
c           elements generated during the triangularization.
c           the total number of rows needed in  abd  is  2*ml+mu+1 .
c           the  ml+mu by ml+mu  upper left triangle and the
c           ml by ml  lower right triangle are not referenced.
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas daxpy,dscal,idamax
c     fortran max0,min0
c
c     internal variables
c
      double precision t
      integer i,idamax,i0,j,ju,jz,j0,j1,k,kp1,l,lm,m,mm,nm1
c
c
      m = ml + mu + 1
      info = 0
c
c     zero initial fill-in columns
c
      j0 = mu + 2
      j1 = min0(n,m) - 1
      if (j1 .lt. j0) go to 30
      do 20 jz = j0, j1
         i0 = m + 1 - jz
         do 10 i = i0, ml
            abd(i,jz) = 0.0d0
   10    continue
   20 continue
   30 continue
      jz = j1
      ju = 0
c
c     gaussian elimination with partial pivoting
c
      nm1 = n - 1
      if (nm1 .lt. 1) go to 130
      do 120 k = 1, nm1
         kp1 = k + 1
c
c        zero next fill-in column
c
         jz = jz + 1
         if (jz .gt. n) go to 50
         if (ml .lt. 1) go to 50
            do 40 i = 1, ml
               abd(i,jz) = 0.0d0
   40       continue
   50    continue
c
c        find l = pivot index
c
         lm = min0(ml,n-k)
c         l = idamax(lm+1,abd(m,k),1) + m - 1
         l=m
         ipvt(k) = l + k - m
c
c        zero pivot implies this column already triangularized
c
         if (abd(l,k) .eq. 0.0d0) go to 100
c
c           interchange if necessary
c
            if (l .eq. m) go to 60
               t = abd(l,k)
               abd(l,k) = abd(m,k)
               abd(m,k) = t
   60       continue
c
c           compute multipliers
c
            t = -1.0d0/abd(m,k)
            call dscal(lm,t,abd(m+1,k),1)
c
c           row elimination with column indexing
c
            ju = min0(max0(ju,mu+ipvt(k)),n)
            mm = m
            if (ju .lt. kp1) go to 90
            do 80 j = kp1, ju
               l = l - 1
               mm = mm - 1
               t = abd(l,j)
               if (l .eq. mm) go to 70
                  abd(l,j) = abd(mm,j)
                  abd(mm,j) = t
   70          continue
               call daxpy(lm,t,abd(m+1,k),1,abd(mm+1,j),1)
   80       continue
   90       continue
         go to 110
  100    continue
            info = k
  110    continue
  120 continue
  130 continue
      ipvt(n) = n
      if (abd(m,n) .eq. 0.0d0) info = n
      return
      end
      subroutine dgbsl(abd,lda,n,ml,mu,ipvt,b,job)
      integer lda,n,ml,mu,ipvt(1),job
      double precision abd(lda,1),b(1)
c
c     dgbsl solves the double precision band system
c     a * x = b  or  trans(a) * x = b
c     using the factors computed by dgbco or dgbfa.
c
c     on entry
c
c        abd     double precision(lda, n)
c                the output from dgbco or dgbfa.
c
c        lda     integer
c                the leading dimension of the array  abd .
c
c        n       integer
c                the order of the original matrix.
c
c        ml      integer
c                number of diagonals below the main diagonal.
c
c        mu      integer
c                number of diagonals above the main diagonal.
c
c        ipvt    integer(n)
c                the pivot vector from dgbco or dgbfa.
c
c        b       double precision(n)
c                the right hand side vector.
c
c        job     integer
c                = 0         to solve  a*x = b ,
c                = nonzero   to solve  trans(a)*x = b , where
c                            trans(a)  is the transpose.
c
c     on return
c
c        b       the solution vector  x .
c
c     error condition
c
c        a division by zero will occur if the input factor contains a
c        zero on the diagonal.  technically this indicates singularity
c        but it is often caused by improper arguments or improper
c        setting of lda .  it will not occur if the subroutines are
c        called correctly and if dgbco has set rcond .gt. 0.0
c        or dgbfa has set info .eq. 0 .
c
c     to compute  inverse(a) * c  where  c  is a matrix
c     with  p  columns
c           call dgbco(abd,lda,n,ml,mu,ipvt,rcond,z)
c           if (rcond is too small) go to ...
c           do 10 j = 1, p
c              call dgbsl(abd,lda,n,ml,mu,ipvt,c(1,j),0)
c        10 continue
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas daxpy,ddot
c     fortran min0
c
c     internal variables
c
      double precision ddot,t
      integer k,kb,l,la,lb,lm,m,nm1
c
      m = mu + ml + 1
      nm1 = n - 1
      if (job .ne. 0) go to 50
c
c        job = 0 , solve  a * x = b
c        first solve l*y = b
c
         if (ml .eq. 0) go to 30
         if (nm1 .lt. 1) go to 30
            do 20 k = 1, nm1
               lm = min0(ml,n-k)
               l = ipvt(k)
               t = b(l)
               if (l .eq. k) go to 10
                  b(l) = b(k)
                  b(k) = t
   10          continue
               call daxpy(lm,t,abd(m+1,k),1,b(k+1),1)
   20       continue
   30    continue
c
c        now solve  u*x = y
c
         do 40 kb = 1, n
            k = n + 1 - kb
            b(k) = b(k)/abd(m,k)
            lm = min0(k,m) - 1
            la = m - lm
            lb = k - lm
            t = -b(k)
            call daxpy(lm,t,abd(la,k),1,b(lb),1)
   40    continue
      go to 100
   50 continue
c
c        job = nonzero, solve  trans(a) * x = b
c        first solve  trans(u)*y = b
c
         do 60 k = 1, n
            lm = min0(k,m) - 1
            la = m - lm
            lb = k - lm
            t = ddot(lm,abd(la,k),1,b(lb),1)
            b(k) = (b(k) - t)/abd(m,k)
   60    continue
c
c        now solve trans(l)*x = y
c
         if (ml .eq. 0) go to 90
         if (nm1 .lt. 1) go to 90
            do 80 kb = 1, nm1
               k = n - kb
               lm = min0(ml,n-k)
               b(k) = b(k) + ddot(lm,abd(m+1,k),1,b(k+1),1)
               l = ipvt(k)
               if (l .eq. k) go to 70
                  t = b(l)
                  b(l) = b(k)
                  b(k) = t
   70          continue
   80       continue
   90    continue
  100 continue
      return
      end


*DECK DGEFA
      SUBROUTINE DGEFA (A, LDA, N, IPVT, INFO)
C***BEGIN PROLOGUE  DGEFA
C***PURPOSE  Factor a matrix using Gaussian elimination.
C***LIBRARY   SLATEC (LINPACK)
C***CATEGORY  D2A1
C***TYPE      DOUBLE PRECISION (SGEFA-S, DGEFA-D, CGEFA-C)
C***KEYWORDS  GENERAL MATRIX, LINEAR ALGEBRA, LINPACK,
C             MATRIX FACTORIZATION
C***AUTHOR  Moler, C. B., (U. of New Mexico)
C***DESCRIPTION
C
C     DGEFA factors a double precision matrix by Gaussian elimination.
C
C     DGEFA is usually called by DGECO, but it can be called
C     directly with a saving in time if  RCOND  is not needed.
C     (Time for DGECO) = (1 + 9/N)*(Time for DGEFA) .
C
C     On Entry
C
C        A       DOUBLE PRECISION(LDA, N)
C                the matrix to be factored.
C
C        LDA     INTEGER
C                the leading dimension of the array  A .
C
C        N       INTEGER
C                the order of the matrix  A .
C
C     On Return
C
C        A       an upper triangular matrix and the multipliers
C                which were used to obtain it.
C                The factorization can be written  A = L*U  where
C                L  is a product of permutation and unit lower
C                triangular matrices and  U  is upper triangular.
C
C        IPVT    INTEGER(N)
C                an integer vector of pivot indices.
C
C        INFO    INTEGER
C                = 0  normal value.
C                = K  if  U(K,K) .EQ. 0.0 .  This is not an error
C                     condition for this subroutine, but it does
C                     indicate that DGESL or DGEDI will divide by zero
C                     if called.  Use  RCOND  in DGECO for a reliable
C                     indication of singularity.
C
C***REFERENCES  J. J. Dongarra, J. R. Bunch, C. B. Moler, and G. W.
C                 Stewart, LINPACK Users' Guide, SIAM, 1979.
C***ROUTINES CALLED  DAXPY, DSCAL, IDAMAX
C***REVISION HISTORY  (YYMMDD)
C   780814  DATE WRITTEN
C   890831  Modified array declarations.  (WRB)
C   890831  REVISION DATE from Version 3.2
C   891214  Prologue converted to Version 4.0 format.  (BAB)
C   900326  Removed duplicate information from DESCRIPTION section.
C           (WRB)
C   920501  Reformatted the REFERENCES section.  (WRB)
C***END PROLOGUE  DGEFA
      INTEGER LDA,N,IPVT(*),INFO
      DOUBLE PRECISION A(LDA,*)
C
      DOUBLE PRECISION T
      INTEGER IDAMAX,J,K,KP1,L,NM1
C
C     GAUSSIAN ELIMINATION WITH PARTIAL PIVOTING
C
C***FIRST EXECUTABLE STATEMENT  DGEFA
      INFO = 0
      NM1 = N - 1
      IF (NM1 .LT. 1) GO TO 70
      DO 60 K = 1, NM1
         KP1 = K + 1
C
C        FIND L = PIVOT INDEX
C
c         L = IDAMAX(N-K+1,A(K,K),1) + K - 1
         L=K
         IPVT(K) = L
C
C        ZERO PIVOT IMPLIES THIS COLUMN ALREADY TRIANGULARIZED
C
         IF (A(L,K) .EQ. 0.0D0) GO TO 40
C
C           INTERCHANGE IF NECESSARY
C
            IF (L .EQ. K) GO TO 10
               T = A(L,K)
               A(L,K) = A(K,K)
               A(K,K) = T
   10       CONTINUE
C
C           COMPUTE MULTIPLIERS
C
            T = -1.0D0/A(K,K)
            CALL DSCAL(N-K,T,A(K+1,K),1)
C
C           ROW ELIMINATION WITH COLUMN INDEXING
C
            DO 30 J = KP1, N
               T = A(L,J)
               IF (L .EQ. K) GO TO 20
                  A(L,J) = A(K,J)
                  A(K,J) = T
   20          CONTINUE
               CALL DAXPY(N-K,T,A(K+1,K),1,A(K+1,J),1)
   30       CONTINUE
         GO TO 50
   40    CONTINUE
            INFO = K
   50    CONTINUE
   60 CONTINUE
   70 CONTINUE
      IPVT(N) = N
      IF (A(N,N) .EQ. 0.0D0) INFO = N
      RETURN
      END
      subroutine dgesl(a,lda,n,ipvt,b,job)
      integer lda,n,ipvt(*),job
      double precision a(lda,*),b(*)
c
c     dgesl solves the double precision system
c     a * x = b  or  trans(a) * x = b
c     using the factors computed by dgeco or dgefa.
c
c     on entry
c
c        a       double precision(lda, n)
c                the output from dgeco or dgefa.
c
c        lda     integer
c                the leading dimension of the array  a .
c
c        n       integer
c                the order of the matrix  a .
c
c        ipvt    integer(n)
c                the pivot vector from dgeco or dgefa.
c
c        b       double precision(n)
c                the right hand side vector.
c
c        job     integer
c                = 0         to solve  a*x = b ,
c                = nonzero   to solve  trans(a)*x = b  where
c                            trans(a)  is the transpose.
c
c     on return
c
c        b       the solution vector  x .
c
c     error condition
c
c        a division by zero will occur if the input factor contains a
c        zero on the diagonal.  technically this indicates singularity
c        but it is often caused by improper arguments or improper
c        setting of lda .  it will not occur if the subroutines are
c        called correctly and if dgeco has set rcond .gt. 0.0
c        or dgefa has set info .eq. 0 .
c
c     to compute  inverse(a) * c  where  c  is a matrix
c     with  p  columns
c           call dgeco(a,lda,n,ipvt,rcond,z)
c           if (rcond is too small) go to ...
c           do 10 j = 1, p
c              call dgesl(a,lda,n,ipvt,c(1,j),0)
c        10 continue
c
c     linpack. this version dated 08/14/78 .
c     cleve moler, university of new mexico, argonne national lab.
c
c     subroutines and functions
c
c     blas daxpy,ddot
c
c     internal variables
c
      double precision ddot,t
      integer k,kb,l,nm1
c
      nm1 = n - 1
      if (job .ne. 0) go to 50
c
c        job = 0 , solve  a * x = b
c        first solve  l*y = b
c
         if (nm1 .lt. 1) go to 30
         do 20 k = 1, nm1
            l = ipvt(k)
            t = b(l)
            if (l .eq. k) go to 10
               b(l) = b(k)
               b(k) = t
   10       continue
            call daxpy(n-k,t,a(k+1,k),1,b(k+1),1)
   20    continue
   30    continue
c
c        now solve  u*x = y
c
         do 40 kb = 1, n
            k = n + 1 - kb
            b(k) = b(k)/a(k,k)
            t = -b(k)
            call daxpy(k-1,t,a(1,k),1,b(1),1)
   40    continue
      go to 100
   50 continue
c
c        job = nonzero, solve  trans(a) * x = b
c        first solve  trans(u)*y = b
c
         do 60 k = 1, n
            t = ddot(k-1,a(1,k),1,b(1),1)
            b(k) = (b(k) - t)/a(k,k)
   60    continue
c
c        now solve trans(l)*x = y
c
         if (nm1 .lt. 1) go to 90
         do 80 kb = 1, nm1
            k = n - kb
            b(k) = b(k) + ddot(n-k,a(k+1,k),1,b(k+1),1)
            l = ipvt(k)
            if (l .eq. k) go to 70
               t = b(l)
               b(l) = b(k)
               b(k) = t
   70       continue
   80    continue
   90    continue
  100 continue
      return
      end
      subroutine  dscal(n,da,dx,incx)
c
c     scales a vector by a constant.
c     uses unrolled loops for increment equal to one.
c     jack dongarra, linpack, 3/11/78.
c     modified 3/93 to return if incx .le. 0.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision da,dx(*)
      integer i,incx,m,mp1,n,nincx
c
      if( n.le.0 .or. incx.le.0 )return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      nincx = n*incx
      do 10 i = 1,nincx,incx
        dx(i) = da*dx(i)
   10 continue
      return
c
c        code for increment equal to 1
c
c
c        clean-up loop
c
   20 m = mod(n,5)
      if( m .eq. 0 ) go to 40
      do 30 i = 1,m
        dx(i) = da*dx(i)
   30 continue
      if( n .lt. 5 ) return
   40 mp1 = m + 1
      do 50 i = mp1,n,5
        dx(i) = da*dx(i)
        dx(i + 1) = da*dx(i + 1)
        dx(i + 2) = da*dx(i + 2)
        dx(i + 3) = da*dx(i + 3)
        dx(i + 4) = da*dx(i + 4)
   50 continue
      return
      end
      subroutine ewset (n, itol, rtol, atol, ycur, ewt)
clll. optimize
c-----------------------------------------------------------------------
c this subroutine sets the error weight vector ewt according to
c     ewt(i) = rtol(i)*abs(ycur(i)) + atol(i),  i = 1,...,n,
c with the subscript on rtol and/or atol possibly replaced by 1 above,
c depending on the value of itol.
c-----------------------------------------------------------------------
      integer n, itol
      integer i
      double precision rtol, atol, ycur, ewt
      dimension rtol(*), atol(*), ycur(n), ewt(n)
c
c
      go to (10, 20, 30, 40), itol
 10   continue
      do 15 i = 1,n
 15     ewt(i) = rtol(1)*dabs(ycur(i)) + atol(1)
      return
 20   continue
      do 25 i = 1,n
 25     ewt(i) = rtol(1)*dabs(ycur(i)) + atol(i)
      return
 30   continue
      do 35 i = 1,n
 35     ewt(i) = rtol(i)*dabs(ycur(i)) + atol(1)
      return
 40   continue
      do 45 i = 1,n
 45     ewt(i) = rtol(i)*dabs(ycur(i)) + atol(i)
      return
c----------------------- end of subroutine ewset -----------------------
      end
      double precision function fnorm (n, a, w)
clll. optimize
c-----------------------------------------------------------------------
c this function computes the norm of a full n by n matrix,
c stored in the array a, that is consistent with the weighted max-norm
c on vectors, with weights stored in the array w..
c   fnorm = max(i=1,...,n) ( w(i) * sum(j=1,...,n) abs(a(i,j))/w(j) )
c-----------------------------------------------------------------------
      integer n,   i, j
      double precision a,   w, an, sum
      dimension a(n,n), w(n)
      an = 0.0d0
      do 20 i = 1,n
        sum = 0.0d0
        do 10 j = 1,n
 10       sum = sum + dabs(a(i,j))/w(j)
        an = dmax1(an,sum*w(i))
 20     continue
      fnorm = an
      return
c----------------------- end of function fnorm -------------------------
      end
      integer function idamax(n,dx,incx)
c
c     finds the index of element having max. absolute value.
c     jack dongarra, linpack, 3/11/78.
c     modified 3/93 to return if incx .le. 0.
c     modified 12/3/93, array(1) declarations changed to array(*)
c
      double precision dx(*),dmax
      integer i,incx,ix,n
c
      idamax = 0
      if( n.lt.1 .or. incx.le.0 ) return
      idamax = 1
      if(n.eq.1)return
      if(incx.eq.1)go to 20
c
c        code for increment not equal to 1
c
      ix = 1
      dmax = dabs(dx(1))
      ix = ix + incx
      do 10 i = 2,n
         if(dabs(dx(ix)).le.dmax) go to 5
         idamax = i
         dmax = dabs(dx(ix))
    5    ix = ix + incx
   10 continue
      return
c
c        code for increment equal to 1
c
   20 dmax = dabs(dx(1))
      do 30 i = 2,n
         if(dabs(dx(i)).le.dmax) go to 30
         idamax = i
         dmax = dabs(dx(i))
   30 continue
      return
      end
      subroutine intdy (t, k, yh, nyh, dky, iflag)
clll. optimize
      integer k, nyh, iflag
      integer iownd, iowns,
     1   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     2   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer i, ic, j, jb, jb2, jj, jj1, jp1
      double precision t, yh, dky
      double precision rowns,
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision c, r, s, tp
      dimension yh(nyh,*), dky(*)
      common /ls0001/ rowns(209),
     2   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround,
     3   iownd(14), iowns(6),
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
c-----------------------------------------------------------------------
c intdy computes interpolated values of the k-th derivative of the
c dependent variable vector y, and stores it in dky.  this routine
c is called within the package with k = 0 and t = tout, but may
c also be called by the user for any k up to the current order.
c (see detailed instructions in the usage documentation.)
c-----------------------------------------------------------------------
c the computed values in dky are gotten by interpolation using the
c nordsieck history array yh.  this array corresponds uniquely to a
c vector-valued polynomial of degree nqcur or less, and dky is set
c to the k-th derivative of this polynomial at t.
c the formula for dky is..
c              q
c  dky(i)  =  sum  c(j,k) * (t - tn)**(j-k) * h**(-j) * yh(i,j+1)
c             j=k
c where  c(j,k) = j*(j-1)*...*(j-k+1), q = nqcur, tn = tcur, h = hcur.
c the quantities  nq = nqcur, l = nq+1, n = neq, tn, and h are
c communicated by common.  the above sum is done in reverse order.
c iflag is returned negative if either k or t is out of bounds.
c-----------------------------------------------------------------------
      iflag = 0
      if (k .lt. 0 .or. k .gt. nq) go to 80
      tp = tn - hu -  100.0d0*uround*(tn + hu)
      if ((t-tp)*(t-tn) .gt. 0.0d0) go to 90
c
      s = (t - tn)/h
      ic = 1
      if (k .eq. 0) go to 15
      jj1 = l - k
      do 10 jj = jj1,nq
 10     ic = ic*jj
 15   c = dfloat(ic)
      do 20 i = 1,n
 20     dky(i) = c*yh(i,l)
      if (k .eq. nq) go to 55
      jb2 = nq - k
      do 50 jb = 1,jb2
        j = nq - jb
        jp1 = j + 1
        ic = 1
        if (k .eq. 0) go to 35
        jj1 = jp1 - k
        do 30 jj = jj1,j
 30       ic = ic*jj
 35     c = dfloat(ic)
        do 40 i = 1,n
 40       dky(i) = c*yh(i,jp1) + s*dky(i)
 50     continue
      if (k .eq. 0) return
 55   r = h**(-k)
      do 60 i = 1,n
 60     dky(i) = r*dky(i)
      return
c
 80   call xerrwv(30hintdy--  k (=i1) illegal      ,
     1   30, 51, 0, 1, k, 0, 0, 0.0d0, 0.0d0)
      iflag = -1
      return
 90   call xerrwv(30hintdy--  t (=r1) illegal      ,
     1   30, 52, 0, 0, 0, 0, 1, t, 0.0d0)
      call xerrwv(
     1  60h      t not in interval tcur - hu (= r1) to tcur (=r2)      ,
     1   60, 52, 0, 0, 0, 0, 2, tp, tn)
      iflag = -2
      return
c----------------------- end of subroutine intdy -----------------------
      end
      subroutine lsoda (f, neq, y, t, tout, itol, rtol, atol, itask,
     1            istate, iopt, rwork, lrw, iwork, liw, jac, jt)
      external f, jac
      integer neq, itol, itask, istate, iopt, lrw, iwork, liw, jt
      double precision y, t, tout, rtol, atol, rwork
      dimension neq(1), y(*), rtol(*), atol(*), rwork(lrw), iwork(liw)
c-----------------------------------------------------------------------
c this is the 24 feb 1997 version of
c lsoda.. livermore solver for ordinary differential equations, with
c         automatic method switching for stiff and nonstiff problems.
c
c this version is in double precision.
c
c lsoda solves the initial value problem for stiff or nonstiff
c systems of first order ode-s,
c     dy/dt = f(t,y) ,  or, in component form,
c     dy(i)/dt = f(i) = f(i,t,y(1),y(2),...,y(neq)) (i = 1,...,neq).
c
c this a variant version of the lsode package.
c it switches automatically between stiff and nonstiff methods.
c this means that the user does not have to determine whether the
c problem is stiff or not, and the solver will automatically choose the
c appropriate method.  it always starts with the nonstiff method.
c
c authors..
c                linda r. petzold  and  alan c. hindmarsh,
c                computing and mathematics research division, l-316
c                lawrence livermore national laboratory
c                livermore, ca 94550.
c
c references..
c 1.  alan c. hindmarsh,  odepack, a systematized collection of ode
c     solvers, in scientific computing, r. s. stepleman et al. (eds.),
c     north-holland, amsterdam, 1983, pp. 55-64.
c 2.  linda r. petzold, automatic selection of methods for solving
c     stiff and nonstiff systems of ordinary differential equations,
c     siam j. sci. stat. comput. 4 (1983), pp. 136-148.
c-----------------------------------------------------------------------
c summary of usage.
c
c communication between the user and the lsoda package, for normal
c situations, is summarized here.  this summary describes only a subset
c of the full set of options available.  see the full description for
c details, including alternative treatment of the jacobian matrix,
c optional inputs and outputs, nonstandard options, and
c instructions for special situations.  see also the example
c problem (with program and output) following this summary.
c
c a. first provide a subroutine of the form..
c               subroutine f (neq, t, y, ydot)
c               dimension y(neq), ydot(neq)
c which supplies the vector function f by loading ydot(i) with f(i).
c
c b. write a main program which calls subroutine lsoda once for
c each point at which answers are desired.  this should also provide
c for possible use of logical unit 6 for output of error messages
c by lsoda.  on the first call to lsoda, supply arguments as follows..
c f      = name of subroutine for right-hand side vector f.
c          this name must be declared external in calling program.
c neq    = number of first order ode-s.
c y      = array of initial values, of length neq.
c t      = the initial value of the independent variable.
c tout   = first point where output is desired (.ne. t).
c itol   = 1 or 2 according as atol (below) is a scalar or array.
c rtol   = relative tolerance parameter (scalar).
c atol   = absolute tolerance parameter (scalar or array).
c          the estimated local error in y(i) will be controlled so as
c          to be less than
c             ewt(i) = rtol*abs(y(i)) + atol     if itol = 1, or
c             ewt(i) = rtol*abs(y(i)) + atol(i)  if itol = 2.
c          thus the local error test passes if, in each component,
c          either the absolute error is less than atol (or atol(i)),
c          or the relative error is less than rtol.
c          use rtol = 0.0 for pure absolute error control, and
c          use atol = 0.0 (or atol(i) = 0.0) for pure relative error
c          control.  caution.. actual (global) errors may exceed these
c          local tolerances, so choose them conservatively.
c itask  = 1 for normal computation of output values of y at t = tout.
c istate = integer flag (input and output).  set istate = 1.
c iopt   = 0 to indicate no optional inputs used.
c rwork  = real work array of length at least..
c             22 + neq * max(16, neq + 9).
c          see also paragraph e below.
c lrw    = declared length of rwork (in user-s dimension).
c iwork  = integer work array of length at least  20 + neq.
c liw    = declared length of iwork (in user-s dimension).
c jac    = name of subroutine for jacobian matrix.
c          use a dummy name.  see also paragraph e below.
c jt     = jacobian type indicator.  set jt = 2.
c          see also paragraph e below.
c note that the main program must declare arrays y, rwork, iwork,
c and possibly atol.
c
c c. the output from the first call (or any call) is..
c      y = array of computed values of y(t) vector.
c      t = corresponding value of independent variable (normally tout).
c istate = 2  if lsoda was successful, negative otherwise.
c          -1 means excess work done on this call (perhaps wrong jt).
c          -2 means excess accuracy requested (tolerances too small).
c          -3 means illegal input detected (see printed message).
c          -4 means repeated error test failures (check all inputs).
c          -5 means repeated convergence failures (perhaps bad jacobian
c             supplied or wrong choice of jt or tolerances).
c          -6 means error weight became zero during problem. (solution
c             component i vanished, and atol or atol(i) = 0.)
c          -7 means work space insufficient to finish (see messages).
c
c d. to continue the integration after a successful return, simply
c reset tout and call lsoda again.  no other parameters need be reset.
c
c e. note.. if and when lsoda regards the problem as stiff, and
c switches methods accordingly, it must make use of the neq by neq
c jacobian matrix, j = df/dy.  for the sake of simplicity, the
c inputs to lsoda recommended in paragraph b above cause lsoda to
c treat j as a full matrix, and to approximate it internally by
c difference quotients.  alternatively, j can be treated as a band
c matrix (with great potential reduction in the size of the rwork
c array).  also, in either the full or banded case, the user can supply
c j in closed form, with a routine whose name is passed as the jac
c argument.  these alternatives are described in the paragraphs on
c rwork, jac, and jt in the full description of the call sequence below.
c
c-----------------------------------------------------------------------
c example problem.
c
c the following is a simple example problem, with the coding
c needed for its solution by lsoda.  the problem is from chemical
c kinetics, and consists of the following three rate equations..
c     dy1/dt = -.04*y1 + 1.e4*y2*y3
c     dy2/dt = .04*y1 - 1.e4*y2*y3 - 3.e7*y2**2
c     dy3/dt = 3.e7*y2**2
c on the interval from t = 0.0 to t = 4.e10, with initial conditions
c y1 = 1.0, y2 = y3 = 0.  the problem is stiff.
c
c the following coding solves this problem with lsoda,
c printing results at t = .4, 4., ..., 4.e10.  it uses
c itol = 2 and atol much smaller for y2 than y1 or y3 because
c y2 has much smaller values.
c at the end of the run, statistical quantities of interest are
c printed (see optional outputs in the full description below).
c
c     external fex
c     double precision atol, rtol, rwork, t, tout, y
c     dimension y(3), atol(3), rwork(70), iwork(23)
c     neq = 3
c     y(1) = 1.0d0
c     y(2) = 0.0d0
c     y(3) = 0.0d0
c     t = 0.0d0
c     tout = 0.4d0
c     itol = 2
c     rtol = 1.0d-4
c     atol(1) = 1.0d-6
c     atol(2) = 1.0d-10
c     atol(3) = 1.0d-6
c     itask = 1
c     istate = 1
c     iopt = 0
c     lrw = 70
c     liw = 23
c     jt = 2
c     do 40 iout = 1,12
c       call lsoda(fex,neq,y,t,tout,itol,rtol,atol,itask,istate,
c    1     iopt,rwork,lrw,iwork,liw,jdum,jt)
c       write(6,20)t,y(1),y(2),y(3)
c 20    format(7h at t =,e12.4,6h   y =,3e14.6)
c       if (istate .lt. 0) go to 80
c 40    tout = tout*10.0d0
c     write(6,60)iwork(11),iwork(12),iwork(13),iwork(19),rwork(15)
c 60  format(/12h no. steps =,i4,11h  no. f-s =,i4,11h  no. j-s =,i4/
c    1   19h method last used =,i2,25h   last switch was at t =,e12.4)
c     stop
c 80  write(6,90)istate
c 90  format(///22h error halt.. istate =,i3)
c     stop
c     end
c
c     subroutine fex (neq, t, y, ydot)
c     double precision t, y, ydot
c     dimension y(3), ydot(3)
c     ydot(1) = -.04d0*y(1) + 1.0d4*y(2)*y(3)
c     ydot(3) = 3.0d7*y(2)*y(2)
c     ydot(2) = -ydot(1) - ydot(3)
c     return
c     end
c
c the output of this program (on a cdc-7600 in single precision)
c is as follows..
c
c   at t =  4.0000e-01   y =  9.851712e-01  3.386380e-05  1.479493e-02
c   at t =  4.0000e+00   y =  9.055333e-01  2.240655e-05  9.444430e-02
c   at t =  4.0000e+01   y =  7.158403e-01  9.186334e-06  2.841505e-01
c   at t =  4.0000e+02   y =  4.505250e-01  3.222964e-06  5.494717e-01
c   at t =  4.0000e+03   y =  1.831975e-01  8.941774e-07  8.168016e-01
c   at t =  4.0000e+04   y =  3.898730e-02  1.621940e-07  9.610125e-01
c   at t =  4.0000e+05   y =  4.936363e-03  1.984221e-08  9.950636e-01
c   at t =  4.0000e+06   y =  5.161831e-04  2.065786e-09  9.994838e-01
c   at t =  4.0000e+07   y =  5.179817e-05  2.072032e-10  9.999482e-01
c   at t =  4.0000e+08   y =  5.283401e-06  2.113371e-11  9.999947e-01
c   at t =  4.0000e+09   y =  4.659031e-07  1.863613e-12  9.999995e-01
c   at t =  4.0000e+10   y =  1.404280e-08  5.617126e-14  1.000000e+00
c
c   no. steps = 361  no. f-s = 693  no. j-s =  64
c   method last used = 2   last switch was at t =  6.0092e-03
c-----------------------------------------------------------------------
c full description of user interface to lsoda.
c
c the user interface to lsoda consists of the following parts.
c
c i.   the call sequence to subroutine lsoda, which is a driver
c      routine for the solver.  this includes descriptions of both
c      the call sequence arguments and of user-supplied routines.
c      following these descriptions is a description of
c      optional inputs available through the call sequence, and then
c      a description of optional outputs (in the work arrays).
c
c ii.  descriptions of other routines in the lsoda package that may be
c      (optionally) called by the user.  these provide the ability to
c      alter error message handling, save and restore the internal
c      common, and obtain specified derivatives of the solution y(t).
c
c iii. descriptions of common blocks to be declared in overlay
c      or similar environments, or to be saved when doing an interrupt
c      of the problem and continued solution later.
c
c iv.  description of a subroutine in the lsoda package,
c      which the user may replace with his own version, if desired.
c      this relates to the measurement of errors.
c
c-----------------------------------------------------------------------
c part i.  call sequence.
c
c the call sequence parameters used for input only are
c     f, neq, tout, itol, rtol, atol, itask, iopt, lrw, liw, jac, jt,
c and those used for both input and output are
c     y, t, istate.
c the work arrays rwork and iwork are also used for conditional and
c optional inputs and optional outputs.  (the term output here refers
c to the return from subroutine lsoda to the user-s calling program.)
c
c the legality of input parameters will be thoroughly checked on the
c initial call for the problem, but not checked thereafter unless a
c change in input parameters is flagged by istate = 3 on input.
c
c the descriptions of the call arguments are as follows.
c
c f      = the name of the user-supplied subroutine defining the
c          ode system.  the system must be put in the first-order
c          form dy/dt = f(t,y), where f is a vector-valued function
c          of the scalar t and the vector y.  subroutine f is to
c          compute the function f.  it is to have the form
c               subroutine f (neq, t, y, ydot)
c               dimension y(1), ydot(1)
c          where neq, t, and y are input, and the array ydot = f(t,y)
c          is output.  y and ydot are arrays of length neq.
c          (in the dimension statement above, 1 is a dummy
c          dimension.. it can be replaced by any value.)
c          subroutine f should not alter y(1),...,y(neq).
c          f must be declared external in the calling program.
c
c          subroutine f may access user-defined quantities in
c          neq(2),... and/or in y(neq(1)+1),... if neq is an array
c          (dimensioned in f) and/or y has length exceeding neq(1).
c          see the descriptions of neq and y below.
c
c          if quantities computed in the f routine are needed
c          externally to lsoda, an extra call to f should be made
c          for this purpose, for consistent and accurate results.
c          if only the derivative dy/dt is needed, use intdy instead.
c
c neq    = the size of the ode system (number of first order
c          ordinary differential equations).  used only for input.
c          neq may be decreased, but not increased, during the problem.
c          if neq is decreased (with istate = 3 on input), the
c          remaining components of y should be left undisturbed, if
c          these are to be accessed in f and/or jac.
c
c          normally, neq is a scalar, and it is generally referred to
c          as a scalar in this user interface description.  however,
c          neq may be an array, with neq(1) set to the system size.
c          (the lsoda package accesses only neq(1).)  in either case,
c          this parameter is passed as the neq argument in all calls
c          to f and jac.  hence, if it is an array, locations
c          neq(2),... may be used to store other integer data and pass
c          it to f and/or jac.  subroutines f and/or jac must include
c          neq in a dimension statement in that case.
c
c y      = a real array for the vector of dependent variables, of
c          length neq or more.  used for both input and output on the
c          first call (istate = 1), and only for output on other calls.
c          on the first call, y must contain the vector of initial
c          values.  on output, y contains the computed solution vector,
c          evaluated at t.  if desired, the y array may be used
c          for other purposes between calls to the solver.
c
c          this array is passed as the y argument in all calls to
c          f and jac.  hence its length may exceed neq, and locations
c          y(neq+1),... may be used to store other real data and
c          pass it to f and/or jac.  (the lsoda package accesses only
c          y(1),...,y(neq).)
c
c t      = the independent variable.  on input, t is used only on the
c          first call, as the initial point of the integration.
c          on output, after each call, t is the value at which a
c          computed solution y is evaluated (usually the same as tout).
c          on an error return, t is the farthest point reached.
c
c tout   = the next value of t at which a computed solution is desired.
c          used only for input.
c
c          when starting the problem (istate = 1), tout may be equal
c          to t for one call, then should .ne. t for the next call.
c          for the initial t, an input value of tout .ne. t is used
c          in order to determine the direction of the integration
c          (i.e. the algebraic sign of the step sizes) and the rough
c          scale of the problem.  integration in either direction
c          (forward or backward in t) is permitted.
c
c          if itask = 2 or 5 (one-step modes), tout is ignored after
c          the first call (i.e. the first call with tout .ne. t).
c          otherwise, tout is required on every call.
c
c          if itask = 1, 3, or 4, the values of tout need not be
c          monotone, but a value of tout which backs up is limited
c          to the current internal t interval, whose endpoints are
c          tcur - hu and tcur (see optional outputs, below, for
c          tcur and hu).
c
c itol   = an indicator for the type of error control.  see
c          description below under atol.  used only for input.
c
c rtol   = a relative error tolerance parameter, either a scalar or
c          an array of length neq.  see description below under atol.
c          input only.
c
c atol   = an absolute error tolerance parameter, either a scalar or
c          an array of length neq.  input only.
c
c             the input parameters itol, rtol, and atol determine
c          the error control performed by the solver.  the solver will
c          control the vector e = (e(i)) of estimated local errors
c          in y, according to an inequality of the form
c                      max-norm of ( e(i)/ewt(i) )   .le.   1,
c          where ewt = (ewt(i)) is a vector of positive error weights.
c          the values of rtol and atol should all be non-negative.
c          the following table gives the types (scalar/array) of
c          rtol and atol, and the corresponding form of ewt(i).
c
c             itol    rtol       atol          ewt(i)
c              1     scalar     scalar     rtol*abs(y(i)) + atol
c              2     scalar     array      rtol*abs(y(i)) + atol(i)
c              3     array      scalar     rtol(i)*abs(y(i)) + atol
c              4     array      array      rtol(i)*abs(y(i)) + atol(i)
c
c          when either of these parameters is a scalar, it need not
c          be dimensioned in the user-s calling program.
c
c          if none of the above choices (with itol, rtol, and atol
c          fixed throughout the problem) is suitable, more general
c          error controls can be obtained by substituting a
c          user-supplied routine for the setting of ewt.
c          see part iv below.
c
c          if global errors are to be estimated by making a repeated
c          run on the same problem with smaller tolerances, then all
c          components of rtol and atol (i.e. of ewt) should be scaled
c          down uniformly.
c
c itask  = an index specifying the task to be performed.
c          input only.  itask has the following values and meanings.
c          1  means normal computation of output values of y(t) at
c             t = tout (by overshooting and interpolating).
c          2  means take one step only and return.
c          3  means stop at the first internal mesh point at or
c             beyond t = tout and return.
c          4  means normal computation of output values of y(t) at
c             t = tout but without overshooting t = tcrit.
c             tcrit must be input as rwork(1).  tcrit may be equal to
c             or beyond tout, but not behind it in the direction of
c             integration.  this option is useful if the problem
c             has a singularity at or beyond t = tcrit.
c          5  means take one step, without passing tcrit, and return.
c             tcrit must be input as rwork(1).
c
c          note..  if itask = 4 or 5 and the solver reaches tcrit
c          (within roundoff), it will return t = tcrit (exactly) to
c          indicate this (unless itask = 4 and tout comes before tcrit,
c          in which case answers at t = tout are returned first).
c
c istate = an index used for input and output to specify the
c          the state of the calculation.
c
c          on input, the values of istate are as follows.
c          1  means this is the first call for the problem
c             (initializations will be done).  see note below.
c          2  means this is not the first call, and the calculation
c             is to continue normally, with no change in any input
c             parameters except possibly tout and itask.
c             (if itol, rtol, and/or atol are changed between calls
c             with istate = 2, the new values will be used but not
c             tested for legality.)
c          3  means this is not the first call, and the
c             calculation is to continue normally, but with
c             a change in input parameters other than
c             tout and itask.  changes are allowed in
c             neq, itol, rtol, atol, iopt, lrw, liw, jt, ml, mu,
c             and any optional inputs except h0, mxordn, and mxords.
c             (see iwork description for ml and mu.)
c          note..  a preliminary call with tout = t is not counted
c          as a first call here, as no initialization or checking of
c          input is done.  (such a call is sometimes useful for the
c          purpose of outputting the initial conditions.)
c          thus the first call for which tout .ne. t requires
c          istate = 1 on input.
c
c          on output, istate has the following values and meanings.
c           1  means nothing was done, as tout was equal to t with
c              istate = 1 on input.  (however, an internal counter was
c              set to detect and prevent repeated calls of this type.)
c           2  means the integration was performed successfully.
c          -1  means an excessive amount of work (more than mxstep
c              steps) was done on this call, before completing the
c              requested task, but the integration was otherwise
c              successful as far as t.  (mxstep is an optional input
c              and is normally 500.)  to continue, the user may
c              simply reset istate to a value .gt. 1 and call again
c              (the excess work step counter will be reset to 0).
c              in addition, the user may increase mxstep to avoid
c              this error return (see below on optional inputs).
c          -2  means too much accuracy was requested for the precision
c              of the machine being used.  this was detected before
c              completing the requested task, but the integration
c              was successful as far as t.  to continue, the tolerance
c              parameters must be reset, and istate must be set
c              to 3.  the optional output tolsf may be used for this
c              purpose.  (note.. if this condition is detected before
c              taking any steps, then an illegal input return
c              (istate = -3) occurs instead.)
c          -3  means illegal input was detected, before taking any
c              integration steps.  see written message for details.
c              note..  if the solver detects an infinite loop of calls
c              to the solver with illegal input, it will cause
c              the run to stop.
c          -4  means there were repeated error test failures on
c              one attempted step, before completing the requested
c              task, but the integration was successful as far as t.
c              the problem may have a singularity, or the input
c              may be inappropriate.
c          -5  means there were repeated convergence test failures on
c              one attempted step, before completing the requested
c              task, but the integration was successful as far as t.
c              this may be caused by an inaccurate jacobian matrix,
c              if one is being used.
c          -6  means ewt(i) became zero for some i during the
c              integration.  pure relative error control (atol(i)=0.0)
c              was requested on a variable which has now vanished.
c              the integration was successful as far as t.
c          -7  means the length of rwork and/or iwork was too small to
c              proceed, but the integration was successful as far as t.
c              this happens when lsoda chooses to switch methods
c              but lrw and/or liw is too small for the new method.
c
c          note..  since the normal output value of istate is 2,
c          it does not need to be reset for normal continuation.
c          also, since a negative input value of istate will be
c          regarded as illegal, a negative output value requires the
c          user to change it, and possibly other inputs, before
c          calling the solver again.
c
c iopt   = an integer flag to specify whether or not any optional
c          inputs are being used on this call.  input only.
c          the optional inputs are listed separately below.
c          iopt = 0 means no optional inputs are being used.
c                   default values will be used in all cases.
c          iopt = 1 means one or more optional inputs are being used.
c
c rwork  = a real array (double precision) for work space, and (in the
c          first 20 words) for conditional and optional inputs and
c          optional outputs.
c          as lsoda switches automatically between stiff and nonstiff
c          methods, the required length of rwork can change during the
c          problem.  thus the rwork array passed to lsoda can either
c          have a static (fixed) length large enough for both methods,
c          or have a dynamic (changing) length altered by the calling
c          program in response to output from lsoda.
c
c                       --- fixed length case ---
c          if the rwork length is to be fixed, it should be at least
c               max (lrn, lrs),
c          where lrn and lrs are the rwork lengths required when the
c          current method is nonstiff or stiff, respectively.
c
c          the separate rwork length requirements lrn and lrs are
c          as follows..
c          if neq is constant and the maximum method orders have
c          their default values, then
c             lrn = 20 + 16*neq,
c             lrs = 22 + 9*neq + neq**2           if jt = 1 or 2,
c             lrs = 22 + 10*neq + (2*ml+mu)*neq   if jt = 4 or 5.
c          under any other conditions, lrn and lrs are given by..
c             lrn = 20 + nyh*(mxordn+1) + 3*neq,
c             lrs = 20 + nyh*(mxords+1) + 3*neq + lmat,
c          where
c             nyh    = the initial value of neq,
c             mxordn = 12, unless a smaller value is given as an
c                      optional input,
c             mxords = 5, unless a smaller value is given as an
c                      optional input,
c             lmat   = length of matrix work space..
c             lmat   = neq**2 + 2              if jt = 1 or 2,
c             lmat   = (2*ml + mu + 1)*neq + 2 if jt = 4 or 5.
c
c                       --- dynamic length case ---
c          if the length of rwork is to be dynamic, then it should
c          be at least lrn or lrs, as defined above, depending on the
c          current method.  initially, it must be at least lrn (since
c          lsoda starts with the nonstiff method).  on any return
c          from lsoda, the optional output mcur indicates the current
c          method.  if mcur differs from the value it had on the
c          previous return, or if there has only been one call to
c          lsoda and mcur is now 2, then lsoda has switched
c          methods during the last call, and the length of rwork
c          should be reset (to lrn if mcur = 1, or to lrs if
c          mcur = 2).  (an increase in the rwork length is required
c          if lsoda returned istate = -7, but not otherwise.)
c          after resetting the length, call lsoda with istate = 3
c          to signal that change.
c
c lrw    = the length of the array rwork, as declared by the user.
c          (this will be checked by the solver.)
c
c iwork  = an integer array for work space.
c          as lsoda switches automatically between stiff and nonstiff
c          methods, the required length of iwork can change during
c          problem, between
c             lis = 20 + neq   and   lin = 20,
c          respectively.  thus the iwork array passed to lsoda can
c          either have a fixed length of at least 20 + neq, or have a
c          dynamic length of at least lin or lis, depending on the
c          current method.  the comments on dynamic length under
c          rwork above apply here.  initially, this length need
c          only be at least lin = 20.
c
c          the first few words of iwork are used for conditional and
c          optional inputs and optional outputs.
c
c          the following 2 words in iwork are conditional inputs..
c            iwork(1) = ml     these are the lower and upper
c            iwork(2) = mu     half-bandwidths, respectively, of the
c                       banded jacobian, excluding the main diagonal.
c                       the band is defined by the matrix locations
c                       (i,j) with i-ml .le. j .le. i+mu.  ml and mu
c                       must satisfy  0 .le.  ml,mu  .le. neq-1.
c                       these are required if jt is 4 or 5, and
c                       ignored otherwise.  ml and mu may in fact be
c                       the band parameters for a matrix to which
c                       df/dy is only approximately equal.
c
c liw    = the length of the array iwork, as declared by the user.
c          (this will be checked by the solver.)
c
c note.. the base addresses of the work arrays must not be
c altered between calls to lsoda for the same problem.
c the contents of the work arrays must not be altered
c between calls, except possibly for the conditional and
c optional inputs, and except for the last 3*neq words of rwork.
c the latter space is used for internal scratch space, and so is
c available for use by the user outside lsoda between calls, if
c desired (but not for use by f or jac).
c
c jac    = the name of the user-supplied routine to compute the
c          jacobian matrix, df/dy, if jt = 1 or 4.  the jac routine
c          is optional, but if the problem is expected to be stiff much
c          of the time, you are encouraged to supply jac, for the sake
c          of efficiency.  (alternatively, set jt = 2 or 5 to have
c          lsoda compute df/dy internally by difference quotients.)
c          if and when lsoda uses df/dy, if treats this neq by neq
c          matrix either as full (jt = 1 or 2), or as banded (jt =
c          4 or 5) with half-bandwidths ml and mu (discussed under
c          iwork above).  in either case, if jt = 1 or 4, the jac
c          routine must compute df/dy as a function of the scalar t
c          and the vector y.  it is to have the form
c               subroutine jac (neq, t, y, ml, mu, pd, nrowpd)
c               dimension y(1), pd(nrowpd,1)
c          where neq, t, y, ml, mu, and nrowpd are input and the array
c          pd is to be loaded with partial derivatives (elements of
c          the jacobian matrix) on output.  pd must be given a first
c          dimension of nrowpd.  t and y have the same meaning as in
c          subroutine f.  (in the dimension statement above, 1 is a
c          dummy dimension.. it can be replaced by any value.)
c               in the full matrix case (jt = 1), ml and mu are
c          ignored, and the jacobian is to be loaded into pd in
c          columnwise manner, with df(i)/dy(j) loaded into pd(i,j).
c               in the band matrix case (jt = 4), the elements
c          within the band are to be loaded into pd in columnwise
c          manner, with diagonal lines of df/dy loaded into the rows
c          of pd.  thus df(i)/dy(j) is to be loaded into pd(i-j+mu+1,j).
c          ml and mu are the half-bandwidth parameters (see iwork).
c          the locations in pd in the two triangular areas which
c          correspond to nonexistent matrix elements can be ignored
c          or loaded arbitrarily, as they are overwritten by lsoda.
c               jac need not provide df/dy exactly.  a crude
c          approximation (possibly with a smaller bandwidth) will do.
c               in either case, pd is preset to zero by the solver,
c          so that only the nonzero elements need be loaded by jac.
c          each call to jac is preceded by a call to f with the same
c          arguments neq, t, and y.  thus to gain some efficiency,
c          intermediate quantities shared by both calculations may be
c          saved in a user common block by f and not recomputed by jac,
c          if desired.  also, jac may alter the y array, if desired.
c          jac must be declared external in the calling program.
c               subroutine jac may access user-defined quantities in
c          neq(2),... and/or in y(neq(1)+1),... if neq is an array
c          (dimensioned in jac) and/or y has length exceeding neq(1).
c          see the descriptions of neq and y above.
c
c jt     = jacobian type indicator.  used only for input.
c          jt specifies how the jacobian matrix df/dy will be
c          treated, if and when lsoda requires this matrix.
c          jt has the following values and meanings..
c           1 means a user-supplied full (neq by neq) jacobian.
c           2 means an internally generated (difference quotient) full
c             jacobian (using neq extra calls to f per df/dy value).
c           4 means a user-supplied banded jacobian.
c           5 means an internally generated banded jacobian (using
c             ml+mu+1 extra calls to f per df/dy evaluation).
c          if jt = 1 or 4, the user must supply a subroutine jac
c          (the name is arbitrary) as described above under jac.
c          if jt = 2 or 5, a dummy argument can be used.
c-----------------------------------------------------------------------
c optional inputs.
c
c the following is a list of the optional inputs provided for in the
c call sequence.  (see also part ii.)  for each such input variable,
c this table lists its name as used in this documentation, its
c location in the call sequence, its meaning, and the default value.
c the use of any of these inputs requires iopt = 1, and in that
c case all of these inputs are examined.  a value of zero for any
c of these optional inputs will cause the default value to be used.
c thus to use a subset of the optional inputs, simply preload
c locations 5 to 10 in rwork and iwork to 0.0 and 0 respectively, and
c then set those of interest to nonzero values.
c
c name    location      meaning and default value
c
c h0      rwork(5)  the step size to be attempted on the first step.
c                   the default value is determined by the solver.
c
c hmax    rwork(6)  the maximum absolute step size allowed.
c                   the default value is infinite.
c
c hmin    rwork(7)  the minimum absolute step size allowed.
c                   the default value is 0.  (this lower bound is not
c                   enforced on the final step before reaching tcrit
c                   when itask = 4 or 5.)
c
c ixpr    iwork(5)  flag to generate extra printing at method switches.
c                   ixpr = 0 means no extra printing (the default).
c                   ixpr = 1 means print data on each switch.
c                   t, h, and nst will be printed on the same logical
c                   unit as used for error messages.
c
c mxstep  iwork(6)  maximum number of (internally defined) steps
c                   allowed during one call to the solver.
c                   the default value is 500.
c
c mxhnil  iwork(7)  maximum number of messages printed (per problem)
c                   warning that t + h = t on a step (h = step size).
c                   this must be positive to result in a non-default
c                   value.  the default value is 10.
c
c mxordn  iwork(8)  the maximum order to be allowed for the nonstiff
c                   (adams) method.  the default value is 12.
c                   if mxordn exceeds the default value, it will
c                   be reduced to the default value.
c                   mxordn is held constant during the problem.
c
c mxords  iwork(9)  the maximum order to be allowed for the stiff
c                   (bdf) method.  the default value is 5.
c                   if mxords exceeds the default value, it will
c                   be reduced to the default value.
c                   mxords is held constant during the problem.
c-----------------------------------------------------------------------
c optional outputs.
c
c as optional additional output from lsoda, the variables listed
c below are quantities related to the performance of lsoda
c which are available to the user.  these are communicated by way of
c the work arrays, but also have internal mnemonic names as shown.
c except where stated otherwise, all of these outputs are defined
c on any successful return from lsoda, and on any return with
c istate = -1, -2, -4, -5, or -6.  on an illegal input return
c (istate = -3), they will be unchanged from their existing values
c (if any), except possibly for tolsf, lenrw, and leniw.
c on any error return, outputs relevant to the error will be defined,
c as noted below.
c
c name    location      meaning
c
c hu      rwork(11) the step size in t last used (successfully).
c
c hcur    rwork(12) the step size to be attempted on the next step.
c
c tcur    rwork(13) the current value of the independent variable
c                   which the solver has actually reached, i.e. the
c                   current internal mesh point in t.  on output, tcur
c                   will always be at least as far as the argument
c                   t, but may be farther (if interpolation was done).
c
c tolsf   rwork(14) a tolerance scale factor, greater than 1.0,
c                   computed when a request for too much accuracy was
c                   detected (istate = -3 if detected at the start of
c                   the problem, istate = -2 otherwise).  if itol is
c                   left unaltered but rtol and atol are uniformly
c                   scaled up by a factor of tolsf for the next call,
c                   then the solver is deemed likely to succeed.
c                   (the user may also ignore tolsf and alter the
c                   tolerance parameters in any other way appropriate.)
c
c tsw     rwork(15) the value of t at the time of the last method
c                   switch, if any.
c
c nst     iwork(11) the number of steps taken for the problem so far.
c
c nfe     iwork(12) the number of f evaluations for the problem so far.
c
c nje     iwork(13) the number of jacobian evaluations (and of matrix
c                   lu decompositions) for the problem so far.
c
c nqu     iwork(14) the method order last used (successfully).
c
c nqcur   iwork(15) the order to be attempted on the next step.
c
c imxer   iwork(16) the index of the component of largest magnitude in
c                   the weighted local error vector ( e(i)/ewt(i) ),
c                   on an error return with istate = -4 or -5.
c
c lenrw   iwork(17) the length of rwork actually required, assuming
c                   that the length of rwork is to be fixed for the
c                   rest of the problem, and that switching may occur.
c                   this is defined on normal returns and on an illegal
c                   input return for insufficient storage.
c
c leniw   iwork(18) the length of iwork actually required, assuming
c                   that the length of iwork is to be fixed for the
c                   rest of the problem, and that switching may occur.
c                   this is defined on normal returns and on an illegal
c                   input return for insufficient storage.
c
c mused   iwork(19) the method indicator for the last successful step..
c                   1 means adams (nonstiff), 2 means bdf (stiff).
c
c mcur    iwork(20) the current method indicator..
c                   1 means adams (nonstiff), 2 means bdf (stiff).
c                   this is the method to be attempted
c                   on the next step.  thus it differs from mused
c                   only if a method switch has just been made.
c
c the following two arrays are segments of the rwork array which
c may also be of interest to the user as optional outputs.
c for each array, the table below gives its internal name,
c its base address in rwork, and its description.
c
c name    base address      description
c
c yh      21             the nordsieck history array, of size nyh by
c                        (nqcur + 1), where nyh is the initial value
c                        of neq.  for j = 0,1,...,nqcur, column j+1
c                        of yh contains hcur**j/factorial(j) times
c                        the j-th derivative of the interpolating
c                        polynomial currently representing the solution,
c                        evaluated at t = tcur.
c
c acor     lacor         array of size neq used for the accumulated
c         (from common   corrections on each step, scaled on output
c           as noted)    to represent the estimated local error in y
c                        on the last step.  this is the vector e in
c                        the description of the error control.  it is
c                        defined only on a successful return from lsoda.
c                        the base address lacor is obtained by
c                        including in the user-s program the
c                        following 3 lines..
c                           double precision rls
c                           common /ls0001/ rls(218), ils(39)
c                           lacor = ils(5)
c
c-----------------------------------------------------------------------
c part ii.  other routines callable.
c
c the following are optional calls which the user may make to
c gain additional capabilities in conjunction with lsoda.
c (the routines xsetun and xsetf are designed to conform to the
c slatec error handling package.)
c
c     form of call                  function
c   call xsetun(lun)          set the logical unit number, lun, for
c                             output of messages from lsoda, if
c                             the default is not desired.
c                             the default value of lun is 6.
c
c   call xsetf(mflag)         set a flag to control the printing of
c                             messages by lsoda.
c                             mflag = 0 means do not print. (danger..
c                             this risks losing valuable information.)
c                             mflag = 1 means print (the default).
c
c                             either of the above calls may be made at
c                             any time and will take effect immediately.
c
c   call srcma(rsav,isav,job) saves and restores the contents of
c                             the internal common blocks used by
c                             lsoda (see part iii below).
c                             rsav must be a real array of length 240
c                             or more, and isav must be an integer
c                             array of length 50 or more.
c                             job=1 means save common into rsav/isav.
c                             job=2 means restore common from rsav/isav.
c                                srcma is useful if one is
c                             interrupting a run and restarting
c                             later, or alternating between two or
c                             more problems solved with lsoda.
c
c   call intdy(,,,,,)         provide derivatives of y, of various
c        (see below)          orders, at a specified point t, if
c                             desired.  it may be called only after
c                             a successful return from lsoda.
c
c the detailed instructions for using intdy are as follows.
c the form of the call is..
c
c   call intdy (t, k, rwork(21), nyh, dky, iflag)
c
c the input parameters are..
c
c t         = value of independent variable where answers are desired
c             (normally the same as the t last returned by lsoda).
c             for valid results, t must lie between tcur - hu and tcur.
c             (see optional outputs for tcur and hu.)
c k         = integer order of the derivative desired.  k must satisfy
c             0 .le. k .le. nqcur, where nqcur is the current order
c             (see optional outputs).  the capability corresponding
c             to k = 0, i.e. computing y(t), is already provided
c             by lsoda directly.  since nqcur .ge. 1, the first
c             derivative dy/dt is always available with intdy.
c rwork(21) = the base address of the history array yh.
c nyh       = column length of yh, equal to the initial value of neq.
c
c the output parameters are..
c
c dky       = a real array of length neq containing the computed value
c             of the k-th derivative of y(t).
c iflag     = integer flag, returned as 0 if k and t were legal,
c             -1 if k was illegal, and -2 if t was illegal.
c             on an error return, a message is also written.
c-----------------------------------------------------------------------
c part iii.  common blocks.
c
c if lsoda is to be used in an overlay situation, the user
c must declare, in the primary overlay, the variables in..
c   (1) the call sequence to lsoda,
c   (2) the three internal common blocks
c         /ls0001/  of length  257  (218 double precision words
c                         followed by 39 integer words),
c         /lsa001/  of length  31    (22 double precision words
c                         followed by  9 integer words),
c         /eh0001/  of length  2 (integer words).
c
c if lsoda is used on a system in which the contents of internal
c common blocks are not preserved between calls, the user should
c declare the above common blocks in his main program to insure
c that their contents are preserved.
c
c if the solution of a given problem by lsoda is to be interrupted
c and then later continued, such as when restarting an interrupted run
c or alternating between two or more problems, the user should save,
c following the return from the last lsoda call prior to the
c interruption, the contents of the call sequence variables and the
c internal common blocks, and later restore these values before the
c next lsoda call for that problem.  to save and restore the common
c blocks, use subroutine srcma (see part ii above).
c
c-----------------------------------------------------------------------
c part iv.  optionally replaceable solver routines.
c
c below is a description of a routine in the lsoda package which
c relates to the measurement of errors, and can be
c replaced by a user-supplied version, if desired.  however, since such
c a replacement may have a major impact on performance, it should be
c done only when absolutely necessary, and only with great caution.
c (note.. the means by which the package version of a routine is
c superseded by the user-s version may be system-dependent.)
c
c (a) ewset.
c the following subroutine is called just before each internal
c integration step, and sets the array of error weights, ewt, as
c described under itol/rtol/atol above..
c     subroutine ewset (neq, itol, rtol, atol, ycur, ewt)
c where neq, itol, rtol, and atol are as in the lsoda call sequence,
c ycur contains the current dependent variable vector, and
c ewt is the array of weights set by ewset.
c
c if the user supplies this subroutine, it must return in ewt(i)
c (i = 1,...,neq) a positive quantity suitable for comparing errors
c in y(i) to.  the ewt array returned by ewset is passed to the
c vmnorm routine, and also used by lsoda in the computation
c of the optional output imxer, and the increments for difference
c quotient jacobians.
c
c in the user-supplied version of ewset, it may be desirable to use
c the current values of derivatives of y.  derivatives up to order nq
c are available from the history array yh, described above under
c optional outputs.  in ewset, yh is identical to the ycur array,
c extended to nq + 1 columns with a column length of nyh and scale
c factors of h**j/factorial(j).  on the first call for the problem,
c given by nst = 0, nq is 1 and h is temporarily set to 1.0.
c the quantities nq, nyh, h, and nst can be obtained by including
c in ewset the statements..
c     double precision h, rls
c     common /ls0001/ rls(218),ils(39)
c     nq = ils(35)
c     nyh = ils(14)
c     nst = ils(36)
c     h = rls(212)
c thus, for example, the current value of dy/dt can be obtained as
c ycur(nyh+i)/h  (i=1,...,neq)  (and the division by h is
c unnecessary when nst = 0).
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
c other routines in the lsoda package.
c
c in addition to subroutine lsoda, the lsoda package includes the
c following subroutines and function routines..
c  intdy    computes an interpolated value of the y vector at t = tout.
c  stoda    is the core integrator, which does one step of the
c           integration and the associated error control.
c  cfode    sets all method coefficients and test constants.
c  prja     computes and preprocesses the jacobian matrix j = df/dy
c           and the newton iteration matrix p = i - h*l0*j.
c  solsy    manages solution of linear system in chord iteration.
c  ewset    sets the error weight vector ewt before each step.
c  vmnorm   computes the weighted max-norm of a vector.
c  fnorm    computes the norm of a full matrix consistent with the
c           weighted max-norm on vectors.
c  bnorm    computes the norm of a band matrix consistent with the
c           weighted max-norm on vectors.
c  srcma    is a user-callable routine to save and restore
c           the contents of the internal common blocks.
c  dgefa and dgesl   are routines from linpack for solving full
c           systems of linear algebraic equations.
c  dgbfa and dgbsl   are routines from linpack for solving banded
c           linear systems.
c  daxpy, dscal, idamax, and ddot   are basic linear algebra modules
c           (blas) used by the above linpack routines.
c  d1mach   computes the unit roundoff in a machine-independent manner.
c  xerrwv, xsetun, and xsetf   handle the printing of all error
c           messages and warnings.  xerrwv is machine-dependent.
c note..  vmnorm, fnorm, bnorm, idamax, ddot, and d1mach are function
c routines.  all the others are subroutines.
c
c the intrinsic and external routines used by lsoda are..
c dabs, dmax1, dmin1, dfloat, max0, min0, mod, dsign, dsqrt, and write.
c
c a block data subprogram is also included with the package,
c for loading some of the variables in internal common.
c
c-----------------------------------------------------------------------
c the following card is for optimized compilation on lll compilers.
clll. optimize
c-----------------------------------------------------------------------
      external prja, solsy
      integer illin, init, lyh, lewt, lacor, lsavf, lwm, liwm,
     1   mxstep, mxhnil, nhnil, ntrep, nslast, nyh, iowns
      integer icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     1   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer insufr, insufi, ixpr, iowns2, jtyp, mused, mxordn, mxords
      integer i, i1, i2, iflag, imxer, kgo, lf0,
     1   leniw, lenrw, lenwm, ml, mord, mu, mxhnl0, mxstp0
      integer len1, len1c, len1n, len1s, len2, leniwc,
     1   lenrwc, lenrwn, lenrws
      double precision rowns,
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision tsw, rowns2, pdnorm
      double precision atoli, ayi, big, ewti, h0, hmax, hmx, rh, rtoli,
     1   tcrit, tdist, tnext, tol, tolsf, tp, size, sum, w0,
     2   d1mach, vmnorm
      dimension mord(2)
      logical ihit
c-----------------------------------------------------------------------
c the following two internal common blocks contain
c (a) variables which are local to any subroutine but whose values must
c     be preserved between calls to the routine (own variables), and
c (b) variables which are communicated between subroutines.
c the structure of each block is as follows..  all real variables are
c listed first, followed by all integers.  within each type, the
c variables are grouped with those local to subroutine lsoda first,
c then those local to subroutine stoda, and finally those used
c for communication.  the block ls0001 is declared in subroutines
c lsoda, intdy, stoda, prja, and solsy.  the block lsa001 is declared
c in subroutines lsoda, stoda, and prja.  groups of variables are
c replaced by dummy arrays in the common declarations in routines
c where those variables are not used.
c-----------------------------------------------------------------------
      common /ls0001/ rowns(209),
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround,
     2   illin, init, lyh, lewt, lacor, lsavf, lwm, liwm,
     3   mxstep, mxhnil, nhnil, ntrep, nslast, nyh, iowns(6),
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      common /lsa001/ tsw, rowns2(20), pdnorm,
     1   insufr, insufi, ixpr, iowns2(2), jtyp, mused, mxordn, mxords
c
      data mord(1),mord(2)/12,5/, mxstp0/500/, mxhnl0/10/
c-----------------------------------------------------------------------
c block a.
c this code block is executed on every call.
c it tests istate and itask for legality and branches appropriately.
c if istate .gt. 1 but the flag init shows that initialization has
c not yet been done, an error return occurs.
c if istate = 1 and tout = t, jump to block g and return immediately.
c-----------------------------------------------------------------------
      if (istate .lt. 1 .or. istate .gt. 3) go to 601
      if (itask .lt. 1 .or. itask .gt. 5) go to 602
      if (istate .eq. 1) go to 10
      if (init .eq. 0) go to 603
      if (istate .eq. 2) go to 200
      go to 20
 10   init = 0
      if (tout .eq. t) go to 430
 20   ntrep = 0
c-----------------------------------------------------------------------
c block b.
c the next code block is executed for the initial call (istate = 1),
c or for a continuation call with parameter changes (istate = 3).
c it contains checking of all inputs and various initializations.
c
c first check legality of the non-optional inputs neq, itol, iopt,
c jt, ml, and mu.
c-----------------------------------------------------------------------
      if (neq(1) .le. 0) go to 604
      if (istate .eq. 1) go to 25
      if (neq(1) .gt. n) go to 605
 25   n = neq(1)
      if (itol .lt. 1 .or. itol .gt. 4) go to 606
      if (iopt .lt. 0 .or. iopt .gt. 1) go to 607
      if (jt .eq. 3 .or. jt .lt. 1 .or. jt .gt. 5) go to 608
      jtyp = jt
      if (jt .le. 2) go to 30
      ml = iwork(1)
      mu = iwork(2)
      if (ml .lt. 0 .or. ml .ge. n) go to 609
      if (mu .lt. 0 .or. mu .ge. n) go to 610
 30   continue
c next process and check the optional inputs. --------------------------
      if (iopt .eq. 1) go to 40
      ixpr = 0
      mxstep = mxstp0
      mxhnil = mxhnl0
      hmxi = 0.0d0
      hmin = 0.0d0
      if (istate .ne. 1) go to 60
      h0 = 0.0d0
      mxordn = mord(1)
      mxords = mord(2)
      go to 60
 40   ixpr = iwork(5)
      if (ixpr .lt. 0 .or. ixpr .gt. 1) go to 611
      mxstep = iwork(6)
      if (mxstep .lt. 0) go to 612
      if (mxstep .eq. 0) mxstep = mxstp0
      mxhnil = iwork(7)
      if (mxhnil .lt. 0) go to 613
      if (mxhnil .eq. 0) mxhnil = mxhnl0
      if (istate .ne. 1) go to 50
      h0 = rwork(5)
      mxordn = iwork(8)
      if (mxordn .lt. 0) go to 628
      if (mxordn .eq. 0) mxordn = 100
      mxordn = min0(mxordn,mord(1))
      mxords = iwork(9)
      if (mxords .lt. 0) go to 629
      if (mxords .eq. 0) mxords = 100
      mxords = min0(mxords,mord(2))
      if ((tout - t)*h0 .lt. 0.0d0) go to 614
 50   hmax = rwork(6)
      if (hmax .lt. 0.0d0) go to 615
      hmxi = 0.0d0
      if (hmax .gt. 0.0d0) hmxi = 1.0d0/hmax
      hmin = rwork(7)
      if (hmin .lt. 0.0d0) go to 616
c-----------------------------------------------------------------------
c set work array pointers and check lengths lrw and liw.
c if istate = 1, meth is initialized to 1 here to facilitate the
c checking of work space lengths.
c pointers to segments of rwork and iwork are named by prefixing l to
c the name of the segment.  e.g., the segment yh starts at rwork(lyh).
c segments of rwork (in order) are denoted  yh, wm, ewt, savf, acor.
c if the lengths provided are insufficient for the current method,
c an error return occurs.  this is treated as illegal input on the
c first call, but as a problem interruption with istate = -7 on a
c continuation call.  if the lengths are sufficient for the current
c method but not for both methods, a warning message is sent.
c-----------------------------------------------------------------------
 60   if (istate .eq. 1) meth = 1
      if (istate .eq. 1) nyh = n
      lyh = 21
      len1n = 20 + (mxordn + 1)*nyh
      len1s = 20 + (mxords + 1)*nyh
      lwm = len1s + 1
      if (jt .le. 2) lenwm = n*n + 2
      if (jt .ge. 4) lenwm = (2*ml + mu + 1)*n + 2
      len1s = len1s + lenwm
      len1c = len1n
      if (meth .eq. 2) len1c = len1s
      len1 = max0(len1n,len1s)
      len2 = 3*n
      lenrw = len1 + len2
      lenrwn = len1n + len2
      lenrws = len1s + len2
      lenrwc = len1c + len2
      iwork(17) = lenrw
      liwm = 1
      leniw = 20 + n
      leniwc = 20
      if (meth .eq. 2) leniwc = leniw
      iwork(18) = leniw
      if (istate .eq. 1 .and. lrw .lt. lenrwc) go to 617
      if (istate .eq. 1 .and. liw .lt. leniwc) go to 618
      if (istate .eq. 3 .and. lrw .lt. lenrwc) go to 550
      if (istate .eq. 3 .and. liw .lt. leniwc) go to 555
      lewt = len1 + 1
      insufr = 0
      if (lrw .ge. lenrw) go to 65
      insufr = 2
      lewt = len1c + 1
      call xerrwv(
     1  60hlsoda--  warning.. rwork length is sufficient for now, but  ,
     1   60, 103, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      may not be later.  integration will proceed anyway.   ,
     1   60, 103, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  50h      length needed is lenrw = i1, while lrw = i2.,
     1   50, 103, 0, 2, lenrw, lrw, 0, 0.0d0, 0.0d0)
 65   lsavf = lewt + n
      lacor = lsavf + n
      insufi = 0
      if (liw .ge. leniw) go to 70
      insufi = 2
      call xerrwv(
     1  60hlsoda--  warning.. iwork length is sufficient for now, but  ,
     1   60, 104, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      may not be later.  integration will proceed anyway.   ,
     1   60, 104, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  50h      length needed is leniw = i1, while liw = i2.,
     1   50, 104, 0, 2, leniw, liw, 0, 0.0d0, 0.0d0)
 70   continue
c check rtol and atol for legality. ------------------------------------
      rtoli = rtol(1)
      atoli = atol(1)
      do 75 i = 1,n
        if (itol .ge. 3) rtoli = rtol(i)
        if (itol .eq. 2 .or. itol .eq. 4) atoli = atol(i)
        if (rtoli .lt. 0.0d0) go to 619
        if (atoli .lt. 0.0d0) go to 620
 75     continue
      if (istate .eq. 1) go to 100
c if istate = 3, set flag to signal parameter changes to stoda. --------
      jstart = -1
      if (n .eq. nyh) go to 200
c neq was reduced.  zero part of yh to avoid undefined references. -----
      i1 = lyh + l*nyh
      i2 = lyh + (maxord + 1)*nyh - 1
      if (i1 .gt. i2) go to 200
      do 95 i = i1,i2
 95     rwork(i) = 0.0d0
      go to 200
c-----------------------------------------------------------------------
c block c.
c the next block is for the initial call only (istate = 1).
c it contains all remaining initializations, the initial call to f,
c and the calculation of the initial step size.
c the error weights in ewt are inverted after being loaded.
c-----------------------------------------------------------------------
 100  uround = d1mach(4)
      tn = t
      tsw = t
      maxord = mxordn
      if (itask .ne. 4 .and. itask .ne. 5) go to 110
      tcrit = rwork(1)
      if ((tcrit - tout)*(tout - t) .lt. 0.0d0) go to 625
      if (h0 .ne. 0.0d0 .and. (t + h0 - tcrit)*h0 .gt. 0.0d0)
     1   h0 = tcrit - t
 110  jstart = 0
      nhnil = 0
      nst = 0
      nje = 0
      nslast = 0
      hu = 0.0d0
      nqu = 0
      mused = 0
      miter = 0
      ccmax = 0.3d0
      maxcor = 3
      msbp = 20
      mxncf = 10
c initial call to f.  (lf0 points to yh(*,2).) -------------------------
      lf0 = lyh + nyh
      call f (neq, t, y, rwork(lf0))
      nfe = 1
c load the initial value vector in yh. ---------------------------------
      do 115 i = 1,n
 115    rwork(i+lyh-1) = y(i)
c load and invert the ewt array.  (h is temporarily set to 1.0.) -------
      nq = 1
      h = 1.0d0
      call ewset (n, itol, rtol, atol, rwork(lyh), rwork(lewt))
      do 120 i = 1,n
        if (rwork(i+lewt-1) .le. 0.0d0) go to 621
 120    rwork(i+lewt-1) = 1.0d0/rwork(i+lewt-1)
c-----------------------------------------------------------------------
c the coding below computes the step size, h0, to be attempted on the
c first step, unless the user has supplied a value for this.
c first check that tout - t differs significantly from zero.
c a scalar tolerance quantity tol is computed, as max(rtol(i))
c if this is positive, or max(atol(i)/abs(y(i))) otherwise, adjusted
c so as to be between 100*uround and 1.0e-3.
c then the computed value h0 is given by..
c
c   h0**(-2)  =  1./(tol * w0**2)  +  tol * (norm(f))**2
c
c where   w0     = max ( abs(t), abs(tout) ),
c         f      = the initial value of the vector f(t,y), and
c         norm() = the weighted vector norm used throughout, given by
c                  the vmnorm function routine, and weighted by the
c                  tolerances initially loaded into the ewt array.
c the sign of h0 is inferred from the initial values of tout and t.
c abs(h0) is made .le. abs(tout-t) in any case.
c-----------------------------------------------------------------------
      if (h0 .ne. 0.0d0) go to 180
      tdist = dabs(tout - t)
      w0 = dmax1(dabs(t),dabs(tout))
      if (tdist .lt. 2.0d0*uround*w0) go to 622
      tol = rtol(1)
      if (itol .le. 2) go to 140
      do 130 i = 1,n
 130    tol = dmax1(tol,rtol(i))
 140  if (tol .gt. 0.0d0) go to 160
      atoli = atol(1)
      do 150 i = 1,n
        if (itol .eq. 2 .or. itol .eq. 4) atoli = atol(i)
        ayi = dabs(y(i))
        if (ayi .ne. 0.0d0) tol = dmax1(tol,atoli/ayi)
 150    continue
 160  tol = dmax1(tol,100.0d0*uround)
      tol = dmin1(tol,0.001d0)
      sum = vmnorm (n, rwork(lf0), rwork(lewt))
      sum = 1.0d0/(tol*w0*w0) + tol*sum**2
      h0 = 1.0d0/dsqrt(sum)
      h0 = dmin1(h0,tdist)
      h0 = dsign(h0,tout-t)
c adjust h0 if necessary to meet hmax bound. ---------------------------
 180  rh = dabs(h0)*hmxi
      if (rh .gt. 1.0d0) h0 = h0/rh
c load h with h0 and scale yh(*,2) by h0. ------------------------------
      h = h0
      do 190 i = 1,n
 190    rwork(i+lf0-1) = h0*rwork(i+lf0-1)
      go to 270
c-----------------------------------------------------------------------
c block d.
c the next code block is for continuation calls only (istate = 2 or 3)
c and is to check stop conditions before taking a step.
c-----------------------------------------------------------------------
 200  nslast = nst
      go to (210, 250, 220, 230, 240), itask
 210  if ((tn - tout)*h .lt. 0.0d0) go to 250
      call intdy (tout, 0, rwork(lyh), nyh, y, iflag)
      if (iflag .ne. 0) go to 627
      t = tout
      go to 420
 220  tp = tn - hu*(1.0d0 + 100.0d0*uround)
      if ((tp - tout)*h .gt. 0.0d0) go to 623
      if ((tn - tout)*h .lt. 0.0d0) go to 250
      t = tn
      go to 400
 230  tcrit = rwork(1)
      if ((tn - tcrit)*h .gt. 0.0d0) go to 624
      if ((tcrit - tout)*h .lt. 0.0d0) go to 625
      if ((tn - tout)*h .lt. 0.0d0) go to 245
      call intdy (tout, 0, rwork(lyh), nyh, y, iflag)
      if (iflag .ne. 0) go to 627
      t = tout
      go to 420
 240  tcrit = rwork(1)
      if ((tn - tcrit)*h .gt. 0.0d0) go to 624
 245  hmx = dabs(tn) + dabs(h)
      ihit = dabs(tn - tcrit) .le. 100.0d0*uround*hmx
      if (ihit) t = tcrit
      if (ihit) go to 400
      tnext = tn + h*(1.0d0 + 4.0d0*uround)
      if ((tnext - tcrit)*h .le. 0.0d0) go to 250
      h = (tcrit - tn)*(1.0d0 - 4.0d0*uround)
      if (istate .eq. 2 .and. jstart .ge. 0) jstart = -2
c-----------------------------------------------------------------------
c block e.
c the next block is normally executed for all calls and contains
c the call to the one-step core integrator stoda.
c
c this is a looping point for the integration steps.
c
c first check for too many steps being taken, update ewt (if not at
c start of problem), check for too much accuracy being requested, and
c check for h below the roundoff level in t.
c-----------------------------------------------------------------------
 250  continue
      if (meth .eq. mused) go to 255
      if (insufr .eq. 1) go to 550
      if (insufi .eq. 1) go to 555
 255  if ((nst-nslast) .ge. mxstep) go to 500
      call ewset (n, itol, rtol, atol, rwork(lyh), rwork(lewt))
      do 260 i = 1,n
        if (rwork(i+lewt-1) .le. 0.0d0) go to 510
 260    rwork(i+lewt-1) = 1.0d0/rwork(i+lewt-1)
 270  tolsf = uround*vmnorm (n, rwork(lyh), rwork(lewt))
      if (tolsf .le. 0.01d0) go to 280
      tolsf = tolsf*200.0d0
      if (nst .eq. 0) go to 626
      go to 520
 280  if ((tn + h) .ne. tn) go to 290
      nhnil = nhnil + 1
      if (nhnil .gt. mxhnil) go to 290
      call xerrwv(50hlsoda--  warning..internal t (=r1) and h (=r2) are,
     1   50, 101, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      such that in the machine, t + h = t on the next step  ,
     1   60, 101, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      (h = step size). solver will continue anyway,
     1   50, 101, 0, 0, 0, 0, 2, tn, h)
      if (nhnil .lt. mxhnil) go to 290
      call xerrwv(50hlsoda--  above warning has been issued i1 times.  ,
     1   50, 102, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      it will not be issued again for this problem,
     1   50, 102, 0, 1, mxhnil, 0, 0, 0.0d0, 0.0d0)
 290  continue
c-----------------------------------------------------------------------
c     call stoda(neq,y,yh,nyh,yh,ewt,savf,acor,wm,iwm,f,jac,prja,solsy)
c-----------------------------------------------------------------------
      call stoda (neq, y, rwork(lyh), nyh, rwork(lyh), rwork(lewt),
     1   rwork(lsavf), rwork(lacor), rwork(lwm), iwork(liwm),
     2   f, jac, prja, solsy)
      kgo = 1 - kflag
      go to (300, 530, 540), kgo
c-----------------------------------------------------------------------
c block f.
c the following block handles the case of a successful return from the
c core integrator (kflag = 0).
c if a method switch was just made, record tsw, reset maxord,
c set jstart to -1 to signal stoda to complete the switch,
c and do extra printing of data if ixpr = 1.
c then, in any case, check for stop conditions.
c-----------------------------------------------------------------------
 300  init = 1
      if (meth .eq. mused) go to 310
      tsw = tn
      maxord = mxordn
      if (meth .eq. 2) maxord = mxords
      if (meth .eq. 2) rwork(lwm) = dsqrt(uround)
      insufr = min0(insufr,1)
      insufi = min0(insufi,1)
      jstart = -1
      if (ixpr .eq. 0) go to 310
      if (meth .eq. 2) call xerrwv(
     1  60hlsoda-- a switch to the bdf (stiff) method has occurred     ,
     1   60, 105, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      if (meth .eq. 1) call xerrwv(
     1  60hlsoda-- a switch to the adams (nonstiff) method has occurred,
     1   60, 106, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h     at t = r1,  tentative step size h = r2,  step nst = i1 ,
     1   60, 107, 0, 1, nst, 0, 2, tn, h)
 310  go to (320, 400, 330, 340, 350), itask
c itask = 1.  if tout has been reached, interpolate. -------------------
 320  if ((tn - tout)*h .lt. 0.0d0) go to 250
      call intdy (tout, 0, rwork(lyh), nyh, y, iflag)
      t = tout
      go to 420
c itask = 3.  jump to exit if tout was reached. ------------------------
 330  if ((tn - tout)*h .ge. 0.0d0) go to 400
      go to 250
c itask = 4.  see if tout or tcrit was reached.  adjust h if necessary.
 340  if ((tn - tout)*h .lt. 0.0d0) go to 345
      call intdy (tout, 0, rwork(lyh), nyh, y, iflag)
      t = tout
      go to 420
 345  hmx = dabs(tn) + dabs(h)
      ihit = dabs(tn - tcrit) .le. 100.0d0*uround*hmx
      if (ihit) go to 400
      tnext = tn + h*(1.0d0 + 4.0d0*uround)
      if ((tnext - tcrit)*h .le. 0.0d0) go to 250
      h = (tcrit - tn)*(1.0d0 - 4.0d0*uround)
      if (jstart .ge. 0) jstart = -2
      go to 250
c itask = 5.  see if tcrit was reached and jump to exit. ---------------
 350  hmx = dabs(tn) + dabs(h)
      ihit = dabs(tn - tcrit) .le. 100.0d0*uround*hmx
c-----------------------------------------------------------------------
c block g.
c the following block handles all successful returns from lsoda.
c if itask .ne. 1, y is loaded from yh and t is set accordingly.
c istate is set to 2, the illegal input counter is zeroed, and the
c optional outputs are loaded into the work arrays before returning.
c if istate = 1 and tout = t, there is a return with no action taken,
c except that if this has happened repeatedly, the run is terminated.
c-----------------------------------------------------------------------
 400  do 410 i = 1,n
 410    y(i) = rwork(i+lyh-1)
      t = tn
      if (itask .ne. 4 .and. itask .ne. 5) go to 420
      if (ihit) t = tcrit
 420  istate = 2
      illin = 0
      rwork(11) = hu
      rwork(12) = h
      rwork(13) = tn
      rwork(15) = tsw
      iwork(11) = nst
      iwork(12) = nfe
      iwork(13) = nje
      iwork(14) = nqu
      iwork(15) = nq
      iwork(19) = mused
      iwork(20) = meth
      return
c
 430  ntrep = ntrep + 1
      if (ntrep .lt. 5) return
      call xerrwv(
     1  60hlsoda--  repeated calls with istate = 1 and tout = t (=r1)  ,
     1   60, 301, 0, 0, 0, 0, 1, t, 0.0d0)
      go to 800
c-----------------------------------------------------------------------
c block h.
c the following block handles all unsuccessful returns other than
c those for illegal input.  first the error message routine is called.
c if there was an error test or convergence test failure, imxer is set.
c then y is loaded from yh, t is set to tn, and the illegal input
c counter illin is set to 0.  the optional outputs are loaded into
c the work arrays before returning.
c-----------------------------------------------------------------------
c the maximum number of steps was taken before reaching tout. ----------
 500  call xerrwv(50hlsoda--  at current t (=r1), mxstep (=i1) steps   ,
     1   50, 201, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      taken on this call before reaching tout     ,
     1   50, 201, 0, 1, mxstep, 0, 1, tn, 0.0d0)
      istate = -1
      go to 580
c ewt(i) .le. 0.0 for some i (not at start of problem). ----------------
 510  ewti = rwork(lewt+i-1)
      call xerrwv(50hlsoda--  at t (=r1), ewt(i1) has become r2 .le. 0.,
     1   50, 202, 0, 1, i, 0, 2, tn, ewti)
      istate = -6
      go to 580
c too much accuracy requested for machine precision. -------------------
 520  call xerrwv(50hlsoda--  at t (=r1), too much accuracy requested  ,
     1   50, 203, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      for precision of machine..  see tolsf (=r2) ,
     1   50, 203, 0, 0, 0, 0, 2, tn, tolsf)
      rwork(14) = tolsf
      istate = -2
      go to 580
c kflag = -1.  error test failed repeatedly or with abs(h) = hmin. -----
 530  call xerrwv(50hlsoda--  at t(=r1) and step size h(=r2), the error,
     1   50, 204, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      test failed repeatedly or with abs(h) = hmin,
     1   50, 204, 0, 0, 0, 0, 2, tn, h)
      istate = -4
      go to 560
c kflag = -2.  convergence failed repeatedly or with abs(h) = hmin. ----
 540  call xerrwv(50hlsoda--  at t (=r1) and step size h (=r2), the    ,
     1   50, 205, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(50h      corrector convergence failed repeatedly     ,
     1   50, 205, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(30h      or with abs(h) = hmin   ,
     1   30, 205, 0, 0, 0, 0, 2, tn, h)
      istate = -5
      go to 560
c rwork length too small to proceed. -----------------------------------
 550  call xerrwv(50hlsoda--  at current t(=r1), rwork length too small,
     1   50, 206, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      to proceed.  the integration was otherwise successful.,
     1   60, 206, 0, 0, 0, 0, 1, tn, 0.0d0)
      istate = -7
      go to 580
c iwork length too small to proceed. -----------------------------------
 555  call xerrwv(50hlsoda--  at current t(=r1), iwork length too small,
     1   50, 207, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      to proceed.  the integration was otherwise successful.,
     1   60, 207, 0, 0, 0, 0, 1, tn, 0.0d0)
      istate = -7
      go to 580
c compute imxer if relevant. -------------------------------------------
 560  big = 0.0d0
      imxer = 1
      do 570 i = 1,n
        size = dabs(rwork(i+lacor-1)*rwork(i+lewt-1))
        if (big .ge. size) go to 570
        big = size
        imxer = i
 570    continue
      iwork(16) = imxer
c set y vector, t, illin, and optional outputs. ------------------------
 580  do 590 i = 1,n
 590    y(i) = rwork(i+lyh-1)
      t = tn
      illin = 0
      rwork(11) = hu
      rwork(12) = h
      rwork(13) = tn
      rwork(15) = tsw
      iwork(11) = nst
      iwork(12) = nfe
      iwork(13) = nje
      iwork(14) = nqu
      iwork(15) = nq
      iwork(19) = mused
      iwork(20) = meth
      return
c-----------------------------------------------------------------------
c block i.
c the following block handles all error returns due to illegal input
c (istate = -3), as detected before calling the core integrator.
c first the error message routine is called.  then if there have been
c 5 consecutive such returns just before this call to the solver,
c the run is halted.
c-----------------------------------------------------------------------
 601  call xerrwv(30hlsoda--  istate (=i1) illegal ,
     1   30, 1, 0, 1, istate, 0, 0, 0.0d0, 0.0d0)
      go to 700
 602  call xerrwv(30hlsoda--  itask (=i1) illegal  ,
     1   30, 2, 0, 1, itask, 0, 0, 0.0d0, 0.0d0)
      go to 700
 603  call xerrwv(50hlsoda--  istate .gt. 1 but lsoda not initialized  ,
     1   50, 3, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      go to 700
 604  call xerrwv(30hlsoda--  neq (=i1) .lt. 1     ,
     1   30, 4, 0, 1, neq(1), 0, 0, 0.0d0, 0.0d0)
      go to 700
 605  call xerrwv(50hlsoda--  istate = 3 and neq increased (i1 to i2)  ,
     1   50, 5, 0, 2, n, neq(1), 0, 0.0d0, 0.0d0)
      go to 700
 606  call xerrwv(30hlsoda--  itol (=i1) illegal   ,
     1   30, 6, 0, 1, itol, 0, 0, 0.0d0, 0.0d0)
      go to 700
 607  call xerrwv(30hlsoda--  iopt (=i1) illegal   ,
     1   30, 7, 0, 1, iopt, 0, 0, 0.0d0, 0.0d0)
      go to 700
 608  call xerrwv(30hlsoda--  jt (=i1) illegal     ,
     1   30, 8, 0, 1, jt, 0, 0, 0.0d0, 0.0d0)
      go to 700
 609  call xerrwv(50hlsoda--  ml (=i1) illegal.. .lt.0 or .ge.neq (=i2),
     1   50, 9, 0, 2, ml, neq(1), 0, 0.0d0, 0.0d0)
      go to 700
 610  call xerrwv(50hlsoda--  mu (=i1) illegal.. .lt.0 or .ge.neq (=i2),
     1   50, 10, 0, 2, mu, neq(1), 0, 0.0d0, 0.0d0)
      go to 700
 611  call xerrwv(30hlsoda--  ixpr (=i1) illegal   ,
     1   30, 11, 0, 1, ixpr, 0, 0, 0.0d0, 0.0d0)
      go to 700
 612  call xerrwv(30hlsoda--  mxstep (=i1) .lt. 0  ,
     1   30, 12, 0, 1, mxstep, 0, 0, 0.0d0, 0.0d0)
      go to 700
 613  call xerrwv(30hlsoda--  mxhnil (=i1) .lt. 0  ,
     1   30, 13, 0, 1, mxhnil, 0, 0, 0.0d0, 0.0d0)
      go to 700
 614  call xerrwv(40hlsoda--  tout (=r1) behind t (=r2)      ,
     1   40, 14, 0, 0, 0, 0, 2, tout, t)
      call xerrwv(50h      integration direction is given by h0 (=r1)  ,
     1   50, 14, 0, 0, 0, 0, 1, h0, 0.0d0)
      go to 700
 615  call xerrwv(30hlsoda--  hmax (=r1) .lt. 0.0  ,
     1   30, 15, 0, 0, 0, 0, 1, hmax, 0.0d0)
      go to 700
 616  call xerrwv(30hlsoda--  hmin (=r1) .lt. 0.0  ,
     1   30, 16, 0, 0, 0, 0, 1, hmin, 0.0d0)
      go to 700
 617  call xerrwv(
     1  60hlsoda--  rwork length needed, lenrw (=i1), exceeds lrw (=i2),
     1   60, 17, 0, 2, lenrw, lrw, 0, 0.0d0, 0.0d0)
      go to 700
 618  call xerrwv(
     1  60hlsoda--  iwork length needed, leniw (=i1), exceeds liw (=i2),
     1   60, 18, 0, 2, leniw, liw, 0, 0.0d0, 0.0d0)
      go to 700
 619  call xerrwv(40hlsoda--  rtol(i1) is r1 .lt. 0.0        ,
     1   40, 19, 0, 1, i, 0, 1, rtoli, 0.0d0)
      go to 700
 620  call xerrwv(40hlsoda--  atol(i1) is r1 .lt. 0.0        ,
     1   40, 20, 0, 1, i, 0, 1, atoli, 0.0d0)
      go to 700
 621  ewti = rwork(lewt+i-1)
      call xerrwv(40hlsoda--  ewt(i1) is r1 .le. 0.0         ,
     1   40, 21, 0, 1, i, 0, 1, ewti, 0.0d0)
      go to 700
 622  call xerrwv(
     1  60hlsoda--  tout (=r1) too close to t(=r2) to start integration,
     1   60, 22, 0, 0, 0, 0, 2, tout, t)
      go to 700
 623  call xerrwv(
     1  60hlsoda--  itask = i1 and tout (=r1) behind tcur - hu (= r2)  ,
     1   60, 23, 0, 1, itask, 0, 2, tout, tp)
      go to 700
 624  call xerrwv(
     1  60hlsoda--  itask = 4 or 5 and tcrit (=r1) behind tcur (=r2)   ,
     1   60, 24, 0, 0, 0, 0, 2, tcrit, tn)
      go to 700
 625  call xerrwv(
     1  60hlsoda--  itask = 4 or 5 and tcrit (=r1) behind tout (=r2)   ,
     1   60, 25, 0, 0, 0, 0, 2, tcrit, tout)
      go to 700
 626  call xerrwv(50hlsoda--  at start of problem, too much accuracy   ,
     1   50, 26, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
      call xerrwv(
     1  60h      requested for precision of machine..  see tolsf (=r1) ,
     1   60, 26, 0, 0, 0, 0, 1, tolsf, 0.0d0)
      rwork(14) = tolsf
      go to 700
 627  call xerrwv(50hlsoda--  trouble from intdy. itask = i1, tout = r1,
     1   50, 27, 0, 1, itask, 0, 1, tout, 0.0d0)
      go to 700
 628  call xerrwv(30hlsoda--  mxordn (=i1) .lt. 0  ,
     1   30, 28, 0, 1, mxordn, 0, 0, 0.0d0, 0.0d0)
      go to 700
 629  call xerrwv(30hlsoda--  mxords (=i1) .lt. 0  ,
     1   30, 29, 0, 1, mxords, 0, 0, 0.0d0, 0.0d0)
c
 700  if (illin .eq. 5) go to 710
      illin = illin + 1
      istate = -3
      return
 710  call xerrwv(50hlsoda--  repeated occurrences of illegal input    ,
     1   50, 302, 0, 0, 0, 0, 0, 0.0d0, 0.0d0)
c
 800  call xerrwv(50hlsoda--  run aborted.. apparent infinite loop     ,
     1   50, 303, 2, 0, 0, 0, 0, 0.0d0, 0.0d0)
      return
c----------------------- end of subroutine lsoda -----------------------
      end
*
*
      SUBROUTINE MOAFEC(L,IGROB,I,IHETER,INDIO)
*     ===================================
*
*     ******************************************************************
*     MOAFEC : MOgador AFfEctation des variables locales
*              aux valeurs contenues dans les grands tableaux
*              ou l'inverse selon la valeur de INDIO
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogent.inc'
* essaiLOC
      INCLUDE 'cmogloc.inc'
      INCLUDE 'cmogtem.inc'
* essaiLOC
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
************************************************************************ 
* si INDIO = 1
* cmoginf.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie
* cmogdon.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie      
* cmogvar.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie 
* cmogsor.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie 
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR en entr�e
* cmogear.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie
* cmogphy.inc : xmo_PIZAHL en entree
* cmogppr.inc : xmo_TETBJ xmo_ATETBJ en entree
*               xmo_TETPORJ xmo_ATETPORJ en sortie
* essaiLOC
* cmogloc.inc : les tableaux sur le maillage du crayon en entr�e
*               les tableaux locaux en sortie
* cmogtem.inc : xmo_TEMPS en entr�e
* essaiLOC
*
*si INDIO = 2
* cmogvar.inc : les tableaux locaux en entr�e
*               les tableaux sur le maillage du crayon en sortie 
* cmogsor.inc : les tableaux locaux en entr�e
*               les tableaux sur le maillage du crayon en sortie 
* cmogear.inc : les tableaux locaux en entr�e
*               les tableaux sur le maillage du crayon en sortie
* essaiLOC
* cmogloc.inc : les tableaux locaux en entr�e
*               les tableaux sur le maillage du crayon en sortie
* essaiLOC
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
      CALL MOMESS('DANS MOAFEC')
*
*
*     ����������������������
      IF (INDIO .EQ. 1) THEN
*     ����������������������
*
* Pour les impressions 
      mo_IMPR1 = mo_IMPR_ANA(L,IGROB,I,1)
      mo_IMPR2 = mo_IMPR_ANA(L,IGROB,I,2)
      mo_IMPR3 = mo_IMPR_ANA(L,IGROB,I,3)
      mo_IMPR4 = mo_IMPR_ANA(L,IGROB,I,4)
      mo_IMPR5 = mo_IMPR_ANA(L,IGROB,I,5)
      mo_IMPRSYNT = mo_IMPR_ANA(L,IGROB,I,6)
*
      IF ( mo_NORDR_ANA(L,IGROB,I) .NE. 0 ) THEN
        mo_NRES1 = mo_NNRES1(mo_NORDR_ANA(L,IGROB,I))
        mo_NRES2 = mo_NNRES2(mo_NORDR_ANA(L,IGROB,I))
        mo_NRES3 = mo_NNRES3(mo_NORDR_ANA(L,IGROB,I))
        mo_NRES4 = mo_NNRES4(mo_NORDR_ANA(L,IGROB,I))
        mo_NRES5 = mo_NNRES5(mo_NORDR_ANA(L,IGROB,I))
        mo_NSYNT = mo_NNSYNT(mo_NORDR_ANA(L,IGROB,I))
      ENDIF
*
* Pour les donnees initiales
      xmo_AGG = xmo_AGG3(L,IGROB,I,IHETER)
      xmo_VTPORI = xmo_VTPORI3(L,IGROB,I,IHETER)
      xmo_VTPORJI = xmo_VTPORJI3(L,IGROB,I,IHETER)
      xmo_CPORI = xmo_CPORI3(L,IGROB,I,IHETER)
      xmo_CPORJI = xmo_CPORJI3(L,IGROB,I,IHETER)
      xmo_ATPOR = xmo_ATPOR3(L,IGROB,I,IHETER)
      xmo_ATPORJ = xmo_ATPORJ3(L,IGROB,I,IHETER)
      xmo_CXEPORI = xmo_CXEPORI3(L,IGROB,I,IHETER)
      xmo_CXEPORJI = xmo_CXEPORJI3(L,IGROB,I,IHETER)
      xmo_MAVOLI = xmo_MAVOLI3(L,IGROB,I,IHETER)
      mo_MATER = mo_MATER3(L,IGROB,I,IHETER)
*
      CALL MORONLEN(mo_PORJ_RON)
      IF (mo_PORJ_RON .EQ. 1) THEN
        xmo_TETPORJ = xmo_PIZAHL / 2.D0
        xmo_ATETPORJ = 1.D0
      ELSE
        xmo_TETPORJ = xmo_TETBJ
        xmo_ATETPORJ = xmo_ATETBJ
      ENDIF
*
*
* Pour les tableaux
*
      DO 10 IVGA = 1, mo_NVGAMX
        xmo_VG(IVGA,2) = xmo_VECG(L,IGROB,I,IHETER,IVGA,2)
 10   CONTINUE
*
*
      DO 20 IVDE = 1, mo_NVDEMX
        xmo_VD(IVDE,2) = xmo_VECD(L,IGROB,I,IHETER,IVDE,2)
 20   CONTINUE
*
      mo_IET = mo_IETCOM(L,IGROB,I,IHETER,2)
      mo_IET_APPR = mo_IETCOM_APPR(L,IGROB,I,IHETER,2)
      mo_PRFOIS = mo_PRFOIS3(L,IGROB,I,IHETER)
*
      DO NPOP = 1, mo_NPOPMX
        xmo_EQUADISP(NPOP) = 
     &               xmo_EQUADISP3(L,IGROB,I,IHETER,NPOP)
      ENDDO
*
      DO IS = 1 , 240
        xmo_RSAV(IS) = xmo_RSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , 50
        mo_ISAV(IS) = mo_ISAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , mo_LWRMAX
        xmo_G_RWORKSAV(IS) =
     &    xmo_G_RWORKSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , mo_LWIMAX
        mo_G_IWORKSAV(IS) = mo_G_IWORKSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , 5
        mo_G_IEQSAV(IS) = mo_G_IEQSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , mo_NEQMAX
        xmo_G_RTOLSAV(IS) = xmo_G_RTOLSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , mo_NVGAMX
        mo_IGTABSAV(IS) = mo_IGTABSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
      DO IS = 1 , mo_NVDEMX
        mo_IDTABSAV(IS) = mo_IDTABSAV3(L,IGROB,I,IHETER,IS)
      ENDDO
*
* essaiLOC
      DO IVGA = 1, mo_NNSPHMX
        xmo_CXERPORLOC0(IVGA) = 
     &                      xmo_CXERPORLOC03(L,IGROB,I,IHETER,IVGA)
      ENDDO
      xmo_TEMPS0 = xmo_TEMPS
* essaiLOC
*
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
      CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
* Initialisation de mo_NVG et mo_NVD pour le point consid�r�
*
      mo_CH12 = 2
      IF (mo_IET .EQ. 0) THEN 
        mo_NVG = mo_CH12*mo_NGG + 3 + mo_NSEQ_C + 3
        mo_NVD = 7
      ELSEIF  (mo_IET .EQ. 2) THEN
        mo_NVG = mo_CH12*mo_NGG + 3 + mo_NSEQ_C + 3 + mo_NSEQR + 3 
        mo_NVD = 10
      ELSEIF  (mo_IET .EQ. 3) THEN
        mo_NVG = mo_NSEQR + 3 + 3
        mo_NVD = 3
      ENDIF
*
      IF (mo_IET .EQ. 2 .OR. mo_IET .EQ. 3) THEN
        xmo_ASEB = xmo_ASEBASC(L,IGROB,I,IHETER)
      ENDIF
*
      IF ( mo_IET .EQ. 0 ) THEN
        xmo_GCREE = xmo_GCREA3(L,IGROB,I,IHETER,2)
        xmo_REMISOL = xmo_REMISOL3(L,IGROB,I,IHETER,2)
        xmo_SORDIF = xmo_SORDIF3(L,IGROB,I,IHETER,2)
        xmo_SORBUL = xmo_SORBUL3(L,IGROB,I,IHETER,2)
        xmo_SORPOR = xmo_SORPOR3(L,IGROB,I,IHETER,2)
        xmo_SORDIFJ = xmo_SORDIFJ3(L,IGROB,I,IHETER,2)
        xmo_SORPERC = xmo_SORPERC3(L,IGROB,I,IHETER,2)
        xmo_SOREJEC = xmo_SOREJEC3(L,IGROB,I,IHETER,2)
        xmo_SOMTOFIS(2) = 
     &      xmo_SOMTOFIS3(L,IGROB,I,IHETER,2)
        xmo_TAUXBETAC(2) = 
     &      xmo_TAUXBETAC3(L,IGROB,I,IHETER,2)
        xmo_TAUXSORBUL(2) = 
     &      xmo_TAUXSORBUL3(L,IGROB,I,IHETER,2)
        xmo_TAUXSORPOR(2) = 
     &      xmo_TAUXSORPOR3(L,IGROB,I,IHETER,2)
        xmo_TAUXREMISOL(2) = 
     &      xmo_TAUXREMISOL3(L,IGROB,I,IHETER,2)
        xmo_TAUXSOURXE(2) = 
     &      xmo_TAUXSOURXE3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTSE(2) = 
     &      xmo_TAUXOUTSE3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTPERC(2) = 
     &      xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTEJEC(2) = 
     &      xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,2)
      ELSEIF (mo_IET .EQ. 2 .OR. mo_IET .EQ. 3)  THEN
        xmo_GCREE = xmo_GCREA3(L,IGROB,I,IHETER,2)
        xmo_SORDIFJ = xmo_SORDIFJ3(L,IGROB,I,IHETER,2)
        xmo_SORPERC = xmo_SORPERC3(L,IGROB,I,IHETER,2)
        xmo_SOREJEC = xmo_SOREJEC3(L,IGROB,I,IHETER,2)
        xmo_SOMTOFIS(2) = 
     &      xmo_SOMTOFIS3(L,IGROB,I,IHETER,2)
        xmo_TAUXBETAC(2) = 
     &      xmo_TAUXBETAC3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTSE(2) = 
     &      xmo_TAUXOUTSE3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTPERC(2) = 
     &      xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,2)
        xmo_TAUXOUTEJEC(2) = 
     &      xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,2)
      ELSE
       WRITE(mo_DEBUG,*) 
     &    'mo_IET doit valoir 0 2 ou 3 dans moafec'
       STOP
      ENDIF
*
*     �����
      ENDIF
*     �����
*
*
*     $$$$$$$$$$$$$$$$$$$$$$
      IF (INDIO .EQ. 2) THEN
*     $$$$$$$$$$$$$$$$$$$$$$
*
      DO 30 IVGA = 1, mo_NVGAMX
        xmo_VECG(L,IGROB,I,IHETER,IVGA,1) = xmo_VG(IVGA,1)
 30   CONTINUE
*
      DO 40 IVDE = 1, mo_NVDEMX
        xmo_VECD(L,IGROB,I,IHETER,IVDE,1) = xmo_VD(IVDE,1) 
 40   CONTINUE
*
      mo_IETCOM(L,IGROB,I,IHETER,1) = mo_IET
      mo_IETCOM_APPR(L,IGROB,I,IHETER,1) = mo_IET_APPR
      mo_PRFOIS3(L,IGROB,I,IHETER) = mo_PRFOIS
*
      DO NPOP = 1, mo_NPOPMX
        xmo_EQUADISP3(L,IGROB,I,IHETER,NPOP) =
     &         xmo_EQUADISP(NPOP)   
      ENDDO
*
      DO IS = 1 , 240
        xmo_RSAV3(L,IGROB,I,IHETER,IS) = xmo_RSAV(IS)
      ENDDO
*
      DO IS = 1 , 50
        mo_ISAV3(L,IGROB,I,IHETER,IS) = mo_ISAV(IS)
      ENDDO
*
      DO IS = 1 , mo_LWRMAX
        xmo_G_RWORKSAV3(L,IGROB,I,IHETER,IS) = 
     &     xmo_G_RWORKSAV(IS)
      ENDDO
*
      DO IS = 1 , mo_LWIMAX
        mo_G_IWORKSAV3(L,IGROB,I,IHETER,IS) = mo_G_IWORKSAV(IS)
      ENDDO
*
      DO IS = 1 , 5
        mo_G_IEQSAV3(L,IGROB,I,IHETER,IS) = mo_G_IEQSAV(IS) 
      ENDDO
*
      DO IS = 1 , mo_NEQMAX
        xmo_G_RTOLSAV3(L,IGROB,I,IHETER,IS) = xmo_G_RTOLSAV(IS)
      ENDDO
*
      DO IS = 1 , mo_NVGAMX
        mo_IGTABSAV3(L,IGROB,I,IHETER,IS) = mo_IGTABSAV(IS)
      ENDDO
*
      DO IS = 1 , mo_NVDEMX
        mo_IDTABSAV3(L,IGROB,I,IHETER,IS) = mo_IDTABSAV(IS)
      ENDDO
*
*
* essaiLOC
      DO IVGA = 1, mo_NNSPHMX
        xmo_CXERPORLOC03(L,IGROB,I,IHETER,IVGA) = 
     &                                         xmo_CXERPORLOC0(IVGA)
      ENDDO
* essaiLOC
      IF ( mo_IET .EQ. 0 ) THEN
        xmo_GCREA3(L,IGROB,I,IHETER,1) = xmo_GCREE
        xmo_REMISOL3(L,IGROB,I,IHETER,1) = xmo_REMISOL
        xmo_SORDIF3(L,IGROB,I,IHETER,1) = xmo_SORDIF 
        xmo_SORBUL3(L,IGROB,I,IHETER,1) = xmo_SORBUL
        xmo_SORPOR3(L,IGROB,I,IHETER,1) = xmo_SORPOR
        xmo_SORDIFJ3(L,IGROB,I,IHETER,1) = xmo_SORDIFJ
        xmo_SORPERC3(L,IGROB,I,IHETER,1) = xmo_SORPERC
        xmo_SOREJEC3(L,IGROB,I,IHETER,1) = xmo_SOREJEC
        xmo_SOMTOFIS3(L,IGROB,I,IHETER,1) =  
     &      xmo_SOMTOFIS(1) 
        xmo_TAUXBETAC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXBETAC(1)
        xmo_TAUXSORBUL3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXSORBUL(1)
        xmo_TAUXSORPOR3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXSORPOR(1)
        xmo_TAUXREMISOL3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXREMISOL(1)
        xmo_TAUXSOURXE3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXSOURXE(1)
        xmo_TAUXOUTSE3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTSE(1)
        xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTPERC(1)
        xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTEJEC(1)
        xmo_ERRGLOB3(L,IGROB,I,IHETER) = 
     &      xmo_ERRGLOB
*
        xmo_ASE3(L,IGROB,I) =  xmo_ASE
*
      ELSEIF (mo_IET .EQ. 2 .OR. mo_IET .EQ. 3)  THEN
* Stockage dans xmo_ASE3 ne sert que pour 
* mocaff appel� par mowmac

        xmo_ASE3(L,IGROB,I) = xmo_ASERIM
*
        xmo_ASEBASC(L,IGROB,I,IHETER) = xmo_ASEB
        xmo_GCREA3(L,IGROB,I,IHETER,1) = xmo_GCREE
        xmo_SORDIFJ3(L,IGROB,I,IHETER,1) = xmo_SORDIFJ
        xmo_SORPERC3(L,IGROB,I,IHETER,1) = xmo_SORPERC
        xmo_SOREJEC3(L,IGROB,I,IHETER,1) = xmo_SOREJEC
        xmo_SOMTOFIS3(L,IGROB,I,IHETER,1) =  
     &      xmo_SOMTOFIS(1)  
        xmo_TAUXBETAC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXBETAC(1) 
        xmo_TAUXOUTSE3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTSE(1)
        xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTPERC(1)
        xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,1) = 
     &      xmo_TAUXOUTEJEC(1)
        xmo_ERRGLOB3(L,IGROB,I,IHETER) = 
     &      xmo_ERRGLOB
      ELSE
        WRITE(mo_DEBUG,*) 
     &    'mo_IET doit valoir 0 2 ou 3 dans moafec'
        STOP
      ENDIF
*
*     $$$$$
      ENDIF
*     $$$$$
*
*
CDEBUG          IF ( L .EQ. mo_LSUIVI .AND. 
CDEBUG     &          IGROB .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          I .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans moafec'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
*
*
      RETURN
      END
      SUBROUTINE MOASER(xmo_FR)
C     =================
C
C
C     ******************************************************************
C     MOASER : MOgador calcul du rayon de la sph�re �quivalente
C              (ASErim) du combustible Restructur� 
C     mo_MODASERIM = 0  xmo_ASERIM = xmo_ASE transmis par le code cible
C                       ou lu dans le jeu de donn�e pour le 
C                       fonctionnement en autonome  
C     mo_MODASERIM = 1  S/V = 3/xmo_ASERIM passe progressivement au cours de 
C                       la restructuration de sa valeur (3/xmo_ASEB = 3/xmo_ASE)
C                       au moment du basculement (0)-->(2), � une valeur 
C                       finale 3/xmo_ASERF
C     mo_MODASERIM = 2  xmo_ASERIM passe brutalement, d�s le moment du 
C                       basculement (0)-->(2), � une valeur xmo_ASERF                                                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogppr.inc : xmo_ASERF en entr�e
* cmogvar.inc : xmo_ASEB en entr�e, xmo_ASERIM en sortie
* cmogent.inc : xmo_ASE en entr�e
* cmoginf.inc : mo_DEBUG en entr�e
************************************************************************
* xmo_ASERF : valeur du rayon de la sph�re �quivalente une fois 
*             le combustible compl�tement restructur� (param�tre du
*             mod�le) (m)
* xmo_ASEB : valeur du rayon de la sph�re �quivalente au moment du 
*            basculement etat classique (0) vers etat restructur� (2) (m)
* xmo_ASERIM : valeur du rayon de la sph�re �quivalente � utiliser (m)
* xmo_FR : fraction volumique restructur�e ()
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOASER')
*
*
* Calcul du rayon de la sph�re �quivalente du combustible restructur�
*
*
      IF (mo_MODASERIM .EQ. 1) THEN
        xmo_SSURV_RIM = 3.D0 / xmo_ASEB * (1.D0 - xmo_FR) +
     &                  3.D0 / xmo_ASERF * xmo_FR
        xmo_ASERIM = 3.D0 / xmo_SSURV_RIM
      ELSEIF (mo_MODASERIM .EQ. 0) THEN
        xmo_ASERIM = xmo_ASE
      ELSEIF (mo_MODASERIM .EQ. 2) THEN
        xmo_ASERIM = xmo_ASERF 
      ELSE
        WRITE(mo_DEBUG,*) 'mo_MODASERIM doit valoir 0 1 ou 2'
        STOP
      ENDIF
*
      RETURN
      END
      SUBROUTINE MOATOL(I_NUL,xmo_G_ATOL_REF)
C     ============================================
C
C
C     ******************************************************************
C     MOATOL : MOgador Calcul de la tol�rance absolue ATOL
C              Utilis�e en cas de nullit� d'une variable y(I_NUL)                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR en entr�e 
* cmogvar.inc : mo_IET xmo_EQUADISP en entr�e 
* cmogpte.inc : xmo_EQUAG xmo_EQUAB xmo_EQUAP xmo_EQUAGJ 
*               xmo_EQUABJ xmo_EQUAPJ xmo_EQUARHOD en entr�e
* cmoginf.inc : mo_DEBUG en entree
* cmogppr.inc : mo_MODELE_DENSIFICATION en entree 
************************************************************************ 
*-----7--0---------0---------0---------0---------0---------0---------0--
*
C
C
*
      CALL MOMESS('DANS MOINEQ_GEAR')      
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C
***********************************************************************
*
***********************************************************************
* ETAT CLASSIQUE - CONNECTE (0)
***********************************************************************
       IF (mo_IET .EQ. 0) THEN
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
       CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
       IF ( xmo_EQUAG*xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NGG
           I_RECHERCHE = I 
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF
*
       IF ( xmo_EQUAB*xmo_EQUADISP(2) .EQ. 1.D0 ) THEN
* equation du gaz dissous au voisinnage des bulles
         IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
           DO I = 1,mo_NGG
             I_RECHERCHE = I_RECHERCHE + 1
             xmo_G_ATOL_REF = 1.e-8
             IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
           ENDDO
         ENDIF
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
       IF (xmo_EQUAGJ*xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         I_RECHERCHE_DEP =  I_RECHERCHE
         DO I = 1,mo_NSEQ_C
           I_RECHERCHE = I_RECHERCHE_DEP + I
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF
*
       IF (xmo_EQUABJ*xmo_EQUADISP(4) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
       IF (xmo_EQUAP*xmo_EQUADISP(5) .EQ. 1.D0 ) THEN
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           I_RECHERCHE = I_RECHERCHE + 1
           xmo_G_ATOL_REF = 1.e12         
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDIF
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           I_RECHERCHE = I_RECHERCHE + 1
           xmo_G_ATOL_REF = 1.e-16
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDIF
       ENDIF
*
       IF (xmo_EQUAPJ*xmo_EQUADISP(6) .EQ. 1.D0 ) THEN
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           I_RECHERCHE = I_RECHERCHE + 1
           xmo_G_ATOL_REF = 1.e12
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDIF
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           I_RECHERCHE = I_RECHERCHE + 1
           xmo_G_ATOL_REF = 1.e-16
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDIF
       ENDIF
*
       IF (xmo_EQUARHOD*xmo_EQUADISP(7) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e10
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
*
       ENDIF
***********************************************************************
* FIN ETAT CLASSIQUE - CONNECTE (0)
***********************************************************************
*
*
************************************************************************
* ETAT 'EN RESTRUCTURATION' (2)
***********************************************************************
       IF (mo_IET .EQ. 2) THEN
*
* equation du gaz dissous zones saines
       IF ( xmo_EQUAG*xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NGG
           I_RECHERCHE = I
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF 
*
* equation des bulles zones saines
       IF ( xmo_EQUAB*xmo_EQUADISP(2) .EQ. 1.D0 ) THEN
* equation du gaz dissous au voisinnage des bulles
         IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
           DO I = 1,mo_NGG
             I_RECHERCHE = I_RECHERCHE + 1
             xmo_G_ATOL_REF = 1.e-8
             IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
           ENDDO
         ENDIF
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equation du gaz dissous inter joints sains
       IF (xmo_EQUAGJ*xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         I_RECHERCHE_DEP = I_RECHERCHE
         DO I = 1,mo_NSEQ_C
           I_RECHERCHE = I_RECHERCHE_DEP + I
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF
*
* equations des bulles inter joints sains
       IF (xmo_EQUABJ*xmo_EQUADISP(4) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equation du gaz dissous zones restructur�es
       IF ( xmo_EQUADISP(5) .EQ. 1.D0 ) THEN
         I_RECHERCHE_DEP = I_RECHERCHE
         DO I = 1,mo_NSEQR
           I_RECHERCHE = I_RECHERCHE_DEP + I
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF
*
* equation des bulles de rim zones restructur�es
       IF (xmo_EQUADISP(6) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equations des pores zones saines
       IF (xmo_EQUAP*xmo_EQUADISP(7) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
*
       IF (xmo_EQUAPJ*xmo_EQUADISP(8) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equations des pores zones restructur�es
       IF (xmo_EQUAPJ*xmo_EQUADISP(9) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equations de la densit� de dislocation
       IF (xmo_EQUARHOD*xmo_EQUADISP(10) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e10
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
*
       ENDIF
***********************************************************************
* FIN ETAT 'EN RESTRUCTURATION' (2)
***********************************************************************
*
*
*
***********************************************************************
* ETAT RESTRUCTURE (3)
***********************************************************************
       IF (mo_IET .EQ. 3) THEN
* equation du gaz dissous zones restructur�es
       IF (xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NSEQR
           I_RECHERCHE  = I
           xmo_G_ATOL_REF = 1.e-8
           IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         ENDDO
       ENDIF
*
* equation des bulles de rim zones restructur�es
       IF (xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
* equations des pores zones restructur�es
       IF (xmo_EQUAPJ*xmo_EQUADISP(4) .EQ. 1.D0 ) THEN
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e12
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-11
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
         I_RECHERCHE = I_RECHERCHE + 1
         xmo_G_ATOL_REF = 1.e-16
         IF ( I_RECHERCHE .EQ. I_NUL ) GOTO 500
       ENDIF
*
*
       ENDIF
***********************************************************************
* FIN ETAT RESTRUCTURE (3)
***********************************************************************
*
 500   CONTINUE
*
       RETURN
       END
      SUBROUTINE MOATOP(L,IGROB,I,IHETER)
C     ===================================
C
C
C     ******************************************************************
C     MOATOP : MOgador calcul du nombre d'ATOmes initial dans les Pores
C              intra et inter                            
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogdon.inc : xmo_PREFRI xmo_TEMFRI, xmo_VTPORI
*               xmo_VTPORJI xmo_CPORI xmo_CPORJI en entr�e
*               xmo_ATPOR xmo_ATPORJ xmo_CXEPORI xmo_CXEPORJI en sortie
* cmogphy.inc : xmo_BOLTZ xmo_PIZAHL xmo_XNAVOG en entr�e 
* cmogppr.inc : xmo_COEF1_GAMMA xmo_COEF2_GAMMA xmo_ATETBJ 
*               xmo_TETBJ mo_MODELE_GAZPORE en entr�e
*               xmo_TETPORJ xmo_ATETPORJ en sortie
************************************************************************
* xmo_VTPORI : volume de pores intra initial / m3 de combustible  ()
* xmo_PREFRI : Pression de frittage (Pa)
* xmo_TEMFRI : Temp�rature de frittage (K)
* xmo_BOLTZ : constante de Boltzman (J/K)
* xmo_PIZAHL : nombre PI ()
* xmo_ATPOR : nombre d'atomes de gaz initial dans l'ensemble des 
*             pores intra / m3 de combustible (at/m3)
*
* xmo_VTPORJI : volume de pores inter initial / m3 de combustible  ()
* xmo_ATPORJ : nombre d'atomes de gaz initial dans l'ensemble des 
*              pores inter / m3 de combustible (at/m3)
* xmo_GAMMA_FRIT : Tension de surface � la temp�rature de frittage (J/m2)
*
* On utilise ici la loi des gaz parfaits
*
************************************************************************
*
      CALL MOMESS('DANS MOATOP')
*
* Utilisation des variables locales
      xmo_VTPORI = xmo_VTPORI3(L,IGROB,I,IHETER) 
      xmo_VTPORJI = xmo_VTPORJI3(L,IGROB,I,IHETER) 
      xmo_CPORI = xmo_CPORI3(L,IGROB,I,IHETER)
      xmo_CPORJI = xmo_CPORJI3(L,IGROB,I,IHETER)
*
      xmo_GAMMA_FRIT = xmo_COEF1_GAMMA + 
     &                 xmo_COEF2_GAMMA * ( xmo_TEMFRI(L) - 273.D0 )
*
      CALL MORONLEN(mo_PORJ_RON)
      IF (mo_PORJ_RON .EQ. 1) THEN
        xmo_TETPORJ = xmo_PIZAHL / 2.D0
        xmo_ATETPORJ = 1.D0
      ELSE
        xmo_TETPORJ = xmo_TETBJ
        xmo_ATETPORJ = xmo_ATETBJ
      ENDIF
*
      IF (xmo_CPORI .GT. 0.D0 .AND. xmo_VTPORI .GT. 0.D0) THEN
        xmo_RPORI =  
     &        (3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTPORI / xmo_CPORI)
     &         **(1.D0/3.D0)
*
        xmo_ATPOR = xmo_VTPORI  * 
     &              ( xmo_PREFRI(L) + 
     &                2.D0 * xmo_GAMMA_FRIT / xmo_RPORI ) / 
     &              xmo_BOLTZ / xmo_TEMFRI(L)
*
      ELSE
        xmo_ATPOR = 0.D0
      ENDIF
*
      IF (xmo_CPORJI .GT. 0.D0 .AND. xmo_VTPORJI .GT. 0.D0) THEN
        xmo_RPORJI =  
     &    ( 3.D0 / 4.D0 / xmo_PIZAHL / xmo_ATETPORJ 
     &      * xmo_VTPORJI / xmo_CPORJI )
     &     **(1.D0/3.D0)
*
        xmo_ATPORJ = xmo_VTPORJI  * 
     &            ( xmo_PREFRI(L)  + 
     &              2.D0 * xmo_GAMMA_FRIT * sin(xmo_TETPORJ)  
     &            / xmo_RPORJI) / xmo_BOLTZ / xmo_TEMFRI(L)
*
      ELSE
        xmo_ATPORJ = 0.D0
      ENDIF
*
      IF ( mo_MODELE_GAZPORE .EQ. 0 ) THEN
* Cas o� le gaz initial pr�sent dans les pores est trait� comme du X�non
        xmo_CXEPORI = xmo_ATPOR / xmo_XNAVOG
        xmo_CXEPORJI = xmo_ATPORJ / xmo_XNAVOG
        xmo_ATPOR = 0.D0
        xmo_ATPORJ = 0.D0
      ELSE
* Cas o� le gaz initial pr�sent dans les pores est trait� � part et reste
* toujours dans les pores 
        xmo_CXEPORI = 0.D0
        xmo_CXEPORJI = 0.D0
      ENDIF
*
*
* Initialisation du tableau xmo_CXEPORI3 xmo_CXEPORJI3 xmo_ATPOR3 xmo_ATPORJ3
*
      xmo_CXEPORI3(L,IGROB,I,IHETER) = xmo_CXEPORI
      xmo_CXEPORJI3(L,IGROB,I,IHETER) = xmo_CXEPORJI
      xmo_ATPOR3(L,IGROB,I,IHETER) = xmo_ATPOR
      xmo_ATPORJ3(L,IGROB,I,IHETER) = xmo_ATPORJ
*
*  
      RETURN
      END

      SUBROUTINE MOBASC_GEAR(mo_IB,N) 
C     =================
C
C
C     ******************************************************************
C     MOBASC : MOgador BASCulement d'�tat du combustible                 
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogppr.inc'
C
************************************************************************
* cmogvar.inc : mo_IET xmo_VG et xmo_VD en entr�e
*               mo_IET_APPR en entr�e et sortie
* cmogphy.inc : 
* cmogpnu.inc : mo_NGG en entr�e
* cmogppr.inc : xmo_RHOLIM  xmo_RHODL 
*               xmo_MARGRHO_APPR xmo_MARGRHO_ETAT23   en entr�es
************************************************************************
* Remarque: N vaut 1 � l'appel
************************************************************************
C
      DOUBLE PRECISION 
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX)
C
      CALL MOMESS('DANS MOBASC_GEAR')
C
C
C Pour pouvoir faire appel a moparl et moparl2 par la suite
C
      DO IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VG(IVGA,N)
      ENDDO
*
      DO IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VD(IVDE,N)
      ENDDO
*
C Test pour d�tection d'un basculement
      mo_IB = 0
      xmo_EPS = 1.D-20
      mo_IET_APPR_EVAL = mo_IET_APPR
C
      CALL MOMESI('dans MOBASC_GEAR deb mo_IET =',mo_IET)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
      IF (mo_IET .EQ. 0) THEN
C       Le combustible est dans l'�tat classique (0)
C       Il est susceptible de basculer vers l'�tat 'en restructuration' (2)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &    xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &    xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
*
* Calcul de xmo_FBJ et xmo_FPORJ
*
        CALL MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,0.D0,
     &              xmo_FBJ,xmo_FPORJ,xmo_FEXT)
*
* Estimation de mo_IET_APPR_EVAL
*
        IF ( xmo_RHOD .GT. (xmo_RHOLIM - xmo_RHODL 
     &                         - xmo_MARGRHO_APPR) ) THEN
          mo_IET_APPR_EVAL = 2
        ELSE
          mo_IET_APPR_EVAL = 0
        ENDIF
*
*
*
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C       Test vis � vis de l'�tat 'en restructuration' (2)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        IF ( xmo_RHOD .GT. (xmo_RHOLIM - xmo_RHODL) ) THEN
            IF ( mo_IET_APPR .EQ. 2 .OR. mo_IET_APPR .EQ. 12 ) THEN
* Dans ce cas, on �tait d�j� proche de l'�tat 'en restructuration' 
* Maintenant, il faut y passer. Le basculement vers l'�tat 'en restructuration' est d�cid�.
              mo_IB = 2
            ELSE
* La valeur obtenue place le combustible dans l'�tat 'en restructuration'.
* Mais ce changement est trop rapide. On doit reprendre le calcul 
* au temps d'avant et faire un pas de temps plus petit.
              mo_IB = -2
            ENDIF
        ELSE
* Pas de basculement prevu. 
            mo_IB = 0
        ENDIF
C
C
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C fin du cas o� le combustible est dans l'�tat classique
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C
C
************************************************************************
************************************************************************
************************************************************************
************************************************************************
      IF (mo_IET .EQ. 2) THEN
C       Le combustible est dans l'�tat 'en restructuration' (2)
C       Il est susceptible de basculer vers l'�tat restructure (3)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
* Estimation de mo_IET_APPR_EVAL
*
        IF ( xmo_RHOD .GT. 
     &      (xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_APPR 
     &                              - xmo_MARGRHO_ETAT23) ) THEN
            mo_IET_APPR_EVAL = 3
        ELSE
            mo_IET_APPR_EVAL = 2
        ENDIF
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C       Test vis � vis de l'�tat restructur� (3)
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C On passe dans l'�tat (3) un peu avant que xmo_RHOD soit �gal
C � xmo_RHOLIM + xmo_RHODL (utilisationde MARGRHO_ETAT23) pour �viter
C d'avoir xmo_FR trop proche de 1, car on a des divisions par (1.D0-xmo_FR)
C
C
        IF (xmo_RHOD .GT. 
     &        (xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_ETAT23) ) THEN
            IF ( mo_IET_APPR .EQ. 3 ) THEN
* Dans ce cas, on �tait d�j� proche de l'�tat restructur�
* Maintenant, il faut y passer. Le basculement vers l'�tat restructur� est d�cid�.
              mo_IB = 23
            ELSE
* La valeur obtenue place le combustible dans l'�tat restructur�.
* Mais ce changement est trop rapide. On doit reprendre le calcul 
* au temps d'avant et faire un pas de temps plus petit.
              mo_IB = -23
            ENDIF
        ELSE
* Pas de basculement prevu. 
            mo_IB = 0
        ENDIF
C
C Dans la phase en restructuration, il est normal que les variables des zones
C saines tendent vers z�ro
C Mais si le pas de temps est trop grand, elles peuvent devenir n�gatives
C Dans ce cas on doit aussi reprendre le calcul 
C au temps d'avant et faire un pas de temps plus petit.
C
****        DO IVGA = 1, mo_NGG+3
****          IF (xmo_VG(IVGA,N) .LE. 0.D0) THEN
****            mo_IB = -23
****          ENDIF
****        ENDDO
C
****        DO IVDE = 1, 3
****          IF (xmo_VD(IVDE,N) .LE. 0.D0) THEN
****            mo_IB = -23
****          ENDIF
****        ENDDO
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
C
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C fin du cas o� le combustible est dans l'�tat 'en restructuration'
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C
* Mise a jour eventuelle de mo_IET_APPR
*
      IF (mo_IB .EQ. 0) THEN
* le calcul va avancer en temps
        mo_IET_APPR = mo_IET_APPR_EVAL
      ELSEIF (mo_IB .LT. 0) THEN
* le calcul va reculer en temps pour recherche du basculement lent
* il faut garder mo_IET_APPR
      ELSE
* le calcul va avancer en temps et on va changer d'�tat
        mo_IET_APPR = mo_IET_APPR_EVAL
      ENDIF
*
      CALL MOMESI('dans MOBASC_GEAR fin mo_IB =',mo_IB)
      CALL MOMESI('dans MOBASC_GEAR fin mo_IET_APPR =',mo_IET_APPR)
C
      RETURN
      END
      SUBROUTINE MOBLOC
     &    (xmo_CBUL,xmo_CGAZ,xmo_VTBUL,xmo_RBUL,mo_BBJT,
     &     xmo_BB,xmo_BBB,xmo_TEXP2)
C     ==================================================================
C
C
C     ******************************************************************
C     MOBLOC : MOgador BLOC de calcul des fr�quences de remise en solution
C              et de la pression interne dans les bulles ou les pores                     
C     ******************************************************************
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
C
************************************************************************
* arguments en entr�e :
* xmo_CGAZ : concentration de gaz (mol/m3) de bulle ou de pore
* xmo_VTBUL : Volume total des pores ou des bulles /m3 de combustible ()
* xmo_RBUL : Rayon des bulles ou des pores (m)
* mo_BBJT : 1  bulle intra 
*         : 2  bulle inter 
*         : 3  tunnel (non utilis�)
*         : 4  pore intra 
*         : 5  pore inter
*         : 6  bulles de rim
*         : 7  pores de rim 
*
* arguments en sortie :
* xmo_BB : fr�quence de remise en solution du gaz (s-1)
* xmo_BBB :fr�quence de remise en solution du gaz avec disparition 
*          de la bulle ou du pore (s-1)
* xmo_TEXP2 : Deuxi�me terme en exponentielle dans l'expression 
*             du flux de lacunes ()
************************************************************************
*
      CALL MOMESS('DANS MOBLOC')
*
      IF (xmo_VTBUL .GT. 0.D0  .AND.  xmo_CBUL .GT. 0.D0) THEN
*
        CALL MOVLAT(xmo_CGAZ,xmo_VTBUL,mo_BBJT,xmo_VAT)
*
*
* Coefficients de remise en solution du gaz xmo_BB
* et xmo_BBB des bulles ou des pores
*
        CALL MORSOL(xmo_CBUL,xmo_VTBUL,xmo_VAT,xmo_RBUL,mo_BBJT,
     &              xmo_BB,xmo_BBB)
*
*
* Calcul de xmo_TEXP2
*
        CALL MOCEXP(xmo_VAT,xmo_RBUL,mo_BBJT,xmo_TEXP2)
*
      ELSE
        xmo_BB = 0.D0
        xmo_BBB = 0.D0
        xmo_TEXP2 = 0.D0
      ENDIF
*
*   
      RETURN
      END
      SUBROUTINE MOBUCH(xmo_TK,xmo_DBUCH)
C     =================
C
C
C     ******************************************************************
C     MOBUCH : MOgador calcul du diam�tre de la sph�re dure 
C              du potentiel de Buchingham                         
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
************************************************************************
* xmo_TK : temp�rature (K)
* xmo_DBUCH : Diam�tre de la sph�re dure (m)
************************************************************************
*
      CALL MOMESS('DANS MOBUCH')
*
      xmo_DBUCH = 4.45D-10 * 
     &            ( 0.8542D0 - 0.03996D0 * LOG( xmo_TK / 231.2D0 ) )
*   
      RETURN
      END
      SUBROUTINE MOBUPJ
     &    (xmo_CGAZ,xmo_CBUL,xmo_RBUL,xmo_VBJ,xmo_DBJ,xmo_PIEGBJ)
C     ==================================================================
C
C
C     ******************************************************************
C     MOBUPJ : MOgador calcul de la vitesse sous gradient thermique, 
C              du coefficient de diffusion al�atoire et du terme de pi�geage
C              du gaz dissous par des BUlles inter en mouvement                        
C     ******************************************************************
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogdon.inc'
C
***********************************************************************
* cmogpas.inc : xmo_VBJSUR xmo_VBJVOL xmo_DBJVO xmo_DBJSU 
*               xmo_FACPBJ xmo_DXEJ en entr�e
* cmogppr.inc : xmo_QVOL xmo_ATETBJ mo_VBJ_OUI en entr�e
* cmogphy.inc : xmo_PIZAHL xmo_XNAVOG  en entr�e
* cmogdon.inc : xmo_AGG en entr�e
************************************************************************ 
C
************************************************************************
* arguments en entr�e :
* xmo_CGAZ : concentration de gaz (mol/m3) de bulle inter 
* xmo_CBUL : concentration de bulle inter (m-3)
* xmo_RBUL : Rayon  de la bulle inter (m)
*
* arguments en sortie :
* xmo_VBJ : vitesse sous gradient thermique de bulle inter (m/s)
* xmo_DBJ : coefficient de diffusion al�atoire de bulle  inter (m2/s)
* xmo_PIEGJ : fr�quence piegeage du gaz dissous par les bulles inter (s-1)
************************************************************************
*
      CALL MOMESS('DANS MOBUPJ')
*
      IF (xmo_RBUL .GT. 0.D0) THEN
*     ++++++++++++++++++++++++++++
*
* vitesse des bulles inter xmo_VBJ (m/s) en mouvement sous gradient thermique
        IF ( mo_VBJ_OUI .EQ. 1 ) THEN
          xmo_WJ_1 = 
     &         1.D0 - min( 1.D0 , 
     &                     xmo_QVOL / 
     &                     ( xmo_ATETBJ *4.D0 / 3.D0 * xmo_PIZAHL * 
     &                       xmo_RBUL**3.D0 ) )
          xmo_WJ_2 = xmo_CGAZ * xmo_XNAVOG / xmo_CBUL
*
          IF (xmo_WJ_1 .GT.0) THEN
            IF (xmo_WJ_2 .GT.0) THEN
              xmo_WJ = xmo_WJ_1 ** xmo_WJ_2
            ELSE
              xmo_WJ = 1.D0
            ENDIF
          ELSE
            xmo_WJ = 0.D0
          ENDIF
*
*
          xmo_VBJ = (xmo_VBJSUR * xmo_WJ / xmo_RBUL + xmo_VBJVOL)
        ELSE
          xmo_VBJ = 0.D0
        ENDIF
*
*
*
* coefficient de diffusion des bulles inter xmo_DBJ
*
*
        xmo_DBJ = xmo_DBJVO / xmo_RBUL**3.D0 +
     &           xmo_DBJSU * xmo_WJ / xmo_RBUL**4.D0
*
* Pour terme de pi�geage :
*
        xmo_PIEGBJ = xmo_FACPBJ * xmo_DXEJ * xmo_CBUL
*
        ELSE
*       ++++
* Cas o� xmo_RBUL est �gal � z�ro
          xmo_PIEGBJ = 0.D0
          xmo_VBJ = 0.D0
          xmo_DBJ = 0.D0
        ENDIF
*       +++++
*   
      RETURN
      END
      SUBROUTINE MOBUPO
     &    (xmo_CGAZ,xmo_CBUL,xmo_RBUL,xmo_D,xmo_TEST,
     &     xmo_VB,xmo_DB)
C     ==================================================================
C
C
C     ******************************************************************
C     MOBUPO : MOgador calcul de la vitesse sous gradient thermique, 
C              du coefficient de diffusion al�atoire des bulles ou des 
C              pores intra en mouvement                        
C     ******************************************************************
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      DOUBLE PRECISION tmpX, tmpY
C
***********************************************************************
* cmogpas.inc : xmo_VBSUR xmo_VBVOL xmo_DBVOL xmo_DBSUR 
* cmogppr.inc : xmo_QVOL 
* cmogphy.inc : xmo_PIZAHL xmo_XNAVOG  en entr�e
************************************************************************ 
C
************************************************************************
* arguments en entr�e :
* xmo_CGAZ : concentration de gaz (mol/m3) de bulle ou de pore
* xmo_CBUL : concentration de bulle ou de pore (m-3)
* xmo_RBUL : Rayon du pore ou de la bulle (m)
* xmo_D : coefficient de diffusion du gaz dans le milieu 
*        (en tenant compte de la pr�sence de dislocation)
* xmo_TEST : 0 correspond au test de suppression de la mobilit�
*            1 correspond au fonctionnement normal
* arguments en sortie :
* xmo_VB : vitesse sous gradient thermique de bulle ou de pore (m/s)
* xmo_DB : coefficient de diffusion al�atoire de bulle ou de pore (m2/s)
************************************************************************
*
      CALL MOMESS('DANS MOBUPO')
*
      IF (xmo_RBUL .GT. 0.D0) THEN
* vitesse des bulles intra xmo_VB (m/s)
*
        IF (xmo_CGAZ .GT. 0.D0) THEN
          tmpX = 1.D0 
     &        - min( 1.D0 , 
     &               xmo_QVOL / 
     &               ( 4.D0 / 3.D0 * xmo_PIZAHL * xmo_RBUL**3.D0))
          tmpY = xmo_CGAZ * xmo_XNAVOG / xmo_CBUL
          xmo_W = tmpX**tmpY
        ELSE
          xmo_W = 1.D0
        ENDIF
*
*
        xmo_VB = xmo_VBSUR * xmo_W / xmo_RBUL + xmo_VBVOL
        xmo_VB = xmo_TEST * xmo_VB
*
*
* coefficient de diffusion des bulles intra xmo_DB
*
*
        xmo_DB = xmo_DBVOL / xmo_RBUL**3.D0 +
     &           xmo_DBSUR * xmo_W / xmo_RBUL**4.D0
        xmo_DB = xmo_TEST * xmo_DB 
*
*
*
        ELSE
* Cas o� xmo_RBUL est �gal � z�ro
          xmo_VB = 0.D0
          xmo_DB = 0.D0
        ENDIF
*   
      RETURN
      END
      SUBROUTINE MOCABI(mo_IET, mo_PREMIERSOUSPAS, DTDECO,
     &                  xmo_CXEM, xmo_CXEB, xmo_CXEPOR,
     &                  xmo_CXEJM, xmo_CXEBJ, xmo_CXEPORJ,
     &                  xmo_CXEHOMM, xmo_CXEBRIM, xmo_CXEPORRIM)
C     =================
C
C
C     ******************************************************************
C     MOCABI : MOgador Calcul des BIlans de gaz                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpte.inc'
C
***********************************************************************
* cmogsor.inc : xmo_TAUXBETAC xmo_TAUXREMISOL xmo_TAUXSOURXE 
*               xmo_TAUXSORBUL xmo_TAUXSORPOR xmo_TAUXSORPOR
*               xmo_TAUXSORPERC  xmo_TAUXSOREJEC en entr�e
*               xmo_GCREE xmo_REMISOL xmo_SORDIF xmo_SORBUL xmo_SORPOR
*               xmo_SORDIFJ xmo_SORPERC xmo_SOREJECen entr�e et sortie
*               xmo_ERRINTRA xmo_ERRINTER xmo_ERRGLOB en sortie
* cmogdon.inc : xmo_CXEPORI  xmo_CXEPORJI en entr�e
* cmogpte.inc : xmo_TEST7 en entr�e
***********************************************************************      
***********************************************************************
* 
*
***********************************************************************
      CALL MOMESS('DANS MOCABI')
*
*
* Les quantit�s ne sont pas calcul�es de la m�me mani�re lors du premier sous-
* pas de temps pour que le gaz cr�e reste en coh�rence avec le calcul du 
* burnup, effectu� dans METEOR, qui ne connait pas les sous pas de temps 
* du module margaret V3.1.
*
        IF (mo_PREMIERSOUSPAS .EQ. 1) THEN
          mo_PREMIERSOUSPAS = 0
          xmo_P1 = 1.D0
          xmo_P2 = 0.D0
        ELSE
          xmo_P1 = 0.5D0
          xmo_P2 = 0.5D0
        ENDIF
*
**********************************************************************
* CAS ETAT CLASSIQUE - CONNECTE
**********************************************************************
      IF (mo_IET .EQ. 0) THEN
*     ***********************
*
        xmo_GCREE = xmo_GCREE + 
     &    ( xmo_P2 * xmo_TAUXBETAC(2) + 
     &      xmo_P1 * xmo_TAUXBETAC(1) ) 
     &    * DTDECO
* 
        xmo_REMISOL = xmo_REMISOL + 
     &     ( xmo_P2 * xmo_TAUXREMISOL(2) + 
     &       xmo_P1 * xmo_TAUXREMISOL(1) ) 
     &     * DTDECO
*
        xmo_SORDIF = xmo_SORDIF + 
     &     ( xmo_P2 * xmo_TAUXSOURXE(2) + 
     &       xmo_P1 * xmo_TAUXSOURXE(1) )
     &     * DTDECO
*
        xmo_SORBUL = xmo_SORBUL + 
     &     ( xmo_P2 * xmo_TAUXSORBUL(2) + 
     &       xmo_P1 * xmo_TAUXSORBUL(1) )
     &     * DTDECO
*
        xmo_SORPOR = xmo_SORPOR + 
     &     ( xmo_P2 * xmo_TAUXSORPOR(2) + 
     &       xmo_P1 * xmo_TAUXSORPOR(1) )
     &     * DTDECO
*
        xmo_SORDIFJ = xmo_SORDIFJ +
     &     ( xmo_P2 * xmo_TAUXOUTSE(2) + 
     &       xmo_P1 * xmo_TAUXOUTSE(1) )
     &     * DTDECO
*
        xmo_SORPERC = xmo_SORPERC +
     &     ( xmo_P2 * xmo_TAUXOUTPERC(2) + 
     &       xmo_P1 * xmo_TAUXOUTPERC(1) )
     &     * DTDECO
*
        xmo_SOREJEC = xmo_SOREJEC +
     &     ( xmo_P2 * xmo_TAUXOUTEJEC(2) + 
     &       xmo_P1 * xmo_TAUXOUTEJEC(1) )
     &     * DTDECO
*
**********************************************************************
* Calcul de l'erreur relative sur le bilan de gaz
**********************************************************************
*
        IF ( ( xmo_GCREE + xmo_CXEPORI ).NE. 0.D0) THEN
          xmo_ERRINTRA = ( ( xmo_GCREE + xmo_CXEPORI ) - 
     &                     ( xmo_CXEM + xmo_CXEB + xmo_CXEPOR 
     &                       + xmo_SORDIF + xmo_SORBUL + xmo_SORPOR
     &                       + xmo_SOREJEC
     &                       - xmo_REMISOL*xmo_TEST7 ) ) 
     &                   / ( xmo_GCREE + xmo_CXEPORI )
        ELSE
          xmo_ERRINTRA = 0.D0
        ENDIF
*
        IF ( (xmo_SORDIF + xmo_SORBUL + xmo_SORPOR 
     &        + xmo_CXEPORJI) .NE. 0.D0) THEN
          xmo_ERRINTER = ( (xmo_SORDIF + xmo_SORBUL + xmo_SORPOR
     &                       + xmo_CXEPORJI ) - 
     &                     ( xmo_CXEJM + xmo_CXEBJ + xmo_CXEPORJ 
     &                       + xmo_SORDIFJ + xmo_SORPERC 
     &                       + xmo_REMISOL ) )  
     &                   /   
     &                   ( xmo_SORDIF + xmo_SORBUL + xmo_SORPOR
     &                     + xmo_CXEPORJI )
        ELSE
          xmo_ERRINTER = 0.D0
        ENDIF
*
        IF ( ( xmo_GCREE + xmo_CXEPORI
     &         + xmo_CXEPORJI ) .NE. 0.D0) THEN
          xmo_ERRGLOB = 
     &       (  ( xmo_GCREE + xmo_CXEPORI+ xmo_CXEPORJI ) - 
     &          ( xmo_CXEM + xmo_CXEB + xmo_CXEPOR 
     &            + xmo_CXEJM + xmo_CXEBJ + xmo_CXEPORJ 
     &            + xmo_SORDIFJ + xmo_SORPERC + xmo_SOREJEC ) ) 
     &    /  ( xmo_GCREE + xmo_CXEPORI+ xmo_CXEPORJI )
        ELSE
          xmo_ERRGLOB = 0.D0
        ENDIF
*
*
        ENDIF
*       *****
*
**********************************************************************
* CAS ETAT 'EN RESTRUCTURATION'
**********************************************************************
        IF (mo_IET .EQ. 2) THEN
*       ************************
*
        xmo_GCREE = xmo_GCREE + 
     &    ( xmo_P2 * xmo_TAUXBETAC(2) + 
     &      xmo_P1 * xmo_TAUXBETAC(1) ) 
     &    * DTDECO
*
        xmo_SORDIFJ = xmo_SORDIFJ +
     &     ( xmo_P2 * xmo_TAUXOUTSE(2) + 
     &       xmo_P1 * xmo_TAUXOUTSE(1) )
     &     * DTDECO
*
        xmo_SORPERC = xmo_SORPERC +
     &     ( xmo_P2 * xmo_TAUXOUTPERC(2) + 
     &       xmo_P1 * xmo_TAUXOUTPERC(1) )
     &     * DTDECO
*
        xmo_SOREJEC = xmo_SOREJEC +
     &     ( xmo_P2 * xmo_TAUXOUTEJEC(2) + 
     &       xmo_P1 * xmo_TAUXOUTEJEC(1) )
     &     * DTDECO
**********************************************************************
* Calcul de l'erreur relative sur le bilan de gaz
**********************************************************************
*
*
          IF ( ( xmo_GCREE + xmo_CXEPORI+ xmo_CXEPORJI ) 
     &         .NE. 0.D0) THEN
            xmo_ERRGLOB = 
     &        ( ( xmo_GCREE + xmo_CXEPORI+ xmo_CXEPORJI ) - 
     &          ( xmo_CXEM + xmo_CXEB + xmo_CXEPOR +
     &            xmo_CXEJM + xmo_CXEBJ + xmo_CXEPORJ +
     &            + xmo_CXEHOMM + xmo_CXEPORRIM + xmo_CXEBRIM
     &            + xmo_SORDIFJ + xmo_SORPERC + xmo_SOREJEC) ) 
     &      / ( xmo_GCREE + xmo_CXEPORI+ xmo_CXEPORJI ) 
          ELSE
            xmo_ERRGLOB = 0.D0
          ENDIF
*
        ENDIF
*       *****
**********************************************************************
* CAS ETAT 'RESTRUCTURE'
**********************************************************************
        IF (mo_IET .EQ. 3) THEN
*       ************************
*
        xmo_GCREE = xmo_GCREE + 
     &    ( xmo_P2 * xmo_TAUXBETAC(2) + 
     &      xmo_P1 * xmo_TAUXBETAC(1) ) 
     &    * DTDECO
*
        xmo_SORDIFJ = xmo_SORDIFJ +
     &     ( xmo_P2 * xmo_TAUXOUTSE(2) + 
     &       xmo_P1 * xmo_TAUXOUTSE(1) )
     &     * DTDECO
*
        xmo_SORPERC = xmo_SORPERC +
     &     ( xmo_P2 * xmo_TAUXOUTPERC(2) + 
     &       xmo_P1 * xmo_TAUXOUTPERC(1) )
     &     * DTDECO
*
        xmo_SOREJEC = xmo_SOREJEC +
     &     ( xmo_P2 * xmo_TAUXOUTEJEC(2) + 
     &       xmo_P1 * xmo_TAUXOUTEJEC(1) )
     &     * DTDECO
**********************************************************************
* Calcul de l'erreur relative sur le bilan de gaz
**********************************************************************
*
*
          IF ( ( xmo_GCREE + xmo_CXEPORI + xmo_CXEPORJI ) 
     &         .NE. 0.D0) THEN
            xmo_ERRGLOB = 
     &         ( ( xmo_GCREE + xmo_CXEPORI + xmo_CXEPORJI ) - 
     &           ( xmo_CXEHOMM + xmo_CXEPORRIM + xmo_CXEBRIM 
     &             + xmo_SORDIFJ + xmo_SORPERC + xmo_SOREJEC) ) / 
     &         ( xmo_GCREE + xmo_CXEPORI + xmo_CXEPORJI )
          ELSE
            xmo_ERRGLOB = 0.D0
          ENDIF
*
        ENDIF
*       *****
*
**********************************************************************
*
*  
      RETURN
      END
      SUBROUTINE MOCACR(xmo_CXENGG,
     &                  mo_NCOURONNE, xmo_RCOURONNE,
     &                  xmo_CXEBJ, xmo_BBJ, xmo_FBJ,
     &                  xmo_CXEBJ1, xmo_BBJ1,
     &                  xmo_AGRAIN, xmo_DXEAM, xmo_CR)
C     ====================
C
C
C     ******************************************************************
C     MOCACR : MOgador calcul de CR : concentration locale de gaz 
C              dissous intra � la surface du grain (non nulle a cause de
C              la remise en solution                         
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmoginf.inc : mo_DEBUG  en entr�e
* cmogppr.inc : xmo_DREM en entr�e
************************************************************************
*
* param�tres de la subroutine en entr�e
* xmo_CXENGG : valeur de la concentration en gaz dissous dans le grain 
*              sur la derni�re couronne (mol/m3)
* xmo_CXEBJ : concentration de gaz dans la population inter consid�r�e (mol/m3)
*             (bulles inter, pores inter, ou gaz dissous inter)
* xmo_BBJ : coefficient de remise en solution du gaz depuis la population inter 
*          consid�r�e (s-1)
* xmo_FBJ : taux de couverture du joint de grain par la population inter consid�r�e (/)
* xmo_CXEBJ1 : concentration de gaz dans une autre population inter (mol/m3)             
* xmo_BBJ1 : coefficient de remise en solution du gaz depuis l'autre population inter 
*          consid�r�e (s-1)
* xmo_AGRAIN : rayon du grain (�tat (0) ou (1) ou (2)) (m)
* xmo_DXEAM : coefficient de diffusion consid�r� pour le gaz dissous intra (m2/s)
*
* en sortie
* xmo_CR : concentration locale de gaz dissous � la p�riph�rie du grain
*          face � la population consid�r�e
*          cette concentration est non nulle du fait de la remise en solution (mol/m3)
*
************************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_RCOURONNE(mo_NNSPHMX+1)
*
      CALL MOMESS('DANS MOCACR')
*
* xmo_RMIL est le rayon relatif du milieu de la derni�re couronne
*
      xmo_RMIL = 
     &   ( xmo_RCOURONNE(mo_NCOURONNE) +
     &     xmo_RCOURONNE(mo_NCOURONNE-1) ) / 2.D0
*
      IF ( xmo_DXEAM .GT. 0.D0  .AND. xmo_DREM .GT. 0.D0 ) THEN
*     ---------------------------------------------------------
*
        IF ( xmo_FBJ .GT. 0.D0 ) THEN
          xmo_TERM1 = 
     &         (xmo_BBJ * xmo_CXEBJ + xmo_BBJ1 * xmo_CXEBJ1)
     &         * xmo_AGRAIN**2.D0 / 3.D0 / xmo_FBJ
     &         * ( 1.D0 - xmo_RMIL ) / xmo_DXEAM
        ELSE
          xmo_TERM1 = 0.D0
        ENDIF
*
        xmo_TERM2 = 
     &     1.D0 + 2.D0 * xmo_AGRAIN * ( 1.D0 - xmo_RMIL ) / xmo_DREM
*
        IF ( xmo_TERM2 .GT. 0.D0 ) THEN
          xmo_CR = ( xmo_CXENGG +  xmo_TERM1 ) /  xmo_TERM2
        ELSE
          WRITE(mo_DEBUG,*) 'Probleme dans mocacr xmo_TERM2 nul'
          STOP
        ENDIF      
*
      ELSE
*     ----
        WRITE(mo_DEBUG,*) 'Probleme dans mocacr'
        STOP
      ENDIF
*     -----
*
*   
      RETURN
      END
      SUBROUTINE MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,xmo_FR,
     &                  xmo_FBJ,xmo_FPORJ,xmo_FEXT)
C     =================
C
C
C     ******************************************************************
C     MOCAFF : MOgador CAlcul de Fbj et de Fporj en fonction de l'�tat 
C              du combustible                      
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogent.inc'
C
***********************************************************************
* cmogvar.inc : mo_IET xmo_ASERIM en entr�e
* cmogdon.inc : xmo_AGG en entr�e 
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogpnu.inc : 
* cmogppr.inc : xmo_ATETPORJ xmo_ATETBJ  en entr�e
* cmogent.inc : xmo_ASE en entr�e
************************************************************************
*
      CALL MOMESS('DANS MOCAFF')
*
************************************************************************
* Etat classique-connect� (0)ou en restructuration (2) 
* calcul de xmo_FPORJ et xmo_FBJ 
************************************************************************
*
*
      IF (xmo_CPORJ .GT. 0.D0 .AND. xmo_VTPORJ .GT. 0.D0) THEN 
        xmo_RPORJ =  
     &      ( 3.D0 / 4.D0 / xmo_PIZAHL / 
     &        xmo_ATETPORJ * xmo_VTPORJ / xmo_CPORJ )
     &       **(1.D0/3.D0)
      ELSE
        xmo_RPORJ = 0.D0
      ENDIF
*
*
************************************************************************
      CALL MORONLEN(mo_PORJ_RON)
      IF (mo_PORJ_RON .EQ. 1) THEN
        xmo_FPORJ_ESTIM = 
     &          4.D0 * xmo_PIZAHL * (xmo_RPORJ ** 2.D0) * xmo_CPORJ
     &               / (3.D0/xmo_AGG) 
      ELSE
        xmo_FPORJ_ESTIM =  
     &              xmo_PIZAHL * (xmo_RPORJ ** 2.D0) * xmo_CPORJ
     &               / (3.D0/2.D0/xmo_AGG)
      ENDIF
*
************************************************************************
*
*
      IF (xmo_CBJ .GT. 0.D0 .AND. xmo_VTBJ .GT. 0.D0) THEN 
          xmo_RBJ = 
     &     ( 3.D0 / 4.D0 / xmo_PIZAHL / 
     &       xmo_ATETBJ * xmo_VTBJ / xmo_CBJ )
     &     **(1.D0/3.D0)
      ELSE
          xmo_RBJ = 0.D0
      ENDIF
*
      xmo_FBJ_ESTIM = xmo_PIZAHL * (xmo_RBJ ** 2.D0) * xmo_CBJ
     &             / (3.D0/2.D0/xmo_AGG)
*
************************************************************************ 
* Fraction de surface de grain non restructur�e donnant directement 
* sur l'ext�rieur (surface libre)
* par m3 de conbustible la surface libre est de 3/xmo_ASE = S/V
* envoy� par le code cible
*
       IF (mo_IET .EQ. 0) THEN
         xmo_FEXT = xmo_AGG / xmo_ASE 
       ELSEIF (mo_IET .EQ. 2) THEN
         xmo_FEXT = xmo_AGG / xmo_ASERIM * (1.D0 - xmo_FR)
       ENDIF
*
************************************************************************
* Dans tous les cas la somme xmo_FBJ + xmo_FPORJ doit etre 
* inf�rieur strictement a 1 - xmo_FR - xmo_FEXT
*
      xmo_SOMME = xmo_FPORJ_ESTIM + xmo_FBJ_ESTIM
      IF ( xmo_SOMME .GE. 0.99D0 * (1.D0 - xmo_FR - xmo_FEXT) ) THEN
        xmo_FPORJ = 
     &     xmo_FPORJ_ESTIM * 0.99D0 * (1.D0 - xmo_FR - xmo_FEXT)
     &      / xmo_SOMME
        xmo_FBJ = 
     &     xmo_FBJ_ESTIM * 0.99D0 * (1.D0 - xmo_FR - xmo_FEXT)
     &      / xmo_SOMME 
      ELSE
        xmo_FPORJ = xmo_FPORJ_ESTIM 
        xmo_FBJ = xmo_FBJ_ESTIM 
      ENDIF
*
*
************************************************************************ 
      RETURN
      END
      SUBROUTINE MOCAFF_RIM(xmo_CPORRIM,xmo_VTPORRIM,
     &                      xmo_CBRIM,xmo_VTBRIM,xmo_FR,
     &              xmo_FBRIM,xmo_FPORRIM,xmo_FEXT_RIM,
     &              xmo_FECHAP_EXT,xmo_FECHAP_BRIM,xmo_FECHAP_PORRIM,
     &              xmo_F_FLUXNUL)
C     =================
C
C
C     ******************************************************************
C     MOCAFF_RIM : MOgador CAlcul des taux de couverture des petits grains
C     (grains des zones restructur�es type rim)                
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogppr.inc : xmo_APG xmo_DIST_ECHAP_RIM en entr�e
* cmogvar.inc : xmo_ASERIM  en entr�e
* cmoginf.inc : mo_DEBUG  en entr�e
************************************************************************
*
      CALL MOMESS('DANS MOCAFF_RIM')
*
************************************************************************
* Pores de rim : Ils sont toujours ronds
************************************************************************
*
*
      IF (xmo_CPORRIM .GT. 0.D0 .AND. xmo_VTPORRIM .GT. 0.D0) THEN 
        xmo_RPORRIM =  
     &      ( 3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTPORRIM / xmo_CPORRIM )
     &       **(1.D0/3.D0)
      ELSE
        xmo_RPORRIM = 0.D0
      ENDIF
*
*
      xmo_FPORRIM = 
     &      4.D0 * xmo_PIZAHL * (xmo_RPORRIM ** 2.D0) * xmo_CPORRIM
     &       / xmo_FR / (3.D0/xmo_APG) 
*
*
************************************************************************
* Bulles de rim : toujours rondes
************************************************************************
*
      IF (xmo_CBRIM .GT. 0.D0 .AND. xmo_VTBRIM .GT. 0.D0) THEN 
          xmo_RBRIM = 
     &     ( 3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTBRIM / xmo_CBRIM )
     &     **(1.D0/3.D0)
      ELSE
          xmo_RBRIM = 0.D0
      ENDIF
*
      xmo_FBRIM = 
     &      4.D0 * xmo_PIZAHL * (xmo_RBRIM ** 2.D0) * xmo_CBRIM
     &       / xmo_FR / (3.D0/xmo_APG)
* 
*
************************************************************************
* Taux de surface de grain pour �jection (sortie directe vers l'ext�rieur)
************************************************************************
*
      xmo_FEXT_RIM = xmo_APG / xmo_ASERIM
*
************************************************************************
* Taux de surface de grain pour �chappement vers l'ext�rieur
************************************************************************
*
      IF ( xmo_ASERIM . GT. 0.D0 .AND. 
     &     (3.D0 * xmo_DIST_ECHAP_RIM - xmo_APG). GT. 0.D0) THEN
*
        xmo_FECHAP_EXT = 
     &      (3.D0 * xmo_DIST_ECHAP_RIM - xmo_APG) / xmo_ASERIM
*
      ELSE
        WRITE(mo_DEBUG,*) 'Pb calcul xmo_FECHAP_EXT'
        STOP
      ENDIF
*
************************************************************************
* Taux de surface de grain pour �chapement vers les bulles de rim
* et vers les pores de rim
* xmo_F_FLUXNUL est diff�rent de z�ro uniquement en tout d�but de restructuration
* dans le cas o� xmo_FBRIM = xmo_FPORRIM = 0.D0
* On impose un flux nul sur cette portion de surface xmo_F_FLUXNUL 
* (le but est juste de garder un bon bilan de gaz)
************************************************************************
      xmo_FECHAP_BRIM_PORRIM = 
     &      1.D0 - xmo_FPORRIM - xmo_FBRIM
     &      - xmo_FECHAP_EXT - xmo_FEXT_RIM
*
      IF ( xmo_FECHAP_BRIM_PORRIM .GT. 0.D0 ) THEN
        IF ( (xmo_FPORRIM + xmo_FBRIM) .GT. 0.D0 ) THEN
          xmo_FECHAP_BRIM = 
     &      xmo_FECHAP_BRIM_PORRIM *
     &      xmo_FBRIM / ( xmo_FPORRIM + xmo_FBRIM )
*
          xmo_FECHAP_PORRIM = 
     &      xmo_FECHAP_BRIM_PORRIM *
     &      xmo_FPORRIM / ( xmo_FPORRIM + xmo_FBRIM )
*
          xmo_F_FLUXNUL = 0.D0
        ELSE
          xmo_FECHAP_BRIM = 0.D0
          xmo_FECHAP_PORRIM = 0.D0
          xmo_F_FLUXNUL = xmo_FECHAP_BRIM_PORRIM
        ENDIF
      ELSE
        WRITE(mo_DEBUG,*) 'xmo_FBRIM = ',xmo_FBRIM
        WRITE(mo_DEBUG,*) 'xmo_FPORRIM = ',xmo_FPORRIM
        WRITE(mo_DEBUG,*) 'xmo_FEXT_RIM = ',xmo_FEXT_RIM
        WRITE(mo_DEBUG,*) 'xmo_FECHAP_EXT = ',xmo_FECHAP_EXT
        WRITE(mo_DEBUG,*) 'xmo_FECHAP_BRIM_PORRIM = ',
     &                     xmo_FECHAP_BRIM_PORRIM
        WRITE(mo_DEBUG,*) 'Pb calcul xmo_FECHAP_BRIM_PORRIM'
        STOP
      ENDIF
************************************************************************
*
*
************************************************************************ 
      RETURN
      END
*
*
      SUBROUTINE MOCAINT
*     =================
*
*     ******************************************************************
*     MOCAINT : MOgador CAlcul des granduers INT�grales
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogsor.inc' 
      INCLUDE 'cmogtem.inc' 
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogdon.inc'   
************************************************************************
* cmoginf.inc : mo_NINTEG en entr�e
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE xmo_HHREF xmo_PRODIS en entr�es 
* cmogsor.inc : xmo_GINTRA3  xmo_GINTER3 xmo_GCREA3 xmo_SORPERC3 
*               xmo_SORDIFJ3 en entr�e 
* cmogtem.inc : xmo_TEMPS en entree
* cmogphy.inc : xmo_PIZAHL en entree
* cmogdon.inc : xmo_VTPORI xmo_VTPORJI en entree
************************************************************************
*
      DOUBLE PRECISION xmo_CREATR(mo_M31MAX),
     &                 xmo_RELATR(mo_M31MAX),
     &                 xmo_GINTRATR(mo_M31MAX),
     &                 xmo_GINTERTR(mo_M31MAX),
     &                 xmo_SORPERCTR(mo_M31MAX),
     &                 xmo_SORDIFJTR(mo_M31MAX),
     &                 xmo_SOREJECTR(mo_M31MAX),
     &    xmo_GONFTOT(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &    xmo_GONFTOTTR(mo_M31MAX),
     &    xmo_RLIMITE(mo_M31MAX),
     &    xmo_GONFTOTTR_BORD(mo_M31MAX)
*     ==================================================================
*
* xmo_GONFTOTTR est le gonflement moyen dans la tranche calcul� jusqu'au 
* rayon o� la fraction restructur�e vaut 0.1
* xmo_GONFTOTTR_BORD est le gonflement moyen dans la tranche calcul� 
* jusqu'au bord
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
      xmo_CREATOT = 0.D0
      xmo_RELATOT = 0.D0
      xmo_SORPERCTOT = 0.D0
      xmo_SORDIFJTOT = 0.D0
      xmo_SOREJECTOT = 0.D0
*
      DO 10 L = 1,mo_NB_TRAX
*
        xmo_CREATR(L) = 
     &     XCALIN(xmo_GCREA3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
        xmo_GINTRATR(L) = 
     &     XCALIN(xmo_GINTRA3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
        xmo_GINTERTR(L) = 
     &     XCALIN(xmo_GINTER3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
       xmo_RELATR(L) = 
     &     xmo_CREATR(L) 
     &     - xmo_GINTRATR(L) - xmo_GINTERTR(L)
*
        xmo_SORPERCTR(L) = 
     &     XCALIN(xmo_SORPERC3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
        xmo_SORDIFJTR(L) = 
     &     XCALIN(xmo_SORDIFJ3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
        xmo_SOREJECTR(L) = 
     &     XCALIN(xmo_SOREJEC3,L,1,mo_NB_ZONE(L),1000,1) 
     &     * xmo_HHREF(L) * (1.D0 - xmo_PRODIS(L))
*
       xmo_CREATOT = xmo_CREATOT + xmo_CREATR(L) 
       xmo_RELATOT = xmo_RELATOT + xmo_RELATR(L)
*
       xmo_SORPERCTOT =  xmo_SORPERCTOT + xmo_SORPERCTR(L)
       xmo_SORDIFJTOT =  xmo_SORDIFJTOT + xmo_SORDIFJTR(L)
       xmo_SOREJECTOT =  xmo_SOREJECTOT + xmo_SOREJECTR(L)
*
 10   CONTINUE
*
        IF (xmo_CREATOT .NE. 0.D0) THEN
          xmo_FRACRELACH = xmo_RELATOT / xmo_CREATOT
          xmo_FRACRELACH_PERC = xmo_SORPERCTOT / xmo_CREATOT
          xmo_FRACRELACH_DIFJ = xmo_SORDIFJTOT / xmo_CREATOT
          xmo_FRACRELACH_EJEC =  xmo_SOREJECTOT / xmo_CREATOT 
        ELSE
          xmo_FRACRELACH = 0.D0
          xmo_FRACRELACH_PERC = 0.D0
          xmo_FRACRELACH_DIFJ = 0.D0
          xmo_FRACRELACH_EJEC =  0.D0
        ENDIF
*
*=======================================================================
* calcul du gonflement moyen hors zones fortement restructur�es
*=======================================================================
*
* Initialisation de xmo_GONFTOT (tableau des gonflements totaux locaux)
      DO L = 1,mo_NB_TRAX
        DO IGROB = 1, mo_NB_ZONE(L)
          IF (IGROB .EQ. 1) THEN
            IDEB = 1
          ELSE
            IDEB = 2
          ENDIF	
          DO I = IDEB, mo_NBNOZO(L,IGROB)
            DO IHETER = 1, mo_NAMAMX
              xmo_GONFTOT(L,IGROB,I,IHETER,1) = 
     &           xmo_GONFS3(L,IGROB,I,IHETER,1) +
     &           xmo_GASPOR(L,IGROB,I,IHETER,1) +
     &           xmo_SINPOR(L,IGROB,I,IHETER,1) -
     &           xmo_VTPORI3(L,IGROB,I,IHETER)  -
     &           xmo_VTPORJI3(L,IGROB,I,IHETER)
            ENDDO
          ENDDO
        ENDDO
      ENDDO
*
      DO L = 1,mo_NB_TRAX
* Recherche de la limite de la zone pas ou peu restructur�e
        CALL MORECHFR(L,mo_IGROB_RES,mo_I_RES,I_PAS_DE_POINT)
*
        IF (I_PAS_DE_POINT .EQ. 0) THEN
*       On a pu trouver un point limite
          xmo_GONFTOTTR(L) = 
     &        XCALIN(xmo_GONFTOT,L,1,mo_IGROB_RES,mo_I_RES,1)
          xmo_RLIMITE(L) = xmo_RPAST(L,mo_IGROB_RES,mo_I_RES)
CDEBUG
CDEBUG          WRITE(20,*) 'mo_IGROB_RES =',mo_IGROB_RES
CDEBUG          WRITE(20,*) 'mo_I_RES =',mo_I_RES
CDEBUG          WRITE(20,*) 'xmo_RPAST(L,mo_IGROB_RES,mo_I_RES) = ',
CDEBUG     &                 xmo_RPAST(L,mo_IGROB_RES,mo_I_RES)
CDEBUG
          xmo_GONFTOTTR(L) = 
     &       xmo_GONFTOTTR(L) / xmo_PIZAHL / xmo_RLIMITE(L)**2
        ELSE
*       Il y a restructuration importante sur toute la largeur de la tranche
          xmo_GONFTOTTR(L) = 0.D0
        ENDIF
*
*
          xmo_GONFTOTTR_BORD(L) = 
     &      XCALIN(xmo_GONFTOT,L,1,mo_NB_ZONE(L),1000,1)
          mo_IGROB_MAX = mo_NB_ZONE(L)
          mo_I_MAX = mo_NBNOZO(L,mo_IGROB_MAX)
          xmo_RMAX = xmo_RPAST(L,mo_IGROB_MAX,mo_I_MAX) 
          xmo_GONFTOTTR_BORD(L) = 
     &       xmo_GONFTOTTR_BORD(L) / xmo_PIZAHL / xmo_RMAX**2
      ENDDO
*     ==================================================================

        WRITE(mo_NINTEG,1000) 
     &     xmo_TEMPS,
     &     xmo_CREATOT,
     &     xmo_RELATOT,
     &     xmo_FRACRELACH,
     &     xmo_FRACRELACH_PERC,
     &     xmo_FRACRELACH_DIFJ,
     &     xmo_FRACRELACH_EJEC,
     &     ( xmo_GONFTOTTR(L), xmo_RLIMITE(L),xmo_GONFTOTTR_BORD(L),
     &       xmo_CREATR(L), xmo_RELATR(L), L = 1,mo_NB_TRAX)
*
*     ==================================================================
*
*
* format pour ecriture dans fichier res_integral
 1000 FORMAT (1X,E26.8,7E26.8,60(E26.8)) 
*

      RETURN
      END
*
*
      DOUBLE PRECISION FUNCTION XCALIN(X,L,IG1,IG2,I2,N)
*     ==============================================
*
*     ******************************************************************
*     XCALIN :  CAlcul des grandeurs LIN�iques
*     XCALIN integre la quantite X(L,IGROB,I,IHETER,N) sur une tranche 
*     de la zone de IG1 (premier point) a la zone IG2 (point I2)
*     N = 1 (valeur courante) ; N = 2 (valeur pr�c�dente)
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmogphy.inc' 
      INCLUDE 'cmogmai.inc'
*      
************************************************************************ 
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogmai.inc : mo_NBNOZO xmo_RPAST
************************************************************************
*
*     ==================================================================
*
      DOUBLE PRECISION X(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2)
*
      CALL MOMESS('DANS XCALIN')
*
* a modifier quand on passera a de l heterogene
      IHETER = 1
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
      XCALIN = 0.D0
      DO 50 IGROB = IG1,IG2
        IEND = mo_NBNOZO(L,IGROB)
        IF (IGROB .EQ. IG2 .AND. I2 .LT. IEND) THEN
          IEND = I2
        ENDIF
        DO 50 I = 2,IEND
          RINN   = xmo_RPAST(L,IGROB,I-1)
          ROUT   = xmo_RPAST(L,IGROB,I)
          RDIF   = ROUT - RINN
          RDIF2  = (ROUT**2 - RINN**2)/2.D+00
          RDIF3  = (ROUT**3 - RINN**3)/3.D+00
          IF (IGROB .GT. 1   .AND.  I .EQ. 2 ) THEN
            IEND_PREC = mo_NBNOZO(L,IGROB-1) 
            FI     = X(L,IGROB-1,IEND_PREC,IHETER,N)
          ELSE
            FI     = X(L,IGROB,I-1,IHETER,N)
          ENDIF
          FO     = X(L,IGROB,I,IHETER,N)
          AFAK   = (FI*ROUT - FO*RINN)/RDIF
          BFAK   = (FO - FI)/RDIF
          XCALIN = XCALIN + AFAK*RDIF2 + BFAK*RDIF3
  50  CONTINUE
*
      XCALIN =2.D0 * xmo_PIZAHL *  XCALIN
*
*
      RETURN
      END
*
*
      SUBROUTINE MOCALV
*     =================
*
*     ******************************************************************
*     MOCALV : MOgador Calcul des variables en fin de pas de temps
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoginf.inc'
************************************************************************      
* cmogpnu.inc : mo_SCHEMA en entr�e
* cmoginf.inc : mo_DEBUG en entree
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
      CALL MOMESS('DANS MOCALV')
*
      IF (mo_SCHEMA .LT. 6 ) THEN
        WRITE(mo_DEBUG,*)  'mo_SCHEMA doit etre egal a 6'
        STOP
      ELSE IF(mo_SCHEMA .EQ. 6) THEN
        CALL MOCALV_GEAR
      ENDIF
*
*
      RETURN
      END
*
      SUBROUTINE MOCALV_GEAR 
C     ======================
C
C
C     ******************************************************************
C     MOCALV_GEAR : MOgador CALcul des Variables � la fin du pas de temps
C                   par l'une des m�thodes de GEAR      
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmoggpt.inc'
      INCLUDE 'cmogvar.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
***********************************************************************
* cmogent.inc : xmo_DTS xmo_TOFISS en entr�e
* cmogtem.inc : xmo_TEMPS en entr�e 
* cmoggpt.inc : xmo_PAS_TEMPS_LIM et mo_NTEMPS_LU en entr�e 
* cmogvar.inc : mo_PRFOIS en entr�e et sortie
***********************************************************************
C
C**********************************
C VARIABLES LOCALES : mo_ISAUTE, mo_IB, TIN TOUT
C                 
C**********************************
C
C
      CALL MOMESS('DANS MOCALV_GEAR')
***********************************************************************
C
C dans le cas o� le calcul commence par un pas de temps o� le taux de fission est
C nul, on recopie simplement les valeurs en d�but , en fin de pas de temps
C
      IF (mo_PRFOIS .EQ. 2 ) THEN
        IF (xmo_TOFISS .GT. 0.D0) THEN
          mo_PRFOIS  = 1
        ELSE
          CALL MOVALI
          xmo_TEMPS = xmo_TEMPS + xmo_DTS
          GOTO 500
        ENDIF
      ENDIF
C
***********************************************************************
      xmo_EPS_TEMPS = 1.D-5
C
      mo_ISAUTE = 0
      mo_IB = 0
C
C Calcul des param�tres secondaires utiles en fonction
C de l'�tat du combustible
C
      CALL MOCAPS_GEAR(mo_ISAUTE)
C
CDEBUG
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans mocalv_gear apres mocaps_gear'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
C
C
C Calcul des vecteurs variables en fin de pas de temps 
C
C
* On sauvegarde le temps final demand� (dans TOUT0)
* et le sous pas de temps utilis� a priori
      TOUT0 = xmo_TEMPS + xmo_DTS
*
      NTEMPS = mo_NTEMPS_LU
      IF (xmo_DTS .GT. xmo_PAS_TEMPS_LIM) THEN
        NTEMPS = int(xmo_DTS / xmo_PAS_TEMPS_LIM ) + 1
      ENDIF
*
*
      DELTAT0 = xmo_DTS / NTEMPS
*
      TIN = xmo_TEMPS
      TOUT = TIN + xmo_DTS
      CALL MOCAVF_GEAR
     &     (TIN,TOUT,NTEMPS,mo_IB,TAVANT,TAPRES)
*
* Traitement de la disparition d'une population
      IF ( mo_IB .EQ. 999 ) THEN
        IF ((TOUT0-TAPRES) .GT. xmo_EPS_TEMPS) THEN
          TIN1 = TAPRES
          TOUT1 = TOUT0
          CALL MOCAVF_GEAR
     &         (TIN1,TOUT1,NTEMPS,mo_IB,TAVANT,TAPRES)
        ELSE
* on ne fait rien de special car l'existence des populations
* sera test�e au d�but du pas de temps code cible suivant
* dans mocavf_gear
          mo_IB = 0
        ENDIF
      ENDIF
*
*
*
*     
* Traitement en cas de basculement avant TOUT
* et poursuite du calcul jusqu'� TOUT
*
*
* message
      IF (mo_IB .NE. 0) THEN
        CALL MOMESS('avant apppel � MOTRAI_GEAR')
        CALL MOMESI('mo_IB =',mo_IB)
        CALL MOMESV('TAVANT =',TAVANT)
        CALL MOMESV(' TAPRES=',TAPRES)
        CALL MOMESV(' DELTAT0=',DELTAT0)
        CALL MOMESV('TOUT0 =',TOUT0)
      ENDIF
* fin de message
C
      IF (mo_IB .NE. 0) THEN
        CALL MOTRAI_GEAR(mo_IB,TAVANT,TAPRES,DELTAT0,TOUT0)
      ENDIF
C
C Les variables r�sultats sont dans xmo_VG(..,1) et xmo_VD(..,1)
C
 500  CONTINUE  
C
      RETURN
      END
      SUBROUTINE MOCAPS_GEAR(mo_ISAUTE)
C     =================
C
C
C     ******************************************************************
C     MOCAPS : MOgador CAlcul des Param�tres Secondaires                 
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogsor.inc'
C
***********************************************************************
* cmogvar.inc : mo_IET en entr�e
* cmogdon.inc : mo_MATER xmo_MAVOLI xmo_AGG en entr�e
* cmogphy.inc : xmo_OMEGA xmo_PIZAHL xmo_XNAVOG xmo_ZD xmo_BOLTZ en entr�e
* cmogent.inc : xmo_TOFISS xmo_TK xmo_GRADTK xmo_ASE xmo_RXEKR xmo_GAMXE
*               xmo_EFISS xmo_VTPORTOT_LU xmo_CPOR_LU xmo_VTPOR_LU
*               xmo_CPORJ_LU xmo_VTPORJ_LU en entr�e
* cmogppr.inc : xmo_TDUJ xmo_RAP_DXE_THMIX xmo_RAP_DXE_ATH,xmo_TDU 
*               xmo_TDXE xmo_PROBU xmo_TOREC 
*               xmo_TKREC xmo_DTKREC xmo_FACBJ xmo_FNB 
*               xmo_FNBJ xmo_TDUS xmo_TDS xmo_QS  xmo_QV xmo_AXEJ xmo_ABJ 
*               xmo_TETBJ xmo_ATETBJ xmo_TETPORJ xmo_EPJ xmo_AVJ  
*               xmo_COEF1_GAMMA
*               xmo_COEF2_GAMMA 
*               xmo_MIN_DISTCOAL xmo_MAX_DISTCOAL xmo_STFMIN xmo_STFMAX xmo_TKMIN xmo_TKMAX
*               xmo_MIN_DISTCOALJ xmo_MAX_DISTCOALJ xmo_STFMINJ xmo_STFMAXJ xmo_TKMINJ xmo_TKMAXJ 
*               en entr�e
* cmogpas.inc : xmo_BETAC xmo_GAMMA xmo_DUJ xmo_DU xmo_DXE xmo_DXEJ 
*               xmo_DXETHMIX xmo_DEXJTHMIX xmo_DBUCH 
*               xmo_OMSKT xmo_PVI xmo_FACRHO xmo_BJ xmo_KG 
*               xmo_DUS xmo_DS xmo_DBVOL xmo_DBSUR xmo_VBVOL xmo_KGJ 
*               xmo_FACGCJ xmo_VBJVOL xmo_VBJSUR xmo_DBJVO xmo_DBJSU 
*               xmo_FACPBJ xmo_PVIJ2 xmo_PVIJ1  
*               xmo_DISTCOAL xmo_DISTCOALJ 
*               en sortie, d�pend de l'�tat du combustible
* cmogpas.inc : xmo_SOMTOFIS(2) en entr�e
************************************************************************
*
* variables locales xmo_DUUO2 xmo_MUXE xmo_MUKR xmo_MU
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
C
      CALL MOMESS('DANS MOCAPS_GEAR')
C
C Soit mo_ISAUTE est �gal � 0 et on doit passer partout
C Soit mo_ISAUTE est diff�rent de z�ro , 
C   dans ce cas il faut sauter 
C   - les rubriques � calculer pour tous les �tats
C   - les rubriques � calculer � la fois dans l'�tat 
C      classique-connect� (0) et dans l'�tat (2) 
C   - les rubriques � calculer uniquement dans l'�tat 
C      classique-connect� (0)
C   car ces calculs ont d�j� �t� faits
C
      IF (mo_ISAUTE .NE. 0)  GOTO 400
C
************************************************************************
* A CALCULER POUR TOUS LES ETATS
************************************************************************
      xmo_BETAC = xmo_TOFISS * xmo_GAMXE / xmo_XNAVOG
C
      xmo_GAMMA = xmo_COEF1_GAMMA + 
     &            xmo_COEF2_GAMMA * (xmo_TK - 273.D0)
C
C
C Coefficient de diffusion du cation au joint
      xmo_DUJTHMIX = 
     &   xmo_TDUJ(1,mo_MATER) * exp( - xmo_TDUJ(2,mo_MATER) / xmo_TK )
     &    +
     &   xmo_TDUJ(3,mo_MATER) * xmo_TOFISS**0.5D0 
     &   * exp(-xmo_TDUJ(4,mo_MATER)/xmo_TK)
C
      xmo_DUJ = xmo_DUJTHMIX +
     &          xmo_TDUJ(5,mo_MATER) * xmo_TOFISS
C
C Coefficient de diffusion du cation en volume
      xmo_DUTHMIX = 
     &   xmo_TDU(1,mo_MATER) * exp( - xmo_TDU(2,mo_MATER) / xmo_TK )
     &   +
     &   xmo_TDU(3,mo_MATER) * xmo_TOFISS**0.5D0 
     &   * exp(-xmo_TDU(4,mo_MATER)/xmo_TK)

      xmo_DU = xmo_DUTHMIX +
     &         xmo_TDU(5,mo_MATER) * xmo_TOFISS
C
C Coefficient de diffusion du X�non en volume
      xmo_DXETH = xmo_TDXE(1) * exp( - xmo_TDXE(2) / xmo_TK )
      xmo_DXEMIX = 
     &   xmo_TDXE(3) * xmo_TOFISS**0.5D0 * exp(-xmo_TDXE(4)/xmo_TK)
      xmo_DXEATH = xmo_TDXE(5) * xmo_TOFISS
C
      xmo_DXETHMIX = xmo_DXETH + xmo_DXEMIX
      xmo_DXE = xmo_DXETHMIX + xmo_DXEATH
C
C
C Recalcul du coefficient de diffusion du X�non en volume
C dans le cas de (U,Pu)O2
      IF (mo_MATER .EQ. 2) THEN
        xmo_DUUO2 = xmo_TDU(1,1) * 
     &              exp( - xmo_TDU(2,1) / xmo_TK ) +
     &              xmo_TDU(3,1) * xmo_TOFISS
        xmo_DXETH = xmo_DXETH * xmo_DU / xmo_DUUO2
        xmo_DXEMIX = xmo_DXEMIX * xmo_DU / xmo_DUUO2
        xmo_DXEATH = xmo_DXEATH * xmo_DU / xmo_DUUO2
        xmo_DXETHMIX = xmo_DXETH + xmo_DXEMIX
        xmo_DXE = xmo_DXETHMIX + xmo_DXEATH
      ENDIF
C
C Coefficient de diffusion du X�non sur le joint
      xmo_DXEJTHMIX = xmo_RAP_DXE_TH * xmo_DXETH +
     &                xmo_RAP_DXE_MIX * xmo_DXEMIX
      xmo_DXEJ = xmo_DXEJTHMIX + 
     &           xmo_RAP_DXE_ATH * xmo_DXEATH
C
      CALL MOBUCH (xmo_TK,xmo_DBUCH)
C
      xmo_OMSKT = xmo_OMEGA  / xmo_BOLTZ / xmo_TK 
C
      xmo_PVI =  4.D0 * xmo_PIZAHL * xmo_DU 
C
C
      xmo_VPERTURB = xmo_MUF_CHOC * xmo_PIZAHL * xmo_DREM**2.D0
      xmo_VINTERAC = 
     &   xmo_DREM * xmo_PIZAHL  * (2.D0 * xmo_RAT)**2.D0
C
      xmo_KG = 2.D0 * xmo_XNAVOG * 
     &         ( 2.D0 * xmo_VPERTURB * xmo_VINTERAC * xmo_TOFISS
     &           +
CCC     &           xmo_PIZAHL / 3.D0 * xmo_RAT * xmo_DXETHMIX )
     &           xmo_PIZAHL * 8.D0 * xmo_RAT * xmo_DXETHMIX )
     &         * xmo_FNB
C
C
      xmo_DUSTHMIX = xmo_TDUS(1,mo_MATER) * 
     &          exp( - xmo_TDUS(2,mo_MATER) / xmo_TK )
     &   +
     &          xmo_TDUS(3,mo_MATER) * xmo_TOFISS**0.5D0 
     &          * exp(-xmo_TDUS(4,mo_MATER)/xmo_TK)
C
      xmo_DUS = 
     &   xmo_DUSTHMIX
     &   +
     &   xmo_TDUS(5,mo_MATER) * xmo_TOFISS
C
      xmo_DSTHMIX = xmo_TDS(1) * 
     &         exp( - xmo_TDS(2) / xmo_TK )
     &   +
     &          xmo_TDS(3) * xmo_TOFISS**0.5D0 
     &          * exp(-xmo_TDS(4)/xmo_TK)
C
      xmo_DS = 
     &   xmo_DSTHMIX
     &   +
     &   xmo_TDS(5) * xmo_TOFISS
C
      xmo_DBVOL = 3.D0 / 4.D0 / xmo_PIZAHL * xmo_DUS * xmo_OMEGA 
C
      xmo_DBSUR = 3.D0 / 2.D0 / xmo_PIZAHL * xmo_DS * 
     &            xmo_OMEGA**(4.D0 / 3.D0)
C
      xmo_VBSUR = xmo_DSTHMIX * xmo_QS * 3.D0 * 
     &            xmo_OMEGA**(1.D0 / 3.D0) / 
     &            xmo_BOLTZ / xmo_TK**2.D0 *
     &            xmo_GRADTK
C
      xmo_VBVOL = xmo_DUSTHMIX * xmo_QV(mo_MATER) / 
     &            xmo_BOLTZ / xmo_TK**2.D0 *
     &            xmo_GRADTK
C
************************************************************************
* A CALCULER POUR  LES ETATS CLASSIQUE - CONNECTE  (0) 
* et 'EN RESTRUCTURATION'(2)
************************************************************************
      IF (mo_IET .EQ. 0  .OR. mo_IET .EQ. 2) THEN
C
        IF (mo_MODELE_DENSIFICATION .EQ. 1) THEN
          IF ( (xmo_VTPORI + xmo_VTPORJI) .GT. 0.D0 ) THEN
            xmo_VTPOR = xmo_VTPORTOT_LU * xmo_VTPORI 
     &                  / (xmo_VTPORI + xmo_VTPORJI)
            xmo_VTPORJ = xmo_VTPORTOT_LU * xmo_VTPORJI 
     &                  / (xmo_VTPORI + xmo_VTPORJI)
          ELSE
            xmo_VTPOR = 0.D0
            xmo_VTPORJ = 0.D0
          ENDIF
*
* les nombres de pores / m3 de combustible restent constants et �gaux
* � leur valeur initiale. Les volumes �voluent en proportion de leur 
* valeur initiale
          xmo_VD(3,2) = xmo_VTPOR
          xmo_VD(6,2) = xmo_VTPORJ
        ELSEIF (mo_MODELE_DENSIFICATION .EQ. 2) THEN
          xmo_VD(1,2) = xmo_CPOR_LU
          xmo_VD(3,2) = xmo_VTPOR_LU
          xmo_VD(4,2) = xmo_CPORJ_LU
          xmo_VD(6,2) = xmo_VTPORJ_LU
        ELSE
* Margaret calcule lui-meme toutes les variables concernant les pores
        ENDIF
C
C
C
        xmo_BJ = xmo_FACBJ * xmo_TOFISS
C
        xmo_FACRHO = 
     &    xmo_PROBU * xmo_EFISS / xmo_MAVOLI * xmo_TOFISS -
     &    1.D0 / (   xmo_TOREC * 
     &             ( 1.D0 + exp(-(xmo_TK-xmo_TKREC)/xmo_DTKREC) ) )
C
C distance de coalescence g�om�trique intra
        xmo_COESTF = (xmo_SOMTOFIS(2) - xmo_STFMIN) 
     &               / (xmo_STFMAX1 - xmo_STFMIN)
        xmo_COESTF = max(xmo_COESTF,0.D0)
        xmo_COETK = (xmo_TK - xmo_TKMIN) / (xmo_TKMAX - xmo_TKMIN)
        xmo_COETK = max(xmo_COETK,0.D0)
        xmo_COETK = min(xmo_COETK,1.D0)
        IF ( xmo_COESTF .LT. 1.D0 ) THEN
          xmo_DISTCOAL = 
     &      (xmo_MAX_DISTCOAL1 - xmo_MIN_DISTCOAL)
     &      * xmo_COESTF * xmo_COETK
     &      + xmo_MIN_DISTCOAL
        ELSE
          xmo_COESTF = (xmo_SOMTOFIS(2) - xmo_STFMAX1) 
     &               / (xmo_STFMAX2 - xmo_STFMAX1)
          xmo_DISTCOAL = 
     &    ((xmo_MAX_DISTCOAL2 - xmo_MAX_DISTCOAL1) * xmo_COESTF
     &      + xmo_MAX_DISTCOAL1 - xmo_MIN_DISTCOAL) * xmo_COETK
     &      + xmo_MIN_DISTCOAL
        ENDIF
C
        xmo_FACPBJ = 4.D0 / 3.D0 * xmo_PIZAHL * xmo_AGG  /
     &             LOG(1.D0+xmo_AXEJ)
*
        xmo_PVIJ1_B = 
     &      4.D0 * xmo_PIZAHL * xmo_DU * 
     &      ( 1.D0 - cos(xmo_TETBJ) ) / sin(xmo_TETBJ)
*
* par d�faut xmo_DUJ vaut z�ro
        xmo_PVIJ2 =  2.D0 * xmo_PIZAHL * xmo_EPJ * xmo_DUJ / 
     &               LOG(1.D0+xmo_AVJ)
*
        xmo_PVIJ1_POR = 
     &      4.D0 * xmo_PIZAHL * xmo_DU * 
     &      ( 1.D0 - cos(xmo_TETPORJ) ) / sin(xmo_TETPORJ)
*
*
      ENDIF
C
************************************************************************
* A CALCULER POUR  LES ETATS CLASSIQUE - CONNECTE  (0)  UNIQUEMENT
************************************************************************
C
      IF (mo_IET .EQ. 0) THEN
C
        xmo_KGJ = 8.D0 / 3.D0 * xmo_PIZAHL * xmo_DXEJTHMIX * xmo_AGG 
     &         * xmo_XNAVOG / LOG(1.D0+xmo_AXEJ) * xmo_FNBJ
C
        xmo_KGJ1 = xmo_XNAVOG * 2.D0 * xmo_VPERTURB * xmo_TOFISS
     &             * xmo_FNBJP
C
        xmo_KGJ2 = xmo_XNAVOG * 2.D0 * xmo_AGG * xmo_VINTERAC 
     &             / 3.D0 / xmo_EPJ
C
        xmo_FACGCJ = 4.D0 / 3.D0 * xmo_PIZAHL * xmo_AGG  /
     &             LOG(1.D0+xmo_ABJ)
C
        xmo_VBJVOL = xmo_VBVOL * ( 1.D0 - cos(xmo_TETBJ) ) /
     &               xmo_ATETBJ / (sin(xmo_TETBJ))**3.D0
C
        xmo_VBJSUR = xmo_VBSUR * ( 1.D0 - cos(xmo_TETBJ) ) /
     &            xmo_ATETBJ / sin(xmo_TETBJ)**2.D0 * 2.D0 / 3.D0
C
        xmo_DBJVO = xmo_DBVOL * ( 1.D0 - cos(xmo_TETBJ) ) /
     &              xmo_ATETBJ**2.D0 / sin(xmo_TETBJ)**3.D0 
C
        xmo_DBJSU = xmo_DBSUR * ( 1.D0 - cos(xmo_TETBJ) ) /
     &              xmo_ATETBJ**2.D0 / sin(xmo_TETBJ)**2.D0
C
C distance de coalescence g�om�trique inter
        xmo_COESTF = (xmo_SOMTOFIS(2) - xmo_STFMINJ) 
     &               / (xmo_STFMAXJ1 - xmo_STFMINJ)
        xmo_COESTF = max(xmo_COESTF,0.D0)
        xmo_COETK = (xmo_TK - xmo_TKMINJ) / (xmo_TKMAXJ - xmo_TKMINJ)
        xmo_COETK = max(xmo_COETK,0.D0)
        xmo_COETK = min(xmo_COETK,1.D0)
        IF ( xmo_COESTF .LT. 1.D0 ) THEN
          xmo_DISTCOALJ = 
     &      (xmo_MAX_DISTCOALJ1 - xmo_MIN_DISTCOALJ)
     &      * xmo_COESTF * xmo_COETK
     &      + xmo_MIN_DISTCOALJ
        ELSE
          xmo_COESTF = (xmo_SOMTOFIS(2) - xmo_STFMAXJ1) 
     &               / (xmo_STFMAXJ2 - xmo_STFMAXJ1)
          xmo_DISTCOALJ = 
     &    ((xmo_MAX_DISTCOALJ2 - xmo_MAX_DISTCOALJ1) * xmo_COESTF
     &      + xmo_MAX_DISTCOALJ1 - xmo_MIN_DISTCOALJ) * xmo_COETK
     &      + xmo_MIN_DISTCOALJ
        ENDIF
C
C
      ENDIF
C
************************************************************************
************************************************************************
************************************************************************
 400  CONTINUE
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C            
      RETURN
      END
      SUBROUTINE MOCAVF_GEAR
     &          (T,TOUT,NTEMPS,mo_IB,TAVANT,TAPRES)
C     ==============================
C
C
C     ******************************************************************
C     MOCAVF_GEAR : MOgador CAlcul des Variables en Fin de pas de temps 
C                   red�coup� ou non par la m�thode de Gear
C                                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
C
CDEBUG
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
C
***********************************************************************
* cmogpnu.inc : 
* cmogvar.inc : xmo_VG et xmo_VD mo_PRFOIS en entr�e et en sortie
*               mo_IET mo_NVG mo_NVD en entr�e
* cmogear.inc : mo_NEQG mo_NEQD xmo_YEXP mo_IGTAB mo_IDTAB mo_G_NEQ xmo_G_ATOL xmo_G_RTOL
*               mo_ISTATE mo_ITASK xmo_G_RWORK mo_ITOL mo_IOPT mo_G_LWR mo_G_IWORK
*               mo_G_LWI mo_JT
* cmogsor.inc : xmo_TAUXBETAC xmo_TAUXREMISOL xmo_TAUXSOURXE 
*               xmo_TAUXSORBUL xmo_TAUXSORPOR xmo_TAUXSORPOR
*               xmo_TAUXSORDIFJ xmo_TAUXSORPERC en entr�e et sortie
* cmoginf.inc : mo_PASSAGE mo_FREQIMPR en entr�e et sortie
*               mo_DEBUG en entr�e 
* cmogtem.inc : xmo_TEMPS pour impression seulement
************************************************************************
* Argument : En entr�e :
* TIN : temps d�but de calcul
* TOUT : temps fin de calcul
* NTEMPS : nombre d'intervalles de calculs entre TIN et TOUT
*            EN sortie :
* mo_IB : 0 normal , 
*         positif un basculement est d�tect� , 
*         n�gatif un basculement trop rapide est d�tect�
* TAVANT TAPRES : limites du sous pas de temps calcul� au moment o� 
*                 un basculement est detect�
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      DOUBLE PRECISION Y(mo_NEQMAX)
C 
CDEBUG     &     ,xmo_VG1(mo_NVGAMX),
CDEBUG     &     xmo_VD1(mo_NVDEMX),
CDEBUG     &                 xmo_CXE(mo_NNSPHMX),
CDEBUG     &                 xmo_CXEJ(mo_NNSPHMX)
C
      INTEGER PRPAS 
c      INTEGER JACDUM(1)
      EXTERNAL JACDUM
*
      EXTERNAL MOFDER_GEAR
*
      CALL MOMESS('DANS MOCAVF_GEAR')
      CALL MOMESI('entr�e mocavf_gear : mo_IET = ',mo_IET)
      CALL MOMESI('mo_NVG = ',mo_NVG)
      CALL MOMESI('mo_NVD = ',mo_NVD)
*
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
       DELTAT_SEUIL = 0.D0
*
* Initialisation de TAVANT et TAPRES
       TAVANT = 0.D0
       TAPRES = 0.D0
*
*
* methode de Gear
CDEBUG       WRITE(mo_DEBUG,*) 'debut mocavf_gear'
CDEBUG       WRITE(mo_DEBUG,*) 'T = ', T
CDEBUG       WRITE(mo_DEBUG,*) 'TOUT = ', TOUT
CDEBUG       WRITE(mo_DEBUG,*) 'NTEMPS = ', NTEMPS
CDEBUG       WRITE(mo_DEBUG,*) 'mo_PRFOIS = ',mo_PRFOIS
*
* On teste si une population a disparu. 
* Si c'est le cas, mo_PRFOIS est mis � 1 par MOTEXI
       CALL MOTEXI(mo_DISP)
*
CDEBUG
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans mocavf_gear entre motexi et moineq_gear'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
       IF (mo_PRFOIS .EQ. 1) THEN
         CALL MOINEQ_GEAR
       ENDIF
*
       IF (mo_PRFOIS .EQ. 0) THEN 
* Restitution des common et des tableaux de travail du solveur
         CALL MOSAUV(xmo_RSAV,mo_ISAV,
     &                       xmo_G_RWORK,xmo_G_RWORKSAV,
     &                       mo_G_IWORK,mo_G_IWORKSAV,
     &                       mo_G_NEQ,mo_G_LWR,mo_G_LWI,
     &                       mo_NEQG,mo_NEQD,mo_G_IEQSAV,
     &                       xmo_G_RTOL,xmo_G_RTOLSAV,
     &                       mo_IGTAB,mo_IGTABSAV,
     &                       mo_IDTAB,mo_IDTABSAV,2)
       ENDIF
*
* Initialisation de xmo_YEXP � partir de xmo_VG et xmo_VD (indice 2)
* et mise � zero de Y
       CALL MOINIT_GEAR(Y)
*
CDEBUG
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans mocavf_gear juste apres init de xmo_YEXP'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_YEXP'
CDEBUG       WRITE(20,*) (xmo_YEXP(IVGA), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_YEXP'
CDEBUG       WRITE(20,*) (xmo_YEXP(mo_NVG+IVGA), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
*      elimination des fausses variables
       DO I = 1,mo_NEQG
         Y(I) = xmo_YEXP(mo_IGTAB(I))
       ENDDO
       DO I = 1, mo_NEQD
         Y(I+mo_NEQG) = xmo_YEXP(mo_IDTAB(I) + mo_NVG)
       ENDDO
*
* Valeur du sous-pas de temps
*
*
       DELTAT = (TOUT-T)/NTEMPS
*
* Sauvegarde du pas de temps de r�f�rence. 
* C'est le m�me que DELTAT0 dans mocalv_gear.
       DELTAT_REF = DELTAT
*
*
* PRPAS est utilis� dans mowvar pour le calcul des bilans de gaz
* PRPAS vaut 1 pour le premier sous pas de temps du grans pas de 
* temps METEOR. Il vaut 0 pour les autres sous-pas de temps.
*
       PRPAS = 1
*
*
* A l'entr�e dans la boucle :
* Si on est pass� dans MOINEQ_GEAR ,mo_ISTATE = 1 et mo_PRFOIS = 1
* Sinon mo_ISTATE = 2
*
       IFIN = 0
 300   CONTINUE
*
************************************************************************
* On vient de reussir un pas de temps. 
* On remet l'option par d�faut pour la gestion des pas de temps internes
* au solveur
         mo_IOPT = 0
* Dans le cas o� on aurait �t� amen� pr�c�demment � diminuer le pas 
* de temps pour des pb de convergence, on cherche � r�augmenter 
* le pas de temps
         DELTAT = DELTAT * 2.D0
         IF (DELTAT .GT. DELTAT_REF) THEN
           DELTAT = DELTAT_REF
         ENDIF
         IF ((T + DELTAT) .GE. TOUT) THEN
           DELTAT = TOUT - T
           IFIN = 1
         ENDIF
************************************************************************
*
*
         TOUT_INTERM = T+DELTAT
*
         IF (mo_PRFOIS .EQ. 0) THEN
           DO I=1,mo_G_NEQ
             IF ( ABS(Y(I)) .GT. 0.D0 ) THEN
               xmo_G_ATOL(I) = xmo_G_RTOL(I)*ABS(Y(I))
             ELSE
               CALL MOATOL(I,xmo_G_ATOL_REF)
               xmo_G_ATOL(I) = xmo_G_ATOL_REF
             ENDIF
           ENDDO
           mo_ISTATE = 3
         ENDIF
*
*
         IF(TOUT_INTERM.GT.T) THEN
* pour eviter interpolation de la solution a t = tout_interm
* pour que la solution soit r�ellemnt calcul�e a t = tout_interm
           mo_ITASK = 4
           TCRIT = TOUT_INTERM
           xmo_G_RWORK(1) = TCRIT
*
*
************************************************************************
* Avant l'appel au solveur on stoke le contenu des COMMON du solveurs
* et les arguments dans des tableaux suffix�s _av
*
         CALL MOSAUV(xmo_RSAV_av,mo_ISAV_av,
     &             xmo_G_RWORK,xmo_G_RWORKSAV_av,
     &             mo_G_IWORK,mo_G_IWORKSAV_av,
     &             mo_G_NEQ,mo_G_LWR,mo_G_LWI,
     &             mo_NEQG,mo_NEQD,mo_G_IEQSAV_av,
     &             xmo_G_RTOL,xmo_G_RTOLSAV_av,
     &             mo_IGTAB,mo_IGTABSAV_av,
     &             mo_IDTAB,mo_IDTABSAV_av,1)
************************************************************************
* appel au solveur
           ICOMPT_CONV = 0
           ICOMPT_IWORK = 0
*
 200       CONTINUE
*
C
CDEBUG
CDEBUG           WRITE(mo_DEBUG,*) ' JUSTE AVANT APPEL LSODA : T = ',T,
CDEBUG     &                ' TOUT_INTERM = ',TOUT_INTERM
CDEBUG           WRITE(mo_DEBUG,*) 'mo_IET = ', mo_IET
CDEBUG           WRITE(mo_DEBUG,*) 'xmo_TK = ', xmo_TK
CDEBUG           WRITE(mo_DEBUG,*) 'mo_NVG = ', mo_NVG,' mo_NVD = ', mo_NVD
*
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG           ENDIF
CDEBUG
*
           CALL LSODA(MOFDER_GEAR,mo_G_NEQ,Y,T,TOUT_INTERM,
     &                mo_ITOL,xmo_G_RTOL,
     &                xmo_G_ATOL,mo_ITASK,mo_ISTATE,mo_IOPT,
     &                xmo_G_RWORK,mo_G_LWR,mo_G_IWORK,
     &                mo_G_LWI,JACDUM,mo_JT)
*
************************************************************************
************************************************************************
* Analyse des erreurs possibles
*
           IF(mo_ISTATE.LE.0) THEN
************************************************************************
CDEBUG
             WRITE(mo_DEBUG,*) ' Probleme lors de l''integration :',
     &                  mo_ISTATE
             IF(mo_G_IWORK(20).EQ.1) THEN
               WRITE(mo_DEBUG,*) ' Methode d''Adams'
             ELSE
               WRITE(mo_DEBUG,*) ' Methode BDF'
             ENDIF
CDEBUG
CESSAI//             WRITE(mo_DEBUG,*) ' Probleme lors de l''integration :',
CESSAI//     &                  mo_ISTATE
CESSAI//             IF(mo_G_IWORK(20).EQ.1) THEN
CESSAI//               WRITE(mo_DEBUG,*) ' Methode d''Adams'
CESSAI//             ELSE
CESSAI//               WRITE(mo_DEBUG,*) ' Methode BDF'
CESSAI//             ENDIF
*
CDEBUG             WRITE(mo_DEBUG,*) 'xmo_TK = ', xmo_TK 
*
************************************************************************
             IF(mo_ISTATE.EQ.-2) THEN
               TOLSF = xmo_G_RWORK(14)
CDEBUG
               WRITE(mo_DEBUG,*) ' facteur reglage tolerance = ',TOLSF
CDEBUG
               DO I=1,mo_G_NEQ
                 xmo_G_RTOL(I) = xmo_G_RTOL(I)*TOLSF
                 xmo_G_ATOL(I) = xmo_G_ATOL(I)*TOLSF
               ENDDO
               mo_ISTATE = 3
************************************************************************
             ELSEIF ( (mo_ISTATE.EQ.-1 .OR. mo_ISTATE.EQ.-5 .OR.
     &                 mo_ISTATE.EQ.-4 ) ) THEN
CDEBUG
               WRITE(mo_DEBUG,*) ' Difficutes de convergence '
               WRITE(mo_DEBUG,*) 'ICOMPT_CONV = ',ICOMPT_CONV
CDEBUG
*
* Copie des tableaux _av dans les tableaux SAV
               CALL MOCOPY(
     &         xmo_RSAV_av, mo_ISAV_av,
     &         mo_G_IEQSAV_av, xmo_G_RWORKSAV_av,
     &         mo_G_IWORKSAV_av, xmo_G_RTOLSAV_av,
     &         mo_IGTABSAV_av, mo_IDTABSAV_av,
     &         xmo_RSAV, mo_ISAV,
     &         mo_G_IEQSAV, xmo_G_RWORKSAV,
     &         mo_G_IWORKSAV, xmo_G_RTOLSAV,
     &         mo_IGTABSAV, mo_IDTABSAV)
* Restitution des common et des tableaux de travail du solveur
               CALL MOSAUV(
     &         xmo_RSAV,mo_ISAV,
     &         xmo_G_RWORK,xmo_G_RWORKSAV,
     &         mo_G_IWORK,mo_G_IWORKSAV,
     &         mo_G_NEQ,mo_G_LWR,
     &         mo_G_LWI,mo_NEQG,mo_NEQD,mo_G_IEQSAV,
     &         xmo_G_RTOL,xmo_G_RTOLSAV,
     &         mo_IGTAB,mo_IGTABSAV,
     &         mo_IDTAB,mo_IDTABSAV,2)
* Reinitialisation de Y
               CALL MOINIT_GEAR(Y)
               DO I = 1,mo_NEQG
                 Y(I) = xmo_YEXP(mo_IGTAB(I))
               ENDDO
               DO I = 1, mo_NEQD
                 Y(I+mo_NEQG) = xmo_YEXP(mo_IDTAB(I) + mo_NVG)
               ENDDO
* Reinitialisation de T
               T = TOUT_INTERM - DELTAT
* Reinitialisation de xmo_G_ATOL
               DO I=1,mo_G_NEQ
                 IF ( ABS(Y(I)) .GT. 0.D0 ) THEN
                   xmo_G_ATOL(I) = xmo_G_RTOL(I)*ABS(Y(I))
                 ELSE
                   CALL MOATOL(I,xmo_G_ATOL_REF)
                   xmo_G_ATOL(I) = xmo_G_ATOL_REF
                 ENDIF
               ENDDO
*
               IF (ICOMPT_CONV .EQ. 0) THEN
*              ----------------------------
                 ICOMPT_CONV = ICOMPT_CONV + 1
* On tente un depart
CDEBUG
                 WRITE(mo_DEBUG,*) ' Premiere tentative : ',
     &                      'on repart avec mo_ISTATE = 1  '
CDEBUG 
                 mo_ISTATE = 1
* On refait fonctionner le solveur
                 GOTO 200
               ELSEIF (ICOMPT_CONV .LT. 10) THEN
*              ---------------------------------
                 ICOMPT_CONV = ICOMPT_CONV + 1
*
                 IF (DELTAT .GT. DELTAT_SEUIL)  THEN
*                ===================================
* On tente de red�couper le pas de temps
CDEBUG
                   WRITE(mo_DEBUG,*) ' tentative numero ', 
     &             ICOMPT_CONV,
     &             ' : on repart avec DELTAT divise par 2  '
CDEBUG
                   DELTAT = DELTAT / 2.D0 
                   TOUT_INTERM = T + DELTAT
* au cas ou IFIN vaudrait 1, il faut le remettre a zero car on ne sera pas arrive a TOUT
* avec un DELTAT reduit
                   IFIN = 0
*
                 ELSEIF (mo_G_IWORK(6) .LT. 10000) THEN
*                ===================================
*                on a d�j� un pas de temps tres petit on tente 
*                d'augmenter le nombre de redecoupages dans le solveur
CDEBUG
                   WRITE(mo_DEBUG,*) ' tentative numero ', 
     &             ICOMPT_CONV,
     &             ' : on repart avec un decoupage interne ',
     &             'multiplie par 2'
CDEBUG
                   ICOMPT_IWORK = ICOMPT_IWORK + 1
                   mo_IOPT = 1
                   mo_G_IWORK(6) = 500 * (2**ICOMPT_IWORK)
                 ENDIF
*                =====
*
                 mo_ITASK = 4
                 TCRIT = TOUT_INTERM
                 xmo_G_RWORK(1) = TCRIT
                 IF (mo_PRFOIS .EQ. 0) THEN
                   mo_ISTATE = 3
                 ELSE
                   mo_ISTATE = 1
                 ENDIF
* On refait fonctionner le solveur
                 GOTO 200
               ELSE
*              ----
CDEBUG
                 WRITE(mo_DEBUG,*) 'ICOMPT_CONV = ',ICOMPT_CONV
                 WRITE(mo_DEBUG,*) ' Plus de tentative supplementaire'
CDEBUG
                 STOP
               ENDIF
*              -----
************************************************************************
             ELSE
               STOP
             ENDIF
           ELSE IF(mo_ISTATE.NE.2) THEN
             WRITE(mo_DEBUG,*) 
     &       ' Arret a une date inattendue : t = ',T
           ENDIF
*
* fin du traitement du cas o� le calcul s'est mal pass�
************************************************************************
* Dans le cas o� le calcul s'est bien pass� :
*
C
CDEBUG           WRITE(mo_DEBUG,*) 'le calcul s est bien passe'
C
*
           IF(mo_ISTATE.EQ.2) THEN
             DO I = 1, mo_NEQG
               xmo_YEXP(mo_IGTAB(I)) = Y(I)
             ENDDO
             DO I = 1, mo_NEQD
               xmo_YEXP(mo_IDTAB(I)+ mo_NVG) = Y(I+mo_NEQG)
             ENDDO
*
*
*
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans mocavf_gear'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_YEXP'
CDEBUG       WRITE(20,*) (xmo_YEXP(IVGA), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_YEXP'
CDEBUG       WRITE(20,*) (xmo_YEXP(mo_NVG+IVGA), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
*
* retour aux tableaux utilis�s hors solveur
*
             DO IVGA = 1,mo_NVG
               xmo_VG(IVGA,1) = xmo_YEXP(IVGA)
             ENDDO
*
             DO IVDE = 1,mo_NVD
               xmo_VD(IVDE,1) = xmo_YEXP(mo_NVG+IVDE)
             ENDDO
*
************************************************************************
************************************************************************
* test de basculement d'�tat du combustible
************************************************************************
*
             CALL MOMESS('dans mocavf_gear, avant test')
             CALL MOMESV('TOUT_INTERM = ',TOUT_INTERM)
*
* Test pour d�tection d'un basculement
*
             CALL MOBASC_GEAR(mo_IB,1) 
*       
* Cas du basculement trop rapide
             IF (mo_IB .LT. 0) THEN
               TAVANT = TOUT_INTERM - DELTAT
               TAPRES = TOUT_INTERM
*
CDEBUG               WRITE(mo_DEBUG,*) 'Basculement trop rapide entre : ',
CDEBUG     &                     TAVANT, ' et ',TAPRES
CDEBUG               WRITE(mo_DEBUG,*) 'mo_NVG = ',mo_NVG,
CDEBUG     &                          ' mo_NVD = ',mo_NVD
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_VG ='
CDEBUG               WRITE(mo_DEBUG,*) (xmo_VG(IVGA,1) ,IVGA = 1,mo_NVG)
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_VD ='
CDEBUG               WRITE(mo_DEBUG,*) (xmo_VD(IVDE,1) ,IVDE = 1,mo_NVD)
*
               GOTO 100
             ENDIF
*
*
* Pour tout ce qu il y a ci-dessous le calcul effectue est valable
             mo_PRFOIS = 0
* calcul du temps atteint
             CALL MOCTPS(DELTAT)
*
CDEBUG             WRITE(mo_DEBUG,*) 'juste apres xmo_TEMPS =',xmo_TEMPS
CDEBUG             WRITE(mo_DEBUG,*) 'TOUT_INTERM = ',TOUT_INTERM
CDEBUG             WRITE(mo_DEBUG,*) 'TOUT = ',TOUT, 'mo_IB = ',mo_IB
*
*
* Impressions
             IIMPR = 0
* compteur des passages dans le sous-programme mowvar
             mo_PASSAGE = mo_PASSAGE + 1
*
* test decidant de l'impression effective des variables
C 
CDEBUG             WRITE(mo_DEBUG,*) 'mocavf_gear avant appel mowvar'
C
             IF ( MOD(mo_PASSAGE,mo_FREQIMPR) .EQ. 0 ) THEN
              IIMPR = 1 
             ENDIF
             IF ( IFIN .EQ. 1) THEN
              IIMPR = 1 
             ENDIF             
             CALL MOWVAR(IIMPR,DELTAT,PRPAS)
             PRPAS = 0
*
* mise � jour des tableaux
             DO IVGA = 1, mo_NVGAMX
               xmo_VG(IVGA,2) = xmo_VG(IVGA,1)
             ENDDO
*
             DO IVDE = 1, mo_NVDEMX
               xmo_VD(IVDE,2) = xmo_VD(IVDE,1)
             ENDDO
*
* mise � jour pour le calcul du bilan de gaz
*
             xmo_TAUXBETAC(2) =  xmo_TAUXBETAC(1)
             xmo_TAUXREMISOL(2) =  xmo_TAUXREMISOL(1)
             xmo_TAUXSOURXE(2) =  xmo_TAUXSOURXE(1)
             xmo_TAUXSORBUL(2) =  xmo_TAUXSORBUL(1)
             xmo_TAUXSORPOR(2) =  xmo_TAUXSORPOR(1)
             xmo_TAUXOUTSE(2) = xmo_TAUXOUTSE(1)
             xmo_TAUXOUTPERC(2) = xmo_TAUXOUTPERC(1)
             xmo_TAUXOUTEJEC(2) = xmo_TAUXOUTEJEC(1)
*
* essaiLOC
* mise � jour des concentrations au voisinnage des bulles
* et des pores et de l'instant xmo_TEMPS0
             CALL MOCLOC0
* essaiLOC
*
*
* Cas du basculement effectif : Le calcul est valable.
* Mais on ne doit pas continuer dans la boucle
* Et on devra appeler moineq_gear
             IF (mo_IB .GT. 0) THEN
               TAVANT = TOUT_INTERM - DELTAT
               TAPRES = TOUT_INTERM
               mo_PRFOIS = 1
CDEBUG               WRITE(mo_DEBUG,*)
CDEBUG     &         'dans mocavf_gear : cas du basculement effectif'
CDEBUG               WRITE(mo_DEBUG,*) 
CDEBUG     &              'basculement effectif mo_IB = ',mo_IB
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_TEMPS =',xmo_TEMPS
CDEBUG               WRITE(mo_DEBUG,*) 'TAPRES = ',TAPRES
               GOTO 100
             ENDIF
*
* Cas o� une population enti�re s'annule. Le calcul est valable
* mais on ne doit pas continuer dans la boucle
* Et on devra appeler moineq_gear
CDEBUG
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG               WRITE(mo_DEBUG,*) 
CDEBUG     &           'juste avant le MOTEXI qui suit le calcul'
CDEBUG               WRITE(mo_DEBUG,*) 'mo_NVG = ',mo_NVG,
CDEBUG     &                          ' mo_NVD = ',mo_NVD
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_VG ='
CDEBUG               WRITE(mo_DEBUG,*) (xmo_VG(IVGA,2) ,IVGA = 1,mo_NVG)
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_VD ='
CDEBUG               WRITE(mo_DEBUG,*) (xmo_VD(IVDE,2) ,IVDE = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
             CALL MOTEXI(mo_DISP)
CDEBUG
CDEBUG             WRITE(mo_DEBUG,*) 'apres MOTEXI mo_DISP = ', mo_DISP
CDEBUG
             IF (mo_DISP .EQ. 1) THEN
               TAVANT = TOUT_INTERM - DELTAT
               TAPRES = TOUT_INTERM
               mo_IB = 999
* mo_PRFOIS est mis � 1 par MOTEXI
*
CDEBUG               WRITE(mo_DEBUG,*)
CDEBUG     &    'dans mocavf_gear : cas de la disparition d une population'
CDEBUG               WRITE(mo_DEBUG,*) 'mo_PRFOIS = ', mo_PRFOIS
CDEBUG               WRITE(mo_DEBUG,*) 'TAVANT= ',TAVANT
CDEBUG               WRITE(mo_DEBUG,*) 'xmo_TEMPS =',xmo_TEMPS
CDEBUG               WRITE(mo_DEBUG,*) 'TAPRES = ',TAPRES
CDEBUG               WRITE(mo_DEBUG,*) 'TOUT = ',TOUT
*
               GOTO 100
             ENDIF
*
*
* fermeture du if istate .eq. 2
           ENDIF
*
* fermeture du if tout .gt. t
         ENDIF
*
* test de sortie normale de la boucle 300
*
CDEBUG         WRITE(mo_DEBUG,*) 'avant test IFIN =', IFIN
*
         IF ( IFIN .EQ. 1) THEN
           GOTO 400
         ELSEIF (TOUT_INTERM + DELTAT .GE. TOUT) THEN
           DELTAT = TOUT - TOUT_INTERM
           IFIN = 1
         ENDIF         
* fermeture de la boucle sur INSTANT
       GOTO 300
*
*
 400  CONTINUE
*
* fin m�thode de Gear
*
*
 100  CONTINUE   
*
************************************************************************
* Stockage des common et des tableaux de travail du solveur
         CALL MOSAUV(
     &     xmo_RSAV,mo_ISAV,
     &     xmo_G_RWORK,xmo_G_RWORKSAV,
     &     mo_G_IWORK,mo_G_IWORKSAV,
     &     mo_G_NEQ,mo_G_LWR,
     &     mo_G_LWI,mo_NEQG,mo_NEQD,mo_G_IEQSAV,
     &     xmo_G_RTOL,xmo_G_RTOLSAV,
     &     mo_IGTAB,mo_IGTABSAV,
     &     mo_IDTAB,mo_IDTABSAV,1)
*
CDEBUG             WRITE(mo_DEBUG,*) 'fin mocavf xmo_TEMPS =',xmo_TEMPS
CDEBUG             WRITE(mo_DEBUG,*) 'TOUT = ',TOUT
*      
      RETURN
      END
      SUBROUTINE MOCEXP(xmo_VAT,xmo_RB,mo_BBJT,xmo_TEXP2)
C     =================
C
C
C     ******************************************************************
C     MOCEXP : MOgador calcul de la deuxi�me exponentielle dans les 
C     expressions de l'�volution des volumes des bulles et des pores                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
C
************************************************************************
* cmogpas.inc : xmo_OMSKT xmo_GAMMA en entr�e
* cmogppr.inc : xmo_VATXES xmo_TETBJ xmo_TETPORJ en entr�e
************************************************************************
* xmo_VAT : Volume disponible par atome (m3)
* xmo_RB : Rayon de la bulle ou du pore (m)
* mo_BBJT : 1  bulle intra 
*         : 2  bulle inter 
*         : 3  tunnel 
*         : 4  pore intra 
*         : 5  pore inter
*         : 6  bulles de rim
*         : 7  pores de rim 
* xmo_TEXP2 : deuxi�me terme dans l'expression du flux de lacunes ()
************************************************************************
*
      CALL MOMESS('DANS MOCEXP')
*
*
*
* Pression interne xmo_PRIB
* 
      CALL MOPINT(xmo_VAT,xmo_PRIB,mo_PB)
*
*
      IF (mo_PB .EQ. 0) THEN
*     +++++++++++++++++++++++++++++++++
* cas o� le gaz est � l'�tat fluide
*
* le calcul de xmo_TEXP2 est diff�rent selon la forme de la bulle,
* pore ou tunnel
*
        IF ( mo_BBJT .EQ. 1 .OR. mo_BBJT .EQ. 4
     &      .OR. mo_BBJT .EQ. 6 .OR. mo_BBJT .EQ. 7) THEN
*       _________________________________________________
*       cas des cavit�s rondes
*       _________________________________________________ 
*
          xmo_PSURF = xmo_PRIB - 2.D0 * xmo_GAMMA / xmo_RB
* 
* 
        ELSEIF (mo_BBJT .EQ. 2) THEN
*       ________________________________________________
*       cas des bulles inter (toujours lenticulaires)
*       ________________________________________________
*
          xmo_PSURF = xmo_PRIB - 
     &            2.D0 * xmo_GAMMA * sin(xmo_TETBJ) / xmo_RB
*
*
        ELSEIF (mo_BBJT .EQ. 5) THEN
*       ________________________________________________
*       cas des pores inter (pas forcement lenticulaires)
*       ________________________________________________
*
          xmo_PSURF = xmo_PRIB - 
     &            2.D0 * xmo_GAMMA * sin(xmo_TETPORJ) / xmo_RB 
        ELSE
*       ________________________________________________
*       cas des tunnels
*       ________________________________________________
          xmo_PSURF = xmo_PRIB - 
     &             xmo_GAMMA * sin(xmo_TETBJ) / xmo_RB
*
*
        ENDIF
*       _____
*
*
        IF ((- xmo_PSURF * xmo_OMSKT) .LT. 10.D0) THEN
            xmo_TEXP2 = 
     &        exp(- xmo_PSURF * xmo_OMSKT)
        ELSE
            xmo_TEXP2 = exp(10.D0)
        ENDIF
*
*
      ELSE
*     ++++
* Cas o� le X�non est � l'�tat solide
        xmo_TEXP2 = 0.D0
      ENDIF
*     +++++
*
*   
      RETURN
      END
      SUBROUTINE MOCLOC(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_RHOD,xmo_TEMPS_COURANT,
     &
     &      xmo_CXERPORLOC,xmo_CXERPORLOCM,
     &      xmo_FREQ1_CXERBLOC,xmo_FREQ2_CXERBLOC,xmo_FREQ3_CXERBLOC)
C     ==============================================================
C
C
C     ******************************************************************
C     MOCLOC : MOgador Calcul des concentrations LOCales au voisinnage
C              des bulles intra et des pores intra                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogloc.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogphy.inc'
CDEBUG
      INCLUDE 'cmogent.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
C
***********************************************************************
* cmogloc.inc : xmo_CXERPORLOC0 en entr�e 
* cmogvar.inc : mo_IET en entr�e
* cmogppr.inc : xmo_RHOLIM xmo_RHODL xmo_MARGRHO_FR xmo_DREM en entr�e 
* cmogpas.inc : xmo_BETAC  en entr�e
* cmogpnu.inc : mo_NGG en entr�e 
* cmogmat.inc : xmo_TVGG en entr�e 
* cmophy.inc : xmo_PIZAHL en entr�e 
************************************************************************
* On suppose que les bulles occupent les sommets d'un r�seau cubique
* de cot� xmo_ACUBE_B
* xmo_SURF_RMS = Surface d�limitant la r�gion de remise en solution, pour
* une bulle, ou dans un cube �l�mentaire
* xmo_VOL_RMS = Volume de la r�gion de remise en solution, pour
* une bulle, ou dans un cube �l�mentaire
* xmo_FREQ1_CXERBLOC xmo_FREQ2_CXERBLOC xmo_FREQ3_CXERBLOC = fr�quence (s-1) 
* apparaissant dans l'�quation d'�volution de xmo_CXERBLOC 
* (concentration locale en gaz au voisinnage
* des bulles, c'est � dire dans la r�gion de remise en solution)
************************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERPORLOC(mo_NNSPHMX)
*
      CALL MOMESS('DANS MOCLOC')
*
CDEBUG
CDEBUG      IF (mo_FLAG .EQ. 1) THEN 
CDEBUG        WRITE(20,*) 'dans mocloc debut'
CDEBUG      ENDIF
CDEBUG
************************************************************************
      IF (mo_IET .EQ. 2) THEN
*
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM + xmo_RHODL - xmo_MARGRHO_FR pour les calcul de xmo_FR xmo_A 
* Sinon, on a des divisions par zero
*
         xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
         xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
*
      ELSE
          xmo_FR = 0.D0
      ENDIF
*
************************************************************************
* coefficient de diffusion du Xe tenant compte de la pr�sence de 
* dislocations
*
      CALL MODXEA(xmo_RHOD,xmo_DXEAM)
*
************************************************************************
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz intra dissous 
*
      CALL MOMOYE (xmo_CXE, xmo_TVGG, mo_NGG, mo_NNSPHMX, xmo_CXEM)
************************************************************************
* calcul des coefficient de remise en solution, de destruction
*
      IF (xmo_VTB .GT. 0.D0) THEN
        CALL MOVLAT(xmo_CXEB,xmo_VTB,1,xmo_VAT)
        CALL MORSOL(xmo_CB,xmo_VTB,xmo_VAT,xmo_RB,1,
     &              xmo_BB,xmo_BBB)
      ENDIF
*
      IF (xmo_VTPOR .GT. 0.D0) THEN
        CALL MOVLAT(xmo_CXEPOR,xmo_VTPOR,4,xmo_VAT) 
        CALL MORSOL(xmo_CPOR,xmo_VTPOR,xmo_VAT,xmo_RPOR,4,
     &              xmo_BPOR,xmo_BPORB)
      ENDIF 
*
*
*
************************************************************************
************************************************************************
************************************************************************
* Traitement des pores : Le volume de remise en solution au voisinnage
* des pores �tant faible et la quantit� de gaz contenu dedans �tant faible
* On d�couple ce calcul du reste
* Calcul de xmo_CXERPORLOC : concentration en gaz, locale, au voisinnage 
* des pores
************************************************************************
************************************************************************
************************************************************************
*
      IF (xmo_CPOR .GT. 0.D0 .AND. xmo_RPOR .GT. 0.D0) THEN
        xmo_XDREMRPOR = 1.D0 + xmo_DREM / xmo_RPOR
        xmo_FRVOLPOR = xmo_VTPOR / (1.D0 - xmo_FR) 
     &                 * (xmo_XDREMRPOR**3.D0 - 1.D0)
        xmo_VOLPPOR = (1.D0 - xmo_FR) / xmo_CPOR * xmo_FRVOLPOR
        xmo_ACUBE_POR = 
     &       ( xmo_CPOR / (1.D0 - xmo_FR) )**(-1.D0/3.D0) 
        xmo_DIST_SURF_POR = SQRT(3.D0) * xmo_ACUBE_POR 
     &                      - 2.D0 * xmo_RPOR
        IF (xmo_DIST_SURF_POR .GT. 2.D0 * xmo_DREM) THEN        
          xmo_LDPOR = 1.D0 / 
     &        (  2.D0 * xmo_XDREMRPOR**2.D0 
     &           / (xmo_DIST_SURF_POR - xmo_DREM) 
     &         +
     &           2.D0 / xmo_DREM )
*
          xmo_LPOR  = 1.D0 / 
     &        (  2.D0 * xmo_XDREMRPOR**2.D0 
     &           / (xmo_DIST_SURF_POR - xmo_DREM) )
        ELSE
          WRITE(20,*) 'xmo_DIST_SURF_POR inf�rieure a 2*xmo_DREM'
          WRITE(20,*) 'xmo_CPOR = ',xmo_CPOR,' xmo_RPOR = ',xmo_RPOR
          WRITE(20,*) 'xmo_VTPOR = ',xmo_VTPOR
          STOP
        ENDIF
*
        xmo_FREQAPOR = 3.D0 * xmo_DXEAM / xmo_RPOR 
     &                 / ( xmo_XDREMRPOR**3.D0 - 1.D0 ) / xmo_LDPOR 
     &                 +
     &                 xmo_BPORB
*
        xmo_GROUPEPOR = 3.D0 * xmo_DXEAM / xmo_RPOR 
     &                 / ( xmo_XDREMRPOR**3.D0 - 1.D0 ) / xmo_LPOR
*
        IF ( xmo_FREQAPOR .GT. 0.99D20) THEN
          xmo_EXPPOR = 0.D0
          xmo_1MOINSEXPPOR = 1.D0
        ELSE
          xmo_EXPPOR = 
     &      exp( -xmo_FREQAPOR*(xmo_TEMPS_COURANT - xmo_TEMPS0) )
          xmo_1MOINSEXPPOR = 1.D0 - xmo_EXPPOR
        ENDIF 
*
        xmo_1SUR_VOLPPOR_CPOR = 1.D0 / xmo_VOLPPOR / xmo_CPOR
*
************************************************************************
* BOUCLE SUR LES COURONNES DU GRAIN
************************************************************************
*
        DO IVGA = 1, mo_NGG
          IF (xmo_CXEM .GT. 0.D0) THEN
            xmo_PROFIL = xmo_CXE(IVGA) / xmo_CXEM
          ELSE
            xmo_PROFIL = 1.D0
          ENDIF
*
          xmo_CXERPORLOC(IVGA) =
     &      1.D0 / xmo_FREQAPOR *
     &      ( xmo_GROUPEPOR * xmo_CXE(IVGA) / (1.D0 - xmo_FR) + 
     &        (xmo_BPOR - xmo_BPORB) * xmo_1SUR_VOLPPOR_CPOR 
     &        * xmo_CXEPOR * xmo_PROFIL  + xmo_BETAC ) 
     &      * xmo_1MOINSEXPPOR
     &      +
     &      xmo_CXERPORLOC0(IVGA) * xmo_EXPPOR
        ENDDO
************************************************************************
* FIN BOUCLE SUR LES COURONNES DU GRAIN
************************************************************************ 
      ELSE
        DO IVGA = 1, mo_NGG
          xmo_CXERPORLOC(IVGA) = 0.D0
        ENDDO
      ENDIF
*
* Calcul de la moyenne de xmo_CXERPORLOC sur le grain
*
      CALL MOMOYE (xmo_CXERPORLOC, xmo_TVGG, mo_NGG, mo_NNSPHMX, 
     &             xmo_CXERPORLOCM)
************************************************************************
************************************************************************
************************************************************************
* Fin de traitement des pores
************************************************************************
************************************************************************
************************************************************************
************************************************************************
*
*
************************************************************************
************************************************************************
* Calcul des grandeurs utiles pour le traitement des bulles
************************************************************************
************************************************************************
      IF (xmo_CB .GT. 0.D0 .AND. xmo_RB .GT. 0.D0 
     &    .AND. xmo_CXEB .GT. 0.D0) THEN
*     +++++++++++++++++++++++++++++++++++++++++++
*
        xmo_ACUBE_B = 
     &       ( xmo_CB / (1.D0 - xmo_FR + xmo_VTB) )**(-1.D0/3.D0)
        xmo_XDREMRB = 1.D0 + xmo_DREM / xmo_RB 
        xmo_RSPHERE_RMS =  xmo_RB  +  xmo_DREM
* 
        IF ( xmo_RSPHERE_RMS  .LT.  xmo_ACUBE_B / 2.D0 ) THEN
* Les sph�res de remise en solution ne s'interpenetrent pas
          mo_INDIC_CXERBLOC = 1 
          xmo_FRVOLPB = 
     &       xmo_VTB / (1.D0 - xmo_FR) * (xmo_XDREMRB**3.D0 - 1.D0)
          xmo_SURF_RMS = 4.D0 * xmo_PIZAHL * xmo_RSPHERE_RMS**2
        ELSEIF ( xmo_RSPHERE_RMS  .LT.  
     &           SQRT(2.D0) * xmo_ACUBE_B / 2.D0 ) THEN
* Les sph�res de remise en solution s'inter�netrent seulemnt sur 
* les ar�tes du cube
          mo_INDIC_CXERBLOC = 2 
          xmo_ASUR2RS = xmo_ACUBE_B / 2.D0 / xmo_RSPHERE_RMS
          xmo_FRVOLPB = 
     &       xmo_VTB / (1.D0 - xmo_FR) * 
     &       ( xmo_XDREMRB**3.D0 * 
     &         ( 1.D0 - 1.5D0 * (1.D0 - xmo_ASUR2RS)**2 
     &                        * (2.D0 + xmo_ASUR2RS) )
     &        - 1.D0 )
CDEBUG
CDEBUG      IF (mo_FLAG .EQ. 1 .AND. xmo_FRVOLPB .EQ. 0) THEN 
CDEBUG        WRITE(20,*) 'xmo_ASUR2RS = ',xmo_ASUR2RS
CDEBUG        WRITE(20,*) 'mo_LLL = ',mo_LLL,'mo_IGRIGR = ',mo_IGRIGR,
CDEBUG     &              'mo_III = ',mo_III
CDEBUG      ENDIF     
CDEBUG
          xmo_SURF_RMS = xmo_PIZAHL * xmo_RSPHERE_RMS * 
     &       ( 6.D0 * xmo_ACUBE_B - 8.D0 * xmo_RSPHERE_RMS )
        ELSE
* On peut consid�rer que la remise en solution se fait en volume car 
* le volume des sph�res de remise en solution repr�sente 96% du 
* volume du cube
          mo_INDIC_CXERBLOC = 0
        ENDIF
*
CDEBUG
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI .AND. mo_FLAG .EQ. 1) THEN
CDEBUG*
CDEBUG             WRITE(20,*) 'mo_INDIC_CXERBLOC = ',mo_INDIC_CXERBLOC
CDEBUG        ENDIF
CDEBUG
        IF (mo_INDIC_CXERBLOC .EQ. 1 .OR. 
     &      mo_INDIC_CXERBLOC .EQ. 2) THEN
CDEBUG
CDEBUG      IF (mo_FLAG .EQ. 1) THEN 
CDEBUG        WRITE(20,*) 'dans mocloc avant cal freq cas indic = 1'
CDEBUG        WRITE(20,*) 'xmo_FRVOLPB = ',xmo_FRVOLPB
CDEBUG      ENDIF     
CDEBUG
* On se servira du calcul de xmo_CXERBLOC
          xmo_VOL_RMS = (1.D0 - xmo_FR) / xmo_CB * xmo_FRVOLPB
          xmo_DIST_SURF_B = SQRT(3.D0) * xmo_ACUBE_B - 2.D0 * xmo_RB
          xmo_FREQ1_CXERBLOC = 
     &        xmo_DXEAM * 2.D0 * xmo_SURF_RMS / xmo_VOL_RMS
     &        / (xmo_DIST_SURF_B - xmo_DREM) / (1.D0 - xmo_FRVOLPB)
CDEBUG
CDEBUG      IF (mo_FLAG .EQ. 1) THEN 
CDEBUG        WRITE(20,*) 'dans mocloc milieu cal freq cas indic = 1'
CDEBUG      ENDIF
CDEBUG
          xmo_FREQ2_CXERBLOC =
     &        4.D0 * xmo_PIZAHL * xmo_RB**2 * xmo_DXEAM 
     &        / xmo_VOL_RMS * 2.D0 / xmo_DREM 
          xmo_FREQ3_CXERBLOC =
     &        (xmo_BB - xmo_BBB) / xmo_VOL_RMS / xmo_CB 
        ELSE
* On veut que xmo_CXERBLOC soit �gal � xmo_CXE
          xmo_FREQ1_CXERBLOC = 1.D20
          xmo_FREQ2_CXERBLOC = 0.D0
          xmo_FREQ3_CXERBLOC = 0.D0
        ENDIF       
*
      ELSE
*     ++++
* On veut que xmo_CXERBLOC soit �gal � xmo_CXE
          xmo_FREQ1_CXERBLOC = 0.D0
          xmo_FREQ2_CXERBLOC = 0.D0
          xmo_FREQ3_CXERBLOC = 0.D0
      ENDIF
*     +++++
*
************************************************************************
************************************************************************
* Fin du calcul des grandeurs utiles pour le traitement des bulles
************************************************************************
************************************************************************
*
*
CDEBUG        WRITE(20,*) 'xmo_TK = ', xmo_TK
CDEBUG        WRITE(20,*) 'xmo_CB = ', xmo_CB
CDEBUG        WRITE(20,*) 'xmo_RB = ', xmo_RB
CDEBUG        WRITE(20,*) 'xmo_VTB = ', xmo_VTB
CDEBUG        WRITE(20,*) 'xmo_FR = ', xmo_FR
CDEBUG        WRITE(20,*) 'xmo_CPOR = ', xmo_CPOR
CDEBUG        WRITE(20,*) 'xmo_RPOR = ', xmo_RPOR
CDEBUG        WRITE(20,*) 'xmo_XDREMRB =',xmo_XDREMRB
CDEBUG        WRITE(20,*) 'xmo_FRVOLPB = ',xmo_FRVOLPB
CDEBUG        WRITE(20,*) 'xmo_DIST_SURF_B =',xmo_DIST_SURF_B
CDEBUG        WRITE(20,*) 'xmo_DIST_SURF_POR =',xmo_DIST_SURF_POR
CDEBUG        WRITE(20,*) 'mo_LLL = ', mo_LLL, ' mo_IGRIGR = ',mo_IGRIGR,
CDEBUG     &              'mo_III = ', mo_III
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI .AND. mo_FLAG .EQ. 1) THEN
CDEBUG*
CDEBUG             WRITE(20,*) 'xmo_TEMPS_COURANT = ',xmo_TEMPS_COURANT
CDEBUG             WRITE(20,*) 'xmo_FRVOLPB = ', xmo_FRVOLPB
CDEBUG             WRITE(20,*) 'xmo_CXE(IVGA)/(1.D0 - xmo_FR)'
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_CXE(IVGA)/ (1.D0 - xmo_FR), IVGA = 1,mo_NGG)
CDEBUG             WRITE(20,*) 'xmo_FRVOLPB * xmo_CXERBLOC(IVGA)'
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_FRVOLPB * xmo_VG(mo_NGG+IVGA,2), IVGA = 1,mo_NGG)
CDEBUG             WRITE(20,*) 'xmo_FREQ1_CXERBLOC =',xmo_FREQ1_CXERBLOC
CDEBUG             WRITE(20,*) 'xmo_FREQ2_CXERBLOC =',xmo_FREQ2_CXERBLOC
CDEBUG             WRITE(20,*) 'xmo_FREQ3_CXERBLOC =',xmo_FREQ3_CXERBLOC
CDEBUG             WRITE(20,*) 'term1 ='
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_FREQ1_CXERBLOC * xmo_CXE(IVGA) / (1.D0 - xmo_FR),
CDEBUG     &        IVGA = 1,mo_NGG) 
CDEBUG             WRITE(20,*) 'term2 ='
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_FREQ3_CXERBLOC * xmo_CXEB, IVGA = 1,mo_NGG)
CDEBUG             WRITE(20,*) 'term3 ='
CDEBUG             WRITE(20,*)  
CDEBUG     &       (-
CDEBUG     &       (xmo_FREQ1_CXERBLOC + xmo_FREQ2_CXERBLOC + xmo_BBB)
CDEBUG     &       * xmo_VG(mo_NGG+IVGA,2),IVGA = 1,mo_NGG)
CDEBUG           ENDIF
CDEBUG
* 
      RETURN
      END
      SUBROUTINE MOCLOC0
C     =================
C
C
C     ******************************************************************
C     MOCLOC0 : MOgador Calcul des concentrations LOCales au voisinnage
C     des bulles et des pores intra au temps courant                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogloc.inc'
      INCLUDE 'cmogtem.inc'
C
C
***********************************************************************
* cmogvar.inc : xmo_VG, xmo_VD, mo_IET en entr�es 
* cmogpnu.inc : mo_NGG  en entr�e
* cmogloc.inc : xmo_CXERPORLOC0 xmo_TEMPS0 en sortie 
* cmogtem.inc : xmo_TEMPS en entr�e           
***********************************************************************      
***********************************************************************
* 
      DOUBLE PRECISION 
* les m�mes tableaux que dans mofder_gear
*
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXERPORLOC(mo_NNSPHMX)
*
***********************************************************************
      CALL MOMESS('DANS MOCLOC0')
*
*
      DO IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VG(IVGA,2)
      ENDDO
*
      DO IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VD(IVDE,2)
      ENDDO
*
*
**********************************************************************
* CAS de l'�tat classique
**********************************************************************
* Utilisation de variables locales plus parlantes
      IF (mo_IET .EQ. 0) THEN
*     ***********************
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &    xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &    xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
**********************************************************************
* CAS de l'�tat en restructuration
**********************************************************************
      ELSEIF (mo_IET .EQ. 2) THEN
*     ************************
*
          CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
      ENDIF 
*     *****
**********************************************************************
* 
        CALL MOCLOC(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_RHOD,xmo_TEMPS,
     &
     &      xmo_CXERPORLOC,xmo_CXERPORLOCM,
     &      xmo_FREQ1_CXERBLOC,xmo_FREQ2_CXERBLOC,xmo_FREQ3_CXERBLOC)
*
        DO IVGA = 1, mo_NGG
          xmo_CXERPORLOC0(IVGA) = xmo_CXERPORLOC(IVGA) 
        ENDDO
*
        xmo_TEMPS0 = xmo_TEMPS
* 
*
*
      RETURN
      END
      SUBROUTINE MOCONS(mo_IET, mo_DIF_SEQ, mo_NSEQ_C)
C     =================
C
C
C     ******************************************************************
C     MOCONS : MOgador Consideration de la diffusion � grande �chelle
C              pour le gaz dissous au joint                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogppr.inc : mo_DIF_SEQ_LU en entr�e
* cmogpnu.inc : mo_NSEQ  en entr�e 
* cmoginf.inc : mo_DEBUG  en entr�e 
************************************************************************
*
      CALL MOMESS('DANS MOCONS')
*
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
* Dans l'�tat 'en restructuration', on ne consid�re jamais la diffusion 
* au joint des gros grains � grande �chelle 
      IF (mo_IET .EQ. 2) THEN
*     +++++++++++++++++++++++
        mo_NSEQ_C = 1
        mo_DIF_SEQ = 0
      ELSEIF (mo_IET .EQ. 0) THEN
*     +++++++++++++++++++++++++++ 
* Cas ou on se trouve dans l'�tat classique-connect�
        IF (mo_DIF_SEQ_LU .EQ. 1) THEN 
          mo_NSEQ_C = mo_NSEQ
          mo_DIF_SEQ = 1
        ELSEIF  (mo_DIF_SEQ_LU .EQ. 0) THEN 
          mo_NSEQ_C = 1
          mo_DIF_SEQ = 0
        ENDIF
*
      ELSEIF (mo_IET .EQ. 3) THEN
*     +++++++++++++++++++++++++++
* Rien de special a faire
      ELSE
*     ++++
        WRITE(mo_DEBUG,*) 
     &       'mo_IET doit valoir 0 2 ou 3 dans MOCONS '
        STOP
      ENDIF
*     +++++
*
      RETURN
      END
*
      SUBROUTINE MOCOPR (xmo_CGAZ, xmo_VOL,xmo_R,
     $                    mo_BBJT,xmo_PINT,xmo_PEQ)
C     ==================
C
C     ******************************************************************
C     MOCOPR : MOgador COmparaison des PRession interne et d'equilibre :
C              - une bulle intra : mo_BBJT = 1
C              - une bulle inter :   mo_BBJT = 2
C              - un tunnel :   mo_BBJT = 3
C              - un pore intra : mo_BBJT = 4
C              - un pore inter : mo_BBJT = 5
C              - une bulle de rim : mo_BBJT = 6
C              - un pore de rim : mo_BBJT = 7
C              - ensemble pores inter et bulles inter : mo_BBJT = 8
C              - nano bulles de rim : mo_BBJT = 9
C              xmo_CGAZ : concentration en gaz de bulle ou de tunnel (mol/m3) 
C              xmo_VOL : volume des bulle ou des pores /m3 ou des tunnel /m3 
C              xmo_R : rayon des bulle ou des pores  ou des tunnel (m)                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogpas.inc : xmo_GAMMA en entr�e
* cmogent.inc : xmo_PHYDR en entr�e
* cmogppr.inc : xmo_TETBJ exmo_TETPORJ n entr�e
************************************************************************
C
C xmo_VAT : Volume disponible par atome de gaz (de fission ou initial) 
C           dans la bulle ou les tunnels ou les pores
C
C
************************************************************************
************************************************************************
C
      CALL MOMESS('DANS MOCOPR')
*
************************************************************************
* Calcul de la pression interne xmo_PINT
************************************************************************
*
* calcul xmo_VAT selon le type de bulle, tunnel pores consid�r�
*
      CALL MOVLAT (xmo_CGAZ, xmo_VOL,mo_BBJT,xmo_VAT)
*
      IF ( xmo_VAT .NE. 0.D0 ) THEN
          CALL MOPINT(xmo_VAT,xmo_PINT,mo_PB)
      ELSE
          xmo_PINT = 0.D0
      ENDIF
*
      IF ( xmo_R .NE. 0.D0 ) THEN
************************************************************************
* Calcul de la pression d'equilibre xmo_PEQ
************************************************************************
      IF (mo_BBJT .EQ. 1 .OR. mo_BBJT .EQ. 4 
     &                   .OR. mo_BBJT .EQ. 6
     &                   .OR. mo_BBJT .EQ. 7
     &                   .OR. mo_BBJT .EQ. 9) THEN
************************************************************************
* Cas des cavit�s rondes : 
*  bulles intra ou pores intra ou 
*  bulles de rim pores de rim ou bulles nano de rim
*
          xmo_PEQ = xmo_PHYDR + 2.D0 * xmo_GAMMA / xmo_R
*
       ELSEIF (mo_BBJT .EQ. 2) THEN 
************************************************************************
* Cas des bulles inter (toujours lenticulaires) : 
* bulles inter 
          xmo_PEQ = 
     &      xmo_PHYDR + 
     &      2.D0 * xmo_GAMMA * sin(xmo_TETBJ) / xmo_R 
*
       ELSEIF (mo_BBJT .EQ. 5) THEN 
************************************************************************
* Cas des pores inter (pas forcement lenticulaires) : 
* pores inter 
          xmo_PEQ = 
     &      xmo_PHYDR + 
     &      2.D0 * xmo_GAMMA * sin(xmo_TETPORJ) / xmo_R 
*
       ELSE
************************************************************************
* Cas des tunnels
          xmo_PEQ = 
     &      xmo_PHYDR + 
     &      xmo_GAMMA * sin(xmo_TETBJ) / xmo_R 
*
       ENDIF
************************************************************************
       ELSE
         xmo_PEQ = 0.D0
       ENDIF
C
C
C
      RETURN
      END
      SUBROUTINE MOCOPY(rsav,isav,ieqsav,rworksav,iworksav,
     &                  rtolsav,igtabsav,idtabsav,
     &                  rsav_c,isav_c,ieqsav_c,rworksav_c,iworksav_c,
     &                  rtolsav_c,igtabsav_c,idtabsav_c)
C     
C
C
C     ******************************************************************
C     MOCOPY : MOgador COPY des tableaux dans les tableaux _c
C     ******************************************************************

*
*
      double precision rsav(240), rsav_c(240)
      integer isav(50), isav_c(50)
*
      double precision  rworksav(*), rtolsav(*)
      double precision  rworksav_c(*), rtolsav_c(*)
      integer iworksav(*), igtabsav(*), idtabsav(*)
      integer iworksav_c(*), igtabsav_c(*), idtabsav_c(*)
      integer ieqsav(5)
      integer ieqsav_c(5)
*
*
      do i = 1, 240
        rsav_c(i) = rsav(i)
      enddo
*
      do i = 1, 50
        isav_c(i) = isav(i)
      enddo
*
      do i = 1, 5
        ieqsav_c(i) = ieqsav(i)
      enddo
*
      neq = ieqsav(1)
      lrw = ieqsav(2)
      liw = ieqsav(3)
      neqg = ieqsav(4)
      neqd = ieqsav(5)
*
      do i = 1, lrw
        rworksav_c(i) = rworksav(i)
      enddo
*
      do i = 1, liw
        iworksav_c(i) = iworksav(i)
      enddo
*
      do i = 1, neq
        rtolsav_c(i) = rtolsav(i)
      enddo
*
      do i = 1, neqg
        igtabsav_c(i) = igtabsav(i)
      enddo
*
      do i = 1, neqd
        idtabsav_c(i) = idtabsav(i)
      enddo
*
*
      return
      end
      SUBROUTINE MOCORC_GEAR(NEQ,T,Y)
C     ====================================
C
C
C     ******************************************************************
C     MOCORC_GEAR : MOgador Corection des valeurs obtenues pour le vecteur Y
C                   des variables effectives                                   
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogvar.inc'
C
C NEQ est le nombre d'equations effectives � r�soudre
C NEQ prend la valeur de mo_G_NEQ 
C Y a pour longueur utile G_NSEQ (= NEQ)
C xmo_YEXP a pour longueur utile mo_NGG+3+mo_NSEQ_C+3+7 (cas classique)
C en entr�e Y. en sortie Y vaut la valeur corrig�e
C mo_NEQG est le nombre d'�quations effectives � r�soudre pour les variables
C      du vecteur 'gaz'
C mo_NEQD est le nombre d'�quations effectives � r�soudre pour les variables
C      du vecteur 'd�fauts'
C mo_NEQG + mo_NEQD = mo_G_NEQ
************************************************************************
* cmogpnu.inc :  
* cmogear.inc : mo_NEQG mo_NEQD mo_IGTAB mo_IDTAB en entr�e
*               xmo_YEXP en entr�e et sortie (en sortie xmo_YEXP est corrig�e)
* cmogvar.inc : mo_NVG en entr�e
************************************************************************
*
      DOUBLE PRECISION Y(*)
*
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOCORC_GEAR')
*
*
       DO I = 1,mo_NEQG
         xmo_YEXP(mo_IGTAB(I)) = Y(I)
       ENDDO
       DO I = 1,mo_NEQD
         xmo_YEXP(mo_IDTAB(I)+mo_NVG) = Y(I+mo_NEQG)
       ENDDO
*
CDEBUG
CDEBUG        WRITE(20,*) 'mocorc_gear mo_NVG = ',mo_NVG
CDEBUG        WRITE(20,*) 'mocorc_gear xmo_YEXP(1+mo_NVG) = ',
CDEBUG     &               xmo_YEXP(1+mo_NVG)
CDEBUG
************************************************************************
*      verification de la validite physique de la solution et correction
*      eventuelle
*
       CALL MOFPOS_GEAR(xmo_YEXP,IDECOU)
*
* On signale qu'une correction a �t� effectu�e et de quel type de 
* correction il s'agit. Impression de Y avnt et apr�s correction
*
       IF(IDECOU.GT.0) THEN
CDEBUG         WRITE(mo_DEBUG,*) ' SOLUTION NON VALIDE CODE ',IDECOU,' CORRIGEE'
CDEBUG         WRITE(mo_DEBUG,*) ' T = ',T,' Y AVANT = ',(Y(I),I=1,NEQ)
         DO I = 1,mo_NEQG
           Y(I) = xmo_YEXP(mo_IGTAB(I))
         ENDDO
         DO I = 1,mo_NEQD
           Y(I+mo_NEQG) = xmo_YEXP(mo_IDTAB(I)+mo_NVG)
         ENDDO
CDEBUG         WRITE(mo_DEBUG,*) ' T = ',T,' Y APRES = ',(Y(I),I=1,NEQ)
       ENDIF
************************************************************************

      RETURN
      END
      SUBROUTINE MOCSTF
C     =================
C
C
C     ******************************************************************
C     MOCSTF : MOgador Calcul de la Somme du Taux de Fission                                                         
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogent.inc'
C
***********************************************************************
* cmogsor.inc : xmo_SOMTOFIS en entr�e et sortie
* cmogent.inc : xmo_DTS  xmo_TOFISS
************************************************************************
* xmo_SOMTOFIS : Int�grale en temps du taux de fission (fiss/m3)
*                Tableau de travail local
* xmo_DTS : Pas de temps du code cible (s)
* xmo_TOFISS : taux de fission lu dans l'historique (fiss/m3/s)
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOCSTF')
*
*
* Calcul du nombre de fission /m3 depuis le d�but de l'irradiation
*
*
      xmo_SOMTOFIS(1) = xmo_SOMTOFIS(2) + xmo_DTS * xmo_TOFISS
*
      RETURN
      END
      SUBROUTINE MOCTPS(DTDECO)
C     =================
C
C
C     ******************************************************************
C     MOCTPS : MOgador Calcul du TemPS xmo_TEMPS                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'cmogtem.inc'
C
***********************************************************************
* cmogtem.inc : xmo_TEMPS en entr�e et sortie
***********************************************************************      
*
*
      CALL MOMESS('DANS MOCTPS')
*
      xmo_TEMPS = xmo_TEMPS + DTDECO
      CALL MOMESV('le calcul a march� jusqu � ', xmo_TEMPS)
*   
      RETURN
      END
      SUBROUTINE MODDES(xmo_CBUL,xmo_VAT,xmo_VTBUL,xmo_RBUL,
     &                  xmo_DDESTR)
C     =================
C
C
C     ******************************************************************
C     MODDES : MOgador Delta DEStruction : Calcul de la fonction variant entre
C              0 et 1 affectant telle que xmo_BBB = xmo_BB * xmo_DDESTR                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
C
***********************************************************************
* cmogppr.inc : xmo_MN  xmo_EXP_DDESTR mo_IDEST xmo_RBDEST 
*               xmo_PDEST en entr�e 
* cmogphy.inc : xmo_XNAVOG  xmo_OMEGA en entr�e 
***********************************************************************
*
      CALL MOMESS('DANS MODDES')
*
      IF (mo_IDEST .EQ. 1) THEN
        IF (xmo_VTBUL .LT. 1.D-20) THEN
            xmo_DDESTR = 1.D0
        ELSE
            xmo_DDESTR = 
     &      ( xmo_MN * xmo_XNAVOG * xmo_CBUL * xmo_OMEGA / xmo_VTBUL ) 
     &       ** xmo_EXP_DDESTR
        ENDIF
      ENDIF
*
      IF (mo_IDEST .EQ. 2) THEN
        xmo_LARG_RB = xmo_RBDEST(2) - xmo_RBDEST(1)
        xmo_MIL_RB = (xmo_RBDEST(2) + xmo_RBDEST(1)) / 2.D0
        IF ( xmo_RBUL .LT. xmo_RBDEST(1) ) THEN
          xmo_DDESTR = 1.D0
        ELSEIF (xmo_RBUL .GT. xmo_RBDEST(2) ) THEN
          xmo_DDESTR = 0.D0
        ELSE
          xmo_DDESTR = 0.5D0 *
     &          ( 1.D0 + sin( xmo_PIZAHL / xmo_LARG_RB *
     &                        ( xmo_MIL_RB - xmo_RBUL ) )  )
        ENDIF
      ENDIF 
*
      IF (mo_IDEST .EQ. 3) THEN
        IF ( xmo_RBUL .LE. xmo_RBDEST(1) ) THEN
          xmo_DDESTR = 1.D0
        ELSEIF (xmo_RBUL .GE. xmo_RBDEST(3) ) THEN
          xmo_DDESTR = 0.D0
        ELSEIF ( xmo_RBUL .LT. xmo_RBDEST(2) ) THEN
          xmo_DDESTR = xmo_A1 * (xmo_RBUL - xmo_RBDEST(1))**3
     &               + xmo_B1 * (xmo_RBUL - xmo_RBDEST(1))**2
     &               + 1.D0
        ELSE
          xmo_DDESTR = xmo_A2 * (xmo_RBUL - xmo_RBDEST(3))**3
     &               + xmo_B2 * (xmo_RBUL - xmo_RBDEST(3))**2
        ENDIF
      ENDIF 
*
      IF (mo_IDEST .EQ. 4) THEN
        IF ( xmo_RBUL .LE. xmo_RBDEST(1) ) THEN
          xmo_DDESTR = 1.D0
        ELSEIF (xmo_RBUL .GE. xmo_RBDEST(3) ) THEN
          xmo_DDESTR = 0.D0
        ELSEIF ( xmo_RBUL .LT. xmo_RBDEST(2) ) THEN
          xmo_DDESTR = xmo_A1 * (xmo_RBUL - xmo_RBDEST(1))**5
     &               + xmo_B1 * (xmo_RBUL - xmo_RBDEST(1))**4
     &               + xmo_C1 * (xmo_RBUL - xmo_RBDEST(1))**3
     &               + 1.D0
        ELSE
          xmo_DDESTR = xmo_A2 * (xmo_RBUL - xmo_RBDEST(3))**5
     &               + xmo_B2 * (xmo_RBUL - xmo_RBDEST(3))**4
     &               + xmo_C2 * (xmo_RBUL - xmo_RBDEST(3))**3
        ENDIF
      ENDIF    
      RETURN
      END
      SUBROUTINE MODLBJ(xmo_RB, xmo_RBJ, xmo_DLBJ)
C     =================
C
C
C     ******************************************************************
C     MODNUC : MOgador Delta BJ : Calcul de la fonction variant entre
C              0 et 1 affectant le terme source de bulles inter
C              en provenance des grains                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogppr.inc : xmo_TDBJ  en entr�e 
* cmogpte.inc : xmo_TEST5  en entr�e
* cmoginf.inc : mo_DEBUG  en entr�e 
************************************************************************
*
      CALL MOMESS('DANS MODLBJ')
*
      xmo_DENOM = (xmo_TDBJ(3) - xmo_TDBJ(1)) * xmo_RB +
     &            ( xmo_TDBJ(4) - xmo_TDBJ(2) )
*
      IF ( xmo_DENOM .LT. 1.D-20 ) THEN
        WRITE(mo_DEBUG,*) 'd�nominateur nul ou n�gatif dans MODLBJ'
        STOP 
      ENDIF
*
      xmo_DLBJ = (1.D0 -
     &           (min(1.D0 , 
     &               max(0.D0 , 
     &                   xmo_RBJ 
     &                   - xmo_TDBJ(1) * xmo_RB - xmo_TDBJ(2)) 
     &               / xmo_DENOM 
     &               ))**2.D0
     &            )**2.D0  
*
* Pour test : possibilit� de ne r�cup�rer aucune bulle d�bouchante
* en inter xmo_TEST5 = 1 en fonctionnement normal
*
      xmo_DLBJ = xmo_TEST5 * xmo_DLBJ 
*
*   
      RETURN
      END
      SUBROUTINE MODNUC(xmo_CB, xmo_CXEB, xmo_RB, 
     &                  xmo_DNUCL)
C     =================
C
C
C     ******************************************************************
C     MODNUC : MOgador Delta NUCl�ation : Calcul de la fonction variant entre
C              0 et 1 affectant le terme de nucl�ation                            
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogppr.inc : xmo_MN  mo_INUCL xmo_RBNU xmo_EXP_DNUCL en entr�e
* cmoginf.inc : mo_DEBUG  en entr�e 
************************************************************************
*
      CALL MOMESS('DANS MODNUC')
*
      IF (mo_INUCL .EQ. 1) THEN
        IF (xmo_CXEB .LT. 1.D-20) THEN
          xmo_DNUCL = 1.D0
        ELSE
          xmo_DNUCL= ( xmo_MN * xmo_CB / xmo_CXEB ) ** xmo_EXP_DNUCL
        ENDIF
      ENDIF  
*
      IF (mo_INUCL .EQ. 2) THEN
        IF (abs(xmo_RBNU(2)-xmo_RBNU(1)) .LT. 1.D-20) THEN
          WRITE(mo_DEBUG,*)  'mauvaises donnees pour xmo_RBNU'
          STOP
        ELSE
          IF (xmo_RB .LT. xmo_RBNU(1) ) THEN
            xmo_DNUCL =  1.D0
          ELSEIF (xmo_RB .LT. xmo_RBNU(2) ) THEN
            xmo_DNUCL =  
     &      (1.D0 - 
     &       ((xmo_RB-xmo_RBNU(1))/(xmo_RBNU(2)-xmo_RBNU(1)))**2.D0
     &      )**2.D0 
          ELSE
            xmo_DNUCL = 0.D0
          ENDIF 
        ENDIF
      ENDIF 
*
      IF (mo_INUCL .EQ. 3) THEN
        IF (xmo_CB .LT. 1.D-20) THEN
          xmo_DNUCL = 1.D0
        ELSE
          xmo_DNUCL= exp(- xmo_EXP_DNUCL * xmo_CXEB / xmo_CB / xmo_MN ) 
        ENDIF
      ENDIF 
*
      IF (mo_INUCL .EQ. 4) THEN
        IF (abs(xmo_RBNU(2)-xmo_RBNU(1)) .LT. 1.D-20) THEN
          WRITE(mo_DEBUG,*)  'mauvaises donnees pour xmo_RBNU'
          STOP
        ELSE
          IF (xmo_RB .LT. xmo_RBNU(1) ) THEN
            xmo_MULT_DNUCL =  1.D0
          ELSEIF (xmo_RB .LT. xmo_RBNU(2) ) THEN
            xmo_MULT_DNUCL =  
     &      (1.D0 - 
     &       ((xmo_RB-xmo_RBNU(1))/(xmo_RBNU(2)-xmo_RBNU(1)))**2.D0
     &      )**2.D0 
          ELSE
            xmo_MULT_DNUCL = 0.D0
          ENDIF 
        ENDIF
        IF (xmo_CXEB .LT. 1.D-20) THEN
          xmo_DNUCL = 1.D0
        ELSE
          xmo_DNUCL= 
     &     ( ( xmo_MN * xmo_CB / xmo_CXEB ) ** xmo_EXP_DNUCL ) 
     &     * xmo_MULT_DNUCL
        ENDIF
      ENDIF 
*
      IF (mo_INUCL .EQ. 5) THEN
        IF (  (abs(xmo_RBNU(2)-xmo_RBNU(1)) .LT. 1.D-20)
     &     .OR. (xmo_MULTRB2 .LT. 1.D-40)  )  THEN
          WRITE(mo_DEBUG,*)  'mauvaises donnees pour xmo_DNUCL'
          STOP
        ELSE
          xmo_RAC_MULTRB2 = SQRT(xmo_MULTRB2)
          IF (xmo_RB .LT. xmo_RBNU(1) ) THEN
            xmo_MULT_DNUCL =  1.D0
          ELSEIF (xmo_RB .LT. xmo_RBNU(2) ) THEN
            xmo_MULT_DNUCL =  
     &      (1-xmo_RAC_MULTRB2)**2 * 
     &      ( 1.D0/(1-xmo_RAC_MULTRB2) - 
     &       ((xmo_RB-xmo_RBNU(1))/(xmo_RBNU(2)-xmo_RBNU(1)))**2.D0
     &      )**2.D0 
          ELSE
            xmo_MULT_DNUCL = xmo_MULTRB2 * 
     &          exp(  -4.D0 * (1-xmo_RAC_MULTRB2) / xmo_RAC_MULTRB2 
     &                 * (xmo_RB-xmo_RBNU(2))  )
          ENDIF 
        ENDIF
*
        IF (xmo_CXEB .LT. 1.D-20) THEN
          xmo_DNUCL = 1.D0
        ELSE
          xmo_DNUCL= 
     &     ( ( xmo_MN * xmo_CB / xmo_CXEB ) ** xmo_EXP_DNUCL ) 
     &     * xmo_MULT_DNUCL
        ENDIF
      ENDIF 
*
*   
      RETURN
      END
      SUBROUTINE MODXEA(xmo_RHOD,xmo_DXEAM)
C     =================
C
C
C     ******************************************************************
C     MODXEA : MOgador calcul du coefficeint de Diffusion du XEnon Am�lior�                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpas.inc'
C
***********************************************************************
* cmogppr.inc : xmo_LDIS  mo_DIFXE_AMELIOREE xmo_RHODI en entr�e
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogpas.inc : xmo_DXE xmo_DXEJ en entr�e
************************************************************************
*
      CALL MOMESS('DANS MODXEA')
*
* Si on veut consid�rer une am�lioration du coef de diffusion
* avec les dislocations , mo_DIFXE_AMELIOREE doi valoir 1 
* si mo_DIFXE_AMELIOREE vaut 0, on aura syst�matiquement xmo_DXEAM = xmo_DXE
* (plus d'influence des dislocations)
*
      xmo_DXEAM =
     &        min(1.D0, xmo_PIZAHL * xmo_LDIS**2.D0 
     &                  * (xmo_RHOD - xmo_RHODI) 
     &                  * mo_DIFXE_AMELIOREE ) * xmo_DXEJ
     &        +
     &        max( 0.D0, 1.D0 - 
     &                   xmo_PIZAHL * xmo_LDIS**2.D0 * 
     &                   (xmo_RHOD - xmo_RHODI) 
     &                   * mo_DIFXE_AMELIOREE ) * xmo_DXE 
*   
      RETURN
      END
      SUBROUTINE MOFCLIC(xmo_RBUL,xmo_TERMEB_NUCLEATION,
     &                   xmo_TERMEB_COALGEOM,
     &                   xmo_TERMEXE_NUCLEATION,
     &                   xmo_TERMEXEB_NUCLEATION)
C     ==================
C
C
C     ******************************************************************
C     MOFCLIC : MOgador Fonction CLICquet : Fonction servant � emp�cher 
C               l'augmentation du nombre des bulles si leur rayon d�passe
C               la valeur xmo_RCLIC                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* xmo_RBUL : Rayon des bulles intra ou inter (entr�e)
* xmo_TERMEB_NUCLEATION : termes de nucl�ation (tendant � faire augmenter
*                        le nombre des bulles (entr�e et sortie)
* xmo_TERMEB_COALGEOM : terme de coalescence g�om�trique (entr�e et sortie)
* xmo_RCLIC : Rayon des bulles au-dessus duquel la nucl�ation de 
*             nouvelle bulle est emp�ch� (entr�e)
***********************************************************************
* cmogppr.inc : mo_CLIQUET xmo_RCLIC en entr�e
***********************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_TERMEXE_NUCLEATION(mo_NNSPHMX)
*
*
      CALL MOMESS('DANS MOFCLIC')
*
      IF (mo_CLIQUET .EQ. 1) THEN
*     ========================
*
*
      IF ( (xmo_RBUL .GT. xmo_RCLIC(1)) .AND.
     &     ( ( xmo_TERMEB_NUCLEATION + xmo_TERMEB_COALGEOM ) 
     &       .GT. 0.D0 )  ) THEN
          xmo_TERMEB_NUCLEATION = 0.D0
          xmo_TERMEB_COALGEOM = 0.D0
          xmo_TERMEXEB_NUCLEATION = 0.D0
          DO IVGA = 1, mo_NNSPHMX
            xmo_TERMEXE_NUCLEATION(IVGA) = 0.D0
          ENDDO
      ENDIF
*
*
*
***********************************************************************
      ELSEIF (mo_CLIQUET .EQ. 2) THEN
*     ================================
*
*
        IF (xmo_RBUL .LT. xmo_RCLIC(1) ) THEN
          xmo_MULT =  1.D0
        ELSEIF (xmo_RBUL .LT. xmo_RCLIC(2) ) THEN
          xmo_MULT =  
     &    (1.D0 - 
     &      ( (xmo_RBUL-xmo_RCLIC(1)) /
     &        (xmo_RCLIC(2)-xmo_RCLIC(1)) )**2.D0
     &    )**2.D0 
        ELSE
            xmo_MULT = 0.D0
        ENDIF 
*
        IF  (xmo_RBUL .GT. xmo_RCLIC(1)) THEN
          xmo_TERMEXEB_NUCLEATION = 
     &                 xmo_TERMEXEB_NUCLEATION * xmo_MULT
          DO IVGA = 1, mo_NNSPHMX
            xmo_TERMEXE_NUCLEATION(IVGA) = 
     &                 xmo_TERMEXE_NUCLEATION(IVGA) * xmo_MULT
          ENDDO
*
          IF  ( ( xmo_TERMEB_NUCLEATION + xmo_TERMEB_COALGEOM ) 
     &          .GT. 0.D0 ) THEN         
          xmo_TERMEB_NUCLEATION = 0.D0
          xmo_TERMEB_COALGEOM = 0.D0
          ENDIF
        ENDIF
*
      ENDIF
*     =====
*    
      RETURN
      END
      SUBROUTINE MOFPOS_GEAR(Y_COMPLET,IDECOU)
C     ===========================
C
C
C     ******************************************************************
C     MOFPOS : MOgador Fonction d�riv�e : test sur la POSsibilit� de 
C              la calculer                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogvar.inc'
C
***********************************************************************
* cmogpnu.inc : mo_NGG, mo_NSEQ, mo_NSEQR en entr�e
* cmogvar.inc : mo_IET mo_NVG mo_NVD mo_DIF_SEQ en entr�e
************************************************************************
* A l'appel Y_COMPLET vaut xmo_YEXP qui est le vecteur de toutes les variables possibles
* y compris celles qui ne bougent pas en temps
************************************************************************ 
      DOUBLE PRECISION Y_COMPLET(*)
C
      DOUBLE PRECISION  xmo_VG1(mo_NVGAMX),
     &                  xmo_VD1(mo_NVDEMX)
*
      CALL MOMESS('DANS MOFPOS_GEAR')
*
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
      DO I = 1, mo_NVG
        xmo_VG1(I) = Y_COMPLET(I)
      ENDDO
*
      DO I = 1,mo_NVD
        xmo_VD1(I) = Y_COMPLET(mo_NVG+I)
      ENDDO
*
      IDECOU = 0
************************************************************************
      IF (mo_IET .EQ. 0) THEN
*     cas classique-connecte (0) 
************************************************************************
*       concentrations en gaz intra dissous
        DO IVGA = 1, mo_NGG
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 1
            CALL MOMESS('CXE <0')
          ENDIF
        ENDDO
*
*       concentrations en gaz dissous au voisinnage des bulles
        DO IVGA = 1, mo_NGG
          IF (xmo_VG1(IVGA+mo_NGG) .LT. 0.D0) THEN
            xmo_VG1(IVGA+mo_NGG) = 0.D0
            IDECOU = 1
            CALL MOMESS('CXERBLOC <0')
          ENDIF
        ENDDO
*
*       nombre de bulles intra /m3
        IF (xmo_VG1(2*mo_NGG+1) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+1) = 0.D0
          IDECOU = 2
          CALL MOMESS('CB < 0')
        ENDIF
*
*       concentrations en gaz de bulles intra 
        IF (xmo_VG1(2*mo_NGG+2) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+2) = 0.D0
          IDECOU = 3
          CALL MOMESS('CXEB < 0')
        ENDIF
*
*       volume total de bulles intra /m3 de combustible frais
        IF (xmo_VG1(2*mo_NGG+3) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3) = 0.D0
          IDECOU = 4
          CALL MOMESS('VTB < 0')
        ENDIF
*
* Si on suppose que les bulles occupent les sommets d'un r�seau cubique
* la condition de non interp�n�tration de deux bulles est
        IF (xmo_VG1(2*mo_NGG+3) .GT. 1.099D0) THEN
          xmo_VG1(2*mo_NGG+3) = 1.099D0
          IDECOU = 63
          CALL MOMESS('VTB > 1.099D0')
        ENDIF
*
* Cas d'existence de bulles intra avec un volume
* de bulles intra nul : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(2*mo_NGG+3) .EQ. 0.D0 ) THEN
          xmo_VG1(2*mo_NGG+1) =0.D0
          IDECOU = 5
          CALL MOMESS('CB > 0 et VTB = 0')
        ENDIF
*
* Cas de non existence de bulles intra avec un volume
* de bulles intra non nul ou une quantit� de gaz de 
* bulles intra non nulle : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(2*mo_NGG+3) .NE. 0.D0 .OR.
     &        xmo_VG1(2*mo_NGG+2) .NE. 0.D0) ) THEN
          xmo_VG1(2*mo_NGG+2) = 0.D0
          xmo_VG1(2*mo_NGG+3) = 0.D0
          IDECOU = 6
          CALL MOMESS('CB = 0 et ( VTB > 0 ou CXEB > 0 ) ')
        ENDIF
*
*       concentrations en gaz inter dissous
        DO 20 IVGA = 2*mo_NGG+4, 2*mo_NGG+3+mo_NSEQ_C
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 7
            CALL MOMESS('CXEJ < 0')
          ENDIF
 20     CONTINUE
*
*       nombre de bulles inter /m3 
*       (la longueur de tunnel /m3 ne peut pas s'annuler)
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) = 0.D0
          IDECOU = 8
          CALL MOMESS('CBJ < 0 ou LTUNJ < 0')
        ENDIF
*
*       concentrations en gaz de bulles inter ou de tunnel 
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) = 0.D0
          IDECOU = 9
          CALL MOMESS('CXEBJ < 0')
        ENDIF
*
*       volume total de bulles inter ou de tunnel/m3 de combustible frais 
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) = 0.D0
          IDECOU = 10
          CALL MOMESS('VTBJ < 0 ou VTTUNJ < 0')
        ENDIF
*
*
* Cas d'existence de bulles inter avec un volume
* de bulles inter nul : Impossible dans le cas classique
*
        IF ( xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .EQ. 0.D0 ) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) = 0.D0
          IDECOU = 11
          CALL MOMESS('CBJ > 0 et VTBJ = 0')
        ENDIF
*
* Cas de non existence de bulles inter avec un volume
* de bulles inter non nul ou une quantit� de gaz de 
* bulles inter non nulle : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .NE. 0.D0 .OR.
     &        xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) .NE. 0.D0) ) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) = 0.D0
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) = 0.D0
          IDECOU = 12
          CALL MOMESS('CBJ = 0 et ( VTBJ > 0 ou CXEBJ > 0 )')
        ENDIF

* les pores
***********
*       nombre de pores intra /m3 
        IF (xmo_VD1(1) .LT. 0.D0) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 13
          CALL MOMESS('CPOR <  0')
        ENDIF
*       concentrations en gaz de pores intra 
        IF (xmo_VD1(2) .LT. 0.D0) THEN
          xmo_VD1(2) = 0.D0
          IDECOU = 14
          CALL MOMESS('CXEPOR <  0')
        ENDIF
*       volume total de pores intra/m3 de combustible frais
        IF (xmo_VD1(3) .LT. 0.D0) THEN
          xmo_VD1(3) = 0.D0
          IDECOU = 15
          CALL MOMESS('VTPOR <  0')
        ENDIF
*
* Cas d'existence de pores intra avec un volume
* de pores intra nul : Impossible
*
        IF ( xmo_VD1(1) .NE. 0.D0 .AND. 
     &       xmo_VD1(3) .EQ. 0.D0 ) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 16
          CALL MOMESS('CPOR > 0 et VTPOR = 0')
        ENDIF
*
* Cas de non existence de pores intra avec un volume
* de pores intra non nul ou une quantit� de gaz de 
* pores intra non nulle : Impossible
*
        IF ( xmo_VD1(1) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(3) .NE. 0.D0 .OR.
     &        xmo_VD1(2) .NE. 0.D0) ) THEN
          xmo_VD1(2) = 0.D0
          xmo_VD1(3) = 0.D0
          IDECOU = 17
          CALL MOMESS('CPOR = 0 et '//
     &                '( VTPOR > 0 ou CXEPOR > 0 ) ')
        ENDIF
*
*       nombre de pores inter /m3 
        IF (xmo_VD1(4) . LT. 0.D0) THEN
          xmo_VD1(4) = 0.D0
          IDECOU = 18
          CALL MOMESS('CPORJ <  0')
        ENDIF
*       concentrations en gaz de pores inter 
        IF (xmo_VD1(5) .LT. 0.D0) THEN
           xmo_VD1(5) = 0.D0
          IDECOU = 19
          CALL MOMESS('CXEPORJ <  0')
        ENDIF
*       volume total de pores inter /m3 de combustible frais
        IF (xmo_VD1(6) .LT. 0.D0) THEN
           xmo_VD1(6) = 0.D0
          IDECOU = 20
          CALL MOMESS('VTPORJ <  0')
        ENDIF
*
* Cas d'existence de pores inter avec un volume
* de pores inter nul : Impossible
*
        IF ( xmo_VD1(4) .NE. 0.D0 .AND. 
     &       xmo_VD1(6) .EQ. 0.D0 ) THEN
           xmo_VD1(4) = 0.D0
          IDECOU = 21
          CALL MOMESS('CPORJ > 0 et VTPORJ = 0')
        ENDIF
*
* Cas de non existence de pores inter avec un volume
* de pores inter non nul ou une quantit� de gaz de 
* pores inter non nulle : Impossible
*
        IF ( xmo_VD1(4) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(6) .NE. 0.D0 .OR.
     &        xmo_VD1(5) .NE. 0.D0) ) THEN
          xmo_VD1(5) = 0.D0
          xmo_VD1(6) = 0.D0
          IDECOU = 22
          CALL MOMESS('CPORJ = 0 et '//
     &                '( VTPORJ > 0 ou CXEPORJ > 0 ) ')
        ENDIF
*
* Cas de concentration de dislocations negatives
        IF ( xmo_VD1(7) .LT. 0.D0  ) THEN
          xmo_VD1(7) = 0.D0
          IDECOU = 22
          CALL MOMESS('RHOD < 0  ')
        ENDIF
************************************************************************
      ELSEIF (mo_IET .EQ. 3) THEN
* cas du combustible restructur�
************************************************************************
*       concentrations en gaz dissous dans le mileu homog�ne
        DO IVGA = 1, mo_NSEQR
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 23
            CALL MOMESS('CXEHOM <  0')
          ENDIF
        ENDDO
*
*
*       nombre de bulles de rim /m3
        IF (xmo_VG1(mo_NSEQR+1) .LT. 0.D0) THEN
          xmo_VG1(mo_NSEQR+1) = 0.D0
          IDECOU = 27
          CALL MOMESS('CBRIM <  0')
        ENDIF
*
*       concentrations en gaz de bulles de rim
        IF (xmo_VG1(mo_NSEQR+2) .LT. 0.D0) THEN
          xmo_VG1(mo_NSEQR+2) = 0.D0
          IDECOU = 28
          CALL MOMESS('CXEBRIM <  0')
        ENDIF
*
*       volume total de bulles de rim /m3 de combustible frais
        IF (xmo_VG1(mo_NSEQR+3) .LT. 0.D0) THEN
          xmo_VG1(mo_NSEQR+3) = 0.D0
          IDECOU = 29
          CALL MOMESS('VTBRIM <  0')
        ENDIF
*
*
* Cas d'existence de bulles de rim avec un volume
* de bulles de rim nul : Impossible
*
        IF ( xmo_VG1(mo_NSEQR+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(mo_NSEQR+3) .EQ. 0.D0 ) THEN
          xmo_VG1(mo_NSEQR+1) = 0.D0
          IDECOU = 30
          CALL MOMESS('CBRIM > 0 et VTBRIM = 0')
        ENDIF
*
* Cas de non existence de bulles de rim avec un volume
* de bulles de rim non nul ou une quantit� de gaz de 
* bulles de rim non nulle : Impossible
*
        IF ( xmo_VG1(mo_NSEQR+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(mo_NSEQR+3) .NE. 0.D0 .OR.
     &        xmo_VG1(mo_NSEQR+2) .NE. 0.D0) ) THEN
          xmo_VG1(mo_NSEQR+2) = 0.D0
          xmo_VG1(mo_NSEQR+3) = 0.D0
          IDECOU = 31
          CALL MOMESS('CBRIM = 0 et '//
     &                '( VTBRIM > 0 ou CXEBRIM > 0 ) ')
        ENDIF

* les pores
***********
*       nombre de pores  /m3 
        IF (xmo_VD1(1) .LT. 0.D0) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 32
          CALL MOMESS('CPORRIM < 0')
        ENDIF
*
*       concentrations en gaz de pores 
        IF (xmo_VD1(2) .LT. 0.D0) THEN
          xmo_VD1(2) = 0.D0
          IDECOU = 33
          CALL MOMESS('CXEPORRIM < 0')
        ENDIF
*
*       volume total de pores / m3 de combustible frais 
        IF (xmo_VD1(3) .LT. 0.D0) THEN
          xmo_VD1(3) = 0.D0
          IDECOU = 34
          CALL MOMESS('VTPORRIM < 0')
        ENDIF
*
*
* Cas d'existence de pores rim avec un volume
* de pores rim nul : Impossible
*
        IF ( xmo_VD1(1) .NE. 0.D0 .AND. 
     &       xmo_VD1(3) .EQ. 0.D0 ) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 35
          CALL MOMESS('CPORRIM > 0 et VTPORRIM = 0')
        ENDIF
*
* Cas de non existence de pores rim avec un volume
* de pores rim non nul ou une quantit� de gaz de 
* pores rim non nulle : Impossible
*
        IF ( xmo_VD1(1) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(3) .NE. 0.D0 .OR.
     &        xmo_VD1(2) .NE. 0.D0) ) THEN
          xmo_VD1(2) = 0.D0
          xmo_VD1(3) = 0.D0
          IDECOU = 36
          CALL MOMESS('CPORRIM = 0 et '//
     &                '( VTPORRIM > 0 ou CXEPORRIM > 0 ) ')
        ENDIF
*
************************************************************************
      ELSEIF (mo_IET .EQ. 2) THEN
* cas du combustible 'en restructuration'
************************************************************************
*       concentrations en gaz dissous zones saines
        DO IVGA = 1, mo_NGG
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 37
            CALL MOMESS('CXE <0')
          ENDIF
        ENDDO
*
*       concentrations en gaz dissous au voisinnage des bulles
        DO IVGA = 1, mo_NGG
          IF (xmo_VG1(IVGA+mo_NGG) .LT. 0.D0) THEN
            xmo_VG1(IVGA+mo_NGG) = 0.D0
            IDECOU = 1
            CALL MOMESS('CXERBLOC <0')
          ENDIF
        ENDDO
*
*       nombre de bulles  zones saines /m3
        IF (xmo_VG1(2*mo_NGG+1) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+1) = 0.D0
          IDECOU = 38
          CALL MOMESS('CB < 0')
        ENDIF
*
*       concentrations en gaz de bulles  zones saines
        IF (xmo_VG1(2*mo_NGG+2) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+2) = 0.D0
          IDECOU = 39
          CALL MOMESS('CXEB < 0')
        ENDIF
*
*       volume total de bulles zones saines /m3 de combustible frais
        IF (xmo_VG1(2*mo_NGG+3) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3) = 0.D0
          IDECOU = 40
          CALL MOMESS('VTB < 0')
        ENDIF
*
* Si on suppose que les bulles occupent les sommets d'un r�seau cubique
* la condition de non interp�n�tration de deux bulles est
        IF (xmo_VG1(2*mo_NGG+3) .GT. 1.099D0) THEN
          xmo_VG1(2*mo_NGG+3) = 1.099D0
          IDECOU = 64
          CALL MOMESS('VTB > 1.099D0')
        ENDIF
*
* Cas d'existence de bulles zones saines avec un volume
* de bulles zones saines nul : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(2*mo_NGG+3) .EQ. 0.D0 ) THEN
          xmo_VG1(2*mo_NGG+1) =0.D0
          IDECOU = 41
          CALL MOMESS('CB > 0 et VTB = 0')
        ENDIF
*
* Cas de non existence de bulles zones saines avec un volume
* de bulles zones saines non nul ou une quantit� de gaz de 
* bulles zones saines non nulle : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(2*mo_NGG+3) .NE. 0.D0 .OR.
     &        xmo_VG1(2*mo_NGG+2) .NE. 0.D0) ) THEN
          xmo_VG1(2*mo_NGG+2) = 0.D0
          xmo_VG1(2*mo_NGG+3) = 0.D0
          IDECOU = 42
          CALL MOMESS('CB = 0 et ( VTB > 0 ou CXEB > 0 ) ')
        ENDIF
*
*       concentrations en gaz inter dissous
        DO  IVGA = 2*mo_NGG+4, 2*mo_NGG+3+mo_NSEQ_C
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 421
            CALL MOMESS('CXEJ < 0')
          ENDIF
        ENDDO
*
*       nombre de bulles inter /m3 
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) = 0.D0
          IDECOU = 422
          CALL MOMESS('CBJ < 0 ')
        ENDIF
*
*       concentrations en gaz de bulles inter 
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) = 0.D0
          IDECOU = 423
          CALL MOMESS('CXEBJ < 0')
        ENDIF
*
*       volume total de bulles inter /m3 de combustible frais 
        IF (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .LT. 0.D0) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) = 0.D0
          IDECOU = 424
          CALL MOMESS('VTBJ < 0 ')
        ENDIF
*
*
* Cas d'existence de bulles inter avec un volume
* de bulles inter nul : Impossible 
*
        IF ( xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .EQ. 0.D0 ) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) = 0.D0
          IDECOU = 425
          CALL MOMESS('CBJ > 0 et VTBJ = 0')
        ENDIF
*
* Cas de non existence de bulles inter avec un volume
* de bulles inter non nul ou une quantit� de gaz de 
* bulles inter non nulle : Impossible
*
        IF ( xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) .NE. 0.D0 .OR.
     &        xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) .NE. 0.D0) ) THEN
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2) = 0.D0
          xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3) = 0.D0
          IDECOU = 426
          CALL MOMESS('CBJ = 0 et ( VTBJ > 0 ou CXEBJ > 0 )')
        ENDIF
*
*
       mo_NZONE_SAINE = 2*mo_NGG+3+mo_NSEQ_C+3        
*
*       concentrations en gaz dissous dans le mileu homog�ne
        DO IVGA = mo_NZONE_SAINE+1, mo_NZONE_SAINE+mo_NSEQR
          IF (xmo_VG1(IVGA) .LT. 0.D0) THEN
            xmo_VG1(IVGA) = 0.D0
            IDECOU = 43
            CALL MOMESS('CXEHOM <  0')
          ENDIF
        ENDDO
*
*
*       nombre de bulles de rim /m3
        IF (xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1) .LT. 0.D0) THEN
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1) = 0.D0
          IDECOU = 47
          CALL MOMESS('CBRIM <  0')
        ENDIF
*
*       concentrations en gaz de bulles de rim
        IF (xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+2) .LT. 0.D0) THEN
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+2) = 0.D0
          IDECOU = 48
          CALL MOMESS('CXEBRIM <  0')
        ENDIF
*
*       volume total de bulles de rim /m3 de combustible frais
        IF (xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3) .LT. 0.D0) THEN
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3) = 0.D0
          IDECOU = 49
          CALL MOMESS('VTBRIM <  0')
        ENDIF
*
*
* Cas d'existence de bulles de rim avec un volume
* de bulles de rim nul : Impossible
*
        IF ( xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1) .NE. 0.D0 .AND. 
     &       xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3) .EQ. 0.D0 ) THEN
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1) = 0.D0
          IDECOU = 50
          CALL MOMESS('CBRIM > 0 et VTBRIM = 0')
        ENDIF
*
* Cas de non existence de bulles de rim avec un volume
* de bulles de rim non nul ou une quantit� de gaz de 
* bulles de rim non nulle : Impossible
*
        IF ( xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1) .EQ. 0.D0 .AND. 
     &       (xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3) .NE. 0.D0 .OR.
     &        xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+2) .NE. 0.D0) ) THEN
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+2) = 0.D0
          xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3) = 0.D0
          IDECOU = 51
          CALL MOMESS('CBRIM = 0 et '//
     &                '( VTBRIM > 0 ou CXEBRIM > 0 ) ')
        ENDIF

* les pores
***********
*       nombre de pores zones saines /m3 
        IF (xmo_VD1(1) .LT. 0.D0) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 52
          CALL MOMESS('CPOR <  0')
        ENDIF
*       concentrations en gaz de pores zones saines 
        IF (xmo_VD1(2) .LT. 0.D0) THEN
          xmo_VD1(2) = 0.D0
          IDECOU = 53
          CALL MOMESS('CXEPOR <  0')
        ENDIF
*       volume total de pores zones saines/m3 de combustible frais
        IF (xmo_VD1(3) .LT. 0.D0) THEN
          xmo_VD1(3) = 0.D0
          IDECOU = 54
          CALL MOMESS('VTPOR <  0')
        ENDIF
*
* Cas d'existence de pores zones saines avec un volume
* de pores zones saines nul : Impossible
*
        IF ( xmo_VD1(1) .NE. 0.D0 .AND. 
     &       xmo_VD1(3) .EQ. 0.D0 ) THEN
          xmo_VD1(1) = 0.D0
          IDECOU = 55
          CALL MOMESS('CPOR > 0 et VTPOR = 0')
        ENDIF
*
* Cas de non existence de pores zones saines avec un volume
* de pores zones saines non nul ou une quantit� de gaz de 
* pores zones saines non nulle : Impossible
*
        IF ( xmo_VD1(1) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(3) .NE. 0.D0 .OR.
     &        xmo_VD1(2) .NE. 0.D0) ) THEN
          xmo_VD1(2) = 0.D0
          xmo_VD1(3) = 0.D0
          IDECOU = 56
          CALL MOMESS('CPOR = 0 et '//
     &                '( VTPOR > 0 ou CXEPOR > 0 ) ')
        ENDIF
*
*       nombre de pores inter /m3 
        IF (xmo_VD1(4) . LT. 0.D0) THEN
          xmo_VD1(4) = 0.D0
          IDECOU = 561
          CALL MOMESS('CPORJ <  0')
        ENDIF
*       concentrations en gaz de pores inter 
        IF (xmo_VD1(5) .LT. 0.D0) THEN
           xmo_VD1(5) = 0.D0
          IDECOU = 562
          CALL MOMESS('CXEPORJ <  0')
        ENDIF
*       volume total de pores inter /m3 de combustible frais
        IF (xmo_VD1(6) .LT. 0.D0) THEN
           xmo_VD1(6) = 0.D0
          IDECOU = 563
          CALL MOMESS('VTPORJ <  0')
        ENDIF
*
* Cas d'existence de pores inter avec un volume
* de pores inter nul : Impossible
*
        IF ( xmo_VD1(4) .NE. 0.D0 .AND. 
     &       xmo_VD1(6) .EQ. 0.D0 ) THEN
           xmo_VD1(4) = 0.D0
          IDECOU = 564
          CALL MOMESS('CPORJ > 0 et VTPORJ = 0')
        ENDIF
*
* Cas de non existence de pores inter avec un volume
* de pores inter non nul ou une quantit� de gaz de 
* pores inter non nulle : Impossible
*
        IF ( xmo_VD1(4) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(6) .NE. 0.D0 .OR.
     &        xmo_VD1(5) .NE. 0.D0) ) THEN
          xmo_VD1(5) = 0.D0
          xmo_VD1(6) = 0.D0
          IDECOU = 565
          CALL MOMESS('CPORJ = 0 et '//
     &                '( VTPORJ > 0 ou CXEPORJ > 0 ) ')
        ENDIF
*
*       nombre de pores de zones restructur�es /m3 
        IF (xmo_VD1(7) .LT. 0.D0) THEN
          xmo_VD1(7) = 0.D0
          IDECOU = 57
          CALL MOMESS('CPORRIM < 0')
        ENDIF
*
*       concentrations en gaz de pores  de zones restructur�es
        IF (xmo_VD1(8) .LT. 0.D0) THEN
          xmo_VD1(8) = 0.D0
          IDECOU = 58
          CALL MOMESS('CXEPORRIM < 0')
        ENDIF
*
*       volume total de pores  de zones restructur�es/ m3 de combustible frais 
        IF (xmo_VD1(9) .LT. 0.D0) THEN
          xmo_VD1(9) = 0.D0
          IDECOU = 59
          CALL MOMESS('VTPORRIM < 0')
        ENDIF
*
*
* Cas d'existence de pores rim avec un volume
* de pores rim nul : Impossible
*
        IF ( xmo_VD1(7) .NE. 0.D0 .AND. 
     &       xmo_VD1(9) .EQ. 0.D0 ) THEN
          xmo_VD1(7) = 0.D0
          IDECOU = 60
          CALL MOMESS('CPORRIM > 0 et VTPORRIM = 0')
        ENDIF
*
* Cas de non existence de pores rim avec un volume
* de pores rim non nul ou une quantit� de gaz de 
* pores rim non nulle : Impossible
*
        IF ( xmo_VD1(7) .EQ. 0.D0 .AND. 
     &       (xmo_VD1(9) .NE. 0.D0 .OR.
     &        xmo_VD1(8) .NE. 0.D0) ) THEN
          xmo_VD1(8) = 0.D0
          xmo_VD1(9) = 0.D0
          IDECOU = 61
          CALL MOMESS('CPORRIM = 0 et '//
     &                '( VTPORRIM > 0 ou CXEPORRIM > 0 ) ')
        ENDIF
*
* Cas de concentration de dislocations negatives
        IF ( xmo_VD1(10) .LT. 0.D0 ) THEN
          xmo_VD1(10) = 0.D0
          IDECOU = 62
          CALL MOMESS('RHOD < 0   ')
        ENDIF
*
************************************************************************
      ENDIF
************************************************************************  
*   
      DO I = 1, mo_NVG
        Y_COMPLET(I) = xmo_VG1(I)
      ENDDO
      DO I = 1,mo_NVD
        Y_COMPLET(mo_NVG+I) = xmo_VD1(I)
      ENDDO
*
      RETURN
      END
      SUBROUTINE MOFRAC_CONNEC(xmo_FCOUV,xmo_FRCONNEC)
C     =================
C
C
C     ******************************************************************
C     MOFRAC_CONNEC : MOgador FRACtion de joint connect�               
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogdon.inc'
C
***********************************************************************
* cmogppr.inc : xmo_FCOUVLIM xmo_DFCOUV mo_COUV_VAR xmo_AGG_REFERENCE 
*               xmo_AGG_EXP en entr�e
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogdon.inc : xmo_AGG en entr�e
************************************************************************
*
* xmo_RHOD est la densit� de dislocation moyenne (m/m3)
* xmo_FR est la fraction volumique restructur�e
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOFRAC_GEAR')
*
      IF (mo_COUV_VAR .EQ. 0) THEN
        xmo_FCOUVLIM_UTIL = xmo_FCOUVLIM
      ELSE
        xmo_FCOUVLIM_UTIL = xmo_FCOUVLIM * 
     &            ( xmo_AGG_REFERENCE / xmo_AGG ) ** xmo_AGG_EXP
      ENDIF
*
* Calcul de la fraction de volume restructur�
*
      IF ( xmo_FCOUV .LT. (xmo_FCOUVLIM_UTIL - xmo_DFCOUV) ) THEN
        xmo_FRCONNEC = 0.D0
      ELSEIF (xmo_FCOUV .GT. (xmo_FCOUVLIM_UTIL + xmo_DFCOUV) ) THEN
        xmo_FRCONNEC = 1.D0
      ELSE
        xmo_FRCONNEC = 0.5D0 *
     &          ( 1.D0 - sin( xmo_PIZAHL / 2.D0 / xmo_DFCOUV *
     &                        ( xmo_FCOUVLIM_UTIL - xmo_FCOUV ) )  )
      ENDIF
*
      RETURN
      END
      SUBROUTINE MOFRAC_GEAR(xmo_RHOD,xmo_FR)
C     =================
C
C
C     ******************************************************************
C     MOFRAC_GEAR : MOgador FRACtion de volumique restructur�e                
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
C
***********************************************************************
* cmogppr.inc : xmo_RHOLIM xmo_RHODL mo_MODELE_FRAC en entr�e
* cmogphy.inc : xmo_PIZAHL en entr�e
************************************************************************
*
* xmo_RHOD est la densit� de dislocation moyenne (m/m3)
* xmo_FR est la fraction volumique restructur�e
* mo_MODELE_FRAC est l'indicateur du choix de calcul de xmo_FR en 
*    fonction de xmo_RHOD.
*    mo_MODELE_FRAC = 1   Fonction sinus
*    mo_MODELE_FRAC = 2   Fonction lin�aire   
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOFRAC_GEAR')
*
*
* Calcul de la fraction de volume restructur�
*
************************************************************************
      IF (mo_MODELE_FRAC .EQ. 1 ) THEN
************************************************************************
      IF ( xmo_RHOD .LT. (xmo_RHOLIM - xmo_RHODL) ) THEN
        xmo_FR = 0.D0
      ELSEIF (xmo_RHOD .GT. (xmo_RHOLIM + xmo_RHODL) ) THEN
        xmo_FR = 1.D0
      ELSE
        xmo_FR = 0.5D0 *
     &          ( 1.D0 - sin( xmo_PIZAHL / 2.D0 / xmo_RHODL *
     &                        ( xmo_RHOLIM - xmo_RHOD ) )  )
      ENDIF
************************************************************************
      ENDIF
************************************************************************
*
*
*
************************************************************************
      IF (mo_MODELE_FRAC .EQ. 2 ) THEN
************************************************************************
      IF ( xmo_RHOD .LT. (xmo_RHOLIM - xmo_RHODL) ) THEN
        xmo_FR = 0.D0
      ELSEIF (xmo_RHOD .GT. (xmo_RHOLIM + xmo_RHODL) ) THEN
        xmo_FR = 1.D0
      ELSE
        xmo_FR = (xmo_RHOD - ( xmo_RHOLIM - xmo_RHODL )) 
     &           / 2.D0 / xmo_RHODL
      ENDIF
************************************************************************
      ENDIF
************************************************************************
      RETURN
      END
      SUBROUTINE MOGMAC(L,IGROB,I,IHETER)
C     =================
C
C
C     ******************************************************************
C     MOGMAC : MOgador calcul des Grandeurs MACroscopiques                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogdon.inc'
C
***********************************************************************
* cmogppr.inc : xmo_XGONF xmo_REPMA xmo_ZETA en entr�e
* cmogphy.inc : xmo_OMEGA xmo_XNAVOG en entr�e
* cmogent.inc : xmo_TOFISS xmo_DTS xmo_CXEJLIM en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR en entr�e
* cmogmat.inc : xmo_TVGG xmo_TVSEQ xmo_TVSEQR en entr�e
* cmogvar.inc : mo_IET xmo_VG xmo_VD en entr�e
* cmogsor.inc : xmo_SOMTOFIS3 en entr�e
*               xmo_GONFS xmo_SINPOR xmo_GASPOR
*               xmo_GCREA3 xmo_GINTER3 xmo_GINTRA3 xmo_GEPMA3 en sortie 
* cmoginf.inc : mo_NMAC en entree
* cmogtem.inc : xmo_TEMPS en entree 
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE xmo_RPAST en entree 
* cmogdon.inc : xmo_CXEPORI3 xmo_CXEPORJI3  en entree 
************************************************************************
*
*
* variables locales
*
*
      DOUBLE PRECISION  xmo_CXE(mo_NNSPHMX),
     &                  xmo_CXERBLOC(mo_NNSPHMX),
     &                  xmo_CXEJ(mo_NNSPHMX),
     &                  xmo_CXEHOM(mo_NNSPHMX),
     &                  xmo_VG1(mo_NVGAMX),
     &                  xmo_VD1(mo_NVDEMX)
*
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOGMAC')
*
***********************************************************************
              DO IVGA = 1, mo_NVGAMX
                 xmo_VG1(IVGA) = xmo_VECG(L,IGROB,I,IHETER,IVGA,1)
              ENDDO
*
              DO IVDE = 1, mo_NVDEMX
                 xmo_VD1(IVDE) = xmo_VECD(L,IGROB,I,IHETER,IVDE,1)
              ENDDO
*
              mo_IET = mo_IETCOM(L,IGROB,I,IHETER,1)
*
              xmo_SORDIFJ = xmo_SORDIFJ3(L,IGROB,I,IHETER,1)
              xmo_SORPERC = xmo_SORPERC3(L,IGROB,I,IHETER,1)
              xmo_ERRGLOB = xmo_ERRGLOB3(L,IGROB,I,IHETER)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat classique-connecte (0)
*
      IF (mo_IET .EQ. 0) THEN
* 
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*     
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*

*
* Les trois variables xmo_CBJ xmo_CXEBJ et xmo_VTBJ xmo_RBJ repr�sentent 
* Ltunj, CXetunj et Vtunj Rtunj dans le cas o� on se trouve dans l'�tat tunnel
*       
*
************************************************************************
* Calcul de xmo_CXEM et xmo_CXEJM
************************************************************************
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz intra dissous 
*
        CALL MOMOYE (xmo_CXE,  xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
*
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
           xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
*
************************************************************************
* calcul du gonflement solide xmo_GONFS3
************************************************************************
*
      xmo_GONFS3(L,IGROB,I,IHETER,1) = 
     &   xmo_XGONF * xmo_OMEGA * xmo_SOMTOFIS3(L,IGROB,I,IHETER,1)
     &   + 
     &   xmo_ZETA * xmo_OMEGA * xmo_XNAVOG * (xmo_CXEM + xmo_CXEJM) 
*
************************************************************************
* calcul du volume des pores rapport� au volume initial xmo_SINPOR
************************************************************************
* 
      xmo_SINPOR(L,IGROB,I,IHETER,1) = xmo_VTPOR + xmo_VTPORJ
*
************************************************************************
* calcul du volume des bulles rapport� au volume initial
* c'est le gonflement gazeux du aux bulles
************************************************************************
*
      xmo_GASPOR(L,IGROB,I,IHETER,1) = xmo_VTB + xmo_VTBJ
*
************************************************************************
* calcul du gaz cr��
* ATTENTION xmo_GCREA3(L,IGROB,I,IHETER,1) est en mol/m3 (diff�rent de METEOR)
* ce calcul est d�j� fait dans moafec
************************************************************************
*
*
************************************************************************
* calcul du gaz intra
* ATTENTION xmo_GINTRA3(L,IGROB,I,IHETER,1) mol/m3 (diff�rent de METEOR)
* ATTENTION le gaz mod�lis� par MARGARET V3.1 comprend le gaz initial contenu
*           dans les pores, qui dans la r�alit� n'est pas du Xe ou du Kr
* On fait une correction pour tenir compte de ceci
************************************************************************
*
      xmo_CORREC_GAZ_FISSION = xmo_GCREA3(L,IGROB,I,IHETER,1) /
     &  ( xmo_GCREA3(L,IGROB,I,IHETER,1) 
     &    + xmo_CXEPORI3(L,IGROB,I,IHETER)
     &    + xmo_CXEPORJI3(L,IGROB,I,IHETER) )
*
      xmo_GINTRA3(L,IGROB,I,IHETER,1) = 
     &      ( xmo_CXEM + xmo_CXEB + xmo_CXEPOR ) 
     &      * xmo_CORREC_GAZ_FISSION
*
************************************************************************
* calcul du gaz inter
* ATTENTION xmo_GINTER3(L,IGROB,I,IHETER,1) est en mol/m3 (diff�rent de METEOR)
************************************************************************
*
      xmo_GINTER3(L,IGROB,I,IHETER,1) = 
     &      ( xmo_CXEJM + xmo_CXEBJ + xmo_CXEPORJ ) 
     &      * xmo_CORREC_GAZ_FISSION
*
************************************************************************
* calcul du gaz mesurable � la microsonde
* ATTENTION xmo_GEPMA3(L,IGROB,I,IHETER,1) est en mol/m3 (diff�rent de METEOR)
************************************************************************
*
*
      xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &             ( xmo_CXEJM + xmo_CXEM ) 
*
      IF (xmo_RB .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEB 
      ENDIF
*
      IF (xmo_RPOR .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPOR 
      ENDIF
*
*
*
      IF (xmo_RBJ .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEBJ 
      ENDIF
*
*
      IF (xmo_RPORJ .LT. xmo_REPMA .AND. mo_IET .EQ. 0) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPORJ 
      ENDIF
*
* Correction pour retomber sur Kr+Xe uniquement
      xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &   xmo_GEPMA3(L,IGROB,I,IHETER,1)
     &      * xmo_CORREC_GAZ_FISSION        
*
*
************************************************************************
* fin du traitement de l'�tat classique-connecte (0) 
*
      ENDIF
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat "en restructuration" (2)
*
      IF (mo_IET .EQ. 2 ) THEN
*
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*           
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
************************************************************************
* Calcul de xmo_CXEM et xmo_CXEHOMM
************************************************************************
*
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz dissous dans les zones saines
*
        CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &               mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
*
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
           xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
* Calcul de la moyenne xmo_CXEHOMM des concentrations en 
* gaz dans le mileu homog�n�is�  
*
        CALL MOMOYE (xmo_CXEHOM, xmo_TVSEQR,
     &                 mo_NSEQR, mo_NNSPHMX, xmo_CXEHOMM)
*
*
*
************************************************************************
* calcul du gonflement solide xmo_GONFS3
************************************************************************
*
* On consid�re que le gaz du mileu homog�n�is� est dissous
* o� au moins dans de tr�s petites bulles ayant peu de lacunes en plus 
* que celles apport�es avec le gaz
*
      xmo_GONFS3(L,IGROB,I,IHETER,1) = 
     &  xmo_XGONF * xmo_OMEGA * xmo_SOMTOFIS3(L,IGROB,I,IHETER,1)
     &  + 
     &  xmo_OMEGA * xmo_XNAVOG * (xmo_CXEM + xmo_CXEJM + xmo_CXEHOMM)  
*
************************************************************************
* calcul du volume des pores rapport� au volume initial xmo_SINPOR
************************************************************************
* 
      xmo_SINPOR(L,IGROB,I,IHETER,1) = 
     &                    xmo_VTPOR + xmo_VTPORJ + xmo_VTPORRIM 
*
************************************************************************
* calcul du volume des bulles rapport� au volume initial
* c'est le gonflement gazeux du aux bulles
************************************************************************
*
      xmo_GASPOR(L,IGROB,I,IHETER,1) = 
     &          xmo_VTB + xmo_VTBJ + xmo_VTBRIM  
*
************************************************************************
* calcul du gaz cr��
* ATTENTION xmo_GCREA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
* fait dans moafec
************************************************************************
*
************************************************************************
* calcul du gaz intra
* ATTENTION xmo_GINTRA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
* ATTENTION le gaz mod�lis� par MARGARET V3.1 comprend le gaz initial contenu
*           dans les pores, qui dans la r�alit� n'est pas du Xe ou du Kr
* On fait une correction pour tenir compte de ceci
************************************************************************
*
      xmo_CORREC_GAZ_FISSION = xmo_GCREA3(L,IGROB,I,IHETER,1) /
     &  ( xmo_GCREA3(L,IGROB,I,IHETER,1) 
     &    + xmo_CXEPORI3(L,IGROB,I,IHETER)
     &    + xmo_CXEPORJI3(L,IGROB,I,IHETER) )
*
      xmo_GINTRA3(L,IGROB,I,IHETER,1) = 
     &       ( xmo_CXEM + xmo_CXEB + xmo_CXEPOR + 
     &         xmo_CXEHOMM  )
     &       * xmo_CORREC_GAZ_FISSION   
*
************************************************************************
* calcul du gaz inter
* ATTENTION xmo_GINTER3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
************************************************************************
*
      xmo_GINTER3(L,IGROB,I,IHETER,1) = 
     &      ( xmo_CXEJM + xmo_CXEBJ + xmo_CXEPORJ + 
     &        xmo_CXEBRIM + xmo_CXEPORRIM )
     &       * xmo_CORREC_GAZ_FISSION    
*
************************************************************************
* calcul du gaz mesurable � la microsonde
* ATTENTION xmo_GEPMA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
************************************************************************
*
*
*
      xmo_GEPMA3(L,IGROB,I,IHETER,1) =  
     &        xmo_CXEM + xmo_CXEJM + xmo_CXEHOMM 
*
      IF (xmo_RB .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEB 
      ENDIF
*
      IF (xmo_RPOR .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPOR 
      ENDIF
*
      IF (xmo_RBJ .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEBJ 
      ENDIF
*
      IF (xmo_RPORJ .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPORJ 
      ENDIF
*
      IF (xmo_RBRIM .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEBRIM 
      ENDIF
*
      IF (xmo_RPORRIM .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPORRIM 
      ENDIF
*
* Correction pour retomber sur Kr+Xe uniquement
      xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &   xmo_GEPMA3(L,IGROB,I,IHETER,1)
     &      * xmo_CORREC_GAZ_FISSION 
*           
*
*
************************************************************************
* fin du traitement de l'�tat "en restructuration" (2)
*
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat restructur� (3)
*
      IF (mo_IET .EQ. 3 ) THEN
*      
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL3(xmo_VG1,xmo_VD1,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM)
*
************************************************************************
* Calcul de xmo_CXEHOMM
************************************************************************
*
*
* Calcul de la moyenne xmo_CXEHOMM des concentrations en 
* gaz dans le mileu homog�n�is�  
*
        CALL MOMOYE (xmo_CXEHOM, xmo_TVSEQR, 
     &                 mo_NSEQR, mo_NNSPHMX, xmo_CXEHOMM)
*
*
*
************************************************************************
* calcul du gonflement solide xmo_GONFS3
************************************************************************
*
* On consid�re que le gaz du mileu homog�n�is� est dissous
* o� au moins dans de tr�s petites bulles ayant peu de lacunes en plus 
* que celles apport�es avec le gaz
*
      xmo_GONFS3(L,IGROB,I,IHETER,1) = 
     &  xmo_XGONF * xmo_OMEGA * xmo_SOMTOFIS3(L,IGROB,I,IHETER,1)
     &  + 
     &  xmo_OMEGA * xmo_XNAVOG * xmo_CXEHOMM  
*
************************************************************************
* calcul du volume des pores rapport� au volume initial xmo_SINPOR
************************************************************************
* 
      xmo_SINPOR(L,IGROB,I,IHETER,1) = xmo_VTPORRIM 
*
************************************************************************
* calcul du volume des bulles rapport� au volume initial
* c'est le gonflement gazeux du aux bulles
************************************************************************
*
      xmo_GASPOR(L,IGROB,I,IHETER,1) = xmo_VTBRIM 
*
************************************************************************
* calcul du gaz cr��
* ATTENTION xmo_GCREA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
* fait dans moafec
************************************************************************
*
************************************************************************
* calcul du gaz intra
* ATTENTION xmo_GINTRA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
* ATTENTION le gaz mod�lis� par MARGARET V3.1 comprend le gaz initial contenu
*           dans les pores, qui dans la r�alit� n'est pas du Xe ou du Kr
* On fait une correction pour tenir compte de ceci
************************************************************************
*
      xmo_CORREC_GAZ_FISSION = xmo_GCREA3(L,IGROB,I,IHETER,1) /
     &  ( xmo_GCREA3(L,IGROB,I,IHETER,1) 
     &    + xmo_CXEPORI3(L,IGROB,I,IHETER)
     &    + xmo_CXEPORJI3(L,IGROB,I,IHETER) )
*
      xmo_GINTRA3(L,IGROB,I,IHETER,1) = 
     &       xmo_CXEHOMM * xmo_CORREC_GAZ_FISSION   
*
************************************************************************
* calcul du gaz inter
* ATTENTION xmo_GINTER3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
************************************************************************
*
      xmo_GINTER3(L,IGROB,I,IHETER,1) = 
     &      ( xmo_CXEBRIM + xmo_CXEPORRIM )
     &       * xmo_CORREC_GAZ_FISSION 
*
************************************************************************
* calcul du gaz mesurable � la microsonde
* ATTENTION xmo_GEPMA3(L,IGROB,I,IHETER,1) est en mol/m3 (different de METEOR)
************************************************************************
*
*
*
      xmo_GEPMA3(L,IGROB,I,IHETER,1) =  xmo_CXEHOMM 
*
      IF (xmo_RBRIM .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEBRIM 
      ENDIF
*
      IF (xmo_RPORRIM .LT. xmo_REPMA ) THEN
        xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &                          xmo_GEPMA3(L,IGROB,I,IHETER,1) 
     &                        + xmo_CXEPORRIM 
      ENDIF
*
* Correction pour retomber sur Kr+Xe uniquement
      xmo_GEPMA3(L,IGROB,I,IHETER,1) = 
     &   xmo_GEPMA3(L,IGROB,I,IHETER,1)
     &      * xmo_CORREC_GAZ_FISSION 
*                 
*
*
************************************************************************
* fin du traitement de l'�tat restructur� (3)
*
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************  
*
*
*
*
* format pour ecriture dans fichier res_macroscopique
 9000 FORMAT (1X,E26.8,3I26,E26.8,I26,35(E26.8)) 
*
      RETURN
      END
      SUBROUTINE MOGRAD_VF(xmo_CXE, xmo_CLIM, xmo_A, mo_INDIC, xmo_GRAD)
C     ====================
C
C
C     ******************************************************************
C     MOGRAD : MOgador calcul du GRADient de concentration de gaz 
C              dissous intra � la surface du grain (version V.F.)                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogpnu.inc : xmo_RGG  xmo_RESQ xmo_RSEQR mo_NGG mo_NSEQ mo_NSEQR en entr�e
* cmoginf.inc : mo_DEBUG  en entr�e
************************************************************************
*
* param�tres de la subroutine
* xmo_CXE : vecteur des concentrations en gaz dissous dans le grain, 
*           ou sur la sph�re �quivalente, ou sur la sph�re �quivalente du rim
* xmo_CLIM : concentration en gaz dissous � la limite du grain, de sph�re 
*           �quivalente, ou de la sph�re �quivalente du rim
* xmo_A : Rayon de la sph�re consid�r�e et � la surface de laquelle
*         on calcule le gradient de concentration (m)
* mo_INDIC : indicateur pr�cisant si on calcule le gradient de concentration 
*            de gaz dissous sur le bord du grain (0), de la sph�re �quivalente (1), 
*            ou de la sph�re �quivalente du rim (2)
* xmo_GRAD : r�sultat de la subroutine : gradient de concentration
*            � la fronti�re du grain, de sph�re 
*            �quivalente, ou de la sph�re �quivalente du rim
*
************************************************************************
*
      DOUBLE PRECISION xmo_CXE(mo_NNSPHMX)
*
      CALL MOMESS('DANS MOGRAD_VF')
*
      IF ( mo_INDIC .EQ. 0 ) THEN 
        xmo_GRAD = 
     &     2.D0 / xmo_A / (xmo_RGG(mo_NGG) - xmo_RGG(mo_NGG-1)) *
     &             ( xmo_CLIM - xmo_CXE(mo_NGG))
      ELSEIF ( mo_INDIC .EQ. 1 ) THEN 
        xmo_GRAD = 
     &     2.D0 / xmo_A / 
     &    (xmo_RSEQ(mo_NSEQ) - xmo_RSEQ(mo_NSEQ-1)) *
     &    ( xmo_CLIM - xmo_CXE(mo_NSEQ)) 

      ELSEIF ( mo_INDIC .EQ. 2 ) THEN
        xmo_GRAD = 
     &     2.D0 / xmo_A / 
     &     (xmo_RSEQR(mo_NSEQR) - xmo_RSEQR(mo_NSEQR-1)) *
     &     ( xmo_CLIM - xmo_CXE(mo_NSEQR)) 
      ELSE
       WRITE(mo_DEBUG,*) 
     &       'mo_INDIC doit valoir 0 1 ou 2 dans MOGRAD'
       STOP 
      ENDIF 
*   
      RETURN
      END
      SUBROUTINE MOINBA_GEAR(mo_IB) 
C     =================
C
C
C     ******************************************************************
C     MOINBA : MOgador INitialisation des variables et de l'etat du combustible
C              apr�s un BAsculement potentiel                                   
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogmat.inc'
C
************************************************************************
* cmogvar.inc : mo_IET xmo_VG xmo_VD mo_NVG mo_NVD en entr�es et sorties
*               xmo_ASEB en sorties
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR xmo_RSEQ xmo_RSEQR en entr�e               
* cmogent.inc : xmo_ASE en entr�es 
* cmogppr.inc : xmo_CBRIM en entr�e
* cmogphy.inc : xmo_PIZAHL xmo_XNAVOG en entr�e
* cmogmat.inc : xmo_TVGG  xmo_TVSEQ   en entr�e
************************************************************************
      DOUBLE PRECISION xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEJINTERP(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX),
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_EQUADISP_AVBASC(mo_NPOPMX)
C
C
      CALL MOMESS('DANS MOINBA_GEAR')
C
      DO NPOP = 1,mo_NPOPMX
        xmo_EQUADISP_AVBASC(NPOP) = xmo_EQUADISP(NPOP)
      ENDDO
C
************************************************************************
C-----7--0---------0---------0---------0---------0---------0---------0--
C***********************************************************************
C***********************************************************************
C***********************************************************************
      IF (mo_IB .EQ. 0) THEN
C       Il n'y a pas de basculement, il n'y a rien � faire
C***********************************************************************
C***********************************************************************
C***********************************************************************
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
      ELSEIF (mo_IB .EQ. 2) THEN
C       Le combustible doit basculer de l'�tat classique (0)
C       vers l'�tat 'en restructuration' (2)
C
C
C***********************************************************************
C mise � jour des populations eventuellement disparues
C***********************************************************************
* gaz intra zone saine
        xmo_EQUADISP(1) = xmo_EQUADISP_AVBASC(1)
* bulle intra zone saine
        xmo_EQUADISP(2) = xmo_EQUADISP_AVBASC(2)
* gaz inter zone saine
        xmo_EQUADISP(3) = xmo_EQUADISP_AVBASC(3)
* bulle inter zone saine
        xmo_EQUADISP(4) = xmo_EQUADISP_AVBASC(4)
* gaz zone restructur�e
        xmo_EQUADISP(5) = 1.D0
* bulle de rim
        xmo_EQUADISP(6) = 1.D0
* pores intra zone saine
        xmo_EQUADISP(7) = xmo_EQUADISP_AVBASC(5)
* pores inter zone saine
        xmo_EQUADISP(8) = xmo_EQUADISP_AVBASC(6)
* pores de rim
        xmo_EQUADISP(9) =  xmo_EQUADISP_AVBASC(6)
* dislocations
        xmo_EQUADISP(10) =  xmo_EQUADISP_AVBASC(7)
C***********************************************************************
CC Utilisation de variables locales plus parlantes
C
        DO  IVGA = 1, mo_NVGAMX
          xmo_VG1(IVGA) = xmo_VG(IVGA,2) 
        ENDDO
C
        DO  IVDE = 1, mo_NVDEMX
          xmo_VD1(IVDE) = xmo_VD(IVDE,2) 
        ENDDO 
        CALL MOPARL(0,xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
C        
C
C calcul de la moyenne de xmo_CXEJ
C
C
        CALL MOCONS(0,mo_DIF_SEQ,mo_NSEQ_C)
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
           xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
C
C***********************************************************************
* Initialisation du nouvel �tat
        mo_IET = 2
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
        mo_NZONE_SAINE = 2*mo_NGG + 3 + mo_NSEQ_C + 3
        mo_NVG = mo_NZONE_SAINE + mo_NSEQR  + 3
        mo_NVD = 3 + 3 + 3 + 1
        xmo_ASEB = xmo_ASE
* initialisation de xmo_ASERIM
        CALL MOASER(0.D0)
C***********************************************************************
C***********************************************************************
C r�initialisation des variables xmo_VG(..,2) et xmo_VD(..,2) 
C***********************************************************************
C
C Les 2*mo_NGG + 3  premi�res variables du vecteur xmo_VG
C ne changent pas
C Autrement dit les variables intra deviennent les variables intra 
C relatives aux zones saines au moment du basculement
C
C Initialisation des variables inter
C Elle ne change que si la diffusion �tait prise en compte dans 
C l'�tat (0) et pas dans l'�tat (1)
C
        IF (mo_DIF_SEQ .EQ. 0) THEN
          xmo_VG(2*mo_NGG+3+1,2) = xmo_CXEJM
          xmo_VG(2*mo_NGG+3+1+1,2) = xmo_CBJ
          xmo_VG(2*mo_NGG+3+1+2,2) = xmo_CXEBJ
          xmo_VG(2*mo_NGG+3+1+3,2) = xmo_VTBJ
        ENDIF
C
C
C
C Concentration en gaz dissous xmo_CXEHOM 
C dans le milieu homog�ne restructur�
        DO  IVGA = 1, mo_NSEQR
          xmo_VG(mo_NZONE_SAINE+IVGA,2) = 0.D0
        ENDDO
C
C
C Bulles de rim dans les zones restructur�es
C
        DO  IVGA = 1, 3
          xmo_VG(mo_NZONE_SAINE+mo_NSEQR+IVGA,2) = 0.D0
        ENDDO
C
C le reste du tableau est nul
C
        DO IVGA = mo_NZONE_SAINE+mo_NSEQR+4, mo_NVGAMX
          xmo_VG(IVGA,2) = 0.D0
        ENDDO 
C
C Les 6 premi�res variables du vecteur xmo_VD ne changent pas
C Autrement dit les variables des pores intra et inter deviennent 
C les variables relativesaux pores des zones saines au moment 
C du basculement 
C
C Les pores de rim sont initialiss � zero               
C
        xmo_VD(7,2) = 0.D0
        xmo_VD(8,2) = 0.D0
        xmo_VD(9,2) = 0.D0
C
C La densit� de dislocation continue a �tre celle 
C des zones saines
C
        xmo_VD(10,2) = xmo_RHOD
C
C Le reste du tableau est nul
        DO IVDE = 11, mo_NVDEMX
          xmo_VD(IVDE,2) = 0.D0
        ENDDO
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
      ELSEIF (mo_IB .EQ. 23) THEN
C
CDEBUG        WRITE(20,*) 'dans moinba_gear passage etat 3'
C
C       Le combustible doit basculer de l'�tat 'en restructuration' (2)
C       vers l'�tat restructur� (3)
C       Normallement, les variables relatives aux zones saines sont 
C       pratiquement nulles
C
C
C***********************************************************************
C mise � jour des populations eventuellement disparues
C***********************************************************************
* gaz zone restructur�e
        xmo_EQUADISP(1) = 1.D0
* bulle de rim
        xmo_EQUADISP(2) = 1.D0
* pores de rim
        xmo_EQUADISP(3) =  xmo_EQUADISP_AVBASC(6)
C***********************************************************************
        DO  IVGA = 1, mo_NVGAMX
          xmo_VG1(IVGA) = xmo_VG(IVGA,2) 
        ENDDO
C
        DO  IVDE = 1, mo_NVDEMX
          xmo_VD1(IVDE) = xmo_VD(IVDE,2) 
        ENDDO 
C
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
C 
C on fait la moyenne de la concentration en gaz dissous des zones saines
C
        CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
C
C calcul de la moyenne de xmo_CXEJ
C
C
        CALL MOCONS(2,mo_DIF_SEQ,mo_NSEQ_C)
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
           xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
C***********************************************************************
C Initialisation du nouvel �tat
        mo_IET = 3
        mo_NVG = mo_NSEQR + 3
        mo_NVD = 3       
C***********************************************************************
C r�initialisation des variables xmo_VG(..,2) et xmo_VD(..,2) en d�but 
C de pas de temps
C***********************************************************************
C
C Concentration en gaz dissous xmo_CXEHOM dans le milieu homog�ne restructur�
        DO  IVGA = 1, mo_NSEQR
          xmo_VG(IVGA,2) = xmo_CXEM + xmo_CXEJM + xmo_CXEHOM(IVGA)
        ENDDO
C
C
C
C Bulles de rim dans les zones restructur�es
C
        xmo_VG(mo_NSEQR+1,2) = xmo_CBRIM
        xmo_VG(mo_NSEQR+2,2) = 
     &         xmo_CXEBRIM + xmo_CXEB + xmo_CXEPOR + xmo_CXEBJ
        xmo_VG(mo_NSEQR+3,2) = 
     &         xmo_VTBRIM + xmo_VTB + xmo_VTPOR + xmo_VTBJ
C
C le reste du tableau est nul
C
        DO IVGA = mo_NSEQR+4, mo_NVGAMX
          xmo_VG(IVGA,2) = 0.D0
        ENDDO 
C
C Pores de rim dans les zones restructur�es
        xmo_VD(1,2) = xmo_CPORRIM
        xmo_VD(2,2) = xmo_CXEPORRIM + xmo_CXEPORJ
        xmo_VD(3,2) = xmo_VTPORRIM + xmo_VTPORJ
C
C Le reste du tableau est nul
        DO IVDE = 4, mo_NVDEMX
          xmo_VD(IVDE,2) = 0.D0
        ENDDO
C
C
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
C***********************************************************************
      ENDIF
C
C Pour le cas o� un basculement effectif est d�tect� exactement au temps 
C de fin du pas de temps METEOR, aucun calcul ne sera effectu� avant
C le passage au pas de temps suvant, il faut donc initialiser
C xmo_VG(...,1) et xmo_VD(...,1) correctement aussi.
C Dans le cas o� le basculement se produit en cours de pas de temps 
C METEOR ces valeurs seront �cras�es.
C
      DO IVGA = 1, mo_NVGAMX
        xmo_VG(IVGA,1) = xmo_VG(IVGA,2)
      ENDDO 
C
      DO IVDE = 1, mo_NVDEMX
        xmo_VD(IVDE,1) = xmo_VD(IVDE,2)
      ENDDO
C
C
      RETURN
      END
      SUBROUTINE MOINEQ_GEAR
C     ============================================
C
C
C     ******************************************************************
C     MOINEQ_GEAR : MOgador INitialisation de la m�thode de GEAR
C                   Equations et param�tres : � faire une seule fois                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR en entr�e 
* cmogvar.inc : mo_IET xmo_EQUADISP en entr�e 
* cmogpte.inc : xmo_EQUAG xmo_EQUAB xmo_EQUAP xmo_EQUAGJ 
*               xmo_EQUABJ xmo_EQUAPJ xmo_EQUARHOD en entr�e
* cmogear.inc : mo_NEQG mo_NEQD mo_G_NEQ mo_IGTAB xmo_G_RTOL 
*               xmo_G_ATOL  
*               mo_ISTATE mo_G_IWORK xmo_G_RWORK  
*               mo_G_LWR mo_G_LWI
*               en sortie
* cmoginf.inc : mo_DEBUG en entree
* cmogppr.inc : mo_MODELE_DENSIFICATION mo_EQUA_CXERBLOC en entree 
************************************************************************ 
*-----7--0---------0---------0---------0---------0---------0---------0--
*
C
C
*
      CALL MOMESS('DANS MOINEQ_GEAR')      
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
C
***********************************************************************
* on initialise le nombre d'�quations effectives, le tableau de 
* correspondances mo_IGTAB et les tol�rances absolues et relatives
*
       mo_NEQG = 0
       mo_NEQD = 0
       mo_G_NEQ =  0
*
***********************************************************************
* ETAT CLASSIQUE (0)
***********************************************************************
       IF (mo_IET .EQ. 0) THEN
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
       CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
       IF ( xmo_EQUAG*xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NGG
           mo_IGTAB(I) = I 
           xmo_G_RTOL(I) = 1.e-4
           xmo_G_ATOL(I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NGG
         mo_G_NEQ = mo_G_NEQ + mo_NGG 
       ENDIF
*
       IF ( xmo_EQUAB*xmo_EQUADISP(2) .EQ. 1.D0 ) THEN
* equations du gaz dissous au voisinnage des bulles
         IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
           DO I = 1,mo_NGG
             mo_IGTAB(mo_NEQG+I) = mo_NGG+I 
             xmo_G_RTOL(mo_NEQG+I) = 1.e-4
             xmo_G_ATOL(mo_NEQG+I) = 1.e-8
           ENDDO
           mo_NEQG = mo_NEQG + mo_NGG
           mo_G_NEQ = mo_G_NEQ + mo_NGG
         ENDIF
* equations des bulles
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3 
       ENDIF
*
       IF (xmo_EQUAGJ*xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NSEQ_C
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+3+I
           xmo_G_RTOL(mo_NEQG+I) = 1.e-4
           xmo_G_ATOL(mo_NEQG+I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NSEQ_C
         mo_G_NEQ = mo_G_NEQ + mo_NSEQ_C
       ENDIF
*
       IF (xmo_EQUABJ*xmo_EQUADISP(4) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+3+mo_NSEQ_C+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3
       ENDIF
*
       IF (xmo_EQUAP*xmo_EQUADISP(5) .EQ. 1.D0 ) THEN
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           DO I = 1,3
             mo_IDTAB(I) = I
           ENDDO
           xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
           xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
           xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
           mo_NEQD = mo_NEQD+3
           mo_G_NEQ = mo_G_NEQ + 3 
         ELSE
*       Margaret ne va calculer que le gaz contenu dans les pores intra
           mo_IDTAB(1) = 2
           xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+1) = 1.e-11
           mo_NEQD = mo_NEQD+1
           mo_G_NEQ = mo_G_NEQ + 1 
         ENDIF
       ENDIF
*
       IF (xmo_EQUAPJ*xmo_EQUADISP(6) .EQ. 1.D0 ) THEN
         IF (mo_MODELE_DENSIFICATION .EQ. 0) THEN
           DO I = 1,3
             mo_IDTAB(mo_NEQD+I) = I+3
           ENDDO
           xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
           xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
           xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
           mo_NEQD = mo_NEQD+3
           mo_G_NEQ = mo_G_NEQ +3
         ELSE
*       Margaret ne va calcul� que le gaz contenu dans les pores inter
           mo_IDTAB(mo_NEQD+1) = 5
           xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
           xmo_G_ATOL(mo_G_NEQ+1) = 1.e-11
           mo_NEQD = mo_NEQD+1
           mo_G_NEQ = mo_G_NEQ +1
         ENDIF
       ENDIF
*
       IF (xmo_EQUARHOD*xmo_EQUADISP(7) .EQ. 1.D0 ) THEN
         mo_IDTAB(mo_NEQD+1) = 7
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e10
         mo_NEQD = mo_NEQD+1
         mo_G_NEQ = mo_G_NEQ +1
       ENDIF
*
       ENDIF
***********************************************************************
* FIN ETAT CLASSIQUE (0)
***********************************************************************
*
*
************************************************************************
* ETAT 'EN RESTRUCTURATION' (2)
***********************************************************************
       IF (mo_IET .EQ. 2) THEN
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
       CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
* equation du gaz dissous intra zones saines
       IF ( xmo_EQUAG*xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NGG
           mo_IGTAB(I) = I 
           xmo_G_RTOL(I) = 1.e-4
           xmo_G_ATOL(I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NGG
         mo_G_NEQ = mo_G_NEQ + mo_NGG
       ENDIF 
*
* equation des bulles intra zones saines
       IF ( xmo_EQUAB*xmo_EQUADISP(2) .EQ. 1.D0 ) THEN
* equations du gaz dissous au voisinnage des bulles
         IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
           DO I = 1,mo_NGG
             mo_IGTAB(mo_NEQG+I) = mo_NGG+I 
             xmo_G_RTOL(mo_NEQG+I) = 1.e-4
             xmo_G_ATOL(mo_NEQG+I) = 1.e-8
           ENDDO
           mo_NEQG = mo_NEQG + mo_NGG
           mo_G_NEQ = mo_G_NEQ + mo_NGG
         ENDIF
* equations des bulles intra de zone saine
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3 
       ENDIF
*
* equation du gaz dissous inter joints sains
       IF (xmo_EQUAGJ*xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NSEQ_C
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+3+I
           xmo_G_RTOL(mo_NEQG+I) = 1.e-4
           xmo_G_ATOL(mo_NEQG+I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NSEQ_C
         mo_G_NEQ = mo_G_NEQ + mo_NSEQ_C
       ENDIF
*
* equations des bulles inter joints sains
       IF (xmo_EQUABJ*xmo_EQUADISP(4) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = 2*mo_NGG+3+mo_NSEQ_C+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3
       ENDIF
*
       mo_NZONE_SAINE = 2*mo_NGG+3+mo_NSEQ_C+3 
*       
* equation du gaz dissous zones restructur�es
       IF ( xmo_EQUADISP(5) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NSEQR
           mo_IGTAB(mo_NEQG+I) = mo_NZONE_SAINE+I
           xmo_G_RTOL(mo_NEQG+I) = 1.e-4
           xmo_G_ATOL(mo_NEQG+I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NSEQR
         mo_G_NEQ = mo_G_NEQ + mo_NSEQR
       ENDIF
*
*
* equation des bulles de rim zones restructur�es
       IF (xmo_EQUADISP(6) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = mo_NZONE_SAINE+mo_NSEQR+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3
       ENDIF
*
* equations des pores intra zones saines
       IF (xmo_EQUAP*xmo_EQUADISP(7) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IDTAB(I) = I
         ENDDO
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
         xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
         xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
         mo_NEQD = mo_NEQD+3
         mo_G_NEQ = mo_G_NEQ + 3 
       ENDIF
*
* equations des pores inter zones saines
       IF (xmo_EQUAPJ*xmo_EQUADISP(8) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IDTAB(mo_NEQD+I) = I+3
         ENDDO
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
         xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
         xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
         mo_NEQD = mo_NEQD+3
         mo_G_NEQ = mo_G_NEQ +3
       ENDIF
*
* equations des pores zones restructur�es
       IF (xmo_EQUAPJ*xmo_EQUADISP(9) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IDTAB(mo_NEQD+I) = I+3+3
         ENDDO
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
         xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
         xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
         mo_NEQD = mo_NEQD+3
         mo_G_NEQ = mo_G_NEQ +3
       ENDIF
*
* equations de la densit� de dislocation
       IF (xmo_EQUARHOD*xmo_EQUADISP(10) .EQ. 1.D0 ) THEN
         mo_IDTAB(mo_NEQD+1) = 10
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e10
         mo_NEQD = mo_NEQD+1
         mo_G_NEQ = mo_G_NEQ +1
       ENDIF
*
*
       ENDIF
***********************************************************************
* FIN ETAT 'EN RESTRUCTURATION' (2)
***********************************************************************
*
*
*
***********************************************************************
* ETAT RESTRUCTURE (3)
***********************************************************************
       IF (mo_IET .EQ. 3) THEN
* equation du gaz dissous zones restructur�es
       IF (xmo_EQUADISP(1) .EQ. 1.D0 ) THEN
         DO I = 1,mo_NSEQR
           mo_IGTAB(mo_NEQG+I) = I
           xmo_G_RTOL(mo_NEQG+I) = 1.e-4
           xmo_G_ATOL(mo_NEQG+I) = 1.e-8
         ENDDO
         mo_NEQG = mo_NEQG + mo_NSEQR
         mo_G_NEQ = mo_G_NEQ + mo_NSEQR
       ENDIF
*
*
* equation des bulles de rim zones restructur�es
       IF (xmo_EQUADISP(2) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IGTAB(mo_NEQG+I) = mo_NSEQR+I
         ENDDO
         xmo_G_RTOL(mo_NEQG+1) = 1.e-4
         xmo_G_ATOL(mo_NEQG+1) = 1.e12
         xmo_G_RTOL(mo_NEQG+2) = 1.e-4
         xmo_G_ATOL(mo_NEQG+2) = 1.e-11
         xmo_G_RTOL(mo_NEQG+3) = 1.e-4
         xmo_G_ATOL(mo_NEQG+3) = 1.e-16
         mo_NEQG = mo_NEQG + 3
         mo_G_NEQ = mo_G_NEQ + 3
       ENDIF
*
* equations des pores zones restructur�es
       IF (xmo_EQUAPJ*xmo_EQUADISP(3) .EQ. 1.D0 ) THEN
         DO I = 1,3
           mo_IDTAB(mo_NEQD+I) = I
         ENDDO
         xmo_G_RTOL(mo_G_NEQ+1) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+1) = 1.e12
         xmo_G_RTOL(mo_G_NEQ+2) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+2) = 1.e-11
         xmo_G_RTOL(mo_G_NEQ+3) = 1.e-4
         xmo_G_ATOL(mo_G_NEQ+3) = 1.e-16
         mo_NEQD = mo_NEQD+3
         mo_G_NEQ = mo_G_NEQ +3
       ENDIF
*
*
       ENDIF
***********************************************************************
* FIN ETAT RESTRUCTURE (3)
***********************************************************************
*
CDEBUG
CDEBUG       WRITE(mo_DEBUG,*) ' en fin de moineq_gear'
CDEBUG       WRITE(mo_DEBUG,*) ' mo_NEQG = ',mo_NEQG,
CDEBUG     &            ' mo_IGTAB = ',(mo_IGTAB(I),I=1,mo_NEQG)
CDEBUG       WRITE(mo_DEBUG,*) ' mo_NEQD = ',mo_NEQD,
CDEBUG     &            ' mo_IDTAB = ',(mo_IDTAB(I),I=1,mo_NEQD)
CDEBUG       WRITE(mo_DEBUG,*) ' mo_G_NEQ = ', mo_G_NEQ
CDEBUG
*
*
*
*  Initialisation des param�tres du solveur lsoda
*
       mo_ISTATE = 1
       DO I=1,5
         mo_G_IWORK(I) = 0
         xmo_G_RWORK(I) = 0.D0
       ENDDO
*
*
       mo_G_LWR = 22 + 16*mo_G_NEQ + mo_G_NEQ**2 
       mo_G_LWI = 20 + mo_G_NEQ
*
CDEBUG
CDEBUG       write(20,*) 'fin moineq_gear mo_NVG = ', mo_NVG
CDEBUG       write(20,*) 'fin moineq_gear mo_NVD = ', mo_NVD
CDEBUG       write(20,*) 'fin moineq_gear mo_G_NEQ = ',mo_G_NEQ
CDEBUG       write(20,*) 'fin moineq_gear mo_G_LWR = ',mo_G_LWR
CDEBUG       write(20,*) 'fin moineq_gear mo_G_LWI = ',mo_G_LWI
CDEBUG
       mo_G_IWORK(1) = mo_G_NEQ - 1
       mo_G_IWORK(2) =  mo_G_NEQ - 1
*        
*
*
       RETURN
       END
*
*
      SUBROUTINE MOINIT
*     =================
*
*     ******************************************************************
*     MOINIT : MOgador INITialisation 
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogdon.inc' 
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogpnu.inc'  
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogsor.inc' 
      INCLUDE 'cmogear.inc'
* essaiLOC
      INCLUDE 'cmogloc.inc' 
* essaiLOC
*     
************************************************************************      
* cmogphy.inc : toutes en sorties
* cmogvar.inc : toutes en sorties
* cmogdon.inc : xmo_CPORI , xmo_VTPORI, xmo_CPORJI et xmo_VTPORJI
*               xmo_AGG en entr�es
* cmogppr.inc : xmo_COECASC xmo_MUF xmo_EFFMAX xmo_EREIMP xmo_TETBJ xmo_RHODI
*               xmo_MN en entr�e
*               xmo_NELSON  xmo_ATETBJ en sortie
* cmogtem.inc : xmo_TEMPS en sortie
* cmogpnu.inc : mo_NGG  mo_NSEQ  en entr�e
* cmogpas.inc : xmo_DBUCH en sortie
* cmogsor.inc : xmo_SOMTOFIS3 xmo_GCREA3 xmo_REMISOL3
*               xmo_SORDIF3 xmo_SORBUL3 xmo_SORPOR3 
*               xmo_SORDIFJ3  xmo_SORPERC3 en sortie
* cmogear.inc : xmo_RSAV3 mo_ISAV3 xmo_G_RWORKSAV3 mo_G_IWORKSAV3 mo_G_IEQSAV3 
*               xmo_G_RTOLSAV3 mo_IGTABSAV3 mo_IDTABSAV3 en sortie
* essaiLOC
* cmogloc.inc : xmo_CXERPORLOC03 en sortie
* essaiLOC
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
      CALL MOMESS('DANS MOINIT')
*
* Initialisation des grandeurs physiques
*
      DATA xmo_OMEGA / 40.9D-30 /
     &     xmo_ZXE / 54.D0 /
     &     xmo_ELEC / 1.6D-19 /
     &     xmo_EPSI0 / 8.854D-12 /
     &     xmo_TCXE / 289.7D0 /
     &     xmo_PCXE / 58.D5 / 
     &     xmo_ZD / 6.D0 /
     &     xmo_XNAVOG / 6.023D23 /
     &     xmo_BOLTZ / 1.380662D-23 /
     &     xmo_PIZAHL / 3.141592654 /
*
* Initialisation de certaines constantes physiques
*
      xmo_BVDW = xmo_BOLTZ * xmo_TCXE / xmo_PCXE / 8.D0
      xmo_AVDW = 27.D0 /64.D0 * xmo_BOLTZ**2.D0 * xmo_TCXE**2.D0
     &       / xmo_PCXE 
*
* initialisation de certains param�tres primaires
*
      IF ( xmo_EFFMAX .GT. 0.D0 .AND. xmo_EREIMP .GT. 0.D0 .AND.
     &     xmo_EREIMP .GT. 0.D0 .AND. xmo_EFFMAX .GT. 0.D0 ) THEN
*     -----------------------------------------------------------
*
        xmo_NELSON = 
     &    xmo_COECASC * 2.D0 * xmo_MUF *
     &    LOG(xmo_EFFMAX / xmo_EREIMP) *
     &    xmo_PIZAHL * xmo_ZXE**4.D0 / xmo_EFFMAX /
     &    xmo_EREIMP *
     &    ( xmo_ELEC**2.D0 / 4.D0 /xmo_PIZAHL / xmo_EPSI0 )**2.D0
      ENDIF
*     _____
*
      xmo_RAT = (3.D0 / 4.D0 / xmo_PIZAHL * xmo_OMEGA)**(1.D0 / 3.D0)
*
      xmo_ATETBJ = (  1.D0 - 3.D0/2.D0 * COS(xmo_TETBJ) + 
     &                1.D0/2.D0 * COS(xmo_TETBJ)**2.D0  ) /
     &             SIN(xmo_TETBJ)**3.D0  
*
*
* initialisation du temps
*
      xmo_TEMPS = 0.D0
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
      CALL MOCONS(0,mo_DIF_SEQ,mo_NSEQ_C)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* Initialisation des variables
*
      DO 10 L = 1,mo_M31MAX
        DO 10 IGROB = 1,mo_IGRMAX
          DO 10 I = 1,mo_IFEMAX
            DO 10 IHETER = 1, mo_NAMAMX
*  Etat du combustible = etat classique (0)
              mo_IETCOM(L,IGROB,I,IHETER,2) = 0 
*
* Variable traceant la premi�re fois o� l'on fait tourner
* un syst�me d'�quation donn� (1 si oui, 0 si non)
* Cette variable trace aussi le d�but du calcul en g�n�ral (2 si d�but)
              mo_PRFOIS3(L,IGROB,I,IHETER) = 2 
*
*
* Variable traceant le fait que des populations peuvent s'annuler
* completement, auquel cas les equations correspondantes ne doivent
* plus etre prises en compte dans le solveur
*
              DO NPOP = 1, mo_NPOPMX
                xmo_EQUADISP3(L,IGROB,I,IHETER,NPOP) = 1.D0
              ENDDO
*
              xmo_SOMTOFIS3(L,IGROB,I,IHETER,2) = 0.D0
              xmo_GONFS3(L,IGROB,I,IHETER,2) = 0.D0
              xmo_GCREA3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_REMISOL3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_SORDIF3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_SORBUL3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_SORPOR3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_SORDIFJ3(L,IGROB,I,IHETER,2) = 0.D0 
              xmo_SORPERC3(L,IGROB,I,IHETER,2) = 0.D0
              xmo_SOREJEC3(L,IGROB,I,IHETER,2) = 0.D0 
*
* Tableau de stockage de la m�thode de GEAR : xmo_RSAV3 et mo_ISAV3
              DO IS = 1,240
                xmo_RSAV3(L,IGROB,I,IHETER,IS) = 0.D0
              ENDDO
*
              DO IS = 1,50
                mo_ISAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
              DO IS = 1,mo_LWRMAX
                xmo_G_RWORKSAV3(L,IGROB,I,IHETER,IS) = 0.D0
              ENDDO
*
              DO IS = 1,mo_LWIMAX
                mo_G_IWORKSAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
              DO IS = 1,5
                mo_G_IEQSAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
              DO IS = 1,mo_NEQMAX
                xmo_G_RTOLSAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
              DO IS = 1,mo_NVGAMX
                mo_IGTABSAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
              DO IS = 1,mo_NVDEMX
                mo_IDTABSAV3(L,IGROB,I,IHETER,IS) = 0
              ENDDO
*
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--
************VARIABLES DU MODELE DE GAZ**********************************
*
*  Concentration en gaz intra dissous aux noeuds interne du maillage 
*  du grain
*
              DO IVGA = 1,mo_NGG
                xmo_VECG(L,IGROB,I,IHETER,IVGA,2) = 0.D0
              ENDDO
*
*  Concentration en gaz intra dissous aux noeuds interne du maillage 
*  du grain au voisinage des bulles intra
*
              DO IVGA = 1,mo_NGG
                xmo_VECG(L,IGROB,I,IHETER,IVGA + mo_NGG,2) = 0.D0
              ENDDO
*
              mo_CH12 = 2
*  Concentration des bulles intragranulaires 
*
              xmo_VECG(L,IGROB,I,IHETER,mo_CH12*mo_NGG+1,2) = 0.D0
*
*  Concentration en gaz de bulles intragranulaires 
*
              xmo_VECG(L,IGROB,I,IHETER,mo_CH12*mo_NGG+2,2) = 0.D0
*
*  Volume total des bulle intragranulaires 
*
              xmo_VECG(L,IGROB,I,IHETER,mo_CH12*mo_NGG+3,2) = 0.D0
*
              mo_N_INTRA = mo_CH12*mo_NGG+3
*  Concentration en gaz inter dissous aux noeuds interne du maillage 
*  de la sph�re �quivalente
* 
              DO 30 IVGA = mo_N_INTRA + 1,mo_N_INTRA+mo_NSEQ_C
                xmo_VECG(L,IGROB,I,IHETER,IVGA,2) = 0.D0
 30           CONTINUE
*
*  Concentration des bulles intergranulaires
*
              xmo_VECG(L,IGROB,I,IHETER,
     &                 mo_N_INTRA+mo_NSEQ_C+1,2) = 0.D0
*
*  Concentration en gaz de bulles intergranulaires
*
              xmo_VECG(L,IGROB,I,IHETER,mo_N_INTRA+mo_NSEQ_C+2,2) 
     &                 = 0.D0
*
*     7--0---------0---------0---------0---------0---------0---------0--
*  Volume total des bulles intergranulaires 
*
              xmo_VECG(L,IGROB,I,IHETER,mo_N_INTRA+mo_NSEQ_C+3,2) 
     &                 = 0.D0
*
*  Le reste du tableau ne sert pas
*
              DO 40 IVGA = mo_N_INTRA+mo_NSEQ_C+4,mo_NVGAMX
                xmo_VECG(L,IGROB,I,IHETER,IVGA,2) = 0.D0
 40           CONTINUE 
*
* essaiLOC
              DO IVGA = 1,mo_NNSPHMX
                xmo_CXERPORLOC03(L,IGROB,I,IHETER,IVGA) = 0.D0
              ENDDO
* essaiLOC
*     7--0---------0---------0---------0---------0---------0---------0--
*******************VARIABLES DU MODELE DE DEFAUT************************
* calcul des quantit�s de gaz contenues initialement dans les 
* pores intra et inter et assimilation ou non de ce gaz � du Xenon selon le 
* mod�le choisi
* NB: Dans MOATOP et dans la suite, xmo_CPORI xmo_CPORJI xmo_CXEPORI xmo_CXEPORJI 
* xmo_VTPORI xmo_VTPORJI  correspondent aux valeurs au point
* (L,IGROB,I,IHETER)
*
*
              CALL MOATOP(L,IGROB,I,IHETER)
*
*  Concentration des pores (= pores de fabrication) intragranulaires
*
              xmo_VECD(L,IGROB,I,IHETER,1,2) = xmo_CPORI
*
*  Concentration en gaz de pores intragranulaires
*  xmo_CXEPORI vient d etre calcul� par MOATOP en fonction du mod�le
*  choisi (gaz initial assimile � du Xenon ou pas)
*
              xmo_VECD(L,IGROB,I,IHETER,2,2) = xmo_CXEPORI
*
*  Volume total des pores intragranulaire
*
              xmo_VECD(L,IGROB,I,IHETER,3,2) = xmo_VTPORI
                 
*
*  Concentration des pores (= pores de fabrication) intergranulaires
*
              xmo_VECD(L,IGROB,I,IHETER,4,2) = xmo_CPORJI
*
*  Concentration en gaz de pores intergranulaires
*  xmo_CXEPORJI vient d etre calcul� par MOATOP en fonction du mod�le
*  choisi (gaz initial assimile � du Xenon ou pas)
*
              xmo_VECD(L,IGROB,I,IHETER,5,2) = xmo_CXEPORJI
*
*   Volume total des pores intergranulaire 
*
              xmo_VECD(L,IGROB,I,IHETER,6,2) = xmo_VTPORJI
*
*
*  Densit� de dislocation initiale (NOGITA et UNE ref39 de la 
*  note de spec num�rique du mod�le MARGARET V3.1 : 10**13.8)
*
              xmo_VECD(L,IGROB,I,IHETER,7,2) = xmo_RHODI
*
************************************************************************
************************************************************************
*              
 10   CONTINUE
*
*
      RETURN
      END
      SUBROUTINE MOINIT_GEAR(Y)
C     ============================================
C
C
C     ******************************************************************
C     MOINIT_GEAR : MOgador INITialisation de la m�thode de GEAR
C                                          
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogear.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
***********************************************************************
* cmogvar.inc : xmo_VG et xmo_VD mo_NVG mo_NVD en entr�e 
* cmogear.inc : xmo_YEXP en sortie 
************************************************************************ 
*-----7--0---------0---------0---------0---------0---------0---------0--
*
C
      DOUBLE PRECISION Y(*)
C
      CALL MOMESS('DANS MOINIT_GEAR')   
*
************************************************************************
* Initialisation des tableaux xmo_YEXP et Y
*
CDEBUG
CDEBUG          IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans moinit_gear juste avant init de xmo_YEXP'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
*
       DO IVGA = 1, mo_NVG
         xmo_YEXP(IVGA) = xmo_VG(IVGA,2)
         Y(IVGA) = 0.D0
       ENDDO
*  
       DO IVDE = 1, mo_NVD
         xmo_YEXP(IVDE+mo_NVG) = xmo_VD(IVDE,2)
         Y(IVDE+mo_NVG) = 0.D0
       ENDDO
*
*
       RETURN
       END
      SUBROUTINE MOITPO(xmo_R1,NDINT1,xmo_R2,NDINT2,xmo_C1,
     &                  xmo_C2)
C     =================
C
C
C     ******************************************************************
C     MOITPO : MOgador InTerPOlation d'un maillage sur l'autre                      
C     ******************************************************************
*
*
***********************************************************************
* xmo_R1 : tableaux des rayons internes d�finissant un maillage adimensionn�
* NDINT1 : Nombres de noeuds internes de ce premier maillage
* xmo_R2 : tableaux des rayons internes d�finissant un autre maillage adimensionn�
* NDINT2 : Nombres de noeuds internes de ce deuxi�me maillage
* xmo_C1 : vecteur de concentrations aux noeuids du premier maillage
* xmo_C2 : vecteur de concentrations aux neouds du second maillage : R�sultat
*          de la subroutine
* xmo_RM2 : tableau des rayons des milieux des mailles. Utilis� en VF
*           uniquement. Variable locale.
***********************************************************************
* 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
***********************************************************************
* cmogpnu.inc : en entr�e
*
***********************************************************************
*
      DOUBLE PRECISION xmo_R1(mo_NNSPHMX+1),
     &                 xmo_R2(mo_NNSPHMX+1),
     &                 xmo_RM1(mo_NNSPHMX+1),
     &                 xmo_RM2(mo_NNSPHMX+1),
     &                 xmo_C1(mo_NNSPHMX),
     &                 xmo_C2(mo_NNSPHMX)
*
*
      CALL MOMESS('DANS MOITPO')
*
************************************************************************ 
* VOLUMES FINIS
************************************************************************ 
************************************************************************
* Remarque : En volume fini, les valeurs du tableau de concentration 
* doivent �tre affect�es aux milieux des mailles
*
* Initialisation du tablau xmo_RM1 des rayons des milieux des mailles
* du premier millage
*
      xmo_RM1(1) = xmo_R1(1) / 2.D0
      DO I = 2, NDINT1
        xmo_RM1(I) = ( xmo_R1(I) + xmo_R1(I-1) ) / 2.D0
      ENDDO
*
* Initialisation du tablau xmo_RM2 des rayones des milieux des mailles
* du deuxi�me millage
*
      xmo_RM2(1) = xmo_R2(1) / 2.D0
      DO I = 2, NDINT2
        xmo_RM2(I) = ( xmo_R2(I) + xmo_R2(I-1) ) / 2.D0
      ENDDO 
*
************************************************************************
* Boucle sur les xmo_RM2(I)
************************************************************************
      DO 100 I = 1, NDINT2
        CALL MORECH_VF(xmo_R1,NDINT1,xmo_RM2(I),NIP)
*
        IF (xmo_RM2(I) .GE. xmo_RM1(NIP)) THEN
*       ++++++++++++++++++++++++++++++++++++++
* Cas o� xmo_RM2(I) se trouve dans la maille NIP, dans la moiti� droite
          IF (NIP .LT. NDINT1) THEN
*         =========================
            xmo_POND = (xmo_RM2(I) - xmo_RM1(NIP)) / 
     &               (xmo_RM1(NIP+1) - xmo_RM1(NIP))
            xmo_C2(I) = 
     &         (xmo_C1(NIP+1) - xmo_C1(NIP)) * xmo_POND + xmo_C1(NIP)
          ELSE
*         ====
*           Cas o� xmo_RM2(I) se trouve dans la moiti� droite de la derni�re couronne
            IF (NDINT1 .GT. 1) THEN
              xmo_POND = (xmo_RM2(I) - xmo_RM1(NIP)) / 
     &               (xmo_RM1(NIP) - xmo_RM1(NIP-1))
              xmo_C2(I) = 
     &         (xmo_C1(NIP) - xmo_C1(NIP-1)) * xmo_POND + xmo_C1(NIP)
              xmo_C2(I) = max( 0.D0 , xmo_C2(I) )
            ELSE
*             Cas o� il n'y a qu'une seule couronne dans le maillage initial
              xmo_C2(I) = xmo_C1(NIP)
            ENDIF
          ENDIF
*         =====
*
        ELSE
*       ++++
* Cas o� xmo_RM2(I) se trouve dans la maille NIP, dans la moiti� gauche
*
          IF (NIP .GT. 1) THEN
            xmo_POND = (xmo_RM2(I) - xmo_RM1(NIP-1)) / 
     &               (xmo_RM1(NIP) - xmo_RM1(NIP-1))
            xmo_C2(I) = 
     &         (xmo_C1(NIP) - xmo_C1(NIP-1)) * xmo_POND + xmo_C1(NIP-1)
          ELSE
            xmo_C2(I) = xmo_C1(1)
          ENDIF
*
        ENDIF
*       +++++
 100  CONTINUE
************************************************************************
* Fin de boucle sur les xmo_RM2(I)
************************************************************************
************************************************************************ 
************************************************************************    
      RETURN
      END
C        1         2         3         4         5         6         7
      SUBROUTINE MOMAPR_VF(xmo_R,NDINT,xmo_TIM,xmo_TV,xmo_RREM,xmo_TIMP)
C
C **********************************************************************
C MOMAPR : MOgadodor MAtrices pour un PRofil de remise en solution
C          Il s'agit de matrices adimensionnelles
C ********************************************************************** 
C
C
C **********************************************************************
C PARAMETRES DU SOUS-PROGRAMME
C **********************************************************************
C
C en entr�e :
C xmo_R : tableau des rayons correspondants aux noeuds du maillage de 
C         la sph�re consid�r�e
C NDINT : Nombre de noeuds internes de la sph�re consid�r�e
C NDINT : (en volumes finis) nombre de noeuds de la sph�re consid�r�e
C xmo_TIM inverse de la matrice masse 
C RREM : Rayon relatif de remise en solution 
C        (Rayon grain - Distance de remise en solution)/ rayon du grain
C
C en sortie :
C xmo_TIMP inverse de la matrice masse * xmo_TP
C 
C tous les reels utilises doivent etre en double precision
C 
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      PARAMETER (NDINTMX = mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_TIM(NDINTMX,NDINTMX),
     &                 xmo_TIMP(NDINTMX),
     &                 xmo_TV(NDINTMX),
     &                 xmo_R(NDINTMX+1)
C
C
C **********************************************************************
C VARIABLES LOCALES
C **********************************************************************
C 
      DOUBLE PRECISION xmo_TP(NDINTMX)
C 
C **********************************************************************
C CORPS DU SOUS-PROGRAMME
C **********************************************************************
C
      CALL MOMESS('DANS MOMAPR_VF')
C
C INITIALISATION DES VECTEURS xmo_TP, xmo_TIMP  a zero
C
      DO 10 I = 1, NDINTMX
         xmo_TIMP(I) = 0.D0
         xmo_TP(I) = 0.D0
 10   CONTINUE
C
C
C
C
************************************************************************
C Calcul du vecteur xmo_TP qui sert pour le traitement de la remise en solution
C du joint vers le grain
************************************************************************
C     Recherche de la maille dans le quel se trouve xmo_RREM
C     NIP est le num�ro de cette maille. La maille NIP est d�limit�e
C     par les noeuds de rayons xmo_R(NIP-1) et xmo_R(NIP)
************************************************************************
*
*
      CALL MORECH_VF(xmo_R,NDINT,xmo_RREM,NIP)
*
*
************************************************************************
      xmo_TP(NIP) = (xmo_R(NIP)**3 - xmo_RREM**3) / 3.D0
      DO K = NIP+1, NDINT
        xmo_TP(K) = xmo_TV(K)
      ENDDO
      DO K = 1, NDINT
        xmo_TP(K) = xmo_TP(K) / (1.D0 - xmo_RREM**3.D0)
      ENDDO
C
************************************************************************
************************************************************************
C Calcul de xmo_TIMP
C
      CALL XMMVEC_DIAG(NDINT,NDINT,NDINTMX,NDINTMX,
     &            xmo_TIM,xmo_TP,xmo_TIMP)
C

      RETURN
      END
C        1         2         3         4         5         6         7
      SUBROUTINE MOMAQ_VF(xmo_R,NDINT,xmo_TIM,xmo_TIMQ)
C
C **********************************************************************
C MOMAQ_VF : MOgadodor MATrices pour l'�quation de la diffusion sur 
C           la sph�res �quivalente ou 
C           la sph�re �quivalente du combustible restructur�
C           Il s'agit de matrices adimensionnelles
C           Calcul du vecteur xmo_TIMQ en volume fini
C ********************************************************************** 
C
C
C **********************************************************************
C PARAMETRES DU SOUS-PROGRAMME
C **********************************************************************
C
C en entr�e :
C xmo_R : tableau des rayons correspondants aux limites des couronnes de 
C         la sph�re consid�r�e
C NDINT : (en volumes finis) Nombre de couronne de la sph�re consid�r�e
C xmo_TIM inverse de la matrice masse 
C en sortie :
C xmo_TIMQ inverse de la matrice masse * vecteur Q
C 
C tous les reels utilises doivent etre en double precision
C 
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      PARAMETER (NDINTMX = mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_TIM(NDINTMX,NDINTMX),
     &                 xmo_TIMQ(NDINTMX),
     &                 xmo_R(NDINTMX+1)
C
C
C **********************************************************************
C VARIABLES LOCALES
C **********************************************************************
C 
      DOUBLE PRECISION xmo_TQ(NDINTMX)
C 
C **********************************************************************
C CORPS DU SOUS-PROGRAMME
C **********************************************************************
C
      CALL MOMESS('DANS MOMAQ_VF')
C
C INITIALISATION DES MATRICES xmo_TM, xmo_TR et xmo_TV a zero
C
      DO 10 I = 1, NDINTMX
         xmo_TQ(I) = 0.D0
         xmo_TIMQ(I) = 0.D0
 10   CONTINUE
C
C
      xmo_TQ(NDINT) = -1.D0 / 0.5D0 / (1.D0 - xmo_R(NDINT-1))
C
C 
C***********************************************************
C Calcul de xmo_TIMQ = xmo_TIM * xmo_TQ
C
      CALL XMMVEC_DIAG (NDINT,NDINT,NDINTMX,NDINTMX,xmo_TIM,
     &          xmo_TQ,xmo_TIMQ)
C 
C
C***********************************************************
C
      RETURN
      END
C        1         2         3         4         5         6         7
      SUBROUTINE MOMATR_VF(xmo_R,NDINT,
     &                     xmo_TM,xmo_TIM,xmo_TIMR,xmo_TV)
C
C **********************************************************************
C MOMAT : MOgadodor MATrices pour l'�quation de la diffusion sur 
C           le gros grain, ou le sph�res �quivalente ou 
C           la sph�re �quivalente du combustible restructur�
C           Il s'agit de matrices adimensionnelles
C ********************************************************************** 
C
C
C **********************************************************************
C PARAMETRES DU SOUS-PROGRAMME
C **********************************************************************
C
C en entr�e :
C xmo_R : tableau des rayons correspondants aux noeuds du maillage de 
C         la sph�re consid�r�e
C NDINT : Nombre de noeuds internes de la sph�re consid�r�e
C NDINT : (en volumes finis) Nombre de noeuds de la sph�re consid�r�e
C
C en sortie :
C xmo_TM matrice masse d�finie dans la note de sp�c num
C xmo_TIM inverse de la matrice masse 
C xmo_TIMR inverse de la matrice masse * matrice rigidit�
C xmo_TV vecteur utilis� pour le calcul de la moyenne de la concentration 
C       sur la sph�re consid�r�e
C 
C tous les reels utilises doivent etre en double precision
C 
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      PARAMETER (NDINTMX = mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_TM(NDINTMX,NDINTMX),xmo_TIM(NDINTMX,NDINTMX),
     &                 xmo_TIMR(NDINTMX,NDINTMX),
     &                 xmo_TV(NDINTMX),
     &                 xmo_R(NDINTMX+1)
C
C
C **********************************************************************
C VARIABLES LOCALES
C **********************************************************************
C 
      DOUBLE PRECISION xmo_TR(NDINTMX,NDINTMX),
     &                 xmo_RM(NDINTMX),
     &                 xmo_R2(NDINTMX)
C 
C **********************************************************************
C CORPS DU SOUS-PROGRAMME
C **********************************************************************
C
      CALL MOMESS('DANS MOMATR_VF')
C
C INITIALISATION DES MATRICES xmo_TM, xmo_TR et xmo_TV a zero
C
      DO 10 I = 1, NDINTMX
         xmo_TV(I) = 0.D0
         DO 10 J = 1, NDINTMX
            xmo_TM(I,J) = 0.D0
            xmo_TIM(I,J) = 0.D0
            xmo_TR(I,J) = 0.D0
            xmo_TIMR(I,J) = 0.D0
 10   CONTINUE
C
C
C
C 
C***********************************************************
C CALCUL DES MATRICE DE MASSE ET DE RIGIDITE DU SYSTEME
C***********************************************************
C
C Le calcul se fait perpendiculairement a la diagonale,
C pour la partie superieure
C
C masse (diagonale):
C
      R3D =  xmo_R(1)**3
      xmo_TM(1,1) = R3D/3.D0
      DO I = 2, NDINT
          R3G = R3D
          R3D = xmo_R(I)**3 
          xmo_TM(I,I) = (R3D - R3G)/3.D0
      ENDDO
C
C-----7--0---------0---------0---------0---------0---------0---------0--
C rigidite (tridiagonale):
C
C     calcul des points milieux des mailles et des carr�s des rayons
      xmo_RM(1) = 0.5D0*xmo_R(1)
      xmo_R2(1) = xmo_R(1)**2
      DO I = 2, NDINT
        xmo_RM(I) = 0.5D0*(xmo_R(I)+xmo_R(I-1))
        xmo_R2(I) = xmo_R(I)**2
      ENDDO
c
c     premi�re ligne
      FLUXG = - xmo_R2(1) / (xmo_RM(2)-xmo_RM(1))
      xmo_TR(1,1) = - FLUXG
      xmo_TR(1,2) = - xmo_TR(1,1)
c
c     mailles int�rieures
      DO I = 2, NDINT-1
        FLUXD = xmo_R2(I) / (xmo_RM(I+1)-xmo_RM(I))
        xmo_TR(I,I) = FLUXD - FLUXG
        xmo_TR(I,I+1) = - FLUXD
        FLUXG = - FLUXD
      ENDDO
C
c     derni�re ligne (prise en compte de la condition aux limites)
      FLUXD = 1.D0 / (1.D0 - xmo_RM(I)) 
      xmo_TR(NDINT,NDINT) = FLUXD - FLUXG 
C
C COMPLETION PAR SYMETRIE
C
      DO 20 I = 1, NDINT
         DO 20 J = I+1, NDINT
            xmo_TM(J,I) = xmo_TM(I,J)
            xmo_TR(J,I) = xmo_TR(I,J)
 20   CONTINUE
C
C
C***********************************************************
C INITIALISATION DU VECTEUR V DES COEFFICIENTS GEOMETRIQUES DE CALCUL
C DE LA CONCENTRATION MOYENNE DANS UN GRAIN
C***********************************************************
C
      DO I = 1, NDINT
          xmo_TV(I) = xmo_TM(I,I)
      ENDDO   
C
C-----7--0---------0---------0---------0---------0---------0---------0--
C
C
C***********************************************************
C
C CALCUL DE L INVERSE xmo_TIM DE TMGG
C
      DO 30 I = 1, NDINT
         DO 30 J = 1, NDINT
            xmo_TIM(I,J) = 0.D0
 30   CONTINUE
C
      DO 40 I = 1, NDINT
         xmo_TIM(I,I) = 1.D0/xmo_TM(I,I)
 40   CONTINUE
C
C***********************************************************
C Calcul de xmo_TIMR
C
      CALL XMMAT(NDINT,NDINT,NDINT,NDINTMX,NDINTMX,NDINTMX,
     &           xmo_TIM,xmo_TR,xmo_TIMR)
C 
C
C***********************************************************
C
      RETURN
      END
      SUBROUTINE MOMESI(xmo_MESSAGE,mo_VALEUR_ENTIERE)
C     =================
C
C
C     ******************************************************************
C     MOMESI : MOgador ecriture d'un message et d'une valeur enti�re 
C              seulement si le temps est superieur au temps d'affichage  
C              lu dans le jeu de donnees                                                        
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogtem.inc : xmo_TEMPS en entr�e
* cmoginf.inc : mo_DEBUG xmo_TPSAFF en entr�e
************************************************************************
* xmo_MESSAGE : Message a ecrire
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CHARACTER * (*) xmo_MESSAGE
*
      IF ( (xmo_TEMPS .GE. xmo_TPSAFF(1)) .AND.
     &     (xmo_TEMPS .LE. xmo_TPSAFF(2)) ) THEN
        WRITE(mo_DEBUG,*) xmo_MESSAGE, mo_VALEUR_ENTIERE
      ENDIF
*
      RETURN
      END
      SUBROUTINE MOMESS(xmo_MESSAGE)
C     =================
C
C
C     ******************************************************************
C     MOMESS : MOgador ecriture d'un message seulement si le temps
C              est superieur au temps d'affichage lu dans le jeu de donnees                                                        
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogtem.inc : xmo_TEMPS en entr�e
* cmoginf.inc : mo_DEBUG xmo_TPSAFF en entr�e
************************************************************************
* xmo_MESSAGE : Message a ecrire
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CHARACTER * (*) xmo_MESSAGE
*
      IF ( (xmo_TEMPS .GE. xmo_TPSAFF(1)) .AND.
     &     (xmo_TEMPS .LE. xmo_TPSAFF(2)) ) THEN
        WRITE(mo_DEBUG,*) xmo_MESSAGE
      ENDIF
*
      RETURN
      END
      SUBROUTINE MOMESV(xmo_MESSAGE,xmo_VALEUR)
C     =================
C
C
C     ******************************************************************
C     MOMESV : MOgador ecriture d'un message et d'une valeur seulement 
C              si le temps est superieur au temps d'affichage lu dans 
C              le jeu de donnees                                                        
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmoginf.inc'
C
***********************************************************************
* cmogtem.inc : xmo_TEMPS en entr�e
* cmoginf.inc : mo_DEBUG xmo_TPSAFF en entr�e
************************************************************************
* xmo_MESSAGE : Message a ecrire
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CHARACTER * (*) xmo_MESSAGE
*
      IF ( (xmo_TEMPS .GE. xmo_TPSAFF(1)) .AND.
     &     (xmo_TEMPS .LE. xmo_TPSAFF(2)) ) THEN
        WRITE(mo_DEBUG,*) xmo_MESSAGE, xmo_VALEUR
      ENDIF
*
      RETURN
      END
      SUBROUTINE MOMOYC_MIN (xmo_CVEC,xmo_FR,xmo_CCOMP,xmo_TM,
     &                   NDINT,
     &                   xmo_CMOYCMIN)
C     =================

C
C
C     ******************************************************************
C     MOMOYC_MINMAX : MOgador MOYenne du Carr� des concentrations 
C     avec des min et ou des max (version VF)
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      INCLUDE 'moparam.inc'
      PARAMETER(NDINTMX=mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_TM(NDINTMX,NDINTMX),
     &                 xmo_CVEC(NDINTMX),
     &                 xmo_CVECMIN(NDINTMX),
     &                 xmo_RESVEC(NDINTMX)
C
      CALL MOMESS('DANS MOMOYC_MIN')
C
      CALL XMMVEC_DIAG
     &     (NDINT,NDINT,NDINTMX,NDINTMX,xmo_TM,xmo_CVEC,xmo_RESVEC)
C
      DO I = 1, NDINT
        xmo_CVECMIN(I) = 
     &      min (xmo_CVEC(I) / (1.D0 - xmo_FR) , xmo_CCOMP ) 
      ENDDO
C
      xmo_PRODSCAL1 = XMVEVE(xmo_CVECMIN,xmo_RESVEC,NDINT,NDINTMX)
C
      xmo_CMOYCMIN = 3.D0 *  xmo_PRODSCAL1
C
      RETURN
      END
      SUBROUTINE MOMOYC_VF (xmo_CVEC,xmo_TM,
     &                   NDINT,
     &                   xmo_CMOYC)
C     =================

C
C
C     ******************************************************************
C     MOMOYC : MOgador MOYenne du Carr� des concentrations (version VF)*
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      INCLUDE 'moparam.inc'
      PARAMETER(NDINTMX=mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_TM(NDINTMX,NDINTMX),
     &                 xmo_CVEC(NDINTMX),
     &                 xmo_RESVEC(NDINTMX)
C
      CALL MOMESS('DANS MOMOYC_VF')
C
      CALL XMMVEC_DIAG
     &     (NDINT,NDINT,NDINTMX,NDINTMX,xmo_TM,xmo_CVEC,xmo_RESVEC)
C
      xmo_PRODSCAL1 = XMVEVE(xmo_CVEC,xmo_RESVEC,NDINT,NDINTMX)
C
      xmo_CMOYC = 3.D0 *  xmo_PRODSCAL1
C
      RETURN
      END
      SUBROUTINE MOMOYE (xmo_CVEC,xmo_TV,
     &                   NDINT,NDINTMX,
     &                   xmo_CMOY)
C     =================

C
C
C     ******************************************************************
C     MOMOYE : MOgador MOYEnne                                         *
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
C
      DOUBLE PRECISION xmo_TV(NDINTMX),
     &                 xmo_CVEC(NDINTMX)
C
C
      CALL MOMESS('DANS MOMOYE')
C
      xmo_PRODSCAL = XMVEVE(xmo_CVEC,xmo_TV,NDINT,NDINTMX)
C
      xmo_CMOY = 3.D0 * xmo_PRODSCAL 
C
      RETURN
      END
      SUBROUTINE MONETT
C     =================
C
C
C     ******************************************************************
C     MONETT : MOgador NETToyage des tableaux locaux                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogear.inc : xmo_G_RWORK xmo_G_RTOL xmo_G_ATOL xmo_YEXP mo_G_IWORK
*               mo_IGTAB mo_IDTAB  xmo_RSAV xmo_G_RWORKSAV xmo_G_RTOLSAV xmo_RSAV_av
*               xmo_G_RWORKSAV_av xmo_G_RTOLSAV_av mo_ISAV
*               mo_G_IWORKSAV  mo_G_IEQSAV  mo_IGTABSAV mo_IDTABSAV mo_ISAV_av
*               mo_G_IWORKSAV_av  mo_G_IEQSAV_av  mo_IGTABSAV_av mo_IDTABSAV_av
*               en sortie
***********************************************************************      
***********************************************************************
* 
*
***********************************************************************
      CALL MOMESS('DANS MONETT')
*
* Nettoyage des valeurs locales des common de cmogdon.inc
      xmo_AGG  = 0.D0
      xmo_VTPORI  = 0.D0
      xmo_VTPORJI  = 0.D0
      xmo_CPORI = 0.D0 
      xmo_CPORJI = 0.D0 
      xmo_ATPOR = 0.D0 
      xmo_ATPORJ = 0.D0
      xmo_CXEPORI  = 0.D0
      xmo_CXEPORJI = 0.D0
      xmo_MAVOLI = 0.D0
      mo_MATER = 0
*
* Nettoyage des valeurs locales des common de cmogppr.inc
      xmo_TETPORJ = 0.D0
      xmo_ATETPORJ = 0.D0
*
* Nettoyage des tableaux locaux des common de cmogear.inc
*
      DO I = 1, mo_LWRMAX
        xmo_G_RWORK(I) = 0.D0
      ENDDO
*
      mo_G_LWR = 0
      mo_G_LWI = 0
      mo_G_NEQ = 0
      mo_NEQG = 0
      mo_NEQD = 0
      mo_ISTATE = 0
*
      DO I = 1, mo_NEQMAX
        xmo_G_RTOL(I) = 0.D0
      ENDDO
*
      DO I = 1, mo_NEQMAX
        xmo_G_ATOL(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_NEQMAX 
        xmo_YEXP(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_LWIMAX 
        mo_G_IWORK(I) = 0
      ENDDO
*
      DO I = 1,mo_NVGAMX 
        mo_IGTAB(I) = 0
      ENDDO
*
      DO I = 1,mo_NVDEMX 
        mo_IDTAB(I) = 0
      ENDDO
*
      DO I = 1, 240
        xmo_RSAV(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_LWRMAX
        xmo_G_RWORKSAV(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_NEQMAX
        xmo_G_RTOLSAV(I) = 0.D0
      ENDDO
*
      DO I = 1,240
        xmo_RSAV_av(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_LWRMAX
        xmo_G_RWORKSAV_av(I) = 0.D0
      ENDDO
*
      DO I = 1,mo_NEQMAX
        xmo_G_RTOLSAV_av(I) = 0.D0
      ENDDO
*
      DO I = 1,50
        mo_ISAV(I) = 0
      ENDDO
*
      DO I = 1,mo_LWIMAX
        mo_G_IWORKSAV(I) = 0
      ENDDO
*
      DO I = 1,5
        mo_G_IEQSAV(I) = 0
      ENDDO
*
      DO I = 1,mo_NVGAMX
        mo_IGTABSAV(I) = 0
      ENDDO
*
      DO I = 1,mo_NVDEMX
        mo_IDTABSAV(I) = 0
      ENDDO
*
      DO I = 1,50
        mo_ISAV_av(I) = 0
      ENDDO
*
      DO I = 1,mo_LWIMAX
        mo_G_IWORKSAV_av(I) = 0
      ENDDO
*
      DO I = 1,5
        mo_G_IEQSAV_av(I) = 0
      ENDDO
*
      DO I = 1,mo_NVGAMX
        mo_IGTABSAV_av(I) = 0
      ENDDO
*
      DO I = 1,mo_NVDEMX
        mo_IDTABSAV_av(I) = 0
      ENDDO
*
*
* nettoyage des valeurs  de cmogent.inc
*
      xmo_ASE = 0.D0  
      xmo_RXEKR = 0.D0  
      xmo_PPLEN  = 0.D0 
      xmo_DTS  = 0.D0 
      xmo_TOFISS  = 0.D0 
      xmo_TK  = 0.D0 
      xmo_GRADTK  = 0.D0 
      xmo_PHYDR  = 0.D0 
      xmo_GAMXE = 0.D0  
      xmo_EFISS = 0.D0  
      xmo_CXEJLIM = 0.D0 
      xmo_DERCXEJLIM  = 0.D0
* 
* nettoyage des valeurs  de cmogpas.inc
*
      xmo_BETAC = 0.D0 
      xmo_DXE = 0.D0 
      xmo_DXEJ  = 0.D0
      xmo_BJ  = 0.D0
      xmo_KG  = 0.D0
      xmo_DUS = 0.D0 
      xmo_DS  = 0.D0
      xmo_DBVOL  = 0.D0              
      xmo_DBSUR  = 0.D0
      xmo_DUJ  = 0.D0
      xmo_DU  = 0.D0
      xmo_VBSUR = 0.D0 
      xmo_VBVOL  = 0.D0               
      xmo_DBUCH  = 0.D0 
      xmo_DBJVO  = 0.D0
      xmo_DBJSU  = 0.D0              
      xmo_VBJVOL = 0.D0 
      xmo_VBJSUR  = 0.D0
      xmo_KGJ  = 0.D0
      xmo_FACGCJ  = 0.D0               
      xmo_FACPBJ  = 0.D0
      xmo_OMSKT  = 0.D0
      xmo_PVI  = 0.D0
      xmo_PVIJ2   = 0.D0               
      xmo_PVIJ1_B  = 0.D0 
      xmo_PVIJ1_POR  = 0.D0             
      xmo_FACRHO  = 0.D0
      xmo_GAMMA   = 0.D0               
      xmo_DISTCOAL = 0.D0
      xmo_DISTCOALJ = 0.D0             
*
* Nettoyage des valeurs locales de cmogvar.inc
*
      DO I = 1,mo_NVGAMX
        DO J = 1,2
          xmo_VG(I,J) = 0.D0
        ENDDO
      ENDDO
*
      DO I = 1,mo_NVDEMX
        DO J = 1,2
          xmo_VD(I,J) = 0.D0
        ENDDO
      ENDDO
*
      DO I = 1,mo_NPOPMX
        xmo_EQUADISP(I) = 0.D0
      ENDDO
*
      xmo_ASEB = 0.D0
      xmo_ASERIM = 0.D0
*
      mo_IET = 0
      mo_IET_APPR = 0
      mo_PRFOIS = 0
      mo_NVG = 0
      mo_NVD = 0
*
* Nettoyage des valeurs locales de cmogsor.inc
*
      xmo_TAUXBETAC(1) = 0.D0
      xmo_TAUXBETAC(2) = 0.D0
      xmo_TAUXSORBUL(1) = 0.D0
      xmo_TAUXSORBUL(2) = 0.D0
      xmo_TAUXSORPOR(1) = 0.D0
      xmo_TAUXSORPOR(2) = 0.D0
      xmo_TAUXREMISOL(1) = 0.D0
      xmo_TAUXREMISOL(2) = 0.D0
      xmo_TAUXSOURXE(1) = 0.D0
      xmo_TAUXSOURXE(2) = 0.D0
      xmo_TAUXOUTSE(1) = 0.D0
      xmo_TAUXOUTSE(2) = 0.D0
      xmo_TAUXOUTPERC(1) = 0.D0
      xmo_TAUXOUTPERC(2) = 0.D0
      xmo_TAUXOUTEJEC(1) = 0.D0
      xmo_TAUXOUTEJEC(2) = 0.D0
      xmo_GCREE = 0.D0
      xmo_REMISOL = 0.D0
      xmo_SORDIF = 0.D0
      xmo_SORBUL = 0.D0
      xmo_SORPOR = 0.D0
      xmo_SORDIFJ = 0.D0
      xmo_SORPERC = 0.D0
      xmo_SOREJEC = 0.D0
      xmo_ERRINTRA = 0.D0
      xmo_ERRINTER  = 0.D0
      xmo_ERRGLOB  = 0.D0
*
      RETURN
      END
      SUBROUTINE MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
C     =================
C
C
C     ******************************************************************
C     MOPARL : MOgador initialisation de variables plus PARLantes                                    
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpnu.inc'
C
***********************************************************************
* cmogppr.inc : xmo_ATETBJ xmo_ATETPORJ   en entr�e
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQ en entr�e
************************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOPARL')
*
*
************************************************************************
************************************************************************
*  POUR ETAT CLASSIQUE - CONNECTE
************************************************************************
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
      CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
*
* Utilisation de variables locales plus parlantes
* et dans le cas o� il y a incoh�rence entre les concentration
* volume total d'un type de cavit�, on met tout � z�ro
*
************************************************************************
* Gaz et bulles intra
************************************************************************
        DO IVGA = 1 ,mo_NGG
          xmo_CXE(IVGA) = xmo_VG1(IVGA)
          IF (xmo_CXE(IVGA) .LE. 0.D0 ) THEN
            xmo_CXE(IVGA) = 0.D0
          ENDIF
        ENDDO
*
        DO IVGA = 1 ,mo_NGG
          xmo_CXERBLOC(IVGA) = xmo_VG1(mo_NGG+IVGA)
          IF (xmo_CXERBLOC(IVGA) .LE. 0.D0 ) THEN
            xmo_CXERBLOC(IVGA) = 0.D0
          ENDIF
        ENDDO
*
        xmo_CB = xmo_VG1(2*mo_NGG+1)
*
        xmo_CXEB = xmo_VG1(2*mo_NGG+2)
*
        xmo_VTB = xmo_VG1(2*mo_NGG+3)
*
        IF (xmo_CB .GT. 0.D0 .AND. xmo_VTB .GT. 0.D0) THEN
          xmo_RB = 
     &        (3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTB / xmo_CB)
     &        **(1.D0/3.D0)
        ELSE
          xmo_RB = 0.D0
          xmo_CB = 0.D0 
          xmo_CXEB =  0.D0
          xmo_VTB = 0.D0 
        ENDIF
*
************************************************************************
* Gaz et bulles inter
************************************************************************
*
        DO 60 IVGA = 2*mo_NGG+4, 2*mo_NGG+3+mo_NSEQ_C
          xmo_CXEJ(IVGA-2*mo_NGG-3) = xmo_VG1(IVGA)
          IF (xmo_CXEJ(IVGA-2*mo_NGG-3) .LE. 0.D0 ) THEN
            xmo_CXEJ(IVGA-2*mo_NGG-3) = 0.D0
          ENDIF
*
*
 60     CONTINUE 
*
*       
        xmo_CBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1)
*
        xmo_CXEBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2)
*
        xmo_VTBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3)
*
*
* Calcul du rayon des bulles inter ou de la demi largeur des tunnels 
* selon que l'on se trouve dans l'etat classique (0) ou tunnel (1)
*
        IF (xmo_CBJ .GT. 0.D0 .AND. xmo_VTBJ .GT. 0.D0) THEN
*
            xmo_RBJ = 
     &          ( 3.D0 / 4.D0 / xmo_PIZAHL / 
     &            xmo_ATETBJ * xmo_VTBJ / xmo_CBJ )
     &          **(1.D0/3.D0)
*
        ELSE
          xmo_RBJ = 0.D0
          xmo_CBJ = 0.D0 
          xmo_CXEBJ =  0.D0
          xmo_VTBJ = 0.D0 
        ENDIF
*
*
************************************************************************
* pores intra
************************************************************************
        xmo_CPOR = xmo_VD1(1)
*
        xmo_CXEPOR = xmo_VD1(2)
*
        xmo_VTPOR = xmo_VD1(3)
*
        IF (xmo_CPOR .GT. 0.D0 .AND. xmo_VTPOR .GT. 0.D0) THEN
          xmo_RPOR =  
     &        (3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTPOR / xmo_CPOR)
     &         **(1.D0/3.D0)
        ELSE
          xmo_RPOR = 0.D0
          xmo_CPOR = 0.D0 
          xmo_CXEPOR =  0.D0
          xmo_VTPOR = 0.D0 
        ENDIF
*
*
************************************************************************
* pores inter 
************************************************************************
          xmo_CPORJ = xmo_VD1(4)
          xmo_CXEPORJ = xmo_VD1(5)
          xmo_VTPORJ = xmo_VD1(6)
*
* Attention dans le cas etat tunnel la valeur stockee dans xmo_VD1(5)
* est bidon
*
          IF (xmo_CPORJ .GT. 0.D0 .AND. xmo_VTPORJ .GT. 0.D0) THEN
            xmo_RPORJ =  
     &      ( 3.D0 / 4.D0 / xmo_PIZAHL / xmo_ATETPORJ 
     &        * xmo_VTPORJ / xmo_CPORJ )
     &       **(1.D0/3.D0)
          ELSE
            xmo_RPORJ = 0.D0
            xmo_CPORJ = 0.D0 
            xmo_CXEPORJ =  0.D0
            xmo_VTPORJ = 0.D0 
          ENDIF
*
************************************************************************
************************************************************************
* densit� de dislocations
************************************************************************
        xmo_RHOD = xmo_VD1(7)
*
      RETURN
      END
      SUBROUTINE MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
C     =================
C
C
C     ******************************************************************
C     MOPARL2 : MOgador initialisation de variables plus PARLantes
C               dans l'�tat 'en restructuration' (2)                                    
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogvar.inc'
C
***********************************************************************
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQR en entr�e
* cmogppr.inc : xmo_ATETBJ xmo_ATETPORJ en entr�e
* cmogvar.inc : xmo_EQUADISP en entr�e
************************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOPARL2')
*
************************************************************************
************************************************************************
*  POUR ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
      CALL MOCONS(2,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
*
* Utilisation de variables locales plus parlantes
* et dans le cas o� il y a incoh�rence entre les concentration
* volume total d'un type de cavit�, on met tou � z�ro
*
* zones saines
*
        DO IVGA = 1 ,mo_NGG
          xmo_CXE(IVGA) = xmo_VG1(IVGA)
          IF (xmo_CXE(IVGA) .LE. 0.D0 ) THEN
            xmo_CXE(IVGA) = 0.D0
          ENDIF
          IF (xmo_EQUADISP(1) .EQ. 0.D0) THEN
            xmo_CXE(IVGA) = 0.D0
          ENDIF
        ENDDO
*
        DO IVGA = 1 ,mo_NGG
          xmo_CXERBLOC(IVGA) = xmo_VG1(mo_NGG+IVGA)
          IF (xmo_CXERBLOC(IVGA) .LE. 0.D0 ) THEN
            xmo_CXERBLOC(IVGA) = 0.D0
          ENDIF
          IF (xmo_EQUADISP(2) .EQ. 0.D0) THEN
            xmo_CXERBLOC(IVGA) = 0.D0
          ENDIF
        ENDDO
*
        xmo_CB = xmo_VG1(2*mo_NGG+1)
*
        xmo_CXEB = xmo_VG1(2*mo_NGG+2)
*
        xmo_VTB = xmo_VG1(2*mo_NGG+3)
*
        IF (xmo_CB .GT. 0.D0 .AND. xmo_VTB .GT. 0.D0) THEN
          xmo_RB = 
     &        (3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTB / xmo_CB)
     &        **(1.D0/3.D0)
        ELSE
          xmo_RB = 0.D0
          xmo_CB = 0.D0 
          xmo_CXEB =  0.D0
          xmo_VTB = 0.D0 
        ENDIF
*
        IF (xmo_EQUADISP(2) .EQ. 0.D0) THEN
          xmo_RB = 0.D0
          xmo_CB = 0.D0 
          xmo_CXEB =  0.D0
          xmo_VTB = 0.D0 
        ENDIF
************************************************************************
* Gaz et bulles inter
************************************************************************
*
        DO  IVGA = 1, mo_NSEQ_C
          xmo_CXEJ(IVGA) = xmo_VG1(2*mo_NGG+3+IVGA)
          IF (xmo_CXEJ(IVGA) .LE. 0.D0 ) THEN
            xmo_CXEJ(IVGA) = 0.D0
          ENDIF
          IF (xmo_EQUADISP(3) .EQ. 0.D0) THEN
            xmo_CXEJ(IVGA) = 0.D0
          ENDIF
        ENDDO
*       
        xmo_CBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+1)
*
        xmo_CXEBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+2)
*
        xmo_VTBJ = xmo_VG1(2*mo_NGG+3+mo_NSEQ_C+3)
*
* Calcul du rayon des bulles inter 
*
        IF (xmo_CBJ .GT. 0.D0 .AND. xmo_VTBJ .GT. 0.D0) THEN
*
            xmo_RBJ = 
     &        ( 3.D0 / 4.D0 / xmo_PIZAHL / 
     &          xmo_ATETBJ * xmo_VTBJ / xmo_CBJ )
     &        **(1.D0/3.D0)
*
        ELSE
          xmo_RBJ = 0.D0
          xmo_CBJ = 0.D0 
          xmo_CXEBJ =  0.D0
          xmo_VTBJ = 0.D0 
        ENDIF
*
        IF (xmo_EQUADISP(4) .EQ. 0.D0) THEN
          xmo_RBJ = 0.D0
          xmo_CBJ = 0.D0 
          xmo_CXEBJ =  0.D0
          xmo_VTBJ = 0.D0 
        ENDIF
*
************************************************************************
* gaz dissous dans les zones restructur�es
************************************************************************
*
        mo_NZONE_SAINE = 2*mo_NGG+3+mo_NSEQ_C+3
        DO 60 IVGA = 1, mo_NSEQR
          xmo_CXEHOM(IVGA) = xmo_VG1(mo_NZONE_SAINE+IVGA)
 60     CONTINUE 
*
*
* bulles de rim
*
        xmo_CBRIM = xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+1)
*
        xmo_CXEBRIM = xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+2)
*
        xmo_VTBRIM = xmo_VG1(mo_NZONE_SAINE+mo_NSEQR+3)
*
        IF (xmo_CBRIM .GT. 0.D0 .AND. 
     &      xmo_VTBRIM .GT. 0.D0) THEN
          xmo_RBRIM = 
     &     (3.D0 / 4.D0 / xmo_PIZAHL  * xmo_VTBRIM / xmo_CBRIM)
     &     **(1.D0/3.D0)
        ELSE
          xmo_RBRIM = 0.D0
          xmo_CBRIM = 0.D0 
          xmo_CXEBRIM =  0.D0
          xmo_VTBRIM = 0.D0 
        ENDIF
*
************************************************************************
* pores dans les zones saines intra
************************************************************************
*
        xmo_CPOR = xmo_VD1(1)
*
        xmo_CXEPOR = xmo_VD1(2)
*
        xmo_VTPOR = xmo_VD1(3)
*
        IF (xmo_CPOR .GT. 0.D0 .AND. xmo_VTPOR .GT. 0.D0) THEN
          xmo_RPOR =  
     &        (3.D0 / 4.D0 / xmo_PIZAHL * xmo_VTPOR / xmo_CPOR)
     &         **(1.D0/3.D0)
        ELSE
          xmo_RPOR = 0.D0
          xmo_CPOR = 0.D0 
          xmo_CXEPOR =  0.D0
          xmo_VTPOR = 0.D0 
        ENDIF
*
        IF (xmo_EQUADISP(7) .EQ. 0.D0) THEN
          xmo_RPOR = 0.D0
          xmo_CPOR = 0.D0 
          xmo_CXEPOR =  0.D0
          xmo_VTPOR = 0.D0 
        ENDIF
************************************************************************
* pores inter 
************************************************************************
          xmo_CPORJ = xmo_VD1(4)
          xmo_CXEPORJ = xmo_VD1(5)
          xmo_VTPORJ = xmo_VD1(6)
*
*
          IF (xmo_CPORJ .GT. 0.D0 .AND. xmo_VTPORJ .GT. 0.D0) THEN
            xmo_RPORJ =  
     &      ( 3.D0 / 4.D0 / xmo_PIZAHL / xmo_ATETPORJ 
     &        * xmo_VTPORJ / xmo_CPORJ )
     &       **(1.D0/3.D0)
          ELSE
            xmo_RPORJ = 0.D0
            xmo_CPORJ = 0.D0 
            xmo_CXEPORJ =  0.D0
            xmo_VTPORJ = 0.D0 
          ENDIF
*
        IF (xmo_EQUADISP(8) .EQ. 0.D0) THEN
            xmo_RPORJ = 0.D0
            xmo_CPORJ = 0.D0 
            xmo_CXEPORJ =  0.D0
            xmo_VTPORJ = 0.D0 
        ENDIF
*
************************************************************************
* pores dans les zones restructur�es
************************************************************************
*
        xmo_CPORRIM = xmo_VD1(7)
*
        xmo_CXEPORRIM = xmo_VD1(8)
*
        xmo_VTPORRIM = xmo_VD1(9)
*
        IF (xmo_CPORRIM .GT. 0.D0 .AND. 
     &      xmo_VTPORRIM .GT. 0.D0) THEN
          xmo_RPORRIM =  
     &    (3.D0 / 4.D0 / xmo_PIZAHL  * xmo_VTPORRIM / xmo_CPORRIM)
     &     **(1.D0/3.D0)
        ELSE
          xmo_RPORRIM = 0.D0
          xmo_CPORRIM = 0.D0 
          xmo_CXEPORRIM = 0.D0
          xmo_VTPORRIM = 0.D0 
        ENDIF
*
        IF (xmo_EQUADISP(9) .EQ. 0.D0) THEN
          xmo_RPORRIM = 0.D0
          xmo_CPORRIM = 0.D0 
          xmo_CXEPORRIM = 0.D0
          xmo_VTPORRIM = 0.D0 
        ENDIF
************************************************************************
        xmo_RHOD = xmo_VD1(10)
************************************************************************
*
*
      RETURN
      END
      SUBROUTINE MOPARL3(xmo_VG1,xmo_VD1,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM)
C     =================
C
C
C     ******************************************************************
C     MOPARL3 : MOgador initialisation de variables plus PARLantes
C               dans l'�tat 'restructur�' (3)                                    
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogpnu.inc'
C
***********************************************************************
* cmogphy.inc : xmo_PIZAHL en entr�e
* cmogpnu.inc : mo_NSEQR en entr�e
************************************************************************
*
      DOUBLE PRECISION 
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXEHOM(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOPARL3')
*
************************************************************************
************************************************************************
*  POUR ETAT 'RESTRUCTURE' (3) UNIQUEMENT
************************************************************************
************************************************************************
*
*
* Utilisation de variables locales plus parlantes
* et dans le cas o� il y a incoh�rence entre les concentration
* volume total d'un type de cavit�, on met tou � z�ro
*
* gaz disous dans les zones restructur�es
*
        DO 60 IVGA = 1, mo_NSEQR
          xmo_CXEHOM(IVGA) = xmo_VG1(IVGA)
 60     CONTINUE 
*
*
* bulles de rim
*
        xmo_CBRIM = xmo_VG1(mo_NSEQR+1)
*
        xmo_CXEBRIM = xmo_VG1(mo_NSEQR+2)
*
        xmo_VTBRIM = xmo_VG1(mo_NSEQR+3)
*
        IF (xmo_CBRIM .GT. 0.D0 .AND. 
     &      xmo_VTBRIM .GT. 0.D0) THEN
          xmo_RBRIM = 
     &     (3.D0 / 4.D0 / xmo_PIZAHL  * xmo_VTBRIM / xmo_CBRIM)
     &     **(1.D0/3.D0)
        ELSE
          xmo_RBRIM = 0.D0
          xmo_CBRIM = 0.D0 
          xmo_CXEBRIM =  0.D0
          xmo_VTBRIM = 0.D0 
        ENDIF
*
*
* pores dans les zones restructur�es
*
        xmo_CPORRIM = xmo_VD1(1)
*
        xmo_CXEPORRIM = xmo_VD1(2)
*
        xmo_VTPORRIM = xmo_VD1(3)
*
        IF (xmo_CPORRIM .GT. 0.D0 .AND. 
     &      xmo_VTPORRIM .GT. 0.D0) THEN
          xmo_RPORRIM =  
     &    (3.D0 / 4.D0 / xmo_PIZAHL  * xmo_VTPORRIM / xmo_CPORRIM)
     &     **(1.D0/3.D0)
        ELSE
          xmo_RPORRIM = 0.D0
          xmo_CPORRIM = 0.D0 
          xmo_CXEPORRIM = 0.D0
          xmo_VTPORRIM = 0.D0 
        ENDIF
*
      RETURN
      END
*
      SUBROUTINE MOPINT (xmo_VAT,xmo_PINT,mo_PB)
C     ==================
C
C     ******************************************************************
C     MOPINT : MOgador Pression INTerne des bulles ou des tunnels 
C              xmo_VAT : Volume disponible par atome
C              xmo_PINT : Pression dans la bulle (Pa) ou dans le tunnel                                  
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogpas.inc : xmo_DBUCH en entr�e
* cmogent.inc : xmo_TK en entr�e
* cmogphy.inc : xmo_BVDW, xmo_AVDW, xmo_PIZAHL, xmo_BOLTZ  en entr�e
* cmogppr.inc : xmo_XLSUP xmo_XLINF
************************************************************************
C
C xmo_VAT : Volume disponible par atome de gaz (de fission ou initial) 
C           dans la bulle ou les tunnels ou les pores
C xmo_PINT : pression interne (Pa)
C mo_PB vaut 1 en cas d'imposibilit� du calcul de la pression
C          et 0 en cas de calcul normal
C xmo_VATSB : Volume disponible par atome / Covolume xmo_BVDW de 
C             l'�quation de Van der Waals
C
C
************************************************************************
C
      CALL MOMESS('DANS MOPINT')
C
      mo_PB = 0
C
      CALL MOMESV('dans MOPINT au debut xmo_VAT =', xmo_VAT)
C
      xmo_VATSB =  xmo_VAT / xmo_BVDW 
C
C
*
*

      IF (xmo_VATSB .LE. xmo_XLINF) THEN
        xmo_XPOND = 0.D0
      ELSEIF (xmo_VATSB .GE. xmo_XLSUP) THEN
        xmo_XPOND = 1.D0
      ELSE
        xmo_XPOND = (xmo_VATSB-xmo_XLINF) /
     &                        (xmo_XLSUP-xmo_XLINF)
        xmo_XPOND = 0.5D0 * 
     &           ( cos(xmo_PIZAHL*(xmo_XPOND + 1.D0)) + 1.D0 )
      ENDIF
C
C Pour l'�quation des sph�res dures avec potentiel modifi� de BUCKINGHAM
C
      YY =  xmo_PIZAHL * xmo_DBUCH**3.D0 / 6.D0  / xmo_VAT
C
      IF ( (1.D0-YY) .LE. 0.D0) THEN
        mo_PB = 1
        CALL MOMESS('PB dans sphere dure')
        GOTO 100
      ENDIF
C        
      xmo_PSD =  xmo_BOLTZ * xmo_TK / xmo_VAT *
     $        (1.D0+YY+YY**2.D0-YY**3.D0) / (1.D0 - YY)**3.D0
C
* Pour xmo_VAT en dessous de xmo_BVDW, xmo_PVDW ne sera pas utilise 
* si xmo_LINF eststrictement superieur a 1
*
      IF ( ( xmo_VAT - xmo_BVDW ) .EQ. 0.D0 ) THEN
        xmo_PVDW = 0.D0
      ELSE
        xmo_PVDW = xmo_TK * xmo_BOLTZ / ( xmo_VAT - xmo_BVDW )
     $              - xmo_AVDW / xmo_VAT**2.D0
      ENDIF
C
C
      xmo_PINT = xmo_XPOND * xmo_PVDW + (1.D0 - xmo_XPOND) * xmo_PSD
C
************************************************
       CALL MOMESV('xmo_XPOND = ',xmo_XPOND)
       CALL MOMESV('xmo_PINT = ',xmo_PINT)
************************************************
C
 100  CONTINUE
      RETURN
      END
      SUBROUTINE MOPROB(xmo_FR,xmo_PROBFR)
C     =================
C
C
C     ******************************************************************
C     MOPROB : MOgador calcul de la PROBabilit� pour qu'un atome de gaz 
C              soit r�implant� de la zone restructur�e vers la zone saine                                                        
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogdon.inc'
C
***********************************************************************
* cmogppr.inc : xmo_DREM en entr�e
* cmogdon.inc : xmo_AGG en entr�e
************************************************************************
* xmo_PROBFR : proba d'�tre remis en solution dans la zone saine
* xmo_FR : fraction volumique restructur�e ()
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOPROB')
*
*
* Calcul de la proba d'�tre remis en solution dans une zone saine
*
*
      IF ( xmo_FR .GT. 0.D0 .AND. xmo_FR .LT. 1) THEN
        DIST = 2.D0 * xmo_AGG * ( 1 - (1-xmo_FR)**(1.D0/3.D0) )
        IF (DIST .GT. xmo_DREM) THEN
          xmo_PROBFR = xmo_DREM / 2.D0 / DIST
        ELSE
          xmo_PROBFR = 1.D0 - DIST / 2.D0 / xmo_DREM
        ENDIF
      ELSEIF ( xmo_FR .LE. 0.D0 ) THEN
          xmo_PROBFR = 1.D0
      ELSE
          xmo_PROBFR = 0.D0 
      ENDIF
*         
      RETURN
      END
*
*
      INTEGER FUNCTION MORANG(L0,IGROB0,I0,IHETER0,NPROC)
*     =================
*
*     ******************************************************************
*     MORANG : MOgador Calcul de RANG du calcul (pour la parallelisation)
*              c'est a dire du numero du processeur sur lequel le calcul
*              doit etre fait
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogmai.inc'
************************************************************************      
* cmogmai.inc : NBNOZO NB_ZONE en entr�e
************************************************************************
* NPROC : nombre total de processeurs
* Dans meteor, le premier noeud de la zone IGROB+1 est confondu avec le 
* dernier noeud de la zone IGROB. Il est inutile de faire le calcul des 
* gaz de fission deux fois. On parcourt donc les zones de I= 1 � NBNOZO(L,1)
* pour la zone 1, et de 2 �  NBNOZO(L,IGROB) pour les zones IGROB sup ou 
* egal a 2.
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
*
      mo_SOMME = 0.D0
*
      IF ( IGROB0 .GE. 2 ) THEN
        mo_SOMME = mo_NBNOZO(L0,1) * mo_NAMAMX      
        DO IGROB = 2 , IGROB0 - 1
          mo_SOMME = mo_SOMME + 
     &                 ( mo_NBNOZO(L0,IGROB)-1 ) * mo_NAMAMX
        ENDDO
      ENDIF
*
      IF ( IGROB0 .EQ. 1 ) THEN
        MORANG = (I0-1) * mo_NAMAMX + IHETER0 - 1
      ELSE
        MORANG = mo_SOMME + (I0-2) * mo_NAMAMX +
     &  	 IHETER0 - 1      
      ENDIF
*
      MORANG = MOD(MORANG,NPROC)
*
      RETURN
      END
*
*
      SUBROUTINE MORDCOAL(xmo_DMIN,xmo_DMAX1,xmo_DMAX2,
     &                    xmo_TKMIN,xmo_TKMAX,
     &                    xmo_STFMIN,xmo_STFMAX1,xmo_STFMAX2)
*     =================
*
*     ******************************************************************
*     MORDCOAL : MOgador Read des param�tres des distances de 
*                coalescence g�om�trique
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
*
      DO 10 J = 1,9
           CALL REDLEC(4)
           IF(TEXT.EQ.'DISTANCE_MIN')THEN
             CALL REDLEC(2)
             xmo_DMIN = DFLOT
           ELSEIF(TEXT.EQ.'TEMPERATURE_K_MIN') THEN
             CALL REDLEC(2)
             xmo_TKMIN = DFLOT
           ELSEIF(TEXT.EQ.'DISTANCE_MAX1') THEN
             CALL REDLEC(2)
             xmo_DMAX1 = DFLOT
           ELSEIF(TEXT.EQ.'DISTANCE_MAX2') THEN
             CALL REDLEC(2)
             xmo_DMAX2 = DFLOT
           ELSEIF(TEXT.EQ.'TEMPERATURE_K_MAX') THEN
             CALL REDLEC(2)
             xmo_TKMAX = DFLOT
           ELSEIF(TEXT.EQ.'SOMTOFIS_MIN') THEN
             CALL REDLEC(2)
             xmo_STFMIN = DFLOT
           ELSEIF(TEXT.EQ.'SOMTOFIS_MAX1') THEN
             CALL REDLEC(2)
             xmo_STFMAX1 = DFLOT
           ELSEIF(TEXT.EQ.'SOMTOFIS_MAX2') THEN
             CALL REDLEC(2)
             xmo_STFMAX2 = DFLOT
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'DISTANCE_DE_COALESCENCE_GEOMETRIQUE'
             STOP
           ENDIF
10    CONTINUE
*
*     7--0---------0---------0---------0---------0---------0---------0--
 9999 WRITE (NEX,900)
*  
 900  FORMAT (//,40X,
     &    'FIN DES PARAMETRES DU MODELE DE COALESCENCE GEOMETRIQUE',/)
*
*
*
*
      RETURN
      END
*
      SUBROUTINE MOREAD
*     =================
C
*     ******************************************************************
*     MOREAD : MOgadodor READ
*     ******************************************************************
C
C
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'credtxt.inc' 
************************************************************************
*   cmoginf.inc : mo_DEBUG mo_NLEC et mo_NSOR en entr�e
*   credcar.inc : TEXT en entr�e  LIGNE en sortie
*   credtxt.inc : NIN NECHO NEX  en sortie 
************************************************************************
C
      NIN   = mo_NLEC
      NEX   = mo_NSOR
      NECHO = mo_NSOR
      LIGNE = ' '
C
C
C
CDEBUG      WRITE(mo_DEBUG,*) 'DANS MOREAD'
C
C
      CALL REDLEC(4)
      IF(TEXT.EQ.'GEOMETRIE_AXIALE')THEN
        CALL MOREAX
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST GEOMETRIE_AXIALE'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF(TEXT.EQ.'GEOMETRIE_RADIALE')THEN
        CALL MOREMA
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST GEOMETRIE_RADIALE'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF(TEXT.EQ.'DONNEES')THEN
        CALL MOREDO
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST DONNEES'
        STOP
      ENDIF
C
C
      CALL REDLEC(4)
      IF(TEXT.EQ.'GAZ_DE_FISSION_MARGARET_V3.1')THEN
        CALL MOREPP
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST GAZ_DE_FISSION_MARGARET_V3.1'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF (TEXT.EQ.'PARAMETRES_DE_RESOLUTION_'//
     +            'NUMERIQUE_MARGARET_V3.1')  THEN
        CALL MOREPN
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST'//
     + ' PARAMETRES_DE_RESOLUTION_NUMERIQUE_MARGARET_V3.1'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF (TEXT.EQ.'PARAMETRES_DE_TESTS')  THEN
        CALL MOREPT
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST PARAMETRES_DE_TESTS'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF (TEXT.EQ.'PARAMETRES_D_IMPRESSION')  THEN
        CALL MOREPI
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST PARAMETRES_D_IMPRESSION'
        STOP
      ENDIF
C
      CALL REDLEC(4)
      IF(TEXT.EQ.'ENTREES')THEN
C       lecture des titres de colonnes pour info
        DO 10 J = 1,18
           CALL REDLEC(4)
10      CONTINUE
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     + ' LE MOT CLE ATTENDU EST ENTREES'
        STOP
      ENDIF
C
      RETURN
      END
*
*
      SUBROUTINE MOREAX
*     =================
*
*     ******************************************************************
*     MOREAX : MOgador REad du maillage AXial
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmogmai.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'LONGUEUR_COLONNE_COMBUSTIBLE')THEN
        CALL REDLEC(2)
        HCOMB  = DFLOT
          IF (HCOMB .LE. 0.D0) THEN
            WRITE (NEX,*) 'Hauteur de colonne negative ou nulle'
            STOP
          ENDIF
      ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' ON ATTENDAIT : ',
     +         ' LONGUEUR_COLONNE_COMBUSTIBLE'
             STOP
      ENDIF
*
*
      IFIN = 3
      DO 30 J = 1,IFIN
        CALL REDLEC(4)
        IF(TEXT.EQ.'TRANCHE_AXIALE')THEN
          CALL REDLEC(1)
          mo_NB_TRAX = NFIXE
          IF (mo_NB_TRAX.GT.mo_M31MAX .OR. mo_NB_TRAX.LE.0) THEN
            WRITE (NEX,*) 'Mauvais nombre de tranches'
            STOP
          ENDIF
        ELSEIF(TEXT.EQ.'HAUTEUR_TRANCHE')THEN
          CALL REDLEC(4)
          IF(TEXT.EQ.'EQUIDISTANTE') THEN
             DO M = 1,mo_NB_TRAX
                 xmo_HHREF(M) = HCOMB / mo_NB_TRAX
             ENDDO
          ELSEIF(TEXT.EQ.'FOURNI')THEN
             SOM = 0.D0
             DO  M = 1,mo_NB_TRAX-1
               CALL REDLEC(2)
               xmo_HHREF(M) = DFLOT
               SOM = SOM + xmo_HHREF(M)
             ENDDO
             xmo_HHREF(mo_NB_TRAX) = HCOMB - SOM
             IF ( xmo_HHREF(mo_NB_TRAX) .LE. 0.D0) THEN
               WRITE (NEX,*) 'Pb dans la definition des tranches'
               STOP
             ENDIF
          ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' VERIFIER VOS MOT CLE ASSOCIES A ',
     +         ' HAUTEUR_TRANCHE'
             STOP
          ENDIF
        ELSEIF(TEXT.EQ.'FIN')THEN
          IF(J .LT. IFIN) THEN
            WRITE(NEX,*)'IL MANQUE UNE OU DES VALEURS DANS LA ',
     +                 'RUBRIQUE GEOMETRIE_AXIALE'
            WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' VERIFIER VOS MOT CLE RUBRIQUE GEOMETRIE_AXIALE '
            STOP
          ENDIF
        ELSE
          WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' VERIFIER VOS MOT CLE  ',
     +         ' RUBRIQUE GEOMETRIE_AXIALE'
          STOP
        ENDIF
30    CONTINUE
*
*
      RETURN
      END
C        1         2         3         4         5         6         7
      SUBROUTINE MORECH_VF(xmo_R,NDINT,xmo_RREM,NIP)
C
C **********************************************************************
C MORECH : MOgadodor RECHerche du numero de la maille
C          dans lequel se trouve le rayon adimensionn� xmo_RREM
C ********************************************************************** 
C
C
C **********************************************************************
C PARAMETRES DU SOUS-PROGRAMME
C **********************************************************************
C
C en entr�e :
C xmo_R : tableau des rayons relatifs correspondants aux noeuds 
C         du maillage de la sph�re consid�r�e (limites de couronnes)
C NDINT : (en volumes finis) nombre de noeuds de la sph�re consid�r�e
C         Y compris le noeud du bord de la sph�re = nombre de couronnes
C         dans la sph�re consid�r�e
C RREM : Rayon relatif consid�r�
C
C en sortie :
C NIP : numero de la couronne (limit�e par deux noeuds)
C       dans lequel se trouve le rayon adimensionn� xmo_RREM
C 
C **********************************************************************
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
      PARAMETER (NDINTMX = mo_NNSPHMX)
C
      DOUBLE PRECISION xmo_R(NDINTMX+1)
C
************************************************************************
* cmoginf : mo_DEBUG en entr�e
************************************************************************
      CALL MOMESS('DANS MORECH_VF')
C
************************************************************************
C     Recherche du num�ro NIP de la maille dans la quelle se trouve xmo_RREM
************************************************************************
      IF(xmo_RREM.LT.0.D0) THEN
        WRITE(mo_DEBUG,*) ' MORECH_VF : ERREUR xmo_RREM = ',xmo_RREM
        STOP
      ENDIF
      DO 150 I = NDINT-1, 1, -1
        IF ( xmo_R(I) .LE. xmo_RREM ) THEN
          NIP = I + 1
          RETURN
        ENDIF
 150  CONTINUE
      NIP = 1
C
************************************************************************
      RETURN
      END
      SUBROUTINE MORECHFR(L,mo_IGROB_RES,mo_I_RES,I_PAS_DE_POINT)
C     =================
C
C
C     ******************************************************************
C     MORECHFR : MOgador RECHerche du premier point dont la FRaction
C                restructur�e est sup�rieure � 10% dans au moins une des
C                phases                         
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogppr.inc'
C
***********************************************************************
* cmogvar.inc : mo_IETCOM xmo_VECG xmo_VECD en entr�e
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE xmo_RPAST en entree 
* cmogppr.inc : xmo_RHOLIM xmo_RHODL xmo_MARGRHO_FR  
*               xmo_FRLIM_CALCUL_GONFMOY en entree
************************************************************************
* arguments 
* en entr�e : L
* en sortie : mo_IGROB_RES   mo_I_RES
************************************************************************
*
* variables locales
*
*
      DOUBLE PRECISION  xmo_CXE(mo_NNSPHMX),
     &                  xmo_CXERBLOC(mo_NNSPHMX),
     &                  xmo_CXEJ(mo_NNSPHMX),
     &                  xmo_CXEHOM(mo_NNSPHMX),
     &                  xmo_VG1(mo_NVGAMX),
     &                  xmo_VD1(mo_NVDEMX)
*
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MORECHFR')
*
***********************************************************************
*
      DO 40 IGROB = 1, mo_NB_ZONE(L)
          IF (IGROB .EQ. 1) THEN
            IDEB = 1
          ELSE
            IDEB = 2
          ENDIF	
          DO 40 I = IDEB, mo_NBNOZO(L,IGROB)
            DO 40 IHETER = 1, mo_NAMAMX
              mo_IET = mo_IETCOM(L,IGROB,I,IHETER,1)
              IF (mo_IET .EQ. 2) THEN
*             +++++++++++++++++++++++
*
************************************************************************
* POUR CALCUL DE xmo_FR
************************************************************************
      DO IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VECG(L,IGROB,I,IHETER,IVGA,1)
      ENDDO
*
      DO IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VECD(L,IGROB,I,IHETER,IVDE,1)
      ENDDO
*
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
************************************************************************
* Calcul de xmo_FR
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM + xmo_RHODL - xmo_MARGRHO_FR pour les calcul de xmo_FR  
* Sinon, on a des divisions par zero
*
          xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
          xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
************************************************************************
*
                IF (xmo_FR .GT. xmo_FRLIM_CALCUL_GONFMOY) THEN
                  GOTO 300
                ENDIF
*
              ELSEIF (mo_IET .EQ. 3) THEN
*             +++++
                GOTO 300
              ENDIF
*             +++++
 40   CONTINUE
*     
      IGROB = mo_NB_ZONE(L)
      I = mo_NBNOZO(L,IGROB) + 1
CDEBUG
CDEBUG      WRITE(20,*) 'Dans morechfr'
CDEBUG      WRITE(20,*) 'IGROB = ',IGROB
CDEBUG      WRITE(20,*) 'I = ',I
CDEBUG
*
 300  CONTINUE
* Le point a retenir est le point pr�c�dent
      IF (I .EQ. 1) THEN
        I_PAS_DE_POINT = 1
        mo_IGROB_RES = 1
        mo_I_RES = 1 
      ELSEIF (I .EQ. 2) THEN
        I_PAS_DE_POINT = 0
        IF (IGROB .EQ. 1) THEN
          mo_I_RES = I - 1
          mo_IGROB_RES = IGROB
        ELSE        
          mo_IGROB_RES = IGROB - 1
          mo_I_RES =  mo_NBNOZO(L,mo_IGROB_RES)
        ENDIF
      ELSEIF (I .GT. 2) THEN
        I_PAS_DE_POINT = 0
        mo_I_RES = I - 1
        mo_IGROB_RES = IGROB
      ENDIF
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREDO
*     =================
*
*     ******************************************************************
*     MOREPP : MOgador REad des DOnn�es
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE mo_NBNOZO en entree      
* cmogdon.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
* Initialisation des tableaux de donnees a zero
      DO L = 1, mo_NB_TRAX
        xmo_PREFRI(L) =  0.D0
        xmo_TEMFRI(L) =  0.D0
        DO IGROB = 1, mo_NB_ZONE(L)
          DO I = 1, mo_NBNOZO(L,IGROB)
            DO IHETER = 1, mo_NAMAMX
              xmo_AGG3(L,IGROB,I,IHETER) = 0.D0
              xmo_VTPORI3(L,IGROB,I,IHETER) = 0.D0
              xmo_VTPORJI3(L,IGROB,I,IHETER) = 0.D0
              xmo_CPORI3(L,IGROB,I,IHETER) = 0.D0
              xmo_CPORJI3(L,IGROB,I,IHETER) = 0.D0
              xmo_MAVOLI3(L,IGROB,I,IHETER) = 0.D0
              mo_MATER3(L,IGROB,I,IHETER) = 0
            ENDDO
          ENDDO
        ENDDO
      ENDDO
*
* Lecture des donnees
* En fonctionnement en autonome mo_NAMAMX = 1
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'PAR_TRANCHE')THEN
        CALL REDLEC(4)
          IF(TEXT.EQ.'NON')THEN
            AEAX1 = 0
            MFIN  = 1
          ELSEIF(TEXT.EQ.'OUI')THEN
            AEAX1 = 1
            MFIN = mo_NB_TRAX
          ELSE
            WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' VERIFIER VOS MOT CLE ASSOCIES A ',
     +         ' PAR_TRANCHE RUBRIQUE DONNEES'
            STOP
          ENDIF
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'LE MOT CLE ATTENDU EST PAR_TRANCHE',
     +         ' DANS RUBRIQUE DONNEES'
        STOP
      ENDIF
*
*
*     ==================================================================
*     ==================================================================
      DO 90 L = 1, MFIN
*     ==================================================================
*     ==================================================================
        IF(AEAX1 .EQ. 1) THEN
          CALL REDLEC(4)
          CALL REDLEC(1)
        ENDIF
*
      DO 10 J = 1,9
           CALL REDLEC(4)
           IF(TEXT.EQ.'RAYON_GRAIN_DE_FABRICATION')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_AGG = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_AGG3(L,IGROB,I,IHETER) = xmo_AGG
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_AGG3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'POROSITE_DE_FABRICATION_INTRA')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_VTPORI = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_VTPORI3(L,IGROB,I,IHETER) = xmo_VTPORI
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_VTPORI3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'POROSITE_DE_FABRICATION_INTER')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_VTPORJI = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_VTPORJI3(L,IGROB,I,IHETER) =
     &                                               xmo_VTPORJI
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_VTPORJI3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'CONCENTRATION_PORES_INTRA')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_CPORI = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_CPORI3(L,IGROB,I,IHETER) = xmo_CPORI
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_CPORI3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'CONCENTRATION_PORES_INTER')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_CPORJI = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_CPORJI3(L,IGROB,I,IHETER) = xmo_CPORJI
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_CPORJI3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'MASSE_VOLUMIQUE_COMBUSTIBLE_FROID')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(2)
                  xmo_MAVOLI = DFLOT
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          xmo_MAVOLI3(L,IGROB,I,IHETER) = xmo_MAVOLI
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(2)
                          xmo_MAVOLI3(L,IGROB,I,IHETER) = DFLOT
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'MATERIAU_UO2_UPUO2')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'MOYEN')THEN
                  CALL REDLEC(1)
                  mo_MATER = NFIXE
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          mo_MATER3(L,IGROB,I,IHETER) = mo_MATER
                        ENDDO
                      ENDDO
                    ENDDO
                ELSEIF(TEXT.EQ.'PROFIL_RADIAL')THEN
                    DO IGROB = 1, mo_NB_ZONE(L)
                      DO I = 1, mo_NBNOZO(L,IGROB)
                        DO IHETER = 1, mo_NAMAMX
                          CALL REDLEC(1)
                          mo_MATER3(L,IGROB,I,IHETER) = NFIXE
                        ENDDO
                      ENDDO
                    ENDDO
                ELSE             
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT MOYEN ou PROFIL_RADIAL '
                  STOP
                ENDIF
*     ==================================================================
           ELSEIF(TEXT.EQ.'PRESSION_DE_FRITTAGE')THEN
                CALL REDLEC(2)
                xmo_PREFRI(L) = DFLOT
*     ==================================================================
           ELSEIF(TEXT.EQ.'TEMPERATURE_DE_FRITTAGE')THEN
                CALL REDLEC(2)
                xmo_TEMFRI(L) = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'DONNEES'
             STOP
           ENDIF
10    CONTINUE
*
*     ==================================================================
*     ==================================================================
 90   CONTINUE
*     ==================================================================
*     ==================================================================
*
      CALL REDLEC(4)
      IF(TEXT.NE.'FIN') THEN
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'DONNEES ON ATTENDAIT FIN'
             STOP
       ENDIF
*
*
      IF ( AEAX1 .EQ. 0  .AND. mo_NB_TRAX .GT. 1 ) THEN
        DO  L = 2, mo_NB_TRAX
          DO IGROB = 1, mo_NB_ZONE(L)
            DO I = 1, mo_NBNOZO(L,IGROB)
              DO IHETER = 1, mo_NAMAMX
              xmo_AGG3(L,IGROB,I,IHETER) = 
     &            xmo_AGG3(1,IGROB,I,IHETER)
              xmo_VTPORI3(L,IGROB,I,IHETER) = 
     &            xmo_VTPORI3(1,IGROB,I,IHETER)
              xmo_VTPORJI3(L,IGROB,I,IHETER) = 
     &            xmo_VTPORJI3(1,IGROB,I,IHETER)
              xmo_CPORI3(L,IGROB,I,IHETER) = 
     &            xmo_CPORI3(1,IGROB,I,IHETER)
              xmo_CPORJI3(L,IGROB,I,IHETER) = 
     &            xmo_CPORJI3(1,IGROB,I,IHETER)
              xmo_ATPOR3(L,IGROB,I,IHETER) = 
     &            xmo_ATPOR3(1,IGROB,I,IHETER)
              xmo_ATPORJ3(L,IGROB,I,IHETER) = 
     &            xmo_ATPORJ3(1,IGROB,I,IHETER)
              xmo_CXEPORI3(L,IGROB,I,IHETER) = 
     &            xmo_CXEPORI3(1,IGROB,I,IHETER)
              xmo_CXEPORJI3(L,IGROB,I,IHETER) = 
     &            xmo_CXEPORJI3(1,IGROB,I,IHETER)
              xmo_MAVOLI3(L,IGROB,I,IHETER) = 
     &            xmo_MAVOLI3(1,IGROB,I,IHETER)
              mo_MATER3(L,IGROB,I,IHETER) = 
     &            mo_MATER3(1,IGROB,I,IHETER)
              xmo_PREFRI(L) = xmo_PREFRI(1)
              xmo_TEMFRI(L) = xmo_TEMFRI(1)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
      ENDIF
*
*     ==================================================================
      WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DES DONNEES',/)
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREEN(L,IGROB,I,mo_DERNIER_NOEUD)
*     =============================================
*
*     ******************************************************************
*     MOREPP : MOgador REad des ENtr�es
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'crednbr.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogppr.inc'   
************************************************************************      
* cmogent.inc : toutes en sorties
* crednbr.inc : DFLOT NFIXE en entr�e
* cmoginf.inc : mo_DEBUG mo_NLEC mo_NSOR
* cmogppr.inc : mo_ASE_IMPOSE xmo_ASE_CENTRE xmo_ASE_BORD en entree
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
CDEBUG      WRITE(mo_DEBUG,*) 'DANS MOREEN'
*
* choix du type de lecture
* 1 : � l'aide de redlec
* 2 : par format
*
      mo_TYPE_LECTURE = 2
*
      IF (mo_TYPE_LECTURE .EQ. 1) THEN
*     ********************************
      CALL REDLEC(1)
      L_LU = NFIXE
      CALL REDLEC(1)
      IGROB_LU = NFIXE
      CALL REDLEC(1)
      I_LU = NFIXE
*
      IF (IGROB .NE. IGROB_LU  .OR. 
     &    I .NE. I_LU) THEN
        WRITE(mo_DEBUG,*) 'Probleme de lecture de l historique'
        STOP
      ENDIF 
*             
      CALL REDLEC(2)
      xmo_DTS = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) ' xmo_DTS = ',xmo_DTS
      CALL REDLEC(2)
      xmo_TOFISS = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) ' xmo_TOFISS = ',xmo_TOFISS
      CALL REDLEC(2)
      xmo_TK = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) ' xmo_TK = ',xmo_TK
      CALL REDLEC(2)
      xmo_GRADTK = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_GRADTK = ',xmo_GRADTK 
      CALL REDLEC(2)
      xmo_PHYDR = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_PHYDR = ',xmo_PHYDR
      CALL REDLEC(2)
      xmo_ASE = DFLOT 
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_ASE = ',xmo_ASE
      CALL REDLEC(2)
      xmo_RXEKR = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_RXEKR = ',xmo_RXEKR
      CALL REDLEC(2)
      xmo_PPLEN = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_PPLEN = ',xmo_PPLEN
      CALL REDLEC(2)
      xmo_GAMXE = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_GAMXE = ',xmo_GAMXE
      CALL REDLEC(2)
      xmo_EFISS = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_EFISS = ',xmo_EFISS
      CALL REDLEC(2)
      xmo_CXEJLIM = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_CXEJLIM = ',xmo_CXEJLIM
      CALL REDLEC(2)
      xmo_VTPORTOT_LU = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_VTPORTOT_LU  = ',xmo_VTPORTOT_LU
      CALL REDLEC(2)
      xmo_CPOR_LU = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_CPOR_LU  = ',xmo_CPOR_LU 
      CALL REDLEC(2)
      xmo_VTPOR_LU = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_VTPOR_LU  = ',xmo_VTPOR_LU
      CALL REDLEC(2)
      xmo_CPORJ_LU = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_CPORJ_LU  = ',xmo_CPORJ_LU
      CALL REDLEC(2)
      xmo_VTPORJ_LU = DFLOT
CDEBUG      WRITE(mo_DEBUG,*) 'xmo_VTPORJ_LU  = ',xmo_VTPORJ_LU
      ELSEIF (mo_TYPE_LECTURE .EQ. 2) THEN
*     ************************************
        READ(mo_NLEC,5000) 
     &                     L_LU, IGROB_LU, I_LU
        IF (IGROB .NE. IGROB_LU  .OR. 
     &    I .NE. I_LU) THEN
          WRITE(mo_DEBUG,*) 'Probleme de lecture de l historique'
          STOP
        ENDIF 
*           
        READ(mo_NLEC,6000) 
     &       xmo_DTS, xmo_TOFISS, xmo_TK, xmo_GRADTK 
        READ(mo_NLEC,6000) 
     &       xmo_PHYDR, xmo_ASE, xmo_RXEKR, xmo_PPLEN 
        READ(mo_NLEC,6000) 
     &       xmo_GAMXE, xmo_EFISS, xmo_CXEJLIM, xmo_VTPORTOT_LU  
        READ(mo_NLEC,6000) 
     &       xmo_CPOR_LU, xmo_VTPOR_LU, xmo_CPORJ_LU, xmo_VTPORJ_LU
*
CDEBUG
CDEBUG        WRITE(mo_DEBUG,5000) L_LU, IGROB_LU, I_LU
CDEBUG        WRITE(mo_DEBUG,6000) 
CDEBUG     &       xmo_DTS, xmo_TOFISS, xmo_TK, xmo_GRADTK 
CDEBUG        WRITE(mo_DEBUG,6000) 
CDEBUG     &       xmo_PHYDR, xmo_ASE, xmo_RXEKR, xmo_PPLEN 
CDEBUG        WRITE(mo_DEBUG,6000) 
CDEBUG     &       xmo_GAMXE, xmo_EFISS, xmo_CXEJLIM, xmo_VTPORTOT_LU  
CDEBUG        WRITE(mo_DEBUG,6000) 
CDEBUG     &       xmo_CPOR_LU, xmo_VTPOR_LU, xmo_CPORJ_LU, xmo_VTPORJ_LU
CDEBUG
*     ================================================================== 
*
*
 4000 FORMAT (1X,'PAS_DE_TEMPS  TEMPS_FINAL  ',E25.16)
 5000 FORMAT (25X,3I8)
 6000 FORMAT (25X,4E25.16)
*
      ENDIF
*     *****
*
* On peut imposer ici la valeur de xmo_ASE et elle vient �craser 
* la valeur lue
      IF (mo_ASE_IMPOSE .EQ. 1) THEN
        IF (mo_DERNIER_NOEUD .EQ. 0) THEN
          xmo_ASE = xmo_ASE_CENTRE
        ELSE
          xmo_ASE = xmo_ASE_BORD
        ENDIF
      ENDIF
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREMA
*     =================
*
*     ******************************************************************
*     MOREMA : MOgador REad du MAillage pastille
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmogmai.inc : mo_NB_TRAX en entree toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      MFIN  = 1
      CALL REDLEC(4)
      IF(TEXT.EQ.'PAR_TRANCHE')THEN
        CALL REDLEC(4)
          IF(TEXT.EQ.'NON')THEN
            AEAX1 = 0
          ELSEIF(TEXT.EQ.'OUI')THEN
            AEAX1 = 1
            MFIN = mo_NB_TRAX
          ELSE
            WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' VERIFIER VOS MOT CLE ASSOCIES A ',
     +         ' PAR_TRANCHE RUBRIQUE GEOMETRIE_RADIALE'
            STOP
          ENDIF
      ELSE
        WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'LE MOT CLE ATTENDU EST PAR_TRANCHE',
     +         ' DANS RUBRIQUE GEOMETRIE_RADIALE'
        STOP
      ENDIF
*
*
*     ==================================================================
*     ==================================================================
      DO 90 L = 1, MFIN
*     ==================================================================
*     ==================================================================
        IF(AEAX1 .EQ. 1) THEN
          CALL REDLEC(4)
          CALL REDLEC(1)
        ENDIF
*
        CALL REDLEC(4)
        IF(TEXT.EQ.'EVIDEMENT')THEN
          CALL REDLEC(2)
          xmo_PRODIS(L)  = DFLOT
        ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' ON ATTENDAIT EVIDEMENT '
             STOP
        ENDIF
*
        CALL REDLEC(4)
        IF(TEXT.EQ.'RAYON_TROU')THEN
          CALL REDLEC(2)
          RPAST_TROU  = DFLOT
        ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' ON ATTENDAIT RAYON_TROU '
             STOP
        ENDIF
*
        CALL REDLEC(4)
        IF(TEXT.EQ.'RAYON_PASTILLE')THEN
            CALL REDLEC(2)
            RPAST_EXT = DFLOT
        ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       'ON ATTENDAIT  RAYON_PASTILLE '
             STOP
        ENDIF
*
        CALL REDLEC(4)
        IF(TEXT.EQ.'NOMBRE_ZONE_RADIALE_PASTILLE')THEN
          CALL REDLEC(1)
          mo_NB_ZONE(L) = NFIXE
        ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       'ON ATTENDAIT  NOMBRE_TOTAL_ZONE_RADIALE '
             STOP
        ENDIF
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'NOMBRE_NOEUD_PAR_ZONE')THEN
          DO IGROB = 1,mo_NB_ZONE(L)
            CALL REDLEC(1)
            mo_NBNOZO(L,IGROB) = NFIXE
          ENDDO
      ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       'ON ATTENDAIT NOMBRE_NOEUD_PAR_ZONE   '
             STOP
      ENDIF
*
      xmo_RPAST(L,1,1) = RPAST_TROU
      xmo_RPAST(L,mo_NB_ZONE(L),mo_NBNOZO(L,mo_NB_ZONE(L))) = 
     +         RPAST_EXT
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'TYPE_MAILLAGE_ZONE')THEN
          CALL REDLEC(4)
          IF(TEXT.EQ.'EQUIDISTANT')THEN
              DIST = (RPAST_EXT - RPAST_TROU) / mo_NB_ZONE(L)
              xmo_RPAST(L,1,mo_NBNOZO(L,1)) = 
     &                  xmo_RPAST(L,1,1) + DIST
              DO IGROB = 2, mo_NB_ZONE(L)-1
                xmo_RPAST(L,IGROB,1) = 
     &                xmo_RPAST(L,IGROB-1,mo_NBNOZO(L,IGROB-1))
                xmo_RPAST(L,IGROB,mo_NBNOZO(L,IGROB)) = 
     &                xmo_RPAST(L,IGROB,1) + DIST
              ENDDO
              IF (mo_NB_ZONE(L) .GT. 1) THEN
                xmo_RPAST(L,mo_NB_ZONE(L),1) = 
     +          xmo_RPAST
     +          (L,mo_NB_ZONE(L)-1,mo_NBNOZO(L,mo_NB_ZONE(L)-1))
              ENDIF
          ELSEIF(TEXT.EQ.'ISOVOLUME')THEN
              SZONE = 
     &         (RPAST_EXT**2.D0 - RPAST_TROU**2.D0)/mo_NB_ZONE(L)
              xmo_RPAST(L,1,mo_NBNOZO(L,1)) = 
     &                         (SZONE + RPAST_TROU**2.D0)**0.5D0
              DO IGROB = 2,mo_NB_ZONE(L)-1
                  xmo_RPAST(L,IGROB,1) = 
     &              xmo_RPAST(L,IGROB-1,mo_NBNOZO(L,IGROB-1))
                  xmo_RPAST(L,IGROB,mo_NBNOZO(L,IGROB)) = 
     &            ( SZONE + 
     &              xmo_RPAST(L,IGROB-1,mo_NBNOZO(L,IGROB-1))**2.D0 )
     &            **0.5D0
              ENDDO
              IF (mo_NB_ZONE(L) .GT. 1) THEN
                xmo_RPAST(L,mo_NB_ZONE(L),1) = 
     +          xmo_RPAST
     +          (L,mo_NB_ZONE(L)-1,mo_NBNOZO(L,mo_NB_ZONE(L)-1))
              ENDIF
          ELSEIF(TEXT.EQ.'FOURNI')THEN
              CALL REDLEC(4)
              IF(TEXT.EQ.'RAYON_ZONE_CRAYON')THEN
                DO IGROB = 1,mo_NB_ZONE(L)
                  CALL REDLEC(2)
                  xmo_RPAST(L,IGROB,mo_NBNOZO(L,IGROB)) = DFLOT
                ENDDO
                DO IGROB = 2,mo_NB_ZONE(L)
                  xmo_RPAST(L,IGROB,1) = 
     +                  xmo_RPAST(L,IGROB-1,mo_NBNOZO(L,IGROB-1))
                ENDDO
              ELSE
                WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'LE MOT CLE ATTENDU EST RAYON_ZONE_CRAYON '
                STOP
              ENDIF 
          ELSE
                WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'LE MOT CLE ATTENDU EST EQUIDISTANT ou ',
     +         'ISOVOLUME ou FOURNI'
                STOP
          ENDIF               
      ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' ON ATTENDAIT TYPE_MAILLAGE_ZONE '
             STOP
      ENDIF
*
*     7--0---------0---------0---------0---------0---------0---------0--
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'TYPE_MAILLAGE_NOEUD')THEN
            CALL REDLEC(4)
            IF(TEXT.EQ.'EQUIDISTANT')THEN
              DO IGROB = 1, mo_NB_ZONE(L)
                IF ( mo_NBNOZO(L,IGROB) . GT. 1) THEN
                  DIST = ( xmo_RPAST(L,IGROB,mo_NBNOZO(L,IGROB)) 
     +                     - xmo_RPAST(L,IGROB,1) ) /
     +                   ( mo_NBNOZO(L,IGROB) - 1 )
                  DO I = 2, mo_NBNOZO(L,IGROB)-1
                    xmo_RPAST(L,IGROB,I) = 
     +                  xmo_RPAST(L,IGROB,I-1) + DIST
                  ENDDO
                ENDIF       
              ENDDO
            ELSEIF(TEXT.EQ.'ISOVOLUME')THEN
              DO IGROB = 1, mo_NB_ZONE(L)
                IF ( mo_NBNOZO(L,IGROB) . GT. 1) THEN
                  SNOEU = 
     +            ( xmo_RPAST(L,IGROB,mo_NBNOZO(L,IGROB))**2.D0 
     +              - xmo_RPAST(L,IGROB,1)**2.D0 ) /
     +            ( mo_NBNOZO(L,IGROB) - 1 )
                  DO I = 2, mo_NBNOZO(L,IGROB)-1
                    xmo_RPAST(L,IGROB,I) = 
     +              ( xmo_RPAST(L,IGROB,I-1)**2.D0 + SNOEU )**0.5D0
                  ENDDO 
                ENDIF           
              ENDDO
            ELSE
              WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'LE MOT CLE ATTENDU EST ',
     +         'EQUIDISTANT ou  ISOVOLUME'
              STOP
            ENDIF
      ELSE
            WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         'ON ATTENDAIT TYPE_MAILLAGE_NOEUD'
            STOP
      ENDIF
*
*
*
*     ==================================================================
*     ==================================================================
 90   CONTINUE
*     ==================================================================
*     ==================================================================
*
      IF ( AEAX1 .EQ. 0  .AND. mo_NB_TRAX .GT. 1 ) THEN
        DO  L = 2, mo_NB_TRAX
          mo_NB_ZONE(L) = mo_NB_ZONE(1)
          DO IGROB = 1, mo_NB_ZONE(1)
            mo_NBNOZO(L,IGROB) = mo_NBNOZO(1,IGROB)
            DO I = 1, mo_NBNOZO(1,IGROB)
              xmo_RPAST(L,IGROB,I) = xmo_RPAST(1,IGROB,I)
            ENDDO
          ENDDO
        ENDDO
      ENDIF
*
*     ==================================================================
      CALL REDLEC(4)
      IF(TEXT.EQ.'FIN') THEN
        GO TO 9999
      ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' ON ATTENDAIT FIN '
             STOP
      ENDIF
*     ==================================================================
*     7--0---------0---------0---------0---------0---------0---------0--
*
 9999 WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DE LA RUBRIQUE GEOMETRIE_TRAITEE',/)
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREPI
*     =================
*
*     ******************************************************************
*     MOREPI : MOgador REad des Param�tres d impression
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmoginf.inc : xmo_TPSAFF mo_FREQIMPR mo_NREPRISELU mo_NTPSINI xmo_TPSINI 
*               mo_DEPART_SUR_REPRISE NOM_FICH_REPRISE
*               mo_NREPRISE mo_NTPSREP xmo_TPSREP 
*               mo_TRANCHE_DEB mo_TRANCHE_FIN 
*               mo_NBPTANA en sortie
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
      DO 10 J = 1,11
           CALL REDLEC(4)
           IF(TEXT.EQ.'TEMPS_AFFICHAGE')THEN
                CALL REDLEC(2)
                xmo_TPSAFF(1) = DFLOT
                CALL REDLEC(2)
                xmo_TPSAFF(2) = DFLOT
           ELSEIF(TEXT.EQ.'FREQUENCE_IMPRESSION')THEN
                CALL REDLEC(1)
                mo_FREQIMPR = NFIXE
           ELSEIF(TEXT.EQ.'POINT_ANALYSE')THEN
                DO L = 1,mo_M31MAX
                DO IGROB = 1, mo_IGRMAX
                  DO I = 1, mo_IFEMAX
                    DO IFICH = 1, mo_IFICHMX
                      mo_IMPR_ANA(L,IGROB,I,IFICH) = 0
                    ENDDO
                    mo_NORDR_ANA(L,IGROB,I) = 0
                  ENDDO
                ENDDO
                ENDDO
                CALL REDLEC(1)
                mo_NBPTANA = NFIXE
                DO K = 1,mo_NBPTANA
                  CALL REDLEC(1)
                  L_LU = NFIXE
                  CALL REDLEC(1)
                  IGROB_LU = NFIXE
                  CALL REDLEC(1)
                  I_LU = NFIXE 
                  mo_NORDR_ANA(L_LU,IGROB_LU,I_LU) = K
                  DO IFICH = 1, mo_IFICHMX
                    CALL REDLEC(1)
                    mo_IMPR_ANA(L_LU,IGROB_LU,I_LU,IFICH) = NFIXE
                  ENDDO                 
                ENDDO
           ELSEIF(TEXT.EQ.'INITIALISATION_SUR_REPRISE')THEN
                mo_DEPART_SUR_REPRISE = 1
                CALL REDLEC(1)
                mo_NREPRISELU = NFIXE
                CALL REDLEC(1)
                mo_NTPSINI = NFIXE
                CALL REDLEC(2)
                xmo_TPSINI = DFLOT
                CALL REDLEC(4)
                IF(TEXT.EQ.'FICHIER')THEN
                  CALL REDLEC(4)
                  NOM_FICH_REPRISE = TEXT
                ELSE
                  WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +            ' ON ATTENDAIT LE MOT CLE : ', 'FICHIER'
                  STOP
                ENDIF
           ELSEIF(TEXT.EQ.'IMPRESSION_POUR_REPRISE')THEN
                CALL REDLEC(1)
                mo_NREPRISE = NFIXE
                DO 20 II = 1, mo_NREPRISE
                  CALL REDLEC(1)
                  mo_NTPSREP(II) = NFIXE
                  CALL REDLEC(2)
                  xmo_TPSREP(II) = DFLOT
 20             CONTINUE
                IF (mo_NREPRISE .GT. 0) THEN
                  CALL REDLEC(4)
                  IF(TEXT.EQ.'TRANCHE')THEN
                    CALL REDLEC(1)
                    mo_TRANCHE_DEB = NFIXE
                    CALL REDLEC(1)
                    mo_TRANCHE_FIN = NFIXE
                  ELSE
                    WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +              ' ON ATTENDAIT LE MOT CLE : ', 'TRANCHE'
                    STOP
                  ENDIF
                ENDIF
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'PARAMETRES_D_IMPRESSION'
             STOP
           ENDIF
10    CONTINUE
*
 9999 WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DES PARAMETRES D IMPRESSION',/)
*
*
*
*
      RETURN
      END
*
*
     
      SUBROUTINE MOREPN
*     =================
*
*     ******************************************************************
*     MOREPN : MOgador REad des Param�tres Num�riques
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoggpt.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************
* cmoggpt.inc : toutes en sorties      
* cmogpnu.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
*
      CALL REDLEC(4)
      IF(TEXT.EQ.'DEGRE_DISCRETISATION_ESPACE') THEN
             CALL REDLEC(1)
             mo_DEGRE = NFIXE
             IF(mo_DEGRE .NE. 0) THEN
               WRITE(NEX,*) 'ERREUR DE LECTURE SUR LA VALEUR '//
     +                 'DEGRE_DISCRETISATION_ESPACE : '//
     +                 'LA VALEUR ATTENDUE EST 0 (VF) '
               STOP
             ENDIF
      ELSEIF(TEXT.EQ.'FIN') THEN
             IFIN = 1
             GO TO 9999
      ELSE
        WRITE(NEX,*) 'LE MOT CLE DEGRE_DISCRETISATION_ESPACE'//
     +      ' EST OBLIGATOIRE EN DEBUT DE RUBRIQUE'//
     +      ' PARAMETRES_DE_RESOLUTION_NUMERIQUE_MARGARET_V3.1'
        STOP
      ENDIF
*
      DO 10 J = 1,7
           CALL REDLEC(4)
*     7--0---------0---------0---------0---------0---------0---------0--  
           IF(TEXT.EQ.'NOMBRE_NOEUDS_INTERNES_GRAIN')THEN
             CALL REDLEC(1)
             NLU = NFIXE
c            maillage volumes finis
             IF(NLU+1.GT.mo_NNSPHMX) THEN
                   WRITE(NEX,*) 
     +             'LE NOMBRE MAX DE RAYONS A FOURNIR EST ',
     +                           mo_NNSPHMX-1
                   STOP
             ENDIF
             CALL REDLEC(4)
             IF(TEXT.EQ.'RAYONS_RELATIFS_ELEMENTS_MAILLAGE_GRAIN')THEN
c              maillage volumes finis
               DO JJ = 1,NLU
                   CALL REDLEC(2)
                   xmo_RGG(JJ) = DFLOT
               ENDDO
               xmo_RGG(NLU+1) = 1.D0
*
*              initialisation de mo_NGG
               mo_NGG = NLU + 1
             ELSE
               WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' LE MOT CLE ATTENDU EST'//
     +         ' RAYONS_RELATIFS_ELEMENTS_MAILLAGE_GRAIN'
               STOP
             ENDIF
*     7--0---------0---------0---------0---------0---------0---------0--  
           ELSEIF(TEXT.EQ.'NOMBRE_NOEUDS_INTERNES'//
     &                    '_SPHERE_EQUIVALENTE')THEN
             CALL REDLEC(1)
             NLU = NFIXE
c            maillage volumes finis
             IF(NLU+1.GT.mo_NNSPHMX) THEN
                 WRITE(NEX,*) 
     +           'LE NOMBRE MAX DE RAYONS A FOURNIR SEQ EST ',
     +            mo_NNSPHMX-1
                 STOP
             ENDIF
             CALL REDLEC(4)
             IF(TEXT.EQ.'RAYONS_RELATIFS_ELEMENTS_MAILLAGE'//
     &                  '_SPHERE_EQUIVALENTE')THEN
c              maillage volumes finis
               DO JJ = 1, NLU
                   call REDLEC(2)
                   xmo_RSEQ(JJ) = DFLOT
               ENDDO
               xmo_RSEQ(NLU+1) = 1.D0
*
*            initialisation de mo_NSEQ
               mo_NSEQ = NLU+1
             ELSE
               WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' LE MOT CLE ATTENDU EST'//
     +         ' RAYONS_RELATIFS_ELEMENTS_MAILLAGE_SPHERE_EQUIVALENTE'
               STOP
             ENDIF
*     7--0---------0---------0---------0---------0---------0---------0-- 
           ELSEIF(TEXT.EQ.'NOMBRE_NOEUDS_INTERNES_'//
     &                    'SPHERE_EQUIVALENTE_RESTRUCTURE')THEN
             CALL REDLEC(1)
             NLU = NFIXE
c            maillage volumes finis
             IF(NLU+1.GT.mo_NNSPHMX) THEN
                 WRITE(NEX,*) 
     +           'LE NOMBRE MAX DE RAYONS A FOURNIR SEQR EST ',
     +            mo_NNSPHMX-1
                 STOP
             ENDIF
             CALL REDLEC(4)
             IF(TEXT.EQ.'RAYONS_RELATIFS_ELEMENTS_MAILLAGE_SPHERE_'//
     &                  'EQUIVALENTE_RESTRUCTURE')THEN
c              maillage volumes finis
               DO JJ = 1, NLU
                   CALL REDLEC(2)
                   xmo_RSEQR(JJ) = DFLOT
               ENDDO
               xmo_RSEQR(NLU+1) = 1.D0
*
*            initialisation de mo_NSEQR
               mo_NSEQR = NLU+1
             ELSE
               WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +         ' LE MOT CLE ATTENDU EST'//
     +         ' RAYONS_RELATIFS_ELEMENTS_MAILLAGE_'//
     +         'SPHERE_EQUIVALENTE_RESTRUCTURE'
               STOP
             ENDIF
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'SCHEMA_NUMERIQUE_EN_TEMPS')THEN
                CALL REDLEC(1)
                mo_SCHEMA = NFIXE
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'NOMBRE_DE_PAS_PAR_PAS_DU_CODE_CIBLE')THEN
                CALL REDLEC(1)
                mo_NTEMPS_LU = NFIXE
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'PAS_DE_TEMPS_MAX')THEN
                CALL REDLEC(2)
                xmo_PAS_TEMPS_LIM = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'PARAMETRES_DE_RESOLUTION_NUMERIQUE'
             STOP
           ENDIF
10    CONTINUE
*
 9999 WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DES PARAMETRES DE RESOLUTION NUMERIQUE',/)
*
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREPP
*     =================
*
*     ******************************************************************
*     MOREPP : MOgador REad des Param�tres Primaires
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'      
************************************************************************      
* cmogppr.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
      DO 10 J = 1,74
           CALL REDLEC(4)
*     7--0---------0---------0---------0---------0---------0---------0--
           IF(TEXT.EQ.'FACTEUR_REMISE_EN_SOLUTION_DEPUIS_JOINT')THEN
                CALL REDLEC(2)
                xmo_FACBJ = DFLOT
           ELSEIF(TEXT.EQ.'MODELE_POUR_GAZ_INITIAL_'//
     &                    'DANS_LES_PORES')THEN
                CALL REDLEC(1)
                mo_MODELE_GAZPORE = NFIXE
           ELSEIF(TEXT.EQ.'REMISE_EN_SOLUTION_DEPUIS_BULLE')THEN
                CALL REDLEC(4)
                IF(TEXT.EQ.'NELSON')THEN
                  mo_RESOL = 1
                  CALL REDLEC(4)
                  IF (TEXT.EQ.'PARAMETRE_MODELE_NELSON')THEN
                     CALL MORNEL
                  ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' LE MOT ATTENDU EST : PARAMETRE_MODELE_NELSON '
                     STOP
                  ENDIF
                ELSEIF (TEXT.EQ.'SIMPLE')THEN 
                  mo_RESOL = 2
                  CALL REDLEC(4)
                  IF(TEXT.EQ.'PARAMETRE_MODELE_SIMPLE')THEN
                     CALL MORSIM
                  ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' LE MOT ATTENDU EST : PARAMETRE_MODELE_SIMPLE '
                     STOP
                  ENDIF
                ELSEIF (TEXT.EQ.'NELSON_SANS_NRE')THEN 
                  mo_RESOL = 3
                  CALL REDLEC(4)
                  IF (TEXT.EQ.'PARAMETRE_MODELE_NELSON')THEN
                     CALL MORNEL
                  ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' LE MOT ATTENDU EST : PARAMETRE_MODELE_NELSON '
                     STOP
                  ENDIF
                ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' LE MOT ATTENDU EST : NELSON OU SIMPLE '
                     STOP
                ENDIF 
           ELSEIF(TEXT.EQ.'RAPPORT_REMISE_EN_SOLUTION_BULLE'//
     &                    '_INTER_SUR_INTRA')THEN
                CALL REDLEC(2)
                xmo_BBJ_S_BB = DFLOT
           ELSEIF(TEXT.EQ.'REMISE_EN_SOLUTION_CAVITE'//
     &                    '_RIM')THEN
                CALL REDLEC(1)
                mo_REM_CAV_RIM = NFIXE
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'FONCTION_DDES')THEN
                CALL REDLEC(1)
                mo_IDEST = NFIXE
                IF (mo_IDEST .EQ. 1) THEN
                  CALL REDLEC(2)
                  xmo_EXP_DDESTR = DFLOT
                ELSEIF (mo_IDEST .EQ. 2) THEN
                  CALL REDLEC(2)
                  xmo_RBDEST(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBDEST(2) = DFLOT
                ELSEIF (mo_IDEST .EQ. 3) THEN
                  CALL REDLEC(2)
                  xmo_RBDEST(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBDEST(2) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBDEST(3) = DFLOT
                  CALL REDLEC(2)
                  xmo_PDEST = DFLOT
                  xmo_LARG1 = xmo_RBDEST(2) - xmo_RBDEST(1)
                  xmo_LARG2 = xmo_RBDEST(3) - xmo_RBDEST(2)
*
                  xmo_A1 = 
     &            (xmo_PDEST * xmo_LARG1 + 1.D0) / xmo_LARG2**3.D0
                  xmo_B1 = 
     &            - (2.D0 * xmo_PDEST * xmo_LARG1 + 3.D0) / 2.D0
     &              / xmo_LARG1**2.D0
*
                  xmo_A2 = 
     &            (xmo_PDEST * xmo_LARG2 + 1.D0) / xmo_LARG2**3.D0
                  xmo_B2 = 
     &            (2.D0 * xmo_PDEST * xmo_LARG2 + 3.D0) / 2.D0
     &              / xmo_LARG2**2.D0
                ELSEIF (mo_IDEST .EQ. 4) THEN
                  CALL REDLEC(2)
                  xmo_RBDEST(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBDEST(2) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBDEST(3) = DFLOT
                  CALL REDLEC(2)
                  xmo_PDEST = DFLOT
                  CALL REDLEC(2)
                  xmo_QDEST = DFLOT
                  xmo_LARG1 = xmo_RBDEST(2) - xmo_RBDEST(1)
                  xmo_LARG2 = xmo_RBDEST(2) - xmo_RBDEST(3)
*
                  xmo_FF = 1.D0
                  xmo_A1 = 
     &              6.D0 / xmo_LARG1**5 * (0.5D0-xmo_FF)
     &              - 3.D0 * xmo_PDEST / xmo_LARG1**4
     &              + xmo_QDEST  /2.D0 / xmo_LARG1**3
                  xmo_B1 = 
     &              - 15.D0 / xmo_LARG1**4 * (0.5D0-xmo_FF)
     &              + 7.D0 * xmo_PDEST / xmo_LARG1**3
     &              - xmo_QDEST / xmo_LARG1**2
                  xmo_C1 = 
     &                10.D0 / xmo_LARG1**3 * (0.5D0-xmo_FF)
     &              - 4.D0 * xmo_PDEST / xmo_LARG1**2
     &              + xmo_QDEST / 2.D0 / xmo_LARG1
*
                  xmo_FF = 0.D0
                  xmo_A2 = 
     &              6.D0 / xmo_LARG2**5 * (0.5D0-xmo_FF)
     &              - 3.D0 * xmo_PDEST / xmo_LARG2**4
     &              + xmo_QDEST  /2.D0 / xmo_LARG2**3
                  xmo_B2 = 
     &              - 15.D0 / xmo_LARG2**4 * (0.5D0-xmo_FF)
     &              + 7.D0 * xmo_PDEST / xmo_LARG2**3
     &              - xmo_QDEST / xmo_LARG2**2
                  xmo_C2 = 
     &                10.D0 / xmo_LARG2**3 * (0.5D0-xmo_FF)
     &              - 4.D0 * xmo_PDEST / xmo_LARG2**2
     &              + xmo_QDEST / 2.D0 / xmo_LARG2
                ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' ON ATTEND 1 OU 2 OU 3 OU 4 '
                     STOP
                ENDIF
           ELSEIF(TEXT.EQ.'DISTANCE_REMISE_EN_SOLUTION')THEN
                CALL REDLEC(2)
                xmo_DREM = DFLOT
           ELSEIF(TEXT.EQ.'FONCTION_DNUCL')THEN
                CALL REDLEC(1)
                mo_INUCL = NFIXE
                IF (mo_INUCL .EQ. 2) THEN
                  CALL REDLEC(2)
                  xmo_RBNU(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBNU(2) = DFLOT
                ELSEIF (mo_INUCL .EQ. 1 .OR. mo_INUCL .EQ. 3) THEN
                  CALL REDLEC(2)
                  xmo_EXP_DNUCL = DFLOT
                ELSEIF (mo_INUCL .EQ. 4) THEN
                  CALL REDLEC(2)
                  xmo_RBNU(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBNU(2) = DFLOT
                  CALL REDLEC(2)
                  xmo_EXP_DNUCL = DFLOT
                ELSEIF (mo_INUCL .EQ. 5) THEN
                  CALL REDLEC(2)
                  xmo_RBNU(1) = DFLOT
                  CALL REDLEC(2)
                  xmo_RBNU(2) = DFLOT
                  CALL REDLEC(2)
                  xmo_MULTRB2 = DFLOT
                  CALL REDLEC(2)
                  xmo_EXP_DNUCL = DFLOT
                ELSE
                     WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     &               ' ON ATTEND 1 OU 2 OU 3 OU 4'
                     STOP
                ENDIF
           ELSEIF(TEXT.EQ.'EFFICACITE_NUCLEATION_HOMOGENE')THEN
                CALL REDLEC(2)
                xmo_FNB = DFLOT
           ELSEIF(TEXT.EQ.'LONGUEUR_POINTE_DE_FISSION_CHOC')THEN
                CALL REDLEC(2)
                xmo_MUF_CHOC = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'CALCUL_VOLUME_DENSITE_PORE')THEN
             CALL REDLEC(4)
             IF(TEXT.EQ.'MARGARET') THEN
*              MARGARET se charge de calculer le volume et la densit� 
*              des pores intra et inter
               mo_MODELE_DENSIFICATION = 0
             ELSEIF(TEXT.EQ.'VTOT_PORE_IMPOSE') THEN
*              MARGARET consid�re en entr�e le volume total des pores 
*              et se charge de la r�partition en intra et inter et du 
*              calcul des densit�s de pores intra et inter
*              A partir de l'�tat (2) MARGARET se charge de tout
               mo_MODELE_DENSIFICATION = 1
             ELSEIF(TEXT.EQ.'MOGADOR-DENS') THEN
*              MARGARET consid�re en entr�e le volume total des pores 
*              intra et inter et les densit�s de pores intra et inter
*              que lui fournit MOGADOR-DENS
*              A partir de l'�tat (2) MARGARET se charge de tout
               mo_MODELE_DENSIFICATION = 2
             ELSE
               WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT
                     STOP
             ENDIF
           ELSEIF(TEXT.EQ.'GAZ_DANS_UNE_BULLE_A_LA_NUCLEATION')THEN
                CALL REDLEC(2)
                xmo_MN = DFLOT
           ELSEIF(TEXT.EQ.'EFFICACITE_NUCLEATION_HOMOGENE_AU_JOINT')THEN
                CALL REDLEC(2)
                xmo_FNBJ = DFLOT
           ELSEIF(TEXT.EQ.'EFFICACITE_NUCLEATION_POINTE_AU_JOINT')THEN
                CALL REDLEC(2)
                xmo_FNBJP = DFLOT
           ELSEIF(TEXT.EQ.'COEFFICIENT_DIFFUSION_GAZ_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_TDXE(1) = DFLOT
                CALL REDLEC(2)
                xmo_TDXE(2) = DFLOT
                CALL REDLEC(2)
                xmo_TDXE(3) = DFLOT
                CALL REDLEC(2)
                xmo_TDXE(4) = DFLOT
                CALL REDLEC(2)
                xmo_TDXE(5) = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'FACTEUR_DIFFUSION_XE_AMELIOREE') THEN
                CALL REDLEC(1)
                mo_DIFXE_AMELIOREE = NFIXE
           ELSEIF(TEXT.EQ.
     &          'COEFFICIENT_DIFFUSION_CATION_EN_SURFACE_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_TDS(1) = DFLOT
                CALL REDLEC(2)
                xmo_TDS(2) = DFLOT
                CALL REDLEC(2)
                xmo_TDS(3) = DFLOT
                CALL REDLEC(2)
                xmo_TDS(4) = DFLOT
                CALL REDLEC(2)
                xmo_TDS(5) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION_EN_VOLUME_PRES'//
     &            '_SURFACE_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_TDUS(1,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(2,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(3,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(4,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(5,1) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION_EN_VOLUME_PRES'//
     &            '_SURFACE_DANS_UPUO2')THEN
                CALL REDLEC(2)
                xmo_TDUS(1,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(2,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(3,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(4,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUS(5,2) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION_EN_VOLUME'//
     &            '_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_TDU(1,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(2,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(3,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(4,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(5,1) = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION_EN_VOLUME'//
     &            '_DANS_UPUO2')THEN
                CALL REDLEC(2)
                xmo_TDU(1,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(2,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(3,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(4,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDU(5,2) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION'//
     &            '_AU_JOINT_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_TDUJ(1,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(2,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(3,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(4,1) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(5,1) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_DIFFUSION_CATION'//
     &            '_AU_JOINT_DANS_UPUO2')THEN
                CALL REDLEC(2)
                xmo_TDUJ(1,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(2,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(3,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(4,2) = DFLOT
                CALL REDLEC(2)
                xmo_TDUJ(5,2) = DFLOT
           ELSEIF(TEXT.EQ.'RAPPORT_DXEJTH_SUR_DXETH')THEN
                CALL REDLEC(2)
                xmo_RAP_DXE_TH = DFLOT
           ELSEIF(TEXT.EQ.'RAPPORT_DXEJMIX_SUR_DXEMIX')THEN
                CALL REDLEC(2)
                xmo_RAP_DXE_MIX = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'RAPPORT_DXEJATH_SUR_DXEATH')THEN
                CALL REDLEC(2)
                xmo_RAP_DXE_ATH = DFLOT
           ELSEIF(TEXT.EQ.'VOLUME_SAUT_ATOME')THEN
                CALL REDLEC(2)
                xmo_QVOL = DFLOT
           ELSEIF(TEXT.EQ.
     &            'CHALEUR_TRANSPORT_AUTO_DIFFUSION_EN_SURFACE')THEN
                CALL REDLEC(2)
                xmo_QS = DFLOT 
           ELSEIF(TEXT.EQ.
     &            'CHALEUR_TRANSPORT_AUTO_DIFFUSION_EN'//
     &            '_VOLUME_DANS_UO2')THEN
                CALL REDLEC(2)
                xmo_QV(1) = DFLOT
           ELSEIF(TEXT.EQ.
     &            'CHALEUR_TRANSPORT_AUTO_DIFFUSION_EN'//
     &            '_VOLUME_DANS_UPUO2')THEN
                CALL REDLEC(2)
                xmo_QV(2) = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_COALESCENCE_BULLES_INTRA_SOUS'//
     &            '_GRADIENT_THERMIQUE')THEN
                CALL REDLEC(2)
                xmo_FMIGR = DFLOT
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_COALESCENCE_PORES_INTRA_SOUS'//
     &            '_GRADIENT_THERMIQUE')THEN
                CALL REDLEC(2)
                xmo_FMIGRPOR = DFLOT
           ELSEIF(TEXT.EQ.
     &            'DISTANCE_DE_COALESCENCE_GEOMETRIQUE_INTRA'
     &            )THEN
                CALL MORDCOAL(xmo_DMIN,xmo_DMAX1,xmo_DMAX2,
     &                        xmo_TKMIN, xmo_TKMAX,
     &                  xmo_STFMIN, xmo_STFMAX1, xmo_STFMAX2)
                IF ( (xmo_TKMAX-xmo_TKMIN) .LE. 0.D0 ) THEN
                  WRITE(NEX,*) 
     &            'les temperatures doivent etre differentes',
     &            ' et donnees dans l ordre croissant' 
                ENDIF
                IF ( ((xmo_STFMAX1-xmo_STFMIN) .LE. 0.D0) .OR.
     &               ((xmo_STFMAX2-xmo_STFMAX1) .LE. 0.D0)) THEN
                  WRITE(NEX,*) 
     &            'les taux de fission integres doivent etre ',
     &            ' differents et donnees dans l ordre croissant' 
                ENDIF
                xmo_MIN_DISTCOAL = xmo_DMIN
                xmo_MAX_DISTCOAL1 = xmo_DMAX1
                xmo_MAX_DISTCOAL2 = xmo_DMAX2
           ELSEIF(TEXT.EQ.
     &            'DISTANCE_DE_COALESCENCE_GEOMETRIQUE_INTER'
     &            )THEN
                CALL MORDCOAL(xmo_DMIN,xmo_DMAX1,xmo_DMAX2,
     &                        xmo_TKMINJ, xmo_TKMAXJ,
     &                  xmo_STFMINJ, xmo_STFMAXJ1,xmo_STFMAXJ2)
                IF ( (xmo_TKMAXJ-xmo_TKMINJ) .LE. 0.D0 ) THEN
                  WRITE(NEX,*) 
     &            'les temperatures doivent etre differentes',
     &            ' et donnees dans l ordre croissant' 
                ENDIF
                IF ( ((xmo_STFMAXJ1-xmo_STFMINJ) .LE. 0.D0) .OR.
     &               ((xmo_STFMAXJ2-xmo_STFMAXJ1) .LE. 0.D0) ) THEN
                  WRITE(NEX,*) 
     &            'les taux de fission integres doivent etre ',
     &            ' differents et donnees dans l ordre croissant' 
                ENDIF
                xmo_MIN_DISTCOALJ = xmo_DMIN
                xmo_MAX_DISTCOALJ1 = xmo_DMAX1
                xmo_MAX_DISTCOALJ2 = xmo_DMAX2
           ELSEIF(TEXT.EQ.
     &            'COEFFICIENT_COALESCENCE_BULLES_INTER_SOUS'//
     &            '_GRADIENT_THERMIQUE')THEN
                CALL REDLEC(2)
                xmo_FMIGRJ = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.
     &            'PRISE_EN_COMPTE_VITESSE_DES_BULLES_INTER_SOUS'//
     &            '_GRADIENT_THERMIQUE')THEN
                CALL REDLEC(1)
                mo_VBJ_OUI = NFIXE 
           ELSEIF(TEXT.EQ.'FONCTION_DELTABJ')THEN
                CALL REDLEC(2)
                xmo_TDBJ(1) = DFLOT
                CALL REDLEC(2)
                xmo_TDBJ(2) = DFLOT
                CALL REDLEC(2)
                xmo_TDBJ(3) = DFLOT
                CALL REDLEC(2)
                xmo_TDBJ(4) = DFLOT
           ELSEIF(TEXT.EQ.'RAPPORT_VOLUME_GAZ_LACUNE')THEN
                CALL REDLEC(2)
                xmo_ZETA = DFLOT
           ELSEIF(TEXT.EQ.'TENSION_DE_SURFACE_LIBRE')THEN
                CALL REDLEC(2)
                xmo_COEF1_GAMMA = DFLOT
                CALL REDLEC(2)
                xmo_COEF2_GAMMA = DFLOT
           ELSEIF(TEXT.EQ.'VOLUME_ATOMIQUE_XE_SOLIDE')THEN
                CALL REDLEC(2)
                xmo_VATXES = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'VAT/BVDW_LIMITES_POUR_EQUATION_D_ETAT')THEN
                CALL REDLEC(2)
                xmo_XLSUP = DFLOT
                CALL REDLEC(2)
                xmo_XLINF = DFLOT
           ELSEIF(TEXT.EQ.'COEFFICIENT_DE_LIMITATION_PERCOLATION')THEN
                CALL REDLEC(2)
                xmo_COELIM_PERC = DFLOT
           ELSEIF(TEXT.EQ.'PARAMETRE_PERCOLATION')THEN
                CALL REDLEC(2)
                xmo_KTUNJ = DFLOT
                CALL REDLEC(2)
                xmo_KTUNJPORE_SUR_KTUNJ = DFLOT
                xmo_KTUNJPORE = xmo_KTUNJPORE_SUR_KTUNJ * xmo_KTUNJ
           ELSEIF(TEXT.EQ.'ALPHA_XE_J')THEN
                CALL REDLEC(2)
                xmo_AXEJ = DFLOT
           ELSEIF(TEXT.EQ.'ALPHA_B_J')THEN
                CALL REDLEC(2)
                xmo_ABJ = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'ANGLE_TETA_RADIAN')THEN
                CALL REDLEC(2)
                xmo_TETBJ = DFLOT
           ELSEIF(TEXT.EQ.'ALPHA_V_J')THEN
                CALL REDLEC(2)
                xmo_AVJ = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'RAYON_ZONE_INFLUENCE_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_LDIS = DFLOT
           ELSEIF(TEXT.EQ.'EPAISSEUR_JOINT_DE_GRAIN')THEN
                CALL REDLEC(2)
                xmo_EPJ = DFLOT
           ELSEIF(TEXT.EQ.'RAYON_GRAIN_RESTRUCTURE')THEN
                CALL REDLEC(2)
                xmo_APG = DFLOT
           ELSEIF(TEXT.EQ.'PRODUCTION_DISLOCATION_FCT_BURNUP')THEN
                CALL REDLEC(2)
                xmo_PROBU = DFLOT
           ELSEIF(TEXT.EQ.'TEMPS_DISPARITION_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_TOREC = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'ZONE_TEMPERATURE_RECUIT')THEN
                CALL REDLEC(2)
                xmo_DTKREC = DFLOT
           ELSEIF(TEXT.EQ.'TEMPERATURE_RECUIT_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_TKREC = DFLOT
           ELSEIF(TEXT.EQ.
     &            'DEMI_LARGEUR_DISPERTION_DENSITE'//
     &            '_DE_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_RHODL = DFLOT
           ELSEIF(TEXT.EQ.'DENSITE_BULLE_COMBUSTIBLE_RESTRUCTURE')THEN
                CALL REDLEC(2)
                xmo_CBRIM0 = DFLOT
           ELSEIF(TEXT.EQ.'DISTANCE_ECHAPPEMENT_RIM')THEN
                CALL REDLEC(2)
                xmo_DIST_ECHAP_RIM = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'MODELE_SPHERE_EQUIVALENTE_RIM')THEN
                CALL REDLEC(1)
                mo_MODASERIM = NFIXE
                IF (mo_MODASERIM .EQ. 1 .OR. mo_MODASERIM .EQ. 2) THEN
                  CALL REDLEC(2)
                  xmo_ASERF = DFLOT
                ENDIF
           ELSEIF(TEXT.EQ.'FAC_GONFLEMENT_SOLIDE')THEN
                CALL REDLEC(2)
                xmo_XGONF = DFLOT
           ELSEIF(TEXT.EQ.'MARGE_APPROCHE_DENSITE_DE'//
     &                    '_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_MARGRHO_APPR = DFLOT
           ELSEIF(TEXT.EQ.'MARGE_ETAT_DENSITE_DE_'//
     &                    'DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_MARGRHO_ETAT23 = DFLOT 
           ELSEIF(TEXT.EQ.'MARGE_FRACTION_RESTRUTUREE'//
     &                    '_DENSITE_DE_DISLOCATION')THEN
                CALL REDLEC(2)
                xmo_MARGRHO_FR = DFLOT 
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.
     &            'SEUIL_DENSITE_DE_DISLOCATION_POUR'//
     &            '_RESTRUCTURATION')THEN
                CALL REDLEC(2)
                xmo_RHOLIM = DFLOT
           ELSEIF(TEXT.EQ.
     &            'TAUX_DE_COUVERTURE_LIMITE_POUR_CONNEXION')THEN
                CALL REDLEC(1)
                mo_COUV_VAR = NFIXE
                CALL REDLEC(2)
                xmo_FCOUVLIM = DFLOT
                CALL REDLEC(2)
                xmo_DFCOUV = DFLOT
                IF (mo_COUV_VAR .NE. 0) THEN
                  CALL REDLEC(2)
                  xmo_AGG_REFERENCE = DFLOT
                  CALL REDLEC(2)
                  xmo_AGG_EXP = DFLOT
                ENDIF
           ELSEIF(TEXT.EQ.
     &            'RAYON_BULLE_MICROSONDE_ELECTRONIQUE')THEN
                CALL REDLEC(2)
                xmo_REPMA = DFLOT
           ELSEIF(TEXT.EQ.
     &            'DIFFUSION_A_GRANDE_ECHELLE_GAZ_'//
     &            'DISSOUS_AU_JOINT')THEN
                CALL REDLEC(1)
                mo_DIF_SEQ_LU = NFIXE
           ELSEIF(TEXT.EQ.'GAZ_DISSOUS_PRES_BULLE_CALCULE_PAR_EQUATION')
     &          THEN
                CALL REDLEC(1)
                mo_EQUA_CXERBLOC = NFIXE
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'COEFFICIENT_MULTIPLICATIF_'//
     &                    'APPORT_LACUNE_RIM')THEN
                CALL REDLEC(2)
                xmo_COE_LAC_RIM = DFLOT
           ELSEIF(TEXT.EQ.'COEFFICIENT_MULTIPLICATIF_'//
     &                    'VOLUME_ALIM_RIM')THEN
                CALL REDLEC(2)
                xmo_COE_VOL_ALIM__RIM = DFLOT
           ELSEIF(TEXT.EQ.'DENSITE_DE_DISLOCATION_INITIALE')THEN
                 CALL REDLEC(2)
                 xmo_RHODI = DFLOT
           ELSEIF(TEXT.EQ.'FRACTION_RESTRUCTUREE_LIMITE_'//
     &                    'POUR_CALCUL_GONFLEMENT_MOYEN')THEN
                 CALL REDLEC(2)
                 xmo_FRLIM_CALCUL_GONFMOY = DFLOT
           ELSEIF(TEXT.EQ.'CALCUL_FRACTION_RESTRUCTUREE')THEN
                CALL REDLEC(1)
                mo_MODELE_FRAC = NFIXE
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'SPHERE_EQUIVALENTE_ETAT_'//
     &                    'CLASSIQUE_IMPOSEE')THEN
                CALL REDLEC(1)
                mo_ASE_IMPOSE = NFIXE
                IF (mo_ASE_IMPOSE .EQ. 1) THEN
                 CALL REDLEC(2)
                 xmo_ASE_CENTRE = DFLOT
                 CALL REDLEC(2)
                 xmo_ASE_BORD = DFLOT
                ENDIF
           ELSEIF(TEXT.EQ.'REMISE_EN_SOLUTION_PORE_INTER') THEN
                CALL REDLEC(1)
                mo_REM_PORE_INTER = NFIXE 
           ELSEIF(TEXT.EQ.'EFFET_CLIQUET_BULLE') THEN 
                CALL REDLEC(1)
                mo_CLIQUET = NFIXE
                IF (mo_CLIQUET .EQ. 1) THEN
                 CALL REDLEC(2)
                 xmo_RCLIC(1) = DFLOT
                ENDIF
                IF (mo_CLIQUET .EQ. 2) THEN
                 CALL REDLEC(2)
                 xmo_RCLIC(1) = DFLOT
                 CALL REDLEC(2)
                 xmo_RCLIC(2) = DFLOT
                 IF (abs(xmo_RCLIC(2)-xmo_RCLIC(1)) .LT. 1.D-20) THEN
                   WRITE(NEX,*)  'mauvaises donnees pour xmo_RCLIC'
                   STOP
                 ENDIF        
                ENDIF               
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'GAZ_DE_FISSION_MARGARET_V3.1'
             STOP
           ENDIF
10    CONTINUE
*
* 
      IF ( xmo_RHODL .GT. xmo_RHOLIM ) THEN
        WRITE(NEX,*) 'La dispertion de la densit� '//
     &               'de dislocation est trop grande'
        STOP
      ENDIF
*
      IF ( (xmo_MARGRHO_APPR + xmo_MARGRHO_ETAT23) 
     &      .GE. xmo_RHODL ) THEN
        WRITE(NEX,*) 'Marge d approche + Marge etat'//
     &               ' > dispertion de la densit� '//
     &               'de dislocation '
        STOP
      ENDIF
*
      IF ( xmo_MARGRHO_ETAT23 
     &      .LE. xmo_MARGRHO_FR ) THEN
        WRITE(NEX,*) 'Marge etat'//
     &               ' < Marge fraction restructur�e : '//
     &               'voir les densit�s de dislocation '
        STOP
      ENDIF
*
 9999 WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DU MODELE MARGARET V3.1',/)
*
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MOREPT
*     =================
*
*     ******************************************************************
*     MOREPT : MOgador REad des Param�tres de Tests
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmogpte.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
      DO 10 J = 1,16
           CALL REDLEC(4)
           IF(TEXT.EQ.'FACTEUR_TERME_APPORT_INTER')THEN
                CALL REDLEC(2)
                xmo_TEST0 = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_MOBILITE_PORES_INTRA') THEN
                CALL REDLEC(2)
                xmo_TEST2 = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_MOBILITE_BULLES_INTRA') THEN
                CALL REDLEC(2)
                xmo_TEST3 = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_CROISSANCE_'//
     &                    'VOLUME_PORES_INTRA') THEN
                CALL REDLEC(2)
                xmo_TEST4 = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_DLBJ') THEN
                CALL REDLEC(2)
                xmo_TEST5 = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--  
           ELSEIF(TEXT.EQ.'TERME_SOURCE_INTER_IMPOSE') THEN
                CALL REDLEC(2)
                xmo_TEST6 = DFLOT
                CALL REDLEC(2)
                xmo_SOURXEIMP = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_REMISE_EN_'//
     &                    'SOLUTION_INTER_INTRA') THEN
                CALL REDLEC(2)
                xmo_TEST7 = DFLOT
           ELSEIF(TEXT.EQ.'FACTEUR_TERME_APPORT_'//
     &                    'INTER_PAR_LES_BULLES')THEN
                CALL REDLEC(2)
                xmo_TEST8 = DFLOT
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'GAZ_INTRA') THEN
                CALL REDLEC(2)
                xmo_EQUAG = DFLOT
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'BULLES_INTRA') THEN
                CALL REDLEC(2)
                xmo_EQUAB = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--  
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'PORES_INTRA') THEN
                CALL REDLEC(2)
                xmo_EQUAP = DFLOT
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'GAZ_INTER') THEN
                CALL REDLEC(2)
                xmo_EQUAGJ = DFLOT
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'BULLES_INTER') THEN
                CALL REDLEC(2)
                xmo_EQUABJ = DFLOT
           ELSEIF(TEXT.EQ.'EQUATIONS_'//
     &                    'PORES_INTER') THEN
                CALL REDLEC(2)
                xmo_EQUAPJ = DFLOT
           ELSEIF(TEXT.EQ.'EQUATION_'//
     &                    'DISLOCATIONS') THEN
                CALL REDLEC(2)
                xmo_EQUARHOD = DFLOT
*     7--0---------0---------0---------0---------0---------0---------0--
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'PARAMETRES_DE_TESTS'
             STOP
           ENDIF
10    CONTINUE
*
 9999 WRITE (NEX,900)     
 900  FORMAT (//,40X,'FIN DES PARAMETRES DE TESTS',/)
*
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MORNEL
*     =================
*
*     ******************************************************************
*     MORNEL : MOgador Read des param�tres du mod�le de NELson
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'cmogppr.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmogppr.inc : toutes en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
*
      DO 10 J = 1,6
           CALL REDLEC(4)
           IF(TEXT.EQ.'LONGUEUR_POINTE_DE_FISSION')THEN
             CALL REDLEC(2)
             xmo_MUF = DFLOT
           ELSEIF(TEXT.EQ.'ENERGIE_MAX_PF') THEN
             CALL REDLEC(2)
             xmo_EFFMAX = DFLOT
           ELSEIF(TEXT.EQ.'ENERGIE_MIN_DE_REIMPLANTATION') THEN
             CALL REDLEC(2)
             xmo_EREIMP = DFLOT
           ELSEIF(TEXT.EQ.'EPAISSEUR_ECRAN_GAZ_DENSE') THEN
             CALL REDLEC(2)
             xmo_EPECGD = DFLOT
           ELSEIF(TEXT.EQ.'COEFFICIENT_DE_CASCADE') THEN
             CALL REDLEC(2)
             xmo_COECASC = DFLOT
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'PARAMETRE_MODELE_NELSON'
             STOP
           ENDIF
10    CONTINUE
*
*     7--0---------0---------0---------0---------0---------0---------0--
 9999 WRITE (NEX,900)
*  
 900  FORMAT (//,40X,'FIN DES PARAMETRES DU MODELE DE NELSON',/)
*
*
*
*
      RETURN
      END
      SUBROUTINE MORONLEN(mo_PORJ_ROND)
C     =================
C
C
C     ******************************************************************
C     MORONLEN : MOgador RONd ou LENticulaire ?                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogphy.inc'
C
***********************************************************************
* cmogppr.inc :  xmo_ATETBJ en entr�e 
* cmogdon.inc :  xmo_VTPORJI xmo_CPORJI xmo_AGG en entr�e 
* cmogphy.inc :  xmo_PIZAHL en entr�e 
***********************************************************************
*
      CALL MOMESS('DANS MORONLEN')
*
* Evaluation du rayon des pores inter (rayon projet� sur le joint)
* en supposant le pore inter lenticulaire
*
      IF (xmo_CPORJI .GT. 0.D0 .AND. xmo_VTPORJI .GT. 0.D0) THEN
        xmo_RPORJI =  
     &    ( 3.D0 / 4.D0 / xmo_PIZAHL / xmo_ATETBJ 
     &      * xmo_VTPORJI / xmo_CPORJI )
     &     **(1.D0/3.D0)
      ELSE
        xmo_RPORJI = 0.D0 
      ENDIF
*
* Si le pore inter est trop grand la mod�lisation en tant que pore 
* lenticulaire n'est pas ad�quate : le pore doit alors �tre consid�r� 
* comme rond

      IF ( xmo_RPORJI .GT. xmo_AGG ) THEN
        mo_PORJ_ROND = 1
      ELSE
        mo_PORJ_ROND = 0
      ENDIF
*   
      RETURN
      END
*
*
      SUBROUTINE MORSIM
*     =================
*
*     ******************************************************************
*     MORSIM : MOgador Read des param�tres du mod�le de remise 
*              en solution SIMple
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'cmogppr.inc'
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'
************************************************************************      
* cmogppr.inc : xmo_FACBB en sorties
* credcar.inc : TEXT en entr�e
* crednbr.inc : DFLOT NFIXE en entr�e
* credtxt.inc : NEX en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
      IFIN = 0
*
      DO 10 J = 1,2
           CALL REDLEC(4)
           IF(TEXT.EQ.'FACTEUR_REMISE_EN_SOLUTION_DEPUIS_BULLE')THEN
             CALL REDLEC(2)
             xmo_FACBB = DFLOT
           ELSEIF(TEXT.EQ.'FIN') THEN
                IFIN = 1
                GO TO 9999
           ELSE
             WRITE(NEX,*)'ERREUR DE LECTURE SUR LE MOT :',TEXT,
     +       ' VERIFIER LES MOTS CLE DE LA RUBRIQUE ', 
     +        'PARAMETRE_MODELE_SIMPLE'
             STOP
           ENDIF
10    CONTINUE
*
*     7--0---------0---------0---------0---------0---------0---------0--
 9999 WRITE (NEX,900)
*  
 900  FORMAT (//,40X,'FIN DES PARAMETRES DU MODELE SIMPLE',/)
*
*
*
*
      RETURN
      END
      SUBROUTINE MORSOL(xmo_CBUL,xmo_VTBUL,xmo_VAT,xmo_RBT,mo_BBJT,
     &                  xmo_BREM,xmo_BBREM)
C     =================
C
C
C     ******************************************************************
C     MORSOL : MOgador calcul du coef b de Remise en SOLution du gaz depuis
C              mo_BBJT : 1  bulle intra 
C                      : 2  bulle inter 
C                      : 3  tunnel (inutilise)
C                      : 4  pore intra 
C                      : 5  pore inter
C                      : 6  bulles de rim
C                      : 7  pores de rim 
C              xmo_CBUL : nombre de bulles ou pores par m3
C              xmo_VTBUL : Volume total des bulles ou des pores(m3)
C              xmo_VAT : volume disponible par atome de gaz (m3)
C              xmo_RBT : Rayon de la bulle ou du pore (m)
C              xmo_BREM :  Coefficient de remise en solution du gaz  (s-1) 
C              xmo_BBREM :  Coefficient de remise en solution des bulles 
C                           ou pores  (s-1)                      
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
C
***********************************************************************
* cmogent.inc : xmo_TOFISS en entr�e
* cmogppr.inc : xmo_FACBB  mo_RESOL 
*               xmo_EPECGD xmo_NELSON mo_REM_CAV_RIM xmo_BBJ_S_BB 
*               mo_REM_CAV_RIM mo_REM_PORE_INTER en entr�e 
* cmogphy.inc : xmo_BVDW en entr�e 
************************************************************************
*
      CALL MOMESS('DANS MORSOL')
*
      CALL MODDES(xmo_CBUL,xmo_VAT,xmo_VTBUL,xmo_RBT,xmo_DDESTR)
*
* Si mo_REM_CAV_RIM vaut zero, on ne consid�re pas de remise en solution
* depuis les cavites des zones restructurees. On sort directement du 
* sous-programme
*
      IF (mo_REM_CAV_RIM .EQ. 0) THEN
        IF (mo_BBJT .EQ. 6 .OR. mo_BBJT .EQ. 7) THEN
          xmo_BREM = 0.D0
          xmo_BBREM = 0.D0
          GOTO 500
        ENDIF
      ENDIF
*
* Si mo_REM_PORE_INTER vaut zero, on ne consid�re pas de remise en solution
* depuis les pores inter. On sort directement du 
* sous-programme
*
      IF (mo_REM_PORE_INTER .EQ. 0) THEN
        IF (mo_BBJT .EQ. 5) THEN
          xmo_BREM = 0.D0
          xmo_BBREM = 0.D0
          GOTO 500
        ENDIF
      ENDIF
*
************************************************************************
      IF (mo_RESOL .EQ. 1) THEN
* Mod�le de remise en solution de NELSON
************************************************************************
*
         xmo_NRE = 
     &         1.D0 -
     &         ( max(0.D0 , 1.D0 - xmo_EPECGD * xmo_VAT /
     &                             xmo_RBT / xmo_BVDW ) )**3.D0  
*
*
*
        IF (xmo_NRE .GT. 0) THEN
          xmo_BREM = 
     &     xmo_NELSON * xmo_NRE**(1.D0 - xmo_DDESTR) * xmo_TOFISS
        ELSE
          xmo_BREM = 0.D0
        ENDIF
*
      ENDIF
************************************************************************
************************************************************************
*
*
*
*
*
************************************************************************
      IF (mo_RESOL .EQ. 2) THEN
* Mod�le de remise en solution SIMPLE
************************************************************************
*
        xmo_BREM = xmo_FACBB *  xmo_TOFISS 
      ENDIF
************************************************************************
************************************************************************
*
*
*
*
************************************************************************
      IF (mo_RESOL .EQ. 3) THEN
* Mod�le de remise en solution Nelson sans xmo_NRE
************************************************************************
*
        xmo_BREM = xmo_NELSON *  xmo_TOFISS 
      ENDIF
************************************************************************
************************************************************************
*
* Traitement special pour les bulles et pores inter
*
      IF (mo_BBJT .EQ. 2  .OR.  mo_BBJT .EQ. 5) THEN
        xmo_BREM = xmo_BREM * xmo_BBJ_S_BB
      ENDIF
*
* Dans tous les cas 
*
      xmo_BBREM = xmo_BREM * xmo_DDESTR 
*
 500  CONTINUE
*  
      RETURN
      END
      SUBROUTINE MORVAR
C     =================
C
C
C     ******************************************************************
C     MORVAR : MOgador Read des VARiables n�cessaires au redemarrage 
C              d'un eventuel calcul au temps demand� pour la reprise              
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogmai.inc'
* essaiLOC
      INCLUDE 'cmogloc.inc'
* essaiLOC
C
***********************************************************************
* cmogvar.inc : xmo_VECG xmo_VECD mo_IETCOM mo_IETCOM_APPR xmo_ASEBASC 
*               xmo_EQUADISP3 en sortie
* cmoginf.inc : mo_DEBUG mo_NREPR  xmo_TPSINI mo_NREPRISELU en entr�e
* cmogtem.inc : xmo_TEMPS en sortie 
* cmogsor.inc : xmo_SOMTOFIS3 xmo_GCREA3 xmo_REMISOL3 xmo_SORDIF3
*               xmo_SORBUL3 xmo_SORPOR3 xmo_SORDIFJ3 xmo_SORPERC3
*               xmo_SOREJEC3
*               xmo_TAUXBETAC3 xmo_TAUXSORBUL3
*               xmo_TAUXSORPOR3 xmo_TAUXREMISOL3
*               xmo_TAUXSOURXE3 xmo_TAUXOUTSE3 xmo_TAUXOUTPERC3
*               xmo_TAUXOUTEJEC3 en sortie
* cmogear.inc : xmo_RSAV3 mo_ISAV3 xmo_G_RWORKSAV3 mo_G_IWORKSAV3 mo_G_IEQSAV3 
*               xmo_G_RTOLSAV3 mo_IGTABSAV3 mo_IDTABSAV3 en sortie
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE mo_NBNOZO en entree
* essaiLOC
* cmogloc.inc : xmo_CXERPORLOC03 en entr�e
* essaiLOC
***********************************************************************      
***********************************************************************
*
*
***********************************************************************
      CALL MOMESS('DANS MORVAR')
*

* test decidant de l'impression effective des variables
      DO 10 II = 1,mo_NREPRISELU
*
        READ(mo_NREPR) mo_NTPSLU
*
        DO 20 L = 1,mo_NB_TRAX
          DO 20 IGROB = 1, mo_NB_ZONE(L)
            DO 20 I = 1, mo_NBNOZO(L,IGROB) 
              DO 20 IHETER = 1, mo_NAMAMX
                READ(mo_NREPR) 
     &            (xmo_VECG(L,IGROB,I,IHETER,IVGA,2),IVGA=1,mo_NVGAMX)
                READ(mo_NREPR) 
     &            (xmo_VECD(L,IGROB,I,IHETER,IVDE,2),IVDE=1,mo_NVDEMX)
                READ(mo_NREPR) 
     &            mo_IETCOM(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            mo_IETCOM_APPR(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            mo_PRFOIS3(L,IGROB,I,IHETER)
*
                READ(mo_NREPR)
     &            (xmo_EQUADISP3(L,IGROB,I,IHETER,NPOP),
     &                           NPOP=1,mo_NPOPMX)
*
                READ(mo_NREPR)
     &            (xmo_RSAV3(L,IGROB,I,IHETER,IS),IS=1,240) 
                READ(mo_NREPR)
     &            (mo_ISAV3(L,IGROB,I,IHETER,IS),IS=1,50) 
                READ(mo_NREPR)
     &            (xmo_G_RWORKSAV3(L,IGROB,I,IHETER,IS),
     &             IS=1,mo_LWRMAX)
                READ(mo_NREPR)
     &          (mo_G_IWORKSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_LWIMAX)
                READ(mo_NREPR)
     &            (mo_G_IEQSAV3(L,IGROB,I,IHETER,IS),IS=1,5)
                READ(mo_NREPR)
     &          (xmo_G_RTOLSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NEQMAX)
                READ(mo_NREPR)
     &            (mo_IGTABSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NVGAMX)
                READ(mo_NREPR)
     &            (mo_IDTABSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NVDEMX)
*
* essaiLOC
                READ(mo_NREPR)
     &            (xmo_CXERPORLOC03(L,IGROB,I,IHETER,IVGA),
     &                           IVGA=1,mo_NNSPHMX)
* essaiLOC 
                READ(mo_NREPR) 
     &            xmo_SOMTOFIS3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_ASEBASC(L,IGROB,I,IHETER)
*
                READ(mo_NREPR) 
     &            xmo_GCREA3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_REMISOL3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SORDIF3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SORBUL3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SORPOR3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SORDIFJ3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SORPERC3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_SOREJEC3(L,IGROB,I,IHETER,2)
*
                READ(mo_NREPR) 
     &            xmo_TAUXBETAC3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXREMISOL3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXSOURXE3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXSORBUL3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXSORPOR3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXOUTSE3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,2)
                READ(mo_NREPR) 
     &            xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,2)
 20     CONTINUE
*
*
        IF ( mo_NTPSLU .EQ. mo_NTPSINI ) THEN
          GOTO 100
        ENDIF
*
 10   CONTINUE
*
      WRITE(mo_DEBUG,*)
     &   'temps de reprise non trouv� dans le fichier de reprise' 
      STOP
*
* Sortie de boucle car on vient de lire les grandeurs n�cessaires � 
* la reprise du calcul au temps demand� pour le demarrage de la reprise
*
 100  CONTINUE
*
* initialisation du temps tenant compte de la reprise
      xmo_TEMPS = xmo_TPSINI
*
*
*
      RETURN
      END
      subroutine mosauv (rsav,isav,
     &                 rwork,rworksav,
     &                 iwork,iworksav,
     &                 neq,lrw,liw,neqg,neqd,ieqsav,
     &                 rtol,rtolsav,
     &                 igtab,igtabsav,
     &                 idtab,idtabsav,
     &                 job)
*
      integer illin, init, lyh, lewt, lacor, lsavf, lwm, liwm,
     1   mxstep, mxhnil, nhnil, ntrep, nslast, nyh, iowns
      integer icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     1   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer insufr, insufi, ixpr, iowns2, jtyp, mused, mxordn, mxords
      integer len1, len1c, len1n, len1s, len2, leniwc,
     1   lenrwc, lenrwn, lenrws
      integer lunit, mesflg
      double precision rowns,
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision tsw, rowns2, pdnorm
*
      double precision rsav(240)
      integer isav(50)
*
      double precision  rwork(*), rworksav(*), rtol(*), rtolsav(*)
      integer iwork(*), iworksav(*), igtab(*), igtabsav(*)
      integer idtab(*), idtabsav(*)
      integer ieqsav(5)
*
*
c-----------------------------------------------------------------------
c the following two internal common blocks contain
c (a) variables which are local to any subroutine but whose values must
c     be preserved between calls to the routine (own variables), and
c (b) variables which are communicated between subroutines.
c the structure of each block is as follows..  all real variables are
c listed first, followed by all integers.  within each type, the
c variables are grouped with those local to subroutine lsoda first,
c then those local to subroutine stoda, and finally those used
c for communication.  the block ls0001 is declared in subroutines
c lsoda, intdy, stoda, prja, and solsy.  the block lsa001 is declared
c in subroutines lsoda, stoda, and prja.  groups of variables are
c replaced by dummy arrays in the common declarations in routines
c where those variables are not used.
c-----------------------------------------------------------------------
c       mesflg = print control flag..
c                1 means print all messages (the default).
c                0 means no printing.
c       lunit  = logical unit number for messages.
c                the default is 6 (machine-dependent).
c-----------------------------------------------------------------------
c-----------------------------------------------------------------------
      common /ls0001/ rowns(209),
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround,
     2   illin, init, lyh, lewt, lacor, lsavf, lwm, liwm,
     3   mxstep, mxhnil, nhnil, ntrep, nslast, nyh, iowns(6),
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
*
      common /lsa001/ tsw, rowns2(20), pdnorm,
     1   insufr, insufi, ixpr, iowns2(2), jtyp, mused, mxordn, mxords
*
      common /eh0001/ mesflg, lunit
*
***********************************************************************
      if (job .eq.1) then
***********************************************************************
      do i = 1, 209
        rsav(i) = rowns(i)
      enddo
*
      rsav(210) = ccmax
      rsav(211) = el0
      rsav(212) = h
      rsav(213) = hmin
      rsav(214) = hmxi
      rsav(215) = hu
      rsav(216) = rc
      rsav(217) = tn
      rsav(218) = uround
      rsav(219) = tsw
*
      do i = 1, 20
        rsav(219+i) = rowns2(i)
      enddo
*
      rsav(240) = pdnorm
*
      isav(1) = illin
      isav(2) = init
      isav(3) = lyh
      isav(4) = lewt
      isav(5) = lacor
      isav(6) = lsavf
      isav(7) = lwm
      isav(8) = liwm
      isav(9) = mxstep
      isav(10) = mxhnil
      isav(11) = nhnil
      isav(12) = ntrep
      isav(13) = nslast
      isav(14) = nyh
*
      do i = 1, 6
        isav(14+i) = iowns(i)
      enddo
*
      isav(21) = icf
      isav(22) = ierpj
      isav(23) = iersl
      isav(24) = jcur
      isav(25) = jstart
      isav(26) = kflag
      isav(27) = l
      isav(28) = meth
      isav(29) = miter
      isav(30) = maxord
      isav(31) = maxcor
      isav(32) = msbp
      isav(33) = mxncf
      isav(34) = n
      isav(35) = nq
      isav(36) = nst
      isav(37) = nfe
      isav(38) = nje
      isav(39) = nqu
      isav(40) = insufr
      isav(41) = insufi
      isav(42) = ixpr
      isav(43) = iowns2(1)
      isav(44) = iowns2(2)
      isav(45) = jtyp
      isav(46) = mused
      isav(47) = mxordn
      isav(48) = mxords
      isav(49) = mesflg
      isav(50) = lunit
*
      ieqsav(1) = neq
      ieqsav(2) = lrw
      ieqsav(3) = liw
      ieqsav(4) = neqg
      ieqsav(5) = neqd
*
      do i = 1, lrw
        rworksav(i) = rwork(i)
      enddo
*
      do i = 1, liw
        iworksav(i) = iwork(i)
      enddo
*
      do i = 1, neq
        rtolsav(i) = rtol(i)
      enddo
*
      do i = 1, neqg
        igtabsav(i) = igtab(i)
      enddo
*
      do i = 1, neqd
        idtabsav(i) = idtab(i)
      enddo
*
***********************************************************************
      endif
***********************************************************************
*
************************************************************************
      if (job .eq. 2) then
***********************************************************************
      do i = 1, 209
        rowns(i) = rsav(i)
      enddo
*
      ccmax =rsav(210)
      el0 =  rsav(211)
      h =  rsav(212)
      hmin =  rsav(213)
      hmxi = rsav(214)
      hu = rsav(215)
      rc = rsav(216)
      tn = rsav(217)
      uround = rsav(218)
      tsw = rsav(219)
*
      do i = 1, 20
        rowns2(i) = rsav(219+i)
      enddo
*
      pdnorm = rsav(240)
*
      illin = isav(1)
      init = isav(2)
      lyh = isav(3)
      lewt = isav(4)
      lacor = isav(5)
      lsavf = isav(6)
      lwm = isav(7)
      liwm = isav(8)
      mxstep = isav(9)
      mxhnil = isav(10)
      nhnil = isav(11)
      ntrep = isav(12)
      nslast = isav(13)
      nyh = isav(14)
*
      do i = 1, 6
        iowns(i) = isav(14+i)
      enddo
*
      icf = isav(21)
      ierpj = isav(22)
      iersl = isav(23)
      jcur = isav(24)
      jstart = isav(25)
      kflag = isav(26)
      l = isav(27)
      meth = isav(28)
      miter = isav(29)
      maxord = isav(30)
      maxcor = isav(31)
      msbp = isav(32)
      mxncf = isav(33)
      n = isav(34)
      nq = isav(35)
      nst = isav(36)
      nfe = isav(37)
      nje = isav(38)
      nqu = isav(39)
      insufr = isav(40)
      insufi = isav(41)
      ixpr = isav(42)
      iowns2(1) = isav(43)
      iowns2(2) =  isav(44)
      jtyp = isav(45)
      mused = isav(46)
      mxordn = isav(47)
      mxords = isav(48)
      mesflg = isav(49)
      lunit = isav(50)
*
      neq = ieqsav(1)
      lrw = ieqsav(2)
      liw = ieqsav(3)
      neqg = ieqsav(4)
      neqd = ieqsav(5)
*
      do i = 1, lrw
        rwork(i) = rworksav(i) 
      enddo
*
      do i = 1, liw
        iwork(i) = iworksav(i)
      enddo
*
      do i = 1, neq
        rtol(i) = rtolsav(i)
      enddo
*
      do i = 1, neqg
        igtab(i) = igtabsav(i)
      enddo
*
      do i = 1, neqd
        idtab(i) = idtabsav(i)
      enddo
*
***********************************************************************
      endif
***********************************************************************
*
*
      return
c----------------------- end of subroutine mosauv ---------------------
      end
      SUBROUTINE MOTERM(
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
* essaiLOC
     &      xmo_TEMPS_COURANT,
* essaiLOC
     &
     &      xmo_XE_NUCLHOMO,
     &      xmo_XE_PIEGBUL,xmo_XE_PIEGPOR,xmo_XE_CREATION,
     &      xmo_XE_REMJOINT,xmo_XE_REMBUL,xmo_XE_REMPOR,
     &      xmo_XE_DIFFUSION,xmo_XE_SORTIEGRAIN,xmo_XE_CLIM,
     &      xmo_XERBLOC_EVOL,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,xmo_B_COALEVB,xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,xmo_XEB_PIEGBPOR,xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,xmo_VTB_GAZ,xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,xmo_POR_COALEVPOR,xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,xmo_XEPOR_PIEGBPOR,xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,xmo_VTPOR_GAZ,xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,xmo_VTPOR_SORTIEGRAIN,
     &      xmo_CXECR  )
C     =================
C
C
C     ******************************************************************
C     MOTERM : MOgador calcul des TERMes utiles dans l'expression des d�riv�es
C      (�quations concernant les variables intra granulaires)
C      dans l'�tat classique ou tunnel                                   
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmogvar.inc'
C
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
C
***********************************************************************
* cmogpas.inc : xmo_KG  xmo_BETAC xmo_BJ xmo_OMSKT xmo_PVI  
*               xmo_FACRHO xmo_DISTCOAL en entr�e
* cmogppr.inc : xmo_FMIGR xmo_FMIGRPOR xmo_ZETA xmo_MN xmo_DREM 
*               xmo_RHODL xmo_RHOLIM xmo_MARGRHO_FR 
*               xmo_BBJ_S_BB mo_MODELE_FRAC en entr�e
* cmogphy.inc : xmo_PIZAHL xmo_XNAVOG  xmo_OMEGA en entr�e
* cmogmat.inc : xmo_TVGG xmo_TMGG    
*               xmo_TVSEQ  
*               xmo_TIMGG xmo_TIMRGG  
*               xmo_TVSEQR  en entr�e
* cmogdon.inc : xmo_AGG  en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR xmo_RGG en entr�e
* cmogent.inc : xmo_CXEJLIM  xmo_PHYDR en entr�e
* cmogpte.inc : xmo_TEST2 xmo_TEST3 en entr�e
* cmogvar.inc : mo_IET en entr�e
************************************************************************
* entr�es et sorties en param�tres
* premier bloc de param�tres  : entr�es
*
* -->Pour les �tats classique (0) et tunnel (1)
* Il s'agit - des variables intra (gaz dissous, bulles et pores), 
*           - des variables inter (parce qu'on en a besoin pour la remise en solution du joint 
*                                vers le grain) 
*           - de la densit� de dislocation
*
* -->Pour l �tat 'en restructuration' (2) 
* Il s'agit - des variables zones saines (gaz dissous, bulles et pores), 
*           - des variables zones restructur�es (gaz ,bulles nano, pores de rim) 
*           (parce qu'on a besoin du gaz dissous et des pores de rim 
*            pourla remise en solution 
*            qui joue en d�but de restructuration surtout) 
*           - de la densit� de dislocation
*
* deuxi�me bloc de param�tres : sorties
* Il s'agit de tous les termes apparaissant dans les �quations des variables intra
* remarque g�n�rale : les termes des �quations calcul�s ci-dessous incluent 
* toujours leur signe. Autrement dit, dans les �quations , on aura que des +
* entre ces termes.
*
* pour equation du gaz dissous intra :
* xmo_XE_NUCLHOMO : Vecteur terme de nucl�ation homog�ne dans l'�quation 
*                   du gaz dissous intra (mol/s/m3)
* xmo_XE_PIEGBUL : Vecteur terme de piegeage du gaz dissous par les bulles (mol/s/m3) 
* xmo_XE_PIEGPOR : Vecteur terme de piegeage du gaz dissous par les pores (mol/s/m3)
* xmo_XE_CREATION : Vecteur terme de cr�ation du gaz dissous par les fissions (mol/s/m3)
* xmo_XE_REMJOINT : Vecteur terme de remise en solution de gaz depuis le joint (mol/s/m3)
* xmo_XE_REMBUL : Vecteur terme de remise en solution de gaz depuis les bulles intra (mol/s/m3)
* xmo_XE_REMPOR : Vecteur terme de remise en solution de gaz depuis les pores intra (mol/s/m3)
* xmo_XE_DIFFUSION : Vecteur terme de diffusion de gaz dissous (mol/s/m3)
* xmo_XE_SORTIEGRAIN : Vecteur terme de passage de gaz dissous des zones saines
*                      aux zones restructur�es (mol/s/m3). Non nul uniquement si 
*                      on se trouve dans l'�tat 'en restructuration' (2)
*
* pour equation du nombre de bulles intra/m3 :
* xmo_B_NUCLHOMO : terme de nucl�ation homog�ne dans l'�quation 
*                   des bulles intra (bulles/s/m3)
* xmo_B_COALEDB : terme de coalescence en mouvement al�atoire (bulles/s/m3)
* xmo_B_COALEVB : terme de coalescence en mouvement sous gradient thermique (bulles/s/m3) 
* xmo_B_COALEGEOM : terme de coalescence g�om�trique. Quand les bulles sont tr�s nombreuses
*                   toute nouvelle bulle coalesce immediatement avec une bulle voisine (bulles/s/m3)
* xmo_B_PIEGBPOR : terme de piegeage des bulles intra par les pores (bulles/s/m3)
* xmo_B_REMBUL : terme de destruction des bulles intra par les pointes de fission (bulles/s/m3)
* xmo_B_SORTIEGRAIN : terme de perte de bulles intra par sortie des bulles hors du grain (bulles/s/m3) 
*
* pour equation du gaz dans les bulles intra/m3 :
* xmo_XEB_NUCLHOMO : terme de nucl�ation homog�ne dans l'�quation 
*                   du gaz des bulles intra (mol/s/m3)
* xmo_XEB_PIEGBUL : terme de piegeage du gaz dissous par les bulles (mol/s/m3) 
* xmo_XEB_PIEGBPOR : terme li� au piegeage des bulles intra par les pores (mol/s/m3)
* xmo_XEB_REMBUL : terme de remise en solution du gaz des bulles intra par les 
*                  pointes de fission, que la bulle soit d�truite ou non (mol/s/m3)
* xmo_XEB_SORTIEGRAIN : terme de perte de gaz de bulles intra par sortie des bulles 
*                       hors du grain (mol/s/m3)
*
* pour equation du volume total des bulles intra/m3 :
* xmo_VTB_LACUNE : terme d'apport de volume par les lacunes (m3/s/m3)
* xmo_VTB_GAZ :  terme d'apport de volume li� aux atomes de gaz �chang�s avec 
*                le r�seau cristallin (m3/s/m3)
* xmo_VTB_PIEGBPOR : terme de perte de volume li� au piegeage des bulles intra 
*                    par les pores (m3/s/m3)
* xmo_VTB_REMBUL : terme de perte de volume li� � la destruction des bulles intra par les 
*                  pointes de fission (m3/s/m3)
* xmo_VTB_SORTIEGRAIN  : terme de perte de volume li� � la sortie des bulles 
*                       hors du grain (m3/s/m3)
*
* pour equation du nombre de pores intra/m3 :
* xmo_POR_COALEDPOR : terme de coalescence en mouvement al�atoire (pores/s/m3)
* xmo_POR_COALEVPOR : terme de coalescence en mouvement sous gradient thermique (pores/s/m3) 
* xmo_POR_REMPOR : terme de destruction des pores intra par les pointes de fission (pores/s/m3)
* xmo_POR_SORTIEGRAIN : terme de perte de pores intra par sortie des pores hors du grain (pores/s/m3) 
*
* pour equation du gaz dans les pores intra/m3 :
* xmo_XEPOR_PIEGPOR : terme de piegeage du gaz dissous par les pores (mol/s/m3) 
* xmo_XEPOR_PIEGBPOR : terme li� au piegeage des bulles intra par les pores (mol/s/m3)
* xmo_XEPOR_REMPOR : terme de remise en solution du gaz des pores intra par les 
*                  pointes de fission, que la pore soit d�truite ou non (mol/s/m3)
* xmo_XEPOR_SORTIEGRAIN : terme de perte de gaz de pores intra par sortie des pores 
*                       hors du grain (mol/s/m3)
*
* pour equation du volume total des pores intra/m3 :
* xmo_VTPOR_LACUNE : terme d'apport de volume par les lacunes (m3/s/m3)
* xmo_VTPOR_GAZ :  terme d'apport de volume li� aux atomes de gaz �chang�s avec 
*                le r�seau cristallin (m3/s/m3)
* xmo_VTPOR_PIEGBPOR : terme d'apport de volume li� au piegeage des bulles intra 
*                    par les pores (m3/s/m3)
* xmo_VTPOR_REMPOR : terme de perte de volume li� � la destruction des pores intra par les 
*                  pointes de fission (m3/s/m3)
* xmo_VTPOR_SORTIEGRAIN  : terme de perte de volume li� � la sortie des pores 
*                       hors du grain (m3/s/m3)
*
************************************************************************
* Variables internes utilis�es dont la signification physique est importante :
* xmo_PIEGB : fr�quence de pi�geage de gaz intra dissous par les bulles intra (s-1)
* xmo_PIEGPOR : fr�quence de pi�geage de gaz intra dissous par les pores intra (s-1)
* xmo_BB : fr�quence de remise en solution du gaz des bulles intra (s-1)
* xmo_BPOR : fr�quence de remise en solution du gaz des pores intra (s-1)
* xmo_CXEJM : moyenne du gaz dissous inter (mol/m3)
* xmo_BBJ : fr�quence de remise en solution du gaz des bulles inter (s-1) 
* xmo_BPORJ : fr�quence de remise en solution du gaz des pores inter (s-1) 
* xmo_DXEAM : coefficient de diffusion du gaz en volume, en tenant 
*             compte de la pr�sence de dislocations (m2/s)
* xmo_CXEM : moyenne du gaz dissous intra (mol/m3)
* xmo_CXECM : moyenne du carr� du gaz dissous intra (mol2/m6)
* xmo_GCDB : Coeff apparaissant dans le terme de coalescence des bulles intra (m3/s)
*            en mouvement al�atoire
* xmo_GCVB : Coeff apparaissant dans le terme de coalescence des bulles intra (m3/s)
*            en mouvement sous gradient thermique
* xmo_HJ : Fr�quence de sortie des bulles intra du grain (s-1)
* xmo_PIEGBPOR : fr�quence de pi�geage des bulles intra par les pores intra (s-1)
* xmo_BBB : fr�quence de destruction des bulles intra par les pointes de fission (s-1)
* xmo_TEXP2B : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � une bulle intra ()
* xmo_GCPORDPOR : Coeff apparaissant dans le terme de coalescence des pores intra (m3/s)
*            en mouvement al�atoire
* xmo_GCPORVPOR : Coeff apparaissant dans le terme de coalescence des pores intra (m3/s)
*            en mouvement sous gradient thermique
* xmo_HJPOR : Fr�quence de sortie des pores intra du grain (s-1)
* xmo_BPOR : fr�quence de remise en solution du gaz des pores intra (s-1)
* xmo_BPORB : fr�quence de destruction des pores intra par les pointes de fission (s-1)
* xmo_TEXP2POR : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � un pore intra ()
* xmo_TEXP2PORJ : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � un pore inter ()                 
************************************************************************
      DOUBLE PRECISION 
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_VECT1(mo_NNSPHMX),
     &                 xmo_XE_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XE_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XE_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XE_CREATION(mo_NNSPHMX),
     &                 xmo_XE_REMJOINT(mo_NNSPHMX),
     &                 xmo_XE_REMBUL(mo_NNSPHMX),
     &                 xmo_XE_REMPOR(mo_NNSPHMX),
     &                 xmo_XE_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XE_CLIM(mo_NNSPHMX),
     &                 xmo_XE_SORTIEGRAIN(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXERPORLOC(mo_NNSPHMX),
     &                 xmo_XERBLOC_EVOL(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOTERM')
*
CDEBUG
CDEBUG        IF (xmo_TEMPS_COURANT .GT. 0.73054280E+08) mo_FLAG = 1 
CDEBUG       mo_FLAG = 1
CDEBUG
************************************************************************
************************************************************************
* INTRA POUR ETAT CLASSIQUE (0) ou ETAT TUNNEL (1)
* ou ZONE SAINE POUR ETAT 'EN RESTRUCTURATION' (2)
************************************************************************
************************************************************************
*
*
************************************************************************
* A FAIRE POUR l ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
        IF (mo_IET .EQ. 2) THEN
*       =======================
*
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM + xmo_RHODL - xmo_MARGRHO_FR pour les calcul de xmo_FR xmo_A 
* Sinon, on a des divisions par zero
*
          xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
          xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
*
*
* Calcul de la d�riv�e temporelle de FR : xmo_DFRSDT
          IF ( xmo_RHOD .LT. xmo_RHOD_FRLIM ) THEN
            IF (mo_MODELE_FRAC .EQ. 1) THEN
              xmo_DFRSDT = xmo_PIZAHL / 4.D0 / xmo_RHODL *
     &          cos( xmo_PIZAHL / 2.D0 / xmo_RHODL *
     &          ( xmo_RHOLIM - xmo_RHOD1 ) ) * xmo_FACRHO * xmo_RHOD1
            ELSEIF (mo_MODELE_FRAC .EQ. 2) THEN
              xmo_DFRSDT = xmo_FACRHO * xmo_RHOD1
     &                     / 2.D0 / xmo_RHODL
            ENDIF
          ELSE
            xmo_DFRSDT = 0.D0
          ENDIF
*
        ELSE
*       ====
          xmo_FR = 0.D0
          xmo_DFRSDT = 0.D0
        ENDIF
*       =====
************************************************************************
* FIN DES ACTIONS A FAIRE POUR L'ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
*
*
* coefficient de diffusion du Xe tenant compte de la pr�sence de 
* dislocations
*
        CALL MODXEA(xmo_RHOD,xmo_DXEAM)
*
************************************************************************
*
* vitesse des bulles intra xmo_VB (m/s)
* coefficient de diffusion des bulles intra xmo_DB (m2/s)
*
       CALL MOBUPO(xmo_CXEB,xmo_CB,xmo_RB,xmo_DXEAM,xmo_TEST3,
     &             xmo_VB,xmo_DB)
*
************************************************************************
* 
* Calcul de delta Nucl xmo_DNUCL
*  
        CALL MODNUC(xmo_CB, xmo_CXEB, xmo_RB, xmo_DNUCL)
*
*
************************************************************************
* vitesse des pores intra xmo_VPOR (m/s)
* coefficient de diffusion des pores intra xmo_DPOR (m2/s)
*
       CALL MOBUPO(xmo_CXEPOR,xmo_CPOR,xmo_RPOR,xmo_DXEAM,xmo_TEST2,
     &             xmo_VPOR,xmo_DPOR)
*
************************************************************************
* fr�quence de pi�geage des bulles intra par les pores intra en mouvement (s-1)
       xmo_PIEGBPOR = 
     &       ( 4.D0 * xmo_PIZAHL * (xmo_RB + xmo_RPOR) * 
     &         (xmo_DB + xmo_DPOR)
     &          +
     &         xmo_PIZAHL * abs(xmo_VPOR - xmo_VB) * 
     &         (xmo_RB + xmo_RPOR)**2.D0    ) * xmo_CPOR
************************************************************************
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz intra dissous 
*
        CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &               mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
* Calcul de la moyenne xmo_CXECM du carr� des concentrations 
* en gaz intra dissous
*
        CALL MOMOYC_VF (xmo_CXE, xmo_TMGG,
     &                   mo_NGG,xmo_CXECM)
*
************************************************************************
*
* Calcul de la moyenne xmo_CXERBLOCM des concentrations en 
* gaz intra dissous au voisinage des bulles intra
*
        IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
          CALL MOMOYE (xmo_CXERBLOC, xmo_TVGG,
     &               mo_NGG, mo_NNSPHMX, xmo_CXERBLOCM)
        ENDIF
*
************************************************************************
* Coalescence des bulles intra : xmo_GCDB en mouvement al�atoire
*                                xmo_GCVB en mouvement sous gradient thermique
*
        xmo_GCDB = 8.D0 * xmo_PIZAHL * xmo_RB * xmo_DB 
        xmo_GCVB = 
     &           4.D0 * xmo_PIZAHL * xmo_RB**2.D0 
     &           * xmo_VB * xmo_FMIGR
*
************************************************************************
* Fr�quence de disparition des bulles intra au joint de grain xmo_HJ
*
        xmo_HJ = 3.D0 * xmo_VB / 4.D0 / xmo_AGG  +
     &           15.D0 * xmo_DB / xmo_AGG**2.D0
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BB
* et xmo_BBB des bulles intra
*
*
        CALL MOBLOC(xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,1,
     &     xmo_BB,xmo_BBB,xmo_TEXP2B)
*
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BPOR
* et xmo_BPORB des pores intra
* 
*
*
        CALL MOBLOC(xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,4,
     &     xmo_BPOR,xmo_BPORB,xmo_TEXP2POR)
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BBJ des bulles inter
*
         CALL MOBLOC(xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &   2,xmo_BBJ,xmo_BBJB,xmo_TEXP2BJ)
*
*
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BPORJ
* et xmo_BPORJB des pores inter
*
*
*
        CALL MOBLOC(xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &  5,xmo_BPORJ,xmo_BPORJB,xmo_TEXP2PORJ)
*
*
************************************************************************
* Calcul des termes intervenant dans le piegeage par les bulles 
* et les pores intra
*
* essaiLOC
CDEBUG
CDEBUG        WRITE(20,*) 'dans moterm avnt mocloc  xmo_CPOR = ',xmo_CPOR
CDEBUG        WRITE(20,*) 'dans moterm avnt mocloc  xmo_RPOR = ',xmo_RPOR
CDEBUG        WRITE(20,*) 'dans moterm avnt mocloc  xmo_TK = ',xmo_TK
CDEBUG
        CALL MOCLOC(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_RHOD,xmo_TEMPS_COURANT,
     &
     &      xmo_CXERPORLOC,xmo_CXERPORLOCM,
     &      xmo_FREQ1_CXERBLOC,xmo_FREQ2_CXERBLOC,xmo_FREQ3_CXERBLOC)
*
************************************************************************
* Dans le cas o� xmo_CXERBLOC n'est pas calcul� par une �quation 
* diff�rentielle, il est calcul� ici
        IF (mo_EQUA_CXERBLOC .EQ. 0) THEN
          IF (xmo_CXEM .GT. 0.D0 .AND. xmo_CB .GT. 0) THEN
            DO IVGA = 1, mo_NGG
              xmo_CXERBLOC(IVGA) = 
     &        (   xmo_FREQ1_CXERBLOC * xmo_CXE(IVGA) 
     &            / (1.D0 - xmo_FR) 
     &          + xmo_FREQ3_CXERBLOC * xmo_CXEB 
     &            * xmo_CXE(IVGA) / xmo_CXEM
     &          + xmo_BETAC   )
     &        /
     &        ( xmo_FREQ1_CXERBLOC + xmo_FREQ2_CXERBLOC + xmo_BBB )
            ENDDO
            CALL MOMOYE (xmo_CXERBLOC, xmo_TVGG,
     &               mo_NGG, mo_NNSPHMX, xmo_CXERBLOCM)
          ELSE
            DO IVGA = 1, mo_NGG
              xmo_CXERBLOC(IVGA) = 0.D0
            ENDDO
            xmo_CXERBLOCM = 0.D0
          ENDIF         
        ENDIF
*
************************************************************************
        xmo_PIEGB = 
     &        4.D0 * xmo_PIZAHL * xmo_RB**2.D0 * xmo_DXEAM
     &        * 2.D0 / xmo_DREM * xmo_CB 
        xmo_PIEGPOR = 
     &        4.D0 * xmo_PIZAHL * xmo_RPOR**2.D0 * xmo_DXEAM
     &        * 2.D0 / xmo_DREM * xmo_CPOR 
* essaiLOC
************************************************************************
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
          xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
************************************************************************
* Coalescence des pores intra xmo_GCPOR
*
        xmo_GCPORDPOR = 8.D0 * xmo_PIZAHL * xmo_RPOR * xmo_DPOR 
        xmo_GCPORVPOR = 
     &           4.D0 * xmo_PIZAHL * xmo_RPOR**2.D0 * xmo_VPOR 
     &           * xmo_FMIGRPOR

*
************************************************************************
* Fr�quence de disparition des pores intra au joint de grain xmo_HJPOR
*
        xmo_HJPOR = 3.D0 * xmo_VPOR / 4.D0 / xmo_AGG  +
     &              15.D0 * xmo_DPOR / xmo_AGG**2.D0
*
*
*
************************************************************************
*
* Calcul du rayon de la sph�re �quivalente pour le rim xmo_ASERIM
*
        IF (mo_IET .EQ. 2) THEN
          CALL MOASER(xmo_FR)
        ENDIF
*
************************************************************************
* Calcul des taux de couverture
*
        CALL MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,xmo_FR,
     &                  xmo_FBJ,xmo_FPORJ,xmo_FEXT)
*
************************************************************************
*  Calcul des concentrations de gaz � la limite grain en face des 
*  diff�rentes populations
*
          xmo_CXENGG = xmo_CXE(mo_NGG)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                xmo_CXEJM, xmo_BJ, 
     &                1.D0 - xmo_FR - xmo_FBJ - xmo_FPORJ - xmo_FEXT,
     &                xmo_CXEBJ, xmo_BBJB, 
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_J)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                xmo_CXEBJ, xmo_BBJ-xmo_BBJB, 
     &                xmo_FBJ,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_BJ)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                xmo_CXEPORJ, xmo_BPORJ, 
     &                xmo_FPORJ,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_PORJ)
*
* pour ejection
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                0.D0,0.D0, 
     &                xmo_FEXT,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_EXT)
* pour ejection
*
          xmo_CXECR = 
     &         (1.D0 - xmo_FR - xmo_FEXT - xmo_FBJ - xmo_FPORJ) 
     &            * xmo_CXECR_J 
     &         + xmo_FBJ * xmo_CXECR_BJ 
     &         + xmo_FPORJ * xmo_CXECR_PORJ 
     &         + xmo_FEXT * xmo_CXECR_EXT
*
*
************************************************************************
************************************************************************
************************************************************************
* TERMES POUR EQUATIONS INTRA
************************************************************************
************************************************************************
* calculs des termes de l'�quation du gaz dissous intra
*
        CALL XMMVEC_TRIDIAG (mo_NGG,mo_NGG,mo_NNSPHMX,mo_NNSPHMX, 
     &               xmo_TIMRGG,xmo_CXE,xmo_VECT1)
*
        DO 100 IVGA = 1, mo_NGG
          xmo_XE_NUCLHOMO(IVGA) =
     &         - xmo_KG * xmo_DNUCL * xmo_CXE(IVGA) * xmo_CXE(IVGA)
     &           / (1.D0-xmo_FR)
*
* essaiLOC
           xmo_XE_PIEGBUL(IVGA) =  
     &                  - xmo_PIEGB * xmo_CXERBLOC(IVGA)
*
           xmo_XE_PIEGPOR(IVGA) = 
     &                  - xmo_PIEGPOR * xmo_CXERPORLOC(IVGA)
* essaiLOC 

          xmo_XE_CREATION(IVGA) =
     &           xmo_BETAC * (1.D0-xmo_FR) 
*
          xmo_XE_CLIM(IVGA) =
     &          - xmo_DXEAM / xmo_AGG**2.D0 
     &            * xmo_CXECR * xmo_TIMQGG(IVGA)
*
          xmo_XE_REMJOINT(IVGA) = 0.D0
*
          IF (xmo_CXEM .GT. 0.D0) THEN
            xmo_XE_REMBUL(IVGA) =
     &         xmo_BB * xmo_CXEB * xmo_CXE(IVGA) / xmo_CXEM 
          ELSE
            xmo_XE_REMBUL(IVGA) = xmo_BB * xmo_CXEB
          ENDIF
*
          IF (xmo_CXEM .GT. 0.D0) THEN
            xmo_XE_REMPOR(IVGA) =
     &       xmo_BPOR * xmo_CXEPOR * xmo_CXE(IVGA) / xmo_CXEM 
          ELSE
            xmo_XE_REMPOR(IVGA) = xmo_BPOR * xmo_CXEPOR
          ENDIF
*
          xmo_XE_DIFFUSION(IVGA) =
     &          - xmo_DXEAM / xmo_AGG**2.D0 * xmo_VECT1(IVGA)
*
          IF (mo_IET .NE. 2) THEN
            xmo_XE_SORTIEGRAIN(IVGA) = 0.D0
          ELSE
            xmo_XE_SORTIEGRAIN(IVGA) = 
     &        - xmo_DFRSDT *  xmo_CXE(IVGA)  / (1.D0-xmo_FR)
          ENDIF 
*
 100    CONTINUE
*
*
************************************************************************
* calcul des termes pour l'�quation de la concentration en gaz dissous 
* au voisinnage des bulles. On calcule xmo_CXERBLOC uniquement parce
* qu'il intervient dans le pi�geage du gaz dissous par les bulles
*
        IF (mo_EQUA_CXERBLOC .EQ. 1) THEN
          IF (xmo_CXEM .GT. 0.D0) THEN
            DO IVGA = 1, mo_NGG
              xmo_XERBLOC_EVOL(IVGA) =
     &         xmo_FREQ1_CXERBLOC * xmo_CXE(IVGA) / (1.D0 - xmo_FR) 
     &         +
     &         xmo_FREQ3_CXERBLOC * xmo_CXEB 
     &           * xmo_CXE(IVGA) / xmo_CXEM
     &         +
     &         xmo_BETAC 
     &         -
     &         (xmo_FREQ1_CXERBLOC + xmo_FREQ2_CXERBLOC + xmo_BBB)
     &         * xmo_CXERBLOC(IVGA)
            ENDDO
          ELSE
            DO IVGA = 1, mo_NGG
              xmo_XERBLOC_EVOL(IVGA) = 0.D0
            ENDDO
          ENDIF
        ENDIF
CDEBUG
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI 
CDEBUG     &          .AND. mo_FLAG .EQ. 1) THEN
CDEBUG
CDEBUG*
CDEBUG             WRITE(20,*) 'dans moterm terme de xmo_XERBLOC_EVOL'
CDEBUG             WRITE(20,*) 'xmo_FREQ1_CXERBLOC =',xmo_FREQ1_CXERBLOC
CDEBUG             WRITE(20,*) 'xmo_FREQ2_CXERBLOC =',xmo_FREQ2_CXERBLOC
CDEBUG             WRITE(20,*) 'xmo_FREQ3_CXERBLOC =',xmo_FREQ3_CXERBLOC
CDEBUG             WRITE(20,*) 'term1 ='
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_FREQ1_CXERBLOC * xmo_CXE(IVGA) / (1.D0 - xmo_FR),
CDEBUG     &        IVGA = 1,mo_NGG) 
CDEBUG             WRITE(20,*) 'term2 ='
CDEBUG             WRITE(20,*) 
CDEBUG     &       (xmo_FREQ3_CXERBLOC * xmo_CXEB , IVGA = 1,mo_NGG)
CDEBUG             WRITE(20,*) 'term3 ='
CDEBUG             WRITE(20,*)  
CDEBUG     &       (-
CDEBUG     &       (xmo_FREQ1_CXERBLOC + xmo_FREQ2_CXERBLOC + xmo_BBB)
CDEBUG     &       * xmo_CXERBLOC(IVGA),IVGA = 1,mo_NGG)
CDEBUG           ENDIF
CDEBUG 
************************************************************************
* calcul des termes pour l'�quation de la concentration de bulles intra xmo_CB
*
        xmo_B_NUCLHOMO = 
     &      xmo_KG * xmo_DNUCL * xmo_CXECM / xmo_MN  / (1.D0-xmo_FR)
        xmo_B_COALEDB =
     &      - xmo_GCDB * xmo_CB**2.D0  / (1.D0-xmo_FR)
        xmo_B_COALEVB =
     &      - xmo_GCVB * xmo_CB**2.D0  / (1.D0-xmo_FR)
*
        IF (xmo_CB .GT. 0.D0) THEN
          xmo_B_COALEGEOM =
     &     - xmo_B_NUCLHOMO *
     &       (2.D0 * xmo_RB + xmo_DISTCOAL) * 
*essaiDISTVB     &       ( xmo_CB / (1.D0 - xmo_FR) )**(1.D0/3.D0)
     &       ( xmo_CB / (1.D0 - xmo_FR + xmo_VTB) )**(1.D0/3.D0)
        ELSE
          xmo_B_COALEGEOM =0 .D0
        ENDIF
*
        xmo_B_PIEGBPOR =
     &      - xmo_PIEGBPOR * xmo_CB  / (1.D0-xmo_FR)
        xmo_B_REMBUL =
     &      - xmo_BBB * xmo_CB 
*
        IF (mo_IET .NE. 2) THEN
          xmo_B_SORTIEGRAIN = - xmo_HJ * xmo_CB
        ELSE
          xmo_B_SORTIEGRAIN = - xmo_DFRSDT *  xmo_CB  / (1.D0-xmo_FR)
        ENDIF 
*
*
************************************************************************
* d�riv�e temporelle de la concentration du gaz de bulles intra xmo_CXEB
*
        xmo_XEB_NUCLHOMO =
     &      xmo_KG * xmo_DNUCL * xmo_CXECM  / (1.D0-xmo_FR)        
* essaiLOC
        xmo_XEB_PIEGBUL =
     &      xmo_PIEGB * xmo_CXERBLOCM
* essaiLOC
*
        xmo_XEB_PIEGBPOR =
     &      - xmo_PIEGBPOR * xmo_CXEB / (1.D0-xmo_FR)
*
        xmo_XEB_REMBUL =
     &      - xmo_BB * xmo_CXEB
*
        IF (mo_IET .NE. 2) THEN
          xmo_XEB_SORTIEGRAIN = - xmo_HJ * xmo_CXEB
        ELSE
          xmo_XEB_SORTIEGRAIN = 
     &       - xmo_DFRSDT *  xmo_CXEB  / (1.D0-xmo_FR)
        ENDIF 
*
************************************************************************
* Application de l'effet cliquet sur les bulles intra : au-dessus d'une certaine taille
* on force dCB/dt � �tre n�gatif ou nul
        CALL MOFCLIC(xmo_RB,xmo_B_NUCLHOMO,xmo_B_COALEGEOM,
     &               xmo_XE_NUCLHOMO,xmo_XEB_NUCLHOMO) 
************************************************************************
* d�riv�e temporelle du volume total de bulles intra xmo_VTB
*
        xmo_VTB_GAZ =
     &   ( xmo_XEB_NUCLHOMO + xmo_XEB_PIEGBUL  
     &     -  (xmo_BB - xmo_BBB) * xmo_CXEB ) *
     &        xmo_ZETA * xmo_OMEGA * xmo_XNAVOG
*
        IF (xmo_RB .GT. 0.D0) THEN
           xmo_VTB_LACUNE = xmo_PVI * xmo_RB * 
     &            ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2B )
     &            * xmo_CB
        ELSE
           xmo_VTB_LACUNE = 0.D0
        ENDIF
*
        xmo_VTB_PIEGBPOR =
     &     - xmo_PIEGBPOR * xmo_VTB / (1.D0-xmo_FR)
        xmo_VTB_REMBUL =
     &     - xmo_BBB * xmo_VTB
*
        IF (mo_IET .NE. 2) THEN
          xmo_VTB_SORTIEGRAIN = -  xmo_HJ * xmo_VTB
        ELSE
          xmo_VTB_SORTIEGRAIN = 
     &       - xmo_DFRSDT *  xmo_VTB  / (1.D0-xmo_FR)
        ENDIF 
*
************************************************************************
* d�riv�e temporelle de la concentration en pores intra xmo_CPOR
*
        xmo_POR_COALEDPOR =
     &    - xmo_GCPORDPOR * xmo_CPOR**2.D0  / (1.D0-xmo_FR)
        xmo_POR_COALEVPOR =
     &    - xmo_GCPORVPOR * xmo_CPOR**2.D0  / (1.D0-xmo_FR)
        xmo_POR_REMPOR = 
     &    - xmo_BPORB * xmo_CPOR 
*
        IF (mo_IET .NE. 2) THEN
          xmo_POR_SORTIEGRAIN = - xmo_HJPOR * xmo_CPOR 
        ELSE
          xmo_POR_SORTIEGRAIN = 
     &       - xmo_DFRSDT *  xmo_CPOR  / (1.D0-xmo_FR)
        ENDIF
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores intra xmo_CXEPOR
*
* essaiLOC
        xmo_XEPOR_PIEGPOR =
     &     xmo_PIEGPOR * xmo_CXERPORLOCM
* essaiLOC
*
        xmo_XEPOR_PIEGBPOR =
     &     xmo_PIEGBPOR *  xmo_CXEB / (1.D0-xmo_FR)
*
        xmo_XEPOR_REMPOR =
     &    - xmo_BPOR * xmo_CXEPOR
*
        IF (mo_IET .NE. 2) THEN 
          xmo_XEPOR_SORTIEGRAIN = - xmo_HJPOR * xmo_CXEPOR 
        ELSE
          xmo_XEPOR_SORTIEGRAIN = 
     &       - xmo_DFRSDT *  xmo_CXEPOR  / (1.D0-xmo_FR)
        ENDIF
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores intra xmo_VTPOR
*
*
        xmo_VTPOR_GAZ =
     &       ( xmo_XEPOR_PIEGPOR 
     &         - ( xmo_BPOR - xmo_BPORB) * xmo_CXEPOR ) 
     &       * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG
*
        IF (xmo_RPOR .GT. 0.D0) THEN
          xmo_VTPOR_LACUNE = xmo_PVI * xmo_RPOR * 
     &            ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2POR )
     &            * xmo_CPOR
        ELSE
          xmo_VTPOR_LACUNE = 0.D0
        ENDIF
*
        xmo_VTPOR_PIEGBPOR =
     &      xmo_PIEGBPOR * xmo_VTB / (1.D0-xmo_FR) 
        xmo_VTPOR_REMPOR =
     &     - xmo_BPORB * xmo_VTPOR
*
        IF (mo_IET .NE. 2) THEN 
        xmo_VTPOR_SORTIEGRAIN = - xmo_HJPOR * xmo_VTPOR
        ELSE
          xmo_VTPOR_SORTIEGRAIN = 
     &       - xmo_DFRSDT *  xmo_VTPOR  / (1.D0-xmo_FR)
        ENDIF
*
C
C
*
************************************************************************
************************************************************************
CDEBUG
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG
CDEBUG       xmo_GRAD_J = -2.D0 * xmo_CXECR_J / xmo_DREM
CDEBUG       xmo_GRAD_BJ = -2.D0 * xmo_CXECR_BJ / xmo_DREM
CDEBUG       xmo_GRAD_PORJ = -2.D0 * xmo_CXECR_PORJ / xmo_DREM
CDEBUG       xmo_GRAD_EXT = -2.D0 * xmo_CXECR_EXT / xmo_DREM
CDEBUG*
CDEBUG        xmo_SOURXE_J = xmo_TEST6 * 
CDEBUG     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_J ) +
CDEBUG     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
CDEBUG*
CDEBUG        xmo_SOURXE_BJ = xmo_TEST6 * 
CDEBUG     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_BJ ) +
CDEBUG     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
CDEBUG*
CDEBUG        xmo_SOURXE_PORJ = xmo_TEST6 * 
CDEBUG     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_PORJ ) +
CDEBUG     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
CDEBUG*
CDEBUG        xmo_SOURXE_EXT = 
CDEBUG     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_EXT ) 
CDEBUG*
CDEBUG*
CDEBUG*
CDEBUG        CALL MOMOYE (xmo_XE_DIFFUSION, xmo_TVGG,
CDEBUG     &                mo_NGG, mo_NNSPHMX, xmo_XE_DIFFUSION_M)
CDEBUG        CALL MOMOYE (xmo_XE_CLIM, xmo_TVGG,
CDEBUG     &                   mo_NGG, mo_NNSPHMX, xmo_XE_CLIM_M)
CDEBUG*
CDEBUG            ERR11 = 
CDEBUG     &        xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M +
CDEBUG     &        (1.D0 - xmo_FR - xmo_FBJ - xmo_FPORJ - xmo_FEXT)
CDEBUG     &         * xmo_SOURXE_J 
CDEBUG     &        - xmo_BJ * xmo_CXEJM  
CDEBUG     &        + xmo_FBJ* xmo_SOURXE_BJ - xmo_BBJ * xmo_CXEBJ 
CDEBUG     &        + xmo_FPORJ* xmo_SOURXE_PORJ -xmo_BPORJ * xmo_CXEPORJ
CDEBUG     &        + xmo_FEXT * xmo_SOURXE_EXT
CDEBUG*
CDEBUG          WRITE(20,*) 'dans moterm ERR11 = ', ERR11
CDEBUG          WRITE(20,*) 'xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M = ',
CDEBUG     &                 xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M
CDEBUG        ENDIF
CDEBUG
*
*
      RETURN
      END
      SUBROUTINE MOTEXI(mo_DISP) 
C     =================
C
C
C     ******************************************************************
C     MOTEXI : MOgador Test EXIstence des equations                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
************************************************************************
* cmogvar.inc : mo_IET xmo_VG xmo_VD en entr�es
*               xmo_EQUADISP en entr�e et sortie 
* cmoginf : mo_DEBUG en entree
************************************************************************
C
      DOUBLE PRECISION 
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX)
C
C
      CALL MOMESS('DANS MOTEXI')
C
C-----7--0---------0---------0---------0---------0---------0---------0--
*
      mo_DISP = 0
*
***********************************************************************
* calcul des grandeurs n�cessaires aux bilans et aux impressions
***********************************************************************
*
      DO 50 IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VG(IVGA,2)
 50   CONTINUE
*
      DO 60 IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VD(IVDE,2)
 60   CONTINUE
*
CDEBUG
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG'
CDEBUG       WRITE(20,*) (xmo_VG(IVGA,2), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD'
CDEBUG       WRITE(20,*) (xmo_VD(IVGA,2), IVGA = 1,mo_NVD)
CDEBUG           ENDIF
CDEBUG
*
**********************************************************************
* CAS de l'�tat classique - connecte
**********************************************************************
      IF (mo_IET .EQ. 0) THEN
*     ***********************
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &    xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &    xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
* On teste si la population des pores intra a disparu
*
        xmo_EPSDISP = 1.D-20
        IF ( xmo_CPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_CXEPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_VTPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_EQUADISP(5) .NE. 0.D0) THEN
          xmo_EQUADISP(5) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des pores intra a disparue'
*
        ENDIF
*
* On teste si la population des pores inter a disparu
*
        xmo_EPSDISP = 1.D-20
        IF ( xmo_CPORJ  .LT. xmo_EPSDISP .AND.
     &       xmo_CXEPORJ .LT. xmo_EPSDISP  .AND.
     &       xmo_VTPORJ .LT. xmo_EPSDISP  .AND.
     &       xmo_EQUADISP(6) .NE. 0.D0) THEN
          xmo_EQUADISP(6) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des pores inter a disparue'
*
        ENDIF
*
      ENDIF
*     *****
C***********************************************************************
C***********************************************************************
C
**********************************************************************
* CAS de l'�tat en restructuration
**********************************************************************
      IF (mo_IET .EQ. 2) THEN
*     ***********************
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
CDEBUG
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans motexi apres moparl2'
CDEBUG       WRITE(20,*) 'xmo_CBJ= ',xmo_CBJ
CDEBUG       WRITE(20,*) 'xmo_CXEBJ= ',xmo_CXEBJ
CDEBUG       WRITE(20,*) 'xmo_VTBJ= ',xmo_VTBJ
CDEBUG           ENDIF
CDEBUG
* On teste si la population des pores intra a disparu
*
        xmo_EPSDISP = 1.D-60
        IF ( xmo_CPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_CXEPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_VTPOR .LT. xmo_EPSDISP  .AND.
     &       xmo_EQUADISP(7) .NE. 0.D0) THEN
          xmo_EQUADISP(7) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des pores intra',
     &               ' zone saine a disparue'
*
        ENDIF
*
* On teste si la population des pores inter a disparu
*
        xmo_EPSDISP = 1.D-60
        IF ( xmo_CPORJ .LT. xmo_EPSDISP  .AND.
     &       xmo_CXEPORJ .LT. xmo_EPSDISP  .AND.
     &       xmo_VTPORJ .LT. xmo_EPSDISP  .AND.
     &       xmo_EQUADISP(8) .NE. 0.D0) THEN
          xmo_EQUADISP(8) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des pores inter ',
     &               ' joints sains a disparue'
*
        ENDIF
*
* On teste si la population des pores de zone restructurees a disparu
* On ne fait disparaitre cette population que si celle des pores 
* inter a deja disparu

        IF ( xmo_CPORRIM .EQ. 0.D0  .AND.
     &       xmo_CXEPORRIM .EQ. 0.D0  .AND.
     &       xmo_VTPORRIM .EQ. 0.D0  .AND.
     &       xmo_EQUADISP(9) .NE. 0.D0 .AND.
     &       xmo_EQUADISP(8) .EQ. 0.D0) THEN
          xmo_EQUADISP(9) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des pores de zones ',
     &               'restructurees a disparue'
*
        ENDIF
*
* On teste si la population des bulles inter de zone saine a disparu
*
        xmo_EPSDISP = 1.D-60
        IF ( xmo_CBJ .LT. xmo_EPSDISP  .AND.
     &       xmo_CXEBJ .LT. xmo_EPSDISP  .AND.
     &       xmo_VTBJ .LT.  xmo_EPSDISP .AND.
     &       xmo_EQUADISP(4) .NE. 0.D0 ) THEN
          xmo_EQUADISP(4) = 0.D0
          mo_PRFOIS = 1
          mo_DISP = 1
*
          WRITE(mo_DEBUG,*) 'La population des bulles de zones ',
     &               'saines a disparue'
*
        ENDIF
*
      ENDIF
*     *****
C***********************************************************************
C***********************************************************************
C
C
      RETURN
      END
*
      SUBROUTINE MOTRAI_GEAR(mo_IB,TAVANT,TAPRES,DELTAT0,TOUT0) 
C     ======================
C
C
C     ******************************************************************
C     MOTRAI_GEAR : MOgador TRAItement du cas o� le calcul s'arr�te � 
C                   cause d'un changement d'�tat du combustible
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmoggpt.inc'
      INCLUDE 'cmoginf.inc'
* pour test
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogdebug.inc'
* fin de pour test
C
***********************************************************************
C cmogear.inc : xmo_RSAV_av mo_ISAV_av mo_G_IEQSAV_av xmo_G_RWORKSAV_av
C               mo_G_IWORKSAV_av xmo_G_RTOLSAV_av mo_IGTABSAV_av mo_IDTABSAV_av en entr�e
C               xmo_RSAV mo_ISAV mo_G_IEQSAV xmo_G_RWORKSAV
C               mo_G_IWORKSAV xmo_G_RTOLSAV mo_IGTABSAV mo_IDTABSAV en sortie
C cmoginf.inc : mo_DEBUG en entree
* cmoggpt.inc : xmo_PAS_TEMPS_LIM et mo_NTEMPS_LU en entr�e 
***********************************************************************
C PARAMETRES :
C mo_IB indique le type de basculement d�tect� 
C       et si ce basculement est trop rapide (valeur n�gative)
C TAVANT et TAPRES sont les temps d�finissant le sous pas de temps
C                  au cours du quel le basculment a �t� d�tect�
C TOUT0 est le temps final que l'on voulait atteindre
C 
C A la sortie de la subroutine, le calcul est fait jusqu'� TOUT0
C et mo_IB est �gal � 0
C Remarque : Si mo_IB est �gal � 0, rien n'est fait dans 
C            cette subroutine
C**********************************
C
C
C
CDEBUG
CDEBUG      WRITE(mo_DEBUG,*) 'DANS MOTRAI_GEAR'
CDEBUG
C
C
      xmo_EPS_TEMPS = 1.D-5
***********************************************************************
C Dans le cas o� le basculement a �t� trop rapide (mo_IB negatif)
C on repart de TAVANT en raffinant de plus en plus le pas de temps
C jusqu'� ce que on d�tecte un basculement lent (mo_IB positif)
C
 100  CONTINUE     
      IF ( mo_IB .LT. 0 ) THEN
        TIN1 = TAVANT
        TOUT1 = TAPRES
        NTEMPS = 2
* on restitue les common du solveur tels qu'ils �taient
* � TAVANT
        CALL MOCOPY(
     &     xmo_RSAV_av, mo_ISAV_av,
     &     mo_G_IEQSAV_av, xmo_G_RWORKSAV_av,
     &     mo_G_IWORKSAV_av, xmo_G_RTOLSAV_av, 
     &     mo_IGTABSAV_av, mo_IDTABSAV_av,
     &     xmo_RSAV, mo_ISAV, 
     &     mo_G_IEQSAV, xmo_G_RWORKSAV,
     &     mo_G_IWORKSAV, xmo_G_RTOLSAV, 
     &     mo_IGTABSAV, mo_IDTABSAV)
*
        CALL MOCAVF_GEAR
     &       (TIN1,TOUT1,NTEMPS,mo_IB,TAVANT,TAPRES)
*
*
CDEBUG        WRITE(mo_DEBUG,*) 'apres MOCAVF_GEAR mo_IB =',mo_IB
*
        GOTO 100
      ENDIF
C
C Il arrive qu'en faisant des plus petit pas on ne retrouve plus le basculement
C l� o� on l'avait trouv� pr�cedemment. On ressort alors de la boucle
C ci-dessus avec mo_IB = 0 � un temps qui peut etre inferieur a TOUT0
C Il faut aller jusqu'a TOUT0
C
      IF ( (mo_IB .EQ. 0) .AND. (TOUT1 .LT.TOUT0) ) THEN
        TIN1 = TOUT1
        TOUT1 = TOUT0
        NTEMPS = mo_NTEMPS_LU
        IF ( (TOUT1-TIN1) .GT. xmo_PAS_TEMPS_LIM) THEN
          NTEMPS = int( (TOUT1-TIN1) / xmo_PAS_TEMPS_LIM ) + 1
        ENDIF
*
        CALL MOCAVF_GEAR
     &       (TIN1,TOUT1,NTEMPS,mo_IB,TAVANT,TAPRES)
*
*
CDEBUG        WRITE(mo_DEBUG,*) 'apres MOCAVF_GEAR mo_IB =',mo_IB
*
        GOTO 100
      ENDIF
C
***********************************************************************
C Dans le cas o� le basculement est lent ou si il y a eu une disparition
C de population (mo_IB positif)
      IF ( mo_IB .GT. 0 ) THEN
* r�initialisation des vecteurs des variables
*
C
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       ENDIF
C
*
        CALL MOINBA_GEAR(mo_IB)
*
C
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       ENDIF
C
*
* calcul des param�tres secondaires utiles dans le nouvel 
* �tat o� l'on se trouve
        mo_ISAUTE = 1
        CALL MOCAPS_GEAR(mo_ISAUTE)
*
*
* Si c'est possible,
* On fait un petit pas de temps apres le basculement
* Puis on termine jusqu'au temps TOUT
        TIN1 = TAPRES
        TOUT1 = TAPRES + (TAPRES - TAVANT)
*
CDEBUG        WRITE(mo_DEBUG,*) 'TIN1 =', TIN1
CDEBUG        WRITE(mo_DEBUG,*) 'TOUT1 =', TOUT1
CDEBUG        WRITE(mo_DEBUG,*) 'TOUT0 =', TOUT0
*
***********************************************************************
* cas o� on peut faire un petit pas de temps
***********************************************************************
        IF (TOUT1 .LT. TOUT0) THEN
*       **************************
*
CDEBUG
CDEBUG        WRITE(mo_DEBUG,*) 'dans motrai_gear cas TOUT1 .LT.TOUT0'
CDEBUG
*
* petit pas de temps
*====================
 200      CONTINUE
          NTEMPS = 1
          TINC = TIN1
          TOUTC = TOUT1
          CALL MOCAVF_GEAR
     &         (TINC,TOUTC,NTEMPS,mo_IB,TAVANT,TAPRES)
          IF (mo_IB .NE. 0) THEN
            IF (mo_IB .EQ. 999) THEN
* Une disparition de population a eu lieu
              TIN1 = TAPRES
              IF (TIN1 .LT. TOUT1 ) GOTO 200
            ELSE
* un nouveau basculement peut encore survenir
              GOTO 100
            ENDIF
          ENDIF
*
* petit pas de temps termin�
* Ici TOUT1 = TAPRES le calcul a �t� effectu� jusqu'� TOUT1
*=================================================================
CDEBUG
CDEBUG        WRITE(mo_DEBUG,*) 'dans motrai_gear petit pas effectu�'
CDEBUG
*
 300        CONTINUE
* pas de temps de longueur proche de DELTAT0
            NTEMPS = int( (TOUT0-TOUT1)/DELTAT0 ) + 1
CDEBUG
CDEBUG        WRITE(mo_DEBUG,*) 'dans motrai_gear apres 300'
CDEBUG        WRITE(mo_DEBUG,*) 'TOUT1 = ',TOUT1
CDEBUG        WRITE(mo_DEBUG,*) 'TOUT0 = ',TOUT0
CDEBUG
            TINC = TOUT1
            TOUTC = TOUT0
            CALL MOCAVF_GEAR
     &          (TINC,TOUTC,NTEMPS,mo_IB,TAVANT,TAPRES)
            IF (mo_IB .NE. 0) THEN
              IF (mo_IB .EQ. 999) THEN
* Une disparition de population a eu lieu
                TOUT1 = TAPRES
                IF (TOUT1 .LT. TOUT0 ) GOTO 300
              ELSE
* un nouveau basculement peut encore intervenir
                GOTO 100
              ENDIF
            ENDIF
*
***********************************************************************
* cas o� on ne peut pas faire un petit pas de temps
* mais on doit calculer jusqu'� TOUT0
***********************************************************************
        ELSEIF (xmo_EPS_TEMPS .LT. (TOUT0 - TIN1)) THEN
*       ***********************************************
*
CDEBUG
CDEBUG        WRITE(mo_DEBUG,*) 'dans motrai_gear cas TOUT1 .GE.TOUT0 '
CDEBUG        WRITE(mo_DEBUG,*) 'et xmo_EPS_TEMPS .LT. (TOUT0 - TIN1)'
CDEBUG
* 
 400      CONTINUE
* dans ce cas on fait un seul pas de temps pour arriver jusqu'� TOUT0
          NTEMPS = 1
          TINC = TIN1
          TOUTC = TOUT0
          CALL MOCAVF_GEAR
     &         (TINC,TOUTC,NTEMPS,mo_IB,TAVANT,TAPRES)
          IF (mo_IB .NE. 0) THEN
            IF (mo_IB .EQ. 999) THEN
* Une disparition de population a eu lieu
              TIN1 = TAPRES
              IF (TIN1 .LT. TOUT0 ) GOTO 400
            ELSE
* un nouveau basculement peut encore intervenir
              GOTO 100
            ENDIF
          ENDIF
*
***********************************************************************
* cas o� on on �tait d�j� � TOUT0
***********************************************************************
        ELSE
*       ****
*
CDEBUG
CDEBUG          WRITE(mo_DEBUG,*) 'dans motrai_gear cas TOUT1 .GE.TOUT0 '
CDEBUG          WRITE(mo_DEBUG,*) 'et xmo_EPS_TEMPS .GE. (TOUT0 - TIN1)'
CDEBUG
*
* dans ce cas, on ne fait rien pour les variables
          DELTAT = TOUT0 - TIN1
* calcul du temps atteint
          CALL MOCTPS(DELTAT)
***********************************************************************
***********************************************************************
        ENDIF
*       *****
***********************************************************************
***********************************************************************
      ENDIF
C fin de 'if mo_IB .GT. 0'
C
C
CDEBUG
CDEBUG      WRITE(mo_DEBUG,*) 'sortie de motrai_gear'
CDEBUG
C
C
C
      RETURN
      END
      SUBROUTINE MOTRM0(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEJ_NUCLHOMO,xmo_XEJ_PIEGBUL,xmo_XEJ_PIEGPOR,
     &      xmo_XEJ_SOURCE,xmo_XEJ_REMJOINT,xmo_XEJ_DIFFUSION,
     &      xmo_XEJ_CLIM3,xmo_XEJ_SORTIEJ,
     &      xmo_BJ_NUCLHOMO,xmo_BJ_APPORT,xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,xmo_BJ_REMBUL,xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,xmo_XEBJ_APPORT,xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,xmo_XEBJ_REMBUL,xmo_XEBJ_SORTIEJ,
     &      xmo_XEBJ_PERCBPOR,xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,xmo_VTBJ_GAZ,xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,xmo_VTBJ_APPORT,xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,xmo_XEPORJ_APPORT,xmo_XEPORJ_SORTIEJ,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,xmo_VTPORJ_GAZ,xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,xmo_VTPORJ_SORTIEJ,
     &      
     &      xmo_SOURXE,xmo_HJ,xmo_HJPOR,xmo_BBJ,xmo_BPORJ,
     &      xmo_OUTSE,xmo_OUTEJEC,xmo_CXEJM,xmo_PINTBJ)
C     =================
C
C
C     ******************************************************************
C     MOTRM0 : MOgador calcul des TeRMes utiles dans l'expression des d�riv�es
C              des variables inter dans l'�tat classique (0)                                   
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmogvar.inc'
C
CDEBUG      INCLUDE 'cmogdebug.inc' 
C
C
***********************************************************************
* cmogpas.inc : xmo_DXEJ xmo_FACPBJ xmo_PVIJ1_B xmo_PVIJ1_POR  xmo_PVIJ2 
*               xmo_OMSKT xmo_FACGCJ xmo_BJ xmo_DISTCOALJ  en entr�e
* cmogppr.inc : xmo_FMIGRJ xmo_FNBJ xmo_MN xmo_ZETA 
*               xmo_COELIM_PERC xmo_KTUNJ xmo_KTUNJPORE mo_DIF_SEQ_LU 
*               xmo_BBJ_S_BB xmo_DREM mo_MODELE_FRAC en entr�e
* cmogphy.inc : xmo_XNAVOG xmo_OMEGA xmo_PIZAHL en entr�e
* cmogmat.inc : xmo_TMSEQ 
*               xmo_TVSEQ xmo_TIMSEQ xmo_TIMRSEQ 
*               xmo_TIMQSEQ  en entr�e
* cmogdon.inc : xmo_AGG  en entr�e
* cmogpnu.inc : mo_NSEQ xmo_RGG en entr�e
* cmogent.inc : xmo_CXEJLIM xmo_ASE xmo_DERCXEJLIM xmo_PHYDR xmo_PPLEN en entr�e
* cmogpte.inc : xmo_TEST0 xmo_TEST2 xmo_TEST3 xmo_TEST6 xmo_SOURXEIMP 
*               en entr�e
* cmogvar.inc : mo_IET xmo_EQUADISP en entree
************************************************************************
*
* entr�es et sorties en param�tres
* premier bloc de param�tres : variables du probl�me : entr�es
*++++++++++++++++++++++++++++
*
* Il s'agit des variables intra (parcqu'on en a besoin pour ce qui sort du grain)  
*           des variables inter 
*           de la densit� de dislocation
*
* deuxi�me bloc de param�tres : sorties
*++++++++++++++++++++++++++++
*
* pour equation du gaz dissous inter :
* xmo_XEJ_NUCLHOMO : Vecteur terme de nucl�ation homog�ne dans l'�quation 
*                   du gaz dissous inter (mol/s/m3)
* xmo_XEJ_PIEGBUL : Vecteur terme de piegeage du gaz dissous par les bulles (mol/s/m3) 
* xmo_XEJ_PIEGPOR : Vecteur terme de piegeage du gaz dissous par les pores (mol/s/m3)
* xmo_XEJ_SOURCE : Vecteur terme source de gaz dissous (il provient de l'intra) (mol/s/m3)
* xmo_XEJ_REMJOINT : Vecteur terme de remise en solution de gaz dissous vers les grains (mol/s/m3)
* xmo_XEJ_DIFFUSION : Vecteur terme de diffusion de gaz dissous (mol/s/m3)
* xmo_XEJ_CLIM3 : Vecteur terme li� � la condtion � la limte contenant le coef de diffusion (mol/s/m3)
*
* pour equation du nombre de bulles inter/m3 :
* xmo_BJ_NUCLHOMO : terme de nucl�ation homog�ne dans l'�quation 
*                   des bulles inter (bulles/s/m3)
* xmo_BJ_APPORT : terme d'apport de bulles en provenance des grains (bulles/s/m3) 
* xmo_BJ_COALEDBJ : terme de coalescence en mouvement al�atoire (bulles/s/m3)
* xmo_BJ_COALEVBJ : terme de coalescence en mouvement sous gradient thermique (bulles/s/m3) 
* xmo_BJ_PIEGBPOR : terme de piegeage des bulles inter par les pores inter (bulles/s/m3)
* xmo_BJ_REMBUL : terme de destruction des bulles inter par les pointes de fission (bulles/s/m3)
*
* pour equation du gaz dans les bulles inter+ pores inetr/m3 :
* xmo_XEBJ_NUCLHOMO : terme de nucl�ation homog�ne dans l'�quation 
*                   du gaz des bulles inter (mol/s/m3)
* xmo_XEBJ_APPORT : terme d'apport de gaz dans les bulles inter 
*                   (gaz en provenance des grains) (mol/s/m3) 
* xmo_XEBJ_PIEGBUL : terme de piegeage du gaz dissous par les bulles (mol/s/m3) 
* xmo_XEBJ_PIEGBPOR : terme li� au piegeage des bulles inter par les pores inter (mol/s/m3)
* xmo_XEBJ_REMBUL : terme de remise en solution du gaz des bulles inter par les 
*                  pointes de fission, que la bulle soit d�truite ou non (mol/s/m3)
*
* pour equation du volume total des bulles inter/m3 :
* xmo_VTBJ_LACUNE : terme d'apport de volume par les lacunes (m3/s/m3)
* xmo_VTBJ_GAZ :  terme d'apport de volume li� aux atomes de gaz �chang�s avec 
*                le r�seau cristallin (m3/s/m3)
* xmo_VTBJ_PIEGBPOR : terme de perte de volume li� au piegeage des bulles inter 
*                    par les pores inter (m3/s/m3)
* xmo_VTBJ_REMBUL : terme de perte de volume li� � la destruction des bulles intra par les 
*                  pointes de fission (m3/s/m3)
* xmo_VTBJ_APPORT  : terme d'apport de volume du aux bulles intra venant 
*                    alimenter les bulles inter (m3/s/m3)
*
* pour equation du nombre de pores inter/m3 :
*     rien � calculer car ce nombre de pores est fix�
*
* pour equation du gaz dans les bulles inter + pores inter/m3 :
* xmo_XEPORJ_PIEGPOR : terme de piegeage du gaz dissous par les pores (mol/s/m3) 
* xmo_XEPORJ_PIEGBPOR: terme li� au piegeage des bulles inter par les pores inter (mol/s/m3)
* xmo_XEPORJ_REMPOR : terme de remise en solution du gaz des pores inter par les 
*                  pointes de fission, le pore inter n'est jamais d�truit (mol/s/m3)
* xmo_XEPORJ_APPORT :terme d'apport de gaz dans les pores inter 
*                   (gaz en provenance des grains) (mol/s/m3)
*
* pour equation du volume total des pores inter/m3 :
* xmo_VTPORJ_LACUNE : terme d'apport de volume par les lacunes (m3/s/m3)
* xmo_VTPORJ_GAZ :  terme d'apport de volume li� aux atomes de gaz �chang�s avec 
*                le r�seau cristallin (m3/s/m3)
* xmo_VTPORJ_PIEGBPOR : terme d'apport de volume li� au piegeage des bulles inter 
*                    par les pores inter (m3/s/m3)
* xmo_VTPORJ_APPORT : terme d'apport de volume du aux bulles intra et aux pores 
*                    intra venant alimenter les pores inter (m3/s/m3)
*
* pour equation du gaz dans les bulles inter + pores inter/m3 :
* xmo_XETUNJ_PERCPLEN : terme de percolation entre le reservoir et le plenum (mol/s/m3) 
*
*
* Troisi�me bloc de param�tres : sorties
*+++++++++++++++++++++++++++++++
*
* Ces sorties ne servent pas dans la constitution de l'expression des d�riv�es
* des variables mais il est utile de pouvoir les imprimer pour pouvoir faire
* des v�rifications de bilan de gaz
*
* xmo_SOURXE : terme source de gaz dissous inter (mol/m3/s) ce gaz provient du 
*              grain par diffusion
* xmo_HJ : Fr�quence de sortie des bulles intra du grain (s-1)
* xmo_HJPOR : Fr�quence de sortie des pores intra du grain (s-1) 
* xmo_BBJ : fr�quence de remise en solution du gaz des bulles inter (s-1) 
* xmo_BPORJ : fr�quence de remise en solution du gaz des pores inter (s-1)
* xmo_OUTSE : taux de sortie par diffusion du gaz dissous inter de la 
*             sph�re �quivalente (mol/m3/s). C'est l'�quivalent de xmo_SOURXE
*             mais pour la sph�re �quivalente
* xmo_CXEJM : moyenne du gaz dissous inter (mol/m3)
************************************************************************
* Variables internes utilis�es dont la signification physique est importante :
* xmo_DXEAM : coefficient de diffusion du gaz en volume, en tenant 
*             compte de la pr�sence de dislocations (m2/s)
* xmo_PIEGBJ : fr�quence de pi�geage de gaz inter dissous par les bulles inter (s-1)
* xmo_PIEGPORJ : fr�quence de pi�geage de gaz inter dissous par les pores inter (s-1)
* xmo_FPORJ : taux de couverture des joints de grain par les pores inter ()
* xmo_FBJ : taux de couverture des joints de grain par les bulles inter ()
* xmo_DLBJ : coefficient permettant de comptabiliser ou non les bulles 
*              intra d�bouchantes en tant que bulles inter () : varie entre 0 et 1
* xmo_GCJ : Coeff apparaissant dans le terme de coalescence des bulles inter (m3/s)
* xmo_PIEGBJPORJ : fr�quence de piegeage des bulles inter par les pores inter (s-1)
* xmo_BBJB : fr�quence de destruction des bulles inter par les pointes de fission (s-1)
* xmo_CXEJCM : moyenne du carr� du gaz dissous inter (mol2/m6)
* xmo_TEXP2BJ: Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � une bulle inter ()
* xmo_BPORJ : fr�quence de remise en solution du gaz des pores inter (s-1)
* xmo_TEXP2PORJ : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � un pore inter () 
*  mo_DIF_SEQ : vaut 0 si la diffusion � grande �chelle du gaz au joint 
*               de grain n'est pas consid�r�e  
* mo_NSEQ_C : vaut 1 si la diffusion � grande �chelle du gaz au joint 
*               de grain n'est pas consid�r�e, sinon cela vaut mo_NSEQ            
************************************************************************
************************************************************************
      DOUBLE PRECISION 
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_VECT1(mo_NNSPHMX),
     &                 xmo_XEJ_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XEJ_SOURCE(mo_NNSPHMX),
     &                 xmo_XEJ_REMJOINT(mo_NNSPHMX),
     &                 xmo_XEJ_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEJ_CLIM3(mo_NNSPHMX),
     &                 xmo_XEJ_SORTIEJ(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOTRM0')
*
************************************************************************
************************************************************************
* POUR ETAT CLASSIQUE et ETAT TUNNEL et ETAT EN RESTRUCTURATION
************************************************************************
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
* A FAIRE POUR l ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
        IF (mo_IET .EQ. 2) THEN
*       =======================
*
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM + xmo_RHODL - xmo_MARGRHO_FR pour les calcul de xmo_FR  
* Sinon, on a des divisions par zero
*
          xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
          xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
*
*
* Calcul de la d�riv�e temporelle de FR : xmo_DFRSDT
          IF ( xmo_RHOD .LT. xmo_RHOD_FRLIM ) THEN
            IF (mo_MODELE_FRAC .EQ. 1) THEN
              xmo_DFRSDT = xmo_PIZAHL / 4.D0 / xmo_RHODL *
     &          cos( xmo_PIZAHL / 2.D0 / xmo_RHODL *
     &          ( xmo_RHOLIM - xmo_RHOD1 ) ) * xmo_FACRHO * xmo_RHOD1
            ELSEIF (mo_MODELE_FRAC .EQ. 2) THEN
              xmo_DFRSDT = xmo_FACRHO * xmo_RHOD1
     &                     / 2.D0 / xmo_RHODL
            ENDIF
          ELSE
            xmo_DFRSDT = 0.D0
          ENDIF
*
        ELSE
*       ====
          xmo_FR = 0.D0
          xmo_DFRSDT = 0.D0
        ENDIF
*       =====
************************************************************************
* FIN DES ACTIONS A FAIRE POUR L'ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
************************************************************************
* coefficient de diffusion du Xe tenant compte de la pr�sence de 
* dislocations
*
        CALL MODXEA(xmo_RHOD,xmo_DXEAM)
*
*
************************************************************************
*
* vitesse des bulles intra xmo_VB (m/s)
* coefficient de diffusion des bulles intra xmo_DB (m2/s)
*
       CALL MOBUPO(xmo_CXEB,xmo_CB,xmo_RB,xmo_DXEAM,xmo_TEST3,
     &             xmo_VB,xmo_DB)
*
************************************************************************
* vitesse des pores intra xmo_VPOR (m/s)
* coefficient de diffusion des pores intra xmo_DPOR (m2/s)
*
       CALL MOBUPO(xmo_CXEPOR,xmo_CPOR,xmo_RPOR,xmo_DXEAM,xmo_TEST2,
     &             xmo_VPOR,xmo_DPOR)
*
************************************************************************
* Fr�quence de disparition des bulles intra au joint de grain xmo_HJ
*
        xmo_HJ = 3.D0 * xmo_VB / 4.D0 / xmo_AGG  +
     &           15.D0 * xmo_DB / xmo_AGG**2.D0
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BBJ
* et xmo_BBJB des bulles inter
* 
        CALL MOBLOC(xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,2,
     &     xmo_BBJ,xmo_BBJB,xmo_TEXP2BJ)
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BPORJ
* et xmo_BPORJB des pores inter (xmo_BPORJB ne sert jamais)
* 
        CALL MOBLOC(xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,5,
     &     xmo_BPORJ,xmo_BPORJB,xmo_TEXP2PORJ)
*
************************************************************************
*
* Coefficient de diffusion des bulles inter xmo_DBJ (m2/s)
* Vitesse des bulles inter sous gradient thermique xmo_VBJ (m/s)
* fr�quence de pi�geage du gaz inter dissous par les bulles inter (s-1)
*
        CALL MOBUPJ
     &    (xmo_CXEBJ,xmo_CBJ,xmo_RBJ,xmo_VBJ,xmo_DBJ,xmo_PIEGBJ)
*
************************************************************************
* fr�quence de pi�geage du gaz dissous au joint par les pores inter (s-1)
*
        xmo_PIEGPORJ = xmo_FACPBJ *  xmo_DXEJ * xmo_CPORJ 
*
************************************************************************
* fr�quence de pi�geage des bulles inter par les pores inter (s-1)
*
        xmo_PIEGBJPORJ = ( xmo_FACGCJ *  xmo_DBJ 
     &                     + 4.D0 * xmo_AGG / 3.D0 * xmo_VBJ * 
     &                       (xmo_RPORJ + xmo_RBJ) ) * xmo_CPORJ             
*
************************************************************************
* Coalescence des bulles inter xmo_GCJ
*
        xmo_GCJDBJ = 
     &       xmo_FACGCJ * xmo_DBJ
        xmo_GCJVBJ = 
     &       4.D0 / 3.D0 * xmo_AGG * xmo_RBJ * xmo_VBJ * xmo_FMIGRJ
*
************************************************************************
*
* Calcul de delta nucl pour les bulles inter xmo_DNUCLJ
*
        CALL MODNUC(xmo_CBJ,xmo_CXEBJ, xmo_RBJ, xmo_DNUCLJ)
*
************************************************************************
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
           xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
************************************************************************
* Calcul de la moyenne xmo_CXEJCM du carr� des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN   
          CALL MOMOYC_VF(xmo_CXEJ, xmo_TMSEQ, mo_NSEQ, xmo_CXEJCM)
        ELSE
           xmo_CXEJCM = xmo_CXEJ(1) * xmo_CXEJ(1)
        ENDIF
*
************************************************************************
* Calcul de la moyenne xmo_CXEJCMIN du carr� des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN   
        CALL MOMOYC_MIN(xmo_CXEJ, xmo_FR, 1.D0/xmo_KGJ2 ,
     &                  xmo_TMSEQ, mo_NSEQ, xmo_CXEJCMIN)
        ELSE
           xmo_CXEJCMIN = 
     &            xmo_CXEJ(1) * 
     &            min (xmo_CXEJ(1) / (1.D0 - xmo_FR) , 1.D0/xmo_KGJ2 ) 
        ENDIF
*
************************************************************************
* Calcul des taux de couverture xmo_FPORJ et xmo_FBJ
* cas �tat classique ou tunnel
*
        CALL MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,xmo_FR,
     &                  xmo_FBJ,xmo_FPORJ,xmo_FEXT)
*
************************************************************************
*  Calcul des concentrations de gaz � la limite grain en face des 
*  diff�rentes populations
*  cas �tat classique ou tunnel
*
*
          xmo_CXENGG = xmo_CXE(mo_NGG)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR),
     &                mo_NGG, xmo_RGG, 
     &                xmo_CXEJM, xmo_BJ, 
     &                1.D0 - xmo_FR - xmo_FBJ - xmo_FPORJ - xmo_FEXT ,
     &                xmo_CXEBJ, xmo_BBJB, 
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_J)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                xmo_CXEBJ, xmo_BBJ-xmo_BBJB, 
     &                xmo_FBJ,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_BJ)
*
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                xmo_CXEPORJ, xmo_BPORJ, 
     &                xmo_FPORJ,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_PORJ)
*
*
* pour ejection
          CALL MOCACR(xmo_CXENGG / (1.D0 - xmo_FR), 
     &                mo_NGG, xmo_RGG,
     &                0.D0,0.D0, 
     &                xmo_FEXT,
     &                0.D0,0.D0,
     &                xmo_AGG, xmo_DXEAM, xmo_CXECR_EXT)
* pour ejection
*
C
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG        ENDIF
C
************************************************************************
* Calcul des gradients de gaz � l'int�rieur du grain face � chacune des 
* populations 
* En face des zones restructur�es le flux de gaz est consid�r� nul
*
       xmo_GRAD_J = -2.D0 * xmo_CXECR_J / xmo_DREM
       xmo_GRAD_BJ = -2.D0 * xmo_CXECR_BJ / xmo_DREM
       xmo_GRAD_PORJ = -2.D0 * xmo_CXECR_PORJ / xmo_DREM
       xmo_GRAD_EXT = -2.D0 * xmo_CXECR_EXT / xmo_DREM
*
************************************************************************
* Calcul du terme source de gaz dissous en inter, provenant des grains (mol/m3/s)
* face � chaque population
        xmo_SOURXE_J = xmo_TEST6 * 
     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_J ) +
     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
*
        xmo_SOURXE_BJ = xmo_TEST6 * 
     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_BJ ) +
     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
*
        xmo_SOURXE_PORJ = xmo_TEST6 * 
     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_PORJ ) +
     &               ( 1.D0 - xmo_TEST6 ) * xmo_SOURXEIMP
*
        xmo_SOURXE_EXT = 
     &               ( - 3.D0 / xmo_AGG * xmo_DXEAM * xmo_GRAD_EXT ) 
************************************************************************ 
* Calcul de xmo_DLBJ : facteur compris entre 0 et 1 permettant 
* de ne pas comptabiliser les bulles intra d�bouchant au joint de grain
* comme des bulles si leur rayon est trop diff�rent de celui des 
* bulles inter d�j� existentes
*
        CALL MODLBJ(xmo_RB,xmo_RBJ,xmo_DLBJ)
*
************************************************************************
* Fr�quence de disparition des pores intra au joint de grain xmo_HJPOR
*
        xmo_HJPOR = 3.D0 * xmo_VPOR / 4.D0 / xmo_AGG  +
     &              15.D0 * xmo_DPOR / xmo_AGG**2.D0
*
*
************************************************************************
* Calcul de la pression interne dans les bulles inter et les pores inter 
* xmo_PINTBJ et xmo_PINTPORJ
* Si mo_PB_BJ vaut 1, le calcul de cette pression n'a pas �t� possible
*
*
          CALL MOVLAT(xmo_CXEBJ,xmo_VTBJ,2,xmo_VAT)
*
          CALL MOPINT(xmo_VAT,xmo_PINTBJ,mo_PB_BJ)
*
          CALL MOVLAT(xmo_CXEPORJ,xmo_VTPORJ,5,xmo_VAT)
*
          CALL MOPINT(xmo_VAT,xmo_PINTPORJ,mo_PB_PORJ)
*
************************************************************************
* Calcul du taux d'interconnection
          xmo_FCOUV = xmo_FBJ + xmo_FPORJ
          CALL MOFRAC_CONNEC(xmo_FCOUV,xmo_FRCONNEC)
*
************************************************************************
************************************************************************
*
*
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* d�riv�e temporelle de la concentration en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN
          CALL XMMVEC_TRIDIAG (mo_NSEQ,mo_NSEQ,mo_NNSPHMX,mo_NNSPHMX,
     &               xmo_TIMRSEQ, xmo_CXEJ,xmo_VECT1)
        ENDIF
*
        DO 300 IVGA = 1, mo_NSEQ_C
*
          xmo_XEJ_NUCLHOMO(IVGA) = 
     &      ( - xmo_KGJ1 * xmo_KGJ2 / xmo_XNAVOG * xmo_DNUCLJ *
     &        ( xmo_CXEJ(IVGA) * 
     &         min (xmo_CXEJ(IVGA) / (1.D0 - xmo_FR), 1/xmo_KGJ2) +
     &         xmo_CXEJ(IVGA) * xmo_CXEJ(IVGA) / (1.D0 - xmo_FR) )
     &        -  xmo_KGJ * xmo_DNUCLJ * xmo_CXEJ(IVGA) 
     &         * xmo_CXEJ(IVGA) / (1.D0 - xmo_FR) ) * xmo_EQUADISP(4)
*
          xmo_XEJ_PIEGBUL(IVGA) =
     &      - xmo_PIEGBJ * xmo_CXEJ(IVGA) / (1.D0 - xmo_FR)
*
          xmo_XEJ_PIEGPOR(IVGA) =
     &      - xmo_PIEGPORJ * xmo_CXEJ(IVGA) / (1.D0 - xmo_FR) 
*
          xmo_XEJ_SOURCE(IVGA) = 
     &       ( 1.D0 - xmo_FR - xmo_FEXT - xmo_FPORJ - xmo_FBJ) 
     &         * xmo_SOURXE_J 
*
          xmo_XEJ_REMJOINT(IVGA) = - xmo_BJ * xmo_CXEJ(IVGA)
*
          IF (mo_DIF_SEQ .EQ. 1) THEN
            xmo_XEJ_DIFFUSION(IVGA) =
     &         - xmo_DXEJ / xmo_ASE**2.D0 *
     &           xmo_VECT1(IVGA)
*
            xmo_XEJ_CLIM3(IVGA) = 
     &          - xmo_DXEJ / xmo_ASE**2.D0 *
     &            xmo_CXEJLIM * xmo_TIMQSEQ(IVGA)
          ELSE
            xmo_XEJ_DIFFUSION(IVGA) = 0.D0
            xmo_XEJ_CLIM3(IVGA) = 0.D0
          ENDIF
*
          xmo_XEJ_SORTIEJ(IVGA) =  
     &       - xmo_DFRSDT *  xmo_CXEJ(IVGA)  / (1.D0 - xmo_FR)
*
 300    CONTINUE
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles inter xmo_CBJ
*
*
        xmo_BJ_NUCLHOMO =
     &       xmo_KGJ1 * xmo_KGJ2 * xmo_CXEJCMIN * xmo_DNUCLJ *
     &      (1.D0 - 
     &       min(xmo_KGJ2 * xmo_CBJ / (1.D0 - xmo_FR) / xmo_XNAVOG ,
     &       1.D0) )
     &      + 
     &      xmo_KGJ / xmo_MN * xmo_CXEJCM * xmo_DNUCLJ
     &      / (1.D0 - xmo_FR) 
* 
        IF (mo_IET .EQ. 0) THEN
          xmo_BJ_APPORT = 
     &      (1.D0 - xmo_FPORJ - xmo_FBJ - xmo_FEXT) 
     &       * xmo_HJ * xmo_CB * xmo_DLBJ
        ELSEIF (mo_IET .EQ. 2) THEN
          xmo_BJ_APPORT = 0.D0
        ENDIF
*
        xmo_BJ_COALEDBJ =
     &      - xmo_GCJDBJ * xmo_CBJ**2.D0 / (1.D0 - xmo_FR)
        xmo_BJ_COALEVBJ =
     &      - xmo_GCJVBJ * xmo_CBJ**2.D0 / (1.D0 - xmo_FR)
*
        IF (xmo_CBJ .GT. 0.D0) THEN
          xmo_BJ_COALEGEOMJ =
     &     - (xmo_BJ_NUCLHOMO)
     &     * (2.D0 * xmo_RBJ + xmo_DISTCOALJ)  
     &     * ( xmo_CBJ * 2.D0 / 3.D0 * xmo_AGG
     &         / (1.D0 - xmo_FR) )**(1.D0/2.D0) 
        ELSE
          xmo_BJ_COALEGEOMJ = 0 .D0
        ENDIF
*
        xmo_BJ_PIEGBPOR =
     &      - xmo_PIEGBJPORJ * xmo_CBJ / (1.D0 - xmo_FR)
*
        xmo_BJ_REMBUL =
     &      - xmo_BBJB * xmo_CBJ
*
        xmo_BJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_CBJ  / (1.D0 - xmo_FR)
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de bulles inter xmo_CXEBJ
*
*
        xmo_XEBJ_NUCLHOMO =
     &       xmo_KGJ1 * xmo_KGJ2 / xmo_XNAVOG * xmo_DNUCLJ *
     &       ( xmo_CXEJCMIN + xmo_CXEJCM / (1.D0 - xmo_FR) )
     &       + 
     &       xmo_KGJ * xmo_CXEJCM * xmo_DNUCLJ / (1.D0 - xmo_FR) 
*
        IF (mo_IET .EQ. 0) THEN
          xmo_XEBJ_APPORT =
     &      (1.D0 - xmo_FPORJ - xmo_FEXT) * xmo_HJ * xmo_CXEB 
     &      + xmo_FBJ * xmo_SOURXE_BJ 
        ELSEIF (mo_IET .EQ. 2) THEN
           xmo_XEBJ_APPORT= xmo_FBJ * xmo_SOURXE_BJ 
        ENDIF
*
        xmo_XEBJ_PIEGBUL =
     &    xmo_PIEGBJ * xmo_CXEJM / (1.D0 - xmo_FR)
*
        xmo_XEBJ_PIEGBPOR =
     &    - xmo_PIEGBJPORJ * xmo_CXEBJ / (1.D0 - xmo_FR) 
*
        xmo_XEBJ_REMBUL = - xmo_BBJ * xmo_CXEBJ
*
        xmo_XEBJ_PERCBPOR = xmo_FRCONNEC * xmo_KTUNJPORE * 
     &    (xmo_PINTPORJ**2.D0 - xmo_PINTBJ**2.D0)
*                  
        xmo_XEBJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_CXEBJ  / (1.D0 - xmo_FR)
* 
* xmo_PERCPLEN est forcement n�gatif : on interdit l'entr�e de gaz du 
* plenum dans les cavit�s interconnect�es du combustible
        xmo_PERCPLEN =
     &    min( - xmo_KTUNJ * xmo_FRCONNEC *
     &           (xmo_PINTBJ**2.D0 - xmo_PPLEN**2.D0) ,
     &         0.D0 )
*
* on doit limiter la perte de gaz par percolation pour que les tunnels
* ne soient pas vides de gaz. xmo_PERCPLEN_LIM est n�gatif
        xmo_PERCPLEN_LIM = 
     &          min( -1.D0 * xmo_COELIM_PERC *
     &                    ( xmo_XEBJ_NUCLHOMO + xmo_XEBJ_APPORT + 
     &                     xmo_XEBJ_PIEGBUL + xmo_XEBJ_REMBUL +
     &                     xmo_XEBJ_PERCBPOR )  ,
     &               0.D0 )
*
* xmo_XEBJ_PERCPLEN est n�gatif ou nul.
* En valeur absolue, c est la plus petite valeur de xmo_PERCPLEN ou
* de xmo_PERCPLEN_LIM qui est retenue
*
        xmo_XEBJ_PERCPLEN = max( xmo_PERCPLEN , xmo_PERCPLEN_LIM )
*
************************************************************************
* Application de l'effet cliquet sur les bulles intra : au-dessus d'une 
* certaine taille on force dCBJ/dt � �tre n�gatif ou nul
        CALL MOFCLIC(xmo_RBJ,xmo_BJ_NUCLHOMO,xmo_BJ_COALEGEOMJ,
     &               xmo_XEJ_NUCLHOMO, xmo_XEBJ_NUCLHOMO) 
*
************************************************************************
* d�riv�e temporelle du volume total de bulles inter xmo_VTBJ
*
        xmo_VTBJ_GAZ = 
     &       (  xmo_FBJ * xmo_SOURXE_BJ * xmo_TEST0 
     &        + xmo_XEBJ_PIEGBUL
     &        + xmo_XEBJ_NUCLHOMO
     &        + xmo_XEBJ_REMBUL ) 
     &       * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG
*
        IF (xmo_RBJ .GT. 0.D0) THEN
          xmo_VTBJ_LACUNE = 
     &           (xmo_PVIJ1_B * xmo_RBJ + xmo_PVIJ2 )
     &           * ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2BJ )
     &           * xmo_CBJ 
        ELSE
          xmo_VTBJ_LACUNE = 0.D0
        ENDIF
*
        xmo_VTBJ_PIEGBPOR = 
     &     - xmo_PIEGBJPORJ * xmo_VTBJ / (1.D0 - xmo_FR) 
*
* terme de diminution du volume des bulles inter du a la disparition
* des lacunes uniquement des bulles d�truites
        xmo_VTBJ_REMBUL = 
     &     - xmo_BBJB * ( xmo_VTBJ 
     &                    - xmo_CXEBJ * xmo_ZETA 
     &                      * xmo_OMEGA * xmo_XNAVOG )
*
        IF (mo_IET .EQ. 0) THEN
          xmo_VTBJ_APPORT = 
     &     (1.D0 - xmo_FPORJ - xmo_FEXT) * xmo_HJ * xmo_VTB 
        ELSEIF (mo_IET .EQ. 2) THEN
          xmo_VTBJ_APPORT= 0.D0
        ENDIF 
*
        xmo_VTBJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_VTBJ  / (1.D0 - xmo_FR)
* 
************************************************************************
************************************************************************
* d�riv�e temporelle de la concentration en pores inter xmo_CPORJ
*
        xmo_PORJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_CPORJ  / (1.D0 - xmo_FR)
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores inter xmo_CXEPORJ
*
        xmo_XEPORJ_PIEGPOR =
     &     xmo_PIEGPORJ * xmo_CXEJM / (1.D0 - xmo_FR) 
*
        xmo_XEPORJ_PIEGBPOR =
     &     xmo_PIEGBJPORJ * xmo_CXEBJ / (1.D0 - xmo_FR) 
*
        xmo_XEPORJ_REMPOR = - xmo_BPORJ * xmo_CXEPORJ
*
        IF (mo_IET .EQ. 0) THEN
          xmo_XEPORJ_APPORT =
     &       (1.D0  - xmo_FEXT) * xmo_HJPOR * xmo_CXEPOR
     &     + xmo_FPORJ * ( xmo_HJ * xmo_CXEB + xmo_SOURXE_PORJ ) 
        ELSEIF (mo_IET .EQ. 2) THEN
          xmo_XEPORJ_APPORT = xmo_FPORJ * xmo_SOURXE_PORJ
        ENDIF 
*
        xmo_XEPORJ_PERCBPOR = - xmo_FRCONNEC * xmo_KTUNJPORE * 
     &    (xmo_PINTPORJ**2.D0 - xmo_PINTBJ**2.D0)
*
        xmo_XEPORJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_CXEPORJ  / (1.D0 - xmo_FR)
*
************************************************************************
* d�riv�e temporelle du volume total des pores inter xmo_VTPORJ
*
        xmo_VTPORJ_GAZ =
     &     (  xmo_FPORJ * xmo_SOURXE_PORJ * xmo_TEST0
     &      + xmo_XEPORJ_PIEGPOR
     &      + xmo_XEPORJ_REMPOR )
     &     * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG 
*
        IF (xmo_RPORJ .GT. 0.D0) THEN
          xmo_VTPORJ_LACUNE = (xmo_PVIJ1_POR * xmo_RPORJ + xmo_PVIJ2)
     &            * ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2PORJ )
     &            * xmo_CPORJ 
        ELSE
          xmo_VTPORJ_LACUNE = 0.D0
        ENDIF
*
        xmo_VTPORJ_PIEGBPOR =
     &     xmo_PIEGBJPORJ * xmo_VTBJ / (1.D0 - xmo_FR)
*
        IF (mo_IET .EQ. 0) THEN
          xmo_VTPORJ_APPORT = 
     &       xmo_FPORJ * xmo_HJ * xmo_VTB 
     &       + (1.D0  - xmo_FEXT) * xmo_HJPOR * xmo_VTPOR
        ELSEIF (mo_IET .EQ. 2) THEN
          xmo_VTPORJ_APPORT = 0.D0
        ENDIF 
*
        xmo_VTPORJ_SORTIEJ =  
     &      - xmo_DFRSDT *  xmo_VTPORJ  / (1.D0 - xmo_FR)
*
************************************************************************
* pour la v�rification des bilans de gaz uniquement :
************************************************************************
        IF (mo_DIF_SEQ .EQ. 1) THEN  
* Calcul du gradient de concentration en gaz inter dissous � la surface
* de la sph�re �quivalente
*
          CALL MOGRAD_VF (xmo_CXEJ,xmo_CXEJLIM, xmo_ASE, 1, xmo_GRADJ)
*
*
* Calcul du taux de sortie mol/m3/s de gaz dissous en inter, 
* sortant de la sph�re �quivalente par diffusion
*
          xmo_OUTSE = - 3.D0 / xmo_ASE * xmo_DXEJ * xmo_GRADJ 
*
        ELSE
          xmo_OUTSE = 0.D0
        ENDIF
************************************************************************
* Calcul du terme source total de gaz inter, venant de l'intra
* 
*
        xmo_SOURXE = 
     &        xmo_FBJ * xmo_SOURXE_BJ +
     &        xmo_FPORJ *  xmo_SOURXE_PORJ +
     &        (1.D0 - xmo_FR - xmo_FEXT - xmo_FBJ - xmo_FPORJ) 
     &        * xmo_SOURXE_J
*
************************************************************************
* Calcul du taux de sortie par ejection
        xmo_OUTEJEC = 
     &        xmo_FEXT * xmo_SOURXE_EXT 
     &      + xmo_FEXT * xmo_HJ * xmo_CXEB
     &      + xmo_FEXT * xmo_HJPOR * xmo_CXEPOR
*
************************************************************************
C
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG        ENDIF
C
*
      RETURN
      END
      SUBROUTINE MOTRM2(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_APPORT,
     &      xmo_BRIM_APPORT,
     &      xmo_XEBRIM_APPORT,
     &      xmo_VTBRIM_APPORT,
     &      xmo_PORRIM_APPORT,
     &      xmo_XEPORRIM_APPORT,
     &      xmo_VTPORRIM_APPORT )
C     =================
C
C
C     ******************************************************************
C     MOTRM2 : MOgador calcul des TERMes utiles dans l'expression des 
C              d�riv�es du vecteur des variables
C      - �quations concernant les variables des zones restructur�es
C      - apparaissnt dans l'�tat 'en restructuration' (2) uniquement                                 
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpnu.inc'
C
***********************************************************************
* cmogpas.inc : xmo_FACRHO en entr�e
* cmogppr.inc : xmo_ZETA  
*               xmo_RHODL xmo_RHOLIM xmo_MARGRHO_FR xmo_CBRIM0 
*               xmo_DREM mo_MODELE_FRAC en entr�e
* cmogphy.inc : xmo_PIZAHL xmo_XNAVOG  xmo_OMEGA en entr�e
* cmogmat.inc : xmo_TVGG  en entr�e
* cmogdon.inc : xmo_AGG  en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQR en entr�e
************************************************************************
* entr�es et sorties en param�tres
* premier bloc de param�tres  : entr�es 
* Il s'agit - des variables zones saines (gaz dissous, bulles et pores), 
*           - de la densit� de dislocation
*
* deuxi�me bloc de param�tres : sorties
* Il s'agit - de termes dans l'expression du vecteur d�riv�e du vecteur des variables
*             Ces termes sont sp�cifiques � l'�tat 'en restructuration' (2) et 
*             n'apparaitront plus pour l'�tat 'restructur�' (3)
*
* remarque g�n�rale : les termes des �quations calcul�s ci-dessous incluent 
* toujours leur signe. Autrement dit, dans les �quations , on aura que des +
* entre ces termes.
*
* pour equation du gaz dissous dans le milieu restructur� :
* xmo_XEHOM_APPORT : Vecteur terme d'apport de gaz  depuis les 
*                    zones non restructur�es(mol/s/m3)
*
* pour equation des bulles de rim     
* xmo_BRIM_APPORT : Terme de cr�ation des bulles de rim pendant la restructuration (bulles/s/m3)
* xmo_XEBRIM_APPORT : Terme de transfert de gaz des zones saines aux bulles de rim 
*                     au cours de la restructuration (mol/s/m3)
* xmo_VTBRIM_APPORT : Terme de transfert de volume des zones saines aux bulles de rim 
*                     au cours de la restructuration (mol/s/m3)
*
*
* pour equation des pores de rim     
* xmo_PORRIM_APPORT : Terme de cr�ation des pores de rim pendant la restructuration (bulles/s/m3)
* xmo_XEPORRIM_APPORT : Terme de transfert de gaz des zones saines aux pores de rim 
*                     au cours de la restructuration (mol/s/m3)
* xmo_VTPORRIM_APPORT : Terme de transfert de volume des zones saines aux pores de rim 
*                     au cours de la restructuration (mol/s/m3)
*
*
*
************************************************************************
* Variables internes utilis�es dont la signification physique est importante :
* xmo_CXEM : moyenne du gaz dissous des zones saines (mol/m3)
* xmo_CXEJM : moyenne du gaz dissous inter (mol/m3)         
************************************************************************
*
*-----7--0---------0---------0---------0---------0---------0---------0--*
*
      DOUBLE PRECISION 
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_XEHOM_APPORT(mo_NNSPHMX)

*
      CALL MOMESS('DANS MOTRM2')
*
************************************************************************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
*
************************************************************************
* A FAIRE POUR l ETAT 'EN RESTRUCTURATION' (2) UNIQUEMENT
************************************************************************
*
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM+xmo_RHODL pour les calcul de xmo_FR xmo_A 
* Sinon, on a des divisions par zero
*
      xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
      xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
      CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
*
*
* Calcul de la d�riv�e temporelle de FR : xmo_DFRSDT
      IF ( xmo_RHOD .LT. xmo_RHOD_FRLIM ) THEN
        IF (mo_MODELE_FRAC .EQ. 1) THEN
          xmo_DFRSDT = xmo_PIZAHL / 4.D0 / xmo_RHODL *
     &      cos( xmo_PIZAHL / 2.D0 / xmo_RHODL *
     &      ( xmo_RHOLIM - xmo_RHOD1 ) ) * xmo_FACRHO * xmo_RHOD1
        ELSEIF (mo_MODELE_FRAC .EQ. 2) THEN
          xmo_DFRSDT = xmo_FACRHO * xmo_RHOD1
     &                 / 2.D0 / xmo_RHODL
        ENDIF
      ELSE
        xmo_DFRSDT = 0.D0
      ENDIF
*
************************************************************************
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz de zone saine dissous 
*
      CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
      IF (mo_DIF_SEQ .EQ. 1) THEN        
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &               mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
      ELSE
          xmo_CXEJM = xmo_CXEJ(1)
      ENDIF
*
*
************************************************************************
************************************************************************
* TERMES APPARAISSANT UNIQUEMENT DANS LES EQUATIONS DE L'ETAT 
* 'EN RESTRUCTURATION' (2)
************************************************************************
************************************************************************
* calculs des termes de l'�quation du gaz dissous dans le milieu restructur�
*
*
        DO 100 IVGA = 1, mo_NSEQR
          xmo_XEHOM_APPORT(IVGA) =
     &     xmo_DFRSDT * (xmo_CXEM + xmo_CXEJM 
     &                   + xmo_CXEBJ + xmo_CXEB + xmo_CXEPOR)
     &       / (1.D0-xmo_FR)    
*
 100    CONTINUE

*
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles de rim 
*
        xmo_BRIM_APPORT = xmo_DFRSDT * xmo_CBRIM0
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les bulles de rim xmo_CXEBRIM
*
        xmo_XEBRIM_APPORT = 0.D0
*
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des bulles de rim xmo_VBRIM
*
*
        xmo_VTBRIM_APPORT =
     &      xmo_DFRSDT 
     &      * (  xmo_VTBJ  + xmo_VTB + xmo_VTPOR
     &           - (xmo_CXEBJ + xmo_CXEB + xmo_CXEPOR) 
     &              * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG  )
     &       / (1.D0-xmo_FR)
*
************************************************************************
* d�riv�e temporelle de la concentration en pores de rim 
*
        xmo_PORRIM_APPORT = 
     &      xmo_DFRSDT * xmo_CPORJ / (1.D0-xmo_FR)
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores de rim xmo_CXEPORRIM
*
        xmo_XEPORRIM_APPORT =
     &      xmo_DFRSDT * xmo_CXEPORJ / (1.D0-xmo_FR)
*
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores de rim xmo_VTPORRIM
*
*
        xmo_VTPORRIM_APPORT =
     &      xmo_DFRSDT * xmo_VTPORJ / (1.D0-xmo_FR)
*
************************************************************************
*
*
      RETURN
      END
      SUBROUTINE MOTRM3(
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_CREATION,xmo_XEHOM_DIFFUSION,
     &      xmo_XEHOM_CLIM3,
     &      
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ,
     &      
     &      xmo_CXEHOMM,xmo_OUTSE,xmo_OUTEJEC )
C     =================
C
C
C     ******************************************************************
C     MOTRM3 : MOgador calcul des TERMes utiles dans l'expression des d�riv�es
C      (�quations concernant les zones restructur�es)
C      dans l'�tat 'en restructuration' (2) ou restructur� (3)                                 
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpte.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
***********************************************************************
* cmogpas.inc :  xmo_BETAC xmo_OMSKT xmo_PVI  
*                xmo_DXE  en entr�e
* cmogppr.inc : xmo_ZETA  xmo_MARGRHO_FR 
*               xmo_RHODL xmo_RHOLIM xmo_APG  xmo_COE_LAC_RIM en entr�e
* cmogphy.inc :  xmo_XNAVOG  xmo_OMEGA en entr�e
* cmogmat.inc : xmo_TVSEQR  xmo_TIMRSEQR xmo_TIMQSEQR en entr�e
* cmogpnu.inc : mo_NSEQR  xmo_RSEQR en entr�e
* cmogent.inc : xmo_PHYDR  en entr�e
* cmogvar.inc : mo_IET en entr�e xmo_ASERIM en sortie
* cmogpte.inc :   en entr�e
************************************************************************
* entr�es et sorties en param�tres
* premier bloc de param�tres  : entr�es
*
* -->Pour les �tats (2) et (3)
* Il s'agit - des variables zones restructur�es (gaz dissous dans les petits grains,  bulles, pores de rim), 
*           - de la densit� de dislocation des zones saines (qui sert uniquement � calculer xmo_FR)
*
* deuxi�me bloc de param�tres : sorties
* Il s'agit de  termes apparaissant dans les �quations des variables des zones restructur�es
* pour les �tats 'en restructuration' (2) ou restructur� (3)
* remarque g�n�rale : les termes des �quations calcul�s ci-dessous incluent 
* toujours leur signe. Autrement dit, dans les �quations , on aura que des +
* entre ces termes.
*
* pour equation du gaz dissous dans les petits grains du milieu restructur� :
* xmo_XEHOM_CREATION : Vecteur terme de cr�ation de gaz par les pointes de fission dans le milieu restructur� (mol/s/m3)
* xmo_XEHOM_DIFFUSION : Vecteur terme de diffusion de gaz dissous (mol/s/m3)
* xmo_XEHOM_CLIM3 : Vecteur terme permettant de tenir compte d'une �ventuelle condition 
*                   � la limite non nulle (mol/s/m3) (ce terme est non nul en EF et en VF ) 
*
* Remarque : dans l'�tat (2) un terme suppl�mentaire xmo_XEHOM_APPORT apparait dans l'�quation. 
*            Il est calcul� par motrm2.f
* xmo_XEHOM_APPORT : Vecteur terme d'apport de gaz par basculement d'une zone non restructur�e
* qui devient restructur�e (mol/s/m3)

* pour equation des bulles de rim     
* xmo_XEBRIM_ALIM_DIRECTE : terme de piegeage du gaz dissous par les bulles de rim . Le gaz provient 
* directement des petits grains bordant la bulle de rim(mol/s/m3) 
* xmo_XEBRIM_ALIM_ECHAP : terme d'arriv�e de gaz dans les bulles de rim par percolation
* le long de tunnels fins, instables aux joints des petits grains (tunnels non mod�lis�s) (mol/s/m3) 
* xmo_XEBRIM_REMBRIM : terme de remise en solution de gaz depuis les bulles de rim (mol/s/m3)
* xmo_VTBRIM_LACUNE : terme de piegeage de lacunes par les bulles de rim (s-1)
* xmo_VTBRIM_GAZ : apport de volume avec le gaz vers les bulles de rim (s-1)
*
* Remarque : dans l'�tat (2) des termes suppl�mentaires xmo_BRIM_APPORT  xmo_XEBRIM_APPORT xmo_VTBRIM_APPORT
*            apparaissent dans l'�quation. 
*            Il sont calcul�s par motrm2.f
* xmo_BRIM_APPORT : Terme de cr�ation des bulles de rim pendant la restructuration (bulles/s/m3) 
* xmo_XEBRIM_APPORT : Terme de transfert de gaz des zones saines aux bulles de rim 
*                     au cours de la restructuration (mol/s/m3)
* xmo_VTBRIM_APPORT : Terme de transfert de volume des zones saines aux bulles de rim 
*                     au cours de la restructuration (mol/s/m3)
*
*  pour equation des pores de rim      
* xmo_XEPORRIM_ALIM_DIRECTE : terme de piegeage du gaz dissous par les pores de rim . Le gaz provient 
* directement des petits grains bordant le pore de rim(mol/s/m3) 
* xmo_XEPORRIM_ALIM_ECHAP : terme d'arriv�e de gaz dans les pores de rim par percolation
* le long de tunnels fins, instables aux joints des petits grains (tunnels non mod�lis�s) (mol/s/m3) 
* xmo_XEPORRIM_REMPORRIM: terme de remise en solution de gaz depuis les pores de rim (mol/s/m3)
* xmo_VTPORRIM_LACUNE : terme de piegeage de lacunes par les pores de rim (s-1)
* xmo_VTPORRIM_GAZ : apport de volume avec le gaz vers les pores de rim (s-1)
*
*
*
*
*
************************************************************************
* Variables internes utilis�es dont la signification physique est importante :
* xmo_BPORRIM : fr�quence de remise en solution du gaz des pores de rim (s-1)
* xmo_BBRIM : fr�quence de remise en solution du gaz des bulles de rim (s-1)
* xmo_CXEHOMM : moyenne du gaz dissous dans les petits grains (mol/m3)
* xmo_TEXP2PORRIM : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � un pore de rim ()
* xmo_TEXP2BRIM : Deuxi�me exponentielle apparaissant dans le terme 
*              d'apport de lacunes � une bulle de rim ()
* xmo_BPORRIM : fr�quence de remise en solution du gaz des pores de rim (s-1)
* xmo_BBRIM : fr�quence de remise en solution du gaz des bulles de rim (s-1)
*         
************************************************************************
      DOUBLE PRECISION 
     &                 xmo_CXEHOM(mo_NNSPHMX),
     &                 xmo_VECT1(mo_NNSPHMX),
     &                 xmo_XEHOM_CREATION(mo_NNSPHMX),
     &                 xmo_XEHOM_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEHOM_CLIM3(mo_NNSPHMX)
*-----7--0---------0---------0---------0---------0---------0---------0--
*
       CALL MOMESS('DANS MOTRM3')
*
************************************************************************
************************************************************************
* ZONE RESTRUCTUREE POUR ETAT 'EN RESTRUCTURATION' (2)
* OU ETAT RESTRUCTURE (3)
************************************************************************
************************************************************************
*
*
************************************************************************
*
        IF (mo_IET .EQ. 2) THEN
*
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM+xmo_RHODL pour le calcul de xmo_FR  
* Pour coh�rence avec moterm et motrm2.
*
          xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
          xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
* 
        ELSE
          xmo_FR = 1.D0
        ENDIF
*
************************************************************************
*
* Calcul du rayon de la sph�re �quivalente pour le rim xmo_ASERIM
*
        CALL MOASER(xmo_FR)
*
************************************************************************
*
* Calcul de la moyenne xmo_CXEHOMM des concentrations en 
* gaz dissous dans les zones restructur�es 
*
          CALL MOMOYE (xmo_CXEHOM, xmo_TVSEQR, 
     &                 mo_NSEQR, mo_NNSPHMX, xmo_CXEHOMM)
*
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BPORRIM
* et xmo_BPORRIMB des pores de rim
*
*
        CALL MOBLOC(xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,
     &     xmo_RPORRIM,7,xmo_BPORRIM,xmo_BPORRIMB,xmo_TEXP2PORRIM)
*
************************************************************************
* Coefficients de remise en solution du gaz xmo_BBRIM
* et xmo_BBRIMB des bulles de rim
* 
        CALL MOBLOC(xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,
     &     xmo_RBRIM,6,xmo_BBRIM,xmo_BBRIMB,xmo_TEXP2BRIM)
*
************************************************************************
* Calcul des taux de couverture des petits grains
*
      CALL MOCAFF_RIM(xmo_CPORRIM,xmo_VTPORRIM,
     &                xmo_CBRIM,xmo_VTBRIM,xmo_FR,
     &              xmo_FBRIM,xmo_FPORRIM,xmo_FEXT_RIM,
     &              xmo_FECHAP_EXT,xmo_FECHAP_BRIM,xmo_FECHAP_PORRIM,
     &              xmo_F_FLUXNUL)
*
************************************************************************
*  Calcul des concentrations de gaz � la limite du petit grain en face des 
*  diff�rentes populations
*
          xmo_CXENSEQR = xmo_CXEHOM(mo_NSEQR)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR ,
     &                mo_NSEQR, xmo_RSEQR, 
     &                xmo_CXEBRIM / xmo_FR, xmo_BBRIM, 
     &                xmo_FBRIM ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_BRIM)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR, 
     &                mo_NSEQR, xmo_RSEQR, 
     &                xmo_CXEPORRIM / xmo_FR, xmo_BPORRIM, 
     &                xmo_FPORRIM ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_PORRIM)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR,
     &                mo_NSEQR, xmo_RSEQR,  
     &                0.D0, 0.D0, 
     &                xmo_FECHAP_EXT ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_ECHAP_EXT)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR , 
     &                mo_NSEQR, xmo_RSEQR,
     &                0.D0, 0.D0, 
     &                xmo_FECHAP_BRIM ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_ECHAP_BRIM)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR , 
     &                mo_NSEQR, xmo_RSEQR,
     &                0.D0, 0.D0, 
     &                xmo_FECHAP_PORRIM ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_ECHAP_PORRIM)
*
          CALL MOCACR(xmo_CXENSEQR / xmo_FR , 
     &                mo_NSEQR, xmo_RSEQR,
     &                0.D0, 0.D0, 
     &                xmo_FEXT_RIM ,
     &                0.D0, 0.D0, 
     &                xmo_APG, xmo_DXE, xmo_CXECR_EXT_RIM)
*
CDEBUG
CDEBUG           IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &          mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI 
CDEBUG     &          .AND. mo_FLAG .EQ. 1) THEN
CDEBUG           WRITE(20,*) 'dans motrm3 xmo_CXENSEQR / xmo_FR = ',
CDEBUG     &                  xmo_CXENSEQR / xmo_FR 
CDEBUG           WRITE(20,*) 'dans motrm3 xmo_CXECR_BRIM = ',xmo_CXECR_BRIM
CDEBUG           WRITE(20,*) 'dans motrm3 xmo_CXECR_ECHAP_EXT = ',
CDEBUG     &          xmo_CXECR_ECHAP_EXT
CDEBUG           ENDIF
CDEBUG
*
          xmo_CXECR = xmo_FR * (
     &         xmo_FBRIM * xmo_CXECR_BRIM +
     &         xmo_FPORRIM * xmo_CXECR_PORRIM +
     &         xmo_FECHAP_EXT * xmo_CXECR_ECHAP_EXT +
     &         xmo_FECHAP_BRIM * xmo_CXECR_ECHAP_BRIM +
     &         xmo_FECHAP_PORRIM * xmo_CXECR_ECHAP_PORRIM +
     &         xmo_FEXT_RIM * xmo_CXECR_EXT_RIM) +
     &         xmo_F_FLUXNUL * xmo_CXENSEQR
*
************************************************************************
************************************************************************
* Calcul des gradients de gaz � l'int�rieur du petit  grain face � chacune des 
* populations
* Flux nul impos� sur la portion de surface  xmo_F_FLUXNUL
*
       xmo_GRAD_BRIM = -2.D0 * xmo_CXECR_BRIM / xmo_DREM
       xmo_GRAD_PORRIM = -2.D0 * xmo_CXECR_PORRIM / xmo_DREM
       xmo_GRAD_ECHAP_EXT = -2.D0 * xmo_CXECR_ECHAP_EXT / xmo_DREM
       xmo_GRAD_ECHAP_BRIM = -2.D0 * xmo_CXECR_ECHAP_BRIM / xmo_DREM
       xmo_GRAD_ECHAP_PORRIM = 
     &              -2.D0 * xmo_CXECR_ECHAP_PORRIM / xmo_DREM
       xmo_GRAD_EXT_RIM = 
     &              -2.D0 * xmo_CXECR_EXT_RIM / xmo_DREM
*
************************************************************************
* Calcul des termes source de gaz provenant des petits grains (mol/m3/s)
* face � chaque population
* ATTENTION
* il s'agit ici de mol/s et par 'm3 de volume restructur�'
        xmo_SOURXE_BRIM =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_BRIM
     &        * xmo_FBRIM 
*
        xmo_SOURXE_PORRIM =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_PORRIM 
     &        * xmo_FPORRIM 
*
        xmo_SOURXE_ECHAP_EXT =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_ECHAP_EXT
     &        * xmo_FECHAP_EXT  
*
        xmo_SOURXE_ECHAP_BRIM =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_ECHAP_BRIM
     &        * xmo_FECHAP_BRIM 
*
        xmo_SOURXE_ECHAP_PORRIM =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_ECHAP_PORRIM
     &        * xmo_FECHAP_PORRIM 
*
        xmo_SOURXE_EXT_RIM =
     &      - 3.D0 / xmo_APG * xmo_DXE * xmo_GRAD_EXT_RIM
     &        * xmo_FEXT_RIM 
************************************************************************
************************************************************************
************************************************************************
* TERMES POUR EQUATIONS ZONES RESTRUCTUREES
************************************************************************
************************************************************************
* calculs des termes de l'�quation du gaz dissous dans le mileu restructur�
*
*
        CALL XMMVEC_TRIDIAG (mo_NSEQR,mo_NSEQR,mo_NNSPHMX,mo_NNSPHMX, 
     &               xmo_TIMRSEQR,xmo_CXEHOM,xmo_VECT1)
*
        DO 100 IVGA = 1, mo_NSEQR
*
          xmo_XEHOM_CREATION(IVGA) =
     &           xmo_BETAC *  xmo_FR 
*
          xmo_XEHOM_DIFFUSION(IVGA) =
     &       - xmo_DXE / xmo_APG**2.D0 * xmo_VECT1(IVGA)
*
           xmo_XEHOM_CLIM3(IVGA) = 
     &          - xmo_DXE / xmo_APG**2.D0 
     &            * xmo_CXECR * xmo_TIMQSEQR(IVGA)
*
 100    CONTINUE
*
*
************************************************************************
* d�riv�e temporelle de la concentration en pores de rim : elle est nulle
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les pores de rim xmo_CXEPORRIM
*
        xmo_XEPORRIM_ALIM_DIRECTE =
     &     xmo_SOURXE_PORRIM * xmo_FR
        xmo_XEPORRIM_ALIM_ECHAP =
     &     xmo_SOURXE_ECHAP_PORRIM * xmo_FR
        xmo_XEPORRIM_REMPORRIM =
     &    - xmo_BPORRIM * xmo_CXEPORRIM
*
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores de rim xmo_VTPORRIM
* xmo_COE_VOL_ALIM_RIM vaut 0 ou 1
* 0 : on consid�re que seul le gaz arrive dans la cavit� (percolation)
* 1 : on consid�re que le gaz et les lacunes associ�es arrivent � la cavit�
* ou que les joints de grains proches de la cavit� s'ouvrent et permettent 
* � la cavit� de gagner du volume
*
        xmo_VTPORRIM_GAZ =
     &       ( xmo_XEPORRIM_ALIM_DIRECTE
     &         + xmo_COE_VOL_ALIM_RIM * xmo_XEPORRIM_ALIM_ECHAP
     &         + xmo_XEPORRIM_REMPORRIM ) 
     &       * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG
*
        IF (xmo_RPORRIM .GT. 0.D0) THEN

          xmo_VTPORRIM_LACUNE = 
     &            xmo_PVI * xmo_RPORRIM * xmo_COE_LAC_RIM *
     &            ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2PORRIM )
     &            * xmo_CPORRIM
        ELSE
          xmo_VTPORRIM_LACUNE = 0.D0
        ENDIF
*
*
*
************************************************************************
* d�riv�e temporelle de la concentration en bulles de rim 
*
* le terme est calcul� dans motrm2.f pour l'�tat 'en restructuration' (2)
* Dans l'�tat restructur� (3) la concentration en bulles de rim est constante
*
*
************************************************************************
* d�riv�e temporelle de la concentration en gaz de fission 
* dans les bulles de rim xmo_CXEBRIM
*
        xmo_XEBRIM_ALIM_DIRECTE =
     &   xmo_SOURXE_BRIM * xmo_FR
        xmo_XEBRIM_ALIM_ECHAP =
     &     xmo_SOURXE_ECHAP_BRIM * xmo_FR  
        xmo_XEBRIM_REMBRIM =
     &    - xmo_BBRIM * xmo_CXEBRIM
*
*
*
*
*
************************************************************************
* d�riv�e temporelle du volume total des pores de rim xmo_VTPORRIM
* xmo_COE_VOL_ALIM_RIM vaut 0 ou 1
* 0 : on consid�re que seul le gaz arrive dans la cavit� (percolation)
* 1 : on consid�re que le gaz et les lacunes associ�es arrivent � la cavit�
* ou que les joints de grains proches de la cavit� s'ouvrent et permettent 
* � la cavit� de gagner du volume
*
        xmo_VTBRIM_GAZ =
     &       ( xmo_XEBRIM_ALIM_DIRECTE 
     &         + xmo_COE_VOL_ALIM_RIM * xmo_XEBRIM_ALIM_ECHAP
     &         + xmo_XEBRIM_REMBRIM ) 
     &       * xmo_ZETA * xmo_OMEGA * xmo_XNAVOG
*
        IF (xmo_RBRIM .GT. 0.D0) THEN
          xmo_VTBRIM_LACUNE = 
     &            xmo_PVI * xmo_RBRIM * xmo_COE_LAC_RIM *
     &            ( exp(-xmo_PHYDR * xmo_OMSKT) - xmo_TEXP2BRIM )
     &            * xmo_CBRIM 
        ELSE
          xmo_VTBRIM_LACUNE = 0.D0
        ENDIF
*
*
*
*
************************************************************************
*
* pour la v�rification des bilans de gaz uniquement :
************************************************************************  
*
*
* Calcul du taux de sortie mol/m3/s de gaz s'�chappant vers l'exterieur
*
        xmo_OUTSE = xmo_SOURXE_ECHAP_EXT * xmo_FR
************************************************************************
*
*
* Calcul du taux de sortie mol/m3/s de gaz sortant directement des 
* petits grains vers l'exterieur (ejection)
*
        xmo_OUTEJEC = xmo_SOURXE_EXT_RIM * xmo_FR
************************************************************************
*
*
      RETURN
      END
*
*
      SUBROUTINE MOVADF
*     =================
*
*     ******************************************************************
*     MOVADF : MOgador VAleurs par D�Faut des param�tres 
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogpte.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmoggpt.inc'
************************************************************************      
* cmogpte.inc : toutes en sorties
* cmogppr.inc : toutes en sorties
* cmogpnu.inc : toutes en sorties
* cmoginf.inc : toutes en sorties
* cmogear.inc : mo_IOPT mo_ITASK mo_ITOL mo_JT en sortie
* cmoggpt.inc : toutes en sorties
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
*
************************************************************************
* Valeur par d�faut des param�tres primaires
************************************************************************
*
* Le gaz contenu initialement dans les pores 
* est diff�rent du gaz de fission
* 0 : NON on traite de le gaz initial comme du Xe
* 1 : OUI le gaz initial reste toujours dans les pores
*
      mo_MODELE_GAZPORE = 0
*
* Traitement des pores
* 0 : MARGARET calcul Vpor Cpor Vporj Cporj
* 1 : Un mod�le simple de METEOR fourni le volume total de pores 
*     (Vpor + Vporj) au cours du temps On garde Cpor et Cporj initial
* 2 : MOGADOR-DENS de METEOR fourni Vpor Cpor Vporj Cporj
*
      mo_MODELE_DENSIFICATION = 1
*
* Diffusion a grande echelle du gaz sur le joint de grain
* 0 : jamais consid�r�e
* 1 : toujours consid�r�e
*
      mo_DIF_SEQ_LU = 0
*
* Rayon de la sph�re �quivalente dans l'�tat classique lu 
* dans l'historique ou impos�
* 0 : lu dans l'historique
* 1 : impos�
* Si le rayon est impos�, on se servira de xmo_ASE_CENTRE pour 
* tous les points  de la pastille sauf le point du bord, o� 
* on se servira de xmo_ASE_BORD. Si le rayon n'est pas impos�
* on ne se sert pas de xmo_ASE_CENTRE et xmo_ASE_BORD
*
      mo_ASE_IMPOSE = 0
      xmo_ASE_CENTRE = 0
      xmo_ASE_BORD = 0
*
* Equation diff�rentielle coupl�e au syst�me pour 
* le calcul de xmo_CXERBLOC
* concentrattion locale en gaz dissous au voisinnage 
* des bulles (zone de remise en solution)
* 0 : pas d'�quation diff�rentielle
* 1 : �quation diff�rentielle
*
      mo_EQUA_CXERBLOC = 0
*
* Remise en solution
* 1 : modele de NELSON
* 2 : modele simple
* 3 : modele de NELSON sans NRE
*
      mo_RESOL = 3
      xmo_FACBJ = 1.6248D-24
      xmo_DREM = 2.D-9
      xmo_MUF = 6.D-6
      xmo_EFFMAX = 67.D6*1.6e-19
      xmo_EREIMP = 300.D0*1.6e-19
      xmo_EPECGD = 1.5D-9
      xmo_COECASC = 20.D0
      xmo_BBJ_S_BB = 0.2D0
      mo_REM_CAV_RIM = 0
      mo_REM_PORE_INTER = 1
*
* controle de dnucl
      mo_INUCL = 4
      xmo_RBNU(1) = 60.D-9
      xmo_RBNU(2) = 85.D-9
      xmo_EXP_DNUCL = 1.D0
      mo_CLIC = 0
      xmo_RCLIC(1) = 1.
      xmo_RCLIC(2) = 1.1
*
* controle de ddestr
      mo_IDEST = 1
      xmo_EXP_DDESTR = 0.7D0
*
* nucleation
      xmo_FNB = 1.D0
      xmo_MN = 3.32D-24
      xmo_FNBJ = 1.D0
      xmo_FNBJP = 0.D0
      xmo_MUF_CHOC = 2.D-6
*
* coalescence geometrique intra
      xmo_TKMIN = 1073.D0
      xmo_TKMAX = 1074.
      xmo_STFMIN = 1.D27
      xmo_STFMAX1 = 1.1D27
      xmo_STFMAX2 = 2.D27
      xmo_MIN_DISTCOAL = 7.D-9
      xmo_MAX_DISTCOAL1 = 400.D-9
      xmo_MAX_DISTCOAL2 = 400.0001D-9
*
* coalescence geometrique inter
      xmo_TKMINJ = 1073.D0
      xmo_TKMAXJ = 1074.
      xmo_STFMINJ = - 1.D27
      xmo_STFMAXJ1 = - 0.9D27
      xmo_STFMAXJ2 = - 0.3D27
      xmo_MIN_DISTCOALJ = 7.D-9
      xmo_MAX_DISTCOALJ1 = 40.D-9
      xmo_MAX_DISTCOALJ2 = 40.001D-9
*
* evolution possible du coefficient de diffusion du Xe 
* avec la densit� de dislocation locale
* 0 : pas d'evolution
* 1 : evolution
*
      mo_DIFXE_AMELIOREE = 0
*
* Coefficient de diffusion Xe
      xmo_TDXE(1) = 5.D-5
      xmo_TDXE(2) = 45200.D0
      xmo_TDXE(3) = 6.95D-25
      xmo_TDXE(4) = 13870.D0
      xmo_TDXE(5) = 6.D-40
*
* Coefficient diffusion U en surface
      xmo_TDS(1) = 50.D0
      xmo_TDS(2) = 54122.D0
      xmo_TDS(3) = 0.D0
      xmo_TDS(4) = 0.D0
      xmo_TDS(5) = 0.D0
*
* Coefficient diffusion U en volume proche surface
* pour UO2
      xmo_TDUS(1,1) = 1.09D-4
      xmo_TDUS(2,1) = 52848.D0
      xmo_TDUS(3,1) = 0.D0
      xmo_TDUS(4,1) = 0.D0
      xmo_TDUS(5,1) = 0.D0
*
* pour UPUO2
      xmo_TDUS(1,2) = 5.46D-14
      xmo_TDUS(2,2) = 15636.D0
      xmo_TDUS(3,2) = 0.D0
      xmo_TDUS(4,2) = 0.D0
      xmo_TDUS(5,2) = 0.D0
*
* Coefficient xmo_DU empirique apport de lacune
* aux cavit�s : on a pris xmo_DUS + terme athermique
* pour UO2
      xmo_TDU(1,1) = 1.09D-4
      xmo_TDU(2,1) = 52848.D0
      xmo_TDU(3,1) = 0.D0
      xmo_TDU(4,1) = 0.D0
      xmo_TDU(5,1) = 0.2D-40
*
* pour UPUO2 Attention � d�finir
      xmo_TDU(1,2) = 5.46D-14
      xmo_TDU(2,2) = 15636.D0
      xmo_TDU(3,2) = 0.D0
      xmo_TDU(4,2) = 0.D0
      xmo_TDU(5,2) = 1.2D-39
*
* Coefficient xmo_DUJ empirique apport de lacune
* aux cavit�s par le joint : Nul car pas de lacunes aux joint
* La concentration � l'�quilibre thermique des lacunes est proche de  0
* pour UO2
      xmo_TDUJ(1,1) = 0.D0
      xmo_TDUJ(2,1) = 0.D0
      xmo_TDUJ(3,1) = 0.D0
      xmo_TDUJ(4,1) = 0.D0
      xmo_TDUJ(5,1) = 0.D0
*
* pour UPUO2
      xmo_TDUJ(1,2) = 0.D0
      xmo_TDUJ(2,2) = 0.D0
      xmo_TDUJ(3,2) = 0.D0
      xmo_TDUJ(4,2) = 0.D0
      xmo_TDUJ(5,2) = 0.D0
*
* Pour d�finition de xmo_DXEJ : coefficient de diffusion de Xe
* au joint
      xmo_RAP_DXE_TH = 100.D0
      xmo_RAP_DXE_MIX = 0.D0
      xmo_RAP_DXE_ATH = 0.D0
*
* Param�tres intervenant dans la mobilit� des cavit�s
      xmo_QVOL = 1.5D-27
      xmo_QS = 6.9D-19
      xmo_QV(1) = 7.29D-19
      xmo_QV(2) = 2.16D-19
      xmo_FMIGR = 1.D0
      xmo_FMIGRPOR = 1.D0
      xmo_FMIGRJ = 1.D0
      mo_VBJ_OUI = 0
*
* Arriv�e des bulles intra en inter
      xmo_TDBJ(1) = 2.D0
      xmo_TDBJ(2) = 1.D-9
      xmo_TDBJ(3) = 3.D0
      xmo_TDBJ(4) = 2.D-9
*
* Evolution du volume des cavit�s
      xmo_ZETA = 1.D0
      xmo_COEF1_GAMMA = 0.41D0 * 0.85D0
      xmo_COEF2_GAMMA = - 0.41D0 * 1.4D-4
      xmo_VATXES = 2.17D-28
      xmo_XLSUP = 2.D0
      xmo_XLINF = 1.2D0
*
      xmo_AXEJ = 1.D0
      xmo_ABJ = 1.D0
      xmo_TETBJ = 0.872665D0
*
* percolation
      xmo_COELIM_PERC = 1.D0
      xmo_KTUNJ = 1.D-19
      xmo_KTUNJPORE = 1.D-18
      mo_COUV_VAR = 0
      xmo_FCOUVLIM = 0.55D0
      xmo_DFCOUV = 0.05D0
      xmo_AGG_REFERENCE = 5.D-6
      xmo_AGG_EXP = 1.D0
*
* Modele de RIM     
      xmo_APG = 75.D-9
      xmo_PROBU = 6.65D-13
      xmo_TOREC = 36.D4
      xmo_DTKREC = 5.D0
      xmo_TKREC = 1043.D0
      xmo_RHODL = 2.31D15
      xmo_RHODI = 6.31D13
      xmo_CBRIM0 = 6.3D17
      xmo_DIST_ECHAP_RIM = 0.583D-6
      mo_MODASERIM = 2
      xmo_ASERF = 50.D-6
      xmo_COE_LAC_RIM = 100.
      xmo_COE_VOL_ALIM_RIM = 0.
      xmo_MARGRHO_APPR = xmo_RHODL / 4.D0
      xmo_MARGRHO_ETAT23 = xmo_RHODL / 40.D0
      xmo_MARGRHO_FR = xmo_RHODL * 1.D-4
      xmo_RHOLIM = 3.63D15
      mo_MODELE_FRAC = 1
*
      xmo_XGONF = 0.095D0
      xmo_REPMA = 300.D-9
      xmo_FRLIM_CALCUL_GONFMOY = 0.1
*
*
************************************************************************
* PARAMETRES NON UTILISES PAR DEFAUT
************************************************************************
* controle de ddestr
      xmo_RBDEST(1) = 1.5D-9
      xmo_RBDEST(2) = 2.5D-9
      xmo_RBDEST(3) = 3.5D-9
      IF (mo_IDEST .EQ. 3) THEN
        xmo_LARG1 = xmo_RBDEST(2) - xmo_RBDEST(1)
        xmo_LARG2 = xmo_RBDEST(3) - xmo_RBDEST(2)
*
        xmo_A1 = (xmo_PDEST * xmo_LARG1 + 1.D0) / xmo_LARG2**3.D0
        xmo_B1 = - (2.D0 * xmo_PDEST * xmo_LARG1 + 3.D0) / 2.D0
     &              / xmo_LARG1**2.D0
*
        xmo_A2 = (xmo_PDEST * xmo_LARG2 + 1.D0) / xmo_LARG2**3.D0
        xmo_B2 = (2.D0 * xmo_PDEST * xmo_LARG2 + 3.D0) / 2.D0
     &              / xmo_LARG2**2.D0
      ENDIF
*
      xmo_AVJ = 1.D0
      xmo_LDIS = 0.5D-9
      xmo_EPJ = 1.D-9
************************************************************************
* NUMERIQUE MAILLAGE
************************************************************************
* Valeurs par d�faut des param�tres num�riques
* Volumes finis
      mo_DEGRE = 0
* 11 couronnes dans le grain
      mo_NGG = 11
* rayons relatifs d�limitant les couronnes
      xmo_RGG(1) = 0.2D0 
      xmo_RGG(2) = 0.3D0
      xmo_RGG(3) = 0.4D0
      xmo_RGG(4) = 0.5D0
      xmo_RGG(5) = 0.6D0
      xmo_RGG(6) = 0.7D0
      xmo_RGG(7) = 0.8D0
      xmo_RGG(8) = 0.85D0
      xmo_RGG(9) = 0.9D0
      xmo_RGG(10) = 0.95D0
      xmo_RGG(11) = 1.D0
*
* 2 couronnes dans la sphere equivalente
      mo_NSEQ = 2
* rayons relatifs d�limitant les couronnes
      xmo_RSEQ(1) =  0.5D0
      xmo_RSEQ(2) =  1.D0
*
* 8 couronnes dans la sphere equivalente du combustible restructure
      mo_NSEQR = 8
* rayons relatifs d�limitant les couronnes
      xmo_RSEQR(1) = 0.25D0
      xmo_RSEQR(2) = 0.5D0
      xmo_RSEQR(3) = 0.7D0
      xmo_RSEQR(4) = 0.9D0
      xmo_RSEQR(5) = 0.95D0
      xmo_RSEQR(6) = 0.99D0
      xmo_RSEQR(7) = 0.995D0
      xmo_RSEQR(8) = 1.D0
*
* mo_SCHEMA = 6 correspond � la methode de GEAR
      mo_SCHEMA = 6
      mo_ITOL = 4
      mo_ITASK = 1 
c jacobienne analytique
      mo_JT = 1
c jacobienne numerique
c      mo_JT = 2
      mo_IOPT = 1
*
* Gestion du pas de temps
       mo_NTEMPS_LU = 1
       xmo_PAS_TEMPS_LIM = 0.26D6
*
************************************************************************
* Valeurs par d�faut des param�tres de tests
*
      xmo_TEST0 = 1.D0
      xmo_TEST2 = 1.D0
      xmo_TEST3 = 1.D0
      xmo_TEST4 = 1.D0
      xmo_TEST5 = 1.D0
      xmo_TEST6 = 1.D0
      xmo_SOURXEIMP = 0.D0
      xmo_TEST7 = 1.D0
      xmo_TEST8 = 1.D0
      xmo_TEST9 = 1.D0
      xmo_TEST10 = 1.D0
      xmo_EQUAG = 1.D0
      xmo_EQUAB = 1.D0
      xmo_EQUAP = 1.D0
      xmo_EQUAGJ = 1.D0
      xmo_EQUABJ = 1.D0
      xmo_EQUAPJ = 1.D0
      xmo_EQUARHOD = 1.D0
*
************************************************************************
* AFFICHAGES
************************************************************************
* Valeurs par d�faut des param�tres d'impression
*
      xmo_TPSAFF(1) = 0.D0
      xmo_TPSAFF(2) = 1.D20
      mo_FREQIMPR = 1
*
      mo_NBPTANA = 0
      DO L = 1, mo_M31MAX
        DO IGROB = 1,mo_IGRMAX
          DO I = 1, mo_IFEMAX
            mo_NORDR_ANA(L,IGROB,I) = 0
            DO IFICH = 1, mo_IFICHMX
              mo_IMPR_ANA(L,IGROB,I,IFICH) = 0
            ENDDO
          ENDDO
        ENDDO
      ENDDO
*
************************************************************************
* REPRISES
************************************************************************
      mo_NREPRISELU = 0
      xmo_TPSINI = 0.D0
      mo_DEPART_SUR_REPRISE = 0
      mo_NREPRISE = 0
      DO II = 1, mo_NREPRISEMX
        xmo_TPSREP(II) = 0.D0
      ENDDO
************************************************************************
************************************************************************
*
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MOVALI
*     =================
*
*     ******************************************************************
*     MOVALI : MOgador VALeurs Inchang�es
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogsor.inc' 
      INCLUDE 'cmoginf.inc'        
************************************************************************ 
* cmogvar.inc : xmo_VECG xmo_VECD en entr�es et en sorties
* cmogsor.inc : xmo_SOMTOFIS 
*               xmo_TAUXBETAC xmo_TAUXSORBUL xmo_TAUXSORPOR 
*               xmo_TAUXREMISOL xmo_TAUXSOURXE xmo_TAUXOUTSE
*               xmo_TAUXOUTPERC xmo_TAUXOUTEJEC 
*               en entr�e et sortie
* cmoginf.inc : mo_DEBUG en entr�e
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
      CALL MOMESS('DANS MOVALI')
*

************************************************************************
************************************************************************
************************************************************************
************************************************************************
* Initialisation des variables pr�c�dentes (indic�es 2)
*
*
      DO 30 IVGA = 1, mo_NVGAMX
        xmo_VG(IVGA,1) = xmo_VG(IVGA,2)
 30   CONTINUE
*
      DO 40 IVDE = 1, mo_NVDEMX
        xmo_VD(IVDE,1) =  xmo_VD(IVDE,2)
 40   CONTINUE
*
      xmo_SOMTOFIS(1) = xmo_SOMTOFIS(2) 
      xmo_TAUXBETAC(1) =xmo_TAUXBETAC(2)
      xmo_TAUXSORBUL(1) = xmo_TAUXSORBUL(2)
      xmo_TAUXSORPOR(1) = xmo_TAUXSORPOR(2)
      xmo_TAUXREMISOL(1) = xmo_TAUXREMISOL(2)
      xmo_TAUXSOURXE(1) = xmo_TAUXSOURXE(2)
      xmo_TAUXOUTSE(1) = xmo_TAUXOUTSE(2)
      xmo_TAUXOUTPERC(1) = xmo_TAUXOUTPERC(2)
      xmo_TAUXOUTEJEC(1) = xmo_TAUXOUTEJEC(2)
*
*
*
      RETURN
      END
*
*
      SUBROUTINE MOVALP
*     =================
*
*     ******************************************************************
*     MOREPP : MOgador VALeurs Pr�c�dentes
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogsor.inc'        
************************************************************************ 
* cmogvar.inc : xmo_VECG xmo_VECD en entr�es et en sorties
* cmogsor.inc : xmo_SOMTOFIS3 xmo_GCREA3 xmo_REMISOL3 xmo_SORDIF3
*               xmo_SORBUL3 xmo_SORPOR3 xmo_SORDIFJ3  xmo_SORPERC3
*               xmo_SOREJEC3
*               xmo_TAUXBETAC3 xmo_TAUXSORBUL3 xmo_TAUXSORPOR3 
*               xmo_TAUXREMISOL3 xmo_TAUXSOURXE3 xmo_TAUXOUTSE3
*               xmo_TAUXOUTPERC3 xmo_TAUXOUTEJEC3
*               en entr�e et sortie
************************************************************************
*     7--0---------0---------0---------0---------0---------0---------0--  
*     ==================================================================
*
      CALL MOMESS('DANS MOVALP')
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* Initialisation des variables pr�c�dentes (indic�es 2)
*
      DO 10 L = 1,mo_M31MAX
        DO 10 IGROB = 1,mo_IGRMAX
          DO 10 I = 1,mo_IFEMAX
            DO 10 IHETER = 1, mo_NAMAMX
*
              DO IVGA = 1,mo_NVGAMX
                xmo_VECG(L,IGROB,I,IHETER,IVGA,2) = 
     &          xmo_VECG(L,IGROB,I,IHETER,IVGA,1)
              ENDDO
* 
              DO IVDE = 1,mo_NVDEMX
                xmo_VECD(L,IGROB,I,IHETER,IVDE,2) = 
     &          xmo_VECD(L,IGROB,I,IHETER,IVDE,1)
              ENDDO
*
              mo_IETCOM(L,IGROB,I,IHETER,2) = 
     &        mo_IETCOM(L,IGROB,I,IHETER,1)  
*
              mo_IETCOM_APPR(L,IGROB,I,IHETER,2) = 
     &        mo_IETCOM_APPR(L,IGROB,I,IHETER,1)
*
              xmo_GONFS3(L,IGROB,I,IHETER,2) =
     &        xmo_GONFS3(L,IGROB,I,IHETER,1)
*
              xmo_GCREA3(L,IGROB,I,IHETER,2) =
     &        xmo_GCREA3(L,IGROB,I,IHETER,1)
*
              xmo_REMISOL3(L,IGROB,I,IHETER,2) = 
     &        xmo_REMISOL3(L,IGROB,I,IHETER,1)
*
              xmo_SORDIF3(L,IGROB,I,IHETER,2) = 
     &        xmo_SORDIF3(L,IGROB,I,IHETER,1)
*
              xmo_SORBUL3(L,IGROB,I,IHETER,2) = 
     &        xmo_SORBUL3(L,IGROB,I,IHETER,1)
*
              xmo_SORPOR3(L,IGROB,I,IHETER,2) = 
     &        xmo_SORPOR3(L,IGROB,I,IHETER,1)
*
              xmo_SORDIFJ3(L,IGROB,I,IHETER,2) =
     &        xmo_SORDIFJ3(L,IGROB,I,IHETER,1)
*
              xmo_SORPERC3(L,IGROB,I,IHETER,2) =
     &        xmo_SORPERC3(L,IGROB,I,IHETER,1)
*
              xmo_SOREJEC3(L,IGROB,I,IHETER,2) =
     &        xmo_SOREJEC3(L,IGROB,I,IHETER,1)
*
              xmo_SOMTOFIS3(L,IGROB,I,IHETER,2) =
     &        xmo_SOMTOFIS3(L,IGROB,I,IHETER,1)
*
              xmo_TAUXBETAC3(L,IGROB,I,IHETER,2) = 
     &        xmo_TAUXBETAC3(L,IGROB,I,IHETER,1)
* 
              xmo_TAUXSORBUL3(L,IGROB,I,IHETER,2) = 
     &        xmo_TAUXSORBUL3(L,IGROB,I,IHETER,1) 
*
              xmo_TAUXSORPOR3(L,IGROB,I,IHETER,2) = 
     &        xmo_TAUXSORPOR3(L,IGROB,I,IHETER,1) 
*
              xmo_TAUXREMISOL3(L,IGROB,I,IHETER,2) = 
     &        xmo_TAUXREMISOL3(L,IGROB,I,IHETER,1) 
*
              xmo_TAUXSOURXE3(L,IGROB,I,IHETER,2) = 
     &        xmo_TAUXSOURXE3(L,IGROB,I,IHETER,1)
* 
              xmo_TAUXOUTSE3(L,IGROB,I,IHETER,2) =
     &        xmo_TAUXOUTSE3(L,IGROB,I,IHETER,1)
* 
              xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,2) =
     &        xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,1) 
*
              xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,2) =
     &        xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,1) 
*
 10   CONTINUE
*
      RETURN
      END
*
      SUBROUTINE MOVLAT (xmo_CGAZ, xmo_VOL,
     $                    mo_BBJT,xmo_VAT)
C     ==================
C
C     ******************************************************************
C     MOVLAT : MOgador calcul du VoLume ATomique dans :
C              - une bulle intra : mo_BBJT = 1
C              - une bulle inter :   mo_BBJT = 2
C              - un tunnel :   mo_BBJT = 3
C              - un pore intra : mo_BBJT = 4
C              - un pore inter : mo_BBJT = 5
C              - une bulle de rim : mo_BBJT = 6
C              - un pore de rim : mo_BBJT = 7
C              - ensemble pores inter et bulles inter : mo_BBJT = 8
C              xmo_CGAZ : concentration en gaz de bulle ou de tunnel (mol/m3) 
C              xmo_VOL : volume des bulle ou des pores /m3 ou des tunnel /m3                       
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogdon.inc'
C
***********************************************************************
* cmogdon.inc : xmo_ATPOR xmo_ATPORJ en entr�e
* cmogphy.inc : xmo_XNAVOG en entr�e
************************************************************************
C
C xmo_NBAT: nombre d atomes de gaz de fission 
C              dans l'ensemble des bulles ou  des pores /m3
C xmo_VAT : Volume disponible par atome de gaz (de fission ou initial) 
C           dans la bulle ou les tunnels ou les pores
C
C
************************************************************************
************************************************************************
C
      CALL MOMESS('DANS MOVLAT')
C
C
C CALCUL DE xmo_VAT selon le type de bulle, tunnel pores consid�r�
C
      EPS = 1.D-20
      XINF = 1.D+20
C
      IF (mo_BBJT .EQ. 1 .OR. mo_BBJT .EQ. 2 
     &                   .OR. mo_BBJT .EQ. 3
     &                   .OR. mo_BBJT .EQ. 6) THEN
C Cas des bulles rondes ou 
C des bulles lenticulaires ou 
C des tunnels
        xmo_NBAT = xmo_CGAZ * xmo_XNAVOG 
C
      ELSEIF (mo_BBJT .EQ. 4) THEN
C Cas des pores intra
        xmo_NBAT = xmo_CGAZ * xmo_XNAVOG  + xmo_ATPOR
C
      ELSEIF (mo_BBJT .EQ. 5 .OR. mo_BBJT .EQ. 7) THEN
C Cas des pores inter ou des pores de rim
        xmo_NBAT = xmo_CGAZ * xmo_XNAVOG  + xmo_ATPORJ
C
      ELSEIF (mo_BBJT .EQ. 8) THEN
C Cas de l'ensemble pores inter + bulles inter 
C Ca sert au moment de l'interconnexion
        xmo_NBAT = xmo_CGAZ * xmo_XNAVOG  + xmo_ATPORJ
      ENDIF
C
C
CDEBUG      IF ( xmo_NBAT .LT. EPS .AND. xmo_VOL .LT. EPS) THEN
CDEBUG          xmo_VAT = XINF
CDEBUG      ELSEIF ( xmo_NBAT .LT. EPS) THEN
CDEBUG          xmo_VAT = XINF
CDEBUG      ELSE
CDEBUG          xmo_VAT = xmo_VOL / xmo_NBAT
CDEBUG      ENDIF
C
      IF ( xmo_NBAT .GT. 0.D0 ) THEN
          xmo_VAT = xmo_VOL / xmo_NBAT
      ELSE
          xmo_VAT = XINF
      ENDIF

************************************************************************
C
C
C
      RETURN
      END
      SUBROUTINE MOWENT
*     ==================
*
*     ------------------------------------------------------------------
*
*     mo_NNRES1 = unit� du fichier d'impression des variables gaz intra ou gaz rim
*     mo_NNRES2 = unit� du fichier d'impression des variables gaz inter
*     mo_NNRES3 = unit� du fichier d'impression des variables pores
*     mo_NNRES4 = unit� du fichier d'impression des variables d�taill�es
*     mo_NMAC = unit� du fichier d'impression des variables macroscopiques
*     mo_NINTEG = unit� du fichier d'impression des variables integrales
*     ==================================================================
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogmai.inc'
************************************************************************
* cmoginf.inc : mo_NNRES1 mo_NNRES2 mo_NNRES3 mo_NNRES4 mo_NNSYNT mo_NMAC  
*               mo_NINTEG mo_NBPTANA en entr�e
* cmogpnu.inc : mo_NGG mo_NSEQ en entr�e
* cmogmai.inc : mo_NB_TRAX
************************************************************************
*
*
*
*     ==================================================================
*
*
      WRITE(mo_NMAC,6000)
*
      WRITE(mo_NINTEG,7000) (L,L,L,L, L = 1,mo_NB_TRAX)   
*
      IF (mo_NBPTANA .GT. 0) THEN
        WRITE(mo_NNRES1(1),1000)
        WRITE(mo_NNRES2(1),2000) (IVGA,IVGA=1,mo_NSEQ)
        WRITE(mo_NNRES3(1),4000) (IVGA,IVGA=1,mo_NSEQ)
        WRITE(mo_NNRES4(1),5000)
        WRITE(mo_NNSYNT(1),3000)
        WRITE(mo_NNRES5(1),8000)
      ENDIF
*
      IF (mo_NBPTANA .GT. 1) THEN
        WRITE(mo_NNRES1(2),1000)
        WRITE(mo_NNRES2(2),2000) (IVGA,IVGA=1,mo_NSEQ)
        WRITE(mo_NNRES3(2),4000) (IVGA,IVGA=1,mo_NSEQ)
        WRITE(mo_NNRES4(2),5000)
        WRITE(mo_NNSYNT(2),3000)
        WRITE(mo_NNRES5(2),8000)
      ENDIF
*
*
*
* fichier _RRR1
 1000 FORMAT (1X,
     &                  11X,'xmo_TEMPS',
     &                     14X,'mo_IET',
     &                 9X,'mo_IET_APPR',
     &                   12X,'xmo_CXEM',
     &                     14X,'xmo_CB',
     &                   12X,'xmo_CXEB',
     &                    13X,'xmo_VTB',
     &                     14X,'xmo_RB', 
     &                   12X,'xmo_CPOR',
     &                 10X,'xmo_CXEPOR',
     &                  11X,'xmo_VTPOR',
     &                   12X,'xmo_RPOR',
     &                   12X,'xmo_RHOD',         
     &           3X,'xmo_XE_CREATION_M',
     &          2X,'xmo_XE_DIFFUSION_M',
     &               7X,'xmo_XE_CLIM_M', 
     &           3X,'xmo_XE_NUCLHOMO_M',
     &            4X,'xmo_XE_PIEGBUL_M',
     &            4X,'xmo_XE_PIEGPOR_M',
     &           3X,'xmo_XE_REMJOINT_M',
     &             5X,'xmo_XE_REMBUL_M',
     &             5X,'xmo_XE_REMPOR_M',
     &              6X,'xmo_B_NUCLHOMO',
     &               7X,'xmo_B_COALEDB',
     &               7X,'xmo_B_COALEVB',
     &             5X,'xmo_B_COALEGEOM',
     &              6X,'xmo_B_PIEGBPOR',
     &                8X,'xmo_B_REMBUL',
     &           3X,'xmo_B_SORTIEGRAIN',
     &            4X,'xmo_XEB_NUCLHOMO',
     &             5X,'xmo_XEB_PIEGBUL',
     &            4X,'xmo_XEB_PIEGBPOR',
     &              6X,'xmo_XEB_REMBUL',
     &         1X,'xmo_XEB_SORTIEGRAIN',
     &              6X,'xmo_VTB_LACUNE',
     &                 9X,'xmo_VTB_GAZ',
     &            4X,'xmo_VTB_PIEGBPOR',
     &              6X,'xmo_VTB_REMBUL',
     &         1X,'xmo_VTB_SORTIEGRAIN',
* a partir d ici 22 caracteres
     &           5X,'xmo_POR_COALEDPOR',
     &           5X,'xmo_POR_COALEVPOR',
     &              8X,'xmo_POR_REMPOR',
     &         3X,'xmo_POR_SORTIEGRAIN',
     &           5X,'xmo_XEPOR_PIEGPOR',
     &          4X,'xmo_XEPOR_PIEGBPOR',
     &            6X,'xmo_XEPOR_REMPOR',
     &       1X,'xmo_XEPOR_SORTIEGRAIN',
     &            6X,'xmo_VTPOR_LACUNE',
     &               9X,'xmo_VTPOR_GAZ',
     &          4X,'xmo_VTPOR_PIEGBPOR',
     &            6X,'xmo_VTPOR_REMPOR',
     &       1X,'xmo_VTPOR_SORTIEGRAIN',
     &                 12X,'xmo_SORDIF',
     &                 12X,'xmo_SORBUL',
     &                 12X,'xmo_SORPOR',
     &                11X,'xmo_SOREJEC',
     &                11X,'xmo_REMISOL',
     &                  13X,'xmo_GCREE',
     &               10X,'xmo_ERRINTRA'  )
*
*
* fichier _RRR2
 2000 FORMAT (1X,
     &                  11X,'xmo_TEMPS', 
     &                     14X,'mo_IET',
     &                 9X,'mo_IET_APPR',
     &                  11X,'xmo_CXEJM',
     &                    13X,'xmo_CBJ',
     &                  11X,'xmo_CXEBJ',
     &                   12X,'xmo_VTBJ', 
     &                    13X,'xmo_RBJ',
     &                  11X,'xmo_CPORJ',
     &                 9X,'xmo_CXEPORJ',
     &                 10X,'xmo_VTPORJ', 
     &                  11X,'xmo_RPORJ', 
     &          2X,'xmo_XEJ_NUCLHOMO_M',
     &           3X,'xmo_XEJ_PIEGBUL_M',
     &           3X,'xmo_XEJ_PIEGPOR_M',
     &            4X,'xmo_XEJ_SOURCE_M',
     &          2X,'xmo_XEJ_REMJOINT_M',
     &         1X,'xmo_XEJ_DIFFUSION_M',
     &             5X,'xmo_XEJ_CLIM3_M',
     &             5X,'xmo_BJ_NUCLHOMO',
     &               7X,'xmo_BJ_APPORT',
     &             5X,'xmo_BJ_COALEDBJ',
     &             5X,'xmo_BJ_COALEVBJ',
     &           3X,'xmo_BJ_COALEGEOMJ',
     &             5X,'xmo_BJ_PIEGBPOR',
     &               7X,'xmo_BJ_REMBUL',
     &           3X,'xmo_XEBJ_NUCLHOMO',
     &             5X,'xmo_XEBJ_APPORT',
     &            4X,'xmo_XEBJ_PIEGBUL',
     &           3X,'xmo_XEBJ_PIEGBPOR',
     &             5X,'xmo_XEBJ_REMBUL',
     &           3X,'xmo_XEBJ_PERCBPOR',
     &           3X,'xmo_XEBJ_PERCPLEN',
     &             5X,'xmo_VTBJ_LACUNE',
     &                8X,'xmo_VTBJ_GAZ',
     &           3X,'xmo_VTBJ_PIEGBPOR',
     &             5X,'xmo_VTBJ_REMBUL',
     &             5X,'xmo_VTBJ_APPORT',
     &          2X,'xmo_XEPORJ_PIEGPOR',
     &         1X,'xmo_XEPORJ_PIEGBPOR',
     &           3X,'xmo_XEPORJ_REMPOR',
     &           3X,'xmo_XEPORJ_APPORT',
     &         1X,'xmo_XEPORJ_PERCBPOR',
     &           3X,'xmo_VTPORJ_LACUNE',
     &              6X,'xmo_VTPORJ_GAZ',
     &         1X,'xmo_VTPORJ_PIEGBPOR',
     &           3X,'xmo_VTPORJ_APPORT',
     &                 9X,'xmo_SORDIFJ',
     &                 9X,'xmo_SORPERC',
     &                 9X,'xmo_REMISOL',
     &                 10X,'xmo_SORDIF',
     &                 10X,'xmo_SORBUL',
     &                 10X,'xmo_SORPOR',
     &                8X,'xmo_ERRINTER',
     &         9(9X,'xmo_CXEJ(',I1,')'),
     &         9(8X,'xmo_CXEJ(',I2,')')  )
*
* fichier _SYNT
 3000 FORMAT (1X,
     &                  17X,'xmo_TEMPS', 
     &                     20X,'mo_IET', 
     &                15X,'mo_IET_APPR',
     &                   18X,'xmo_CXEM',
     &                     20X,'xmo_CB', 
     &                   18X,'xmo_CXEB', 
     &                    19X,'xmo_VTB', 
     &                     20X,'xmo_RB',
     &                 16X,'xmo_PINT_B',
     &                  17X,'xmo_PEQ_B',
     &                   18X,'xmo_CPOR',
     &                 16X,'xmo_CXEPOR',
     &                  17X,'xmo_VTPOR',
     &                   18X,'xmo_RPOR',
     &               14X,'xmo_PINT_POR',
     &                15X,'xmo_PEQ_POR',
     &                   18X,'xmo_RHOD',
     &                  17X,'xmo_CXEJM',
     &                    19X,'xmo_CBJ',
     &                  17X,'xmo_CXEBJ',
     &                   18X,'xmo_VTBJ',
     &                    19X,'xmo_RBJ',
     &                15X,'xmo_PINT_BJ',
     &                 16X,'xmo_PEQ_BJ',
     &                  17X,'xmo_CPORJ',
     &                15X,'xmo_CXEPORJ',
     &                 16X,'xmo_VTPORJ',
     &                  17X,'xmo_RPORJ',
     &              13X,'xmo_PINT_PORJ',
     &               14X,'xmo_PEQ_PORJ',
     &                15X,'xmo_CXEHOMM',
     &                  17X,'xmo_CBRIM',
     &                15X,'xmo_CXEBRIM',
     &                 16X,'xmo_VTBRIM',
     &                  17X,'xmo_RBRIM',
     &              13X,'xmo_PINT_BRIM',
     &               14X,'xmo_PEQ_BRIM',
     &                15X,'xmo_CPORRIM',
     &              13X,'xmo_CXEPORRIM',
     &               14X,'xmo_VTPORRIM',
     &                15X,'xmo_RPORRIM',
     &            11X,'xmo_PINT_PORRIM',
     &             12X,'xmo_PEQ_PORRIM',
     &                  17X,'xmo_GCREE',
     &                15X,'xmo_SORDIFJ',
     &                15X,'xmo_SORPERC',
     &                15X,'xmo_SOREJEC',
     &                15X,'xmo_ERRGLOB' )
*
 4000 FORMAT (1X,
     &                  11X,'xmo_TEMPS', 
     &                     14X,'mo_IET',
     &                 9X,'mo_IET_APPR',
     &                  11X,'xmo_CXEJM',
     &                    13X,'xmo_CBJ',
     &                  11X,'xmo_CXEBJ',
     &                   12X,'xmo_VTBJ', 
     &                    13X,'xmo_RBJ',
     &                  11X,'xmo_CPORJ',
     &                 9X,'xmo_CXEPORJ',
     &                 10X,'xmo_VTPORJ', 
     &                  11X,'xmo_RPORJ', 
     &          2X,'xmo_XEJ_NUCLHOMO_M',
     &           3X,'xmo_XEJ_PIEGBUL_M',
     &           3X,'xmo_XEJ_PIEGPOR_M',
     &            4X,'xmo_XEJ_SOURCE_M',
     &          2X,'xmo_XEJ_REMJOINT_M',
     &         1X,'xmo_XEJ_DIFFUSION_M',
     &             5X,'xmo_XEJ_CLIM3_M',
     &             5X,'xmo_BJ_NUCLHOMO',
     &               7X,'xmo_BJ_APPORT',
     &             5X,'xmo_BJ_COALEDBJ',
     &             5X,'xmo_BJ_COALEVBJ',
     &           3X,'xmo_BJ_COALEGEOMJ',
     &             5X,'xmo_BJ_PIEGBPOR',
     &               7X,'xmo_BJ_REMBUL',
     &           3X,'xmo_XEBJ_NUCLHOMO',
     &             5X,'xmo_XEBJ_APPORT',
     &            4X,'xmo_XEBJ_PIEGBUL',
     &             5X,'xmo_XEBJ_REMBUL',
     &          2X,'xmo_XEPORJ_PIEGPOR',
     &           3X,'xmo_XEPORJ_REMPOR',
     &           3X,'xmo_XEPORJ_APPORT',
     &         1X,'xmo_XETUNJ_PERCPLEN',
     &             5X,'xmo_VTBJ_LACUNE',
     &                8X,'xmo_VTBJ_GAZ',
     &           3X,'xmo_VTBJ_PIEGBPOR',
     &             5X,'xmo_VTBJ_REMBUL',
     &             5X,'xmo_VTBJ_APPORT',
     &           3X,'xmo_VTPORJ_LACUNE',
     &              6X,'xmo_VTPORJ_GAZ',
     &         1X,'xmo_VTPORJ_PIEGBPOR',
     &           3X,'xmo_VTPORJ_APPORT',
     &                 9X,'xmo_SORDIFJ',
     &                 9X,'xmo_REMISOL',
     &                 10X,'xmo_SORDIF',
     &                 10X,'xmo_SORBUL',
     &                 10X,'xmo_SORPOR',
     &                8X,'xmo_ERRINTER',
     &                 10X,'xmo_PINTBJ',
     &         9(9X,'xmo_CXEJ(',I1,')'),
     &         9(8X,'xmo_CXEJ(',I2,')')  )
*
* fichier _RRR4
 5000 FORMAT (1X,
     &                            21X,'xmo_TEMPS', 
     &                               24X,'mo_IET', 
     &                          19X,'mo_IET_APPR',
     &                             22X,'xmo_CXEM',
     &                               24X,'xmo_CB',
     &                             22X,'xmo_CXEB',
     &                              23X,'xmo_VTB',
     &                               24X,'xmo_RB',
     &                             22X,'xmo_CPOR',
     &                           20X,'xmo_CXEPOR',
     &                            21X,'xmo_VTPOR',
     &                             22X,'xmo_RPOR',
     &                            21X,'xmo_CXEJM',
     &                              23X,'xmo_CBJ',
     &                            21X,'xmo_CXEBJ',
     &                             22X,'xmo_VTBJ',
     &                              23X,'xmo_RBJ',
     &                            21X,'xmo_CPORJ',
     &                          19X,'xmo_CXEPORJ',
     &                           20X,'xmo_VTPORJ',
     &                            21X,'xmo_RPORJ',
     &                             22X,'xmo_RHOD',
     &                          19X,'xmo_CXEHOMM',
     &                            21X,'xmo_CBRIM',
     &                          19X,'xmo_CXEBRIM',
     &                           20X,'xmo_VTBRIM',
     &                           21X, 'xmo_RBRIM',
     &                          19X,'xmo_CPORRIM',
     &                        17X,'xmo_CXEPORRIM',
     &                         18X,'xmo_VTPORRIM',
     &                          19X,'xmo_RPORRIM',
     &                    13X,'xmo_XE_NUCLHOMO_M',
     &                     14X,'xmo_XE_PIEGBUL_M',
     &                     14X,'xmo_XE_PIEGPOR_M',
     &                    13X,'xmo_XE_CREATION_M',
     &                    13X,'xmo_XE_REMJOINT_M',
     &                      15X,'xmo_XE_REMBUL_M',
     &                      15X,'xmo_XE_REMPOR_M',
     &                   12X,'xmo_XE_DIFFUSION_M',
     &                        17X,'xmo_XE_CLIM_M',
     &                 10X,'xmo_XE_SORTIEGRAIN_M',
     &                       16X,'xmo_B_NUCLHOMO',
     &                        17X,'xmo_B_COALEDB',
     &                        17X,'xmo_B_COALEVB',
     &                      15X,'xmo_B_COALEGEOM',
     &                       16X,'xmo_B_PIEGBPOR',
     &                         18X,'xmo_B_REMBUL',
     &                    13X,'xmo_B_SORTIEGRAIN',
     &                     14X,'xmo_XEB_NUCLHOMO',
     &                      15X,'xmo_XEB_PIEGBUL',
     &                     14X,'xmo_XEB_PIEGBPOR',
     &                       16X,'xmo_XEB_REMBUL',
     &                  11X,'xmo_XEB_SORTIEGRAIN',
     &                       16X,'xmo_VTB_LACUNE',
     &                          19X,'xmo_VTB_GAZ',
     &                     14X,'xmo_VTB_PIEGBPOR',
     &                       16X,'xmo_VTB_REMBUL',
     &                  11X,'xmo_VTB_SORTIEGRAIN',
     &                    13X,'xmo_POR_COALEDPOR',
     &                    13X,'xmo_POR_COALEVPOR',
     &                       16X,'xmo_POR_REMPOR',
     &                  11X,'xmo_POR_SORTIEGRAIN',
     &                    13X,'xmo_XEPOR_PIEGPOR',
     &                   12X,'xmo_XEPOR_PIEGBPOR',
     &                     14X,'xmo_XEPOR_REMPOR',
     &                 9X,'xmo_XEPOR_SORTIEGRAIN',
     &                     14X,'xmo_VTPOR_LACUNE',
     &                        17X,'xmo_VTPOR_GAZ',
     &                   12X,'xmo_VTPOR_PIEGBPOR',
     &                     14X,'xmo_VTPOR_REMPOR',
     &                 9X,'xmo_VTPOR_SORTIEGRAIN',
*
     &           '            xmo_XEJ_NUCLHOMO_M',
     &           '             xmo_XEJ_PIEGBUL_M',
     &           '             xmo_XEJ_PIEGPOR_M',
     &           '              xmo_XEJ_SOURCE_M',
     &           '            xmo_XEJ_REMJOINT_M',
     &           '           xmo_XEJ_DIFFUSION_M',
     &           '               xmo_XEJ_CLIM3_M',
     &           '             xmo_XEJ_SORTIEJ_M',
     &           '               xmo_BJ_NUCLHOMO',
     &           '                 xmo_BJ_APPORT',
     &           '               xmo_BJ_COALEDBJ',
     &           '               xmo_BJ_COALEVBJ',
     &           '             xmo_BJ_COALEGEOMJ',
     &           '               xmo_BJ_PIEGBPOR',
     &           '                 xmo_BJ_REMBUL',
     &           '                xmo_BJ_SORTIEJ',
     &           '             xmo_XEBJ_NUCLHOMO',
     &           '               xmo_XEBJ_APPORT',
     &           '              xmo_XEBJ_PIEGBUL',
     &           '             xmo_XEBJ_PIEGBPOR',
     &           '               xmo_XEBJ_REMBUL',
     &           '              xmo_XEBJ_SORTIEJ',
     &           '               xmo_VTBJ_LACUNE',
     &           '                  xmo_VTBJ_GAZ',
     &           '             xmo_VTBJ_PIEGBPOR',
     &           '               xmo_VTBJ_REMBUL',
     &           '               xmo_VTBJ_APPORT',
     &           '              xmo_VTBJ_SORTIEJ',
     &           '              xmo_PORJ_SORTIEJ',
     &           '            xmo_XEPORJ_PIEGPOR',
     &           '           xmo_XEPORJ_PIEGBPOR',
     &           '             xmo_XEPORJ_REMPOR',
     &           '             xmo_XEPORJ_APPORT',
     &           '            xmo_XEPORJ_SORTIEJ',
     &           '             xmo_VTPORJ_LACUNE',
     &           '                xmo_VTPORJ_GAZ',
     &           '           xmo_VTPORJ_PIEGBPOR',
     &           '             xmo_VTPORJ_APPORT',
     &           '            xmo_VTPORJ_SORTIEJ',
*
     &                   12X,'xmo_XEHOM_APPORT_M',
     &                 10X,'xmo_XEHOM_CREATION_M',
     &                 9X,'xmo_XEHOM_DIFFUSION_M',
     &                    13X,'xmo_XEHOM_CLIM3_M',
     &                      15X,'xmo_BRIM_APPORT',
     &                    13X,'xmo_XEBRIM_APPORT',
     &               7X,'xmo_XEBRIM_ALIM_DIRECTE',
     &                 9X,'xmo_XEBRIM_ALIM_ECHAP',
     &                   12X,'xmo_XEBRIM_REMBRIM',
     &                    13X,'xmo_VTBRIM_APPORT',
     &                    13X,'xmo_VTBRIM_LACUNE',
     &                       16X,'xmo_VTBRIM_GAZ',
     &                    13X,'xmo_PORRIM_APPORT',
     &                  11X,'xmo_XEPORRIM_APPORT',
     &             5X,'xmo_XEPORRIM_ALIM_DIRECTE',
     &               7X,'xmo_XEPORRIM_ALIM_ECHAP',
     &                8X,'xmo_XEPORRIM_REMPORRIM',
     &                  11X,'xmo_VTPORRIM_APPORT',
     &                  11X,'xmo_VTPORRIM_LACUNE',
     &                     14X,'xmo_VTPORRIM_GAZ'  )
*
* fichier _RMAC
 6000 FORMAT (1X,
     &                  17X,'xmo_TEMPS',
     &                          25X,'L', 
     &                      21X,'IGROB',
     &                          25X,'I',
     &                  17X,'xmo_RPAST',
     &                     20X,'mo_IET', 
     &                   18X,'xmo_CXEM',
     &                     20X,'xmo_CB', 
     &                   18X,'xmo_CXEB', 
     &                    19X,'xmo_VTB', 
     &                     20X,'xmo_RB',
     &                   18X,'xmo_CPOR',
     &                 16X,'xmo_CXEPOR',
     &                  17X,'xmo_VTPOR',
     &                   18X,'xmo_RPOR',
     &                   18X,'xmo_RHOD',
     &                  17X,'xmo_CXEJM',
     &                    19X,'xmo_CBJ',
     &                  17X,'xmo_CXEBJ',
     &                   18X,'xmo_VTBJ',
     &                    19X,'xmo_RBJ',
     &                  17X,'xmo_CPORJ',
     &                15X,'xmo_CXEPORJ',
     &                 16X,'xmo_VTPORJ',
     &                  17X,'xmo_RPORJ',
     &                15X,'xmo_CXEHOMM',
     &                  17X,'xmo_CBRIM',
     &                15X,'xmo_CXEBRIM',
     &                 16X,'xmo_VTBRIM',
     &                  17X,'xmo_RBRIM',
     &                15X,'xmo_CPORRIM',
     &              13X,'xmo_CXEPORRIM',
     &               14X,'xmo_VTPORRIM',
     &                15X,'xmo_RPORRIM',
     &    2X,'xmo_GCREE + DS PORES INI',
     &                15X,'xmo_SORDIFJ',
     &                15X,'xmo_SORPERC',
     &                15X,'xmo_SOREJEC',
     &                15X,'xmo_ERRGLOB',      
     &                 16X,'xmo_GCREA3', 
     &                15X,'xmo_GINTRA3',
     &                15X,'xmo_GINTER3',
     &                 16X,'xmo_GEPMA3', 
     &              13X,'xmo_SOMTOFIS3',
     &                 16X,'xmo_GONFS3',
     &                 16X,'xmo_SINPOR',
     &                 16X,'xmo_GASPOR',
     &                     20X,'xmo_FR',
     &                    19X,'xmo_FBJ',
     &                  17X,'xmo_FPORJ',  
     &             12X,'xmo_PART_INTRA',
     &             12X,'xmo_PART_INTER', 
     &             12X,'xmo_PART_RETEN'   )
*
*
 7000 FORMAT (1X,
     &                            17X,'xmo_TEMPS',
     &                          15X,'xmo_CREATOT',
     &                          15X,'xmo_RELATOT',
     &                       12X,'xmo_FRACRELACH',
     &                   7X,'xmo_FRACRELACH_PERC',
     &                   7X,'xmo_FRACRELACH_DIFJ',
     &                   7X,'xmo_FRACRELACH_EJEC',
     &                   9(10X,'xmo_GONFTOTTR('I1')', 
     &                       12X,'xmo_RLIMITE('I1')',
     &                 5X,'xmo_GONFTOTTR_BORD('I1')', 
     &                        13X,'xmo_CREATR('I1')',
     &                        13X,'xmo_RELATR('I1')'),
     &                  21(9X,'xmo_GONFTOTTR('I2')', 
     &                      11X,'xmo_RLIMITE('I2')',
     &                4X,'xmo_GONFTOTTR_BORD('I2')', 
     &                       12X,'xmo_CREATR('I2')',
     &                       12X,'xmo_RELATR('I2')')   )
*
 8000 FORMAT (1X,
     &                             8X,'xmo_TEMPS', 
     &                               11X,'mo_IET', 
     &                           6X,'mo_IET_APPR',
     &                              9X,'xmo_ERR1',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR4',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR5',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR6',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR7',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR8',
     &                               11X,'VALEUR',
     &                              9X,'xmo_ERR9',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR10',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR11',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR12',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR13',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR14',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR15',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR16',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR17',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR18',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR19',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERR20',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERRDE',
     &                               11X,'VALEUR',
     &                             8X,'xmo_ERRDR',
     &                               11X,'VALEUR',
     &                            7X,'xmo_ERRTOT',
     &                             8X,'xmo_BETAC' )
*
      RETURN
      END
      SUBROUTINE MOWMAC(L,IGROB,I,IHETER)
C     =================
C
C
C     ******************************************************************
C     MOWMAC : MOgador Write des Grandeurs MACroscopiques                           
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogphy.inc'
      INCLUDE 'cmogent.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
CDEBUG
C
***********************************************************************
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR en entr�e
* cmogmat.inc : xmo_TVGG xmo_TVSEQ xmo_TVSEQR en entr�e
* cmogvar.inc : mo_IETCOM xmo_VECG xmo_VECD en entr�e
*               xmo_ASERIM en sortie
* cmogsor.inc : xmo_SOMTOFIS3 
*               xmo_GONFS xmo_SINPOR xmo_GASPOR
*               xmo_GCREA3 xmo_GINTER3 xmo_GINTRA3 xmo_GEPMA3
*               xmo_ASE3 et xmo_ATETPORJ3 en entree
* cmoginf.inc : mo_NMAC en entree
* cmogtem.inc : xmo_TEMPS en entree 
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE xmo_RPAST en entree 
* cmogdon.inc : xmo_CXEPORI3 xmo_CXEPORJI3  en entree
* cmogppr.inc : xmo_RHOLIM xmo_RHODL xmo_MARGRHO_FR  en entree
*               xmo_ATETPORJ en sortie
* cmogent.inc : xmo_ASE en sortie
* cmogphy.inc : xmo_PIZAHL en entr�e
************************************************************************
*
*
* variables locales
*
*
      DOUBLE PRECISION  xmo_CXE(mo_NNSPHMX),
     &                  xmo_CXERBLOC(mo_NNSPHMX),
     &                  xmo_CXEJ(mo_NNSPHMX),
     &                  xmo_CXEHOM(mo_NNSPHMX),
     &                  xmo_VG1(mo_NVGAMX),
     &                  xmo_VD1(mo_NVDEMX)
*
*-----7--0---------0---------0---------0---------0---------0---------0--
*
      CALL MOMESS('DANS MOWMAC')
*
***********************************************************************
      DO IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VECG(L,IGROB,I,IHETER,IVGA,1)
      ENDDO
*
      DO IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VECD(L,IGROB,I,IHETER,IVDE,1)
      ENDDO
*
CDEBUG
CDEBUG          IF ( L .EQ. mo_LSUIVI .AND. 
CDEBUG     &          IGROB .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          I .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(20,*) 'dans mowmac'
CDEBUG       WRITE(20,*) 'mo_NVG = ',mo_NVG,' xmo_VG1'
CDEBUG       WRITE(20,*) (xmo_VG1(IVGA), IVGA = 1,mo_NVG)
CDEBUG       WRITE(20,*) 'mo_NVD = ',mo_NVD,' xmo_VD1'
CDEBUG       WRITE(20,*) (xmo_VD1(IVGA), IVGA = 1,mo_NVD)
CDEBUG          ENDIF
CDEBUG
      mo_IET = mo_IETCOM(L,IGROB,I,IHETER,1)
*
      xmo_SORDIFJ = xmo_SORDIFJ3(L,IGROB,I,IHETER,1)
      xmo_SORPERC = xmo_SORPERC3(L,IGROB,I,IHETER,1)
      xmo_SOREJEC = xmo_SOREJEC3(L,IGROB,I,IHETER,1)
      xmo_ERRGLOB = xmo_ERRGLOB3(L,IGROB,I,IHETER)
*
      IF (mo_IET .NE. 3) THEN
*      ----------------------
* pour mocaff xmo_ASE ou xmo_ASERIM
        IF (mo_IET .EQ. 0) THEN
          xmo_ASE = xmo_ASE3(L,IGROB,I)
        ELSEIF (mo_IET .EQ. 2) THEN
          xmo_ASERIM = xmo_ASE3(L,IGROB,I)          
        ENDIF
*
* pour mocaff xmo_ATETPORJ
        xmo_CPORJI = xmo_CPORJI3(L,IGROB,I,IHETER)
        xmo_VPORJI = xmo_VTPORJI3(L,IGROB,I,IHETER)
        xmo_AGG = xmo_AGG3(L,IGROB,I,IHETER)
        CALL MORONLEN(mo_PORJ_RON)
        IF (mo_PORJ_RON .EQ. 1) THEN
          xmo_TETPORJ = xmo_PIZAHL / 2.D0
          xmo_ATETPORJ = 1.D0
        ELSE
         xmo_TETPORJ = xmo_TETBJ
          xmo_ATETPORJ = xmo_ATETBJ
        ENDIF
      ELSE
*     ----
        xmo_ASERIM = xmo_ASE3(L,IGROB,I)
      ENDIF
*     -----
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat classique-connecte (0)
*
      IF (mo_IET .EQ. 0) THEN
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
*
*
************************************************************************
* Calcul de xmo_CXEM et xmo_CXEJM
************************************************************************
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz intra dissous 
*
        CALL MOMOYE (xmo_CXE,  xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
*
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &                 mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
          xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
* Calcul des taux de couverture xmo_FPORJ et xmo_FBJ
*
        CALL MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,0.D0,
     &                  xmo_FBJ,xmo_FPORJ,xmo_FEXT)
*
Calcul des part inter intra et retention (/)
        IF ( xmo_GCREA3(L,IGROB,I,IHETER,1) .GT. 0.D0 ) THEN
          xmo_PART_INTRA = xmo_GINTRA3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
          xmo_PART_INTER = xmo_GINTER3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
          xmo_PART_RETEN = xmo_PART_INTRA + xmo_PART_INTER
        ELSE
          xmo_PART_INTRA = 0.D0
          xmo_PART_INTER = 0.D0
          xmo_PART_RETEN = 0.D0
        ENDIF
*
        WRITE(mo_NMAC,9000) 
     &      xmo_TEMPS,  
     &      L, IGROB, I, 
     &      xmo_RPAST(L,IGROB,I),
     &      mo_IET, 
     &      xmo_CXEM,
     &      xmo_CB, xmo_CXEB, xmo_VTB, xmo_RB,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_RHOD,
     &      xmo_CXEJM,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1) + 
     &         xmo_CXEPORI3(L,IGROB,I,IHETER) +
     &         xmo_CXEPORJI3(L,IGROB,I,IHETER),
     &      xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1), 
     &      xmo_GINTRA3(L,IGROB,I,IHETER,1),
     &      xmo_GINTER3(L,IGROB,I,IHETER,1),
     &      xmo_GEPMA3(L,IGROB,I,IHETER,1), 
     &      xmo_SOMTOFIS3(L,IGROB,I,IHETER,1),
     &      xmo_GONFS3(L,IGROB,I,IHETER,1),
     &      xmo_SINPOR(L,IGROB,I,IHETER,1),
     &      xmo_GASPOR(L,IGROB,I,IHETER,1),
     &      0.D0,
     &      xmo_FBJ,
     &      xmo_FPORJ,  
     &      xmo_PART_INTRA,
     &      xmo_PART_INTER, 
     &      xmo_PART_RETEN
*
*
************************************************************************
* fin du traitement de l'�tat classique-connecte (0)
*
      ENDIF
*
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat "en restructuration" (2)
*
      IF (mo_IET .EQ. 2 ) THEN
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
************************************************************************
* Calcul de xmo_CXEM et xmo_CXEHOMM
************************************************************************
*
*
* Calcul de la moyenne xmo_CXEM des concentrations en 
* gaz dissous dans les zones saines
*
        CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &               mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
* Calcul de la moyenne xmo_CXEJM des concentrations en gaz inter dissous
*
        IF (mo_DIF_SEQ .EQ. 1) THEN
          CALL MOMOYE (xmo_CXEJ, xmo_TVSEQ, 
     &                 mo_NSEQ, mo_NNSPHMX, xmo_CXEJM)
        ELSE
          xmo_CXEJM = xmo_CXEJ(1)
        ENDIF
*
* Calcul de la moyenne xmo_CXEHOMM des concentrations en 
* gaz dans le mileu homog�n�is�  
*
        CALL MOMOYE (xmo_CXEHOM, xmo_TVSEQR,
     &                 mo_NSEQR, mo_NNSPHMX, xmo_CXEHOMM)
*
************************************************************************
* Calcul de xmo_FR
* On doit utiliser une densit� de dislocation xmo_RHOD1 ne d�passant pas
* xmo_RHODLIM + xmo_RHODL - xmo_MARGRHO_FR pour les calcul de xmo_FR  
* Sinon, on a des divisions par zero
*
          xmo_RHOD_FRLIM =  xmo_RHOLIM + xmo_RHODL - xmo_MARGRHO_FR
          xmo_RHOD1 = min(xmo_RHOD, xmo_RHOD_FRLIM)
*
* Calcul de la fraction de volume restructur� xmo_FR
          CALL MOFRAC_GEAR(xmo_RHOD1,xmo_FR)
************************************************************************
* Calcul des taux de couverture xmo_FPORJ et xmo_FBJ
*
        CALL MOCAFF(xmo_CPORJ,xmo_VTPORJ,xmo_CBJ,xmo_VTBJ,xmo_FR,
     &                  xmo_FBJ,xmo_FPORJ,xmo_FEXT)
************************************************************************
*
* Calcul des part inter intra et retention (/)
        xmo_PART_INTRA = xmo_GINTRA3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
        xmo_PART_INTER = xmo_GINTER3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
        xmo_PART_RETEN = xmo_PART_INTRA + xmo_PART_INTER
*
*
        WRITE(mo_NMAC,9000) 
     &      xmo_TEMPS,  
     &      L, IGROB,I, 
     &      xmo_RPAST(L,IGROB,I),
     &      mo_IET, 
     &      xmo_CXEM,
     &      xmo_CB, xmo_CXEB, xmo_VTB, xmo_RB,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_RHOD,
     &      xmo_CXEJM,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1) + 
     &         xmo_CXEPORI3(L,IGROB,I,IHETER) +
     &         xmo_CXEPORJI3(L,IGROB,I,IHETER),
     &      xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1), 
     &      xmo_GINTRA3(L,IGROB,I,IHETER,1),
     &      xmo_GINTER3(L,IGROB,I,IHETER,1),
     &      xmo_GEPMA3(L,IGROB,I,IHETER,1), 
     &      xmo_SOMTOFIS3(L,IGROB,I,IHETER,1),
     &      xmo_GONFS3(L,IGROB,I,IHETER,1),
     &      xmo_SINPOR(L,IGROB,I,IHETER,1),
     &      xmo_GASPOR(L,IGROB,I,IHETER,1),
     &      xmo_FR,
     &      xmo_FBJ,
     &      xmo_FPORJ, 
     &      xmo_PART_INTRA,
     &      xmo_PART_INTER, 
     &      xmo_PART_RETEN                  
*
*
************************************************************************
* fin du traitement de l'�tat "en restructuration" (2)
*
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************
* le combustible est dans l'�tat restructur� (3)
*
      IF (mo_IET .EQ. 3) THEN
*      
************************************************************************
* Utilisation de variables locales plus parlantes
************************************************************************
*
        CALL MOPARL3(xmo_VG1,xmo_VD1,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM)
*
************************************************************************
* Calcul de xmo_CXEHOMM
************************************************************************
*
*
* Calcul de la moyenne xmo_CXEHOMM des concentrations en 
* gaz dans le mileu homog�n�is�  
*
        CALL MOMOYE (xmo_CXEHOM, xmo_TVSEQR, 
     &                 mo_NSEQR, mo_NNSPHMX, xmo_CXEHOMM)
*
* Calcul des part inter intra et retention (/)
        xmo_PART_INTRA = xmo_GINTRA3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
        xmo_PART_INTER = xmo_GINTER3(L,IGROB,I,IHETER,1) /
     &                   xmo_GCREA3(L,IGROB,I,IHETER,1)
        xmo_PART_RETEN = xmo_PART_INTRA + xmo_PART_INTER
*
        WRITE(mo_NMAC,9000) 
     &      xmo_TEMPS, 
     &      L, IGROB,I, 
     &      xmo_RPAST(L,IGROB,I),
     &      mo_IET, 
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1) + 
     &         xmo_CXEPORI3(L,IGROB,I,IHETER) +
     &         xmo_CXEPORJI3(L,IGROB,I,IHETER),
     &      xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB,
     &      xmo_GCREA3(L,IGROB,I,IHETER,1), 
     &      xmo_GINTRA3(L,IGROB,I,IHETER,1),
     &      xmo_GINTER3(L,IGROB,I,IHETER,1),
     &      xmo_GEPMA3(L,IGROB,I,IHETER,1), 
     &      xmo_SOMTOFIS3(L,IGROB,I,IHETER,1),
     &      xmo_GONFS3(L,IGROB,I,IHETER,1),
     &      xmo_SINPOR(L,IGROB,I,IHETER,1),
     &      xmo_GASPOR(L,IGROB,I,IHETER,1),
     &      1.D0,
     &      0.D0,
     &      0.D0,  
     &      xmo_PART_INTRA,
     &      xmo_PART_INTER, 
     &      xmo_PART_RETEN                    
*
*
************************************************************************
* fin du traitement de l'�tat restructur� (3)
*
      ENDIF
************************************************************************
************************************************************************
************************************************************************
************************************************************************
************************************************************************  
*
*
*
*
* format pour ecriture dans fichier res_macroscopique
 9000 FORMAT (1X,E26.8,3I26,E26.8,I26,47(E26.8)) 
*
      RETURN
      END
      SUBROUTINE MOWVAR(IIMPR,DTDECO,mo_PREMIERSOUSPAS)
C     =================
C
C
C     ******************************************************************
C     MOWVAR : MOgador Write au temps xmo_TEMPS des vecteurs VARiables
C              dans les fichiers d�di�s et calcul de ce temps                        
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogpas.inc'
      INCLUDE 'cmogent.inc'
      INCLUDE 'cmogsor.inc'
C
CDEBUG 
      INCLUDE 'cmogdebug.inc'
CDEBUG 
C
C
***********************************************************************
* cmogvar.inc : xmo_VG, xmo_VD, mo_IET en entr�es 
* cmogpnu.inc : mo_NGG mo_NSEQ mo_NSEQR xmo_RGG en entr�e
* cmoginf.inc : mo_NRES1 mo_NRES2 mo_NRES3 mo_NSYNT mo_FREQIMPR en entr�e              
* cmogtem.inc : xmo_TEMPS en entr�e 
* cmogmat.inc : xmo_TVGG xmo_TVSEQR  en entr�e
*               pour test xmoTIMPGG en entr�e
* cmogdon.inc : xmo_AGG en entr�e
* cmogpas.inc : xmo_BETAC xmo_BJ xmo_DXE  en entr�e
* cmogent.inc : xmo_CXEJLIM  en entr�e
* cmogsor.inc : xmo_TAUXBETAC xmo_TAUXREMISOL xmo_TAUXSOURXE 
*               xmo_TAUXSORBUL xmo_TAUXSORPOR xmo_TAUXSORPOR
*               xmo_TAUXOUTSE xmo_TAUXOUTPERC xmo_TAUXOUTEJEC
*               xmo_GCREE xmo_REMISOL xmo_SORDIF xmo_SORBUL xmo_SORPOR
*               xmo_SORDIFJ xmo_SORPERC
*               en entr�e et sortie
***********************************************************************      
* IIMPR est � la fois un param�tre d'entr�e et de sortie
* en entr�e : si IIMPR = 0 l'impression se fait � la fr�quence mo_FREQIMPR
*             si IIMPR = 1 l'impression se fait syst�matiquement
* en sortie : l'impression n'a pas �t� effectu�e alors IIMPR = 0
*             l'impression a �t� effectu�e alors       IIMPR = 1
***********************************************************************
* 
      DOUBLE PRECISION 
* les m�mes tableaux que dans mofder_gear
*
     &                 xmo_VG1(mo_NVGAMX),
     &                 xmo_VD1(mo_NVDEMX),
     &                 xmo_CXE(mo_NNSPHMX),
     &                 xmo_CXERBLOC(mo_NNSPHMX),
     &                 xmo_CXEJ(mo_NNSPHMX),
     &                 xmo_CXEHOM(mo_NNSPHMX),
     &                 xmo_XE_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XE_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XE_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XE_CREATION(mo_NNSPHMX),
     &                 xmo_XE_REMJOINT(mo_NNSPHMX),
     &                 xmo_XE_REMBUL(mo_NNSPHMX),
     &                 xmo_XE_REMPOR(mo_NNSPHMX),
     &                 xmo_XE_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XE_SORTIEGRAIN(mo_NNSPHMX),
     &                 xmo_XE_CLIM(mo_NNSPHMX),
     &                 xmo_XERBLOC_EVOL(mo_NNSPHMX),
     &                 xmo_XEJ_NUCLHOMO(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGBUL(mo_NNSPHMX),
     &                 xmo_XEJ_SOURCE(mo_NNSPHMX),
     &                 xmo_XEJ_REMJOINT(mo_NNSPHMX),
     &                 xmo_XEJ_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGPOR(mo_NNSPHMX),
     &                 xmo_XEJ_CLIM3(mo_NNSPHMX),
     &                 xmo_XEJ_PIEGTUN(mo_NNSPHMX),
     &                 xmo_XEJ_SORTIEJ(mo_NNSPHMX),
     &                 xmo_XEHOM_CREATION(mo_NNSPHMX),
     &                 xmo_XEHOM_DIFFUSION(mo_NNSPHMX),
     &                 xmo_XEHOM_APPORT(mo_NNSPHMX),
     &                 xmo_XEHOM_CLIM3(mo_NNSPHMX)
*
***********************************************************************
      CALL MOMESS('DANS MOWVAR')
*
C
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       IF (mo_IET .EQ. 2) THEN
CDEBUG               WRITE(mo_DEBUG,*) 'd�but MOWVAR'
CDEBUG       ENDIF
CDEBUG       ENDIF
CDEBUG
CDEBUG             mo_FLAG = 1
CDEBUG
C
*
*
***********************************************************************
* calcul des grandeurs n�cessaires aux bilans et aux impressions
***********************************************************************
*
      DO 50 IVGA = 1, mo_NVGAMX
        xmo_VG1(IVGA) = xmo_VG(IVGA,1)
 50   CONTINUE
*
      DO 60 IVDE = 1, mo_NVDEMX
        xmo_VD1(IVDE) = xmo_VD(IVDE,1)
 60   CONTINUE
*
*
**********************************************************************
* CAS de l'�tat classique
**********************************************************************
      IF (mo_IET .EQ. 0) THEN
*     ***********************
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
* Utilisation de variables locales plus parlantes
*
        CALL MOPARL(mo_IET,xmo_VG1,xmo_VD1,
     &    xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &    xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &               xmo_RHOD)
*
* Calcul de termes des d�riv�es des variables intra
*
        CALL MOTERM(
     &      xmo_CXE,xmo_CXERBLOC,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
* essaiLOC
     &      xmo_TEMPS,
* essaiLOC
     &
     &      xmo_XE_NUCLHOMO,
     &      xmo_XE_PIEGBUL,xmo_XE_PIEGPOR,xmo_XE_CREATION,
     &      xmo_XE_REMJOINT,xmo_XE_REMBUL,xmo_XE_REMPOR,
     &      xmo_XE_DIFFUSION,xmo_XE_SORTIEGRAIN,xmo_XE_CLIM,
     &      xmo_XERBLOC_EVOL,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,xmo_B_COALEVB,xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,xmo_XEB_PIEGBPOR,xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,xmo_VTB_GAZ,xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,xmo_POR_COALEVPOR,xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,xmo_XEPOR_PIEGBPOR,xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,xmo_VTPOR_GAZ,xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,xmo_VTPOR_SORTIEGRAIN,
     &      xmo_CXECR  )
*
* Calcul des termes des d�riv�es des variables inter dans le cas 
* o� l'�tat du combustible est "classique" (0)
*
        CALL MOTRM0(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEJ_NUCLHOMO,xmo_XEJ_PIEGBUL,xmo_XEJ_PIEGPOR,
     &      xmo_XEJ_SOURCE,xmo_XEJ_REMJOINT,xmo_XEJ_DIFFUSION,
     &      xmo_XEJ_CLIM3,xmo_XEJ_SORTIEJ,
     &      xmo_BJ_NUCLHOMO,xmo_BJ_APPORT,xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,xmo_BJ_REMBUL,xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,xmo_XEBJ_APPORT,xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,xmo_XEBJ_REMBUL,xmo_XEBJ_SORTIEJ,
     &      xmo_XEBJ_PERCBPOR,xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,xmo_VTBJ_GAZ,xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,xmo_VTBJ_APPORT,xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,xmo_XEPORJ_APPORT,xmo_XEPORJ_SORTIEJ,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,xmo_VTPORJ_GAZ,xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,xmo_VTPORJ_SORTIEJ,
     &      
     &      xmo_SOURXE,xmo_HJ,xmo_HJPOR,xmo_BBJ,xmo_BPORJ,
     &      xmo_OUTSE,xmo_OUTEJEC,xmo_CXEJM,xmo_PINTBJ)
*
        CALL MOMOYE (xmo_CXE,  xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
        CALL MOMOYE (xmo_XE_NUCLHOMO, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_NUCLHOMO_M)
*
        CALL MOMOYE (xmo_XE_PIEGBUL, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_PIEGBUL_M)
*
        CALL MOMOYE (xmo_XE_PIEGPOR, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_PIEGPOR_M)
*
        CALL MOMOYE (xmo_XE_CREATION, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_CREATION_M)
*
        CALL MOMOYE (xmo_XE_REMJOINT, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_REMJOINT_M)
*
        CALL MOMOYE (xmo_XE_REMBUL, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_REMBUL_M)
*
        CALL MOMOYE (xmo_XE_REMPOR, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_REMPOR_M)
*
        CALL MOMOYE (xmo_XE_DIFFUSION, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_DIFFUSION_M)
*
        CALL MOMOYE (xmo_XE_CLIM, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_CLIM_M)
*
        CALL MOMOYE (xmo_XE_SORTIEGRAIN, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_SORTIEGRAIN_M)
*
        IF (mo_DIF_SEQ .EQ. 1) THEN
*       ===========================
          CALL MOMOYE (xmo_XEJ_NUCLHOMO, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_NUCLHOMO_M)
*
          CALL MOMOYE (xmo_XEJ_PIEGBUL, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_PIEGBUL_M)
*
          CALL MOMOYE (xmo_XEJ_PIEGPOR, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_PIEGPOR_M)
*
          CALL MOMOYE (xmo_XEJ_SOURCE, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_SOURCE_M)
*
          CALL MOMOYE (xmo_XEJ_REMJOINT, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_REMJOINT_M)
*
          CALL MOMOYE (xmo_XEJ_DIFFUSION, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_DIFFUSION_M)
*
          CALL MOMOYE (xmo_XEJ_CLIM3, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_CLIM3_M)
*
          CALL MOMOYE (xmo_XEJ_SORTIEJ, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_SORTIEJ_M)
        ELSE
*       ====
          xmo_XEJ_NUCLHOMO_M = xmo_XEJ_NUCLHOMO(1)
          xmo_XEJ_PIEGBUL_M = xmo_XEJ_PIEGBUL(1)
          xmo_XEJ_PIEGPOR_M = xmo_XEJ_PIEGPOR(1)
          xmo_XEJ_SOURCE_M = xmo_XEJ_SOURCE(1)
          xmo_XEJ_REMJOINT_M = xmo_XEJ_REMJOINT(1)
          xmo_XEJ_DIFFUSION_M = xmo_XEJ_DIFFUSION(1)
          xmo_XEJ_CLIM3_M = xmo_XEJ_CLIM3(1)
          xmo_XEJ_SORTIEJ_M = xmo_XEJ_SORTIEJ(1)
        ENDIF
*       =====
**********************************************************************
* Calcul des taux de gaz �chang�s (mol/s/m3)
**********************************************************************
*
        xmo_TAUXBETAC(1) = xmo_BETAC
        xmo_TAUXREMISOL(1) = xmo_BJ * xmo_CXEJM + 
     &                       xmo_BBJ * xmo_CXEBJ +
     &                       xmo_BPORJ * xmo_CXEPORJ
        xmo_TAUXSOURXE(1) = xmo_SOURXE
        xmo_TAUXSORBUL(1) = xmo_HJ * xmo_CXEB
        xmo_TAUXSORPOR(1) = xmo_HJPOR * xmo_CXEPOR
        xmo_TAUXOUTSE(1) = xmo_OUTSE
        xmo_TAUXOUTPERC(1) = - xmo_XEBJ_PERCPLEN
        xmo_TAUXOUTEJEC(1) = xmo_OUTEJEC
*
**********************************************************************
* Calcul des quantit�s de gaz et des les bilans
**********************************************************************
        CALL MOCABI(mo_IET, mo_PREMIERSOUSPAS, DTDECO,
     &                  xmo_CXEM, xmo_CXEB, xmo_CXEPOR,
     &                  xmo_CXEJM, xmo_CXEBJ, xmo_CXEPORJ,
     &                  0.D0, 0.D0, 0.D0)
*
**********************************************************************
* Calcul des pressions internes et des pressions d'�quilibre  : etat 0
**********************************************************************
* bulles intra
        CALL MOCOPR (xmo_CXEB, xmo_VTB, xmo_RB, 1,
     &               xmo_PINT_B, xmo_PEQ_B)
*
* bulles inter
        CALL MOCOPR (xmo_CXEBJ, xmo_VTBJ, xmo_RBJ, 2,
     &               xmo_PINT_BJ, xmo_PEQ_BJ)
*
* pores intra
        CALL MOCOPR (xmo_CXEPOR, xmo_VTPOR, xmo_RPOR, 4,
     &               xmo_PINT_POR, xmo_PEQ_POR)
*
* pores inter
        CALL MOCOPR (xmo_CXEPORJ, xmo_VTPORJ, xmo_RPORJ, 5,
     &               xmo_PINT_PORJ, xmo_PEQ_PORJ)
**********************************************************************
* Impressions d�taill�es
**********************************************************************
*
        IF ( IIMPR .EQ. 1 ) THEN
*       +++++++++++++++++++++++++
*
        IF ( mo_IMPR1 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans le fichier r�sultat1 
* Analyse des phenomenes intra (pour les etats (0) et (1))
*
          WRITE(mo_NRES1,7000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      xmo_CXEM,xmo_CB,xmo_CXEB,xmo_VTB, xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR, xmo_RPOR,
     &               xmo_RHOD,
*
     &      xmo_XE_CREATION_M,
     &      xmo_XE_DIFFUSION_M,
     &      xmo_XE_CLIM_M, 
     &      xmo_XE_NUCLHOMO_M,
     &      xmo_XE_PIEGBUL_M,
     &      xmo_XE_PIEGPOR_M,
     &      xmo_XE_REMJOINT_M,
     &      xmo_XE_REMBUL_M,
     &      xmo_XE_REMPOR_M, 
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,
     &      xmo_B_COALEVB,
     &      xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,
     &      xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,
     &      xmo_XEB_PIEGBPOR,
     &      xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,
     &      xmo_VTB_GAZ,
     &      xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,
     &      xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,
     &      xmo_POR_COALEVPOR,
     &      xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,
     &      xmo_XEPOR_PIEGBPOR,
     &      xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,
     &      xmo_VTPOR_GAZ,
     &      xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,
     &      xmo_VTPOR_SORTIEGRAIN,
     &      xmo_SORDIF,
     &      xmo_SORBUL,
     &      xmo_SORPOR,
     &      xmo_SOREJEC,
     &      xmo_REMISOL,
     &      xmo_GCREE,
     &      xmo_ERRINTRA,
     &      (xmo_CXE(IVGA),IVGA=1,mo_NGG)
*
        ENDIF 
*       :::::
*
        IF ( mo_IMPR2 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans le fichier r�sultat2 
* Analyse des phenomenes inter (pour l'etat (0) )                  
*
            WRITE(mo_NRES2,8000) xmo_TEMPS, mo_IET,mo_IET_APPR,
     &      xmo_CXEJM,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ, xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ, xmo_RPORJ, 
     &      xmo_XEJ_NUCLHOMO_M,
     &      xmo_XEJ_PIEGBUL_M,
     &      xmo_XEJ_PIEGPOR_M,
     &      xmo_XEJ_SOURCE_M,
     &      xmo_XEJ_REMJOINT_M,
     &      xmo_XEJ_DIFFUSION_M,
     &      xmo_XEJ_CLIM3_M,
     &      xmo_BJ_NUCLHOMO,
     &      xmo_BJ_APPORT,
     &      xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,
     &      xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,
     &      xmo_BJ_REMBUL,
     &      xmo_XEBJ_NUCLHOMO,
     &      xmo_XEBJ_APPORT,
     &      xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,
     &      xmo_XEBJ_REMBUL,
     &      xmo_XEBJ_PERCBPOR,
     &      xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,
     &      xmo_VTBJ_GAZ,
     &      xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,
     &      xmo_VTBJ_APPORT,
     &      xmo_XEPORJ_PIEGPOR,
     &      xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,
     &      xmo_XEPORJ_APPORT,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,
     &      xmo_VTPORJ_GAZ,
     &      xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,
     &      xmo_SORDIFJ,
     &      xmo_SORPERC,
     &      xmo_REMISOL,
     &      xmo_SORDIF,
     &      xmo_SORBUL,
     &      xmo_SORPOR,
     &      xmo_ERRINTER,
     &      (xmo_CXEJ(IVGA),IVGA=1,mo_NSEQ_C)
*
        ENDIF 
*       :::::
*
        IF ( mo_IMPRSYNT .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans le fichier synthese
*
            WRITE(mo_NSYNT,9000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      xmo_CXEM,
     &      xmo_CB, xmo_CXEB, xmo_VTB, xmo_RB,
     &      xmo_PINT_B, xmo_PEQ_B,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_PINT_POR, xmo_PEQ_POR,
     &      xmo_RHOD,
     &      xmo_CXEJM,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_PINT_BJ, xmo_PEQ_BJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_PINT_PORJ, xmo_PEQ_PORJ,
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0, 
     &      xmo_GCREE,xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB
*
*
        ENDIF 
*       :::::
*
        ENDIF
*       +++++
*
      ENDIF
*     *****
*
**********************************************************************
* CAS ETAT 'EN RESTRUCTURATION'
**********************************************************************
      IF (mo_IET .EQ. 2) THEN
*     ************************
*
* Consideration de la diffusion du gaz dissous a grande echelle ou non
*
        CALL MOCONS(mo_IET,mo_DIF_SEQ,mo_NSEQ_C)
*
          CALL MOPARL2(xmo_VG1,xmo_VD1,
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD)
*
        CALL MOTERM(
     &      xmo_CXE,xmo_CXERBLOC,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
* essaiLOC
     &      xmo_TEMPS,
* essaiLOC
     &
     &      xmo_XE_NUCLHOMO,
     &      xmo_XE_PIEGBUL,xmo_XE_PIEGPOR,xmo_XE_CREATION,
     &      xmo_XE_REMJOINT,xmo_XE_REMBUL,xmo_XE_REMPOR,
     &      xmo_XE_DIFFUSION,xmo_XE_SORTIEGRAIN,xmo_XE_CLIM,
     &      xmo_XERBLOC_EVOL,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,xmo_B_COALEVB,xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,xmo_XEB_PIEGBPOR,xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,xmo_VTB_GAZ,xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,xmo_POR_COALEVPOR,xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,xmo_XEPOR_PIEGBPOR,xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,xmo_VTPOR_GAZ,xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,xmo_VTPOR_SORTIEGRAIN,
     &      xmo_CXECR  )
*
* Calcul des termes des d�riv�es des variables inter 
*
        CALL MOTRM0(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &               xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEJ_NUCLHOMO,xmo_XEJ_PIEGBUL,xmo_XEJ_PIEGPOR,
     &      xmo_XEJ_SOURCE,xmo_XEJ_REMJOINT,xmo_XEJ_DIFFUSION,
     &      xmo_XEJ_CLIM3,xmo_XEJ_SORTIEJ,
     &      xmo_BJ_NUCLHOMO,xmo_BJ_APPORT,xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,xmo_BJ_REMBUL,xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,xmo_XEBJ_APPORT,xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,xmo_XEBJ_REMBUL,xmo_XEBJ_SORTIEJ,
     &      xmo_XEBJ_PERCBPOR,xmo_XEBJ_PERCPLEN,
     &      xmo_VTBJ_LACUNE,xmo_VTBJ_GAZ,xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,xmo_VTBJ_APPORT,xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,xmo_XEPORJ_APPORT,xmo_XEPORJ_SORTIEJ,
     &      xmo_XEPORJ_PERCBPOR,
     &      xmo_VTPORJ_LACUNE,xmo_VTPORJ_GAZ,xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,xmo_VTPORJ_SORTIEJ,
     &      
     &      xmo_SOURXE,xmo_HJ,xmo_HJPOR,xmo_BBJ,xmo_BPORJ,
     &      xmo_OUTSE_SEQ,xmo_OUTEJEC,xmo_CXEJM,xmo_PINTBJ)
*
* Calcul des termes des d�riv�es des variables des zones resructur�es
* dans le cas o� l'�tat du combustible est "en restructuration" (2)
*
      CALL MOTRM2(
     &      xmo_CXE,xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &              xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJ,xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &               xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_APPORT,
     &      xmo_BRIM_APPORT,
     &      xmo_XEBRIM_APPORT,
     &      xmo_VTBRIM_APPORT,
     &      xmo_PORRIM_APPORT,
     &      xmo_XEPORRIM_APPORT,
     &      xmo_VTPORRIM_APPORT )
*
* Calcul des termes des d�riv�es des variables des zones resructur�es
* 
*
      CALL MOTRM3(
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_CREATION,xmo_XEHOM_DIFFUSION,
     &      xmo_XEHOM_CLIM3,
     &      
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ,
     &      
     &      xmo_CXEHOMM,xmo_OUTSE_RIM,xmo_OUTEJEC_RIM )
*
* calcul de xmo_CXEM
          CALL MOMOYE (xmo_CXE, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_CXEM)
*
          CALL MOMOYE (xmo_XE_NUCLHOMO, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX,xmo_XE_NUCLHOMO _M)
          CALL MOMOYE (xmo_XE_PIEGBUL,xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_PIEGBUL_M)
          CALL MOMOYE (xmo_XE_PIEGPOR, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_PIEGPOR_M)
          CALL MOMOYE (xmo_XE_CREATION, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_CREATION_M)
          CALL MOMOYE (xmo_XE_REMJOINT, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_REMJOINT_M)
          CALL MOMOYE (xmo_XE_REMBUL, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_REMBUL_M)
          CALL MOMOYE (xmo_XE_REMPOR, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_REMPOR_M)
          CALL MOMOYE (xmo_XE_DIFFUSION, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_DIFFUSION_M)
          CALL MOMOYE (xmo_XE_CLIM, xmo_TVGG,
     &                   mo_NGG, mo_NNSPHMX, xmo_XE_CLIM_M)
          CALL MOMOYE (xmo_XE_SORTIEGRAIN, xmo_TVGG,
     &                mo_NGG, mo_NNSPHMX, xmo_XE_SORTIEGRAIN_M)
*
        IF (mo_DIF_SEQ .EQ. 1) THEN
*       ===========================
          CALL MOMOYE (xmo_XEJ_NUCLHOMO, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_NUCLHOMO_M)
*
          CALL MOMOYE (xmo_XEJ_PIEGBUL, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_PIEGBUL_M)
*
          CALL MOMOYE (xmo_XEJ_PIEGPOR, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_PIEGPOR_M)
*
          CALL MOMOYE (xmo_XEJ_SOURCE, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_SOURCE_M)
*
          CALL MOMOYE (xmo_XEJ_REMJOINT, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_REMJOINT_M)
*
          CALL MOMOYE (xmo_XEJ_DIFFUSION, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_DIFFUSION_M)
*
          CALL MOMOYE (xmo_XEJ_CLIM3, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_CLIM3_M)
*
          CALL MOMOYE (xmo_XEJ_SORTIEJ, xmo_TVSEQ,
     &                   mo_NSEQ, mo_NNSPHMX, xmo_XEJ_SORTIEJ_M)
        ELSE
*       ====
          xmo_XEJ_NUCLHOMO_M = xmo_XEJ_NUCLHOMO(1)
          xmo_XEJ_PIEGBUL_M = xmo_XEJ_PIEGBUL(1)
          xmo_XEJ_PIEGPOR_M = xmo_XEJ_PIEGPOR(1)
          xmo_XEJ_SOURCE_M = xmo_XEJ_SOURCE(1)
          xmo_XEJ_REMJOINT_M = xmo_XEJ_REMJOINT(1)
          xmo_XEJ_DIFFUSION_M = xmo_XEJ_DIFFUSION(1)
          xmo_XEJ_CLIM3_M = xmo_XEJ_CLIM3(1)
          xmo_XEJ_SORTIEJ_M = xmo_XEJ_SORTIEJ(1)
        ENDIF
*       =====
*
          CALL MOMOYE (xmo_XEHOM_APPORT, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_APPORT_M)
          CALL MOMOYE (xmo_XEHOM_CREATION, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_CREATION_M)
          CALL MOMOYE (xmo_XEHOM_DIFFUSION, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_DIFFUSION_M)
          CALL MOMOYE (xmo_XEHOM_CLIM3, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_CLIM3_M)
*
*
*
***********************************************************************
* Calcul des taux de gaz �chang�s (mol/s/m3)
**********************************************************************
*
        xmo_TAUXBETAC(1) = xmo_BETAC
        xmo_TAUXOUTSE(1) = xmo_OUTSE_SEQ + xmo_OUTSE_RIM
*essaiPERC2        xmo_TAUXOUTPERC(1) = 0.D0
        xmo_TAUXOUTPERC(1) = - xmo_XEBJ_PERCPLEN
*essaiPERC2
        xmo_TAUXOUTEJEC(1) = xmo_OUTEJEC + xmo_OUTEJEC_RIM
*
**********************************************************************
* Calcul des quantit�s de gaz et des les bilans
**********************************************************************
        CALL MOCABI(mo_IET, mo_PREMIERSOUSPAS, DTDECO,
     &                  xmo_CXEM, xmo_CXEB, xmo_CXEPOR,
     &                  xmo_CXEJM, xmo_CXEBJ, xmo_CXEPORJ,
     &                  xmo_CXEHOMM, xmo_CXEBRIM , xmo_CXEPORRIM)
*
*
**********************************************************************
* Calcul des pressions internes et des pressions d'�quilibre  : etat 2
**********************************************************************
* bulles intra
        CALL MOCOPR (xmo_CXEB, xmo_VTB, xmo_RB, 1,
     &               xmo_PINT_B, xmo_PEQ_B)
*
* pores intra
        CALL MOCOPR (xmo_CXEPOR, xmo_VTPOR, xmo_RPOR, 4,
     &               xmo_PINT_POR, xmo_PEQ_POR)
*
* bulles inter
        CALL MOCOPR (xmo_CXEBJ, xmo_VTBJ, xmo_RBJ, 2,
     &               xmo_PINT_BJ, xmo_PEQ_BJ)
*
* pores inter
        CALL MOCOPR (xmo_CXEPORJ, xmo_VTPORJ, xmo_RPORJ, 5,
     &               xmo_PINT_PORJ, xmo_PEQ_PORJ)
*
* bulles de rim
        CALL MOCOPR (xmo_CXEBRIM, xmo_VTBRIM, xmo_RBRIM, 6 ,
     &               xmo_PINT_BRIM, xmo_PEQ_BRIM)
*
* pores de rim
        CALL MOCOPR (xmo_CXEPORRIM, xmo_VTPORRIM, xmo_RPORRIM, 7,
     &               xmo_PINT_PORRIM, xmo_PEQ_PORRIM)
**********************************************************************
* Impressions d�taill�es
**********************************************************************
        IF ( IIMPR .EQ. 1 ) THEN
*       +++++++++++++++++++++++++
*
        IF ( mo_IMPR4 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans les fichiers resultat4 
* (anlyse des resultats pour les etats (2) et (3))
*
            WRITE(mo_NRES4,8700) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      xmo_CXEM,
     &      xmo_CB,xmo_CXEB,xmo_VTB,xmo_RB,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_CXEJM,
     &      xmo_CBJ,xmo_CXEBJ,xmo_VTBJ,xmo_RBJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_RHOD,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
*
     &      xmo_XE_NUCLHOMO_M,
     &      xmo_XE_PIEGBUL_M,
     &      xmo_XE_PIEGPOR_M,
     &      xmo_XE_CREATION_M,
     &      xmo_XE_REMJOINT_M,
     &      xmo_XE_REMBUL_M,
     &      xmo_XE_REMPOR_M,
     &      xmo_XE_DIFFUSION_M,
     &      xmo_XE_CLIM_M,
     &      xmo_XE_SORTIEGRAIN_M,
     &      xmo_B_NUCLHOMO,
     &      xmo_B_COALEDB,
     &      xmo_B_COALEVB,
     &      xmo_B_COALEGEOM,
     &      xmo_B_PIEGBPOR,
     &      xmo_B_REMBUL,
     &      xmo_B_SORTIEGRAIN,
     &      xmo_XEB_NUCLHOMO,
     &      xmo_XEB_PIEGBUL,
     &      xmo_XEB_PIEGBPOR,
     &      xmo_XEB_REMBUL,
     &      xmo_XEB_SORTIEGRAIN,
     &      xmo_VTB_LACUNE,
     &      xmo_VTB_GAZ,
     &      xmo_VTB_PIEGBPOR,
     &      xmo_VTB_REMBUL,
     &      xmo_VTB_SORTIEGRAIN,
     &      xmo_POR_COALEDPOR,
     &      xmo_POR_COALEVPOR,
     &      xmo_POR_REMPOR,
     &      xmo_POR_SORTIEGRAIN,
     &      xmo_XEPOR_PIEGPOR,
     &      xmo_XEPOR_PIEGBPOR,
     &      xmo_XEPOR_REMPOR,
     &      xmo_XEPOR_SORTIEGRAIN,
     &      xmo_VTPOR_LACUNE,
     &      xmo_VTPOR_GAZ,
     &      xmo_VTPOR_PIEGBPOR,
     &      xmo_VTPOR_REMPOR,
     &      xmo_VTPOR_SORTIEGRAIN,
*
     &      xmo_XEJ_NUCLHOMO_M,
     &      xmo_XEJ_PIEGBUL_M,
     &      xmo_XEJ_PIEGPOR_M,
     &      xmo_XEJ_SOURCE_M,
     &      xmo_XEJ_REMJOINT_M,
     &      xmo_XEJ_DIFFUSION_M,
     &      xmo_XEJ_CLIM3_M,
     &      xmo_XEJ_SORTIEJ_M,
     &      xmo_BJ_NUCLHOMO,
     &      xmo_BJ_APPORT,
     &      xmo_BJ_COALEDBJ,
     &      xmo_BJ_COALEVBJ,
     &      xmo_BJ_COALEGEOMJ,
     &      xmo_BJ_PIEGBPOR,
     &      xmo_BJ_REMBUL,
     &      xmo_BJ_SORTIEJ,
     &      xmo_XEBJ_NUCLHOMO,
     &      xmo_XEBJ_APPORT,
     &      xmo_XEBJ_PIEGBUL,
     &      xmo_XEBJ_PIEGBPOR,
     &      xmo_XEBJ_REMBUL,
     &      xmo_XEBJ_SORTIEJ,
     &      xmo_VTBJ_LACUNE,
     &      xmo_VTBJ_GAZ,
     &      xmo_VTBJ_PIEGBPOR,
     &      xmo_VTBJ_REMBUL,
     &      xmo_VTBJ_APPORT,
     &      xmo_VTBJ_SORTIEJ,
     &      xmo_PORJ_SORTIEJ,
     &      xmo_XEPORJ_PIEGPOR,
     &      xmo_XEPORJ_PIEGBPOR,
     &      xmo_XEPORJ_REMPOR,
     &      xmo_XEPORJ_APPORT,
     &      xmo_XEPORJ_SORTIEJ,
     &      xmo_VTPORJ_LACUNE,
     &      xmo_VTPORJ_GAZ,
     &      xmo_VTPORJ_PIEGBPOR,
     &      xmo_VTPORJ_APPORT,
     &      xmo_VTPORJ_SORTIEJ,
*
     &      xmo_XEHOM_APPORT_M,
     &      xmo_XEHOM_CREATION_M,
     &      xmo_XEHOM_DIFFUSION_M,
     &      xmo_XEHOM_CLIM3_M,
     &      xmo_BRIM_APPORT,
     &      xmo_XEBRIM_APPORT,
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_APPORT,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      xmo_PORRIM_APPORT,
     &      xmo_XEPORRIM_APPORT,
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_APPORT,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ
*
        ENDIF 
*       :::::
*
        IF ( mo_IMPRSYNT .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans le fichier synthese
*
            WRITE(mo_NSYNT,9000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      xmo_CXEM,
     &      xmo_CB, xmo_CXEB, xmo_VTB, xmo_RB,
     &      xmo_PINT_B, xmo_PEQ_B,
     &      xmo_CPOR,xmo_CXEPOR,xmo_VTPOR,xmo_RPOR,
     &      xmo_PINT_POR, xmo_PEQ_POR,
     &      xmo_RHOD,
     &      xmo_CXEJM,
     &      xmo_CBJ, xmo_CXEBJ, xmo_VTBJ, xmo_RBJ,
     &      xmo_PINT_BJ, xmo_PEQ_BJ,
     &      xmo_CPORJ,xmo_CXEPORJ,xmo_VTPORJ,xmo_RPORJ,
     &      xmo_PINT_PORJ, xmo_PEQ_PORJ,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_PINT_BRIM, xmo_PEQ_BRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_PINT_PORRIM, xmo_PEQ_PORRIM,
     &      xmo_GCREE,xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB
*
        ENDIF 
*       :::::
*
*
*
        IF ( mo_IMPR5 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Pour v�rification bilans termes a terme
*
            ERR1 = (xmo_XE_NUCLHOMO_M + xmo_XEB_NUCLHOMO) 
*
            ERR4 = (xmo_XE_PIEGBUL_M + xmo_XEB_PIEGBUL) 
*    
            ERR5 = ( xmo_XE_PIEGPOR_M + xmo_XEPOR_PIEGPOR) 
*
            ERR6 = (xmo_XE_REMBUL_M + xmo_XEB_REMBUL) 
*
            ERR7 = (xmo_XE_REMPOR_M + xmo_XEPOR_REMPOR) 
*
            ERR8 = (xmo_XEB_PIEGBPOR + xmo_XEPOR_PIEGBPOR)
*
            ERR9 = (xmo_XE_SORTIEGRAIN_M +
     &              xmo_XEJ_SORTIEJ_M +  
     &              xmo_XEHOM_APPORT_M) 
*
            ERR10 = (xmo_XEB_SORTIEGRAIN + xmo_XEBJ_SORTIEJ + 
     &               xmo_XEPOR_SORTIEGRAIN + xmo_XEBRIM_APPORT) 
*
            ERR11 = (xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M +
     &               xmo_XEJ_SOURCE_M + xmo_XEJ_REMJOINT_M + 
     &               xmo_XEBJ_APPORT + xmo_XEBJ_REMBUL +
     &               xmo_XEPORJ_APPORT + xmo_XEPORJ_REMPOR +
     &               xmo_OUTEJEC)

*
            ERR12 = xmo_XEJ_NUCLHOMO_M +xmo_XEBJ_NUCLHOMO
*
            ERR13 = xmo_XEJ_PIEGBUL_M +xmo_XEBJ_PIEGBUL
*
            ERR14 = xmo_XEJ_PIEGPOR_M +xmo_XEPORJ_PIEGPOR
*
            ERR15 = xmo_XEBJ_PIEGBPOR +xmo_XEPORJ_PIEGBPOR
*

*
            ERRDE = xmo_XEJ_DIFFUSION_M + xmo_XEJ_CLIM3_M 
     &               + xmo_OUTSE_SEQ
*
            ERRDR = xmo_XEHOM_DIFFUSION_M + xmo_XEHOM_CLIM3_M 
     &               + xmo_OUTSE_RIM 
     &               + xmo_OUTEJEC_RIM
     &               + xmo_XEBRIM_ALIM_DIRECTE
     &               + xmo_XEBRIM_ALIM_ECHAP
     &               + xmo_XEBRIM_REMBRIM
     &               + xmo_XEPORRIM_ALIM_DIRECTE
     &               + xmo_XEPORRIM_ALIM_ECHAP
     &               + xmo_XEPORRIM_REMPORRIM
*
*
            ERRTOT = ERR1 + ERR4 + ERR5 + ERR6 +
     &         ERR7 + ERR8 + ERR9 + ERR10 + ERR11 + ERR12 +
     &         ERR13 + ERR14 + ERR15 + 
     &         ERRDE + ERRDR
*
            WRITE(mo_NRES5,6000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &                  ERR1, xmo_XEB_NUCLHOMO,
     &                  ERR4, xmo_XEB_PIEGBUL,
     &                  ERR5, xmo_XEPOR_PIEGPOR,
     &                  ERR6, xmo_XEB_REMBUL,
     &                  ERR7, xmo_XEPOR_REMPOR,
     &                  ERR8, xmo_XEPOR_PIEGBPOR,
     &                  ERR9, xmo_XEHOM_APPORT_M,
     &                  ERR10, xmo_XEBRIM_APPORT,
     &      ERR11, xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M,
*
     &      ERR12, xmo_XEBJ_NUCLHOMO,
     &      ERR13, xmo_XEBJ_PIEGBUL,
     &      ERR14, xmo_XEPORJ_PIEGPOR,
     &      ERR15, xmo_XEPORJ_PIEGBPOR,
*
     &      ERRDE, xmo_OUTSE_SEQ,
     &      ERRDR, xmo_XEHOM_DIFFUSION_M + xmo_XEHOM_CLIM3_M,
     &      ERRTOT, 
     &      xmo_BETAC
*
*
CDEBUG     &      , xmo_SOURXE, xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M
CDEBUG       IF ( mo_LLL .EQ. mo_LSUIVI .AND. 
CDEBUG     &      mo_IGRIGR .EQ. mo_IGRSUIVI  .AND.
CDEBUG     &          mo_III .EQ. mo_ISUIVI ) THEN
CDEBUG       WRITE(mo_DEBUG,*) 'dans MOWVAR xmo_SOURXE = ',xmo_SOURXE
CDEBUG       WRITE(mo_DEBUG,*) 
CDEBUG     &      'dans MOWVAR xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M = ',
CDEBUG     &                   xmo_XE_DIFFUSION_M + xmo_XE_CLIM_M
CDEBUG       ENDIF
C
*
        ENDIF 
*       :::::
*
        ENDIF 
*       ++++++
      ENDIF
*     *****
**********************************************************************
* CAS ETAT 'RESTRUCTURE'
**********************************************************************
      IF (mo_IET .EQ. 3) THEN
*     ************************
**********************************************************************
* Impressions d�taill�es
**********************************************************************
*
          CALL MOPARL3(xmo_VG1,xmo_VD1,
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM)
*
* Calcul des termes des d�riv�es des variables des zones resructur�es
* 
*
          CALL MOTRM3(
     &      xmo_CXEHOM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_RHOD,
     &
     &      xmo_XEHOM_CREATION,xmo_XEHOM_DIFFUSION,
     &      xmo_XEHOM_CLIM3,
     &      
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ,
     &      
     &      xmo_CXEHOMM,xmo_OUTSE_RIM,xmo_OUTEJEC_RIM )
*
          CALL MOMOYE (xmo_XEHOM_CREATION, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_CREATION_M)
          CALL MOMOYE (xmo_XEHOM_DIFFUSION, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_DIFFUSION_M)
          CALL MOMOYE (xmo_XEHOM_CLIM3, xmo_TVSEQR,
     &                mo_NSEQR, mo_NNSPHMX, xmo_XEHOM_CLIM3_M)
*
***********************************************************************
* Calcul des taux de gaz �chang�s (mol/s/m3)
**********************************************************************
*
        xmo_TAUXBETAC(1) = xmo_BETAC
        xmo_TAUXOUTSE(1) = xmo_OUTSE_RIM
        xmo_TAUXOUTPERC(1) = 0.D0
        xmo_TAUXOUTEJEC(1) = xmo_OUTEJEC_RIM
*
**********************************************************************
* Calcul des quantit�s de gaz et des les bilans
**********************************************************************
        CALL MOCABI(mo_IET, mo_PREMIERSOUSPAS, DTDECO,
     &                  0.D0 ,0.D0 ,0.D0 ,
     &                  0.D0 ,0.D0 ,0.D0 ,
     &                  xmo_CXEHOMM, xmo_CXEBRIM, xmo_CXEPORRIM)
*
**********************************************************************
* Calcul des pressions internes et des pressions d'�quilibre  : etat 3
**********************************************************************
*
* bulles de rim
        CALL MOCOPR (xmo_CXEBRIM, xmo_VTBRIM, xmo_RBRIM, 6 ,
     &               xmo_PINT_BRIM, xmo_PEQ_BRIM)
*
* pores de rim
        CALL MOCOPR (xmo_CXEPORRIM, xmo_VTPORRIM, xmo_RPORRIM, 7,
     &               xmo_PINT_PORRIM, xmo_PEQ_PORRIM)
**********************************************************************
*
        IF ( IIMPR .EQ. 1 ) THEN
*       +++++++++++++++++++++++++
*
        IF ( mo_IMPR4 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* Ecriture dans le fichier resultat4 (analyse des etats (2) et (3))
*
            WRITE(mo_NRES4,8700) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
*
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
*
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,
*
     &      0.D0,
     &      xmo_XEHOM_CREATION_M,
     &      xmo_XEHOM_DIFFUSION_M,
     &      xmo_XEHOM_CLIM3_M,
     &      0.D0,
     &      0.D0,
     &      xmo_XEBRIM_ALIM_DIRECTE,
     &      xmo_XEBRIM_ALIM_ECHAP,
     &      xmo_XEBRIM_REMBRIM,
     &      0.D0,
     &      xmo_VTBRIM_LACUNE,
     &      xmo_VTBRIM_GAZ,
     &      0.D0,
     &      0.D0,
     &      xmo_XEPORRIM_ALIM_DIRECTE,
     &      xmo_XEPORRIM_ALIM_ECHAP,
     &      xmo_XEPORRIM_REMPORRIM,
     &      0.D0,
     &      xmo_VTPORRIM_LACUNE,
     &      xmo_VTPORRIM_GAZ
*
        ENDIF
*       :::::
*
        IF ( mo_IMPRSYNT .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
* ecriture dans le fichier de synthese
*
            WRITE(mo_NSYNT,9000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      0.D0,
     &      0.D0,0.D0 ,0.D0 ,0.D0 ,
     &      0.D0,0.D0 ,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,
     &      0.D0,
     &      0.D0,0.D0 ,0.D0 ,0.D0 ,
     &      0.D0,0.D0 ,
     &      0.D0,0.D0,0.D0,0.D0,
     &      0.D0,0.D0,
     &      xmo_CXEHOMM,
     &      xmo_CBRIM,xmo_CXEBRIM,xmo_VTBRIM,xmo_RBRIM,
     &      xmo_PINT_BRIM, xmo_PEQ_BRIM,
     &      xmo_CPORRIM,xmo_CXEPORRIM,xmo_VTPORRIM,xmo_RPORRIM,
     &      xmo_PINT_PORRIM, xmo_PEQ_PORRIM,
     &      xmo_GCREE,xmo_SORDIFJ,xmo_SORPERC,xmo_SOREJEC,
     &      xmo_ERRGLOB
*
        ENDIF
*       :::::
*
*
        IF ( mo_IMPR5 .EQ. 1 ) THEN
*       ::::::::::::::::::::::::
*  Pour v�rification bilans terme a terme
*
*
            ERRDR = xmo_XEHOM_DIFFUSION_M + xmo_XEHOM_CLIM3_M 
     &               + xmo_OUTSE_RIM
     &               + xmo_OUTEJEC_RIM 
     &               + xmo_XEBRIM_ALIM_DIRECTE
     &               + xmo_XEBRIM_ALIM_ECHAP
     &               + xmo_XEBRIM_REMBRIM
     &               + xmo_XEPORRIM_ALIM_DIRECTE
     &               + xmo_XEPORRIM_ALIM_ECHAP
     &               + xmo_XEPORRIM_REMPORRIM
*
            ERRTOT = ERRDR

            WRITE(mo_NRES5,6000) xmo_TEMPS, mo_IET, mo_IET_APPR,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      0.D0,0.D0,
     &      ERRDR, xmo_XEHOM_DIFFUSION_M + xmo_XEHOM_CLIM3_M,
     &      ERRTOT, 
     &      xmo_BETAC
*
*
*
        ENDIF
*       :::::
*
        ENDIF 
*       ++++++
      ENDIF
*     *****
*
**********************************************************************
* CAS ETAT TUNNEL supprime
**********************************************************************
CDEBUG
CDEBUG             mo_FLAG = 0
CDEBUG
*
C
*
* format pour ecriture dans le fichier de verification des bilans terme a terme
 6000 FORMAT (1X,E17.8,2I17,57E17.8)
* 
* format pour ecriture dans fichier resultat1
 7000 FORMAT (1X,E20.8,I20,I20,36(E20.8),20(E22.8),11(E22.8))
*
* format pour ecriture dans fichier resultat2 
 8000 FORMAT (1X,E20.8,I20,I20,70(E20.8))
*
* format pour ecriture dans fichier resultat3 
 8500 FORMAT (1X,E20.8,I20,I20,65(E20.8))
*
* format pour ecriture dans fichier resultat4 
 8700 FORMAT (1X,E30.8,I30,I30,127(E30.8))
*
* format pour ecriture dans fichier synthese
 9000 FORMAT (1X,E26.8,I26,I26,45(E26.8)) 
*
*
      RETURN
      END
      subroutine prja (neq, y, yh, nyh, ewt, ftem, savf, wm, iwm,
     1   f, jac)
clll. optimize
      external f, jac
      integer neq, nyh, iwm
      integer iownd, iowns,
     1   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     2   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer iownd2, iowns2, jtyp, mused, mxordn, mxords
      integer i, i1, i2, ier, ii, j, j1, jj, lenp,
     1   mba, mband, meb1, meband, ml, ml3, mu, np1
      double precision y, yh, ewt, ftem, savf, wm
      double precision rowns,
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision rownd2, rowns2, pdnorm
      double precision con, fac, hl0, r, r0, srur, yi, yj, yjj,
     1   vmnorm, fnorm, bnorm
      dimension neq(1), y(*), yh(nyh,*), ewt(*), ftem(*), savf(*),
     1   wm(*), iwm(*)
      common /ls0001/ rowns(209),
     2   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround,
     3   iownd(14), iowns(6),
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      common /lsa001/ rownd2, rowns2(20), pdnorm,
     1   iownd2(3), iowns2(2), jtyp, mused, mxordn, mxords
c-----------------------------------------------------------------------
c prja is called by stoda to compute and process the matrix
c p = i - h*el(1)*j , where j is an approximation to the jacobian.
c here j is computed by the user-supplied routine jac if
c miter = 1 or 4 or by finite differencing if miter = 2 or 5.
c j, scaled by -h*el(1), is stored in wm.  then the norm of j (the
c matrix norm consistent with the weighted max-norm on vectors given
c by vmnorm) is computed, and j is overwritten by p.  p is then
c subjected to lu decomposition in preparation for later solution
c of linear systems with p as coefficient matrix. this is done
c by dgefa if miter = 1 or 2, and by dgbfa if miter = 4 or 5.
c
c in addition to variables described previously, communication
c with prja uses the following..
c y     = array containing predicted values on entry.
c ftem  = work array of length n (acor in stoda).
c savf  = array containing f evaluated at predicted y.
c wm    = real work space for matrices.  on output it contains the
c         lu decomposition of p.
c         storage of matrix elements starts at wm(3).
c         wm also contains the following matrix-related data..
c         wm(1) = sqrt(uround), used in numerical jacobian increments.
c iwm   = integer work space containing pivot information, starting at
c         iwm(21).   iwm also contains the band parameters
c         ml = iwm(1) and mu = iwm(2) if miter is 4 or 5.
c el0   = el(1) (input).
c pdnorm= norm of jacobian matrix. (output).
c ierpj = output error flag,  = 0 if no trouble, .gt. 0 if
c         p matrix found to be singular.
c jcur  = output flag = 1 to indicate that the jacobian matrix
c         (or approximation) is now current.
c this routine also uses the common variables el0, h, tn, uround,
c miter, n, nfe, and nje.
c-----------------------------------------------------------------------
      nje = nje + 1
      ierpj = 0
      jcur = 1
      hl0 = h*el0
      go to (100, 200, 300, 400, 500), miter
c if miter = 1, call jac and multiply by scalar. -----------------------
 100  lenp = n*n
      do 110 i = 1,lenp
 110    wm(i+2) = 0.0d0
      call jac (neq, tn, y, 0, 0, wm(3), n)
      con = -hl0
      do 120 i = 1,lenp
 120    wm(i+2) = wm(i+2)*con
      go to 240
c if miter = 2, make n calls to f to approximate j. --------------------
 200  fac = vmnorm (n, savf, ewt)
      r0 = 1000.0d0*dabs(h)*uround*dfloat(n)*fac
      if (r0 .eq. 0.0d0) r0 = 1.0d0
      srur = wm(1)
      j1 = 2
      do 230 j = 1,n
        yj = y(j)
        r = dmax1(srur*dabs(yj),r0/ewt(j))
        y(j) = y(j) + r
        fac = -hl0/r
        call f (neq, tn, y, ftem)
        do 220 i = 1,n
 220      wm(i+j1) = (ftem(i) - savf(i))*fac
        y(j) = yj
        j1 = j1 + n
 230    continue
      nfe = nfe + n
 240  continue
c compute norm of jacobian. --------------------------------------------
      pdnorm = fnorm (n, wm(3), ewt)/dabs(hl0)
c add identity matrix. -------------------------------------------------
      np1 = n + 1
      j = 3
      do 250 i = 1,n
        wm(j) = wm(j) + 1.0d0
 250    j = j + np1
c do lu decomposition on p. --------------------------------------------
      call dgefa (wm(3), n, n, iwm(21), ier)
      if (ier .ne. 0) ierpj = 1
      call printmat(wm(3),n,ml,mu)
      return
c dummy block only, since miter is never 3 in this routine. ------------
 300  return
c if miter = 4, call jac and multiply by scalar. -----------------------
 400  ml = iwm(1)
      mu = iwm(2)
      ml3 = ml + 3
      mband = ml + mu + 1
      meband = mband + ml
      lenp = meband*n
      do 410 i = 1,lenp
 410    wm(i+2) = 0.0d0
      call jac (neq, tn, y, ml, mu, wm(ml3), meband)
      con = -hl0
      do 420 i = 1,lenp
 420    wm(i+2) = wm(i+2)*con
      call printmat(wm(3),n,ml,mu)
      go to 570
c if miter = 5, make mband calls to f to approximate j. ----------------
 500  ml = iwm(1)
      mu = iwm(2)
      mband = ml + mu + 1
      mba = min0(mband,n)
      meband = mband + ml
      meb1 = meband - 1
      srur = wm(1)
      fac = vmnorm (n, savf, ewt)
      r0 = 1000.0d0*dabs(h)*uround*dfloat(n)*fac
      if (r0 .eq. 0.0d0) r0 = 1.0d0
      do 560 j = 1,mba
        do 530 i = j,n,mband
          yi = y(i)
          r = dmax1(srur*dabs(yi),r0/ewt(i))
 530      y(i) = y(i) + r
        call f (neq, tn, y, ftem)
        do 550 jj = j,n,mband
          y(jj) = yh(jj,1)
          yjj = y(jj)
          r = dmax1(srur*dabs(yjj),r0/ewt(jj))
          fac = -hl0/r
          i1 = max0(jj-mu,1)
          i2 = min0(jj+ml,n)
          ii = jj*meb1 - ml + 2
          do 540 i = i1,i2
 540        wm(ii+i) = (ftem(i) - savf(i))*fac
 550      continue
 560    continue
      nfe = nfe + mba
 570  continue
c      write(28,*) (y(i),i=1,n)
      call printmat(wm(3),n,ml,mu)
c      stop
c compute norm of jacobian. --------------------------------------------
      pdnorm = bnorm (n, wm(3), meband, ml, mu, ewt)/dabs(hl0)
c add identity matrix. -------------------------------------------------
      ii = mband + 2
      do 580 i = 1,n
        wm(ii) = wm(ii) + 1.0d0
 580    ii = ii + meband
c do lu decomposition of p. --------------------------------------------
      call dgbfa (wm(3), meband, n, ml, mu, iwm(21), ier)
      if (ier .ne. 0) ierpj = 1
      return
c----------------------- end of subroutine prja ------------------------
      end

c Impression
      subroutine printmat(w,n,ml,mu)
      double precision w(*)
      integer n,ml,mu
      integer prem
      save prem
      data prem/0/

      mband = ml + mu + 1
      mba = min0(mband,n)
      meband = mband + ml
      meb1 = meband - 1
c      do ii=1,n
c          j1 = max0(ii-ml,1)
c          j2 = min0(ii+mu,n)
c          iii=ii-ml
c          write(28,*) ii,j1,j2,(w(iii+j*meb1),j=j1,j2)
c      end do

      if(int(prem/1000)*1000.EQ.prem) then
         print *,'pas no ',prem,'taille jac=',n,'pas no ',prem
         do i=1,n
            print *,(dble(w(i+j*n)),j=1,n),'ligne',i
         end do
      endif

      prem=prem+1
c      if(prem.eq.2) then
c        stop
c      endif
      end
C       *********************************
C       *         @(#)reddec.f	1.1     7/27/98           *
C       *********************************
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D D E C     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D D E C     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D D E C     ++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*
      SUBROUTINE REDDEC(TEXT,NTEXT,DFLOT,NFIXE,ITYPE,IDEB,IFIN)
*     =================
C&T
C&T         SOUS-PROGRAMME REDDEC
C&T         ---------------------
C&T
C&F
C&F         FONCTION :
C&F            CE SOUS-PROGRAMME DECODE UNE CHAINE DE CARACTERES :
C&F            .ITYPE=4 : C'EST UN TEXTE
C&F            .ITYPE=3 : C'EST UN MOT
C&F            .ITYPE=2 : C'EST UN REEL CONVERTI DANS DFLOT EN DOUBLE
C&F            PRECISION
C&F            .ITYPE=1 : C'EST UN ENTIER CONVERTI DANS NFIXE
C&F            .ITYPE=0 : TEXT EST REMPLI DE BLANCS
C&F            .ITYPE=-1: CHAINE INCOMPREHENSIBLE
C&F            .ITYPE=-2: LE NOMBRE LU EST INCORRECT
C&F            .ITYPE=-3: MOT INCORRECT
C&F            .ITYPE=-4: IL MANQUE UN QUOTA A LA CHAINE
C&F
C&A
C&A         ARGUMENTS :
C&A            .TEXT  : CONTIENT LA CHAINE DE CARACTERES
C&A            .NTEXT : DIMENSION DE TEXT
C&A            .DFLOT : CONTIENDRA EVENTUELLEMENT UN REEL DOUBLE
C&A            .NFIXE : CONTIENDRA EVENTUELLEMENT UN ENTIER
C&A            .ITYPE : TYPE DE VARIABLE LUE
C&A            .IDEB  : RANG DU PREMIER CARACTERE
C&A            .IFIN  : RANG DU DERNIER CARACTERE
C&A
C&E
C&E         VARIABLES D'ENTREE :
C&E            .TEXT
C&E            .NTEXT
C&E
C&S
C&S         VARIABLES DE SORTIE
C&S            .DFLOT
C&S            .NFIXE
C&S            .ITYPE
C&S            .IDEB
C&S            .IFIN
C&S
C&I
C&I         VARIABLES INTERNES :
C&I            .NUM   : TABLEAU CONTENANT LES CARACTERES NUMERIQUES
C&I            .ALPH  : TABLEAU CONTENANT LES CARACTERES 'ALPHA'
C&I            .T     : CHAINE CONTENANT LE NOMBRE LU MAIS ECRIT EN
C&I            FORMAT STANDARD
C&I            .FMT   : CHAINE CONTENANT LE FORMAT DE LECTURE DE T
C&I            .PAIR  : VARIABLE LOGIQUE SERVANT AU CONTROLE DE PARITE
C&I            DES QUOTAS DANS UN TEXTE
C&I            .INUM  : DIMENSION DE NUM
C&I            .IALPH : DIMENSION DE ALPH
C&I            .IPOIN : EMPLACEMENT DU POINT DANS LA CHAINE T
C&I            .IEXPO : EMPLACEMENT DE L'EXPOSANT DANS LA CHAINE T
C&I            .ISIGN : EMPLACEMENT DU SIGNE DANS LA CHAINE T (DEUXIEME
C&I            SIGNE)
C&I            .IP    : COMPTEUR DE POINT
C&I            .IE    : COMPTEUR D'EXPOSANT
C&I            .IS    : COMPTEUR DE SIGNE(LE PREMIER SIGNE N'ETANT PAS
C&I            COMPTABILISE)
C&I            .NCAR  : NOMBRE DE CARACTERES DE T
C&I            .IC    : VARIABLE AUXILIAIRE PERMETTANT DE DECOUVRIR
C&I            UN CARACTERE INVALIDE DANS UN NOMBRE
C&I            .K1    : VARIABLE AUXILIAIRE SERVANT , SOIT AU
C&I            REMPLISSAGE DE T , SOIT A CELUI DE FMT
C&I            .K2    : VARIABLE AUXILIAIRE SERVANT AU REMPLISSAGE
C&I            DE FMT
C&I            .I,J OU L : INDICES DE BOUCLES
C&I            .I1    : SOUS-INDICE (I+1 PAR EXEMPLE)
C&I
      DIMENSION TEXT(*),NUM(13),ALPH(63)
      CHARACTER*1 TEXT,NUM,ALPH,CAR
      CHARACTER*50 T
      CHARACTER*10 FMT
      DOUBLE PRECISION DFLOT
      LOGICAL PAIR
C
C           INITIALISATION
C
      DATA NUM/'1','2','3','4','5','6','7','8','9','0','.','+','-'/
      DATA ALPH/'A','B','C','D','E','F','G','H','I','J','K','L','M',
     *          'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
     *          'a','b','c','d','e','f','g','h','i','j','k','l','m',
     *          'n','o','p','q','r','s','t','u','v','w','x','y','z',
     *          '$','&','#',']','[','%','_','*','<','>','/'/
      T=' '
      FMT=' '
      INUM=13
      IALPH=63
      IPOIN=0
      ISIGN=0
      IEXPO=0
      IP=0
      IE=0
      IS=0
      DFLOT=0.
      NFIXE=0
      ITYPE=0
      IDEB=0
      IFIN=NTEXT
C
C           DETECTION D'ERREURS DE DONNEES
C
      IF(NTEXT.LE.0) RETURN
      NCAR=0
      DO 20 I=1,NTEXT
         IF(TEXT(I).NE.' ') NCAR=NCAR+1
  20  CONTINUE
      IF(NCAR.EQ.0) RETURN
      ITYPE=-1
C
C           CALCUL DE IDEB
C
      DO 30 I=1,NTEXT
         IF(TEXT(I).NE.' ') THEN
            IDEB=I
            GO TO 31
         ENDIF
  30  CONTINUE
  31  CONTINUE
C
C           CAS D'UN TEXTE
C
      IF(TEXT(IDEB).EQ.'''') THEN
         ITYPE=-4
         PAIR=.TRUE.
         DO 35 I=IDEB,NTEXT
            IF(TEXT(I).EQ.'''') THEN
               PAIR=.NOT.PAIR
               IF(PAIR) THEN
                  IF(I.EQ.NTEXT) THEN
                     ITYPE=4
                     IFIN=I
                     RETURN
                  ELSE
                     I1=I+1
                     IF(TEXT(I1).EQ.' ') THEN
                        ITYPE=4
                        IFIN=I
                        RETURN
                     ENDIF
                  ENDIF
               ENDIF
            ENDIF
  35     CONTINUE
         RETURN
      ENDIF
C
C           CALCUL DE IFIN POUR UNE VARIABLE AUTRE QU'UN TEXTE
C
      DO 40 I=IDEB,NTEXT
         IF(TEXT(I).EQ.' ') THEN
            IFIN=I-1
            GO TO 41
         ENDIF
  40  CONTINUE
  41  CONTINUE
      NCAR=IFIN-IDEB+1
C
C           CAS D'UN MOT
C
      DO 50 I=1,IALPH
         IF(TEXT(IDEB).EQ.ALPH(I)) THEN
            ITYPE=-3
            IC=0
            DO 49 J=IDEB,IFIN
               DO 47 L=1,IALPH
                  IF(TEXT(J).EQ.ALPH(L)) THEN
                     IC=IC+1
                     GO TO 49
                  ENDIF
  47           CONTINUE
               DO 48 L=1,INUM
                  IF(TEXT(J).EQ.NUM(L)) THEN
                     IC=IC+1
                     GO TO 49
                  ENDIF
  48           CONTINUE
  49        CONTINUE
            IF(IC.EQ.NCAR) ITYPE=3
            RETURN
         ENDIF
  50  CONTINUE
C
C           CAS D'UN NOMBRE (OU D'UNE CHAINE INCORRECTE)
C
      DO 60 I=1,INUM
         IF(TEXT(IDEB).EQ.NUM(I)) THEN
            ITYPE=-2
            GO TO 61
         ENDIF
  60  CONTINUE
  61  CONTINUE
      IF(ITYPE.EQ.-1) RETURN
      IF(NCAR.GT.46) RETURN
C
C           RECHERCHE DU SIGNE INITIAL
C
      IF(TEXT(IDEB).EQ.'+'.OR.TEXT(IDEB).EQ.'-') THEN
         DO 70 K=1,NCAR
            K1=IDEB+K-1
            T(K:K)=TEXT(K1)
  70     CONTINUE
      ELSE
         NCAR=NCAR+1
         T(1:1)='+'
         DO 80 K=2,NCAR
            K1=IDEB+K-2
            T(K:K)=TEXT(K1)
  80     CONTINUE
      ENDIF
C
C           DETECTION D'ERREUR : LA CHAINE N'EST COMPOSEE QUE D'UN SIGNE
C
      IF(NCAR.EQ.1) RETURN
C
C           RECHERCHE D'UN POINT , D'UN SIGNE OU D'UN EXPOSANT
C
      DO 100 I=2,NCAR
         IF(T(I:I).EQ.'.') THEN
            IPOIN=I
            IP=IP+1
         ELSE IF(T(I:I).EQ.'+'.OR.T(I:I).EQ.'-') THEN
            ISIGN=I
            IS=IS+1
         ELSE IF(T(I:I).EQ.'E'.OR.T(I:I).EQ.'e'
     +         .OR.T(I:I).EQ.'D'.OR.T(I:I).EQ.'d'
     +         .OR.T(I:I).EQ.'Q') THEN
            IEXPO=I
            IE=IE+1
         ELSE
            IC=0
            DO 90 J=1,10
               IF(T(I:I).EQ.NUM(J)) THEN
                  IC=1
                  GO TO 91
               ENDIF
  90        CONTINUE
  91        CONTINUE
            IF(IC.EQ.0) RETURN
         ENDIF
 100  CONTINUE
C
C            DETECTION DES ERREURS DE POINTS,D'EXPOSANTS OU DE SIGNES
C
      IF(IP.GT.1.OR.IS.GT.1.OR.IE.GT.1) RETURN
      IF(IE.EQ.1) THEN
         IF(IPOIN.GT.IEXPO) RETURN
         IEXPO1=IEXPO+1
         IF(IS.EQ.1.AND.ISIGN.NE.IEXPO1) RETURN
      ELSE
         IF(IS.EQ.1) RETURN
      ENDIF
C
C           CONSTRUCTION DE T
C
      IF(IE.EQ.0.AND.IP.EQ.0) THEN
         ITYPE=1
      ELSE
         ITYPE=2
         IF(IE.EQ.0) THEN
            NCAR=NCAR+1
            IEXPO=NCAR
         ENDIF
         IF(IP.EQ.0) THEN
            IPOIN=IEXPO
            IEXPO=IPOIN+1
            DO 110 I=NCAR,IPOIN,-1
               CAR=T(I:I)
               I1=I+1
               T(I1:I1)=CAR
 110        CONTINUE
            NCAR=NCAR+1
            T(IPOIN:IPOIN)='.'
            T(IEXPO:IEXPO)='D'
         ENDIF
         IF(IS.EQ.0) THEN
            ISIGN=IEXPO+1
            DO 120 I=NCAR,ISIGN,-1
               CAR=T(I:I)
               I1=I+1
               T(I1:I1)=CAR
 120        CONTINUE
            NCAR=NCAR+1
            T(ISIGN:ISIGN)='+'
         ENDIF
         K1=NCAR-ISIGN
         IF(K1.GT.2) THEN
            ITYPE=-2
            RETURN
         ELSE IF(K1.EQ.1) THEN
            NCAR=NCAR+1
            K1=NCAR-1
            CAR=T(K1:K1)
            T(NCAR:NCAR)=CAR
            T(K1:K1)='0'
         ELSE IF(K1.EQ.0) THEN
            NCAR=NCAR+2
            K1=NCAR-1
            T(NCAR:NCAR)='0'
            T(K1:K1)='0'
         ENDIF
      ENDIF
C
C           CONSTRUCTION DU FORMAT DE LECTURE
C
      FMT=' (      ) '
      K1=NCAR/10
      K2=MOD(NCAR,10)
      IF(K1.NE.0) FMT(4:4)=NUM(K1)
      IF(K2.EQ.0) K2=10
      FMT(5:5)=NUM(K2)
C
      IF(ITYPE.EQ.2) THEN
         FMT(6:6)='.'
         FMT(3:3)='D'
         K1=IEXPO-IPOIN-1
         K2=MOD(K1,10)
         K1=K1/10
         IF(K1.NE.0) FMT(7:7)=NUM(K1)
         IF(K2.EQ.0) K2=10
         FMT(8:8)=NUM(K2)
         READ(T,FMT,ERR=999) DFLOT
      ELSE
         FMT(3:3)='I'
         READ(T,FMT,ERR=999) NFIXE
         DFLOT=NFIXE
      ENDIF
C
      RETURN
C
C           ERREUR EN LECTURE (NOMBRE TROP GRAND)
C
 999  CONTINUE
      ITYPE=-2
      RETURN
      END
C       *********************************
C       *         @(#)rederr.f	1.1     7/27/98           *
C       *********************************
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D E R R     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D E R R     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D E R R     ++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*
      SUBROUTINE REDERR(IER)
*     =================
C&T
C
*     ******************************************************************
*     *                                                                *
*     *                Subroutine    R E D E R R                       *
*     *                                                                *
*     *           DELIVRE LES MESSAGES CORRESPONDANTS                  *
*     *           AUX ERREURS DETECTEES DANS REDLEC .                  *
*     *                                                                *
*     *                                                                *
*     ******************************************************************
C&A
C&A         ARGUMENTS:
C&A         ----------
C&A            - IER : NUMERO DE L'ERREUR DETECTEE
C&A
C&C
C
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'      
      INCLUDE 'credver.inc'  
C
      CHARACTER*20 TYPE(4)
C
      WRITE(NEX,*) '    L''EXPRESSION LUE : ',TEXT(1:12)
      WRITE(NEX,*) '    -------------------'
      WRITE(NEX,*) '    CETTE EXPRESSION N''ETAIT PAS ATTENDUE'
      WRITE(NEX,*)
      WRITE(NEX,*) '    EXPLICATION PLUS PRECISE :'
      WRITE(NEX,*) '    --------------------------'
C
      IF(IER.EQ.10) THEN
         WRITE(NEX,10)
      ELSE IF(IER.EQ.20) THEN
         WRITE(NEX,20)
      ELSE IF(IER.EQ.30) THEN
         WRITE(NEX,30)
      ELSE IF(IER.EQ.40) THEN
         WRITE(NEX,40)
      ELSE IF(IER.EQ.50) THEN
         WRITE(NEX,50)
      ELSE IF(IER.EQ.60) THEN
         WRITE(NEX,60)
         WRITE(NEX,*)
         TYPE(1)='ENTIER'
         TYPE(2)='REEL'
         TYPE(3)='MOT CLE'
         TYPE(4)='TEXTE (ENTRE QUOTAS)'
         WRITE(NEX,*) '    LE TYPE DE LA DONNEE LUE EST "',
     1                     TYPE(ITYPE),'"'
         WRITE(NEX,*) '    ON A ATTENDU UNE DONNEE DU TYPE',
     1                     '/DES TYPES :'
         DO 5 I=1,4
           IF (IN(I) .EQ. 1)  THEN
             WRITE(NEX,'(T51,A)') TYPE(I)
           ENDIF
5        CONTINUE
         WRITE(NEX,*)
C
      ELSE IF(IER.EQ.70) THEN
         WRITE(NEX,70)
         WRITE(NEX,75)
      ELSE IF(IER.EQ.80) THEN
         WRITE(NEX,80)
         WRITE(NEX,75)
      ENDIF
C
      STOP
C
C           FORMATS
C
  10  FORMAT('     L''ARGUMENT EST INCORRECT ')
  20  FORMAT('     IL MANQUE UN QUOTA DANS LE TEXTE')
  30  FORMAT('     LE MOT EST INCORRECT ')
  40  FORMAT('     LE NOMBRE EST INCORRECT ')
  50  FORMAT('     LA CHAINE DE CARACTERES EST INCOMPREHENSIBLE')
  60  FORMAT('     LE TYPE DE DONNEE LUE NE CORRESPOND PAS A ',
     *             'CELUI DEMANDE :')
  70  FORMAT('     L''UNITE D''ENTREE EST INCORRECTE OU ON EST')
  75  FORMAT('     ARRIVE A LA FIN DU FICHIER')
  80  FORMAT('     L''UNITE DE SORTIE EST INCORRECTE OU ON EST')
C
      END
C       *********************************
C       *         @(#)redlec.f	1.1     7/27/98           *
C       *********************************
*
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D L E C     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D L E C     ++++++++++++++++++++
* +++++++++++++++      SUBROUTINE   R E D L E C     ++++++++++++++++++++
* +++++++++++++++                                   ++++++++++++++++++++
* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*
      SUBROUTINE REDLEC(N)
*     =================
C&T
C
C
*     ******************************************************************
*     *                                                                *
*     *                Subroutine    R E D L E C                       *
*     *                                                                *
*     *             PILOTE LA LECTURE D'UNE DONNEE DEFINIE PAR N       *
*     *                                                                *
*     ******************************************************************
C&F             - N=8 : ON VEUT LIRE UN TEXTE
C&F             - N=4 : ON VEUT LIRE UN MOT
C&F             - N=2 : ON VEUT LIRE UN REEL(CONVERTI DANS DFLOT EN DOUBLE
C&F                     PRECISION)
C&F             - N=1 : ON VEUT LIRE UN ENTIER(CONVERTI DANS NFIXE)
C&F
C&F            TOUTE COMBINAISON EST POSSIBLE : N=4+2+1=7 SIGNIFIE DONC
C&F            QU'IL FAUDRA LIRE , SOIT UN MOT , SOIT UN REEL , SOIT UN
C&F            ENTIER .
C&F            EN CAS D'ERREURS , DES MESSAGES SERONT ENVOYES ET UN
C&F            COMPTEUR SERA INCREMENTE .
C&F
C
      INCLUDE 'credcar.inc' 
      INCLUDE 'crednbr.inc'   
      INCLUDE 'credtxt.inc'      
      INCLUDE 'credver.inc'  
C
      CHARACTER*1  CAR
C
C  --> INITIALISATION
C
      IF(NIN.LE.0.OR.NIN.GE.100) NIN=5
      IF(NEX.EQ.NIN)  THEN
        WRITE(NEX,*) 
        WRITE(NEX,*) 'ERREUR GENERALE :'
        WRITE(NEX,*) '-----------------'
        WRITE(NEX,*)
        WRITE(NEX,*) '    LE NUMERO DE L''UNITE DE SORTIE EST LE MEME'
        WRITE(NEX,*) '    QUE CELUI DE L''UNITE D''ENTREE !'
        WRITE(NEX,'(A,I3,A)') '    (NEX=NIN=',NIN,')'
        STOP
      ENDIF
      IF(NECHO.GE.100.OR.NECHO.EQ.0) NECHO=6
      IF(NECHO.EQ.NIN)  THEN
        NECHO=NECHO+1
        WRITE(NEX,*)
        WRITE(NEX,*) 'MENTION GENERALE :'
        WRITE(NEX,*) '------------------'
        WRITE(NEX,*)
        WRITE(NEX,*) '    LE NUMERO DE L''UNITE D''ENTREE'
        WRITE(NEX,*) '    EST LE MEME QUE CELUI DE L''UNITE DE'
        WRITE(NEX,*) '    SORTIE DE L''"ECHO" DES DONNEES DE REDLEC'
        WRITE(NEX,'(A,I3,A)') '    (NECHO=NIN=',NIN,') !'
        WRITE(NEX,*) '    POUR CETTE RAISON ON MET L''UNITE DE L''EC',
     1                    'HO'
        WRITE(NEX,'(A,A,I3,A)') '    EGALE A L''UNITE DE L''ECHO + 1',
     1                    '(--> NECHO=',NECHO,')'
        WRITE(NEX,*)
      ENDIF
C
      IF(N.LE.0.OR.N.GE.16) THEN
        IER=10
        CALL REDERR(IER)
        NBERR=NBERR+1
        RETURN
      ENDIF
C
      NC=132
      TEXT=' '
      DFLOT=0.
      NFIXE=0
      ITYPE=0
      NCAR=0
C
C  --> LECTURE D'UNE LIGNE
C
      IF(LIGNE.EQ.TEXT) THEN
20      READ(UNIT=NIN,FMT=100,END=91) LIGNE
        DO 30 I =1,NC
          IF(LIGNE(I:I) .EQ. CHAR(9)) LIGNE(I:I) = ' '
 30     CONTINUE
        IF(NECHO.GE.0) WRITE(UNIT=NECHO,FMT=110) LIGNE
        CAR=LIGNE(1:1)
        IF(CAR.EQ.'$'.OR.LIGNE.EQ.' ') GO TO 20
      ENDIF
C
C  --> DECODAGE DE LA PREMIERE VARIABLE DE LIGNE
C
      CALL REDDEC(LIGNE,NC,DFLOT,NFIXE,ITYPE,IDEB,IFIN)
C
      NCAR=IFIN-IDEB+1
      TEXT(1:NCAR)=LIGNE(IDEB:IFIN)
      LIGNE(IDEB:IFIN)=' '
C
      IF(ITYPE.LE.0) THEN
        NBERR=NBERR+1
        IF(ITYPE.EQ.-4) THEN
          IER=20
        ELSE IF(ITYPE.EQ.-3) THEN
          IER=30
        ELSE IF(ITYPE.EQ.-2) THEN
          IER=40
        ELSE IF(ITYPE.EQ.-1) THEN
          IER=50
        ENDIF
        CALL REDERR(IER)
        RETURN
      ENDIF
C
C  --> RECHERCHE DU TYPE DE VARIABLE DEMANDE
C
      N1=N
      IN(1)=MOD(N1,2)
      N1=N1/2
      IN(2)=MOD(N1,2)
      N1=N1/2
      IN(3)=MOD(N1,2)
      N1=N1/2
      IN(4)=MOD(N1,2)
C
C  --> COMPARAISON AVEC LE TYPE TROUVE
C
      IF (IN(2) .EQ. 1 .AND. ITYPE .EQ. 1)  THEN
        ITYPE=2
        GOTO 50
      ENDIF
C
      IF(IN(ITYPE).NE.1) THEN
        IER=60
        CALL REDERR(IER)
        NBERR=NBERR+1
      ENDIF
C
C  --> FIN NORMALE DE REDLEC
C
50    RETURN
C
C  --> ERREURS EN LECTURE OU ECRITURE
C
91    CONTINUE
      IER=70
      CALL REDERR(IER)
      ITYPE=0
      RETURN
C
C  --> FORMATS
C
100   FORMAT(A132)
110   FORMAT(A132)
C
      END
      subroutine solsy (wm, iwm, x, tem)
clll. optimize
      integer iwm
      integer iownd, iowns,
     1   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     2   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer i, meband, ml, mu
      double precision wm, x, tem
      double precision rowns,
     1   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision di, hl0, phl0, r
      dimension wm(*), iwm(*), x(1), tem(1)
      common /ls0001/ rowns(209),
     2   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround,
     3   iownd(14), iowns(6),
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
c-----------------------------------------------------------------------
c this routine manages the solution of the linear system arising from
c a chord iteration.  it is called if miter .ne. 0.
c if miter is 1 or 2, it calls dgesl to accomplish this.
c if miter = 3 it updates the coefficient h*el0 in the diagonal
c matrix, and then computes the solution.
c if miter is 4 or 5, it calls dgbsl.
c communication with solsy uses the following variables..
c wm    = real work space containing the inverse diagonal matrix if
c         miter = 3 and the lu decomposition of the matrix otherwise.
c         storage of matrix elements starts at wm(3).
c         wm also contains the following matrix-related data..
c         wm(1) = sqrt(uround) (not used here),
c         wm(2) = hl0, the previous value of h*el0, used if miter = 3.
c iwm   = integer work space containing pivot information, starting at
c         iwm(21), if miter is 1, 2, 4, or 5.  iwm also contains band
c         parameters ml = iwm(1) and mu = iwm(2) if miter is 4 or 5.
c x     = the right-hand side vector on input, and the solution vector
c         on output, of length n.
c tem   = vector of work space of length n, not used in this version.
c iersl = output flag (in common).  iersl = 0 if no trouble occurred.
c         iersl = 1 if a singular matrix arose with miter = 3.
c this routine also uses the common variables el0, h, miter, and n.
c-----------------------------------------------------------------------
      iersl = 0
      go to (100, 100, 300, 400, 400), miter
 100  call dgesl (wm(3), n, n, iwm(21), x, 0)
      return
c
 300  phl0 = wm(2)
      hl0 = h*el0
      wm(2) = hl0
      if (hl0 .eq. phl0) go to 330
      r = hl0/phl0
      do 320 i = 1,n
        di = 1.0d0 - r*(1.0d0 - 1.0d0/wm(i+2))
        if (dabs(di) .eq. 0.0d0) go to 390
 320    wm(i+2) = 1.0d0/di
 330  do 340 i = 1,n
 340    x(i) = wm(i+2)*x(i)
      return
 390  iersl = 1
      return
c
 400  ml = iwm(1)
      mu = iwm(2)
      meband = 2*ml + mu + 1
      call dgbsl (wm(3), meband, n, ml, mu, iwm(21), x, 0)
      return
c----------------------- end of subroutine solsy -----------------------
      end
      subroutine stoda (neq, y, yh, nyh, yh1, ewt, savf, acor,
     1   wm, iwm, f, jac, pjac, slvs)
clll. optimize
      external f, jac, pjac, slvs
      integer neq, nyh, iwm
      integer iownd, ialth, ipup, lmax, meo, nqnyh, nslp,
     1   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     2   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      integer iownd2, icount, irflag, jtyp, mused, mxordn, mxords
      integer i, i1, iredo, iret, j, jb, m, ncf, newq
      integer lm1, lm1p1, lm2, lm2p1, nqm1, nqm2
      double precision y, yh, yh1, ewt, savf, acor, wm
      double precision conit, crate, el, elco, hold, rmax, tesco,
     2   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround
      double precision rownd2, pdest, pdlast, ratio, cm1, cm2,
     1   pdnorm
      double precision dcon, ddn, del, delp, dsm, dup, exdn, exsm, exup,
     1   r, rh, rhdn, rhsm, rhup, told, vmnorm
      double precision alpha, dm1, dm2, exm1, exm2, pdh, pnorm, rate,
     1   rh1, rh1it, rh2, rm, sm1
      dimension neq(1), y(*), yh(nyh,*), yh1(*), ewt(*), savf(*),
     1   acor(*), wm(*), iwm(*)
      dimension sm1(12)
      common /ls0001/ conit, crate, el(13), elco(13,12),
     1   hold, rmax, tesco(3,12),
     2   ccmax, el0, h, hmin, hmxi, hu, rc, tn, uround, iownd(14),
     3   ialth, ipup, lmax, meo, nqnyh, nslp,
     4   icf, ierpj, iersl, jcur, jstart, kflag, l, meth, miter,
     5   maxord, maxcor, msbp, mxncf, n, nq, nst, nfe, nje, nqu
      common /lsa001/ rownd2, pdest, pdlast, ratio, cm1(12), cm2(5),
     1   pdnorm,
     2   iownd2(3), icount, irflag, jtyp, mused, mxordn, mxords
      data sm1/0.5d0, 0.575d0, 0.55d0, 0.45d0, 0.35d0, 0.25d0,
     1   0.20d0, 0.15d0, 0.10d0, 0.075d0, 0.050d0, 0.025d0/
c-----------------------------------------------------------------------
c stoda performs one step of the integration of an initial value
c problem for a system of ordinary differential equations.
c note.. stoda is independent of the value of the iteration method
c indicator miter, when this is .ne. 0, and hence is independent
c of the type of chord method used, or the jacobian structure.
c communication with stoda is done with the following variables..
c
c y      = an array of length .ge. n used as the y argument in
c          all calls to f and jac.
c neq    = integer array containing problem size in neq(1), and
c          passed as the neq argument in all calls to f and jac.
c yh     = an nyh by lmax array containing the dependent variables
c          and their approximate scaled derivatives, where
c          lmax = maxord + 1.  yh(i,j+1) contains the approximate
c          j-th derivative of y(i), scaled by h**j/factorial(j)
c          (j = 0,1,...,nq).  on entry for the first step, the first
c          two columns of yh must be set from the initial values.
c nyh    = a constant integer .ge. n, the first dimension of yh.
c yh1    = a one-dimensional array occupying the same space as yh.
c ewt    = an array of length n containing multiplicative weights
c          for local error measurements.  local errors in y(i) are
c          compared to 1.0/ewt(i) in various error tests.
c savf   = an array of working storage, of length n.
c acor   = a work array of length n, used for the accumulated
c          corrections.  on a successful return, acor(i) contains
c          the estimated one-step local error in y(i).
c wm,iwm = real and integer work arrays associated with matrix
c          operations in chord iteration (miter .ne. 0).
c pjac   = name of routine to evaluate and preprocess jacobian matrix
c          and p = i - h*el0*jac, if a chord method is being used.
c          it also returns an estimate of norm(jac) in pdnorm.
c slvs   = name of routine to solve linear system in chord iteration.
c ccmax  = maximum relative change in h*el0 before pjac is called.
c h      = the step size to be attempted on the next step.
c          h is altered by the error control algorithm during the
c          problem.  h can be either positive or negative, but its
c          sign must remain constant throughout the problem.
c hmin   = the minimum absolute value of the step size h to be used.
c hmxi   = inverse of the maximum absolute value of h to be used.
c          hmxi = 0.0 is allowed and corresponds to an infinite hmax.
c          hmin and hmxi may be changed at any time, but will not
c          take effect until the next change of h is considered.
c tn     = the independent variable. tn is updated on each step taken.
c jstart = an integer used for input only, with the following
c          values and meanings..
c               0  perform the first step.
c           .gt.0  take a new step continuing from the last.
c              -1  take the next step with a new value of h,
c                    n, meth, miter, and/or matrix parameters.
c              -2  take the next step with a new value of h,
c                    but with other inputs unchanged.
c          on return, jstart is set to 1 to facilitate continuation.
c kflag  = a completion code with the following meanings..
c               0  the step was succesful.
c              -1  the requested error could not be achieved.
c              -2  corrector convergence could not be achieved.
c              -3  fatal error in pjac or slvs.
c          a return with kflag = -1 or -2 means either
c          abs(h) = hmin or 10 consecutive failures occurred.
c          on a return with kflag negative, the values of tn and
c          the yh array are as of the beginning of the last
c          step, and h is the last step size attempted.
c maxord = the maximum order of integration method to be allowed.
c maxcor = the maximum number of corrector iterations allowed.
c msbp   = maximum number of steps between pjac calls (miter .gt. 0).
c mxncf  = maximum number of convergence failures allowed.
c meth   = current method.
c          meth = 1 means adams method (nonstiff)
c          meth = 2 means bdf method (stiff)
c          meth may be reset by stoda.
c miter  = corrector iteration method.
c          miter = 0 means functional iteration.
c          miter = jt .gt. 0 means a chord iteration corresponding
c          to jacobian type jt.  (the lsoda argument jt is
c          communicated here as jtyp, but is not used in stoda
c          except to load miter following a method switch.)
c          miter may be reset by stoda.
c n      = the number of first-order differential equations.
c-----------------------------------------------------------------------
      kflag = 0
      told = tn
      ncf = 0
      ierpj = 0
      iersl = 0
      jcur = 0
      icf = 0
      delp = 0.0d0
      if (jstart .gt. 0) go to 200
      if (jstart .eq. -1) go to 100
      if (jstart .eq. -2) go to 160
c-----------------------------------------------------------------------
c on the first call, the order is set to 1, and other variables are
c initialized.  rmax is the maximum ratio by which h can be increased
c in a single step.  it is initially 1.e4 to compensate for the small
c initial h, but then is normally equal to 10.  if a failure
c occurs (in corrector convergence or error test), rmax is set at 2
c for the next increase.
c cfode is called to get the needed coefficients for both methods.
c-----------------------------------------------------------------------
      lmax = maxord + 1
      nq = 1
      l = 2
      ialth = 2
      rmax = 10000.0d0
      rc = 0.0d0
      el0 = 1.0d0
      crate = 0.7d0
      hold = h
      nslp = 0
      ipup = miter
      iret = 3
c initialize switching parameters.  meth = 1 is assumed initially. -----
      icount = 20
      irflag = 0
      pdest = 0.0d0
      pdlast = 0.0d0
      ratio = 5.0d0
      call cfode (2, elco, tesco)
      do 10 i = 1,5
 10     cm2(i) = tesco(2,i)*elco(i+1,i)
      call cfode (1, elco, tesco)
      do 20 i = 1,12
 20     cm1(i) = tesco(2,i)*elco(i+1,i)
      go to 150
c-----------------------------------------------------------------------
c the following block handles preliminaries needed when jstart = -1.
c ipup is set to miter to force a matrix update.
c if an order increase is about to be considered (ialth = 1),
c ialth is reset to 2 to postpone consideration one more step.
c if the caller has changed meth, cfode is called to reset
c the coefficients of the method.
c if h is to be changed, yh must be rescaled.
c if h or meth is being changed, ialth is reset to l = nq + 1
c to prevent further changes in h for that many steps.
c-----------------------------------------------------------------------
 100  ipup = miter
      lmax = maxord + 1
      if (ialth .eq. 1) ialth = 2
      if (meth .eq. mused) go to 160
      call cfode (meth, elco, tesco)
      ialth = l
      iret = 1
c-----------------------------------------------------------------------
c the el vector and related constants are reset
c whenever the order nq is changed, or at the start of the problem.
c-----------------------------------------------------------------------
 150  do 155 i = 1,l
 155    el(i) = elco(i,nq)
      nqnyh = nq*nyh
      rc = rc*el(1)/el0
      el0 = el(1)
      conit = 0.5d0/dfloat(nq+2)
      go to (160, 170, 200), iret
c-----------------------------------------------------------------------
c if h is being changed, the h ratio rh is checked against
c rmax, hmin, and hmxi, and the yh array rescaled.  ialth is set to
c l = nq + 1 to prevent a change of h for that many steps, unless
c forced by a convergence or error test failure.
c-----------------------------------------------------------------------
 160  if (h .eq. hold) go to 200
      rh = h/hold
      h = hold
      iredo = 3
      go to 175
 170  rh = dmax1(rh,hmin/dabs(h))
 175  rh = dmin1(rh,rmax)
      rh = rh/dmax1(1.0d0,dabs(h)*hmxi*rh)
c-----------------------------------------------------------------------
c if meth = 1, also restrict the new step size by the stability region.
c if this reduces h, set irflag to 1 so that if there are roundoff
c problems later, we can assume that is the cause of the trouble.
c-----------------------------------------------------------------------
      if (meth .eq. 2) go to 178
      irflag = 0
      pdh = dmax1(dabs(h)*pdlast,0.000001d0)
      if (rh*pdh*1.00001d0 .lt. sm1(nq)) go to 178
      rh = sm1(nq)/pdh
      irflag = 1
 178  continue
      r = 1.0d0
      do 180 j = 2,l
        r = r*rh
        do 180 i = 1,n
 180      yh(i,j) = yh(i,j)*r
      h = h*rh
      rc = rc*rh
      ialth = l
      if (iredo .eq. 0) go to 690
c-----------------------------------------------------------------------
c this section computes the predicted values by effectively
c multiplying the yh array by the pascal triangle matrix.
c rc is the ratio of new to old values of the coefficient  h*el(1).
c when rc differs from 1 by more than ccmax, ipup is set to miter
c to force pjac to be called, if a jacobian is involved.
c in any case, pjac is called at least every msbp steps.
c-----------------------------------------------------------------------
 200  if (dabs(rc-1.0d0) .gt. ccmax) ipup = miter
      if (nst .ge. nslp+msbp) ipup = miter
      tn = tn + h
      i1 = nqnyh + 1
      do 215 jb = 1,nq
        i1 = i1 - nyh
cdir$ ivdep
        do 210 i = i1,nqnyh
 210      yh1(i) = yh1(i) + yh1(i+nyh)
 215    continue
      pnorm = vmnorm (n, yh1, ewt)
c-----------------------------------------------------------------------
c up to maxcor corrector iterations are taken.  a convergence test is
c made on the r.m.s. norm of each correction, weighted by the error
c weight vector ewt.  the sum of the corrections is accumulated in the
c vector acor(i).  the yh array is not altered in the corrector loop.
c-----------------------------------------------------------------------
 220  m = 0
      rate = 0.0d0
      del = 0.0d0
      do 230 i = 1,n
 230    y(i) = yh(i,1)
      call f (neq, tn, y, savf)
      nfe = nfe + 1
      if (ipup .le. 0) go to 250
c-----------------------------------------------------------------------
c if indicated, the matrix p = i - h*el(1)*j is reevaluated and
c preprocessed before starting the corrector iteration.  ipup is set
c to 0 as an indicator that this has been done.
c-----------------------------------------------------------------------
      call pjac (neq, y, yh, nyh, ewt, acor, savf, wm, iwm, f, jac)
      ipup = 0
      rc = 1.0d0
      nslp = nst
      crate = 0.7d0
      if (ierpj .ne. 0) go to 430
 250  do 260 i = 1,n
 260    acor(i) = 0.0d0
 270  if (miter .ne. 0) go to 350
c-----------------------------------------------------------------------
c in the case of functional iteration, update y directly from
c the result of the last function evaluation.
c-----------------------------------------------------------------------
      do 290 i = 1,n
        savf(i) = h*savf(i) - yh(i,2)
 290    y(i) = savf(i) - acor(i)
      del = vmnorm (n, y, ewt)
      do 300 i = 1,n
        y(i) = yh(i,1) + el(1)*savf(i)
 300    acor(i) = savf(i)
      go to 400
c-----------------------------------------------------------------------
c in the case of the chord method, compute the corrector error,
c and solve the linear system with that as right-hand side and
c p as coefficient matrix.
c-----------------------------------------------------------------------
 350  do 360 i = 1,n
 360    y(i) = h*savf(i) - (yh(i,2) + acor(i))
      call slvs (wm, iwm, y, savf)
      if (iersl .lt. 0) go to 430
      if (iersl .gt. 0) go to 410
      del = vmnorm (n, y, ewt)
      do 380 i = 1,n
        acor(i) = acor(i) + y(i)
 380    y(i) = yh(i,1) + el(1)*acor(i)
c-----------------------------------------------------------------------
c test for convergence.  if m.gt.0, an estimate of the convergence
c rate constant is stored in crate, and this is used in the test.
c
c we first check for a change of iterates that is the size of
c roundoff error.  if this occurs, the iteration has converged, and a
c new rate estimate is not formed.
c in all other cases, force at least two iterations to estimate a
c local lipschitz constant estimate for adams methods.
c on convergence, form pdest = local maximum lipschitz constant
c estimate.  pdlast is the most recent nonzero estimate.
c-----------------------------------------------------------------------
 400  continue
      if (del .le. 100.0d0*pnorm*uround) go to 450
      if (m .eq. 0 .and. meth .eq. 1) go to 405
      if (m .eq. 0) go to 402
      rm = 1024.0d0
      if (del .le. 1024.0d0*delp) rm = del/delp
      rate = dmax1(rate,rm)
      crate = dmax1(0.2d0*crate,rm)
 402  dcon = del*dmin1(1.0d0,1.5d0*crate)/(tesco(2,nq)*conit)
      if (dcon .gt. 1.0d0) go to 405
      pdest = dmax1(pdest,rate/dabs(h*el(1)))
      if (pdest .ne. 0.0d0) pdlast = pdest
      go to 450
 405  continue
      m = m + 1
      if (m .eq. maxcor) go to 410
      if (m .ge. 2 .and. del .gt. 2.0d0*delp) go to 410
      delp = del
      call f (neq, tn, y, savf)
      nfe = nfe + 1
      go to 270
c-----------------------------------------------------------------------
c the corrector iteration failed to converge.
c if miter .ne. 0 and the jacobian is out of date, pjac is called for
c the next try.  otherwise the yh array is retracted to its values
c before prediction, and h is reduced, if possible.  if h cannot be
c reduced or mxncf failures have occurred, exit with kflag = -2.
c-----------------------------------------------------------------------
 410  if (miter .eq. 0 .or. jcur .eq. 1) go to 430
      icf = 1
      ipup = miter
      go to 220
 430  icf = 2
      ncf = ncf + 1
      rmax = 2.0d0
      tn = told
      i1 = nqnyh + 1
      do 445 jb = 1,nq
        i1 = i1 - nyh
cdir$ ivdep
        do 440 i = i1,nqnyh
 440      yh1(i) = yh1(i) - yh1(i+nyh)
 445    continue
      if (ierpj .lt. 0 .or. iersl .lt. 0) go to 680
      if (dabs(h) .le. hmin*1.00001d0) go to 670
      if (ncf .eq. mxncf) go to 670
      rh = 0.25d0
      ipup = miter
      iredo = 1
      go to 170
c-----------------------------------------------------------------------
c the corrector has converged.  jcur is set to 0
c to signal that the jacobian involved may need updating later.
c the local error test is made and control passes to statement 500
c if it fails.
c-----------------------------------------------------------------------
 450  jcur = 0
      if (m .eq. 0) dsm = del/tesco(2,nq)
      if (m .gt. 0) dsm = vmnorm (n, acor, ewt)/tesco(2,nq)
      if (dsm .gt. 1.0d0) go to 500
c-----------------------------------------------------------------------
c after a successful step, update the yh array.
c decrease icount by 1, and if it is -1, consider switching methods.
c if a method switch is made, reset various parameters,
c rescale the yh array, and exit.  if there is no switch,
c consider changing h if ialth = 1.  otherwise decrease ialth by 1.
c if ialth is then 1 and nq .lt. maxord, then acor is saved for
c use in a possible order increase on the next step.
c if a change in h is considered, an increase or decrease in order
c by one is considered also.  a change in h is made only if it is by a
c factor of at least 1.1.  if not, ialth is set to 3 to prevent
c testing for that many steps.
c-----------------------------------------------------------------------
      kflag = 0
      iredo = 0
      nst = nst + 1
      hu = h
      nqu = nq
      mused = meth
      do 460 j = 1,l
        do 460 i = 1,n
 460      yh(i,j) = yh(i,j) + el(j)*acor(i)
      icount = icount - 1
      if (icount .ge. 0) go to 488
      if (meth .eq. 2) go to 480
c-----------------------------------------------------------------------
c we are currently using an adams method.  consider switching to bdf.
c if the current order is greater than 5, assume the problem is
c not stiff, and skip this section.
c if the lipschitz constant and error estimate are not polluted
c by roundoff, go to 470 and perform the usual test.
c otherwise, switch to the bdf methods if the last step was
c restricted to insure stability (irflag = 1), and stay with adams
c method if not.  when switching to bdf with polluted error estimates,
c in the absence of other information, double the step size.
c
c when the estimates are ok, we make the usual test by computing
c the step size we could have (ideally) used on this step,
c with the current (adams) method, and also that for the bdf.
c if nq .gt. mxords, we consider changing to order mxords on switching.
c compare the two step sizes to decide whether to switch.
c the step size advantage must be at least ratio = 5 to switch.
c-----------------------------------------------------------------------
      if (nq .gt. 5) go to 488
      if (dsm .gt. 100.0d0*pnorm*uround .and. pdest .ne. 0.0d0)
     1   go to 470
      if (irflag .eq. 0) go to 488
      rh2 = 2.0d0
      nqm2 = min0(nq,mxords)
      go to 478
 470  continue
      exsm = 1.0d0/dfloat(l)
      rh1 = 1.0d0/(1.2d0*dsm**exsm + 0.0000012d0)
      rh1it = 2.0d0*rh1
      pdh = pdlast*dabs(h)
      if (pdh*rh1 .gt. 0.00001d0) rh1it = sm1(nq)/pdh
      rh1 = dmin1(rh1,rh1it)
      if (nq .le. mxords) go to 474
         nqm2 = mxords
         lm2 = mxords + 1
         exm2 = 1.0d0/dfloat(lm2)
         lm2p1 = lm2 + 1
         dm2 = vmnorm (n, yh(1,lm2p1), ewt)/cm2(mxords)
         rh2 = 1.0d0/(1.2d0*dm2**exm2 + 0.0000012d0)
         go to 476
 474  dm2 = dsm*(cm1(nq)/cm2(nq))
      rh2 = 1.0d0/(1.2d0*dm2**exsm + 0.0000012d0)
      nqm2 = nq
 476  continue
      if (rh2 .lt. ratio*rh1) go to 488
c the switch test passed.  reset relevant quantities for bdf. ----------
 478  rh = rh2
      icount = 20
      meth = 2
      miter = jtyp
      pdlast = 0.0d0
      nq = nqm2
      l = nq + 1
      go to 170
c-----------------------------------------------------------------------
c we are currently using a bdf method.  consider switching to adams.
c compute the step size we could have (ideally) used on this step,
c with the current (bdf) method, and also that for the adams.
c if nq .gt. mxordn, we consider changing to order mxordn on switching.
c compare the two step sizes to decide whether to switch.
c the step size advantage must be at least 5/ratio = 1 to switch.
c if the step size for adams would be so small as to cause
c roundoff pollution, we stay with bdf.
c-----------------------------------------------------------------------
 480  continue
      exsm = 1.0d0/dfloat(l)
      if (mxordn .ge. nq) go to 484
         nqm1 = mxordn
         lm1 = mxordn + 1
         exm1 = 1.0d0/dfloat(lm1)
         lm1p1 = lm1 + 1
         dm1 = vmnorm (n, yh(1,lm1p1), ewt)/cm1(mxordn)
         rh1 = 1.0d0/(1.2d0*dm1**exm1 + 0.0000012d0)
         go to 486
 484  dm1 = dsm*(cm2(nq)/cm1(nq))
      rh1 = 1.0d0/(1.2d0*dm1**exsm + 0.0000012d0)
      nqm1 = nq
      exm1 = exsm
 486  rh1it = 2.0d0*rh1
      pdh = pdnorm*dabs(h)
      if (pdh*rh1 .gt. 0.00001d0) rh1it = sm1(nqm1)/pdh
      rh1 = dmin1(rh1,rh1it)
      rh2 = 1.0d0/(1.2d0*dsm**exsm + 0.0000012d0)
      if (rh1*ratio .lt. 5.0d0*rh2) go to 488
      alpha = dmax1(0.001d0,rh1)
      dm1 = (alpha**exm1)*dm1
      if (dm1 .le. 1000.0d0*uround*pnorm) go to 488
c the switch test passed.  reset relevant quantities for adams. --------
      rh = rh1
      icount = 20
      meth = 1
      miter = 0
      pdlast = 0.0d0
      nq = nqm1
      l = nq + 1
      go to 170
c
c no method switch is being made.  do the usual step/order selection. --
 488  continue
      ialth = ialth - 1
      if (ialth .eq. 0) go to 520
      if (ialth .gt. 1) go to 700
      if (l .eq. lmax) go to 700
      do 490 i = 1,n
 490    yh(i,lmax) = acor(i)
      go to 700
c-----------------------------------------------------------------------
c the error test failed.  kflag keeps track of multiple failures.
c restore tn and the yh array to their previous values, and prepare
c to try the step again.  compute the optimum step size for this or
c one lower order.  after 2 or more failures, h is forced to decrease
c by a factor of 0.2 or less.
c-----------------------------------------------------------------------
 500  kflag = kflag - 1
      tn = told
      i1 = nqnyh + 1
      do 515 jb = 1,nq
        i1 = i1 - nyh
cdir$ ivdep
        do 510 i = i1,nqnyh
 510      yh1(i) = yh1(i) - yh1(i+nyh)
 515    continue
      rmax = 2.0d0
      if (dabs(h) .le. hmin*1.00001d0) go to 660
      if (kflag .le. -3) go to 640
      iredo = 2
      rhup = 0.0d0
      go to 540
c-----------------------------------------------------------------------
c regardless of the success or failure of the step, factors
c rhdn, rhsm, and rhup are computed, by which h could be multiplied
c at order nq - 1, order nq, or order nq + 1, respectively.
c in the case of failure, rhup = 0.0 to avoid an order increase.
c the largest of these is determined and the new order chosen
c accordingly.  if the order is to be increased, we compute one
c additional scaled derivative.
c-----------------------------------------------------------------------
 520  rhup = 0.0d0
      if (l .eq. lmax) go to 540
      do 530 i = 1,n
 530    savf(i) = acor(i) - yh(i,lmax)
      dup = vmnorm (n, savf, ewt)/tesco(3,nq)
      exup = 1.0d0/dfloat(l+1)
      rhup = 1.0d0/(1.4d0*dup**exup + 0.0000014d0)
 540  exsm = 1.0d0/dfloat(l)
      rhsm = 1.0d0/(1.2d0*dsm**exsm + 0.0000012d0)
      rhdn = 0.0d0
      if (nq .eq. 1) go to 550
      ddn = vmnorm (n, yh(1,l), ewt)/tesco(1,nq)
      exdn = 1.0d0/dfloat(nq)
      rhdn = 1.0d0/(1.3d0*ddn**exdn + 0.0000013d0)
c if meth = 1, limit rh according to the stability region also. --------
 550  if (meth .eq. 2) go to 560
      pdh = dmax1(dabs(h)*pdlast,0.000001d0)
      if (l .lt. lmax) rhup = dmin1(rhup,sm1(l)/pdh)
      rhsm = dmin1(rhsm,sm1(nq)/pdh)
      if (nq .gt. 1) rhdn = dmin1(rhdn,sm1(nq-1)/pdh)
      pdest = 0.0d0
 560  if (rhsm .ge. rhup) go to 570
      if (rhup .gt. rhdn) go to 590
      go to 580
 570  if (rhsm .lt. rhdn) go to 580
      newq = nq
      rh = rhsm
      go to 620
 580  newq = nq - 1
      rh = rhdn
      if (kflag .lt. 0 .and. rh .gt. 1.0d0) rh = 1.0d0
      go to 620
 590  newq = l
      rh = rhup
      if (rh .lt. 1.1d0) go to 610
      r = el(l)/dfloat(l)
      do 600 i = 1,n
 600    yh(i,newq+1) = acor(i)*r
      go to 630
 610  ialth = 3
      go to 700
c if meth = 1 and h is restricted by stability, bypass 10 percent test.
 620  if (meth .eq. 2) go to 622
      if (rh*pdh*1.00001d0 .ge. sm1(newq)) go to 625
 622  if (kflag .eq. 0 .and. rh .lt. 1.1d0) go to 610
 625  if (kflag .le. -2) rh = dmin1(rh,0.2d0)
c-----------------------------------------------------------------------
c if there is a change of order, reset nq, l, and the coefficients.
c in any case h is reset according to rh and the yh array is rescaled.
c then exit from 690 if the step was ok, or redo the step otherwise.
c-----------------------------------------------------------------------
      if (newq .eq. nq) go to 170
 630  nq = newq
      l = nq + 1
      iret = 2
      go to 150
c-----------------------------------------------------------------------
c control reaches this section if 3 or more failures have occured.
c if 10 failures have occurred, exit with kflag = -1.
c it is assumed that the derivatives that have accumulated in the
c yh array have errors of the wrong order.  hence the first
c derivative is recomputed, and the order is set to 1.  then
c h is reduced by a factor of 10, and the step is retried,
c until it succeeds or h reaches hmin.
c-----------------------------------------------------------------------
 640  if (kflag .eq. -10) go to 660
      rh = 0.1d0
      rh = dmax1(hmin/dabs(h),rh)
      h = h*rh
      do 645 i = 1,n
 645    y(i) = yh(i,1)
      call f (neq, tn, y, savf)
      nfe = nfe + 1
      do 650 i = 1,n
 650    yh(i,2) = h*savf(i)
      ipup = miter
      ialth = 5
      if (nq .eq. 1) go to 200
      nq = 1
      l = 2
      iret = 3
      go to 150
c-----------------------------------------------------------------------
c all returns are made through this section.  h is saved in hold
c to allow the caller to change h on the next step.
c-----------------------------------------------------------------------
 660  kflag = -1
      go to 720
 670  kflag = -2
      go to 720
 680  kflag = -3
      go to 720
 690  rmax = 10.0d0
 700  r = 1.0d0/tesco(2,nqu)
      do 710 i = 1,n
 710    acor(i) = acor(i)*r
 720  hold = h
      jstart = 1
      return
c----------------------- end of subroutine stoda -----------------------
      end
      double precision function vmnorm (n, v, w)
clll. optimize
c-----------------------------------------------------------------------
c this function routine computes the weighted max-norm
c of the vector of length n contained in the array v, with weights
c contained in the array w of length n..
c   vmnorm = max(i=1,...,n) abs(v(i))*w(i)
c-----------------------------------------------------------------------
      integer n,   i
      double precision v, w,   vm
      dimension v(n), w(n)
      vm = 0.0d0
      do 10 i = 1,n
 10     vm = dmax1(vm,dabs(v(i))*w(i))
      vmnorm = vm
      return
c----------------------- end of function vmnorm ------------------------
      end
      subroutine xerrwv (msg, nmes, nerr, level, ni, i1, i2, nr, r1, r2)
      integer msg, nmes, nerr, level, ni, i1, i2, nr,
     1   i, lun, lunit, mesflg, ncpw, nch, nwds
      double precision r1, r2
      dimension msg(nmes)
c-----------------------------------------------------------------------
c subroutines xerrwv, xsetf, and xsetun, as given here, constitute
c a simplified version of the slatec error handling package.
c written by a. c. hindmarsh at llnl.  version of march 30, 1987.
c this version is in double precision.
c
c all arguments are input arguments.
c
c msg    = the message (hollerith literal or integer array).
c nmes   = the length of msg (number of characters).
c nerr   = the error number (not used).
c level  = the error level..
c          0 or 1 means recoverable (control returns to caller).
c          2 means fatal (run is aborted--see note below).
c ni     = number of integers (0, 1, or 2) to be printed with message.
c i1,i2  = integers to be printed, depending on ni.
c nr     = number of reals (0, 1, or 2) to be printed with message.
c r1,r2  = reals to be printed, depending on nr.
c
c note..  this routine is machine-dependent and specialized for use
c in limited context, in the following ways..
c 1. the number of hollerith characters stored per word, denoted
c    by ncpw below, is a data-loaded constant.
c 2. the value of nmes is assumed to be at most 60.
c    (multi-line messages are generated by repeated calls.)
c 3. if level = 2, control passes to the statement   stop
c    to abort the run.  this statement may be machine-dependent.
c 4. r1 and r2 are assumed to be in double precision and are printed
c    in d21.13 format.
c 5. the common block /eh0001/ below is data-loaded (a machine-
c    dependent feature) with default values.
c    this block is needed for proper retention of parameters used by
c    this routine which the user can reset by calling xsetf or xsetun.
c    the variables in this block are as follows..
c       mesflg = print control flag..
c                1 means print all messages (the default).
c                0 means no printing.
c       lunit  = logical unit number for messages.
c                the default is 6 (machine-dependent).
c-----------------------------------------------------------------------
c the following are instructions for installing this routine
c in different machine environments.
c
c to change the default output unit, change the data statement
c in the block data subprogram below.
c
c for a different number of characters per word, change the
c data statement setting ncpw below, and format 10.  alternatives for
c various computers are shown in comment cards.
c
c for a different run-abort command, change the statement following
c statement 100 at the end.
c-----------------------------------------------------------------------
      common /eh0001/ mesflg, lunit
c-----------------------------------------------------------------------
c the following data-loaded value of ncpw is valid for the cdc-6600
c and cdc-7600 computers.
c     data ncpw/10/
c the following is valid for the cray-1 computer.
c     data ncpw/8/
c the following is valid for the burroughs 6700 and 7800 computers.
c     data ncpw/6/
c the following is valid for the pdp-10 computer.
c     data ncpw/5/
c the following is valid for the vax computer with 4 bytes per integer,
c and for the ibm-360, ibm-370, ibm-303x, and ibm-43xx computers.
      data ncpw/4/
c the following is valid for the pdp-11, or vax with 2-byte integers.
c     data ncpw/2/
c-----------------------------------------------------------------------
cys      lunit=10
      lunit=6
      mesflg=1
      if (mesflg .eq. 0) go to 100
c get logical unit number. ---------------------------------------------
cys     lun = 10
      lun = 6
c get number of words in message. --------------------------------------
      nch = min0(nmes,60)
      nwds = nch/ncpw
      if (nch .ne. nwds*ncpw) nwds = nwds + 1
c write the message. ---------------------------------------------------
      write (lun, 10) (msg(i),i=1,nwds)
c-----------------------------------------------------------------------
c the following format statement is to have the form
c 10  format(1x,mmann)
c where nn = ncpw and mm is the smallest integer .ge. 60/ncpw.
c the following is valid for ncpw = 10.
c 10  format(1x,6a10)
c the following is valid for ncpw = 8.
c 10  format(1x,8a8)
c the following is valid for ncpw = 6.
c 10  format(1x,10a6)
c the following is valid for ncpw = 5.
c 10  format(1x,12a5)
c the following is valid for ncpw = 4.
  10  format(1x,15a4)
c the following is valid for ncpw = 2.
c 10  format(1x,30a2)
c-----------------------------------------------------------------------
      if (ni .eq. 1) write (lun, 20) i1
 20   format(6x,23hin above message,  i1 =,i10)
      if (ni .eq. 2) write (lun, 30) i1,i2
 30   format(6x,23hin above message,  i1 =,i10,3x,4hi2 =,i10)
      if (nr .eq. 1) write (lun, 40) r1
 40   format(6x,23hin above message,  r1 =,d21.13)
      if (nr .eq. 2) write (lun, 50) r1,r2
 50   format(6x,15hin above,  r1 =,d21.13,3x,4hr2 =,d21.13)
c abort the run if level = 2. ------------------------------------------
 100  if (level .ne. 2) return
      stop
c----------------------- end of subroutine xerrwv ----------------------
      end
C***********************************************************
C******************     XMMAT    *************************
C***********************************************************
C
C
C      PROCEDURE DE MULTIPLICATION DE 2 MATRICES  
C             MATRICE1*MATRICE2
C      DE  DIMENSION DE VALEUR DECLAREE 
C      MAT1= L1MAX,K1MAX
C      MAT2= K1MAX,K2MAX
C      MATRES=L1MAX,K2MAX
C
C**********************************************************
C
C   L1MAX : NBRE DE LIGNES DE LA MATRICE 1
C   K1MAX : NBRE DE COLONNES DE LA MATRICE 1
C   L2MAX : NBRE DE LIGNES DE LA MATRICE 2
C   K2MAX : NBRE DE COLONNES DE LA MATRICE 2
C   MAT1 : MATRICE 1 DE TAILLE NL1*NC1
C   MAT2 : MATRICE 2 DE TAILLE NL2*NC2
C   MATRES : MATRICE PRODUIT DE TAILLE NL1*NC2
C
      SUBROUTINE XMMAT (NL1,NC1,NC2,L1MAX,K1MAX,K2MAX,
     +             MAT1,MAT2,MATRES)
C
      INTEGER I1,I2,I3,I4,KK,NL1,NC1,NC2,L1MAX,K1MAX,K2MAX
      DOUBLE PRECISION MAT1(L1MAX,K1MAX),MAT2(K1MAX,K2MAX)
      DOUBLE PRECISION MATRES(L1MAX,K2MAX)
C
C
      DO 50 I2 = 1,NC2
        DO 40 I1 = 1,NL1
          MATRES(I1,I2) = 0.D+00
 40     CONTINUE         
 50   CONTINUE
C
C      
      DO 150 I4 = 1,NC2
        DO 140 I3 = 1,NL1
          DO 130 KK = 1,NC1
              MATRES(I3,I4) = MATRES(I3,I4)+(MAT1(I3,KK)*MAT2(KK,I4))
 130      CONTINUE
 140    CONTINUE         
 150  CONTINUE
C        
      RETURN
      END
C***********************************************************
C******************     XMMVEC    *************************
C***********************************************************
C
C      PROCEDURE DE MULTIPLICATION D'UNE MATRICE AVEC UN VECTEUR 
C             MATRICE*VECTEUR
C      MATRICE DE DIMESIONS DECLAREES SUIVANTES
C            NBRE DE LIGNES=L1MAX
C            NBRE DE COLONNES=K1MAX
C      VECTEUR DE TAILLE DECLAREE=K1MAX
C
C*************************************************************
C
C   NL1 : NBRE DE LIGNES DE LA MATRICE 1
C   NC1 : NBRE DE COLONNES DE LA MATRICE 1
C   NL :  NBRE D'ELEMENT DU VECTEUR COLONNE
C   
C   MAT1 : MATRICE  DE TAILLE NL1*NC1
C   VECT : VECTEUR DE TAILLE NL
C   VECRES : VECTEUR COLONNE RESULTANT DE TAILLE NL1
C
      SUBROUTINE XMMVEC (NL1,NC1,L1MAX,K1MAX,MAT1,
     +          VECT,VECRES)
C
      INTEGER I2,KK,NL1,NC1,L1MAX,K1MAX
      DOUBLE PRECISION MAT1(L1MAX,K1MAX)
      DOUBLE PRECISION VECT(*), VECRES(*)
C
      DO 50 I2 = 1,NL1
        VECRES(I2) = MAT1(I2,1)*VECT(1)
 50   CONTINUE
C
C        
      DO 150 KK = 2,NC1
        DO 130 I2 = 1,NL1
             VECRES(I2) = VECRES(I2)+(MAT1(I2,KK)*VECT(KK))
 130    CONTINUE 
 150  CONTINUE
C        
      RETURN
      END
CC***********************************************************
C******************     XMMVEC    *************************
C***********************************************************
C
C      PROCEDURE DE MULTIPLICATION D'UNE MATRICE AVEC UN VECTEUR 
C             MATRICE*VECTEUR
C      MATRICE DE DIMESIONS DECLAREES SUIVANTES
C            NBRE DE LIGNES=L1MAX
C            NBRE DE COLONNES=K1MAX
C      VECTEUR DE TAILLE DECLAREE=K1MAX
C
C*************************************************************
C
C   NL1 : NBRE DE LIGNES DE LA MATRICE 1
C   NC1 : NBRE DE COLONNES DE LA MATRICE 1
C   NL1 :  NBRE D'ELEMENT DU VECTEUR COLONNE
C   
C   MAT1 : MATRICE  DE TAILLE NL1*NC1
C   VECT : VECTEUR DE TAILLE NL1
C   VECRES : VECTEUR COLONNE RESULTANT DE TAILLE NL1
C
      SUBROUTINE XMMVEC_DIAG (NL1,NC1,L1MAX,K1MAX,MAT1,
     +          VECT,VECRES)
C
      INTEGER I2,KK,NL1,NC1,L1MAX,K1MAX
      DOUBLE PRECISION MAT1(L1MAX,K1MAX),VECT(K1MAX)
      DOUBLE PRECISION VECRES(L1MAX)
C
C
C        
      DO 150 I2 = 1,NL1
        VECRES(I2) = MAT1(I2,I2)*VECT(I2)
 150  CONTINUE
C        
      RETURN
      END
CC***********************************************************
C******************     XMMVEC    *************************
C***********************************************************
C
C      PROCEDURE DE MULTIPLICATION D'UNE MATRICE AVEC UN VECTEUR 
C             MATRICE*VECTEUR
C      MATRICE DE DIMESIONS DECLAREES SUIVANTES
C            NBRE DE LIGNES=L1MAX
C            NBRE DE COLONNES=K1MAX
C      VECTEUR DE TAILLE DECLAREE=K1MAX
C
C*************************************************************
C
C   NL1 : NBRE DE LIGNES DE LA MATRICE 1
C   NC1 : NBRE DE COLONNES DE LA MATRICE 1
C   NL1 :  NBRE D'ELEMENT DU VECTEUR COLONNE
C   
C   MAT1 : MATRICE  DE TAILLE NL1*NC1
C   VECT : VECTEUR DE TAILLE NL1
C   VECRES : VECTEUR COLONNE RESULTANT DE TAILLE NL1
C
      SUBROUTINE XMMVEC_TRIDIAG (NL1,NC1,L1MAX,K1MAX,MAT1,
     +          VECT,VECRES)
C
      INTEGER II,KK,NL1,NC1,L1MAX,K1MAX
      DOUBLE PRECISION MAT1(L1MAX,K1MAX),VECT(K1MAX)
      DOUBLE PRECISION VECRES(L1MAX)
C
      DO 50 II = 1,NL1
        VECRES(II) = MAT1(II,II)*VECT(II)
 50   CONTINUE
C
C 
      VECRES(1) = VECRES(1) + MAT1(1,2) * VECT(2)
C       
      DO 150 II = 2,NL1-1
        KK = II-1 
        VECRES(II) = VECRES(II) + MAT1(II,KK) * VECT(KK)
        KK = II+1
        VECRES(II) = VECRES(II) + MAT1(II,KK) * VECT(KK)
 150  CONTINUE
C
      VECRES(NL1) = 
     &       VECRES(NL1) + MAT1(NL1,NL1-1) * VECT(NL1-1) 
C
C       
      RETURN
      END
C*
      DOUBLE PRECISION FUNCTION XMVEVE(V,W,NDIM,NDIMMX)
C     =================================================
C
C***********************************************************************
C*                                                                     *
C*                                                                     *
C*    CALCUL DU PRODUIT SCALAIRE DE 2 VECTEURS                         *
C*                                                                     *
C*                                                                     *
C***********************************************************************
C
C
C **********************************************************************
C PARAMETRES DU SOUS-PROGRAMME
C **********************************************************************
C
      INTEGER          NDIM,NDIMMX,K
      DOUBLE PRECISION V(NDIMMX)
      DOUBLE PRECISION W(NDIMMX)
C
C **********************************************************************
C CORPS DU SOUS-PROGRAMME
C **********************************************************************
C
      XMVEVE = V(1)*W(1)
      DO 10 K = 2, NDIM
            XMVEVE = XMVEVE + V(K)*W(K)
 10   CONTINUE
C
      RETURN
      END
C      SUBROUTINE ARRET_PROG
C     ====================
C
C
C     ******************************************************************
C     ARRET_PROG : Arret du programme                   
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      include 'cmpi.inc'   
C

        STOP

C
C   
      RETURN
      END
*
*
      SUBROUTINE MODIST(L,IGROB,I,IHETER)
*     ===================================
*
*     ******************************************************************
*     MODIST : MOgador Parallele distribution des valeurs contenues dans
*              les grands tableaux aux autres processeurs.
*     ******************************************************************
*
*
*     ------------------------------------------------------------------
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogear.inc'
**mpi

***mpi
      RETURN
      END
C
*     ------------------------------------------------------------------
*     FONCTIONNEMENT DE MARGARET V3.1 EN AUTONOME
*     ------------------------------------------------------------------
C

      PROGRAM MOGAUT 
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
*
      INCLUDE 'moparam.inc' 
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogmai.inc'
      INCLUDE 'cmogdon.inc'
      INCLUDE 'cmogppr.inc'
      INCLUDE 'cmogpnu.inc'
      INCLUDE 'cmogmat.inc'
      INCLUDE 'cmogtem.inc'
CDEBUG
      INCLUDE 'cmogdebug.inc'
      INCLUDE 'crednbr.inc'   
CDEBUG
*
************************************************************************
* cmoginf.inc : mo_NLEC  mo_NSOR mo_NNRES1 mo_NNRES2 
*               mo_NNRES3 mo_NNRES4 mo_NNRES5 mo_NMAC 
*               mo_NREPW mo_NREPR mo_PASSAGE en sortie
*               mo_DEPART_SUR_REPRISE mo_NTPSINI en entr�e
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE mo_NBNOZO en entr�e
* cmogdon.inc : xmo_AGG en entr�e
* cmogppr.inc : xmo_DREM en entr�e
* cmogpnu.inc : xmo_RGG mo_NGG xmo_RSEQ mo_NSEQ  xmo_RSEQR mo_NSEQR
*               en entr�e
* cmogmat.inc : toutes les variables en sortie
* cmogtem.inc : xmo_TEMPS en entr�e
************************************************************************
**MPI
* Fichier de definitions pour la bibliotheque MPI

      include 'cmpi.inc'
      character*4 formatI
      real xmpiRank
      integer nbChar
***MPI
*
*
      CHARACTER*72 PHRASE
*
*     ==================================================================
*
**MPI

      mpiRank = 0
      mpiSize = 1

      formatI = '(i )'
      xmpiRank = max(1,mpiRank)
      nbChar = log10(xmpiRank) + 1
      write(formatI(3:3),'(i1)') nbChar
      write(mpisRank,formatI) mpiRank
c**MPI
c
c
c
c Unit�s logique des fichiers de lecture et d impression
      mo_NLEC  = 7
      mo_NSOR  = 8
      mo_DEBUG = 20
      mo_NMAC = 10
      mo_NREPW = 11
      mo_NREPR = 12
      mo_NINTEG = 30
      mo_NNSYNT(1) = 13
      mo_NNRES1(1)  = 14
      mo_NNRES2(1)  = 15
      mo_NNRES3(1)  = 16
      mo_NNRES4(1) = 17
      mo_NNRES5(1) = 18
c
c
      mo_NNSYNT(2) = 23
      mo_NNRES1(2)  = 24
      mo_NNRES2(2)  = 25
      mo_NNRES3(2)  = 26
      mo_NNRES4(2) = 27
      mo_NNRES5(2) = 28
c
c nombre de passages dans le sous-programme d impression MOWVAR
      mo_PASSAGE = 0
*
*
CC      PHRASE =' Calcul en cours, veuillez patienter ...'
CC      WRITE (5,'(A)') PHRASE
*
      CALL MOOPEN(1)
*
      CALL MOVADF
*
      CALL MOREAD
*
      CALL MOOPEN(2)
*
      CALL MOINIT
*
**MPI

* Execution par le process maitre de rang 0
      CALL MOWENT

***MPI
*
************************************************************************
* Initialisation des matrices adimensionn�es
*
************************************************************************
* Calcul des matrices et vecteurs utiles pour le grain
************************************************************************
C       volumes finis
        CALL MOMATR_VF(xmo_RGG,mo_NGG,
     &              xmo_TMGG,xmo_TIMGG,xmo_TIMRGG,
     &              xmo_TVGG)
C
        CALL MOMAQ_VF(xmo_RGG,mo_NGG,xmo_TIMGG,xmo_TIMQGG)
************************************************************************
* Calcul des matrices et vecteurs utiles pour la sph�re �quivalente
************************************************************************
        CALL MOMATR_VF(xmo_RSEQ,mo_NSEQ,
     &              xmo_TMSEQ,xmo_TIMSEQ,xmo_TIMRSEQ,
     &              xmo_TVSEQ)
C
        CALL MOMAQ_VF(xmo_RSEQ,mo_NSEQ,xmo_TIMSEQ,xmo_TIMQSEQ)
************************************************************************
* Calcul des matrices et vecteurs utiles pour la sph�re �quivalente
* du combustible restructur�
************************************************************************
        CALL MOMATR_VF(xmo_RSEQR,mo_NSEQR,
     &              xmo_TMSEQR,xmo_TIMSEQR,xmo_TIMRSEQR,
     &              xmo_TVSEQR)
C
        CALL MOMAQ_VF(xmo_RSEQR,mo_NSEQR,xmo_TIMSEQR,xmo_TIMQSEQR)
************************************************************************
************************************************************************
************************************************************************
************************************************************************
      mo_ICOMPTE = 0
*
      IF (mo_DEPART_SUR_REPRISE .EQ. 1) THEN
        CALL MORVAR
        mo_ICOMPTE = mo_NTPSINI
      ENDIF
*
10    CONTINUE
      xmo_TEMPS_DEPART = xmo_TEMPS
      mo_ICOMPTE = mo_ICOMPTE+1
      CALL REDLEC(4) 
      CALL REDLEC(4)
      CALL REDLEC(2)
CDEBUG
CDEBUG      WRITE(mo_DEBUG,4000) DFLOT
CDEBUG
      DO 20 L = 1,mo_NB_TRAX
        DO 20 IGROB = 1, mo_NB_ZONE(L)
          IF (IGROB .EQ. 1) THEN
            IDEB = 1
          ELSE
            IDEB = 2
          ENDIF
          DO 20 I = IDEB, mo_NBNOZO(L,IGROB)
CDEBUG 
            mo_LLL = L
            mo_IGRIGR = IGROB
            mo_III = I
            mo_FLAG = 0
            mo_LSUIVI = 1
            mo_IGRSUIVI = 7
            mo_ISUIVI = 5
CDEBUG  
            DO 20 IHETER = 1, mo_NAMAMX
**MPI
***MPI

              xmo_TEMPS = xmo_TEMPS_DEPART
              CALL MONETT
              CALL MOAFEC(L,IGROB,I,IHETER,1)
*
* Calcul de la matrice xmo_TIMPGG  volumes finis
              xmo_RREM = (xmo_AGG - xmo_DREM) / xmo_AGG
              CALL MOMAPR_VF(xmo_RGG,mo_NGG,xmo_TIMGG,xmo_TVGG,
     &                         xmo_RREM,xmo_TIMPGG)
*
**MPI

***MPI
* Rep�arge du dernier noeud. Ca sert dans le cas o� on veut imposer
* une sph�re �quivalente xmo_ASE diff�rente de celle qui va �tre lue
* dans le jeu de donn�e (utile en phase de validation uniquement)
            IF ( (IGROB .EQ. mo_NB_ZONE(L)) .AND.
     &           (I .EQ. mo_NBNOZO(L,IGROB)) ) THEN
              mo_DERNIER_NOEUD = 1
            ELSE
              mo_DERNIER_NOEUD = 0
            ENDIF
            CALL MOREEN(L,IGROB,I,mo_DERNIER_NOEUD)

**MPI

***MPI
CDEBUG
CDEBUG              WRITE(mo_DEBUG,*) 'TRAITEMENT ',mo_ICOMPTE
CDEBUG 
              CALL MOCSTF
              CALL MOCALV
              CALL MOAFEC(L,IGROB,I,IHETER,2)
**MPI

***MPI
 20   CONTINUE
*
*
      DO 30 L = 1,mo_NB_TRAX
        DO 30 IGROB = 1, mo_NB_ZONE(L)
          IF (IGROB .EQ. 1) THEN
            IDEB = 1
          ELSE
            IDEB = 2
          ENDIF
          DO 30 I = IDEB, mo_NBNOZO(L,IGROB) 
            DO 30 IHETER = 1, mo_NAMAMX
**MPI

***MPI
              CALL MOGMAC(L,IGROB,I,IHETER)
              CALL MOWMAC(L,IGROB,I,IHETER)
**MPI

***MPI
 30   CONTINUE
*
*
*
**MPI

***MPI
*
*
      CALL MOCAINT
      CALL MOVALP
      CALL MOWREP(mo_ICOMPTE)
      GOTO 10
*
*
**MPI

***MPI
*
*
 4000 FORMAT (1X,'PAS_DE_TEMPS  TEMPS_FINAL  ',E25.16) 
*  
      STOP
      END
      SUBROUTINE MOOPEN(mo_IOPTION)
*     =============================
*
*     ------------------------------------------------------------------
*
*     mo_NLEC  = unit� du fichier de donn�e + entr�es
*     mo_NSOR  = unit� du fichier de sortie
*     mo_NNRES1(1) = unit� du fichier d impression resultat1 analyse intra etat (0) et (1)
*     mo_NNRES2(1) = unit� du fichier d impression resultat2 analyse inter etat (0) 
*     mo_NNRES3(1) = unit� du fichier d impression resultat3 analyse inter etat (1)
*     mo_NNRES4(1) = unit� du fichier d impression resultat4 analyse  etat (2) et (3)
*     mo_NNRES5(1) = unit� du fichier d impression resultat5 analyse bilan terme a terme
*     mo_NNSYNT(1) = unit� du fichier d impression synthese
*     mo_NNRES1(2) = unit� du fichier d impression resultat1 analyse intra etat (0) et (1) - 2ieme point d analyse
*     mo_NNRES2(2) = unit� du fichier d impression resultat2 analyse inter etat (0) - 2ieme point d analyse
*     mo_NNRES3(2) = unit� du fichier d impression resultat3 analyse inter etat (1) - 2ieme point d analyse
*     mo_NNRES4(2) = unit� du fichier d impression resultat4 analyse  etat (2) et (3) - 2ieme point d analyse
*     mo_NNRES5(2) = unit� du fichier d impression resultat5 analyse bilan terme a terme - 2ieme point d analyse
*     mo_NNSYNT(2) = unit� du fichier d impression synthese - 2ieme point d analyse
*     mo_NMAC = unit� du fichier d impression des variables macroscopiques
*     mo_NINTEG = unit� du fichier d impression des variables int�grales
*     mo_NREPW = unit� du fichier d impression des variables pour reprise
*     mo_NREPR = unit� du fichier de lecture des variables pour reprise
*     ==================================================================
*
      IMPLICIT DOUBLE PRECISION (A-H,O-Z) 
      INCLUDE 'moparam.inc'
      INCLUDE 'cmoginf.inc'
************************************************************************
* cmoginf.inc : mo_NLEC mo_NSOR 
*               mo_NNRES1 mo_NNRES2 mo_NNRES3 mo_NNRES4 mo_NNRES5 mo_NNSYNT
*               mo_NMAC mo_NINTEG mo_NREPW mo_NREPR 
*               mo_NREPRISE mo_NBPTANA mo_DEPART_SUR_REPRISE 
*               NOM_FICH_REPRISE en entr�e
************************************************************************
*
*
**MPI
      include 'cmpi.inc'
***MPI
*
      CHARACTER*45 NFILE
*
*     ==================================================================
      IF (mo_IOPTION .EQ. 1) THEN
*     ==================================================================
*
**MPI

*
        OPEN (UNIT = mo_NLEC,FILE = 'lecture',
     &        STATUS='OLD')
        OPEN (UNIT = mo_NSOR,FILE = 'sortie',
     &        STATUS='UNKNOWN')
        OPEN (UNIT = mo_DEBUG,FILE = 'debuggage',
     &        STATUS='UNKNOWN')
        OPEN (UNIT = mo_NMAC,FILE = 'res_macroscopique',
     &        STATUS='UNKNOWN')
        OPEN (UNIT = mo_NINTEG,FILE = 'res_integral',
     &        STATUS='UNKNOWN')

***MPI
*
*     ==================================================================
      ENDIF
*     ==================================================================
*
*
*
*
*
*     ==================================================================
*     Ouverture de fichiers en fonction de ce qui a ete demande dans
*     le jeu de donnee
*     ==================================================================
      IF (mo_IOPTION .EQ. 2) THEN
*     ==================================================================
*
* Depart sur reprise demande dans le jeu de donnees
*
        IF (mo_DEPART_SUR_REPRISE .EQ. 1) THEN
           OPEN (UNIT = mo_NREPR,FILE = NOM_FICH_REPRISE,
     &           STATUS='OLD',ACCESS='SEQUENTIAL',
     &           FORM='UNFORMATTED')
        ENDIF
*
* Sauvegardes demandees dans le jeu de donnees
*
        IF (mo_NREPRISE .GT. 0) THEN
          OPEN (UNIT = mo_NREPW,FILE = 'reprise_w',
     &          STATUS='UNKNOWN',ACCESS='SEQUENTIAL',
     &          FORM='UNFORMATTED')
        ENDIF
*
* Analyse fine demandee sur au moins 1 point
*
        IF (mo_NBPTANA .GT. 0 ) THEN
          OPEN (UNIT = mo_NNRES1(1),FILE = 'resultat1',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES2(1),FILE = 'resultat2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES3(1),FILE = 'resultat3',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES4(1),FILE = 'resultat4',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES5(1),FILE = 'resultat5',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNSYNT(1),FILE = 'synthese',
     &          STATUS='UNKNOWN')
        ENDIF
*
* Analyse fine demandee sur 2 points
*
        IF (mo_NBPTANA .EQ. 2 ) THEN
          OPEN (UNIT = mo_NNRES1(2),FILE = 'resultat1_2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES2(2),FILE = 'resultat2_2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES3(2),FILE = 'resultat3_2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES4(2),FILE = 'resultat4_2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNRES5(2),FILE = 'resultat5_2',
     &          STATUS='UNKNOWN')
          OPEN (UNIT = mo_NNSYNT(2),FILE = 'synthese_2',
     &          STATUS='UNKNOWN')
        ENDIF
*
*     ==================================================================
      ENDIF
*     ==================================================================
*
      RETURN
      END
      SUBROUTINE MOWREP(mo_ICOMPTE)
C     =================
C
C
C     ******************************************************************
C     MOWREP : MOgador Write aux temps demand�s pour les REPrises
C              des grandeurs n�cessaires au redemarrage du calcul                      
C     ******************************************************************
C 
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      INCLUDE 'moparam.inc'
      INCLUDE 'cmogvar.inc'
      INCLUDE 'cmoginf.inc'
      INCLUDE 'cmogtem.inc'
      INCLUDE 'cmogsor.inc'
      INCLUDE 'cmogear.inc'
      INCLUDE 'cmogmai.inc'
* essaiLOC
      INCLUDE 'cmogloc.inc'
* essaiLOC
C
***********************************************************************
* cmogvar.inc : xmo_VECG xmo_VECD mo_IETCOM mo_IETCOM_APPR xmo_ASEBASC 
*               xmo_EQUADISP3 en entr�es 
* cmoginf.inc : mo_NREPW mo_NTPSREP mo_NREPRISE mo_TRANCHE_DEB 
*               mo_TRANCHE_FIN en entr�e
* cmogtem.inc : xmo_TEMPS en entr�e et sortie
* cmogsor.inc : xmo_SOMTOFIS3 xmo_GCREA3 xmo_REMISOL3 xmo_SORDIF3
*               xmo_SORBUL3 xmo_SORPOR3 xmo_SORDIFJ3 xmo_SORPERC3 xmo_SOREJEC3
*               xmo_TAUXBETAC3 xmo_TAUXSORBUL3
*               xmo_TAUXSORPOR3 xmo_TAUXREMISOL3
*               xmo_TAUXSOURXE3 xmo_TAUXOUTSE3  xmo_TAUXOUTPERC3 
*               xmo_TAUXOUTEJEC3 en entr�e
* cmogear.inc : xmo_RSAV3 mo_ISAV3 xmo_G_RWORKSAV3 mo_G_IWORKSAV3 mo_G_IEQSAV3 
*               xmo_G_RTOLSAV3 mo_IGTABSAV3 mo_IDTABSAV3 en entr�e
* cmogmai.inc : mo_NB_TRAX mo_NB_ZONE mo_NBNOZO en entree
* essaiLOC
* cmogloc.inc : xmo_CXERPORLOC03 en entr�e et sortie
* essaiLOC
***********************************************************************      
***********************************************************************
**MPI

      include 'cmpi.inc'

***MPI
*
*
***********************************************************************
*
*
* test decidant de l impression effective des variables
      DO  II = 1,mo_NREPRISE
          IF (mo_ICOMPTE .EQ. mo_NTPSREP(II) )  IREP = 1
      ENDDO
*
*
*
      IF ( IREP .EQ. 1 ) THEN
*     +++++++++++++++++++++++++
* on imprime le vecteur des variables dans les fichiers de reprise
*
**MPI

*
*
* Execution par le process maitre
      IF (mpiRank .eq. 0) THEN
*
***MPI
*
        WRITE(mo_NREPW) mo_ICOMPTE
*
        DO L = mo_TRANCHE_DEB,mo_TRANCHE_FIN
          DO IGROB = 1, mo_NB_ZONE(L)
            DO I = 1, mo_NBNOZO(L,IGROB) 
              DO IHETER = 1, mo_NAMAMX
                WRITE(mo_NREPW) 
     &            (xmo_VECG(L,IGROB,I,IHETER,IVGA,2),IVGA=1,mo_NVGAMX)
                WRITE(mo_NREPW) 
     &            (xmo_VECD(L,IGROB,I,IHETER,IVDE,2),IVDE=1,mo_NVDEMX)
                WRITE(mo_NREPW) 
     &            mo_IETCOM(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            mo_IETCOM_APPR(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            mo_PRFOIS3(L,IGROB,I,IHETER)
*
                WRITE(mo_NREPW)
     &            (xmo_EQUADISP3(L,IGROB,I,IHETER,NPOP),
     &                           NPOP=1,mo_NPOPMX)
*
                WRITE(mo_NREPW)
     &            (xmo_RSAV3(L,IGROB,I,IHETER,IS),IS=1,240) 
                WRITE(mo_NREPW)
     &            (mo_ISAV3(L,IGROB,I,IHETER,IS),IS=1,50) 
                WRITE(mo_NREPW)
     &            (xmo_G_RWORKSAV3(L,IGROB,I,IHETER,IS),
     &            IS=1,mo_LWRMAX)
                WRITE(mo_NREPW)
     &            (mo_G_IWORKSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_LWIMAX)
                WRITE(mo_NREPW)
     &            (mo_G_IEQSAV3(L,IGROB,I,IHETER,IS),IS=1,5)
                WRITE(mo_NREPW)
     &            (xmo_G_RTOLSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NEQMAX)
                WRITE(mo_NREPW)
     &            (mo_IGTABSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NVGAMX)
                WRITE(mo_NREPW)
     &            (mo_IDTABSAV3(L,IGROB,I,IHETER,IS),IS=1,mo_NVDEMX)
*
* essaiLOC
                WRITE(mo_NREPW)
     &            (xmo_CXERPORLOC03(L,IGROB,I,IHETER,IVGA),
     &                           IVGA=1,mo_NNSPHMX)
* essaiLOC 
                WRITE(mo_NREPW) 
     &            xmo_SOMTOFIS3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_ASEBASC(L,IGROB,I,IHETER)
*
                WRITE(mo_NREPW) 
     &            xmo_GCREA3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_REMISOL3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SORDIF3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SORBUL3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SORPOR3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SORDIFJ3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SORPERC3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_SOREJEC3(L,IGROB,I,IHETER,2)
*
                WRITE(mo_NREPW) 
     &            xmo_TAUXBETAC3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXREMISOL3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXSOURXE3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXSORBUL3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXSORPOR3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXOUTSE3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXOUTPERC3(L,IGROB,I,IHETER,2)
                WRITE(mo_NREPW) 
     &            xmo_TAUXOUTEJEC3(L,IGROB,I,IHETER,2)
              ENDDO
            ENDDO
          ENDDO
        ENDDO
*
*
**MPI
      ENDIF
***MPI
      ENDIF
*     +++++
*  
      RETURN
      END
