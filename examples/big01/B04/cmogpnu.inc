* common des variables
* fichier cmogpnu.inc
*     7--0---------0---------0---------0---------0---------0---------0--
*     ------------------------------------------------------------------
* Pour la m�thode en espace (El�ments finis ou volumes finis)
*
      COMMON /MOGPN1/ xmo_RGG(mo_NNSPHMX+1),
     &                xmo_RSEQ(mo_NNSPHMX+1),
     &                xmo_RSEQR(mo_NNSPHMX+1)
*
      COMMON /MOGPN2/ mo_NGG,
     &                mo_NSEQ,
     &                mo_NSEQR,
     &                mo_DEGRE
*
      COMMON /MOGPN3/ mo_SCHEMA
*     ------------------------------------------------------------------
