* common des variables
* fichier cmogmat.inc
************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*     ------------------------------------------------------------------
      COMMON /MOGMAT/ xmo_TMGG(mo_NNSPHMX,mo_NNSPHMX), 
     &                xmo_TIMGG(mo_NNSPHMX,mo_NNSPHMX),
     &                xmo_TIMRGG(mo_NNSPHMX,mo_NNSPHMX), 
     &                xmo_TVGG(mo_NNSPHMX),
     &                xmo_TIMQGG(mo_NNSPHMX),
     &                xmo_TIMPGG(mo_NNSPHMX),
     &                xmo_TIMPGG_ETAT2(mo_NNSPHMX),
*
     &                xmo_TMSEQ(mo_NNSPHMX,mo_NNSPHMX), 
     &                xmo_TIMSEQ(mo_NNSPHMX,mo_NNSPHMX),
     &                xmo_TIMRSEQ(mo_NNSPHMX,mo_NNSPHMX), 
     &                xmo_TVSEQ(mo_NNSPHMX),
     &                xmo_TIMQSEQ(mo_NNSPHMX),
*
     &                xmo_TMSEQR(mo_NNSPHMX,mo_NNSPHMX),
     &                xmo_TIMSEQR(mo_NNSPHMX,mo_NNSPHMX),
     &                xmo_TIMRSEQR(mo_NNSPHMX,mo_NNSPHMX), 
     &                xmo_TVSEQR(mo_NNSPHMX),
     &                xmo_TIMQSEQR(mo_NNSPHMX)
*
*     ------------------------------------------------------------------