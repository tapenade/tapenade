* common des variables
* fichier cmogvar.inc
************************************************************************
*     ------------------------------------------------------------------
      COMMON /MOGVA1/ 
     &  xmo_VECG(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NVGAMX,2),
     &  xmo_VECD(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NVDEMX,2),
     &  xmo_ASEBASC(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &  xmo_EQUADISP3
     &      (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NPOPMX),
     &  xmo_VG(mo_NVGAMX,2),
     &  xmo_VD(mo_NVDEMX,2),
     &  xmo_EQUADISP(mo_NPOPMX),
     &  xmo_ASEB, 
     &  xmo_ASERIM 
*
      COMMON /MOGVA2/ 
     &  mo_IETCOM(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &  mo_IETCOM_APPR(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &  mo_PRFOIS3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &  mo_IET,
     &  mo_IET_APPR,
     &  mo_PRFOIS,
     &  mo_NVG,
     &  mo_NVD
*     ------------------------------------------------------------------