*
*
*     --- Definition of symbolic constants
* Parametres : fichier parame.inc
************************************************************
*
*
*     --- Maximum number of radial coarse zones
*
      PARAMETER (mo_IGRMAX = 20) 
*
*     --- Maximum number of mesh points in a coarse zone
*
      PARAMETER (mo_IFEMAX = 15)
*
*     --- Maximum number of axial slices
*
      PARAMETER (mo_M31MAX  = 2)
*
*     --- Maximum number de type de fichier d analyse
*
      PARAMETER (mo_IFICHMX  = 6)
*
*     --- Agglomerate evolution model
*
      PARAMETER (mo_NAMAMX = 1)
*
      PARAMETER (mo_NNSPHMX = 11)
*
      PARAMETER (mo_NVGAMX = 
     &           2*mo_NNSPHMX+3+mo_NNSPHMX+3+mo_NNSPHMX+3)
*
      PARAMETER (mo_NVDEMX = 10)
*
*
      PARAMETER(mo_NREPRISEMX = 20)
*
* nombre max d equations dans Gear
* mo_NEQMAX doit etre superieur � 7
      PARAMETER(mo_NEQMAX = mo_NVGAMX + mo_NVDEMX)
*
*
      PARAMETER(mo_LWRMAX = 22 + 16 * mo_NEQMAX + mo_NEQMAX**2)
      PARAMETER(mo_LWIMAX = 20 + mo_NEQMAX)
*
* Nombre max de populations decrites
      PARAMETER(mo_NPOPMX = 10)
