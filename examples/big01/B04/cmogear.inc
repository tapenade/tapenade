************************************************************************
*-----7--0---------0---------0---------0---------0---------0---------0--
*
C
      DOUBLE PRECISION xmo_G_RWORK,xmo_G_RTOL,xmo_G_ATOL,xmo_YEXP
      COMMON/MOGRGEAR/xmo_G_RWORK(mo_LWRMAX),
     &              xmo_G_RTOL(mo_NEQMAX),
     &              xmo_G_ATOL(mo_NEQMAX),
     &              xmo_YEXP(mo_NEQMAX)
*
*
      INTEGER mo_G_IWORK,mo_G_LWR,mo_G_LWI,mo_G_NEQ
      COMMON/MOGIGEAR/mo_G_IWORK(mo_LWIMAX),
     &              mo_G_LWR,
     &              mo_G_LWI,
     &              mo_G_NEQ,
     &              mo_NEQG,
     &              mo_NEQD,
     &              mo_IGTAB(mo_NVGAMX),
     &              mo_IDTAB(mo_NVDEMX),
     &              mo_JT,
     &              mo_IOPT,
     &              mo_ISTATE,
     &              mo_ITOL,
     &              mo_ITASK
*
*
      DOUBLE PRECISION 
     &       xmo_RSAV,xmo_G_RWORKSAV,xmo_G_RTOLSAV,
     &       xmo_RSAV3,xmo_G_RWORKSAV3,xmo_G_RTOLSAV3,
     &       xmo_RSAV_av,xmo_G_RWORKSAV_av,xmo_G_RTOLSAV_av
      INTEGER mo_ISAV, mo_G_IEQSAV, mo_G_IWORKSAV, 
     &        mo_IGTABSAV, mo_IDTABSAV,
     &        mo_ISAV3, mo_G_IEQSAV3, mo_G_IWORKSAV3, 
     &        mo_IGTABSAV3, mo_IDTABSAV3,
     &        mo_ISAV_av, mo_G_IEQSAV_av, mo_G_IWORKSAV_av, 
     &        mo_IGTABSAV_av, mo_IDTABSAV_av
      COMMON/MOGRRECUP/
     &   xmo_RSAV3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,240),
     &   xmo_G_RWORKSAV3
     &         (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_LWRMAX),
     &   xmo_G_RTOLSAV3
     &         (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NEQMAX),
     &   xmo_RSAV(240),
     &   xmo_G_RWORKSAV(mo_LWRMAX),
     &   xmo_G_RTOLSAV(mo_NEQMAX),
     &   xmo_RSAV_av(240),
     &   xmo_G_RWORKSAV_av(mo_LWRMAX),
     &   xmo_G_RTOLSAV_av(mo_NEQMAX)
      COMMON/MOGIRECUP/
     &  mo_ISAV3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,50),
     &  mo_G_IWORKSAV3
     &       (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_LWIMAX),
     &  mo_G_IEQSAV3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,5),
     &  mo_IGTABSAV3
     &       (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NVGAMX),
     &  mo_IDTABSAV3
     &       (mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,mo_NVDEMX),
     &  mo_ISAV(50),
     &  mo_G_IWORKSAV(mo_LWIMAX),
     &  mo_G_IEQSAV(5),
     &  mo_IGTABSAV(mo_NVGAMX),
     &  mo_IDTABSAV(mo_NVDEMX),
     &  mo_ISAV_av(50),
     &  mo_G_IWORKSAV_av(mo_LWIMAX),
     &  mo_G_IEQSAV_av(5),
     &  mo_IGTABSAV_av(mo_NVGAMX),
     &  mo_IDTABSAV_av(mo_NVDEMX)

