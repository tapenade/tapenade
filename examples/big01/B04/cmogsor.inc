* common des sorties
* fichier cmogsor.inc
************************************************************************
*     ------------------------------------------------------------------
      COMMON /MOGSOR/ 
     &     xmo_SOMTOFIS3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GEPMA3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GONFS3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GASPOR(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_SINPOR(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GCREA3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GINTER3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_GINTRA3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &     xmo_ASE3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX),
     &     xmo_SOMTOFIS(2)
*
*
      COMMON /MOGBIL/ 
     &   xmo_REMISOL3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SORDIF3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SORBUL3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SORPOR3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SORDIFJ3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SORPERC3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_SOREJEC3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXBETAC3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXSORBUL3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXSORPOR3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXREMISOL3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXSOURXE3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXOUTSE3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXOUTPERC3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_TAUXOUTEJEC3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX,2),
     &   xmo_ERRGLOB3(mo_M31MAX,mo_IGRMAX,mo_IFEMAX,mo_NAMAMX),
     &   xmo_TAUXBETAC(2),
     &   xmo_TAUXSORBUL(2),
     &   xmo_TAUXSORPOR(2),
     &   xmo_TAUXREMISOL(2),
     &   xmo_TAUXSOURXE(2),
     &   xmo_TAUXOUTSE(2),
     &   xmo_TAUXOUTPERC(2),
     &   xmo_TAUXOUTEJEC(2),
     &   xmo_GCREE,
     &   xmo_REMISOL,
     &   xmo_SORDIF,
     &   xmo_SORBUL,
     &   xmo_SORPOR,
     &   xmo_SORDIFJ,
     &   xmo_SORPERC,
     &   xmo_SOREJEC,
     &   xmo_ERRINTRA, 
     &   xmo_ERRINTER, 
     &   xmo_ERRGLOB
*     ------------------------------------------------------------------