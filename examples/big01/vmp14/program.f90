
!
!	swe2Dxy.f90
!	Shallow water equations on the unit square
!
!	Created by Mihai on 2/23/08.
!      mihai@vt.edu
!	Copyright 2008. All rights reserved.
!
!      We use the composite scheme for rectangular grids proposed by Liska and Wendroff

module swe2Dxy_Parameters

   public
   !save

   !gravitational acceleration [m/s^2]
   double precision, parameter :: gAcc = 9.80616d0

   !domain boundaries [unit square]
   double precision, parameter :: xMin = 0.d0, xMax = 1.d0
   double precision, parameter :: yMin = 0.d0, yMax = 1.d0

   !number of grid points along each direction
   integer, parameter :: Mx = 75
   integer, parameter :: My = 75

   !grid spacings
   double precision, parameter :: dx = (xMax - xMin) / (Mx - 1)
   double precision, parameter :: dy = (yMax - yMin) / (My - 1)

   !number of time steps
   integer, parameter :: N = 100

   !CFL number
   double precision, parameter :: CFL = 0.6d0

   !time step length
   !will be estimated s.t. the CFL ~ 0.6
   double precision :: dt

!~~> Text file where we store the solution
!    The solution U will be written at every time step
   integer, parameter :: solnFile = 777

!~~> temporary variables (defined on dual grids)
   double precision, allocatable :: tmp1(:,:,:), tmp2(:,:,:), &
                                    tmp3(:,:,:), tmp4(:,:,:)

! !~~> Private (helper) functions
!    private f
!    private g

!~~> End module
end module swe2Dxy_Parameters

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Setup initial conditions
! Compute time step dt based on initial conditions
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine swe2Dxy_InitialConditions(U)

   use swe2Dxy_Parameters
   implicit none

   double precision, intent(inout) :: U(3,Mx,My)
   double precision :: hMax, speed

   intrinsic maxval
   intrinsic dabs
   intrinsic max

!~~> Initialize the grid
   !h(t0,x,y) -> Like a dam breaking problem
   U(1,:,:) = 200.d0;
   U(1,Mx/2-5:Mx/2+5,My/2-5:My/2+5) = 250.d0;

   !u(t0,x,y) = v(t0,x,y) = 0
   U(2,:,:) = 0.d0
   U(3,:,:) = 0.d0

!~~> Compute dt
!~~> Need maximum initial wave speed
   ! max among the eigenvalues of the x and y flux Jacobians
   ! maximum height or depth (in absolute value)
    hMax = maxval(dabs(U(1,:,:)))    !maximum height (or depth)
    speed = sqrt(gAcc*hMax)
!~~> Now compute a suitable time step
    dt = CFL*max(dx,dy)/speed

end subroutine swe2Dxy_InitialConditions


subroutine swe2Dxy_BoundaryConditions(U)

   use swe2Dxy_Parameters
   implicit none

   double precision, intent(inout) :: U(3,Mx,My)

!~~> boundary conditions (reflective)
!~~> h(t,x,y)
    U(1,:,1) = U(1,:,2)
    U(1,1,:) = U(1,2,:)
    U(1,Mx,:) = U(1,Mx-1,:)
    U(1,:,My) = U(1,:,My-1)
!~~> u(t,x,y)
    U(2,:,1) = U(2,:,2)
    U(2,1,:) = -U(2,2,:)
    U(2,Mx,:) = -U(2,Mx-1,:)
    U(2,:,My) = U(2,:,My-1)
!~~> v(t,x,y)
    U(3,:,1) = -U(3,:,2)
    U(3,1,:) = U(3,2,:)
    U(3,Mx,:) = U(3,Mx-1,:)
    U(3,:,My) = -U(3,:,My-1)

end subroutine swe2Dxy_BoundaryConditions

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Solves the SWE equations
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
subroutine swe2Dxy_Solve(U)

   use swe2Dxy_Parameters
   implicit none

!    interface
!       function F(input)
!           double precision, intent(in) :: input(3,Mx-2,My-2)
!           double precision :: F(3,Mx-2,My-2)
!       end function F
! 
!       function G(input)
!           double precision, intent(in) :: input(3,Mx-2,My-2)
!           double precision :: G(3,Mx-2,My-2)
!       end function G
!    end interface

!~~> solution
!    U(1,:,:) = h(t,x,y)
!    U(2,:,:) = u(t,x,y)
!    U(3,:,:) = v(t,x,y)
   double precision, intent(inout) :: U(3,Mx,My)
!~~> time loop counter
   integer :: k
!~~> spatial domain counters
   integer :: i,j
!~~> allocator flag
   integer :: info

   !dt/dx and dt/dy ratios (for convenience)
   double precision :: lambdaX , lambdaY
!~~> Now compute the lambda values
   lambdaX = dt/dx
   lambdaY = dt/dy

   print *, 'Opening solution file...'
   open(unit=solnFile, file='swe2D_U.txt')
   print *, 'Opened solution file for writing: ./swe2D_U.txt'

!~~> Write the initial condition h(t0,x,y) only.
   do j=1,My
      do i=1,Mx
         write (solnFile,*) U(1,i,j)
      end do
   end do

   print *, 'Allocating dual grids...'
!~~> allocate temporary variables (defined on dual grids)
   allocate(tmp1(3,Mx-2,My-2), &
          & tmp2(3,Mx-2,My-2), &
          & tmp3(3,Mx-2,My-2), &
          tmp4(3,Mx-2,My-2), stat=info)
   if (info .ne. 0) then
      	print *, 'Allocation error for dual grids (tmp1 ... tmp4).'
      	close(solnFile)
	stop 1
   end if
   print *, 'Allocated dual grids.'

!~~> time stepping loop
   do k = 1,N

    call swe2Dxy_BoundaryConditions(U)

!~~> Updates along x and y directions
!    i=2,Mx-1
!    j=2,My-1

    tmp1 = 0.25d0 * (U(:,2:Mx-1,2:My-1) + U(:,3:Mx,2:My-1) + U(:,2:Mx-1,3:My) + U(:,3:Mx,3:My))	&
        + 0.5d0 * lambdaX * (F(0.5d0 * (U(:,3:Mx,3:My) + U(:,3:Mx,2:My-1))		&
        + 0.25d0 * lambdaY * (G(U(:,3:Mx,3:My)) - G(U(:,3:Mx,2:My-1))))		&
        - F(0.5d0*(U(:,2:Mx-1,3:My) + U(:,2:Mx-1,2:My-1))					&
        + 0.25d0 * lambdaY * (G(U(:,2:Mx-1,3:My)) - G(U(:,2:Mx-1,2:My-1)))))		&
        + 0.5d0 * lambdaY * (G(0.5d0*(U(:,3:Mx,3:My) + U(:,2:Mx-1,3:My))		&
        + 0.25d0 * lambdaX * (F(U(:,3:Mx,3:My)) - F(U(:,2:Mx-1,3:My))))		&
        - G(0.5d0*(U(:,3:Mx,2:My-1) + U(:,2:Mx-1,2:My-1))					&
        + 0.25d0 * lambdaY * (F(U(:,3:Mx,2:My-1)) - F(U(:,2:Mx-1,2:My-1)))));

    tmp2 = 0.25d0 *(U(:,2:Mx-1,1:My-2) + U(:,3:Mx,1:My-2) + U(:,2:Mx-1,2:My-1) + U(:,3:Mx,2:My-1))	&
        + 0.5d0 *lambdaX * (F(0.5d0 * (U(:,3:Mx,2:My-1) + U(:,3:Mx,1:My-2))		&
        + 0.25d0 *lambdaY * (G(U(:,3:Mx,2:My-1)) - G(U(:,3:Mx,1:My-2))))		&
        - F(0.5d0 *(U(:,2:Mx-1,2:My-1) + U(:,2:Mx-1,1:My-2))					&
        + 0.25d0 *lambdaY * (G(U(:,2:Mx-1,2:My-1)) - G(U(:,2:Mx-1,1:My-2)))))		&
        + lambdaY/2 * (G(0.5d0 * (U(:,3:Mx,2:My-1) + U(:,2:Mx-1,2:My-1))			&
        + 0.25d0 *lambdaX * (F(U(:,3:Mx,2:My-1)) - F(U(:,2:Mx-1,2:My-1))))			&
        - G(0.5d0 *(U(:,3:Mx,1:My-2) + U(:,2:Mx-1,1:My-2))				&
        + 0.25d0 *lambdaX * (F(U(:,3:Mx,1:My-2)) - F(U(:,2:Mx-1,1:My-2)))));

    tmp3 = 0.25d0 *(U(:,1:Mx-2,2:Mx-1) + U(:,2:Mx-1,2:My-1) + U(:,1:Mx-2,3:My) + U(:,2:Mx-1,3:My))	&
        + 0.5d0 *lambdaX * (F(0.5d0 *(U(:,2:Mx-1,3:My) + U(:,2:Mx-1,2:My-1))			&
        + 0.25d0 *lambdaX * (G(U(:,2:Mx-1,3:My)) - G(U(:,2:Mx-1,2:My-1))))			&
        - F(0.5d0 *(U(:,1:Mx-2,3:My) + U(:,1:Mx-2,2:My-1))				&
        + 0.25d0 *lambdaY * (G(U(:,1:Mx-2,3:My)) - G(U(:,1:Mx-2,2:My-1)))))		&
        + 0.5d0 *lambdaY * (G(0.5d0 *(U(:,2:Mx-1,3:My) + U(:,1:Mx-2,3:My))		&
        + 0.25d0 *lambdaX * (F(U(:,2:Mx-1,3:My)) - F(U(:,1:Mx-2,3:My))))		&
        - G(0.5d0 *(U(:,2:Mx-1,2:My-1) + U(:,1:Mx-2,2:My-1))					&
        + 0.25d0 *lambdaX * (F(U(:,2:Mx-1,2:My-1)) - F(U(:,1:Mx-2,2:My-1)))));

    tmp4 = 0.25d0 *(U(:,1:Mx-2,1:My-2) + U(:,2:Mx-1,1:My-2) + U(:,1:Mx-2,2:My-1) + U(:,2:Mx-1,2:My-1))	&
        + 0.5d0 *lambdaX * (F(0.5d0 *(U(:,2:Mx-1,2:My-1) + U(:,2:Mx-1,1:My-2))			&
        + 0.25d0 *lambdaY * (G(U(:,2:Mx-1,2:My-1)) - G(U(:,2:Mx-1,1:My-2))))			&
        - F(0.5d0 *(U(:,1:Mx-2,2:My-1) + U(:,1:Mx-2,1:My-2))				        &
        + 0.25d0 *lambdaY * (G(U(:,1:Mx-2,2:My-1)) - G(U(:,1:Mx-2,1:My-2)))))		        &
        + 0.5d0 *lambdaY * (G(0.5d0 *(U(:,2:Mx-1,2:My-1) + U(:,1:Mx-2,2:My-1))			&
        + 0.25d0 *lambdaX * (F(U(:,2:Mx-1,2:My-1)) - F(U(:,1:Mx-2,2:My-1))))			&
        - G(0.5d0 *(U(:,2:Mx-1,1:My-2) + U(:,1:Mx-2,1:My-2))				        &
        + 0.25d0 *lambdaX * (F(U(:,2:Mx-1,1:My-2)) - F(U(:,1:Mx-2,1:My-2)))));

    U(:,2:Mx-1,2:My-1) = U(:,2:Mx-1,2:My-1)				&
        + lambdaX/2.d0 * (F(tmp1) + F(tmp2) - F(tmp3) - F(tmp4))	&
        + lambdaY/2.d0 * (G(tmp1) + G(tmp3) - G(tmp2) - G(tmp4));

!~~> Write the solution file
    do j=1,My
      do i=1,Mx
         !Write only h(t,x,y) (we don't visualize u or v)
         write (solnFile,*) U(1,i,j)
      end do
    end do 

    write(*,*) 'Step ', k, ' done.'

!~~> end time stepping loop
   end do

!~~> deallocate temp variables for dual grids
   deallocate(tmp1, tmp2, tmp3, tmp4, stat=info)
   if (info .ne. 0) then
      	print *, 'Deallocation error for dual grids (tmp1 ... tmp4).'
        close(solnFile)
	stop 1
   end if

!~~> Cleanup: close the solution file
   close(solnFile)

   CONTAINS

   function F(U1)

       use swe2Dxy_parameters
       implicit none
       double precision, intent(in) :: U1(3,Mx-2,My-2)
       double precision :: F(3,Mx-2,My-2)

       !computes F(U1) given U1
       F(1,:,:) = U1(2,:,:)
       F(2,:,:) = U1(2,:,:)**2 / U1(1,:,:) + 0.5d0 *gAcc*U1(1,:,:)**2
       F(3,:,:) = U1(2,:,:)*U1(3,:,:) / U1(1,:,:)

   end function F


   function G(U1)

       use swe2Dxy_parameters
       implicit none
       double precision, intent(in) :: U1(3,Mx-2,My-2)
       double precision :: G(3,Mx-2,My-2)

       !computes G(U1) given U1
       G(1,:,:) = U1(3,:,:)
       G(2,:,:) = U1(2,:,:)*U1(3,:,:) / U1(1,:,:)
       G(3,:,:) = U1(3,:,:)**2 / U1(1,:,:) + 0.5d0*gAcc*U1(1,:,:)**2

   end function G

end subroutine swe2Dxy_Solve

!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
! Main program
!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
program swe2Dxy_main

   use swe2Dxy_parameters
   implicit none

!~~> (primal) Grid:
   ! U(1,:,:) = h(x,y)
   ! U(2,:,:) = u(x,y)
   ! U(3,:,:) = v(x,y)
   double precision, allocatable :: U(:,:,:)

!~~> Allocation/deallocation error flag
   integer info

!~~> Allocate U = U(3,Mx,My)
   allocate(U(3,Mx,My), stat=info)
   if (info .ne. 0) then
	print *, 'Allocation error. Could not allocate U. Exiting...'
	stop 1
   end if

!~~> Setup initial conditions and compute time step
   print *, 'Setting up initial conditions...'
   call swe2Dxy_InitialConditions(U)
   print *, 'Initial conditions set up.'

!~~> Solve the SWE equations
   print *, 'Solving the SWE PDEs...'
   call swe2Dxy_Solve(U)
   print *, 'Solved the SWE PDEs.'

!~~> Deallocate U
   deallocate(U, stat=info)
   if (info .ne. 0) then
	print *, 'Deallocation error. Could not deallocate U. Exiting...'
	stop 1
   end if

end program swe2Dxy_main
