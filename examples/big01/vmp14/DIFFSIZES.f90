module diffsizes
     integer, parameter :: ISIZE1OFtmp1 = 4
     integer, parameter :: ISIZE1OFtmp2 = 4
     integer, parameter :: ISIZE1OFtmp3 = 4
     integer, parameter :: ISIZE1OFtmp4 = 4
     integer, parameter :: ISIZE2OFtmp1 = 4
     integer, parameter :: ISIZE2OFtmp2 = 4
     integer, parameter :: ISIZE2OFtmp3 = 4
     integer, parameter :: ISIZE2OFtmp4 = 4
     integer, parameter :: ISIZE3OFtmp1 = 4
     integer, parameter :: ISIZE3OFtmp2 = 4
     integer, parameter :: ISIZE3OFtmp3 = 4
     integer, parameter :: ISIZE3OFtmp4 = 4
     integer, parameter :: ISIZE1OFDrftmp1 = 4
     integer, parameter :: ISIZE1OFDrftmp2 = 4
     integer, parameter :: ISIZE1OFDrftmp3 = 4
     integer, parameter :: ISIZE1OFDrftmp4 = 4
     integer, parameter :: ISIZE2OFDrftmp1 = 4
     integer, parameter :: ISIZE2OFDrftmp2 = 4
     integer, parameter :: ISIZE2OFDrftmp3 = 4
     integer, parameter :: ISIZE2OFDrftmp4 = 4
     integer, parameter :: ISIZE3OFDrftmp1 = 4
     integer, parameter :: ISIZE3OFDrftmp2 = 4
     integer, parameter :: ISIZE3OFDrftmp3 = 4
     integer, parameter :: ISIZE3OFDrftmp4 = 4

end module diffsizes
