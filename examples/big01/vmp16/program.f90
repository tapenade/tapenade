!-------------------------------------------------------------------------------


! *** simtest1.f90 ***

MODULE simtest1
IMPLICIT NONE

TYPE CrType
	REAL*8 :: &
    F,     &      ! phase ratio
    FDFac, &      ! used in numerical calculation
    thres         ! used when calculating presolution
  REAL*8, DIMENSION(:), ALLOCATABLE :: &
    Cmob          !  initial concentration i mobile phase
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: &
    Cinj          ! boundary conditions
  INTEGER*4 :: & 
		Ncomp  , &    ! number of components in sample
		isoterm, &    ! id number of isoterm type
    Nx     , &    ! number of spacesteps
    Nt            ! number of timesteps
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: &
		isodat        ! real isoterm data (parameters)
  COMPLEX*16, DIMENSION(:, :), ALLOCATABLE :: &
		isodatIm      ! complex isoterm data (parameters)
END TYPE CrType


PRIVATE :: &
	qCalc

PUBLIC :: &
  preSolve    , &
	solveReal   , &
  solveComplex, &
  solveRealFull

CONTAINS


! solve
! Solves PDE with correct accuracy
SUBROUTINE solveReal(this, C, indexLim)
	
  IMPLICIT NONE

  ! PARAMETERS
  TYPE(CrType)               , INTENT(in)           :: this
  REAL*8, DIMENSION(0 :, :)  , INTENT(out)          :: C
  REAL*8, DIMENSION(0 :, :)  , INTENT(in), OPTIONAL :: indexLim 

  ! VARIABLES
  INTEGER                            ::	allocstat, j, n
  REAL(8), DIMENSION(:), ALLOCATABLE :: qStat, qNew, qOld, CNew, COld
	 
  ALLOCATE(qStat(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qOld(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( CNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( COld(1 : this%Ncomp), stat = allocstat)
  
	C     = this%Cinj(0:this%Nt, 1:this%Ncomp)
  qStat = qCalc(this, this%Cmob)              ! stat. phase conc at t = 0
  

  IF (PRESENT(indexLim)) THEN
    
    space_integration1: DO j = 1, this%Nx
		  CNew = this%Cmob 
		  qNew = qStat
    
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalc(this, CNew)
			  C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		  END DO time_integration1
	
    END DO space_integration1

  ELSE

    space_integration2: DO j = 1, this%Nx
		  CNew = this%Cmob 
		  qNew = qStat
    
      time_integration2: DO n = 0, this%Nt
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalc(this, CNew)
			  C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		  END DO time_integration2
	
    END DO space_integration2

  ENDIF


  DEALLOCATE(qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE solveReal



! solve
! Solves PDE with correct accuracy for complex isotherm parameters
SUBROUTINE solveComplex(this, C, indexLim)
	
  IMPLICIT NONE

  ! PARAMETERS
  TYPE(CrType)                 , INTENT(in)           :: this
  COMPLEX*16, DIMENSION(0 :, :), INTENT(out)          :: C
  REAL*8    , DIMENSION(0 :, :), INTENT(in), OPTIONAL :: indexLim 

  ! VARIABLES
  INTEGER*4                             :: allocstat, j, n
  COMPLEX*16, DIMENSION(:), ALLOCATABLE :: CmobIm, qStat, qNew, qOld, CNew, COld
  COMPLEX*16                            :: FDFacIm, FIm
	 
  ALLOCATE(CmobIm(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qStat(1 : this%Ncomp), stat = allocstat)
  ALLOCATE(  qNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE(  qOld(1 : this%Ncomp), stat = allocstat)
  ALLOCATE(  CNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE(  COld(1 : this%Ncomp), stat = allocstat)
  
  ! Explicit conversion, maybe not needed
  FDFacIm = CMPLX(this%FDFac, 0.0, kind = 8)
  FIm     = CMPLX(this%F, 0.0, kind = 8)
	C       = CMPLX(this%Cinj(0:this%Nt, 1:this%Ncomp), 0.0, kind = 8)
  CmobIm  = CMPLX(this%Cmob, 0.0, kind = 8)

  qStat   = qCalcComplex(this, CmobIm)     ! stat. phase conc at t = 0
  

  IF (PRESENT(indexLim)) THEN
    
    space_integration1: DO j = 1, this%Nx
		  CNew = CmobIm 
		  qNew = qStat
    
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalcComplex(this, CNew)
			  C(n, :) = CNew - FDFacIm*( CNew - COld + FIm*(qNew - qOld) )
		  END DO time_integration1
	
    END DO space_integration1

  ELSE

    space_integration2: DO j = 1, this%Nx
		  CNew = CmobIm 
		  qNew = qStat
    
      time_integration2: DO n = 0, this%Nt
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalcComplex(this, CNew)
			  C(n, :) = CNew - FDFacIm*( CNew - COld + FIm*(qNew - qOld) ) 
		  END DO time_integration2
	
    END DO space_integration2

  ENDIF


  DEALLOCATE(CmobIm, qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE solveComplex



SUBROUTINE solveRealFull(this, Cfull, indexLim)
	
  IMPLICIT NONE

  ! PARAMETERS
  TYPE(CrType)                 , INTENT(in)           :: this
  REAL*8, DIMENSION(0:, 0 :, :), INTENT(out)          :: Cfull
  REAL*8, DIMENSION(0 :, :)    , INTENT(in), OPTIONAL :: indexLim 

  ! VARIABLES
  INTEGER*4                            :: allocstat, j, n
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: C
  REAL*8, DIMENSION(:)   , ALLOCATABLE :: qStat, qNew, qOld, CNew, COld
	 
  ALLOCATE(C(0:this%Nt, 1:this%Ncomp), stat = allocstat)
  ALLOCATE(qStat(1 : this%Ncomp)     , stat = allocstat)
  ALLOCATE( qNew(1 : this%Ncomp)     , stat = allocstat)
  ALLOCATE( qOld(1 : this%Ncomp)     , stat = allocstat)
  ALLOCATE( CNew(1 : this%Ncomp)     , stat = allocstat)
  ALLOCATE( COld(1 : this%Ncomp)     , stat = allocstat)
  
	C              = this%Cinj(0:this%Nt, 1:this%Ncomp)
  Cfull(0, :, :) = C
  qStat = qCalc(this, this%Cmob)              ! stat. phase conc at t = 0
  

  IF (PRESENT(indexLim)) THEN
    
    space_integration1: DO j = 1, this%Nx
		  CNew = this%Cmob 
		  qNew = qStat
    
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalc(this, CNew)
			  C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		  END DO time_integration1
      Cfull(j, :, :) = C 
	
    END DO space_integration1

  ELSE

    space_integration2: DO j = 1, this%Nx
		  CNew = this%Cmob 
		  qNew = qStat
    
      time_integration2: DO n = 0, this%Nt
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalc(this, CNew)
			  C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		  END DO time_integration2
      Cfull(j, :, :) = C 
	
    END DO space_integration2

  END IF


  DEALLOCATE(C, qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE solveRealFull



SUBROUTINE solveComplexFull(this, Cfull, indexLim)
	
  IMPLICIT NONE

  ! PARAMETERS
  TYPE(CrType)                     , INTENT(in)           :: this
  COMPLEX*16, DIMENSION(0:, 0 :, :), INTENT(out)          :: Cfull
  REAL*8    , DIMENSION(0 :, :)    , INTENT(in), OPTIONAL :: indexLim 

  ! VARIABLES
  INTEGER*4                                :: allocstat, j, n
  COMPLEX*16, DIMENSION(:, :), ALLOCATABLE :: C
  COMPLEX*16, DIMENSION(:)   , ALLOCATABLE :: CmobIm, qStat, qNew, qOld, CNew, &
                                              COld
  COMPLEX*16                               :: FDFacIm, FIm
	 
  ALLOCATE(C(0:this%Nt, 1:this%Ncomp), stat = allocstat)
  ALLOCATE(CmobIm(1 : this%Ncomp)    , stat = allocstat)
  ALLOCATE( qStat(1 : this%Ncomp)    , stat = allocstat)
  ALLOCATE(  qNew(1 : this%Ncomp)    , stat = allocstat)
  ALLOCATE(  qOld(1 : this%Ncomp)    , stat = allocstat)
  ALLOCATE(  CNew(1 : this%Ncomp)    , stat = allocstat)
  ALLOCATE(  COld(1 : this%Ncomp)    , stat = allocstat)
  
	! Explicit conversion, maybe not needed
  FDFacIm = CMPLX(this%FDFac, 0.0, kind = 8)
  FIm     = CMPLX(this%F, 0.0, kind = 8)
  CmobIm  = CMPLX(this%Cmob, 0.0, kind = 8)
	C       = CMPLX(this%Cinj(0:this%Nt, 1:this%Ncomp), 0.0, kind = 8)
  
  Cfull(0, :, :) = C
  qStat = qCalcComplex(this, CmobIm)              ! stat. phase conc at t = 0
  

  IF (PRESENT(indexLim)) THEN
    
    space_integration1: DO j = 1, this%Nx
		  CNew = CmobIm 
		  qNew = qStat
    
      time_integration1: DO n = INT(indexLim(j, 1)), INT(indexLim(j, 2))
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalcComplex(this, CNew)
			  C(n, :) = CNew - FDFacIm*( CNew - COld + FIm*(qNew - qOld) ) 
		  END DO time_integration1
      Cfull(j, :, :) = C 
	
    END DO space_integration1

  ELSE

    space_integration2: DO j = 1, this%Nx
		  CNew = CmobIm 
		  qNew = qStat
    
      time_integration2: DO n = 0, this%Nt
		    COld    = CNew
			  CNew    = C(n, :)
			  qOld    = qNew
			  qNew    = qCalcComplex(this, CNew)
			  C(n, :) = CNew - FDFacIm*( CNew - COld + FIm*(qNew - qOld) ) 
		  END DO time_integration2
      Cfull(j, :, :) = C 
	
    END DO space_integration2

  END IF


  DEALLOCATE(CmobIm, C, qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE solveComplexFull



! presolve
SUBROUTINE preSolve(this, indexLim)
	
  IMPLICIT NONE

  ! PARAMETERS
  TYPE(CrType)              , INTENT(in)  :: this
  REAL*8 , DIMENSION(0 :, :), INTENT(out) :: indexLim 

  ! VARIABLES
  INTEGER*4                            :: allocstat, j, n
  REAL*8, DIMENSION(:)   , ALLOCATABLE :: qStat, qNew, qOld, CNew, COld
  REAL*8, DIMENSION(:, :), ALLOCATABLE :: C

  ALLOCATE(C(0:this%Nt, 1:this%Ncomp), stat = allocstat)
  ALLOCATE(qStat(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( qOld(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( CNew(1 : this%Ncomp), stat = allocstat)
  ALLOCATE( COld(1 : this%Ncomp), stat = allocstat)
  
  C     = this%Cinj(0:this%Nt, 1:this%Ncomp)
  qStat = qCalc(this, this%Cmob)              ! stat. phase conc at t = 0

  ! Calculate indexlimits for injection
  indexLim(0, 1) = 0
  DO n = 0, this%Nt
    IF ( MAXVAL(ABS(C(n, :) - this%Cmob)) .GT. this%thres) THEN
      indexLim(0, 1) = MAX(0, n-1)
      EXIT
    ENDIF
  ENDDO
  indexLim(0, 2) = this%Nt
  DO n = this%Nt, 0, -1
    IF ( MAXVAL(ABS(C(n, :) - this%Cmob)) .GT. this%thres) THEN
      indexLim(0, 2) = MIN(this%Nt, n+1)
      EXIT
    ENDIF
  ENDDO
  

  space_integration: DO j = 1, this%Nx
		CNew = this%Cmob 
		qNew = qStat
    
    time_integration: DO n = 0, this%Nt
		    COld    = CNew
			CNew    = C(n, :)
			qOld    = qNew
			qNew    = qCalc(this, CNew)
			C(n, :) = CNew - this%FDFac*( CNew - COld + this%F*(qNew - qOld) ) 
		END DO time_integration

    ! Calculate indexlimits
    indexLim(j, 1) = 0
    DO n = 0, this%Nt
      IF ( MAXVAL(ABS(C(n, :) - this%Cmob)) .GT. this%thres) THEN
        indexLim(j, 1) = MAX(0, n-1)
        EXIT
      ENDIF
    ENDDO
    indexLim(j, 2) = this%Nt
    DO n = this%Nt, 0, -1
      IF ( MAXVAL(ABS(C(n, :) - this%Cmob)) .GT. this%thres) THEN
        indexLim(j, 2) = MIN(this%Nt, n+1)
        EXIT
      ENDIF
    ENDDO
	
  END DO space_integration


  DEALLOCATE(C, qStat, qNew, qOld, CNew, COld, stat = allocstat)
	    
END SUBROUTINE preSolve



!-------------------------------------------------------------------------------
! qCalc
! calculate the concentration q in the stationary phase    
FUNCTION qCalc(this, Cmob)

  TYPE(CrType), INTENT(in)               :: this
  REAL*8, DIMENSION(:), INTENT(in)       :: Cmob
	REAL*8, DIMENSION(UBOUND(Cmob, DIM=1)) :: qCalc
	
  REAL*8, DIMENSION(1:2) :: P
	REAL*8	               :: L, Q, temp
  INTEGER*4              :: i, Nsites, Ncomp

  
  SELECT CASE (this%isoterm)

	
	CASE (1)	
    ! Linear. ncomp>=1. 1 param/comp
		qCalc = this%isodat(:, 1) * Cmob


	CASE (2)	
    ! Non-competitive single site Langmuir. ncomp>=1.  2 param/comp 	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob)

    
	CASE (3)	
    ! Competitive single site Langmuir. ncomp>=1.  2 param/comp 
		! Capacity = p1/p2 should be equal for all comps
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob))


	CASE (4)	
    ! Competitive Levan-Vermeulen. ncomp = 2. single site. 2 param/comp	
		! Same parameters as comp Langmuir. The two components are now allowed 
    ! to have dIFferent capacies
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc=0
		ELSE
			P    = this%isodat(:,2) * Cmob
			temp = 1.0 + SUM(P)
			Q    = (this%isodat(1,1) * Cmob(1) + &
        this%isodat(2, 1) * Cmob(2)) / (temp - 1.0) ! Weighted monolayer capac.   
			L    = (this%isodat(1, 1) / this%isodat(1, 2) - &
        this%isodat(2, 1) / this%isodat(2, 2)) * &
			  P(1) * P(2) * LOG(temp) / (temp - 1.0)**2   ! Correction term
			qCalc(1) = Q * P(1) / temp + L
			qCalc(2) = Q * P(2) / temp - L
		END IF


  CASE (5)	
    ! Non-competitive bi-Langmuir. ncomp>=1	. 4 param/comp
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob) + &
			this%isodat(:, 3) * Cmob / (1.0 + this%isodat(:, 4) * Cmob)


  CASE (6)
    ! Competitive bi-Langmuir ncomp>=1. 4 param/comp
		! capacity1=param 1 / param 2 should be equal for all components and
		! capacity2=param 3 / param 4 should be equal for all components for 
    ! thermodynamic consistency
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob)) + &
      this%isodat(:, 3) * Cmob / (1.0 + SUM(this%isodat(:, 4) * Cmob))


  CASE (24)
    ! Generalized competitive bi-Langmuir ncomp>=1. 2*Ncomp + 2 param/comp
    Ncomp = SIZE(this%isodat, 1)
    DO i = 1, Ncomp 
		  qCalc(i) = &
        this%isodat(i, 1) * Cmob(i) / &
        (1.0 + SUM(this%isodat(i, 2 : Ncomp + 1) * Cmob)) + &
        this%isodat(i, Ncomp + 2) * Cmob(i) / &
        (1.0 + SUM(this%isodat(i, Ncomp + 3 : 2*Ncomp + 2) * Cmob))
    END DO


  CASE (21)	
    ! Non-competitive tri-Langmuir. ncomp>=1. 6 param/comp
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + this%isodat(:, 2) * Cmob) + &
			this%isodat(:, 3) * Cmob / (1.0 + this%isodat(:, 4) * Cmob) + &
      this%isodat(:, 5) * Cmob / (1.0 + this%isodat(:, 6) * Cmob)


  CASE (22)
    ! Competitive tri-Langmuir ncomp>=1. 6 param/comp
		! capacity1=param 1 / param 2 should be equal for all components and
		! capacity2=param 3 / param 4 should be equal for all components and 
    ! capacity3=param 5 / param 6 should be equal for all components for
    ! thermodynamic consistency
		qCalc = &
      this%isodat(:, 1) * Cmob / (1.0 + SUM(this%isodat(:, 2) * Cmob)) + &
      this%isodat(:, 3) * Cmob / (1.0 + SUM(this%isodat(:, 4) * Cmob)) + &
      this%isodat(:, 5) * Cmob / (1.0 + SUM(this%isodat(:, 6) * Cmob))


  CASE (7)
    ! Non-competitive n-Langmuir ncomp>=1. 2*n param/comp
		! capacity(n) = param n / param n+1 should be equal for all components and 
    ! thermodynamic consistency
    Nsites = (SIZE(this%isodat, 2))/2
    qCalc = 0.0
    DO i = 0, Nsites - 1
		  qCalc = qCalc + this%isodat(:,  2*i + 1) * Cmob /  & 
        (1.0 + this%isodat(:, 2*i + 2) * Cmob)
    END DO


  CASE (20)
    ! Competitive n-Langmuir ncomp>=1. 2*n param/comp
		! capacity(n) = param n / param n+1 should be equal for all components and 
    ! thermodynamic consistency
    Nsites = (SIZE(this%isodat, 2))/2
    qCalc = 0.0
    DO i = 0, Nsites - 1
		  qCalc = qCalc + this%isodat(:,  2*i + 1) * Cmob /  & 
        (1.0 + SUM(this%isodat(:, 2*i + 2) * Cmob))
    END DO


	CASE (8) ! 7	
    ! non-competitive Unilan, ncomp>=1. 3 param/comp
		! p1 capacity	
		qCalc = ( this%isodat(:, 1) / (2*this%isodat(:, 2))) * &
      LOG( (1 + this%isodat(:, 3) * Cmob * EXP(this%isodat(:, 2))) /&
			(1.0 + this%isodat(:, 3) * Cmob * EXP(-this%isodat(:, 2))) )

	
	CASE (9)	
    ! Single component Moreau single site. ncomp=1. 3 param/comp
		! Takes adsorbate-adsorbate interactions into account p1 capacity
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) )


	CASE (10)	
    ! Single component bi-Moreau. ncomp=1. 6 param/comp 
		! Takes adsorbate-adsorbate interactions into account
		! p1, p4 site 1 and 2 capacities
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * ((this%isodat(:, 2) * Cmob)**2) ) + &
			this%isodat(:, 4) * (this%isodat(:, 5) * Cmob + this%isodat(:, 6) * &
      ((this%isodat(:, 5) * Cmob)**2) ) / (1.0 + 2*this%isodat(:, 5) * Cmob + &
      this%isodat(:, 6) * ((this%isodat(:, 5) * Cmob)**2) )

 
	CASE (11)	
    ! Single Statistical isotherm( Quadratic). ncomp=1. 3 param/comp
		! param 1,2,3 corresponds to param 1,2,4 in binary statistical isotherm
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob**2) / (1 + this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob**2)


	CASE (12)	
    ! Binary Statistical isotherm (Quadratic) (Ruthven, Goddard). ncomp= 2. 
    ! 4 param/comp, p3 MUST be equal for both comps
		! p1 =capacity should be equal for both comps (for thermodynamic consistency)	
		qCalc = this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 4) * Cmob**2) / &
			(1.0 + this%isodat(1, 2) * Cmob(1) + this%isodat(2, 2) * Cmob(2) + &
      this%isodat(1, 3) * Cmob(1) * Cmob(2) + this%isodat(1, 4) * Cmob(1)**2 + &
      this%isodat(2, 4) * Cmob(2)**2)				

	CASE (23)	
    ! Two site binary Statistical isotherm (Quadratic). 
    ! ncomp= 2. 8 param/comp, p3, p7 MUST be equal for both comps
    ! p1, p4 (capacity) should be equal for both comps (for thermodynamic 
    ! consistency)
		qCalc = &
      this%isodat(:, 1) * ( this%isodat(:, 2) * Cmob + &
      this%isodat(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 4) * Cmob**2) / &
			(1.0 + this%isodat(1, 2) * Cmob(1) + this%isodat(2, 2) * Cmob(2) + &
      this%isodat(1, 3) * Cmob(1) * Cmob(2) + this%isodat(1, 4) * Cmob(1)**2 + &
      this%isodat(2, 4) * Cmob(2)**2) + &
      this%isodat(:, 5) * ( this%isodat(:, 6) * Cmob + &
      this%isodat(:, 7) * Cmob(1) * Cmob(2) + 2*this%isodat(:, 8) * Cmob**2) / &
			(1.0 + this%isodat(1, 6) * Cmob(1) + this%isodat(2, 6) * Cmob(2) + &
      this%isodat(1, 7) * Cmob(1) * Cmob(2) + this%isodat(1, 8) * Cmob(1)**2 + &
      this%isodat(2, 8) * Cmob(2)**2)

		
	CASE (13)	
    ! Competitive single site Toth. ncomp>=1. 3 param/comp
		! p3 (heterogenity constant should be equal for all components	
		qCalc = this%isodat(:, 1) * Cmob / ( 1.0 + (SUM(this%isodat(:,2) * Cmob)** &
      (this%isodat(:, 3))))**(1.0/this%isodat(:, 3) )

	
	CASE (14)	
    ! Competitive bi-Toth. ncomp >= 1. 6 param/comp
		! param 3 (heterogenity constant should be equal for all components
		! param 6 (heterogenity constant should be equal for all components	
		qCalc = this%isodat(:, 1) * Cmob / (1.0 + &
      (SUM(this%isodat(:, 2) * Cmob)**(this%isodat(:, 3))))** &
      (1.0 / this%isodat(:, 3)) + this%isodat(:, 4) * Cmob / &
      (1.0 + (SUM(this%isodat(:, 5) * Cmob)**(this%isodat(:, 6))))** &
      (1.0 / this%isodat(:, 6))	

	
	CASE(15)	
    ! Non-competitive Freundlich. ncomp>=1.  2 param/comp
		! p2 heterogenity const
		! p1 corresponds to langmuir a term (possibly) 
		qCalc = this%isodat(:, 1) * Cmob**(this%isodat(:, 2))
	
		
	CASE (16)	
    ! Competitive Jovanovic-Freundlich. ncomp>=1. 3 param/comp
		! p1 capacity
		! p3 (heterogenity const) should be equal for all comps
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc = 0.0
		ELSE
		  temp  = SUM( this%isodat(:,2) * Cmob )
		  qCalc = this%isodat(:, 1) * this%isodat(:, 2) * Cmob * ( 1.0 - &
        EXP( -(temp**(this%isodat(:, 3)))) ) / temp
		END IF

	
	CASE (17)	
    ! Competitive Jovanovic ncomp>=1. 2 param/comp
		! Homogenous version of Jovanovic-Freundlich isotherm
		! p1 capacity
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalc = 0.0
		ELSE
		  temp  = SUM( this%isodat(:, 2) * Cmob )
		  qCalc = this%isodat(:, 1) * this%isodat(:, 2) * Cmob * ( 1.0 - &
        EXP(-temp) ) / temp
		END IF


	CASE (18) 
    ! Competitive Freundlich-Langmuir. ncomp>=1. 3 param/comp
		! here the heterogenity constants (param 2) don't have to be equal
		! p1 capacity
		qCalc = ( this%isodat(:, 1)* this%isodat(:, 2)*Cmob** &
      (this%isodat(:, 3)) ) / ( 1.0 + SUM(this%isodat(:,2)*Cmob** &
      (this%isodat(:, 3))) ) 
	
	CASE (19) 
    ! Competitive Jaroniec-Freundlich-Langmuir. ncomp>=1. 3 param/comp
		! p1 capacity (probably) 
		! p3 heterogenity constant should be equal for all comp
		temp  = SUM(this%isodat(:, 2) * Cmob)
		qCalc = ( this%isodat(:,1) * Cmob / temp ) * &
      ( (temp**(this%isodat(:,3))) / (1.0 + temp**(this%isodat(:, 3))) ) 


	CASE DEFAULT
    STOP

 
  END SELECT


END FUNCTION qCalc    
  


!-------------------------------------------------------------------------------
! qCalcComplex
! calculate the concentration q in the stationary phase for complex isotherm
! parameters    
FUNCTION qCalcComplex(this, Cmob)

  TYPE(CrType), INTENT(in)                    :: this
  COMPLEX*16 , DIMENSION(:), INTENT(in)       :: Cmob
	COMPLEX*16 , DIMENSION(UBOUND(Cmob, DIM=1)) :: qCalcComplex
	
  COMPLEX*16, DIMENSION(1:2) :: P
	COMPLEX*16                 :: L, Q, temp
  INTEGER*4                  :: i, Nsites, Ncomp

  
  SELECT CASE (this%isoterm)

	
	CASE (1)	
    ! Linear. ncomp>=1. 1 param/comp
		qCalcComplex = this%isodatIm(:, 1) * Cmob


	CASE (2)	
    ! Non-competitive single site Langmuir. ncomp>=1.  2 param/comp 	
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + &
      this%isodatIm(:, 2) * Cmob)

    
	CASE (3)	
    ! Competitive single site Langmuir. ncomp>=1.  2 param/comp 
		! Capacity = p1/p2 should be equal for all comps
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + &
      SUM(this%isodatIm(:, 2) * Cmob))


	CASE (4)	
    ! Competitive Levan-Vermeulen. ncomp = 2. single site. 2 param/comp	
		! Same parameters as comp Langmuir. The two components are now allowed 
    ! to have dIFferent capacies
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalcComplex=0
		ELSE
			P    = this%isodatIm(:,2) * Cmob
			temp = 1.0 + SUM(P)
			Q    = (this%isodatIm(1,1) * Cmob(1) + &
        this%isodatIm(2, 1) * Cmob(2)) / (temp - 1.0) ! Weighted monolayer cap1.   
			L    = (this%isodatIm(1, 1) / this%isodatIm(1, 2) - &
        this%isodatIm(2, 1) / this%isodatIm(2, 2)) * &
			  P(1) * P(2) * LOG(temp) / (temp - 1.0)**2     ! Correction term
			qCalcComplex(1) = Q * P(1) / temp + L
			qCalcComplex(2) = Q * P(2) / temp - L
		END IF


  CASE (5)	
    ! Non-competitive bi-Langmuir. ncomp>=1	. 4 param/comp
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + this%isodatIm(:, 2) * &
      Cmob) + this%isodatIm(:, 3) * Cmob / (1.0 + this%isodatIm(:, 4) * Cmob)


  CASE (6)
    ! Competitive bi-Langmuir ncomp>=1. 4 param/comp
		! capacity1=param 1 / param 2 should be equal for all components and
		! capacity2=param 3 / param 4 should be equal for all components for 
    ! thermodynamic consistency
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + & 
      SUM(this%isodatIm(:, 2) * Cmob)) + this%isodatIm(:, 3) * Cmob / &
      (1.0 + SUM(this%isodatIm(:, 4) * Cmob))

 
  CASE (24)
    ! Generalized competitive bi-Langmuir ncomp>=1. 2*Ncomp + 2 param/comp
    Ncomp = SIZE(this%isodatIm, 1)
    DO i = 1, Ncomp 
		  qCalcComplex(i) = &
        this%isodatIm(i, 1) * Cmob(i) / &
        (1.0 + SUM(this%isodatIm(i, 2 : Ncomp + 1) * Cmob)) + &
        this%isodatIm(i, Ncomp + 2) * Cmob(i) / &
        (1.0 + SUM(this%isodatIm(i, Ncomp + 3 : 2*Ncomp + 2) * Cmob))
    END DO


  CASE (21)	
    ! Non-competitive tri-Langmuir. ncomp>=1. 6 param/comp
		qCalcComplex = &
      this%isodatIm(:, 1) * Cmob / (1.0 + this%isodatIm(:, 2) * Cmob) + &
			this%isodatIm(:, 3) * Cmob / (1.0 + this%isodatIm(:, 4) * Cmob) + &
      this%isodatIm(:, 5) * Cmob / (1.0 + this%isodatIm(:, 6) * Cmob)


  CASE (22)
    ! Competitive tri-Langmuir ncomp>=1. 6 param/comp
		! capacity1=param 1 / param 2 should be equal for all components and
		! capacity2=param 3 / param 4 should be equal for all components and 
    ! capacity3=param 5 / param 6 should be equal for all components for
    ! thermodynamic consistency
		qCalcComplex = &
      this%isodatIm(:, 1) * Cmob / (1.0 + SUM(this%isodatIm(:, 2) * Cmob)) + &
      this%isodatIm(:, 3) * Cmob / (1.0 + SUM(this%isodatIm(:, 4) * Cmob)) + &
      this%isodatIm(:, 5) * Cmob / (1.0 + SUM(this%isodatIm(:, 6) * Cmob))


  CASE (7)
    ! Non-competitive n-Langmuir ncomp>=1. 2*n param/comp
		! capacity(n) = param n / param n+1 should be equal for all components and 
    ! thermodynamic consistency
    Nsites = (size(this%isodatIm, 2))/2
    qCalcComplex = 0.0
    DO i = 0, Nsites - 1
		  qCalcComplex = qCalcComplex + this%isodatIm(:, 2*i + 1) * Cmob /  & 
        (1.0 + this%isodatIm(:, 2*i + 2) * Cmob)
    END DO


  CASE (20)
    ! Competitive n-Langmuir ncomp>=1. 2*n param/comp
		! capacity(n) = param n / param n+1 should be equal for all components and 
    ! thermodynamic consistency
    Nsites = (size(this%isodatIm, 2))/2
    qCalcComplex = 0.0
    DO i = 0, Nsites - 1
		  qCalcComplex = qCalcComplex + this%isodatIm(:, 2*i + 1) * Cmob /  & 
        (1.0 + SUM(this%isodatIm(:, 2*i + 2) * Cmob))
    END DO


	CASE (8) ! 7	
    ! non-competitive Unilan, ncomp>=1. 3 param/comp
		! p1 capacity	
		qCalcComplex = ( this%isodatIm(:, 1) / (2*this%isodatIm(:, 2))) * &
      LOG( (1 + this%isodatIm(:, 3) * Cmob * EXP(this%isodatIm(:, 2))) /&
			(1.0 + this%isodatIm(:, 3) * Cmob * EXP(-this%isodatIm(:, 2))) )

	
	CASE (9)	
    ! Single component Moreau single site. ncomp=1. 3 param/comp
		! Takes adsorbate-adsorbate interactions into account p1 capacity
		qCalcComplex = this%isodatIm(:, 1) * ( this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * ((this%isodatIm(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * ((this%isodatIm(:, 2) * Cmob)**2) )


	CASE (10)	
    ! Single component bi-Moreau. ncomp=1. 6 param/comp 
		! Takes adsorbate-adsorbate interactions into account
		! p1, p4 site 1 and 2 capacities
		qCalcComplex = this%isodatIm(:, 1) * ( this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * ((this%isodatIm(:, 2) * Cmob)**2) ) / &
			(1.0 + 2*this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * ((this%isodatIm(:, 2) * Cmob)**2) ) + &
			this%isodatIm(:, 4) * (this%isodatIm(:, 5) * &
      Cmob + this%isodatIm(:, 6) * ((this%isodatIm(:, 5) * Cmob)**2) ) / & 
      (1.0 + 2*this%isodatIm(:, 5) * Cmob + this%isodatIm(:, 6) * &
      ((this%isodatIm(:, 5) * Cmob)**2) )

 
	CASE (11)	
    ! Single Statistical isotherm( Quadratic). ncomp=1. 3 param/comp
		! param 1,2,3 corresponds to param 1,2,4 in binary statistical isotherm
		qCalcComplex = this%isodatIm(:, 1) * ( this%isodatIm(:,2) * Cmob + &
      this%isodatIm(:, 3) * Cmob**2) / (1 + this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * Cmob**2)


	CASE (12)	
    ! Binary Statistical isotherm (Quadratic) (Ruthven, Goddard). ncomp= 2. 
    ! 4 param/comp, p3 MUST be equal for both comps
		! p1 =capacity should be equal for both comps (for thermodynamic consistency)	
		qCalcComplex = this%isodatIm(:, 1) * ( this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodatIm(:, 4) * &
      Cmob**2) / (1.0 + this%isodatIm(1, 2) * Cmob(1) + this%isodatIm(2, 2) * & 
      Cmob(2) + this%isodatIm(1, 3) * Cmob(1) * Cmob(2) + &
      this%isodatIm(1, 4) * Cmob(1)**2 + this%isodatIm(2, 4) * Cmob(2)**2)
      
      
  CASE (23)	
    ! Two site binary Statistical isotherm (Quadratic). 
    ! ncomp= 2. 8 param/comp, p3, p7 MUST be equal for both comps
    ! p1, p4 (capacity) should be equal for both comps (for thermodynamic 
    ! consistency)
		qCalcComplex = &
      this%isodatIm(:, 1) * ( this%isodatIm(:, 2) * Cmob + &
      this%isodatIm(:, 3) * Cmob(1) * Cmob(2) + 2*this%isodatIm(:, 4) * &
      Cmob**2) / (1.0 + this%isodatIm(1, 2) * Cmob(1) + this%isodatIm(2, 2) * & 
      Cmob(2) + this%isodatIm(1, 3) * Cmob(1) * Cmob(2) + &
      this%isodatIm(1, 4) * Cmob(1)**2 + this%isodatIm(2, 4) * Cmob(2)**2) + &
      this%isodatIm(:, 5) * ( this%isodatIm(:, 6) * Cmob + &
      this%isodatIm(:, 7) * Cmob(1) * Cmob(2) + 2*this%isodatIm(:, 8) * &
      Cmob**2) /  (1.0 + this%isodatIm(1, 6) * Cmob(1) + this%isodatIm(2, 6) * &
      Cmob(2) + this%isodatIm(1, 7) * Cmob(1) * Cmob(2) + &
      this%isodatIm(1, 8) * Cmob(1)**2 + this%isodatIm(2, 8) * Cmob(2)**2)		

		
	CASE (13)	
    ! Competitive single site Toth. ncomp>=1. 3 param/comp
		! p3 (heterogenity constant should be equal for all components	
		qCalcComplex = this%isodatIm(:, 1) * Cmob / ( 1.0 + & 
      (SUM(this%isodatIm(:,2) * Cmob)**(this%isodatIm(:, 3))))** &
      (1.0/this%isodatIm(:, 3) )

	
	CASE (14)	
    ! Competitive bi-Toth. ncomp >= 1. 6 param/comp
		! param 3 (heterogenity constant should be equal for all components
		! param 6 (heterogenity constant should be equal for all components	
		qCalcComplex = this%isodatIm(:, 1) * Cmob / (1.0 + &
      (SUM(this%isodatIm(:, 2) * Cmob)**(this%isodatIm(:, 3))))** &
      (1.0 / this%isodatIm(:, 3)) + this%isodatIm(:, 4) * Cmob / &
      (1.0 + (SUM(this%isodatIm(:, 5) * Cmob)**(this%isodatIm(:, 6))))** &
      (1.0 / this%isodatIm(:, 6))	

	
	CASE(15)	
    ! Non-competitive Freundlich. ncomp>=1.  2 param/comp
		! p2 heterogenity const
		! p1 corresponds to langmuir a term (possibly) 
		qCalcComplex = this%isodatIm(:, 1) * Cmob**(this%isodatIm(:, 2))
	
		
	CASE (16)	
    ! Competitive Jovanovic-Freundlich. ncomp>=1. 3 param/comp
		! p1 capacity
		! p3 (heterogenity const) should be equal for all comps
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalcComplex = 0.0
		ELSE
		  temp  = SUM( this%isodatIm(:,2) * Cmob )
		  qCalcComplex = this%isodatIm(:, 1) * this%isodatIm(:, 2) * Cmob * &
        ( 1.0 - EXP( -(temp**(this%isodatIm(:, 3)))) ) / temp
		END IF

	
	CASE (17)	
    ! Competitive Jovanovic ncomp>=1. 2 param/comp
		! Homogenous version of Jovanovic-Freundlich isotherm
		! p1 capacity
		IF (SUM(ABS(Cmob)) <= 1.0E-16) THEN
			qCalcComplex = 0.0
		ELSE
		  temp  = SUM( this%isodatIm(:, 2) * Cmob )
		  qCalcComplex = this%isodatIm(:, 1) * this%isodatIm(:, 2) * Cmob * &
        ( 1.0 - EXP(-temp) ) / temp
		END IF


	CASE (18) 
    ! Competitive Freundlich-Langmuir. ncomp>=1. 3 param/comp
		! here the heterogenity constants (param 2) don't have to be equal
		! p1 capacity
		qCalcComplex = ( this%isodatIm(:, 1)* this%isodatIm(:, 2)*Cmob** &
      (this%isodatIm(:, 3)) ) / ( 1.0 + SUM(this%isodatIm(:,2)*Cmob** &
      (this%isodatIm(:, 3))) ) 
	
	CASE (19) 
    ! Competitive Jaroniec-Freundlich-Langmuir. ncomp>=1. 3 param/comp
		! p1 capacity (probably) 
		! p3 heterogenity constant should be equal for all comp
		temp  = SUM(this%isodatIm(:, 2) * Cmob)
		qCalcComplex = ( this%isodatIm(:,1) * Cmob / temp ) * &
      ( (temp**(this%isodatIm(:,3))) / (1.0 + temp**(this%isodatIm(:, 3))) ) 


	CASE DEFAULT
    STOP

 
  END SELECT


END FUNCTION qCalcComplex 
  


END MODULE simtest1

