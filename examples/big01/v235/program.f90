!
! Modulo indice di precisione
!
! Il modulo deve essere inserito in tutte le routines ed 
! i moduli che definiscono delle variabili locali di tipo reale. 
! Le variabili vanno necessariamente definite, tranne casi 
! particolari, nel modo seguente:
!
!           real(iprec), ...... :: pippo, pluto, .... 
!
!  A. Miliozzi       30-07-1999      creazione
!
module precis
!
!  Livello di presisione: (10) cifre con esponente */- 10*(50)
!
  integer , parameter :: iprec=selected_real_kind(10,50)
!
end module precis



! 
! Modulo proprieta' fisiche gas ed acqua 
! 
! Proprieta' gas/aria 
! RR      Gases constant = 8314.41 J/(kmol K) 
! AM      Dry Air molar mass = 28.9645 g/mol 
! CPA     Dry air heat capacity (const. press.) =1005.7 J/(kg K) 
! TKA     Dry air heat conductivity =0.0259 W/(m K) 
! VM      Water Vapor molar mass = 18.01528 g/mol  
! CPV     Water Vapor heat capacity (const. press) = 1805 J/(kg K)  
! TKV     Water Vapor heat conductivity = 0.0186 W/(m K) 
! 
! Proprieta' acqua                                  
! HVAP    .. 
! DHVAT 
! D2HVTT                               
! RHOW 
! DRHWP 
! DRHWC 
! DRHWT                         
! D2RHWTT 
! D2RHWCC 
! D2RHWCT                         
! CPW 
! DCPWT 
! TKW 
! VISCW                                
! DVISWT                                
! 
! CDIF 
! DCDIFT 
! DCDIFC 
! DCDIFP                         
! 
!  A. Miliozzi       15-06-1999      creazione 
! 
module comphys 
! 
use precis 
! 
! proprieta' gas/aria 
real(iprec), save :: RR=8314.41 , AM=28.9645 , CPA=1005.7 , TKA=0.0259  
real(iprec), save :: VM=18.01528 , CPV=1805. , TKV=0.0186                                                         
!                                                                        
! proprieta' acqua                                                                        
real(iprec), save :: HVAP, DHVAT, D2HVTT                             
real(iprec), save :: RHOW, DRHWP, DRHWC, DRHWT, D2RHWTT, D2RHWCC, D2RHWCT                         
real(iprec), save :: CPW, DCPWT, TKW, VISCW, DVISWT                               
! 
! real(iprec), save :: CDIF, DCDIFT,DCDIFC,DCDIFP                         
! 
end module comphys 


!
! Modulo GDL Gradi di Liberta'
!
! tt
! pc
! pg
! uu
! vv
!
!  A. Miliozzi       03-08-1999      creazione
!
module comgdl
!
use precis
!
! riempiti da ICOND
   real(iprec), save :: tt, pc, pg, uu, vv
!
end module comgdl


module comate
!
use precis
!
!real(iprec), save :: POR, PORo, DPORT       
real(iprec), save :: POR, DPORT  ! rn  dichiaro la variabile poro all'intero delle subr. dei materiali                     
real(iprec), save :: CKK, DCKKT, DCKKP, DCKKC                          
real(iprec), save :: RHOS, DRHST                        
real(iprec), save :: STRF, DSTRFT, TORT                              
real(iprec), save :: TKS, DTKST, CPS, DCPST                        
real(iprec), save :: HYDW, DEHYDWT, HYDREN
real(iprec), save :: EMOD, DEMODT, EMODT, DEMODTT, VCOEFF, DVCT, BETAS, ALFA
real(iprec), save :: AT,AC,BT,BC,XK0
real(iprec), save :: DLD
integer    , save :: IFDLD
real(iprec), save :: PVW, DPVWC, DPVWT
real(iprec), save :: CKW, CKG, DCKWC, DCKWT, DCKGC, DCKGT
real(iprec), save :: KBW, DKBWC, DKBWT
real(iprec), save :: SS, DSSC, DSST, D2SSCC, D2SSTT, D2SSCT, WATCON
real(iprec), save :: FINV,FSTE
!
real(iprec), save :: emod0 ! aggiunto emod0 per ricordarsi il valore del modulo
!                            di Young a 20°C
!

!M. Del Pin (October 2002) per versione multimateriale e smoothing
real(iprec), save, dimension(:)  , allocatable::  satn, pvapn, relhn, porosn

real(iprec), save :: ttmax ! aggiunta come variabile sul punto gauss o sul nodo corrente
                           ! per la temperatura massima 13/12/2003 


end module comate


SUBROUTINE WATPROP (IRHS, PG, PC, TT)                             
! 
use precis 
use comphys 
! 
IMPLICIT none                                
!                                                                        
integer    :: IRHS 
real(iprec):: PG, PC, TT                           
!                                                                        
REAL(iprec):: VISCWo, CONB, CONC            
REAL(iprec):: RHOWo,CWAT,BETWAT,HVAPo,Patm,TTo,TTcrit,TTDIF         
REAL(iprec):: a0,a1,a2,a3,a4,a5,b0,b1,b2,b3,b4,b5,tem,prif,pr1         
!                                                                        
!       CONSTANTS FOR FORMULAS                                                 
!                                                                        
VISCWo=  0.6612 
CONB  =  229.  
CONC  = -1.562            
! 
RHOWo =  999.84                                                         
CWAT  =  0.2E+9                                                         
BETWAT= -0.414E-3                                                
HVAPo =  2.7E+5                                                       
! 
a0 =  4.8863E-7 ; a1 = -1.6528E-9 ; a2 =  1.8621E-12 
a3 =  2.4266E-13; a4 = -1.5996E-15; a5 =  3.3703E-18 
b0 =  1.0213E3  ; b1 = -7.7377E-1 ; b2 =  8.7696E-3 
b3 = -9.2118E-5 ; b4 =  3.3534E-7 ; b5 = -4.4034E-10 
pr1 = 1.0E7 ; prif = 2.0E7 
!                                                                        
Patm  =  101325.                                                         
TTo   =  273.15                                                          
TTcrit=  647.3                                                           
! 
tem= TT - TTo 
!                                                                        
!       WATER DENSITY                                                          
!                                                                        
IF (TT.LT.TTcrit) THEN                                                  
  !RHOW =  RHOWo *(1.+(PG-Patm)/CWAT+BETWAT*(TT-TTo))                 
  !DRHWP=  RHOWo/CWAT                                                  
  !DRHWC=  0.                                                          
  !DRHWT=  RHOWo*BETWAT                                                
  !amRHOW =  RHOWo *(1.+BETWAT*(TT-TTo))                                   
  RHOW =  (b0+(b1+(b2+(b3+(b4+b5*tem)*tem)*tem)*tem)*tem) + (pr1-prif)* & 
          (a0+(a1+(a2+(a3+(a4+a5*tem)*tem)*tem)*tem)*tem) 
  DRHWP=  0.                  
  DRHWC=  0.                                                             
  !amDRHWT=  RHOWo*BETWAT                                                   
  DRHWT=  (b1+(2*b2+(3*b3+(4*b4+5*b5*tem)*tem)*tem)*tem) + (pr1-prif)* & 
          (a1+(2*a2+(3*a3+(4*a4+5*a5*tem)*tem)*tem)*tem) 
  !amD2RHWTT=0.                                                            
  D2RHWTT=(2*b2+(6*b3+(12*b4+20*b5*tem)*tem)*tem) + (pr1-prif)* & 
          (2*a2+(6*a3+(12*a4+20*a5*tem)*tem)*tem)                                                           
  D2RHWCC=0.                                                             
  D2RHWCT=0.                                                      
ELSE                                                                    
  !RHOW =  RHOWo *(1.+(PG-Patm)/CWAT+BETWAT*(TTcrit-TTo))             
  !DRHWP=  RHOWo/CWAT                                                  
  !amRHOW =  RHOWo *(1.+BETWAT*(TTcrit-TTo))                               
  tem = TTcrit - TTo 
  RHOW =  (b0+(b1+(b2+(b3+(b4+b5*tem)*tem)*tem)*tem)*tem) + (pr1-prif)* & 
          (a0+(a1+(a2+(a3+(a4+a5*tem)*tem)*tem)*tem)*tem) 
  DRHWP=  0.                                                             
  DRHWC=  0.                                                             
  DRHWT=  0.                                                             
  D2RHWTT=0.                                                            
  D2RHWCC=0.                                                             
  D2RHWCT=0.                                                      
ENDIF                                                                     
!                                                                        
!       WATER VISCOSITY    = 1000e-6 Pa*s at 20 C                              
!                                                                        
VISCW = VISCWo*(TT-CONB)**CONC                                   
DVISWT= (CONC-1.)*VISCW/(TT-CONB)                                
!                                                                        
!       WATER SPECIFIC HEAT AT CONST. PRESSURE =4181 J/kg K              
!                                                                        
CPW=4181.                                                        
DCPWT=0.                                                                
!                                                                        
!        WATER HEAT CONDUCTIVITY = 0.6 W/(m K)                           
!                                                                        
TKW=0.6                                                           
!                                                                        
!       WATER LATENT HEAT OF VAPORISATION of Forsyth                           
!                                                                        
IF (TT.LT.TTcrit) THEN                                                  
  TTDIF = TTcrit - TT                                                    
  HVAP  = HVAPo * TTDIF**0.38                                            
  !                                                                        
  DHVAT = -0.38 * HVAP / TTDIF                                           
  D2HVTT=  0.62 * DHVAT / TTDIF                                          
ELSE                                                                    
  HVAP  = 0.                                                             
  DHVAT = 0.                                                             
  D2HVTT= 0.                                                             
ENDIF                                                                                 
! 
END subroutine watprop                                                               


SUBROUTINE SATUR_C(W1, C1, IRHS)
  !
  use precis
  use comgdl
  use comphys
  use comate
  !
  IMPLICIT NONE
  !                               
  integer :: irhs
  real(iprec) :: w1,c1
  !
  real(iprec) :: ttcrit, tamb, tem                                  
  real(iprec) :: tt1, mt, dtt1tt, d2tt1tt, dmttt, d2mttt 
  real(iprec) :: amrr, hr, dhrtt, dhrcc , d2hrtt, d2hrcc, d2hrct  
  real(iprec) :: ssmh, lnh, xfun, dxfuntt, dxfuncc                                        
  real(iprec) :: mt1, dm1ttt, d2m1ttt, fcm, dfcmtt, d2fcmtt                                        
  !
  !***********************************
  ! Costanti 
  !***********************************
  TTcrit= 647.3  ; Tamb= 25.0
  !
  !***********************************
  ! Temperatura in gradi centigradi
  !***********************************
  if(tt.lt.ttcrit) then
     TEM = TT - 273.15                                            
     IF (TEM.LT.Tamb) TEM = Tamb                                                 
  else
     TEM = TTcrit - 273.15  
  endif
  !        
  !***********************************
  ! Calcolo densita'
  !***********************************
  CALL WATPROP(IRHS, PG, PC, TT)                   
  !
  !***********************************
  ! Calcolo m(T)
  !***********************************
  tt1 = ((tem+10.)/(25.+10.)) ** 2
  mt1 = 1.04 - (tt1/(24.+tt1))
  if(tt.lt.ttcrit) then
     fcm = (ttcrit-tt)/(ttcrit-298.15)
     mt  = mt1 * fcm
     dtt1tt = 2. * (tem+10.) / ((25.+10.)**2)
     dm1ttt = -24. * dtt1tt / ((24.+tt1)**2)  
     dfcmtt = -1./(ttcrit-298.15)
     if(mt.lt.1.E-4) mt=1.E-4
  else
     fcm    = 0.
     mt     = 1.E-4
     dtt1tt = 0.
     dm1ttt = 0.  
     dfcmtt = 0.
  endif
  dmttt = dm1ttt * fcm + mt1 * dfcmtt
  !
  !***********************************
  ! Calcolo umidita' relativa
  !***********************************
  if(tt.lt.ttcrit) then
     amrr = vm / (rr*tt*rhow) 
     hr = exp(- pc*amrr)
     dhrtt = amrr * hr * pc * (1./tt + drhwt/rhow)
     dhrcc = - amrr * hr
  else
     amrr = vm / (rr*ttcrit*rhow) 
     hr = exp(- pc*amrr)
     dhrtt = 0.
     dhrcc = - amrr * hr
  endif
  !
  !***********************************
  ! Calcolo saturazione
  !***********************************
  !
  ss = hr ** (1./mt) 
  lnh  = log(hr)
  if(tt.lt.ttcrit) then
     xfun = dhrtt - hr/mt*lnh*dmttt 
     ssmh = ss / mt / hr
     dsst = ssmh * xfun
     dssc = ssmh * dhrcc 
  else
     xfun = dhrtt - hr/mt*lnh*dmttt 
     ssmh = ss / mt / hr
     dsst = 0.
     dssc = ssmh * dhrcc 
  endif
  !
  IF (SS.LT.1.E-10) THEN                                                 
     SS=1.E-10 ; DSST=0. ; DSSC=0.                                                               
  ENDIF
  !
  !***********************************
  ! Calcolo contenuto d'acqua
  !***********************************
  !
  watcon = c1 * ( (w1/c1) ** (1./mt) ) * ss
  !
  IF (IRHS.EQ.0) THEN                                             
     !                                                                       
     if(tt.lt.ttcrit) then
        !
        d2tt1tt = dtt1tt / (tem+10.)
        d2m1ttt = dm1ttt * (2.*dtt1tt/(24.+tt1) - d2tt1tt/dtt1tt)  
        d2fcmtt = 0. 
        d2mttt  = d2m1ttt*fcm + 2.*dm1ttt*dfcmtt + mt1*d2fcmtt
        d2hrtt  = dhrtt*(dhrtt/hr-1./tt-drhwt/rhow) + &
             amrr*hr*pc*(1./(tt**2)+(drhwt**2)/(rhow**2)-d2rhwtt/rhow)
        d2hrcc = - amrr * dhrcc
        d2hrct = dhrtt * (1./pc+dhrcc/hr)
        !
        IF (SS.LT.1.E-10) THEN                                                 
           d2sstt = 0. ; d2sscc = 0. ; d2ssct = 0.
        ELSE
           dxfuntt= d2hrtt - (dhrtt*dmttt*(lnh+1.)+hr*lnh*d2mttt)/mt
           d2sstt = ssmh * ((dsst/ss-dhrtt/hr)*xfun+dxfuntt)
           d2sscc = ssmh * (dssc*dhrcc/ss-(dhrcc**2)/hr+d2hrcc)
           dxfuncc= d2hrct - dmttt*(lnh+1.)*dhrcc/mt
           d2ssct = ssmh * ((dssc/ss-dhrcc/hr)*xfun+dxfuncc)
        ENDIF
        !
     else
        !
        d2rhwtt= 0.
        d2mttt  = 0.  
        d2hrtt  = 0.
        d2hrcc = - amrr * dhrcc
        d2hrct = 0.
        !
        IF (SS.LT.1.E-10) THEN                                                 
           d2sstt = 0. ; d2sscc = 0. ; d2ssct = 0.
        ELSE
           d2sstt = 0.
           d2sscc = ssmh * (dssc*dhrcc/ss-(dhrcc**2)/hr+d2hrcc)
           d2ssct = 0.
        ENDIF
        !
     endif
     !
  ENDIF

! Moltiplicazione di SS e delle sue derivate prime e seconde per 0.02 (per PROMATEC):

  if(ss<1e-10) ss=1e-10
  ss     = 0.02*ss
  dsst   = 0.02*dsst
  dssc   = 0.02*dssc
  d2sstt = 0.02*d2sstt
  d2sscc = 0.02*d2sscc
  d2ssct = 0.02*d2ssct

  !                                                                       
END subroutine SATUR_C
