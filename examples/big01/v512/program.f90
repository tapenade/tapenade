      subroutine IMP_PROD_LOSS( prod &
      ,       loss &
      ,        y &
      ,        rxt &
      ,        het_rates)
! Stacy Walters, NCAR, 1998.
! Modified by Didier Hauglustaine, IPSL, for LMDZ/INCA, 2000.

      implicit none

!--------------------------------------------------------------------
!     ... Dummy args               
!--------------------------------------------------------------------
      real, dimension(1,945), intent(out) :: &
           prod &
      ,     loss
      real, intent(in)    ::  y(1,945)
      real, intent(in)    ::  rxt(1,4522)
      real, intent(in)    ::  het_rates(1,37)



!--------------------------------------------------------------------
!     ... Local variables          
!--------------------------------------------------------------------
      integer :: k

!--------------------------------------------------------------------
!       ... Loss and production for Implicit method
!--------------------------------------------------------------------

      do k = 1,1
         loss(k,98) = (rxt(k,4131)* y(k,705))* y(k,1)
         prod(k,98) = 0.
         loss(k,118) = (rxt(k,4168)* y(k,705))* y(k,2)
         prod(k,118) = 0.
         loss(k,119) = ((rxt(k,334) +rxt(k,335))* y(k,705))* y(k,3)
         prod(k,119) = 0.
         loss(k,90) = ((rxt(k,336) +rxt(k,337))* y(k,705))* y(k,4)
         prod(k,90) = 0.
         loss(k,593) = ((rxt(k,707) +rxt(k,708) +rxt(k,709) +rxt(k,710))* y(k,705))* y(k,5)
         prod(k,593) = 0.
         loss(k,528) = ((rxt(k,820) +rxt(k,821) +rxt(k,822) +rxt(k,823))* y(k,705))* y(k,6)
         prod(k,528) = 0.
         loss(k,595) = ((rxt(k,711) +rxt(k,712) +rxt(k,713) +rxt(k,714))* y(k,705))* y(k,7)
         prod(k,595) = 0.
         loss(k,91) = (rxt(k,338)* y(k,705))* y(k,8)
         prod(k,91) = 0.
         loss(k,120) = ((rxt(k,3873) +rxt(k,3874))* y(k,705))* y(k,9)
         prod(k,120) = 0.
         loss(k,121) = ((rxt(k,3870) +rxt(k,3871))* y(k,705))* y(k,10)
         prod(k,121) = 0.
         loss(k,531) = ((rxt(k,645) +rxt(k,646) +rxt(k,647) +rxt(k,648))* y(k,705))* y(k,11)
         prod(k,531) = 0.
         loss(k,436) = ((rxt(k,715) +rxt(k,716) +rxt(k,717) +rxt(k,718))* y(k,705))* y(k,12)
         prod(k,436) = 0.
         loss(k,437) = ((rxt(k,812) +rxt(k,813) +rxt(k,814) +rxt(k,815))* y(k,705))* y(k,13)
         prod(k,437) = 0.
         loss(k,672) = ((rxt(k,2226) +rxt(k,2227) +rxt(k,2228) +rxt(k,2229) +rxt(k,2230))* y(k,705))* y(k,14)
         prod(k,672) = 0.
         loss(k,438) = ((rxt(k,719) +rxt(k,720) +rxt(k,721) +rxt(k,722))* y(k,705))* y(k,15)
         prod(k,438) = 0.
         loss(k,694) = ((rxt(k,2231) +rxt(k,2232) +rxt(k,2233) +rxt(k,2234) +rxt(k,2235))* y(k,705))* y(k,16)
         prod(k,694) = 0.
         loss(k,439) = ((rxt(k,723) +rxt(k,724) +rxt(k,725) +rxt(k,726))* y(k,705))* y(k,17)
         prod(k,439) = 0.
         loss(k,535) = ((rxt(k,2581) +rxt(k,2582) +rxt(k,2583) +rxt(k,2584))* y(k,705))* y(k,18)
         prod(k,535) = 0.
         loss(k,190) = ((rxt(k,1115) +rxt(k,1116))* y(k,705))* y(k,19)
         prod(k,190) = 0.
         loss(k,440) = ((rxt(k,2676) +rxt(k,2677) +rxt(k,2678))* y(k,705))* y(k,20)
         prod(k,440) = 0.
         loss(k,93) = (rxt(k,333)* y(k,705))* y(k,21)
         prod(k,93) = 0.
         loss(k,152) = ((rxt(k,3941) +rxt(k,3942) +rxt(k,3943))* y(k,705))* y(k,22)
         prod(k,152) = 0.
         loss(k,600) = ((rxt(k,3497) +rxt(k,3498) +rxt(k,3499) +rxt(k,3500))* y(k,705))* y(k,23)
         prod(k,600) = 0.
         loss(k,442) = ((rxt(k,649) +rxt(k,650) +rxt(k,651) +rxt(k,652))* y(k,705))* y(k,24)
         prod(k,442) = 0.
         loss(k,283) = ((rxt(k,731) +rxt(k,732) +rxt(k,733))* y(k,705))* y(k,25)
         prod(k,283) = 0.
         loss(k,775) = ((rxt(k,1787) +rxt(k,1788))* y(k,693) + (rxt(k,1784) +rxt(k,1785) +rxt(k,1786))* y(k,696) + (rxt(k,1781) + &
rxt(k,1782) +rxt(k,1783))* y(k,705) +rxt(k,1789)* y(k,839))* y(k,26)
         prod(k,775) = 0.
         loss(k,444) = ((rxt(k,3174) +rxt(k,3175) +rxt(k,3176) +rxt(k,3177))* y(k,705))* y(k,27)
         prod(k,444) = 0.
         loss(k,284) = ((rxt(k,1229) +rxt(k,1230) +rxt(k,1231))* y(k,705))* y(k,28)
         prod(k,284) = 0.
         loss(k,446) = ((rxt(k,951) +rxt(k,952) +rxt(k,953) +rxt(k,954))* y(k,705))* y(k,29)
         prod(k,446) = 0.
         loss(k,447) = ((rxt(k,816) +rxt(k,817) +rxt(k,818) +rxt(k,819))* y(k,705))* y(k,30)
         prod(k,447) = 0.
         loss(k,285) = ((rxt(k,1034) +rxt(k,1035) +rxt(k,1036))* y(k,705))* y(k,31)
         prod(k,285) = 0.
         loss(k,602) = ((rxt(k,2236) +rxt(k,2237) +rxt(k,2238) +rxt(k,2239))* y(k,705))* y(k,32)
         prod(k,602) = 0.
         loss(k,861) = ((rxt(k,1123) +rxt(k,1124) +rxt(k,1125))* y(k,693) + (rxt(k,1120) +rxt(k,1121) +rxt(k,1122))* y(k,696) &
 + (rxt(k,1117) +rxt(k,1118) +rxt(k,1119))* y(k,705) + (rxt(k,1126) +rxt(k,1127) +rxt(k,1128) +rxt(k,1129)) &
* y(k,839))* y(k,33)
         prod(k,861) = 0.
         loss(k,286) = (rxt(k,3980)* y(k,693) +rxt(k,3979)* y(k,705))* y(k,34)
         prod(k,286) = 0.
         loss(k,539) = ((rxt(k,2672) +rxt(k,2673) +rxt(k,2674) +rxt(k,2675))* y(k,705))* y(k,35)
         prod(k,539) = 0.
         loss(k,338) = ((rxt(k,894) +rxt(k,895) +rxt(k,896))* y(k,705))* y(k,36)
         prod(k,338) = 0.
         loss(k,449) = ((rxt(k,653) +rxt(k,654) +rxt(k,655) +rxt(k,656))* y(k,705))* y(k,37)
         prod(k,449) = 0.
         loss(k,450) = ((rxt(k,737) +rxt(k,738) +rxt(k,739) +rxt(k,740))* y(k,705))* y(k,38)
         prod(k,450) = 0.
         loss(k,224) = ((rxt(k,2538) +rxt(k,2539) +rxt(k,2540))* y(k,705))* y(k,39)
         prod(k,224) = 0.
         loss(k,542) = ((rxt(k,1042) +rxt(k,1043) +rxt(k,1044) +rxt(k,1045))* y(k,705))* y(k,40)
         prod(k,542) = 0.
         loss(k,543) = ((rxt(k,924) +rxt(k,925) +rxt(k,926) +rxt(k,927))* y(k,705))* y(k,41)
         prod(k,543) = 0.
         loss(k,451) = ((rxt(k,980) +rxt(k,981) +rxt(k,982) +rxt(k,983))* y(k,705))* y(k,42)
         prod(k,451) = 0.
         loss(k,340) = ((rxt(k,1059) +rxt(k,1060) +rxt(k,1061))* y(k,705))* y(k,43)
         prod(k,340) = 0.
         loss(k,341) = ((rxt(k,1009) +rxt(k,1010) +rxt(k,1011))* y(k,705))* y(k,44)
         prod(k,341) = 0.
         loss(k,287) = ((rxt(k,2679) +rxt(k,2680) +rxt(k,2681))* y(k,705))* y(k,45)
         prod(k,287) = 0.
         loss(k,342) = ((rxt(k,897) +rxt(k,898) +rxt(k,899))* y(k,705))* y(k,46)
         prod(k,342) = 0.
         loss(k,288) = ((rxt(k,734) +rxt(k,735) +rxt(k,736))* y(k,705))* y(k,47)
         prod(k,288) = 0.
         loss(k,452) = ((rxt(k,2646) +rxt(k,2647) +rxt(k,2648))* y(k,705))* y(k,48)
         prod(k,452) = 0.
         loss(k,810) = ((rxt(k,1225) +rxt(k,1226) +rxt(k,1227))* y(k,693) + (rxt(k,1221) +rxt(k,1222) +rxt(k,1223) +rxt(k,1224)) &
* y(k,696) + (rxt(k,1218) +rxt(k,1219) +rxt(k,1220))* y(k,705) +rxt(k,1228)* y(k,839))* y(k,49)
         prod(k,810) = 0.
         loss(k,741) = ((rxt(k,3909) +rxt(k,3910) +rxt(k,3911))* y(k,693) + (rxt(k,3907) +rxt(k,3908))* y(k,696) + (rxt(k,3904) + &
rxt(k,3905) +rxt(k,3906))* y(k,705))* y(k,50)
         prod(k,741) = 0.
         loss(k,548) = ((rxt(k,3400) +rxt(k,3401) +rxt(k,3402) +rxt(k,3403))* y(k,705))* y(k,51)
         prod(k,548) = 0.
         loss(k,852) = ((rxt(k,1089) +rxt(k,1090) +rxt(k,1091))* y(k,693) + (rxt(k,1085) +rxt(k,1086) +rxt(k,1087) +rxt(k,1088)) &
* y(k,696) + (rxt(k,1082) +rxt(k,1083) +rxt(k,1084))* y(k,705) +rxt(k,1092)* y(k,839))* y(k,52)
         prod(k,852) = 0.
         loss(k,811) = ((rxt(k,1968) +rxt(k,1969))* y(k,693) + (rxt(k,1964) +rxt(k,1965) +rxt(k,1966) +rxt(k,1967))* y(k,696) &
 + (rxt(k,1961) +rxt(k,1962) +rxt(k,1963))* y(k,705) +rxt(k,1970)* y(k,839))* y(k,53)
         prod(k,811) = 0.
         loss(k,343) = ((rxt(k,3487) +rxt(k,3488) +rxt(k,3489))* y(k,705))* y(k,54)
         prod(k,343) = 0.
         loss(k,812) = ((rxt(k,2090) +rxt(k,2091))* y(k,693) + (rxt(k,2086) +rxt(k,2087) +rxt(k,2088) +rxt(k,2089))* y(k,696) &
 + (rxt(k,2083) +rxt(k,2084) +rxt(k,2085))* y(k,705) +rxt(k,2092)* y(k,839))* y(k,55)
         prod(k,812) = 0.
         loss(k,813) = ((rxt(k,2110) +rxt(k,2111))* y(k,693) + (rxt(k,2106) +rxt(k,2107) +rxt(k,2108) +rxt(k,2109))* y(k,696) &
 + (rxt(k,2103) +rxt(k,2104) +rxt(k,2105))* y(k,705) +rxt(k,2112)* y(k,839))* y(k,56)
         prod(k,813) = 0.
         loss(k,814) = ((rxt(k,2130) +rxt(k,2131))* y(k,693) + (rxt(k,2126) +rxt(k,2127) +rxt(k,2128) +rxt(k,2129))* y(k,696) &
 + (rxt(k,2123) +rxt(k,2124) +rxt(k,2125))* y(k,705) +rxt(k,2132)* y(k,839))* y(k,57)
         prod(k,814) = 0.
         loss(k,815) = ((rxt(k,2150) +rxt(k,2151))* y(k,693) + (rxt(k,2146) +rxt(k,2147) +rxt(k,2148) +rxt(k,2149))* y(k,696) &
 + (rxt(k,2143) +rxt(k,2144) +rxt(k,2145))* y(k,705) +rxt(k,2152)* y(k,839))* y(k,58)
         prod(k,815) = 0.
         loss(k,733) = (rxt(k,2592)* y(k,693) + (rxt(k,2589) +rxt(k,2590) +rxt(k,2591))* y(k,705) + rxt(k,1) + rxt(k,2) + rxt(k,3) &
 + het_rates(k,23))* y(k,59)
         prod(k,733) = 0.
         loss(k,736) = (rxt(k,2753)* y(k,693) + (rxt(k,2749) +rxt(k,2750) +rxt(k,2751) +rxt(k,2752))* y(k,705) + rxt(k,4) &
 + rxt(k,5) + rxt(k,6) + het_rates(k,24))* y(k,60)
         prod(k,736) = 0.
         loss(k,453) = ((rxt(k,2906) +rxt(k,2907) +rxt(k,2908))* y(k,705))* y(k,61)
         prod(k,453) = 0.
         loss(k,734) = (rxt(k,2894)* y(k,693) + (rxt(k,2891) +rxt(k,2892) +rxt(k,2893))* y(k,705) + rxt(k,7) + rxt(k,8) + rxt(k,9) &
 + het_rates(k,25))* y(k,62)
         prod(k,734) = 0.
         loss(k,344) = ((rxt(k,3087) +rxt(k,3088) +rxt(k,3089))* y(k,705))* y(k,63)
         prod(k,344) = 0.
         loss(k,725) = (rxt(k,3066)* y(k,693) + (rxt(k,3063) +rxt(k,3064) +rxt(k,3065))* y(k,705) + rxt(k,10) + rxt(k,11) &
 + rxt(k,12) + het_rates(k,26))* y(k,64)
         prod(k,725) = 0.
         loss(k,345) = ((rxt(k,3196) +rxt(k,3197) +rxt(k,3198))* y(k,705))* y(k,65)
         prod(k,345) = 0.
         loss(k,726) = (rxt(k,3192)* y(k,693) + (rxt(k,3189) +rxt(k,3190) +rxt(k,3191))* y(k,705) + rxt(k,13) + rxt(k,14) &
 + rxt(k,15) + het_rates(k,27))* y(k,66)
         prod(k,726) = 0.
         loss(k,816) = ((rxt(k,1934) +rxt(k,1935))* y(k,693) + (rxt(k,1930) +rxt(k,1931) +rxt(k,1932) +rxt(k,1933))* y(k,696) &
 + (rxt(k,1927) +rxt(k,1928) +rxt(k,1929))* y(k,705) +rxt(k,1936)* y(k,839))* y(k,67)
         prod(k,816) = 0.
         loss(k,609) = ((rxt(k,3346) +rxt(k,3347) +rxt(k,3348) +rxt(k,3349))* y(k,705))* y(k,68)
         prod(k,609) = 0.
         loss(k,289) = ((rxt(k,928) +rxt(k,929) +rxt(k,930))* y(k,705))* y(k,69)
         prod(k,289) = 0.
         loss(k,346) = ((rxt(k,824) +rxt(k,825) +rxt(k,826))* y(k,705))* y(k,70)
         prod(k,346) = 0.
         loss(k,857) = ((rxt(k,1492) +rxt(k,1493))* y(k,693) + (rxt(k,1488) +rxt(k,1489) +rxt(k,1490) +rxt(k,1491))* y(k,696) &
 + (rxt(k,1484) +rxt(k,1485) +rxt(k,1486) +rxt(k,1487))* y(k,705) +rxt(k,1494)* y(k,839))* y(k,71)
         prod(k,857) = 0.
         loss(k,858) = ((rxt(k,1242) +rxt(k,1243))* y(k,693) + (rxt(k,1238) +rxt(k,1239) +rxt(k,1240) +rxt(k,1241))* y(k,696) &
 + (rxt(k,1234) +rxt(k,1235) +rxt(k,1236) +rxt(k,1237))* y(k,705) +rxt(k,1244)* y(k,839))* y(k,72)
         prod(k,858) = 0.
         loss(k,521) = ((rxt(k,3211) +rxt(k,3212) +rxt(k,3213) +rxt(k,3214))* y(k,705))* y(k,73)
         prod(k,521) = 0.
         loss(k,257) = ((rxt(k,984) +rxt(k,985) +rxt(k,986))* y(k,705))* y(k,74)
         prod(k,257) = 0.
         loss(k,258) = ((rxt(k,1037) +rxt(k,1038) +rxt(k,1039))* y(k,705))* y(k,75)
         prod(k,258) = 0.
         loss(k,395) = ((rxt(k,727) +rxt(k,728) +rxt(k,729) +rxt(k,730))* y(k,705))* y(k,76)
         prod(k,395) = 0.
         loss(k,396) = ((rxt(k,900) +rxt(k,901) +rxt(k,902) +rxt(k,903))* y(k,705))* y(k,77)
         prod(k,396) = 0.
         loss(k,335) = ((rxt(k,955) +rxt(k,956) +rxt(k,957))* y(k,705))* y(k,78)
         prod(k,335) = 0.
         loss(k,221) = ((rxt(k,1012) +rxt(k,1013) +rxt(k,1014))* y(k,705))* y(k,79)
         prod(k,221) = 0.
         loss(k,223) = ((rxt(k,1062) +rxt(k,1063) +rxt(k,1064))* y(k,705))* y(k,80)
         prod(k,223) = 0.
         loss(k,823) = ((rxt(k,1431) +rxt(k,1432) +rxt(k,1433))* y(k,693) + (rxt(k,1427) +rxt(k,1428) +rxt(k,1429) +rxt(k,1430)) &
* y(k,696) + (rxt(k,1424) +rxt(k,1425) +rxt(k,1426))* y(k,705) +rxt(k,1434)* y(k,839))* y(k,81)
         prod(k,823) = 0.
         loss(k,784) = ((rxt(k,1467) +rxt(k,1468) +rxt(k,1469))* y(k,693) + (rxt(k,1463) +rxt(k,1464) +rxt(k,1465) +rxt(k,1466)) &
* y(k,696) + (rxt(k,1460) +rxt(k,1461) +rxt(k,1462))* y(k,705) +rxt(k,1470)* y(k,839))* y(k,82)
         prod(k,784) = 0.
         loss(k,805) = ((rxt(k,1797) +rxt(k,1798))* y(k,693) + (rxt(k,1793) +rxt(k,1794) +rxt(k,1795) +rxt(k,1796))* y(k,696) &
 + (rxt(k,1790) +rxt(k,1791) +rxt(k,1792))* y(k,705) +rxt(k,1799)* y(k,839))* y(k,83)
         prod(k,805) = 0.
         loss(k,862) = ((rxt(k,1138) +rxt(k,1139) +rxt(k,1140))* y(k,693) + (rxt(k,1134) +rxt(k,1135) +rxt(k,1136) +rxt(k,1137)) &
* y(k,696) + (rxt(k,1130) +rxt(k,1131) +rxt(k,1132) +rxt(k,1133))* y(k,705) +rxt(k,1141)* y(k,839))* y(k,84)
         prod(k,862) = 0.
         loss(k,272) = ((rxt(k,282) +rxt(k,283) +rxt(k,284))* y(k,705))* y(k,85)
         prod(k,272) = 0.
         loss(k,331) = ((rxt(k,274) +rxt(k,275) +rxt(k,276))* y(k,705))* y(k,86)
         prod(k,331) = 0.
         loss(k,524) = ((rxt(k,697) +rxt(k,698) +rxt(k,699) +rxt(k,700))* y(k,705))* y(k,87)
         prod(k,524) = 0.
         loss(k,428) = ((rxt(k,285) +rxt(k,286) +rxt(k,287) +rxt(k,288))* y(k,705))* y(k,88)
         prod(k,428) = 0.
         loss(k,523) = ((rxt(k,787) +rxt(k,788) +rxt(k,789) +rxt(k,790))* y(k,705))* y(k,89)
         prod(k,523) = 0.
         loss(k,527) = ((rxt(k,871) +rxt(k,872) +rxt(k,873) +rxt(k,874))* y(k,705))* y(k,90)
         prod(k,527) = 0.
         loss(k,529) = ((rxt(k,748) +rxt(k,749) +rxt(k,750) +rxt(k,751))* y(k,705))* y(k,91)
         prod(k,529) = 0.
         loss(k,532) = ((rxt(k,875) +rxt(k,876) +rxt(k,877) +rxt(k,878))* y(k,705))* y(k,92)
         prod(k,532) = 0.
         loss(k,222) = ((rxt(k,258) +rxt(k,259) +rxt(k,260))* y(k,705))* y(k,93)
         prod(k,222) = 0.
         loss(k,729) = (rxt(k,2740)* y(k,693) + (rxt(k,2737) +rxt(k,2738) +rxt(k,2739))* y(k,705) + rxt(k,16) + rxt(k,17)) &
* y(k,94)
         prod(k,729) = 0.
         loss(k,545) = ((rxt(k,606) +rxt(k,607) +rxt(k,608) +rxt(k,609))* y(k,705))* y(k,95)
         prod(k,545) = 0.
         loss(k,490) = ((rxt(k,622) +rxt(k,623) +rxt(k,624) +rxt(k,625))* y(k,705))* y(k,96)
         prod(k,490) = 0.
         loss(k,546) = ((rxt(k,664) +rxt(k,665) +rxt(k,666) +rxt(k,667))* y(k,705))* y(k,97)
         prod(k,546) = 0.
         loss(k,364) = ((rxt(k,784) +rxt(k,785) +rxt(k,786))* y(k,705))* y(k,98)
         prod(k,364) = 0.
         loss(k,259) = ((rxt(k,868) +rxt(k,869) +rxt(k,870))* y(k,705))* y(k,99)
         prod(k,259) = 0.
         loss(k,260) = ((rxt(k,2846) +rxt(k,2847) +rxt(k,2848))* y(k,705))* y(k,100)
         prod(k,260) = 0.
         loss(k,772) = ((rxt(k,1575) +rxt(k,1576))* y(k,693) + (rxt(k,1571) +rxt(k,1572) +rxt(k,1573) +rxt(k,1574))* y(k,696) &
 + (rxt(k,1568) +rxt(k,1569) +rxt(k,1570))* y(k,705) +rxt(k,1577)* y(k,839))* y(k,101)
         prod(k,772) = 0.
         loss(k,465) = ((rxt(k,689) +rxt(k,690) +rxt(k,691) +rxt(k,692))* y(k,705))* y(k,102)
         prod(k,465) = 0.
         loss(k,384) = ((rxt(k,909) +rxt(k,910) +rxt(k,911))* y(k,705))* y(k,103)
         prod(k,384) = 0.
         loss(k,313) = ((rxt(k,668) +rxt(k,669) +rxt(k,670))* y(k,705))* y(k,104)
         prod(k,313) = 0.
         loss(k,598) = ((rxt(k,3762) +rxt(k,3763) +rxt(k,3764) +rxt(k,3765))* y(k,705))* y(k,105)
         prod(k,598) = 0.
         loss(k,603) = ((rxt(k,3715) +rxt(k,3716) +rxt(k,3717) +rxt(k,3718))* y(k,705))* y(k,106)
         prod(k,603) = 0.
         loss(k,390) = ((rxt(k,936) +rxt(k,937) +rxt(k,938))* y(k,705))* y(k,107)
         prod(k,390) = 0.
         loss(k,549) = ((rxt(k,3590) +rxt(k,3591) +rxt(k,3592) +rxt(k,3593))* y(k,705))* y(k,108)
         prod(k,549) = 0.
         loss(k,393) = ((rxt(k,752) +rxt(k,753) +rxt(k,754))* y(k,705))* y(k,109)
         prod(k,393) = 0.
         loss(k,399) = ((rxt(k,962) +rxt(k,963) +rxt(k,964))* y(k,705))* y(k,110)
         prod(k,399) = 0.
         loss(k,403) = ((rxt(k,879) +rxt(k,880) +rxt(k,881))* y(k,705))* y(k,111)
         prod(k,403) = 0.
         loss(k,318) = ((rxt(k,2682) +rxt(k,2683) +rxt(k,2684))* y(k,705))* y(k,112)
         prod(k,318) = 0.
         loss(k,866) = ((rxt(k,1540) +rxt(k,1541) +rxt(k,1542))* y(k,693) + (rxt(k,1536) +rxt(k,1537) +rxt(k,1538) +rxt(k,1539)) &
* y(k,696) + (rxt(k,1533) +rxt(k,1534) +rxt(k,1535))* y(k,705) +rxt(k,1543)* y(k,839))* y(k,113)
         prod(k,866) = 0.
         loss(k,280) = ((rxt(k,267) +rxt(k,268) +rxt(k,269))* y(k,705))* y(k,114)
         prod(k,280) = 0.
         loss(k,422) = ((rxt(k,626) +rxt(k,627) +rxt(k,628))* y(k,705))* y(k,115)
         prod(k,422) = 0.
         loss(k,425) = ((rxt(k,671) +rxt(k,672) +rxt(k,673))* y(k,705))* y(k,116)
         prod(k,425) = 0.
         loss(k,426) = ((rxt(k,794) +rxt(k,795) +rxt(k,796))* y(k,705))* y(k,117)
         prod(k,426) = 0.
         loss(k,427) = ((rxt(k,882) +rxt(k,883) +rxt(k,884))* y(k,705))* y(k,118)
         prod(k,427) = 0.
         loss(k,591) = ((rxt(k,2368) +rxt(k,2369) +rxt(k,2370) +rxt(k,2371))* y(k,705))* y(k,119)
         prod(k,591) = 0.
         loss(k,804) = ((rxt(k,1288) +rxt(k,1289))* y(k,693) + (rxt(k,1284) +rxt(k,1285) +rxt(k,1286) +rxt(k,1287))* y(k,696) &
 + (rxt(k,1281) +rxt(k,1282) +rxt(k,1283))* y(k,705) +rxt(k,1290)* y(k,839))* y(k,120)
         prod(k,804) = 0.
         loss(k,530) = ((rxt(k,3090) +rxt(k,3091) +rxt(k,3092) +rxt(k,3093))* y(k,705))* y(k,121)
         prod(k,530) = 0.
         loss(k,764) = ((rxt(k,1317) +rxt(k,1318))* y(k,693) + (rxt(k,1315) +rxt(k,1316))* y(k,696) + (rxt(k,1313) +rxt(k,1314)) &
* y(k,705) +rxt(k,1319)* y(k,839))* y(k,122)
         prod(k,764) = 0.
         loss(k,768) = ((rxt(k,1694) +rxt(k,1695) +rxt(k,1696))* y(k,693) + (rxt(k,1691) +rxt(k,1692) +rxt(k,1693))* y(k,696) &
 + (rxt(k,1689) +rxt(k,1690))* y(k,705) +rxt(k,1697)* y(k,839))* y(k,123)
         prod(k,768) = 0.
         loss(k,803) = ((rxt(k,1922) +rxt(k,1923) +rxt(k,1924) +rxt(k,1925))* y(k,693) + (rxt(k,1919) +rxt(k,1920) +rxt(k,1921)) &
* y(k,696) + (rxt(k,1915) +rxt(k,1916) +rxt(k,1917) +rxt(k,1918))* y(k,705) +rxt(k,1926)* y(k,839))* y(k,124)
         prod(k,803) = 0.
         loss(k,592) = ((rxt(k,3252) +rxt(k,3253) +rxt(k,3254) +rxt(k,3255))* y(k,705))* y(k,125)
         prod(k,592) = 0.
         loss(k,471) = ((rxt(k,3594) +rxt(k,3595) +rxt(k,3596) +rxt(k,3597))* y(k,705))* y(k,126)
         prod(k,471) = 0.
         loss(k,884) = ((rxt(k,1807) +rxt(k,1808) +rxt(k,1809) +rxt(k,1810))* y(k,693) + (rxt(k,1803) +rxt(k,1804) +rxt(k,1805) + &
rxt(k,1806))* y(k,696) + (rxt(k,1800) +rxt(k,1801) +rxt(k,1802))* y(k,705) +rxt(k,1811)* y(k,839))* y(k,127)
         prod(k,884) = 0.
         loss(k,837) = ((rxt(k,1911) +rxt(k,1912) +rxt(k,1913))* y(k,693) + (rxt(k,1907) +rxt(k,1908) +rxt(k,1909) +rxt(k,1910)) &
* y(k,696) + (rxt(k,1904) +rxt(k,1905) +rxt(k,1906))* y(k,705) +rxt(k,1914)* y(k,839))* y(k,128)
         prod(k,837) = 0.
         loss(k,466) = ((rxt(k,773) +rxt(k,774) +rxt(k,775) +rxt(k,776))* y(k,705))* y(k,129)
         prod(k,466) = 0.
         loss(k,381) = ((rxt(k,854) +rxt(k,855) +rxt(k,856))* y(k,705))* y(k,130)
         prod(k,381) = 0.
         loss(k,387) = ((rxt(k,991) +rxt(k,992) +rxt(k,993))* y(k,705))* y(k,131)
         prod(k,387) = 0.
         loss(k,605) = ((rxt(k,3735) +rxt(k,3736) +rxt(k,3737) +rxt(k,3738))* y(k,705))* y(k,132)
         prod(k,605) = 0.
         loss(k,388) = ((rxt(k,965) +rxt(k,966) +rxt(k,967))* y(k,705))* y(k,133)
         prod(k,388) = 0.
         loss(k,389) = ((rxt(k,830) +rxt(k,831) +rxt(k,832))* y(k,705))* y(k,134)
         prod(k,389) = 0.
         loss(k,269) = ((rxt(k,2811) +rxt(k,2812) +rxt(k,2813))* y(k,705) + rxt(k,18) + rxt(k,19))* y(k,135)
         prod(k,269) = 0.
         loss(k,892) = ((rxt(k,1528) +rxt(k,1529) +rxt(k,1530) +rxt(k,1531))* y(k,693) + (rxt(k,1524) +rxt(k,1525) +rxt(k,1526) + &
rxt(k,1527))* y(k,696) + (rxt(k,1521) +rxt(k,1522) +rxt(k,1523))* y(k,705) +rxt(k,1532)* y(k,839))* y(k,136)
         prod(k,892) = 0.
         loss(k,406) = ((rxt(k,629) +rxt(k,630) +rxt(k,631))* y(k,705))* y(k,137)
         prod(k,406) = 0.
         loss(k,419) = ((rxt(k,674) +rxt(k,675) +rxt(k,676))* y(k,705))* y(k,138)
         prod(k,419) = 0.
         loss(k,420) = ((rxt(k,755) +rxt(k,756) +rxt(k,757))* y(k,705))* y(k,139)
         prod(k,420) = 0.
         loss(k,421) = ((rxt(k,833) +rxt(k,834) +rxt(k,835))* y(k,705))* y(k,140)
         prod(k,421) = 0.
         loss(k,903) = ((rxt(k,1598) +rxt(k,1599) +rxt(k,1600) +rxt(k,1601))* y(k,693) + (rxt(k,1593) +rxt(k,1594) +rxt(k,1595) + &
rxt(k,1596) +rxt(k,1597))* y(k,696) + (rxt(k,1590) +rxt(k,1591) +rxt(k,1592))* y(k,705) +rxt(k,1602)* y(k,839)) &
* y(k,141)
         prod(k,903) = 0.
         loss(k,578) = ((rxt(k,3366) +rxt(k,3367) +rxt(k,3368) +rxt(k,3369))* y(k,705))* y(k,142)
         prod(k,578) = 0.
         loss(k,526) = ((rxt(k,3501) +rxt(k,3502) +rxt(k,3503) +rxt(k,3504))* y(k,705))* y(k,143)
         prod(k,526) = 0.
         loss(k,538) = ((rxt(k,3598) +rxt(k,3599) +rxt(k,3600) +rxt(k,3601))* y(k,705))* y(k,144)
         prod(k,538) = 0.
         loss(k,544) = ((rxt(k,857) +rxt(k,858) +rxt(k,859) +rxt(k,860))* y(k,705))* y(k,145)
         prod(k,544) = 0.
         loss(k,267) = ((rxt(k,677) +rxt(k,678) +rxt(k,679))* y(k,705))* y(k,146)
         prod(k,267) = 0.
         loss(k,386) = ((rxt(k,797) +rxt(k,798) +rxt(k,799))* y(k,705))* y(k,147)
         prod(k,386) = 0.
         loss(k,404) = ((rxt(k,885) +rxt(k,886) +rxt(k,887))* y(k,705))* y(k,148)
         prod(k,404) = 0.
         loss(k,491) = ((rxt(k,3602) +rxt(k,3603) +rxt(k,3604) +rxt(k,3605))* y(k,705))* y(k,149)
         prod(k,491) = 0.
         loss(k,94) = (rxt(k,2884)* y(k,705))* y(k,150)
         prod(k,94) = 0.
         loss(k,410) = ((rxt(k,3646) +rxt(k,3647) +rxt(k,3648))* y(k,705))* y(k,151)
         prod(k,410) = 0.
         loss(k,441) = ((rxt(k,1019) +rxt(k,1020) +rxt(k,1021))* y(k,705))* y(k,152)
         prod(k,441) = 0.
         loss(k,251) = ((rxt(k,791) +rxt(k,792) +rxt(k,793))* y(k,705))* y(k,153)
         prod(k,251) = 0.
         loss(k,304) = ((rxt(k,836) +rxt(k,837) +rxt(k,838))* y(k,705))* y(k,154)
         prod(k,304) = 0.
         loss(k,309) = ((rxt(k,912) +rxt(k,913) +rxt(k,914))* y(k,705))* y(k,155)
         prod(k,309) = 0.
         loss(k,262) = ((rxt(k,3360) +rxt(k,3361) +rxt(k,3362))* y(k,705))* y(k,156)
         prod(k,262) = 0.
         loss(k,378) = ((rxt(k,3363) +rxt(k,3364) +rxt(k,3365))* y(k,705))* y(k,157)
         prod(k,378) = 0.
         loss(k,383) = ((rxt(k,1048) +rxt(k,1049) +rxt(k,1050))* y(k,705))* y(k,158)
         prod(k,383) = 0.
         loss(k,766) = (rxt(k,4022)* y(k,546) + (rxt(k,4019) +rxt(k,4020) +rxt(k,4021))* y(k,693) + (rxt(k,4016) +rxt(k,4017) + &
rxt(k,4018))* y(k,696) + (rxt(k,4012) +rxt(k,4013) +rxt(k,4014) +rxt(k,4015))* y(k,705))* y(k,159)
         prod(k,766) = 0.
         loss(k,738) = (rxt(k,4062)* y(k,693) + (rxt(k,4058) +rxt(k,4059) +rxt(k,4060) +rxt(k,4061))* y(k,705))* y(k,160)
         prod(k,738) = 0.
         loss(k,353) = ((rxt(k,3308) +rxt(k,3309) +rxt(k,3310) +rxt(k,3311))* y(k,705))* y(k,161)
         prod(k,353) = 0.
         loss(k,370) = ((rxt(k,3185) +rxt(k,3186) +rxt(k,3187) +rxt(k,3188))* y(k,705))* y(k,162)
         prod(k,370) = 0.
         loss(k,95) = (rxt(k,3981)* y(k,705))* y(k,163)
         prod(k,95) = 0.
         loss(k,458) = ((rxt(k,2763) +rxt(k,2764) +rxt(k,2765) +rxt(k,2766))* y(k,705))* y(k,164)
         prod(k,458) = 0.
         loss(k,352) = ((rxt(k,2909) +rxt(k,2910) +rxt(k,2911))* y(k,705))* y(k,165)
         prod(k,352) = 0.
         loss(k,360) = ((rxt(k,3202) +rxt(k,3203) +rxt(k,3204))* y(k,705))* y(k,166)
         prod(k,360) = 0.
         loss(k,566) = ((rxt(k,3293) +rxt(k,3294) +rxt(k,3295) +rxt(k,3296))* y(k,705))* y(k,167)
         prod(k,566) = 0.
         loss(k,853) = ((rxt(k,1298) +rxt(k,1299) +rxt(k,1300))* y(k,693) + (rxt(k,1294) +rxt(k,1295) +rxt(k,1296) +rxt(k,1297)) &
* y(k,696) + (rxt(k,1291) +rxt(k,1292) +rxt(k,1293))* y(k,705) +rxt(k,1301)* y(k,839))* y(k,168)
         prod(k,853) = 0.
         loss(k,681) = ((rxt(k,3730) +rxt(k,3731) +rxt(k,3732) +rxt(k,3733) +rxt(k,3734))* y(k,705))* y(k,169)
         prod(k,681) = 0.
         loss(k,365) = ((rxt(k,3199) +rxt(k,3200) +rxt(k,3201))* y(k,705))* y(k,170)
         prod(k,365) = 0.
         loss(k,261) = ((rxt(k,3287) +rxt(k,3288) +rxt(k,3289))* y(k,705))* y(k,171)
         prod(k,261) = 0.
         loss(k,854) = ((rxt(k,3586) +rxt(k,3587) +rxt(k,3588))* y(k,693) + (rxt(k,3584) +rxt(k,3585))* y(k,696) + (rxt(k,3580) + &
rxt(k,3581) +rxt(k,3582) +rxt(k,3583))* y(k,705) +rxt(k,3589)* y(k,839))* y(k,172)
         prod(k,854) = 0.
         loss(k,371) = ((rxt(k,3505) +rxt(k,3506) +rxt(k,3507) +rxt(k,3508))* y(k,705))* y(k,173)
         prod(k,371) = 0.
         loss(k,468) = ((rxt(k,3312) +rxt(k,3313) +rxt(k,3314) +rxt(k,3315))* y(k,705))* y(k,174)
         prod(k,468) = 0.
         loss(k,500) = ((rxt(k,3136) +rxt(k,3137) +rxt(k,3138) +rxt(k,3139))* y(k,705))* y(k,175)
         prod(k,500) = 0.
         loss(k,824) = ((rxt(k,1161) +rxt(k,1162) +rxt(k,1163))* y(k,693) + (rxt(k,1157) +rxt(k,1158) +rxt(k,1159) +rxt(k,1160)) &
* y(k,696) + (rxt(k,1154) +rxt(k,1155) +rxt(k,1156))* y(k,705) +rxt(k,1164)* y(k,839))* y(k,176)
         prod(k,824) = 0.
         loss(k,847) = ((rxt(k,1309) +rxt(k,1310) +rxt(k,1311))* y(k,693) + (rxt(k,1305) +rxt(k,1306) +rxt(k,1307) +rxt(k,1308)) &
* y(k,696) + (rxt(k,1302) +rxt(k,1303) +rxt(k,1304))* y(k,705) +rxt(k,1312)* y(k,839))* y(k,177)
         prod(k,847) = 0.
         loss(k,870) = ((rxt(k,1564) +rxt(k,1565) +rxt(k,1566))* y(k,693) + (rxt(k,1560) +rxt(k,1561) +rxt(k,1562) +rxt(k,1563)) &
* y(k,696) + (rxt(k,1557) +rxt(k,1558) +rxt(k,1559))* y(k,705) +rxt(k,1567)* y(k,839))* y(k,178)
         prod(k,870) = 0.
         loss(k,623) = ((rxt(k,2975) +rxt(k,2976) +rxt(k,2977) +rxt(k,2978))* y(k,705))* y(k,179)
         prod(k,623) = 0.
         loss(k,786) = ((rxt(k,1171) +rxt(k,1172) +rxt(k,1173))* y(k,693) + (rxt(k,1167) +rxt(k,1168) +rxt(k,1169) +rxt(k,1170)) &
* y(k,696) + (rxt(k,1165) +rxt(k,1166))* y(k,705) +rxt(k,1174)* y(k,839))* y(k,180)
         prod(k,786) = 0.
         loss(k,891) = ((rxt(k,1327) +rxt(k,1328) +rxt(k,1329))* y(k,693) + (rxt(k,1323) +rxt(k,1324) +rxt(k,1325) +rxt(k,1326)) &
* y(k,696) + (rxt(k,1320) +rxt(k,1321) +rxt(k,1322))* y(k,705) +rxt(k,1330)* y(k,839))* y(k,181)
         prod(k,891) = 0.
         loss(k,909) = ((rxt(k,1612) +rxt(k,1613) +rxt(k,1614) +rxt(k,1615))* y(k,693) + (rxt(k,1607) +rxt(k,1608) +rxt(k,1609) + &
rxt(k,1610) +rxt(k,1611))* y(k,696) + (rxt(k,1603) +rxt(k,1604) +rxt(k,1605) +rxt(k,1606))* y(k,705) +rxt(k,1616) &
* y(k,839))* y(k,182)
         prod(k,909) = 0.
         loss(k,902) = ((rxt(k,1845) +rxt(k,1846) +rxt(k,1847) +rxt(k,1848))* y(k,693) + (rxt(k,1840) +rxt(k,1841) +rxt(k,1842) + &
rxt(k,1843) +rxt(k,1844))* y(k,696) + (rxt(k,1836) +rxt(k,1837) +rxt(k,1838) +rxt(k,1839))* y(k,705) +rxt(k,1849) &
* y(k,839))* y(k,183)
         prod(k,902) = 0.
         loss(k,413) = ((rxt(k,994) +rxt(k,995) +rxt(k,996))* y(k,705))* y(k,184)
         prod(k,413) = 0.
         loss(k,415) = ((rxt(k,888) +rxt(k,889) +rxt(k,890))* y(k,705))* y(k,185)
         prod(k,415) = 0.
         loss(k,505) = ((rxt(k,3074) +rxt(k,3075) +rxt(k,3076))* y(k,705) + rxt(k,20) + rxt(k,21))* y(k,186)
         prod(k,505) = 0.
         loss(k,512) = ((rxt(k,3256) +rxt(k,3257) +rxt(k,3258) +rxt(k,3259))* y(k,705))* y(k,187)
         prod(k,512) = 0.
         loss(k,515) = ((rxt(k,3370) +rxt(k,3371) +rxt(k,3372) +rxt(k,3373))* y(k,705))* y(k,188)
         prod(k,515) = 0.
         loss(k,423) = ((rxt(k,3606) +rxt(k,3607) +rxt(k,3608) +rxt(k,3609))* y(k,705))* y(k,189)
         prod(k,423) = 0.
         loss(k,536) = ((rxt(k,2777) +rxt(k,2778) +rxt(k,2779) +rxt(k,2780))* y(k,705))* y(k,190)
         prod(k,536) = 0.
         loss(k,250) = ((rxt(k,250) +rxt(k,251) +rxt(k,252))* y(k,705))* y(k,191)
         prod(k,250) = 0.
         loss(k,727) = (rxt(k,2588)* y(k,693) + (rxt(k,2585) +rxt(k,2586) +rxt(k,2587))* y(k,705) + rxt(k,22) + rxt(k,23) &
 + rxt(k,24))* y(k,192)
         prod(k,727) = 0.
         loss(k,443) = ((rxt(k,603) +rxt(k,604) +rxt(k,605))* y(k,705))* y(k,193)
         prod(k,443) = 0.
         loss(k,445) = ((rxt(k,610) +rxt(k,611) +rxt(k,612))* y(k,705))* y(k,194)
         prod(k,445) = 0.
         loss(k,296) = ((rxt(k,632) +rxt(k,633) +rxt(k,634))* y(k,705))* y(k,195)
         prod(k,296) = 0.
         loss(k,297) = ((rxt(k,680) +rxt(k,681) +rxt(k,682))* y(k,705))* y(k,196)
         prod(k,297) = 0.
         loss(k,249) = ((rxt(k,758) +rxt(k,759) +rxt(k,760))* y(k,705))* y(k,197)
         prod(k,249) = 0.
         loss(k,191) = ((rxt(k,839) +rxt(k,840) +rxt(k,841))* y(k,705))* y(k,198)
         prod(k,191) = 0.
         loss(k,356) = ((rxt(k,2692) +rxt(k,2693) +rxt(k,2694))* y(k,705))* y(k,199)
         prod(k,356) = 0.
         loss(k,728) = (rxt(k,3070)* y(k,693) + (rxt(k,3067) +rxt(k,3068) +rxt(k,3069))* y(k,705) + rxt(k,25) + rxt(k,26)) &
* y(k,200)
         prod(k,728) = 0.
         loss(k,96) = (rxt(k,2712)* y(k,705))* y(k,201)
         prod(k,96) = 0.
         loss(k,828) = ((rxt(k,1660) +rxt(k,1661) +rxt(k,1662))* y(k,693) + (rxt(k,1656) +rxt(k,1657) +rxt(k,1658) +rxt(k,1659)) &
* y(k,696) + (rxt(k,1653) +rxt(k,1654) +rxt(k,1655))* y(k,705) +rxt(k,1663)* y(k,839))* y(k,202)
         prod(k,828) = 0.
         loss(k,122) = ((rxt(k,3917) +rxt(k,3918))* y(k,705))* y(k,203)
         prod(k,122) = 0.
         loss(k,454) = ((rxt(k,3027) +rxt(k,3028) +rxt(k,3029) +rxt(k,3030))* y(k,705))* y(k,204)
         prod(k,454) = 0.
         loss(k,380) = ((rxt(k,3340) +rxt(k,3341) +rxt(k,3342))* y(k,705))* y(k,205)
         prod(k,380) = 0.
         loss(k,392) = ((rxt(k,2853) +rxt(k,2854) +rxt(k,2855))* y(k,705))* y(k,206)
         prod(k,392) = 0.
         loss(k,319) = ((rxt(k,761) +rxt(k,762) +rxt(k,763))* y(k,705))* y(k,207)
         prod(k,319) = 0.
         loss(k,874) = ((rxt(k,1553) +rxt(k,1554) +rxt(k,1555))* y(k,693) + (rxt(k,1548) +rxt(k,1549) +rxt(k,1550) +rxt(k,1551) + &
rxt(k,1552))* y(k,696) + (rxt(k,1544) +rxt(k,1545) +rxt(k,1546) +rxt(k,1547))* y(k,705) +rxt(k,1556)* y(k,839)) &
* y(k,208)
         prod(k,874) = 0.
         loss(k,482) = ((rxt(k,635) +rxt(k,636) +rxt(k,637) +rxt(k,638))* y(k,705))* y(k,209)
         prod(k,482) = 0.
         loss(k,483) = ((rxt(k,693) +rxt(k,694) +rxt(k,695) +rxt(k,696))* y(k,705))* y(k,210)
         prod(k,483) = 0.
         loss(k,486) = ((rxt(k,777) +rxt(k,778) +rxt(k,779) +rxt(k,780))* y(k,705))* y(k,211)
         prod(k,486) = 0.
         loss(k,487) = ((rxt(k,861) +rxt(k,862) +rxt(k,863) +rxt(k,864))* y(k,705))* y(k,212)
         prod(k,487) = 0.
         loss(k,898) = ((rxt(k,1253) +rxt(k,1254))* y(k,693) + (rxt(k,1249) +rxt(k,1250) +rxt(k,1251) +rxt(k,1252))* y(k,696) &
 + (rxt(k,1245) +rxt(k,1246) +rxt(k,1247) +rxt(k,1248))* y(k,705) +rxt(k,1255)* y(k,839))* y(k,213)
         prod(k,898) = 0.
         loss(k,321) = ((rxt(k,842) +rxt(k,843) +rxt(k,844))* y(k,705))* y(k,214)
         prod(k,321) = 0.
         loss(k,865) = ((rxt(k,1504) +rxt(k,1505) +rxt(k,1506))* y(k,693) + (rxt(k,1499) +rxt(k,1500) +rxt(k,1501) +rxt(k,1502) + &
rxt(k,1503))* y(k,696) + (rxt(k,1495) +rxt(k,1496) +rxt(k,1497) +rxt(k,1498))* y(k,705) +rxt(k,1507)* y(k,839)) &
* y(k,215)
         prod(k,865) = 0.
         loss(k,901) = ((rxt(k,1685) +rxt(k,1686) +rxt(k,1687))* y(k,693) + (rxt(k,1680) +rxt(k,1681) +rxt(k,1682) +rxt(k,1683) + &
rxt(k,1684))* y(k,696) + (rxt(k,1677) +rxt(k,1678) +rxt(k,1679))* y(k,705) +rxt(k,1688)* y(k,839))* y(k,216)
         prod(k,901) = 0.
         loss(k,326) = ((rxt(k,701) +rxt(k,702) +rxt(k,703))* y(k,705))* y(k,217)
         prod(k,326) = 0.
         loss(k,327) = ((rxt(k,803) +rxt(k,804) +rxt(k,805))* y(k,705))* y(k,218)
         prod(k,327) = 0.
         loss(k,887) = ((rxt(k,1978) +rxt(k,1979) +rxt(k,1980) +rxt(k,1981))* y(k,693) + (rxt(k,1974) +rxt(k,1975) +rxt(k,1976) + &
rxt(k,1977))* y(k,696) + (rxt(k,1971) +rxt(k,1972) +rxt(k,1973))* y(k,705) +rxt(k,1982)* y(k,839))* y(k,219)
         prod(k,887) = 0.
         loss(k,522) = ((rxt(k,3509) +rxt(k,3510) +rxt(k,3511) +rxt(k,3512))* y(k,705))* y(k,220)
         prod(k,522) = 0.
         loss(k,533) = ((rxt(k,3766) +rxt(k,3767) +rxt(k,3768) +rxt(k,3769))* y(k,705))* y(k,221)
         prod(k,533) = 0.
         loss(k,599) = ((rxt(k,3696) +rxt(k,3697) +rxt(k,3698) +rxt(k,3699))* y(k,705))* y(k,222)
         prod(k,599) = 0.
         loss(k,541) = ((rxt(k,3719) +rxt(k,3720) +rxt(k,3721) +rxt(k,3722))* y(k,705))* y(k,223)
         prod(k,541) = 0.
         loss(k,339) = ((rxt(k,915) +rxt(k,916) +rxt(k,917))* y(k,705))* y(k,224)
         prod(k,339) = 0.
         loss(k,337) = ((rxt(k,764) +rxt(k,765) +rxt(k,766))* y(k,705))* y(k,225)
         prod(k,337) = 0.
         loss(k,567) = ((rxt(k,3513) +rxt(k,3514) +rxt(k,3515) +rxt(k,3516))* y(k,705))* y(k,226)
         prod(k,567) = 0.
         loss(k,504) = ((rxt(k,3610) +rxt(k,3611) +rxt(k,3612) +rxt(k,3613))* y(k,705))* y(k,227)
         prod(k,504) = 0.
         loss(k,570) = ((rxt(k,3723) +rxt(k,3724) +rxt(k,3725) +rxt(k,3726))* y(k,705))* y(k,228)
         prod(k,570) = 0.
         loss(k,225) = ((rxt(k,939) +rxt(k,940) +rxt(k,941))* y(k,705))* y(k,229)
         prod(k,225) = 0.
         loss(k,291) = ((rxt(k,942) +rxt(k,943) +rxt(k,944))* y(k,705))* y(k,230)
         prod(k,291) = 0.
         loss(k,308) = ((rxt(k,968) +rxt(k,969) +rxt(k,970))* y(k,705))* y(k,231)
         prod(k,308) = 0.
         loss(k,502) = ((rxt(k,3614) +rxt(k,3615) +rxt(k,3616) +rxt(k,3617))* y(k,705))* y(k,232)
         prod(k,502) = 0.
         loss(k,503) = ((rxt(k,3649) +rxt(k,3650) +rxt(k,3651) +rxt(k,3652))* y(k,705))* y(k,233)
         prod(k,503) = 0.
         loss(k,226) = ((rxt(k,971) +rxt(k,972) +rxt(k,973))* y(k,705))* y(k,234)
         prod(k,226) = 0.
         loss(k,227) = ((rxt(k,997) +rxt(k,998) +rxt(k,999))* y(k,705))* y(k,235)
         prod(k,227) = 0.
         loss(k,228) = ((rxt(k,1022) +rxt(k,1023) +rxt(k,1024))* y(k,705))* y(k,236)
         prod(k,228) = 0.
         loss(k,513) = ((rxt(k,3490) +rxt(k,3491) +rxt(k,3492) +rxt(k,3493))* y(k,705))* y(k,237)
         prod(k,513) = 0.
         loss(k,229) = ((rxt(k,1000) +rxt(k,1001) +rxt(k,1002))* y(k,705))* y(k,238)
         prod(k,229) = 0.
         loss(k,230) = ((rxt(k,1025) +rxt(k,1026) +rxt(k,1027))* y(k,705))* y(k,239)
         prod(k,230) = 0.
         loss(k,695) = ((rxt(k,2276) +rxt(k,2277) +rxt(k,2278) +rxt(k,2279) +rxt(k,2280))* y(k,705))* y(k,240)
         prod(k,695) = 0.
         loss(k,693) = ((rxt(k,2317) +rxt(k,2318) +rxt(k,2319) +rxt(k,2320) +rxt(k,2321))* y(k,705))* y(k,241)
         prod(k,693) = 0.
         loss(k,699) = ((rxt(k,2354) +rxt(k,2355) +rxt(k,2356) +rxt(k,2357) +rxt(k,2358))* y(k,705))* y(k,242)
         prod(k,699) = 0.
         loss(k,689) = ((rxt(k,2390) +rxt(k,2391) +rxt(k,2392) +rxt(k,2393) +rxt(k,2394))* y(k,705))* y(k,243)
         prod(k,689) = 0.
         loss(k,690) = ((rxt(k,2422) +rxt(k,2423) +rxt(k,2424) +rxt(k,2425) +rxt(k,2426))* y(k,705))* y(k,244)
         prod(k,690) = 0.
         loss(k,697) = ((rxt(k,2454) +rxt(k,2455) +rxt(k,2456) +rxt(k,2457) +rxt(k,2458))* y(k,705))* y(k,245)
         prod(k,697) = 0.
         loss(k,698) = ((rxt(k,2486) +rxt(k,2487) +rxt(k,2488) +rxt(k,2489) +rxt(k,2490))* y(k,705))* y(k,246)
         prod(k,698) = 0.
         loss(k,298) = ((rxt(k,2767) +rxt(k,2768) +rxt(k,2769))* y(k,705))* y(k,247)
         prod(k,298) = 0.
         loss(k,300) = ((rxt(k,3205) +rxt(k,3206) +rxt(k,3207))* y(k,705))* y(k,248)
         prod(k,300) = 0.
         loss(k,826) = ((rxt(k,2010) +rxt(k,2011) +rxt(k,2012) +rxt(k,2013))* y(k,693) + (rxt(k,2006) +rxt(k,2007) +rxt(k,2008) + &
rxt(k,2009))* y(k,696) + (rxt(k,2004) +rxt(k,2005))* y(k,705) +rxt(k,2014)* y(k,839))* y(k,249)
         prod(k,826) = 0.
         loss(k,161) = ((rxt(k,3912) +rxt(k,3913) +rxt(k,3914))* y(k,705))* y(k,250)
         prod(k,161) = 0.
         loss(k,354) = ((rxt(k,704) +rxt(k,705) +rxt(k,706))* y(k,705))* y(k,251)
         prod(k,354) = 0.
         loss(k,355) = ((rxt(k,3739) +rxt(k,3740) +rxt(k,3741))* y(k,705))* y(k,252)
         prod(k,355) = 0.
         loss(k,481) = ((rxt(k,3700) +rxt(k,3701) +rxt(k,3702) +rxt(k,3703))* y(k,705))* y(k,253)
         prod(k,481) = 0.
         loss(k,514) = ((rxt(k,3374) +rxt(k,3375) +rxt(k,3376) +rxt(k,3377))* y(k,705))* y(k,254)
         prod(k,514) = 0.
         loss(k,363) = ((rxt(k,3517) +rxt(k,3518) +rxt(k,3519))* y(k,705))* y(k,255)
         prod(k,363) = 0.
         loss(k,305) = ((rxt(k,3618) +rxt(k,3619) +rxt(k,3620))* y(k,705))* y(k,256)
         prod(k,305) = 0.
         loss(k,873) = ((rxt(k,1624) +rxt(k,1625) +rxt(k,1626))* y(k,693) + (rxt(k,1620) +rxt(k,1621) +rxt(k,1622) +rxt(k,1623)) &
* y(k,696) + (rxt(k,1617) +rxt(k,1618) +rxt(k,1619))* y(k,705) +rxt(k,1627)* y(k,839))* y(k,257)
         prod(k,873) = 0.
         loss(k,307) = ((rxt(k,642) +rxt(k,643) +rxt(k,644))* y(k,705))* y(k,258)
         prod(k,307) = 0.
         loss(k,231) = ((rxt(k,806) +rxt(k,807) +rxt(k,808))* y(k,705))* y(k,259)
         prod(k,231) = 0.
         loss(k,601) = ((rxt(k,2856) +rxt(k,2857) +rxt(k,2858) +rxt(k,2859))* y(k,705))* y(k,260)
         prod(k,601) = 0.
         loss(k,369) = ((rxt(k,3653) +rxt(k,3654) +rxt(k,3655))* y(k,705))* y(k,261)
         prod(k,369) = 0.
         loss(k,162) = ((rxt(k,1232) +rxt(k,1233))* y(k,705))* y(k,262)
         prod(k,162) = 0.
         loss(k,897) = ((rxt(k,1150) +rxt(k,1151) +rxt(k,1152))* y(k,693) + (rxt(k,1146) +rxt(k,1147) +rxt(k,1148) +rxt(k,1149)) &
* y(k,696) + (rxt(k,1142) +rxt(k,1143) +rxt(k,1144) +rxt(k,1145))* y(k,705) +rxt(k,1153)* y(k,839))* y(k,263)
         prod(k,897) = 0.
         loss(k,843) = ((rxt(k,1265) +rxt(k,1266) +rxt(k,1267))* y(k,693) + (rxt(k,1260) +rxt(k,1261) +rxt(k,1262) +rxt(k,1263) + &
rxt(k,1264))* y(k,696) + (rxt(k,1256) +rxt(k,1257) +rxt(k,1258) +rxt(k,1259))* y(k,705) +rxt(k,1268)* y(k,839)) &
* y(k,264)
         prod(k,843) = 0.
         loss(k,848) = ((rxt(k,1517) +rxt(k,1518) +rxt(k,1519))* y(k,693) + (rxt(k,1512) +rxt(k,1513) +rxt(k,1514) +rxt(k,1515) + &
rxt(k,1516))* y(k,696) + (rxt(k,1508) +rxt(k,1509) +rxt(k,1510) +rxt(k,1511))* y(k,705) +rxt(k,1520)* y(k,839)) &
* y(k,265)
         prod(k,848) = 0.
         loss(k,572) = ((rxt(k,3083) +rxt(k,3084) +rxt(k,3085) +rxt(k,3086))* y(k,705) + rxt(k,27) + rxt(k,28) + rxt(k,29)) &
* y(k,266)
         prod(k,572) = 0.
         loss(k,863) = ((rxt(k,1820) +rxt(k,1821) +rxt(k,1822))* y(k,693) + (rxt(k,1816) +rxt(k,1817) +rxt(k,1818) +rxt(k,1819)) &
* y(k,696) + (rxt(k,1812) +rxt(k,1813) +rxt(k,1814) +rxt(k,1815))* y(k,705) +rxt(k,1823)* y(k,839))* y(k,267)
         prod(k,863) = 0.
         loss(k,597) = ((rxt(k,2842) +rxt(k,2843) +rxt(k,2844) +rxt(k,2845))* y(k,705))* y(k,268)
         prod(k,597) = 0.
         loss(k,893) = ((rxt(k,1672) +rxt(k,1673) +rxt(k,1674) +rxt(k,1675))* y(k,693) + (rxt(k,1667) +rxt(k,1668) +rxt(k,1669) + &
rxt(k,1670) +rxt(k,1671))* y(k,696) + (rxt(k,1664) +rxt(k,1665) +rxt(k,1666))* y(k,705) +rxt(k,1676)* y(k,839)) &
* y(k,269)
         prod(k,893) = 0.
         loss(k,740) = ((rxt(k,2745) +rxt(k,2746) +rxt(k,2747) +rxt(k,2748))* y(k,693) + (rxt(k,2741) +rxt(k,2742) +rxt(k,2743) + &
rxt(k,2744))* y(k,705) + rxt(k,30) + rxt(k,31) + rxt(k,32) + het_rates(k,28))* y(k,270)
         prod(k,740) = 0.
         loss(k,488) = ((rxt(k,3260) +rxt(k,3261) +rxt(k,3262) +rxt(k,3263))* y(k,705))* y(k,271)
         prod(k,488) = 0.
         loss(k,496) = ((rxt(k,3378) +rxt(k,3379) +rxt(k,3380) +rxt(k,3381))* y(k,705))* y(k,272)
         prod(k,496) = 0.
         loss(k,412) = ((rxt(k,3520) +rxt(k,3521) +rxt(k,3522))* y(k,705))* y(k,273)
         prod(k,412) = 0.
         loss(k,232) = ((rxt(k,918) +rxt(k,919) +rxt(k,920))* y(k,705))* y(k,274)
         prod(k,232) = 0.
         loss(k,233) = ((rxt(k,945) +rxt(k,946) +rxt(k,947))* y(k,705))* y(k,275)
         prod(k,233) = 0.
         loss(k,234) = ((rxt(k,974) +rxt(k,975) +rxt(k,976))* y(k,705))* y(k,276)
         prod(k,234) = 0.
         loss(k,235) = ((rxt(k,1003) +rxt(k,1004) +rxt(k,1005))* y(k,705))* y(k,277)
         prod(k,235) = 0.
         loss(k,236) = ((rxt(k,1028) +rxt(k,1029) +rxt(k,1030))* y(k,705))* y(k,278)
         prod(k,236) = 0.
         loss(k,237) = ((rxt(k,1051) +rxt(k,1052) +rxt(k,1053))* y(k,705))* y(k,279)
         prod(k,237) = 0.
         loss(k,330) = ((rxt(k,613) +rxt(k,614) +rxt(k,615))* y(k,705))* y(k,280)
         prod(k,330) = 0.
         loss(k,332) = ((rxt(k,639) +rxt(k,640) +rxt(k,641))* y(k,705))* y(k,281)
         prod(k,332) = 0.
         loss(k,333) = ((rxt(k,683) +rxt(k,684) +rxt(k,685))* y(k,705))* y(k,282)
         prod(k,333) = 0.
         loss(k,336) = ((rxt(k,800) +rxt(k,801) +rxt(k,802))* y(k,705))* y(k,283)
         prod(k,336) = 0.
         loss(k,238) = ((rxt(k,845) +rxt(k,846) +rxt(k,847))* y(k,705))* y(k,284)
         prod(k,238) = 0.
         loss(k,907) = ((rxt(k,1419) +rxt(k,1420) +rxt(k,1421) +rxt(k,1422))* y(k,693) + (rxt(k,1415) +rxt(k,1416) +rxt(k,1417) + &
rxt(k,1418))* y(k,696) + (rxt(k,1411) +rxt(k,1412) +rxt(k,1413) +rxt(k,1414))* y(k,705) +rxt(k,1423)* y(k,839)) &
* y(k,285)
         prod(k,907) = 0.
         loss(k,448) = ((rxt(k,2695) +rxt(k,2696) +rxt(k,2697))* y(k,705))* y(k,286)
         prod(k,448) = 0.
         loss(k,608) = ((rxt(k,2860) +rxt(k,2861) +rxt(k,2862) +rxt(k,2863))* y(k,705))* y(k,287)
         prod(k,608) = 0.
         loss(k,97) = (rxt(k,2713)* y(k,705))* y(k,288)
         prod(k,97) = 0.
         loss(k,610) = ((rxt(k,2993) +rxt(k,2994) +rxt(k,2995) +rxt(k,2996))* y(k,705))* y(k,289)
         prod(k,610) = 0.
         loss(k,890) = ((rxt(k,1636) +rxt(k,1637) +rxt(k,1638) +rxt(k,1639))* y(k,693) + (rxt(k,1631) +rxt(k,1632) +rxt(k,1633) + &
rxt(k,1634) +rxt(k,1635))* y(k,696) + (rxt(k,1628) +rxt(k,1629) +rxt(k,1630))* y(k,705) +rxt(k,1640)* y(k,839)) &
* y(k,290)
         prod(k,890) = 0.
         loss(k,876) = ((rxt(k,1946) +rxt(k,1947) +rxt(k,1948))* y(k,693) + (rxt(k,1941) +rxt(k,1942) +rxt(k,1943) +rxt(k,1944) + &
rxt(k,1945))* y(k,696) + (rxt(k,1937) +rxt(k,1938) +rxt(k,1939) +rxt(k,1940))* y(k,705) +rxt(k,1949)* y(k,839)) &
* y(k,291)
         prod(k,876) = 0.
         loss(k,397) = ((rxt(k,781) +rxt(k,782) +rxt(k,783))* y(k,705))* y(k,292)
         prod(k,397) = 0.
         loss(k,290) = ((rxt(k,865) +rxt(k,866) +rxt(k,867))* y(k,705))* y(k,293)
         prod(k,290) = 0.
         loss(k,556) = ((rxt(k,3094) +rxt(k,3095) +rxt(k,3096) +rxt(k,3097))* y(k,705))* y(k,294)
         prod(k,556) = 0.
         loss(k,558) = ((rxt(k,3523) +rxt(k,3524) +rxt(k,3525) +rxt(k,3526))* y(k,705))* y(k,295)
         prod(k,558) = 0.
         loss(k,540) = ((rxt(k,3621) +rxt(k,3622) +rxt(k,3623) +rxt(k,3624))* y(k,705))* y(k,296)
         prod(k,540) = 0.
         loss(k,606) = ((rxt(k,3625) +rxt(k,3626) +rxt(k,3627) +rxt(k,3628))* y(k,705))* y(k,297)
         prod(k,606) = 0.
         loss(k,477) = ((rxt(k,3656) +rxt(k,3657) +rxt(k,3658) +rxt(k,3659))* y(k,705))* y(k,298)
         prod(k,477) = 0.
         loss(k,607) = ((rxt(k,3742) +rxt(k,3743) +rxt(k,3744) +rxt(k,3745))* y(k,705))* y(k,299)
         prod(k,607) = 0.
         loss(k,519) = ((rxt(k,3704) +rxt(k,3705) +rxt(k,3706) +rxt(k,3707))* y(k,705))* y(k,300)
         prod(k,519) = 0.
         loss(k,239) = ((rxt(k,1054) +rxt(k,1055) +rxt(k,1056))* y(k,705))* y(k,301)
         prod(k,239) = 0.
         loss(k,706) = ((rxt(k,2281) +rxt(k,2282) +rxt(k,2283) +rxt(k,2284) +rxt(k,2285))* y(k,705))* y(k,302)
         prod(k,706) = 0.
         loss(k,702) = ((rxt(k,2322) +rxt(k,2323) +rxt(k,2324) +rxt(k,2325) +rxt(k,2326))* y(k,705))* y(k,303)
         prod(k,702) = 0.
         loss(k,704) = ((rxt(k,2359) +rxt(k,2360) +rxt(k,2361) +rxt(k,2362) +rxt(k,2363))* y(k,705))* y(k,304)
         prod(k,704) = 0.
         loss(k,708) = ((rxt(k,2395) +rxt(k,2396) +rxt(k,2397) +rxt(k,2398) +rxt(k,2399))* y(k,705))* y(k,305)
         prod(k,708) = 0.
         loss(k,709) = ((rxt(k,2427) +rxt(k,2428) +rxt(k,2429) +rxt(k,2430) +rxt(k,2431))* y(k,705))* y(k,306)
         prod(k,709) = 0.
         loss(k,711) = ((rxt(k,2459) +rxt(k,2460) +rxt(k,2461) +rxt(k,2462) +rxt(k,2463))* y(k,705))* y(k,307)
         prod(k,711) = 0.
         loss(k,712) = ((rxt(k,2491) +rxt(k,2492) +rxt(k,2493) +rxt(k,2494) +rxt(k,2495))* y(k,705))* y(k,308)
         prod(k,712) = 0.
         loss(k,240) = ((rxt(k,3208) +rxt(k,3209) +rxt(k,3210))* y(k,705))* y(k,309)
         prod(k,240) = 0.
         loss(k,241) = ((rxt(k,767) +rxt(k,768) +rxt(k,769))* y(k,705))* y(k,310)
         prod(k,241) = 0.
         loss(k,192) = ((rxt(k,891) +rxt(k,892) +rxt(k,893))* y(k,705))* y(k,311)
         prod(k,192) = 0.
         loss(k,844) = ((rxt(k,1278) +rxt(k,1279))* y(k,693) + (rxt(k,1273) +rxt(k,1274) +rxt(k,1275) +rxt(k,1276) +rxt(k,1277)) &
* y(k,696) + (rxt(k,1269) +rxt(k,1270) +rxt(k,1271) +rxt(k,1272))* y(k,705) +rxt(k,1280)* y(k,839))* y(k,312)
         prod(k,844) = 0.
         loss(k,470) = ((rxt(k,3264) +rxt(k,3265) +rxt(k,3266) +rxt(k,3267))* y(k,705))* y(k,313)
         prod(k,470) = 0.
         loss(k,472) = ((rxt(k,3382) +rxt(k,3383) +rxt(k,3384) +rxt(k,3385))* y(k,705))* y(k,314)
         prod(k,472) = 0.
         loss(k,476) = ((rxt(k,3527) +rxt(k,3528) +rxt(k,3529) +rxt(k,3530))* y(k,705))* y(k,315)
         prod(k,476) = 0.
         loss(k,292) = ((rxt(k,3629) +rxt(k,3630) +rxt(k,3631))* y(k,705))* y(k,316)
         prod(k,292) = 0.
         loss(k,882) = ((rxt(k,1479) +rxt(k,1480) +rxt(k,1481) +rxt(k,1482))* y(k,693) + (rxt(k,1475) +rxt(k,1476) +rxt(k,1477) + &
rxt(k,1478))* y(k,696) + (rxt(k,1471) +rxt(k,1472) +rxt(k,1473) +rxt(k,1474))* y(k,705) +rxt(k,1483)* y(k,839)) &
* y(k,317)
         prod(k,882) = 0.
         loss(k,242) = ((rxt(k,921) +rxt(k,922) +rxt(k,923))* y(k,705))* y(k,318)
         prod(k,242) = 0.
         loss(k,293) = ((rxt(k,686) +rxt(k,687) +rxt(k,688))* y(k,705))* y(k,319)
         prod(k,293) = 0.
         loss(k,294) = ((rxt(k,770) +rxt(k,771) +rxt(k,772))* y(k,705))* y(k,320)
         prod(k,294) = 0.
         loss(k,295) = ((rxt(k,848) +rxt(k,849) +rxt(k,850))* y(k,705))* y(k,321)
         prod(k,295) = 0.
         loss(k,243) = ((rxt(k,851) +rxt(k,852) +rxt(k,853))* y(k,705))* y(k,322)
         prod(k,243) = 0.
         loss(k,244) = ((rxt(k,3343) +rxt(k,3344) +rxt(k,3345))* y(k,705))* y(k,323)
         prod(k,244) = 0.
         loss(k,619) = ((rxt(k,2286) +rxt(k,2287) +rxt(k,2288) +rxt(k,2289))* y(k,705))* y(k,324)
         prod(k,619) = 0.
         loss(k,625) = ((rxt(k,2327) +rxt(k,2328) +rxt(k,2329) +rxt(k,2330))* y(k,705))* y(k,325)
         prod(k,625) = 0.
         loss(k,626) = ((rxt(k,2364) +rxt(k,2365) +rxt(k,2366) +rxt(k,2367))* y(k,705))* y(k,326)
         prod(k,626) = 0.
         loss(k,627) = ((rxt(k,2400) +rxt(k,2401) +rxt(k,2402) +rxt(k,2403))* y(k,705))* y(k,327)
         prod(k,627) = 0.
         loss(k,629) = ((rxt(k,2432) +rxt(k,2433) +rxt(k,2434) +rxt(k,2435))* y(k,705))* y(k,328)
         prod(k,629) = 0.
         loss(k,634) = ((rxt(k,2464) +rxt(k,2465) +rxt(k,2466) +rxt(k,2467))* y(k,705))* y(k,329)
         prod(k,634) = 0.
         loss(k,635) = ((rxt(k,2496) +rxt(k,2497) +rxt(k,2498) +rxt(k,2499))* y(k,705))* y(k,330)
         prod(k,635) = 0.
         loss(k,579) = ((rxt(k,3770) +rxt(k,3771) +rxt(k,3772) +rxt(k,3773))* y(k,705))* y(k,331)
         prod(k,579) = 0.
         loss(k,429) = ((rxt(k,3080) +rxt(k,3081) +rxt(k,3082))* y(k,705) + rxt(k,33) + rxt(k,34))* y(k,332)
         prod(k,429) = 0.
         loss(k,432) = ((rxt(k,3386) +rxt(k,3387) +rxt(k,3388) +rxt(k,3389))* y(k,705))* y(k,333)
         prod(k,432) = 0.
         loss(k,525) = ((rxt(k,3531) +rxt(k,3532) +rxt(k,3533) +rxt(k,3534))* y(k,705))* y(k,334)
         prod(k,525) = 0.
         loss(k,435) = ((rxt(k,3632) +rxt(k,3633) +rxt(k,3634) +rxt(k,3635))* y(k,705))* y(k,335)
         prod(k,435) = 0.
         loss(k,193) = ((rxt(k,948) +rxt(k,949) +rxt(k,950))* y(k,705))* y(k,336)
         prod(k,193) = 0.
         loss(k,194) = ((rxt(k,977) +rxt(k,978) +rxt(k,979))* y(k,705))* y(k,337)
         prod(k,194) = 0.
         loss(k,195) = ((rxt(k,1006) +rxt(k,1007) +rxt(k,1008))* y(k,705))* y(k,338)
         prod(k,195) = 0.
         loss(k,196) = ((rxt(k,1031) +rxt(k,1032) +rxt(k,1033))* y(k,705))* y(k,339)
         prod(k,196) = 0.
         loss(k,163) = ((rxt(k,1057) +rxt(k,1058))* y(k,705))* y(k,340)
         prod(k,163) = 0.
         loss(k,911) = (rxt(k,302)* y(k,705) + rxt(k,35))* y(k,341)
         prod(k,911) = (.999*rxt(k,1097)*y(k,585) +1.200*rxt(k,1169)*y(k,180) +2.000*rxt(k,1316)*y(k,122) + &
1.200*rxt(k,1325)*y(k,181) +1.500*rxt(k,1595)*y(k,141) +1.500*rxt(k,1610)*y(k,182) +1.500*rxt(k,1693)*y(k,123) + &
1.500*rxt(k,1843)*y(k,183) +1.200*rxt(k,1909)*y(k,128) +1.500*rxt(k,1921)*y(k,124) +1.650*rxt(k,2076)*y(k,881) + &
.114*rxt(k,2731)*y(k,595) +1.500*rxt(k,3454)*y(k,436) +1.460*rxt(k,3475)*y(k,587) +.900*rxt(k,3546)*y(k,529)) &
*y(k,696) + (rxt(k,4217)*y(k,690) +rxt(k,4219)*y(k,693) +rxt(k,4220)*y(k,620) +rxt(k,4221)*y(k,758) + &
rxt(k,4222)*y(k,370) +rxt(k,4223)*y(k,589) +.500*rxt(k,4224)*y(k,633) +.500*rxt(k,4225)*y(k,768) + &
.500*rxt(k,4226)*y(k,769))*y(k,842) + (2.859*rxt(k,304)*y(k,573) +.468*rxt(k,2966)*y(k,474) + &
.044*rxt(k,2977)*y(k,179))*y(k,705) +rxt(k,477)*y(k,840)
         loss(k,245) = ((rxt(k,2983) +rxt(k,2984) +rxt(k,2985))* y(k,705))* y(k,342)
         prod(k,245) = 0.
         loss(k,246) = (rxt(k,598)* y(k,696) + (rxt(k,240) +rxt(k,241))* y(k,705))* y(k,343)
         prod(k,246) = 0.
         loss(k,855) = ((rxt(k,2514) +rxt(k,2515) +rxt(k,2516))* y(k,693) + (rxt(k,2512) +rxt(k,2513))* y(k,696) + (rxt(k,2509) + &
rxt(k,2510) +rxt(k,2511))* y(k,705) +rxt(k,2517)* y(k,839) + rxt(k,36) + rxt(k,37) + rxt(k,38))* y(k,344)
         prod(k,855) = 0.
         loss(k,123) = ((rxt(k,3887) +rxt(k,3888))* y(k,705))* y(k,345)
         prod(k,123) = 0.
         loss(k,781) = ((rxt(k,2535) +rxt(k,2536))* y(k,693) + (rxt(k,2533) +rxt(k,2534))* y(k,696) + (rxt(k,2530) +rxt(k,2531) + &
rxt(k,2532))* y(k,705) +rxt(k,2537)* y(k,839))* y(k,346)
         prod(k,781) = 0.
         loss(k,247) = ((rxt(k,3049) +rxt(k,3050) +rxt(k,3051))* y(k,705))* y(k,347)
         prod(k,247) = 0.
         loss(k,922) = ((rxt(k,522) +rxt(k,523) +rxt(k,524))* y(k,696) + (rxt(k,518) +rxt(k,519) +rxt(k,520) +rxt(k,521)) &
* y(k,705) + rxt(k,39) + rxt(k,40))* y(k,348)
         prod(k,922) = (rxt(k,4227)*y(k,690) +rxt(k,4229)*y(k,693) +rxt(k,4230)*y(k,620) +rxt(k,4231)*y(k,758) + &
rxt(k,4232)*y(k,370) +rxt(k,4233)*y(k,589) +.500*rxt(k,4234)*y(k,633) +.500*rxt(k,4235)*y(k,768) + &
.500*rxt(k,4236)*y(k,769))*y(k,843) + (.250*rxt(k,2563)*y(k,527) +.250*rxt(k,2712)*y(k,201) + &
.150*rxt(k,2713)*y(k,288) +.250*rxt(k,2884)*y(k,150))*y(k,705) +2.100*rxt(k,538)*y(k,696)*y(k,350) +rxt(k,148) &
*y(k,756)
         loss(k,923) = ((rxt(k,529) +rxt(k,530) +rxt(k,531))* y(k,696) + (rxt(k,525) +rxt(k,526) +rxt(k,527) +rxt(k,528)) &
* y(k,705) + rxt(k,41))* y(k,349)
         prod(k,923) = (rxt(k,4237)*y(k,690) +rxt(k,4239)*y(k,693) +rxt(k,4240)*y(k,620) +rxt(k,4241)*y(k,758) + &
rxt(k,4242)*y(k,370) +rxt(k,4243)*y(k,589) +.500*rxt(k,4244)*y(k,633) +.500*rxt(k,4245)*y(k,768) + &
.500*rxt(k,4246)*y(k,769))*y(k,844) + (.750*rxt(k,2563)*y(k,527) +.750*rxt(k,2712)*y(k,201) + &
.850*rxt(k,2713)*y(k,288) +.750*rxt(k,2884)*y(k,150))*y(k,705) +2.100*rxt(k,538)*y(k,696)*y(k,350) +rxt(k,148) &
*y(k,756)
         loss(k,930) = ((rxt(k,536) +rxt(k,537) +rxt(k,538) +rxt(k,539) +rxt(k,540) +rxt(k,541))* y(k,696) + (rxt(k,532) + &
rxt(k,533) +rxt(k,534) +rxt(k,535))* y(k,705))* y(k,350)
         prod(k,930) = (.464*rxt(k,600)*y(k,359) +1.248*rxt(k,2176)*y(k,833) +.916*rxt(k,2179)*y(k,383) + &
.956*rxt(k,2184)*y(k,665) +.990*rxt(k,2188)*y(k,706) +1.390*rxt(k,2193)*y(k,746) +.752*rxt(k,2205)*y(k,676) + &
.968*rxt(k,2209)*y(k,572) +1.125*rxt(k,2214)*y(k,644) +.875*rxt(k,2219)*y(k,704) +1.295*rxt(k,2224)*y(k,721) + &
.410*rxt(k,2229)*y(k,14) +1.150*rxt(k,2234)*y(k,16) +.644*rxt(k,2238)*y(k,32) +.692*rxt(k,2250)*y(k,373) + &
1.164*rxt(k,2254)*y(k,807) +1.085*rxt(k,2259)*y(k,596) +.825*rxt(k,2264)*y(k,697) +1.215*rxt(k,2269)*y(k,711) + &
1.325*rxt(k,2274)*y(k,719) +.395*rxt(k,2279)*y(k,240) +1.095*rxt(k,2284)*y(k,302) +.632*rxt(k,2288)*y(k,324) + &
.408*rxt(k,2296)*y(k,667) +.604*rxt(k,2300)*y(k,376) +1.010*rxt(k,2305)*y(k,597) +.740*rxt(k,2310)*y(k,698) + &
1.090*rxt(k,2315)*y(k,712) +.375*rxt(k,2320)*y(k,241) +1.045*rxt(k,2325)*y(k,303) +.616*rxt(k,2329)*y(k,325) + &
.300*rxt(k,2334)*y(k,630) +.532*rxt(k,2337)*y(k,377) +.980*rxt(k,2342)*y(k,598) +.710*rxt(k,2347)*y(k,699) + &
1.050*rxt(k,2352)*y(k,713) +.365*rxt(k,2357)*y(k,242) +1.020*rxt(k,2362)*y(k,304) +.608*rxt(k,2366)*y(k,326) + &
.192*rxt(k,2371)*y(k,119) +.480*rxt(k,2374)*y(k,378) +.748*rxt(k,2378)*y(k,599) +.660*rxt(k,2383)*y(k,700) + &
.970*rxt(k,2387)*y(k,714) +.350*rxt(k,2393)*y(k,243) +.980*rxt(k,2398)*y(k,305) +.592*rxt(k,2402)*y(k,327) + &
.432*rxt(k,2406)*y(k,380) +.712*rxt(k,2410)*y(k,600) +.615*rxt(k,2415)*y(k,701) +.910*rxt(k,2419)*y(k,715) + &
.340*rxt(k,2425)*y(k,244) +.950*rxt(k,2430)*y(k,306) +.580*rxt(k,2434)*y(k,328) +.396*rxt(k,2438)*y(k,381) + &
.680*rxt(k,2442)*y(k,601) +.575*rxt(k,2447)*y(k,702) +.855*rxt(k,2451)*y(k,716) +.330*rxt(k,2457)*y(k,245) + &
.925*rxt(k,2462)*y(k,307) +.572*rxt(k,2466)*y(k,329) +.364*rxt(k,2470)*y(k,382) +.652*rxt(k,2474)*y(k,602) + &
.545*rxt(k,2479)*y(k,703) +.805*rxt(k,2483)*y(k,717) +.320*rxt(k,2489)*y(k,246) +.900*rxt(k,2494)*y(k,308) + &
.560*rxt(k,2498)*y(k,330) +.896*rxt(k,3057)*y(k,369) +1.256*rxt(k,3061)*y(k,372) +.910*rxt(k,3224)*y(k,496) + &
.840*rxt(k,3307)*y(k,744) +.840*rxt(k,3500)*y(k,23) +.740*rxt(k,3679)*y(k,466) +.335*rxt(k,3733)*y(k,169) + &
.490*rxt(k,3777)*y(k,463) +.464*rxt(k,4085)*y(k,443) +.464*rxt(k,4089)*y(k,692) +.464*rxt(k,4093)*y(k,438) + &
.464*rxt(k,4109)*y(k,371) +.812*rxt(k,4120)*y(k,656) +1.344*rxt(k,4123)*y(k,426) +1.344*rxt(k,4127)*y(k,718)) &
*y(k,705) + (rxt(k,4247)*y(k,690) +rxt(k,4249)*y(k,693) +rxt(k,4250)*y(k,620) +rxt(k,4251)*y(k,758) + &
rxt(k,4252)*y(k,370) +rxt(k,4253)*y(k,589) +.500*rxt(k,4254)*y(k,633) +.500*rxt(k,4255)*y(k,768) + &
.500*rxt(k,4256)*y(k,769))*y(k,845) +.834*rxt(k,539)*y(k,696)*y(k,350)
         loss(k,124) = ((rxt(k,1080) +rxt(k,1081))* y(k,705))* y(k,351)
         prod(k,124) = 0.
         loss(k,299) = ((rxt(k,3125) +rxt(k,3126) +rxt(k,3127))* y(k,705))* y(k,352)
         prod(k,299) = 0.
         loss(k,748) = (rxt(k,4027)* y(k,546) +rxt(k,4026)* y(k,693) + (rxt(k,4023) +rxt(k,4024) +rxt(k,4025))* y(k,705)) &
* y(k,353)
         prod(k,748) = 0.
         loss(k,347) = ((rxt(k,2720) +rxt(k,2721) +rxt(k,2722) +rxt(k,2723))* y(k,705))* y(k,354)
         prod(k,347) = 0.
         loss(k,822) = ((rxt(k,2025) +rxt(k,2026) +rxt(k,2027) +rxt(k,2028))* y(k,693) + (rxt(k,2019) +rxt(k,2020) +rxt(k,2021) + &
rxt(k,2022) +rxt(k,2023) +rxt(k,2024))* y(k,696) + (rxt(k,2015) +rxt(k,2016) +rxt(k,2017) +rxt(k,2018))* y(k,705) &
 +rxt(k,2029)* y(k,839))* y(k,355)
         prod(k,822) = 0.
         loss(k,807) = ((rxt(k,3442) +rxt(k,3443) +rxt(k,3444))* y(k,693) + (rxt(k,3437) +rxt(k,3438) +rxt(k,3439) +rxt(k,3440) + &
rxt(k,3441))* y(k,696) + (rxt(k,3433) +rxt(k,3434) +rxt(k,3435) +rxt(k,3436))* y(k,705) +rxt(k,3445)* y(k,839)) &
* y(k,356)
         prod(k,807) = 0.
         loss(k,696) = ( + rxt(k,42))* y(k,357)
         prod(k,696) = (rxt(k,4257)*y(k,690) +rxt(k,4259)*y(k,693) +rxt(k,4260)*y(k,620) +rxt(k,4261)*y(k,758) + &
rxt(k,4262)*y(k,370) +rxt(k,4263)*y(k,589) +.500*rxt(k,4264)*y(k,633) +.500*rxt(k,4265)*y(k,768) + &
.500*rxt(k,4266)*y(k,769))*y(k,846) + (1.332*rxt(k,2611)*y(k,591) +1.332*rxt(k,2802)*y(k,628) + &
1.332*rxt(k,2932)*y(k,514) +1.332*rxt(k,3233)*y(k,363) +1.332*rxt(k,3246)*y(k,569))*y(k,696) &
 + (3.340*rxt(k,2703)*y(k,626) +3.500*rxt(k,2710)*y(k,592) +2.580*rxt(k,2871)*y(k,513) + &
.004*rxt(k,3694)*y(k,812) +.670*rxt(k,3981)*y(k,163))*y(k,705)
         loss(k,762) = (rxt(k,517)* y(k,693) +rxt(k,516)* y(k,705) + rxt(k,43))* y(k,358)
         prod(k,762) = (rxt(k,4267)*y(k,690) +rxt(k,4269)*y(k,693) +rxt(k,4270)*y(k,620) +rxt(k,4271)*y(k,758) + &
rxt(k,4272)*y(k,370) +rxt(k,4273)*y(k,589) +.500*rxt(k,4274)*y(k,633) +.500*rxt(k,4275)*y(k,768) + &
.500*rxt(k,4276)*y(k,769))*y(k,847) + (1.200*rxt(k,2197)*y(k,776) +1.200*rxt(k,2242)*y(k,360))*y(k,696)
         loss(k,348) = ((rxt(k,599) +rxt(k,600) +rxt(k,601) +rxt(k,602))* y(k,705))* y(k,359)
         prod(k,348) = 0.
         loss(k,773) = ((rxt(k,2244) +rxt(k,2245) +rxt(k,2246))* y(k,693) + (rxt(k,2242) +rxt(k,2243))* y(k,696) + (rxt(k,2240) + &
rxt(k,2241))* y(k,705) +rxt(k,2247)* y(k,839))* y(k,360)
         prod(k,773) = 0.
         loss(k,349) = ((rxt(k,2949) +rxt(k,2950) +rxt(k,2951) +rxt(k,2952))* y(k,705))* y(k,361)
         prod(k,349) = 0.
         loss(k,568) = ((rxt(k,3273) +rxt(k,3274) +rxt(k,3275) +rxt(k,3276))* y(k,705))* y(k,362)
         prod(k,568) = 0.
         loss(k,878) = ((rxt(k,3234) +rxt(k,3235) +rxt(k,3236) +rxt(k,3237))* y(k,693) + (rxt(k,3230) +rxt(k,3231) +rxt(k,3232) + &
rxt(k,3233))* y(k,696) + (rxt(k,3226) +rxt(k,3227) +rxt(k,3228) +rxt(k,3229))* y(k,705) +rxt(k,3238)* y(k,839)) &
* y(k,363)
         prod(k,878) = 0.
         loss(k,248) = ((rxt(k,3218) +rxt(k,3219) +rxt(k,3220))* y(k,705))* y(k,364)
         prod(k,248) = 0.
         loss(k,350) = ((rxt(k,3151) +rxt(k,3152) +rxt(k,3153))* y(k,705))* y(k,365)
         prod(k,350) = 0.
         loss(k,534) = ((rxt(k,2989) +rxt(k,2990) +rxt(k,2991) +rxt(k,2992))* y(k,705))* y(k,366)
         prod(k,534) = 0.
         loss(k,537) = ((rxt(k,3121) +rxt(k,3122) +rxt(k,3123) +rxt(k,3124))* y(k,705))* y(k,367)
         prod(k,537) = 0.
         loss(k,301) = ((rxt(k,2666) +rxt(k,2667) +rxt(k,2668))* y(k,705))* y(k,368)
         prod(k,301) = 0.
         loss(k,547) = ((rxt(k,3055) +rxt(k,3056) +rxt(k,3057) +rxt(k,3058))* y(k,705))* y(k,369)
         prod(k,547) = 0.
         loss(k,940) = (2.*rxt(k,457)* y(k,370) + (rxt(k,3960) +rxt(k,3961))* y(k,427) + (rxt(k,446) +rxt(k,447))* y(k,548) &
 + (rxt(k,473) +rxt(k,474))* y(k,589) +rxt(k,454)* y(k,620) + (rxt(k,450) +rxt(k,451))* y(k,633) +rxt(k,445) &
* y(k,690) +rxt(k,443)* y(k,691) + (rxt(k,448) +rxt(k,449))* y(k,693) + (rxt(k,455) +rxt(k,456))* y(k,758) &
 +rxt(k,452)* y(k,768) +rxt(k,453)* y(k,769) + het_rates(k,36))* y(k,370)
         prod(k,940) = (rxt(k,516)*y(k,705) +rxt(k,517)*y(k,693))*y(k,358) + (1.200*rxt(k,125) +rxt(k,444))*y(k,709)
         loss(k,351) = ((rxt(k,4107) +rxt(k,4108) +rxt(k,4109) +rxt(k,4110))* y(k,705))* y(k,371)
         prod(k,351) = 0.
         loss(k,616) = ((rxt(k,3059) +rxt(k,3060) +rxt(k,3061) +rxt(k,3062))* y(k,705))* y(k,372)
         prod(k,616) = 0.
         loss(k,633) = ((rxt(k,2248) +rxt(k,2249) +rxt(k,2250) +rxt(k,2251))* y(k,705))* y(k,373)
         prod(k,633) = 0.
         loss(k,164) = ((rxt(k,1065) +rxt(k,1066))* y(k,705))* y(k,374)
         prod(k,164) = 0.
         loss(k,197) = ((rxt(k,3481) +rxt(k,3482) +rxt(k,3483))* y(k,705))* y(k,375)
         prod(k,197) = 0.
         loss(k,617) = ((rxt(k,2298) +rxt(k,2299) +rxt(k,2300) +rxt(k,2301))* y(k,705))* y(k,376)
         prod(k,617) = 0.
         loss(k,618) = ((rxt(k,2335) +rxt(k,2336) +rxt(k,2337) +rxt(k,2338))* y(k,705))* y(k,377)
         prod(k,618) = 0.
         loss(k,621) = ((rxt(k,2372) +rxt(k,2373) +rxt(k,2374) +rxt(k,2375))* y(k,705))* y(k,378)
         prod(k,621) = 0.
         loss(k,777) = ((rxt(k,3938) +rxt(k,3939) +rxt(k,3940))* y(k,437) + (rxt(k,3935) +rxt(k,3936))* y(k,693) + (rxt(k,3932) + &
rxt(k,3933) +rxt(k,3934))* y(k,696) + (rxt(k,3930) +rxt(k,3931))* y(k,705) +rxt(k,3937)* y(k,839))* y(k,379)
         prod(k,777) = 0.
         loss(k,631) = ((rxt(k,2404) +rxt(k,2405) +rxt(k,2406) +rxt(k,2407))* y(k,705))* y(k,380)
         prod(k,631) = 0.
         loss(k,636) = ((rxt(k,2436) +rxt(k,2437) +rxt(k,2438) +rxt(k,2439))* y(k,705))* y(k,381)
         prod(k,636) = 0.
         loss(k,639) = ((rxt(k,2468) +rxt(k,2469) +rxt(k,2470) +rxt(k,2471))* y(k,705))* y(k,382)
         prod(k,639) = 0.
         loss(k,641) = ((rxt(k,2177) +rxt(k,2178) +rxt(k,2179) +rxt(k,2180))* y(k,705))* y(k,383)
         prod(k,641) = 0.
         loss(k,125) = ((rxt(k,3876) +rxt(k,3877))* y(k,705))* y(k,384)
         prod(k,125) = 0.
         loss(k,774) = ((rxt(k,1106) +rxt(k,1107) +rxt(k,1108))* y(k,693) + (rxt(k,1104) +rxt(k,1105))* y(k,696) + (rxt(k,1102) + &
rxt(k,1103))* y(k,705) +rxt(k,1109)* y(k,839))* y(k,385)
         prod(k,774) = 0.
         loss(k,888) = ((rxt(k,1339) +rxt(k,1340) +rxt(k,1341))* y(k,693) + (rxt(k,1334) +rxt(k,1335) +rxt(k,1336) +rxt(k,1337) + &
rxt(k,1338))* y(k,696) + (rxt(k,1331) +rxt(k,1332) +rxt(k,1333))* y(k,705) +rxt(k,1342)* y(k,839))* y(k,386)
         prod(k,888) = 0.
         loss(k,879) = ((rxt(k,1649) +rxt(k,1650) +rxt(k,1651))* y(k,693) + (rxt(k,1644) +rxt(k,1645) +rxt(k,1646) +rxt(k,1647) + &
rxt(k,1648))* y(k,696) + (rxt(k,1641) +rxt(k,1642) +rxt(k,1643))* y(k,705) +rxt(k,1652)* y(k,839))* y(k,387)
         prod(k,879) = 0.
         loss(k,126) = ((rxt(k,331) +rxt(k,332))* y(k,705))* y(k,388)
         prod(k,126) = 0.
         loss(k,834) = ((rxt(k,1182) +rxt(k,1183) +rxt(k,1184))* y(k,693) + (rxt(k,1178) +rxt(k,1179) +rxt(k,1180) +rxt(k,1181)) &
* y(k,696) + (rxt(k,1175) +rxt(k,1176) +rxt(k,1177))* y(k,705) +rxt(k,1185)* y(k,839))* y(k,389)
         prod(k,834) = 0.
         loss(k,165) = ((rxt(k,3970) +rxt(k,3971) +rxt(k,3972))* y(k,705))* y(k,390)
         prod(k,165) = 0.
         loss(k,800) = ((rxt(k,1349) +rxt(k,1350) +rxt(k,1351))* y(k,693) + (rxt(k,1346) +rxt(k,1347) +rxt(k,1348))* y(k,696) &
 + (rxt(k,1343) +rxt(k,1344) +rxt(k,1345))* y(k,705) +rxt(k,1352)* y(k,839))* y(k,391)
         prod(k,800) = 0.
         loss(k,830) = ((rxt(k,1705) +rxt(k,1706) +rxt(k,1707))* y(k,693) + (rxt(k,1701) +rxt(k,1702) +rxt(k,1703) +rxt(k,1704)) &
* y(k,696) + (rxt(k,1698) +rxt(k,1699) +rxt(k,1700))* y(k,705) +rxt(k,1708)* y(k,839))* y(k,392)
         prod(k,830) = 0.
         loss(k,198) = ((rxt(k,827) +rxt(k,828) +rxt(k,829))* y(k,705))* y(k,393)
         prod(k,198) = 0.
         loss(k,302) = ((rxt(k,2656) +rxt(k,2657) +rxt(k,2658))* y(k,705))* y(k,394)
         prod(k,302) = 0.
         loss(k,905) = ((rxt(k,1361) +rxt(k,1362) +rxt(k,1363))* y(k,693) + (rxt(k,1356) +rxt(k,1357) +rxt(k,1358) +rxt(k,1359) + &
rxt(k,1360))* y(k,696) + (rxt(k,1353) +rxt(k,1354) +rxt(k,1355))* y(k,705) +rxt(k,1364)* y(k,839))* y(k,395)
         prod(k,905) = 0.
         loss(k,908) = ((rxt(k,1752) +rxt(k,1753) +rxt(k,1754))* y(k,693) + (rxt(k,1747) +rxt(k,1748) +rxt(k,1749) +rxt(k,1750) + &
rxt(k,1751))* y(k,696) + (rxt(k,1744) +rxt(k,1745) +rxt(k,1746))* y(k,705) +rxt(k,1755)* y(k,839))* y(k,396)
         prod(k,908) = 0.
         loss(k,153) = ((rxt(k,3915) +rxt(k,3916))* y(k,705))* y(k,397)
         prod(k,153) = 0.
         loss(k,868) = ((rxt(k,1585) +rxt(k,1586) +rxt(k,1587) +rxt(k,1588))* y(k,693) + (rxt(k,1581) +rxt(k,1582) +rxt(k,1583) + &
rxt(k,1584))* y(k,696) + (rxt(k,1578) +rxt(k,1579) +rxt(k,1580))* y(k,705) +rxt(k,1589)* y(k,839))* y(k,398)
         prod(k,868) = 0.
         loss(k,252) = ((rxt(k,4053) +rxt(k,4054) +rxt(k,4055))* y(k,705))* y(k,399)
         prod(k,252) = 0.
         loss(k,806) = ((rxt(k,1857) +rxt(k,1858))* y(k,693) + (rxt(k,1853) +rxt(k,1854) +rxt(k,1855) +rxt(k,1856))* y(k,696) &
 + (rxt(k,1850) +rxt(k,1851) +rxt(k,1852))* y(k,705) +rxt(k,1859)* y(k,839))* y(k,400)
         prod(k,806) = 0.
         loss(k,253) = ((rxt(k,4028) +rxt(k,4029) +rxt(k,4030))* y(k,705))* y(k,401)
         prod(k,253) = 0.
         loss(k,199) = ((rxt(k,904) +rxt(k,905) +rxt(k,906))* y(k,705))* y(k,402)
         prod(k,199) = 0.
         loss(k,357) = ((rxt(k,2834) +rxt(k,2835) +rxt(k,2836) +rxt(k,2837))* y(k,705))* y(k,403)
         prod(k,357) = 0.
         loss(k,799) = ((rxt(k,1990) +rxt(k,1991))* y(k,693) + (rxt(k,1986) +rxt(k,1987) +rxt(k,1988) +rxt(k,1989))* y(k,696) &
 + (rxt(k,1983) +rxt(k,1984) +rxt(k,1985))* y(k,705) +rxt(k,1992)* y(k,839))* y(k,404)
         prod(k,799) = 0.
         loss(k,200) = ((rxt(k,931) +rxt(k,932) +rxt(k,933))* y(k,705))* y(k,405)
         prod(k,200) = 0.
         loss(k,201) = ((rxt(k,3494) +rxt(k,3495) +rxt(k,3496))* y(k,705))* y(k,406)
         prod(k,201) = 0.
         loss(k,358) = ((rxt(k,2770) +rxt(k,2771) +rxt(k,2772))* y(k,705))* y(k,407)
         prod(k,358) = 0.
         loss(k,359) = ((rxt(k,3284) +rxt(k,3285) +rxt(k,3286))* y(k,705))* y(k,408)
         prod(k,359) = 0.
         loss(k,166) = ((rxt(k,958) +rxt(k,959))* y(k,705))* y(k,409)
         prod(k,166) = 0.
         loss(k,361) = ((rxt(k,2972) +rxt(k,2973) +rxt(k,2974))* y(k,705))* y(k,410)
         prod(k,361) = 0.
         loss(k,167) = ((rxt(k,987) +rxt(k,988))* y(k,705))* y(k,411)
         prod(k,167) = 0.
         loss(k,501) = ((rxt(k,3071) +rxt(k,3072) +rxt(k,3073))* y(k,705) + rxt(k,44) + rxt(k,45))* y(k,412)
         prod(k,501) = 0.
         loss(k,168) = ((rxt(k,1015) +rxt(k,1016))* y(k,705))* y(k,413)
         prod(k,168) = 0.
         loss(k,362) = ((rxt(k,3193) +rxt(k,3194) +rxt(k,3195))* y(k,705) + rxt(k,46) + rxt(k,47))* y(k,414)
         prod(k,362) = 0.
         loss(k,169) = ((rxt(k,1040) +rxt(k,1041))* y(k,705))* y(k,415)
         prod(k,169) = 0.
         loss(k,202) = ((rxt(k,3353) +rxt(k,3354) +rxt(k,3355))* y(k,705))* y(k,416)
         prod(k,202) = 0.
         loss(k,303) = ((rxt(k,3430) +rxt(k,3431) +rxt(k,3432))* y(k,705))* y(k,417)
         prod(k,303) = 0.
         loss(k,154) = ((rxt(k,2576) +rxt(k,2577))* y(k,705))* y(k,418)
         prod(k,154) = 0.
         loss(k,254) = ((rxt(k,2714) +rxt(k,2715) +rxt(k,2716))* y(k,705))* y(k,419)
         prod(k,254) = 0.
         loss(k,255) = ((rxt(k,2717) +rxt(k,2718) +rxt(k,2719))* y(k,705))* y(k,420)
         prod(k,255) = 0.
         loss(k,256) = ((rxt(k,2885) +rxt(k,2886) +rxt(k,2887))* y(k,705))* y(k,421)
         prod(k,256) = 0.
         loss(k,306) = ((rxt(k,2888) +rxt(k,2889) +rxt(k,2890))* y(k,705))* y(k,422)
         prod(k,306) = 0.
         loss(k,916) = (rxt(k,484)* y(k,693) +rxt(k,483)* y(k,705) + rxt(k,48) + rxt(k,49) + het_rates(k,30))* y(k,423)
         prod(k,916) = (1.500*rxt(k,1075)*y(k,739) +2.000*rxt(k,1105)*y(k,385) +2.000*rxt(k,1113)*y(k,784) + &
2.800*rxt(k,1169)*y(k,180) +2.000*rxt(k,1179)*y(k,389) +2.000*rxt(k,1190)*y(k,788) +1.124*rxt(k,1211)*y(k,780) + &
2.500*rxt(k,1336)*y(k,386) +3.500*rxt(k,1358)*y(k,395) +3.500*rxt(k,1370)*y(k,792) +2.500*rxt(k,1382)*y(k,798) + &
2.500*rxt(k,1394)*y(k,785) +1.250*rxt(k,1452)*y(k,781) +2.000*rxt(k,1583)*y(k,398) +2.800*rxt(k,1622)*y(k,257) + &
2.500*rxt(k,1646)*y(k,387) +3.500*rxt(k,1682)*y(k,216) +2.000*rxt(k,1714)*y(k,794) +2.500*rxt(k,1726)*y(k,786) + &
3.500*rxt(k,1749)*y(k,396) +2.500*rxt(k,1761)*y(k,799) +3.500*rxt(k,1774)*y(k,793) +2.500*rxt(k,1829)*y(k,787) + &
2.800*rxt(k,1976)*y(k,219) +.800*rxt(k,2242)*y(k,360) +1.500*rxt(k,2568)*y(k,447) +2.100*rxt(k,3546)*y(k,529)) &
*y(k,696) + (1.900*rxt(k,299)*y(k,519) +.005*rxt(k,574)*y(k,731) +.966*rxt(k,2506)*y(k,510) + &
.081*rxt(k,2550)*y(k,729) +.034*rxt(k,2561)*y(k,535) +.021*rxt(k,2601)*y(k,773) +.020*rxt(k,2673)*y(k,35) + &
.123*rxt(k,2683)*y(k,112) +.020*rxt(k,2703)*y(k,626) +.004*rxt(k,2764)*y(k,164) +.005*rxt(k,2870)*y(k,513) + &
.004*rxt(k,2976)*y(k,179) +.020*rxt(k,4185)*y(k,734) +.004*rxt(k,4198)*y(k,763))*y(k,705) &
 + (rxt(k,4277)*y(k,690) +rxt(k,4279)*y(k,693) +rxt(k,4280)*y(k,620) +rxt(k,4281)*y(k,758) + &
rxt(k,4282)*y(k,370) +rxt(k,4283)*y(k,589) +.500*rxt(k,4284)*y(k,633) +.500*rxt(k,4285)*y(k,768) + &
.500*rxt(k,4286)*y(k,769))*y(k,848) +rxt(k,57)*y(k,447) +.300*rxt(k,1070)*y(k,839)*y(k,512) +.934*rxt(k,83) &
*y(k,582) +1.070*rxt(k,154)*y(k,761) +.788*rxt(k,158)*y(k,762) +3.560*rxt(k,161)*y(k,764)
         loss(k,1) = ( + rxt(k,50))* y(k,424)
         prod(k,1) = 0.
         loss(k,889) = ((rxt(k,490) +rxt(k,491))* y(k,705))* y(k,425)
         prod(k,889) = (1.336*rxt(k,2522)*y(k,741) +1.280*rxt(k,2545)*y(k,605) +.030*rxt(k,2628)*y(k,837) + &
2.397*rxt(k,2650)*y(k,500) +.340*rxt(k,2824)*y(k,579) +1.988*rxt(k,2840)*y(k,726) +2.268*rxt(k,2866)*y(k,603) + &
1.396*rxt(k,2944)*y(k,566) +.800*rxt(k,2951)*y(k,361) +.700*rxt(k,2959)*y(k,772) +2.172*rxt(k,3021)*y(k,724) + &
1.276*rxt(k,3025)*y(k,449) +2.004*rxt(k,3029)*y(k,204) +.392*rxt(k,3033)*y(k,640) +2.460*rxt(k,3047)*y(k,509) + &
.039*rxt(k,3126)*y(k,352) +.404*rxt(k,3134)*y(k,562) +.484*rxt(k,3138)*y(k,175) +2.524*rxt(k,3176)*y(k,27) + &
.092*rxt(k,3254)*y(k,125) +.092*rxt(k,3258)*y(k,187) +.040*rxt(k,3262)*y(k,271) +.812*rxt(k,3266)*y(k,313) + &
.024*rxt(k,3278)*y(k,682) +.784*rxt(k,3282)*y(k,593) +1.060*rxt(k,3310)*y(k,161) +.608*rxt(k,3331)*y(k,471) + &
.100*rxt(k,3368)*y(k,142) +.032*rxt(k,3372)*y(k,188) +.016*rxt(k,3376)*y(k,254) +.024*rxt(k,3380)*y(k,272) + &
.740*rxt(k,3384)*y(k,314) +.044*rxt(k,3388)*y(k,333) +.018*rxt(k,3395)*y(k,684) +.712*rxt(k,3410)*y(k,486) + &
.672*rxt(k,3414)*y(k,487) +1.731*rxt(k,3428)*y(k,534) +.068*rxt(k,3503)*y(k,143) +.024*rxt(k,3507)*y(k,173) + &
.132*rxt(k,3511)*y(k,220) +.020*rxt(k,3515)*y(k,226) +.009*rxt(k,3519)*y(k,255) +.015*rxt(k,3522)*y(k,273) + &
.316*rxt(k,3525)*y(k,295) +.592*rxt(k,3529)*y(k,315) +.040*rxt(k,3533)*y(k,334) +.015*rxt(k,3536)*y(k,686) + &
.480*rxt(k,3570)*y(k,469) +.040*rxt(k,3592)*y(k,108) +.040*rxt(k,3596)*y(k,126) +.056*rxt(k,3600)*y(k,144) + &
.140*rxt(k,3604)*y(k,149) +.020*rxt(k,3608)*y(k,189) +.016*rxt(k,3612)*y(k,227) +.016*rxt(k,3616)*y(k,232) + &
.006*rxt(k,3619)*y(k,256) +.188*rxt(k,3623)*y(k,296) +.504*rxt(k,3627)*y(k,297) +.273*rxt(k,3630)*y(k,316) + &
.032*rxt(k,3634)*y(k,335) +.012*rxt(k,3637)*y(k,688) +.016*rxt(k,3651)*y(k,233) +.009*rxt(k,3655)*y(k,261) + &
.308*rxt(k,3658)*y(k,298) +.012*rxt(k,3698)*y(k,222) +.008*rxt(k,3702)*y(k,253) +.456*rxt(k,3706)*y(k,300) + &
.028*rxt(k,3717)*y(k,106) +.012*rxt(k,3721)*y(k,223) +.012*rxt(k,3725)*y(k,228) +.032*rxt(k,3737)*y(k,132) + &
.006*rxt(k,3741)*y(k,252) +.380*rxt(k,3744)*y(k,299) +.024*rxt(k,3764)*y(k,105) +.012*rxt(k,3768)*y(k,221) + &
.012*rxt(k,3772)*y(k,331) +.330*rxt(k,3981)*y(k,163))*y(k,705) + (.225*rxt(k,1076)*y(k,739) + &
.300*rxt(k,1105)*y(k,385) +.300*rxt(k,1113)*y(k,784) +.180*rxt(k,1169)*y(k,180) +.300*rxt(k,1180)*y(k,389) + &
.300*rxt(k,1191)*y(k,788) +.152*rxt(k,1212)*y(k,780) +.375*rxt(k,1337)*y(k,386) +.225*rxt(k,1359)*y(k,395) + &
.225*rxt(k,1371)*y(k,792) +.375*rxt(k,1383)*y(k,798) +.375*rxt(k,1395)*y(k,785) +.190*rxt(k,1453)*y(k,781) + &
.300*rxt(k,1584)*y(k,398) +.180*rxt(k,1623)*y(k,257) +.375*rxt(k,1647)*y(k,387) +.225*rxt(k,1683)*y(k,216) + &
.300*rxt(k,1715)*y(k,794) +.375*rxt(k,1727)*y(k,786) +.225*rxt(k,1750)*y(k,396) +.375*rxt(k,1762)*y(k,799) + &
.225*rxt(k,1775)*y(k,793) +.375*rxt(k,1830)*y(k,787) +.180*rxt(k,1977)*y(k,219) +1.200*rxt(k,2242)*y(k,360) + &
.225*rxt(k,2568)*y(k,447))*y(k,696) + (1.120*rxt(k,419)*y(k,620) +1.120*rxt(k,447)*y(k,370))*y(k,548) &
 +.150*rxt(k,37)*y(k,344) +.200*rxt(k,421)*y(k,633)*y(k,620) +2.691*rxt(k,2633)*y(k,837)*y(k,693)
         loss(k,520) = ((rxt(k,4121) +rxt(k,4122) +rxt(k,4123) +rxt(k,4124))* y(k,705))* y(k,426)
         prod(k,520) = 0.
         loss(k,919) = ((rxt(k,3960) +rxt(k,3961))* y(k,370) + 2.*rxt(k,3964)* y(k,427) +rxt(k,3950)* y(k,548) + (rxt(k,3962) + &
rxt(k,3963))* y(k,589) + (rxt(k,3956) +rxt(k,3957))* y(k,620) +rxt(k,3953)* y(k,633) + (rxt(k,3948) +rxt(k,3949)) &
* y(k,690) +rxt(k,339)* y(k,691) + (rxt(k,3951) +rxt(k,3952))* y(k,693) + (rxt(k,3958) +rxt(k,3959))* y(k,758) &
 +rxt(k,3954)* y(k,768) +rxt(k,3955)* y(k,769))* y(k,427)
         prod(k,919) = (1.200*rxt(k,53) +rxt(k,3947))*y(k,431) +rxt(k,3946)*y(k,819)*y(k,705)
         loss(k,73) = ( + rxt(k,51))* y(k,428)
         prod(k,73) =.750*rxt(k,3950)*y(k,548)*y(k,427)
         loss(k,74) = ( + rxt(k,3967))* y(k,429)
         prod(k,74) = (rxt(k,52) +rxt(k,3966)*y(k,705))*y(k,430)
         loss(k,75) = (rxt(k,3966)* y(k,705) + rxt(k,52))* y(k,430)
         prod(k,75) = 0.
         loss(k,170) = ( + rxt(k,53) + rxt(k,54) + rxt(k,3947))* y(k,431)
         prod(k,170) =rxt(k,339)*y(k,691)*y(k,427)
         loss(k,99) = (rxt(k,326)* y(k,705))* y(k,432)
         prod(k,99) = 0.
         loss(k,143) = (rxt(k,3821)* y(k,705) + rxt(k,55))* y(k,433)
         prod(k,143) = 0.
         loss(k,81) = (rxt(k,3819)* y(k,705))* y(k,434)
         prod(k,81) = 0.
         loss(k,82) = (rxt(k,329)* y(k,705))* y(k,435)
         prod(k,82) = 0.
         loss(k,894) = ((rxt(k,3456) +rxt(k,3457) +rxt(k,3458) +rxt(k,3459))* y(k,693) + (rxt(k,3451) +rxt(k,3452) +rxt(k,3453) + &
rxt(k,3454) +rxt(k,3455))* y(k,696) + (rxt(k,3446) +rxt(k,3447) +rxt(k,3448) +rxt(k,3449) +rxt(k,3450))* y(k,705) &
 +rxt(k,3460)* y(k,839))* y(k,436)
         prod(k,894) = 0.
         loss(k,778) = ((rxt(k,3938) +rxt(k,3939) +rxt(k,3940))* y(k,379) + (rxt(k,4050) +rxt(k,4051) +rxt(k,4052))* y(k,439) &
 +rxt(k,3883)* y(k,441) + (rxt(k,3927) +rxt(k,3928) +rxt(k,3929))* y(k,779))* y(k,437)
         prod(k,778) = 0.
         loss(k,366) = ((rxt(k,4091) +rxt(k,4092) +rxt(k,4093) +rxt(k,4094))* y(k,705))* y(k,438)
         prod(k,366) = 0.
         loss(k,780) = ((rxt(k,4050) +rxt(k,4051) +rxt(k,4052))* y(k,437) + (rxt(k,4047) +rxt(k,4048))* y(k,693) + (rxt(k,4044) + &
rxt(k,4045) +rxt(k,4046))* y(k,696) + (rxt(k,4041) +rxt(k,4042) +rxt(k,4043))* y(k,705) +rxt(k,4049)* y(k,839)) &
* y(k,439)
         prod(k,780) = 0.
         loss(k,83) = (rxt(k,327)* y(k,705))* y(k,440)
         prod(k,83) = 0.
         loss(k,732) = (rxt(k,3883)* y(k,437) + (rxt(k,3880) +rxt(k,3881))* y(k,693) + (rxt(k,3878) +rxt(k,3879))* y(k,705) &
 +rxt(k,3882)* y(k,839))* y(k,441)
         prod(k,732) = 0.
         loss(k,100) = ((rxt(k,3884) +rxt(k,3885))* y(k,705))* y(k,442)
         prod(k,100) = 0.
         loss(k,367) = ((rxt(k,4083) +rxt(k,4084) +rxt(k,4085) +rxt(k,4086))* y(k,705))* y(k,443)
         prod(k,367) = 0.
         loss(k,144) = ((rxt(k,3854) +rxt(k,3855))* y(k,705))* y(k,444)
         prod(k,144) = 0.
         loss(k,368) = (rxt(k,494)* y(k,705) + rxt(k,56))* y(k,445)
         prod(k,368) =rxt(k,229)*y(k,633)*y(k,548)
         loss(k,739) = (rxt(k,514)* y(k,693) + (rxt(k,512) +rxt(k,513))* y(k,705))* y(k,446)
         prod(k,739) = (2.280*rxt(k,600)*y(k,359) +.724*rxt(k,2175)*y(k,833) +.560*rxt(k,2179)*y(k,383) + &
.636*rxt(k,2183)*y(k,665) +.805*rxt(k,2187)*y(k,706) +.795*rxt(k,2192)*y(k,746) +.456*rxt(k,2205)*y(k,676) + &
.588*rxt(k,2209)*y(k,572) +.750*rxt(k,2213)*y(k,644) +.710*rxt(k,2218)*y(k,704) +.740*rxt(k,2223)*y(k,721) + &
.155*rxt(k,2228)*y(k,14) +.110*rxt(k,2233)*y(k,16) +.160*rxt(k,2237)*y(k,32) +.420*rxt(k,2250)*y(k,373) + &
.708*rxt(k,2254)*y(k,807) +.720*rxt(k,2258)*y(k,596) +.670*rxt(k,2263)*y(k,697) +.695*rxt(k,2268)*y(k,711) + &
.755*rxt(k,2273)*y(k,719) +.150*rxt(k,2278)*y(k,240) +.105*rxt(k,2283)*y(k,302) +.156*rxt(k,2287)*y(k,324) + &
rxt(k,2291)*y(k,816) +.944*rxt(k,2296)*y(k,667) +.368*rxt(k,2300)*y(k,376) +.670*rxt(k,2304)*y(k,597) + &
.600*rxt(k,2309)*y(k,698) +.625*rxt(k,2314)*y(k,712) +.140*rxt(k,2319)*y(k,241) +.100*rxt(k,2324)*y(k,303) + &
.152*rxt(k,2328)*y(k,325) +.944*rxt(k,2333)*y(k,630) +.324*rxt(k,2337)*y(k,377) +.650*rxt(k,2341)*y(k,598) + &
.575*rxt(k,2346)*y(k,699) +.600*rxt(k,2351)*y(k,713) +.140*rxt(k,2356)*y(k,242) +.095*rxt(k,2361)*y(k,304) + &
.152*rxt(k,2365)*y(k,326) +.944*rxt(k,2370)*y(k,119) +.292*rxt(k,2374)*y(k,378) +.496*rxt(k,2378)*y(k,599) + &
.535*rxt(k,2382)*y(k,700) +.555*rxt(k,2387)*y(k,714) +.135*rxt(k,2392)*y(k,243) +.090*rxt(k,2397)*y(k,305) + &
.148*rxt(k,2401)*y(k,327) +.264*rxt(k,2406)*y(k,380) +.472*rxt(k,2410)*y(k,600) +.500*rxt(k,2414)*y(k,701) + &
.520*rxt(k,2419)*y(k,715) +.130*rxt(k,2424)*y(k,244) +.090*rxt(k,2429)*y(k,306) +.144*rxt(k,2433)*y(k,328) + &
.240*rxt(k,2438)*y(k,381) +.452*rxt(k,2442)*y(k,601) +.470*rxt(k,2446)*y(k,702) +.490*rxt(k,2451)*y(k,716) + &
.125*rxt(k,2456)*y(k,245) +.085*rxt(k,2461)*y(k,307) +.140*rxt(k,2465)*y(k,329) +.224*rxt(k,2470)*y(k,382) + &
.432*rxt(k,2474)*y(k,602) +.440*rxt(k,2478)*y(k,703) +.460*rxt(k,2483)*y(k,717) +.120*rxt(k,2488)*y(k,246) + &
.085*rxt(k,2493)*y(k,308) +.140*rxt(k,2497)*y(k,330) +.544*rxt(k,3057)*y(k,369) +.728*rxt(k,3061)*y(k,372) + &
.525*rxt(k,3223)*y(k,496) +.680*rxt(k,3306)*y(k,744) +.680*rxt(k,3499)*y(k,23) +.600*rxt(k,3678)*y(k,466) + &
.195*rxt(k,3732)*y(k,169) +.395*rxt(k,3776)*y(k,463) +2.280*rxt(k,4084)*y(k,443) +2.280*rxt(k,4088)*y(k,692) + &
2.280*rxt(k,4092)*y(k,438) +2.280*rxt(k,4108)*y(k,371) +.468*rxt(k,4119)*y(k,656) +.780*rxt(k,4123)*y(k,426) + &
.780*rxt(k,4127)*y(k,718) +1.500*rxt(k,4129)*y(k,745) +rxt(k,4132)*y(k,809) +1.500*rxt(k,4175)*y(k,604))*y(k,705) &
 + (rxt(k,480) +rxt(k,479)*y(k,548))*y(k,841)
         loss(k,840) = ((rxt(k,2570) +rxt(k,2571) +rxt(k,2572) +rxt(k,2573))* y(k,693) + (rxt(k,2567) +rxt(k,2568) +rxt(k,2569)) &
* y(k,696) + (rxt(k,2564) +rxt(k,2565) +rxt(k,2566))* y(k,705) +rxt(k,2574)* y(k,839) + rxt(k,57) &
 + het_rates(k,29))* y(k,447)
         prod(k,840) = 0.
         loss(k,84) = (rxt(k,3820)* y(k,705) + rxt(k,58))* y(k,448)
         prod(k,84) = 0.
         loss(k,604) = ((rxt(k,3023) +rxt(k,3024) +rxt(k,3025) +rxt(k,3026))* y(k,705))* y(k,449)
         prod(k,604) = 0.
         loss(k,155) = ((rxt(k,245) +rxt(k,246))* y(k,705))* y(k,450)
         prod(k,155) = 0.
         loss(k,145) = ((rxt(k,253) +rxt(k,254))* y(k,705))* y(k,451)
         prod(k,145) = 0.
         loss(k,171) = ((rxt(k,261) +rxt(k,262) +rxt(k,263))* y(k,705))* y(k,452)
         prod(k,171) = 0.
         loss(k,172) = ((rxt(k,270) +rxt(k,271))* y(k,705))* y(k,453)
         prod(k,172) = 0.
         loss(k,203) = ((rxt(k,277) +rxt(k,278) +rxt(k,279))* y(k,705))* y(k,454)
         prod(k,203) = 0.
         loss(k,156) = ((rxt(k,289) +rxt(k,290))* y(k,705))* y(k,455)
         prod(k,156) = 0.
         loss(k,782) = ((rxt(k,1442) +rxt(k,1443) +rxt(k,1444))* y(k,693) + (rxt(k,1438) +rxt(k,1439) +rxt(k,1440) +rxt(k,1441)) &
* y(k,696) + (rxt(k,1435) +rxt(k,1436) +rxt(k,1437))* y(k,705) +rxt(k,1445)* y(k,839))* y(k,456)
         prod(k,782) = 0.
         loss(k,880) = ((rxt(k,1203) +rxt(k,1204) +rxt(k,1205))* y(k,693) + (rxt(k,1200) +rxt(k,1201) +rxt(k,1202))* y(k,696) &
 + (rxt(k,1197) +rxt(k,1198) +rxt(k,1199))* y(k,705) +rxt(k,1206)* y(k,839))* y(k,457)
         prod(k,880) = 0.
         loss(k,173) = ((rxt(k,3681) +rxt(k,3682) +rxt(k,3683))* y(k,705))* y(k,458)
         prod(k,173) = 0.
         loss(k,372) = ((rxt(k,3042) +rxt(k,3043) +rxt(k,3044) +rxt(k,3045))* y(k,705))* y(k,459)
         prod(k,372) = 0.
         loss(k,204) = ((rxt(k,3178) +rxt(k,3179) +rxt(k,3180))* y(k,705))* y(k,460)
         prod(k,204) = 0.
         loss(k,373) = ((rxt(k,3324) +rxt(k,3325) +rxt(k,3326) +rxt(k,3327))* y(k,705))* y(k,461)
         prod(k,373) = 0.
         loss(k,562) = ((rxt(k,3564) +rxt(k,3565) +rxt(k,3566) +rxt(k,3567))* y(k,705))* y(k,462)
         prod(k,562) = 0.
         loss(k,676) = ((rxt(k,3774) +rxt(k,3775) +rxt(k,3776) +rxt(k,3777) +rxt(k,3778))* y(k,705))* y(k,463)
         prod(k,676) = 0.
         loss(k,263) = ((rxt(k,2757) +rxt(k,2758) +rxt(k,2759))* y(k,705) + rxt(k,59) + rxt(k,60))* y(k,464)
         prod(k,263) = 0.
         loss(k,264) = ((rxt(k,2705) +rxt(k,2706) +rxt(k,2707))* y(k,705))* y(k,465)
         prod(k,264) = 0.
         loss(k,687) = ((rxt(k,3676) +rxt(k,3677) +rxt(k,3678) +rxt(k,3679) +rxt(k,3680))* y(k,705))* y(k,466)
         prod(k,687) = 0.
         loss(k,455) = ((rxt(k,3672) +rxt(k,3673) +rxt(k,3674) +rxt(k,3675))* y(k,705))* y(k,467)
         prod(k,455) = 0.
         loss(k,456) = ((rxt(k,3316) +rxt(k,3317) +rxt(k,3318) +rxt(k,3319))* y(k,705))* y(k,468)
         prod(k,456) = 0.
         loss(k,457) = ((rxt(k,3568) +rxt(k,3569) +rxt(k,3570) +rxt(k,3571))* y(k,705))* y(k,469)
         prod(k,457) = 0.
         loss(k,596) = ((rxt(k,3035) +rxt(k,3036) +rxt(k,3037) +rxt(k,3038))* y(k,705))* y(k,470)
         prod(k,596) = 0.
         loss(k,651) = ((rxt(k,3328) +rxt(k,3329) +rxt(k,3330) +rxt(k,3331))* y(k,705))* y(k,471)
         prod(k,651) = 0.
         loss(k,374) = ((rxt(k,3561) +rxt(k,3562) +rxt(k,3563))* y(k,705))* y(k,472)
         prod(k,374) = 0.
         loss(k,459) = ((rxt(k,3162) +rxt(k,3163) +rxt(k,3164) +rxt(k,3165))* y(k,705))* y(k,473)
         prod(k,459) = 0.
         loss(k,550) = ((rxt(k,2964) +rxt(k,2965) +rxt(k,2966) +rxt(k,2967))* y(k,705))* y(k,474)
         prod(k,550) = 0.
         loss(k,460) = ((rxt(k,3356) +rxt(k,3357) +rxt(k,3358) +rxt(k,3359))* y(k,705))* y(k,475)
         prod(k,460) = 0.
         loss(k,375) = ((rxt(k,3077) +rxt(k,3078) +rxt(k,3079))* y(k,705) + rxt(k,61) + rxt(k,62))* y(k,476)
         prod(k,375) = 0.
         loss(k,552) = ((rxt(k,3684) +rxt(k,3685) +rxt(k,3686) +rxt(k,3687))* y(k,705))* y(k,477)
         prod(k,552) = 0.
         loss(k,461) = ((rxt(k,3158) +rxt(k,3159) +rxt(k,3160) +rxt(k,3161))* y(k,705))* y(k,478)
         prod(k,461) = 0.
         loss(k,895) = ((rxt(k,2054) +rxt(k,2055) +rxt(k,2056) +rxt(k,2057))* y(k,693) + (rxt(k,2049) +rxt(k,2050) +rxt(k,2051) + &
rxt(k,2052) +rxt(k,2053))* y(k,696) + (rxt(k,2045) +rxt(k,2046) +rxt(k,2047) +rxt(k,2048))* y(k,705) +rxt(k,2058) &
* y(k,839))* y(k,479)
         prod(k,895) = 0.
         loss(k,763) = (rxt(k,4011)* y(k,546) + (rxt(k,4008) +rxt(k,4009) +rxt(k,4010))* y(k,693) + (rxt(k,4005) +rxt(k,4006) + &
rxt(k,4007))* y(k,696) + (rxt(k,4002) +rxt(k,4003) +rxt(k,4004))* y(k,705))* y(k,480)
         prod(k,763) = 0.
         loss(k,752) = (rxt(k,3836)* y(k,546) + (rxt(k,3833) +rxt(k,3834) +rxt(k,3835))* y(k,693) + (rxt(k,3831) +rxt(k,3832)) &
* y(k,696) + (rxt(k,3829) +rxt(k,3830))* y(k,705))* y(k,481)
         prod(k,752) = 0.
         loss(k,127) = ((rxt(k,2557) +rxt(k,2558))* y(k,705))* y(k,482)
         prod(k,127) = 0.
         loss(k,653) = (rxt(k,3869)* y(k,693) + (rxt(k,3867) +rxt(k,3868))* y(k,705))* y(k,483)
         prod(k,653) = 0.
         loss(k,128) = ((rxt(k,2559) +rxt(k,2560))* y(k,705))* y(k,484)
         prod(k,128) = 0.
         loss(k,611) = ((rxt(k,3320) +rxt(k,3321) +rxt(k,3322) +rxt(k,3323))* y(k,705))* y(k,485)
         prod(k,611) = 0.
         loss(k,554) = ((rxt(k,3408) +rxt(k,3409) +rxt(k,3410) +rxt(k,3411))* y(k,705))* y(k,486)
         prod(k,554) = 0.
         loss(k,555) = ((rxt(k,3412) +rxt(k,3413) +rxt(k,3414) +rxt(k,3415))* y(k,705))* y(k,487)
         prod(k,555) = 0.
         loss(k,557) = ((rxt(k,3404) +rxt(k,3405) +rxt(k,3406) +rxt(k,3407))* y(k,705))* y(k,488)
         prod(k,557) = 0.
         loss(k,615) = ((rxt(k,3170) +rxt(k,3171) +rxt(k,3172) +rxt(k,3173))* y(k,705))* y(k,489)
         prod(k,615) = 0.
         loss(k,265) = ((rxt(k,3039) +rxt(k,3040) +rxt(k,3041))* y(k,705))* y(k,490)
         prod(k,265) = 0.
         loss(k,560) = ((rxt(k,3166) +rxt(k,3167) +rxt(k,3168) +rxt(k,3169))* y(k,705))* y(k,491)
         prod(k,560) = 0.
         loss(k,668) = ((rxt(k,3154) +rxt(k,3155) +rxt(k,3156) +rxt(k,3157))* y(k,705))* y(k,492)
         prod(k,668) = 0.
         loss(k,462) = ((rxt(k,2979) +rxt(k,2980) +rxt(k,2981) +rxt(k,2982))* y(k,705))* y(k,493)
         prod(k,462) = 0.
         loss(k,376) = ((rxt(k,3558) +rxt(k,3559) +rxt(k,3560))* y(k,705))* y(k,494)
         prod(k,376) = 0.
         loss(k,377) = ((rxt(k,3297) +rxt(k,3298) +rxt(k,3299))* y(k,705))* y(k,495)
         prod(k,377) = 0.
         loss(k,675) = ((rxt(k,3221) +rxt(k,3222) +rxt(k,3223) +rxt(k,3224) +rxt(k,3225))* y(k,705))* y(k,496)
         prod(k,675) = 0.
         loss(k,157) = ((rxt(k,3999) +rxt(k,4000) +rxt(k,4001))* y(k,705))* y(k,497)
         prod(k,157) = 0.
         loss(k,379) = ((rxt(k,2919) +rxt(k,2920) +rxt(k,2921))* y(k,705))* y(k,498)
         prod(k,379) = 0.
         loss(k,720) = ((rxt(k,4142) +rxt(k,4143) +rxt(k,4144))* y(k,693) + (rxt(k,4139) +rxt(k,4140) +rxt(k,4141))* y(k,705)) &
* y(k,499)
         prod(k,720) = 0.
         loss(k,310) = ((rxt(k,2649) +rxt(k,2650) +rxt(k,2651))* y(k,705))* y(k,500)
         prod(k,310) = 0.
         loss(k,851) = ((rxt(k,2793) +rxt(k,2794) +rxt(k,2795))* y(k,693) + (rxt(k,2791) +rxt(k,2792))* y(k,696) + (rxt(k,2788) + &
rxt(k,2789) +rxt(k,2790))* y(k,705) +rxt(k,2796)* y(k,839))* y(k,501)
         prod(k,851) = 0.
         loss(k,101) = ((rxt(k,3982) +rxt(k,3983))* y(k,705))* y(k,502)
         prod(k,101) = 0.
         loss(k,751) = (rxt(k,3844)* y(k,546) + (rxt(k,3841) +rxt(k,3842) +rxt(k,3843))* y(k,693) + (rxt(k,3839) +rxt(k,3840)) &
* y(k,696) + (rxt(k,3837) +rxt(k,3838))* y(k,705))* y(k,503)
         prod(k,751) = 0.
         loss(k,463) = ((rxt(k,318) +rxt(k,319) +rxt(k,320) +rxt(k,321))* y(k,705))* y(k,504)
         prod(k,463) = 0.
         loss(k,660) = ((rxt(k,2937) +rxt(k,2938) +rxt(k,2939) +rxt(k,2940))* y(k,705))* y(k,505)
         prod(k,660) = 0.
         loss(k,464) = ((rxt(k,657) +rxt(k,658) +rxt(k,659) +rxt(k,660))* y(k,705))* y(k,506)
         prod(k,464) = 0.
         loss(k,266) = ((rxt(k,741) +rxt(k,742) +rxt(k,743))* y(k,705))* y(k,507)
         prod(k,266) = 0.
         loss(k,205) = ((rxt(k,2541) +rxt(k,2542) +rxt(k,2543))* y(k,705))* y(k,508)
         prod(k,205) = 0.
         loss(k,311) = ((rxt(k,3046) +rxt(k,3047) +rxt(k,3048))* y(k,705))* y(k,509)
         prod(k,311) = 0.
         loss(k,102) = (rxt(k,2506)* y(k,705))* y(k,510)
         prod(k,102) = 0.
         loss(k,129) = (rxt(k,242)* y(k,705))* y(k,511)
         prod(k,129) = 0.
         loss(k,767) = ((rxt(k,238) +rxt(k,239))* y(k,693) + (rxt(k,1067) +rxt(k,1068))* y(k,696) + (rxt(k,236) +rxt(k,237)) &
* y(k,705) + (rxt(k,1069) +rxt(k,1070) +rxt(k,1071))* y(k,839))* y(k,512)
         prod(k,767) = 0.
         loss(k,688) = ((rxt(k,2868) +rxt(k,2869) +rxt(k,2870) +rxt(k,2871) +rxt(k,2872))* y(k,705))* y(k,513)
         prod(k,688) = 0.
         loss(k,859) = ((rxt(k,2933) +rxt(k,2934) +rxt(k,2935))* y(k,693) + (rxt(k,2929) +rxt(k,2930) +rxt(k,2931) +rxt(k,2932)) &
* y(k,696) + (rxt(k,2926) +rxt(k,2927) +rxt(k,2928))* y(k,705) +rxt(k,2936)* y(k,839))* y(k,514)
         prod(k,859) = 0.
         loss(k,130) = ((rxt(k,3865) +rxt(k,3866))* y(k,705))* y(k,515)
         prod(k,130) = 0.
         loss(k,467) = ((rxt(k,2849) +rxt(k,2850) +rxt(k,2851) +rxt(k,2852))* y(k,705))* y(k,516)
         prod(k,467) = 0.
         loss(k,382) = ((rxt(k,312) +rxt(k,313) +rxt(k,314))* y(k,705))* y(k,517)
         prod(k,382) = 0.
         loss(k,580) = ((rxt(k,2688) +rxt(k,2689) +rxt(k,2690) +rxt(k,2691))* y(k,705))* y(k,518)
         prod(k,580) = 0.
         loss(k,174) = ((rxt(k,298) +rxt(k,299))* y(k,705))* y(k,519)
         prod(k,174) = 0.
         loss(k,761) = (rxt(k,4040)* y(k,546) + (rxt(k,4037) +rxt(k,4038) +rxt(k,4039))* y(k,693) + (rxt(k,4034) +rxt(k,4035) + &
rxt(k,4036))* y(k,696) + (rxt(k,4031) +rxt(k,4032) +rxt(k,4033))* y(k,705))* y(k,520)
         prod(k,761) = 0.
         loss(k,758) = (rxt(k,4104)* y(k,546) + (rxt(k,4101) +rxt(k,4102) +rxt(k,4103))* y(k,693) + (rxt(k,4098) +rxt(k,4099) + &
rxt(k,4100))* y(k,696) + (rxt(k,4095) +rxt(k,4096) +rxt(k,4097))* y(k,705))* y(k,521)
         prod(k,758) = 0.
         loss(k,754) = (rxt(k,3853)* y(k,546) + (rxt(k,3850) +rxt(k,3851) +rxt(k,3852))* y(k,693) + (rxt(k,3848) +rxt(k,3849)) &
* y(k,696) + (rxt(k,3845) +rxt(k,3846) +rxt(k,3847))* y(k,705))* y(k,522)
         prod(k,754) = 0.
         loss(k,469) = ((rxt(k,2781) +rxt(k,2782) +rxt(k,2783) +rxt(k,2784))* y(k,705))* y(k,523)
         prod(k,469) = 0.
         loss(k,158) = ((rxt(k,4115) +rxt(k,4116))* y(k,705))* y(k,524)
         prod(k,158) = 0.
         loss(k,146) = ((rxt(k,2500) +rxt(k,2501))* y(k,705))* y(k,525)
         prod(k,146) = 0.
         loss(k,588) = ((rxt(k,2818) +rxt(k,2819) +rxt(k,2820) +rxt(k,2821))* y(k,705))* y(k,526)
         prod(k,588) = 0.
         loss(k,103) = (rxt(k,2563)* y(k,705))* y(k,527)
         prod(k,103) = 0.
         loss(k,312) = ((rxt(k,2603) +rxt(k,2604) +rxt(k,2605))* y(k,705))* y(k,528)
         prod(k,312) = 0.
         loss(k,910) = ((rxt(k,3549) +rxt(k,3550) +rxt(k,3551) +rxt(k,3552) +rxt(k,3553))* y(k,693) + (rxt(k,3543) +rxt(k,3544) + &
rxt(k,3545) +rxt(k,3546) +rxt(k,3547) +rxt(k,3548))* y(k,696) + (rxt(k,3538) +rxt(k,3539) +rxt(k,3540) + &
rxt(k,3541) +rxt(k,3542))* y(k,705) +rxt(k,3554)* y(k,839))* y(k,529)
         prod(k,910) = 0.
         loss(k,385) = (rxt(k,2505)* y(k,693) +rxt(k,2504)* y(k,705) + rxt(k,63))* y(k,530)
         prod(k,385) = 0.
         loss(k,731) = (rxt(k,2810)* y(k,693) + (rxt(k,2807) +rxt(k,2808) +rxt(k,2809))* y(k,705) + rxt(k,64) + rxt(k,65)) &
* y(k,531)
         prod(k,731) = 0.
         loss(k,913) = ((rxt(k,508) +rxt(k,509))* y(k,693) +rxt(k,507)* y(k,705) + rxt(k,66) + rxt(k,67))* y(k,532)
         prod(k,913) = (rxt(k,4297)*y(k,690) +rxt(k,4299)*y(k,693) +rxt(k,4300)*y(k,620) +rxt(k,4301)*y(k,758) + &
rxt(k,4302)*y(k,370) +rxt(k,4303)*y(k,589) +.500*rxt(k,4304)*y(k,633) +.500*rxt(k,4305)*y(k,768) + &
.500*rxt(k,4306)*y(k,769))*y(k,852) + (1.296*rxt(k,523)*y(k,348) +1.296*rxt(k,530)*y(k,349) + &
2.196*rxt(k,538)*y(k,350) +.092*rxt(k,566)*y(k,582) +rxt(k,2513)*y(k,344) +1.500*rxt(k,2568)*y(k,447))*y(k,696) &
 +1.400*rxt(k,240)*y(k,705)*y(k,343) +.390*rxt(k,39)*y(k,348) +rxt(k,147)*y(k,756)
         loss(k,104) = ((rxt(k,2507) +rxt(k,2508))* y(k,705))* y(k,533)
         prod(k,104) = 0.
         loss(k,268) = ((rxt(k,3427) +rxt(k,3428) +rxt(k,3429))* y(k,705))* y(k,534)
         prod(k,268) = 0.
         loss(k,147) = ((rxt(k,2561) +rxt(k,2562))* y(k,705))* y(k,535)
         prod(k,147) = 0.
         loss(k,105) = (rxt(k,402)* y(k,705))* y(k,536)
         prod(k,105) =rxt(k,69)*y(k,537)
         loss(k,914) = (rxt(k,482)* y(k,693) +rxt(k,481)* y(k,705) + rxt(k,68) + rxt(k,69) + het_rates(k,11))* y(k,537)
         prod(k,914) = (.300*rxt(k,546)*y(k,590) +.200*rxt(k,555)*y(k,664) +.496*rxt(k,565)*y(k,582) +2.000*rxt(k,588)*y(k,586) + &
2.000*rxt(k,1068)*y(k,512) +1.500*rxt(k,1075)*y(k,739) +2.000*rxt(k,1086)*y(k,52) +2.001*rxt(k,1096)*y(k,585) + &
1.500*rxt(k,1121)*y(k,33) +2.000*rxt(k,1136)*y(k,84) +2.000*rxt(k,1148)*y(k,263) +2.668*rxt(k,1159)*y(k,176) + &
rxt(k,1211)*y(k,780) +2.000*rxt(k,1222)*y(k,49) +2.000*rxt(k,1240)*y(k,72) +2.000*rxt(k,1251)*y(k,213) + &
2.500*rxt(k,1262)*y(k,264) +2.500*rxt(k,1275)*y(k,312) +2.668*rxt(k,1286)*y(k,120) +2.668*rxt(k,1296)*y(k,168) + &
2.668*rxt(k,1307)*y(k,177) +1.250*rxt(k,1452)*y(k,781) +2.000*rxt(k,1490)*y(k,71) +2.500*rxt(k,1501)*y(k,215) + &
2.500*rxt(k,1514)*y(k,265) +2.668*rxt(k,1526)*y(k,136) +2.668*rxt(k,1538)*y(k,113) +2.500*rxt(k,1550)*y(k,208) + &
2.668*rxt(k,1562)*y(k,178) +2.668*rxt(k,1573)*y(k,101) +2.000*rxt(k,1795)*y(k,83) +2.668*rxt(k,1805)*y(k,127) + &
2.668*rxt(k,1818)*y(k,267) +2.000*rxt(k,1932)*y(k,67) +2.500*rxt(k,1943)*y(k,291) +2.000*rxt(k,1966)*y(k,53) + &
1.380*rxt(k,2036)*y(k,879) +.680*rxt(k,2064)*y(k,771) +2.000*rxt(k,2088)*y(k,55) +2.000*rxt(k,2108)*y(k,56) + &
2.000*rxt(k,2128)*y(k,57) +2.000*rxt(k,2148)*y(k,58) +.800*rxt(k,2197)*y(k,776) +rxt(k,2513)*y(k,344) + &
rxt(k,2534)*y(k,346) +2.668*rxt(k,2610)*y(k,591) +rxt(k,2622)*y(k,606) +rxt(k,2631)*y(k,837) + &
.200*rxt(k,2640)*y(k,552) +.900*rxt(k,2731)*y(k,595) +rxt(k,2792)*y(k,501) +2.668*rxt(k,2801)*y(k,628) + &
2.668*rxt(k,2931)*y(k,514) +2.004*rxt(k,3011)*y(k,554) +rxt(k,3103)*y(k,668) +rxt(k,3114)*y(k,567) + &
2.668*rxt(k,3232)*y(k,363) +2.668*rxt(k,3245)*y(k,569) +.070*rxt(k,3474)*y(k,587) +.114*rxt(k,3545)*y(k,529) + &
rxt(k,3585)*y(k,172) +2.001*rxt(k,4045)*y(k,439))*y(k,696) + (rxt(k,488)*y(k,637) +.300*rxt(k,494)*y(k,445) + &
.010*rxt(k,573)*y(k,731) +.087*rxt(k,1230)*y(k,28) +.067*rxt(k,2506)*y(k,510) +.070*rxt(k,2507)*y(k,533) + &
.531*rxt(k,2547)*y(k,550) +.081*rxt(k,2549)*y(k,729) +.366*rxt(k,2559)*y(k,484) +.034*rxt(k,2561)*y(k,535) + &
.036*rxt(k,2594)*y(k,575) +.066*rxt(k,2677)*y(k,20) +.012*rxt(k,2778)*y(k,190) +.126*rxt(k,2882)*y(k,725) + &
.024*rxt(k,3664)*y(k,818) +rxt(k,3818)*y(k,631) +.040*rxt(k,4181)*y(k,733))*y(k,705) + (rxt(k,230)*y(k,548) + &
rxt(k,231)*y(k,633) +rxt(k,403)*y(k,690) +rxt(k,404)*y(k,693) +2.000*rxt(k,405)*y(k,633) + &
.750*rxt(k,409)*y(k,768) +.750*rxt(k,414)*y(k,769) +2.000*rxt(k,421)*y(k,620) +2.000*rxt(k,433)*y(k,758) + &
2.000*rxt(k,450)*y(k,370) +2.000*rxt(k,466)*y(k,589) +rxt(k,3953)*y(k,427) +.500*rxt(k,4314)*y(k,853))*y(k,633) &
 + (rxt(k,461)*y(k,690) +.880*rxt(k,462)*y(k,548) +2.000*rxt(k,464)*y(k,693) +rxt(k,467)*y(k,768) + &
rxt(k,468)*y(k,769) +2.000*rxt(k,469)*y(k,620) +2.000*rxt(k,471)*y(k,758) +2.000*rxt(k,473)*y(k,370) + &
2.000*rxt(k,475)*y(k,589) +2.000*rxt(k,3962)*y(k,427) +rxt(k,4313)*y(k,853))*y(k,589) + (rxt(k,4307)*y(k,690) + &
rxt(k,4309)*y(k,693) +rxt(k,4310)*y(k,620) +rxt(k,4311)*y(k,758) +rxt(k,4312)*y(k,370) + &
.500*rxt(k,4315)*y(k,768) +.500*rxt(k,4316)*y(k,769))*y(k,853) +rxt(k,37)*y(k,344) +rxt(k,56)*y(k,445) +rxt(k,63) &
*y(k,530) +rxt(k,67)*y(k,532) +rxt(k,74)*y(k,550) +2.000*rxt(k,76)*y(k,552) +.600*rxt(k,83)*y(k,582) +rxt(k,87) &
*y(k,590) +1.200*rxt(k,88)*y(k,594) +rxt(k,92)*y(k,631) +.370*rxt(k,153)*y(k,761) +1.323*rxt(k,165)*y(k,765)
         loss(k,842) = (rxt(k,489)* y(k,705))* y(k,538)
         prod(k,842) = (.999*rxt(k,546)*y(k,590) +1.404*rxt(k,556)*y(k,664) +.400*rxt(k,566)*y(k,582) +1.020*rxt(k,590)*y(k,586) + &
.740*rxt(k,1068)*y(k,512) +.555*rxt(k,1075)*y(k,739) +.740*rxt(k,1087)*y(k,52) +.369*rxt(k,1097)*y(k,585) + &
.555*rxt(k,1121)*y(k,33) +.740*rxt(k,1137)*y(k,84) +.740*rxt(k,1149)*y(k,263) +.492*rxt(k,1160)*y(k,176) + &
.372*rxt(k,1211)*y(k,780) +.740*rxt(k,1223)*y(k,49) +.740*rxt(k,1241)*y(k,72) +.740*rxt(k,1251)*y(k,213) + &
.925*rxt(k,1263)*y(k,264) +.925*rxt(k,1276)*y(k,312) +.492*rxt(k,1287)*y(k,120) +.492*rxt(k,1296)*y(k,168) + &
.492*rxt(k,1308)*y(k,177) +.465*rxt(k,1453)*y(k,781) +.740*rxt(k,1491)*y(k,71) +.925*rxt(k,1502)*y(k,215) + &
.925*rxt(k,1515)*y(k,265) +.492*rxt(k,1527)*y(k,136) +.492*rxt(k,1539)*y(k,113) +.925*rxt(k,1551)*y(k,208) + &
.492*rxt(k,1563)*y(k,178) +.492*rxt(k,1573)*y(k,101) +.740*rxt(k,1795)*y(k,83) +.492*rxt(k,1806)*y(k,127) + &
.492*rxt(k,1818)*y(k,267) +.740*rxt(k,1932)*y(k,67) +.925*rxt(k,1944)*y(k,291) +.740*rxt(k,1966)*y(k,53) + &
1.710*rxt(k,2037)*y(k,879) +1.228*rxt(k,2064)*y(k,771) +.740*rxt(k,2088)*y(k,55) +.740*rxt(k,2108)*y(k,56) + &
.740*rxt(k,2128)*y(k,57) +.740*rxt(k,2148)*y(k,58) +1.200*rxt(k,2197)*y(k,776) +.370*rxt(k,2513)*y(k,344) + &
.370*rxt(k,2534)*y(k,346) +.492*rxt(k,2611)*y(k,591) +.370*rxt(k,2622)*y(k,606) +.370*rxt(k,2631)*y(k,837) + &
.666*rxt(k,2640)*y(k,552) +.777*rxt(k,2731)*y(k,595) +.370*rxt(k,2792)*y(k,501) +.492*rxt(k,2801)*y(k,628) + &
.492*rxt(k,2931)*y(k,514) +.740*rxt(k,3012)*y(k,554) +.370*rxt(k,3103)*y(k,668) +.370*rxt(k,3114)*y(k,567) + &
.492*rxt(k,3232)*y(k,363) +.492*rxt(k,3245)*y(k,569) +.025*rxt(k,3475)*y(k,587) +.370*rxt(k,3585)*y(k,172) + &
.369*rxt(k,4045)*y(k,439))*y(k,696) + (.600*rxt(k,241)*y(k,343) +.004*rxt(k,324)*y(k,661) + &
1.314*rxt(k,2501)*y(k,525) +1.314*rxt(k,2503)*y(k,623) +1.448*rxt(k,2522)*y(k,741) +.021*rxt(k,2539)*y(k,39) + &
2.154*rxt(k,2542)*y(k,508) +2.008*rxt(k,2583)*y(k,18) +.018*rxt(k,2648)*y(k,48) +1.056*rxt(k,2658)*y(k,394) + &
.072*rxt(k,2661)*y(k,583) +.564*rxt(k,2836)*y(k,403) +.004*rxt(k,2879)*y(k,657) +.024*rxt(k,2981)*y(k,493) + &
.004*rxt(k,3029)*y(k,204) +.004*rxt(k,3096)*y(k,294) +.004*rxt(k,3168)*y(k,491) +.008*rxt(k,3172)*y(k,489) + &
.036*rxt(k,3183)*y(k,823) +.032*rxt(k,3187)*y(k,162) +.120*rxt(k,3314)*y(k,174) +.020*rxt(k,3322)*y(k,485) + &
.008*rxt(k,3330)*y(k,471) +.036*rxt(k,3334)*y(k,821) +.028*rxt(k,3338)*y(k,813) +.004*rxt(k,3410)*y(k,486) + &
.008*rxt(k,3414)*y(k,487) +.028*rxt(k,3418)*y(k,824) +.044*rxt(k,3425)*y(k,815) +.024*rxt(k,3574)*y(k,820) + &
.084*rxt(k,3578)*y(k,835) +.032*rxt(k,3690)*y(k,814) +.052*rxt(k,3713)*y(k,822) +.330*rxt(k,3886)*y(k,607) + &
.660*rxt(k,3982)*y(k,502))*y(k,705)
         loss(k,106) = (rxt(k,3875)* y(k,705))* y(k,539)
         prod(k,106) = 0.
         loss(k,131) = (rxt(k,3856)* y(k,705))* y(k,540)
         prod(k,131) = 0.
         loss(k,132) = ((rxt(k,3973) +rxt(k,3974))* y(k,705))* y(k,541)
         prod(k,132) = 0.
         loss(k,133) = ((rxt(k,4070) +rxt(k,4071))* y(k,705))* y(k,542)
         prod(k,133) = 0.
         loss(k,134) = ((rxt(k,4113) +rxt(k,4114))* y(k,705))* y(k,543)
         prod(k,134) = 0.
         loss(k,135) = ((rxt(k,4111) +rxt(k,4112))* y(k,705))* y(k,544)
         prod(k,135) = 0.
         loss(k,107) = ( + rxt(k,70))* y(k,545)
         prod(k,107) =rxt(k,3823)*y(k,559)*y(k,548)
         loss(k,928) = (rxt(k,4022)* y(k,159) +rxt(k,4027)* y(k,353) +rxt(k,4011)* y(k,480) +rxt(k,3836)* y(k,481) +rxt(k,3844) &
* y(k,503) +rxt(k,4040)* y(k,520) +rxt(k,4104)* y(k,521) +rxt(k,3853)* y(k,522) +rxt(k,4160)* y(k,574) &
 +rxt(k,3903)* y(k,580) +rxt(k,3817)* y(k,608) +rxt(k,221)* y(k,705) +rxt(k,3989)* y(k,806) +rxt(k,4082) &
* y(k,810) +rxt(k,3895)* y(k,829) + rxt(k,71) + het_rates(k,4))* y(k,546)
         prod(k,928) = (.400*rxt(k,397)*y(k,548) +rxt(k,482)*y(k,537) +rxt(k,484)*y(k,423) +rxt(k,487)*y(k,757) + &
2.000*rxt(k,508)*y(k,532) +rxt(k,511)*y(k,645) +rxt(k,514)*y(k,446) +rxt(k,517)*y(k,358) +rxt(k,548)*y(k,590) + &
.600*rxt(k,568)*y(k,582) +rxt(k,2505)*y(k,530) +2.901*rxt(k,2515)*y(k,344) +rxt(k,2526)*y(k,737) + &
2.092*rxt(k,2571)*y(k,447) +rxt(k,2588)*y(k,192) +rxt(k,2592)*y(k,59) +.030*rxt(k,2614)*y(k,591) + &
2.000*rxt(k,2642)*y(k,552) +rxt(k,2740)*y(k,94) +4.000*rxt(k,2746)*y(k,270) +rxt(k,2753)*y(k,60) + &
.759*rxt(k,2794)*y(k,501) +.030*rxt(k,2804)*y(k,628) +rxt(k,2810)*y(k,531) +rxt(k,2894)*y(k,62) + &
.072*rxt(k,2934)*y(k,514) +rxt(k,3001)*y(k,653) +3.628*rxt(k,3015)*y(k,554) +rxt(k,3066)*y(k,64) + &
rxt(k,3070)*y(k,200) +3.780*rxt(k,3105)*y(k,668) +4.880*rxt(k,3116)*y(k,567) +rxt(k,3192)*y(k,66) + &
1.656*rxt(k,3235)*y(k,363) +2.508*rxt(k,3248)*y(k,569) +3.000*rxt(k,3587)*y(k,172) +3.000*rxt(k,3814)*y(k,608) + &
3.000*rxt(k,3833)*y(k,481) +3.000*rxt(k,3841)*y(k,503) +3.000*rxt(k,3850)*y(k,522) +2.000*rxt(k,3893)*y(k,829) + &
3.000*rxt(k,3900)*y(k,580) +3.000*rxt(k,3909)*y(k,50) +rxt(k,3987)*y(k,806) +3.000*rxt(k,3997)*y(k,660) + &
3.000*rxt(k,4009)*y(k,480) +3.000*rxt(k,4020)*y(k,159) +rxt(k,4026)*y(k,353) +3.000*rxt(k,4038)*y(k,520) + &
rxt(k,4062)*y(k,160) +3.000*rxt(k,4080)*y(k,810) +3.000*rxt(k,4102)*y(k,521) +3.000*rxt(k,4136)*y(k,658) + &
3.000*rxt(k,4142)*y(k,499) +3.000*rxt(k,4158)*y(k,574) +3.000*rxt(k,4165)*y(k,720) +3.000*rxt(k,4172)*y(k,827)) &
*y(k,693) + (2.000*rxt(k,387) +2.000*rxt(k,388))*y(k,666) + (rxt(k,220)*y(k,691) +rxt(k,3819)*y(k,434))*y(k,705) &
 +.300*rxt(k,4521)*y(k,910)
         loss(k,391) = (rxt(k,396)* y(k,705) + rxt(k,72) + rxt(k,224) + het_rates(k,5))* y(k,547)
         prod(k,391) =rxt(k,223)*y(k,691)*y(k,548)
         loss(k,932) = ((rxt(k,446) +rxt(k,447))* y(k,370) +rxt(k,3950)* y(k,427) + 2.*(rxt(k,226) +rxt(k,227))* y(k,548) &
 +rxt(k,3823)* y(k,559) +rxt(k,3824)* y(k,578) + (rxt(k,462) +rxt(k,463))* y(k,589) + (rxt(k,418) +rxt(k,419)) &
* y(k,620) + (rxt(k,229) +rxt(k,230))* y(k,633) +rxt(k,395)* y(k,690) +rxt(k,223)* y(k,691) + (rxt(k,397) + &
rxt(k,398))* y(k,693) +rxt(k,3988)* y(k,695) +rxt(k,225)* y(k,696) +rxt(k,401)* y(k,705) + (rxt(k,429) + &
rxt(k,430))* y(k,758) +rxt(k,407)* y(k,768) +rxt(k,412)* y(k,769) +rxt(k,479)* y(k,841) + het_rates(k,9)) &
* y(k,548)
         prod(k,932) = (rxt(k,222) +rxt(k,228) +.600*rxt(k,240)*y(k,343) +1.900*rxt(k,298)*y(k,519) +2.859*rxt(k,303)*y(k,573) + &
rxt(k,393)*y(k,693) +rxt(k,394)*y(k,696) +rxt(k,400)*y(k,549) +rxt(k,402)*y(k,536) +rxt(k,481)*y(k,537) + &
rxt(k,488)*y(k,637) +rxt(k,489)*y(k,538) +.740*rxt(k,502)*y(k,756) +.700*rxt(k,507)*y(k,532) + &
2.360*rxt(k,572)*y(k,731) +1.134*rxt(k,577)*y(k,761) +2.280*rxt(k,600)*y(k,359) +.724*rxt(k,2173)*y(k,833) + &
.560*rxt(k,2177)*y(k,383) +.636*rxt(k,2181)*y(k,665) +.805*rxt(k,2185)*y(k,706) +.795*rxt(k,2190)*y(k,746) + &
.456*rxt(k,2203)*y(k,676) +.588*rxt(k,2207)*y(k,572) +.750*rxt(k,2211)*y(k,644) +.710*rxt(k,2216)*y(k,704) + &
.740*rxt(k,2221)*y(k,721) +.155*rxt(k,2226)*y(k,14) +.110*rxt(k,2231)*y(k,16) +.160*rxt(k,2236)*y(k,32) + &
.420*rxt(k,2248)*y(k,373) +.708*rxt(k,2252)*y(k,807) +.720*rxt(k,2256)*y(k,596) +.670*rxt(k,2261)*y(k,697) + &
.695*rxt(k,2266)*y(k,711) +.755*rxt(k,2271)*y(k,719) +.150*rxt(k,2276)*y(k,240) +.105*rxt(k,2281)*y(k,302) + &
.156*rxt(k,2286)*y(k,324) +rxt(k,2290)*y(k,816) +.944*rxt(k,2294)*y(k,667) +.368*rxt(k,2298)*y(k,376) + &
.670*rxt(k,2302)*y(k,597) +.600*rxt(k,2307)*y(k,698) +.625*rxt(k,2312)*y(k,712) +.140*rxt(k,2317)*y(k,241) + &
.100*rxt(k,2322)*y(k,303) +.152*rxt(k,2327)*y(k,325) +.944*rxt(k,2331)*y(k,630) +.324*rxt(k,2335)*y(k,377) + &
.650*rxt(k,2339)*y(k,598) +.575*rxt(k,2344)*y(k,699) +.600*rxt(k,2349)*y(k,713) +.140*rxt(k,2354)*y(k,242) + &
.095*rxt(k,2359)*y(k,304) +.152*rxt(k,2364)*y(k,326) +.944*rxt(k,2368)*y(k,119) +.292*rxt(k,2372)*y(k,378) + &
.496*rxt(k,2376)*y(k,599) +.535*rxt(k,2380)*y(k,700) +.555*rxt(k,2385)*y(k,714) +.135*rxt(k,2390)*y(k,243) + &
.090*rxt(k,2395)*y(k,305) +.148*rxt(k,2400)*y(k,327) +.264*rxt(k,2404)*y(k,380) +.472*rxt(k,2408)*y(k,600) + &
.500*rxt(k,2412)*y(k,701) +.520*rxt(k,2417)*y(k,715) +.130*rxt(k,2422)*y(k,244) +.090*rxt(k,2427)*y(k,306) + &
.144*rxt(k,2432)*y(k,328) +.240*rxt(k,2436)*y(k,381) +.452*rxt(k,2440)*y(k,601) +.470*rxt(k,2444)*y(k,702) + &
.490*rxt(k,2449)*y(k,716) +.125*rxt(k,2454)*y(k,245) +.085*rxt(k,2459)*y(k,307) +.140*rxt(k,2464)*y(k,329) + &
.224*rxt(k,2468)*y(k,382) +.432*rxt(k,2472)*y(k,602) +.440*rxt(k,2476)*y(k,703) +.460*rxt(k,2481)*y(k,717) + &
.120*rxt(k,2486)*y(k,246) +.085*rxt(k,2491)*y(k,308) +.140*rxt(k,2496)*y(k,330) +rxt(k,2506)*y(k,510) + &
2.000*rxt(k,2507)*y(k,533) +2.277*rxt(k,2527)*y(k,677) +2.367*rxt(k,2546)*y(k,550) +2.961*rxt(k,2549)*y(k,729) + &
.834*rxt(k,2554)*y(k,636) +1.634*rxt(k,2559)*y(k,484) +2.000*rxt(k,2561)*y(k,535) +rxt(k,2563)*y(k,527) + &
2.388*rxt(k,2593)*y(k,575) +1.752*rxt(k,2597)*y(k,679) +2.529*rxt(k,2600)*y(k,773) +3.492*rxt(k,2672)*y(k,35) + &
2.748*rxt(k,2676)*y(k,20) +2.241*rxt(k,2679)*y(k,45) +2.940*rxt(k,2682)*y(k,112) +1.170*rxt(k,2685)*y(k,635) + &
.752*rxt(k,2688)*y(k,518) +.483*rxt(k,2692)*y(k,199) +.768*rxt(k,2695)*y(k,286) +3.340*rxt(k,2701)*y(k,626) + &
.879*rxt(k,2705)*y(k,465) +3.520*rxt(k,2708)*y(k,592) +rxt(k,2712)*y(k,201) +rxt(k,2713)*y(k,288) + &
1.893*rxt(k,2717)*y(k,420) +2.920*rxt(k,2763)*y(k,164) +2.370*rxt(k,2767)*y(k,247) +1.440*rxt(k,2770)*y(k,407) + &
1.936*rxt(k,2773)*y(k,563) +1.876*rxt(k,2777)*y(k,190) +.584*rxt(k,2814)*y(k,825) +1.292*rxt(k,2849)*y(k,516) + &
.495*rxt(k,2853)*y(k,206) +.704*rxt(k,2856)*y(k,260) +.608*rxt(k,2860)*y(k,287) +2.580*rxt(k,2868)*y(k,513) + &
.472*rxt(k,2877)*y(k,657) +2.874*rxt(k,2881)*y(k,725) +rxt(k,2884)*y(k,150) +1.770*rxt(k,2888)*y(k,422) + &
1.233*rxt(k,2906)*y(k,61) +1.941*rxt(k,2909)*y(k,165) +2.596*rxt(k,2912)*y(k,647) +2.340*rxt(k,2972)*y(k,410) + &
3.140*rxt(k,2975)*y(k,179) +.870*rxt(k,2986)*y(k,742) +.492*rxt(k,2989)*y(k,366) +1.860*rxt(k,2993)*y(k,289) + &
.624*rxt(k,3002)*y(k,730) +.448*rxt(k,3006)*y(k,554) +.396*rxt(k,3035)*y(k,470) +1.392*rxt(k,3039)*y(k,490) + &
.519*rxt(k,3052)*y(k,811) +1.744*rxt(k,3055)*y(k,369) +.728*rxt(k,3059)*y(k,372) +1.074*rxt(k,3087)*y(k,63) + &
1.372*rxt(k,3090)*y(k,121) +1.804*rxt(k,3144)*y(k,722) +.495*rxt(k,3148)*y(k,723) +.660*rxt(k,3151)*y(k,365) + &
.368*rxt(k,3162)*y(k,473) +.684*rxt(k,3166)*y(k,491) +.296*rxt(k,3170)*y(k,489) +.304*rxt(k,3181)*y(k,823) + &
.579*rxt(k,3196)*y(k,65) +.906*rxt(k,3199)*y(k,170) +1.095*rxt(k,3202)*y(k,166) +1.272*rxt(k,3205)*y(k,248) + &
1.098*rxt(k,3208)*y(k,309) +1.248*rxt(k,3211)*y(k,73) +1.225*rxt(k,3221)*y(k,496) +2.636*rxt(k,3293)*y(k,167) + &
.420*rxt(k,3297)*y(k,495) +3.288*rxt(k,3300)*y(k,832) +.680*rxt(k,3304)*y(k,744) +.356*rxt(k,3316)*y(k,468) + &
.264*rxt(k,3320)*y(k,485) +.268*rxt(k,3332)*y(k,821) +.492*rxt(k,3336)*y(k,813) +1.398*rxt(k,3350)*y(k,830) + &
1.653*rxt(k,3363)*y(k,157) +.672*rxt(k,3400)*y(k,51) +.572*rxt(k,3404)*y(k,488) +.256*rxt(k,3416)*y(k,824) + &
.765*rxt(k,3420)*y(k,834) +.224*rxt(k,3423)*y(k,815) +.205*rxt(k,3446)*y(k,436) +1.716*rxt(k,3465)*y(k,632) + &
.780*rxt(k,3484)*y(k,571) +.780*rxt(k,3487)*y(k,54) +1.024*rxt(k,3490)*y(k,237) +.680*rxt(k,3497)*y(k,23) + &
.015*rxt(k,3538)*y(k,529) +.378*rxt(k,3558)*y(k,494) +.252*rxt(k,3561)*y(k,472) +.560*rxt(k,3564)*y(k,462) + &
.252*rxt(k,3572)*y(k,820) +.428*rxt(k,3576)*y(k,835) +1.338*rxt(k,3646)*y(k,151) +1.248*rxt(k,3663)*y(k,818) + &
3.140*rxt(k,3667)*y(k,817) +.316*rxt(k,3672)*y(k,467) +.600*rxt(k,3676)*y(k,466) +.192*rxt(k,3688)*y(k,814) + &
.168*rxt(k,3711)*y(k,822) +.195*rxt(k,3730)*y(k,169) +.395*rxt(k,3774)*y(k,463) +.916*rxt(k,3810)*y(k,608) + &
rxt(k,3820)*y(k,448) +.486*rxt(k,3829)*y(k,481) +.970*rxt(k,3837)*y(k,503) +1.458*rxt(k,3845)*y(k,522) + &
.500*rxt(k,3867)*y(k,483) +.926*rxt(k,3896)*y(k,580) +1.593*rxt(k,3904)*y(k,50) +rxt(k,3965)*y(k,760) + &
rxt(k,3979)*y(k,34) +.639*rxt(k,3990)*y(k,660) +rxt(k,4001)*y(k,497) +.201*rxt(k,4002)*y(k,480) + &
1.616*rxt(k,4012)*y(k,159) +.555*rxt(k,4023)*y(k,353) +.948*rxt(k,4031)*y(k,520) +1.196*rxt(k,4058)*y(k,160) + &
2.280*rxt(k,4083)*y(k,443) +2.280*rxt(k,4087)*y(k,692) +2.280*rxt(k,4091)*y(k,438) +.453*rxt(k,4095)*y(k,521) + &
2.280*rxt(k,4107)*y(k,371) +.468*rxt(k,4117)*y(k,656) +.780*rxt(k,4121)*y(k,426) +.780*rxt(k,4125)*y(k,718) + &
1.400*rxt(k,4129)*y(k,745) +.540*rxt(k,4133)*y(k,658) +.615*rxt(k,4139)*y(k,499) +.804*rxt(k,4151)*y(k,574) + &
.780*rxt(k,4161)*y(k,720) +.705*rxt(k,4169)*y(k,827) +1.400*rxt(k,4175)*y(k,604) +1.305*rxt(k,4177)*y(k,732) + &
1.120*rxt(k,4180)*y(k,733) +2.204*rxt(k,4184)*y(k,734) +1.704*rxt(k,4188)*y(k,735) +1.578*rxt(k,4191)*y(k,736) + &
2.912*rxt(k,4197)*y(k,763) +1.624*rxt(k,4205)*y(k,765))*y(k,705) + (1.566*rxt(k,522)*y(k,348) + &
1.566*rxt(k,529)*y(k,349) +3.324*rxt(k,536)*y(k,350) +.324*rxt(k,545)*y(k,590) +.256*rxt(k,554)*y(k,664) + &
1.600*rxt(k,564)*y(k,582) +.330*rxt(k,587)*y(k,586) +1.500*rxt(k,598)*y(k,343) +.320*rxt(k,1067)*y(k,512) + &
.495*rxt(k,1074)*y(k,739) +.380*rxt(k,1085)*y(k,52) +.159*rxt(k,1095)*y(k,585) +.340*rxt(k,1104)*y(k,385) + &
.340*rxt(k,1112)*y(k,784) +.240*rxt(k,1120)*y(k,33) +.380*rxt(k,1134)*y(k,84) +.380*rxt(k,1146)*y(k,263) + &
.212*rxt(k,1157)*y(k,176) +.204*rxt(k,1167)*y(k,180) +.400*rxt(k,1178)*y(k,389) +.400*rxt(k,1189)*y(k,788) + &
.090*rxt(k,1200)*y(k,457) +.484*rxt(k,1210)*y(k,780) +.380*rxt(k,1221)*y(k,49) +.380*rxt(k,1238)*y(k,72) + &
.380*rxt(k,1249)*y(k,213) +.475*rxt(k,1260)*y(k,264) +.475*rxt(k,1273)*y(k,312) +.212*rxt(k,1284)*y(k,120) + &
.212*rxt(k,1294)*y(k,168) +.212*rxt(k,1305)*y(k,177) +.036*rxt(k,1323)*y(k,181) +.500*rxt(k,1334)*y(k,386) + &
.090*rxt(k,1346)*y(k,391) +.255*rxt(k,1356)*y(k,395) +.255*rxt(k,1368)*y(k,792) +.500*rxt(k,1380)*y(k,798) + &
.500*rxt(k,1392)*y(k,785) +.090*rxt(k,1404)*y(k,789) +.120*rxt(k,1415)*y(k,285) +.036*rxt(k,1427)*y(k,81) + &
.120*rxt(k,1438)*y(k,456) +.490*rxt(k,1450)*y(k,781) +.036*rxt(k,1463)*y(k,82) +.120*rxt(k,1475)*y(k,317) + &
.380*rxt(k,1488)*y(k,71) +.475*rxt(k,1499)*y(k,215) +.475*rxt(k,1512)*y(k,265) +.212*rxt(k,1524)*y(k,136) + &
.212*rxt(k,1536)*y(k,113) +.475*rxt(k,1548)*y(k,208) +.212*rxt(k,1560)*y(k,178) +.212*rxt(k,1571)*y(k,101) + &
.400*rxt(k,1581)*y(k,398) +.045*rxt(k,1593)*y(k,141) +.045*rxt(k,1607)*y(k,182) +.204*rxt(k,1620)*y(k,257) + &
.045*rxt(k,1631)*y(k,290) +.500*rxt(k,1644)*y(k,387) +.120*rxt(k,1656)*y(k,202) +.045*rxt(k,1667)*y(k,269) + &
.255*rxt(k,1680)*y(k,216) +.120*rxt(k,1701)*y(k,392) +.400*rxt(k,1712)*y(k,794) +.500*rxt(k,1724)*y(k,786) + &
.120*rxt(k,1736)*y(k,790) +.255*rxt(k,1747)*y(k,396) +.500*rxt(k,1759)*y(k,799) +.255*rxt(k,1772)*y(k,793) + &
.380*rxt(k,1793)*y(k,83) +.212*rxt(k,1803)*y(k,127) +.212*rxt(k,1816)*y(k,267) +.500*rxt(k,1827)*y(k,787) + &
.045*rxt(k,1840)*y(k,183) +.120*rxt(k,1853)*y(k,400) +.120*rxt(k,1863)*y(k,782) +.120*rxt(k,1875)*y(k,783) + &
.120*rxt(k,1886)*y(k,791) +.120*rxt(k,1897)*y(k,796) +.036*rxt(k,1907)*y(k,128) +.380*rxt(k,1930)*y(k,67) + &
.475*rxt(k,1941)*y(k,291) +.120*rxt(k,1953)*y(k,797) +.380*rxt(k,1964)*y(k,53) +.204*rxt(k,1974)*y(k,219) + &
.120*rxt(k,1986)*y(k,404) +.120*rxt(k,1996)*y(k,795) +.036*rxt(k,2006)*y(k,249) +.054*rxt(k,2019)*y(k,355) + &
.740*rxt(k,2034)*y(k,879) +.045*rxt(k,2049)*y(k,479) +.532*rxt(k,2062)*y(k,771) +.015*rxt(k,2073)*y(k,881) + &
.380*rxt(k,2086)*y(k,55) +.120*rxt(k,2096)*y(k,800) +.380*rxt(k,2106)*y(k,56) +.120*rxt(k,2116)*y(k,801) + &
.380*rxt(k,2126)*y(k,57) +.120*rxt(k,2136)*y(k,802) +.380*rxt(k,2146)*y(k,58) +.120*rxt(k,2156)*y(k,803) + &
.120*rxt(k,2166)*y(k,804) +1.660*rxt(k,2512)*y(k,344) +.260*rxt(k,2533)*y(k,346) +2.505*rxt(k,2567)*y(k,447) + &
.212*rxt(k,2609)*y(k,591) +.160*rxt(k,2621)*y(k,606) +.160*rxt(k,2630)*y(k,837) +.488*rxt(k,2639)*y(k,552) + &
.477*rxt(k,2730)*y(k,595) +.160*rxt(k,2791)*y(k,501) +.212*rxt(k,2799)*y(k,628) +.212*rxt(k,2929)*y(k,514) + &
.716*rxt(k,3010)*y(k,554) +.160*rxt(k,3102)*y(k,668) +.160*rxt(k,3113)*y(k,567) +.212*rxt(k,3230)*y(k,363) + &
.212*rxt(k,3243)*y(k,569) +.045*rxt(k,3437)*y(k,356) +.045*rxt(k,3451)*y(k,436) +.065*rxt(k,3472)*y(k,587) + &
.168*rxt(k,3543)*y(k,529) +.160*rxt(k,3584)*y(k,172) +.120*rxt(k,3752)*y(k,588) +.150*rxt(k,3787)*y(k,652) + &
.150*rxt(k,3800)*y(k,651) +.900*rxt(k,3860)*y(k,556) +.045*rxt(k,3921)*y(k,779) +.045*rxt(k,3932)*y(k,379) + &
.159*rxt(k,4044)*y(k,439))*y(k,696) + (rxt(k,404)*y(k,633) +rxt(k,482)*y(k,537) +1.400*rxt(k,508)*y(k,532) + &
1.374*rxt(k,3814)*y(k,608) +.732*rxt(k,3833)*y(k,481) +1.524*rxt(k,3841)*y(k,503) +1.308*rxt(k,3850)*y(k,522) + &
1.461*rxt(k,3900)*y(k,580) +1.308*rxt(k,3909)*y(k,50) +.807*rxt(k,3996)*y(k,660) +1.041*rxt(k,4019)*y(k,159) + &
.807*rxt(k,4037)*y(k,520) +rxt(k,4319)*y(k,854))*y(k,693) + (rxt(k,403)*y(k,690) +2.000*rxt(k,405)*y(k,633) + &
.500*rxt(k,409)*y(k,768) +.500*rxt(k,414)*y(k,769) +1.800*rxt(k,422)*y(k,620) +2.000*rxt(k,433)*y(k,758) + &
2.000*rxt(k,450)*y(k,370) +rxt(k,466)*y(k,589) +.500*rxt(k,4324)*y(k,854))*y(k,633) + (rxt(k,4317)*y(k,690) + &
rxt(k,4320)*y(k,620) +rxt(k,4321)*y(k,758) +rxt(k,4322)*y(k,370) +rxt(k,4323)*y(k,589) + &
.500*rxt(k,4325)*y(k,768) +.500*rxt(k,4326)*y(k,769))*y(k,854) + (rxt(k,48) +rxt(k,49))*y(k,423) &
 + (.610*rxt(k,72) +rxt(k,224))*y(k,547) + (rxt(k,3864) +rxt(k,3862)*y(k,691))*y(k,556) &
 + (2.400*rxt(k,1069)*y(k,512) +rxt(k,1126)*y(k,33))*y(k,839) +3.000*rxt(k,1)*y(k,59) +3.000*rxt(k,4)*y(k,60) &
 +3.000*rxt(k,7)*y(k,62) +3.000*rxt(k,10)*y(k,64) +3.000*rxt(k,13)*y(k,66) +2.000*rxt(k,16)*y(k,94) &
 +3.000*rxt(k,22)*y(k,192) +2.000*rxt(k,25)*y(k,200) +3.000*rxt(k,30)*y(k,270) +3.200*rxt(k,38)*y(k,344) &
 +2.046*rxt(k,40)*y(k,348) +rxt(k,56)*y(k,445) +2.000*rxt(k,57)*y(k,447) +2.000*rxt(k,63)*y(k,530) &
 +2.000*rxt(k,64)*y(k,531) +2.000*rxt(k,66)*y(k,532) +2.000*rxt(k,68)*y(k,537) +rxt(k,70)*y(k,545) +rxt(k,74) &
*y(k,550) +2.000*rxt(k,76)*y(k,552) +rxt(k,3863)*y(k,555) +3.000*rxt(k,79)*y(k,557) +2.466*rxt(k,84)*y(k,582) &
 +2.000*rxt(k,86)*y(k,590) +rxt(k,92)*y(k,631) +rxt(k,95)*y(k,645) +3.000*rxt(k,102)*y(k,653) +2.000*rxt(k,141) &
*y(k,737) +.568*rxt(k,146)*y(k,755) +2.000*rxt(k,147)*y(k,756) +2.000*rxt(k,149)*y(k,757) +rxt(k,151)*y(k,760) &
 +1.720*rxt(k,156)*y(k,761) +1.212*rxt(k,157)*y(k,762) +rxt(k,159)*y(k,763) +.064*rxt(k,160)*y(k,764) &
 +1.323*rxt(k,164)*y(k,765) +rxt(k,173)*y(k,770)
         loss(k,108) = (rxt(k,400)* y(k,705) + rxt(k,73) + het_rates(k,6))* y(k,549)
         prod(k,108) = (rxt(k,226)*y(k,548) +rxt(k,227)*y(k,548))*y(k,548)
         loss(k,206) = ((rxt(k,2546) +rxt(k,2547) +rxt(k,2548))* y(k,705) + rxt(k,74))* y(k,550)
         prod(k,206) = 0.
         loss(k,109) = ( + rxt(k,75))* y(k,551)
         prod(k,109) =rxt(k,3824)*y(k,578)*y(k,548)
         loss(k,796) = ((rxt(k,2641) +rxt(k,2642) +rxt(k,2643) +rxt(k,2644))* y(k,693) + (rxt(k,2639) +rxt(k,2640))* y(k,696) &
 + (rxt(k,2636) +rxt(k,2637) +rxt(k,2638))* y(k,705) +rxt(k,2645)* y(k,839) + rxt(k,76) + rxt(k,77))* y(k,552)
         prod(k,796) = 0.
         loss(k,394) = (rxt(k,392)* y(k,705) + rxt(k,78) + rxt(k,4519) + het_rates(k,3))* y(k,553)
         prod(k,394) =rxt(k,219)*y(k,705)*y(k,690) +rxt(k,117)*y(k,694) +rxt(k,4517)*y(k,884) +.700*rxt(k,4521)*y(k,910)
         loss(k,860) = ((rxt(k,3014) +rxt(k,3015) +rxt(k,3016) +rxt(k,3017))* y(k,693) + (rxt(k,3010) +rxt(k,3011) +rxt(k,3012) + &
rxt(k,3013))* y(k,696) + (rxt(k,3006) +rxt(k,3007) +rxt(k,3008) +rxt(k,3009))* y(k,705) +rxt(k,3018)* y(k,839)) &
* y(k,554)
         prod(k,860) = 0.
         loss(k,701) = (rxt(k,3859)* y(k,691) +rxt(k,3858)* y(k,696) + rxt(k,3863))* y(k,555)
         prod(k,701) =1.100*rxt(k,3860)*y(k,696)*y(k,556) +rxt(k,3857)*y(k,705)*y(k,650)
         loss(k,700) = (rxt(k,3862)* y(k,691) + (rxt(k,3860) +rxt(k,3861))* y(k,696) + rxt(k,3864))* y(k,556)
         prod(k,700) = (rxt(k,3858)*y(k,696) +rxt(k,3859)*y(k,691))*y(k,555)
         loss(k,612) = ((rxt(k,3461) +rxt(k,3462) +rxt(k,3463) +rxt(k,3464))* y(k,705) + rxt(k,79) + rxt(k,80) + rxt(k,81)) &
* y(k,557)
         prod(k,612) = 0.
         loss(k,314) = ((rxt(k,809) +rxt(k,810) +rxt(k,811))* y(k,705))* y(k,558)
         prod(k,314) = 0.
         loss(k,723) = (rxt(k,3823)* y(k,548) +rxt(k,330)* y(k,691) +rxt(k,3822)* y(k,696))* y(k,559)
         prod(k,723) = (rxt(k,55) +rxt(k,3821)*y(k,705))*y(k,433) + (rxt(k,3825)*y(k,690) +1.700*rxt(k,3826)*y(k,578))*y(k,578) &
 +rxt(k,70)*y(k,545) +rxt(k,75)*y(k,551) +rxt(k,82)*y(k,577)
         loss(k,76) = (rxt(k,3827)* y(k,578))* y(k,560)
         prod(k,76) =.150*rxt(k,3826)*y(k,578)*y(k,578)
         loss(k,77) = (rxt(k,3828)* y(k,578))* y(k,561)
         prod(k,77) =rxt(k,3827)*y(k,578)*y(k,560)
         loss(k,473) = ((rxt(k,3132) +rxt(k,3133) +rxt(k,3134) +rxt(k,3135))* y(k,705))* y(k,562)
         prod(k,473) = 0.
         loss(k,474) = ((rxt(k,2773) +rxt(k,2774) +rxt(k,2775) +rxt(k,2776))* y(k,705))* y(k,563)
         prod(k,474) = 0.
         loss(k,475) = ((rxt(k,3642) +rxt(k,3643) +rxt(k,3644) +rxt(k,3645))* y(k,705))* y(k,564)
         prod(k,475) = 0.
         loss(k,315) = ((rxt(k,3215) +rxt(k,3216) +rxt(k,3217))* y(k,705))* y(k,565)
         prod(k,315) = 0.
         loss(k,551) = ((rxt(k,2941) +rxt(k,2942) +rxt(k,2943) +rxt(k,2944))* y(k,705))* y(k,566)
         prod(k,551) = 0.
         loss(k,904) = ((rxt(k,3115) +rxt(k,3116) +rxt(k,3117) +rxt(k,3118) +rxt(k,3119))* y(k,693) + (rxt(k,3113) +rxt(k,3114)) &
* y(k,696) + (rxt(k,3109) +rxt(k,3110) +rxt(k,3111) +rxt(k,3112))* y(k,705) +rxt(k,3120)* y(k,839))* y(k,567)
         prod(k,904) = 0.
         loss(k,654) = ((rxt(k,3268) +rxt(k,3269) +rxt(k,3270) +rxt(k,3271) +rxt(k,3272))* y(k,705))* y(k,568)
         prod(k,654) = 0.
         loss(k,896) = ((rxt(k,3247) +rxt(k,3248) +rxt(k,3249) +rxt(k,3250))* y(k,693) + (rxt(k,3243) +rxt(k,3244) +rxt(k,3245) + &
rxt(k,3246))* y(k,696) + (rxt(k,3239) +rxt(k,3240) +rxt(k,3241) +rxt(k,3242))* y(k,705) +rxt(k,3251)* y(k,839)) &
* y(k,569)
         prod(k,896) = 0.
         loss(k,316) = ((rxt(k,2663) +rxt(k,2664) +rxt(k,2665))* y(k,705))* y(k,570)
         prod(k,316) = 0.
         loss(k,398) = ((rxt(k,3484) +rxt(k,3485) +rxt(k,3486))* y(k,705))* y(k,571)
         prod(k,398) = 0.
         loss(k,628) = ((rxt(k,2207) +rxt(k,2208) +rxt(k,2209) +rxt(k,2210))* y(k,705))* y(k,572)
         prod(k,628) = 0.
         loss(k,270) = ((rxt(k,303) +rxt(k,304) +rxt(k,305))* y(k,705))* y(k,573)
         prod(k,270) = 0.
         loss(k,760) = (rxt(k,4160)* y(k,546) + (rxt(k,4157) +rxt(k,4158) +rxt(k,4159))* y(k,693) + (rxt(k,4154) +rxt(k,4155) + &
rxt(k,4156))* y(k,696) + (rxt(k,4151) +rxt(k,4152) +rxt(k,4153))* y(k,705))* y(k,574)
         prod(k,760) = 0.
         loss(k,478) = ((rxt(k,2593) +rxt(k,2594) +rxt(k,2595) +rxt(k,2596))* y(k,705))* y(k,575)
         prod(k,478) = 0.
         loss(k,553) = ((rxt(k,3390) +rxt(k,3391) +rxt(k,3392) +rxt(k,3393))* y(k,705))* y(k,576)
         prod(k,553) = 0.
         loss(k,110) = ( + rxt(k,82))* y(k,577)
         prod(k,110) =rxt(k,330)*y(k,691)*y(k,559)
         loss(k,717) = (rxt(k,3824)* y(k,548) +rxt(k,3827)* y(k,560) +rxt(k,3828)* y(k,561) + 2.*rxt(k,3826)* y(k,578) &
 +rxt(k,3825)* y(k,690))* y(k,578)
         prod(k,717) =rxt(k,3822)*y(k,696)*y(k,559)
         loss(k,400) = ((rxt(k,2822) +rxt(k,2823) +rxt(k,2824) +rxt(k,2825))* y(k,705))* y(k,579)
         prod(k,400) = 0.
         loss(k,749) = (rxt(k,3903)* y(k,546) + (rxt(k,3900) +rxt(k,3901) +rxt(k,3902))* y(k,693) + (rxt(k,3898) +rxt(k,3899)) &
* y(k,696) + (rxt(k,3896) +rxt(k,3897))* y(k,705))* y(k,580)
         prod(k,749) = 0.
         loss(k,317) = ((rxt(k,616) +rxt(k,617) +rxt(k,618))* y(k,705))* y(k,581)
         prod(k,317) = 0.
         loss(k,929) = ((rxt(k,568) +rxt(k,569) +rxt(k,570) +rxt(k,571))* y(k,693) + (rxt(k,564) +rxt(k,565) +rxt(k,566) + &
rxt(k,567))* y(k,696) + (rxt(k,560) +rxt(k,561) +rxt(k,562) +rxt(k,563))* y(k,705) + rxt(k,83) + rxt(k,84)) &
* y(k,582)
         prod(k,929) = (rxt(k,4327)*y(k,690) +rxt(k,4329)*y(k,693) +rxt(k,4330)*y(k,620) +rxt(k,4331)*y(k,758) + &
rxt(k,4332)*y(k,370) +rxt(k,4333)*y(k,589) +.500*rxt(k,4334)*y(k,633) +.500*rxt(k,4335)*y(k,768) + &
.500*rxt(k,4336)*y(k,769))*y(k,855) + (.018*rxt(k,539)*y(k,350) +.750*rxt(k,589)*y(k,586) + &
1.852*rxt(k,1212)*y(k,780))*y(k,696) + (.695*rxt(k,503)*y(k,756) +.015*rxt(k,3541)*y(k,529))*y(k,705)
         loss(k,479) = ((rxt(k,2659) +rxt(k,2660) +rxt(k,2661) +rxt(k,2662))* y(k,705))* y(k,583)
         prod(k,479) = 0.
         loss(k,480) = ((rxt(k,2922) +rxt(k,2923) +rxt(k,2924) +rxt(k,2925))* y(k,705))* y(k,584)
         prod(k,480) = 0.
         loss(k,783) = ((rxt(k,1098) +rxt(k,1099) +rxt(k,1100))* y(k,693) + (rxt(k,1095) +rxt(k,1096) +rxt(k,1097))* y(k,696) &
 + (rxt(k,1093) +rxt(k,1094))* y(k,705) +rxt(k,1101)* y(k,839))* y(k,585)
         prod(k,783) = 0.
         loss(k,818) = ((rxt(k,592) +rxt(k,593) +rxt(k,594))* y(k,693) + (rxt(k,587) +rxt(k,588) +rxt(k,589) +rxt(k,590) + &
rxt(k,591))* y(k,696) + (rxt(k,584) +rxt(k,585) +rxt(k,586))* y(k,705) + (rxt(k,595) +rxt(k,596) +rxt(k,597)) &
* y(k,839))* y(k,586)
         prod(k,818) = 0.
         loss(k,845) = ((rxt(k,3477) +rxt(k,3478) +rxt(k,3479))* y(k,693) + (rxt(k,3472) +rxt(k,3473) +rxt(k,3474) +rxt(k,3475) + &
rxt(k,3476))* y(k,696) + (rxt(k,3469) +rxt(k,3470) +rxt(k,3471))* y(k,705) +rxt(k,3480)* y(k,839))* y(k,587)
         prod(k,845) = 0.
         loss(k,798) = ((rxt(k,3756) +rxt(k,3757))* y(k,693) + (rxt(k,3752) +rxt(k,3753) +rxt(k,3754) +rxt(k,3755))* y(k,696) &
 + (rxt(k,3749) +rxt(k,3750) +rxt(k,3751))* y(k,705) +rxt(k,3758)* y(k,839))* y(k,588)
         prod(k,798) = 0.
         loss(k,937) = ((rxt(k,473) +rxt(k,474))* y(k,370) + (rxt(k,3962) +rxt(k,3963))* y(k,427) + (rxt(k,462) +rxt(k,463)) &
* y(k,548) + 2.*rxt(k,475)* y(k,589) + (rxt(k,469) +rxt(k,470))* y(k,620) +rxt(k,466)* y(k,633) +rxt(k,461) &
* y(k,690) +rxt(k,235)* y(k,691) + (rxt(k,464) +rxt(k,465))* y(k,693) + (rxt(k,471) +rxt(k,472))* y(k,758) &
 +rxt(k,467)* y(k,768) +rxt(k,468)* y(k,769) + het_rates(k,37))* y(k,589)
         prod(k,937) = (.868*rxt(k,518)*y(k,348) +.868*rxt(k,525)*y(k,349) +.824*rxt(k,532)*y(k,350) +1.500*rxt(k,542)*y(k,590) + &
1.156*rxt(k,560)*y(k,582) +.087*rxt(k,1229)*y(k,28) +2.250*rxt(k,2509)*y(k,344) +1.263*rxt(k,2564)*y(k,447) + &
1.140*rxt(k,2636)*y(k,552))*y(k,705) + (rxt(k,4337)*y(k,690) +rxt(k,4339)*y(k,693) +rxt(k,4340)*y(k,620) + &
rxt(k,4341)*y(k,758) +rxt(k,4342)*y(k,370) +rxt(k,4343)*y(k,589) +.500*rxt(k,4344)*y(k,633) + &
.500*rxt(k,4345)*y(k,768) +.500*rxt(k,4346)*y(k,769))*y(k,856) + (rxt(k,548)*y(k,590) +.600*rxt(k,568)*y(k,582) + &
2.901*rxt(k,2514)*y(k,344) +2.092*rxt(k,2570)*y(k,447) +2.000*rxt(k,2641)*y(k,552))*y(k,693) + (1.200*rxt(k,88) + &
rxt(k,458))*y(k,594) +rxt(k,38)*y(k,344) +rxt(k,40)*y(k,348) +rxt(k,86)*y(k,590) +.800*rxt(k,111)*y(k,664)
         loss(k,943) = ((rxt(k,548) +rxt(k,549))* y(k,693) + (rxt(k,545) +rxt(k,546) +rxt(k,547))* y(k,696) + (rxt(k,542) + &
rxt(k,543) +rxt(k,544))* y(k,705) + (rxt(k,550) +rxt(k,2575))* y(k,839) + rxt(k,85) + rxt(k,86) + rxt(k,87)) &
* y(k,590)
         prod(k,943) = (rxt(k,4347)*y(k,690) +rxt(k,4349)*y(k,693) +rxt(k,4350)*y(k,620) +rxt(k,4351)*y(k,758) + &
rxt(k,4352)*y(k,370) +rxt(k,4353)*y(k,589) +.500*rxt(k,4354)*y(k,633) +.500*rxt(k,4355)*y(k,768) + &
.500*rxt(k,4356)*y(k,769))*y(k,857) + (.018*rxt(k,539)*y(k,350) +1.950*rxt(k,589)*y(k,586) + &
1.500*rxt(k,1121)*y(k,33) +rxt(k,1212)*y(k,780))*y(k,696)
         loss(k,827) = ((rxt(k,2613) +rxt(k,2614) +rxt(k,2615))* y(k,693) + (rxt(k,2609) +rxt(k,2610) +rxt(k,2611) +rxt(k,2612)) &
* y(k,696) + (rxt(k,2606) +rxt(k,2607) +rxt(k,2608))* y(k,705) +rxt(k,2616)* y(k,839))* y(k,591)
         prod(k,827) = 0.
         loss(k,401) = ((rxt(k,2708) +rxt(k,2709) +rxt(k,2710) +rxt(k,2711))* y(k,705))* y(k,592)
         prod(k,401) = 0.
         loss(k,645) = ((rxt(k,3280) +rxt(k,3281) +rxt(k,3282) +rxt(k,3283))* y(k,705))* y(k,593)
         prod(k,645) = 0.
         loss(k,646) = (rxt(k,460)* y(k,691) +rxt(k,459)* y(k,705) + rxt(k,88) + rxt(k,89) + rxt(k,458) + het_rates(k,15)) &
* y(k,594)
         prod(k,646) =rxt(k,235)*y(k,691)*y(k,589)
         loss(k,832) = ((rxt(k,2733) +rxt(k,2734) +rxt(k,2735))* y(k,693) + (rxt(k,2730) +rxt(k,2731) +rxt(k,2732))* y(k,696) &
 + (rxt(k,2727) +rxt(k,2728) +rxt(k,2729))* y(k,705) +rxt(k,2736)* y(k,839))* y(k,595)
         prod(k,832) = 0.
         loss(k,671) = ((rxt(k,2256) +rxt(k,2257) +rxt(k,2258) +rxt(k,2259) +rxt(k,2260))* y(k,705))* y(k,596)
         prod(k,671) = 0.
         loss(k,673) = ((rxt(k,2302) +rxt(k,2303) +rxt(k,2304) +rxt(k,2305) +rxt(k,2306))* y(k,705))* y(k,597)
         prod(k,673) = 0.
         loss(k,674) = ((rxt(k,2339) +rxt(k,2340) +rxt(k,2341) +rxt(k,2342) +rxt(k,2343))* y(k,705))* y(k,598)
         prod(k,674) = 0.
         loss(k,647) = ((rxt(k,2376) +rxt(k,2377) +rxt(k,2378) +rxt(k,2379))* y(k,705))* y(k,599)
         prod(k,647) = 0.
         loss(k,648) = ((rxt(k,2408) +rxt(k,2409) +rxt(k,2410) +rxt(k,2411))* y(k,705))* y(k,600)
         prod(k,648) = 0.
         loss(k,649) = ((rxt(k,2440) +rxt(k,2441) +rxt(k,2442) +rxt(k,2443))* y(k,705))* y(k,601)
         prod(k,649) = 0.
         loss(k,650) = ((rxt(k,2472) +rxt(k,2473) +rxt(k,2474) +rxt(k,2475))* y(k,705))* y(k,602)
         prod(k,650) = 0.
         loss(k,402) = ((rxt(k,2864) +rxt(k,2865) +rxt(k,2866) +rxt(k,2867))* y(k,705))* y(k,603)
         prod(k,402) = 0.
         loss(k,136) = ((rxt(k,4175) +rxt(k,4176))* y(k,705))* y(k,604)
         prod(k,136) = 0.
         loss(k,159) = ((rxt(k,2544) +rxt(k,2545))* y(k,705))* y(k,605)
         prod(k,159) = 0.
         loss(k,850) = ((rxt(k,2623) +rxt(k,2624) +rxt(k,2625))* y(k,693) + (rxt(k,2621) +rxt(k,2622))* y(k,696) + (rxt(k,2617) + &
rxt(k,2618) +rxt(k,2619) +rxt(k,2620))* y(k,705) +rxt(k,2626)* y(k,839))* y(k,606)
         prod(k,850) = 0.
         loss(k,111) = (rxt(k,3886)* y(k,705))* y(k,607)
         prod(k,111) = 0.
         loss(k,753) = (rxt(k,3817)* y(k,546) + (rxt(k,3814) +rxt(k,3815) +rxt(k,3816))* y(k,693) + (rxt(k,3812) +rxt(k,3813)) &
* y(k,696) + (rxt(k,3810) +rxt(k,3811))* y(k,705))* y(k,608)
         prod(k,753) = 0.
         loss(k,112) = (rxt(k,328)* y(k,705))* y(k,609)
         prod(k,112) = 0.
         loss(k,652) = ((rxt(k,2826) +rxt(k,2827) +rxt(k,2828) +rxt(k,2829))* y(k,705))* y(k,610)
         prod(k,652) = 0.
         loss(k,207) = ((rxt(k,3639) +rxt(k,3640) +rxt(k,3641))* y(k,705))* y(k,611)
         prod(k,207) = 0.
         loss(k,208) = ((rxt(k,3660) +rxt(k,3661) +rxt(k,3662))* y(k,705))* y(k,612)
         prod(k,208) = 0.
         loss(k,209) = ((rxt(k,3727) +rxt(k,3728) +rxt(k,3729))* y(k,705))* y(k,613)
         prod(k,209) = 0.
         loss(k,210) = ((rxt(k,3759) +rxt(k,3760) +rxt(k,3761))* y(k,705))* y(k,614)
         prod(k,210) = 0.
         loss(k,484) = ((rxt(k,2968) +rxt(k,2969) +rxt(k,2970) +rxt(k,2971))* y(k,705))* y(k,615)
         prod(k,484) = 0.
         loss(k,485) = ((rxt(k,3140) +rxt(k,3141) +rxt(k,3142) +rxt(k,3143))* y(k,705))* y(k,616)
         prod(k,485) = 0.
         loss(k,211) = ((rxt(k,3290) +rxt(k,3291) +rxt(k,3292))* y(k,705))* y(k,617)
         prod(k,211) = 0.
         loss(k,212) = ((rxt(k,3397) +rxt(k,3398) +rxt(k,3399))* y(k,705))* y(k,618)
         prod(k,212) = 0.
         loss(k,213) = ((rxt(k,3555) +rxt(k,3556) +rxt(k,3557))* y(k,705))* y(k,619)
         prod(k,213) = 0.
         loss(k,941) = (rxt(k,454)* y(k,370) + (rxt(k,3956) +rxt(k,3957))* y(k,427) + (rxt(k,418) +rxt(k,419))* y(k,548) &
 + (rxt(k,469) +rxt(k,470))* y(k,589) + 2.*rxt(k,425)* y(k,620) + (rxt(k,421) +rxt(k,422))* y(k,633) +rxt(k,417) &
* y(k,690) +rxt(k,232)* y(k,691) +rxt(k,420)* y(k,693) + (rxt(k,439) +rxt(k,440))* y(k,758) +rxt(k,423)* y(k,768) &
 +rxt(k,424)* y(k,769) + het_rates(k,34))* y(k,620)
         prod(k,941) = (rxt(k,461)*y(k,690) +.880*rxt(k,462)*y(k,548) +2.000*rxt(k,464)*y(k,693) +rxt(k,466)*y(k,633) + &
rxt(k,467)*y(k,768) +rxt(k,468)*y(k,769) +2.000*rxt(k,469)*y(k,620) +2.000*rxt(k,471)*y(k,758) + &
2.000*rxt(k,473)*y(k,370) +2.000*rxt(k,475)*y(k,589) +2.000*rxt(k,3962)*y(k,427) +rxt(k,4363)*y(k,858))*y(k,589) &
 + (rxt(k,4357)*y(k,690) +rxt(k,4359)*y(k,693) +rxt(k,4360)*y(k,620) +rxt(k,4361)*y(k,758) + &
rxt(k,4362)*y(k,370) +.500*rxt(k,4364)*y(k,633) +.500*rxt(k,4365)*y(k,768) +.500*rxt(k,4366)*y(k,769))*y(k,858) &
 + (rxt(k,483)*y(k,423) +rxt(k,510)*y(k,645) +rxt(k,2504)*y(k,530) +.531*rxt(k,2546)*y(k,550) + &
.330*rxt(k,3886)*y(k,607) +.330*rxt(k,3981)*y(k,163))*y(k,705) + (rxt(k,484)*y(k,423) +rxt(k,511)*y(k,645) + &
rxt(k,2505)*y(k,530))*y(k,693) + (1.200*rxt(k,121) +rxt(k,233))*y(k,707) +2.000*rxt(k,18)*y(k,135) &
 +3.000*rxt(k,27)*y(k,266) +2.000*rxt(k,33)*y(k,332) +.620*rxt(k,35)*y(k,341) +.610*rxt(k,40)*y(k,348) &
 +.078*rxt(k,536)*y(k,696)*y(k,350) +2.000*rxt(k,42)*y(k,357) +2.000*rxt(k,44)*y(k,412) +2.000*rxt(k,46)*y(k,414) &
 +rxt(k,74)*y(k,550) +.934*rxt(k,84)*y(k,582) +rxt(k,86)*y(k,590) +1.200*rxt(k,89)*y(k,594) +2.000*rxt(k,90) &
*y(k,625) +2.000*rxt(k,93)*y(k,634) +rxt(k,95)*y(k,645) +3.000*rxt(k,96)*y(k,646) +2.000*rxt(k,99)*y(k,649) &
 +2.000*rxt(k,105)*y(k,655) +2.000*rxt(k,107)*y(k,659) +2.000*rxt(k,109)*y(k,663) +1.200*rxt(k,128)*y(k,731) &
 +2.000*rxt(k,129)*y(k,732) +3.000*rxt(k,131)*y(k,733)
         loss(k,320) = ((rxt(k,619) +rxt(k,620) +rxt(k,621))* y(k,705))* y(k,621)
         prod(k,320) = 0.
         loss(k,271) = ((rxt(k,661) +rxt(k,662) +rxt(k,663))* y(k,705))* y(k,622)
         prod(k,271) = 0.
         loss(k,148) = ((rxt(k,2502) +rxt(k,2503))* y(k,705))* y(k,623)
         prod(k,148) = 0.
         loss(k,613) = ((rxt(k,2830) +rxt(k,2831) +rxt(k,2832) +rxt(k,2833))* y(k,705))* y(k,624)
         prod(k,613) = 0.
         loss(k,921) = ((rxt(k,306) +rxt(k,307) +rxt(k,308))* y(k,705) + rxt(k,90) + rxt(k,91))* y(k,625)
         prod(k,921) = (.840*rxt(k,565)*y(k,582) +.052*rxt(k,1136)*y(k,84) +.052*rxt(k,1148)*y(k,263) + &
1.332*rxt(k,1160)*y(k,176) +.052*rxt(k,1240)*y(k,72) +.052*rxt(k,1251)*y(k,213) +.065*rxt(k,1262)*y(k,264) + &
.065*rxt(k,1276)*y(k,312) +1.332*rxt(k,1287)*y(k,120) +1.332*rxt(k,1296)*y(k,168) +1.332*rxt(k,1308)*y(k,177) + &
.065*rxt(k,1337)*y(k,386) +1.500*rxt(k,1359)*y(k,395) +1.500*rxt(k,1371)*y(k,792) +.065*rxt(k,1383)*y(k,798) + &
.065*rxt(k,1395)*y(k,785) +.032*rxt(k,1430)*y(k,81) +.052*rxt(k,1490)*y(k,71) +.065*rxt(k,1502)*y(k,215) + &
.065*rxt(k,1515)*y(k,265) +.065*rxt(k,1551)*y(k,208) +1.332*rxt(k,1573)*y(k,101) +.052*rxt(k,1583)*y(k,398) + &
.040*rxt(k,1596)*y(k,141) +.040*rxt(k,1610)*y(k,182) +1.200*rxt(k,1622)*y(k,257) +1.500*rxt(k,1634)*y(k,290) + &
.065*rxt(k,1647)*y(k,387) +.052*rxt(k,1658)*y(k,202) +1.500*rxt(k,1670)*y(k,269) +1.500*rxt(k,1683)*y(k,216) + &
1.500*rxt(k,1693)*y(k,123) +.052*rxt(k,1703)*y(k,392) +.052*rxt(k,1714)*y(k,794) +.065*rxt(k,1727)*y(k,786) + &
.052*rxt(k,1738)*y(k,790) +1.500*rxt(k,1750)*y(k,396) +.065*rxt(k,1762)*y(k,799) +1.500*rxt(k,1775)*y(k,793) + &
1.332*rxt(k,1806)*y(k,127) +.065*rxt(k,1830)*y(k,787) +.040*rxt(k,1843)*y(k,183) +.100*rxt(k,1855)*y(k,400) + &
.052*rxt(k,1865)*y(k,782) +.100*rxt(k,1877)*y(k,783) +.052*rxt(k,1888)*y(k,791) +.100*rxt(k,1899)*y(k,796) + &
.032*rxt(k,1910)*y(k,128) +1.500*rxt(k,1921)*y(k,124) +.065*rxt(k,1944)*y(k,291) +.100*rxt(k,1955)*y(k,797) + &
.100*rxt(k,1988)*y(k,404) +.100*rxt(k,1998)*y(k,795) +.032*rxt(k,2009)*y(k,249) +.048*rxt(k,2022)*y(k,355) + &
.100*rxt(k,2098)*y(k,800) +.052*rxt(k,2118)*y(k,801) +.052*rxt(k,2138)*y(k,802) +.052*rxt(k,2158)*y(k,803) + &
.052*rxt(k,2168)*y(k,804) +.024*rxt(k,3547)*y(k,529) +.052*rxt(k,3754)*y(k,588) +.030*rxt(k,3802)*y(k,651)) &
*y(k,696) + (.550*rxt(k,559)*y(k,664) +.550*rxt(k,1079)*y(k,739) +.550*rxt(k,1092)*y(k,52) + &
.600*rxt(k,1101)*y(k,585) +rxt(k,1109)*y(k,385) +rxt(k,1114)*y(k,784) +.550*rxt(k,1141)*y(k,84) + &
.550*rxt(k,1153)*y(k,263) +.600*rxt(k,1164)*y(k,176) +rxt(k,1174)*y(k,180) +rxt(k,1185)*y(k,389) + &
rxt(k,1196)*y(k,788) +.240*rxt(k,1206)*y(k,457) +.550*rxt(k,1244)*y(k,72) +.550*rxt(k,1255)*y(k,213) + &
.550*rxt(k,1280)*y(k,312) +.600*rxt(k,1290)*y(k,120) +.600*rxt(k,1301)*y(k,168) +.600*rxt(k,1312)*y(k,177) + &
rxt(k,1319)*y(k,122) +rxt(k,1330)*y(k,181) +.760*rxt(k,1342)*y(k,386) +.760*rxt(k,1352)*y(k,391) + &
.600*rxt(k,1364)*y(k,395) +.600*rxt(k,1376)*y(k,792) +.880*rxt(k,1388)*y(k,798) +.760*rxt(k,1400)*y(k,785) + &
.760*rxt(k,1410)*y(k,789) +.600*rxt(k,1532)*y(k,136) +.600*rxt(k,1543)*y(k,113) +.550*rxt(k,1556)*y(k,208) + &
.600*rxt(k,1567)*y(k,178) +.600*rxt(k,1577)*y(k,101) +rxt(k,1589)*y(k,398) +.600*rxt(k,1602)*y(k,141) + &
.600*rxt(k,1616)*y(k,182) +.600*rxt(k,1627)*y(k,257) +.600*rxt(k,1640)*y(k,290) +.600*rxt(k,1676)*y(k,269) + &
.600*rxt(k,1688)*y(k,216) +rxt(k,1697)*y(k,123) +rxt(k,1720)*y(k,794) +.600*rxt(k,1755)*y(k,396) + &
.600*rxt(k,1780)*y(k,793) +.600*rxt(k,1811)*y(k,127) +.600*rxt(k,1849)*y(k,183) +.880*rxt(k,1871)*y(k,782) + &
rxt(k,1914)*y(k,128) +rxt(k,1926)*y(k,124) +.550*rxt(k,2626)*y(k,606) +.550*rxt(k,2635)*y(k,837) + &
.550*rxt(k,2736)*y(k,595) +.550*rxt(k,2796)*y(k,501) +.600*rxt(k,2806)*y(k,628) +.600*rxt(k,2936)*y(k,514) + &
.550*rxt(k,3108)*y(k,668) +.550*rxt(k,3120)*y(k,567) +.600*rxt(k,3238)*y(k,363) +.600*rxt(k,3251)*y(k,569) + &
.880*rxt(k,3926)*y(k,779) +.880*rxt(k,3937)*y(k,379) +.600*rxt(k,4049)*y(k,439))*y(k,839) &
 + (2.157*rxt(k,317)*y(k,662) +.060*rxt(k,580)*y(k,761) +1.938*rxt(k,2551)*y(k,729) +2.529*rxt(k,2602)*y(k,773) + &
1.923*rxt(k,2678)*y(k,20) +1.893*rxt(k,2719)*y(k,420) +2.920*rxt(k,2765)*y(k,164) +2.370*rxt(k,2769)*y(k,247) + &
3.140*rxt(k,2977)*y(k,179) +.104*rxt(k,3693)*y(k,812) +.183*rxt(k,4195)*y(k,762))*y(k,705) &
 + (rxt(k,4367)*y(k,690) +rxt(k,4369)*y(k,693) +rxt(k,4370)*y(k,620) +rxt(k,4371)*y(k,758) + &
rxt(k,4372)*y(k,370) +rxt(k,4373)*y(k,589) +.500*rxt(k,4374)*y(k,633) +.500*rxt(k,4375)*y(k,768) + &
.500*rxt(k,4376)*y(k,769))*y(k,859) +.466*rxt(k,83)*y(k,582) +.620*rxt(k,152)*y(k,761) +1.212*rxt(k,158)*y(k,762)
         loss(k,614) = ((rxt(k,2701) +rxt(k,2702) +rxt(k,2703) +rxt(k,2704))* y(k,705))* y(k,626)
         prod(k,614) = 0.
         loss(k,214) = ((rxt(k,3708) +rxt(k,3709) +rxt(k,3710))* y(k,705))* y(k,627)
         prod(k,214) = 0.
         loss(k,821) = ((rxt(k,2803) +rxt(k,2804) +rxt(k,2805))* y(k,693) + (rxt(k,2799) +rxt(k,2800) +rxt(k,2801) +rxt(k,2802)) &
* y(k,696) + (rxt(k,2797) +rxt(k,2798))* y(k,705) +rxt(k,2806)* y(k,839))* y(k,628)
         prod(k,821) = 0.
         loss(k,215) = ((rxt(k,3746) +rxt(k,3747) +rxt(k,3748))* y(k,705))* y(k,629)
         prod(k,215) = 0.
         loss(k,559) = ((rxt(k,2331) +rxt(k,2332) +rxt(k,2333) +rxt(k,2334))* y(k,705))* y(k,630)
         prod(k,559) = 0.
         loss(k,113) = (rxt(k,3818)* y(k,705) + rxt(k,92))* y(k,631)
         prod(k,113) = 0.
         loss(k,489) = ((rxt(k,3465) +rxt(k,3466) +rxt(k,3467) +rxt(k,3468))* y(k,705))* y(k,632)
         prod(k,489) = 0.
         loss(k,935) = ((rxt(k,450) +rxt(k,451))* y(k,370) +rxt(k,3953)* y(k,427) + (rxt(k,229) +rxt(k,230))* y(k,548) +rxt(k,466) &
* y(k,589) + (rxt(k,421) +rxt(k,422))* y(k,620) + 2.*(rxt(k,231) +rxt(k,405))* y(k,633) +rxt(k,403)* y(k,690) &
 +rxt(k,404)* y(k,693) + (rxt(k,433) +rxt(k,434))* y(k,758) +rxt(k,409)* y(k,768) +rxt(k,414)* y(k,769) &
 + het_rates(k,31))* y(k,633)
         prod(k,935) = (1.065*rxt(k,1074)*y(k,739) +1.420*rxt(k,1104)*y(k,385) +1.420*rxt(k,1112)*y(k,784) + &
.852*rxt(k,1167)*y(k,180) +1.420*rxt(k,1178)*y(k,389) +1.420*rxt(k,1189)*y(k,788) +.712*rxt(k,1210)*y(k,780) + &
1.775*rxt(k,1334)*y(k,386) +1.065*rxt(k,1356)*y(k,395) +1.065*rxt(k,1368)*y(k,792) +1.775*rxt(k,1380)*y(k,798) + &
1.775*rxt(k,1392)*y(k,785) +.890*rxt(k,1450)*y(k,781) +1.420*rxt(k,1581)*y(k,398) +.852*rxt(k,1620)*y(k,257) + &
1.775*rxt(k,1644)*y(k,387) +1.065*rxt(k,1680)*y(k,216) +1.420*rxt(k,1712)*y(k,794) +1.775*rxt(k,1724)*y(k,786) + &
1.065*rxt(k,1747)*y(k,396) +1.775*rxt(k,1759)*y(k,799) +1.065*rxt(k,1772)*y(k,793) +1.775*rxt(k,1827)*y(k,787) + &
.852*rxt(k,1974)*y(k,219) +1.065*rxt(k,2567)*y(k,447))*y(k,696) + (rxt(k,417)*y(k,690) + &
.880*rxt(k,418)*y(k,548) +rxt(k,420)*y(k,693) +1.800*rxt(k,422)*y(k,633) +rxt(k,423)*y(k,768) + &
rxt(k,424)*y(k,769) +2.000*rxt(k,425)*y(k,620) +2.000*rxt(k,439)*y(k,758) +rxt(k,454)*y(k,370) + &
2.000*rxt(k,469)*y(k,589) +2.000*rxt(k,3956)*y(k,427) +rxt(k,4380)*y(k,860))*y(k,620) + (rxt(k,4377)*y(k,690) + &
rxt(k,4379)*y(k,693) +rxt(k,4381)*y(k,758) +rxt(k,4382)*y(k,370) +rxt(k,4383)*y(k,589) + &
.500*rxt(k,4384)*y(k,633) +.500*rxt(k,4385)*y(k,768) +.500*rxt(k,4386)*y(k,769))*y(k,860) + (rxt(k,583) + &
1.018*rxt(k,490)*y(k,425) +.700*rxt(k,494)*y(k,445) +rxt(k,3819)*y(k,434))*y(k,705) + (rxt(k,48) +rxt(k,49)) &
*y(k,423) + (.750*rxt(k,595)*y(k,586) +1.530*rxt(k,1069)*y(k,512))*y(k,839) +1.380*rxt(k,35)*y(k,341) &
 +.702*rxt(k,38)*y(k,344) +.346*rxt(k,40)*y(k,348) +rxt(k,55)*y(k,433) +.800*rxt(k,111)*y(k,664) +.800*rxt(k,121) &
*y(k,707) +rxt(k,477)*y(k,840)
         loss(k,322) = ((rxt(k,2669) +rxt(k,2670) +rxt(k,2671))* y(k,705) + rxt(k,93) + rxt(k,94))* y(k,634)
         prod(k,322) = 0.
         loss(k,323) = ((rxt(k,2685) +rxt(k,2686) +rxt(k,2687))* y(k,705))* y(k,635)
         prod(k,323) = 0.
         loss(k,216) = ((rxt(k,2554) +rxt(k,2555) +rxt(k,2556))* y(k,705))* y(k,636)
         prod(k,216) = 0.
         loss(k,405) = (rxt(k,488)* y(k,705))* y(k,637)
         prod(k,405) = (rxt(k,231)*y(k,633) +.250*rxt(k,409)*y(k,768) +.250*rxt(k,414)*y(k,769))*y(k,633)
         loss(k,137) = ((rxt(k,300) +rxt(k,301))* y(k,705))* y(k,638)
         prod(k,137) = 0.
         loss(k,149) = ((rxt(k,4068) +rxt(k,4069))* y(k,705))* y(k,639)
         prod(k,149) = 0.
         loss(k,407) = ((rxt(k,3031) +rxt(k,3032) +rxt(k,3033) +rxt(k,3034))* y(k,705))* y(k,640)
         prod(k,407) = 0.
         loss(k,492) = ((rxt(k,2652) +rxt(k,2653) +rxt(k,2654) +rxt(k,2655))* y(k,705))* y(k,641)
         prod(k,492) = 0.
         loss(k,408) = ((rxt(k,2945) +rxt(k,2946) +rxt(k,2947) +rxt(k,2948))* y(k,705))* y(k,642)
         prod(k,408) = 0.
         loss(k,150) = ((rxt(k,2552) +rxt(k,2553))* y(k,705))* y(k,643)
         prod(k,150) = 0.
         loss(k,669) = ((rxt(k,2211) +rxt(k,2212) +rxt(k,2213) +rxt(k,2214) +rxt(k,2215))* y(k,705))* y(k,644)
         prod(k,669) = 0.
         loss(k,912) = (rxt(k,511)* y(k,693) +rxt(k,510)* y(k,705) + rxt(k,95))* y(k,645)
         prod(k,912) = (1.704*rxt(k,523)*y(k,348) +1.704*rxt(k,530)*y(k,349) +1.104*rxt(k,538)*y(k,350) + &
2.700*rxt(k,546)*y(k,590) +3.800*rxt(k,555)*y(k,664) +2.968*rxt(k,566)*y(k,582) +rxt(k,2534)*y(k,346) + &
rxt(k,2622)*y(k,606) +2.000*rxt(k,2640)*y(k,552) +rxt(k,2792)*y(k,501) +2.000*rxt(k,3012)*y(k,554) + &
rxt(k,3103)*y(k,668) +rxt(k,3114)*y(k,567) +rxt(k,3585)*y(k,172))*y(k,696) + (rxt(k,4387)*y(k,690) + &
rxt(k,4389)*y(k,693) +rxt(k,4390)*y(k,620) +rxt(k,4391)*y(k,758) +rxt(k,4392)*y(k,370) +rxt(k,4393)*y(k,589) + &
.500*rxt(k,4394)*y(k,633) +.500*rxt(k,4395)*y(k,768) +.500*rxt(k,4396)*y(k,769))*y(k,861) &
 + (.735*rxt(k,503)*y(k,756) +1.930*rxt(k,2507)*y(k,533) +2.367*rxt(k,2547)*y(k,550) + &
1.634*rxt(k,2559)*y(k,484) +.670*rxt(k,3886)*y(k,607) +1.340*rxt(k,3982)*y(k,502))*y(k,705) +.610*rxt(k,39) &
*y(k,348) +.120*rxt(k,2574)*y(k,839)*y(k,447) +rxt(k,147)*y(k,756)
         loss(k,561) = ((rxt(k,2895) +rxt(k,2896) +rxt(k,2897) +rxt(k,2898))* y(k,705) + rxt(k,96) + rxt(k,97) + rxt(k,98)) &
* y(k,646)
         prod(k,561) = 0.
         loss(k,493) = ((rxt(k,2912) +rxt(k,2913) +rxt(k,2914) +rxt(k,2915))* y(k,705))* y(k,647)
         prod(k,493) = 0.
         loss(k,494) = ((rxt(k,2873) +rxt(k,2874) +rxt(k,2875) +rxt(k,2876))* y(k,705))* y(k,648)
         prod(k,494) = 0.
         loss(k,495) = ((rxt(k,2760) +rxt(k,2761) +rxt(k,2762))* y(k,705) + rxt(k,99) + rxt(k,100))* y(k,649)
         prod(k,495) = 0.
         loss(k,85) = (rxt(k,3857)* y(k,705) + rxt(k,101))* y(k,650)
         prod(k,85) = 0.
         loss(k,808) = ((rxt(k,3805) +rxt(k,3806) +rxt(k,3807) +rxt(k,3808))* y(k,693) + (rxt(k,3800) +rxt(k,3801) +rxt(k,3802) + &
rxt(k,3803) +rxt(k,3804))* y(k,696) + (rxt(k,3797) +rxt(k,3798) +rxt(k,3799))* y(k,705) +rxt(k,3809)* y(k,839)) &
* y(k,651)
         prod(k,808) = 0.
         loss(k,819) = ((rxt(k,3792) +rxt(k,3793) +rxt(k,3794) +rxt(k,3795))* y(k,693) + (rxt(k,3787) +rxt(k,3788) +rxt(k,3789) + &
rxt(k,3790) +rxt(k,3791))* y(k,696) + (rxt(k,3784) +rxt(k,3785) +rxt(k,3786))* y(k,705) +rxt(k,3796)* y(k,839)) &
* y(k,652)
         prod(k,819) = 0.
         loss(k,742) = (rxt(k,3001)* y(k,693) + (rxt(k,2997) +rxt(k,2998) +rxt(k,2999) +rxt(k,3000))* y(k,705) + rxt(k,102) &
 + rxt(k,103) + rxt(k,104))* y(k,653)
         prod(k,742) = 0.
         loss(k,324) = ((rxt(k,2785) +rxt(k,2786) +rxt(k,2787))* y(k,705))* y(k,654)
         prod(k,324) = 0.
         loss(k,563) = ((rxt(k,2899) +rxt(k,2900) +rxt(k,2901) +rxt(k,2902))* y(k,705) + rxt(k,105) + rxt(k,106))* y(k,655)
         prod(k,563) = 0.
         loss(k,620) = ((rxt(k,4117) +rxt(k,4118) +rxt(k,4119) +rxt(k,4120))* y(k,705))* y(k,656)
         prod(k,620) = 0.
         loss(k,564) = ((rxt(k,2877) +rxt(k,2878) +rxt(k,2879) +rxt(k,2880))* y(k,705))* y(k,657)
         prod(k,564) = 0.
         loss(k,722) = ((rxt(k,4136) +rxt(k,4137) +rxt(k,4138))* y(k,693) + (rxt(k,4133) +rxt(k,4134) +rxt(k,4135))* y(k,705)) &
* y(k,658)
         prod(k,722) = 0.
         loss(k,497) = ((rxt(k,2754) +rxt(k,2755) +rxt(k,2756))* y(k,705) + rxt(k,107) + rxt(k,108))* y(k,659)
         prod(k,497) = 0.
         loss(k,745) = ((rxt(k,3996) +rxt(k,3997) +rxt(k,3998))* y(k,693) + (rxt(k,3993) +rxt(k,3994) +rxt(k,3995))* y(k,696) &
 + (rxt(k,3990) +rxt(k,3991) +rxt(k,3992))* y(k,705))* y(k,660)
         prod(k,745) = 0.
         loss(k,565) = ((rxt(k,322) +rxt(k,323) +rxt(k,324) +rxt(k,325))* y(k,705))* y(k,661)
         prod(k,565) = 0.
         loss(k,409) = ((rxt(k,315) +rxt(k,316) +rxt(k,317))* y(k,705))* y(k,662)
         prod(k,409) = 0.
         loss(k,498) = ((rxt(k,2903) +rxt(k,2904) +rxt(k,2905))* y(k,705) + rxt(k,109) + rxt(k,110))* y(k,663)
         prod(k,498) = 0.
         loss(k,939) = (rxt(k,558)* y(k,693) + (rxt(k,554) +rxt(k,555) +rxt(k,556) +rxt(k,557))* y(k,696) + (rxt(k,551) + &
rxt(k,552) +rxt(k,553))* y(k,705) +rxt(k,559)* y(k,839) + rxt(k,111) + rxt(k,112))* y(k,664)
         prod(k,939) = (rxt(k,4397)*y(k,690) +rxt(k,4399)*y(k,693) +rxt(k,4400)*y(k,620) +rxt(k,4401)*y(k,758) + &
rxt(k,4402)*y(k,370) +rxt(k,4403)*y(k,589) +.500*rxt(k,4404)*y(k,633) +.500*rxt(k,4405)*y(k,768) + &
.500*rxt(k,4406)*y(k,769))*y(k,862) + (.024*rxt(k,539)*y(k,350) +.800*rxt(k,589)*y(k,586) + &
1.125*rxt(k,1121)*y(k,33) +.752*rxt(k,1212)*y(k,780))*y(k,696) +.448*rxt(k,3008)*y(k,705)*y(k,554) &
 +.250*rxt(k,1217)*y(k,839)*y(k,780)
         loss(k,622) = ((rxt(k,2181) +rxt(k,2182) +rxt(k,2183) +rxt(k,2184))* y(k,705))* y(k,665)
         prod(k,622) = 0.
         loss(k,151) = ( + rxt(k,218) + rxt(k,387) + rxt(k,388) + het_rates(k,8))* y(k,666)
         prod(k,151) =rxt(k,217)*y(k,693)*y(k,691)
         loss(k,499) = ((rxt(k,2294) +rxt(k,2295) +rxt(k,2296) +rxt(k,2297))* y(k,705))* y(k,667)
         prod(k,499) = 0.
         loss(k,885) = ((rxt(k,3104) +rxt(k,3105) +rxt(k,3106) +rxt(k,3107))* y(k,693) + (rxt(k,3102) +rxt(k,3103))* y(k,696) &
 + (rxt(k,3098) +rxt(k,3099) +rxt(k,3100) +rxt(k,3101))* y(k,705) +rxt(k,3108)* y(k,839))* y(k,668)
         prod(k,885) = 0.
         loss(k,175) = ((rxt(k,293) +rxt(k,294))* y(k,705))* y(k,669)
         prod(k,175) = 0.
         loss(k,176) = ((rxt(k,907) +rxt(k,908))* y(k,705))* y(k,670)
         prod(k,176) = 0.
         loss(k,177) = ((rxt(k,934) +rxt(k,935))* y(k,705))* y(k,671)
         prod(k,177) = 0.
         loss(k,178) = ((rxt(k,960) +rxt(k,961))* y(k,705))* y(k,672)
         prod(k,178) = 0.
         loss(k,179) = ((rxt(k,989) +rxt(k,990))* y(k,705))* y(k,673)
         prod(k,179) = 0.
         loss(k,180) = ((rxt(k,1017) +rxt(k,1018))* y(k,705))* y(k,674)
         prod(k,180) = 0.
         loss(k,181) = ((rxt(k,1046) +rxt(k,1047))* y(k,705))* y(k,675)
         prod(k,181) = 0.
         loss(k,624) = ((rxt(k,2203) +rxt(k,2204) +rxt(k,2205) +rxt(k,2206))* y(k,705))* y(k,676)
         prod(k,624) = 0.
         loss(k,325) = ((rxt(k,2527) +rxt(k,2528) +rxt(k,2529))* y(k,705))* y(k,677)
         prod(k,325) = 0.
         loss(k,217) = ((rxt(k,247) +rxt(k,248) +rxt(k,249))* y(k,705))* y(k,678)
         prod(k,217) = 0.
         loss(k,411) = ((rxt(k,2597) +rxt(k,2598) +rxt(k,2599))* y(k,705))* y(k,679)
         prod(k,411) = 0.
         loss(k,273) = ((rxt(k,255) +rxt(k,256) +rxt(k,257))* y(k,705))* y(k,680)
         prod(k,273) = 0.
         loss(k,218) = ((rxt(k,264) +rxt(k,265) +rxt(k,266))* y(k,705))* y(k,681)
         prod(k,218) = 0.
         loss(k,274) = ((rxt(k,3277) +rxt(k,3278) +rxt(k,3279))* y(k,705))* y(k,682)
         prod(k,274) = 0.
         loss(k,182) = ((rxt(k,272) +rxt(k,273))* y(k,705))* y(k,683)
         prod(k,182) = 0.
         loss(k,275) = ((rxt(k,3394) +rxt(k,3395) +rxt(k,3396))* y(k,705))* y(k,684)
         prod(k,275) = 0.
         loss(k,183) = ((rxt(k,280) +rxt(k,281))* y(k,705))* y(k,685)
         prod(k,183) = 0.
         loss(k,276) = ((rxt(k,3535) +rxt(k,3536) +rxt(k,3537))* y(k,705))* y(k,686)
         prod(k,276) = 0.
         loss(k,184) = ((rxt(k,291) +rxt(k,292))* y(k,705))* y(k,687)
         prod(k,184) = 0.
         loss(k,277) = ((rxt(k,3636) +rxt(k,3637) +rxt(k,3638))* y(k,705))* y(k,688)
         prod(k,277) = 0.
         loss(k,705) = ((rxt(k,4066) +rxt(k,4067))* y(k,693) + (rxt(k,4063) +rxt(k,4064) +rxt(k,4065))* y(k,705))* y(k,689)
         prod(k,705) = 0.
         loss(k,942) = (rxt(k,445)* y(k,370) + (rxt(k,3948) +rxt(k,3949))* y(k,427) +rxt(k,395)* y(k,548) +rxt(k,3825)* y(k,578) &
 +rxt(k,461)* y(k,589) +rxt(k,417)* y(k,620) +rxt(k,403)* y(k,633) + 2.*rxt(k,386)* y(k,690) +rxt(k,385) &
* y(k,693) +rxt(k,383)* y(k,696) +rxt(k,219)* y(k,705) + (rxt(k,427) +rxt(k,428))* y(k,758) +rxt(k,406)* y(k,768) &
 +rxt(k,411)* y(k,769) +rxt(k,215)* y(k,839))* y(k,690)
         prod(k,942) = (rxt(k,113) +rxt(k,382)*y(k,839) +rxt(k,389)*y(k,693) +rxt(k,3859)*y(k,555) +rxt(k,3862)*y(k,556) + &
rxt(k,4145)*y(k,751) +2.000*rxt(k,4147)*y(k,752))*y(k,691) + (rxt(k,92) +rxt(k,3818)*y(k,705))*y(k,631) &
 +rxt(k,78)*y(k,553) +rxt(k,114)*y(k,693) +rxt(k,4518)*y(k,884)
         loss(k,934) = (rxt(k,443)* y(k,370) +rxt(k,339)* y(k,427) +rxt(k,223)* y(k,548) +rxt(k,3859)* y(k,555) +rxt(k,3862) &
* y(k,556) +rxt(k,330)* y(k,559) +rxt(k,235)* y(k,589) +rxt(k,460)* y(k,594) +rxt(k,232)* y(k,620) +rxt(k,217) &
* y(k,693) +rxt(k,340)* y(k,695) +rxt(k,384)* y(k,696) +rxt(k,220)* y(k,705) +rxt(k,4145)* y(k,751) &
 + (rxt(k,4147) +rxt(k,4148))* y(k,752) +rxt(k,234)* y(k,758) + (rxt(k,216) +rxt(k,382))* y(k,839) +rxt(k,476) &
* y(k,840) +rxt(k,478)* y(k,841) + rxt(k,113) + rxt(k,341) + het_rates(k,2))* y(k,691)
         prod(k,934) = (rxt(k,115) +2.000*rxt(k,385)*y(k,690) +rxt(k,393)*y(k,705) +1.600*rxt(k,397)*y(k,548) + &
2.000*rxt(k,399)*y(k,693) +rxt(k,404)*y(k,633) +rxt(k,408)*y(k,768) +rxt(k,413)*y(k,769) +rxt(k,420)*y(k,620) + &
2.000*rxt(k,431)*y(k,758) +2.000*rxt(k,448)*y(k,370) +2.000*rxt(k,464)*y(k,589) +rxt(k,3869)*y(k,483) + &
2.000*rxt(k,3951)*y(k,427) +rxt(k,3980)*y(k,34) +rxt(k,4409)*y(k,863))*y(k,693) + (rxt(k,215)*y(k,839) + &
rxt(k,383)*y(k,696) +2.000*rxt(k,386)*y(k,690) +rxt(k,395)*y(k,548) +rxt(k,403)*y(k,633) +rxt(k,406)*y(k,768) + &
rxt(k,417)*y(k,620) +2.000*rxt(k,427)*y(k,758) +rxt(k,445)*y(k,370) +rxt(k,461)*y(k,589) +rxt(k,3825)*y(k,578) + &
2.000*rxt(k,3948)*y(k,427) +rxt(k,4407)*y(k,863))*y(k,690) + (rxt(k,392)*y(k,553) +rxt(k,396)*y(k,547) + &
.114*rxt(k,577)*y(k,761) +.183*rxt(k,4194)*y(k,762) +.032*rxt(k,4197)*y(k,763) +.084*rxt(k,4201)*y(k,764) + &
.048*rxt(k,4205)*y(k,765) +.048*rxt(k,4213)*y(k,767))*y(k,705) + (rxt(k,4410)*y(k,620) +rxt(k,4411)*y(k,758) + &
rxt(k,4412)*y(k,370) +rxt(k,4413)*y(k,589) +.500*rxt(k,4414)*y(k,633) +.500*rxt(k,4415)*y(k,768) + &
.500*rxt(k,4416)*y(k,769))*y(k,863) + (1.200*rxt(k,53) +rxt(k,3947))*y(k,431) + (.610*rxt(k,72) +rxt(k,224)) &
*y(k,547) + (1.200*rxt(k,88) +rxt(k,458))*y(k,594) + (1.200*rxt(k,121) +rxt(k,233))*y(k,707) &
 + (1.200*rxt(k,123) +rxt(k,426))*y(k,708) + (1.200*rxt(k,125) +rxt(k,444))*y(k,709) +rxt(k,50)*y(k,424) &
 +rxt(k,71)*y(k,546) +rxt(k,82)*y(k,577) +rxt(k,218)*y(k,666) +5.000*rxt(k,156)*y(k,761) +2.000*rxt(k,157) &
*y(k,762) +rxt(k,159)*y(k,763) +4.000*rxt(k,160)*y(k,764) +3.000*rxt(k,164)*y(k,765) +4.000*rxt(k,167)*y(k,766) &
 +2.000*rxt(k,171)*y(k,767) +2.000*rxt(k,4520)*y(k,885)*y(k,884)
         loss(k,414) = ((rxt(k,4087) +rxt(k,4088) +rxt(k,4089) +rxt(k,4090))* y(k,705))* y(k,692)
         prod(k,414) = 0.
         loss(k,931) = ((rxt(k,1787) +rxt(k,1788))* y(k,26) + (rxt(k,1123) +rxt(k,1124) +rxt(k,1125))* y(k,33) +rxt(k,3980) &
* y(k,34) + (rxt(k,1225) +rxt(k,1226) +rxt(k,1227))* y(k,49) + (rxt(k,3909) +rxt(k,3910) +rxt(k,3911))* y(k,50) &
 + (rxt(k,1089) +rxt(k,1090) +rxt(k,1091))* y(k,52) + (rxt(k,1968) +rxt(k,1969))* y(k,53) + (rxt(k,2090) + &
rxt(k,2091))* y(k,55) + (rxt(k,2110) +rxt(k,2111))* y(k,56) + (rxt(k,2130) +rxt(k,2131))* y(k,57) &
 + (rxt(k,2150) +rxt(k,2151))* y(k,58) +rxt(k,2592)* y(k,59) +rxt(k,2753)* y(k,60) +rxt(k,2894)* y(k,62) &
 +rxt(k,3066)* y(k,64) +rxt(k,3192)* y(k,66) + (rxt(k,1934) +rxt(k,1935))* y(k,67) + (rxt(k,1492) +rxt(k,1493)) &
* y(k,71) + (rxt(k,1242) +rxt(k,1243))* y(k,72) + (rxt(k,1431) +rxt(k,1432) +rxt(k,1433))* y(k,81) &
 + (rxt(k,1467) +rxt(k,1468) +rxt(k,1469))* y(k,82) + (rxt(k,1797) +rxt(k,1798))* y(k,83) + (rxt(k,1138) + &
rxt(k,1139) +rxt(k,1140))* y(k,84) +rxt(k,2740)* y(k,94) + (rxt(k,1575) +rxt(k,1576))* y(k,101) + (rxt(k,1540) + &
rxt(k,1541) +rxt(k,1542))* y(k,113) + (rxt(k,1288) +rxt(k,1289))* y(k,120) + (rxt(k,1317) +rxt(k,1318))* y(k,122) &
 + (rxt(k,1694) +rxt(k,1695) +rxt(k,1696))* y(k,123) + (rxt(k,1922) +rxt(k,1923) +rxt(k,1924) +rxt(k,1925)) &
* y(k,124) + (rxt(k,1807) +rxt(k,1808) +rxt(k,1809) +rxt(k,1810))* y(k,127) + (rxt(k,1911) +rxt(k,1912) + &
rxt(k,1913))* y(k,128) + (rxt(k,1528) +rxt(k,1529) +rxt(k,1530) +rxt(k,1531))* y(k,136) + (rxt(k,1598) + &
rxt(k,1599) +rxt(k,1600) +rxt(k,1601))* y(k,141) + (rxt(k,4019) +rxt(k,4020) +rxt(k,4021))* y(k,159) +rxt(k,4062) &
* y(k,160) + (rxt(k,1298) +rxt(k,1299) +rxt(k,1300))* y(k,168) + (rxt(k,3586) +rxt(k,3587) +rxt(k,3588)) &
* y(k,172) + (rxt(k,1161) +rxt(k,1162) +rxt(k,1163))* y(k,176) + (rxt(k,1309) +rxt(k,1310) +rxt(k,1311)) &
* y(k,177) + (rxt(k,1564) +rxt(k,1565) +rxt(k,1566))* y(k,178) + (rxt(k,1171) +rxt(k,1172) +rxt(k,1173)) &
* y(k,180) + (rxt(k,1327) +rxt(k,1328) +rxt(k,1329))* y(k,181) + (rxt(k,1612) +rxt(k,1613) +rxt(k,1614) + &
rxt(k,1615))* y(k,182) + (rxt(k,1845) +rxt(k,1846) +rxt(k,1847) +rxt(k,1848))* y(k,183) +rxt(k,2588)* y(k,192) &
 +rxt(k,3070)* y(k,200) + (rxt(k,1660) +rxt(k,1661) +rxt(k,1662))* y(k,202) + (rxt(k,1553) +rxt(k,1554) + &
rxt(k,1555))* y(k,208) + (rxt(k,1253) +rxt(k,1254))* y(k,213) + (rxt(k,1504) +rxt(k,1505) +rxt(k,1506))* y(k,215) &
 + (rxt(k,1685) +rxt(k,1686) +rxt(k,1687))* y(k,216) + (rxt(k,1978) +rxt(k,1979) +rxt(k,1980) +rxt(k,1981)) &
* y(k,219) + (rxt(k,2010) +rxt(k,2011) +rxt(k,2012) +rxt(k,2013))* y(k,249) + (rxt(k,1624) +rxt(k,1625) + &
rxt(k,1626))* y(k,257) + (rxt(k,1150) +rxt(k,1151) +rxt(k,1152))* y(k,263) + (rxt(k,1265) +rxt(k,1266) + &
rxt(k,1267))* y(k,264) + (rxt(k,1517) +rxt(k,1518) +rxt(k,1519))* y(k,265) + (rxt(k,1820) +rxt(k,1821) + &
rxt(k,1822))* y(k,267) + (rxt(k,1672) +rxt(k,1673) +rxt(k,1674) +rxt(k,1675))* y(k,269) + (rxt(k,2745) + &
rxt(k,2746) +rxt(k,2747) +rxt(k,2748))* y(k,270) + (rxt(k,1419) +rxt(k,1420) +rxt(k,1421) +rxt(k,1422))* y(k,285) &
 + (rxt(k,1636) +rxt(k,1637) +rxt(k,1638) +rxt(k,1639))* y(k,290) + (rxt(k,1946) +rxt(k,1947) +rxt(k,1948)) &
* y(k,291) + (rxt(k,1278) +rxt(k,1279))* y(k,312) + (rxt(k,1479) +rxt(k,1480) +rxt(k,1481) +rxt(k,1482)) &
* y(k,317) + (rxt(k,2514) +rxt(k,2515) +rxt(k,2516))* y(k,344) + (rxt(k,2535) +rxt(k,2536))* y(k,346) &
 +rxt(k,4026)* y(k,353) + (rxt(k,2025) +rxt(k,2026) +rxt(k,2027) +rxt(k,2028))* y(k,355) + (rxt(k,3442) + &
rxt(k,3443) +rxt(k,3444))* y(k,356) +rxt(k,517)* y(k,358) + (rxt(k,2244) +rxt(k,2245) +rxt(k,2246))* y(k,360) &
 + (rxt(k,3234) +rxt(k,3235) +rxt(k,3236) +rxt(k,3237))* y(k,363) + (rxt(k,448) +rxt(k,449))* y(k,370) &
 + (rxt(k,3935) +rxt(k,3936))* y(k,379) + (rxt(k,1106) +rxt(k,1107) +rxt(k,1108))* y(k,385) + (rxt(k,1339) + &
rxt(k,1340) +rxt(k,1341))* y(k,386) + (rxt(k,1649) +rxt(k,1650) +rxt(k,1651))* y(k,387) + (rxt(k,1182) + &
rxt(k,1183) +rxt(k,1184))* y(k,389) + (rxt(k,1349) +rxt(k,1350) +rxt(k,1351))* y(k,391) + (rxt(k,1705) + &
rxt(k,1706) +rxt(k,1707))* y(k,392) + (rxt(k,1361) +rxt(k,1362) +rxt(k,1363))* y(k,395) + (rxt(k,1752) + &
rxt(k,1753) +rxt(k,1754))* y(k,396) + (rxt(k,1585) +rxt(k,1586) +rxt(k,1587) +rxt(k,1588))* y(k,398) &
 + (rxt(k,1857) +rxt(k,1858))* y(k,400) + (rxt(k,1990) +rxt(k,1991))* y(k,404) +rxt(k,484)* y(k,423) &
 + (rxt(k,3951) +rxt(k,3952))* y(k,427) + (rxt(k,3456) +rxt(k,3457) +rxt(k,3458) +rxt(k,3459))* y(k,436) &
 + (rxt(k,4047) +rxt(k,4048))* y(k,439) + (rxt(k,3880) +rxt(k,3881))* y(k,441) +rxt(k,514)* y(k,446) &
 + (rxt(k,2570) +rxt(k,2571) +rxt(k,2572) +rxt(k,2573))* y(k,447) + (rxt(k,1442) +rxt(k,1443) +rxt(k,1444)) &
* y(k,456) + (rxt(k,1203) +rxt(k,1204) +rxt(k,1205))* y(k,457) + (rxt(k,2054) +rxt(k,2055) +rxt(k,2056) + &
rxt(k,2057))* y(k,479) + (rxt(k,4008) +rxt(k,4009) +rxt(k,4010))* y(k,480) + (rxt(k,3833) +rxt(k,3834) + &
rxt(k,3835))* y(k,481) +rxt(k,3869)* y(k,483) + (rxt(k,4142) +rxt(k,4143) +rxt(k,4144))* y(k,499) &
 + (rxt(k,2793) +rxt(k,2794) +rxt(k,2795))* y(k,501) + (rxt(k,3841) +rxt(k,3842) +rxt(k,3843))* y(k,503) &
 + (rxt(k,238) +rxt(k,239))* y(k,512) + (rxt(k,2933) +rxt(k,2934) +rxt(k,2935))* y(k,514) + (rxt(k,4037) + &
rxt(k,4038) +rxt(k,4039))* y(k,520) + (rxt(k,4101) +rxt(k,4102) +rxt(k,4103))* y(k,521) + (rxt(k,3850) + &
rxt(k,3851) +rxt(k,3852))* y(k,522) + (rxt(k,3549) +rxt(k,3550) +rxt(k,3551) +rxt(k,3552) +rxt(k,3553))* y(k,529) &
 +rxt(k,2505)* y(k,530) +rxt(k,2810)* y(k,531) + (rxt(k,508) +rxt(k,509))* y(k,532) +rxt(k,482)* y(k,537) &
 + (rxt(k,397) +rxt(k,398))* y(k,548) + (rxt(k,2641) +rxt(k,2642) +rxt(k,2643) +rxt(k,2644))* y(k,552) &
 + (rxt(k,3014) +rxt(k,3015) +rxt(k,3016) +rxt(k,3017))* y(k,554) + (rxt(k,3115) +rxt(k,3116) +rxt(k,3117) + &
rxt(k,3118) +rxt(k,3119))* y(k,567) + (rxt(k,3247) +rxt(k,3248) +rxt(k,3249) +rxt(k,3250))* y(k,569) &
 + (rxt(k,4157) +rxt(k,4158) +rxt(k,4159))* y(k,574) + (rxt(k,3900) +rxt(k,3901) +rxt(k,3902))* y(k,580) &
 + (rxt(k,568) +rxt(k,569) +rxt(k,570) +rxt(k,571))* y(k,582) + (rxt(k,1098) +rxt(k,1099) +rxt(k,1100))* y(k,585) &
 + (rxt(k,592) +rxt(k,593) +rxt(k,594))* y(k,586) + (rxt(k,3477) +rxt(k,3478) +rxt(k,3479))* y(k,587) &
 + (rxt(k,3756) +rxt(k,3757))* y(k,588) + (rxt(k,464) +rxt(k,465))* y(k,589) + (rxt(k,548) +rxt(k,549))* y(k,590) &
 + (rxt(k,2613) +rxt(k,2614) +rxt(k,2615))* y(k,591) + (rxt(k,2733) +rxt(k,2734) +rxt(k,2735))* y(k,595) &
 + (rxt(k,2623) +rxt(k,2624) +rxt(k,2625))* y(k,606) + (rxt(k,3814) +rxt(k,3815) +rxt(k,3816))* y(k,608) &
 +rxt(k,420)* y(k,620) + (rxt(k,2803) +rxt(k,2804) +rxt(k,2805))* y(k,628) +rxt(k,404)* y(k,633) +rxt(k,511) &
* y(k,645) + (rxt(k,3805) +rxt(k,3806) +rxt(k,3807) +rxt(k,3808))* y(k,651) + (rxt(k,3792) +rxt(k,3793) + &
rxt(k,3794) +rxt(k,3795))* y(k,652) +rxt(k,3001)* y(k,653) + (rxt(k,4136) +rxt(k,4137) +rxt(k,4138))* y(k,658) &
 + (rxt(k,3996) +rxt(k,3997) +rxt(k,3998))* y(k,660) +rxt(k,558)* y(k,664) + (rxt(k,3104) +rxt(k,3105) + &
rxt(k,3106) +rxt(k,3107))* y(k,668) + (rxt(k,4066) +rxt(k,4067))* y(k,689) +rxt(k,385)* y(k,690) + (rxt(k,217) + &
rxt(k,389))* y(k,691) + 2.*rxt(k,399)* y(k,693) +rxt(k,393)* y(k,705) + (rxt(k,4165) +rxt(k,4166) +rxt(k,4167)) &
* y(k,720) +rxt(k,2526)* y(k,737) + (rxt(k,1077) +rxt(k,1078))* y(k,739) +rxt(k,487)* y(k,757) + (rxt(k,431) + &
rxt(k,432))* y(k,758) +rxt(k,408)* y(k,768) +rxt(k,413)* y(k,769) + (rxt(k,2066) +rxt(k,2067) +rxt(k,2068)) &
* y(k,771) + (rxt(k,2199) +rxt(k,2200) +rxt(k,2201))* y(k,776) + (rxt(k,3924) +rxt(k,3925))* y(k,779) &
 + (rxt(k,1214) +rxt(k,1215) +rxt(k,1216))* y(k,780) + (rxt(k,1455) +rxt(k,1456) +rxt(k,1457) +rxt(k,1458)) &
* y(k,781) + (rxt(k,1867) +rxt(k,1868) +rxt(k,1869) +rxt(k,1870))* y(k,782) + (rxt(k,1879) +rxt(k,1880) + &
rxt(k,1881))* y(k,783) + (rxt(k,295) +rxt(k,296) +rxt(k,297))* y(k,784) + (rxt(k,1397) +rxt(k,1398) +rxt(k,1399)) &
* y(k,785) + (rxt(k,1729) +rxt(k,1730) +rxt(k,1731))* y(k,786) + (rxt(k,1832) +rxt(k,1833) +rxt(k,1834)) &
* y(k,787) + (rxt(k,1193) +rxt(k,1194) +rxt(k,1195))* y(k,788) + (rxt(k,1407) +rxt(k,1408) +rxt(k,1409)) &
* y(k,789) + (rxt(k,1740) +rxt(k,1741) +rxt(k,1742))* y(k,790) + (rxt(k,1890) +rxt(k,1891) +rxt(k,1892)) &
* y(k,791) + (rxt(k,1373) +rxt(k,1374) +rxt(k,1375))* y(k,792) + (rxt(k,1777) +rxt(k,1778) +rxt(k,1779)) &
* y(k,793) + (rxt(k,1716) +rxt(k,1717) +rxt(k,1718) +rxt(k,1719))* y(k,794) + (rxt(k,2000) +rxt(k,2001) + &
rxt(k,2002))* y(k,795) + (rxt(k,1901) +rxt(k,1902))* y(k,796) + (rxt(k,1957) +rxt(k,1958) +rxt(k,1959))* y(k,797) &
 + (rxt(k,1385) +rxt(k,1386) +rxt(k,1387))* y(k,798) + (rxt(k,1764) +rxt(k,1765) +rxt(k,1766) +rxt(k,1767)) &
* y(k,799) + (rxt(k,2100) +rxt(k,2101))* y(k,800) + (rxt(k,2120) +rxt(k,2121))* y(k,801) + (rxt(k,2140) + &
rxt(k,2141))* y(k,802) + (rxt(k,2160) +rxt(k,2161))* y(k,803) + (rxt(k,2170) +rxt(k,2171))* y(k,804) +rxt(k,3987) &
* y(k,806) + (rxt(k,4079) +rxt(k,4080) +rxt(k,4081))* y(k,810) + (rxt(k,4172) +rxt(k,4173) +rxt(k,4174)) &
* y(k,827) + (rxt(k,3893) +rxt(k,3894))* y(k,829) + (rxt(k,2632) +rxt(k,2633) +rxt(k,2634))* y(k,837) &
 + (rxt(k,2040) +rxt(k,2041) +rxt(k,2042) +rxt(k,2043))* y(k,879) + (rxt(k,2078) +rxt(k,2079) +rxt(k,2080) + &
rxt(k,2081))* y(k,881) + rxt(k,114) + rxt(k,115) + het_rates(k,7))* y(k,693)
         prod(k,931) = (rxt(k,216)*y(k,839) +rxt(k,384)*y(k,696) +rxt(k,460)*y(k,594))*y(k,691) +.800*rxt(k,54)*y(k,431) &
 +rxt(k,221)*y(k,705)*y(k,546) +.390*rxt(k,72)*y(k,547) +1.200*rxt(k,89)*y(k,594) +rxt(k,218)*y(k,666) &
 +.800*rxt(k,120)*y(k,707) +.800*rxt(k,122)*y(k,708) +.800*rxt(k,124)*y(k,709)
         loss(k,219) = (rxt(k,515)* y(k,705) + rxt(k,116) + rxt(k,117))* y(k,694)
         prod(k,219) =rxt(k,478)*y(k,841)*y(k,691)
         loss(k,719) = (rxt(k,3988)* y(k,548) +rxt(k,340)* y(k,691))* y(k,695)
         prod(k,719) = (rxt(k,3987)*y(k,806) +rxt(k,4026)*y(k,353) +rxt(k,4062)*y(k,160))*y(k,693) + (2.913*rxt(k,3984)*y(k,806) + &
2.397*rxt(k,4023)*y(k,353) +2.580*rxt(k,4058)*y(k,160))*y(k,705)
         loss(k,944) = ((rxt(k,1784) +rxt(k,1785) +rxt(k,1786))* y(k,26) + (rxt(k,1120) +rxt(k,1121) +rxt(k,1122))* y(k,33) &
 + (rxt(k,1221) +rxt(k,1222) +rxt(k,1223) +rxt(k,1224))* y(k,49) + (rxt(k,3907) +rxt(k,3908))* y(k,50) &
 + (rxt(k,1085) +rxt(k,1086) +rxt(k,1087) +rxt(k,1088))* y(k,52) + (rxt(k,1964) +rxt(k,1965) +rxt(k,1966) + &
rxt(k,1967))* y(k,53) + (rxt(k,2086) +rxt(k,2087) +rxt(k,2088) +rxt(k,2089))* y(k,55) + (rxt(k,2106) + &
rxt(k,2107) +rxt(k,2108) +rxt(k,2109))* y(k,56) + (rxt(k,2126) +rxt(k,2127) +rxt(k,2128) +rxt(k,2129))* y(k,57) &
 + (rxt(k,2146) +rxt(k,2147) +rxt(k,2148) +rxt(k,2149))* y(k,58) + (rxt(k,1930) +rxt(k,1931) +rxt(k,1932) + &
rxt(k,1933))* y(k,67) + (rxt(k,1488) +rxt(k,1489) +rxt(k,1490) +rxt(k,1491))* y(k,71) + (rxt(k,1238) + &
rxt(k,1239) +rxt(k,1240) +rxt(k,1241))* y(k,72) + (rxt(k,1427) +rxt(k,1428) +rxt(k,1429) +rxt(k,1430))* y(k,81) &
 + (rxt(k,1463) +rxt(k,1464) +rxt(k,1465) +rxt(k,1466))* y(k,82) + (rxt(k,1793) +rxt(k,1794) +rxt(k,1795) + &
rxt(k,1796))* y(k,83) + (rxt(k,1134) +rxt(k,1135) +rxt(k,1136) +rxt(k,1137))* y(k,84) + (rxt(k,1571) + &
rxt(k,1572) +rxt(k,1573) +rxt(k,1574))* y(k,101) + (rxt(k,1536) +rxt(k,1537) +rxt(k,1538) +rxt(k,1539))* y(k,113) &
 + (rxt(k,1284) +rxt(k,1285) +rxt(k,1286) +rxt(k,1287))* y(k,120) + (rxt(k,1315) +rxt(k,1316))* y(k,122) &
 + (rxt(k,1691) +rxt(k,1692) +rxt(k,1693))* y(k,123) + (rxt(k,1919) +rxt(k,1920) +rxt(k,1921))* y(k,124) &
 + (rxt(k,1803) +rxt(k,1804) +rxt(k,1805) +rxt(k,1806))* y(k,127) + (rxt(k,1907) +rxt(k,1908) +rxt(k,1909) + &
rxt(k,1910))* y(k,128) + (rxt(k,1524) +rxt(k,1525) +rxt(k,1526) +rxt(k,1527))* y(k,136) + (rxt(k,1593) + &
rxt(k,1594) +rxt(k,1595) +rxt(k,1596) +rxt(k,1597))* y(k,141) + (rxt(k,4016) +rxt(k,4017) +rxt(k,4018))* y(k,159) &
 + (rxt(k,1294) +rxt(k,1295) +rxt(k,1296) +rxt(k,1297))* y(k,168) + (rxt(k,3584) +rxt(k,3585))* y(k,172) &
 + (rxt(k,1157) +rxt(k,1158) +rxt(k,1159) +rxt(k,1160))* y(k,176) + (rxt(k,1305) +rxt(k,1306) +rxt(k,1307) + &
rxt(k,1308))* y(k,177) + (rxt(k,1560) +rxt(k,1561) +rxt(k,1562) +rxt(k,1563))* y(k,178) + (rxt(k,1167) + &
rxt(k,1168) +rxt(k,1169) +rxt(k,1170))* y(k,180) + (rxt(k,1323) +rxt(k,1324) +rxt(k,1325) +rxt(k,1326))* y(k,181) &
 + (rxt(k,1607) +rxt(k,1608) +rxt(k,1609) +rxt(k,1610) +rxt(k,1611))* y(k,182) + (rxt(k,1840) +rxt(k,1841) + &
rxt(k,1842) +rxt(k,1843) +rxt(k,1844))* y(k,183) + (rxt(k,1656) +rxt(k,1657) +rxt(k,1658) +rxt(k,1659))* y(k,202) &
 + (rxt(k,1548) +rxt(k,1549) +rxt(k,1550) +rxt(k,1551) +rxt(k,1552))* y(k,208) + (rxt(k,1249) +rxt(k,1250) + &
rxt(k,1251) +rxt(k,1252))* y(k,213) + (rxt(k,1499) +rxt(k,1500) +rxt(k,1501) +rxt(k,1502) +rxt(k,1503))* y(k,215) &
 + (rxt(k,1680) +rxt(k,1681) +rxt(k,1682) +rxt(k,1683) +rxt(k,1684))* y(k,216) + (rxt(k,1974) +rxt(k,1975) + &
rxt(k,1976) +rxt(k,1977))* y(k,219) + (rxt(k,2006) +rxt(k,2007) +rxt(k,2008) +rxt(k,2009))* y(k,249) &
 + (rxt(k,1620) +rxt(k,1621) +rxt(k,1622) +rxt(k,1623))* y(k,257) + (rxt(k,1146) +rxt(k,1147) +rxt(k,1148) + &
rxt(k,1149))* y(k,263) + (rxt(k,1260) +rxt(k,1261) +rxt(k,1262) +rxt(k,1263) +rxt(k,1264))* y(k,264) &
 + (rxt(k,1512) +rxt(k,1513) +rxt(k,1514) +rxt(k,1515) +rxt(k,1516))* y(k,265) + (rxt(k,1816) +rxt(k,1817) + &
rxt(k,1818) +rxt(k,1819))* y(k,267) + (rxt(k,1667) +rxt(k,1668) +rxt(k,1669) +rxt(k,1670) +rxt(k,1671))* y(k,269) &
 + (rxt(k,1415) +rxt(k,1416) +rxt(k,1417) +rxt(k,1418))* y(k,285) + (rxt(k,1631) +rxt(k,1632) +rxt(k,1633) + &
rxt(k,1634) +rxt(k,1635))* y(k,290) + (rxt(k,1941) +rxt(k,1942) +rxt(k,1943) +rxt(k,1944) +rxt(k,1945))* y(k,291) &
 + (rxt(k,1273) +rxt(k,1274) +rxt(k,1275) +rxt(k,1276) +rxt(k,1277))* y(k,312) + (rxt(k,1475) +rxt(k,1476) + &
rxt(k,1477) +rxt(k,1478))* y(k,317) +rxt(k,598)* y(k,343) + (rxt(k,2512) +rxt(k,2513))* y(k,344) + (rxt(k,2533) + &
rxt(k,2534))* y(k,346) + (rxt(k,522) +rxt(k,523) +rxt(k,524))* y(k,348) + (rxt(k,529) +rxt(k,530) +rxt(k,531)) &
* y(k,349) + (rxt(k,536) +rxt(k,537) +rxt(k,538) +rxt(k,539) +rxt(k,540) +rxt(k,541))* y(k,350) + (rxt(k,2019) + &
rxt(k,2020) +rxt(k,2021) +rxt(k,2022) +rxt(k,2023) +rxt(k,2024))* y(k,355) + (rxt(k,3437) +rxt(k,3438) + &
rxt(k,3439) +rxt(k,3440) +rxt(k,3441))* y(k,356) + (rxt(k,2242) +rxt(k,2243))* y(k,360) + (rxt(k,3230) + &
rxt(k,3231) +rxt(k,3232) +rxt(k,3233))* y(k,363) + (rxt(k,3932) +rxt(k,3933) +rxt(k,3934))* y(k,379) &
 + (rxt(k,1104) +rxt(k,1105))* y(k,385) + (rxt(k,1334) +rxt(k,1335) +rxt(k,1336) +rxt(k,1337) +rxt(k,1338)) &
* y(k,386) + (rxt(k,1644) +rxt(k,1645) +rxt(k,1646) +rxt(k,1647) +rxt(k,1648))* y(k,387) + (rxt(k,1178) + &
rxt(k,1179) +rxt(k,1180) +rxt(k,1181))* y(k,389) + (rxt(k,1346) +rxt(k,1347) +rxt(k,1348))* y(k,391) &
 + (rxt(k,1701) +rxt(k,1702) +rxt(k,1703) +rxt(k,1704))* y(k,392) + (rxt(k,1356) +rxt(k,1357) +rxt(k,1358) + &
rxt(k,1359) +rxt(k,1360))* y(k,395) + (rxt(k,1747) +rxt(k,1748) +rxt(k,1749) +rxt(k,1750) +rxt(k,1751))* y(k,396) &
 + (rxt(k,1581) +rxt(k,1582) +rxt(k,1583) +rxt(k,1584))* y(k,398) + (rxt(k,1853) +rxt(k,1854) +rxt(k,1855) + &
rxt(k,1856))* y(k,400) + (rxt(k,1986) +rxt(k,1987) +rxt(k,1988) +rxt(k,1989))* y(k,404) + (rxt(k,3451) + &
rxt(k,3452) +rxt(k,3453) +rxt(k,3454) +rxt(k,3455))* y(k,436) + (rxt(k,4044) +rxt(k,4045) +rxt(k,4046))* y(k,439) &
 + (rxt(k,2567) +rxt(k,2568) +rxt(k,2569))* y(k,447) + (rxt(k,1438) +rxt(k,1439) +rxt(k,1440) +rxt(k,1441)) &
* y(k,456) + (rxt(k,1200) +rxt(k,1201) +rxt(k,1202))* y(k,457) + (rxt(k,2049) +rxt(k,2050) +rxt(k,2051) + &
rxt(k,2052) +rxt(k,2053))* y(k,479) + (rxt(k,4005) +rxt(k,4006) +rxt(k,4007))* y(k,480) + (rxt(k,3831) + &
rxt(k,3832))* y(k,481) + (rxt(k,2791) +rxt(k,2792))* y(k,501) + (rxt(k,3839) +rxt(k,3840))* y(k,503) &
 + (rxt(k,1067) +rxt(k,1068))* y(k,512) + (rxt(k,2929) +rxt(k,2930) +rxt(k,2931) +rxt(k,2932))* y(k,514) &
 + (rxt(k,4034) +rxt(k,4035) +rxt(k,4036))* y(k,520) + (rxt(k,4098) +rxt(k,4099) +rxt(k,4100))* y(k,521) &
 + (rxt(k,3848) +rxt(k,3849))* y(k,522) + (rxt(k,3543) +rxt(k,3544) +rxt(k,3545) +rxt(k,3546) +rxt(k,3547) + &
rxt(k,3548))* y(k,529) +rxt(k,225)* y(k,548) + (rxt(k,2639) +rxt(k,2640))* y(k,552) + (rxt(k,3010) +rxt(k,3011) + &
rxt(k,3012) +rxt(k,3013))* y(k,554) +rxt(k,3858)* y(k,555) + (rxt(k,3860) +rxt(k,3861))* y(k,556) +rxt(k,3822) &
* y(k,559) + (rxt(k,3113) +rxt(k,3114))* y(k,567) + (rxt(k,3243) +rxt(k,3244) +rxt(k,3245) +rxt(k,3246)) &
* y(k,569) + (rxt(k,4154) +rxt(k,4155) +rxt(k,4156))* y(k,574) + (rxt(k,3898) +rxt(k,3899))* y(k,580) &
 + (rxt(k,564) +rxt(k,565) +rxt(k,566) +rxt(k,567))* y(k,582) + (rxt(k,1095) +rxt(k,1096) +rxt(k,1097))* y(k,585) &
 + (rxt(k,587) +rxt(k,588) +rxt(k,589) +rxt(k,590) +rxt(k,591))* y(k,586) + (rxt(k,3472) +rxt(k,3473) + &
rxt(k,3474) +rxt(k,3475) +rxt(k,3476))* y(k,587) + (rxt(k,3752) +rxt(k,3753) +rxt(k,3754) +rxt(k,3755))* y(k,588) &
 + (rxt(k,545) +rxt(k,546) +rxt(k,547))* y(k,590) + (rxt(k,2609) +rxt(k,2610) +rxt(k,2611) +rxt(k,2612)) &
* y(k,591) + (rxt(k,2730) +rxt(k,2731) +rxt(k,2732))* y(k,595) + (rxt(k,2621) +rxt(k,2622))* y(k,606) &
 + (rxt(k,3812) +rxt(k,3813))* y(k,608) + (rxt(k,2799) +rxt(k,2800) +rxt(k,2801) +rxt(k,2802))* y(k,628) &
 + (rxt(k,3800) +rxt(k,3801) +rxt(k,3802) +rxt(k,3803) +rxt(k,3804))* y(k,651) + (rxt(k,3787) +rxt(k,3788) + &
rxt(k,3789) +rxt(k,3790) +rxt(k,3791))* y(k,652) + (rxt(k,3993) +rxt(k,3994) +rxt(k,3995))* y(k,660) &
 + (rxt(k,554) +rxt(k,555) +rxt(k,556) +rxt(k,557))* y(k,664) + (rxt(k,3102) +rxt(k,3103))* y(k,668) +rxt(k,383) &
* y(k,690) +rxt(k,384)* y(k,691) +rxt(k,394)* y(k,705) + (rxt(k,1074) +rxt(k,1075) +rxt(k,1076))* y(k,739) &
 +rxt(k,4146)* y(k,751) + (rxt(k,4149) +rxt(k,4150))* y(k,752) + (rxt(k,2062) +rxt(k,2063) +rxt(k,2064) + &
rxt(k,2065))* y(k,771) + (rxt(k,2197) +rxt(k,2198))* y(k,776) + (rxt(k,3921) +rxt(k,3922) +rxt(k,3923))* y(k,779) &
 + (rxt(k,1210) +rxt(k,1211) +rxt(k,1212) +rxt(k,1213))* y(k,780) + (rxt(k,1450) +rxt(k,1451) +rxt(k,1452) + &
rxt(k,1453) +rxt(k,1454))* y(k,781) + (rxt(k,1863) +rxt(k,1864) +rxt(k,1865) +rxt(k,1866))* y(k,782) &
 + (rxt(k,1875) +rxt(k,1876) +rxt(k,1877) +rxt(k,1878))* y(k,783) + (rxt(k,1112) +rxt(k,1113))* y(k,784) &
 + (rxt(k,1392) +rxt(k,1393) +rxt(k,1394) +rxt(k,1395) +rxt(k,1396))* y(k,785) + (rxt(k,1724) +rxt(k,1725) + &
rxt(k,1726) +rxt(k,1727) +rxt(k,1728))* y(k,786) + (rxt(k,1827) +rxt(k,1828) +rxt(k,1829) +rxt(k,1830) + &
rxt(k,1831))* y(k,787) + (rxt(k,1189) +rxt(k,1190) +rxt(k,1191) +rxt(k,1192))* y(k,788) + (rxt(k,1404) + &
rxt(k,1405) +rxt(k,1406))* y(k,789) + (rxt(k,1736) +rxt(k,1737) +rxt(k,1738) +rxt(k,1739))* y(k,790) &
 + (rxt(k,1886) +rxt(k,1887) +rxt(k,1888) +rxt(k,1889))* y(k,791) + (rxt(k,1368) +rxt(k,1369) +rxt(k,1370) + &
rxt(k,1371) +rxt(k,1372))* y(k,792) + (rxt(k,1772) +rxt(k,1773) +rxt(k,1774) +rxt(k,1775) +rxt(k,1776))* y(k,793) &
 + (rxt(k,1712) +rxt(k,1713) +rxt(k,1714) +rxt(k,1715))* y(k,794) + (rxt(k,1996) +rxt(k,1997) +rxt(k,1998) + &
rxt(k,1999))* y(k,795) + (rxt(k,1897) +rxt(k,1898) +rxt(k,1899) +rxt(k,1900))* y(k,796) + (rxt(k,1953) + &
rxt(k,1954) +rxt(k,1955) +rxt(k,1956))* y(k,797) + (rxt(k,1380) +rxt(k,1381) +rxt(k,1382) +rxt(k,1383) + &
rxt(k,1384))* y(k,798) + (rxt(k,1759) +rxt(k,1760) +rxt(k,1761) +rxt(k,1762) +rxt(k,1763))* y(k,799) &
 + (rxt(k,2096) +rxt(k,2097) +rxt(k,2098) +rxt(k,2099))* y(k,800) + (rxt(k,2116) +rxt(k,2117) +rxt(k,2118) + &
rxt(k,2119))* y(k,801) + (rxt(k,2136) +rxt(k,2137) +rxt(k,2138) +rxt(k,2139))* y(k,802) + (rxt(k,2156) + &
rxt(k,2157) +rxt(k,2158) +rxt(k,2159))* y(k,803) + (rxt(k,2166) +rxt(k,2167) +rxt(k,2168) +rxt(k,2169))* y(k,804) &
 + (rxt(k,4076) +rxt(k,4077) +rxt(k,4078))* y(k,810) + (rxt(k,3891) +rxt(k,3892))* y(k,829) + (rxt(k,2630) + &
rxt(k,2631))* y(k,837) +rxt(k,381)* y(k,839) + (rxt(k,2034) +rxt(k,2035) +rxt(k,2036) +rxt(k,2037) +rxt(k,2038) + &
rxt(k,2039))* y(k,879) + (rxt(k,2073) +rxt(k,2074) +rxt(k,2075) +rxt(k,2076) +rxt(k,2077))* y(k,881) + rxt(k,118) &
 + rxt(k,119) + het_rates(k,1))* y(k,696)
         prod(k,944) = (.300*rxt(k,419)*y(k,620) +.300*rxt(k,430)*y(k,758) +.300*rxt(k,447)*y(k,370) +.300*rxt(k,463)*y(k,589) + &
.250*rxt(k,3950)*y(k,427))*y(k,548) +rxt(k,214)*y(k,839)
         loss(k,677) = ((rxt(k,2261) +rxt(k,2262) +rxt(k,2263) +rxt(k,2264) +rxt(k,2265))* y(k,705))* y(k,697)
         prod(k,677) = 0.
         loss(k,678) = ((rxt(k,2307) +rxt(k,2308) +rxt(k,2309) +rxt(k,2310) +rxt(k,2311))* y(k,705))* y(k,698)
         prod(k,678) = 0.
         loss(k,679) = ((rxt(k,2344) +rxt(k,2345) +rxt(k,2346) +rxt(k,2347) +rxt(k,2348))* y(k,705))* y(k,699)
         prod(k,679) = 0.
         loss(k,655) = ((rxt(k,2380) +rxt(k,2381) +rxt(k,2382) +rxt(k,2383) +rxt(k,2384))* y(k,705))* y(k,700)
         prod(k,655) = 0.
         loss(k,656) = ((rxt(k,2412) +rxt(k,2413) +rxt(k,2414) +rxt(k,2415) +rxt(k,2416))* y(k,705))* y(k,701)
         prod(k,656) = 0.
         loss(k,657) = ((rxt(k,2444) +rxt(k,2445) +rxt(k,2446) +rxt(k,2447) +rxt(k,2448))* y(k,705))* y(k,702)
         prod(k,657) = 0.
         loss(k,658) = ((rxt(k,2476) +rxt(k,2477) +rxt(k,2478) +rxt(k,2479) +rxt(k,2480))* y(k,705))* y(k,703)
         prod(k,658) = 0.
         loss(k,680) = ((rxt(k,2216) +rxt(k,2217) +rxt(k,2218) +rxt(k,2219) +rxt(k,2220))* y(k,705))* y(k,704)
         prod(k,680) = 0.
         loss(k,945) = (rxt(k,4131)* y(k,1) +rxt(k,4168)* y(k,2) + (rxt(k,334) +rxt(k,335))* y(k,3) + (rxt(k,336) +rxt(k,337)) &
* y(k,4) + (rxt(k,707) +rxt(k,708) +rxt(k,709) +rxt(k,710))* y(k,5) + (rxt(k,820) +rxt(k,821) +rxt(k,822) + &
rxt(k,823))* y(k,6) + (rxt(k,711) +rxt(k,712) +rxt(k,713) +rxt(k,714))* y(k,7) +rxt(k,338)* y(k,8) &
 + (rxt(k,3873) +rxt(k,3874))* y(k,9) + (rxt(k,3870) +rxt(k,3871))* y(k,10) + (rxt(k,645) +rxt(k,646) + &
rxt(k,647) +rxt(k,648))* y(k,11) + (rxt(k,715) +rxt(k,716) +rxt(k,717) +rxt(k,718))* y(k,12) + (rxt(k,812) + &
rxt(k,813) +rxt(k,814) +rxt(k,815))* y(k,13) + (rxt(k,2226) +rxt(k,2227) +rxt(k,2228) +rxt(k,2229) +rxt(k,2230)) &
* y(k,14) + (rxt(k,719) +rxt(k,720) +rxt(k,721) +rxt(k,722))* y(k,15) + (rxt(k,2231) +rxt(k,2232) +rxt(k,2233) + &
rxt(k,2234) +rxt(k,2235))* y(k,16) + (rxt(k,723) +rxt(k,724) +rxt(k,725) +rxt(k,726))* y(k,17) + (rxt(k,2581) + &
rxt(k,2582) +rxt(k,2583) +rxt(k,2584))* y(k,18) + (rxt(k,1115) +rxt(k,1116))* y(k,19) + (rxt(k,2676) + &
rxt(k,2677) +rxt(k,2678))* y(k,20) +rxt(k,333)* y(k,21) + (rxt(k,3941) +rxt(k,3942) +rxt(k,3943))* y(k,22) &
 + (rxt(k,3497) +rxt(k,3498) +rxt(k,3499) +rxt(k,3500))* y(k,23) + (rxt(k,649) +rxt(k,650) +rxt(k,651) + &
rxt(k,652))* y(k,24) + (rxt(k,731) +rxt(k,732) +rxt(k,733))* y(k,25) + (rxt(k,1781) +rxt(k,1782) +rxt(k,1783)) &
* y(k,26) + (rxt(k,3174) +rxt(k,3175) +rxt(k,3176) +rxt(k,3177))* y(k,27) + (rxt(k,1229) +rxt(k,1230) + &
rxt(k,1231))* y(k,28) + (rxt(k,951) +rxt(k,952) +rxt(k,953) +rxt(k,954))* y(k,29) + (rxt(k,816) +rxt(k,817) + &
rxt(k,818) +rxt(k,819))* y(k,30) + (rxt(k,1034) +rxt(k,1035) +rxt(k,1036))* y(k,31) + (rxt(k,2236) +rxt(k,2237) + &
rxt(k,2238) +rxt(k,2239))* y(k,32) + (rxt(k,1117) +rxt(k,1118) +rxt(k,1119))* y(k,33) +rxt(k,3979)* y(k,34) &
 + (rxt(k,2672) +rxt(k,2673) +rxt(k,2674) +rxt(k,2675))* y(k,35) + (rxt(k,894) +rxt(k,895) +rxt(k,896))* y(k,36) &
 + (rxt(k,653) +rxt(k,654) +rxt(k,655) +rxt(k,656))* y(k,37) + (rxt(k,737) +rxt(k,738) +rxt(k,739) +rxt(k,740)) &
* y(k,38) + (rxt(k,2538) +rxt(k,2539) +rxt(k,2540))* y(k,39) + (rxt(k,1042) +rxt(k,1043) +rxt(k,1044) + &
rxt(k,1045))* y(k,40) + (rxt(k,924) +rxt(k,925) +rxt(k,926) +rxt(k,927))* y(k,41) + (rxt(k,980) +rxt(k,981) + &
rxt(k,982) +rxt(k,983))* y(k,42) + (rxt(k,1059) +rxt(k,1060) +rxt(k,1061))* y(k,43) + (rxt(k,1009) +rxt(k,1010) + &
rxt(k,1011))* y(k,44) + (rxt(k,2679) +rxt(k,2680) +rxt(k,2681))* y(k,45) + (rxt(k,897) +rxt(k,898) +rxt(k,899)) &
* y(k,46) + (rxt(k,734) +rxt(k,735) +rxt(k,736))* y(k,47) + (rxt(k,2646) +rxt(k,2647) +rxt(k,2648))* y(k,48) &
 + (rxt(k,1218) +rxt(k,1219) +rxt(k,1220))* y(k,49) + (rxt(k,3904) +rxt(k,3905) +rxt(k,3906))* y(k,50) &
 + (rxt(k,3400) +rxt(k,3401) +rxt(k,3402) +rxt(k,3403))* y(k,51) + (rxt(k,1082) +rxt(k,1083) +rxt(k,1084)) &
* y(k,52) + (rxt(k,1961) +rxt(k,1962) +rxt(k,1963))* y(k,53) + (rxt(k,3487) +rxt(k,3488) +rxt(k,3489))* y(k,54) &
 + (rxt(k,2083) +rxt(k,2084) +rxt(k,2085))* y(k,55) + (rxt(k,2103) +rxt(k,2104) +rxt(k,2105))* y(k,56) &
 + (rxt(k,2123) +rxt(k,2124) +rxt(k,2125))* y(k,57) + (rxt(k,2143) +rxt(k,2144) +rxt(k,2145))* y(k,58) &
 + (rxt(k,2589) +rxt(k,2590) +rxt(k,2591))* y(k,59) + (rxt(k,2749) +rxt(k,2750) +rxt(k,2751) +rxt(k,2752)) &
* y(k,60) + (rxt(k,2906) +rxt(k,2907) +rxt(k,2908))* y(k,61) + (rxt(k,2891) +rxt(k,2892) +rxt(k,2893))* y(k,62) &
 + (rxt(k,3087) +rxt(k,3088) +rxt(k,3089))* y(k,63) + (rxt(k,3063) +rxt(k,3064) +rxt(k,3065))* y(k,64) &
 + (rxt(k,3196) +rxt(k,3197) +rxt(k,3198))* y(k,65) + (rxt(k,3189) +rxt(k,3190) +rxt(k,3191))* y(k,66) &
 + (rxt(k,1927) +rxt(k,1928) +rxt(k,1929))* y(k,67) + (rxt(k,3346) +rxt(k,3347) +rxt(k,3348) +rxt(k,3349)) &
* y(k,68) + (rxt(k,928) +rxt(k,929) +rxt(k,930))* y(k,69) + (rxt(k,824) +rxt(k,825) +rxt(k,826))* y(k,70) &
 + (rxt(k,1484) +rxt(k,1485) +rxt(k,1486) +rxt(k,1487))* y(k,71) + (rxt(k,1234) +rxt(k,1235) +rxt(k,1236) + &
rxt(k,1237))* y(k,72) + (rxt(k,3211) +rxt(k,3212) +rxt(k,3213) +rxt(k,3214))* y(k,73) + (rxt(k,984) +rxt(k,985) + &
rxt(k,986))* y(k,74) + (rxt(k,1037) +rxt(k,1038) +rxt(k,1039))* y(k,75) + (rxt(k,727) +rxt(k,728) +rxt(k,729) + &
rxt(k,730))* y(k,76) + (rxt(k,900) +rxt(k,901) +rxt(k,902) +rxt(k,903))* y(k,77) + (rxt(k,955) +rxt(k,956) + &
rxt(k,957))* y(k,78) + (rxt(k,1012) +rxt(k,1013) +rxt(k,1014))* y(k,79) + (rxt(k,1062) +rxt(k,1063) +rxt(k,1064)) &
* y(k,80) + (rxt(k,1424) +rxt(k,1425) +rxt(k,1426))* y(k,81) + (rxt(k,1460) +rxt(k,1461) +rxt(k,1462))* y(k,82) &
 + (rxt(k,1790) +rxt(k,1791) +rxt(k,1792))* y(k,83) + (rxt(k,1130) +rxt(k,1131) +rxt(k,1132) +rxt(k,1133)) &
* y(k,84) + (rxt(k,282) +rxt(k,283) +rxt(k,284))* y(k,85) + (rxt(k,274) +rxt(k,275) +rxt(k,276))* y(k,86) &
 + (rxt(k,697) +rxt(k,698) +rxt(k,699) +rxt(k,700))* y(k,87) + (rxt(k,285) +rxt(k,286) +rxt(k,287) +rxt(k,288)) &
* y(k,88) + (rxt(k,787) +rxt(k,788) +rxt(k,789) +rxt(k,790))* y(k,89) + (rxt(k,871) +rxt(k,872) +rxt(k,873) + &
rxt(k,874))* y(k,90) + (rxt(k,748) +rxt(k,749) +rxt(k,750) +rxt(k,751))* y(k,91) + (rxt(k,875) +rxt(k,876) + &
rxt(k,877) +rxt(k,878))* y(k,92) + (rxt(k,258) +rxt(k,259) +rxt(k,260))* y(k,93) + (rxt(k,2737) +rxt(k,2738) + &
rxt(k,2739))* y(k,94) + (rxt(k,606) +rxt(k,607) +rxt(k,608) +rxt(k,609))* y(k,95) + (rxt(k,622) +rxt(k,623) + &
rxt(k,624) +rxt(k,625))* y(k,96) + (rxt(k,664) +rxt(k,665) +rxt(k,666) +rxt(k,667))* y(k,97) + (rxt(k,784) + &
rxt(k,785) +rxt(k,786))* y(k,98) + (rxt(k,868) +rxt(k,869) +rxt(k,870))* y(k,99) + (rxt(k,2846) +rxt(k,2847) + &
rxt(k,2848))* y(k,100) + (rxt(k,1568) +rxt(k,1569) +rxt(k,1570))* y(k,101) + (rxt(k,689) +rxt(k,690) + &
rxt(k,691) +rxt(k,692))* y(k,102) + (rxt(k,909) +rxt(k,910) +rxt(k,911))* y(k,103) + (rxt(k,668) +rxt(k,669) + &
rxt(k,670))* y(k,104) + (rxt(k,3762) +rxt(k,3763) +rxt(k,3764) +rxt(k,3765))* y(k,105) + (rxt(k,3715) + &
rxt(k,3716) +rxt(k,3717) +rxt(k,3718))* y(k,106) + (rxt(k,936) +rxt(k,937) +rxt(k,938))* y(k,107) &
 + (rxt(k,3590) +rxt(k,3591) +rxt(k,3592) +rxt(k,3593))* y(k,108) + (rxt(k,752) +rxt(k,753) +rxt(k,754)) &
* y(k,109) + (rxt(k,962) +rxt(k,963) +rxt(k,964))* y(k,110) + (rxt(k,879) +rxt(k,880) +rxt(k,881))* y(k,111) &
 + (rxt(k,2682) +rxt(k,2683) +rxt(k,2684))* y(k,112) + (rxt(k,1533) +rxt(k,1534) +rxt(k,1535))* y(k,113) &
 + (rxt(k,267) +rxt(k,268) +rxt(k,269))* y(k,114) + (rxt(k,626) +rxt(k,627) +rxt(k,628))* y(k,115) &
 + (rxt(k,671) +rxt(k,672) +rxt(k,673))* y(k,116) + (rxt(k,794) +rxt(k,795) +rxt(k,796))* y(k,117) &
 + (rxt(k,882) +rxt(k,883) +rxt(k,884))* y(k,118) + (rxt(k,2368) +rxt(k,2369) +rxt(k,2370) +rxt(k,2371)) &
* y(k,119) + (rxt(k,1281) +rxt(k,1282) +rxt(k,1283))* y(k,120) + (rxt(k,3090) +rxt(k,3091) +rxt(k,3092) + &
rxt(k,3093))* y(k,121) + (rxt(k,1313) +rxt(k,1314))* y(k,122) + (rxt(k,1689) +rxt(k,1690))* y(k,123) &
 + (rxt(k,1915) +rxt(k,1916) +rxt(k,1917) +rxt(k,1918))* y(k,124) + (rxt(k,3252) +rxt(k,3253) +rxt(k,3254) + &
rxt(k,3255))* y(k,125) + (rxt(k,3594) +rxt(k,3595) +rxt(k,3596) +rxt(k,3597))* y(k,126) + (rxt(k,1800) + &
rxt(k,1801) +rxt(k,1802))* y(k,127) + (rxt(k,1904) +rxt(k,1905) +rxt(k,1906))* y(k,128) + (rxt(k,773) + &
rxt(k,774) +rxt(k,775) +rxt(k,776))* y(k,129) + (rxt(k,854) +rxt(k,855) +rxt(k,856))* y(k,130) + (rxt(k,991) + &
rxt(k,992) +rxt(k,993))* y(k,131) + (rxt(k,3735) +rxt(k,3736) +rxt(k,3737) +rxt(k,3738))* y(k,132) &
 + (rxt(k,965) +rxt(k,966) +rxt(k,967))* y(k,133) + (rxt(k,830) +rxt(k,831) +rxt(k,832))* y(k,134) &
 + (rxt(k,2811) +rxt(k,2812) +rxt(k,2813))* y(k,135) + (rxt(k,1521) +rxt(k,1522) +rxt(k,1523))* y(k,136) &
 + (rxt(k,629) +rxt(k,630) +rxt(k,631))* y(k,137) + (rxt(k,674) +rxt(k,675) +rxt(k,676))* y(k,138) &
 + (rxt(k,755) +rxt(k,756) +rxt(k,757))* y(k,139) + (rxt(k,833) +rxt(k,834) +rxt(k,835))* y(k,140) &
 + (rxt(k,1590) +rxt(k,1591) +rxt(k,1592))* y(k,141) + (rxt(k,3366) +rxt(k,3367) +rxt(k,3368) +rxt(k,3369)) &
* y(k,142) + (rxt(k,3501) +rxt(k,3502) +rxt(k,3503) +rxt(k,3504))* y(k,143) + (rxt(k,3598) +rxt(k,3599) + &
rxt(k,3600) +rxt(k,3601))* y(k,144) + (rxt(k,857) +rxt(k,858) +rxt(k,859) +rxt(k,860))* y(k,145) + (rxt(k,677) + &
rxt(k,678) +rxt(k,679))* y(k,146) + (rxt(k,797) +rxt(k,798) +rxt(k,799))* y(k,147) + (rxt(k,885) +rxt(k,886) + &
rxt(k,887))* y(k,148) + (rxt(k,3602) +rxt(k,3603) +rxt(k,3604) +rxt(k,3605))* y(k,149) +rxt(k,2884)* y(k,150) &
 + (rxt(k,3646) +rxt(k,3647) +rxt(k,3648))* y(k,151) + (rxt(k,1019) +rxt(k,1020) +rxt(k,1021))* y(k,152) &
 + (rxt(k,791) +rxt(k,792) +rxt(k,793))* y(k,153) + (rxt(k,836) +rxt(k,837) +rxt(k,838))* y(k,154) &
 + (rxt(k,912) +rxt(k,913) +rxt(k,914))* y(k,155) + (rxt(k,3360) +rxt(k,3361) +rxt(k,3362))* y(k,156) &
 + (rxt(k,3363) +rxt(k,3364) +rxt(k,3365))* y(k,157) + (rxt(k,1048) +rxt(k,1049) +rxt(k,1050))* y(k,158) &
 + (rxt(k,4012) +rxt(k,4013) +rxt(k,4014) +rxt(k,4015))* y(k,159) + (rxt(k,4058) +rxt(k,4059) +rxt(k,4060) + &
rxt(k,4061))* y(k,160) + (rxt(k,3308) +rxt(k,3309) +rxt(k,3310) +rxt(k,3311))* y(k,161) + (rxt(k,3185) + &
rxt(k,3186) +rxt(k,3187) +rxt(k,3188))* y(k,162) +rxt(k,3981)* y(k,163) + (rxt(k,2763) +rxt(k,2764) + &
rxt(k,2765) +rxt(k,2766))* y(k,164) + (rxt(k,2909) +rxt(k,2910) +rxt(k,2911))* y(k,165) + (rxt(k,3202) + &
rxt(k,3203) +rxt(k,3204))* y(k,166) + (rxt(k,3293) +rxt(k,3294) +rxt(k,3295) +rxt(k,3296))* y(k,167) &
 + (rxt(k,1291) +rxt(k,1292) +rxt(k,1293))* y(k,168) + (rxt(k,3730) +rxt(k,3731) +rxt(k,3732) +rxt(k,3733) + &
rxt(k,3734))* y(k,169) + (rxt(k,3199) +rxt(k,3200) +rxt(k,3201))* y(k,170) + (rxt(k,3287) +rxt(k,3288) + &
rxt(k,3289))* y(k,171) + (rxt(k,3580) +rxt(k,3581) +rxt(k,3582) +rxt(k,3583))* y(k,172) + (rxt(k,3505) + &
rxt(k,3506) +rxt(k,3507) +rxt(k,3508))* y(k,173) + (rxt(k,3312) +rxt(k,3313) +rxt(k,3314) +rxt(k,3315))* y(k,174) &
 + (rxt(k,3136) +rxt(k,3137) +rxt(k,3138) +rxt(k,3139))* y(k,175) + (rxt(k,1154) +rxt(k,1155) +rxt(k,1156)) &
* y(k,176) + (rxt(k,1302) +rxt(k,1303) +rxt(k,1304))* y(k,177) + (rxt(k,1557) +rxt(k,1558) +rxt(k,1559)) &
* y(k,178) + (rxt(k,2975) +rxt(k,2976) +rxt(k,2977) +rxt(k,2978))* y(k,179) + (rxt(k,1165) +rxt(k,1166)) &
* y(k,180) + (rxt(k,1320) +rxt(k,1321) +rxt(k,1322))* y(k,181) + (rxt(k,1603) +rxt(k,1604) +rxt(k,1605) + &
rxt(k,1606))* y(k,182) + (rxt(k,1836) +rxt(k,1837) +rxt(k,1838) +rxt(k,1839))* y(k,183) + (rxt(k,994) + &
rxt(k,995) +rxt(k,996))* y(k,184) + (rxt(k,888) +rxt(k,889) +rxt(k,890))* y(k,185) + (rxt(k,3074) +rxt(k,3075) + &
rxt(k,3076))* y(k,186) + (rxt(k,3256) +rxt(k,3257) +rxt(k,3258) +rxt(k,3259))* y(k,187) + (rxt(k,3370) + &
rxt(k,3371) +rxt(k,3372) +rxt(k,3373))* y(k,188) + (rxt(k,3606) +rxt(k,3607) +rxt(k,3608) +rxt(k,3609))* y(k,189) &
 + (rxt(k,2777) +rxt(k,2778) +rxt(k,2779) +rxt(k,2780))* y(k,190) + (rxt(k,250) +rxt(k,251) +rxt(k,252)) &
* y(k,191) + (rxt(k,2585) +rxt(k,2586) +rxt(k,2587))* y(k,192) + (rxt(k,603) +rxt(k,604) +rxt(k,605))* y(k,193) &
 + (rxt(k,610) +rxt(k,611) +rxt(k,612))* y(k,194) + (rxt(k,632) +rxt(k,633) +rxt(k,634))* y(k,195) &
 + (rxt(k,680) +rxt(k,681) +rxt(k,682))* y(k,196) + (rxt(k,758) +rxt(k,759) +rxt(k,760))* y(k,197) &
 + (rxt(k,839) +rxt(k,840) +rxt(k,841))* y(k,198) + (rxt(k,2692) +rxt(k,2693) +rxt(k,2694))* y(k,199) &
 + (rxt(k,3067) +rxt(k,3068) +rxt(k,3069))* y(k,200) +rxt(k,2712)* y(k,201) + (rxt(k,1653) +rxt(k,1654) + &
rxt(k,1655))* y(k,202) + (rxt(k,3917) +rxt(k,3918))* y(k,203) + (rxt(k,3027) +rxt(k,3028) +rxt(k,3029) + &
rxt(k,3030))* y(k,204) + (rxt(k,3340) +rxt(k,3341) +rxt(k,3342))* y(k,205) + (rxt(k,2853) +rxt(k,2854) + &
rxt(k,2855))* y(k,206) + (rxt(k,761) +rxt(k,762) +rxt(k,763))* y(k,207) + (rxt(k,1544) +rxt(k,1545) + &
rxt(k,1546) +rxt(k,1547))* y(k,208) + (rxt(k,635) +rxt(k,636) +rxt(k,637) +rxt(k,638))* y(k,209) + (rxt(k,693) + &
rxt(k,694) +rxt(k,695) +rxt(k,696))* y(k,210) + (rxt(k,777) +rxt(k,778) +rxt(k,779) +rxt(k,780))* y(k,211) &
 + (rxt(k,861) +rxt(k,862) +rxt(k,863) +rxt(k,864))* y(k,212) + (rxt(k,1245) +rxt(k,1246) +rxt(k,1247) + &
rxt(k,1248))* y(k,213) + (rxt(k,842) +rxt(k,843) +rxt(k,844))* y(k,214) + (rxt(k,1495) +rxt(k,1496) + &
rxt(k,1497) +rxt(k,1498))* y(k,215) + (rxt(k,1677) +rxt(k,1678) +rxt(k,1679))* y(k,216) + (rxt(k,701) + &
rxt(k,702) +rxt(k,703))* y(k,217) + (rxt(k,803) +rxt(k,804) +rxt(k,805))* y(k,218) + (rxt(k,1971) +rxt(k,1972) + &
rxt(k,1973))* y(k,219) + (rxt(k,3509) +rxt(k,3510) +rxt(k,3511) +rxt(k,3512))* y(k,220) + (rxt(k,3766) + &
rxt(k,3767) +rxt(k,3768) +rxt(k,3769))* y(k,221) + (rxt(k,3696) +rxt(k,3697) +rxt(k,3698) +rxt(k,3699))* y(k,222) &
 + (rxt(k,3719) +rxt(k,3720) +rxt(k,3721) +rxt(k,3722))* y(k,223) + (rxt(k,915) +rxt(k,916) +rxt(k,917)) &
* y(k,224) + (rxt(k,764) +rxt(k,765) +rxt(k,766))* y(k,225) + (rxt(k,3513) +rxt(k,3514) +rxt(k,3515) + &
rxt(k,3516))* y(k,226) + (rxt(k,3610) +rxt(k,3611) +rxt(k,3612) +rxt(k,3613))* y(k,227) + (rxt(k,3723) + &
rxt(k,3724) +rxt(k,3725) +rxt(k,3726))* y(k,228) + (rxt(k,939) +rxt(k,940) +rxt(k,941))* y(k,229) + (rxt(k,942) + &
rxt(k,943) +rxt(k,944))* y(k,230) + (rxt(k,968) +rxt(k,969) +rxt(k,970))* y(k,231) + (rxt(k,3614) +rxt(k,3615) + &
rxt(k,3616) +rxt(k,3617))* y(k,232) + (rxt(k,3649) +rxt(k,3650) +rxt(k,3651) +rxt(k,3652))* y(k,233) &
 + (rxt(k,971) +rxt(k,972) +rxt(k,973))* y(k,234) + (rxt(k,997) +rxt(k,998) +rxt(k,999))* y(k,235) &
 + (rxt(k,1022) +rxt(k,1023) +rxt(k,1024))* y(k,236) + (rxt(k,3490) +rxt(k,3491) +rxt(k,3492) +rxt(k,3493)) &
* y(k,237) + (rxt(k,1000) +rxt(k,1001) +rxt(k,1002))* y(k,238) + (rxt(k,1025) +rxt(k,1026) +rxt(k,1027)) &
* y(k,239) + (rxt(k,2276) +rxt(k,2277) +rxt(k,2278) +rxt(k,2279) +rxt(k,2280))* y(k,240) + (rxt(k,2317) + &
rxt(k,2318) +rxt(k,2319) +rxt(k,2320) +rxt(k,2321))* y(k,241) + (rxt(k,2354) +rxt(k,2355) +rxt(k,2356) + &
rxt(k,2357) +rxt(k,2358))* y(k,242) + (rxt(k,2390) +rxt(k,2391) +rxt(k,2392) +rxt(k,2393) +rxt(k,2394))* y(k,243) &
 + (rxt(k,2422) +rxt(k,2423) +rxt(k,2424) +rxt(k,2425) +rxt(k,2426))* y(k,244) + (rxt(k,2454) +rxt(k,2455) + &
rxt(k,2456) +rxt(k,2457) +rxt(k,2458))* y(k,245) + (rxt(k,2486) +rxt(k,2487) +rxt(k,2488) +rxt(k,2489) + &
rxt(k,2490))* y(k,246) + (rxt(k,2767) +rxt(k,2768) +rxt(k,2769))* y(k,247) + (rxt(k,3205) +rxt(k,3206) + &
rxt(k,3207))* y(k,248) + (rxt(k,2004) +rxt(k,2005))* y(k,249) + (rxt(k,3912) +rxt(k,3913) +rxt(k,3914))* y(k,250) &
 + (rxt(k,704) +rxt(k,705) +rxt(k,706))* y(k,251) + (rxt(k,3739) +rxt(k,3740) +rxt(k,3741))* y(k,252) &
 + (rxt(k,3700) +rxt(k,3701) +rxt(k,3702) +rxt(k,3703))* y(k,253) + (rxt(k,3374) +rxt(k,3375) +rxt(k,3376) + &
rxt(k,3377))* y(k,254) + (rxt(k,3517) +rxt(k,3518) +rxt(k,3519))* y(k,255) + (rxt(k,3618) +rxt(k,3619) + &
rxt(k,3620))* y(k,256) + (rxt(k,1617) +rxt(k,1618) +rxt(k,1619))* y(k,257) + (rxt(k,642) +rxt(k,643) +rxt(k,644)) &
* y(k,258) + (rxt(k,806) +rxt(k,807) +rxt(k,808))* y(k,259) + (rxt(k,2856) +rxt(k,2857) +rxt(k,2858) + &
rxt(k,2859))* y(k,260) + (rxt(k,3653) +rxt(k,3654) +rxt(k,3655))* y(k,261) + (rxt(k,1232) +rxt(k,1233))* y(k,262) &
 + (rxt(k,1142) +rxt(k,1143) +rxt(k,1144) +rxt(k,1145))* y(k,263) + (rxt(k,1256) +rxt(k,1257) +rxt(k,1258) + &
rxt(k,1259))* y(k,264) + (rxt(k,1508) +rxt(k,1509) +rxt(k,1510) +rxt(k,1511))* y(k,265) + (rxt(k,3083) + &
rxt(k,3084) +rxt(k,3085) +rxt(k,3086))* y(k,266) + (rxt(k,1812) +rxt(k,1813) +rxt(k,1814) +rxt(k,1815))* y(k,267) &
 + (rxt(k,2842) +rxt(k,2843) +rxt(k,2844) +rxt(k,2845))* y(k,268) + (rxt(k,1664) +rxt(k,1665) +rxt(k,1666)) &
* y(k,269) + (rxt(k,2741) +rxt(k,2742) +rxt(k,2743) +rxt(k,2744))* y(k,270) + (rxt(k,3260) +rxt(k,3261) + &
rxt(k,3262) +rxt(k,3263))* y(k,271) + (rxt(k,3378) +rxt(k,3379) +rxt(k,3380) +rxt(k,3381))* y(k,272) &
 + (rxt(k,3520) +rxt(k,3521) +rxt(k,3522))* y(k,273) + (rxt(k,918) +rxt(k,919) +rxt(k,920))* y(k,274) &
 + (rxt(k,945) +rxt(k,946) +rxt(k,947))* y(k,275) + (rxt(k,974) +rxt(k,975) +rxt(k,976))* y(k,276) &
 + (rxt(k,1003) +rxt(k,1004) +rxt(k,1005))* y(k,277) + (rxt(k,1028) +rxt(k,1029) +rxt(k,1030))* y(k,278) &
 + (rxt(k,1051) +rxt(k,1052) +rxt(k,1053))* y(k,279) + (rxt(k,613) +rxt(k,614) +rxt(k,615))* y(k,280) &
 + (rxt(k,639) +rxt(k,640) +rxt(k,641))* y(k,281) + (rxt(k,683) +rxt(k,684) +rxt(k,685))* y(k,282) &
 + (rxt(k,800) +rxt(k,801) +rxt(k,802))* y(k,283) + (rxt(k,845) +rxt(k,846) +rxt(k,847))* y(k,284) &
 + (rxt(k,1411) +rxt(k,1412) +rxt(k,1413) +rxt(k,1414))* y(k,285) + (rxt(k,2695) +rxt(k,2696) +rxt(k,2697)) &
* y(k,286) + (rxt(k,2860) +rxt(k,2861) +rxt(k,2862) +rxt(k,2863))* y(k,287) +rxt(k,2713)* y(k,288) &
 + (rxt(k,2993) +rxt(k,2994) +rxt(k,2995) +rxt(k,2996))* y(k,289) + (rxt(k,1628) +rxt(k,1629) +rxt(k,1630)) &
* y(k,290) + (rxt(k,1937) +rxt(k,1938) +rxt(k,1939) +rxt(k,1940))* y(k,291) + (rxt(k,781) +rxt(k,782) + &
rxt(k,783))* y(k,292) + (rxt(k,865) +rxt(k,866) +rxt(k,867))* y(k,293) + (rxt(k,3094) +rxt(k,3095) +rxt(k,3096) + &
rxt(k,3097))* y(k,294) + (rxt(k,3523) +rxt(k,3524) +rxt(k,3525) +rxt(k,3526))* y(k,295) + (rxt(k,3621) + &
rxt(k,3622) +rxt(k,3623) +rxt(k,3624))* y(k,296) + (rxt(k,3625) +rxt(k,3626) +rxt(k,3627) +rxt(k,3628))* y(k,297) &
 + (rxt(k,3656) +rxt(k,3657) +rxt(k,3658) +rxt(k,3659))* y(k,298) + (rxt(k,3742) +rxt(k,3743) +rxt(k,3744) + &
rxt(k,3745))* y(k,299) + (rxt(k,3704) +rxt(k,3705) +rxt(k,3706) +rxt(k,3707))* y(k,300) + (rxt(k,1054) + &
rxt(k,1055) +rxt(k,1056))* y(k,301) + (rxt(k,2281) +rxt(k,2282) +rxt(k,2283) +rxt(k,2284) +rxt(k,2285))* y(k,302) &
 + (rxt(k,2322) +rxt(k,2323) +rxt(k,2324) +rxt(k,2325) +rxt(k,2326))* y(k,303) + (rxt(k,2359) +rxt(k,2360) + &
rxt(k,2361) +rxt(k,2362) +rxt(k,2363))* y(k,304) + (rxt(k,2395) +rxt(k,2396) +rxt(k,2397) +rxt(k,2398) + &
rxt(k,2399))* y(k,305) + (rxt(k,2427) +rxt(k,2428) +rxt(k,2429) +rxt(k,2430) +rxt(k,2431))* y(k,306) &
 + (rxt(k,2459) +rxt(k,2460) +rxt(k,2461) +rxt(k,2462) +rxt(k,2463))* y(k,307) + (rxt(k,2491) +rxt(k,2492) + &
rxt(k,2493) +rxt(k,2494) +rxt(k,2495))* y(k,308) + (rxt(k,3208) +rxt(k,3209) +rxt(k,3210))* y(k,309) &
 + (rxt(k,767) +rxt(k,768) +rxt(k,769))* y(k,310) + (rxt(k,891) +rxt(k,892) +rxt(k,893))* y(k,311) &
 + (rxt(k,1269) +rxt(k,1270) +rxt(k,1271) +rxt(k,1272))* y(k,312) + (rxt(k,3264) +rxt(k,3265) +rxt(k,3266) + &
rxt(k,3267))* y(k,313) + (rxt(k,3382) +rxt(k,3383) +rxt(k,3384) +rxt(k,3385))* y(k,314) + (rxt(k,3527) + &
rxt(k,3528) +rxt(k,3529) +rxt(k,3530))* y(k,315) + (rxt(k,3629) +rxt(k,3630) +rxt(k,3631))* y(k,316) &
 + (rxt(k,1471) +rxt(k,1472) +rxt(k,1473) +rxt(k,1474))* y(k,317) + (rxt(k,921) +rxt(k,922) +rxt(k,923)) &
* y(k,318) + (rxt(k,686) +rxt(k,687) +rxt(k,688))* y(k,319) + (rxt(k,770) +rxt(k,771) +rxt(k,772))* y(k,320) &
 + (rxt(k,848) +rxt(k,849) +rxt(k,850))* y(k,321) + (rxt(k,851) +rxt(k,852) +rxt(k,853))* y(k,322) &
 + (rxt(k,3343) +rxt(k,3344) +rxt(k,3345))* y(k,323) + (rxt(k,2286) +rxt(k,2287) +rxt(k,2288) +rxt(k,2289)) &
* y(k,324) + (rxt(k,2327) +rxt(k,2328) +rxt(k,2329) +rxt(k,2330))* y(k,325) + (rxt(k,2364) +rxt(k,2365) + &
rxt(k,2366) +rxt(k,2367))* y(k,326) + (rxt(k,2400) +rxt(k,2401) +rxt(k,2402) +rxt(k,2403))* y(k,327) &
 + (rxt(k,2432) +rxt(k,2433) +rxt(k,2434) +rxt(k,2435))* y(k,328) + (rxt(k,2464) +rxt(k,2465) +rxt(k,2466) + &
rxt(k,2467))* y(k,329) + (rxt(k,2496) +rxt(k,2497) +rxt(k,2498) +rxt(k,2499))* y(k,330) + (rxt(k,3770) + &
rxt(k,3771) +rxt(k,3772) +rxt(k,3773))* y(k,331) + (rxt(k,3080) +rxt(k,3081) +rxt(k,3082))* y(k,332) &
 + (rxt(k,3386) +rxt(k,3387) +rxt(k,3388) +rxt(k,3389))* y(k,333) + (rxt(k,3531) +rxt(k,3532) +rxt(k,3533) + &
rxt(k,3534))* y(k,334) + (rxt(k,3632) +rxt(k,3633) +rxt(k,3634) +rxt(k,3635))* y(k,335) + (rxt(k,948) + &
rxt(k,949) +rxt(k,950))* y(k,336) + (rxt(k,977) +rxt(k,978) +rxt(k,979))* y(k,337) + (rxt(k,1006) +rxt(k,1007) + &
rxt(k,1008))* y(k,338) + (rxt(k,1031) +rxt(k,1032) +rxt(k,1033))* y(k,339) + (rxt(k,1057) +rxt(k,1058))* y(k,340) &
 +rxt(k,302)* y(k,341) + (rxt(k,2983) +rxt(k,2984) +rxt(k,2985))* y(k,342) + (rxt(k,240) +rxt(k,241))* y(k,343) &
 + (rxt(k,2509) +rxt(k,2510) +rxt(k,2511))* y(k,344) + (rxt(k,3887) +rxt(k,3888))* y(k,345) + (rxt(k,2530) + &
rxt(k,2531) +rxt(k,2532))* y(k,346) + (rxt(k,3049) +rxt(k,3050) +rxt(k,3051))* y(k,347) + (rxt(k,518) + &
rxt(k,519) +rxt(k,520) +rxt(k,521))* y(k,348) + (rxt(k,525) +rxt(k,526) +rxt(k,527) +rxt(k,528))* y(k,349) &
 + (rxt(k,532) +rxt(k,533) +rxt(k,534) +rxt(k,535))* y(k,350) + (rxt(k,1080) +rxt(k,1081))* y(k,351) &
 + (rxt(k,3125) +rxt(k,3126) +rxt(k,3127))* y(k,352) + (rxt(k,4023) +rxt(k,4024) +rxt(k,4025))* y(k,353) &
 + (rxt(k,2720) +rxt(k,2721) +rxt(k,2722) +rxt(k,2723))* y(k,354) + (rxt(k,2015) +rxt(k,2016) +rxt(k,2017) + &
rxt(k,2018))* y(k,355) + (rxt(k,3433) +rxt(k,3434) +rxt(k,3435) +rxt(k,3436))* y(k,356) +rxt(k,516)* y(k,358) &
 + (rxt(k,599) +rxt(k,600) +rxt(k,601) +rxt(k,602))* y(k,359) + (rxt(k,2240) +rxt(k,2241))* y(k,360) &
 + (rxt(k,2949) +rxt(k,2950) +rxt(k,2951) +rxt(k,2952))* y(k,361) + (rxt(k,3273) +rxt(k,3274) +rxt(k,3275) + &
rxt(k,3276))* y(k,362) + (rxt(k,3226) +rxt(k,3227) +rxt(k,3228) +rxt(k,3229))* y(k,363) + (rxt(k,3218) + &
rxt(k,3219) +rxt(k,3220))* y(k,364) + (rxt(k,3151) +rxt(k,3152) +rxt(k,3153))* y(k,365) + (rxt(k,2989) + &
rxt(k,2990) +rxt(k,2991) +rxt(k,2992))* y(k,366) + (rxt(k,3121) +rxt(k,3122) +rxt(k,3123) +rxt(k,3124))* y(k,367) &
 + (rxt(k,2666) +rxt(k,2667) +rxt(k,2668))* y(k,368) + (rxt(k,3055) +rxt(k,3056) +rxt(k,3057) +rxt(k,3058)) &
* y(k,369) + (rxt(k,4107) +rxt(k,4108) +rxt(k,4109) +rxt(k,4110))* y(k,371) + (rxt(k,3059) +rxt(k,3060) + &
rxt(k,3061) +rxt(k,3062))* y(k,372) + (rxt(k,2248) +rxt(k,2249) +rxt(k,2250) +rxt(k,2251))* y(k,373) &
 + (rxt(k,1065) +rxt(k,1066))* y(k,374) + (rxt(k,3481) +rxt(k,3482) +rxt(k,3483))* y(k,375) + (rxt(k,2298) + &
rxt(k,2299) +rxt(k,2300) +rxt(k,2301))* y(k,376) + (rxt(k,2335) +rxt(k,2336) +rxt(k,2337) +rxt(k,2338))* y(k,377) &
 + (rxt(k,2372) +rxt(k,2373) +rxt(k,2374) +rxt(k,2375))* y(k,378) + (rxt(k,3930) +rxt(k,3931))* y(k,379) &
 + (rxt(k,2404) +rxt(k,2405) +rxt(k,2406) +rxt(k,2407))* y(k,380) + (rxt(k,2436) +rxt(k,2437) +rxt(k,2438) + &
rxt(k,2439))* y(k,381) + (rxt(k,2468) +rxt(k,2469) +rxt(k,2470) +rxt(k,2471))* y(k,382) + (rxt(k,2177) + &
rxt(k,2178) +rxt(k,2179) +rxt(k,2180))* y(k,383) + (rxt(k,3876) +rxt(k,3877))* y(k,384) + (rxt(k,1102) + &
rxt(k,1103))* y(k,385) + (rxt(k,1331) +rxt(k,1332) +rxt(k,1333))* y(k,386) + (rxt(k,1641) +rxt(k,1642) + &
rxt(k,1643))* y(k,387) + (rxt(k,331) +rxt(k,332))* y(k,388) + (rxt(k,1175) +rxt(k,1176) +rxt(k,1177))* y(k,389) &
 + (rxt(k,3970) +rxt(k,3971) +rxt(k,3972))* y(k,390) + (rxt(k,1343) +rxt(k,1344) +rxt(k,1345))* y(k,391) &
 + (rxt(k,1698) +rxt(k,1699) +rxt(k,1700))* y(k,392) + (rxt(k,827) +rxt(k,828) +rxt(k,829))* y(k,393) &
 + (rxt(k,2656) +rxt(k,2657) +rxt(k,2658))* y(k,394) + (rxt(k,1353) +rxt(k,1354) +rxt(k,1355))* y(k,395) &
 + (rxt(k,1744) +rxt(k,1745) +rxt(k,1746))* y(k,396) + (rxt(k,3915) +rxt(k,3916))* y(k,397) + (rxt(k,1578) + &
rxt(k,1579) +rxt(k,1580))* y(k,398) + (rxt(k,4053) +rxt(k,4054) +rxt(k,4055))* y(k,399) + (rxt(k,1850) + &
rxt(k,1851) +rxt(k,1852))* y(k,400) + (rxt(k,4028) +rxt(k,4029) +rxt(k,4030))* y(k,401) + (rxt(k,904) + &
rxt(k,905) +rxt(k,906))* y(k,402) + (rxt(k,2834) +rxt(k,2835) +rxt(k,2836) +rxt(k,2837))* y(k,403) &
 + (rxt(k,1983) +rxt(k,1984) +rxt(k,1985))* y(k,404) + (rxt(k,931) +rxt(k,932) +rxt(k,933))* y(k,405) &
 + (rxt(k,3494) +rxt(k,3495) +rxt(k,3496))* y(k,406) + (rxt(k,2770) +rxt(k,2771) +rxt(k,2772))* y(k,407) &
 + (rxt(k,3284) +rxt(k,3285) +rxt(k,3286))* y(k,408) + (rxt(k,958) +rxt(k,959))* y(k,409) + (rxt(k,2972) + &
rxt(k,2973) +rxt(k,2974))* y(k,410) + (rxt(k,987) +rxt(k,988))* y(k,411) + (rxt(k,3071) +rxt(k,3072) + &
rxt(k,3073))* y(k,412) + (rxt(k,1015) +rxt(k,1016))* y(k,413) + (rxt(k,3193) +rxt(k,3194) +rxt(k,3195))* y(k,414) &
 + (rxt(k,1040) +rxt(k,1041))* y(k,415) + (rxt(k,3353) +rxt(k,3354) +rxt(k,3355))* y(k,416) + (rxt(k,3430) + &
rxt(k,3431) +rxt(k,3432))* y(k,417) + (rxt(k,2576) +rxt(k,2577))* y(k,418) + (rxt(k,2714) +rxt(k,2715) + &
rxt(k,2716))* y(k,419) + (rxt(k,2717) +rxt(k,2718) +rxt(k,2719))* y(k,420) + (rxt(k,2885) +rxt(k,2886) + &
rxt(k,2887))* y(k,421) + (rxt(k,2888) +rxt(k,2889) +rxt(k,2890))* y(k,422) +rxt(k,483)* y(k,423) + (rxt(k,490) + &
rxt(k,491))* y(k,425) + (rxt(k,4121) +rxt(k,4122) +rxt(k,4123) +rxt(k,4124))* y(k,426) +rxt(k,3966)* y(k,430) &
 +rxt(k,326)* y(k,432) +rxt(k,3821)* y(k,433) +rxt(k,3819)* y(k,434) +rxt(k,329)* y(k,435) + (rxt(k,3446) + &
rxt(k,3447) +rxt(k,3448) +rxt(k,3449) +rxt(k,3450))* y(k,436) + (rxt(k,4091) +rxt(k,4092) +rxt(k,4093) + &
rxt(k,4094))* y(k,438) + (rxt(k,4041) +rxt(k,4042) +rxt(k,4043))* y(k,439) +rxt(k,327)* y(k,440) + (rxt(k,3878) + &
rxt(k,3879))* y(k,441) + (rxt(k,3884) +rxt(k,3885))* y(k,442) + (rxt(k,4083) +rxt(k,4084) +rxt(k,4085) + &
rxt(k,4086))* y(k,443) + (rxt(k,3854) +rxt(k,3855))* y(k,444) +rxt(k,494)* y(k,445) + (rxt(k,512) +rxt(k,513)) &
* y(k,446) + (rxt(k,2564) +rxt(k,2565) +rxt(k,2566))* y(k,447) +rxt(k,3820)* y(k,448) + (rxt(k,3023) + &
rxt(k,3024) +rxt(k,3025) +rxt(k,3026))* y(k,449) + (rxt(k,245) +rxt(k,246))* y(k,450) + (rxt(k,253) +rxt(k,254)) &
* y(k,451) + (rxt(k,261) +rxt(k,262) +rxt(k,263))* y(k,452) + (rxt(k,270) +rxt(k,271))* y(k,453) + (rxt(k,277) + &
rxt(k,278) +rxt(k,279))* y(k,454) + (rxt(k,289) +rxt(k,290))* y(k,455) + (rxt(k,1435) +rxt(k,1436) +rxt(k,1437)) &
* y(k,456) + (rxt(k,1197) +rxt(k,1198) +rxt(k,1199))* y(k,457) + (rxt(k,3681) +rxt(k,3682) +rxt(k,3683)) &
* y(k,458) + (rxt(k,3042) +rxt(k,3043) +rxt(k,3044) +rxt(k,3045))* y(k,459) + (rxt(k,3178) +rxt(k,3179) + &
rxt(k,3180))* y(k,460) + (rxt(k,3324) +rxt(k,3325) +rxt(k,3326) +rxt(k,3327))* y(k,461) + (rxt(k,3564) + &
rxt(k,3565) +rxt(k,3566) +rxt(k,3567))* y(k,462) + (rxt(k,3774) +rxt(k,3775) +rxt(k,3776) +rxt(k,3777) + &
rxt(k,3778))* y(k,463) + (rxt(k,2757) +rxt(k,2758) +rxt(k,2759))* y(k,464) + (rxt(k,2705) +rxt(k,2706) + &
rxt(k,2707))* y(k,465) + (rxt(k,3676) +rxt(k,3677) +rxt(k,3678) +rxt(k,3679) +rxt(k,3680))* y(k,466) &
 + (rxt(k,3672) +rxt(k,3673) +rxt(k,3674) +rxt(k,3675))* y(k,467) + (rxt(k,3316) +rxt(k,3317) +rxt(k,3318) + &
rxt(k,3319))* y(k,468) + (rxt(k,3568) +rxt(k,3569) +rxt(k,3570) +rxt(k,3571))* y(k,469) + (rxt(k,3035) + &
rxt(k,3036) +rxt(k,3037) +rxt(k,3038))* y(k,470) + (rxt(k,3328) +rxt(k,3329) +rxt(k,3330) +rxt(k,3331))* y(k,471) &
 + (rxt(k,3561) +rxt(k,3562) +rxt(k,3563))* y(k,472) + (rxt(k,3162) +rxt(k,3163) +rxt(k,3164) +rxt(k,3165)) &
* y(k,473) + (rxt(k,2964) +rxt(k,2965) +rxt(k,2966) +rxt(k,2967))* y(k,474) + (rxt(k,3356) +rxt(k,3357) + &
rxt(k,3358) +rxt(k,3359))* y(k,475) + (rxt(k,3077) +rxt(k,3078) +rxt(k,3079))* y(k,476) + (rxt(k,3684) + &
rxt(k,3685) +rxt(k,3686) +rxt(k,3687))* y(k,477) + (rxt(k,3158) +rxt(k,3159) +rxt(k,3160) +rxt(k,3161))* y(k,478) &
 + (rxt(k,2045) +rxt(k,2046) +rxt(k,2047) +rxt(k,2048))* y(k,479) + (rxt(k,4002) +rxt(k,4003) +rxt(k,4004)) &
* y(k,480) + (rxt(k,3829) +rxt(k,3830))* y(k,481) + (rxt(k,2557) +rxt(k,2558))* y(k,482) + (rxt(k,3867) + &
rxt(k,3868))* y(k,483) + (rxt(k,2559) +rxt(k,2560))* y(k,484) + (rxt(k,3320) +rxt(k,3321) +rxt(k,3322) + &
rxt(k,3323))* y(k,485) + (rxt(k,3408) +rxt(k,3409) +rxt(k,3410) +rxt(k,3411))* y(k,486) + (rxt(k,3412) + &
rxt(k,3413) +rxt(k,3414) +rxt(k,3415))* y(k,487) + (rxt(k,3404) +rxt(k,3405) +rxt(k,3406) +rxt(k,3407))* y(k,488) &
 + (rxt(k,3170) +rxt(k,3171) +rxt(k,3172) +rxt(k,3173))* y(k,489) + (rxt(k,3039) +rxt(k,3040) +rxt(k,3041)) &
* y(k,490) + (rxt(k,3166) +rxt(k,3167) +rxt(k,3168) +rxt(k,3169))* y(k,491) + (rxt(k,3154) +rxt(k,3155) + &
rxt(k,3156) +rxt(k,3157))* y(k,492) + (rxt(k,2979) +rxt(k,2980) +rxt(k,2981) +rxt(k,2982))* y(k,493) &
 + (rxt(k,3558) +rxt(k,3559) +rxt(k,3560))* y(k,494) + (rxt(k,3297) +rxt(k,3298) +rxt(k,3299))* y(k,495) &
 + (rxt(k,3221) +rxt(k,3222) +rxt(k,3223) +rxt(k,3224) +rxt(k,3225))* y(k,496) + (rxt(k,3999) +rxt(k,4000) + &
rxt(k,4001))* y(k,497) + (rxt(k,2919) +rxt(k,2920) +rxt(k,2921))* y(k,498) + (rxt(k,4139) +rxt(k,4140) + &
rxt(k,4141))* y(k,499) + (rxt(k,2649) +rxt(k,2650) +rxt(k,2651))* y(k,500) + (rxt(k,2788) +rxt(k,2789) + &
rxt(k,2790))* y(k,501) + (rxt(k,3982) +rxt(k,3983))* y(k,502) + (rxt(k,3837) +rxt(k,3838))* y(k,503) &
 + (rxt(k,318) +rxt(k,319) +rxt(k,320) +rxt(k,321))* y(k,504) + (rxt(k,2937) +rxt(k,2938) +rxt(k,2939) + &
rxt(k,2940))* y(k,505) + (rxt(k,657) +rxt(k,658) +rxt(k,659) +rxt(k,660))* y(k,506) + (rxt(k,741) +rxt(k,742) + &
rxt(k,743))* y(k,507) + (rxt(k,2541) +rxt(k,2542) +rxt(k,2543))* y(k,508) + (rxt(k,3046) +rxt(k,3047) + &
rxt(k,3048))* y(k,509) +rxt(k,2506)* y(k,510) +rxt(k,242)* y(k,511) + (rxt(k,236) +rxt(k,237))* y(k,512) &
 + (rxt(k,2868) +rxt(k,2869) +rxt(k,2870) +rxt(k,2871) +rxt(k,2872))* y(k,513) + (rxt(k,2926) +rxt(k,2927) + &
rxt(k,2928))* y(k,514) + (rxt(k,3865) +rxt(k,3866))* y(k,515) + (rxt(k,2849) +rxt(k,2850) +rxt(k,2851) + &
rxt(k,2852))* y(k,516) + (rxt(k,312) +rxt(k,313) +rxt(k,314))* y(k,517) + (rxt(k,2688) +rxt(k,2689) + &
rxt(k,2690) +rxt(k,2691))* y(k,518) + (rxt(k,298) +rxt(k,299))* y(k,519) + (rxt(k,4031) +rxt(k,4032) + &
rxt(k,4033))* y(k,520) + (rxt(k,4095) +rxt(k,4096) +rxt(k,4097))* y(k,521) + (rxt(k,3845) +rxt(k,3846) + &
rxt(k,3847))* y(k,522) + (rxt(k,2781) +rxt(k,2782) +rxt(k,2783) +rxt(k,2784))* y(k,523) + (rxt(k,4115) + &
rxt(k,4116))* y(k,524) + (rxt(k,2500) +rxt(k,2501))* y(k,525) + (rxt(k,2818) +rxt(k,2819) +rxt(k,2820) + &
rxt(k,2821))* y(k,526) +rxt(k,2563)* y(k,527) + (rxt(k,2603) +rxt(k,2604) +rxt(k,2605))* y(k,528) &
 + (rxt(k,3538) +rxt(k,3539) +rxt(k,3540) +rxt(k,3541) +rxt(k,3542))* y(k,529) +rxt(k,2504)* y(k,530) &
 + (rxt(k,2807) +rxt(k,2808) +rxt(k,2809))* y(k,531) +rxt(k,507)* y(k,532) + (rxt(k,2507) +rxt(k,2508))* y(k,533) &
 + (rxt(k,3427) +rxt(k,3428) +rxt(k,3429))* y(k,534) + (rxt(k,2561) +rxt(k,2562))* y(k,535) +rxt(k,402)* y(k,536) &
 +rxt(k,481)* y(k,537) +rxt(k,489)* y(k,538) +rxt(k,3875)* y(k,539) +rxt(k,3856)* y(k,540) + (rxt(k,3973) + &
rxt(k,3974))* y(k,541) + (rxt(k,4070) +rxt(k,4071))* y(k,542) + (rxt(k,4113) +rxt(k,4114))* y(k,543) &
 + (rxt(k,4111) +rxt(k,4112))* y(k,544) +rxt(k,221)* y(k,546) +rxt(k,396)* y(k,547) +rxt(k,401)* y(k,548) &
 +rxt(k,400)* y(k,549) + (rxt(k,2546) +rxt(k,2547) +rxt(k,2548))* y(k,550) + (rxt(k,2636) +rxt(k,2637) + &
rxt(k,2638))* y(k,552) +rxt(k,392)* y(k,553) + (rxt(k,3006) +rxt(k,3007) +rxt(k,3008) +rxt(k,3009))* y(k,554) &
 + (rxt(k,3461) +rxt(k,3462) +rxt(k,3463) +rxt(k,3464))* y(k,557) + (rxt(k,809) +rxt(k,810) +rxt(k,811)) &
* y(k,558) + (rxt(k,3132) +rxt(k,3133) +rxt(k,3134) +rxt(k,3135))* y(k,562) + (rxt(k,2773) +rxt(k,2774) + &
rxt(k,2775) +rxt(k,2776))* y(k,563) + (rxt(k,3642) +rxt(k,3643) +rxt(k,3644) +rxt(k,3645))* y(k,564) &
 + (rxt(k,3215) +rxt(k,3216) +rxt(k,3217))* y(k,565) + (rxt(k,2941) +rxt(k,2942) +rxt(k,2943) +rxt(k,2944)) &
* y(k,566) + (rxt(k,3109) +rxt(k,3110) +rxt(k,3111) +rxt(k,3112))* y(k,567) + (rxt(k,3268) +rxt(k,3269) + &
rxt(k,3270) +rxt(k,3271) +rxt(k,3272))* y(k,568) + (rxt(k,3239) +rxt(k,3240) +rxt(k,3241) +rxt(k,3242))* y(k,569) &
 + (rxt(k,2663) +rxt(k,2664) +rxt(k,2665))* y(k,570) + (rxt(k,3484) +rxt(k,3485) +rxt(k,3486))* y(k,571) &
 + (rxt(k,2207) +rxt(k,2208) +rxt(k,2209) +rxt(k,2210))* y(k,572) + (rxt(k,303) +rxt(k,304) +rxt(k,305)) &
* y(k,573) + (rxt(k,4151) +rxt(k,4152) +rxt(k,4153))* y(k,574) + (rxt(k,2593) +rxt(k,2594) +rxt(k,2595) + &
rxt(k,2596))* y(k,575) + (rxt(k,3390) +rxt(k,3391) +rxt(k,3392) +rxt(k,3393))* y(k,576) + (rxt(k,2822) + &
rxt(k,2823) +rxt(k,2824) +rxt(k,2825))* y(k,579) + (rxt(k,3896) +rxt(k,3897))* y(k,580) + (rxt(k,616) + &
rxt(k,617) +rxt(k,618))* y(k,581) + (rxt(k,560) +rxt(k,561) +rxt(k,562) +rxt(k,563))* y(k,582) + (rxt(k,2659) + &
rxt(k,2660) +rxt(k,2661) +rxt(k,2662))* y(k,583) + (rxt(k,2922) +rxt(k,2923) +rxt(k,2924) +rxt(k,2925))* y(k,584) &
 + (rxt(k,1093) +rxt(k,1094))* y(k,585) + (rxt(k,584) +rxt(k,585) +rxt(k,586))* y(k,586) + (rxt(k,3469) + &
rxt(k,3470) +rxt(k,3471))* y(k,587) + (rxt(k,3749) +rxt(k,3750) +rxt(k,3751))* y(k,588) + (rxt(k,542) + &
rxt(k,543) +rxt(k,544))* y(k,590) + (rxt(k,2606) +rxt(k,2607) +rxt(k,2608))* y(k,591) + (rxt(k,2708) + &
rxt(k,2709) +rxt(k,2710) +rxt(k,2711))* y(k,592) + (rxt(k,3280) +rxt(k,3281) +rxt(k,3282) +rxt(k,3283))* y(k,593) &
 + (rxt(k,2727) +rxt(k,2728) +rxt(k,2729))* y(k,595) + (rxt(k,2256) +rxt(k,2257) +rxt(k,2258) +rxt(k,2259) + &
rxt(k,2260))* y(k,596) + (rxt(k,2302) +rxt(k,2303) +rxt(k,2304) +rxt(k,2305) +rxt(k,2306))* y(k,597) &
 + (rxt(k,2339) +rxt(k,2340) +rxt(k,2341) +rxt(k,2342) +rxt(k,2343))* y(k,598) + (rxt(k,2376) +rxt(k,2377) + &
rxt(k,2378) +rxt(k,2379))* y(k,599) + (rxt(k,2408) +rxt(k,2409) +rxt(k,2410) +rxt(k,2411))* y(k,600) &
 + (rxt(k,2440) +rxt(k,2441) +rxt(k,2442) +rxt(k,2443))* y(k,601) + (rxt(k,2472) +rxt(k,2473) +rxt(k,2474) + &
rxt(k,2475))* y(k,602) + (rxt(k,2864) +rxt(k,2865) +rxt(k,2866) +rxt(k,2867))* y(k,603) + (rxt(k,4175) + &
rxt(k,4176))* y(k,604) + (rxt(k,2544) +rxt(k,2545))* y(k,605) + (rxt(k,2617) +rxt(k,2618) +rxt(k,2619) + &
rxt(k,2620))* y(k,606) +rxt(k,3886)* y(k,607) + (rxt(k,3810) +rxt(k,3811))* y(k,608) +rxt(k,328)* y(k,609) &
 + (rxt(k,2826) +rxt(k,2827) +rxt(k,2828) +rxt(k,2829))* y(k,610) + (rxt(k,3639) +rxt(k,3640) +rxt(k,3641)) &
* y(k,611) + (rxt(k,3660) +rxt(k,3661) +rxt(k,3662))* y(k,612) + (rxt(k,3727) +rxt(k,3728) +rxt(k,3729)) &
* y(k,613) + (rxt(k,3759) +rxt(k,3760) +rxt(k,3761))* y(k,614) + (rxt(k,2968) +rxt(k,2969) +rxt(k,2970) + &
rxt(k,2971))* y(k,615) + (rxt(k,3140) +rxt(k,3141) +rxt(k,3142) +rxt(k,3143))* y(k,616) + (rxt(k,3290) + &
rxt(k,3291) +rxt(k,3292))* y(k,617) + (rxt(k,3397) +rxt(k,3398) +rxt(k,3399))* y(k,618) + (rxt(k,3555) + &
rxt(k,3556) +rxt(k,3557))* y(k,619) + (rxt(k,619) +rxt(k,620) +rxt(k,621))* y(k,621) + (rxt(k,661) +rxt(k,662) + &
rxt(k,663))* y(k,622) + (rxt(k,2502) +rxt(k,2503))* y(k,623) + (rxt(k,2830) +rxt(k,2831) +rxt(k,2832) + &
rxt(k,2833))* y(k,624) + (rxt(k,306) +rxt(k,307) +rxt(k,308))* y(k,625) + (rxt(k,2701) +rxt(k,2702) + &
rxt(k,2703) +rxt(k,2704))* y(k,626) + (rxt(k,3708) +rxt(k,3709) +rxt(k,3710))* y(k,627) + (rxt(k,2797) + &
rxt(k,2798))* y(k,628) + (rxt(k,3746) +rxt(k,3747) +rxt(k,3748))* y(k,629) + (rxt(k,2331) +rxt(k,2332) + &
rxt(k,2333) +rxt(k,2334))* y(k,630) +rxt(k,3818)* y(k,631) + (rxt(k,3465) +rxt(k,3466) +rxt(k,3467) +rxt(k,3468)) &
* y(k,632) + (rxt(k,2669) +rxt(k,2670) +rxt(k,2671))* y(k,634) + (rxt(k,2685) +rxt(k,2686) +rxt(k,2687)) &
* y(k,635) + (rxt(k,2554) +rxt(k,2555) +rxt(k,2556))* y(k,636) +rxt(k,488)* y(k,637) + (rxt(k,300) +rxt(k,301)) &
* y(k,638) + (rxt(k,4068) +rxt(k,4069))* y(k,639) + (rxt(k,3031) +rxt(k,3032) +rxt(k,3033) +rxt(k,3034)) &
* y(k,640) + (rxt(k,2652) +rxt(k,2653) +rxt(k,2654) +rxt(k,2655))* y(k,641) + (rxt(k,2945) +rxt(k,2946) + &
rxt(k,2947) +rxt(k,2948))* y(k,642) + (rxt(k,2552) +rxt(k,2553))* y(k,643) + (rxt(k,2211) +rxt(k,2212) + &
rxt(k,2213) +rxt(k,2214) +rxt(k,2215))* y(k,644) +rxt(k,510)* y(k,645) + (rxt(k,2895) +rxt(k,2896) +rxt(k,2897) + &
rxt(k,2898))* y(k,646) + (rxt(k,2912) +rxt(k,2913) +rxt(k,2914) +rxt(k,2915))* y(k,647) + (rxt(k,2873) + &
rxt(k,2874) +rxt(k,2875) +rxt(k,2876))* y(k,648) + (rxt(k,2760) +rxt(k,2761) +rxt(k,2762))* y(k,649) +rxt(k,3857) &
* y(k,650) + (rxt(k,3797) +rxt(k,3798) +rxt(k,3799))* y(k,651) + (rxt(k,3784) +rxt(k,3785) +rxt(k,3786)) &
* y(k,652) + (rxt(k,2997) +rxt(k,2998) +rxt(k,2999) +rxt(k,3000))* y(k,653) + (rxt(k,2785) +rxt(k,2786) + &
rxt(k,2787))* y(k,654) + (rxt(k,2899) +rxt(k,2900) +rxt(k,2901) +rxt(k,2902))* y(k,655) + (rxt(k,4117) + &
rxt(k,4118) +rxt(k,4119) +rxt(k,4120))* y(k,656) + (rxt(k,2877) +rxt(k,2878) +rxt(k,2879) +rxt(k,2880))* y(k,657) &
 + (rxt(k,4133) +rxt(k,4134) +rxt(k,4135))* y(k,658) + (rxt(k,2754) +rxt(k,2755) +rxt(k,2756))* y(k,659) &
 + (rxt(k,3990) +rxt(k,3991) +rxt(k,3992))* y(k,660) + (rxt(k,322) +rxt(k,323) +rxt(k,324) +rxt(k,325))* y(k,661) &
 + (rxt(k,315) +rxt(k,316) +rxt(k,317))* y(k,662) + (rxt(k,2903) +rxt(k,2904) +rxt(k,2905))* y(k,663) &
 + (rxt(k,551) +rxt(k,552) +rxt(k,553))* y(k,664) + (rxt(k,2181) +rxt(k,2182) +rxt(k,2183) +rxt(k,2184)) &
* y(k,665) + (rxt(k,2294) +rxt(k,2295) +rxt(k,2296) +rxt(k,2297))* y(k,667) + (rxt(k,3098) +rxt(k,3099) + &
rxt(k,3100) +rxt(k,3101))* y(k,668) + (rxt(k,293) +rxt(k,294))* y(k,669) + (rxt(k,907) +rxt(k,908))* y(k,670) &
 + (rxt(k,934) +rxt(k,935))* y(k,671) + (rxt(k,960) +rxt(k,961))* y(k,672) + (rxt(k,989) +rxt(k,990))* y(k,673) &
 + (rxt(k,1017) +rxt(k,1018))* y(k,674) + (rxt(k,1046) +rxt(k,1047))* y(k,675) + (rxt(k,2203) +rxt(k,2204) + &
rxt(k,2205) +rxt(k,2206))* y(k,676) + (rxt(k,2527) +rxt(k,2528) +rxt(k,2529))* y(k,677) + (rxt(k,247) + &
rxt(k,248) +rxt(k,249))* y(k,678) + (rxt(k,2597) +rxt(k,2598) +rxt(k,2599))* y(k,679) + (rxt(k,255) +rxt(k,256) + &
rxt(k,257))* y(k,680) + (rxt(k,264) +rxt(k,265) +rxt(k,266))* y(k,681) + (rxt(k,3277) +rxt(k,3278) +rxt(k,3279)) &
* y(k,682) + (rxt(k,272) +rxt(k,273))* y(k,683) + (rxt(k,3394) +rxt(k,3395) +rxt(k,3396))* y(k,684) &
 + (rxt(k,280) +rxt(k,281))* y(k,685) + (rxt(k,3535) +rxt(k,3536) +rxt(k,3537))* y(k,686) + (rxt(k,291) + &
rxt(k,292))* y(k,687) + (rxt(k,3636) +rxt(k,3637) +rxt(k,3638))* y(k,688) + (rxt(k,4063) +rxt(k,4064) + &
rxt(k,4065))* y(k,689) +rxt(k,219)* y(k,690) +rxt(k,220)* y(k,691) + (rxt(k,4087) +rxt(k,4088) +rxt(k,4089) + &
rxt(k,4090))* y(k,692) +rxt(k,393)* y(k,693) +rxt(k,515)* y(k,694) +rxt(k,394)* y(k,696) + (rxt(k,2261) + &
rxt(k,2262) +rxt(k,2263) +rxt(k,2264) +rxt(k,2265))* y(k,697) + (rxt(k,2307) +rxt(k,2308) +rxt(k,2309) + &
rxt(k,2310) +rxt(k,2311))* y(k,698) + (rxt(k,2344) +rxt(k,2345) +rxt(k,2346) +rxt(k,2347) +rxt(k,2348))* y(k,699) &
 + (rxt(k,2380) +rxt(k,2381) +rxt(k,2382) +rxt(k,2383) +rxt(k,2384))* y(k,700) + (rxt(k,2412) +rxt(k,2413) + &
rxt(k,2414) +rxt(k,2415) +rxt(k,2416))* y(k,701) + (rxt(k,2444) +rxt(k,2445) +rxt(k,2446) +rxt(k,2447) + &
rxt(k,2448))* y(k,702) + (rxt(k,2476) +rxt(k,2477) +rxt(k,2478) +rxt(k,2479) +rxt(k,2480))* y(k,703) &
 + (rxt(k,2216) +rxt(k,2217) +rxt(k,2218) +rxt(k,2219) +rxt(k,2220))* y(k,704) + (rxt(k,2185) +rxt(k,2186) + &
rxt(k,2187) +rxt(k,2188) +rxt(k,2189))* y(k,706) + (rxt(k,2698) +rxt(k,2699) +rxt(k,2700))* y(k,710) &
 + (rxt(k,2266) +rxt(k,2267) +rxt(k,2268) +rxt(k,2269) +rxt(k,2270))* y(k,711) + (rxt(k,2312) +rxt(k,2313) + &
rxt(k,2314) +rxt(k,2315) +rxt(k,2316))* y(k,712) + (rxt(k,2349) +rxt(k,2350) +rxt(k,2351) +rxt(k,2352) + &
rxt(k,2353))* y(k,713) + (rxt(k,2385) +rxt(k,2386) +rxt(k,2387) +rxt(k,2388) +rxt(k,2389))* y(k,714) &
 + (rxt(k,2417) +rxt(k,2418) +rxt(k,2419) +rxt(k,2420) +rxt(k,2421))* y(k,715) + (rxt(k,2449) +rxt(k,2450) + &
rxt(k,2451) +rxt(k,2452) +rxt(k,2453))* y(k,716) + (rxt(k,2481) +rxt(k,2482) +rxt(k,2483) +rxt(k,2484) + &
rxt(k,2485))* y(k,717) + (rxt(k,4125) +rxt(k,4126) +rxt(k,4127) +rxt(k,4128))* y(k,718) + (rxt(k,2271) + &
rxt(k,2272) +rxt(k,2273) +rxt(k,2274) +rxt(k,2275))* y(k,719) + (rxt(k,4161) +rxt(k,4162) +rxt(k,4163) + &
rxt(k,4164))* y(k,720) + (rxt(k,2221) +rxt(k,2222) +rxt(k,2223) +rxt(k,2224) +rxt(k,2225))* y(k,721) &
 + (rxt(k,3144) +rxt(k,3145) +rxt(k,3146) +rxt(k,3147))* y(k,722) + (rxt(k,3148) +rxt(k,3149) +rxt(k,3150)) &
* y(k,723) + (rxt(k,3019) +rxt(k,3020) +rxt(k,3021) +rxt(k,3022))* y(k,724) + (rxt(k,2881) +rxt(k,2882) + &
rxt(k,2883))* y(k,725) + (rxt(k,2838) +rxt(k,2839) +rxt(k,2840) +rxt(k,2841))* y(k,726) + (rxt(k,3128) + &
rxt(k,3129) +rxt(k,3130) +rxt(k,3131))* y(k,727) + (rxt(k,744) +rxt(k,745) +rxt(k,746) +rxt(k,747))* y(k,728) &
 + (rxt(k,2549) +rxt(k,2550) +rxt(k,2551))* y(k,729) + (rxt(k,3002) +rxt(k,3003) +rxt(k,3004) +rxt(k,3005)) &
* y(k,730) + (rxt(k,572) +rxt(k,573) +rxt(k,574) +rxt(k,575) +rxt(k,576))* y(k,731) + (rxt(k,4177) +rxt(k,4178) + &
rxt(k,4179))* y(k,732) + (rxt(k,4180) +rxt(k,4181) +rxt(k,4182) +rxt(k,4183))* y(k,733) + (rxt(k,4184) + &
rxt(k,4185) +rxt(k,4186) +rxt(k,4187))* y(k,734) + (rxt(k,4188) +rxt(k,4189) +rxt(k,4190))* y(k,735) &
 + (rxt(k,4191) +rxt(k,4192) +rxt(k,4193))* y(k,736) + (rxt(k,2524) +rxt(k,2525))* y(k,737) + (rxt(k,243) + &
rxt(k,244))* y(k,738) + (rxt(k,1072) +rxt(k,1073))* y(k,739) + (rxt(k,2916) +rxt(k,2917) +rxt(k,2918))* y(k,740) &
 + (rxt(k,2520) +rxt(k,2521) +rxt(k,2522) +rxt(k,2523))* y(k,741) + (rxt(k,2986) +rxt(k,2987) +rxt(k,2988)) &
* y(k,742) + (rxt(k,2953) +rxt(k,2954) +rxt(k,2955) +rxt(k,2956))* y(k,743) + (rxt(k,3304) +rxt(k,3305) + &
rxt(k,3306) +rxt(k,3307))* y(k,744) + (rxt(k,4129) +rxt(k,4130))* y(k,745) + (rxt(k,2190) +rxt(k,2191) + &
rxt(k,2192) +rxt(k,2193) +rxt(k,2194))* y(k,746) + (rxt(k,3968) +rxt(k,3969))* y(k,747) + (rxt(k,3944) + &
rxt(k,3945))* y(k,748) + (rxt(k,3975) +rxt(k,3976))* y(k,749) + (rxt(k,3977) +rxt(k,3978))* y(k,750) &
 + (rxt(k,4056) +rxt(k,4057))* y(k,753) + (rxt(k,4072) +rxt(k,4073))* y(k,754) + (rxt(k,499) +rxt(k,500) + &
rxt(k,501))* y(k,755) + (rxt(k,502) +rxt(k,503) +rxt(k,504) +rxt(k,505) +rxt(k,506))* y(k,756) + (rxt(k,485) + &
rxt(k,486))* y(k,757) + (rxt(k,492) +rxt(k,493))* y(k,759) +rxt(k,3965)* y(k,760) + (rxt(k,577) +rxt(k,578) + &
rxt(k,579) +rxt(k,580) +rxt(k,581) +rxt(k,582))* y(k,761) + (rxt(k,4194) +rxt(k,4195) +rxt(k,4196))* y(k,762) &
 + (rxt(k,4197) +rxt(k,4198) +rxt(k,4199) +rxt(k,4200))* y(k,763) + (rxt(k,4201) +rxt(k,4202) +rxt(k,4203) + &
rxt(k,4204))* y(k,764) + (rxt(k,4205) +rxt(k,4206) +rxt(k,4207) +rxt(k,4208))* y(k,765) + (rxt(k,4209) + &
rxt(k,4210) +rxt(k,4211) +rxt(k,4212))* y(k,766) + (rxt(k,4213) +rxt(k,4214) +rxt(k,4215) +rxt(k,4216))* y(k,767) &
 + (rxt(k,495) +rxt(k,496) +rxt(k,497) +rxt(k,498))* y(k,770) + (rxt(k,2059) +rxt(k,2060) +rxt(k,2061))* y(k,771) &
 + (rxt(k,2957) +rxt(k,2958) +rxt(k,2959) +rxt(k,2960))* y(k,772) + (rxt(k,2600) +rxt(k,2601) +rxt(k,2602)) &
* y(k,773) +rxt(k,4105)* y(k,774) +rxt(k,4106)* y(k,775) + (rxt(k,2195) +rxt(k,2196))* y(k,776) +rxt(k,3872) &
* y(k,778) + (rxt(k,3919) +rxt(k,3920))* y(k,779) + (rxt(k,1207) +rxt(k,1208) +rxt(k,1209))* y(k,780) &
 + (rxt(k,1446) +rxt(k,1447) +rxt(k,1448) +rxt(k,1449))* y(k,781) + (rxt(k,1860) +rxt(k,1861) +rxt(k,1862)) &
* y(k,782) + (rxt(k,1872) +rxt(k,1873) +rxt(k,1874))* y(k,783) + (rxt(k,1110) +rxt(k,1111))* y(k,784) &
 + (rxt(k,1389) +rxt(k,1390) +rxt(k,1391))* y(k,785) + (rxt(k,1721) +rxt(k,1722) +rxt(k,1723))* y(k,786) &
 + (rxt(k,1824) +rxt(k,1825) +rxt(k,1826))* y(k,787) + (rxt(k,1186) +rxt(k,1187) +rxt(k,1188))* y(k,788) &
 + (rxt(k,1401) +rxt(k,1402) +rxt(k,1403))* y(k,789) + (rxt(k,1733) +rxt(k,1734) +rxt(k,1735))* y(k,790) &
 + (rxt(k,1883) +rxt(k,1884) +rxt(k,1885))* y(k,791) + (rxt(k,1365) +rxt(k,1366) +rxt(k,1367))* y(k,792) &
 + (rxt(k,1769) +rxt(k,1770) +rxt(k,1771))* y(k,793) + (rxt(k,1709) +rxt(k,1710) +rxt(k,1711))* y(k,794) &
 + (rxt(k,1993) +rxt(k,1994) +rxt(k,1995))* y(k,795) + (rxt(k,1894) +rxt(k,1895) +rxt(k,1896))* y(k,796) &
 + (rxt(k,1950) +rxt(k,1951) +rxt(k,1952))* y(k,797) + (rxt(k,1377) +rxt(k,1378) +rxt(k,1379))* y(k,798) &
 + (rxt(k,1756) +rxt(k,1757) +rxt(k,1758))* y(k,799) + (rxt(k,2093) +rxt(k,2094) +rxt(k,2095))* y(k,800) &
 + (rxt(k,2113) +rxt(k,2114) +rxt(k,2115))* y(k,801) + (rxt(k,2133) +rxt(k,2134) +rxt(k,2135))* y(k,802) &
 + (rxt(k,2153) +rxt(k,2154) +rxt(k,2155))* y(k,803) + (rxt(k,2163) +rxt(k,2164) +rxt(k,2165))* y(k,804) &
 + (rxt(k,2961) +rxt(k,2962) +rxt(k,2963))* y(k,805) + (rxt(k,3984) +rxt(k,3985) +rxt(k,3986))* y(k,806) &
 + (rxt(k,2252) +rxt(k,2253) +rxt(k,2254) +rxt(k,2255))* y(k,807) + (rxt(k,309) +rxt(k,310) +rxt(k,311)) &
* y(k,808) +rxt(k,4132)* y(k,809) + (rxt(k,4074) +rxt(k,4075))* y(k,810) + (rxt(k,3052) +rxt(k,3053) + &
rxt(k,3054))* y(k,811) + (rxt(k,3692) +rxt(k,3693) +rxt(k,3694) +rxt(k,3695))* y(k,812) + (rxt(k,3336) + &
rxt(k,3337) +rxt(k,3338) +rxt(k,3339))* y(k,813) + (rxt(k,3688) +rxt(k,3689) +rxt(k,3690) +rxt(k,3691))* y(k,814) &
 + (rxt(k,3423) +rxt(k,3424) +rxt(k,3425) +rxt(k,3426))* y(k,815) + (rxt(k,2290) +rxt(k,2291) +rxt(k,2292) + &
rxt(k,2293))* y(k,816) + (rxt(k,3667) +rxt(k,3668) +rxt(k,3669) +rxt(k,3670) +rxt(k,3671))* y(k,817) &
 + (rxt(k,3663) +rxt(k,3664) +rxt(k,3665) +rxt(k,3666))* y(k,818) +rxt(k,3946)* y(k,819) + (rxt(k,3572) + &
rxt(k,3573) +rxt(k,3574) +rxt(k,3575))* y(k,820) + (rxt(k,3332) +rxt(k,3333) +rxt(k,3334) +rxt(k,3335))* y(k,821) &
 + (rxt(k,3711) +rxt(k,3712) +rxt(k,3713) +rxt(k,3714))* y(k,822) + (rxt(k,3181) +rxt(k,3182) +rxt(k,3183) + &
rxt(k,3184))* y(k,823) + (rxt(k,3416) +rxt(k,3417) +rxt(k,3418) +rxt(k,3419))* y(k,824) + (rxt(k,2814) + &
rxt(k,2815) +rxt(k,2816) +rxt(k,2817))* y(k,825) + (rxt(k,2578) +rxt(k,2579) +rxt(k,2580))* y(k,826) &
 + (rxt(k,4169) +rxt(k,4170) +rxt(k,4171))* y(k,827) + (rxt(k,2724) +rxt(k,2725) +rxt(k,2726))* y(k,828) &
 + (rxt(k,3889) +rxt(k,3890))* y(k,829) + (rxt(k,3350) +rxt(k,3351) +rxt(k,3352))* y(k,830) + (rxt(k,2518) + &
rxt(k,2519))* y(k,831) + (rxt(k,3300) +rxt(k,3301) +rxt(k,3302) +rxt(k,3303))* y(k,832) + (rxt(k,2173) + &
rxt(k,2174) +rxt(k,2175) +rxt(k,2176))* y(k,833) + (rxt(k,3420) +rxt(k,3421) +rxt(k,3422))* y(k,834) &
 + (rxt(k,3576) +rxt(k,3577) +rxt(k,3578) +rxt(k,3579))* y(k,835) + (rxt(k,3779) +rxt(k,3780) +rxt(k,3781) + &
rxt(k,3782) +rxt(k,3783))* y(k,836) + (rxt(k,2627) +rxt(k,2628) +rxt(k,2629))* y(k,837) + (rxt(k,2030) + &
rxt(k,2031) +rxt(k,2032) +rxt(k,2033))* y(k,879) + (rxt(k,2070) +rxt(k,2071) +rxt(k,2072))* y(k,881) + rxt(k,222) &
 + rxt(k,228) + rxt(k,583) + het_rates(k,10))* y(k,705)
         prod(k,945) = (rxt(k,225)*y(k,548) +2.478*rxt(k,522)*y(k,348) +2.478*rxt(k,529)*y(k,349) +2.826*rxt(k,536)*y(k,350) + &
.624*rxt(k,545)*y(k,590) +.656*rxt(k,554)*y(k,664) +1.140*rxt(k,564)*y(k,582) +1.330*rxt(k,587)*y(k,586) + &
.500*rxt(k,598)*y(k,343) +.320*rxt(k,1067)*y(k,512) +1.050*rxt(k,1074)*y(k,739) +.512*rxt(k,1085)*y(k,52) + &
2.160*rxt(k,1095)*y(k,585) +1.080*rxt(k,1104)*y(k,385) +1.080*rxt(k,1112)*y(k,784) +.240*rxt(k,1120)*y(k,33) + &
.512*rxt(k,1134)*y(k,84) +.512*rxt(k,1146)*y(k,263) +2.880*rxt(k,1157)*y(k,176) +3.448*rxt(k,1167)*y(k,180) + &
1.272*rxt(k,1178)*y(k,389) +1.272*rxt(k,1189)*y(k,788) +.285*rxt(k,1200)*y(k,457) +.796*rxt(k,1210)*y(k,780) + &
.512*rxt(k,1221)*y(k,49) +.512*rxt(k,1238)*y(k,72) +.512*rxt(k,1249)*y(k,213) +.640*rxt(k,1260)*y(k,264) + &
.640*rxt(k,1273)*y(k,312) +2.880*rxt(k,1284)*y(k,120) +2.880*rxt(k,1294)*y(k,168) +2.880*rxt(k,1305)*y(k,177) + &
2.000*rxt(k,1315)*y(k,122) +2.912*rxt(k,1323)*y(k,181) +1.590*rxt(k,1334)*y(k,386) +.285*rxt(k,1346)*y(k,391) + &
4.310*rxt(k,1356)*y(k,395) +4.310*rxt(k,1368)*y(k,792) +1.590*rxt(k,1380)*y(k,798) +1.590*rxt(k,1392)*y(k,785) + &
.285*rxt(k,1404)*y(k,789) +.380*rxt(k,1415)*y(k,285) +2.912*rxt(k,1427)*y(k,81) +.380*rxt(k,1438)*y(k,456) + &
1.115*rxt(k,1450)*y(k,781) +2.912*rxt(k,1463)*y(k,82) +.380*rxt(k,1475)*y(k,317) +.512*rxt(k,1488)*y(k,71) + &
.640*rxt(k,1499)*y(k,215) +.640*rxt(k,1512)*y(k,265) +2.880*rxt(k,1524)*y(k,136) +2.880*rxt(k,1536)*y(k,113) + &
.640*rxt(k,1548)*y(k,208) +2.880*rxt(k,1560)*y(k,178) +2.880*rxt(k,1571)*y(k,101) +1.272*rxt(k,1581)*y(k,398) + &
3.640*rxt(k,1593)*y(k,141) +3.640*rxt(k,1607)*y(k,182) +3.448*rxt(k,1620)*y(k,257) +3.640*rxt(k,1631)*y(k,290) + &
1.590*rxt(k,1644)*y(k,387) +.380*rxt(k,1656)*y(k,202) +3.640*rxt(k,1667)*y(k,269) +4.310*rxt(k,1680)*y(k,216) + &
3.000*rxt(k,1691)*y(k,123) +.380*rxt(k,1701)*y(k,392) +1.272*rxt(k,1712)*y(k,794) +1.590*rxt(k,1724)*y(k,786) + &
.380*rxt(k,1736)*y(k,790) +4.310*rxt(k,1747)*y(k,396) +1.590*rxt(k,1759)*y(k,799) +4.310*rxt(k,1772)*y(k,793) + &
3.000*rxt(k,1784)*y(k,26) +.512*rxt(k,1793)*y(k,83) +2.880*rxt(k,1803)*y(k,127) +2.880*rxt(k,1816)*y(k,267) + &
1.590*rxt(k,1827)*y(k,787) +3.640*rxt(k,1840)*y(k,183) +.380*rxt(k,1853)*y(k,400) +.380*rxt(k,1863)*y(k,782) + &
.380*rxt(k,1875)*y(k,783) +.380*rxt(k,1886)*y(k,791) +.380*rxt(k,1897)*y(k,796) +2.912*rxt(k,1907)*y(k,128) + &
3.000*rxt(k,1919)*y(k,124) +.512*rxt(k,1930)*y(k,67) +.640*rxt(k,1941)*y(k,291) +.380*rxt(k,1953)*y(k,797) + &
.512*rxt(k,1964)*y(k,53) +3.448*rxt(k,1974)*y(k,219) +.380*rxt(k,1986)*y(k,404) +.380*rxt(k,1996)*y(k,795) + &
2.912*rxt(k,2006)*y(k,249) +4.368*rxt(k,2019)*y(k,355) +2.120*rxt(k,2034)*y(k,879) +3.645*rxt(k,2049)*y(k,479) + &
1.212*rxt(k,2062)*y(k,771) +4.540*rxt(k,2073)*y(k,881) +.512*rxt(k,2086)*y(k,55) +.380*rxt(k,2096)*y(k,800) + &
.512*rxt(k,2106)*y(k,56) +.380*rxt(k,2116)*y(k,801) +.512*rxt(k,2126)*y(k,57) +.380*rxt(k,2136)*y(k,802) + &
.512*rxt(k,2146)*y(k,58) +.380*rxt(k,2156)*y(k,803) +.380*rxt(k,2166)*y(k,804) +.660*rxt(k,2512)*y(k,344) + &
.260*rxt(k,2533)*y(k,346) +1.560*rxt(k,2567)*y(k,447) +2.880*rxt(k,2609)*y(k,591) +.160*rxt(k,2621)*y(k,606) + &
.160*rxt(k,2630)*y(k,837) +.488*rxt(k,2639)*y(k,552) +.423*rxt(k,2730)*y(k,595) +.160*rxt(k,2791)*y(k,501) + &
2.880*rxt(k,2799)*y(k,628) +2.880*rxt(k,2929)*y(k,514) +.320*rxt(k,3010)*y(k,554) +.160*rxt(k,3102)*y(k,668) + &
.160*rxt(k,3113)*y(k,567) +2.880*rxt(k,3230)*y(k,363) +2.880*rxt(k,3243)*y(k,569) +3.640*rxt(k,3437)*y(k,356) + &
3.640*rxt(k,3451)*y(k,436) +3.560*rxt(k,3472)*y(k,587) +4.368*rxt(k,3543)*y(k,529) +.160*rxt(k,3584)*y(k,172) + &
.380*rxt(k,3752)*y(k,588) +.475*rxt(k,3787)*y(k,652) +.475*rxt(k,3800)*y(k,651) +2.000*rxt(k,3812)*y(k,608) + &
2.000*rxt(k,3831)*y(k,481) +2.000*rxt(k,3839)*y(k,503) +2.000*rxt(k,3848)*y(k,522) +2.000*rxt(k,3891)*y(k,829) + &
2.000*rxt(k,3898)*y(k,580) +2.000*rxt(k,3907)*y(k,50) +.144*rxt(k,3921)*y(k,779) +.144*rxt(k,3932)*y(k,379) + &
3.000*rxt(k,3993)*y(k,660) +3.000*rxt(k,4005)*y(k,480) +3.000*rxt(k,4016)*y(k,159) +3.000*rxt(k,4034)*y(k,520) + &
2.160*rxt(k,4044)*y(k,439) +3.000*rxt(k,4076)*y(k,810) +3.000*rxt(k,4098)*y(k,521) +3.000*rxt(k,4154)*y(k,574)) &
*y(k,696) + (1.400*rxt(k,240)*y(k,343) +.300*rxt(k,494)*y(k,445) +2.976*rxt(k,495)*y(k,770) + &
2.520*rxt(k,499)*y(k,755) +.695*rxt(k,502)*y(k,756) +.464*rxt(k,599)*y(k,359) +4.000*rxt(k,2018)*y(k,355) + &
1.248*rxt(k,2173)*y(k,833) +.916*rxt(k,2177)*y(k,383) +.956*rxt(k,2181)*y(k,665) +.990*rxt(k,2185)*y(k,706) + &
1.390*rxt(k,2190)*y(k,746) +.752*rxt(k,2203)*y(k,676) +.968*rxt(k,2207)*y(k,572) +1.125*rxt(k,2211)*y(k,644) + &
.875*rxt(k,2216)*y(k,704) +1.295*rxt(k,2221)*y(k,721) +.410*rxt(k,2226)*y(k,14) +1.150*rxt(k,2231)*y(k,16) + &
.644*rxt(k,2236)*y(k,32) +.692*rxt(k,2248)*y(k,373) +1.164*rxt(k,2252)*y(k,807) +1.085*rxt(k,2256)*y(k,596) + &
.825*rxt(k,2261)*y(k,697) +1.215*rxt(k,2266)*y(k,711) +1.325*rxt(k,2271)*y(k,719) +.395*rxt(k,2276)*y(k,240) + &
1.095*rxt(k,2281)*y(k,302) +.632*rxt(k,2286)*y(k,324) +.408*rxt(k,2294)*y(k,667) +.604*rxt(k,2298)*y(k,376) + &
1.010*rxt(k,2302)*y(k,597) +.740*rxt(k,2307)*y(k,698) +1.090*rxt(k,2312)*y(k,712) +.375*rxt(k,2317)*y(k,241) + &
1.045*rxt(k,2322)*y(k,303) +.616*rxt(k,2327)*y(k,325) +.300*rxt(k,2331)*y(k,630) +.532*rxt(k,2335)*y(k,377) + &
.980*rxt(k,2339)*y(k,598) +.710*rxt(k,2344)*y(k,699) +1.050*rxt(k,2349)*y(k,713) +.365*rxt(k,2354)*y(k,242) + &
1.020*rxt(k,2359)*y(k,304) +.608*rxt(k,2364)*y(k,326) +.192*rxt(k,2368)*y(k,119) +.480*rxt(k,2372)*y(k,378) + &
.748*rxt(k,2376)*y(k,599) +.660*rxt(k,2380)*y(k,700) +.970*rxt(k,2385)*y(k,714) +.350*rxt(k,2390)*y(k,243) + &
.980*rxt(k,2395)*y(k,305) +.592*rxt(k,2400)*y(k,327) +.432*rxt(k,2404)*y(k,380) +.712*rxt(k,2408)*y(k,600) + &
.615*rxt(k,2412)*y(k,701) +.910*rxt(k,2417)*y(k,715) +.340*rxt(k,2422)*y(k,244) +.950*rxt(k,2427)*y(k,306) + &
.580*rxt(k,2432)*y(k,328) +.396*rxt(k,2436)*y(k,381) +.680*rxt(k,2440)*y(k,601) +.575*rxt(k,2444)*y(k,702) + &
.855*rxt(k,2449)*y(k,716) +.330*rxt(k,2454)*y(k,245) +.925*rxt(k,2459)*y(k,307) +.572*rxt(k,2464)*y(k,329) + &
.364*rxt(k,2468)*y(k,382) +.652*rxt(k,2472)*y(k,602) +.545*rxt(k,2476)*y(k,703) +.805*rxt(k,2481)*y(k,717) + &
.320*rxt(k,2486)*y(k,246) +.900*rxt(k,2491)*y(k,308) +.560*rxt(k,2496)*y(k,330) +.896*rxt(k,3055)*y(k,369) + &
1.256*rxt(k,3059)*y(k,372) +.910*rxt(k,3221)*y(k,496) +.840*rxt(k,3304)*y(k,744) +.840*rxt(k,3497)*y(k,23) + &
.740*rxt(k,3676)*y(k,466) +.335*rxt(k,3730)*y(k,169) +.490*rxt(k,3774)*y(k,463) +.670*rxt(k,3886)*y(k,607) + &
.670*rxt(k,3981)*y(k,163) +1.340*rxt(k,3982)*y(k,502) +.464*rxt(k,4083)*y(k,443) +.464*rxt(k,4087)*y(k,692) + &
.464*rxt(k,4091)*y(k,438) +.464*rxt(k,4107)*y(k,371) +.812*rxt(k,4117)*y(k,656) +1.344*rxt(k,4121)*y(k,426) + &
1.344*rxt(k,4125)*y(k,718) +.400*rxt(k,4129)*y(k,745) +.500*rxt(k,4132)*y(k,809) +.400*rxt(k,4175)*y(k,604)) &
*y(k,705) + (rxt(k,4417)*y(k,690) +rxt(k,4419)*y(k,693) +rxt(k,4420)*y(k,620) +rxt(k,4421)*y(k,758) + &
rxt(k,4422)*y(k,370) +rxt(k,4423)*y(k,589) +.500*rxt(k,4424)*y(k,633) +.500*rxt(k,4425)*y(k,768) + &
.500*rxt(k,4426)*y(k,769))*y(k,864) + (rxt(k,395)*y(k,690) +1.600*rxt(k,397)*y(k,693) +.880*rxt(k,418)*y(k,620) + &
.880*rxt(k,429)*y(k,758) +.880*rxt(k,446)*y(k,370) +.880*rxt(k,462)*y(k,589))*y(k,548) +.534*rxt(k,38)*y(k,344) &
 +rxt(k,51)*y(k,428) +rxt(k,3967)*y(k,429) +rxt(k,52)*y(k,430) +rxt(k,56)*y(k,445) +rxt(k,71)*y(k,546) &
 +.390*rxt(k,72)*y(k,547) +2.000*rxt(k,73)*y(k,549) +rxt(k,75)*y(k,551) +rxt(k,78)*y(k,553) +rxt(k,86)*y(k,590) &
 +4.000*rxt(k,146)*y(k,755) +2.000*rxt(k,147)*y(k,756) +rxt(k,151)*y(k,760) +rxt(k,173)*y(k,770) &
 +2.000*rxt(k,390)*y(k,838)
         loss(k,659) = ((rxt(k,2185) +rxt(k,2186) +rxt(k,2187) +rxt(k,2188) +rxt(k,2189))* y(k,705))* y(k,706)
         prod(k,659) = 0.
         loss(k,185) = ( + rxt(k,120) + rxt(k,121) + rxt(k,233) + het_rates(k,12))* y(k,707)
         prod(k,185) =rxt(k,232)*y(k,691)*y(k,620)
         loss(k,569) = ( + rxt(k,122) + rxt(k,123) + rxt(k,426) + het_rates(k,13))* y(k,708)
         prod(k,569) =rxt(k,234)*y(k,758)*y(k,691)
         loss(k,278) = ( + rxt(k,124) + rxt(k,125) + rxt(k,444) + het_rates(k,14))* y(k,709)
         prod(k,278) =rxt(k,443)*y(k,691)*y(k,370)
         loss(k,186) = ((rxt(k,2698) +rxt(k,2699) +rxt(k,2700))* y(k,705))* y(k,710)
         prod(k,186) = 0.
         loss(k,682) = ((rxt(k,2266) +rxt(k,2267) +rxt(k,2268) +rxt(k,2269) +rxt(k,2270))* y(k,705))* y(k,711)
         prod(k,682) = 0.
         loss(k,683) = ((rxt(k,2312) +rxt(k,2313) +rxt(k,2314) +rxt(k,2315) +rxt(k,2316))* y(k,705))* y(k,712)
         prod(k,683) = 0.
         loss(k,684) = ((rxt(k,2349) +rxt(k,2350) +rxt(k,2351) +rxt(k,2352) +rxt(k,2353))* y(k,705))* y(k,713)
         prod(k,684) = 0.
         loss(k,661) = ((rxt(k,2385) +rxt(k,2386) +rxt(k,2387) +rxt(k,2388) +rxt(k,2389))* y(k,705))* y(k,714)
         prod(k,661) = 0.
         loss(k,662) = ((rxt(k,2417) +rxt(k,2418) +rxt(k,2419) +rxt(k,2420) +rxt(k,2421))* y(k,705))* y(k,715)
         prod(k,662) = 0.
         loss(k,663) = ((rxt(k,2449) +rxt(k,2450) +rxt(k,2451) +rxt(k,2452) +rxt(k,2453))* y(k,705))* y(k,716)
         prod(k,663) = 0.
         loss(k,664) = ((rxt(k,2481) +rxt(k,2482) +rxt(k,2483) +rxt(k,2484) +rxt(k,2485))* y(k,705))* y(k,717)
         prod(k,664) = 0.
         loss(k,506) = ((rxt(k,4125) +rxt(k,4126) +rxt(k,4127) +rxt(k,4128))* y(k,705))* y(k,718)
         prod(k,506) = 0.
         loss(k,685) = ((rxt(k,2271) +rxt(k,2272) +rxt(k,2273) +rxt(k,2274) +rxt(k,2275))* y(k,705))* y(k,719)
         prod(k,685) = 0.
         loss(k,724) = ((rxt(k,4165) +rxt(k,4166) +rxt(k,4167))* y(k,693) + (rxt(k,4161) +rxt(k,4162) +rxt(k,4163) +rxt(k,4164)) &
* y(k,705))* y(k,720)
         prod(k,724) = 0.
         loss(k,686) = ((rxt(k,2221) +rxt(k,2222) +rxt(k,2223) +rxt(k,2224) +rxt(k,2225))* y(k,705))* y(k,721)
         prod(k,686) = 0.
         loss(k,571) = ((rxt(k,3144) +rxt(k,3145) +rxt(k,3146) +rxt(k,3147))* y(k,705))* y(k,722)
         prod(k,571) = 0.
         loss(k,416) = ((rxt(k,3148) +rxt(k,3149) +rxt(k,3150))* y(k,705))* y(k,723)
         prod(k,416) = 0.
         loss(k,507) = ((rxt(k,3019) +rxt(k,3020) +rxt(k,3021) +rxt(k,3022))* y(k,705))* y(k,724)
         prod(k,507) = 0.
         loss(k,328) = ((rxt(k,2881) +rxt(k,2882) +rxt(k,2883))* y(k,705))* y(k,725)
         prod(k,328) = 0.
         loss(k,573) = ((rxt(k,2838) +rxt(k,2839) +rxt(k,2840) +rxt(k,2841))* y(k,705))* y(k,726)
         prod(k,573) = 0.
         loss(k,630) = ((rxt(k,3128) +rxt(k,3129) +rxt(k,3130) +rxt(k,3131))* y(k,705))* y(k,727)
         prod(k,630) = 0.
         loss(k,508) = ((rxt(k,744) +rxt(k,745) +rxt(k,746) +rxt(k,747))* y(k,705))* y(k,728)
         prod(k,508) = 0.
         loss(k,417) = ((rxt(k,2549) +rxt(k,2550) +rxt(k,2551))* y(k,705))* y(k,729)
         prod(k,417) = 0.
         loss(k,509) = ((rxt(k,3002) +rxt(k,3003) +rxt(k,3004) +rxt(k,3005))* y(k,705))* y(k,730)
         prod(k,509) = 0.
         loss(k,925) = ((rxt(k,572) +rxt(k,573) +rxt(k,574) +rxt(k,575) +rxt(k,576))* y(k,705) + rxt(k,126) + rxt(k,127) &
 + rxt(k,128))* y(k,731)
         prod(k,925) = (2.250*rxt(k,596)*y(k,586) +3.000*rxt(k,1128)*y(k,33) +.760*rxt(k,1206)*y(k,457) + &
.500*rxt(k,1217)*y(k,780) +.500*rxt(k,1228)*y(k,49) +.550*rxt(k,1268)*y(k,264) +.240*rxt(k,1342)*y(k,386) + &
.240*rxt(k,1352)*y(k,391) +.400*rxt(k,1364)*y(k,395) +.400*rxt(k,1376)*y(k,792) +.120*rxt(k,1388)*y(k,798) + &
.240*rxt(k,1400)*y(k,785) +.240*rxt(k,1410)*y(k,789) +rxt(k,1423)*y(k,285) +rxt(k,1434)*y(k,81) + &
rxt(k,1445)*y(k,456) +.750*rxt(k,1459)*y(k,781) +rxt(k,1470)*y(k,82) +rxt(k,1483)*y(k,317) + &
.550*rxt(k,1494)*y(k,71) +.550*rxt(k,1507)*y(k,215) +.550*rxt(k,1520)*y(k,265) +.400*rxt(k,1602)*y(k,141) + &
.400*rxt(k,1616)*y(k,182) +.400*rxt(k,1627)*y(k,257) +.400*rxt(k,1640)*y(k,290) +rxt(k,1652)*y(k,387) + &
rxt(k,1663)*y(k,202) +.400*rxt(k,1676)*y(k,269) +.400*rxt(k,1688)*y(k,216) +rxt(k,1708)*y(k,392) + &
rxt(k,1732)*y(k,786) +rxt(k,1743)*y(k,790) +.400*rxt(k,1755)*y(k,396) +rxt(k,1768)*y(k,799) + &
.400*rxt(k,1780)*y(k,793) +rxt(k,1789)*y(k,26) +.550*rxt(k,1799)*y(k,83) +.600*rxt(k,1823)*y(k,267) + &
rxt(k,1835)*y(k,787) +.400*rxt(k,1849)*y(k,183) +rxt(k,1859)*y(k,400) +.120*rxt(k,1871)*y(k,782) + &
rxt(k,1882)*y(k,783) +rxt(k,1893)*y(k,791) +rxt(k,1903)*y(k,796) +.550*rxt(k,1936)*y(k,67) + &
.550*rxt(k,1949)*y(k,291) +rxt(k,1960)*y(k,797) +.550*rxt(k,1970)*y(k,53) +rxt(k,1982)*y(k,219) + &
rxt(k,1992)*y(k,404) +rxt(k,2003)*y(k,795) +rxt(k,2014)*y(k,249) +rxt(k,2029)*y(k,355) + &
.600*rxt(k,2044)*y(k,879) +rxt(k,2058)*y(k,479) +.600*rxt(k,2069)*y(k,771) +rxt(k,2082)*y(k,881) + &
.550*rxt(k,2092)*y(k,55) +rxt(k,2102)*y(k,800) +.550*rxt(k,2112)*y(k,56) +rxt(k,2122)*y(k,801) + &
.550*rxt(k,2132)*y(k,57) +rxt(k,2142)*y(k,802) +.550*rxt(k,2152)*y(k,58) +rxt(k,2162)*y(k,803) + &
rxt(k,2172)*y(k,804) +rxt(k,2202)*y(k,776) +rxt(k,2247)*y(k,360) +.550*rxt(k,3018)*y(k,554) + &
rxt(k,3445)*y(k,356) +rxt(k,3460)*y(k,436) +rxt(k,3480)*y(k,587) +rxt(k,3554)*y(k,529) + &
.550*rxt(k,3589)*y(k,172) +rxt(k,3758)*y(k,588) +rxt(k,3796)*y(k,652) +rxt(k,3809)*y(k,651))*y(k,839) &
 + (.500*rxt(k,589)*y(k,586) +.375*rxt(k,1122)*y(k,33) +.276*rxt(k,1213)*y(k,780) +1.752*rxt(k,1223)*y(k,49) + &
1.700*rxt(k,1241)*y(k,72) +2.125*rxt(k,1263)*y(k,264) +2.125*rxt(k,1276)*y(k,312) +1.020*rxt(k,1430)*y(k,81) + &
2.190*rxt(k,1454)*y(k,781) +1.052*rxt(k,1465)*y(k,82) +1.700*rxt(k,1491)*y(k,71) +2.125*rxt(k,1502)*y(k,215) + &
2.125*rxt(k,1515)*y(k,265) +1.332*rxt(k,1527)*y(k,136) +1.332*rxt(k,1539)*y(k,113) +1.332*rxt(k,1563)*y(k,178) + &
2.125*rxt(k,1647)*y(k,387) +2.125*rxt(k,1727)*y(k,786) +2.125*rxt(k,1762)*y(k,799) +1.752*rxt(k,1796)*y(k,83) + &
1.332*rxt(k,1818)*y(k,267) +2.125*rxt(k,1830)*y(k,787) +1.275*rxt(k,1843)*y(k,183) +1.700*rxt(k,1889)*y(k,791) + &
1.752*rxt(k,1933)*y(k,67) +1.700*rxt(k,1956)*y(k,797) +1.752*rxt(k,1967)*y(k,53) +1.200*rxt(k,1977)*y(k,219) + &
3.400*rxt(k,1988)*y(k,404) +1.700*rxt(k,1999)*y(k,795) +1.530*rxt(k,2023)*y(k,355) +4.620*rxt(k,2038)*y(k,879) + &
1.315*rxt(k,2052)*y(k,479) +3.320*rxt(k,2065)*y(k,771) +2.095*rxt(k,2076)*y(k,881) +1.752*rxt(k,2089)*y(k,55) + &
3.400*rxt(k,2098)*y(k,800) +1.752*rxt(k,2109)*y(k,56) +3.452*rxt(k,2118)*y(k,801) +1.752*rxt(k,2129)*y(k,57) + &
3.452*rxt(k,2138)*y(k,802) +1.752*rxt(k,2149)*y(k,58) +3.452*rxt(k,2158)*y(k,803) +3.452*rxt(k,2168)*y(k,804) + &
.024*rxt(k,2732)*y(k,595) +1.315*rxt(k,3440)*y(k,356) +1.315*rxt(k,3454)*y(k,436) +1.345*rxt(k,3476)*y(k,587) + &
1.686*rxt(k,3548)*y(k,529) +3.452*rxt(k,3754)*y(k,588) +3.645*rxt(k,3790)*y(k,652) +4.345*rxt(k,3803)*y(k,651)) &
*y(k,696) + (2.520*rxt(k,500)*y(k,755) +.370*rxt(k,503)*y(k,756) +1.645*rxt(k,575)*y(k,731) + &
.048*rxt(k,580)*y(k,761) +1.096*rxt(k,2562)*y(k,535) +2.356*rxt(k,2674)*y(k,35) +2.880*rxt(k,2684)*y(k,112) + &
1.170*rxt(k,2686)*y(k,635) +1.292*rxt(k,2851)*y(k,516) +1.770*rxt(k,2889)*y(k,422) +1.941*rxt(k,2911)*y(k,165) + &
2.596*rxt(k,2914)*y(k,647) +1.677*rxt(k,2974)*y(k,410) +.870*rxt(k,2987)*y(k,742) +1.392*rxt(k,3040)*y(k,490) + &
1.804*rxt(k,3146)*y(k,722) +.660*rxt(k,3153)*y(k,365) +.684*rxt(k,3168)*y(k,491) +1.095*rxt(k,3204)*y(k,166) + &
1.272*rxt(k,3206)*y(k,248) +1.098*rxt(k,3209)*y(k,309) +1.892*rxt(k,3295)*y(k,167) +2.356*rxt(k,3302)*y(k,832) + &
1.398*rxt(k,3352)*y(k,830) +1.653*rxt(k,3365)*y(k,157) +.672*rxt(k,3402)*y(k,51) +.572*rxt(k,3406)*y(k,488) + &
.765*rxt(k,3422)*y(k,834) +1.716*rxt(k,3467)*y(k,632) +.560*rxt(k,3566)*y(k,462) +.428*rxt(k,3578)*y(k,835) + &
1.338*rxt(k,3648)*y(k,151) +3.140*rxt(k,3670)*y(k,817) +.916*rxt(k,3810)*y(k,608) +.486*rxt(k,3829)*y(k,481) + &
.970*rxt(k,3837)*y(k,503) +1.182*rxt(k,3846)*y(k,522) +.926*rxt(k,3896)*y(k,580) +1.593*rxt(k,3905)*y(k,50) + &
.639*rxt(k,3991)*y(k,660) +1.312*rxt(k,4014)*y(k,159) +.708*rxt(k,4032)*y(k,520) +.540*rxt(k,4135)*y(k,658) + &
.615*rxt(k,4140)*y(k,499) +.804*rxt(k,4152)*y(k,574) +.780*rxt(k,4163)*y(k,720) +2.204*rxt(k,4186)*y(k,734) + &
1.704*rxt(k,4190)*y(k,735) +1.578*rxt(k,4193)*y(k,736) +.084*rxt(k,4203)*y(k,764) +.048*rxt(k,4207)*y(k,765) + &
.048*rxt(k,4215)*y(k,767))*y(k,705) + (1.374*rxt(k,3815)*y(k,608) +.732*rxt(k,3834)*y(k,481) + &
1.524*rxt(k,3842)*y(k,503) +1.308*rxt(k,3851)*y(k,522) +1.461*rxt(k,3901)*y(k,580) +1.308*rxt(k,3910)*y(k,50) + &
.807*rxt(k,3997)*y(k,660) +1.041*rxt(k,4020)*y(k,159) +.807*rxt(k,4038)*y(k,520) +rxt(k,4429)*y(k,865))*y(k,693) &
 + (rxt(k,4427)*y(k,690) +rxt(k,4430)*y(k,620) +rxt(k,4431)*y(k,758) +rxt(k,4432)*y(k,370) + &
rxt(k,4433)*y(k,589) +.500*rxt(k,4434)*y(k,633) +.500*rxt(k,4435)*y(k,768) +.500*rxt(k,4436)*y(k,769))*y(k,865) &
 +rxt(k,41)*y(k,349) +1.200*rxt(k,111)*y(k,664) +rxt(k,340)*y(k,695)*y(k,691) +.568*rxt(k,143)*y(k,755) &
 +.950*rxt(k,152)*y(k,761) +rxt(k,159)*y(k,763) +.064*rxt(k,162)*y(k,764)
         loss(k,574) = ((rxt(k,4177) +rxt(k,4178) +rxt(k,4179))* y(k,705) + rxt(k,129) + rxt(k,130))* y(k,732)
         prod(k,574) = 0.
         loss(k,632) = ((rxt(k,4180) +rxt(k,4181) +rxt(k,4182) +rxt(k,4183))* y(k,705) + rxt(k,131) + rxt(k,132) + rxt(k,133)) &
* y(k,733)
         prod(k,632) = 0.
         loss(k,575) = ((rxt(k,4184) +rxt(k,4185) +rxt(k,4186) +rxt(k,4187))* y(k,705) + rxt(k,134) + rxt(k,135))* y(k,734)
         prod(k,575) = 0.
         loss(k,510) = ((rxt(k,4188) +rxt(k,4189) +rxt(k,4190))* y(k,705) + rxt(k,136) + rxt(k,137) + rxt(k,138))* y(k,735)
         prod(k,510) = 0.
         loss(k,511) = ((rxt(k,4191) +rxt(k,4192) +rxt(k,4193))* y(k,705) + rxt(k,139) + rxt(k,140))* y(k,736)
         prod(k,511) = 0.
         loss(k,744) = (rxt(k,2526)* y(k,693) + (rxt(k,2524) +rxt(k,2525))* y(k,705) + rxt(k,141) + rxt(k,142))* y(k,737)
         prod(k,744) =rxt(k,2575)*y(k,839)*y(k,590)
         loss(k,187) = ((rxt(k,243) +rxt(k,244))* y(k,705))* y(k,738)
         prod(k,187) = 0.
         loss(k,787) = ((rxt(k,1077) +rxt(k,1078))* y(k,693) + (rxt(k,1074) +rxt(k,1075) +rxt(k,1076))* y(k,696) + (rxt(k,1072) + &
rxt(k,1073))* y(k,705) +rxt(k,1079)* y(k,839))* y(k,739)
         prod(k,787) = 0.
         loss(k,279) = ((rxt(k,2916) +rxt(k,2917) +rxt(k,2918))* y(k,705))* y(k,740)
         prod(k,279) = 0.
         loss(k,418) = ((rxt(k,2520) +rxt(k,2521) +rxt(k,2522) +rxt(k,2523))* y(k,705))* y(k,741)
         prod(k,418) = 0.
         loss(k,329) = ((rxt(k,2986) +rxt(k,2987) +rxt(k,2988))* y(k,705))* y(k,742)
         prod(k,329) = 0.
         loss(k,576) = ((rxt(k,2953) +rxt(k,2954) +rxt(k,2955) +rxt(k,2956))* y(k,705))* y(k,743)
         prod(k,576) = 0.
         loss(k,577) = ((rxt(k,3304) +rxt(k,3305) +rxt(k,3306) +rxt(k,3307))* y(k,705))* y(k,744)
         prod(k,577) = 0.
         loss(k,138) = ((rxt(k,4129) +rxt(k,4130))* y(k,705))* y(k,745)
         prod(k,138) = 0.
         loss(k,665) = ((rxt(k,2190) +rxt(k,2191) +rxt(k,2192) +rxt(k,2193) +rxt(k,2194))* y(k,705))* y(k,746)
         prod(k,665) = 0.
         loss(k,139) = ((rxt(k,3968) +rxt(k,3969))* y(k,705))* y(k,747)
         prod(k,139) = 0.
         loss(k,140) = ((rxt(k,3944) +rxt(k,3945))* y(k,705))* y(k,748)
         prod(k,140) = 0.
         loss(k,114) = ((rxt(k,3975) +rxt(k,3976))* y(k,705))* y(k,749)
         prod(k,114) = 0.
         loss(k,115) = ((rxt(k,3977) +rxt(k,3978))* y(k,705))* y(k,750)
         prod(k,115) = 0.
         loss(k,188) = (rxt(k,4145)* y(k,691) +rxt(k,4146)* y(k,696))* y(k,751)
         prod(k,188) = 0.
         loss(k,189) = ((rxt(k,4147) +rxt(k,4148))* y(k,691) + (rxt(k,4149) +rxt(k,4150))* y(k,696))* y(k,752)
         prod(k,189) = (rxt(k,4145)*y(k,691) +rxt(k,4146)*y(k,696))*y(k,751)
         loss(k,141) = ((rxt(k,4056) +rxt(k,4057))* y(k,705))* y(k,753)
         prod(k,141) = 0.
         loss(k,142) = ((rxt(k,4072) +rxt(k,4073))* y(k,705))* y(k,754)
         prod(k,142) = 0.
         loss(k,926) = ((rxt(k,499) +rxt(k,500) +rxt(k,501))* y(k,705) + rxt(k,143) + rxt(k,144) + rxt(k,145) + rxt(k,146)) &
* y(k,755)
         prod(k,926) = (rxt(k,4437)*y(k,690) +rxt(k,4439)*y(k,693) +rxt(k,4440)*y(k,620) +rxt(k,4441)*y(k,758) + &
rxt(k,4442)*y(k,370) +rxt(k,4443)*y(k,589) +.500*rxt(k,4444)*y(k,633) +.500*rxt(k,4445)*y(k,768) + &
.500*rxt(k,4446)*y(k,769))*y(k,873)
         loss(k,917) = ((rxt(k,502) +rxt(k,503) +rxt(k,504) +rxt(k,505) +rxt(k,506))* y(k,705) + rxt(k,147) + rxt(k,148)) &
* y(k,756)
         prod(k,917) = (rxt(k,4447)*y(k,690) +rxt(k,4449)*y(k,693) +rxt(k,4450)*y(k,620) +rxt(k,4451)*y(k,758) + &
rxt(k,4452)*y(k,370) +rxt(k,4453)*y(k,589) +.500*rxt(k,4454)*y(k,633) +.500*rxt(k,4455)*y(k,768) + &
.500*rxt(k,4456)*y(k,769))*y(k,874)
         loss(k,920) = (rxt(k,487)* y(k,693) + (rxt(k,485) +rxt(k,486))* y(k,705) + rxt(k,149) + rxt(k,150))* y(k,757)
         prod(k,920) = (2.976*rxt(k,496)*y(k,770) +.715*rxt(k,574)*y(k,731) +.006*rxt(k,579)*y(k,761) + &
2.277*rxt(k,2528)*y(k,677) +.939*rxt(k,2550)*y(k,729) +.834*rxt(k,2555)*y(k,636) +.870*rxt(k,2561)*y(k,535) + &
2.388*rxt(k,2595)*y(k,575) +1.752*rxt(k,2598)*y(k,679) +1.136*rxt(k,2674)*y(k,35) +.825*rxt(k,2677)*y(k,20) + &
2.241*rxt(k,2680)*y(k,45) +.752*rxt(k,2690)*y(k,518) +.483*rxt(k,2693)*y(k,199) +.768*rxt(k,2696)*y(k,286) + &
.879*rxt(k,2706)*y(k,465) +.024*rxt(k,2709)*y(k,592) +.018*rxt(k,2768)*y(k,247) +1.440*rxt(k,2771)*y(k,407) + &
1.936*rxt(k,2774)*y(k,563) +1.876*rxt(k,2779)*y(k,190) +.584*rxt(k,2815)*y(k,825) +.495*rxt(k,2854)*y(k,206) + &
.704*rxt(k,2858)*y(k,260) +.608*rxt(k,2862)*y(k,287) +.472*rxt(k,2878)*y(k,657) +2.874*rxt(k,2882)*y(k,725) + &
1.233*rxt(k,2907)*y(k,61) +.663*rxt(k,2973)*y(k,410) +.492*rxt(k,2990)*y(k,366) +1.860*rxt(k,2995)*y(k,289) + &
.624*rxt(k,3003)*y(k,730) +.396*rxt(k,3037)*y(k,470) +.519*rxt(k,3053)*y(k,811) +1.074*rxt(k,3088)*y(k,63) + &
1.372*rxt(k,3091)*y(k,121) +.495*rxt(k,3149)*y(k,723) +.368*rxt(k,3163)*y(k,473) +.296*rxt(k,3172)*y(k,489) + &
.304*rxt(k,3182)*y(k,823) +.579*rxt(k,3197)*y(k,65) +.906*rxt(k,3200)*y(k,170) +1.248*rxt(k,3212)*y(k,73) + &
.700*rxt(k,3222)*y(k,496) +.748*rxt(k,3294)*y(k,167) +.420*rxt(k,3298)*y(k,495) +.932*rxt(k,3301)*y(k,832) + &
.356*rxt(k,3317)*y(k,468) +.264*rxt(k,3322)*y(k,485) +.268*rxt(k,3334)*y(k,821) +.492*rxt(k,3337)*y(k,813) + &
.256*rxt(k,3417)*y(k,824) +.224*rxt(k,3424)*y(k,815) +.205*rxt(k,3448)*y(k,436) +.780*rxt(k,3485)*y(k,571) + &
.780*rxt(k,3488)*y(k,54) +1.024*rxt(k,3491)*y(k,237) +.378*rxt(k,3559)*y(k,494) +.252*rxt(k,3562)*y(k,472) + &
.252*rxt(k,3573)*y(k,820) +1.248*rxt(k,3665)*y(k,818) +.055*rxt(k,3669)*y(k,817) +.316*rxt(k,3673)*y(k,467) + &
.192*rxt(k,3689)*y(k,814) +.168*rxt(k,3713)*y(k,822) +.273*rxt(k,3846)*y(k,522) +rxt(k,3979)*y(k,34) + &
.201*rxt(k,4003)*y(k,480) +.304*rxt(k,4013)*y(k,159) +.555*rxt(k,4024)*y(k,353) +.240*rxt(k,4032)*y(k,520) + &
1.196*rxt(k,4060)*y(k,160) +.453*rxt(k,4096)*y(k,521) +1.305*rxt(k,4179)*y(k,732) +1.120*rxt(k,4182)*y(k,733) + &
.032*rxt(k,4199)*y(k,763))*y(k,705) + (2.000*rxt(k,1087)*y(k,52) +2.000*rxt(k,1136)*y(k,84) + &
2.000*rxt(k,1148)*y(k,263) +2.000*rxt(k,1180)*y(k,389) +2.000*rxt(k,1191)*y(k,788) +2.625*rxt(k,1202)*y(k,457) + &
2.000*rxt(k,1222)*y(k,49) +2.000*rxt(k,1240)*y(k,72) +2.000*rxt(k,1251)*y(k,213) +2.500*rxt(k,1262)*y(k,264) + &
2.500*rxt(k,1275)*y(k,312) +2.800*rxt(k,1325)*y(k,181) +2.500*rxt(k,1336)*y(k,386) +3.000*rxt(k,1347)*y(k,391) + &
2.500*rxt(k,1382)*y(k,798) +2.500*rxt(k,1394)*y(k,785) +3.000*rxt(k,1405)*y(k,789) +3.500*rxt(k,1417)*y(k,285) + &
3.500*rxt(k,1440)*y(k,456) +2.500*rxt(k,1452)*y(k,781) +3.500*rxt(k,1477)*y(k,317) +2.000*rxt(k,1490)*y(k,71) + &
2.500*rxt(k,1501)*y(k,215) +2.500*rxt(k,1514)*y(k,265) +2.500*rxt(k,1550)*y(k,208) +2.000*rxt(k,1583)*y(k,398) + &
3.500*rxt(k,1595)*y(k,141) +3.500*rxt(k,1609)*y(k,182) +3.500*rxt(k,1634)*y(k,290) +2.500*rxt(k,1646)*y(k,387) + &
4.000*rxt(k,1658)*y(k,202) +3.500*rxt(k,1670)*y(k,269) +4.000*rxt(k,1703)*y(k,392) +2.000*rxt(k,1714)*y(k,794) + &
2.500*rxt(k,1726)*y(k,786) +4.000*rxt(k,1738)*y(k,790) +2.500*rxt(k,1761)*y(k,799) +2.000*rxt(k,1795)*y(k,83) + &
2.500*rxt(k,1829)*y(k,787) +3.500*rxt(k,1842)*y(k,183) +4.000*rxt(k,1855)*y(k,400) +4.000*rxt(k,1865)*y(k,782) + &
4.000*rxt(k,1877)*y(k,783) +4.000*rxt(k,1888)*y(k,791) +4.000*rxt(k,1899)*y(k,796) +2.800*rxt(k,1909)*y(k,128) + &
2.000*rxt(k,1932)*y(k,67) +2.500*rxt(k,1943)*y(k,291) +4.000*rxt(k,1955)*y(k,797) +2.000*rxt(k,1966)*y(k,53) + &
4.000*rxt(k,1988)*y(k,404) +4.000*rxt(k,1998)*y(k,795) +2.000*rxt(k,2088)*y(k,55) +4.000*rxt(k,2098)*y(k,800) + &
2.000*rxt(k,2108)*y(k,56) +4.000*rxt(k,2118)*y(k,801) +2.000*rxt(k,2128)*y(k,57) +4.000*rxt(k,2138)*y(k,802) + &
2.000*rxt(k,2148)*y(k,58) +4.000*rxt(k,2158)*y(k,803) +4.000*rxt(k,2168)*y(k,804) +2.100*rxt(k,2731)*y(k,595) + &
.196*rxt(k,3012)*y(k,554) +3.500*rxt(k,3453)*y(k,436) +3.470*rxt(k,3474)*y(k,587) +2.100*rxt(k,3546)*y(k,529) + &
4.000*rxt(k,3754)*y(k,588) +5.000*rxt(k,3789)*y(k,652) +5.000*rxt(k,3802)*y(k,651))*y(k,696) &
 + (rxt(k,550)*y(k,590) +.450*rxt(k,559)*y(k,664) +.450*rxt(k,1079)*y(k,739) +.450*rxt(k,1092)*y(k,52) + &
.400*rxt(k,1101)*y(k,585) +.450*rxt(k,1141)*y(k,84) +.450*rxt(k,1153)*y(k,263) +.400*rxt(k,1164)*y(k,176) + &
.250*rxt(k,1217)*y(k,780) +.500*rxt(k,1228)*y(k,49) +.450*rxt(k,1244)*y(k,72) +.450*rxt(k,1255)*y(k,213) + &
.450*rxt(k,1268)*y(k,264) +.450*rxt(k,1280)*y(k,312) +.400*rxt(k,1290)*y(k,120) +.400*rxt(k,1301)*y(k,168) + &
.400*rxt(k,1312)*y(k,177) +.250*rxt(k,1459)*y(k,781) +.450*rxt(k,1494)*y(k,71) +.450*rxt(k,1507)*y(k,215) + &
.450*rxt(k,1520)*y(k,265) +.400*rxt(k,1532)*y(k,136) +.400*rxt(k,1543)*y(k,113) +.450*rxt(k,1556)*y(k,208) + &
.400*rxt(k,1567)*y(k,178) +.400*rxt(k,1577)*y(k,101) +.450*rxt(k,1799)*y(k,83) +.400*rxt(k,1811)*y(k,127) + &
.400*rxt(k,1823)*y(k,267) +.450*rxt(k,1936)*y(k,67) +.450*rxt(k,1949)*y(k,291) +.450*rxt(k,1970)*y(k,53) + &
.400*rxt(k,2044)*y(k,879) +.400*rxt(k,2069)*y(k,771) +.450*rxt(k,2092)*y(k,55) +.450*rxt(k,2112)*y(k,56) + &
.450*rxt(k,2132)*y(k,57) +.450*rxt(k,2152)*y(k,58) +rxt(k,2517)*y(k,344) +.450*rxt(k,2537)*y(k,346) + &
.880*rxt(k,2574)*y(k,447) +.400*rxt(k,2616)*y(k,591) +.450*rxt(k,2626)*y(k,606) +.450*rxt(k,2635)*y(k,837) + &
rxt(k,2645)*y(k,552) +.450*rxt(k,2736)*y(k,595) +.450*rxt(k,2796)*y(k,501) +.400*rxt(k,2806)*y(k,628) + &
.400*rxt(k,2936)*y(k,514) +.450*rxt(k,3018)*y(k,554) +.450*rxt(k,3108)*y(k,668) +.450*rxt(k,3120)*y(k,567) + &
.400*rxt(k,3238)*y(k,363) +.400*rxt(k,3251)*y(k,569) +.450*rxt(k,3589)*y(k,172) +.400*rxt(k,4049)*y(k,439)) &
*y(k,839) + (rxt(k,4457)*y(k,690) +rxt(k,4459)*y(k,693) +rxt(k,4460)*y(k,620) +rxt(k,4461)*y(k,758) + &
rxt(k,4462)*y(k,370) +rxt(k,4463)*y(k,589) +.500*rxt(k,4464)*y(k,633) +.500*rxt(k,4465)*y(k,768) + &
.500*rxt(k,4466)*y(k,769))*y(k,867) +rxt(k,3980)*y(k,693)*y(k,34) +.340*rxt(k,143)*y(k,755) +.370*rxt(k,154) &
*y(k,761) +1.323*rxt(k,165)*y(k,765) +rxt(k,173)*y(k,770)
         loss(k,936) = ((rxt(k,455) +rxt(k,456))* y(k,370) + (rxt(k,3958) +rxt(k,3959))* y(k,427) + (rxt(k,429) +rxt(k,430)) &
* y(k,548) + (rxt(k,471) +rxt(k,472))* y(k,589) + (rxt(k,439) +rxt(k,440))* y(k,620) + (rxt(k,433) +rxt(k,434)) &
* y(k,633) + (rxt(k,427) +rxt(k,428))* y(k,690) +rxt(k,234)* y(k,691) + (rxt(k,431) +rxt(k,432))* y(k,693) &
 + 2.*(rxt(k,441) +rxt(k,442))* y(k,758) + (rxt(k,435) +rxt(k,436))* y(k,768) + (rxt(k,437) +rxt(k,438)) &
* y(k,769) + het_rates(k,35))* y(k,758)
         prod(k,936) = (1.930*rxt(k,485)*y(k,757) +.300*rxt(k,507)*y(k,532) +.640*rxt(k,2290)*y(k,816) + &
1.916*rxt(k,2294)*y(k,667) +1.656*rxt(k,2331)*y(k,630) +1.400*rxt(k,2368)*y(k,119) +1.930*rxt(k,2524)*y(k,737) + &
.366*rxt(k,2559)*y(k,484) +2.742*rxt(k,2585)*y(k,192) +2.715*rxt(k,2589)*y(k,59) +.020*rxt(k,2701)*y(k,626) + &
2.922*rxt(k,2737)*y(k,94) +3.368*rxt(k,2741)*y(k,270) +3.392*rxt(k,2749)*y(k,60) +2.772*rxt(k,2807)*y(k,531) + &
.005*rxt(k,2868)*y(k,513) +2.394*rxt(k,2891)*y(k,62) +3.564*rxt(k,2997)*y(k,653) +2.262*rxt(k,3063)*y(k,64) + &
2.307*rxt(k,3067)*y(k,200) +2.142*rxt(k,3189)*y(k,66) +2.784*rxt(k,3461)*y(k,557) +.104*rxt(k,3692)*y(k,812) + &
.660*rxt(k,3982)*y(k,502))*y(k,705) + (rxt(k,487)*y(k,757) +.700*rxt(k,508)*y(k,532) +rxt(k,2526)*y(k,737) + &
rxt(k,2588)*y(k,192) +rxt(k,2592)*y(k,59) +rxt(k,2740)*y(k,94) +3.888*rxt(k,2745)*y(k,270) +rxt(k,2753)*y(k,60) + &
rxt(k,2810)*y(k,531) +rxt(k,2894)*y(k,62) +rxt(k,3001)*y(k,653) +rxt(k,3066)*y(k,64) +rxt(k,3070)*y(k,200) + &
rxt(k,3192)*y(k,66) +rxt(k,4469)*y(k,868))*y(k,693) + (rxt(k,4467)*y(k,690) +rxt(k,4470)*y(k,620) + &
rxt(k,4471)*y(k,758) +rxt(k,4472)*y(k,370) +rxt(k,4473)*y(k,589) +.500*rxt(k,4474)*y(k,633) + &
.500*rxt(k,4475)*y(k,768) +.500*rxt(k,4476)*y(k,769))*y(k,868) + (1.200*rxt(k,123) +rxt(k,426))*y(k,708) &
 + (2.000*rxt(k,4147)*y(k,691) +2.000*rxt(k,4149)*y(k,696))*y(k,752) +2.000*rxt(k,20)*y(k,186) +2.000*rxt(k,59) &
*y(k,464) +2.000*rxt(k,61)*y(k,476) +2.000*rxt(k,76)*y(k,552) +.600*rxt(k,84)*y(k,582) +1.800*rxt(k,128)*y(k,731) &
 +2.000*rxt(k,134)*y(k,734) +3.000*rxt(k,136)*y(k,735) +2.000*rxt(k,139)*y(k,736)
         loss(k,899) = ((rxt(k,492) +rxt(k,493))* y(k,705))* y(k,759)
         prod(k,899) = (1.488*rxt(k,566)*y(k,582) +1.700*rxt(k,1087)*y(k,52) +1.700*rxt(k,1137)*y(k,84) + &
1.700*rxt(k,1149)*y(k,263) +1.700*rxt(k,1180)*y(k,389) +1.700*rxt(k,1191)*y(k,788) +1.700*rxt(k,1252)*y(k,213) + &
1.020*rxt(k,1325)*y(k,181) +2.125*rxt(k,1337)*y(k,386) +2.550*rxt(k,1348)*y(k,391) +2.125*rxt(k,1383)*y(k,798) + &
2.125*rxt(k,1395)*y(k,785) +2.550*rxt(k,1406)*y(k,789) +2.125*rxt(k,1552)*y(k,208) +1.700*rxt(k,1584)*y(k,398) + &
1.275*rxt(k,1596)*y(k,141) +1.275*rxt(k,1610)*y(k,182) +1.275*rxt(k,1634)*y(k,290) +3.400*rxt(k,1659)*y(k,202) + &
1.275*rxt(k,1670)*y(k,269) +3.400*rxt(k,1704)*y(k,392) +1.700*rxt(k,1715)*y(k,794) +3.400*rxt(k,1739)*y(k,790) + &
3.400*rxt(k,1855)*y(k,400) +3.400*rxt(k,1866)*y(k,782) +3.400*rxt(k,1877)*y(k,783) +1.700*rxt(k,1889)*y(k,791) + &
3.400*rxt(k,1899)*y(k,796) +1.020*rxt(k,1910)*y(k,128) +2.125*rxt(k,1944)*y(k,291) +1.700*rxt(k,1955)*y(k,797) + &
1.700*rxt(k,1998)*y(k,795) +1.020*rxt(k,2009)*y(k,249) +.800*rxt(k,2197)*y(k,776) +.800*rxt(k,2242)*y(k,360) + &
.765*rxt(k,2732)*y(k,595) +.768*rxt(k,3547)*y(k,529) +.710*rxt(k,3790)*y(k,652))*y(k,696) &
 + (.800*rxt(k,2557)*y(k,482) +.576*rxt(k,2584)*y(k,18) +.015*rxt(k,2650)*y(k,500) +.800*rxt(k,2654)*y(k,641) + &
1.824*rxt(k,2667)*y(k,368) +2.337*rxt(k,2699)*y(k,710) +2.504*rxt(k,2820)*y(k,526) +.324*rxt(k,2828)*y(k,610) + &
.524*rxt(k,2832)*y(k,624) +.036*rxt(k,2840)*y(k,726) +.236*rxt(k,2844)*y(k,268) +.228*rxt(k,2876)*y(k,648) + &
1.580*rxt(k,2939)*y(k,505) +.884*rxt(k,2947)*y(k,642) +1.700*rxt(k,2955)*y(k,743) +.020*rxt(k,2970)*y(k,615) + &
2.048*rxt(k,3044)*y(k,459) +.045*rxt(k,3048)*y(k,509) +.712*rxt(k,3123)*y(k,367) +1.316*rxt(k,3130)*y(k,727) + &
.004*rxt(k,3142)*y(k,616) +.364*rxt(k,3156)*y(k,492) +.072*rxt(k,3160)*y(k,478) +1.290*rxt(k,3271)*y(k,568) + &
.588*rxt(k,3275)*y(k,362) +.036*rxt(k,3285)*y(k,408) +.012*rxt(k,3325)*y(k,461) +.404*rxt(k,3392)*y(k,576) + &
.004*rxt(k,3666)*y(k,818) +.025*rxt(k,3670)*y(k,817) +1.444*rxt(k,3694)*y(k,812) +.995*rxt(k,3782)*y(k,836)) &
*y(k,705) + (1.120*rxt(k,430)*y(k,758) +1.120*rxt(k,463)*y(k,589))*y(k,548) + (.550*rxt(k,2537)*y(k,346) + &
.600*rxt(k,2616)*y(k,591))*y(k,839)
         loss(k,86) = (rxt(k,3965)* y(k,705) + rxt(k,151))* y(k,760)
         prod(k,86) = 0.
         loss(k,927) = ((rxt(k,577) +rxt(k,578) +rxt(k,579) +rxt(k,580) +rxt(k,581) +rxt(k,582))* y(k,705) + rxt(k,152) &
 + rxt(k,153) + rxt(k,154) + rxt(k,155) + rxt(k,156) + het_rates(k,16))* y(k,761)
         prod(k,927) = (rxt(k,4487)*y(k,690) +rxt(k,4489)*y(k,693) +rxt(k,4490)*y(k,620) +rxt(k,4491)*y(k,758) + &
rxt(k,4492)*y(k,370) +rxt(k,4493)*y(k,589) +.500*rxt(k,4494)*y(k,633) +.500*rxt(k,4495)*y(k,768) + &
.500*rxt(k,4496)*y(k,769))*y(k,869) + (rxt(k,4477)*y(k,690) +rxt(k,4479)*y(k,693) +rxt(k,4480)*y(k,620) + &
rxt(k,4481)*y(k,758) +rxt(k,4482)*y(k,370) +rxt(k,4483)*y(k,589) +.500*rxt(k,4484)*y(k,633) + &
.500*rxt(k,4485)*y(k,768) +.500*rxt(k,4486)*y(k,769))*y(k,877) + (1.134*rxt(k,581)*y(k,761) + &
2.912*rxt(k,4199)*y(k,763) +1.624*rxt(k,4207)*y(k,765))*y(k,705) +rxt(k,476)*y(k,840)*y(k,691)
         loss(k,516) = ((rxt(k,4194) +rxt(k,4195) +rxt(k,4196))* y(k,705) + rxt(k,157) + rxt(k,158) + het_rates(k,17))* y(k,762)
         prod(k,516) = 0.
         loss(k,637) = ((rxt(k,4197) +rxt(k,4198) +rxt(k,4199) +rxt(k,4200))* y(k,705) + rxt(k,159) + het_rates(k,18))* y(k,763)
         prod(k,637) = 0.
         loss(k,691) = ((rxt(k,4201) +rxt(k,4202) +rxt(k,4203) +rxt(k,4204))* y(k,705) + rxt(k,160) + rxt(k,161) + rxt(k,162) &
 + rxt(k,163) + het_rates(k,19))* y(k,764)
         prod(k,691) = 0.
         loss(k,692) = ((rxt(k,4205) +rxt(k,4206) +rxt(k,4207) +rxt(k,4208))* y(k,705) + rxt(k,164) + rxt(k,165) + rxt(k,166) &
 + het_rates(k,20))* y(k,765)
         prod(k,692) = 0.
         loss(k,638) = ((rxt(k,4209) +rxt(k,4210) +rxt(k,4211) +rxt(k,4212))* y(k,705) + rxt(k,167) + rxt(k,168) + rxt(k,169) &
 + rxt(k,170) + het_rates(k,21))* y(k,766)
         prod(k,638) = 0.
         loss(k,517) = ((rxt(k,4213) +rxt(k,4214) +rxt(k,4215) +rxt(k,4216))* y(k,705) + rxt(k,171) + rxt(k,172) &
 + het_rates(k,22))* y(k,767)
         prod(k,517) = 0.
         loss(k,938) = (rxt(k,3954)* y(k,427) +rxt(k,407)* y(k,548) +rxt(k,467)* y(k,589) +rxt(k,423)* y(k,620) +rxt(k,409) &
* y(k,633) +rxt(k,406)* y(k,690) +rxt(k,408)* y(k,693) + (rxt(k,435) +rxt(k,436))* y(k,758) + 2.*rxt(k,410) &
* y(k,768) +rxt(k,415)* y(k,769) + het_rates(k,32))* y(k,768)
         prod(k,938) = (2.000*rxt(k,236)*y(k,512) +rxt(k,242)*y(k,511) +1.930*rxt(k,243)*y(k,738) +3.796*rxt(k,245)*y(k,450) + &
4.002*rxt(k,247)*y(k,678) +3.090*rxt(k,250)*y(k,191) +3.606*rxt(k,253)*y(k,451) +4.515*rxt(k,255)*y(k,680) + &
5.520*rxt(k,258)*y(k,93) +7.314*rxt(k,261)*y(k,452) +4.686*rxt(k,264)*y(k,681) +5.283*rxt(k,267)*y(k,114) + &
2.544*rxt(k,270)*y(k,453) +3.008*rxt(k,272)*y(k,683) +5.316*rxt(k,274)*y(k,86) +6.261*rxt(k,277)*y(k,454) + &
2.864*rxt(k,280)*y(k,685) +5.679*rxt(k,282)*y(k,85) +9.7040014*rxt(k,285)*y(k,88) +3.998*rxt(k,289)*y(k,455) + &
2.758*rxt(k,291)*y(k,687) +2.690*rxt(k,293)*y(k,669) +.100*rxt(k,298)*y(k,519) +2.000*rxt(k,300)*y(k,638) + &
rxt(k,302)*y(k,341) +.138*rxt(k,303)*y(k,573) +2.901*rxt(k,306)*y(k,625) +2.034*rxt(k,309)*y(k,808) + &
2.974*rxt(k,312)*y(k,517) +3.372*rxt(k,315)*y(k,662) +3.812*rxt(k,318)*y(k,504) +6.444*rxt(k,322)*y(k,661) + &
rxt(k,326)*y(k,432) +rxt(k,327)*y(k,440) +rxt(k,328)*y(k,609) +rxt(k,329)*y(k,435) +2.246*rxt(k,331)*y(k,388) + &
rxt(k,333)*y(k,21) +4.000*rxt(k,334)*y(k,3) +2.000*rxt(k,336)*y(k,4) +rxt(k,338)*y(k,8) + &
.070*rxt(k,485)*y(k,757) +.982*rxt(k,490)*y(k,425) +2.000*rxt(k,492)*y(k,759) +1.004*rxt(k,495)*y(k,770) + &
.666*rxt(k,499)*y(k,755) +2.945*rxt(k,502)*y(k,756) +1.600*rxt(k,512)*y(k,446) +2.892*rxt(k,518)*y(k,348) + &
2.892*rxt(k,525)*y(k,349) +2.932*rxt(k,532)*y(k,350) +1.500*rxt(k,542)*y(k,590) +2.925*rxt(k,551)*y(k,664) + &
2.680*rxt(k,560)*y(k,582) +2.365*rxt(k,573)*y(k,731) +5.856*rxt(k,578)*y(k,761) +2.958*rxt(k,584)*y(k,586) + &
1.160*rxt(k,599)*y(k,359) +5.349*rxt(k,603)*y(k,193) +7.540*rxt(k,606)*y(k,95) +5.013*rxt(k,610)*y(k,194) + &
5.499*rxt(k,613)*y(k,280) +7.965*rxt(k,616)*y(k,581) +6.882*rxt(k,619)*y(k,621) +6.476*rxt(k,622)*y(k,96) + &
5.544*rxt(k,626)*y(k,115) +6.534*rxt(k,629)*y(k,137) +4.902*rxt(k,632)*y(k,195) +9.320*rxt(k,635)*y(k,209) + &
5.022*rxt(k,639)*y(k,281) +5.238*rxt(k,642)*y(k,258) +8.6920013*rxt(k,645)*y(k,11) + &
8.5760012*rxt(k,649)*y(k,24) +8.584*rxt(k,653)*y(k,37) +9.0480013*rxt(k,657)*y(k,506) + &
5.211*rxt(k,661)*y(k,622) +5.592*rxt(k,664)*y(k,97) +5.898*rxt(k,668)*y(k,104) +5.457*rxt(k,671)*y(k,116) + &
6.030*rxt(k,674)*y(k,138) +6.534*rxt(k,677)*y(k,146) +4.620*rxt(k,680)*y(k,196) +4.812*rxt(k,683)*y(k,282) + &
4.653*rxt(k,686)*y(k,319) +9.1680012*rxt(k,689)*y(k,102) +8.18*rxt(k,693)*y(k,210) +6.672*rxt(k,697)*y(k,87) + &
5.457*rxt(k,701)*y(k,217) +5.304*rxt(k,704)*y(k,251) +8.1520014*rxt(k,707)*y(k,5) +8.22*rxt(k,711)*y(k,7) + &
6.748*rxt(k,715)*y(k,12) +8.320*rxt(k,719)*y(k,15) +8.016*rxt(k,723)*y(k,17) +8.38*rxt(k,727)*y(k,76) + &
5.502*rxt(k,731)*y(k,25) +6.114*rxt(k,734)*y(k,47) +7.624*rxt(k,737)*y(k,38) +4.839*rxt(k,741)*y(k,507) + &
8.5080013*rxt(k,744)*y(k,728) +7.120*rxt(k,748)*y(k,91) +5.829*rxt(k,752)*y(k,109) +5.313*rxt(k,755)*y(k,139) + &
4.503*rxt(k,758)*y(k,197) +6.288*rxt(k,761)*y(k,207) +6.060*rxt(k,764)*y(k,225) +4.485*rxt(k,767)*y(k,310) + &
4.488*rxt(k,770)*y(k,320) +9.236001*rxt(k,773)*y(k,129) +7.284*rxt(k,777)*y(k,211) +5.721*rxt(k,781)*y(k,292) + &
4.200*rxt(k,784)*y(k,98) +7.876*rxt(k,787)*y(k,89) +5.055*rxt(k,791)*y(k,153) +5.067*rxt(k,794)*y(k,117) + &
5.958*rxt(k,797)*y(k,147) +4.512*rxt(k,800)*y(k,283) +5.343*rxt(k,803)*y(k,218) +4.596*rxt(k,806)*y(k,259) + &
5.703*rxt(k,809)*y(k,558) +7.468*rxt(k,812)*y(k,13) +7.668*rxt(k,816)*y(k,30) +7.696*rxt(k,820)*y(k,6) + &
5.571*rxt(k,824)*y(k,70) +4.281*rxt(k,827)*y(k,393) +5.481*rxt(k,830)*y(k,134) +4.845*rxt(k,833)*y(k,140) + &
4.989*rxt(k,836)*y(k,154) +4.338*rxt(k,839)*y(k,198) +4.947*rxt(k,842)*y(k,214) +4.437*rxt(k,845)*y(k,284) + &
4.341*rxt(k,848)*y(k,321) +4.281*rxt(k,851)*y(k,322) +6.297*rxt(k,854)*y(k,130) +9.144001*rxt(k,857)*y(k,145) + &
6.996*rxt(k,861)*y(k,212) +5.232*rxt(k,865)*y(k,293) +4.206*rxt(k,868)*y(k,99) +7.004*rxt(k,871)*y(k,90) + &
6.628*rxt(k,875)*y(k,92) +5.439*rxt(k,879)*y(k,111) +4.677*rxt(k,882)*y(k,118) +5.289*rxt(k,885)*y(k,148) + &
4.815*rxt(k,888)*y(k,185) +4.359*rxt(k,891)*y(k,311) +4.968*rxt(k,894)*y(k,36) +5.211*rxt(k,897)*y(k,46) + &
6.644*rxt(k,900)*y(k,77) +4.092*rxt(k,904)*y(k,402) +2.648*rxt(k,907)*y(k,670) +5.724*rxt(k,909)*y(k,103) + &
4.707*rxt(k,912)*y(k,155) +5.328*rxt(k,915)*y(k,224) +4.329*rxt(k,918)*y(k,274) +4.317*rxt(k,921)*y(k,318) + &
6.884*rxt(k,924)*y(k,41) +4.710*rxt(k,928)*y(k,69) +3.993*rxt(k,931)*y(k,405) +2.620*rxt(k,934)*y(k,671) + &
5.412*rxt(k,936)*y(k,107) +5.019*rxt(k,939)*y(k,229) +4.719*rxt(k,942)*y(k,230) +4.236*rxt(k,945)*y(k,275) + &
4.173*rxt(k,948)*y(k,336) +6.672*rxt(k,951)*y(k,29) +4.590*rxt(k,955)*y(k,78) +2.690*rxt(k,958)*y(k,409) + &
2.602*rxt(k,960)*y(k,672) +5.577*rxt(k,962)*y(k,110) +5.097*rxt(k,965)*y(k,133) +4.605*rxt(k,968)*y(k,231) + &
4.818*rxt(k,971)*y(k,234) +4.170*rxt(k,974)*y(k,276) +4.131*rxt(k,977)*y(k,337) +6.272*rxt(k,980)*y(k,42) + &
4.428*rxt(k,984)*y(k,74) +2.682*rxt(k,987)*y(k,411) +2.590*rxt(k,989)*y(k,673) +5.610*rxt(k,991)*y(k,131) + &
5.223*rxt(k,994)*y(k,184) +4.428*rxt(k,997)*y(k,235) +4.503*rxt(k,1000)*y(k,238) +4.131*rxt(k,1003)*y(k,277) + &
4.092*rxt(k,1006)*y(k,338) +4.485*rxt(k,1009)*y(k,44) +4.542*rxt(k,1012)*y(k,79) +2.662*rxt(k,1015)*y(k,413) + &
2.582*rxt(k,1017)*y(k,674) +5.061*rxt(k,1019)*y(k,152) +4.401*rxt(k,1022)*y(k,236) +4.410*rxt(k,1025)*y(k,239) + &
4.098*rxt(k,1028)*y(k,278) +4.056*rxt(k,1031)*y(k,339) +4.335*rxt(k,1034)*y(k,31) +4.323*rxt(k,1037)*y(k,75) + &
2.646*rxt(k,1040)*y(k,415) +6.176*rxt(k,1042)*y(k,40) +2.576*rxt(k,1046)*y(k,675) +5.202*rxt(k,1048)*y(k,158) + &
4.071*rxt(k,1051)*y(k,279) +4.329*rxt(k,1054)*y(k,301) +2.710*rxt(k,1057)*y(k,340) +4.254*rxt(k,1059)*y(k,43) + &
4.428*rxt(k,1062)*y(k,80) +2.638*rxt(k,1065)*y(k,374) +1.968*rxt(k,1072)*y(k,739) +2.000*rxt(k,1080)*y(k,351) + &
2.958*rxt(k,1082)*y(k,52) +1.800*rxt(k,1093)*y(k,585) +1.930*rxt(k,1102)*y(k,385) +1.930*rxt(k,1110)*y(k,784) + &
1.922*rxt(k,1115)*y(k,19) +3.567*rxt(k,1117)*y(k,33) +4.372*rxt(k,1130)*y(k,84) +4.528*rxt(k,1142)*y(k,263) + &
2.817*rxt(k,1154)*y(k,176) +1.870*rxt(k,1165)*y(k,180) +2.832*rxt(k,1175)*y(k,389) +2.817*rxt(k,1186)*y(k,788) + &
2.970*rxt(k,1197)*y(k,457) +4.005*rxt(k,1207)*y(k,780) +4.533*rxt(k,1218)*y(k,49) +2.844*rxt(k,1229)*y(k,28) + &
1.870*rxt(k,1232)*y(k,262) +5.368*rxt(k,1234)*y(k,72) +5.820*rxt(k,1245)*y(k,213) +4.816*rxt(k,1256)*y(k,264) + &
4.568*rxt(k,1269)*y(k,312) +2.949*rxt(k,1281)*y(k,120) +2.784*rxt(k,1291)*y(k,168) +2.787*rxt(k,1302)*y(k,177) + &
1.804*rxt(k,1313)*y(k,122) +2.727*rxt(k,1320)*y(k,181) +2.793*rxt(k,1331)*y(k,386) +2.889*rxt(k,1343)*y(k,391) + &
2.724*rxt(k,1353)*y(k,395) +2.724*rxt(k,1365)*y(k,792) +2.880*rxt(k,1377)*y(k,798) +2.781*rxt(k,1389)*y(k,785) + &
2.865*rxt(k,1401)*y(k,789) +3.876*rxt(k,1411)*y(k,285) +2.748*rxt(k,1424)*y(k,81) +2.898*rxt(k,1435)*y(k,456) + &
4.392*rxt(k,1446)*y(k,781) +2.745*rxt(k,1460)*y(k,82) +3.936*rxt(k,1472)*y(k,317) +5.088*rxt(k,1484)*y(k,71) + &
5.220*rxt(k,1495)*y(k,215) +5.540*rxt(k,1508)*y(k,265) +2.733*rxt(k,1521)*y(k,136) +3.192*rxt(k,1533)*y(k,113) + &
8.164*rxt(k,1544)*y(k,208) +2.907*rxt(k,1557)*y(k,178) +4.851*rxt(k,1568)*y(k,101) +2.982*rxt(k,1578)*y(k,398) + &
2.634*rxt(k,1590)*y(k,141) +3.544*rxt(k,1603)*y(k,182) +2.634*rxt(k,1617)*y(k,257) +2.637*rxt(k,1628)*y(k,290) + &
2.712*rxt(k,1641)*y(k,387) +2.763*rxt(k,1653)*y(k,202) +2.628*rxt(k,1664)*y(k,269) +2.616*rxt(k,1677)*y(k,216) + &
1.730*rxt(k,1689)*y(k,123) +2.850*rxt(k,1698)*y(k,392) +2.982*rxt(k,1709)*y(k,794) +2.697*rxt(k,1721)*y(k,786) + &
2.826*rxt(k,1733)*y(k,790) +2.652*rxt(k,1744)*y(k,396) +2.724*rxt(k,1756)*y(k,799) +2.652*rxt(k,1769)*y(k,793) + &
2.610*rxt(k,1781)*y(k,26) +3.723*rxt(k,1790)*y(k,83) +2.550*rxt(k,1800)*y(k,127) +4.176*rxt(k,1812)*y(k,267) + &
2.625*rxt(k,1824)*y(k,787) +3.412*rxt(k,1836)*y(k,183) +3.885*rxt(k,1850)*y(k,400) +2.904*rxt(k,1860)*y(k,782) + &
3.192*rxt(k,1872)*y(k,783) +3.222*rxt(k,1883)*y(k,791) +3.861*rxt(k,1894)*y(k,796) +2.613*rxt(k,1904)*y(k,128) + &
3.372*rxt(k,1915)*y(k,124) +3.597*rxt(k,1927)*y(k,67) +4.864*rxt(k,1937)*y(k,291) +3.240*rxt(k,1950)*y(k,797) + &
3.489*rxt(k,1961)*y(k,53) +2.502*rxt(k,1971)*y(k,219) +3.804*rxt(k,1983)*y(k,404) +3.177*rxt(k,1993)*y(k,795) + &
1.700*rxt(k,2004)*y(k,249) +4.168*rxt(k,2015)*y(k,355) +3.996*rxt(k,2030)*y(k,879) +3.888*rxt(k,2045)*y(k,479) + &
7.947*rxt(k,2059)*y(k,771) +2.310*rxt(k,2070)*y(k,881) +3.414*rxt(k,2083)*y(k,55) +3.123*rxt(k,2093)*y(k,800) + &
3.381*rxt(k,2103)*y(k,56) +3.126*rxt(k,2113)*y(k,801) +3.369*rxt(k,2123)*y(k,57) +3.126*rxt(k,2133)*y(k,802) + &
3.366*rxt(k,2143)*y(k,58) +3.120*rxt(k,2153)*y(k,803) +3.114*rxt(k,2163)*y(k,804) +1.816*rxt(k,2173)*y(k,833) + &
2.180*rxt(k,2177)*y(k,383) +2.080*rxt(k,2181)*y(k,665) +2.770*rxt(k,2185)*y(k,706) +2.435*rxt(k,2190)*y(k,746) + &
1.640*rxt(k,2195)*y(k,776) +2.340*rxt(k,2203)*y(k,676) +2.048*rxt(k,2207)*y(k,572) +2.620*rxt(k,2211)*y(k,644) + &
2.860*rxt(k,2216)*y(k,704) +2.490*rxt(k,2221)*y(k,721) +3.720*rxt(k,2226)*y(k,14) +3.135*rxt(k,2231)*y(k,16) + &
2.680*rxt(k,2236)*y(k,32) +1.616*rxt(k,2240)*y(k,360) +2.364*rxt(k,2248)*y(k,373) +1.744*rxt(k,2252)*y(k,807) + &
2.620*rxt(k,2256)*y(k,596) +2.875*rxt(k,2261)*y(k,697) +2.530*rxt(k,2266)*y(k,711) +2.390*rxt(k,2271)*y(k,719) + &
3.600*rxt(k,2276)*y(k,240) +3.070*rxt(k,2281)*y(k,302) +2.592*rxt(k,2286)*y(k,324) +1.936*rxt(k,2290)*y(k,816) + &
.600*rxt(k,2295)*y(k,667) +2.448*rxt(k,2298)*y(k,376) +2.685*rxt(k,2302)*y(k,597) +2.960*rxt(k,2307)*y(k,698) + &
2.655*rxt(k,2312)*y(k,712) +3.585*rxt(k,2317)*y(k,241) +3.085*rxt(k,2322)*y(k,303) +2.584*rxt(k,2327)*y(k,325) + &
.900*rxt(k,2332)*y(k,630) +2.512*rxt(k,2335)*y(k,377) +2.690*rxt(k,2339)*y(k,598) +2.970*rxt(k,2344)*y(k,699) + &
2.685*rxt(k,2349)*y(k,713) +3.575*rxt(k,2354)*y(k,242) +3.085*rxt(k,2359)*y(k,304) +2.580*rxt(k,2364)*y(k,326) + &
1.200*rxt(k,2369)*y(k,119) +2.568*rxt(k,2372)*y(k,378) +2.192*rxt(k,2376)*y(k,599) +3.030*rxt(k,2380)*y(k,700) + &
2.760*rxt(k,2385)*y(k,714) +3.575*rxt(k,2390)*y(k,243) +3.110*rxt(k,2395)*y(k,305) +2.584*rxt(k,2400)*y(k,327) + &
2.616*rxt(k,2404)*y(k,380) +2.228*rxt(k,2408)*y(k,600) +3.080*rxt(k,2412)*y(k,701) +2.830*rxt(k,2417)*y(k,715) + &
3.590*rxt(k,2422)*y(k,244) +3.135*rxt(k,2427)*y(k,306) +2.596*rxt(k,2432)*y(k,328) +2.656*rxt(k,2436)*y(k,381) + &
2.268*rxt(k,2440)*y(k,601) +3.125*rxt(k,2444)*y(k,702) +2.895*rxt(k,2449)*y(k,716) +3.590*rxt(k,2454)*y(k,245) + &
3.150*rxt(k,2459)*y(k,307) +2.600*rxt(k,2464)*y(k,329) +2.692*rxt(k,2468)*y(k,382) +2.300*rxt(k,2472)*y(k,602) + &
3.165*rxt(k,2476)*y(k,703) +2.950*rxt(k,2481)*y(k,717) +3.595*rxt(k,2486)*y(k,246) +3.170*rxt(k,2491)*y(k,308) + &
2.604*rxt(k,2496)*y(k,330) +4.000*rxt(k,2500)*y(k,525) +2.000*rxt(k,2502)*y(k,623) +.750*rxt(k,2509)*y(k,344) + &
4.000*rxt(k,2518)*y(k,831) +8.8240013*rxt(k,2520)*y(k,741) +.070*rxt(k,2524)*y(k,737) + &
.714*rxt(k,2527)*y(k,677) +3.000*rxt(k,2530)*y(k,346) +3.687*rxt(k,2538)*y(k,39) +3.000*rxt(k,2541)*y(k,508) + &
1.970*rxt(k,2544)*y(k,605) +.102*rxt(k,2546)*y(k,550) +.039*rxt(k,2549)*y(k,729) +2.000*rxt(k,2552)*y(k,643) + &
2.166*rxt(k,2554)*y(k,636) +2.000*rxt(k,2557)*y(k,482) +1.671*rxt(k,2564)*y(k,447) +3.564*rxt(k,2576)*y(k,418) + &
5.829*rxt(k,2578)*y(k,826) +10.160*rxt(k,2581)*y(k,18) +.279*rxt(k,2585)*y(k,192) +.309*rxt(k,2589)*y(k,59) + &
1.612*rxt(k,2593)*y(k,575) +1.410*rxt(k,2597)*y(k,679) +.495*rxt(k,2600)*y(k,773) +5.580*rxt(k,2603)*y(k,528) + &
2.883*rxt(k,2606)*y(k,591) +4.132*rxt(k,2617)*y(k,606) +2.883*rxt(k,2627)*y(k,837) +1.788*rxt(k,2636)*y(k,552) + &
5.841*rxt(k,2646)*y(k,48) +2.898*rxt(k,2649)*y(k,500) +4.180*rxt(k,2652)*y(k,641) +2.883*rxt(k,2656)*y(k,394) + &
3.924*rxt(k,2659)*y(k,583) +3.552*rxt(k,2663)*y(k,570) +3.255*rxt(k,2666)*y(k,368) +2.883*rxt(k,2669)*y(k,634) + &
.488*rxt(k,2672)*y(k,35) +.243*rxt(k,2676)*y(k,20) +.729*rxt(k,2679)*y(k,45) +.057*rxt(k,2682)*y(k,112) + &
1.800*rxt(k,2685)*y(k,635) +3.168*rxt(k,2688)*y(k,518) +2.418*rxt(k,2692)*y(k,199) +3.855*rxt(k,2695)*y(k,286) + &
6.516*rxt(k,2698)*y(k,710) +.612*rxt(k,2701)*y(k,626) +2.037*rxt(k,2705)*y(k,465) +.460*rxt(k,2708)*y(k,592) + &
5.091*rxt(k,2714)*y(k,419) +1.686*rxt(k,2717)*y(k,420) +8.148*rxt(k,2720)*y(k,354) +7.290*rxt(k,2724)*y(k,828) + &
2.805*rxt(k,2727)*y(k,595) +.141*rxt(k,2737)*y(k,94) +.964*rxt(k,2742)*y(k,270) +.676*rxt(k,2749)*y(k,60) + &
4.578*rxt(k,2754)*y(k,659) +2.886*rxt(k,2757)*y(k,464) +3.609*rxt(k,2760)*y(k,649) +1.340*rxt(k,2763)*y(k,164) + &
.660*rxt(k,2767)*y(k,247) +1.770*rxt(k,2770)*y(k,407) +3.176*rxt(k,2773)*y(k,563) +2.548*rxt(k,2777)*y(k,190) + &
3.784*rxt(k,2781)*y(k,523) +5.109*rxt(k,2785)*y(k,654) +4.059*rxt(k,2788)*y(k,501) +1.870*rxt(k,2797)*y(k,628) + &
.273*rxt(k,2807)*y(k,531) +2.805*rxt(k,2811)*y(k,135) +4.368*rxt(k,2814)*y(k,825) +4.152*rxt(k,2818)*y(k,526) + &
4.196*rxt(k,2822)*y(k,579) +4.344*rxt(k,2826)*y(k,610) +4.580*rxt(k,2830)*y(k,624) +4.712*rxt(k,2834)*y(k,403) + &
3.952*rxt(k,2838)*y(k,726) +6.988*rxt(k,2842)*y(k,268) +3.153*rxt(k,2846)*y(k,100) +2.532*rxt(k,2849)*y(k,516) + &
3.564*rxt(k,2853)*y(k,206) +4.724*rxt(k,2856)*y(k,260) +6.064*rxt(k,2860)*y(k,287) +6.668*rxt(k,2864)*y(k,603) + &
2.260*rxt(k,2869)*y(k,513) +3.808*rxt(k,2873)*y(k,648) +5.576*rxt(k,2877)*y(k,657) +.120*rxt(k,2881)*y(k,725) + &
3.324*rxt(k,2885)*y(k,421) +1.518*rxt(k,2888)*y(k,422) +.798*rxt(k,2891)*y(k,62) +6.868*rxt(k,2895)*y(k,646) + &
5.752*rxt(k,2899)*y(k,655) +5.025*rxt(k,2903)*y(k,663) +2.442*rxt(k,2906)*y(k,61) +1.245*rxt(k,2909)*y(k,165) + &
1.928*rxt(k,2912)*y(k,647) +5.565*rxt(k,2916)*y(k,740) +4.413*rxt(k,2919)*y(k,498) +3.676*rxt(k,2922)*y(k,584) + &
2.907*rxt(k,2926)*y(k,514) +4.160*rxt(k,2937)*y(k,505) +6.844*rxt(k,2941)*y(k,566) +5.920*rxt(k,2945)*y(k,642) + &
4.812*rxt(k,2949)*y(k,361) +3.992*rxt(k,2953)*y(k,743) +7.096*rxt(k,2957)*y(k,772) +4.671*rxt(k,2961)*y(k,805) + &
3.656*rxt(k,2964)*y(k,474) +5.500*rxt(k,2968)*y(k,615) +.600*rxt(k,2972)*y(k,410) +.780*rxt(k,2975)*y(k,179) + &
6.896*rxt(k,2979)*y(k,493) +2.706*rxt(k,2983)*y(k,342) +2.922*rxt(k,2986)*y(k,742) +4.084*rxt(k,2989)*y(k,366) + &
3.596*rxt(k,2993)*y(k,289) +.628*rxt(k,2997)*y(k,653) +6.056*rxt(k,3002)*y(k,730) +4.760*rxt(k,3006)*y(k,554) + &
6.888*rxt(k,3019)*y(k,724) +5.648*rxt(k,3023)*y(k,449) +7.040*rxt(k,3027)*y(k,204) +6.240*rxt(k,3031)*y(k,640) + &
4.824*rxt(k,3035)*y(k,470) +1.452*rxt(k,3039)*y(k,490) +3.680*rxt(k,3042)*y(k,459) +2.799*rxt(k,3046)*y(k,509) + &
4.896*rxt(k,3049)*y(k,347) +3.357*rxt(k,3052)*y(k,811) +1.216*rxt(k,3055)*y(k,369) +1.744*rxt(k,3059)*y(k,372) + &
.981*rxt(k,3063)*y(k,64) +.822*rxt(k,3067)*y(k,200) +4.347*rxt(k,3071)*y(k,412) +3.375*rxt(k,3074)*y(k,186) + &
3.330*rxt(k,3077)*y(k,476) +4.791*rxt(k,3080)*y(k,332) +5.936*rxt(k,3083)*y(k,266) +2.658*rxt(k,3087)*y(k,63) + &
3.420*rxt(k,3090)*y(k,121) +4.516*rxt(k,3094)*y(k,294) +5.404*rxt(k,3098)*y(k,668) +6.400*rxt(k,3109)*y(k,567) + &
4.688*rxt(k,3121)*y(k,367) +4.515*rxt(k,3125)*y(k,352) +4.008*rxt(k,3128)*y(k,727) +6.248*rxt(k,3132)*y(k,562) + &
7.060*rxt(k,3136)*y(k,175) +5.840*rxt(k,3140)*y(k,616) +1.988*rxt(k,3144)*y(k,722) +2.214*rxt(k,3148)*y(k,723) + &
2.763*rxt(k,3151)*y(k,365) +5.568*rxt(k,3154)*y(k,492) +3.536*rxt(k,3158)*y(k,478) +5.604*rxt(k,3162)*y(k,473) + &
5.040*rxt(k,3166)*y(k,491) +5.112*rxt(k,3170)*y(k,489) +6.772*rxt(k,3174)*y(k,27) +2.772*rxt(k,3178)*y(k,460) + &
7.256*rxt(k,3181)*y(k,823) +7.300*rxt(k,3185)*y(k,162) +1.149*rxt(k,3189)*y(k,66) +4.287*rxt(k,3193)*y(k,414) + &
3.273*rxt(k,3196)*y(k,65) +2.523*rxt(k,3199)*y(k,170) +2.325*rxt(k,3202)*y(k,166) +2.073*rxt(k,3205)*y(k,248) + &
2.244*rxt(k,3208)*y(k,309) +3.836*rxt(k,3211)*y(k,73) +5.184*rxt(k,3215)*y(k,565) +5.076*rxt(k,3218)*y(k,364) + &
2.480*rxt(k,3221)*y(k,496) +3.760*rxt(k,3226)*y(k,363) +4.028*rxt(k,3239)*y(k,569) +6.912*rxt(k,3252)*y(k,125) + &
6.440*rxt(k,3256)*y(k,187) +6.432*rxt(k,3260)*y(k,271) +6.120*rxt(k,3264)*y(k,313) +8.040*rxt(k,3268)*y(k,568) + &
4.444*rxt(k,3273)*y(k,362) +4.467*rxt(k,3277)*y(k,682) +7.832*rxt(k,3280)*y(k,593) +4.260*rxt(k,3284)*y(k,408) + &
5.073*rxt(k,3287)*y(k,171) +4.389*rxt(k,3290)*y(k,617) +1.464*rxt(k,3293)*y(k,167) +4.062*rxt(k,3297)*y(k,495) + &
.692*rxt(k,3300)*y(k,832) +2.144*rxt(k,3304)*y(k,744) +6.692*rxt(k,3308)*y(k,161) + &
8.3360014*rxt(k,3312)*y(k,174) +4.900*rxt(k,3316)*y(k,468) +4.916*rxt(k,3320)*y(k,485) + &
5.608*rxt(k,3324)*y(k,461) +6.568*rxt(k,3328)*y(k,471) +6.760*rxt(k,3332)*y(k,821) +5.932*rxt(k,3336)*y(k,813) + &
4.791*rxt(k,3340)*y(k,205) +4.002*rxt(k,3343)*y(k,323) +3.564*rxt(k,3346)*y(k,68) +2.124*rxt(k,3350)*y(k,830) + &
4.119*rxt(k,3353)*y(k,416) +5.680*rxt(k,3356)*y(k,475) +3.882*rxt(k,3360)*y(k,156) +1.653*rxt(k,3363)*y(k,157) + &
7.528*rxt(k,3366)*y(k,142) +6.212*rxt(k,3370)*y(k,188) +6.152*rxt(k,3374)*y(k,254) +5.896*rxt(k,3378)*y(k,272) + &
6.048*rxt(k,3382)*y(k,314) +5.792*rxt(k,3386)*y(k,333) +5.696*rxt(k,3390)*y(k,576) +4.263*rxt(k,3394)*y(k,684) + &
4.209*rxt(k,3397)*y(k,618) +4.716*rxt(k,3400)*y(k,51) +4.952*rxt(k,3404)*y(k,488) +6.560*rxt(k,3408)*y(k,486) + &
6.744*rxt(k,3412)*y(k,487) +6.604*rxt(k,3416)*y(k,824) +2.814*rxt(k,3420)*y(k,834) +7.384*rxt(k,3423)*y(k,815) + &
4.653*rxt(k,3427)*y(k,534) +5.145*rxt(k,3430)*y(k,417) +3.196*rxt(k,3433)*y(k,356) +3.960*rxt(k,3446)*y(k,436) + &
1.464*rxt(k,3461)*y(k,557) +3.064*rxt(k,3465)*y(k,632) +2.325*rxt(k,3469)*y(k,587) +3.978*rxt(k,3481)*y(k,375) + &
3.030*rxt(k,3484)*y(k,571) +2.835*rxt(k,3487)*y(k,54) +4.244*rxt(k,3490)*y(k,237) +5.913*rxt(k,3494)*y(k,406) + &
2.032*rxt(k,3497)*y(k,23) +7.584*rxt(k,3501)*y(k,143) +6.056*rxt(k,3505)*y(k,173) +6.432*rxt(k,3509)*y(k,220) + &
6.708*rxt(k,3513)*y(k,226) +4.332*rxt(k,3517)*y(k,255) +4.278*rxt(k,3520)*y(k,273) +6.484*rxt(k,3523)*y(k,295) + &
5.712*rxt(k,3527)*y(k,315) +5.688*rxt(k,3531)*y(k,334) +4.110*rxt(k,3535)*y(k,686) +3.955*rxt(k,3538)*y(k,529) + &
4.071*rxt(k,3555)*y(k,619) +3.873*rxt(k,3558)*y(k,494) +4.122*rxt(k,3561)*y(k,472) +4.844*rxt(k,3564)*y(k,462) + &
6.288*rxt(k,3568)*y(k,469) +6.360*rxt(k,3572)*y(k,820) +6.260*rxt(k,3576)*y(k,835) +5.564*rxt(k,3580)*y(k,172) + &
7.088*rxt(k,3590)*y(k,108) +6.292*rxt(k,3594)*y(k,126) +7.052*rxt(k,3598)*y(k,144) +7.076*rxt(k,3602)*y(k,149) + &
5.876*rxt(k,3606)*y(k,189) +6.940*rxt(k,3610)*y(k,227) +6.716*rxt(k,3614)*y(k,232) +4.215*rxt(k,3618)*y(k,256) + &
6.424*rxt(k,3621)*y(k,296) +6.192*rxt(k,3625)*y(k,297) +4.167*rxt(k,3629)*y(k,316) +5.560*rxt(k,3632)*y(k,335) + &
4.011*rxt(k,3636)*y(k,688) +3.981*rxt(k,3639)*y(k,611) +5.704*rxt(k,3642)*y(k,564) +2.070*rxt(k,3646)*y(k,151) + &
6.492*rxt(k,3649)*y(k,233) +4.437*rxt(k,3653)*y(k,261) +6.348*rxt(k,3656)*y(k,298) +3.930*rxt(k,3660)*y(k,612) + &
3.496*rxt(k,3663)*y(k,818) +2.215*rxt(k,3667)*y(k,817) +5.428*rxt(k,3672)*y(k,467) +3.050*rxt(k,3677)*y(k,466) + &
3.864*rxt(k,3681)*y(k,458) +5.776*rxt(k,3684)*y(k,477) +6.628*rxt(k,3688)*y(k,814) +2.908*rxt(k,3692)*y(k,812) + &
6.604*rxt(k,3696)*y(k,222) +6.396*rxt(k,3700)*y(k,253) +6.012*rxt(k,3704)*y(k,300) +3.900*rxt(k,3708)*y(k,627) + &
5.764*rxt(k,3711)*y(k,822) +6.760*rxt(k,3715)*y(k,106) +6.532*rxt(k,3719)*y(k,223) +6.436*rxt(k,3723)*y(k,228) + &
3.879*rxt(k,3727)*y(k,613) +6.430*rxt(k,3731)*y(k,169) +6.600*rxt(k,3735)*y(k,132) +4.815*rxt(k,3739)*y(k,252) + &
6.092*rxt(k,3742)*y(k,299) +3.864*rxt(k,3746)*y(k,629) +3.144*rxt(k,3749)*y(k,588) +3.792*rxt(k,3759)*y(k,614) + &
7.048*rxt(k,3762)*y(k,105) +6.352*rxt(k,3766)*y(k,221) +6.876*rxt(k,3770)*y(k,331) +3.515*rxt(k,3775)*y(k,463) + &
9.88*rxt(k,3779)*y(k,836) +3.723*rxt(k,3784)*y(k,652) +3.516*rxt(k,3797)*y(k,651) +1.084*rxt(k,3810)*y(k,608) + &
rxt(k,3821)*y(k,433) +1.028*rxt(k,3829)*y(k,481) +1.030*rxt(k,3837)*y(k,503) +1.542*rxt(k,3845)*y(k,522) + &
2.000*rxt(k,3854)*y(k,444) +rxt(k,3856)*y(k,540) +2.000*rxt(k,3865)*y(k,515) +3.000*rxt(k,3867)*y(k,483) + &
2.000*rxt(k,3870)*y(k,10) +rxt(k,3872)*y(k,778) +2.302*rxt(k,3873)*y(k,9) +2.000*rxt(k,3875)*y(k,539) + &
2.246*rxt(k,3876)*y(k,384) +2.000*rxt(k,3878)*y(k,441) +2.000*rxt(k,3884)*y(k,442) +2.000*rxt(k,3887)*y(k,345) + &
2.000*rxt(k,3889)*y(k,829) +1.074*rxt(k,3896)*y(k,580) +1.407*rxt(k,3904)*y(k,50) +3.000*rxt(k,3912)*y(k,250) + &
1.910*rxt(k,3915)*y(k,397) +2.000*rxt(k,3917)*y(k,203) +2.000*rxt(k,3919)*y(k,779) +2.000*rxt(k,3930)*y(k,379) + &
3.291*rxt(k,3941)*y(k,22) +1.900*rxt(k,3944)*y(k,748) +2.000*rxt(k,3968)*y(k,747) +2.898*rxt(k,3970)*y(k,390) + &
1.920*rxt(k,3973)*y(k,541) +1.960*rxt(k,3975)*y(k,749) +1.960*rxt(k,3977)*y(k,750) +.165*rxt(k,3984)*y(k,806) + &
2.583*rxt(k,3990)*y(k,660) +1.920*rxt(k,3999)*y(k,497) +2.688*rxt(k,4002)*y(k,480) +2.376*rxt(k,4012)*y(k,159) + &
.045*rxt(k,4023)*y(k,353) +5.136*rxt(k,4028)*y(k,401) +1.290*rxt(k,4031)*y(k,520) +4.305*rxt(k,4041)*y(k,439) + &
5.136*rxt(k,4053)*y(k,399) +1.840*rxt(k,4056)*y(k,753) +.372*rxt(k,4058)*y(k,160) +2.550*rxt(k,4063)*y(k,689) + &
1.870*rxt(k,4068)*y(k,639) +1.870*rxt(k,4070)*y(k,542) +3.340*rxt(k,4072)*y(k,754) +4.594*rxt(k,4074)*y(k,810) + &
1.160*rxt(k,4083)*y(k,443) +1.160*rxt(k,4087)*y(k,692) +1.160*rxt(k,4091)*y(k,438) +2.298*rxt(k,4095)*y(k,521) + &
.680*rxt(k,4105)*y(k,774) +.500*rxt(k,4106)*y(k,775) +1.160*rxt(k,4107)*y(k,371) +1.804*rxt(k,4111)*y(k,544) + &
1.804*rxt(k,4113)*y(k,543) +1.804*rxt(k,4115)*y(k,524) +2.432*rxt(k,4117)*y(k,656) +1.676*rxt(k,4121)*y(k,426) + &
1.676*rxt(k,4125)*y(k,718) +.400*rxt(k,4131)*y(k,1) +3.504*rxt(k,4133)*y(k,658) +3.324*rxt(k,4139)*y(k,499) + &
1.743*rxt(k,4151)*y(k,574) +4.360*rxt(k,4161)*y(k,720) +.400*rxt(k,4168)*y(k,2) +2.991*rxt(k,4169)*y(k,827) + &
1.587*rxt(k,4178)*y(k,732) +2.664*rxt(k,4180)*y(k,733) +1.572*rxt(k,4184)*y(k,734) +1.086*rxt(k,4188)*y(k,735) + &
1.245*rxt(k,4191)*y(k,736) +3.114*rxt(k,4194)*y(k,762) +1.428*rxt(k,4197)*y(k,763) +4.652*rxt(k,4201)*y(k,764) + &
2.880*rxt(k,4206)*y(k,765) +5.036*rxt(k,4209)*y(k,766) +5.276*rxt(k,4213)*y(k,767))*y(k,705) &
 + (2.000*rxt(k,238)*y(k,512) +2.760*rxt(k,295)*y(k,784) +2.000*rxt(k,431)*y(k,758) +2.000*rxt(k,448)*y(k,370) + &
rxt(k,548)*y(k,590) +3.196*rxt(k,568)*y(k,582) +2.808*rxt(k,592)*y(k,586) +1.898*rxt(k,1077)*y(k,739) + &
2.985*rxt(k,1089)*y(k,52) +2.883*rxt(k,1098)*y(k,585) +2.760*rxt(k,1106)*y(k,385) +3.165*rxt(k,1123)*y(k,33) + &
4.845*rxt(k,1138)*y(k,84) +5.034*rxt(k,1150)*y(k,263) +5.553*rxt(k,1161)*y(k,176) +2.805*rxt(k,1171)*y(k,180) + &
3.444*rxt(k,1182)*y(k,389) +3.444*rxt(k,1193)*y(k,788) +3.039*rxt(k,1203)*y(k,457) +3.147*rxt(k,1214)*y(k,780) + &
5.304*rxt(k,1225)*y(k,49) +3.216*rxt(k,1242)*y(k,72) +3.316*rxt(k,1253)*y(k,213) +5.475*rxt(k,1265)*y(k,264) + &
3.120*rxt(k,1278)*y(k,312) +3.538*rxt(k,1288)*y(k,120) +5.385*rxt(k,1298)*y(k,168) +5.187*rxt(k,1309)*y(k,177) + &
1.804*rxt(k,1317)*y(k,122) +3.204*rxt(k,1327)*y(k,181) +4.398*rxt(k,1339)*y(k,386) +4.371*rxt(k,1349)*y(k,391) + &
2.796*rxt(k,1361)*y(k,395) +2.796*rxt(k,1373)*y(k,792) +4.575*rxt(k,1385)*y(k,798) +4.398*rxt(k,1397)*y(k,785) + &
4.371*rxt(k,1407)*y(k,789) +4.872*rxt(k,1419)*y(k,285) +2.790*rxt(k,1431)*y(k,81) +2.823*rxt(k,1442)*y(k,456) + &
5.124*rxt(k,1455)*y(k,781) +2.736*rxt(k,1467)*y(k,82) +3.504*rxt(k,1479)*y(k,317) +3.018*rxt(k,1492)*y(k,71) + &
5.628*rxt(k,1504)*y(k,215) +4.734*rxt(k,1517)*y(k,265) +8.7320013*rxt(k,1528)*y(k,136) + &
5.937*rxt(k,1540)*y(k,113) +6.930*rxt(k,1553)*y(k,208) +5.451*rxt(k,1564)*y(k,178) +3.392*rxt(k,1575)*y(k,101) + &
6.256*rxt(k,1585)*y(k,398) +5.740*rxt(k,1598)*y(k,141) +8.4160013*rxt(k,1612)*y(k,182) + &
2.763*rxt(k,1624)*y(k,257) +4.084*rxt(k,1636)*y(k,290) +4.494*rxt(k,1649)*y(k,387) +4.338*rxt(k,1660)*y(k,202) + &
4.084*rxt(k,1672)*y(k,269) +4.767*rxt(k,1685)*y(k,216) +2.595*rxt(k,1694)*y(k,123) +4.395*rxt(k,1705)*y(k,392) + &
6.256*rxt(k,1716)*y(k,794) +4.494*rxt(k,1729)*y(k,786) +4.395*rxt(k,1740)*y(k,790) +3.132*rxt(k,1752)*y(k,396) + &
6.656*rxt(k,1764)*y(k,799) +3.132*rxt(k,1777)*y(k,793) +1.654*rxt(k,1787)*y(k,26) +2.852*rxt(k,1797)*y(k,83) + &
10.304*rxt(k,1807)*y(k,127) +4.860*rxt(k,1820)*y(k,267) +4.251*rxt(k,1832)*y(k,787) + &
8.184001*rxt(k,1845)*y(k,183) +2.852*rxt(k,1857)*y(k,400) +5.968*rxt(k,1867)*y(k,782) + &
4.344*rxt(k,1879)*y(k,783) +4.260*rxt(k,1890)*y(k,791) +2.852*rxt(k,1901)*y(k,796) +6.339*rxt(k,1911)*y(k,128) + &
3.336*rxt(k,1922)*y(k,124) +2.784*rxt(k,1934)*y(k,67) +5.526*rxt(k,1946)*y(k,291) +4.086*rxt(k,1957)*y(k,797) + &
2.716*rxt(k,1968)*y(k,53) +6.740*rxt(k,1978)*y(k,219) +2.652*rxt(k,1990)*y(k,404) +3.966*rxt(k,2000)*y(k,795) + &
3.244*rxt(k,2010)*y(k,249) +4.200*rxt(k,2025)*y(k,355) +9.740*rxt(k,2040)*y(k,879) +4.440*rxt(k,2054)*y(k,479) + &
6.771*rxt(k,2066)*y(k,771) +4.880*rxt(k,2078)*y(k,881) +2.664*rxt(k,2090)*y(k,55) +2.604*rxt(k,2100)*y(k,800) + &
2.634*rxt(k,2110)*y(k,56) +2.586*rxt(k,2120)*y(k,801) +2.614*rxt(k,2130)*y(k,57) +2.582*rxt(k,2140)*y(k,802) + &
2.602*rxt(k,2150)*y(k,58) +2.572*rxt(k,2160)*y(k,803) +2.564*rxt(k,2170)*y(k,804) +2.610*rxt(k,2199)*y(k,776) + &
2.610*rxt(k,2244)*y(k,360) +.093*rxt(k,2514)*y(k,344) +2.000*rxt(k,2535)*y(k,346) +1.752*rxt(k,2570)*y(k,447) + &
2.883*rxt(k,2613)*y(k,591) +3.624*rxt(k,2623)*y(k,606) +2.883*rxt(k,2632)*y(k,837) +1.920*rxt(k,2641)*y(k,552) + &
2.805*rxt(k,2733)*y(k,595) +.208*rxt(k,2745)*y(k,270) +4.578*rxt(k,2793)*y(k,501) +3.567*rxt(k,2803)*y(k,628) + &
4.881*rxt(k,2933)*y(k,514) +3.924*rxt(k,3014)*y(k,554) +4.496*rxt(k,3104)*y(k,668) +8.445*rxt(k,3115)*y(k,567) + &
5.664*rxt(k,3234)*y(k,363) +6.936*rxt(k,3247)*y(k,569) +2.445*rxt(k,3442)*y(k,356) +5.348*rxt(k,3456)*y(k,436) + &
3.894*rxt(k,3477)*y(k,587) +5.480*rxt(k,3549)*y(k,529) +4.374*rxt(k,3586)*y(k,172) +2.560*rxt(k,3756)*y(k,588) + &
6.456*rxt(k,3792)*y(k,652) +5.968*rxt(k,3805)*y(k,651) +1.626*rxt(k,3814)*y(k,608) +1.536*rxt(k,3833)*y(k,481) + &
1.476*rxt(k,3841)*y(k,503) +1.692*rxt(k,3850)*y(k,522) +2.000*rxt(k,3880)*y(k,441) +2.000*rxt(k,3893)*y(k,829) + &
1.539*rxt(k,3900)*y(k,580) +1.692*rxt(k,3909)*y(k,50) +1.898*rxt(k,3924)*y(k,779) +1.898*rxt(k,3935)*y(k,379) + &
2.000*rxt(k,3951)*y(k,427) +1.332*rxt(k,3996)*y(k,660) +2.883*rxt(k,4008)*y(k,480) +1.884*rxt(k,4019)*y(k,159) + &
1.332*rxt(k,4037)*y(k,520) +3.842*rxt(k,4047)*y(k,439) +1.700*rxt(k,4066)*y(k,689) +6.891*rxt(k,4079)*y(k,810) + &
2.706*rxt(k,4101)*y(k,521) +3.840*rxt(k,4136)*y(k,658) +3.840*rxt(k,4142)*y(k,499) +2.382*rxt(k,4157)*y(k,574) + &
3.696*rxt(k,4165)*y(k,720) +3.696*rxt(k,4172)*y(k,827))*y(k,693) + (1.956*rxt(k,522)*y(k,348) + &
1.956*rxt(k,529)*y(k,349) +1.548*rxt(k,536)*y(k,350) +.300*rxt(k,545)*y(k,590) +.200*rxt(k,554)*y(k,664) + &
.192*rxt(k,564)*y(k,582) +.960*rxt(k,587)*y(k,586) +.252*rxt(k,1085)*y(k,52) +2.001*rxt(k,1095)*y(k,585) + &
.244*rxt(k,1134)*y(k,84) +.240*rxt(k,1146)*y(k,263) +2.560*rxt(k,1158)*y(k,176) +2.800*rxt(k,1168)*y(k,180) + &
.252*rxt(k,1179)*y(k,389) +.252*rxt(k,1190)*y(k,788) +.360*rxt(k,1200)*y(k,457) +.252*rxt(k,1221)*y(k,49) + &
.420*rxt(k,1238)*y(k,72) +.240*rxt(k,1249)*y(k,213) +.400*rxt(k,1260)*y(k,264) +.405*rxt(k,1273)*y(k,312) + &
2.492*rxt(k,1285)*y(k,120) +2.492*rxt(k,1294)*y(k,168) +2.492*rxt(k,1306)*y(k,177) +2.000*rxt(k,1315)*y(k,122) + &
2.948*rxt(k,1324)*y(k,181) +.305*rxt(k,1335)*y(k,386) +.375*rxt(k,1346)*y(k,391) +3.360*rxt(k,1357)*y(k,395) + &
3.360*rxt(k,1369)*y(k,792) +.300*rxt(k,1381)*y(k,798) +.305*rxt(k,1393)*y(k,785) +.375*rxt(k,1404)*y(k,789) + &
.460*rxt(k,1416)*y(k,285) +2.668*rxt(k,1428)*y(k,81) +.900*rxt(k,1438)*y(k,456) +.305*rxt(k,1451)*y(k,781) + &
2.676*rxt(k,1464)*y(k,82) +.852*rxt(k,1476)*y(k,317) +.452*rxt(k,1488)*y(k,71) +.520*rxt(k,1499)*y(k,215) + &
.505*rxt(k,1512)*y(k,265) +2.408*rxt(k,1525)*y(k,136) +2.408*rxt(k,1537)*y(k,113) +.580*rxt(k,1549)*y(k,208) + &
2.408*rxt(k,1561)*y(k,178) +2.408*rxt(k,1571)*y(k,101) +.240*rxt(k,1582)*y(k,398) +3.680*rxt(k,1594)*y(k,141) + &
3.685*rxt(k,1608)*y(k,182) +2.620*rxt(k,1621)*y(k,257) +3.550*rxt(k,1632)*y(k,290) +.525*rxt(k,1645)*y(k,387) + &
.492*rxt(k,1656)*y(k,202) +3.550*rxt(k,1668)*y(k,269) +3.275*rxt(k,1681)*y(k,216) +2.940*rxt(k,1691)*y(k,123) + &
.496*rxt(k,1701)*y(k,392) +.240*rxt(k,1713)*y(k,794) +.525*rxt(k,1725)*y(k,786) +.496*rxt(k,1736)*y(k,790) + &
3.275*rxt(k,1748)*y(k,396) +.400*rxt(k,1760)*y(k,799) +3.275*rxt(k,1773)*y(k,793) +2.481*rxt(k,1784)*y(k,26) + &
.428*rxt(k,1793)*y(k,83) +2.308*rxt(k,1804)*y(k,127) +2.308*rxt(k,1816)*y(k,267) +.565*rxt(k,1828)*y(k,787) + &
3.815*rxt(k,1841)*y(k,183) +.492*rxt(k,1853)*y(k,400) +.492*rxt(k,1864)*y(k,782) +.480*rxt(k,1875)*y(k,783) + &
.672*rxt(k,1886)*y(k,791) +.492*rxt(k,1897)*y(k,796) +2.944*rxt(k,1908)*y(k,128) +2.904*rxt(k,1919)*y(k,124) + &
.404*rxt(k,1930)*y(k,67) +.560*rxt(k,1941)*y(k,291) +.668*rxt(k,1953)*y(k,797) +.396*rxt(k,1964)*y(k,53) + &
2.316*rxt(k,1975)*y(k,219) +.844*rxt(k,1986)*y(k,404) +.696*rxt(k,1996)*y(k,795) +2.368*rxt(k,2007)*y(k,249) + &
9.066*rxt(k,2020)*y(k,355) +2.750*rxt(k,2035)*y(k,879) +3.095*rxt(k,2050)*y(k,479) +1.140*rxt(k,2063)*y(k,771) + &
4.360*rxt(k,2074)*y(k,881) +.376*rxt(k,2086)*y(k,55) +.872*rxt(k,2096)*y(k,800) +.360*rxt(k,2106)*y(k,56) + &
.852*rxt(k,2116)*y(k,801) +.352*rxt(k,2126)*y(k,57) +.824*rxt(k,2136)*y(k,802) +.344*rxt(k,2146)*y(k,58) + &
.816*rxt(k,2156)*y(k,803) +.800*rxt(k,2166)*y(k,804) +2.668*rxt(k,2610)*y(k,591) +2.560*rxt(k,2800)*y(k,628) + &
2.492*rxt(k,2930)*y(k,514) +.004*rxt(k,3010)*y(k,554) +2.308*rxt(k,3231)*y(k,363) +2.308*rxt(k,3244)*y(k,569) + &
2.960*rxt(k,3438)*y(k,356) +3.825*rxt(k,3452)*y(k,436) +3.685*rxt(k,3473)*y(k,587) +4.032*rxt(k,3544)*y(k,529) + &
.812*rxt(k,3752)*y(k,588) +.940*rxt(k,3787)*y(k,652) +.950*rxt(k,3800)*y(k,651) +2.000*rxt(k,3812)*y(k,608) + &
2.000*rxt(k,3831)*y(k,481) +2.000*rxt(k,3839)*y(k,503) +2.000*rxt(k,3848)*y(k,522) +2.000*rxt(k,3891)*y(k,829) + &
2.000*rxt(k,3898)*y(k,580) +2.000*rxt(k,3907)*y(k,50) +.189*rxt(k,3921)*y(k,779) +.189*rxt(k,3932)*y(k,379) + &
2.883*rxt(k,3993)*y(k,660) +2.883*rxt(k,4005)*y(k,480) +2.883*rxt(k,4016)*y(k,159) +2.883*rxt(k,4034)*y(k,520) + &
2.001*rxt(k,4044)*y(k,439) +6.891*rxt(k,4076)*y(k,810) +2.706*rxt(k,4098)*y(k,521) +2.382*rxt(k,4154)*y(k,574)) &
*y(k,696) + (2.000*rxt(k,427)*y(k,690) +.880*rxt(k,429)*y(k,548) +2.000*rxt(k,433)*y(k,633) + &
2.000*rxt(k,435)*y(k,768) +2.000*rxt(k,437)*y(k,769) +2.000*rxt(k,439)*y(k,620) +4.000*rxt(k,441)*y(k,758) + &
2.000*rxt(k,455)*y(k,370) +2.000*rxt(k,456)*y(k,370) +2.000*rxt(k,471)*y(k,589) +4.000*rxt(k,3958)*y(k,427)) &
*y(k,758) + (rxt(k,445)*y(k,690) +.880*rxt(k,446)*y(k,548) +2.000*rxt(k,450)*y(k,633) +rxt(k,453)*y(k,769) + &
rxt(k,454)*y(k,620) +2.000*rxt(k,457)*y(k,370) +2.000*rxt(k,473)*y(k,589) +4.000*rxt(k,3960)*y(k,427))*y(k,370) &
 + (2.000*rxt(k,3948)*y(k,690) +2.000*rxt(k,3956)*y(k,620) +2.000*rxt(k,3962)*y(k,589) + &
2.000*rxt(k,3964)*y(k,427))*y(k,427) + (rxt(k,3883)*y(k,441) +2.847*rxt(k,3927)*y(k,779) + &
2.847*rxt(k,3938)*y(k,379) +5.043*rxt(k,4050)*y(k,439))*y(k,437) + (.720*rxt(k,595)*y(k,586) + &
.870*rxt(k,1069)*y(k,512) +.940*rxt(k,1126)*y(k,33))*y(k,839) +5.058*rxt(k,4)*y(k,60) +5.627*rxt(k,7)*y(k,62) &
 +5.151*rxt(k,10)*y(k,64) +4.839*rxt(k,13)*y(k,66) +1.922*rxt(k,16)*y(k,94) +2.000*rxt(k,18)*y(k,135) &
 +1.960*rxt(k,20)*y(k,186) +2.880*rxt(k,22)*y(k,192) +3.216*rxt(k,25)*y(k,200) +4.836*rxt(k,27)*y(k,266) &
 +1.282*rxt(k,30)*y(k,270) +3.444*rxt(k,33)*y(k,332) +3.618*rxt(k,44)*y(k,412) +3.434*rxt(k,46)*y(k,414) &
 +rxt(k,50)*y(k,424) +.800*rxt(k,53)*y(k,431) +2.000*rxt(k,59)*y(k,464) +1.920*rxt(k,61)*y(k,476) &
 +1.922*rxt(k,64)*y(k,531) +4.290*rxt(k,79)*y(k,557) +rxt(k,87)*y(k,590) +2.000*rxt(k,90)*y(k,625) &
 +2.000*rxt(k,93)*y(k,634) +3.882*rxt(k,96)*y(k,646) +1.920*rxt(k,99)*y(k,649) +5.469*rxt(k,102)*y(k,653) &
 +3.382*rxt(k,105)*y(k,655) +1.960*rxt(k,107)*y(k,659) +1.922*rxt(k,109)*y(k,663) +.800*rxt(k,123)*y(k,708) &
 +.800*rxt(k,124)*y(k,709) +4.770*rxt(k,128)*y(k,731) +2.026*rxt(k,129)*y(k,732) +5.091*rxt(k,131)*y(k,733) &
 +3.430*rxt(k,134)*y(k,734) +5.427*rxt(k,136)*y(k,735) +3.434*rxt(k,139)*y(k,736) +2.000*rxt(k,142)*y(k,737) &
 +3.128*rxt(k,146)*y(k,755) +2.000*rxt(k,150)*y(k,757) +2.205*rxt(k,156)*y(k,761) +.788*rxt(k,157)*y(k,762) &
 +4.904*rxt(k,160)*y(k,764) +1.449*rxt(k,164)*y(k,765) +5.688*rxt(k,167)*y(k,766) +1.604*rxt(k,171)*y(k,767) &
 +rxt(k,480)*y(k,841)
         loss(k,933) = (rxt(k,453)* y(k,370) +rxt(k,3955)* y(k,427) +rxt(k,412)* y(k,548) +rxt(k,468)* y(k,589) +rxt(k,424) &
* y(k,620) +rxt(k,414)* y(k,633) +rxt(k,411)* y(k,690) +rxt(k,413)* y(k,693) + (rxt(k,437) +rxt(k,438))* y(k,758) &
 +rxt(k,415)* y(k,768) + 2.*rxt(k,416)* y(k,769) + het_rates(k,33))* y(k,769)
         prod(k,933) = (.070*rxt(k,243)*y(k,738) +.102*rxt(k,245)*y(k,450) +.237*rxt(k,247)*y(k,678) +.126*rxt(k,250)*y(k,191) + &
.234*rxt(k,253)*y(k,451) +.435*rxt(k,255)*y(k,680) +.264*rxt(k,258)*y(k,93) +.672*rxt(k,261)*y(k,452) + &
.675*rxt(k,264)*y(k,681) +.420*rxt(k,267)*y(k,114) +.402*rxt(k,270)*y(k,453) +.590*rxt(k,272)*y(k,683) + &
.570*rxt(k,274)*y(k,86) +1.170*rxt(k,277)*y(k,454) +.708*rxt(k,280)*y(k,685) +.942*rxt(k,282)*y(k,85) + &
.980*rxt(k,285)*y(k,88) +.950*rxt(k,289)*y(k,455) +.796*rxt(k,291)*y(k,687) +.856*rxt(k,293)*y(k,669) + &
.003*rxt(k,303)*y(k,573) +.117*rxt(k,307)*y(k,625) +.201*rxt(k,309)*y(k,808) +.180*rxt(k,312)*y(k,517) + &
.234*rxt(k,316)*y(k,662) +.404*rxt(k,319)*y(k,504) +.520*rxt(k,322)*y(k,661) +.016*rxt(k,495)*y(k,770) + &
.087*rxt(k,499)*y(k,755) +.620*rxt(k,502)*y(k,756) +.240*rxt(k,518)*y(k,348) +.240*rxt(k,525)*y(k,349) + &
.468*rxt(k,532)*y(k,350) +.075*rxt(k,551)*y(k,664) +.164*rxt(k,560)*y(k,582) +.355*rxt(k,573)*y(k,731) + &
1.050*rxt(k,578)*y(k,761) +.279*rxt(k,584)*y(k,586) +.096*rxt(k,599)*y(k,359) +.285*rxt(k,603)*y(k,193) + &
.704*rxt(k,607)*y(k,95) +.555*rxt(k,610)*y(k,194) +.468*rxt(k,613)*y(k,280) +.492*rxt(k,616)*y(k,581) + &
.915*rxt(k,620)*y(k,621) +.836*rxt(k,622)*y(k,96) +.645*rxt(k,626)*y(k,115) +.663*rxt(k,629)*y(k,137) + &
.807*rxt(k,632)*y(k,195) +.952*rxt(k,635)*y(k,209) +.750*rxt(k,639)*y(k,281) +.639*rxt(k,642)*y(k,258) + &
1.548*rxt(k,646)*y(k,11) +1.492*rxt(k,650)*y(k,24) +1.524*rxt(k,654)*y(k,37) +1.548*rxt(k,657)*y(k,506) + &
.990*rxt(k,661)*y(k,622) +1.164*rxt(k,664)*y(k,97) +.849*rxt(k,668)*y(k,104) +.897*rxt(k,671)*y(k,116) + &
1.086*rxt(k,674)*y(k,138) +1.053*rxt(k,677)*y(k,146) +1.023*rxt(k,680)*y(k,196) +1.014*rxt(k,683)*y(k,282) + &
.972*rxt(k,686)*y(k,319) +1.080*rxt(k,689)*y(k,102) +1.300*rxt(k,693)*y(k,210) +1.048*rxt(k,697)*y(k,87) + &
.846*rxt(k,701)*y(k,217) +.822*rxt(k,704)*y(k,251) +1.756*rxt(k,708)*y(k,5) +1.820*rxt(k,712)*y(k,7) + &
1.464*rxt(k,715)*y(k,12) +1.744*rxt(k,720)*y(k,15) +1.768*rxt(k,724)*y(k,17) +1.832*rxt(k,728)*y(k,76) + &
1.242*rxt(k,731)*y(k,25) +1.380*rxt(k,734)*y(k,47) +1.744*rxt(k,737)*y(k,38) +1.092*rxt(k,741)*y(k,507) + &
1.828*rxt(k,744)*y(k,728) +1.320*rxt(k,748)*y(k,91) +1.137*rxt(k,752)*y(k,109) +1.233*rxt(k,755)*y(k,139) + &
1.239*rxt(k,758)*y(k,197) +1.059*rxt(k,761)*y(k,207) +1.365*rxt(k,764)*y(k,225) +1.101*rxt(k,767)*y(k,310) + &
1.185*rxt(k,770)*y(k,320) +1.600*rxt(k,773)*y(k,129) +1.472*rxt(k,777)*y(k,211) +1.161*rxt(k,781)*y(k,292) + &
1.011*rxt(k,784)*y(k,98) +1.668*rxt(k,787)*y(k,89) +1.167*rxt(k,791)*y(k,153) +1.110*rxt(k,794)*y(k,117) + &
1.242*rxt(k,797)*y(k,147) +1.179*rxt(k,800)*y(k,283) +1.074*rxt(k,803)*y(k,218) +1.176*rxt(k,806)*y(k,259) + &
1.596*rxt(k,809)*y(k,558) +1.956*rxt(k,813)*y(k,13) +2.032*rxt(k,816)*y(k,30) +1.904*rxt(k,820)*y(k,6) + &
1.443*rxt(k,824)*y(k,70) +1.131*rxt(k,827)*y(k,393) +1.386*rxt(k,830)*y(k,134) +1.341*rxt(k,833)*y(k,140) + &
1.299*rxt(k,836)*y(k,154) +1.347*rxt(k,839)*y(k,198) +1.125*rxt(k,842)*y(k,214) +1.347*rxt(k,845)*y(k,284) + &
1.284*rxt(k,848)*y(k,321) +1.221*rxt(k,851)*y(k,322) +1.320*rxt(k,854)*y(k,130) +1.736*rxt(k,857)*y(k,145) + &
1.608*rxt(k,861)*y(k,212) +1.248*rxt(k,865)*y(k,293) +1.188*rxt(k,868)*y(k,99) +1.720*rxt(k,871)*y(k,90) + &
1.368*rxt(k,875)*y(k,92) +1.278*rxt(k,879)*y(k,111) +1.245*rxt(k,882)*y(k,118) +1.410*rxt(k,885)*y(k,148) + &
1.221*rxt(k,888)*y(k,185) +1.296*rxt(k,891)*y(k,311) +1.419*rxt(k,894)*y(k,36) +1.470*rxt(k,897)*y(k,46) + &
1.760*rxt(k,900)*y(k,77) +1.236*rxt(k,904)*y(k,402) +.894*rxt(k,907)*y(k,670) +1.392*rxt(k,909)*y(k,103) + &
1.401*rxt(k,912)*y(k,155) +1.617*rxt(k,915)*y(k,224) +1.422*rxt(k,918)*y(k,274) +1.407*rxt(k,921)*y(k,318) + &
2.104*rxt(k,925)*y(k,41) +1.542*rxt(k,928)*y(k,69) +1.296*rxt(k,931)*y(k,405) +.916*rxt(k,934)*y(k,671) + &
1.569*rxt(k,936)*y(k,107) +1.446*rxt(k,939)*y(k,229) +1.518*rxt(k,942)*y(k,230) +1.452*rxt(k,945)*y(k,275) + &
1.428*rxt(k,948)*y(k,336) +2.096*rxt(k,951)*y(k,29) +1.554*rxt(k,955)*y(k,78) +.924*rxt(k,958)*y(k,409) + &
.930*rxt(k,960)*y(k,672) +1.551*rxt(k,962)*y(k,110) +1.647*rxt(k,965)*y(k,133) +1.536*rxt(k,968)*y(k,231) + &
1.500*rxt(k,971)*y(k,234) +1.470*rxt(k,974)*y(k,276) +1.458*rxt(k,977)*y(k,337) +2.040*rxt(k,980)*y(k,42) + &
1.557*rxt(k,984)*y(k,74) +.950*rxt(k,987)*y(k,411) +.940*rxt(k,989)*y(k,673) +1.686*rxt(k,991)*y(k,131) + &
1.602*rxt(k,994)*y(k,184) +1.512*rxt(k,997)*y(k,235) +1.587*rxt(k,1000)*y(k,238) +1.482*rxt(k,1003)*y(k,277) + &
1.464*rxt(k,1006)*y(k,338) +1.497*rxt(k,1009)*y(k,44) +1.632*rxt(k,1012)*y(k,79) +.958*rxt(k,1015)*y(k,413) + &
.946*rxt(k,1017)*y(k,674) +1.689*rxt(k,1019)*y(k,152) +1.539*rxt(k,1022)*y(k,236) +1.578*rxt(k,1025)*y(k,239) + &
1.485*rxt(k,1028)*y(k,278) +1.470*rxt(k,1031)*y(k,339) +1.479*rxt(k,1034)*y(k,31) +1.566*rxt(k,1037)*y(k,75) + &
.962*rxt(k,1040)*y(k,415) +2.164*rxt(k,1042)*y(k,40) +.950*rxt(k,1046)*y(k,675) +1.689*rxt(k,1048)*y(k,158) + &
1.488*rxt(k,1051)*y(k,279) +1.557*rxt(k,1054)*y(k,301) +.994*rxt(k,1057)*y(k,340) +1.494*rxt(k,1059)*y(k,43) + &
1.623*rxt(k,1062)*y(k,80) +.966*rxt(k,1065)*y(k,374) +.032*rxt(k,1072)*y(k,739) +.081*rxt(k,1082)*y(k,52) + &
.200*rxt(k,1093)*y(k,585) +.070*rxt(k,1102)*y(k,385) +.070*rxt(k,1110)*y(k,784) +.078*rxt(k,1115)*y(k,19) + &
.147*rxt(k,1117)*y(k,33) +.304*rxt(k,1130)*y(k,84) +.300*rxt(k,1142)*y(k,263) +.198*rxt(k,1154)*y(k,176) + &
.130*rxt(k,1165)*y(k,180) +.198*rxt(k,1175)*y(k,389) +.198*rxt(k,1186)*y(k,788) +.213*rxt(k,1197)*y(k,457) + &
.222*rxt(k,1207)*y(k,780) +.195*rxt(k,1218)*y(k,49) +.189*rxt(k,1229)*y(k,28) +.130*rxt(k,1232)*y(k,262) + &
.416*rxt(k,1234)*y(k,72) +.492*rxt(k,1246)*y(k,213) +.484*rxt(k,1256)*y(k,264) +.468*rxt(k,1269)*y(k,312) + &
.306*rxt(k,1281)*y(k,120) +.303*rxt(k,1291)*y(k,168) +.303*rxt(k,1302)*y(k,177) +.196*rxt(k,1313)*y(k,122) + &
.297*rxt(k,1320)*y(k,181) +.306*rxt(k,1331)*y(k,386) +.315*rxt(k,1343)*y(k,391) +.297*rxt(k,1353)*y(k,395) + &
.297*rxt(k,1365)*y(k,792) +.315*rxt(k,1377)*y(k,798) +.303*rxt(k,1389)*y(k,785) +.312*rxt(k,1401)*y(k,789) + &
.428*rxt(k,1412)*y(k,285) +.297*rxt(k,1424)*y(k,81) +.300*rxt(k,1435)*y(k,456) +.436*rxt(k,1446)*y(k,781) + &
.438*rxt(k,1460)*y(k,82) +.620*rxt(k,1472)*y(k,317) +.800*rxt(k,1484)*y(k,71) +.716*rxt(k,1495)*y(k,215) + &
.820*rxt(k,1508)*y(k,265) +.414*rxt(k,1521)*y(k,136) +.447*rxt(k,1533)*y(k,113) +.772*rxt(k,1544)*y(k,208) + &
.453*rxt(k,1557)*y(k,178) +.498*rxt(k,1568)*y(k,101) +.462*rxt(k,1578)*y(k,398) +.411*rxt(k,1590)*y(k,141) + &
.552*rxt(k,1603)*y(k,182) +.414*rxt(k,1617)*y(k,257) +.411*rxt(k,1628)*y(k,290) +.426*rxt(k,1641)*y(k,387) + &
.432*rxt(k,1653)*y(k,202) +.411*rxt(k,1664)*y(k,269) +.411*rxt(k,1677)*y(k,216) +.270*rxt(k,1689)*y(k,123) + &
.447*rxt(k,1698)*y(k,392) +.462*rxt(k,1709)*y(k,794) +.426*rxt(k,1721)*y(k,786) +.441*rxt(k,1733)*y(k,790) + &
.417*rxt(k,1744)*y(k,396) +.423*rxt(k,1756)*y(k,799) +.417*rxt(k,1769)*y(k,793) +.549*rxt(k,1781)*y(k,26) + &
.780*rxt(k,1790)*y(k,83) +.531*rxt(k,1800)*y(k,127) +.784*rxt(k,1812)*y(k,267) +.552*rxt(k,1824)*y(k,787) + &
.712*rxt(k,1836)*y(k,183) +.813*rxt(k,1850)*y(k,400) +.585*rxt(k,1860)*y(k,782) +.606*rxt(k,1872)*y(k,783) + &
.675*rxt(k,1883)*y(k,791) +.804*rxt(k,1894)*y(k,796) +.546*rxt(k,1904)*y(k,128) +.708*rxt(k,1915)*y(k,124) + &
.927*rxt(k,1927)*y(k,67) +1.144*rxt(k,1937)*y(k,291) +.831*rxt(k,1950)*y(k,797) +1.026*rxt(k,1961)*y(k,53) + &
.729*rxt(k,1971)*y(k,219) +1.125*rxt(k,1983)*y(k,404) +.936*rxt(k,1993)*y(k,795) +.300*rxt(k,2004)*y(k,249) + &
.788*rxt(k,2015)*y(k,355) +.736*rxt(k,2030)*y(k,879) +.680*rxt(k,2045)*y(k,479) +1.098*rxt(k,2059)*y(k,771) + &
.690*rxt(k,2070)*y(k,881) +1.092*rxt(k,2083)*y(k,55) +1.002*rxt(k,2093)*y(k,800) +1.134*rxt(k,2103)*y(k,56) + &
1.053*rxt(k,2113)*y(k,801) +1.164*rxt(k,2123)*y(k,57) +1.086*rxt(k,2133)*y(k,802) +1.185*rxt(k,2143)*y(k,58) + &
1.107*rxt(k,2153)*y(k,803) +1.119*rxt(k,2163)*y(k,804) +.216*rxt(k,2174)*y(k,833) +.344*rxt(k,2178)*y(k,383) + &
.328*rxt(k,2182)*y(k,665) +.435*rxt(k,2186)*y(k,706) +.380*rxt(k,2191)*y(k,746) +.360*rxt(k,2195)*y(k,776) + &
.452*rxt(k,2204)*y(k,676) +.396*rxt(k,2208)*y(k,572) +.505*rxt(k,2212)*y(k,644) +.550*rxt(k,2217)*y(k,704) + &
.480*rxt(k,2222)*y(k,721) +.715*rxt(k,2227)*y(k,14) +.605*rxt(k,2232)*y(k,16) +.516*rxt(k,2237)*y(k,32) + &
.384*rxt(k,2240)*y(k,360) +.520*rxt(k,2249)*y(k,373) +.384*rxt(k,2253)*y(k,807) +.575*rxt(k,2257)*y(k,596) + &
.635*rxt(k,2262)*y(k,697) +.555*rxt(k,2267)*y(k,711) +.525*rxt(k,2272)*y(k,719) +.855*rxt(k,2277)*y(k,240) + &
.730*rxt(k,2282)*y(k,302) +.616*rxt(k,2287)*y(k,324) +.424*rxt(k,2291)*y(k,816) +.132*rxt(k,2295)*y(k,667) + &
.584*rxt(k,2299)*y(k,376) +.640*rxt(k,2303)*y(k,597) +.705*rxt(k,2308)*y(k,698) +.630*rxt(k,2313)*y(k,712) + &
.895*rxt(k,2318)*y(k,241) +.770*rxt(k,2323)*y(k,303) +.648*rxt(k,2328)*y(k,325) +.200*rxt(k,2332)*y(k,630) + &
.628*rxt(k,2336)*y(k,377) +.675*rxt(k,2340)*y(k,598) +.745*rxt(k,2345)*y(k,699) +.670*rxt(k,2350)*y(k,713) + &
.920*rxt(k,2355)*y(k,242) +.795*rxt(k,2360)*y(k,304) +.664*rxt(k,2365)*y(k,326) +.264*rxt(k,2369)*y(k,119) + &
.660*rxt(k,2373)*y(k,378) +.564*rxt(k,2377)*y(k,599) +.780*rxt(k,2381)*y(k,700) +.710*rxt(k,2386)*y(k,714) + &
.940*rxt(k,2391)*y(k,243) +.815*rxt(k,2396)*y(k,305) +.680*rxt(k,2401)*y(k,327) +.688*rxt(k,2405)*y(k,380) + &
.584*rxt(k,2409)*y(k,600) +.810*rxt(k,2413)*y(k,701) +.745*rxt(k,2418)*y(k,715) +.940*rxt(k,2423)*y(k,244) + &
.825*rxt(k,2428)*y(k,306) +.680*rxt(k,2433)*y(k,328) +.704*rxt(k,2437)*y(k,381) +.600*rxt(k,2441)*y(k,601) + &
.830*rxt(k,2445)*y(k,702) +.765*rxt(k,2450)*y(k,716) +.950*rxt(k,2455)*y(k,245) +.835*rxt(k,2460)*y(k,307) + &
.688*rxt(k,2465)*y(k,329) +.720*rxt(k,2469)*y(k,382) +.616*rxt(k,2473)*y(k,602) +.845*rxt(k,2477)*y(k,703) + &
.790*rxt(k,2482)*y(k,717) +.960*rxt(k,2487)*y(k,246) +.850*rxt(k,2492)*y(k,308) +.696*rxt(k,2497)*y(k,330) + &
.032*rxt(k,2520)*y(k,741) +.009*rxt(k,2527)*y(k,677) +.030*rxt(k,2544)*y(k,605) +.069*rxt(k,2564)*y(k,447) + &
.146*rxt(k,2576)*y(k,418) +.237*rxt(k,2578)*y(k,826) +.236*rxt(k,2581)*y(k,18) +.012*rxt(k,2585)*y(k,192) + &
.024*rxt(k,2589)*y(k,59) +.148*rxt(k,2593)*y(k,575) +.039*rxt(k,2597)*y(k,679) +.018*rxt(k,2600)*y(k,773) + &
.228*rxt(k,2603)*y(k,528) +.117*rxt(k,2606)*y(k,591) +.168*rxt(k,2617)*y(k,606) +.117*rxt(k,2627)*y(k,837) + &
.072*rxt(k,2636)*y(k,552) +.240*rxt(k,2646)*y(k,48) +.120*rxt(k,2649)*y(k,500) +.172*rxt(k,2652)*y(k,641) + &
.117*rxt(k,2656)*y(k,394) +.156*rxt(k,2660)*y(k,583) +.117*rxt(k,2663)*y(k,570) +.117*rxt(k,2666)*y(k,368) + &
.117*rxt(k,2670)*y(k,634) +.020*rxt(k,2672)*y(k,35) +.009*rxt(k,2676)*y(k,20) +.030*rxt(k,2679)*y(k,45) + &
.003*rxt(k,2682)*y(k,112) +.030*rxt(k,2685)*y(k,635) +.080*rxt(k,2689)*y(k,518) +.099*rxt(k,2692)*y(k,199) + &
.090*rxt(k,2695)*y(k,286) +.024*rxt(k,2702)*y(k,626) +.084*rxt(k,2705)*y(k,465) +.020*rxt(k,2708)*y(k,592) + &
.354*rxt(k,2714)*y(k,419) +.117*rxt(k,2717)*y(k,420) +.564*rxt(k,2721)*y(k,354) +.501*rxt(k,2724)*y(k,828) + &
.195*rxt(k,2727)*y(k,595) +.009*rxt(k,2737)*y(k,94) +.048*rxt(k,2742)*y(k,270) +.072*rxt(k,2750)*y(k,60) + &
.195*rxt(k,2755)*y(k,659) +.201*rxt(k,2757)*y(k,464) +.228*rxt(k,2761)*y(k,649) +.088*rxt(k,2763)*y(k,164) + &
.045*rxt(k,2767)*y(k,247) +.105*rxt(k,2770)*y(k,407) +.140*rxt(k,2773)*y(k,563) +.148*rxt(k,2777)*y(k,190) + &
.264*rxt(k,2781)*y(k,523) +.243*rxt(k,2785)*y(k,654) +.282*rxt(k,2788)*y(k,501) +.130*rxt(k,2797)*y(k,628) + &
.027*rxt(k,2808)*y(k,531) +.195*rxt(k,2811)*y(k,135) +.304*rxt(k,2814)*y(k,825) +.288*rxt(k,2819)*y(k,526) + &
.220*rxt(k,2823)*y(k,579) +.268*rxt(k,2826)*y(k,610) +.300*rxt(k,2830)*y(k,624) +.296*rxt(k,2834)*y(k,403) + &
.264*rxt(k,2838)*y(k,726) +.268*rxt(k,2842)*y(k,268) +.210*rxt(k,2846)*y(k,100) +.176*rxt(k,2850)*y(k,516) + &
.171*rxt(k,2853)*y(k,206) +.220*rxt(k,2857)*y(k,260) +.220*rxt(k,2861)*y(k,287) +.308*rxt(k,2864)*y(k,603) + &
.155*rxt(k,2869)*y(k,513) +.276*rxt(k,2874)*y(k,648) +.236*rxt(k,2877)*y(k,657) +.006*rxt(k,2881)*y(k,725) + &
.534*rxt(k,2885)*y(k,421) +.165*rxt(k,2888)*y(k,422) +.120*rxt(k,2892)*y(k,62) +.396*rxt(k,2896)*y(k,646) + &
.408*rxt(k,2900)*y(k,655) +.453*rxt(k,2904)*y(k,663) +.234*rxt(k,2906)*y(k,61) +.129*rxt(k,2909)*y(k,165) + &
.172*rxt(k,2912)*y(k,647) +.318*rxt(k,2916)*y(k,740) +.348*rxt(k,2919)*y(k,498) +.396*rxt(k,2922)*y(k,584) + &
.315*rxt(k,2926)*y(k,514) +.432*rxt(k,2938)*y(k,505) +.480*rxt(k,2942)*y(k,566) +.688*rxt(k,2945)*y(k,642) + &
.488*rxt(k,2949)*y(k,361) +.420*rxt(k,2953)*y(k,743) +.440*rxt(k,2958)*y(k,772) +.624*rxt(k,2961)*y(k,805) + &
.344*rxt(k,2965)*y(k,474) +.468*rxt(k,2968)*y(k,615) +.066*rxt(k,2972)*y(k,410) +.080*rxt(k,2975)*y(k,179) + &
.456*rxt(k,2979)*y(k,493) +.294*rxt(k,2983)*y(k,342) +.219*rxt(k,2986)*y(k,742) +.448*rxt(k,2989)*y(k,366) + &
.280*rxt(k,2994)*y(k,289) +.052*rxt(k,2997)*y(k,653) +.348*rxt(k,3002)*y(k,730) +.496*rxt(k,3007)*y(k,554) + &
.508*rxt(k,3020)*y(k,724) +.444*rxt(k,3024)*y(k,449) +.440*rxt(k,3027)*y(k,204) +.504*rxt(k,3031)*y(k,640) + &
.440*rxt(k,3036)*y(k,470) +.156*rxt(k,3039)*y(k,490) +.452*rxt(k,3042)*y(k,459) +.300*rxt(k,3046)*y(k,509) + &
.336*rxt(k,3049)*y(k,347) +.243*rxt(k,3052)*y(k,811) +.144*rxt(k,3056)*y(k,369) +.272*rxt(k,3060)*y(k,372) + &
.201*rxt(k,3064)*y(k,64) +.147*rxt(k,3068)*y(k,200) +.579*rxt(k,3072)*y(k,412) +.489*rxt(k,3074)*y(k,186) + &
.486*rxt(k,3077)*y(k,476) +.507*rxt(k,3081)*y(k,332) +.716*rxt(k,3084)*y(k,266) +.414*rxt(k,3087)*y(k,63) + &
.392*rxt(k,3090)*y(k,121) +.604*rxt(k,3094)*y(k,294) +.828*rxt(k,3098)*y(k,668) +.824*rxt(k,3110)*y(k,567) + &
.680*rxt(k,3121)*y(k,367) +.666*rxt(k,3125)*y(k,352) +.592*rxt(k,3128)*y(k,727) +.736*rxt(k,3132)*y(k,562) + &
.696*rxt(k,3136)*y(k,175) +.816*rxt(k,3140)*y(k,616) +.308*rxt(k,3145)*y(k,722) +.345*rxt(k,3149)*y(k,723) + &
.345*rxt(k,3151)*y(k,365) +.636*rxt(k,3155)*y(k,492) +.548*rxt(k,3159)*y(k,478) +.536*rxt(k,3162)*y(k,473) + &
.500*rxt(k,3167)*y(k,491) +.556*rxt(k,3171)*y(k,489) +.624*rxt(k,3175)*y(k,27) +.690*rxt(k,3178)*y(k,460) + &
.616*rxt(k,3181)*y(k,823) +1.424*rxt(k,3185)*y(k,162) +.288*rxt(k,3190)*y(k,66) +.888*rxt(k,3194)*y(k,414) + &
.687*rxt(k,3196)*y(k,65) +.459*rxt(k,3199)*y(k,170) +.489*rxt(k,3202)*y(k,166) +.435*rxt(k,3205)*y(k,248) + &
.471*rxt(k,3208)*y(k,309) +.788*rxt(k,3211)*y(k,73) +.699*rxt(k,3215)*y(k,565) +.636*rxt(k,3218)*y(k,364) + &
.390*rxt(k,3222)*y(k,496) +.784*rxt(k,3226)*y(k,363) +.776*rxt(k,3240)*y(k,569) +.980*rxt(k,3252)*y(k,125) + &
1.156*rxt(k,3256)*y(k,187) +1.036*rxt(k,3260)*y(k,271) +1.176*rxt(k,3264)*y(k,313) +1.175*rxt(k,3269)*y(k,568) + &
.864*rxt(k,3273)*y(k,362) +.927*rxt(k,3277)*y(k,682) +1.264*rxt(k,3281)*y(k,593) +.852*rxt(k,3284)*y(k,408) + &
.864*rxt(k,3287)*y(k,171) +.906*rxt(k,3290)*y(k,617) +.292*rxt(k,3293)*y(k,167) +.636*rxt(k,3297)*y(k,495) + &
.140*rxt(k,3300)*y(k,832) +.336*rxt(k,3305)*y(k,744) +.820*rxt(k,3308)*y(k,161) +.900*rxt(k,3312)*y(k,174) + &
.992*rxt(k,3316)*y(k,468) +.760*rxt(k,3321)*y(k,485) +.868*rxt(k,3324)*y(k,461) +.844*rxt(k,3329)*y(k,471) + &
.796*rxt(k,3333)*y(k,821) +.712*rxt(k,3336)*y(k,813) +1.179*rxt(k,3340)*y(k,205) +.993*rxt(k,3343)*y(k,323) + &
.900*rxt(k,3346)*y(k,68) +.546*rxt(k,3351)*y(k,830) +1.071*rxt(k,3353)*y(k,416) +1.128*rxt(k,3357)*y(k,475) + &
.903*rxt(k,3360)*y(k,156) +.408*rxt(k,3363)*y(k,157) +1.628*rxt(k,3366)*y(k,142) +1.456*rxt(k,3370)*y(k,188) + &
1.236*rxt(k,3374)*y(k,254) +1.352*rxt(k,3378)*y(k,272) +1.436*rxt(k,3382)*y(k,314) +1.460*rxt(k,3386)*y(k,333) + &
1.192*rxt(k,3390)*y(k,576) +1.107*rxt(k,3394)*y(k,684) +1.092*rxt(k,3397)*y(k,618) +.780*rxt(k,3400)*y(k,51) + &
.804*rxt(k,3405)*y(k,488) +1.028*rxt(k,3409)*y(k,486) +1.092*rxt(k,3413)*y(k,487) +.936*rxt(k,3416)*y(k,824) + &
.495*rxt(k,3421)*y(k,834) +1.016*rxt(k,3423)*y(k,815) +.744*rxt(k,3427)*y(k,534) +1.389*rxt(k,3430)*y(k,417) + &
.952*rxt(k,3433)*y(k,356) +1.130*rxt(k,3447)*y(k,436) +.412*rxt(k,3462)*y(k,557) +.856*rxt(k,3465)*y(k,632) + &
.696*rxt(k,3469)*y(k,587) +1.188*rxt(k,3481)*y(k,375) +.900*rxt(k,3484)*y(k,571) +.849*rxt(k,3487)*y(k,54) + &
1.140*rxt(k,3490)*y(k,237) +.963*rxt(k,3494)*y(k,406) +.448*rxt(k,3498)*y(k,23) +1.928*rxt(k,3501)*y(k,143) + &
1.660*rxt(k,3505)*y(k,173) +1.484*rxt(k,3509)*y(k,220) +1.708*rxt(k,3513)*y(k,226) +1.122*rxt(k,3517)*y(k,255) + &
1.236*rxt(k,3520)*y(k,273) +1.592*rxt(k,3523)*y(k,295) +1.632*rxt(k,3527)*y(k,315) +1.636*rxt(k,3531)*y(k,334) + &
1.227*rxt(k,3535)*y(k,686) +1.155*rxt(k,3539)*y(k,529) +1.218*rxt(k,3555)*y(k,619) +.993*rxt(k,3558)*y(k,494) + &
.828*rxt(k,3561)*y(k,472) +.936*rxt(k,3565)*y(k,462) +1.116*rxt(k,3568)*y(k,469) +1.084*rxt(k,3572)*y(k,820) + &
1.040*rxt(k,3577)*y(k,835) +1.716*rxt(k,3580)*y(k,172) +1.820*rxt(k,3590)*y(k,108) +1.696*rxt(k,3594)*y(k,126) + &
2.040*rxt(k,3598)*y(k,144) +1.852*rxt(k,3602)*y(k,149) +1.836*rxt(k,3606)*y(k,189) +2.028*rxt(k,3610)*y(k,227) + &
1.908*rxt(k,3614)*y(k,232) +1.326*rxt(k,3618)*y(k,256) +1.712*rxt(k,3621)*y(k,296) +1.816*rxt(k,3625)*y(k,297) + &
1.317*rxt(k,3629)*y(k,316) +1.748*rxt(k,3632)*y(k,335) +1.305*rxt(k,3636)*y(k,688) +1.296*rxt(k,3639)*y(k,611) + &
1.628*rxt(k,3643)*y(k,564) +.696*rxt(k,3646)*y(k,151) +1.896*rxt(k,3649)*y(k,233) +1.347*rxt(k,3653)*y(k,261) + &
1.984*rxt(k,3656)*y(k,298) +1.344*rxt(k,3660)*y(k,612) +.928*rxt(k,3664)*y(k,818) +.645*rxt(k,3668)*y(k,817) + &
1.440*rxt(k,3672)*y(k,467) +.730*rxt(k,3677)*y(k,466) +1.323*rxt(k,3681)*y(k,458) +1.816*rxt(k,3685)*y(k,477) + &
1.324*rxt(k,3688)*y(k,814) +.992*rxt(k,3693)*y(k,812) +2.152*rxt(k,3696)*y(k,222) +1.964*rxt(k,3700)*y(k,253) + &
2.008*rxt(k,3704)*y(k,300) +1.377*rxt(k,3708)*y(k,627) +1.344*rxt(k,3712)*y(k,822) +2.136*rxt(k,3715)*y(k,106) + &
2.192*rxt(k,3719)*y(k,223) +2.112*rxt(k,3723)*y(k,228) +1.395*rxt(k,3727)*y(k,613) +1.760*rxt(k,3731)*y(k,169) + &
2.224*rxt(k,3735)*y(k,132) +1.485*rxt(k,3739)*y(k,252) +2.144*rxt(k,3742)*y(k,299) +1.407*rxt(k,3746)*y(k,629) + &
1.140*rxt(k,3749)*y(k,588) +1.392*rxt(k,3759)*y(k,614) +2.172*rxt(k,3762)*y(k,105) +2.244*rxt(k,3766)*y(k,221) + &
2.256*rxt(k,3770)*y(k,331) +.870*rxt(k,3775)*y(k,463) +2.650*rxt(k,3780)*y(k,836) +1.065*rxt(k,3784)*y(k,652) + &
1.203*rxt(k,3797)*y(k,651) +.090*rxt(k,3915)*y(k,397) +.045*rxt(k,3941)*y(k,22) +.100*rxt(k,3945)*y(k,748) + &
.126*rxt(k,3970)*y(k,390) +.080*rxt(k,3973)*y(k,541) +.040*rxt(k,3975)*y(k,749) +.040*rxt(k,3977)*y(k,750) + &
.003*rxt(k,3984)*y(k,806) +.105*rxt(k,3990)*y(k,660) +.080*rxt(k,3999)*y(k,497) +.111*rxt(k,4002)*y(k,480) + &
.096*rxt(k,4012)*y(k,159) +.003*rxt(k,4024)*y(k,353) +.240*rxt(k,4028)*y(k,401) +.054*rxt(k,4031)*y(k,520) + &
.120*rxt(k,4041)*y(k,439) +.240*rxt(k,4053)*y(k,399) +.160*rxt(k,4056)*y(k,753) +.016*rxt(k,4059)*y(k,160) + &
.450*rxt(k,4063)*y(k,689) +.130*rxt(k,4068)*y(k,639) +.130*rxt(k,4070)*y(k,542) +.438*rxt(k,4072)*y(k,754) + &
.622*rxt(k,4074)*y(k,810) +.096*rxt(k,4084)*y(k,443) +.096*rxt(k,4088)*y(k,692) +.096*rxt(k,4092)*y(k,438) + &
.249*rxt(k,4095)*y(k,521) +.096*rxt(k,4108)*y(k,371) +.196*rxt(k,4111)*y(k,544) +.196*rxt(k,4113)*y(k,543) + &
.196*rxt(k,4115)*y(k,524) +.288*rxt(k,4118)*y(k,656) +.200*rxt(k,4122)*y(k,426) +.200*rxt(k,4126)*y(k,718) + &
.492*rxt(k,4134)*y(k,658) +.477*rxt(k,4140)*y(k,499) +.453*rxt(k,4151)*y(k,574) +.740*rxt(k,4162)*y(k,720) + &
.528*rxt(k,4170)*y(k,827) +.111*rxt(k,4178)*y(k,732) +.284*rxt(k,4181)*y(k,733) +.244*rxt(k,4185)*y(k,734) + &
.228*rxt(k,4189)*y(k,735) +.324*rxt(k,4192)*y(k,736) +.222*rxt(k,4195)*y(k,762) +.096*rxt(k,4198)*y(k,763) + &
.540*rxt(k,4202)*y(k,764) +.444*rxt(k,4206)*y(k,765) +1.140*rxt(k,4209)*y(k,766) +1.680*rxt(k,4214)*y(k,767)) &
*y(k,705) + (.240*rxt(k,295)*y(k,784) +.204*rxt(k,569)*y(k,582) +.192*rxt(k,592)*y(k,586) + &
.102*rxt(k,1077)*y(k,739) +.240*rxt(k,1089)*y(k,52) +.117*rxt(k,1098)*y(k,585) +.240*rxt(k,1106)*y(k,385) + &
.195*rxt(k,1123)*y(k,33) +.498*rxt(k,1138)*y(k,84) +.447*rxt(k,1150)*y(k,263) +.195*rxt(k,1161)*y(k,176) + &
.195*rxt(k,1171)*y(k,180) +.402*rxt(k,1182)*y(k,389) +.402*rxt(k,1193)*y(k,788) +.375*rxt(k,1203)*y(k,457) + &
.300*rxt(k,1214)*y(k,780) +.348*rxt(k,1225)*y(k,49) +.474*rxt(k,1242)*y(k,72) +.376*rxt(k,1253)*y(k,213) + &
.672*rxt(k,1265)*y(k,264) +.464*rxt(k,1278)*y(k,312) +.268*rxt(k,1288)*y(k,120) +.294*rxt(k,1298)*y(k,168) + &
.519*rxt(k,1309)*y(k,177) +.196*rxt(k,1317)*y(k,122) +.345*rxt(k,1327)*y(k,181) +.663*rxt(k,1339)*y(k,386) + &
.660*rxt(k,1349)*y(k,391) +.294*rxt(k,1361)*y(k,395) +.294*rxt(k,1373)*y(k,792) +.618*rxt(k,1385)*y(k,798) + &
.663*rxt(k,1397)*y(k,785) +.660*rxt(k,1407)*y(k,789) +.872*rxt(k,1420)*y(k,285) +.303*rxt(k,1431)*y(k,81) + &
.495*rxt(k,1442)*y(k,456) +.624*rxt(k,1455)*y(k,781) +.426*rxt(k,1467)*y(k,82) +.800*rxt(k,1479)*y(k,317) + &
.600*rxt(k,1492)*y(k,71) +.951*rxt(k,1504)*y(k,215) +.909*rxt(k,1517)*y(k,265) +1.216*rxt(k,1528)*y(k,136) + &
.627*rxt(k,1540)*y(k,113) +.699*rxt(k,1553)*y(k,208) +.798*rxt(k,1564)*y(k,178) +.338*rxt(k,1575)*y(k,101) + &
1.048*rxt(k,1586)*y(k,398) +.836*rxt(k,1598)*y(k,141) +1.108*rxt(k,1612)*y(k,182) +.405*rxt(k,1624)*y(k,257) + &
.624*rxt(k,1636)*y(k,290) +.897*rxt(k,1649)*y(k,387) +.795*rxt(k,1660)*y(k,202) +.624*rxt(k,1672)*y(k,269) + &
.495*rxt(k,1685)*y(k,216) +.405*rxt(k,1694)*y(k,123) +.879*rxt(k,1705)*y(k,392) +1.048*rxt(k,1717)*y(k,794) + &
.897*rxt(k,1729)*y(k,786) +.879*rxt(k,1740)*y(k,790) +.471*rxt(k,1752)*y(k,396) +1.132*rxt(k,1764)*y(k,799) + &
.471*rxt(k,1777)*y(k,793) +.346*rxt(k,1787)*y(k,26) +.710*rxt(k,1797)*y(k,83) +1.768*rxt(k,1808)*y(k,127) + &
.618*rxt(k,1820)*y(k,267) +1.059*rxt(k,1832)*y(k,787) +1.364*rxt(k,1845)*y(k,183) +.710*rxt(k,1857)*y(k,400) + &
1.216*rxt(k,1868)*y(k,782) +.897*rxt(k,1879)*y(k,783) +1.062*rxt(k,1890)*y(k,791) +.710*rxt(k,1901)*y(k,796) + &
1.221*rxt(k,1911)*y(k,128) +.700*rxt(k,1922)*y(k,124) +.806*rxt(k,1934)*y(k,67) +1.272*rxt(k,1946)*y(k,291) + &
1.185*rxt(k,1957)*y(k,797) +.866*rxt(k,1968)*y(k,53) +1.292*rxt(k,1978)*y(k,219) +.846*rxt(k,1990)*y(k,404) + &
1.266*rxt(k,2000)*y(k,795) +.964*rxt(k,2010)*y(k,249) +1.172*rxt(k,2026)*y(k,355) +2.444*rxt(k,2040)*y(k,879) + &
1.184*rxt(k,2055)*y(k,479) +1.686*rxt(k,2066)*y(k,771) +1.372*rxt(k,2078)*y(k,881) +.900*rxt(k,2090)*y(k,55) + &
.880*rxt(k,2100)*y(k,800) +.922*rxt(k,2110)*y(k,56) +.906*rxt(k,2120)*y(k,801) +.936*rxt(k,2130)*y(k,57) + &
.924*rxt(k,2140)*y(k,802) +.944*rxt(k,2150)*y(k,58) +.934*rxt(k,2160)*y(k,803) +.940*rxt(k,2170)*y(k,804) + &
.390*rxt(k,2199)*y(k,776) +.390*rxt(k,2244)*y(k,360) +.006*rxt(k,2514)*y(k,344) +.152*rxt(k,2571)*y(k,447) + &
.117*rxt(k,2613)*y(k,591) +.150*rxt(k,2623)*y(k,606) +.117*rxt(k,2632)*y(k,837) +.080*rxt(k,2641)*y(k,552) + &
.195*rxt(k,2733)*y(k,595) +.008*rxt(k,2746)*y(k,270) +.318*rxt(k,2793)*y(k,501) +.249*rxt(k,2803)*y(k,628) + &
.528*rxt(k,2934)*y(k,514) +.420*rxt(k,3014)*y(k,554) +.620*rxt(k,3104)*y(k,668) +.860*rxt(k,3116)*y(k,567) + &
1.136*rxt(k,3234)*y(k,363) +1.060*rxt(k,3248)*y(k,569) +.726*rxt(k,3442)*y(k,356) +1.556*rxt(k,3456)*y(k,436) + &
1.164*rxt(k,3477)*y(k,587) +1.595*rxt(k,3550)*y(k,529) +1.359*rxt(k,3586)*y(k,172) +.940*rxt(k,3756)*y(k,588) + &
1.936*rxt(k,3792)*y(k,652) +2.060*rxt(k,3805)*y(k,651) +.102*rxt(k,3924)*y(k,779) +.102*rxt(k,3935)*y(k,379) + &
.054*rxt(k,3996)*y(k,660) +.117*rxt(k,4008)*y(k,480) +.078*rxt(k,4019)*y(k,159) +.054*rxt(k,4037)*y(k,520) + &
.078*rxt(k,4047)*y(k,439) +.300*rxt(k,4066)*y(k,689) +.933*rxt(k,4079)*y(k,810) +.294*rxt(k,4101)*y(k,521) + &
.600*rxt(k,4137)*y(k,658) +.600*rxt(k,4143)*y(k,499) +.618*rxt(k,4157)*y(k,574) +.690*rxt(k,4166)*y(k,720) + &
.690*rxt(k,4173)*y(k,827))*y(k,693) + (.042*rxt(k,537)*y(k,350) +.040*rxt(k,587)*y(k,586) + &
.004*rxt(k,1135)*y(k,84) +.012*rxt(k,1147)*y(k,263) +.104*rxt(k,1158)*y(k,176) +.015*rxt(k,1201)*y(k,457) + &
.016*rxt(k,1239)*y(k,72) +.008*rxt(k,1250)*y(k,213) +.025*rxt(k,1261)*y(k,264) +.015*rxt(k,1274)*y(k,312) + &
.172*rxt(k,1285)*y(k,120) +.172*rxt(k,1295)*y(k,168) +.172*rxt(k,1306)*y(k,177) +.005*rxt(k,1335)*y(k,386) + &
.140*rxt(k,1357)*y(k,395) +.140*rxt(k,1369)*y(k,792) +.015*rxt(k,1381)*y(k,798) +.005*rxt(k,1393)*y(k,785) + &
.044*rxt(k,1416)*y(k,285) +.284*rxt(k,1428)*y(k,81) +.064*rxt(k,1439)*y(k,456) +.010*rxt(k,1451)*y(k,781) + &
.404*rxt(k,1464)*y(k,82) +.092*rxt(k,1476)*y(k,317) +.032*rxt(k,1489)*y(k,71) +.055*rxt(k,1500)*y(k,215) + &
.050*rxt(k,1513)*y(k,265) +.260*rxt(k,1525)*y(k,136) +.260*rxt(k,1537)*y(k,113) +.015*rxt(k,1549)*y(k,208) + &
.260*rxt(k,1561)*y(k,178) +.260*rxt(k,1572)*y(k,101) +.008*rxt(k,1582)*y(k,398) +.010*rxt(k,1594)*y(k,141) + &
.005*rxt(k,1608)*y(k,182) +.180*rxt(k,1621)*y(k,257) +.140*rxt(k,1632)*y(k,290) +.020*rxt(k,1645)*y(k,387) + &
.012*rxt(k,1657)*y(k,202) +.140*rxt(k,1668)*y(k,269) +.225*rxt(k,1681)*y(k,216) +.060*rxt(k,1692)*y(k,123) + &
.004*rxt(k,1702)*y(k,392) +.008*rxt(k,1713)*y(k,794) +.020*rxt(k,1725)*y(k,786) +.004*rxt(k,1737)*y(k,790) + &
.225*rxt(k,1748)*y(k,396) +.025*rxt(k,1760)*y(k,799) +.225*rxt(k,1773)*y(k,793) +.519*rxt(k,1785)*y(k,26) + &
.048*rxt(k,1794)*y(k,83) +.360*rxt(k,1804)*y(k,127) +.360*rxt(k,1817)*y(k,267) +.040*rxt(k,1828)*y(k,787) + &
.015*rxt(k,1841)*y(k,183) +.012*rxt(k,1854)*y(k,400) +.008*rxt(k,1864)*y(k,782) +.020*rxt(k,1876)*y(k,783) + &
.016*rxt(k,1887)*y(k,791) +.012*rxt(k,1898)*y(k,796) +.004*rxt(k,1908)*y(k,128) +.096*rxt(k,1920)*y(k,124) + &
.064*rxt(k,1931)*y(k,67) +.080*rxt(k,1942)*y(k,291) +.024*rxt(k,1954)*y(k,797) +.084*rxt(k,1965)*y(k,53) + &
.484*rxt(k,1975)*y(k,219) +.036*rxt(k,1987)*y(k,404) +.036*rxt(k,1997)*y(k,795) +.700*rxt(k,2007)*y(k,249) + &
2.022*rxt(k,2020)*y(k,355) +.560*rxt(k,2035)*y(k,879) +.885*rxt(k,2050)*y(k,479) +.232*rxt(k,2063)*y(k,771) + &
.565*rxt(k,2074)*y(k,881) +.100*rxt(k,2087)*y(k,55) +.048*rxt(k,2097)*y(k,800) +.108*rxt(k,2107)*y(k,56) + &
.064*rxt(k,2117)*y(k,801) +.112*rxt(k,2127)*y(k,57) +.080*rxt(k,2137)*y(k,802) +.116*rxt(k,2147)*y(k,58) + &
.100*rxt(k,2157)*y(k,803) +.116*rxt(k,2167)*y(k,804) +.104*rxt(k,2800)*y(k,628) +.172*rxt(k,2930)*y(k,514) + &
.360*rxt(k,3231)*y(k,363) +.360*rxt(k,3244)*y(k,569) +.875*rxt(k,3438)*y(k,356) +.035*rxt(k,3452)*y(k,436) + &
.030*rxt(k,3473)*y(k,587) +.384*rxt(k,3544)*y(k,529) +.124*rxt(k,3753)*y(k,588) +.190*rxt(k,3788)*y(k,652) + &
.205*rxt(k,3801)*y(k,651) +.117*rxt(k,3993)*y(k,660) +.117*rxt(k,4005)*y(k,480) +.117*rxt(k,4016)*y(k,159) + &
.117*rxt(k,4034)*y(k,520) +.933*rxt(k,4076)*y(k,810) +.294*rxt(k,4098)*y(k,521) +.618*rxt(k,4154)*y(k,574)) &
*y(k,696) + (.153*rxt(k,3927)*y(k,779) +.153*rxt(k,3938)*y(k,379) +.117*rxt(k,4050)*y(k,439))*y(k,437) &
 + (.030*rxt(k,595)*y(k,586) +.060*rxt(k,1127)*y(k,33))*y(k,839) +.060*rxt(k,1)*y(k,59) +.207*rxt(k,4)*y(k,60) &
 +.338*rxt(k,7)*y(k,62) +.758*rxt(k,10)*y(k,64) +.756*rxt(k,13)*y(k,66) +.078*rxt(k,16)*y(k,94) +.040*rxt(k,20) &
*y(k,186) +.120*rxt(k,22)*y(k,192) +.474*rxt(k,25)*y(k,200) +.495*rxt(k,27)*y(k,266) +.159*rxt(k,30)*y(k,270) &
 +.238*rxt(k,33)*y(k,332) +.252*rxt(k,44)*y(k,412) +.372*rxt(k,46)*y(k,414) +.080*rxt(k,61)*y(k,476) &
 +.078*rxt(k,64)*y(k,531) +1.116*rxt(k,79)*y(k,557) +.159*rxt(k,96)*y(k,646) +.080*rxt(k,99)*y(k,649) &
 +.321*rxt(k,102)*y(k,653) +.138*rxt(k,105)*y(k,655) +.040*rxt(k,107)*y(k,659) +.078*rxt(k,109)*y(k,663) &
 +.261*rxt(k,126)*y(k,731) +.153*rxt(k,131)*y(k,733) +.140*rxt(k,134)*y(k,734) +.378*rxt(k,136)*y(k,735) &
 +.372*rxt(k,139)*y(k,736) +.308*rxt(k,146)*y(k,755) +.510*rxt(k,153)*y(k,761) +.324*rxt(k,161)*y(k,764) &
 +.225*rxt(k,165)*y(k,765) +.860*rxt(k,167)*y(k,766) +.480*rxt(k,171)*y(k,767)
         loss(k,924) = ((rxt(k,495) +rxt(k,496) +rxt(k,497) +rxt(k,498))* y(k,705) + rxt(k,173))* y(k,770)
         prod(k,924) = (rxt(k,4497)*y(k,690) +rxt(k,4499)*y(k,693) +rxt(k,4500)*y(k,620) +rxt(k,4501)*y(k,758) + &
rxt(k,4502)*y(k,370) +rxt(k,4503)*y(k,589) +.500*rxt(k,4504)*y(k,633) +.500*rxt(k,4505)*y(k,768) + &
.500*rxt(k,4506)*y(k,769))*y(k,876)
         loss(k,801) = ((rxt(k,2066) +rxt(k,2067) +rxt(k,2068))* y(k,693) + (rxt(k,2062) +rxt(k,2063) +rxt(k,2064) +rxt(k,2065)) &
* y(k,696) + (rxt(k,2059) +rxt(k,2060) +rxt(k,2061))* y(k,705) +rxt(k,2069)* y(k,839))* y(k,771)
         prod(k,801) = 0.
         loss(k,518) = ((rxt(k,2957) +rxt(k,2958) +rxt(k,2959) +rxt(k,2960))* y(k,705))* y(k,772)
         prod(k,518) = 0.
         loss(k,424) = ((rxt(k,2600) +rxt(k,2601) +rxt(k,2602))* y(k,705))* y(k,773)
         prod(k,424) = 0.
         loss(k,116) = (rxt(k,4105)* y(k,705))* y(k,774)
         prod(k,116) = 0.
         loss(k,117) = (rxt(k,4106)* y(k,705))* y(k,775)
         prod(k,117) = 0.
         loss(k,770) = ((rxt(k,2199) +rxt(k,2200) +rxt(k,2201))* y(k,693) + (rxt(k,2197) +rxt(k,2198))* y(k,696) + (rxt(k,2195) + &
rxt(k,2196))* y(k,705) +rxt(k,2202)* y(k,839))* y(k,776)
         prod(k,770) = 0.
         loss(k,2) = 0.
         prod(k,2) =rxt(k,228)*y(k,705)
         loss(k,88) = (rxt(k,3872)* y(k,705))* y(k,778)
         prod(k,88) = 0.
         loss(k,776) = ((rxt(k,3927) +rxt(k,3928) +rxt(k,3929))* y(k,437) + (rxt(k,3924) +rxt(k,3925))* y(k,693) + (rxt(k,3921) + &
rxt(k,3922) +rxt(k,3923))* y(k,696) + (rxt(k,3919) +rxt(k,3920))* y(k,705) +rxt(k,3926)* y(k,839))* y(k,779)
         prod(k,776) = 0.
         loss(k,871) = ((rxt(k,1214) +rxt(k,1215) +rxt(k,1216))* y(k,693) + (rxt(k,1210) +rxt(k,1211) +rxt(k,1212) +rxt(k,1213)) &
* y(k,696) + (rxt(k,1207) +rxt(k,1208) +rxt(k,1209))* y(k,705) +rxt(k,1217)* y(k,839))* y(k,780)
         prod(k,871) = 0.
         loss(k,886) = ((rxt(k,1455) +rxt(k,1456) +rxt(k,1457) +rxt(k,1458))* y(k,693) + (rxt(k,1450) +rxt(k,1451) +rxt(k,1452) + &
rxt(k,1453) +rxt(k,1454))* y(k,696) + (rxt(k,1446) +rxt(k,1447) +rxt(k,1448) +rxt(k,1449))* y(k,705) +rxt(k,1459) &
* y(k,839))* y(k,781)
         prod(k,886) = 0.
         loss(k,883) = ((rxt(k,1867) +rxt(k,1868) +rxt(k,1869) +rxt(k,1870))* y(k,693) + (rxt(k,1863) +rxt(k,1864) +rxt(k,1865) + &
rxt(k,1866))* y(k,696) + (rxt(k,1860) +rxt(k,1861) +rxt(k,1862))* y(k,705) +rxt(k,1871)* y(k,839))* y(k,782)
         prod(k,883) = 0.
         loss(k,820) = ((rxt(k,1879) +rxt(k,1880) +rxt(k,1881))* y(k,693) + (rxt(k,1875) +rxt(k,1876) +rxt(k,1877) +rxt(k,1878)) &
* y(k,696) + (rxt(k,1872) +rxt(k,1873) +rxt(k,1874))* y(k,705) +rxt(k,1882)* y(k,839))* y(k,783)
         prod(k,820) = 0.
         loss(k,771) = ((rxt(k,295) +rxt(k,296) +rxt(k,297))* y(k,693) + (rxt(k,1112) +rxt(k,1113))* y(k,696) + (rxt(k,1110) + &
rxt(k,1111))* y(k,705) +rxt(k,1114)* y(k,839))* y(k,784)
         prod(k,771) = 0.
         loss(k,881) = ((rxt(k,1397) +rxt(k,1398) +rxt(k,1399))* y(k,693) + (rxt(k,1392) +rxt(k,1393) +rxt(k,1394) +rxt(k,1395) + &
rxt(k,1396))* y(k,696) + (rxt(k,1389) +rxt(k,1390) +rxt(k,1391))* y(k,705) +rxt(k,1400)* y(k,839))* y(k,785)
         prod(k,881) = 0.
         loss(k,872) = ((rxt(k,1729) +rxt(k,1730) +rxt(k,1731))* y(k,693) + (rxt(k,1724) +rxt(k,1725) +rxt(k,1726) +rxt(k,1727) + &
rxt(k,1728))* y(k,696) + (rxt(k,1721) +rxt(k,1722) +rxt(k,1723))* y(k,705) +rxt(k,1732)* y(k,839))* y(k,786)
         prod(k,872) = 0.
         loss(k,877) = ((rxt(k,1832) +rxt(k,1833) +rxt(k,1834))* y(k,693) + (rxt(k,1827) +rxt(k,1828) +rxt(k,1829) +rxt(k,1830) + &
rxt(k,1831))* y(k,696) + (rxt(k,1824) +rxt(k,1825) +rxt(k,1826))* y(k,705) +rxt(k,1835)* y(k,839))* y(k,787)
         prod(k,877) = 0.
         loss(k,835) = ((rxt(k,1193) +rxt(k,1194) +rxt(k,1195))* y(k,693) + (rxt(k,1189) +rxt(k,1190) +rxt(k,1191) +rxt(k,1192)) &
* y(k,696) + (rxt(k,1186) +rxt(k,1187) +rxt(k,1188))* y(k,705) +rxt(k,1196)* y(k,839))* y(k,788)
         prod(k,835) = 0.
         loss(k,788) = ((rxt(k,1407) +rxt(k,1408) +rxt(k,1409))* y(k,693) + (rxt(k,1404) +rxt(k,1405) +rxt(k,1406))* y(k,696) &
 + (rxt(k,1401) +rxt(k,1402) +rxt(k,1403))* y(k,705) +rxt(k,1410)* y(k,839))* y(k,789)
         prod(k,788) = 0.
         loss(k,809) = ((rxt(k,1740) +rxt(k,1741) +rxt(k,1742))* y(k,693) + (rxt(k,1736) +rxt(k,1737) +rxt(k,1738) +rxt(k,1739)) &
* y(k,696) + (rxt(k,1733) +rxt(k,1734) +rxt(k,1735))* y(k,705) +rxt(k,1743)* y(k,839))* y(k,790)
         prod(k,809) = 0.
         loss(k,825) = ((rxt(k,1890) +rxt(k,1891) +rxt(k,1892))* y(k,693) + (rxt(k,1886) +rxt(k,1887) +rxt(k,1888) +rxt(k,1889)) &
* y(k,696) + (rxt(k,1883) +rxt(k,1884) +rxt(k,1885))* y(k,705) +rxt(k,1893)* y(k,839))* y(k,791)
         prod(k,825) = 0.
         loss(k,900) = ((rxt(k,1373) +rxt(k,1374) +rxt(k,1375))* y(k,693) + (rxt(k,1368) +rxt(k,1369) +rxt(k,1370) +rxt(k,1371) + &
rxt(k,1372))* y(k,696) + (rxt(k,1365) +rxt(k,1366) +rxt(k,1367))* y(k,705) +rxt(k,1376)* y(k,839))* y(k,792)
         prod(k,900) = 0.
         loss(k,906) = ((rxt(k,1777) +rxt(k,1778) +rxt(k,1779))* y(k,693) + (rxt(k,1772) +rxt(k,1773) +rxt(k,1774) +rxt(k,1775) + &
rxt(k,1776))* y(k,696) + (rxt(k,1769) +rxt(k,1770) +rxt(k,1771))* y(k,705) +rxt(k,1780)* y(k,839))* y(k,793)
         prod(k,906) = 0.
         loss(k,867) = ((rxt(k,1716) +rxt(k,1717) +rxt(k,1718) +rxt(k,1719))* y(k,693) + (rxt(k,1712) +rxt(k,1713) +rxt(k,1714) + &
rxt(k,1715))* y(k,696) + (rxt(k,1709) +rxt(k,1710) +rxt(k,1711))* y(k,705) +rxt(k,1720)* y(k,839))* y(k,794)
         prod(k,867) = 0.
         loss(k,833) = ((rxt(k,2000) +rxt(k,2001) +rxt(k,2002))* y(k,693) + (rxt(k,1996) +rxt(k,1997) +rxt(k,1998) +rxt(k,1999)) &
* y(k,696) + (rxt(k,1993) +rxt(k,1994) +rxt(k,1995))* y(k,705) +rxt(k,2003)* y(k,839))* y(k,795)
         prod(k,833) = 0.
         loss(k,789) = ((rxt(k,1901) +rxt(k,1902))* y(k,693) + (rxt(k,1897) +rxt(k,1898) +rxt(k,1899) +rxt(k,1900))* y(k,696) &
 + (rxt(k,1894) +rxt(k,1895) +rxt(k,1896))* y(k,705) +rxt(k,1903)* y(k,839))* y(k,796)
         prod(k,789) = 0.
         loss(k,831) = ((rxt(k,1957) +rxt(k,1958) +rxt(k,1959))* y(k,693) + (rxt(k,1953) +rxt(k,1954) +rxt(k,1955) +rxt(k,1956)) &
* y(k,696) + (rxt(k,1950) +rxt(k,1951) +rxt(k,1952))* y(k,705) +rxt(k,1960)* y(k,839))* y(k,797)
         prod(k,831) = 0.
         loss(k,875) = ((rxt(k,1385) +rxt(k,1386) +rxt(k,1387))* y(k,693) + (rxt(k,1380) +rxt(k,1381) +rxt(k,1382) +rxt(k,1383) + &
rxt(k,1384))* y(k,696) + (rxt(k,1377) +rxt(k,1378) +rxt(k,1379))* y(k,705) +rxt(k,1388)* y(k,839))* y(k,798)
         prod(k,875) = 0.
         loss(k,839) = ((rxt(k,1764) +rxt(k,1765) +rxt(k,1766) +rxt(k,1767))* y(k,693) + (rxt(k,1759) +rxt(k,1760) +rxt(k,1761) + &
rxt(k,1762) +rxt(k,1763))* y(k,696) + (rxt(k,1756) +rxt(k,1757) +rxt(k,1758))* y(k,705) +rxt(k,1768)* y(k,839)) &
* y(k,799)
         prod(k,839) = 0.
         loss(k,790) = ((rxt(k,2100) +rxt(k,2101))* y(k,693) + (rxt(k,2096) +rxt(k,2097) +rxt(k,2098) +rxt(k,2099))* y(k,696) &
 + (rxt(k,2093) +rxt(k,2094) +rxt(k,2095))* y(k,705) +rxt(k,2102)* y(k,839))* y(k,800)
         prod(k,790) = 0.
         loss(k,791) = ((rxt(k,2120) +rxt(k,2121))* y(k,693) + (rxt(k,2116) +rxt(k,2117) +rxt(k,2118) +rxt(k,2119))* y(k,696) &
 + (rxt(k,2113) +rxt(k,2114) +rxt(k,2115))* y(k,705) +rxt(k,2122)* y(k,839))* y(k,801)
         prod(k,791) = 0.
         loss(k,793) = ((rxt(k,2140) +rxt(k,2141))* y(k,693) + (rxt(k,2136) +rxt(k,2137) +rxt(k,2138) +rxt(k,2139))* y(k,696) &
 + (rxt(k,2133) +rxt(k,2134) +rxt(k,2135))* y(k,705) +rxt(k,2142)* y(k,839))* y(k,802)
         prod(k,793) = 0.
         loss(k,794) = ((rxt(k,2160) +rxt(k,2161))* y(k,693) + (rxt(k,2156) +rxt(k,2157) +rxt(k,2158) +rxt(k,2159))* y(k,696) &
 + (rxt(k,2153) +rxt(k,2154) +rxt(k,2155))* y(k,705) +rxt(k,2162)* y(k,839))* y(k,803)
         prod(k,794) = 0.
         loss(k,795) = ((rxt(k,2170) +rxt(k,2171))* y(k,693) + (rxt(k,2166) +rxt(k,2167) +rxt(k,2168) +rxt(k,2169))* y(k,696) &
 + (rxt(k,2163) +rxt(k,2164) +rxt(k,2165))* y(k,705) +rxt(k,2172)* y(k,839))* y(k,804)
         prod(k,795) = 0.
         loss(k,281) = ((rxt(k,2961) +rxt(k,2962) +rxt(k,2963))* y(k,705))* y(k,805)
         prod(k,281) = 0.
         loss(k,746) = (rxt(k,3989)* y(k,546) +rxt(k,3987)* y(k,693) + (rxt(k,3984) +rxt(k,3985) +rxt(k,3986))* y(k,705)) &
* y(k,806)
         prod(k,746) = 0.
         loss(k,640) = ((rxt(k,2252) +rxt(k,2253) +rxt(k,2254) +rxt(k,2255))* y(k,705))* y(k,807)
         prod(k,640) = 0.
         loss(k,220) = ((rxt(k,309) +rxt(k,310) +rxt(k,311))* y(k,705))* y(k,808)
         prod(k,220) = 0.
         loss(k,89) = (rxt(k,4132)* y(k,705))* y(k,809)
         prod(k,89) = 0.
         loss(k,750) = (rxt(k,4082)* y(k,546) + (rxt(k,4079) +rxt(k,4080) +rxt(k,4081))* y(k,693) + (rxt(k,4076) +rxt(k,4077) + &
rxt(k,4078))* y(k,696) + (rxt(k,4074) +rxt(k,4075))* y(k,705))* y(k,810)
         prod(k,750) = 0.
         loss(k,430) = ((rxt(k,3052) +rxt(k,3053) +rxt(k,3054))* y(k,705))* y(k,811)
         prod(k,430) = 0.
         loss(k,666) = ((rxt(k,3692) +rxt(k,3693) +rxt(k,3694) +rxt(k,3695))* y(k,705))* y(k,812)
         prod(k,666) = 0.
         loss(k,581) = ((rxt(k,3336) +rxt(k,3337) +rxt(k,3338) +rxt(k,3339))* y(k,705))* y(k,813)
         prod(k,581) = 0.
         loss(k,582) = ((rxt(k,3688) +rxt(k,3689) +rxt(k,3690) +rxt(k,3691))* y(k,705))* y(k,814)
         prod(k,582) = 0.
         loss(k,583) = ((rxt(k,3423) +rxt(k,3424) +rxt(k,3425) +rxt(k,3426))* y(k,705))* y(k,815)
         prod(k,583) = 0.
         loss(k,584) = ((rxt(k,2290) +rxt(k,2291) +rxt(k,2292) +rxt(k,2293))* y(k,705))* y(k,816)
         prod(k,584) = 0.
         loss(k,715) = ((rxt(k,3667) +rxt(k,3668) +rxt(k,3669) +rxt(k,3670) +rxt(k,3671))* y(k,705))* y(k,817)
         prod(k,715) = 0.
         loss(k,667) = ((rxt(k,3663) +rxt(k,3664) +rxt(k,3665) +rxt(k,3666))* y(k,705))* y(k,818)
         prod(k,667) = 0.
         loss(k,87) = (rxt(k,3946)* y(k,705))* y(k,819)
         prod(k,87) = (rxt(k,151) +rxt(k,3965)*y(k,705))*y(k,760)
         loss(k,585) = ((rxt(k,3572) +rxt(k,3573) +rxt(k,3574) +rxt(k,3575))* y(k,705))* y(k,820)
         prod(k,585) = 0.
         loss(k,642) = ((rxt(k,3332) +rxt(k,3333) +rxt(k,3334) +rxt(k,3335))* y(k,705))* y(k,821)
         prod(k,642) = 0.
         loss(k,643) = ((rxt(k,3711) +rxt(k,3712) +rxt(k,3713) +rxt(k,3714))* y(k,705))* y(k,822)
         prod(k,643) = 0.
         loss(k,586) = ((rxt(k,3181) +rxt(k,3182) +rxt(k,3183) +rxt(k,3184))* y(k,705))* y(k,823)
         prod(k,586) = 0.
         loss(k,587) = ((rxt(k,3416) +rxt(k,3417) +rxt(k,3418) +rxt(k,3419))* y(k,705))* y(k,824)
         prod(k,587) = 0.
         loss(k,431) = ((rxt(k,2814) +rxt(k,2815) +rxt(k,2816) +rxt(k,2817))* y(k,705))* y(k,825)
         prod(k,431) = 0.
         loss(k,282) = ((rxt(k,2578) +rxt(k,2579) +rxt(k,2580))* y(k,705))* y(k,826)
         prod(k,282) = 0.
         loss(k,721) = ((rxt(k,4172) +rxt(k,4173) +rxt(k,4174))* y(k,693) + (rxt(k,4169) +rxt(k,4170) +rxt(k,4171))* y(k,705)) &
* y(k,827)
         prod(k,721) = 0.
         loss(k,334) = ((rxt(k,2724) +rxt(k,2725) +rxt(k,2726))* y(k,705))* y(k,828)
         prod(k,334) = 0.
         loss(k,737) = (rxt(k,3895)* y(k,546) + (rxt(k,3893) +rxt(k,3894))* y(k,693) + (rxt(k,3891) +rxt(k,3892))* y(k,696) &
 + (rxt(k,3889) +rxt(k,3890))* y(k,705))* y(k,829)
         prod(k,737) = 0.
         loss(k,433) = ((rxt(k,3350) +rxt(k,3351) +rxt(k,3352))* y(k,705))* y(k,830)
         prod(k,433) = 0.
         loss(k,160) = ((rxt(k,2518) +rxt(k,2519))* y(k,705))* y(k,831)
         prod(k,160) = 0.
         loss(k,589) = ((rxt(k,3300) +rxt(k,3301) +rxt(k,3302) +rxt(k,3303))* y(k,705))* y(k,832)
         prod(k,589) = 0.
         loss(k,644) = ((rxt(k,2173) +rxt(k,2174) +rxt(k,2175) +rxt(k,2176))* y(k,705))* y(k,833)
         prod(k,644) = 0.
         loss(k,434) = ((rxt(k,3420) +rxt(k,3421) +rxt(k,3422))* y(k,705))* y(k,834)
         prod(k,434) = 0.
         loss(k,590) = ((rxt(k,3576) +rxt(k,3577) +rxt(k,3578) +rxt(k,3579))* y(k,705))* y(k,835)
         prod(k,590) = 0.
         loss(k,670) = ((rxt(k,3779) +rxt(k,3780) +rxt(k,3781) +rxt(k,3782) +rxt(k,3783))* y(k,705))* y(k,836)
         prod(k,670) = 0.
         loss(k,779) = ((rxt(k,2632) +rxt(k,2633) +rxt(k,2634))* y(k,693) + (rxt(k,2630) +rxt(k,2631))* y(k,696) + (rxt(k,2627) + &
rxt(k,2628) +rxt(k,2629))* y(k,705) +rxt(k,2635)* y(k,839))* y(k,837)
         prod(k,779) = 0.
         loss(k,78) = ( + rxt(k,390) + rxt(k,391))* y(k,838)
         prod(k,78) =rxt(k,118)*y(k,696)
         loss(k,918) = (rxt(k,1789)* y(k,26) + (rxt(k,1126) +rxt(k,1127) +rxt(k,1128) +rxt(k,1129))* y(k,33) +rxt(k,1228)* y(k,49) &
 +rxt(k,1092)* y(k,52) +rxt(k,1970)* y(k,53) +rxt(k,2092)* y(k,55) +rxt(k,2112)* y(k,56) +rxt(k,2132)* y(k,57) &
 +rxt(k,2152)* y(k,58) +rxt(k,1936)* y(k,67) +rxt(k,1494)* y(k,71) +rxt(k,1244)* y(k,72) +rxt(k,1434)* y(k,81) &
 +rxt(k,1470)* y(k,82) +rxt(k,1799)* y(k,83) +rxt(k,1141)* y(k,84) +rxt(k,1577)* y(k,101) +rxt(k,1543)* y(k,113) &
 +rxt(k,1290)* y(k,120) +rxt(k,1319)* y(k,122) +rxt(k,1697)* y(k,123) +rxt(k,1926)* y(k,124) +rxt(k,1811) &
* y(k,127) +rxt(k,1914)* y(k,128) +rxt(k,1532)* y(k,136) +rxt(k,1602)* y(k,141) +rxt(k,1301)* y(k,168) &
 +rxt(k,3589)* y(k,172) +rxt(k,1164)* y(k,176) +rxt(k,1312)* y(k,177) +rxt(k,1567)* y(k,178) +rxt(k,1174) &
* y(k,180) +rxt(k,1330)* y(k,181) +rxt(k,1616)* y(k,182) +rxt(k,1849)* y(k,183) +rxt(k,1663)* y(k,202) &
 +rxt(k,1556)* y(k,208) +rxt(k,1255)* y(k,213) +rxt(k,1507)* y(k,215) +rxt(k,1688)* y(k,216) +rxt(k,1982) &
* y(k,219) +rxt(k,2014)* y(k,249) +rxt(k,1627)* y(k,257) +rxt(k,1153)* y(k,263) +rxt(k,1268)* y(k,264) &
 +rxt(k,1520)* y(k,265) +rxt(k,1823)* y(k,267) +rxt(k,1676)* y(k,269) +rxt(k,1423)* y(k,285) +rxt(k,1640) &
* y(k,290) +rxt(k,1949)* y(k,291) +rxt(k,1280)* y(k,312) +rxt(k,1483)* y(k,317) +rxt(k,2517)* y(k,344) &
 +rxt(k,2537)* y(k,346) +rxt(k,2029)* y(k,355) +rxt(k,3445)* y(k,356) +rxt(k,2247)* y(k,360) +rxt(k,3238) &
* y(k,363) +rxt(k,3937)* y(k,379) +rxt(k,1109)* y(k,385) +rxt(k,1342)* y(k,386) +rxt(k,1652)* y(k,387) &
 +rxt(k,1185)* y(k,389) +rxt(k,1352)* y(k,391) +rxt(k,1708)* y(k,392) +rxt(k,1364)* y(k,395) +rxt(k,1755) &
* y(k,396) +rxt(k,1589)* y(k,398) +rxt(k,1859)* y(k,400) +rxt(k,1992)* y(k,404) +rxt(k,3460)* y(k,436) &
 +rxt(k,4049)* y(k,439) +rxt(k,3882)* y(k,441) +rxt(k,2574)* y(k,447) +rxt(k,1445)* y(k,456) +rxt(k,1206) &
* y(k,457) +rxt(k,2058)* y(k,479) +rxt(k,2796)* y(k,501) + (rxt(k,1069) +rxt(k,1070) +rxt(k,1071))* y(k,512) &
 +rxt(k,2936)* y(k,514) +rxt(k,3554)* y(k,529) +rxt(k,2645)* y(k,552) +rxt(k,3018)* y(k,554) +rxt(k,3120) &
* y(k,567) +rxt(k,3251)* y(k,569) +rxt(k,1101)* y(k,585) + (rxt(k,595) +rxt(k,596) +rxt(k,597))* y(k,586) &
 +rxt(k,3480)* y(k,587) +rxt(k,3758)* y(k,588) + (rxt(k,550) +rxt(k,2575))* y(k,590) +rxt(k,2616)* y(k,591) &
 +rxt(k,2736)* y(k,595) +rxt(k,2626)* y(k,606) +rxt(k,2806)* y(k,628) +rxt(k,3809)* y(k,651) +rxt(k,3796) &
* y(k,652) +rxt(k,559)* y(k,664) +rxt(k,3108)* y(k,668) +rxt(k,215)* y(k,690) + (rxt(k,216) +rxt(k,382)) &
* y(k,691) +rxt(k,381)* y(k,696) +rxt(k,1079)* y(k,739) +rxt(k,2069)* y(k,771) +rxt(k,2202)* y(k,776) &
 +rxt(k,3926)* y(k,779) +rxt(k,1217)* y(k,780) +rxt(k,1459)* y(k,781) +rxt(k,1871)* y(k,782) +rxt(k,1882) &
* y(k,783) +rxt(k,1114)* y(k,784) +rxt(k,1400)* y(k,785) +rxt(k,1732)* y(k,786) +rxt(k,1835)* y(k,787) &
 +rxt(k,1196)* y(k,788) +rxt(k,1410)* y(k,789) +rxt(k,1743)* y(k,790) +rxt(k,1893)* y(k,791) +rxt(k,1376) &
* y(k,792) +rxt(k,1780)* y(k,793) +rxt(k,1720)* y(k,794) +rxt(k,2003)* y(k,795) +rxt(k,1903)* y(k,796) &
 +rxt(k,1960)* y(k,797) +rxt(k,1388)* y(k,798) +rxt(k,1768)* y(k,799) +rxt(k,2102)* y(k,800) +rxt(k,2122) &
* y(k,801) +rxt(k,2142)* y(k,802) +rxt(k,2162)* y(k,803) +rxt(k,2172)* y(k,804) +rxt(k,2635)* y(k,837) &
 +rxt(k,2044)* y(k,879) +rxt(k,2082)* y(k,881) + rxt(k,214))* y(k,839)
         prod(k,918) =rxt(k,58)*y(k,448) +rxt(k,3864)*y(k,556) +2.000*rxt(k,101)*y(k,650) +rxt(k,113)*y(k,691) +rxt(k,115) &
*y(k,693) +rxt(k,119)*y(k,696) +rxt(k,391)*y(k,838)
         loss(k,802) = (rxt(k,476)* y(k,691) + rxt(k,477))* y(k,840)
         prod(k,802) = (rxt(k,4507)*y(k,690) +rxt(k,4509)*y(k,693) +rxt(k,4510)*y(k,620) +rxt(k,4511)*y(k,758) + &
rxt(k,4512)*y(k,370) +rxt(k,4513)*y(k,589) +.500*rxt(k,4514)*y(k,633) +.500*rxt(k,4515)*y(k,768) + &
.500*rxt(k,4516)*y(k,769))*y(k,870) +.762*rxt(k,309)*y(k,808)*y(k,705)
         loss(k,915) = (rxt(k,479)* y(k,548) +rxt(k,478)* y(k,691) + rxt(k,480))* y(k,841)
         prod(k,915) = (rxt(k,445)*y(k,690) +.880*rxt(k,446)*y(k,548) +2.000*rxt(k,448)*y(k,693) +2.000*rxt(k,450)*y(k,633) + &
rxt(k,452)*y(k,768) +rxt(k,453)*y(k,769) +rxt(k,454)*y(k,620) +2.000*rxt(k,456)*y(k,758) + &
2.000*rxt(k,457)*y(k,370) +2.000*rxt(k,473)*y(k,589) +2.000*rxt(k,3961)*y(k,427))*y(k,370) &
 + (.400*rxt(k,512)*y(k,705) +rxt(k,514)*y(k,693))*y(k,446) +rxt(k,515)*y(k,705)*y(k,694) +.800*rxt(k,125) &
*y(k,709)
         loss(k,759) = (rxt(k,4222)* y(k,370) +rxt(k,4218)* y(k,548) +rxt(k,4223)* y(k,589) +rxt(k,4220)* y(k,620) +rxt(k,4224) &
* y(k,633) +rxt(k,4217)* y(k,690) +rxt(k,4219)* y(k,693) +rxt(k,4221)* y(k,758) +rxt(k,4225)* y(k,768) &
 +rxt(k,4226)* y(k,769))* y(k,842)
         prod(k,759) = (1.408*rxt(k,244)*y(k,738) +.210*rxt(k,251)*y(k,191) +4.665*rxt(k,268)*y(k,114) +2.484*rxt(k,276)*y(k,86) + &
1.182*rxt(k,283)*y(k,85) +1.384*rxt(k,287)*y(k,88) +2.034*rxt(k,310)*y(k,808) +.072*rxt(k,316)*y(k,662) + &
.064*rxt(k,320)*y(k,504) +.220*rxt(k,324)*y(k,661) +.024*rxt(k,579)*y(k,761) +2.286*rxt(k,605)*y(k,193) + &
.808*rxt(k,608)*y(k,95) +1.029*rxt(k,611)*y(k,194) +2.310*rxt(k,618)*y(k,581) +.056*rxt(k,624)*y(k,96) + &
2.151*rxt(k,627)*y(k,115) +.891*rxt(k,630)*y(k,137) +.402*rxt(k,633)*y(k,195) +2.472*rxt(k,637)*y(k,209) + &
.028*rxt(k,647)*y(k,11) +.008*rxt(k,666)*y(k,97) +3.303*rxt(k,669)*y(k,104) +1.644*rxt(k,672)*y(k,116) + &
.204*rxt(k,675)*y(k,138) +1.713*rxt(k,678)*y(k,146) +.066*rxt(k,681)*y(k,196) +5.344*rxt(k,691)*y(k,102) + &
1.412*rxt(k,695)*y(k,210) +.080*rxt(k,699)*y(k,87) +1.986*rxt(k,705)*y(k,251) +.448*rxt(k,709)*y(k,5) + &
.104*rxt(k,713)*y(k,7) +.096*rxt(k,717)*y(k,12) +1.736*rxt(k,750)*y(k,91) +1.815*rxt(k,753)*y(k,109) + &
.087*rxt(k,756)*y(k,139) +.042*rxt(k,759)*y(k,197) +1.660*rxt(k,775)*y(k,129) +.832*rxt(k,779)*y(k,211) + &
.555*rxt(k,782)*y(k,292) +.003*rxt(k,786)*y(k,98) +.060*rxt(k,789)*y(k,89) +.918*rxt(k,792)*y(k,153) + &
1.122*rxt(k,795)*y(k,117) +.609*rxt(k,798)*y(k,147) +.152*rxt(k,822)*y(k,6) +.336*rxt(k,831)*y(k,134) + &
.048*rxt(k,834)*y(k,140) +.435*rxt(k,837)*y(k,154) +.036*rxt(k,840)*y(k,198) +.747*rxt(k,855)*y(k,130) + &
2.620*rxt(k,859)*y(k,145) +.752*rxt(k,863)*y(k,212) +.258*rxt(k,866)*y(k,293) +.028*rxt(k,873)*y(k,90) + &
.020*rxt(k,877)*y(k,92) +1.698*rxt(k,880)*y(k,111) +.810*rxt(k,883)*y(k,118) +.486*rxt(k,886)*y(k,148) + &
1.047*rxt(k,889)*y(k,185) +1.388*rxt(k,902)*y(k,77) +1.158*rxt(k,910)*y(k,103) +.360*rxt(k,913)*y(k,155) + &
1.041*rxt(k,937)*y(k,107) +1.569*rxt(k,963)*y(k,110) +.207*rxt(k,966)*y(k,133) +.141*rxt(k,992)*y(k,131) + &
1.797*rxt(k,995)*y(k,184) +.768*rxt(k,1020)*y(k,152) +1.515*rxt(k,1049)*y(k,158) +1.800*rxt(k,1094)*y(k,585) + &
.624*rxt(k,1144)*y(k,263) +1.870*rxt(k,1166)*y(k,180) +.020*rxt(k,1247)*y(k,213) +.332*rxt(k,1270)*y(k,312) + &
.207*rxt(k,1282)*y(k,120) +3.610*rxt(k,1314)*y(k,122) +2.685*rxt(k,1321)*y(k,181) +.888*rxt(k,1496)*y(k,215) + &
.135*rxt(k,1522)*y(k,136) +2.172*rxt(k,1546)*y(k,208) +.033*rxt(k,1580)*y(k,398) +2.577*rxt(k,1591)*y(k,141) + &
3.380*rxt(k,1605)*y(k,182) +.024*rxt(k,1654)*y(k,202) +1.730*rxt(k,1690)*y(k,123) +.033*rxt(k,1711)*y(k,794) + &
.464*rxt(k,1813)*y(k,267) +3.180*rxt(k,1838)*y(k,183) +.021*rxt(k,1861)*y(k,782) +.432*rxt(k,1873)*y(k,783) + &
2.454*rxt(k,1905)*y(k,128) +3.244*rxt(k,1917)*y(k,124) +.164*rxt(k,1938)*y(k,291) +.136*rxt(k,2017)*y(k,355) + &
.140*rxt(k,2032)*y(k,879) +.417*rxt(k,2060)*y(k,771) +1.266*rxt(k,2071)*y(k,881) +.201*rxt(k,2587)*y(k,192) + &
1.276*rxt(k,2595)*y(k,575) +3.216*rxt(k,2661)*y(k,583) +1.701*rxt(k,2665)*y(k,570) +1.872*rxt(k,2728)*y(k,595) + &
.069*rxt(k,2739)*y(k,94) +.384*rxt(k,2743)*y(k,270) +1.551*rxt(k,2762)*y(k,649) +1.244*rxt(k,2775)*y(k,563) + &
.876*rxt(k,2783)*y(k,523) +.812*rxt(k,2824)*y(k,579) +2.156*rxt(k,2832)*y(k,624) +3.120*rxt(k,2844)*y(k,268) + &
.006*rxt(k,2847)*y(k,100) +.816*rxt(k,2875)*y(k,648) +3.072*rxt(k,2897)*y(k,646) +2.316*rxt(k,2905)*y(k,663) + &
.432*rxt(k,2914)*y(k,647) +.204*rxt(k,2924)*y(k,584) +3.052*rxt(k,2943)*y(k,566) +1.852*rxt(k,2947)*y(k,642) + &
.522*rxt(k,2962)*y(k,805) +.104*rxt(k,2966)*y(k,474) +.476*rxt(k,2977)*y(k,179) +.204*rxt(k,2995)*y(k,289) + &
.020*rxt(k,2999)*y(k,653) +.756*rxt(k,3076)*y(k,186) +1.527*rxt(k,3079)*y(k,476) +2.121*rxt(k,3082)*y(k,332) + &
.340*rxt(k,3096)*y(k,294) +1.544*rxt(k,3111)*y(k,567) +2.120*rxt(k,3133)*y(k,562) +.004*rxt(k,3146)*y(k,722) + &
1.612*rxt(k,3160)*y(k,478) +2.289*rxt(k,3216)*y(k,565) +.456*rxt(k,3241)*y(k,569) +2.776*rxt(k,3254)*y(k,125) + &
.576*rxt(k,3265)*y(k,313) +3.300*rxt(k,3270)*y(k,568) +.988*rxt(k,3282)*y(k,593) +.128*rxt(k,3302)*y(k,832) + &
2.504*rxt(k,3358)*y(k,475) +1.539*rxt(k,3361)*y(k,156) +.105*rxt(k,3364)*y(k,157) +.132*rxt(k,3368)*y(k,142) + &
.044*rxt(k,3388)*y(k,333) +1.724*rxt(k,3391)*y(k,576) +1.164*rxt(k,3431)*y(k,417) +.152*rxt(k,3434)*y(k,356) + &
3.435*rxt(k,3448)*y(k,436) +.392*rxt(k,3463)*y(k,557) +.352*rxt(k,3467)*y(k,632) +1.674*rxt(k,3470)*y(k,587) + &
.027*rxt(k,3486)*y(k,571) +.408*rxt(k,3492)*y(k,237) +.092*rxt(k,3515)*y(k,226) +1.248*rxt(k,3525)*y(k,295) + &
1.890*rxt(k,3540)*y(k,529) +.140*rxt(k,3592)*y(k,108) +.592*rxt(k,3616)*y(k,232) +.060*rxt(k,3627)*y(k,297) + &
1.080*rxt(k,3644)*y(k,564) +.012*rxt(k,3647)*y(k,151) +.693*rxt(k,3654)*y(k,261) +1.184*rxt(k,3665)*y(k,818) + &
1.115*rxt(k,3669)*y(k,817) +.076*rxt(k,3686)*y(k,477) +.144*rxt(k,3698)*y(k,222) +.036*rxt(k,3717)*y(k,106) + &
.040*rxt(k,3725)*y(k,228) +.060*rxt(k,3737)*y(k,132) +.032*rxt(k,3744)*y(k,299) +.064*rxt(k,3764)*y(k,105) + &
.068*rxt(k,3768)*y(k,221) +.056*rxt(k,3772)*y(k,331) +3.295*rxt(k,3781)*y(k,836) +.104*rxt(k,4211)*y(k,766)) &
*y(k,705) + (1.932*rxt(k,1099)*y(k,585) +2.382*rxt(k,1151)*y(k,263) +2.805*rxt(k,1172)*y(k,180) + &
1.732*rxt(k,1289)*y(k,120) +3.610*rxt(k,1318)*y(k,122) +2.175*rxt(k,1328)*y(k,181) +1.008*rxt(k,1386)*y(k,798) + &
1.155*rxt(k,1505)*y(k,215) +.072*rxt(k,1530)*y(k,136) +2.202*rxt(k,1554)*y(k,208) +.132*rxt(k,1587)*y(k,398) + &
2.312*rxt(k,1600)*y(k,141) +.492*rxt(k,1614)*y(k,182) +.681*rxt(k,1661)*y(k,202) +2.175*rxt(k,1686)*y(k,216) + &
2.595*rxt(k,1695)*y(k,123) +.132*rxt(k,1718)*y(k,794) +1.336*rxt(k,1809)*y(k,127) +2.382*rxt(k,1821)*y(k,267) + &
.360*rxt(k,1847)*y(k,183) +.044*rxt(k,1869)*y(k,782) +1.086*rxt(k,1880)*y(k,783) +1.332*rxt(k,1912)*y(k,128) + &
3.284*rxt(k,1923)*y(k,124) +.663*rxt(k,1947)*y(k,291) +.008*rxt(k,2012)*y(k,249) +.276*rxt(k,2027)*y(k,355) + &
1.572*rxt(k,2042)*y(k,879) +1.290*rxt(k,2067)*y(k,771) +1.576*rxt(k,2079)*y(k,881) +2.805*rxt(k,2734)*y(k,595) + &
.104*rxt(k,2747)*y(k,270) +4.145*rxt(k,3117)*y(k,567) +2.784*rxt(k,3249)*y(k,569) +.030*rxt(k,3443)*y(k,356) + &
.336*rxt(k,3458)*y(k,436) +.252*rxt(k,3478)*y(k,587) +.275*rxt(k,3551)*y(k,529))*y(k,693) &
 + (.240*rxt(k,1148)*y(k,263) +.105*rxt(k,1276)*y(k,312) +2.328*rxt(k,1286)*y(k,120) +.300*rxt(k,1382)*y(k,798) + &
.250*rxt(k,1501)*y(k,215) +.285*rxt(k,1551)*y(k,208) +.180*rxt(k,1596)*y(k,141) +.240*rxt(k,1658)*y(k,202) + &
3.055*rxt(k,1683)*y(k,216) +2.308*rxt(k,1818)*y(k,267) +.480*rxt(k,1877)*y(k,783) +.020*rxt(k,1944)*y(k,291) + &
2.070*rxt(k,2022)*y(k,355) +.620*rxt(k,2037)*y(k,879) +.288*rxt(k,2064)*y(k,771) +.010*rxt(k,3440)*y(k,356)) &
*y(k,696) +2.880*rxt(k,23)*y(k,192) +rxt(k,32)*y(k,270) +1.920*rxt(k,62)*y(k,476) +rxt(k,97)*y(k,646) &
 +1.920*rxt(k,100)*y(k,649) +.351*rxt(k,104)*y(k,653) +.040*rxt(k,152)*y(k,761) +.181*rxt(k,169)*y(k,766)
         loss(k,713) = (rxt(k,4232)* y(k,370) +rxt(k,4228)* y(k,548) +rxt(k,4233)* y(k,589) +rxt(k,4230)* y(k,620) +rxt(k,4234) &
* y(k,633) +rxt(k,4227)* y(k,690) +rxt(k,4229)* y(k,693) +rxt(k,4231)* y(k,758) +rxt(k,4235)* y(k,768) &
 +rxt(k,4236)* y(k,769))* y(k,843)
         prod(k,713) = (.365*rxt(k,505)*y(k,756) +.780*rxt(k,534)*y(k,350) +.116*rxt(k,601)*y(k,359) +.008*rxt(k,1414)*y(k,285) + &
.780*rxt(k,2175)*y(k,833) +.336*rxt(k,2179)*y(k,383) +1.344*rxt(k,2183)*y(k,665) +1.265*rxt(k,2187)*y(k,706) + &
.225*rxt(k,2192)*y(k,746) +.268*rxt(k,2205)*y(k,676) +.344*rxt(k,2209)*y(k,572) +1.530*rxt(k,2213)*y(k,644) + &
1.085*rxt(k,2218)*y(k,704) +.200*rxt(k,2223)*y(k,721) +1.750*rxt(k,2228)*y(k,14) +.440*rxt(k,2233)*y(k,16) + &
1.280*rxt(k,2238)*y(k,32) +.240*rxt(k,2250)*y(k,373) +.404*rxt(k,2254)*y(k,807) +1.440*rxt(k,2258)*y(k,596) + &
rxt(k,2263)*y(k,697) +.185*rxt(k,2268)*y(k,711) +.200*rxt(k,2273)*y(k,719) +1.620*rxt(k,2278)*y(k,240) + &
.405*rxt(k,2283)*y(k,302) +1.212*rxt(k,2288)*y(k,324) +.240*rxt(k,2292)*y(k,816) +.240*rxt(k,2296)*y(k,667) + &
.208*rxt(k,2300)*y(k,376) +1.320*rxt(k,2304)*y(k,597) +.880*rxt(k,2309)*y(k,698) +.165*rxt(k,2314)*y(k,712) + &
1.535*rxt(k,2319)*y(k,241) +.380*rxt(k,2324)*y(k,303) +1.168*rxt(k,2329)*y(k,325) +.360*rxt(k,2333)*y(k,630) + &
.180*rxt(k,2337)*y(k,377) +1.275*rxt(k,2341)*y(k,598) +.840*rxt(k,2346)*y(k,699) +.155*rxt(k,2351)*y(k,713) + &
1.490*rxt(k,2356)*y(k,242) +.370*rxt(k,2361)*y(k,304) +1.144*rxt(k,2366)*y(k,326) +.480*rxt(k,2370)*y(k,119) + &
.160*rxt(k,2374)*y(k,378) +.964*rxt(k,2378)*y(k,599) +.775*rxt(k,2382)*y(k,700) +.145*rxt(k,2387)*y(k,714) + &
1.425*rxt(k,2392)*y(k,243) +.355*rxt(k,2397)*y(k,305) +1.112*rxt(k,2402)*y(k,327) +.144*rxt(k,2406)*y(k,380) + &
.916*rxt(k,2410)*y(k,600) +.720*rxt(k,2414)*y(k,701) +.135*rxt(k,2419)*y(k,715) +1.380*rxt(k,2424)*y(k,244) + &
.345*rxt(k,2429)*y(k,306) +1.092*rxt(k,2434)*y(k,328) +.132*rxt(k,2438)*y(k,381) +.872*rxt(k,2442)*y(k,601) + &
.675*rxt(k,2446)*y(k,702) +.125*rxt(k,2451)*y(k,716) +1.340*rxt(k,2456)*y(k,245) +.335*rxt(k,2461)*y(k,307) + &
1.068*rxt(k,2466)*y(k,329) +.124*rxt(k,2470)*y(k,382) +.836*rxt(k,2474)*y(k,602) +.635*rxt(k,2478)*y(k,703) + &
.120*rxt(k,2483)*y(k,717) +1.300*rxt(k,2488)*y(k,246) +.325*rxt(k,2493)*y(k,308) +1.048*rxt(k,2498)*y(k,330) + &
.340*rxt(k,3057)*y(k,369) +.756*rxt(k,3061)*y(k,372) +.545*rxt(k,3223)*y(k,496) +1.072*rxt(k,3306)*y(k,744) + &
.016*rxt(k,3348)*y(k,68) +1.016*rxt(k,3499)*y(k,23) +.875*rxt(k,3678)*y(k,466) +.185*rxt(k,3732)*y(k,169) + &
.570*rxt(k,3776)*y(k,463) +.116*rxt(k,4085)*y(k,443) +.116*rxt(k,4089)*y(k,692) +.116*rxt(k,4093)*y(k,438) + &
.116*rxt(k,4109)*y(k,371) +.504*rxt(k,4119)*y(k,656) +.840*rxt(k,4123)*y(k,426) +.840*rxt(k,4127)*y(k,718)) &
*y(k,705) +.004*rxt(k,1128)*y(k,839)*y(k,33)
         loss(k,714) = (rxt(k,4242)* y(k,370) +rxt(k,4238)* y(k,548) +rxt(k,4243)* y(k,589) +rxt(k,4240)* y(k,620) +rxt(k,4244) &
* y(k,633) +rxt(k,4237)* y(k,690) +rxt(k,4239)* y(k,693) +rxt(k,4241)* y(k,758) +rxt(k,4245)* y(k,768) &
 +rxt(k,4246)* y(k,769))* y(k,844)
         prod(k,714) = (.365*rxt(k,505)*y(k,756) +.780*rxt(k,534)*y(k,350) +1.044*rxt(k,601)*y(k,359) +.008*rxt(k,1414)*y(k,285) + &
.780*rxt(k,2175)*y(k,833) +.864*rxt(k,2179)*y(k,383) +.576*rxt(k,2183)*y(k,665) +1.265*rxt(k,2188)*y(k,706) + &
.335*rxt(k,2192)*y(k,746) +.688*rxt(k,2205)*y(k,676) +.884*rxt(k,2209)*y(k,572) +.655*rxt(k,2213)*y(k,644) + &
1.085*rxt(k,2219)*y(k,704) +.305*rxt(k,2223)*y(k,721) +1.750*rxt(k,2229)*y(k,14) +1.760*rxt(k,2234)*y(k,16) + &
1.280*rxt(k,2238)*y(k,32) +.620*rxt(k,2250)*y(k,373) +1.044*rxt(k,2254)*y(k,807) +.615*rxt(k,2258)*y(k,596) + &
rxt(k,2264)*y(k,697) +.280*rxt(k,2268)*y(k,711) +.305*rxt(k,2273)*y(k,719) +1.620*rxt(k,2279)*y(k,240) + &
1.615*rxt(k,2284)*y(k,302) +1.212*rxt(k,2288)*y(k,324) +.360*rxt(k,2292)*y(k,816) +.360*rxt(k,2296)*y(k,667) + &
.532*rxt(k,2300)*y(k,376) +.565*rxt(k,2304)*y(k,597) +.880*rxt(k,2310)*y(k,698) +.245*rxt(k,2314)*y(k,712) + &
1.535*rxt(k,2320)*y(k,241) +1.530*rxt(k,2325)*y(k,303) +1.168*rxt(k,2329)*y(k,325) +.540*rxt(k,2333)*y(k,630) + &
.468*rxt(k,2337)*y(k,377) +.545*rxt(k,2341)*y(k,598) +.840*rxt(k,2347)*y(k,699) +.235*rxt(k,2351)*y(k,713) + &
1.490*rxt(k,2357)*y(k,242) +1.485*rxt(k,2362)*y(k,304) +1.144*rxt(k,2366)*y(k,326) +.720*rxt(k,2370)*y(k,119) + &
.416*rxt(k,2374)*y(k,378) +.412*rxt(k,2378)*y(k,599) +.775*rxt(k,2382)*y(k,700) +.215*rxt(k,2387)*y(k,714) + &
1.425*rxt(k,2393)*y(k,243) +1.420*rxt(k,2398)*y(k,305) +1.112*rxt(k,2402)*y(k,327) +.376*rxt(k,2406)*y(k,380) + &
.392*rxt(k,2410)*y(k,600) +.720*rxt(k,2414)*y(k,701) +.200*rxt(k,2419)*y(k,715) +1.380*rxt(k,2425)*y(k,244) + &
1.380*rxt(k,2430)*y(k,306) +1.092*rxt(k,2434)*y(k,328) +.344*rxt(k,2438)*y(k,381) +.376*rxt(k,2442)*y(k,601) + &
.675*rxt(k,2446)*y(k,702) +.190*rxt(k,2451)*y(k,716) +1.340*rxt(k,2457)*y(k,245) +1.335*rxt(k,2462)*y(k,307) + &
1.068*rxt(k,2466)*y(k,329) +.316*rxt(k,2470)*y(k,382) +.356*rxt(k,2474)*y(k,602) +.635*rxt(k,2478)*y(k,703) + &
.175*rxt(k,2483)*y(k,717) +1.300*rxt(k,2489)*y(k,246) +1.295*rxt(k,2494)*y(k,308) +1.048*rxt(k,2498)*y(k,330) + &
.876*rxt(k,3057)*y(k,369) +.756*rxt(k,3061)*y(k,372) +.545*rxt(k,3223)*y(k,496) +1.072*rxt(k,3306)*y(k,744) + &
.016*rxt(k,3348)*y(k,68) +1.016*rxt(k,3499)*y(k,23) +.875*rxt(k,3679)*y(k,466) +.185*rxt(k,3733)*y(k,169) + &
.570*rxt(k,3777)*y(k,463) +1.044*rxt(k,4085)*y(k,443) +1.044*rxt(k,4089)*y(k,692) +1.044*rxt(k,4093)*y(k,438) + &
1.044*rxt(k,4109)*y(k,371) +.504*rxt(k,4119)*y(k,656) +.840*rxt(k,4123)*y(k,426) +.840*rxt(k,4127)*y(k,718)) &
*y(k,705) +.004*rxt(k,1128)*y(k,839)*y(k,33)
         loss(k,707) = (rxt(k,4252)* y(k,370) +rxt(k,4248)* y(k,548) +rxt(k,4253)* y(k,589) +rxt(k,4250)* y(k,620) +rxt(k,4254) &
* y(k,633) +rxt(k,4247)* y(k,690) +rxt(k,4249)* y(k,693) +rxt(k,4251)* y(k,758) +rxt(k,4255)* y(k,768) &
 +rxt(k,4256)* y(k,769))* y(k,845)
         prod(k,707) = (1.430*rxt(k,2193)*y(k,746) +1.330*rxt(k,2224)*y(k,721) +.755*rxt(k,2234)*y(k,16) + &
1.250*rxt(k,2269)*y(k,711) +1.365*rxt(k,2274)*y(k,719) +.720*rxt(k,2284)*y(k,302) +1.125*rxt(k,2315)*y(k,712) + &
.685*rxt(k,2325)*y(k,303) +1.080*rxt(k,2352)*y(k,713) +.670*rxt(k,2362)*y(k,304) +rxt(k,2388)*y(k,714) + &
.645*rxt(k,2398)*y(k,305) +.935*rxt(k,2420)*y(k,715) +.625*rxt(k,2430)*y(k,306) +.880*rxt(k,2452)*y(k,716) + &
.605*rxt(k,2462)*y(k,307) +.825*rxt(k,2484)*y(k,717) +.590*rxt(k,2494)*y(k,308))*y(k,705) &
 +.275*rxt(k,2076)*y(k,881)*y(k,696)
         loss(k,735) = (rxt(k,4262)* y(k,370) +rxt(k,4258)* y(k,548) +rxt(k,4263)* y(k,589) +rxt(k,4260)* y(k,620) +rxt(k,4264) &
* y(k,633) +rxt(k,4257)* y(k,690) +rxt(k,4259)* y(k,693) +rxt(k,4261)* y(k,758) +rxt(k,4265)* y(k,768) &
 +rxt(k,4266)* y(k,769))* y(k,846)
         prod(k,735) = (.914*rxt(k,493)*y(k,759) +.076*rxt(k,534)*y(k,350) +.092*rxt(k,2017)*y(k,355) +.174*rxt(k,2061)*y(k,771) + &
.925*rxt(k,2187)*y(k,706) +.795*rxt(k,2218)*y(k,704) +2.235*rxt(k,2228)*y(k,14) +.560*rxt(k,2233)*y(k,16) + &
.730*rxt(k,2263)*y(k,697) +2.075*rxt(k,2278)*y(k,240) +.515*rxt(k,2283)*y(k,302) +.645*rxt(k,2309)*y(k,698) + &
1.965*rxt(k,2319)*y(k,241) +.485*rxt(k,2324)*y(k,303) +.615*rxt(k,2346)*y(k,699) +1.905*rxt(k,2356)*y(k,242) + &
.470*rxt(k,2361)*y(k,304) +.565*rxt(k,2382)*y(k,700) +1.820*rxt(k,2392)*y(k,243) +.450*rxt(k,2397)*y(k,305) + &
.525*rxt(k,2414)*y(k,701) +1.770*rxt(k,2424)*y(k,244) +.440*rxt(k,2429)*y(k,306) +.495*rxt(k,2446)*y(k,702) + &
1.715*rxt(k,2456)*y(k,245) +.425*rxt(k,2461)*y(k,307) +.465*rxt(k,2478)*y(k,703) +1.660*rxt(k,2488)*y(k,246) + &
.410*rxt(k,2493)*y(k,308) +.687*rxt(k,2531)*y(k,346) +.024*rxt(k,2605)*y(k,528) +2.850*rxt(k,2607)*y(k,591) + &
.864*rxt(k,2619)*y(k,606) +1.268*rxt(k,2654)*y(k,641) +.588*rxt(k,2665)*y(k,570) +.450*rxt(k,2668)*y(k,368) + &
.088*rxt(k,2704)*y(k,626) +.276*rxt(k,2710)*y(k,592) +.252*rxt(k,2789)*y(k,501) +1.870*rxt(k,2798)*y(k,628) + &
.092*rxt(k,2820)*y(k,526) +.552*rxt(k,2829)*y(k,610) +.312*rxt(k,2833)*y(k,624) +.272*rxt(k,2845)*y(k,268) + &
.125*rxt(k,2871)*y(k,513) +2.412*rxt(k,2927)*y(k,514) +.080*rxt(k,2940)*y(k,505) +.044*rxt(k,2955)*y(k,743) + &
.356*rxt(k,2970)*y(k,615) +.236*rxt(k,3008)*y(k,554) +.272*rxt(k,3044)*y(k,459) +.387*rxt(k,3050)*y(k,347) + &
.200*rxt(k,3100)*y(k,668) +.144*rxt(k,3111)*y(k,567) +.028*rxt(k,3123)*y(k,367) +.044*rxt(k,3131)*y(k,727) + &
.232*rxt(k,3142)*y(k,616) +.004*rxt(k,3157)*y(k,492) +.150*rxt(k,3179)*y(k,460) +2.712*rxt(k,3228)*y(k,363) + &
2.720*rxt(k,3241)*y(k,569) +.015*rxt(k,3271)*y(k,568) +.028*rxt(k,3275)*y(k,362) +.012*rxt(k,3286)*y(k,408) + &
.993*rxt(k,3288)*y(k,171) +.123*rxt(k,3291)*y(k,617) +.784*rxt(k,3306)*y(k,744) +.500*rxt(k,3326)*y(k,461) + &
.008*rxt(k,3392)*y(k,576) +.090*rxt(k,3398)*y(k,618) +.744*rxt(k,3499)*y(k,23) +.051*rxt(k,3556)*y(k,619) + &
.092*rxt(k,3582)*y(k,172) +.036*rxt(k,3640)*y(k,611) +.033*rxt(k,3661)*y(k,612) +.005*rxt(k,3670)*y(k,817) + &
.640*rxt(k,3678)*y(k,466) +.078*rxt(k,3682)*y(k,458) +.964*rxt(k,3686)*y(k,477) +.360*rxt(k,3695)*y(k,812) + &
.027*rxt(k,3709)*y(k,627) +.024*rxt(k,3728)*y(k,613) +.024*rxt(k,3747)*y(k,629) +.415*rxt(k,3776)*y(k,463) + &
.005*rxt(k,3782)*y(k,836))*y(k,705) + (.048*rxt(k,1432)*y(k,81) +1.876*rxt(k,2535)*y(k,346) + &
2.226*rxt(k,2624)*y(k,606) +.357*rxt(k,2794)*y(k,501) +.048*rxt(k,3016)*y(k,554) +.024*rxt(k,3106)*y(k,668) + &
.010*rxt(k,3117)*y(k,567))*y(k,693) + (.486*rxt(k,2023)*y(k,355) +.378*rxt(k,2037)*y(k,879) + &
.304*rxt(k,2065)*y(k,771))*y(k,696)
         loss(k,710) = (rxt(k,4272)* y(k,370) +rxt(k,4268)* y(k,548) +rxt(k,4273)* y(k,589) +rxt(k,4270)* y(k,620) +rxt(k,4274) &
* y(k,633) +rxt(k,4267)* y(k,690) +rxt(k,4269)* y(k,693) +rxt(k,4271)* y(k,758) +rxt(k,4275)* y(k,768) &
 +rxt(k,4276)* y(k,769))* y(k,847)
         prod(k,710) = (.260*rxt(k,2175)*y(k,833) +.164*rxt(k,2183)*y(k,665) +.235*rxt(k,2187)*y(k,706) + &
.440*rxt(k,2192)*y(k,746) +1.640*rxt(k,2196)*y(k,776) +.085*rxt(k,2213)*y(k,644) +.135*rxt(k,2218)*y(k,704) + &
.135*rxt(k,2223)*y(k,721) +.225*rxt(k,2228)*y(k,14) +.180*rxt(k,2233)*y(k,16) +.120*rxt(k,2238)*y(k,32) + &
1.640*rxt(k,2241)*y(k,360) +.040*rxt(k,2258)*y(k,596) +.060*rxt(k,2263)*y(k,697) +.060*rxt(k,2268)*y(k,711) + &
.135*rxt(k,2273)*y(k,719) +.115*rxt(k,2278)*y(k,240) +.115*rxt(k,2283)*y(k,302) +.056*rxt(k,2288)*y(k,324) + &
.025*rxt(k,2304)*y(k,597) +.035*rxt(k,2309)*y(k,698) +.035*rxt(k,2314)*y(k,712) +.085*rxt(k,2319)*y(k,241) + &
.085*rxt(k,2324)*y(k,303) +.040*rxt(k,2329)*y(k,325) +.015*rxt(k,2341)*y(k,598) +.025*rxt(k,2346)*y(k,699) + &
.025*rxt(k,2351)*y(k,713) +.070*rxt(k,2356)*y(k,242) +.070*rxt(k,2361)*y(k,304) +.036*rxt(k,2366)*y(k,326) + &
.050*rxt(k,2392)*y(k,243) +.050*rxt(k,2397)*y(k,305) +.024*rxt(k,2402)*y(k,327) +.035*rxt(k,2424)*y(k,244) + &
.035*rxt(k,2429)*y(k,306) +.020*rxt(k,2434)*y(k,328) +.025*rxt(k,2456)*y(k,245) +.025*rxt(k,2461)*y(k,307) + &
.012*rxt(k,2466)*y(k,329) +.010*rxt(k,2488)*y(k,246) +.010*rxt(k,2493)*y(k,308) +.004*rxt(k,2498)*y(k,330) + &
1.420*rxt(k,4119)*y(k,656) +.543*rxt(k,4171)*y(k,827))*y(k,705) + (.660*rxt(k,2200)*y(k,776) + &
.660*rxt(k,2245)*y(k,360) +.924*rxt(k,4174)*y(k,827))*y(k,693)
         loss(k,838) = (rxt(k,4282)* y(k,370) +rxt(k,4278)* y(k,548) +rxt(k,4283)* y(k,589) +rxt(k,4280)* y(k,620) +rxt(k,4284) &
* y(k,633) +rxt(k,4277)* y(k,690) +rxt(k,4279)* y(k,693) +rxt(k,4281)* y(k,758) +rxt(k,4285)* y(k,768) &
 +rxt(k,4286)* y(k,769))* y(k,848)
         prod(k,838) = (.390*rxt(k,236)*y(k,512) +rxt(k,242)*y(k,511) +1.896*rxt(k,248)*y(k,678) +.441*rxt(k,256)*y(k,680) + &
.033*rxt(k,265)*y(k,681) +.024*rxt(k,268)*y(k,114) +.015*rxt(k,275)*y(k,86) +.004*rxt(k,286)*y(k,88) + &
.020*rxt(k,299)*y(k,519) +.138*rxt(k,304)*y(k,573) +1.512*rxt(k,307)*y(k,625) +.492*rxt(k,313)*y(k,517) + &
.508*rxt(k,319)*y(k,504) +2.048*rxt(k,323)*y(k,661) +1.754*rxt(k,331)*y(k,388) +.070*rxt(k,486)*y(k,757) + &
.284*rxt(k,492)*y(k,759) +.048*rxt(k,497)*y(k,770) +.060*rxt(k,500)*y(k,755) +.516*rxt(k,561)*y(k,582) + &
.415*rxt(k,574)*y(k,731) +2.574*rxt(k,579)*y(k,761) +2.340*rxt(k,604)*y(k,193) +2.920*rxt(k,607)*y(k,95) + &
.009*rxt(k,611)*y(k,194) +2.958*rxt(k,614)*y(k,280) +.261*rxt(k,617)*y(k,581) +.016*rxt(k,623)*y(k,96) + &
1.227*rxt(k,627)*y(k,115) +.063*rxt(k,630)*y(k,137) +.144*rxt(k,633)*y(k,195) +5.312*rxt(k,636)*y(k,209) + &
.624*rxt(k,640)*y(k,281) +2.511*rxt(k,643)*y(k,258) +.260*rxt(k,651)*y(k,24) +.004*rxt(k,655)*y(k,37) + &
.544*rxt(k,658)*y(k,506) +.006*rxt(k,662)*y(k,622) +.168*rxt(k,665)*y(k,97) +1.227*rxt(k,669)*y(k,104) + &
.315*rxt(k,672)*y(k,116) +1.086*rxt(k,675)*y(k,138) +.075*rxt(k,681)*y(k,196) +.531*rxt(k,684)*y(k,282) + &
.012*rxt(k,687)*y(k,319) +2.728*rxt(k,690)*y(k,102) +2.228*rxt(k,694)*y(k,210) +.856*rxt(k,698)*y(k,87) + &
1.608*rxt(k,702)*y(k,217) +.723*rxt(k,705)*y(k,251) +.428*rxt(k,709)*y(k,5) +.004*rxt(k,713)*y(k,7) + &
.020*rxt(k,716)*y(k,12) +.964*rxt(k,721)*y(k,15) +.076*rxt(k,725)*y(k,17) +.476*rxt(k,729)*y(k,76) + &
.135*rxt(k,732)*y(k,25) +.087*rxt(k,735)*y(k,47) +.052*rxt(k,738)*y(k,38) +.453*rxt(k,742)*y(k,507) + &
.004*rxt(k,745)*y(k,728) +.008*rxt(k,749)*y(k,91) +.201*rxt(k,753)*y(k,109) +.039*rxt(k,756)*y(k,139) + &
3.963*rxt(k,762)*y(k,207) +1.968*rxt(k,765)*y(k,225) +.147*rxt(k,768)*y(k,310) +.096*rxt(k,771)*y(k,320) + &
2.260*rxt(k,774)*y(k,129) +1.952*rxt(k,778)*y(k,211) +.219*rxt(k,782)*y(k,292) +.072*rxt(k,785)*y(k,98) + &
1.312*rxt(k,788)*y(k,89) +.246*rxt(k,795)*y(k,117) +.876*rxt(k,798)*y(k,147) +.273*rxt(k,801)*y(k,283) + &
.747*rxt(k,804)*y(k,218) +.441*rxt(k,807)*y(k,259) +.416*rxt(k,814)*y(k,13) +.084*rxt(k,817)*y(k,30) + &
.428*rxt(k,821)*y(k,6) +.426*rxt(k,825)*y(k,70) +.039*rxt(k,831)*y(k,134) +.102*rxt(k,834)*y(k,140) + &
.324*rxt(k,837)*y(k,154) +.711*rxt(k,843)*y(k,214) +.108*rxt(k,846)*y(k,284) +.057*rxt(k,849)*y(k,321) + &
.003*rxt(k,852)*y(k,322) +.087*rxt(k,855)*y(k,130) +1.796*rxt(k,858)*y(k,145) +1.476*rxt(k,862)*y(k,212) + &
.312*rxt(k,866)*y(k,293) +.008*rxt(k,872)*y(k,90) +.952*rxt(k,876)*y(k,92) +.165*rxt(k,880)*y(k,111) + &
.090*rxt(k,883)*y(k,118) +.003*rxt(k,886)*y(k,148) +.120*rxt(k,889)*y(k,185) +.171*rxt(k,892)*y(k,311) + &
.678*rxt(k,895)*y(k,36) +.678*rxt(k,898)*y(k,46) +.012*rxt(k,901)*y(k,77) +.075*rxt(k,905)*y(k,402) + &
.642*rxt(k,910)*y(k,103) +.003*rxt(k,913)*y(k,155) +1.464*rxt(k,916)*y(k,224) +.087*rxt(k,919)*y(k,274) + &
.003*rxt(k,922)*y(k,318) +.884*rxt(k,926)*y(k,41) +.042*rxt(k,929)*y(k,69) +.048*rxt(k,932)*y(k,405) + &
.129*rxt(k,937)*y(k,107) +.903*rxt(k,940)*y(k,229) +.261*rxt(k,943)*y(k,230) +.075*rxt(k,946)*y(k,275) + &
.027*rxt(k,949)*y(k,336) +1.164*rxt(k,952)*y(k,29) +.045*rxt(k,956)*y(k,78) +.498*rxt(k,963)*y(k,110) + &
.006*rxt(k,966)*y(k,133) +.210*rxt(k,969)*y(k,231) +.396*rxt(k,972)*y(k,234) +.069*rxt(k,975)*y(k,276) + &
.024*rxt(k,978)*y(k,337) +.704*rxt(k,981)*y(k,42) +.021*rxt(k,985)*y(k,74) +.201*rxt(k,992)*y(k,131) + &
.120*rxt(k,995)*y(k,184) +.165*rxt(k,998)*y(k,235) +.198*rxt(k,1001)*y(k,238) +.063*rxt(k,1004)*y(k,277) + &
.018*rxt(k,1007)*y(k,338) +.240*rxt(k,1010)*y(k,44) +.003*rxt(k,1020)*y(k,152) +.135*rxt(k,1023)*y(k,236) + &
.174*rxt(k,1026)*y(k,239) +.060*rxt(k,1029)*y(k,278) +.018*rxt(k,1032)*y(k,339) +.018*rxt(k,1038)*y(k,75) + &
.576*rxt(k,1043)*y(k,40) +.003*rxt(k,1049)*y(k,158) +.054*rxt(k,1052)*y(k,279) +.003*rxt(k,1055)*y(k,301) + &
.042*rxt(k,1060)*y(k,43) +1.968*rxt(k,1073)*y(k,739) +3.860*rxt(k,1103)*y(k,385) +3.860*rxt(k,1111)*y(k,784) + &
.188*rxt(k,1131)*y(k,84) +.648*rxt(k,1143)*y(k,263) +1.870*rxt(k,1166)*y(k,180) +2.793*rxt(k,1176)*y(k,389) + &
2.778*rxt(k,1187)*y(k,788) +1.560*rxt(k,1208)*y(k,780) +1.728*rxt(k,1219)*y(k,49) +.120*rxt(k,1230)*y(k,28) + &
.020*rxt(k,1235)*y(k,72) +2.108*rxt(k,1246)*y(k,213) +.980*rxt(k,1257)*y(k,264) +.004*rxt(k,1270)*y(k,312) + &
.072*rxt(k,1303)*y(k,177) +.012*rxt(k,1321)*y(k,181) +2.682*rxt(k,1332)*y(k,386) +.045*rxt(k,1344)*y(k,391) + &
2.697*rxt(k,1354)*y(k,395) +2.697*rxt(k,1366)*y(k,792) +2.511*rxt(k,1378)*y(k,798) +2.694*rxt(k,1390)*y(k,785) + &
.018*rxt(k,1402)*y(k,789) +.020*rxt(k,1413)*y(k,285) +3.084*rxt(k,1447)*y(k,781) +.036*rxt(k,1473)*y(k,317) + &
.016*rxt(k,1485)*y(k,71) +1.056*rxt(k,1496)*y(k,215) +.364*rxt(k,1509)*y(k,265) +.363*rxt(k,1534)*y(k,113) + &
4.676*rxt(k,1545)*y(k,208) +.042*rxt(k,1558)*y(k,178) +2.172*rxt(k,1579)*y(k,398) +.052*rxt(k,1604)*y(k,182) + &
2.556*rxt(k,1618)*y(k,257) +.039*rxt(k,1629)*y(k,290) +2.457*rxt(k,1642)*y(k,387) +.015*rxt(k,1654)*y(k,202) + &
.027*rxt(k,1665)*y(k,269) +2.562*rxt(k,1678)*y(k,216) +.075*rxt(k,1699)*y(k,392) +2.172*rxt(k,1710)*y(k,794) + &
2.457*rxt(k,1722)*y(k,786) +.081*rxt(k,1734)*y(k,790) +2.574*rxt(k,1745)*y(k,396) +2.598*rxt(k,1757)*y(k,799) + &
2.574*rxt(k,1770)*y(k,793) +.024*rxt(k,1813)*y(k,267) +2.307*rxt(k,1825)*y(k,787) +.008*rxt(k,1837)*y(k,183) + &
.081*rxt(k,1851)*y(k,400) +.033*rxt(k,1884)*y(k,791) +.111*rxt(k,1895)*y(k,796) +.040*rxt(k,1916)*y(k,124) + &
.032*rxt(k,1938)*y(k,291) +.060*rxt(k,1951)*y(k,797) +2.322*rxt(k,1972)*y(k,219) +.015*rxt(k,1984)*y(k,404) + &
.057*rxt(k,1994)*y(k,795) +.012*rxt(k,2094)*y(k,800) +.006*rxt(k,2114)*y(k,801) +.006*rxt(k,2134)*y(k,802) + &
.006*rxt(k,2154)*y(k,803) +.006*rxt(k,2164)*y(k,804) +1.640*rxt(k,2241)*y(k,360) +.501*rxt(k,2510)*y(k,344) + &
.100*rxt(k,2522)*y(k,741) +.070*rxt(k,2525)*y(k,737) +.621*rxt(k,2528)*y(k,677) +.045*rxt(k,2531)*y(k,346) + &
.249*rxt(k,2542)*y(k,508) +.036*rxt(k,2550)*y(k,729) +1.569*rxt(k,2565)*y(k,447) +2.216*rxt(k,2583)*y(k,18) + &
.033*rxt(k,2586)*y(k,192) +.045*rxt(k,2590)*y(k,59) +.144*rxt(k,2594)*y(k,575) +.240*rxt(k,2598)*y(k,679) + &
.693*rxt(k,2601)*y(k,773) +.040*rxt(k,2618)*y(k,606) +.128*rxt(k,2653)*y(k,641) +.008*rxt(k,2661)*y(k,583) + &
.081*rxt(k,2664)*y(k,570) +.372*rxt(k,2667)*y(k,368) +.772*rxt(k,2673)*y(k,35) +.420*rxt(k,2677)*y(k,20) + &
1.713*rxt(k,2686)*y(k,635) +.332*rxt(k,2689)*y(k,518) +.003*rxt(k,2693)*y(k,199) +.330*rxt(k,2696)*y(k,286) + &
.032*rxt(k,2722)*y(k,354) +1.872*rxt(k,2728)*y(k,595) +.084*rxt(k,2751)*y(k,60) +1.773*rxt(k,2755)*y(k,659) + &
1.677*rxt(k,2758)*y(k,464) +.804*rxt(k,2761)*y(k,649) +.584*rxt(k,2764)*y(k,164) +.522*rxt(k,2768)*y(k,247) + &
.081*rxt(k,2771)*y(k,407) +.392*rxt(k,2774)*y(k,563) +1.004*rxt(k,2778)*y(k,190) +.884*rxt(k,2782)*y(k,523) + &
.114*rxt(k,2786)*y(k,654) +.008*rxt(k,2819)*y(k,526) +.384*rxt(k,2827)*y(k,610) +.032*rxt(k,2831)*y(k,624) + &
.476*rxt(k,2835)*y(k,403) +.164*rxt(k,2839)*y(k,726) +.020*rxt(k,2843)*y(k,268) +1.660*rxt(k,2850)*y(k,516) + &
1.107*rxt(k,2854)*y(k,206) +.508*rxt(k,2857)*y(k,260) +.628*rxt(k,2861)*y(k,287) +.004*rxt(k,2878)*y(k,657) + &
.084*rxt(k,2896)*y(k,646) +.780*rxt(k,2900)*y(k,655) +.006*rxt(k,2907)*y(k,61) +.390*rxt(k,2910)*y(k,165) + &
.828*rxt(k,2913)*y(k,647) +2.619*rxt(k,2917)*y(k,740) +.258*rxt(k,2920)*y(k,498) +.004*rxt(k,2923)*y(k,584) + &
.212*rxt(k,2938)*y(k,505) +.012*rxt(k,2943)*y(k,566) +.464*rxt(k,2950)*y(k,361) +.148*rxt(k,2954)*y(k,743) + &
3.336*rxt(k,2958)*y(k,772) +1.180*rxt(k,2969)*y(k,615) +.207*rxt(k,2973)*y(k,410) +.060*rxt(k,2976)*y(k,179) + &
.276*rxt(k,2980)*y(k,493) +2.478*rxt(k,2984)*y(k,342) +1.905*rxt(k,2987)*y(k,742) +.052*rxt(k,2990)*y(k,366) + &
1.604*rxt(k,2994)*y(k,289) +1.660*rxt(k,3003)*y(k,730) +.032*rxt(k,3007)*y(k,554) +.232*rxt(k,3025)*y(k,449) + &
.044*rxt(k,3028)*y(k,204) +.044*rxt(k,3036)*y(k,470) +1.452*rxt(k,3040)*y(k,490) +.006*rxt(k,3053)*y(k,811) + &
.132*rxt(k,3068)*y(k,200) +.039*rxt(k,3072)*y(k,412) +.561*rxt(k,3075)*y(k,186) +.702*rxt(k,3078)*y(k,476) + &
1.824*rxt(k,3084)*y(k,266) +1.008*rxt(k,3091)*y(k,121) +1.164*rxt(k,3095)*y(k,294) +.064*rxt(k,3099)*y(k,668) + &
.408*rxt(k,3122)*y(k,367) +.057*rxt(k,3126)*y(k,352) +.248*rxt(k,3129)*y(k,727) +2.336*rxt(k,3137)*y(k,175) + &
.092*rxt(k,3141)*y(k,616) +1.808*rxt(k,3145)*y(k,722) +1.371*rxt(k,3152)*y(k,365) +.232*rxt(k,3155)*y(k,492) + &
.768*rxt(k,3163)*y(k,473) +.836*rxt(k,3167)*y(k,491) +.112*rxt(k,3171)*y(k,489) +.004*rxt(k,3182)*y(k,823) + &
.120*rxt(k,3200)*y(k,170) +.549*rxt(k,3203)*y(k,166) +.426*rxt(k,3206)*y(k,248) +.092*rxt(k,3212)*y(k,73) + &
.096*rxt(k,3219)*y(k,364) +.036*rxt(k,3227)*y(k,363) +.620*rxt(k,3253)*y(k,125) +.048*rxt(k,3257)*y(k,187) + &
.716*rxt(k,3261)*y(k,271) +.010*rxt(k,3270)*y(k,568) +.416*rxt(k,3274)*y(k,362) +.112*rxt(k,3281)*y(k,593) + &
.051*rxt(k,3285)*y(k,408) +.462*rxt(k,3288)*y(k,171) +.292*rxt(k,3294)*y(k,167) +.003*rxt(k,3298)*y(k,495) + &
.004*rxt(k,3301)*y(k,832) +.068*rxt(k,3309)*y(k,161) +.216*rxt(k,3313)*y(k,174) +.036*rxt(k,3317)*y(k,468) + &
.148*rxt(k,3321)*y(k,485) +.104*rxt(k,3330)*y(k,471) +.072*rxt(k,3333)*y(k,821) +.004*rxt(k,3337)*y(k,813) + &
.003*rxt(k,3341)*y(k,205) +.008*rxt(k,3347)*y(k,68) +.309*rxt(k,3351)*y(k,830) +.020*rxt(k,3357)*y(k,475) + &
.072*rxt(k,3367)*y(k,142) +.172*rxt(k,3371)*y(k,188) +.692*rxt(k,3375)*y(k,254) +.040*rxt(k,3379)*y(k,272) + &
.444*rxt(k,3383)*y(k,314) +.664*rxt(k,3401)*y(k,51) +1.176*rxt(k,3405)*y(k,488) +.088*rxt(k,3409)*y(k,486) + &
.132*rxt(k,3413)*y(k,487) +.496*rxt(k,3417)*y(k,824) +.792*rxt(k,3421)*y(k,834) +.004*rxt(k,3424)*y(k,815) + &
.020*rxt(k,3447)*y(k,436) +.020*rxt(k,3463)*y(k,557) +.084*rxt(k,3466)*y(k,632) +.393*rxt(k,3470)*y(k,587) + &
.036*rxt(k,3491)*y(k,237) +.021*rxt(k,3495)*y(k,406) +1.148*rxt(k,3502)*y(k,143) +.384*rxt(k,3506)*y(k,173) + &
.988*rxt(k,3510)*y(k,220) +.056*rxt(k,3514)*y(k,226) +.129*rxt(k,3518)*y(k,255) +.102*rxt(k,3521)*y(k,273) + &
.284*rxt(k,3524)*y(k,295) +.004*rxt(k,3528)*y(k,315) +.176*rxt(k,3532)*y(k,334) +1.890*rxt(k,3539)*y(k,529) + &
.138*rxt(k,3559)*y(k,494) +.006*rxt(k,3562)*y(k,472) +.644*rxt(k,3565)*y(k,462) +.036*rxt(k,3569)*y(k,469) + &
.024*rxt(k,3573)*y(k,820) +.572*rxt(k,3577)*y(k,835) +.104*rxt(k,3581)*y(k,172) +.140*rxt(k,3591)*y(k,108) + &
.144*rxt(k,3595)*y(k,126) +.012*rxt(k,3599)*y(k,144) +.648*rxt(k,3603)*y(k,149) +.008*rxt(k,3607)*y(k,189) + &
.916*rxt(k,3611)*y(k,227) +.024*rxt(k,3615)*y(k,232) +.147*rxt(k,3619)*y(k,256) +.584*rxt(k,3622)*y(k,296) + &
.036*rxt(k,3626)*y(k,297) +.027*rxt(k,3630)*y(k,316) +.004*rxt(k,3633)*y(k,335) +.044*rxt(k,3643)*y(k,564) + &
.428*rxt(k,3650)*y(k,233) +.033*rxt(k,3654)*y(k,261) +.540*rxt(k,3657)*y(k,298) +.005*rxt(k,3669)*y(k,817) + &
.120*rxt(k,3673)*y(k,467) +.020*rxt(k,3689)*y(k,814) +.020*rxt(k,3697)*y(k,222) +.548*rxt(k,3701)*y(k,253) + &
.220*rxt(k,3705)*y(k,300) +.124*rxt(k,3712)*y(k,822) +.084*rxt(k,3716)*y(k,106) +.364*rxt(k,3720)*y(k,223) + &
.016*rxt(k,3724)*y(k,228) +.020*rxt(k,3736)*y(k,132) +.471*rxt(k,3740)*y(k,252) +.004*rxt(k,3743)*y(k,299) + &
.006*rxt(k,3750)*y(k,588) +.084*rxt(k,3763)*y(k,105) +.020*rxt(k,3767)*y(k,221) +.176*rxt(k,3771)*y(k,331) + &
.055*rxt(k,3781)*y(k,836) +.279*rxt(k,3785)*y(k,652) +.006*rxt(k,3798)*y(k,651) +1.298*rxt(k,3855)*y(k,444) + &
rxt(k,3856)*y(k,540) +2.000*rxt(k,3865)*y(k,515) +1.754*rxt(k,3876)*y(k,384) +1.407*rxt(k,3904)*y(k,50) + &
.084*rxt(k,4013)*y(k,159) +.816*rxt(k,4029)*y(k,401) +.816*rxt(k,4054)*y(k,399) +.160*rxt(k,4059)*y(k,160) + &
.432*rxt(k,4134)*y(k,658) +1.908*rxt(k,4140)*y(k,499) +1.743*rxt(k,4152)*y(k,574) +.028*rxt(k,4182)*y(k,733) + &
1.180*rxt(k,4186)*y(k,734) +.258*rxt(k,4189)*y(k,735) +.087*rxt(k,4192)*y(k,736) +3.066*rxt(k,4195)*y(k,762) + &
.492*rxt(k,4199)*y(k,763) +4.320*rxt(k,4202)*y(k,764) +.024*rxt(k,4206)*y(k,765) +1.356*rxt(k,4210)*y(k,766) + &
.004*rxt(k,4214)*y(k,767))*y(k,705) + (4.230*rxt(k,296)*y(k,784) +2.000*rxt(k,432)*y(k,758) + &
.225*rxt(k,1090)*y(k,52) +4.230*rxt(k,1107)*y(k,385) +2.748*rxt(k,1162)*y(k,176) +2.805*rxt(k,1172)*y(k,180) + &
1.443*rxt(k,1183)*y(k,389) +1.443*rxt(k,1194)*y(k,788) +1.722*rxt(k,1215)*y(k,780) +1.362*rxt(k,1266)*y(k,264) + &
2.679*rxt(k,1299)*y(k,168) +.360*rxt(k,1340)*y(k,386) +.030*rxt(k,1350)*y(k,391) +2.706*rxt(k,1362)*y(k,395) + &
2.706*rxt(k,1374)*y(k,792) +.204*rxt(k,1386)*y(k,798) +.360*rxt(k,1398)*y(k,785) +.030*rxt(k,1408)*y(k,789) + &
.096*rxt(k,1420)*y(k,285) +1.280*rxt(k,1456)*y(k,781) +.012*rxt(k,1480)*y(k,317) +1.203*rxt(k,1505)*y(k,215) + &
.009*rxt(k,1518)*y(k,265) +.172*rxt(k,1529)*y(k,136) +1.824*rxt(k,1541)*y(k,113) +2.202*rxt(k,1554)*y(k,208) + &
.402*rxt(k,1565)*y(k,178) +.020*rxt(k,1586)*y(k,398) +.144*rxt(k,1599)*y(k,141) +1.384*rxt(k,1613)*y(k,182) + &
2.595*rxt(k,1625)*y(k,257) +.096*rxt(k,1637)*y(k,290) +.039*rxt(k,1650)*y(k,387) +.096*rxt(k,1673)*y(k,269) + &
.330*rxt(k,1686)*y(k,216) +.020*rxt(k,1717)*y(k,794) +.039*rxt(k,1730)*y(k,786) +1.992*rxt(k,1753)*y(k,396) + &
1.212*rxt(k,1765)*y(k,799) +1.992*rxt(k,1778)*y(k,793) +.030*rxt(k,1833)*y(k,787) +.176*rxt(k,1846)*y(k,183) + &
.044*rxt(k,1868)*y(k,782) +.004*rxt(k,1923)*y(k,124) +1.256*rxt(k,1979)*y(k,219) +.008*rxt(k,2011)*y(k,249) + &
.036*rxt(k,2055)*y(k,479) +.660*rxt(k,2245)*y(k,360) +.744*rxt(k,2572)*y(k,447) +.008*rxt(k,3015)*y(k,554) + &
.712*rxt(k,3105)*y(k,668) +.005*rxt(k,3117)*y(k,567) +.304*rxt(k,3235)*y(k,363) +.052*rxt(k,3457)*y(k,436) + &
1.850*rxt(k,3550)*y(k,529) +.348*rxt(k,3587)*y(k,172) +.408*rxt(k,3793)*y(k,652) +.008*rxt(k,3806)*y(k,651) + &
1.692*rxt(k,3910)*y(k,50) +.960*rxt(k,4137)*y(k,658) +2.400*rxt(k,4143)*y(k,499) +2.382*rxt(k,4158)*y(k,574)) &
*y(k,693) + (.188*rxt(k,567)*y(k,582) +.252*rxt(k,1086)*y(k,52) +2.232*rxt(k,1159)*y(k,176) + &
.252*rxt(k,1180)*y(k,389) +.252*rxt(k,1191)*y(k,788) +.225*rxt(k,1262)*y(k,264) +2.492*rxt(k,1296)*y(k,168) + &
.152*rxt(k,1325)*y(k,181) +.375*rxt(k,1347)*y(k,391) +2.930*rxt(k,1359)*y(k,395) +2.930*rxt(k,1371)*y(k,792) + &
.375*rxt(k,1405)*y(k,789) +.260*rxt(k,1501)*y(k,215) +.010*rxt(k,1514)*y(k,265) +.285*rxt(k,1550)*y(k,208) + &
2.620*rxt(k,1622)*y(k,257) +3.115*rxt(k,1633)*y(k,290) +.252*rxt(k,1658)*y(k,202) +3.115*rxt(k,1669)*y(k,269) + &
1.254*rxt(k,1692)*y(k,123) +.252*rxt(k,1703)*y(k,392) +.252*rxt(k,1738)*y(k,790) +.225*rxt(k,1761)*y(k,799) + &
.252*rxt(k,1865)*y(k,782) +.252*rxt(k,1888)*y(k,791) +.564*rxt(k,1976)*y(k,219) +.005*rxt(k,3453)*y(k,436) + &
.190*rxt(k,3789)*y(k,652) +2.000*rxt(k,3907)*y(k,50) +2.382*rxt(k,4155)*y(k,574))*y(k,696) &
 + (2.000*rxt(k,428)*y(k,690) +.880*rxt(k,429)*y(k,548) +2.000*rxt(k,434)*y(k,633) +2.000*rxt(k,435)*y(k,768) + &
2.000*rxt(k,437)*y(k,769) +2.000*rxt(k,440)*y(k,620) +4.000*rxt(k,441)*y(k,758) +2.000*rxt(k,456)*y(k,370) + &
2.000*rxt(k,472)*y(k,589) +2.000*rxt(k,3959)*y(k,427))*y(k,758) +.087*rxt(k,28)*y(k,266) +2.000*rxt(k,59) &
*y(k,464) +2.000*rxt(k,90)*y(k,625) +.800*rxt(k,122)*y(k,708) +.489*rxt(k,126)*y(k,731) +.004*rxt(k,130)*y(k,732) &
 +2.208*rxt(k,132)*y(k,733) +.231*rxt(k,137)*y(k,735) +2.000*rxt(k,142)*y(k,737) +.104*rxt(k,145)*y(k,755) &
 +2.000*rxt(k,150)*y(k,757) +1.150*rxt(k,154)*y(k,761) +.788*rxt(k,158)*y(k,762) +2.580*rxt(k,161)*y(k,764) &
 +1.364*rxt(k,168)*y(k,766)
         loss(k,3) = 0.
         prod(k,3) = (2.000*rxt(k,3948)*y(k,690) +2.000*rxt(k,3952)*y(k,693) +2.000*rxt(k,3956)*y(k,620) + &
2.000*rxt(k,3958)*y(k,758) +2.000*rxt(k,3960)*y(k,370) +2.000*rxt(k,3963)*y(k,589) +2.000*rxt(k,3964)*y(k,427)) &
*y(k,427) +rxt(k,51)*y(k,428) +.800*rxt(k,54)*y(k,431)
         loss(k,4) = (rxt(k,4292)* y(k,370) +rxt(k,4288)* y(k,548) +rxt(k,4293)* y(k,589) +rxt(k,4290)* y(k,620) +rxt(k,4294) &
* y(k,633) +rxt(k,4287)* y(k,690) +rxt(k,4289)* y(k,693) +rxt(k,4291)* y(k,758) +rxt(k,4295)* y(k,768) &
 +rxt(k,4296)* y(k,769))* y(k,850)
         prod(k,4) = (.162*rxt(k,262)*y(k,452) +.002*rxt(k,290)*y(k,455) +.070*rxt(k,485)*y(k,757) +1.336*rxt(k,519)*y(k,348) + &
1.336*rxt(k,526)*y(k,349) +.456*rxt(k,533)*y(k,350) +1.248*rxt(k,542)*y(k,590) +1.344*rxt(k,561)*y(k,582) + &
.063*rxt(k,620)*y(k,621) +1.124*rxt(k,646)*y(k,11) +.032*rxt(k,650)*y(k,24) +.144*rxt(k,654)*y(k,37) + &
.072*rxt(k,658)*y(k,506) +.084*rxt(k,708)*y(k,5) +.728*rxt(k,712)*y(k,7) +.140*rxt(k,716)*y(k,12) + &
.032*rxt(k,720)*y(k,15) +.152*rxt(k,724)*y(k,17) +.120*rxt(k,728)*y(k,76) +.003*rxt(k,732)*y(k,25) + &
.036*rxt(k,738)*y(k,38) +.052*rxt(k,745)*y(k,728) +.015*rxt(k,810)*y(k,558) +.004*rxt(k,813)*y(k,13) + &
.052*rxt(k,817)*y(k,30) +.284*rxt(k,821)*y(k,6) +.012*rxt(k,895)*y(k,36) +.020*rxt(k,901)*y(k,77) + &
.024*rxt(k,925)*y(k,41) +.020*rxt(k,952)*y(k,29) +.008*rxt(k,981)*y(k,42) +.003*rxt(k,1010)*y(k,44) + &
.003*rxt(k,1035)*y(k,31) +.020*rxt(k,1043)*y(k,40) +.003*rxt(k,1060)*y(k,43) +.054*rxt(k,1198)*y(k,457) + &
.056*rxt(k,1412)*y(k,285) +.021*rxt(k,1425)*y(k,81) +.028*rxt(k,1472)*y(k,317) +.008*rxt(k,2016)*y(k,355) + &
.008*rxt(k,2031)*y(k,879) +1.314*rxt(k,2500)*y(k,525) +1.314*rxt(k,2502)*y(k,623) +.501*rxt(k,2509)*y(k,344) + &
.006*rxt(k,2518)*y(k,831) +1.912*rxt(k,2521)*y(k,741) +.070*rxt(k,2524)*y(k,737) +.021*rxt(k,2538)*y(k,39) + &
1.280*rxt(k,2545)*y(k,605) +.800*rxt(k,2557)*y(k,482) +.099*rxt(k,2565)*y(k,447) +.147*rxt(k,2579)*y(k,826) + &
2.032*rxt(k,2582)*y(k,18) +.234*rxt(k,2586)*y(k,192) +.156*rxt(k,2590)*y(k,59) +.717*rxt(k,2604)*y(k,528) + &
.136*rxt(k,2618)*y(k,606) +1.332*rxt(k,2637)*y(k,552) +.956*rxt(k,2653)*y(k,641) +.900*rxt(k,2698)*y(k,710) + &
.188*rxt(k,2702)*y(k,626) +.012*rxt(k,2718)*y(k,420) +.040*rxt(k,2721)*y(k,354) +.075*rxt(k,2725)*y(k,828) + &
.069*rxt(k,2738)*y(k,94) +.500*rxt(k,2742)*y(k,270) +.172*rxt(k,2750)*y(k,60) +.099*rxt(k,2808)*y(k,531) + &
.028*rxt(k,2815)*y(k,825) +.380*rxt(k,2827)*y(k,610) +.564*rxt(k,2831)*y(k,624) +.048*rxt(k,2839)*y(k,726) + &
1.824*rxt(k,2865)*y(k,603) +.152*rxt(k,2874)*y(k,648) +.042*rxt(k,2892)*y(k,62) +1.192*rxt(k,2942)*y(k,566) + &
.884*rxt(k,2946)*y(k,642) +.040*rxt(k,2950)*y(k,361) +.040*rxt(k,2954)*y(k,743) +.176*rxt(k,2969)*y(k,615) + &
.084*rxt(k,2998)*y(k,653) +1.152*rxt(k,3024)*y(k,449) +2.004*rxt(k,3028)*y(k,204) +.004*rxt(k,3032)*y(k,640) + &
2.064*rxt(k,3043)*y(k,459) +.027*rxt(k,3047)*y(k,509) +.027*rxt(k,3064)*y(k,64) +.336*rxt(k,3068)*y(k,200) + &
.004*rxt(k,3099)*y(k,668) +.156*rxt(k,3110)*y(k,567) +.032*rxt(k,3122)*y(k,367) +.028*rxt(k,3129)*y(k,727) + &
.408*rxt(k,3137)*y(k,175) +.108*rxt(k,3141)*y(k,616) +1.448*rxt(k,3175)*y(k,27) +.759*rxt(k,3179)*y(k,460) + &
.004*rxt(k,3186)*y(k,162) +.024*rxt(k,3190)*y(k,66) +.004*rxt(k,3227)*y(k,363) +.084*rxt(k,3240)*y(k,569) + &
.036*rxt(k,3253)*y(k,125) +.064*rxt(k,3257)*y(k,187) +1.040*rxt(k,3269)*y(k,568) +.024*rxt(k,3274)*y(k,362) + &
.057*rxt(k,3291)*y(k,617) +.984*rxt(k,3309)*y(k,161) +.248*rxt(k,3325)*y(k,461) +.552*rxt(k,3329)*y(k,471) + &
.036*rxt(k,3367)*y(k,142) +.016*rxt(k,3371)*y(k,188) +.020*rxt(k,3387)*y(k,333) +.045*rxt(k,3398)*y(k,618) + &
.852*rxt(k,3428)*y(k,534) +.048*rxt(k,3462)*y(k,557) +.012*rxt(k,3502)*y(k,143) +.012*rxt(k,3506)*y(k,173) + &
.020*rxt(k,3532)*y(k,334) +.036*rxt(k,3556)*y(k,619) +.432*rxt(k,3569)*y(k,469) +.004*rxt(k,3581)*y(k,172) + &
.028*rxt(k,3591)*y(k,108) +.028*rxt(k,3595)*y(k,126) +.004*rxt(k,3599)*y(k,144) +.128*rxt(k,3603)*y(k,149) + &
.008*rxt(k,3607)*y(k,189) +.016*rxt(k,3633)*y(k,335) +.030*rxt(k,3640)*y(k,611) +.027*rxt(k,3661)*y(k,612) + &
.025*rxt(k,3668)*y(k,817) +.066*rxt(k,3682)*y(k,458) +.024*rxt(k,3709)*y(k,627) +.020*rxt(k,3716)*y(k,106) + &
.021*rxt(k,3728)*y(k,613) +.012*rxt(k,3736)*y(k,132) +.018*rxt(k,3747)*y(k,629) +.016*rxt(k,3763)*y(k,105) + &
.930*rxt(k,3780)*y(k,836))*y(k,705) + (rxt(k,549)*y(k,590) +2.288*rxt(k,569)*y(k,582) + &
.008*rxt(k,2011)*y(k,249) +.020*rxt(k,2026)*y(k,355) +.108*rxt(k,2041)*y(k,879) +.093*rxt(k,2515)*y(k,344) + &
.876*rxt(k,2571)*y(k,447) +.345*rxt(k,2624)*y(k,606) +1.808*rxt(k,2642)*y(k,552) +.100*rxt(k,2746)*y(k,270) + &
.501*rxt(k,2804)*y(k,628) +.064*rxt(k,3105)*y(k,668) +2.550*rxt(k,3116)*y(k,567) +.028*rxt(k,3235)*y(k,363) + &
1.216*rxt(k,3248)*y(k,569) +.006*rxt(k,3587)*y(k,172))*y(k,693) + (.032*rxt(k,1439)*y(k,456) + &
.024*rxt(k,1477)*y(k,317) +.306*rxt(k,2021)*y(k,355))*y(k,696) + (.834*rxt(k,1070)*y(k,512) + &
.460*rxt(k,1127)*y(k,33))*y(k,839)
         loss(k,5) = 0.
         prod(k,5) =1.900*rxt(k,3944)*y(k,748)*y(k,705)
         loss(k,730) = (rxt(k,4302)* y(k,370) +rxt(k,4298)* y(k,548) +rxt(k,4303)* y(k,589) +rxt(k,4300)* y(k,620) +rxt(k,4304) &
* y(k,633) +rxt(k,4297)* y(k,690) +rxt(k,4299)* y(k,693) +rxt(k,4301)* y(k,758) +rxt(k,4305)* y(k,768) &
 +rxt(k,4306)* y(k,769))* y(k,852)
         prod(k,730) = (.130*rxt(k,504)*y(k,756) +.428*rxt(k,520)*y(k,348) +.428*rxt(k,527)*y(k,349) +1.096*rxt(k,533)*y(k,350) + &
.600*rxt(k,562)*y(k,582) +1.160*rxt(k,601)*y(k,359) +.004*rxt(k,717)*y(k,12) +.952*rxt(k,2174)*y(k,833) + &
.736*rxt(k,2178)*y(k,383) +.400*rxt(k,2182)*y(k,665) +.420*rxt(k,2186)*y(k,706) +1.430*rxt(k,2191)*y(k,746) + &
.584*rxt(k,2204)*y(k,676) +.752*rxt(k,2208)*y(k,572) +.455*rxt(k,2212)*y(k,644) +.360*rxt(k,2217)*y(k,704) + &
1.290*rxt(k,2222)*y(k,721) +.360*rxt(k,2227)*y(k,14) +.370*rxt(k,2232)*y(k,16) +.528*rxt(k,2249)*y(k,373) + &
.884*rxt(k,2253)*y(k,807) +.430*rxt(k,2257)*y(k,596) +.330*rxt(k,2262)*y(k,697) +1.185*rxt(k,2267)*y(k,711) + &
1.295*rxt(k,2272)*y(k,719) +.335*rxt(k,2277)*y(k,240) +.340*rxt(k,2282)*y(k,302) +.600*rxt(k,2291)*y(k,816) + &
.600*rxt(k,2295)*y(k,667) +.452*rxt(k,2299)*y(k,376) +.395*rxt(k,2303)*y(k,597) +.290*rxt(k,2308)*y(k,698) + &
1.050*rxt(k,2313)*y(k,712) +.315*rxt(k,2318)*y(k,241) +.325*rxt(k,2323)*y(k,303) +.500*rxt(k,2332)*y(k,630) + &
.396*rxt(k,2336)*y(k,377) +.380*rxt(k,2340)*y(k,598) +.275*rxt(k,2345)*y(k,699) +rxt(k,2350)*y(k,713) + &
.305*rxt(k,2355)*y(k,242) +.315*rxt(k,2360)*y(k,304) +.400*rxt(k,2369)*y(k,119) +.352*rxt(k,2373)*y(k,378) + &
.288*rxt(k,2377)*y(k,599) +.255*rxt(k,2381)*y(k,700) +.920*rxt(k,2386)*y(k,714) +.295*rxt(k,2391)*y(k,243) + &
.300*rxt(k,2396)*y(k,305) +.320*rxt(k,2405)*y(k,380) +.272*rxt(k,2409)*y(k,600) +.240*rxt(k,2413)*y(k,701) + &
.855*rxt(k,2418)*y(k,715) +.285*rxt(k,2423)*y(k,244) +.290*rxt(k,2428)*y(k,306) +.292*rxt(k,2437)*y(k,381) + &
.260*rxt(k,2441)*y(k,601) +.225*rxt(k,2445)*y(k,702) +.805*rxt(k,2450)*y(k,716) +.275*rxt(k,2455)*y(k,245) + &
.280*rxt(k,2460)*y(k,307) +.268*rxt(k,2469)*y(k,382) +.248*rxt(k,2473)*y(k,602) +.210*rxt(k,2477)*y(k,703) + &
.755*rxt(k,2482)*y(k,717) +.265*rxt(k,2487)*y(k,246) +.275*rxt(k,2492)*y(k,308) +.249*rxt(k,2510)*y(k,344) + &
1.569*rxt(k,2566)*y(k,447) +.003*rxt(k,2591)*y(k,59) +.087*rxt(k,2715)*y(k,419) +.003*rxt(k,2726)*y(k,828) + &
.016*rxt(k,2743)*y(k,270) +.003*rxt(k,2809)*y(k,531) +.128*rxt(k,3000)*y(k,653) +.744*rxt(k,3056)*y(k,369) + &
.928*rxt(k,3060)*y(k,372) +.670*rxt(k,3222)*y(k,496) +.352*rxt(k,3305)*y(k,744) +.003*rxt(k,3342)*y(k,205) + &
.008*rxt(k,3435)*y(k,356) +.336*rxt(k,3498)*y(k,23) +.290*rxt(k,3677)*y(k,466) +.225*rxt(k,3732)*y(k,169) + &
.190*rxt(k,3775)*y(k,463) +1.160*rxt(k,4084)*y(k,443) +1.160*rxt(k,4088)*y(k,692) +1.160*rxt(k,4092)*y(k,438) + &
1.160*rxt(k,4108)*y(k,371) +.620*rxt(k,4118)*y(k,656) +1.028*rxt(k,4122)*y(k,426) +1.028*rxt(k,4126)*y(k,718)) &
*y(k,705) + (.004*rxt(k,2042)*y(k,879) +.744*rxt(k,2572)*y(k,447) +.004*rxt(k,2747)*y(k,270))*y(k,693) &
 +.012*rxt(k,2022)*y(k,696)*y(k,355) +.036*rxt(k,1071)*y(k,839)*y(k,512)
         loss(k,849) = (rxt(k,4312)* y(k,370) +rxt(k,4308)* y(k,548) +rxt(k,4313)* y(k,589) +rxt(k,4310)* y(k,620) +rxt(k,4314) &
* y(k,633) +rxt(k,4307)* y(k,690) +rxt(k,4309)* y(k,693) +rxt(k,4311)* y(k,758) +rxt(k,4315)* y(k,768) &
 +rxt(k,4316)* y(k,769))* y(k,853)
         prod(k,849) = (3.220*rxt(k,236)*y(k,512) +.219*rxt(k,251)*y(k,191) +2.784*rxt(k,259)*y(k,93) +.030*rxt(k,268)*y(k,114) + &
.348*rxt(k,275)*y(k,86) +1.365*rxt(k,283)*y(k,85) +3.156*rxt(k,286)*y(k,88) +.162*rxt(k,298)*y(k,519) + &
.158*rxt(k,300)*y(k,638) +rxt(k,302)*y(k,341) +.138*rxt(k,304)*y(k,573) +.264*rxt(k,307)*y(k,625) + &
2.034*rxt(k,310)*y(k,808) +.018*rxt(k,313)*y(k,517) +.702*rxt(k,316)*y(k,662) +.220*rxt(k,319)*y(k,504) + &
.872*rxt(k,323)*y(k,661) +rxt(k,326)*y(k,432) +rxt(k,328)*y(k,609) +.492*rxt(k,331)*y(k,388) + &
2.000*rxt(k,334)*y(k,3) +.048*rxt(k,496)*y(k,770) +.252*rxt(k,543)*y(k,590) +.900*rxt(k,552)*y(k,664) + &
.220*rxt(k,561)*y(k,582) +1.055*rxt(k,574)*y(k,731) +.066*rxt(k,578)*y(k,761) +1.872*rxt(k,585)*y(k,586) + &
.036*rxt(k,604)*y(k,193) +.908*rxt(k,607)*y(k,95) +.003*rxt(k,611)*y(k,194) +.015*rxt(k,614)*y(k,280) + &
.261*rxt(k,617)*y(k,581) +.048*rxt(k,620)*y(k,621) +.180*rxt(k,623)*y(k,96) +.024*rxt(k,627)*y(k,115) + &
1.449*rxt(k,630)*y(k,137) +.057*rxt(k,633)*y(k,195) +.652*rxt(k,636)*y(k,209) +.006*rxt(k,640)*y(k,281) + &
.006*rxt(k,643)*y(k,258) +.448*rxt(k,646)*y(k,11) +.032*rxt(k,650)*y(k,24) +.104*rxt(k,654)*y(k,37) + &
.040*rxt(k,658)*y(k,506) +.033*rxt(k,662)*y(k,622) +.096*rxt(k,665)*y(k,97) +.060*rxt(k,669)*y(k,104) + &
.024*rxt(k,672)*y(k,116) +.711*rxt(k,675)*y(k,138) +.465*rxt(k,678)*y(k,146) +.042*rxt(k,681)*y(k,196) + &
.003*rxt(k,684)*y(k,282) +.006*rxt(k,687)*y(k,319) +.460*rxt(k,690)*y(k,102) +.472*rxt(k,694)*y(k,210) + &
.096*rxt(k,698)*y(k,87) +.021*rxt(k,702)*y(k,217) +.015*rxt(k,705)*y(k,251) +.264*rxt(k,708)*y(k,5) + &
.416*rxt(k,712)*y(k,7) +.584*rxt(k,716)*y(k,12) +.056*rxt(k,720)*y(k,15) +.076*rxt(k,724)*y(k,17) + &
.088*rxt(k,728)*y(k,76) +.048*rxt(k,732)*y(k,25) +.153*rxt(k,735)*y(k,47) +.076*rxt(k,738)*y(k,38) + &
.006*rxt(k,742)*y(k,507) +.040*rxt(k,745)*y(k,728) +.176*rxt(k,749)*y(k,91) +.462*rxt(k,753)*y(k,109) + &
.486*rxt(k,756)*y(k,139) +.006*rxt(k,759)*y(k,197) +.159*rxt(k,762)*y(k,207) +.024*rxt(k,765)*y(k,225) + &
.003*rxt(k,768)*y(k,310) +.003*rxt(k,771)*y(k,320) +1.520*rxt(k,774)*y(k,129) +.236*rxt(k,778)*y(k,211) + &
.363*rxt(k,782)*y(k,292) +.054*rxt(k,785)*y(k,98) +1.188*rxt(k,788)*y(k,89) +.003*rxt(k,792)*y(k,153) + &
.009*rxt(k,795)*y(k,117) +.105*rxt(k,798)*y(k,147) +.003*rxt(k,801)*y(k,283) +.021*rxt(k,804)*y(k,218) + &
.009*rxt(k,810)*y(k,558) +.056*rxt(k,813)*y(k,13) +.132*rxt(k,817)*y(k,30) +.596*rxt(k,821)*y(k,6) + &
.099*rxt(k,825)*y(k,70) +.576*rxt(k,831)*y(k,134) +.255*rxt(k,834)*y(k,140) +.021*rxt(k,843)*y(k,214) + &
.003*rxt(k,849)*y(k,321) +.003*rxt(k,852)*y(k,322) +.945*rxt(k,855)*y(k,130) +.944*rxt(k,858)*y(k,145) + &
.188*rxt(k,862)*y(k,212) +.210*rxt(k,866)*y(k,293) +.030*rxt(k,869)*y(k,99) +.668*rxt(k,872)*y(k,90) + &
.100*rxt(k,876)*y(k,92) +.006*rxt(k,880)*y(k,111) +.006*rxt(k,883)*y(k,118) +.033*rxt(k,886)*y(k,148) + &
.003*rxt(k,889)*y(k,185) +.003*rxt(k,895)*y(k,36) +.063*rxt(k,898)*y(k,46) +.024*rxt(k,901)*y(k,77) + &
.177*rxt(k,910)*y(k,103) +.003*rxt(k,916)*y(k,224) +.040*rxt(k,925)*y(k,41) +.009*rxt(k,929)*y(k,69) + &
.156*rxt(k,937)*y(k,107) +.003*rxt(k,943)*y(k,230) +.028*rxt(k,952)*y(k,29) +.003*rxt(k,956)*y(k,78) + &
.048*rxt(k,963)*y(k,110) +.057*rxt(k,966)*y(k,133) +.003*rxt(k,969)*y(k,231) +.020*rxt(k,981)*y(k,42) + &
.006*rxt(k,985)*y(k,74) +.129*rxt(k,992)*y(k,131) +.006*rxt(k,995)*y(k,184) +.012*rxt(k,1010)*y(k,44) + &
.003*rxt(k,1013)*y(k,79) +.030*rxt(k,1020)*y(k,152) +.006*rxt(k,1035)*y(k,31) +.006*rxt(k,1038)*y(k,75) + &
.012*rxt(k,1043)*y(k,40) +.015*rxt(k,1049)*y(k,158) +.006*rxt(k,1060)*y(k,43) +.003*rxt(k,1063)*y(k,80) + &
1.968*rxt(k,1073)*y(k,739) +2.844*rxt(k,1083)*y(k,52) +1.800*rxt(k,1094)*y(k,585) +2.124*rxt(k,1118)*y(k,33) + &
3.068*rxt(k,1131)*y(k,84) +2.876*rxt(k,1143)*y(k,263) +2.772*rxt(k,1155)*y(k,176) +.084*rxt(k,1198)*y(k,457) + &
1.245*rxt(k,1208)*y(k,780) +1.035*rxt(k,1219)*y(k,49) +1.932*rxt(k,1235)*y(k,72) +1.444*rxt(k,1246)*y(k,213) + &
2.616*rxt(k,1257)*y(k,264) +2.600*rxt(k,1270)*y(k,312) +2.439*rxt(k,1282)*y(k,120) +2.640*rxt(k,1292)*y(k,168) + &
2.616*rxt(k,1303)*y(k,177) +.012*rxt(k,1332)*y(k,386) +.108*rxt(k,1412)*y(k,285) +.021*rxt(k,1425)*y(k,81) + &
.003*rxt(k,1436)*y(k,456) +.448*rxt(k,1447)*y(k,781) +.003*rxt(k,1461)*y(k,82) +.044*rxt(k,1473)*y(k,317) + &
1.576*rxt(k,1485)*y(k,71) +2.212*rxt(k,1496)*y(k,215) +1.636*rxt(k,1509)*y(k,265) +2.454*rxt(k,1522)*y(k,136) + &
2.136*rxt(k,1534)*y(k,113) +.816*rxt(k,1545)*y(k,208) +2.244*rxt(k,1558)*y(k,178) +.246*rxt(k,1569)*y(k,101) + &
.036*rxt(k,1579)*y(k,398) +.016*rxt(k,1604)*y(k,182) +.003*rxt(k,1618)*y(k,257) +.021*rxt(k,1642)*y(k,387) + &
.009*rxt(k,1699)*y(k,392) +.036*rxt(k,1710)*y(k,794) +.006*rxt(k,1722)*y(k,786) +.009*rxt(k,1745)*y(k,396) + &
.009*rxt(k,1770)*y(k,793) +1.077*rxt(k,1791)*y(k,83) +2.427*rxt(k,1801)*y(k,127) +2.860*rxt(k,1813)*y(k,267) + &
.009*rxt(k,1825)*y(k,787) +.024*rxt(k,1837)*y(k,183) +.018*rxt(k,1851)*y(k,400) +.024*rxt(k,1861)*y(k,782) + &
.024*rxt(k,1873)*y(k,783) +.006*rxt(k,1884)*y(k,791) +.072*rxt(k,1905)*y(k,128) +.012*rxt(k,1916)*y(k,124) + &
.993*rxt(k,1928)*y(k,67) +1.828*rxt(k,1938)*y(k,291) +.006*rxt(k,1951)*y(k,797) +.924*rxt(k,1962)*y(k,53) + &
.006*rxt(k,1972)*y(k,219) +.030*rxt(k,1984)*y(k,404) +.006*rxt(k,1994)*y(k,795) +.088*rxt(k,2016)*y(k,355) + &
3.136*rxt(k,2031)*y(k,879) +1.152*rxt(k,2046)*y(k,479) +.540*rxt(k,2060)*y(k,771) +.873*rxt(k,2084)*y(k,55) + &
.012*rxt(k,2094)*y(k,800) +.834*rxt(k,2104)*y(k,56) +.006*rxt(k,2114)*y(k,801) +.801*rxt(k,2124)*y(k,57) + &
.006*rxt(k,2134)*y(k,802) +.774*rxt(k,2144)*y(k,58) +.003*rxt(k,2154)*y(k,803) +.003*rxt(k,2164)*y(k,804) + &
1.640*rxt(k,2196)*y(k,776) +.082*rxt(k,2501)*y(k,525) +.082*rxt(k,2503)*y(k,623) +.249*rxt(k,2510)*y(k,344) + &
.012*rxt(k,2519)*y(k,831) +.916*rxt(k,2521)*y(k,741) +.624*rxt(k,2528)*y(k,677) +1.644*rxt(k,2530)*y(k,346) + &
.033*rxt(k,2539)*y(k,39) +.102*rxt(k,2547)*y(k,550) +.036*rxt(k,2550)*y(k,729) +.172*rxt(k,2552)*y(k,643) + &
1.944*rxt(k,2554)*y(k,636) +1.642*rxt(k,2577)*y(k,418) +.039*rxt(k,2579)*y(k,826) +.092*rxt(k,2582)*y(k,18) + &
.033*rxt(k,2586)*y(k,192) +.042*rxt(k,2590)*y(k,59) +1.536*rxt(k,2594)*y(k,575) +.924*rxt(k,2598)*y(k,679) + &
.048*rxt(k,2601)*y(k,773) +2.850*rxt(k,2607)*y(k,591) +2.724*rxt(k,2618)*y(k,606) +2.616*rxt(k,2628)*y(k,837) + &
.453*rxt(k,2637)*y(k,552) +.117*rxt(k,2647)*y(k,48) +.003*rxt(k,2657)*y(k,394) +.084*rxt(k,2660)*y(k,583) + &
.669*rxt(k,2664)*y(k,570) +.084*rxt(k,2670)*y(k,634) +.032*rxt(k,2673)*y(k,35) +.729*rxt(k,2680)*y(k,45) + &
.057*rxt(k,2683)*y(k,112) +.003*rxt(k,2686)*y(k,635) +2.196*rxt(k,2689)*y(k,518) +2.289*rxt(k,2693)*y(k,199) + &
2.991*rxt(k,2696)*y(k,286) +.645*rxt(k,2699)*y(k,710) +.244*rxt(k,2702)*y(k,626) +2.037*rxt(k,2706)*y(k,465) + &
.231*rxt(k,2715)*y(k,419) +.303*rxt(k,2718)*y(k,420) +.100*rxt(k,2721)*y(k,354) +.078*rxt(k,2725)*y(k,828) + &
.933*rxt(k,2728)*y(k,595) +.072*rxt(k,2738)*y(k,94) +.368*rxt(k,2743)*y(k,270) +.044*rxt(k,2750)*y(k,60) + &
1.836*rxt(k,2755)*y(k,659) +.948*rxt(k,2761)*y(k,649) +.136*rxt(k,2764)*y(k,164) +.786*rxt(k,2771)*y(k,407) + &
2.164*rxt(k,2774)*y(k,563) +1.824*rxt(k,2778)*y(k,190) +.012*rxt(k,2782)*y(k,523) +.018*rxt(k,2786)*y(k,654) + &
1.185*rxt(k,2789)*y(k,501) +1.870*rxt(k,2798)*y(k,628) +.069*rxt(k,2808)*y(k,531) +.792*rxt(k,2812)*y(k,135) + &
2.200*rxt(k,2815)*y(k,825) +.372*rxt(k,2823)*y(k,579) +.004*rxt(k,2827)*y(k,610) +.424*rxt(k,2831)*y(k,624) + &
.004*rxt(k,2839)*y(k,726) +.132*rxt(k,2843)*y(k,268) +.771*rxt(k,2847)*y(k,100) +.132*rxt(k,2850)*y(k,516) + &
1.218*rxt(k,2854)*y(k,206) +2.760*rxt(k,2857)*y(k,260) +4.680*rxt(k,2861)*y(k,287) +.016*rxt(k,2865)*y(k,603) + &
.195*rxt(k,2869)*y(k,513) +.900*rxt(k,2875)*y(k,648) +1.228*rxt(k,2878)*y(k,657) +.120*rxt(k,2882)*y(k,725) + &
.177*rxt(k,2886)*y(k,421) +.120*rxt(k,2889)*y(k,422) +.006*rxt(k,2892)*y(k,62) +3.308*rxt(k,2896)*y(k,646) + &
1.352*rxt(k,2900)*y(k,655) +2.643*rxt(k,2904)*y(k,663) +.294*rxt(k,2907)*y(k,61) +.096*rxt(k,2910)*y(k,165) + &
.668*rxt(k,2913)*y(k,647) +.033*rxt(k,2920)*y(k,498) +.008*rxt(k,2923)*y(k,584) +2.412*rxt(k,2927)*y(k,514) + &
.004*rxt(k,2938)*y(k,505) +.208*rxt(k,2942)*y(k,566) +2.304*rxt(k,2946)*y(k,642) +.004*rxt(k,2954)*y(k,743) + &
.024*rxt(k,2958)*y(k,772) +2.295*rxt(k,2962)*y(k,805) +1.552*rxt(k,2965)*y(k,474) +.288*rxt(k,2976)*y(k,179) + &
.160*rxt(k,2980)*y(k,493) +2.200*rxt(k,2990)*y(k,366) +.352*rxt(k,2994)*y(k,289) +.124*rxt(k,2999)*y(k,653) + &
2.548*rxt(k,3003)*y(k,730) +1.036*rxt(k,3007)*y(k,554) +.116*rxt(k,3020)*y(k,724) +.012*rxt(k,3024)*y(k,449) + &
.044*rxt(k,3028)*y(k,204) +.020*rxt(k,3032)*y(k,640) +.932*rxt(k,3036)*y(k,470) +1.203*rxt(k,3053)*y(k,811) + &
.297*rxt(k,3072)*y(k,412) +.570*rxt(k,3075)*y(k,186) +.777*rxt(k,3078)*y(k,476) +.060*rxt(k,3081)*y(k,332) + &
.064*rxt(k,3084)*y(k,266) +.162*rxt(k,3088)*y(k,63) +1.640*rxt(k,3091)*y(k,121) +.180*rxt(k,3095)*y(k,294) + &
1.272*rxt(k,3099)*y(k,668) +1.292*rxt(k,3110)*y(k,567) +.004*rxt(k,3129)*y(k,727) +.008*rxt(k,3133)*y(k,562) + &
.044*rxt(k,3137)*y(k,175) +.100*rxt(k,3145)*y(k,722) +2.166*rxt(k,3149)*y(k,723) +.008*rxt(k,3155)*y(k,492) + &
.084*rxt(k,3159)*y(k,478) +.856*rxt(k,3163)*y(k,473) +.188*rxt(k,3167)*y(k,491) +1.384*rxt(k,3171)*y(k,489) + &
.088*rxt(k,3175)*y(k,27) +.784*rxt(k,3182)*y(k,823) +.136*rxt(k,3186)*y(k,162) +.042*rxt(k,3194)*y(k,414) + &
.162*rxt(k,3197)*y(k,65) +1.017*rxt(k,3200)*y(k,170) +.024*rxt(k,3203)*y(k,166) +.724*rxt(k,3212)*y(k,73) + &
.057*rxt(k,3216)*y(k,565) +2.712*rxt(k,3227)*y(k,363) +2.720*rxt(k,3240)*y(k,569) +.032*rxt(k,3253)*y(k,125) + &
.008*rxt(k,3257)*y(k,187) +.004*rxt(k,3261)*y(k,271) +.016*rxt(k,3265)*y(k,313) +.345*rxt(k,3269)*y(k,568) + &
1.456*rxt(k,3281)*y(k,593) +.048*rxt(k,3294)*y(k,167) +.846*rxt(k,3298)*y(k,495) +.104*rxt(k,3301)*y(k,832) + &
.204*rxt(k,3313)*y(k,174) +.720*rxt(k,3317)*y(k,468) +1.124*rxt(k,3321)*y(k,485) +.084*rxt(k,3329)*y(k,471) + &
.656*rxt(k,3333)*y(k,821) +1.092*rxt(k,3337)*y(k,813) +.003*rxt(k,3341)*y(k,205) +.078*rxt(k,3344)*y(k,323) + &
2.508*rxt(k,3347)*y(k,68) +2.376*rxt(k,3357)*y(k,475) +.285*rxt(k,3361)*y(k,156) +.015*rxt(k,3364)*y(k,157) + &
.868*rxt(k,3367)*y(k,142) +.004*rxt(k,3371)*y(k,188) +.004*rxt(k,3375)*y(k,254) +.004*rxt(k,3379)*y(k,272) + &
.004*rxt(k,3383)*y(k,314) +.028*rxt(k,3387)*y(k,333) +.096*rxt(k,3391)*y(k,576) +.052*rxt(k,3401)*y(k,51) + &
.104*rxt(k,3405)*y(k,488) +.216*rxt(k,3409)*y(k,486) +.212*rxt(k,3413)*y(k,487) +.588*rxt(k,3417)*y(k,824) + &
.069*rxt(k,3421)*y(k,834) +.560*rxt(k,3424)*y(k,815) +.090*rxt(k,3431)*y(k,417) +.016*rxt(k,3434)*y(k,356) + &
.215*rxt(k,3447)*y(k,436) +.080*rxt(k,3462)*y(k,557) +.048*rxt(k,3466)*y(k,632) +.192*rxt(k,3470)*y(k,587) + &
.111*rxt(k,3485)*y(k,571) +.099*rxt(k,3488)*y(k,54) +.156*rxt(k,3491)*y(k,237) +.032*rxt(k,3502)*y(k,143) + &
.028*rxt(k,3510)*y(k,220) +.592*rxt(k,3514)*y(k,226) +.003*rxt(k,3518)*y(k,255) +.003*rxt(k,3521)*y(k,273) + &
.008*rxt(k,3524)*y(k,295) +.004*rxt(k,3528)*y(k,315) +.004*rxt(k,3532)*y(k,334) +.070*rxt(k,3539)*y(k,529) + &
.777*rxt(k,3559)*y(k,494) +.516*rxt(k,3562)*y(k,472) +.100*rxt(k,3565)*y(k,462) +.056*rxt(k,3569)*y(k,469) + &
.556*rxt(k,3573)*y(k,820) +.124*rxt(k,3577)*y(k,835) +.932*rxt(k,3581)*y(k,172) +.336*rxt(k,3591)*y(k,108) + &
.004*rxt(k,3595)*y(k,126) +.020*rxt(k,3599)*y(k,144) +.012*rxt(k,3603)*y(k,149) +.016*rxt(k,3611)*y(k,227) + &
.048*rxt(k,3615)*y(k,232) +.008*rxt(k,3622)*y(k,296) +.328*rxt(k,3626)*y(k,297) +1.348*rxt(k,3643)*y(k,564) + &
.018*rxt(k,3647)*y(k,151) +.008*rxt(k,3650)*y(k,233) +.008*rxt(k,3657)*y(k,298) +1.332*rxt(k,3664)*y(k,818) + &
.170*rxt(k,3668)*y(k,817) +.620*rxt(k,3673)*y(k,467) +.048*rxt(k,3685)*y(k,477) +.436*rxt(k,3689)*y(k,814) + &
.044*rxt(k,3697)*y(k,222) +.004*rxt(k,3701)*y(k,253) +.004*rxt(k,3705)*y(k,300) +.688*rxt(k,3712)*y(k,822) + &
.096*rxt(k,3716)*y(k,106) +.008*rxt(k,3720)*y(k,223) +.176*rxt(k,3724)*y(k,228) +.032*rxt(k,3736)*y(k,132) + &
.120*rxt(k,3743)*y(k,299) +.021*rxt(k,3750)*y(k,588) +.128*rxt(k,3763)*y(k,105) +.028*rxt(k,3767)*y(k,221) + &
.152*rxt(k,3771)*y(k,331) +.355*rxt(k,3780)*y(k,836) +.210*rxt(k,3785)*y(k,652) +.117*rxt(k,3798)*y(k,651) + &
rxt(k,3821)*y(k,433) +1.542*rxt(k,3845)*y(k,522) +.702*rxt(k,3854)*y(k,444) +1.500*rxt(k,3868)*y(k,483) + &
.302*rxt(k,3873)*y(k,9) +.492*rxt(k,3876)*y(k,384) +2.000*rxt(k,3887)*y(k,345) +2.889*rxt(k,3912)*y(k,250) + &
.336*rxt(k,3942)*y(k,22) +2.000*rxt(k,3968)*y(k,747) +.036*rxt(k,3971)*y(k,390) +.084*rxt(k,3985)*y(k,806) + &
1.086*rxt(k,4003)*y(k,480) +2.292*rxt(k,4013)*y(k,159) +2.160*rxt(k,4029)*y(k,401) +1.290*rxt(k,4032)*y(k,520) + &
2.847*rxt(k,4042)*y(k,439) +2.160*rxt(k,4054)*y(k,399) +.160*rxt(k,4059)*y(k,160) +2.298*rxt(k,4096)*y(k,521) + &
.648*rxt(k,4162)*y(k,720) +1.224*rxt(k,4170)*y(k,827) +1.332*rxt(k,4178)*y(k,732) +2.408*rxt(k,4181)*y(k,733) + &
.032*rxt(k,4185)*y(k,734) +.028*rxt(k,4198)*y(k,763) +.048*rxt(k,4202)*y(k,764) +.196*rxt(k,4210)*y(k,766)) &
*y(k,705) + (1.956*rxt(k,524)*y(k,348) +1.956*rxt(k,531)*y(k,349) +.978*rxt(k,540)*y(k,350) + &
.300*rxt(k,547)*y(k,590) +.200*rxt(k,556)*y(k,664) +.004*rxt(k,567)*y(k,582) +.960*rxt(k,590)*y(k,586) + &
2.001*rxt(k,1096)*y(k,585) +.328*rxt(k,1159)*y(k,176) +2.800*rxt(k,1168)*y(k,180) +.110*rxt(k,1275)*y(k,312) + &
.168*rxt(k,1286)*y(k,120) +.268*rxt(k,1307)*y(k,177) +2.000*rxt(k,1315)*y(k,122) +2.800*rxt(k,1324)*y(k,181) + &
.435*rxt(k,1358)*y(k,395) +.435*rxt(k,1370)*y(k,792) +.272*rxt(k,1429)*y(k,81) +.260*rxt(k,1465)*y(k,82) + &
.004*rxt(k,1477)*y(k,317) +.260*rxt(k,1526)*y(k,136) +.132*rxt(k,1538)*y(k,113) +.260*rxt(k,1562)*y(k,178) + &
2.408*rxt(k,1573)*y(k,101) +3.500*rxt(k,1595)*y(k,141) +3.500*rxt(k,1609)*y(k,182) +.435*rxt(k,1633)*y(k,290) + &
.435*rxt(k,1669)*y(k,269) +.220*rxt(k,1682)*y(k,216) +1.686*rxt(k,1692)*y(k,123) +.350*rxt(k,1749)*y(k,396) + &
.350*rxt(k,1774)*y(k,793) +.267*rxt(k,1785)*y(k,26) +.248*rxt(k,1805)*y(k,127) +3.500*rxt(k,1842)*y(k,183) + &
2.800*rxt(k,1909)*y(k,128) +1.650*rxt(k,1920)*y(k,124) +.065*rxt(k,1943)*y(k,291) +.232*rxt(k,2008)*y(k,249) + &
2.060*rxt(k,2021)*y(k,355) +.066*rxt(k,2036)*y(k,879) +.445*rxt(k,2051)*y(k,479) +1.750*rxt(k,2075)*y(k,881) + &
1.528*rxt(k,2611)*y(k,591) +1.468*rxt(k,2801)*y(k,628) +1.428*rxt(k,2931)*y(k,514) +.004*rxt(k,3011)*y(k,554) + &
1.324*rxt(k,3232)*y(k,363) +1.324*rxt(k,3245)*y(k,569) +.310*rxt(k,3439)*y(k,356) +3.515*rxt(k,3453)*y(k,436) + &
3.520*rxt(k,3474)*y(k,587) +2.478*rxt(k,3545)*y(k,529) +.090*rxt(k,3789)*y(k,652) +.050*rxt(k,3802)*y(k,651) + &
2.000*rxt(k,3848)*y(k,522) +.189*rxt(k,3922)*y(k,779) +.189*rxt(k,3933)*y(k,379) +1.152*rxt(k,4006)*y(k,480) + &
2.883*rxt(k,4017)*y(k,159) +2.883*rxt(k,4035)*y(k,520) +2.706*rxt(k,4099)*y(k,521))*y(k,696) &
 + (.908*rxt(k,569)*y(k,582) +1.932*rxt(k,1099)*y(k,585) +.345*rxt(k,1124)*y(k,33) +.057*rxt(k,1162)*y(k,176) + &
.027*rxt(k,1299)*y(k,168) +.018*rxt(k,1328)*y(k,181) +.012*rxt(k,1480)*y(k,317) +2.152*rxt(k,1529)*y(k,136) + &
.186*rxt(k,1565)*y(k,178) +.160*rxt(k,1586)*y(k,398) +1.368*rxt(k,1599)*y(k,141) +.956*rxt(k,1613)*y(k,182) + &
.012*rxt(k,1637)*y(k,290) +.012*rxt(k,1673)*y(k,269) +.160*rxt(k,1717)*y(k,794) +.048*rxt(k,1765)*y(k,799) + &
4.612*rxt(k,1808)*y(k,127) +.908*rxt(k,1846)*y(k,183) +.108*rxt(k,1868)*y(k,782) +2.277*rxt(k,1912)*y(k,128) + &
.004*rxt(k,1923)*y(k,124) +.585*rxt(k,1947)*y(k,291) +.028*rxt(k,2026)*y(k,355) +.108*rxt(k,2041)*y(k,879) + &
.312*rxt(k,2055)*y(k,479) +.028*rxt(k,2079)*y(k,881) +.660*rxt(k,2200)*y(k,776) +.112*rxt(k,2642)*y(k,552) + &
.100*rxt(k,2747)*y(k,270) +3.328*rxt(k,3015)*y(k,554) +.055*rxt(k,3117)*y(k,567) +.028*rxt(k,3249)*y(k,569) + &
.072*rxt(k,3457)*y(k,436) +1.210*rxt(k,3550)*y(k,529) +.404*rxt(k,3793)*y(k,652) +.216*rxt(k,3806)*y(k,651) + &
1.692*rxt(k,3851)*y(k,522) +1.218*rxt(k,4009)*y(k,480) +1.884*rxt(k,4020)*y(k,159) +1.332*rxt(k,4038)*y(k,520) + &
1.922*rxt(k,4048)*y(k,439) +2.706*rxt(k,4102)*y(k,521) +.693*rxt(k,4166)*y(k,720) +1.386*rxt(k,4173)*y(k,827)) &
*y(k,693) + (.720*rxt(k,596)*y(k,586) +.834*rxt(k,1070)*y(k,512))*y(k,839) +2.000*rxt(k,18)*y(k,135) &
 +1.044*rxt(k,31)*y(k,270) +2.160*rxt(k,4051)*y(k,439)*y(k,437) +.012*rxt(k,80)*y(k,557) +rxt(k,85)*y(k,590) &
 +.158*rxt(k,93)*y(k,634) +1.044*rxt(k,97)*y(k,646) +3.141*rxt(k,103)*y(k,653) +.909*rxt(k,126)*y(k,731) &
 +.066*rxt(k,129)*y(k,732) +4.752*rxt(k,132)*y(k,733) +.305*rxt(k,153)*y(k,761) +1.468*rxt(k,168)*y(k,766)
         loss(k,864) = (rxt(k,4322)* y(k,370) +rxt(k,4318)* y(k,548) +rxt(k,4323)* y(k,589) +rxt(k,4320)* y(k,620) +rxt(k,4324) &
* y(k,633) +rxt(k,4317)* y(k,690) +rxt(k,4319)* y(k,693) +rxt(k,4321)* y(k,758) +rxt(k,4325)* y(k,768) &
 +rxt(k,4326)* y(k,769))* y(k,854)
         prod(k,864) = (2.000*rxt(k,236)*y(k,512) +rxt(k,242)*y(k,511) +1.930*rxt(k,243)*y(k,738) +1.898*rxt(k,245)*y(k,450) + &
2.763*rxt(k,247)*y(k,678) +.594*rxt(k,250)*y(k,191) +2.565*rxt(k,255)*y(k,680) +.063*rxt(k,258)*y(k,93) + &
2.328*rxt(k,261)*y(k,452) +2.325*rxt(k,264)*y(k,681) +2.580*rxt(k,267)*y(k,114) +1.598*rxt(k,270)*y(k,453) + &
1.410*rxt(k,272)*y(k,683) +.588*rxt(k,274)*y(k,86) +1.527*rxt(k,277)*y(k,454) +1.292*rxt(k,280)*y(k,685) + &
.957*rxt(k,282)*y(k,85) +1.050*rxt(k,289)*y(k,455) +1.204*rxt(k,291)*y(k,687) +1.144*rxt(k,293)*y(k,669) + &
.100*rxt(k,298)*y(k,519) +2.000*rxt(k,300)*y(k,638) +.138*rxt(k,303)*y(k,573) +1.128*rxt(k,306)*y(k,625) + &
2.034*rxt(k,309)*y(k,808) +.384*rxt(k,312)*y(k,517) +2.229*rxt(k,315)*y(k,662) +.572*rxt(k,318)*y(k,504) + &
2.140*rxt(k,322)*y(k,661) +.070*rxt(k,485)*y(k,757) +.982*rxt(k,490)*y(k,425) +2.000*rxt(k,492)*y(k,759) + &
.956*rxt(k,496)*y(k,770) +.270*rxt(k,500)*y(k,755) +2.825*rxt(k,504)*y(k,756) +1.600*rxt(k,512)*y(k,446) + &
2.084*rxt(k,519)*y(k,348) +2.084*rxt(k,526)*y(k,349) +2.244*rxt(k,533)*y(k,350) +1.500*rxt(k,542)*y(k,590) + &
.900*rxt(k,551)*y(k,664) +2.680*rxt(k,560)*y(k,582) +1.895*rxt(k,572)*y(k,731) +1.830*rxt(k,577)*y(k,761) + &
2.721*rxt(k,584)*y(k,586) +1.160*rxt(k,599)*y(k,359) +2.643*rxt(k,603)*y(k,193) +1.216*rxt(k,606)*y(k,95) + &
2.445*rxt(k,610)*y(k,194) +2.532*rxt(k,613)*y(k,280) +2.499*rxt(k,616)*y(k,581) +1.359*rxt(k,619)*y(k,621) + &
1.812*rxt(k,622)*y(k,96) +2.355*rxt(k,626)*y(k,115) +2.337*rxt(k,629)*y(k,137) +2.193*rxt(k,632)*y(k,195) + &
2.948*rxt(k,635)*y(k,209) +2.250*rxt(k,639)*y(k,281) +2.361*rxt(k,642)*y(k,258) +2.092*rxt(k,645)*y(k,11) + &
.868*rxt(k,649)*y(k,24) +1.116*rxt(k,653)*y(k,37) +1.608*rxt(k,657)*y(k,506) +2.010*rxt(k,661)*y(k,622) + &
1.864*rxt(k,664)*y(k,97) +2.151*rxt(k,668)*y(k,104) +2.103*rxt(k,671)*y(k,116) +1.914*rxt(k,674)*y(k,138) + &
1.947*rxt(k,677)*y(k,146) +1.977*rxt(k,680)*y(k,196) +1.986*rxt(k,683)*y(k,282) +2.028*rxt(k,686)*y(k,319) + &
2.848*rxt(k,689)*y(k,102) +2.660*rxt(k,693)*y(k,210) +.720*rxt(k,697)*y(k,87) +2.154*rxt(k,701)*y(k,217) + &
2.178*rxt(k,704)*y(k,251) +.600*rxt(k,707)*y(k,5) +1.324*rxt(k,711)*y(k,7) +2.536*rxt(k,715)*y(k,12) + &
.960*rxt(k,719)*y(k,15) +.692*rxt(k,723)*y(k,17) +1.224*rxt(k,727)*y(k,76) +1.758*rxt(k,731)*y(k,25) + &
1.620*rxt(k,734)*y(k,47) +2.244*rxt(k,737)*y(k,38) +1.905*rxt(k,741)*y(k,507) +1.552*rxt(k,744)*y(k,728) + &
1.908*rxt(k,748)*y(k,91) +1.863*rxt(k,752)*y(k,109) +1.767*rxt(k,755)*y(k,139) +1.761*rxt(k,758)*y(k,197) + &
1.941*rxt(k,761)*y(k,207) +1.635*rxt(k,764)*y(k,225) +1.899*rxt(k,767)*y(k,310) +1.815*rxt(k,770)*y(k,320) + &
2.372*rxt(k,773)*y(k,129) +2.496*rxt(k,777)*y(k,211) +1.839*rxt(k,781)*y(k,292) +1.356*rxt(k,784)*y(k,98) + &
1.584*rxt(k,787)*y(k,89) +1.833*rxt(k,791)*y(k,153) +1.890*rxt(k,794)*y(k,117) +1.758*rxt(k,797)*y(k,147) + &
1.821*rxt(k,800)*y(k,283) +1.926*rxt(k,803)*y(k,218) +1.824*rxt(k,806)*y(k,259) +1.092*rxt(k,809)*y(k,558) + &
1.912*rxt(k,812)*y(k,13) +1.940*rxt(k,816)*y(k,30) +2.088*rxt(k,820)*y(k,6) +1.554*rxt(k,824)*y(k,70) + &
1.866*rxt(k,827)*y(k,393) +1.614*rxt(k,830)*y(k,134) +1.659*rxt(k,833)*y(k,140) +1.701*rxt(k,836)*y(k,154) + &
1.653*rxt(k,839)*y(k,198) +1.875*rxt(k,842)*y(k,214) +1.653*rxt(k,845)*y(k,284) +1.716*rxt(k,848)*y(k,321) + &
1.779*rxt(k,851)*y(k,322) +1.680*rxt(k,854)*y(k,130) +2.236*rxt(k,857)*y(k,145) +2.360*rxt(k,861)*y(k,212) + &
1.752*rxt(k,865)*y(k,293) +1.314*rxt(k,868)*y(k,99) +1.648*rxt(k,871)*y(k,90) +1.988*rxt(k,875)*y(k,92) + &
1.722*rxt(k,879)*y(k,111) +1.755*rxt(k,882)*y(k,118) +1.590*rxt(k,885)*y(k,148) +1.779*rxt(k,888)*y(k,185) + &
1.704*rxt(k,891)*y(k,311) +1.581*rxt(k,894)*y(k,36) +1.524*rxt(k,897)*y(k,46) +2.220*rxt(k,900)*y(k,77) + &
1.764*rxt(k,904)*y(k,402) +1.106*rxt(k,907)*y(k,670) +1.608*rxt(k,909)*y(k,103) +1.599*rxt(k,912)*y(k,155) + &
1.383*rxt(k,915)*y(k,224) +1.578*rxt(k,918)*y(k,274) +1.593*rxt(k,921)*y(k,318) +1.876*rxt(k,924)*y(k,41) + &
1.458*rxt(k,928)*y(k,69) +1.704*rxt(k,931)*y(k,405) +1.084*rxt(k,934)*y(k,671) +1.431*rxt(k,936)*y(k,107) + &
1.554*rxt(k,939)*y(k,229) +1.482*rxt(k,942)*y(k,230) +1.548*rxt(k,945)*y(k,275) +1.572*rxt(k,948)*y(k,336) + &
1.896*rxt(k,951)*y(k,29) +1.446*rxt(k,955)*y(k,78) +1.076*rxt(k,958)*y(k,409) +1.070*rxt(k,960)*y(k,672) + &
1.449*rxt(k,962)*y(k,110) +1.353*rxt(k,965)*y(k,133) +1.464*rxt(k,968)*y(k,231) +1.500*rxt(k,971)*y(k,234) + &
1.530*rxt(k,974)*y(k,276) +1.542*rxt(k,977)*y(k,337) +1.952*rxt(k,980)*y(k,42) +1.443*rxt(k,984)*y(k,74) + &
1.050*rxt(k,987)*y(k,411) +1.060*rxt(k,989)*y(k,673) +1.314*rxt(k,991)*y(k,131) +1.398*rxt(k,994)*y(k,184) + &
1.488*rxt(k,997)*y(k,235) +1.413*rxt(k,1000)*y(k,238) +1.518*rxt(k,1003)*y(k,277) +1.536*rxt(k,1006)*y(k,338) + &
1.497*rxt(k,1009)*y(k,44) +1.365*rxt(k,1012)*y(k,79) +1.042*rxt(k,1015)*y(k,413) +1.054*rxt(k,1017)*y(k,674) + &
1.311*rxt(k,1019)*y(k,152) +1.461*rxt(k,1022)*y(k,236) +1.422*rxt(k,1025)*y(k,239) +1.515*rxt(k,1028)*y(k,278) + &
1.530*rxt(k,1031)*y(k,339) +1.518*rxt(k,1034)*y(k,31) +1.434*rxt(k,1037)*y(k,75) +1.036*rxt(k,1040)*y(k,415) + &
1.824*rxt(k,1042)*y(k,40) +1.050*rxt(k,1046)*y(k,675) +1.311*rxt(k,1048)*y(k,158) +1.512*rxt(k,1051)*y(k,279) + &
1.443*rxt(k,1054)*y(k,301) +1.006*rxt(k,1057)*y(k,340) +1.503*rxt(k,1059)*y(k,43) +1.374*rxt(k,1062)*y(k,80) + &
1.032*rxt(k,1065)*y(k,374) +1.968*rxt(k,1072)*y(k,739) +2.000*rxt(k,1080)*y(k,351) +2.919*rxt(k,1082)*y(k,52) + &
1.800*rxt(k,1093)*y(k,585) +1.930*rxt(k,1102)*y(k,385) +1.930*rxt(k,1110)*y(k,784) +1.922*rxt(k,1115)*y(k,19) + &
2.853*rxt(k,1117)*y(k,33) +3.696*rxt(k,1130)*y(k,84) +3.600*rxt(k,1142)*y(k,263) +2.802*rxt(k,1154)*y(k,176) + &
1.870*rxt(k,1165)*y(k,180) +2.802*rxt(k,1175)*y(k,389) +2.802*rxt(k,1186)*y(k,788) +2.763*rxt(k,1197)*y(k,457) + &
2.769*rxt(k,1207)*y(k,780) +2.805*rxt(k,1218)*y(k,49) +2.724*rxt(k,1229)*y(k,28) +.936*rxt(k,1232)*y(k,262) + &
3.584*rxt(k,1234)*y(k,72) +1.480*rxt(k,1245)*y(k,213) +3.512*rxt(k,1256)*y(k,264) +3.532*rxt(k,1269)*y(k,312) + &
2.655*rxt(k,1281)*y(k,120) +2.697*rxt(k,1291)*y(k,168) +2.697*rxt(k,1302)*y(k,177) +1.804*rxt(k,1313)*y(k,122) + &
2.700*rxt(k,1320)*y(k,181) +2.694*rxt(k,1331)*y(k,386) +2.685*rxt(k,1343)*y(k,391) +2.703*rxt(k,1353)*y(k,395) + &
2.703*rxt(k,1365)*y(k,792) +2.655*rxt(k,1377)*y(k,798) +2.697*rxt(k,1389)*y(k,785) +2.685*rxt(k,1401)*y(k,789) + &
3.524*rxt(k,1411)*y(k,285) +2.703*rxt(k,1424)*y(k,81) +2.610*rxt(k,1435)*y(k,456) +3.556*rxt(k,1446)*y(k,781) + &
2.538*rxt(k,1460)*y(k,82) +3.328*rxt(k,1471)*y(k,317) +3.200*rxt(k,1484)*y(k,71) +3.280*rxt(k,1495)*y(k,215) + &
3.176*rxt(k,1508)*y(k,265) +2.586*rxt(k,1521)*y(k,136) +2.553*rxt(k,1533)*y(k,113) +3.100*rxt(k,1544)*y(k,208) + &
2.547*rxt(k,1557)*y(k,178) +.246*rxt(k,1568)*y(k,101) +2.508*rxt(k,1578)*y(k,398) +2.577*rxt(k,1590)*y(k,141) + &
3.428*rxt(k,1603)*y(k,182) +2.586*rxt(k,1617)*y(k,257) +2.589*rxt(k,1628)*y(k,290) +2.574*rxt(k,1641)*y(k,387) + &
2.547*rxt(k,1653)*y(k,202) +2.589*rxt(k,1664)*y(k,269) +2.568*rxt(k,1677)*y(k,216) +1.730*rxt(k,1689)*y(k,123) + &
2.553*rxt(k,1698)*y(k,392) +2.508*rxt(k,1709)*y(k,794) +2.574*rxt(k,1721)*y(k,786) +2.559*rxt(k,1733)*y(k,790) + &
2.583*rxt(k,1744)*y(k,396) +2.577*rxt(k,1756)*y(k,799) +2.583*rxt(k,1769)*y(k,793) +2.445*rxt(k,1781)*y(k,26) + &
2.220*rxt(k,1790)*y(k,83) +2.439*rxt(k,1800)*y(k,127) +3.200*rxt(k,1812)*y(k,267) +2.445*rxt(k,1824)*y(k,787) + &
3.264*rxt(k,1836)*y(k,183) +2.187*rxt(k,1850)*y(k,400) +2.265*rxt(k,1860)*y(k,782) +2.364*rxt(k,1872)*y(k,783) + &
2.325*rxt(k,1883)*y(k,791) +2.196*rxt(k,1894)*y(k,796) +2.454*rxt(k,1904)*y(k,128) +3.280*rxt(k,1915)*y(k,124) + &
2.073*rxt(k,1927)*y(k,67) +2.856*rxt(k,1937)*y(k,291) +2.169*rxt(k,1950)*y(k,797) +1.974*rxt(k,1961)*y(k,53) + &
2.271*rxt(k,1971)*y(k,219) +1.875*rxt(k,1983)*y(k,404) +2.064*rxt(k,1993)*y(k,795) +1.700*rxt(k,2004)*y(k,249) + &
3.196*rxt(k,2015)*y(k,355) +3.244*rxt(k,2030)*y(k,879) +3.308*rxt(k,2045)*y(k,479) +.705*rxt(k,2059)*y(k,771) + &
2.310*rxt(k,2070)*y(k,881) +1.908*rxt(k,2083)*y(k,55) +1.998*rxt(k,2093)*y(k,800) +1.866*rxt(k,2103)*y(k,56) + &
1.947*rxt(k,2113)*y(k,801) +1.836*rxt(k,2123)*y(k,57) +1.914*rxt(k,2133)*y(k,802) +1.815*rxt(k,2143)*y(k,58) + &
1.893*rxt(k,2153)*y(k,803) +1.881*rxt(k,2163)*y(k,804) +1.816*rxt(k,2173)*y(k,833) +2.180*rxt(k,2177)*y(k,383) + &
2.080*rxt(k,2181)*y(k,665) +2.770*rxt(k,2185)*y(k,706) +2.435*rxt(k,2190)*y(k,746) +1.640*rxt(k,2195)*y(k,776) + &
2.340*rxt(k,2203)*y(k,676) +2.048*rxt(k,2207)*y(k,572) +2.620*rxt(k,2211)*y(k,644) +2.860*rxt(k,2216)*y(k,704) + &
2.490*rxt(k,2221)*y(k,721) +3.720*rxt(k,2226)*y(k,14) +3.135*rxt(k,2231)*y(k,16) +2.680*rxt(k,2236)*y(k,32) + &
1.616*rxt(k,2240)*y(k,360) +2.364*rxt(k,2248)*y(k,373) +1.744*rxt(k,2252)*y(k,807) +2.620*rxt(k,2256)*y(k,596) + &
2.875*rxt(k,2261)*y(k,697) +2.530*rxt(k,2266)*y(k,711) +2.390*rxt(k,2271)*y(k,719) +3.600*rxt(k,2276)*y(k,240) + &
3.070*rxt(k,2281)*y(k,302) +2.592*rxt(k,2286)*y(k,324) +1.936*rxt(k,2290)*y(k,816) +.600*rxt(k,2294)*y(k,667) + &
2.448*rxt(k,2298)*y(k,376) +2.685*rxt(k,2302)*y(k,597) +2.960*rxt(k,2307)*y(k,698) +2.655*rxt(k,2312)*y(k,712) + &
3.585*rxt(k,2317)*y(k,241) +3.085*rxt(k,2322)*y(k,303) +2.584*rxt(k,2327)*y(k,325) +.900*rxt(k,2331)*y(k,630) + &
2.512*rxt(k,2335)*y(k,377) +2.690*rxt(k,2339)*y(k,598) +2.970*rxt(k,2344)*y(k,699) +2.685*rxt(k,2349)*y(k,713) + &
3.575*rxt(k,2354)*y(k,242) +3.085*rxt(k,2359)*y(k,304) +2.580*rxt(k,2364)*y(k,326) +1.200*rxt(k,2368)*y(k,119) + &
2.568*rxt(k,2372)*y(k,378) +2.192*rxt(k,2376)*y(k,599) +3.030*rxt(k,2380)*y(k,700) +2.760*rxt(k,2385)*y(k,714) + &
3.575*rxt(k,2390)*y(k,243) +3.110*rxt(k,2395)*y(k,305) +2.584*rxt(k,2400)*y(k,327) +2.616*rxt(k,2404)*y(k,380) + &
2.228*rxt(k,2408)*y(k,600) +3.080*rxt(k,2412)*y(k,701) +2.830*rxt(k,2417)*y(k,715) +3.590*rxt(k,2422)*y(k,244) + &
3.135*rxt(k,2427)*y(k,306) +2.596*rxt(k,2432)*y(k,328) +2.656*rxt(k,2436)*y(k,381) +2.268*rxt(k,2440)*y(k,601) + &
3.125*rxt(k,2444)*y(k,702) +2.895*rxt(k,2449)*y(k,716) +3.590*rxt(k,2454)*y(k,245) +3.150*rxt(k,2459)*y(k,307) + &
2.600*rxt(k,2464)*y(k,329) +2.692*rxt(k,2468)*y(k,382) +2.300*rxt(k,2472)*y(k,602) +3.165*rxt(k,2476)*y(k,703) + &
2.950*rxt(k,2481)*y(k,717) +3.595*rxt(k,2486)*y(k,246) +3.170*rxt(k,2491)*y(k,308) +2.604*rxt(k,2496)*y(k,330) + &
2.000*rxt(k,2500)*y(k,525) +2.000*rxt(k,2502)*y(k,623) +.750*rxt(k,2509)*y(k,344) +.276*rxt(k,2518)*y(k,831) + &
3.096*rxt(k,2520)*y(k,741) +.070*rxt(k,2524)*y(k,737) +.711*rxt(k,2527)*y(k,677) +3.000*rxt(k,2530)*y(k,346) + &
.876*rxt(k,2538)*y(k,39) +.384*rxt(k,2541)*y(k,508) +1.970*rxt(k,2544)*y(k,605) +.039*rxt(k,2549)*y(k,729) + &
2.000*rxt(k,2552)*y(k,643) +2.166*rxt(k,2554)*y(k,636) +2.000*rxt(k,2557)*y(k,482) +1.671*rxt(k,2564)*y(k,447) + &
2.733*rxt(k,2578)*y(k,826) +3.212*rxt(k,2581)*y(k,18) +.246*rxt(k,2585)*y(k,192) +.264*rxt(k,2589)*y(k,59) + &
1.464*rxt(k,2593)*y(k,575) +1.209*rxt(k,2597)*y(k,679) +.456*rxt(k,2600)*y(k,773) +2.523*rxt(k,2603)*y(k,528) + &
2.883*rxt(k,2606)*y(k,591) +3.792*rxt(k,2617)*y(k,606) +2.853*rxt(k,2627)*y(k,837) +1.788*rxt(k,2636)*y(k,552) + &
2.634*rxt(k,2646)*y(k,48) +.468*rxt(k,2649)*y(k,500) +3.700*rxt(k,2652)*y(k,641) +.516*rxt(k,2656)*y(k,394) + &
3.272*rxt(k,2659)*y(k,583) +2.883*rxt(k,2663)*y(k,570) +2.883*rxt(k,2666)*y(k,368) +.444*rxt(k,2669)*y(k,634) + &
.488*rxt(k,2672)*y(k,35) +.243*rxt(k,2676)*y(k,20) +.729*rxt(k,2679)*y(k,45) +.057*rxt(k,2682)*y(k,112) + &
1.800*rxt(k,2685)*y(k,635) +2.476*rxt(k,2688)*y(k,518) +2.418*rxt(k,2692)*y(k,199) +2.142*rxt(k,2695)*y(k,286) + &
1.563*rxt(k,2698)*y(k,710) +.612*rxt(k,2701)*y(k,626) +2.037*rxt(k,2705)*y(k,465) +.460*rxt(k,2708)*y(k,592) + &
1.872*rxt(k,2714)*y(k,419) +.990*rxt(k,2717)*y(k,420) +3.268*rxt(k,2720)*y(k,354) +2.496*rxt(k,2724)*y(k,828) + &
2.805*rxt(k,2727)*y(k,595) +.069*rxt(k,2737)*y(k,94) +.516*rxt(k,2741)*y(k,270) +.356*rxt(k,2749)*y(k,60) + &
.462*rxt(k,2754)*y(k,659) +1.125*rxt(k,2757)*y(k,464) +.306*rxt(k,2760)*y(k,649) +.992*rxt(k,2763)*y(k,164) + &
.585*rxt(k,2767)*y(k,247) +1.452*rxt(k,2770)*y(k,407) +1.924*rxt(k,2773)*y(k,563) +1.976*rxt(k,2777)*y(k,190) + &
.988*rxt(k,2781)*y(k,523) +2.757*rxt(k,2785)*y(k,654) +1.491*rxt(k,2788)*y(k,501) +1.870*rxt(k,2797)*y(k,628) + &
.120*rxt(k,2807)*y(k,531) +3.112*rxt(k,2814)*y(k,825) +.836*rxt(k,2818)*y(k,526) +.060*rxt(k,2822)*y(k,579) + &
3.672*rxt(k,2826)*y(k,610) +1.512*rxt(k,2830)*y(k,624) +1.408*rxt(k,2834)*y(k,403) +1.760*rxt(k,2838)*y(k,726) + &
3.588*rxt(k,2842)*y(k,268) +2.742*rxt(k,2846)*y(k,100) +1.896*rxt(k,2849)*y(k,516) +2.334*rxt(k,2853)*y(k,206) + &
2.120*rxt(k,2856)*y(k,260) +3.116*rxt(k,2860)*y(k,287) +3.244*rxt(k,2864)*y(k,603) +.595*rxt(k,2868)*y(k,513) + &
1.312*rxt(k,2873)*y(k,648) +3.292*rxt(k,2877)*y(k,657) +.120*rxt(k,2881)*y(k,725) +1.158*rxt(k,2885)*y(k,421) + &
1.065*rxt(k,2888)*y(k,422) +.336*rxt(k,2891)*y(k,62) +.048*rxt(k,2895)*y(k,646) +1.696*rxt(k,2899)*y(k,655) + &
.063*rxt(k,2903)*y(k,663) +1.533*rxt(k,2906)*y(k,61) +.930*rxt(k,2909)*y(k,165) +1.232*rxt(k,2912)*y(k,647) + &
2.682*rxt(k,2916)*y(k,740) +1.767*rxt(k,2919)*y(k,498) +.108*rxt(k,2922)*y(k,584) +2.421*rxt(k,2926)*y(k,514) + &
1.724*rxt(k,2937)*y(k,505) +3.280*rxt(k,2941)*y(k,566) +1.460*rxt(k,2945)*y(k,642) +2.752*rxt(k,2949)*y(k,361) + &
1.720*rxt(k,2953)*y(k,743) +2.864*rxt(k,2957)*y(k,772) +.195*rxt(k,2961)*y(k,805) +.932*rxt(k,2964)*y(k,474) + &
3.532*rxt(k,2968)*y(k,615) +.594*rxt(k,2972)*y(k,410) +.780*rxt(k,2975)*y(k,179) +2.712*rxt(k,2979)*y(k,493) + &
2.451*rxt(k,2983)*y(k,342) +1.914*rxt(k,2986)*y(k,742) +3.060*rxt(k,2989)*y(k,366) +.532*rxt(k,2993)*y(k,289) + &
.276*rxt(k,2998)*y(k,653) +3.028*rxt(k,3002)*y(k,730) +1.828*rxt(k,3006)*y(k,554) +1.320*rxt(k,3019)*y(k,724) + &
2.280*rxt(k,3023)*y(k,449) +3.312*rxt(k,3027)*y(k,204) +3.108*rxt(k,3031)*y(k,640) +2.488*rxt(k,3035)*y(k,470) + &
1.452*rxt(k,3039)*y(k,490) +3.484*rxt(k,3042)*y(k,459) +.225*rxt(k,3046)*y(k,509) +2.664*rxt(k,3049)*y(k,347) + &
2.238*rxt(k,3052)*y(k,811) +1.216*rxt(k,3055)*y(k,369) +1.744*rxt(k,3059)*y(k,372) +.408*rxt(k,3063)*y(k,64) + &
.411*rxt(k,3067)*y(k,200) +1.539*rxt(k,3071)*y(k,412) +.894*rxt(k,3074)*y(k,186) +.285*rxt(k,3077)*y(k,476) + &
2.079*rxt(k,3080)*y(k,332) +.600*rxt(k,3083)*y(k,266) +1.509*rxt(k,3087)*y(k,63) +2.236*rxt(k,3090)*y(k,121) + &
.780*rxt(k,3094)*y(k,294) +2.276*rxt(k,3098)*y(k,668) +2.976*rxt(k,3109)*y(k,567) +2.560*rxt(k,3121)*y(k,367) + &
2.292*rxt(k,3125)*y(k,352) +1.956*rxt(k,3128)*y(k,727) +2.860*rxt(k,3132)*y(k,562) +3.228*rxt(k,3136)*y(k,175) + &
3.184*rxt(k,3140)*y(k,616) +1.816*rxt(k,3144)*y(k,722) +2.115*rxt(k,3148)*y(k,723) +1.995*rxt(k,3151)*y(k,365) + &
1.628*rxt(k,3154)*y(k,492) +1.072*rxt(k,3158)*y(k,478) +3.096*rxt(k,3162)*y(k,473) +2.452*rxt(k,3166)*y(k,491) + &
2.636*rxt(k,3170)*y(k,489) +2.296*rxt(k,3174)*y(k,27) +2.310*rxt(k,3178)*y(k,460) +3.084*rxt(k,3181)*y(k,823) + &
2.540*rxt(k,3185)*y(k,162) +.492*rxt(k,3189)*y(k,66) +1.545*rxt(k,3193)*y(k,414) +1.734*rxt(k,3196)*y(k,65) + &
1.632*rxt(k,3199)*y(k,170) +1.419*rxt(k,3202)*y(k,166) +1.293*rxt(k,3205)*y(k,248) +1.431*rxt(k,3208)*y(k,309) + &
1.968*rxt(k,3211)*y(k,73) +2.301*rxt(k,3215)*y(k,565) +2.364*rxt(k,3218)*y(k,364) +2.480*rxt(k,3221)*y(k,496) + &
3.008*rxt(k,3226)*y(k,363) +3.168*rxt(k,3239)*y(k,569) +2.964*rxt(k,3252)*y(k,125) +2.816*rxt(k,3256)*y(k,187) + &
2.928*rxt(k,3260)*y(k,271) +2.012*rxt(k,3264)*y(k,313) +3.460*rxt(k,3268)*y(k,568) +2.504*rxt(k,3273)*y(k,362) + &
2.049*rxt(k,3277)*y(k,682) +1.952*rxt(k,3280)*y(k,593) +2.106*rxt(k,3284)*y(k,408) +2.136*rxt(k,3287)*y(k,171) + &
2.094*rxt(k,3290)*y(k,617) +1.072*rxt(k,3293)*y(k,167) +1.941*rxt(k,3297)*y(k,495) +.572*rxt(k,3300)*y(k,832) + &
2.144*rxt(k,3304)*y(k,744) +3.104*rxt(k,3308)*y(k,161) +1.980*rxt(k,3312)*y(k,174) +2.652*rxt(k,3316)*y(k,468) + &
2.216*rxt(k,3320)*y(k,485) +3.132*rxt(k,3324)*y(k,461) +2.784*rxt(k,3328)*y(k,471) +2.732*rxt(k,3332)*y(k,821) + &
2.796*rxt(k,3336)*y(k,813) +1.491*rxt(k,3340)*y(k,205) +.681*rxt(k,3343)*y(k,323) +2.912*rxt(k,3346)*y(k,68) + &
1.053*rxt(k,3350)*y(k,830) +1.509*rxt(k,3353)*y(k,416) +.076*rxt(k,3356)*y(k,475) +2.097*rxt(k,3360)*y(k,156) + &
.939*rxt(k,3363)*y(k,157) +2.308*rxt(k,3366)*y(k,142) +2.524*rxt(k,3370)*y(k,188) +2.748*rxt(k,3374)*y(k,254) + &
2.624*rxt(k,3378)*y(k,272) +1.824*rxt(k,3382)*y(k,314) +2.512*rxt(k,3386)*y(k,333) +2.344*rxt(k,3390)*y(k,576) + &
1.872*rxt(k,3394)*y(k,684) +1.908*rxt(k,3397)*y(k,618) +2.548*rxt(k,3400)*y(k,51) +2.324*rxt(k,3404)*y(k,488) + &
1.840*rxt(k,3408)*y(k,486) +1.508*rxt(k,3412)*y(k,487) +2.804*rxt(k,3416)*y(k,824) +1.557*rxt(k,3420)*y(k,834) + &
2.760*rxt(k,3423)*y(k,815) +1.374*rxt(k,3427)*y(k,534) +.018*rxt(k,3430)*y(k,417) +3.024*rxt(k,3433)*y(k,356) + &
3.635*rxt(k,3446)*y(k,436) +.724*rxt(k,3461)*y(k,557) +1.424*rxt(k,3465)*y(k,632) +2.304*rxt(k,3469)*y(k,587) + &
1.560*rxt(k,3481)*y(k,375) +1.320*rxt(k,3484)*y(k,571) +1.374*rxt(k,3487)*y(k,54) +1.836*rxt(k,3490)*y(k,237) + &
2.037*rxt(k,3494)*y(k,406) +2.032*rxt(k,3497)*y(k,23) +2.012*rxt(k,3501)*y(k,143) +2.324*rxt(k,3505)*y(k,173) + &
2.384*rxt(k,3509)*y(k,220) +2.272*rxt(k,3513)*y(k,226) +1.869*rxt(k,3517)*y(k,255) +1.749*rxt(k,3520)*y(k,273) + &
2.092*rxt(k,3523)*y(k,295) +1.776*rxt(k,3527)*y(k,315) +2.340*rxt(k,3531)*y(k,334) +1.755*rxt(k,3535)*y(k,686) + &
3.805*rxt(k,3538)*y(k,529) +1.782*rxt(k,3555)*y(k,619) +1.629*rxt(k,3558)*y(k,494) +1.920*rxt(k,3561)*y(k,472) + &
2.208*rxt(k,3564)*y(k,462) +2.836*rxt(k,3568)*y(k,469) +2.668*rxt(k,3572)*y(k,820) +1.856*rxt(k,3576)*y(k,835) + &
2.260*rxt(k,3580)*y(k,172) +2.168*rxt(k,3590)*y(k,108) +2.292*rxt(k,3594)*y(k,126) +1.912*rxt(k,3598)*y(k,144) + &
2.136*rxt(k,3602)*y(k,149) +2.152*rxt(k,3606)*y(k,189) +1.956*rxt(k,3610)*y(k,227) +2.076*rxt(k,3614)*y(k,232) + &
1.668*rxt(k,3618)*y(k,256) +2.100*rxt(k,3621)*y(k,296) +1.684*rxt(k,3625)*y(k,297) +1.410*rxt(k,3629)*y(k,316) + &
2.236*rxt(k,3632)*y(k,335) +1.683*rxt(k,3636)*y(k,688) +1.704*rxt(k,3639)*y(k,611) +.820*rxt(k,3642)*y(k,564) + &
.966*rxt(k,3646)*y(k,151) +2.088*rxt(k,3649)*y(k,233) +1.644*rxt(k,3653)*y(k,261) +1.708*rxt(k,3656)*y(k,298) + &
1.656*rxt(k,3660)*y(k,612) +1.812*rxt(k,3663)*y(k,818) +1.195*rxt(k,3667)*y(k,817) +2.248*rxt(k,3672)*y(k,467) + &
1.870*rxt(k,3676)*y(k,466) +1.677*rxt(k,3681)*y(k,458) +1.912*rxt(k,3684)*y(k,477) +2.480*rxt(k,3688)*y(k,814) + &
.728*rxt(k,3692)*y(k,812) +1.836*rxt(k,3696)*y(k,222) +2.028*rxt(k,3700)*y(k,253) +1.536*rxt(k,3704)*y(k,300) + &
1.623*rxt(k,3708)*y(k,627) +1.968*rxt(k,3711)*y(k,822) +1.856*rxt(k,3715)*y(k,106) +1.796*rxt(k,3719)*y(k,223) + &
1.876*rxt(k,3723)*y(k,228) +1.605*rxt(k,3727)*y(k,613) +2.695*rxt(k,3730)*y(k,169) +1.752*rxt(k,3735)*y(k,132) + &
1.512*rxt(k,3739)*y(k,252) +1.472*rxt(k,3742)*y(k,299) +1.593*rxt(k,3746)*y(k,629) +1.860*rxt(k,3749)*y(k,588) + &
1.479*rxt(k,3759)*y(k,614) +1.820*rxt(k,3762)*y(k,105) +1.744*rxt(k,3766)*y(k,221) +1.732*rxt(k,3770)*y(k,331) + &
2.825*rxt(k,3774)*y(k,463) +2.215*rxt(k,3779)*y(k,836) +1.935*rxt(k,3784)*y(k,652) +1.797*rxt(k,3797)*y(k,651) + &
1.084*rxt(k,3810)*y(k,608) +rxt(k,3821)*y(k,433) +1.028*rxt(k,3829)*y(k,481) +1.030*rxt(k,3837)*y(k,503) + &
1.542*rxt(k,3845)*y(k,522) +.702*rxt(k,3854)*y(k,444) +rxt(k,3856)*y(k,540) +2.000*rxt(k,3865)*y(k,515) + &
2.000*rxt(k,3887)*y(k,345) +2.000*rxt(k,3889)*y(k,829) +1.407*rxt(k,3904)*y(k,50) +2.982*rxt(k,3912)*y(k,250) + &
1.910*rxt(k,3915)*y(k,397) +2.000*rxt(k,3917)*y(k,203) +2.000*rxt(k,3919)*y(k,779) +2.000*rxt(k,3930)*y(k,379) + &
1.900*rxt(k,3944)*y(k,748) +2.000*rxt(k,3968)*y(k,747) +1.542*rxt(k,3970)*y(k,390) +1.920*rxt(k,3973)*y(k,541) + &
1.617*rxt(k,3990)*y(k,660) +1.920*rxt(k,3999)*y(k,497) +2.688*rxt(k,4002)*y(k,480) +2.292*rxt(k,4012)*y(k,159) + &
.045*rxt(k,4023)*y(k,353) +.651*rxt(k,4028)*y(k,401) +1.290*rxt(k,4031)*y(k,520) +1.440*rxt(k,4041)*y(k,439) + &
.651*rxt(k,4053)*y(k,399) +1.840*rxt(k,4056)*y(k,753) +.208*rxt(k,4058)*y(k,160) +2.550*rxt(k,4063)*y(k,689) + &
1.870*rxt(k,4068)*y(k,639) +1.870*rxt(k,4070)*y(k,542) +1.562*rxt(k,4072)*y(k,754) +1.378*rxt(k,4074)*y(k,810) + &
1.160*rxt(k,4083)*y(k,443) +1.160*rxt(k,4087)*y(k,692) +1.160*rxt(k,4091)*y(k,438) +2.298*rxt(k,4095)*y(k,521) + &
.680*rxt(k,4105)*y(k,774) +.500*rxt(k,4106)*y(k,775) +1.160*rxt(k,4107)*y(k,371) +1.804*rxt(k,4115)*y(k,524) + &
2.432*rxt(k,4117)*y(k,656) +1.676*rxt(k,4121)*y(k,426) +1.676*rxt(k,4125)*y(k,718) +.400*rxt(k,4131)*y(k,1) + &
1.536*rxt(k,4133)*y(k,658) +1.416*rxt(k,4139)*y(k,499) +1.743*rxt(k,4151)*y(k,574) +1.880*rxt(k,4161)*y(k,720) + &
.400*rxt(k,4168)*y(k,2) +1.224*rxt(k,4169)*y(k,827) +1.302*rxt(k,4177)*y(k,732) +2.348*rxt(k,4180)*y(k,733) + &
1.176*rxt(k,4184)*y(k,734) +.846*rxt(k,4188)*y(k,735) +.897*rxt(k,4191)*y(k,736) +1.062*rxt(k,4194)*y(k,762) + &
.960*rxt(k,4197)*y(k,763) +.536*rxt(k,4201)*y(k,764) +1.520*rxt(k,4205)*y(k,765) +.624*rxt(k,4209)*y(k,766) + &
2.256*rxt(k,4213)*y(k,767))*y(k,705) + (2.000*rxt(k,238)*y(k,512) +.645*rxt(k,295)*y(k,784) + &
2.000*rxt(k,431)*y(k,758) +rxt(k,548)*y(k,590) +3.196*rxt(k,568)*y(k,582) +2.247*rxt(k,592)*y(k,586) + &
1.898*rxt(k,1077)*y(k,739) +2.760*rxt(k,1089)*y(k,52) +.645*rxt(k,1106)*y(k,385) +2.445*rxt(k,1123)*y(k,33) + &
2.502*rxt(k,1138)*y(k,84) +2.553*rxt(k,1150)*y(k,263) +2.748*rxt(k,1161)*y(k,176) +1.185*rxt(k,1182)*y(k,389) + &
1.185*rxt(k,1193)*y(k,788) +.192*rxt(k,1203)*y(k,457) +1.410*rxt(k,1214)*y(k,780) +2.652*rxt(k,1225)*y(k,49) + &
1.526*rxt(k,1242)*y(k,72) +2.328*rxt(k,1265)*y(k,264) +1.536*rxt(k,1278)*y(k,312) +1.732*rxt(k,1288)*y(k,120) + &
2.679*rxt(k,1298)*y(k,168) +2.481*rxt(k,1309)*y(k,177) +.477*rxt(k,1327)*y(k,181) +1.977*rxt(k,1339)*y(k,386) + &
2.124*rxt(k,1349)*y(k,391) +.090*rxt(k,1361)*y(k,395) +.090*rxt(k,1373)*y(k,792) +2.178*rxt(k,1385)*y(k,798) + &
1.977*rxt(k,1397)*y(k,785) +2.124*rxt(k,1407)*y(k,789) +.468*rxt(k,1419)*y(k,285) +.033*rxt(k,1431)*y(k,81) + &
1.617*rxt(k,1442)*y(k,456) +2.096*rxt(k,1455)*y(k,781) +.066*rxt(k,1467)*y(k,82) +1.428*rxt(k,1479)*y(k,317) + &
1.400*rxt(k,1492)*y(k,71) +2.049*rxt(k,1504)*y(k,215) +2.091*rxt(k,1517)*y(k,265) +2.584*rxt(k,1528)*y(k,136) + &
2.373*rxt(k,1540)*y(k,113) +2.202*rxt(k,1553)*y(k,208) +2.136*rxt(k,1564)*y(k,178) +1.244*rxt(k,1585)*y(k,398) + &
.712*rxt(k,1598)*y(k,141) +2.368*rxt(k,1612)*y(k,182) +.168*rxt(k,1624)*y(k,257) +.604*rxt(k,1636)*y(k,290) + &
2.067*rxt(k,1649)*y(k,387) +2.034*rxt(k,1660)*y(k,202) +.604*rxt(k,1672)*y(k,269) +2.175*rxt(k,1685)*y(k,216) + &
2.046*rxt(k,1705)*y(k,392) +1.244*rxt(k,1716)*y(k,794) +2.067*rxt(k,1729)*y(k,786) +2.046*rxt(k,1740)*y(k,790) + &
.537*rxt(k,1752)*y(k,396) +2.696*rxt(k,1764)*y(k,799) +.537*rxt(k,1777)*y(k,793) +1.290*rxt(k,1797)*y(k,83) + &
1.980*rxt(k,1807)*y(k,127) +2.382*rxt(k,1820)*y(k,267) +1.911*rxt(k,1832)*y(k,787) +2.248*rxt(k,1845)*y(k,183) + &
1.290*rxt(k,1857)*y(k,400) +1.216*rxt(k,1867)*y(k,782) +2.070*rxt(k,1879)*y(k,783) +1.920*rxt(k,1890)*y(k,791) + &
1.290*rxt(k,1901)*y(k,796) +.447*rxt(k,1911)*y(k,128) +.012*rxt(k,1922)*y(k,124) +1.194*rxt(k,1934)*y(k,67) + &
1.635*rxt(k,1946)*y(k,291) +1.800*rxt(k,1957)*y(k,797) +1.134*rxt(k,1968)*y(k,53) +2.516*rxt(k,1978)*y(k,219) + &
1.154*rxt(k,1990)*y(k,404) +1.719*rxt(k,2000)*y(k,795) +.060*rxt(k,2010)*y(k,249) +.224*rxt(k,2025)*y(k,355) + &
1.320*rxt(k,2040)*y(k,879) +.304*rxt(k,2054)*y(k,479) +1.314*rxt(k,2066)*y(k,771) +.680*rxt(k,2078)*y(k,881) + &
1.100*rxt(k,2090)*y(k,55) +1.120*rxt(k,2100)*y(k,800) +1.078*rxt(k,2110)*y(k,56) +1.094*rxt(k,2120)*y(k,801) + &
1.064*rxt(k,2130)*y(k,57) +1.076*rxt(k,2140)*y(k,802) +1.056*rxt(k,2150)*y(k,58) +1.066*rxt(k,2160)*y(k,803) + &
1.060*rxt(k,2170)*y(k,804) +1.950*rxt(k,2199)*y(k,776) +1.950*rxt(k,2244)*y(k,360) +.093*rxt(k,2514)*y(k,344) + &
2.000*rxt(k,2535)*y(k,346) +1.008*rxt(k,2570)*y(k,447) +2.883*rxt(k,2613)*y(k,591) +2.748*rxt(k,2623)*y(k,606) + &
.189*rxt(k,2632)*y(k,837) +1.920*rxt(k,2641)*y(k,552) +2.805*rxt(k,2733)*y(k,595) +.104*rxt(k,2745)*y(k,270) + &
1.146*rxt(k,2793)*y(k,501) +.789*rxt(k,2803)*y(k,628) +.168*rxt(k,2933)*y(k,514) +3.368*rxt(k,3014)*y(k,554) + &
3.300*rxt(k,3104)*y(k,668) +4.060*rxt(k,3115)*y(k,567) +1.932*rxt(k,3234)*y(k,363) +2.684*rxt(k,3247)*y(k,569) + &
.042*rxt(k,3442)*y(k,356) +2.108*rxt(k,3456)*y(k,436) +1.584*rxt(k,3477)*y(k,587) +1.270*rxt(k,3549)*y(k,529) + &
1.641*rxt(k,3586)*y(k,172) +1.060*rxt(k,3756)*y(k,588) +1.864*rxt(k,3792)*y(k,652) +1.932*rxt(k,3805)*y(k,651) + &
1.626*rxt(k,3814)*y(k,608) +1.536*rxt(k,3833)*y(k,481) +1.476*rxt(k,3841)*y(k,503) +1.692*rxt(k,3850)*y(k,522) + &
2.000*rxt(k,3893)*y(k,829) +1.692*rxt(k,3909)*y(k,50) +1.332*rxt(k,3996)*y(k,660) +2.883*rxt(k,4008)*y(k,480) + &
1.884*rxt(k,4019)*y(k,159) +1.332*rxt(k,4037)*y(k,520) +1.700*rxt(k,4066)*y(k,689) +2.067*rxt(k,4079)*y(k,810) + &
2.706*rxt(k,4101)*y(k,521) +1.440*rxt(k,4136)*y(k,658) +1.440*rxt(k,4142)*y(k,499) +2.382*rxt(k,4157)*y(k,574) + &
1.386*rxt(k,4165)*y(k,720) +1.386*rxt(k,4172)*y(k,827))*y(k,693) + (.570*rxt(k,540)*y(k,350) + &
.200*rxt(k,554)*y(k,664) +.252*rxt(k,1085)*y(k,52) +.244*rxt(k,1134)*y(k,84) +.240*rxt(k,1146)*y(k,263) + &
.252*rxt(k,1178)*y(k,389) +.252*rxt(k,1189)*y(k,788) +.252*rxt(k,1221)*y(k,49) +.232*rxt(k,1238)*y(k,72) + &
.285*rxt(k,1260)*y(k,264) +.295*rxt(k,1273)*y(k,312) +.152*rxt(k,1323)*y(k,181) +.305*rxt(k,1334)*y(k,386) + &
.375*rxt(k,1346)*y(k,391) +.300*rxt(k,1380)*y(k,798) +.305*rxt(k,1392)*y(k,785) +.375*rxt(k,1404)*y(k,789) + &
.008*rxt(k,1415)*y(k,285) +.140*rxt(k,1427)*y(k,81) +.436*rxt(k,1438)*y(k,456) +.300*rxt(k,1450)*y(k,781) + &
.404*rxt(k,1475)*y(k,317) +.220*rxt(k,1488)*y(k,71) +.260*rxt(k,1499)*y(k,215) +.260*rxt(k,1512)*y(k,265) + &
.285*rxt(k,1548)*y(k,208) +.180*rxt(k,1593)*y(k,141) +.185*rxt(k,1607)*y(k,182) +.190*rxt(k,1631)*y(k,290) + &
.290*rxt(k,1644)*y(k,387) +.492*rxt(k,1656)*y(k,202) +.190*rxt(k,1667)*y(k,269) +.496*rxt(k,1701)*y(k,392) + &
.290*rxt(k,1724)*y(k,786) +.496*rxt(k,1736)*y(k,790) +.285*rxt(k,1759)*y(k,799) +.204*rxt(k,1793)*y(k,83) + &
.275*rxt(k,1827)*y(k,787) +.175*rxt(k,1840)*y(k,183) +.492*rxt(k,1853)*y(k,400) +.252*rxt(k,1863)*y(k,782) + &
.480*rxt(k,1875)*y(k,783) +.484*rxt(k,1886)*y(k,791) +.492*rxt(k,1897)*y(k,796) +.188*rxt(k,1930)*y(k,67) + &
.230*rxt(k,1941)*y(k,291) +.476*rxt(k,1953)*y(k,797) +.168*rxt(k,1964)*y(k,53) +.464*rxt(k,1986)*y(k,404) + &
.464*rxt(k,1996)*y(k,795) +.012*rxt(k,2006)*y(k,249) +.612*rxt(k,2019)*y(k,355) +.420*rxt(k,2034)*y(k,879) + &
.105*rxt(k,2049)*y(k,479) +.292*rxt(k,2062)*y(k,771) +.280*rxt(k,2073)*y(k,881) +.152*rxt(k,2086)*y(k,55) + &
.452*rxt(k,2096)*y(k,800) +.144*rxt(k,2106)*y(k,56) +.436*rxt(k,2116)*y(k,801) +.136*rxt(k,2126)*y(k,57) + &
.420*rxt(k,2136)*y(k,802) +.132*rxt(k,2146)*y(k,58) +.400*rxt(k,2156)*y(k,803) +.384*rxt(k,2166)*y(k,804) + &
1.140*rxt(k,2609)*y(k,591) +1.092*rxt(k,2799)*y(k,628) +1.064*rxt(k,2929)*y(k,514) +.004*rxt(k,3010)*y(k,554) + &
.984*rxt(k,3230)*y(k,363) +.984*rxt(k,3243)*y(k,569) +.020*rxt(k,3437)*y(k,356) +.155*rxt(k,3451)*y(k,436) + &
.150*rxt(k,3472)*y(k,587) +.090*rxt(k,3543)*y(k,529) +.376*rxt(k,3752)*y(k,588) +.435*rxt(k,3787)*y(k,652) + &
.420*rxt(k,3800)*y(k,651) +2.000*rxt(k,3812)*y(k,608) +2.000*rxt(k,3831)*y(k,481) +2.000*rxt(k,3839)*y(k,503) + &
2.000*rxt(k,3848)*y(k,522) +2.000*rxt(k,3891)*y(k,829) +2.000*rxt(k,3907)*y(k,50) +2.883*rxt(k,3993)*y(k,660) + &
2.883*rxt(k,4005)*y(k,480) +2.883*rxt(k,4016)*y(k,159) +2.883*rxt(k,4034)*y(k,520) +2.067*rxt(k,4076)*y(k,810) + &
2.706*rxt(k,4098)*y(k,521) +2.382*rxt(k,4154)*y(k,574))*y(k,696) + (2.000*rxt(k,427)*y(k,690) + &
.880*rxt(k,429)*y(k,548) +2.000*rxt(k,433)*y(k,633) +2.000*rxt(k,435)*y(k,768) +2.000*rxt(k,437)*y(k,769) + &
2.000*rxt(k,439)*y(k,620) +4.000*rxt(k,441)*y(k,758) +2.000*rxt(k,455)*y(k,370) +2.000*rxt(k,471)*y(k,589) + &
2.000*rxt(k,3959)*y(k,427))*y(k,758) + (1.422*rxt(k,3927)*y(k,779) +1.422*rxt(k,3938)*y(k,379) + &
.720*rxt(k,4050)*y(k,439))*y(k,437) + (.870*rxt(k,1069)*y(k,512) +.468*rxt(k,1126)*y(k,33))*y(k,839) &
 +2.960*rxt(k,1)*y(k,59) +2.793*rxt(k,4)*y(k,60) +2.622*rxt(k,7)*y(k,62) +2.422*rxt(k,10)*y(k,64) &
 +2.244*rxt(k,13)*y(k,66) +1.960*rxt(k,20)*y(k,186) +2.880*rxt(k,22)*y(k,192) +1.526*rxt(k,25)*y(k,200) &
 +2.605*rxt(k,27)*y(k,266) +2.841*rxt(k,30)*y(k,270) +1.762*rxt(k,33)*y(k,332) +1.748*rxt(k,44)*y(k,412) &
 +1.628*rxt(k,46)*y(k,414) +2.000*rxt(k,59)*y(k,464) +1.920*rxt(k,61)*y(k,476) +1.884*rxt(k,79)*y(k,557) &
 +2.000*rxt(k,90)*y(k,625) +2.000*rxt(k,93)*y(k,634) +2.841*rxt(k,96)*y(k,646) +1.920*rxt(k,99)*y(k,649) &
 +.351*rxt(k,103)*y(k,653) +1.862*rxt(k,105)*y(k,655) +1.960*rxt(k,107)*y(k,659) +.800*rxt(k,123)*y(k,708) &
 +2.739*rxt(k,128)*y(k,731) +2.000*rxt(k,129)*y(k,732) +2.847*rxt(k,131)*y(k,733) +1.860*rxt(k,134)*y(k,734) &
 +2.622*rxt(k,136)*y(k,735) +1.628*rxt(k,139)*y(k,736) +2.000*rxt(k,142)*y(k,737) +3.128*rxt(k,143)*y(k,755) &
 +2.000*rxt(k,150)*y(k,757) +2.770*rxt(k,156)*y(k,761) +.788*rxt(k,157)*y(k,762) +3.616*rxt(k,160)*y(k,764) &
 +1.449*rxt(k,164)*y(k,765) +3.140*rxt(k,167)*y(k,766) +1.520*rxt(k,171)*y(k,767) +rxt(k,480)*y(k,841)
         loss(k,817) = (rxt(k,4332)* y(k,370) +rxt(k,4328)* y(k,548) +rxt(k,4333)* y(k,589) +rxt(k,4330)* y(k,620) +rxt(k,4334) &
* y(k,633) +rxt(k,4327)* y(k,690) +rxt(k,4329)* y(k,693) +rxt(k,4331)* y(k,758) +rxt(k,4335)* y(k,768) &
 +rxt(k,4336)* y(k,769))* y(k,855)
         prod(k,817) = (.924*rxt(k,535)*y(k,350) +1.071*rxt(k,585)*y(k,586) +2.000*rxt(k,1080)*y(k,351) + &
.015*rxt(k,1084)*y(k,52) +1.082*rxt(k,1116)*y(k,19) +1.413*rxt(k,1118)*y(k,33) +.036*rxt(k,1132)*y(k,84) + &
.036*rxt(k,1144)*y(k,263) +.033*rxt(k,1155)*y(k,176) +.036*rxt(k,1176)*y(k,389) +.039*rxt(k,1187)*y(k,788) + &
2.778*rxt(k,1208)*y(k,780) +.042*rxt(k,1220)*y(k,49) +2.724*rxt(k,1230)*y(k,28) +1.870*rxt(k,1233)*y(k,262) + &
.032*rxt(k,1236)*y(k,72) +.008*rxt(k,1248)*y(k,213) +.108*rxt(k,1258)*y(k,264) +.024*rxt(k,1271)*y(k,312) + &
.084*rxt(k,1283)*y(k,120) +.054*rxt(k,1293)*y(k,168) +.024*rxt(k,1304)*y(k,177) +.006*rxt(k,1322)*y(k,181) + &
.060*rxt(k,1333)*y(k,386) +.069*rxt(k,1344)*y(k,391) +.006*rxt(k,1354)*y(k,395) +.006*rxt(k,1366)*y(k,792) + &
.072*rxt(k,1378)*y(k,798) +.060*rxt(k,1390)*y(k,785) +.072*rxt(k,1402)*y(k,789) +.032*rxt(k,1413)*y(k,285) + &
.003*rxt(k,1426)*y(k,81) +.102*rxt(k,1437)*y(k,456) +.312*rxt(k,1448)*y(k,781) +.072*rxt(k,1462)*y(k,82) + &
.200*rxt(k,1474)*y(k,317) +.052*rxt(k,1486)*y(k,71) +.100*rxt(k,1497)*y(k,215) +.116*rxt(k,1510)*y(k,265) + &
.009*rxt(k,1522)*y(k,136) +.126*rxt(k,1535)*y(k,113) +.016*rxt(k,1547)*y(k,208) +.030*rxt(k,1559)*y(k,178) + &
.012*rxt(k,1592)*y(k,141) +.028*rxt(k,1605)*y(k,182) +.027*rxt(k,1619)*y(k,257) +.039*rxt(k,1630)*y(k,290) + &
.060*rxt(k,1643)*y(k,387) +.102*rxt(k,1655)*y(k,202) +.042*rxt(k,1666)*y(k,269) +.051*rxt(k,1678)*y(k,216) + &
.099*rxt(k,1700)*y(k,392) +.063*rxt(k,1723)*y(k,786) +.105*rxt(k,1734)*y(k,790) +.039*rxt(k,1746)*y(k,396) + &
.120*rxt(k,1757)*y(k,799) +.039*rxt(k,1771)*y(k,793) +.081*rxt(k,1782)*y(k,26) +.036*rxt(k,1792)*y(k,83) + &
.015*rxt(k,1802)*y(k,127) +.144*rxt(k,1814)*y(k,267) +.066*rxt(k,1826)*y(k,787) +.036*rxt(k,1838)*y(k,183) + &
.108*rxt(k,1852)*y(k,400) +.033*rxt(k,1862)*y(k,782) +.156*rxt(k,1874)*y(k,783) +.105*rxt(k,1885)*y(k,791) + &
.117*rxt(k,1895)*y(k,796) +.060*rxt(k,1917)*y(k,124) +.030*rxt(k,1929)*y(k,67) +.056*rxt(k,1939)*y(k,291) + &
.108*rxt(k,1952)*y(k,797) +.021*rxt(k,1963)*y(k,53) +.105*rxt(k,1973)*y(k,219) +.081*rxt(k,1985)*y(k,404) + &
.111*rxt(k,1995)*y(k,795) +.048*rxt(k,2047)*y(k,479) +.021*rxt(k,2085)*y(k,55) +.093*rxt(k,2095)*y(k,800) + &
.018*rxt(k,2105)*y(k,56) +.096*rxt(k,2115)*y(k,801) +.018*rxt(k,2125)*y(k,57) +.090*rxt(k,2135)*y(k,802) + &
.018*rxt(k,2145)*y(k,58) +.081*rxt(k,2155)*y(k,803) +.078*rxt(k,2165)*y(k,804) +.075*rxt(k,2928)*y(k,514) + &
.248*rxt(k,3228)*y(k,363) +.240*rxt(k,3241)*y(k,569) +.064*rxt(k,3435)*y(k,356) +.045*rxt(k,3449)*y(k,436) + &
.065*rxt(k,3541)*y(k,529) +.084*rxt(k,3751)*y(k,588) +.759*rxt(k,3786)*y(k,652) +.177*rxt(k,3799)*y(k,651) + &
.018*rxt(k,3913)*y(k,250) +.036*rxt(k,4042)*y(k,439))*y(k,705) + (2.808*rxt(k,593)*y(k,586) + &
.360*rxt(k,1124)*y(k,33) +1.293*rxt(k,1215)*y(k,780) +.052*rxt(k,1457)*y(k,781) +.112*rxt(k,2056)*y(k,479) + &
.184*rxt(k,2080)*y(k,881) +.030*rxt(k,2614)*y(k,591) +.030*rxt(k,2804)*y(k,628) +.066*rxt(k,2934)*y(k,514) + &
1.340*rxt(k,3236)*y(k,363) +1.940*rxt(k,3249)*y(k,569) +.090*rxt(k,3552)*y(k,529) +1.376*rxt(k,3794)*y(k,652) + &
.260*rxt(k,3807)*y(k,651))*y(k,693) + (.065*rxt(k,1454)*y(k,781) +.035*rxt(k,2052)*y(k,479) + &
.005*rxt(k,2076)*y(k,881) +.006*rxt(k,3547)*y(k,529) +.305*rxt(k,3790)*y(k,652) +.060*rxt(k,3803)*y(k,651)) &
*y(k,696)
         loss(k,716) = (rxt(k,4342)* y(k,370) +rxt(k,4338)* y(k,548) +rxt(k,4343)* y(k,589) +rxt(k,4340)* y(k,620) +rxt(k,4344) &
* y(k,633) +rxt(k,4337)* y(k,690) +rxt(k,4339)* y(k,693) +rxt(k,4341)* y(k,758) +rxt(k,4345)* y(k,768) &
 +rxt(k,4346)* y(k,769))* y(k,856)
         prod(k,716) = (.027*rxt(k,1197)*y(k,457) +.036*rxt(k,1411)*y(k,285) +.012*rxt(k,1471)*y(k,317))*y(k,705) &
 + (.960*rxt(k,590)*y(k,696) +.720*rxt(k,596)*y(k,839))*y(k,586) +.472*rxt(k,1126)*y(k,839)*y(k,33)
         loss(k,757) = (rxt(k,4352)* y(k,370) +rxt(k,4348)* y(k,548) +rxt(k,4353)* y(k,589) +rxt(k,4350)* y(k,620) +rxt(k,4354) &
* y(k,633) +rxt(k,4347)* y(k,690) +rxt(k,4349)* y(k,693) +rxt(k,4351)* y(k,758) +rxt(k,4355)* y(k,768) &
 +rxt(k,4356)* y(k,769))* y(k,857)
         prod(k,757) = (.690*rxt(k,585)*y(k,586) +.021*rxt(k,1083)*y(k,52) +1.440*rxt(k,1118)*y(k,33) +.188*rxt(k,1131)*y(k,84) + &
.084*rxt(k,1144)*y(k,263) +.054*rxt(k,1199)*y(k,457) +1.728*rxt(k,1219)*y(k,49) +.192*rxt(k,1235)*y(k,72) + &
.020*rxt(k,1247)*y(k,213) +.028*rxt(k,1258)*y(k,264) +.332*rxt(k,1271)*y(k,312) +.069*rxt(k,1303)*y(k,177) + &
.021*rxt(k,1322)*y(k,181) +.052*rxt(k,1413)*y(k,285) +.009*rxt(k,1425)*y(k,81) +.003*rxt(k,1436)*y(k,456) + &
.076*rxt(k,1448)*y(k,781) +.148*rxt(k,1485)*y(k,71) +.052*rxt(k,1497)*y(k,215) +.064*rxt(k,1510)*y(k,265) + &
.132*rxt(k,1522)*y(k,136) +.036*rxt(k,1546)*y(k,208) +.072*rxt(k,1558)*y(k,178) +.039*rxt(k,1591)*y(k,141) + &
.032*rxt(k,1605)*y(k,182) +.129*rxt(k,1791)*y(k,83) +.033*rxt(k,1802)*y(k,127) +.028*rxt(k,1838)*y(k,183) + &
.123*rxt(k,1928)*y(k,67) +.228*rxt(k,1939)*y(k,291) +.114*rxt(k,1962)*y(k,53) +.096*rxt(k,2084)*y(k,55) + &
.090*rxt(k,2104)*y(k,56) +.087*rxt(k,2124)*y(k,57) +.081*rxt(k,2144)*y(k,58) +.032*rxt(k,3348)*y(k,68) + &
.025*rxt(k,3448)*y(k,436) +.015*rxt(k,3540)*y(k,529) +.015*rxt(k,3913)*y(k,250))*y(k,705) &
 + (2.652*rxt(k,1226)*y(k,49) +1.632*rxt(k,1456)*y(k,781) +.156*rxt(k,2056)*y(k,479) +.030*rxt(k,3551)*y(k,529)) &
*y(k,693) + (.252*rxt(k,1223)*y(k,49) +.155*rxt(k,1453)*y(k,781) +.075*rxt(k,2052)*y(k,479) + &
.084*rxt(k,3547)*y(k,529))*y(k,696) +.460*rxt(k,1127)*y(k,839)*y(k,33)
         loss(k,765) = (rxt(k,4362)* y(k,370) +rxt(k,4358)* y(k,548) +rxt(k,4363)* y(k,589) +rxt(k,4360)* y(k,620) +rxt(k,4364) &
* y(k,633) +rxt(k,4357)* y(k,690) +rxt(k,4359)* y(k,693) +rxt(k,4361)* y(k,758) +rxt(k,4365)* y(k,768) &
 +rxt(k,4366)* y(k,769))* y(k,858)
         prod(k,765) = (rxt(k,302)*y(k,341) +1.530*rxt(k,306)*y(k,625) +.804*rxt(k,519)*y(k,348) +.804*rxt(k,526)*y(k,349) + &
.468*rxt(k,533)*y(k,350) +2.025*rxt(k,552)*y(k,664) +.145*rxt(k,572)*y(k,731) +.717*rxt(k,619)*y(k,621) + &
.212*rxt(k,645)*y(k,11) +1.408*rxt(k,649)*y(k,24) +1.356*rxt(k,653)*y(k,37) +.936*rxt(k,707)*y(k,5) + &
.812*rxt(k,711)*y(k,7) +1.208*rxt(k,719)*y(k,15) +1.488*rxt(k,723)*y(k,17) +.336*rxt(k,727)*y(k,76) + &
.012*rxt(k,737)*y(k,38) +.104*rxt(k,812)*y(k,13) +.024*rxt(k,816)*y(k,30) +.008*rxt(k,820)*y(k,6) + &
.003*rxt(k,824)*y(k,70) +.012*rxt(k,924)*y(k,41) +.003*rxt(k,955)*y(k,78) +.012*rxt(k,1411)*y(k,285) + &
.004*rxt(k,1471)*y(k,317) +.872*rxt(k,2520)*y(k,741) +2.154*rxt(k,2541)*y(k,508) +2.412*rxt(k,2649)*y(k,500) + &
.072*rxt(k,2659)*y(k,583) +2.394*rxt(k,2669)*y(k,634) +1.437*rxt(k,2698)*y(k,710) +2.283*rxt(k,2754)*y(k,659) + &
2.355*rxt(k,2760)*y(k,649) +1.227*rxt(k,2788)*y(k,501) +2.013*rxt(k,2811)*y(k,135) +2.872*rxt(k,2818)*y(k,526) + &
.340*rxt(k,2822)*y(k,579) +1.665*rxt(k,2868)*y(k,513) +.076*rxt(k,2873)*y(k,648) +3.512*rxt(k,2895)*y(k,646) + &
1.836*rxt(k,2899)*y(k,655) +2.316*rxt(k,2903)*y(k,663) +.264*rxt(k,2926)*y(k,514) +1.840*rxt(k,2937)*y(k,505) + &
.680*rxt(k,2957)*y(k,772) +2.472*rxt(k,2964)*y(k,474) +2.152*rxt(k,3019)*y(k,724) +.849*rxt(k,3071)*y(k,412) + &
.381*rxt(k,3080)*y(k,332) +2.640*rxt(k,3083)*y(k,266) +.628*rxt(k,3154)*y(k,492) +.072*rxt(k,3158)*y(k,478) + &
.956*rxt(k,3174)*y(k,27) +.540*rxt(k,3193)*y(k,414) +.760*rxt(k,3280)*y(k,593) +.003*rxt(k,3350)*y(k,830) + &
.420*rxt(k,3353)*y(k,416) +.712*rxt(k,3408)*y(k,486) +.672*rxt(k,3412)*y(k,487) +.249*rxt(k,3481)*y(k,375) + &
.032*rxt(k,3684)*y(k,477) +2.176*rxt(k,3692)*y(k,812) +.129*rxt(k,3759)*y(k,614) +.252*rxt(k,4177)*y(k,732) + &
.248*rxt(k,4180)*y(k,733))*y(k,705) + (2.001*rxt(k,1095)*y(k,585) +2.232*rxt(k,1157)*y(k,176) + &
2.800*rxt(k,1167)*y(k,180) +2.328*rxt(k,1284)*y(k,120) +2.224*rxt(k,1305)*y(k,177) +2.000*rxt(k,1315)*y(k,122) + &
2.800*rxt(k,1323)*y(k,181) +2.930*rxt(k,1356)*y(k,395) +2.930*rxt(k,1368)*y(k,792) +2.256*rxt(k,1427)*y(k,81) + &
2.284*rxt(k,1463)*y(k,82) +2.148*rxt(k,1524)*y(k,136) +2.272*rxt(k,1536)*y(k,113) +2.148*rxt(k,1560)*y(k,178) + &
3.500*rxt(k,1593)*y(k,141) +3.500*rxt(k,1607)*y(k,182) +2.930*rxt(k,1631)*y(k,290) +2.930*rxt(k,1667)*y(k,269) + &
3.055*rxt(k,1680)*y(k,216) +2.754*rxt(k,1691)*y(k,123) +2.920*rxt(k,1747)*y(k,396) +2.920*rxt(k,1772)*y(k,793) + &
2.214*rxt(k,1784)*y(k,26) +2.060*rxt(k,1803)*y(k,127) +3.500*rxt(k,1840)*y(k,183) +2.800*rxt(k,1907)*y(k,128) + &
2.751*rxt(k,1919)*y(k,124) +2.008*rxt(k,2006)*y(k,249) +.006*rxt(k,2019)*y(k,355) +2.410*rxt(k,2049)*y(k,479) + &
2.505*rxt(k,2073)*y(k,881) +2.505*rxt(k,3437)*y(k,356) +3.500*rxt(k,3451)*y(k,436) +3.400*rxt(k,3472)*y(k,587) + &
3.648*rxt(k,3543)*y(k,529))*y(k,696) + (1.539*rxt(k,2793)*y(k,501) +2.070*rxt(k,2933)*y(k,514))*y(k,693) &
 +2.000*rxt(k,18)*y(k,135) +rxt(k,87)*y(k,590)
         loss(k,756) = (rxt(k,4372)* y(k,370) +rxt(k,4368)* y(k,548) +rxt(k,4373)* y(k,589) +rxt(k,4370)* y(k,620) +rxt(k,4374) &
* y(k,633) +rxt(k,4367)* y(k,690) +rxt(k,4369)* y(k,693) +rxt(k,4371)* y(k,758) +rxt(k,4375)* y(k,768) &
 +rxt(k,4376)* y(k,769))* y(k,859)
         prod(k,756) = (1.455*rxt(k,248)*y(k,678) +.714*rxt(k,256)*y(k,680) +.060*rxt(k,262)*y(k,452) +.700*rxt(k,287)*y(k,88) + &
2.520*rxt(k,314)*y(k,517) +2.576*rxt(k,320)*y(k,504) +1.320*rxt(k,324)*y(k,661) +.150*rxt(k,505)*y(k,756) + &
.516*rxt(k,520)*y(k,348) +.516*rxt(k,527)*y(k,349) +1.248*rxt(k,543)*y(k,590) +.600*rxt(k,562)*y(k,582) + &
.575*rxt(k,575)*y(k,731) +1.020*rxt(k,580)*y(k,761) +.114*rxt(k,605)*y(k,193) +.036*rxt(k,608)*y(k,95) + &
.018*rxt(k,612)*y(k,194) +1.887*rxt(k,614)*y(k,280) +.072*rxt(k,624)*y(k,96) +1.548*rxt(k,628)*y(k,115) + &
.054*rxt(k,631)*y(k,137) +.384*rxt(k,637)*y(k,209) +.768*rxt(k,640)*y(k,281) +1.701*rxt(k,643)*y(k,258) + &
.044*rxt(k,647)*y(k,11) +.012*rxt(k,659)*y(k,506) +.052*rxt(k,666)*y(k,97) +.885*rxt(k,670)*y(k,104) + &
.894*rxt(k,673)*y(k,116) +.279*rxt(k,676)*y(k,138) +.186*rxt(k,684)*y(k,282) +.345*rxt(k,687)*y(k,319) + &
.236*rxt(k,691)*y(k,102) +.068*rxt(k,695)*y(k,210) +2.012*rxt(k,699)*y(k,87) +2.622*rxt(k,702)*y(k,217) + &
1.332*rxt(k,706)*y(k,251) +.008*rxt(k,713)*y(k,7) +.008*rxt(k,739)*y(k,38) +.008*rxt(k,746)*y(k,728) + &
.016*rxt(k,750)*y(k,91) +.036*rxt(k,754)*y(k,109) +.102*rxt(k,757)*y(k,139) +1.821*rxt(k,762)*y(k,207) + &
.174*rxt(k,765)*y(k,225) +.018*rxt(k,771)*y(k,320) +.536*rxt(k,775)*y(k,129) +.032*rxt(k,779)*y(k,211) + &
.006*rxt(k,783)*y(k,292) +.920*rxt(k,789)*y(k,89) +.006*rxt(k,796)*y(k,117) +.375*rxt(k,799)*y(k,147) + &
.057*rxt(k,801)*y(k,283) +1.725*rxt(k,804)*y(k,218) +.135*rxt(k,807)*y(k,259) +.016*rxt(k,818)*y(k,30) + &
.020*rxt(k,822)*y(k,6) +.033*rxt(k,832)*y(k,134) +.021*rxt(k,835)*y(k,140) +.213*rxt(k,837)*y(k,154) + &
2.058*rxt(k,843)*y(k,214) +.042*rxt(k,846)*y(k,284) +.012*rxt(k,849)*y(k,321) +.270*rxt(k,856)*y(k,130) + &
.028*rxt(k,859)*y(k,145) +.032*rxt(k,863)*y(k,212) +.560*rxt(k,873)*y(k,90) +.840*rxt(k,877)*y(k,92) + &
.006*rxt(k,881)*y(k,111) +.003*rxt(k,884)*y(k,118) +.078*rxt(k,887)*y(k,148) +.006*rxt(k,890)*y(k,185) + &
.003*rxt(k,896)*y(k,36) +.453*rxt(k,911)*y(k,103) +.018*rxt(k,913)*y(k,155) +.096*rxt(k,916)*y(k,224) + &
.036*rxt(k,919)*y(k,274) +.009*rxt(k,922)*y(k,318) +.008*rxt(k,926)*y(k,41) +.012*rxt(k,938)*y(k,107) + &
.324*rxt(k,940)*y(k,229) +.165*rxt(k,943)*y(k,230) +.033*rxt(k,946)*y(k,275) +.008*rxt(k,953)*y(k,29) + &
.384*rxt(k,964)*y(k,110) +.003*rxt(k,967)*y(k,133) +.138*rxt(k,969)*y(k,231) +.315*rxt(k,972)*y(k,234) + &
.027*rxt(k,975)*y(k,276) +.004*rxt(k,982)*y(k,42) +.060*rxt(k,993)*y(k,131) +.006*rxt(k,996)*y(k,184) + &
.090*rxt(k,998)*y(k,235) +.051*rxt(k,1001)*y(k,238) +.027*rxt(k,1004)*y(k,277) +.003*rxt(k,1021)*y(k,152) + &
.084*rxt(k,1023)*y(k,236) +.048*rxt(k,1026)*y(k,239) +.024*rxt(k,1029)*y(k,278) +.004*rxt(k,1044)*y(k,40) + &
.003*rxt(k,1050)*y(k,158) +.024*rxt(k,1052)*y(k,279) +.009*rxt(k,1055)*y(k,301) +2.760*rxt(k,1155)*y(k,176) + &
.160*rxt(k,1257)*y(k,264) +2.619*rxt(k,1282)*y(k,120) +2.622*rxt(k,1292)*y(k,168) +2.604*rxt(k,1303)*y(k,177) + &
2.679*rxt(k,1354)*y(k,395) +2.679*rxt(k,1366)*y(k,792) +.008*rxt(k,1497)*y(k,215) +.016*rxt(k,1509)*y(k,265) + &
.549*rxt(k,1534)*y(k,113) +.088*rxt(k,1546)*y(k,208) +2.595*rxt(k,1569)*y(k,101) +2.541*rxt(k,1618)*y(k,257) + &
2.541*rxt(k,1629)*y(k,290) +2.541*rxt(k,1665)*y(k,269) +2.538*rxt(k,1678)*y(k,216) +1.730*rxt(k,1690)*y(k,123) + &
2.529*rxt(k,1745)*y(k,396) +2.529*rxt(k,1770)*y(k,793) +2.403*rxt(k,1801)*y(k,127) +3.236*rxt(k,1917)*y(k,124) + &
.212*rxt(k,2046)*y(k,479) +.654*rxt(k,2539)*y(k,39) +.032*rxt(k,2583)*y(k,18) +.261*rxt(k,2604)*y(k,528) + &
.033*rxt(k,2607)*y(k,591) +2.451*rxt(k,2647)*y(k,48) +.108*rxt(k,2654)*y(k,641) +.099*rxt(k,2658)*y(k,394) + &
.444*rxt(k,2670)*y(k,634) +1.748*rxt(k,2690)*y(k,518) +.096*rxt(k,2703)*y(k,626) +.360*rxt(k,2756)*y(k,659) + &
1.280*rxt(k,2779)*y(k,190) +2.820*rxt(k,2783)*y(k,523) +.204*rxt(k,2786)*y(k,654) +2.008*rxt(k,2816)*y(k,825) + &
.144*rxt(k,2820)*y(k,526) +2.088*rxt(k,2828)*y(k,610) +.312*rxt(k,2832)*y(k,624) +.804*rxt(k,2836)*y(k,403) + &
1.392*rxt(k,2840)*y(k,726) +2.388*rxt(k,2847)*y(k,100) +1.396*rxt(k,2851)*y(k,516) +1.104*rxt(k,2855)*y(k,206) + &
1.572*rxt(k,2858)*y(k,260) +.008*rxt(k,2862)*y(k,287) +.308*rxt(k,2866)*y(k,603) +.085*rxt(k,2870)*y(k,513) + &
.080*rxt(k,2875)*y(k,648) +.076*rxt(k,2879)*y(k,657) +.016*rxt(k,2897)*y(k,646) +.580*rxt(k,2901)*y(k,655) + &
2.385*rxt(k,2917)*y(k,740) +2.163*rxt(k,2921)*y(k,498) +3.440*rxt(k,2924)*y(k,584) +.009*rxt(k,2927)*y(k,514) + &
1.124*rxt(k,2939)*y(k,505) +.216*rxt(k,2943)*y(k,566) +.456*rxt(k,2947)*y(k,642) +1.012*rxt(k,2951)*y(k,361) + &
1.204*rxt(k,2955)*y(k,743) +.044*rxt(k,2959)*y(k,772) +.828*rxt(k,2966)*y(k,474) +1.528*rxt(k,2969)*y(k,615) + &
4.460*rxt(k,2981)*y(k,493) +2.466*rxt(k,2984)*y(k,342) +.906*rxt(k,2987)*y(k,742) +2.032*rxt(k,2991)*y(k,366) + &
1.344*rxt(k,3004)*y(k,730) +.196*rxt(k,3021)*y(k,724) +2.980*rxt(k,3025)*y(k,449) +.172*rxt(k,3029)*y(k,204) + &
.252*rxt(k,3033)*y(k,640) +1.620*rxt(k,3037)*y(k,470) +1.148*rxt(k,3043)*y(k,459) +.174*rxt(k,3047)*y(k,509) + &
.732*rxt(k,3076)*y(k,186) +.752*rxt(k,3085)*y(k,266) +1.064*rxt(k,3092)*y(k,121) +2.900*rxt(k,3096)*y(k,294) + &
.896*rxt(k,3123)*y(k,367) +1.624*rxt(k,3130)*y(k,727) +.080*rxt(k,3134)*y(k,562) +2.700*rxt(k,3138)*y(k,175) + &
.552*rxt(k,3141)*y(k,616) +1.804*rxt(k,3146)*y(k,722) +1.233*rxt(k,3152)*y(k,365) +2.920*rxt(k,3156)*y(k,492) + &
2.312*rxt(k,3160)*y(k,478) +1.404*rxt(k,3164)*y(k,473) +.056*rxt(k,3168)*y(k,491) +.032*rxt(k,3172)*y(k,489) + &
.004*rxt(k,3176)*y(k,27) +1.401*rxt(k,3179)*y(k,460) +.764*rxt(k,3183)*y(k,823) +.004*rxt(k,3187)*y(k,162) + &
.020*rxt(k,3213)*y(k,73) +2.334*rxt(k,3216)*y(k,565) +1.953*rxt(k,3219)*y(k,364) +.012*rxt(k,3228)*y(k,363) + &
.236*rxt(k,3241)*y(k,569) +2.188*rxt(k,3254)*y(k,125) +.848*rxt(k,3258)*y(k,187) +1.240*rxt(k,3262)*y(k,271) + &
.712*rxt(k,3266)*y(k,313) +2.330*rxt(k,3270)*y(k,568) +1.252*rxt(k,3275)*y(k,362) +.064*rxt(k,3282)*y(k,593) + &
.564*rxt(k,3295)*y(k,167) +.044*rxt(k,3302)*y(k,832) +2.696*rxt(k,3310)*y(k,161) +.568*rxt(k,3314)*y(k,174) + &
1.148*rxt(k,3318)*y(k,468) +1.520*rxt(k,3322)*y(k,485) +.628*rxt(k,3325)*y(k,461) +3.380*rxt(k,3330)*y(k,471) + &
1.944*rxt(k,3334)*y(k,821) +.544*rxt(k,3338)*y(k,813) +.856*rxt(k,3368)*y(k,142) +.428*rxt(k,3372)*y(k,188) + &
.928*rxt(k,3376)*y(k,254) +.448*rxt(k,3380)*y(k,272) +.788*rxt(k,3384)*y(k,314) +.088*rxt(k,3392)*y(k,576) + &
1.112*rxt(k,3402)*y(k,51) +1.436*rxt(k,3406)*y(k,488) +.316*rxt(k,3410)*y(k,486) +.560*rxt(k,3414)*y(k,487) + &
1.712*rxt(k,3418)*y(k,824) +.936*rxt(k,3425)*y(k,815) +1.395*rxt(k,3428)*y(k,534) +.644*rxt(k,3503)*y(k,143) + &
.300*rxt(k,3507)*y(k,173) +1.064*rxt(k,3511)*y(k,220) +.036*rxt(k,3515)*y(k,226) +.408*rxt(k,3525)*y(k,295) + &
.456*rxt(k,3529)*y(k,315) +.048*rxt(k,3533)*y(k,334) +1.312*rxt(k,3566)*y(k,462) +2.592*rxt(k,3570)*y(k,469) + &
1.576*rxt(k,3574)*y(k,820) +.392*rxt(k,3578)*y(k,835) +1.464*rxt(k,3592)*y(k,108) +1.092*rxt(k,3596)*y(k,126) + &
.140*rxt(k,3600)*y(k,144) +.864*rxt(k,3604)*y(k,149) +.164*rxt(k,3608)*y(k,189) +.028*rxt(k,3612)*y(k,227) + &
.896*rxt(k,3623)*y(k,296) +.028*rxt(k,3627)*y(k,297) +.012*rxt(k,3634)*y(k,335) +.284*rxt(k,3651)*y(k,233) + &
.032*rxt(k,3658)*y(k,298) +1.252*rxt(k,3665)*y(k,818) +.615*rxt(k,3670)*y(k,817) +.012*rxt(k,3674)*y(k,467) + &
1.472*rxt(k,3690)*y(k,814) +.108*rxt(k,3694)*y(k,812) +.004*rxt(k,3698)*y(k,222) +.288*rxt(k,3702)*y(k,253) + &
.172*rxt(k,3706)*y(k,300) +1.308*rxt(k,3713)*y(k,822) +1.068*rxt(k,3717)*y(k,106) +.108*rxt(k,3721)*y(k,223) + &
.004*rxt(k,3725)*y(k,228) +.276*rxt(k,3737)*y(k,132) +.576*rxt(k,3740)*y(k,252) +.004*rxt(k,3744)*y(k,299) + &
.904*rxt(k,3764)*y(k,105) +.032*rxt(k,3772)*y(k,331) +4.210*rxt(k,3781)*y(k,836) +2.000*rxt(k,3870)*y(k,10) + &
1.500*rxt(k,3878)*y(k,441) +2.000*rxt(k,3884)*y(k,442) +.642*rxt(k,4030)*y(k,401) +.642*rxt(k,4055)*y(k,399) + &
2.308*rxt(k,4182)*y(k,733) +2.176*rxt(k,4203)*y(k,764) +1.912*rxt(k,4211)*y(k,766))*y(k,705) &
 + (.057*rxt(k,1162)*y(k,176) +1.044*rxt(k,1266)*y(k,264) +.027*rxt(k,1299)*y(k,168) + &
2.616*rxt(k,1362)*y(k,395) +2.616*rxt(k,1374)*y(k,792) +.039*rxt(k,1505)*y(k,215) +.030*rxt(k,1518)*y(k,265) + &
1.401*rxt(k,1541)*y(k,113) +.099*rxt(k,1554)*y(k,208) +2.427*rxt(k,1625)*y(k,257) +2.772*rxt(k,1638)*y(k,290) + &
2.772*rxt(k,1674)*y(k,269) +.330*rxt(k,1686)*y(k,216) +2.595*rxt(k,1695)*y(k,123) +1.992*rxt(k,1753)*y(k,396) + &
.656*rxt(k,1766)*y(k,799) +1.992*rxt(k,1778)*y(k,793) +3.288*rxt(k,1924)*y(k,124) +1.416*rxt(k,1979)*y(k,219)) &
*y(k,693) + (.175*rxt(k,1263)*y(k,264) +.010*rxt(k,1502)*y(k,215) +.035*rxt(k,1515)*y(k,265) + &
2.272*rxt(k,1538)*y(k,113) +.015*rxt(k,1551)*y(k,208) +.175*rxt(k,1762)*y(k,799) +1.752*rxt(k,1976)*y(k,219)) &
*y(k,696) +.321*rxt(k,28)*y(k,266) +.415*rxt(k,152)*y(k,761) +1.980*rxt(k,162)*y(k,764)
         loss(k,743) = (rxt(k,4382)* y(k,370) +rxt(k,4378)* y(k,548) +rxt(k,4383)* y(k,589) +rxt(k,4380)* y(k,620) +rxt(k,4384) &
* y(k,633) +rxt(k,4377)* y(k,690) +rxt(k,4379)* y(k,693) +rxt(k,4381)* y(k,758) +rxt(k,4385)* y(k,768) &
 +rxt(k,4386)* y(k,769))* y(k,860)
         prod(k,743) = (2.433*rxt(k,312)*y(k,517) +.486*rxt(k,315)*y(k,662) +2.576*rxt(k,318)*y(k,504) + &
1.340*rxt(k,322)*y(k,661) +.072*rxt(k,603)*y(k,193) +.036*rxt(k,606)*y(k,95) +.100*rxt(k,635)*y(k,209) + &
.072*rxt(k,689)*y(k,102) +.040*rxt(k,693)*y(k,210) +.028*rxt(k,773)*y(k,129) +.032*rxt(k,777)*y(k,211) + &
.028*rxt(k,857)*y(k,145) +.028*rxt(k,861)*y(k,212) +.100*rxt(k,1142)*y(k,263) +.009*rxt(k,1207)*y(k,780) + &
.936*rxt(k,1232)*y(k,262) +.024*rxt(k,1245)*y(k,213) +.042*rxt(k,1281)*y(k,120) +.003*rxt(k,1320)*y(k,181) + &
.030*rxt(k,1377)*y(k,798) +.008*rxt(k,1446)*y(k,781) +.128*rxt(k,1544)*y(k,208) +.012*rxt(k,1590)*y(k,141) + &
.020*rxt(k,1603)*y(k,182) +.021*rxt(k,1653)*y(k,202) +.021*rxt(k,1677)*y(k,216) +.016*rxt(k,1812)*y(k,267) + &
.024*rxt(k,1836)*y(k,183) +.030*rxt(k,1872)*y(k,783) +.016*rxt(k,1915)*y(k,124) +.692*rxt(k,2688)*y(k,518) + &
.160*rxt(k,2720)*y(k,354) +.008*rxt(k,2741)*y(k,270) +2.748*rxt(k,2781)*y(k,523) +3.380*rxt(k,2822)*y(k,579) + &
.144*rxt(k,2842)*y(k,268) +.048*rxt(k,2846)*y(k,100) +.640*rxt(k,2849)*y(k,516) +.956*rxt(k,2856)*y(k,260) + &
.052*rxt(k,2860)*y(k,287) +2.332*rxt(k,2873)*y(k,648) +.885*rxt(k,2919)*y(k,498) +3.496*rxt(k,2922)*y(k,584) + &
.032*rxt(k,2941)*y(k,566) +2.181*rxt(k,2961)*y(k,805) +.832*rxt(k,2979)*y(k,493) +.258*rxt(k,2983)*y(k,342) + &
1.332*rxt(k,2993)*y(k,289) +.104*rxt(k,2998)*y(k,653) +1.152*rxt(k,3023)*y(k,449) +.248*rxt(k,3027)*y(k,204) + &
.676*rxt(k,3035)*y(k,470) +2.616*rxt(k,3094)*y(k,294) +.004*rxt(k,3109)*y(k,567) +.076*rxt(k,3144)*y(k,722) + &
.045*rxt(k,3148)*y(k,723) +1.112*rxt(k,3154)*y(k,492) +2.308*rxt(k,3158)*y(k,478) +.360*rxt(k,3166)*y(k,491) + &
.512*rxt(k,3170)*y(k,489) +.004*rxt(k,3239)*y(k,569) +.030*rxt(k,3268)*y(k,568) +1.120*rxt(k,3312)*y(k,174) + &
.760*rxt(k,3320)*y(k,485) +.316*rxt(k,3328)*y(k,471) +.208*rxt(k,3332)*y(k,821) +.056*rxt(k,3356)*y(k,475) + &
.300*rxt(k,3404)*y(k,488) +.424*rxt(k,3408)*y(k,486) +.728*rxt(k,3412)*y(k,487) +.183*rxt(k,3420)*y(k,834) + &
.030*rxt(k,3446)*y(k,436) +.025*rxt(k,3538)*y(k,529) +.296*rxt(k,3564)*y(k,462) +.680*rxt(k,3576)*y(k,835) + &
.020*rxt(k,3642)*y(k,564) +.236*rxt(k,3684)*y(k,477) +.520*rxt(k,3711)*y(k,822) +.030*rxt(k,3779)*y(k,836) + &
1.500*rxt(k,3867)*y(k,483) +1.074*rxt(k,3896)*y(k,580) +.084*rxt(k,3984)*y(k,806) +1.804*rxt(k,4111)*y(k,544) + &
1.804*rxt(k,4113)*y(k,543))*y(k,705) + (.948*rxt(k,1098)*y(k,585) +.099*rxt(k,1553)*y(k,208) + &
.004*rxt(k,2745)*y(k,270) +.070*rxt(k,3115)*y(k,567) +.036*rxt(k,3247)*y(k,569) +.040*rxt(k,3549)*y(k,529) + &
1.539*rxt(k,3900)*y(k,580))*y(k,693) + (.015*rxt(k,1548)*y(k,208) +2.000*rxt(k,3898)*y(k,580))*y(k,696) &
 +2.328*rxt(k,103)*y(k,653)
         loss(k,747) = (rxt(k,4392)* y(k,370) +rxt(k,4388)* y(k,548) +rxt(k,4393)* y(k,589) +rxt(k,4390)* y(k,620) +rxt(k,4394) &
* y(k,633) +rxt(k,4387)* y(k,690) +rxt(k,4389)* y(k,693) +rxt(k,4391)* y(k,758) +rxt(k,4395)* y(k,768) &
 +rxt(k,4396)* y(k,769))* y(k,861)
         prod(k,747) = (.982*rxt(k,491)*y(k,425) +1.260*rxt(k,505)*y(k,756) +.500*rxt(k,513)*y(k,446) +1.068*rxt(k,520)*y(k,348) + &
1.068*rxt(k,527)*y(k,349) +.612*rxt(k,534)*y(k,350) +.252*rxt(k,543)*y(k,590) +.900*rxt(k,552)*y(k,664) + &
.696*rxt(k,563)*y(k,582) +.004*rxt(k,647)*y(k,11) +.024*rxt(k,659)*y(k,506) +.004*rxt(k,709)*y(k,5) + &
.016*rxt(k,746)*y(k,728) +.006*rxt(k,811)*y(k,558) +.040*rxt(k,1044)*y(k,40) +.080*rxt(k,2017)*y(k,355) + &
.028*rxt(k,2032)*y(k,879) +.604*rxt(k,2174)*y(k,833) +.468*rxt(k,2178)*y(k,383) +1.520*rxt(k,2182)*y(k,665) + &
1.190*rxt(k,2186)*y(k,706) +.560*rxt(k,2191)*y(k,746) +.372*rxt(k,2204)*y(k,676) +.476*rxt(k,2208)*y(k,572) + &
1.730*rxt(k,2212)*y(k,644) +1.020*rxt(k,2217)*y(k,704) +.505*rxt(k,2222)*y(k,721) +.900*rxt(k,2227)*y(k,14) + &
2.025*rxt(k,2232)*y(k,16) +2.560*rxt(k,2237)*y(k,32) +.332*rxt(k,2249)*y(k,373) +.560*rxt(k,2253)*y(k,807) + &
1.630*rxt(k,2257)*y(k,596) +.940*rxt(k,2262)*y(k,697) +.465*rxt(k,2267)*y(k,711) +.505*rxt(k,2272)*y(k,719) + &
.835*rxt(k,2277)*y(k,240) +1.860*rxt(k,2282)*y(k,302) +2.424*rxt(k,2287)*y(k,324) +.288*rxt(k,2299)*y(k,376) + &
1.495*rxt(k,2303)*y(k,597) +.830*rxt(k,2308)*y(k,698) +.410*rxt(k,2313)*y(k,712) +.790*rxt(k,2318)*y(k,241) + &
1.760*rxt(k,2323)*y(k,303) +2.336*rxt(k,2328)*y(k,325) +.400*rxt(k,2333)*y(k,630) +.252*rxt(k,2336)*y(k,377) + &
1.440*rxt(k,2340)*y(k,598) +.790*rxt(k,2345)*y(k,699) +.390*rxt(k,2350)*y(k,713) +.765*rxt(k,2355)*y(k,242) + &
1.705*rxt(k,2360)*y(k,304) +2.288*rxt(k,2365)*y(k,326) +.800*rxt(k,2370)*y(k,119) +.224*rxt(k,2373)*y(k,378) + &
1.088*rxt(k,2377)*y(k,599) +.725*rxt(k,2381)*y(k,700) +.360*rxt(k,2386)*y(k,714) +.735*rxt(k,2391)*y(k,243) + &
1.630*rxt(k,2396)*y(k,305) +2.224*rxt(k,2401)*y(k,327) +.204*rxt(k,2405)*y(k,380) +1.036*rxt(k,2409)*y(k,600) + &
.675*rxt(k,2413)*y(k,701) +.335*rxt(k,2418)*y(k,715) +.710*rxt(k,2423)*y(k,244) +1.585*rxt(k,2428)*y(k,306) + &
2.184*rxt(k,2433)*y(k,328) +.184*rxt(k,2437)*y(k,381) +.988*rxt(k,2441)*y(k,601) +.635*rxt(k,2445)*y(k,702) + &
.315*rxt(k,2450)*y(k,716) +.690*rxt(k,2455)*y(k,245) +1.535*rxt(k,2460)*y(k,307) +2.140*rxt(k,2465)*y(k,329) + &
.168*rxt(k,2469)*y(k,382) +.944*rxt(k,2473)*y(k,602) +.595*rxt(k,2477)*y(k,703) +.295*rxt(k,2482)*y(k,717) + &
.670*rxt(k,2487)*y(k,246) +1.485*rxt(k,2492)*y(k,308) +2.096*rxt(k,2497)*y(k,330) +1.644*rxt(k,2531)*y(k,346) + &
.066*rxt(k,2605)*y(k,528) +2.724*rxt(k,2619)*y(k,606) +.453*rxt(k,2637)*y(k,552) +.054*rxt(k,2651)*y(k,500) + &
.372*rxt(k,2668)*y(k,368) +.244*rxt(k,2703)*y(k,626) +.160*rxt(k,2710)*y(k,592) +.008*rxt(k,2751)*y(k,60) + &
1.185*rxt(k,2789)*y(k,501) +2.013*rxt(k,2812)*y(k,135) +.044*rxt(k,2824)*y(k,579) +.312*rxt(k,2828)*y(k,610) + &
.008*rxt(k,2841)*y(k,726) +3.124*rxt(k,2844)*y(k,268) +.195*rxt(k,2870)*y(k,513) +.054*rxt(k,2893)*y(k,62) + &
.152*rxt(k,2939)*y(k,505) +.195*rxt(k,2963)*y(k,805) +.104*rxt(k,2966)*y(k,474) +.140*rxt(k,2970)*y(k,615) + &
.620*rxt(k,3008)*y(k,554) +.009*rxt(k,3048)*y(k,509) +.093*rxt(k,3050)*y(k,347) +.472*rxt(k,3056)*y(k,369) + &
.588*rxt(k,3060)*y(k,372) +.051*rxt(k,3065)*y(k,64) +1.272*rxt(k,3100)*y(k,668) +1.264*rxt(k,3111)*y(k,567) + &
.016*rxt(k,3130)*y(k,727) +.020*rxt(k,3142)*y(k,616) +1.260*rxt(k,3156)*y(k,492) +.045*rxt(k,3191)*y(k,66) + &
.425*rxt(k,3223)*y(k,496) +.012*rxt(k,3275)*y(k,362) +1.008*rxt(k,3305)*y(k,744) +.080*rxt(k,3326)*y(k,461) + &
.003*rxt(k,3429)*y(k,534) +.060*rxt(k,3463)*y(k,557) +.956*rxt(k,3498)*y(k,23) +.932*rxt(k,3582)*y(k,172) + &
.820*rxt(k,3678)*y(k,466) +.028*rxt(k,3686)*y(k,477) +.360*rxt(k,3694)*y(k,812) +.145*rxt(k,3732)*y(k,169) + &
.535*rxt(k,3776)*y(k,463) +.392*rxt(k,4118)*y(k,656) +.652*rxt(k,4122)*y(k,426) +.652*rxt(k,4126)*y(k,718) + &
.500*rxt(k,4129)*y(k,745) +.500*rxt(k,4175)*y(k,604))*y(k,705) + (.570*rxt(k,540)*y(k,350) + &
.042*rxt(k,2037)*y(k,879) +1.140*rxt(k,2611)*y(k,591) +1.092*rxt(k,2802)*y(k,628) +1.064*rxt(k,2932)*y(k,514) + &
.984*rxt(k,3233)*y(k,363) +.984*rxt(k,3246)*y(k,569) +2.001*rxt(k,4046)*y(k,439))*y(k,696) &
 + (.032*rxt(k,570)*y(k,582) +.231*rxt(k,1204)*y(k,457) +.796*rxt(k,1421)*y(k,285) +.008*rxt(k,2027)*y(k,355) + &
.112*rxt(k,2643)*y(k,552))*y(k,693)
         loss(k,792) = (rxt(k,4402)* y(k,370) +rxt(k,4398)* y(k,548) +rxt(k,4403)* y(k,589) +rxt(k,4400)* y(k,620) +rxt(k,4404) &
* y(k,633) +rxt(k,4397)* y(k,690) +rxt(k,4399)* y(k,693) +rxt(k,4401)* y(k,758) +rxt(k,4405)* y(k,768) &
 +rxt(k,4406)* y(k,769))* y(k,862)
         prod(k,792) = (.960*rxt(k,585)*y(k,586) +.045*rxt(k,1083)*y(k,52) +.840*rxt(k,1116)*y(k,19) +.020*rxt(k,1132)*y(k,84) + &
.120*rxt(k,1144)*y(k,263) +.012*rxt(k,1155)*y(k,176) +.003*rxt(k,1199)*y(k,457) +.036*rxt(k,1236)*y(k,72) + &
.024*rxt(k,1247)*y(k,213) +.204*rxt(k,1258)*y(k,264) +.012*rxt(k,1292)*y(k,168) +.024*rxt(k,1332)*y(k,386) + &
.021*rxt(k,1354)*y(k,395) +.021*rxt(k,1366)*y(k,792) +.024*rxt(k,1390)*y(k,785) +.012*rxt(k,1426)*y(k,81) + &
.392*rxt(k,1448)*y(k,781) +.015*rxt(k,1461)*y(k,82) +.008*rxt(k,1473)*y(k,317) +.060*rxt(k,1486)*y(k,71) + &
.272*rxt(k,1497)*y(k,215) +.208*rxt(k,1510)*y(k,265) +.009*rxt(k,1534)*y(k,113) +.048*rxt(k,1546)*y(k,208) + &
.020*rxt(k,1605)*y(k,182) +.018*rxt(k,1618)*y(k,257) +.009*rxt(k,1629)*y(k,290) +.030*rxt(k,1642)*y(k,387) + &
.009*rxt(k,1665)*y(k,269) +.006*rxt(k,1699)*y(k,392) +.030*rxt(k,1722)*y(k,786) +.006*rxt(k,1734)*y(k,790) + &
.018*rxt(k,1746)*y(k,396) +.018*rxt(k,1771)*y(k,793) +.039*rxt(k,1791)*y(k,83) +.024*rxt(k,1814)*y(k,267) + &
.030*rxt(k,1825)*y(k,787) +.024*rxt(k,1838)*y(k,183) +.021*rxt(k,1851)*y(k,400) +.006*rxt(k,1884)*y(k,791) + &
.021*rxt(k,1895)*y(k,796) +.036*rxt(k,1928)*y(k,67) +.033*rxt(k,1951)*y(k,797) +.030*rxt(k,1962)*y(k,53) + &
.009*rxt(k,1972)*y(k,219) +.048*rxt(k,1984)*y(k,404) +.030*rxt(k,1994)*y(k,795) +.076*rxt(k,2047)*y(k,479) + &
.027*rxt(k,2084)*y(k,55) +.045*rxt(k,2094)*y(k,800) +.027*rxt(k,2104)*y(k,56) +.048*rxt(k,2114)*y(k,801) + &
.027*rxt(k,2124)*y(k,57) +.045*rxt(k,2134)*y(k,802) +.024*rxt(k,2144)*y(k,58) +.042*rxt(k,2154)*y(k,803) + &
.039*rxt(k,2164)*y(k,804) +.141*rxt(k,2790)*y(k,501) +.328*rxt(k,3009)*y(k,554) +.460*rxt(k,3100)*y(k,668) + &
.468*rxt(k,3111)*y(k,567) +.030*rxt(k,3449)*y(k,436) +.027*rxt(k,3471)*y(k,587) +.025*rxt(k,3540)*y(k,529) + &
.620*rxt(k,3582)*y(k,172) +.042*rxt(k,3750)*y(k,588) +.126*rxt(k,3785)*y(k,652) +.261*rxt(k,3798)*y(k,651)) &
*y(k,705) + (1.380*rxt(k,1124)*y(k,33) +.018*rxt(k,1215)*y(k,780) +.064*rxt(k,1457)*y(k,781) + &
.036*rxt(k,2056)*y(k,479) +.492*rxt(k,2080)*y(k,881) +.711*rxt(k,2794)*y(k,501) +3.272*rxt(k,3016)*y(k,554) + &
3.220*rxt(k,3106)*y(k,668) +4.055*rxt(k,3118)*y(k,567) +1.584*rxt(k,3478)*y(k,587) +.040*rxt(k,3551)*y(k,529) + &
1.641*rxt(k,3588)*y(k,172) +.364*rxt(k,3794)*y(k,652) +.520*rxt(k,3807)*y(k,651))*y(k,693) &
 + (.080*rxt(k,1453)*y(k,781) +.120*rxt(k,3475)*y(k,587) +.080*rxt(k,3790)*y(k,652) +.120*rxt(k,3803)*y(k,651)) &
*y(k,696)
         loss(k,785) = (rxt(k,4412)* y(k,370) +rxt(k,4408)* y(k,548) +rxt(k,4413)* y(k,589) +rxt(k,4410)* y(k,620) +rxt(k,4414) &
* y(k,633) +rxt(k,4407)* y(k,690) +rxt(k,4409)* y(k,693) +rxt(k,4411)* y(k,758) +rxt(k,4415)* y(k,768) &
 +rxt(k,4416)* y(k,769))* y(k,863)
         prod(k,785) = (2.115*rxt(k,295)*y(k,784) +.561*rxt(k,593)*y(k,586) +1.932*rxt(k,1098)*y(k,585) + &
2.115*rxt(k,1106)*y(k,385) +.360*rxt(k,1123)*y(k,33) +.057*rxt(k,1161)*y(k,176) +2.805*rxt(k,1171)*y(k,180) + &
1.413*rxt(k,1182)*y(k,389) +1.413*rxt(k,1193)*y(k,788) +2.436*rxt(k,1203)*y(k,457) +1.293*rxt(k,1214)*y(k,780) + &
.027*rxt(k,1298)*y(k,168) +1.804*rxt(k,1317)*y(k,122) +2.175*rxt(k,1327)*y(k,181) +.360*rxt(k,1339)*y(k,386) + &
.216*rxt(k,1349)*y(k,391) +2.616*rxt(k,1361)*y(k,395) +2.616*rxt(k,1373)*y(k,792) +.204*rxt(k,1385)*y(k,798) + &
.360*rxt(k,1397)*y(k,785) +.216*rxt(k,1407)*y(k,789) +2.564*rxt(k,1419)*y(k,285) +2.664*rxt(k,1431)*y(k,81) + &
.888*rxt(k,1442)*y(k,456) +1.280*rxt(k,1455)*y(k,781) +2.508*rxt(k,1467)*y(k,82) +1.768*rxt(k,1479)*y(k,317) + &
.204*rxt(k,1528)*y(k,136) +.066*rxt(k,1564)*y(k,178) +.152*rxt(k,1585)*y(k,398) +2.456*rxt(k,1598)*y(k,141) + &
.524*rxt(k,1612)*y(k,182) +2.427*rxt(k,1624)*y(k,257) +2.772*rxt(k,1636)*y(k,290) +.039*rxt(k,1649)*y(k,387) + &
.171*rxt(k,1660)*y(k,202) +2.772*rxt(k,1672)*y(k,269) +.330*rxt(k,1685)*y(k,216) +2.595*rxt(k,1694)*y(k,123) + &
.075*rxt(k,1705)*y(k,392) +.152*rxt(k,1716)*y(k,794) +.039*rxt(k,1729)*y(k,786) +.075*rxt(k,1740)*y(k,790) + &
1.992*rxt(k,1752)*y(k,396) +.172*rxt(k,1764)*y(k,799) +1.992*rxt(k,1777)*y(k,793) +1.654*rxt(k,1787)*y(k,26) + &
.188*rxt(k,1807)*y(k,127) +.030*rxt(k,1832)*y(k,787) +.392*rxt(k,1845)*y(k,183) +.068*rxt(k,1867)*y(k,782) + &
.033*rxt(k,1879)*y(k,783) +.021*rxt(k,1890)*y(k,791) +1.332*rxt(k,1911)*y(k,128) +3.288*rxt(k,1922)*y(k,124) + &
.093*rxt(k,1946)*y(k,291) +.015*rxt(k,1957)*y(k,797) +.192*rxt(k,1978)*y(k,219) +.015*rxt(k,2000)*y(k,795) + &
2.976*rxt(k,2010)*y(k,249) +2.572*rxt(k,2025)*y(k,355) +2.504*rxt(k,2054)*y(k,479) +1.944*rxt(k,2078)*y(k,881) + &
.744*rxt(k,2570)*y(k,447) +2.232*rxt(k,3442)*y(k,356) +.336*rxt(k,3456)*y(k,436) +.252*rxt(k,3477)*y(k,587) + &
2.095*rxt(k,3549)*y(k,529) +.204*rxt(k,3792)*y(k,652) +.012*rxt(k,3805)*y(k,651) +1.898*rxt(k,3924)*y(k,779) + &
1.898*rxt(k,3935)*y(k,379))*y(k,693) + (1.878*rxt(k,577)*y(k,761) +1.533*rxt(k,4194)*y(k,762) + &
2.840*rxt(k,4201)*y(k,764) +.364*rxt(k,4205)*y(k,765) +2.236*rxt(k,4209)*y(k,766) +.016*rxt(k,4213)*y(k,767)) &
*y(k,705)
         loss(k,594) = (rxt(k,4422)* y(k,370) +rxt(k,4418)* y(k,548) +rxt(k,4423)* y(k,589) +rxt(k,4420)* y(k,620) +rxt(k,4424) &
* y(k,633) +rxt(k,4417)* y(k,690) +rxt(k,4419)* y(k,693) +rxt(k,4421)* y(k,758) +rxt(k,4425)* y(k,768) &
 +rxt(k,4426)* y(k,769))* y(k,864)
         prod(k,594) = (.048*rxt(k,496)*y(k,770) +.123*rxt(k,500)*y(k,755) +.120*rxt(k,504)*y(k,756) +rxt(k,3875)*y(k,539)) &
*y(k,705)
         loss(k,797) = (rxt(k,4432)* y(k,370) +rxt(k,4428)* y(k,548) +rxt(k,4433)* y(k,589) +rxt(k,4430)* y(k,620) +rxt(k,4434) &
* y(k,633) +rxt(k,4427)* y(k,690) +rxt(k,4429)* y(k,693) +rxt(k,4431)* y(k,758) +rxt(k,4435)* y(k,768) &
 +rxt(k,4436)* y(k,769))* y(k,865)
         prod(k,797) = (1.191*rxt(k,256)*y(k,680) +2.064*rxt(k,265)*y(k,681) +1.194*rxt(k,271)*y(k,453) + &
1.318*rxt(k,273)*y(k,683) +.051*rxt(k,278)*y(k,454) +1.244*rxt(k,281)*y(k,685) +.116*rxt(k,287)*y(k,88) + &
1.168*rxt(k,292)*y(k,687) +1.114*rxt(k,294)*y(k,669) +.030*rxt(k,314)*y(k,517) +.021*rxt(k,317)*y(k,662) + &
.104*rxt(k,324)*y(k,661) +.136*rxt(k,497)*y(k,770) +.252*rxt(k,501)*y(k,755) +1.328*rxt(k,562)*y(k,582) + &
.035*rxt(k,575)*y(k,731) +.186*rxt(k,580)*y(k,761) +.480*rxt(k,612)*y(k,194) +.108*rxt(k,615)*y(k,280) + &
.018*rxt(k,621)*y(k,621) +1.072*rxt(k,624)*y(k,96) +.075*rxt(k,628)*y(k,115) +.543*rxt(k,631)*y(k,137) + &
1.545*rxt(k,634)*y(k,195) +.008*rxt(k,637)*y(k,209) +.705*rxt(k,641)*y(k,281) +.159*rxt(k,644)*y(k,258) + &
.028*rxt(k,651)*y(k,24) +.132*rxt(k,655)*y(k,37) +.028*rxt(k,659)*y(k,506) +.753*rxt(k,662)*y(k,622) + &
1.396*rxt(k,666)*y(k,97) +.792*rxt(k,673)*y(k,116) +.972*rxt(k,676)*y(k,138) +.678*rxt(k,678)*y(k,146) + &
1.647*rxt(k,682)*y(k,196) +1.566*rxt(k,685)*y(k,282) +1.128*rxt(k,688)*y(k,319) +.004*rxt(k,691)*y(k,102) + &
.408*rxt(k,695)*y(k,210) +.236*rxt(k,699)*y(k,87) +.399*rxt(k,703)*y(k,217) +.117*rxt(k,706)*y(k,251) + &
.020*rxt(k,710)*y(k,5) +.372*rxt(k,714)*y(k,7) +1.468*rxt(k,717)*y(k,12) +.780*rxt(k,721)*y(k,15) + &
.224*rxt(k,725)*y(k,17) +.120*rxt(k,729)*y(k,76) +1.266*rxt(k,733)*y(k,25) +.144*rxt(k,735)*y(k,47) + &
.360*rxt(k,739)*y(k,38) +1.002*rxt(k,742)*y(k,507) +.116*rxt(k,746)*y(k,728) +.224*rxt(k,750)*y(k,91) + &
1.377*rxt(k,754)*y(k,109) +1.194*rxt(k,757)*y(k,139) +1.608*rxt(k,759)*y(k,197) +.054*rxt(k,763)*y(k,207) + &
1.221*rxt(k,766)*y(k,225) +1.458*rxt(k,768)*y(k,310) +1.689*rxt(k,772)*y(k,320) +.236*rxt(k,775)*y(k,129) + &
1.280*rxt(k,779)*y(k,211) +.393*rxt(k,783)*y(k,292) +1.014*rxt(k,786)*y(k,98) +.440*rxt(k,789)*y(k,89) + &
.780*rxt(k,792)*y(k,153) +1.581*rxt(k,796)*y(k,117) +1.227*rxt(k,799)*y(k,147) +1.575*rxt(k,802)*y(k,283) + &
1.053*rxt(k,805)*y(k,218) +1.509*rxt(k,807)*y(k,259) +.279*rxt(k,811)*y(k,558) +1.428*rxt(k,814)*y(k,13) + &
.128*rxt(k,818)*y(k,30) +.592*rxt(k,822)*y(k,6) +.429*rxt(k,826)*y(k,70) +1.257*rxt(k,828)*y(k,393) + &
.951*rxt(k,832)*y(k,134) +1.476*rxt(k,835)*y(k,140) +.831*rxt(k,838)*y(k,154) +1.548*rxt(k,840)*y(k,198) + &
.534*rxt(k,844)*y(k,214) +1.509*rxt(k,846)*y(k,284) +1.566*rxt(k,850)*y(k,321) +1.383*rxt(k,852)*y(k,322) + &
.354*rxt(k,856)*y(k,130) +.396*rxt(k,859)*y(k,145) +1.176*rxt(k,863)*y(k,212) +.951*rxt(k,867)*y(k,293) + &
1.158*rxt(k,869)*y(k,99) +.692*rxt(k,873)*y(k,90) +.932*rxt(k,877)*y(k,92) +1.332*rxt(k,881)*y(k,111) + &
1.590*rxt(k,884)*y(k,118) +1.260*rxt(k,887)*y(k,148) +1.518*rxt(k,890)*y(k,185) +1.608*rxt(k,892)*y(k,311) + &
.741*rxt(k,896)*y(k,36) +.627*rxt(k,899)*y(k,46) +1.436*rxt(k,902)*y(k,77) +1.482*rxt(k,905)*y(k,402) + &
1.080*rxt(k,908)*y(k,670) +1.452*rxt(k,911)*y(k,103) +1.131*rxt(k,914)*y(k,155) +1.131*rxt(k,917)*y(k,224) + &
1.470*rxt(k,919)*y(k,274) +1.503*rxt(k,922)*y(k,318) +.708*rxt(k,926)*y(k,41) +1.224*rxt(k,929)*y(k,69) + &
1.440*rxt(k,932)*y(k,405) +1.060*rxt(k,935)*y(k,671) +1.122*rxt(k,938)*y(k,107) +1.419*rxt(k,940)*y(k,229) + &
1.377*rxt(k,944)*y(k,230) +1.452*rxt(k,946)*y(k,275) +1.515*rxt(k,949)*y(k,336) +.900*rxt(k,953)*y(k,29) + &
.978*rxt(k,957)*y(k,78) +.968*rxt(k,959)*y(k,409) +1.050*rxt(k,961)*y(k,672) +.951*rxt(k,964)*y(k,110) + &
1.020*rxt(k,967)*y(k,133) +1.329*rxt(k,970)*y(k,231) +.912*rxt(k,972)*y(k,234) +1.446*rxt(k,975)*y(k,276) + &
1.497*rxt(k,978)*y(k,337) +1.116*rxt(k,982)*y(k,42) +1.275*rxt(k,985)*y(k,74) +.966*rxt(k,988)*y(k,411) + &
1.042*rxt(k,990)*y(k,673) +2.313*rxt(k,993)*y(k,131) +1.140*rxt(k,996)*y(k,184) +1.323*rxt(k,998)*y(k,235) + &
1.278*rxt(k,1001)*y(k,238) +1.479*rxt(k,1004)*y(k,277) +1.515*rxt(k,1007)*y(k,338) +.927*rxt(k,1011)*y(k,44) + &
1.047*rxt(k,1013)*y(k,79) +.966*rxt(k,1016)*y(k,413) +1.038*rxt(k,1018)*y(k,674) +1.065*rxt(k,1021)*y(k,152) + &
1.323*rxt(k,1023)*y(k,236) +1.305*rxt(k,1026)*y(k,239) +1.479*rxt(k,1029)*y(k,278) +1.512*rxt(k,1032)*y(k,339) + &
.957*rxt(k,1036)*y(k,31) +1.296*rxt(k,1038)*y(k,75) +.966*rxt(k,1041)*y(k,415) +1.392*rxt(k,1044)*y(k,40) + &
1.034*rxt(k,1047)*y(k,675) +1.062*rxt(k,1050)*y(k,158) +1.479*rxt(k,1052)*y(k,279) +1.422*rxt(k,1055)*y(k,301) + &
1.002*rxt(k,1058)*y(k,340) +1.104*rxt(k,1061)*y(k,43) +1.101*rxt(k,1063)*y(k,80) +.966*rxt(k,1066)*y(k,374) + &
.076*rxt(k,1132)*y(k,84) +.044*rxt(k,1145)*y(k,263) +1.052*rxt(k,1236)*y(k,72) +.084*rxt(k,1258)*y(k,264) + &
.132*rxt(k,1271)*y(k,312) +.123*rxt(k,1344)*y(k,391) +.138*rxt(k,1379)*y(k,798) +.123*rxt(k,1402)*y(k,789) + &
1.080*rxt(k,1486)*y(k,71) +.140*rxt(k,1498)*y(k,215) +1.004*rxt(k,1510)*y(k,265) +2.445*rxt(k,1523)*y(k,136) + &
2.094*rxt(k,1535)*y(k,113) +2.415*rxt(k,1559)*y(k,178) +.303*rxt(k,1580)*y(k,398) +.048*rxt(k,1643)*y(k,387) + &
.108*rxt(k,1655)*y(k,202) +.126*rxt(k,1700)*y(k,392) +.303*rxt(k,1711)*y(k,794) +.048*rxt(k,1723)*y(k,786) + &
.126*rxt(k,1735)*y(k,790) +2.364*rxt(k,1782)*y(k,26) +.777*rxt(k,1792)*y(k,83) +3.064*rxt(k,1814)*y(k,267) + &
.066*rxt(k,1826)*y(k,787) +.032*rxt(k,1839)*y(k,183) +1.215*rxt(k,1852)*y(k,400) +.249*rxt(k,1862)*y(k,782) + &
.267*rxt(k,1874)*y(k,783) +.651*rxt(k,1885)*y(k,791) +1.215*rxt(k,1896)*y(k,796) +.756*rxt(k,1929)*y(k,67) + &
.456*rxt(k,1939)*y(k,291) +.708*rxt(k,1952)*y(k,797) +.756*rxt(k,1963)*y(k,53) +2.157*rxt(k,1973)*y(k,219) + &
1.329*rxt(k,1985)*y(k,404) +.690*rxt(k,1995)*y(k,795) +3.124*rxt(k,2032)*y(k,879) +1.148*rxt(k,2047)*y(k,479) + &
.534*rxt(k,2061)*y(k,771) +1.266*rxt(k,2071)*y(k,881) +.774*rxt(k,2085)*y(k,55) +.696*rxt(k,2095)*y(k,800) + &
.783*rxt(k,2105)*y(k,56) +.681*rxt(k,2115)*y(k,801) +.798*rxt(k,2125)*y(k,57) +.684*rxt(k,2135)*y(k,802) + &
.816*rxt(k,2145)*y(k,58) +.702*rxt(k,2155)*y(k,803) +.720*rxt(k,2165)*y(k,804) +.980*rxt(k,2180)*y(k,383) + &
1.384*rxt(k,2206)*y(k,676) +.820*rxt(k,2210)*y(k,572) +.350*rxt(k,2214)*y(k,644) +.555*rxt(k,2219)*y(k,704) + &
.560*rxt(k,2224)*y(k,721) +1.504*rxt(k,2251)*y(k,373) +.296*rxt(k,2255)*y(k,807) +.525*rxt(k,2259)*y(k,596) + &
.820*rxt(k,2264)*y(k,697) +.820*rxt(k,2269)*y(k,711) +.455*rxt(k,2274)*y(k,719) +.240*rxt(k,2279)*y(k,240) + &
.240*rxt(k,2284)*y(k,302) +.116*rxt(k,2289)*y(k,324) +1.336*rxt(k,2292)*y(k,816) +1.712*rxt(k,2301)*y(k,376) + &
.770*rxt(k,2305)*y(k,597) +1.155*rxt(k,2310)*y(k,698) +1.160*rxt(k,2315)*y(k,712) +.435*rxt(k,2320)*y(k,241) + &
.435*rxt(k,2325)*y(k,303) +.208*rxt(k,2330)*y(k,325) +1.868*rxt(k,2338)*y(k,377) +.855*rxt(k,2342)*y(k,598) + &
1.265*rxt(k,2347)*y(k,699) +1.270*rxt(k,2352)*y(k,713) +.525*rxt(k,2357)*y(k,242) +.525*rxt(k,2362)*y(k,304) + &
.256*rxt(k,2367)*y(k,326) +1.992*rxt(k,2375)*y(k,378) +.820*rxt(k,2379)*y(k,599) +1.480*rxt(k,2383)*y(k,700) + &
1.480*rxt(k,2388)*y(k,714) +.675*rxt(k,2393)*y(k,243) +.675*rxt(k,2398)*y(k,305) +.336*rxt(k,2403)*y(k,327) + &
2.096*rxt(k,2407)*y(k,380) +.924*rxt(k,2411)*y(k,600) +1.635*rxt(k,2415)*y(k,701) +1.640*rxt(k,2420)*y(k,715) + &
.785*rxt(k,2425)*y(k,244) +.785*rxt(k,2430)*y(k,306) +.392*rxt(k,2435)*y(k,328) +2.180*rxt(k,2439)*y(k,381) + &
1.020*rxt(k,2443)*y(k,601) +1.775*rxt(k,2447)*y(k,702) +1.775*rxt(k,2452)*y(k,716) +.885*rxt(k,2457)*y(k,245) + &
.885*rxt(k,2462)*y(k,307) +.448*rxt(k,2467)*y(k,329) +2.252*rxt(k,2471)*y(k,382) +1.108*rxt(k,2475)*y(k,602) + &
1.895*rxt(k,2479)*y(k,703) +1.900*rxt(k,2484)*y(k,717) +.990*rxt(k,2489)*y(k,246) +.990*rxt(k,2494)*y(k,308) + &
.504*rxt(k,2499)*y(k,330) +1.278*rxt(k,2552)*y(k,643) +.144*rxt(k,2555)*y(k,636) +.150*rxt(k,2580)*y(k,826) + &
.279*rxt(k,2599)*y(k,679) +.204*rxt(k,2619)*y(k,606) +1.332*rxt(k,2638)*y(k,552) +.171*rxt(k,2648)*y(k,48) + &
.156*rxt(k,2654)*y(k,641) +.087*rxt(k,2687)*y(k,635) +.824*rxt(k,2690)*y(k,518) +.081*rxt(k,2694)*y(k,199) + &
.183*rxt(k,2697)*y(k,286) +.188*rxt(k,2704)*y(k,626) +2.037*rxt(k,2706)*y(k,465) +.028*rxt(k,2722)*y(k,354) + &
.390*rxt(k,2726)*y(k,828) +.372*rxt(k,2765)*y(k,164) +.252*rxt(k,2772)*y(k,407) +.012*rxt(k,2775)*y(k,563) + &
.028*rxt(k,2783)*y(k,523) +.393*rxt(k,2787)*y(k,654) +1.140*rxt(k,2790)*y(k,501) +.028*rxt(k,2816)*y(k,825) + &
.368*rxt(k,2820)*y(k,526) +.056*rxt(k,2829)*y(k,610) +.040*rxt(k,2833)*y(k,624) +.340*rxt(k,2836)*y(k,403) + &
1.004*rxt(k,2851)*y(k,516) +1.230*rxt(k,2855)*y(k,206) +1.084*rxt(k,2858)*y(k,260) +.148*rxt(k,2862)*y(k,287) + &
.072*rxt(k,2866)*y(k,603) +1.665*rxt(k,2871)*y(k,513) +2.868*rxt(k,2879)*y(k,657) +.120*rxt(k,2883)*y(k,725) + &
.591*rxt(k,2886)*y(k,421) +.345*rxt(k,2890)*y(k,422) +.352*rxt(k,2901)*y(k,655) +.759*rxt(k,2908)*y(k,61) + &
.483*rxt(k,2911)*y(k,165) +.140*rxt(k,2914)*y(k,647) +.255*rxt(k,2917)*y(k,740) +.312*rxt(k,2921)*y(k,498) + &
.052*rxt(k,2924)*y(k,584) +.189*rxt(k,2928)*y(k,514) +.256*rxt(k,2940)*y(k,505) +1.048*rxt(k,2951)*y(k,361) + &
.200*rxt(k,2956)*y(k,743) +.156*rxt(k,2970)*y(k,615) +.177*rxt(k,2974)*y(k,410) +.168*rxt(k,2978)*y(k,179) + &
1.452*rxt(k,2981)*y(k,493) +.240*rxt(k,2984)*y(k,342) +1.008*rxt(k,2988)*y(k,742) +1.040*rxt(k,2991)*y(k,366) + &
.244*rxt(k,2995)*y(k,289) +.008*rxt(k,2999)*y(k,653) +1.516*rxt(k,3004)*y(k,730) +1.272*rxt(k,3009)*y(k,554) + &
.200*rxt(k,3021)*y(k,724) +.240*rxt(k,3026)*y(k,449) +.004*rxt(k,3029)*y(k,204) +.376*rxt(k,3033)*y(k,640) + &
2.832*rxt(k,3037)*y(k,470) +1.452*rxt(k,3040)*y(k,490) +.016*rxt(k,3044)*y(k,459) +.477*rxt(k,3051)*y(k,347) + &
2.910*rxt(k,3054)*y(k,811) +.228*rxt(k,3062)*y(k,372) +.207*rxt(k,3069)*y(k,200) +1.041*rxt(k,3073)*y(k,412) + &
.476*rxt(k,3085)*y(k,266) +.945*rxt(k,3089)*y(k,63) +.764*rxt(k,3092)*y(k,121) +.064*rxt(k,3096)*y(k,294) + &
1.236*rxt(k,3101)*y(k,668) +1.356*rxt(k,3112)*y(k,567) +1.016*rxt(k,3123)*y(k,367) +1.917*rxt(k,3127)*y(k,352) + &
.160*rxt(k,3131)*y(k,727) +.532*rxt(k,3134)*y(k,562) +.052*rxt(k,3138)*y(k,175) +1.552*rxt(k,3142)*y(k,616) + &
.084*rxt(k,3146)*y(k,722) +.045*rxt(k,3150)*y(k,723) +.771*rxt(k,3153)*y(k,365) +.316*rxt(k,3157)*y(k,492) + &
2.928*rxt(k,3164)*y(k,473) +2.740*rxt(k,3168)*y(k,491) +2.676*rxt(k,3173)*y(k,489) +.759*rxt(k,3179)*y(k,460) + &
3.688*rxt(k,3183)*y(k,823) +.012*rxt(k,3187)*y(k,162) +1.461*rxt(k,3195)*y(k,414) +1.152*rxt(k,3198)*y(k,65) + &
.981*rxt(k,3201)*y(k,170) +.825*rxt(k,3204)*y(k,166) +.543*rxt(k,3207)*y(k,248) +.954*rxt(k,3209)*y(k,309) + &
.756*rxt(k,3213)*y(k,73) +.039*rxt(k,3217)*y(k,565) +.441*rxt(k,3219)*y(k,364) +1.385*rxt(k,3224)*y(k,496) + &
.248*rxt(k,3229)*y(k,363) +.040*rxt(k,3242)*y(k,569) +.068*rxt(k,3255)*y(k,125) +1.012*rxt(k,3258)*y(k,187) + &
1.416*rxt(k,3262)*y(k,271) +.568*rxt(k,3266)*y(k,313) +.015*rxt(k,3271)*y(k,568) +.808*rxt(k,3276)*y(k,362) + &
1.941*rxt(k,3278)*y(k,682) +.340*rxt(k,3283)*y(k,593) +1.707*rxt(k,3286)*y(k,408) +.990*rxt(k,3289)*y(k,171) + &
1.833*rxt(k,3291)*y(k,617) +.168*rxt(k,3295)*y(k,167) +2.040*rxt(k,3299)*y(k,495) +.456*rxt(k,3310)*y(k,161) + &
2.456*rxt(k,3314)*y(k,174) +2.572*rxt(k,3318)*y(k,468) +2.728*rxt(k,3323)*y(k,485) +.236*rxt(k,3326)*y(k,461) + &
1.616*rxt(k,3331)*y(k,471) +3.620*rxt(k,3335)*y(k,821) +4.344*rxt(k,3338)*y(k,813) +.582*rxt(k,3342)*y(k,205) + &
.522*rxt(k,3344)*y(k,323) +.272*rxt(k,3348)*y(k,68) +.183*rxt(k,3352)*y(k,830) +1.482*rxt(k,3354)*y(k,416) + &
.104*rxt(k,3358)*y(k,475) +.471*rxt(k,3361)*y(k,156) +.102*rxt(k,3365)*y(k,157) +.824*rxt(k,3369)*y(k,142) + &
1.992*rxt(k,3372)*y(k,188) +1.592*rxt(k,3376)*y(k,254) +1.784*rxt(k,3380)*y(k,272) +.788*rxt(k,3384)*y(k,314) + &
2.280*rxt(k,3388)*y(k,333) +.508*rxt(k,3392)*y(k,576) +1.845*rxt(k,3395)*y(k,684) +1.770*rxt(k,3398)*y(k,618) + &
2.732*rxt(k,3402)*y(k,51) +2.740*rxt(k,3406)*y(k,488) +2.652*rxt(k,3410)*y(k,486) +2.372*rxt(k,3414)*y(k,487) + &
3.416*rxt(k,3418)*y(k,824) +2.760*rxt(k,3422)*y(k,834) +4.012*rxt(k,3425)*y(k,815) +.003*rxt(k,3432)*y(k,417) + &
.116*rxt(k,3435)*y(k,356) +.135*rxt(k,3449)*y(k,436) +.008*rxt(k,3464)*y(k,557) +.096*rxt(k,3467)*y(k,632) + &
.411*rxt(k,3471)*y(k,587) +1.551*rxt(k,3482)*y(k,375) +.918*rxt(k,3486)*y(k,571) +1.017*rxt(k,3489)*y(k,54) + &
.664*rxt(k,3492)*y(k,237) +2.112*rxt(k,3495)*y(k,406) +1.204*rxt(k,3503)*y(k,143) +1.908*rxt(k,3507)*y(k,173) + &
2.112*rxt(k,3511)*y(k,220) +1.876*rxt(k,3515)*y(k,226) +1.638*rxt(k,3519)*y(k,255) +1.683*rxt(k,3522)*y(k,273) + &
1.328*rxt(k,3525)*y(k,295) +1.240*rxt(k,3529)*y(k,315) +2.180*rxt(k,3533)*y(k,334) +1.731*rxt(k,3536)*y(k,686) + &
1.890*rxt(k,3541)*y(k,529) +1.707*rxt(k,3556)*y(k,619) +2.376*rxt(k,3560)*y(k,494) +2.706*rxt(k,3563)*y(k,472) + &
2.692*rxt(k,3566)*y(k,462) +1.764*rxt(k,3570)*y(k,469) +3.320*rxt(k,3574)*y(k,820) +3.356*rxt(k,3578)*y(k,835) + &
.996*rxt(k,3583)*y(k,172) +2.012*rxt(k,3593)*y(k,108) +2.212*rxt(k,3596)*y(k,126) +1.660*rxt(k,3600)*y(k,144) + &
1.804*rxt(k,3604)*y(k,149) +1.908*rxt(k,3608)*y(k,189) +1.804*rxt(k,3612)*y(k,227) +1.976*rxt(k,3616)*y(k,232) + &
1.608*rxt(k,3620)*y(k,256) +2.004*rxt(k,3623)*y(k,296) +1.492*rxt(k,3627)*y(k,297) +1.368*rxt(k,3631)*y(k,316) + &
2.128*rxt(k,3634)*y(k,335) +1.662*rxt(k,3637)*y(k,688) +1.644*rxt(k,3640)*y(k,611) +.800*rxt(k,3644)*y(k,564) + &
.399*rxt(k,3648)*y(k,151) +2.056*rxt(k,3651)*y(k,233) +1.557*rxt(k,3655)*y(k,261) +1.580*rxt(k,3658)*y(k,298) + &
1.605*rxt(k,3661)*y(k,612) +.040*rxt(k,3666)*y(k,818) +.005*rxt(k,3671)*y(k,817) +3.764*rxt(k,3674)*y(k,467) + &
.120*rxt(k,3679)*y(k,466) +1.599*rxt(k,3682)*y(k,458) +.448*rxt(k,3687)*y(k,477) +3.652*rxt(k,3690)*y(k,814) + &
.732*rxt(k,3695)*y(k,812) +1.552*rxt(k,3698)*y(k,222) +2.004*rxt(k,3702)*y(k,253) +1.416*rxt(k,3706)*y(k,300) + &
1.578*rxt(k,3709)*y(k,627) +3.336*rxt(k,3714)*y(k,822) +1.712*rxt(k,3718)*y(k,106) +1.520*rxt(k,3721)*y(k,223) + &
1.812*rxt(k,3725)*y(k,228) +1.563*rxt(k,3728)*y(k,613) +1.910*rxt(k,3733)*y(k,169) +1.336*rxt(k,3738)*y(k,132) + &
1.551*rxt(k,3741)*y(k,252) +1.380*rxt(k,3744)*y(k,299) +1.557*rxt(k,3747)*y(k,629) +.684*rxt(k,3751)*y(k,588) + &
1.479*rxt(k,3760)*y(k,614) +2.660*rxt(k,3765)*y(k,105) +1.604*rxt(k,3768)*y(k,221) +2.720*rxt(k,3772)*y(k,331) + &
1.575*rxt(k,3777)*y(k,463) +.020*rxt(k,3782)*y(k,836) +.030*rxt(k,3786)*y(k,652) +.369*rxt(k,3799)*y(k,651) + &
1.084*rxt(k,3811)*y(k,608) +1.028*rxt(k,3830)*y(k,481) +1.030*rxt(k,3838)*y(k,503) +1.542*rxt(k,3846)*y(k,522) + &
2.000*rxt(k,3889)*y(k,829) +1.074*rxt(k,3897)*y(k,580) +1.407*rxt(k,3905)*y(k,50) +.084*rxt(k,3985)*y(k,806) + &
1.617*rxt(k,3991)*y(k,660) +1.920*rxt(k,4000)*y(k,497) +1.086*rxt(k,4004)*y(k,480) +2.292*rxt(k,4014)*y(k,159) + &
1.290*rxt(k,4033)*y(k,520) +.160*rxt(k,4060)*y(k,160) +1.275*rxt(k,4064)*y(k,689) +2.298*rxt(k,4096)*y(k,521) + &
1.416*rxt(k,4141)*y(k,499) +1.743*rxt(k,4152)*y(k,574) +1.880*rxt(k,4163)*y(k,720) +.705*rxt(k,4170)*y(k,827) + &
.006*rxt(k,4190)*y(k,735) +.093*rxt(k,4193)*y(k,736) +.040*rxt(k,4203)*y(k,764) +.360*rxt(k,4207)*y(k,765) + &
.324*rxt(k,4211)*y(k,766) +.012*rxt(k,4215)*y(k,767))*y(k,705) + (.012*rxt(k,1481)*y(k,317) + &
.192*rxt(k,1518)*y(k,265) +1.654*rxt(k,1788)*y(k,26) +.192*rxt(k,1980)*y(k,219) +1.576*rxt(k,2080)*y(k,881) + &
.064*rxt(k,3458)*y(k,436) +1.820*rxt(k,3552)*y(k,529) +.087*rxt(k,3588)*y(k,172) +.020*rxt(k,3794)*y(k,652) + &
.020*rxt(k,3807)*y(k,651) +1.626*rxt(k,3815)*y(k,608) +1.536*rxt(k,3834)*y(k,481) +1.476*rxt(k,3842)*y(k,503) + &
1.692*rxt(k,3851)*y(k,522) +2.000*rxt(k,3893)*y(k,829) +1.539*rxt(k,3901)*y(k,580) +1.692*rxt(k,3910)*y(k,50) + &
1.332*rxt(k,3997)*y(k,660) +1.218*rxt(k,4009)*y(k,480) +1.884*rxt(k,4021)*y(k,159) +1.332*rxt(k,4039)*y(k,520) + &
1.700*rxt(k,4067)*y(k,689) +2.706*rxt(k,4102)*y(k,521) +1.440*rxt(k,4143)*y(k,499) +2.382*rxt(k,4158)*y(k,574) + &
1.386*rxt(k,4167)*y(k,720))*y(k,693) + (.220*rxt(k,1516)*y(k,265) +.020*rxt(k,1967)*y(k,53) + &
.020*rxt(k,2089)*y(k,55) +.020*rxt(k,2109)*y(k,56) +.020*rxt(k,2129)*y(k,57) +.016*rxt(k,2149)*y(k,58) + &
.020*rxt(k,2159)*y(k,803) +.020*rxt(k,2169)*y(k,804) +.010*rxt(k,3440)*y(k,356) +.004*rxt(k,3755)*y(k,588) + &
.005*rxt(k,3791)*y(k,652) +.005*rxt(k,3803)*y(k,651) +2.000*rxt(k,3812)*y(k,608) +2.000*rxt(k,3831)*y(k,481) + &
2.000*rxt(k,3839)*y(k,503) +2.000*rxt(k,3849)*y(k,522) +2.000*rxt(k,3891)*y(k,829) +2.000*rxt(k,3898)*y(k,580) + &
2.000*rxt(k,3908)*y(k,50) +2.883*rxt(k,3994)*y(k,660) +1.152*rxt(k,4006)*y(k,480) +2.883*rxt(k,4017)*y(k,159) + &
2.883*rxt(k,4035)*y(k,520) +2.706*rxt(k,4099)*y(k,521) +2.382*rxt(k,4155)*y(k,574))*y(k,696) +1.526*rxt(k,26) &
*y(k,200) +2.097*rxt(k,29)*y(k,266) +.018*rxt(k,81)*y(k,557) +2.792*rxt(k,145)*y(k,755) +1.305*rxt(k,155) &
*y(k,761) +.340*rxt(k,162)*y(k,764) +1.449*rxt(k,166)*y(k,765) +.944*rxt(k,169)*y(k,766) +1.520*rxt(k,172) &
*y(k,767)
         loss(k,6) = 0.
         prod(k,6) = (.960*rxt(k,4138)*y(k,658) +.960*rxt(k,4144)*y(k,499) +.924*rxt(k,4167)*y(k,720) +.924*rxt(k,4173)*y(k,827)) &
*y(k,693) + (.432*rxt(k,4135)*y(k,658) +.492*rxt(k,4141)*y(k,499) +.600*rxt(k,4163)*y(k,720) + &
.543*rxt(k,4171)*y(k,827))*y(k,705)
         loss(k,829) = (rxt(k,4462)* y(k,370) +rxt(k,4458)* y(k,548) +rxt(k,4463)* y(k,589) +rxt(k,4460)* y(k,620) +rxt(k,4464) &
* y(k,633) +rxt(k,4457)* y(k,690) +rxt(k,4459)* y(k,693) +rxt(k,4461)* y(k,758) +rxt(k,4465)* y(k,768) &
 +rxt(k,4466)* y(k,769))* y(k,867)
         prod(k,829) = (.522*rxt(k,244)*y(k,738) +1.898*rxt(k,246)*y(k,450) +.360*rxt(k,248)*y(k,678) +.384*rxt(k,251)*y(k,191) + &
.660*rxt(k,256)*y(k,680) +.063*rxt(k,259)*y(k,93) +2.268*rxt(k,262)*y(k,452) +.339*rxt(k,265)*y(k,681) + &
.282*rxt(k,268)*y(k,114) +.406*rxt(k,271)*y(k,453) +.110*rxt(k,273)*y(k,683) +.300*rxt(k,275)*y(k,86) + &
1.476*rxt(k,278)*y(k,454) +.048*rxt(k,281)*y(k,685) +.939*rxt(k,283)*y(k,85) +1.548*rxt(k,286)*y(k,88) + &
1.050*rxt(k,290)*y(k,455) +.036*rxt(k,292)*y(k,687) +.030*rxt(k,294)*y(k,669) +1.128*rxt(k,308)*y(k,625) + &
.018*rxt(k,313)*y(k,517) +.072*rxt(k,320)*y(k,504) +.112*rxt(k,323)*y(k,661) +.800*rxt(k,493)*y(k,759) + &
.820*rxt(k,497)*y(k,770) +.225*rxt(k,501)*y(k,755) +2.240*rxt(k,504)*y(k,756) +1.628*rxt(k,519)*y(k,348) + &
1.628*rxt(k,526)*y(k,349) +2.025*rxt(k,552)*y(k,664) +.052*rxt(k,562)*y(k,582) +2.010*rxt(k,575)*y(k,731) + &
.216*rxt(k,579)*y(k,761) +.303*rxt(k,604)*y(k,193) +.412*rxt(k,608)*y(k,95) +1.971*rxt(k,611)*y(k,194) + &
.207*rxt(k,614)*y(k,280) +2.505*rxt(k,617)*y(k,581) +2.058*rxt(k,621)*y(k,621) +2.060*rxt(k,623)*y(k,96) + &
.198*rxt(k,627)*y(k,115) +1.611*rxt(k,630)*y(k,137) +.669*rxt(k,633)*y(k,195) +.184*rxt(k,636)*y(k,209) + &
1.389*rxt(k,640)*y(k,281) +.501*rxt(k,643)*y(k,258) +2.256*rxt(k,647)*y(k,11) +2.248*rxt(k,651)*y(k,24) + &
2.340*rxt(k,655)*y(k,37) +2.356*rxt(k,659)*y(k,506) +1.269*rxt(k,662)*y(k,622) +1.420*rxt(k,665)*y(k,97) + &
.171*rxt(k,669)*y(k,104) +.507*rxt(k,672)*y(k,116) +.852*rxt(k,675)*y(k,138) +1.290*rxt(k,678)*y(k,146) + &
.447*rxt(k,681)*y(k,196) +.444*rxt(k,684)*y(k,282) +1.131*rxt(k,687)*y(k,319) +.136*rxt(k,690)*y(k,102) + &
2.152*rxt(k,694)*y(k,210) +.400*rxt(k,698)*y(k,87) +.132*rxt(k,702)*y(k,217) +.783*rxt(k,705)*y(k,251) + &
1.504*rxt(k,709)*y(k,5) +1.756*rxt(k,713)*y(k,7) +1.412*rxt(k,716)*y(k,12) +1.364*rxt(k,721)*y(k,15) + &
1.948*rxt(k,725)*y(k,17) +2.036*rxt(k,729)*y(k,76) +.519*rxt(k,732)*y(k,25) +1.584*rxt(k,735)*y(k,47) + &
1.944*rxt(k,739)*y(k,38) +.915*rxt(k,742)*y(k,507) +2.552*rxt(k,746)*y(k,728) +2.448*rxt(k,749)*y(k,91) + &
.471*rxt(k,753)*y(k,109) +1.242*rxt(k,756)*y(k,139) +.192*rxt(k,759)*y(k,197) +.066*rxt(k,762)*y(k,207) + &
.432*rxt(k,765)*y(k,225) +.984*rxt(k,768)*y(k,310) +.378*rxt(k,771)*y(k,320) +1.272*rxt(k,774)*y(k,129) + &
1.248*rxt(k,778)*y(k,211) +2.670*rxt(k,782)*y(k,292) +1.110*rxt(k,785)*y(k,98) +1.012*rxt(k,788)*y(k,89) + &
1.095*rxt(k,792)*y(k,153) +.372*rxt(k,795)*y(k,117) +.555*rxt(k,798)*y(k,147) +.351*rxt(k,801)*y(k,283) + &
.228*rxt(k,804)*y(k,218) +.435*rxt(k,807)*y(k,259) +1.083*rxt(k,810)*y(k,558) +.600*rxt(k,814)*y(k,13) + &
1.904*rxt(k,818)*y(k,30) +1.860*rxt(k,822)*y(k,6) +1.233*rxt(k,825)*y(k,70) +1.035*rxt(k,828)*y(k,393) + &
1.314*rxt(k,831)*y(k,134) +.618*rxt(k,834)*y(k,140) +.921*rxt(k,837)*y(k,154) +.105*rxt(k,840)*y(k,198) + &
.732*rxt(k,843)*y(k,214) +.186*rxt(k,846)*y(k,284) +.414*rxt(k,849)*y(k,321) +.891*rxt(k,852)*y(k,322) + &
2.226*rxt(k,855)*y(k,130) +1.452*rxt(k,858)*y(k,145) +1.492*rxt(k,862)*y(k,212) +1.872*rxt(k,866)*y(k,293) + &
.708*rxt(k,869)*y(k,99) +2.080*rxt(k,872)*y(k,90) +1.736*rxt(k,876)*y(k,92) +.453*rxt(k,880)*y(k,111) + &
.234*rxt(k,883)*y(k,118) +.915*rxt(k,886)*y(k,148) +.480*rxt(k,889)*y(k,185) +.321*rxt(k,892)*y(k,311) + &
.867*rxt(k,895)*y(k,36) +.999*rxt(k,898)*y(k,46) +.852*rxt(k,902)*y(k,77) +.492*rxt(k,905)*y(k,402) + &
.026*rxt(k,908)*y(k,670) +.252*rxt(k,910)*y(k,103) +.660*rxt(k,913)*y(k,155) +.342*rxt(k,916)*y(k,224) + &
.114*rxt(k,919)*y(k,274) +.237*rxt(k,922)*y(k,318) +1.236*rxt(k,926)*y(k,41) +.369*rxt(k,929)*y(k,69) + &
.399*rxt(k,932)*y(k,405) +.022*rxt(k,935)*y(k,671) +.798*rxt(k,937)*y(k,107) +.174*rxt(k,940)*y(k,229) + &
.327*rxt(k,943)*y(k,230) +.099*rxt(k,946)*y(k,275) +.171*rxt(k,949)*y(k,336) +1.048*rxt(k,953)*y(k,29) + &
.630*rxt(k,956)*y(k,78) +.162*rxt(k,959)*y(k,409) +.020*rxt(k,961)*y(k,672) +.666*rxt(k,963)*y(k,110) + &
1.074*rxt(k,966)*y(k,133) +.366*rxt(k,969)*y(k,231) +.882*rxt(k,972)*y(k,234) +.087*rxt(k,975)*y(k,276) + &
.129*rxt(k,978)*y(k,337) +1.252*rxt(k,982)*y(k,42) +.234*rxt(k,985)*y(k,74) +.118*rxt(k,988)*y(k,411) + &
.018*rxt(k,990)*y(k,673) +.306*rxt(k,992)*y(k,131) +.408*rxt(k,995)*y(k,184) +.324*rxt(k,998)*y(k,235) + &
.180*rxt(k,1001)*y(k,238) +.042*rxt(k,1004)*y(k,277) +.117*rxt(k,1007)*y(k,338) +1.113*rxt(k,1011)*y(k,44) + &
.393*rxt(k,1013)*y(k,79) +.106*rxt(k,1016)*y(k,413) +.016*rxt(k,1018)*y(k,674) +.684*rxt(k,1020)*y(k,152) + &
.258*rxt(k,1023)*y(k,236) +.156*rxt(k,1026)*y(k,239) +.039*rxt(k,1029)*y(k,278) +.108*rxt(k,1032)*y(k,339) + &
1.293*rxt(k,1035)*y(k,31) +.189*rxt(k,1038)*y(k,75) +.098*rxt(k,1041)*y(k,415) +.700*rxt(k,1044)*y(k,40) + &
.016*rxt(k,1047)*y(k,675) +.504*rxt(k,1049)*y(k,158) +.036*rxt(k,1052)*y(k,279) +.210*rxt(k,1055)*y(k,301) + &
.042*rxt(k,1058)*y(k,340) +.978*rxt(k,1061)*y(k,43) +.339*rxt(k,1063)*y(k,80) +.092*rxt(k,1066)*y(k,374) + &
2.838*rxt(k,1083)*y(k,52) +3.380*rxt(k,1131)*y(k,84) +2.792*rxt(k,1143)*y(k,263) +2.763*rxt(k,1176)*y(k,389) + &
2.763*rxt(k,1187)*y(k,788) +2.703*rxt(k,1198)*y(k,457) +1.035*rxt(k,1219)*y(k,49) +2.448*rxt(k,1235)*y(k,72) + &
1.432*rxt(k,1247)*y(k,213) +2.756*rxt(k,1257)*y(k,264) +3.044*rxt(k,1270)*y(k,312) +.009*rxt(k,1292)*y(k,168) + &
2.679*rxt(k,1321)*y(k,181) +2.625*rxt(k,1332)*y(k,386) +5.004*rxt(k,1344)*y(k,391) +2.475*rxt(k,1378)*y(k,798) + &
2.613*rxt(k,1390)*y(k,785) +5.004*rxt(k,1402)*y(k,789) +3.448*rxt(k,1413)*y(k,285) +2.679*rxt(k,1425)*y(k,81) + &
2.529*rxt(k,1436)*y(k,456) +2.860*rxt(k,1447)*y(k,781) +2.481*rxt(k,1461)*y(k,82) +3.232*rxt(k,1473)*y(k,317) + &
2.012*rxt(k,1485)*y(k,71) +2.216*rxt(k,1496)*y(k,215) +2.044*rxt(k,1509)*y(k,265) +.868*rxt(k,1545)*y(k,208) + &
.099*rxt(k,1558)*y(k,178) +2.238*rxt(k,1579)*y(k,398) +2.538*rxt(k,1591)*y(k,141) +3.388*rxt(k,1604)*y(k,182) + &
2.547*rxt(k,1629)*y(k,290) +2.502*rxt(k,1642)*y(k,387) +4.743*rxt(k,1654)*y(k,202) +2.547*rxt(k,1665)*y(k,269) + &
4.689*rxt(k,1699)*y(k,392) +2.238*rxt(k,1710)*y(k,794) +2.499*rxt(k,1722)*y(k,786) +4.671*rxt(k,1734)*y(k,790) + &
.009*rxt(k,1745)*y(k,396) +2.457*rxt(k,1757)*y(k,799) +.009*rxt(k,1770)*y(k,793) +.036*rxt(k,1782)*y(k,26) + &
1.374*rxt(k,1791)*y(k,83) +.021*rxt(k,1801)*y(k,127) +2.343*rxt(k,1825)*y(k,787) +3.236*rxt(k,1837)*y(k,183) + &
1.770*rxt(k,1851)*y(k,400) +4.119*rxt(k,1861)*y(k,782) +3.651*rxt(k,1873)*y(k,783) +3.201*rxt(k,1884)*y(k,791) + &
1.737*rxt(k,1895)*y(k,796) +2.454*rxt(k,1905)*y(k,128) +.016*rxt(k,1916)*y(k,124) +1.251*rxt(k,1928)*y(k,67) + &
2.716*rxt(k,1938)*y(k,291) +2.736*rxt(k,1951)*y(k,797) +1.164*rxt(k,1962)*y(k,53) +.006*rxt(k,1972)*y(k,219) + &
.966*rxt(k,1984)*y(k,404) +2.559*rxt(k,1994)*y(k,795) +1.700*rxt(k,2005)*y(k,249) +3.104*rxt(k,2016)*y(k,355) + &
.184*rxt(k,2031)*y(k,879) +2.156*rxt(k,2046)*y(k,479) +1.200*rxt(k,2060)*y(k,771) +1.044*rxt(k,2071)*y(k,881) + &
1.089*rxt(k,2084)*y(k,55) +2.445*rxt(k,2094)*y(k,800) +1.035*rxt(k,2104)*y(k,56) +2.379*rxt(k,2114)*y(k,801) + &
.996*rxt(k,2124)*y(k,57) +2.313*rxt(k,2134)*y(k,802) +.960*rxt(k,2144)*y(k,58) +2.253*rxt(k,2154)*y(k,803) + &
2.199*rxt(k,2164)*y(k,804) +.270*rxt(k,2519)*y(k,831) +.024*rxt(k,2522)*y(k,741) +.093*rxt(k,2529)*y(k,677) + &
.624*rxt(k,2531)*y(k,346) +.189*rxt(k,2539)*y(k,39) +.042*rxt(k,2542)*y(k,508) +.006*rxt(k,2551)*y(k,729) + &
.090*rxt(k,2555)*y(k,636) +.099*rxt(k,2565)*y(k,447) +2.583*rxt(k,2579)*y(k,826) +.076*rxt(k,2583)*y(k,18) + &
.012*rxt(k,2587)*y(k,192) +.219*rxt(k,2591)*y(k,59) +.044*rxt(k,2595)*y(k,575) +.729*rxt(k,2599)*y(k,679) + &
.099*rxt(k,2602)*y(k,773) +2.172*rxt(k,2604)*y(k,528) +.237*rxt(k,2628)*y(k,837) +.009*rxt(k,2647)*y(k,48) + &
.288*rxt(k,2650)*y(k,500) +.956*rxt(k,2653)*y(k,641) +.363*rxt(k,2657)*y(k,394) +.048*rxt(k,2661)*y(k,583) + &
.513*rxt(k,2664)*y(k,570) +.237*rxt(k,2667)*y(k,368) +.084*rxt(k,2674)*y(k,35) +.033*rxt(k,2678)*y(k,20) + &
.729*rxt(k,2680)*y(k,45) +.057*rxt(k,2683)*y(k,112) +.320*rxt(k,2690)*y(k,518) +.051*rxt(k,2694)*y(k,199) + &
.270*rxt(k,2697)*y(k,286) +.105*rxt(k,2699)*y(k,710) +.104*rxt(k,2709)*y(k,592) +1.872*rxt(k,2715)*y(k,419) + &
.990*rxt(k,2718)*y(k,420) +3.388*rxt(k,2722)*y(k,354) +2.106*rxt(k,2725)*y(k,828) +.933*rxt(k,2728)*y(k,595) + &
.003*rxt(k,2738)*y(k,94) +.144*rxt(k,2743)*y(k,270) +.348*rxt(k,2751)*y(k,60) +.609*rxt(k,2756)*y(k,659) + &
1.125*rxt(k,2758)*y(k,464) +.306*rxt(k,2762)*y(k,649) +.564*rxt(k,2765)*y(k,164) +.567*rxt(k,2768)*y(k,247) + &
1.203*rxt(k,2772)*y(k,407) +.664*rxt(k,2775)*y(k,563) +.200*rxt(k,2779)*y(k,190) +.012*rxt(k,2782)*y(k,523) + &
2.160*rxt(k,2786)*y(k,654) +.120*rxt(k,2809)*y(k,531) +1.080*rxt(k,2816)*y(k,825) +.596*rxt(k,2819)*y(k,526) + &
.396*rxt(k,2828)*y(k,610) +.324*rxt(k,2832)*y(k,624) +.264*rxt(k,2835)*y(k,403) +.200*rxt(k,2840)*y(k,726) + &
.088*rxt(k,2844)*y(k,268) +.416*rxt(k,2858)*y(k,260) +.148*rxt(k,2862)*y(k,287) +.012*rxt(k,2865)*y(k,603) + &
.195*rxt(k,2870)*y(k,513) +.264*rxt(k,2875)*y(k,648) +.392*rxt(k,2879)*y(k,657) +.582*rxt(k,2886)*y(k,421) + &
.738*rxt(k,2889)*y(k,422) +.309*rxt(k,2893)*y(k,62) +.384*rxt(k,2897)*y(k,646) +2.600*rxt(k,2901)*y(k,655) + &
.063*rxt(k,2904)*y(k,663) +.792*rxt(k,2908)*y(k,61) +.450*rxt(k,2910)*y(k,165) +.664*rxt(k,2913)*y(k,647) + &
.042*rxt(k,2917)*y(k,740) +1.473*rxt(k,2920)*y(k,498) +.004*rxt(k,2923)*y(k,584) +.372*rxt(k,2939)*y(k,505) + &
.060*rxt(k,2943)*y(k,566) +.124*rxt(k,2946)*y(k,642) +.692*rxt(k,2950)*y(k,361) +.340*rxt(k,2955)*y(k,743) + &
.192*rxt(k,2959)*y(k,772) +2.000*rxt(k,2965)*y(k,474) +1.504*rxt(k,2969)*y(k,615) +.420*rxt(k,2973)*y(k,410) + &
.564*rxt(k,2977)*y(k,179) +.048*rxt(k,2980)*y(k,493) +.780*rxt(k,2991)*y(k,366) +.056*rxt(k,2995)*y(k,289) + &
.208*rxt(k,2999)*y(k,653) +.168*rxt(k,3004)*y(k,730) +.596*rxt(k,3008)*y(k,554) +.020*rxt(k,3020)*y(k,724) + &
.220*rxt(k,3025)*y(k,449) +2.480*rxt(k,3032)*y(k,640) +.308*rxt(k,3037)*y(k,470) +.064*rxt(k,3043)*y(k,459) + &
3.456*rxt(k,3050)*y(k,347) +.414*rxt(k,3054)*y(k,811) +.354*rxt(k,3064)*y(k,64) +.210*rxt(k,3069)*y(k,200) + &
1.770*rxt(k,3073)*y(k,412) +.483*rxt(k,3075)*y(k,186) +.285*rxt(k,3078)*y(k,476) +2.505*rxt(k,3081)*y(k,332) + &
2.076*rxt(k,3085)*y(k,266) +.567*rxt(k,3088)*y(k,63) +.484*rxt(k,3092)*y(k,121) +.092*rxt(k,3095)*y(k,294) + &
.064*rxt(k,3100)*y(k,668) +.716*rxt(k,3122)*y(k,367) +.642*rxt(k,3126)*y(k,352) +.248*rxt(k,3130)*y(k,727) + &
2.272*rxt(k,3133)*y(k,562) +.128*rxt(k,3138)*y(k,175) +1.496*rxt(k,3141)*y(k,616) +.003*rxt(k,3150)*y(k,723) + &
.606*rxt(k,3152)*y(k,365) +.224*rxt(k,3156)*y(k,492) +.260*rxt(k,3160)*y(k,478) +.340*rxt(k,3164)*y(k,473) + &
.452*rxt(k,3172)*y(k,489) +.016*rxt(k,3176)*y(k,27) +.240*rxt(k,3183)*y(k,823) +2.932*rxt(k,3187)*y(k,162) + &
.447*rxt(k,3190)*y(k,66) +.675*rxt(k,3194)*y(k,414) +.585*rxt(k,3197)*y(k,65) +.906*rxt(k,3201)*y(k,170) + &
.594*rxt(k,3203)*y(k,166) +1.026*rxt(k,3206)*y(k,248) +.954*rxt(k,3209)*y(k,309) +1.212*rxt(k,3213)*y(k,73) + &
.018*rxt(k,3216)*y(k,565) +2.211*rxt(k,3219)*y(k,364) +.016*rxt(k,3228)*y(k,363) +.168*rxt(k,3254)*y(k,125) + &
1.744*rxt(k,3258)*y(k,187) +1.544*rxt(k,3261)*y(k,271) +.768*rxt(k,3265)*y(k,313) +.170*rxt(k,3270)*y(k,568) + &
.544*rxt(k,3274)*y(k,362) +.174*rxt(k,3278)*y(k,682) +1.596*rxt(k,3282)*y(k,593) +.627*rxt(k,3285)*y(k,408) + &
.954*rxt(k,3288)*y(k,171) +.225*rxt(k,3291)*y(k,617) +.920*rxt(k,3295)*y(k,167) +.747*rxt(k,3299)*y(k,495) + &
.572*rxt(k,3302)*y(k,832) +1.464*rxt(k,3309)*y(k,161) +.004*rxt(k,3313)*y(k,174) +.928*rxt(k,3318)*y(k,468) + &
.388*rxt(k,3322)*y(k,485) +3.516*rxt(k,3325)*y(k,461) +.036*rxt(k,3330)*y(k,471) +.216*rxt(k,3334)*y(k,821) + &
.360*rxt(k,3338)*y(k,813) +1.365*rxt(k,3341)*y(k,205) +1.158*rxt(k,3344)*y(k,323) +2.880*rxt(k,3347)*y(k,68) + &
.900*rxt(k,3351)*y(k,830) +.447*rxt(k,3354)*y(k,416) +.492*rxt(k,3358)*y(k,475) +1.644*rxt(k,3361)*y(k,156) + &
1.581*rxt(k,3364)*y(k,157) +1.032*rxt(k,3368)*y(k,142) +.564*rxt(k,3372)*y(k,188) +1.352*rxt(k,3375)*y(k,254) + &
1.204*rxt(k,3379)*y(k,272) +.528*rxt(k,3383)*y(k,314) +.328*rxt(k,3387)*y(k,333) +1.796*rxt(k,3391)*y(k,576) + &
.027*rxt(k,3395)*y(k,684) +.051*rxt(k,3398)*y(k,618) +.632*rxt(k,3401)*y(k,51) +.044*rxt(k,3406)*y(k,488) + &
.200*rxt(k,3418)*y(k,824) +.164*rxt(k,3425)*y(k,815) +.105*rxt(k,3431)*y(k,417) +2.844*rxt(k,3434)*y(k,356) + &
3.500*rxt(k,3448)*y(k,436) +.732*rxt(k,3463)*y(k,557) +1.492*rxt(k,3466)*y(k,632) +1.911*rxt(k,3470)*y(k,587) + &
.261*rxt(k,3482)*y(k,375) +.405*rxt(k,3485)*y(k,571) +.357*rxt(k,3488)*y(k,54) +1.212*rxt(k,3492)*y(k,237) + &
1.812*rxt(k,3495)*y(k,406) +.512*rxt(k,3503)*y(k,143) +.508*rxt(k,3506)*y(k,173) +.340*rxt(k,3510)*y(k,220) + &
1.356*rxt(k,3514)*y(k,226) +.828*rxt(k,3518)*y(k,255) +.237*rxt(k,3521)*y(k,273) +.448*rxt(k,3524)*y(k,295) + &
.340*rxt(k,3528)*y(k,315) +.228*rxt(k,3533)*y(k,334) +.024*rxt(k,3536)*y(k,686) +1.890*rxt(k,3540)*y(k,529) + &
.027*rxt(k,3556)*y(k,619) +.168*rxt(k,3560)*y(k,494) +.603*rxt(k,3563)*y(k,472) +.528*rxt(k,3566)*y(k,462) + &
.736*rxt(k,3570)*y(k,469) +.648*rxt(k,3574)*y(k,820) +.108*rxt(k,3582)*y(k,172) +.152*rxt(k,3592)*y(k,108) + &
.152*rxt(k,3596)*y(k,126) +1.208*rxt(k,3600)*y(k,144) +.352*rxt(k,3604)*y(k,149) +.300*rxt(k,3607)*y(k,189) + &
1.024*rxt(k,3611)*y(k,227) +1.004*rxt(k,3615)*y(k,232) +.222*rxt(k,3619)*y(k,256) +.172*rxt(k,3622)*y(k,296) + &
.700*rxt(k,3626)*y(k,297) +.189*rxt(k,3630)*y(k,316) +.340*rxt(k,3633)*y(k,335) +.021*rxt(k,3637)*y(k,688) + &
.024*rxt(k,3640)*y(k,611) +1.324*rxt(k,3644)*y(k,564) +1.134*rxt(k,3647)*y(k,151) +.748*rxt(k,3650)*y(k,233) + &
.198*rxt(k,3654)*y(k,261) +.768*rxt(k,3657)*y(k,298) +.021*rxt(k,3661)*y(k,612) +.644*rxt(k,3665)*y(k,818) + &
1.145*rxt(k,3669)*y(k,817) +.368*rxt(k,3674)*y(k,467) +1.380*rxt(k,3686)*y(k,477) +.484*rxt(k,3690)*y(k,814) + &
.260*rxt(k,3693)*y(k,812) +1.452*rxt(k,3697)*y(k,222) +.556*rxt(k,3701)*y(k,253) +.328*rxt(k,3705)*y(k,300) + &
.018*rxt(k,3709)*y(k,627) +.364*rxt(k,3713)*y(k,822) +.564*rxt(k,3717)*y(k,106) +1.112*rxt(k,3720)*y(k,223) + &
.864*rxt(k,3724)*y(k,228) +.015*rxt(k,3728)*y(k,613) +.505*rxt(k,3731)*y(k,169) +.992*rxt(k,3737)*y(k,132) + &
.183*rxt(k,3740)*y(k,252) +.408*rxt(k,3743)*y(k,299) +.015*rxt(k,3747)*y(k,629) +2.232*rxt(k,3750)*y(k,588) + &
.129*rxt(k,3760)*y(k,614) +.184*rxt(k,3764)*y(k,105) +.852*rxt(k,3767)*y(k,221) +.488*rxt(k,3771)*y(k,331) + &
.130*rxt(k,3781)*y(k,836) +3.429*rxt(k,3785)*y(k,652) +2.718*rxt(k,3798)*y(k,651) +.078*rxt(k,3913)*y(k,250) + &
.258*rxt(k,3916)*y(k,397) +2.000*rxt(k,3917)*y(k,203) +1.845*rxt(k,3971)*y(k,390) +1.605*rxt(k,4003)*y(k,480) + &
.045*rxt(k,4024)*y(k,353) +1.302*rxt(k,4029)*y(k,401) +1.302*rxt(k,4054)*y(k,399) +.048*rxt(k,4060)*y(k,160) + &
1.275*rxt(k,4064)*y(k,689) +1.378*rxt(k,4075)*y(k,810) +.038*rxt(k,4116)*y(k,524) +1.536*rxt(k,4134)*y(k,658) + &
1.832*rxt(k,4162)*y(k,720) +1.557*rxt(k,4179)*y(k,732) +.256*rxt(k,4182)*y(k,733) +1.388*rxt(k,4186)*y(k,734) + &
1.575*rxt(k,4189)*y(k,735) +1.665*rxt(k,4192)*y(k,736) +.032*rxt(k,4203)*y(k,764) +.032*rxt(k,4207)*y(k,765) + &
.788*rxt(k,4210)*y(k,766) +.004*rxt(k,4214)*y(k,767))*y(k,705) + (2.000*rxt(k,238)*y(k,512) + &
.872*rxt(k,570)*y(k,582) +.048*rxt(k,1139)*y(k,84) +1.413*rxt(k,1183)*y(k,389) +1.413*rxt(k,1194)*y(k,788) + &
2.205*rxt(k,1204)*y(k,457) +.468*rxt(k,1310)*y(k,177) +2.175*rxt(k,1328)*y(k,181) +.381*rxt(k,1340)*y(k,386) + &
.432*rxt(k,1350)*y(k,391) +.204*rxt(k,1386)*y(k,798) +.381*rxt(k,1398)*y(k,785) +.432*rxt(k,1408)*y(k,789) + &
1.768*rxt(k,1420)*y(k,285) +2.616*rxt(k,1432)*y(k,81) +.888*rxt(k,1443)*y(k,456) +1.280*rxt(k,1456)*y(k,781) + &
2.508*rxt(k,1468)*y(k,82) +1.768*rxt(k,1480)*y(k,317) +.009*rxt(k,1518)*y(k,265) +.332*rxt(k,1529)*y(k,136) + &
.066*rxt(k,1565)*y(k,178) +.152*rxt(k,1587)*y(k,398) +2.456*rxt(k,1599)*y(k,141) +.524*rxt(k,1613)*y(k,182) + &
2.772*rxt(k,1637)*y(k,290) +.039*rxt(k,1650)*y(k,387) +.345*rxt(k,1661)*y(k,202) +2.772*rxt(k,1673)*y(k,269) + &
.162*rxt(k,1706)*y(k,392) +.152*rxt(k,1718)*y(k,794) +.039*rxt(k,1730)*y(k,786) +.162*rxt(k,1741)*y(k,790) + &
.123*rxt(k,1753)*y(k,396) +.172*rxt(k,1765)*y(k,799) +.123*rxt(k,1778)*y(k,793) +.192*rxt(k,1808)*y(k,127) + &
.030*rxt(k,1833)*y(k,787) +1.704*rxt(k,1846)*y(k,183) +.092*rxt(k,1869)*y(k,782) +.066*rxt(k,1880)*y(k,783) + &
.039*rxt(k,1891)*y(k,791) +1.332*rxt(k,1912)*y(k,128) +.933*rxt(k,1947)*y(k,291) +.030*rxt(k,1958)*y(k,797) + &
1.052*rxt(k,1979)*y(k,219) +.030*rxt(k,2001)*y(k,795) +2.976*rxt(k,2011)*y(k,249) +2.736*rxt(k,2027)*y(k,355) + &
1.032*rxt(k,2041)*y(k,879) +2.564*rxt(k,2056)*y(k,479) +.027*rxt(k,2067)*y(k,771) +.364*rxt(k,2079)*y(k,881) + &
.004*rxt(k,2747)*y(k,270) +.108*rxt(k,3106)*y(k,668) +.108*rxt(k,3236)*y(k,363) +2.262*rxt(k,3443)*y(k,356) + &
.336*rxt(k,3457)*y(k,436) +.252*rxt(k,3478)*y(k,587) +.275*rxt(k,3551)*y(k,529) +.207*rxt(k,3587)*y(k,172) + &
.416*rxt(k,3793)*y(k,652) +.044*rxt(k,3806)*y(k,651) +1.662*rxt(k,4009)*y(k,480) +2.067*rxt(k,4080)*y(k,810) + &
1.440*rxt(k,4137)*y(k,658) +1.617*rxt(k,4166)*y(k,720))*y(k,693) + (.244*rxt(k,1136)*y(k,84) + &
.232*rxt(k,1240)*y(k,72) +.190*rxt(k,1275)*y(k,312) +2.224*rxt(k,1307)*y(k,177) +.305*rxt(k,1336)*y(k,386) + &
.305*rxt(k,1394)*y(k,785) +.008*rxt(k,1417)*y(k,285) +2.396*rxt(k,1429)*y(k,81) +.436*rxt(k,1440)*y(k,456) + &
2.284*rxt(k,1465)*y(k,82) +.404*rxt(k,1478)*y(k,317) +.220*rxt(k,1490)*y(k,71) +.010*rxt(k,1514)*y(k,265) + &
2.148*rxt(k,1526)*y(k,136) +2.148*rxt(k,1562)*y(k,178) +.185*rxt(k,1609)*y(k,182) +.290*rxt(k,1646)*y(k,387) + &
.244*rxt(k,1703)*y(k,392) +.290*rxt(k,1726)*y(k,786) +.244*rxt(k,1738)*y(k,790) +2.920*rxt(k,1750)*y(k,396) + &
2.920*rxt(k,1775)*y(k,793) +2.214*rxt(k,1785)*y(k,26) +.204*rxt(k,1795)*y(k,83) +2.060*rxt(k,1805)*y(k,127) + &
.275*rxt(k,1829)*y(k,787) +.175*rxt(k,1842)*y(k,183) +.492*rxt(k,1855)*y(k,400) +.232*rxt(k,1888)*y(k,791) + &
.492*rxt(k,1899)*y(k,796) +1.251*rxt(k,1920)*y(k,124) +.188*rxt(k,1932)*y(k,67) +.230*rxt(k,1943)*y(k,291) + &
.476*rxt(k,1955)*y(k,797) +.148*rxt(k,1966)*y(k,53) +.464*rxt(k,1988)*y(k,404) +.464*rxt(k,1998)*y(k,795) + &
2.020*rxt(k,2008)*y(k,249) +1.440*rxt(k,2022)*y(k,355) +.036*rxt(k,2036)*y(k,879) +2.500*rxt(k,2051)*y(k,479) + &
.855*rxt(k,2075)*y(k,881) +.132*rxt(k,2088)*y(k,55) +.452*rxt(k,2098)*y(k,800) +.124*rxt(k,2108)*y(k,56) + &
.436*rxt(k,2118)*y(k,801) +.116*rxt(k,2128)*y(k,57) +.420*rxt(k,2138)*y(k,802) +.116*rxt(k,2148)*y(k,58) + &
.380*rxt(k,2158)*y(k,803) +.364*rxt(k,2168)*y(k,804) +2.510*rxt(k,3439)*y(k,356) +.150*rxt(k,3454)*y(k,436) + &
.030*rxt(k,3475)*y(k,587) +1.548*rxt(k,3546)*y(k,529) +.372*rxt(k,3754)*y(k,588) +.065*rxt(k,3789)*y(k,652) + &
.240*rxt(k,3802)*y(k,651) +1.728*rxt(k,4006)*y(k,480) +2.067*rxt(k,4077)*y(k,810))*y(k,696) +2.960*rxt(k,2) &
*y(k,59) +2.793*rxt(k,5)*y(k,60) +2.622*rxt(k,8)*y(k,62) +2.442*rxt(k,11)*y(k,64) +2.244*rxt(k,14)*y(k,66) &
 +1.960*rxt(k,21)*y(k,186) +.087*rxt(k,28)*y(k,266) +1.839*rxt(k,31)*y(k,270) +1.762*rxt(k,34)*y(k,332) &
 +1.748*rxt(k,45)*y(k,412) +1.628*rxt(k,47)*y(k,414) +1.866*rxt(k,80)*y(k,557) +1.839*rxt(k,97)*y(k,646) &
 +1.862*rxt(k,106)*y(k,655) +1.960*rxt(k,108)*y(k,659) +2.340*rxt(k,127)*y(k,731) +1.974*rxt(k,130)*y(k,732) &
 +.639*rxt(k,132)*y(k,733) +1.860*rxt(k,135)*y(k,734) +2.622*rxt(k,137)*y(k,735) +1.796*rxt(k,140)*y(k,736) &
 +.232*rxt(k,145)*y(k,755) +.315*rxt(k,154)*y(k,761) +1.516*rxt(k,168)*y(k,766)
         loss(k,769) = (rxt(k,4472)* y(k,370) +rxt(k,4468)* y(k,548) +rxt(k,4473)* y(k,589) +rxt(k,4470)* y(k,620) +rxt(k,4474) &
* y(k,633) +rxt(k,4467)* y(k,690) +rxt(k,4469)* y(k,693) +rxt(k,4471)* y(k,758) +rxt(k,4475)* y(k,768) &
 +rxt(k,4476)* y(k,769))* y(k,868)
         prod(k,769) = (1.766*rxt(k,253)*y(k,451) +.303*rxt(k,277)*y(k,454) +.222*rxt(k,306)*y(k,625) +.245*rxt(k,572)*y(k,731) + &
.006*rxt(k,616)*y(k,581) +.006*rxt(k,619)*y(k,621) +.148*rxt(k,645)*y(k,11) +.232*rxt(k,649)*y(k,24) + &
.004*rxt(k,653)*y(k,37) +.840*rxt(k,657)*y(k,506) +.708*rxt(k,707)*y(k,5) +.040*rxt(k,711)*y(k,7) + &
.088*rxt(k,719)*y(k,15) +.052*rxt(k,723)*y(k,17) +.608*rxt(k,727)*y(k,76) +.620*rxt(k,744)*y(k,728) + &
.309*rxt(k,809)*y(k,558) +.028*rxt(k,812)*y(k,13) +.003*rxt(k,827)*y(k,393) +.006*rxt(k,897)*y(k,46) + &
.016*rxt(k,900)*y(k,77) +.008*rxt(k,924)*y(k,41) +.008*rxt(k,951)*y(k,29) +.008*rxt(k,980)*y(k,42) + &
.003*rxt(k,1009)*y(k,44) +.003*rxt(k,1034)*y(k,31) +.008*rxt(k,1042)*y(k,40) +.003*rxt(k,1059)*y(k,43) + &
.057*rxt(k,1435)*y(k,456) +.024*rxt(k,1460)*y(k,82) +.032*rxt(k,1471)*y(k,317) +.006*rxt(k,1781)*y(k,26) + &
.016*rxt(k,2015)*y(k,355) +.020*rxt(k,2030)*y(k,879) +.012*rxt(k,2045)*y(k,479) +1.197*rxt(k,2059)*y(k,771) + &
1.724*rxt(k,2518)*y(k,831) +2.124*rxt(k,2538)*y(k,39) +.462*rxt(k,2541)*y(k,508) +.102*rxt(k,2546)*y(k,550) + &
1.854*rxt(k,2576)*y(k,418) +.027*rxt(k,2578)*y(k,826) +.552*rxt(k,2581)*y(k,18) +.249*rxt(k,2603)*y(k,528) + &
.040*rxt(k,2617)*y(k,606) +.030*rxt(k,2627)*y(k,837) +.126*rxt(k,2646)*y(k,48) +.128*rxt(k,2652)*y(k,641) + &
2.367*rxt(k,2656)*y(k,394) +.496*rxt(k,2659)*y(k,583) +.042*rxt(k,2669)*y(k,634) +.774*rxt(k,2714)*y(k,419) + &
.012*rxt(k,2720)*y(k,354) +.003*rxt(k,2724)*y(k,828) +.056*rxt(k,2741)*y(k,270) +.180*rxt(k,2749)*y(k,60) + &
.060*rxt(k,2754)*y(k,659) +1.677*rxt(k,2757)*y(k,464) +.111*rxt(k,2760)*y(k,649) +.081*rxt(k,2807)*y(k,531) + &
.792*rxt(k,2811)*y(k,135) +.008*rxt(k,2818)*y(k,526) +.060*rxt(k,2826)*y(k,610) +2.184*rxt(k,2830)*y(k,624) + &
2.296*rxt(k,2834)*y(k,403) +1.976*rxt(k,2838)*y(k,726) +.448*rxt(k,2864)*y(k,603) +1.308*rxt(k,2885)*y(k,421) + &
.150*rxt(k,2891)*y(k,62) +.044*rxt(k,2895)*y(k,646) +.056*rxt(k,2899)*y(k,655) +.165*rxt(k,2903)*y(k,663) + &
.008*rxt(k,2937)*y(k,505) +.204*rxt(k,2941)*y(k,566) +1.852*rxt(k,2945)*y(k,642) +.760*rxt(k,2949)*y(k,361) + &
1.864*rxt(k,2953)*y(k,743) +.020*rxt(k,2957)*y(k,772) +.252*rxt(k,2964)*y(k,474) +.004*rxt(k,2998)*y(k,653) + &
1.228*rxt(k,3006)*y(k,554) +.016*rxt(k,3019)*y(k,724) +.124*rxt(k,3023)*y(k,449) +.388*rxt(k,3031)*y(k,640) + &
.064*rxt(k,3042)*y(k,459) +2.478*rxt(k,3046)*y(k,509) +.132*rxt(k,3063)*y(k,64) +.135*rxt(k,3067)*y(k,200) + &
.033*rxt(k,3071)*y(k,412) +1.617*rxt(k,3074)*y(k,186) +2.229*rxt(k,3077)*y(k,476) +.033*rxt(k,3080)*y(k,332) + &
.044*rxt(k,3083)*y(k,266) +.896*rxt(k,3098)*y(k,668) +.196*rxt(k,3109)*y(k,567) +.760*rxt(k,3121)*y(k,367) + &
.039*rxt(k,3125)*y(k,352) +1.452*rxt(k,3128)*y(k,727) +.404*rxt(k,3132)*y(k,562) +.076*rxt(k,3136)*y(k,175) + &
.120*rxt(k,3174)*y(k,27) +.032*rxt(k,3185)*y(k,162) +.078*rxt(k,3189)*y(k,66) +.027*rxt(k,3193)*y(k,414) + &
.208*rxt(k,3226)*y(k,363) +.056*rxt(k,3239)*y(k,569) +.056*rxt(k,3252)*y(k,125) +.024*rxt(k,3256)*y(k,187) + &
.040*rxt(k,3260)*y(k,271) +.812*rxt(k,3264)*y(k,313) +.335*rxt(k,3268)*y(k,568) +.632*rxt(k,3273)*y(k,362) + &
.024*rxt(k,3277)*y(k,682) +.024*rxt(k,3280)*y(k,593) +.042*rxt(k,3284)*y(k,408) +.076*rxt(k,3308)*y(k,161) + &
.056*rxt(k,3328)*y(k,471) +.330*rxt(k,3340)*y(k,205) +1.323*rxt(k,3343)*y(k,323) +.188*rxt(k,3346)*y(k,68) + &
2.740*rxt(k,3356)*y(k,475) +.064*rxt(k,3366)*y(k,142) +.016*rxt(k,3370)*y(k,188) +.016*rxt(k,3374)*y(k,254) + &
.024*rxt(k,3378)*y(k,272) +.740*rxt(k,3382)*y(k,314) +.024*rxt(k,3386)*y(k,333) +.468*rxt(k,3390)*y(k,576) + &
.018*rxt(k,3394)*y(k,684) +.879*rxt(k,3427)*y(k,534) +1.590*rxt(k,3430)*y(k,417) +.024*rxt(k,3433)*y(k,356) + &
.080*rxt(k,3461)*y(k,557) +.060*rxt(k,3501)*y(k,143) +.012*rxt(k,3505)*y(k,173) +.132*rxt(k,3509)*y(k,220) + &
.020*rxt(k,3513)*y(k,226) +.009*rxt(k,3517)*y(k,255) +.015*rxt(k,3520)*y(k,273) +.316*rxt(k,3523)*y(k,295) + &
.592*rxt(k,3527)*y(k,315) +.020*rxt(k,3531)*y(k,334) +.015*rxt(k,3535)*y(k,686) +.048*rxt(k,3568)*y(k,469) + &
.024*rxt(k,3580)*y(k,172) +.012*rxt(k,3590)*y(k,108) +.012*rxt(k,3594)*y(k,126) +.048*rxt(k,3598)*y(k,144) + &
.012*rxt(k,3602)*y(k,149) +.012*rxt(k,3606)*y(k,189) +.016*rxt(k,3610)*y(k,227) +.016*rxt(k,3614)*y(k,232) + &
.006*rxt(k,3618)*y(k,256) +.188*rxt(k,3621)*y(k,296) +.504*rxt(k,3625)*y(k,297) +.273*rxt(k,3629)*y(k,316) + &
.016*rxt(k,3632)*y(k,335) +.012*rxt(k,3636)*y(k,688) +1.532*rxt(k,3642)*y(k,564) +.016*rxt(k,3649)*y(k,233) + &
.009*rxt(k,3653)*y(k,261) +.308*rxt(k,3656)*y(k,298) +.012*rxt(k,3663)*y(k,818) +.020*rxt(k,3667)*y(k,817) + &
1.060*rxt(k,3676)*y(k,466) +.012*rxt(k,3696)*y(k,222) +.008*rxt(k,3700)*y(k,253) +.456*rxt(k,3704)*y(k,300) + &
.008*rxt(k,3715)*y(k,106) +.012*rxt(k,3719)*y(k,223) +.012*rxt(k,3723)*y(k,228) +.015*rxt(k,3730)*y(k,169) + &
.024*rxt(k,3735)*y(k,132) +.006*rxt(k,3739)*y(k,252) +.380*rxt(k,3742)*y(k,299) +.008*rxt(k,3762)*y(k,105) + &
.012*rxt(k,3766)*y(k,221) +.012*rxt(k,3770)*y(k,331) +.420*rxt(k,3774)*y(k,463) +.110*rxt(k,3779)*y(k,836) + &
.030*rxt(k,4177)*y(k,732) +.376*rxt(k,4184)*y(k,734) +.225*rxt(k,4188)*y(k,735) +.198*rxt(k,4191)*y(k,736)) &
*y(k,705) + (1.956*rxt(k,523)*y(k,348) +1.956*rxt(k,530)*y(k,349) +.978*rxt(k,540)*y(k,350) + &
.300*rxt(k,547)*y(k,590) +.200*rxt(k,556)*y(k,664) +.192*rxt(k,564)*y(k,582) +.328*rxt(k,1157)*y(k,176) + &
.360*rxt(k,1200)*y(k,457) +.168*rxt(k,1284)*y(k,120) +2.492*rxt(k,1294)*y(k,168) +.268*rxt(k,1305)*y(k,177) + &
.435*rxt(k,1357)*y(k,395) +.435*rxt(k,1369)*y(k,792) +.448*rxt(k,1415)*y(k,285) +.272*rxt(k,1428)*y(k,81) + &
.260*rxt(k,1463)*y(k,82) +.004*rxt(k,1475)*y(k,317) +.260*rxt(k,1524)*y(k,136) +.132*rxt(k,1536)*y(k,113) + &
.260*rxt(k,1560)*y(k,178) +2.408*rxt(k,1571)*y(k,101) +2.620*rxt(k,1620)*y(k,257) +.435*rxt(k,1632)*y(k,290) + &
.435*rxt(k,1668)*y(k,269) +.220*rxt(k,1681)*y(k,216) +.186*rxt(k,1691)*y(k,123) +.350*rxt(k,1748)*y(k,396) + &
.350*rxt(k,1773)*y(k,793) +.267*rxt(k,1784)*y(k,26) +.248*rxt(k,1803)*y(k,127) +2.308*rxt(k,1816)*y(k,267) + &
.150*rxt(k,1919)*y(k,124) +2.316*rxt(k,1974)*y(k,219) +.232*rxt(k,2007)*y(k,249) +1.782*rxt(k,2020)*y(k,355) + &
.400*rxt(k,2034)*y(k,879) +.290*rxt(k,2050)*y(k,479) +.152*rxt(k,2062)*y(k,771) +1.205*rxt(k,2074)*y(k,881) + &
1.528*rxt(k,2609)*y(k,591) +1.468*rxt(k,2799)*y(k,628) +1.428*rxt(k,2929)*y(k,514) +1.324*rxt(k,3230)*y(k,363) + &
1.324*rxt(k,3243)*y(k,569) +.290*rxt(k,3438)*y(k,356) +.186*rxt(k,3544)*y(k,529))*y(k,696) &
 + (.096*rxt(k,1419)*y(k,285) +.028*rxt(k,2025)*y(k,355) +.236*rxt(k,2040)*y(k,879) +.008*rxt(k,2054)*y(k,479) + &
.102*rxt(k,2623)*y(k,606) +2.691*rxt(k,2632)*y(k,837) +1.962*rxt(k,2803)*y(k,628) +.234*rxt(k,2933)*y(k,514) + &
.212*rxt(k,3014)*y(k,554) +.076*rxt(k,3104)*y(k,668) +.010*rxt(k,3115)*y(k,567) +.932*rxt(k,3234)*y(k,363) + &
.220*rxt(k,3247)*y(k,569))*y(k,693) +1.922*rxt(k,64)*y(k,531)
         loss(k,836) = (rxt(k,4492)* y(k,370) +rxt(k,4488)* y(k,548) +rxt(k,4493)* y(k,589) +rxt(k,4490)* y(k,620) +rxt(k,4494) &
* y(k,633) +rxt(k,4487)* y(k,690) +rxt(k,4489)* y(k,693) +rxt(k,4491)* y(k,758) +rxt(k,4495)* y(k,768) &
 +rxt(k,4496)* y(k,769))* y(k,869)
         prod(k,836) = (.645*rxt(k,296)*y(k,784) +2.288*rxt(k,570)*y(k,582) +2.760*rxt(k,1090)*y(k,52) + &
.645*rxt(k,1107)*y(k,385) +1.065*rxt(k,1125)*y(k,33) +2.502*rxt(k,1139)*y(k,84) +2.652*rxt(k,1151)*y(k,263) + &
1.185*rxt(k,1183)*y(k,389) +1.185*rxt(k,1194)*y(k,788) +.192*rxt(k,1204)*y(k,457) +1.389*rxt(k,1216)*y(k,780) + &
2.652*rxt(k,1226)*y(k,49) +1.526*rxt(k,1243)*y(k,72) +1.690*rxt(k,1254)*y(k,213) +2.478*rxt(k,1266)*y(k,264) + &
1.536*rxt(k,1279)*y(k,312) +2.679*rxt(k,1300)*y(k,168) +2.010*rxt(k,1310)*y(k,177) +.477*rxt(k,1329)*y(k,181) + &
1.977*rxt(k,1340)*y(k,386) +2.124*rxt(k,1350)*y(k,391) +.090*rxt(k,1362)*y(k,395) +.090*rxt(k,1374)*y(k,792) + &
2.220*rxt(k,1387)*y(k,798) +1.977*rxt(k,1398)*y(k,785) +2.124*rxt(k,1408)*y(k,789) +.468*rxt(k,1421)*y(k,285) + &
.033*rxt(k,1432)*y(k,81) +1.617*rxt(k,1443)*y(k,456) +2.104*rxt(k,1457)*y(k,781) +.066*rxt(k,1468)*y(k,82) + &
1.432*rxt(k,1481)*y(k,317) +1.400*rxt(k,1493)*y(k,71) +2.301*rxt(k,1505)*y(k,215) +2.136*rxt(k,1519)*y(k,265) + &
2.216*rxt(k,1530)*y(k,136) +.063*rxt(k,1541)*y(k,113) +2.427*rxt(k,1555)*y(k,208) +2.136*rxt(k,1566)*y(k,178) + &
2.860*rxt(k,1587)*y(k,398) +.712*rxt(k,1600)*y(k,141) +2.368*rxt(k,1614)*y(k,182) +.168*rxt(k,1625)*y(k,257) + &
.604*rxt(k,1638)*y(k,290) +2.067*rxt(k,1650)*y(k,387) +2.064*rxt(k,1661)*y(k,202) +.604*rxt(k,1674)*y(k,269) + &
2.265*rxt(k,1687)*y(k,216) +2.046*rxt(k,1706)*y(k,392) +2.860*rxt(k,1718)*y(k,794) +2.067*rxt(k,1730)*y(k,786) + &
2.046*rxt(k,1741)*y(k,790) +.540*rxt(k,1754)*y(k,396) +2.788*rxt(k,1766)*y(k,799) +.540*rxt(k,1779)*y(k,793) + &
1.290*rxt(k,1798)*y(k,83) +.696*rxt(k,1809)*y(k,127) +2.481*rxt(k,1821)*y(k,267) +1.911*rxt(k,1833)*y(k,787) + &
2.248*rxt(k,1847)*y(k,183) +1.290*rxt(k,1858)*y(k,400) +2.780*rxt(k,1869)*y(k,782) +2.115*rxt(k,1880)*y(k,783) + &
1.920*rxt(k,1891)*y(k,791) +1.290*rxt(k,1902)*y(k,796) +.447*rxt(k,1913)*y(k,128) +.012*rxt(k,1924)*y(k,124) + &
1.194*rxt(k,1935)*y(k,67) +1.635*rxt(k,1948)*y(k,291) +1.800*rxt(k,1958)*y(k,797) +1.134*rxt(k,1969)*y(k,53) + &
2.836*rxt(k,1980)*y(k,219) +1.154*rxt(k,1991)*y(k,404) +1.719*rxt(k,2001)*y(k,795) +.060*rxt(k,2012)*y(k,249) + &
.224*rxt(k,2027)*y(k,355) +1.320*rxt(k,2042)*y(k,879) +.276*rxt(k,2057)*y(k,479) +1.368*rxt(k,2067)*y(k,771) + &
.684*rxt(k,2080)*y(k,881) +1.100*rxt(k,2091)*y(k,55) +1.120*rxt(k,2101)*y(k,800) +1.078*rxt(k,2111)*y(k,56) + &
1.094*rxt(k,2121)*y(k,801) +1.064*rxt(k,2131)*y(k,57) +1.076*rxt(k,2141)*y(k,802) +1.056*rxt(k,2151)*y(k,58) + &
1.066*rxt(k,2161)*y(k,803) +1.060*rxt(k,2171)*y(k,804) +1.950*rxt(k,2200)*y(k,776) +1.950*rxt(k,2245)*y(k,360) + &
.093*rxt(k,2515)*y(k,344) +.124*rxt(k,2536)*y(k,346) +1.008*rxt(k,2572)*y(k,447) +.627*rxt(k,2624)*y(k,606) + &
1.808*rxt(k,2643)*y(k,552) +2.805*rxt(k,2734)*y(k,595) +1.614*rxt(k,2795)*y(k,501) +.501*rxt(k,2805)*y(k,628) + &
2.070*rxt(k,2935)*y(k,514) +.260*rxt(k,3016)*y(k,554) +.136*rxt(k,3106)*y(k,668) +.085*rxt(k,3118)*y(k,567) + &
1.332*rxt(k,3236)*y(k,363) +.140*rxt(k,3249)*y(k,569) +.045*rxt(k,3443)*y(k,356) +2.112*rxt(k,3458)*y(k,436) + &
1.584*rxt(k,3479)*y(k,587) +1.330*rxt(k,3552)*y(k,529) +1.060*rxt(k,3757)*y(k,588) +2.564*rxt(k,3794)*y(k,652) + &
2.464*rxt(k,3807)*y(k,651))*y(k,693) + (1.830*rxt(k,581)*y(k,761) +1.062*rxt(k,4196)*y(k,762) + &
.960*rxt(k,4199)*y(k,763) +.536*rxt(k,4204)*y(k,764) +1.520*rxt(k,4208)*y(k,765) +.624*rxt(k,4211)*y(k,766) + &
2.256*rxt(k,4215)*y(k,767))*y(k,705)
         loss(k,718) = (rxt(k,4512)* y(k,370) +rxt(k,4508)* y(k,548) +rxt(k,4513)* y(k,589) +rxt(k,4510)* y(k,620) +rxt(k,4514) &
* y(k,633) +rxt(k,4507)* y(k,690) +rxt(k,4509)* y(k,693) +rxt(k,4511)* y(k,758) +rxt(k,4515)* y(k,768) &
 +rxt(k,4516)* y(k,769))* y(k,870)
         prod(k,718) = (2.280*rxt(k,250)*y(k,191) +2.673*rxt(k,258)*y(k,93) +1.842*rxt(k,274)*y(k,86) +1.104*rxt(k,282)*y(k,85) + &
1.100*rxt(k,285)*y(k,88) +.048*rxt(k,315)*y(k,662) +.448*rxt(k,318)*y(k,504) +2.040*rxt(k,606)*y(k,95) + &
1.356*rxt(k,622)*y(k,96) +.972*rxt(k,664)*y(k,97) +2.232*rxt(k,697)*y(k,87) +.772*rxt(k,748)*y(k,91) + &
.633*rxt(k,784)*y(k,98) +.748*rxt(k,787)*y(k,89) +.495*rxt(k,868)*y(k,99) +.632*rxt(k,871)*y(k,90) + &
.640*rxt(k,875)*y(k,92) +2.004*rxt(k,1245)*y(k,213) +2.256*rxt(k,1568)*y(k,101) +.030*rxt(k,1578)*y(k,398) + &
.030*rxt(k,1709)*y(k,794) +.030*rxt(k,1800)*y(k,127) +.150*rxt(k,1860)*y(k,782))*y(k,705) &
 + (1.624*rxt(k,1253)*y(k,213) +1.662*rxt(k,1575)*y(k,101) +1.556*rxt(k,1585)*y(k,398) + &
1.556*rxt(k,1716)*y(k,794) +.060*rxt(k,1807)*y(k,127) +1.500*rxt(k,1867)*y(k,782))*y(k,693) &
 + (.240*rxt(k,1249)*y(k,213) +.240*rxt(k,1581)*y(k,398) +.240*rxt(k,1712)*y(k,794) +.240*rxt(k,1863)*y(k,782) + &
.144*rxt(k,1907)*y(k,128))*y(k,696) +1.922*rxt(k,16)*y(k,94) +1.922*rxt(k,109)*y(k,663)
         loss(k,7) = 0.
         prod(k,7) =1.900*rxt(k,3944)*y(k,748)*y(k,705)
         loss(k,8) = 0.
         prod(k,8) = (2.000*rxt(k,3949)*y(k,690) +2.000*rxt(k,3952)*y(k,693) +2.000*rxt(k,3957)*y(k,620) + &
2.000*rxt(k,3958)*y(k,758) +2.000*rxt(k,3960)*y(k,370) +2.000*rxt(k,3963)*y(k,589) +2.000*rxt(k,3964)*y(k,427)) &
*y(k,427) +.800*rxt(k,53)*y(k,431)
         loss(k,841) = (rxt(k,4442)* y(k,370) +rxt(k,4438)* y(k,548) +rxt(k,4443)* y(k,589) +rxt(k,4440)* y(k,620) +rxt(k,4444) &
* y(k,633) +rxt(k,4437)* y(k,690) +rxt(k,4439)* y(k,693) +rxt(k,4441)* y(k,758) +rxt(k,4445)* y(k,768) &
 +rxt(k,4446)* y(k,769))* y(k,873)
         prod(k,841) = (3.000*rxt(k,257)*y(k,680) +3.000*rxt(k,259)*y(k,93) +3.000*rxt(k,262)*y(k,452) + &
3.000*rxt(k,265)*y(k,681) +3.000*rxt(k,269)*y(k,114) +2.000*rxt(k,271)*y(k,453) +2.000*rxt(k,273)*y(k,683) + &
3.000*rxt(k,276)*y(k,86) +3.000*rxt(k,278)*y(k,454) +2.000*rxt(k,281)*y(k,685) +3.000*rxt(k,284)*y(k,85) + &
4.000*rxt(k,287)*y(k,88) +2.000*rxt(k,290)*y(k,455) +2.000*rxt(k,292)*y(k,687) +2.000*rxt(k,294)*y(k,669) + &
3.000*rxt(k,317)*y(k,662) +4.000*rxt(k,320)*y(k,504) +4.000*rxt(k,325)*y(k,661) +3.565*rxt(k,506)*y(k,756) + &
1.600*rxt(k,512)*y(k,446) +3.132*rxt(k,520)*y(k,348) +3.132*rxt(k,527)*y(k,349) +3.176*rxt(k,535)*y(k,350) + &
2.844*rxt(k,563)*y(k,582) +2.640*rxt(k,576)*y(k,731) +3.816*rxt(k,581)*y(k,761) +3.000*rxt(k,586)*y(k,586) + &
3.000*rxt(k,605)*y(k,193) +4.000*rxt(k,608)*y(k,95) +3.000*rxt(k,612)*y(k,194) +3.000*rxt(k,615)*y(k,280) + &
3.000*rxt(k,618)*y(k,581) +3.000*rxt(k,621)*y(k,621) +4.000*rxt(k,624)*y(k,96) +3.000*rxt(k,628)*y(k,115) + &
3.000*rxt(k,631)*y(k,137) +3.000*rxt(k,634)*y(k,195) +4.000*rxt(k,637)*y(k,209) +3.000*rxt(k,641)*y(k,281) + &
3.000*rxt(k,644)*y(k,258) +4.000*rxt(k,648)*y(k,11) +4.000*rxt(k,651)*y(k,24) +4.000*rxt(k,655)*y(k,37) + &
4.000*rxt(k,660)*y(k,506) +3.000*rxt(k,663)*y(k,622) +4.000*rxt(k,666)*y(k,97) +3.000*rxt(k,670)*y(k,104) + &
3.000*rxt(k,673)*y(k,116) +3.000*rxt(k,676)*y(k,138) +3.000*rxt(k,679)*y(k,146) +3.000*rxt(k,682)*y(k,196) + &
3.000*rxt(k,685)*y(k,282) +3.000*rxt(k,688)*y(k,319) +4.000*rxt(k,691)*y(k,102) +4.000*rxt(k,695)*y(k,210) + &
4.000*rxt(k,699)*y(k,87) +3.000*rxt(k,703)*y(k,217) +3.000*rxt(k,706)*y(k,251) +4.000*rxt(k,710)*y(k,5) + &
4.000*rxt(k,714)*y(k,7) +4.000*rxt(k,717)*y(k,12) +4.000*rxt(k,721)*y(k,15) +4.000*rxt(k,725)*y(k,17) + &
4.000*rxt(k,729)*y(k,76) +3.000*rxt(k,733)*y(k,25) +3.000*rxt(k,736)*y(k,47) +4.000*rxt(k,739)*y(k,38) + &
3.000*rxt(k,743)*y(k,507) +4.000*rxt(k,747)*y(k,728) +4.000*rxt(k,750)*y(k,91) +3.000*rxt(k,754)*y(k,109) + &
3.000*rxt(k,757)*y(k,139) +3.000*rxt(k,760)*y(k,197) +3.000*rxt(k,763)*y(k,207) +3.000*rxt(k,766)*y(k,225) + &
3.000*rxt(k,769)*y(k,310) +3.000*rxt(k,772)*y(k,320) +4.000*rxt(k,775)*y(k,129) +4.000*rxt(k,779)*y(k,211) + &
3.000*rxt(k,783)*y(k,292) +3.000*rxt(k,786)*y(k,98) +4.000*rxt(k,789)*y(k,89) +3.000*rxt(k,793)*y(k,153) + &
3.000*rxt(k,796)*y(k,117) +3.000*rxt(k,799)*y(k,147) +3.000*rxt(k,802)*y(k,283) +3.000*rxt(k,805)*y(k,218) + &
3.000*rxt(k,808)*y(k,259) +3.000*rxt(k,811)*y(k,558) +4.000*rxt(k,814)*y(k,13) +4.000*rxt(k,818)*y(k,30) + &
4.000*rxt(k,823)*y(k,6) +3.000*rxt(k,826)*y(k,70) +3.000*rxt(k,828)*y(k,393) +3.000*rxt(k,832)*y(k,134) + &
3.000*rxt(k,835)*y(k,140) +3.000*rxt(k,838)*y(k,154) +3.000*rxt(k,840)*y(k,198) +3.000*rxt(k,844)*y(k,214) + &
3.000*rxt(k,847)*y(k,284) +3.000*rxt(k,850)*y(k,321) +3.000*rxt(k,853)*y(k,322) +3.000*rxt(k,856)*y(k,130) + &
4.000*rxt(k,859)*y(k,145) +4.000*rxt(k,863)*y(k,212) +3.000*rxt(k,867)*y(k,293) +3.000*rxt(k,870)*y(k,99) + &
4.000*rxt(k,873)*y(k,90) +4.000*rxt(k,877)*y(k,92) +3.000*rxt(k,881)*y(k,111) +3.000*rxt(k,884)*y(k,118) + &
3.000*rxt(k,887)*y(k,148) +3.000*rxt(k,890)*y(k,185) +3.000*rxt(k,892)*y(k,311) +3.000*rxt(k,896)*y(k,36) + &
3.000*rxt(k,899)*y(k,46) +4.000*rxt(k,902)*y(k,77) +3.000*rxt(k,905)*y(k,402) +2.000*rxt(k,908)*y(k,670) + &
3.000*rxt(k,911)*y(k,103) +3.000*rxt(k,914)*y(k,155) +3.000*rxt(k,917)*y(k,224) +3.000*rxt(k,920)*y(k,274) + &
3.000*rxt(k,923)*y(k,318) +4.000*rxt(k,927)*y(k,41) +3.000*rxt(k,930)*y(k,69) +3.000*rxt(k,932)*y(k,405) + &
2.000*rxt(k,935)*y(k,671) +3.000*rxt(k,938)*y(k,107) +3.000*rxt(k,941)*y(k,229) +3.000*rxt(k,944)*y(k,230) + &
3.000*rxt(k,947)*y(k,275) +3.000*rxt(k,949)*y(k,336) +4.000*rxt(k,953)*y(k,29) +3.000*rxt(k,957)*y(k,78) + &
2.000*rxt(k,959)*y(k,409) +2.000*rxt(k,961)*y(k,672) +3.000*rxt(k,964)*y(k,110) +3.000*rxt(k,967)*y(k,133) + &
3.000*rxt(k,970)*y(k,231) +3.000*rxt(k,973)*y(k,234) +3.000*rxt(k,976)*y(k,276) +3.000*rxt(k,978)*y(k,337) + &
4.000*rxt(k,982)*y(k,42) +3.000*rxt(k,986)*y(k,74) +2.000*rxt(k,988)*y(k,411) +2.000*rxt(k,990)*y(k,673) + &
3.000*rxt(k,993)*y(k,131) +3.000*rxt(k,996)*y(k,184) +3.000*rxt(k,999)*y(k,235) +3.000*rxt(k,1002)*y(k,238) + &
3.000*rxt(k,1005)*y(k,277) +3.000*rxt(k,1007)*y(k,338) +3.000*rxt(k,1011)*y(k,44) +3.000*rxt(k,1013)*y(k,79) + &
2.000*rxt(k,1016)*y(k,413) +2.000*rxt(k,1018)*y(k,674) +3.000*rxt(k,1021)*y(k,152) +3.000*rxt(k,1024)*y(k,236) + &
3.000*rxt(k,1027)*y(k,239) +3.000*rxt(k,1030)*y(k,278) +3.000*rxt(k,1032)*y(k,339) +3.000*rxt(k,1036)*y(k,31) + &
3.000*rxt(k,1039)*y(k,75) +2.000*rxt(k,1041)*y(k,415) +4.000*rxt(k,1045)*y(k,40) +2.000*rxt(k,1047)*y(k,675) + &
3.000*rxt(k,1050)*y(k,158) +3.000*rxt(k,1053)*y(k,279) +3.000*rxt(k,1056)*y(k,301) +2.000*rxt(k,1058)*y(k,340) + &
3.000*rxt(k,1061)*y(k,43) +3.000*rxt(k,1063)*y(k,80) +2.000*rxt(k,1066)*y(k,374) +4.000*rxt(k,1132)*y(k,84) + &
4.000*rxt(k,1145)*y(k,263) +3.000*rxt(k,1156)*y(k,176) +2.000*rxt(k,1166)*y(k,180) +3.000*rxt(k,1176)*y(k,389) + &
3.000*rxt(k,1187)*y(k,788) +3.000*rxt(k,1199)*y(k,457) +3.000*rxt(k,1209)*y(k,780) +3.000*rxt(k,1220)*y(k,49) + &
2.913*rxt(k,1231)*y(k,28) +2.000*rxt(k,1233)*y(k,262) +4.000*rxt(k,1236)*y(k,72) +4.000*rxt(k,1248)*y(k,213) + &
4.000*rxt(k,1259)*y(k,264) +4.000*rxt(k,1271)*y(k,312) +3.000*rxt(k,1283)*y(k,120) +3.000*rxt(k,1293)*y(k,168) + &
3.000*rxt(k,1304)*y(k,177) +2.000*rxt(k,1314)*y(k,122) +3.000*rxt(k,1322)*y(k,181) +3.000*rxt(k,1333)*y(k,386) + &
3.000*rxt(k,1345)*y(k,391) +3.000*rxt(k,1355)*y(k,395) +3.000*rxt(k,1367)*y(k,792) +3.000*rxt(k,1379)*y(k,798) + &
3.000*rxt(k,1391)*y(k,785) +3.000*rxt(k,1403)*y(k,789) +4.000*rxt(k,1414)*y(k,285) +3.000*rxt(k,1426)*y(k,81) + &
3.000*rxt(k,1437)*y(k,456) +4.000*rxt(k,1448)*y(k,781) +3.000*rxt(k,1462)*y(k,82) +4.000*rxt(k,1474)*y(k,317) + &
4.000*rxt(k,1486)*y(k,71) +4.000*rxt(k,1498)*y(k,215) +4.000*rxt(k,1511)*y(k,265) +3.000*rxt(k,1523)*y(k,136) + &
3.000*rxt(k,1535)*y(k,113) +4.000*rxt(k,1547)*y(k,208) +3.000*rxt(k,1559)*y(k,178) +3.000*rxt(k,1569)*y(k,101) + &
3.000*rxt(k,1580)*y(k,398) +3.000*rxt(k,1592)*y(k,141) +4.000*rxt(k,1606)*y(k,182) +3.000*rxt(k,1619)*y(k,257) + &
3.000*rxt(k,1630)*y(k,290) +3.000*rxt(k,1643)*y(k,387) +3.000*rxt(k,1655)*y(k,202) +3.000*rxt(k,1666)*y(k,269) + &
3.000*rxt(k,1679)*y(k,216) +2.000*rxt(k,1690)*y(k,123) +3.000*rxt(k,1700)*y(k,392) +3.000*rxt(k,1711)*y(k,794) + &
3.000*rxt(k,1723)*y(k,786) +3.000*rxt(k,1735)*y(k,790) +3.000*rxt(k,1746)*y(k,396) +3.000*rxt(k,1757)*y(k,799) + &
3.000*rxt(k,1771)*y(k,793) +3.000*rxt(k,1783)*y(k,26) +3.000*rxt(k,1792)*y(k,83) +3.000*rxt(k,1802)*y(k,127) + &
4.000*rxt(k,1814)*y(k,267) +3.000*rxt(k,1826)*y(k,787) +4.000*rxt(k,1839)*y(k,183) +3.000*rxt(k,1852)*y(k,400) + &
3.000*rxt(k,1862)*y(k,782) +3.000*rxt(k,1874)*y(k,783) +3.000*rxt(k,1885)*y(k,791) +3.000*rxt(k,1896)*y(k,796) + &
3.000*rxt(k,1905)*y(k,128) +4.000*rxt(k,1917)*y(k,124) +3.000*rxt(k,1929)*y(k,67) +4.000*rxt(k,1939)*y(k,291) + &
3.000*rxt(k,1952)*y(k,797) +3.000*rxt(k,1963)*y(k,53) +3.000*rxt(k,1973)*y(k,219) +3.000*rxt(k,1985)*y(k,404) + &
3.000*rxt(k,1995)*y(k,795) +2.000*rxt(k,2005)*y(k,249) +4.000*rxt(k,2017)*y(k,355) +4.000*rxt(k,2032)*y(k,879) + &
4.000*rxt(k,2047)*y(k,479) +3.000*rxt(k,2061)*y(k,771) +3.000*rxt(k,2071)*y(k,881) +3.000*rxt(k,2085)*y(k,55) + &
3.000*rxt(k,2095)*y(k,800) +3.000*rxt(k,2105)*y(k,56) +3.000*rxt(k,2115)*y(k,801) +3.000*rxt(k,2125)*y(k,57) + &
3.000*rxt(k,2135)*y(k,802) +3.000*rxt(k,2145)*y(k,58) +3.000*rxt(k,2155)*y(k,803) +3.000*rxt(k,2165)*y(k,804) + &
.292*rxt(k,2176)*y(k,833) +1.132*rxt(k,2180)*y(k,383) +.188*rxt(k,2184)*y(k,665) +.275*rxt(k,2188)*y(k,706) + &
.510*rxt(k,2193)*y(k,746) +2.000*rxt(k,2196)*y(k,776) +1.648*rxt(k,2206)*y(k,676) +.976*rxt(k,2210)*y(k,572) + &
.515*rxt(k,2214)*y(k,644) +.825*rxt(k,2219)*y(k,704) +.825*rxt(k,2224)*y(k,721) +.270*rxt(k,2229)*y(k,14) + &
.215*rxt(k,2234)*y(k,16) +.144*rxt(k,2239)*y(k,32) +2.000*rxt(k,2241)*y(k,360) +1.836*rxt(k,2251)*y(k,373) + &
.360*rxt(k,2255)*y(k,807) +.690*rxt(k,2259)*y(k,596) +1.075*rxt(k,2264)*y(k,697) +1.075*rxt(k,2269)*y(k,711) + &
.720*rxt(k,2274)*y(k,719) +.445*rxt(k,2279)*y(k,240) +.445*rxt(k,2285)*y(k,302) +.212*rxt(k,2289)*y(k,324) + &
1.628*rxt(k,2292)*y(k,816) +2.116*rxt(k,2301)*y(k,376) +.985*rxt(k,2305)*y(k,597) +1.480*rxt(k,2310)*y(k,698) + &
1.480*rxt(k,2315)*y(k,712) +.645*rxt(k,2320)*y(k,241) +.645*rxt(k,2326)*y(k,303) +.312*rxt(k,2330)*y(k,325) + &
2.332*rxt(k,2338)*y(k,377) +1.090*rxt(k,2342)*y(k,598) +1.615*rxt(k,2347)*y(k,699) +1.615*rxt(k,2352)*y(k,713) + &
.750*rxt(k,2357)*y(k,242) +.750*rxt(k,2363)*y(k,304) +.364*rxt(k,2367)*y(k,326) +2.504*rxt(k,2375)*y(k,378) + &
1.028*rxt(k,2379)*y(k,599) +1.860*rxt(k,2383)*y(k,700) +1.865*rxt(k,2388)*y(k,714) +.920*rxt(k,2393)*y(k,243) + &
.920*rxt(k,2399)*y(k,305) +.452*rxt(k,2403)*y(k,327) +2.644*rxt(k,2407)*y(k,380) +1.164*rxt(k,2411)*y(k,600) + &
2.065*rxt(k,2415)*y(k,701) +2.070*rxt(k,2420)*y(k,715) +1.040*rxt(k,2425)*y(k,244) +1.040*rxt(k,2431)*y(k,306) + &
.520*rxt(k,2435)*y(k,328) +2.760*rxt(k,2439)*y(k,381) +1.288*rxt(k,2443)*y(k,601) +2.245*rxt(k,2447)*y(k,702) + &
2.245*rxt(k,2452)*y(k,716) +1.150*rxt(k,2457)*y(k,245) +1.150*rxt(k,2463)*y(k,307) +.580*rxt(k,2467)*y(k,329) + &
2.856*rxt(k,2471)*y(k,382) +1.404*rxt(k,2475)*y(k,602) +2.405*rxt(k,2479)*y(k,703) +2.405*rxt(k,2484)*y(k,717) + &
1.265*rxt(k,2489)*y(k,246) +1.265*rxt(k,2495)*y(k,308) +.648*rxt(k,2499)*y(k,330) +3.000*rxt(k,2716)*y(k,419) + &
1.107*rxt(k,2719)*y(k,420) +4.000*rxt(k,2722)*y(k,354) +3.000*rxt(k,2726)*y(k,828) +3.000*rxt(k,2729)*y(k,595) + &
.078*rxt(k,2739)*y(k,94) +.632*rxt(k,2744)*y(k,270) +.608*rxt(k,2751)*y(k,60) +3.000*rxt(k,2756)*y(k,659) + &
3.000*rxt(k,2758)*y(k,464) +3.000*rxt(k,2762)*y(k,649) +1.080*rxt(k,2765)*y(k,164) +.630*rxt(k,2769)*y(k,247) + &
1.560*rxt(k,2772)*y(k,407) +2.064*rxt(k,2775)*y(k,563) +2.124*rxt(k,2779)*y(k,190) +4.000*rxt(k,2783)*y(k,523) + &
3.000*rxt(k,2787)*y(k,654) +3.000*rxt(k,2790)*y(k,501) +2.000*rxt(k,2798)*y(k,628) +.228*rxt(k,2809)*y(k,531) + &
3.000*rxt(k,2812)*y(k,135) +3.416*rxt(k,2816)*y(k,825) +4.000*rxt(k,2821)*y(k,526) +4.000*rxt(k,2824)*y(k,579) + &
4.000*rxt(k,2829)*y(k,610) +4.000*rxt(k,2833)*y(k,624) +4.000*rxt(k,2836)*y(k,403) +4.000*rxt(k,2841)*y(k,726) + &
4.000*rxt(k,2845)*y(k,268) +3.000*rxt(k,2848)*y(k,100) +2.708*rxt(k,2851)*y(k,516) +2.505*rxt(k,2855)*y(k,206) + &
3.296*rxt(k,2859)*y(k,260) +3.392*rxt(k,2863)*y(k,287) +4.000*rxt(k,2866)*y(k,603) +2.415*rxt(k,2871)*y(k,513) + &
4.000*rxt(k,2876)*y(k,648) +3.528*rxt(k,2880)*y(k,657) +.126*rxt(k,2883)*y(k,725) +3.000*rxt(k,2887)*y(k,421) + &
1.230*rxt(k,2890)*y(k,422) +.606*rxt(k,2893)*y(k,62) +4.000*rxt(k,2897)*y(k,646) +4.000*rxt(k,2901)*y(k,655) + &
3.000*rxt(k,2905)*y(k,663) +1.767*rxt(k,2908)*y(k,61) +1.059*rxt(k,2911)*y(k,165) +1.404*rxt(k,2914)*y(k,647) + &
3.000*rxt(k,2918)*y(k,740) +3.000*rxt(k,2921)*y(k,498) +4.000*rxt(k,2924)*y(k,584) +3.000*rxt(k,2928)*y(k,514) + &
4.000*rxt(k,2940)*y(k,505) +4.000*rxt(k,2944)*y(k,566) +4.000*rxt(k,2947)*y(k,642) +4.000*rxt(k,2951)*y(k,361) + &
4.000*rxt(k,2956)*y(k,743) +4.000*rxt(k,2959)*y(k,772) +3.000*rxt(k,2963)*y(k,805) +4.000*rxt(k,2967)*y(k,474) + &
4.000*rxt(k,2971)*y(k,615) +.660*rxt(k,2974)*y(k,410) +.860*rxt(k,2978)*y(k,179) +4.000*rxt(k,2981)*y(k,493) + &
3.000*rxt(k,2985)*y(k,342) +2.130*rxt(k,2988)*y(k,742) +3.508*rxt(k,2991)*y(k,366) +2.140*rxt(k,2996)*y(k,289) + &
3.376*rxt(k,3004)*y(k,730) +3.552*rxt(k,3009)*y(k,554) +4.000*rxt(k,3021)*y(k,724) +4.000*rxt(k,3026)*y(k,449) + &
4.000*rxt(k,3030)*y(k,204) +4.000*rxt(k,3033)*y(k,640) +3.604*rxt(k,3038)*y(k,470) +1.608*rxt(k,3041)*y(k,490) + &
4.000*rxt(k,3044)*y(k,459) +3.000*rxt(k,3048)*y(k,509) +3.000*rxt(k,3051)*y(k,347) +2.481*rxt(k,3054)*y(k,811) + &
.264*rxt(k,3062)*y(k,372) +.738*rxt(k,3065)*y(k,64) +.693*rxt(k,3069)*y(k,200) +3.000*rxt(k,3073)*y(k,412) + &
3.000*rxt(k,3076)*y(k,186) +3.000*rxt(k,3079)*y(k,476) +3.000*rxt(k,3082)*y(k,332) +4.000*rxt(k,3085)*y(k,266) + &
1.926*rxt(k,3089)*y(k,63) +2.628*rxt(k,3092)*y(k,121) +4.000*rxt(k,3097)*y(k,294) +4.000*rxt(k,3101)*y(k,668) + &
4.000*rxt(k,3112)*y(k,567) +4.000*rxt(k,3124)*y(k,367) +3.000*rxt(k,3127)*y(k,352) +4.000*rxt(k,3131)*y(k,727) + &
4.000*rxt(k,3134)*y(k,562) +4.000*rxt(k,3139)*y(k,175) +4.000*rxt(k,3143)*y(k,616) +2.196*rxt(k,3147)*y(k,722) + &
2.505*rxt(k,3150)*y(k,723) +2.340*rxt(k,3153)*y(k,365) +4.000*rxt(k,3157)*y(k,492) +4.000*rxt(k,3161)*y(k,478) + &
3.632*rxt(k,3164)*y(k,473) +3.316*rxt(k,3169)*y(k,491) +3.704*rxt(k,3173)*y(k,489) +4.000*rxt(k,3176)*y(k,27) + &
3.000*rxt(k,3180)*y(k,460) +3.696*rxt(k,3184)*y(k,823) +4.000*rxt(k,3188)*y(k,162) +.858*rxt(k,3191)*y(k,66) + &
3.000*rxt(k,3195)*y(k,414) +2.421*rxt(k,3198)*y(k,65) +2.094*rxt(k,3201)*y(k,170) +1.905*rxt(k,3204)*y(k,166) + &
1.728*rxt(k,3207)*y(k,248) +1.902*rxt(k,3210)*y(k,309) +2.752*rxt(k,3213)*y(k,73) +3.000*rxt(k,3217)*y(k,565) + &
3.000*rxt(k,3220)*y(k,364) +1.600*rxt(k,3224)*y(k,496) +4.000*rxt(k,3229)*y(k,363) +4.000*rxt(k,3242)*y(k,569) + &
4.000*rxt(k,3255)*y(k,125) +4.000*rxt(k,3259)*y(k,187) +4.000*rxt(k,3262)*y(k,271) +4.000*rxt(k,3266)*y(k,313) + &
5.000*rxt(k,3271)*y(k,568) +4.000*rxt(k,3276)*y(k,362) +3.000*rxt(k,3279)*y(k,682) +4.000*rxt(k,3283)*y(k,593) + &
3.000*rxt(k,3286)*y(k,408) +3.000*rxt(k,3289)*y(k,171) +3.000*rxt(k,3292)*y(k,617) +1.364*rxt(k,3296)*y(k,167) + &
2.580*rxt(k,3299)*y(k,495) +.712*rxt(k,3303)*y(k,832) +4.000*rxt(k,3310)*y(k,161) +4.000*rxt(k,3314)*y(k,174) + &
3.644*rxt(k,3318)*y(k,468) +3.736*rxt(k,3323)*y(k,485) +4.000*rxt(k,3326)*y(k,461) +4.000*rxt(k,3331)*y(k,471) + &
3.732*rxt(k,3335)*y(k,821) +3.508*rxt(k,3339)*y(k,813) +3.000*rxt(k,3342)*y(k,205) +3.000*rxt(k,3345)*y(k,323) + &
4.000*rxt(k,3349)*y(k,68) +1.602*rxt(k,3352)*y(k,830) +3.000*rxt(k,3354)*y(k,416) +4.000*rxt(k,3358)*y(k,475) + &
3.000*rxt(k,3362)*y(k,156) +1.347*rxt(k,3365)*y(k,157) +4.000*rxt(k,3369)*y(k,142) +4.000*rxt(k,3373)*y(k,188) + &
4.000*rxt(k,3376)*y(k,254) +4.000*rxt(k,3380)*y(k,272) +4.000*rxt(k,3384)*y(k,314) +4.000*rxt(k,3388)*y(k,333) + &
4.000*rxt(k,3393)*y(k,576) +3.000*rxt(k,3396)*y(k,684) +3.000*rxt(k,3399)*y(k,618) +3.328*rxt(k,3402)*y(k,51) + &
3.428*rxt(k,3407)*y(k,488) +4.000*rxt(k,3411)*y(k,486) +4.000*rxt(k,3415)*y(k,487) +3.744*rxt(k,3419)*y(k,824) + &
2.235*rxt(k,3422)*y(k,834) +3.776*rxt(k,3426)*y(k,815) +3.000*rxt(k,3429)*y(k,534) +3.000*rxt(k,3432)*y(k,417) + &
4.000*rxt(k,3435)*y(k,356) +4.795*rxt(k,3449)*y(k,436) +1.216*rxt(k,3464)*y(k,557) +2.284*rxt(k,3467)*y(k,632) + &
3.000*rxt(k,3471)*y(k,587) +3.000*rxt(k,3482)*y(k,375) +2.220*rxt(k,3486)*y(k,571) +2.220*rxt(k,3489)*y(k,54) + &
2.976*rxt(k,3492)*y(k,237) +3.000*rxt(k,3495)*y(k,406) +4.000*rxt(k,3504)*y(k,143) +4.000*rxt(k,3507)*y(k,173) + &
4.000*rxt(k,3511)*y(k,220) +4.000*rxt(k,3516)*y(k,226) +3.000*rxt(k,3519)*y(k,255) +3.000*rxt(k,3522)*y(k,273) + &
4.000*rxt(k,3526)*y(k,295) +4.000*rxt(k,3529)*y(k,315) +4.000*rxt(k,3534)*y(k,334) +3.000*rxt(k,3537)*y(k,686) + &
4.985*rxt(k,3541)*y(k,529) +3.000*rxt(k,3557)*y(k,619) +2.622*rxt(k,3560)*y(k,494) +2.748*rxt(k,3563)*y(k,472) + &
3.440*rxt(k,3567)*y(k,462) +4.000*rxt(k,3571)*y(k,469) +3.748*rxt(k,3575)*y(k,820) +3.572*rxt(k,3579)*y(k,835) + &
4.000*rxt(k,3583)*y(k,172) +4.000*rxt(k,3593)*y(k,108) +4.000*rxt(k,3597)*y(k,126) +4.000*rxt(k,3601)*y(k,144) + &
4.000*rxt(k,3605)*y(k,149) +4.000*rxt(k,3608)*y(k,189) +4.000*rxt(k,3612)*y(k,227) +4.000*rxt(k,3616)*y(k,232) + &
3.000*rxt(k,3620)*y(k,256) +4.000*rxt(k,3623)*y(k,296) +4.000*rxt(k,3628)*y(k,297) +3.000*rxt(k,3631)*y(k,316) + &
4.000*rxt(k,3634)*y(k,335) +3.000*rxt(k,3638)*y(k,688) +3.000*rxt(k,3641)*y(k,611) +4.000*rxt(k,3644)*y(k,564) + &
1.662*rxt(k,3648)*y(k,151) +4.000*rxt(k,3651)*y(k,233) +3.000*rxt(k,3655)*y(k,261) +4.000*rxt(k,3658)*y(k,298) + &
3.000*rxt(k,3662)*y(k,612) +2.752*rxt(k,3666)*y(k,818) +1.860*rxt(k,3671)*y(k,817) +3.684*rxt(k,3674)*y(k,467) + &
1.475*rxt(k,3679)*y(k,466) +3.000*rxt(k,3682)*y(k,458) +4.000*rxt(k,3687)*y(k,477) +3.808*rxt(k,3691)*y(k,814) + &
3.896*rxt(k,3695)*y(k,812) +4.000*rxt(k,3699)*y(k,222) +4.000*rxt(k,3702)*y(k,253) +4.000*rxt(k,3706)*y(k,300) + &
3.000*rxt(k,3710)*y(k,627) +3.832*rxt(k,3714)*y(k,822) +4.000*rxt(k,3718)*y(k,106) +4.000*rxt(k,3721)*y(k,223) + &
4.000*rxt(k,3726)*y(k,228) +3.000*rxt(k,3729)*y(k,613) +4.005*rxt(k,3733)*y(k,169) +4.000*rxt(k,3738)*y(k,132) + &
3.000*rxt(k,3741)*y(k,252) +4.000*rxt(k,3745)*y(k,299) +3.000*rxt(k,3748)*y(k,629) +3.000*rxt(k,3751)*y(k,588) + &
3.000*rxt(k,3760)*y(k,614) +4.000*rxt(k,3765)*y(k,105) +4.000*rxt(k,3768)*y(k,221) +4.000*rxt(k,3773)*y(k,331) + &
2.670*rxt(k,3777)*y(k,463) +5.000*rxt(k,3782)*y(k,836) +3.000*rxt(k,3786)*y(k,652) +3.000*rxt(k,3799)*y(k,651) + &
.224*rxt(k,4060)*y(k,160) +3.000*rxt(k,4064)*y(k,689) +2.000*rxt(k,4069)*y(k,639) +2.000*rxt(k,4075)*y(k,810) + &
2.547*rxt(k,4097)*y(k,521) +.680*rxt(k,4105)*y(k,774) +.500*rxt(k,4106)*y(k,775) +2.000*rxt(k,4116)*y(k,524) + &
1.588*rxt(k,4120)*y(k,656) +.400*rxt(k,4131)*y(k,1) +2.460*rxt(k,4133)*y(k,658) +2.385*rxt(k,4139)*y(k,499) + &
2.196*rxt(k,4153)*y(k,574) +3.220*rxt(k,4161)*y(k,720) +.400*rxt(k,4168)*y(k,2) +2.295*rxt(k,4169)*y(k,827) + &
1.695*rxt(k,4179)*y(k,732) +2.880*rxt(k,4183)*y(k,733) +1.796*rxt(k,4186)*y(k,734) +1.296*rxt(k,4190)*y(k,735) + &
1.422*rxt(k,4193)*y(k,736) +1.056*rxt(k,4200)*y(k,763) +3.916*rxt(k,4204)*y(k,764) +2.328*rxt(k,4208)*y(k,765) + &
4.000*rxt(k,4212)*y(k,766) +3.952*rxt(k,4215)*y(k,767))*y(k,705) + (3.400*rxt(k,570)*y(k,582) + &
3.000*rxt(k,593)*y(k,586) +3.000*rxt(k,1139)*y(k,84) +3.000*rxt(k,1151)*y(k,263) +3.000*rxt(k,1163)*y(k,176) + &
3.000*rxt(k,1172)*y(k,180) +3.000*rxt(k,1184)*y(k,389) +3.000*rxt(k,1195)*y(k,788) +3.000*rxt(k,1205)*y(k,457) + &
3.000*rxt(k,1216)*y(k,780) +3.000*rxt(k,1226)*y(k,49) +2.000*rxt(k,1243)*y(k,72) +2.000*rxt(k,1254)*y(k,213) + &
3.000*rxt(k,1266)*y(k,264) +2.000*rxt(k,1279)*y(k,312) +2.000*rxt(k,1289)*y(k,120) +3.000*rxt(k,1300)*y(k,168) + &
3.000*rxt(k,1310)*y(k,177) +2.000*rxt(k,1318)*y(k,122) +3.000*rxt(k,1329)*y(k,181) +3.000*rxt(k,1341)*y(k,386) + &
3.000*rxt(k,1351)*y(k,391) +3.000*rxt(k,1363)*y(k,395) +3.000*rxt(k,1375)*y(k,792) +3.000*rxt(k,1387)*y(k,798) + &
3.000*rxt(k,1399)*y(k,785) +3.000*rxt(k,1409)*y(k,789) +4.000*rxt(k,1421)*y(k,285) +3.000*rxt(k,1433)*y(k,81) + &
3.000*rxt(k,1443)*y(k,456) +4.000*rxt(k,1457)*y(k,781) +3.000*rxt(k,1468)*y(k,82) +4.000*rxt(k,1481)*y(k,317) + &
2.000*rxt(k,1493)*y(k,71) +3.000*rxt(k,1506)*y(k,215) +3.000*rxt(k,1519)*y(k,265) +4.000*rxt(k,1530)*y(k,136) + &
3.000*rxt(k,1541)*y(k,113) +3.000*rxt(k,1555)*y(k,208) +3.000*rxt(k,1566)*y(k,178) +2.000*rxt(k,1576)*y(k,101) + &
4.000*rxt(k,1587)*y(k,398) +4.000*rxt(k,1600)*y(k,141) +4.000*rxt(k,1614)*y(k,182) +3.000*rxt(k,1626)*y(k,257) + &
4.000*rxt(k,1638)*y(k,290) +3.000*rxt(k,1651)*y(k,387) +3.000*rxt(k,1662)*y(k,202) +4.000*rxt(k,1674)*y(k,269) + &
3.000*rxt(k,1687)*y(k,216) +3.000*rxt(k,1695)*y(k,123) +3.000*rxt(k,1706)*y(k,392) +4.000*rxt(k,1718)*y(k,794) + &
3.000*rxt(k,1731)*y(k,786) +3.000*rxt(k,1741)*y(k,790) +3.000*rxt(k,1754)*y(k,396) +4.000*rxt(k,1766)*y(k,799) + &
3.000*rxt(k,1779)*y(k,793) +2.000*rxt(k,1788)*y(k,26) +2.000*rxt(k,1798)*y(k,83) +4.000*rxt(k,1809)*y(k,127) + &
3.000*rxt(k,1821)*y(k,267) +3.000*rxt(k,1834)*y(k,787) +4.000*rxt(k,1847)*y(k,183) +2.000*rxt(k,1858)*y(k,400) + &
4.000*rxt(k,1869)*y(k,782) +3.000*rxt(k,1881)*y(k,783) +3.000*rxt(k,1891)*y(k,791) +2.000*rxt(k,1902)*y(k,796) + &
3.000*rxt(k,1913)*y(k,128) +4.000*rxt(k,1924)*y(k,124) +2.000*rxt(k,1935)*y(k,67) +3.000*rxt(k,1948)*y(k,291) + &
3.000*rxt(k,1958)*y(k,797) +2.000*rxt(k,1969)*y(k,53) +4.000*rxt(k,1980)*y(k,219) +2.000*rxt(k,1991)*y(k,404) + &
3.000*rxt(k,2001)*y(k,795) +4.000*rxt(k,2012)*y(k,249) +4.000*rxt(k,2028)*y(k,355) +4.000*rxt(k,2042)*y(k,879) + &
4.000*rxt(k,2057)*y(k,479) +3.000*rxt(k,2067)*y(k,771) +4.000*rxt(k,2081)*y(k,881) +2.000*rxt(k,2091)*y(k,55) + &
2.000*rxt(k,2101)*y(k,800) +2.000*rxt(k,2111)*y(k,56) +2.000*rxt(k,2121)*y(k,801) +2.000*rxt(k,2131)*y(k,57) + &
2.000*rxt(k,2141)*y(k,802) +2.000*rxt(k,2151)*y(k,58) +2.000*rxt(k,2161)*y(k,803) +2.000*rxt(k,2171)*y(k,804) + &
3.000*rxt(k,2200)*y(k,776) +3.000*rxt(k,2245)*y(k,360) +3.000*rxt(k,2734)*y(k,595) +.112*rxt(k,2748)*y(k,270) + &
3.000*rxt(k,2795)*y(k,501) +3.000*rxt(k,2805)*y(k,628) +3.000*rxt(k,2935)*y(k,514) +4.000*rxt(k,3016)*y(k,554) + &
4.000*rxt(k,3107)*y(k,668) +5.000*rxt(k,3118)*y(k,567) +4.000*rxt(k,3236)*y(k,363) +4.000*rxt(k,3250)*y(k,569) + &
3.000*rxt(k,3444)*y(k,356) +4.000*rxt(k,3458)*y(k,436) +3.000*rxt(k,3479)*y(k,587) +5.000*rxt(k,3552)*y(k,529) + &
3.000*rxt(k,3588)*y(k,172) +2.000*rxt(k,3757)*y(k,588) +4.000*rxt(k,3795)*y(k,652) +4.000*rxt(k,3808)*y(k,651) + &
3.000*rxt(k,4080)*y(k,810) +3.000*rxt(k,4102)*y(k,521) +3.000*rxt(k,4136)*y(k,658) +3.000*rxt(k,4142)*y(k,499) + &
3.000*rxt(k,4158)*y(k,574) +3.000*rxt(k,4165)*y(k,720) +3.000*rxt(k,4172)*y(k,827))*y(k,693) &
 + (1.956*rxt(k,524)*y(k,348) +1.956*rxt(k,531)*y(k,349) +1.584*rxt(k,541)*y(k,350) +.192*rxt(k,567)*y(k,582) + &
rxt(k,590)*y(k,586) +.252*rxt(k,1137)*y(k,84) +.252*rxt(k,1149)*y(k,263) +2.668*rxt(k,1160)*y(k,176) + &
2.800*rxt(k,1169)*y(k,180) +.252*rxt(k,1181)*y(k,389) +.252*rxt(k,1192)*y(k,788) +.375*rxt(k,1202)*y(k,457) + &
.252*rxt(k,1223)*y(k,49) +.252*rxt(k,1241)*y(k,72) +.252*rxt(k,1252)*y(k,213) +.315*rxt(k,1263)*y(k,264) + &
.315*rxt(k,1277)*y(k,312) +2.668*rxt(k,1287)*y(k,120) +2.668*rxt(k,1297)*y(k,168) +2.668*rxt(k,1308)*y(k,177) + &
2.000*rxt(k,1316)*y(k,122) +2.948*rxt(k,1326)*y(k,181) +.315*rxt(k,1337)*y(k,386) +.375*rxt(k,1348)*y(k,391) + &
3.500*rxt(k,1359)*y(k,395) +3.500*rxt(k,1371)*y(k,792) +.315*rxt(k,1383)*y(k,798) +.315*rxt(k,1395)*y(k,785) + &
.375*rxt(k,1406)*y(k,789) +.500*rxt(k,1417)*y(k,285) +2.948*rxt(k,1430)*y(k,81) +.500*rxt(k,1440)*y(k,456) + &
.315*rxt(k,1454)*y(k,781) +2.948*rxt(k,1466)*y(k,82) +.500*rxt(k,1478)*y(k,317) +.252*rxt(k,1491)*y(k,71) + &
.315*rxt(k,1503)*y(k,215) +.315*rxt(k,1516)*y(k,265) +2.668*rxt(k,1527)*y(k,136) +2.668*rxt(k,1539)*y(k,113) + &
.315*rxt(k,1552)*y(k,208) +2.668*rxt(k,1563)*y(k,178) +2.668*rxt(k,1574)*y(k,101) +.252*rxt(k,1584)*y(k,398) + &
3.685*rxt(k,1596)*y(k,141) +3.685*rxt(k,1610)*y(k,182) +2.800*rxt(k,1623)*y(k,257) +3.685*rxt(k,1634)*y(k,290) + &
.315*rxt(k,1647)*y(k,387) +.500*rxt(k,1659)*y(k,202) +3.685*rxt(k,1670)*y(k,269) +3.500*rxt(k,1683)*y(k,216) + &
3.000*rxt(k,1693)*y(k,123) +.500*rxt(k,1704)*y(k,392) +.252*rxt(k,1715)*y(k,794) +.315*rxt(k,1727)*y(k,786) + &
.500*rxt(k,1739)*y(k,790) +3.500*rxt(k,1750)*y(k,396) +.315*rxt(k,1763)*y(k,799) +3.500*rxt(k,1775)*y(k,793) + &
3.000*rxt(k,1786)*y(k,26) +.252*rxt(k,1796)*y(k,83) +2.668*rxt(k,1806)*y(k,127) +2.668*rxt(k,1819)*y(k,267) + &
.315*rxt(k,1830)*y(k,787) +3.685*rxt(k,1843)*y(k,183) +.500*rxt(k,1856)*y(k,400) +.500*rxt(k,1866)*y(k,782) + &
.500*rxt(k,1878)*y(k,783) +.500*rxt(k,1889)*y(k,791) +.500*rxt(k,1900)*y(k,796) +2.948*rxt(k,1910)*y(k,128) + &
3.000*rxt(k,1921)*y(k,124) +.252*rxt(k,1933)*y(k,67) +.315*rxt(k,1945)*y(k,291) +.500*rxt(k,1956)*y(k,797) + &
.252*rxt(k,1967)*y(k,53) +2.800*rxt(k,1977)*y(k,219) +.500*rxt(k,1989)*y(k,404) +.500*rxt(k,1999)*y(k,795) + &
2.948*rxt(k,2009)*y(k,249) +4.422*rxt(k,2023)*y(k,355) +1.380*rxt(k,2038)*y(k,879) +3.690*rxt(k,2052)*y(k,479) + &
.680*rxt(k,2065)*y(k,771) +4.555*rxt(k,2077)*y(k,881) +.252*rxt(k,2089)*y(k,55) +.500*rxt(k,2099)*y(k,800) + &
.252*rxt(k,2109)*y(k,56) +.500*rxt(k,2119)*y(k,801) +.252*rxt(k,2129)*y(k,57) +.500*rxt(k,2139)*y(k,802) + &
.252*rxt(k,2149)*y(k,58) +.500*rxt(k,2159)*y(k,803) +.500*rxt(k,2169)*y(k,804) +2.668*rxt(k,2802)*y(k,628) + &
2.668*rxt(k,2932)*y(k,514) +.004*rxt(k,3012)*y(k,554) +2.668*rxt(k,3233)*y(k,363) +2.668*rxt(k,3246)*y(k,569) + &
3.685*rxt(k,3440)*y(k,356) +3.685*rxt(k,3454)*y(k,436) +3.585*rxt(k,3476)*y(k,587) +4.314*rxt(k,3548)*y(k,529) + &
.500*rxt(k,3755)*y(k,588) +.625*rxt(k,3791)*y(k,652) +.625*rxt(k,3804)*y(k,651) +3.000*rxt(k,4077)*y(k,810) + &
3.000*rxt(k,4099)*y(k,521) +3.000*rxt(k,4155)*y(k,574))*y(k,696) +3.000*rxt(k,5)*y(k,60) +3.000*rxt(k,8)*y(k,62) &
 +3.000*rxt(k,11)*y(k,64) +3.000*rxt(k,14)*y(k,66) +2.000*rxt(k,17)*y(k,94) +2.000*rxt(k,19)*y(k,135) &
 +2.000*rxt(k,21)*y(k,186) +2.000*rxt(k,26)*y(k,200) +3.000*rxt(k,29)*y(k,266) +3.000*rxt(k,32)*y(k,270) &
 +2.000*rxt(k,34)*y(k,332) +2.000*rxt(k,45)*y(k,412) +2.000*rxt(k,47)*y(k,414) +2.000*rxt(k,60)*y(k,464) &
 +2.000*rxt(k,62)*y(k,476) +2.000*rxt(k,65)*y(k,531) +3.000*rxt(k,81)*y(k,557) +.750*rxt(k,596)*y(k,839)*y(k,586) &
 +3.000*rxt(k,98)*y(k,646) +2.000*rxt(k,100)*y(k,649) +2.000*rxt(k,106)*y(k,655) +2.000*rxt(k,108)*y(k,659) &
 +2.000*rxt(k,110)*y(k,663) +3.000*rxt(k,127)*y(k,731) +2.000*rxt(k,130)*y(k,732) +3.000*rxt(k,133)*y(k,733) &
 +2.000*rxt(k,135)*y(k,734) +3.000*rxt(k,137)*y(k,735) +2.000*rxt(k,140)*y(k,736) +3.632*rxt(k,145)*y(k,755) &
 +2.955*rxt(k,155)*y(k,761) +2.936*rxt(k,162)*y(k,764) +1.777*rxt(k,166)*y(k,765) +4.000*rxt(k,170)*y(k,766) &
 +2.000*rxt(k,172)*y(k,767)
         loss(k,703) = (rxt(k,4452)* y(k,370) +rxt(k,4448)* y(k,548) +rxt(k,4453)* y(k,589) +rxt(k,4450)* y(k,620) +rxt(k,4454) &
* y(k,633) +rxt(k,4447)* y(k,690) +rxt(k,4449)* y(k,693) +rxt(k,4451)* y(k,758) +rxt(k,4455)* y(k,768) &
 +rxt(k,4456)* y(k,769))* y(k,874)
         prod(k,703) = (1.256*rxt(k,601)*y(k,359) +1.740*rxt(k,2176)*y(k,833) +1.392*rxt(k,2180)*y(k,383) + &
2.220*rxt(k,2184)*y(k,665) +2.930*rxt(k,2188)*y(k,706) +2.305*rxt(k,2193)*y(k,746) +1.140*rxt(k,2206)*y(k,676) + &
1.468*rxt(k,2210)*y(k,572) +2.610*rxt(k,2214)*y(k,644) +2.590*rxt(k,2220)*y(k,704) +2.145*rxt(k,2225)*y(k,721) + &
4.170*rxt(k,2229)*y(k,14) +3.525*rxt(k,2235)*y(k,16) +3.052*rxt(k,2239)*y(k,32) +1.048*rxt(k,2251)*y(k,373) + &
1.768*rxt(k,2255)*y(k,807) +2.510*rxt(k,2259)*y(k,596) +2.435*rxt(k,2265)*y(k,697) +2.015*rxt(k,2270)*y(k,711) + &
2.195*rxt(k,2275)*y(k,719) +4.015*rxt(k,2280)*y(k,240) +3.360*rxt(k,2285)*y(k,302) +3.000*rxt(k,2289)*y(k,324) + &
.732*rxt(k,2293)*y(k,816) +.732*rxt(k,2297)*y(k,667) +.912*rxt(k,2301)*y(k,376) +2.335*rxt(k,2305)*y(k,597) + &
2.185*rxt(k,2311)*y(k,698) +1.805*rxt(k,2316)*y(k,712) +3.840*rxt(k,2321)*y(k,241) +3.210*rxt(k,2326)*y(k,303) + &
2.920*rxt(k,2330)*y(k,325) +1.100*rxt(k,2334)*y(k,630) +.808*rxt(k,2338)*y(k,377) +2.275*rxt(k,2342)*y(k,598) + &
2.100*rxt(k,2348)*y(k,699) +1.735*rxt(k,2353)*y(k,713) +3.745*rxt(k,2358)*y(k,242) +3.135*rxt(k,2363)*y(k,304) + &
2.880*rxt(k,2367)*y(k,326) +1.464*rxt(k,2371)*y(k,119) +.724*rxt(k,2375)*y(k,378) +1.728*rxt(k,2379)*y(k,599) + &
1.945*rxt(k,2383)*y(k,700) +1.610*rxt(k,2388)*y(k,714) +3.595*rxt(k,2394)*y(k,243) +3.010*rxt(k,2399)*y(k,305) + &
2.808*rxt(k,2403)*y(k,327) +.660*rxt(k,2407)*y(k,380) +1.648*rxt(k,2411)*y(k,600) +1.820*rxt(k,2415)*y(k,701) + &
1.505*rxt(k,2420)*y(k,715) +3.490*rxt(k,2426)*y(k,244) +2.920*rxt(k,2431)*y(k,306) +2.756*rxt(k,2435)*y(k,328) + &
.604*rxt(k,2439)*y(k,381) +1.576*rxt(k,2443)*y(k,601) +1.710*rxt(k,2447)*y(k,702) +1.415*rxt(k,2452)*y(k,716) + &
3.390*rxt(k,2458)*y(k,245) +2.835*rxt(k,2463)*y(k,307) +2.708*rxt(k,2467)*y(k,329) +.556*rxt(k,2471)*y(k,382) + &
1.512*rxt(k,2475)*y(k,602) +1.610*rxt(k,2479)*y(k,703) +1.330*rxt(k,2484)*y(k,717) +3.290*rxt(k,2490)*y(k,246) + &
2.750*rxt(k,2495)*y(k,308) +2.656*rxt(k,2499)*y(k,330) +1.360*rxt(k,3058)*y(k,369) +1.752*rxt(k,3062)*y(k,372) + &
1.265*rxt(k,3224)*y(k,496) +2.480*rxt(k,3307)*y(k,744) +2.480*rxt(k,3500)*y(k,23) +2.185*rxt(k,3680)*y(k,466) + &
.470*rxt(k,3734)*y(k,169) +1.445*rxt(k,3778)*y(k,463) +1.256*rxt(k,4085)*y(k,443) +1.256*rxt(k,4089)*y(k,692) + &
1.256*rxt(k,4093)*y(k,438) +1.256*rxt(k,4109)*y(k,371) +1.132*rxt(k,4120)*y(k,656) +1.876*rxt(k,4124)*y(k,426) + &
1.876*rxt(k,4128)*y(k,718) +.200*rxt(k,4130)*y(k,745) +.500*rxt(k,4132)*y(k,809) +.200*rxt(k,4176)*y(k,604)) &
*y(k,705)
         loss(k,9) = 0.
         prod(k,9) =2.000*rxt(k,3945)*y(k,748)*y(k,705)
         loss(k,755) = (rxt(k,4502)* y(k,370) +rxt(k,4498)* y(k,548) +rxt(k,4503)* y(k,589) +rxt(k,4500)* y(k,620) +rxt(k,4504) &
* y(k,633) +rxt(k,4497)* y(k,690) +rxt(k,4499)* y(k,693) +rxt(k,4501)* y(k,758) +rxt(k,4505)* y(k,768) &
 +rxt(k,4506)* y(k,769))* y(k,876)
         prod(k,755) = (2.000*rxt(k,237)*y(k,512) +rxt(k,242)*y(k,511) +2.000*rxt(k,244)*y(k,738) +2.000*rxt(k,246)*y(k,450) + &
3.000*rxt(k,248)*y(k,678) +3.000*rxt(k,252)*y(k,191) +2.000*rxt(k,254)*y(k,451) +.100*rxt(k,299)*y(k,519) + &
2.000*rxt(k,300)*y(k,638) +rxt(k,302)*y(k,341) +.141*rxt(k,305)*y(k,573) +3.000*rxt(k,308)*y(k,625) + &
2.238*rxt(k,310)*y(k,808) +3.000*rxt(k,314)*y(k,517) +rxt(k,326)*y(k,432) +rxt(k,327)*y(k,440) + &
rxt(k,328)*y(k,609) +rxt(k,329)*y(k,435) +2.000*rxt(k,332)*y(k,388) +rxt(k,333)*y(k,21) + &
2.000*rxt(k,334)*y(k,3) +2.000*rxt(k,336)*y(k,4) +rxt(k,338)*y(k,8) +.070*rxt(k,486)*y(k,757) + &
.982*rxt(k,491)*y(k,425) +2.000*rxt(k,493)*y(k,759) +1.024*rxt(k,497)*y(k,770) +.480*rxt(k,501)*y(k,755) + &
1.500*rxt(k,543)*y(k,590) +3.000*rxt(k,553)*y(k,664) +.942*rxt(k,581)*y(k,761) +2.000*rxt(k,1073)*y(k,739) + &
2.000*rxt(k,1080)*y(k,351) +3.000*rxt(k,1084)*y(k,52) +2.000*rxt(k,1094)*y(k,585) +2.000*rxt(k,1103)*y(k,385) + &
2.000*rxt(k,1111)*y(k,784) +2.000*rxt(k,1116)*y(k,19) +3.000*rxt(k,1118)*y(k,33) +2.000*rxt(k,2501)*y(k,525) + &
2.000*rxt(k,2503)*y(k,623) +.750*rxt(k,2510)*y(k,344) +2.000*rxt(k,2519)*y(k,831) +4.000*rxt(k,2523)*y(k,741) + &
.070*rxt(k,2525)*y(k,737) +.723*rxt(k,2529)*y(k,677) +3.000*rxt(k,2532)*y(k,346) +3.000*rxt(k,2540)*y(k,39) + &
3.000*rxt(k,2543)*y(k,508) +2.000*rxt(k,2545)*y(k,605) +.102*rxt(k,2547)*y(k,550) +.039*rxt(k,2551)*y(k,729) + &
2.000*rxt(k,2553)*y(k,643) +2.166*rxt(k,2555)*y(k,636) +2.000*rxt(k,2558)*y(k,482) +1.737*rxt(k,2566)*y(k,447) + &
2.000*rxt(k,2577)*y(k,418) +3.000*rxt(k,2580)*y(k,826) +4.000*rxt(k,2584)*y(k,18) +.258*rxt(k,2587)*y(k,192) + &
.285*rxt(k,2591)*y(k,59) +1.612*rxt(k,2595)*y(k,575) +1.248*rxt(k,2599)*y(k,679) +.471*rxt(k,2602)*y(k,773) + &
3.000*rxt(k,2605)*y(k,528) +3.000*rxt(k,2608)*y(k,591) +4.000*rxt(k,2619)*y(k,606) +3.000*rxt(k,2629)*y(k,837) + &
1.860*rxt(k,2638)*y(k,552) +3.000*rxt(k,2648)*y(k,48) +3.000*rxt(k,2651)*y(k,500) +4.000*rxt(k,2655)*y(k,641) + &
3.000*rxt(k,2658)*y(k,394) +4.000*rxt(k,2662)*y(k,583) +3.000*rxt(k,2665)*y(k,570) +3.000*rxt(k,2668)*y(k,368) + &
3.000*rxt(k,2671)*y(k,634) +.508*rxt(k,2674)*y(k,35) +.252*rxt(k,2678)*y(k,20) +.759*rxt(k,2681)*y(k,45) + &
.060*rxt(k,2684)*y(k,112) +1.830*rxt(k,2687)*y(k,635) +3.248*rxt(k,2691)*y(k,518) +2.517*rxt(k,2694)*y(k,199) + &
2.232*rxt(k,2697)*y(k,286) +3.000*rxt(k,2699)*y(k,710) +.640*rxt(k,2704)*y(k,626) +2.121*rxt(k,2707)*y(k,465) + &
.480*rxt(k,2710)*y(k,592) +.436*rxt(k,3000)*y(k,653) +1.084*rxt(k,3811)*y(k,608) +1.028*rxt(k,3830)*y(k,481) + &
1.030*rxt(k,3838)*y(k,503) +1.542*rxt(k,3846)*y(k,522) +2.000*rxt(k,3855)*y(k,444) +rxt(k,3856)*y(k,540) + &
2.000*rxt(k,3865)*y(k,515) +1.500*rxt(k,3868)*y(k,483) +2.000*rxt(k,3870)*y(k,10) +rxt(k,3872)*y(k,778) + &
2.000*rxt(k,3873)*y(k,9) +rxt(k,3875)*y(k,539) +2.000*rxt(k,3877)*y(k,384) +2.000*rxt(k,3879)*y(k,441) + &
2.000*rxt(k,3884)*y(k,442) +2.000*rxt(k,3887)*y(k,345) +2.000*rxt(k,3889)*y(k,829) +1.074*rxt(k,3897)*y(k,580) + &
1.407*rxt(k,3905)*y(k,50) +3.000*rxt(k,3914)*y(k,250) +2.000*rxt(k,3916)*y(k,397) +2.000*rxt(k,3917)*y(k,203) + &
2.000*rxt(k,3919)*y(k,779) +2.000*rxt(k,3930)*y(k,379) +3.000*rxt(k,3942)*y(k,22) +2.000*rxt(k,3968)*y(k,747) + &
3.000*rxt(k,3972)*y(k,390) +2.000*rxt(k,3973)*y(k,541) +2.000*rxt(k,3975)*y(k,749) +2.000*rxt(k,3977)*y(k,750) + &
.087*rxt(k,3985)*y(k,806) +1.722*rxt(k,3991)*y(k,660) +2.799*rxt(k,4004)*y(k,480) +2.384*rxt(k,4014)*y(k,159) + &
.048*rxt(k,4025)*y(k,353) +3.000*rxt(k,4030)*y(k,401) +1.344*rxt(k,4033)*y(k,520) +3.000*rxt(k,4043)*y(k,439) + &
3.000*rxt(k,4055)*y(k,399) +2.000*rxt(k,4056)*y(k,753) +2.000*rxt(k,4070)*y(k,542) +2.000*rxt(k,4072)*y(k,754) + &
2.000*rxt(k,4111)*y(k,544) +2.000*rxt(k,4113)*y(k,543) +2.817*rxt(k,4196)*y(k,762))*y(k,705) &
 + (2.000*rxt(k,238)*y(k,512) +3.000*rxt(k,296)*y(k,784) +2.000*rxt(k,431)*y(k,758) +rxt(k,549)*y(k,590) + &
2.000*rxt(k,1078)*y(k,739) +3.000*rxt(k,1090)*y(k,52) +3.000*rxt(k,1099)*y(k,585) +3.000*rxt(k,1107)*y(k,385) + &
3.000*rxt(k,1125)*y(k,33) +.099*rxt(k,2516)*y(k,344) +2.000*rxt(k,2536)*y(k,346) +1.908*rxt(k,2572)*y(k,447) + &
3.000*rxt(k,2614)*y(k,591) +3.000*rxt(k,2625)*y(k,606) +3.000*rxt(k,2633)*y(k,837) +2.000*rxt(k,2643)*y(k,552) + &
1.626*rxt(k,3815)*y(k,608) +1.536*rxt(k,3834)*y(k,481) +1.476*rxt(k,3842)*y(k,503) +1.692*rxt(k,3851)*y(k,522) + &
2.000*rxt(k,3880)*y(k,441) +2.000*rxt(k,3894)*y(k,829) +1.539*rxt(k,3901)*y(k,580) +1.692*rxt(k,3910)*y(k,50) + &
2.000*rxt(k,3925)*y(k,779) +2.000*rxt(k,3936)*y(k,379) +1.389*rxt(k,3998)*y(k,660) +3.000*rxt(k,4010)*y(k,480) + &
1.959*rxt(k,4021)*y(k,159) +1.389*rxt(k,4039)*y(k,520) +2.000*rxt(k,4048)*y(k,439))*y(k,693) &
 + (.300*rxt(k,547)*y(k,590) +.200*rxt(k,556)*y(k,664) +.252*rxt(k,1087)*y(k,52) +2.001*rxt(k,1097)*y(k,585) + &
2.668*rxt(k,2612)*y(k,591) +2.000*rxt(k,3813)*y(k,608) +2.000*rxt(k,3832)*y(k,481) +2.000*rxt(k,3840)*y(k,503) + &
2.000*rxt(k,3849)*y(k,522) +2.000*rxt(k,3892)*y(k,829) +2.000*rxt(k,3899)*y(k,580) +2.000*rxt(k,3908)*y(k,50) + &
.189*rxt(k,3923)*y(k,779) +.189*rxt(k,3934)*y(k,379) +3.000*rxt(k,3994)*y(k,660) +3.000*rxt(k,4007)*y(k,480) + &
3.000*rxt(k,4017)*y(k,159) +3.000*rxt(k,4035)*y(k,520) +2.001*rxt(k,4046)*y(k,439))*y(k,696) &
 + (2.000*rxt(k,427)*y(k,690) +.880*rxt(k,430)*y(k,548) +2.000*rxt(k,434)*y(k,633) +2.000*rxt(k,435)*y(k,768) + &
2.000*rxt(k,437)*y(k,769) +2.000*rxt(k,440)*y(k,620) +4.000*rxt(k,441)*y(k,758) +2.000*rxt(k,455)*y(k,370) + &
2.000*rxt(k,472)*y(k,589) +2.000*rxt(k,3959)*y(k,427))*y(k,758) + (rxt(k,3883)*y(k,441) + &
3.000*rxt(k,3928)*y(k,779) +3.000*rxt(k,3939)*y(k,379) +3.000*rxt(k,4052)*y(k,439))*y(k,437) &
 + (.870*rxt(k,1071)*y(k,512) +rxt(k,1128)*y(k,33))*y(k,839) +3.000*rxt(k,2)*y(k,59) +3.000*rxt(k,23)*y(k,192) &
 +rxt(k,85)*y(k,590) +2.000*rxt(k,91)*y(k,625) +2.000*rxt(k,94)*y(k,634) +3.000*rxt(k,104)*y(k,653) &
 +.800*rxt(k,122)*y(k,708) +2.000*rxt(k,142)*y(k,737) +2.000*rxt(k,150)*y(k,757) +.330*rxt(k,155)*y(k,761) &
 +.788*rxt(k,158)*y(k,762)
         loss(k,856) = (rxt(k,4482)* y(k,370) +rxt(k,4478)* y(k,548) +rxt(k,4483)* y(k,589) +rxt(k,4480)* y(k,620) +rxt(k,4484) &
* y(k,633) +rxt(k,4477)* y(k,690) +rxt(k,4479)* y(k,693) +rxt(k,4481)* y(k,758) +rxt(k,4485)* y(k,768) &
 +rxt(k,4486)* y(k,769))* y(k,877)
         prod(k,856) = (.070*rxt(k,243)*y(k,738) +.102*rxt(k,245)*y(k,450) +.237*rxt(k,247)*y(k,678) +.126*rxt(k,251)*y(k,191) + &
.234*rxt(k,253)*y(k,451) +.435*rxt(k,255)*y(k,680) +.264*rxt(k,259)*y(k,93) +.672*rxt(k,261)*y(k,452) + &
.675*rxt(k,264)*y(k,681) +.420*rxt(k,267)*y(k,114) +.402*rxt(k,270)*y(k,453) +.590*rxt(k,272)*y(k,683) + &
.570*rxt(k,275)*y(k,86) +1.170*rxt(k,278)*y(k,454) +.708*rxt(k,280)*y(k,685) +.942*rxt(k,283)*y(k,85) + &
.980*rxt(k,286)*y(k,88) +.950*rxt(k,289)*y(k,455) +.796*rxt(k,291)*y(k,687) +.856*rxt(k,293)*y(k,669) + &
.003*rxt(k,304)*y(k,573) +.117*rxt(k,307)*y(k,625) +.201*rxt(k,310)*y(k,808) +.180*rxt(k,313)*y(k,517) + &
.234*rxt(k,316)*y(k,662) +.404*rxt(k,319)*y(k,504) +.520*rxt(k,323)*y(k,661) +.016*rxt(k,495)*y(k,770) + &
.087*rxt(k,499)*y(k,755) +.620*rxt(k,503)*y(k,756) +.240*rxt(k,518)*y(k,348) +.240*rxt(k,525)*y(k,349) + &
.468*rxt(k,532)*y(k,350) +.075*rxt(k,551)*y(k,664) +.164*rxt(k,561)*y(k,582) +.355*rxt(k,573)*y(k,731) + &
1.050*rxt(k,578)*y(k,761) +.279*rxt(k,584)*y(k,586) +.096*rxt(k,600)*y(k,359) +.285*rxt(k,604)*y(k,193) + &
.704*rxt(k,607)*y(k,95) +.555*rxt(k,610)*y(k,194) +.468*rxt(k,613)*y(k,280) +.492*rxt(k,617)*y(k,581) + &
.915*rxt(k,620)*y(k,621) +.836*rxt(k,623)*y(k,96) +.645*rxt(k,626)*y(k,115) +.663*rxt(k,629)*y(k,137) + &
.807*rxt(k,632)*y(k,195) +.952*rxt(k,636)*y(k,209) +.750*rxt(k,639)*y(k,281) +.639*rxt(k,642)*y(k,258) + &
1.548*rxt(k,646)*y(k,11) +1.492*rxt(k,650)*y(k,24) +1.524*rxt(k,654)*y(k,37) +1.548*rxt(k,658)*y(k,506) + &
.990*rxt(k,661)*y(k,622) +1.164*rxt(k,665)*y(k,97) +.849*rxt(k,668)*y(k,104) +.897*rxt(k,671)*y(k,116) + &
1.086*rxt(k,674)*y(k,138) +1.053*rxt(k,677)*y(k,146) +1.023*rxt(k,680)*y(k,196) +1.014*rxt(k,683)*y(k,282) + &
.972*rxt(k,686)*y(k,319) +1.080*rxt(k,690)*y(k,102) +1.300*rxt(k,694)*y(k,210) +1.048*rxt(k,698)*y(k,87) + &
.846*rxt(k,701)*y(k,217) +.822*rxt(k,704)*y(k,251) +1.756*rxt(k,708)*y(k,5) +1.820*rxt(k,712)*y(k,7) + &
1.464*rxt(k,715)*y(k,12) +1.744*rxt(k,720)*y(k,15) +1.768*rxt(k,724)*y(k,17) +1.832*rxt(k,728)*y(k,76) + &
1.242*rxt(k,731)*y(k,25) +1.380*rxt(k,734)*y(k,47) +1.744*rxt(k,738)*y(k,38) +1.092*rxt(k,741)*y(k,507) + &
1.828*rxt(k,745)*y(k,728) +1.320*rxt(k,749)*y(k,91) +1.137*rxt(k,752)*y(k,109) +1.233*rxt(k,755)*y(k,139) + &
1.239*rxt(k,758)*y(k,197) +1.059*rxt(k,761)*y(k,207) +1.365*rxt(k,764)*y(k,225) +1.101*rxt(k,767)*y(k,310) + &
1.185*rxt(k,770)*y(k,320) +1.600*rxt(k,774)*y(k,129) +1.472*rxt(k,778)*y(k,211) +1.161*rxt(k,781)*y(k,292) + &
1.011*rxt(k,785)*y(k,98) +1.668*rxt(k,788)*y(k,89) +1.167*rxt(k,791)*y(k,153) +1.110*rxt(k,794)*y(k,117) + &
1.242*rxt(k,797)*y(k,147) +1.179*rxt(k,800)*y(k,283) +1.074*rxt(k,803)*y(k,218) +1.176*rxt(k,806)*y(k,259) + &
1.596*rxt(k,810)*y(k,558) +1.956*rxt(k,813)*y(k,13) +2.032*rxt(k,817)*y(k,30) +1.904*rxt(k,821)*y(k,6) + &
1.443*rxt(k,825)*y(k,70) +1.131*rxt(k,828)*y(k,393) +1.386*rxt(k,830)*y(k,134) +1.341*rxt(k,833)*y(k,140) + &
1.299*rxt(k,836)*y(k,154) +1.347*rxt(k,839)*y(k,198) +1.125*rxt(k,842)*y(k,214) +1.347*rxt(k,845)*y(k,284) + &
1.284*rxt(k,848)*y(k,321) +1.221*rxt(k,851)*y(k,322) +1.320*rxt(k,854)*y(k,130) +1.736*rxt(k,858)*y(k,145) + &
1.608*rxt(k,862)*y(k,212) +1.248*rxt(k,865)*y(k,293) +1.188*rxt(k,869)*y(k,99) +1.720*rxt(k,872)*y(k,90) + &
1.368*rxt(k,876)*y(k,92) +1.278*rxt(k,879)*y(k,111) +1.245*rxt(k,882)*y(k,118) +1.410*rxt(k,885)*y(k,148) + &
1.221*rxt(k,888)*y(k,185) +1.296*rxt(k,891)*y(k,311) +1.419*rxt(k,894)*y(k,36) +1.470*rxt(k,898)*y(k,46) + &
1.760*rxt(k,901)*y(k,77) +1.236*rxt(k,904)*y(k,402) +.894*rxt(k,907)*y(k,670) +1.392*rxt(k,909)*y(k,103) + &
1.401*rxt(k,912)*y(k,155) +1.617*rxt(k,915)*y(k,224) +1.422*rxt(k,918)*y(k,274) +1.407*rxt(k,921)*y(k,318) + &
2.104*rxt(k,925)*y(k,41) +1.542*rxt(k,928)*y(k,69) +1.296*rxt(k,931)*y(k,405) +.916*rxt(k,934)*y(k,671) + &
1.569*rxt(k,936)*y(k,107) +1.446*rxt(k,939)*y(k,229) +1.518*rxt(k,942)*y(k,230) +1.452*rxt(k,945)*y(k,275) + &
1.428*rxt(k,948)*y(k,336) +2.096*rxt(k,952)*y(k,29) +1.554*rxt(k,956)*y(k,78) +.924*rxt(k,958)*y(k,409) + &
.930*rxt(k,960)*y(k,672) +1.551*rxt(k,962)*y(k,110) +1.647*rxt(k,965)*y(k,133) +1.536*rxt(k,968)*y(k,231) + &
1.500*rxt(k,971)*y(k,234) +1.470*rxt(k,974)*y(k,276) +1.458*rxt(k,977)*y(k,337) +2.040*rxt(k,981)*y(k,42) + &
1.557*rxt(k,984)*y(k,74) +.950*rxt(k,987)*y(k,411) +.940*rxt(k,989)*y(k,673) +1.686*rxt(k,991)*y(k,131) + &
1.602*rxt(k,994)*y(k,184) +1.512*rxt(k,997)*y(k,235) +1.587*rxt(k,1000)*y(k,238) +1.482*rxt(k,1003)*y(k,277) + &
1.464*rxt(k,1006)*y(k,338) +1.497*rxt(k,1010)*y(k,44) +1.632*rxt(k,1012)*y(k,79) +.958*rxt(k,1015)*y(k,413) + &
.946*rxt(k,1017)*y(k,674) +1.689*rxt(k,1019)*y(k,152) +1.539*rxt(k,1022)*y(k,236) +1.578*rxt(k,1025)*y(k,239) + &
1.485*rxt(k,1028)*y(k,278) +1.470*rxt(k,1031)*y(k,339) +1.479*rxt(k,1035)*y(k,31) +1.566*rxt(k,1037)*y(k,75) + &
.962*rxt(k,1040)*y(k,415) +2.164*rxt(k,1043)*y(k,40) +.950*rxt(k,1046)*y(k,675) +1.689*rxt(k,1048)*y(k,158) + &
1.488*rxt(k,1051)*y(k,279) +1.557*rxt(k,1054)*y(k,301) +.994*rxt(k,1057)*y(k,340) +1.494*rxt(k,1060)*y(k,43) + &
1.623*rxt(k,1062)*y(k,80) +.966*rxt(k,1065)*y(k,374) +.032*rxt(k,1072)*y(k,739) +.081*rxt(k,1082)*y(k,52) + &
.200*rxt(k,1093)*y(k,585) +.070*rxt(k,1102)*y(k,385) +.070*rxt(k,1110)*y(k,784) +.078*rxt(k,1115)*y(k,19) + &
.147*rxt(k,1117)*y(k,33) +.304*rxt(k,1130)*y(k,84) +.300*rxt(k,1143)*y(k,263) +.198*rxt(k,1154)*y(k,176) + &
.130*rxt(k,1165)*y(k,180) +.198*rxt(k,1175)*y(k,389) +.198*rxt(k,1186)*y(k,788) +.213*rxt(k,1198)*y(k,457) + &
.222*rxt(k,1208)*y(k,780) +.195*rxt(k,1218)*y(k,49) +.189*rxt(k,1230)*y(k,28) +.130*rxt(k,1233)*y(k,262) + &
.416*rxt(k,1234)*y(k,72) +.492*rxt(k,1246)*y(k,213) +.484*rxt(k,1256)*y(k,264) +.468*rxt(k,1269)*y(k,312) + &
.306*rxt(k,1282)*y(k,120) +.303*rxt(k,1291)*y(k,168) +.303*rxt(k,1302)*y(k,177) +.196*rxt(k,1313)*y(k,122) + &
.297*rxt(k,1321)*y(k,181) +.306*rxt(k,1331)*y(k,386) +.315*rxt(k,1343)*y(k,391) +.297*rxt(k,1353)*y(k,395) + &
.297*rxt(k,1365)*y(k,792) +.315*rxt(k,1378)*y(k,798) +.303*rxt(k,1389)*y(k,785) +.312*rxt(k,1401)*y(k,789) + &
.428*rxt(k,1412)*y(k,285) +.297*rxt(k,1424)*y(k,81) +.300*rxt(k,1436)*y(k,456) +.436*rxt(k,1447)*y(k,781) + &
.438*rxt(k,1461)*y(k,82) +.620*rxt(k,1472)*y(k,317) +.800*rxt(k,1484)*y(k,71) +.716*rxt(k,1495)*y(k,215) + &
.820*rxt(k,1508)*y(k,265) +.414*rxt(k,1521)*y(k,136) +.447*rxt(k,1533)*y(k,113) +.772*rxt(k,1545)*y(k,208) + &
.453*rxt(k,1557)*y(k,178) +.498*rxt(k,1569)*y(k,101) +.462*rxt(k,1579)*y(k,398) +.411*rxt(k,1591)*y(k,141) + &
.552*rxt(k,1604)*y(k,182) +.414*rxt(k,1617)*y(k,257) +.411*rxt(k,1628)*y(k,290) +.426*rxt(k,1641)*y(k,387) + &
.432*rxt(k,1654)*y(k,202) +.411*rxt(k,1664)*y(k,269) +.411*rxt(k,1678)*y(k,216) +.270*rxt(k,1689)*y(k,123) + &
.447*rxt(k,1698)*y(k,392) +.462*rxt(k,1710)*y(k,794) +.426*rxt(k,1721)*y(k,786) +.441*rxt(k,1733)*y(k,790) + &
.417*rxt(k,1744)*y(k,396) +.423*rxt(k,1756)*y(k,799) +.417*rxt(k,1769)*y(k,793) +.549*rxt(k,1782)*y(k,26) + &
.780*rxt(k,1790)*y(k,83) +.531*rxt(k,1801)*y(k,127) +.784*rxt(k,1813)*y(k,267) +.552*rxt(k,1824)*y(k,787) + &
.712*rxt(k,1837)*y(k,183) +.813*rxt(k,1850)*y(k,400) +.585*rxt(k,1861)*y(k,782) +.606*rxt(k,1873)*y(k,783) + &
.675*rxt(k,1883)*y(k,791) +.804*rxt(k,1894)*y(k,796) +.546*rxt(k,1904)*y(k,128) +.708*rxt(k,1916)*y(k,124) + &
.927*rxt(k,1927)*y(k,67) +1.144*rxt(k,1937)*y(k,291) +.831*rxt(k,1950)*y(k,797) +1.026*rxt(k,1961)*y(k,53) + &
.729*rxt(k,1971)*y(k,219) +1.125*rxt(k,1983)*y(k,404) +.936*rxt(k,1993)*y(k,795) +.300*rxt(k,2004)*y(k,249) + &
.788*rxt(k,2016)*y(k,355) +.736*rxt(k,2031)*y(k,879) +.680*rxt(k,2046)*y(k,479) +1.098*rxt(k,2060)*y(k,771) + &
.690*rxt(k,2070)*y(k,881) +1.092*rxt(k,2083)*y(k,55) +1.002*rxt(k,2093)*y(k,800) +1.134*rxt(k,2103)*y(k,56) + &
1.053*rxt(k,2113)*y(k,801) +1.164*rxt(k,2123)*y(k,57) +1.086*rxt(k,2133)*y(k,802) +1.185*rxt(k,2143)*y(k,58) + &
1.107*rxt(k,2153)*y(k,803) +1.119*rxt(k,2163)*y(k,804) +.216*rxt(k,2174)*y(k,833) +.344*rxt(k,2178)*y(k,383) + &
.328*rxt(k,2182)*y(k,665) +.435*rxt(k,2186)*y(k,706) +.380*rxt(k,2191)*y(k,746) +.360*rxt(k,2195)*y(k,776) + &
.452*rxt(k,2204)*y(k,676) +.396*rxt(k,2208)*y(k,572) +.505*rxt(k,2212)*y(k,644) +.550*rxt(k,2217)*y(k,704) + &
.480*rxt(k,2222)*y(k,721) +.715*rxt(k,2227)*y(k,14) +.605*rxt(k,2232)*y(k,16) +.516*rxt(k,2237)*y(k,32) + &
.384*rxt(k,2240)*y(k,360) +.520*rxt(k,2249)*y(k,373) +.384*rxt(k,2253)*y(k,807) +.575*rxt(k,2257)*y(k,596) + &
.635*rxt(k,2262)*y(k,697) +.555*rxt(k,2267)*y(k,711) +.525*rxt(k,2272)*y(k,719) +.855*rxt(k,2277)*y(k,240) + &
.730*rxt(k,2282)*y(k,302) +.616*rxt(k,2287)*y(k,324) +.424*rxt(k,2291)*y(k,816) +.132*rxt(k,2295)*y(k,667) + &
.584*rxt(k,2299)*y(k,376) +.640*rxt(k,2303)*y(k,597) +.705*rxt(k,2308)*y(k,698) +.630*rxt(k,2313)*y(k,712) + &
.895*rxt(k,2318)*y(k,241) +.770*rxt(k,2323)*y(k,303) +.648*rxt(k,2328)*y(k,325) +.200*rxt(k,2332)*y(k,630) + &
.628*rxt(k,2336)*y(k,377) +.675*rxt(k,2340)*y(k,598) +.745*rxt(k,2345)*y(k,699) +.670*rxt(k,2350)*y(k,713) + &
.920*rxt(k,2355)*y(k,242) +.795*rxt(k,2360)*y(k,304) +.664*rxt(k,2365)*y(k,326) +.264*rxt(k,2369)*y(k,119) + &
.660*rxt(k,2373)*y(k,378) +.564*rxt(k,2377)*y(k,599) +.780*rxt(k,2381)*y(k,700) +.710*rxt(k,2386)*y(k,714) + &
.940*rxt(k,2391)*y(k,243) +.815*rxt(k,2396)*y(k,305) +.680*rxt(k,2401)*y(k,327) +.688*rxt(k,2405)*y(k,380) + &
.584*rxt(k,2409)*y(k,600) +.810*rxt(k,2413)*y(k,701) +.745*rxt(k,2418)*y(k,715) +.940*rxt(k,2423)*y(k,244) + &
.825*rxt(k,2428)*y(k,306) +.680*rxt(k,2433)*y(k,328) +.704*rxt(k,2437)*y(k,381) +.600*rxt(k,2441)*y(k,601) + &
.830*rxt(k,2445)*y(k,702) +.765*rxt(k,2450)*y(k,716) +.950*rxt(k,2455)*y(k,245) +.835*rxt(k,2460)*y(k,307) + &
.688*rxt(k,2465)*y(k,329) +.720*rxt(k,2469)*y(k,382) +.616*rxt(k,2473)*y(k,602) +.845*rxt(k,2477)*y(k,703) + &
.790*rxt(k,2482)*y(k,717) +.960*rxt(k,2487)*y(k,246) +.850*rxt(k,2492)*y(k,308) +.696*rxt(k,2497)*y(k,330) + &
.032*rxt(k,2521)*y(k,741) +.009*rxt(k,2528)*y(k,677) +.030*rxt(k,2544)*y(k,605) +.069*rxt(k,2565)*y(k,447) + &
.146*rxt(k,2576)*y(k,418) +.237*rxt(k,2579)*y(k,826) +.236*rxt(k,2582)*y(k,18) +.012*rxt(k,2586)*y(k,192) + &
.024*rxt(k,2590)*y(k,59) +.148*rxt(k,2594)*y(k,575) +.039*rxt(k,2598)*y(k,679) +.018*rxt(k,2601)*y(k,773) + &
.228*rxt(k,2604)*y(k,528) +.117*rxt(k,2606)*y(k,591) +.168*rxt(k,2618)*y(k,606) +.117*rxt(k,2628)*y(k,837) + &
.072*rxt(k,2637)*y(k,552) +.240*rxt(k,2647)*y(k,48) +.120*rxt(k,2650)*y(k,500) +.172*rxt(k,2653)*y(k,641) + &
.117*rxt(k,2657)*y(k,394) +.156*rxt(k,2660)*y(k,583) +.117*rxt(k,2663)*y(k,570) +.117*rxt(k,2666)*y(k,368) + &
.117*rxt(k,2670)*y(k,634) +.020*rxt(k,2673)*y(k,35) +.009*rxt(k,2677)*y(k,20) +.030*rxt(k,2680)*y(k,45) + &
.003*rxt(k,2683)*y(k,112) +.030*rxt(k,2686)*y(k,635) +.080*rxt(k,2689)*y(k,518) +.099*rxt(k,2693)*y(k,199) + &
.090*rxt(k,2696)*y(k,286) +.024*rxt(k,2702)*y(k,626) +.084*rxt(k,2706)*y(k,465) +.020*rxt(k,2709)*y(k,592) + &
.354*rxt(k,2715)*y(k,419) +.117*rxt(k,2718)*y(k,420) +.564*rxt(k,2721)*y(k,354) +.501*rxt(k,2725)*y(k,828) + &
.195*rxt(k,2727)*y(k,595) +.009*rxt(k,2738)*y(k,94) +.048*rxt(k,2742)*y(k,270) +.072*rxt(k,2750)*y(k,60) + &
.195*rxt(k,2755)*y(k,659) +.201*rxt(k,2758)*y(k,464) +.228*rxt(k,2761)*y(k,649) +.088*rxt(k,2764)*y(k,164) + &
.045*rxt(k,2768)*y(k,247) +.105*rxt(k,2771)*y(k,407) +.140*rxt(k,2774)*y(k,563) +.148*rxt(k,2778)*y(k,190) + &
.264*rxt(k,2782)*y(k,523) +.243*rxt(k,2785)*y(k,654) +.282*rxt(k,2789)*y(k,501) +.130*rxt(k,2797)*y(k,628) + &
.027*rxt(k,2808)*y(k,531) +.195*rxt(k,2812)*y(k,135) +.304*rxt(k,2815)*y(k,825) +.288*rxt(k,2819)*y(k,526) + &
.220*rxt(k,2823)*y(k,579) +.268*rxt(k,2827)*y(k,610) +.300*rxt(k,2831)*y(k,624) +.296*rxt(k,2835)*y(k,403) + &
.264*rxt(k,2839)*y(k,726) +.268*rxt(k,2843)*y(k,268) +.210*rxt(k,2847)*y(k,100) +.176*rxt(k,2850)*y(k,516) + &
.171*rxt(k,2854)*y(k,206) +.220*rxt(k,2857)*y(k,260) +.220*rxt(k,2861)*y(k,287) +.308*rxt(k,2865)*y(k,603) + &
.155*rxt(k,2869)*y(k,513) +.276*rxt(k,2874)*y(k,648) +.236*rxt(k,2878)*y(k,657) +.006*rxt(k,2882)*y(k,725) + &
.534*rxt(k,2886)*y(k,421) +.165*rxt(k,2889)*y(k,422) +.120*rxt(k,2892)*y(k,62) +.396*rxt(k,2896)*y(k,646) + &
.408*rxt(k,2900)*y(k,655) +.453*rxt(k,2904)*y(k,663) +.234*rxt(k,2907)*y(k,61) +.129*rxt(k,2910)*y(k,165) + &
.172*rxt(k,2913)*y(k,647) +.318*rxt(k,2916)*y(k,740) +.348*rxt(k,2920)*y(k,498) +.396*rxt(k,2923)*y(k,584) + &
.315*rxt(k,2927)*y(k,514) +.432*rxt(k,2938)*y(k,505) +.480*rxt(k,2942)*y(k,566) +.688*rxt(k,2946)*y(k,642) + &
.488*rxt(k,2950)*y(k,361) +.420*rxt(k,2954)*y(k,743) +.440*rxt(k,2958)*y(k,772) +.624*rxt(k,2962)*y(k,805) + &
.344*rxt(k,2965)*y(k,474) +.468*rxt(k,2968)*y(k,615) +.066*rxt(k,2973)*y(k,410) +.080*rxt(k,2976)*y(k,179) + &
.456*rxt(k,2980)*y(k,493) +.294*rxt(k,2984)*y(k,342) +.219*rxt(k,2987)*y(k,742) +.448*rxt(k,2990)*y(k,366) + &
.280*rxt(k,2994)*y(k,289) +.052*rxt(k,2997)*y(k,653) +.348*rxt(k,3003)*y(k,730) +.496*rxt(k,3007)*y(k,554) + &
.508*rxt(k,3020)*y(k,724) +.444*rxt(k,3024)*y(k,449) +.440*rxt(k,3028)*y(k,204) +.504*rxt(k,3032)*y(k,640) + &
.440*rxt(k,3036)*y(k,470) +.156*rxt(k,3040)*y(k,490) +.452*rxt(k,3043)*y(k,459) +.300*rxt(k,3047)*y(k,509) + &
.336*rxt(k,3049)*y(k,347) +.243*rxt(k,3053)*y(k,811) +.144*rxt(k,3056)*y(k,369) +.272*rxt(k,3060)*y(k,372) + &
.201*rxt(k,3064)*y(k,64) +.147*rxt(k,3068)*y(k,200) +.579*rxt(k,3072)*y(k,412) +.489*rxt(k,3075)*y(k,186) + &
.486*rxt(k,3078)*y(k,476) +.507*rxt(k,3081)*y(k,332) +.716*rxt(k,3084)*y(k,266) +.414*rxt(k,3088)*y(k,63) + &
.392*rxt(k,3091)*y(k,121) +.604*rxt(k,3095)*y(k,294) +.828*rxt(k,3099)*y(k,668) +.824*rxt(k,3110)*y(k,567) + &
.680*rxt(k,3122)*y(k,367) +.666*rxt(k,3126)*y(k,352) +.592*rxt(k,3129)*y(k,727) +.736*rxt(k,3133)*y(k,562) + &
.696*rxt(k,3137)*y(k,175) +.816*rxt(k,3140)*y(k,616) +.308*rxt(k,3145)*y(k,722) +.345*rxt(k,3149)*y(k,723) + &
.345*rxt(k,3152)*y(k,365) +.636*rxt(k,3155)*y(k,492) +.548*rxt(k,3159)*y(k,478) +.536*rxt(k,3163)*y(k,473) + &
.500*rxt(k,3167)*y(k,491) +.556*rxt(k,3171)*y(k,489) +.624*rxt(k,3175)*y(k,27) +.690*rxt(k,3178)*y(k,460) + &
.616*rxt(k,3182)*y(k,823) +1.424*rxt(k,3186)*y(k,162) +.288*rxt(k,3190)*y(k,66) +.888*rxt(k,3194)*y(k,414) + &
.687*rxt(k,3197)*y(k,65) +.459*rxt(k,3200)*y(k,170) +.489*rxt(k,3203)*y(k,166) +.435*rxt(k,3206)*y(k,248) + &
.471*rxt(k,3209)*y(k,309) +.788*rxt(k,3212)*y(k,73) +.699*rxt(k,3215)*y(k,565) +.636*rxt(k,3218)*y(k,364) + &
.390*rxt(k,3222)*y(k,496) +.784*rxt(k,3227)*y(k,363) +.776*rxt(k,3240)*y(k,569) +.980*rxt(k,3253)*y(k,125) + &
1.156*rxt(k,3257)*y(k,187) +1.036*rxt(k,3261)*y(k,271) +1.176*rxt(k,3265)*y(k,313) +1.175*rxt(k,3269)*y(k,568) + &
.864*rxt(k,3274)*y(k,362) +.927*rxt(k,3278)*y(k,682) +1.264*rxt(k,3281)*y(k,593) +.852*rxt(k,3285)*y(k,408) + &
.864*rxt(k,3287)*y(k,171) +.906*rxt(k,3290)*y(k,617) +.292*rxt(k,3294)*y(k,167) +.636*rxt(k,3298)*y(k,495) + &
.140*rxt(k,3301)*y(k,832) +.336*rxt(k,3305)*y(k,744) +.820*rxt(k,3309)*y(k,161) +.900*rxt(k,3313)*y(k,174) + &
.992*rxt(k,3317)*y(k,468) +.760*rxt(k,3321)*y(k,485) +.868*rxt(k,3324)*y(k,461) +.844*rxt(k,3329)*y(k,471) + &
.796*rxt(k,3333)*y(k,821) +.712*rxt(k,3337)*y(k,813) +1.179*rxt(k,3341)*y(k,205) +.993*rxt(k,3344)*y(k,323) + &
.900*rxt(k,3347)*y(k,68) +.546*rxt(k,3351)*y(k,830) +1.071*rxt(k,3354)*y(k,416) +1.128*rxt(k,3357)*y(k,475) + &
.903*rxt(k,3360)*y(k,156) +.408*rxt(k,3364)*y(k,157) +1.628*rxt(k,3367)*y(k,142) +1.456*rxt(k,3371)*y(k,188) + &
1.236*rxt(k,3375)*y(k,254) +1.352*rxt(k,3379)*y(k,272) +1.436*rxt(k,3383)*y(k,314) +1.460*rxt(k,3387)*y(k,333) + &
1.192*rxt(k,3391)*y(k,576) +1.107*rxt(k,3395)*y(k,684) +1.092*rxt(k,3397)*y(k,618) +.780*rxt(k,3401)*y(k,51) + &
.804*rxt(k,3405)*y(k,488) +1.028*rxt(k,3409)*y(k,486) +1.092*rxt(k,3413)*y(k,487) +.936*rxt(k,3417)*y(k,824) + &
.495*rxt(k,3421)*y(k,834) +1.016*rxt(k,3424)*y(k,815) +.744*rxt(k,3428)*y(k,534) +1.389*rxt(k,3431)*y(k,417) + &
.952*rxt(k,3434)*y(k,356) +1.130*rxt(k,3447)*y(k,436) +.412*rxt(k,3462)*y(k,557) +.856*rxt(k,3466)*y(k,632) + &
.696*rxt(k,3469)*y(k,587) +1.188*rxt(k,3482)*y(k,375) +.900*rxt(k,3485)*y(k,571) +.849*rxt(k,3488)*y(k,54) + &
1.140*rxt(k,3491)*y(k,237) +.963*rxt(k,3494)*y(k,406) +.448*rxt(k,3498)*y(k,23) +1.928*rxt(k,3502)*y(k,143) + &
1.660*rxt(k,3506)*y(k,173) +1.484*rxt(k,3510)*y(k,220) +1.708*rxt(k,3514)*y(k,226) +1.122*rxt(k,3518)*y(k,255) + &
1.236*rxt(k,3521)*y(k,273) +1.592*rxt(k,3524)*y(k,295) +1.632*rxt(k,3528)*y(k,315) +1.636*rxt(k,3532)*y(k,334) + &
1.227*rxt(k,3536)*y(k,686) +1.155*rxt(k,3539)*y(k,529) +1.218*rxt(k,3555)*y(k,619) +.993*rxt(k,3559)*y(k,494) + &
.828*rxt(k,3562)*y(k,472) +.936*rxt(k,3565)*y(k,462) +1.116*rxt(k,3569)*y(k,469) +1.084*rxt(k,3573)*y(k,820) + &
1.040*rxt(k,3577)*y(k,835) +1.716*rxt(k,3581)*y(k,172) +1.820*rxt(k,3591)*y(k,108) +1.696*rxt(k,3595)*y(k,126) + &
2.040*rxt(k,3599)*y(k,144) +1.852*rxt(k,3603)*y(k,149) +1.836*rxt(k,3607)*y(k,189) +2.028*rxt(k,3611)*y(k,227) + &
1.908*rxt(k,3615)*y(k,232) +1.326*rxt(k,3619)*y(k,256) +1.712*rxt(k,3622)*y(k,296) +1.816*rxt(k,3626)*y(k,297) + &
1.317*rxt(k,3630)*y(k,316) +1.748*rxt(k,3633)*y(k,335) +1.305*rxt(k,3637)*y(k,688) +1.296*rxt(k,3639)*y(k,611) + &
1.628*rxt(k,3643)*y(k,564) +.696*rxt(k,3647)*y(k,151) +1.896*rxt(k,3650)*y(k,233) +1.347*rxt(k,3654)*y(k,261) + &
1.984*rxt(k,3657)*y(k,298) +1.344*rxt(k,3660)*y(k,612) +.928*rxt(k,3664)*y(k,818) +.645*rxt(k,3668)*y(k,817) + &
1.440*rxt(k,3673)*y(k,467) +.730*rxt(k,3677)*y(k,466) +1.323*rxt(k,3681)*y(k,458) +1.816*rxt(k,3685)*y(k,477) + &
1.324*rxt(k,3689)*y(k,814) +.992*rxt(k,3693)*y(k,812) +2.152*rxt(k,3697)*y(k,222) +1.964*rxt(k,3701)*y(k,253) + &
2.008*rxt(k,3705)*y(k,300) +1.377*rxt(k,3708)*y(k,627) +1.344*rxt(k,3712)*y(k,822) +2.136*rxt(k,3716)*y(k,106) + &
2.192*rxt(k,3720)*y(k,223) +2.112*rxt(k,3724)*y(k,228) +1.395*rxt(k,3727)*y(k,613) +1.760*rxt(k,3731)*y(k,169) + &
2.224*rxt(k,3736)*y(k,132) +1.485*rxt(k,3740)*y(k,252) +2.144*rxt(k,3743)*y(k,299) +1.407*rxt(k,3746)*y(k,629) + &
1.140*rxt(k,3749)*y(k,588) +1.392*rxt(k,3760)*y(k,614) +2.172*rxt(k,3763)*y(k,105) +2.244*rxt(k,3767)*y(k,221) + &
2.256*rxt(k,3771)*y(k,331) +.870*rxt(k,3775)*y(k,463) +2.650*rxt(k,3780)*y(k,836) +1.065*rxt(k,3784)*y(k,652) + &
1.203*rxt(k,3797)*y(k,651) +.090*rxt(k,3915)*y(k,397) +.045*rxt(k,3941)*y(k,22) +.100*rxt(k,3945)*y(k,748) + &
.126*rxt(k,3971)*y(k,390) +.003*rxt(k,3985)*y(k,806) +.105*rxt(k,3991)*y(k,660) +.080*rxt(k,3999)*y(k,497) + &
.111*rxt(k,4003)*y(k,480) +.096*rxt(k,4013)*y(k,159) +.003*rxt(k,4024)*y(k,353) +.240*rxt(k,4029)*y(k,401) + &
.054*rxt(k,4032)*y(k,520) +.120*rxt(k,4042)*y(k,439) +.240*rxt(k,4054)*y(k,399) +.016*rxt(k,4059)*y(k,160) + &
.450*rxt(k,4063)*y(k,689) +.130*rxt(k,4068)*y(k,639) +.622*rxt(k,4074)*y(k,810) +.096*rxt(k,4084)*y(k,443) + &
.096*rxt(k,4088)*y(k,692) +.096*rxt(k,4092)*y(k,438) +.249*rxt(k,4096)*y(k,521) +.096*rxt(k,4108)*y(k,371) + &
.196*rxt(k,4115)*y(k,524) +.288*rxt(k,4118)*y(k,656) +.200*rxt(k,4122)*y(k,426) +.200*rxt(k,4126)*y(k,718) + &
.492*rxt(k,4134)*y(k,658) +.477*rxt(k,4140)*y(k,499) +.453*rxt(k,4152)*y(k,574) +.740*rxt(k,4162)*y(k,720) + &
.528*rxt(k,4170)*y(k,827) +.111*rxt(k,4178)*y(k,732) +.284*rxt(k,4181)*y(k,733) +.244*rxt(k,4185)*y(k,734) + &
.228*rxt(k,4189)*y(k,735) +.324*rxt(k,4192)*y(k,736) +.222*rxt(k,4195)*y(k,762) +.096*rxt(k,4198)*y(k,763) + &
.540*rxt(k,4202)*y(k,764) +.444*rxt(k,4206)*y(k,765) +1.140*rxt(k,4210)*y(k,766) +1.680*rxt(k,4214)*y(k,767)) &
*y(k,705) + (.240*rxt(k,296)*y(k,784) +.204*rxt(k,569)*y(k,582) +.192*rxt(k,592)*y(k,586) + &
.102*rxt(k,1077)*y(k,739) +.240*rxt(k,1089)*y(k,52) +.117*rxt(k,1099)*y(k,585) +.240*rxt(k,1107)*y(k,385) + &
.195*rxt(k,1124)*y(k,33) +.498*rxt(k,1138)*y(k,84) +.447*rxt(k,1150)*y(k,263) +.195*rxt(k,1162)*y(k,176) + &
.195*rxt(k,1171)*y(k,180) +.402*rxt(k,1183)*y(k,389) +.402*rxt(k,1194)*y(k,788) +.375*rxt(k,1204)*y(k,457) + &
.300*rxt(k,1215)*y(k,780) +.348*rxt(k,1225)*y(k,49) +.474*rxt(k,1242)*y(k,72) +.376*rxt(k,1253)*y(k,213) + &
.672*rxt(k,1265)*y(k,264) +.464*rxt(k,1278)*y(k,312) +.268*rxt(k,1288)*y(k,120) +.294*rxt(k,1299)*y(k,168) + &
.519*rxt(k,1309)*y(k,177) +.196*rxt(k,1317)*y(k,122) +.345*rxt(k,1328)*y(k,181) +.663*rxt(k,1340)*y(k,386) + &
.660*rxt(k,1350)*y(k,391) +.294*rxt(k,1362)*y(k,395) +.294*rxt(k,1374)*y(k,792) +.618*rxt(k,1386)*y(k,798) + &
.663*rxt(k,1398)*y(k,785) +.660*rxt(k,1408)*y(k,789) +.872*rxt(k,1420)*y(k,285) +.303*rxt(k,1432)*y(k,81) + &
.495*rxt(k,1443)*y(k,456) +.624*rxt(k,1456)*y(k,781) +.426*rxt(k,1468)*y(k,82) +.800*rxt(k,1480)*y(k,317) + &
.600*rxt(k,1492)*y(k,71) +.951*rxt(k,1504)*y(k,215) +.909*rxt(k,1517)*y(k,265) +1.216*rxt(k,1529)*y(k,136) + &
.627*rxt(k,1540)*y(k,113) +.699*rxt(k,1554)*y(k,208) +.798*rxt(k,1565)*y(k,178) +.338*rxt(k,1575)*y(k,101) + &
1.048*rxt(k,1586)*y(k,398) +.836*rxt(k,1599)*y(k,141) +1.108*rxt(k,1613)*y(k,182) +.405*rxt(k,1625)*y(k,257) + &
.624*rxt(k,1637)*y(k,290) +.897*rxt(k,1650)*y(k,387) +.795*rxt(k,1661)*y(k,202) +.624*rxt(k,1673)*y(k,269) + &
.495*rxt(k,1686)*y(k,216) +.405*rxt(k,1694)*y(k,123) +.879*rxt(k,1706)*y(k,392) +1.048*rxt(k,1717)*y(k,794) + &
.897*rxt(k,1730)*y(k,786) +.879*rxt(k,1741)*y(k,790) +.471*rxt(k,1753)*y(k,396) +1.132*rxt(k,1765)*y(k,799) + &
.471*rxt(k,1778)*y(k,793) +.346*rxt(k,1787)*y(k,26) +.710*rxt(k,1797)*y(k,83) +1.768*rxt(k,1808)*y(k,127) + &
.618*rxt(k,1820)*y(k,267) +1.059*rxt(k,1833)*y(k,787) +1.364*rxt(k,1846)*y(k,183) +.710*rxt(k,1857)*y(k,400) + &
1.216*rxt(k,1868)*y(k,782) +.897*rxt(k,1880)*y(k,783) +1.062*rxt(k,1891)*y(k,791) +.710*rxt(k,1901)*y(k,796) + &
1.221*rxt(k,1912)*y(k,128) +.700*rxt(k,1923)*y(k,124) +.806*rxt(k,1934)*y(k,67) +1.272*rxt(k,1947)*y(k,291) + &
1.185*rxt(k,1958)*y(k,797) +.866*rxt(k,1968)*y(k,53) +1.292*rxt(k,1979)*y(k,219) +.846*rxt(k,1990)*y(k,404) + &
1.266*rxt(k,2001)*y(k,795) +.964*rxt(k,2011)*y(k,249) +1.172*rxt(k,2026)*y(k,355) +2.444*rxt(k,2041)*y(k,879) + &
1.184*rxt(k,2055)*y(k,479) +1.686*rxt(k,2066)*y(k,771) +1.372*rxt(k,2079)*y(k,881) +.900*rxt(k,2090)*y(k,55) + &
.880*rxt(k,2100)*y(k,800) +.922*rxt(k,2110)*y(k,56) +.906*rxt(k,2120)*y(k,801) +.936*rxt(k,2130)*y(k,57) + &
.924*rxt(k,2140)*y(k,802) +.944*rxt(k,2150)*y(k,58) +.934*rxt(k,2160)*y(k,803) +.940*rxt(k,2170)*y(k,804) + &
.390*rxt(k,2199)*y(k,776) +.390*rxt(k,2244)*y(k,360) +.006*rxt(k,2515)*y(k,344) +.152*rxt(k,2571)*y(k,447) + &
.117*rxt(k,2613)*y(k,591) +.150*rxt(k,2624)*y(k,606) +.117*rxt(k,2633)*y(k,837) +.080*rxt(k,2642)*y(k,552) + &
.195*rxt(k,2733)*y(k,595) +.008*rxt(k,2746)*y(k,270) +.318*rxt(k,2794)*y(k,501) +.249*rxt(k,2804)*y(k,628) + &
.528*rxt(k,2934)*y(k,514) +.420*rxt(k,3015)*y(k,554) +.620*rxt(k,3105)*y(k,668) +.860*rxt(k,3116)*y(k,567) + &
1.136*rxt(k,3235)*y(k,363) +1.060*rxt(k,3248)*y(k,569) +.726*rxt(k,3443)*y(k,356) +1.556*rxt(k,3457)*y(k,436) + &
1.164*rxt(k,3478)*y(k,587) +1.595*rxt(k,3550)*y(k,529) +1.359*rxt(k,3586)*y(k,172) +.940*rxt(k,3756)*y(k,588) + &
1.936*rxt(k,3793)*y(k,652) +2.060*rxt(k,3806)*y(k,651) +.102*rxt(k,3924)*y(k,779) +.102*rxt(k,3935)*y(k,379) + &
.054*rxt(k,3997)*y(k,660) +.117*rxt(k,4008)*y(k,480) +.078*rxt(k,4020)*y(k,159) +.054*rxt(k,4038)*y(k,520) + &
.078*rxt(k,4047)*y(k,439) +.300*rxt(k,4066)*y(k,689) +.933*rxt(k,4079)*y(k,810) +.294*rxt(k,4101)*y(k,521) + &
.600*rxt(k,4137)*y(k,658) +.600*rxt(k,4143)*y(k,499) +.618*rxt(k,4157)*y(k,574) +.690*rxt(k,4166)*y(k,720) + &
.690*rxt(k,4173)*y(k,827))*y(k,693) + (.042*rxt(k,537)*y(k,350) +.040*rxt(k,588)*y(k,586) + &
.004*rxt(k,1135)*y(k,84) +.012*rxt(k,1147)*y(k,263) +.104*rxt(k,1158)*y(k,176) +.015*rxt(k,1201)*y(k,457) + &
.016*rxt(k,1239)*y(k,72) +.008*rxt(k,1250)*y(k,213) +.025*rxt(k,1261)*y(k,264) +.015*rxt(k,1274)*y(k,312) + &
.172*rxt(k,1285)*y(k,120) +.172*rxt(k,1295)*y(k,168) +.172*rxt(k,1306)*y(k,177) +.005*rxt(k,1335)*y(k,386) + &
.140*rxt(k,1357)*y(k,395) +.140*rxt(k,1369)*y(k,792) +.015*rxt(k,1381)*y(k,798) +.005*rxt(k,1393)*y(k,785) + &
.044*rxt(k,1416)*y(k,285) +.284*rxt(k,1428)*y(k,81) +.064*rxt(k,1439)*y(k,456) +.010*rxt(k,1451)*y(k,781) + &
.404*rxt(k,1464)*y(k,82) +.092*rxt(k,1476)*y(k,317) +.032*rxt(k,1489)*y(k,71) +.055*rxt(k,1500)*y(k,215) + &
.050*rxt(k,1513)*y(k,265) +.260*rxt(k,1525)*y(k,136) +.260*rxt(k,1537)*y(k,113) +.015*rxt(k,1549)*y(k,208) + &
.260*rxt(k,1561)*y(k,178) +.260*rxt(k,1572)*y(k,101) +.008*rxt(k,1582)*y(k,398) +.010*rxt(k,1594)*y(k,141) + &
.005*rxt(k,1608)*y(k,182) +.180*rxt(k,1621)*y(k,257) +.140*rxt(k,1632)*y(k,290) +.020*rxt(k,1645)*y(k,387) + &
.012*rxt(k,1657)*y(k,202) +.140*rxt(k,1668)*y(k,269) +.225*rxt(k,1681)*y(k,216) +.060*rxt(k,1692)*y(k,123) + &
.004*rxt(k,1702)*y(k,392) +.008*rxt(k,1713)*y(k,794) +.020*rxt(k,1725)*y(k,786) +.004*rxt(k,1737)*y(k,790) + &
.225*rxt(k,1748)*y(k,396) +.025*rxt(k,1760)*y(k,799) +.225*rxt(k,1773)*y(k,793) +.519*rxt(k,1785)*y(k,26) + &
.048*rxt(k,1794)*y(k,83) +.360*rxt(k,1804)*y(k,127) +.360*rxt(k,1817)*y(k,267) +.040*rxt(k,1828)*y(k,787) + &
.015*rxt(k,1841)*y(k,183) +.012*rxt(k,1854)*y(k,400) +.008*rxt(k,1864)*y(k,782) +.020*rxt(k,1876)*y(k,783) + &
.016*rxt(k,1887)*y(k,791) +.012*rxt(k,1898)*y(k,796) +.004*rxt(k,1908)*y(k,128) +.096*rxt(k,1920)*y(k,124) + &
.064*rxt(k,1931)*y(k,67) +.080*rxt(k,1942)*y(k,291) +.024*rxt(k,1954)*y(k,797) +.084*rxt(k,1965)*y(k,53) + &
.484*rxt(k,1975)*y(k,219) +.036*rxt(k,1987)*y(k,404) +.036*rxt(k,1997)*y(k,795) +.700*rxt(k,2007)*y(k,249) + &
2.020*rxt(k,2020)*y(k,355) +.560*rxt(k,2035)*y(k,879) +.885*rxt(k,2050)*y(k,479) +.232*rxt(k,2063)*y(k,771) + &
.565*rxt(k,2074)*y(k,881) +.100*rxt(k,2087)*y(k,55) +.048*rxt(k,2097)*y(k,800) +.108*rxt(k,2107)*y(k,56) + &
.064*rxt(k,2117)*y(k,801) +.112*rxt(k,2127)*y(k,57) +.080*rxt(k,2137)*y(k,802) +.116*rxt(k,2147)*y(k,58) + &
.100*rxt(k,2157)*y(k,803) +.116*rxt(k,2167)*y(k,804) +.104*rxt(k,2800)*y(k,628) +.172*rxt(k,2930)*y(k,514) + &
.360*rxt(k,3231)*y(k,363) +.360*rxt(k,3244)*y(k,569) +.875*rxt(k,3438)*y(k,356) +.035*rxt(k,3452)*y(k,436) + &
.030*rxt(k,3473)*y(k,587) +.384*rxt(k,3544)*y(k,529) +.124*rxt(k,3753)*y(k,588) +.190*rxt(k,3788)*y(k,652) + &
.205*rxt(k,3801)*y(k,651) +.117*rxt(k,3994)*y(k,660) +.117*rxt(k,4006)*y(k,480) +.117*rxt(k,4017)*y(k,159) + &
.117*rxt(k,4035)*y(k,520) +.933*rxt(k,4077)*y(k,810) +.294*rxt(k,4099)*y(k,521) +.618*rxt(k,4155)*y(k,574)) &
*y(k,696) + (.153*rxt(k,3928)*y(k,779) +.153*rxt(k,3939)*y(k,379) +.117*rxt(k,4051)*y(k,439))*y(k,437) &
 + (.030*rxt(k,595)*y(k,586) +.060*rxt(k,1127)*y(k,33))*y(k,839) +.060*rxt(k,2)*y(k,59) +.207*rxt(k,5)*y(k,60) &
 +.338*rxt(k,8)*y(k,62) +.558*rxt(k,11)*y(k,64) +.756*rxt(k,14)*y(k,66) +.078*rxt(k,17)*y(k,94) +.040*rxt(k,21) &
*y(k,186) +.120*rxt(k,23)*y(k,192) +.474*rxt(k,26)*y(k,200) +.495*rxt(k,28)*y(k,266) +.159*rxt(k,31)*y(k,270) &
 +.238*rxt(k,34)*y(k,332) +.252*rxt(k,45)*y(k,412) +.372*rxt(k,47)*y(k,414) +.080*rxt(k,62)*y(k,476) &
 +.078*rxt(k,65)*y(k,531) +1.116*rxt(k,80)*y(k,557) +.159*rxt(k,97)*y(k,646) +.080*rxt(k,100)*y(k,649) &
 +.321*rxt(k,102)*y(k,653) +.138*rxt(k,106)*y(k,655) +.040*rxt(k,108)*y(k,659) +.078*rxt(k,110)*y(k,663) &
 +.261*rxt(k,126)*y(k,731) +.153*rxt(k,132)*y(k,733) +.140*rxt(k,135)*y(k,734) +.378*rxt(k,137)*y(k,735) &
 +.372*rxt(k,140)*y(k,736) +.308*rxt(k,143)*y(k,755) +.510*rxt(k,153)*y(k,761) +.324*rxt(k,161)*y(k,764) &
 +.225*rxt(k,165)*y(k,765) +.860*rxt(k,168)*y(k,766) +.480*rxt(k,172)*y(k,767)
         loss(k,10) = 0.
         prod(k,10) = 0.
         loss(k,846) = ((rxt(k,2040) +rxt(k,2041) +rxt(k,2042) +rxt(k,2043))* y(k,693) + (rxt(k,2034) +rxt(k,2035) +rxt(k,2036) + &
rxt(k,2037) +rxt(k,2038) +rxt(k,2039))* y(k,696) + (rxt(k,2030) +rxt(k,2031) +rxt(k,2032) +rxt(k,2033))* y(k,705) &
 +rxt(k,2044)* y(k,839))* y(k,879)
         prod(k,846) = 0.
         loss(k,11) = 0.
         prod(k,11) = 0.
         loss(k,869) = ((rxt(k,2078) +rxt(k,2079) +rxt(k,2080) +rxt(k,2081))* y(k,693) + (rxt(k,2073) +rxt(k,2074) +rxt(k,2075) + &
rxt(k,2076) +rxt(k,2077))* y(k,696) + (rxt(k,2070) +rxt(k,2071) +rxt(k,2072))* y(k,705) +rxt(k,2082)* y(k,839)) &
* y(k,881)
         prod(k,869) = 0.
         loss(k,12) = 0.
         prod(k,12) = 0.
         loss(k,13) = 0.
         prod(k,13) = 0.
         loss(k,92) = (rxt(k,4520)* y(k,885) + rxt(k,4517) + rxt(k,4518))* y(k,884)
         prod(k,92) =rxt(k,4519)*y(k,553)
         loss(k,79) = (rxt(k,4520)* y(k,884))* y(k,885)
         prod(k,79) = 0.
         loss(k,14) = 0.
         prod(k,14) = 0.
         loss(k,15) = 0.
         prod(k,15) = 0.
         loss(k,16) = 0.
         prod(k,16) = 0.
         loss(k,17) = 0.
         prod(k,17) = 0.
         loss(k,18) = 0.
         prod(k,18) = 0.
         loss(k,19) = 0.
         prod(k,19) = 0.
         loss(k,20) = 0.
         prod(k,20) = 0.
         loss(k,21) = 0.
         prod(k,21) = 0.
         loss(k,22) = 0.
         prod(k,22) = 0.
         loss(k,23) = 0.
         prod(k,23) = 0.
         loss(k,24) = 0.
         prod(k,24) = 0.
         loss(k,25) = 0.
         prod(k,25) = 0.
         loss(k,26) = 0.
         prod(k,26) = 0.
         loss(k,27) = 0.
         prod(k,27) = 0.
         loss(k,28) = 0.
         prod(k,28) = 0.
         loss(k,29) = 0.
         prod(k,29) = 0.
         loss(k,30) = 0.
         prod(k,30) = 0.
         loss(k,31) = 0.
         prod(k,31) = 0.
         loss(k,32) = 0.
         prod(k,32) = 0.
         loss(k,33) = 0.
         prod(k,33) = 0.
         loss(k,34) = 0.
         prod(k,34) = 0.
         loss(k,35) = 0.
         prod(k,35) = 0.
         loss(k,36) = 0.
         prod(k,36) = 0.
         loss(k,37) = 0.
         prod(k,37) = 0.
         loss(k,80) = ( + rxt(k,4521) + rxt(k,4522))* y(k,910)
         prod(k,80) =rxt(k,341)*y(k,691)
         loss(k,38) = 0.
         prod(k,38) = 0.
         loss(k,39) = 0.
         prod(k,39) = (.210*rxt(k,244)*y(k,738) +.306*rxt(k,246)*y(k,450) +-.114*rxt(k,249)*y(k,678) +.123*rxt(k,252)*y(k,191) + &
1.298*rxt(k,254)*y(k,451) +.474*rxt(k,257)*y(k,680) +-.248999*rxt(k,260)*y(k,93) +3.762*rxt(k,263)*y(k,452) + &
.483*rxt(k,266)*y(k,681) +.561*rxt(k,269)*y(k,114) +1.206*rxt(k,271)*y(k,453) +2.222*rxt(k,273)*y(k,683) + &
1.482*rxt(k,276)*y(k,86) +8.337*rxt(k,279)*y(k,454) +4.144*rxt(k,281)*y(k,685) +6.204*rxt(k,284)*y(k,85) + &
6.264*rxt(k,288)*y(k,88) +7.148*rxt(k,290)*y(k,455) +6.108*rxt(k,292)*y(k,687) +8.09*rxt(k,294)*y(k,669) + &
.002*rxt(k,299)*y(k,519) +3.842*rxt(k,301)*y(k,638) +.009*rxt(k,305)*y(k,573) +.900*rxt(k,308)*y(k,625) + &
-.039*rxt(k,311)*y(k,808) +2.829*rxt(k,314)*y(k,517) +3.246*rxt(k,317)*y(k,662) +5.260*rxt(k,321)*y(k,504) + &
7.664*rxt(k,325)*y(k,661) +rxt(k,327)*y(k,440) +rxt(k,329)*y(k,435) +2.000*rxt(k,335)*y(k,3) + &
1.764*rxt(k,337)*y(k,4) +.982*rxt(k,491)*y(k,425) +.910*rxt(k,493)*y(k,759) +-.46*rxt(k,498)*y(k,770) + &
.060*rxt(k,501)*y(k,755) +13.370*rxt(k,506)*y(k,756) +.300*rxt(k,507)*y(k,532) +10.10*rxt(k,513)*y(k,446) + &
1.136*rxt(k,521)*y(k,348) +1.136*rxt(k,528)*y(k,349) +3.752*rxt(k,535)*y(k,350) +-1.2479995*rxt(k,544)*y(k,590) + &
2.175*rxt(k,553)*y(k,664) +2.016*rxt(k,563)*y(k,582) +4.385*rxt(k,576)*y(k,731) +.240*rxt(k,582)*y(k,761) + &
.501*rxt(k,586)*y(k,586) +-3.9039996*rxt(k,602)*y(k,359) +.279*rxt(k,605)*y(k,193) +1.028*rxt(k,609)*y(k,95) + &
2.697*rxt(k,612)*y(k,194) +.444*rxt(k,615)*y(k,280) +.198*rxt(k,618)*y(k,581) +4.665*rxt(k,621)*y(k,621) + &
4.280*rxt(k,625)*y(k,96) +.963*rxt(k,628)*y(k,115) +4.467*rxt(k,631)*y(k,137) +3.330*rxt(k,634)*y(k,195) + &
1.360*rxt(k,638)*y(k,209) +3.777*rxt(k,641)*y(k,281) +2.877*rxt(k,644)*y(k,258) +9.2320013*rxt(k,648)*y(k,11) + &
8.040*rxt(k,652)*y(k,24) +8.0640011*rxt(k,656)*y(k,37) +7.636*rxt(k,660)*y(k,506) +6.690*rxt(k,663)*y(k,622) + &
7.828*rxt(k,667)*y(k,97) +2.430*rxt(k,670)*y(k,104) +3.183*rxt(k,673)*y(k,116) +4.485*rxt(k,676)*y(k,138) + &
4.140*rxt(k,679)*y(k,146) +6.249*rxt(k,682)*y(k,196) +5.379*rxt(k,685)*y(k,282) +6.597*rxt(k,688)*y(k,319) + &
2.124*rxt(k,692)*y(k,102) +5.820*rxt(k,696)*y(k,210) +4.072*rxt(k,700)*y(k,87) +2.409*rxt(k,703)*y(k,217) + &
3.270*rxt(k,706)*y(k,251) +10.276001*rxt(k,710)*y(k,5) +10.34*rxt(k,714)*y(k,7) +9.1120014*rxt(k,718)*y(k,12) + &
8.068*rxt(k,722)*y(k,15) +10.6920013*rxt(k,726)*y(k,17) +10.5240011*rxt(k,730)*y(k,76) + &
7.074*rxt(k,733)*y(k,25) +9.7770014*rxt(k,736)*y(k,47) +13.2720013*rxt(k,740)*y(k,38) + &
7.779*rxt(k,743)*y(k,507) +10.64*rxt(k,747)*y(k,728) +10.84*rxt(k,751)*y(k,91) +4.050*rxt(k,754)*y(k,109) + &
7.479*rxt(k,757)*y(k,139) +9.21*rxt(k,760)*y(k,197) +4.755*rxt(k,763)*y(k,207) +5.532*rxt(k,766)*y(k,225) + &
8.3970013*rxt(k,769)*y(k,310) +8.355*rxt(k,772)*y(k,320) +7.976*rxt(k,776)*y(k,129) +8.948*rxt(k,780)*y(k,211) + &
7.176*rxt(k,783)*y(k,292) +8.7810011*rxt(k,786)*y(k,98) +9.6520014*rxt(k,790)*y(k,89) + &
9.276001*rxt(k,793)*y(k,153) +5.847*rxt(k,796)*y(k,117) +5.337*rxt(k,799)*y(k,147) +8.646*rxt(k,802)*y(k,283) + &
5.139*rxt(k,805)*y(k,218) +8.1630011*rxt(k,808)*y(k,259) +11.532*rxt(k,811)*y(k,558) +12.712*rxt(k,815)*y(k,13) + &
16.864*rxt(k,819)*y(k,30) +13.1560011*rxt(k,823)*y(k,6) +11.1120014*rxt(k,826)*y(k,70) + &
9.558*rxt(k,829)*y(k,393) +10.242*rxt(k,832)*y(k,134) +10.5570011*rxt(k,835)*y(k,140) + &
11.6520014*rxt(k,838)*y(k,154) +12.207*rxt(k,841)*y(k,198) +8.1750011*rxt(k,844)*y(k,214) + &
11.922*rxt(k,847)*y(k,284) +11.493001*rxt(k,850)*y(k,321) +11.6940012*rxt(k,853)*y(k,322) + &
8.8380013*rxt(k,856)*y(k,130) +10.316*rxt(k,860)*y(k,145) +13.268*rxt(k,864)*y(k,212) + &
9.582*rxt(k,867)*y(k,293) +11.790*rxt(k,870)*y(k,99) +13.752*rxt(k,874)*y(k,90) +13.0080013*rxt(k,878)*y(k,92) + &
7.527*rxt(k,881)*y(k,111) +9.660*rxt(k,884)*y(k,118) +9.426*rxt(k,887)*y(k,148) +8.7180014*rxt(k,890)*y(k,185) + &
11.271*rxt(k,893)*y(k,311) +13.056*rxt(k,896)*y(k,36) +12.9840012*rxt(k,899)*y(k,46) +13.988*rxt(k,903)*y(k,77) + &
12.066*rxt(k,906)*y(k,402) +10.078001*rxt(k,908)*y(k,670) +8.433*rxt(k,911)*y(k,103) +14.67*rxt(k,914)*y(k,155) + &
12.171*rxt(k,917)*y(k,224) +14.988*rxt(k,920)*y(k,274) +14.787*rxt(k,923)*y(k,318) + &
21.5079994*rxt(k,927)*y(k,41) +15.2040014*rxt(k,930)*y(k,69) +15.2910013*rxt(k,933)*y(k,405) + &
12.078001*rxt(k,935)*y(k,671) +13.875001*rxt(k,938)*y(k,107) +15.186*rxt(k,941)*y(k,229) + &
16.464*rxt(k,944)*y(k,230) +17.9969997*rxt(k,947)*y(k,275) +17.7749996*rxt(k,950)*y(k,336) + &
24.448*rxt(k,954)*y(k,29) +18.819*rxt(k,957)*y(k,78) +12.162*rxt(k,959)*y(k,409) +14.06*rxt(k,961)*y(k,672) + &
14.703001*rxt(k,964)*y(k,110) +19.0739994*rxt(k,967)*y(k,133) +19.7369995*rxt(k,970)*y(k,231) + &
19.8299999*rxt(k,973)*y(k,234) +20.9969997*rxt(k,976)*y(k,276) +20.8349991*rxt(k,979)*y(k,337) + &
27.832*rxt(k,983)*y(k,42) +21.2579994*rxt(k,986)*y(k,74) +14.150*rxt(k,988)*y(k,411) + &
16.054*rxt(k,990)*y(k,673) +15.894001*rxt(k,993)*y(k,131) +18.663*rxt(k,996)*y(k,184) + &
23.3279991*rxt(k,999)*y(k,235) +23.670*rxt(k,1002)*y(k,238) +23.874*rxt(k,1005)*y(k,277) + &
23.739*rxt(k,1008)*y(k,338) +23.613*rxt(k,1011)*y(k,44) +24.7439995*rxt(k,1014)*y(k,79) + &
16.138*rxt(k,1016)*y(k,413) +18.048*rxt(k,1018)*y(k,674) +24.072*rxt(k,1021)*y(k,152) + &
26.448*rxt(k,1024)*y(k,236) +26.694*rxt(k,1027)*y(k,239) +26.8829994*rxt(k,1030)*y(k,278) + &
26.7479992*rxt(k,1033)*y(k,339) +26.4869995*rxt(k,1036)*y(k,31) +27.2189999*rxt(k,1039)*y(k,75) + &
18.138*rxt(k,1041)*y(k,415) +35.22*rxt(k,1045)*y(k,40) +20.048*rxt(k,1047)*y(k,675) + &
25.4039993*rxt(k,1050)*y(k,158) +29.8859997*rxt(k,1053)*y(k,279) +29.454*rxt(k,1056)*y(k,301) + &
19.898*rxt(k,1058)*y(k,340) +29.3759995*rxt(k,1061)*y(k,43) +30.6359997*rxt(k,1064)*y(k,80) + &
20.132*rxt(k,1066)*y(k,374) +.096*rxt(k,1073)*y(k,739) +-3.9999995*rxt(k,1081)*y(k,351) + &
.183*rxt(k,1084)*y(k,52) +.400*rxt(k,1094)*y(k,585) +.140*rxt(k,1103)*y(k,385) +.140*rxt(k,1111)*y(k,784) + &
1.238*rxt(k,1116)*y(k,19) +-3.8309996*rxt(k,1119)*y(k,33) +3.124*rxt(k,1133)*y(k,84) + &
2.420*rxt(k,1145)*y(k,263) +.213*rxt(k,1156)*y(k,176) +.130*rxt(k,1166)*y(k,180) +-.242999*rxt(k,1177)*y(k,389) + &
-.227999*rxt(k,1188)*y(k,788) +5.139*rxt(k,1199)*y(k,457) +4.596*rxt(k,1209)*y(k,780) +.888*rxt(k,1220)*y(k,49) + &
.429*rxt(k,1231)*y(k,28) +1.066*rxt(k,1233)*y(k,262) +4.804*rxt(k,1237)*y(k,72) +2.776*rxt(k,1248)*y(k,213) + &
5.640*rxt(k,1259)*y(k,264) +6.216*rxt(k,1272)*y(k,312) +2.166*rxt(k,1283)*y(k,120) +2.709*rxt(k,1293)*y(k,168) + &
2.610*rxt(k,1304)*y(k,177) +.006*rxt(k,1314)*y(k,122) +.015*rxt(k,1322)*y(k,181) +2.517*rxt(k,1333)*y(k,386) + &
.075*rxt(k,1345)*y(k,391) +.006*rxt(k,1355)*y(k,395) +.006*rxt(k,1367)*y(k,792) +2.445*rxt(k,1379)*y(k,798) + &
2.559*rxt(k,1391)*y(k,785) +.018*rxt(k,1403)*y(k,789) +10.268*rxt(k,1414)*y(k,285) +8.040*rxt(k,1426)*y(k,81) + &
7.719*rxt(k,1437)*y(k,456) +2.748*rxt(k,1449)*y(k,781) +10.434001*rxt(k,1462)*y(k,82) + &
13.256*rxt(k,1474)*y(k,317) +7.984*rxt(k,1487)*y(k,71) +7.400*rxt(k,1498)*y(k,215) +6.828*rxt(k,1511)*y(k,265) + &
.414*rxt(k,1523)*y(k,136) +.030*rxt(k,1535)*y(k,113) +3.184*rxt(k,1547)*y(k,208) +.729*rxt(k,1559)*y(k,178) + &
-1.6379995*rxt(k,1570)*y(k,101) +5.097*rxt(k,1580)*y(k,398) +2.961*rxt(k,1592)*y(k,141) + &
3.896*rxt(k,1606)*y(k,182) +3.030*rxt(k,1619)*y(k,257) +.420*rxt(k,1630)*y(k,290) +5.295*rxt(k,1643)*y(k,387) + &
2.898*rxt(k,1655)*y(k,202) +.429*rxt(k,1666)*y(k,269) +2.982*rxt(k,1679)*y(k,216) +.270*rxt(k,1690)*y(k,123) + &
2.817*rxt(k,1700)*y(k,392) +5.097*rxt(k,1711)*y(k,794) +5.304*rxt(k,1723)*y(k,786) +2.874*rxt(k,1735)*y(k,790) + &
2.931*rxt(k,1746)*y(k,396) +5.295*rxt(k,1758)*y(k,799) +2.931*rxt(k,1771)*y(k,793) +5.991*rxt(k,1783)*y(k,26) + &
8.6070013*rxt(k,1792)*y(k,83) +8.3850012*rxt(k,1802)*y(k,127) +3.780*rxt(k,1815)*y(k,267) + &
8.190*rxt(k,1826)*y(k,787) +7.836*rxt(k,1839)*y(k,183) +5.718*rxt(k,1852)*y(k,400) +5.787*rxt(k,1862)*y(k,782) + &
5.679*rxt(k,1874)*y(k,783) +5.820*rxt(k,1885)*y(k,791) +5.784*rxt(k,1896)*y(k,796) +5.928*rxt(k,1906)*y(k,128) + &
4.620*rxt(k,1918)*y(k,124) +11.370*rxt(k,1929)*y(k,67) +14.676*rxt(k,1940)*y(k,291) +8.76*rxt(k,1952)*y(k,797) + &
14.2110014*rxt(k,1963)*y(k,53) +7.455*rxt(k,1973)*y(k,219) +11.721*rxt(k,1985)*y(k,404) + &
11.7720013*rxt(k,1995)*y(k,795) +13.10*rxt(k,2005)*y(k,249) +10.530*rxt(k,2061)*y(k,771) + &
11.334*rxt(k,2072)*y(k,881) +17.0669994*rxt(k,2085)*y(k,55) +14.796*rxt(k,2095)*y(k,800) + &
20.0009995*rxt(k,2105)*y(k,56) +17.7689991*rxt(k,2115)*y(k,801) +22.8929996*rxt(k,2125)*y(k,57) + &
20.7929993*rxt(k,2135)*y(k,802) +25.8299999*rxt(k,2145)*y(k,58) +23.7989998*rxt(k,2155)*y(k,803) + &
26.808*rxt(k,2165)*y(k,804) +.436*rxt(k,2176)*y(k,833) +4.848*rxt(k,2180)*y(k,383) +2.780*rxt(k,2184)*y(k,665) + &
2.420*rxt(k,2189)*y(k,706) +1.995*rxt(k,2194)*y(k,746) +.720*rxt(k,2196)*y(k,776) +9.464*rxt(k,2206)*y(k,676) + &
8.740*rxt(k,2210)*y(k,572) +9.125001*rxt(k,2215)*y(k,644) +8.52*rxt(k,2220)*y(k,704) + &
7.640*rxt(k,2225)*y(k,721) +5.320*rxt(k,2230)*y(k,14) +5.950*rxt(k,2235)*y(k,16) +5.956*rxt(k,2239)*y(k,32) + &
.936*rxt(k,2241)*y(k,360) +13.72*rxt(k,2251)*y(k,373) +12.1280012*rxt(k,2255)*y(k,807) + &
14.46*rxt(k,2260)*y(k,596) +13.985*rxt(k,2265)*y(k,697) +13.120*rxt(k,2270)*y(k,711) + &
12.43*rxt(k,2275)*y(k,719) +11.1350012*rxt(k,2280)*y(k,240) +11.5150013*rxt(k,2285)*y(k,302) + &
10.308*rxt(k,2289)*y(k,324) +16.3199997*rxt(k,2293)*y(k,816) +19.7959995*rxt(k,2297)*y(k,667) + &
17.9519997*rxt(k,2301)*y(k,376) +19.905*rxt(k,2306)*y(k,597) +19.7649994*rxt(k,2311)*y(k,698) + &
18.7549992*rxt(k,2316)*y(k,712) +16.610*rxt(k,2321)*y(k,241) +16.9449997*rxt(k,2326)*y(k,303) + &
14.52*rxt(k,2330)*y(k,325) +22.4239998*rxt(k,2334)*y(k,630) +22.2439995*rxt(k,2338)*y(k,377) + &
25.125*rxt(k,2343)*y(k,598) +24.9899998*rxt(k,2348)*y(k,699) +23.9549999*rxt(k,2353)*y(k,713) + &
21.8799992*rxt(k,2358)*y(k,242) +22.1949997*rxt(k,2363)*y(k,304) +18.604*rxt(k,2367)*y(k,326) + &
25.0639992*rxt(k,2371)*y(k,119) +26.4279995*rxt(k,2375)*y(k,378) +24.2679996*rxt(k,2379)*y(k,599) + &
30.3799992*rxt(k,2384)*y(k,700) +29.465*rxt(k,2389)*y(k,714) +27.2399998*rxt(k,2394)*y(k,243) + &
27.540*rxt(k,2399)*y(k,305) +22.7639999*rxt(k,2403)*y(k,327) +30.572*rxt(k,2407)*y(k,380) + &
28.472*rxt(k,2411)*y(k,600) +35.72*rxt(k,2416)*y(k,701) +34.7449989*rxt(k,2421)*y(k,715) + &
32.5349998*rxt(k,2426)*y(k,244) +32.72*rxt(k,2431)*y(k,306) +26.888*rxt(k,2435)*y(k,328) + &
34.728*rxt(k,2439)*y(k,381) +32.632*rxt(k,2443)*y(k,601) +40.97*rxt(k,2448)*y(k,702) + &
40.055*rxt(k,2453)*y(k,716) +37.75*rxt(k,2458)*y(k,245) +37.985*rxt(k,2463)*y(k,307) + &
31.016*rxt(k,2467)*y(k,329) +38.812*rxt(k,2471)*y(k,382) +36.7799988*rxt(k,2475)*y(k,602) + &
46.25*rxt(k,2480)*y(k,703) +45.360*rxt(k,2485)*y(k,717) +42.97*rxt(k,2490)*y(k,246) + &
43.1199989*rxt(k,2495)*y(k,308) +35.104*rxt(k,2499)*y(k,330) +1.208*rxt(k,2501)*y(k,525) + &
1.208*rxt(k,2503)*y(k,623) +.001*rxt(k,2506)*y(k,510) +-1.9299995*rxt(k,2508)*y(k,533) + &
-2.2499995*rxt(k,2511)*y(k,344) +2.708*rxt(k,2523)*y(k,741) +.030*rxt(k,2529)*y(k,677) + &
2.331*rxt(k,2532)*y(k,346) +.630*rxt(k,2540)*y(k,39) +.279*rxt(k,2543)*y(k,508) +1.980*rxt(k,2545)*y(k,605) + &
-.101999*rxt(k,2548)*y(k,550) +1.938*rxt(k,2551)*y(k,729) +1.840*rxt(k,2553)*y(k,643) + &
3.420*rxt(k,2556)*y(k,636) +2.800*rxt(k,2558)*y(k,482) +-.365999*rxt(k,2560)*y(k,484) + &
3.288*rxt(k,2562)*y(k,535) +rxt(k,2563)*y(k,527) +.138*rxt(k,2566)*y(k,447) +.080*rxt(k,2577)*y(k,418) + &
1.662*rxt(k,2580)*y(k,826) +2.112*rxt(k,2584)*y(k,18) +2.730*rxt(k,2587)*y(k,192) +2.760*rxt(k,2591)*y(k,59) + &
2.128*rxt(k,2596)*y(k,575) +1.245*rxt(k,2599)*y(k,679) +.003*rxt(k,2602)*y(k,773) +1.314*rxt(k,2605)*y(k,528) + &
3.117*rxt(k,2608)*y(k,591) +-.92*rxt(k,2620)*y(k,606) +7.821*rxt(k,2629)*y(k,837) +4.128*rxt(k,2638)*y(k,552) + &
.810*rxt(k,2648)*y(k,48) +.591*rxt(k,2651)*y(k,500) +1.664*rxt(k,2655)*y(k,641) +1.497*rxt(k,2658)*y(k,394) + &
.244*rxt(k,2662)*y(k,583) +.312*rxt(k,2665)*y(k,570) +1.320*rxt(k,2668)*y(k,368) +4.524*rxt(k,2671)*y(k,634) + &
-3.5319996*rxt(k,2675)*y(k,35) +.774*rxt(k,2678)*y(k,20) +2.181*rxt(k,2681)*y(k,45) +5.772*rxt(k,2684)*y(k,112) + &
.849*rxt(k,2687)*y(k,635) +3.184*rxt(k,2691)*y(k,518) +7.023*rxt(k,2694)*y(k,199) +3.597*rxt(k,2697)*y(k,286) + &
.255*rxt(k,2700)*y(k,710) +.632*rxt(k,2704)*y(k,626) +5.400*rxt(k,2707)*y(k,465) +-.211999*rxt(k,2711)*y(k,592) + &
4.533*rxt(k,2716)*y(k,419) +3.441*rxt(k,2719)*y(k,420) +5.884*rxt(k,2723)*y(k,354) +3.168*rxt(k,2726)*y(k,828) + &
.738*rxt(k,2729)*y(k,595) +5.823*rxt(k,2739)*y(k,94) +6.948*rxt(k,2744)*y(k,270) +7.400*rxt(k,2752)*y(k,60) + &
.435*rxt(k,2756)*y(k,659) +2.034*rxt(k,2759)*y(k,464) +.462*rxt(k,2762)*y(k,649) +2.556*rxt(k,2766)*y(k,164) + &
2.451*rxt(k,2769)*y(k,247) +3.981*rxt(k,2772)*y(k,407) +4.608*rxt(k,2776)*y(k,563) +3.920*rxt(k,2780)*y(k,190) + &
-.224*rxt(k,2784)*y(k,523) +3.642*rxt(k,2787)*y(k,654) +2.298*rxt(k,2790)*y(k,501) +.130*rxt(k,2798)*y(k,628) + &
5.745*rxt(k,2809)*y(k,531) +.597*rxt(k,2813)*y(k,135) +2.756*rxt(k,2817)*y(k,825) +.036*rxt(k,2821)*y(k,526) + &
10.188*rxt(k,2825)*y(k,579) +3.068*rxt(k,2829)*y(k,610) +1.152*rxt(k,2833)*y(k,624) +3.704*rxt(k,2837)*y(k,403) + &
1.832*rxt(k,2841)*y(k,726) +2.792*rxt(k,2845)*y(k,268) +3.351*rxt(k,2848)*y(k,100) + &
-4.5079994*rxt(k,2852)*y(k,516) +2.739*rxt(k,2855)*y(k,206) +2.204*rxt(k,2859)*y(k,260) + &
9.504*rxt(k,2863)*y(k,287) +8.7320013*rxt(k,2867)*y(k,603) +-1.7999995*rxt(k,2872)*y(k,513) + &
9.7480011*rxt(k,2876)*y(k,648) +2.760*rxt(k,2880)*y(k,657) +5.376*rxt(k,2883)*y(k,725) +rxt(k,2884)*y(k,150) + &
5.403*rxt(k,2887)*y(k,421) +1.986*rxt(k,2890)*y(k,422) +8.511*rxt(k,2893)*y(k,62) +.560*rxt(k,2898)*y(k,646) + &
2.568*rxt(k,2902)*y(k,655) +.375*rxt(k,2905)*y(k,663) +5.661*rxt(k,2908)*y(k,61) +.456*rxt(k,2911)*y(k,165) + &
.940*rxt(k,2915)*y(k,647) +.342*rxt(k,2918)*y(k,740) +.465*rxt(k,2921)*y(k,498) +3.416*rxt(k,2925)*y(k,584) + &
1.977*rxt(k,2928)*y(k,514) +4.612*rxt(k,2940)*y(k,505) +6.060*rxt(k,2944)*y(k,566) +.724*rxt(k,2948)*y(k,642) + &
3.812*rxt(k,2952)*y(k,361) +3.236*rxt(k,2956)*y(k,743) +11.092001*rxt(k,2960)*y(k,772) + &
7.107*rxt(k,2963)*y(k,805) +3.344*rxt(k,2967)*y(k,474) +5.192*rxt(k,2971)*y(k,615) +2.817*rxt(k,2974)*y(k,410) + &
6.284*rxt(k,2978)*y(k,179) +-6.9999995*rxt(k,2982)*y(k,493) +.282*rxt(k,2985)*y(k,342) + &
2.016*rxt(k,2988)*y(k,742) +.824*rxt(k,2992)*y(k,366) +9.604001*rxt(k,2996)*y(k,289) + &
11.684001*rxt(k,3000)*y(k,653) +-.804*rxt(k,3005)*y(k,730) +.912*rxt(k,3009)*y(k,554) + &
10.096*rxt(k,3022)*y(k,724) +1.612*rxt(k,3026)*y(k,449) +14.252*rxt(k,3030)*y(k,204) +8.30*rxt(k,3034)*y(k,640) + &
5.920*rxt(k,3038)*y(k,470) +2.904*rxt(k,3041)*y(k,490) +6.920*rxt(k,3045)*y(k,459) +2.961*rxt(k,3048)*y(k,509) + &
.870*rxt(k,3051)*y(k,347) +4.932*rxt(k,3054)*y(k,811) +8.072*rxt(k,3058)*y(k,369) +.068*rxt(k,3062)*y(k,372) + &
11.370*rxt(k,3065)*y(k,64) +10.320*rxt(k,3069)*y(k,200) +3.798*rxt(k,3073)*y(k,412) +4.878*rxt(k,3076)*y(k,186) + &
3.780*rxt(k,3079)*y(k,476) +3.159*rxt(k,3082)*y(k,332) +2.488*rxt(k,3086)*y(k,266) +7.761*rxt(k,3089)*y(k,63) + &
7.584*rxt(k,3093)*y(k,121) +5.968*rxt(k,3097)*y(k,294) +4.876*rxt(k,3101)*y(k,668) +2.008*rxt(k,3112)*y(k,567) + &
6.716*rxt(k,3124)*y(k,367) +3.267*rxt(k,3127)*y(k,352) +7.192*rxt(k,3131)*y(k,727) +4.868*rxt(k,3135)*y(k,562) + &
6.008*rxt(k,3139)*y(k,175) +5.804*rxt(k,3143)*y(k,616) +3.804*rxt(k,3147)*y(k,722) +14.955*rxt(k,3150)*y(k,723) + &
.852*rxt(k,3153)*y(k,365) +2.208*rxt(k,3157)*y(k,492) +6.288*rxt(k,3161)*y(k,478) + &
-2.9159994*rxt(k,3165)*y(k,473) +2.008*rxt(k,3169)*y(k,491) +4.108*rxt(k,3173)*y(k,489) + &
15.3360014*rxt(k,3177)*y(k,27) +5.343*rxt(k,3180)*y(k,460) +3.340*rxt(k,3184)*y(k,823) + &
14.30*rxt(k,3188)*y(k,162) +14.1120014*rxt(k,3191)*y(k,66) +6.678*rxt(k,3195)*y(k,414) + &
9.3120012*rxt(k,3198)*y(k,65) +8.667*rxt(k,3201)*y(k,170) +6.642*rxt(k,3204)*y(k,166) + &
6.570*rxt(k,3207)*y(k,248) +6.000*rxt(k,3210)*y(k,309) +14.368001*rxt(k,3214)*y(k,73) + &
3.258*rxt(k,3217)*y(k,565) +2.901*rxt(k,3220)*y(k,364) +9.14*rxt(k,3225)*y(k,496) +10.212*rxt(k,3229)*y(k,363) + &
9.736001*rxt(k,3242)*y(k,569) +6.468*rxt(k,3255)*y(k,125) +9.9440012*rxt(k,3259)*y(k,187) + &
6.060*rxt(k,3263)*y(k,271) +10.580*rxt(k,3267)*y(k,313) +6.760*rxt(k,3272)*y(k,568) + &
10.664*rxt(k,3276)*y(k,362) +6.150*rxt(k,3279)*y(k,682) +9.528*rxt(k,3283)*y(k,593) +6.381*rxt(k,3286)*y(k,408) + &
5.019*rxt(k,3289)*y(k,171) +6.342*rxt(k,3292)*y(k,617) +9.9960012*rxt(k,3296)*y(k,167) + &
3.591*rxt(k,3299)*y(k,495) +11.84*rxt(k,3303)*y(k,832) +1.760*rxt(k,3307)*y(k,744) +5.700*rxt(k,3311)*y(k,161) + &
7.704*rxt(k,3315)*y(k,174) +1.380*rxt(k,3319)*y(k,468) +.836*rxt(k,3323)*y(k,485) +9.792*rxt(k,3327)*y(k,461) + &
1.060*rxt(k,3331)*y(k,471) +4.768*rxt(k,3335)*y(k,821) +4.196*rxt(k,3339)*y(k,813) +11.334*rxt(k,3342)*y(k,205) + &
10.389*rxt(k,3345)*y(k,323) +16.9519997*rxt(k,3349)*y(k,68) +10.914*rxt(k,3352)*y(k,830) + &
9.5010014*rxt(k,3355)*y(k,416) +8.9280014*rxt(k,3359)*y(k,475) +8.922*rxt(k,3362)*y(k,156) + &
8.9490013*rxt(k,3365)*y(k,157) +12.9320011*rxt(k,3369)*y(k,142) +11.4320011*rxt(k,3373)*y(k,188) + &
9.796*rxt(k,3377)*y(k,254) +11.5760012*rxt(k,3381)*y(k,272) +13.328001*rxt(k,3385)*y(k,314) + &
12.236001*rxt(k,3389)*y(k,333) +12.144001*rxt(k,3393)*y(k,576) +9.117*rxt(k,3396)*y(k,684) + &
9.27*rxt(k,3399)*y(k,618) +3.172*rxt(k,3403)*y(k,51) +2.672*rxt(k,3407)*y(k,488) +8.988*rxt(k,3411)*y(k,486) + &
9.0760012*rxt(k,3415)*y(k,487) +.064*rxt(k,3419)*y(k,824) +1.044*rxt(k,3422)*y(k,834) + &
.312*rxt(k,3426)*y(k,815) +9.9960012*rxt(k,3429)*y(k,534) +12.981*rxt(k,3432)*y(k,417) + &
24.180*rxt(k,3436)*y(k,356) +20.260*rxt(k,3450)*y(k,436) +25.1679993*rxt(k,3464)*y(k,557) + &
18.2439995*rxt(k,3468)*y(k,632) +11.5170012*rxt(k,3471)*y(k,587) +12.285*rxt(k,3483)*y(k,375) + &
15.3450012*rxt(k,3486)*y(k,571) +15.294*rxt(k,3489)*y(k,54) +21.016*rxt(k,3493)*y(k,237) + &
6.072*rxt(k,3496)*y(k,406) +9.9960012*rxt(k,3500)*y(k,23) +14.440*rxt(k,3504)*y(k,143) + &
15.004*rxt(k,3508)*y(k,173) +10.4840012*rxt(k,3512)*y(k,220) +13.2040014*rxt(k,3516)*y(k,226) + &
10.650*rxt(k,3519)*y(k,255) +11.493001*rxt(k,3522)*y(k,273) +13.604001*rxt(k,3526)*y(k,295) + &
16.9519997*rxt(k,3530)*y(k,315) +15.712*rxt(k,3534)*y(k,334) +12.105*rxt(k,3537)*y(k,686) + &
15.955*rxt(k,3542)*y(k,529) +12.129*rxt(k,3557)*y(k,619) +7.095*rxt(k,3560)*y(k,494) + &
5.703*rxt(k,3563)*y(k,472) +6.356*rxt(k,3567)*y(k,462) +8.480*rxt(k,3571)*y(k,469) +3.944*rxt(k,3575)*y(k,820) + &
7.456*rxt(k,3579)*y(k,835) +20.544*rxt(k,3583)*y(k,172) +13.516*rxt(k,3593)*y(k,108) + &
15.292*rxt(k,3597)*y(k,126) +17.312*rxt(k,3601)*y(k,144) +15.80*rxt(k,3605)*y(k,149) + &
19.8799992*rxt(k,3609)*y(k,189) +15.896*rxt(k,3613)*y(k,227) +15.7320013*rxt(k,3617)*y(k,232) + &
14.4060011*rxt(k,3620)*y(k,256) +15.488*rxt(k,3624)*y(k,296) +18.840*rxt(k,3628)*y(k,297) + &
14.9040012*rxt(k,3631)*y(k,316) +19.540*rxt(k,3635)*y(k,335) +15.075*rxt(k,3638)*y(k,688) + &
15.1140013*rxt(k,3641)*y(k,611) +20.1679993*rxt(k,3645)*y(k,564) +17.9459991*rxt(k,3648)*y(k,151) + &
19.964*rxt(k,3652)*y(k,233) +15.792*rxt(k,3655)*y(k,261) +21.5559998*rxt(k,3659)*y(k,298) + &
18.0839996*rxt(k,3662)*y(k,612) +26.552*rxt(k,3666)*y(k,818) +27.495*rxt(k,3671)*y(k,817) + &
13.816*rxt(k,3675)*y(k,467) +27.9899998*rxt(k,3680)*y(k,466) +18.090*rxt(k,3683)*y(k,458) + &
25.684*rxt(k,3687)*y(k,477) +9.72*rxt(k,3691)*y(k,814) +24.496*rxt(k,3695)*y(k,812) + &
24.8279991*rxt(k,3699)*y(k,222) +24.232*rxt(k,3703)*y(k,253) +27.0599995*rxt(k,3707)*y(k,300) + &
21.0839996*rxt(k,3710)*y(k,627) +15.584*rxt(k,3714)*y(k,822) +26.4759998*rxt(k,3718)*y(k,106) + &
29.1639996*rxt(k,3722)*y(k,223) +29.4599991*rxt(k,3726)*y(k,228) +24.090*rxt(k,3729)*y(k,613) + &
44.9749985*rxt(k,3734)*y(k,169) +34.1599998*rxt(k,3738)*y(k,132) +22.9589996*rxt(k,3741)*y(k,252) + &
35.492*rxt(k,3745)*y(k,299) +27.0569992*rxt(k,3748)*y(k,629) +29.739*rxt(k,3751)*y(k,588) + &
30.1289997*rxt(k,3761)*y(k,614) +30.2639999*rxt(k,3765)*y(k,105) +38.0239983*rxt(k,3769)*y(k,221) + &
31.8199997*rxt(k,3773)*y(k,331) +48.5299988*rxt(k,3778)*y(k,463) +32.1049995*rxt(k,3783)*y(k,836) + &
35.076*rxt(k,3786)*y(k,652) +37.355999*rxt(k,3799)*y(k,651) +10.00*rxt(k,3811)*y(k,608) +rxt(k,3820)*y(k,448) + &
5.084*rxt(k,3830)*y(k,481) +8.00*rxt(k,3838)*y(k,503) +12.705*rxt(k,3847)*y(k,522) +.702*rxt(k,3855)*y(k,444) + &
rxt(k,3857)*y(k,650) +rxt(k,3868)*y(k,483) +-3.9999995*rxt(k,3871)*y(k,10) +3.698*rxt(k,3874)*y(k,9) + &
2.000*rxt(k,3875)*y(k,539) +3.000*rxt(k,3879)*y(k,441) +-3.9999995*rxt(k,3885)*y(k,442) + &
4.000*rxt(k,3888)*y(k,345) +6.000*rxt(k,3890)*y(k,829) +7.074*rxt(k,3897)*y(k,580) + &
-11.8139992*rxt(k,3906)*y(k,50) +.051*rxt(k,3914)*y(k,250) +4.686*rxt(k,3916)*y(k,397) + &
2.000*rxt(k,3920)*y(k,779) +2.000*rxt(k,3931)*y(k,379) +.690*rxt(k,3943)*y(k,22) +.300*rxt(k,3945)*y(k,748) + &
4.000*rxt(k,3969)*y(k,747) +.378*rxt(k,3972)*y(k,390) +6.000*rxt(k,3974)*y(k,541) +6.000*rxt(k,3976)*y(k,749) + &
6.000*rxt(k,3978)*y(k,750) +rxt(k,3979)*y(k,34) +1.340*rxt(k,3983)*y(k,502) +.342*rxt(k,3986)*y(k,806) + &
2.166*rxt(k,3992)*y(k,660) +4.000*rxt(k,4000)*y(k,497) +1.686*rxt(k,4004)*y(k,480) + &
-9.5719986*rxt(k,4015)*y(k,159) +.594*rxt(k,4025)*y(k,353) +.294*rxt(k,4030)*y(k,401) + &
2.322*rxt(k,4033)*y(k,520) +.288*rxt(k,4043)*y(k,439) +.294*rxt(k,4055)*y(k,399) +8.00*rxt(k,4057)*y(k,753) + &
4.412*rxt(k,4061)*y(k,160) +.825*rxt(k,4065)*y(k,689) +9.22*rxt(k,4069)*y(k,639) +10.00*rxt(k,4071)*y(k,542) + &
10.00*rxt(k,4073)*y(k,754) +4.134*rxt(k,4075)*y(k,810) +-3.9039996*rxt(k,4086)*y(k,443) + &
-3.9039996*rxt(k,4090)*y(k,692) +-3.9039996*rxt(k,4094)*y(k,438) +.939*rxt(k,4097)*y(k,521) + &
6.000*rxt(k,4105)*y(k,774) +6.000*rxt(k,4106)*y(k,775) +-3.9039996*rxt(k,4110)*y(k,371) + &
10.1960011*rxt(k,4112)*y(k,544) +10.1960011*rxt(k,4114)*y(k,543) +10.71*rxt(k,4116)*y(k,524) + &
.084*rxt(k,4120)*y(k,656) +.480*rxt(k,4124)*y(k,426) +.480*rxt(k,4128)*y(k,718) +4.000*rxt(k,4130)*y(k,745) + &
8.00*rxt(k,4131)*y(k,1) +2.000*rxt(k,4132)*y(k,809) +12.3120012*rxt(k,4135)*y(k,658) + &
4.692*rxt(k,4141)*y(k,499) +5.514*rxt(k,4153)*y(k,574) +9.256*rxt(k,4163)*y(k,720) +10.00*rxt(k,4168)*y(k,2) + &
19.7759991*rxt(k,4171)*y(k,827) +18.*rxt(k,4176)*y(k,604) +3.822*rxt(k,4179)*y(k,732) + &
5.936*rxt(k,4183)*y(k,733) +5.588*rxt(k,4187)*y(k,734) +6.456*rxt(k,4190)*y(k,735) + &
9.2670012*rxt(k,4193)*y(k,736) +2.568*rxt(k,4196)*y(k,762) +4.924*rxt(k,4200)*y(k,763) + &
.688*rxt(k,4204)*y(k,764) +3.880*rxt(k,4208)*y(k,765) +6.240*rxt(k,4212)*y(k,766) + &
16.0039997*rxt(k,4216)*y(k,767))*y(k,705) + (-1.9999995*rxt(k,239)*y(k,512) +1.770*rxt(k,297)*y(k,784) + &
-.7*rxt(k,509)*y(k,532) +rxt(k,514)*y(k,446) +3.000*rxt(k,549)*y(k,590) +4.000*rxt(k,558)*y(k,664) + &
3.260*rxt(k,571)*y(k,582) +.192*rxt(k,593)*y(k,586) +5.388*rxt(k,1078)*y(k,739) +-6.4499993*rxt(k,1091)*y(k,52) + &
2.622*rxt(k,1100)*y(k,585) +1.770*rxt(k,1108)*y(k,385) +3.225*rxt(k,1125)*y(k,33) + &
-3.1439996*rxt(k,1140)*y(k,84) +-10.7399988*rxt(k,1152)*y(k,263) +8.049*rxt(k,1163)*y(k,176) + &
-.194999*rxt(k,1173)*y(k,180) +1.647*rxt(k,1184)*y(k,389) +1.647*rxt(k,1195)*y(k,788) + &
4.290*rxt(k,1205)*y(k,457) +5.115*rxt(k,1216)*y(k,780) +-13.6079988*rxt(k,1227)*y(k,49) + &
6.892*rxt(k,1254)*y(k,213) +7.800*rxt(k,1267)*y(k,264) +5.196*rxt(k,1289)*y(k,120) +5.331*rxt(k,1300)*y(k,168) + &
1.422*rxt(k,1311)*y(k,177) +.006*rxt(k,1318)*y(k,122) +.297*rxt(k,1341)*y(k,386) +.060*rxt(k,1351)*y(k,391) + &
.180*rxt(k,1363)*y(k,395) +.180*rxt(k,1375)*y(k,792) +3.072*rxt(k,1387)*y(k,798) +.297*rxt(k,1399)*y(k,785) + &
.060*rxt(k,1409)*y(k,789) +7.788*rxt(k,1422)*y(k,285) +7.944*rxt(k,1433)*y(k,81) +2.664*rxt(k,1444)*y(k,456) + &
5.812*rxt(k,1458)*y(k,781) +10.5240011*rxt(k,1469)*y(k,82) +9.1960011*rxt(k,1482)*y(k,317) + &
2.000*rxt(k,1493)*y(k,71) +4.539*rxt(k,1506)*y(k,215) +1.413*rxt(k,1519)*y(k,265) +3.700*rxt(k,1531)*y(k,136) + &
7.608*rxt(k,1542)*y(k,113) +9.261*rxt(k,1555)*y(k,208) +2.208*rxt(k,1566)*y(k,178) +5.324*rxt(k,1576)*y(k,101) + &
2.724*rxt(k,1588)*y(k,398) +2.752*rxt(k,1601)*y(k,141) +.372*rxt(k,1615)*y(k,182) +2.664*rxt(k,1626)*y(k,257) + &
1.024*rxt(k,1639)*y(k,290) +3.021*rxt(k,1651)*y(k,387) +.768*rxt(k,1662)*y(k,202) +1.024*rxt(k,1675)*y(k,269) + &
4.065*rxt(k,1687)*y(k,216) +.405*rxt(k,1696)*y(k,123) +2.964*rxt(k,1707)*y(k,392) +2.724*rxt(k,1719)*y(k,794) + &
3.021*rxt(k,1731)*y(k,786) +2.964*rxt(k,1742)*y(k,790) +2.613*rxt(k,1754)*y(k,396) + &
-1.1319995*rxt(k,1767)*y(k,799) +2.613*rxt(k,1779)*y(k,793) +4.000*rxt(k,1788)*y(k,26) + &
4.000*rxt(k,1798)*y(k,83) +7.780*rxt(k,1810)*y(k,127) +-1.7399995*rxt(k,1822)*y(k,267) + &
6.030*rxt(k,1834)*y(k,787) +2.876*rxt(k,1848)*y(k,183) +4.000*rxt(k,1858)*y(k,400) +1.420*rxt(k,1870)*y(k,782) + &
2.472*rxt(k,1881)*y(k,783) +5.991*rxt(k,1892)*y(k,791) +4.000*rxt(k,1902)*y(k,796) +3.723*rxt(k,1913)*y(k,128) + &
4.712*rxt(k,1925)*y(k,124) +6.000*rxt(k,1935)*y(k,67) +4.185*rxt(k,1948)*y(k,291) +9.00*rxt(k,1959)*y(k,797) + &
8.00*rxt(k,1969)*y(k,53) +2.748*rxt(k,1981)*y(k,219) +8.00*rxt(k,1991)*y(k,404) +12.00*rxt(k,2002)*y(k,795) + &
24.8799992*rxt(k,2013)*y(k,249) +7.725*rxt(k,2068)*y(k,771) +9.4720011*rxt(k,2081)*y(k,881) + &
10.00*rxt(k,2091)*y(k,55) +10.00*rxt(k,2101)*y(k,800) +12.00*rxt(k,2111)*y(k,56) +12.00*rxt(k,2121)*y(k,801) + &
14.00*rxt(k,2131)*y(k,57) +14.00*rxt(k,2141)*y(k,802) +16.*rxt(k,2151)*y(k,58) +16.*rxt(k,2161)*y(k,803) + &
18.*rxt(k,2171)*y(k,804) +4.680*rxt(k,2201)*y(k,776) +7.020*rxt(k,2246)*y(k,360) +3.291*rxt(k,2516)*y(k,344) + &
2.372*rxt(k,2536)*y(k,346) +3.180*rxt(k,2573)*y(k,447) +rxt(k,2588)*y(k,192) +rxt(k,2592)*y(k,59) + &
8.295001*rxt(k,2615)*y(k,591) +2.217*rxt(k,2625)*y(k,606) +-2.1569996*rxt(k,2634)*y(k,837) + &
-5.5839996*rxt(k,2644)*y(k,552) +-11.414999*rxt(k,2735)*y(k,595) +2.000*rxt(k,2740)*y(k,94) + &
7.752*rxt(k,2748)*y(k,270) +2.000*rxt(k,2753)*y(k,60) +3.942*rxt(k,2795)*y(k,501) +3.963*rxt(k,2805)*y(k,628) + &
2.000*rxt(k,2810)*y(k,531) +3.000*rxt(k,2894)*y(k,62) +2.760*rxt(k,2935)*y(k,514) +3.000*rxt(k,3001)*y(k,653) + &
2.660*rxt(k,3017)*y(k,554) +4.000*rxt(k,3066)*y(k,64) +4.000*rxt(k,3070)*y(k,200) +8.448*rxt(k,3107)*y(k,668) + &
-2.0799994*rxt(k,3119)*y(k,567) +5.000*rxt(k,3192)*y(k,66) +6.736*rxt(k,3237)*y(k,363) + &
4.808*rxt(k,3250)*y(k,569) +18.4979992*rxt(k,3444)*y(k,356) +15.4160013*rxt(k,3459)*y(k,436) + &
5.664*rxt(k,3479)*y(k,587) +14.200*rxt(k,3553)*y(k,529) +16.437*rxt(k,3588)*y(k,172) +20.*rxt(k,3757)*y(k,588) + &
38.076*rxt(k,3795)*y(k,652) +44.992*rxt(k,3808)*y(k,651) +-14.999999*rxt(k,3816)*y(k,608) + &
-7.6079993*rxt(k,3835)*y(k,481) +-11.999999*rxt(k,3843)*y(k,503) +13.6920013*rxt(k,3852)*y(k,522) + &
2.000*rxt(k,3869)*y(k,483) +4.000*rxt(k,3881)*y(k,441) +6.000*rxt(k,3894)*y(k,829) + &
-10.5389986*rxt(k,3902)*y(k,580) +12.384*rxt(k,3911)*y(k,50) +1.592*rxt(k,3925)*y(k,779) + &
1.592*rxt(k,3936)*y(k,379) +rxt(k,3980)*y(k,34) +1.158*rxt(k,3998)*y(k,660) +2.214*rxt(k,4010)*y(k,480) + &
7.902*rxt(k,4021)*y(k,159) +2.490*rxt(k,4039)*y(k,520) +5.610*rxt(k,4048)*y(k,439) +rxt(k,4062)*y(k,160) + &
2.000*rxt(k,4067)*y(k,689) +6.201*rxt(k,4081)*y(k,810) +2.706*rxt(k,4103)*y(k,521) +10.440*rxt(k,4138)*y(k,658) + &
3.240*rxt(k,4144)*y(k,499) +4.236*rxt(k,4159)*y(k,574) +5.532*rxt(k,4167)*y(k,720) +17.538*rxt(k,4174)*y(k,827)) &
*y(k,693) + (2.616*rxt(k,524)*y(k,348) +2.616*rxt(k,531)*y(k,349) +3.450*rxt(k,541)*y(k,350) + &
.300*rxt(k,547)*y(k,590) +-.199999*rxt(k,557)*y(k,664) +1.316*rxt(k,567)*y(k,582) + &
-2.7949996*rxt(k,591)*y(k,586) +.210*rxt(k,1076)*y(k,739) +.092*rxt(k,1088)*y(k,52) +.280*rxt(k,1105)*y(k,385) + &
.280*rxt(k,1113)*y(k,784) +4.125*rxt(k,1122)*y(k,33) +3.632*rxt(k,1137)*y(k,84) +3.596*rxt(k,1149)*y(k,263) + &
.192*rxt(k,1160)*y(k,176) +.168*rxt(k,1170)*y(k,180) +.372*rxt(k,1181)*y(k,389) +.372*rxt(k,1192)*y(k,788) + &
5.505*rxt(k,1202)*y(k,457) +4.568*rxt(k,1213)*y(k,780) +-1.8239995*rxt(k,1224)*y(k,49) + &
2.496*rxt(k,1241)*y(k,72) +7.380*rxt(k,1252)*y(k,213) +2.810*rxt(k,1264)*y(k,264) +3.025*rxt(k,1277)*y(k,312) + &
1.328*rxt(k,1287)*y(k,120) +1.180*rxt(k,1297)*y(k,168) +1.448*rxt(k,1308)*y(k,177) +.052*rxt(k,1326)*y(k,181) + &
4.890*rxt(k,1338)*y(k,386) +.150*rxt(k,1348)*y(k,391) +-.09*rxt(k,1360)*y(k,395) +-.09*rxt(k,1372)*y(k,792) + &
4.845*rxt(k,1384)*y(k,798) +4.890*rxt(k,1396)*y(k,785) +.150*rxt(k,1406)*y(k,789) +11.268*rxt(k,1418)*y(k,285) + &
3.076*rxt(k,1430)*y(k,81) +11.176*rxt(k,1441)*y(k,456) +.325*rxt(k,1454)*y(k,781) +6.620*rxt(k,1466)*y(k,82) + &
15.096*rxt(k,1478)*y(k,317) +6.436*rxt(k,1491)*y(k,71) +7.470*rxt(k,1503)*y(k,215) +7.300*rxt(k,1516)*y(k,265) + &
2.668*rxt(k,1527)*y(k,136) +.288*rxt(k,1539)*y(k,113) +13.8950014*rxt(k,1552)*y(k,208) + &
2.668*rxt(k,1563)*y(k,178) +7.480*rxt(k,1574)*y(k,101) +7.660*rxt(k,1584)*y(k,398) +4.685*rxt(k,1597)*y(k,141) + &
4.700*rxt(k,1611)*y(k,182) +1.188*rxt(k,1623)*y(k,257) +-.224999*rxt(k,1635)*y(k,290) + &
3.470*rxt(k,1648)*y(k,387) +3.696*rxt(k,1659)*y(k,202) +-.224999*rxt(k,1671)*y(k,269) + &
1.705*rxt(k,1684)*y(k,216) +.120*rxt(k,1693)*y(k,123) +3.732*rxt(k,1704)*y(k,392) +7.660*rxt(k,1715)*y(k,794) + &
3.470*rxt(k,1728)*y(k,786) +3.732*rxt(k,1739)*y(k,790) +1.860*rxt(k,1751)*y(k,396) +3.160*rxt(k,1763)*y(k,799) + &
1.860*rxt(k,1776)*y(k,793) +8.7480011*rxt(k,1786)*y(k,26) +10.2840014*rxt(k,1796)*y(k,83) + &
9.22*rxt(k,1806)*y(k,127) +4.000*rxt(k,1819)*y(k,267) +8.3950014*rxt(k,1831)*y(k,787) + &
5.845*rxt(k,1844)*y(k,183) +7.252*rxt(k,1856)*y(k,400) +7.480*rxt(k,1866)*y(k,782) +7.240*rxt(k,1878)*y(k,783) + &
2.596*rxt(k,1889)*y(k,791) +7.252*rxt(k,1900)*y(k,796) +7.628*rxt(k,1910)*y(k,128) +1.569*rxt(k,1921)*y(k,124) + &
14.236001*rxt(k,1933)*y(k,67) +24.190*rxt(k,1945)*y(k,291) +6.128*rxt(k,1956)*y(k,797) + &
18.1159992*rxt(k,1967)*y(k,53) +6.980*rxt(k,1977)*y(k,219) +4.992*rxt(k,1989)*y(k,404) + &
10.092001*rxt(k,1999)*y(k,795) +21.4239998*rxt(k,2009)*y(k,249) +17.995*rxt(k,2023)*y(k,355) + &
18.040*rxt(k,2038)*y(k,879) +12.1520014*rxt(k,2065)*y(k,771) +14.120*rxt(k,2077)*y(k,881) + &
22.068*rxt(k,2089)*y(k,55) +8.9560013*rxt(k,2099)*y(k,800) +26.044*rxt(k,2109)*y(k,56) + &
12.7880011*rxt(k,2119)*y(k,801) +30.044*rxt(k,2129)*y(k,57) +16.7399998*rxt(k,2139)*y(k,802) + &
34.0439987*rxt(k,2149)*y(k,58) +20.620*rxt(k,2159)*y(k,803) +24.572*rxt(k,2169)*y(k,804) + &
3.200*rxt(k,2198)*y(k,776) +3.200*rxt(k,2243)*y(k,360) +.800*rxt(k,2534)*y(k,346) +.210*rxt(k,2569)*y(k,447) + &
2.860*rxt(k,2612)*y(k,591) +3.000*rxt(k,2622)*y(k,606) +6.000*rxt(k,2631)*y(k,837) +2.781*rxt(k,2732)*y(k,595) + &
5.000*rxt(k,2792)*y(k,501) +.900*rxt(k,2802)*y(k,628) +4.736*rxt(k,2932)*y(k,514) +13.004*rxt(k,3013)*y(k,554) + &
9.00*rxt(k,3103)*y(k,668) +9.00*rxt(k,3114)*y(k,567) +12.264*rxt(k,3233)*y(k,363) +12.264*rxt(k,3246)*y(k,569) + &
22.8199997*rxt(k,3441)*y(k,356) +15.695*rxt(k,3455)*y(k,436) +15.700*rxt(k,3476)*y(k,587) + &
16.2479992*rxt(k,3548)*y(k,529) +17.*rxt(k,3585)*y(k,172) +28.596*rxt(k,3755)*y(k,588) + &
51.5699997*rxt(k,3791)*y(k,652) +50.25*rxt(k,3804)*y(k,651) +10.00*rxt(k,3813)*y(k,608) + &
8.00*rxt(k,3832)*y(k,481) +8.00*rxt(k,3840)*y(k,503) +10.00*rxt(k,3849)*y(k,522) +6.000*rxt(k,3892)*y(k,829) + &
8.00*rxt(k,3899)*y(k,580) +10.00*rxt(k,3908)*y(k,50) +.813*rxt(k,3923)*y(k,779) +.813*rxt(k,3934)*y(k,379) + &
-5.9999995*rxt(k,3995)*y(k,660) +1.950*rxt(k,4007)*y(k,480) +8.8830013*rxt(k,4018)*y(k,159) + &
8.8830013*rxt(k,4036)*y(k,520) +6.201*rxt(k,4078)*y(k,810) +2.706*rxt(k,4100)*y(k,521) + &
8.00*rxt(k,4149)*y(k,752) +4.236*rxt(k,4156)*y(k,574))*y(k,696) + (rxt(k,550)*y(k,590) + &
.450*rxt(k,559)*y(k,664) +-3.0299995*rxt(k,597)*y(k,586) +.600*rxt(k,1071)*y(k,512) +.550*rxt(k,1079)*y(k,739) + &
.450*rxt(k,1092)*y(k,52) +.400*rxt(k,1101)*y(k,585) +-6.5879993*rxt(k,1129)*y(k,33) +1.450*rxt(k,1141)*y(k,84) + &
1.450*rxt(k,1153)*y(k,263) +1.400*rxt(k,1164)*y(k,176) +rxt(k,1174)*y(k,180) +rxt(k,1185)*y(k,389) + &
rxt(k,1196)*y(k,788) +.520*rxt(k,1206)*y(k,457) +.250*rxt(k,1217)*y(k,780) +.500*rxt(k,1228)*y(k,49) + &
2.450*rxt(k,1244)*y(k,72) +2.450*rxt(k,1255)*y(k,213) +1.350*rxt(k,1268)*y(k,264) +2.450*rxt(k,1280)*y(k,312) + &
2.400*rxt(k,1290)*y(k,120) +2.400*rxt(k,1301)*y(k,168) +2.400*rxt(k,1312)*y(k,177) +2.000*rxt(k,1319)*y(k,122) + &
2.000*rxt(k,1330)*y(k,181) +1.520*rxt(k,1342)*y(k,386) +1.520*rxt(k,1352)*y(k,391) +1.200*rxt(k,1364)*y(k,395) + &
1.200*rxt(k,1376)*y(k,792) +1.760*rxt(k,1388)*y(k,798) +1.520*rxt(k,1400)*y(k,785) +1.520*rxt(k,1410)*y(k,789) + &
.750*rxt(k,1459)*y(k,781) +rxt(k,1470)*y(k,82) +rxt(k,1483)*y(k,317) +2.350*rxt(k,1494)*y(k,71) + &
2.350*rxt(k,1507)*y(k,215) +2.350*rxt(k,1520)*y(k,265) +3.400*rxt(k,1532)*y(k,136) +3.400*rxt(k,1543)*y(k,113) + &
3.450*rxt(k,1556)*y(k,208) +3.400*rxt(k,1567)*y(k,178) +3.400*rxt(k,1577)*y(k,101) +3.000*rxt(k,1589)*y(k,398) + &
2.200*rxt(k,1602)*y(k,141) +2.200*rxt(k,1616)*y(k,182) +2.200*rxt(k,1627)*y(k,257) +2.200*rxt(k,1640)*y(k,290) + &
rxt(k,1652)*y(k,387) +rxt(k,1663)*y(k,202) +2.200*rxt(k,1676)*y(k,269) +2.200*rxt(k,1688)*y(k,216) + &
3.000*rxt(k,1697)*y(k,123) +rxt(k,1708)*y(k,392) +3.000*rxt(k,1720)*y(k,794) +rxt(k,1732)*y(k,786) + &
rxt(k,1743)*y(k,790) +2.200*rxt(k,1755)*y(k,396) +rxt(k,1768)*y(k,799) +2.200*rxt(k,1780)*y(k,793) + &
2.000*rxt(k,1789)*y(k,26) +3.350*rxt(k,1799)*y(k,83) +4.400*rxt(k,1811)*y(k,127) +3.200*rxt(k,1823)*y(k,267) + &
2.000*rxt(k,1835)*y(k,787) +3.200*rxt(k,1849)*y(k,183) +2.000*rxt(k,1859)*y(k,400) +3.760*rxt(k,1871)*y(k,782) + &
2.000*rxt(k,1882)*y(k,783) +2.000*rxt(k,1893)*y(k,791) +2.000*rxt(k,1903)*y(k,796) +4.000*rxt(k,1914)*y(k,128) + &
4.000*rxt(k,1926)*y(k,124) +4.350*rxt(k,1936)*y(k,67) +4.350*rxt(k,1949)*y(k,291) +3.000*rxt(k,1960)*y(k,797) + &
5.350*rxt(k,1970)*y(k,53) +4.000*rxt(k,1982)*y(k,219) +4.000*rxt(k,1992)*y(k,404) +4.000*rxt(k,2003)*y(k,795) + &
4.000*rxt(k,2014)*y(k,249) +4.000*rxt(k,2029)*y(k,355) +5.200*rxt(k,2044)*y(k,879) +4.000*rxt(k,2058)*y(k,479) + &
5.200*rxt(k,2069)*y(k,771) +4.000*rxt(k,2082)*y(k,881) +6.350*rxt(k,2092)*y(k,55) +5.000*rxt(k,2102)*y(k,800) + &
7.350*rxt(k,2112)*y(k,56) +6.000*rxt(k,2122)*y(k,801) +8.35*rxt(k,2132)*y(k,57) +7.000*rxt(k,2142)*y(k,802) + &
9.35*rxt(k,2152)*y(k,58) +8.00*rxt(k,2162)*y(k,803) +9.00*rxt(k,2172)*y(k,804) +2.000*rxt(k,2202)*y(k,776) + &
3.000*rxt(k,2247)*y(k,360) +rxt(k,2574)*y(k,447) +rxt(k,2575)*y(k,590) +rxt(k,2616)*y(k,591) + &
.450*rxt(k,2626)*y(k,606) +.450*rxt(k,2635)*y(k,837) +rxt(k,2645)*y(k,552) +1.450*rxt(k,2736)*y(k,595) + &
1.450*rxt(k,2796)*y(k,501) +1.400*rxt(k,2806)*y(k,628) +2.400*rxt(k,2936)*y(k,514) +1.350*rxt(k,3018)*y(k,554) + &
3.450*rxt(k,3108)*y(k,668) +3.450*rxt(k,3120)*y(k,567) +4.400*rxt(k,3238)*y(k,363) +4.400*rxt(k,3251)*y(k,569) + &
4.000*rxt(k,3445)*y(k,356) +4.000*rxt(k,3460)*y(k,436) +4.000*rxt(k,3480)*y(k,587) +4.000*rxt(k,3554)*y(k,529) + &
6.350*rxt(k,3589)*y(k,172) +10.00*rxt(k,3758)*y(k,588) +13.00*rxt(k,3796)*y(k,652) +13.00*rxt(k,3809)*y(k,651) + &
2.000*rxt(k,3882)*y(k,441) +.880*rxt(k,3926)*y(k,779) +.880*rxt(k,3937)*y(k,379) +.400*rxt(k,4049)*y(k,439)) &
*y(k,839) + (rxt(k,3817)*y(k,608) +2.000*rxt(k,3836)*y(k,481) +2.000*rxt(k,3844)*y(k,503) + &
2.000*rxt(k,3853)*y(k,522) +3.000*rxt(k,3895)*y(k,829) +3.000*rxt(k,3903)*y(k,580) +4.000*rxt(k,3989)*y(k,806) + &
4.000*rxt(k,4011)*y(k,480) +4.000*rxt(k,4022)*y(k,159) +4.000*rxt(k,4027)*y(k,353) +4.000*rxt(k,4040)*y(k,520) + &
6.000*rxt(k,4082)*y(k,810) +6.000*rxt(k,4104)*y(k,521) +9.00*rxt(k,4160)*y(k,574))*y(k,546) &
 + (2.000*rxt(k,3883)*y(k,441) +.972*rxt(k,3929)*y(k,779) +.972*rxt(k,3940)*y(k,379) +1.218*rxt(k,4052)*y(k,439)) &
*y(k,437) + (2.000*rxt(k,340)*y(k,695) +2.000*rxt(k,476)*y(k,840) +8.00*rxt(k,4147)*y(k,752))*y(k,691) &
 + (rxt(k,479)*y(k,841) +4.000*rxt(k,3988)*y(k,695))*y(k,548) + (6.000*rxt(k,116) +6.000*rxt(k,117))*y(k,694) &
 +-.18*rxt(k,3)*y(k,59) +2.379*rxt(k,6)*y(k,60) +4.866*rxt(k,9)*y(k,62) +7.326*rxt(k,12)*y(k,64) &
 +9.7320013*rxt(k,15)*y(k,66) +.156*rxt(k,17)*y(k,94) +1.880*rxt(k,21)*y(k,186) +-.36*rxt(k,24)*y(k,192) &
 +1.271*rxt(k,29)*y(k,266) +1.479*rxt(k,32)*y(k,270) +3.286*rxt(k,34)*y(k,332) +-.852*rxt(k,36)*y(k,344) &
 +.434*rxt(k,39)*y(k,348) +rxt(k,41)*y(k,349) +7.000*rxt(k,43)*y(k,358) +3.244*rxt(k,45)*y(k,412) &
 +4.884*rxt(k,47)*y(k,414) +rxt(k,50)*y(k,424) +rxt(k,58)*y(k,448) +1.760*rxt(k,62)*y(k,476) +1.766*rxt(k,65) &
*y(k,531) +-1.9999995*rxt(k,77)*y(k,552) +14.580*rxt(k,81)*y(k,557) +.466*rxt(k,83)*y(k,582) +3.842*rxt(k,94) &
*y(k,634) +1.479*rxt(k,98)*y(k,646) +.240*rxt(k,100)*y(k,649) +rxt(k,101)*y(k,650) +6.552*rxt(k,104)*y(k,653) &
 +1.586*rxt(k,106)*y(k,655) +.120*rxt(k,108)*y(k,659) +.156*rxt(k,110)*y(k,663) +-4.3999996*rxt(k,112)*y(k,664) &
 +.273*rxt(k,127)*y(k,731) +.004*rxt(k,130)*y(k,732) +.297*rxt(k,133)*y(k,733) +1.580*rxt(k,135)*y(k,734) &
 +4.404*rxt(k,138)*y(k,735) +4.380*rxt(k,140)*y(k,736) +.068*rxt(k,144)*y(k,755) +rxt(k,148)*y(k,756) &
 +1.980*rxt(k,155)*y(k,761) +rxt(k,159)*y(k,763) +-.568*rxt(k,163)*y(k,764) +5.664*rxt(k,166)*y(k,765) &
 +6.712*rxt(k,170)*y(k,766) +8.00*rxt(k,172)*y(k,767) +rxt(k,480)*y(k,841) +rxt(k,4522)*y(k,910)
         loss(k,40) = 0.
         prod(k,40) = 0.
         loss(k,41) = 0.
         prod(k,41) = 0.
         loss(k,42) = 0.
         prod(k,42) = 0.
         loss(k,43) = 0.
         prod(k,43) = 0.
         loss(k,44) = 0.
         prod(k,44) = 0.
         loss(k,45) = 0.
         prod(k,45) = 0.
         loss(k,46) = 0.
         prod(k,46) = 0.
         loss(k,47) = 0.
         prod(k,47) = 0.
         loss(k,48) = 0.
         prod(k,48) = 0.
         loss(k,49) = 0.
         prod(k,49) = 0.
         loss(k,50) = 0.
         prod(k,50) = 0.
         loss(k,51) = 0.
         prod(k,51) = 0.
         loss(k,52) = 0.
         prod(k,52) = 0.
         loss(k,53) = 0.
         prod(k,53) = 0.
         loss(k,54) = 0.
         prod(k,54) = 0.
         loss(k,55) = 0.
         prod(k,55) = 0.
         loss(k,56) = 0.
         prod(k,56) = 0.
         loss(k,57) = 0.
         prod(k,57) = 0.
         loss(k,58) = 0.
         prod(k,58) = 0.
         loss(k,59) = 0.
         prod(k,59) = 0.
         loss(k,60) = 0.
         prod(k,60) = 0.
         loss(k,61) = 0.
         prod(k,61) = 0.
         loss(k,62) = 0.
         prod(k,62) = 0.
         loss(k,63) = 0.
         prod(k,63) = 0.
         loss(k,64) = 0.
         prod(k,64) = 0.
         loss(k,65) = 0.
         prod(k,65) = 0.
         loss(k,66) = 0.
         prod(k,66) = 0.
         loss(k,67) = 0.
         prod(k,67) = 0.
         loss(k,68) = 0.
         prod(k,68) = 0.
         loss(k,69) = 0.
         prod(k,69) = 0.
         loss(k,70) = 0.
         prod(k,70) = 0.
         loss(k,71) = 0.
         prod(k,71) = 0.
         loss(k,72) = 0.
         prod(k,72) = 0.
      end do

      end subroutine IMP_PROD_LOSS
