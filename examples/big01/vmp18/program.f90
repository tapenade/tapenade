
!         Arakawa C staggered grid (Arakawa 1966)
!
!    --j y
!   |           --- V ---           V ------- V         U -- T -- U
!   i          |         |          |         |         |         |
!   x          U    T    U          T    U    T         |    V    |
!              |         |          |         |         |         |
!               --- V ---           V ------- V         U -- T -- U
!                 T-cell               U-cell              V-cell
!
!       T(eta, H, tr, d, f, mask,   U(u, mask_u         V(v, mask_v
!           x, y, and bottom      x_u, y_u)           x_v, y_v)
!            stress)


module physic_params
       integer, parameter :: M=20
       integer, parameter :: N=20
       double precision, parameter :: gama=0.1
       double precision, parameter :: karman=0.4
       double precision, parameter :: lb=0.05
       double precision, parameter :: Nu=0.1
       double precision, parameter :: g = 9.8
       double precision, parameter :: K = 1.0
       double precision dx,dy,dt,dt_tr
       double precision time,time_tr
       double precision fintime
       integer maxit
       double precision, dimension(M,N) ::D
       double precision, dimension(M,N) ::eta
       double precision, dimension(M,N) ::H
       double precision, dimension(M,N) ::Tr
       double precision, dimension(M+1,N) ::u
       double precision, dimension(M,N+1) ::v
       double precision, dimension(M+1,N) ::u_a
       double precision, dimension(M,N+1) ::v_a
       double precision, dimension(M,N) ::RHSeta
       double precision, dimension(N,M) ::RHSetatran
       double precision, dimension(M+1,N) ::RHSu
       double precision, dimension(M,N+1) ::RHSv
       double precision, dimension(N,M) ::Dtran
       double precision, dimension(N,M) ::etatran
       double precision, dimension(N,M) ::eta_oldtran
       double precision, dimension(N,M) ::eta_newtran
       double precision, dimension(N,M) ::Htran
       double precision, dimension(N,M) ::Trtran
       double precision, dimension(N,M+1) ::utran
       double precision, dimension(N+1,M) ::vtran
       double precision, dimension(N,M) ::H_oldtran
       double precision, dimension(N,M+1) ::u_oldtran
       double precision, dimension(N+1,M) ::v_oldtran
       double precision, dimension(N,M) ::H_newtran
       double precision, dimension(N,M+1) ::u_newtran
       double precision, dimension(N+1,M) ::v_newtran
       double precision, dimension(N+1,M) ::v_atran
       double precision, dimension(N,M) ::Htemp
       double precision, dimension(N,M+1) ::utemp
       double precision, dimension(N+1,M) ::vttemp
       double precision, dimension(N,M) ::Htrantemp
       double precision, dimension(N,M+1) ::utrantemp
       double precision, dimension(N+1,M) ::vtrantemp
       double precision, dimension(M,N) ::eta_old
       double precision, dimension(M,N) ::H_old
       double precision, dimension(M,N) ::Tr_old
       double precision, dimension(M+1,N) ::u_old
       double precision, dimension(M,N+1) ::v_old
       double precision, dimension(M,N) ::eta_new
       double precision, dimension(M,N) ::H_new
       double precision, dimension(M,N) ::Tr_new
       double precision, dimension(M+1,N) ::u_new
       double precision, dimension(M,N+1) ::v_new
       double precision, dimension(M,N) ::H0
       double precision, dimension(M,N) ::Tr0
       double precision, dimension(M+1,N) ::u0
       double precision, dimension(M,N+1) ::v0   
       double precision, dimension(M,N) ::x
       double precision, dimension(M+1,N) ::x_u
       double precision, dimension(M,N+1) ::x_v
       double precision, dimension(M,N) ::y
       double precision, dimension(M+1,N) ::y_u
       double precision, dimension(M,N+1) ::y_v
end module physic_params

	program FourD_var
        use physic_params
        implicit none
        double precision x0,y0
        integer i,j
!	Problem specify data
         dx=20
         dy=20
	 time=0.0
         fintime= 100.0
	 dt=0.1
         time_tr= time + 2.0 * dt;
	 maxit=int((fintime-time)/dt)

!	Prepare mesh
        x0 = dx/2;
        y0 = dy/2;
!	H
	do i=1,M
	  do j=1,N
              x(i,j)=dble(i*j);
              y(i,j)=dble(i*j);
          end do
        end do        
	do j = 1,N
    	x(1:M,j) = x0 + (x(1:M,j)/j - 1) * dx;
	end do
	do i = 1,M
    	y(i,1:N) = y0 + (y(i,1:N)/i - 1) * dy;
	end do
!	U
        do i=1,M+1
          do j=1,N
              x_u(i,j)=i*j;
              y_u(i,j)=i*j;
          end do
        end do          
	do  j= 1,N
   	x_u(:,j) = x0 + (x_u(:,j)/j - 1) * dx - dx/2;
	end do
	do i = 1,M
    	y_u(i,:) = y0 + (y_u(i,:)/i - 1) * dy;
	end do
!	V
	do i=1,M
          do j=1,N+1
              x_v(i,j)=i*j;
              y_v(i,j)=i*j;
          end do
        end do
	do j = 1,N
    	x_v(:,j) = x0 + (x_v(:,j)/j - 1) * dx;
	end do
	do i = 1,M
    	y_v(i,:) = y0 + (y_v(i,:)/i - 1) * dy - dy/2;
	end do
!	Get Bathymetry
	do i=1,M
	do j=1,N
		D(i,j)=10.0
	end do
	end do

!	Generate IC
!	eta
	eta= 0.01 * exp( - ( ( x - 200.0 )**2.0 / 60.0**2.0 + ( y - 200.0 )**2.0 / 60.0**2.0 ))
!	Main input
        H0=eta+D;
        u0=0.0
        u0=0.0
        Tr0=0.0
!	H
	eta_old = eta;
	eta_new = eta;     
       H_old = H0;
       H =H0;
       H_new =H0;
! 	U
	u_old=u0;
	u = u0;
	u_new = u0;
	u_a = u0;
!	V
        v_old=v0;
        v = v0;
        v_new = v0;
        v_a = v0;
!	Tr
        Tr0(8:11,8:11)= 1.0
	Tr_old=Tr0;
        Tr = Tr0;
        Tr_new = Tr0;	

	call forward
       do i = 1,M
           write (*,'(20e11.4)') (Tr(i,j),j=1,N)
       enddo

	end

	subroutine forward
	use physic_params
        implicit none
	integer i,a,b
	do i=1, maxit
!	Update time
    	time = time + dt
!	Compute the time scheme for  momentum and continuity
!    	For the water elevation: continutiy
    	call ComputeCon
        eta_new = eta_old + 2.0 * dt * RHSeta;
        H_new = eta_new + D;
!	For the momentum: 1 means U and -1 means V
    	call ComputeMon_CSFT(1D0)
        call ComputeMon_CSFT(-1D0);
!	OpenBoundary treatmment
        call ComputeOB(-1D0);
        call ComputeOB(1D0);
! 	Asselin-Robert filter 
    	eta = eta + gama * ( eta_old - 2.0 * eta + eta_new )
    	u = u + gama * ( u_old - 2.0 * u + u_new )
    	v = v + gama * ( v_old - 2.0 * v + v_new )
 
!	Update matrices
    	eta_old = eta
    	eta = eta_new
    	H_old = H
    	H = H_new
    	u_old = u
    	u = u_new
    	v_old = v
    	v = v_new

!	Compute all the tracers
!	Adveraging velocity
	u_a = (dble(i - 1.0) * u_a + u ) / dble(i);
	v_a = (dble(i - 1.0) * v_a + v ) / dble(i);

        if (time_tr .le. time) then
        write(*,*) i
       	call ComputeTr(i)
        time_tr = time_tr + dt_tr
        end if
	end do

	end

	subroutine ComputeCon
	use physic_params
        implicit none
        external transpose
        double precision transpose
	RHSeta(2:M-1,2:N-1)= -1.0*(0.5*(H(3:M,2:N-1)+H(2:M-1,2:N-1))*u(3:M,2:N-1)-0.5*(H(2:M-1,2:N-1)+H(1:M-2,2:N-1))*u(2:M-1,2:N-1))/dx
        Htran=transpose(H)
        vtran=transpose(v)
!       transpose dimension(N,M)
!       transpose u dimension(N,M+1)
!       transpose v dimension(N+1,M)
	Htrantemp(2:N-1,2:M-1)=-1.0*(0.5*(Htran(3:N,2:M-1)+Htran(2:N-1,2:M-1))*vtran(3:N,2:M-1)-0.5*(Htran(2:N-1,2:M-1)&
&	+Htran(1:N-2,2:M-1))*vtran(2:N-1,2:M-1))/dy
	RHSeta(2:M-1,2:N-1)=RHSeta(2:M-1,2:N-1)+transpose(Htrantemp(2:N-1,2:M-1))
	end

	subroutine ComputeMon_CSFT(weight)
	use physic_params
        implicit none
        external transpose
        double precision transpose
        integer a,b
        double precision :: weight
        double precision, dimension(M-1,N-2) ::cd_L
        double precision, dimension(N-1,M-2) ::cd_Ltran
	if (weight .gt. 0.0) then
!	U convection
	RHSu(2:M,2:N-1) = -0.2500*((u(3:M+1,2:N-1)+u(2:M,2:N-1))**2 *H(2:M,2:N-1)-(u(2:M,2:N-1)+u(1:M-1,2:N-1))**2 *H(1:M-1,2:N-1))/dx 
!	V convection
        RHSu(2:M,2:N-1) =  RHSu(2:M,2:N-1)-0.0625*(( H(2:M,3:N) + H(2:M,2:N-1) + H(1:M-1,3:N) + H(1:M-1,2:N-1))*(v(2:M,3:N)&
&	+v(1:M-1,3:N))*(u(2:M,3:N) + u(2:M,2:N-1)) -(H(2:M,2:N-1) + H(2:M,1:N-2) + H(1:M-1,2:N-1) + H(1:M-1,1:N-2))* (v(2:M,2:N-1)& 
&	+ v(1:M-1,2:N-1) )*(u(2:M,2:N-1) + u(2:M,1:N-2) ))/dy
!	Pressure
        RHSu(2:M,2:N-1) =  RHSu(2:M,2:N-1)-g * 0.5* (H(2:M,2:N-1) + H(1:M-1,2:N-1)) * (eta(2:M,2:N-1) - eta(1:M-1,2:N-1) ) / dx
!	U diffusion
        RHSu(2:M,2:N-1) =  RHSu(2:M,2:N-1)+(Nu * H_old(2:M,2:N-1) * ( u_old(3:M+1,2:N-1) - u_old(2:M,2:N-1) ) / dx& 
&	- Nu * H_old(1:M-1,2:N-1) * (u_old(2:M,2:N-1) - u_old(1:M-1,2:N-1) ) / dx ) / dx 
!	V diffusion
        RHSu(2:M,2:N-1) =  RHSu(2:M,2:N-1)+(Nu * H_old(2:M,2:N-1) * ( u_old(2:M,3:N) - u_old(2:M,2:N-1) )/dy& 
&	- Nu * H_old(2:M,1:N-2) * (u_old(2:M,2:N-1) - u_old(2:M,1:N-2) )/dy) / dy
!	Bottom Stress
        cd_L = karman / log((0.5*0.5*(H_old(2:M,2:N-1)+ H_old(1:M-1,2:N-1))+ lb)/lb) ;
        cd_L = max(cd_L * cd_L, 0.0025);
        RHSu(2:M,2:N-1) = RHSu(2:M,2:N-1) - cd_L * sqrt (u_old(2:M,2:N-1)**2 + (0.25*(v_old(2:M,1:N-2)+v_old(2:M,2:N-2+1)&
&	+v_old(1:M-1,1:N-2)+v_old(1:M-1,2:N-2+1)))**2)*u_old(2:M,2:N-1) 
!	time marching
	u_new(2:M,2:N-1) = (u_old(2:M,2:N-1) * (H_old(2:M,2:N-1) + H_old(1:M-1,2:N-1) ) + 2.0 * 2.0 * dt * RHSu(2:M,2:N-1))/& 
&	( H_new(2:M,2:N-1) + H_new(1:M-1,2:N-1));
!        write(*,*) 'This is u max=%f \n',maxval(u_new)
!        do a = 1,M+1
!           write (*,'(20e11.4)') (u_new(a,b),b=1,N)
!       enddo
	else
	Htran=transpose(H)
        H_oldtran=transpose(H_old)
        etatran=transpose(eta)
        vtran=transpose(v)
        v_oldtran=transpose(v_old)
        utran=transpose(u)
        u_oldtran=transpose(u_old)
        H_newtran=transpose(H_new)
        u_newtran=transpose(u_new)
        v_newtran=transpose(v_new)
!       U convection
        vtrantemp(2:N,2:M-1) = - 0.2500*((vtran(3:N+1,2:M-1) + vtran(2:N,2:M-1))**2 * Htran(2:N,2:M-1)-(vtran(2:N,2:M-1)&
&	+ vtran(1:N-1,2:M-1) )**2 * Htran(1:N-1,2:M-1))/dy
!       V convection
        vtrantemp(2:N,2:M-1) =  vtrantemp(2:N,2:M-1) - 0.0625*(( Htran(2:N,3:M) + Htran(2:N,2:M-1) + Htran(1:N-1,3:M)&
&	+ Htran(1:N-1,2:M-1))*(utran(2:N,3:M)+utran(1:N-1,3:M))*(vtran(2:N,3:M) + vtran(2:N,2:M-1)) -(Htran(2:N,2:M-1)& 
&	+ Htran(2:N,1:M-2) + Htran(1:N-1,2:M-1) + Htran(1:N-1,1:M-2))* (utran(2:N,2:M-1) + utran(1:N-1,2:M-1) )&
&	*(vtran(2:N,2:M-1) + vtran(2:N,1:M-2) ))/dx
!       Pressure
        vtrantemp(2:N,2:M-1) =  vtrantemp(2:N,2:M-1)- g * 0.5* (Htran(2:N,2:M-1) + Htran(1:N-1,2:M-1)) * (etatran(2:N,2:M-1)& 
&	- etatran(1:N-1,2:M-1) ) / dy
!       U diffusion
        vtrantemp(2:N,2:M-1) =  vtrantemp(2:N,2:M-1)+ ( Nu * H_oldtran(2:N,2:M-1) * ( v_oldtran(3:N+1,2:M-1)-v_oldtran(2:N,2:M-1))&
&	/ dy - Nu * H_oldtran(1:N-1,2:M-1) * (v_oldtran(2:N,2:M-1) - v_oldtran(1:N-1,2:M-1) ) / dy ) / dy
!       V diffusion
        vtrantemp(2:N,2:M-1) =  vtrantemp(2:N,2:M-1)+ ( Nu * H_oldtran(2:N,2:M-1) * ( v_oldtran(2:N,3:M)-v_oldtran(2:N,2:M-1))/dx&
&	-Nu * H_oldtran(2:N,1:M-2) * (u_oldtran(2:N,2:M-1) - u_oldtran(2:N,1:M-2) )/dx) / dx
!       Bottom Stress
        cd_Ltran = karman / log( ( 0.5 * 0.5* (H_oldtran(2:N,2:M-1) + H_oldtran(1:N-1,2:M-1)) + lb ) / lb ) ;
        cd_Ltran = max(cd_Ltran * cd_Ltran, 0.0025);
        vtrantemp(2:N,2:M-1) = vtrantemp(2:N,2:M-1) - cd_Ltran*sqrt(v_oldtran(2:N,2:M-1)**2 + (0.25*(u_oldtran(2:N,1:M-2)&
&	+u_oldtran(2:N,2:M-2+1)+u_oldtran(1:N-1,1:M-2)+u_oldtran(1:N-1,2:M-2+1)))**2)*v_oldtran(2:N,2:M-1) 
!	Time marching
	v_newtran(2:N,2:M-1) =(v_oldtran(2:N,2:M-1)* (H_oldtran(2:N,2:M-1) + H_oldtran(1:N-1,2:M-1) ) + 2.0* 2.0 * dt&
&	* vtrantemp(2:N,2:M-1))/(H_newtran(2:N,2:M-1) + H_newtran(1:N-1,2:M-1) );
!	transpose back
        v_new=transpose(v_newtran)         
	end if
	end
	
	subroutine ComputeOB(weight)
	use physic_params
        implicit none
        external transpose
        double precision transpose
        double precision :: weight
       
	if (weight .gt. 0D0) then
!    %Waterlevel: GW radiation 
    	eta_new(1:M:M-1,:)=eta_old(1:M:M-1,:)-2.0*dt/dx*sqrt(g*H_old(1:M:M-1,:))*(eta_old(1:M:M-1,:)-eta_old(2:M-1:M-3,:))
        H_new(1:M:M-1,:) = eta_new(1:M:M-1,:) + D(1:M:M-1,:)
!    %Normal velocity: Flather 
!    u_new(1:M:M+1,:) = [-1 * ones(1,N); ones(1,N)] .*  mask_u(1:M:M+1,:) ...        .* sqrt( g ./ H_new(1:M-1:M,:) ) .* eta_new(1:M-1:M,:);
     u_new(1,:)=-1.0*sqrt( g / H_new(1,:) ) * eta_new(1,:)
     u_new(M+1,:)=1.0*sqrt( g / H_new(M,:) ) * eta_new(M,:)
!    %Normal velocity: Null-gradient 
!      u_new(1:M+1:M,1:N) = u_new(2:M:M-2,1:N)
!    %Tangential velocity: GW radiation 
      v_new(1:M:M-1,2:N) =(v_old(1:M:M-1,2:N)*(H_old(1:M:M-1,2:N) + H_old(1:M:M-1,1:N-1)) - (2.0*dt)/dx*sqrt(g*0.5*&
&	( H_old(1:M:M-1,2:N)+ H_old(1:M:M-1,1:N-1) ) ) * ( v_old(1:M:M-1,2:N) - v_old( 2:M-1:M-3,2:N)))/ ( H_new(1:M:M-1,2:N) &
&	+ H_new(1:M:M-1,1:N-1))
	else
        Dtran=transpose(D)
        eta_oldtran=transpose(eta_old)
        eta_newtran=transpose(eta_new)
        H_newtran=transpose(H_new)
        H_oldtran=transpose(H_old)
        v_newtran=transpose(v_new)
        v_oldtran=transpose(v_old)
        u_newtran=transpose(u_new)
        u_oldtran=transpose(u_old)
!    %Waterlevel: GW radiation 
    	eta_newtran(1:N:N-1,:)=eta_oldtran(1:N:N-1,:)-2.0*dt/dy * sqrt(g*H_oldtran(1:N:N-1,:))*(eta_oldtran(1:N:N-1,:)&
&	-eta_oldtran(2:N-1:N-3,:))
        H_newtran(1:N:N-1,:) = eta_newtran(1:N:N-1,:) + Dtran(1:N:M-1,:)
!	Normal velocity: Flather 
        v_newtran(1,:)=-1.0* sqrt( g / H_newtran(1,:) ) * eta_newtran(1,:)
        v_newtran(N+1,:)=1.0* sqrt( g / H_newtran(N,:) ) * eta_newtran(N,:)
!    %Normal velocity: Null-gradient 
!        v_newtran(1:N+1:N,1:M) = v_newtran(2:N:N-2,1:M)
!    %Tangential velocity: GW radiation 
        u_newtran(1:N:N-1,2:M) =(u_oldtran(1:N:N-1,2:M)*(H_oldtran(1:N:N-1,2:M) + H_oldtran(1:N:N-1,1:M-1)) - 2.0*dt / dy *&
&	sqrt( g * 0.5 * ( H_oldtran(1:N:N-1,2:M)+H_oldtran(1:N:N-1,1:M-1)))*(u_oldtran(1:N:N-1,2:M)-u_oldtran( 2:N-1:N-3,2:M)))&
&	/( H_newtran(1:N:N-1,2:M) + H_newtran(1:N:N-1,1:M-1))
        eta_new=transpose(eta_newtran)
  	H_new=transpose(H_newtran)
        v_new=transpose(v_newtran)
        u_new=transpose(u_newtran)
	end if
	end

	subroutine ComputeTr(i)
	use physic_params
        implicit none
	double precision,dimension(M,N)::TrRHS
	integer,dimension(2)::ind
        integer :: i,a,b
!	Call for u-flux
	call Calflux(1D0)
	TrRHS=RHSeta
!	Call for v-flux
	call Calflux(-1D0)
        TrRHS=TrRHS+RHSeta
!       Tracer FORWARD-IN-TIME SCHEME
        ind=minloc(TrRHS)
        if ( TrRHS(ind(1),ind(2)) .eq. 0.0) then
        dt_tr= 100.0*dt
        else
        dt_tr = dble(min( floor( abs(- H(ind(1),ind(2)) * Tr(ind(1),ind(2)) / TrRHS(ind(1),ind(2))) / dt ), 100)) * dt
        end if
        Tr_new =(H * Tr + dt_tr * TrRHS) / H_new
!       Update Tracer
    	Tr = Tr_new
	end
	
        subroutine Calflux(weight)
        use physic_params
        implicit none
        external transpose
        double precision transpose
        double precision :: weight
	double precision,dimension(M+1,N):: Hm
        double precision,dimension(M+1,N):: TrUm
        double precision,dimension(M+1,N):: TrUp
        double precision,dimension(M+1,N):: TrDif
        double precision,dimension(N+1,M):: Hmtran
        double precision,dimension(N+1,M):: TrUmtran
        double precision,dimension(N+1,M):: TrUptran
        double precision,dimension(N+1,M):: TrDiftran
        double precision,dimension(M+1,N):: Temp
        double precision,dimension(N+1,M):: Temptran
        if (weight .gt. 0D0) then
!   Interior 
    TrUm(1:M,:) = 0.5 * (abs(u_a(1:M,:)) - u_a(1:M,:)) * Tr
    TrUp(2:M+1,:) = 0.5 * (abs(u_a(2:M+1,:)) + u_a(2:M+1,:)) * Tr
    TrDif(2:M,:) = Tr(1:M-1,:) - Tr(2:M,:)
    Hm(2:M,:) = 0.5 * ( H(1:M-1,:) + H(2:M,:) )
!   Boundaries
    TrUm(M+1,:) = 0.5 * (abs(u_a(M+1,:)) - u_a(M+1,:)) * Tr(M,:)
    TrUp(1,:) = 0.5 * (abs(u_a(1,:)) + u_a(1,:)) * Tr(1,:)
    TrDif(1:M+1:M,:) = 0.0
    Hm(1:M+1:M,:) = H(1:M:M-1,:)
!   CalFlux
    Temp = Hm*(TrUp-TrUm + K/dx*TrDif)/dx
!   Flux balance e.g. Tracer increment
    RHSeta = Temp(1:M,:)-Temp(2:M+1,:)
	else
	v_atran=transpose(v_a)
        Trtran=transpose(Tr)
        Htran=transpose(H)
!   Interior 
    	TrUmtran(1:N,:) = 0.5 * (abs(v_atran(1:N,:)) - v_atran(1:N,:)) * Trtran
    	TrUptran(2:N+1,:) = 0.5 * (abs(v_atran(2:N+1,:)) + v_atran(2:N+1,:)) * Trtran
    	TrDiftran(2:N,:) = Trtran(1:N-1,:) - Trtran(2:N,:)
    	Hmtran(2:N,:) = 0.5 * (Htran(1:N-1,:) + Htran(2:N,:) )
!   Boundaries
    	TrUmtran(N+1,:) = 0.5 * (abs(v_atran(N+1,:)) - v_atran(N+1,:)) * Trtran(N,:)
    	TrUptran(1,:) = 0.5 * (abs(v_atran(1,:)) + v_atran(1,:)) * Trtran(1,:)
    	TrDiftran(1:N+1:N,:) = 0.0
    	Hm(1:N+1:N,:) = H(1:N:N-1,:)
!   CalFlux
    	Temptran = Hmtran*(TrUptran-TrUmtran + K/dy*TrDiftran)/dy
!   Flux balance e.g. Tracer increment
    	RHSetatran = Temptran(1:N,:)-Temptran(2:N+1,:)
    	RHSeta=transpose(RHSetatran)
	end if
	end
