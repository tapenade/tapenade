
      BLOCK DATA        
      COMMON/PARAMETER/dt,kp,dl
      integer niter,nsim
      real dl,dt,kp
      data dt,kp/1800.,50./           
      END

*******************************************************************

      program   inverse

      INCLUDE 'INVERSE_TEST.inc'

      COMMON/twoDarrays/
     1     t1(im,in),t2(im,in),t3(im,in),t4(im,in),t5(im,in)
      
      external simul_rc,euclid
      external ctonb,ctcab
      
      integer  i,j,k,p,indic,io,imode(2),iz(5),nr,impres
      integer omode,izs(nn),imr,inr
      
      real pi,a,tetsc,xmax,aa,bb,xxmax,yymax,ubb
      real ymax,ff0,tetg,rzs(nn),xminr,xmaxr,yminr,ymaxr,dum1,dum2
      

      double precision xx,ff,gg,dxmin,df1,dz(ndz),dzs(nn),epsg
      double precision u,v,psi,advu,advv,dif,tr
      double precision t1,t2,t3,t4,t5,tt,tt0,tr0,u0,v0,uv
      
      logical reverse
      
      dimension u(im1,in),v(im,in1),uv(im,in),tt(im,in),tt0(im,in)      
      dimension psi(im+1,in+1),tetg(im,in),u0(im,in),v0(im,in)      
      dimension xx(nn),gg(nn),tr0(im+2,in+2),ubb(im+1,in)      
      character*4 ds

      dl=300000/in
c     **************************************************************

c     INITIAL CONDITIONS FOR THE "TRUE" FIELDS

        CALL CREATION_IMAGE(tt0,u0,v0,u,v)
        
c     printing of initial fields

        CALL ECRITUREGRD('tt0.grd',tt0) 
        CALL ECRITUREGRD('uu0.grd',u0) 
        CALL ECRITUREGRD('vv0.grd',v0) 

c     ************************************************************

c     INTEGRATION OF TEMPERATURE EQUATION IN DIRECT TIME
c     AND GENERATION OF "OBSERVATIONS"
        
        
      DO p=1,q

         CALL  modele_direct_T2(tt0,u,v,tt)  
            
         IF(p.eq.p1) CALL Tcopy(tt0,t1)         
         IF(p.eq.p2) CALL Tcopy(tt0,t2)
         IF(p.eq.p3) CALL Tcopy(tt0,t3)
         IF(p.eq.p4) CALL Tcopy(tt0,t4)          
         IF(p.eq.p5) CALL Tcopy(tt0,t5)  
                       
         CALL Tcopy(tt,tt0) 
          
      ENDDO

c     print of "observations"

c     CALL ECRITUREGRD('t01.grd',t1)
c     CALL ECRITUREGRD('t02.grd',t2)
c     CALL ECRITUREGRD('t03.grd',t3)
c     CALL ECRITUREGRD('t04.grd',t4)
c     CALL ECRITUREGRD('t05.grd',t5)

      ff0=0.
      
      DO i=1,im
         DO j=1,in
            ff0=ff0+t1(i,j)**2+t2(i,j)**2+t3(i,j)**2
            ff0=ff0+t4(i,j)**2+t5(i,j)**2
         ENDDO
      ENDDO
      
      ff0=0.5*ff0
      
c     ********************************************************************************************      
      
c     SIMULATION OF GUESS ARRAYS FOR T,U,V

      WRITE (*,*) 'debut evaluation du 1st guess T'

      DO i=1,im
         DO j=1,in
            tetg(i,j)=0.2*(t1(i,j)+t2(i,j)+t3(i,j)+t4(i,j)+t5(i,j))
         ENDDO
      ENDDO

      DO i=1,im1
         DO j=1,in
            u(i,j)=0. 
         ENDDO
      ENDDO

      DO i=1,im
         DO j=1,in1
            v(i,j)=0.
        ENDDO
      ENDDO
      
      WRITE (*,*)'tetg',  tetg(10,10)

     
c     formation of the coupled vector

      WRITE (*,*) 'debut formation des vecteurs coupl�s 1D'
      DO i=1,im1
         DO j=1,in
            xx(im1*(j-1)+i)=u(i,j)
         ENDDO
      ENDDO

      DO i=1,im
         DO j=1,in1
            xx(nu+im*(j-1)+i)=v(i,j)
         ENDDO
      ENDDO

      DO i=1,im
	DO j=1,in
           xx(nu+nv+im*(j-1)+i)=tetg(i,j)
         ENDDO
      ENDDO

      WRITE (*,*) 'fin formation des vecteurs coupl�s 1D'

      indic=1

      WRITE (*,*) 'Appel de simul'
      CALL SIMUL(indic,nn,xx,ff,gg)
      WRITE (*,*) 'Retour de simul'

  
c     ******OPTIMIZATION LOOP***********************************

      reverse=.true.
	
      indic=1
      dxmin=0.000000001
      df1=ff0/1000.
      epsg=0.000001
      impres=3
      io=0
      imode(1)=1
      imode(2)=0
c     niter=3
c     nsim=3

      nr=0

      DO 
         call m1qn3 (simul_rc,euclid,ctonb,ctcab,nn,xx,ff,gg,dxmin,df1,
     1           epsg,impres,io,imode,omode,niter,nsim,iz,dz,ndz,
     2 	           reverse,indic,izs,rzs,dzs) 
         nr=nr+1
         write(*,*) nr,ff

 
         if (indic.eq.0) exit
         call simul(indic,nn,xx,ff,gg)
      ENDDO      


c     **********OUTPUT****************************************************

c     unpack arrays

      DO i=1,im
         DO j=1,in
             tt(i,j)=xx(nu+nv+im*(j-1)+i)
         ENDDO
      ENDDO

      DO i=1,im1
         DO j=1,in
            u(i,j)=xx(im1*(j-1)+i)
        ENDDO
      ENDDO

      DO i=1,im
         DO j=1,in1
            v(i,j)=xx(nu+im*(j-1)+i)
         ENDDO
      ENDDO

 61    format(a4)
 62    format(2i4)
 64    format(2f8.3)
 65    format(2f8.2)	
 66    format(2f6.2)

 9999  format(1071f8.3)
 
      CALL  ECRITUREGRD('tt1.grd',tt) 
    

      DO i=1,im
         DO j=1,in
            uv(i,j)=0.5*(u(i,j)+u(i+1,j))
         ENDDO
      ENDDO

      CALL  ECRITUREGRD('uu1.grd',uv)

      
      DO i=1,im
         DO j=1,in
            uv(i,j)=0.5*(v(i,j)+v(i,j+1))
         ENDDO
      ENDDO

      CALL  ECRITUREGRD('vv1.grd',uv)

      
   
      END program inverse 

**************************************************
c     6 tourbillons  
**************************************************
      SUBROUTINE CREATION_IMAGE(t0,u0,v0,u,v)

      INCLUDE 'INVERSE_TEST.inc'

      DOUBLE PRECISION u0,v0,t0  
      DIMENSION u0(im,in),v0(im,in),t0(im,in)                     
     
      DOUBLE PRECISION psi,u,v
      DIMENSION u(im+1,in),v(im,in+1)

      DIMENSION psi(im+1,in+1)  
      INTEGER i,j      
      REAL pi,a,tetsc 

      dl=300000/in
      pi=3.14159	
      a=0.5*90000./pi
      tetsc=100.
      
      DO i=1,im+1
         DO j=1,in+1
            psi(i,j)=a*sin(2*pi*(i-1)/im)*sin(3*pi*(j-1)/in)
         ENDDO
      ENDDO
      
      DO i=1,im+1
         DO j=1,in
            u(i,j)=(psi(i,j+1)-psi(i,j))/dl
         ENDDO
      ENDDO
      
      
      DO i=1,im
         DO j=1,in+1
            v(i,j)=-(psi(i+1,j)-psi(i,j))/dl
         ENDDO
      ENDDO
      
      DO i=1,im
         DO j=1,in
            t0(i,j)=tetsc*((j-0.5)/in-0.5)
         ENDDO
      ENDDO
      
      DO i=1,im
         DO j=1,in
            u0(i,j)=0.5*(u(i,j)+u(i+1,j))                            
            v0(i,j)=0.5*(v(i,j)+v(i,j+1))             
         ENDDO
      ENDDO
      
      END SUBROUTINE CREATION_IMAGE


***********************************************************
c                ECRITURE GRD                 
***********************************************************


      SUBROUTINE ECRITUREGRD(filename,t)

      INCLUDE 'INVERSE_TEST.inc'
                                                                      
      CHARACTER* 7 filename
      DIMENSION t(im,in)
      DOUBLE PRECISION t
      INTEGER  i,j

      CHARACTER*4 ds
        
      INTEGER imr, inr
      REAL xminr, yminr, xmaxr, ymaxr
      INTEGER dum1,dum2
      dl=300000/in
      ds ='DSAA'
      imr= im 
      inr= in
      xminr=  0.  
      yminr=  0.  
      xmaxr=im*dl/1000 
      ymaxr=in*dl/1000
      dum1 = -3  
      dum2 = 3


      OPEN(UNIT=50, FILE=filename, STATUS='unknown')

      WRITE(50,'(a4)')   ds 
      WRITE(50,'(2i4)')   imr,inr 
      WRITE(50,'(2f8.3)') xminr,xmaxr
      WRITE(50,'(2f8.3)') yminr,ymaxr
      WRITE(50,'(2i4)') dum1,dum2

      DO j=1,in
           WRITE(50,9999) (t(i,j),i=1,im)
      ENDDO

      CLOSE (UNIT=50)

 43   format(109f16.4)              
 9999  format(1071f8.3)  

      END SUBROUTINE ECRITUREGRD
***************************************************************
   
*********        modele direct              ************************
    
C             INPUT     t1;U1;V1 
c             OUTPUT    t2 
*******************************************************************

      SUBROUTINE modele_direct_T2(tt0,u,v,tt)  

      INCLUDE 'INVERSE_TEST.inc'

      COMMON/twoDarrays/
     1     t1(im,in),t2(im,in),t3(im,in),t4(im,in),t5(im,in)  
                                                       
      DOUBLE PRECISION U1,V1,tt0,tt,tr0,Ur,Vr,t1,t2,t3,t4,t5
      DOUBLE PRECISION advu,advv,dif,u,v
      
      DIMENSION U1(im,in),V1(im,in), tt0(im,in),tt(im,in)
      DIMENSION tr0(im+2,in+2), u(im1,in),v(im,in1)

      INTEGER  i,j

      dl=300000/in
      kp=50.
      dt=1800.
 
      CALL T_Tr(tt0,tr0)
             
c     simulation of the advection and diffusion terms and temperature field
c     on the next time step
         

      
         DO i=1,im
            DO j=1,in
               
               advu=u(i,j)*(tr0(i+1,j+1)-tr0(i,j+1))/dl
               advu=advu+u(i+1,j)*(tr0(i+2,j+1)-tr0(i+1,j+1))/dl
               advu=0.5*advu
               advv=v(i,j)*(tr0(i+1,j+1)-tr0(i+1,j))/dl
               advv=advv+v(i,j+1)*(tr0(i+1,j+2)-tr0(i+1,j+1))/dl
               advv=0.5*advv         
               dif=tr0(i,j+1)+tr0(i+1,j+2)+tr0(i+2,j+1)+tr0(i+1,j)
     +              -4*tr0(i+1,j+1)        
               dif=kp*dif/dl/dl        
               tt(i,j)=tt0(i,j)+(dif-advu-advv)*dt
               
            ENDDO
         ENDDO  
         
                         
      END SUBROUTINE modele_direct_T2  

************************************************************
c     T copy � Tobs
***********************************************************
      SUBROUTINE Tcopy(T,Tobs)

      INCLUDE 'INVERSE_TEST.inc'        
        
      INTEGER i,j   
      DOUBLE PRECISION T,Tobs	
      DIMENSION T(im,in),Tobs(im,in)  
      DO i=1,im
         DO j=1,in                 
            Tobs(i,j)=T(i,j)              
         ENDDO
      ENDDO
      
      END SUBROUTINE Tcopy


****************************************************************
c        t(im,in) ----- t0(im+2,in+2)
c
c        le bord de t0(im+2,in+2) copy le bord de t(im,in)  
c                                                   
****************************************************************

      SUBROUTINE T_Tr(t,tr)
      
      INCLUDE 'INVERSE_TEST.inc'            

      DOUBLE PRECISION t,tr
      DIMENSION t(im,in), tr(im+2,in+2)
      INTEGER  i,j
   
      DO i=2,im+1 
         DO j=2,in+1
            tr(i,j)=t(i-1,j-1)
         ENDDO
      ENDDO
      
      DO i=2,im+1
         tr(i,1)=tr(i,2)
         tr(i,in+2)=tr(i,in+1)
      ENDDO
      
      DO j=2,in+1
         tr(1,j)=tr(2,j)
         tr(im+2,j)=tr(im+1,j)
      ENDDO
      
      tr(1,1)=tr(1,2)
      tr(1,in+2)=tr(1,in+1)
      tr(im+2,1)=tr(im+1,1)
      tr(im+2,in+2)=tr(im+1,in+2)


      END SUBROUTINE T_Tr

*****************************************************************
C     INPUT     T2; U2; V2; tdirect, udirect,vdirect
c     OUTPUT    T1; 
c     tdirect, udirect,vdirect ne servent pas dans modele_adjoint_T1
******************************************************************
      SUBROUTINE modele_adjoint_T1(t2,u2,v2,t1)
                                                        
      INCLUDE 'INVERSE_TEST.inc'   
      
      DOUBLE PRECISION t2,u2,v2,t1,tr,ur,vr,tdirect,udirect,vdirect         
      DIMENSION t1(im,in)
      DIMENSION u2(im1,in),v2(im,in1),t2(im,in)
      DIMENSION udirect(im,in),vdirect(im,in),tdirect(im,in)
      DIMENSION tr(im+2,in+2), ur(im+1,in),vr(im,in+1)
      INTEGER  i,j     

      DOUBLE PRECISION adu,adv,df

      dl=300000/in
      dt=1800.
      kp=50.

      CALL T_Tr(t2,tr)
c      CALL UV_UrVr(u2,ur,v2,vr)

c      write(*,*)'t2=',t2(10,10),'  tr=' ,tr(10,10)  

c      stop
      DO j=1,in
         DO i=1,im
           
            adu=u2(i+1,j)*(tr(i+2,j+1)+tr(i+1,j+1))
            adu=adu-u2(i,j)*(tr(i+1,j+1)+tr(i,j+1))
            adu=0.5*adu/dl
            adv=v2(i,j+1)*(tr(i+1,j+2)+tr(i+1,j+1))
            adv=adv-v2(i,j)*(tr(i+1,j+1)+tr(i+1,j))
            adv=0.5*adv/dl
            df=tr(i,j+1)+tr(i+1,j+2)+tr(i+2,j+1)+tr(i+1,j)
     +           -4*tr(i+1,j+1)
            df=kp*df/dl/dl           
            t1(i,j)=t2(i,j)+(adu+adv+df)*dt 
c         write(*,*)'adu=',adu,'  adv=' ,adv, '  df=' ,df
c            write(*,*)'  df=' ,df
         ENDDO
      ENDDO



c      write(*,*)'t2=',t2(10,10),'  t1=' ,t1(10,10)  


     
c      write(*,*)'adu=',adu,'  adv=' ,adv, '  df=' ,df

      write(*,*)'  df=' ,df

      END   SUBROUTINE modele_adjoint_T1

***********************************************************

      SUBROUTINE modele_inverse_T1(t2,u2,v2,Tdirect,tobs,t1)
                                                        
      INCLUDE 'INVERSE_TEST.inc'   

      DOUBLE PRECISION t2,u2,v2,t1,tr,Ur,Vr,ass,Tdirect,tobs         
      DIMENSION t1(im,in)
      DIMENSION u2(im1,in),v2(im,in1),t2(im,in)
      DIMENSION tr(im+2,in+2), ur(im+1,in),vr(im,in+1)
      DIMENSION Tdirect(im,in),tobs(im,in)
      INTEGER  i,j     
      
      DOUBLE PRECISION adu,adv,df

      dl=300000/in
      dt=1800.
      kp=50.

      CALL T_Tr(t2,tr)
            
      DO j=1,in
         DO i=1,im
                        
            adu=u2(i+1,j)*(tr(i+2,j+1)+tr(i+1,j+1))
            adu=adu-u2(i,j)*(tr(i+1,j+1)+tr(i,j+1))
            adu=0.5*adu/dl
            adv=v2(i,j+1)*(tr(i+1,j+2)+tr(i+1,j+1))
            adv=adv-v2(i,j)*(tr(i+1,j+1)+tr(i+1,j))
            adv=0.5*adv/dl
            df=tr(i,j+1)+tr(i+1,j+2)+tr(i+2,j+1)+tr(i+1,j)
     +           -4*tr(i+1,j+1)
            df=kp*df/dl/dl
c            t1(i,j)=t2(i,j)+(adu+adv+df)*dt 


            ass=Tdirect(i,j)-tobs(i,j)
            t1(i,j)=t2(i,j)+(adu+adv+df-ass/dt)*dt 
        
         ENDDO
      ENDDO
           
      END   SUBROUTINE modele_inverse_T1
***********************************************************

      SUBROUTINE modele_inverse_U1V1(t2,u2,v2,tdirect,u1,v1)

      INCLUDE 'INVERSE_TEST.inc'         
                                                                    
      DOUBLE PRECISION t2,u2,v2,tdirect,u1,v1,tr1,tr         
      DIMENSION u1(im1,in),v1(im,in1)
      DIMENSION u2(im1,in),v2(im,in1),t2(im,in),tdirect(im,in)
      DIMENSION tr1(im+2,in+2),tr(im+2,in+2)
      INTEGER  i,j     
      DOUBLE PRECISION adu,adv

      dl=300000/in
      dt=1800.
      kp=50.
      CALL T_Tr(t2,tr1)
      CALL T_Tr(tdirect,tr)
  
      DO i=1,im1
         DO j=1,in
            adu=0.5*(tr1(i+1,j+1)+tr1(i,j+1))*(tr(i+1,j+1)-tr(i,j+1))
            u1(i,j)=u2(i,j)-adu/dl*dt
         ENDDO
      ENDDO
      
      DO i=1,im
         DO j=1,in1
            adv=0.5*(tr1(i+1,j+1)+tr1(i+1,j))*(tr(i+1,j+1)-tr(i+1,j))
            v1(i,j)=v2(i,j)-adv/dl*dt
         ENDDO
      ENDDO
      
      END SUBROUTINE modele_inverse_U1V1
                                                  
c     ******************************************************************
      SUBROUTINE MODELE_DIRECT(t0,u0,v0,t,u,v)

      INCLUDE 'INVERSE_TEST.inc'
 
      DOUBLE PRECISION u0,v0,t0  
      DIMENSION u0(im1,in),v0(im,in1),t0(im,in) 
      
      DIMENSION u(im1,in),v(im,in1),t(im,in)        
      DOUBLE PRECISION u,v,t
                 
      CALL modele_DIRECT_T2(t0,u0,v0,t)         
      CALL modele_DIRECT_U2V2(u0,v0,u,v)         
         
      END SUBROUTINE MODELE_DIRECT
c     ******************************************************************

***************************************************************
C             INTPUT  V1; U1 
C             OUTPUT  U2; V2
c
C             Le modele direct plus simple:  U2=U1 ; V2=V1
*************************************************************
      SUBROUTINE MODELE_DIRECT_U2V2(U1,V1,U2,V2)

      INCLUDE 'INVERSE_TEST.inc'                                                                      
 
      DOUBLE PRECISION U1,V1,U2,V2            
      DIMENSION U1(im1,in),V1(im,in1), U2(im1,in),V2(im,in1)

      INTEGER  i,j     

      DO j=1,in
         DO i=1,im1
           U2(i,j)=U1(i,j)
         ENDDO
      ENDDO

      DO j=1,in1
         DO i=1,im
           V2(i,j)=V1(i,j)
         ENDDO
      ENDDO

      END SUBROUTINE MODELE_DIRECT_U2V2
***************************************************************

      SUBROUTINE TETE(t,u,v,f)
      
      INCLUDE 'INVERSE_TEST.inc'   
      COMMON/twoDarrays/
     1  t51(im,in),t52(im,in),t53(im,in),t54(im,in),t55(im,in)
      double precision t51,t52,t53,t54,t55
      double precision  t1,u1,v1
      double precision  t2,u2,v2           
      integer p

      dimension t1(im,in),u1(im+1,in),v1(im,in+1)
      dimension t2(im,in),u2(im+1,in),v2(im,in+1)

       DO j=1,in
         DO i=1,im
           t1(i,j)=t(i,j)
         ENDDO
      ENDDO

       DO j=1,in
         DO i=1,im1
           U1(i,j)=U(i,j)
         ENDDO
      ENDDO

      DO j=1,in1
         DO i=1,im
           V1(i,j)=V(i,j)
         ENDDO
      ENDDO
     

      DO p = 1, q
         CALL modele_direct(t1,u1,v1,t2,u2,v2)
         DO i=1,im
            DO j=1,in   
               if (pp.eq.p1) f=f+0.5*(t51(i,j)-t(i,j))**2
               if (pp.eq.p2) f=f+0.5*(t52(i,j)-t(i,j))**2
               if (pp.eq.p3) f=f+0.5*(t53(i,j)-t(i,j))**2
               if (pp.eq.p4) f=f+0.5*(t54(i,j)-t(i,j))**2
               if (pp.eq.p5) f=f+0.5*(t55(i,j)-t(i,j))**2
            ENDDO
         ENDDO       
         DO j=1,in
            DO i=1,im
               t1(i,j)=t2(i,j)
            ENDDO
         ENDDO

         DO j=1,in
            DO i=1,im1
               U1(i,j)=U2(i,j)
            ENDDO
         ENDDO

         DO j=1,in1
            DO i=1,im
               V1(i,j)=V2(i,j)
            ENDDO
         ENDDO       
                 
      ENDDO
     
      END SUBROUTINE TETE

c     ******************************************************************

      SUBROUTINE SIMUL(indic, n,x,f,g)

      INCLUDE 'INVERSE_TEST.inc'   
      
	COMMON/twoDarrays/
     1 t1(im,in),t2(im,in),t3(im,in),t4(im,in),t5(im,in)
      
        double precision  tet,tet0,df,adu,adv,ass,ug,vg,tr1,tr
        double precision t1,t2,t3,t4,t5,tb,ub,vb,tb0,ub0,vb0
        double precision x(n),g(n),f,tdr

        dimension ug(im+1,in),vg(im,in+1),tdr(im,in)
	dimension tb0(im,in),tr1(im+2,in+2),tet(im,in)
	dimension ub0(im+1,in),vb0(im,in+1),tet0(im,in)
	dimension tb(im,in),ub(im+1,in),vb(im,in+1),tr(im+2,in+2)
	
	integer pp,s,indic

        dl=300000/in  

        DO i=1,im
           DO j=1,in
              tb0(i,j)=0.
           ENDDO
        ENDDO
           
        DO i=1,im1
           DO j=1,in
              ub0(i,j)=0.
           ENDDO
        ENDDO
        
        DO i=1,im
           DO j=1,in1
              vb0(i,j)=0.
           ENDDO
        ENDDO
 

         
 
         
      DO i=1,im1
         DO j=1,in
            ug(i,j)=x(im1*(j-1)+i)
         ENDDO
      ENDDO
      
      DO i=1,im
         DO j=1,in1
            vg(i,j)=x(nu+im*(j-1)+i)
         ENDDO
      ENDDO
      

      qq=q

      f=0.
***************************************************************
      do 2000 s=1,qq

         do 230 i=1,im
            do 230 j=1,in
               tet0(i,j)=x(nu+nv+im*(j-1)+i)
 230        continue
            

            DO pp=1,qq-s+1 
           
               CALL modele_direct_T2(tet0,ug,vg,tet)
               
               IF (s.gt.1) goto 315              
               DO i=1,im
                  DO j=1,in   
                     if (pp.eq.p1) f=f+0.5*(t1(i,j)-tet0(i,j))**2
                     if (pp.eq.p2) f=f+0.5*(t2(i,j)-tet0(i,j))**2
                     if (pp.eq.p3) f=f+0.5*(t3(i,j)-tet0(i,j))**2
                     if (pp.eq.p4) f=f+0.5*(t4(i,j)-tet0(i,j))**2
                     if (pp.eq.p5) f=f+0.5*(t5(i,j)-tet0(i,j))**2
                  ENDDO
               ENDDO               
 315           CONTINUE
           
c     STOGAGE tdirect for modele_inverse             
               CALL Tcopy(tet0,tdr) 
      
c     rewriting new temperature field under the old name
               CALL Tcopy(tet,tet0)
      
            ENDDO
      
c     end of direct equation simulation. Next step of inverse equation.

            CALL modele_adjoint_T1(tb0,ug,vg,tb)
            
            DO i=1,im
               DO j=1,in  
                  ass=0.
             IF(qq-s+1.eq.p1)  ass=tdr(i,j)-t1(i,j)                      
             IF(qq-s+1.eq.p2)  ass=tdr(i,j)-t2(i,j)                                    
             IF(qq-s+1.eq.p3)  ass=tdr(i,j)-t3(i,j)                                 
             IF(qq-s+1.eq.p4)  ass=tdr(i,j)-t4(i,j)                                   
             IF(qq-s+1.eq.p5) then 
                ass=tdr(i,j)-t5(i,j)
              WRITE (*,*)'when p5, ass', ass
            endif
             tb(i,j)=tb(i,j)-ass
          ENDDO
       ENDDO 

       IF(qq-s+1.eq.p5) then
          WRITE (*,*)'when p5, tb0=', tb0(10,10),'tb=', tb(10,10)
          WRITE (*,*)'ug=', ug(10,10),'vg=', vg(10,10)
       ENDIF

C            CALL modele_inverse_T1(tb0,ug,vg,tdr,tdr,tb) 
            
C       IF(qq-s+1.eq.p1)CALL modele_inverse_T1(tb0,ug,vg,tdr,t1,tb)                        
C       IF(qq-s+1.eq.p2)CALL modele_inverse_T1(tb0,ug,vg,tdr,t2,tb)                                    
C       IF(qq-s+1.eq.p3)CALL modele_inverse_T1(tb0,ug,vg,tdr,t3,tb)                                    
C       IF(qq-s+1.eq.p4)CALL modele_inverse_T1(tb0,ug,vg,tdr,t4,tb)                                       
C       IF(qq-s+1.eq.p5)CALL modele_inverse_T1(tb0,ug,vg,tdr,t5,tb)  
       
      
      CALL modele_inverse_U1V1(tb0,ub0,vb0,tdr,ub,vb)
      
      
      CALL Tcopy(tb,tb0) 
      
      
      do 360 i=1,im1
         do 360 j=1,in
            ub0(i,j)=ub(i,j)
 360     continue
         
         do 365 i=1,im
            do 365 j=1,in1
               vb0(i,j)=vb(i,j)
 365        continue
            
	             
 2000    continue 
         

         write(*,*)'vb(i,j)',vb(10,10)

c         stop
         f=f
         
         do 370 i=1,im1
            do 370  j=1,in
               g(im1*(j-1)+i)=-ub0(i,j)
 370        continue 
            
            do 375 i=1,im
               do 375 j=1,in1
                  g(nu+im*(j-1)+i)=-vb0(i,j)
 375           continue
               
               do 380 i=1,im
                  do 380 j=1,in
                     g(nu+nv+im*(j-1)+i)=-tb0(i,j)
380	continue
        
        

      return
	
	end     



c     ********************************************************************************************

      subroutine m1qn3 (simul,prosca,ctonb,ctcab,n,x,f,g,dxmin,df1,
     /                  epsg,impres,io,imode,omode,niter,nsim,iz,dz,ndz,
     /                  reverse,indic,izs,rzs,dzs)
c----
c
c     M1QN3, Version 3.0, March 2005
c     Jean Charles Gilbert, Claude Lemarechal, INRIA.
c
c     M1qn3 has two running modes: the SID (Scalar Initial Scaling) mode
c     and the DIS (Diagonal Initial Scaling) mode. Both do not require
c     the same amount of storage, the same subroutines, ...
c     In the description below, items that differ in the DIS mode with
c     respect to the SIS mode are given in brakets.
c
c     Use the following subroutines:
c         M1QN3A
c         DDD, DDDS
c         NLIS0 + DCUBE (Dec 88)
c         MUPDTS, DYSTBL.
c
c     The following routines are proposed to the user in case the
c     Euclidean scalar product is used:
c         DUCLID, DTONBE, DTCABE.
c
c     La sous-routine M1QN3 est une interface entre le programme
c     appelant et la sous-routine M1QN3A, le minimiseur proprement dit.
c
c     Le module PROSCA est sense realiser le produit scalaire de deux
c     vecteurs de Rn; le module DTONB est sense realiser le changement
c     de coordonnees correspondant au changement de bases: base
c     euclidienne -> base orthonormale (pour le produit scalaire
c     PROSCA); le module CTBAB fait la transformation inverse: base
c     orthonormale -> base euclidienne.
c
c     Iz is an integer working zone for M1QN3A, its dimension is 5.
c     It is formed of 5 scalars that are set by the optimizer:
c         - the dimension of the problem,
c         - an identifier of the scaling mode,
c         - the number of updates,
c         - two pointers.
c
c     Dz est la zone de travail pour M1QN3A, de dimension ndz.
c     Elle est subdivisee en
c         3 [ou 4] vecteurs de dimension n: d,gg,[diag,]aux
c         m scalaires: alpha
c         m vecteurs de dimension n: ybar
c         m vecteurs de dimension n: sbar
c
c     m est alors le plus grand entier tel que
c         m*(2*n+1)+3*n .le. ndz [m*(2*n+1)+4*n .le. ndz)]
c     soit m := (ndz-3*n) / (2*n+1) [m := (ndz-4*n) / (2*n+1)].
c     Il faut avoir m >= 1, donc ndz >= 5n+1 [ndz >= 6n+1].
c
c     A chaque iteration la metrique est formee a partir d un multiple
c     de l'identite [d'une matrice diagonale] D qui est mise a jour m
c     fois par la formule de BFGS en utilisant les m couples {y,s} les
c     plus recents.
c
c     Reverse communication is monitored by the logical variables
c     reverse and indic.
c
c     reverse (logical, input)
c       .true.  : reverse communication
c       .false. : direct communication
c     indic (integer, input-output) has only a meaning in reverse
c       communication
c       (on entry)  = 0 stop
c                   = 1 first call
c                   = 2-4 function-gradient normally computed
c                   < 0 function-gradient could not be computed
c       (on return) > 0 please compute function-gradient
c                   = 0 m1qn3 has finished.
c
c----
c
c         arguments
c
      logical reverse
      integer n,impres,io,imode(2),omode,niter,nsim,iz(5),ndz,izs(*),indic
      real rzs(*)
      double precision x(n),f,g(n),dxmin,df1,epsg,dz(ndz),dzs(*)
      external simul,prosca,ctonb,ctcab
c
c         variables locales
c
      logical inmemo,sscale
      integer ntravu,id,igg,idiag,iaux,ialpha,iybar,isbar,m,mmemo
      double precision d1,d2,ps
      save
c
c --- skip if reverse (in all the subroutines, the label 9999 is used
c     for reverse communication skipping)
	reverse=.true.
	

c
      if (reverse .and. indic.ne.1) goto 9999
c
c---- impressions initiales et controle des arguments
c
      if (impres.ge.1) then
          write (io,900) n,dxmin,df1,epsg,niter,nsim,impres
          if (reverse) then
              write (io,'(5x,a)') "reverse communication"
          else
              write (io,'(5x,a)') "direct communication"
          endif
      endif
900   format (/" M1QN3 (Version 3.0, March 2005): entry point"/
     /    5x,"dimension of the problem (n):",i14/
     /    5x,"absolute precision on x (dxmin):",9x,1pd9.2/
     /    5x,"expected decrease for f (df1):",11x,1pd9.2/
     /    5x,"relative precision on g (epsg):",10x,1pd9.2/
     /    5x,"maximal number of iterations (niter):",i6/
     /    5x,"maximal number of simulations (nsim):",i6/
     /    5x,"printing level (impres):",15x,i4)
      if (n.le.0.or.niter.le.0.or.nsim.le.0.or.dxmin.le.0.d+0.or.
     /    epsg.le.0.d+0.or.epsg.gt.1.d+0) then
          omode=2
          if (reverse) indic = 0
          if (impres.ge.1) write (io,901)
901       format (/" >>> m1qn3: inconsistent call")
          return
      endif
c
c---- what method
c
      if (imode(1).eq.0) then
          if (impres.ge.1) write (io,920)
  920     format (/" m1qn3: Diagonal Initial Scaling mode")
          sscale=.false.
      else
          if (impres.ge.1) write (io,921)
  921     format (/" m1qn3: Scalar Initial Scaling mode")
          sscale=.true.
      endif
c
      if ((ndz.lt.5*n+1).or.((.not.sscale).and.(ndz.lt.6*n+1))) then
          omode=2
          if (reverse) indic = 0
          if (impres.ge.1) write (io,902)
902       format (/" >>> m1qn3: not enough memory allocated")
          return
      endif
c
c---- Compute m
c
      call mupdts (sscale,inmemo,n,m,ndz)
c
c     --- Check the value of m (if (y,s) pairs in core, m will be >= 1)
c
      if (m.lt.1) then
          omode=2
          if (reverse) indic = 0
          if (impres.ge.1) write (io,9020)
 9020     format (/" >>> m1qn3: m is set too small in mupdts")
          return
      endif
c
c     --- mmemo = number of (y,s) pairs in core memory
c
      mmemo=1
      if (inmemo) mmemo=m
c
      ntravu=2*(2+mmemo)*n+m
      if (sscale) ntravu=ntravu-n
      if (impres.ge.1) write (io,903) ndz,ntravu,m
903   format (/5x,"allocated memory (ndz) :",i9/
     /         5x,"used memory :           ",i9/
     /         5x,"number of updates :     ",i9)
      if (ndz.lt.ntravu) then
          omode=2
          if (reverse) indic = 0
          if (impres.ge.1) write (io,902)
          return
      endif
c
      if (impres.ge.1) then
          if (inmemo) then
              write (io,907)
          else
              write (io,908)
          endif
      endif
907   format (5x,"(y,s) pairs are stored in core memory")
908   format (5x,"(y,s) pairs are stored by the user")
c
c---- cold start or warm restart ?
c     check iz: iz(1)=n, iz(2)=(0 if DIS, 1 if SIS),
c               iz(3)=m, iz(4)=jmin, iz(5)=jmax
c
      if (imode(2).eq.0) then
          if (impres.ge.1) write (io,922)
      else
          iaux=0
          if (sscale) iaux=1
          if (iz(1).ne.n.or.iz(2).ne.iaux.or.iz(3).ne.m.or.iz(4).lt.1
     &        .or.iz(5).lt.1.or.iz(4).gt.iz(3).or.iz(5).gt.iz(3)) then
              omode=2
              if (reverse) indic = 0
              if (impres.ge.1) write (io,923)
              return
          endif
          if (impres.ge.1) write (io,924)
      endif
  922 format (/" m1qn3: cold start"/1x)
  923 format (/" >>> m1qn3: inconsistent iz for a warm restart")
  924 format (/" m1qn3: warm restart"/1x)
      iz(1)=n
      iz(2)=0
      if (sscale) iz(2)=1
      iz(3)=m
c
c---- split the working zone dz
c
      idiag=1
      iybar=idiag+n
      if (sscale) iybar=1
      isbar=iybar+n*mmemo
      id=isbar+n*mmemo
      igg=id+n
      iaux=igg+n
      ialpha=iaux+n
c
c---- call the optimization code
c
 9999 continue
      call m1qn3a (simul,prosca,ctonb,ctcab,n,x,f,g,dxmin,df1,epsg,
     /             impres,io,imode,omode,niter,nsim,inmemo,iz(3),iz(4),
     /             iz(5),dz(id),dz(igg),dz(idiag),dz(iaux),dz(ialpha),
     /             dz(iybar),dz(isbar),reverse,indic,izs,rzs,dzs)
      if (reverse .and. indic.ne.0) return
c
c---- impressions finales
c
      if (impres.ge.1) write (io,905) omode,niter,nsim,epsg
905   format (/1x,79("-")/
     /        /" m1qn3: output mode is ",i2
     /        /5x,"number of iterations: ",i14
     /        /5x,"number of simulations: ",i13
     /        /5x,"realized relative precision on g: ",1pd9.2)
      call prosca (n,x,x,ps,izs,rzs,dzs)
      d1=dsqrt(ps)
      call prosca (n,g,g,ps,izs,rzs,dzs)
      d2=dsqrt(ps)
      if (impres.ge.1) write (io,906) d1,f,d2
906   format (5x,"norm of x = ",1pd15.8
     /       /5x,"f         = ",1pd15.8
     /       /5x,"norm of g = ",1pd15.8)
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine m1qn3a (simul,prosca,ctonb,ctcab,n,x,f,g,dxmin,df1,
     /                   epsg,impres,io,imode,omode,niter,nsim,inmemo,m,
     /                   jmin,jmax,d,gg,diag,aux,alpha,ybar,sbar,
     /                   reverse,indic,izs,rzs,dzs)
c----
c
c     Code d'optimisation proprement dit.
c
c----
c
c         arguments
c
      logical inmemo,reverse
      integer n,impres,io,imode(2),omode,niter,nsim,m,jmin,jmax,izs(*),
     &    indic
      real rzs(*)
      double precision x(n),f,g(n),dxmin,df1,epsg,d(n),gg(n),diag(n),
     &    aux(n),alpha(m),ybar(n,1),sbar(n,1),dzs(*)
      external simul,prosca,ctonb,ctcab
c
c         variables locales
c
      logical sscale,cold,warm
      integer i,itmax,moderl,isim,jcour
      double precision d1,t,tmin,tmax,gnorm,eps1,ff,preco,precos,ys,den,
     &    dk,dk1,ps,ps2,hp0
      save
c
c         parametres
c
      double precision rm1,rm2
      parameter (rm1=0.0001d+0,rm2=0.99d+0)
      double precision pi
      parameter (pi=3.1415927d+0)
      double precision rmin
c
c --- skip if reverse
c
      if (reverse .and. indic.ne.1) goto 9999
c
c---- initialisation
c
      rmin=1.d-20
c
      sscale=.true.
      if (imode(1).eq.0) sscale=.false.
c
      warm=.false.
      if (imode(2).eq.1) warm=.true.
      cold=.not.warm
c
      itmax=niter
      niter=0
      isim=1
      eps1=1.d+0
c
      call prosca (n,g,g,ps,izs,rzs,dzs)
      gnorm=dsqrt(ps)
      if (impres.ge.1) write (io,900) f,gnorm
  900 format (5x,"f         = ",1pd15.8
     /       /5x,"norm of g = ",1pd15.8)
      if (gnorm.lt.rmin) then
          omode=2
          if (impres.ge.1) write (io,901)
          goto 1000
      endif
  901 format (/" >>> m1qn3a: initial gradient is too small")
c
c     --- initialisation pour dd
c
      if (cold) then
          jmin=1
          jmax=0
      endif
      jcour=1
      if (inmemo) jcour=jmax
c
c     --- mise a l'echelle de la premiere direction de descente
c
      if (cold) then
c
c         --- use Fletcher's scaling and initialize diag to 1.
c
          precos=2.d+0*df1/gnorm**2
          do 10 i=1,n
              d(i)=-g(i)*precos
              diag(i)=1.d+0
   10     continue
          if (impres.ge.5) write(io,902) precos
  902     format (/" m1qn3a: descent direction -g: precon = ",d10.3)
      else
c
c         --- use the matrix stored in [diag and] the (y,s) pairs
c
          if (sscale) then
              call prosca (n,ybar(1,jcour),ybar(1,jcour),ps,izs,rzs,dzs)
              precos=1.d+0/ps
          endif
          do 11 i=1,n
              d(i)=-g(i)
  11      continue
          if (inmemo) then
              call dd (prosca,ctonb,ctcab,n,sscale,m,d,aux,jmin,jmax,
     /                 precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
          else
              call dds (prosca,ctonb,ctcab,n,sscale,m,d,aux,jmin,jmax,
     /                  precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
          endif
      endif
c
      if (impres.eq.3) then
          write(io,903)
          write(io,904)
      endif
      if (impres.eq.4) write(io,903)
  903 format (/1x,79("-"))
  904 format (1x)
c
c     --- initialisation pour mlis3
c
      tmax=1.d+20
      call prosca (n,d,g,hp0,izs,rzs,dzs)
      if (hp0.ge.0.d+0) then
          omode=7
          if (impres.ge.1) write (io,905) niter,hp0
          goto 1000
      endif
  905 format (/" >>> m1qn3 (iteration ",i2,"): "
     /        /5x," the search direction d is not a ",
     /         "descent direction: (g,d) = ",d12.5)
c
c     --- compute the angle (-g,d)
c
      if (warm.and.impres.ge.5) then
          call prosca (n,g,g,ps,izs,rzs,dzs)
          ps=dsqrt(ps)
          call prosca (n,d,d,ps2,izs,rzs,dzs)
          ps2=dsqrt(ps2)
          ps=hp0/ps/ps2
          ps=dmin1(-ps,1.d+0)
          ps=dacos(ps)
          d1=ps*180.d+0/pi
          write (io,906) sngl(d1)
      endif
  906 format (/" m1qn3: descent direction d: ",
     /        "angle(-g,d) = ",f5.1," degrees")
c
c---- Debut de l'iteration. on cherche x(k+1) de la forme x(k) + t*d,
c     avec t > 0. On connait d.
c
c         Debut de la boucle: etiquette 100,
c         Sortie de la boucle: goto 1000.
c
100   niter=niter+1
      if (.not.reverse .and. impres.lt.0) then
          if (mod(niter,-impres).eq.0) then
              indic=1
              call simul (indic,n,x,f,g,izs,rzs,dzs)
          endif
      endif
      if (impres.ge.5) write(io,903)
      if (impres.ge.4) write(io,904)
      if (impres.ge.3) write (io,910) niter,isim,f,hp0
  910 format (" m1qn3: iter ",i3,", simul ",i3,
     /        ", f=",1pd15.8,", h'(0)=",d12.5)
      do 101 i=1,n
          gg(i)=g(i)
101   continue
      ff=f
c
c     --- recherche lineaire et nouveau point x(k+1)
c
      if (impres.ge.5) write (io,911)
  911 format (/" m1qn3: line search")
c
c         --- calcul de tmin
c
      tmin=0.d+0
      do 200 i=1,n
          tmin=dmax1(tmin,dabs(d(i)))
200   continue
      tmin=dxmin/tmin
      t=1.d+0
      d1=hp0
c
 9999 continue
      call mlis3 (n,simul,prosca,x,f,d1,t,tmin,tmax,d,g,rm2,rm1,
     &            impres,io,moderl,isim,nsim,aux,
     &            reverse,indic,izs,rzs,dzs)
      if (reverse .and. indic.ne.1) return
c
c         --- mlis3 renvoie les nouvelles valeurs de x, f et g
c
      if (moderl.ne.0) then
          if (moderl.lt.0) then
c
c             --- calcul impossible
c                 t, g: ou les calculs sont impossibles
c                 x, f: ceux du t_gauche (donc f <= ff)
c
              omode=moderl
          elseif (moderl.eq.1) then
c
c             --- descente bloquee sur tmax
c                 [sortie rare (!!) d'apres le code de mlis3]
c
              omode=3
              if (impres.ge.1) write(io,912) niter
  912         format (/" >>> m1qn3 (iteration ",i3,
     /                "): line search blocked on tmax: ",
     /                "decrease the scaling")
          elseif (moderl.eq.4) then
c
c             --- nsim atteint
c                 x, f: ceux du t_gauche (donc f <= ff)
c
              omode=5
          elseif (moderl.eq.5) then
c
c             --- arret demande par l'utilisateur (indic = 0)
c                 x, f: ceux en sortie du simulateur
c
              omode=0
          elseif (moderl.eq.6) then
c
c             --- arret sur dxmin ou appel incoherent
c                 x, f: ceux du t_gauche (donc f <= ff)
c
              omode=6
          endif
          goto 1000
      endif
c
c NOTE: stopping tests are now done after having updated the matrix, so
c that update information can be stored in case of a later warm restart
c
c     --- mise a jour de la matrice
c
      if (m.gt.0) then
c
c         --- mise a jour des pointeurs
c
          jmax=jmax+1
          if (jmax.gt.m) jmax=jmax-m
          if ((cold.and.niter.gt.m).or.(warm.and.jmin.eq.jmax)) then
              jmin=jmin+1
              if (jmin.gt.m) jmin=jmin-m
          endif
          if (inmemo) jcour=jmax
c
c         --- y, s et (y,s)
c
          do 400 i=1,n
              sbar(i,jcour)=t*d(i)
              ybar(i,jcour)=g(i)-gg(i)
400       continue
          if (impres.ge.5) then
              call prosca (n,sbar(1,jcour),sbar(1,jcour),ps,izs,rzs,dzs)
              dk1=dsqrt(ps)
              if (niter.gt.1) write (io,930) dk1/dk
  930         format (/" m1qn3: convergence rate, s(k)/s(k-1) = ",
     /                1pd12.5)
              dk=dk1
          endif
          call prosca (n,ybar(1,jcour),sbar(1,jcour),ys,izs,rzs,dzs)
          if (ys.le.0.d+0) then
              omode=7
              if (impres.ge.1) write (io,931) niter,ys
  931         format (/" >>> m1qn3 (iteration ",i2,
     /                "): the scalar product (y,s) = ",d12.5
     /                /27x,"is not positive")
              goto 1000
          endif
c
c         --- ybar et sbar
c
          d1=dsqrt(1.d+0/ys)
          do 410 i=1,n
              sbar(i,jcour)=d1*sbar(i,jcour)
              ybar(i,jcour)=d1*ybar(i,jcour)
  410     continue
          if (.not.inmemo) call ystbl (.true.,ybar,sbar,n,jmax)
c
c         --- compute the scalar or diagonal preconditioner
c
          if (impres.ge.5) write(io,932)
  932     format (/" m1qn3: matrix update:")
c
c             --- Here is the Oren-Spedicato factor, for scalar scaling
c
          if (sscale) then
              call prosca (n,ybar(1,jcour),ybar(1,jcour),ps,izs,rzs,dzs)
              precos=1.d+0/ps
c
              if (impres.ge.5) write (io,933) precos
  933         format (5x,"Oren-Spedicato factor = ",d10.3)
c
c             --- Scale the diagonal to Rayleigh's ellipsoid.
c                 Initially (niter.eq.1) and for a cold start, this is
c                 equivalent to an Oren-Spedicato scaling of the
c                 identity matrix.
c
          else
              call ctonb (n,ybar(1,jcour),aux,izs,rzs,dzs)
              ps=0.d0
              do 420 i=1,n
                  ps=ps+diag(i)*aux(i)*aux(i)
  420         continue
              d1=1.d0/ps
              if (impres.ge.5) then
                  write (io,934) d1
  934             format(5x,"fitting the ellipsoid: factor = ",1pd10.3)
              endif
              do 421 i=1,n
                  diag(i)=diag(i)*d1
  421         continue
c
c             --- update the diagonal
c                 (gg is used as an auxiliary vector)
c
              call ctonb (n,sbar(1,jcour),gg,izs,rzs,dzs)
              ps=0.d0
              do 430 i=1,n
                  ps=ps+gg(i)*gg(i)/diag(i)
  430         continue
              den=ps
              do 431 i=1,n
                  diag(i)=1.d0/
     &                   (1.d0/diag(i)+aux(i)**2-(gg(i)/diag(i))**2/den)
                  if (diag(i).le.0.d0) then
                      if (impres.ge.5) write (io,935) i,diag(i),rmin
                      diag(i)=rmin
                  endif
  431         continue
  935         format (/" >>> m1qn3-WARNING: diagonal element ",i8,
     &                 " is negative (",d10.3,"), reset to ",d10.3)
c
              if (impres.ge.5) then
                  ps=0.d0
                  do 440 i=1,n
                      ps=ps+diag(i)
  440             continue
                  ps=ps/n
                  preco=ps
c
                  ps2=0.d0
                  do 441 i=1,n
                      ps2=ps2+(diag(i)-ps)**2
  441             continue
                  ps2=dsqrt(ps2/n)
                  write (io,936) preco,ps2
  936             format (5x,"updated diagonal: average value = ",
     &                    1pd10.3,", sqrt(variance) = ",d10.3)
              endif
          endif
      endif
c
c     --- tests d'arret
c
      call prosca(n,g,g,ps,izs,rzs,dzs)
      eps1=ps
      eps1=dsqrt(eps1)/gnorm
c
      if (impres.ge.5) write (io,940) eps1
  940 format (/" m1qn3: stopping criterion on g: ",1pd12.5)
      if (eps1.lt.epsg) then
          omode=1
          goto 1000
      endif
      if (niter.eq.itmax) then
          omode=4
          if (impres.ge.1) write (io,941) niter
  941     format (/" >>> m1qn3 (iteration ",i3,
     /            "): maximal number of iterations")
          goto 1000
      endif
      if (isim.gt.nsim) then
          omode=5
          if (impres.ge.1) write (io,942) niter,isim
  942     format (/" >>> m1qn3 (iteration ",i3,"): ",i6,
     /            " simulations (maximal number reached)")
          goto 1000
      endif
c
c     --- calcul de la nouvelle direction de descente d = - H.g
c
      if (m.eq.0) then
          preco=2.d0*(ff-f)/(eps1*gnorm)**2
          do 500 i=1,n
              d(i)=-g(i)*preco
  500     continue
      else
          do 510 i=1,n
              d(i)=-g(i)
  510     continue
          if (inmemo) then
              call dd (prosca,ctonb,ctcab,n,sscale,m,d,aux,jmin,jmax,
     /                  precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
          else
              call dds (prosca,ctonb,ctcab,n,sscale,m,d,aux,jmin,jmax,
     /                   precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
          endif
      endif
c
c         --- test: la direction d est-elle de descente ?
c             hp0 sera utilise par mlis3
c
      call prosca (n,d,g,hp0,izs,rzs,dzs)
      if (hp0.ge.0.d+0) then
          omode=7
          if (impres.ge.1) write (io,905) niter,hp0
          goto 1000
      endif
      if (impres.ge.5) then
          call prosca (n,g,g,ps,izs,rzs,dzs)
          ps=dsqrt(ps)
          call prosca (n,d,d,ps2,izs,rzs,dzs)
          ps2=dsqrt(ps2)
          ps=hp0/ps/ps2
          ps=dmin1(-ps,1.d+0)
          ps=dacos(ps)
          d1=ps
          d1=d1*180.d0/pi
          write (io,906) sngl(d1)
      endif
c
c---- on poursuit les iterations
c
      goto 100
c
c---- retour
c
 1000 continue
c     --- n1qn3 has finished for ever: hence indic = 0
      indic=0
      nsim=isim
      epsg=eps1
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine dd (prosca,ctonb,ctcab,n,sscale,nm,depl,aux,jmin,jmax,
     &                precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
c----
c
c     calcule le produit H.g ou
c         . H est une matrice construite par la formule de bfgs inverse
c           a nm memoires a partir de la matrice diagonale diag
c           dans un espace hilbertien dont le produit scalaire
c           est donne par prosca
c           (cf. J. Nocedal, Math. of Comp. 35/151 (1980) 773-782)
c         . g est un vecteur de dimension n (en general le gradient)
c
c     la matrice diag apparait donc comme un preconditionneur diagonal
c
c     depl = g (en entree), = H g (en sortie)
c
c     la matrice H est memorisee par les vecteurs des tableaux
c     ybar, sbar et les pointeurs jmin, jmax
c
c     alpha(nm) est une zone de travail
c
c     izs(*),rzs(*),dzs(*) sont des zones de travail pour prosca
c
c----
c
c         arguments
c
      logical sscale
      integer n,nm,jmin,jmax,izs(*)
      real rzs(*)
      double precision depl(n),precos,diag(n),alpha(nm),ybar(n,1),
     &    sbar(n,1),aux(n),dzs(*)
      external prosca,ctonb,ctcab
c
c         variables locales
c
      integer jfin,i,j,jp
      double precision r,ps
c
      jfin=jmax
      if (jfin.lt.jmin) jfin=jmax+nm
c
c         phase de descente
c
      do 100 j=jfin,jmin,-1
          jp=j
          if (jp.gt.nm) jp=jp-nm
          call prosca (n,depl,sbar(1,jp),ps,izs,rzs,dzs)
          r=ps
          alpha(jp)=r
          do 20 i=1,n
              depl(i)=depl(i)-r*ybar(i,jp)
20        continue
100   continue
c
c         preconditionnement
c
      if (sscale) then
          do 150 i=1,n
              depl(i)=depl(i)*precos
  150     continue
      else
          call ctonb (n,depl,aux,izs,rzs,dzs)
          do 151 i=1,n
              aux(i)=aux(i)*diag(i)
  151     continue
          call ctcab (n,aux,depl,izs,rzs,dzs)
      endif
c
c         remontee
c
      do 200 j=jmin,jfin
          jp=j
          if (jp.gt.nm) jp=jp-nm
          call prosca (n,depl,ybar(1,jp),ps,izs,rzs,dzs)
          r=alpha(jp)-ps
          do 120 i=1,n
              depl(i)=depl(i)+r*sbar(i,jp)
120       continue
200   continue
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine dds (prosca,ctonb,ctcab,n,sscale,nm,depl,aux,jmin,
     &                 jmax,precos,diag,alpha,ybar,sbar,izs,rzs,dzs)
c----
c
c     This subroutine has the same role as dd (computation of the
c     product H.g). It supposes however that the (y,s) pairs are not
c     stored in core memory, but on a devise chosen by the user.
c     The access to this devise is performed via the subroutine ystbl.
c
c----
c
c         arguments
c
      logical sscale
      integer n,nm,jmin,jmax,izs(*)
      real rzs(*)
      double precision depl(n),precos,diag(n),alpha(nm),ybar(n),sbar(n),
     &    aux(n),dzs(*)
      external prosca,ctonb,ctcab
c
c         variables locales
c
      integer jfin,i,j,jp
      double precision r,ps
c
      jfin=jmax
      if (jfin.lt.jmin) jfin=jmax+nm
c
c         phase de descente
c
      do 100 j=jfin,jmin,-1
          jp=j
          if (jp.gt.nm) jp=jp-nm
          call ystbl (.false.,ybar,sbar,n,jp)
          call prosca (n,depl,sbar,ps,izs,rzs,dzs)
          r=ps
          alpha(jp)=r
          do 20 i=1,n
              depl(i)=depl(i)-r*ybar(i)
20        continue
100   continue
c
c         preconditionnement
c
      if (sscale) then
          do 150 i=1,n
              depl(i)=depl(i)*precos
  150     continue
      else
          call ctonb (n,depl,aux,izs,rzs,dzs)
          do 151 i=1,n
              aux(i)=aux(i)*diag(i)
  151     continue
          call ctcab (n,aux,depl,izs,rzs,dzs)
      endif
c
c         remontee
c
      do 200 j=jmin,jfin
          jp=j
          if (jp.gt.nm) jp=jp-nm
          call ystbl (.false.,ybar,sbar,n,jp)
          call prosca (n,depl,ybar(1),ps,izs,rzs,dzs)
          r=alpha(jp)-ps
          do 120 i=1,n
              depl(i)=depl(i)+r*sbar(i)
120       continue
200   continue
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine mlis3 (n,simul,prosca,x,f,fpn,t,tmin,tmax,d,g,
     1                  amd,amf,imp,io,logic,nap,napmax,xn,
     1                  reverse,indic,izs,rzs,dzs)
c ----
c
c     mlis3 + minuscules + commentaires
c     + version amelioree (XII 88): interpolation cubique systematique
c       et anti-overflows
c     + declaration variables (II/89, JCG).
c     + barr is also progressively decreased (12/93, CL & JChG).
c       barmul is set to 5.
c
c     ----------------------------------------------------------------
c
c        en sortie logic =
c
c        0          descente serieuse
c        1          descente bloquee sur tmax
c        4          nap > napmax
c        5          retour a l'utilisateur
c        6          fonction et gradient pas d'accord
c        < 0        contrainte implicite active
c
c        en sortie indic =
c        1           mlis3 a termine
c        4           calcul fonction-gradient
c
c ----
c
c --- arguments
c
      external simul,prosca
      logical reverse
      integer n,imp,io,logic,nap,napmax,izs(*),indic
      real rzs(*)
      double precision x(n),f,fpn,t,tmin,tmax,d(n),g(n),amd,amf,xn(n),
     /    dzs(*)
c
c --- variables locales
c
      logical t_increased
      integer i,indica,indicd
      double precision tesf,tesd,tg,fg,fpg,td,ta,fa,fpa,d2,fn,fp,ffn,fd,
     / fpd,z,test,barmin,barmul,barmax,barr,gauche,droite,taa,ps
      save
c
 1000 format (/4x," mlis3   ",4x,"fpn=",1pd10.3," d2=",d9.2,
     1 "  tmin=",d9.2," tmax=",d9.2)
 1001 format (/4x," mlis3",3x,"stop on tmin",8x,
     1   "step",11x,"functions",5x,"derivatives")
 1002 format (4x," mlis3",37x,1pd10.3,2d11.3)
 1003 format (4x," mlis3",1pd14.3,2d11.3)
 1004 format (4x," mlis3",37x,1pd10.3," indic=",i3)
 1005 format (4x," mlis3",14x,1pd18.8,d18.8,d11.3)
 1006 format (4x," mlis3",14x,1pd18.8,"      indic=",i3)
 1007 format (/4x," mlis3",10x,"tmin forced to tmax")
 1008 format (/4x," mlis3",10x,"inconsistent call")
c
c --- skip if reverse
c
      if (reverse .and. indic.ne.1) goto 9999
c
      if (n.gt.0 .and. fpn.lt.0.d0 .and. t.gt.0.d0
     1 .and. tmax.gt.0.d0 .and. amf.gt.0.d0
     1 .and. amd.gt.amf .and. amd.lt.1.d0) go to 5
      logic=6
      go to 999
    5 tesf=amf*fpn
      tesd=amd*fpn
      barmin=0.01d0
      barmul=5.d0
      barmax=0.3d0
      barr=barmin
      td=0.d0
      tg=0.d0
      fn=f
      fg=fn
      fpg=fpn
      ta=0.d0
      fa=fn
      fpa=fpn
      call prosca (n,d,d,ps,izs,rzs,dzs)
      d2=ps
c
c               elimination d'un t initial ridiculement petit
c
c<
c     if (t.gt.tmin) go to 20
c     t=tmin
c     if (t.le.tmax) go to 20
c     if (imp.gt.0) write (io,1007)
c     tmin=tmax
c  20 if (fn+t*fpn.lt.fn+0.9d0*t*fpn) go to 30
c     t=2.d0*t
c     go to 20
c changed into
      if (t.lt.tmin) then
          t=tmin
          if (imp.ge.4) write (io,'(a)')
     &        ' mlis3: initial step-size increased to tmin'
          if (t.gt.tmax) then
              if (imp.gt.0) write (io,1007)
              tmin=tmax
          endif
      endif
      t_increased = .false.
      do while (fn+t*fpn.ge.fn+0.9d0*t*fpn)
          t_increased = .true.
          t = 2.d0*t
      enddo
      if (t_increased .and. (imp.ge.4)) write (io,'(a,1pd10.3)')
     &    ' mlis3: initial step-size increased to ',t
c>
   30 indica=1
      logic=0
      if (t.gt.tmax) then
          t=tmax
          logic=1
      endif
      if (imp.ge.4) write (io,1000) fpn,d2,tmin,tmax
c
c     --- nouveau x
c         initialize xn to the current iterate
c         use x as the trial iterate
c
      do i=1,n
          xn(i)=x(i)
          x(i)=xn(i)+t*d(i)
      enddo
c
c --- boucle
c
  100 nap=nap+1
      if(nap.gt.napmax) then
          logic=4
          fn=fg
          do i=1,n
              xn(i)=xn(i)+tg*d(i)
          enddo
          go to 999
      endif
c
c     --- appel simulateur
c
      indic=4
      if (reverse) then
          return
      else
          call simul(indic,n,x,f,g,izs,rzs,dzs)
      endif
 9999 continue
      if (indic.eq.0) then
c
c         --- arret demande par l'utilisateur
c
          logic=5
          fn=f
          do 170 i=1,n
              xn(i)=x(i)
  170     continue
          go to 999
      endif
      if(indic.lt.0) then
c
c         --- les calculs n'ont pas pu etre effectues par le simulateur
c
          td=t
          indicd=indic
          logic=0
          if (imp.ge.4) write (io,1004) t,indic
          t=tg+0.1d0*(td-tg)
          go to 905
      endif
c
c     --- les tests elementaires sont faits, on y va
c
      call prosca (n,d,g,ps,izs,rzs,dzs)
      fp=ps
c
c     --- premier test de Wolfe
c
      ffn=f-fn
      if(ffn.gt.t*tesf) then
          td=t
          fd=f
          fpd=fp
          indicd=indic
          logic=0
          if(imp.ge.4) write (io,1002) t,ffn,fp
          go to 500
      endif
c
c     --- test 1 ok, donc deuxieme test de Wolfe
c
      if(imp.ge.4) write (io,1003) t,ffn,fp
      if(fp.gt.tesd) then
          logic=0
          go to 320
      endif
      if (logic.eq.0) go to 350
c
c     --- test 2 ok, donc pas serieux, on sort
c
  320 fn=f
      do 330 i=1,n
          xn(i)=x(i)
  330 continue
      go to 999
c
c
c
  350 tg=t
      fg=f
      fpg=fp
      if(td.ne.0.d0) go to 500
c
c              extrapolation
c
      taa=t
      gauche=(1.d0+barmin)*t
      droite=10.d0*t
      call ecube (t,f,fp,ta,fa,fpa,gauche,droite)
      ta=taa
      if(t.lt.tmax) go to 900
      logic=1
      t=tmax
      go to 900
c
c              interpolation
c
  500 if(indica.le.0) then
          ta=t
          t=0.9d0*tg+0.1d0*td
          go to 900
      endif
      test=barr*(td-tg)
      gauche=tg+test
      droite=td-test
      taa=t
      call ecube (t,f,fp,ta,fa,fpa,gauche,droite)
      ta=taa
      if (t.gt.gauche .and. t.lt.droite) then
          barr=dmax1(barmin,barr/barmul)
c         barr=barmin
        else
          barr=dmin1(barmul*barr,barmax)
      endif
c
c --- fin de boucle
c     - t peut etre bloque sur tmax
c       (venant de l'extrapolation avec logic=1)
c
  900 fa=f
      fpa=fp
  905 indica=indic
c
c --- faut-il continuer ?
c
      if (td.eq.0.d0) go to 950
      if (td-tg.lt.tmin) go to 920
c
c     --- limite de precision machine (arret de secours) ?
c
      do 910 i=1,n
          z=xn(i)+t*d(i)
          if (z.ne.xn(i).and.z.ne.x(i)) go to 950
  910 continue
c
c --- arret sur dxmin ou de secours
c
  920 logic=6
c
c     si indicd<0, derniers calculs non faits par simul
c
      if (indicd.lt.0) logic=indicd
c
c     si tg=0, xn = xn_depart,
c     sinon on prend xn=x_gauche qui fait decroitre f
c
      if (tg.eq.0.d0) go to 940
      fn=fg
      do 930 i=1,n
  930 xn(i)=xn(i)+tg*d(i)
  940 if (imp.le.0) go to 999
      write (io,1001)
      write (io,1005) tg,fg,fpg
      if (logic.eq.6) write (io,1005) td,fd,fpd
      if (logic.eq.7) write (io,1006) td,indicd
      go to 999
c
c               recopiage de x et boucle
c
  950 do 960 i=1,n
  960 x(i)=xn(i)+t*d(i)
      go to 100
c     --- linesearch finished, no skip at next entry in mlis3
  999 indic=1
      return
      end
c
c
c-----------------------------------------------------------------------
c
      subroutine ecube (t,f,fp,ta,fa,fpa,tlower,tupper)
c
c --- arguments
c
      double precision sign,den,anum,t,f,fp,ta,fa,fpa,tlower,tupper
c
c --- variables locales
c
      double precision z1,b,discri
c
c           Using f and fp at t and ta, computes new t by cubic formula
c           safeguarded inside [tlower,tupper].
c
      z1=fp+fpa-3.d0*(fa-f)/(ta-t)
      b=z1+fp
c
c              first compute the discriminant (without overflow)
c
      if (dabs(z1).le.1.d0) then
          discri=z1*z1-fp*fpa
        else
          discri=fp/z1
          discri=discri*fpa
          discri=z1-discri
          if (z1.ge.0.d0 .and. discri.ge.0.d0) then
              discri=dsqrt(z1)*dsqrt(discri)
              go to 120
          endif
          if (z1.le.0.d0 .and. discri.le.0.d0) then
              discri=dsqrt(-z1)*dsqrt(-discri)
              go to 120
          endif
          discri=-1.d0
      endif
      if (discri.lt.0.d0) then
          if (fp.lt.0.d0) t=tupper
          if (fp.ge.0.d0) t=tlower
          go to 900
      endif
c
c  discriminant nonnegative, compute solution (without overflow)
c
      discri=dsqrt(discri)
  120 if (t-ta.lt.0.d0) discri=-discri
      sign=(t-ta)/dabs(t-ta)
      if (b*sign.gt.0.d+0) then
          t=t+fp*(ta-t)/(b+discri)
        else
          den=z1+b+fpa
          anum=b-discri
          if (dabs((t-ta)*anum).lt.(tupper-tlower)*dabs(den)) then
              t=t+anum*(ta-t)/den
            else
              t=tupper
          endif
      endif
  900 t=dmax1(t,tlower)
      t=dmin1(t,tupper)
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine mupdts (sscale,inmemo,n,m,nrz)
c
c         arguments
c
      logical sscale,inmemo
      integer n,m,nrz
c----
c
c     On entry:
c       sscale: .true. if scalar initial scaling,
c               .false. if diagonal initial scaling
c       n:      number of variables
c
c     This routine has to return:
c       m:      the number of updates to form the approximate Hessien H,
c       inmemo: .true., if the vectors y and s used to form H are stored
c                  in core memory,
c               .false. otherwise (storage of y and s on disk, for
c                  instance).
c     When inmemo=.false., the routine "ystbl", which stores and
c     restores (y,s) pairs, has to be rewritten.
c
c----
c
      if (sscale) then
          m=(nrz-3*n)/(2*n+1)
      else
          m=(nrz-4*n)/(2*n+1)
      endif
      inmemo=.true.
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine ystbl (store,ybar,sbar,n,j)
c----
c
c     This subroutine should store (if store = .true.) or restore
c     (if store = .false.) a pair (ybar,sbar) at or from position
c     j in memory. Be sure to have 1 <= j <= m, where m in the number
c     of updates specified by subroutine mupdts.
c
c     The subroutine is used only when the (y,s) pairs are not
c     stored in core memory in the arrays ybar(.,.) and sbar(.,.).
c     In this case, the subroutine has to be written by the user.
c
c----
c
c         arguments
c
      logical store
      integer n,j
      double precision ybar(n),sbar(n)
c
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine ctonb (n,u,v,izs,rzs,dzs)
      integer n,izs(*)
      real rzs(*)
      double precision u(1),v(1),dzs(*)
c
      integer i
c
      do i=1,n
          v(i)=u(i)
      enddo
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine ctcab (n,u,v,izs,rzs,dzs)
      integer n,izs(*)
      real rzs(*)
      double precision u(1),v(1),dzs(*)
c
      integer i
c
      do i=1,n
          v(i)=u(i)
      enddo
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine euclid (n,x,y,ps,izs,rzs,dzs)
      integer n,izs(*)
      real rzs(*)
      double precision x(n),y(n),ps,dzs(*)
c
      integer i
c
      ps=0.d0
      do i=1,n
          ps=ps+x(i)*y(i)
      enddo
      return
      end
c
c-----------------------------------------------------------------------
c
      subroutine simul_rc (indic,n,x,f,g,izs,rzs,dzs)
      integer indic,n,izs(*)
      real rzs(*)
      double precision x(n),f,g(n),dzs(*)
c
      return
      end

