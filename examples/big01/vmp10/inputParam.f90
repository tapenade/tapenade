
!
!      ******************************************************************
!      *                                                                *
!      * File:          inputParam.f90                                  *
!      * Author:        Edwin van der Weide, Steve Repsher,             *
!      *                Arathi K. Gopinath, Andre C. Marta              *
!      * Starting date: 12-11-2002                                      *
!      * Last modified: 06-29-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module inputDiscretization
!
!      ******************************************************************
!      *                                                                *
!      * Input parameters which are related to the discretization of    *
!      * the governing equations, i.e. scheme parameters, time accuracy *
!      * (in case of an unsteady computation) and preconditioning info. *
!      *                                                                *
!      ******************************************************************
!
       use accuracy
       implicit none
       save
!
!      ******************************************************************
!      *                                                                *
!      * Definition of some parameters which make the code more         *
!      * readable. The actual values of this parameters are arbitrary;  *
!      * in the code always the symbolic names are (should be) used.    *
!      *                                                                *
!      ******************************************************************
!
       integer(kind=intType), parameter :: noDiss     = 1_intType, &
                                           dissScalar = 2_intType, &
                                           dissMatrix = 3_intType
       integer(kind=intType), parameter :: noPrecond  = 1_intType, &
                                           Turkel     = 2_intType, &
                                           ChoiMerkle = 3_intType

       integer(kind=intType), parameter :: NonConservative = 1_intType, &
                                           Conservative    = 2_intType
!
!      ******************************************************************
!      *                                                                *
!      * Definition of the discretization input parameters.             *
!      *                                                                *
!      ******************************************************************
!
       ! spaceDiscr:         Fine grid discretization.
       ! spaceDiscrCoarse:   Coarse grid discretization.
       ! orderFlow:          Order of the discretization for the mean
       !                     flow equations.
       ! orderTurb:          Order of the discretization of the advective
       !                     terms of the turbulent transport equations.
       ! precond:            Preconditioner.
       ! useCompactDiss:     Whether or not to use compact dissipation
       !                     stencil for the mean flow equations.
       ! useCompactDissTurb: Idem for the turbulence equations.
       ! useCompactViscous:  Whether or not to use compact viscous
       !                     stencil.
       ! nonMatchTreatment:  Treatment of the non-matching block
       !                     boundaries. Either NonConservative or
       !                     Conservative.
       ! coefDiss2nd:        Coefficient of the second order dissipation.
       ! coefDissHigh:       Coefficient of the higher order dissipation.
       ! coefDissCoarse:     Coefficient of the second order dissipation
       !                     on the coarser grids in the mg cycle. On the
       !                     coarser grids a first order scheme is used.
       ! tauPenalty:         Penalty parameter for the interface
       !                     treatment, tauPenalty >= 1.
       ! vortexCorr:         Whether or not a vortex correction must be
       !                     applied. Steady flow only.

       integer(kind=intType) :: spaceDiscr, spaceDiscrCoarse
       integer(kind=intType) :: orderFlow, orderTurb, precond
       integer(kind=intType) :: nonMatchTreatment

       real(kind=realType) :: coefDiss2nd, coefDissHigh, coefDissCoarse
       real(kind=realType) :: tauPenalty

       logical :: useCompactDiss, useCompactDissTurb, useCompactViscous
       logical :: vortexCorr

       end module inputDiscretization

!      ==================================================================

       module inputPhysics
!
!      ******************************************************************
!      *                                                                *
!      * Input parameters which are related to the physics of the flow, *
!      * like governing equations, mode of the equations, turbulence    *
!      * model and free stream conditions.                              *
!      *                                                                *
!      ******************************************************************
!
       use precision
       implicit none
       save
!
!      ******************************************************************
!      *                                                                *
!      * Definition of some parameters which make the code more         *
!      * readable. The actual values of this parameters are arbitrary;  *
!      * in the code always the symbolic names are (should be) used.    *
!      *                                                                *
!      ******************************************************************
!
       integer(kind=intType), parameter ::                              &
                                      EulerEquations       = 1_intType, &
                                      NSEquations          = 2_intType, &
                                      RANSEquations        = 3_intType, &
                                      MHDLowMagReEquations = 4_intType, &
                                      MHDIdealEquations    = 5_intType, &
                                      MHDFullEquations     = 6_intType
       integer(kind=intType), parameter :: steady        = 1_intType,   &
                                           unsteady      = 2_intType,   &
                                           timeSpectral  = 3_intType
       integer(kind=intType), parameter :: internalFlow = 1_intType,    &
                                           externalFlow = 2_intType
       integer(kind=intType), parameter :: cpConstant      = 1_intType, &
                                           cpTempCurveFits = 2_intType
       integer(kind=intType), parameter ::                              &
                                  baldwinLomax           =  1_intType,  &
                                  spalartAllmaras        =  2_intType,  &
                                  spalartAllmarasEdwards =  3_intType,  &
                                  komegaWilcox           =  4_intType,  &
                                  komegaModified         =  5_intType,  &
                                  ktau                   =  6_intType,  &
                                  menterSST              =  7_intType,  &
                                  v2f                    = 10_intType
       integer(kind=intType), parameter :: strain       = 1_intType,    &
                                           vorticity    = 2_intType,    &
                                           katoLaunder  = 3_intType
!
!      ******************************************************************
!      *                                                                *
!      * Definition of the physics input parameters.                    *
!      *                                                                *
!      ******************************************************************
!
       ! equations:           Governing equations to be solved.
       ! equationMode:        Mode of the equations, steady, unsteady
       !                      or timeSpectral.
       ! flowType:            Type of flow, internal or external.
       ! cpModel:             Which cp model, constant or function of
       !                      temperature via curve fits.
       ! turbModel:           Turbulence model.
       ! turbProd:            Which production term to use in the transport
       !                      turbulence equations, strain, vorticity or
       !                      kato-launder.
       ! rvfN:                Determines the version of v2f turbulence model.
       ! rvfB:                Whether or not to solve v2f with an
       !                      upper bound.
       ! wallFunctions:       Whether or not to use wall functions.
       ! wallDistanceNeeded:  Whether or not the wall distance is needed
       !                      for the turbulence model in a RANS problem.
       ! Mach:                Free stream Mach number.
       ! MachCoef:            Mach number used to compute coefficients;
       !                      only relevant for translating geometries.
       ! velDirFreestream(3): Direction of the free-stream velocity.
       !                      Internally this vector is scaled to a unit
       !                      vector, so there is no need to specify a
       !                      unit vector. Specifying this vector solves
       !                      the problem of angle of attack and yaw angle
       !                      definition as well as the direction of the
       !                      axis (e.g. y- or z-axis in spanwise direction).
       ! rhoInfFreeStream:    Free stream density in S.I-units. Together with
       !                      pInfFreeStream and vInfFreeStream this is an
       !                      alternative way of specifying the free-stream.
       !                      Useful when you don't know the Mach and
       !                      Reynolds number or if you want to do moving
       !                      bodies.
       ! pInfFreeStream:      Free stream pressure in S.I-units. See the
       !                      comments for rhoInfFreeStream.
       ! vInfFreeStream:      Free stream velocity in S.I-units. See the
       !                      comments for rhoInfFreeStream.
       ! TInfFreeStream:      Free stream temperature in Kelvin.
       ! RGasDim:             Gas constant in S.I. units.
       ! liftDirection(3):    Direction vector for the lift.
       ! dragDirection(3):    Direction vector for the drag.
       ! Reynolds:            Reynolds number.
       ! ReynoldsLength:      Length used to compute the Reynolds number.
       ! gammaConstant:       Constant specific heat ratio.
       ! RGasDim:             Gas constant in S.I. units.
       ! Prandtl:             Prandtl number.
       ! PrandtlTurb:         Turbulent prandtl number.
       ! pklim:               Limiter for the production of k, the production
       !                      is limited to pklim times the destruction.
       ! wallOffset:          Offset from the wall when wall functions
       !                      are used.
       ! eddyVisInfRatio:     Free stream value of the eddy viscosity.
       ! turbIntensityInf:    Free stream value of the turbulent intensity.
       ! surfaceRef:          Reference area for the force and moments
       !                      computation.
       ! lengthRef:           Reference length for the moments computation.
       ! pointRef(3):         Moment reference point.

       integer(kind=intType) :: equations, equationMode, flowType
       integer(kind=intType) :: turbModel, cpModel, turbProd
       integer(kind=intType) :: rvfN
       logical               :: rvfB

       logical :: wallFunctions, wallDistanceNeeded

       real(kind=realType) :: Mach, MachCoef
       real(kind=realType) :: Reynolds, ReynoldsLength
       real(kind=realType) :: rhoInfFreeStream, pInfFreeStream
       real(kind=realType) :: vInfFreeStream,   TInfFreeStream
       real(kind=realType) :: gammaConstant, RGasDim
       real(kind=realType) :: Prandtl, PrandtlTurb, pklim, wallOffset
       real(kind=realType) :: eddyVisInfRatio, turbIntensityInf
       real(kind=realType) :: surfaceRef, lengthRef

       real(kind=realType), dimension(3) :: velDirFreestream
       real(kind=realType), dimension(3) :: liftDirection
       real(kind=realType), dimension(3) :: dragDirection
       real(kind=realType), dimension(3) :: pointRef

       ! mu0InfFreeStream:   Free stream magnetic permeability in 
       !                     SI-units (free space = 4 pi * 10^-7 H/m).
       ! sigmaInfFreeStream: Free stream electrical conductivity in
       !                     SI-units.

       real(kind=realType) :: mu0InfFreeStream, sigmaInfFreeStream

       ! pointBref(3):       Imposed magnetic field reference point.

       real(kind=realType), dimension(3) :: pointBref

       end module inputPhysics
