
!
!     ******************************************************************
!     *                                                                *
!     * File:          residualAdj.f90                                 *
!     * Author:        Edwin van der Weide, Andre C. Marta             *
!     * Starting date: 03-15-2003                                      *
!     * Last modified: 07-31-2006                                      *
!     *                                                                *
!     ******************************************************************
!
      subroutine residualAdj(wAdj, dwAdj, iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * residualAdj computes the residual of the mean flow equations   *
!     * on the finest grid level using auxiliar routines based on the  *
!     * node indices.                                                  *
!     *                                                                *
!     * Simple routine to compute the residual in a single node of the *
!     *  multiblock mesh solver SUmbVertex. The state in a stencil that*
!     *  is 5 nodes wide in each coordinate direction (centered about  *
!     *  the central node) is given by wAdj(-2:2,-2:2,-2:2,nw) and the *
!     *  residual is returned in the array dwAdj(nw).                  *
!     *                                                                *
!     * This routine is based on /solver/residual.f90.                 *
!     *                                                                *
!     ******************************************************************
!
!!      use blockPointers
!!      use communication
      use flowVarRefState     ! nw
      use inputDiscretization ! spaceDiscr
!!      use inputIteration
!!      use inputPhysics
!!      use iteration
!!      use section
      implicit none
!
!     Subroutine arguments
!
      real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                   intent(in) :: wAdj
      real(kind=realType), dimension(nw), intent(out) :: dwAdj

      integer(kind=intType), intent(in) :: iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: i, j, k, l

      real(kind=realType), dimension(-2:2,-2:2,-2:2) :: rhoEAdj
      real(kind=realType), dimension(-1:1,nw) :: fluxXiAdj,  &
                                                 fluxEtaAdj, &
                                                 fluxZetaAdj
!
!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      ! Compute the total energy in terms of the variables wAdj.

      call computeEnergyAdj(wAdj, rhoEAdj)

      ! Initialize the residual to zero.

      do l=1,nwFlow
        dwAdj(l) = zero
      enddo
!
!     ******************************************************************
!     *                                                                *
!     * Inviscid fluxes.                                               *
!     *                                                                *
!     ******************************************************************
!
      ! Compute the inviscid fluxes for the specified node.

      call inviscidFluxAdj(wAdj, rhoEAdj, fluxXiAdj, fluxEtaAdj, &
                           fluxZetaAdj, iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     *  Artificial dissipation residual, if needed.                   *
!     *                                                                *
!     ******************************************************************
!
      testDiss: if(spaceDiscr /= noDiss) then

      ! Compute the artificial dissipation for all time instances.

!!      call artificialDissipationRes(nn, nTime, discr)

      endif testDiss
!
!     ******************************************************************
!     *                                                                *
!     * Inviscid residuals.                                            *
!     *                                                                *
!     ******************************************************************
!
      ! Compute the inviscid and viscous residuals.

      call inviscidAndViscousResAdj(wAdj, dwAdj, fluxXiAdj,  &
                                    fluxEtaAdj, fluxZetaAdj, &
                                    iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * Magnetic residuals for MagnetoHydroDynamics.                   *
!     *                                                                *
!     ******************************************************************
!
      ! Check whether or not the MHD residuals should be added

!!      testMHD : if( magnetic ) then

        ! MHD residuals should be added.
        ! Loop over the number of local blocks.

!!        domainMagResLoop: do nn=1,nDom

          ! Determine the number of time instances for this block.

!!          sectionID = flowDoms(nn,currentLevel,1)%sectionID
!!          nTime     = sections(sectionID)%nTimeInstances

          ! Set the owned block dimensions.

!!          il = flowDoms(nn,currentLevel,1)%il
!!          jl = flowDoms(nn,currentLevel,1)%jl
!!          kl = flowDoms(nn,currentLevel,1)%kl

          ! Determine MHD residuals according to the equations that
          ! are being solved.

!!          select case (equations)

!!            case (MHDLowMagReEquations)

              ! Compute the low Reynolds magnetic model residuals for
              ! all time instances.

!!              call lowMagneticSourceRes(nn, nTime)

!!            case (MHDIdealEquations)

              ! Compute the ideal MHD model residuals for
              ! all time instances.
               
!!              call terminate("residual", &
!!                             "Ideal MHD residuals not implemented yet")

!!            case (MHDFullEquations)

              ! Compute the ideal MHD model residuals for
              ! all time instances.

!!              call terminate("residual", &
!!                             "Full MHD residuals not implemented yet")


!!          end select

!!        enddo domainMagResLoop

!!      endif testMHD 
!
!     ******************************************************************
!     *                                                                *
!     * Add the penalty terms to the discretization, both internal and *
!     * boundary conditions.                                           *
!     *                                                                *
!     ******************************************************************
!
      ! Determine the state vector to penalize against for the
      ! physical boundary conditions.

!!    call penaltyStateBCs(nn, nTime, correctForK)

      ! Compute the penalty terms on the (original) block boundaries.

!!    call penaltyTerms(nn, nTime, correctForK)

      end subroutine residualAdj
