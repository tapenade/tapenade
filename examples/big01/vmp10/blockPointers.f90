
!
!      ******************************************************************
!      *                                                                *
!      * File:          blockPointers.f90                               *
!      * Author:        Edwin van der Weide, Steve Repsher,             *
!      *                Andre C. Marta                                  *
!      * Starting date: 03-07-2003                                      *
!      * Last modified: 07-21-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module blockPointers
!
!      ******************************************************************
!      *                                                                *
!      * This module contains the pointers for all variables inside a   *
!      * block. The pointers are set via the subroutine setPointers,    *
!      * which can be found in the utils directory. In this way the     *
!      * code becomes much more readable. The relation to the original  *
!      * multiblock grid is not copied, because it does not affect the  *
!      * computation.                                                   *
!      *                                                                *
!      * See the module block for the meaning of the variables.         *
!      *                                                                *
!      * Note that the dimensions are not pointers, but integers.       *
!      * Consequently changing dimensions of a block must be done only  *
!      * with the variables of floDoms.                                 *
!      *                                                                *
!      ******************************************************************
!
       use block
       implicit none
!
!      ******************************************************************
!      *                                                                *
!      * Additional info, such that it is known to which block the data *
!      * inside this module belongs.                                    *
!      *                                                                *
!      ******************************************************************
!
       ! sectionID:   the section to which this block belongs.
       ! nbkLocal :   local block number.
       ! nbkGlobal:   global block number in the original cgns grid.
       ! mgLevel:     the multigrid level.
       ! spectralSol: the spectral solution index of this block.

       integer(kind=intType) :: sectionID
       integer(kind=intType) :: nbkLocal, nbkGlobal, mgLevel
       integer(kind=intType) :: spectralSol
!
!      ******************************************************************
!      *                                                                *
!      * Variables, which are either copied or the pointer is set to    *
!      * the correct variable in the block. See the module block for    *
!      * meaning of the variables.                                      *
!      *                                                                *
!      ******************************************************************
!
       integer(kind=intType) :: il, jl, kl, ib, jb, kb, ie, je, ke
       integer(kind=intType) :: icb, jcb, kcb, ice, jce, kce

       logical :: isRightHanded

       integer(kind=intType) :: orderFlowXi,   orderTurbXi
       integer(kind=intType) :: orderFlowEta,  orderTurbEta
       integer(kind=intType) :: orderFlowZeta, orderTurbZeta

       logical :: iMinBoundaryStencil, iMaxBoundaryStencil
       logical :: jMinBoundaryStencil, jMaxBoundaryStencil
       logical :: kMinBoundaryStencil, kMaxBoundaryStencil

       integer(kind=intType) :: iBegOr, iEndOr, jBegOr, jEndOr
       integer(kind=intType) :: kBegOr, kEndOr

       integer(kind=intType) :: nSubface, nBocos
       integer(kind=intType) :: nViscBocos, nInvBocos
       integer(kind=intType) :: n1to1Internal, n1to1, nNonMatch

       integer(kind=intType), dimension(:), pointer :: BCType
       integer(kind=intType), dimension(:), pointer :: BCFaceID
       integer(kind=intType), dimension(:), pointer :: singularDirFace
       integer(kind=intType), dimension(:), pointer :: cgnsSubface
       integer(kind=intType), dimension(:), pointer :: subfaceFine
       integer(kind=intType), dimension(:), pointer :: subfaceCoarse

       integer(kind=intType), dimension(:), pointer :: iBeg, iEnd
       integer(kind=intType), dimension(:), pointer :: jBeg, jEnd
       integer(kind=intType), dimension(:), pointer :: kBeg, kEnd

       integer(kind=intType), dimension(:), pointer :: diBeg, diEnd
       integer(kind=intType), dimension(:), pointer :: djBeg, djEnd
       integer(kind=intType), dimension(:), pointer :: dkBeg, dkEnd

       integer(kind=intType), dimension(:), pointer :: neighBlock
       integer(kind=intType), dimension(:), pointer :: neighProc
       integer(kind=intType), dimension(:), pointer :: l1, l2, l3
       integer(kind=intType), dimension(:), pointer :: groupNum

       type(nonMatchNeighType), dimension(:), pointer :: nonMatchNeigh

       integer(kind=intType) :: nNodesOverset, nNodesOversetAll
       integer(kind=intType) :: nHoles, nOrphans

       integer(kind=intType), dimension(:,:,:), pointer :: iblank

       integer(kind=intType), dimension(:,:), pointer :: ibndry
       integer(kind=intType), dimension(:,:), pointer :: idonor
       real(kind=realType),   dimension(:,:), pointer :: overint

       integer(kind=intType), dimension(:), pointer :: neighBlockOver
       integer(kind=intType), dimension(:), pointer :: neighProcOver

       type(BCDataType),      dimension(:), pointer :: BCData
       type(penaltyDataType), dimension(:), pointer :: penaltyData

       real(kind=realType), dimension(:,:,:,:),   pointer :: x
       real(kind=realType), dimension(:,:,:,:,:), pointer :: xOld
       real(kind=realType), dimension(:,:,:,:),   pointer :: s

       logical :: blockIsMoving, addGridVelocities

       real(kind=realType), dimension(:,:,:,:), pointer :: dxi
       real(kind=realType), dimension(:,:,:,:), pointer :: deta
       real(kind=realType), dimension(:,:,:,:), pointer :: dzeta

       real(kind=realType), dimension(:,:,:),   pointer :: detJinv
       real(kind=realType), dimension(:,:,:,:), pointer :: detJinvOld

       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricA
       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricB
       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricC
       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricD
       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricE
       real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricF

       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbA
       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbB
       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbC
       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbD
       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbE
       real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbF

       real(kind=realType), dimension(:,:,:,:),   pointer :: w
       real(kind=realType), dimension(:,:,:,:,:), pointer :: UOld
       real(kind=realType), dimension(:,:,:),     pointer :: rhoE
       real(kind=realType), dimension(:,:,:),     pointer :: gamma
       real(kind=realType), dimension(:,:,:),     pointer :: muLam
       real(kind=realType), dimension(:,:,:),     pointer :: muTurb

       real(kind=realType), dimension(:,:,:,:),   pointer :: b0

       real(kind=realType), dimension(:,:,:,:), pointer :: fluxXi
       real(kind=realType), dimension(:,:,:,:), pointer :: fluxEta
       real(kind=realType), dimension(:,:,:,:), pointer :: fluxZeta

       real(kind=realType), dimension(:,:,:),   pointer :: rhoE1
       real(kind=realType), dimension(:,:,:,:), pointer :: dw
       real(kind=realType), dimension(:,:,:,:), pointer :: w1, wr

       integer(kind=intType), dimension(:,:), pointer :: mgIFine
       integer(kind=intType), dimension(:,:), pointer :: mgJFine
       integer(kind=intType), dimension(:,:), pointer :: mgKFine

       real(kind=realType), dimension(:,:), pointer :: mgIWeightFineSol
       real(kind=realType), dimension(:,:), pointer :: mgJWeightFineSol
       real(kind=realType), dimension(:,:), pointer :: mgKWeightFineSol

       real(kind=realType), dimension(:,:), pointer :: mgIWeightFineRes
       real(kind=realType), dimension(:,:), pointer :: mgJWeightFineRes
       real(kind=realType), dimension(:,:), pointer :: mgKWeightFineRes

       integer(kind=intType), dimension(:,:), pointer :: mgICoarse
       integer(kind=intType), dimension(:,:), pointer :: mgJCoarse
       integer(kind=intType), dimension(:,:), pointer :: mgKCoarse

       real(kind=realType), dimension(:,:,:,:), pointer :: wn
       real(kind=realType), dimension(:,:,:),   pointer :: rhoEn
       real(kind=realType), dimension(:,:,:),   pointer :: dt

       real(kind=realType), dimension(:,:,:), pointer :: d2Wall

       real(kind=realType), dimension(:,:,:,:), pointer :: bmti1
       real(kind=realType), dimension(:,:,:,:), pointer :: bmti2
       real(kind=realType), dimension(:,:,:,:), pointer :: bmtj1
       real(kind=realType), dimension(:,:,:,:), pointer :: bmtj2
       real(kind=realType), dimension(:,:,:,:), pointer :: bmtk1
       real(kind=realType), dimension(:,:,:,:), pointer :: bmtk2

       real(kind=realType), dimension(:,:,:), pointer :: bvti1, bvti2
       real(kind=realType), dimension(:,:,:), pointer :: bvtj1, bvtj2
       real(kind=realType), dimension(:,:,:), pointer :: bvtk1, bvtk2

       integer(kind=intType), dimension(:,:,:), pointer :: globalNode

       end module blockPointers
