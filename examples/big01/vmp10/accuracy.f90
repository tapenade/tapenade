
!
!      ******************************************************************
!      *                                                                *
!      * File:          accuracy.f90                                    *
!      * Author:        Edwin van der Weide                             *
!      * Starting date: 02-16-2006                                      *
!      * Last modified: 02-16-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module accuracy
!
!      ******************************************************************
!      *                                                                *
!      * Definition of some parameters which make the code more         *
!      * readable. The actual values of this parameters are arbitrary;  *
!      * in the code always the symbolic names are (should be) used.    *
!      *                                                                *
!      ******************************************************************
!
       use precision
       implicit none
       save
!
       integer(kind=intType), parameter :: firstOrder   = 1_intType, &
                                           secondOrder  = 2_intType, &
                                           thirdOrder   = 3_intType, &
                                           fourthOrder  = 4_intType, &
                                           fifthOrder   = 5_intType, &
                                           sixthOrder   = 6_intType, &
                                           seventhOrder = 7_intType, &
                                           eighthOrder  = 8_intType
       end module accuracy
