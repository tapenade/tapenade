
!
!     ******************************************************************
!     *                                                                *
!     * File:          inviscidFluxAdj.f90                             *
!     * Author:        Edwin van der Weide, Andre C. Marta             *
!     * Starting date: 03-24-2003                                      *
!     * Last modified: 08-01-2006                                      *
!     *                                                                *
!     ******************************************************************
!
      subroutine inviscidFluxAdj(wAdj, rhoEAdj, fluxXiAdj, fluxEtaAdj, &
                                 fluxZetaAdj, iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * Adjoint suited routine based on the inviscidFlux.f90 file but  *
!     * computing the fluxes for a computational stencil centered at   *
!     * node (i,j,k).                                                  *
!     *                                                                *
!     * inviscidFlux computes the Euler fluxes in the three directions *
!     * for all time instances of block nn. It is assumed that the     *
!     * variables defining dimensions of the block, defined in         *
!     * blockPointers, already contain the correct values.             *
!     * This routine also serves as an initialization for the fluxes,  *
!     * i.e. it is assumed that this routine is called first when      *
!     * computing the fluxes in the nodes.                             *
!     *                                                                *
!     * This routine is based on /solver/inviscidFlux.f90.             *
!     *                                                                *
!     ******************************************************************
!
      use blockPointers
      use flowVarRefState ! nw
      use iteration
      implicit none
!
!     Subroutine arguments
!
      real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                   intent(in) :: wAdj
      real(kind=realType), dimension(-2:2,-2:2,-2:2), &
                                                   intent(in) :: rhoEAdj
      real(kind=realType), dimension(-1:1,nw), &
                       intent(out) :: fluxXiAdj, fluxEtaAdj, fluxZetaAdj

      integer(kind=intType), intent(in) :: iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: ii, jj, kk
!
!     ******************************************************************
!     *                                                                *
!     * Inviscid flux computation in the three directions.             *
!     *                                                                *
!     ******************************************************************
!
      ! Flux Xi in the i-direction.

      jj=0; kk=0
      do ii=-1,1

        ! Check if I'm responsible for the boundary treatment on
        ! the iMin or iMax side.

        if( iMinBoundaryStencil .and. iNode==1 .and. ii==-1) then
          fluxXiAdj(ii,:) = zero
        elseif( iMaxBoundaryStencil .and. iNode==il .and. ii==1) then
          fluxXiAdj(ii,:) = zero
        else
          call computeInvFluxes(dxi,   fluxXiAdj, ii)
        endif

      enddo

      ! Flux Eta in the j-direction.

      ii=0; kk=0
      do jj=-1,1

        ! Check if I'm responsible for the boundary treatment on
        ! the jMin or jMax side.

        if( jMinBoundaryStencil .and. jNode==1 .and. jj==-1) then
          fluxEtaAdj(jj,:) = zero
        elseif( jMaxBoundaryStencil .and. jNode==jl .and. jj==1) then
          fluxEtaAdj(jj,:) = zero
        else
          call computeInvFluxes(deta,  fluxEtaAdj, jj)
        endif

      enddo

      ! Flux Zeta in the k-direction.

      ii=0; jj=0
      do kk=-1,1

        ! Check if I'm responsible for the boundary treatment on
        ! the kMin or kMax side.

        if( kMinBoundaryStencil .and. kNode==1 .and. kk==-1) then
          fluxZetaAdj(kk,:) = zero
        elseif( kMaxBoundaryStencil .and. kNode==kl .and. kk==1) then
          fluxZetaAdj(kk,:) = zero
        else
          call computeInvFluxes(dzeta, fluxZetaAdj, kk)
        endif

      enddo

      !=================================================================

      contains

        !===============================================================

        subroutine computeInvFluxes(dksi, flux, idir)
!
!       ****************************************************************
!       *                                                              *
!       * Internal subroutine, which computes the inviscid fluxes in   *
!       * owned nodes for the curvilinear coordinate mapping.          *
!       *                                                              *
!       ****************************************************************
!
        implicit none
!
!       Subroutine arguments.
!
        real(kind=realType), dimension(:,:,:,:), pointer :: dksi
        real(kind=realType), dimension(-1:1,nw), intent(inout) :: flux
        integer(kind=intType) :: idir
!
!       Local variables.
!
        integer(kind=intType) :: i, j, k

        real(kind=realType) :: uCon, vCon, dksix, dksiy, dksiz
        real(kind=realType) :: rhoFlux
!
!       ****************************************************************
!       *                                                              *
!       * Begin execution                                              *
!       *                                                              *
!       ****************************************************************
!
        ! Check whether or not the grid velocities should be added.

        testAddGridVel: if( addGridVelocities ) then

          ! Grid velocities must be added.
          ! Compute the contra-variant velocity components, both with
          ! and without the grid velocities, and the fluxes themselves.

          k = kk + kNode
          j = jj + jNode
          i = ii + iNode

          dksix = dksi(i,j,k,1)*detJinv(i,j,k)
          dksiy = dksi(i,j,k,2)*detJinv(i,j,k)
          dksiz = dksi(i,j,k,3)*detJinv(i,j,k)

          uCon = (wAdj(ii,jj,kk,ivx) - s(i,j,k,1))*dksix &
               + (wAdj(ii,jj,kk,ivy) - s(i,j,k,2))*dksiy &
               + (wAdj(ii,jj,kk,ivz) - s(i,j,k,3))*dksiz
          vCon = wAdj(ii,jj,kk,ivx)*dksix &
               + wAdj(ii,jj,kk,ivy)*dksiy &
               + wAdj(ii,jj,kk,ivz)*dksiz

          rhoFlux = wAdj(ii,jj,kk,irho)*uCon

          flux(idir,irho)  = rhoFlux
          flux(idir,imx)   = wAdj(ii,jj,kk,ivx)*rhoFlux + wAdj(ii,jj,kk,ip)*dksix
          flux(idir,imy)   = wAdj(ii,jj,kk,ivy)*rhoFlux + wAdj(ii,jj,kk,ip)*dksiy
          flux(idir,imz)   = wAdj(ii,jj,kk,ivz)*rhoFlux + wAdj(ii,jj,kk,ip)*dksiz
          flux(idir,irhoE) = rhoEAdj(ii,jj,kk)*uCon + wAdj(ii,jj,kk,ip)*vCon

        else testAddGridVel

          ! Grid velocities should not be added.
          ! Compute the contra-variant velocity components and
          ! the fluxes themselves.

          k = kk + kNode
          j = jj + jNode
          i = ii + iNode

          dksix = dksi(i,j,k,1)*detJinv(i,j,k)
          dksiy = dksi(i,j,k,2)*detJinv(i,j,k)
          dksiz = dksi(i,j,k,3)*detJinv(i,j,k)

          uCon = wAdj(ii,jj,kk,ivx)*dksix + wAdj(ii,jj,kk,ivy)*dksiy &
               + wAdj(ii,jj,kk,ivz)*dksiz

          rhoFlux = wAdj(ii,jj,kk,irho)*uCon

          flux(idir,irho)  = rhoFlux
          flux(idir,imx)   = wAdj(ii,jj,kk,ivx)*rhoFlux + wAdj(ii,jj,kk,ip)*dksix
          flux(idir,imy)   = wAdj(ii,jj,kk,ivy)*rhoFlux + wAdj(ii,jj,kk,ip)*dksiy
          flux(idir,imz)   = wAdj(ii,jj,kk,ivz)*rhoFlux + wAdj(ii,jj,kk,ip)*dksiz
          flux(idir,irhoE) = (rhoEAdj(ii,jj,kk) + wAdj(ii,jj,kk,ip))*uCon

        endif testAddGridVel

        end subroutine computeInvFluxes

      end subroutine inviscidFluxAdj
