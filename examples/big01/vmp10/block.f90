
!
!      ******************************************************************
!      *                                                                *
!      * File:          block.f90                                       *
!      * Author:        Edwin van der Weide, Steve Repsher,             *
!      *                Seonghyeon Hahn, Andre C. Marta                 *
!      * Starting date: 12-19-2002                                      *
!      * Last modified: 07-25-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module block
!
!      ******************************************************************
!      *                                                                *
!      * This module contains the definition of the derived data type   *
!      * for block, which is the basic building block for this code.    *
!      *                                                                *
!      * Apart from the derived data type for block, this module also   *
!      * contains the actual array for storing the blocks and the       *
!      * number of blocks stored on this processor.                     *
!      *                                                                *
!      ******************************************************************
!
       use constants
       implicit none
       save

       ! Parameters used for subsonic inlet bc treatment.

       integer(kind=intType), parameter :: noSubInlet      = 0_intType
       integer(kind=intType), parameter :: totalConditions = 1_intType
       integer(kind=intType), parameter :: massFlow        = 2_intType

       ! Parameters which define the curvilinear directions.
       ! Note that xi == i-direction, eta = j-direction and
       ! zeta = k-direction.

       integer(kind=intType), parameter :: noDir   = 0_intType
       integer(kind=intType), parameter :: xiDir   = 1_intType
       integer(kind=intType), parameter :: etaDir  = 2_intType
       integer(kind=intType), parameter :: zetaDir = 3_intType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type BCDataType, which      *
!      * stores the prescribed data of boundary faces. For all the      *
!      * arrays the first two dimensions equal the dimensions of the    *
!      * subface, possibly extended with one halo node.                 *
!      *                                                                *
!      ******************************************************************
!
       type BCDataType

         ! iBeg, iEnd: Node range in the first direction of the subface.
         ! jBeg, jEnd: Idem in the second direction.

         integer(kind=intType) :: iBeg, iEnd, jBeg, jEnd

         ! norm(:,:,3): The outward pointing nodal normal multiplied by
         !              the integration coefficient for that node.

         real(kind=realType), dimension(:,:,:), pointer :: norm

         ! subsonicInletTreatment: which boundary condition treatment
         !                         to use for subsonic inlets; either
         !                         totalConditions or massFlow.

         integer(kind=intType) :: subsonicInletTreatment

         ! uSlip(:,:,3):  the 3 components of the velocity vector on
         !                a viscous wall. Note that this value can
         !                be different from the mesh velocity.
         ! TNS_Wall(:,:): Wall temperature for isothermal walls.

         real(kind=realType), dimension(:,:,:), pointer :: uSlip
         real(kind=realType), dimension(:,:),   pointer :: TNS_Wall

         ! ptInlet(:,:):       Total pressure at subsonic inlets.
         ! ttInlet(:,:):       Total temperature at subsonic inlets.
         ! htInlet(:,:):       Total enthalpy at subsonic inlets.
         ! flowXDirInlet(:,:): X-direction of the flow for subsonic
         !                     inlets.
         ! flowYDirInlet(:,:): Idem in y-direction.
         ! flowZDirInlet(:,:): Idem in z-direction.

         real(kind=realType), dimension(:,:), pointer :: ptInlet
         real(kind=realType), dimension(:,:), pointer :: ttInlet
         real(kind=realType), dimension(:,:), pointer :: htInlet
         real(kind=realType), dimension(:,:), pointer :: flowXDirInlet
         real(kind=realType), dimension(:,:), pointer :: flowYDirInlet
         real(kind=realType), dimension(:,:), pointer :: flowZDirInlet

         ! turbInlet(:,:,nt1:nt2): Turbulence variables at inlets,
         !                         either subsonic or supersonic.

         real(kind=realType), dimension(:,:,:), pointer :: turbInlet

         ! rho(:,:):  density; used for multiple bc's.
         ! velX(:,:): x-velocity; used for multiple bc's.
         ! velY(:,:): y-velocity; used for multiple bc's.
         ! velZ(:,:): z-velocity; used for multiple bc's.
         ! ps(:,:):   static pressure; used for multiple bc's.

         real(kind=realType), dimension(:,:), pointer :: rho
         real(kind=realType), dimension(:,:), pointer :: velX
         real(kind=realType), dimension(:,:), pointer :: velY
         real(kind=realType), dimension(:,:), pointer :: velZ
         real(kind=realType), dimension(:,:), pointer :: ps

         ! rhoE(:,:):  Total energy; used for multiple bc's.
         ! gamma(:,:): Specific heat ratio; used for multiple bc's.

         real(kind=realType), dimension(:,:), pointer :: rhoE
         real(kind=realType), dimension(:,:), pointer :: gamma

       end type BCDataType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type penaltyDataType, which *
!      * stores the data for the interfaces for which the penalty       *
!      * approach should be used.                                       *
!      *                                                                *
!      ******************************************************************
!
       type penaltyDataType

         ! iBeg,  jBeg:  Starting indices of the owned nodes of the
         !               subface.
         ! iEnd,  jEnd:  Ending indices of the nodes of the subface.
         ! iBegD, jBegD: Starting indices of the nodes of the
         !               subface, including donor nodes possibly needed
         !               for interpolation (sliding mesh and 
         !               non-matching block boundaries).

         integer(kind=intType) :: iBeg,  jBeg, iEnd, jEnd
         integer(kind=intType) :: iBegD, jBegD

         ! internalPenalty:    Whether or not this is an internal
         !                     boundary, i.e. no physical boundary, that
         !                     must be treated with the penalty
         !                     technique.
         ! slidingInterface:   Whether or not this is a subface that
         !                     belongs to a sliding mesh interface.
         ! rotationalPeriodic: Whether or not this is a rotational
         !                     periodic subface.

         logical :: internalPenalty
         logical :: slidingInterface
         logical :: rotationalPeriodic

         ! nRotations:            Number of rotations needed by the donor
         !                        in order to cover the entire interface of
         !                        the receiver. This is typically the case
         !                        when blade rows of different pitches are
         !                        encountered. This value can only
         !                        be larger than 1 for sliding mesh
         !                        interfaces using Harmonic Balance.
         ! rotations(nRotations): The corresponding number of rotations
         !                        over the blade passage angle. These
         !                        numbers are positive when the rotation
         !                        takes places in the direction of the
         !                        rotation axis of the corresponding
         !                        section and negative otherwise.
         ! time:                  The time for which the donor info must
         !                        be constructed at this time instance.
         !                        This is identical for all rotations.

         integer(kind=intType) :: nRotations
         real(kind=realType)   :: time

         integer(kind=intType), dimension(:), pointer :: rotations

         ! w(iBeg:iEnd,jBeg:jEnd,nw):           Prescribed penalty data.
         !                                      The primitive flow
         !                                      variables (rho,u,v,w,p)
         !                                      as well as the primitive
         !                                      turbulent variables are
         !                                      stored.
         ! dw(iBeg:iEnd,jBeg:jEnd,2:nw):        Prescribed viscous fluxes
         !                                      for a viscous simulation.
         ! rhoE(iBeg:iEnd,jBeg:jEnd):           Idem for the total energy.
         ! gamma(iBeg:iEnd,jBeg:jEnd):          Idem for the specific heat
         !                                      ratio.
         ! wBuf(:,iBegD:iEnd,jBegD:jEnd,nw):    Buffer used during the
         !                                      data exchange. The first
         !                                      dimension is nRotations.
         ! dwBuf(:,iBegD:iEnd,jBegD:jEnd,2:nw): Idem for dw.
         ! rhoEBuf(:,iBegD:iEnd,jBegD:jEnd):    Idem for rhoE.
         ! gammaBuf(:,iBegD:iEnd,jBegD:jEnd):   Idem for gamma.
         ! rotMat(iBegD:iEnd,jBegD:jEnd,3,3):   Rotation matrix to
         !                                      transform the Cartesian
         !                                      velocity components to
         !                                      cylindrical ones. The
         !                                      interpolation for sliding
         !                                      meshes and rotational
         !                                      periodic boundaries (or
         !                                      just an exchange for the
         !                                      latter) is a lot easier
         !                                      in cylindrical velocity
         !                                      components.

         real(kind=realType), dimension(:,:),     pointer :: rhoE, gamma
         real(kind=realType), dimension(:,:,:),   pointer :: rhoEBuf
         real(kind=realType), dimension(:,:,:),   pointer :: gammaBuf
         real(kind=realType), dimension(:,:,:),   pointer :: w, dw
         real(kind=realType), dimension(:,:,:,:), pointer :: wBuf, dwBuf
         real(kind=realType), dimension(:,:,:,:), pointer :: rotMat

         ! wBufAlloc(iBegD:iEnd,jBegD:jEnd,nw): Helper array for wBuf.
         ! dwBufAlloc(iBegD:iEnd,jBegD:jEnd,2:nw): Idem for dwBuf.
         ! rhoEBufAlloc(iBegD:iEnd,jBegD:jEnd): Idem for rhoE.
         ! gammaBufAlloc(iBegD:iEnd,jBegD:jEnd): Idem for gamma.

         real(kind=realType), dimension(:,:),   pointer :: rhoEBufAlloc
         real(kind=realType), dimension(:,:),   pointer :: gammaBufAlloc
         real(kind=realType), dimension(:,:,:), pointer :: wBufAlloc
         real(kind=realType), dimension(:,:,:), pointer :: dwBufAlloc

         ! Global node numbering used by the adjoint solver.
         !
         ! globalNode(iBeg:iEnd,jBeg:jEnd):     Prescribed global node
         !                                      number for the penalty
         !                                      terms connectivity.
         ! globalNodeBuf(:,iBegD:iEnd,jBegD:jEnd):Buffer used during the
         !                                      data exchange. The first
         !                                      dimension is nRotations.

         integer(kind=intType), dimension(:,:),   pointer :: globalNode
         integer(kind=intType), dimension(:,:,:), pointer :: globalNodeBuf

       end type penaltyDataType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type nonMatchNeighType,     *
!      * which stores the information of the possible neighbors of a    *
!      * non-matching abutting subface.                                 *
!      *                                                                *
!      ******************************************************************
!
       type nonMatchNeighType

         ! nDonorBlocks:    Number of possible donor blocks for this
         !                  subface. It is possible that the subface
         !                  abuts multiple donor blocks.
         ! donorBlocks(:):  Local block ID's of the blocks interfacing
         !                  with the current subface.
         !                  Dimension [nDonorBlocks].
         ! donorProcs(:):   Processor ID's where these blocks are
         !                  stored. Dimension [nDonorBlocks].
         ! donorFaceIDs(:): Block face IDs of the donor blocks, which
         !                  abut this subface. Dimension [nDonorBlocks].

         integer(kind=intType) :: nDonorBlocks

         integer(kind=intType), dimension(:), pointer :: donorBlocks
         integer(kind=intType), dimension(:), pointer :: donorProcs
         integer(kind=intType), dimension(:), pointer :: donorFaceIDs

       end type nonMatchNeighType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type block_type, which      *
!      * stores dimensions, coordinates, solution, etc.                 *
!      *                                                                *
!      ******************************************************************
!
       type blockType
!
!        ****************************************************************
!        *                                                              *
!        * Block dimensions and orientation.                            *
!        *                                                              *
!        ****************************************************************
!
         ! il, jl, kl:    Block integer dimensions for no halo node based
         !                quantities.
         ! ib, jb, kb:    Node start indices for the storage of variables.
         !                These are less than 1 if halo's are stored in
         !                the corresponding direction. Otherwise the
         !                values are 1.
         ! ie, je, ke:    Node end indices for the storage of variables.
         !                These are larger than il, jl, kl if halo's are
         !                stored in the corresponding direction.
         !                Otherwise the values are il, jl and kl
         !                respectively.
         ! isRightHanded: Whether or not the block is right handed.

         integer(kind=intType) :: il, jl, kl, ib, jb, kb, ie, je, ke

         logical :: isRightHanded
!
!        ****************************************************************
!        *                                                              *
!        * Accuracy of the discretization in the three directions.      *
!        *                                                              *
!        ****************************************************************
!
         ! orderFlowXi:   Accuracy of the discretization of the flow
         !                equations in xi-direction  (== i-direction).
         ! orderFlowEta:  Idem in eta-direction (== j-direction).
         ! orderFlowZeta: Idem in zeta-direction (== k-direction).
         ! orderTurbXi:   Idem for the turbulence equations.
         ! orderTurbEta:  Idem for the turbulence equations.
         ! orderTurbZeta: Idem for the turbulence equations.

         integer(kind=intType) :: orderFlowXi,   orderTurbXi
         integer(kind=intType) :: orderFlowEta,  orderTurbEta
         integer(kind=intType) :: orderFlowZeta, orderTurbZeta
!
!        ****************************************************************
!        *                                                              *
!        * Logicals for the discretization of points near boundaries.   *
!        *                                                              *
!        ****************************************************************
!
         ! iMinBoundaryStencil: Whether or not I'm responsible for the
         !                      boundary stencil on the iMin side.
         ! iMaxBoundaryStencil: Idem on the iMax side.
         ! jMinBoundaryStencil: Idem on the jMin side.
         ! jMaxBoundaryStencil: Idem on the jMax side.
         ! kMinBoundaryStencil: Idem on the kMin side.
         ! kMaxBoundaryStencil: Idem on the kMax side.

         logical :: iMinBoundaryStencil, iMaxBoundaryStencil
         logical :: jMinBoundaryStencil, jMaxBoundaryStencil
         logical :: kMinBoundaryStencil, kMaxBoundaryStencil
!
!        ****************************************************************
!        *                                                              *
!        * Block boundary conditions.                                   *
!        *                                                              *
!        ****************************************************************
!
         ! nSubface:            Total number of subfaces on this block.
         ! nViscBocos:          Number of viscous wall boundary subfaces.
         !                      These are numbered first.
         ! nInvBocos:           Number of inviscid wall boundary
         !                      subfaces. These follow the viscous ones.
         ! nBocos:              Total number of physical boundary
         !                      subfaces.
         ! n1to1Internal:       Number of 1 to 1 block boundaries, which
         !                      occured due to block splitting.
         ! n1to1:               Number of 1 to 1 block boundaries that
         !                      are present in the original grid.
         ! nNonMatch:           Number of non-matching block boundaries.
         ! BCType(:):           Boundary condition or block boundary type
         !                      for each subface. See the module BCTypes
         !                      for the possibilities.
         ! BCFaceID(:):         Block face location of each subface.
         !                      Possible values are: iMin, iMax, jMin, 
         !                      jMax, kMin, kMax.
         ! singularDirFace(:):  The direction for which the transformation
         !                      to curvilinear coordinates is singular.
         !                      Only needed for faces which collapse onto
         !                      a line. For faces which collapse onto a
         !                      point both directions of the subface are
         !                      singular.
         ! cgnsSubface(:):      The subface in the corresponding cgns
         !                      block. As cgns distinguishes between
         !                      boundary and internal boundaries, the
         !                      BCType of the subface is needed to
         !                      know which one to take. A negative number
         !                      can occur on the coarser grid levels for
         !                      non-matching block boundaries. This
         !                      indicates that this subface was a 1 to 1
         !                      matching subface on the fine grid, but
         !                      could not be maintained on the coarser
         !                      grid levels.
         ! subfaceFine(:):      The ID of this subface on a 1 level
         !                      finer grid. Due to block splitting it
         !                      is possible that some subfaces disappear
         !                      and consequently there is no 1 to 1
         !                      correspondence anymore. Not allocated
         !                      on the finest grid.
         ! subfaceCoarse(:):    The ID of this subface on a 1 level
         !                      coarser grid. Due to block splitting it
         !                      is possible that some subfaces disappear
         !                      and consequently there is no 1 to 1
         !                      correspondence anymore. Not allocated
         !                      on the coarsest grid.
         ! iBeg(:), iEnd(:):    Lower and upper limits for the nodes
         ! jBeg(:), jEnd(:):    in each of the index directions on a
         ! kBeg(:), kEnd(:):    given subface. Note that one of these
         !                      indices does not change since we will
         !                      be moving on a block face.
         ! diBeg(:), diEnd(:):  Lower and upper limits for the nodes in
         ! djBeg(:), djEnd(:):  each of the index directions of the
         ! dkBeg(:), dkEnd(:):  donor subface for this particular
         !                      subface. Note that one of these indices
         !                      does not change since we will be moving
         !                      on a face.
         ! neighBlock(:)        Local block number to which this subface
         !                      connects. Not for physical boundaries.
         ! neighProc(:)         Processor number where the neighbor block
         !                      is stored. Not for physical boundaries.
         ! l1(:), l2(:), l3(:): Short hand for the transformation
         !                      matrix between this subface and the
         !                      neighbor block. Only for 1 to 1 subfaces.
         ! groupNum(:)          Group number to which this subface
         !                      belongs. Only for physical boundaries.
         !                      If this subface does not belong to any
         !                      group, the corresponding entry in this
         !                      array is zeroed out. If the subface
         !                      belongs to a sliding mesh interface the
         !                      absolute value of groupNum contains the
         !                      ID of the sliding mesh interface. One
         !                      side of the interface gets a positive
         !                      number, the other side a negative one.
         ! nonMatchNeigh(:):    Neighboring information for the
         !                      non-matching abutting subfaces.

         integer(kind=intType) :: nSubface, nBocos
         integer(kind=intType) :: nViscBocos, nInvBocos
         integer(kind=intType) :: n1to1Internal, n1to1, nNonMatch

         integer(kind=intType), dimension(:), pointer :: BCType
         integer(kind=intType), dimension(:), pointer :: BCFaceID
         integer(kind=intType), dimension(:), pointer :: singularDirFace
         integer(kind=intType), dimension(:), pointer :: cgnsSubface
         integer(kind=intType), dimension(:), pointer :: subfaceFine
         integer(kind=intType), dimension(:), pointer :: subfaceCoarse

         integer(kind=intType), dimension(:), pointer :: iBeg, iEnd
         integer(kind=intType), dimension(:), pointer :: jBeg, jEnd
         integer(kind=intType), dimension(:), pointer :: kBeg, kEnd

         integer(kind=intType), dimension(:), pointer :: diBeg, diEnd
         integer(kind=intType), dimension(:), pointer :: djBeg, djEnd
         integer(kind=intType), dimension(:), pointer :: dkBeg, dkEnd

         integer(kind=intType), dimension(:), pointer :: neighBlock
         integer(kind=intType), dimension(:), pointer :: neighProc
         integer(kind=intType), dimension(:), pointer :: l1, l2, l3
         integer(kind=intType), dimension(:), pointer :: groupNum

         type(nonMatchNeighType), dimension(:), pointer :: nonMatchNeigh
!
!        ****************************************************************
!        *                                                              *
!        * Overset boundary (fringe) cells and blanked cells.           *
!        *                                                              *
!        ****************************************************************
!
         !  iblank(ib:ie,jb:je,kb:ke):  Stores an integer for every node
         !                              of this block, including halos.
         !                              The following convention is used:
         !                              + field = 1
         !                              + hole = 0
         !                              + fringe >= 9 preprocessing
         !                                        = 0 solver
         !                              + oversetOuterBound boco = -1
         !  nHoles:                     Number of owned hole nodes.
         !  nNodesOverset:              Number of owned overset nodes
         !                              with donors.
         !  nNodesOversetAll:           Total number of overset nodes
         !                              including fringe from 1-to-1
         !                              halos and orphans.
         !  nOrphans:                   Number of orphans (boundary
         !                              nodes without donors).
         !  ibndry(3,..):               Indices for each overset node.
         !  idonor(3,..):               Donor indices for each overset
         !                              node.
         !  overint(3,..):              Interpolants for the donor
         !                              stencil.
         !  neighBlockOver(..):         Local block number to which donor
         !                              node belongs.
         !  neighProcOver(..):          Processor ID where the neighbor
         !                              block is stored.

         integer(kind=intType) :: nNodesOverset, nNodesOversetAll
         integer(kind=intType) :: nHoles, nOrphans

         integer(kind=intType), dimension(:,:,:), pointer :: iblank

         integer(kind=intType), dimension(:,:), pointer :: ibndry
         integer(kind=intType), dimension(:,:), pointer :: idonor
         real(kind=realType),   dimension(:,:), pointer :: overint

         integer(kind=intType), dimension(:), pointer :: neighBlockOver
         integer(kind=intType), dimension(:), pointer :: neighProcOver
!
!        ****************************************************************
!        *                                                              *
!        * Boundary data for the boundary subfaces.                     *
!        *                                                              *
!        ****************************************************************
!
         ! BCData(nBocos): The boundary data for each of the boundary
         !                 subfaces.

         type(BCDataType), dimension(:), pointer :: BCData
!
!        ****************************************************************
!        *                                                              *
!        * Penalty data for the subfaces.                               *
!        *                                                              *
!        ****************************************************************
!
         ! penaltyDataType(nSubface): The prescribed penalty data for
         !                            the nodes on the subfaces.

         type(penaltyDataType), dimension(:), pointer :: penaltyData
!
!        ****************************************************************
!        *                                                              *
!        * Mesh related variables.                                      *
!        *                                                              *
!        ****************************************************************
!
         ! x(ib:ie,jb:je,kb:ke,3): xyz locations of grid points in
         !                         the block, including possible halos.
         ! xOld(nOld,:,:,:,:):     Coordinates on older time levels;
         !                         only needed for unsteady problems on
         !                         deforming grids. Only allocated on
         !                         the finest grid level. The blank
         !                         dimensions are equal to the
         !                         dimensions of x.
         ! s(ib:ie,jb:je,kb:ke,3): Mesh velocities in the grid points.
         !                         Only for moving mesh parts.
         ! blockIsMoving:          Whether or not the block is moving.
         ! addGridVelocities:      Whether or not the face velocities
         !                         are allocated and set.

         real(kind=realType), dimension(:,:,:,:),   pointer :: x
         real(kind=realType), dimension(:,:,:,:,:), pointer :: xOld
         real(kind=realType), dimension(:,:,:,:),   pointer :: s

         logical :: blockIsMoving
         logical :: addGridVelocities
!
!        ****************************************************************
!        *                                                              *
!        * Metric variables.                                            *
!        *                                                              *
!        ****************************************************************
!
         ! dxiFlow(ib:ie,jl,kl,3):         Partial derivatives of xi
         !                                 w.r.t. the physical coordinates
         !                                 for the mean flow equations.
         ! detaFlow(il,jb:je,kl,3):        Idem for eta.
         ! dzetaFlow(il,jl,kb:ke,3):       Idem for zeta.
         ! detJinvFlow(ib:ie,jb:je,kb:ke): Inverse of the determinant of
         !                                 the Jacobian of the
         !                                 transformation between physical
         !                                 and curvi-linear coordinates
         !                                 for the mean flow equations.
         ! detJinvOldFlow(nOld,il,jl,kl):  Inverse of the Jacobians on the
         !                                 older time levels for the mean
         !                                 flow equations. Only needed
         !                                 for unsteady problems on
         !                                 deforming grids. Only allocated
         !                                 on the finest grid level.

         real(kind=realType), dimension(:,:,:,:), pointer :: dxiFlow
         real(kind=realType), dimension(:,:,:,:), pointer :: detaFlow
         real(kind=realType), dimension(:,:,:,:), pointer :: dzetaFlow

         real(kind=realType), dimension(:,:,:),   pointer :: detJinvFlow
         real(kind=realType), dimension(:,:,:,:), pointer :: detJinvOldFlow

         ! dxiTurb(ib:ie,jl,kl,3):         Partial derivatives of xi
         !                                 w.r.t. the physical coordinates
         !                                 for the turbulence equations.
         ! detaTurb(il,jb:je,kl,3):        Idem for eta.
         ! dzetaTurb(il,jl,kb:ke,3):       Idem for zeta.
         ! detJinvTurb(ib:ie,jb:je,kb:ke): Inverse of the determinant of
         !                                 the Jacobian of the
         !                                 transformation between physical
         !                                 and curvi-linear coordinates
         !                                 for the turbulence equations.

         real(kind=realType), dimension(:,:,:,:), pointer :: dxiTurb
         real(kind=realType), dimension(:,:,:,:), pointer :: detaTurb
         real(kind=realType), dimension(:,:,:,:), pointer :: dzetaTurb
         real(kind=realType), dimension(:,:,:),   pointer :: detJinvTurb

         ! viscMetricA(ib:ie,jl,kl,7): Metric terms which occur in
         !                             the xi-xi derivatives of the
         !                             viscous terms.
         ! viscMetricB(il,jb:je,kl,7): Idem for the eta-eta derivatives.
         ! viscMetricC(il,jl,kb:ke,7): Idem for the zeta-zeta derivatives.
         ! viscMetricD(il,jl,kl,10):   Idem for the xi-eta derivatives.
         ! viscMetricE(il,jl,kl,10):   Idem for the xi-zeta derivatives.
         ! viscMetricF(il,jl,kl,10):   Idem for the eta-zeta derivatives.

         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricA
         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricB
         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricC
         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricD
         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricE
         real(kind=realType), dimension(:,:,:,:), pointer :: viscMetricF

         ! viscMetricTurbA(ib:ie,jl,kl): Turbulent metric terms which
         !                               occur in the xi-xi derivatives
         !                               of the viscous terms.
         ! viscMetricTurbB(il,jb:je,kl): Idem for the eta-eta derivatives.
         ! viscMetricTurbC(il,jl,kb:ke): Idem for the zeta-zeta derivatives.
         ! viscMetricTurbD(il,jl,kl):    Idem for the xi-eta derivatives.
         ! viscMetricTurbE(il,jl,kl):    Idem for the xi-zeta derivatives.
         ! viscMetricTurbF(il,jl,kl):    Idem for the eta-zeta derivatives.

         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbA
         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbB
         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbC
         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbD
         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbE
         real(kind=realType), dimension(:,:,:), pointer :: viscMetricTurbF
!
!        ****************************************************************
!        *                                                              *
!        * Flow variables.                                              *
!        *                                                              *
!        ****************************************************************
!
         ! w(ib:ie,jb:je,kb:ke,1:nw): The primitive variables.
         !                            w(i,j,k,1:nwf) are the flow field
         !                            variables, rho, u, v, w and p.
         !                            w(i,j,k,nt1:nt2) turbulent 
         !                            variables; also the primitive
         !                            variables are stored.
         ! UOld(nOld,il,jl,kl,nw):    Solution on older time levels,
         !                            needed for the time integration for
         !                            unsteady problems. In contrast to
         !                            w, the conservative variables are
         !                            stored in UOld for the flow
         !                            variables; the turbulent variables
         !                            are still the primitive ones.
         !                            Only allocated on the finest mesh.
         ! rhoE(ib:ie,jb:je,kb:ke):   Total energy per unit volume.
         ! gamma(ib:ie,jb:je,kb:ke):  Specific heat ratio.
         ! muLam(ib:ie,jb:je,kb:ke):  Laminar viscosity; only allocated
         !                            for viscous problems.
         ! muTurb(ib:ie,jb:je,kb:ke): Eddy viscosity; only allocated for
         !                            RANS problems with eddy viscosity
         !                            models.

         real(kind=realType), dimension(:,:,:,:),   pointer :: w
         real(kind=realType), dimension(:,:,:,:,:), pointer :: UOld
         real(kind=realType), dimension(:,:,:),     pointer :: rhoE
         real(kind=realType), dimension(:,:,:),     pointer :: gamma
         real(kind=realType), dimension(:,:,:),     pointer :: muLam
         real(kind=realType), dimension(:,:,:),     pointer :: muTurb
!
!        ****************************************************************
!        *                                                              *
!        * Imposed magnetic field (MHD computations only).              *
!        *                                                              *
!        ****************************************************************
!
         ! b0(ib:ie,jb:je,kb:ke,3):  Imposed magnetic field b0.

         real(kind=realType), dimension(:,:,:,:),   pointer :: b0
!
!        ****************************************************************
!        *                                                              *
!        *           Fluxes in the three directions.                    *
!        *                                                              *
!        ****************************************************************
!
         ! fluxXi  (is:it,jl,kl,nw): Flux in xi-direction (i-direction)
         !                           for the nodes. is and it are
         !                           different from 1 and il only if
         !                           block splitting is used. In this
         !                           case fluxXi will have halo's such
         !                           that the proper sequential
         !                           discretization is recovered. The
         !                           number of halo's stored in fluxXi
         !                           equals the number of neighbors
         !                           needed in the stencil for the
         !                           central discretization of the 1st
         !                           derivative.
         !                           Only allocated on the finest grid.
         ! fluxEta (il,js:jt,kl,nw): Idem for the flux in eta-direction
         !                           (j-direction).
         ! fluxZeta(il,jl,ks:kt,nw): Idem for the flux in zeta-direction
         !                           (k-direction).

         real(kind=realType), dimension(:,:,:,:), pointer :: fluxXi
         real(kind=realType), dimension(:,:,:,:), pointer :: fluxEta
         real(kind=realType), dimension(:,:,:,:), pointer :: fluxZeta
!
!        ****************************************************************
!        *                                                              *
!        * Residual and multigrid variables.                            *
!        *                                                              *
!        ****************************************************************
!
         ! dw(is:it,js:jt,ks:kt,nw): Values of the combined residuals.
         !                           Only allocated on the finest mesh.
         !                           The starting indices is, js and ks
         !                           are either 0 or 1. If a block is
         !                           split on a min boundary the
         !                           corresponding starting index is 0;
         !                           otherwise it is 1.
         !                           For the ending indices it, jt and kt
         !                           it is similar.
         ! w1(il,jl,kl,nMGVar):      Values of the MG variables upon
         !                           first entry to a coarser mesh; only
         !                           allocated on the coarser grids. The
         !                           variables used to compute the
         !                           multigrid corrections are the
         !                           primitive ones. nMGVar is the number
         !                           of variables to which multigrid must
         !                           be applied, which are either the
         !                           mean flow variables or all variables.
         ! rhoE1(il,jl,kl)           Value of the total energy upon first
         !                           entry to a coarser grid. Only for
         !                           unsteady problems and only allocated
         !                           on the coarser grids.
         ! wr(il,jl,kl,nMGVar):      Multigrid forcing terms; only
         !                           allocated on the coarser grids.
         !                           The forcing term of course contains
         !                           conservative residuals, at least for
         !                           the flow variables.

         real(kind=realType), dimension(:,:,:),   pointer :: rhoE1
         real(kind=realType), dimension(:,:,:,:), pointer :: dw
         real(kind=realType), dimension(:,:,:,:), pointer :: w1, wr

         ! mgIFine(il,3): The fine grid i-nodes used for the restriction
         !                of the solution and residual to the coarse
         !                grid. Only on the coarser grids.
         ! mgJFine(jl,3): Idem for j-nodes.
         ! mgKFine(kl,3): Idem for k-nodes.

         ! mgIWeightFineSol(il,3): The corresponding interpolation
         !                         weights for the solution in
         !                         i-direction.
         ! mgJWeightFineSol(jl,3): Idem for weights in j-direction.
         ! mgKWeightFineSol(kl,3): Idem for weights in k-direction.

         ! mgIWeightFineRes(il,3): The corresponding interpolation
         !                         weights for the residual in
         !                         i-direction.
         ! mgJWeightFineRes(jl,3): Idem for weights in j-direction.
         ! mgKWeightFineRes(kl,3): Idem for weights in k-direction.

         ! mgICoarse(il,2): The two coarse grid i-nodes used for the
         !                  interpolation of the correction to the
         !                  fine grid. Not on the coarsest grid.
         ! mgJCoarse(jl,2): Idem for j-nodes.
         ! mgKCoarse(kl,2): Idem for k-nodes.

         integer(kind=intType), dimension(:,:), pointer :: mgIFine
         integer(kind=intType), dimension(:,:), pointer :: mgJFine
         integer(kind=intType), dimension(:,:), pointer :: mgKFine

         real(kind=realType), dimension(:,:), pointer :: mgIWeightFineSol
         real(kind=realType), dimension(:,:), pointer :: mgJWeightFineSol
         real(kind=realType), dimension(:,:), pointer :: mgKWeightFineSol

         real(kind=realType), dimension(:,:), pointer :: mgIWeightFineRes
         real(kind=realType), dimension(:,:), pointer :: mgJWeightFineRes
         real(kind=realType), dimension(:,:), pointer :: mgKWeightFineRes

         integer(kind=intType), dimension(:,:), pointer :: mgICoarse
         integer(kind=intType), dimension(:,:), pointer :: mgJCoarse
         integer(kind=intType), dimension(:,:), pointer :: mgKCoarse

         ! iimap(il): The mapping from coarse to fine grid nodes in
         !            i-direction. Not on the finest grid.
         ! jjmap(jl): Idem in j-direction.
         ! kkmap(kl): Idem in k-direction.

         integer(kind=intType), dimension(:), pointer :: iimap
         integer(kind=intType), dimension(:), pointer :: jjmap
         integer(kind=intType), dimension(:), pointer :: kkmap
!
!        ****************************************************************
!        *                                                              *
!        * Time-stepping variables. Only allocated on the finest grid.  *
!        *                                                              *
!        ****************************************************************
!
         ! wn(il,jl,kl,nMGVar): Values of the update (primitive)
         !                      variables at the beginning of the
         !                      RungeKutta iteration. Only allocated for
         !                      RungeKutta smoother and on the fine grid
         !                      level.
         ! rhoEn(il,jl,kl):     Idem for the total energy.
         ! dt(il,jl,kl):        Pseudo time step.

         real(kind=realType), dimension(:,:,:,:), pointer :: wn
         real(kind=realType), dimension(:,:,:),   pointer :: rhoEn
         real(kind=realType), dimension(:,:,:),   pointer :: dt
!
!        ****************************************************************
!        *                                                              *
!        * Turbulence model variables.                                  *
!        *                                                              *
!        ****************************************************************
!
         ! d2Wall(il,jl,kl): Distance to the nearest viscous wall.

         real(kind=realType), dimension(:,:,:), pointer :: d2Wall

         ! bmti1(jl,kl,nt1:nt2,nt1:nt2): Matrix used for the implicit
         !                               boundary condition treatment of
         !                               the turbulence equations at the
         !                               iMin boundary. Only allocated on
         !                               the finest level and for the 1st
         !                               spectral solution.
         ! bmti2(jl,kl,nt1:nt2,nt1:nt2): Idem for the iMax boundary.
         ! bmtj1(il,kl,nt1:nt2,nt1:nt2): Idem for the jMin boundary.
         ! bmtj2(il,kl,nt1:nt2,nt1:nt2): Idem for the jMax boundary.
         ! bmtk1(il,jl,nt1:nt2,nt1:nt2): Idem for the kMin boundary.
         ! bmtk2(il,jl,nt1:nt2,nt1:nt2): Idem for the kMax boundary.

         real(kind=realType), dimension(:,:,:,:), pointer :: bmti1
         real(kind=realType), dimension(:,:,:,:), pointer :: bmti2
         real(kind=realType), dimension(:,:,:,:), pointer :: bmtj1
         real(kind=realType), dimension(:,:,:,:), pointer :: bmtj2
         real(kind=realType), dimension(:,:,:,:), pointer :: bmtk1
         real(kind=realType), dimension(:,:,:,:), pointer :: bmtk2

         ! bvti1(jl,kl,nt1:nt2): RHS vector used for the implicit
         !                       boundary condition treatment of the
         !                       turbulence equations at the iMin
         !                       boundary. Only allocated on the finest
         !                       level and for the 1st spectral solution.
         ! bvti2(jl,kl,nt1:nt2): Idem for the iMax boundary.
         ! bvtj1(il,kl,nt1:nt2): Idem for the jMin boundary.
         ! bvtj2(il,kl,nt1:nt2): Idem for the jMax boundary.
         ! bvti2(jl,kl,nt1:nt2): Idem for the iMax boundary.
         ! bvtk1(il,kl,nt1:nt2): Idem for the kMin boundary.
         ! bvtk2(il,kl,nt1:nt2): idem for the kMax boundary.

         real(kind=realType), dimension(:,:,:), pointer :: bvti1, bvti2
         real(kind=realType), dimension(:,:,:), pointer :: bvtj1, bvtj2
         real(kind=realType), dimension(:,:,:), pointer :: bvtk1, bvtk2
!
!        ****************************************************************
!        *                                                              *
!        * Relation to the original cgns grid.                          *
!        *                                                              *
!        ****************************************************************
!
         ! sectionID:      The section of the grid this block belongs to.
         ! cgnsBlockID:    Block/zone number of the cgns grid to which
         !                 this block is related.
         ! iBegOr, iEndOr: Range of points of this block in the
         ! jBegOr, jEndOr: corresponding cgns block, i.e. for this block
         ! kBegOr, kEndOr: iBegOr <= i <= iEndOr, jBegOr <= j <= jEndOr, 
         !                 kBegOr <= k <= kEndOr.
         !                 It is of course possible that the entire
         !                 block is stored.

         integer(kind=intType) :: cgnsBlockID, sectionID
         integer(kind=intType) :: iBegOr, iEndOr, jBegOr, jEndOr
         integer(kind=intType) :: kBegOr, kEndOr
!
!        ****************************************************************
!        *                                                              *
!        * Global node numbering used by the adjoint solver.            *
!        *                                                              *
!        ****************************************************************
!
         ! globalNode(ib:ie,jb:je,kb:ke):   Global node numbering.

         integer(kind=intType), dimension(:,:,:), pointer :: globalNode

       end type blockType
!
!      ******************************************************************
!      *                                                                *
!      * Array of all blocks at all multigrid levels and spectral sols. *
!      *                                                                *
!      ******************************************************************
!
       ! nDom:            number of local computational blocks.
       ! flowDoms(:,:,:): array of blocks. Dimensions are
       !                  (nDom,nLevels,nTimeInstancesMax)

       integer(kind=intType) :: nDom

!tapenade!       type(blockType), allocatable, dimension(:,:,:) :: flowDoms
!
!      ******************************************************************
!      *                                                                *
!      * Additional info needed in the flow solver.                     *
!      *                                                                *
!      ******************************************************************
!
       ! nNodeGlobal(:,:): Global number of nodes on every MG level
       !                   for all the time instances. The 1st dimension
       !                   is the number of multigrid levels, the
       !                   second the maximum number of time instances.

       integer(kind=intType), allocatable, dimension(:,:) :: nNodeGlobal

       end module block
