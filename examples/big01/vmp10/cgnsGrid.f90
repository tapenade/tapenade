
!
!      ******************************************************************
!      *                                                                *
!      * File:          cgnsGrid.f90                                    *
!      * Author:        Edwin van der Weide, Steve Repsher,             *
!      *                Seonghyeon Hahn                                 *
!      * Starting date: 12-17-2002                                      *
!      * Last modified: 02-13-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module cgnsGrid
!
!      ******************************************************************
!      *                                                                *
!      * This module contains the derived data type for storing the     *
!      * information of the original cgns grid file. Information stored *
!      * is number of blocks, block sizes, zone names, etc. this info   *
!      * is needed again when the solution is written to file. Remember *
!      * that the original blocks may be split to obtain a better       *
!      * load balance. Note that this info is stored on all processors. *
!      *                                                                *
!      * Apart from the derived data type for the cgns blocks, this     *
!      * module also contains the name of the base and the physical     *
!      * dimensions of the problem.                                     *
!      *                                                                *
!      ******************************************************************
!
       use constants
       implicit none
       save
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived datatype to store the actual     *
!      * data of the boundary conditions.                               *
!      *                                                                *
!      ******************************************************************
!
       type cgnsBCDataArray

         ! mass:  Mass units used for the data.
         ! len:   Length units used for the data.
         ! time:  Time units used for the data.
         ! temp:  Temperature units used for the data.
         ! angle: Angle units used for the data.

         integer :: mass, len, time, temp, angle

         ! arrayName:   The name of this array with boundary condition
         !              data.
         ! nDimensions: Number of dimensions for which the data is
         !              specified.
         ! dataDim(3):  Number of data points of every dimensions.
         !              Upper limit is 3, although for BC data the
         !              maximum is usually 2.
         ! dataArr(:):  The actual data. Assumed is that only floating
         !              point data is prescribed and not integer or
         !              character data. Note that dataArr is a 1D array
         !              even if the data is multi-dimensional.

         integer                                    :: nDimensions
         integer(kind=intType), dimension(3)        :: dataDim
         character(len=maxCGNSNameLen)              :: arrayName
         real(kind=realType), pointer, dimension(:) :: dataArr

       end type cgnsBCDataArray
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived datatype to store the prescribed *
!      * boundary data for a boundary subface.                          *
!      *                                                                *
!      ******************************************************************
!
       type cgnsBCDatasetType

         ! datasetName:      Name of the dataset.
         ! BCType:           Boundary condition type.
         ! nDirichletArrays: The number of Dirichlet arrays in the
         !                   data set.
         ! nNeumannArrays:   The number of Neumann arrays in the
         !                   data set.
         ! dirichletArrays:  The Dirichlet arrays.
         ! neumannArrays:    The Neumann arrays.

         character(len=maxCGNSNameLen) :: datasetName
         integer                       :: BCType
         integer(kind=intType)         :: nDirichletArrays
         integer(kind=intType)         :: nNeumannArrays

         type(cgnsBCDataArray), pointer, dimension(:) :: dirichletArrays
         type(cgnsBCDataArray), pointer, dimension(:) :: neumannArrays

       end type cgnsBCDatasetType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store cgns 1 to 1   *
!      * block to block, i.e. continuous grid lines across block        *
!      * boundaries, connectivities.                                    *
!      *                                                                *
!      ******************************************************************
!
       type cgns1to1ConnType

         ! connectName: Name of the interface.
         ! donorName:   Name of the zone/block interfacing with the
         !              current zone/block.
         ! donorBlock:  Zone/block ID of the zone/block interfacing
         !              with the current subface.

         character(len=maxCGNSNameLen) :: connectName
         character(len=maxCGNSNameLen) :: donorName
         integer(kind=intType)         :: donorBlock

         ! iBeg, iEnd:    Lower and upper limits for the nodes in each of
         ! jBeg, jEnd:    the index directions on the subface. Note that
         ! kBeg, kEnd:    one of these indices does not change since we
         !                will be moving on a block face.
         ! diBeg, diEnd:  Lower and upper limits for the nodes in each of
         ! djBeg, djEnd:  the index directions of the donor subface. Note
         ! dkBeg, dkEnd:  that one of these indices does not change since
         !                we will be moving on a face.
         ! l1, l2, l3:    Short hand for the transformation matrix
         !                between this subface and the neighbor block.

         integer(kind=intType) :: iBeg,  jBeg,  kBeg
         integer(kind=intType) :: iEnd,  jEnd,  kEnd
         integer(kind=intType) :: diBeg, djBeg, dkBeg
         integer(kind=intType) :: diEnd, djEnd, dkEnd
         integer(kind=intType) :: l1, l2, l3

         ! periodic:          Whether or not the subface is a periodic
         !                    boundary.
         ! rotationCenter(3): The center of rotation for a periodic
         !                    boundary.
         ! rotationAngles(3): The rotation angles for a periodic
         !                    boundary.
         ! translation(3):    The translation vector for a periodic
         !                    boundary.

         logical                           :: periodic
         real(kind=realType), dimension(3) :: rotationCenter
         real(kind=realType), dimension(3) :: rotationAngles
         real(kind=realType), dimension(3) :: translation

       end type cgns1to1ConnType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store cgns          *
!      * non-matching abutting block to block connectivities.           *
!      *                                                                *
!      ******************************************************************
!
       type cgnsNonMatchAbuttingConnType

         ! nDonorBlocks: Number of donor blocks. It is possible that the
         !               subface abuts multiple donor blocks.
         ! connectNames: Names of the interfaces.
         !               Dimension [nDonorBlocks].
         ! donorNames:   Names of the zone/block interfacing with the
         !               current subface. Dimension [nDonorBlocks].
         ! donorBlocks:  Zone/block IDs of the zones/blocks interfacing
         !               with the current subface.
         !               Dimension [nDonorBlocks].

         integer(kind=intType)                        :: nDonorBlocks
         integer(kind=intType), pointer, dimension(:) :: donorBlocks

         character(len=maxCGNSNameLen), pointer, dimension(:) :: &
                                                            connectNames
         character(len=maxCGNSNameLen), pointer, dimension(:) :: &
                                                           donorNames

         ! iBeg, iEnd:   Lower and upper limits for the nodes in each of
         ! jBeg, jEnd:   the index directions on the subface. Note that
         ! kBeg, kEnd:   one of these indices does not change since we
         !               will be moving on a block face.
         ! donorFaceIDs: Block face IDs of the donor blocks, which abut
         !               this subface. Dimension [nDonorBlocks].

         integer(kind=intType) :: iBeg, jBeg, kBeg
         integer(kind=intType) :: iEnd, jEnd, kEnd

         integer(kind=intType), pointer, dimension(:) :: donorFaceIDs

         ! periodic:          Whether or not the subface is a periodic
         !                    boundary.
         ! rotationCenter(3): The center of rotation for a periodic
         !                    boundary.
         ! rotationAngles(3): The rotation angles for a periodic
         !                    boundary.
         ! translation(3):    The translation vector for a periodic
         !                    boundary.

         logical                           :: periodic
         real(kind=realType), dimension(3) :: rotationCenter
         real(kind=realType), dimension(3) :: rotationAngles
         real(kind=realType), dimension(3) :: translation

       end type cgnsNonMatchAbuttingConnType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store cgns overset  *
!      * connectivity (i.e. overlapping grids to be handled via the     *
!      * chimera approach).                                             *
!      *                                                                *
!      ******************************************************************
!
       type cgnsOversetConnType

         ! connectName: Name of the interface.
         ! donorName:   Name of the zone/block interfacing with the
         !              current zone/block.
         ! donorBlock:  Zone/block ID of the zone/block interfacing
         !              with the current zone/block.

         character(len=maxCGNSNameLen) :: connectName
         character(len=maxCGNSNameLen) :: donorName
         integer(kind=intType)         :: donorBlock

         ! npnts:       Number of points to be interpolated (should
         !              equal the number of points in the donor list).
         ! ibndry(:,:): Indices for this block to be interpolated.
         !              [dimension(3,npnts)].
         ! idonor(:,:): Indices for donor block that provide information.
         !              [dimension(3,npnts)].
         ! interp(:,:): Interpolation weights for the donor stencil
         !              [dimension(3,npnts)].

         integer(kind=intType) :: npnts
         integer(kind=intType), pointer, dimension(:,:) :: ibndry
         integer(kind=intType), pointer, dimension(:,:) :: idonor
         real(kind=realType),   pointer, dimension(:,:) :: interp

       end type cgnsOversetConnType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store cgns overset  *
!      * holes. (these points are grouped together and ignored when     *
!      * calculating residuals).                                        *
!      *                                                                *
!      ******************************************************************
!
       type cgnsHolesType

         ! holeName:     Name of the interface.
         ! npnts:        Number of points in this hole set.
         ! indices(:,:): Indices for the hole points.
         !               [dimension(3,npnts)]

         character(len=maxCGNSNameLen) :: holeName
         integer(kind=intType)         :: npnts
         integer(kind=intType), pointer, dimension(:,:) :: indices

       end type cgnsHolesType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store cgns block    *
!      * boundary conditions.                                           *
!      *                                                                *
!      ******************************************************************
!
       type cgnsBocoType

         ! bocoName:        Name of the boundary condition.
         ! userDefinedName: Name of the CGNS user defined data node if
         !                  the CGNS boundary condition is UserDefined.
         ! BCTypeCGNS:      CGNS boundary condition type.
         ! BCType:          Internal boundary condition type.

         character(len=maxCGNSNameLen) :: bocoName
         character(len=maxCGNSNameLen) :: userDefinedName
         integer                       :: BCTypeCGNS
         integer(kind=intType)         :: BCType

         ! ptSetType:      The way the boundary condition faces are
         !                 specified; either a point range or an
         !                 individual set of points.
         ! nPnts:          Number of points in the boundary condition set
         !                 defining this boundary region. For a point
         !                 range this is 2.
         ! normalIndex:    Index vector indicating the computational
         !                 coordinate direction of the boundary condition
         !                 patch normal.
         ! normalListFlag: A flag indicating whether or not boundary
         !                 normals are defined.
         !                 normalListFlag == 0: normals are not defined.
         !                 normalListFlag == 1: normals are defined.
         ! normalDataType: Data type used for the definition of the
         !                 normals. Admissible types are realSingle and
         !                 realDouble.

         integer               :: ptSetType
         integer               :: normalIndex
         integer               :: normalListFlag
         integer               :: normalDataType
         integer(kind=intType) :: nPnts

         ! familyID:         Corresponding family number. If the face
         !                   does not belong to a family this value is 0.
         ! slidingID:        The ID of the sliding mesh interface of
         !                   which this boco is part. 0 means that this
         !                   family is not part of a sliding mesh
         !                   interface. This value can be positive and
         !                   negative in order to distinguish between the
         !                   two sides of the interface. The absolute
         !                   value is the actual ID of the interface.
         ! nDataSet:         Number of boundary condition datasets for
         !                   the current boundary condition.
         ! dataSet(:):       The actual boundary condition data sets.
         ! dataSetAllocated: Whether or not I actually allocated the
         !                   memory for dataSet. It is possible that
         !                   dataSet points to corresponding entry
         !                   of a family.

         integer(kind=intType) :: familyID
         integer(kind=intType) :: slidingID
         integer(kind=intType) :: nDataSet
         logical               :: dataSetAllocated

         type(cgnsBCDatasetType), pointer, dimension(:) :: dataSet

         ! iBeg, iEnd:   Lower and upper limits for the nodes in each of
         ! jBeg, jEnd:   the index directions on the subface. Note that
         ! kBeg, kEnd:   one of these indices does not change since we
         !               will be moving on a block face.
         ! rotRate(3):   The rotation rate of the boundary face. It is
         !               possible that this differs from the rotation
         !               rate of the corresponding block, e.g. for a
         !               casing in a turbomachinery problem.
         ! rotCenter(3): The corresponding rotation center.
         ! actualFace:   Whether or not this subface is an actual face.
         !               Some mesh generators (such as ICEM CFD hexa)
         !               include edges and points as boundary conditions.
         !               These should not be considered by the flow
         !               solver. In those cases, actual_face is .false.
         
         integer(kind=intType) :: iBeg, jBeg, kBeg
         integer(kind=intType) :: iEnd, jEnd, kEnd
         logical               :: actualFace

         real(kind=realType), dimension(3) :: rotCenter
         real(kind=realType), dimension(3) :: rotRate

       end type cgnsBocoType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store the data of a *
!      * cgns block.                                                    *
!      *                                                                *
!      ******************************************************************
!
       type cgnsBlockInfoType
!
!        ****************************************************************
!        *                                                              *
!        * Information read from the cgns file.                         *
!        *                                                              *
!        ****************************************************************
!
         ! zoneType: The type of the zone. Should be structured.
         ! zoneName: Zone name for this block.

         integer                       :: zoneType
         character(len=maxCGNSNameLen) :: zoneName

         ! nSubBlocks:      The number or subblocks into which this
         !                  CGNS block is split. Due to the possibility
         !                  of splitting the block during runtime,
         !                  multiple processors could store a part of the
         !                  block.
         ! procStored(:):   The corresponding processor ID's on which
         !                  the subblocks are stored.
         ! localBlockID(:): The local block ID's of the subblocks.
         ! iBegOr(:):       The nodal ranges of the subblocks.
         ! iEndOr(:):       Due to the fact that a nodal scheme is used
         ! jBegOr(:):       rather than a cell centered scheme, there is
         ! jEndOr(:):       no overlap in the nodal ranges of the
         ! kBegOr(:):       subblocks.
         ! kEndOr(:):

         integer                        :: nSubBlocks
         integer, dimension(:), pointer :: procStored
         integer, dimension(:), pointer :: localBlockID
         integer, dimension(:), pointer :: iBegOr, jBegOr, kBegOr
         integer, dimension(:), pointer :: iEndOr, jEndOr, kEndOr

         ! mass:               Mass units used for the grid.
         ! len:                Length units used for the grid.
         ! time:               Time units used for the grid.
         ! temp:               Temperature units used for the grid.
         ! angle:              Angle units used for the grid.
         ! gridUnitsSpecified: gridUnitsSpecified
         ! LRef:               The conversion factor to meters for
         !                     this block.

         integer :: mass, len, time, temp, angle
         logical :: gridUnitsSpecified
         real(kind=realType) :: LRef

         ! familyID:  Corresponding family number. If the block does not
         !            belong to a family this value is 0.
         ! sectionID: The section of the grid this block belongs to.
         
         integer(kind=intType) :: familyID
         integer(kind=intType) :: sectionID

         ! il, jl, kl:              Nodal block dimensions.
         ! n1to1:                   Total number of 1 to 1 block to block
         !                          connectivities, i.e. continous grid
         !                          lines, for this block. Also the number
         !                          of 1 to 1 connectivities stored in
         !                          general connectivity nodes is
         !                          incorporated in n1to1.
         ! n1to1General:            Number of 1 to 1 block to block
         !                          connectivities stored in general
         !                          connectivities.
         ! nNonMatchAbutting:       Number of non-matching abutting block
         !                          to block connectivities.
         ! nOverset:                Number of overset block to block
         !                          connectivities.
         ! nNodesOverset:           The total number of overset boundary
         !                          nodes.
         ! nHoles:                  Number of overset hole sets for
         !                          this block.
         ! conn1to1(:):             Array of 1 to 1 block to block
         !                          connectivities. Dimension [n1to1].
         ! connNonMatchAbutting(:): Array of non-matching abutting block
         !                          to block connectivities.
         !                          Dimension [nNonMatchAbutting].
         ! connOver(:):             Array of overset block to block 
         !                          connectivities. Dimension [nOverSet].
         ! hole(:):                 Array of overset hole sets.
         !                          Dimension [nHoles].

         integer(kind=intType) :: il, jl, kl
         integer(kind=intType) :: n1to1, n1to1General
         integer(kind=intType) :: nNonMatchAbutting
         integer(kind=intType) :: nOverset, nNodesOverset
         integer(kind=intType) :: nHoles

         type(cgns1to1ConnType),    pointer, dimension(:) :: conn1to1
         type(cgnsOversetConnType), pointer, dimension(:) :: connOver
         type(cgnsHolesType),       pointer, dimension(:) :: hole

         type(cgnsNonMatchAbuttingConnType), pointer, dimension(:) :: &
                                                   connNonMatchAbutting

         ! nBocos:      Number of boundary conditions for this block.
         ! bocoInfo(:): Array of boundary conditions.
         !              Dimension [nBocos].

         integer(kind=intType)                     :: nBocos
         type(cgnsBocoType), pointer, dimension(:) :: bocoInfo

         ! rotatingFrameSpecified: Whether or not a rotating frame
         !                         is specified.
         ! rotCenter(3):           The corresponding rotation center.
         ! rotRate(3):             The corresponding rotation rate.

         logical                           :: rotatingFrameSpecified
         real(kind=realType), dimension(3) :: rotCenter
         real(kind=realType), dimension(3) :: rotRate

         ! orderXi:   Accuracy of the discretization in 
         !            xi-direction  (== i-direction).
         ! orderEta:  Idem in eta-direction (== j-direction).
         ! orderZeta: Idem in zeta-direction (== k-direction).

         integer(kind=intType) :: orderXi, orderEta, orderZeta

       end type cgnsBlockInfoType
!
!      ******************************************************************
!      *                                                                *
!      * The definition of the derived data type to store the data of a *
!      * cgns family.                                                   *
!      *                                                                *
!      ******************************************************************
!
       type cgnsFamilyType

         ! familyName:      Name of the family.
         ! BCTypeCGNS:      CGNS boundary condition type.
         ! BCType:          Internal boundary condition type.
         ! bcName:          Family BC name.
         ! userDefinedName: Name of the CGNS user defined data node if
         !                  the CGNS boundary condition is UserDefined.

         integer                       :: BCTypeCGNS
         integer(kind=intType)         :: BCType
         character(len=maxCGNSNameLen) :: familyName
         character(len=maxCGNSNameLen) :: bcName
         character(len=maxCGNSNameLen) :: userDefinedName

         ! slidingID:            The ID of the sliding mesh interface of
         !                       which this family is part. 0 means that
         !                       this family is not part of a sliding
         !                       mesh interface. This value can be
         !                       positive and negative in order to
         !                       distinguish between the two sides of the
         !                       interface. The absolute value is the
         !                       actual ID of the interface.
         ! bleedRegionID:        The ID of the bleed flow region of which
         !                       this family is part. 0 means that this
         !                       family does not belong to a bleed flow
         !                       region. There is no need to distinguish
         !                       between an inflow and an outflow bleed
         !                       for the ID, because the boundary
         !                       conditions are different.
         ! wallFunctions:        Whether or not to use wall functions for
         !                       this family. Only for viscous walls for
         !                       a RANS computation. Default is the value
         !                       specified in the parameter file.
         ! contributeToForce:    Whether or not this family contributes
         !                       to the total forces and moments to be
         !                       computed. Only for wall boundaries.
         !                       Default is yes.
         ! monitorForceSeparate: Whether or not to monitor the forces
         !                       and moments separately for this family.
         !                       Default is no.

         integer(kind=intType) :: slidingID
         integer(kind=intType) :: bleedRegionID
         logical               :: wallFunctions
         logical               :: contributeToForce
         logical               :: monitorForceSeparate

         ! nDataSet:               Number of boundary condition datasets
         !                         for this family.
         ! dataSet(:):             The actual boundary condition data
         !                         sets. Dimension(nDataSet).
         ! rotatingFrameSpecified: Whether or not a rotating frame is
         !                         specified.
         ! rotCenter(3):           The corresponding rotation center.
         ! rotRate(3):             The corresponding rotation rate.

         integer(kind=intType) :: nDataSet
         logical               :: rotatingFrameSpecified

         type(cgnsBCDatasetType), pointer, dimension(:) :: dataSet
         real(kind=realType),              dimension(3) :: rotCenter
         real(kind=realType),              dimension(3) :: rotRate

       end type cgnsFamilyType
!
!      ******************************************************************
!      *                                                                *
!      * Definition of the variables stored in this module.             *
!      *                                                                *
!      ******************************************************************
!
       ! cgnsCellDim:     Dimensions of the computational cells. Should
       !                  be 3 for this code.
       ! cgnsPhysDim:     Physical dimension, i.e. number of coordinates.
       !                  Should also be 3.
       ! cgnsBaseName:    Name of the cgns base.
       ! cgnsNDom:        Number of blocks (zones) in the cgns grid.
       ! cgnsNFamilies:   Number of families.
       ! cgnsDoms(:):     Array of cgns blocks. Dimension [cgnsNDom].
       ! cgnsFamilies(:): Array of families. Dimension [cgnsNFamilies].
       ! oversetPresent:  Whether or not there are overset grids present.

       integer               :: cgnsCellDim
       integer               :: cgnsPhysDim
       integer(kind=intType) :: cgnsNDom
       integer(kind=intType) :: cgnsNFamilies
       logical               :: oversetPresent

       character(len=maxCGNSNameLen) :: cgnsBaseName

!tapenade!       type(cgnsBlockInfoType), allocatable, dimension(:) :: cgnsDoms
!tapenade!       type(cgnsFamilyType),    allocatable, dimension(:) :: cgnsFamilies

       ! cgnsNSliding:              Number of sliding mesh interfaces
       !                            in the grid.
       ! cgnsNDomainInterfaces:     Number of domain interfaces, i.e.
       !                            interfaces with other CFD codes,
       !                            in the grid.
       ! famIDsDomainInterfaces(:): The family ID's of the domain
       !                            interfaces.
       !                            Dimension [cgnsNDomainInterfaces].
       ! bcIDsDomainInterfaces(:):  The BC ID's of the domain interfaces.
       !                            Dimension [cgnsNDomainInterfaces].

       integer(kind=intType) :: cgnsNSliding
       integer(kind=intType) :: cgnsNDomainInterfaces

!tapenade!       integer(kind=intType), allocatable, dimension(:) :: &
!tapenade!                                            famIDsDomainInterfaces
       integer(kind=intType), allocatable, dimension(:,:) :: &
                                            bcIDsDomainInterfaces

       end module cgnsGrid
