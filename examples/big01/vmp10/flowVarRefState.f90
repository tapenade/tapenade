
!
!      ******************************************************************
!      *                                                                *
!      * File:          flowVarRefState.f90                             *
!      * Author:        Edwin van der Weide , Seonghyeon Hahn,          *
!      *                Andre C. Marta                                  *
!      * Starting date: 01-03-2003                                      *
!      * Last modified: 07-05-2006                                      *
!      *                                                                *
!      ******************************************************************
!
       module flowVarRefState
!
!      ******************************************************************
!      *                                                                *
!      * Module that contains information about the reference state as  *
!      * well as the nondimensional free stream state.                  *
!      *                                                                *
!      ******************************************************************
!
       use constants
       implicit none
       save

       ! nw:       Total number of independent variables including the
       !           turbulent variables.
       ! nwFlow:   Number of flow variables. For perfect gas computations
       !           this is 5.
       ! nwTurb:   Number of turbulent variables, nwTurb = nw - nwFlow
       ! nt1, nt2: Initial and final indices for turbulence variables

       integer(kind=intType) :: nw, nwFlow, nwTurb, nt1, nt2

       ! pRef:     Reference pressure (in Pa) used to nondimensionalize
       !           the flow equations.
       ! rhoRef:   Reference density (in kg/m^3) used to
       !           nondimensionalize the flow equations.
       ! TRef:     Reference temperature (in K) used to nondimensionalize
       !           the flow equations.
       ! muRef:    Scale for the viscosity, 
       !           muRef = rhoRef*sqrt(pRef/rhoRef); there is also a
       !           reference length in the nondimensionalization of mu,
       !           but this is 1.0, because all the coordinates are 
       !           converted to meters.
       ! timeRef:  time scale; needed for a correct 
       !           nondimensionalization of unsteady problems.
       !           timeRef = sqrt(rhoRef/pRef); for the reference
       !           length, see the comments for muRef.

       real(kind=realType) :: pRef, rhoRef, TRef
       real(kind=realType) :: muRef, timeRef

       ! LRef:          Conversion factor of the length unit of the
       !                grid to meter. e.g. if the grid is in mm.,
       !                LRef = 1.e-3.
       ! LRefSpecified: Whether or not a conversion factor is specified
       !                in the input file.

       real(kind=realType) :: LRef
       logical ::             LRefSpecified

       ! pInfDim:   Free stream pressure in Pa.
       ! rhoInfDim: Free stream density in kg/m^3.
       ! TInfDim:   Free stream temperature in K.
       ! muDim:     Free stream molecular viscosity in kg/(m s)

       real(kind=realType) :: pInfDim, rhoInfDim, TInfDim, muDim

       ! wInf(nw): Nondimensional free stream state vector.
       !           Variables stored are rho, u, v, w and p.
       ! pInf:     Nondimensional free stream pressure.
       ! pInfCorr: Nondimensional free stream pressure, corrected for
       !           a possible presence of 2/3 rhok.
       ! rhoInf:   Nondimensional free stream density.
       ! vInf:     Nondimensional free stream velocity.
       ! rhoEInf:  Nondimensional free stream total energy.
       ! muInf:    Nondimensional free stream viscosity.
       ! RGas:     Nondimensional gas constant.
       ! gammaInf: Free stream specific heat ratio.

       real(kind=realType) :: rhoInf, vInf, pInf, pInfCorr, rhoEInf
       real(kind=realType) :: RGas, muInf, gammaInf
!tapenade!       real(kind=realType), dimension(:), allocatable :: wInf

       ! mu0Ref:     Reference magnetic permeability (in H/m) used to
       !             nondimensionalize the flow equations.
       ! sigmaRef:   Reference electrical conductivity (in S/m) used to
       !             nondimensionalize the flow equations.
       ! bRef:       Reference magnetic field (in T) used to
       !             nondimensionalize the flow equations.

       real(kind=realType) :: mu0Ref, sigmaRef, bRef

       ! mu0InfDim:   Free stream magnetic permeability in H/m.
       ! sigmaInfDim: Free stream electrical conductivity in S/m.

       real(kind=realType) :: mu0InfDim, sigmaInfDim

       ! mu0Inf:   Nondimensional freestream magnetic permeability
       ! sigmaInf: Nondimensional freestream electrical conductivity

       real(kind=realType) :: mu0Inf, sigmaInf

       ! MagneticForce:       Magnetic force number.
       ! MagneticReynolds:    Magnetic Reynolds number.
       ! MagneticInteraction: Magnetic interaction parameter.

       real(kind=realType) :: MagneticForce
       real(kind=realType) :: MagneticReynolds
       real(kind=realType) :: MagneticInteraction

       ! viscous:   whether or not this is a viscous computation.
       ! kPresent:  whether or not a turbulent kinetic energy is present
       !            in the turbulence model.
       ! eddyModel: whether or not the turbulence model is an eddy
       !            viscosity model.
       ! magnetic:  whether or not this is an MHD computation.

       logical :: kPresent, eddyModel, viscous, magnetic

       end module flowVarRefState
