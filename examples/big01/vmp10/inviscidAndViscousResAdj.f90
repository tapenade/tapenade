
!
!     ******************************************************************
!     *                                                                *
!     * File:          inviscidAndViscousResAdj.f90                    *
!     * Author:        Edwin van der Weide, Andre C. Marta             *
!     * Starting date: 05-22-2006                                      *
!     * Last modified: 08-02-2006                                      *
!     *                                                                *
!     ******************************************************************
!
      subroutine inviscidAndViscousResAdj(wAdj, dwAdj, fluxXiAdj,  &
                                          fluxEtaAdj, fluxZetaAdj, &
                                          iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * inviscidAndViscousRes computes the residual of the inviscid    *
!     * and viscous fluxes. When this routine is called the fluxes in  *
!     * xi, eta and zeta direction should contain the correct values.  *
!     * The residual is obtained by applying the first derivative      *
!     * difference operator to these fluxes.                           *
!     *                                                                *
!     * This routine is based on /solver/inviscidAndViscousRes.f90.    *
!     *                                                                *
!     ******************************************************************
!
      use blockPointers
      use cgnsGrid
      use flowVarRefState ! nw
      use inputPhysics
      use iteration
      implicit none
!
!     Subroutine arguments
!
      real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                      intent(in) :: wAdj
      real(kind=realType), dimension(nw), intent(inout) :: dwAdj
      real(kind=realType), dimension(-1:1,nw), &
                        intent(in) :: fluxXiAdj, fluxEtaAdj, fluxZetaAdj

      integer(kind=intType), intent(in) :: iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: i, j, k

      real(kind=realType) :: rvol, wx, wy, wz
!
!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      ! Residual in xi-direction.

      call diffXi_1stAdj(dwAdj, fluxXiAdj, nw, orderFlowXi, &
                         iNode, jNode, kNode)

      ! Residual eta-direction.

      call diffEta_1stAdj(dwAdj, fluxEtaAdj, nw, orderFlowEta, &
                          iNode, jNode, kNode)

      ! Residual in zeta-direction.

      call diffZeta_1stAdj(dwAdj, fluxZetaAdj, nw, orderFlowZeta, &
                           iNode, jNode, kNode)
!
!     ****************************************************************
!     *                                                              *
!     * The rotational source terms for a moving block in steady     *
!     * mode. These source terms account for the centrifugal         *
!     * acceleration and the Coriolis term. However, as the          *
!     * equations are solved in the inertial frame and not in the    *
!     * moving frame, the form is different than what you normally   *
!     * find in a text book.                                         *
!     *                                                              *
!     ****************************************************************
!
!!      rotation: if(blockIsMoving .and. equationMode == steady) then
!!
!!        ! Compute the three nonDimensional angular velocities.
!!
!!        wx = timeRef*cgnsDoms(nbkGlobal)%rotRate(1)
!!        wy = timeRef*cgnsDoms(nbkGlobal)%rotRate(2)
!!        wz = timeRef*cgnsDoms(nbkGlobal)%rotRate(3)
!!
!!        ! Compute the rotational source terms for the momentum 
!!        ! equations.
!!
!!        k = kNode
!!        j = jNode
!!        i = iNode
!!
!!        rvol = wAdj(0,0,0,irho)*detJinv(i,j,k)
!!
!!        dwAdj(imx) = dwAdj(imx) &
!!                   + rvol*(wy*wAdj(0,0,0,ivz) -wz*wAdj(0,0,0,ivy))
!!        dwAdj(imy) = dwAdj(imy) &
!!                   + rvol*(wz*wAdj(0,0,0,ivx) -wx*wAdj(0,0,0,ivz))
!!        dwAdj(imz) = dwAdj(imz) &
!!                   + rvol*(wx*wAdj(0,0,0,ivy) -wy*wAdj(0,0,0,ivx))
!!
!!      endif rotation

      end subroutine inviscidAndViscousResAdj
