
!
!     ******************************************************************
!     *                                                                *
!     * File:          computeEnergyAdj.f90                            *
!     * Author:        Andre C. Marta                                  *
!     * Starting date: 07-31-2006                                      *
!     * Last modified: 08-01-2006                                      *
!     *                                                                *
!     ******************************************************************
!
      subroutine computeEnergyAdj(wAdj, rhoEAdj)
!
!     ******************************************************************
!     *                                                                *
!     * Simple routine to compute the total energy rhoE from the       *
!     * primitive variables w.                                         *
!     * A calorically perfect gas, i.e. constant gamma, is assumed.    *
!     *                                                                *
!     ******************************************************************
!
      use flowVarRefState ! nw, kPresent
      use inputPhysics    ! gammaConstant
      implicit none
!
!     Subroutine arguments
!
      real(kind=realType), dimension(-2:2,-2:2,-2:2,nw), &
                                                  intent(in) :: wAdj
      real(kind=realType), dimension(-2:2,-2:2,-2:2), &
                                                  intent(out) :: rhoEAdj
!
!     Local variables
!
      integer(kind=intType) :: i, j, k

      real(kind=realType) :: gm1, factK, v2

!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      gm1 = gammaConstant - one

      ! Check the situation.

      if( kPresent ) then

        ! A separate equation for the turbulent kinetic energy is
        ! present. This variable must be taken into account.

        factK = five*third - gammaConstant

        do k=-2,2
          do j=-2,2
            do i=-2,2
              v2 = wAdj(i,j,k,ivx)**2 + wAdj(i,j,k,ivy)**2 &
                 + wAdj(i,j,k,ivz)**2

              rhoEAdj(i,j,k) = ( wAdj(i,j,k,ip) &
                     - factK*wAdj(i,j,k,irho)*wAdj(i,j,k,itu1) ) / gm1 &
                     + half*wAdj(i,j,k,irho)*v2
            enddo
          enddo
        enddo

      else

        ! No separate equation for the turbulent kinetic enery.
        ! Use the standard formula.

        do k=-2,2
          do j=-2,2
            do i=-2,2
              v2 = wAdj(i,j,k,ivx)**2 + wAdj(i,j,k,ivy)**2 &
                 + wAdj(i,j,k,ivz)**2

              rhoEAdj(i,j,k) = wAdj(i,j,k,ip)/gm1 + half*wAdj(i,j,k,irho)*v2
            enddo
          enddo
        enddo

      endif

      end subroutine computeEnergyAdj
