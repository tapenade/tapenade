
!
!     ******************************************************************
!     *                                                                *
!     * File:          firstDerivativesAdj.f90                         *
!     * Author:        Edwin van der Weide, Andre C. Marta             *
!     * Starting date: 02-11-2006                                      *
!     * Last modified: 08-01-2006                                      *
!     *                                                                *
!     ******************************************************************
!
      subroutine diffXi_1stAdj(res, var, nVar, orderXi, &
                               iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * diffXi_1st computes the first derivative of the function var   *
!     * in xi-direction (== i-direction) and adds the result to res.   *
!     * The spacing in xi-direction is uniform and equal to 1, such    *
!     * that only differences need to be taken.                        *
!     * It is assumed that the variables which define the              *
!     * corresponding block size, ib, ie, il, etc., which are stored   *
!     * in blockPointers, contain the correct values.                  *
!     * Idem for iMinBoundaryStencil and iMaxBoundaryStencil, which    *
!     * define whether or not this block is responsible for the        *
!     * discretization at the block boundaries.                        *
!     *                                                                *
!     ******************************************************************
!
      use accuracy
      use blockPointers, only : il, jl, kl,          &
                                iMinBoundaryStencil, &
                                iMaxBoundaryStencil
      use constants
      implicit none
!
!     Subroutine arguments.
!
      real(kind=realType), dimension(nVar), intent(inout) :: res
      real(kind=realType), dimension(-1:1,nVar), intent(in) :: var

      integer(kind=intType), intent(in) :: nVar, orderXi, &
                                           iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: nn

      real(kind=realType) :: diff
!
!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      ! Determine the accuracy for this block and select the
      ! appropriate discretization.

      select case (orderXi)

        case (firstOrder, secondOrder)

          ! Second order discretization is used for the first
          ! derivative, at least for interior points.

          ! Check if I'm responsible for the boundary treatment on
          ! the iMin side.

          if( iMinBoundaryStencil .and. iNode==1 ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(1,nn) - var(0,nn)
              res(nn) = res(nn) + diff
            enddo

          ! Check if I'm responsible for the boundary treatment on
          ! the iMax side.

          elseif( iMaxBoundaryStencil .and. iNode==il ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(0,nn) - var(-1,nn)
              res(nn) = res(nn) + diff
            enddo

          ! The internal discretization.

          else

            ! Just a standard second order finite difference.

            do nn=1,nVar
              diff = half*(var(1,nn) - var(-1,nn))
              res(nn) = res(nn) + diff
            enddo

          endif

        !===============================================================

        case (thirdOrder, fourthOrder)

          ! Fourth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (fifthOrder,sixthOrder)

          ! Sixth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (seventhOrder, eighthOrder)

          ! Eighth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

      end select

      end subroutine diffXi_1stAdj

      !=================================================================

      subroutine diffEta_1stAdj(res, var, nVar, orderEta, &
                                iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * diffEta_1st computes the first derivative of the function      *
!     * var in eta-direction (== j-direction) and adds the result to   *
!     * res. The spacing in eta-direction is uniform and equal to 1,   *
!     * such that only differences need to be taken.                   *
!     * It is assumed that the variables which define the              *
!     * corresponding block size, ib, ie, il, etc., which are stored   *
!     * in blockPointers, contain the correct values.                  *
!     * Idem for jMinBoundaryStencil and jMaxBoundaryStencil, which    *
!     * define whether or not this block is responsible for the        *
!     * discretization at the block boundaries.                        *
!     *                                                                *
!     ******************************************************************
!
      use accuracy
      use blockPointers, only : il, jl, kl,          &
                                   jMinBoundaryStencil, &
                                   jMaxBoundaryStencil
      use constants
      implicit none
!
!     Subroutine arguments.
!
      real(kind=realType), dimension(nVar), intent(inout) :: res
      real(kind=realType), dimension(-1:1,nVar), intent(in) :: var

      integer(kind=intType), intent(in) :: nVar, orderEta, &
                                           iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: nn

      real(kind=realType) :: diff
!
!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      ! Determine the accuracy for this block and select the
      ! appropriate discretization.

      select case (orderEta)

        case (firstOrder, secondOrder)

          ! Second order discretization is used for the first
          ! derivative, at least for interior points.

          ! Check if I'm responsible for the boundary treatment on
          ! the jMin side.

          if( jMinBoundaryStencil .and. jNode==1 ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(1,nn) - var(0,nn)
              res(nn) = res(nn) + diff
            enddo

          ! Check if I'm responsible for the boundary treatment on
          ! the jMax side.

          elseif( jMaxBoundaryStencil .and. jNode==jl ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(0,nn) - var(-1,nn)
              res(nn) = res(nn) + diff
            enddo

          ! The internal discretization.

          else

            ! Just a standard second order finite difference.

            do nn=1,nVar
              diff = half*(var(1,nn) - var(-1,nn))
              res(nn) = res(nn) + diff
            enddo

          endif

        !===============================================================

        case (thirdOrder, fourthOrder)

          ! Fourth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (fifthOrder,sixthOrder)

          ! Sixth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (seventhOrder, eighthOrder)

          ! Eighth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

      end select

      end subroutine diffEta_1stAdj

      !=================================================================

      subroutine diffZeta_1stAdj(res, var, nVar, orderZeta, &
                                 iNode, jNode, kNode)
!
!     ******************************************************************
!     *                                                                *
!     * diffZeta_1st computes the first derivative of the function     *
!     * var in zeta-direction (== k-direction) and adds the result     *
!     * to res. The spacing in eta-direction is uniform and equal      *
!     * to 1, such that only differences need to be taken.             *
!     * It is assumed that the variables which define the              *
!     * corresponding block size, ib, ie, il, etc., which are stored   *
!     * in blockPointers, contain the correct values.                  *
!     * Idem kMinBoundaryStencil and kMaxBoundaryStencil, which        *
!     * define whether or not this block is responsible for the        *
!     * discretization at the block boundaries.                        *
!     *                                                                *
!     ******************************************************************
!
      use accuracy
      use blockPointers, only : il, jl, kl,          &
                                kMinBoundaryStencil, &
                                kMaxBoundaryStencil
      use constants
      implicit none
!
!     Subroutine arguments.
!
      real(kind=realType), dimension(nVar), intent(inout) :: res
      real(kind=realType), dimension(-1:1,nVar), intent(in) :: var

      integer(kind=intType), intent(in) :: nVar, orderZeta, &
                                           iNode, jNode, kNode
!
!     Local variables.
!
      integer(kind=intType) :: nn

      real(kind=realType) :: diff
!
!     ******************************************************************
!     *                                                                *
!     * Begin execution                                                *
!     *                                                                *
!     ******************************************************************
!
      ! Determine the accuracy for this block and select the
      ! appropriate discretization.

      select case (orderZeta)

        case (firstOrder, secondOrder)

          ! Second order discretization is used for the first
          ! derivative, at least for interior points.

          ! Check if I'm responsible for the boundary treatment on
          ! the kMin side.

          if( kMinBoundaryStencil .and. kNode==1 ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(1,nn) - var(0,nn)
              res(nn) = res(nn) + diff
            enddo

          ! Check if I'm responsible for the boundary treatment on
          ! the kMax side.

          elseif( kMaxBoundaryStencil .and. kNode==kl ) then

            ! Do the boundary discretization, which is just a 
            ! one-sided difference in this case.

            do nn=1,nVar
              diff = var(0,nn) - var(-1,nn)
              res(nn) = res(nn) + diff
            enddo

          ! The internal discretization.

          else

            ! Just a standard second order finite difference.

            do nn=1,nVar
              diff = half*(var(1,nn) - var(-1,nn))
              res(nn) = res(nn) + diff
            enddo

          endif

        !===============================================================

        case (thirdOrder, fourthOrder)

          ! Fourth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (fifthOrder,sixthOrder)

          ! Sixth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

        !===============================================================

        case (seventhOrder, eighthOrder)

          ! Eighth order discretization is used for the first
          ! derivative, at least for interior points.

          stop

      end select

      end subroutine diffZeta_1stAdj
