Tapenade Algorithmic Differentiation Tool
=========================================

[Tapenade](https://team.inria.fr/ecuador/en/tapenade/)
is a tool for Algorithmic Differentiation of programs.
Given a set of source files (Fortran or C) that compute a mathematical function F,
Tapenade produces another set of files (in the same language) that compute derivatives of F.

- Dependencies
    - Docker [https://www.docker.com](https://www.docker.com)
    - Java [https://jdk.java.net](https://jdk.java.net)

- Compilation
    - after modification of java files:  
    `./gradlew`

    - after modification of IL formalism or parsers:  
   `./gradlew clean; ./gradlew`

- Execution  
    `./bin/tapenade -version`  
    `./bin/tapenade nonRegressions/set01/lh001/program.f -d`  
    creates files `program_d.f` and `program_d.msg`, into the current directory.

- Execution from tapenade docker image  
    Call `tapenadocker` instead of `tapenade`.    
    `tapenadocker` shell script runs the `registry.gitlab.inria.fr/tapenade/tapenade`
    docker image.  
    To download the latest tapenade image:  
    `docker pull registry.gitlab.inria.fr/tapenade/tapenade`  
    Browse [Tapenade Container Registry](https://gitlab.inria.fr/tapenade/tapenade/container_registry/643) to get
    all Tapenade docker images.

- Gitlab pages  
    [https://tapenade.gitlabpages.inria.fr/tapenade/](https://tapenade.gitlabpages.inria.fr/tapenade/)
contains the latest distribution and documentation.  
    Browse [Tapenade Package Registry](https://gitlab.inria.fr/tapenade/tapenade/-/packages/894) to get all Tapenade tar archives.

- Documentation
    - [Tapenade User Documentation](https://tapenade.gitlabpages.inria.fr/userdoc)  
    - [Tapenade Developer Documentation](https://tapenade.gitlabpages.inria.fr/tapenade/docs/html)
    
    To build this documentation see [docs/README.md](docs/README.md) and
    [docs/README-otherdoc.md](docs/README-otherdoc.md).

- [CONTRIBUTING](CONTRIBUTING.md)
   
- [LICENSE](LICENSE.md)
   
- [AUTHORS](AUTHORS.md)

- [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md)
