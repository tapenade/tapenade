subroutine fonctiontTest(buf, resultat)
use mpi 
implicit none 
integer::ierror
integer::count
integer::root     ! rang d un processus
integer::req
integer::tag
integer::i 
logical::flag
real*8,allocatable,dimension(:)::buf
real*8,allocatable,dimension(:)::resultat
integer,dimension(MPI_STATUS_SIZE)::status
tag=10
count=10
root=0
! initialisation environnement MPI
call MPI_INIT(ierror)
allocate(buf(10))
allocate(resultat(10))

  do i=1,10
  buf(i)=i
  end do

call mpi_isend(buf,count,MPI_DOUBLE_PRECISION,root,tag,req,ierror)
 
  101 continue
  
  CALL MPI_TEST(req, flag, status, ierror)
    
   if (flag.eqv..false.) then 
   goto 101
   else 
  do i=1,10
  resultat(i)=buf(i)*buf(i)
  end do
  end if 

   end subroutine
