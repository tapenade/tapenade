module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

function top(r,s)
use M
implicit none
real, dimension(n) :: r,s
real top
  interface compute
      function compute (x,y)
       use M
       implicit none
       real, dimension(n) :: x,y
       real :: compute
      end function compute
  end interface

real, external :: ftest
top = ftest(r,s,compute)
end function top

