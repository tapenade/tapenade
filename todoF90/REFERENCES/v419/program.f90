!un exemple pour les questions de contexte d'appel
! du code differentie, les _D et les _CD, inspire de
! l'exemple dans la FAQ #usagecontext. cf bug Jean.
module MMA
  REAL, ALLOCATABLE, DIMENSION(:) :: AA
  REAL, ALLOCATABLE, DIMENSION(:) :: TT1
  REAL, ALLOCATABLE, DIMENSION(:) :: TT2
contains
  subroutine initA()
    ALLOCATE(AA(20))
  end subroutine initA

  subroutine initSome(someTT)
    REAL, ALLOCATABLE, DIMENSION(:) :: someTT
    ALLOCATE(someTT(20))
  end subroutine initSome

  subroutine deallocSome(someTT)
    REAL, ALLOCATABLE, DIMENSION(:) :: someTT
    DEALLOCATE(someTT)
  end subroutine deallocSome
end module MMA

module MMB
  INTEGER :: flag
  REAL :: zz
end module MMB

program MAIN
  use MMA
  use MMB
  REAL, ALLOCATABLE, DIMENSION(:) :: X
  INTEGER i
  REAL cva(2)
  common /cc1/cva
!  call getflag(flag)
  flag = 1
  ALLOCATE(X(30))
  call initA()
  call initSome(TT1)
  call initSome(TT2)
  do i = 1,20
     AA(i) = 100.0/i
     X(i) = 3.3*i
  enddo
  cva(1) = 1.8
  cva(2) = 8.1
  call ROOT(X)
  print*,'zz=',zz
  DEALLOCATE(AA)
  DEALLOCATE(X)
  call deallocSome(TT1)
  call deallocSome(TT2)
end program MAIN

subroutine ROOT(X)
  use MMA
  use MMB
  REAL, DIMENSION(*) :: X
  REAL :: V
  REAL :: cv1,cv2
  common /cc1/cv1,cv2

  zz = cv1*cv2
  zz = zz*(TT1(1)*TT2(2))
  v = SUM(AA)
  if (flag>2) then
     zz = zz*v
  endif
  zz = zz + SUM(X)
end subroutine ROOT
     
  
  
  

  
