!        Generated by TAPENADE     (INRIA, Ecuador team)
!  Tapenade 3.14 (r7129) - 30 Oct 2018 14:29
!
MODULE AATYPES
  IMPLICIT NONE
  TYPE REAL4_RD
      SEQUENCE 
      REAL :: v
      REAL :: d
  END TYPE REAL4_RD
END MODULE AATYPES

MODULE EXAMPLE3_RD
  USE AATYPES
  IMPLICIT NONE
  PRIVATE 
  REAL :: x, y
  TYPE(REAL4_RD), PUBLIC :: u
END MODULE EXAMPLE3_RD

MODULE M1_RD
  USE AATYPES
  USE EXAMPLE3_RD
  IMPLICIT NONE
  REAL :: z

CONTAINS
!  Differentiation of f4 in forward (tangent) mode (with options associationByAddress):
!   variations   of useful results: t
!   with respect to varying inputs: u t
!   RW status of diff variables: u:in t:in-out
  SUBROUTINE F4_RD(t)
    IMPLICIT NONE
    TYPE(REAL4_RD) :: t
    REAL :: f4
    t%d = (t%d*t%v+t%v*t%d)*u%v + t%v**2*u%d
    t%v = t%v**2*u%v
  END SUBROUTINE F4_RD

  FUNCTION F4(t)
    IMPLICIT NONE
    REAL :: t
    REAL :: f4
    t = t**2*u%v
  END FUNCTION F4

END MODULE M1_RD

