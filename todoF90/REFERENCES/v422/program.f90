module example3
 implicit none
 private
 real :: x,y
 real, public :: u
end module

module m1
 use example3
 real :: z
 contains
      function f4(t)
      real t
      t = t * t * u
      end function

end module m1

