!        Generated by TAPENADE     (INRIA, Tropics team)
!  Tapenade 3.7 (r4809M) - 20 Mar 2013 16:41
!
MODULE M_D
  IMPLICIT NONE
  INTEGER, PARAMETER :: n=2
  REAL :: global
  REAL :: globald
END MODULE M_D

!  Differentiation of top in forward (tangent) mode:
!   variations   of useful results: r s top
!   with respect to varying inputs: r s
!   RW status of diff variables: r:in-out s:in-out top:out
FUNCTION TOP_D(r, rd, s, sd, top)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL, DIMENSION(n) :: rd, sd
  REAL :: top
  REAL :: top_d
  REAL, EXTERNAL :: COMPUTE_CD
  REAL, EXTERNAL :: COMPUTE_D
  REAL, EXTERNAL :: FTEST_CD
  REAL :: FTEST_D
  top_d = FTEST_D(r, rd, s, sd, COMPUTE_CD, COMPUTE_D, top)
END FUNCTION TOP_D

!  Differentiation of ftest in forward (tangent) mode:
!   variations   of useful results: r s ftest
!   with respect to varying inputs: r s
FUNCTION FTEST_D(r, rd, s, sd, COMPUTE_CD, COMPUTE_D, ftest)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL, DIMENSION(n) :: rd, sd
  REAL :: ftest
  REAL :: ftest_d
  REAL, EXTERNAL :: COMPUTE_CD
  REAL :: COMPUTE_D
  EXTERNAL COMPUTE_D
  ftest_d = COMPUTE_D(r, rd, s, sd, ftest)
END FUNCTION FTEST_D

!  Differentiation of compute in forward (tangent) mode:
!   variations   of useful results: global y compute
!   with respect to varying inputs: global x
!   RW status of diff variables: global:in-out x:in y:out compute:out
FUNCTION COMPUTE_D(x, xd, y, yd, compute)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: x, y
  REAL, DIMENSION(n) :: xd, yd
  REAL :: compute
  REAL :: compute_d
  yd = 2*xd
  y = 2*x
  compute_d = yd(1)*y(2) + y(1)*yd(2)
  compute = y(1)*y(2)
  globald = globald + compute_d
  global = global + compute
END FUNCTION COMPUTE_D

SUBROUTINE PROG_CD()
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL :: TOP_CD
  REAL :: result1
  r(1) = 3.0
  s(1) = 0.0
  r(2) = 2.0
  s(2) = 0.0
  result1 = TOP_CD(r, s)
  PRINT*, result1
END SUBROUTINE PROG_CD

FUNCTION TOP_CD(r, s) RESULT (TOP)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL :: top
  REAL, EXTERNAL :: COMPUTE_CD
  REAL, EXTERNAL :: FTEST_CD
  top = FTEST_CD(r, s, compute_cd)
END FUNCTION TOP_CD

FUNCTION FTEST_CD(r, s, COMPUTE_CD) RESULT (FTEST)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: r, s
  REAL :: ftest
  REAL, EXTERNAL :: COMPUTE_CD
  ftest = COMPUTE_CD(r, s)
END FUNCTION FTEST_CD

FUNCTION COMPUTE_CD(x, y) RESULT (COMPUTE)
  USE M_D
  IMPLICIT NONE
  REAL, DIMENSION(n) :: x, y
  REAL :: compute
  y = 2*x
  compute = y(1)*y(2)
  global = global + compute
END FUNCTION COMPUTE_CD

