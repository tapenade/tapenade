program SYSTEME

   integer         :: ErreurNumero
   integer         :: retour

   ErreurNumero = 0

   if( ErreurNumero /= 0 ) then
      stop
   endif

   DT_Trac = DT * DBLE(FreqCouplage)

   if( ErreurNumero /= 0 ) stop
   nb_sect = size(X)

   retour = 0
   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if


   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   DebitFlux  = W0

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   P1 = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   P2 = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   B1 = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   B2 = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   BS = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   RH1 = W0    ! W0 = 0._DOUBLE

   if( retour /= 0 ) then
      ErreurNumero = 5
      stop
   end if
   RH2 = W0    ! W0 = 0._DOUBLE

   if( Noyau == NOYAU_MASCARET ) then
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if
      XFRON = W0    ! W0 = 0._DOUBLE
   endif

   allocate (SVRAI(size(X)), STAT = retour)

   if( retour /= 0 ) then
      Erreurft_c = err_5c
      stop
   end if

   Q1 = Q
   if( OptionCasier ) then
      allocate( QCasier(size(Z)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if
      QCasier = W0
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if
      CoefficientA = W0
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if
      CoefficientC = W0
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if
      CoefficientB = W0
   endif

   if( OptionTracer ) then
      allocate( QT(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( ST(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( BT(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( QT_ANT(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( ST_ANT(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( BT_ANT(size(X)) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( NbCourant(nb_bief) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( MASS(nb_bief,Nbtrac) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( FLUMAS(nb_bief,Nbtrac) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( FLUENT(nb_bief,Nbtrac) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( FLUSOR(nb_bief,Nbtrac) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      allocate( FLUSRC(nb_bief,Nbtrac) , STAT = retour )
      if( retour /= 0 ) then
         ErreurNumero = 5
         stop
      end if

      FLUENT = W0

   endif

   icouplage = 0

   do while( ErreurNumero == 0 )
      if( phase_planim == PHASE_CALCUL ) then
         call  PLANIM             ( &
            Erreur                  ) ! Erreur
         if( ErreurNumero /= 0 ) then
            stop
         endif
      endif

      if( phase_intersect == PHASE_INITIALISATION .or. phase_intersect == PHASE_CALCUL) then

         if( ErreurNumero /= 0 ) then
            stop
         endif

      endif

      if( phase_qcl == PHASE_INITIALISATION .or. phase_qcl == PHASE_CALCUL ) then
         if( noyau == NOYAU_MASCARET .and. phase_qcl == PHASE_INITIALISATION ) then
            Temps1 = Temps + DT
         else
            Temps1 = Temps
         endif
         call QCL             ( &
              Erreur            ) ! Erreur
         CALL CPU_TIME( t1 )
         if( ErreurNumero /= 0 ) then
            stop
         endif
      endif


   if( OptionCasier ) then

      if( phase_clpluie == PHASE_CALCUL ) then
         if( ErreurNumero /= 0 ) then
            stop
         endif

      end if

   end if
   do while( ( PhaseCouplage == PHASE_INITIALISATION) .or. ( PhaseCouplage == PHASE_CALCUL ) )

      if( phase_stock_couplage == PHASE_CALCUL ) then
         call STOCK_COUPLAGE( Z , Ziter , Q1 , Q1iter , Q2 , Q2iter )
      end if

      if( phase_casier == PHASE_CALCUL ) then

         call REZCAS              (&
              Erreur                  )! erreur

         if( ErreurNumero /= 0 ) then
            stop
         endif

      end if

      select case( Noyau )

         case( NOYAU_SARAP )

            if( phase_sarap == PHASE_CALCUL ) then
               if( ( OptionTracer ).and.( npass.GE.1 ) ) then
                  continue
               elseif( OptionTracer ) then
                  write(6,*)'! Calcul Tracer en permanent => Passage unique dans SARAP !'
                  write(6,110) Temps
               endif
               110     format(' Temps = ',f12.3)

               if( ErreurNumero /= 0 ) then
                  stop
               endif

            endif

         case( NOYAU_REZODT )

            if( phase_rezo == PHASE_INITIALISATION .or. phase_rezo == PHASE_CALCUL ) then

               if( ErreurNumero /= 0 ) then
                  stop
               endif

            endif

         case( NOYAU_MASCARET )

            if( phase_mascaret == PHASE_INITIALISATION .or. phase_mascaret == PHASE_CALCUL ) then
               if( ErreurNumero /= 0 ) then
                  stop
               endif

               if( OptionTracer ) then
                  do j = 1,size(ConnectOrigineBief)
                     m = ConnectFinBief(j)
                     do I = l , m
                        call RHSBP_S         ( &
                             Erreur            &
                                   )

                        if( ErreurNumero /= 0 ) then
                           stop
                        end if
                     enddo
                  enddo
               endif
           end if
      end select

      if( phase_increment_couplage == PHASE_CALCUL ) then
         call INCREMENT_COUPLAGE (    &
                            Z,        &
                            TETA3)
      end if

      if( OptionCasier ) then
         if( PhaseCouplage == PHASE_INITIALISATION ) then
            PhaseCouplage = PHASE_ARRET
            if( retour /= 0 ) then
               ErreurNumero = 6
            end if
         else
            if( icouplage == (NMITER + 1) ) then
               PhaseCouplage = PHASE_ARRET
            end if
         end if
      else
         PhaseCouplage = PHASE_ARRET
      endif
   end do

   if( phase_post == PHASE_INITIALISATION .or. phase_post == PHASE_CALCUL ) then
      if( ErreurNumero /= 0 ) then
         stop
      endif
   endif

   if( phase_post_casier == PHASE_INITIALISATION .or. phase_post_casier == PHASE_CALCUL ) then

      call POST_CASIER ( &
                TEMPS   )

   end if

   if( phase_post_imp == PHASE_INITIALISATION .or. phase_post_imp == PHASE_CALCUL ) then

      Q  = Q1 + Q2

      call     POST_IMP ( &
                                 )
      if( ErreurNumero /= 0 ) then
         stop
      endif

   endif

   if( phase_post_imp_casier == PHASE_INITIALISATION .or. phase_post_imp_casier == PHASE_CALCUL ) then

      call POST_IMP_CASIER(              &
                   Erreur                 )

      if( ErreurNumero /= 0 ) then
         stop
      endif

   end if

   if( phase_stock == PHASE_INITIALISATION .or. phase_stock == PHASE_CALCUL ) then

      Q  = Q1+ Q2

      call STOCK       ( X               , &
                         Erreur          )
      if (ErreurNumero /= 0) then
         stop
      endif
   endif

   if( OptionTracer ) then
      if( (Phase_tracer == PHASE_INITIALISATION ).OR.( Phase_tracer == PHASE_CALCUL ) ) then
         call QCL_TRACER( &
                 Erreur )

         do i = 1 , nb_sect
            QT(i) = Q1(i) + Q2(i)
         enddo

         if( Temps.le.( TempsInitial + DT ) ) then
            do i = 1 , nb_sect
               QT_ANT(i) = QT(i)
            enddo
         endif

         call TRACER(                  &
                             ERREUR )

         do i = 1 , nb_sect
            BT_ANT(i) = BT(i)
         enddo

         if( phase_post_imp == PHASE_INITIALISATION .or. phase_post_imp == PHASE_CALCUL ) then
            call POST_IMP_TRACER( &
                         Erreur )
               if( ErreurNumero /= 0 ) then
                  stop
               endif
         endif

         if( phase_stock == PHASE_INITIALISATION .or. phase_stock == PHASE_CALCUL ) then

            VarStoTracer(VARTR_A)    = .true.
         endif
      endif
   endif
   num_pas = num_pas + 1
   select case( PhaseSimulation )
      case( PHASE_INITIALISATION )
         PhaseSimulation = PHASE_CALCUL
      case( PHASE_TERMINAISON )
         If( Noyau == NOYAU_MASCARET ) then

         endif

         exit

      case( PHASE_CALCUL )
         if( .true. ) then
            WRITE (6,*) 'FIN CORRECTE DU CALCUL'
         endif
   end select

   end do 

end program SYSTEME
