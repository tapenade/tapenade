!! a tester en changeant ad/allDiff/FlowGraphDifferentiator.java
!! dans differentiateProcedureInMode
!! adEnv.curUnitIsContext = true;
!! 
!! avec -b -nooptim activity

  REAL(KIND=8) FUNCTION RETARD(CK,I_ZONE,SP,TEMPS,IFIM1)
   INTEGER:: I_ZONE,SP
  INTEGER, OPTIONAL ::IFIM1
   REAL(KIND=8):: CK,SORP,EPSI
  REAL(KIND=8), OPTIONAL ::TEMPS
   EPSI=1.0D-14
   RETARD=CK*I_ZONE
 END FUNCTION

 SUBROUTINE COMP_PRECIPITATION(CK,DELTAT)
  REAL(KIND=8) :: DELTAT, CK(2,2), RN
  INTEGER:: SP, I_ZONE
     RN = RETARD(CK(1,1),I_ZONE,SP)
 END SUBROUTINE
