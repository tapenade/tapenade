module M
   implicit none
   integer, parameter :: n = 2
   real :: global
end module M

module M1_I
   interface
      function compute (x,y)
       use M
       implicit none
       real, dimension(n) :: x,y
       real :: compute
      end function compute
   end interface
end module M1_I

function compute(x,y)
  use M
  implicit none
  real, dimension(n) :: x,y
  real :: compute
  y = 2 * x
  compute = y(1) * y(2)
  global = global + compute
end function compute

module M2_I
   interface
      function ftest(r,s,compute)
         use M
         use M1_I
         implicit none
         real, dimension(n) :: r,s
         real:: ftest, compute
      end function ftest
   end interface
end module M2_I

function ftest(r,s,compute)
  use M
  use M1_I
  implicit none
  real, dimension(n) :: r,s
  real ftest
  interface
      function compute (x,y)
       use M
       implicit none
       real, dimension(n) :: x,y
       real :: compute
      end function compute
  end interface
  ftest = compute(r,s)
end function ftest

function top(r,s)
use M
implicit none
real, dimension(n) :: r,s
real top
  interface
      function compute (x,y)
       use M
       implicit none
       real, dimension(n) :: x,y
       real :: compute
      end function compute
  end interface

real, external :: ftest
top = ftest(r,s,compute)
end function top


program prog
use M
use M1_I
use M2_I
implicit none
real, dimension(n) :: r,s
r(1) = 3.0
s(1) = 0.0
r(2) = 2.0
s(2) = 0.0
print*,ftest(r,s,compute)
!! print*,compute(r,s) avec ca change les interfaces differentiees
end program prog
