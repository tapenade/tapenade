 program  test


 call TimeLoop()  


end program test
    

subroutine TimeLoop(k)
   integer n,i
   REAL*8,allocatable,dimension(:)       :: a
   n=10
   allocate(a(n))
  do i=1,SIZE(a)
     a(i)=2*i*k
  end do

   call f(a)

   deallocate(a)

   n=20
  allocate(a(n))
  do i=1,SIZE(a)
    a(i)=i
  end do
    call g(a)

  do i=1,SIZE(a)
     a(i)=2*i*k*a(i)
  end do


 end subroutine TimeLoop

subroutine f(a )

  REAL*8,allocatable,dimension(:)       :: a
  integer i
    
  do i=1,SIZE(a)
     a(i)=2*a(i)+4
  end do
   
  endsubroutine  f
  
  subroutine g(a )

    REAL*8,allocatable,dimension(:)       :: a
    integer i
    
    do i=1,SIZE(a)
       a(i)=2*a(i)+3
    end do
  end  subroutine g
