module example3
 implicit none

 type, private :: vector
     real :: x,y
 end type vector

 type(vector) :: u,v,w
 private addvector

contains
 function addvector(a,b)
  type(vector), intent(in) :: a,b
  type(vector) :: addvector
  addvector%x = a%x + b%x
 end function addvector

end module
