MODULE TEST
  IMPLICIT NONE
  private
  character(len=*), parameter :: FoX_version = '4.1.2'
  public :: FoX_version
  public :: operator(//)
  public :: operator(.add.)
  public :: foo
END MODULE TEST
