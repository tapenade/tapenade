MODULE flincom
  INTEGER, PARAMETER :: nbfile_max = 200
  INTEGER, SAVE :: nbfiles_flincom = 0
  INTEGER, SAVE :: ncids(nbfile_max), ncnbd(nbfile_max), &
                   ncfunli(nbfile_max), ncnba(nbfile_max)
  INTEGER, SAVE :: ncnbva(nbfile_max), ncdims(nbfile_max,4)
  LOGICAL, SAVE :: ncfileopen(nbfile_max)=.FALSE.
  INTEGER, SAVE :: cind_vid, cind_fid, cind_len
  INTEGER,DIMENSION(:),ALLOCATABLE,SAVE :: cindex
  INTEGER,DIMENSION(4) :: w_sta, w_len, w_dim
  REAL,DIMENSION(:),ALLOCATABLE,SAVE :: var_tmp
  REAL,DIMENSION(:),ALLOCATABLE,SAVE :: buff_tmp_flinget_r3d_zoom2d
  REAL,DIMENSION(:),ALLOCATABLE,SAVE :: buff_tmp_flinget_r2d_zoom2d
CONTAINS

SUBROUTINE flinopen_work &
  (filename, iideb, iilen, jjdeb, jjlen, do_test, &
   iim, jjm, llm, lon, lat, lev, ttm, itaus, date0, dt, fid_out)
!  IMPLICIT NONE
  CHARACTER(LEN=*) :: filename
  LOGICAL :: do_test
  INTEGER :: iim, jjm, llm, ttm, iideb, iilen, jjdeb, jjlen
  REAL :: lon(iilen,jjlen), lat(iilen,jjlen), lev(llm)
  INTEGER :: itaus(ttm)
  REAL :: date0, dt
  INTEGER :: fid_out
  REAL, PARAMETER :: eps = 1.e-4
  INTEGER :: iret, vid, fid, nbdim, i, iilast, jjlast
  INTEGER :: it, len, gdtt_id, old_id, iv, gdtmaf_id
  CHARACTER(LEN=250) :: name
  CHARACTER(LEN=80) :: units, calendar
  INTEGER :: tmp_iim, tmp_jjm, tmp_llm, tmp_ttm
  REAL :: x_first, x_last
  INTEGER :: year, month, day
  REAL :: r_year, r_month, r_day
  INTEGER :: year0, month0, day0, hours0, minutes0, seci
  REAL :: sec, sec0
  CHARACTER :: strc
  REAL,DIMENSION(:),ALLOCATABLE :: vec_tmp
  LOGICAL :: open_file
  LOGICAL :: check = .FALSE.
  iilast = iideb+iilen-1
  IF (ttm > 0) THEN
    gdtmaf_id = -1
    DO iv=1,ncnbva(fid_out)
      name=''
!$AD DO-NOT-DIFF
      iret = NF90_INQUIRE_VARIABLE (fid, iv, name)
      units=''
      iret = NF90_GET_ATT (fid, iv, 'units', units)
!$AD END-DO-NOT-DIFF
      IF (INDEX(units,'seconds since') > 0) gdtmaf_id = iv
    ENDDO
    ALLOCATE(vec_tmp(ttm))
    iret = NF90_GET_VAR (fid, vid, vec_tmp, &
              1 ,  ttm )
    itaus(1:ttm) = NINT(vec_tmp(1:ttm))
  ENDIF
END SUBROUTINE flinopen_work
END MODULE flincom
