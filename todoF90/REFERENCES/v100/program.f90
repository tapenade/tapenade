! from OpenAD/Regression/testRoundTrip/examples/mod_f_arg
	subroutine head(x,y) 
	  double precision, dimension(1) :: x
	  double precision, dimension(1) :: y
	  x(1)=x(1)*10
          y(1)=mod(x(1),2.0D0)
	end subroutine
