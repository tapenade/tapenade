program main
 use mpi
 implicit none
 integer :: nb_procs,rang,code
 real :: val, valeur
 integer, parameter :: n = 3
 real, dimension(n) :: ce, ceb

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 if (nb_procs < 3) then
   print *, '!!!!! mpiexec -n 3 ou plus'
 end if

 ce(1) = 1
 ce(2) = 2
 ce(3) = 3

 ceb(1) = 10
 ceb(2) = 20
 ceb(3) = 30

 if (rang == 0) then
   call test_b(ce, ceb, rang)
 end if
 if (rang == 1) then
   call test_b(ce, ceb, rang)
 end if


 print *,'Moi, proc. ',rang,', j''ai recu   ',ce(1), ce(2), ce(3)
 print *,'Moi, proc. ',rang,', j''ai recu d ',ceb(1), ceb(2), ceb(3)

 call MPI_FINALIZE (code)
end program main
