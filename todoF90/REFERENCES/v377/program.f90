program main
 use mpi
 implicit none
 integer :: nb_procs,rang,code
 real :: val, valeur
 integer, parameter :: n = 3
 real, dimension(n) :: ce

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

 if (nb_procs < 3) then
   print *, '!!!!! mpiexec -n 3 ou plus'
 end if

 ce(1) = 1
 ce(2) = 2
 ce(3) = 3
 if (rang == 0) then
   call test(ce, rang)
 end if
 if (rang == 1) then
   call test(ce, rang)
 end if


 print *,'Moi, proc. ',rang,', j''ai recu ',ce(1), ce(2), ce(3)

 call MPI_FINALIZE (code)
end program main



subroutine test(ce, rank)
 use mpi
 implicit none
 integer, parameter :: n = 3
 real, dimension(n) :: ce
 real, dimension(n) :: rbuf, sbuf
 integer :: i, rank
 integer, dimension(n) :: node
 integer, dimension(n) :: rpid
 integer, dimension(n) :: req
 integer :: ierr
 integer, dimension(MPI_STATUS_SIZE) :: istat, statut

 node(1) = 0
 node(2) = 1
 node(3) = 2
 rpid(1) = 1
 rpid(2) = 1
 rpid(3) = 1

 do i=1,n
   call MPI_irecv(rbuf(i), 1, MPI_REAL, rank-1, rpid(i), MPI_COMM_WORLD, req(i), ierr)
 end do
 do i=1,n
   sbuf(i) = ce(i)
   call MPI_send(sbuf(i), 1, MPI_REAL, rank+1,  rpid(i), MPI_COMM_WORLD, statut,  ierr)
 end do
 do i=1,n
   call MPI_wait(req(i), istat, ierr)
 end do
 do i=1,n
   ce(i) = rbuf(i)
 end do
end subroutine test
