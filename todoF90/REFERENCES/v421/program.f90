module test 
contains
subroutine g(z,u)
real, dimension(2) :: u
real z
 print *,'u1',  u(1)
 z = u(1) * u(1)
 u(2) = z
 print *,'z',  z
end subroutine

subroutine top(x,y)
real x,y
call g(x, u=(/y/))
end subroutine top
end module

program main
 use test
 real x,y
 y = 2
 call top(x,y)
 print *,'x',  x
end program main
