module m
    real, dimension(:), allocatable :: someTData
    integer, dimension(:), allocatable :: i
contains
    subroutine setupData(dim)
      integer :: dim
      if (allocated(someTData)) deallocate(someTData)
      allocate(someTData(dim))
      allocate(i(dim))
    end subroutine
end module m
