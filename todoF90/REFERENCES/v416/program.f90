  subroutine precechcin(x,y,nm_ha,Tm_ha)
  !
  implicit none 
  external solveTsys
  !
  real, dimension(nm_ha,nm_ha) :: MC_ha(nm_ha,nm_ha)
  real :: x,y, Tm_ha, solveTsys
  integer :: nm_ha, kp,j_ha
         MC_ha = 0.
          do kp = 1, 2
            MC_ha(1,2) = 1 / 5 / 2.
            do j_ha = 1, nm_ha-1
              MC_ha(j_ha,j_ha) = 1.
              MC_ha(j_ha,j_ha+1) = 1. / 2. / Tm_ha
              MC_ha(j_ha+1,j_ha) = - 1. / 2. / Tm_ha
            end do
            MC_ha(nm_ha,nm_ha) = 1. + 1. / Tm_ha
            MC_ha(nm_ha,nm_ha-1) = - 1. / Tm_ha
            !
          end do
          !
  y=x*x*MC_ha(1,1)
  end subroutine precechcin
