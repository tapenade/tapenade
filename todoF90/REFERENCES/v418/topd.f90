program topd
 use mpi
 use aatypes
 implicit none
 integer, dimension( MPI_STATUS_SIZE ) :: statut
 integer, parameter :: etiquette=100
 integer :: nb_procs,rang, &
 num_proc_precedent,num_proc_suivant,code
 TYPE(REAL4_RD) :: vald, valeurd

 integer :: oldtypes(0:0), blockcounts(0:0), offsets(0:0), extent, ierr

 call MPI_INIT (code)
 call MPI_COMM_SIZE ( MPI_COMM_WORLD ,nb_procs,code)
 call MPI_COMM_RANK ( MPI_COMM_WORLD ,rang,code)

!  Setup description of the 2 MPI_REAL fields of real4_rd
 offsets(0) = 0 
 oldtypes(0) = MPI_REAL 
 blockcounts(0) = 2
 call MPI_TYPE_EXTENT(MPI_REAL, extent, ierr) 
!  Now define structured type and commit it  
 call MPI_TYPE_STRUCT(1, blockcounts, offsets, oldtypes, real4rdtype, ierr) 
 call MPI_TYPE_COMMIT(real4rdtype, ierr) 

 num_proc_suivant=mod(rang+1,nb_procs)
 num_proc_precedent=mod(nb_procs+rang-1,nb_procs)

 vald%v = rang+1000
 vald%d = rang+2000

 if (rang == 0) then
   call msg1_rd(vald, valeurd, num_proc_precedent,num_proc_suivant,etiquette, code)
 else
   call msg2_rd(vald, valeurd, num_proc_precedent,num_proc_suivant,etiquette, code)
 end if

 print *,'Moi, proc. ',rang,', j''ai recu valeur ',valeurd%v,' du proc. ',num_proc_precedent
 print *,'et , proc. ',rang,', j''ai recu valeur ',valeurd%d,' du proc. ',num_proc_precedent

 call MPI_FINALIZE (code)
end program topd

