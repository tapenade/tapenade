! from huneeus-lidar
SUBROUTINE nl_model_mie_orig(alpha_ext,PP)
IMPLICIT NONE

integer, parameter :: nmax_dim=110, mmax=18, mopt=5
real, dimension(2) :: M_conc_obs, M_conc_bgd
real :: lambda(1:3)
real, dimension(2,2) :: lid_sig_obs, lid_sig_bgd
real, dimension(6) :: rad_obs, rad_bgd, R_rad
real, dimension(2,2) :: R_lid
real :: r0_obs, r0_bgd, B_r0, B_conc

REAL rho
PARAMETER (rho=1.769E3)  !--kg/m3
                
REAL x  !--size parameter
REAL r  !--radius
INTEGER Nmu, Nmumax
PARAMETER (Nmumax=1)
REAL angle(0:Nmumax)
DATA angle/120., 180./                                
INTEGER bin, Nbin
PARAMETER (Nbin=10000)
INTEGER Nmaxim,Nstart !--number of iterations
COMPLEX k2, k3, z1, z2
COMPLEX u1,u5,u6,u8
COMPLEX a(1:21000), b(1:21000)
COMPLEX I 
COMPLEX s1, s2
INTEGER n  !--loop index
REAL pi, nnn, ss, tt, mu, rho1, rho2 
COMPLEX nn
REAL Q_ext, Q_abs, g, omega   !--parameters for radius r
DOUBLE PRECISION sigma_sca(1:2), sigma_ext(1:2), masse  !--averaged parameters
DOUBLE PRECISION ext_ratio(1:2), alpha_ext(1:2)
DOUBLE PRECISION PP(0:Nmumax,1:2)
INTEGER lda, lev
DOUBLE PRECISION number(0:Nbin)
REAL deltar, phase(0:Nmumax)

DO lda=1,2
pi=4.*ATAN(1.)
I=CMPLX(0.,1.)
masse=0.0
sigma_sca(lda)=0.0
sigma_ext(lda)=0.0
DO Nmu=0, Nmumax
   PP(Nmu,lda)=0.0
ENDDO
                      
DO bin=0, Nbin
r=2.0
deltar=1./(1.*Nbin)
masse=masse+4./3.*pi*(r**3)*deltar*rho*1.E3 !--g/m3
x = 3.0
k3=CMPLX(1.0,0.0)
z2=CMPLX(x,0.0)
IF (x.LE.8.) THEN
Nmaxim=INT(x+4*x**(1./3.)+1.)+1
ELSEIF (8..LT.x.AND.x.LT.4200.) THEN
Nmaxim=INT(x+4.05*x**(1./3.)+2.)+1
ELSEIF (4200..LE.x.AND.x.LE.20000.) THEN
Nmaxim=INT(x+4*x**(1./3.)+2.)+1
ELSE
print *,'Non Lineal model'
STOP
ENDIF

Nstart=Nmaxim+10
sigma_ext(lda)=sigma_ext(lda) + r*2.0

DO Nmu=0, Nmumax
mu = COS(angle(Nmu)/180.*pi)

ENDDO   !---Nmu
ENDDO   !---bin
alpha_ext(lda)=sigma_ext(lda)/masse

DO Nmu=0, Nmumax
   PP(Nmu,lda)=pi/sigma_sca(lda)*PP(Nmu,lda)
ENDDO
ENDDO  !--lda

END  SUBROUTINE nl_model_mie_orig
