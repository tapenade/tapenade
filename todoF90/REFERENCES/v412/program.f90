      double precision function f0(t1,t2,t3)
      double precision t1,t2,t3
      f0 = t1 + t2
      return
      end function

      double precision function f1(t1,t2,t3)
      double precision t1,t2,t3
      f1 = t1 * t2
      return
      end function

      double precision function f2(t1,t2,t3)
      double precision t1,t2,t3
      f2 = t1 / t2
      return
      end function

      double precision function f3(t1,t2)
      double precision t1,t2
      f3 = t1 ** t2
      return
      end function

      double precision function f4(t)
      double precision t
      f4 = exp(t)
      return
      end function

      double precision function f5(t)
      double precision t, undef
      f5 = undef(t)
      return
      end function

      real function f6(t)
      real t
      f6 = exp(t)
      return
      end function

      real function f7(t)
      real t, unused
      unused = exp(t)
      return
      end function


      subroutine top(x,y)
      double precision x,y
      double precision z
      y = f0(x,y,z) + f1(x,y,z) + f2(x,y,z) + f3(x,y) + f4(x) + f5(y)
      y = y + f6(x)
      y = y + f7(x)
      return
      end subroutine
