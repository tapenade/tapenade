module FoX_dom_types

  type DOMConfiguration
    private
    integer :: parameters = 1024
    ! FIXME make sure this is 32 bit at least.
  end type DOMConfiguration

  type DOMImplementation
    private
    character(len=7) :: id = "FoX_DOM"
    logical :: FoX_checks = .true. ! Do extra checks not mandated by DOM
  end type DOMImplementation

  type ListNode
    private
    type(Node), pointer :: this => null()
  end type ListNode 

  type NodeList
    private
    character, pointer :: nodeName(:) => null() ! What was getByTagName run on?
    character, pointer :: localName(:) => null() ! What was getByTagNameNS run on?
    character, pointer :: namespaceURI(:) => null() ! What was getByTagNameNS run on?
    type(Node), pointer :: element => null() ! which element or document was the getByTagName run from?
    type(ListNode), pointer :: nodes(:) => null()
    integer :: length = 0
  end type NodeList

  type NodeListptr
    private
    type(NodeList), pointer :: this
  end type NodeListptr

  type NamedNodeMap
    private
    logical :: readonly = .false.
    type(Node), pointer :: ownerElement => null()
    type(ListNode), pointer :: nodes(:) => null()
    integer :: length = 0
  end type NamedNodeMap

  type documentExtras
    type(DOMImplementation), pointer :: implementation => null() ! only for doctype
    type(Node), pointer :: docType => null()
    type(Node), pointer :: documentElement => null()
    character, pointer :: inputEncoding(:) => null()
    character, pointer :: xmlEncoding(:) => null()
    type(NodeListPtr), pointer :: nodelists(:) => null() ! document
    ! In order to keep track of all nodes not connected to the document
    logical :: liveNodeLists ! For the document, are nodelists live?
    type(NodeList) :: hangingNodes ! For the document, list of nodes not associated with doc
    type(xml_doc_state), pointer :: xds => null()
    logical :: strictErrorChecking = .true.
    logical :: brokenNS = .false. ! FIXME consolidate these logical variables into bitmask
    type(DOMConfiguration), pointer :: domConfig => null()
  end type documentExtras

  type elementOrAttributeExtras
    logical :: isId = .false.
  end type elementOrAttributeExtras

  type docTypeExtras
    logical :: illFormed = .false.
  end type docTypeExtras

  type Node
    private
    logical :: readonly = .false.
    character, pointer, dimension(:)         :: nodeName => null()
    character, pointer, dimension(:)         :: nodeValue => null()
    integer             :: nodeType        = 0
    type(Node), pointer :: parentNode      => null()
    type(Node), pointer :: firstChild      => null()
    type(Node), pointer :: lastChild       => null()
    type(Node), pointer :: previousSibling => null()
    type(Node), pointer :: nextSibling     => null()
    type(Node), pointer :: ownerDocument   => null()
    type(NodeList) :: childNodes ! not for text, cdata, PI, comment, notation, docType, XPath
    logical :: inDocument = .false.! For a node, is this node associated to the doc?
    logical :: ignorableWhitespace = .false. ! Text nodes only
    type(documentExtras), pointer :: docExtras => null()
    type(elementOrAttributeExtras), pointer :: elExtras => null()
    type(docTypeExtras), pointer :: dtdExtras => null()
    integer :: textContentLength = 0
  end type Node

  type error_t
    integer :: severity = 1
    integer :: error_code = 0
    character, dimension(:), pointer :: msg => null()
  end type error_t

  type error_stack
    type(error_t), dimension(:), pointer :: stack => null()
  end type error_stack

  type DOMException
    private
    type(error_stack) :: stack
  end type DOMException

  type URI
    private
    integer :: i
  end type URI

  type notation
    character(len=1), dimension(1) :: name
  end type notation

  type notation_list
    type(notation), dimension(:), pointer :: list
  end type notation_list

  type element_t
    logical :: empty = .false.
  end type element_t

  type element_list
    type(element_t), pointer :: list(:) => null()
  end type element_list

  type xml_doc_state
    logical :: building = .false. 
  end type xml_doc_state

  type dict_item
    character(len=1), pointer, dimension(:) :: nsURI => null()
    character(len=1), pointer, dimension(:) :: localName => null()
    character(len=1), pointer, dimension(:) :: prefix => null()
    character(len=1), pointer, dimension(:) :: key => null()
    character(len=1), pointer, dimension(:) :: value => null()
    logical :: specified = .true.
    logical :: declared = .false.
    logical :: isId = .false.
    integer :: type = 11
  end type dict_item

  TYPE DICT_ITEM_PTR
      TYPE(DICT_ITEM), POINTER :: d => NULL()
  END TYPE DICT_ITEM_PTR

  type dictionary_t
    private
    type(dict_item_ptr), dimension(:), pointer :: list => null()
    character, dimension(:), pointer           :: base => null()
    integer :: i
  end type dictionary_t

end module FoX_dom_types

module FoX_dom
  use FoX_dom_types

  interface item
    module procedure item_nl
    module procedure item_nnm
  end interface

  interface destroy
    module procedure destroy_dict_item
    module procedure destroy_dict
  end interface

  interface extractDataContent
     module procedure extractDataContentCmplxDpSca
     module procedure extractDataContentCmplxDpArr
     module procedure extractDataContentCmplxDpMat
     module procedure extractDataContentCmplxSpSca
     module procedure extractDataContentCmplxSpArr
     module procedure extractDataContentCmplxSpMat
     module procedure extractDataContentRealDpSca
     module procedure extractDataContentRealDpArr
     module procedure extractDataContentRealDpMat
     module procedure extractDataContentRealSpSca
     module procedure extractDataContentRealSpArr
     module procedure extractDataContentRealSpMat
     module procedure extractDataContentIntSca
     module procedure extractDataContentIntArr
     module procedure extractDataContentIntMat
     module procedure extractDataContentLgSca
     module procedure extractDataContentLgArr
     module procedure extractDataContentLgMat
     module procedure extractDataContentChSca
     module procedure extractDataContentChArr
     module procedure extractDataContentChMat
  end interface extractDataContent

contains

  function item_nl(list, index) result (np) 
    use FoX_dom_types
    type(NodeList), pointer :: list
    integer, intent(in) :: index
    type(Node), pointer :: np
  end function item_nl

  function item_nnm(map, index)result(np) 
    type(NamedNodeMap), pointer :: map
    integer, intent(in) :: index
    type(Node), pointer :: np
  end function item_nnm

  function getElementsByTagName(doc, tagName, name, ex)result(list) 
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: doc
    character(len=*), intent(in), optional :: tagName, name
    type(NodeList), pointer :: list
  end function getElementsByTagName

  function getTextContent(arg, ex)result(c) 
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=78) :: c
  end function getTextContent

  function parsefile(filename, configuration, iostat, ex) 
    type(DOMException), intent(out), optional :: ex
    character(len=*), intent(in) :: filename
    type(DOMConfiguration), pointer, optional :: configuration
    integer, intent(out), optional :: iostat
    type(Node), pointer :: parsefile
  end function parsefile

  function getExceptionCode(ex) result(n)
    type(DOMException), intent(inout) :: ex
    integer :: n
  end function getExceptionCode

  function newDOMConfig() result(dc)
    type(DOMConfiguration), pointer :: dc
    allocate(dc)
  end function newDOMConfig

  function getdocumentElement(np, ex)result(c) 
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: np
    type(Node), pointer :: c
  end function getdocumentElement

  recursive subroutine setParameter(domConfig, name, value, ex)
    type(DOMException), intent(out), optional :: ex
    type(DOMConfiguration), pointer :: domConfig
    character(len=*), intent(in) :: name
    logical, intent(in) :: value
    integer :: i, n
  end subroutine setParameter

  function inException(ex) result(p)
    type(DOMException), intent(in) :: ex
    logical :: p
  end function inException

  subroutine destroy_dict_item(d)
    type(dict_item), pointer :: d
  end subroutine destroy_dict_item

  subroutine destroy_dict(dict)
    type(dictionary_t), intent(inout) :: dict
    integer :: i
  end subroutine destroy_dict


subroutine extractDataContentCmplxDpSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(dp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxDpSca

subroutine extractDataContentCmplxSpSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(sp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxSpSca

subroutine extractDataContentRealDpSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(dp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealDpSca

subroutine extractDataContentRealSpSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(sp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealSpSca

subroutine extractDataContentIntSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    integer, intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentIntSca

subroutine extractDataContentLgSca(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    logical, intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentLgSca

subroutine extractDataContentChSca(arg, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentChSca


subroutine extractDataContentCmplxDpArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(dp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxDpArr

subroutine extractDataContentCmplxSpArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(sp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxSpArr

subroutine extractDataContentRealDpArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(dp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealDpArr

subroutine extractDataContentRealSpArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(sp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealSpArr

subroutine extractDataContentIntArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    integer, dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentIntArr

subroutine extractDataContentLgArr(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    logical, dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentLgArr

subroutine extractDataContentChArr(arg, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), dimension(:), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentChArr


subroutine extractDataContentCmplxDpMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(dp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxDpMat

subroutine extractDataContentCmplxSpMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    complex(sp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentCmplxSpMat

subroutine extractDataContentRealDpMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(dp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealDpMat

subroutine extractDataContentRealSpMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    real(sp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentRealSpMat

subroutine extractDataContentIntMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    integer, dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentIntMat

subroutine extractDataContentLgMat(arg, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    logical, dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentLgMat

subroutine extractDataContentChMat(arg, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), dimension(:,:), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataContentChMat


subroutine extractDataAttributeCmplxDpSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(dp), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxDpSca

subroutine extractDataAttributeCmplxSpSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(sp), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxSpSca

subroutine extractDataAttributeRealDpSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(dp), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealDpSca

subroutine extractDataAttributeRealSpSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(sp), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealSpSca

subroutine extractDataAttributeIntSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    integer, intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeIntSca

subroutine extractDataAttributeLgSca(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeLgSca

subroutine extractDataAttributeChSca(arg, name, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    character(len=*), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeChSca


subroutine extractDataAttributeCmplxDpArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(dp), dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxDpArr

subroutine extractDataAttributeCmplxSpArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(sp), dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxSpArr

subroutine extractDataAttributeRealDpArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(dp), dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealDpArr

subroutine extractDataAttributeRealSpArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(sp), dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealSpArr

subroutine extractDataAttributeIntArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    integer, dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat


  end subroutine extractDataAttributeIntArr

subroutine extractDataAttributeLgArr(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeLgArr

subroutine extractDataAttributeChArr(arg, name, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    character(len=*), dimension(:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeChArr


subroutine extractDataAttributeCmplxDpMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(dp), dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxDpMat

subroutine extractDataAttributeCmplxSpMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    complex(sp), dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeCmplxSpMat

subroutine extractDataAttributeRealDpMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(dp), dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealDpMat

subroutine extractDataAttributeRealSpMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    real(sp), dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeRealSpMat

subroutine extractDataAttributeIntMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    integer, dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeIntMat

subroutine extractDataAttributeLgMat(arg, name, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeLgMat

subroutine extractDataAttributeChMat(arg, name, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: name

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    character(len=*), dimension(:,:), intent(out) :: data
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttributeChMat


subroutine extractDataAttNSCmplxDpSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(dp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxDpSca

subroutine extractDataAttNSCmplxSpSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(sp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxSpSca

subroutine extractDataAttNSRealDpSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(dp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSRealDpSca

subroutine extractDataAttNSRealSpSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(sp), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSRealSpSca

subroutine extractDataAttNSIntSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    integer, intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSIntSca

subroutine extractDataAttNSLgSca(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    logical, intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSLgSca

subroutine extractDataAttNSChSca(arg, namespaceURI, localName, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    character(len=*), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSChSca


subroutine extractDataAttNSCmplxDpArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(dp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxDpArr

subroutine extractDataAttNSCmplxSpArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(sp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxSpArr

subroutine extractDataAttNSRealDpArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(dp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSRealDpArr

subroutine extractDataAttNSRealSpArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(sp), dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSRealSpArr

subroutine extractDataAttNSIntArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    integer, dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSIntArr

subroutine extractDataAttNSLgArr(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    logical, dimension(:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSLgArr

subroutine extractDataAttNSChArr(arg, namespaceURI, localName, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    character(len=*), dimension(:), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSChArr


subroutine extractDataAttNSCmplxDpMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(dp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxDpMat

subroutine extractDataAttNSCmplxSpMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    complex(sp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSCmplxSpMat

subroutine extractDataAttNSRealDpMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(dp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat


  end subroutine extractDataAttNSRealDpMat

subroutine extractDataAttNSRealSpMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    real(sp), dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSRealSpMat

subroutine extractDataAttNSIntMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    integer, dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSIntMat

subroutine extractDataAttNSLgMat(arg, namespaceURI, localName, data, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    logical, dimension(:,:), intent(out) :: data

    integer, intent(out), optional :: num, iostat

  end subroutine extractDataAttNSLgMat

subroutine extractDataAttNSChMat(arg, namespaceURI, localName, data, separator, csv, num, iostat, ex)
    type(DOMException), intent(out), optional :: ex
    type(Node), pointer :: arg
    character(len=*), intent(in) :: namespaceURI, localName
    character(len=*), dimension(:,:), intent(out) :: data

    logical, intent(in), optional :: csv
    character, intent(in), optional :: separator
    integer, intent(out), optional :: num, iostat
  end subroutine extractDataAttNSChMat

end module FoX_dom

module FoX_sax
end module FoX_sax



MODULE M_PRECISION
   implicit none
   integer, parameter :: DOUBLE = SELECTED_REAL_KIND(15,100)
   integer, parameter :: SIMPLE = SELECTED_REAL_KIND( 6, 37)
end module M_PRECISION


module M_SINGULARITE_T
use M_PRECISION

! Constantes reperant le type de singularite

integer, parameter :: SINGULARITE_TYPE_ZAMONT_ZAVAL_Q = 1
integer, parameter :: SINGULARITE_TYPE_ZAMONT_Q       = 2
integer, parameter :: SINGULARITE_TYPE_PROFIL_CRETE   = 3
integer, parameter :: SINGULARITE_TYPE_CRETE_COEFF    = 4
integer, parameter :: SINGULARITE_TYPE_Z_T            = 5
integer, parameter :: SINGULARITE_TYPE_Q_ZAMONT       = 6
integer, parameter :: SINGULARITE_TYPE_Q_ZAVAL        = 7
integer, parameter :: SINGULARITE_TYPE_VANNE          = 8

integer, parameter :: SINGULARITE_TYPE_NB_MAX         = 8


TYPE SINGULARITE_T
  sequence
  character(30) :: Nom          ! Nom du seuil ou du barrage
  integer       :: Numero       ! Numero du seuil
  integer       :: NumBranche   ! Numero de la branche
  real(DOUBLE)  :: AbscisseRel  ! Abscisse relative
  integer       :: Section      ! Numero de la section
  integer       :: Type         ! Type
  logical       :: EtatEfface   ! Etat efface
  Integer       :: Epaisseur_seuil ! Seuil epais ou mince (1 seuil epais)
  real(DOUBLE)  :: CoteCrete    ! Cote de crete
  real(DOUBLE)  :: CoeffDebit   ! Coefficient de debit
  real(DOUBLE)  :: CoteRupture  ! Cote de rupture
  real (DOUBLE) :: Pente        ! Pente d'ouverture de la breche
  real (DOUBLE) :: Debit        ! Debit constant a travers la singularite
  real(DOUBLE)  :: LargeurVanne ! Largeur de la vanne
  integer       :: NumeroLoi    ! Numero de la loi

  real(DOUBLE), dimension(:)  , pointer :: PtZ      => null() ! Points Zamont de la loi
  real(DOUBLE), dimension(:)  , pointer :: PtQ      => null() ! Points Q de la loi
  real(DOUBLE), dimension(:,:), pointer :: PtZamont => null() ! Points Zamont de la loi
  real(DOUBLE), dimension(:)  , pointer :: PtZaval  => null() ! Points Zaval de la loi
  real(DOUBLE)                          :: PtZsup   ! Cote sup de la vanne
  real(DOUBLE)                          :: PtZinf   ! Cote inf de la vanne
  real(DOUBLE), dimension(:)  , pointer :: PtX      => null() ! Abscisses de la crete
  real(DOUBLE), dimension(:)  , pointer :: PtY      => null() ! Cotes     de la crete

end type SINGULARITE_T
end module M_SINGULARITE_T


module M_REZOMAT_T
use M_PRECISION

TYPE REZOMAT_T

  sequence

  integer                          :: SOLV          ! Choix du solveur lineaire : LAPACK-DGBSV (1) ou Y12M (2)
  integer                          :: N             ! Ordre de la matrice
  integer                          :: NNZ           ! Nombre d'elements non nuls de la matrice
  integer                          :: NN            ! dimension tableau de travail SNR de Y12M
  integer                          :: NN1           ! dimension tableau de travail RNR de Y12M
  integer                          :: IHA           ! dimension tableau de travail HA de Y12M
  integer                          :: KL            ! Largeur de demi-bande inferieure LAPACK
  integer                          :: KU            ! Largeur de demi-bande superieure LAPACK
  integer                          :: LDAB          ! Calcul d'un espace de travail LAPACK
  integer                          :: IFAIL         ! Diagnostic d'erreur
  integer , dimension(:) , pointer :: ipiv          => null()  ! Permutation numerique LAPACK
  integer , dimension(:) , pointer :: IFLAG         => null()  ! pilotage de Y12M
  integer , dimension(:) , pointer :: rowA          => null()  ! Indices de ligne de chaque element non nul
  integer , dimension(:) , pointer :: colA          => null()  ! Indices de colonne de chaque element non nul
  integer , dimension(:) , pointer :: snr           => null()  ! Indices de ligne de chaque element non nul
  integer , dimension(:) , pointer :: rnr           => null()  ! Indices de colonne de chaque element non nul
  integer , dimension(:,:),pointer :: ha            => null()  ! Tableau de travail  Y12M
  integer , dimension(:) , pointer :: noVarDQ       => null()  ! Table de correspondance avec le vecteur solution DQ
  integer , dimension(:) , pointer :: noVarDZ       => null()  ! Table de correspondance avec le vecteur solution DZ
  integer , dimension(:) , pointer :: noVarDQl      => null()  ! Table de correspondance avec le vecteur solution DQl dans les liaisons
  integer , dimension(:) , pointer :: noVarDZc      => null()  ! Table de correspondance avec le vecteur solution DZc dans les casiers
  integer , dimension(:) , pointer :: typSec        => null()  ! Typage des sections de calcul : 0 = quelconque , -1 = debit impose , -2 = cote imposee, >0 = confluent
  integer , dimension(:) , pointer :: headConflu    => null()  ! Pour chaque confluent, indique la premiere section de la liste avec signe + ou - suivant origine ou extremite
  integer , dimension(:) , pointer :: nextSecConflu => null() ! Section suivante de la liste avec signe + ou - suivant origine ou extremite
  integer , dimension(:) , pointer :: SecSin        => null() ! Pour chaque section, donne le numero de la singularite presente immediatement apres
  integer , dimension(:) , pointer :: SecLiai       => null() ! Pour chaque section donne le nombre des liaisons
  integer , dimension(:) , pointer :: LiaiSec       => null() ! Pour chaque liaison donne la section Amont renvoit 0 si la liaison est de type C-C
  real*8  , dimension(:) , pointer :: AFLAG         => null() ! parametres de calcul de Y12M
  real*8  , dimension(:) , pointer :: valA          => null() ! Valeurs des elements non nuls de la matrice
  real*8  , dimension(:) , pointer :: b             => null() ! Second membre du systeme d'equations lineaires
  real*8  , dimension(:) , pointer :: pivot         => null() ! Valeurs des pivots
  real*8  , dimension(:,:) , pointer :: AB          => null() ! Matrice bande LAPACK

END TYPE REZOMAT_T

end module M_REZOMAT_T


module M_SINGULARITE_REZO_I
interface

subroutine SINGULARITE_REZO( Matrice , Singularite , X)


  use M_REZOMAT_T           ! Definition du type REZOMAT_T
  use M_SINGULARITE_T       ! Definition du type SINGULARITE_T

  implicit none             ! Pas de type implicite par defaut


  type(REZOMAT_T) , intent(inout)                :: Matrice      ! Description de la matrice du probleme
  type (SINGULARITE_T), dimension(:), intent(in) :: Singularite  ! Tableau des singularite
  real(DOUBLE)    , dimension(:) , intent(in   ) :: X            ! Pour connaitre le nombre de section

end subroutine SINGULARITE_REZO

end interface
end module M_SINGULARITE_REZO_I

module M_SING3_I

interface

   subroutine  SING3( &
        DXP,DYP     , & ! Points X et Y de la crete
    Epaisseur_Seuil )   

     use M_PRECISION   

     real(DOUBLE), dimension(:), intent(inout   ) :: DXP, DYP
     Integer                   , intent(in   ) :: Epaisseur_Seuil
   end subroutine SING3

   end interface
end module M_SING3_I



subroutine SINGULARITE_REZO( Matrice , Singularite , X)
   use M_SINGULARITE_T       ! Definition du type SINGULARITE_T
   use M_REZOMAT_T
   use M_SING3_I
   use Fox_dom 

   implicit none             ! Pas de type implicite par defaut

   type(REZOMAT_T) , intent(inout)                :: Matrice      ! Description de la matrice du probleme
   type (SINGULARITE_T), dimension(:), intent(in) :: Singularite  ! Tableau des singularite
   real(DOUBLE)    , dimension(:) , intent(in   ) :: X            ! Pour connaitre le nombre de section
   integer retour   ! Code retour suite a erreur lors de l'allocation dynamique
   integer i        ! indice de boucle  
   type(Node), pointer :: champ4
   !
   ! Initialisations
   !

   Matrice%SecSin(:) = 0

   !
   ! Calcul de la table de correspondance
   !
   PASS0 : do i = 1 , size(Singularite)
      Matrice%SecSin(Singularite(I)%Section) = i
   end do PASS0

    call extractDataContent(champ4, Singularite(1)%Epaisseur_Seuil)


               call SING3                         ( &
                     Singularite(1)%PtX    , &
                     Singularite(1)%PtY    , &
            Singularite(1)%Epaisseur_Seuil)

   return

end subroutine SINGULARITE_REZO

MODULE MOD_FOR_SAVES
  USE M_REZOMAT_T
  TYPE(REZOMAT_T) :: matrice
END MODULE MOD_FOR_SAVES


subroutine  REZO    ( &
! Donnees / Resultats
    Z               , & ! Cote de la surface libre
    Q1, Q2          , & ! Debits mineur et majeur
    P1, P2          , & ! Perimetres mouilles mineur et majeur
    B1, B2, BS      , & ! Largeurs au miroir mineur, majeur et de stockage
    RH1, RH2        , & ! Rayons hydrauliques mineur et majeur
    S1, S2          , & ! Sections mouillee mineur et majeur
    DTLEVY          , & ! Pas de temps optimal
    Beta            , & ! Coefficient Beta de repartition des lits
    Froude          , & ! Nombre de Froude
    Qinjec          , & ! Debit injecte
    Qdeverse        , & ! debit total deverse par un deversoir lateral ponctuel ou lineique
    Temps           , & ! Temps
    PhaseSimulation , & ! Phase de la simulation
! Profils
! Modele
    X                , & ! Maillage
    CF1, CF2         , & ! Coefficients de frottement mineur et majeur
    ZREF             , & ! Cote de reference
    XDT              , & ! Position section/profil amont
    IDT              , & ! Numero du profil amont d'une section
    Singularite      , & ! Singularites
    PCSing           , & ! Pertes de charges singulieres
    ModeleLit        , & ! Modele du lit
    Abaque           , & ! Abaques des pertes de  charges aux confluences
! Parametres
    DTImpression     , & ! Pas de temps d'impression
    Impression       , & ! Flag d'autorisation d'impression
    UniteListing     , & ! Unite logique du fichier listing
    LoiFrottement    , & ! Loi de frottement
    PerteChargeConfluent,& ! Perte de charge automatique aux confluents
! Etat
    TempsPrecedent   , & ! Temps precedent
    Tinitial         , & ! Temps de debut de simulation
    NumeroPas        , & ! Numero du pas de temps
    DPDZ1, DPDZ2     , & ! Derivee de P1 et de P2 / Z
    OptionCasier     , & ! Flag de presence de casiers
    NoConvection     , & ! Attenuation de la convection
    CQMV                  )
   use M_PRECISION           ! Type DOUBLE
   use M_SINGULARITE_T       ! Definition du type SINGULARITE_T
   use MOD_FOR_SAVES
   use M_REZOMAT_T
   use M_SINGULARITE_REZO_I

   !.. Implicit Declarations
   implicit none

   !.. Arguments ..
   real(DOUBLE), dimension(:), intent(inout) :: Z, Q1, Q2
   real(DOUBLE), dimension(:), intent(inout) :: P1, P2
   real(DOUBLE), dimension(:), intent(inout) :: B1, B2, BS
   real(DOUBLE), dimension(:), intent(inout) :: RH1, RH2
   real(DOUBLE), dimension(:), intent(inout) :: S1, S2
   real(DOUBLE)              , intent(  out) :: DTLEVY
   real(DOUBLE), dimension(:), intent(  out) :: Beta
   real(DOUBLE), dimension(:), intent(  out) :: Froude
   ! Maillage
   real(DOUBLE), dimension(:), intent(in   ) :: X, CF1, CF2
   real(DOUBLE), dimension(:), intent(in   ) :: ZREF
   real(DOUBLE), dimension(:), intent(in   ) :: XDT
   integer     , dimension(:), intent(in   ) :: IDT
   ! Conditions aux limites

   ! Variables planimetrees


   ! Debits d apports

   ! Temps
   real(DOUBLE)                      , intent(in   ) :: Temps
   integer                           , intent(in   ) :: PhaseSimulation
   real(DOUBLE)                      , intent(in   ) :: DTImpression
   ! Pertes de charge singulieres
   real(DOUBLE)        , dimension(:), intent(inout) :: PCSing
   ! Confluences

   real(DOUBLE)    , dimension(:,:,:) , intent(in   ) :: Abaque
   ! Table de connectivite du reseau

   ! Singularites
   type (SINGULARITE_T), dimension(:), pointer , intent(inout) :: Singularite
   ! Parametres
   integer                           , intent(in   ) :: ModeleLit
   logical                           , intent(in   ) :: Impression
   integer                           , intent(in   ) :: UniteListing
   integer                           , intent(in   ) :: LoiFrottement
   logical                           , intent(in   ) :: PerteChargeConfluent
   ! Deversoirs

   real(DOUBLE)        , dimension(:), intent(inout) :: Qdeverse
   ! Etats
   real(DOUBLE)                      , intent(inout) :: Tinitial
   real(DOUBLE)                      , intent(inout) :: TempsPrecedent
   integer                           , intent(inout) :: NumeroPas
   real(DOUBLE)        , dimension(:), pointer       :: DPDZ1, DPDZ2
   ! Casier
   logical                           , intent(in   ) :: OptionCasier



   ! Flag calcul particulier
   logical                           , intent(in   ) :: NoConvection
   integer                           , intent(in   ) :: CQMV


   ! Tableaux de travail
   ! Debits d'apports generalise (avec deversoirs)
   real(DOUBLE), dimension(:)    ,intent (inout)     :: QINJEC
   real(DOUBLE), dimension(size(X))                  :: Q
   ! Coefficients de l'equation discretisee d'une singularites
   real(DOUBLE), dimension(:), allocatable :: ASING, BSING, CSING, DSING
   ! Debit passant sur cette singularite
   real(DOUBLE), dimension(:), allocatable :: QSING
   ! Coefficients de l'equation discretisee d'une cond a la limite
   ! RDLIM*DQ + SDLIM*DZ = TDLIM
   real(DOUBLE), dimension(2)   :: RDLIM, SDLIM, TDLIM
   !.. Scalairs locaux ..
   logical        :: impression_sing   ! Impression info sur singularites
   integer        :: I                 ! Compteur
   integer        :: ibief             ! Compteur sur les biefs
   integer        :: ising             ! Compteur sur les singularites
   integer        :: iliai             ! Compteur sur les liaisons
   integer        :: iext              ! Compteur sur les extremites libres
   integer        :: NumSeuil          ! Numero de seuil
   integer        :: NS                ! Numero de section de calcul
   integer        :: appel_KSING       ! phase d'appel de KSING (1 ou 2)
   integer        :: II1, II2
   real(DOUBLE)   :: Celerite
   real(DOUBLE)   :: DT, dt_lim
   real(DOUBLE)   :: HI, VI, v_amont_sing
   character(132) :: arbredappel_old   ! Arbre d'appel precedent l'entree du sous programme
   real(DOUBLE)   :: v_moyen           ! Vitesse moyenne
   real(DOUBLE)   :: debitance         ! Debitance
   integer        :: num_bief          ! Numero de bief
   real(DOUBLE)   :: abs_rel           ! abscisse relative sur un bief
   integer        :: retour            ! Code de retour des fonctions intrinseques
   integer        :: dim               ! Dimension des tableaux ASING...
   ! Integration des casiers :
   integer      :: dimLiaison
   integer      :: NumLiai, Appel_Kliaison
   real(DOUBLE) :: ZAM, ZAV, ZfAM, ZfAV
   real(DOUBLE)         , dimension(:)  , allocatable   :: Aliai, Bliai, Cliai, Dliai, Qliai
   real(DOUBLE) :: DiffDebit
   real(DOUBLE) :: Factor
   ! Save
   Save DT
   ! Les Constantes sont declares dans le module M_PARAMETRES

   !.. Fonctions intrinseques ..
   intrinsic DMIN1, MOD, DSQRT

   matrice%ab(2,1) = cf1(1)
   matrice%ab(2,2) = cf2(1)
      call SINGULARITE_REZO( Matrice , Singularite , X)
   z = cf1 * cf2
   matrice%ab(2,1) = z(1)

end subroutine REZO


subroutine  SING3       ( &
            DXP,DYP     , & ! Points X et Y de la crete
        Epaisseur_seuil )


   use M_PRECISION        ! Type DOUBLE


   implicit none

   !.. Formal Arguments ..

   real(DOUBLE), dimension(:), intent(inout   ) :: DXP, DYP
   integer                   , intent(in   ) :: Epaisseur_Seuil

   !.. Local Scalars ..
   integer      :: N,sens_ecoul 
   real(DOUBLE) :: C1 =   0._DOUBLE ! Coefficients de la fonction
   real(DOUBLE) :: C2 = -25._DOUBLE ! permettant le passage
   real(DOUBLE) :: C3 =  40._DOUBLE ! du regime denoye
   real(DOUBLE) :: C4 = -15._DOUBLE ! au regime noye pour un seuil epais
   !
   real(DOUBLE) :: D1 = 0.385_DOUBLE ! Coefficients de la fonction
   real(DOUBLE) :: D2 = 1.5_DOUBLE   ! permettant le passage du regime
   real(DOUBLE) :: D3 = 0.5_DOUBLE   ! denoye au regime noye pour un
   real(DOUBLE) :: D4 = 0.615_DOUBLE ! seuil mince
   !
   real(DOUBLE)   :: C,CH,CH1,CT,DCDRH,DQDZ,H1,H2,PAS,Q,Q1
   real(DOUBLE)   :: RH,S,S1,VAM1,Z,Z1,ZA,ZAMONT,ZB
   !character(132) :: arbredappel_old

   !.. Intrinsic Functions ..
   intrinsic DMAX1 , DSQRT



      If( epaisseur_seuil == 1 ) then
         dyp = dxp * dxp
      end if
   return

end subroutine SING3



subroutine toplevel()
   use M_SINGULARITE_T  
   use MOD_FOR_SAVES
   type (SINGULARITE_T), dimension(:), pointer :: Singularite  ! Tableau des singularite
   allocate(singularite(1))
   allocate(matrice%ab(2,2))
end subroutine toplevel
