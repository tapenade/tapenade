module m
contains
 subroutine addvector(a,b,c)
  type :: vector
     real, dimension(2) :: x
  end type vector
  type :: rect
     type(vector) :: w,h
  end type rect
  integer lenf
  CHARACTER(len=50) :: fichier
  type(rect), intent(in) :: a,b
  type(rect), intent(out) :: c
  real, dimension(2) :: ind_nf
  c%w%x(1) = a%w%x(1) + b%w%x(2)
  lenf = LEN(TRIM(fichier))
  ind_nf(:) = (/2, 4/)
 end subroutine addvector
end module m
