

package fr.inria.tapenade.representation;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.utils.TapIntList;

public final class ILUtilsTest {

    public static void main(String[] args) {
        TapIntList toVal = new TapIntList(0, null);
        System.out.println(
                "-00023 + +0100.E-5 = "
                        + ILUtils.addNumericalStrings("-00023", true, "+0100.E-5", true)
                        + " should be -22.999");
        System.out.println(
                "2.d0 + -1 = " + ILUtils.addNumericalStrings("2.d0", true, "-1", true)
                        + " should be 1.0D0");
        System.out.println(
                "2.10 + -1 = " + ILUtils.addNumericalStrings("2.10", true, "-1", true)
                        + " should be 1.1");
        System.out.println(
                "040705.06e-50 + 111.111e-53 = "
                        + ILUtils.addNumericalStrings("040705.06e-50", true, "111.111e-53", true)
                        + " should be 0.40705171111E-45");
        System.out.println(
                "040705.06e-50 + 111.111e-48 = "
                        + ILUtils.addNumericalStrings("040705.06e-50", true, "111.111e-48", true)
                        + " should be 0.5181616E-45");
        System.out.println(
                "040705.06e-50 + -111.111e-48 = "
                        + ILUtils.addNumericalStrings("040705.06e-50", true, "-111.111e-48", true)
                        + " should be 0.2959396E-45");
        System.out.println(
                "-040705.06e-50 + -111.111e-48 = "
                        + ILUtils.addNumericalStrings("-040705.06e-50", true, "-111.111e-48", true)
                        + " should be -0.5181616E-45");
        System.out.println(
                "-040705.06e-50 + 111.111e-48 = "
                        + ILUtils.addNumericalStrings("-040705.06e-50", true, "111.111e-48", true)
                        + " should be -0.2959396E-45");
        System.out.println(
                "040705.06e-50 + 111.111e48 = "
                        + ILUtils.addNumericalStrings("040705.06e-50", true, "111.111e48", true)
                        + " should be 0.111111E51 and looses precision!");
        System.out.println(
                "040705.06e50 + 111.111e48 = "
                        + ILUtils.addNumericalStrings("040705.06e50", true, "111.111e48", true)
                        + " should be 0.4070617111E55");
        System.out.println(
                "040705.06d50 + 111.111d48 = "
                        + ILUtils.addNumericalStrings("040705.06d50", true, "111.111d48", true)
                        + " should be 0.4070617111D55");
        System.out.println(
                "-0431 + 1234.d0 = "
                        + ILUtils.addNumericalStrings("-0431", true, "1234.d0", true)
                        + " should be 803.0D0");
        System.out.println(
                "-0431 + -1234.d0 = "
                        + ILUtils.addNumericalStrings("-0431", true, "-1234.d0", true)
                        + " should be -1665.0D0");
        System.out.println(
                "0431 + 1234.d0 = "
                        + ILUtils.addNumericalStrings("0431", true, "1234.d0", true)
                        + " should be 1665.0D0");
        System.out.println(
                "0431 + -1234.d0 = "
                        + ILUtils.addNumericalStrings("0431", true, "-1234.d0", true)
                        + " should be -803.0D0");
        System.out.println(
                "0431 + 9 = "
                        + ILUtils.addNumericalStrings("0431", true, "9", true)
                        + " should be 440.0");
        System.out.println(
                "0.5d0 + 0.5d-2 = "
                        + ILUtils.addNumericalStrings("0.5d0", true, "0.5d-2", true)
                        + " should be 0.505D0");
        System.out.println(
                "0.5d0 + -1 = "
                        + ILUtils.addNumericalStrings("0.5d0", true, "-1", true)
                        + " should be -0.5D0");
        System.out.println(
                "-10d0 + 2D0 = "
                        + ILUtils.addNumericalStrings("10d0", false, "2D0", true)
                        + " should be -8.0D0");
        System.out.println(
                "testing integer div  +10/+7 = " + (10 / 7) + " should be 1");
        System.out.println(
                "testing integer div  +10/-7 = " + (10 / -7) + " should be -1");
        System.out.println(
                "testing integer div  -10/+7 = " + (-10 / 7) + " should be -1");
        System.out.println(
                "testing integer div  -10/-7 = " + (-10 / -7) + " should be 1");
        System.out.println(
                "testing ILUtils.ILUtils.realIsInt on -4.7e3: "
                        + ILUtils.realIsInt("-4.7e3", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on -00000e-7: "
                        + ILUtils.realIsInt("-00000e-7", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on -00000e-7: "
                        + ILUtils.realIsInt("-00000e-7", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on 043.0e-7: "
                        + ILUtils.realIsInt("043.0e-7", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on 03.0043.0e7: "
                        + ILUtils.realIsInt("03.0043.0e7", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on 03.0043.0e27: " + Integer.MAX_VALUE + "! "
                        + ILUtils.realIsInt("03.0043.0e27", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on -1d40: " + Integer.MAX_VALUE + "! "
                        + ILUtils.realIsInt("-1d40", toVal) + " = " + toVal.head);
        System.out.println(
                "testing ILUtils.realIsInt on 0d40: " + Integer.MAX_VALUE + "! "
                        + ILUtils.realIsInt("0d40", toVal) + " = " + toVal.head);

    }
}
