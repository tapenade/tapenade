/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2020 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.representation.TapList;

/**
 * List of Objects, simply chained forward.
 */
public final class TapListTest<T> { //API

    public static void main(String[] args) {
        TapList target = new TapList<>(false,
                new TapList<>(new TapList<>(false, new TapList<>(true, null)),
                        new TapList<>(new TapList<>(false, null),
                                new TapList<>(new TapList<>(false, null),
                                        null))));
        TapList added = new TapList<>(false, new TapList<>(true, null));
        System.out.println("TARGET:" + target + " ADDED:" + added);
        TapList result = TapList.cumulWithOper(target, added, SymbolTableConstants.CUMUL_OR);
        System.out.println("RESULT:" + result);

        // Test toString when there are cycles:
        TapList list0 = new TapList<>(null, null);
        list0.head = list0;
        list0.tail = list0;
        // Should print:(CYCLE - 1 .CYCLE - 1)
        System.out.println("list0 is:" + list0);

        TapList list1 = new TapList<>(new TapIntList(1, null),
                new TapList<>(null,
                        new TapList<>(new TapIntList(3, null),
                                new TapList<>(null, null))));
        list1.tail.tail.tail.tail = list1.tail;
        list1.tail.tail.tail.head = list1;
        list1.tail.head = new TapList<>(new TapIntList(2, null), list1.tail);
        // Should print:( < 1 > ( < 2 > .CYCLE - 2) <3 > CYCLE - 4 .CYCLE - 3)
        System.out.println("list1 is:" + list1);
    }
}