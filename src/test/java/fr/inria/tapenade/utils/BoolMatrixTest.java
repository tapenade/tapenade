/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.TapEnv;

import java.util.Random;

/**
 * Unitary test of BoolMatrix.
 */
public class BoolMatrixTest {
//     public static boolean testInsertDelete(int a, int b, int rows, int cols, boolean t, boolean v) {
//         BoolMatrix bm = new BoolMatrix(rows, cols);

//         /* Fill randomly */
//         bm.setExplicitZero();
//         Random r = new Random();
//         for (int k = 0; k < rows * cols / 2; k++) {
//             int i = r.nextInt(rows);
//             int j = r.nextInt(cols);
//             bm.set(i, j, true);
//         }
//         /* Keep a reference */
//         BoolMatrix bm2 = bm.copy();
//         /* Add b cols at idx a */
//         if (t) {
//             bm.insertCols(a, b);
//         }

//         /* Add a lines at idx b */
//         if (v) {
//             bm.insertRows(b, a);
//         }

//         /* Remove b cols a idx a */
//         if (t) {
//             bm.deleteCols(a, b);
//         }

//         /* Remove a lines a idx b*/
//         if (v) {
//             bm.deleteRows(b, a);
//         }

//         bm.insertColsAndRows(a, b);
//         bm.deleteColsAndRows(a, b);


//         return bm.equals(bm2);
//     }

    public static void main(String[] args) throws java.io.IOException {
        BoolMatrix bm = new BoolMatrix(5, 74);
        System.out.println("Tests unitaires de BoolMatrix:");
        TapEnv.dumpObject(bm);
        BoolMatrix bml = new BoolMatrix(4, 10);
        BoolMatrix bmr = new BoolMatrix(10, 8);
        bml.setExplicitZero();
        bmr.setExplicitZero();
        bml.setIdentityRow(3);
        bml.set(2, 5, true);
        bmr.setIdentityRow(3);
        bmr.set(5, 7, true);
        TapEnv.dumpObject(bml);
        System.out.println("times");
        TapEnv.dumpObject(bmr);
        System.out.println("=");
        BoolMatrix bmp = bml.times(bmr);
        TapEnv.dumpObject(bmp);
        BoolVector bs = new BoolVector(8);
        bs.set(7, true);
        BoolVector bsp = bmp.times(bs);
        System.out.println(" times " + bs + " equals " + bsp);
        System.out.println("Test de cumulOrTimes:");
        BoolMatrix cumul = new BoolMatrix(6, 2);
        BoolMatrix id = new BoolMatrix(6, 6);
        BoolMatrix r2 = new BoolMatrix(6, 2);
        TapEnv.dumpObject(cumul);
        id.setIdentity();
        TapEnv.dumpObject(id);
        r2.setExplicitZero();
        r2.set(1, 0, true);
        r2.set(4, 0, true);
        r2.set(4, 1, true);
        TapEnv.dumpObject(r2);
        cumul.cumulOrTimes(id, r2);
        System.out.println("apres cumulOrTimes:");
        TapEnv.dumpObject(cumul);


//         /** Tests of insert/delete cols/rows */
//         Random r = new Random();
//         for (int k = 0; k < 10; k++) {
//             int n = r.nextInt(5);
//             int m = r.nextInt(5);

//             if (!testInsertDelete(3, 2, 5 + m, 5 + m, n < 10, m > 10)) {
//                 System.out.println("error for the values n: " + n + " r: " + m);
//             }
//         }
    }
}
