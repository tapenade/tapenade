/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2020 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */
/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */

package fr.inria.tapenade.utils;

import java.util.Random;

/**
 * Unitary tests of BoolVector.
 */
public class BoolVectorTest {

    public static String ltOrGt(int a, int b) {
        if (a < b) {
            return "<";
        } else if (a == b) {
            return "=";
        } else {
            return ">";
        }
    }

//     public static boolean testMove(BoolVector bvBefore, BoolVector bvAfter, int a, int b, int len) {
//         for (int i = 0; i < Math.min(len - b, len - a); i++) {
//             if (bvBefore.get(a + i) ^ bvAfter.get(b + i)) {
//                 return false;
//             }
//         }
//         return true;
//     }

//     public static void testRandomMove(Random randomGenerator, int len) {
//         int a = randomGenerator.nextInt(len);
//         int b = randomGenerator.nextInt(len);
//         testParticularMove(a, b, len);
//     }

//     public static void testParticularMove(int a, int b, int len) {
//         BoolVector bv = new BoolVector(len);
//         bv.set(a, true);
//         Random r = new Random();
//         for (int i = 0; i < len; i++) {
//             bv.set(r.nextInt(len), true);
//         }
//         BoolVector bvBefore = bv.copy();
//         bv.move(a, b);
//         if (!testMove(bvBefore, bv, a, b, len)) {
//                  /*
//                      System.out.println("");
//                      System.out.println("bv = " + bvBefore.toString(len));
//                      System.out.println("move from a: " + a + " to b: " + b + " | a / bSetSize " + ltOrGt(a/bSetSize,b/bSetSize) + " b / bSetSize | a % _ " 
//                                    + ltOrGt(a%bSetSize, b%bSetSize) + " b %_");

//                      System.out.println("bv = " + bv.toString(len));
//                  */
//         }
//     }

    public static void main(String[] args) {
        System.out.println("Tests de decalages:");
        BoolVector bv0 = new BoolVector(BoolVector.B_SET_SIZE);
        System.out.println(" init:" + bv0.toString());
        System.out.println(" Ref :..........(1)..........(2)..........(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ZERO;
        System.out.println(" zero:" + bv0.toString());
        System.out.println(" Ref :..........(1)..........(2)..........(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ONE;
        System.out.println(" one :" + bv0.toString());
        System.out.println(" Ref :1.........(1)..........(2)..........(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ALL;
        System.out.println(" all :" + bv0.toString());
        System.out.println(" Ref :1111111111(1)1111111111(2)1111111111(3)11 =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ZERO;
        bv0.set(0, true);
        bv0.set(2, true);
        bv0.set(8, true);
        bv0.set(23, true);
        bv0.set(27, true);
        bv0.set(28, true);
        bv0.set(31, true);
        System.out.println(" some:" + bv0.toString());
        System.out.println(" Ref :1.1.....1.(1)..........(2)...1...11.(3).1 =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.getSubInt(bv0.bSets[0], 5, 28);
        System.out.println(" gsub:" + bv0.toString());
        System.out.println(" Ref :...1......(1)........1.(2)..1.......(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.setSubInt(bv0.bSets[0], 63, 17, 21);
        System.out.println(" ssub:" + bv0.toString());
        System.out.println(" Ref :...1......(1).......111(2)1.1.......(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ONE << 4;
        System.out.println(" 1<<4:" + bv0.toString());
        System.out.println(" Ref :....1.....(1)..........(2)..........(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ALL >>> 5;
        System.out.println(" a>>5:" + bv0.toString());
        System.out.println(" Ref :1111111111(1)1111111111(2)1111111...(3).. =" + bv0.bSets[0]);
        System.out.println();
        bv0.bSets[0] = BoolVector.B_SET_ALL << 9;
        System.out.println(" a<<9:" + bv0.toString());
        System.out.println(" Ref :.........1(1)1111111111(2)1111111111(3)11 =" + bv0.bSets[0]);
        System.out.println();

        System.out.println("Tests unitaires de BoolVector:");
        int len = 80;
        BoolVector bv = new BoolVector(len);
        BoolVector bv2 = new BoolVector(len);
        int i;
        for (i = 0; i < len; i += 4) {
            bv.set(i, true);
            bv.set(i + 2, true);
            bv2.set(i, true);
            bv2.set(i + 1, true);
        }
        System.out.println("bv =             " + bv.toString(len));
        System.out.println("bv2=             " + bv2.toString(len));
        System.out.println("DISTANCE bv     TO bv2:" + bv.distance(bv2, 80));
        System.out.println("DISTANCE bv TO not-bv2:" + bv.distance(bv2.not(), 80));
        bv.cumulOr(bv2, 2, 23, 21);
        System.out.println("bv1or2(2,23,21)      :" + bv.toString(len));

        bv = new BoolVector(446);
        bv2 = new BoolVector(446);
        bv2.set(202, true);
        System.out.println("bv =     " + bv);
        System.out.println("bv2=     " + bv2);
        boolean grows = bv2.cumulOrGrows(bv, 446);
        System.out.println("new bv2= " + bv2 + " grows:" + grows);

//         Random randomGenerator = new Random();
//         testParticularMove(14, 17, len);
//         long time = System.currentTimeMillis();
//         for (i = 0; i < 100000; i++) {
//             testRandomMove(randomGenerator, len);
//         }
//         time = System.currentTimeMillis() - time;
//         double time2 = ((double) time) / 100000.;
//         System.out.println("It took on average " + time2 + " millisecond by testRandomMove.");

        int bso = BoolVector.B_SET_ONE;
        System.out.println("bso:  " + bso);
        System.out.println("===============");
        for (i = 0; i < 32; ++i) {
            System.out.println("bso<<" + i + ": " + (bso << i) + " =?= "); //+ maskOneLeft[i]);
        }
        int bsa = BoolVector.B_SET_ALL;
        System.out.println("bsa:  " + bsa);
        System.out.println("===============");
        for (i = 0; i < 32; ++i) {
            System.out.println("bsa<<" + i + ": " + (bsa << i) + " =?= " + //maskAllLeft[i] +
                    "~(bsa<<" + i + "): " + (~(bsa << i)) + " =?= " + //maskAllLeftNot[i] +
                    "   bsa>>>" + i + ": " + (bsa >>> i) + " =?= "); //+ maskAllRight[i]);
        }
    }
}
