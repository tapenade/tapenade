/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2019 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */
package fr.inria.tapenade.frontend;

import fr.inria.tapenade.frontend.TreeProtocol;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class TreeProtocolTest {
    public static void main(String[] args) {
        InputStream is;
        Tree tree;
        try {
            if (args.length == 0) {
                is = System.in;
            } else if (args.length == 1) {
                is = new FileInputStream(args[0]);
            } else if (args.length == 2) {
                is = new FileInputStream(args[0]);
            } else {
                is = new FileInputStream(args[0]);
            }
            TreeProtocol inputTreeProto = new TreeProtocol(is);
            try {
                if (inputTreeProto.seeOperator().code != ILLang.op_endOfProtocol) {
                    tree = inputTreeProto.readTree();
                    System.out.println(
                            "TreeProtocol -> tree = " + tree.toString());
                    System.out.println("Check syntax " + (tree.checkSyntax("protocol") ? "ok" : "wrong"));
                } else {
                    System.out.println("ViewTreeProtocol: exit...");
                    System.exit(0);
                }
            } catch (NullPointerException e) {
                System.out.println("ViewTreeProtocol: " + e);
            }

        } catch (FileNotFoundException e) {
            System.out.println("ViewTreeProtocol: " + e);
        } catch (IOException e) {
            //System.out.println("ViewTreeProtocol: " + e);
        }
    }
}
