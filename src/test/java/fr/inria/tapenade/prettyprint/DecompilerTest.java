/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * mailto:tapenade AT inria DOT fr
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.frontend.TreeProtocol;
import fr.inria.tapenade.prettyprint.CDecompiler;
import fr.inria.tapenade.prettyprint.Decompiler;
import fr.inria.tapenade.prettyprint.FortranDecompiler;
import fr.inria.tapenade.prettyprint.TextPrinter;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;
import java.io.InputStream;

public class DecompilerTest {
    /**
     * Shorthand for "get the tree then decompile it".
     */
    public static void main(String[] args) {
        InputStream is;
        int language;
        is = System.in;
        String lang = "";
        if (args.length > 0) {
            lang = args[0];
        }
        if ("fortran".equals(lang)) {
            language = TapEnv.FORTRAN;
        } else if ("c".equals(lang)) {
            language = TapEnv.C;
        } else {
            language = TapEnv.FORTRAN90;
        }
        decompile(is, language);
    }

    private static void decompile(InputStream inputStream, int language) {
        Tree tree = null;
        try {
            tree = new TreeProtocol(inputStream).readTree();
        } catch (IOException e) {
            TapEnv.systemError("(Decompiler) I-O error " + e.getMessage() + " while reading input stream");
        }
        try {
            final TextPrinter textPrinter = new TextPrinter("test");
            textPrinter.initFile(language);
            Decompiler languagePrinter = null;
            if (TapEnv.isFortran(language)) {
                languagePrinter = new FortranDecompiler(textPrinter, null);
            } else if (TapEnv.isC(language)) {
                languagePrinter = new CDecompiler(textPrinter, null);
            }
            if (languagePrinter != null) {
                languagePrinter.decompileTree(tree, language, false, false);
                languagePrinter.newLine(0);
            }
            textPrinter.newLine();
            textPrinter.closeFile();
        } catch (final Exception e) {
            System.out.println("Could not open file " + "test" + " for unmodified input file output!");
        }
    }
}
