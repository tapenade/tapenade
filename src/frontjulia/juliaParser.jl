using Libdl

dbg_lvl = 0
# println(@__DIR__)
# println(Type(@__DIR__))
c_libname = joinpath(normpath(dirname(@__FILE__)), "julialib.so")
#println("This is a string")
#println(c_libname)
lib = Libdl.dlopen(c_libname) # Open the library explicitly.

# Functions from the tree builder library for parsing the julia tree
c_newJuliaTreeBuilder = Libdl.dlsym(lib, :newJuliaTreeBuilder)   # Get a symbol for the function to call. !!!! Redefine this library
c_startJuliaTree = Libdl.dlsym(lib, :startJuliaTree)
c_turnListFrontier = Libdl.dlsym(lib, :turnListFrontier)
## To review: c_startAnnotation = Libdl.dlsym(lib, :startTree) ### This is an extern function, and we might want to have this pointing to another lib!
c_putValue = Libdl.dlsym(lib, :putValue) ## Also extern
c_putTree = Libdl.dlsym(lib, :putTree) ## Also extern
c_oneLessWaiting = Libdl.dlsym(lib, :oneLessWaiting) 
c_putListTree = Libdl.dlsym(lib, :putListTree) ## Extern
c_startDummyTree = Libdl.dlsym(lib, :startDummyTree) ## Extern
c_terminateListTree = Libdl.dlsym(lib, :terminateListTree) ## Extern
c_getTreeBuilt = Libdl.dlsym(lib, :getTreeBuilt) ## Extern
c_getListTreeBuilt = Libdl.dlsym(lib, :getListTreeBuilt) ## Extern
c_deleteTreeBuilder = Libdl.dlsym(lib, :deleteTreeBuilder) ## Extern
c_resetTreeBuilder = Libdl.dlsym(lib, :resetTreeBuilder) ## Extern
c_removeTree = Libdl.dlsym(lib, :removeTree)
c_showTreeBuilderState = Libdl.dlsym(lib, :showTreeBuilderState) ## Debug purposes
c_julia2ilProtocol = Libdl.dlsym(lib, :julia2ilProtocol) ## Useful to send the tree to tapenade
c_listTreeLength = Libdl.dlsym(lib, :listTreeLength)
c_listTreeNth = Libdl.dlsym(lib, :listTreeNth)


EndOfList = :EndOfList
BeginInclude = :BeginInclude
EndInclude = :EndInclude
cstSymb = :ident

# The following should be in an external, hard coded, file.
metalnames = Dict(
    :(=) => "assign",
    # :. => "package", # Check whether the dot refers to a local path or is synonym for an import
    :. => "dot", 
    :block => "block",
    :vect => "vect",
    :vcat => "vcat",
    :row => "row",
    :return => "return",
    :tuple => "tuple",
    :ref => "ref",
    :curly => "curly",
    :call => "call", 
    :function => "function",
    :using => "using", 
    :import => "import",
    :toplevel => "toplevel",
    # :(:) => "rangeop",
    :end => "end",
    Int64 => "int64",
    Float64 => "float64",
    Int32 => "int32",
    Float32 => "float32",
    String => "string",
    LineNumberNode => "linenumber",
    Nothing => "none",
    :(+=) => "plusAssign",
    :(-=) => "minusAssign",
    :(*=) => "timesAssign",
    :(/=) => "divAssign",

    # :map => "map",
    :BeginInclude => "beginInclude",
    :EndInclude => "endInclude",
    :EndOfList => "endOfList" #=,
    cstSymb => "ident" =#
)

function file2ast(fname::AbstractString, outfile::AbstractString)
    
    tree_builder = ccall(c_newJuliaTreeBuilder, Ptr{Cvoid}, (Cint,), 5)
    all_symbols = Set()
    # Send a first "file" tree
    ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, "file")
    open(outfile, "w") do out 
        code = read(fname, String)
        ## println(code)

        exp_in_waiting = []
        
        prev = 0
        next = 1
        while next > prev 
            prev = next
            exp, next = Meta.parse(code, prev)
            push!(exp_in_waiting, exp)
            if (dbg_lvl > 1)
                println("     ***** (New line) Currently looking at: ", exp)
            end
            while !isempty(exp_in_waiting)
                e = pop!(exp_in_waiting)
                if (dbg_lvl > 1)
                    println("     ***** (New element) Currently looking at: ", string(e))
                end
                if e isa Expr 
                    # Parse expression
                    # Write current instruction
                    if (dbg_lvl > 1)
                        println("E is an expression with head ", e.head)
                        println("Operator ", e.head, " has ", length(e.args), " children. ")
                        println("E is an expression with args ", e.args)
                    end

                    op = e.head

                    if op == :call && e.args[1] == :include
                        push!(exp_in_waiting, Expr(EndInclude, e.args[2]))
                        pathToFile = joinpath(dirname(fname), e.args[2])
                        push!(exp_in_waiting, Meta.parse(join(["begin", read(pathToFile, String), "end"], ";")))
                        push!(exp_in_waiting, Expr(BeginInclude, e.args[2]))
                        continue
                    end

                    if !(op in (:(=), BeginInclude, EndInclude))
                        if (dbg_lvl > 2)
                            println("Pushing an end of list")
                        end
                        push!(exp_in_waiting, EndOfList)
                    end
                    for arg in Iterators.reverse(e.args)
                        push!(exp_in_waiting, arg)
                    end
                    #= Let's not care about this here, and handle this in the ttml translation
                    if ((op == :.) && (length(e.args) == 2))
                        # In this case, we are dealing with a julia broadcasting, i.e. map op
                        op = :map
                    end
                    =#
                    if haskey(metalnames, op) 
                        push!(all_symbols, metalnames[op]) 
                        if (dbg_lvl > 1)
                            println("E being an expression, creating new tree of type an expression with head ", metalnames[op])
                        end
                        ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, metalnames[op])
                        #  push!(exp_in_waiting, e.head) 
                        write(out, metalnames[op]) # New C node from tree builder
                        write(out, "\n")
                    else
                        # Current operator has not been handled so far
                        println("JuliaParser warning: Unknown operator '", op, "'. It will be parsed as an 'unknown' until it is added to the parser.")
                        push!(all_symbols, op) 
                        ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, "unknown")
                        write(out, "unknown") 
                        write(out, "\n")
                    end

                elseif e isa Symbol
                    # Add symbol to file
                    if (dbg_lvl > 1)
                        println("E is a symbol: ", e)
                    end
                    if (e == EndOfList)
                        if (dbg_lvl > 1)
                            println("Terminating list tree")
                        end
                        write(out, "EndOfList")
                        ccall(c_terminateListTree, Cvoid, (Ptr{Cvoid},), tree_builder)
                    elseif haskey(metalnames, e) 
                        if (dbg_lvl > 1)
                            println("E has a metal name defined: ", metalnames[e])
                        end
                        write(out, metalnames[e]) # New leaf for current operator
                        ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, metalnames[e])
                    else
                        if (dbg_lvl > 1)
                            println("We assumed no metal names. Hence writing it as an ident")
                        end
                        write(out, "ident\n")
                        write(out, e) # New leaf for current operator
                        ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, "ident")
                        ccall(c_putValue, Cvoid, (Ptr{Cvoid}, Cstring), tree_builder, string(e))
                    end
                    write(out, "\n")
                else
                    # Neither a symbol nor an expression, it's probably a constant
                    if (dbg_lvl > 1)
                        println("Type of our constant thing is ", typeof(e))
                        println("This specific thing is ", e)
                        println("This specific thing is ", string(e))
                    end
                    push!(all_symbols, metalnames[typeof(e)])
                    write(out, metalnames[typeof(e)])
                    write(out, "\n")
                    write(out, string(e))
                    write(out, "\n")
                    ccall(c_startJuliaTree, Ptr{Cvoid}, (Ptr{Cvoid}, Cstring,), tree_builder, metalnames[typeof(e)])
                    ccall(c_putValue, Cvoid, (Ptr{Cvoid}, Cstring), tree_builder, string(e))
                    
                end
                # ccall(c_showTreeBuilderState, Cvoid, (Ptr{Cvoid},), tree_builder)
                if (dbg_lvl > 3)
                    println("Stack is currently ", exp_in_waiting)
                end
            end
            
	    # ccall(c_showTreeBuilderState, Cvoid, (Ptr{Cvoid},), tree_builder)
            
        end

        # Conclude the file before closing the output writing
        write(out, "EndOfList")
        ccall(c_terminateListTree, Cvoid, (Ptr{Cvoid},), tree_builder)

    end
    if (dbg_lvl > 2)
        ccall(c_showTreeBuilderState, Cvoid, (Ptr{Cvoid},), tree_builder)
    end
    if (dbg_lvl > 3)
        println("Recovering the built tree")
    end
    parsedTree = ccall(c_getTreeBuilt, Ptr{Cvoid}, (Ptr{Cvoid},), tree_builder)
    if (dbg_lvl > 3)
        println("Built tree recover.")
    end
    
    #=if (dbg_lvl > 3)
        println("Trying to recover the tree list")
    end
    listTrees = ccall(c_getListTreeBuilt, Ptr{Cvoid}, (Ptr{Cvoid},), tree_builder)
    if (dbg_lvl > 3)
        println("Tree list recovered")
    end=#


    # Some voodoo magic to access C stdout!
    # It is based on the fact that stdout is in fact a FILE* with file descriptor 1
    c_stdout = Libc.FILE(Libc.RawFD(1), "w")  # corresponds to C standard output

    #= listLength = ccall(c_listTreeLength, Cint, (Ptr{Cvoid},), listTrees)
    # Tree *listTreeNth(ListTree *listTree, int rank) ;
    if (dbg_lvl > 3)
        println("We have recovered ", listLength, " trees")
    end

    for i = 1:listLength
        parsedTree = ccall(c_listTreeNth, Ptr{Cvoid}, (Ptr{Cvoid}, Cint), listTrees, i-1)
        ccall(c_julia2ilProtocol, Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), parsedTree, c_stdout)
    end =#
    ccall(c_julia2ilProtocol, Cvoid, (Ptr{Cvoid}, Ptr{Cvoid}), parsedTree, c_stdout)
    return all_symbols
end


function main(args)
    all_symbols = file2ast(args[1], args[2])
end

main(ARGS)

Libdl.dlclose(lib)
