definition of julia is
  abstract syntax
   ALL  ::= none toplevel function curly linenumber float64 float32 float16 int64 int32 int16 int8 boolean ref block using import vcat vect block return tuple call assign dot endInclude;  -- Left for ttml: map, package

   file -> ALL * ... ;
   vect -> ALL * ... ; -- + instead of * if at least one element
   call -> ALL + ... ; -- should be + : at least one element is required
   assign -> ALL ALL ;
   -- package -> ALL * ... ;
   using -> ALL * ... ; -- Verify
   import -> ALL * ... ;
   ref -> ALL * ... ;
   block -> ALL * ... ;
   toplevel -> ALL * ... ;
   function -> ALL * ... ;
   curly -> ALL * ... ;
   return -> ALL * ... ;
   -- map -> ALL * ... ;
   dot -> ALL * ... ; -- In julia, this is a Symbol ':.'
   tuple -> ALL * ... ;
   vcat -> ALL * ...  ;
   row -> ALL * ...  ;
   hcat -> ALL * ...  ;
   end  -> ;
   endOfList  ->  ;
   beginInclude -> ALL  ; -- In fact, it is only the path to the included file
   endInclude  -> ALL ; -- In fact, it is only the path to the included file
   rangeop  ->  ; -- Range operator == ":(:)" in Julia -- infix for accessing entries in a vector. This is being passed as a call and therefore does not have any child
   plusAssign -> ALL * ...  ;
   minusAssign -> ALL * ...  ;
   divAssign -> ALL * ...  ;
   timesAssign -> ALL * ...  ;

   unknown -> ALL * ...  ;

   linenumber -> implemented as STRING ; -- TO BE CONTINUED

   int64  -> implemented as STRING ;
   int32  -> implemented as INTEGER ; -- Might be a int32 or 16
   int16  -> implemented as STRING ;
   int8  -> implemented as STRING ;
   boolean  -> implemented as STRING ;
   float64  -> implemented as STRING ;
   float32  -> implemented as STRING ;
   float16  -> implemented as STRING ;
   ident  -> implemented as STRING ;
   string -> implemented as STRING ;

   none  -> ; -- Atom

   -- We will need to add the module keyword
   -- On the ttml side of things, it should look something along the lines of 
   -- module [(ID) L] -> module [(ID) declarations L] ;

end definition
