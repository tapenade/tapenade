julia juliaParser.jl examples/ttmltests.jl ttmltestsoutput > traceRunTtmlTests
julia juliaParser.jl examples/curly_test.jl curly_testoutput > traceRunCurly_test
julia juliaParser.jl examples/small_test_file.jl small_test_fileoutput > traceRunSmall_test_file
julia juliaParser.jl examples/testfile.jl testfileoutput > traceRunTestfile
julia juliaParser.jl examples/testinclude.jl testincludeoutput > traceRunTestinclude
julia juliaParser.jl examples/micro.jl microoutput > traceRunMicro
