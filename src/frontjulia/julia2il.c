
/****** GENERATED FILE : DO NOT EDIT !!!! ******/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../front/cvtp.h"
#include "../front/treeBuilder.h"
#include "tablesjulia.c"
#include "../../build/generated-src/c/main/front/tablesil.c"

#define new(TYPE) (TYPE*)malloc(sizeof(TYPE))

/* TYPES: */

/* Type for "ttml function for translation of trees with a fixed root operator". */
typedef void (TTMLOperatorTreeTranslator)(Tree*, int, short) ;

/* Used to cut a ListTree in two ListTree's */
typedef struct _Cutter{
  ListTree **cut ;
  ListTree *left, *right ;
}Cutter ;

/* GLOBAL VARIABLES: */
static TreeBuilder *targetBuilder = NULL ;
typedef struct TargetBuilderStack_ { 
    TreeBuilder* top; 
    struct TargetBuilderStack_* next;
} TargetBuilderStack;
static TargetBuilderStack* stack = NULL ;
static FILE *protocolOutputStream = NULL ;
static const char *(contextNames[]) = {"TOP", "range", "inblock", "package"} ;

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) ;

void sendTreeProtocol(Tree *tree, FILE *protocolOutputStream) {
  if ((tree == NULL) || (tree->oper == NULL)) {
    fprintf(stdout, "incomplete tree\n") ;
  } else {
    if (tree->annotations != NULL) {
      KeyListTree *tlAnnotations = tree->annotations ;
      while (tlAnnotations != NULL) {
	fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
	fprintf(protocolOutputStream, "%s\n", tlAnnotations->key) ;
	sendTreeProtocol(tlAnnotations->tree, protocolOutputStream) ;
	tlAnnotations = tlAnnotations->next ;
      }
    }
    fprintf(protocolOutputStream, "%d\n", tree->oper->rank) ;
    if (tree->oper->arity == 0) {
        if (tree->contents.value)
	           fprintf(protocolOutputStream, "%s\n", tree->contents.value) ;
    } else {
      sendListTreeProtocol(tree->contents.sons, protocolOutputStream) ;
      if (tree->oper->arity == -1)
        fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
    }
  }
}

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) {
    while (listTree != NULL) {
      sendTreeProtocol(listTree->tree, protocolOutputStream) ;
      listTree = listTree->next ;
    }
}

void sendOp(int operRk) {
  if (stack->top)
    startTree(stack->top, operRk) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", operRk) ;
  else
    printf("%d\n", operRk) ;
}

void sendValue(const char *value) {
  if (stack->top)
    putValue(stack->top, strdup(value)) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%s\n", value) ;
  else
    printf("%s\n", value) ;
}

void endList() {
  if (stack->top)
    terminateListTree(stack->top) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
  else
    printf("%d\n", ilEndOfListCode) ;
}

void sendAnnotation(char *name) {
    if (stack->top) {
      startAnnotation(stack->top, name) ;
  } else if (protocolOutputStream) {
    fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
      fprintf(protocolOutputStream, "%s\n", name) ;
  } else {
      printf("%d\n", ilAnnotationCode) ;
      printf("%s\n", name) ;
  }
}

void acceptAnnotations() {
    if (stack->top) stack->top->acceptAnnotations = 1 ;
}

void sendTree(Tree *tree) {
  if (stack->top)
    putTree(stack->top, tree) ;
  else
    sendTreeProtocol(tree,
		     (protocolOutputStream)?protocolOutputStream:stdout) ;
}

void sendListTree(ListTree *listTree) {
  if (stack->top)
    putListTree(stack->top, listTree) ;
  else
    sendListTreeProtocol(listTree,
	                        (protocolOutputStream)?protocolOutputStream:stdout) ;
}

Tree *sendDummyOp(int arity) {
    if (stack->top)
	return startDummyTree(stack->top, arity) ;
    else {
	printf("Cannot start a dummy tree with no targetBuilder\n") ;
	return NULL ;
    }
}

void cutAt(Cutter *cutter, ListTree *listTree, int cutPos) {
  /* only implemented case: cutPos < 0 */
  int i = listTreeLength(listTree) + cutPos ;
  if (i==0) {
    cutter->left = NULL ;
    cutter->right = listTree ;
    cutter->cut = NULL ;
  } else {
    cutter->left = listTree ;
    while ((--i)>0) listTree = listTree->next ;
    cutter->right = listTree->next ;
    listTree->next = NULL ;
    cutter->cut = &(listTree->next) ;
  }
}

void undoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=cutter->right ;
}

void redoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=NULL ;
}

/*Forward declarations: */
void julia2ilTTList(ListTree *listTree, int context, short resultKind) ;
void julia2ilTT(Tree *fTree, int context, short resultKind) ;

void ttmlNoRule(Tree *fTree, int context, short resultKind) {
      char *message = (char*)malloc(200) ;
      sprintf(message, "Unexpected source operator: %s (%s)",
		   fTree->oper->name, contextNames[context]) ;
      sendOp(ilParsingErrorCode) ;
      sendValue(message) ;
}

void julia2ilTT1(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE file(X)* -> 
              file(X)*  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(78) ;
    listStars = fTree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    julia2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT2(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE vect L -> 
              arrayConstructor L  */
    sendOp(10) ;
    julia2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT3(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 4) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,":") == 0)) {
    /* RULE call(ident:":",START,STEP,END) -> 
              arrayTriplet(START,range::END,STEP)  */
    sendOp(12) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->next->tree, 1, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,":") == 0)) {
    /* RULE call(ident:":",START,END) -> 
              arrayTriplet(START,END,none())  */
    sendOp(12) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"*") == 0)) {
    /* RULE call(ident:"*",EXP1,EXP2) -> 
              mul(EXP1,EXP2)  */
    sendOp(137) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"/") == 0)) {
    /* RULE call(ident:"/",EXP1,EXP2) -> 
              div(EXP1,EXP2)  */
    sendOp(64) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"+") == 0)) {
    /* RULE call(ident:"+",EXP1,EXP2) -> 
              add(EXP1,EXP2)  */
    sendOp(3) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"-") == 0)) {
    /* RULE call(ident:"-",EXP1,EXP2) -> 
              sub(EXP1,EXP2)  */
    sendOp(187) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 37) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"-") == 0)) {
    /* RULE call(ident:"-",EXP) -> 
              minus(EXP)  */
    sendOp(128) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE call[(FUNCNAME),PARAMS] -> 
              call(none(),FUNCNAME,expressions PARAMS)  */
    sendOp(31) ;
    sendOp(143) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(73) ;
    julia2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT4(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 3) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 2)) {
    /* RULE assign(call(FNAME,ARGS),EXPR) -> 
              assign(FNAME,lambda(ARGS,EXPR))  */
    sendOp(14) ;
    julia2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(117) ;
    julia2ilTT(fTree->contents.sons->tree->contents.sons->next->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE assign(LHS,RHS) -> 
              assign(LHS,RHS)  */
    sendOp(14) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT5(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 1) &&
      (fTree->contents.sons->tree->oper->rank == 13) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 1)) {
    /* RULE using[(dot(ID))] -> 
              useDecl(ID,none())  */
    sendOp(204) ;
    julia2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE using L -> 
              blockStatement package::L  */
    sendOp(27) ;
    julia2ilTTList(fTree->contents.sons, 3, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT6(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 1) &&
      (fTree->contents.sons->tree->oper->rank == 13) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 1)) {
    /* RULE import[(dot(ID))] -> 
              useDecl(ID,none())  */
    sendOp(204) ;
    julia2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE import L -> 
              blockStatement package::L  */
    sendOp(27) ;
    julia2ilTTList(fTree->contents.sons, 3, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT7(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE ref[(EXPR),(INDEX)*] -> 
              arrayAccess(EXPR,expressions[(INDEX)*])  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(9) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(73) ;
    listStars = fTree->contents.sons->next ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    julia2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT8(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE block LIST -> 
              blockStatement inblock::LIST  */
    sendOp(27) ;
    julia2ilTTList(fTree->contents.sons, 2, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT9(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE toplevel L -> 
              blockStatement L  */
    sendOp(27) ;
    julia2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT10(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 3) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) >= 1)) {
    /* RULE function(call[(FNAME),(PARAMS)*],BODY) -> 
              function(none(),void(),none(),FNAME,varDeclarations[(PARAMS)*],BODY)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(91) ;
    sendOp(143) ;
    sendOp(211) ;
    sendOp(143) ;
    julia2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(207) ;
    listStars = fTree->contents.sons->tree->contents.sons->next ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    julia2ilTT(listStars->tree, 0, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT12(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE return EXPR -> 
              return(expressions EXPR,none())  */
    sendOp(173) ;
    sendOp(73) ;
    julia2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
    sendOp(143) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT13(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE dot(ID,L) -> 
              blockStatement[(stringCst:"JULIA2IL:Unavailable broadcasting (map) operator"),(ID),L]  */
    sendOp(27) ;
    sendOp(185) ;
    sendValue("JULIA2IL:Unavailable broadcasting (map) operator") ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT15(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE vcat(EXP1,EXP2) -> 
              concat(EXP1,EXP2)  */
    sendOp(44) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT16(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE row L -> 
              arrayConstructor L  */
    sendOp(10) ;
    julia2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT18(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && (context == 1)) {
    /* RULE range::end() -> 
              none()  */
    sendOp(143) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT20(Tree *fTree, int context, short resultKind) {
  if ((resultKind == -1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 38)) {
    /* RULE beginInclude(string ID) -> 
              (include ID)  */
    sendOp(104) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT21(Tree *fTree, int context, short resultKind) {
  if ((resultKind == -1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 38)) {
    /* RULE endInclude(string ID) -> 
              (include:"tapenade end #include")  */
    sendOp(104) ;
    sendValue("tapenade end #include") ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT23(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE plusAssign(LHS,RHS) -> 
              plusAssign(LHS,RHS)  */
    sendOp(155) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT24(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE minusAssign(LHS,RHS) -> 
              minusAssign(LHS,RHS)  */
    sendOp(129) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT25(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE divAssign(LHS,RHS) -> 
              divAssign(LHS,RHS)  */
    sendOp(65) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT26(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE timesAssign(LHS,RHS) -> 
              timesAssign(LHS,RHS)  */
    sendOp(196) ;
    julia2ilTT(fTree->contents.sons->tree, 0, 1) ;
    julia2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT29(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE int64 VAL -> 
              intCst VAL  */
    sendOp(106) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT30(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE int32 VAL -> 
              intCst VAL  */
    sendOp(106) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT31(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE int16 VAL -> 
              intCst VAL  */
    sendOp(106) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT32(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE int8 VAL -> 
              intCst VAL  */
    sendOp(106) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT33(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE boolean VAL -> 
              boolCst VAL  */
    sendOp(28) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT34(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE float64 VAL -> 
              realCst VAL  */
    sendOp(165) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT35(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE float32 VAL -> 
              realCst VAL  */
    sendOp(165) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT36(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE float16 VAL -> 
              realCst VAL  */
    sendOp(165) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT37(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE ident VAL -> 
              ident VAL  */
    sendOp(98) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT38(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE string VAL -> 
              stringCst VAL  */
    sendOp(185) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTT39(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE none() -> 
              none()  */
    sendOp(143) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void julia2ilTTList(ListTree *listTree, int context, short resultKind) {
  if ((resultKind == -1) && (context == 2) &&
      (listTreeLength(listTree) >= 3) &&
      (listTree->tree->oper->rank == 28) &&
      (listTree->next->tree->oper->rank == 28)) {
    /* RULE inblock::[(linenumber N1,linenumber N2,INSTR),INSTRS] -> 
              [(INSTR^{line:stringCst N2}),inblock::INSTRS]  */
    sendAnnotation("line") ;
    sendOp(185) ;
    sendValue((char *)listTree->next->tree->contents.value) ;
    acceptAnnotations() ;
    julia2ilTT(listTree->next->next->tree, 0, 1) ;
    julia2ilTTList(listTree->next->next->next, 2, -1) ;
  } else  if ((resultKind == -1) && (context == 2) &&
      (listTreeLength(listTree) >= 2) &&
      (listTree->tree->oper->rank == 28)) {
    /* RULE inblock::[(linenumber N2,INSTR),INSTRS] -> 
              [(INSTR^{line:stringCst N2}),inblock::INSTRS]  */
    sendAnnotation("line") ;
    sendOp(185) ;
    sendValue((char *)listTree->tree->contents.value) ;
    acceptAnnotations() ;
    julia2ilTT(listTree->next->tree, 0, 1) ;
    julia2ilTTList(listTree->next->next, 2, -1) ;
  } else  if ((resultKind == -1) && (context == 2) &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE inblock::[(INSTR),INSTRS] -> 
              [(INSTR),inblock::INSTRS]  */
    julia2ilTT(listTree->tree, 0, 1) ;
    julia2ilTTList(listTree->next, 2, -1) ;
  } else  if ((resultKind == -1) && (context == 2) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE inblock::[] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 3) &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 13) &&
      (listTreeLength(listTree->tree->contents.sons) == 1)) {
    /* RULE package::[(dot(ID)),L] -> 
              [(useDecl(ID,none())),package::L]  */
    sendOp(204) ;
    julia2ilTT(listTree->tree->contents.sons->tree, 0, 1) ;
    sendOp(143) ;
    julia2ilTTList(listTree->next, 3, -1) ;
  } else  if ((resultKind == -1) && (context == 3) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE package::[] -> 
              []  */
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE [(X),Y] -> 
              [(X),Y]  */
    julia2ilTT(listTree->tree, 0, 1) ;
    julia2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) == 0)) {
    /* RULE [] -> 
              []  */
  } else {
    char *message = (char*)malloc(1000) ;
    int len = 0 ;
    message[0] = '\0' ;
    message = strcat(message, "Unexpected source list: (") ;
    while (listTree != NULL) {
      message = strcat(message, listTree->tree->oper->name) ;
      listTree = listTree->next ;
      if (listTree) {
        if (len>5) {
          message = strcat(message, " ...") ;
          listTree=NULL ;
        } else message = strcat(message, " ") ;
        len++ ;
      }
    }
    message = strcat(message, ") (") ;
    message = strcat(message, contextNames[context]) ;
    message = strcat(message, ")") ;
    sendOp(ilParsingErrorCode) ;
    sendValue(message) ;
  }
}


static TTMLOperatorTreeTranslator *(julia2ilOperTreeTrans[]) = {
  /*   0...*/ &ttmlNoRule,&julia2ilTT1,&julia2ilTT2,&julia2ilTT3,&julia2ilTT4,
  /*   5...*/ &julia2ilTT5,&julia2ilTT6,&julia2ilTT7,&julia2ilTT8,&julia2ilTT9,
  /*  10...*/ &julia2ilTT10,&ttmlNoRule,&julia2ilTT12,&julia2ilTT13,&ttmlNoRule,
  /*  15...*/ &julia2ilTT15,&julia2ilTT16,&ttmlNoRule,&julia2ilTT18,&ttmlNoRule,
  /*  20...*/ &julia2ilTT20,&julia2ilTT21,&ttmlNoRule,&julia2ilTT23,&julia2ilTT24,
  /*  25...*/ &julia2ilTT25,&julia2ilTT26,&ttmlNoRule,&ttmlNoRule,&julia2ilTT29,
  /*  30...*/ &julia2ilTT30,&julia2ilTT31,&julia2ilTT32,&julia2ilTT33,&julia2ilTT34,
  /*  35...*/ &julia2ilTT35,&julia2ilTT36,&julia2ilTT37,&julia2ilTT38,&julia2ilTT39,
  /*  40...*/ &ttmlNoRule,&ttmlNoRule,&ttmlNoRule,NULL,NULL,
  /* end...*/ NULL
} ;

void julia2ilTT(Tree *fTree, int context, short resultKind) {
  (*(julia2ilOperTreeTrans[fTree->oper->rank]))(fTree, context, resultKind) ;
}

void julia2ilProtocol(Tree *fTree, FILE *outputStream) {
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = NULL ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = outputStream ;
  julia2ilTT(fTree, 0, 1) ;
  stack = stack->next;
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
}

Tree *julia2ilTree(Tree *fTree) {
  Tree *returnTree ;
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = newTreeBuilder(ilOperators) ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = NULL ;
  julia2ilTT(fTree, 0, 1) ;
  returnTree = getTreeBuilt(stack->top) ;
  stack = stack->next;
  deleteTreeBuilder(newStack->top);
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
  return returnTree ;
}
