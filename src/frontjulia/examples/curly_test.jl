xf_fixed = [ 42.165, 0, 0, 0, 0 ]          # final state (free final longitude)

# More involved data structures
init = Dict{Real, Tuple{Real, Vector{Real}}}()

# Multiple things on a single line
tf = 15.2055; p0 = -[ .361266, 22.2412, 7.87736, 0, 0, -5.90802 ]; init[60] = (tf, p0)
