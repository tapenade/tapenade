my_pi = 3.14

#function perimeter(r::Number)::AbstractFloat
function perimeter(r)
    out = 2*pi*r
    return out
end