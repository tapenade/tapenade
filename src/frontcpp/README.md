C++ Parser (cppParser)
======================

To build a cppParser executable from tapenade directory:

`./gradlew frontcpp`

`cppParser` is created in a subdirectory of `tapenade/bin/`
according the operating system name: linux or mac, using local 
version of Clang.

`./gradlew frontcppdocker`

`cppParser` is created in `tapenade/bin/linux` using a Docker 
image of Clang/LLVM in
`registry.gitlab.inria.fr/tapenade/distrib`.

A Docker image of cppParser is pushed in
`registry.gitlab.inria.fr/tapenade/tapenade/cppparser`.

registry.gitlab.inria.fr/tapenade/distrib is built with:

`./docker-build-cpp-make.sh`
