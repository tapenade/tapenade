static int nClangOperators = 167;
static Operator clangOperators[] = {               /* STATUS : */
    {"opNone", 0, 0},                              /* a none() tree is always useful */
    {"opAbiTagAttr", -1, 1}, 
    {"opAccessSpecDecl", 1, 2},                    /* to verify */
    {"opAliasAttr", -1, 3},
    {"opAlignedAtt", -1, 4},
    {"opAlwaysInlineAttr", -1, 5},
    {"opArraySubscriptExpr", 3, 6},                /* to verify */
    {"opAsmLabelAttr", -1, 7},
    {"opAtomicExpr", 1, 8},
    {"opBinaryOperator", 3, 9},                    /* done */
    {"opBreakStmt", 0, 10},                        /* done */
    {"opCStyleCastExpr", 2, 11},                   /* done */
    {"opCXX11NoReturnAttr ",-1, 12},
    {"opCXXBindTemporaryExpr", -1, 13},            /* no TTML */
    {"opCXXBoolLiteralExpr", 0, 14},               /* done */
    {"opCXXCatchStmt", 2, 15},                     /* done */
    {"opCXXConstCastExpr", 2, 16},                 /* done */
    {"opCXXConstructExpr", -1, 17},                /* to verify */      
    {"opCXXConstructorDecl", -1, 18},              /* TTML */       //Objet
    {"opCXXConversionDecl", -1, 19},
    {"opCXXCtorInitializer", 2, 20},               /* TTML */       //Objet
    {"opCXXDeleteExpr", 2, 21},                    /* done */       
    {"opCXXDependentScopeMemberExpr", -1, 22},
    {"opCXXDestructorDecl", -1, 23},               /* TTML */       //Objet
    {"opCXXDynamicCastExpr", 2, 24},               /* done */
    {"opCXXFunctionalCastExpr", 2, 25},            /* done */
    {"opCXXMemberCallExpr", -1, 26},               /* to verify */  
    {"opCXXMethodDecl", -1, 27},                   /* done */       
    {"opCXXNewExpr", -1, 28},                      /* done */       
    {"opCXXNoexceptExpr", -1, 29},
    {"opCXXNullPtrLiteralExpr", 0, 30},            /* TTML */       //Other
    {"opCXXOperatorCallExpr", -1, 31},             /* TTML */       //Objet
    {"opCXXPseudoDestructorExpr", -1, 32},
    {"opCXXRecordDecl", -1, 33},                   /* TTML */       //Objet
    {"opCXXReinterpretCastExpr", 2, 34},           /* done */
    {"opCXXScalarValueInitExpr", 0, 35},           /* to verify */
    {"opCXXStaticCastExpr", 2, 36},                /* done */
    {"opCXXTemporaryObjectExpr", -1, 37},          /* to verify */
    {"opCXXThisExpr", 0, 38},                      /* done */
    {"opCXXThrowExpr", 1, 39},                     /* done */
    {"opCXXTryStmt", -1, 40},                      /* to verify */
    {"opCXXUnresolvedConstructExpr", -1, 41},      /* TTML */       //Trouver un cas
    {"opCallExpr", -1, 42},                        /* done */
    {"opCaseStmt", 1, 43},                         /* done */       
    {"opCharacterLiteral", 0, 44},                 /* done */
    {"opClassTemplateDecl", -1, 45},               /* no TTML */
    {"opClassTemplatePartialSpecializationDecl", -1, 46},
    {"opClassTemplateSpecialization", -1, 47},     /* no TTML */
    {"opClassTemplateSpecializationDecl", -1, 48}, /* to verify */
    {"opCompoundAssignOperator", 3, 49},           /* done */
    {"opCompoundLiteralExpr", -1, 50},
    {"opCompoundStmt", -1, 51},                    /* done */
    {"opConditionalOperator", 3, 52},              /* to verify */
    {"opConstAttr", -1, 53},
    {"opContinueStmt", 0, 54},                     /* done */
    {"opDeclRefExpr", 1, 55},                      /* done */
    {"opDeclStmt", -1, 56},                        /* done */
    {"opDefaultStmt", 0, 57},                      /* done */       
    {"opDependentScopeDeclRefExpr", -1, 58},
    {"opDeprecatedAttr", -1, 59},
    {"opDoStmt", 2, 60},                           /* done */
    {"opEmptyDecl", -1, 61},                       /* unused */
    {"opEnumConstantDecl", -1, 62},                /* done */
    {"opEnumDecl", -1, 63},                        /* done */
    {"opExprWithCleanups", 0, 64},                 /* no TTML. */
    {"opFunctionType", -1, 65},                    /* TTML */
    {"opFieldDecl", -1, 66},                       /* done */   
    {"opFloatingLiteral", 0, 67},                  /* done */
    {"opForStmt", 4, 68},                          /* done */
    {"opFormatAttr", -1, 69},
    {"opFriendDecl", -1, 70},
    {"opFunction", -1, 71},                        /* Unused ? */
    {"opFunctionDecl", -1, 72},                    /* done */
    {"opFunctionTemplateDecl", -1, 73},            /* no TTML */
    {"opGCCAsmStmt", -1, 74},                    
    {"opGNUNullExpr", 0, 75},
    {"opIfStmt", -1, 76},                          /* done */
    {"opImplicitCastExpr", 1, 77},                 /* no TTML. */
    {"opImplicitValueInitExpr", 0, 78},            /* to verify */
    {"opIndirectFieldDecl", -1, 79},
    {"opInitListExpr", -1, 80},                    /* TTML */
    {"opIntegerLiteral", 0, 81},                   /* done */
    {"opLinkageSpecDecl", -1, 82},
    {"opMaterializeTemporaryExpr", 0, 83},         /* no TTML. */
    {"opMayAliasAttr", -1, 84},
    {"opMemberExpr",3, 85},                        /* to verify */      
    {"opModeAttr",-1, 86},
    {"opNamespaceDecl",-1, 87},                    /* TTML */       //Namespace
    {"opNoDebugAttr",-1, 88},
    {"opNoThrowAttr",-1, 89},
    {"opNonNullAttr",-1, 90},
    {"opNonTypeTemplateParmDecl",-1, 91},
    {"opInclude",2, 92},                           /* TTML */
    {"opNullStmt",-1, 93},                         /* no TTML. */
    {"opPackExpansionExpr",-1, 94},
    {"opPackedAttr",-1, 95},
    {"opParenExpr",1, 96},                         /* done */
    {"opParenListExpr",-1, 97},
    {"opParmVarDecl", -1, 98},                     /* done */
    {"opPureAttr",-1, 99},
    {"opRestrictAttr",-1, 100},
    {"opReturnStmt", -1, 101},                     /* done */ 
    {"opReturnsTwiceAttr",-1, 102},
    {"opShuffleVectorExpr",-1, 103},
    {"opSizeOfPackExpr",-1, 104},
    {"opStaticAssertDecl",-1, 105},
    {"opStringLiteral", 0, 106},                   /* done */
    {"opSubstNonTypeTemplateParmExpr",-1, 107},
    {"opSwitchStmt",2, 108},                       /* done */
    {"opTemplateArgument",-1, 109},
    {"opTemplateTemplateParmDecl",-1, 110},
    {"opTemplateTypeParmDecl",-1, 111},
    {"opTranslationUnitDecl", -1, 112},            /* done */
    {"opTypeAliasDecl",2, 113},                    /* TTML */       //Namespace
    {"opTypeAliasTemplateDecl",-1, 114},
    {"opTypeTraitExpr",-1, 115},
    {"opTypedefDecl", 2, 116},                     /* TTML */       //Namespace
    {"opUnaryExprOrTypeTraitExpr", -1, 117},       /* TTML */       //Trouver un cas
    {"opUnaryOperator", 3, 118},                   /* done */
    {"opUnresolvedLookupExpr",-1, 119},
    {"opUnresolvedMemberExpr",-1, 120},
    {"opUnresolvedUsingValueDecl",-1, 121},
    {"opUnusedAttr",-1, 122},
    {"opUsingDecl", 2, 123},                       /* done */       
    {"opUsingDirectiveDecl",1, 124},               /* done */       
    {"opScopeAccess",2, 125},                      /* done */
    {"opVarDecl", -1, 126},                        /* done */
    {"opVisibilityAttr",-1, 127},
    {"opWarnUnusedResultAttr",-1, 128},
    {"opWeakRefAttr",-1, 129},
    {"opWhileStmt",2, 130},                        /* done */
    {"opReference",2, 131},                        
    {"opPointerType",1, 132},                     
    {"opQualifiers",-1, 133},                      
    {"opArrayType",2, 134},                       
    {"opType", 2, 135},                           /* done */
    {"opName", 0, 136},                            /* done */   
    {"opGotoStmt", 1, 137},                        /* done */
    {"opLabelStmt", 2, 138},                       /* done */
    {"opKeyword", 0, 139},                         /* done */
    {"opVAArgExpr", 2, 140},                       /* TTML */       //Other
    {"opCXXDefaultArgExpr", 0, 141},               /* TTML */       //Other
    {"opArrayType", 2, 142},                       /* done */
    {"opCXXForRangeStmt", 3, 143},                 /* to verify */
    {"opNamespaceAliasDecl", 2, 144},              /* TTML */       //Namespace
    {"opPredefinedExpr", 1, 145},                  /* no TTML. */
    {"opVarTemplateDecl", -1, 146},                /* no TTML */
    {"opVarTemplateSpecializationDecl", -1 ,147},  /* to verify */
    {"opCXXStdInitializerListExpr",1,148},
    {"opCXXTypeidExpr",1,149},
    {"opOpaqueValueExpr",-1,150},
    {"opConstantExpr",1,151},
    {"opEnReserve6",-1,152},                       /* In case clang needs more ops... */
    {"opEnReserve5",-1,153},                       /* In case clang needs more ops... */
    {"opEnReserve4",-1,154},                       /* In case clang needs more ops... */
    {"opEnReserve3",-1,155},                       /* In case clang needs more ops... */
    {"opEnReserve2",-1,156},                       /* In case clang needs more ops... */
    {"opEnReserve1",-1,157},                       /* In case clang needs more ops... */
    {"opEnReserve",-1,158},                        /* In case clang needs more ops... */
    {"dummyListop",-1,159},                        /* Dummy list operator.    Doesn't belong to clang */
    {"dummyAtomop",0,160},                         /* Dummy atomic operator.  Doesn't belong to clang */
    {"dummyArity1op",1,161},                       /* Dummy arity 1 operator. Doesn't belong to clang */
    {"dummyArity2op",2,162},                       /* Dummy arity 2 operator. Doesn't belong to clang */
    {"dummyArity3op",3,163},                       /* Dummy arity 3 operator. Doesn't belong to clang */
    {"dummyArity4op",4,164},                       /* Dummy arity 4 operator. Doesn't belong to clang */
    {"dummyArity5op",5,165},                       /* Dummy arity 5 operator. Doesn't belong to clang */
    {"dummyArity6op",6,166},                       /* Dummy arity 6 operator. Doesn't belong to clang */
};

static short clangNullTreeCode = 0 ;
static short clangLabelTreeCode = 203 ;
static short clangLabelsTreeCode = 291 ;
static short clangLabstatTreeCode = 142 ;
static short clangCommentTreeCode = 293 ;
static short clangCommentsTreeCode = 294 ;



