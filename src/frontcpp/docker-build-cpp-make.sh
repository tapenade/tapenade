#!/usr/bin/env bash

docker build -f Dockerfile-cpp-make -t registry.gitlab.inria.fr/tapenade/distrib:latest -t registry.gitlab.inria.fr/tapenade/distrib:$(cat ../../build/resources/main/gittag) .

if [ "$(uname)" = "Linux" ]; then
  docker push registry.gitlab.inria.fr/tapenade/distrib
else
  echo "docker push Time out on macos"
fi
