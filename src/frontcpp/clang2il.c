
/****** GENERATED FILE : DO NOT EDIT !!!! ******/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "../front/cvtp.h"
#include "../front/treeBuilder.h"
#include "tablesclang.c"
#include "../../build/generated-src/c/main/front/tablesil.c"

#define new(TYPE) (TYPE*)malloc(sizeof(TYPE))

/* TYPES: */

/* Type for "ttml function for translation of trees with a fixed root operator". */
typedef void (TTMLOperatorTreeTranslator)(Tree*, int, short) ;

/* Used to cut a ListTree in two ListTree's */
typedef struct _Cutter{
  ListTree **cut ;
  ListTree *left, *right ;
}Cutter ;

/* GLOBAL VARIABLES: */
static TreeBuilder *targetBuilder = NULL ;
typedef struct TargetBuilderStack_ { 
    TreeBuilder* top; 
    struct TargetBuilderStack_* next;
} TargetBuilderStack;
static TargetBuilderStack* stack = NULL ;
static FILE *protocolOutputStream = NULL ;
static const char *(contextNames[]) = {"TOP", "block", "inSwitch", "type", "inFriends"} ;

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) ;

void sendTreeProtocol(Tree *tree, FILE *protocolOutputStream) {
  if ((tree == NULL) || (tree->oper == NULL)) {
    fprintf(stdout, "incomplete tree\n") ;
  } else {
    if (tree->annotations != NULL) {
      KeyListTree *tlAnnotations = tree->annotations ;
      while (tlAnnotations != NULL) {
	fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
	fprintf(protocolOutputStream, "%s\n", tlAnnotations->key) ;
	sendTreeProtocol(tlAnnotations->tree, protocolOutputStream) ;
	tlAnnotations = tlAnnotations->next ;
      }
    }
    fprintf(protocolOutputStream, "%d\n", tree->oper->rank) ;
    if (tree->oper->arity == 0) {
        if (tree->contents.value)
	           fprintf(protocolOutputStream, "%s\n", tree->contents.value) ;
    } else {
      sendListTreeProtocol(tree->contents.sons, protocolOutputStream) ;
      if (tree->oper->arity == -1)
        fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
    }
  }
}

void sendListTreeProtocol(ListTree *listTree, FILE *protocolOutputStream) {
    while (listTree != NULL) {
      sendTreeProtocol(listTree->tree, protocolOutputStream) ;
      listTree = listTree->next ;
    }
}

void sendOp(int operRk) {
  if (stack->top)
    startTree(stack->top, operRk) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", operRk) ;
  else
    printf("%d\n", operRk) ;
}

void sendValue(const char *value) {
  if (stack->top)
    putValue(stack->top, strdup(value)) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%s\n", value) ;
  else
    printf("%s\n", value) ;
}

void endList() {
  if (stack->top)
    terminateListTree(stack->top) ;
  else if (protocolOutputStream)
    fprintf(protocolOutputStream, "%d\n", ilEndOfListCode) ;
  else
    printf("%d\n", ilEndOfListCode) ;
}

void sendAnnotation(const char *name) {
    if (stack->top) {
      startAnnotation(stack->top, name) ;
  } else if (protocolOutputStream) {
    fprintf(protocolOutputStream, "%d\n", ilAnnotationCode) ;
      fprintf(protocolOutputStream, "%s\n", name) ;
  } else {
      printf("%d\n", ilAnnotationCode) ;
      printf("%s\n", name) ;
  }
}

void acceptAnnotations() {
    if (stack->top) stack->top->acceptAnnotations = 1 ;
}

void sendTree(Tree *tree) {
  if (stack->top)
    putTree(stack->top, tree) ;
  else
    sendTreeProtocol(tree,
		     (protocolOutputStream)?protocolOutputStream:stdout) ;
}

void sendListTree(ListTree *listTree) {
  if (stack->top)
    putListTree(stack->top, listTree) ;
  else
    sendListTreeProtocol(listTree,
	                        (protocolOutputStream)?protocolOutputStream:stdout) ;
}

Tree *sendDummyOp(int arity) {
    if (stack->top)
	return startDummyTree(stack->top, arity) ;
    else {
	printf("Cannot start a dummy tree with no targetBuilder\n") ;
	return NULL ;
    }
}

void cutAt(Cutter *cutter, ListTree *listTree, int cutPos) {
  /* only implemented case: cutPos < 0 */
  int i = listTreeLength(listTree) + cutPos ;
  if (i==0) {
    cutter->left = NULL ;
    cutter->right = listTree ;
    cutter->cut = NULL ;
  } else {
    cutter->left = listTree ;
    while ((--i)>0) listTree = listTree->next ;
    cutter->right = listTree->next ;
    listTree->next = NULL ;
    cutter->cut = &(listTree->next) ;
  }
}

void undoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=cutter->right ;
}

void redoCut(Cutter *cutter) {
  if (cutter->cut != NULL) *(cutter->cut)=NULL ;
}

/*Forward declarations: */
void clang2ilTTList(ListTree *listTree, int context, short resultKind) ;
void clang2ilTT(Tree *fTree, int context, short resultKind) ;

void ttmlNoRule(Tree *fTree, int context, short resultKind) {
      char *message = (char*)malloc(200) ;
      sprintf(message, "Unexpected source operator: %s (%s)",
		   fTree->oper->name, contextNames[context]) ;
      sendOp(ilParsingErrorCode) ;
      sendValue(message) ;
}

void action1(Tree *fTree, short resultKind) {
    /* RULE type::T -> 
              T  */
    clang2ilTT(fTree, 0, resultKind) ;
}

void clang2ilTT0(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opNone() -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT2(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139)) {
    /* RULE opAccessSpecDecl(opKeyword A) -> 
              accessDecl(ident A,expressions())  */
    sendOp(2) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(71) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT6(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opArraySubscriptExpr(Type,Pointer,Shift) -> 
              pointerAccess(Pointer,Shift)  */
    sendOp(151) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT8(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE opAtomicExpr(Expr) -> 
              Expr  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT9(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"=") == 0)) {
    /* RULE opBinaryOperator(opName:"=",Exp1,Exp2) -> 
              assign(Exp1,Exp2)  */
    sendOp(14) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"<<") == 0)) {
    /* RULE opBinaryOperator(opName:"<<",Exp1,Exp2) -> 
              leftShift(Exp1,Exp2)  */
    sendOp(116) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,">>") == 0)) {
    /* RULE opBinaryOperator(opName:">>",Exp1,Exp2) -> 
              rightShift(Exp1,Exp2)  */
    sendOp(168) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"&&") == 0)) {
    /* RULE opBinaryOperator(opName:"&&",Exp1,Exp2) -> 
              and(Exp1,Exp2)  */
    sendOp(6) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"&") == 0)) {
    /* RULE opBinaryOperator(opName:"&",Exp1,Exp2) -> 
              bitAnd(Exp1,Exp2)  */
    sendOp(18) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"||") == 0)) {
    /* RULE opBinaryOperator(opName:"||",Exp1,Exp2) -> 
              or(Exp1,Exp2)  */
    sendOp(143) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"|") == 0)) {
    /* RULE opBinaryOperator(opName:"|",Exp1,Exp2) -> 
              bitOr(Exp1,Exp2)  */
    sendOp(22) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"^") == 0)) {
    /* RULE opBinaryOperator(opName:"^",Exp1,Exp2) -> 
              bitXor(Exp1,Exp2)  */
    sendOp(24) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"==") == 0)) {
    /* RULE opBinaryOperator(opName:"==",Exp1,Exp2) -> 
              eq(Exp1,Exp2)  */
    sendOp(68) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"!=") == 0)) {
    /* RULE opBinaryOperator(opName:"!=",Exp1,Exp2) -> 
              neq(Exp1,Exp2)  */
    sendOp(137) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,">") == 0)) {
    /* RULE opBinaryOperator(opName:">",Exp1,Exp2) -> 
              gt(Exp1,Exp2)  */
    sendOp(95) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"<") == 0)) {
    /* RULE opBinaryOperator(opName:"<",Exp1,Exp2) -> 
              lt(Exp1,Exp2)  */
    sendOp(122) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,">=") == 0)) {
    /* RULE opBinaryOperator(opName:">=",Exp1,Exp2) -> 
              ge(Exp1,Exp2)  */
    sendOp(92) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"<=") == 0)) {
    /* RULE opBinaryOperator(opName:"<=",Exp1,Exp2) -> 
              le(Exp1,Exp2)  */
    sendOp(115) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"+") == 0)) {
    /* RULE opBinaryOperator(opName:"+",Exp1,Exp2) -> 
              add(Exp1,Exp2)  */
    sendOp(3) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"-") == 0)) {
    /* RULE opBinaryOperator(opName:"-",Exp1,Exp2) -> 
              sub(Exp1,Exp2)  */
    sendOp(181) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"*") == 0)) {
    /* RULE opBinaryOperator(opName:"*",Exp1,Exp2) -> 
              mul(Exp1,Exp2)  */
    sendOp(133) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"/") == 0)) {
    /* RULE opBinaryOperator(opName:"/",Exp1,Exp2) -> 
              div(Exp1,Exp2)  */
    sendOp(62) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"%") == 0)) {
    /* RULE opBinaryOperator(opName:"%",Exp1,Exp2) -> 
              mod(Exp1,Exp2)  */
    sendOp(126) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,",") == 0)) {
    /* RULE opBinaryOperator(opName:",",Exp1,Exp2) -> 
              expressions[(Exp1,Exp2)]  */
    sendOp(71) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT10(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opBreakStmt B -> 
              break()  */
    sendOp(30) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT11(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCStyleCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"CStyle"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("CStyle") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT14(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXBoolLiteralExpr X -> 
              boolCst X  */
    sendOp(28) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT15(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXCatchStmt(Decl,Body) -> 
              catch(Decl,Body)  */
    sendOp(33) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT16(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXConstCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"ConstCast"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("ConstCast") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT17(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->next->tree->oper->rank == 139) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 150)) {
    /* RULE opCXXConstructExpr(Class,opKeyword T,opOpaqueValueExpr X) -> 
              constructorCall(Class,ident T,expressions[])  */
    sendOp(47) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->next->tree->contents.value) ;
    sendOp(71) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->next->tree->oper->rank == 139)) {
    /* RULE opCXXConstructExpr[(Class,opKeyword T),Args] -> 
              constructorCall(Class,ident T,expressions[Args])  */
    sendOp(47) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->next->tree->contents.value) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT18(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 6) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 159)) {
    /* RULE opCXXConstructorDecl[(Name,Type,Modifiers,SolvedName,dummyListop Args),X,(Body)] -> 
              constructor(Modifiers,Name,varDeclarations Args,blockStatement[X],Body)  */
    Cutter *cutter0 ;
    cutter0 = new(Cutter) ;
    sendOp(46) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(199) ;
    clang2ilTTList(fTree->contents.sons->next->next->next->next->tree->contents.sons, 0, -1) ;
    endList() ;
    sendOp(27) ;
    cutAt(cutter0, fTree->contents.sons->next->next->next->next->next, -1) ;
    clang2ilTTList(cutter0->left, 0, -1) ;
    endList() ;
    undoCut(cutter0) ;
    clang2ilTT(tailSon(fTree->contents.sons,0), 0, 1) ;
    free(cutter0) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 5) &&
      (fTree->contents.sons->next->next->next->next->tree->oper->rank == 159)) {
    /* RULE opCXXConstructorDecl(Name,Type,Modifiers,SolvedName,dummyListop Args) -> 
              varDeclaration(Modifiers,none(),declarators(functionDeclarator(Name,argDeclarations Args)))  */
    sendOp(198) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(138) ;
    sendOp(54) ;
    sendOp(90) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(8) ;
    clang2ilTTList(fTree->contents.sons->next->next->next->next->tree->contents.sons, 0, -1) ;
    endList() ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT19(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXConversionDecl X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT20(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == -1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 135)) {
    /* RULE opCXXCtorInitializer(opType(M,T),Construct) -> 
              (Construct)  */
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 85)) {
    /* RULE opCXXCtorInitializer(Member:=opMemberExpr(A,B,C),Expr) -> 
              assign(Member,Expr)  */
    sendOp(14) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT21(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXDeleteExpr(Kind,Member) -> 
              deallocate(Member,none())  */
    sendOp(52) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT23(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 5) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opCXXDestructorDecl(opName N,Type,Modifiers,ClassName,Body) -> 
              destructor(Modifiers,ident N,Body)  */
    sendOp(56) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->next->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 4) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opCXXDestructorDecl(opName N,Type,Modifiers,ClassName) -> 
              destructor(Modifiers,ident N,none())  */
    sendOp(56) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT24(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXDynamicCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"DynamicCast"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("DynamicCast") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT25(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXFunctionalCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"FunctionalCast"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("FunctionalCast") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT26(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 85) &&
      (fTree->contents.sons->tree->contents.sons->next->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.sons->next->tree->contents.value,"->") == 0)) {
    /* RULE opCXXMemberCallExpr[(opMemberExpr(Object,opKeyword:"->",Name)),Args] -> 
              call(pointerAccess(Object,none()),Name,expressions[Args])  */
    sendOp(31) ;
    sendOp(151) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 85) &&
      (fTree->contents.sons->tree->contents.sons->next->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.sons->next->tree->contents.value,".") == 0)) {
    /* RULE opCXXMemberCallExpr[(opMemberExpr(Object,opKeyword:".",Name)),Args] -> 
              call(Object,Name,expressions[Args])  */
    sendOp(31) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT27(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 5) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 133) &&
      (tailSon(fTree->contents.sons,0)->oper->rank == 51)) {
    /* RULE opCXXMethodDecl[(opName N,Type,opQualifiers Quals,ParentClass),Args,(Content:=opCompoundStmt[Body])] -> 
              function(modifiers[Quals],Type,ParentClass,ident N,varDeclarations[Args],Content)  */
    Cutter *cutter0 ;
    cutter0 = new(Cutter) ;
    sendOp(89) ;
    sendOp(130) ;
    clang2ilTTList(fTree->contents.sons->next->next->tree->contents.sons, 0, -1) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(199) ;
    cutAt(cutter0, fTree->contents.sons->next->next->next->next, -1) ;
    clang2ilTTList(cutter0->left, 0, -1) ;
    endList() ;
    undoCut(cutter0) ;
    clang2ilTT(tailSon(fTree->contents.sons,0), 0, 1) ;
    free(cutter0) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 4) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 133)) {
    /* RULE opCXXMethodDecl[(opName N,Type,opQualifiers Quals,ParentClass),Args] -> 
              varDeclaration(modifiers[Quals],Type,declarators(functionDeclarator(ident N,argDeclarations[Args])))  */
    sendOp(198) ;
    sendOp(130) ;
    clang2ilTTList(fTree->contents.sons->next->next->tree->contents.sons, 0, -1) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(90) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(8) ;
    clang2ilTTList(fTree->contents.sons->next->next->next->next, 0, -1) ;
    endList() ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT28(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"operator new") == 0)) {
    /* RULE opCXXNewExpr(opName:"operator new",Type,NumberOfTs) -> 
              allocate(none(),none(),Type,none(),none())  */
    sendOp(5) ;
    sendOp(138) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 4) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"operator new") == 0)) {
    /* RULE opCXXNewExpr[(opName:"operator new",Type,NumberOfTs,Init),Rest] -> 
              allocate(none(),none(),Type,none(),Init)  */
    sendOp(5) ;
    sendOp(138) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"operator new[]") == 0)) {
    /* RULE opCXXNewExpr(opName:"operator new[]",Type,NumberOfTs) -> 
              allocate(none(),NumberOfTs,Type,none(),none())  */
    sendOp(5) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 4) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"operator new[]") == 0)) {
    /* RULE opCXXNewExpr[(opName:"operator new[]",Type,NumberOfTs,Init),Rest] -> 
              allocate(none(),NumberOfTs,Type,none(),Init)  */
    sendOp(5) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT29(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE opCXXNoexceptExpr(X) -> 
              X  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT30(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXNullPtrLiteralExpr Empty -> 
              call(none(),ident:"NULL",expressions[])  */
    sendOp(31) ;
    sendOp(138) ;
    sendOp(96) ;
    sendValue("NULL") ;
    sendOp(71) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT31(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"Function") == 0)) {
    /* RULE opCXXOperatorCallExpr[(opKeyword:"Function",Where),Args] -> 
              call(none(),Where,expressions[Args])  */
    sendOp(31) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (fTree->contents.sons->next->tree->oper->rank == 55)) {
    /* RULE opCXXOperatorCallExpr[(opKeyword Method,opDeclRefExpr(Name),Lhs),Args] -> 
              call(Lhs,Name,expressions[Args])  */
    sendOp(31) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree->contents.sons->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next->next->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE opCXXOperatorCallExpr[(Operator),Args] -> 
              call(none(),Operator,expressions[Args])  */
    sendOp(31) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT33(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"class") == 0) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 133) &&
      (listTreeLength(fTree->contents.sons->next->next->tree->contents.sons) >= 1)) {
    /* RULE opCXXRecordDecl[(opKeyword:"class",Name,opQualifiers[Modifiers,(Parent)]),Decls] -> 
              class(modifiers[],Name,parentClasses(parentClass(modifiers[Modifiers],Parent)),declarations[Decls])  */
    Cutter *cutter0 ;
    cutter0 = new(Cutter) ;
    sendOp(36) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(148) ;
    sendOp(147) ;
    sendOp(130) ;
    cutAt(cutter0, fTree->contents.sons->next->next->tree->contents.sons, -1) ;
    clang2ilTTList(cutter0->left, 0, -1) ;
    endList() ;
    undoCut(cutter0) ;
    clang2ilTT(tailSon(fTree->contents.sons->next->next->tree->contents.sons,0), 0, 1) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next->next, 0, -1) ;
    endList() ;
    free(cutter0) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"class") == 0)) {
    /* RULE opCXXRecordDecl[(opKeyword:"class",Name),Decls] -> 
              class(modifiers[],Name,parentClasses(),declarations[Decls])  */
    sendOp(36) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(148) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"struct") == 0)) {
    /* RULE opCXXRecordDecl[(opKeyword:"struct",Name),Decls] -> 
              recordType(Name,modifiers[],declarations[block::Decls])  */
    sendOp(160) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(130) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next, 1, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"union") == 0)) {
    /* RULE opCXXRecordDecl[(opKeyword:"union",Name),Decls] -> 
              unionType(Name,declarations[block::Decls])  */
    sendOp(194) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next, 1, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT34(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXReinterpretCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"ReinterpretCast"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("ReinterpretCast") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT35(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (strcmp(fTree->contents.value,"default") == 0)) {
    /* RULE opCXXScalarValueInitExpr:"default" -> 
              intCst:"0"  */
    sendOp(103) ;
    sendValue("0") ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT36(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXStaticCastExpr(Type,Exp) -> 
              cast(Type,Exp)^{castStyle:ident:"StaticCast"}  */
    sendAnnotation("castStyle") ;
    sendOp(96) ;
    sendValue("StaticCast") ;
    acceptAnnotations() ;
    sendOp(32) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT37(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->next->tree->oper->rank == 139)) {
    /* RULE opCXXTemporaryObjectExpr[(Classe,opKeyword T),Args] -> 
              constructorCall(Classe,ident T,expressions[Args])  */
    sendOp(47) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->next->tree->contents.value) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT38(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (strcmp(fTree->contents.value,"this") == 0)) {
    /* RULE opCXXThisExpr:"this" -> 
              ident:"this"  */
    sendOp(96) ;
    sendValue("this") ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT39(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXThrowExpr(Cst) -> 
              throw(Cst)  */
    sendOp(187) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT40(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE opCXXTryStmt[(Body),Catchs] -> 
              try(Body,catchs[Catchs],none())  */
    sendOp(190) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(34) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT42(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 55) &&
      (fTree->contents.sons->tree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.sons->tree->contents.value,"pow") == 0)) {
    /* RULE opCallExpr(opDeclRefExpr(opName:"pow"),Arg1,Arg2) -> 
              power(Arg1,Arg2)  */
    sendOp(155) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE opCallExpr[(Where),Args] -> 
              call(none(),Where,expressions[Args])  */
    sendOp(31) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 85) &&
      (fTree->contents.sons->tree->contents.sons->next->tree->oper->rank == 139) &&
      (fTree->contents.sons->tree->contents.sons->next->next->tree->oper->rank == 136)) {
    /* RULE opCallExpr[(opMemberExpr(Object,opKeyword K,opName N)),Args] -> 
              call(Object,ident N,expressions[Args])  */
    sendOp(31) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.sons->next->next->tree->contents.value) ;
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT44(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCharacterLiteral X -> 
              stringCst X  */
    sendOp(179) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT48(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"class") == 0) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 133) &&
      (listTreeLength(fTree->contents.sons->next->next->tree->contents.sons) >= 1)) {
    /* RULE opClassTemplateSpecializationDecl[(opKeyword:"class",Name,opQualifiers[Modifiers,(Parent)]),Decls] -> 
              class(modifiers[],Name,parentClasses(parentClass(modifiers[Modifiers],Parent)),declarations[Decls])  */
    Cutter *cutter0 ;
    cutter0 = new(Cutter) ;
    sendOp(36) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(148) ;
    sendOp(147) ;
    sendOp(130) ;
    cutAt(cutter0, fTree->contents.sons->next->next->tree->contents.sons, -1) ;
    clang2ilTTList(cutter0->left, 0, -1) ;
    endList() ;
    undoCut(cutter0) ;
    clang2ilTT(tailSon(fTree->contents.sons->next->next->tree->contents.sons,0), 0, 1) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next->next, 0, -1) ;
    endList() ;
    free(cutter0) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"class") == 0)) {
    /* RULE opClassTemplateSpecializationDecl[(opKeyword:"class",Name),Decls] -> 
              class(modifiers[],Name,parentClasses(),declarations[Decls])  */
    sendOp(36) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(148) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 2) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"struct") == 0)) {
    /* RULE opClassTemplateSpecializationDecl[(opKeyword:"struct",Name),Decls] -> 
              recordType(Name,modifiers[],declarations[block::Decls])  */
    sendOp(160) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(130) ;
    endList() ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next->next, 1, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT49(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"+=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"+=",Exp1,Exp2) -> 
              plusAssign(Exp1,Exp2)  */
    sendOp(150) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"-=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"-=",Exp1,Exp2) -> 
              minusAssign(Exp1,Exp2)  */
    sendOp(125) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"*=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"*=",Exp1,Exp2) -> 
              timesAssign(Exp1,Exp2)  */
    sendOp(189) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"/=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"/=",Exp1,Exp2) -> 
              divAssign(Exp1,Exp2)  */
    sendOp(63) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"%=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"%=",Exp1,Exp2) -> 
              modAssign(Exp1,Exp2)  */
    sendOp(127) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"&=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"&=",Exp1,Exp2) -> 
              bitAndAssign(Exp1,Exp2)  */
    sendOp(19) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"|=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"|=",Exp1,Exp2) -> 
              bitOrAssign(Exp1,Exp2)  */
    sendOp(23) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"^=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"^=",Exp1,Exp2) -> 
              bitXorAssign(Exp1,Exp2)  */
    sendOp(25) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,">>=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:">>=",Exp1,Exp2) -> 
              rightShiftAssign(Exp1,Exp2)  */
    sendOp(169) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"<<=") == 0)) {
    /* RULE opCompoundAssignOperator(opName:"<<=",Exp1,Exp2) -> 
              leftShiftAssign(Exp1,Exp2)  */
    sendOp(117) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT50(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCompoundLiteralExpr X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT51(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCompoundStmt X -> 
              blockStatement block::X  */
    sendOp(27) ;
    clang2ilTTList(fTree->contents.sons, 1, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT52(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opConditionalOperator(Test,Then,Else) -> 
              ifExpression(Test,Then,Else)  */
    sendOp(99) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT54(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opContinueStmt C -> 
              continue()  */
    sendOp(49) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT55(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE opDeclRefExpr(Name) -> 
              Name  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT56(Tree *fTree, int context, short resultKind) {
  if (!context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE opDeclStmt(VarDecl) -> 
              VarDecl  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opDeclStmt[VarDecls] -> 
              expressions[VarDecls]  */
    sendOp(71) ;
    clang2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT58(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opDependentScopeDeclRefExpr X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT60(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opDoStmt(Body,Test) -> 
              loop(none(),none(),until(Test),Body)  */
    sendOp(121) ;
    sendOp(138) ;
    sendOp(138) ;
    sendOp(195) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT62(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 1) &&
      (fTree->contents.sons->tree->oper->rank == 106)) {
    /* RULE opEnumConstantDecl(opStringLiteral Name) -> 
              ident Name  */
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 106)) {
    /* RULE opEnumConstantDecl(opStringLiteral Name,Value) -> 
              assign(ident Name,Value)  */
    sendOp(14) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT63(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opEnumDecl[(opName N),EnumCsts] -> 
              enumType(ident N,declarators[EnumCsts])  */
    sendOp(67) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(54) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT65(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE opFunctionType[(Return),Args] -> 
              functionType(Return,argDeclarations[Args])  */
    sendOp(91) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(8) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT66(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opFieldDecl(opName N,Type) -> 
              varDeclaration(modifiers[],Type,declarators(ident N))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 159)) {
    /* RULE opFieldDecl(dummyListop[DECLS],Type) -> 
              varDeclaration(modifiers[],Type,declarators[DECLS])  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    clang2ilTTList(fTree->contents.sons->tree->contents.sons, 0, -1) ;
    endList() ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 0) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 0)) {
    /* RULE opFieldDecl(opNone(),ClassOrStructDecl,opNone()) -> 
              ClassOrStructDecl  */
    clang2ilTT(fTree->contents.sons->next->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opFieldDecl(opName N,Type,Value) -> 
              varDeclaration(modifiers[],Type,declarators(assign(ident N,Value)))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(14) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT67(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opFloatingLiteral X -> 
              realCst X  */
    sendOp(159) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT68(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opForStmt(Init,Test,Loop,Body) -> 
              loop(none(),none(),for(Init,Test,Loop),Body)  */
    sendOp(121) ;
    sendOp(138) ;
    sendOp(138) ;
    sendOp(79) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT70(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opFriendDecl FriendDeclarations -> 
              friendsDeclaration inFriends::FriendDeclarations  */
    sendOp(88) ;
    clang2ilTTList(fTree->contents.sons, 4, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT72(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 4) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (tailSon(fTree->contents.sons,0)->oper->rank == 51)) {
    /* RULE opFunctionDecl[(Quals,opName N,Type),Decls,(Content:=opCompoundStmt[Body])] -> 
              function(Quals,Type,none(),ident N,varDeclarations[Decls],Content)  */
    Cutter *cutter0 ;
    cutter0 = new(Cutter) ;
    sendOp(89) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(138) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->next->tree->contents.value) ;
    sendOp(199) ;
    cutAt(cutter0, fTree->contents.sons->next->next->next, -1) ;
    clang2ilTTList(cutter0->left, 0, -1) ;
    endList() ;
    undoCut(cutter0) ;
    clang2ilTT(tailSon(fTree->contents.sons,0), 0, 1) ;
    free(cutter0) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3)) {
    /* RULE opFunctionDecl[(Quals,Name,Type),Args] -> 
              varDeclaration(Quals,Type,declarators(functionDeclarator(Name,argDeclarations[Args])))  */
    sendOp(198) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(90) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(8) ;
    clang2ilTTList(fTree->contents.sons->next->next->next, 0, -1) ;
    endList() ;
    endList() ;
  } else  if ((resultKind == 1) && (context == 4) &&
      (listTreeLength(fTree->contents.sons) >= 3)) {
    /* RULE inFriends::opFunctionDecl[(Quals,Name,Type),Args] -> 
              friendFunction(Type,functionDeclarator(Name,argDeclarations[Args]))  */
    sendOp(86) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(90) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(8) ;
    clang2ilTTList(fTree->contents.sons->next->next->next, 0, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT73(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE opFunctionTemplateDecl(Function) -> 
              Function  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 72)) {
    /* RULE opFunctionTemplateDecl[(Function:=opFunctionDecl[X]),Suite] -> 
              [(Function),Suite]  */
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT74(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opGCCAsmStmt X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT75(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opGNUNullExpr() -> 
              ident:"NULL"  */
    sendOp(96) ;
    sendValue("NULL") ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT76(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3)) {
    /* RULE opIfStmt(Cond,Then,Else) -> 
              if(Cond,Then,Else)  */
    sendOp(98) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE opIfStmt(Cond,Then) -> 
              if(Cond,Then,none())  */
    sendOp(98) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT78(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (strcmp(fTree->contents.value,"default") == 0)) {
    /* RULE opImplicitValueInitExpr:"default" -> 
              intCst:"0"  */
    sendOp(103) ;
    sendValue("0") ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT80(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"arrayfiller") == 0)) {
    /* RULE opInitListExpr[(opKeyword:"arrayfiller"),Values] -> 
              arrayConstructor[Values]  */
    sendOp(10) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"array") == 0)) {
    /* RULE opInitListExpr[(opKeyword:"array"),Values] -> 
              arrayConstructor[Values]  */
    sendOp(10) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"struct") == 0)) {
    /* RULE opInitListExpr[(opKeyword:"struct"),Values] -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT81(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opIntegerLiteral X -> 
              intCst X  */
    sendOp(103) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT82(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opLinkageSpecDecl X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT85(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,".") == 0)) {
    /* RULE opMemberExpr(Object,opKeyword:".",Name) -> 
              fieldAccess(Object,Name)  */
    sendOp(75) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"->") == 0)) {
    /* RULE opMemberExpr(Object,opKeyword:"->",Name) -> 
              fieldAccess(pointerAccess(Object,none()),Name)  */
    sendOp(75) ;
    sendOp(151) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT87(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 1) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opNamespaceDecl[(opName N),Decls] -> 
              nameSpace(ident N,declarations[Decls])  */
    sendOp(136) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    sendOp(53) ;
    clang2ilTTList(fTree->contents.sons->next, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT92(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (fTree->contents.sons->next->tree->oper->rank == 139)) {
    /* RULE opInclude(opKeyword State,opKeyword File) -> 
              include()  */
    sendOp(101) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT96(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE opParenExpr(X) -> 
              X  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT97(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opParenListExpr X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT98(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"") == 0)) {
    /* RULE opParmVarDecl(opName:"",Type) -> 
              varDeclaration(modifiers[],Type,declarators())  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opParmVarDecl(opName N,Type) -> 
              varDeclaration(modifiers[],Type,declarators(ident N))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opParmVarDecl[(opName N,Type,Value),Rest] -> 
              varDeclaration(modifiers[],Type,declarators(assign(ident N,Value)))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(14) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT101(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE opReturnStmt(Exp) -> 
              return(Exp,none())  */
    sendOp(167) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 0)) {
    /* RULE opReturnStmt() -> 
              return(none(),none())  */
    sendOp(167) ;
    sendOp(138) ;
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT103(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opShuffleVectorExpr X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT104(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 0)) {
    /* RULE opSizeOfPackExpr[] -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT105(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opStaticAssertDecl X -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT106(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opStringLiteral X -> 
              stringCst X  */
    sendOp(179) ;
    sendValue((char *)fTree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT107(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) == 1)) {
    /* RULE opSubstNonTypeTemplateParmExpr(Expr) -> 
              Expr  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT108(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 51)) {
    /* RULE opSwitchStmt(Expression,opCompoundStmt(Body)*) -> 
              switch(Expression,switchCases(inSwitch::Body)*)  */
    ListTree *listStars ;
    int listStarsLength ;
    sendOp(183) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(185) ;
    listStars = fTree->contents.sons->next->tree->contents.sons ;
    listStarsLength = listTreeLength(listStars) ;
    while(listStarsLength > 0) {
    clang2ilTT(listStars->tree, 2, 1) ;
      listStars = listStars->next ;
      listStarsLength -= 1 ;
    }
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT112(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opTranslationUnitDecl X -> 
              blockStatement X  */
    sendOp(27) ;
    clang2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT113(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opTypeAliasDecl(opName NewType,Type) -> 
              typeDeclaration(ident NewType,Type)  */
    sendOp(191) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT114(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context &&
      (listTreeLength(fTree->contents.sons) >= 1)) {
    /* RULE opTypeAliasTemplateDecl[(TypeAlias),Ignored] -> 
              TypeAlias  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT115(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 0)) {
    /* RULE opTypeTraitExpr[] -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT116(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 136)) {
    /* RULE opTypedefDecl(Type,opName NewType) -> 
              typeDeclaration(ident NewType,Type)  */
    sendOp(191) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->next->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT117(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"sizeof") == 0)) {
    /* RULE opUnaryExprOrTypeTraitExpr(opName:"sizeof",Type) -> 
              sizeof(Type)  */
    sendOp(175) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"alignof") == 0)) {
    /* RULE opUnaryExprOrTypeTraitExpr(opName:"alignof",Type) -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT118(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"*") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"*",Exp) -> 
              pointerAccess(Exp,none())  */
    sendOp(151) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"postfix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"++") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"postfix",opName:"++",Exp) -> 
              unary(ident:"++postfix",Exp)  */
    sendOp(193) ;
    sendOp(96) ;
    sendValue("++postfix") ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"++") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"++",Exp) -> 
              unary(ident:"++prefix",Exp)  */
    sendOp(193) ;
    sendOp(96) ;
    sendValue("++prefix") ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"postfix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"--") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"postfix",opName:"--",Exp) -> 
              unary(ident:"--postfix",Exp)  */
    sendOp(193) ;
    sendOp(96) ;
    sendValue("--postfix") ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"--") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"--",Exp) -> 
              unary(ident:"--prefix",Exp)  */
    sendOp(193) ;
    sendOp(96) ;
    sendValue("--prefix") ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"&") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"&",Exp) -> 
              address(Exp)  */
    sendOp(4) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if (!context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"+") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"+",Exp) -> 
              Exp  */
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"-") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"-",Exp) -> 
              minus(Exp)  */
    sendOp(124) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"!") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"!",Exp) -> 
              not(Exp)  */
    sendOp(139) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"prefix") == 0) &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"~") == 0)) {
    /* RULE opUnaryOperator(opKeyword:"prefix",opName:"~",Exp) -> 
              bitNot(Exp)  */
    sendOp(21) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"!") == 0)) {
    /* RULE opUnaryOperator(Type,opName:"!",Exp) -> 
              not(Exp)  */
    sendOp(139) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 136) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"-") == 0)) {
    /* RULE opUnaryOperator(Type,opName:"-",Exp) -> 
              sub(intCst:"0",Exp)  */
    sendOp(181) ;
    sendOp(103) ;
    sendValue("0") ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opUnaryOperator(Type,Name,Exp) -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT123(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opUsingDecl(opName Name,Namespace) -> 
              using(Namespace,ident Name)  */
    sendOp(197) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT124(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opUsingDirectiveDecl(Namespace) -> 
              using(Namespace,none())  */
    sendOp(197) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT125(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opScopeAccess(Largest,Content) -> 
              scopeAccess(Largest,Content)  */
    sendOp(172) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT126(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 0) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 0)) {
    /* RULE opVarDecl(opNone(),Type,opNone()) -> 
              varDeclaration(modifiers[],type::Type,declarators())  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 3, 1) ;
    sendOp(54) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 159)) {
    /* RULE opVarDecl(dummyListop[DECLS],Type) -> 
              varDeclaration(modifiers[],type::Type,declarators[DECLS])  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 3, 1) ;
    sendOp(54) ;
    clang2ilTTList(fTree->contents.sons->tree->contents.sons, 0, -1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opVarDecl[(opName N,Type),UselessThings,(InitValue)] -> 
              varDeclaration(modifiers[],type::Type,declarators(assign(ident N,InitValue)))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 3, 1) ;
    sendOp(54) ;
    sendOp(14) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(tailSon(fTree->contents.sons,0), 0, 1) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2)) {
    /* RULE opVarDecl(VarOptInit,Type) -> 
              varDeclaration(modifiers[],type::Type,declarators(VarOptInit))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 3, 1) ;
    sendOp(54) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT130(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opWhileStmt(Test,Body) -> 
              loop(none(),none(),while(Test),Body)  */
    sendOp(121) ;
    sendOp(138) ;
    sendOp(138) ;
    sendOp(205) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT131(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"&") == 0)) {
    /* RULE opReference(opKeyword:"&",ReferencedType) -> 
              referenceType(ReferencedType)^{refKind:ident:"lvalue"}  */
    sendAnnotation("refKind") ;
    sendOp(96) ;
    sendValue("lvalue") ;
    acceptAnnotations() ;
    sendOp(163) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 139) &&
      (strcmp(fTree->contents.sons->tree->contents.value,"&&") == 0)) {
    /* RULE opReference(opKeyword:"&&",ReferencedType) -> 
              referenceType(ReferencedType)^{refKind:ident:"rvalue"}  */
    sendAnnotation("refKind") ;
    sendOp(96) ;
    sendValue("rvalue") ;
    acceptAnnotations() ;
    sendOp(163) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT132(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opPointerType(PointedType) -> 
              pointerType(PointedType)  */
    sendOp(154) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT133(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opQualifiers Quals -> 
              modifiers Quals  */
    sendOp(130) ;
    clang2ilTTList(fTree->contents.sons, 0, -1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT135(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && (context == 4)) {
    /* RULE inFriends::Type:=opType(QQ,TT) -> 
              friendType(Type)  */
    sendOp(87) ;
    clang2ilTT(fTree, 0, 1) ;
  } else  if (!context &&
      (fTree->contents.sons->tree->oper->rank == 133) &&
      (listTreeLength(fTree->contents.sons->tree->contents.sons) == 0)) {
    /* RULE opType(opQualifiers(),Type) -> 
              type::Type  */
    clang2ilTT(fTree->contents.sons->next->tree, 3, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opType(Quals,Type) -> 
              modifiedType(Quals,type::Type)  */
    sendOp(129) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 3, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT136(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"void") == 0)) {
    /* RULE type::opName:"void" -> 
              void()  */
    sendOp(203) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"int") == 0)) {
    /* RULE type::opName:"int" -> 
              integer()  */
    sendOp(104) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"long") == 0)) {
    /* RULE type::opName:"long" -> 
              modifiedType(modifiers(ident:"long"),integer())  */
    sendOp(129) ;
    sendOp(130) ;
    sendOp(96) ;
    sendValue("long") ;
    endList() ;
    sendOp(104) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"short") == 0)) {
    /* RULE type::opName:"short" -> 
              modifiedType(modifiers(ident:"short"),integer())  */
    sendOp(129) ;
    sendOp(130) ;
    sendOp(96) ;
    sendValue("short") ;
    endList() ;
    sendOp(104) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"float") == 0)) {
    /* RULE type::opName:"float" -> 
              float()  */
    sendOp(78) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"double") == 0)) {
    /* RULE type::opName:"double" -> 
              modifiedType(modifiers(ident:"double"),float())  */
    sendOp(129) ;
    sendOp(130) ;
    sendOp(96) ;
    sendValue("double") ;
    endList() ;
    sendOp(78) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"_Bool") == 0)) {
    /* RULE type::opName:"_Bool" -> 
              boolean()  */
    sendOp(29) ;
  } else  if ((resultKind == 1) && (context == 3) &&
      (strcmp(fTree->contents.value,"char") == 0)) {
    /* RULE type::opName:"char" -> 
              character()  */
    sendOp(35) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opName Name -> 
              ident Name  */
    sendOp(96) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT137(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opGotoStmt(opName N) -> 
              goto(label N)  */
    sendOp(93) ;
    sendOp(111) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT138(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opLabelStmt(opName N,Stmt) -> 
              labelStatement(label N,Stmt)  */
    sendOp(112) ;
    sendOp(111) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT139(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opKeyword Name -> 
              ident Name  */
    sendOp(96) ;
    sendValue((char *)fTree->contents.value) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT140(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 55)) {
    /* RULE opVAArgExpr(Type,opDeclRefExpr(DestVar)) -> 
              none()  */
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT141(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXDefaultArgExpr Type -> 
              none()  */
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXDefaultArgExpr X -> 
              none()  */
    sendOp(138) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT142(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context &&
      (fTree->contents.sons->next->tree->oper->rank == 81) &&
      (strcmp(fTree->contents.sons->next->tree->contents.value,"unknown") == 0)) {
    /* RULE opArrayType(Element,opIntegerLiteral:"unknown") -> 
              arrayType(Element,dimColons(none()))  */
    sendOp(13) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(60) ;
    sendOp(138) ;
    endList() ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opArrayType(Element,Number) -> 
              arrayType(Element,dimColons(dimColon(intCst:"0",sub(Number,intCst:"1"))))  */
    sendOp(13) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    sendOp(60) ;
    sendOp(59) ;
    sendOp(103) ;
    sendValue("0") ;
    sendOp(181) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(103) ;
    sendValue("1") ;
    endList() ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT143(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && !context) {
    /* RULE opCXXForRangeStmt(Init,List,Body) -> 
              loop(none(),none(),forall(forallRanges(forallRangeSet(Init,List)),none()),Body)  */
    sendOp(121) ;
    sendOp(138) ;
    sendOp(138) ;
    sendOp(80) ;
    sendOp(83) ;
    sendOp(81) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    endList() ;
    sendOp(138) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT144(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opNamespaceAliasDecl(Namespace1,Namespace2) -> 
              none()  */
    sendOp(138) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT147(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) >= 3) &&
      (fTree->contents.sons->tree->oper->rank == 136) &&
      (fTree->contents.sons->next->tree->oper->rank == 135) &&
      (fTree->contents.sons->next->next->tree->oper->rank == 81)) {
    /* RULE opVarTemplateSpecializationDecl[(opName Name,opType(M,T),opIntegerLiteral N),Body] -> 
              none()  */
    sendOp(138) ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 2) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opVarTemplateSpecializationDecl(opName N,Type) -> 
              varDeclaration(modifiers[],Type,declarators(ident N))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    endList() ;
  } else  if ((resultKind == 1) && !context &&
      (listTreeLength(fTree->contents.sons) == 3) &&
      (fTree->contents.sons->tree->oper->rank == 136)) {
    /* RULE opVarTemplateSpecializationDecl(opName N,Type,Value) -> 
              varDeclaration(modifiers[],Type,declarators(assign(ident N,Value)))  */
    sendOp(198) ;
    sendOp(130) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
    sendOp(54) ;
    sendOp(14) ;
    sendOp(96) ;
    sendValue((char *)fTree->contents.sons->tree->contents.value) ;
    clang2ilTT(fTree->contents.sons->next->next->tree, 0, 1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT148(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if (!context) {
    /* RULE opCXXStdInitializerListExpr(List) -> 
              List  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT149(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else  if ((resultKind == 1) && !context) {
    /* RULE opCXXTypeidExpr(TypeOrExpr) -> 
              call(none(),ident:"typeid",expressions(TypeOrExpr))  */
    sendOp(31) ;
    sendOp(138) ;
    sendOp(96) ;
    sendValue("typeid") ;
    sendOp(71) ;
    clang2ilTT(fTree->contents.sons->tree, 0, 1) ;
    endList() ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT151(Tree *fTree, int context, short resultKind) {
  if (!context) {
    /* RULE opConstantExpr(X) -> 
              X  */
    clang2ilTT(fTree->contents.sons->tree, 0, resultKind) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTT162(Tree *fTree, int context, short resultKind) {
  if ((resultKind == 1) && (context == 2) &&
      (fTree->contents.sons->tree->oper->rank == 57)) {
    /* RULE inSwitch::dummyArity2op(opDefaultStmt(),Body) -> 
              switchCase(expressions(),Body)  */
    sendOp(184) ;
    sendOp(71) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((resultKind == 1) && (context == 2) &&
      (fTree->contents.sons->tree->oper->rank == 43)) {
    /* RULE inSwitch::dummyArity2op(opCaseStmt(Case),Body) -> 
              switchCase(expressions(Case),Body)  */
    sendOp(184) ;
    sendOp(71) ;
    clang2ilTT(fTree->contents.sons->tree->contents.sons->tree, 0, 1) ;
    endList() ;
    clang2ilTT(fTree->contents.sons->next->tree, 0, 1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTTXTree(Tree *fTree, int context, short resultKind) {
  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    action1(fTree, resultKind) ;
  } else
    ttmlNoRule(fTree, context, resultKind) ;
}

void clang2ilTTList(ListTree *listTree, int context, short resultKind) {
  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 56)) {
    /* RULE block::[(opDeclStmt Decls),Suite] -> 
              [Decls,block::Suite]  */
    clang2ilTTList(listTree->tree->contents.sons, 0, -1) ;
    clang2ilTTList(listTree->next, 1, -1) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE block::[(Statement),Suite] -> 
              [(Statement),block::Suite]  */
    clang2ilTT(listTree->tree, 0, 1) ;
    clang2ilTTList(listTree->next, 1, -1) ;
  } else  if ((resultKind == -1) && (context == 1) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE block::[] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 4) &&
      (listTreeLength(listTree) == 0)) {
    /* RULE inFriends::[] -> 
              []  */
  } else  if ((resultKind == -1) && (context == 4) &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE inFriends::[(First),Suite] -> 
              [(inFriends::First),inFriends::Suite]  */
    clang2ilTT(listTree->tree, 4, 1) ;
    clang2ilTTList(listTree->next, 4, -1) ;
  } else  if ((context == 3)) {
    /* RULE type::T -> 
              T  */
    clang2ilTTList(listTree, 0, resultKind) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) == 0)) {
    /* RULE [] -> 
              []  */
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1) &&
      (listTree->tree->oper->rank == 107)) {
    /* RULE [(opSubstNonTypeTemplateParmExpr X),Suite] -> 
              [Suite]  */
    clang2ilTTList(listTree->next, 0, -1) ;
  } else  if ((resultKind == -1) && !context &&
      (listTreeLength(listTree) >= 1)) {
    /* RULE [(First),Suite] -> 
              [(First),Suite]  */
    clang2ilTT(listTree->tree, 0, 1) ;
    clang2ilTTList(listTree->next, 0, -1) ;
  } else {
    char *message = (char*)malloc(1000) ;
    int len = 0 ;
    message[0] = '\0' ;
    message = strcat(message, "Unexpected source list: (") ;
    while (listTree != NULL) {
      message = strcat(message, listTree->tree->oper->name) ;
      listTree = listTree->next ;
      if (listTree) {
        if (len>5) {
          message = strcat(message, " ...") ;
          listTree=NULL ;
        } else message = strcat(message, " ") ;
        len++ ;
      }
    }
    message = strcat(message, ") (") ;
    message = strcat(message, contextNames[context]) ;
    message = strcat(message, ")") ;
    sendOp(ilParsingErrorCode) ;
    sendValue(message) ;
  }
}


static TTMLOperatorTreeTranslator *(clang2ilOperTreeTrans[]) = {
  /*   0...*/ &clang2ilTT0,&clang2ilTTXTree,&clang2ilTT2,&clang2ilTTXTree,&clang2ilTTXTree,
  /*   5...*/ &clang2ilTTXTree,&clang2ilTT6,&clang2ilTTXTree,&clang2ilTT8,&clang2ilTT9,
  /*  10...*/ &clang2ilTT10,&clang2ilTT11,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT14,
  /*  15...*/ &clang2ilTT15,&clang2ilTT16,&clang2ilTT17,&clang2ilTT18,&clang2ilTT19,
  /*  20...*/ &clang2ilTT20,&clang2ilTT21,&clang2ilTTXTree,&clang2ilTT23,&clang2ilTT24,
  /*  25...*/ &clang2ilTT25,&clang2ilTT26,&clang2ilTT27,&clang2ilTT28,&clang2ilTT29,
  /*  30...*/ &clang2ilTT30,&clang2ilTT31,&clang2ilTTXTree,&clang2ilTT33,&clang2ilTT34,
  /*  35...*/ &clang2ilTT35,&clang2ilTT36,&clang2ilTT37,&clang2ilTT38,&clang2ilTT39,
  /*  40...*/ &clang2ilTT40,&clang2ilTTXTree,&clang2ilTT42,&clang2ilTTXTree,&clang2ilTT44,
  /*  45...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT48,&clang2ilTT49,
  /*  50...*/ &clang2ilTT50,&clang2ilTT51,&clang2ilTT52,&clang2ilTTXTree,&clang2ilTT54,
  /*  55...*/ &clang2ilTT55,&clang2ilTT56,&clang2ilTTXTree,&clang2ilTT58,&clang2ilTTXTree,
  /*  60...*/ &clang2ilTT60,&clang2ilTTXTree,&clang2ilTT62,&clang2ilTT63,&clang2ilTTXTree,
  /*  65...*/ &clang2ilTT65,&clang2ilTT66,&clang2ilTT67,&clang2ilTT68,&clang2ilTTXTree,
  /*  70...*/ &clang2ilTT70,&clang2ilTTXTree,&clang2ilTT72,&clang2ilTT73,&clang2ilTT74,
  /*  75...*/ &clang2ilTT75,&clang2ilTT76,&clang2ilTTXTree,&clang2ilTT78,&clang2ilTTXTree,
  /*  80...*/ &clang2ilTT80,&clang2ilTT81,&clang2ilTT82,&clang2ilTTXTree,&clang2ilTTXTree,
  /*  85...*/ &clang2ilTT85,&clang2ilTTXTree,&clang2ilTT87,&clang2ilTTXTree,&clang2ilTTXTree,
  /*  90...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT92,&clang2ilTTXTree,&clang2ilTTXTree,
  /*  95...*/ &clang2ilTTXTree,&clang2ilTT96,&clang2ilTT97,&clang2ilTT98,&clang2ilTTXTree,
  /* 100...*/ &clang2ilTTXTree,&clang2ilTT101,&clang2ilTTXTree,&clang2ilTT103,&clang2ilTT104,
  /* 105...*/ &clang2ilTT105,&clang2ilTT106,&clang2ilTT107,&clang2ilTT108,&clang2ilTTXTree,
  /* 110...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT112,&clang2ilTT113,&clang2ilTT114,
  /* 115...*/ &clang2ilTT115,&clang2ilTT116,&clang2ilTT117,&clang2ilTT118,&clang2ilTTXTree,
  /* 120...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT123,&clang2ilTT124,
  /* 125...*/ &clang2ilTT125,&clang2ilTT126,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,
  /* 130...*/ &clang2ilTT130,&clang2ilTT131,&clang2ilTT132,&clang2ilTT133,&clang2ilTTXTree,
  /* 135...*/ &clang2ilTT135,&clang2ilTT136,&clang2ilTT137,&clang2ilTT138,&clang2ilTT139,
  /* 140...*/ &clang2ilTT140,&clang2ilTT141,&clang2ilTT142,&clang2ilTT143,&clang2ilTT144,
  /* 145...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT147,&clang2ilTT148,&clang2ilTT149,
  /* 150...*/ &clang2ilTTXTree,&clang2ilTT151,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,
  /* 155...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTTXTree,
  /* 160...*/ &clang2ilTTXTree,&clang2ilTTXTree,&clang2ilTT162,&clang2ilTTXTree,&clang2ilTTXTree,
  /* 165...*/ &clang2ilTTXTree,&clang2ilTTXTree,NULL,NULL,NULL,
  /* end...*/ NULL
} ;

void clang2ilTT(Tree *fTree, int context, short resultKind) {
  (*(clang2ilOperTreeTrans[fTree->oper->rank]))(fTree, context, resultKind) ;
}

void clang2ilProtocol(Tree *fTree, FILE *outputStream) {
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = NULL ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = outputStream ;
  clang2ilTT(fTree, 0, 1) ;
  stack = stack->next;
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
}

Tree *clang2ilTree(Tree *fTree) {
  Tree *returnTree ;
  TreeBuilder *previousTargetBuilder = targetBuilder ;
  FILE *previousProtocolOutputStream = protocolOutputStream ;
  TargetBuilderStack *newStack = new(TargetBuilderStack);
  newStack->top = newTreeBuilder(ilOperators) ;
  newStack->next = stack;
  stack = newStack;
  protocolOutputStream = NULL ;
  clang2ilTT(fTree, 0, 1) ;
  returnTree = getTreeBuilt(stack->top) ;
  stack = stack->next;
  deleteTreeBuilder(newStack->top);
  free(newStack);
  protocolOutputStream = previousProtocolOutputStream ;
  targetBuilder = previousTargetBuilder ;
  return returnTree ;
}
