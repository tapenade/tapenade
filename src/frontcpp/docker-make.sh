#!/usr/bin/env bash

if [ "$(uname)" = "Linux" ]; then
  docker run --rm -v "$PWD":/usr/tapenade/src/frontcpp -v "$PWD"/../../:/usr/tapenade -u $(stat -c "%u:%g" ./) -w /usr/tapenade/src/frontcpp registry.gitlab.inria.fr/tapenade/distrib make
else
  docker run --rm -v "$PWD":/usr/tapenade/src/frontcpp -v "$PWD"/../../:/usr/tapenade -w /usr/tapenade/src/frontcpp registry.gitlab.inria.fr/tapenade/distrib make
fi

cp ../../bin/linux/cppParser .

docker build -t registry.gitlab.inria.fr/tapenade/tapenade/cppparser:latest -t registry.gitlab.inria.fr/tapenade/tapenade/cppparser:$(cat ../../build/resources/main/gittag) .

/bin/rm cppParser

if [ "$(uname)" = "Linux" ]; then
  docker push registry.gitlab.inria.fr/tapenade/tapenade/cppparser
else
  echo "docker push Time out on macos"
fi
