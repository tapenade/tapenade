/* C++ parser: converts a C++ source code into Tapenade's IL.
 * Not all features of C++ are supported yet.
 * This parser is based on clang's ASTVisitor class, please refer to
 * https://clang.llvm.org/doxygen/index.html for a documentation of the
 * tree operators that represent statements, declarations, or types. */
 
#include <clang/AST/ASTContext.h>
#include <clang/AST/RecursiveASTVisitor.h>
#include <clang/Frontend/CompilerInstance.h>
#include <clang/Frontend/FrontendAction.h>
#include <clang/Tooling/Tooling.h>
#include <clang/Tooling/CommonOptionsParser.h>
extern "C" {
  #include "../front/cvtp.h"
  #include "../front/treeBuilder.h"
  #include "tablesclang.c"
  #include "clang2il.h"
  #include <string.h>
   void showFrontierRec(TreeGrowthFrontier *frontier) ;
}

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

static llvm::cl::OptionCategory MyToolCategory("Parser-to-Tapenade options");
static cl::extrahelp MoreHelp("\ncppParser - $Id$ \n Converts a C++ source code into Tapenade's IL format.\n\n");
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);
// static Tree *parsedTree = nullptr;
static TreeBuilder *tb;
// [llh 6mar19] These global strings text and name are dangerous. Get rid of them!
static const char* text;
static const char* name;
Operator getOperOfStmt(const char* clangOperName, Stmt* X) ;
Operator getOperOfDecl(const char* clangOperName, Decl* X) ;
static bool trace0 = false ;
static bool trace1 = false ;
static bool trace2 = false ;

/** Memo of the short names for instantiated templated names. */
typedef struct _TemplateSuffixesChain {
  char *templateSuffix ;
  struct _TemplateSuffixesChain *next ;
} TemplateSuffixesChain ;
TemplateSuffixesChain *templateSuffixesChain = NULL ;

// /** Memo of the short names for instantiated templated names. */
// typedef struct _TemplateTreesChain {
//   ListTree* templateTrees ;
//   struct _TemplateTreesChain *next ;
// } TemplateTreesChain ;
// TemplateTreesChain *templateTreesChain = NULL ;

int getSetTemplateSuffixRank(std::string templateSuffix) {
  TemplateSuffixesChain *inTemplateSuffixesChain = templateSuffixesChain ;
  int rank = 1 ;
  while (inTemplateSuffixesChain->next &&
         templateSuffix.compare(inTemplateSuffixesChain->next->templateSuffix)!=0) {
    ++rank ;
    inTemplateSuffixesChain = inTemplateSuffixesChain->next ;
  }
  if (inTemplateSuffixesChain->next==NULL) {
    // If the given templateSuffix is not found,
    //  add it as a new element at the tail of templateSuffixesChain:
    inTemplateSuffixesChain->next = (TemplateSuffixesChain*)malloc(sizeof(TemplateSuffixesChain)) ;
    inTemplateSuffixesChain->next->templateSuffix = strdup(templateSuffix.c_str()) ;
    inTemplateSuffixesChain->next->next = NULL ;
  }
  return rank ;
}

void extendIdentWithSpecialization(Tree *identTree, const ClassTemplateSpecializationDecl *classTemplateSpecializationD) {
  // This rebuilding of the specialized class full name is a pity: there must be a way to ask Clang for it! :
  if (classTemplateSpecializationD->getTemplateArgs().size()>0) {
    std::string templatesSuffix = std::string("<") ;
    for (unsigned int i=0, nbArgs=classTemplateSpecializationD->getTemplateArgs().size() ; i<nbArgs ; ++i) {
      TemplateArgument arg = classTemplateSpecializationD->getTemplateArgs().get(i) ;
      if (i>0) templatesSuffix += ", " ;
      raw_string_ostream strStream(templatesSuffix) ;
      // In Clang-11 was: arg.print(LangOptions(), strStream) ;
      arg.print(LangOptions(), strStream, false) ;
      templatesSuffix = strStream.str() ;
    }
    // Add a white space to mimic the behavior of Clang's standard QualType.getAsString()
    templatesSuffix += (templatesSuffix.back()=='>'?" >":">") ;
    std::string className =
      std::string(identTree->contents.value)+"_SPEC"+std::to_string(getSetTemplateSuffixRank(templatesSuffix)) ;

// if (strstr(className.c_str(), "T_Conformation_traits_SPEC")) {
// llvm::errs() << "LOCa " << className << " :: " << templatesSuffix << "\n";
// }
// if (templatesSuffix.length()>21 && 0==templatesSuffix.compare(templatesSuffix.length()-21, templatesSuffix.length(), "traits_AKMA<double> >")){
//   llvm::errs() << "LOCa " << className << " :: " << templatesSuffix << "\n";
// }

    identTree->contents.value = strdup(className.c_str()) ;
  }
}

std::string condenseTemplateSuffix(std::string name) {
  size_t templateStart = name.find('<') ;
  size_t also_close_bracket = name.find('>') ;
  if (templateStart==std::string::npos
      || also_close_bracket==std::string::npos
      || also_close_bracket==templateStart+1) {
    // i.e. template specialization not found (in particular, xx<>, operator<< and operator< must not match...)
    return name ;
  } else {
// llvm::errs() <<"BEFORE CONDENSE "<<name<<" TAIL:"<<name.substr(templateStart, std::string::npos)<<"\n" ;
    int rank = getSetTemplateSuffixRank(name.substr(templateStart, std::string::npos)) ;

// if (strstr(name.substr(0,templateStart).c_str(), "T_Conformation_traits")) {
// llvm::errs() << "LOCb _SPEC" << rank << " :: " << name.substr(templateStart, std::string::npos) << " name:" << name << "\n";
// }
// std::string subs = name.substr(templateStart, std::string::npos) ;
// if (subs.length()>21 && 0==subs.compare(subs.length()-21, subs.length(), "traits_AKMA<double> >")) {
//   llvm::errs() << "LOCb _SPEC" << rank << " :: " << subs << " name:" << name << "\n";
// }

// llvm::errs() <<"  GET/SET WITH RANK "<<rank<<"\n" ;
    return name.substr(0,templateStart)+"_SPEC"+std::to_string(rank) ;
  }
}

// The iterator on clang's AST, inheriting from class RecursiveASTVisitor:
class NodeVisitor : public RecursiveASTVisitor<NodeVisitor> {

public:
  explicit NodeVisitor(ASTContext *Context, StringRef InFile) : Context(Context) {
    indentLevel = 0;
  }

//   ListTree* collectTemplateArgumentTrees(ClassTemplateSpecializationDecl *classTemplateSpecializationD) {
//     ListTree* result = NULL ;
//     for (int i=classTemplateSpecializationD->getTemplateArgs().size()-1 ; i>=0 ; --i) {

//       TemplateArgument arg = classTemplateSpecializationD->getTemplateArgs().get(i) ;
//       Tree *argTree = NULL ;
//       switch(arg.getKind()) {
//       case clang::TemplateArgument::Type:
// //         llvm::errs()  << " arg as type dump:\n" ;
// //         arg.getAsType().dump() ;
// //         llvm::errs()  << " end arg as type dump!\n" ;
// //         llvm::errs()  << "BUILT TYPE TREE FOR:"<<arg.getAsType().getAsString()<<" :\n" ;
//         argTree = buildTypeTree(arg.getAsType()) ;
// //         showTree(argTree, 5) ;
//         break ;
//       case clang::TemplateArgument::NullPtr:
//         argTree = mkTree(&(tb->langOp[30/*opCXXNullPtrLiteralExpr*/]), NULL) ;
//         break ;
//       case clang::TemplateArgument::Integral:
//         argTree = mkAtom(&(tb->langOp[81/*opIntegerLiteral*/]), strdup(arg.getAsIntegral().toString(10).c_str())) ;
//         break ;
//       default:
//         llvm::errs()  << "DONT KNOW HOW TO READ TEMPLATE ARGUMENT OF TYPE "<<arg.getKind()<<"\n" ;
//         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_TemplateArgument")) ;
// //       case clang::TemplateArgument::TemplateExpansion:
// //         break ;
// //       case clang::TemplateArgument::Pack:
// //         break ;
// //       case clang::TemplateArgument::Declaration:
// //         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_Declaration")) ;
// //         break ;
// //       case clang::TemplateArgument::Template:
// //         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_Template")) ;
// //         break ;
// //       case clang::TemplateArgument::Expression:
// //         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_Expression")) ;
// //         break ;
// //       case clang::TemplateArgument::NULL:
// //         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_Null_emptyTemplateArg")) ;
// //         break ;
// //       default:
// //         argTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup("some_TemplateArgument")) ;
//       }
//       result = mkSons(argTree, result) ;
//     }
//     return result ;
//   }

//   int getSetSpecializedNameRank(ListTree* templateTrees) {
//     TemplateTreesChain *inTemplateTreesChain = templateTreesChain ;
//     int rank = 1 ;
//     while (inTemplateTreesChain->next &&
//            !equalListTrees(templateTrees, inTemplateTreesChain->next->templateTrees)) {
//       ++rank ;
//       inTemplateTreesChain = inTemplateTreesChain->next ;
//     }
//     if (inTemplateTreesChain->next!=NULL)
//       // The given templateTrees is already registered as the rank-th element of templateTreesChain:
//       return rank ;
//     else {
//       // or we must add the given templateTrees as a new, rank-th +1, element of templateTreesChain:
//       inTemplateTreesChain->next = (TemplateTreesChain*)malloc(sizeof(TemplateTreesChain)) ;
//       inTemplateTreesChain->next->templateTrees = templateTrees ;
//       inTemplateTreesChain->next->next = NULL ;
//       return rank+1 ;
//     }
//   }

  int isAnonymousName(Tree *tree) {
    if (tree->oper->rank==136/*Name*/)
      return strlen(treeAtomValue(tree))==0 ;
    else if (tree->oper->rank==135/*Type*/)
      return isAnonymousName(tree->contents.sons->next->tree) ;
    else
      return 0 ;
  }

  int isAType(Tree *tree) {
    return tree->oper->rank==135/*Type*/
      || tree->oper->rank==33/*CXXRecordDecl*/ ;
  }

  ListTree* collectVarNames(ListTree* varDecls, ListTree** toSequel) {
    ListTree *collectedVarNames = NULL ;
    ListTree **toCollectedTail = &collectedVarNames ;
    Tree* oneVarName ;
    while (varDecls &&
           (varDecls->tree->oper->rank==66/*FieldDecl*/
             || varDecls->tree->oper->rank==126/*VarDecl*/)
            && isAnonymousName(varDecls->tree->contents.sons->next->tree)) {
      oneVarName = varDecls->tree->contents.sons->tree ;
      if (varDecls->tree->contents.sons->next->next) {  // if there is an initializer expression
        oneVarName = mkTree(&(tb->langOp[9/*BinaryOperator*/]),
                            mkSons(mkAtom(&(tb->langOp[136/*Name*/]), strdup("=")),
                              mkSons(oneVarName,
                                varDecls->tree->contents.sons->next->next))) ;
      }
      *toCollectedTail = mkSons(oneVarName, NULL) ;
      toCollectedTail = &((*toCollectedTail)->next) ;
      varDecls = varDecls->next ;
    }
    *toSequel = varDecls ;
    return collectedVarNames ;
  }

  Tree* addInlineToQualifiers(Tree *qualifiers) {
    if (qualifiers->oper->rank!=133/*Qualifiers*/)
      qualifiers = mkTree(&(tb->langOp[133/*Qualifiers*/]), NULL) ;
    // Check qualifier "inline" is already in qualifiers:
    bool alreadyHere = false ;
    ListTree* quals = qualifiers->contents.sons ;
    while (quals!=NULL && !alreadyHere) {
      alreadyHere = (quals->tree->oper->rank==136/*Name*/
                     && 0==strncmp(quals->tree->contents.value,"inline",6)) ;
      quals = quals->next ;
    }
    // If not, add it:
    if (!alreadyHere)
      qualifiers->contents.sons =
        mkSons(mkAtom(&(tb->langOp[136/*Name*/]), strdup("inline")),
               qualifiers->contents.sons) ;
    return qualifiers ;
  }

  /** Complex cleanup and post-process on the Clang Tree, that cannot be done just with ttml. */
  void rearrangeClangOutput(Operator oper, Tree *resultTree, Decl *decl, Stmt *stmt) {

    if (oper.rank==72/*FunctionDecl*/) {
      // Add empty qualifiers as first child:
      resultTree->contents.sons =
        mkSons(mkTree(&(tb->langOp[133/*Qualifiers*/]), NULL),
               resultTree->contents.sons) ;
      // Add the inline qualifier if specified here:
      if (dyn_cast<FunctionDecl>(decl)->isInlineSpecified())
        resultTree->contents.sons->tree = addInlineToQualifiers(resultTree->contents.sons->tree) ;
    } else if (oper.rank==18/*CXXConstructorDecl*/) {
      // Add the inline qualifier if specified here:
      if (dyn_cast<FunctionDecl>(decl)->isInlineSpecified())
        resultTree->contents.sons->next->next->tree = addInlineToQualifiers(resultTree->contents.sons->next->next->tree) ;
      // [llh] Silly Clang looses the structure of the arguments list (in many cases, not only in CXXConstructorDecl),
      // so we gather by hand the parmVarDecl's into a dummy tree (of list arity):
      ListTree *toParmVarDeclSons = resultTree->contents.sons->next->next->next ;
      ListTree *toEndParmVarDeclSons = toParmVarDeclSons ;
      while (toEndParmVarDeclSons->next && toEndParmVarDeclSons->next->tree->oper->rank==98/*ParmVarDecl*/)
        toEndParmVarDeclSons = toEndParmVarDeclSons->next ;
      ListTree *newListCell = (ListTree*)malloc(sizeof(ListTree)) ;
      newListCell->next = toEndParmVarDeclSons->next ;
      toEndParmVarDeclSons->next = NULL ;
      Operator* dummyOper = &(tb->langOp[159/*dummyListop*/]) ;
      Tree *dummyTree = mkTree(dummyOper, toParmVarDeclSons->next) ;
      newListCell->tree = dummyTree ;
      toParmVarDeclSons->next = newListCell ;
// if (strstr(resultTree->contents.sons->tree->contents.value, "Residues_iterator_from_chain")) {
// llvm::errs()  << "STRUCTURE PROVIDED BY CLANG:\n" ;
// decl->dump() ;
// llvm::errs()  << "\n" ;
// printf("RESULT CONSTRUCTORDECL:\n") ;
// showTree(resultTree, 2) ;
// printf("\n") ;
// }
    } else if (oper.rank==23/*CXXDestructorDecl*/) {
      // Add the inline qualifier if specified here:
      if (dyn_cast<FunctionDecl>(decl)->isInlineSpecified())
        resultTree->contents.sons->next->next->tree = addInlineToQualifiers(resultTree->contents.sons->next->next->tree) ;
    } else if (oper.rank==108     // opSwitchStmt
               && resultTree->contents.sons->next->tree->oper->rank==51/*compoundStmt*/
               && resultTree->contents.sons->next->tree->contents.sons) {
      // [llh] Silly Clang looses the structure of the list of switch cases,
      // so we gather by hand each of the successive switch cases into a dummy tree (of list arity):
      // Note: if we reach here, we assume/assert:
      //         (resultTree->contents.sons->next->tree->contents.sons->tree->oper->rank==43 ||   // opCaseStmt
      //          resultTree->contents.sons->next->tree->contents.sons->tree->oper->rank==57)     // opDefaultStmt
      ListTree *rawContents = resultTree->contents.sons->next->tree->contents.sons ;
      Tree *caseTree ;
      ListTree **toLastCaseCell ;
      while (rawContents) {
        caseTree = mkTree(&(tb->langOp[162/*dummyArity2op*/]),
                          mkSons(rawContents->tree,
                            mkSons(mkTree(&(tb->langOp[51/*compoundStmt*/]), rawContents->next),
                              NULL))) ;
        toLastCaseCell = &(caseTree->contents.sons->next->tree->contents.sons) ;
        rawContents->tree = caseTree ;
        while (rawContents->next &&
               (rawContents->next->tree->oper->rank!=43 &&    // opCaseStmt
                rawContents->next->tree->oper->rank!=57)) {   // opDefaultStmt
          toLastCaseCell = &((*toLastCaseCell)->next) ;
          rawContents->next = rawContents->next->next ;
        }
        *toLastCaseCell = NULL ;
        rawContents = rawContents->next ;
      }
    } else if (oper.rank==33/*CXXRecordDecl*/ || oper.rank==56/*DeclStmt*/) {
      // [llh] Clang splits a lot. Here we gather again because splitting has introduced
      // anonymous types, followed by declarations of (maybe anonymous) variables of anonymous type:
      ListTree *inListDecls = resultTree->contents.sons ;
      if (oper.rank==33/*CXXRecordDecl*/) inListDecls = inListDecls->next->next ;
      while (inListDecls) {
        if (inListDecls->next &&
            (inListDecls->next->tree->oper->rank==66/*FieldDecl*/
             || inListDecls->next->tree->oper->rank==126/*VarDecl*/)
            && isAnonymousName(inListDecls->next->tree->contents.sons->next->tree)) {
          if (inListDecls->tree->oper->rank==33/*CXXRecordDecl*/ &&
              isAnonymousName(inListDecls->tree->contents.sons->next->tree)) {
            //Case: DeclStmt(..., RecordDecl(Keyword:_ ,Name:"",...), FieldDecl(Name:V,Type(Q,Name:""),INITVAL)*, ...)
            // ---> DeclStmt(..., FieldDecl(DummyList(Assign(Name:V, INITVAL))*, Type(Q,RecordDecl(Keyword:_ ,Name:""))), ...)
            // TODO: here we create plenty of memory leaks: fix this!
            Tree *typeTree = inListDecls->tree ;
            Tree *finalTree = inListDecls->next->tree ;
            ListTree *toSequel ;
            ListTree *varNames = collectVarNames(inListDecls->next, &toSequel) ;
            finalTree->contents.sons->next->next = NULL ;
            Tree *varNamesTree ;
            if (varNames->next) {
              Operator* dummyOper = &(tb->langOp[159/*dummyListop*/]) ;
              varNamesTree = mkTree(dummyOper, varNames) ;
            } else {
              varNamesTree = varNames->tree ;
            }
            finalTree->contents.sons->tree = varNamesTree ;
            finalTree->contents.sons->next->tree = typeTree ;
            inListDecls->next = toSequel ;
            inListDecls->tree = finalTree ;
          }
        } else {
          if (isAType(inListDecls->tree)) {
            //Case: DeclStmt(TYPE, VarDecl(Name:V, TTT, ...), ...)
            // ---> DeclStmt(VarDecl(none(), TYPE, none()), VarDecl(Name:V, TTT, ...), ...)
            Tree *finalTree = mkTree(&(tb->langOp[(oper.rank==56/*DeclStmt*/?126/*VarDecl*/:66/*FieldDecl*/)]),
                                     mkSons(mkTree(&(tb->langOp[0/*None*/]), NULL),
                                       mkSons(inListDecls->tree,
                                         mkSons(mkTree(&(tb->langOp[0]), NULL), NULL)))) ;
            inListDecls->tree = finalTree ;
          }
        }
        inListDecls = inListDecls->next ;
      }
    } else if (oper.rank==87/*NamespaceDecl*/ &&
               resultTree->contents.sons!=NULL &&
               resultTree->contents.sons->next==NULL) {
      //Case of an empty namespace decl (e.g. because remaining templated decl are removed): remove it!
//       printf("opNamespaceDecl TREE:\n") ;
//       showTree(resultTree, 5) ;
//       printf("\nEND opNamespaceDecl TREE.\n") ;

      removeTree(tb, resultTree) ;
    } else if (oper.rank==85/*MemberExpr*/) {
      MemberExpr *memberExprStmt = dyn_cast<MemberExpr>(stmt) ;
      // Wierd behavior about "templated members" ?... :
      // Gather missing info from other parts of "stmt":
      char *arrowOrDot = strdup(memberExprStmt->isArrow()?"->":".") ;
      mkPost(resultTree, mkAtom(&(tb->langOp[139/*Keyword*/]), arrowOrDot)) ;
      oneLessWaiting(tb) ;
      std::string name;
      if (memberExprStmt->hasQualifier())
        name = memberExprStmt->getMemberDecl()->getQualifiedNameAsString();
      else
        name = memberExprStmt->getMemberDecl()->getNameAsString();
      Tree *memberTree = resolScope(name.c_str()) ;
      mkPost(resultTree, memberTree) ;
      oneLessWaiting(tb) ;
//       // tell the treeBuilder's frontier that this resultTree is finished:
//       tb->frontier->remainingTrees = 0 ; // Useless and dangerous, now that we consider all clang ops as lists!
      if (memberExprStmt->hasTemplateKeyword() || memberExprStmt->hasExplicitTemplateArgs()) {
//         printf("HAS TEMPLATES!\n") ;
//         showTree(resultTree, 2) ;
//         printf("\n") ;
        // retrieve and detach the template Tree.
//         Tree *templateTree = resultTree->contents.sons->tree ;
        if (resultTree->contents.sons->next->next->next) {
          resultTree->contents.sons = resultTree->contents.sons->next ;
        }
        // attach the template Tree to the opName
        Tree *refTree = resultTree->contents.sons->next->next->tree ;
        while (refTree->oper->rank==125/*scopeAccess*/) {
          refTree = refTree->contents.sons->next->tree ;
        }
        if (refTree->oper->rank==136/*Name*/) {
          int numTemplateArgs = memberExprStmt->getNumTemplateArgs() ;
          const TemplateArgumentLoc* templateArgs = memberExprStmt->getTemplateArgs() ;
          std::string templatesSuffix = std::string("<") ;
          while (numTemplateArgs>0) {
            TemplateArgument arg = templateArgs->getArgument() ;
            raw_string_ostream strStream(templatesSuffix) ;
            // In Clang-11 was:arg.print(LangOptions(), strStream) ;
            arg.print(LangOptions(), strStream, false) ;
            templatesSuffix = strStream.str() ;
            ++templateArgs ;
            --numTemplateArgs ;
            if (numTemplateArgs>0)
              templatesSuffix += ", " ;
            else
              // Add a white space to mimic the behavior of Clang's standard QualType.getAsString():
              templatesSuffix += (templatesSuffix.back()=='>'?" >":">") ;
          }
          std::string specializedName =
            std::string(refTree->contents.value)+"_SPEC"+std::to_string(getSetTemplateSuffixRank(templatesSuffix)) ;

// if (strstr(specializedName.c_str(), "T_Conformation_traits_SPEC")) {
// llvm::errs() << "LOCc " << specializedName << " :: " << templatesSuffix << "\n";
// }
// if (templatesSuffix.length()>21 && 0==templatesSuffix.compare(templatesSuffix.length()-21, templatesSuffix.length(), "traits_AKMA<double> >")) {
//   llvm::errs() << "LOCc " << specializedName << " :: " << templatesSuffix << "\n" ;
// }

//           std::string specializedName =
//             std::string(refTree->contents.value)
//             +"<"+std::to_string(getSetSpecializedNameRank(mkSons(templateTree, NULL)))+">" ;

          refTree->contents.value = strdup(specializedName.c_str()) ;
        } else {
          printf("MemberExpr HAS LOST TEMPLATE!\n") ;
          showTree(resultTree, 2) ;
          printf("\n") ;
        }
      }
    } else if (oper.rank==55/*DeclRefExpr*/) {
      DeclRefExpr *declRefStmt = dyn_cast<DeclRefExpr>(stmt) ;
      if (declRefStmt->hasTemplateKWAndArgsInfo() && listTreeLength(resultTree->contents.sons)>=2) {
//         Tree *templateTree = resultTree->contents.sons->next->tree ;
        resultTree->contents.sons->next = resultTree->contents.sons->next->next ;
        // attach the template arguments to the opName
        Tree *refTree = resultTree->contents.sons->tree ;
        while (refTree->oper->rank==125/*ScopeAccess*/) {
          refTree = refTree->contents.sons->next->tree ;
        }
        if (refTree->oper->rank==136/*Name*/) {
          int numTemplateArgs = declRefStmt->getNumTemplateArgs() ;
          const TemplateArgumentLoc* templateArgs = declRefStmt->getTemplateArgs() ;
          std::string templatesSuffix = std::string("<") ;
          while (numTemplateArgs>0) {
            TemplateArgument arg = templateArgs->getArgument() ;
            raw_string_ostream strStream(templatesSuffix) ;
            // In Clang-11 was: arg.print(LangOptions(), strStream) ;
            arg.print(LangOptions(), strStream, false) ;
            templatesSuffix = strStream.str() ;
            ++templateArgs ;
            --numTemplateArgs ;
            if (numTemplateArgs>0)
              templatesSuffix += ", " ;
            else
              // Add a white space to mimic the behavior of Clang's standard QualType.getAsString():
              templatesSuffix += (templatesSuffix.back()=='>'?" >":">") ;
          }
          std::string specializedName =
            std::string(refTree->contents.value)+"_SPEC"+std::to_string(getSetTemplateSuffixRank(templatesSuffix)) ;

// if (strstr(specializedName.c_str(), "T_Conformation_traits_SPEC")) {
// llvm::errs() << "LOCd " << specializedName << " :: " << templatesSuffix << "\n";
// }
// if (templatesSuffix.length()>21 && 0==templatesSuffix.compare(templatesSuffix.length()-21, templatesSuffix.length(), "traits_AKMA<double> >")) {
//   llvm::errs() << "LOCd " << specializedName << " :: " << templatesSuffix << "\n" ;
// }

//           std::string specializedName =
//             std::string(refTree->contents.value)
//             +"<"+std::to_string(getSetSpecializedNameRank(mkSons(templateTree, NULL)))+">" ;

          refTree->contents.value = strdup(specializedName.c_str()) ;
        } else {
          printf("DeclRefExpr HAS LOST TEMPLATE!\n") ;
          showTree(resultTree, 2) ;
          printf("\n") ;
        }
      }
      //Remove possible remaining children (maybe we should exploit them better?):
      resultTree->contents.sons->next = NULL ;
    } else if (oper.rank==22/*CXXDependentScopeMemberExpr*/) {
      // Turn it into a opMemberExpr:
      resultTree->oper = &(clangOperators[85/*MemberExpr*/]) ;
      // Gather missing info from other parts of "stmt":
      CXXDependentScopeMemberExpr *memberExprStmt = dyn_cast<CXXDependentScopeMemberExpr>(stmt) ;
      char *arrowOrDot = strdup(memberExprStmt->isArrow()?"->":".") ;
      mkPost(resultTree, mkAtom(&(tb->langOp[139/*Keyword*/]), arrowOrDot)) ;
      std::string name;
      name = memberExprStmt->getMember().getAsString();
      mkPost(resultTree, resolScope(name.c_str())) ;
    } else if (oper.rank==120/*UnresolvedMemberExpr*/) {
      // Turn it into a opMemberExpr:
      resultTree->oper = &(clangOperators[85/*MemberExpr*/]) ;
      // Gather missing info from other parts of "stmt":
      UnresolvedMemberExpr *memberExpr = dyn_cast<UnresolvedMemberExpr>(stmt) ;
      mkPost(resultTree, mkTree(&(tb->langOp[0/*None*/]), NULL)) ; // must find the base expr!
      char *arrowOrDot = strdup(memberExpr->isArrow()?"->":".") ;
      mkPost(resultTree, mkAtom(&(tb->langOp[139/*Keyword*/]), arrowOrDot)) ;
      std::string name = memberExpr->getMemberName().getAsString();
      mkPost(resultTree, resolScope(name.c_str())) ;
    } else if (oper.rank==119/*UnresolvedLookupExpr*/) {
      // Throw contents away (should be an empty list anyway...) :
      freeListTreeNode(treeSons(resultTree)) ;
      // Turn it into a opName:
      resultTree->oper = &(clangOperators[136/*Name*/]) ;
      // Find the name in the given "stmt":
      UnresolvedLookupExpr *lookupExpr = dyn_cast<UnresolvedLookupExpr>(stmt) ;
      std::string name = lookupExpr->getName().getAsString() ;
      resultTree->contents.value = (AtomValue)strdup(name.c_str()) ;
    } else if (oper.rank==41/*CXXUnresolvedConstructExpr*/) {
      // Throw contents away (should be an empty list anyway...) :
      freeListTreeNode(treeSons(resultTree)) ;
      // Turn it into a opName:
      resultTree->oper = &(clangOperators[136/*Name*/]) ;
      resultTree->contents.value = (AtomValue)strdup("CXX_UNRESOLVED_CONSTRUCT") ;
    } else if (oper.rank==8/*AtomicExpr*/) {
      // This is "variadic" stuff. I don't understand these. Let's keep only 1st child...
      resultTree->contents.sons->next = NULL ;
    } else if (oper.rank==25/*CXXFunctionalCastExpr*/) {
      // Sometimes Clang adds an extra 2nd son, that I don't understand
      if (listTreeLength(resultTree->contents.sons)==3)
        resultTree->contents.sons->next = resultTree->contents.sons->next->next ;
    } else if (oper.rank==11/*CStyleCastExpr*/) {
      // Sometimes Clang adds extra sons, that I don't understand
      if (listTreeLength(resultTree->contents.sons)>2)
        resultTree->contents.sons->next->next = NULL ;
    } else if (oper.rank==138/*LabelStmt*/ || oper.rank==130/*WhileStmt*/) {
      // Clang may have put no 2nd son, instead of an empty statement!
      if (listTreeLength(resultTree->contents.sons)==1)
        mkPost(resultTree, mkTree(&(tb->langOp[0/*None*/]), NULL)) ;
    }

    // When final rearranged resultTree is of fixed arity, check actual arity matches:
    if (resultTree!=NULL && resultTree->oper->arity>0
        && resultTree->oper->arity!=listTreeLength(resultTree->contents.sons)) {
      printf("WRONG CHILDREN NUMBER IN %s, EXPECTED %i, HAS %i\n",
             resultTree->oper->name, resultTree->oper->arity, listTreeLength(resultTree->contents.sons)) ;
      llvm::errs()  << "STRUCTURE BUILT BY CLANG:\n" ;
      if (decl!=NULL) decl->dump() ; else if (stmt!=NULL) stmt->dump() ;
      llvm::errs()  << "\n" ;
      printf("RESULTTREE:\n") ;
      showTree(resultTree, 2) ;
      printf("\n") ;
    }
  }

  /* All the Traverse*Decl methods define the way you move through the AST.
   * This is dirty to overload them with node building stuff when there exist
   * a corresponding Visit*Decl method. But sometimes, there is not, so do it there. */

  bool TraverseDecl(Decl *D) {
    if (!D || (D->isImplicit())) return true;

// Uncommenting the following forces Clang to ignore code from "system" includes
// It seems I need to go into these "system" includes for the big SBL code...
    if (D->getLocation().isValid() && Context->getSourceManager().isInSystemHeader(D->getLocation())) return true;

    //~ if (currentFileID != Context->getSourceManager().getFileID(D->getLocation())){
      //~ if(D->getLocation().isValid()){
        //~ llvm::errs() << "passing from " << currentFileName << " (" << currentFileID.getHashValue() << ") to ";
        //~ FileID newFileID = Context->getSourceManager().getFileID(D->getLocation());
        //~ StringRef newFileName = Context->getSourceManager().getFilename(D->getLocation());

        //~ if (std::find(prevIncludes.begin(), prevIncludes.end(), newFileID) != prevIncludes.end()){
          //~ startTree(tb, 92);
          //~ startTree(tb, 139/*Keyword*/);
          //~ putValue(tb, strdup("tapenade end #include"));
          //~ startTree(tb, 139/*Keyword*/);
          //~ putValue(tb, strdup(newFileName.str().c_str()));
        //~ } else {
          //~ startTree(tb, 92);
          //~ startTree(tb, 139/*Keyword*/);
          //~ putValue(tb, strdup("tapenade begin #include"));
          //~ startTree(tb, 139/*Keyword*/);
          //~ putValue(tb, strdup(currentFileName.str().c_str()));
        //~ }
        //~ prevIncludes.push_back(newFileID);
        //~ currentFileID = newFileID;
        //~ currentFileName = newFileName;
        //~ llvm::errs() << currentFileName << " (" << currentFileID.getHashValue() << ")\n";
      //~ }
    //~ }
    const char* name = D->getDeclKindName();
    Operator oper = getOperOfDecl(name, D) ;

//     llvm::outs()  << "\n";
//     indent();
//     llvm::outs()  << "(\033[02;32m" << name  << "\033[00m (" << "\033[00;33m" << oper.rank  << "\033[00m) ";
//     if (oper.arity!=0)
//        ++indentLevel;

    if (oper.rank == 61/*EmptyDecl*/) return true;
    if (dyn_cast<DeclaratorDecl>(D) && dyn_cast<DeclaratorDecl>(D)->getNumTemplateParameterLists()) return true;

// llvm::outs()  <<"D "<<name<<":"<<oper.rank<<" @"<<D<<"\n";

    Tree *resultTree = NULL ;
    if (oper.rank==45/*ClassTemplateDecl*/) {
      // in System files, this iteration is ENORMOUS (or cycles??) => we skip it
      if (D->getLocation().isValid() && Context->getSourceManager().isInSystemHeader(D->getLocation())) {
// llvm::outs()  <<"<D "<<name<<":"<<oper.rank<<"\n";
        return true;
      }
      ClassTemplateDecl *classTemplateD = dyn_cast<ClassTemplateDecl>(D) ;
// llvm::outs()<<" ClassTemplateDecl : "<<classTemplateD->getNameAsString()<<" isFirst:"<<classTemplateD->isFirstDecl()<<"\n" ;
// [llh] I ~think~ this following test ~might~ avoid duplicate traversal of D when it belongs
// to a namespace which is declared in two parts. cf T_Conformation_traits
// and cf doc of clang::Redeclarable<decl_type>
      if (classTemplateD->isFirstDecl()==1) {
       for (ClassTemplateDecl::spec_iterator curD=classTemplateD->spec_begin(), lastD=classTemplateD->spec_end(); curD!=lastD; ++curD) {
// llvm::outs()<<"TRAVERSEDECL 1 @"<<*curD<<"\n" ;
        TraverseDecl(*curD) ;
       }
      }
    } else if (oper.rank==73/*FunctionTemplateDecl*/) {
      FunctionTemplateDecl *functionTemplateD = dyn_cast<FunctionTemplateDecl>(D) ;
      for (FunctionTemplateDecl::spec_iterator curD=functionTemplateD->spec_begin(), lastD=functionTemplateD->spec_end(); curD!=lastD; ++curD) {
        TraverseDecl(*curD) ;
      }
    } else if (oper.rank==48/*ClassTemplateSpecializationDecl*/) {
      resultTree = startTree(tb, oper.rank);
// ++indentLevel;
// llvm::outs() <<indentLevel<<"+"<<oper.rank<<":"<<name<<" DECL\n" ;
// ClassTemplateSpecializationDecl *SD = dyn_cast<ClassTemplateSpecializationDecl>(D) ;
// llvm::outs()<<"TRAVERSESPECDECL 1 @"<<D<<" : "<<SD->getNameAsString()<<"\n" ;
      RecursiveASTVisitor<NodeVisitor>::TraverseDecl(D);

//       ClassTemplateSpecializationDecl *classTemplateSpecializationD = dyn_cast<ClassTemplateSpecializationDecl>(D) ;
//       ListTree* templateTrees = collectTemplateArgumentTrees(classTemplateSpecializationD) ;
//       std::string specializedName =
//         std::string(resultTree->contents.sons->next->tree->contents.value)
//         +"<"+std::to_string(getSetSpecializedNameRank(templateTrees))+">" ;
//       resultTree->contents.sons->next->tree->contents.value = strdup(specializedName.c_str()) ;

      ClassTemplateSpecializationDecl *classTemplateSpecializationD = dyn_cast<ClassTemplateSpecializationDecl>(D) ;
      Tree *classNameIdentTree = resultTree->contents.sons->next->tree ;
      if (classNameIdentTree->oper->rank==125/*ScopeAccess*/)
        classNameIdentTree = classNameIdentTree->contents.sons->next->tree ;
      extendIdentWithSpecialization(classNameIdentTree, classTemplateSpecializationD) ;

      rearrangeClangOutput(oper, resultTree, D, NULL) ;

      terminateListTree(tb);
// llvm::outs() <<indentLevel<<"-"<<resultTree->oper->rank<<":"<<resultTree->oper->name<<"\n" ;
// --indentLevel;
    } else if (oper.rank==70/*FriendDecl*/) {
      resultTree = startTree(tb, oper.rank);
      FriendDecl *friendD = dyn_cast<FriendDecl>(D) ;
      clang::TypeSourceInfo *friendType = friendD->getFriendType() ;
      if (friendType!=NULL) {
        putType(friendType->getType()) ;
      } else { //this friend declaration is about a function. Call recursive traversal:
        RecursiveASTVisitor<NodeVisitor>::TraverseDecl(D);
      }
      terminateListTree(tb);
    } else if (oper.rank==112/*TranslationUnitDecl*/) {
      resultTree = startTree(tb, oper.rank) ;
      TranslationUnitDecl *translationUnitD = dyn_cast<TranslationUnitDecl>(D) ;
      for (DeclContext::decl_iterator curD=translationUnitD->decls_begin(), lastD=translationUnitD->decls_end(); curD!=lastD; ++curD) {
// (*curD)->dump() ;
        TraverseDecl(*curD) ;
      }
      terminateListTree(tb) ;
    } else if (oper.rank!=46/*ClassTemplatePartialSpecializationDecl*/ &&
               oper.rank!=111/*TemplateTypeParmDecl*/ &&
               oper.rank!=114/*TypeAliasTemplateDecl*/ &&
               oper.rank!=146/*VarTemplateDecl*/) {
      // ^ Skip all sorts of Decl trees in which templates are not fully resolved
      resultTree = startTree(tb, oper.rank);
// ++indentLevel;
// llvm::outs() <<indentLevel<<"+"<<oper.rank<<":"<<name<<" DECL\n" ;
      if (oper.arity!=0) turnListFrontier(tb, resultTree) ; // Trick to ensure that extra/missing children error will remain localized
      RecursiveASTVisitor<NodeVisitor>::TraverseDecl(D);
      rearrangeClangOutput(oper, resultTree, D, NULL) ;
      if (oper.arity!=0) terminateListTree(tb); // end of above Trick

// if (oper.rank==87/*NamespaceDecl*/) {
//   printf(" CLOSED NAMESPACE %s, FRONTIER NOW:", resultTree->contents.sons->tree->contents.value) ;
//   showFrontierRec(tb->frontier) ;
//   printf("\n") ;
// }

// llvm::outs() <<indentLevel<<"-"<<resultTree->oper->rank<<":"<<resultTree->oper->name<<"\n" ;
// --indentLevel;
    }

//     if (oper.arity!=0) {
//       --indentLevel;
//       llvm::outs()  << "\n";
//       indent();
//     }    
//     llvm::outs()  << ")";

// llvm::outs()  <<"<D "<<name<<":"<<oper.rank<<"\n";

    return true;
  }

  bool TraverseStmt(Stmt *X) {
    if (!X) return true;
//     if (currentFileID != Context->getSourceManager().getFileID(X->getLocStart())){
//       if(X->getLocStart().isValid()){
//         llvm::outs() << "passing from " << currentFileName << " (" << currentFileID.getHashValue() << ") to ";
//         currentFileID = Context->getSourceManager().getFileID(X->getLocStart());
//         currentFileName = Context->getSourceManager().getFilename(X->getLocStart());
//         llvm::outs() << currentFileName << " (" << currentFileID.getHashValue() << ")\n";
//       }
//     }

    const char* name = X->getStmtClassName();
    Operator oper = getOperOfStmt(name, X);

//     llvm::outs()  << "\n";
//     indent();
//     llvm::outs()  << "(\033[00;32m" << name  << "\033[00m (" << "\033[00;33m" << oper.rank  << "\033[00m) ";
//     if (oper.arity!=0) {
//       ++indentLevel;
//     }

    if (oper.rank == 93/*NullStmt*/ or oper.rank == 141/*CXXDefaultArgExpr*/) return true;
    if (oper.rank==77/*ImplicitCastExpr*/ or oper.rank==43/*CaseStmt*/
        or oper.rank==64/*ExprWithCleanups*/ or oper.rank==83/*MaterializeTemporaryExpr*/
        or oper.rank==145/*PredefinedExpr*/ or oper.rank==13/*CXXBindTemporaryExpr*/) {
      RecursiveASTVisitor<NodeVisitor>::TraverseStmt(X);
      return true;
    }

// llvm::outs()  <<"X "<<name<<":"<<oper.rank<<"\n";

    Tree *resultTree = NULL ;
    if (oper.rank==68/*ForStmt*/) {
      resultTree = startTree(tb, 68/*ForStmt*/);
// ++indentLevel;
// llvm::outs() <<indentLevel<<"+"<<oper.rank<<":"<<name<<" STMT\n" ;
      // Complicated trick to handle opForStmt, which may be arity 5 instead of 4 due to the "condition variable"
      turnListFrontier(tb, resultTree) ; // begin Trick
      RecursiveASTVisitor<NodeVisitor>::TraverseStmt(X);
      while (listTreeLength(resultTree->contents.sons)<4) {
        mkPre(mkTree(&(tb->langOp[0/*None*/]), NULL), resultTree) ;
      }
      if (listTreeLength(resultTree->contents.sons)==5) {
        cutSon(resultTree, 3) ;
      }
      rearrangeClangOutput(oper, resultTree, NULL, X) ;
      terminateListTree(tb); // end of above Trick
// llvm::outs() <<indentLevel<<"-"<<resultTree->oper->rank<<":"<<resultTree->oper->name<<"\n" ;
// --indentLevel;
    } else if (oper.rank==149/*CXXTypeidExpr*/) {
      CXXTypeidExpr *typeIdX = dyn_cast<CXXTypeidExpr>(X) ;
      resultTree = startTree(tb, 149/*CXXTypeidExpr*/) ;
      turnListFrontier(tb, resultTree) ; // Trick to ensure that extra/missing children error will remain localized
// ++indentLevel;
// llvm::outs() <<indentLevel<<"+"<<oper.rank<<":"<<name<<" STMT\n" ;
      if (typeIdX->isTypeOperand()) {
        putType(typeIdX->getTypeOperand(*Context)) ;
      } else { //typeid has an "Expr" operand
        TraverseStmt(typeIdX->getExprOperand()) ;
      }
      rearrangeClangOutput(oper, resultTree, NULL, X) ;
      terminateListTree(tb); // end of above Trick
// llvm::outs() <<indentLevel<<"-"<<resultTree->oper->rank<<":"<<resultTree->oper->name<<"\n" ;
// --indentLevel;
    } else if (oper.rank==39/*CXXThrowExpr*/) {
      CXXThrowExpr *throwX = dyn_cast<CXXThrowExpr>(X) ;
      resultTree = startTree(tb, 39/*CXXThrowExpr*/) ;
      Expr *thrown = throwX->getSubExpr() ;
      if (thrown)
        TraverseStmt(thrown) ;
      else
        startTree(tb, 0/*None*/) ;
    } else if (oper.rank==15/*CXXCatchStmt*/) {
      CXXCatchStmt *catchS = dyn_cast<CXXCatchStmt>(X) ;
      resultTree = startTree(tb, 15/*CXXCatchStmt*/) ;
      VarDecl *caught = catchS->getExceptionDecl() ;
      if (caught)
        TraverseDecl(caught) ;
      else
        startTree(tb, 0/*None*/) ;
      TraverseStmt(catchS->getHandlerBlock()) ;
    } else if (oper.rank==118/*UnaryOperator*/) {
      UnaryOperator *unaryOp = dyn_cast<UnaryOperator>(X) ;
      resultTree = startTree(tb, 118/*UnaryOperator*/) ;
      turnListFrontier(tb, resultTree) ; // Trick to ensure that extra/missing children error will remain localized
      RecursiveASTVisitor<NodeVisitor>::TraverseStmt(X);
      if (!(unaryOp->getSubExpr())) { // if 3rd child is missing
        startTree(tb, 0/*None*/) ;
      }
      terminateListTree(tb); // end of above Trick
    } else {
      resultTree = startTree(tb, oper.rank);
// ++indentLevel;
// llvm::outs() <<indentLevel<<"+"<<oper.rank<<":"<<name<<" STMT\n" ;
      if (oper.arity!=0) turnListFrontier(tb, resultTree) ; // Trick to ensure that extra/missing children error will remain localized
      RecursiveASTVisitor<NodeVisitor>::TraverseStmt(X);
      rearrangeClangOutput(oper, resultTree, NULL, X) ;
      if (oper.arity!=0) terminateListTree(tb); // end of above Trick
// llvm::outs() <<indentLevel<<"-"<<resultTree->oper->rank<<":"<<resultTree->oper->name<<"\n" ;
// --indentLevel;
    }

//     if (oper.arity!=0) {
//       --indentLevel;
//       llvm::outs()  << "\n";
//       indent();
//     }
//     llvm::outs()  << ")";

// llvm::outs()  <<"<X "<<name<<":"<<oper.rank<<"\n";

    return true;
  }

  bool TraverseType(QualType T) {
//     llvm::outs()  << " traverseType " << T.getAsString() <<" dump:"<<dumpType(T) ;
    RecursiveASTVisitor<NodeVisitor>::TraverseType(T);
    return true;
  }

  bool TraverseInitListExpr(InitListExpr *S, DataRecursionQueue *Queue = nullptr) {
    if ( !getDerived().WalkUpFromInitListExpr(S))
      return false;
//     llvm::outs()  << dumpType(S->getType());
    if (S->hasArrayFiller()) {
      text = "arrayfiller";
    } else if (0==strncmp(dumpType(S->getType()),"struct",6)) {
      text = "struct";
    } else {
      text = "array";
    }
    startTree(tb, 139/*Keyword*/);
    putValue(tb, strdup(text));
    if (!getDerived().TraverseSynOrSemInitListExpr(S->isSemanticForm() ? S : S->getSemanticForm(), Queue))         
      return false;
    //~ if (!Queue && shouldTraversePostOrder() and !getDerived().WalkUpFromInitListExpr(S)) 
      //~ return false;                                    
    return true;                                                        
  }

  bool TraverseConstructorInitializer(CXXCtorInitializer *X) {
//     llvm::outs()  << "\n";
//     indent();
    if (X->isBaseInitializer()) {
//       text = dumpType(X->getTypeSourceInfo()->getType());
//       llvm::outs()  << "(\033[00;32mCXXCtorInitializerCall\033[00m (\033[00;33m20\033[00m) " << text;
//       startTree(tb, 20/*CXXCtorInitializer*/);
//       putType(X->getTypeSourceInfo()->getType());
      ++indentLevel;
      RecursiveASTVisitor<NodeVisitor>::TraverseConstructorInitializer(X);
      --indentLevel;
//       llvm::outs()  << "\n";
//       indent();
//       llvm::outs()  << ")";
    }
    if (X->isMemberInitializer()) {
      std::string text = X->getMember()->getNameAsString();
//       llvm::outs()  << "(\033[00;32mCXXCtorInitializer\033[00m (\033[00;33m20\033[00m) " << text;
      Tree *resultTree = startTree(tb, 20/*CXXCtorInitializer*/);
      startTree(tb, 85/*MemberExpr*/);
      startTree(tb, 38/*CXXThisExpr*/);
      putValue(tb, strdup("this"));
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("->"));
      startTree(tb, 136/*Name*/);
      putValue(tb, strdup(condenseTemplateSuffix(text).c_str())) ;
      ++indentLevel;
      RecursiveASTVisitor<NodeVisitor>::TraverseConstructorInitializer(X);
      if (!resultTree->contents.sons->next) startTree(tb,0/*None*/) ; // If no 2nd child was created, create one!
       startTree(tb,0/*None*/) ;
      --indentLevel;
//       llvm::outs()  << "\n";
//       indent();
//       llvm::outs()  << ")";
    }
    return true;                                                        
  }

  bool TraverseFunctionTemplateDecl(FunctionTemplateDecl *D) {
    // We ignore all canonical template stuff, and traverse only the specializations        
    if (!WalkUpFromFunctionTemplateDecl(D)) return false;                                             
    for (auto *FD : D->specializations()) {
      for (auto *RD : FD->redecls()) {
        //~ switch (RD->getTemplateSpecializationKind()) {
          //~ case TSK_Undeclared:
          //~ case TSK_ImplicitInstantiation:
          //~ case TSK_ExplicitInstantiationDeclaration:
          //~ case TSK_ExplicitInstantiationDefinition:
            //~ if (!TraverseDecl(RD)) return false;
            //~ break;
    
          //~ case TSK_ExplicitSpecialization:
            //~ break;
        //~ }
        if (!TraverseDecl(RD)) return false;
      }
    }
    return true;                                                        
  }

  bool TraverseClassTemplateDecl(ClassTemplateDecl *D) {
    if (!WalkUpFromClassTemplateDecl(D)) return false;                                             
    for (auto *SD : D->specializations()) {
      for (auto *RD : SD->redecls()) {
        if (cast<CXXRecordDecl>(RD)->isInjectedClassName()) continue;
        //~ switch (cast<ClassTemplateSpecializationDecl>(RD)->getSpecializationKind()) {
          //~ case TSK_Undeclared:
          //~ case TSK_ImplicitInstantiation:
            //~ if(!TraverseDecl(RD)) return false;
            //~ break;
          //~ case TSK_ExplicitInstantiationDeclaration:
          //~ case TSK_ExplicitInstantiationDefinition:
          //~ case TSK_ExplicitSpecialization:
            //~ break;
        //~ }
        if (!TraverseDecl(RD)) return false;
      }
    }
    return true;                                                        
  }

  bool TraverseVarTemplateDecl(VarTemplateDecl *D) {
    if (!WalkUpFromVarTemplateDecl(D)) return false;                                             
    for (auto *SD : D->specializations()) {
      for (auto *RD : SD->redecls()) {
        //~ switch (cast<VarTemplateSpecializationDecl>(RD)->getSpecializationKind()) {
          //~ case TSK_Undeclared:
          //~ case TSK_ImplicitInstantiation:
            //~ if(!TraverseDecl(RD)) return false;
            //~ break;
          //~ case TSK_ExplicitInstantiationDeclaration:
          //~ case TSK_ExplicitInstantiationDefinition:
          //~ case TSK_ExplicitSpecialization:
            //~ break;
        //~ }
        
        if (!TraverseDecl(RD)) return false;
      }
    }
    return true;                                                        
  }



  /* Overload a Visit* method to do stuff when iterating on a Node of given kind.
   * Note: Visiting order of the default implementation of the Traverse* methods is PRE-ORDER.
   *
   * Not all node kinds need to overload the Visit* method: only those where you collect information
   * on the node that are not children nodes (names and types, the most often) */

  bool VisitNamedDecl(const NamedDecl *ND){
//     llvm::outs()  <<  ND->getNameAsString();
    return true;
  }

  bool VisitAccessSpecDecl(const AccessSpecDecl *X) {
    switch(X->getAccess()){
      case AS_public:
        text = "public"; break;
      case AS_private:
        text = "private"; break;
      case AS_protected:
        text = "protected"; break;
      case AS_none:
        text = "none"; break;
    }
    startTree(tb, 139/*Keyword*/);
//     llvm::outs()  << text;
    putValue(tb, strdup(text));
    return true;
  }

  bool VisitArraySubscriptExpr(const ArraySubscriptExpr *X) {
    putType(X->getType());
    return true;
  }

  bool VisitBinaryOperator(const BinaryOperator *X) {
//     llvm::outs()  << BinaryOperator::getOpcodeStr(X->getOpcode());
    std::string nameStr = BinaryOperator::getOpcodeStr(X->getOpcode()).str();
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(nameStr.c_str()));
    //~ putTree(tb, resolScope(nameStr.c_str())) ;
    return true;
  }

  bool VisitBreakStmt (const BreakStmt  *X) {
    putValue(tb, strdup(""));
    return true;
  }

  bool VisitCStyleCastExpr(const CStyleCastExpr *X) {
    putType(X->getType());
    return true;
  }

  bool VisitCXXBoolLiteralExpr(const CXXBoolLiteralExpr *X) {
    if (X->getValue()) {
      text = "true";
    } else {
      text = "false";
    }
    putValue(tb, strdup(text));
    return true;
  }

  bool VisitCXXConstCastExpr(const CXXConstCastExpr *X) {
    putType(X->getType());
    return true;
  }

  bool VisitCXXConstructExpr(const CXXConstructExpr *X) {
    std::string classname = std::string(dumpType(X->getType()));
    putTree(tb, resolScope(classname.c_str())) ;
    CXXConstructorDecl *Ctor = X->getConstructor();
    std::string proto = std::string(dumpType(Ctor->getType()));
//     llvm::outs() << "'" << proto.c_str() << "' ";
    if (proto == std::string("void (const ")+classname+std::string(" &) noexcept")) {
      text = "copy";
    } else if (proto == std::string("void (")+classname+std::string(" &&) noexcept")) {
      text = "move";
    } else if (proto == std::string("void (void)") || proto == std::string("void (void) noexcept")) {
      text = "default";
    } else {
      text = "other";
//       llvm::outs()  << classname;
    }
    startTree(tb,139/*Keyword*/);
    putValue(tb, strdup(text));
    return true;
  }

  bool VisitCXXDefaultArgExpr( const CXXDefaultArgExpr *X) {
//     llvm::outs()  << dumpType(X->getParam()->getType());
//     text = dumpType(X->getParam()->getType());
//     putTree(tb, resolScope(strdup(text)));
    return true;
  }

  bool VisitCXXDeleteExpr(const CXXDeleteExpr *X) {
    std::string name = X->getOperatorDelete()->getNameInfo().getName().getAsString();
//     llvm::outs()  << name;
    putTree(tb, resolScope(name.c_str())) ;
    return true;
  }

  bool VisitCXXDynamicCastExpr(const CXXDynamicCastExpr *X) {
    putType(X->getType());
    return true;
  }

  bool VisitCXXFunctionalCastExpr(const CXXFunctionalCastExpr *X) {
    putType(X->getType());
    return true;
  }
  
  bool VisitCXXMethodDecl(const CXXMethodDecl *X) {
    startTree(tb, 133/*Qualifiers*/);
    if (X->isStatic()){
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("static"));
    }
    if (X->isVirtual()){
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("virtual"));
    }
    if (X->isConst()){
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("const"));
    }
    if (X->isVolatile()){
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("volatile"));
    }
    terminateListTree(tb);
    Tree *parentClassTree = resolScope(X->getParent()->getQualifiedNameAsString().c_str()) ;
    const char *parentOp = (dyn_cast<Decl>(X->getParent()))->getDeclKindName() ;
    int testLength = strlen(parentOp) ;
    if (testLength<27) testLength=27 ;
    if (!strncmp(parentOp, "ClassTemplateSpecialization", testLength)) {
      Tree *parentClassIdentTree = parentClassTree ;
      if (parentClassIdentTree->oper->rank==125/*ScopeAccess*/)
        parentClassIdentTree = parentClassIdentTree->contents.sons->next->tree ;
      const ClassTemplateSpecializationDecl *classTemplateSpecializationD =
        dyn_cast<ClassTemplateSpecializationDecl>(dyn_cast<Decl>(X->getParent())) ;
      extendIdentWithSpecialization(parentClassIdentTree, classTemplateSpecializationD) ;
    }
    putTree(tb, parentClassTree) ;
    return true;
  }

  bool VisitCXXNewExpr(const CXXNewExpr *X) {
    std::string nameStr = X->getOperatorNew()->getNameInfo().getName().getAsString();
//     llvm::outs()  << nameStr;
//     text = dumpType(X->getAllocatedType());
//     llvm::outs()  << " " << text ;
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(condenseTemplateSuffix(nameStr).c_str()));
    //~ putTree(tb, resolScope(nameStr.c_str())) ;

    putType(X->getAllocatedType());
    if (strlen(nameStr.c_str()) < 14) {
      startTree(tb, 81);
      putValue(tb, strdup("1"));
    }
    return true;
  }

  bool VisitCXXNullPtrLiteralExpr(const CXXNullPtrLiteralExpr *X) {
    putValue(tb, strdup(""));
    return true;
  }

  bool VisitCXXRecordDecl(const CXXRecordDecl *X) {
    startTree(tb, 139/*Keyword*/);
    putValue(tb, strdup(X->getKindName().str().c_str()));
    std::string fullQClassName = X->getQualifiedNameAsString() ;
    putTree(tb, resolScope(fullQClassName.c_str())) ;
    if (!X->isCompleteDefinition()) return true;
//     llvm::outs()  << " (inherits from " << X->getNumBases()<< " bases)";
    if (!X->getNumBases()) return true;
    startTree(tb, 133/*Qualifiers*/);
    for (const auto &I : X->bases()) {
//       llvm::outs()  << "\n";
      ++indentLevel;
//       indent();
      switch (I.getAccessSpecifier()){
        case AS_public:
          text = "public" ; break;
        case AS_protected:
          text = "protected" ; break;
        case AS_private:
          text = "private" ; break;
        case AS_none:
          text = "none" ; break;
      }
//       llvm::outs()  << '(' << text << " " << dumpType(I.getTypeSourceInfo()->getType()) << ')';
      --indentLevel;
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup(text));
      putType(I.getTypeSourceInfo()->getType());
    }
    terminateListTree(tb); //Qualifiers
    return true;
  }

  bool VisitCXXReinterpretCastExpr(const CXXReinterpretCastExpr *X) {
    putType(X->getType());


    return true;
  }

  bool VisitCXXScalarValueInitExpr(const CXXScalarValueInitExpr *X) {
    putValue(tb, strdup("default"));
    return true;
  }

  bool VisitCXXStaticCastExpr(const CXXStaticCastExpr *X) {
    putType(X->getType());


    return true;
  }

  bool VisitCXXTemporaryObjectExpr(const CXXTemporaryObjectExpr *X) {
    
    return true;
  }

  bool VisitCXXThisExpr(const CXXThisExpr *X) {
    putValue(tb, strdup("this"));
    return true;
  }

  bool VisitCaseStmt(const CaseStmt *X) {
    startTree(tb, 43);
    return true;
  }

  bool VisitCharacterLiteral(const CharacterLiteral *X) {
    char a = X->getValue();
    if (a!='\n') {
      char* newtext = (char*) malloc(sizeof(char)+1);
      sprintf(newtext,"%c",a);
      
      putValue(tb, strdup(newtext));
//       llvm::outs()  << newtext;
      free(newtext);
    } else {
      putValue(tb, strdup("\\n"));
//       llvm::outs()  << "\\n";
    }
    return true;
  }

  bool VisitContinueStmt(const ContinueStmt *X) {
    putValue(tb, strdup(""));
    return true;
  }

  bool VisitDeclRefExpr(const DeclRefExpr *X) {
    std::string name;
    if (X->hasQualifier())
        name = X->getDecl()->getQualifiedNameAsString();
    else
        name = X->getDecl()->getNameAsString(); 
    putTree(tb, resolScope(name.c_str())) ;
    return true;
  }

  bool VisitDefaultStmt(const DefaultStmt *X) {
    putValue(tb, strdup(""));
    return true;
  }

  bool VisitEnumConstantDecl(const EnumConstantDecl *X) {
    std::string textStr = X->getNameAsString();
    startTree(tb, 106/*StringLiteral*/);
    putValue(tb, strdup(textStr.c_str()));
    return true;
  }

  bool VisitEnumDecl(const EnumDecl *X) {
    std::string textStr = X->getNameAsString();
    putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

  bool VisitFieldDecl(const FieldDecl *X) {
    std::string name;
    if (X->getQualifier())
        name = X->getQualifiedNameAsString();
    else
        name = X->getNameAsString(); 
    putTree(tb, resolScope(name.c_str())) ;
    putType(X->getType());
    return X->hasInClassInitializer();
  }

  bool VisitFloatingLiteral(const FloatingLiteral *X) {
//     llvm::outs()  << X->getValueAsApproximateDouble();
    std::string textStr = std::to_string(X->getValueAsApproximateDouble());
    putValue(tb, strdup(textStr.c_str()));
    return true;
  }

  bool VisitFunctionDecl(const FunctionDecl *X) {
    std::string name = X->getNameInfo().getName().getAsString();
    putTree(tb, resolScope(name.c_str())) ;
    putType(X->getReturnType());
    return true;
  }

  bool VisitGNUNullExpr(const GNUNullExpr *X) {
    putValue(tb, strdup(""));
    return true;
  }

  bool VisitGotoStmt(const GotoStmt *X) {
//     llvm::outs()  << X->getLabel()->getQualifiedNameAsString().c_str();
    std::string textStr = X->getLabel()->getNameAsString();
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(textStr.c_str()));
    //~ putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

  bool VisitImplicitValueInitExpr(const ImplicitValueInitExpr *X) {
    putValue(tb, strdup("default"));
    return true;
  }

  bool VisitIntegerLiteral(const IntegerLiteral *X){
//     llvm::outs()  << X->getValue();
    // In Clang-11 was: std::string textStr = X->getValue().toString(10, true);
    // Danger, are we sure this is always a signed? :
    std::string textStr = llvm::toString(X->getValue(), 10, true);
    putValue(tb, strdup(textStr.c_str()));
    return true;
  }

  bool VisitLabelDecl(const LabelDecl *X) {
//     llvm::outs()  << X->getQualifiedNameAsString().c_str();
    std::string textStr = X->getNameAsString();
    putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

  bool VisitLabelStmt(const LabelStmt *X) {
//     llvm::outs()  << X->getDecl()->getQualifiedNameAsString().c_str();
    std::string textStr = X->getDecl()->getNameAsString();
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(textStr.c_str()));
    //~ putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

//   bool VisitMemberExpr(const MemberExpr *X) {
// //     llvm::outs()  << (X->isArrow() ? "->" : ".") << *X->getMemberDecl();
//     text = (X->isArrow() ? "->" : ".");
//     std::string name;
//     if (X->hasQualifier())
//         name = X->getMemberDecl()->getQualifiedNameAsString();
//     else
//         name = X->getMemberDecl()->getNameAsString();
//     startTree(tb, 139/*Keyword*/);
//     putValue(tb, strdup(text));
//     putTree(tb, resolScope(name.c_str())) ;
//     return true;
//   }

  bool VisitNamespaceAliasDecl(const NamespaceAliasDecl *X) {
    std::string textStr = X->getQualifiedNameAsString() ;
    putTree(tb, resolScope(textStr.c_str())) ;
    textStr = X->getAliasedNamespace()->getDeclName().getAsString() ;
    putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

  bool VisitNamespaceDecl(const NamespaceDecl *X) {
    std::string textStr = X->getNameAsString();
// printf("   NAMESPACE NAME: %s\n",textStr.c_str()) ;
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(textStr.c_str()));
    return true;
  }

  bool VisitStringLiteral(const clang::StringLiteral *X){
    //~ X->outputString(llvm::outs());
    std::string S = X->getString().str();
    size_t pch = S.find("\n");
    while(pch != std::string::npos)
    {
      S.replace(pch,1,"\\n");
      pch = S.find("\n");
    }
    putValue(tb, strdup(S.c_str()));
    return true;
  }

//   bool VisitTranslationUnitDecl(const TranslationUnitDecl *X) {
// //     currentFileID = Context->getSourceManager().getFileID(X->getLocation());
// //     currentFileName = Context->getSourceManager().getFilename(X->getLocation());
//     return true;
//   }

  bool VisitTypeAliasDecl(const TypeAliasDecl *X) {
    std::string textStr = X->getQualifiedNameAsString();
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(condenseTemplateSuffix(textStr).c_str()));
    //~ putTree(tb, resolScope(textStr.c_str())) ;
    putTree(tb, resolScope(dumpType(X->getUnderlyingType()))) ;
    return true;
  }

  bool VisitTypedefDecl(const TypedefDecl *X) {
    QualType T = X->getUnderlyingType();
    text = dumpType(T);
//     llvm::outs()  << " " << text << " ";
    putType(T);
    std::string name = X->getNameAsString();
    startTree(tb, 136/*Name*/);
    putValue(tb,strdup(condenseTemplateSuffix(name).c_str()));
    return false;
  }

  bool VisitUnaryExprOrTypeTraitExpr(const UnaryExprOrTypeTraitExpr *X) {
    switch (X->getKind()){
      case UETT_SizeOf:
        name = "sizeof";
        break;
      case UETT_AlignOf:
        name = "alignof";
        break;
      case UETT_VecStep:
        name = "vecStep";
        break;
      case UETT_OpenMPRequiredSimdAlign:
        name = "OpenMPRequiredSimdAlign";
        break;
      // In Clang-11 inexistent case:
      case UETT_PreferredAlignOf:
        name = "PreferredAlignOf";
        break;
    }
    startTree(tb, 136/*Name*/) ;
    putValue(tb, strdup(name)) ;
    if (X->isArgumentType())
      putType(X->getArgumentType());
//     else {
//       startTree(tb, 136/*Name*/);
//       putValue(tb, strdup("SHOULD_BE_EXPRESSION_TREE"));
//     }
    return true;
  }

  bool VisitUnaryOperator(const UnaryOperator *X) {
//     llvm::outs() << (X->isPostfix() ? "postfix" : "prefix") << " " << UnaryOperator::getOpcodeStr(X->getOpcode());
    std::string nameStr = UnaryOperator::getOpcodeStr(X->getOpcode()).str();
    if (X->isPostfix())
      text = "postfix";
    else
      text = "prefix";
    startTree(tb, 139/*Keyword*/);
    putValue(tb, strdup(text));
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(nameStr.c_str()));
    return true;
  }

  bool VisitUsingDecl(const UsingDecl *X) {
    /*std::string textStr = X->getQualifiedNameAsString() ;*/
    startTree(tb, 136/*Name*/);
    putValue(tb, strdup(/*textStr*/X->getQualifiedNameAsString().c_str()));
    const char* text2 ;
    if (X->getQualifier() &&  X->getQualifier()->getAsNamespace()) {
// X->getQualifier()->print(llvm::outs(), X->getASTContext().getPrintingPolicy());
      text2 = X->getQualifier()->getAsNamespace()->getQualifiedNameAsString().c_str() ;
    } else {
      text2 = "strange_namespace";
    }
    putTree(tb, resolScope(text2)) ;
    return true;
  }

  bool VisitUsingDirectiveDecl(const UsingDirectiveDecl *X) {
    std::string textStr = X->getNominatedNamespace()->getQualifiedNameAsString();
    putTree(tb, resolScope(textStr.c_str())) ;
    return true;
  }

  bool VisitVAArgExpr(const VAArgExpr *X) {
    putType(X->getWrittenTypeInfo()->getType());
    return true;
  }

  bool VisitVarDecl(const VarDecl *X) {
// llvm::errs()  << "VARDECL BUILT BY CLANG:\n" ;
// X->dump() ;
    const std::string nameStr = X->getName().str() ;
// llvm::errs() <<"NAME IS "<<X->getName()<<" or "<<X->getNameAsString()<<" or "<<X->getQualifiedNameAsString()<<" CHOOSED:"<<nameStr<<"\n" ;
    putTree(tb, resolScope(nameStr.c_str())) ;
    if (X->getStorageClass() != SC_None){
      switch(X->getStorageClass()) {
        case SC_None:                 break;
        case SC_Auto:                 text = "auto"; break;
        case SC_Extern:               text = "extern"; break;
        case SC_PrivateExtern:        text = "__private_extern__"; break;
        case SC_Register:             text = "register"; break;
        case SC_Static:               text = "static"; break;
      }
      startTree(tb, 135/*Type*/) ;
      startTree(tb, 133/*Qualifiers*/) ;
      startTree(tb, 139/*Keyword*/) ;
      putValue(tb, strdup(text));
      terminateListTree(tb); //Qualifiers
    }
    putType(X->getType());
    return X->hasInit();
  }

  //This asks clang to visit template specializations.
  bool shouldVisitTemplateInstantiations() const { return true; }

  //This hides from the AST all implicit declarations: basic typedefs, malloc/new/free/delete, copy constructors, ...
  bool shouldVisitImplicitCode() const { return false; }

//   bool shouldVisitIncludedEntities() const { return true; }
  
private:
  ASTContext  *Context;     //Information about the input source code (files, line numbers...)
  int         indentLevel;
public:
  FileID      currentFileID;
  StringRef   currentFileName;
  std::vector<FileID> prevIncludes;

  void indent() {
    int i = indentLevel;
    while (i-->0)
      llvm::outs()  << "| ";
  }

  const char* dumpType(const QualType& T) {
    SplitQualType T_split = T.split();
     
    if (!T.isNull()) {
      SplitQualType D_split = T.getSplitDesugaredType();
      if (T_split != D_split) {
        return strdup(QualType::getAsString(D_split, LangOptions()).c_str());
      } else {
        return strdup(QualType::getAsString(T_split, LangOptions()).c_str());
      }
    } else {
//       llvm::outs()  << " strange type..." ;
      return strdup("strange type...");
    }
  }

  Tree* buildTypeTree(QualType T) {

    ListTree* qualifiersList = NULL ;
    if (T.isLocalVolatileQualified()) {
      qualifiersList =
        mkSons(mkAtom(&(tb->langOp[139/*Keyword*/]), strdup("volatile")),
               qualifiersList) ;
      T.removeLocalVolatile();
    }
    if (T.isLocalConstQualified()) {
      qualifiersList =
        mkSons(mkAtom(&(tb->langOp[139/*Keyword*/]), strdup("const")),
               qualifiersList) ;
      T.removeLocalConst();
    }
    if (T.isLocalRestrictQualified()) {
      qualifiersList =
        mkSons(mkAtom(&(tb->langOp[139/*Keyword*/]), strdup("restrict")),
               qualifiersList) ;
      T.removeLocalRestrict();
    }

    Tree* typeBasis = NULL ;
    if (dyn_cast<TypedefType>(T)) {
// llvm::errs()  << "     GOING INSIDE TYPEDEF:\n" ;
// T.getTypePtr()->dump() ;
// llvm::errs()  << "       CANONICAL:\n" ;
// T.getCanonicalType().getTypePtr()->dump() ;
// llvm::errs()  << "     end TYPEDEF\n" ;
      // TODO: To avoid cases of infinite looping, probably we should rather return just the type name!
      typeBasis = buildTypeTree(T.getCanonicalType()) ;
    } else if (T.getTypePtr()->isPointerType()) {
      typeBasis = mkTree(&(tb->langOp[132/*opPointerType*/]),
                        mkSons(buildTypeTree(T.getTypePtr()->getPointeeType()), NULL)) ;
    } else if (T.getTypePtr()->isReferenceType()) {
      typeBasis = mkTree(&(tb->langOp[131/*opReference*/]),
                        mkSons(mkAtom(&(tb->langOp[139/*Keyword*/]), strdup("&")),
                               mkSons(buildTypeTree(T.getTypePtr()->getPointeeType()), NULL))) ;
    } else if (T.getTypePtr()->isFunctionType()) {
      QualType TinsideTypeDef = T.getCanonicalType() ;
      clang::FunctionType* function =
        static_cast<clang::FunctionType*>(const_cast<clang::Type*>(TinsideTypeDef.getTypePtr()));
      typeBasis = mkTree(&(tb->langOp[65/*opFunctionType*/]),
                        mkSons(buildTypeTree(function->getReturnType()), NULL)) ;
      FunctionProtoType* proto = static_cast<FunctionProtoType*>(function);
      for (const QualType &A : proto->param_types()) {
        mkPost(typeBasis, buildTypeTree(A)) ;
      }
    } else if (T.getTypePtr()->isArrayType()) {
      clang::ArrayType* array = static_cast<clang::ArrayType*>(const_cast<clang::Type*>(T.getTypePtr()));
      const char *intLit ;
      if (auto carray =  dyn_cast_or_null<ConstantArrayType>(array))
        intLit = std::to_string(carray->getSize().getLimitedValue()).c_str() ;
      else
        intLit = "unknown" ;
      typeBasis = mkTree(&(tb->langOp[142/*opArrayType*/]),
                         mkSons(buildTypeTree(array->getElementType()),
                                mkSons(mkAtom(&(tb->langOp[81/*opIntegerLiteral*/]), strdup(intLit)), NULL))) ;
    } else {
      SplitQualType T_split = T.split();
      const std::string full = QualType::getAsString(T_split, LangOptions()) ;
//       llvm::errs() << " GOING TO RESOL SCOPE:" << full << '\n' ;
      typeBasis = resolScope(full.c_str()) ;
    }

    return mkTree(&(tb->langOp[135/*Type*/]),
                  mkSons(mkTree(&(tb->langOp[133/*Qualifiers*/]), qualifiersList),
                         mkSons(typeBasis, NULL))) ;
  }

  void putType(QualType T) {
    startTree(tb, 135/*Type*/);
    // build first child Tree for qualifiers:
    startTree(tb, 133/*Qualifiers*/);
    if (T.isLocalRestrictQualified()) {
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("restrict")) ;
      T.removeLocalRestrict();
    }
    if (T.isLocalConstQualified()) {
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("const")) ;
      T.removeLocalConst();
    }
    if (T.isLocalVolatileQualified()) {
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("volatile")) ;
      T.removeLocalVolatile();
    }
    if (dyn_cast<AtomicType>(T)) {
      startTree(tb, 139/*Keyword*/);
      putValue(tb, strdup("_Atomic")) ;
    }
    terminateListTree(tb);  //Qualifiers

// llvm::errs() << "TYPE TO RECOGNIZE:\n" ;
// T.getTypePtr()->dump() ;
// llvm::errs() << "\n" ;

    // build second child Tree for the non-qualified type:
    if (dyn_cast<BuiltinType>(T) && !T.getTypePtr()->isDependentType()) { //Danger: dyn_cast<BuiltinType>(T) IS NOT T.isBuiltinType() !!
      const clang::BuiltinType* builtin = static_cast<const clang::BuiltinType*>(T.getTypePtr());
      const char *full = builtin->getNameAsCString(LangOptions()) ;
// llvm::errs() << "GOING TO RESOLSCOPE ON BuiltinType:" << strdup(full) << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<TypedefType>(T)) {

      // To avoid infinite looping in recursive types, we must return the type NAME:
//       const clang::TypedefType* typedefT = static_cast<const clang::TypedefType*>(T.getTypePtr());
//       char *full0 = strdup(QualType::getAsString(typedefT->desugar().split(), LangOptions()).c_str()) ;
// llvm::errs() << "ALT FULL0: " << full0 << "\n" ;
      const clang::TypedefType *typeDef = T->getAs<TypedefType>() ;
//       char *full1 = strdup(typeDef->getDecl()->getNameAsString().c_str()) ;
// llvm::errs() << "ALT FULL1: " << full1 << "\n" ;
//       std::string full2 = typeDef->getDecl()->getDeclName().getAsString() ;
// llvm::errs() << "ALT FULL2: " << full2 << "\n" ;
// llvm::errs() << "TYPEDEF DECL TO RECOGNIZE:\n" ;
// typeDef->getDecl()->dump() ;
// llvm::errs() << "\n" ;

      char *full = strdup(typeDef->getDecl()->getQualifiedNameAsString().c_str()) ;
// llvm::errs() << "GOING TO RESOLSCOPE ON NAMED TYPE NAME " << full << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<TemplateSpecializationType>(T)) {
// llvm::errs() << "TYPE RECOGNIZED AS TemplateSpecializationType:\n" ;
// T.getTypePtr()->dump() ;
// llvm::errs() << "\n" ;
      const clang::TemplateSpecializationType* templateST = static_cast<const clang::TemplateSpecializationType*>(T.getTypePtr());
      QualType SingleStepDesugar =
         templateST->getLocallyUnqualifiedSingleStepDesugaredType();
      char *full = strdup(QualType::getAsString(SingleStepDesugar.split(), LangOptions()).c_str()) ;
// llvm::errs() << "SINGLESTEPDESUGAR " << strdup(QualType::getAsString(SingleStepDesugar.split(), LangOptions()).c_str()) << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<ElaboratedType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS ElaboratedType:\n" ;
//       T.getTypePtr()->dump() ;
//       llvm::errs() << "\n" ;
// llvm::errs() << "GOING TO RESOLSCOPE ON ElaboratedType " << strdup(QualType::getAsString(T.split(), LangOptions()).c_str()) << "\n" ;
      const clang::ElaboratedType* elabT = static_cast<const clang::ElaboratedType*>(T.getTypePtr());
//       llvm::errs() << "ELABT.GETNAMEDTYPE:\n" ;
//       elabT->getNamedType().dump() ;
//       llvm::errs() << "\n" ;
      char *full = strdup(QualType::getAsString(elabT->getNamedType().split(), LangOptions()).c_str()) ;
//llvm::errs() << "full:" << full << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<SubstTemplateTypeParmType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS SubstTemplateTypeParmType:\n" ;
//       T.getTypePtr()->dump() ;
//       llvm::errs() << "\n" ;
      //const clang::SubstTemplateTypeParmType* substT = static_cast<const clang::SubstTemplateTypeParmType*>(T.getTypePtr());
// llvm::errs() << "GOING TO RESOLSCOPE ON SubstTemplateTypeParmType " << strdup(QualType::getAsString(T.split(), LangOptions()).c_str()) << "\n" ;
      putTree(tb, resolScope(QualType::getAsString(T.split(), LangOptions()).c_str())) ;

    } else if (dyn_cast<RecordType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS RecordType.\n" ;
      char* full = strdup(QualType::getAsString(T.split(), LangOptions()).c_str());
// llvm::errs() << "GOING TO RESOLSCOPE ON RecordType NAME " << full << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<EnumType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS EnumType.\n" ;
      char* full = strdup(QualType::getAsString(T.split(), LangOptions()).c_str());
// llvm::errs() << "GOING TO RESOLSCOPE ON EnumType NAME " << full << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<AutoType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS AutoType.\n" ;
      char* full = strdup(QualType::getAsString(T.split(), LangOptions()).c_str());
// llvm::errs() << "GOING TO RESOLSCOPE ON AutoType NAME " << full << "\n" ;
      putTree(tb, resolScope(full)) ;

    } else if (dyn_cast<AtomicType>(T)) {
//       llvm::errs() << "TYPE RECOGNIZED AS AtomicType.\n" ;
      // _Atomic qualifier has already beed added to the 133/*Qualifiers*/ list
      const clang::AtomicType* atomicT = static_cast<const clang::AtomicType*>(T.getTypePtr());
      putType(atomicT->getValueType()) ;

    } else if (T.getTypePtr()->isPointerType()) {
      startTree(tb,132/*PointerType*/);
      putType(T.getTypePtr()->getPointeeType());

    } else if (T.getTypePtr()->isReferenceType()) {
      startTree(tb,131/*Reference*/);
      startTree(tb,139/*Keyword*/);
      putValue(tb, strdup("&")) ;
      putType(T.getTypePtr()->getPointeeType());

    } else if (T.getTypePtr()->isFunctionType()) {
//       QualType TinsideTypeDef = T.getCanonicalType() ;
//       clang::FunctionType* function = static_cast<clang::FunctionType*>(const_cast<clang::Type*>(TinsideTypeDef.getTypePtr()));
// let's try instead *not* to canonicalize:
      clang::FunctionType* function = static_cast<clang::FunctionType*>(const_cast<clang::Type*>(T.getTypePtr())) ;
      FunctionProtoType* proto = static_cast<FunctionProtoType*>(function);
      startTree(tb,65/*FunctionType*/);
      putType(function->getReturnType());
      for (const QualType &A : proto->param_types()) {
        putType(A);
      }
      terminateListTree(tb); //FunctionType

    } else if (T.getTypePtr()->isArrayType()) {
      startTree(tb, 142/*ArrayType*/);
      clang::ArrayType* array = static_cast<clang::ArrayType*>(const_cast<clang::Type*>(T.getTypePtr()));
      putType(array->getElementType());
      startTree(tb, 81/*IntegerLiteral*/); //[llh] TODO: care for cases other that plain integer litteral!!
      if (auto carray =  dyn_cast_or_null<ConstantArrayType>(array)){
        putValue(tb, strdup(std::to_string(carray->getSize().getLimitedValue()).c_str()));
      } else {
        putValue(tb, strdup("unknown"));
      }

    } else { // Catch-all case, to be avoided! :
      llvm::errs() << "TYPE NOT RECOGNIZED: " ;
      T.getTypePtr()->dump() ;
      llvm::errs() << "\n" ;
      startTree(tb, 136/*Name*/) ;
      putValue(tb, strdup(QualType::getAsString(T.split(), LangOptions()).c_str()));
    }
  }

  /** Builds the Tree for a composite identifier of shape "NN::CC<TT1,TT2<TT3>>::ID" */
  Tree* resolScope(const char* full) {
    // "anonymous" ?
    if (0==strncmp(full, "(anonymous", 10)) {
      return mkAtom(&(tb->langOp[136/*Name*/]), strdup("")) ;
    } else {
      const char* identStart ;
      int identOffset ;
      int inTemplateParams = 0 ;
      Tree *result = NULL ;
      // first, get rid of initial "typename " or "class " or "struct "
      if (0==strncmp(full, "typename ", 9)) full += 9 ;
      if (0==strncmp(full, "class ", 6))  full += 6 ;
      if (0==strncmp(full, "struct ", 7))  full += 7 ;
      // then advance through successive "::" and build Tree on the fly.
      while (*full) {
        identStart = full ;
        identOffset = 0 ;
        while (true) {
          if (!*full) {
            break ;
          } else if (!inTemplateParams && *full==':' && *(full+1)==':') {
            full += 2 ;
            break ;
          } else if (*full=='<') {
            ++inTemplateParams ;
          } else if (*full=='>') {
            --inTemplateParams ;
          }
          ++identOffset ;
          ++full ;
        }
        Tree *newSubTree = mkAtom(&(tb->langOp[136/*Name*/]), strdup(condenseTemplateSuffix(std::string(identStart, identOffset)).c_str()));
        if (result) {
          result = mkTree(&(tb->langOp[125/*ScopeAccess*/]),
                     mkSons(result, mkSons(newSubTree, NULL))) ;
        } else {
          result = newSubTree ;
        }
      }
      return (result ? result : mkAtom(&(tb->langOp[136/*Name*/]), strdup(""))) ;
    }
  }

} ;

// Launches the iterator on the first AST node:
class TreeVisitor : public clang::ASTConsumer {

public:
  explicit TreeVisitor(ASTContext *Context, StringRef InFile) : Visitor(Context, InFile) {
    Visitor.currentFileID = Context->getSourceManager().getMainFileID();
    Visitor.currentFileName = InFile;
    Visitor.prevIncludes.push_back(Visitor.currentFileID);
  }

  virtual void HandleTranslationUnit(clang::ASTContext &Context) {
    //Starts the recursive iterations from the location of the first Translation Unit
    Decl *builtByClang = Context.getTranslationUnitDecl() ;
    StringRef fileName = Context.getSourceManager().getFileEntryForID(Context.getSourceManager().getMainFileID())->getName() ;
    if (trace0) {
      llvm::errs()  << "BUILT BY CLANG:\n" ;
      builtByClang->dump() ;
      llvm::errs()  << ".......\n" ;
    }
    startTree(tb, 136); // opName
    putValue(tb, strdup(fileName.str().c_str()));
    Visitor.TraverseDecl(builtByClang);
  }

private:
  NodeVisitor Visitor;
};

// Customized "FrontendAction" of clang's frontend compiler. Tells clang what to do with the AST:
class VisitingAction : public ASTFrontendAction {

public:
  virtual std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &Compiler, StringRef InFile) {
    return std::unique_ptr<ASTConsumer>(new TreeVisitor(&Compiler.getASTContext(), InFile));
  }

};

// Used to search a const char* in the language tables.
size_t max(size_t a, size_t b){
  if (a>b) return a;
  return b;
}

int getOperIndex(const char* myClangOperName, int namelen) {
  int i = 0;
  while (i<nClangOperators &&
         strncmp(myClangOperName, clangOperators[i].name,
                 max(namelen, strlen(clangOperators[i].name))))
    ++i ;
  return  i ;
}

Operator getOperOfDecl(const char* clangOperName, Decl* X) {
  size_t namelen = strlen(clangOperName) + 6 ;
  char *myClangOperName = (char*)malloc(namelen+1);
  sprintf(myClangOperName, "op%sDecl", clangOperName);
  int i = getOperIndex(myClangOperName, namelen) ;
  if (i>=nClangOperators) {
    llvm::errs() << "UNEXPECTED CLANG OPERATOR: " << clangOperName << " OF DECL:\n" ;
    X->dump() ;
    llvm::errs() << "\n" ;
    i = 0 ; // corresponds to "my Clang" opNone.
  }
  return clangOperators[i];
}

Operator getOperOfStmt(const char* clangOperName, Stmt* X) {
  size_t namelen = strlen(clangOperName) + 2 ;
  char *myClangOperName = (char*)malloc(namelen+1);
  sprintf(myClangOperName, "op%s", clangOperName);
  int i = getOperIndex(myClangOperName, namelen) ;
  if (i>=nClangOperators) {
    llvm::errs() << "UNEXPECTED CLANG OPERATOR: " << clangOperName << " OF STMT:\n" ;
    X->dump() ;
    llvm::errs() << "\n" ;
    i = 0 ; // corresponds to "my Clang" opNone.
  }
  return clangOperators[i];
}

/** C++ parser for Tapenade: takes in a list of C++ file names, writes out the contents
 * of these files in the form of IL protocol. In-between, uses Clang to parse the files,
 * traverses the Clang internal representation to build an AST with Clang's operators,
 * and finally translates the Clang AST into an IL AST with clang2il.ttml */
int main(int argc, const char **argv) {
  // Get the files to compile from the command line:
  if (argc==0) {
    llvm::outs() << "Requires at least one file name argument\n" ;
    exit(0) ;
  }
  if (argc>0) {
    if (strstr(argv[1], "trace012")) {
      trace0 = trace1 = trace2 = true ;
    } else if (strstr(argv[1], "trace01")) {
      trace0 = trace1 = true ;
    } else if (strstr(argv[1], "trace12")) {
      trace1 = trace2 = true ;
    } else if (strstr(argv[1], "trace02")) {
      trace0 = trace2 = true ;
    } else if (strstr(argv[1], "trace0")) {
      trace0 = true ;
    } else if (strstr(argv[1], "trace1")) {
      trace1 = true ;
    } else if (strstr(argv[1], "trace2")) {
      trace2 = true ;
    }
  }
  bool trace = trace0 || trace1 || trace2 ;
  const char** newargv = (trace?argv+1:argv) ;
  if (trace) --argc ;
  // In Clang-11 was: CommonOptionsParser OptionsParser(argc, newargv, MyToolCategory);
  CommonOptionsParser OptionsParser =
    llvm::cantFail(CommonOptionsParser::create(argc, newargv, MyToolCategory));
  // Parse the files:
  ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());
  // Traverse Clang's IR to build a Clang AST:

  TemplateSuffixesChain firstCellOfTemplateSuffixesChain = {NULL, NULL} ;
  templateSuffixesChain = &firstCellOfTemplateSuffixesChain ;
  tb = newTreeBuilder(clangOperators);
  int result = Tool.run(newFrontendActionFactory<VisitingAction>().get());
  int rank = 0 ;
  while (templateSuffixesChain->next!=NULL) {
    templateSuffixesChain = templateSuffixesChain->next ;
    ++rank ;
    llvm::errs() <<"_SPEC"<<rank<<" : "<<templateSuffixesChain->templateSuffix<<"\n" ;
  }
//   TemplateTreesChain firstCellOfTemplateTreesChain = {NULL, NULL} ;
//   templateTreesChain = &firstCellOfTemplateTreesChain ;
//   tb = newTreeBuilder(clangOperators);
//   int result = Tool.run(newFrontendActionFactory<VisitingAction>().get());

//   printf("-------------- SHORT NAMES: --------------\n") ;
//   templateTreesChain = templateTreesChain->next ;
//   int rank = 1 ;
//   while (templateTreesChain!=NULL) {
//     printf("<%i> :\n",rank) ;
//     showListTree(templateTreesChain->templateTrees, 0) ;
//     printf("\n") ;
//     ++rank ;
//     templateTreesChain = templateTreesChain->next ;
//   }

  ListTree *parsedTrees = getListTreeBuilt(tb);
  Tree *parsedFileName, *parsedFileTree ;
  char *fileName ;
  while (parsedTrees!=NULL) {
    parsedFileName = parsedTrees->tree ;
    fileName = parsedFileName->contents.value ;
    parsedTrees = parsedTrees->next ;
    parsedFileTree = parsedTrees->tree ;
    if (trace1) {
      printf("-------------- CLANG --------------\n") ;
      printf("From file %s:\n", fileName) ;
      showTree(parsedFileTree, 0);
    }
    if (trace2) {
      printf("-------------- IL --------------\n") ;
      showTree(clang2ilTree(parsedFileTree), 0);
    }
    if (!(trace0||trace1||trace2)) {
      fprintf(stdout,"From file %s\n", fileName) ;
      // Translate the Clang AST into an IL AST, and send it:
      clang2ilProtocol(parsedFileTree, stdout);
    }
    parsedTrees = parsedTrees->next ;
  }
  return result;
}

// Chasser les getAsString()
// -> aller voir isDependentType/isInstantiationDependentType
// + Question des types de kind=8="pack" ??
