/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public abstract class Phylum {
    /**
     * The (ordered) list of all phyla created for this formalism
     */
    public static MiniList allPhyla = null;
    public String name = null;
    public int rank = -1;
    public MiniList operators = null;

    public Phylum(String name, Formalism ctf) {
        this.name = name;
        allPhyla = new MiniList(this, allPhyla);
    }

    public void insert(Operator op) {
        operators = new MiniList(op, operators);
    }

    public String creation() {
        return "<Forbidden unspecified Phylum:" + name + " rank:" + rank + ">";
    }

}
