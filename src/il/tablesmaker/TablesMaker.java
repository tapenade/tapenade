/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class TablesMaker {

    public static void main(final String[] args) {

        String targetMode = "tablesC";
        if (args.length > 0
                && ("tablesC".equals(args[0])
                || "tablesJavaConstants".equals(args[0])
                || "tablesJava".equals(args[0]))) {
            targetMode = args[0];
        }

        Formalism formalism = new Formalism().create();
        Phylum.allPhyla = MiniList.nreverse(Phylum.allPhyla);
        MiniList pha = Phylum.allPhyla;
        int phNumber = 0;
        while (pha != null) {
            Phylum ph = (Phylum) pha.head;
            phNumber++;
            ph.rank = phNumber;
            pha = pha.tail;
        }
        Operator.allOperators = MiniList.nreverse(Operator.allOperators);
        MiniList ops = Operator.allOperators;
        int opNumber = 0;
        while (ops != null) {
            Operator op = (Operator) ops.head;
            opNumber++;
            op.rank = opNumber;
            ops = ops.tail;
        }

        if ("tablesJavaConstants".equals(targetMode)) {

            System.out.println("/*");
            System.out.println(" * TAPENADE Automatic Differentiation Engine");
            System.out.println(" * Copyright (C) 1999-2021 Inria");
            System.out.println(" * See the LICENSE.md file in the project root for more information.");
            System.out.println(" *");
            System.out.println(" */");
            System.out.println();
            System.out.println("package fr.inria.tapenade.utils;");
            System.out.println();
            System.out.println("/**");
            System.out.println(" * [GENERATED FILE] IL operator constants.");
            System.out.println(" * This file has been generated from il.metal through command:");
            System.out.println(" * ./gradlew illang");
            System.out.println(" */");
            System.out.println("public final class ILLang {");
            System.out.println();
            ops = Operator.allOperators;
            while (ops != null) {
                Operator op = (Operator) ops.head;
                System.out.println("    public static final int op_" + op.name + " = " + op.rank + ";");
                ops = ops.tail;
            }
            System.out.println("}");

        } else if ("tablesJava".equals(targetMode)) {

            System.out.println("/*");
            System.out.println(" * TAPENADE Automatic Differentiation Engine");
            System.out.println(" * Copyright (C) 1999-2021 Inria");
            System.out.println(" * See the LICENSE.md file in the project root for more information.");
            System.out.println(" *");
            System.out.println(" */");
            System.out.println();
            System.out.println("package fr.inria.tapenade.utils;");
            System.out.println();
            System.out.println("/**");
            System.out.println(" * [GENERATED FILE] IL Operator array.");
            System.out.println(" * This file has been generated from il.metal through command:");
            System.out.println(" * ./gradlew illang");
            System.out.println(" */");
            System.out.println("public final class ILLangOps {");
            System.out.println();
            ops = Operator.allOperators;
            System.out.println("    private static final Operator[] ops = {");
            System.out.println("            null,");
            ops = Operator.allOperators;
            while (ops != null) {
                Operator op = (Operator) ops.head;
                System.out.println("            " + op.creation() + ",");
                ops = ops.tail;
            }
            System.out.println("    };");
            System.out.println();
            System.out.println("    private static final Phylum[] phyla = {");
            System.out.println("            null,");
            pha = Phylum.allPhyla;
            while (pha != null) {
                Phylum ph = (Phylum) pha.head;
                System.out.println("            " + ph.creation() + ",");
                pha = pha.tail;
            }
            System.out.println("    };");
            System.out.println();
            System.out.println("    private ILLangOps() {");
            System.out.println("    }");
            System.out.println();
            System.out.println("    public static Operator ops(int rank) {");
            System.out.println("        return ILLangOps.ops[rank];");
            System.out.println("    }");
            System.out.println();
            System.out.println("    public static int opsLength() {");
            System.out.println("        return ILLangOps.ops.length;");
            System.out.println("    }");
            System.out.println();
            System.out.println("    public static Phylum phyla(int rank) {");
            System.out.println("        return ILLangOps.phyla[rank];");
            System.out.println("    }");
            System.out.println("}");

        } else if ("tablesC".equals(targetMode)) {

            System.out.println("//           GENERATED FILE: DO NOT EDIT !");
            System.out.println("//           =============================");
            System.out.println("// This file has been generated from formalism.java through command:");
            System.out.println("// $> ./gradlew illang");
            System.out.println();
            System.out.println("static Operator ilOperators[] = {");
            System.out.println("  {\"_null_tree\", 0, 0},");
            ops = Operator.allOperators;
            while (ops != null) {
                Operator op = (Operator) ops.head;
                int arityForHere = op.arity();
                if (arityForHere < -1) {
                    arityForHere = 0;
                }
                System.out.println("  {\"" + op.name + "\", " + arityForHere + ", " + op.rank + "},");
                ops = ops.tail;
            }
            System.out.println("(NULL)} ;");
            System.out.println();
            defineDirectCodeForTablesC("endOfList", "ilEndOfListCode");
            defineDirectCodeForTablesC("annotation", "ilAnnotationCode");
            defineDirectCodeForTablesC("none", "ilNoneCode");
            defineDirectCodeForTablesC("parsingError", "ilParsingErrorCode");
            defineDirectCodeForTablesC("switchCases", "ilSwitchCasesCode");
            defineDirectCodeForTablesC("switchCase", "ilSwitchCaseCode");
            defineDirectCodeForTablesC("constructor", "ilConstructorCode");
            defineDirectCodeForTablesC("modifiers", "ilModifiersCode");
            defineDirectCodeForTablesC("ident", "ilIdentCode");
            defineDirectCodeForTablesC("varDeclarations", "ilVarDeclarationsCode");
            defineDirectCodeForTablesC("varDeclaration", "ilVarDeclarationCode");
            defineDirectCodeForTablesC("constructorCall", "ilConstructorCallCode");
            defineDirectCodeForTablesC("blockStatement", "ilBlockStatementCode");
            defineDirectCodeForTablesC("assign", "ilAssignCode");
            defineDirectCodeForTablesC("methodCall", "ilMethodCallCode");
            defineDirectCodeForTablesC("call", "ilCallCode");
            defineDirectCodeForTablesC("expressions", "ilExpressionsCode");
            defineDirectCodeForTablesC("functionDeclarator", "ilFunctionDeclaratorCode");

        }
    }

    public static void defineDirectCodeForTablesC(String opName, String opNameForC) {
        MiniList ops = Operator.allOperators;
        Operator foundOp = null;
        while (ops != null && foundOp == null) {
            Operator op = (Operator) ops.head;
            if (opName.equals(op.name)) {
                foundOp = op;
            }
            ops = ops.tail;
        }
        if (foundOp != null) {
            System.out.println("static short " + opNameForC + " = " + foundOp.rank + " ;");
        }
    }

}
