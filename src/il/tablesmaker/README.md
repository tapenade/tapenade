Imperative Language definition: IL
==================================

Our build chain has a small compiler that compiles `il.metal` into
`tablesil.c` and into `ILLang.java`

`cd <TAPENADE>`

`./gradlew illang`


This directory defines a utility tool 
that builds the Java "tables" for our language IL
automatically from the central definition of IL.

The central definition of IL is basically the il.metal file in .. i.e.:

`<TAPENADE>/src/il/il.metal`

The metal compiler has taken care of creating the "formalism" IL into:
`<TAPENADE>/src/il/formalism.java`

The tool in this directory restarts from this "formalism.java" and creates either:  
* ILLang.java  
which is the description of IL used by the Java code of Tapenade.
It should be placed in 
`<TAPENADE>/build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java`

* tablesil.c  
which is the description of IL used by C code such as the x2il.ttml translators
(e.g. \<TAPENADE>/src/frontf/f2il.ttml or \<TAPENADE>/src/frontCPP/clang2il.ttml).
It should be placed in 
`<TAPENADE>/build/generated-src/c/main/front/tablesil.c`

This tool works by 
1) first copy and slightly simplify formalism.java into temporary Formalism.java,
which can be read as a standalone java class,
2) then compile this Formalism.java together with local "dummy" classes that
define the basic concepts (operator, phylum...)
and a main class that does the needed translation work,
3) then run the resulting java class to produce he required table file.
4) finally erase the temporary Formalism.java.

