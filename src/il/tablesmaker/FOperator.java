/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class FOperator extends Operator {

    public Phylum[] args = null;

    public FOperator(String name, Formalism ctf, Phylum[] argsPhyla, int argslength) {
        super(name);
        this.args = new Phylum[argslength];
        for (int i = 0; i < argslength; ++i) {
            this.args[i] = argsPhyla[i];
        }
    }

    public String creation() {
        String argPhyla = "";
        for (int i = args.length - 1; i >= 0; --i) {
            if (i == 0) {
                argPhyla = args[i].rank + argPhyla;
            } else {
                argPhyla = ", " + args[i].rank + argPhyla;
            }
        }
        if (args.length == 0) {
            argPhyla = "null";
        } else {
            argPhyla = "new int[]{" + argPhyla + "}";
        }
        return "new Operator(\"" + name + "\", " + rank + ", " + args.length + ", " + argPhyla + ")";
    }

    public int arity() {
        return args.length;
    }
}
