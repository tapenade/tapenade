#!/usr/bin/env bash

mkdir -p ../../../build/generated-src/java/main/tablesmaker
sed -f extractMiddle ../formalism.java >../../../build/generated-src/java/main/tablesmaker/Formalism.java-new
/bin/rm ../formalism.java

if [ -f ../../../build/generated-src/java/main/tablesmaker/Formalism.java-old ]; then
  DIFF=$(diff ../../../build/generated-src/java/main/tablesmaker/Formalism.java-new ../../../build/generated-src/java/main/tablesmaker/Formalism.java-old)
else
  DIFF="tobuild"
fi

if [ "$DIFF" != "" ]; then
  mv ../../../build/generated-src/java/main/tablesmaker/Formalism.java-new ../../../build/generated-src/java/main/tablesmaker/Formalism.java
fi
