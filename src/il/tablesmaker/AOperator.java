/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class AOperator extends Operator {
    int type = -1;

    public AOperator(String name, Formalism ctf, int type) {
        super(name);
        this.type = type;
    }

    public String creation() {
        return "new Operator(\"" + name + "\", " + rank + ", " + (type == Arity.INT ? "-2" : "-3") + ", null)";
    }

    public int arity() {
        return (type == Arity.INT ? -2 : -3);
    }

}
