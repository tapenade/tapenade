/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class Arity {

    public static int STRING = 1;
    public static int INT = 2;

}
