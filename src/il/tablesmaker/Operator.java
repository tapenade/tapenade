/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public abstract class Operator {
    /**
     * The (ordered) list of all operators created for this formalism
     */
    public static MiniList allOperators = null;
    public String name = null;
    public int rank = -1;

    public Operator(String name) {
        this.name = name;
        allOperators = new MiniList(this, allOperators);
    }

    public String creation() {
        return "<Forbidden unspecified Operator:" + name + " rank:" + rank + ">";
    }

    public abstract int arity();
}
