#!/usr/bin/env bash

mkdir -p ../../../build/generated-src/il/main/fr/inria/tapenade/utils
mkdir -p ../../../build/generated-src/java/main/fr/inria/tapenade/utils

mkdir -p ../../../build/generated-src/c/main/front

if [ -f ../../../build/generated-src/java/main/tablesmaker/Formalism.java-old ]; then
  DIFF_FORMALISM=$(diff ../../../build/generated-src/java/main/tablesmaker/Formalism.java-old ../../../build/generated-src/java/main/tablesmaker/Formalism.java)
else
  DIFF_FORMALISM="tobuild"
fi

if [ "$DIFF_FORMALISM" != "" ]; then
  java -classpath ../../../build/classes/java/main/ tablesmaker.TablesMaker tablesJavaConstants >../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java-new
  java -classpath ../../../build/classes/java/main/ tablesmaker.TablesMaker tablesJava >../../../build/generated-src/java/main/fr/inria/tapenade/utils/ILLangOps.java-new
  java -classpath ../../../build/classes/java/main/ tablesmaker.TablesMaker tablesC >../../../build/generated-src/c/main/front/tablesil.c-new
  if [ -f ../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java ]; then
    DIFF_LANG=$(diff ../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java-new ../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java)
  else
    DIFF_LANG="tobuild"
  fi
  if [ "DIFF_LANG" != "" ]; then
    mv ../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java-new ../../../build/generated-src/il/main/fr/inria/tapenade/utils/ILLang.java
    mv ../../../build/generated-src/java/main/fr/inria/tapenade/utils/ILLangOps.java-new ../../../build/generated-src/java/main/fr/inria/tapenade/utils/ILLangOps.java
    mv ../../../build/generated-src/c/main/front/tablesil.c-new ../../../build/generated-src/c/main/front/tablesil.c
  fi
fi

cp ../../../build/generated-src/java/main/tablesmaker/Formalism.java ../../../build/generated-src/java/main/tablesmaker/Formalism.java-old
