/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class LOperator extends Operator {

    public Phylum arg = null;

    public LOperator(String name, Formalism ctf, Phylum phylum, StarArity ar) {
        super(name);
        arg = phylum;
    }

    public String creation() {
        return "new Operator(\"" + name + "\", " + rank + ", -1, null)";
    }

    public int arity() {
        return -1;
    }

}
