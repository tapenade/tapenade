/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class MiniList {

    public Object head = null;
    public MiniList tail = null;

    public MiniList(Object head, MiniList tail) {
        this.head = head;
        this.tail = tail;
    }

    public static MiniList nreverse(MiniList list) {
        MiniList tmp;
        MiniList result = null;
        while (list != null) {
            tmp = list.tail;
            list.tail = result;
            result = list;
            list = tmp;
        }
        return result;
    }

}
