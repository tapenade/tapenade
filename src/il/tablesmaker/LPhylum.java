/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package tablesmaker;

public class LPhylum extends Phylum {

    public LPhylum(String name, Formalism ctf) {
        super(name, ctf);
    }

    public String creation() {
        String phylOps = null;
        MiniList inOps = operators;
        while (inOps != null) {
            if (phylOps == null) {
                phylOps = ((Operator) inOps.head).rank + "";
            } else {
                phylOps = ((Operator) inOps.head).rank + ", " + phylOps;
            }
            inOps = inOps.tail;
        }
        if (phylOps == null) {
            phylOps = "";
        }
        return "new Phylum(\"" + name + "\", " + rank + ", new int[]{" + phylOps + "})";
    }

}
