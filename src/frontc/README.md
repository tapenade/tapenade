C parser
========

C parser is defined in `src/frontc` using ANTLR.

To build a C parser from tapenade directory:

`./gradlew frontc`

It creates a Java archive in `build/libs/frontc.jar`.

`java -classpath $TAPENADE_HOME/build/libs/frontc.jar fr/inria/frontc/C2IL <file.c>`
translates `<file.c>` into an IL abstract tree.
