package fr.inria.frontc;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public class PreprocessorInfoChannel {
    private final Hashtable lineLists = new Hashtable(); // indexed by Token number
    private int firstValidTokenNumber;
    private int maxTokenNumber;

    public void addLineForTokenNumber(Object line, Integer toknum) {
        if (lineLists.containsKey(toknum)) {
            Vector lines = (Vector) lineLists.get(toknum);
            lines.addElement(line);
        } else {
            Vector lines = new Vector();
            lines.addElement(line);
            lineLists.put(toknum, lines);
            if (maxTokenNumber < toknum) {
                maxTokenNumber = toknum;
            }
        }
    }

    public int getMaxTokenNumber() {
        return maxTokenNumber;
    }

    public Vector extractLinesPrecedingTokenNumber(Integer toknum) {
        Vector lines = new Vector();
        if (toknum == null) {
            return lines;
        }
        for (int i = firstValidTokenNumber; i < toknum; i++) {
            Integer inti = i;
            if (lineLists.containsKey(inti)) {
                Vector tokenLineVector = (Vector) lineLists.get(inti);
                if (tokenLineVector != null) {
                    Enumeration tokenLines = tokenLineVector.elements();
                    while (tokenLines.hasMoreElements()) {
                        lines.addElement(tokenLines.nextElement());
                    }
                    lineLists.remove(inti);
                }
            }
        }
        firstValidTokenNumber = toknum;
        return lines;
    }

    /* [ first, last [ */
    public Vector extractLinesBetweenTokenNumbers(Integer first, Integer last) {
        Vector lines = new Vector();
        if (first == null || last == null) {
            return lines;
        }
        for (int i = first; i < last; i++) {
            Integer inti = i;
            if (lineLists.containsKey(inti)) {
                Vector tokenLineVector = (Vector) lineLists.get(inti);
                if (tokenLineVector != null) {
                    Enumeration tokenLines = tokenLineVector.elements();
                    while (tokenLines.hasMoreElements()) {
                        lines.addElement(tokenLines.nextElement());
                    }
                    lineLists.remove(inti);
                }
            }
        }
        return lines;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("PreprocessorInfoChannel:\n");
        for (int i = 0; i <= maxTokenNumber + 1; i++) {
            Integer inti = i;
            if (lineLists.containsKey(inti)) {
                Vector tokenLineVector = (Vector) lineLists.get(inti);
                if (tokenLineVector != null) {
                    Enumeration tokenLines = tokenLineVector.elements();
                    while (tokenLines.hasMoreElements()) {
                        sb.append(inti).append(":").append(tokenLines.nextElement()).append('\n');
                    }
                }
            }
        }
        return sb.toString();
    }
}



