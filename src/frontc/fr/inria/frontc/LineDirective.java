package fr.inria.frontc;

class LineDirective {
    String source = "";
    int line = 1;
    private LineDirective parent;
    private boolean enteringFile;
    private boolean returningToFile;
    private boolean systemHeader;
    private boolean treatAsC;

    public LineDirective() {
    }

    public LineDirective(LineDirective lobj) {
        super();
        parent = lobj.getParent();
        source = lobj.getSource();
        line = lobj.getLine();
        enteringFile = lobj.getEnteringFile();
        returningToFile = lobj.getReturningToFile();
        systemHeader = lobj.getSystemHeader();
        treatAsC = lobj.getTreatAsC();
    }

    public LineDirective(String src) {
        super();
        source = src;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String src) {
        source = src;
    }

    private LineDirective getParent() {
        return parent;
    }

    public void setParent(LineDirective par) {
        parent = par;
    }

    private int getLine() {
        return line;
    }

    public void setLine(int l) {
        line = l;
    }

    public void newline() {
        line++;
    }

    public boolean getEnteringFile() {
        return enteringFile;
    }

    public void setEnteringFile(boolean v) {
        enteringFile = v;
    }

    public boolean getReturningToFile() {
        return returningToFile;
    }

    public void setReturningToFile(boolean v) {
        returningToFile = v;
    }

    public boolean getSystemHeader() {
        return systemHeader;
    }

    public void setSystemHeader(boolean v) {
        systemHeader = v;
    }

    private boolean getTreatAsC() {
        return treatAsC;
    }

    public void setTreatAsC(boolean v) {
        treatAsC = v;
    }

    @Override
    public String toString() {
        StringBuffer ret;
        ret = new StringBuffer("# " + line + " \"" + source + "\"");
        if (enteringFile) {
            ret.append(" 1");
        }
        if (returningToFile) {
            ret.append(" 2");
        }
        if (systemHeader) {
            ret.append(" 3");
        }
        if (treatAsC) {
            ret.append(" 4");
        }
        return ret.toString();
    }
}

