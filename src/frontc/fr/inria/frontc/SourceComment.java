package fr.inria.frontc;

import java.util.Vector;

public class SourceComment {
    final Vector<String> lines;

    public SourceComment() {
        lines = new Vector<>();
    }

    public SourceComment(String s) {
        lines = new Vector<>();
        lines.addElement(s);
    }

    public SourceComment(SourceComment sc) {
        super();
        lines = new Vector<>(sc.lines);
    }

    public void addLine(String s) {
        lines.addElement(s);
    }

    @Override
    public String toString() {
        return lines.toString();
    }
}
