package fr.inria.frontc;

import antlr.CommonToken;

public class CToken extends CommonToken {
    public static boolean debugMode = false ;
    private String source = "";
    private int tokenNumber;

    public String getSource() {
        return source;
    }

    public void setSource(String src) {
        source = src;
    }

    public int getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(int i) {
        tokenNumber = i;
    }

    public void setText(String txt) {
        super.setText(txt) ;
        if (debugMode) {
            System.out.println("   set text @"+Integer.toHexString(hashCode())+" := "+txt) ;
        }
    }

    @Override
    public String toString() {
        return "CToken:" + "(" + hashCode() + ")" + "[" + getType() + "] " +
                getText() + " line:" + getLine() + " source:" + source;
    }
}
