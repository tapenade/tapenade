/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.frontc;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import fr.inria.frontc.STDCTokenTypes;
import fr.inria.frontc.StdCLexer;
import fr.inria.frontc.StdCParser;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

public class C2IL {
    private final boolean txtIL;
    public boolean debugMode;
    private final String input_name;
    private final String output_name;
    private boolean originalSourceOnly;
    private DataInputStream input;
    private PrintStream output;

    public C2IL() {
        super();
        txtIL = false;
        input_name = "-";
        output_name = "-";
    }

    public C2IL(String file_name) {
        super();
        txtIL = false;
        input_name = file_name;
        output_name = "-";
    }

    public C2IL(String in, String out) {
        super();
        txtIL = false;
        input_name = in;
        output_name = out;
    }

    public C2IL(String file_name, boolean b) {
        super();
        txtIL = b;
        input_name = file_name;
        output_name = "-";
    }

    private C2IL(String in, String out, boolean b) {
        super();
        txtIL = b;
        input_name = in;
        output_name = out;
    }

    /**
     * @param args options file names
     */
    public static void main(String[] args) {
        String inName = "";
        String outName = "";
        boolean textMode = false;
        boolean originalSourceOnly = false;
        boolean debugMode = false ;
        for (int c=0 ; c<args.length ; ++c) {
            String arg = args[c];
            switch (arg) {
                case "-text":
                    textMode = true;
                    break;
                case "-s":
                    originalSourceOnly = true;
                    break;
                case "-debug":
                    debugMode = true;
                    break;
                case "-o":
                    ++c;
                    outName = args[c];
                    break;
                default:
                    inName = args[c];
            }
        }
        try {
            if (debugMode) CToken.debugMode = true ;
            C2IL converter = new C2IL(inName, outName, textMode);
            converter.debugMode = debugMode ;
            converter.setConvertOriginalSourceOnly(originalSourceOnly);
            converter.convert();
        } catch (Exception e) {
            System.err.println("exception: " + e);
            //e.printStackTrace();
        }
    }

    private void initialize_input(String file_name) throws FileNotFoundException {
        if (file_name.equals("-") || file_name.isEmpty()) {
            input = new DataInputStream(System.in);
        } else {
            input = new DataInputStream(new FileInputStream(file_name));
        }
    }

    private void initialize_output(String fName) throws FileNotFoundException {
        if (fName.equals("-") || fName.isEmpty()) {
            output = new PrintStream(System.out);
        } else {
            output = new PrintStream(fName);
        }
    }

    private void close_files() throws IOException {
        output.close();
        input.close();
    }

    private void setConvertOriginalSourceOnly(boolean b) {
        originalSourceOnly = b;
    }

    private void convert()
            throws IOException {
        initialize_input(input_name);
        initialize_output(output_name);
        StdCLexer lexer = new StdCLexer(input);
        lexer.setTokenObjectClass(CToken.class.getName());
        lexer.initGnuLiterals();
        lexer.setOriginalSource(input_name);
        lexer.setDebugMode(debugMode) ;
        StdCParser parser = new StdCParser(lexer);
        int indexCpp = input_name.indexOf("_preprocessed.c");
        String origInputFileName = input_name;
        if (indexCpp > 0) {
            origInputFileName = input_name.substring(0, input_name.indexOf("_preprocessed.c"));
        }
        parser.inputFileName = origInputFileName;
        parser.setASTNodeClass(TNode.class.getName());
        TNode.setTokenVocabulary(STDCTokenTypes.class.getName());
        StdC2IL c2il = new StdC2IL(origInputFileName, lexer.getPreprocessorInfoChannel(), output);
        c2il.setASTNodeClass(TNode.class.getName());
        c2il.setTextIL(txtIL);
        c2il.setConvertOriginalSourceOnly(originalSourceOnly);
        try {
            parser.translationUnit();
            if (debugMode) {
                System.out.print("----C-------C-------C-------C----") ;
                TNode.printTree((TNode)parser.getAST()) ;
                System.out.println() ;
            }
            c2il.translationUnit(parser.getAST());
        } catch (RecognitionException | TokenStreamException e) {
            System.err.println("Fatal IO error:\n" + e);
            return;
        }
        close_files();
    }
}
