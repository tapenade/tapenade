/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        Copyright (c) Non, Inc. 1997 -- All Rights Reserved

PROJECT:        C Compiler
MODULE:         Parser
FILE:           stdc.g

AUTHOR:         John D. Mitchell (john@non.net), Jul 12, 1997

REVISION HISTORY:

        Name    Date            Description
        ----    ----            -----------
        JDM     97.07.12        Initial version.
        JTC     97.11.18        Declaration vs declarator & misc. hacking.
        JDM     97.11.20        Fixed:  declaration vs funcDef,
                                        parenthesized expressions,
                                        declarator iteration,
                                        varargs recognition,
                                        empty source file recognition,
                                        and some typos.
        NC		2006.05.xx		Tree

DESCRIPTION:

        This grammar supports the Standard C language.

        Note clearly that this grammar does *NOT* deal with
        preprocessor functionality (including things like trigraphs)
        Nor does this grammar deal with multi-byte characters nor strings
        containing multi-byte characters [these constructs are "exercises
        for the reader" as it were :-)].

        Please refer to the ISO/ANSI C Language Standard if you believe
        this grammar to be in error.  Please cite chapter and verse in any
        correspondence to the author to back up your claim.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

header {
package fr.inria.frontc;
}
{
import java.io.*;
}

             
class StdCParser extends Parser;

options
        {
        k = 2;
        exportVocab = STDC;
        buildAST = true;
        ASTLabelType = "TNode";

        // Copied following options from java grammar.
        codeGenMakeSwitchThreshold = 2;
        codeGenBitsetTestThreshold = 3;
        }


{
    // 
    public String inputFileName = "-";
    
    // Suppport C++-style single-line comments?
    public static boolean CPPComments = true;

    // access to symbol table
    public CSymbolTable symbolTable = new CSymbolTable();

    // source for names to unnamed scopes
    protected int unnamedScopeCounter = 0;

	// ast represents all the keywords already encountered in the
	// declaration. modifier is the AST of a single keyword to be added
	// to the new AST. If the tree ast is already a NModifiedTypeSpec, then
	// the new modifier simply needs to be added (last) to the modifiers.
	// Otherwise build a new NModifiedTypeSpec around the ast tree.
	protected TNode addTypeSpecModifier(TNode ast, TNode modifier) {
		TNode newAST = ast;
		if (ast != null && ast.getType() == NModifiedTypeSpec) {
        	TNode mods = (TNode)(ast.getFirstChild());
			if (mods != null && mods.getType() == NModifiers) {
				if (mods.getLastChild() != null)
					mods.getLastChild().addSibling(modifier);
			}
			return ast;
		}
		else {
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NModifiers));
			tempList.add(modifier);
			TNode mods = (TNode)astFactory.make(tempList);
			tempList = new ASTArray(3);
			tempList.add(astFactory.create(NModifiedTypeSpec));
			tempList.add(mods);
			tempList.add(ast);
			newAST = (TNode)astFactory.make(tempList);
			return newAST;
		}
	}
	
	protected TNode addTypeNameModifier(TNode ast, TNode modifier) {
		if (ast != null && ast.getType() == NModifiedType) {
        	TNode mods = (TNode)(ast.getFirstChild());
			if (mods != null && mods.getType() == NModifiers) {
				if (mods.getLastChild() != null)
					mods.getLastChild().addSibling(modifier);
			}
			return ast;
		}
		else if (ast != null && ast.getType() == NModifiedTypeSpec) {
			TNode modifiers = (TNode)(ast.getFirstChild());
			TNode modifiedThing = (TNode)(modifiers.getNextSibling());
			if (modifiedThing != null) {
				modifiedThing.removeSelf();
			}
			modifiers.addSibling(addTypeNameModifier(modifiedThing, modifier));
			return ast;
		}
		else {
			TNode newAST;
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NModifiers));
			tempList.add(modifier);
			TNode mods = (TNode)astFactory.make(tempList);
			if (ast == null) {
				tempList = new ASTArray(2);
				tempList.add(astFactory.create(NModifiedType));
				tempList.add(mods);
			}
			else {
				tempList = new ASTArray(3);
				tempList.add(astFactory.create(NModifiedType));
				tempList.add(mods);
				tempList.add(ast);
			}
			newAST = (TNode)astFactory.make(tempList);
			return newAST;			
		}
	}
	
	protected TNode setBaseType(TNode ast, TNode type) {
		if (ast != null && ast.getType() == NModifiedType) {
        	TNode modifiers = (TNode)(ast.getFirstChild());
        	TNode modifiedThing = (TNode)(modifiers.getNextSibling());
        	if (modifiedThing == null) {
        		modifiers.addSibling(type);
        	}
			return ast;
		}
		else if (ast != null && ast.getType() == NModifiedTypeSpec) {
			TNode modifiers = (TNode)(ast.getFirstChild());
			TNode modifiedThing = (TNode)(modifiers.getNextSibling());
			if (modifiedThing == null) {
				modifiers.addSibling(type);
			}
			else {
				setBaseType(modifiedThing, type);
			}
			return ast;
		}
		else if (ast == null)
			return type;
		return ast;
	}
	
	protected TNode makeTypeFromPointerGroup(TNode pg, TNode in) {
		if (pg == null) {
                  return (TNode)astFactory.dupTree(in);
		}
		if (pg.getType() == STAR) {
			ASTArray tempList;
			if (in != null) {
				tempList = new ASTArray(2);
				tempList.add(astFactory.create(NPointerType));
				tempList.add( in );
			}
			else {
				tempList = new ASTArray(1);
				tempList.add(astFactory.create(NPointerType));
			}
			TNode new_in = (TNode)astFactory.make(tempList);
			return makeTypeFromPointerGroup((TNode)pg.getNextSibling(), new_in);
		}
		else if (in != null && in.getType() == NModifiedTypeSpec) {
                        TNode next = (TNode)pg.getNextSibling();
                        pg.removeSelf();
			TNode mods = (TNode)(in.getFirstChild());
			if (mods!=null && mods.getType()==NModifiers) {
                          mods.addChild(pg) ;
			}
			return makeTypeFromPointerGroup(next, in);
		}
		else {
                        TNode first = (TNode)pg.getFirstChild() ;
			TNode next = (TNode)pg.getNextSibling();
			pg.removeSelf();
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NModifiers));
			tempList.add( pg );
			TNode mods = (TNode)astFactory.make(tempList);
			tempList = new ASTArray(3);
			tempList.add(astFactory.create(NModifiedTypeSpec));
			tempList.add(mods);
			tempList.add(in);
			TNode new_in = (TNode)astFactory.make(tempList);
			return makeTypeFromPointerGroup(next, new_in);
		}
	}
	
	protected TNode makeDeclaratorFromPointerGroup(TNode pg, TNode in) {
		if (pg == null)
			return in;
		if (pg.getType() == STAR) {
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NPointerDeclarator));
			tempList.add(makeDeclaratorFromPointerGroup((TNode)pg.getNextSibling(), in));
			return (TNode)astFactory.make(tempList);
		}
		else {
			TNode ast = makeDeclaratorFromPointerGroup((TNode)pg.getNextSibling(), in);
			if (ast != null && ast.getType() == NModifiedDeclarator) {
				// Augment the modifiers list with pg
				TNode mods = (TNode)(ast.getFirstChild());
				if (mods != null && mods.getType() == NModifiers) {
					if (mods.getLastChild() != null)
						mods.getLastChild().addSibling( astFactory.dupTree(pg) );
				}
				return ast;
			}
			else {
				ASTArray tempList = new ASTArray(2);
				tempList.add(astFactory.create(NModifiers));
				tempList.add( astFactory.dupTree(pg) );
				TNode mods = (TNode)astFactory.make(tempList);
				tempList = new ASTArray(3);
				tempList.add(astFactory.create(NModifiedDeclarator));
				tempList.add(mods);
				tempList.add(ast);
				return (TNode)astFactory.make(tempList);				
			}
		}
	}
	
	protected TNode getDeclaratorIdent(TNode decl) {
		if (decl.getType() == NPointerDeclarator) {
			return getDeclaratorIdent((TNode)decl.getFirstChild());
		}
		else if (decl.getType() == NArrayDeclarator) {
			return getDeclaratorIdent((TNode)decl.getFirstChild());
		}
		else if (decl.getType() == NModifiedDeclarator) {
			TNode mods = (TNode)decl.getFirstChild();
			return getDeclaratorIdent((TNode)mods.getNextSibling());
		}
		else if (decl.getType() == NFunctionDeclarator) {
			return getDeclaratorIdent((TNode)decl.getFirstChild());
		}
		else
			return decl;
	}
	
	protected TNode makeTypeFromDeclarator(TNode decl, TNode type) {
		TNode new_decl, new_type;
		if (decl == null)
			return type;
		if (decl.getType() == NDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			return makeTypeFromDeclarator(new_decl, type);
		}
		else if (decl.getType() == NPointerDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			// build #( #[NPointerType], type)
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NPointerType));
			tempList.add(astFactory.dupTree(type));
			new_type = (TNode)astFactory.make(tempList);
			return makeTypeFromDeclarator(new_decl, new_type);
		}
		else if (decl.getType() == NArrayDeclarator) {
			// decl may be in "degenerated" form where there is only one son
			// as the dimension.
			new_decl = (TNode)decl.getFirstChild();
			TNode dims;
			if (new_decl != null && new_decl.getType() == NDimColons) {
				dims = new_decl;
				new_decl = null;
			} else {
				dims = (TNode)new_decl.getNextSibling();
			}
			// build #( #[NArrayType], type, dims);
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NArrayType));
			tempList.add(astFactory.dupTree(type));
			tempList.add(astFactory.dupTree(dims));
			new_type = (TNode)astFactory.make(tempList);
			return makeTypeFromDeclarator(new_decl, new_type);
		}
		else if (decl.getType() == NModifiedDeclarator) {
			TNode mods = (TNode)decl.getFirstChild();
			new_decl = (TNode)mods.getNextSibling();
			// #( #[NModifiedTypeSpec], mods, type)
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NModifiedTypeSpec));
			tempList.add(astFactory.dupTree(mods));
			tempList.add(astFactory.dupTree(type));
			new_type = (TNode)astFactory.make(tempList);
			return makeTypeFromDeclarator(new_decl, new_type);
		}
		else if (decl.getType() == NFunctionDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			TNode args;
			if (new_decl != null
				&& (new_decl.getType() == NParameterTypeList
					|| new_decl.getType() == NUnspecifiedParameterList)) {
				args = new_decl;
				new_decl = null;
			}
			else
				args = (TNode)new_decl.getNextSibling();
			// build #( #[], type, args)
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NFunctionType));
			tempList.add(astFactory.dupTree(type));
			tempList.add(astFactory.dupTree(args));
			new_type = (TNode)astFactory.make(tempList);
			return makeTypeFromDeclarator(new_decl, new_type);
		}
		else
			return type;
	}
	
	protected TNode getFunctionReturnType(String fname, TNode decl, TNode type) {
		TNode new_decl, new_type;
		if (decl == null)
			return type;
		if (decl.getType() == NDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			return getFunctionReturnType(fname, new_decl, type);
		}
		else if (decl.getType() == NPointerDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			// build #( #[NPointerType], type)
			ASTArray tempList = new ASTArray(2);
			tempList.add(astFactory.create(NPointerType));
			tempList.add(astFactory.dupTree(type));
			new_type = (TNode)astFactory.make(tempList);
			return getFunctionReturnType(fname, new_decl, new_type);
		}
		else if (decl.getType() == NArrayDeclarator) {
			// decl may be in "degenerated" form where there is only one son
			// as the dimension.
			new_decl = (TNode)decl.getFirstChild();
			TNode dims;
			if (new_decl != null && new_decl.getType() == NDimColons) {
				dims = new_decl;
				new_decl = null;
			} else {
				dims = (TNode)new_decl.getNextSibling();
			}
			// build #( #[NArrayType], type, dims);
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NArrayType));
			tempList.add(astFactory.dupTree(type));
			tempList.add(astFactory.dupTree(dims));
			new_type = (TNode)astFactory.make(tempList);
			return getFunctionReturnType(fname, new_decl, new_type);
		}
		else if (decl.getType() == NModifiedDeclarator) {
			TNode mods = (TNode)decl.getFirstChild();
			new_decl = (TNode)mods.getNextSibling();
			// #( #[NModifiedTypeSpec], mods, type)
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NModifiedTypeSpec));
			tempList.add(astFactory.dupTree(mods));
			tempList.add(astFactory.dupTree(type));
			new_type = (TNode)astFactory.make(tempList);
			return getFunctionReturnType(fname, new_decl, new_type);
		}
		else if (decl.getType() == NFunctionDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			if (new_decl.getType() == ID && new_decl.getText().equals(fname)) {
				return type;
			}
			TNode args;
			if (new_decl != null
				&& (new_decl.getType() == NParameterTypeList
					|| new_decl.getType() == NUnspecifiedParameterList)) {
				args = new_decl;
				new_decl = null;
			}
			else
				args = (TNode)new_decl.getNextSibling();
			// build #( #[], type, args)
			ASTArray tempList = new ASTArray(3);
			tempList.add(astFactory.create(NFunctionType));
			tempList.add(astFactory.dupTree(type));
			tempList.add(astFactory.dupTree(args));
			new_type = (TNode)astFactory.make(tempList);
			return getFunctionReturnType(fname, new_decl, new_type);
		}
		else
			return type;
	}

	protected TNode getFunctionParameters(String fname, TNode decl) {
		TNode new_decl;
		if (decl == null)
			return null;
		if (decl.getType() == NDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			return getFunctionParameters(fname, new_decl);
		}
		else if (decl.getType() == NPointerDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			return getFunctionParameters(fname, new_decl);
		}
		else if (decl.getType() == NArrayDeclarator) {
			// decl may be in "degenerated" form where there is only one son
			// as the dimension.
			new_decl = (TNode)decl.getFirstChild();
			if (new_decl != null && new_decl.getType() == NDimColons)
				new_decl = null;
			return getFunctionParameters(fname, new_decl);
		}
		else if (decl.getType() == NModifiedDeclarator) {
			TNode mods = (TNode)decl.getFirstChild();
			new_decl = (TNode)mods.getNextSibling();
			return getFunctionParameters(fname, new_decl);
		}
		else if (decl.getType() == NFunctionDeclarator) {
			new_decl = (TNode)decl.getFirstChild();
			if (new_decl.getType() == ID && new_decl.getText().equals(fname)) {
				return (TNode)new_decl.getNextSibling();
			}
			TNode args;
			if (new_decl != null
				&& (new_decl.getType() == NParameterTypeList
					|| new_decl.getType() == NUnspecifiedParameterList))
				new_decl = null;
			return getFunctionParameters(fname, new_decl);
		}
		else
			return null;
	}
	
    public boolean isTypedefName(String name) {
      boolean returnValue = false;
      TNode node = symbolTable.lookupNameInCurrentScope(name);
      for (; node != null; node = (TNode) node.getNextSibling() ) {
        if(node.getType() == LITERAL_typedef) {
            returnValue = true;
            break;
        }
      }
      return returnValue;
    }


    public String getAScopeName() {
      return "" + (unnamedScopeCounter++);
    }

    public void pushScope(String scopeName) {
      symbolTable.pushScope(scopeName);
    }

    public void popScope() {
      symbolTable.popScope();
    }

    int traceDepth = 0;
    public void reportError(RecognitionException ex) {
      try {
            System.err.println("C Parsing error: " +ex + " token name:" + tokenNames[LA(1)]);
          }
	  catch (TokenStreamException e) {
            System.err.println("C Parsing error: " +ex);
          }
    }
    public void reportError(String s) {
        System.err.println("C Parsing error from String: " + s);
    }
    public void reportWarning(String s) {
        System.err.println("C Parsing warning from String: " + s);
    }

    public void match(int t) throws MismatchedTokenException {
		boolean debugging = false;
          
        if ( debugging ) {
        	for (int x=0; x<traceDepth; x++) System.out.print(' ');
				try {
           			System.out.println("Match("+tokenNames[t]+") with LA(1)="+
                						tokenNames[LA(1)] +
                						((inputState.guessing>0)?" [inputState.guessing "+ inputState.guessing + "]":""));
           		}
           		catch (TokenStreamException e) {
            		System.out.println("Match("+tokenNames[t]+") " + ((inputState.guessing>0)?" [inputState.guessing "+ inputState.guessing + "]":""));
           		}
		}
		try {
            if ( LA(1) != t ) {
                if ( debugging ) {
                    for (int x=0; x<traceDepth; x++) System.out.print(' ');
                    System.out.println("token mismatch: "+tokenNames[LA(1)]
                                + "!="+tokenNames[t]);
                }
	        	throw new MismatchedTokenException(tokenNames, LT(1), t, false,
	        										getFilename());
            } else {
                // mark token as consumed -- fetch next token deferred until LA/LT
                consume();
            }
		}
		catch (TokenStreamException e) {
		}
	}
        
    public void traceIn(String rname) {
      traceDepth += 1;
      for (int x=0; x<traceDepth; x++) System.out.print(' ');
      try {
        System.out.println("> "+rname+"; LA(1)==("+ tokenNames[LT(1).getType()] 
            + ") " + LT(1).getText() + " [inputState.guessing "+ inputState.guessing + "]");
      }
      catch (TokenStreamException e) {
      }
    }
    public void traceOut(String rname) {
      for (int x=0; x<traceDepth; x++) System.out.print(' ');
      try {
        System.out.println("< "+rname+"; LA(1)==("+ tokenNames[LT(1).getType()] 
            + ") "+LT(1).getText() + " [inputState.guessing "+ inputState.guessing + "]");
      }
      catch (TokenStreamException e) {
      }
      traceDepth -= 1;
    }

	public static void main(String[] args) {
		boolean parseOnce = true;
		for (int i = 0; i < args.length; i++) {
        	try {
            		String programName = args[i];
            		DataInputStream dis = null;
            		if (programName.equals("-")) {
                		dis = new DataInputStream( System.in );
            		}
            		else {
                		dis = new DataInputStream(new FileInputStream(programName));
            		}
            		StdCLexer lexer = new StdCLexer ( dis );
            		lexer.setTokenObjectClass(CToken.class.getName());
            		lexer.initGnuLiterals();
            		lexer.setOriginalSource(programName);
            		StdCParser parser = new StdCParser ( lexer );
            		parser.inputFileName = programName;

            		// set AST node type to TNode or get nasty cast class errors
            		parser.setASTNodeClass(TNode.class.getName());
            		TNode.setTokenVocabulary(STDCTokenTypes.class.getName());

            		// invoke parser
            		try {
	                		parser.translationUnit();
	                		System.out.println(lexer.getPreprocessorInfoChannel());
	                		TNode.printTree(parser.getAST());
            				System.out.flush();
            		}
            		catch (RecognitionException e) {
                		System.err.println("Fatal IO error:\n"+e);
                		System.exit(1);
            		}
            		catch (TokenStreamException e) {
            			if (! e.getMessage().equals("EOF")) {
                			System.err.println("Fatal IO error:\n"+e.toString());
                			System.exit(1);
            			}
            		}
        	}
        	catch  ( Exception e ) {
        		System.err.println ( "exception: " + e);
        	}
		}
	}
	
}



translationUnit
			/* Allow empty source file, gcc does it, but the standard
			 * forbids it */
        :       ( externalList )?
        ;

/* Another start rule, to be used to parse the file one declaration/definition
 * at a time
 */

definitionOrDeclaration
		:	externalDef
		;

externalList
        :       ( externalDef )+
        ;

asm_expr
        :       "asm"^ 
                ("volatile")? LCURLY expr RCURLY ( SEMI )+
        ;

externalDef
        :       SEMI
                    { ## = #( #[NEmptyStatement], ## ); }
        |       ( "typedef" | declaration )=> declaration
        |       ( attributeDecl )* functionDef
        |       asm_expr
        ;


declaration !
					{
						AST ds1 = null;
						boolean isTypedef = false;
					}
        :	   ( attributeDecl )*
               (td:"typedef" ! { isTypedef = true; } )?
               ds:declSpecifiers
                      { 
                      	ds1 = astFactory.dupList(#ds);
                      	/* When inside a type definition, the typedef literal
                      	   is part of ## but not of #ds. however it is needed
                      	   in order for the isTypedefName() function to work. */
                      	if (isTypedef)
							ds1 = #(null, astFactory.dupList(#td), ds1);
                      }
                (
                    il:initDeclList[ds1]
                )?
                sc:SEMI !
                	{
                		// no initDecl and a structOrUnion or Enum in ds means a type definition
                		if ( #il == null && (#ds.getType() == NStructType ||
                							 #ds.getType() == NUnionType ||
                							 #ds.getType() == NEnumType)) {
                			//TNode name = (TNode)(#ds.getFirstChild());
                			## = #( #[NTypeDeclaration],
                						#[NNone],
                						#ds);
                		}
                		else if (isTypedef) {
                			TNode decl = #il;
                			while (decl != null) {
                				/* decl is a NInitDecl tree, the declarator is the first
                			 	 * child, this declarator is also a NDeclarator node and
                				 * the actual declarator is its first child */
                				TNode declarator = (TNode)decl.getFirstChild().getFirstChild();
                				TNode id = getDeclaratorIdent(declarator);
                				TNode type = makeTypeFromDeclarator(declarator,
                									(TNode)astFactory.dupTree(#ds));
                				TNode tdef = #( #[NTypeDeclaration],
                								astFactory.dupTree(id), type);
                				## = #( null, ##, tdef);
                				decl = (TNode)decl.getNextSibling();
                			}
                		}
                		else
                            ## = #( #[NDeclaration], #ds, #il);
                        TNode semicolon = (TNode)#sc;
                        if (semicolon.getAttribute("tokenNumber") != null) {
                        	##.setAttribute("lastToken",
                        			semicolon.getAttribute("tokenNumber"));
                        }
                    }
        ;


oldDeclaration
                                        { AST ds1 = null; }
        :       ds:declSpecifiers       { ds1 = astFactory.dupList(#ds); }
                (                       
                    initDeclList[ds1]
                )?
                SEMI!
                                        { ## = #( #[NDeclaration], #ds); }
                
        ;


declSpecifiers
				{
					int specCount=0, n_stars;
					TNode ds=null;
				}
        :
            (     options { // this loop properly aborts when
                            //  it finds a non-typedefName ID MBZ
                            warnWhenFollowAmbig = false;
                          } :
	          ! s:storageClassSpecifier
			{ ## = addTypeSpecModifier(##, #s); }
                | ! q:typeQualifier
                	{ ## = addTypeSpecModifier(##, #q); }
                | ! specCount = m:typeModifier[specCount]
                	{ ## = addTypeNameModifier(##, #m); }
                | ! ( nonModifiedTypeSpecifier[specCount] )=>
                	specCount = t:nonModifiedTypeSpecifier[specCount]
                	{ ## = setBaseType(##, #t); }
                | ! n_stars = pg:qualifiedPointerGroup
                	{ ## = makeTypeFromPointerGroup(#pg, ##); }
            )+
        ;

storageClassSpecifier
        :     (	  "auto"                  
                | "register"
                | functionStorageClassSpecifier )
        ;


functionStorageClassSpecifier
        :  "extern"
        |! "static"
      	   {
       		if (symbolTable.nbScopes() > 0)
       			## = #[ NStaticLocal, "static" ];
       		else
       			## = #[ NStaticGlobal, "static" ];
           }
        |  "inline"
        |  "__inline__"
        |  "__noinline__"
	|  "__global__"
	|  "__device__"
	|  "__host__"
        |  "__constant__"
        |  "__shared__"
        |  "__managed__"
        ;


typeQualifier
        :		"const"
        |		"__const"
        |		"__const__"
        |		"volatile"
        |		"__volatile"
        |		"__volatile__"
        |		"restrict"
        |		"__restrict"
        |		"__restrict__"
        ;

nonModifiedTypeSpecifier[int specCount] returns [int retSpecCount]
                              { retSpecCount = specCount + 1; }
        :
        (       baseType ( attributeDecl )*
        |       structOrUnionSpecifier ( attributeDecl )*
        |       enumSpecifier
        |       { specCount == 0 }? typedefName
        )
        ;

typeModifier  [int specCount] returns [int retSpecCount]
                              { retSpecCount = specCount + 1; }
		: (
				"short"
			|	"long"
			|	"double"
			|	"signed"
			|	"__signed"
			|	"__signed__"
			|	"unsigned"
			|   "float"
		  )
		;
		
typeNameModifier
		:
		(
				"short"
			|	"long"
			|	"double"
			|	"signed"
			|	"unsigned"
			|   "float"
	    )
		;		
		
baseType
		:
		(
				"void"
        |       "char"
        |       "int"
        |		"_Complex"
        |       "float"
        |		"_Bool"
        |       "_Float128"
        |		gccBuiltinType
        )
		;

gccBuiltinType
		:	(
				"__builtin_va_list"
			)
		;

typedefName
        :       { isTypedefName ( LT(1).getText() ) }?
                i:ID                    { ## = #(#[NTypedefName], #i); }
        ;

structOrUnionSpecifier !
	                    { String scopeName; Object lasttk = null; }
        :       sou:structOrUnion
                ( ( ID LCURLY )=> i:ID l:LCURLY
                        {
                            scopeName = #sou.getText() + " " + #i.getText();
                            #l.setText(scopeName);
                            pushScope(scopeName);
                        }
				  dl1:structDeclarationList
                        { popScope(); }
                  r1:RCURLY
                  {
                  		## = #( null, #i, #dl1 );
                  		lasttk = ((TNode)#r1).getAttribute("tokenNumber");
                  }
                |   l1:LCURLY
                        {
		                    scopeName = getAScopeName();
		                    #l1.setText(scopeName);
		                    pushScope(scopeName);
	                    }
                    dl2:structDeclarationList
                        { popScope(); }
                    r2:RCURLY
                    {
                    	## = #( null, #[NNone], #dl2 );
                    	lasttk = ((TNode)#r2).getAttribute("tokenNumber");
                    }
                | id:ID
                	{
                		## = #id;
                		lasttk = ((TNode)#id).getAttribute("tokenNumber");
                	}
                )
                {
                	if (#sou.getType() == LITERAL_struct)
		                ## = #( #[NStructType], ## );
		            else
		            	## = #( #[NUnionType], ## );
		            ((TNode)##).setAttribute("lastToken", lasttk);
                }
        ;


structOrUnion
        :       "struct"
        |       "union"
        ;


structDeclarationList
        :       ( structDeclaration )+
        ;


structDeclaration
        :       specifierQualifierList structDeclaratorList ( sc:SEMI ! )+
        	{
        		## = #( #[NStructBodyElement], ##);
        		TNode semicolon = (TNode)#sc;
                if (semicolon.getAttribute("tokenNumber") != null) {
                	##.setAttribute("lastToken",
                			semicolon.getAttribute("tokenNumber"));
                }
        	}
        ;


specifierQualifierList !
                                { int specCount = 0; }
        :       (               options {   // this loop properly aborts when
                                            // it finds a non-typedefName ID MBZ
                                            warnWhenFollowAmbig = false;
                                        } :
                   /* ( typeSpecifier[specCount] )=>
                        specCount = typeSpecifier[specCount] */
                  specCount = m:typeModifier[specCount]
                	{ ## = addTypeNameModifier(##, #m); }
                | ( nonModifiedTypeSpecifier[specCount] )=>
                	specCount = t:nonModifiedTypeSpecifier[specCount]
                	{ ## = setBaseType(##, #t); }
                | q:typeQualifier
                	{ ## = addTypeSpecModifier(##, #q); }
                )+
        ;


structDeclaratorList
        :       (structDeclarator ( COMMA! structDeclarator )*)?
        ;


structDeclarator
				{ String dummy; }
        :
        (       COLON constExpr
        |       dummy = dc:declarator[false] ( COLON constExpr )?
        									 ( attributeDecl )*
        )
                                    { ## = #( #[NStructDeclarator], ##); }
        ;


enumSpecifier !
        :       "enum"
                ( ( ID LCURLY )=> i:ID LCURLY l:enumList[i.getText()] RCURLY
                	{ ## = #( #[NEnumType], #i, #l ); }
                | LCURLY el:enumList["anonymous"] RCURLY
                	{ ## = #( #[NEnumType], #[NNone], #el ); }
                | id:ID
                	{ ## = #( #[NEnumType], #id ); }
                )
        ;


enumList[String enumName]
        :       enumerator[enumName] ( COMMA! ( enumerator[enumName] )? )*
        ;

enumerator[String enumName]
        :       i:ID                { symbolTable.add(  i.getText(),
                                                        #(   null,
                                                            #[LITERAL_enum, "enum"],
                                                            #[ ID, enumName]
                                                         )
                                                     );
                                    }
                (ASSIGN constExpr)?
        ;


initDeclList[AST declarationSpecifiers]
        :       initDecl[declarationSpecifiers] 
                ( COMMA! initDecl[declarationSpecifiers] )*
        ;
 

initDecl[AST declarationSpecifiers]
                                        { String declName = ""; }
        :       declName = d:declarator[false]
                                        {   AST ds1, d1;
                                            ds1 = astFactory.dupList(declarationSpecifiers);
                                            d1 = astFactory.dupList(#d);
                                            symbolTable.add(declName, #(null, ds1, d1) );
                                        }
                ( attributeDecl )*
                ( ASSIGN initializer
                | COLON expr
                )?
                                        { ## = #( #[NInitDecl], ## ); }

        ;
        
attributeDecl !
        :       "__attribute" LPAREN LPAREN attributeList RPAREN RPAREN
                | "asm"^ LPAREN stringConst RPAREN
        		{ ## = null; }
        ;

attributeList
        :       attribute
        		( options{warnWhenFollowAmbig=false;}: COMMA attribute )*
        		( COMMA )?
        ;

attribute
        :       ( ~(LPAREN | RPAREN | COMMA)
                |  LPAREN attributeList RPAREN
                )*
        ;

qualifiedPointerGroup returns [int n_stars]
						{ n_stars = 0; }
		:
			( options { greedy=true; } : STAR
        			( options { greedy=true; } : typeQualifier )+
        			{ n_stars++; }
        		)+
		;

pointerGroup returns [int n_stars]
						{ n_stars = 0; }
        :       ( options { greedy=true; } : STAR
        			( options { greedy=true; } : typeQualifier )*
        			{ n_stars++; }
        		)+
        ;

idList
        :       ID ( COMMA! ID )*
        ;


initializer
        :       ( 	assignExpr
        				{ ## = #( #[NInitializer], ## ); }
                |	LCURLY! initializerList ( COMMA! )? RCURLY!
                		{ ## = #( #[NLcurlyInitializer], ## ); }
                )
        ;


initializerList
        :       initializer ( COMMA! initializer )*
        ;


declarator ! [boolean isFunctionDefinition] returns [String declName]
                {
                	declName = "";
			int n_stars = 0;
			TNode dims = null;
                        boolean isRefVar = false ;
				}
        :
                ( n_stars = pg:pointerGroup )?
                ( BAND {isRefVar = true;})?
                ( id:ID
                	{ declName = id.getText(); ## = #id; }
                | LPAREN declName = d:declarator[false] RPAREN
                	{ ## = (TNode)#d.getFirstChild(); }
                )
                        { if (isRefVar) {
                            ## = #( #[NReferenceDecl], ## ) ;
                          }
                        }
                ( LBRACKET dim:dimensionSpecifier RBRACKET
                	{
                    	if (dims == null)
                    		dims = #dim;
                    	else
                    		dims = #( null, dims, #dim);
                	}
                	{
                    	if (dims != null)
                			## = #( #[NArrayDeclarator], ##,
                					#( #[NDimColons], dims ));
                		dims = null;
                	}
                )*
                ( LPAREN
					{ 
                    	if (isFunctionDefinition) {
                        	pushScope(declName);
                        }
                        else {
                        	pushScope("!"+declName); 
                        }
                     }
                    (
							{  ## = #( null, ##, #[NUnspecifiedParameterList] ); }
                    | ("void" RPAREN)=> "void"
                    		{ ## = #( null, ##, #[NParameterTypeList] ); }
                    | (declSpecifiers)=> p:parameterTypeList
                        	{
                            	## = #( null, ##, #( #[NParameterTypeList], #p ) );
                            }
                    | i:idList
                            {
                            	## = #( null, ##, #( #[NParameterTypeList], #i ) );
                            }
                    )
                     {
                     	popScope();
                     }    
                  RPAREN
                  	{
                  		## = #( #[NFunctionDeclarator], ## );	
                  	}
                )*
				{
					if (n_stars > 0) {
						## = makeDeclaratorFromPointerGroup(#pg, ##);
					}
					## = #( #[NDeclarator], ## );
				}
        ;

dimensionSpecifier !
		:	(
					{ ## = #( #[NDimColon], #[NNone], #[NNone]); }
			|	( STAR RBRACKET )=> STAR
					{ ## = #( #[NDimColon], #[NNone], #[NNone]); }
			|	e:constExpr
					{ ## = #( #[NDimColon], #[IntIntConst, "0"],
											#( #[MINUS],
													#e,
													#[IntIntConst, "1"] )); }
			)
		;

parameterTypeList
        :       parameterDeclaration
                (   options {
                            warnWhenFollowAmbig = false;
                        } : 
                  COMMA!
                  parameterDeclaration
                )*
                ( COMMA!
                  VARARGS
                )?
        ;


parameterDeclaration
                            { String declName; }
        :       ds:declSpecifiers
                ( ( declarator[false] )=> declName = d:declarator[false]
                            {
                            AST d2, ds2;
                            d2 = astFactory.dupList(#d);
                            ds2 = astFactory.dupList(#ds);
                            symbolTable.add(declName, #(null, ds2, d2));
                            }
                | ! ad:abstractDeclarator
                	{
                		## = makeTypeFromDeclarator(#ad,
                							(TNode)astFactory.dupList(#ds));
                	}
                )?
                {
                	## = #( #[NParameterDeclaration], ## );
                }
        ;

/* JTC:
 * This handles both new and old style functions.
 * see declarator rule to see differences in parameters
 * and here (declaration SEMI)* is the param type decls for the
 * old style.  may want to do some checking to check for illegal
 * combinations (but I assume all parsed code will be legal?)
 */

functionDef !
                            { String declName;
                              TNode mods = null,type = null, params = null, funcName = null;}
        :
        		( attributeDecl )* 
        		( ( functionStorageClassSpecifier )=>
        			( s:functionStorageClassSpecifier
        					{ mods = #( null, mods, #s); } )+
        		| )
                ( (functionDeclSpecifiers)=> ds:functionDeclSpecifiers
                |  //epsilon
                )
                declName = d:declarator[true]
                {
                                        Integer nameFirstTok = (Integer)#d.getAttribute("firstToken") ;
                                        Integer nameLastTok  = (Integer)#d.getAttribute("lastToken") ;
					AST d2, ds2;
					d2 = astFactory.dupList(#d);
					ds2 = astFactory.dupList(#ds);
					symbolTable.add(declName, #(null, ds2, d2));
					pushScope(declName);
					type = getFunctionReturnType(declName, #d, #ds);
					params = getFunctionParameters(declName, #d);
                                        funcName = (TNode)astFactory.create(ID,declName) ;
                                        funcName.setAttribute("firstToken", nameFirstTok) ;
                                        funcName.setAttribute("lastToken", nameLastTok) ;
                }
                ( olddecls:oldDeclarationList )? (VARARGS)? ( SEMI! )*
                            { popScope(); }
                b:compoundStatement[declName]
                            { ## = #( #[NFunctionDef],
                            			#( #[NModifiers], mods),
                                                type, funcName, params, #( #[NOldDecl], olddecls), #b ); }
        ;

functionDeclSpecifiers
                                { int specCount = 0, n_stars; }
        :       (               options {   // this loop properly aborts when
                                            // it finds a non-typedefName ID MBZ
                                            warnWhenFollowAmbig = false;
                                        } :
                  ! q:typeQualifier
                	{ ## = addTypeSpecModifier(##, #q); }
                | ! specCount = m:typeModifier[specCount]
                	{ ## = addTypeNameModifier(##, #m); }
                | ! ( nonModifiedTypeSpecifier[specCount] )=>
                	specCount = t:nonModifiedTypeSpecifier[specCount]
                	{ ## = setBaseType(##, #t); }
                | ! n_stars = pg:pointerGroup
                	{ ## = makeTypeFromPointerGroup(#pg, ##); }
                )+
        ;

declarationList
        :       (               options {   // this loop properly aborts when
                                            // it finds a non-typedefName ID MBZ
                                            warnWhenFollowAmbig = false;
                                        } :
                	declaration
                )*
        ;

oldDeclarationList
        :       (               options {   // this loop properly aborts when
                                            // it finds a non-typedefName ID MBZ
                                            warnWhenFollowAmbig = false;
                                        } :
                	oldDeclaration
                )+
        ;

declarationPredictor
        :       ( options {      //only want to look at declaration if I don't see typedef
                    warnWhenFollowAmbig = false;
                	}:
                "typedef"
                | declaration
                )
        ;


compoundStatement[String scopeName]
        :       lc:LCURLY!
                            { pushScope(scopeName); }
                (
                	( ( declarationPredictor)=> declaration
                	| statement )
                )*
                            { popScope(); }
                rc:RCURLY!
                	{
                    	## = #( #[NCompoundStatement, scopeName], ##);
                        TNode last = (TNode)#rc;
                        if (last.getAttribute("tokenNumber") != null) {
                        	##.setAttribute("lastToken",
                        			last.getAttribute("tokenNumber"));
                        }
                    }
        ;

    
statementList
        :       ( statement )*
        ;
statement
        :       SEMI                     // Empty statements
					{ ## = #( #[NEmptyStatement], ## ); }
        |       compoundStatement[getAScopeName()]       // Group of statements

        |       expr sc1:SEMI!			 // Expressions
        			{
        				## = #( #[NStatementExpr], ## );
        				TNode last = (TNode)#sc1;
                        if (last.getAttribute("tokenNumber") != null) {
                        	##.setAttribute("lastToken",
                        			last.getAttribute("tokenNumber"));
                        }
        			}

// Iteration statements:

        |       "while"^ LPAREN! expr RPAREN! statement
        |!      "do" st:statement "while" LPAREN ts:expr RPAREN SEMI
        			{ ## = #( #[LITERAL_do, "do"], #ts, #st ); }
        |!      "for"
                LPAREN
                ( (declSpecifiers)=> d1:declaration | ( e1:expr )? SEMI )
                ( e2:expr )? SEMI ( e3:expr )? RPAREN
                s:statement
                                    {
                                        if ( #e1 == null) {
                                        	if (#d1 != null) {
                                        		#e1 = #d1;
                                        	} else {
                                        		#e1 = #[ NEmptyExpression ];
                                        	}
                                        }
                                        if ( #e2 == null) { #e2 = #[ NEmptyExpression ]; }
                                        if ( #e3 == null) { #e3 = #[ NEmptyExpression ]; }
                                        ## = #( #[LITERAL_for, "for"], #e1, #e2, #e3, #s );
                                    }


// Jump statements:

        |       "goto"^ ID SEMI
        |       "continue" SEMI
        |       "break" SEMI
        |       "return"^ ( expr )? SEMI


// Labeled statements:
        |       ID COLON! (options {warnWhenFollowAmbig=false;}:statement)? { ## = #( #[NLabel], ## ); }


// Selection statements:

        |       "if"^
                 LPAREN! expr RPAREN! statement  
                ( //standard if-else ambiguity
                        options {
                            warnWhenFollowAmbig = false;
                        } :
                "else" statement )?
        |       "switch"^ LPAREN! expr RPAREN! LCURLY! (switchCaseStatement)+ RCURLY!
        ;


switchCaseStatement
        :       "case"^ constExpr COLON! statementList
        |       "default"^ COLON! statementList
        ;


expr
        :       assignExpr (options {
                                /* MBZ:
                                    COMMA is ambiguous between comma expressions and
                                    argument lists.  argExprList should get priority,
                                    and it does by being deeper in the expr rule tree
                                    and using (COMMA assignExpr)*
                                */
                                warnWhenFollowAmbig = false;
                            } :
                            c:COMMA^ { #c.setType(NCommaExpr); } assignExpr         
                            )*
        ;


assignExpr
        :       conditionalExpr ( a:assignOperator! assignExpr { ## = #( #a, ## );} )?
        ;

assignOperator
        :       ASSIGN
        |       DIV_ASSIGN
        |       PLUS_ASSIGN
        |       MINUS_ASSIGN
        |       STAR_ASSIGN
        |       MOD_ASSIGN
        |       RSHIFT_ASSIGN
        |       LSHIFT_ASSIGN
        |       BAND_ASSIGN
        |       BOR_ASSIGN
        |       BXOR_ASSIGN
        ;


conditionalExpr
        :       logicalOrExpr
                ( QUESTION^ expr COLON! conditionalExpr )?
        ;


constExpr
        :       conditionalExpr
        ;

logicalOrExpr
        :       logicalAndExpr ( LOR^ logicalAndExpr )*
        ;


logicalAndExpr
        :       inclusiveOrExpr ( LAND^ inclusiveOrExpr )*
        ;

inclusiveOrExpr
        :       exclusiveOrExpr ( BOR^ exclusiveOrExpr )*
        ;


exclusiveOrExpr
        :       bitAndExpr ( BXOR^ bitAndExpr )*
        ;


bitAndExpr
        :       equalityExpr ( BAND^ equalityExpr )*
        ;



equalityExpr
        :       relationalExpr
                ( ( EQUAL^ | NOT_EQUAL^ ) relationalExpr )*
        ;


relationalExpr
        :       shiftExpr
                ( ( LT^ | LTE^ | GT^ | GTE^ ) shiftExpr )*
        ;



shiftExpr
        :       additiveExpr
                ( ( LSHIFT^ | RSHIFT^ ) additiveExpr )*
        ;


additiveExpr
        :       multExpr
                ( ( PLUS^ | MINUS^ ) multExpr )*
        ;


multExpr
        :       castExpr
                ( ( STAR^ | DIV^ | MOD^ ) castExpr )*
        ;


castExpr
        :       ( LPAREN typeName RPAREN )=>
                LPAREN! typeName RPAREN! ( castExpr )
                            { ## = #( #[NCast, "("], ## ); }

        |       unaryExpr
        ;


typeName
        :       sq:specifierQualifierList
        		( ad:abstractDeclarator
        			{
	        			## = makeTypeFromDeclarator(#ad,
	        								(TNode)astFactory.dupTree(#sq));
        			}
        		)?
        ;

abstractDeclarator !
			{ TNode decl = null; int dummy; }
		:
		(	dummy = pg:pointerGroup
			( ad1:directAbstractDeclarator[decl]
					{ decl = #ad1; }
			)*
				{ ## = makeDeclaratorFromPointerGroup(#pg, decl); }
		|	( ad2:directAbstractDeclarator[decl]
				{ decl = #ad2; }
			)+
				{ ## = decl; }
		)
		;

directAbstractDeclarator ! [TNode decl]
		:
		(	( LPAREN
				( ( STAR | LPAREN | LBRACKET )=> ad:abstractDeclarator
					{ ## = #ad; }
				| ("void" RPAREN )=> "void"
					{ ## = #( #[NFunctionDeclarator], decl, #[NParameterTypeList] ); }
				|
					{ ## = #( #[NFunctionDeclarator], decl, #[NUnspecifiedParameterList] ); }
				| pl:parameterTypeList
					{ ## = #( #[NFunctionDeclarator], decl,
                        			  		#( #[NParameterTypeList], #pl )); }
				)
			RPAREN )
		|	( LBRACKET dim:dimensionSpecifier RBRACKET )
				{ ## = #( #[NArrayDeclarator], decl,
                						 #( #[NDimColons], dim )); }
		)
		;

unaryExpr
        :       postfixExpr
        |       INC^ unaryExpr
        |       DEC^ unaryExpr
        |       starOperator castExpr { ## = #( #[NStarExpr], ## ); }
        |       u:unaryOperator castExpr { ## = #( #[NUnaryExpr], ## ); }
        |       "sizeof"^
                ( ( LPAREN typeName )=> LPAREN typeName RPAREN
                | unaryExpr
                )
        |       "__alignof"^
                ( ( LPAREN typeName )=> LPAREN typeName RPAREN
                | unaryExpr
                )       
        |       gnuAsmExpr
        ;

starOperator
        :       STAR
        ;

unaryOperator
        :       BAND
        |       PLUS
        |       MINUS
        |       BNOT
        |       LNOT
        ;

gnuAsmExpr
        :       "asm"^ ("volatile")? 
                LPAREN stringConst
                ( options { warnWhenFollowAmbig = false; }:
                  COLON (strOptExprPair ( COMMA strOptExprPair)* )?
                  ( options { warnWhenFollowAmbig = false; }:
                    COLON (strOptExprPair ( COMMA strOptExprPair)* )?
                  )?
                )?
                ( COLON stringConst ( COMMA stringConst)* )?
                RPAREN
                                { ##.setType(NGnuAsmExpr); }
        ;
        
//GCC requires the PARENs
strOptExprPair
        :  stringConst ( LPAREN expr RPAREN )?
        ;
        
postfixExpr !
        :       p1:primaryExpr
        				{ ## = #p1; }
                ( 
                	p2:postfixSuffix[#p1]
                		{ ## = #( #[NPostfixExpr], #p2 ); } 
                )?
        ;
postfixSuffix ! [ TNode prim ]
        :
                ( PTR i1:ID
                		{ ## = #( #[PTR], prim, #i1 ); prim = ##; }
                | DOT i2:ID
                		{ ## = #( #[DOT], prim, #i2 ); prim = ##; }
		| THREELT ce:expr THREEGT cfc:functionCall
                		{ ## = #( #[NFunctionCall], prim, #cfc ); prim = ##;
				  ## = #( #[NCudaCall], #ce, prim ); prim = ##; }
                | fc:functionCall
                		{ ## = #( #[NFunctionCall], prim, #fc ); prim = ##; }
                | LBRACKET e:expr RBRACKET
                		{ ## = #( #[NArrayAccess], prim, #e ); prim = ##; }
                | INC
                		{ ## = #( #[INC], prim ); prim = ##; }
                | DEC
                		{ ## = #( #[DEC], prim ); prim = ##; }
                )+
        ;

functionCall
        :
                LPAREN^ (argExprList)? RPAREN
        ;
    

primaryExpr
        :       ID
        |       charConst
        |       intConst
        |       floatConst
        |       stringConst

// JTC:
// ID should catch the enumerator
// leaving it in gives ambiguous err
//      | enumerator
        |       LPAREN! expr RPAREN!        { ## = #( #[NExpressionGroup, "("], ## ); }
        ;

argExprList
        :       assignExpr ( COMMA! assignExprOrType )*
        ;

assignExprOrType
		:		assignExpr
		|		typeName
		;
		
pointerBaseType
		:
                bt:baseType starOperator                  { ## = #( #[NPointerBaseType], ##); }
		;


protected
charConst
        :       CharLiteral
        ;


protected
stringConst
        :       (StringLiteral)+                { ## = #(#[NStringSeq], ##); }
        ;


protected
intConst
        :       IntOctalConst
        |       LongOctalConst
        |       UnsignedOctalConst
        |       IntIntConst
        |       LongIntConst
        |       UnsignedIntConst
        |       IntHexConst
        |       LongHexConst
        |       UnsignedHexConst
        ;


protected
floatConst
        :       FloatDoubleConst
        |       DoubleDoubleConst
        |       LongDoubleConst
        ;




    

dummy
        :       NTypedefName
        |       NInitDecl
        |       NDeclarator
        |	NStructBodyElement
        |       NStructDeclarator
        |       NDeclaration
        |       NCast
        |       NPointerGroup
        |       NExpressionGroup
        |       NFunctionCall
        |       NCudaCall
        |       NNonemptyAbstractDeclarator
        |       NInitializer
        |       NStatementExpr
        |	NEmptyStatement
        |       NEmptyExpression
        |	NUnspecifiedParameterList
        |       NParameterTypeList
        |       NFunctionDef
        |       NCompoundStatement
        |       NParameterDeclaration
        |       NCommaExpr
        |       NStarExpr
        |       NUnaryExpr
        |       NLabel
        |       NPostfixExpr
        |       NRangeExpr
        |       NStringSeq
        |       NInitializerElementLabel
        |       NLcurlyInitializer
        |       NAsmAttribute
        |       NGnuAsmExpr
        |       NTypeMissing
        |       NReferenceDecl
        |		NModifiedType
        |		NModifiedTypeSpec
        |		NModifiers
        |		NPointerType
        |		NPointerBaseType
        |		NStructType
        |		NUnionType
        |		NTypeDeclaration
        |		NPointerDeclarator
        |		NArrayDeclarator
        |		NFunctionDeclarator
        |		NModifiedDeclarator
        |		NDimColons
        |		NDimColon
        |		NNone
        |		NFunctionType
        |		NArrayType
        |		NEnumType
        |		NArrayAccess
        |		NStaticGlobal
        |		NStaticLocal
        |		NOldDecl
        ;






{

}

class StdCLexer extends Lexer;

options
        {
        k = 3;
        exportVocab = STDC;
        testLiterals = false;
        }
tokens {
		LITERAL___extension__ = "__extension__";
}
{
  LineDirective LineDirective = new LineDirective();
  String originalSource = "";
  PreprocessorInfoChannel preprocessorInfoChannel = new PreprocessorInfoChannel();
  int tokenNumber = 0;
  boolean countingTokens = true;
  int deferredLineCount = 0;
  boolean throwsOnEOF = false;
  boolean debugMode = false ;

        public void setDebugMode(boolean debug) {
            debugMode = debug ;
        }
  
  public void enableExceptionOnEOF() {
  	throwsOnEOF = true;
  }
  public void disableExceptionOnEOF() {
  	throwsOnEOF = false;
  }
  
  // GnuC specific litterals.
  public void initGnuLiterals()
  {
    literals.put(new ANTLRHashString("__asm", this), LITERAL_asm);
    literals.put(new ANTLRHashString("__asm__", this), LITERAL_asm);
    literals.put(new ANTLRHashString("__attribute__", this), LITERAL___attribute);
    literals.put(new ANTLRHashString("__const", this), LITERAL___const);
    literals.put(new ANTLRHashString("__const__", this), LITERAL___const__);
    literals.put(new ANTLRHashString("__inline", this), LITERAL_inline);
    literals.put(new ANTLRHashString("__inline__", this), LITERAL_inline);
    literals.put(new ANTLRHashString("__signed", this), LITERAL___signed);
    literals.put(new ANTLRHashString("__signed__", this), LITERAL___signed__);
    literals.put(new ANTLRHashString("__volatile", this), LITERAL___volatile);
    literals.put(new ANTLRHashString("__volatile__", this), LITERAL___volatile__);
    literals.put(new ANTLRHashString("__restrict", this), LITERAL___restrict);
  }

  public void setCountingTokens(boolean ct) 
  {
    countingTokens = ct;
    if ( countingTokens ) {
      tokenNumber = 0;
    }
    else {
      tokenNumber = 1;
    }
  }

  public void setOriginalSource(String src) 
  {
    originalSource = src;
    LineDirective.setSource(src);
  }
  public void setSource(String src) 
  {
    LineDirective.setSource(src);
  }
  
  public PreprocessorInfoChannel getPreprocessorInfoChannel() 
  {
    return preprocessorInfoChannel;
  }

  public void setPreprocessingDirective(String pre)
  {
    preprocessorInfoChannel.addLineForTokenNumber( pre, tokenNumber);
  }
  
  protected Token makeToken(int t)
  {
    if ( t != Token.SKIP && countingTokens) {
        tokenNumber++;
    }
    CToken tok = (CToken) super.makeToken(t);
    tok.setLine(LineDirective.line);
    tok.setSource(LineDirective.source);
    tok.setTokenNumber(tokenNumber);

    LineDirective.line += deferredLineCount;
    deferredLineCount = 0;
    if (debugMode) {
        System.out.println(" JUST BUILT TOKEN "+tok.getTokenNumber()+" @"+Integer.toHexString(tok.hashCode())+" type:"+tok.getType()+" text:"+tok.getText()+" line:"+tok.getLine()+" src:"+tok.getSource()) ;
    }
    return tok;
  }

    public void deferredNewline() { 
        deferredLineCount++;
    }

    public void newline() { 
        LineDirective.newline();
    }


    public void uponEOF() throws TokenStreamException, CharStreamException {
    	if (throwsOnEOF)
    		throw new TokenStreamException("EOF");
    }

}

protected
Vocabulary
        :       '\3'..'\377'
        ;


/* Operators: */

ASSIGN          : '=' ;
COLON           : ':' ;
COMMA           : ',' ;
QUESTION        : '?' ;
SEMI            : ';' ;
PTR             : "->" ;


// DOT & VARARGS are commented out since they are generated as part of
// the Number rule below due to some bizarre lexical ambiguity shme.

// DOT  :       '.' ;
protected
DOT:;

// VARARGS      : "..." ;
protected
VARARGS:;


LPAREN          : '(' ;
RPAREN          : ')' ;
LBRACKET        : '[' ;
RBRACKET        : ']' ;
LCURLY          : '{' ;
RCURLY          : '}' ;

EQUAL           : "==" ;
NOT_EQUAL       : "!=" ;
THREELT         : "<<<" ;
LTE             : "<=" ;
LT              : "<" ;
THREEGT         : ">>>" ;
GTE             : ">=" ;
GT              : ">" ;

DIV             : '/' ;
DIV_ASSIGN      : "/=" ;
PLUS            : '+' ;
PLUS_ASSIGN     : "+=" ;
INC             : "++" ;
MINUS           : '-' ;
MINUS_ASSIGN    : "-=" ;
DEC             : "--" ;
STAR            : '*' ;
STAR_ASSIGN     : "*=" ;
MOD             : '%' ;
MOD_ASSIGN      : "%=" ;
RSHIFT          : ">>" ;
RSHIFT_ASSIGN   : ">>=" ;
LSHIFT          : "<<" ;
LSHIFT_ASSIGN   : "<<=" ;

LAND            : "&&" ;
LNOT            : '!' ;
LOR             : "||" ;

BAND            : '&' ;
BAND_ASSIGN     : "&=" ;
BNOT            : '~' ;
BOR             : '|' ;
BOR_ASSIGN      : "|=" ;
BXOR            : '^' ;
BXOR_ASSIGN     : "^=" ;


Whitespace
        :       ( ( '\003'..'\010' | '\t' | '\013' | '\f' | '\016'.. '\037' | '\177'..'\377' | ' ' )
                | "\r\n"                { newline(); }
                | ( '\n' | '\r' )       { newline(); }
                )                       { _ttype = Token.SKIP;  }
        ;


Comment
			{
				SourceComment sc = new SourceComment();
			}
        :       "/*"
                ( { LA(2) != '/' }? '*'
                | { LA(2) == '\n' }? "\r\n"!
                	{
                		sc.addLine(getText());
                		setText("");
                		deferredNewline();
                	}
                | ( '\r'! | '\n'! )
                	{
                		sc.addLine(getText());
                		setText("");
                		deferredNewline();
                	}
                | ~( '*'| '\r' | '\n' )
                )*
                "*/"
                    {
                		sc.addLine(getText());
                	}
                
                	{
                		preprocessorInfoChannel.addLineForTokenNumber(sc,tokenNumber);
                		_ttype = Token.SKIP;  
					}
        ;


CPPComment
        :
                "//" ( ~('\n') )* 
                    {
                    	preprocessorInfoChannel.addLineForTokenNumber(new SourceComment(getText()),
                													  tokenNumber);
                        _ttype = Token.SKIP;
                    }
        ;

PREPROC_DIRECTIVE
options {
  paraphrase = "a line directive";
}
        :
        '#'
        (	( "line" | (( ' ' | '\t' | '\014')+ '0'..'9') )=> LineDirectiveProcess
          | ( "pragma" )=> "pragma" (~'\n')*
          			{ preprocessorInfoChannel.addLineForTokenNumber(new PragmaDirective(getText()),
          														tokenNumber);
          			}
          | (~'\n')* 
            		{ setPreprocessingDirective(getText()); }
        )
                {  
                    _ttype = Token.SKIP;
                }
        ;

protected  Space:
        ( ' ' | '\t' | '\014')
        ;

protected LineDirectiveProcess
{
        boolean oldCountingTokens = countingTokens;
        countingTokens = false;
}
:
                {
                        LineDirective = new LineDirective();
                        deferredLineCount = 0;
                }
        ("line")?  //this would be for if the directive started "#line", but not there for GNU directives
        (Space)+
        n:Number { LineDirective.setLine(Integer.parseInt(n.getText())); } 
        (options {greedy=true;}: Space)+
        (options {greedy=true;}:
        		  fn:StringLiteral
        		  		{  try {  LineDirective.setSource(fn.getText()); } 
                           catch (StringIndexOutOfBoundsException e) { /*not possible*/ }
                        }
                | fi:ID { LineDirective.setSource(fi.getText()); }
        )?
        (options {greedy=true;}: Space)*
        (options {greedy=true;}: "1"            { LineDirective.setEnteringFile(true); } )?
        (options {greedy=true;}: Space)*
        (options {greedy=true;}: "2"            { LineDirective.setReturningToFile(true); } )?
        (options {greedy=true;}: Space)*
        (options {greedy=true;}: "3"            { LineDirective.setSystemHeader(true); } )?
        (options {greedy=true;}: Space)*
        (options {greedy=true;}: "4"            { LineDirective.setTreatAsC(true); } )?
        (~('\r' | '\n'))*
        ("\r\n" | "\r" | "\n")
                {
                        preprocessorInfoChannel.addLineForTokenNumber(new LineDirective(LineDirective), tokenNumber);
                        countingTokens = oldCountingTokens;
                }
        ;



/* Literals: */

/*
 * Note that we do NOT handle tri-graphs nor multi-byte sequences.
 */


/*
 * Note that we can't have empty character constants (even though we
 * can have empty strings :-).
 */
CharLiteral
        :       '\''! ( Escape | ~( '\'' ) ) '\''!
        ;


/*
 * Can't have raw imbedded newlines in string constants.  Strict reading of
 * the standard gives odd dichotomy between newlines & carriage returns.
 * Go figure.
 */
StringLiteral
        :       '"'!
                ( ( Escape )=> Escape
                | (
                    '\r'        { deferredNewline(); }
                  | '\n'        {
                                deferredNewline();
                                _ttype = BadStringLiteral;
                                }
                  | '\\' '\n'   {
                                deferredNewline();
                                }
                  )
                | ~( '"' | '\r' | '\n' | '\\' )
                )*
                '"'!
        ;


protected BadStringLiteral
        :       // Imaginary token.
        ;


/*
 * Handle the various escape sequences.
 *
 * Note carefully that these numeric escape *sequences* are *not* of the
 * same form as the C language numeric *constants*.
 *
 * There is no such thing as a binary numeric escape sequence.
 *
 * Octal escape sequences are either 1, 2, or 3 octal digits exactly.
 *
 * There is no such thing as a decimal escape sequence.
 *
 * Hexadecimal escape sequences are begun with a leading \x and continue
 * until a non-hexadecimal character is found.
 *
 * No real handling of tri-graph sequences, yet.
 */

protected
Escape  
        :       '\\'
                ( options{warnWhenFollowAmbig=false;}:
                  'a'
                | 'b'
                | 'f'
                | 'n'
                | 'r'
                | 't'
                | 'v'
                | '"'
                | '\''
                | '\\'
                | '?'
                | ('0'..'3') ( options{warnWhenFollowAmbig=false;}: Digit ( options{warnWhenFollowAmbig=false;}: Digit )? )?
                | ('4'..'7') ( options{warnWhenFollowAmbig=false;}: Digit )?
                | 'x' ( options{warnWhenFollowAmbig=false;}: Digit | 'a'..'f' | 'A'..'F' )+
                )
        ;


/* Numeric Constants: */

protected
Digit
        :       '0'..'9'
        ;

protected
LongSuffix
        :       'l'
        |       'L'
        ;

protected
UnsignedSuffix
        :       'u'
        |       'U'
        ;

protected
FloatSuffix
        :       'f'
        |       'F'
        |       'i'
        |       'I'
        ;

protected
Exponent
        :       ( 'e' | 'E' ) ( '+' | '-' )? ( Digit )+
        ;


protected
DoubleDoubleConst:;

protected
FloatDoubleConst:;

protected
LongDoubleConst:;

protected
IntOctalConst:;

protected
LongOctalConst:;

protected
UnsignedOctalConst:;

protected
IntIntConst:;

protected
LongIntConst:;

protected
UnsignedIntConst:;

protected
IntHexConst:;

protected
LongHexConst:;

protected
UnsignedHexConst:;




Number
        :       ( ( Digit )+ ( '.' | 'e' | 'E' ) )=> ( Digit )+
                ( '.' ( Digit )* ( Exponent )?
                | Exponent
                )                       { _ttype = DoubleDoubleConst;   }
                ( FloatSuffix           { _ttype = FloatDoubleConst;    }
                | LongSuffix            { _ttype = LongDoubleConst;     }
                )*

        |       ( "..." )=> "..."       { _ttype = VARARGS;     }

        |       '.'                     { _ttype = DOT; }
                ( ( Digit )+ ( Exponent )?
                                        { _ttype = DoubleDoubleConst;   }
                  ( FloatSuffix         { _ttype = FloatDoubleConst;    }
                  | LongSuffix          { _ttype = LongDoubleConst;     }
                  )*
                )?

        |       '0' ( '0'..'7' )*       { _ttype = IntOctalConst;       }
                ( LongSuffix            { _ttype = LongOctalConst;      }
                | UnsignedSuffix        { _ttype = UnsignedOctalConst;  }
                )*

        |       '1'..'9' ( Digit )*     { _ttype = IntIntConst;         }
                ( LongSuffix            { _ttype = LongIntConst;        }
                | UnsignedSuffix        { _ttype = UnsignedIntConst;    }
                )*

        |       '0' ( 'x' | 'X' ) ( 'a'..'f' | 'A'..'F' | Digit )+
                                        { _ttype = IntHexConst;         }
                ( LongSuffix            { _ttype = LongHexConst;        }
                | UnsignedSuffix        { _ttype = UnsignedHexConst;    }
                )*
        ;

IDMEAT
		:	i:ID
			{
				if (i.getType() == LITERAL___extension__) {
					$setType(Token.SKIP);
				} else {
					$setType(i.getType());
				}
			}
		;

protected ID
        options 
                {
                testLiterals = true;
                }
        :       ( 'a'..'z' | 'A'..'Z' | '_' )
                (options {greedy=true;} : 'a'..'z' | 'A'..'Z' | '_' | '0'..'9' )*
        ;

