package fr.inria.frontc;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;


public class CSymbolTable {

    /**
     * holds list of scopes.
     */
    private final Vector scopeStack;

    /**
     * table where all defined names are mapped to TNode tree nodes.
     */
    private final Hashtable<String, TNode> symTable;

    public CSymbolTable() {
        super();
        scopeStack = new Vector(10);
        symTable = new Hashtable<>(533);
    }


    /**
     * push a new scope onto the scope stack.
     */
    public void pushScope(String s) {
        scopeStack.addElement(s);
    }

    /**
     * pop the last scope off the scope stack.
     */
    public void popScope() {
        int size = scopeStack.size();
        if (size > 0) {
            scopeStack.removeElementAt(size - 1);
        }
    }

    /**
     * returns the number of scopes in the stack.
     */
    public int nbScopes() {
        return scopeStack.size();
    }

    /**
     * return the current scope as a string.
     */
    private String currentScopeAsString() {
        StringBuilder buf = new StringBuilder(100);
        boolean first = true;
        Enumeration e = scopeStack.elements();
        while (e.hasMoreElements()) {
            if (first) {
                first = false;
            } else {
                buf.append("::");
            }
            buf.append(e.nextElement().toString());
        }
        return buf.toString();
    }

    /**
     * given a name for a type, append it with the
     * current scope.
     */
    private String addCurrentScopeToName(String name) {
        String currScope = currentScopeAsString();
        return addScopeToName(currScope, name);
    }

    /**
     * given a name for a type, append it with the
     * given scope.  MBZ.
     */
    private String addScopeToName(String scope, String name) {
        if (scope == null || !scope.isEmpty()) {
            return scope + "::" + name;
        } else {
            return name;
        }
    }

    /**
     * remove one level of scope from name MBZ.
     */
    private String removeOneLevelScope(String scopeName) {
        int index = scopeName.lastIndexOf("::");
        if (index > 0) {
            return scopeName.substring(0, index);
        }
        if (!scopeName.isEmpty()) {
            return "";
        }
        return null;
    }

    /**
     * add a node to the table with it's key as
     * the current scope and the name.
     */
    public TNode add(String name, TNode node) {
        return symTable.put(addCurrentScopeToName(name), node);
    }


    /**
     * lookup a fully scoped name in the symbol table.
     */
    public TNode lookupScopedName(String scopedName) {
        return symTable.get(scopedName);
    }

    /**
     * lookup an unscoped name in the table by prepending
     * the current scope.
     * MBZ -- if not found, pop scopes and look again.
     */
    public TNode lookupNameInCurrentScope(String name) {
        String scope = currentScopeAsString();
        String scopedName;
        TNode tnode = null;
        while (tnode == null && scope != null) {
            scopedName = addScopeToName(scope, name);
            tnode = symTable.get(scopedName);
            scope = removeOneLevelScope(scope);
        }
        return tnode;
    }

    /**
     * convert this table to a string.
     */
    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(300);
        buff.append("CSymbolTable { \nCurrentScope: ").append(currentScopeAsString()).append("\nDefinedSymbols:\n");
        Enumeration<String> ke = symTable.keys();
        Enumeration<TNode> ve = symTable.elements();
        while (ke.hasMoreElements()) {
            buff.append(ke.nextElement().toString()).append(" (").append(TNode.getNameForType(ve.nextElement().getType())).append(")\n");
        }
        buff.append("}\n");
        return buff.toString();
    }

}
