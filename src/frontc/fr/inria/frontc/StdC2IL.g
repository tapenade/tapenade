/*%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        Copyright (c) Non, Inc. 1998 -- All Rights Reserved

PROJECT:        C Compiler
MODULE:         StdC2IL
FILE:           StdC2IL.g

AUTHOR:         Monty Zukowski (jamz@cdsnet.net) April 28, 1998
				Nicolas Chleq (INRIA) April 2006

DESCRIPTION:

                This tree grammar is for a Gnu C AST.
                It turns the tree back into IL.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

header {
package fr.inria.frontc;
}

{
import java.io.*;
import java.util.*;

import fr.inria.tapenade.utils.ILLang;
}
class StdC2IL extends TreeParser;

options
        {
        importVocab = STDC;
        buildAST = false;
        ASTLabelType = "TNode";

        // Copied following options from java grammar.
        codeGenMakeSwitchThreshold = 2;
        codeGenBitsetTestThreshold = 3;
        }


{
	int tabs = 0;
	PrintStream currentOutput;
	String originalSource;
	String currentSource = "";
	LineDirective trueSourceFile;
	PreprocessorInfoChannel preprocessorInfoChannel = null;
	Stack sourceFiles = new Stack();
	boolean txtIL = false;
	boolean noOutput = false;
	boolean originalSourceOnly = false;

public StdC2IL( String source, PreprocessorInfoChannel preprocChannel )
{
		originalSource = source;
        preprocessorInfoChannel = preprocChannel;
        currentOutput = System.out;
}

public StdC2IL( String source, PreprocessorInfoChannel preprocChannel,
				PrintStream output)
{
	originalSource = source;
	preprocessorInfoChannel = preprocChannel;
	currentOutput = output;
}

public void setTextIL(boolean b)
{
	txtIL = b;
}

public void setConvertOriginalSourceOnly(boolean b)
{
	originalSourceOnly = b;
}

void printLineDirective(LineDirective l) {
	// we always return to the trueSourceFile, we never enter it from another file
	// force it to be returning if in fact we aren't currently in trueSourceFile
	if (( trueSourceFile != null ) //trueSource exists
		&& ( !currentSource.equals(trueSourceFile.getSource()) ) //currently not in trueSource
		&& ( trueSourceFile.getSource().equals(l.getSource())  ) ) { //returning to trueSource
		l.setEnteringFile( false );
		l.setReturningToFile( true );
	}
    // the very first line directive always represents the true sourcefile
	if ( trueSourceFile == null ) {
		trueSourceFile = new LineDirective(l.getSource());
		sourceFiles.push(trueSourceFile);
	}
//System.out.println("Directive: " + l.toString());
//System.out.println("top= " + ((LineDirective)sourceFiles.peek()).getSource());
//System.out.println("trueSourceFile= " + trueSourceFile.getSource());
//System.out.println("originalSource= " + originalSource);
//System.out.println("currentSource= " + currentSource);
//System.out.println("Entering:"+ l.getEnteringFile() +" Returning:"+l.getReturningToFile()) ;

    // keep our own stack of files entered
    if ( l.getEnteringFile() ) {
    	if (originalSourceOnly
    		&& (trueSourceFile.getSource().equals(currentSource))) {
    		/* always ouput these directives */
    		boolean old = noOutput;
    		noOutput = false;
    		startAnnotation("include");
    		start_variableArityNode("comments", ILLang.op_comments);
    		String incname;
    		if (l.getSystemHeader()) {
    			incname = "<" + l.getSource() + ">";
    		} else {
    			incname = "\"" + l.getSource() + "\"";
    		}
    		printAtom("stringCst", "#include " + incname,
    						ILLang.op_stringCst);
    		end_variableArityNode();
    		endAnnotation();
    		noOutput = old;
    	}
    	startAnnotation("include");
    	start_variableArityNode("comments", ILLang.op_comments);
        printAtom("stringCst", "tapenade begin #include " + l.getSource(),
    						ILLang.op_stringCst);
    	end_variableArityNode();
    	endAnnotation(); 
    	currentSource = l.getSource();
        sourceFiles.push(l);
        /*
        System.out.println("* currentSource-> " + currentSource);
    	System.out.println("* top-> " + ((LineDirective)sourceFiles.peek()).getSource());
    	*/
        
    }
    // if returning to a file, pop the exited files off the stack
    if ( l.getReturningToFile() ) {
        LineDirective top = (LineDirective) sourceFiles.peek();
        while (( top != trueSourceFile ) && (! l.getSource().equals(top.getSource()) )) {
            sourceFiles.pop();
            top = (LineDirective) sourceFiles.peek();
        }
        currentSource = l.getSource();
        startAnnotation("include");
    		start_variableArityNode("comments", ILLang.op_comments);
        printAtom("stringCst", "tapenade end #include " + l.getSource(),
    						ILLang.op_stringCst);
    	end_variableArityNode();
    	endAnnotation();
        /*
        System.out.println("* currentSource-> " + currentSource);
        System.out.println("* top-> " + ((LineDirective)sourceFiles.peek()).getSource());
        */
    }
}

private Vector mergePreprocs(Vector preprocs)
{
	Vector newpreprocs = new Vector();
	if ( preprocs.size() > 0 ) {
		Object[] a = preprocs.toArray();
		for (int i = 0; i < a.length; i++) {
			Object o = a[i];
			if (o == null)
				continue;
			if (o instanceof LineDirective) {
				/* never merge #line directives */
				newpreprocs.addElement(o);
				continue;
			}
			if (o instanceof SourceComment) {
				SourceComment sc = new SourceComment((SourceComment)o);
				for (int j = i + 1; j < a.length; j++) {
					Object o2 = a[j];
					if (o2 instanceof SourceComment) {
						sc.lines.addAll(((SourceComment)o2).lines);
						a[j] = null;
					}
				}
				newpreprocs.addElement(sc);
				continue;
			}
			newpreprocs.addElement(o);
		}
	}
	return newpreprocs;
}

private String removeCommentDelimitor(String comment, String delimitor, boolean start) {
     String result = comment;
     int index = comment.indexOf(delimitor);
     int length = delimitor.length();
     if (index == 0 && start) {
         result = comment.substring(length);
     } else if (index >= 0 && index == comment.length() - length && !start) {
         result = comment.substring(0, comment.length() - length);
     }
     return result;
}


void printPreprocs(Vector preprocs, boolean post) 
{
    if ( preprocs.size() > 0 ) {
    	//Vector npps = mergePreprocs(preprocs);
    	Vector npps = preprocs;
        Enumeration e = npps.elements();
        while (e.hasMoreElements()) {
            Object o = e.nextElement();
            if (o instanceof LineDirective) {
                printLineDirective((LineDirective) o);
            }
            else if (o instanceof SourceComment) {
                Enumeration le = ((SourceComment)o).lines.elements();
            	Object firstL = le.nextElement();
            	String firstComment = firstL.toString();
            	boolean commentBlock = firstComment.indexOf("/*") == 0;
            	boolean commentSlash = firstComment.indexOf("//") == 0;
            	if (commentBlock) {
            	   if (post)
            		    startAnnotation("postCommentsBlock");
            	   else
            		    startAnnotation("preCommentsBlock");
            	   start_variableArityNode("comments", ILLang.op_commentsBlock);
            	} else if (commentSlash) {
            	   if (post)
            		    startAnnotation("postComments");
            	   else
            		    startAnnotation("preComments");
            	   start_variableArityNode("comments", ILLang.op_comments);
            	}
            	le = ((SourceComment)o).lines.elements();
            	boolean first = true;
        		while (le.hasMoreElements()) {
            		Object l = le.nextElement();
            		String comment = l.toString();
            		if (first) {
            		    comment = removeCommentDelimitor(comment, "/*", true);
            		}
            		comment = removeCommentDelimitor(comment, "*/", false);
            		printAtom("stringCst", removeCommentDelimitor(comment, "//", true),
            				  ILLang.op_stringCst);
            	    first = false;
        		}
            	end_variableArityNode();
            	endAnnotation();
            }
            else if (o instanceof PragmaDirective) {
            	if (post)
            		startAnnotation("postComments");
            	else
            		startAnnotation("preComments");
            	start_variableArityNode("comments", ILLang.op_comments);
            	printAtom("stringCst", o.toString(),
            				ILLang.op_stringCst);
				end_variableArityNode();
				endAnnotation();
            }
            else if (o.toString().indexOf("#include ") == 0) {
                String include = o.toString();
                include = removeCommentDelimitor(include, "#include ", true);
            	printAtom("include", include, ILLang.op_include);
            }
            else {
            	if (post)
            		startAnnotation("postComments");
            	else
            		startAnnotation("preComments");
            	start_variableArityNode("comments", ILLang.op_comments);
            	printAtom("stringCst", o.toString(),
            				ILLang.op_stringCst);
				end_variableArityNode();
            	endAnnotation();
            }
        }
    }

}

void printDirectivesAndCommentsBefore(TNode n) {
	Object ft = n.getAttribute("firstToken");
	if (ft == null)
		ft = n.getAttribute("tokenNumber");
	if (ft != null) {
		Vector preprocs = preprocessorInfoChannel.extractLinesPrecedingTokenNumber((Integer)ft);
    	printPreprocs(preprocs, false);
	}
	/* if n is the last son of its parent, also process the preprocessing
	 * between n and the end of its parent.
	 */
	if (n.getNextSibling() == null) {
		Integer from = (Integer)n.getAttribute("lastToken");
		Integer to;
		if (n.getParent() != null)
			to = (Integer)n.getParent().getAttribute("lastToken");
		else
			to = Integer.valueOf(preprocessorInfoChannel.getMaxTokenNumber() + 1);
		if (from != null && to != null) {
			Vector preprocs
				= preprocessorInfoChannel.extractLinesBetweenTokenNumbers(from, to);
			printPreprocs(preprocs, true);
		}
	}
}

void printDirectivesAndCommentsAfter(TNode n) {
//	Object ft = n.getAttribute("lastToken");
//	if (ft == null)
//		ft = n.getAttribute("tokenNumber");
//	if (ft != null) {
//		 /* count the last token plus the token corresponding to the
//		  * comment/directive itself (if it exists) */
//		Integer tn = Integer.valueOf((Integer)ft);
//		Vector preprocs = preprocessorInfoChannel.extractLinesPrecedingTokenNumber(tn);
//    	printPreprocs(preprocs, true);
//	}
}

void updateOutputStatus(TNode n) {
	if (originalSourceOnly) {
		if (n.getAttribute("source") != null) {
			String s = (String)(n.getAttribute("source"));
			if (trueSourceFile != null
				&& trueSourceFile.getSource().equals(s)) {
				noOutput = false;
			} else {
			    noOutput = !originalSource.equals(s);
			}
		}
	}
}


void initializePrinting()
{
}

void finalizePrinting() {
}

void printTabs() {
	if (noOutput)
		return;
    for ( int i = 0; i< tabs; i++ ) {
        currentOutput.print( "  " );
    }
}

void startAnnotation(String type)
{
	if (noOutput)
		return;
	start_fixedArityNode("annotation", ILLang.op_annotation);
	if (txtIL) {
		printTabs();
		currentOutput.println(type);
	}
	else {
		currentOutput.println(type);
	}
}

void endAnnotation() {
	end_fixedArityNode();
}

void printAtomZero(String name, int code) {
    if (noOutput)
	return;
    if (txtIL) {
	printTabs();
	currentOutput.println(name+"()");
    } else {
	currentOutput.println(code);
    }
}

void printAtom(String type, String value, int code)
{
	if (noOutput)
		return;
	if (txtIL) {
		printTabs();
		currentOutput.println(type + ":" + value + " ");
	}
	else {
		currentOutput.println(code);
		currentOutput.println("" + value + "");
	}
}

void attachLineNumber(int lineno) {
  if (noOutput || lineno<=0) {

  } else if (txtIL) {
    printTabs();
    currentOutput.println("++line++ "+lineno);
  } else {
    currentOutput.println(ILLang.op_annotation) ;
    currentOutput.println("line") ;
    currentOutput.println(ILLang.op_intCst) ;
    currentOutput.println(lineno) ;
  }
}

void printIdent(String s, int lineno) {
        attachLineNumber(lineno) ;
	printAtom("ident", s, ILLang.op_ident);
}

void printIdent(String s) {
	printAtom("ident", s, ILLang.op_ident);
}

void printIdent(TNode t, int lineno) {
        attachLineNumber(lineno) ;
	printIdent(t.getText());
}

void printIdent(TNode t) {
	printIdent(t.getText());
}

void printIntCst(TNode c) {
	printAtom("intCst", c.getText(), ILLang.op_intCst);
}

void printIntCst(String s) {
	printAtom("intCst", s, ILLang.op_intCst);
}

void printBitCst(TNode c) {
	printAtom("bitCst", c.getText(), ILLang.op_bitCst);
}

void printBitCst(String c) {
	printAtom("bitCst", c, ILLang.op_bitCst);
}

void printRealCst(String s) {
	printAtom("realCst", s, ILLang.op_realCst);
}

void printRealCst(TNode c) {
	printRealCst(c.getText());
}

void printStringCst(String s) {
	printAtom("stringCst", s, ILLang.op_stringCst);
}

void printStringCst(TNode c) {
	printStringCst(c.getText());
}

void collectStringCst(String s) {
        collectOneString(s) ;
}

void collectStringCst(TNode c) {
	collectStringCst(c.getText());
}

void printNone() {
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println("none()");
	}
	else {
		currentOutput.println(ILLang.op_none);
	}
}

void printVarArgs() {
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println("variableArgList()");
	}
	else {
		currentOutput.println(ILLang.op_variableArgList);
	}
}

//void start_varDeclarations() {
//	start_variableArityNode("varDeclarations", ILLang.op_varDeclarations);
//}

//void end_varDeclarations() {
//	end_variableArityNode();
//}

//void start_modifiedType() {
//	start_fixedArityNode("modifiedType", ILLang.op_modifiedType);
//}

//void end_modifiedType() {
//	end_fixedArityNode();
//}

void start_modifiers() {
	start_variableArityNode("modifiers", ILLang.op_modifiers);
}

void end_modifiers() {
	end_variableArityNode();
}

//void start_pointerType() {
//	start_fixedArityNode("pointerType", ILLang.op_pointerType);
//}

//void end_pointerType() {
//	end_fixedArityNode();
//}

int myGetLineNum(TNode c) {
    return (c==null ? -1 : c.getLineNum()) ;
}

void start_fixedArityNode(String node_name, int code) {
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println(node_name + "(");
		tabs++;
	}
	else {
		currentOutput.println(code);
	}
}

void start_fixedArityNode(String node_name, int code, int lineno) {
        attachLineNumber(lineno) ;
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println(node_name + "(");
		tabs++;
	}
	else {
		currentOutput.println(code);
	}
}

void end_fixedArityNode() {
	if (noOutput)
		return;
	if (txtIL) {
		tabs--;
		printTabs();
		currentOutput.println(")");
	}
}

void start_variableArityNode(String node_name, int code) {
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println(node_name + "[");
		tabs++;
	}
	else {
		currentOutput.println(code);
	}	
}

void start_variableArityNode(String node_name, int code, int lineno) {
        attachLineNumber(lineno) ;
	if (noOutput)
		return;
	if (txtIL) {
		printTabs(); currentOutput.println(node_name + "[");
		tabs++;
	}
	else {
		currentOutput.println(code);
	}
}

void end_variableArityNode() {
	if (noOutput)
		return;
	if (txtIL) {
		tabs--;
		printTabs();
		currentOutput.println("]");
	}
	else
		currentOutput.println(ILLang.op_endOfList);
}

     /** Mechanism to transform the C string sequences into one op_stringCst */
     String collectedString = null ;

     void initStringCollection() {
         collectedString = "" ;
     }

     void collectOneString(String s) {
         collectedString = collectedString+s ;
     }

     String getCollectedString() {
         String result = collectedString ;
         collectedString = null ;
         return result ;
     }


        int traceDepth = 0;
        public void reportError(RecognitionException ex) {
          if ( ex != null)   {
                System.err.println("C->IL Tree Parsing Recognition Error: " + ex.getClass().getName() );
          }
        }
        public void reportError(NoViableAltException ex) {
                System.err.println("C->IL Tree Parsing NoViableAlt Error: " + ex.toString());
                TNode.printTree( ex.node );
        }
        public void reportError(MismatchedTokenException ex) {
          if ( ex != null)   {
                TNode.printTree( ex.node );
                System.err.println("C->IL Tree Parsing MismatchedToken Error: " + ex );
          }
        }
        public void reportError(String s) {
                System.err.println("C->IL Error from String: " + s);
        }
        public void reportWarning(String s) {
                System.err.println("C->IL Warning from String: " + s);
        }
        protected void match(AST t, int ttype) throws MismatchedTokenException {
                //System.out.println("match("+ttype+"); cursor is "+t);
                super.match(t, ttype);
        }
        public void match(AST t, BitSet b) throws MismatchedTokenException {
                //System.out.println("match("+b+"); cursor is "+t);
                super.match(t, b);
        }
        protected void matchNot(AST t, int ttype) throws MismatchedTokenException {
                //System.out.println("matchNot("+ttype+"); cursor is "+t);
                super.matchNot(t, ttype);
                }
        public void traceIn(String rname, AST t) {
          traceDepth += 1;
          for (int x=0; x<traceDepth; x++) System.out.print(' ');
          super.traceIn(rname, t);   
        }
        public void traceOut(String rname, AST t) {
          for (int x=0; x<traceDepth; x++) System.out.print(' ');
          super.traceOut(rname, t);
          traceDepth -= 1;
        }



}


translationUnit  options {
  defaultErrorHandler=false;
}
        :		{
        			start_variableArityNode("file", ILLang.op_file);
        			initializePrinting();
        		}
        	( externalList )?
               	{
                 finalizePrinting();
                 noOutput = false;
                 end_variableArityNode();
               	}
               
        ;

definitionOrDeclaration
		:
			( externalDef )+
		;

externalList
        :
			( externalDef )+
        ;


externalDef
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
   printDirectivesAndCommentsBefore(in);
   updateOutputStatus(in);
  }
        :
        (       declaration
        |       functionDef
        |       typelessDeclaration
        )
        	{
				printDirectivesAndCommentsAfter(in);
        	}
        |		#(NEmptyStatement SEMI)		// Empty statement
        ;

typelessDeclaration
        :       #(NTypeMissing initDeclList s: SEMI)
        ;


declaration
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
:
  #( NDeclaration 
  {
   start_fixedArityNode("varDeclaration", ILLang.op_varDeclaration, lineno);
   start_modifiers();
   end_modifiers();
  }
  declSpecifiers (initDeclList)?
  {
   end_fixedArityNode();
  }
  )
  |
  #( NTypeDeclaration
  {
   start_fixedArityNode("typeDeclaration", ILLang.op_typeDeclaration, lineno);
  }
  ( i:ID
  {
   printIdent( i );
  }
  | NNone
  {
   printNone();
  }
  )
  declSpecifiers
  {
   end_fixedArityNode();
  }
  ) ;


declSpecifiers
        :
        	  modifiedType
        	| modifiedTypeName
        	| pointerType
        	| functionType
        	| arrayType
        	| recordType
        	| unionType
        	| enumType
        	| baseType
        	| typedefName
        ;

modifiedType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
		#( NModifiedTypeSpec
				{ start_fixedArityNode("modifiedType", ILLang.op_modifiedType, lineno); }
			#( NModifiers typeSpecModifiers )
			(		{ printAtomZero("void", ILLang.op_void); }
				| declSpecifiers
			)
				{ end_fixedArityNode(); }
		  )
		;
		
modifiedTypeName
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
		#( NModifiedType
				{ start_fixedArityNode("modifiedType", ILLang.op_modifiedType, lineno); }
			#( NModifiers mods:typeNameModifiers )
			(	/* empty type means either int or float depending on the
				   modifiers */
					{// TODO float float
						if (mods.getType() == LITERAL_double
						    || mods.firstSiblingOfType(LITERAL_double) != null)
				  		  printAtomZero("float",ILLang.op_float);
				  		else if (mods.getType() == LITERAL_float
				  		        || mods.firstSiblingOfType(LITERAL_float) != null )
	        			  	  printAtomZero("float",ILLang.op_float);
				  	 	else
					  	  printAtomZero("integer",ILLang.op_integer);
					}
			|	baseType
			)
				{ end_fixedArityNode(); }
		  )
		;

typeSpecModifiers
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
				{ start_modifiers(); }
			( typeSpecModifier )+
				{ end_modifiers(); }
		;

typeNameModifiers
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
				{ start_modifiers(); }
			( typeNameModifier )+
				{ end_modifiers(); }
		;

typeSpecModifier
		:
				storageClassSpecifier
			|	typeQualifier
		;

typeNameModifier
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
				"short"			{ printIdent("short"); }
			|	"long"			{ printIdent("long"); }
			|	"double"		{ printIdent("double"); }
			|	"signed"		{ printIdent("signed"); }
			|	"unsigned"		{ printIdent("unsigned"); }
			|	"float"			{ printIdent("float"); }
		;

baseType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
		(
		"void"			{ printAtomZero("void",ILLang.op_void); }
        |       "char"			{ printAtomZero("character",ILLang.op_character); }
        |       "int"			{ printAtomZero("integer",ILLang.op_integer); }
        |       "float"			{ printAtomZero("float",ILLang.op_float); }
        |       "_Float128"			{ printAtomZero("float",ILLang.op_float); }
        |	"_Bool"			{ printAtomZero("boolean",ILLang.op_boolean); }
        |	"_Complex"		{ printAtomZero("complex",ILLang.op_complex); }
        |	gccBuiltinType
        )
		;

gccBuiltinType
		:	(
				"__builtin_va_list"		{ printIdent("__builtin_va_list"); }
			)
		;

pointerType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			#( NPointerType 
					{ start_fixedArityNode("pointerType", ILLang.op_pointerType, lineno); }
				declSpecifiers
					{ end_fixedArityNode(); }
			  )
		;

functionType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			#( NFunctionType
					{ start_fixedArityNode("functionType", ILLang.op_functionType, lineno); }
				declSpecifiers
				( NUnspecifiedParameterList
						{ printNone(); }
				|	#( n:NParameterTypeList
						{ start_variableArityNode("argDeclarations", ILLang.op_argDeclarations, lineno); }
						parameterTypeList
						{ end_variableArityNode(); }
				 	)
				 )
					{ end_fixedArityNode(); }
			 )
		;

arrayType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			#( NArrayType
					{ start_fixedArityNode("arrayType", ILLang.op_arrayType, lineno); }
				declSpecifiers
				#( NDimColons
						{ start_variableArityNode("dimColons", ILLang.op_dimColons, lineno); }
					( dimColon )*
						{ end_variableArityNode(); }
				 )
				 	{ end_fixedArityNode(); }
			 )
		;
		
recordType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			#( NStructType
					{ start_fixedArityNode("recordType", ILLang.op_recordType, lineno); }
				( i:ID { printIdent(#i); } | NNone { printNone(); } )
					{
						start_variableArityNode("modifiers", ILLang.op_modifiers);
						end_variableArityNode();
					}
				( structBody
				|		{ printNone(); }
				)
					{ end_fixedArityNode(); }
			 )
		;

unionType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			#( NUnionType
					{ start_fixedArityNode("unionType", ILLang.op_unionType, lineno); }
				( i:ID { printIdent(#i); } | NNone { printNone(); } )
				( unionBody
				|		{ printNone(); }
				)
					{ end_fixedArityNode(); }
			 )
		;
		
storageClassSpecifier
        :       a:"auto" 			{ printIdent("auto"); }
        |       b:"register"                    { printIdent("register"); }
        |       functionStorageClassSpecifier
        ;

functionStorageClassSpecifier
        :       "extern"                      { printIdent("extern"); }
        |	NStaticGlobal		      { printIdent("private"); }
        |	NStaticLocal		      { printIdent("save"); }
        |       "static"                      { printIdent("static"); }
        |       "inline"                      { printIdent("inline"); }
        |       "__inline__"                  { printIdent("__inline__"); }
        |       "__noinline__"                { printIdent("__noinline__"); }
	|       "__global__"                  { printIdent("__global__"); }
	|       "__device__"                  { printIdent("__device__"); }
	|       "__host__"                    { printIdent("__host__"); }
        |       "__constant__"                { printIdent("__constant__"); }
        |       "__shared__"                  { printIdent("__shared__"); }
        |       "__managed__"                 { printIdent("__managed__"); }
        ;


typeQualifier
        :       a:"const"                       { printIdent("const"); }
        |       aa:"__const"                    { printIdent("__const"); }
        |       aaa:"__const__"                 { printIdent("__const__"); }
        |       b:"volatile"                    { printIdent("volatile"); }
        |       bb:"__volatile"                 { printIdent("__volatile"); }
        |       bbb:"__volatile__"              { printIdent("__volatile__"); }
        |	r:"restrict"			{ printIdent("restrict"); }
        |	rr:"__restrict"			{ printIdent("__restrict"); }
        |	rrr:"__restrict__"		{ printIdent("__restrict__"); }
        ;

typedefName
        :       #(NTypedefName i:ID         { printIdent( i ); } )
        ;
   
structBody
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        		{ start_variableArityNode("declarations", ILLang.op_declarations, lineno); }
        	structDeclarationList
        		{ end_variableArityNode(); }
        ;

unionBody
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        		{ start_variableArityNode("varDeclarations", ILLang.op_varDeclarations, lineno); }
        	structDeclarationList
        		{ end_variableArityNode(); }
        ;

structDeclarationList
        :       ( structDeclaration )+
        ;


structDeclaration
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
   printDirectivesAndCommentsBefore(in);
  }
        :
        	#( NStructBodyElement
        	        { start_fixedArityNode("varDeclaration", ILLang.op_varDeclaration, lineno);
        							start_modifiers();
        							end_modifiers();
        			}
               declSpecifiers structDeclaratorList
               		{
               			end_fixedArityNode();
               		}               
             )
        ;


specifierQualifierList
        :       (
        		  modifiedType
        		| modifiedTypeName
        		| recordType
        		| unionType
        		| enumType
        		| baseType
        		| typedefName
                )+
        ;


structDeclaratorList
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        			{ start_variableArityNode("declarators", ILLang.op_declarators, lineno); }
                ( structDeclarator )*
                	{
                		end_variableArityNode();
                	}
        ;


structDeclarator
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
   printDirectivesAndCommentsBefore(in);
  }
        :
        (	( #(NStructDeclarator (declarator)? COLON) )=>
        	#( NStructDeclarator
        			{ start_fixedArityNode("bitfieldDeclarator", ILLang.op_bitfieldDeclarator, lineno); }
        		( declarator
        		|		{ printIdent(""); }	/* ou printNone() ? */
        		)
        		COLON expr
        			{ end_fixedArityNode(); }
        	)
        |
        	#( NStructDeclarator       
            	declarator
        	)
        )
        ;


enumType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :   #(  NEnumType
        			{ start_fixedArityNode("enumType", ILLang.op_enumType, lineno); }
                ( i:ID { printIdent( i ); } | NNone { printNone(); } )
				(
					enumList
				|		{ printNone(); }
                )
                	{
                		end_fixedArityNode();
                	}
             )
        ;


enumList
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        	{ start_variableArityNode("declarators", ILLang.op_declarators, lineno); }
		enumerator ( enumerator )*
			{ end_variableArityNode(); }
        ;

enumerator
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        ( ( ID ASSIGN )=> i:ID
           		{
        			start_fixedArityNode("assign", ILLang.op_assign, lineno);
        			printIdent(#i);
        		}
        	ASSIGN expr
        		{ end_fixedArityNode(); }
        | id:ID
        		{ printIdent(#id); }
        )
        ;

initDeclList
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        	{ start_variableArityNode("declarators", ILLang.op_declarators, lineno); }
        
		initDecl     
		( initDecl )*
			{ end_variableArityNode(); }
        ;


initDecl
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        (	( #( NInitDecl declarator ASSIGN ) )=>
				#(NInitDecl
					{ start_fixedArityNode("assign", ILLang.op_assign, lineno); }
                	declarator
                	(  a:ASSIGN
                  		initializer
                	 | b:COLON
                  		expr
                	)
                	{ end_fixedArityNode(); }
                 )
        |		#(NInitDecl
                	declarator
                )
        )
        ;


pointerGroup returns [ int n_stars ]
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
   n_stars = 0;
  }
        :       #( NPointerGroup 
                   ( a:STAR
                   		{	n_stars++;
                   			start_fixedArityNode("Declarator", ILLang.op_pointerDeclarator, lineno);
                   		}
                     ( typeQualifier )* 
                   )+ 
                )
        ;



idList
        :       i:ID
                (  COMMA
                   id:ID
                )*
        ;



initializer
        :
        (
        		lcurlyInitializer
		|       #( NInitializer expr )
		)
		;

/*
initializerElementLabel
        :   #( NInitializerElementLabel
                (
                    ( l:LBRACKET              { print( l ); }
                        expr
                        r:RBRACKET            { print( r ); }
                        (a1:ASSIGN             { print( a1 ); } )?
                    )
                    | i1:ID c:COLON           { print( i1 ); print( c ); } 
                    | d:DOT i2:ID a2:ASSIGN      { print( d ); print( i2 ); print( a2 ); }
                )
            )
        ;
*/
lcurlyInitializer
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :     #(NLcurlyInitializer
        			{ start_variableArityNode("arrayConstructor", ILLang.op_arrayConstructor, lineno); }
                initializerList
                	{ end_variableArityNode(); }
                )
        ;

initializerList
        :       ( initializer )*
        ;


declarator
        :
        	#( NDeclarator declarator_sub )
        ;

declarator_sub
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
		(
			id:ID				{ printIdent( id ); }
                |       #( NReferenceDecl
                                        { start_fixedArityNode("referenceDeclarator", ILLang.op_referenceDeclarator, lineno); }
				declarator_sub
					{ end_fixedArityNode(); }
				)
		|	#( NPointerDeclarator
					{ start_fixedArityNode("pointerDeclarator", ILLang.op_pointerDeclarator, lineno); }
				declarator_sub
					{ end_fixedArityNode(); }
				)
		|	#( NArrayDeclarator
					{ start_fixedArityNode("arrayDeclarator", ILLang.op_arrayDeclarator, lineno); }
				declarator_sub
				#( NDimColons
						{ start_variableArityNode("dimColons", ILLang.op_dimColons, lineno); }
					( dimColon )+
						{ end_variableArityNode(); }
					)
					{ end_fixedArityNode(); }
				)
		|	#( NModifiedDeclarator
					{ start_fixedArityNode("modifiedDeclarator", ILLang.op_modifiedDeclarator, lineno); }
				#( NModifiers
						{ start_modifiers(); }
					( typeSpecModifier )+
						{ end_modifiers(); }
					)
				declarator_sub
					{ end_fixedArityNode(); }
				)
		|	#( NFunctionDeclarator
					{ start_fixedArityNode("functionDeclarator", ILLang.op_functionDeclarator, lineno); }
				declarator_sub
				(
					NUnspecifiedParameterList
						{ printNone(); }
				|	#( NParameterTypeList
						{ start_variableArityNode("argDeclarations", ILLang.op_argDeclarations, lineno); }
						parameterTypeList
						{ end_variableArityNode(); }
					 )
				)
					{ end_fixedArityNode(); }
			)
		)
		;

dimColon
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:	#( NDimColon
					{ start_fixedArityNode("dimColon", ILLang.op_dimColon, lineno); }
				( NNone { printNone(); }
								| expr )
				( NNone { printNone(); }
								| expr )
					{ end_fixedArityNode(); }
			)
		;
		
parameterTypeList
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
			( parameterDeclaration )*
			( VARARGS 
					{ printVarArgs(); }
			)?
        ;
    


parameterDeclaration
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
  : #( NParameterDeclaration
       (
         {
          start_fixedArityNode("varDeclaration", ILLang.op_varDeclaration, lineno);
          start_modifiers();
          end_modifiers();
         }
         declSpecifiers
         {
          start_variableArityNode("declarators", ILLang.op_declarators, lineno);
         }
         ( declarator )*
         {
          end_variableArityNode();
          end_fixedArityNode();
         }
        )
      )  ;


functionDef
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :   #( NFunctionDef
        			{ start_fixedArityNode("function", ILLang.op_function, lineno); }
                #( NModifiers functionDefModifiers )
                (	( "void" )=> "void"		{ printAtomZero("void",ILLang.op_void); }
                |	functionDeclSpecifiers
                |    { printNone(); }
                )
		{ printNone(); }
                n:ID					{ printIdent(#n, lineno); }
                ( NUnspecifiedParameterList
						{ printNone(); }
                | #( NParameterTypeList
						{ start_variableArityNode("varDeclarations", ILLang.op_varDeclarations, lineno); }
					(
					   parameterDeclaration
					   | i:ID  { start_fixedArityNode("varDeclaration", ILLang.op_varDeclaration, lineno);
										start_modifiers();
										end_modifiers();
							     printNone();
								 start_variableArityNode("declarators", ILLang.op_declarators, lineno);
							     printIdent(i);
								 end_variableArityNode();
							     end_fixedArityNode(); }
					   | VARARGS
					   		{ printVarArgs(); }
					 )*
						{ end_variableArityNode(); }
				   )
				)
                (#( NOldDecl
                      (
				        { start_fixedArityNode("blockStatement", ILLang.op_blockStatement, lineno); }
				        declarationList
				        |
				        { start_fixedArityNode("blockStatement", ILLang.op_blockStatement, lineno); }
				      )
				  )
                )
                tailCompoundStatement
                	    { end_fixedArityNode(); }
            )
        ;
/*
exception
catch [RecognitionException ex]
                        {
                        reportError(ex);
                        System.out.println("PROBLEM TREE:\n" 
                                                + _t.toStringList());
                        if (_t!=null) {_t = _t.getNextSibling();}
                        }
*/

functionDeclSpecifiers
        :       declSpecifiers
        ;
        
functionDefModifiers
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
			{ start_variableArityNode("modifiers", ILLang.op_modifiers, lineno); }
		(	functionStorageClassSpecifier
		|	typeQualifier )*
			{ end_variableArityNode(); }
		;

declarationList
        :       
                (   //ANTLR doesn't know that declarationList properly eats all the declarations
                    //so it warns about the ambiguity
                    options {
                        warnWhenFollowAmbig = false;
                    } :
                	localLabelDecl
                |	declaration
                )+
        ;

localLabelDecl
        :   #(a:"__label__"
              ( i:ID
              )+
            )
        ;
   


compoundStatement
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       #( NCompoundStatement
        			{ start_variableArityNode("blockStatement", ILLang.op_blockStatement, lineno); }
//                	( declarationList
//                	| functionDef
//                	)*
                	( statementList )
                	{ end_variableArityNode(); }
                )                               
                                                
        ;
        
tailCompoundStatement
        :       #( NCompoundStatement
//                	( declarationList
//                	| functionDef
//                	)*
                	( statementList )
                	{ end_variableArityNode(); }
                )                               
                                                
        ;

statementList
        :       ( statement )*
        ;

statement
			{
				TNode in = ##_in;
				printDirectivesAndCommentsBefore(in);
			}
        :       ( statementBody | declaration )
        	{
        		printDirectivesAndCommentsAfter(in);
        	}
        ;

statementBody
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :

                compoundStatement       	// Group of statements
        |		#(NEmptyStatement SEMI)		// Empty statement
						{ printNone(); }
        |       #(NStatementExpr
                	expr
                )                    // Expressions

// Iteration statements:

        |       #( "while"
        					{
        						start_fixedArityNode("loop", ILLang.op_loop, lineno);
        						printNone();	// ident?
        						printNone();	// label?
        						start_fixedArityNode("while", ILLang.op_while, lineno);
        					}
                	expr
                			{	end_fixedArityNode(); } // while
                	statement
                			{ end_fixedArityNode(); }
                )

        |       #( "do"		{
        						start_fixedArityNode("loop", ILLang.op_loop, lineno);
        						printNone();	// ident?
        						printNone();	// label?
        						start_fixedArityNode("until", ILLang.op_until, lineno);
        					}
                	expr
                			{	end_fixedArityNode(); } // until
                	statement
                			{ end_fixedArityNode(); }
                )

        |       #( "for"
        					{
        						start_fixedArityNode("loop", ILLang.op_loop, lineno);
        						printNone();	// ident?
        						printNone();	// label?
        						start_fixedArityNode("for", ILLang.op_for, lineno);
        					}
                	( declaration | expr )
                	expr
                	expr
                			{	end_fixedArityNode(); } // for
                	statement
                			{ end_fixedArityNode(); }
                )

// Jump statements:

        |       #( "goto"
        			{ start_fixedArityNode("goto", ILLang.op_goto, lineno); }
                   i:ID SEMI
                   { printAtom("label", #i.getText(),
                   	 			ILLang.op_label);
                   	 end_fixedArityNode(); }
                )
        |       "continue" SEMI
        			{
        				start_fixedArityNode("cycle", ILLang.op_cycle, lineno);
        				printNone();
        				end_fixedArityNode();
        			}
        |       "break" SEMI
        			{
        				start_fixedArityNode("break", ILLang.op_break, lineno);
        				printNone();
        				end_fixedArityNode();
        			}
        |       #( "return"           { start_fixedArityNode("return", ILLang.op_return, lineno);
        								 }
                	( { printNone(); } | expr ) SEMI
                                        {printNone(); end_fixedArityNode(); }
                )

// Labeled statements:
        |       #( NLabel
        				{ start_fixedArityNode("labelStatement", ILLang.op_labelStatement, lineno); }
                	ni:ID
                			{ printAtom("label", #ni.getText(),
                						ILLang.op_label); }
                	( { printNone(); } | statement )
                		{ end_fixedArityNode(); }
                )

        |       #( "case"
        				{
        					start_fixedArityNode("switchCase", ILLang.op_switchCase, lineno);
        					start_variableArityNode("expressions", ILLang.op_expressions, lineno); }
                	expr
                		{
                			end_variableArityNode();
                		    start_variableArityNode("blockStatement", ILLang.op_blockStatement, lineno); }
                		statementList
                			{ end_variableArityNode(); 
                  		      end_fixedArityNode(); }
                )

        |       #( "default"
        				{
        					start_fixedArityNode("switchCase", ILLang.op_switchCase, lineno);
        					start_variableArityNode("expressions", ILLang.op_expressions, lineno);
        					end_variableArityNode();
        					start_variableArityNode("blockStatement", ILLang.op_blockStatement, lineno);
        				}
                	statementList
                		{	end_variableArityNode();
                			end_fixedArityNode(); }
                )

// Selection statements:

        |       #( "if"				{ start_fixedArityNode("if", ILLang.op_if, lineno); }
                 	expr
                	statement  
                	(	( "else" )=>
                    		"else" statement
                    |	{ printNone(); }
                	)
                					{ end_fixedArityNode(); }
                )
        |       #( "switch"
        				{ start_fixedArityNode("switch", ILLang.op_switch, lineno); }
                	expr
                		{ start_variableArityNode("switchCases", ILLang.op_switchCases, lineno); }
                	statementList
                		{
                			end_variableArityNode();   // switchCases
                			end_fixedArityNode();
                		}
                )



        ;

expr
        :       
                binaryExpr
        |       conditionalExpr
        |       castExpr
        |       unaryExpr
        |       postfixExpr
        |		arrayAccessExpr
        |		fieldAccessExpr
        |       primaryExpr
        |       emptyExpr
        |       compoundStatementExpr
        |       initializer
        |       rangeExpr
        |       gnuAsmExpr
        |		ptrPostFixExpr
        |       cudaCallExpr
        |       functionCallExpr
        ;

exprOrType
        :       
                binaryExpr
        |       conditionalExpr
        |       castExpr
        |       unaryExpr
        |       postfixExpr
        |		arrayAccessExpr
        |		fieldAccessExpr
        |       primaryExpr
        |       emptyExpr
        |       compoundStatementExpr
        |       initializer
        |       rangeExpr
        |       gnuAsmExpr
        |		
                typeName
        ;
        
pointerBaseType
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		: #(
		  NPointerBaseType
		              { start_fixedArityNode("pointerType", ILLang.op_pointerType, lineno); }
          baseType
        			  { end_fixedArityNode(); }
          )
		;

emptyExpr
        :   NEmptyExpression		{ printNone(); }
        ;

compoundStatementExpr
        :   #( LPAREN
                compoundStatement 
                RPAREN
            )
        ;

rangeExpr
        :   #(NRangeExpr expr v:VARARGS expr)
        ;

gnuAsmExpr
        :   #(n:NGnuAsmExpr
                (v:"volatile" )? 
                LPAREN
                stringConst
                (  options { warnWhenFollowAmbig = false; }:
                    COLON
                    (strOptExprPair 
                        ( COMMA strOptExprPair)* 
                    )?
                  (  options { warnWhenFollowAmbig = false; }:
                    COLON
                      (strOptExprPair 
                        ( COMMA strOptExprPair)* 
                      )?
                  )?
                )?
                ( COLON
                  stringConst 
                  ( COMMA
                    stringConst
                  )* 
                )?
                RPAREN
            )
        ;

strOptExprPair
        :   stringConst 
            ( 
            LPAREN
            expr 
            RPAREN
            )?
        ;

binaryOperator
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       ASSIGN	{ start_fixedArityNode("assign", ILLang.op_assign, lineno); }
        |       DIV_ASSIGN		{ start_fixedArityNode("divAssign", ILLang.op_divAssign, lineno); }
        |       PLUS_ASSIGN		{ start_fixedArityNode("plusAssign", ILLang.op_plusAssign, lineno); }
        |       MINUS_ASSIGN	{ start_fixedArityNode("minusAssign", ILLang.op_minusAssign, lineno); }
        |       STAR_ASSIGN		{ start_fixedArityNode("timesAssign", ILLang.op_timesAssign, lineno); }
        |       MOD_ASSIGN		{ start_fixedArityNode("modAssign", ILLang.op_modAssign, lineno); }
        |       RSHIFT_ASSIGN		{ start_fixedArityNode("rightShiftAssign", ILLang.op_rightShiftAssign, lineno); }
        |       LSHIFT_ASSIGN		{ start_fixedArityNode("leftShiftAssign", ILLang.op_leftShiftAssign, lineno); }
        |       BAND_ASSIGN		{ start_fixedArityNode("bitAndAssign", ILLang.op_bitAndAssign, lineno); }
        |       BOR_ASSIGN		{ start_fixedArityNode("bitOrAssign", ILLang.op_bitOrAssign, lineno); }
        |       BXOR_ASSIGN		{ start_fixedArityNode("bitXorAssign", ILLang.op_bitXorAssign, lineno); }
        |       LOR			{ start_fixedArityNode("or", ILLang.op_or, lineno); }
        |       LAND		{ start_fixedArityNode("and", ILLang.op_and, lineno); }
        |       BOR			{ start_fixedArityNode("binary", ILLang.op_binary, lineno);
        					  printIdent("|"); }
        |       BXOR		{ start_fixedArityNode("binary", ILLang.op_binary, lineno);
        					  printIdent("^"); }
        |       BAND		{ start_fixedArityNode("binary", ILLang.op_binary, lineno);
        					  printIdent("&"); }
        |       EQUAL	{ start_fixedArityNode("eq", ILLang.op_eq, lineno); }
        |       NOT_EQUAL	{ start_fixedArityNode("neq", ILLang.op_neq, lineno); }
        |       LT		{ start_fixedArityNode("lt", ILLang.op_lt, lineno); }
        |       LTE		{ start_fixedArityNode("le", ILLang.op_le, lineno); }
        |       GT		{ start_fixedArityNode("gt", ILLang.op_gt, lineno); }
        |       GTE		{ start_fixedArityNode("ge", ILLang.op_ge, lineno); }
        |       LSHIFT	{ start_fixedArityNode("leftShift", ILLang.op_leftShift, lineno); }
        |       RSHIFT	{ start_fixedArityNode("rightShift", ILLang.op_rightShift, lineno); }
        |       PLUS	{ start_fixedArityNode("add", ILLang.op_add, lineno); }
        |       MINUS	{ start_fixedArityNode("sub", ILLang.op_sub, lineno); }
        |       STAR	{ start_fixedArityNode("mul", ILLang.op_mul, lineno); }
        |       DIV		{ start_fixedArityNode("div", ILLang.op_div, lineno); }
        |       MOD		{ start_fixedArityNode("mod", ILLang.op_mod, lineno); }
        ;

binaryExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        (       b:binaryOperator
                    // no rules allowed as roots, so here I manually get 
                    // the first and second children of the binary operator
                    // and then print them out in the right order
                {       
                	TNode e1, e2;
                    e1 = (TNode) b.getFirstChild();
                    e2 = (TNode) e1.getNextSibling();
                    expr( e1 );
                    expr( e2 );
                    end_fixedArityNode();
                }
        |		c:NCommaExpr
        		{
        			TNode e1, e2;
                    e1 = (TNode) c.getFirstChild();
                    e2 = (TNode) e1.getNextSibling();
        			start_variableArityNode("expressions", ILLang.op_expressions, lineno);
        			expr( e1 );
        			expr( e2 );
        			end_variableArityNode();
        		}
        )                            
        ;

        
conditionalExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       #( QUESTION		{ start_fixedArityNode("ifExpression", ILLang.op_ifExpression, lineno); }
                	expr
                	expr
                	expr
                				{ end_fixedArityNode(); }
                )
        ;


castExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       #( NCast		{ start_fixedArityNode("cast", ILLang.op_cast, lineno); }
                	typeName    
                	expr
                				{ end_fixedArityNode(); }
                 )
        ;


typeName
        :       declSpecifiers
        ;

unaryExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       #(	INC { start_fixedArityNode("unary", ILLang.op_unary, lineno);
        				  printIdent("++prefix"); }
        			expr { end_fixedArityNode(); } )
        |       #(	DEC { start_fixedArityNode("unary", ILLang.op_unary, lineno);
        				  printIdent("--prefix"); }
        			expr { end_fixedArityNode(); } )
        |       #( NStarExpr starOperator expr { printNone() ; end_fixedArityNode(); } )
        |       #( NUnaryExpr unaryOperator expr { end_fixedArityNode(); } )
        |       #( s:"sizeof"
        				{
        					start_fixedArityNode("sizeof", ILLang.op_sizeof, lineno);
        				}
                    ( ( LPAREN typeName )=> 
                        LPAREN typeName RPAREN
                    | expr
                    )
                    	{ end_fixedArityNode(); }
                )
        ;

cudaCallExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
  :
    #( NCudaCall
              { start_fixedArityNode("parallelRegion", ILLang.op_parallelRegion, lineno);
                printIdent("CUDA"); }
       argExprList
              { start_variableArityNode("blockStatement", ILLang.op_blockStatement, lineno); }
       functionCallExpr
              { end_variableArityNode();
                end_fixedArityNode(); }
    ) ;


functionCallExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :      #( NFunctionCall
        					{ start_fixedArityNode("call", ILLang.op_call, lineno);
    						  printNone();	// object
						 }
				
        			primaryOrArrayExpr
        			#( LPAREN
        						{ start_variableArityNode("expressions", ILLang.op_expressions, lineno); }
        				(argExprList)? RPAREN
        						{ end_variableArityNode(); }
        			 )
        					{ end_fixedArityNode(); }
        		 )
        ;


starOperator
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       STAR    { start_fixedArityNode("pointerAccess", ILLang.op_pointerAccess, lineno); }
        ;

unaryOperator
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :       BAND	{ start_fixedArityNode("address", ILLang.op_address, lineno); }
        |       PLUS	{ start_fixedArityNode("unary", ILLang.op_unary, lineno); printIdent("+"); }
        |       MINUS	{ start_fixedArityNode("minus", ILLang.op_minus, lineno); }
        |       BNOT	{ start_fixedArityNode("unary", ILLang.op_unary, lineno); printIdent("~"); }
        |       LNOT	{ start_fixedArityNode("not", ILLang.op_not, lineno); }
        |       LAND
        ;


postfixExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        #( NPostfixExpr
           (
        	ptrPostFixExpr 
            |   fieldAccessExpr
        	|	arrayAccessExpr
        	|	cudaCallExpr
        	|	functionCallExpr
        	|	#( INC	{ start_fixedArityNode("unary", ILLang.op_unary, lineno);
        				  printIdent("++postfix"); }
        			expr { end_fixedArityNode(); } )
        	|	#( DEC	{ start_fixedArityNode("unary", ILLang.op_unary, lineno);
        				  printIdent("--postfix"); }
        			expr { end_fixedArityNode(); } )
        	)
        )
        ;

ptrPostFixExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
	    :
	    #( PTR		{ start_fixedArityNode("fieldAccess", ILLang.op_fieldAccess, lineno);
        			          start_fixedArityNode("pointerAccess", ILLang.op_pointerAccess, lineno); }
        			        r1:expr
                                        { printNone() ; end_fixedArityNode(); }
        			        i1:expr	 )
        			        { end_fixedArityNode(); }
	    ;

arrayAccessExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
        :
        #( NArrayAccess
            { start_fixedArityNode("pointerAccess", ILLang.op_pointerAccess, lineno); }
            r3:expr
            i3:expr
            { end_fixedArityNode(); }
         )
        ;

fieldAccessExpr
  {
   TNode in = ##_in;
   int lineno = myGetLineNum(in);
  }
		:
		#( DOT		{ start_fixedArityNode("fieldAccess", ILLang.op_fieldAccess, lineno); }
        			r2:expr i2:expr	 
        					{ end_fixedArityNode(); } )
		;

primaryOrArrayExpr
	    :	primaryExpr
	    |	arrayAccessExpr
	    |   fieldAccessExpr
	    ;

primaryExpr
        :       i:ID                            { printIdent( i ); }
        |       n:Number
        |		c:intConst						{ printIntCst( c ); }
        |		b:bitConst						{ printBitCst( b ); }
        |		f:floatConst					{ printRealCst( f ); }
        |       charConst
        |       stringConst

// JTC:
// ID should catch the enumerator
// leaving it in gives ambiguous err
//      | enumerator

        |       #( NExpressionGroup
                 	expr
                 )
        ;



argExprList
        :       expr ( exprOrType )*
        ;



protected
charConst
        :       c:CharLiteral
        			{ printAtom("letter", c.getText().replace('\'', '"'),
        						ILLang.op_letter); }
        ;


protected
stringConst
        :       #( NStringSeq
                    { initStringCollection() ;}
                    (
                    s:StringLiteral					{ collectStringCst( s ); }
                    )+
                    { printAtom("stringCst", getCollectedString(), ILLang.op_stringCst); }
                )
        ;


protected
intConst
        :       IntOctalConst
        |       LongOctalConst        
        |       UnsignedOctalConst
        |       IntIntConst
        |       UnsignedIntConst
        ;
        
        
protected
bitConst
        :       IntHexConst
        |       LongHexConst
        |       UnsignedHexConst
        ;        
                
                
protected
floatConst
        :       FloatDoubleConst
        |       DoubleDoubleConst
        |       LongDoubleConst
        |       LongIntConst
        ;
