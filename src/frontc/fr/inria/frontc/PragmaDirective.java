package fr.inria.frontc;

public class PragmaDirective {
    private final String text;

    public PragmaDirective(String s) {
        super();
        text = s;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }
}
