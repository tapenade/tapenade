#!/usr/bin/env bash

TEMP=$(mktemp)
TEMPDOC=$(mktemp)

./gradlew -q clean
./gradlew -q >$TEMP 2>&1

PATH=$PWD/bin:$PATH
cd nonRegressions
DIFFCOMMAND=tapenade
./runAD-jenkins "gitlab" runAD-list-ok-short
