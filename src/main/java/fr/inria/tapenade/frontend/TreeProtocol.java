/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.frontend;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.ILLangOps;
import fr.inria.tapenade.utils.Operator;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.Tree;
import fr.inria.tapenade.utils.ToInt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Manages a connection with an input stream that
 * carries a tree, in agreement with our tree protocol.
 * Operators may come as strings or coded as integers.
 */
public class TreeProtocol {

    /**
     * Contains the InputStream.
     */
    private final LibReader libReader;
    /**
     * The maximal known rank of a IL Operator.
     */
    private final int maxILCode = ILLangOps.opsLength() - 1;

    /**
     * When true, things arriving through the inputStream will be shown.
     */
    public void setTraceOn(boolean traceOn) {
        this.traceOn = traceOn;
    }

    private boolean traceOn;
    /**
     * To allow pop Operator but also just see it without popping it.
     */
    private Operator topOperator;
    /**
     * The index of the operator or value that was just read.
     * This index starts at 0 before the first "read".
     */
    private int position;
    /*Debug stuff: display the state of the arriving tree. */
    private TapList<Operator> debugStackOps;
    private TapIntList debugStackRks;
    private TapList<TreeProtocol.TreeReader> treeReaders;

    /**
     * Creates one TreeProtocol. Opens a reader on the stream "is".
     */
    public TreeProtocol(InputStream is) {
        super();
        this.libReader =
                new LibReader(new BufferedReader(new InputStreamReader(is)));
    }

    /**
     * Creates one TreeProtocol. Opens a reader on the stream "reader".
     */
    public TreeProtocol(BufferedReader reader) {
        super();
        this.libReader = new LibReader(reader);
    }

    public TreeProtocol(LibReader reader) {
        super();
        this.libReader = reader;
    }

    /**
     * @return the index of the last thing (operator or value) read.
     * This index starts at 0 (before the first thing is read).
     */
    public int position() {
        return position;
    }

    public void setPosition(int pos) {
        position = pos;
    }

    /**
     * @return the next thing (assumed to be an Operator)
     * on the InputStream. But keeps it, so that next
     * see or read will return the same Operator.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    public Operator seeOperator() throws IOException {
        if (topOperator == null) {
            topOperator = popOperator();
        }
        return topOperator;
    }

    /**
     * @return the next thing (assumed to be an Operator)
     * on the InputStream. Does not keep it, so that next
     * see or read will return next operator on the stream.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    public Operator readOperator() throws IOException {
        position++;
        Operator oper = topOperator;
        if (oper != null) {
            topOperator = null;
        } else {
            oper = popOperator();
        }
        // Mechanism to build several copies of the currently built tree:
        if (treeReaders != null) {
            TapList<TreeProtocol.TreeReader> inTreeReaders = treeReaders;
            while (inTreeReaders != null) {
                inTreeReaders.head.acceptReadOperator(oper, position);
                inTreeReaders = inTreeReaders.tail;
            }
        }
        return oper;
    }

    /**
     * @return Pops the next element from the input stream. Assumes that it represents
     * an operator. Either it is an integer, and it is the op_code of the
     * operator, or it is a string, and it is the name of this "IL" operator.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    private Operator popOperator() throws IOException {
        Operator operator;
        int code;
        String line = libReader.reader.readLine();
        while (line != null && line.isEmpty()) {
            line = libReader.reader.readLine();
        }
        if (line == null) {
            throw new IOException("Parsing error");
        }

        try {
            code = Integer.parseInt(line);
            if (code <= 0 || code > maxILCode) {
                operator = null;
            } else {
                operator = ILLangOps.ops(code);
            }
        } catch (NumberFormatException e) {
            //Temporary: because Tapenade users may have Libraries that use the old IL syntax:
            //  -- modifiedTypeName doesn't exits any more, replaced by modifiedTypeSpec
            if ("modifiedTypeName".equals(line)) {
                line = "modifiedTypeSpec";
            }
            operator = ILUtils.operator(line);
        }
        if (traceOn) {
            int trd = treeReaderDepth();
            TapEnv.printlnOnTrace();
            while (trd > 0) {
                TapEnv.printOnTrace(" |");
                --trd;
            }
            TapEnv.printOnTrace(operator == null ? "null" :
                    operator.code == ILLang.op_endOfList ? "End_of_List" :
                            operator.code + ":" + operator.name +
                                    (operator.arity >= 0 ? "[" + operator.arity + "]" :
                                            operator.arity == -1 ? "[*]" : ""));
        }
        if (operator == null) {
            throw new IOException("Not a tree operator: " + line);
        }
        if (operator.code == ILLang.op_parsingError) {
            String message = readString();
            throw new IOException(message);
        }
        return operator;
    }

    /**
     * Assumes next thing on stack is an int.
     *
     * @return Pops it and returns it.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    private int readInt() throws IOException {
        int number;
        try {
            number = Integer.parseInt(libReader.reader.readLine());
        } catch (NumberFormatException e) {
            TapEnv.printlnOnTrace(" Problem with long integers in IL");
            number = -9999;
        }
        if (traceOn) {
            TapEnv.printOnTrace("->" + number);
        }
        // Mechanism to build several copies of the currently built tree:
        if (treeReaders != null) {
            TapList<TreeProtocol.TreeReader> inTreeReaders = treeReaders;
            while (inTreeReaders != null) {
                inTreeReaders.head.acceptReadInt(number);
                inTreeReaders = inTreeReaders.tail;
            }
        }
        return number;
    }

    /**
     * Assumes next thing on stack is a String.
     *
     * @return Pops it and returns it.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    public String readString() throws IOException {
        String str = libReader.reader.readLine();
        if (traceOn) {
            TapEnv.printOnTrace("->" + str);
        }
        // Mechanism to build several copies of the currently built tree:
        if (treeReaders != null) {
            TapList<TreeProtocol.TreeReader> inTreeReaders = treeReaders;
            while (inTreeReaders != null) {
                inTreeReaders.head.acceptReadString(str);
                inTreeReaders = inTreeReaders.tail;
            }
        }
        return str;
    }

    private ToInt smallestLineNumber = null ;

    /** Same as readTree(), but returns into toLineNumber
     * the smallest line number found while reading this Tree. */
    public Tree readTree(ToInt toLineNumber) throws IOException {
        smallestLineNumber = toLineNumber ;
        smallestLineNumber.set(-1) ;
        Tree resultTree = readTree() ;
        smallestLineNumber = null ;
        return resultTree ;
    }

    /**
     * Read a Tree with this protocol.
     *
     * @return the next incoming complete Tree on the InputStream.
     * Advances the pointer in the InputStream to the next token
     * immediately following this returned tree.
     * Attaches to the top of the returned tree an annotation with
     * the "position" in the protocol just after
     * the top operator of this tree was read.
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    public Tree readTree() throws IOException {
        int nextOperPosition = position + 1;
        Tree resultTree = readTreeRec();
        ILUtils.setPosition(resultTree, nextOperPosition);
        return resultTree;
    }

    /**
     * TreeReader related methods:
     * <p>
     * The following is used to build Trees from the incoming protocol,
     * but this time by methods triggered by the arrival of the protocol elements
     * instead of by explicitly getting these elements from the protocol pipe.
     * This allows us to read a Tree twice, in parallel, from the same protocol.
     *
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    private Tree readTreeRec()
            throws IOException {
        Operator oper;
        TapList<String> commentNames = null;
        TapList<Tree> commentTrees = null;
        // Sinon on avale les endOfList
        while (seeOperator().code == ILLang.op_annotation) {
            readOperator();
            String annotationName = readString() ;
            Tree annotationValue = readTree() ;
            // Remove "line number" annotations, and don't count them in the position counter:
            if ("line".equals(annotationName)) {
                position -= 2 ;
                if (smallestLineNumber != null) {
                    int thisLineNumber = annotationValue.intValue() ;
                    if (smallestLineNumber.get()==-1 || thisLineNumber<smallestLineNumber.get()) {
                        smallestLineNumber.set(thisLineNumber) ;
                    }
                }
            } else {
                commentNames = new TapList<>(annotationName, commentNames);
                commentTrees = new TapList<>(annotationValue, commentTrees);
            }
        }
        oper = seeOperator();
        if (oper.code == ILLang.op_endOfList) {
            throw new IOException("Parsing error");
        } else if (oper.code == ILLang.op_endOfProtocol) {
            TapEnv.printlnOnTrace("End: operator = endOfProtocol");
            oper = readOperator();
        } else {
            oper = readOperator();
        }
        Tree resultTree = oper.tree();
        while (commentNames != null) {
            resultTree.setAnnotation(
                    commentNames.head, commentTrees.head);
            commentNames = commentNames.tail;
            commentTrees = commentTrees.tail;
        }
        if (oper.code != ILLang.op_endOfProtocol) {
            switch (oper.arity) {
                case -2:
                    resultTree.setValue(readInt());
                    break;
                case -3:
                    resultTree.setValue(readString());
                    break;
                case -1: {
                    //Case of list arity operator:
                    // First son is coming first:
                    int i = 1;
                    while (seeOperator().code != ILLang.op_endOfList) {
                        resultTree.addChild(readTreeRec(), i);
                        i++;
                    }
                    readOperator();
                    break;
                }
                default:
                    //Case of a fixed arity operator:
                    // First son is coming first:
                    for (int i = 1; i <= oper.arity; ++i) {
                        resultTree.setChild(readTreeRec(), i);
                    }
                    break;
            }
        }
        return resultTree;
    }

    public void pushTreeReader() {
        treeReaders = new TapList<>(new TreeProtocol.TreeReader(), treeReaders);
    }

    public Tree popTreeReader() {
        Tree finalTree = treeReaders.head.getFinalTree();
        treeReaders = treeReaders.tail;
        return finalTree;
    }


    private int treeReaderDepth() {
        if (treeReaders == null) {
            return 0;
        }
	TapList<TreeProtocol.TreeReader> inTreeReaders = treeReaders;
        while (inTreeReaders.tail != null) {
            inTreeReaders = inTreeReaders.tail;
        }
        TapList<TreeProtocol.TreeReaderLevel> treeReaderLevels = inTreeReaders.head.treeReaderLevels;
        int depth = 0;
        while (treeReaderLevels != null) {
            if (treeReaderLevels.head.tree != null && !treeReaderLevels.head.isComplete()) {
                ++depth;
            }
            treeReaderLevels = treeReaderLevels.tail;
        }
        return depth;
    }

    /**
     * Used by GeneralLibReader.
     * Read an IL Tree written in prefix form with parentheses.
     *
     * @throws IOException when operator is incorrect or when stream is empty.
     */
    public Tree readPrefixTree() throws IOException {
        int nextToken = libReader.readNextToken();
        String nextValue = libReader.sval;

        //Temporary: because Tapenade users may have Libraries that use the old IL syntax:
        //  -- modifiedTypeName doesn't exits any more, replaced by modifiedTypeSpec
        if ("modifiedTypeName".equals(nextValue)) {
            nextValue = "modifiedTypeSpec";
        }

        Operator oper = ILUtils.operator(nextValue);
        assert oper != null; //[llh 22Sept20] this assert does not seem to work properly.
        if (traceOn) {
            TapEnv.println();
            TapEnv.printOnTrace(oper.code + ':' + oper.name +
                    (oper.arity >= 0 ? '[' + oper.arity + ']' :
                            oper.arity == -1 ? "[*]" : ""));
        }
        int arity = oper.arity;
        Tree resultTree = oper.tree();
        if (arity >= -1) {
            libReader.readNextToken();
            // Read upto the incoming "("
            nextToken = libReader.seeNextToken();
            int i = 0;
            while (nextToken != LibReader.CLOSEPARENTH) {
                while (nextToken == LibReader.COMMA) {
                    libReader.readNextToken();
                    // Read the incoming ","
                    nextToken = libReader.seeNextToken();
                }
                if (nextToken != LibReader.CLOSEPARENTH) {
                    i++;
                    if (arity >= 0) {
                        resultTree.setChild(readPrefixTree(), i);
                    } else {
                        resultTree.addChild(readPrefixTree(), i);
                    }
                    nextToken = libReader.seeNextToken();
                }
            }
            libReader.readNextToken();
        } else {
            // Read upto the incoming ")"
            libReader.readNextToken();
            nextValue = libReader.sval;
            if (nextToken == LibReader.COLON) {
                libReader.readNextToken();
                nextValue = libReader.sval;
            }
            if (traceOn) {
                TapEnv.printOnTrace("->" + nextValue);
            }
            if (arity == -2) {
                resultTree.setValue(Integer.parseInt(nextValue));
            } else {
                resultTree.setValue(nextValue);
            }
        }
        libReader.eliminateSpaces();
        return resultTree;
    }

    /**
     * One element of the list of objects that constitute a TreeReader.
     * Contains one level (i.e. one incomplete node) of an unfinished Tree.
     */
    private static final class TreeReaderLevel {
        private Tree tree;
        private String annot;
        private int freeRank;

        private TreeReaderLevel(Tree tree, String annot, int freeRank) {
            super();
            this.tree = tree;
            this.annot = annot;
            this.freeRank = freeRank;
        }

        private boolean isComplete() {
            if (freeRank == -1) {
                return true;
            }
            if (tree == null) {
                return false;
            }
            return tree.isFixed() && freeRank >= tree.operator().arity;
        }

        @Override
        public String toString() {
            return "(" + annot + "|" + freeRank + "|" + tree + ')';
        }
    }

    /**
     * One TreeReader holds an incomplete Tree which is currently being built
     * as operators and leaf values are progressively "accepted" (in preorder).
     */
    private static final class TreeReader {
        private TapList<TreeProtocol.TreeReaderLevel> treeReaderLevels;

        private TreeReader() {
            super();
            // This initial TreeReaderLevel will hold the final built tree:
            this.treeReaderLevels =
                    new TapList<>(new TreeProtocol.TreeReaderLevel(null, "finalTree", 0), null);
        }

        private Tree getFinalTree() {
            //=> there is certainly one tree to get, and it is complete.
            return treeReaderLevels.head.tree;
        }

        private void pushReaderLevel(TreeProtocol.TreeReaderLevel newLevel) {
            treeReaderLevels = new TapList<>(newLevel, treeReaderLevels);
        }

        private void acceptReadOperator(Operator oper, int position) {
            if (oper.code == ILLang.op_annotation) {
                //=> annotation String then Tree are coming
                pushReaderLevel(new TreeProtocol.TreeReaderLevel(null, null, 0));
            } else if (oper.code == ILLang.op_endOfList) {
                //=>first tree is certainly an incomplete list-arity tree.
                // Mark first tree now complete. Start folding:
                treeReaderLevels.head.freeRank = -1;
                foldTreeReaderLevels();
            } else {
                //=>this is a standard tree operator
                Tree newTree = oper.tree();
                // collect all complete annotations waiting and attach them to newTree:
                while (treeReaderLevels != null && treeReaderLevels.head.annot != null && treeReaderLevels.head.isComplete()) {
                    newTree.setAnnotation(treeReaderLevels.head.annot, treeReaderLevels.head.tree);
                    treeReaderLevels = treeReaderLevels.tail;
                }
                // check whether newTree is already complete or not:
                int newFreeRank = 0;
                if (oper.arity == 0) {
                    // newTree is already complete iff it is fixed arity with arity 0:
                    newFreeRank = -1;
                }
                // then just put newTree here:
                if (treeReaderLevels != null && treeReaderLevels.head.tree == null) {
                    //=> this happens only when receiving an annotation, or at top level:
                    // Put newTree as the tree of the top TreeReaderLevel, which is empty:
                    treeReaderLevels.head.tree = newTree;
                    treeReaderLevels.head.freeRank = newFreeRank;
                    // Only on these "top" or "top annotation" newTree, we attach position:
                    ILUtils.setPosition(newTree, position);
                    // Don't fold, since we are on an annotation!
                } else {
                    // Put newTree into a new TreeReaderLevel:
                    pushReaderLevel(new TreeProtocol.TreeReaderLevel(newTree, null, newFreeRank));
                    // if newTree is complete, start folding:
                    if (newFreeRank == -1) {
                        foldTreeReaderLevels();
                    }
                }
            }
        }

        private void acceptReadString(String text) {
            // Rightfully crash if trying to accept a string with no place to put it!
            TreeProtocol.TreeReaderLevel firstLevel = treeReaderLevels.head;
            if (firstLevel.tree == null) { //=>previous op was certainly an op_annotation
                firstLevel.annot = text;
            } else { //=>first tree level is certainly an atomic string operator
                firstLevel.tree.setValue(text);
                //=>first tree is certainly complete.
                firstLevel.freeRank = -1;
                // Start folding:
                foldTreeReaderLevels();
            }
        }

        private void acceptReadInt(int number) {
            TreeProtocol.TreeReaderLevel firstLevel = treeReaderLevels.head;
            //=>first tree level is certainly an atomic integer operator
            firstLevel.tree.setValue(number);
            //=>first tree is certainly complete.
            firstLevel.freeRank = -1;
            // Start folding:
            foldTreeReaderLevels();
        }

        private void foldTreeReaderLevels() {
            while (treeReaderLevels != null && treeReaderLevels.tail != null &&
                    // ^ While there is something to fold and something to fold into, and
                    treeReaderLevels.head.annot == null &&
                    // ^ the first tree level is not an annotation, and
                    treeReaderLevels.head.isComplete()
                // ^ the first tree level is completely built
            ) {
                // then take the first tree and remove its level:
                Tree newSon = treeReaderLevels.head.tree;
                treeReaderLevels = treeReaderLevels.tail;
                // and add it as a new son of the next tree:
                if (treeReaderLevels.head.tree.isFixed()) {
                    treeReaderLevels.head.tree.setChild(newSon, 1 + treeReaderLevels.head.freeRank);
                } else {
                    treeReaderLevels.head.tree.addChild(newSon, 1 + treeReaderLevels.head.freeRank);
                }
                treeReaderLevels.head.freeRank++;
            }
        }
    }
}
