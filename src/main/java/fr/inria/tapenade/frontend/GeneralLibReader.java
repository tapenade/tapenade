/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.frontend;

import fr.inria.tapenade.representation.AtomFuncDerivative;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.FlowGraphBuilder;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.FunctionTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.MetaTypeSpec;
import fr.inria.tapenade.representation.ModifiedTypeSpec;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.PrimitiveTypeSpec;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VoidTypeSpec;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Object that reads information from Tapenade library files
 * about signatures of external or intrinsic procedures or functions.
 */
public class GeneralLibReader {
    /**
     * "Mots-clefs" :
     */
    private static final String FUNCTION = "function";
    private static final String SUBROUTINE = "subroutine";
    private static final String CONSTANT = "constant";
    private static final String EXTERNAL = "external";
    private static final String INTRINSIC = "intrinsic";
    private static final String SHAPE = "shape";
    private static final String TYPE = "type";
    /**
     * in-out signature des parametres de la subroutine ou de la fonction
     * (y compris le parametre resultat de la fonction)
     * Current InOutAnalyzer N R W RW
     */
    private static final String NOT_READ_NOT_WRITTEN = "notreadnotwritten";
    private static final String READ_NOT_WRITTEN = "readnotwritten";
    private static final String NOT_READ_THEN_WRITTEN = "notreadthenwritten";
    private static final String READ_THEN_WRITTEN = "readthenwritten";

    private static final String DERIVATIVE = "derivative";
    private static final String DEPENDENCIES = "deps";
    /**
     * code de la fonction a inliner
     */
    private static final String INLINE = "inline";
    /**
     * le resultat d'une fonction
     */
    private static final String RESULT = "result";
    /**
     * les commons  ( specifique au langage fortran )
     */
    private static final String COMMON = "common";
    /**
     * les globales ( specifique au langage C )
     */
    private static final String GLOBAL = "global";
    /**
     * les parametres
     */
    private static final String PARAM = "param";
    private static final String DESTOFPARAM = "destofparam";
    /**
     * compteur des subroutines ou fonctions lues sans que l'utilisateur leur ait
     * donne un nom
     */
    private int icountSubWithNoNameDefined;
    /**
     * nombre d'elements de l'externalShape
     */
    private int nbArgShape;
    /**
     * nom de la subroutine ou fonction ou constante
     */
    private String name;
    /**
     * Drapeau de decodage d'une function
     */
    private boolean decodedFunction;
    /**
     * Drapeau de decodage d'une subroutine
     */
    private boolean decodedSubroutine;
    /**
     * Drapeau de decodage d'une  constante
     */
    private boolean decodedConst;
    /**
     * Drapeau de decodage d'une fonction intrinsic:
     * on considere par defaut que c'est une subroutine external_or_intrinsic
     */
    private boolean decodedIntrinsic;
    private boolean decodedExternal;
    /**
     * Drapeau de decodage d'une externalShape
     */
    private boolean decodedShape;
    /**
     * Drapeau de decodage des types des parametres de la subroutine
     * ou de la fonction ( au sens general: parametres formels + commons
     */
    private boolean decodedType;
    /**
     * Drapeaux de decodage de l'in-out signature des parametres
     * de la subroutine ou de la fonction (y compris le parametre resultat
     * de la fonction):
     * -  decodedR  : decodage de l'information  parametres lus
     * -  decodedN : decodage de l'information  parametres non lus
     * -  decodedW  : decodage de l'information  parametres ecrits
     * -  decodedRW : decodage de l'information  parametres RW
     */
    private boolean decodedN;
    private boolean decodedR;
    private boolean decodedW;
    private boolean decodedRW;
    private boolean decodedTBR;
    /**
     * Drapeau de decodage d'une fonction ou subroutine a inliner
     */
    private boolean decodedInline;
    /**
     * La Unit qu'on est en train de construire
     */
    private Unit unit;
    /**
     * numero de la zone correspondant a l'argument retour dans le cas d'une fonction
     */
    private int returnIndex;
    /**
     * Tableaux de l'in-out signature des parametres de la subroutine ou de la
     * fonction (y compris le parametre resultat de la fonction):
     */
    private BoolVector decodUnitN;
    private BoolVector decodUnitR;
    private BoolVector decodUnitW;
    private BoolVector decodUnitRW;
    private BoolVector decodUnitTBR ;
    /**
     * nombre d'elements contenus dans les BoolVector decodUnitXX
     */
    private int nbArgN;
    private int nbArgR;
    private int nbArgW;
    private int nbArgRW;
    private int nbArgTBR;
    /**
     * Liste d'arbres donnant le type des parametres
     */
    private TapList<Tree> decodType;
    /**
     * nombre d'elements de la liste decodType
     */
    private int nbArgType;
    /**
     * Liste de zoneInfo donnant l'externalshape
     */
    private TapList<ZoneInfo> decodShape;
    /**
     * si okToArchive on introduit la definition de l'external shape de la
     * fonction/subroutine dans le callGraph, ou la definition de la constante
     */
    private boolean okToArchive;
    private int vectorLength;
    private int libraryLanguage = TapEnv.UNDEFINED;

    /**
     * Creates a reader, and immediately reads the given "fileName".
     */
    public GeneralLibReader(String directory, String fileName,
                            CallGraph callGraph, int language) {
        super();
        synchronized (this) {
            try {
                libraryLanguage = language;
                TapEnv.setInputLanguage(language);
                String libDirectory = directory;
                if (!(TapEnv.isServlet() || libDirectory.isEmpty())) {
                    libDirectory = System.getProperty("tapenade_home") + File.separator + "resources"
                            + File.separator + directory;
                }
                File libFile = new File(libDirectory + fileName);
                LibReader libReader = null;
                if (libFile.exists()) {
                    libReader = new LibReader(
                            new BufferedReader(new FileReader(libDirectory + fileName)));
                    TapEnv.printlnOnTrace(15, "@@ Loading information from library "
                            + libDirectory + fileName + " ("
                            + TapEnv.languageName(language) + ')');
                } else if (!libDirectory.isEmpty()) {
                    // if resources/lib/fileName doesn't exist:
                    TapEnv.printlnOnTrace(15, "@@ Loading information from tapenade.jar"
                            + File.separator + directory + fileName + " ("
                            + TapEnv.languageName(language) + ')');
                    InputStream in = this.getClass().getResourceAsStream(File.separator + directory + fileName);
                    if (in != null) {
                        libReader = new LibReader(new BufferedReader(new InputStreamReader(in)));
                    } else {
                        TapEnv.systemError("(Reading library " + fileName + ") File not found");
                    }
                } else {
                    TapEnv.systemError("(Reading library " + fileName + ") File not found");
                }
                if (libReader != null) {
                    libReader.setLineCommentChar('#');
                    decodLibReader(directory, fileName, libReader, callGraph);
                }
            } catch (FileNotFoundException e) {
                TapEnv.systemError("(Reading library " + fileName + ") File not found");
            } catch (IOException e) {
                TapEnv.systemError("(Reading library " + fileName + ") I-O Error " + e.getMessage());
            }
        }
    }

    /**
     * Decode un fichier fileName qui contient la definition de
     * constantes et de subroutines (ou fonctions) externes ou intrinsics pouvant etre inlinees,
     * de facon a construire des constantes ou des units ( externalShape + FunctionTypeSpec ).
     */
    private final void decodLibReader(
            String directory, String fileName,
            LibReader libReader, CallGraph callGraph) throws IOException {
        TreeProtocol treeProtocol = new TreeProtocol(libReader);
        if (TapEnv.traceInputIL()) {
            treeProtocol.setTraceOn(true);
        }
        /*On lit le premier mot du fichier  fileName */
        int nextToken = libReader.readNextToken();
        String nextValue = libReader.sval;
        /*nom de la subroutine,de la fonction,de la constante en cours de definition */
        while (nextToken != -1) {
            if (nextValue.equals(FUNCTION) || nextValue.equals(SUBROUTINE)) {
                ToBool inlined = new ToBool(false);
                subOrFuncDefine(
                        directory + fileName, libReader, treeProtocol, nextValue,
                        callGraph, inlined);
            } else if (nextValue.equals(CONSTANT)) {
                constDefine(directory + fileName, libReader, treeProtocol);
            } else {
                TapEnv.systemWarning(14, "(Reading library " + fileName + ") unrecognized token:" + nextValue);
            }
            //On rale si la precedente fonction ou subroutine ou
            // constante n'a pas ete completement definie et on remplit les manques
            okToArchive &= decodedInline || allDefine(directory + fileName);
            if (okToArchive) {
                archive(fileName, callGraph);
            } else {
                if (unit != null) {
                    TapEnv.systemWarning(-1, "(Reading library " + fileName + ") Error while reading definition of " + name);
                    callGraph.deleteUnit(unit);
                    unit = null;
                }
            }
            decodShape = null;
            /*On retablit les logiques pour la suite */
            decodedIntrinsic = false;
            decodedExternal = false;
            decodedInline = false;
            decodedShape = false;
            decodedType = false;
            decodedN = false;
            decodedR = false;
            decodedW = false;
            decodedRW = false;
            decodedTBR = false;
            decodedFunction = false;
            decodedSubroutine = false;
            decodedConst = false;
            /*On avance dans le fichier  fileName */
            nextToken = libReader.readNextToken();
            nextValue = libReader.sval;
        }
    }

    /**
     * La methode subOrFuncDefine decode les mots-clefs qui servent a definir une subroutine
     * ou une fonction
     */
    private void subOrFuncDefine(
            String fileName, LibReader libReader,
            TreeProtocol treeProtocol, String subOrFunc, CallGraph callGraph,
            ToBool inlined) throws IOException {
        okToArchive = true;
        returnIndex = -1;
        nbArgN = 0;
        nbArgR = 0;
        nbArgW = 0;
        nbArgRW = 0;
        nbArgTBR = 0;
        nbArgType = 0;
        nbArgShape = 0;
        name = nameDefine(fileName, libReader, subOrFunc);
        int nextToken = libReader.seeNextToken();
        String nextValue = libReader.sval;
        FunctionDecl functionDecl;
        unit = Unit.makeExternalOrIntrinsic(name, callGraph, libraryLanguage);
        functionDecl = new FunctionDecl(ILUtils.build(ILLang.op_ident, name),
                unit, callGraph.languageRootSymbolTable(libraryLanguage));
        if (subOrFunc.equals(FUNCTION)) {
            decodedFunction = true;
        } else {
            decodedSubroutine = true;
        }
        while (nextToken != -1
                && !(nextValue.equals(FUNCTION) || nextValue.equals(SUBROUTINE)
                || nextValue.equals(CONSTANT))) {
            nextToken = libReader.readNextToken();
            nextValue = libReader.sval.toLowerCase() ;
            switch (nextValue) {
                case INTRINSIC:
                    decodedIntrinsic = true;
                    //Eat up a possible following ":"
                    nextToken = libReader.seeNextToken();
                    nextValue = libReader.sval;
                    if (":".equals(nextValue)) {
                        libReader.readNextToken();
                    }
                    break;
                case EXTERNAL:
                    decodedExternal = true;
                    //Eat up a possible following ":"
                    nextToken = libReader.seeNextToken();
                    nextValue = libReader.sval;
                    if (":".equals(nextValue)) {
                        libReader.readNextToken();
                    }
                    break;
                case SHAPE:
                    decodShape = shapeDecoding(libReader);
                    nbArgShape = TapList.length(decodShape);
                    decodedShape = true;
                    break;
                case TYPE:
                    decodType = typeDecoding(libReader, treeProtocol);
                    nbArgType = TapList.length(decodType);
                    decodedType = true;
                    break;
                case INLINE:
                    /*on lit eventuellement un ":" */
                    nextToken = libReader.seeNextToken();
                    if (nextToken == LibReader.COLON) {
                        libReader.readNextToken();
                    }
                    libReader.eliminateSpaces();
                    FlowGraphBuilder.build(treeProtocol, callGraph.languageRootSymbolTable(libraryLanguage),
                            callGraph, libraryLanguage, unit, true);
                    decodedInline = true;
                    inlined.set(true);
                    break;
                case NOT_READ_NOT_WRITTEN:
                case "notreadnorwritten":
                    decodUnitN = publicZoneVectorDecoding(libReader);
                    nbArgN = vectorLength;
                    decodedN = true;
                    break;
                case READ_NOT_WRITTEN:
                    decodUnitR = publicZoneVectorDecoding(libReader);
                    nbArgR = vectorLength;
                    decodedR = true;
                    break;
                case NOT_READ_THEN_WRITTEN:
                    decodUnitW = publicZoneVectorDecoding(libReader);
                    nbArgW = vectorLength;
                    decodedW = true;
                    break;
                case READ_THEN_WRITTEN:
                    decodUnitRW = publicZoneVectorDecoding(libReader);
                    nbArgRW = vectorLength;
                    decodedRW = true;
                    break;
                case "tbr":
                    decodUnitTBR = publicZoneVectorDecoding(libReader);
                    nbArgTBR = vectorLength;
                    decodedTBR = true;
                    break;
                case DEPENDENCIES:
                    // pop optional ":"
                    if (libReader.seeNextToken() == LibReader.COLON) {
                        libReader.readNextToken();
                    }
                    BoolMatrix depsDefinedByUser = boolMatrixDecoding(libReader);
                    if (okToArchive) {
                        unit.unitADDependencies = depsDefinedByUser;
                    } else {
                        TapEnv.systemWarning(-1, "(Reading A.D. library " + fileName + ") Error while reading dependence info on " + unit.name());
                    }
                    break;
                case DERIVATIVE:
                    // pop optional ":"
                    if (libReader.seeNextToken() == LibReader.COLON) {
                        libReader.readNextToken();
                    }
                    Tree jacobianInfo;
                    try {
                        jacobianInfo = treeProtocol.readPrefixTree();
                    } catch (IOException e) {
                        TapEnv.systemError("(Reading A.D. library " + fileName + ") Error " + e.getMessage() + " while reading derivative info on " + unit.name());
                        jacobianInfo = null;
                    }
                    if (jacobianInfo != null) {
                        AtomFuncDerivative.incorporateNewAtomFunc(jacobianInfo, unit);
                    }
                    break;
                default:
                    TapEnv.systemWarning(14, "(Reading library " + fileName + ") unrecognized keyword:" + nextValue);
            }
            nextToken = libReader.seeNextToken();
            nextValue = libReader.sval;
        }
    }

    /**
     * La methode typeDecoding decode les types des arguments d'une fonction ou subroutine.
     *
     * @return une liste d'arbres.
     */
    private TapList<Tree> typeDecoding(LibReader libReader, TreeProtocol treeProtocol)
            throws IOException {
        TapList<Tree> types = new TapList<>(null, null);
        TapList<Tree> typesTail = types;
        int nextToken = libReader.readNextToken();
        /*On lit juqu'a "(" */
        while (nextToken != -1 && nextToken != LibReader.OPENPARENTH) {
            nextToken = libReader.readNextToken();
        }
        nextToken = libReader.seeNextToken();
        /*On lit juqu'a ")" */
        while (nextToken != -1 && nextToken != LibReader.CLOSEPARENTH) {
            if (nextToken == LibReader.COMMA) {
                libReader.readNextToken();
                nextToken = libReader.seeNextToken();
            }
            typesTail = typesTail.placdl(treeProtocol.readPrefixTree());
            nextToken = libReader.seeNextToken();
        }
        libReader.readNextToken();
        return types.tail;
    }

    /**
     * Decode un ensemble de valeurs entre parentheses et separees
     * par des virgules.
     *
     * @return a BoolVector.
     */
    private BoolVector publicZoneVectorDecoding(LibReader libReader)
            throws IOException {
        TapIntList elements = new TapIntList(0, null);
        TapIntList elementsTail = elements;
        int nextToken = libReader.readNextToken();
        /*On lit juqu'a "(" */
        while (nextToken != -1 && nextToken != LibReader.OPENPARENTH) {
            nextToken = libReader.readNextToken();
        }
        nextToken = libReader.readNextToken();
        /*On lit juqu'a ")" */
        while (nextToken != -1 && nextToken != LibReader.CLOSEPARENTH) {
            if (nextToken == LibReader.COMMA) {
                nextToken = libReader.readNextToken();
            }
            elementsTail = elementsTail.placdl(Integer.parseInt(libReader.sval));
            nextToken = libReader.readNextToken();
        }
        elements = elements.tail;
        vectorLength = TapIntList.length(elements);
        BoolVector result = new BoolVector(vectorLength);
        int index = 0;
        while (elements != null) {
            result.set(index, elements.head != 0);
            index++;
            elements = elements.tail;
        }
        return result;
    }

    /**
     * La methode shapeDecoding decode l'externalShape d'une subroutine ou fonction :
     * nombre d'arguments, parametre de retour pour une fonction , et commons utilises
     */
    private TapList<ZoneInfo> shapeDecoding(LibReader libReader) throws IOException {
        int nbZoneParam = 0;
        TapList<ZoneInfo> llistShape = new TapList<>(null, null);
        TapList<ZoneInfo> llistShapeTail = llistShape;
        ZoneInfo zoneInfoParam;
        String nextValue;
        int startInterval = 0;
        int endInterval = 0;
        boolean infiniteEndOffset = false;
        int indexParam;
        int nextToken = libReader.readNextToken();
        /*On lit juqu'a "(" */
        while (nextToken != -1 && nextToken != LibReader.OPENPARENTH) {
            nextToken = libReader.readNextToken();
        }
        nextToken = libReader.readNextToken();
        /*On lit juqu'a ")" */
        while (nextToken != -1 && nextToken != LibReader.CLOSEPARENTH) {
            if (nextToken == LibReader.TOKEN) {
                nextValue = libReader.sval;
                switch (nextValue) {
                    case PARAM:
                        /*On lit le numero du parametre */
                        nextToken = libReader.readNextToken();
                        if (libReader.sval.equals("*")) {
                            /*Pour un nombre quelconque de parametres */
                            indexParam = -1;
                        } else {
                            indexParam = Integer.parseInt(libReader.sval);
                        }
                        zoneInfoParam = new ZoneInfo(-1, -1, false);
                        zoneInfoParam.index = indexParam;
                        zoneInfoParam.description = "arg#"+indexParam ;
                        zoneInfoParam.setKind(SymbolTableConstants.PARAMETER);
                        zoneInfoParam.declarationUnit = unit ;
                        break;
                    case DESTOFPARAM:
                        /*On lit le numero du parametre */
                        nextToken = libReader.readNextToken();
                        indexParam = Integer.parseInt(libReader.sval);
                        zoneInfoParam = new ZoneInfo(-1, -1, false);
                        zoneInfoParam.index = indexParam;
                        zoneInfoParam.description = "*arg#"+indexParam ;
                        zoneInfoParam.accessTree = ILUtils.build(ILLang.op_pointerAccess);
                        zoneInfoParam.setKind(SymbolTableConstants.PARAMETER);
                        zoneInfoParam.declarationUnit = unit ;
                        break;
                    case RESULT:
                        zoneInfoParam = new ZoneInfo(-1, -1, false);
                        zoneInfoParam.description = "result" ;
                        zoneInfoParam.setKind(SymbolTableConstants.RESULT);
                        zoneInfoParam.index = 0;
                        zoneInfoParam.declarationUnit = unit ;
                        returnIndex = nbZoneParam;
                        break;
                    case COMMON:
                        StringBuilder commonName = new StringBuilder();
                    /*le common blanc peut avoir deux formes :
                     common // a,b,c ou common a,b,c        */
                        nextToken = libReader.readNextToken();
                        while (nextToken != LibReader.OPENBRACKET) {
                            if (nextToken == LibReader.SLASH) {
                                commonName.append('/');
                            }
                            if (nextToken == LibReader.TOKEN) {
                                commonName.append(libReader.sval);
                            }
                            nextToken = libReader.readNextToken();
                        }
                        nextToken = libReader.readNextToken();
                        while (nextToken != LibReader.TOKEN) {
                            nextToken = libReader.readNextToken();
                        }
                        startInterval = Integer.parseInt(libReader.sval);
                        nextToken = libReader.readNextToken();
                        while (nextToken != LibReader.TOKEN) {
                            nextToken = libReader.readNextToken();
                        }
                        if (libReader.sval.equals("*")) {
                            endInterval = -1;
                            infiniteEndOffset = true;
                        } else {
                            endInterval = Integer.parseInt(libReader.sval);
                            infiniteEndOffset = false;
                        }
                        zoneInfoParam =
                                new ZoneInfo(startInterval, endInterval, infiniteEndOffset);
                        zoneInfoParam.setKind(SymbolTableConstants.GLOBAL);
                        zoneInfoParam.commonName = commonName.toString();
                        zoneInfoParam.description = zoneInfoParam.commonName+"["+startInterval+","+(infiniteEndOffset?"+inf":endInterval)+"[" ;
                        zoneInfoParam.declarationUnit = unit ;
                        break;
                    case GLOBAL:
                        nextToken = libReader.readNextToken();
                        while (nextToken != LibReader.TOKEN) {
                            nextToken = libReader.readNextToken();
                        }
                        String globalName = libReader.sval;
                        zoneInfoParam =
                                new ZoneInfo(startInterval, endInterval, infiniteEndOffset);
                        zoneInfoParam.setKind(SymbolTableConstants.GLOBAL);
                        zoneInfoParam.commonName = globalName;
                        zoneInfoParam.description = "global:"+globalName ;
                        zoneInfoParam.declarationUnit = unit ;
                        break;
                    default:
                        TapEnv.fileWarning(TapEnv.MSG_DEFAULT, 0, "(LB01) Error in library file: unknown keyword in shape: " + nextValue);
                        zoneInfoParam = null;
                        break;
                }
                if (zoneInfoParam != null) {
                    zoneInfoParam.zoneNb = nbZoneParam;
                }
                llistShapeTail = llistShapeTail.placdl(zoneInfoParam);
                ++nbZoneParam;
            }
            nextToken = libReader.readNextToken();
        }
        return llistShape.tail;
    }

    /**
     * La methode constDefine decode les mots-clefs qui servent a definir une constante
     */
    private void constDefine(
            String fileName, LibReader libReader,
            TreeProtocol treeProtocol) throws IOException {
        okToArchive = true;
        decodedConst = true;
        name = nameDefine(fileName, libReader, CONSTANT);
        int nextToken = libReader.seeNextToken();
        String nextValue = libReader.sval;
        while (nextToken != -1
                && !(nextValue.equals(FUNCTION) || nextValue.equals(SUBROUTINE)
                || nextValue.equals(CONSTANT))) {
            nextToken = libReader.readNextToken();
            nextValue = libReader.sval;
            if (nextValue.equals(TYPE)) {
                /*on lit eventuellement un ":" */
                nextToken = libReader.seeNextToken();
                if (nextToken == LibReader.COLON) {
                    libReader.readNextToken(); /*un seul element */
                }
                decodType = new TapList<>(treeProtocol.readPrefixTree(), null);
                decodedType = true; /*decodage */
            }
            nextToken = libReader.seeNextToken();
            nextValue = libReader.sval;
        }
    }

    /**
     * La methode nameDefine decode le nom de la subroutine ou de la fonction ou de la constante
     */
    private String nameDefine(
            String fileName, LibReader libReader,
            String subOrFuncOrConst) throws IOException {
        String functionName = null;
        int nextToken = libReader.readNextToken();
        while (nextToken != LibReader.COLON && functionName == null) {
            if (nextToken == LibReader.TOKEN) {
                functionName = libReader.sval;
            }
            nextToken = libReader.readNextToken();
        }
        if (functionName == null && !subOrFuncOrConst.equals(CONSTANT)) {
            // la fonction ou subroutine n'a pas de nom,
            // on lui en donne un par defaut
            okToArchive = false;
            functionName = "sub" + icountSubWithNoNameDefined;
            ++icountSubWithNoNameDefined;
            TapEnv.systemWarning(14, "(Reading library " + fileName + ") a " + subOrFuncOrConst + " has no name");
        }
        return functionName;
    }

    /**
     * La methode allDefine verifie que la subroutine ou la fonction ou la constante ont ete
     * completement decrite :
     * si c'est le cas elle rend un booleen a vrai,
     * sinon elle donne des defauts si c'est possible, et rend un booleen a vrai
     * et si ce n'est pas possible, elle rend alors un booleen a faux
     */
    private boolean allDefine(String fileName) {
        boolean ok = true;
        int ncountClefsManquantes = 0;
        String clefsManquantes = "";
        int ncountClefsAvecDefaut = 0;
        String clefsAvecDefaut = "";
        if (decodedConst && !decodedType) {
            clefsManquantes += " TYPE ";
            ncountClefsManquantes += 1;
            ok = false;
        }
        if (decodedFunction || decodedSubroutine) {
            if (!decodedShape) {
                clefsManquantes += " SHAPE ";
                ncountClefsManquantes += 1;
                ok = false;
            }
            if (decodedShape && decodedType && nbArgType != nbArgShape) {
                ok = false;
                TapEnv.systemWarning(14, "(Reading library " + fileName + ") Wrong number of values, TYPE: " + nbArgType + ", SHAPE: " + nbArgShape);
            }
            if (!decodedN) {
                clefsAvecDefaut += " N ";
                ncountClefsAvecDefaut += 1;
                if (ok) {
                    decodUnitN = new BoolVector(nbArgShape);
                    decodUnitN.setTrue();
                    nbArgN = nbArgType;
                }
            }
            if (!decodedR) {
                clefsAvecDefaut += " R ";
                ncountClefsAvecDefaut += 1;
                if (ok) {
                    decodUnitR = new BoolVector(nbArgShape);
                    decodUnitR.setTrue();
                    nbArgR = nbArgType;
                }
            }
            if (!decodedW) {
                clefsAvecDefaut += " W ";
                ncountClefsAvecDefaut += 1;
                if (ok) {
                    decodUnitW = new BoolVector(nbArgShape);
                    if (!decodedIntrinsic) {
                        decodUnitW.setFalse();
                    }
                    nbArgW = nbArgType;
                }
            }
            if (!decodedRW) {
                clefsAvecDefaut += " RW ";
                ncountClefsAvecDefaut += 1;
                if (ok) {
                    decodUnitRW = new BoolVector(nbArgShape);
                    decodUnitRW.setFalse();
                    nbArgRW = nbArgType;
                }
            }
            if (!decodedTBR) {
                clefsAvecDefaut += " TBR ";
                ncountClefsAvecDefaut += 1;
                if (ok) {
                    decodUnitTBR = new BoolVector(nbArgShape);
                    decodUnitTBR.setTrue();
                    nbArgTBR = nbArgType;
                }
            }
            if (ok && returnIndex != -1) {
                decodUnitN.set(returnIndex, false);
                decodUnitR.set(returnIndex, false);
                decodUnitW.set(returnIndex, true);
                decodUnitRW.set(returnIndex, false);
                decodUnitTBR.set(returnIndex, false);
            }
        }
        if (ncountClefsManquantes > 0) {
            String information = "";
            if (ncountClefsManquantes != 1) {
                information += "s :";
            }
            information += clefsManquantes;
            if (decodedFunction) {
                information += " in function ";
            }
            if (decodedSubroutine) {
                information += " in subroutine ";
            }
            if (decodedConst) {
                information += " in constant ";
            }
            information += name;
            /*Il manque des informations: on envoie un message a l'utilisateur */
            TapEnv.systemWarning(14, "(Reading library " + fileName + ") Missing key" + information);
        }
        if (returnIndex == -1 ? decodedFunction : decodedSubroutine) {
            if (returnIndex == -1) {
                TapEnv.systemWarning(14, "(Reading library " + fileName + ") Missing return value in function " + name);
            } else {
                TapEnv.systemWarning(14, "(Reading library " + fileName + ") Return value in subroutine " + name);
            }
        }
        if (ok && (decodedFunction || decodedSubroutine)
                && (nbArgN != nbArgType || nbArgR != nbArgType
                || nbArgW != nbArgType || nbArgRW != nbArgType)) {
            ok = false;
            TapEnv.systemWarning(14, "(Reading library " + fileName + ") Incomplete information for arrays N, R, W and RW, " + nbArgType + " values are needed");
        }
        return ok;
    }

    /**
     * la methode archive construit a partir des valeurs decodees la representation d'une
     * constante ou d'une subroutine: si on a decode une constante auparavant elle construit le
     * symbolDecl de la constante et le range dans la symbolTable du callGraph, dans le cas
     * d'une subroutine ou fonction que l'on ne doit pas inliner la methode archive definit
     * l'externalShape et le FunctionTypeSpec et pour une subroutine a inliner elle l'ajoute
     * a la liste des routines a inliner.
     */
    private void archive(String fileName, CallGraph callGraph) {
        SymbolTable symbolTable = callGraph.languageRootSymbolTable(libraryLanguage);
        if (decodedConst) {
            TapList<String> toAccess = new TapList<>(null, null);
            ToBool isPointer = new ToBool(false);
            WrapperTypeSpec argType =
                    TypeSpec.build(decodType.head, symbolTable, new Instruction(decodType.head),
                            new TapList<>(null, null), new TapList<>(null, null),
                            toAccess, isPointer, null);
            SymbolDecl symbolDecl =
                    SymbolDecl.build(
                            argType, toAccess.tail, isPointer.get(),
                            ILUtils.build(ILLang.op_ident, name),
                            SymbolTableConstants.CONSTANT, symbolTable, null, null);
            symbolDecl.addExtraInfo(new TapList<String>("extern", null)) ;
            symbolTable.addSymbolDecl(symbolDecl);
            TapEnv.printlnOnTrace(20, "@@ Added info on constant " + symbolDecl.symbol);
        } else if (decodedFunction || decodedSubroutine) {
            if (decodedIntrinsic) {
                unit.turnIntrinsic();
            }
            if (decodedExternal) {
                unit.turnExternal();
            }
            TapEnv.get().inlinedFunctions.addLibraryUnit(unit) ;
            if (decodedInline) {
                unit.isMadeForInline = true;
                unit.fixImplicits();
                unit.exportFunctionTypeSpec();
            } else {
                // Build externalShape and functionTypeSpec.
                //  The two must share their WrapperTypeSpec's !!
                int n = TapList.length(decodShape);
                int nZoneInt = 0;
                int nZoneReal = 0;
                int nZonePtr = 0;
                WrapperTypeSpec[] paramTypes = new WrapperTypeSpec[n];
                WrapperTypeSpec resultType = new WrapperTypeSpec(new VoidTypeSpec());
                int nbParams = 0;
                ZoneInfo[] externalShape = new ZoneInfo[n];
                ZoneInfo zoneInfo ;
                WrapperTypeSpec argType;
                unit.globalZonesNumber4 = new int[]{0,0,0,0} ;
                for (int i = 0; i < n; i++) {
                    zoneInfo = decodShape.head ;
                    externalShape[i] = zoneInfo ;
                    if (decodType == null) {
                        argType = new WrapperTypeSpec(null);
                    } else {
                        ToBool isPointer = new ToBool(false);
                        argType = TypeSpec.build(decodType.head, symbolTable, new Instruction(decodType.head),
                                new TapList<>(null, null), new TapList<>(null, null),
                                new TapList<>(null, null), isPointer, null);
                        if (isPointer.get()) {
                            argType = new WrapperTypeSpec(new PointerTypeSpec(argType, null));
                        }
                        decodType = decodType.tail;
                    }

if (zoneInfo.commonName!=null) {
    MemMap commonMap = callGraph.globalFortranMaps.getSetMemMap(zoneInfo.commonName) ;
    // Ad-hoc mechanism to pass this zoneInfo to MemMap.allocateZones(), so that this zoneInfo receives its final zone numbers:
    // NOTE: this mechanism is very limited anyway (only for COMMON shape elements in "lib" files) and should be totally rewritten!
    Tree dummyIdentTree = ILUtils.build(ILLang.op_ident, "dummyVar") ;
    dummyIdentTree.setAnnotation("zoneInfo", zoneInfo) ;
    VariableDecl varDecl = new VariableDecl(dummyIdentTree, argType) ;
    commonMap.insertVariableAt(zoneInfo.startOffset, (zoneInfo.infiniteEndOffset?0:zoneInfo.endOffset-zoneInfo.startOffset),
                               varDecl, null, callGraph.globalFortranMaps) ;
}

                    if (zoneInfo.isParameter()) {
                        int index = zoneInfo.index;
                        paramTypes[index - 1] = argType;
                        if (index > nbParams) {
                            nbParams = index;
                        }
                    } else if (zoneInfo.isResult()) {
                        resultType = argType;
                    }
                    ToObject<ModifiedTypeSpec> toModifiedType = new ToObject<>(null);
                    argType = TypeSpec.peelSizeModifier(argType, toModifiedType);

                    // When we encounter a type for "destofparam N",
                    // this implies the type of arg#N must be a pointer to that:
                    if (zoneInfo.isParameter() && zoneInfo.accessTree!=null &&
                            zoneInfo.accessTree.opCode() == ILLang.op_pointerAccess) {
                        int index = zoneInfo.index;
                        paramTypes[index - 1] = new WrapperTypeSpec(new PointerTypeSpec(paramTypes[index - 1], null));
                    }
                    zoneInfo.type = argType;
                    zoneInfo.typeSizeModifier =
                            toModifiedType.obj() == null ? -1 : toModifiedType.obj().sizeModifierValue();
                    boolean isInt = false;
                    boolean isReal = false;
                    boolean isPtr = false;
                    if (TypeSpec.isA(argType.wrappedType, SymbolTableConstants.PRIMITIVETYPE)) {
                        String baseTypeName = ((PrimitiveTypeSpec) argType.wrappedType).name();
                        if (baseTypeName.equals("integer")) {
                            isReal = false;
                            isInt = true;
                        } else if (baseTypeName.equals("float") || baseTypeName.equals("complex")) {
                            isReal = true;
                            isInt = false;
                        }
                    } else if (TypeSpec.isA(argType.wrappedType, SymbolTableConstants.POINTERTYPE)) {
                        isPtr = true ;
                    } else if (TypeSpec.isA(argType.wrappedType, SymbolTableConstants.METATYPE)) {
                        String baseTypeName = ((MetaTypeSpec) argType.wrappedType).name;
                        if (baseTypeName.startsWith("float") || baseTypeName.startsWith("complex")) {
                            isReal = true;
                            isInt = false;
                        } else {
                            isReal = true;
                            isInt = true;
                        }
                    } else if (argType.wrappedType == null) {
                        isReal = true;
                        isInt = true;
                    } else {
                        isReal = false;
                        isInt = false;
                    }
                    if (isInt) {
                        zoneInfo.intZoneNb = nZoneInt;
                        nZoneInt++;
                    }
                    if (isReal) {
                        zoneInfo.realZoneNb = nZoneReal;
                        nZoneReal++;
                    }
                    if (isPtr) {
                        zoneInfo.ptrZoneNb = nZonePtr;
                        nZonePtr++;
                    }
                    // unclean patch: we require that all global zones appear first,
                    // so that they get the first zone numbers, so that we can emulate
                    // a global SymbolTable below the formal params SymbolTable.
                    if (zoneInfo.isGlobal()) {
                        if (zoneInfo.zoneNb >= unit.globalZonesNumber4[0]) {
                            unit.globalZonesNumber4[0] = 1+zoneInfo.zoneNb ;
                            unit.globalZonesNumber4[1] = 1+zoneInfo.realZoneNb ;
                            unit.globalZonesNumber4[2] = 1+zoneInfo.intZoneNb ;
                            unit.globalZonesNumber4[3] = 1+zoneInfo.ptrZoneNb ;
                        }
                    }
                    decodShape = decodShape.tail;
                }
                WrapperTypeSpec[] finalParamTypes = new WrapperTypeSpec[nbParams];
                System.arraycopy(paramTypes, 0, finalParamTypes, 0, nbParams);
                unit.setFunctionTypeSpec(new FunctionTypeSpec(resultType, finalParamTypes));
                unit.externalShape = externalShape;
                unit.globalZonesNumber4 = new int[]{0,0,0,0} ; //Bizarre. Can't there be a few globals?
                unit.publicZonesNumber4 = new int[]{n, nZoneReal, nZoneInt, nZonePtr} ;
                unit.unitInOutN = decodUnitN;
                unit.unitInOutR = decodUnitR;
                unit.unitInOutW = decodUnitW;
                unit.unitInOutRW = decodUnitRW;
            }
            FunctionDecl functionDecl =
                    new FunctionDecl(
                            ILUtils.build(ILLang.op_ident, name), unit,
                            symbolTable);
            TapList<FunctionDecl> prevFunctionDecls =
                    symbolTable.getFunctionDecl(name, functionDecl.returnTypeSpec(),
                            functionDecl.argumentsTypesSpec(), false);
            FunctionDecl prevFunctionDecl = prevFunctionDecls == null ? null : prevFunctionDecls.head;

            // If there is already a FunctionDecl for "name", with the same argument types:
            // then: warning duplicate decl ; merge the two ; don't add the new one !
            // (except if the existing is an empty preliminary declaration)
            if (prevFunctionDecl != null
                && prevFunctionDecl.unit() != unit
                && !(prevFunctionDecl.isIntrinsic()
                     && WrapperTypeSpec.isFree(prevFunctionDecl.functionTypeSpec().returnType)
                     && prevFunctionDecl.functionTypeSpec().argumentsTypes == null)
                && prevFunctionDecl.sameTypes(unit.functionTypeSpec().returnType,
                                              unit.functionTypeSpec().argumentsTypes)
                && !prevFunctionDecl.isExternal()
                && prevFunctionDecl.unit().language() == unit.language()) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DD11) Duplicate declaration of function " + name + " in library file " + fileName);
                if (prevFunctionDecl.unit().diffInfo == null) {
                    prevFunctionDecl.unit().diffInfo = unit.diffInfo;
                }
                if (!prevFunctionDecl.unit().isMadeForInline && unit.isMadeForInline) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DD11) Inline definition of " + name + " may be lost");
                }
                if (prevFunctionDecl.unit().unitADDependencies == null) {
                    prevFunctionDecl.unit().unitADDependencies = unit.unitADDependencies;
                }
                if (prevFunctionDecl.unit().unitInOutN == null) {
                    prevFunctionDecl.unit().unitInOutN = unit.unitInOutN;
                }
                if (prevFunctionDecl.unit().unitInOutR == null) {
                    prevFunctionDecl.unit().unitInOutR = unit.unitInOutR;
                }
                if (prevFunctionDecl.unit().unitInOutW == null) {
                    prevFunctionDecl.unit().unitInOutW = unit.unitInOutW;
                }
                if (prevFunctionDecl.unit().unitInOutRW == null) {
                    prevFunctionDecl.unit().unitInOutRW = unit.unitInOutRW;
                }
                unit = null;
            } else {
                symbolTable.addSymbolDecl(functionDecl);
                TapEnv.printlnOnTrace(20, "@@ Added info on procedure " + functionDecl.symbol);
            }
        } else {
            TapEnv.systemWarning(14, "(Reading library " + fileName + ") lost part of lib file. name:" + name);
        }
    }

    /**
     * Decode un ensemble de valeurs entre parentheses et separees
     * par des virgules et les rend sous la forme d'une BoolMatrix (avec la convention
     * que lorsqu'on decode id la ligne correspondante de la matrice est nulle).
     */
    private BoolMatrix boolMatrixDecoding(LibReader libReader)
            throws IOException {
        int nextToken = libReader.readNextToken();
        /*On lit juqu'a "(" */
        while (nextToken != -1 && nextToken != LibReader.OPENPARENTH) {
            nextToken = libReader.readNextToken();
        }
        nextToken = libReader.readNextToken();
        /*On lit juqu'a ")" */
        BoolMatrix result = new BoolMatrix(nbArgShape, nbArgShape);
        result.setIdentity();
        int line = 0;
        int column = 0;
        BoolVector tmpRow = null;
        while (nextToken != -1 && nextToken != LibReader.CLOSEPARENTH) {
            if (nextToken == LibReader.COMMA) {
                nextToken = libReader.readNextToken();
            }
            if (libReader.sval.equalsIgnoreCase("id")) {
                line++;
                column = 0;
            } else {
                if (tmpRow == null) {
                    tmpRow = new BoolVector(nbArgShape);
                }
                if (!libReader.sval.equals("0")) {
                    tmpRow.set(column, true);
                }
                column++;
                if (column == nbArgShape) {
                    if (line < nbArgShape) {
                        result.setRow(line, tmpRow);
                    }
                    line++;
                    tmpRow = null;
                    column = 0;
                }
            }
            nextToken = libReader.readNextToken();
        }
        if (line == nbArgShape && column == 0) {
            return result;
        } else {
            // On rale si on a pas le bon nombre de valeurs
            okToArchive = false;
            return null;
        }
    }
}
