/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.frontend;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapEnvForThread;
import fr.inria.tapenade.representation.TapList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Parser calls external parsers for Fortran, C
 * in a separate process.
 */
public final class Parser {

    /**
     * Empty constructor.
     */
    private Parser() {
    }

    /**
     * Calls external parser for Fortran, C or C++.
     *
     * @param ppCommands  preprocessing command or null
     * @param commands    parsing command
     * @param file        parsed file
     * @return BufferedReader with IL output
     * @throws IOException if an input error is detected
     */
    public static synchronized BufferedReader parse(
            final String[] ppCommands,
            String[] commands, final String file) throws IOException {
        File inputFile = new File(file);
        if (!inputFile.exists()) {
            throw new FileNotFoundException((TapEnv.isServlet() ? inputFile.getName() : file)
                    + " (No such file or directory)");
        }
        TapEnv.printlnOnTrace(15, "@@ Parsing " + file);
        TapEnv.setCurrentParsedFile(file);
        Runtime rt = Runtime.getRuntime();
        // In new Threads below, we want to share the thread-specific part of TapEnv:
        final TapEnvForThread tapEnvForThread = TapEnv.get();
        if (ppCommands != null) {
            if (tapEnvForThread.traceParser) {
                TapEnv.printOnTrace("PREPROCESSING COMMAND=");
                for (String ppCommand : ppCommands) {
                    TapEnv.printOnTrace(" " + ppCommand);
                }
                TapEnv.printlnOnTrace();
            }
            try {
                final Process preproc = rt.exec(ppCommands);
                final BufferedReader errorPreproc =
                        new BufferedReader(
                                new InputStreamReader(preproc.getErrorStream()));
                new Thread(() -> {
                    String line;
                    try {
                        while ((line = errorPreproc.readLine()) != null) {
                            tapEnvForThread.parserError(line);
                            tapEnvForThread.flushOutputStream();
                        }
                    } catch (IOException exc) {
                        tapEnvForThread.parserError("Parsing interrupted " + file);
                    }
                }).start();
                int returnValue;
                try {
                    returnValue = preproc.waitFor();
                    if (returnValue != 0) {
                        TapEnv.parserError(ppCommands[0] + " preprocessor error, return value " + returnValue);
                    }
                } catch (InterruptedException ignored) {
                    Thread.currentThread().interrupt();
                }
                preproc.getOutputStream().close();
            } catch (IOException e) {
                TapEnv.parserError(ppCommands[0] + " command not found");
            }

            if (TapEnv.relatedLanguageIsC()) {
                int indexOfPreprocessedFileName = 4;
                File preprocessedFile = new File(commands[indexOfPreprocessedFileName]);
                if (!preprocessedFile.exists()) {
                    TapEnv.parserError(ppCommands[0] + " preprocessor error, temporary file not created: " + preprocessedFile);
                    TapEnv.parserError(ppCommands[0] + " try recover by parsing directly: " + file);
                    commands[indexOfPreprocessedFileName] = file;
                }
            } else if (TapEnv.relatedLanguageIsCPLUSPLUS()) {
                File jsonFile = new File("o.json");
                File cPlusPlusParserFile = new File(commands[0]);
                if (!cPlusPlusParserFile.exists()) {
                    TapEnv.toolError("No parser for C++");
                    return null;
                } else if (!jsonFile.exists()) {
                    TapEnv.parserError(ppCommands[0] + " parser warning, compile_commands.json not found");
                } else {
                    try (BufferedReader input = new BufferedReader(new FileReader(jsonFile))) {
                        modifyJsonFile(ppCommands, input);
                    }
                }
            }
        }
        if (tapEnvForThread.traceParser) {
            TapEnv.printOnTrace("PARSING COMMAND=");
            for (String command : commands) {
                TapEnv.printOnTrace(" " + command);
            }
            TapEnv.printlnOnTrace();
        }
        final Process p = rt.exec(commands);
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        final BufferedReader errorReader =
                new BufferedReader(
                        new InputStreamReader(p.getErrorStream()));

        new Thread(() -> {
            String line;
            try {
                while ((line = errorReader.readLine()) != null) {
                    tapEnvForThread.parserError(line);
                    tapEnvForThread.flushOutputStream();
                }
            } catch (IOException ignored) {
            }
        }).start();

        new Thread(() -> {
            try {
                p.waitFor();
            } catch (InterruptedException e) {
                tapEnvForThread.parserError("Parsing interrupted " + file);
                Thread.currentThread().interrupt();
            }
        }).start();
        p.getOutputStream().close();
        return reader;
    }

    /**
     * Modify compile_commands.json file created during preprocess
     * with ppCommands.
     *
     * @param ppCommands {@code clang++ -MJ o.json -c }
     * @param input       o.json file
     */
    private static void modifyJsonFile(String[] ppCommands,
                                       BufferedReader input) {
        File compileJsonFile = new File("compile_commands.json");
        if (compileJsonFile.exists()
                && !compileJsonFile.renameTo(new File("compile_commands.json" + "~"))) {
            TapEnv.parserError("Parsing interrupted compile_commands.json~");
        }
        try (OutputStreamWriter toFile = new FileWriter("compile_commands.json")) {
            int nbLine = ppCommands.length - TapEnv.CPLUSPLUS_PARSER_NB_ARGS;
            String line;
            String firstLine;
            String lastLine;
            firstLine = input.readLine();
            if (nbLine == 1) {
                firstLine = firstLine.replace("{", "[{");
                firstLine = firstLine.replace("},", "}]");
                toFile.write(firstLine);
            } else if (nbLine == 2) {
                firstLine = firstLine.replace("{", "[{");
                toFile.write(firstLine);
                toFile.write(System.lineSeparator());
                lastLine = input.readLine();
                lastLine = lastLine.replace("},", "}]");
                toFile.write(lastLine);
            } else {
                firstLine = firstLine.replace("{", "[{");
                toFile.write(firstLine);
                toFile.write(System.lineSeparator());
                for (int i = 2; i < nbLine; i++) {
                    line = input.readLine();
                    toFile.write(line);
                    toFile.write(System.lineSeparator());
                }
                lastLine = input.readLine();
                lastLine = lastLine.replace("},", "}]");
                toFile.write(lastLine);
            }
            toFile.write(System.lineSeparator());
        } catch (IOException e) {
            TapEnv.parserError("Parsing interrupted compile_commands.json");
        }
    }

    /**
     * Clean temporary files created during parsing of C++ files.
     *
     * @param fileNames C++ parsed file names.
     */
    public static void cleanJsonFiles(TapList<String> fileNames) {
        File jsonFile = new File("o.json");
        if (jsonFile.exists() && !jsonFile.delete()) {
            TapEnv.parserError("Cannot remove o.json");
        }
        jsonFile = new File("compile_commands.json");
        if (jsonFile.exists() && !jsonFile.delete()) {
            TapEnv.parserError("Cannot remove compile_commands.json");
        }
        jsonFile = new File("compile_commands.json~");
        if (jsonFile.exists() && !jsonFile.renameTo(new File("compile_commands.json"))) {
            TapEnv.parserError("Cannot rename compile_commands.json");
        }
        String fileName;
        while (fileNames != null) {
            fileName = TapEnv.stripLanguageExtension(TapEnv.stripPath(fileNames.head)) + ".o";
            jsonFile = new File(fileName);
            if (jsonFile.exists() && !jsonFile.delete()) {
                TapEnv.parserError("Cannot remove " + fileName);
            }
            fileNames = fileNames.tail;
        }
    }
}
