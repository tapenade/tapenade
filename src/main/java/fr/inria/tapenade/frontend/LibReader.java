/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.frontend;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * A partir d'un buffer reader cette classe permet de lire des mots
 * separes par des virgules, des parentheses ouvrantes, des parentheses
 * fermantes, des crochets ouvrants, des deux points, des slashs et en eliminant
 * les blancs, les caracteres de fin de ligne et les return.
 */
final class LibReader {

    protected static final int COLON = 1;
    protected static final int OPENPARENTH = 2;
    protected static final int COMMA = 3;
    protected static final int CLOSEPARENTH = 4;
    protected static final int TOKEN = 5;
    protected static final int OPENBRACKET = 6;
    protected static final int SLASH = 7;
    /**
     * Le buffer a lire.
     */
    protected final BufferedReader reader;
    /**
     * sval contient la chaine de caracteres qui vient d'etre lue.
     */
    protected String sval;
    private boolean hasLineCommentChar;
    private char lineCommentChar = '#';
    private int status = -1;

    protected LibReader(BufferedReader reader) {
        super();
        this.reader = reader;
    }

    /**
     * When hasLineCommentChar, this LibReader will ignore all text between a "lineCommentChar" and the next newline.
     */
    protected void setLineCommentChar(char lineCommentChar) {
        hasLineCommentChar = true;
        this.lineCommentChar = lineCommentChar;
    }

    /**
     * Lit des caracteres jusqu'a qu'ils soient differents de
     * ' ',  Tab, '\n', '\r' et retourne sous la forme d'un entier
     * l'information que le dernier caractere lu est :
     * ':' --&gt; int COLON
     * ',' --&gt; int COMMA
     * '(' --&gt; int OPENPARENTH
     * ')' --&gt; int CLOSEPARENTH
     * '[' --&gt; int OPENBRACKET
     * '/' --&gt; int SLASH
     * si le dernier caractere lu est different de ':',',','(',')','['
     * et de ' ',  Tab, '\n', '\r'
     * il construit une chaine de carateres dont le premier caractere
     * est ce dernier caractere lu,et il complete la chaine de caracteres
     * en lisant le buffer jusqu'a qu'il rencontre ':',',','(',')','['
     * ou  ' ',  Tab, '\n', '\r'. Dans ce cas il retourne un entier egal
     * a la constante TOKEN :
     * chaine de caracteres  --&gt; int TOKEN
     * <p>
     * et sval contiendra la chaine de caracteres correspondante.
     */
    private int popNextToken() throws IOException {
        int c = reader.read();
        char nextChar = (char) c;
        StringBuilder sbuf = new StringBuilder(256);
        boolean inComment = false;
        while (inComment || hasLineCommentChar && nextChar == lineCommentChar
                || nextChar == ' ' || nextChar == 9 || nextChar == '\n' || nextChar == '\r') {
            if (hasLineCommentChar && nextChar == lineCommentChar) {
                inComment = true;
            } else if (nextChar == '\n' || nextChar == '\r') {
                inComment = false;
            }
            c = reader.read();
            nextChar = (char) c;
        }
        switch (nextChar) {
            case ':':
                sval = ":";
                return COLON;
            case '(':
                sval = "(";
                return OPENPARENTH;
            case ',':
                sval = ",";
                return COMMA;
            case ')':
                sval = ")";
                return CLOSEPARENTH;
            case '[':
                sval = "[";
                return OPENBRACKET;
            case '/':
                sval = "/";
                return SLASH;
            default:
                if (c == -1) {
                    sval = "";
                    return -1;
                } else {
                    sbuf.setLength(0);
                    sbuf.append(nextChar);
                    reader.mark(2);
                    c = reader.read();
                    nextChar = (char) c;
                    while (c != -1 && nextChar != ' '
                            && nextChar != '(' && nextChar != ')'
                            && nextChar != ',' && nextChar != ':' && nextChar != '['
                            && nextChar != '/'
                            && nextChar != 9 && nextChar != '\n' && nextChar != '\r'
                            && !(hasLineCommentChar && nextChar == lineCommentChar)) {
                        sbuf.append(nextChar);
                        reader.mark(2);
                        c = reader.read();
                        nextChar = (char) c;
                    }
                    reader.reset();
                    sval = sbuf.toString();
                    return TOKEN;
                }
        }
    }

    protected int seeNextToken() throws IOException {
        if (status == -1) {
            status = popNextToken();
        }
        return status;
    }

    protected int readNextToken() throws IOException {
        if (status != -1) {
            int result = status;
            status = -1;
            return result;
        } else {
            return popNextToken();
        }
    }

    /**
     * Lit des caracteres jusqu'a qu'ils soient differents de
     * ' ',  Tab, '\n', '\r' et se repositionne juste avant le
     * premier caractere rencontre different de ' ',  Tab, '\n', '\r'.
     */
    protected void eliminateSpaces() throws IOException {
        char nextChar;
        reader.mark(2);
        nextChar = (char) reader.read();
        boolean inComment = false;
        while (inComment || hasLineCommentChar && nextChar == lineCommentChar
                || nextChar == ' ' || nextChar == 9 || nextChar == '\n' || nextChar == '\r') {
            if (hasLineCommentChar && nextChar == lineCommentChar) {
                inComment = true;
            } else if (nextChar == '\n' || nextChar == '\r') {
                inComment = false;
            }
            reader.mark(2);
            nextChar = (char) reader.read();
        }
        reader.reset();
    }
}
