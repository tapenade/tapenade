Imperative Language Abstract Syntax Trees - <span style="color:red">**[State 0]**</span>
========================================================================================

In this state, we have the IL trees of all the input files, provided in
the form of serialized Abstract Syntax Trees. Polish pre-order notation:
Operator first, then sub-trees. Operators are encoded as small integers.
They could be encoded as single bytes, as there are only 192 of them at
present. List-arity operators have a special marker for end-of-list.
Fixed-arity operators do not need/have a marker for end-of-list. Leaf
operators are followed by their \"value\", either an integer or a
String.

The IL language (Imperative Language) is a union of the constructs
encountered in the various input languages that Tapenade accepts. IL may
grow with new constructs as needed when extending Tapenade to new
languages. At present it holds 192 operators. IL has no concrete syntax,
to emphasize the fact that it is an abstract language that is not bound
to anyone's concrete syntax, be it Fortran, C, or others.

IL only has an abstract syntax (a collection of operators, each one
specifying its expected sub-trees). Each sub-tree of a given operator is
specified by a \"phylum\", which is the set of acceptable head operators
for this sub-tree.

Obviously, IL also has a semantics, which is in general clear from the
operator's definition and from the construct they originate from in an
actual language. However, when some IL constructs have to define their
semantics more precisely, this must be written down in the
[syntaxIL.md](../../../../../../../docs/syntaxIL.md) description.
