/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

/**
 * Tapenade frontend.
 * Calls external parsers for Fortran, C in a separate process.
 * Translates the set of input files into their abstract syntax trees.
 * Reads information from Tapenade library files.
 *
 * @author Inria - Ecuador team, tapenade@inria.fr
 */
package fr.inria.tapenade.frontend;
