Tapenade parsers - <span style="color:green">**<Step -1>**</span>
=================================================================

* Files:  
`Parser TreeProtocol` 
calls language-specific parsers for Fortran, C or C++.
See corresponding README.md files in:
   - [src/frontf for Fortran Parser](../../../../../../frontf/README.md),
   - [src/frontc for C Parser](../../../../../../frontc/README.md),
   - [src/frontcpp C++ Parser](../../../../../../frontcpp/README.md).

Calls external parsers for Fortran, C in a separate process.
Translates the set of input files into their abstract syntax trees.

This calls different parsers according to the input language. We provide
one parser for Fortran (77, 90, and up), C, and now C++ (in progress).
In principle, we don't consider the parsers a part of Tapenade (which is
why we here call "**State 0**" the state *after* parsing). We only
provide them for convenience. One advantage is that we may use an
external parser without worrying about licensing issues. The only
official input "language" that Tapenade accepts is called "IL"
(Imperative Language, see below). Any parser (ours or others) must
produce output in the IL formalism, which will be fed into Tapenade. We
define and document this IL "language" 
(see [IL Reference Manual](../../../../../../../docs/syntaxIL.md)). 
IL is a union of the constructs encountered in the various accepted input
languages.

`Parser.parse(TapEnv tapEnv, String[] preProcessCommands, String[] commands, String fileName)`
takes as input the `TapEnv`, a list of `String` defining the system
command to call to trigger an optional preprocessor (e.g. `cpp`), a list
of `String` defining the system command to call to trigger the parser
itself, and again the name of the parsed file for use in the error
messages (is this 4th input really useful?). It returns a
`BufferedReader` stream that will provide the stream of IL operators.

Possible error messages sent by the parser command onto `stderr` are
collected into the `TapEnv`, to be attached later into the IL stream.
