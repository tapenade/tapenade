/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

final class PlainStruct extends ControlStruct {
    protected PlainStruct(Block block) {
        this.controlStructBlock = block;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
    }

    /**
     * (Recursive) Inserts Let structures into the tree inside.
     * There is no "inside" here!.
     */
    @Override
    protected void insertLetStructure() {
    }

    /**
     * (Recursive) Orders the lists of control structures inside.
     * There is no "inside" here!.
     */
    @Override
    protected void reorderBody() {
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        naturalFlow = controlStructBlock.flow();
        Instruction lastInstr = controlStructBlock.lastInstr();
        if (lastInstr != null) {
            Tree lastTree = lastInstr.tree;
            if (lastTree != null &&
                    lastTree.opCode() == ILLang.op_break) {
                naturalFlow = null;
            }
        }
        return naturalFlow;
    }

    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings, TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        TapList<Instruction> nextInstructions = controlStructBlock.instructions;
        TapList<FGArrow> natArrows = null;
        if (TapEnv.get().createStub) {
            // on ne garde que les declarations
            Instruction curInstruction;
            TapList<Instruction> declarationsInstructions = null;
            while (nextInstructions != null) {
                curInstruction = nextInstructions.head;
                if (curInstruction.isADeclaration()) {
                    declarationsInstructions = new TapList<>(curInstruction, declarationsInstructions);
                }
                nextInstructions = nextInstructions.tail;
            }
            nextInstructions = TapList.reverse(declarationsInstructions);
            if (nextInstructions != null) {
                preGenerateBody(nextInstructions, naturalArrows, toFutureIncludes, fileUserHelpStrings, skipSubUnits);
            }
        } else {
            Tree lastInstructionTree = preGenerateBody(nextInstructions, naturalArrows,
                    toFutureIncludes, fileUserHelpStrings, skipSubUnits);
            TapList<FGArrow> arrows = controlStructBlock.flow();
            if (lastInstructionTree != null &&
                    lastInstructionTree.opCode() == ILLang.op_break) {
                while (enclosingStructs != null &&
                        !(enclosingStructs.head instanceof SwitchStruct)) {
                    enclosingStructs = enclosingStructs.tail;
                }
                if (enclosingStructs != null) {
                    SwitchStruct switchStruct = (SwitchStruct) enclosingStructs.head;
                    switchStruct.naturalArrowsAfterSwitch =
                            TapList.append(switchStruct.naturalArrowsAfterSwitch, arrows);
                    arrows = null;
                }
            }
            FGArrow arrow;
            while (arrows != null) {
                arrow = arrows.head;
                if (arrow.mayBeNatural()) {
                    natArrows = new TapList<>(arrow, natArrows);
                }
                arrows = arrows.tail;
            }
        }
        return natArrows;
    }

    @Override
    protected void generateTree(boolean delayGoto,
                                TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        // Holds the FGArrow representing the default case out of current controlStructBlock.
        //  The destination of this FGArrow should be placed syntactically just after
        //  the current controlStructBlock :
        FGArrow defaultArrow;
        TapList<FGArrow> arrows = controlStructBlock.flow();
        int lastOperator = -1;
        Tree lastInstructionTree =
                treesReverse == null ? null : treesReverse.head;
        if (arrows != null) {
            if (lastInstructionTree != null &&
                    lastInstructionTree.opCode() == ILLang.op_labelStatement) {
                lastInstructionTree = lastInstructionTree.down(2);
            }
            if (lastInstructionTree != null) {
                lastOperator = lastInstructionTree.opCode();
            }
            switch (lastOperator) {
                case ILLang.op_compGoto:
                    assert lastInstructionTree != null;
                    checkCompGoto(lastInstructionTree);
                    defaultArrow = controlStructBlock.getFGArrowTestCase(FGConstants.COMP_GOTO, FGConstants.DEFAULT);
                    break;
                case ILLang.op_call:
                    if (arrows.head.test == FGConstants.CALL) {
                        /*if the call has a non-standard exit: */
                        checkCallGoto(lastInstructionTree);
                        defaultArrow = controlStructBlock.getFGArrowTestCase(FGConstants.CALL, FGConstants.DEFAULT);
                    } else {
                        defaultArrow = arrows.head;
                    }
                    break;
                case ILLang.op_ioCall:
                    if (arrows.head.test == FGConstants.IO_GOTO) {
                        /*if the ioCall has a non-standard exit: */
                        assert lastInstructionTree != null;
                        checkIoGoto(lastInstructionTree);
                        defaultArrow =
                                controlStructBlock.getFGArrowTestCase(FGConstants.IO_GOTO, FGConstants.DEFAULT);
                    } else {
                        defaultArrow = arrows.head;
                    }
                    break;
                case ILLang.op_gotoLabelVar:
                    checkGotoLabelVar();
                    defaultArrow = null;
                    break;
                default:
                    defaultArrow = arrows.head;
                    break;
            }
            if (defaultArrow != null && defaultArrow.carry != null && !delayGoto) {
                insertInstructionTree(turnCarryIntoJump(defaultArrow), false);
            }
        }
    }

    private void checkCompGoto(Tree instTree) {
        Tree[] compGotoLabels = instTree.down(1).children();
        FGArrow arrow;
        for (int i = compGotoLabels.length - 1; i >= 0; i--) {
            arrow = controlStructBlock.getFGArrowTestCase(FGConstants.COMP_GOTO, i + 1);
            compGotoLabels[i].setValue(getDestinationLabel(arrow));
        }
    }

    private void checkGotoLabelVar() {
        /*When regenerating a gotoLabelVar, we should check that
         * the destination label hasn't changed. Otherwise, we should
         * change the "assignLabelVar"'s accordingly.
         * BUT HERE we just suppose that the destination labels
         * have NOT changed !! So we check nothing, and
         * simply erase the labels carried on the flow arrows.*/
        TapList<FGArrow> arrows = controlStructBlock.flow();
        FGArrow arrow;
        while (arrows != null) {
            arrow = arrows.head;
            arrow.carry = null;
            arrows = arrows.tail;
        }
    }

    private void checkIoGoto(Tree instTree) {
        Tree[] ioStats = instTree.down(2).children();
        FGArrow arrow;
        for (int i = ioStats.length - 1; i >= 0; i--) {
            if (ioStats[i].opCode() == ILLang.op_nameEq) {
                String name = ioStats[i].down(1).stringValue();
                switch (name) {
                    case "end":
                        arrow = controlStructBlock.getFGArrowTestCase(FGConstants.IO_GOTO, FGConstants.IO_END);
                        break;
                    case "err":
                        arrow = controlStructBlock.getFGArrowTestCase(FGConstants.IO_GOTO, FGConstants.IO_ERR);
                        break;
                    case "eor":
                        arrow = controlStructBlock.getFGArrowTestCase(FGConstants.IO_GOTO, FGConstants.IO_EOR);
                        break;
                    default:
                        arrow = null;
                        break;
                }
                if (arrow != null) {
                    ioStats[i].setChild(
                            ILUtils.build(ILLang.op_label, getDestinationLabel(arrow)), 2);
                }
            }
        }
    }

    private void checkCallGoto(Tree instTree) {
        Tree[] callArgs = ILUtils.getArguments(instTree).children();
        FGArrow arrow;
        int labelRank = 0;
        for (Tree callArg : callArgs) {
            if (callArg.opCode() == ILLang.op_label) {
                labelRank++;
                arrow = controlStructBlock.getFGArrowTestCase(FGConstants.CALL, labelRank);
                callArg.setValue(getDestinationLabel(arrow));
            }
        }
    }

    private String getDestinationLabel(FGArrow arrow) {
        String label = null;
        if (arrow != null) {
            if (arrow.carry != null) {
                label = ((Tree) arrow.carry).stringValue();
            }
            if (label == null) {
                if (arrow.inACycle && arrow.destination instanceof HeaderBlock) {
                    label = ((HeaderBlock) arrow.destination).origCycleLabel();
                } else {
                    label = arrow.destination.origLabel();
                }
            }
            if (!arrow.containsCase(FGConstants.DEFAULT)) {
                arrow.carry = null;
            }
        }
        return label;
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("Plain ");
        super.dump(indent);
    }

    @Override
    public String toString() {
        return "PLAIN:" + controlStructBlock + " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions;
    }
}
