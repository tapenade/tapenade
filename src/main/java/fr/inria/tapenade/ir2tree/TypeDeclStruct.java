/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeDecl;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

/**
 * Tree regeneration of type declaration.
 */
final class TypeDeclStruct extends DeclStruct {
    private final TypeDecl typeDecl;

    protected TypeDeclStruct(TypeDecl typeDecl) {
        this.typeDecl = typeDecl;
        usedNames = typeDecl.dependsOn();
        kind = StructConstants.TYPE;
        symbolDecl = typeDecl;
    }

    protected TypeDecl typeDecl() {
        return typeDecl;
    }

    @Override
    protected Tree generateTree(TapList<SymbolDecl> declaredTypes, SymbolTable symbolTable) {
        Tree result = null;
        if (typeDecl.isATrueSymbolDecl) {
            TypeSpec declaredTypeValue = typeDecl.typeSpec;
            while (declaredTypeValue instanceof WrapperTypeSpec) {
                declaredTypeValue = ((WrapperTypeSpec) declaredTypeValue).wrappedType;
            }
            Tree genTree;
            if (declaredTypeValue == null) {
                TapEnv.toolWarning(-1, "(Type declaration regeneration) null type");
                genTree = ILUtils.build(ILLang.op_ident, "UnknownType");
            } else {
                genTree = declaredTypeValue.generateTree(symbolTable, typeDecl.dependsOn, declaredTypes, false, null);
            }
            if (isEmptyName(typeDecl, genTree)) {
                result = ILUtils.build(
                        ILLang.op_typeDeclaration,
                        ILUtils.build(ILLang.op_none),
                        genTree);
            } else {
                if (genTree.parent() != null) {
                    genTree = ILUtils.copy(genTree);
                }
                result = ILUtils.build(
                        ILLang.op_typeDeclaration,
                        ILUtils.build(ILLang.op_ident, typeDecl.symbol),
                        genTree);
                if (genTree.opCode() == ILLang.op_recordType
                        && genTree.down(1).opCode() == ILLang.op_none) {
                    genTree.setChild(ILUtils.build(ILLang.op_ident, typeDecl.symbol), 1);
                }
            }
            if (typeDecl.extraInfo() != null) {
                Tree accessDecl =
                        ILUtils.build(
                                ILLang.op_accessDecl,
                                ILUtils.build(
                                        ILLang.op_ident, typeDecl.extraInfo().head),
                                ILUtils.build(
                                        ILLang.op_expressions,
                                        ILUtils.build(ILLang.op_ident, typeDecl.symbol)));
                result = ILUtils.build(ILLang.op_declarations, accessDecl, result);
            }
            if (TypeSpec.isA(typeDecl.typeSpec, SymbolTableConstants.NAMEDTYPE)) {
                if (genTree.opCode() == ILLang.op_ident
                        && typeDecl.symbol.equals(genTree.stringValue())) {
                    result = null;
                }
            }
        }
        return result;
    }

    private boolean isEmptyName(TypeDecl typeDecl, Tree genTree) {
        boolean result;
        String operatorName = genTree.opName();
        result = typeDecl.symbol.startsWith(operatorName + "_");
        return result;
    }

    @Override
    public String toString() {
        return "declaration of type " + typeDecl.symbol + ':' + typeDecl.typeSpec;
    }
}
