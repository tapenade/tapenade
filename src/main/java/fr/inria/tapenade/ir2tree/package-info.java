/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

/**
 * Tapenade tree regeneration.
 * Translates the differentiated internal representation into new,
 * differentiated abstract syntax tree.
 *
 * @author Inria - Ecuador team, tapenade@inria.fr
 */
package fr.inria.tapenade.ir2tree;
