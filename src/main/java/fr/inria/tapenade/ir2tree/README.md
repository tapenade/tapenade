Creating the output IL Abstract Syntax Trees 
============================================

### <span style="color:green">**<Step 5>**</span>
* Files:

`TreeGen ControlStruct PlainStruct IfThenElseStruct LoopStruct SwitchStruct LetStruct NameSpaceStruct UnitStruct DeclStruct VariableDeclStruct FunctionDeclStruct InterfaceDeclStruct TypeDeclStruct`

```
      Tapenade.outputCallGraph()
      --> Tapenade.prepareFutureFile()
          --> TreeGen.generate()      
```

Missing: details on TreeGen

Differentiated IL ASTs ready - <span style="color:red">**[State 6]**</span>
---------------------------------------------------------------------------
