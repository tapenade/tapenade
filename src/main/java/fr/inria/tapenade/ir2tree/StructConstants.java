/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

/**
 * Constants used by the tree generation for internal representation.
 */
final class StructConstants {
    protected static final int USE = 0;
    protected static final int TYPE = 1;
    protected static final int CONSTANT = 2;
    protected static final int VARIABLE = 3;
    protected static final int COMMON = 4;
    protected static final int EQUIVALENCE = 5;
    protected static final int FUNCTION = 6;
    protected static final int INTERFACE = 7;

    private StructConstants() {
    }
}
