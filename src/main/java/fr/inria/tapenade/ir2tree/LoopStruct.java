/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * ControlStruct for explicit loop (do n ; do times; do while ; do until...).
 */
final class LoopStruct extends ControlStruct {
    private final String cycleLabel = null;
    private TapList<ControlStruct> body;
    private FGArrow loopArrow;
    private FGArrow exitArrow;

    protected LoopStruct(Block block) {
        this.controlStructBlock = block;
        TapList<FGArrow> arrows = block.flow();
        FGArrow arrow;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.test == FGConstants.LOOP) {
                TapIntList cases = arrow.cases;
                while (cases != null) {
                    if (cases.head == FGConstants.EXIT) {
                        exitArrow = arrow;
                    } else if (cases.head == FGConstants.NEXT) {
                        loopArrow = arrow;
                    }
                    cases = cases.tail;
                }
            }
            arrows = arrows.tail;
        }
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        body = new TapList<>(struct, body);
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        body = insertLetStructureChained(body, controlStructBlock.symbolTable);
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position (here: controlStructBlock ranks...).
     */
    @Override
    protected void reorderBody() {
        body = reorderBodyChained(body);
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        // On a loop with multiple exit destinations, there can be no naturalNext,
        // because our current choice is to place the "break" exits after the loop,
        // and only after place the "normal" exit sequel. Therefore the textual loop
        // must be followed to a goto to the "normal" sequel and therefore no loop
        // exit, regardless "break" or "normal" can go naturally to its sequel.
        if (controlStructBlock instanceof HeaderBlock && controlStructBlock.enclosingLoop() != null
                && allGoToDestination(controlStructBlock.enclosingLoop().exitArrows, naturalNext)) {
            this.naturalNext = naturalNext;
        }
        propagateNaturalNextChained(body, controlStructBlock);
    }

    private boolean allGoToDestination(TapList<FGArrow> arrows, Block destination) {
        boolean ok = true;
        while (ok && arrows != null) {
            ok = arrows.head.destination == destination;
            arrows = arrows.tail;
        }
        return ok;
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        propagateNaturalFlowChained(body);
        naturalFlow = new TapList<>(exitArrow, null);
        return naturalFlow;
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows,
                                               TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings,
                                               TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        TapList<FGArrow> afterIteration = preGenerateTreeChained(body, toFutureIncludes, fileUserHelpStrings,
                new TapList<>(loopArrow, null),
                new TapList<>(this, enclosingStructs), skipSubUnits);
        structuredTree = checkLabels(
                controlStructBlock.instructions.head.copyTreePlusComments(),
                naturalArrows, afterIteration, null);
        putDirectivesBackAsComments(controlStructBlock.instructions.head.directives) ;
        insertInstructionTree(structuredTree, true);
        if (structuredTree.opCode() == ILLang.op_labelStatement) {
            structuredTree = structuredTree.down(2);
        }
        //Prepare the "DO" loops that come from C "for" loops with embedded declaration,
        // to be turned back into "for" loops with their embedded declaration:
        if (structuredTree.opCode() == ILLang.op_loop &&
                structuredTree.down(3).opCode() == ILLang.op_do &&
                controlStructBlock instanceof HeaderBlock &&
                ((HeaderBlock) controlStructBlock).declaresItsIterator) {
            VariableDecl indexDecl =
                    controlStructBlock.symbolTable.getVariableDecl(ILUtils.baseName(structuredTree.down(3).down(1)));
            if (indexDecl != null && indexDecl.type().wrappedType != null) {
                Tree typeTree =
                        indexDecl.type().wrappedType.generateTree(controlStructBlock.symbolTable, null, null, true, null);
                Tree indexTree = structuredTree.down(3).cutChild(1);
                structuredTree.down(3).setChild(
                        ILUtils.build(ILLang.op_varDeclaration,
                                ILUtils.build(ILLang.op_modifiers),
                                typeTree,
                                ILUtils.build(ILLang.op_declarators, indexTree)), 1);
            }
        }
        if (exitArrow == null) {
            return null;
        } else {
            return new TapList<>(exitArrow, null);
        }
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        TapList<Tree> treeList =
                generateTreeChained(body, false, cycleLabel, toFutureIncludes, fileUserHelpStrings);
        if (TapList.length(treeList) == 1) {
            structuredTree.setChild(treeList.head, 4);
        } else {
            structuredTree.setChild(
                    ILUtils.build(ILLang.op_blockStatement, treeList),
                    4);
        }
        if (loopArrow != null) {
            loopArrow.carry = null;
        }
        if (exitArrow != null && exitArrow.carry != null && !delayGoto) {
            insertInstructionTree(turnCarryIntoJump(exitArrow), false);
        }
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("Loop ");
        super.dump(indent);
        TapEnv.print(" Body:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = body; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("}");
    }

    @Override
    public String toString() {
        return "LOOP:" + controlStructBlock + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions) + ":(" + body + ')';
    }
}
