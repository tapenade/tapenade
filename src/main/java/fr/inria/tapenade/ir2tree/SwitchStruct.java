/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

final class SwitchStruct extends ControlStruct {
    private final int casesArrowNumber;
    protected TapList<FGArrow> naturalArrowsAfterSwitch;
    private FGArrow[] casesArrow;
    private TapList<ControlStruct>[] casesBody;
    private TapList<FGArrow> uniqueFollow;

    protected SwitchStruct(Block block) {
        this.controlStructBlock = block;
        FGArrow arrow;
        casesArrowNumber = TapList.length(block.flow());
        if (casesArrowNumber != -1) {
            casesArrow = new FGArrow[casesArrowNumber];
            casesBody = new TapList[casesArrowNumber];
        }
        // Sort flowing FGArrows by growing rank in the sequence of cases.
        // Note: Warning: cases NOMATCH and DEFAULT (mutually exclusive) come first!
        TapList<FGArrow> sortedArrows = sortArrowsByCases(block.flow());
        // Ensuite on construit un tableau casesArrow dont les elements sont les
        //  FGArrow de la liste sortedArrows mais on place la  FGArrow de case default
        //  dans la derniere case de casesArrow.
        int index = 0;
        while (sortedArrows != null) {
            arrow = sortedArrows.head;
            if (arrow.test == FGConstants.CASE) {
                if (arrow.cases.head == FGConstants.DEFAULT || arrow.cases.head == FGConstants.NOMATCH) {
                    casesArrow[casesArrowNumber - 1] = arrow;
                } else {
                    casesArrow[index] = arrow;
                    ++index;
                }
            }
            sortedArrows = sortedArrows.tail;
        }
    }

    /**
     * Insert the given "struct" into this ControlStruct,
     * at the correct place determined by "arrow".
     */
    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        if (arrow.cases.head != FGConstants.NOMATCH) {
            int rank;
            if (arrow.cases.head == FGConstants.DEFAULT) {
                rank = casesArrowNumber - 1;
            } else {
                //On retrouve le rank de arrow dans le tableau casesArrow
                // en comparant (le premier element de la liste cases de )
                // arrow avec casesArrow[rank] :
                int j = 0;
                rank = -1;
                while (rank == -1 && j < casesArrowNumber) {
                    if (arrow.cases.head == casesArrow[j].cases.head) {
                        rank = j;
                    }
                    ++j;
                }
            }
            casesBody[rank] = new TapList<>(struct, casesBody[rank]);
        }
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        for (int i = 0; i < casesArrowNumber; i++) {
            casesBody[i] =
                    insertLetStructureChained(casesBody[i], controlStructBlock.symbolTable);
        }
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position...
     */
    @Override
    protected void reorderBody() {
        for (int i = casesBody.length - 1; i >= 0; i--) {
            casesBody[i] = reorderBodyChained(casesBody[i]);
        }
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
        for (int i = casesBody.length - 1; i >= 0; i--) {
            if (casesBody[i] != null) {
                propagateNaturalNextChained(casesBody[i], naturalNext);
                naturalNext = casesBody[i].head.controlStructBlock;
            }
        }
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        TapList<FGArrow> curNaturalFlow = null;
        for (int i = 0; i < casesBody.length; i++) {
            curNaturalFlow = new TapList<>(casesArrow[i], curNaturalFlow);
            if (casesBody[i] != null) {
                curNaturalFlow = propagateNaturalFlowChained(casesBody[i]);
            }
        }
        naturalFlow = curNaturalFlow;
        return naturalFlow;
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows,
                                               TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings,
                                               TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {

        structuredTree = preGenerateBody(controlStructBlock.instructions, naturalArrows,
                toFutureIncludes, fileUserHelpStrings, skipSubUnits);
        if (structuredTree.opCode() == ILLang.op_labelStatement) {
            structuredTree = structuredTree.down(2);
        }
        TapList<FGArrow> afterCase = null;
        for (int i = 0; i < casesArrowNumber; i++) {
            if (casesArrow[i] != null) {
                if (casesArrow[i].cases.head == FGConstants.NOMATCH) {
                    naturalArrowsAfterSwitch =
                            new TapList<>(casesArrow[i], naturalArrowsAfterSwitch);
                } else {
                    afterCase = new TapList<>(casesArrow[i], afterCase);
                    if (casesBody[i] != null) {
                        afterCase = preGenerateTreeChained(
                                casesBody[i], toFutureIncludes, fileUserHelpStrings, afterCase,
                                new TapList<>(this, enclosingStructs), skipSubUnits);
                    }
                    if (TapEnv.relatedUnit().isFortran()) {
                        //In Fortran, control never goes into next case:
                        naturalArrowsAfterSwitch =
                                TapList.append(naturalArrowsAfterSwitch, afterCase);
                        afterCase = null;
                    }
                }

            }
        }
        TapList<FGArrow> result = TapList.append(naturalArrowsAfterSwitch, afterCase);
        Block destination = null;
        Block arrowDestination;
        boolean cycle = false;
        boolean arrowCycle;
        boolean sameDestination = true;
        TapList<FGArrow> arrows = result;
        while (arrows != null && sameDestination) {
            arrowDestination = arrows.head.destination;
            arrowCycle = arrows.head.inACycle;
            sameDestination =
                    destination == null
                            || arrowDestination == destination && cycle == arrowCycle;
            destination = arrowDestination;
            cycle = arrowCycle;
            arrows = arrows.tail;
        }
        if (result != null && sameDestination) {
            uniqueFollow = result;
        }
        return result;
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto,
                                TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                TapList<Tree> fileUserHelpStrings) {
        int firstCase;
        int mergeCase;
        Tree mergeTree;
        TapIntList casesList;
        // On regroupe les cas (seulement en Fortran):
        if (TapEnv.relatedUnit().isFortran()) {
            for (int i = 0; i < casesArrowNumber; i++) {
                if (casesArrow[i] != null && TapIntList.length(casesArrow[i].cases) > 1) {
                    casesList = casesArrow[i].cases;
                    if (casesList.head == -2) {
                        // C'est le cas NOMATCH:
                        firstCase = -2;
                    } else if (casesList.head == -1) {
                        // C'est le cas DEFAULT:
                        firstCase = structuredTree.down(2).length();
                    } else {
                        firstCase = casesList.head + 1;
                    }
                    casesList = casesList.tail;
                    while (casesList != null) {
                        mergeCase = casesList.head + 1;
                        if (firstCase != -2) {
                            mergeTree = mergeExprSwitchCase(
                                    structuredTree.down(2).down(firstCase).down(1),
                                    structuredTree.down(2).down(mergeCase).down(1));
                            // le cas firstCase recoit l'expression resultante des deux cas:
                            structuredTree.down(2).down(firstCase).setChild(mergeTree, 1);
                        }
                        // le cas mergeCase recoit l'expression none,
                        // ce qui permettra ulterieurement de detecter
                        // que ce cas est devenu inutile:
                        structuredTree.down(2).down(mergeCase).setChild(
                                ILUtils.build(ILLang.op_none), 1);
                        casesList = casesList.tail;
                    }
                }
            }
        }

        // On detruit les cas inutiles
        int numberOfCases = structuredTree.down(2).length();
        for (int i = 1; i <= numberOfCases; i++) {
            if (ILUtils.isNullOrNone(structuredTree.down(2).down(i).down(1))) {
                structuredTree.down(2).removeChild(i);
                --i;
                --numberOfCases;
            }
        }
        Tree caseTree;
        Tree[] sons = structuredTree.down(2).children();
        int swIndex = 0;
        for (int i = 0; i < casesArrowNumber; i++) {
            if (casesArrow[i].cases.head != FGConstants.NOMATCH) {
                if (casesArrow[i] != null && casesArrow[i].carry != null) {
                    caseTree = ILUtils.build(ILLang.op_blockStatement,
                            turnCarryIntoJump(casesArrow[i]));
                    //We must wipe the carry on this casesArrow[i] but also
                    // on all the arrows that reach this case (when a case
                    // continues into the next case)
                    TapList<FGArrow> allCaseArrows = casesArrow[i].destination.backFlow();
                    while (allCaseArrows != null) {
                        allCaseArrows.head.carry = null;
                        allCaseArrows = allCaseArrows.tail;
                    }
                } else {
                    TapList<Tree> caseSons = generateTreeChained(casesBody[i],
                            false, null, toFutureIncludes, fileUserHelpStrings);
                    caseTree = ILUtils.build(ILLang.op_blockStatement, caseSons);
                }
                if (!TapEnv.relatedUnit().isFortran()) {
                    int nbMergedCases = TapIntList.length(casesArrow[i].cases);
                    if (nbMergedCases > 1) {
                        swIndex += nbMergedCases - 1;
                    }
                }
                if (ILUtils.isNotNoneNorEmpty(caseTree)) {
                    sons[swIndex].setChild(caseTree, 2);
                } else {
                    if (TapEnv.relatedUnit().isC()) {
                        sons[swIndex].setChild(ILUtils.build(ILLang.op_blockStatement,
                                ILUtils.build(ILLang.op_break)), 2);
                    }
                }
                swIndex++;
            }
        }
        if (uniqueFollow != null && !delayGoto) {
            Tree followJump = null;
            FGArrow followArrow;
            while (uniqueFollow != null) {
                followArrow = uniqueFollow.head;
                if (followJump == null) {
                    followJump = turnCarryIntoJump(followArrow);
                }
                followArrow.carry = null;
                uniqueFollow = uniqueFollow.tail;
            }
            if (followJump != null) {
                insertInstructionTree(followJump, true);
            }
        }
    }

    /**
     * A partir de deux expressions provenant d'arbre de l'operateur switchcase
     * on contruit "l'union" des deux expressions.
     */
    private Tree mergeExprSwitchCase(Tree expr1, Tree expr2) {
        Tree result;
        if (expr1.children().length != 0 && expr2.children().length != 0) {
            expr2.addChildren(ILUtils.copiedSonsList(expr1), -1);
            result = expr2;
        } else {
            result = ILUtils.build(ILLang.op_expressions);
        }
        return result;
    }

    /**
     * On trie la liste des FGArrow par ordre croissant du premier element
     * de la liste cases.
     */
    private TapList<FGArrow> sortArrowsByCases(TapList<FGArrow> arrows) {
        TapList<FGArrow> sortedArrows = new TapList<>(null, null);
        TapList<FGArrow> inSortedArrows;
        int rank;
        while (arrows != null) {
            if (arrows.head.cases != null) {
                rank = arrows.head.cases.head;
                inSortedArrows = sortedArrows;
                while
                (inSortedArrows.tail != null
                        && inSortedArrows.tail.head.cases.head < rank) {
                    inSortedArrows = inSortedArrows.tail;
                }
                inSortedArrows.placdl(arrows.head);
            }
            arrows = arrows.tail;
        }
        return sortedArrows.tail;
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("Switch ");
        super.dump(indent);
        for (int i = 0; i < casesArrowNumber; ++i) {
            TapEnv.print(" Case #" + i + casesArrow[i].cases + ":{");
            TapEnv.println();
            for (TapList<ControlStruct> inBody = casesBody[i]; inBody != null; inBody = inBody.tail) {
                TapEnv.indent(indent + 2);
                inBody.head.dump(indent + 2);
                TapEnv.println();
            }
            TapEnv.indent(indent);
            TapEnv.print("}");
        }
    }

    @Override
    public String toString() {
        StringBuilder contents = new StringBuilder();
        for (int i = 0; i < casesArrowNumber; i++) {
            contents.append("(#").append(i).append('[').append(casesArrow[i].cases).append("] ").append(casesBody[i]).append(')');
        }
        return "SWITCH:" + controlStructBlock
                + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode())
                + " INSTRS:" + controlStructBlock.instructions) + ':' + contents;
    }
}
