/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * This structure represents a piece of Flow Graph that is the scope for new local variables.
 * In other words it is a piece of C code between an opening and closing curly braces {} and
 * that declares local variables.
 */
final class LetStruct extends ControlStruct {

    protected TapList<ControlStruct> body;

    private SymbolTable symbolTable;

    protected LetStruct(SymbolTable symbolTable) {
        super();
        this.symbolTable = symbolTable;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        body = new TapList<>(struct, body);
    }

    protected void addControlStructAtTail(ControlStruct struct) {
        if (body == null) {
            body = new TapList<>(struct, null);
        } else {
            body.newR(struct);
        }
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        body = insertLetStructureChained(body, symbolTable);
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position...
     */
    @Override
    protected void reorderBody() {
        body = reorderBodyChained(body);
        // Only for pure "scope" LETs, put the Block with lowest rank as this LetStruct's Block:
        controlStructBlock = body.head.controlStructBlock;
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
        propagateNaturalNextChained(body, naturalNext);
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        naturalFlow = propagateNaturalFlowChained(body);
        return naturalFlow;
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings, TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        // Arrows coming from inside this Let scope must be ignored
        // at this level => add them into ignoredArrows
        TapList<FGArrow> arrows = controlStructBlock.backFlow();
        TapList<FGArrow> ignoredArrows = null;
        FGArrow arrow;
        SymbolTable letST;
        SymbolTable origST;
        letST = controlStructBlock.symbolTable;
        while (arrows != null) {
            arrow = arrows.head;
            origST = arrow.origin.symbolTable;
            if (origST.nestedIn(letST) && arrow.mayBeNatural()) {
                ignoredArrows = new TapList<>(arrow, ignoredArrows);
            }
            arrows = arrows.tail;
        }
        structuredTree =
                checkLabels(ILUtils.build(ILLang.op_blockStatement), naturalArrows,
                        null, ignoredArrows);
        // We reset the structuredTree to a simple blockStatement without the label,
        // because this caused a duplicated label in the generated code!
        // TODO: this is a temporary hack that must be done cleanly later!
        structuredTree = ILUtils.build(ILLang.op_blockStatement);
        insertInstructionTree(structuredTree, true);
        return preGenerateTreeChained(body, toFutureIncludes, fileUserHelpStrings, naturalArrows,
                new TapList<>(this, enclosingStructs), skipSubUnits);
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        int i = 1;
        TapList<Tree> sons = generateTreeChained(body, delayGoto, null, toFutureIncludes, fileUserHelpStrings);
        while (sons != null) {
            structuredTree.addChild(sons.head, i);
            i++;
            sons = sons.tail;
        }
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("Let ");
        super.dump(indent);
        TapEnv.print(" Contents:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = body; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("}");
    }

    @Override
    public String toString() {
        return "LET:" + controlStructBlock + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions) + " :(" + body + ')';
    }
}
