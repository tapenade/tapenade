/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * Tree regeneration of Function, Procedure.
 */
final class FunctionDeclStruct extends DeclStruct {
    protected TapList<FunctionDecl> functionDecls;

    protected FunctionDeclStruct(FunctionDecl functionDecl) {
        this.functionDecls = new TapList<>(functionDecl, null);
        usedNames = functionDecl.dependsOn();
        if (functionDecl.isModule()) {
            kind = StructConstants.USE;
        } else {
            kind = StructConstants.FUNCTION;
        }
        symbolDecl = functionDecl;
    }

    @Override
    protected Tree generateTree(TapList<SymbolDecl> declaredTypes, SymbolTable symbolTable) {
        Tree result;
        TapList<Tree> nameTrees = null;
        TapList<FunctionDecl> inFunctionDecls = functionDecls;
        FunctionDecl funcDecl = null;
        Tree funcNameTree;
        while (inFunctionDecls != null) {
            funcDecl = inFunctionDecls.head;
            funcNameTree = ILUtils.build(ILLang.op_ident, funcDecl.symbol);
            nameTrees = new TapList<>(funcNameTree, nameTrees);
            inFunctionDecls = inFunctionDecls.tail;
        }
        if (funcDecl == null) {
            result = null;
        } else if (funcDecl.unit() == null) {
            result = TapEnv.relatedLanguageIsC()
                    ? funcDecl.declarator
                    : ILUtils.build(ILLang.op_external, nameTrees);
        } else if (funcDecl.isIntrinsic()) {
            result = ILUtils.build(ILLang.op_intrinsic, nameTrees);
        } else if (funcDecl.isInterface()) {
            if (funcDecl.unit().enclosingUnitOfInterface == null) {
                Tree unitTree = TreeGen.generate(funcDecl.unit(), false, true, false,
                        new TapList<>(null, null), new TapList<>(null, null));
                // We are (almost?) certain that unitTree contains only one child, which is the funcDecl.unit()'s declarator code:
                if (unitTree != null) {
                    unitTree = unitTree.cutChild(1);
                }
                result = ILUtils.build(ILLang.op_interfaceDecl,
                        ILUtils.build(ILLang.op_none),
                        ILUtils.build(ILLang.op_interfaces,
                                ILUtils.build(ILLang.op_interface, unitTree)));
            } else {
                result = null;
            }
        } else if (funcDecl.isRenamed()) {
            // traite' dans UnitStruct.java 
            result = null;
        } else if (funcDecl.isModule()) {
            TapList<String> extraInfoLL = funcDecl.extraInfo();
            Tree extraInfo;
            if (extraInfoLL == null) {
                extraInfo = ILUtils.build(ILLang.op_renamedVisibles);
                result =
                        ILUtils.build(ILLang.op_useDecl, nameTrees.head, extraInfo);
            } else {
                TapList<TapPair<Unit, Tree>> tmpInfo = extraInfoLL.findExtraInfos(TapEnv.relatedUnit());
                if (tmpInfo == null) {
                    extraInfo = ILUtils.build(ILLang.op_renamedVisibles);
                    result =
                            ILUtils.build(ILLang.op_useDecl, nameTrees.head, extraInfo);
                } else {
                    result = ILUtils.build(ILLang.op_blockStatement);
                    while (tmpInfo != null) {
                        extraInfo = ILUtils.build(ILLang.op_useDecl,
                                nameTrees.head,
                                ILUtils.copy(tmpInfo.head.second));
                        result.addChild(extraInfo, result.length() + 1);
                        tmpInfo = tmpInfo.tail;
                    }
                }
            }
        } else if (funcDecl.unit().hasSource()) {
            // pas toujours cf nonRegression v24
            result = null;
        } else {
            // Catch-all case:
            result = TapEnv.relatedLanguageIsC()
                    ? funcDecl.declarator
                    : ILUtils.build(ILLang.op_external, nameTrees);
        }
        return result;
    }

    @Override
    public String toString() {
        return "declaration of Function "
                + functionDecls.head.symbol;
    }
}
