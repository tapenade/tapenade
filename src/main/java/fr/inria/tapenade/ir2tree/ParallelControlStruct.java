/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * This structure represents a piece of Flow Graph that defines a parallel region.
 * This structure is quite simple, with a single entry arrow (arrowIntoBody),
 * a single exit arrow, and one chain of ControlStruct as a body.
 */
final class ParallelControlStruct extends ControlStruct {

    private final FGArrow arrowIntoBody;
    private TapList<ControlStruct> body;

    protected ParallelControlStruct(Block block) {
        this.controlStructBlock = block;
        arrowIntoBody = block.flow().head;
    }

    @Override
    protected ControlStruct getBlock(Block block) {
        ControlStruct result = null;
        if (this.controlStructBlock == block) {
            result = this;
        } else {
            TapList<ControlStruct> inBody = body;
            while (inBody != null && result == null) {
                result = inBody.head.getBlock(block);
                inBody = inBody.tail;
            }
        }
        return result;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        body = new TapList<>(struct, body);
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        if (body != null) {
            body = insertLetStructureChained(body, body.head.controlStructBlock.symbolTable);
        }
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position...
     */
    @Override
    protected void reorderBody() {
        body = reorderBodyChained(body);
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
        propagateNaturalNextChained(body, naturalNext);
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        return propagateNaturalFlowChained(body);
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings, TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        structuredTree = preGenerateBody(controlStructBlock.instructions, naturalArrows,
                toFutureIncludes, fileUserHelpStrings, skipSubUnits);
        if (structuredTree.opCode() == ILLang.op_labelStatement) {
            structuredTree = structuredTree.down(2);
        }
        return preGenerateTreeChained(body, toFutureIncludes, fileUserHelpStrings,
                new TapList<>(arrowIntoBody, null),
                new TapList<>(this, enclosingStructs), skipSubUnits);
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        TapList<Tree> bodySons = generateTreeChained(body, true, null, toFutureIncludes, fileUserHelpStrings);
        Tree bodyTree;
        if (bodySons == null || structuredTree.opCode() != ILLang.op_parallelLoop) {
            bodyTree = ILUtils.build(ILLang.op_blockStatement, bodySons);
        } else {
            bodyTree = bodySons.head;
        }
        // TODO: If the "body" part is empty, we should probably remove the whole parallel region
        // if (ILUtils.isNotNoneNorEmpty(bodyTree)) {
        structuredTree.setChild(bodyTree, 3);
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("ParallelControl ");
        super.dump(indent);
        TapEnv.print(" Contents:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = body; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("}");
    }

    @Override
    public String toString() {
        return "PARALLEL:" + controlStructBlock + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions) + ":(" + body + ')';
    }
}
