/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.CStuff;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * Abstract class for IfThenElse, Let, Loop, Switch,.. Controls.
 */
public abstract class ControlStruct {
    protected Block controlStructBlock;
    protected TapList<Tree> treesReverse;
    protected Tree structuredTree;
    protected Block naturalNext;
    protected TapList<FGArrow> naturalFlow;
    protected ControlStruct aboveControlStruct;
    // Used by dontLooseComments and insertInstructionTree
    private TapList<Tree> danglingCommentsReverse;

    protected static TapList<ControlStruct> insertLetStructureChained(TapList<ControlStruct> chainedStructs,
                                                                      SymbolTable refSymbolTable) {
        // a dummy LetStruct built only to make the algorithm simpler:
        LetStruct dummyLetStruct = new LetStruct(refSymbolTable);
        //Association list: "nested SymbolTable" -> "the LetStruct built for it"
        TapList<TapPair<SymbolTable, LetStruct>> allLetStructs =
                new TapList<>(new TapPair<>(refSymbolTable, dummyLetStruct), null);
        ControlStruct controlStruct;
        SymbolTable targetSymbolTable;
        LetStruct targetLetStruct;
        while (chainedStructs != null) {
            controlStruct = chainedStructs.head;
            controlStruct.insertLetStructure();
            targetSymbolTable = controlStruct.controlStructBlock.symbolTable;
            targetLetStruct = getSetLetStructOfSymbolTable(targetSymbolTable,
                    allLetStructs, refSymbolTable, controlStruct.controlStructBlock);
            if (targetLetStruct == null) {
                targetLetStruct = dummyLetStruct;
            }
            targetLetStruct.addControlStructAtTail(controlStruct);
            chainedStructs = chainedStructs.tail;
        }
        return dummyLetStruct.body;
    }

    /**
     * Recursive function that get/sets the LetStruct associated to the given "symbolTable".
     * The result is found in the A-List "allLetStructs", and if not found, it is
     * created and registered into "allLetStructs".
     */
    private static LetStruct getSetLetStructOfSymbolTable(SymbolTable symbolTable,
                                                          TapList<TapPair<SymbolTable, LetStruct>> allLetStructs,
                                                          SymbolTable refSymbolTable, Block block) {
        LetStruct found;
        if (symbolTable == null || refSymbolTable.nestedIn(symbolTable)) {
            found = TapList.cassq(refSymbolTable, allLetStructs);
        } else { // Case of plain scopes: "{...}"
            found = TapList.cassq(symbolTable, allLetStructs);
            if (found == null) {
                LetStruct above =
                        getSetLetStructOfSymbolTable(symbolTable.basisSymbolTable(),
                                allLetStructs, refSymbolTable, block);
                found = new LetStruct(symbolTable);
                found.controlStructBlock = block;
                allLetStructs.newR(new TapPair<>(symbolTable, found));
                above.addControlStructAtTail(found);
            }
        }
        return found;
    }

    protected static TapList<ControlStruct> reorderBodyChained(TapList<ControlStruct> chainedStructs) {
        TapList<ControlStruct> toSortedStructs = new TapList<>(null, null);
        TapList<ControlStruct> inSortedStructs;
        ControlStruct controlStruct;
        while (chainedStructs != null) {
            controlStruct = chainedStructs.head;
            controlStruct.reorderBody();
            inSortedStructs = toSortedStructs;
            // Find the correct place to insert:
            while (inSortedStructs.tail != null &&
                    mustReorderAfter(controlStruct, inSortedStructs.tail.head)) {
                inSortedStructs = inSortedStructs.tail;
            }
            inSortedStructs.tail = new TapList<>(controlStruct, inSortedStructs.tail);
            chainedStructs = chainedStructs.tail;
        }
        return toSortedStructs.tail;
    }

    /**
     * Order Control structs by increasing block ranks.
     */
    private static boolean mustReorderAfter(ControlStruct str1, ControlStruct str2) {
        return str1.controlStructBlock.rank > str2.controlStructBlock.rank;
    }

    protected static void propagateNaturalNextChained(TapList<ControlStruct> chainedStructs, Block lastNaturalNext) {
        ControlStruct controlStruct;
        while (chainedStructs != null) {
            controlStruct = chainedStructs.head;
            chainedStructs = chainedStructs.tail;
            controlStruct.propagateNaturalNext(chainedStructs != null ?
                    chainedStructs.head.controlStructBlock :
                    lastNaturalNext);
        }
    }

    protected static TapList<FGArrow> propagateNaturalFlowChained(TapList<ControlStruct> chainedStructs) {
        ControlStruct controlStruct;
        TapList<FGArrow> naturalFlow = null;
        while (chainedStructs != null) {
            controlStruct = chainedStructs.head;
            naturalFlow = controlStruct.propagateNaturalFlow();
            chainedStructs = chainedStructs.tail;
        }
        return naturalFlow;
    }

    protected static TapList<FGArrow> preGenerateTreeChained(TapList<ControlStruct> chainedStructs,
                                                             TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                                             TapList<Tree> fileUserHelpStrings,
                                                             TapList<FGArrow> naturalArrows,
                                                             TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        while (chainedStructs != null) {
            naturalArrows = chainedStructs.head.preGenerateTree(
                    naturalArrows, toFutureIncludes, fileUserHelpStrings, enclosingStructs, skipSubUnits);
            chainedStructs = chainedStructs.tail;
        }
        return naturalArrows;
    }

    private static Tree generatedTreeFor(Instruction instr,
                                         TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                         TapList<Tree> fileUserHelpStrings, boolean skipSubUnits) {
        boolean traceThisUnit =
            TapEnv.get().srcUnitsToTraceInTreeGen != null
            && (TapEnv.get().srcUnitsToTraceInTreeGen.head == null
                || TapList.contains(TapEnv.get().srcUnitsToTraceInTreeGen, TapEnv.relatedUnit()));
        Unit definedUnit = instr.isUnitDefinitionStub();
        if (definedUnit != null) {
            if (skipSubUnits) {
                return null;
            }
            Tree unitTree;
            if (definedUnit.rank() == -1) {
                TapEnv.toolWarning(TapEnv.MSG_ERROR,
                        "Warning: regenerated sub-unit seems external (rank==-1): " + definedUnit);
            }
            unitTree = TreeGen.generate(definedUnit, false, false, false, toFutureIncludes,
                    definedUnit.isC() ? fileUserHelpStrings : null);
            // We are (almost?) certain that unitTree contains only one child, which is the definedUnit's code:
            if (unitTree != null) {
                unitTree = unitTree.cutChild(1);
            }
            return unitTree;
        } else if (instr.tree != null) {
            if (instr.tree.opCode() == ILLang.op_include
                    && instr.fromInclude() != null && instr.fromInclude().tree == null) {
                // This include has been inlined, e.g. because it contains a Fortran IMPLICIT declaration...
                if (traceThisUnit) {
                    TapEnv.printlnOnTrace("Skipping include instruction because inlined: "+ILUtils.toString(instr.tree)) ;
                }
                return null;
            } else if (TapEnv.relatedUnit().isC() && isOrContainsAnAssignedAllocate(instr.tree)) {
                // Put back C or Cuda syntax for allocation when appropriate:
                Tree newTree = ILUtils.copy(instr.tree) ;
                if (newTree.opCode() == ILLang.op_varDeclaration) {
                    Tree[] declarators = newTree.down(3).children() ;
                    for (int i = declarators.length-1 ; i>=0 ; --i) {
                        declarators[i].setChild(CStuff.buildMalloc(declarators[i].down(2), null), 2) ;
                    } 
                } else {
                    Tree newMalloc = CStuff.buildMalloc(newTree.down(2), newTree.down(1)) ;
                    if (ILUtils.isCallingString(newMalloc, "cudaMalloc", true))
                        newTree = newMalloc ;
                    else
                        newTree.setChild(newMalloc, 2) ;
                }
                if (traceThisUnit) {
                    TapEnv.printlnOnTrace("Transform IL "+instr.tree+" into C "+newTree) ;
                }
                instr.copyCommentsToTree(newTree) ;
                return newTree ;
            } else if (TapEnv.relatedUnit().isC() && instr.tree.opCode() == ILLang.op_deallocate) {
                // Put back C or Cuda syntax for deallocation when appropriate:
                Tree sourceTree = instr.tree.getAnnotation("sourcetree") ;
                while (sourceTree!=null && sourceTree.opCode()==ILLang.op_cast) {
                    sourceTree = sourceTree.down(2) ;
                }
                if (ILUtils.isCallingString(sourceTree, "cudaMalloc", true)
                    || ILUtils.isCallingString(sourceTree, "cudaFree", true)) {
                    Tree newCudaFree =
                        ILUtils.buildCall(null,
                           ILUtils.build(ILLang.op_ident, "cudaFree"),
                           ILUtils.build(ILLang.op_expressions,
                             ILUtils.copy(instr.tree.down(1)))) ;
                    if (traceThisUnit) {
                        TapEnv.printlnOnTrace("Transform IL "+instr.tree+" into Cuda "+newCudaFree) ;
                    }
                    instr.copyCommentsToTree(newCudaFree) ;
                    return newCudaFree ;
                } else {
                    Tree newFree =
                        ILUtils.buildCall(null,
                           ILUtils.build(ILLang.op_ident, "free"),
                           ILUtils.build(ILLang.op_expressions,
                             ILUtils.copy(instr.tree.down(1)))) ;
                    if (traceThisUnit) {
                        TapEnv.printlnOnTrace("Transform IL "+instr.tree+" into C "+newFree) ;
                    }
                    instr.copyCommentsToTree(newFree) ;
                    return newFree ;
                }
            } else {
                return instr.copyTreePlusComments();
            }
        } else {
            return null ;
        }
    }

    private static boolean isOrContainsAnAssignedAllocate(Tree tree) {
        boolean result = false ;
        if (tree.opCode() == ILLang.op_varDeclaration) {
            Tree[] declarators = tree.down(3).children() ;
            for (int i = declarators.length-1 ; i>=0 && !result ; --i) {
                result = (declarators[i].opCode()==ILLang.op_assign
                          && ILUtils.isAllocate(declarators[i].down(2))) ;
            }
        } else {
            result = (tree.opCode()==ILLang.op_assign
                      && ILUtils.isAllocate(tree.down(2))) ;
        }
        return result ;
    }

    protected static TapList<Tree> generateTreeChained(
            TapList<ControlStruct> chainedStructs,
            boolean delayGoto, String lastLabel, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
            TapList<Tree> fileUserHelpStrings) {
        TapList<Tree> topInstrList = new TapList<>(null, null);
        TapList<Tree> tailInstrList = topInstrList;
        TapList<Tree> subTreeList;
        ControlStruct chainedStruct;
        while (chainedStructs != null) {
            chainedStruct = chainedStructs.head;
            chainedStruct.generateTree(
                    chainedStructs.tail == null && delayGoto, toFutureIncludes, fileUserHelpStrings);
            subTreeList = TapList.reverse(chainedStruct.treesReverse);
            chainedStruct.treesReverse = null;
            while (subTreeList != null) {
                tailInstrList = tailInstrList.placdl(subTreeList.head);
                subTreeList = subTreeList.tail;
            }
            chainedStructs = chainedStructs.tail;
        }
        if (lastLabel != null) {
            tailInstrList.placdl(
                    setLabelAround(lastLabel, ILUtils.build(ILLang.op_none)));
        }
        return topInstrList.tail;
    }

    private static Tree setLabelAround(String label, Tree tree) {
        Tree labelledTree = ILUtils.build(ILLang.op_labelStatement,
                ILUtils.build(ILLang.op_label, label),
                tree);
        Tree comments;
        comments = tree.getAnnotation("preComments");
        if (comments != null) {
            tree.removeAnnotation("preComments");
            labelledTree.setAnnotation("preComments", comments);
        }
        comments = tree.getAnnotation("postComments");
        if (comments != null) {
            tree.removeAnnotation("postComments");
            labelledTree.setAnnotation("postComments", comments);
        }
        return labelledTree;
    }

    protected ControlStruct getBlock(Block block) {
        ControlStruct result;
        if (this.controlStructBlock == block) {
            result = this;
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Insert the given "struct" into this ControlStruct,
     * at the correct place determined by "arrow".
     */
    protected abstract void addControlStruct(ControlStruct struct, FGArrow arrow);

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    protected abstract void insertLetStructure();

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following the order of the block ranks...
     */
    protected abstract void reorderBody();

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    protected abstract void propagateNaturalNext(Block naturalNext);

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    protected abstract TapList<FGArrow> propagateNaturalFlow();

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    protected abstract TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows,
                                                        TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                                        TapList<Tree> fileUserHelpStrings,
                                                        TapList<ControlStruct> enclosingStructs, boolean skipSubUnits);

    /**
     * Utility called during preGenerateTree().
     * Prepares the list of instructions of the current Block, taking care of removing
     * empty and phantom instructions, but keeping the comments at the best possible place.
     *
     * @return the last instruction Tree.
     */
    protected Tree preGenerateBody(TapList<Instruction> instructions,
                                   TapList<FGArrow> naturalArrows,
                                   TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                   TapList<Tree> fileUserHelpStrings, boolean skipSubUnits) {
        Tree instructionTree;
        Instruction instruction;
        danglingCommentsReverse = null;
        if (!TapEnv.get().createStub) {
            if (instructions == null) {
                instructionTree =
                        ILUtils.build(TapEnv.relatedLanguageIsC() ? ILLang.op_none : ILLang.op_continue);
                instruction = new Instruction(instructionTree, null, controlStructBlock);
            } else {
                instruction = instructions.head;
                instructionTree = generatedTreeFor(instruction, toFutureIncludes, fileUserHelpStrings, skipSubUnits);
                instructions = instructions.tail;
            }
            instructionTree =
                    checkLabels(instructionTree, naturalArrows, null, null);
        } else {
            instruction = instructions.head;
            instructionTree = generatedTreeFor(instruction, toFutureIncludes, fileUserHelpStrings, skipSubUnits);
            instructions = instructions.tail;
        }
        while (true) {
            if ((instructions != null || treesReverse != null || !hasComments(instruction))
                && (instructionTree == null
                     || instructionTree.opCode() == ILLang.op_continue
                     || instructionTree.opCode() == ILLang.op_none
                     // reject declarations of Fortran's old intrinsic "%VAL" :
                     || (instruction.isPhantom && instructionTree.opCode() == ILLang.op_intrinsic
                         && ILUtils.isIdent(instructionTree.down(1), "%val", false))
                     || (instruction.isPhantom && instructionTree.opCode() == ILLang.op_varDeclaration
                         && ILUtils.isIdent(instructionTree.down(3).down(1), "%val", false))
                     // reject declarations added in the middle of the code by inline:
                     || (instruction.isPhantom && hasOwnScope(instruction.block))
                     // [VMP] isPhantom is true for intrinsic declarations
                     // that are added into SymbolTable when typeChecking op_call.
                     // Comment out next line to make these phantom declarations appear:
                     || instruction.isPhantom
                     )) {
                // Skip "empty" instructions, but keep their comments
                // to attach them to the next surrounding location:
                // Don't skip if this will be the only instruction in "controlStructBlock".
                dontLooseComments(instruction);
            } else {
                putDirectivesBackAsComments(instruction.directives) ;
                insertInstructionTree(instructionTree, true);
            }
            if (instructions == null) {
                break;
            }
            instruction = instructions.head;
            instructionTree = generatedTreeFor(instruction, toFutureIncludes, fileUserHelpStrings, skipSubUnits);
            instructions = instructions.tail;
        }
        // Special case of a Block that contains only empty instructions with comments:
        if (treesReverse==null && danglingCommentsReverse!=null) {
            Tree emptyTree = ILUtils.build(ILLang.op_none) ;
            emptyTree.setAnnotation("preComments",
                                    ILUtils.listReversedToComments(danglingCommentsReverse)) ; 
            treesReverse = new TapList<>(emptyTree, null);
            danglingCommentsReverse = null;
        }
// System.out.println("AT THE END OF preGenerateBody of "+this+" TREESREVERSE="+treesReverse+" INSTRUCTIONTREE="+instructionTree+" danglingCommentsReverse:"+danglingCommentsReverse);
        return instructionTree;
    }

    private static boolean hasOwnScope(Block block) {
        return (block.unit()==null ||
                block.symbolTable!=block.unit().privateSymbolTable()) ;
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    protected abstract void generateTree(boolean delayGoto,
                                         TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                         TapList<Tree> fileUserHelpStrings);

    protected Tree checkLabels(Tree structTree,
                               TapList<FGArrow> naturalArrows, TapList<FGArrow> naturalCycleArrows,
                               TapList<FGArrow> ignoredArrows) {
        TapList<FGArrow> arrows = controlStructBlock.backFlow();
        FGArrow arrow;
        String externalLabel = null;
        String internalLabel = null;
        boolean isALoopHeader =
                controlStructBlock.instructions != null && controlStructBlock.headInstr().isALoop();
        boolean arrowMustJump;
        boolean arrowCanBreak;
        boolean labelDeepInside;
        boolean loopCycle;
        ControlStruct aroundOriginBreakable;
        while (arrows != null) {
            arrow = arrows.head;
            if (!TapList.contains(ignoredArrows, arrow)) {
                loopCycle = isALoopHeader && arrow.inACycle;
                labelDeepInside = !arrow.containsCase(FGConstants.DEFAULT) &&
                        (arrow.test == FGConstants.COMP_GOTO || arrow.test == FGConstants.IO_GOTO || arrow.test == FGConstants.CALL);
                arrowMustJump = (labelDeepInside ||
                        !TapList.contains(loopCycle ? naturalCycleArrows : naturalArrows, arrow))
                        && (arrow.cases == null || arrow.cases.head != FGConstants.ELSEWHERE)
                        && arrow.origin.lastTest() != ILLang.op_return;
                aroundOriginBreakable = arrow.origin.getControlStruct();
                while (aroundOriginBreakable != null &&
                       // One can use op_break to exit a switch (in C only):
                       !(aroundOriginBreakable instanceof SwitchStruct && TapEnv.relatedLanguageIsC()) &&
                       // One can use op_break to exit a loop:
                       !(aroundOriginBreakable instanceof LoopStruct)) {
                    aroundOriginBreakable = aroundOriginBreakable.aboveControlStruct;
                }
                // we can use the Fortran break (called EXIT) !TapEnv.relatedUnit().isFortran() &&
                arrowCanBreak = !labelDeepInside && aroundOriginBreakable != null &&
                        aroundOriginBreakable.naturalNext == arrow.destination;
                //PATCH due to poor design of LetStruct (cf C:v11) :
                if (this instanceof LetStruct && loopCycle) {
                    arrowMustJump = false;
                }
                if (arrowMustJump) {
                    if (arrowCanBreak) {
                        arrow.carry =
                                ILUtils.build(ILLang.op_break);
                    } else if (loopCycle) {
                        if (internalLabel == null) {
                            if (controlStructBlock instanceof HeaderBlock) {
                                internalLabel =
                                        ((HeaderBlock) controlStructBlock).origCycleLabel();
                            }
                            if (internalLabel == null) {
                                if (controlStructBlock.isALoop()) {
                                    //There is a internal looping label distinct from the external label.
                                    internalLabel = mkNewLabel();
                                    ((HeaderBlock) controlStructBlock).setOrigCycleLabel(internalLabel);
                                } else { //Then it is a loop built with only goto's:
                                    //The internal looping label is the same as the external label.
                                    internalLabel = controlStructBlock.origLabel();
                                    if (internalLabel == null) {
                                        internalLabel = mkNewLabel();
                                        controlStructBlock.setOrigLabel(internalLabel);
                                    }
                                }
                            }
                        }
                        arrow.carry =
                                ILUtils.build(ILLang.op_label, internalLabel);
                    } else {
                        if (externalLabel == null) {
                            externalLabel = controlStructBlock.origLabel();
                            if (externalLabel == null) {
                                externalLabel = mkNewLabel();
                                controlStructBlock.setOrigLabel(externalLabel);
                            }
                        }
                        arrow.carry =
                                ILUtils.build(ILLang.op_label, externalLabel);
                    }
                }
            }
            arrows = arrows.tail;
        }
        boolean alreadySet = false;
        if (internalLabel != null) {
            if (structTree.opCode() == ILLang.op_loop) {
                structTree.setChild(
                        ILUtils.build(ILLang.op_label, internalLabel), 2);
                alreadySet = true;
            }
        }
        if (externalLabel != null && !(alreadySet && externalLabel.equals(internalLabel))) {
            if (structTree.opCode() == ILLang.op_function) {
                // Special case (cf F90:v193) of a contained function def combined with a "jump"
                // to the exit that goes to this def: the function def cannot be labelled:
                // place function def first, then prepare a continue that will get the label.
                Tree continueTree = ILUtils.build(TapEnv.relatedLanguageIsC() ? ILLang.op_none : ILLang.op_continue);
                continueTree = setLabelAround(externalLabel, continueTree);
                insertInstructionTree(continueTree, false);
            } else {
                // Standard case, put label around structTree:
                structTree = setLabelAround(externalLabel, structTree);
            }
        }
        return structTree;
    }

    private String mkNewLabel() {
        while (TapList.containsEquals(TapEnv.get().usedLabels, Integer.toString(TapEnv.get().nextNewLabel))) {
            TapEnv.get().nextNewLabel = TapEnv.get().nextNewLabel + 10;
        }
        String newLabel = Integer.toString(TapEnv.get().nextNewLabel);
        TapEnv.get().usedLabels = new TapList<>(newLabel, TapEnv.get().usedLabels);
        TapEnv.get().nextNewLabel = TapEnv.get().nextNewLabel + 10;
        return newLabel;
    }

    protected Tree turnCarryIntoJump(FGArrow arrow) {
        Tree labelTree = (Tree) arrow.carry;
        arrow.carry = null;
        if (labelTree == null) {
            return null;
        } else if (labelTree.opCode() == ILLang.op_break) {
            return labelTree;
        } else {
            return ILUtils.build(ILLang.op_goto, labelTree);
        }
    }

    protected void insertInstructionTree(Tree instructionTree, boolean atTheEnd) {
        if (danglingCommentsReverse != null) {
            Tree preCommentsTree = instructionTree.getAnnotation("preComments");
            TapList<Tree> preCommentsListReversed =
                    preCommentsTree == null ? null : ILUtils.commentsToListReversed(ILUtils.copy(preCommentsTree));
            preCommentsListReversed =
                    TapList.append(preCommentsListReversed, danglingCommentsReverse);
            instructionTree.setAnnotation("preComments",
                    ILUtils.listReversedToComments(preCommentsListReversed));
            danglingCommentsReverse = null;
        }
        if (atTheEnd) {
            treesReverse = new TapList<>(instructionTree, treesReverse);
        } else {
            TapList<Tree> trees = treesReverse;
            TapList<Tree> funTrees = null;
            while (trees != null && (trees.head.opCode() == ILLang.op_function
                    || trees.head.opCode() == ILLang.op_program)) {
                funTrees = new TapList<>(trees.head, funTrees);
                trees = trees.tail;
            }
            treesReverse = new TapList<>(instructionTree, trees);
            while (funTrees != null) {
                treesReverse = new TapList<>(funTrees.head, treesReverse);
                funTrees = funTrees.tail;
            }
        }
    }

    private boolean hasComments(Instruction instruction) {
        return !ILUtils.isNullOrNoneOrEmptyList(instruction.preComments) ||
                !ILUtils.isNullOrNoneOrEmptyList(instruction.postComments);
    }

    private void dontLooseComments(Instruction emptyInstruction) {
        Tree commentsTree;
        TapList<Tree> commentsListReversed;
        commentsTree = emptyInstruction.preComments;
        if (commentsTree != null) {
            commentsTree = ILUtils.copy(commentsTree);
            commentsListReversed = ILUtils.commentsToListReversed(commentsTree);
            danglingCommentsReverse = TapList.append(commentsListReversed, danglingCommentsReverse);
        }
        putDirectivesBackAsComments(emptyInstruction.directives) ;
        commentsTree = emptyInstruction.postComments;
        if (commentsTree != null) {
            commentsTree = ILUtils.copy(commentsTree);
            commentsListReversed = ILUtils.commentsToListReversed(commentsTree);
            danglingCommentsReverse = TapList.append(commentsListReversed, danglingCommentsReverse);
        }
        if (danglingCommentsReverse != null && treesReverse != null && treesReverse.head != null) {
            Tree receiver = treesReverse.head;
            commentsTree = receiver.getAnnotation("postComments");
            commentsListReversed =
                    commentsTree == null ? null : ILUtils.commentsToListReversed(ILUtils.copy(commentsTree));
            commentsListReversed =
                    TapList.append(danglingCommentsReverse, commentsListReversed);
            receiver.setAnnotation("postComments",
                    ILUtils.listReversedToComments(commentsListReversed));
            danglingCommentsReverse = null;
        }
    }

    protected void putDirectivesBackAsComments(TapList<Directive> directives) {
        while (directives!=null) {
            if (directives.head!=null) {
                danglingCommentsReverse =
                    new TapList<>(ILUtils.build(ILLang.op_stringCst,
                                    directives.head.rebuildFullDirective()),
                                  danglingCommentsReverse) ;
            }
            directives = directives.tail ;
        }
    }

    /**
     * Prints in detail the contents of this ControlStruct, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing.
     */
    public void dump(int indent) throws java.io.IOException {
        if (controlStructBlock == null) {
            TapEnv.print(" no CB");
        } else {
            TapEnv.print(" CB: "+controlStructBlock
                         +" Instructions:" + controlStructBlock.instructions);
        }
    }

    @Override
    public String toString() {
        return "Unspecified ControlStruct:" + controlStructBlock;
    }
}
