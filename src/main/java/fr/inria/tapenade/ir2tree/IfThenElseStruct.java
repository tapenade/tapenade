/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * Tree regeneration of if then else structure.
 */
final class IfThenElseStruct extends ControlStruct {
    private FGArrow thenArrow;
    private TapList<ControlStruct> thenBody;
    private FGArrow elseArrow;
    private TapList<ControlStruct> elseBody;
    private TapList<FGArrow> uniqueFollow;

    protected IfThenElseStruct(Block block) {
        this.controlStructBlock = block;
        TapList<FGArrow> arrows = block.flow();
        FGArrow arrow;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.test == FGConstants.IF) {
                if (arrow.cases.head == FGConstants.TRUE) {
                    thenArrow = arrow;
                } else if (arrow.cases.head == FGConstants.FALSE) {
                    elseArrow = arrow;
                }
            }
            arrows = arrows.tail;
        }
    }

    @Override
    protected ControlStruct getBlock(Block block) {
        ControlStruct result = null;
        if (this.controlStructBlock == block) {
            result = this;
        } else {
            TapList<ControlStruct> inBody = thenBody;
            while (inBody != null && result == null) {
                result = inBody.head.getBlock(block);
                inBody = inBody.tail;
            }
            inBody = elseBody;
            while (inBody != null && result == null) {
                result = inBody.head.getBlock(block);
                inBody = inBody.tail;
            }
        }
        return result;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        if (arrow.cases.head == FGConstants.TRUE) {
            thenBody = new TapList<>(struct, thenBody);
        } else {
            elseBody = new TapList<>(struct, elseBody);
        }
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        thenBody = insertLetStructureChained(thenBody, controlStructBlock.symbolTable);
        elseBody = insertLetStructureChained(elseBody, controlStructBlock.symbolTable);
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position...
     */
    @Override
    protected void reorderBody() {
        thenBody = reorderBodyChained(thenBody);
        elseBody = reorderBodyChained(elseBody);
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
        propagateNaturalNextChained(thenBody, naturalNext);
        propagateNaturalNextChained(elseBody, naturalNext);
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        TapList<FGArrow> afterThen = propagateNaturalFlowChained(thenBody);
        TapList<FGArrow> afterElse = propagateNaturalFlowChained(elseBody);
        naturalFlow = TapList.append(afterThen, afterElse);
        return naturalFlow;
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings, TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        structuredTree = preGenerateBody(controlStructBlock.instructions, naturalArrows, toFutureIncludes, fileUserHelpStrings, skipSubUnits);
        if (structuredTree.opCode() == ILLang.op_labelStatement) {
            structuredTree = structuredTree.down(2);
        }
        TapList<FGArrow> afterThen = null;
        TapList<FGArrow> afterElse = null;
        if (thenArrow != null) {
            afterThen = preGenerateTreeChained(thenBody, toFutureIncludes, fileUserHelpStrings,
                    new TapList<>(thenArrow, null),
                    new TapList<>(this, enclosingStructs), skipSubUnits);
        }
        if (elseArrow != null) {
            afterElse = preGenerateTreeChained(elseBody, toFutureIncludes, fileUserHelpStrings,
                    new TapList<>(elseArrow, null),
                    new TapList<>(this, enclosingStructs), skipSubUnits);
        }
        TapList<FGArrow> result = TapList.append(afterThen, afterElse);
        TapList<FGArrow> arrows;
        if (thenArrow == null && elseArrow == null) {
            arrows = controlStructBlock.flow();
            FGArrow arrow;
            while (arrows != null) {
                arrow = arrows.head;
                if (arrow.mayBeNatural()) {
                    result = new TapList<>(arrow, result);
                }
                arrows = arrows.tail;
            }
        }
        Block destination = null;
        Block arrowDestination;
        boolean cycle = false;
        boolean arrowCycle;
        boolean sameDestination = true;
        arrows = result;
        while (arrows != null && sameDestination) {
            arrowDestination = arrows.head.destination;
            arrowCycle = arrows.head.inACycle;
            sameDestination =
                    destination == null
                            || arrowDestination == destination && cycle == arrowCycle;
            destination = arrowDestination;
            cycle = arrowCycle;
            arrows = arrows.tail;
        }
        if (result != null && sameDestination) {
            uniqueFollow = result;
        }
        return result;
    }

    /**
     * (Recursive) final regeneration of the structured Tree
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        Tree thenTree;
        Tree elseTree;
        if (thenArrow != null && thenArrow.carry != null) {
            if (uniqueFollow == null) {
                thenTree = turnCarryIntoJump(thenArrow);
            } else {
                thenTree = ILUtils.build(ILLang.op_none);
            }
        } else {
            TapList<Tree> thenSons = generateTreeChained(thenBody,
                    uniqueFollow != null, null, toFutureIncludes, fileUserHelpStrings);
            if (thenSons != null && thenSons.tail == null) {
                thenTree = thenSons.head;
            } else {
                thenTree = ILUtils.build(ILLang.op_blockStatement, thenSons);
            }
        }
        if (elseArrow != null && elseArrow.carry != null) {
            if (uniqueFollow == null) {
                elseTree = turnCarryIntoJump(elseArrow);
            } else {
                elseTree = ILUtils.build(ILLang.op_none);
            }
        } else {
            TapList<Tree> elseSons = generateTreeChained(elseBody,
                    uniqueFollow != null, null, toFutureIncludes, fileUserHelpStrings);
            if (elseSons != null && elseSons.tail == null) {
                elseTree = elseSons.head;
            } else {
                elseTree = ILUtils.build(ILLang.op_blockStatement, elseSons);
            }
        }
        if (ILUtils.isNotNoneNorEmpty(thenTree)) {
            structuredTree.setChild(thenTree, 2);
            structuredTree.setChild(elseTree, 3);
        } else {
            if (ILUtils.isNotNoneNorEmpty(elseTree)) {
                Tree testTree = structuredTree.cutChild(1);
                int negOp = -99;
                switch (testTree.opCode()) {
                    case ILLang.op_eq:
                        negOp = ILLang.op_neq;
                        break;
                    case ILLang.op_neq:
                        negOp = ILLang.op_eq;
                        break;
                    case ILLang.op_gt:
                        negOp = ILLang.op_le;
                        break;
                    case ILLang.op_ge:
                        negOp = ILLang.op_lt;
                        break;
                    case ILLang.op_lt:
                        negOp = ILLang.op_ge;
                        break;
                    case ILLang.op_le:
                        negOp = ILLang.op_gt;
                        break;
                    default:
                        break;
                }
                Tree testTreeOpposite;
                if (negOp == -99) {
                    testTreeOpposite = ILUtils.build(ILLang.op_not, testTree);
                } else {
                    testTreeOpposite = ILUtils.build(negOp, testTree.cutChild(1), testTree.cutChild(2));
                    if (testTree.getAnnotation("boolean") != null) {
                        testTreeOpposite.setAnnotation("boolean", "boolean");
                    }
                }
                structuredTree.setChild(testTreeOpposite, 1);
                structuredTree.setChild(elseTree, 2);
                structuredTree.setChild(thenTree, 3);
            } else {
                /*TODO: If the "then" part AND the "else" part are empty,
                 * we should probably remove the whole "if" ! */
                if (structuredTree.length() >= 3) {
                    structuredTree.setChild(thenTree, 2);
                    structuredTree.setChild(elseTree, 3);
                }
            }
        }
        if (uniqueFollow != null && !delayGoto) {
            Tree followJump = null;
            FGArrow followArrow;
            while (uniqueFollow != null) {
                followArrow = uniqueFollow.head;
                if (followJump == null) {
                    followJump = turnCarryIntoJump(followArrow);
                }
                followArrow.carry = null;
                uniqueFollow = uniqueFollow.tail;
            }
            if (followJump != null) {
                insertInstructionTree(followJump, true);
            }
        }
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("IfThenElse ");
        super.dump(indent);
        TapEnv.print(" Then:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = thenBody; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("} Else:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = elseBody; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("}");
    }

    @Override
    public String toString() {
        return "IF:" + controlStructBlock + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions) + ":(" + thenBody + ")(" + elseBody + ')';
    }
}
