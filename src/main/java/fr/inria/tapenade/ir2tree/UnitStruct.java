/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.*;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

final class UnitStruct extends ControlStruct {
    private final Unit unit;
    private final boolean isInterface;
    private final SymbolTable symbolTableInside;
    protected TapList<ControlStruct> body;
    private FGArrow entryArrow;
    private Tree paramsListTree = ILUtils.build(ILLang.op_varDeclarations);
    private TapList<Instruction> publicDeclarationsInstructions;
    private boolean traceThisUnit;

    protected UnitStruct(Unit unit, boolean isInterface) {
        this.unit = unit;
        this.controlStructBlock = unit.entryBlock;
        symbolTableInside = unit.bodySymbolTable();
        if (unit.entryBlock != null && unit.entryBlock.flow() != null) {
            this.entryArrow = unit.entryBlock.flow().head;
        }
        this.isInterface = isInterface;
    }

    protected void setTraceThisUnit(boolean traceThisUnit) {
        this.traceThisUnit = traceThisUnit;
    }

    @Override
    protected ControlStruct getBlock(Block block) {
        ControlStruct result = null;
        if (this.controlStructBlock == block) {
            result = this;
        } else {
            TapList<ControlStruct> inBody = body;
            while (inBody != null && result == null) {
                result = inBody.head.getBlock(block);
                inBody = inBody.tail;
            }
        }
        return result;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        body = new TapList<>(struct, body);
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's
     * and also nested local parallel controls (e.g. OMP).
     */
    @Override
    protected void insertLetStructure() {
        body = insertLetStructureChained(body, symbolTableInside);
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position.
     */
    @Override
    protected void reorderBody() {
        body = reorderBodyChained(body);
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        propagateNaturalNextChained(body, null);
        this.naturalNext = null;
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        naturalFlow = propagateNaturalFlowChained(body);
        return null;
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows,
                                               TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings,
                                               TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        unit.publicSymbolTable().declarationsBlock.checkUnusedDeclaration(unit);
        publicDeclarationsInstructions = unit.publicSymbolTable().declarationsBlock.instructions;

        Block declBlock;
        TapList<Block> allBlocks = (isInterface || TapEnv.get().createStub ? null : unit.allBlocks);
        if ((isInterface || TapEnv.get().createStub || unit.isModule()) && //[FEWER NONREGRESSION] ? Added to limit differences ? maybe remove?
                unit.privateSymbolTable() != null) {
            declBlock = symbolTableInside.declarationsBlock;
            allBlocks = TapList.addUnlessPresent(allBlocks, declBlock);
        }
        if ((isInterface || TapEnv.get().createStub || unit.isModule()) && //[FEWER NONREGRESSION] ? Added to limit differences ? maybe remove?
                unit.publicSymbolTable() != null) {
            declBlock = unit.publicSymbolTable().declarationsBlock;
            allBlocks = TapList.addUnlessPresent(allBlocks, declBlock);
        }

        if (!unit.isClass()) {
            // Prepare the Block that may receive extra missing declarations:
            Block blockForExtraDecls = null ;
            if (unit.privateSymbolTable()!=null) {
                blockForExtraDecls = unit.privateSymbolTable().declarationsBlock ;
            }
            // Weird cases occurring on empty Unit's:
            if (blockForExtraDecls==null || blockForExtraDecls instanceof ExitBlock) {
                blockForExtraDecls = (allBlocks==null ? null : allBlocks.head) ;
            }
            for (TapList<Block> blocks = allBlocks; blocks != null; blocks = blocks.tail) {
                Block block = blocks.head;
                block.instructions =
                    preGenerateListOfInstructions(block, toFutureIncludes, block==blockForExtraDecls);
            }
        }

        TapList<FGArrow> afterBody = preGenerateTreeChained(body, toFutureIncludes, fileUserHelpStrings,
                new TapList<>(entryArrow, null),
                new TapList<>(this, enclosingStructs),
                skipSubUnits);
        buildStructuredTree();
        if (unit.preComments != null) {
            structuredTree.setAnnotation("preComments", ILUtils.copy(unit.preComments));
        }
        if (unit.postComments != null) {
            structuredTree.setAnnotation("postComments", ILUtils.copy(unit.postComments));
        }
        if (unit.preCommentsBlock != null) {
            structuredTree.setAnnotation("preCommentsBlock", ILUtils.copy(unit.preCommentsBlock));
        }
        if (unit.postCommentsBlock != null) {
            structuredTree.setAnnotation("postCommentsBlock", ILUtils.copy(unit.postCommentsBlock));
        }
        insertInstructionTree(structuredTree, true);
        return afterBody;
    }

    //TODO: look for refactoring with TreeGen.generateReverseListOfTree()
    private TapList<Instruction> preGenerateListOfInstructions(Block block,
                                 TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, boolean onFirstBlock) {
// boolean traceHere = unit.name().startsWith("test_primitives_u") ;
        if (!unit.isC()) {
            // Needed if someone has added a Push, Pop, or other operation, at the start
            // of the first Block, which starts with declarations (One shouldn't do that!)
            block.sortDeclarationsAndOperations();
        }

// if (traceHere) {
// System.out.println("INSTS AT POINT 0  (onFirstBlock="+onFirstBlock+") :");
// TapList<Instruction> contents = block.instructions ;
// while (contents!=null) {
//     System.out.print("  ===> ") ;
//     try{contents.head.dump();}catch(Exception e){} ;
//     System.out.println(); contents=contents.tail;
// }
// }

        if (onFirstBlock
                // Don't add anything if in Fortran and at the global, file level:
                && !(unit.isTranslationUnit() && unit.isFortran())) {
            //Take care of the Unit's public and private declarations:
            // First, add a declaration into the public declarationBlock for each
            //  public SymbolDecl that still misses a declaration Tree:
            assert unit.publicSymbolTable() != null;
            Block publicDeclarationBlock = unit.publicSymbolTable().declarationsBlock;
            TapList<TapPair<Tree, SymbolDecl>> missingDecls =
                    TreeGen.generateMissingDeclarations(unit, unit.publicSymbolTable(),
                            false, true, publicDeclarationBlock, traceThisUnit);
            while (missingDecls != null) {
                TapPair<Tree, SymbolDecl> missingInfo = missingDecls.head;
                if (unit.isModule()) {
                    publicDeclarationBlock.addInstrDeclTlBeforeUse(
                            new Instruction(missingInfo.first),
                            new TapList<>(missingInfo.second, null),
                            null, unit.publicSymbolTable(), true);

                } else {
                    // pb: Il faut les ajouter avant les decls locales/privees
                    publicDeclarationBlock.addInstrHd(
                            new Instruction(missingInfo.first));
                }
                missingDecls = missingDecls.tail;
            }
            // next, if the publicDeclarationBlock is not a std Block of the Unit,
            // pour its instructions into the present 1st Block:
            // (But don't do this for Translation Units (files) because if their publicDeclarationBlock is not
            // a std Block, it's just the result of imperfect design: TODO fix that!)
            if (publicDeclarationBlock != block && publicDeclarationBlock.instructions != null && !unit.isTranslationUnit()) {
                if (!(block instanceof BasicBlock)) {
                    //[llh] this case should not happen: someone must have added an initial BasicBlock !
                    TapEnv.fileWarning(-1, null, "Unit should start with a plain controlStructBlock for declarations!");
                }
                TapList<Instruction> publicDeclInstructions = publicDeclarationBlock.instructions;
                if (traceThisUnit) {
                    TapEnv.printlnOnTrace("  Inserting public declarations from " + publicDeclarationBlock
                            + '@' + Integer.toHexString(publicDeclarationBlock.hashCode())
                            + " -> " + publicDeclInstructions);
                }
                while (publicDeclInstructions != null) {
                    Instruction publicDeclInstr = publicDeclInstructions.head;
                    if (//Don't copy a sub-Unit definition
                            publicDeclInstr.tree != null && publicDeclInstr.tree.getAnnotation("Unit") == null
                                    //Don't copy the current procedure's header:
                                    && publicDeclInstr.tree.opCode() != ILLang.op_call) {
                        if (publicDeclInstr.tree.opCode() == ILLang.op_useDecl) {
                            // c'est une declaration useDecl AATYPES rajoutee si assoc by address
                            block.addInstrHd(publicDeclInstr);
                        } else {
                            block.addInstrDeclTlBeforeUse(publicDeclInstr,
                                    block.symbolDeclDeclared(publicDeclInstr),
                                    null, unit.publicSymbolTable(), true);
                        }
                    }
                    publicDeclInstructions = publicDeclInstructions.tail;
                }
            }
            publicDeclarationsInstructions = publicDeclarationBlock.instructions;
            if (publicDeclarationBlock != block) {
                publicDeclarationBlock.instructions = null;
            }
            // finally, add a declaration into the present 1st Block for each
            //  private SymbolDecl that still misses a declaration Tree:
            if (symbolTableInside.declarationsBlock.rank == -2) { //cf unit.isDeadPrivateDeclarationBlock()
                TapList<Instruction> privateDeclInstructions = symbolTableInside.declarationsBlock.instructions;
                block.addInstrDeclTl(privateDeclInstructions);
            }
            if (!unit.isTranslationUnit()) {
                missingDecls =
                        TreeGen.generateMissingDeclarations(unit, symbolTableInside,
                                false, false, block, traceThisUnit);
            }
            while (missingDecls != null) {
                TapPair<Tree, SymbolDecl> missingInfo = missingDecls.head;
                block.addInstrDeclTlBeforeUse(new Instruction(missingInfo.first),
                        new TapList<>(missingInfo.second, null),
                        null, symbolTableInside, true);
                missingDecls = missingDecls.tail;
            }
        }

// if (traceHere) {
// System.out.println("INSTS AT POINT 1:");
// TapList<Instruction> contents = block.instructions ;
// while (contents!=null) {
//     System.out.print("  ===> ") ;
//     try{contents.head.dump();}catch(Exception e){} ;
//     System.out.println(); contents=contents.tail;
// }
// }

        TapList<Instruction> insts = block.instructions;
        TapList<Instruction> newInsts = new TapList<>(null, null);
        TapList<Instruction> toNewInsts = newInsts;
        Instruction previousInstruction = null;
        boolean containsInclude = false;
        TapList<TapPair<Instruction, TapList<Instruction>>> toIncludesBeingBuilt = new TapList<>(null, null);
        while (insts != null) {
            Instruction instruction = insts.head;
            //Special case of an instruction that will be discarded, but that has a comment.
            // replace it with a op_none, so that the comment at least will appear.
            if ((instruction.tree==null || instruction.isAWhereControl())
                && (instruction.preComments!=null || instruction.preCommentsBlock!=null)) {
                Instruction dummyInstr = new Instruction(ILUtils.build(ILLang.op_none)) ;
                dummyInstr.preComments = instruction.preComments ;
                dummyInstr.preCommentsBlock = instruction.preCommentsBlock ;
                instruction = dummyInstr ;
            }
            if (ILUtils.isAUnitPlaceHolder(instruction.tree)) {
                newInsts = TreeGen.insertInstructionWatchingIncludes(
                        instruction, newInsts,
                        unit == unit.origUnit,
                        toIncludesBeingBuilt, toFutureIncludes);
            } else if (instruction.tree != null
                       // discard op_where<test><none><none>
                       && !instruction.isAWhereControl()
                       && !(instruction.tree.opCode() == ILLang.op_varDeclaration
                            && instruction.tree.down(3).length() == 0)
                       && (!TapEnv.get().createStub /*|| instruction.isADeclaration()*/)) {
                // Go back to Fortran95's pointer and array syntax.
                // [llh]: Probably not the right place to do so !
                //array syntax: rebuild where's
                TapList<TapPair<Tree, Boolean>> reversedMask = reverseMask(instruction.whereMask());
                if (reversedMask != null) {
                    if (previousInstruction != null &&
                            previousInstruction.whereMask() != null &&
                            !previousInstruction.whereMask().isEmpty() &&
                            insertInPreviousWhere(instruction.tree, reversedMask, previousInstruction.tree)) {
                        instruction.tree = null;
                    } else {
                        instruction.tree = buildWhereTree(reversedMask, instruction.tree);
                        // This forces flushing the last pending #include instructions:
                        newInsts = TreeGen.insertInstructionWatchingIncludes(
                                null, newInsts,
                                unit == unit.origUnit,
                                toIncludesBeingBuilt, toFutureIncludes);
                        newInsts = newInsts.placdl(instruction);
                        previousInstruction = instruction;
                    }
                } else if (instruction.isInStdCInclude()) {
                    // Only keep the top-level include itself, which is of the form:
                    // Instr:include <stdXXX.h> {from Instr:include <stdXXX.h>}
                    if (instruction.tree.opCode() == ILLang.op_include &&
                            instruction.fromIncludeRootName() != null &&
                            instruction.fromIncludeRootName().equals(instruction.tree.stringValue())) {
                        newInsts = newInsts.placdl(instruction);
                        previousInstruction = instruction;
                    }
                } else if (TapEnv.get().expandAllIncludeFile
                           || instruction.fromInclude() == null
                           || instruction.fromInclude().tree == null
                           || (instruction.fromInclude().tree.opCode() == ILLang.op_include
                               && instruction.fromInclude().tree.stringValue().isEmpty())) {
                    // This forces flushing the last pending #include instructions:
                    newInsts = TreeGen.insertInstructionWatchingIncludes(
                            null, newInsts,
                            unit == unit.origUnit,
                            toIncludesBeingBuilt, toFutureIncludes);
                    if (!(TapEnv.get().expandAllIncludeFile
                            && instruction.tree.opCode() == ILLang.op_include
                            && !TapEnv.isStdIncludeName(instruction.tree.stringValue()))) {
                        newInsts = newInsts.placdl(instruction);
                        previousInstruction = instruction;
                    } else if (instruction.preComments!=null || instruction.preCommentsBlock!=null) {
                        Instruction dummyInstr = new Instruction(ILUtils.build(ILLang.op_none)) ;
                        dummyInstr.preComments = instruction.preComments ;
                        dummyInstr.preCommentsBlock = instruction.preCommentsBlock ;
                        newInsts = newInsts.placdl(dummyInstr);
                    }
                } else {
                    containsInclude = true;
                    if (instruction.tree != null && instruction.tree.equalsTree(instruction.fromInclude().tree)) {
                        instruction.fromInclude().preComments = instruction.preComments;
                        instruction.fromInclude().preCommentsBlock = instruction.preCommentsBlock;
                        instruction.fromInclude().postComments = instruction.postComments;
                        instruction.fromInclude().postCommentsBlock = instruction.postCommentsBlock;
                    }
                    newInsts = TreeGen.insertInstructionWatchingIncludes(
                            instruction, newInsts,
                            unit == unit.origUnit,
                            toIncludesBeingBuilt, toFutureIncludes);
                    previousInstruction = instruction;
                }
            }
            insts = insts.tail;
        }
        // This forces flushing the last pending #include instructions:
        newInsts = TreeGen.insertInstructionWatchingIncludes(
                null, newInsts,
                unit == unit.origUnit,
                toIncludesBeingBuilt, toFutureIncludes);
        newInsts = toNewInsts.tail;

// if (traceHere) {
// System.out.println("NEWINSTS AT POINT 2:") ;
// TapList<Instruction> contents = newInsts ;
// while (contents!=null) {
//     System.out.print("  ===> ") ;
//     try{contents.head.dump();}catch(Exception e){} ;
//     System.out.println(); contents=contents.tail;
// }
// }

        // Remove repeated include commands (maybe not really necessary?) cf set02/v053 set10/lh070.
        if (containsInclude) {
            toNewInsts = new TapList<>(null, newInsts);
            TapList<Instruction> inNewInsts = toNewInsts;
            TapList<Tree> seenIncludes = null;
            Instruction curInstr;
            while (inNewInsts.tail != null) {
                curInstr = inNewInsts.tail.head;
                if (curInstr.tree != null && curInstr.tree.opCode() == ILLang.op_include) {
                    if (TapList.containsTreeEquals(seenIncludes, curInstr.tree)) {
                        inNewInsts.tail = inNewInsts.tail.tail;
                    } else {
                        seenIncludes = new TapList<>(curInstr.tree, seenIncludes);
                        inNewInsts = inNewInsts.tail;
                    }
                } else {
                    inNewInsts = inNewInsts.tail;
                }
            }
            newInsts = toNewInsts.tail;
        }
        return newInsts;
    }

    private TapList<Instruction> buildIncludeChain(Instruction instruction) {
        Instruction includeInstr = instruction.fromInclude();
        if (instruction.tree.opCode() == ILLang.op_include && includeInstr != null &&
                instruction.tree.stringValue().equals(includeInstr.tree.stringValue())) {
            // Because of choice that Instr:include "foo.h" is {from Instr:include "foo.h"}:
            instruction = includeInstr;
        }
        TapList<Instruction> chain = new TapList<>(instruction, null);
        Instruction fromInclude = instruction.fromInclude();
        while (fromInclude != null) {
            chain = new TapList<>(fromInclude, chain);
            fromInclude = fromInclude.fromInclude();
        }
        return chain;
    }

    private void buildStructuredTree() {
        if (unit.isModule()) {
            structuredTree = ILUtils.build(ILLang.op_module);
        } else if (unit.isClass()) {
            structuredTree = ILUtils.copy(unit.entryBlock.lastInstr().tree);
        } else if (unit == unit.callGraph().getMainUnit() && unit.language() != TapEnv.C) {
            structuredTree = ILUtils.build(ILLang.op_program);
        } else if (unit.isProcedure()) {
            structuredTree = ILUtils.build(ILLang.op_function);
        } else {
            structuredTree = ILUtils.build(ILLang.op_blockStatement);
        }
    }

    protected void preGenerateForwardTree() {
        buildStructuredTree();
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                TapList<Tree> fileUserHelpStrings) {
        Block publicDeclBlock = unit.publicSymbolTable().declarationsBlock;
        Block privateDeclBlock = symbolTableInside.declarationsBlock;
        TapList<Tree> declsAndStats = null;
        ToObject<Tree> toFunctionTypeDeclTree = new ToObject<>(null);

        if (!((isInterface || unit.isPackage())
                && unit.allBlocks != null && publicDeclBlock == unit.allBlocks.head)
                && !unit.isTranslationUnit() // TODO: simplify with condition above!!
                && unit.isC() //No global declarations in files in Fortran
                && TapList.length(publicDeclBlock.instructions) != 0) {
            declsAndStats = TapList.reverse(
                    TreeGen.generateReverseListOfTree(publicDeclBlock, toFutureIncludes,
                            fileUserHelpStrings, isInterface/*, skipSubUnits*/));
        }

        // Prepare the Tree for the return type of a function :
        Tree returnTypeTree = null;
        TapList<SymbolDecl> externalTypes = null;
        if (unit.externalSymbolTable() != null) {
            externalTypes = unit.externalSymbolTable().getAllTypeDecls();
        }
        if (structuredTree.opCode() == ILLang.op_function) {
            returnTypeTree = unit.buildReturnTypeTree(privateDeclBlock,
                    externalTypes, toFunctionTypeDeclTree, publicDeclarationsInstructions);
        }
        if (traceThisUnit) {
            System.out.println("BUILD RETURN TYPE TREE OF " + unit + " FTS:" + unit.functionTypeSpec() + " ==> " + returnTypeTree);
        }

        // Tail-Insert the main "body" part:
        TapList<Tree> bodyDeclsAndStats = generateTreeChained(body, false, null, toFutureIncludes, fileUserHelpStrings);

        declsAndStats = TapList.append(declsAndStats, bodyDeclsAndStats);

        // Tail-Insert the FORMAT declarations:
        TapList<Tree> formats = TapList.reverse(unit.formats());
        if (!isInterface && !unit.isModule() && formats != null) {
            while (formats != null) {
                declsAndStats = TapList.addLast(declsAndStats, formats.head);
                formats = formats.tail;
            }
        }

        if (toFunctionTypeDeclTree.obj() != null) {
            declsAndStats = Block.addTreeDeclTl(declsAndStats, new TapList<>(toFunctionTypeDeclTree.obj(), null));
        }

        // [SUBUNITS] For the time being, adding of sub-Unit definitions is done here (and in Tapenade) for pure C and Fortran's.
        // Alternatively, it could be done in generateReverseListOfTree() (see [SUBUNITS]),
        //  which might be better because it would preserve the original declarations/definitions order.
        if (!unit.isCPlusPlus() && !unit.isTranslationUnit()) {
            // Tail-Insert the definitions of the "contained" procedures stored in "nonFlowInstructions":
            TapList<Instruction> nonFlowInstructions = unit.nonFlowInstructions;
            Tree nonFlowTree;
            Unit subUnit;
            while (nonFlowInstructions != null) {
                nonFlowTree = nonFlowInstructions.head.tree;
                if (nonFlowTree.opCode() == ILLang.op_class ||
                        nonFlowTree.opCode() == ILLang.op_constructor ||
                        nonFlowTree.opCode() == ILLang.op_function) {
                    subUnit = nonFlowTree.getAnnotation("Unit");
                    if (subUnit != null) {
                        Tree unitTree = TreeGen.generate(subUnit, false, false,
                                false, toFutureIncludes,
                                unit.isC() ? fileUserHelpStrings : null);
                        if (unitTree != null) {
                            Tree[] newTrees = unitTree.children();
                            for (Tree newTree : newTrees) {
                                declsAndStats = TapList.addLast(declsAndStats, newTree);
                            }
                        }
                    }
                }
                nonFlowInstructions = nonFlowInstructions.tail;
            }
        }

        // Fill the final "structuredTree":
        if (unit.isModule()) {
            structuredTree.setChild(
                    ILUtils.build(ILLang.op_ident, unit.name()), 1);
            structuredTree.setChild(
                    ILUtils.build(ILLang.op_declarations, declsAndStats), 2);
        } else if (unit.isClass()) {
            structuredTree.setChild(ILUtils.build(ILLang.op_declarations, declsAndStats), 4);
        } else if (unit.isProcedure()) {
            if (unit.modifiers != null) {
                structuredTree.setChild(ILUtils.copy(unit.modifiers), 1);
            } else {
                structuredTree.setChild(ILUtils.build(ILLang.op_modifiers), 1);
            }

            int rank = 2; // op_function arity 6, op_program arity 4
            if (structuredTree.opCode() == ILLang.op_function) {
                if (returnTypeTree != null) {
                    structuredTree.setChild(returnTypeTree, rank);
                }
                rank = rank + 1;
                structuredTree.setChild(ILUtils.build(ILLang.op_none), rank);
                rank = rank + 1;
            }

            structuredTree.setChild(ILUtils.build(ILLang.op_ident, unit.name()), rank);
            Tree explicitReturnTree = null;
            // The explicit return Tree can be found either in the Unit's "otherReturnVar" field,
            // or as an annotation "explicitReturnVar" on the Unit's header "op_call" tree (down(1)!)
            if (unit.publicSymbolTable().getFunctionDecl(unit.name(), null, null, false) != null && unit.otherReturnVar() != null) {
                explicitReturnTree = ILUtils.build(ILLang.op_ident, unit.otherReturnVar().symbol);
            }
            Tree unitHeader = unit.headTree();
            if (explicitReturnTree == null && unitHeader != null) {
                if (unitHeader.opCode() == ILLang.op_call) {
                    explicitReturnTree = ILUtils.getCalledName(unitHeader).getAnnotation("explicitReturnVar");
                } else {
                    explicitReturnTree = unitHeader.down(1).getAnnotation("explicitReturnVar");
                }
            }
            if (explicitReturnTree != null) {
                structuredTree.down(rank).setAnnotation("explicitReturnVar", explicitReturnTree);
            }
            rank = rank + 1;
            structuredTree.setChild(paramsListTree, rank);
            rank = rank + 1;
            structuredTree.setChild(
                    ILUtils.build(ILLang.op_blockStatement, declsAndStats), rank);
        } else {
            // Unit is a Translation Unit:
            while (declsAndStats != null) {
                structuredTree.addChild(declsAndStats.head, -1);
                declsAndStats = declsAndStats.tail;
            }
        }
    }

    protected void generateForwardTree() {
        Block privateDeclBlock = null;
        if (unit.privateSymbolTable() != null) {
            privateDeclBlock = symbolTableInside.declarationsBlock;
        }
        Tree returnTypeTree = null;
        if (structuredTree.opCode() == ILLang.op_function) {
            returnTypeTree = unit.buildReturnTypeTree(privateDeclBlock, null,
                    null, publicDeclarationsInstructions);
        }
        if (unit.modifiers != null) {
            structuredTree.setChild(unit.modifiers, 1);
        } else {
            structuredTree.setChild(ILUtils.build(ILLang.op_modifiers), 1);
        }
        int rank = 2;
        if (structuredTree.opCode() == ILLang.op_function) {
            if (returnTypeTree != null) {
                structuredTree.setChild(returnTypeTree, 2);
            }
            rank = rank + 1;
            structuredTree.setChild(ILUtils.build(ILLang.op_none), 3);
            rank = rank + 1;
        }
        structuredTree.setChild(
                ILUtils.build(ILLang.op_ident, unit.name()), rank);
        rank = rank + 1;
        structuredTree.setChild(paramsListTree, rank);
        rank = rank + 1;
        structuredTree.setChild(
                ILUtils.build(ILLang.op_none), rank);
    }

    protected void generateParameterList() {
        if (!unit.isModule()) {
            paramsListTree = ILUtils.build(ILLang.op_varDeclarations,
                    unit.generateHeaderParamsList(unit.publicSymbolTable()));
        }
    }

    /**
     * set usedLabels.
     */
    protected void findUsedLabels() {
        /*reinitialiser pour chaque unit
         */
        TapEnv.get().nextNewLabel = 100;
        TapEnv.get().usedLabels = null;
        TapList<Block> allBlocks =
                new TapList<>(unit.exitBlock(), unit.allBlocks());
        Block block;
        String label;
        while (allBlocks != null) {
            block = allBlocks.head;
            if ((label = block.origLabel()) != null) {
                TapEnv.get().usedLabels =
                        new TapList<>(label, TapEnv.get().usedLabels);
            }
            if (block instanceof HeaderBlock
                    && (label = ((HeaderBlock) block).origCycleLabel()) != null) {
                TapEnv.get().usedLabels =
                        new TapList<>(label, TapEnv.get().usedLabels);
            }
            allBlocks = allBlocks.tail;
        }
        TapList<Tree> formats = unit.formats();
        Tree formatTree;
        while (formats != null) {
            formatTree = formats.head;
            TapEnv.get().usedLabels =
                    new TapList<>(formatTree.down(1).stringValue(), TapEnv.get().usedLabels);
            formats = formats.tail;
        }
    }

    private TapList<TapPair<Tree, Boolean>> reverseMask(InstructionMask mask) {
        TapList<TapPair<Tree, Boolean>> result = null;
        boolean neg;
        Tree maskTest;
        while (mask != null && !mask.isEmpty()) {
            neg = !mask.isTrueBranch;
            maskTest = mask.getControlTree();
            while (maskTest.opCode() == ILLang.op_not) {
                neg = !neg;
                maskTest = maskTest.down(1);
            }
            result = new TapList<>(new TapPair<>(maskTest, neg ? Boolean.TRUE : Boolean.FALSE), result);
            mask = mask.enclosingMask;
        }
        return result;
    }

    /**
     * Tries to insert the new statement "newTree" at the tail of the previous statement "previousWhere"
     * (which must be a WHERE statement) at a position that corresponds to the mask "newMask".
     *
     * @return true if this insertion is possible (and is now done), false otherwise.
     */
    private boolean insertInPreviousWhere(Tree newTree, TapList<TapPair<Tree, Boolean>> newMask, Tree previousWhere) {
        // If one tree is in a DO-multiDir, and the other is not, don't merge !! :
        if (newTree.opCode() == ILLang.op_loop) {
            if (previousWhere.opCode() != ILLang.op_loop) {
                return false;
            }
        } else {
            if (previousWhere.opCode() == ILLang.op_loop) {
                return false;
            }
        }
        int targetCase = getCaseOfTest(newMask.head, previousWhere);
        if (targetCase == 3
                // ^ we can insert into the ELSEWHERE part of previousWhere
                ||
                targetCase == 2 && ILUtils.isNullOrNoneOrEmptyList(previousWhere.down(3))
            // ^ we can insert into the WHERE part of previousWhere
        ) {
            // then do the insertion:
            insertInWhereBranch(newTree, newMask.tail, previousWhere, targetCase);
            return true;
        } else {
            // else insertion into previousWhere is impossible:
            return false;
        }
    }

    /**
     * Inserts statement "newTree", with mask "newMask", at the tail of the
     * "caseRank" case (i.e. son) of the WHERE statement "whereTree".
     */
    private void insertInWhereBranch(Tree newTree, TapList<TapPair<Tree, Boolean>> newMask,
                                     Tree whereTree, int caseRank) {
        // given "newTree" and "whereTree" may be DO loops (when multiDir).
        // Go under the DO:
        if (whereTree.opCode() == ILLang.op_loop) {
            whereTree = whereTree.down(4);
        }
        // We are pretty sure that "newTree" and "whereTree" have the same multiDir structure:
        if (newTree.opCode() == ILLang.op_loop) {
            newTree = newTree.cutChild(4);
        }

        Tree caseTree = whereTree.down(caseRank);
        if (ILUtils.isNullOrNoneOrEmptyList(caseTree)) {
            whereTree.setChild(ILUtils.build(ILLang.op_blockStatement, buildWhereTree(newMask, newTree)), caseRank);
        } else {
            if (caseTree.opCode() != ILLang.op_blockStatement) {
                caseTree = ILUtils.build(ILLang.op_blockStatement, whereTree.cutChild(caseRank));
                whereTree.setChild(caseTree, caseRank);
            }
            int caseLength = caseTree.length();
            Tree lastInCase = caseTree.down(caseLength);
            if (newMask == null
                    || lastInCase.opCode() != ILLang.op_where
                    || !insertInPreviousWhere(newTree, newMask, lastInCase)) {
                caseTree.addChild(buildWhereTree(newMask, newTree), caseLength + 1);
            }
        }
    }

    /**
     * Identifies the test "oneMask" (possibly negated) in the test "whereTest" of a WHERE statement.
     *
     * @return 2 if the test is the WHERE part, 3 if it is the ELSEWHERE part, -1 otherwise.
     */
    private int getCaseOfTest(TapPair<Tree, Boolean> oneMask, Tree whereTree) {
        // given "whereTree" may be a DO loop (when multiDir).
        // Go under the DO:
        if (whereTree.opCode() == ILLang.op_loop) {
            whereTree = whereTree.down(4);
        }
        Tree whereTest = whereTree.down(1);
        Tree maskTree = oneMask.first;
        boolean neg = Boolean.TRUE.equals(oneMask.second);
        while (whereTest.opCode() == ILLang.op_not) {
            neg = !neg;
            whereTest = whereTest.down(1);
        }
        if (whereTest.equalsTree(maskTree)) {
            return neg ? 3 : 2;
        } else {
            return -1;
        }
    }

    private Tree buildWhereTree(TapList<TapPair<Tree, Boolean>> newMask, Tree newTree) {
        // given "newTree" may be a DO (when multiDir).
        // Peel the DO off and put it back in the end:
        Tree doTree = null;
        if (newTree.opCode() == ILLang.op_loop) {
            doTree = newTree.cutChild(3);
            newTree = newTree.cutChild(4);
        }
        if (newMask != null) {
            newTree = buildWhereTree(newMask.tail, newTree);
            TapPair<Tree, Boolean> oneMask = newMask.head;
            Tree testTree = ILUtils.copy(oneMask.first);
            // il faudrait faire:
            //testTree = TreeGen.changeToFortranStylePointers(testTree, symbolTable, null) ;
            // mais pb lors de la comparaison des testTree
            newTree = ILUtils.build(ILLang.op_where,
                    Boolean.TRUE.equals(oneMask.second)
                            ? ILUtils.build(ILLang.op_not, testTree)
                            : testTree,
                    ILUtils.build(ILLang.op_blockStatement, newTree),
                    ILUtils.build(ILLang.op_none),
                    ILUtils.copy(testTree.getAnnotation("namedWhere")));
            testTree.removeAnnotation("namedWhere");
        }
        // multiDir case.
        if (doTree != null) {
            newTree = ILUtils.build(ILLang.op_loop,
                    ILUtils.build(ILLang.op_none),
                    ILUtils.build(ILLang.op_none),
                    doTree,
                    newTree);
        }
        return newTree;
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("Unit ");
        super.dump(indent);
        TapEnv.print(" Contents:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = body; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.print("}");
        TapEnv.println();
    }

    @Override
    public String toString() {
        return "UNIT:" + controlStructBlock + " @" + Integer.toHexString(controlStructBlock.hashCode())
                + " INSTRS:" + controlStructBlock.instructions + ":(" + body + ')';
    }
}
