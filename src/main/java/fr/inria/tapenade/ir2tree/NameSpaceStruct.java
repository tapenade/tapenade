/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.ILLangOps;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * This structure represents a piece of Flow Graph that defines (a part of) a namespace.
 * Therefore, the Flow Graph this is a part of, is not really a flow but rather an ordered,
 * sequential list of declarations/definitions, with no instructions.
 * Therefore this structure is quite simple, with a single entry arrow (arrowIntoBody),
 * a single exit arrow, and a linear chain of ControlStruct as a body.
 */
final class NameSpaceStruct extends ControlStruct {

    private final FGArrow arrowIntoBody;
    private TapList<ControlStruct> body;

    protected NameSpaceStruct(Block block) {
        this.controlStructBlock = block;
        arrowIntoBody = block.flow().head;
    }

    @Override
    protected ControlStruct getBlock(Block block) {
        ControlStruct result = null;
        if (this.controlStructBlock == block) {
            result = this;
        } else {
            TapList<ControlStruct> inBody = body;
            while (inBody != null && result == null) {
                result = inBody.head.getBlock(block);
                inBody = inBody.tail;
            }
        }
        return result;
    }

    @Override
    protected void addControlStruct(ControlStruct struct, FGArrow arrow) {
        body = new TapList<>(struct, body);
    }

    /**
     * (Recursive) Inserts Let structures into the tree
     * of ControlStruct's to reflect nested local SymbolTable's.
     */
    @Override
    protected void insertLetStructure() {
        if (body != null) {
            body = insertLetStructureChained(body, body.head.controlStructBlock.symbolTable);
        }
    }

    /**
     * (Recursive) Orders the lists of control structures inside,
     * following all available indications on position...
     */
    @Override
    protected void reorderBody() {
        body = reorderBodyChained(body);
    }

    /**
     * (Recursive) precomputation of the "naturalNext" info:
     * the next Block reached by natural flow from each ControlStruct.
     */
    @Override
    protected void propagateNaturalNext(Block naturalNext) {
        this.naturalNext = naturalNext;
        propagateNaturalNextChained(body, naturalNext);
    }

    /**
     * (Recursive) precomputation of the "naturalFlow: info:
     * the TapList of FGArrow that would naturally flow out of
     * this ControlStruct, i.e. without any additional explicit jump.
     */
    @Override
    protected TapList<FGArrow> propagateNaturalFlow() {
        return propagateNaturalFlowChained(body);
    }

    /**
     * (Recursive) preparation before tree regeneration. Labels the arrows that
     * need explicit jumps, prepares the skeleton of the future Tree.
     *
     * @return a TapList of FGArrow
     */
    @Override
    protected TapList<FGArrow> preGenerateTree(TapList<FGArrow> naturalArrows, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes,
                                               TapList<Tree> fileUserHelpStrings, TapList<ControlStruct> enclosingStructs, boolean skipSubUnits) {
        structuredTree = preGenerateBody(controlStructBlock.instructions, naturalArrows,
                toFutureIncludes, fileUserHelpStrings, skipSubUnits);
        if (structuredTree.opCode() == ILLang.op_labelStatement) {
            structuredTree = structuredTree.down(2);
        }
        return preGenerateTreeChained(body, toFutureIncludes, fileUserHelpStrings,
                new TapList<>(arrowIntoBody, null),
                new TapList<>(this, enclosingStructs), skipSubUnits);
    }

    /**
     * (Recursive) final regeneration of the structured Tree.
     */
    @Override
    protected void generateTree(boolean delayGoto, TapList<TapTriplet<Tree, Tree, Integer>> toFutureIncludes, TapList<Tree> fileUserHelpStrings) {
        Tree bodyTree;
        TapList<Tree> bodySons = generateTreeChained(body, true, null, toFutureIncludes, fileUserHelpStrings);
        if (bodySons != null && bodySons.tail == null) {
            bodyTree = bodySons.head;
            if (bodyTree.opCode() == ILLang.op_blockStatement) {
                // if bodyTree is a op_blockStatement, turn it into an op_declarations:
                bodyTree.setOperator(ILLangOps.ops(ILLang.op_declarations));
            } else if (bodyTree.opCode() != ILLang.op_declarations) {
                // else if bodyTree is not already a op_declarations, op_nameSpace requires an op_declarations anyway:
                bodyTree = ILUtils.build(ILLang.op_declarations, bodyTree);
            }
        } else {
            bodyTree = ILUtils.build(ILLang.op_declarations, bodySons);
        }
        // TODO: If the "body" part is empty, we should probably remove the whole nameSpace
        // if (ILUtils.isNotNoneNorEmpty(bodyTree))
        structuredTree.setChild(bodyTree, 2);
    }

    @Override
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print("NameSpace ");
        super.dump(indent);
        TapEnv.print(" Contents:{");
        TapEnv.println();
        for (TapList<ControlStruct> inBody = body; inBody != null; inBody = inBody.tail) {
            TapEnv.indent(indent + 2);
            inBody.head.dump(indent + 2);
            TapEnv.println();
        }
        TapEnv.indent(indent);
        TapEnv.print("}");
    }

    @Override
    public String toString() {
        return "NS:" + controlStructBlock + (controlStructBlock == null ? "" : " @" + Integer.toHexString(controlStructBlock.hashCode()) + " INSTRS:" + controlStructBlock.instructions) + ":(" + body + ')';
    }
}
