/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.Tree;

/**
 * Abstract class for all structures.
 */
public abstract class DeclStruct {

    protected TapList<SymbolDecl> usedNames;
    /**
     * Kind of declaration, used for a preferred order between declarations:
     * first come USE (0), then TYPES (1), then CONSTANTS (2), then VARIABLES (3),
     * then COMMONS (4), EQUIVALENCES (5), and FUNCTIONS (6).
     */
    protected int kind = -1;
    protected SymbolDecl symbolDecl;

    protected void addUsedSymbol(SymbolDecl usedSymbolDecl) {
        usedNames = new TapList<>(usedSymbolDecl, usedNames);
    }

    protected abstract Tree generateTree(TapList<SymbolDecl> declaredTypes, SymbolTable symbolTable);

    @Override
    public String toString() {
        return "Abstract DeclStruct !!!";
    }
}
