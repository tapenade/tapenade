/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.InterfaceDecl;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

final class InterfaceDeclStruct extends DeclStruct {
    private final TapList<InterfaceDecl> interfaceDecls;

    protected InterfaceDeclStruct(InterfaceDecl interfaceDecl) {
        this.interfaceDecls = new TapList<>(interfaceDecl, null);
        usedNames = interfaceDecl.dependsOn();
        kind = StructConstants.INTERFACE;
        symbolDecl = interfaceDecl;
    }

    @Override
    protected Tree generateTree(TapList<SymbolDecl> declaredTypes, SymbolTable symbolTable) {
        TapList<InterfaceDecl> inInterfaceDecls = interfaceDecls;
        InterfaceDecl interfaceDecl;
        Tree result = null;
        while (inInterfaceDecls != null) {
            interfaceDecl = inInterfaceDecls.head;
            if (interfaceDecl.contents != null) {
                result =
                        ILUtils.build(
                                ILLang.op_interfaceDecl, interfaceDecl.nameTree,
                                ILUtils.build(
                                        ILLang.op_interfaces, interfaceDecl.contents));
            } else {
                TapList<Unit> interfaceUnits = interfaceDecl.interfaceUnits();
                //next test is a dirty hack. To make it clean, InterfaceUnits and
                // functionDecls must be kept in a single list !
                if (interfaceUnits != null && interfaceUnits.head.rank() == -1) {
                    interfaceUnits = null;
                }
                if (interfaceUnits == null) {
                    TapList<FunctionDecl> functionDecls = interfaceDecl.functionDecls;
                    FunctionDecl functionDecl;
                    while (functionDecls != null) {
                        functionDecl = functionDecls.head;
                        if (functionDecl != null && functionDecl.unit() != null) {
                            interfaceUnits = new TapList<>(functionDecl.unit(), interfaceUnits);
                        }
                        functionDecls = functionDecls.tail;
                    }
                    interfaceUnits = TapList.nreverse(interfaceUnits);
                }
                TapList<Tree> hdTrees = new TapList<>(null, null);
                TapList<Tree> tlTrees = hdTrees;
                while (interfaceUnits != null) {
                    Unit interfaceUnit = interfaceUnits.head;
                    TapEnv.pushRelatedUnit(interfaceUnit);
                    Tree unitTree = TreeGen.generate(interfaceUnit, false, true, false,
                            new TapList<>(null, null), new TapList<>(null, null));
                    // We are (almost?) certain that unitTree contains only one child, which is the interfaceUnit's interface code:
                    if (unitTree != null) {
                        unitTree = unitTree.cutChild(1);
                    }
                    tlTrees = tlTrees.placdl(ILUtils.build(ILLang.op_interface, unitTree));
                    TapEnv.popRelatedUnit();
                    interfaceUnits = interfaceUnits.tail;
                }
                result =
                        ILUtils.build(ILLang.op_interfaceDecl,
                                interfaceDecl.nameTree,
                                ILUtils.build(ILLang.op_interfaces, hdTrees.tail));
            }
            inInterfaceDecls = inInterfaceDecls.tail;
        }
        return result;
    }

    @Override
    public String toString() {
        return "declaration of Interface " + interfaceDecls.head.symbol;
    }
}
