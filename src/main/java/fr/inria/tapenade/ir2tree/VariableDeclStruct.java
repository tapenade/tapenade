/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.ir2tree;

import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.ArrayTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.ModifiedTypeSpec;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

/**
 * Tree regeneration of Variable.
 */
public final class VariableDeclStruct extends DeclStruct {

    private final Tree generatedTree;
    private final TapList<VariableDecl> variableDecls;
    private WrapperTypeSpec sharedType;

    protected VariableDeclStruct(VariableDecl variableDecl, Tree tree) {
        generatedTree = tree;
        symbolDecl = variableDecl;
        variableDecls = new TapList<>(variableDecl, null);
        sharedType = variableDecl.type();
        // Do *NOT* regroup variable with same base types, but different type modifiers
        if (sharedType != null && sharedType.wrappedType != null) {
            if (TypeSpec.isA(sharedType, SymbolTableConstants.ARRAYTYPE)
                    && !containsNameEq(sharedType)
                    && !(TapEnv.relatedLanguageIsFortran()
                    && sharedType.wrappedType.elementType() != null
                    && sharedType.wrappedType.elementType()
                    .isPrimitiveTypeCharacter())
                    && sharedType.getAllDimensions() != null) {
                sharedType =
                        sharedType.wrappedType.elementType();
                while (TypeSpec.isA(sharedType, SymbolTableConstants.ARRAYTYPE)) {
                    sharedType =
                            sharedType.wrappedType.elementType();
                }
            } else if (!TapEnv.relatedLanguageIsFortran()
                    && TypeSpec.isA(sharedType, SymbolTableConstants.MODIFIEDTYPE)
                    && TypeSpec.isA(sharedType.wrappedType.elementType(), SymbolTableConstants.ARRAYTYPE)) {
                ModifiedTypeSpec modTypeSpec = (ModifiedTypeSpec) sharedType.wrappedType;
                sharedType =
                        new WrapperTypeSpec(
                                new ModifiedTypeSpec(modTypeSpec.elementType().wrappedType.elementType(), modTypeSpec));
            }
            if (TypeSpec.isA(sharedType, SymbolTableConstants.POINTERTYPE)
                    && !TapEnv.relatedLanguageIsFortran()) {
                if (((PointerTypeSpec) sharedType.wrappedType).offsetLength == null) {
                    sharedType = ((PointerTypeSpec) sharedType.wrappedType).destinationType;
                } else {
                    while (TypeSpec.isA(sharedType, SymbolTableConstants.POINTERTYPE)
                            && ((PointerTypeSpec) sharedType.wrappedType).offsetLength != null) {
                        sharedType = ((PointerTypeSpec) sharedType.wrappedType).destinationType;
                    }
                }
            }
        }
        usedNames = variableDecl.dependsOn();
        kind = variableDecl.isA(SymbolTableConstants.CONSTANT) ? StructConstants.CONSTANT : StructConstants.VARIABLE;
        symbolDecl = variableDecl;
    }

    /**
     * true if typeSpec is defined with nameEq spec.
     * Supposes that typeSpec is an ArrayTypeSpec.
     */
    private boolean containsNameEq(WrapperTypeSpec typeSpec) {
        boolean result = false;
        ArrayDim[] arrayDim = ((ArrayTypeSpec) typeSpec.wrappedType).dimensions();
        int i = 0;
        while (i < arrayDim.length && !result) {
            if (arrayDim[i].tree() != null && arrayDim[i].tree().opCode() == ILLang.op_dimColon) {
                result = arrayDim[i].tree().down(2).opCode() == ILLang.op_nameEq;
            }
            i = i + 1;
        }
        return result;
    }

    @Override
    public Tree generateTree(TapList<SymbolDecl> declaredTypes, SymbolTable symbolTable) {
        TapList<Tree> hdDeclarators = new TapList<>(null, null);
        TapList<Tree> tlDeclarators = hdDeclarators;
        TapList<VariableDecl> inVariableDecls = variableDecls;
        VariableDecl variableDecl;
        Tree result = null;
        while (inVariableDecls != null) {
            variableDecl = inVariableDecls.head;
            if (!((variableDecl.symbol.equals("true") || variableDecl.symbol.equals("false"))
                    && symbolTable.basisSymbolTable() == null)) {
                Tree declarator = generateDeclarator(variableDecl, sharedType, declaredTypes, symbolTable);
                if (variableDecl.isFunctionName) {
                    declarator.setAnnotation("isFunctionName", Boolean.TRUE);
                }
                tlDeclarators = tlDeclarators.placdl(declarator);
            }
            inVariableDecls = inVariableDecls.tail;
        }
        if (sharedType == null) {
            result = ILUtils.build(kind == StructConstants.CONSTANT ? ILLang.op_constDeclaration : ILLang.op_varDeclaration);
        } else if (hdDeclarators.tail != null) {
            if (hdDeclarators.tail.head.opCode() != ILLang.op_varDeclaration) {
                Tree sharedTypeTree =
                        sharedType.generateTree(null, variableDecls.head.dependsOn(), declaredTypes, true, null);
                if (symbolDecl.extraInfo() != null) {
                    sharedTypeTree = ILUtils.build(ILLang.op_modifiedType,
                            ILUtils.build(ILLang.op_modifiers,
                                    ILUtils.buildListIdents(symbolDecl.extraInfo())),
                            sharedTypeTree);
                }
                if (kind == StructConstants.CONSTANT) {
                    result = ILUtils.build(ILLang.op_constDeclaration,
                            sharedTypeTree,
                            ILUtils.build(ILLang.op_declarators, hdDeclarators.tail));
                } else {
                    result = ILUtils.build(ILLang.op_varDeclaration,
                            ILUtils.build(ILLang.op_modifiers),
                            sharedTypeTree,
                            ILUtils.build(ILLang.op_declarators, hdDeclarators.tail));
                }
            } else {
                result = hdDeclarators.tail.head;
            }
        }
        return result;
    }

    private Tree generateDeclarator(
            VariableDecl variableDecl,
            WrapperTypeSpec sharedTypeSpec, TapList<SymbolDecl> declaredTypes,
            SymbolTable symbolTable) {
        WrapperTypeSpec typeSpec = variableDecl.type();
        Instruction instr = null;
        if (symbolTable.declarationsBlock != null) //TEMP FIX OF A BUG FOR C++
        {
            instr = symbolTable.declarationsBlock.getOperatorDeclarationInstruction(
                    variableDecl, ILLang.op_varDeclaration, null);
        }
        Tree declarator = (generatedTree != null ? generatedTree : ILUtils.build(ILLang.op_ident, variableDecl.symbol)) ;
        if (variableDecl.isReference()) {
            declarator = ILUtils.build(ILLang.op_referenceDeclarator, declarator) ;
        }
        declarator = TypeSpec.addDeclaratorModifiers(declarator, variableDecl.accessInfo);
        if (variableDecl.hasInstructionWithRootOperator(ILLang.op_none, null) || instr == null) {
            declarator = TypeSpec.generateDeclaratorTree(sharedTypeSpec, declaredTypes, declarator, typeSpec);
            if (variableDecl.initializationTree() != null) {
                Tree initTree = ILUtils.copy(variableDecl.generateInitializationTree());
                initTree.setChild(declarator, 1);
                declarator = initTree;
            }
        } else {
            if (!TapEnv.relatedLanguageIsFortran()) {
                Tree origDecl = instr.tree;
                declarator = ILUtils.copy(origDecl);
                // utile quand on traduit du fortran en c, cf nonRegrC mix04
                int rankOfDeclVar = 0;
                if (origDecl.opCode() == ILLang.op_varDeclaration
                        && origDecl.down(3).length() > 1) {
                    int rank = 1;
                    boolean found = false;
                    while (!found && rank <= origDecl.down(3).length()) {
                        found = variableDecl.symbol.equals(ILUtils.baseName(origDecl.down(3).down(rank)));
                        if (found) {
                            rankOfDeclVar = rank;
                        }
                        rank = rank + 1;
                    }
                    rank = rankOfDeclVar + 1;
                    while (rank <= declarator.down(3).length()) {
                        declarator.down(3).removeChild(rank);
                    }
                    rank = rankOfDeclVar - 1;
                    while (rank >= 1) {
                        declarator.down(3).removeChild(rank);
                        rank = rank - 1;
                    }
                    origDecl.down(3).removeChild(rankOfDeclVar);
                } else {
                    instr.tree = null;
                    variableDecl.setNoneInstruction();
                }
            }
        }
        return declarator;
    }

    protected VariableDecl getFirstVariableDecl() {
        return variableDecls.head;
    }

    @Override
    public String toString() {
        StringBuilder names = new StringBuilder();
        TapList<VariableDecl> inVariableDecls = variableDecls;
        while (inVariableDecls != null) {
            names.append(',').append(inVariableDecls.head.symbol);
            inVariableDecls = inVariableDecls.tail;
        }
        return "declaration of " + sharedType + " variables " + names;
    }
}
