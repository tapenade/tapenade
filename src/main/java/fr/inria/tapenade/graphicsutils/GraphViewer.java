/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.graphicsutils;

import javax.swing.JFrame;
import javax.swing.UIManager;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
public final class GraphViewer {

    private static TapWindow tapWindow;
    private static Component contents;

    private GraphViewer() {
    }

    public static void open() {
        try {
            UIManager.setLookAndFeel(
                    UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException
                | ClassCastException | NullPointerException e) {
            System.out.println("GraphViewer "+e);
        }
        JFrame frame = new JFrame();
        tapWindow = new TapWindow();
        contents = tapWindow.createComponents();
        frame.getContentPane().add(contents);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                contents.invalidate();
            }
        });
        frame.pack();
        frame.setVisible(true);
    }

    public static void view(Object tapObject) {
        tapWindow.view(tapObject);
    }
}
