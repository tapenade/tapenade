/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class Node {
    private static Color bg = new Color(250, 220, 100);
    private static final Color fg = Color.black;
    protected double x;
    protected double y;
    protected double h2 = -1.0;
    protected double w2 = -1.0;
    protected boolean fixed;
    protected double speedX;
    protected double speedY;

    protected Node() {
    }

    protected void draw(Graphics graphics) {
        w2 = 32;
        h2 = 20;
        graphics.setColor(bg);
        graphics.fillOval(
                (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
        graphics.setColor(fg);
        graphics.drawOval(
                (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
    }

    protected void setSelected(boolean selected) {
        bg = selected ? Color.red : new Color(250, 220, 100);
    }
}
