/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.representation.ExitBlock;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class ExitBlockNode extends BlockNode {
    protected ExitBlockNode(ExitBlock exitBlock) {
        super(exitBlock, null, false);
        fg = Color.red;
    }

    @Override
    protected void draw(Graphics graphics) {
        w2 = h2 = graphics.getFontMetrics().getAscent() / (double) 2f;
        graphics.setColor(bg);
        graphics.fillOval(
                (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
        graphics.setColor(fg);
        graphics.drawOval(
                (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
        graphics.drawOval(
                (int) (x - w2 / 2f), (int) (y - h2 / 2f), (int) w2, (int) h2);
    }
}
