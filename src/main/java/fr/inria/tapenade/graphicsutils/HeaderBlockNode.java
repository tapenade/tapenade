/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.representation.HeaderBlock;

import java.io.ByteArrayOutputStream;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class HeaderBlockNode extends BlockNode {
    protected HeaderBlockNode(HeaderBlock headerBlock,
                              ByteArrayOutputStream byteStream, boolean withDecls) {
        super(headerBlock, byteStream, withDecls);
        doubleBorder = true;
    }
}
