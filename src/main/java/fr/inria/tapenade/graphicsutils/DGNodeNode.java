/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.differentiation.NewBlockGraphNode;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class DGNodeNode extends Node {
    private static final Color bg = new Color(255, 180, 255);
    private static final Color fg = Color.black;
    private String text;

    protected DGNodeNode(NewBlockGraphNode node) {
        super();
        this.text = node.text;
    }

    @Override
    protected void draw(Graphics graphics) {
        FontMetrics fm = graphics.getFontMetrics();
        graphics.setColor(bg);
        double w = fm.stringWidth(text) + (double) 10f;
        double h = fm.getHeight() + (double) 2f;
        w2 = w / 2;
        h2 = h / 2;
        graphics.fillRect(
                (int) (x - w / 2), (int) (y - h / 2), (int) w, (int) h);
        graphics.setColor(fg);
        graphics.drawRect(
                (int) (x - w / 2), (int) (y - h / 2), (int) w, (int) h);
        graphics.drawString(
                text, (int) (x - (w - 10) / 2),
                (int) (y - (h - 1) / 2) + fm.getAscent());
    }
}
