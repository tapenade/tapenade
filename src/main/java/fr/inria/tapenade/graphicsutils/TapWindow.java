/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.differentiation.NewBlockGraph;
import fr.inria.tapenade.differentiation.NewBlockGraphArrow;
import fr.inria.tapenade.differentiation.NewBlockGraphNode;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Panel;
import java.io.ByteArrayOutputStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class TapWindow {

    private final JLabel label = new JLabel();
    private final GraphPanel graphPanel = new GraphPanel();
    private Object displayedObject;
    private Object aboveObject;
    private int[] nodeIndexes;

    protected TapWindow() {
        super();
    }

    protected Component createComponents() {
        label.setText("Tapenade display");
        JPanel mainPanel = new JPanel();
        Panel controlPanel = new Panel();
        mainPanel.setBorder(
                BorderFactory.createEmptyBorder(10, 5, 5, 5));
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(label, BorderLayout.NORTH);
        mainPanel.add(graphPanel, BorderLayout.CENTER);
        mainPanel.add(controlPanel, BorderLayout.SOUTH);
        JButton buttonDown = new JButton();
        buttonDown.setText("Down");
        controlPanel.add(buttonDown);
        buttonDown.addActionListener(e -> view(selectedObject()));
        JButton buttonUp = new JButton();
        buttonUp.setText("Up");
        controlPanel.add(buttonUp);
        buttonUp.addActionListener(e -> view(aboveObject));
        return mainPanel;
    }

    protected void view(Object tapObject) {
        graphPanel.reset();
        if (tapObject instanceof CallGraph) {
            CallGraph callGraph = (CallGraph) tapObject;
            label.setText("CallGraph: " + callGraph.name());
            aboveObject = callGraph;
            displayedObject = callGraph;
            nodeIndexes = new int[callGraph.nbUnits()];
            Unit unit;
            TapList<Unit> unitsToDump = null;
            TapList<Unit> inUnitsToDump;
            // Collect units to dump and count them:
            for (int i = callGraph.nbUnits() - 1; i >= 0; --i) {
                unit = callGraph.sortedUnit(i);
                nodeIndexes[i] = -1;
                assert unit != null;
                if (!MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                        && !unit.inStdCIncludeFile()
                        && (unit.isStandard() || unit.isExternal() || unit.isModule())) {
                    unitsToDump = new TapList<>(unit, unitsToDump);
                }
            }
            int nbUnitsToDump = TapList.length(unitsToDump);
            // Count call arrows to dump:
            int nbEdgesToDump = 0;
            CallArrow arrow;
            inUnitsToDump = unitsToDump;
            while (inUnitsToDump != null) {
                unit = inUnitsToDump.head;
                TapList<CallArrow> callArrows = unit.callees();
                while (callArrows != null) {
                    arrow = callArrows.head;
                    Unit arrowDest = arrow.destination;
                    if (arrow.isCall() &&
                            (arrowDest.isStandard() || arrowDest.isExternal())) {
                        ++nbEdgesToDump;
                    }
                    callArrows = callArrows.tail;
                }
                inUnitsToDump = inUnitsToDump.tail;
            }
            graphPanel.nodes = new Node[callGraph.nbUnits()];
            graphPanel.edges = new Edge[nbEdgesToDump];
            // Place units into the graph:
            inUnitsToDump = unitsToDump;
            int unitNb = 1;
            while (inUnitsToDump != null) {
                unit = inUnitsToDump.head;
                if (unit.isStandard() || unit.isExternal()) {
                    nodeIndexes[unit.rank()] =
                            graphPanel.addNode(new UnitNode(unit), 0.5,
                                    (double) unitNb / (double) nbUnitsToDump);
                    ++unitNb;
                }
                inUnitsToDump = inUnitsToDump.tail;
            }
            // Place call arrows into the graph:
            inUnitsToDump = unitsToDump;
            while (inUnitsToDump != null) {
                unit = inUnitsToDump.head;
                int nbArrows = 0;
                TapList<CallArrow> callArrows = unit.callees();
                while (callArrows != null) {
                    arrow = callArrows.head;
                    if (arrow.isCall() && nodeIndexes[arrow.destination().rank()] != -1) {
                        ++nbArrows;
                    }
                    callArrows = callArrows.tail;
                }
                unitNb = 1;
                callArrows = unit.callees();
                while (callArrows != null) {
                    arrow = callArrows.head;
                    if (arrow.isCall() && nodeIndexes[arrow.destination().rank()] != -1) {
                        graphPanel.addEdge(
                                nodeIndexes[arrow.origin().rank()],
                                nodeIndexes[arrow.destination().rank()],
                                new Edge(), 50 * (2 * unitNb - nbArrows - 1), 50);
                    }
                    callArrows = callArrows.tail;
                    unitNb++;
                }
                inUnitsToDump = inUnitsToDump.tail;
            }
            graphPanel.start();
        } else if (tapObject instanceof Unit) {
            Unit unit = (Unit) tapObject;
            aboveObject = unit.callGraph();
            displayedObject = unit;
            nodeIndexes = new int[unit.nbBlocks + 2];
            TapList<Block> allBlocks = unit.allBlocks();
            Block block;
            TapList<FGArrow> arrows;
            FGArrow arrow;
            Node newNode;
            TapList<SymbolTable> displayedSymbolTables;
            graphPanel.nodes = new Node[100];
            graphPanel.edges = new Edge[200];
            if (unit.entryBlock() != null) {
                displayedSymbolTables =
                        new TapList<>(unit.entryBlock().symbolTable, null);
                boolean withDecls;
                try {
                    ByteArrayOutputStream
                            byteStream = new ByteArrayOutputStream();
                    TapEnv.pushOutputStream(byteStream);
                    label.setText("Unit : " + unit.name());
                    nodeIndexes[0] =
                            graphPanel.addNode(
                                    new EntryBlockNode(unit.entryBlock(), byteStream, true),
                                    0.5, 1.0 / (double) (unit.nbBlocks + 3));
                    int i;
                    while (allBlocks != null) {
                        block = allBlocks.head;
                        if (TapList.contains(displayedSymbolTables, block.symbolTable)) {
                            withDecls = false;
                        } else {
                            withDecls = true;
                            displayedSymbolTables =
                                    new TapList<>(block.symbolTable, displayedSymbolTables);
                        }
                        if (block instanceof HeaderBlock) {
                            newNode = new HeaderBlockNode((HeaderBlock) block, byteStream, withDecls);
                        } else {
                            newNode = new BlockNode(block, byteStream, withDecls);
                        }
                        nodeIndexes[block.rank + 1] =
                                graphPanel.addNode(
                                        newNode, 0.5,
                                        (double) (block.rank + 2)
                                                / (double) (unit.nbBlocks + 3));
                        allBlocks = allBlocks.tail;
                    }
                    nodeIndexes[unit.nbBlocks + 1] =
                            graphPanel.addNode(
                                    new ExitBlockNode(unit.exitBlock()), 0.5,
                                    (double) (unit.nbBlocks + 2)
                                            / (double) (unit.nbBlocks + 3));
                    allBlocks =
                            new TapList<>(unit.entryBlock(), unit.allBlocks());
                    int nbArrows;
                    boolean loopsInLoop;
                    while (allBlocks != null) {
                        block = allBlocks.head;
                        arrows = block.flow();
                        nbArrows = TapList.length(arrows);
                        i = 1;
                        while (arrows != null) {
                            arrow = arrows.head;
                            loopsInLoop =
                                    arrow.destination instanceof HeaderBlock
                                            && isInLoop(
                                            block, (HeaderBlock) arrow.destination);
                            graphPanel.addEdge(
                                    nodeIndexes[block.rank + 1],
                                    nodeIndexes[arrow.destination.rank + 1],
                                    new Edge(),
                                    loopsInLoop ? 0 : 50 * (2 * i - nbArrows - 1),
                                    loopsInLoop ? 0 : 60);
                            arrows = arrows.tail;
                            i++;
                        }
                        allBlocks = allBlocks.tail;
                    }
                    TapEnv.popOutputStream();
                    byteStream.close();
                    graphPanel.start();
                } catch (java.io.IOException e) {
                    System.out.println("byteStream broken "+e);
                }
            }
        } else if (tapObject instanceof NewBlockGraph) {
            NewBlockGraph graph = (NewBlockGraph) tapObject;
            TapList<NewBlockGraphNode> nodes = graph.nodes;
            int nbNodes = TapList.length(nodes);
            aboveObject = graph;
            displayedObject = graph;
            nodeIndexes = new int[nbNodes];
            NewBlockGraphNode node;
            NewBlockGraphArrow arrow;
            TapList<NewBlockGraphArrow> arrows;
            graphPanel.nodes = new Node[100];
            graphPanel.edges = new Edge[200];
            label.setText("Adjoint graph");
            for (int i = 0; i < nbNodes; i++) {
                node = nodes.head;
                nodes = nodes.tail;
                node.rank = i;
                nodeIndexes[i] =
                        graphPanel.addNode(
                                new DGNodeNode(node), 0.5,
                                (double) (i + 1)
                                        / (double) (nbNodes + 1));
            }
            nodes = graph.nodes;
            for (int i = 0; i < nbNodes; i++) {
                node = nodes.head;
                nodes = nodes.tail;
                arrows = node.flow;
                while (arrows != null) {
                    arrow = arrows.head;
                    graphPanel.addEdge(
                            nodeIndexes[arrow.origin().rank],
                            nodeIndexes[arrow.destination().rank],
                            new Edge(), 0, 50);
                    arrows = arrows.tail;
                }
            }
            graphPanel.start();
        } else {
            aboveObject = displayedObject;
            displayedObject = null;
            nodeIndexes = null;
        }
    }

    private Object selectedObject() {
        if (nodeIndexes != null && graphPanel.selectedIndex() >= 0) {
            int tapRank = -1;
            int graphRank = graphPanel.selectedIndex();
            for (int i = 0; i < nodeIndexes.length && tapRank == -1; i++) {
                if (nodeIndexes[i] == graphRank) {
                    tapRank = i;
                }
            }
            if (tapRank >= 0) {
                if (displayedObject instanceof CallGraph) {
                    return ((CallGraph) displayedObject).sortedUnit(tapRank);
                } else if (displayedObject instanceof Unit) {
                    Unit unit = (Unit) displayedObject;
                    if (tapRank <= 0) {
                        return unit.entryBlock();
                    } else if (tapRank >= unit.nbBlocks + 1) {
                        return unit.exitBlock();
                    } else {
                        return TapList.nth(unit.allBlocks(), tapRank - 1);
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private boolean isInLoop(Block block, HeaderBlock header) {
        Block loop1 = header.enclosingLoop();
        Block loop2 = block.enclosingLoop();
        boolean result = false;
        while (!result && loop2 != null) {
            if (loop2 == loop1) {
                result = true;
            }
            loop2 = loop2.enclosingLoop();
        }
        return result;
    }
}
