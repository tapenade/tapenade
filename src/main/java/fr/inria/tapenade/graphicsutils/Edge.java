/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class Edge {
    protected Node from;
    protected Node to;
    protected int[] x3 = new int[3];
    protected int[] y3 = new int[3];
    protected int idealX;
    protected int idealY;

    protected Edge() {
    }

    protected void draw(Graphics graphics) {
        double x1 = from.x;
        double y1 = from.y + from.h2;
        double x2 = to.x;
        double y2 = to.y - to.h2;
        graphics.setColor(Color.black);
        graphics.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
        double len = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        double dx = (x1 - x2) / len;
        double dy = (y1 - y2) / len;
        x3[0] = (int) ((x1 + x2) / 2);
        y3[0] = (int) ((y1 + y2) / 2);
        x3[1] = (int) (x3[0] + 12 * dx - 4 * dy);
        y3[1] = (int) (y3[0] + 12 * dy + 4 * dx);
        x3[2] = (int) (x3[0] + 12 * dx + 4 * dy);
        y3[2] = (int) (y3[0] + 12 * dy - 4 * dx);
        graphics.fillPolygon(x3, y3, 3);
    }
}
