/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
final class GraphPanel extends Panel
        implements Runnable, MouseListener, MouseMotionListener {

    private static final long serialVersionUID = 1234056794876L;
    private int nnodes;
    protected Node[] nodes;
    private int nedges;
    protected Edge[] edges;
    private Thread relaxer;
    private Node selected;
    private Node pick;
    private boolean pickfixed;
    private Image offscreen;
    private Dimension offscreensize;
    private Graphics offgraphics;

    protected GraphPanel() {
        addMouseListener(this);
    }

    protected int selectedIndex() {
        if (selected != null) {
            for (int i = 0; i < nnodes; i++) {
                if (nodes[i] == selected) {
                    return i;
                }
            }
        }
        return -1;
    }

    protected void reset() {
        nodes = null;
        edges = null;
        nnodes = 0;
        nedges = 0;
        repaint();
        selected = null;
        relaxer = null;
    }

    protected int addNode(Node n, double rx, double ry) {
        Dimension d = getSize();
        n.x = rx * d.width + 20 * (Math.random() - 0.5);
        n.y = ry * d.height + 20 * (Math.random() - 0.5);
        nodes[nnodes] = n;
        return nnodes++;
    }

    protected void addEdge(int fromIndex, int toIndex, Edge e, int idealX, int idealY) {
        e.from = nodes[fromIndex];
        e.to = nodes[toIndex];
        e.idealX = idealX;
        e.idealY = idealY;
        edges[nedges++] = e;
    }

    protected void start() {
        relaxer = new Thread(this);
        relaxer.start();
    }

    @Override
    public void run() {
        Thread me = Thread.currentThread();
        repaint();
        while (relaxer == me) {
            relax();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                System.out.println("GraphPanel InterruptedException");
                Thread.currentThread().interrupt();
            }
        }
    }

    private synchronized void relax() {
        Dimension d = getSize();
        int width = d.width;
        int height = d.height;
        Edge edge;
        Node node1, node2;
        double deltaX, deltaY;
        int i, j;
        for (i = 0; i < nedges; ++i) {
            edge = edges[i];
            if (edge.idealX != 0 || edge.idealY != 0) {
                deltaX = edge.to.x - edge.from.x - edge.idealX;
                if (deltaX > 1.0 || deltaX < -1.0) {
                    deltaX = deltaX / 10.0;
                    edge.from.speedX += deltaX / 5;
                    edge.to.speedX -= deltaX / 5;
                }
                deltaY =
                        edge.to.y - edge.to.h2 - edge.from.y - edge.from.h2
                                - edge.idealY;
                if (deltaY > 1.0 || deltaY < -1.0) {
                    deltaY = deltaY / 10.0;
                    edge.from.speedY += deltaY / 5;
                    edge.to.speedY -= deltaY / 5;
                }
            }
        }
        for (i = 0; i < nnodes; i++) {
            node1 = nodes[i];
            for (j = i + 1; j < nnodes; j++) {
                node2 = nodes[j];
                if (node1.y - 3 < node2.y + 3
                        && node2.y - 3 < node1.y + 3) {
                    deltaX = node2.x - node1.x;
                    if (deltaX > 0) {
                        deltaX = 30 - deltaX;
                        if (deltaX > 30) {
                            deltaX = 30;
                        }
                        if (deltaX < 0) {
                            deltaX = 0;
                        }
                        deltaX *= deltaX;
                    } else {
                        deltaX = -30 - deltaX;
                        if (deltaX < -30) {
                            deltaX = -30;
                        }
                        if (deltaX > 0) {
                            deltaX = 0;
                        }
                        deltaX *= -deltaX;
                    }
                    node1.speedX -= deltaX / 20;
                    node2.speedX += deltaX / 20;
                }
                if (node1.x - 3 < node2.x + 3
                        && node2.x - 3 < node1.x + 3) {
                    deltaY = node2.y - node1.y;
                    if (deltaY > 0) {
                        deltaY = 30 - deltaY;
                        if (deltaY > 30) {
                            deltaY = 30;
                        }
                        if (deltaY < 0) {
                            deltaY = 0;
                        }
                        deltaY *= deltaY;
                    } else {
                        deltaY = -30 - deltaY;
                        if (deltaY < -30) {
                            deltaY = -30;
                        }
                        if (deltaY > 0) {
                            deltaY = 0;
                        }
                        deltaY *= -deltaY;
                    }
                    node1.speedY -= deltaY / 20;
                    node2.speedY += deltaY / 20;
                }
            }
            if (node1.x - node1.w2 - 15 < 0) {
                deltaX = node1.w2 + 15 - node1.x;
                deltaX *= deltaX;
            } else if (node1.x + node1.w2 + 15 > width) {
                deltaX = width - node1.x - node1.w2 - 15;
                deltaX *= -deltaX;
            } else {
                deltaX = 0;
            }
            node1.speedX += deltaX / 10;
            if (node1.y - node1.h2 - 15 < 0) {
                deltaY = node1.h2 + 15 - node1.y;
                deltaY *= deltaY;
            } else if (node1.y + node1.h2 + 15 > height) {
                deltaY = height - node1.y - node1.h2 - 15;
                deltaY *= -deltaY;
            } else {
                deltaY = 0;
            }
            node1.speedY += deltaY / 10;
            if (node1 instanceof UnitNode) {
                UnitNode unitNode1 = (UnitNode) node1;
                if (unitNode1.isTopNode()) {
                    unitNode1.speedY -= 5;
                } else if (unitNode1.isBottomNode()) {
                    unitNode1.speedY += 5;
                }
            }
        }
        boolean needRepaint = false;
        for (i = 0; i < nnodes; i++) {
            node1 = nodes[i];
            if (node1.speedX * node1.speedX + node1.speedY * node1.speedY < 0.01) {
                node1.speedX = 0.0;
                node1.speedY = 0.0;
            } else {
                needRepaint = true;
                node1.x += Math.max(-5, Math.min(5, node1.speedX));
                node1.y += Math.max(-5, Math.min(5, node1.speedY));
                node1.speedX /= 3;
                node1.speedY /= 3;
            }
        }
        if (needRepaint) {
            repaint();
        }
    }

    @Override
    public synchronized void update(Graphics g) {
        Dimension d = getSize();
        if
        (offscreen == null || d.width != offscreensize.width
                || d.height != offscreensize.height) {
            offscreen = createImage(d.width, d.height);
            offscreensize = d;
            if (offgraphics != null) {
                offgraphics.dispose();
            }
            offgraphics = offscreen.getGraphics();
            offgraphics.setFont(getFont());
        }
        offgraphics.setColor(Color.white);
        offgraphics.fillRect(0, 0, d.width, d.height);
        for (int i = 0; i < nedges; i++) {
            edges[i].draw(offgraphics);
        }
        for (int i = 0; i < nnodes; i++) {
            nodes[i].draw(offgraphics);
        }
        g.drawImage(offscreen, 0, 0, null);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        addMouseMotionListener(this);
        double bestdist = Double.MAX_VALUE;
        int x = e.getX();
        int y = e.getY();
        for (int i = 0; i < nnodes; i++) {
            Node n = nodes[i];
            double dist = (n.x - x) * (n.x - x) + (n.y - y) * (n.y - y);
            if (dist < bestdist) {
                pick = n;
                bestdist = dist;
            }
        }
        if (pick != null) {
            if (selected != null) {
                selected.setSelected(false);
            }
            selected = pick;
            selected.setSelected(true);
            pickfixed = pick.fixed;
            pick.fixed = true;
            pick.x = x;
            pick.y = y;
        }
        repaint();
        e.consume();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        removeMouseMotionListener(this);
        if (pick != null) {
            pick.x = e.getX();
            pick.y = e.getY();
            pick.fixed = pickfixed;
            pick = null;
        }
        repaint();
        e.consume();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (pick != null) {
            pick.x = e.getX();
            pick.y = e.getY();
        }
        repaint();
        e.consume();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}
