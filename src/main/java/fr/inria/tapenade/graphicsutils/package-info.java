/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 *
 * @author Inria - Ecuador team, tapenade@inria.fr
 */
package fr.inria.tapenade.graphicsutils;
