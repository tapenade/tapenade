/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.EntryBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.LoopBlock;
import fr.inria.tapenade.representation.MemoryMaps;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.io.ByteArrayOutputStream;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class BlockNode extends Node {

    private String[] lines;
    protected Color bg = new Color(255, 230, 170);
    protected Color fg = Color.black;
    protected boolean doubleBorder;
    private String[] decls;
    private static final Color declFg = Color.blue;
    protected boolean declsAfter;

    protected BlockNode(Block block,
                        ByteArrayOutputStream byteStream, boolean withDecls) {
        super();
        TapList<String> listTexts = null;
        String text;
        TapList<Instruction> instructions = block.instructions;
        while (instructions != null) {
            listTexts = new TapList<>(ILUtils.toString(instructions.head.tree),
                    listTexts);
            instructions = instructions.tail;
        }
        lines = new String[TapList.length(listTexts)];
        for (int i = lines.length - 1; i >= 0; i--) {
            lines[i] = listTexts.head;
            listTexts = listTexts.tail;
        }
        // When required, compute the lines to display declarations:
        if (withDecls) {
            TapList<SymbolDecl> symbolDecls = block.symbolTable.getAllTopSymbolDecls();
            listTexts = null;
            while (symbolDecls != null) {
                try {
                    byteStream.reset();
                    TapEnv.print("  ");
                    symbolDecls.head.dump(0);
                    text = byteStream.toString();
                } catch (java.io.IOException e) {
                    System.out.println("OutputStream for node labels broken "+e);
                    text = "SymbolDecl";
                }
                listTexts = new TapList<>(text, listTexts);
                symbolDecls = symbolDecls.tail;
            }
            if (block instanceof EntryBlock) {
                TapList<MemMap> inMaps = block.unit().localMaps.maps.tail;
                while (inMaps != null) {
                    try {
                        byteStream.reset();
                        TapEnv.print("  "+inMaps.head);
                        // was MemoryMap.dumpMap((TapList) inMaps.head);
                        text = byteStream.toString();
                    } catch (java.io.IOException e) {
                        System.out.println("OutputStream for node labels broken "+e);
                        text = "map";
                    }
                    listTexts = new TapList<>(text, listTexts);
                    inMaps = inMaps.tail;
                }
            }
            decls = new String[TapList.length(listTexts)];
            for (int i = decls.length - 1; i >= 0; i--) {
                decls[i] = listTexts.head;
                listTexts = listTexts.tail;
            }
            /*Compute the BG color, getting red with increasing loop nesting level: */
        }
        int level = 0;
        LoopBlock enclosing = block.enclosingLoop();
        while (enclosing != null) {
            level++;
            enclosing = enclosing.enclosingLoop();
        }
        if (level > 0) {
            bg = new Color(255, 230 - level * 30, 170 - level * 30);
        }
    }

    @Override
    protected void draw(Graphics graphics) {
        FontMetrics fm = graphics.getFontMetrics();
        int dh = fm.getHeight() + 1;
        int w = 0;
        int h = 1;
        int nw;
        int i;
        double epsilon = 0.00000001;
        if (Math.abs(w2 + 1.0) < epsilon || Math.abs(h2 + 1.0) < epsilon) {
            /*On the 1st time this node is drawn, compute its width and height: */
            if (lines != null) {
                h += dh * lines.length;
                for (i = lines.length - 1; i >= 0; i--) {
                    nw = fm.stringWidth(lines[i]);
                    if (nw > w) {
                        w = nw;
                    }
                }
            }
            if (decls != null) {
                h += dh * decls.length;
                for (i = decls.length - 1; i >= 0; i--) {
                    nw = fm.stringWidth(decls[i]);
                    if (nw > w) {
                        w = nw;
                    }
                }
            }
            w += 10;
            w2 = w / (double) 2f;
            h2 = h / (double) 2f;
        }
        graphics.setColor(bg);
        if (doubleBorder) {
            graphics.fillRect(
                    (int) (x - w2 - 3), (int) (y - h2 - 3), (int) (2 * w2 + 6),
                    (int) (2 * h2 + 6));
            graphics.setColor(fg);
            graphics.drawRect(
                    (int) (x - w2 - 3), (int) (y - h2 - 3), (int) (2 * w2 + 6),
                    (int) (2 * h2 + 6));
        } else {
            graphics.fillRect(
                    (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
            graphics.setColor(fg);
        }
        graphics.drawRect(
                (int) (x - w2), (int) (y - h2), (int) (2 * w2), (int) (2 * h2));
        int tx = (int) (x - w2 + 5);
        int line1y = (int) (y + 0.5 - h2) + fm.getAscent();
        int thisLine1y = line1y;
        if (decls != null && !declsAfter) {
            thisLine1y += decls.length * dh;
        }
        assert lines != null;
        for (i = lines.length - 1; i >= 0; i--) {
            graphics.drawString(lines[i], tx, thisLine1y + i * dh);
        }
        if (decls != null) {
            graphics.setColor(fg);
            thisLine1y =
                    (int) (y - h2) + (declsAfter ? lines.length : decls.length) * dh;
            graphics.drawLine(
                    (int) (x - w2), thisLine1y, (int) (x + w2), thisLine1y);
            graphics.setColor(declFg);
            thisLine1y = line1y;
            if (declsAfter) {
                thisLine1y += lines.length * dh;
            }
            for (i = decls.length - 1; i >= 0; i--) {
                graphics.drawString(decls[i], tx, thisLine1y + i * dh);
            }
        }
    }
}
