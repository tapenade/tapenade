/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class UnitNode extends Node {
    private static Color fg = new Color(0, 0, 0);
    private String text;
    private Color bg;
    private int topOrBottom;

    protected UnitNode(Unit unit) {
        super();
        String nodeName = unit.name();
        Unit containerUnit = unit.upperLevelUnit();
        while (containerUnit != null) {
            nodeName = containerUnit.name() + ":" + nodeName;
            containerUnit = containerUnit.upperLevelUnit();
        }
        text = nodeName;
        if (!callsOneCall(unit)) {
            topOrBottom = -1;
            bg = new Color(255, 255, 150);
        } else if (!calledByCall(unit)) {
            topOrBottom = 1;
            bg = new Color(255, 180, 180);
        } else {
            topOrBottom = 0;
            bg = new Color(150, 255, 250);
        }
    }

    /**
     * Utility for graph display: returns true when this Unit
     * calls another Unit (really a CALL).
     */
    private static boolean callsOneCall(Unit unit) {
        boolean calls = false;
        TapList<CallArrow> arrows = unit.callees();
        while (!calls && arrows != null) {
            calls = arrows.head.isCall();
            arrows = arrows.tail;
        }
        return calls;
    }

    /**
     * Utility for graph display: returns true when this Unit is called
     * (really a CALL) by another one.
     */
    private static boolean calledByCall(Unit unit) {
        boolean called = false;
        TapList<CallArrow> arrows = unit.callers();
        while (!called && arrows != null) {
            called = arrows.head.isCall();
            arrows = arrows.tail;
        }
        return called;
    }

    @Override
    protected void draw(Graphics graphics) {
        FontMetrics fm = graphics.getFontMetrics();
        graphics.setColor(bg);
        int w = fm.stringWidth(text) + 10;
        int h = fm.getHeight() + 2;
        w2 = w / (double) 2f;
        h2 = h / (double) 2f;
        graphics.fillRect(
                (int) (x - w / (double) 2f), (int) (y - h / (double) 2f), w, h);
        graphics.setColor(fg);
        graphics.drawRect(
                (int) (x - w / (double) 2f), (int) (y - h / (double) 2f), w, h);
        graphics.drawString(
                text, (int) (x - (w - 10) / (double) 2f),
                (int) (y - (h - 1) / (double) 2f) + fm.getAscent());
    }

    protected boolean isTopNode() {
        return topOrBottom == 1;
    }

    protected boolean isBottomNode() {
        return topOrBottom == -1;
    }
}
