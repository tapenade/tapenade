/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.graphicsutils;

import fr.inria.tapenade.representation.EntryBlock;

import java.awt.Color;
import java.io.ByteArrayOutputStream;

/**
 * Static class for graphic display of Tapenade objects
 * with -viewcallgraph option.
 */
class EntryBlockNode extends BlockNode {
    protected EntryBlockNode(EntryBlock entryBlock,
                             ByteArrayOutputStream byteStream, boolean withDecls) {
        super(entryBlock, byteStream, withDecls);
        fg = Color.red;
        declsAfter = true;
    }
}
