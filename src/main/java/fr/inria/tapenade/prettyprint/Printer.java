/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Abstract class for Text or HTML printer.
 */
public abstract class Printer {
    private final OutputStreamWriter toFile;

    protected Printer(String toFileName) throws IOException {
        super();
        File file = new File(toFileName);
        if (file.exists()) {
            if (!file.renameTo(new File(toFileName + "~"))) {
                TapEnv.toolWarning(-1, "Cannot rename " + toFileName + "~");
            }
        }
        this.toFile = new FileWriter(toFileName);
    }

    protected Printer(OutputStream p) {
        super();
        this.toFile = new OutputStreamWriter(p);
    }

    public void initFile(int language) {
    }

    /**
     * Flush and close current OutputStreamWriter.
     */
    public void closeFile() {
        try {
            toFile.flush();
            toFile.close();
        } catch (IOException e) {
            TapEnv.systemError("(Decompiler) I-O error " + e.getMessage() + " while closing output file " + toFile);
        }
    }

    public abstract void space(int nbSpaces) throws IOException;

    public abstract void newLine() throws IOException;

    /**
     * @param kind determines the graphical syntax highlighting (e.g. for HTML).
     *             Possible values are {"plain", "string", "constant", "comment", "keyword",
     *             "funcname", "typename", "label", "modifier", "vardecl"}
     */
    public abstract void printText(String text, String kind)
            throws IOException;

    /**
     * @return true if the target language contains html-like anchors.
     */
    public boolean hasAnchors() {
        return false;
    }

    public void setTargetNameAndZone(String name, String zone) {
    }

    public void resetTargetNameAndZone() {
    }

    public void defineAnchor(String ref) {
    }

    public void startAnchorRef(String ref) {
    }

    public void endAnchorRef() {
    }

    /**
     * Print a Tapenade message in the generated file.
     *
     * @param msgTty   Tapenade error or warning message.
     * @param showMsg  true with tapenade option -msginfile.
     * @param language used add comment delimiter before the message.
     * @throws IOException if an output error is detected.
     */
    public void messageRef(TapList<String> msgTty, boolean showMsg, int language)
            throws IOException {
    }

    protected OutputStreamWriter toFile() {
        return toFile;
    }
}
