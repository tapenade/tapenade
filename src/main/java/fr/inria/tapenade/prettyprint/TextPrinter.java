/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Prints text output file.
 */
public final class TextPrinter extends Printer {
    public TextPrinter(String toFileName) throws IOException {
        super(toFileName);
    }

    public TextPrinter(OutputStream s) {
        super(s);
    }

    @Override
    public void space(int nbSpaces) throws IOException {
        for (int i = nbSpaces; i > 0; --i) {
            toFile().write(" ");
        }
    }

    @Override
    public void newLine() throws IOException {
        toFile().write(System.lineSeparator());
    }

    @Override
    public void printText(String text, String kind) throws IOException {
        toFile().write(text);
    }

    @Override
    public void messageRef(TapList<String> msgTty, boolean showMsg, int language)
            throws IOException {
        if (showMsg) {
            while (msgTty != null) {
                toFile().write(TapEnv.commentStart(language) + " TAPENADE: " + msgTty.head);
                if (msgTty.tail != null) {
                    newLine();
                }
                msgTty = msgTty.tail;
            }
        }
    }
}
