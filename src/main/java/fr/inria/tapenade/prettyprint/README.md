Tapenade pretty-printers for Fortran and C output files 
=======================================================

### <span style="color:green">**<Step 6>**</span>

* Files: [Javadoc](../../../../../../../build/fr/inria/tapenade/prettyprint/package-index.html)

`Decompiler DotDecompiler FortranDecompiler CDecompiler`  
`Printer TextPrinter HtmlPrinter`  
`CallGraphPrinter CallGraphDisplayer`

The "prettyprint" package contains classes that
translate the differentiated abstract syntax trees into 
Fortran and C output files.

FortranDecompiler and CDecompiler classes extend the Decompiler class.

TextPrint and HtmlPrinter extend the Printer class.

`new XXXDecompiler().decompileTree()`

Missing: Management of indentation and line cutting

Missing: details on generating text vs. generating HTML
