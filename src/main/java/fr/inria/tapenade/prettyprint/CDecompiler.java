/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;

/**
 * Decompiler for ANSI C. Original author Nicolas Chleq.
 * <p>
 * KEY PRINCIPLE ABOUT WHITE SPACES AND NEWLINES:<br>
 * Decompilation of any sub-tree takes care ONLY of printing from the first
 * non-whitespace-non-newline character to the last non-whitespace-non-newline character of this sub-tree.
 * Therefore, writing the whitespaces and newlines immediately surrounding decompilation of the sub-tree
 * is the task of the parent tree of sub-tree (or parent of parent, etc).
 * Exceptions may exist, but they must remain rare and motivated.
 */
public final class CDecompiler extends Decompiler {
    private static final int INDENT_STEP = 4;
    private static final int PREC_UNARY = 35;
    private static final int PREC_BINARY = 33;
    private static final int LPREC_BINARY = PREC_BINARY;
    private static final int RPREC_BINARY = PREC_BINARY + 1;
    private static final int PREC_FIELD = 31;
    private static final int LPREC_FIELD = PREC_FIELD;
    private static final int RPREC_FIELD = PREC_FIELD + 1;
    private static final int PREC_SUBSCRIPT = 31;
    private static final int LPREC_SUBSCRIPT = PREC_SUBSCRIPT;
    private static final int PREC_FCALL = 31;
    private static final int LPREC_FCALL = PREC_FCALL;
    private static final int RPREC_FCALL = PREC_FCALL + 1;
    private static final int PREC_POSTDECR = 31;
    private static final int PREC_POSTINCR = 31;
    private static final int PREC_SIZEOF = 29;
    private static final int PREC_PREDECR = 29;
    private static final int PREC_PREINCR = 29;
    private static final int PREC_BNOT = 29;
    private static final int PREC_NOT = 29;
    private static final int PREC_MINUS = 29;
    private static final int PREC_PLUS = 29;
    private static final int PREC_ADDROF = 29;
    private static final int PREC_PTRDEREF = 30;
    private static final int PREC_CAST = 29;
    private static final int PREC_MUL = 25;
    private static final int LPREC_MUL = PREC_MUL;
    private static final int RPREC_MUL = PREC_MUL + 1;
    private static final int PREC_DIV = 25;
    private static final int LPREC_DIV = PREC_DIV;
    private static final int RPREC_DIV = PREC_DIV + 1;
    private static final int PREC_MOD = 25;
    private static final int LPREC_MOD = PREC_MOD;
    private static final int RPREC_MOD = PREC_MOD + 1;
    private static final int PREC_ADD = 23;
    private static final int LPREC_ADD = PREC_ADD;
    private static final int RPREC_ADD = PREC_ADD + 1;
    private static final int PREC_SUB = 23;
    private static final int LPREC_SUB = PREC_SUB;
    private static final int RPREC_SUB = PREC_SUB + 1;
    private static final int PREC_LSHIFT = 21;
    private static final int LPREC_LSHIFT = PREC_LSHIFT;
    private static final int RPREC_LSHIFT = PREC_LSHIFT + 1;
    private static final int PREC_RSHIFT = 21;
    private static final int LPREC_RSHIFT = PREC_RSHIFT;
    private static final int RPREC_RSHIFT = PREC_RSHIFT + 1;
    private static final int PREC_GE = 19;
    private static final int LPREC_GE = PREC_GE;
    private static final int RPREC_GE = PREC_GE + 1;
    private static final int PREC_GT = 19;
    private static final int LPREC_GT = PREC_GT;
    private static final int RPREC_GT = PREC_GT + 1;
    private static final int PREC_LE = 19;
    private static final int LPREC_LE = PREC_LE;
    private static final int RPREC_LE = PREC_LE + 1;
    private static final int PREC_LT = 19;
    private static final int LPREC_LT = PREC_LT;
    private static final int RPREC_LT = PREC_LT + 1;
    private static final int PREC_EQ = 17;
    private static final int LPREC_EQ = PREC_EQ;
    private static final int RPREC_EQ = PREC_EQ;
    private static final int PREC_NEQ = 17;
    private static final int LPREC_NEQ = PREC_NEQ;
    private static final int RPREC_NEQ = PREC_NEQ;
    /* bitwise operators */
    private static final int PREC_BAND = 15;
    private static final int LPREC_BAND = PREC_BAND;
    private static final int RPREC_BAND = PREC_BAND + 1;
    private static final int PREC_BXOR = 13;
    private static final int LPREC_BXOR = PREC_BXOR;
    private static final int RPREC_BXOR = PREC_BXOR + 1;
    private static final int PREC_BOR = 11;
    private static final int LPREC_BOR = PREC_BOR;
    private static final int RPREC_BOR = PREC_BOR + 1;
    /* logical operators */
    private static final int PREC_AND = 9;
    private static final int LPREC_AND = PREC_AND;
    private static final int RPREC_AND = PREC_AND + 1;
    private static final int PREC_OR = 7;
    private static final int LPREC_OR = PREC_OR;
    private static final int RPREC_OR = PREC_OR + 1;
    /* Conditional expression */
    private static final int PREC_CONDEXP = 5;
    /* All assign operators */
    private static final int PREC_ASSIGN = 3;
    private static final int LPREC_ASSIGN = PREC_ASSIGN;
    private static final int RPREC_ASSIGN = PREC_ASSIGN + 1;
    // Constants for used contexts (from 0 to 31)
    private static final int INPARAMSLIS = 5;
    private static final int INBRACES_CTX = 6;
    private static final int RESULTEXPECTED_CTX = 7;
    private static final int DEEPEXPR_CTX = 8;
    private static final int ISVARDECL_CTX = 9;
    private static final int ISFUNCNAME_CTX = 10;
    private static final int INUNITBODY_CTX = 15; // implies no blank line between statements.

    private static final int INBODY_CONTEXT = 1 << INUNITBODY_CTX;

    private static final int TYPE_TREE = 0;
    private static final int DECL_TREE = 1;

    private boolean endOfUnit;

    /** The size parameters passed to an op_parallelSpread, e.g. a Cuda call. */
    private Tree parallelSpreadSizes = null ;

    public CDecompiler(Printer printer, String htmlMsgFileName) {
        super(printer, htmlMsgFileName);
    }

    /**
     * @return true when "tree", after peeling the useless singleton blockStatement levels,
     * either contains declarations or contains more than 1 instruction (depends on "orIsLong").
     */
    private static boolean hasDeclsOrIsLongOrIsEmpty(Tree tree, boolean orIsLong) {
        Tree[] contents = new Tree[1];
        contents[0] = tree;
        while (contents.length == 1
                && contents[0].opCode() == ILLang.op_blockStatement) {
            contents = contents[0].children();
        }
        if (orIsLong && contents.length != 1) {
            return true;
        }
        int i = 0;
        boolean hasDecls = false;
        while (!hasDecls && i < contents.length) {
            hasDecls = contents[i] != null && ILUtils.isADeclaration(contents[i]);
            i++;
        }
        return hasDecls;
    }

    /**
     * Assumes tree is a clean nest of blockStatements containing
     * a single instruction.
     *
     * @return this instruction.
     */
    private static Tree singleInstructionIn(Tree tree) {
        Tree[] contents = new Tree[1];
        contents[0] = tree;
        while (contents.length == 1
                && contents[0].opCode() == ILLang.op_blockStatement) {
            contents = contents[0].children();
        }
        if (contents.length == 0) {
            return null;
        } else {
            return contents[0];
        }
    }

    private static boolean hasSingleIfNoElse(Tree tree) {
        Tree[] contents = new Tree[1];
        contents[0] = tree;
        while (contents.length == 1
                && contents[0].opCode() == ILLang.op_blockStatement) {
            contents = contents[0].children();
        }
        if (contents.length == 1 && contents[0].opCode() == ILLang.op_if) {
            Tree elsePart = contents[0].down(3);
            return elsePart == null || elsePart.opCode() == ILLang.op_none
                    || !elsePart.isAtom() && elsePart.length() == 0;
        } else {
            return false;
        }
    }

    @Override
    public void decompileTree(Tree tree, int lang, boolean inInclude, boolean showMsgs) {
        if (tree != null) {
            try {
                showMessages = showMsgs;
                language = lang;
                maxColumn = 78;
                commentChar = "//";
                reinitialize();
                decompile(tree, 0, 0, contextPlus(inInclude ? INBODY_CONTEXT : EMPTY_CONTEXT, INBRACES_CTX));
            } catch (IOException e) {
                TapEnv.systemError("(C decompiler) I-O error " + e.getMessage() + " while writing output file " + printer.toFile());
            }
        } else {
            TapEnv.toolWarning(-1, "(C decompiler) empty tree");
        }
    }

    private void decompile(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        boolean braces;
        switch (tree.opCode()) {
            case ILLang.op_file: // op_file obsolescent...
            case ILLang.op_blockStatement: {
                braces =
                        !hasContext(context, INBRACES_CTX) && hasDeclsOrIsLongOrIsEmpty(tree, false);
                int indent2 = indent;
                if (braces) {
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                    newLine(indent);
                    indent2 = indent + INDENT_STEP / 2;
                }
                Tree[] elements = tree.children();
                if (elements.length > 1) {
                    context = contextMinus(context, INBRACES_CTX);
                }
                for (int i = 0; i < elements.length; ++i) {
                    decompile(elements[i], indent2, precedence, context);
                    if (i + 1 < elements.length) {
                        if (!(elements[i + 1].opCode() == ILLang.op_function && isAFunctionPrototype(elements[i + 1]))
                            //op_continue, op_none, print nothing, therefore there is no need for a newline:
                            && elements[i+1].opCode()!=ILLang.op_continue
                            && elements[i+1].opCode()!=ILLang.op_none) {
                            newLine(indent2);
                        }
                        if (elements[i].opCode() == ILLang.op_function
                                || elements[i].opCode() == ILLang.op_program
                                || elements[i].opCode() == ILLang.op_class
                                || elements[i].opCode() == ILLang.op_nameSpace
                                || elements[i + 1].opCode() == ILLang.op_function
                                || elements[i + 1].opCode() == ILLang.op_program
                                || elements[i + 1].opCode() == ILLang.op_class
                                || elements[i + 1].opCode() == ILLang.op_nameSpace) {
                            newLine(indent2);
                        }
                    } else {
                        endOfUnit = true;
                    }
                }
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                break;
            }
            case ILLang.op_nameSpace:
                print("namespace", indent, "plain");
                space();
                printThingName(tree.down(1), indent, "plain");
                space();
                print("{", indent, "plain");
                newLine(indent);
                decompile(tree.down(2), indent + INDENT_STEP, precedence, contextPlus(context, INBRACES_CTX));
                newLine(indent);
                print("}", indent, "plain");
                break;
            case ILLang.op_class:
                decompileModifiers(tree.down(1), indent, context);
                if (tree.down(1).length() > 0) {
                    space();
                }
                print("class", indent, "plain");
                space();
                printThingName(tree.down(2), indent, "plain");
                space();
                if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(3))) {
                    print(":", indent, "plain");
                    space();
                    decompile(tree.down(3), indent + INDENT_STEP + 4, precedence, contextPlus(context, INPARAMSLIS));
                    space();
                }
                print("{", indent, "plain");
                if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(4))) {
                    newLine(indent);
                    decompile(tree.down(4), indent + INDENT_STEP, precedence, contextPlus(context, INBRACES_CTX));
                    newLine(indent);
                }
                print("}", indent, "plain");
                print(";", indent, "plain");
                break;
            case ILLang.op_function:
                if (tree.down(4).opCode() != ILLang.op_ident && tree.down(4).opCode() != ILLang.op_scopeAccess) {
                    TapEnv.toolWarning(-1, "(C decompiler) [Function] Unexpected function name " + tree.down(4).opName());
                    break;
                }
                decompileModifiers(tree.down(1), indent, context);
                if (tree.down(1).length() > 0) {
                    space();
                }
                decompileTypeSpec(tree.down(2), indent, precedence, context);
                space();
                printThingName(tree.down(4), indent, "plain");
                print("(", indent, "plain");
                int indentForParams = indent + INDENT_STEP + INDENT_STEP; // [llh 16Feb2018] 2nd INDENT_STEP should be (isAFunctionPrototype(tree)?0:INDENT_STEP)
                if (indentForParams > posX) {
                    indentForParams = posX;
                }
                decompile(tree.down(5), indentForParams, precedence,
                        contextPlus(context, INPARAMSLIS));
                print(")", indent, "plain");
                if (isAFunctionPrototype(tree)) {
                    print(";", indent, "plain");
                } else {
                    space();
                    print("{", indent, "plain");
                    if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(6))) {
                        context = contextPlus(context, INBRACES_CTX);
                        newLine(indent);
                        decompile(tree.down(6), indent + INDENT_STEP, precedence, context);
                        newLine(indent);
                    }
                    print("}", indent, "plain");
                }
                endOfUnit = true;
                break;
            case ILLang.op_program:
                /* checks */
                if (tree.down(2).opCode() != ILLang.op_ident && tree.down(2).opCode() != ILLang.op_scopeAccess) {
                    TapEnv.toolWarning(-1, "(C decompiler) [Function] Unexpected program name " + tree.down(2).opName());
                    break;
                }
                decompileModifiers(tree.down(1), indent, context);
                if (tree.down(1).length() > 0) {
                    space();
                }
                printThingName(tree.down(2), indent, "plain");
                print("(", indent, "plain");
                decompile(tree.down(3), indent, precedence, contextPlus(context, INPARAMSLIS));
                print(")", indent, "plain");
                braces = tree.down(4).opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                    newLine(indent);
                }
                decompile(tree.down(4), indent + INDENT_STEP, precedence, context);
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                endOfUnit = true;
                break;
            case ILLang.op_constructor:
                decompileModifiers(tree.down(1), indent, context); //qualifiers
                if (tree.down(1).length() > 0) {
                    space();
                }
                printThingName(tree.down(2), indent, "plain"); //name
                print("(", indent, "plain");
                decompile(tree.down(3), indent, precedence, contextPlus(context, INPARAMSLIS)); //arguments
                print(")", indent, "plain");
                decompileExpression(tree.down(4), indent, precedence, context); //pre-block
                braces = tree.down(5).opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                    newLine(indent);
                }
                decompile(tree.down(5), indent + INDENT_STEP, precedence, context); //body
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                endOfUnit = true;
                break;
            case ILLang.op_destructor:
                decompileModifiers(tree.down(1), indent, context); //qualifiers
                if (tree.down(1).length() > 0) {
                    space();
                }
                printThingName(tree.down(2), indent, "plain"); //name
                print("()", indent, "plain");
                braces = tree.down(3).opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                    newLine(indent);
                }
                decompile(tree.down(3), indent + INDENT_STEP, precedence, context); //body
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                endOfUnit = true;
                break;
            case ILLang.op_parentClasses:
            case ILLang.op_declarations:
            case ILLang.op_varDeclarations: {
                Tree[] elements = tree.children();
                for (int i = 0; i < elements.length; i++) {
                    decompile(elements[i], indent, precedence, context);
                    if (i < elements.length - 1) {
                        if (hasContext(context, INPARAMSLIS)) {
                            print(",", indent, "plain");
                            space();
                        } else {
                            newLine(indent);
                        }
                    }
                }
                break;
            }
            case ILLang.op_parentClass:
                decompileModifiers(tree.down(1), indent, context);
                space();
                decompile(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_varDeclaration: {
                Tree type = tree.down(2);
                if (type.opCode() == ILLang.op_functionType) {
                    Tree[] pair2 = buildDeclaratorFromType(type, ILUtils.build(ILLang.op_none));
                    decompileTypeSpec(pair2[TYPE_TREE], indent, precedence, context);
                    space();
                    boolean paren = false;
                    if (tree.down(3).length() != 0) {
                        paren = true;
                    }
                    if (paren) {
                        print("(", indent, "plain");
                    }
                    decompileDeclarator(tree.down(3), indent, precedence, context);
                    if (paren) {
                        print(")", indent, "plain");
                    }
                    decompileDeclarator(pair2[DECL_TREE], indent, precedence, context);
                } else if (type.opCode() == ILLang.op_pointerType
                        || type.opCode() == ILLang.op_arrayType
                        || type.opCode() == ILLang.op_modifiedType
                        && (type.down(2).opCode() == ILLang.op_pointerType
                        || type.down(2).opCode() == ILLang.op_arrayType)) {
                    int length = tree.down(3).length();
                    if (length > 0) {
                        Tree[] pair = buildDeclaratorFromType(type, tree.down(3).down(1));
                        decompileTypeSpec(pair[TYPE_TREE], indent, precedence, context);
                        space();
                        decompileDeclarator(pair[DECL_TREE], indent, precedence, context);
                        if (!hasContext(context, INPARAMSLIS)) {
                            if (1 != length) {
                                print(",", indent, "plain");
                                space();
                            }
                        }
                        for (int i = 2; i <= length; i++) {
                            Tree[] pair2 = buildDeclaratorFromType(type, tree.down(3).down(i));
                            decompileDeclarator(pair2[DECL_TREE], indent, precedence, context);
                            if (!hasContext(context, INPARAMSLIS)) {
                                if (i != length) {
                                    print(",", indent, "plain");
                                    space();
                                }
                            }
                        }
                    } else {
                        if (type.opCode() != ILLang.op_none) {
                            decompileTypeSpec(type, indent, precedence, context);
                            if (tree.down(3).length() > 0) {
                                space();
                            }
                        }
                        decompileDeclarator(tree.down(3), indent, precedence, context);
                    }
                    if (!hasContext(context, INPARAMSLIS)) {
                        print(";", indent, "plain");
                    }
                } else {
                    if (ILUtils.isNullOrNone(type) || tree.down(3).length() != 1) {
                        if (type.opCode() != ILLang.op_none) {
                            decompileTypeSpec(type, indent, precedence, context);
                            if (tree.down(3).length() > 0) {
                                space();
                            }
                        }
                        decompileDeclarator(tree.down(3), indent, precedence, context);
                    } else {
                        //Try and put the "const" first:
                        Tree[] pair = buildDeclaratorFromType(type, tree.down(3).down(1));
                        decompileTypeSpec(pair[TYPE_TREE], indent, precedence, context);
                        space();
                        decompileDeclarator(pair[DECL_TREE], indent, precedence, context);
                    }
                    if (!hasContext(context, INPARAMSLIS)) {
                        print(";", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_constDeclaration:
                print("const", indent, "plain");
                space();
                if (tree.down(1).opCode() == ILLang.op_pointerType
                        || tree.down(1).opCode() == ILLang.op_arrayType
                        || tree.down(1).opCode() == ILLang.op_modifiedType && (tree
                        .down(1).down(2).opCode() == ILLang.op_pointerType || tree
                        .down(1).down(2).opCode() == ILLang.op_arrayType)) {
                    int length = tree.down(2).length();
                    if (length > 0) {
                        Tree[] pair = buildDeclaratorFromType(tree.down(1),
                                tree.down(2).down(1));
                        decompileTypeSpec(pair[TYPE_TREE], indent, precedence, context);
                        space();
                        decompileDeclarator(pair[DECL_TREE], indent, precedence,
                                context);
                        if (!hasContext(context, INPARAMSLIS)) {
                            if (1 != length) {
                                print(",", indent, "plain");
                                space();
                            }
                        }
                        for (int i = 2; i <= length; i++) {
                            Tree[] pair2 = buildDeclaratorFromType(
                                    tree.down(1), tree.down(2).down(i));
                            decompileDeclarator(pair2[DECL_TREE], indent, precedence,
                                    context);
                            if (!hasContext(context, INPARAMSLIS)) {
                                if (i != length) {
                                    print(",", indent, "plain");
                                    space();
                                }
                            }
                        }
                        if (!hasContext(context, INPARAMSLIS)) {
                            print(";", indent, "plain");
                        }
                    } else {
                        if (tree.down(1).opCode() != ILLang.op_none) {
                            decompileTypeSpec(tree.down(1), indent, precedence,
                                    context);
                            if (tree.down(2).length() > 0) {
                                space();
                            }
                        }
                        decompileDeclarator(tree.down(2), indent, precedence,
                                context);
                        if (!hasContext(context, INPARAMSLIS)) {
                            print(";", indent, "plain");
                        }
                    }
                } else {
                    if (tree.down(1).opCode() != ILLang.op_none) {
                        decompileTypeSpec(tree.down(1), indent, precedence,
                                context);
                        if (tree.down(2).length() > 0) {
                            space();
                        }
                    }
                    decompileDeclarator(tree.down(2), indent, precedence,
                            context);
                    if (!hasContext(context, INPARAMSLIS)) {
                        print(";", indent, "plain");
                    }
                }
                break;
            case ILLang.op_variableArgList:
                print("...", indent, "plain");
                if (!hasContext(context, INPARAMSLIS)) {
                    print(";", indent, "plain");
                }
                break;
            case ILLang.op_typeDeclaration: {
                Tree decl = tree.down(1);
                if (decl.opCode() == ILLang.op_ident) {
                    if (tree.down(2).opCode() == ILLang.op_recordType
                            && decl.stringValue().startsWith("struct ")) {
                        decl = ILUtils.build(ILLang.op_none);
                    } else if (tree.down(2).opCode() == ILLang.op_unionType
                            && decl.stringValue().startsWith("union ")) {
                        decl = ILUtils.build(ILLang.op_none);
                    } else if (tree.down(2).opCode() == ILLang.op_enumType
                            && decl.stringValue().startsWith("enum ")) {
                        decl = ILUtils.build(ILLang.op_none);
                    }
                }
                Tree type = tree.down(2);
                if (decl.opCode() != ILLang.op_ident &&
                        decl.opCode() != ILLang.op_none &&
                        decl.opCode() != ILLang.op_scopeAccess) {
                    TapEnv.toolWarning(-1, "(C decompiler) [typeDeclaration] Bad TypeName " + decl.opName());
                } else {
                    boolean trueTypedef = false;
                    boolean defFunction = type.opCode() == ILLang.op_functionType
                            || type.opCode() == ILLang.op_arrayType
                            || type.opCode() == ILLang.op_pointerType
                            && type.down(1).opCode() == ILLang.op_functionType;
                    if (decl.opCode() == ILLang.op_ident) {
                        trueTypedef = true;
                    }
                    Tree[] pair = buildDeclaratorFromType(type, decl);
                    if (trueTypedef) {
                        print("typedef", indent, "keyword");
                        space();
                    }
                    if (defFunction) {
                        decompileTypeSpec(pair[TYPE_TREE], indent, precedence, context);
                    } else {
                        decompileTypeSpec(type, indent, precedence, context);
                    }
                    if (trueTypedef) {
                        space();
                        if (defFunction) {
                            decompileDeclarator(pair[DECL_TREE], indent, precedence, context);
                        } else {
                            decompileDeclarator(decl, indent, precedence, context);
                        }
                    }
                }
                print(";", indent, "plain");
                break;
            }
            case ILLang.op_include:
                print("#include " + tree.stringValue(), indent, "include");
                break;
            case ILLang.op_labelStatement:
                decompileExpression(tree.down(1), indent - INDENT_STEP / 2, 0, context);
                print(":", indent, "plain");
                newLine(indent);
                decompile(tree.down(2), indent, precedence, context);
                // In a labelStatement only (?), op_none must print a ";"
                if (tree.down(2).opCode()==ILLang.op_none) {
                    print(";", indent, "plain");
                }
                break;
            case ILLang.op_if: {
                print("if", indent, "keyword");
                space();
                print("(", indent, "plain");
                decompileExpression(tree.down(1), indent, precedence, context);
                print(")", indent, "plain");
                braces = hasDeclsOrIsLongOrIsEmpty(tree.down(2), true);
                boolean hasElse;
                hasElse = !ILUtils.isNullOrNoneOrEmptyList(tree.down(3));
                if (hasElse && !braces && hasSingleIfNoElse(tree.down(2))) {
                    braces = true;
                }
                context = contextMinus(context, INBRACES_CTX);
                int context2 = context;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context2 = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(tree.down(2), indent + INDENT_STEP, precedence, context2);
                if (braces) {
                    newLine(indent + INDENT_STEP);
                    print("}", indent, "plain");
                }
                if (hasElse) {
                    if (braces) {
                        space();
                    } else {
                        newLine(indent);
                    }
                    context2 = context;
                    print("else", indent, "keyword");
                    braces = hasDeclsOrIsLongOrIsEmpty(tree.down(3), true);
                    Tree singleElse = null;
                    if (!braces) {
                        singleElse = singleInstructionIn(tree.down(3));
                    }
                    if (singleElse != null && singleElse.opCode() == ILLang.op_if) {
                        /* case of a if else if... */
                        space();
                        decompile(singleElse, indent, precedence, context2);
                    } else {
                        if (braces) {
                            space();
                            print("{", indent, "plain");
                            context2 = contextPlus(context, INBRACES_CTX);
                        }
                        newLine(indent);
                        decompile(tree.down(3), indent + INDENT_STEP, precedence, context2);
                        if (braces) {
                            newLine(indent);
                            print("}", indent, "plain");
                        }
                    }
                }
                break;
            }
            case ILLang.op_switch: {
                print("switch", indent, "keyword");
                space();
                print("(", indent, "plain");
                decompileExpression(tree.down(1), indent, precedence, context);
                print(")", indent, "plain");
                space();
                print("{", indent, "plain");
                newLine(indent);
                Tree[] cases = tree.down(2).children();
                context = contextMinus(context, INBRACES_CTX);
                for (Tree aCase : cases) {
                    decompile(aCase, indent, precedence, context);
                    newLine(indent);
                }
                print("}", indent, "plain");
                break;
            }
            case ILLang.op_switchCase: {
                Tree[] subCases = tree.down(1).children();
                if (subCases.length == 0) {
                    print("default", indent, "keyword");
                    space();
                    print(":", indent, "plain");
                } else {
                    for (int i = 0; i < subCases.length; i++) {
                        print("case", indent, "keyword");
                        space();
                        decompileExpression(subCases[i], indent, precedence, context);
                        space();
                        print(":", indent, "plain");
                        if (i < subCases.length - 1) {
                            newLine(indent);
                        }
                    }
                }
                if (tree.down(2).length() != 0) {
                    braces = hasDeclsOrIsLongOrIsEmpty(tree.down(2), false);
                    if (braces) {
                        space();
                        print("{", indent, "plain");
                        context = contextPlus(context, INBRACES_CTX);
                    }
                    newLine(indent);
                    decompile(tree.down(2), indent + INDENT_STEP, precedence, context);
                    if (braces) {
                        newLine(indent);
                        print("}", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_loop:
                decompileLoop(tree, indent, precedence, context);
                break;
            case ILLang.op_forallRangeSet:
                decompile(tree.down(1), indent, precedence, contextPlus(context, INPARAMSLIS));
                space();
                print(":", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_try:
                print("try", indent, "keyword");
                space();
                print("{", indent, "plain");
                newLine(indent);
                decompile(tree.down(1), indent + INDENT_STEP, precedence, contextPlus(context, INBRACES_CTX));
                newLine(indent);
                print("}", indent, "plain");
                space();
                decompile(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_catchs: {
                Tree[] catches = tree.children();
                for (Tree aCatch : catches) {
                    decompile(aCatch, indent, precedence, context);
                }
                break;
            }
            case ILLang.op_catch:
                space();
                print("catch", indent, "keyword");
                space();
                print("(", indent, "plain");
                decompile(tree.down(1), indent, precedence, context);
                print(") {", indent, "plain");
                newLine(indent);
                decompile(tree.down(2), indent + INDENT_STEP, precedence, contextPlus(context, INBRACES_CTX));
                newLine(indent);
                print("}", indent, "plain");
                space();
                break;
            case ILLang.op_throw:
                print("throw", indent, "keyword");
                space();
                decompile(tree.down(1), indent, precedence, context);
                print(";", indent, "plain");
                break;
            case ILLang.op_break:
            case ILLang.op_exit:
                print("break", indent, "keyword");
                print(";", indent, "plain");
                break;
            case ILLang.op_continue:
                // In IL, our op_continue means no-op, not the C continue which means cycle (i.e. our op_cycle).!
                break;
            case ILLang.op_none:
                break;
            case ILLang.op_cycle:
                print("continue", indent, "keyword");
                print(";", indent, "plain");
                break;
            case ILLang.op_return:
                print("return", indent, "keyword");
                if (tree.down(1).opCode() != ILLang.op_none) {
                    space();
                    decompileExpression(tree.down(1), indent, precedence, context);
                }
                print(";", indent, "plain");
                break;
            case ILLang.op_goto:
                print("goto", indent, "keyword");
                space();
                printLabel(tree.down(1), indent);
                print(";", indent, "plain");
                break;
            case ILLang.op_implicit:
                break;
            case ILLang.op_accessDecl:
                decompileExpression(tree.down(1), indent - 2, precedence, context);
                space();
                print(":", indent, "plain");
                break;
            case ILLang.op_friendsDeclaration:
                if (!ILUtils.isNullOrNoneOrEmptyList(tree)) {
                    Tree[] friendsList = tree.children();
                    print("friend", indent, "keyword");
                    for (Tree value : friendsList) {
                        space();
                        decompileExpression(value, indent + 2, precedence, context);
                    }
                    print(";", indent, "plain");
                }
                break;
            default:
                decompileExpression(tree, indent, precedence, context);
                if (!hasContext(context, INPARAMSLIS)) {
                    print(";", indent, "plain");
                }
                break;
        }
        postDecompile(tree, indent);
    }

    private boolean isAFunctionPrototype(Tree tree) {
        return tree.down(6).opCode() == ILLang.op_none;
    }

    private void decompileTypeSpec(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        if (tree.opCode() == ILLang.op_modifiedType
                && tree.down(1).length() != 0
                && tree.down(1).down(1).opCode() == ILLang.op_ident
                && (tree.down(1).down(1).stringValue().equals("restrict")
                || tree.down(1).down(1).stringValue().equals("__restrict"))
                && tree.down(2).opCode() == ILLang.op_pointerType) {
            if (tree.down(1).length() > 1) {
                Tree otherModifiers = ILUtils.copy(tree.down(1));
                assert otherModifiers != null;
                otherModifiers.removeChild(1);
                decompileModifiers(otherModifiers, indent, context);
                space();
            }
            decompileTypeSpec(tree.down(2), indent, precedence, context);
            space();
            print(tree.down(1).down(1).stringValue(), indent, "modifier");
        } else {
            switch (tree.opCode()) {
                case ILLang.op_void:
                    print("void", indent, "typename");
                    break;
                case ILLang.op_integer:
                    print("int", indent, "typename");
                    break;
                case ILLang.op_float:
                    print("float", indent, "typename");
                    break;
                case ILLang.op_complex:
                    print("complex", indent, "typename");
                    break;
                case ILLang.op_boolean:
                    if (this.language == TapEnv.CPLUSPLUS) {
                        print("bool", indent, "typename");
                    } else {
                        print("_Bool", indent, "typename");
                    }
                    break;
                case ILLang.op_character:
                    print("char", indent, "typename");
                    break;
                case ILLang.op_ident:
                    printAtomValue(tree, indent, "typename");
                    break;
                case ILLang.op_modifiedType:
                    if (modifiersContains(tree.down(1), "restrict") != -1
                        || modifiersContains(tree.down(1), "__restrict") != -1) {
                        decompileTypeSpec(tree.down(2), indent, precedence, context);
                        space();
                        print(tree.down(1).down(1).stringValue(), indent, "modifier");
                    } else if (tree.down(2).opCode() == ILLang.op_pointerType) {
                        Tree[] pair1 = buildDeclaratorFromType(tree, ILUtils.build(ILLang.op_none));
                        decompileTypeSpec(pair1[TYPE_TREE], indent, precedence, context);
                        space();
                        decompileDeclarator(pair1[DECL_TREE], indent, precedence, context);
                    } else if ((modifiersContains(tree.down(1), "8") != -1
                            || modifiersContains(tree.down(1), "c_double") != -1)
                            && tree.down(2).opCode() == ILLang.op_float) {
                        // cas mixed-language real*8 et real(c_double) == double:
                        print("double", indent, "typename");
                    } else if (modifiersContains(tree.down(1), "c_int") != -1
                            && tree.down(2).opCode() == ILLang.op_integer) {
                        // cas mixed-language int(c_int) == int:
                        print("int", indent, "typename");
                    } else if (modifiersContains(tree.down(1), "float") != -1
                            && tree.down(2).opCode() == ILLang.op_float) {
                        //[llh 4Sept2017] Patch because a bug in version 6119 has created "float real" C types,
                        // and they must be understood as a plain "float".
                        decompileTypeSpec(tree.down(2), indent, precedence, context);
                    } else {
                        decompileModifiers(tree.down(1), indent, context);
                        // if modifiers contains "double", then the base type is "float"
                        // but it must not be printed : "double float" is not legal in C
                        // [vmp] double complex is legal
                        if (!((modifiersContains(tree.down(1), "double") != -1
                                || modifiersContains(tree.down(1), "long double") != -1)
                                && tree.down(2).opCode() == ILLang.op_float)) {
                            if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(1))) {
                                space();
                            }
                            decompileTypeSpec(tree.down(2), indent, precedence, context);
                        }
                    }
                    break;
                case ILLang.op_arrayType:
                case ILLang.op_pointerType:
                case ILLang.op_functionType:
                    Tree[] pair2 = buildDeclaratorFromType(tree, ILUtils.build(ILLang.op_none));
                    decompileTypeSpec(pair2[TYPE_TREE], indent, precedence, context);
                    space();
                    decompileDeclarator(pair2[DECL_TREE], indent, precedence, context);
                    break;
                case ILLang.op_recordType: {
                    boolean emptyVarDecl = tree.down(3).opCode() == ILLang.op_none
                            || tree.down(3).opCode() == ILLang.op_varDeclarations
                            && tree.down(3).length() == 0;
                    if (tree.down(1).opCode() == ILLang.op_ident) {
                        if (!tree.down(1).stringValue().startsWith("struct ")) {
                            print("struct", indent, "keyword");
                            space();
                        }
                        printThingName(tree.down(1), indent, "typename");
                        if (!emptyVarDecl) {
                            space();
                        }
                    } else {
                        print("struct", indent, "keyword");
                        space();
                    }
                    // tree.down(2) est modifiers[] vide dans notre cas
                    if (!emptyVarDecl) {
                        print("{", indent, "plain");
                        newLine(indent);
                        decompile(tree.down(3), indent + INDENT_STEP, 0, context);
                        newLine(indent);
                        print("}", indent, "plain");
                    }
                    break;
                }
                case ILLang.op_unionType: {
                    boolean emptyVarDecl = tree.down(2).opCode() == ILLang.op_none
                            || tree.down(2).opCode() == ILLang.op_varDeclarations
                            && tree.down(2).length() == 0;
                    if (tree.down(1).opCode() == ILLang.op_ident) {
                        if (!tree.down(1).stringValue().startsWith("union ")) {
                            print("union", indent, "keyword");
                            space();
                        }
                        printThingName(tree.down(1), indent, "typename");
                        if (!emptyVarDecl) {
                            space();
                        }
                    } else {
                        print("union", indent, "keyword");
                        space();
                    }
                    if (!emptyVarDecl) {
                        print("{", indent, "plain");
                        newLine(indent);
                        decompile(tree.down(2), indent + INDENT_STEP, 0, context);
                        newLine(indent);
                        print("}", indent, "plain");
                    }
                    break;
                }
                case ILLang.op_enumType:
                    if (tree.down(1).opCode() == ILLang.op_ident) {
                        if (!tree.down(1).stringValue().startsWith("enum ")) {
                            print("enum", indent, "keyword");
                            space();
                        }
                        printThingName(tree.down(1), indent, "typename");
                        space();
                    } else {
                        print("enum", indent, "keyword");
                        space();
                    }
                    if (tree.down(2).opCode() == ILLang.op_declarators) {
                        print("{", indent, "plain");
                        decompileDeclarator(tree.down(2), indent, precedence, context);
                        print("}", indent, "plain");
                    }
                    break;
                case ILLang.op_referenceType:
                    decompileTypeSpec(tree.down(1), indent, precedence, context);
                    print("&", indent, "plain");
                    break;
                case ILLang.op_scopeAccess:
                    decompileTypeSpec(tree.down(1), indent, precedence, context);
                    print("::", indent, "plain");
                    decompileTypeSpec(tree.down(2), indent, precedence, context);
                    break;
                case ILLang.op_none:
                    break;
                default:
                    TapEnv.toolWarning(-1, "(C decompiler) [WrapperTypeSpec] Unexpected operator " + tree.opName());
                    break;
            }
        }
        postDecompile(tree, indent);
    }

    private void decompileModifiers(Tree tree, int indent, int context) throws IOException {
        switch (tree.opCode()) {
            case ILLang.op_modifiers: {
                Tree[] elements = tree.children();
                TapList<Tree> sortedModifiers = sortModifiers(elements);
                while (sortedModifiers != null) {
                    decompileModifiers(sortedModifiers.head, indent, context);
                    if (sortedModifiers.tail != null) {
                        space();
                    }
                    sortedModifiers = sortedModifiers.tail;
                }
                break;
            }
            case ILLang.op_ident: {
                String modifierText = tree.stringValue();
                if (modifierText != null) {
                    if (modifierText.equals("private")
                            || modifierText.equals("save")) {
                        print("static", indent, "modifier");
                    } else if (modifierText.equals("external")) {
                        print("extern", indent, "modifier");
                    } else {
                        print(modifierText, indent, "modifier");
                    }
                }
                break;
            }
            case ILLang.op_intCst:
            case ILLang.op_none: {
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(C decompiler) [Modifier] Unexpected operator " + tree.opName());
                break;
        }
    }

    private void decompileExpression(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        switch (tree.opCode()) {
            case ILLang.op_unary: {
                String opStr = tree.down(1).stringValue();
                switch (opStr) {
                    case "++prefix":
                        decompileUnaryExprPrefix(tree.down(2), indent, precedence,
                                context, "++", PREC_PREINCR, PREC_PREINCR);
                        break;
                    case "--prefix":
                        decompileUnaryExprPrefix(tree.down(2), indent, precedence,
                                context, "--", PREC_PREDECR, PREC_PREDECR);
                        break;
                    case "++postfix":
                        decompileUnaryExprPostfix(tree.down(2), indent, precedence,
                                context, "++", PREC_POSTINCR, PREC_POSTINCR);
                        break;
                    case "--postfix":
                        decompileUnaryExprPostfix(tree.down(2), indent, precedence,
                                context, "--", PREC_POSTDECR, PREC_POSTDECR);
                        break;
                    case "~":
                        decompileUnaryExprPrefix(tree.down(2), indent, precedence,
                                context, "~", PREC_BNOT, PREC_BNOT);
                        break;
                    case "+":
                        decompileUnaryExprPrefix(tree.down(2), indent, precedence,
                                context, "+", PREC_PLUS, PREC_PLUS);
                        break;
                    default:
                        decompileUnaryExprPrefix(tree.down(2), indent, precedence,
                                context, opStr, PREC_UNARY, PREC_UNARY);
                        break;
                }
                break;
            }
            case ILLang.op_minus:
                decompileUnaryExprPrefix(tree.down(1), indent, precedence,
                        context, "-", PREC_MINUS, PREC_MINUS);
                break;
            case ILLang.op_not:
                decompileUnaryExprPrefix(tree.down(1), indent, precedence,
                        context, "!", PREC_NOT, PREC_NOT);
                break;
            case ILLang.op_bitNot:
                decompileUnaryExprPrefix(tree.down(1), indent, precedence,
                        context, "~", PREC_NOT, PREC_NOT);
                break;
            case ILLang.op_binary: {
                String opStr = tree.down(1).stringValue();
                int maxPrecedence;
                int leftPrecedence;
                int rightPrecedence;
                switch (opStr) {
                    case "|":
                        maxPrecedence = PREC_BOR;
                        leftPrecedence = LPREC_BOR;
                        rightPrecedence = RPREC_BOR;
                        break;
                    case "&":
                        maxPrecedence = PREC_BAND;
                        leftPrecedence = LPREC_BAND;
                        rightPrecedence = RPREC_BAND;
                        break;
                    case "^":
                        maxPrecedence = PREC_BXOR;
                        leftPrecedence = LPREC_BXOR;
                        rightPrecedence = RPREC_BXOR;
                        break;
                    default:
                        maxPrecedence = PREC_BINARY;
                        leftPrecedence = LPREC_BINARY;
                        rightPrecedence = RPREC_BINARY;
                        break;
                }
                decompileBinaryExpr(tree.down(2), tree.down(3),
                        indent, precedence, context, tree.opCode(),
                        opStr, maxPrecedence, leftPrecedence, rightPrecedence);
                break;
            }
            case ILLang.op_add:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "+", PREC_ADD, LPREC_ADD, RPREC_ADD);
                break;
            case ILLang.op_sub:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "-", PREC_SUB, LPREC_SUB, RPREC_SUB);
                break;
            case ILLang.op_mul:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "*", PREC_MUL, LPREC_MUL, RPREC_MUL);
                break;
            case ILLang.op_div:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "/", PREC_DIV, LPREC_DIV, RPREC_DIV);
                break;
            case ILLang.op_power:
                boolean power2 =
                        tree.down(2).opCode() == ILLang.op_intCst && tree.down(2).intValue() == 2;
                if (power2) {
                    decompileBinaryExpr(tree.down(1), tree.down(1),
                            indent, precedence, context, tree.opCode(),
                            "*", PREC_MUL, LPREC_MUL, RPREC_MUL);
                } else {
                    print("pow(", indent, "plain");
                    decompileExpression(tree.down(1), indent, RPREC_FCALL, context);
                    print(",", indent, "plain");
                    space();
                    decompileExpression(tree.down(2), indent, RPREC_FCALL, context);
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_leftShift:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "<<", PREC_LSHIFT, LPREC_LSHIFT, RPREC_LSHIFT);
                break;
            case ILLang.op_rightShift:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        ">>", PREC_RSHIFT, LPREC_RSHIFT, RPREC_RSHIFT);
                break;
            case ILLang.op_mod:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "%", PREC_MOD, LPREC_MOD, RPREC_MOD);
                break;
            case ILLang.op_eq:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "==", PREC_EQ, LPREC_EQ, RPREC_EQ);
                break;
            case ILLang.op_neq:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "!=", PREC_NEQ, LPREC_NEQ, RPREC_NEQ);
                break;
            case ILLang.op_ge:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        ">=", PREC_GE, LPREC_GE, RPREC_GE);
                break;
            case ILLang.op_gt:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        ">", PREC_GT, LPREC_GT, RPREC_GT);
                break;
            case ILLang.op_le:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "<=", PREC_LE, LPREC_LE, RPREC_LE);
                break;
            case ILLang.op_lt:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "<", PREC_LT, LPREC_LT, RPREC_LT);
                break;
            case ILLang.op_and:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "&&", PREC_AND, LPREC_AND, RPREC_AND);
                break;
            case ILLang.op_bitAnd:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "&", PREC_AND, LPREC_AND, RPREC_AND);
                break;
            case ILLang.op_or:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "||", PREC_OR, LPREC_OR, RPREC_OR);
                break;
            case ILLang.op_bitOr:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "|", PREC_OR, LPREC_OR, RPREC_OR);
                break;
            case ILLang.op_bitXor:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "^", PREC_BXOR, LPREC_BXOR, RPREC_BXOR);
                break;
            case ILLang.op_ident:
                printAtomValue(tree, indent, "plain");
                break;
            case ILLang.op_concatIdent:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "", 8, 8, 9);
                break;
            case ILLang.op_scopeAccess:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "::", 32, 32, 33);
                break;
            case ILLang.op_arrayAccess: {
                decompileExpression(tree.down(1), indent, LPREC_SUBSCRIPT, context);
                Tree[] dims = tree.down(2).children();
                for (int i = dims.length - 1; i >= 0; i--) {
                    print("[", indent, "keyword");
                    decompileExpression(dims[i], indent, 0, context);
                    print("]", indent, "keyword");
                }
                break;
            }
            case ILLang.op_fieldAccess:
                if (isAStarPointerAccess(tree.down(1))) {
                    boolean paren = isAStarPointerAccess(tree.down(1).down(1));
                    if (paren) {
                        print("(", indent, "plain");
                    }
                    decompileExpression(tree.down(1).down(1), indent, 0, context);
                    if (paren) {
                        print(")", indent, "plain");
                    }
                    print("->", indent, "keyword");
                } else {
                    decompileExpression(tree.down(1), indent, LPREC_FIELD, context);
                    print(".", indent, "keyword");
                }
                decompileExpression(tree.down(2), indent, RPREC_FIELD, context);
                break;
            case ILLang.op_pointerAccess: {
                if (ILUtils.isNullOrNone(tree.down(2))) {
                    print("*", indent, "keyword");
                    decompileExpression(tree.down(1), indent, PREC_PTRDEREF, context);
                } else {
                    boolean paren = (tree.down(1).opCode() == ILLang.op_pointerAccess
                                     && !ILUtils.isNullOrNone(tree.down(2))
                                     && ILUtils.isNullOrNone(tree.down(1).down(2))) ;
                    if (paren) {
                        print("(", indent, "plain");
                    }
                    decompileExpression(tree.down(1), indent, PREC_PTRDEREF, context);
                    if (paren) {
                        print(")", indent, "plain");
                    }
                    print("[", indent, "keyword");
                    decompileExpression(tree.down(2), indent, 0, context);
                    print("]", indent, "keyword");
                }
                break;
            }
            case ILLang.op_constructorCall: {
                decompileExpression(tree.down(1), indent, 0, context);
                print("(", indent, "plain");
                /*list of arguments: */
                decompileExpression(tree.down(3),
                        hasContext(context, RESULTEXPECTED_CTX) ? indent : indent + 1,
                        0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                break;
            }
	    case ILLang.op_parallelRegion: {
                // So far only for the Cuda case (i.e. tree.down(1) == ident:"CUDA")
		parallelSpreadSizes = tree.down(2) ;
		decompileExpression(tree.down(3), indent, 0, context);
		parallelSpreadSizes = null ;
		break ;
	    }
            case ILLang.op_call: {
                String calledString = ILUtils.getCalledNameString(tree);
                if (!ILUtils.isNullOrNone(ILUtils.getObject(tree))) {
                    decompileExpression(ILUtils.getObject(tree), indent, 0, context);
                    print(".", indent, "plain");
                    decompileExpression(ILUtils.getCalledName(tree), indent, 0, context);
		    if (parallelSpreadSizes!=null) {
			print("<<<", indent, "plain");
			decompileExpression(parallelSpreadSizes, indent, 0, context);
			print(">>>", indent, "plain");
		    }
                    if (!hasContext(context, RESULTEXPECTED_CTX)) {
                        indent = posX;
                    }
                    print("(", indent, "plain");
                    // list of arguments:
                    decompileExpression(ILUtils.getArguments(tree),
                            hasContext(context, RESULTEXPECTED_CTX) ? indent : indent + 1,
                            0, contextPlus(context, RESULTEXPECTED_CTX));
                    print(")", indent, "plain");
                } else if ("ifExpression".equals(calledString)) {
                    Tree arguments = ILUtils.getArguments(tree);
                    print("(", indent, "plain");
                    decompileExpression(arguments.down(1), indent, PREC_CONDEXP, context);
                    space();
                    print("?", indent, "plain");
                    space();
                    decompileExpression(arguments.down(2), indent, PREC_CONDEXP, context);
                    space();
                    print(":", indent, "plain");
                    space();
                    decompileExpression(arguments.down(3), indent, PREC_CONDEXP, context);
                    print(")", indent, "plain");
                } else if ("operator()".equals(calledString)) {
                    Tree[] arguments = ILUtils.getArguments(tree).children();
                    decompileExpression(arguments[0], indent, 0, context);
                    if (!hasContext(context, RESULTEXPECTED_CTX)) {
                        indent = posX;
                    }
                    print("(", indent, "plain");
                    // list of arguments:
                    for (int i = 1; i < arguments.length; ++i) {
                        if (i != 1) {
                            print(",", indent, "plain");
                            space();
                        }
                        decompileExpression(arguments[i], indent, 0, context);
                    }
                    print(")", indent, "plain");
                } else if (calledString != null && calledString.startsWith("operator")
                        && ILUtils.getArguments(tree).length() == 2) {
                    Tree arguments = ILUtils.getArguments(tree);
                    switch (calledString) {
                        case "operator=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator&=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("&=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator/=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("/=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator|=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("|=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator<<=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("<<=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator-=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("-=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator%=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("%=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator+=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("+=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator*=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("*=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator^=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print("^=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator>>=":
                            if (precedence > PREC_ASSIGN) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, LPREC_ASSIGN, context);
                            space();
                            print(">>=", indent, "keyword");
                            space();
                            decompileExpression(arguments.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                            if (precedence > PREC_ASSIGN) {
                                print(")", indent, "plain");
                            }
                            break;
                        case "operator[]":
                            boolean paren = arguments.down(1).opCode() == ILLang.op_pointerAccess
                                    && !ILUtils.isNullOrNone(arguments.down(2))
                                    && ILUtils.isNullOrNone(arguments.down(1).down(2));
                            if (paren) {
                                print("(", indent, "plain");
                            }
                            decompileExpression(arguments.down(1), indent, PREC_PTRDEREF, context);
                            if (paren) {
                                print(")", indent, "plain");
                            }
                            print("[", indent, "keyword");
                            decompileExpression(arguments.down(2), indent, 0, context);
                            print("]", indent, "keyword");
                            break;
                        case "operator->":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "->", PREC_FIELD, LPREC_FIELD, RPREC_FIELD);
                            break;
                        case "operator<<":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "<<", PREC_LSHIFT, LPREC_LSHIFT, RPREC_LSHIFT);
                            break;
                        case "operator>>":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    ">>", PREC_RSHIFT, LPREC_RSHIFT, RPREC_RSHIFT);
                            break;
                        case "operator==":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "==", PREC_EQ, LPREC_EQ, RPREC_EQ);
                            break;
                        case "operator!=":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "!=", PREC_NEQ, LPREC_NEQ, RPREC_NEQ);
                            break;
                        case "operator>=":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    ">=", PREC_GE, LPREC_GE, RPREC_GE);
                            break;
                        case "operator>":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    ">", PREC_GT, LPREC_GT, RPREC_GT);
                            break;
                        case "operator<=":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "<=", PREC_LE, LPREC_LE, RPREC_LE);
                            break;
                        case "operator<":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "<", PREC_LT, LPREC_LT, RPREC_LT);
                            break;
                        case "operator%":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "%", PREC_MOD, LPREC_MOD, RPREC_MOD);
                            break;
                        case "operator+":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "+", PREC_ADD, LPREC_ADD, RPREC_ADD);
                            break;
                        case "operator-":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "-", PREC_SUB, LPREC_SUB, RPREC_SUB);
                            break;
                        case "operator*":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "*", PREC_MUL, LPREC_MUL, RPREC_MUL);
                            break;
                        case "operator/":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "/", PREC_DIV, LPREC_DIV, RPREC_DIV);
                            break;
                        case "operator&":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "&", PREC_AND, LPREC_AND, RPREC_AND);
                            break;
                        case "operator|":
                            decompileBinaryExpr(arguments.down(1), arguments.down(2),
                                    indent, precedence, context, -1,
                                    "|", PREC_OR, LPREC_OR, RPREC_OR);
                            break;
                        default:
                            break;
                    }
                } else {
                    Tree annot = tree.getAnnotation("sourcetree");
                    if (annot != null) {
                        String opStr;
                        if (annot.opCode() == ILLang.op_unary) {
                            opStr = annot.down(1).stringValue();
                            switch (opStr) {
                                case "++prefix":
                                    decompileUnaryExprPrefix(annot.down(2), indent, precedence,
                                            context, "++", PREC_PREINCR, PREC_PREINCR);
                                    break;
                                case "--prefix":
                                    decompileUnaryExprPrefix(annot.down(2), indent, precedence,
                                            context, "--", PREC_PREDECR, PREC_PREDECR);
                                    break;
                                case "++postfix":
                                    decompileUnaryExprPostfix(annot.down(2), indent, precedence,
                                            context, "++", PREC_POSTINCR, PREC_POSTINCR);
                                    break;
                                case "--postfix":
                                    decompileUnaryExprPostfix(annot.down(2), indent, precedence,
                                            context, "--", PREC_POSTDECR, PREC_POSTDECR);
                                    break;
                                case "~":
                                    decompileUnaryExprPrefix(annot.down(2), indent, precedence,
                                            context, "~", PREC_BNOT, PREC_BNOT);
                                    break;
                                case "+":
                                    decompileUnaryExprPrefix(annot.down(2), indent, precedence,
                                            context, "+", PREC_PLUS, PREC_PLUS);
                                    break;
                                default:
                                    decompileUnaryExprPrefix(annot.down(2), indent, precedence,
                                            context, opStr, PREC_UNARY, PREC_UNARY);
                                    break;
                            }
                        } else if (annot.opCode() == ILLang.op_binary) {
                            decompileExpression(annot, indent, precedence, context);
                        } else {
                            decompileExpression(annot, indent, precedence, context);
                        }
                    } else {
                        boolean paren = false;
                        if (isAStarPointerAccess(ILUtils.getCalledName(tree))) {
                            paren = true;
                        }
                        if (paren) {
                            print("(", indent, "plain");
                        }
                        decompileExpression(ILUtils.getCalledName(tree), indent, 0, context);
                        if (paren) {
                            print(")", indent, "plain");
                        }
                        if (!hasContext(context, RESULTEXPECTED_CTX)) {
                            indent = posX;
                        }
			if (parallelSpreadSizes!=null) {
			    print("<<<", indent, "plain");
			    decompileExpression(parallelSpreadSizes, indent, 0, context);
			    print(">>>", indent, "plain");
			}
                        print("(", indent, "plain");
                        /*list of arguments: */
                        decompileExpression(ILUtils.getArguments(tree),
                                hasContext(context, RESULTEXPECTED_CTX) ? indent : indent + 1,
                                0, contextPlus(context, RESULTEXPECTED_CTX));
                        print(")", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_ioCall: {
                decompileExpression(tree.down(1), indent, LPREC_FCALL, context);
                print("(", indent, "plain");
                indent = posX;
                decompileExpression(tree.down(3), indent, 0, context);
                print(")", indent, "plain");
                break;
            }
            case ILLang.op_address: {
                boolean paren0 = (precedence > PREC_ADDROF) ;
                if (paren0) {
                    print("(", indent, "plain");
                }
                print("&", indent, "keyword");
                boolean paren = tree.down(1).opCode() != ILLang.op_ident;
                // Apparently parentheses are never necessary, but code is clearer with parentheses:
                if (paren) {
                    print("(", indent, "plain");
                }
                decompileExpression(tree.down(1), indent, PREC_ADDROF, context);
                if (paren) {
                    print(")", indent, "plain");
                }
                if (paren0) {
                    print(")", indent, "plain");
                }
                break;
            }
            case ILLang.op_cast:
                print("(", indent, "plain");
                decompileTypeSpec(tree.down(1), indent, 0, context);
                print(")", indent, "plain");
                decompileExpression(tree.down(2), indent, PREC_CAST, context);
                break;
            case ILLang.op_sizeof:
                print("sizeof(", indent, "operator");
                if (isTypeSpec(tree.down(1))) {
                    decompileTypeSpec(tree.down(1), indent, PREC_SIZEOF, context);
                } else {
                    decompileExpression(tree.down(1), indent, PREC_SIZEOF, context);
                }
                print(")", indent, "operator");
                break;
            case ILLang.op_assign: {
                Tree annot = tree.getAnnotation("sourcetree");
                if (annot != null) {
                    decompileExpression(annot, indent, precedence, context);
                } else {
                    if (precedence > PREC_ASSIGN) {
                        print("(", indent, "plain");
                    }
                    decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                    space();
                    print("=", indent, "plain");
                    space();
                    decompileExpression(tree.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                    if (precedence > PREC_ASSIGN) {
                        print(")", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_bitAndAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("&=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_divAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("/=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_bitOrAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("|=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_leftShiftAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("<<=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_minusAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("-=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_modAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("%=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_plusAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("+=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_timesAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("*=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_bitXorAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("^=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_rightShiftAssign:
                decompileExpression(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print(">>=", indent, "keyword");
                space();
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                break;
            case ILLang.op_arrayConstructor: {
                print("{", indent, "plain");
                Tree[] elements = tree.children();
                for (int i = 0; i < elements.length; ++i) {
                    if (i > 0) {
                        print(",", indent + INDENT_STEP, "plain");
                        space();
                    }
                    decompileExpression(elements[i], indent + INDENT_STEP, 0, context);
                }
                print("}", indent, "plain");
                break;
            }
            case ILLang.op_stringCst:
                printString("\"" + tree.stringValue() + "\"");
                break;
            case ILLang.op_intCst:
            case ILLang.op_bitCst:
            case ILLang.op_realCst:
                printAtomValue(tree, indent, "constant");
                break;
            case ILLang.op_complexConstructor:
                if (ILUtils.isZero(tree.down(2))) {
                    decompileExpression(tree.down(1), indent, LPREC_ADD, context);
                } else if (ILUtils.isZero(tree.down(1))) {
                    decompileExpression(tree.down(2), indent, LPREC_MUL, context);
                    print("*", indent, "plain");
                    print("1.0iF", indent, "constant");
                } else {
                    print("(", indent, "plain");
                    decompileExpression(tree.down(1), indent, LPREC_ADD, context);
                    print("+", indent, "plain");
                    decompileExpression(tree.down(2), indent, LPREC_MUL, context);
                    print("*", indent, "plain");
                    print("1.0iF", indent, "constant");
                    print(")", indent, "plain");
                }
                break ;
            case ILLang.op_letter:
                print("'" + tree.stringValue() + "'", indent, "plain");
                break;
            case ILLang.op_label: {
                printLabel(tree, indent);
                break;
            }
            case ILLang.op_none:
            case ILLang.op_implicit:
            case ILLang.op_external:
                break;
            case ILLang.op_blockStatement:
                // weird case of comma-separated lists of expressions.
            case ILLang.op_expressions: {
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; ++i) {
                    if (sons[i].opCode() == ILLang.op_varDeclaration) {
                        decompile(sons[i], indent, precedence, contextPlus(context, INPARAMSLIS));
                    } else {
                        decompileExpression(sons[i], indent, precedence, context);
                    }
                    if (i + 1 < sons.length) {
                        print(",", indent, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_strings: {
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; ++i) {
                    if (sons[i].opCode() == ILLang.op_stringCst) {
                        decompileExpression(sons[i], indent, precedence, context);
                        if (i + 1 < sons.length) {
                            space();
                        }
                    } else {
                        TapEnv.toolWarning(-1, "(C decompiler) Unexpected operator in strings " + sons[i].opName());
                    }
                }
                break;
            }
            case ILLang.op_ifExpression:
                print("(", indent, "plain");
                decompileExpression(tree.down(1), indent, PREC_CONDEXP, context);
                space();
                print("?", indent, "plain");
                space();
                decompileExpression(tree.down(2), indent, PREC_CONDEXP, context);
                space();
                print(":", indent, "plain");
                space();
                decompileExpression(tree.down(3), indent, PREC_CONDEXP, context);
                print(")", indent, "plain");
                break;
            case ILLang.op_dimColon:
                if (tree.down(2).opCode() != ILLang.op_none) {
                    if (tree.down(1).opCode() == ILLang.op_none || ILUtils.evalsToZero(tree.down(1))) {
                        // dimColon(0, N)  -> [N+1]
                        decompileExpression(ILUtils.addTree(ILUtils.copy(tree.down(2)), ILUtils.build(ILLang.op_intCst, 1)),
                                indent, 0, context);
                    }
                }
                break;
            case ILLang.op_boolCst:
                print(tree.stringValue(), indent, "constant");
                break;
            case ILLang.op_common:
                print("common", indent, "keyword");
                space();
                if (ILUtils.isNotNone(tree.down(1))) {
                    print(tree.down(1).stringValue(), indent, "vardecl");
                } else {
                    print("//", indent, "vardecl");
                }
                space();
                decompile(tree.down(2), indent, 0, contextPlus(context, ISVARDECL_CTX));
                break;
            case ILLang.op_data:
                print("data", indent, "keyword");
                space();
                decompile(tree.down(1), indent + 4, 0, EMPTY_CONTEXT);
                space();
                print("/", indent + 3, "plain");
                decompile(tree.down(2), indent + 5, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print("/", indent + 4, "plain");
                break;
            case ILLang.op_equivalence: {
                print("equivalence", indent, "keyword");
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; i++) {
                    space();
                    print("(", indent + 4, "plain");
                    decompile(sons[i], indent + 5, 0, context);
                    print(")", indent + 4, "plain");
                    if (i < sons.length - 1) {
                        print(",", indent + 4, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_intrinsic: {
                print("intrinsic", indent, "keyword");
                space();
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; i++) {
                    decompile(sons[i], indent + 4, 0, contextPlus(context, ISFUNCNAME_CTX));
                    if (i < sons.length - 1) {
                        print(",", indent + 4, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_save: {
                print("save", indent, "keyword");
                space();
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; i++) {
                    decompile(sons[i], indent + 4, 0, context);
                    if (i < sons.length - 1) {
                        print(",", indent + 4, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_star:
                print("*", indent, "keyword");
                break;
            case ILLang.op_stop:
                print("stop", indent, "keyword");
                break;
            case ILLang.op_pointerDeclarator:
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_bitfieldDeclarator:
            case ILLang.op_functionDeclarator:
                decompileDeclarator(tree, indent, precedence, context);
                break;
            case ILLang.op_using:
                print("using", indent, "keyword");
                space();
                decompileExpression(tree.down(1), indent, precedence, context);
                print("::", indent, "keyword");
                decompileExpression(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_friendType:
                decompileExpression(tree.down(1), indent, precedence, context);
                break;
            default:
                if (isTypeSpec(tree)) {
                    decompileTypeSpec(tree, indent, precedence, context);
                } else {
                    TapEnv.toolWarning(-1, "(C decompiler) [Expression] Unexpected operator " + tree.opName());
                }
                break;
        }
        postDecompile(tree, indent);
    }

    private void decompileUnaryExprPrefix(Tree subTree, int indent,
                                          int curPrecedence, int context, String op, int maxPrecedence,
                                          int sonPrecedence) throws IOException {
        if (curPrecedence >= PREC_MUL || maxPrecedence >= PREC_MUL) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        if (curPrecedence > maxPrecedence) {
            print("(", indent, "plain");
        }
        if (op!=null) print(op, indent, "plain");
        decompileExpression(subTree, indent, sonPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (curPrecedence > maxPrecedence) {
            print(")", indent, "plain");
        }
    }

    private void decompileUnaryExprPostfix(Tree subTree, int indent,
                                           int curPrecedence, int context, String op, int maxPrecedence,
                                           int sonPrecedence) throws IOException {
        if (curPrecedence >= PREC_MUL || maxPrecedence >= PREC_MUL) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        if (curPrecedence > maxPrecedence) {
            print("(", indent, "plain");
        }
        decompileExpression(subTree, indent, sonPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (op!=null) print(op, indent, "plain");
        if (curPrecedence > maxPrecedence) {
            print(")", indent, "plain");
        }
    }

    private void decompileBinaryExpr(Tree leftTree, Tree rightTree, int indent,
                                     int curPrecedence, int context, int operator, String op,
                                     int maxPrecedence, int leftPrecedence, int rightPrecedence) throws IOException {
        boolean leftParen = operator == ILLang.op_mul && isAStarPointerAccess(leftTree);
        boolean rightParen = (operator == ILLang.op_mul || operator == ILLang.op_div)
                && isAStarPointerAccess(rightTree);
        if (curPrecedence >= PREC_MUL || maxPrecedence >= PREC_MUL) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        if (curPrecedence > maxPrecedence) {
            print("(", indent, "plain");
        }
        if (leftParen) {
            print("(", indent, "plain");
        }
        decompileExpression(leftTree, indent, leftPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (leftParen) {
            print(")", indent, "plain");
        }
        if (!hasContext(context, DEEPEXPR_CTX) &&
                leftTree.opCode() != ILLang.op_none) {
            space();
        }
        print(op, indent, "plain");
        if (!hasContext(context, DEEPEXPR_CTX) &&
                rightTree.opCode() != ILLang.op_none) {
            space();
        }
        if (rightParen) {
            print("(", indent, "plain");
        }
        decompileExpression(rightTree, indent, rightPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (rightParen) {
            print(")", indent, "plain");
        }
        if (curPrecedence > maxPrecedence) {
            print(")", indent, "plain");
        }
    }

    private void decompileDeclarator(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        switch (tree.opCode()) {
            case ILLang.op_declarators: {
                Tree[] sons = tree.children();
                for (int i = 0; i < sons.length; ++i) {
                    decompileDeclarator(sons[i], indent, precedence, context);
                    if (i + 1 < sons.length) {
                        print(",", indent, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_assign:
                decompileDeclarator(tree.down(1), indent, LPREC_ASSIGN, context);
                space();
                print("=", indent, "plain");
                space();
                if (tree.down(2).opCode() == ILLang.op_expressions) {
                    print("{", indent, "plain");
                }
                decompileExpression(tree.down(2), indent, RPREC_ASSIGN, context);
                if (tree.down(2).opCode() == ILLang.op_expressions) {
                    print("}", indent, "plain");
                }
                break;
            case ILLang.op_arrayDeclarator:
                if (precedence > PREC_SUBSCRIPT) {
                    print("(", indent, "plain");
                }
                decompileDeclarator(tree.down(1), indent, PREC_SUBSCRIPT, context);
                decompileDeclarator(tree.down(2), indent, precedence, context);
                if (precedence > PREC_SUBSCRIPT) {
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_dimColons: {
                Tree[] sons = tree.children();
                for (Tree son : sons) {
                    decompileDeclarator(son, indent, precedence, context);
                }
                break;
            }
            case ILLang.op_dimColon:
                if (tree.down(2).opCode() == ILLang.op_none) {
                    print("[]", indent, "plain");
                } else {
                    print("[", indent, "plain");
                    if (tree.down(1).opCode() != ILLang.op_none && !ILUtils.evalsToZero(tree.down(1))) {
                        // prototype C de procedure Fortran:
                        if (tree.down(2).opCode() != ILLang.op_star) {
                            decompileExpression(ILUtils.subTree(ILUtils.copy(tree.down(2)), ILUtils.copy(tree.down(1))),
                                    indent, 0, context);
                        }
                    } else {
                        // dimColon(0, N)  -> [N+1]
                        decompileExpression(ILUtils.addTree(ILUtils.copy(tree.down(2)), ILUtils.build(ILLang.op_intCst, 1)),
                                indent, 0, context);
                    }
                    print("]", indent, "plain");
                }
                break;
            case ILLang.op_ident:
                printAtomValue(tree, indent, "plain");
                break;
            case ILLang.op_pointerDeclarator:
                if (precedence > PREC_PTRDEREF) {
                    print("(", indent, "plain");
                }
                print("*", indent, "keyword");
                decompileDeclarator(tree.down(1), indent, PREC_PTRDEREF,
                        context);
                if (precedence > PREC_PTRDEREF) {
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_modifiedDeclarator:
                decompileModifiers(tree.down(1), indent, context);
                space();
                decompileDeclarator(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_bitfieldDeclarator:
                decompileDeclarator(tree.down(1), indent, precedence, context);
                print(":", indent, "plain");
                decompileExpression(tree.down(2), indent, precedence, context);
                break;
            case ILLang.op_functionDeclarator:
                if (precedence > PREC_FCALL) {
                    print("(", indent, "plain");
                }
                decompileDeclarator(tree.down(1), indent, PREC_FCALL, context);
                if (tree.down(2).opCode() == ILLang.op_none) {
                    print("()", indent, "plain");
                } else {
                    print("(", indent + INDENT_STEP, "plain");
                    decompileDeclarator(tree.down(2), indent + INDENT_STEP, 0, context);
                    print(")", indent + INDENT_STEP, "plain");
                }
                if (precedence > PREC_FCALL) {
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_referenceDeclarator:
                print("&", indent, "plain");
                decompileDeclarator(tree.down(1), indent, precedence, context);
                break ;
            case ILLang.op_argDeclarations:
                Tree[] sons = tree.children();
                if (sons.length == 0) {
                    print("void", indent, "plain");
                } else {
                    for (int i = 0; i < sons.length; ++i) {
                        if (isTypeSpec(sons[i])) {
                            decompileTypeSpec(sons[i], indent, precedence, context);
                        } else {
                            decompileDeclarator(sons[i], indent, precedence, context);
                        }
                        if (i < sons.length - 1) {
                            print(",", indent, "plain");
                            space();
                        }
                    }
                }
                break;
            case ILLang.op_varDeclaration:
                decompile(tree, indent, precedence, contextPlus(context, INPARAMSLIS));
                break;
            case ILLang.op_variableArgList:
                print("...", indent, "plain");
                break;

            case ILLang.op_none:
                break;
            default:
                TapEnv.toolWarning(-1, "(C decompiler) [SimpleDeclarator] Unexpected operator " + tree.opName());
                break;
        }
        postDecompile(tree, indent);
    }

    private void decompileLoop(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        boolean braces;
        Tree control = tree.down(3);
        Tree body = tree.down(4);
        Tree label = tree.down(2);
        context = contextMinus(context, INBRACES_CTX);
        switch (control.opCode()) {
            case ILLang.op_while:
                print("while", indent, "keyword");
                print("(", indent, "plain");
                decompileExpression(control.down(1), indent, precedence, context);
                print(")", indent, "plain");
                braces = hasDeclsOrIsLongOrIsEmpty(body, true) || label.opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(body, indent + INDENT_STEP, precedence, context);
                if (label.opCode() != ILLang.op_none) {
                    newLine(indent);
                    decompileExpression(label, indent - INDENT_STEP / 2, 0, context);
                    print(": ;", indent, "plain");
                    newLine(indent);
                }
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                break;
            case ILLang.op_until: {/* do { } while (); loop */
                print("do", indent, "keyword");
                braces = hasDeclsOrIsLongOrIsEmpty(body, true) || label.opCode() != ILLang.op_none;
                int context2 = context;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context2 = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(body, indent + INDENT_STEP, precedence, context2);
                if (label.opCode() != ILLang.op_none) {
                    newLine(indent);
                    decompileExpression(label, indent - INDENT_STEP / 2, 0, context);
                    print(": ;", indent, "plain");
                }
                newLine(indent);
                if (braces) {
                    print("}", indent, "plain");
                    space();
                }
                print("while", indent, "keyword");
                print("(", indent, "plain");
                decompileExpression(control.down(1), indent, precedence, context);
                print(");", indent, "plain");
            }
            break;
            case ILLang.op_for: {
                boolean multiForBounds =
                        control.down(1) != null && control.down(1).opCode() == ILLang.op_blockStatement
                                || control.down(2) != null && control.down(2).opCode() == ILLang.op_blockStatement
                                || control.down(3) != null && control.down(3).opCode() == ILLang.op_blockStatement;
                print("for", indent, "keyword");
                space();
                print("(", indent + 3, "plain");
                if (control.down(1).opCode() == ILLang.op_varDeclaration) {
                    decompile(control.down(1), indent + 5, precedence, contextPlus(context, INPARAMSLIS));
                } else {
                    decompileExpression(control.down(1), indent + 5, precedence, context);
                }
                print(";", indent + 5, "plain");
                space();
                if (multiForBounds) {
                    newLine(indent + 5);
                }
                if (control.down(2).opCode() == ILLang.op_varDeclaration) {
                    decompile(control.down(2), indent + 5, precedence, contextPlus(context, INPARAMSLIS));
                } else {
                    decompileExpression(control.down(2), indent + 5, precedence, context);
                }
                print(";", indent + 5, "plain");
                space();
                if (multiForBounds) {
                    newLine(indent + 5);
                }
                decompileExpression(control.down(3), indent + 5, precedence, context);
                print(")", indent + 3, "plain");
                braces = hasDeclsOrIsLongOrIsEmpty(body, true) || label.opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent + 2, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(body, indent + INDENT_STEP, precedence, context);
                if (label.opCode() != ILLang.op_none) {
                    newLine(indent);
                    decompileExpression(label, indent - INDENT_STEP / 2, 0, context);
                    print(": ;", indent, "plain");
                }
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                break;
            }
            case ILLang.op_forall: {
                print("for", indent, "keyword");
                space();
                print("(", indent + 3, "plain");
                Tree[] ranges = control.down(1).children();
                for (int i = 0; i < ranges.length; ++i) {
                    decompile(ranges[i], indent + 5, precedence, context);
                    if (i < ranges.length - 1) {
                        print(",", indent + 5, "plain");
                        space();
                    }
                }
                print(")", indent + 3, "plain");
                braces = hasDeclsOrIsLongOrIsEmpty(body, true);
                if (braces) {
                    space();
                    print("{", indent + 2, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(body, indent + INDENT_STEP, precedence, context);
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                break;
            }
            case ILLang.op_do: {
                Tree stride = control.down(4);
                if (ILUtils.isNullOrNone(stride)) {
                    stride = ILUtils.build(ILLang.op_intCst, 1);
                }
                String test = "<=";
                if (!ILUtils.evalsToGTZero(stride)) {
                    if (ILUtils.evalsToLTZero(stride)) {
                        test = ">=";
                    } else {
                        TapEnv.toolWarning(-1, "(C decompiler) Can't choose loop exit test in " + ILUtils.toString(control));
                    }
                }
                Tree ctvar = control.down(1);
                Tree cttype = null;
                if (ctvar.opCode() == ILLang.op_varDeclaration) {
                    cttype = ctvar.down(2);
                    ctvar = ctvar.down(3).down(1);
                }
                Tree toTree = control.down(3);
                Tree nextctvar;
                if (ILUtils.evalsToOne(stride)) {
                    nextctvar = ILUtils.build(ILLang.op_unary,
                            ILUtils.build(ILLang.op_ident, "++prefix"),
                            ILUtils.copy(ctvar));
                    toTree = ILUtils.addTree(toTree, stride);
                    test = "<";
                } else if (ILUtils.evalsToMinusOne(stride)) {
                    nextctvar = ILUtils.build(ILLang.op_unary,
                            ILUtils.build(ILLang.op_ident, "--prefix"),
                            ILUtils.copy(ctvar));
                    toTree = ILUtils.addTree(toTree, stride);
                    test = ">";
                } else if (ILUtils.isExpressionIntConstant(stride) &&
                        ILUtils.intConstantValue(stride) < 0) {
                    nextctvar = ILUtils.build(ILLang.op_minusAssign,
                            ILUtils.copy(ctvar),
                            ILUtils.build(ILLang.op_intCst,
                                    -ILUtils.intConstantValue(stride)));
                } else {
                    nextctvar = ILUtils.build(ILLang.op_plusAssign,
                            ILUtils.copy(ctvar),
                            ILUtils.copy(stride));
                }
                int context2 = contextPlus(context, DEEPEXPR_CTX);
                print("for", indent, "keyword");
                space();
                print("(", indent, "plain");
                if (cttype != null) {
                    decompileTypeSpec(cttype, indent, precedence, context2);
                    space();
                }
                decompileExpression(ctvar, indent, precedence, context2);
                space();
                print("=", indent, "plain");
                space();
                decompileExpression(control.down(2), indent, precedence, context2);
                print(";", indent, "plain");
                space();
                decompileExpression(ctvar, indent, precedence, context2);
                space();
                print(test, indent, "plain");
                space();
                decompileExpression(toTree, indent, precedence, context2);
                print(";", indent, "plain");
                space();
                decompileExpression(nextctvar, indent, precedence, context2);
                print(")", indent, "plain");
                braces = hasDeclsOrIsLongOrIsEmpty(body, true) || label.opCode() != ILLang.op_none;
                if (braces) {
                    space();
                    print("{", indent, "plain");
                    context = contextPlus(context, INBRACES_CTX);
                }
                newLine(indent);
                decompile(body, indent + INDENT_STEP, precedence, context);
                if (label.opCode() != ILLang.op_none) {
                    newLine(indent);
                    decompileExpression(label, indent - INDENT_STEP / 2, 0, context);
                    print(": ;", indent, "plain");
                }
                if (braces) {
                    newLine(indent);
                    print("}", indent, "plain");
                }
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(C decompiler) [Loop] Unexpected control operator " + tree.opName());
                break;
        }
        postDecompile(tree, indent);
    }

    private boolean isTypeSpec(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_modifiedType:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_functionType:
            case ILLang.op_recordType:
            case ILLang.op_unionType:
            case ILLang.op_enumType:
            case ILLang.op_none:
                return true;
            default:
                break;
        }
        return false;
    }

    private boolean isAStarPointerAccess(Tree tree) {
        return tree.opCode() == ILLang.op_pointerAccess
                && (ILUtils.isNullOrNone(tree.down(2))
                || isAStarPointerAccess(tree.down(1)));
    }

    private TapList<Tree> sortModifiers(Tree[] modifiers) {
        TapList<Tree> toResult = new TapList<>(null, null);
        TapList<Tree> result = toResult;
        TapList<Tree> first = null;
        TapList<Tree> second = null;
        for (int i = modifiers.length - 1; i >= 0; --i) {
            if (modifiers[i].opCode() == ILLang.op_ident) {
                String modifierText = modifiers[i].stringValue();
                if (modifierText.equals("private")
                        || modifierText.equals("save")
                        || modifierText.equals("unsigned")) {
                    first = new TapList<>(modifiers[i], first);
                } else if (modifierText.equals("const")) {
                    second = new TapList<>(modifiers[i], second);
                } else {
                    result.placdl(modifiers[i]);
                }
            } else if (modifiers[i].opCode() != ILLang.op_none) {
                result.placdl(modifiers[i]);
            }
        }
        return TapList.append(first, TapList.append(second, toResult.tail));
    }

    private int modifiersContains(Tree tree, String m) {
        int rank = -1;
        if (tree.opCode() == ILLang.op_modifiers) {
            Tree[] sons = tree.children();
            for (int i = 0; i < sons.length; ++i) {
                Tree s = sons[i];
                if ((s.opCode() == ILLang.op_ident || s.opCode() == ILLang.op_intCst)
                        && s.stringValue().equals(m)) {
                    rank = i + 1;
                }
            }
        }
        return rank;
    }

    /**
     * Applies the following rules:<br>
     * {@code { pointerType(T), D }  -> { T, pointerDeclarator(D) }} <br>
     * {@code { arrayType(T, dimColons(...)), D } -> { T, arrayDeclarator(D, dimColons(...)) }} <br>
     * {@code { functionType(T, argDeclarations(...)), D } -> { T, functionDeclarator(D, argDeclarations(...)) }}<br>
     * {@code { modifiedTypeSpec(modifiers[...], T), D } -> { T, modifiedDeclarator(modifiers[...], D) }}
     *
     * @return an array of size 2 of two tree : the first is a type and
     * the second is a declarator built from the type tree, with the argument
     * tree decl as its inner component.
     */
    private Tree[] buildDeclaratorFromType(Tree type, Tree decl) {

        Tree newType = ILUtils.copy(type);
        Tree newDecl = ILUtils.copy(decl);
        Tree value = null;
        boolean changed = true;
        boolean assign = decl.opCode() == ILLang.op_assign;
        if (assign) {
            newDecl = ILUtils.copy(decl.down(1));
            value = ILUtils.copy(decl.down(2));
        }
        while (changed) {
            changed = false;
            assert newType != null;
            switch (newType.opCode()) {
                case ILLang.op_pointerType:
                    newDecl = ILUtils.build(ILLang.op_pointerDeclarator, newDecl);
                    newType = newType.down(1);
                    changed = true;
                    break;
                case ILLang.op_arrayType:
                    newDecl = ILUtils.build(ILLang.op_arrayDeclarator,
                            newDecl, ILUtils.copy(newType.down(2)));
                    newType = newType.down(1);
                    changed = true;
                    break;
                case ILLang.op_functionType:
                    newDecl = ILUtils.build(ILLang.op_functionDeclarator,
                            newDecl, ILUtils.copy(newType.down(2)));
                    newType = newType.down(1);
                    changed = true;
                    break;
                case ILLang.op_modifiedType:
                    if (modifiersContains(newType.down(1), "restrict") != -1
                        || modifiersContains(newType.down(1), "__restrict") != -1) {
                        changed = false;
                    } else {
                        int rankd = modifiersContains(newType.down(1), "double");
                        int rankld = modifiersContains(newType.down(1), "long double");
                        if ((rankd != -1 || rankld != -1) && newType.down(2).opCode() == ILLang.op_float) {
                            if (newType.down(1).length() == 1) {
                                newType = ILUtils.build(ILLang.op_ident, rankd == -1 ? "long double" : "double");
                            } else {
                                newType.setChild(ILUtils.build(ILLang.op_ident, rankd == -1 ? "long double" : "double"), 2);
                                newType.down(1).removeChild(rankd == -1 ? rankld : rankd);
                            }
                            changed = true;
                        } else {
                            Tree subTypeSpec = newType.down(2);
                            if (subTypeSpec.opCode() != ILLang.op_ident) {
                                Tree[] pair = buildDeclaratorFromType(subTypeSpec, newDecl);
                                newType = ILUtils.build(ILLang.op_modifiedType,
                                        ILUtils.copy(newType.down(1)),
                                        ILUtils.copy(pair[TYPE_TREE]));
                                newDecl = pair[DECL_TREE];
                                changed = newType.equalsTree(pair[TYPE_TREE]);
                            }
                        }
                    }
                    break;
                default:
                    changed = false;
                    break;
            }
        }
        //Conversely, change from { T, modifiedDeclarator("const", D) }  -> { modifiedTypeSpec("const", T), D }
        // and { T, arrayDeclarator(modifiedDeclarator("const", D), DIMS) }  -> { modifiedTypeSpec("const", T), arrayDeclarator(D, DIMS) }
        // (All these are "dirty" patches. Maybe others would be useful?)
        // just to have the "const" modifier more nicely before the type.
        if (ILUtils.isAConstModified(newDecl)) {
            newType = TypeSpec.addTypeModifiers(newType, new TapList<>(ILUtils.copy(newDecl.down(1).down(1)), null));
            newDecl = newDecl.down(2);
        } else {
            assert newDecl != null;
            if (newDecl.opCode() == ILLang.op_arrayDeclarator
                    && ILUtils.isAConstModified(newDecl.down(1))) {
                newType = TypeSpec.addTypeModifiers(newType, new TapList<>(ILUtils.copy(newDecl.down(1).down(1).down(1)), null));
                newDecl = ILUtils.build(ILLang.op_arrayDeclarator,
                        ILUtils.copy(newDecl.down(1).down(2)),
                        ILUtils.copy(newDecl.down(2)));
            }
        }
        Tree[] pair = new Tree[2];
        newType.copyAnnotations(type);
        pair[TYPE_TREE] = newType;
        if (assign) {
            newDecl = ILUtils.build(ILLang.op_assign, ILUtils.copy(newDecl), value);
        }
        newDecl.copyAnnotations(decl);
        pair[DECL_TREE] = newDecl;
        return pair;
    }

    /**
     * Prints all waiting line-comments, and resets waiting comments to null.
     */
    @Override
    protected void tryFlushComments(int indent, boolean prefix) throws IOException {
        if (posX == 0 || endOfUnit) {
            if (waitingComments != null) {
                if (endOfUnit) {
                    if (posX != 0) {
                        printer.newLine();
                        posX = 0;
                        posY++;
                    }
                }
                while (waitingComments != null) {
                    printOneCommentLine(commentChar + waitingComments.head, indent);
                    if (waitingComments.tail != null || prefix) {
                        printer.newLine();
                    }
                    posX = 0;
                    posY++;
                    waitingComments = waitingComments.tail;
                }
                endOfUnit = false;
            }
        }
    }

    /**
     * Prints all waiting block-comments, and resets waiting comments to null.
     */
    @Override
    protected void tryFlushCommentsBlock(int indent, boolean prefix) throws IOException {
        if (posX == 0 || endOfUnit) {
            if (waitingCommentsBlock != null) {
                if (endOfUnit && posX != 0) {
                    printer.newLine();
                    posX = 0;
                    posY++;
                }
                print("/*", indent, "comment");
                while (waitingCommentsBlock != null) {
                    //indent(commentIndent); a mettre quand les blancs d'indentation seront
                    // supprime's par le parser c
                    printString(waitingCommentsBlock.head);
                    if (waitingCommentsBlock.tail == null) {
                        print("*/", indent, "comment");
                    }
                    if (waitingCommentsBlock.tail != null || prefix) {
                        printer.newLine();
                    }
                    posX = 0;
                    posY++;
                    waitingCommentsBlock = waitingCommentsBlock.tail;
                }
                endOfUnit = false;
            }
        }
    }

    /**
     * Prints the name of a "thing". This name may be prefixed with a scopeAccess "::",
     * but it may not be a computed expression.
     */
    private void printThingName(Tree tree, int indent, String kind)
            throws IOException {
        if (tree.opCode() == ILLang.op_scopeAccess) {
            printThingName(tree.down(1), indent, "typename");
            tree = tree.down(2);
            print("::", indent, "keyword");
        }
        printAtomValue(tree, indent, kind);
    }

    /**
     * Prints the contents of an atomic Tree.
     */
    private void printAtomValue(Tree atomTree, int indent, String kind)
            throws IOException {
        String atomValueString = atomTree.stringValue();
        if (atomTree.isIntAtom()) {
            atomValueString = Integer.toString(atomTree.intValue());
        }
        print((atomValueString != null ? atomValueString : "Error:nullTree"), indent, kind);
    }

    private void printLabel(Tree tree, int indent) throws IOException {
        String labelStr = tree.stringValue();
        if (labelStr == null || '0' <= labelStr.charAt(0) && labelStr.charAt(0) <= '9') {
            labelStr = "label" + labelStr;
        }
        print(labelStr, indent, "label");
    }

    private void printOneCommentLine(String text, int indent) throws IOException {
        printer.space(indent);
        printer.printText(text, "comment");
    }

}
