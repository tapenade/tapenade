/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;

/**
 * Abstract class for Fortran, C decompilers.
 */
public class Decompiler {
    protected static final int EMPTY_CONTEXT = 0;
    protected final Printer printer;
    private final String messageFileName;
    private final int[] elementaryContexts;
    protected int language = 1;
    /**
     * To display Tapenade message.
     */
    protected boolean showMessages;
    protected int posX;
    protected int posY;
    protected TapList<String> waitingComments;
    protected TapList<String> waitingCommentsBlock;
    protected String commentChar = "!!";
    protected String eolContinuationChar = "";
    protected String continuationChar = "";
    protected int maxColumn = 256;
    private Tree endLineComment;
    private TapList<Tree> seenTreesStack;
    private TapList<TapList<String>> waitingMessages;
    private String hereAnchorPrefix = "";
    private String otherAnchorPrefix = "";
    private String messageAnchorPrefix = "";
    private String otherMessageAnchorPrefix = "";
    private boolean inContinuation;

    protected Decompiler(Printer printer, String msgFileName) {
        this.printer = printer;
        this.messageFileName = msgFileName;
        this.elementaryContexts = new int[32];
        for (int i = 0; i < 32; i++) {
            this.elementaryContexts[i] = 1 << i;
        }
    }

    protected void reinitialize() {
        this.posY = 0;
        this.posX = 0;
        this.waitingComments = null;
        this.waitingCommentsBlock = null;
        this.waitingMessages = null;
        this.seenTreesStack = null;
        this.endLineComment = null;
    }

    protected int contextPlus(int context, int newContextRk) {
        return context | this.elementaryContexts[newContextRk];
    }

    protected int contextMinus(int context, int newContextRk) {
        return context & ~this.elementaryContexts[newContextRk];
    }

    protected boolean hasContext(int context, int newContextRk) {
        return (context & this.elementaryContexts[newContextRk]) != 0;
    }

    public void decompileTree(Tree tree, int language, boolean inInclude, boolean dumps) {
    }

    /**
     * Process annotations of a tree before it is decompiled.
     * This function may be called several successive times for the same tree
     * depending on the call order of all the decompile functions above.
     * Only the 1st call will have an effect.
     * The calls to this function are all paired with a corresponding call
     * of the postDecompile(), using the global stack "seenTreesStack".
     *
     * @param tree   the tree to process.
     * @param indent the indentation to use if something has to be printed.
     * @throws IOException printer exception
     */
    protected void preDecompile(Tree tree, int indent) throws IOException {
        if (this.seenTreesStack == null || this.seenTreesStack.head != tree) {
            TapPair<String, String> toOtherTags = tree.getAnnotation("toOtherTags");
            TapPair<String, String> toMessageTags = tree.getAnnotation("toMessageTags");
            boolean isUnit = tree.opCode() == ILLang.op_function ||
                    tree.opCode() == ILLang.op_program;
            //[llh 23/07/2013 This solves a bug in Mascaret, but might break something in the HTML output?
            if (this.printer.hasAnchors()) {
                // Anchor mechanism: setting and opening:
                if (toOtherTags != null && toOtherTags.first != null) {
                    if (isUnit) {
                        this.hereAnchorPrefix = toOtherTags.first;
                        this.printer.defineAnchor(this.hereAnchorPrefix);
                    } else {
                        this.printer.defineAnchor(this.hereAnchorPrefix + toOtherTags.first);
                    }
                }
                if (toOtherTags != null && toOtherTags.second != null) {
                    if (isUnit) {
                        this.otherAnchorPrefix = toOtherTags.second;
                        this.printer.startAnchorRef(this.otherAnchorPrefix);
                    } else {
                        this.printer.startAnchorRef(this.otherAnchorPrefix + toOtherTags.second);
                    }
                }
                // Anchor mechanism: setting and opening for messages:
                if (toMessageTags != null && toMessageTags.first != null) {
                    if (isUnit) {
                        this.messageAnchorPrefix = toMessageTags.first;
                        this.printer.defineAnchor(this.messageAnchorPrefix);
                    } else {
                        this.printer.defineAnchor(this.messageAnchorPrefix + toMessageTags.first);
                    }
                }
                if (toMessageTags != null && toMessageTags.second != null) {
                    this.printer.setTargetNameAndZone(this.messageFileName, "msg");
                    if (isUnit) {
                        this.otherMessageAnchorPrefix = toMessageTags.second;
                        this.printer.startAnchorRef(this.otherMessageAnchorPrefix);
                    } else {
                        this.printer.startAnchorRef(this.otherMessageAnchorPrefix + toMessageTags.second);
                    }
                    print("", indent, "comment");
                    this.printer.messageRef(null, this.showMessages, this.language);
                    this.printer.resetTargetNameAndZone();
                }
                if (toMessageTags != null && toMessageTags.second != null) {
                    if (isUnit) {
                        this.printer.endAnchorRef();
                        this.otherMessageAnchorPrefix = "";
                    } else {
                        this.printer.endAnchorRef();
                    }
                }
            }
            TapList<String> msgTty = tree.getAnnotation("message");
            tree.setAnnotation("message", null);
            if (msgTty != null && this.showMessages) {
                accumulateMessages(msgTty);
            }
            // Get pre-comments to be placed into the program text:
            this.waitingComments =
                    accumulateComments(tree.getAnnotation("preComments"), this.waitingComments);
            // Get block-pre-comments to be placed into the program text:
            this.waitingCommentsBlock =
                    accumulateComments(tree.getAnnotation("preCommentsBlock"), this.waitingCommentsBlock);
            if (!this.inContinuation) {
                tryFlushMessages();
                tryFlushCommentsBlock(indent, true);
                tryFlushComments(indent, true);
            }
        }
        this.seenTreesStack = new TapList<>(tree, this.seenTreesStack);
    }

    /**
     * Process annotations of a tree after it has been decompiled.
     * This function uses stack "seenTreesStack" to make sure each tree is
     * postDecompiled only once, in principle on the last call to postDecompile().
     *
     * @param tree   the tree to process.
     * @param indent the indentation to use if something has to be printed.
     * @throws IOException printer exception
     */
    protected void postDecompile(Tree tree, int indent) throws IOException {
        this.seenTreesStack = this.seenTreesStack.tail;
        if (this.seenTreesStack == null || this.seenTreesStack.head != tree) {
            //"End-of-line" comments, e.g. PUSH/POP numbers for Tangent-on-Reverse :
            Tree newEndLineComment = tree.getAnnotation("endLineComment");
            if (newEndLineComment != null && ILUtils.isNotNoneNorEmpty(newEndLineComment)) {
                this.endLineComment = newEndLineComment;
            }
            // Get post-comments to be placed into the program text:
            this.waitingComments =
                    accumulateComments(tree.getAnnotation("postComments"), this.waitingComments);
            // Get block-post-comments to be placed into the program text:
            this.waitingCommentsBlock =
                    accumulateComments(tree.getAnnotation("postCommentsBlock"), this.waitingCommentsBlock);
            if (!this.inContinuation) {
                tryFlushCommentsBlock(indent, false);
                tryFlushComments(indent, false);
                tryFlushMessages();
            }
            if (this.printer.hasAnchors()) {
                // Anchor mechanism: closure:
                TapPair<String, String> toOtherTags = tree.getAnnotation("toOtherTags");
                boolean isUnit = tree.opCode() == ILLang.op_function ||
                        tree.opCode() == ILLang.op_program;
                if (toOtherTags != null && toOtherTags.second != null) {
                    if (isUnit) {
                        this.printer.endAnchorRef();
                        this.otherAnchorPrefix = "";
                    } else {
                        this.printer.endAnchorRef();
                    }
                }
            }
        }
    }

    /**
     * Prints all waiting line-comments, and resets waiting comments to null.
     */
    protected void tryFlushComments(int indent, boolean prefix) throws IOException {
    }

    /**
     * Prints all waiting block-comments, and resets waiting comments to null.
     */
    protected void tryFlushCommentsBlock(int indent, boolean prefix) throws IOException {
    }

    /**
     * Accumulates comments into a list of waiting comments.
     */
    private TapList<String> accumulateComments(Tree comments, TapList<String> waitingComms) {
        if (comments != null && ILUtils.isNotNoneNorEmpty(comments)) {
            TapList<String> toWaitingcomms = new TapList<>(null, waitingComms);
            TapList<String> tlWaitingcomms = toWaitingcomms;
            while (tlWaitingcomms.tail != null) {
                tlWaitingcomms = tlWaitingcomms.tail;
            }
            if (comments.opCode() == ILLang.op_comments ||
                    comments.opCode() == ILLang.op_commentsBlock) {
                Tree[] sons = comments.children();
                for (Tree son : sons) {
                    if (son != null && son.opCode() == ILLang.op_stringCst) {
                        tlWaitingcomms =
                                tlWaitingcomms.placdl(son.stringValue());
                    }
                }
            } else if (comments.opCode() == ILLang.op_stringCst) {
                tlWaitingcomms =
                        tlWaitingcomms.placdl(comments.stringValue());
            }
            return toWaitingcomms.tail;
        } else {
            return waitingComms;
        }
    }

    /**
     * Accumulates messages into the list of waiting error messages.
     */
    private void accumulateMessages(TapList<String> msgs) {
        TapList<TapList<String>> toWaitingMessages = new TapList<>(null, this.waitingMessages);
        TapList<TapList<String>> tlWaitingMessages = toWaitingMessages;
        while (tlWaitingMessages.tail != null) {
            tlWaitingMessages = tlWaitingMessages.tail;
        }
        tlWaitingMessages = tlWaitingMessages.placdl(msgs);
        this.waitingMessages = toWaitingMessages.tail;
    }

    /**
     * Prints all waiting line-messages, and resets waiting error messages to null.
     */
    private void tryFlushMessages() throws IOException {
        if (this.showMessages && this.posX == 0) {
            while (this.waitingMessages != null) {
                this.printer.messageRef(this.waitingMessages.head,
                        this.showMessages, this.language);
                this.printer.newLine();
                this.posX = 0;
                this.posY++;
                this.waitingMessages = this.waitingMessages.tail;
            }
        }
    }

    private void flushEndLineComment() throws IOException {
        if (this.endLineComment != null) {
            if (this.language == TapEnv.FORTRAN) {
                //No end-of-line comments in FORTRAN77: must go to newline first
                this.printer.newLine();
                this.posX = 0;
                this.posY++;
            } else {
                space();
            }
            this.printer.printText(this.commentChar, "comment");
            space();
            this.printer.printText(this.endLineComment.stringValue(), "comment");
            this.endLineComment = null;
        }
    }

    /**
     * prints a new line.
     */
    public void newLine(int indent) throws IOException {
        flushEndLineComment();
        if (this.inContinuation) {
            // otherwise will not compile (Warning: '&' not allowed by itself in line)
            this.printer.space(1);
            this.printer.printText(this.commentChar, "comment");
            this.printer.printText(".", "plain");
        }
        this.printer.newLine();
        this.posX = 0;
        this.posY++;
        if (!this.inContinuation) {
            tryFlushCommentsBlock(indent, true);
            tryFlushComments(indent, true);
            tryFlushMessages();
        }
    }

    /**
     * Prints one whiteSpace.
     * If there was already a space just before, prints nothing.
     */
    protected void space() throws IOException {
        boolean alreadySpace = checkForIndentAndLineTooLong(1, 0);
        if (!alreadySpace) {
            this.printer.space(1);
            this.posX++;
        }
    }

    /**
     * Prints the word "word" onto the output. If the word is too
     * long, we go to a new line or we may even CUT the word.
     *
     * @param indent current indentation level, to be used in case the word starts
     *               a new line or "continuation line".
     * @param kind   the kind of word, that will determine its HTML rendering.
     *               It can be one of {"plain", "string", "constant", "keyword",
     *               "funcname", "typename", "label", "modifier", "vardecl"}.
     */
    protected void print(String word, int indent, String kind) throws IOException {
        if (!"#include".equals(word)) {
            // Even in Fortran, a #include directive must not
            // be indented nor cut in several lines :
            checkForIndentAndLineTooLong(word.length(), indent);
            while (this.posX + word.length() > this.maxColumn) {
                this.printer.printText(word.substring(0, this.maxColumn - this.posX), kind);
                word = word.substring(this.maxColumn - this.posX);
                continuationLine(indent, true);
            }
        }
        this.printer.printText(word, kind);
        this.inContinuation = false;
        this.posX += word.length();
    }

    protected void printString(String word) throws IOException {
        this.printer.printText(word, "string");
        this.posX += word.length();
    }

    /**
     * When the first string on the current line is arriving, and we are
     * not in the middle of a cut identifier, print the required "indent".
     *
     * @return true when, after this is done, the current position
     * is preceded by white spaces. In that case, a subsequent space()
     * must do nothing.
     * When the length of the next string to print is such that
     * characters would go beyond column maxColumn,
     * we try the following options:
     * -1- start a new "continuation line"
     * -2- if we are already at the beginning of a line,
     * try to remove some indentation.
     * -3- otherwise cut the word in two, and put no whitespace
     * between the continuation character in column 6 and the
     * second half of the word.
     */
    private boolean checkForIndentAndLineTooLong(int nextStringLength, int indent) throws IOException {
        int minIndent = this.language == TapEnv.FORTRAN ? 6 : 0;
        int minStart = Math.max(this.posX, minIndent);
        boolean lengthFits = minStart + nextStringLength <= this.maxColumn;
        if (lengthFits || minIndent + nextStringLength > this.maxColumn) {
            // If either the next string fits on this line (possibly taking
            // off some indentation) or the next string doesn't fit but
            // wouldn't even fit on an entire continuation line,
            // then prepare for printing next string on the current line:
            int targetX = minIndent + indent;
            if (targetX > this.maxColumn - nextStringLength) {
                targetX = this.maxColumn - nextStringLength;
            }
            if (targetX > this.posX) {
                indent(targetX);
                return true;
            } else {
                return false;
            }
        } else {
            // else start a continuation line:
            int maxIndent = this.maxColumn - nextStringLength - minIndent;
            continuationLine(Math.min(indent, maxIndent), false);
            return true;
        }
    }

    private void indent(int indentValue) throws IOException {
        this.printer.space(indentValue - this.posX);
        this.posX = indentValue;
    }

    /**
     * inIdent is true when this continuation will be in the middle
     * of a long identifier, in which case we must insert NO white space
     * after the continuation sign on the continuation line.
     */
    private void continuationLine(int indent, boolean inIdent) throws IOException {
        int minIndent = this.language == TapEnv.FORTRAN ? 6 : 0;
        if (this.language == TapEnv.FORTRAN90 || this.language == TapEnv.FORTRAN2003) {
            this.printer.printText(this.eolContinuationChar, "label");
        }
        this.inContinuation = true;
        this.printer.newLine();
        this.posY++;
        int targetX = minIndent + indent;
        int afterContinuationX = this.continuationChar.length();
        if (minIndent > afterContinuationX) {
            this.printer.space(minIndent - afterContinuationX);
            afterContinuationX = minIndent;
        }
        this.printer.printText(this.continuationChar, "label");
        if (inIdent) {
            this.posX = afterContinuationX;
        } else if (afterContinuationX >= targetX) {
            this.posX = afterContinuationX;
        } else {
            this.printer.space(targetX - afterContinuationX);
            this.posX = targetX;
        }
    }
}
