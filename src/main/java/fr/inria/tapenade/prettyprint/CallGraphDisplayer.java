/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.utils.TapIntList;

import java.io.IOException;

/**
 * This class displays a CallGraph in HTML.
 * In fact, the writing in a HTML file is made in the CallGraphPrinter.
 * Here, the CallGraph is read Unit by Unit and the printing method of
 * CallGraphPrinter is called.
 */
public final class CallGraphDisplayer {
    private final CallGraph cg;
    private final CallGraphPrinter printer;
    private TapList<Unit> alreadyDisplayed;
    private final boolean existsGlobalDeclarations;
    private final String uniqueProgramFileName;
    private final String suffixForDiffMode;
    private String outputHtmlDirectory;
    private final String initialOutputHtmlDirectory;
    private final String callGraphTitle;
    private final String targetZone;

    /**
     * Constructor.
     *
     * @param cg                the CallGraph to display in HTML
     * @param existsGlobalDecls true if GlobalDeclarations.
     */
    public CallGraphDisplayer(CallGraph cg, String htmlFileName,
                              boolean existsGlobalDecls,
                              String uniqueProgramFileName, String outputHtmlDirectory, String suffixForDiffMode,
                              String callGraphTitle, String targetZone) throws IOException {
        super();
        this.cg = cg;
        this.printer = new CallGraphPrinter(htmlFileName);
        existsGlobalDeclarations = existsGlobalDecls;
        this.uniqueProgramFileName = uniqueProgramFileName;
        this.outputHtmlDirectory = outputHtmlDirectory;
        this.suffixForDiffMode = suffixForDiffMode;
        this.initialOutputHtmlDirectory = outputHtmlDirectory;
        this.callGraphTitle = callGraphTitle;
        this.targetZone = targetZone;
    }

    /**
     * Display a CallGraph in HTML.
     */
    public void display() throws IOException {
        if (cg != null) {
            printer.printText("<!DOCTYPE html>", null);
            printer.printText(TapEnv.bootstrapCss, null);
            printer.printText(TapEnv.tapenadeCss, null);
            printer.printText("<title>" + callGraphTitle + "</title>", null);
            printer.printText("<body>", null);
            printer.newLine();
            printer.printText("<h3>" + callGraphTitle + "</h3>", null);
            printer.newLine();
            if (existsGlobalDeclarations) {
                printer.bullet();
                String globalDeclsIncludeName =
                        TapEnv.extendStringWithSuffix("GlobalDeclarations", suffixForDiffMode);
                addItem(globalDeclsIncludeName, globalDeclsIncludeName, null);
            }
            Unit unit;
            printer.printText("FILES:<br>", null);
            printer.newLine();
            printer.openList();
            if (uniqueProgramFileName != null) {
                TapIntList outputLanguages = null;
                for (int i = 0; i < cg.nbUnits(); ++i) {
                    unit = cg.sortedUnit(i);
                    assert unit != null;
                    if (unit.isTranslationUnit()
                            && !unit.name().endsWith("iso_c_bindingf.f90")
                            && !TapIntList.contains(outputLanguages, unit.language())) {
                        outputLanguages = new TapIntList(unit.language(), outputLanguages);
                    }
                }
                while (outputLanguages != null) {
                    printer.bullet();
                    String displayFileName;
                    String uniqueFileName = TapEnv.extendStringWithSuffix(uniqueProgramFileName, suffixForDiffMode)
                            + TapEnv.outputExtension(outputLanguages.head);
                    if ("_p".equals(suffixForDiffMode)) {
                        displayFileName = uniqueProgramFileName + TapEnv.outputExtension(outputLanguages.head);
                    } else {
                        displayFileName = uniqueFileName;
                    }

                    addItem(displayFileName, uniqueFileName + ".html", null);
                    outputLanguages = outputLanguages.tail;
                }
            } else {
                for (int i = 0; i < cg.nbUnits(); ++i) {
                    unit = cg.sortedUnit(i);
                    assert unit != null;
                    if (unit.isTranslationUnit() && !unit.name().endsWith("iso_c_bindingf.f90")) {
                        displayTranslationUnit(unit);
                    }
                }
            }
            printer.closeList();
            printer.printText("UNITS:<br>", null);
            printer.newLine();
            alreadyDisplayed = null;
            printer.openList();
            for (int i = 0; i < cg.nbUnits(); ++i) {
                unit = cg.sortedUnit(i);
                if (!unit.isExternal() && !(unit.isIntrinsic() && !unit.hasSource())
                    // Avoid repetition:
                    && !TapList.contains(alreadyDisplayed, unit)) {
                    displayUnit(unit, null);
                }
            }
            printer.closeList();
        } else {
            printer.printText(
                    "<link type=\"text/CSS\" rel=\"stylesheet\" href=\"tapenade.css\">", null);
            printer.printText("<center><i>" + callGraphTitle +
                    " is empty!</i></center>", null);
        }
        printer.printText("</body>", null);
        alreadyDisplayed = null;
        printer.newLine();
        printer.closeFile();
    }

    /**
     * Display a Unit.
     * Moreover this method calls itself on the Unit called by the Unit given in parameter.
     *
     * @param unit the unit to be displayed.
     */
    private void displayUnit(Unit unit, CallArrow callArrow) throws IOException {
        boolean oneFile = uniqueProgramFileName != null;
        if (!unit.name().isEmpty()
                // Don't show interface definitions!
                && !unit.isInterface()
                && !unit.isTranslationUnit()
                // Don't show "system" routines
                && !unit.isIntrinsic()
                && !MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language())
                && !unit.inStdCIncludeFile()
                && !unit.name().startsWith("iso_c_binding")
                && (unit.isTopInFile() || !unit.upperLevelUnit().name().startsWith("iso_c_binding"))) {
            String upperLevelUnitName;
            String unitSuffix;
            Unit upperLevelUnit = unit;
            while (!upperLevelUnit.isTopInFile()) {
                upperLevelUnit = upperLevelUnit.upperLevelUnit();
            }
            while (upperLevelUnit.enclosingUnitOfInterface != null) {
                upperLevelUnit = upperLevelUnit.enclosingUnitOfInterface;
            }
            upperLevelUnitName = upperLevelUnit.name();
            if (!oneFile) {
                if (upperLevelUnit.translationUnitSymbolTable() != null) {
                    upperLevelUnitName = TapEnv.stripPath(upperLevelUnit.translationUnitSymbolTable().containerFileName);
                }
                String suffix = "";
                if (cg == TapEnv.get().origCallGraph()) {
                    suffix = TapEnv.get().preprocessFileSuffix;
                }
                unitSuffix = suffix;
                upperLevelUnitName = upperLevelUnitName + unitSuffix;
            }

            String htmlTargetFileName = null;
            if (!unit.isExternal() && !unit.isUndefined() && !unit.isVarFunction()) {
                if (uniqueProgramFileName != null) {
                    htmlTargetFileName = uniqueProgramFileName;
                } else {
                    Unit fileUnit = unit;
                    while (fileUnit != null && !fileUnit.isTranslationUnit()) {
                        fileUnit = fileUnit.upperLevelUnit();
                    }
                    assert fileUnit != null;
                    htmlTargetFileName = TapEnv.stripLanguageExtension(TapEnv.stripPath(fileUnit.name()));
                }
                htmlTargetFileName = TapEnv.extendStringWithSuffix(htmlTargetFileName, suffixForDiffMode)
                        + TapEnv.outputExtension(unit.language()) + ".html";
            }

            printer.bullet();
            if (callArrow != null) {
                if (callArrow.isImport()) {
                    printer.printText("Uses ", null);
                } else if (callArrow.isCall()) {
                    printer.printText("Calls ", null);
                } else if (callArrow.isContain()) {
                    printer.printText("Contains ", null);
                }
            }

            if (unit.isModule()) {
                printer.printText("Module ", null);
            } else if (unit.isIntrinsic()) {
                printer.printText("Intrinsic ", null);
            } else if (unit.isExternal()) {
                printer.printText("External ", null);
            }
            if (unit.isIntrinsic() || unit.isExternal() || unit.isStandard()) {
                if (unit.isAFunction()) {
                    printer.printText("function ", null);
                } else if (unit.isFortran()) {
                    printer.printText("subroutine ", null);
                } else {
                    printer.printText("procedure ", null);
                }
            }

            String unitName = unit.name();
            String displayedUnitName = unit.name();
            if (unit.isFortran()) {
                displayedUnitName = displayedUnitName.toUpperCase();
            }
            changeLanguageSuffix(unit.language());

            if (callArrow != null && callArrow.srcCallName() != null) {
                printer.printText(callArrow.srcCallName() + " i.e. ", null);
            }

            if (!TapList.contains(alreadyDisplayed, unit)) {
                alreadyDisplayed = new TapList<>(unit, alreadyDisplayed);
                addItem(displayedUnitName, htmlTargetFileName, unitName);
                TapList<CallArrow> callArrows;
                callArrows = TapList.reverse(unit.callees());
                boolean listOpen = false;
                if (callArrows != null) {
                    printer.openList();
                    listOpen = true;
                }
                while (callArrows != null) {
                    Unit calledUnit = callArrows.head.destination;
                    if (!calledUnit.isInterface() && !calledUnit.isIntrinsic()
                            && calledUnit.rank() != -1) {
                        displayUnit(calledUnit, callArrows.head);
                    }
                    callArrows = callArrows.tail;
                }
                if (listOpen) {
                    printer.closeList();
                }
            } else if (unit.callees() == null) {
                addItem(displayedUnitName, htmlTargetFileName, unitName);
            } else {
                addItem(displayedUnitName + " ...", htmlTargetFileName, unitName);
            }
        }
    }

    private void changeLanguageSuffix(int lang) {
        if (outputHtmlDirectory != null && outputHtmlDirectory.endsWith(".html")) {
            String rootOutputHtmlDirectory = initialOutputHtmlDirectory.substring(0, initialOutputHtmlDirectory.length() - 5);
            outputHtmlDirectory = rootOutputHtmlDirectory + TapEnv.outputExtension(lang) + ".html";
        }
    }

    private void displayTranslationUnit(Unit unit) throws IOException {
        String fileName = TapEnv.stripLanguageExtension(TapEnv.stripPath(unit.name()));
        String displayFileName;
        if ("_p".equals(suffixForDiffMode)) {
            displayFileName = fileName + TapEnv.outputExtension(unit.language());
        } else {
            displayFileName = TapEnv.extendStringWithSuffix(fileName, suffixForDiffMode)
                    + TapEnv.outputExtension(unit.language());
        }
        String htmlTargetFileName = TapEnv.extendStringWithSuffix(fileName, suffixForDiffMode)
                + TapEnv.outputExtension(unit.language()) + ".html";
        printer.bullet();
        addItem(displayFileName, htmlTargetFileName, null);
    }

    /**
     * Write a Unit name into the HTML file.
     *
     * @param displayName    the name to be displayed for this Unit.
     * @param targetFileName if non-null, the name of the HTML file (without path) that this name must link to.
     * @param targetAnchor   if non-null, the name of some target anchor in that target HTML file.
     */
    private void addItem(String displayName, String targetFileName, String targetAnchor) throws IOException {
        if (targetFileName != null) {
            String targetFullFileName = (outputHtmlDirectory == null ? "" : outputHtmlDirectory) + targetFileName;
            printer.printText("<a href=\"" + targetFullFileName + (targetAnchor == null ? "" : "#" + targetAnchor)
                    + "\" target=\"" + targetZone + "\">", null);
            printer.printText("<code class=\"funcname\">" + displayName + "</code></a> <br>", null);
            printer.newLine();
        } else {
            printer.printText("<code class=\"external\">" + displayName + "</code> <br>", null);
            printer.newLine();
        }
    }
}
