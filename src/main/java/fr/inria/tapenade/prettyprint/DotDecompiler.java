/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;
import java.util.HashMap;

public final class DotDecompiler extends Decompiler {
    private final HashMap<Tree, Integer> map;
    private int lastID;

    /**
     * We assume all atom values are given in LOWERCASE
     */
    public DotDecompiler(Printer printer, String htmlMsgName) {
        super(printer, htmlMsgName);

        map = new HashMap<>();
        lastID = 0;
    }

    private void decompile(Tree tree) {
        if (tree != null) {
            try {
                print("digraph IL {", "init");
                newLine();
                decompileSons(tree);
                print("}", "init");
                newLine();
            } catch (IOException e) {
                TapEnv.systemError("(Decompiler) I-O error " + e.getMessage() + " while writing output file");
            }
        } else {
            TapEnv.toolWarning(-1, "(Decompiler) empty tree!");
        }
    }

    /**
     *
     */
    private void decompileSons(Tree tree) throws IOException {
        printNode(tree);
        Tree[] sons = tree.children();
        if (sons == null) {
            return;
        }
        for (Tree son : sons) {
            decompile(son);
            printEdge(tree, son);
        }
    }

    private void print(String word, String kind) throws IOException {
        printer.printText(word, kind);
    }

    private void printNode(Tree t) throws IOException {
        int id = getNodeId(t);
        String str = t.opName();
        if (t.isStringAtom()) {
            str += ":" + t.stringValue();
        } else if (t.isIntAtom()) {
            str += ":" + t.intValue();
        }
        print("  " + id + " [label=\"" + str + "\"];", "node");
        newLine();
    }

    private void printEdge(Tree t1, Tree t2) throws IOException {
        print(" " + getNodeId(t1) + " -> " + getNodeId(t2), "edge");
        newLine();
    }

    private void newLine() throws IOException {
        printer.newLine();
    }

    private synchronized int getNodeId(Tree n) {
        if (map.get(n) == null) {
            map.put(n, ++lastID);
        }
        return map.get(n);
    }
}
