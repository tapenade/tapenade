/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;

/**
 * Decompiler for Julia.
 * <p>
 * KEY PRINCIPLE ABOUT WHITE SPACES AND NEWLINES:<br>
 * Decompilation of any sub-tree takes care ONLY of printing from the first
 * non-whitespace-non-newline character to the last non-whitespace-non-newline character of this sub-tree.
 * Therefore, writing the whitespaces and newlines immediately surrounding decompilation of the sub-tree
 * is the task of the parent tree of sub-tree (or parent of parent, etc).
 * Exceptions may exist, but they must remain rare and motivated.
 */
public final class JuliaDecompiler extends Decompiler {
    private static final int INDENT_STEP = 2;

    private static final int RESULTEXPECTED_CTX = 7;
    private static final int DEEPEXPR_CTX = 8;
    
    private static final int INBODY_CONTEXT = 1;
    /* All assign operators */
    private static final int PREC_ASSIGN = 3;
    private static final int LPREC_ASSIGN = PREC_ASSIGN;
    private static final int RPREC_ASSIGN = PREC_ASSIGN + 1;
    private static final int PREC_MINUS = 27;
    private static final int PREC_PLUS = 27;
    private static final int PREC_MUL = 25;
    private static final int LPREC_MUL = PREC_MUL;
    private static final int RPREC_MUL = PREC_MUL + 1;
    private static final int PREC_DIV = 25;
    private static final int LPREC_DIV = PREC_DIV;
    private static final int RPREC_DIV = PREC_DIV + 1;
    private static final int PREC_ADD = 23;
    private static final int LPREC_ADD = PREC_ADD;
    private static final int RPREC_ADD = PREC_ADD + 1;
    private static final int PREC_SUB = 23;
    private static final int LPREC_SUB = PREC_SUB;
    private static final int RPREC_SUB = PREC_SUB + 1;
    private static final int PREC_POW = 29; 
    private static final int LPREC_POW = PREC_POW;
    private static final int RPREC_POW = PREC_POW + 1;
    
    
    

    private boolean endOfUnit;

    /** The size parameters passed to an op_parallelSpread, e.g. a Cuda call. */
    private Tree parallelSpreadSizes = null ;

    public JuliaDecompiler(Printer printer, String htmlMsgFileName) {
        super(printer, htmlMsgFileName);
    }

    @Override
    public void decompileTree(Tree tree, int lang, boolean inInclude, boolean showMsgs) {
        if (tree != null) {
            try {
                showMessages = showMsgs;
                language = lang;
                maxColumn = 999;
                commentChar = "#";
                reinitialize();
                decompile(tree, 0, 0, EMPTY_CONTEXT);
            } catch (IOException e) {
                TapEnv.systemError("(Julia decompiler) I-O error " + e.getMessage() + " while writing output file " + printer.toFile());
            }
        } else {
            TapEnv.toolWarning(-1, "(Julia decompiler) empty tree");
        }
    }

    private void decompile(Tree tree, int indent, int precedence, int context)
            throws IOException {
        // preDecompile(tree, indent);
        boolean braces;
        System.out.println("Current tree is " + tree);
        switch (tree.opCode()) {
            case ILLang.op_blockStatement: {
                Tree[] children = tree.children();
                for( int i = 0; i < children.length; ++i) {
                    if (i > 0) {
                        newLine(indent);
                    }
                    decompile(children[i], indent, precedence, context);
                }
                break;
            }
            case ILLang.op_varDeclaration: {
                decompile(tree.down(3), indent, precedence, context);
                if(!ILUtils.isNullOrNone(tree.down(2))) {
                    print("::", indent, "plain");
                    decompile(tree.down(2), indent, precedence, context);
                }
                break;
            }
            case ILLang.op_integer: {
                print("Int", indent, "keyword");
                break;
            }
            case ILLang.op_float: {
                print("Float", indent, "keyword");
                break;
            }
            case ILLang.op_declarators: {
                Tree[] children = tree.children();
                for( int i = 0; i < children.length; ++i) {
                    if (i > 0) {
                        print(", ", indent, "plain");
                    }
                    decompile(children[i], indent, precedence, context);
                }
                break;
            }
            case ILLang.op_ident: 
                printAtomValue(tree, indent, "plain");
                break;
            case ILLang.op_assign: {
                Tree annot = tree.getAnnotation("sourcetree");
                if (annot != null) {
                    decompile(annot, indent, precedence, context);
                } else {
                    decompile(tree.down(1), indent, LPREC_ASSIGN, context);
                    space();
                    print("=", indent, "plain");
                    space();
                    decompile(tree.down(2), indent + INDENT_STEP, RPREC_ASSIGN, context);
                }
                break;
            }
            case ILLang.op_intCst: 
            case ILLang.op_bitCst:
            case ILLang.op_realCst:
                printAtomValue(tree, indent, "constant");
                break;
            case ILLang.op_add:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "+", PREC_ADD, LPREC_ADD, RPREC_ADD);
                break;
            case ILLang.op_sub:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "-", PREC_SUB, LPREC_SUB, RPREC_SUB);
                break;
            case ILLang.op_mul:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "*", PREC_MUL, LPREC_MUL, RPREC_MUL);
                break;
            case ILLang.op_div:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "/", PREC_DIV, LPREC_DIV, RPREC_DIV);
                break;
            case ILLang.op_power:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        "^", PREC_POW, LPREC_POW, RPREC_POW);
                break;
            case ILLang.op_minus:
                if (tree.down(1).opCode() == ILLang.op_minus) {
                    decompile(tree.down(1).down(1), indent, precedence, context);
                } else {
                    decompileUnaryExpr(tree.down(1), indent, precedence, context,
                            "-", 7, 10);
                }
                break;

            case ILLang.op_plusAssign:
                decompileBinaryExpr(tree.down(1), tree.down(2),
                        indent, precedence, context, tree.opCode(),
                        " += ", PREC_POW, LPREC_POW, RPREC_POW);
                break;
                
            default:
                TapEnv.toolWarning(-1, "(Julia decompiler) [Expression] Unexpected operator " + tree.opName() + " in " + tree);
                break;
            // case ILLang.op_file: // op_file obsolescent...
            // case ILLang.op_blockStatement: {
            //     braces =
            //             !hasContext(context, INBRACES_CTX) && hasDeclsOrIsLongOrIsEmpty(tree, false);
            //     int indent2 = indent;
            //     if (braces) {
            //         print("{", indent, "plain");
            //         context = contextPlus(context, INBRACES_CTX);
            //         newLine(indent);
            //         indent2 = indent + INDENT_STEP / 2;
            //     }
            //     Tree[] elements = tree.children();
            //     if (elements.length > 1) {
            //         context = contextMinus(context, INBRACES_CTX);
            //     }
            //     for (int i = 0; i < elements.length; ++i) {
            //         decompile(elements[i], indent2, precedence, context);
            //         if (i + 1 < elements.length) {
            //             if (!(elements[i + 1].opCode() == ILLang.op_function && isAFunctionPrototype(elements[i + 1]))
            //                 //op_continue, op_none, print nothing, therefore there is no need for a newline:
            //                 && elements[i+1].opCode()!=ILLang.op_continue
            //                 && elements[i+1].opCode()!=ILLang.op_none) {
            //                 newLine(indent2);
            //             }
            //             if (elements[i].opCode() == ILLang.op_function
            //                     || elements[i].opCode() == ILLang.op_program
            //                     || elements[i].opCode() == ILLang.op_class
            //                     || elements[i].opCode() == ILLang.op_nameSpace
            //                     || elements[i + 1].opCode() == ILLang.op_function
            //                     || elements[i + 1].opCode() == ILLang.op_program
            //                     || elements[i + 1].opCode() == ILLang.op_class
            //                     || elements[i + 1].opCode() == ILLang.op_nameSpace) {
            //                 newLine(indent2);
            //             }
            //         } else {
            //             endOfUnit = true;
            //         }
            //     }
            //     if (braces) {
            //         newLine(indent);
            //         print("}", indent, "plain");
            //     }
            //     break;
            }
            
        // postDecompile(tree, indent);
    }

    /**
     * Prints all waiting line-comments, and resets waiting comments to null.
     */
    @Override
    protected void tryFlushComments(int indent, boolean prefix) throws IOException {
        if (posX == 0 || endOfUnit) {
            if (waitingComments != null) {
                if (endOfUnit) {
                    if (posX != 0) {
                        printer.newLine();
                        posX = 0;
                        posY++;
                    }
                }
                while (waitingComments != null) {
                    printOneCommentLine(commentChar + waitingComments.head, indent);
                    if (waitingComments.tail != null || prefix) {
                        printer.newLine();
                    }
                    posX = 0;
                    posY++;
                    waitingComments = waitingComments.tail;
                }
                endOfUnit = false;
            }
        }
    }

    /**
     * Prints all waiting block-comments, and resets waiting comments to null.
     */
    @Override
    protected void tryFlushCommentsBlock(int indent, boolean prefix) throws IOException {
        if (posX == 0 || endOfUnit) {
            if (waitingCommentsBlock != null) {
                if (endOfUnit && posX != 0) {
                    printer.newLine();
                    posX = 0;
                    posY++;
                }
                print("/*", indent, "comment");
                while (waitingCommentsBlock != null) {
                    //indent(commentIndent); a mettre quand les blancs d'indentation seront
                    // supprime's par le parser c
                    printString(waitingCommentsBlock.head);
                    if (waitingCommentsBlock.tail == null) {
                        print("*/", indent, "comment");
                    }
                    if (waitingCommentsBlock.tail != null || prefix) {
                        printer.newLine();
                    }
                    posX = 0;
                    posY++;
                    waitingCommentsBlock = waitingCommentsBlock.tail;
                }
                endOfUnit = false;
            }
        }
    }

    /**
     * Prints the contents of an atomic Tree.
     */
    private void printAtomValue(Tree atomTree, int indent, String kind)
            throws IOException {
        String atomValueString = atomTree.stringValue();
        if (atomTree.isIntAtom()) {
            atomValueString = Integer.toString(atomTree.intValue());
        }
        print((atomValueString != null ? atomValueString : "Error:nullTree"), indent, kind);
    }

    private void printOneCommentLine(String text, int indent) throws IOException {
        printer.space(indent);
        printer.printText(text, "comment");
    }

    private void decompileExpression(Tree tree, int indent, int precedence, int context)
            throws IOException {
        System.out.println("About to decompile tree " + tree);
    }

    private void decompileBinaryExpr(Tree leftTree, Tree rightTree, int indent,
                                     int curPrecedence, int context, int operator, String op,
                                     int maxPrecedence, int leftPrecedence, int rightPrecedence) throws IOException {
        
        if (curPrecedence >= PREC_MUL || maxPrecedence >= PREC_MUL) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        if (curPrecedence > maxPrecedence) {
            print("(", indent, "plain");
        }
        decompile(leftTree, indent, leftPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (!hasContext(context, DEEPEXPR_CTX) &&
                leftTree.opCode() != ILLang.op_none) {
            space();
        }
        print(op, indent, "plain");
        if (!hasContext(context, DEEPEXPR_CTX) &&
                rightTree.opCode() != ILLang.op_none) {
            space();
        }
        decompile(rightTree, indent, rightPrecedence, contextPlus(context, RESULTEXPECTED_CTX));
        if (curPrecedence > maxPrecedence) {
            print(")", indent, "plain");
        }
    }

    private void decompileUnaryExpr(Tree subTree, int indent,
                                    int curPrecedence, int context, String op, int maxPrecedence,
                                    int childPrecedence) throws IOException {
        if (curPrecedence >= 8 || maxPrecedence >= 8) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        context = contextPlus(context, RESULTEXPECTED_CTX);
        // boolean inDataCtx = hasContext(context, INDATA_CTX);
        if (curPrecedence > maxPrecedence) { //} && !inDataCtx) {
            print("(", indent, "plain");
        }
        print(op, indent, "plain");
        decompile(subTree, indent, childPrecedence, context);
        if (curPrecedence > maxPrecedence) { // && !inDataCtx) {
            print(")", indent, "plain");
        }
    }

}
