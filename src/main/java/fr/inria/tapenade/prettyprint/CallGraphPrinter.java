/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import java.io.IOException;

/**
 * Class providing primitives to write a CallGraph in a HTML file.
 */
final class CallGraphPrinter extends Printer {

    /**
     * @param filename the name of the file in which the HTML code will be written.
     */
    protected CallGraphPrinter(String filename) throws IOException {
        super(filename);
    }

    /**
     * Print text in the HTML file.
     *
     * @param text the text to print.
     * @param kind (not used).
     */
    @Override
    public void printText(String text, String kind) throws IOException {
        toFile().write(text);
    }

    /**
     * Print nbSpaces white spaces in the HTML file.
     */
    @Override
    public void space(int nbSpaces) throws IOException {
        for (int i = nbSpaces; i > 0; i--) {
            toFile().write(" ");
        }
    }

    /**
     * Print a new line in the HTML file.
     */
    @Override
    public void newLine() throws IOException {
        toFile().write(System.lineSeparator());
    }

    /**
     * Write in the HTML file a markup which opens a list.
     */
    public void openList() throws IOException {
        toFile().write("<ul compact>");
        newLine();
    }

    /**
     * Write in the HTML file a markup which closes a list.
     */
    public void closeList() throws IOException {
        toFile().write("</ul>");
        newLine();
    }

    /**
     * Write a bullet in the HTML file.
     */
    public void bullet() throws IOException {
        toFile().write("<img src=\"puce.gif\" border=\"0\" width=\"16\" height=\"16\" alt=\"\">");
    }
}
