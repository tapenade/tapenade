/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.prettyprint;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.utils.*;

import java.io.IOException;

/**
 * Decompiler for Fortran77 (fixed format) and Fortran95.
 */
public final class FortranDecompiler extends Decompiler {
    private static final int LINE_LENGTH_MAX = 128;
    private static final String KWABSTRACT = "ABSTRACT";
    private static final String KWASSIGN = "ASSIGN";
    private static final String KWASSIGNMENT = "ASSIGNMENT";
    private static final String KWBLOCKDATA = "BLOCKDATA";
    private static final String KWCALL = "CALL";
//     private static final String KWCASE = "CASE";
    private static final String KWCOMMON = "COMMON";
    private static final String KWCYCLE = "CYCLE";
    private static final String KWDATA = "DATA";
    private static final String KWDIMENSION = "DIMENSION";
//     private static final String KWDEFAULT = "DEFAULT";
    private static final String KWDO = "DO";
    private static final String KWELSE = "ELSE";
    private static final String KWELSEWHERE = "ELSEWHERE";
    private static final String KWEND = "END";
    private static final String KWENDDO = "ENDDO";
    private static final String KWENDFORALL = "END FORALL";
    private static final String KWENDIF = "END IF";
//     private static final String KWENDSELECT = "END SELECT";
    private static final String KWEQUIVALENCE = "EQUIVALENCE";
//     private static final String KWEXIT = "EXIT";
    private static final String KWEXTERNAL = "EXTERNAL";
    private static final String KWFORALL = "FORALL";
    private static final String KWFORMAT = "FORMAT";
    private static final String KWFUNCTION = "FUNCTION";
    private static final String KWGOTO = "GOTO";
    private static final String KWIF = "IF";
    private static final String KWIMPLICIT = "IMPLICIT";
    private static final String KWINCLUDE = "INCLUDE";
    private static final String KWINTERFACE = "INTERFACE";
    private static final String KWINTRINSIC = "INTRINSIC";
    private static final String KWMODULE = "MODULE";
    private static final String KWNAMELIST = "NAMELIST";
    private static final String KWNONE = "NONE";
    private static final String KWONLY = "ONLY";
    private static final String KWOPERATOR = "OPERATOR";
    private static final String KWPARAMETER = "PARAMETER";
    private static final String KWPOINTER = "POINTER";
    private static final String KWPROCEDURE = "PROCEDURE";
    private static final String KWPROGRAM = "PROGRAM";
    private static final String KWRESULT = "RESULT";
    private static final String KWRETURN = "RETURN";
    private static final String KWSAVE = "SAVE";
//     private static final String KWSELECT = "SELECT";
//     private static final String KWSELECTCASE = "SELECT CASE";
    private static final String KWSTOP = "STOP";
    private static final String KWSUBROUTINE = "SUBROUTINE";
    private static final String KWTHEN = "THEN";
    private static final String KWTIMES = "TIMES";
    private static final String KWTO = "TO";
    private static final String KWUNTIL = "UNTIL";
    private static final String KWUSE = "USE";
    private static final String KWIMPORT = "IMPORT";
    private static final String KWWHERE = "WHERE";
    private static final String KWWHILE = "WHILE";
    private static final String KWNULLIFY = "NULLIFY";
    private static final String KWALLOCATE = "ALLOCATE";
    private static final String KWDEALLOCATE = "DEALLOCATE";
    private static final String KWMOD = "MOD";
    /**
     * Constants for used contexts (from 0 to 31)
     */
    private static final int INCOMMENT_CTX = 1;
    private static final int ISMODIFIER_CTX = 2;
    private static final int ISTYPENAME_CTX = 3;
    private static final int ISVARDECL_CTX = 4;
    private static final int ISFUNCNAME_CTX = 5;
    private static final int ISLABEL_CTX = 6;
    private static final int RESULTEXPECTED_CTX = 7;
    private static final int DEEPEXPR_CTX = 8;
    private static final int INELSEIF_CTX = 9;
    private static final int INELSEWHERE_CTX = 10;
    private static final int ININTERFACE_CTX = 11;
    private static final int INCHARSTAR_CTX = 12;
    private static final int INDATA_CTX = 13;
    private static final int INBINDC_CTX = 14;
    /**
     * implies no blank line between statements.
     */
    private static final int INUNITBODY_CTX = 15;
    /**
     * implies C$OMP+ on continuation lines
     */
    private static final int INOMP_CTX = 16;

    private static final int INBODY_CONTEXT = 1 << INUNITBODY_CTX;
    private static final int INOMP_CONTEXT = 1 << INOMP_CTX;

    private boolean labelWaiting;
    /**
     * We assume all atom values are given in LOWERCASE
     */
    private boolean allUC;
    private boolean allLC;
    private boolean fortranStyle9x;
    // [llh] TODO: when new Trees deal with phyla again, retrieve expressionPhylum:
    private TapIntList expressionPhylum; //ILUtils.phyla[ILUtils.ph_Expression];

    public FortranDecompiler(Printer printer, String htmlMsgFileName) {
        super(printer, htmlMsgFileName);
    }

    @Override
    public void decompileTree(Tree tree, int language, boolean inInclude, boolean dumps) {
        if (tree != null) {
            try {
                showMessages = dumps;
                this.language = language;
                maxColumn = 72;
                fortranStyle9x = this.language == TapEnv.FORTRAN90 || this.language == TapEnv.FORTRAN2003;
                if (fortranStyle9x) {
                    maxColumn = TapEnv.get().fortran90OutputLineLength;
                }
                commentChar = fortranStyle9x ? "!" : "C";
                continuationChar = fortranStyle9x ? "&" : "+";
                eolContinuationChar = fortranStyle9x ? "&" : "";
                reinitialize();
                decompile(tree, 0, 0, (inInclude ? INBODY_CONTEXT : EMPTY_CONTEXT));
            } catch (IOException e) {
                TapEnv.systemError("(Decompiler) I-O error " + e.getMessage() + " while writing output file ");
            }
        } else {
            TapEnv.toolWarning(-1, "(Fortran decompiler) null tree");
        }
    }

    /**
     * Main decompilation method: sweeps the tree and calls for printing operations.
     * <p>
     * Convention 1: all subtrees take care only of their own decompilation:
     * they do not print surrounding blanks, parentheses, or newlines.
     * EXCEPTIONS: op_blockStatement adds a trailing newLine when not INUNITBODYctx,
     * op_modifiers adds a trailing space.
     * Convention 2: the position in the line is kept in a internal variable.
     * the elementary printing operations (print, printComment,
     * printLabel, space, newLine) take care of
     * putting the necessary whitespaces, lines,... according
     * to this position and to the FORTRAN syntax, and update
     * this position.
     * Convention 3: also, the management of the anchor mechanism is kept internal.
     * as well as the output file style (HTML or text).
     *
     * @param tree       is the current Tree that must be decompiled.
     * @param indent     is the current level of indentation.
     *                   It is used before the first word of the current
     *                   line is printed, and before the first word of optional "continuation" lines.
     * @param precedence is the current level of parenthesis precedence (default 0).
     * @param context    is bit-set that summarizes the following boolean contexts:
     *                   {INCOMMENTctx, ISMODIFIERctx, ISTYPENAMEctx, ISVARDECLctx, ISFUNCNAMEctx,
     *                   ISLABELctx, RESULTEXPECTEDctx, DEEPEXPRctx, INELSEIFctx, INELSEWHEREctx,
     *                   ININTERFACEctx, INCHARSTARctx, INUNITBODYctx}
     *                   and default initial value is EMPTY_CONTEXT.
     */
    private void decompile(Tree tree, int indent, int precedence, int context)
            throws IOException {
        preDecompile(tree, indent);
        switch (tree.opCode()) {
            case ILLang.op_file: // op_file obsolescent?
            case ILLang.op_blockStatement:
            case ILLang.op_declarations: {
                Tree[] children = tree.children();
                boolean inFunctions = false;
                for (int i=0 ; i<children.length ; ++i) {
//System.out.println("NEXT SON OF LIST IS:"+children[i]+" INUNITBODY:"+hasContext(context, INUNITBODY_CTX)) ;
                    if (hasContext(context, INUNITBODY_CTX) && !inFunctions
                        && (children[i].opCode() == ILLang.op_function
                            || (children[i].opCode() == ILLang.op_varDeclaration
                                && children[i].down(2).opCode() == ILLang.op_procedureInterfaceName))) {
                        newLine(indent);
                        print("CONTAINS", (fortranStyle9x ? indent-2 : indent), "keyword");
                        newLine(indent);
                        inFunctions = true;
                        //[llh 30Mar18] would prefer UNcommented, to put air between CONTAIN'ed units:
                        context = contextMinus(context, INUNITBODY_CTX);
                    }
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1) {
                        if (children[i].opCode()!=ILLang.op_none) {
                            // op_none prints nothing, therefore there is no need for a newline.
                            newLine(indent);
                        }
                        if (!hasContext(context, INUNITBODY_CTX)) {
                            newLine(indent);
                        }
                    } else { // [FEWER NONREGRESSION] ELSE CASE ONLY FOR FEWER DIFFS IN NONREGRESSION. TODO: REMOVE AND ACCEPT AD:
                        if (tree.parent() != null && !hasContext(context, INUNITBODY_CTX)) {
                            newLine(indent);
                        }
                    }
                }
                break;
            }
            case ILLang.op_modifiers: {
                Tree[] modifiers = tree.children();
                for (Tree modifier : modifiers) {
                    if (modifier.isAtom()) {
                        printAtomValue(modifier, true, indent, "modifier");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_varDeclarations:
            case ILLang.op_labels:
            case ILLang.op_letterss: {
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1) {
                        print(", ", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_declarators:
            case ILLang.op_varDeclarators: {
                Tree[] children = tree.children();
                context = contextPlus(context, DEEPEXPR_CTX);
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1) {
                        print(", ", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_dimColons: {
                Tree[] children = tree.children();
                context = contextMinus(context, ISTYPENAME_CTX);
                context = contextPlus(context, RESULTEXPECTED_CTX);
                context = contextPlus(context, DEEPEXPR_CTX);
                // Reverse to the "Fortran" order of array indices:
                for (int i = children.length - 1; i >= 0; i--) {
                    decompile(children[i], indent, 0, context);
                    if (i > 0) {
                        print(", ", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_include: {
                int subIndent = 0 ;
                if (TapEnv.useSharpInclude()) {
                    // If user prefers to see #include in the generated Fortran file:
                    print("#include", 0, "keyword");
                } else {
                    print(KWINCLUDE, indent, "keyword");
                    subIndent = indent+4 ;
                }
                print(" '", subIndent, "plain");
                print(tree.stringValue(), subIndent, "funcname");
                print("'", subIndent, "plain");
                break;
            }
            case ILLang.op_expressions: {
                Tree[] children = tree.children();
                boolean testNone = false;
                if (tree.parent() != null) {
                    testNone = tree.parent().opCode() == ILLang.op_call;
                }
                boolean isLast;
                for (int i = 0; i < children.length; i++) {
                    // Labels in op_expressions are labels in a call: must have a "*" :
                    if (children[i].opCode() == ILLang.op_label) {
                        print("*", indent, "label");
                    }
                    isLast = testNone;
                    if (testNone) {
                        for (int j = i + 1; j < children.length; j++) {
                            isLast = isLast
                                    && children[j].opCode() == ILLang.op_none;
                        }
                    }
                    if (testNone) {
                        if (children[i].opCode() != ILLang.op_none) {
                            decompile(children[i], indent, 0, context);
                            if (!isLast && i < children.length - 1) {
                                print(", ", indent, "plain");
                            }
                        }
                    } else {
                        decompile(children[i], indent, 0, context);
                        if (i < children.length - 1
                                && children[i + 1].opCode() != ILLang.op_none) {
                            print(", ", indent, "plain");
                        }
                    }
                }
                break;
            }
            case ILLang.op_idents: {
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1
                            && children[i + 1].opCode() != ILLang.op_none) {
                        print(", ", indent, "plain");
                    }
                }
                break;
            }
            case ILLang.op_save: {
                print(KWSAVE, indent, "keyword");
                space();
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent + 4, 0, context);
                    if (i < children.length - 1) {
                        print(", ", indent + 4, "plain");
                    }
                }
                break;
            }
            case ILLang.op_intrinsic: {
                print(KWINTRINSIC, indent, "keyword");
                space();
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent + 4, 0, contextPlus(context, ISFUNCNAME_CTX));
                    if (i < children.length - 1) {
                        print(", ", indent + 4, "plain");
                    }
                }
                break;
            }
            case ILLang.op_external: {
                print(KWEXTERNAL, indent, "keyword");
                space();
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent + 4, 0, contextPlus(context, ISFUNCNAME_CTX));
                    if (i < children.length - 1) {
                        print(", ", indent + 4, "plain");
                    }
                }
                break;
            }
            case ILLang.op_varDimDeclaration: {
                if (tree.length() != 0) {
                    print(KWDIMENSION, indent, "keyword");
                    space();
                    context = contextMinus(context, ISTYPENAME_CTX);
                    Tree[] children = tree.children();
                    for (int i = 0; i < children.length; i++) {
                        decompile(children[i], indent + 4, 0, contextPlus(context, ISVARDECL_CTX));
                        if (i < children.length - 1) {
                            print(", ", indent + 4, "plain");
                        }
                    }
                }
                break;
            }
            case ILLang.op_equivalence: {
                print(KWEQUIVALENCE, indent, "keyword");
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    print(" (", indent + 4, "plain");
                    decompile(children[i], indent + 5, 0, context);
                    print(")", indent + 4, "plain");
                    if (i < children.length - 1) {
                        print(", ", indent + 4, "plain");
                    }
                }
                break;
            }
            case ILLang.op_implicit: {
                print(KWIMPLICIT, indent, "keyword");
                space();
                Tree[] children = tree.children();
                if (children.length == 0) {
                    print(KWNONE, indent, "keyword");
                } else {
                    for (int i = 0; i < children.length; i++) {
                        decompile(children[i], indent + 4, 0, context);
                        if (i < children.length - 1) {
                            print(", ", indent + 4, "plain");
                        }
                    }
                }
                break;
            }
            case ILLang.op_program:
                // modifiers: (last space is printed by recursive call)
                decompile(tree.down(1), indent, 0, contextPlus(context, ISMODIFIER_CTX));
                if (ILUtils.isNotNone(tree.down(2)) &&
                        !(tree.down(2).opCode() == ILLang.op_ident &&
                                tree.down(2).stringValue().startsWith("_main_"))) {
                    print(KWPROGRAM, indent, "keyword");
                    space();
                    printAtomValue(tree.down(2), !allLC, indent, "funcname");
                }
                newLine(indent);
                // body:
                if (fortranStyle9x) {
                    decompile(tree.down(3), indent + 2, 0, INBODY_CONTEXT);
                    decompile(tree.down(4), indent + 2, 0, INBODY_CONTEXT);
                } else {
                    decompile(tree.down(4), indent, 0, INBODY_CONTEXT);
                }
                if (!labelWaiting) {
                    newLine(indent);
                } else {
                    labelWaiting = false;
                }
                print(KWEND, indent, "keyword");
                if (fortranStyle9x) {
                    if (ILUtils.isNotNone(tree.down(2)) &&
                            !(tree.down(2).opCode() == ILLang.op_ident &&
                                    tree.down(2).stringValue().equals("_main_"))) {
                        space();
                        print(KWPROGRAM, indent, "keyword");
                        space();
                        printAtomValue(tree.down(2), !allLC, indent, "funcname");
                    }
                }
                break;
            case ILLang.op_function: {
                if (tree.down(1).opCode() != ILLang.op_accessDecl) {
                    decompile(tree.down(1), indent, 0, contextPlus(context, ISMODIFIER_CTX));
                }
                String kw;
                if (ILUtils.isNotVoid(tree.down(2))) {
                    // return type:
                    if (tree.down(2).opCode() != ILLang.op_none) {
                        decompile9xType(tree.down(2), indent, contextPlus(context, ISTYPENAME_CTX));
                        space();
                    }
                    kw = KWFUNCTION;
                } else {
                    if (hasContext(context, ININTERFACE_CTX)) {
                        if (tree.down(2).opCode() == ILLang.op_void) {
                            kw = KWSUBROUTINE;
                        } else {
                            kw = KWFUNCTION;
                        }
                    } else {
                        kw = KWSUBROUTINE;
                    }
                }
                print(kw, indent, "keyword");
                space();
                // function name:
                printAtomValue(tree.down(4), !allLC, indent, "funcname");
                // formal parameters:
                print("(", indent, "plain");
                decompile(tree.down(5), fortranStyle9x ? indent + 2 : indent,
                        0, contextPlus(context, ISVARDECL_CTX));
                print(")", indent, "plain");

                // RESULT (other_variable) :
                Tree annotExplicitReturn = tree.down(4).getAnnotation("explicitReturnVar");
                if (!ILUtils.isNullOrNone(annotExplicitReturn)) {
                    space();
                    print(KWRESULT, indent, "plain");
                    space();
                    print("(", indent, "plain");
                    printAtomValue(annotExplicitReturn, allUC, indent, "plain");
                    print(")", indent, "plain");
                }

                // BIND(c, ...) :
                Tree annotBind = tree.down(4).getAnnotation("bind");
                if (annotBind != null && annotBind.opCode() == ILLang.op_accessDecl) {
                    // bind(c) dans une interface:
                    space();
                    decompile(annotBind, indent, 0, contextPlus(context, INBINDC_CTX));
                }
                if (tree.down(1).opCode() == ILLang.op_accessDecl) {
                    space();
                    decompile(tree.down(1), indent, 0, contextPlus(context, INBINDC_CTX));
                }

                // body:
                newLine(indent);
                decompile(tree.down(6), fortranStyle9x ? indent + 2 : indent, 0, INBODY_CONTEXT);
                if (!labelWaiting) {
                    newLine(indent);
                } else {
                    labelWaiting = false;
                }
                print(KWEND, indent, "keyword");
                if (fortranStyle9x) {
                    space();
                    print(kw, indent, "keyword");
                    space();
                    printAtomValue(tree.down(4), !allLC, indent, "funcname");
                }
                break;
            }
            case ILLang.op_module:
                boolean isBlockData = false;
                String blockDataName;
                if (tree.down(1).opCode() == ILLang.op_ident) {
                    blockDataName = tree.down(1).stringValue();
                    isBlockData = blockDataName.startsWith("_blockdata_");
                    if (isBlockData) {
                        blockDataName = blockDataName.substring(11);
                        tree.down(1).setValue(blockDataName);
                    }
                }
                if (isBlockData) {
                    print(KWBLOCKDATA, indent, "keyword");
                } else {
                    print(KWMODULE, indent, "keyword");
                }
                if (ILUtils.isNotNone(tree.down(1)) &&
                        !(tree.down(1).opCode() == ILLang.op_ident &&
                                tree.down(1).stringValue().equals("_main_"))) {
                    space();
                    printAtomValue(tree.down(1), !allLC, indent, "funcname");
                }
                newLine(indent);
                decompile(tree.down(2), indent + 2, 0, INBODY_CONTEXT);
                if (!labelWaiting) {
                    newLine(indent);
                } else {
                    labelWaiting = false;
                }
                print(KWEND, indent, "keyword");
                if (!isBlockData) {
                    space();
                    print(KWMODULE, indent, "keyword");
                    if (ILUtils.isNotNone(tree.down(1)) &&
                            !(tree.down(1).opCode() == ILLang.op_ident &&
                                    tree.down(1).stringValue().equals("_main_"))) {
                        space();
                        printAtomValue(tree.down(1), !allLC, indent, "funcname");
                    }
                }
                break;
            case ILLang.op_modifiedType: {
                TapList<Tree> modifiersBefore = new TapList<>(null, null);
                TapList<Tree> typeSuffixes = new TapList<>(null, null);
                TapList<Tree> modifiersAfter = new TapList<>(null, null);
                String basisTypeName =
                        sortModifiers(tree, modifiersBefore, typeSuffixes, modifiersAfter);
                boolean hasAllocatable =
                    TapList.containsTree(modifiersAfter.tail, ILUtils.build(ILLang.op_ident, "allocatable")) ;
                modifiersBefore = modifiersBefore.tail;
                while (modifiersBefore != null) {
                    decompile(modifiersBefore.head, indent, 0, contextPlus(context, ISMODIFIER_CTX));
                    space();
                    modifiersBefore = modifiersBefore.tail;
                }
                if (basisTypeName == null) {
                    Tree typeTree = tree.down(2);
                    // When we have both allocatable and pointer, we don't print pointer:
                    if (hasAllocatable && typeTree.opCode() == ILLang.op_pointerType) {
                        typeTree = typeTree.down(1);
                    }
                    decompile(typeTree, indent, 0, contextPlus(context, ISTYPENAME_CTX));
                } else {
                    if (!allLC) {
                        basisTypeName = basisTypeName.toUpperCase();
                    }
                    print(basisTypeName, indent, "typename");
                }
                typeSuffixes = typeSuffixes.tail;
                while (typeSuffixes != null) {
                    if (language == TapEnv.FORTRAN) {
                        print("*", indent, "typename");
                    }
                    print("(", indent, "typename");
                    decompile(typeSuffixes.head, indent, 0,
                            contextPlus(contextPlus(contextMinus(context, ISTYPENAME_CTX),
                                    ISMODIFIER_CTX), RESULTEXPECTED_CTX));
                    print(")", indent, "typename");
                    typeSuffixes = typeSuffixes.tail;
                }
                //modifiersAfter.head is reserved for the op_dimColons, in order to put it first
                if (modifiersAfter.head == null) {
                    modifiersAfter = modifiersAfter.tail;
                }
                while (modifiersAfter != null) {
                  Tree modifier = modifiersAfter.head;
                  // When we have both allocatable and pointer, we don't print pointer:
                  if (!(hasAllocatable && ILUtils.isIdent(modifier, "pointer", false))) {
                    print(",", indent, "plain");
                    space();
                    boolean isIntent = false;
                    boolean isDimension = false;
                    if (modifier.opCode() == ILLang.op_ident) {
                        String modifText = ILUtils.toString(modifier).toUpperCase();
                        isIntent = modifText.equals("IN") ||
                                modifText.equals("OUT") || modifText.equals("INOUT");
                        if (isIntent) {
                            print("INTENT(", indent, "typename");
                        }
                    } else if (modifier.opCode() == ILLang.op_dimColons) {
                        isDimension = true;
                        print(KWDIMENSION + "(", indent, "typename");
                    }
                    decompile(modifier, indent, 0, context);
                    if (isIntent || isDimension) {
                        print(")", indent, "typename");
                    }
                  }
                  modifiersAfter = modifiersAfter.tail;
                }
                break;
            }
            case ILLang.op_varDeclaration:
                if (!fortranStyle9x && tree.down(2).opCode() == ILLang.op_arrayType) {
                    Tree baseType = ILUtils.baseTypeTree(tree.down(2));
                    assert baseType != null;
                    if (baseType.opCode() != ILLang.op_character) {
                        tree = ILUtils.arrayTypeToArrayDeclarator(tree);
                    }
                }
                TapList<Tree> trees = ILUtils.splitLongDeclaration(tree);
                if (trees != null) {
                    while (trees != null) {
                        decompile(trees.head, indent, 0, context);
                        if (trees.tail != null) {
                            newLine(indent);
                        }
                        trees = trees.tail;
                    }
                } else {
                    if (ILUtils.isNotNone(tree.down(2))) {
                        decompile9xType(tree.down(2), indent, context);
                        space();
                        if (fortranStyle9x || ILUtils.hasInitializations(tree.down(3))
                                || ILUtils.hasFortran90Modifiers(tree.down(2))) {
                            print("::", indent, "plain");
                            space();
                        }
                    }
                    decompile(tree.down(3), fortranStyle9x ? indent : posX - 6,
                            0, contextPlus(context, ISVARDECL_CTX));
                }
                break;
            case ILLang.op_class:
                print("TYPE", indent, "keyword");
                space();
                printAtomValue(tree.down(2), !allLC, indent, "plain");
                newLine(indent);
                decompile(tree.down(4), indent + 4, 0, contextPlus(context, INUNITBODY_CTX));
                newLine(indent);
                print(KWEND, indent, "keyword");
                space();
                print("TYPE", indent, "keyword");
                space();
                printAtomValue(tree.down(2), !allLC, indent, "plain");
                break;
            case ILLang.op_extends:
                print("EXTENDS(", indent, "keyword");
                decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                print(")", indent, "keyword");
                break ;
            case ILLang.op_typeDeclaration: {
                print("TYPE", indent, "keyword");
                Tree modifiersAfterType = getModifiersForTypeDecl(tree.down(2), null) ;
                if (modifiersAfterType!=null) {
                    print(", ", indent, "plain");
                    decompile(modifiersAfterType, indent+1, 0, context) ;
                    print(" ::", indent, "plain");
                }
                space();
                decompile(tree.down(1), indent + 1, 0, contextPlus(context, ISTYPENAME_CTX));
                newLine(indent);
                decompile(tree.down(2), indent + 4, 0, contextPlus(context, INUNITBODY_CTX));
                if (fortranStyle9x && tree.down(2).opCode() != ILLang.op_recordType) {
                    newLine(indent);
                }
                print(KWEND, indent, "keyword");
                space();
                print("TYPE", indent, "keyword");
                if (fortranStyle9x) {
                    space();
                    printAtomValue(tree.down(1), !allLC, indent, "plain");
                }
                if (tree.down(2).opCode() != ILLang.op_recordType) {
                    TapEnv.toolWarning(-1, "(Fortran decompiler) undefined type: " + tree.down(1).stringValue());
                }
                break;
            }
            case ILLang.op_recordType: {
                ToBool modifiersRemain = new ToBool(false);
                getModifiersForTypeDecl(tree, modifiersRemain) ;
                if (modifiersRemain.get()) {
                    Tree modifiers = tree.down(2);
                    decompile(modifiers, indent, 0, EMPTY_CONTEXT);
                    newLine(indent);
                }
                Tree[] fields = tree.down(3).children();
                for (Tree field : fields) {
                    decompile(field, indent, 0, INBODY_CONTEXT);
                    newLine(indent);
                }
                if (fields.length == 0) {
                    print("TYPE", indent, "keyword");
                    print("(", indent, "keyword");
                    decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                    print(")", indent, "keyword");
                }
                break;
            }
            case ILLang.op_pointerType:
                if (fortranStyle9x) {
                    decompile9xType(tree.down(1), indent, context);
                } else {
                    decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                }
                print(",", indent, "plain");
                space();
                print(KWPOINTER, indent, "modifier");
                break;
            case ILLang.op_sizedDeclarator: {
                decompile(tree.down(1), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print("*", indent, "plain");
                boolean intValue = tree.down(2).opCode() == ILLang.op_intCst;
                if (intValue) {
                    decompile(tree.down(2), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                } else {
                    print("(", indent, "plain");
                    decompile(tree.down(2), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                    print(")", indent, "plain");
                }
                break;
            }
            case ILLang.op_arrayType:
                if (fortranStyle9x) {
                    decompile9xType(tree.down(1), indent, context);
                } else {
                    decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                }
                if (tree.down(1).opCode() == ILLang.op_character) {
                    if (fortranStyle9x) {
                        print("(", indent, "plain");
                        if (tree.down(2).down(1).down(2).opCode() != ILLang.op_nameEq
                                && tree.down(2).down(1).down(2).opCode() != ILLang.op_expressions) {
                            print("len", indent, "keyword");
                            print("=", indent, "plain");
                        }
                        context = contextMinus(context, ISTYPENAME_CTX);
                        decompile(tree.down(2), indent, 0, contextPlus(context, INCHARSTAR_CTX));
                        print(")", indent, "plain");
                    } else {
                        print("*", indent, "plain");
                        if (tree.down(2).length() == 1 &&
                                tree.down(2).down(1).down(1).opCode() == ILLang.op_intCst &&
                                tree.down(2).down(1).down(1).intValue() == 1 &&
                                (tree.down(2).down(1).down(2).opCode() == ILLang.op_none
                                        || tree.down(2).down(1).down(2).opCode() == ILLang.op_star)) {
                            print("(*)", indent, "plain");
                        } else if (tree.down(2).length() == 1 &&
                                tree.down(2).down(1).down(1).opCode() == ILLang.op_intCst &&
                                tree.down(2).down(1).down(1).intValue() == 1 &&
                                tree.down(2).down(1).down(2).opCode() == ILLang.op_intCst) {
                            decompile(tree.down(2), indent, 0,
                                    contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                        } else {
                            print("(", indent, "plain");
                            decompile(tree.down(2), indent, 0,
                                    contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                            print(")", indent, "plain");
                        }
                    }
                } else {
                    // cf F77 B04: c'est faux en fortran77, on ne doit pas passer par la
                    print(",", indent, "plain");
                    space();
                    print(KWDIMENSION, indent, "plain");
                    context = contextMinus(context, ISTYPENAME_CTX);
                    print("(", indent, "plain");
                    decompile(tree.down(2), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                    print(")", indent, "plain");

                }
                break;
            case ILLang.op_arrayDeclarator:
                decompile(tree.down(1), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print("(", indent, "plain");
                decompile(tree.down(2), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print(")", indent, "plain");
                break;
            case ILLang.op_dimColon:
                if (ILUtils.isNotNone(tree.down(1)) &&
                        !ILUtils.evalsToOne(tree.down(1))) {
                    decompile(tree.down(1), indent, 0, context);
                    print(":", indent, "plain");
                }
                if (ILUtils.isNotNone(tree.down(2))) {
                    decompile(tree.down(2), indent, 0, context);
                } else {
                    if (!ILUtils.isNotNone(tree.down(1)) || ILUtils.evalsToOne(tree.down(1))) {
                        if (!hasContext(context, INCHARSTAR_CTX)
                                && (language == TapEnv.FORTRAN90 || language == TapEnv.FORTRAN2003)) {
                            print(":", indent, "plain");
                        } else {
                            print("*", indent, "plain");
                        }
                    }
                }
                break;
            case ILLang.op_labelStatement:
                if (tree.down(1).opCode() == ILLang.op_labels) {
                    // Input IL tree produced by f2il may contain an op_labels
                    printLabel(tree.down(1).down(1).stringValue());
                } else {
                    printLabel(tree.down(1).stringValue());
                }
                if (tree.down(2).opCode() == ILLang.op_none) {
                    labelWaiting = true;
                } else if (tree.down(2).opCode() == ILLang.op_break) {
                    //[llh 20160603] I'd like to understand why ? :
                    print("CONTINUE", indent, "keyword");
                } else {
                    decompile(tree.down(2), indent, 0, INBODY_CONTEXT);
                }
                break;
            case ILLang.op_if: {
                print(KWIF, indent, "keyword");
                print(" (", indent, "plain");
                // test expression:
                decompile(tree.down(1), indent + 4, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(") ", indent, "plain");
                Tree simpleExpr;
                if (ILUtils.isNullOrNoneOrEmptyList(tree.down(3)) &&
                        (simpleExpr = isSimpleExpr(tree.down(2))) != null &&
                        !hasContext(context, INELSEIF_CTX)) {
                    // generate a IF-STATEMENT:
                    decompile(simpleExpr, indent + 2, 0, EMPTY_CONTEXT);
                } else {
                    // generate a IF_CONSTRUCT:
                    print(KWTHEN, indent, "keyword");
                    newLine(indent);
                    // then case:
                    decompile(tree.down(2), indent + 2, 0, INBODY_CONTEXT);
                    // else case:
                    if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(3))) {
                        newLine(indent);
                        print(KWELSE, indent, "keyword");
                        if (isSingleMergeableIf(tree.down(3))) {
                            space();
                            decompile(tree.down(3), indent, 0, contextPlus(context, INELSEIF_CTX));
                        } else {
                            newLine(indent);
                            decompile(tree.down(3), indent + 2, 0, INBODY_CONTEXT);
                        }
                    }
                    if (!hasContext(context, INELSEIF_CTX)) {
                        if (!labelWaiting) {
                            newLine(indent);
                        } else {
                            labelWaiting = false;
                        }
                        if (!fortranStyle9x) {
                            print(KWENDIF, indent, "keyword");
                        } else {
                            print(KWEND, indent, "keyword");
                            space();
                            print(KWIF, indent, "keyword");
                        }
                    }
                }
                break;
            }
            case ILLang.op_where: {
                String optId = null;
                int optIdIndent = indent;
                if (!ILUtils.isNullOrNone(tree.down(4))) {
                    optId = tree.down(4).stringValue();
                    optIdIndent = indent - optId.length() - 1;
                    if (optIdIndent < 0) {
                        optIdIndent = 0;
                    }
                }
                if (!hasContext(context, INELSEWHERE_CTX)) {
                    if (optId != null) {
                        print(optId + ":", optIdIndent, "label");
                    }
                    print(KWWHERE, indent, "keyword");
                    space();
                }
                print("(", indent, "plain");
                // test expression:
                decompile(tree.down(1), indent + 4, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(") ", indent, "plain");
                if (optId != null && hasContext(context, INELSEWHERE_CTX)) {
                    print(optId, optIdIndent, "label");
                }
                Tree simpleExpr;
                if (ILUtils.isNullOrNoneOrEmptyList(tree.down(3)) &&
                        (simpleExpr = isSimpleExpr(tree.down(2))) != null &&
                        !hasContext(context, INELSEWHERE_CTX)) {
                    decompile(simpleExpr, indent + 2, 0, context);
                } else {
                    newLine(indent);
                    decompile(tree.down(2), indent + 2, 0, contextMinus(context, INELSEWHERE_CTX));
                    if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(3))) {
                        newLine(indent);
                        print(KWELSEWHERE, indent, "keyword");
                        if (isSingleMergeableWhere(tree.down(3))) {
                            space();
                            decompile(tree.down(3), indent, 0, contextPlus(context, INELSEWHERE_CTX));
                        } else {
                            newLine(indent);
                            decompile(tree.down(3), indent + 2, 0, contextMinus(context, INELSEWHERE_CTX));
                        }
                    }
                    if (!hasContext(context, INELSEWHERE_CTX)) {
                        if (!labelWaiting) {
                            newLine(indent);
                        } else {
                            labelWaiting = false;
                        }
                        print(KWEND, indent, "keyword");
                        space();
                        print(KWWHERE, indent, "keyword");
                        if (optId != null) {
                            space();
                            print(optId, optIdIndent, "label");
                        }
                    }
                }
                break;
            }
            case ILLang.op_switchCases: {
                Tree[] cases = tree.children();
                for (int i=0 ; i<cases.length ; ++i) {
                    decompile(cases[i], indent, 0, context);
                    if (i < cases.length-1
                        && cases[i+1].down(2) != null
                        && cases[i+1].down(2).length() != 0) {
                        newLine(indent);
                    }
                }
                break;
            }
            case ILLang.op_loop: {
                int ctrOpCode = tree.down(3).opCode();
                /*optional identifier: */
                String optId = null;
                int optIdIndent = indent;
                if (ILUtils.isNotNone(tree.down(1))) {
                    optId = tree.down(1).stringValue();
                    optIdIndent = indent - optId.length() - 1;
                    if (optIdIndent < 0) {
                        optIdIndent = 0;
                    }
                }

                if (optId != null && !ILUtils.isNotNone(tree.down(2))) {
                    print(optId + ":", optIdIndent, "label");
                }
                if (ctrOpCode == ILLang.op_forall) {
                    print(KWFORALL, indent, "keyword");
                    space();
                } else {
                    print(KWDO, indent, "keyword");
                    space();
                    /*do loop label: */
                    if (ILUtils.isNotNone(tree.down(2))) {
                        printAtomValue(tree.down(2), allUC, indent, "label");
                        space();
                    }
                }
                /*loop control: */
                decompile(tree.down(3), indent + 3, 0, EMPTY_CONTEXT);
                /*body: */
                Tree simpleExpr = isSimpleExpr(tree.down(4));
                if (ctrOpCode == ILLang.op_forall && simpleExpr != null) {
                    /*generate a FORALL-STATEMENT: */
                    space();
                    decompile(simpleExpr, indent + 2, 0, INBODY_CONTEXT);
                } else {
                    /*generate a structured statement: */
                    newLine(indent);
                    decompile(tree.down(4), indent + 2, 0, INBODY_CONTEXT);
                    if (!labelWaiting) {
                        newLine(indent);
                    } else {
                        labelWaiting = false;
                    }
                    if (ctrOpCode == ILLang.op_forall) {
                        print(KWENDFORALL, indent, "keyword");
                    } else if (ILUtils.isNotNone(tree.down(2))) {
                        printLabel(tree.down(2).stringValue());
                        print("CONTINUE", indent, "keyword");
                    } else if (!fortranStyle9x) {
                        print(KWENDDO, indent, "keyword");
                    } else {
                        print(KWEND, indent, "keyword");
                        space();
                        print(KWDO, indent, "keyword");
                    }
                    if (optId != null && !ILUtils.isNotNone(tree.down(2))) {
                        space();
                        print(optId, optIdIndent, "label");
                    }
                }
                break;
            }
            case ILLang.op_do:
                printAtomValue(tree.down(1), allUC, indent, "plain");
                context = contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX);
                print("=", indent, "plain");
                decompile(tree.down(2), indent + 1, 0, context);
                print(",", indent, "plain");
                decompile(tree.down(3), indent + 1, 0, context);
                if (ILUtils.isNotNone(tree.down(4))) {
                    print(",", indent, "plain");
                    decompile(tree.down(4), indent + 1, 0, context);
                }
                break;
            case ILLang.op_for:
                if (ILUtils.mayBeTransformed2Do(tree)) {
                    decompile(ILUtils.transformFor2Do(tree), indent, precedence, context);
                } else {
                    TapEnv.toolWarning(-1, "(Fortran decompiler) cannot rewrite this C-style \"for\" into a \"do\": " + ILUtils.toString(tree));
                }
                break;
            case ILLang.op_times:
                if (fortranStyle9x) {
                    print(KWTIMES, indent, "keyword");
                    space();
                }
                print("(", indent, "plain");
                if (fortranStyle9x) {
                    decompile(tree.down(1), indent + 7, 0, contextPlus(context, RESULTEXPECTED_CTX));
                } else {
                    decompile(tree.down(1), indent + 1, 0, contextPlus(context, RESULTEXPECTED_CTX));
                }
                print(") ", indent, "plain");
                if (!fortranStyle9x) {
                    print(KWTIMES, indent, "keyword");
                } else {
                    space();
                }
                break;
            case ILLang.op_while:
                print(KWWHILE, indent, "keyword");
                print(" (", indent, "plain");
                decompile(tree.down(1), indent + 7, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                break;
            case ILLang.op_until:
                print(KWUNTIL, indent, "keyword");
                print(" (", indent, "plain");
                decompile(tree.down(1), indent + 7, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                break;
            case ILLang.op_forall:
                print("(", indent, "plain");
                Tree[] ranges = tree.down(1).children();
                for (int i = 0; i < ranges.length; i++) {
                    decompile(ranges[i], indent, 0, context);
                    if (i < ranges.length - 1) {
                        print(", ", indent, "plain");
                    }
                }
                if (ILUtils.isNotNone(tree.down(2))) {
                    print(",", indent, "plain");
                    decompile(tree.down(2), indent + 1, 0, context);
                }
                print(") ", indent, "plain");
                break;
            case ILLang.op_forallRangeTriplet:
                printAtomValue(tree.down(1), allUC, indent, "plain");
                int context2 = contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX);
                print("=", indent, "plain");
                decompile(tree.down(2), indent + 1, 0, context2);
                print(":", indent, "plain");
                decompile(tree.down(3), indent + 1, 0, context2);
                if (ILUtils.isNotNone(tree.down(4))) {
                    print(":", indent, "plain");
                    decompile(tree.down(4), indent + 1, 0, context2);
                }
                break;
            case ILLang.op_continue:
                print("CONTINUE", indent, "keyword");
                break;
            case ILLang.op_stop:
                print(KWSTOP, indent, "keyword");
                break;
            case ILLang.op_goto:
                print(KWGOTO, indent, "keyword");
                space();
                printAtomValue(tree.down(1), allUC, indent + 1, "label");
                break;
            case ILLang.op_compGoto:
                print(KWGOTO, indent, "keyword");
                print(" (", indent, "plain");
                decompile(tree.down(1), indent + 6, 0, contextPlus(context, ISLABEL_CTX));
                print(") ", indent, "plain");
                decompile(tree.down(2), indent + 4, 0, contextPlus(context, RESULTEXPECTED_CTX));
                break;
            case ILLang.op_gotoLabelVar:
                print(KWGOTO, indent, "keyword");
                space();
                printAtomValue(tree.down(1), allUC, indent + 1, "plain");
                /*optional allowed destination labels: */
                if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(2))) {
                    print(" (", indent, "plain");
                    decompile(tree.down(2), indent + 7, 0, contextPlus(context, ISLABEL_CTX));
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_assignLabelVar:
                print(KWASSIGN, indent, "keyword");
                space();
                decompile(tree.down(1), indent + 7, 0, contextPlus(context, RESULTEXPECTED_CTX));
                space();
                print(KWTO, indent + 4, "keyword");
                space();
                printAtomValue(tree.down(2), allUC, indent + 8, "label");
                break;
            case ILLang.op_call: {
                Tree annot = tree.getAnnotation("sourcetree");
                if (annot != null) {
                    String opStr;
                    if (annot.opCode() == ILLang.op_binary) {
                        opStr = annot.down(2).stringValue();
                        if (opStr.equals("add")) {
                            opStr = "+";
                        } else if (opStr.equals("sub")) {
                            opStr = "-";
                        } else if (opStr.equals("mul")) {
                            opStr = "*";
                        } else if (opStr.equals("div")) {
                            opStr = "/";
                        } else if (opStr.equals("assign")) {
                            opStr = "=";
                        } else if (opStr.equals("ge")) {
                            opStr = ".GE.";
                        } else if (opStr.equals("gt")) {
                            opStr = ".GT.";
                        } else if (opStr.equals("le")) {
                            opStr = ".LE.";
                        } else if (opStr.equals("lt")) {
                            opStr = ".LT.";
                        } else if (allUC) {
                            opStr = opStr.toUpperCase();
                        }
                        decompileBinaryExpr(ILUtils.getArguments(tree).down(1),
                                ILUtils.getArguments(tree).down(2), indent, precedence, context,
                                opStr, 1, 1, 1);
                    } else if (annot.opCode() == ILLang.op_unary) {
                        opStr = annot.down(1).stringValue();
                        if (opStr.equals("sub")) {
                            opStr = "-";
                        } else if (allUC) {
                            opStr = opStr.toUpperCase() + " ";
                        } else {
                            opStr = opStr + " ";
                        }
                        decompileUnaryExpr(ILUtils.getArguments(tree).down(1), indent,
                                precedence, context, opStr, 1, 1);
                    } else {
                        // keyword and optional arguments
                        Tree annot2 = tree.getAnnotation("optionaltree");
                        if (annot2 != null) {
                            ILUtils.reBuildNameEqArgs(tree, annot2);
                            annot = annot2;
                        } else {
                            tree.removeAnnotation("sourcetree");
                            annot = tree;
                        }
                        decompile(annot, indent, precedence, context);
                    }
                } else {
                    if (!hasContext(context, RESULTEXPECTED_CTX)) {
                        print(KWCALL, indent, "keyword");
                        space();
                    }
                    if (!ILUtils.isNullOrNone(tree.down(1))) {
                        decompile(tree.down(1), indent, 0, context) ;
                        print("%", indent, "plain");
                    }
                    decompile(ILUtils.getCalledName(tree), indent, 0, contextPlus(context, ISFUNCNAME_CTX));
                    if (!hasContext(context, RESULTEXPECTED_CTX)) {
                        indent = posX - 6;
                    }
                    print("(", indent, "plain");
                    // arguments list:
                    decompile(ILUtils.getArguments(tree),
                            hasContext(context, RESULTEXPECTED_CTX) ? indent : indent + 1,
                            0, contextPlus(context, RESULTEXPECTED_CTX));
                    print(")", indent, "plain");
                }
                break;
            }
            case ILLang.op_constructorCall: {
                decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                print("(", indent, "plain");
                // arguments list:
                decompile(tree.down(3), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                break;
            }
            case ILLang.op_constDeclaration: {
                if (!fortranStyle9x) {
                    Tree[] declarators;
                    Tree declarator;
                    Tree initTree;
                    if (ILUtils.isNotNoneNorVoid(tree.down(1))) {
                        decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                        space();
                        declarators = tree.down(2).children();
                        for (int i = 0; i < declarators.length; i++) {
                            declarator = declarators[i];
                            if (declarator.opCode() == ILLang.op_assign) {
                                declarator = declarator.down(1);
                            }
                            decompile(declarator, indent + 2, 0, contextPlus(context, ISVARDECL_CTX));
                            if (i < declarators.length - 1) {
                                print(", ", indent + 4, "plain");
                            }
                        }
                        newLine(indent);
                    }
                    print(KWPARAMETER, indent, "keyword");
                    print(" (", indent + 3, "plain");
                    declarators = tree.down(2).children();
                    for (int i = 0; i < declarators.length; i++) {
                        declarator = declarators[i];
                        initTree = null;
                        if (declarator.opCode() == ILLang.op_assign) {
                            initTree = declarator.down(2);
                            declarator = declarator.down(1);
                        }
                        if (declarator.opCode() == ILLang.op_arrayDeclarator ||
                                declarator.opCode() == ILLang.op_sizedDeclarator) {
                            declarator = declarator.down(1);
                        }
                        decompile(declarator, indent + 3, 0, contextPlus(context, ISVARDECL_CTX));
                        if (initTree != null) {
                            print("=", indent + 3, "plain");
                            if (initTree.opCode() == ILLang.op_expressions) {
                                print("(", indent, "plain");
                            }
                            decompile(initTree, indent + 3, 0,
                                    contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                            if (initTree.opCode() == ILLang.op_expressions) {
                                print(")", indent, "plain");
                            }
                        }
                        if (i < declarators.length - 1) {
                            print(", ", indent + 3, "plain");
                        }
                    }
                    print(")", indent + 4, "plain");
                } else {
                    if (ILUtils.isNotNone(tree.down(1))) {
                        decompile9xType(tree.down(1), indent, context);
                        space();
                        print("::", indent, "plain");
                        space();
                        decompile(tree.down(2), indent, 0, contextPlus(context, ISVARDECL_CTX));
                    } else {
                        print(KWPARAMETER, indent, "keyword");
                        print(" (", indent + 3, "plain");
                        decompile(tree.down(2), indent, 0, contextPlus(context, ISVARDECL_CTX));
                        print(")", indent + 4, "plain");
                    }
                }
                break;
            }
            case ILLang.op_return:
                print(KWRETURN, indent, "keyword");
                if (ILUtils.isNotNone(tree.down(2))) {
                    space();
                    decompile(tree.down(2), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                }
                break;
            case ILLang.op_common:
                print(KWCOMMON, indent, "keyword");
                space();
                if (ILUtils.isNotNone(tree.down(1))) {
                    printAtomValue(tree.down(1), allUC, indent, "vardecl");
                } else {
                    print("//", indent, "vardecl");
                }
                space();
                decompile(tree.down(2), indent, 0, contextPlus(context, ISVARDECL_CTX));
                break;
            case ILLang.op_ioCall: {
                printAtomValue(tree.down(1), !allLC, indent, "keyword");
                if (tree.down(1).opCode() == ILLang.op_ident &&
                        tree.down(1).stringValue().equals("pause")) {
                    space();
                }
                /*IO control: */
                indent = posX - 6;
                Tree ioSpecs = tree.down(2);
                boolean parenthesis;
                parenthesis = !(tree.down(1).opCode() == ILLang.op_ident &&
                        tree.down(1).stringValue().equals("pause"));
                if (ioSpecs.opCode() == ILLang.op_expressions &&
                        ioSpecs.length() == 2 &&
                        ioSpecs.down(1).opCode() == ILLang.op_none) {
                    ioSpecs = ioSpecs.down(2);
                }
                if (ioSpecs.opCode() == ILLang.op_expressions) {
                    Tree[] children = ioSpecs.children();
                    if (parenthesis) {
                        print("(", indent, "plain");
                    }
                    for (int i = 0; i < children.length; i++) {
                        decompileIOSpec(children[i], i + 1, indent + 1, EMPTY_CONTEXT);
                        if (i < children.length - 1) {
                            print(", ", indent + 1, "plain");
                        }
                    }
                    if (parenthesis) {
                        print(")", indent, "plain");
                    }
                } else {
                    if (ioSpecs.opCode() != ILLang.op_star) {
                        print(" ", indent, "plain");
                    }
                    decompileIOSpec(ioSpecs, 2, indent, EMPTY_CONTEXT);
                    if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(3))) {
                        print(",", indent, "plain");
                    }
                }
                space();
                /*IO expressions: */
                decompile(tree.down(3), indent + 1, 0, contextPlus(context, RESULTEXPECTED_CTX));
                break;
            }
            case ILLang.op_nameEq:
                decompileIOSpec(tree, 0, indent, context);
                break;
            case ILLang.op_arrayAccess: {
                decompile(tree.down(1), indent, 0, context);
                print("(", indent, "plain");
                // on inverse l'ordre pour revenir dans l'ordre "FORTRAN" des indices
                Tree[] expressions = tree.down(2).children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    decompile(expressions[i], indent, 0,
                            contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                    if (i > 0) {
                        print(", ", indent, "plain");
                    }
                }
                print(")", indent, "plain");
                break;
            }
            case ILLang.op_ident:
                if (hasContext(context, ISFUNCNAME_CTX) || isMarkedAsFunctionName(tree)) {
                    printAtomValue(tree, !allLC, indent, "funcname");
                } else if (hasContext(context, ISTYPENAME_CTX)) {
                    printAtomValueTypeName(tree, !allLC, indent);
                } else if (hasContext(context, ISMODIFIER_CTX)) {
                    printAtomValue(tree, allUC, indent, "modifier");
                } else if (hasContext(context, ISVARDECL_CTX)) {
                    printAtomValue(tree, allUC, indent, "vardecl");
                } else if (hasContext(context, ISLABEL_CTX)) {
                    printAtomValue(tree, allUC, indent, "label");
                } else {
                    printAtomValue(tree, allUC, indent, "plain");
                }
                break;
            case ILLang.op_metavar:
                print("METAVAR:", indent, "plain");
                printAtomValue(tree, false, indent, "label");
                break;
            case ILLang.op_strings: {
                Tree[] children = tree.children();
                print("'", indent, "string");
                for (Tree child : children) {
                    if (child.opCode() == ILLang.op_stringCst) {
                        print(quote(child.stringValue()), 0, "string");
                    } else {
                        TapEnv.toolWarning(-1, "(Fortran decompiler) unexpected operator in strings " + child.opName());
                    }
                }
                print("'", indent, "string");
                break;
            }
            case ILLang.op_stringCst:
                if (hasContext(context, INCOMMENT_CTX)) {
                    printComment(tree.stringValue());
                } else {
                    print("'" + quote(tree.stringValue()) + "'", indent,
                            "string");
                }
                break;
            case ILLang.op_intCst:
                if (hasContext(context, ISLABEL_CTX)) {
                    printAtomValue(tree, allUC, indent, "label");
                } else {
                    int localPrecedence = tree.intValue() < 0 ? 6 : 10;
                    if (precedence > localPrecedence) {
                        print("(", indent, "plain");
                    }
                    printAtomValue(tree, allUC, indent, "constant");
                    if (precedence > localPrecedence) {
                        print(")", indent, "plain");
                    }
                }
                break;
            case ILLang.op_boolCst:
                printString("." + tree.stringValue() + ".", allUC, indent, "constant");
                break;
            case ILLang.op_bitCst:
                printString(tree.stringValue(), allUC, indent,
                        "constant");
                break;
            case ILLang.op_realCst: {
                int localPrecedence = tree.stringValue().charAt(0) == '-'
                        ? 6 : 10;
                if (precedence > localPrecedence) {
                    print("(", indent, "plain");
                }
                printAtomValue(tree, allUC, indent, "constant");
                if (precedence > localPrecedence) {
                    print(")", indent, "plain");
                }
                break;
            }
            case ILLang.op_formatElem:
                printAtomValue(tree, allUC, indent, "constant");
                break;
            case ILLang.op_star:
                print("*", indent, "keyword");
                break;
            case ILLang.op_assign:
                if (precedence > 1) {
                    print("(", indent, "plain");
                }
                // For weird cases (XAIF) with a call on the LHS, make it a function call:
                decompile(tree.down(1), indent, 2, contextPlus(context, RESULTEXPECTED_CTX));
                print(hasContext(context, DEEPEXPR_CTX) ? "=" : " = ", indent, "plain");
                if (tree.down(2).opCode() == ILLang.op_allocate) {
                    print("ALLOCATION_FORBIDDEN_HERE", indent, "plain");
                } else {
                    if (tree.down(2).opCode() == ILLang.op_expressions) {
                        print("(", indent + 1, "plain");
                    }
                    if (hasContext(context, ISVARDECL_CTX)) {
                        context = contextPlus(context, DEEPEXPR_CTX);
                    }
                    decompile(tree.down(2), indent + 2, 1, contextPlus(context, RESULTEXPECTED_CTX));
                    if (tree.down(2).opCode() == ILLang.op_expressions) {
                        print(")", indent + 1, "plain");
                    }
                }
                if (precedence > 1) {
                    print(")", indent, "plain");
                }
                break;
            case ILLang.op_pointerAssign:
                if (tree.down(2).opCode() == ILLang.op_allocate) {
                    // in Fortran, V=>op_allocate(,D,,Status) -> "ALLOCATE(V(D),Status)"
                    print(KWALLOCATE, indent, "keyword");
                    print("(", indent, "keyword");
                    context = contextPlus(contextPlus(context, DEEPEXPR_CTX), RESULTEXPECTED_CTX);
                    decompile(tree.down(1), indent, 2, context);
                    if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(2).down(2))) {
                        print("(", indent, "keyword");
                        // Be careful of putting the indices back into Fortran order:
                        decompile(ILUtils.reverseArrayComponentOrder(tree.down(2).down(2)),
                                indent, 0, context);
                        print(")", indent, "keyword");
                    }
                    if (!ILUtils.isNullOrNone(tree.down(2).down(4))) {
                        print(", ", indent, "plain");
                        decompile(tree.down(2).down(4), indent, 0, context);
                    }
                    print(")", indent, "keyword");
                } else {
                    // Else normal pointer assignment:
                    if (precedence > 1) {
                        print("(", indent, "plain");
                    }
                    decompile(tree.down(1), indent, 2, context);
                    print(" => ", 0, "plain");
                    decompile(tree.down(2), indent + 2, 1, contextPlus(context, RESULTEXPECTED_CTX));
                    if (precedence > 1) {
                        print(")", indent, "plain");
                    }
                }
                break;
            case ILLang.op_add:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "+", 7, 7, 8);
                break;
            case ILLang.op_sub:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "-", 7, 7, 8);
                break;
            case ILLang.op_mul:
            case ILLang.op_repeatedValue:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "*", 8, 8, 9);
                break;
            case ILLang.op_div:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "/", 8, 8, 9);
                break;
            case ILLang.op_power:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "**", 9, 10, 9);
                break;
            case ILLang.op_eq:
                if (tree.getAnnotation("boolean") != null) {
                    decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                            context, ".EQV.", 3, 6, 7);
                } else {
                    decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                            context, ".EQ.", 6, 6, 7);
                }
                break;
            case ILLang.op_neq:
                if (tree.getAnnotation("boolean") != null) {
                    decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                            context, ".NEQV.", 3, 6, 7);
                } else {
                    decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                            context, ".NE.", 6, 6, 7);
                }
                break;
            case ILLang.op_ge:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".GE.", 6, 6, 7);
                break;
            case ILLang.op_le:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".LE.", 6, 6, 7);
                break;
            case ILLang.op_gt:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".GT.", 6, 6, 7);
                break;
            case ILLang.op_lt:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".LT.", 6, 6, 7);
                break;
            case ILLang.op_and:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".AND.", 4, 4, 5);
                break;
            case ILLang.op_or:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".OR.", 3, 5, 5);
                break;
            case ILLang.op_xor:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, ".XOR.", 2, 5, 5);
                break;
            case ILLang.op_concat:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "//", 8, 8, 9);
                break;
            case ILLang.op_concatIdent:
                decompileBinaryExpr(tree.down(1), tree.down(2), indent, precedence,
                        context, "", 8, 8, 9);
                break;
            case ILLang.op_minus:
                if (tree.down(1).opCode() == ILLang.op_minus) {
                    decompile(tree.down(1).down(1), indent, precedence, context);
                } else {
                    decompileUnaryExpr(tree.down(1), indent, precedence, context,
                            "-", 7, 10);
                }
                break;
            case ILLang.op_not:
                /*Take away not(not(X)) ==> X */
                if (tree.down(1).opCode() == ILLang.op_not) {
                    decompile(tree.down(1).down(1), indent, precedence, context);
                } else {
                    decompileUnaryExpr(tree.down(1), indent, precedence, context,
                            ".NOT.", 4, 5);
                }
                break;
            case ILLang.op_complexConstructor:
                print("(", indent, "plain");
                decompile(tree.down(1), indent, 0, context);
                print(",", indent, "plain");
                decompile(tree.down(2), indent, 0, context);
                print(")", indent, "plain");
                break;
            case ILLang.op_multiCast: {
                print("(/", indent, "plain");
                decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                print(" :: ", indent, "plain");
                Tree[] children = tree.down(2).children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                    if (i < children.length - 1) {
                        print(", ", indent, "plain");
                    }
                }
                print("/)", indent, "plain");
                break;
            }
            case ILLang.op_arrayConstructor: {
                print("(/", indent, "plain");
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                    if (i < children.length - 1) {
                        print(", ", indent, "plain");
                    }
                }
                print("/)", indent, "plain");
                break;
            }
            case ILLang.op_arrayTriplet:
                decompile(tree.down(1), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print(":", indent, "plain");
                decompile(tree.down(2), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                if (ILUtils.isNotNone(tree.down(3))) {
                    print(":", indent, "plain");
                    decompile(tree.down(3), indent + 1, 0, context);
                }
                break;
            case ILLang.op_unary: {
                String opStr = tree.down(1).stringValue();
                opStr = opStr + " ";
                if (allUC) {
                    opStr = opStr.toUpperCase();
                }
                if (tree.down(2).opCode() == ILLang.op_none) {
                    print(KWOPERATOR, indent, "keyword");
                    print("(", indent, "plain");
                    printString(opStr, true, indent, "plain");
                    print(")", indent, "plain");
                } else {
                    decompileUnaryExpr(tree.down(2), indent, precedence, context,
                            opStr, 1, 1);
                }
                break;
            }
            case ILLang.op_binary: {
                String opStr = tree.down(2).stringValue();
                if (opStr.equals("add")) {
                    opStr = "+";
                } else if (opStr.equals("sub")) {
                    opStr = "-";
                } else if (opStr.equals("mul")) {
                    opStr = "*";
                } else if (opStr.equals("div")) {
                    opStr = "/";
                } else if (opStr.equals("assign")) {
                    opStr = "=";
                } else if (opStr.equals("ge")) {
                    opStr = ".GE.";
                } else if (opStr.equals("gt")) {
                    opStr = ".GT.";
                } else if (opStr.equals("le")) {
                    opStr = ".LE.";
                } else if (opStr.equals("lt")) {
                    opStr = ".LT.";
                } else if (allUC) {
                    opStr = opStr.toUpperCase();
                }
                if (tree.down(1).opCode() == ILLang.op_none && tree.down(3).opCode() == ILLang.op_none) {
                    print(KWOPERATOR, indent, "keyword");
                    print("(", indent, "plain");
                    printString(opStr, true, indent, "plain");
                    print(")", indent, "plain");
                } else {
                    decompileBinaryExpr(tree.down(1), tree.down(3), indent, precedence,
                            context, opStr, 1, 1, 1);
                }
                break;
            }
            case ILLang.op_cycle:
                print(KWCYCLE, indent, "keyword");
                if (ILUtils.isNotNone(tree.down(1))) {
                    space();
                    printAtomValue(tree.down(1), allUC, indent, "label");
                }
                break;
            case ILLang.op_exit:
                print("EXIT", indent, "keyword");
                if (ILUtils.isNotNone(tree.down(1))) {
                    space();
                    printAtomValue(tree.down(1), allUC, indent, "label");
                }
                break;
            case ILLang.op_data:
                // on ne doit pas mettre de parentheses autour de (-100) dans un data
                print(KWDATA, indent, "keyword");
                space();
                decompile(tree.down(1), indent + 4, 0, EMPTY_CONTEXT);
                print(" /", indent + 3, "plain");
                decompile(tree.down(2), indent + 5, 0,
                        contextPlus(contextPlus(contextPlus(context, RESULTEXPECTED_CTX), INDATA_CTX), DEEPEXPR_CTX));
                print("/", indent + 4, "plain");
                break;
            case ILLang.op_nameList:
                print(KWNAMELIST, indent, "keyword");
                space();
                print("/", indent + 3, "plain");
                decompile(tree.down(1), indent + 4, 0, EMPTY_CONTEXT);
                print("/ ", indent + 5, "plain");
                decompile(tree.down(2), indent + 4, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                break;
            case ILLang.op_fieldAccess:
                decompile(tree.down(1), indent, 0, context);
                print("%", indent, "plain");
                printAtomValue(tree.down(2), allUC, indent, "plain");
                break;
            case ILLang.op_pointerAccess:
                decompile(tree.down(1), indent, precedence, context);
                break;
            case ILLang.op_format: {
                Tree[] children = tree.down(2).children();
                printLabel(tree.down(1).stringValue());
                print(KWFORMAT, indent, "keyword");
                print("(", indent, "plain");
                for (int i = 0; i < children.length; i++) {
                    if (i > 0 &&
                            children[i - 1].opCode() != ILLang.op_formatElem &&
                            children[i].opCode() != ILLang.op_formatElem) {
                        print(" ", indent, "plain");
                    }
                    decompile(children[i], indent + 7, 0, EMPTY_CONTEXT);
                }
                print(")", indent + 6, "plain");
                break;
            }
            case ILLang.op_typedLetters:
                decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                print(" (", indent, "plain");
                decompile(tree.down(2), indent, 0, EMPTY_CONTEXT);
                print(")", indent, "plain");
                break;
            case ILLang.op_letterRange:
                printAtomValue(tree.down(1), allUC, indent, "plain");
                print("-", indent, "plain");
                printAtomValue(tree.down(2), allUC, indent, "plain");
                break;
            case ILLang.op_letter:
                printAtomValue(tree, allUC, indent, "plain");
                break;
            case ILLang.op_iterativeVariableRef:
                print("(", indent, "plain");
                decompile(tree.down(1), indent, 0, contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print(", ", indent, "plain");
                decompile(tree.down(2), indent, 0, EMPTY_CONTEXT);
                print(")", indent, "plain");
                break;
            case ILLang.op_label:
                printAtomValue(tree, allUC, indent, "label");
                break;
            case ILLang.op_statementFunctionDeclaration:
                decompile(tree.down(1), indent, 0,
                        contextPlus(contextPlus(context, ISFUNCNAME_CTX), RESULTEXPECTED_CTX));
                print(" = ", 0, "plain");
                decompile(tree.down(2), indent + 2, 0, contextPlus(context, RESULTEXPECTED_CTX));
                break;
            case ILLang.op_substringAccess:
                decompile(tree.down(1), indent, 0, context);
                print("(", indent, "plain");
                decompile(tree.down(2), indent, 0,
                        contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print(":", indent, "plain");
                decompile(tree.down(2), indent, 0,
                        contextPlus(contextPlus(context, RESULTEXPECTED_CTX), DEEPEXPR_CTX));
                print(")", indent, "plain");
                break;
            case ILLang.op_none:
                break;
            case ILLang.op_atomicStatement: {
                boolean isOmp = ILUtils.isIdent(tree.down(1), "omp", false);
                String oldContinuationChar = null;
                printComment("$");
                posX += commentChar.length() + 1;
                decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                printer.space(1);
                ++posX;
                if (isOmp) {
                    oldContinuationChar = this.continuationChar;
                    this.continuationChar = this.commentChar + "$OMP" + this.continuationChar;
                }
                print("ATOMIC", indent, "comment");
                printer.space(1);
                decompile(tree.down(2), posX - 11, 0, INOMP_CONTEXT);
                newLine(indent);
                if (isOmp) {
                    this.continuationChar = oldContinuationChar;
                }
                decompile(tree.down(3), indent, 0, context);
                break;
            }
            case ILLang.op_parallelRegion: {
                boolean isOmp = ILUtils.isIdent(tree.down(1), "omp", false);
                Tree uniqueParallelLoopInside = getSingleParallelLoop(tree.down(3));
                String oldContinuationChar = null;
                if (uniqueParallelLoopInside != null) {
                    printComment("$");
                    posX += commentChar.length() + 1;
                    decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                    printer.space(1);
                    ++posX;
                    if (isOmp) {
                        oldContinuationChar = this.continuationChar;
                        this.continuationChar = this.commentChar + "$OMP" + this.continuationChar;
                    }
                    print("PARALLEL DO", indent, "comment");
                    printer.space(1);
                    decompile(tree.down(2), posX - 15, 0, isOmp ? INOMP_CONTEXT : EMPTY_CONTEXT);
                    if (!ILUtils.isNullOrNoneOrEmptyList(uniqueParallelLoopInside.down(2))) {
                        if (!ILUtils.isNullOrNoneOrEmptyList(tree.down(2))) {
                            print(",", indent, "plain");
                            printer.space(1);
                        }
                        decompile(uniqueParallelLoopInside.down(2), posX - 15, 0, isOmp ? INOMP_CONTEXT : EMPTY_CONTEXT);
                    }
                    newLine(indent);
                    if (isOmp) {
                        this.continuationChar = oldContinuationChar;
                    }
                    decompile(uniqueParallelLoopInside.down(3), indent, 0, context);
                } else {
                    printComment("$");
                    posX += commentChar.length() + 1;
                    decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                    printer.space(1);
                    ++posX;
                    if (isOmp) {
                        oldContinuationChar = this.continuationChar;
                        this.continuationChar = this.commentChar + "$OMP" + this.continuationChar;
                    }
                    print("PARALLEL", indent, "comment");
                    printer.space(1);
                    decompile(tree.down(2), posX - 11, 0, INOMP_CONTEXT);
                    newLine(indent);
                    if (isOmp) {
                        this.continuationChar = oldContinuationChar;
                    }
                    decompile(tree.down(3), indent, 0, context);
                    newLine(indent);
                    printComment("$");
                    posX += commentChar.length() + 1;
                    decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                    printer.space(1);
                    ++posX;
                    print("END PARALLEL", indent, "comment");
                }
                break;
            }
            case ILLang.op_parallelLoop: {
                boolean isOmp = ILUtils.isIdent(tree.down(1), "omp", false);
                printComment("$");
                posX += commentChar.length() + 1;
                decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                printer.space(1);
                ++posX;
                String oldContinuationChar = null;
                if (isOmp) {
                    oldContinuationChar = this.continuationChar;
                    this.continuationChar = this.commentChar + "$OMP" + this.continuationChar;
                }
                print("DO", indent, "comment");
                printer.space(1);
                decompile(tree.down(2), posX - 11, 0, isOmp ? INOMP_CONTEXT : EMPTY_CONTEXT);
                newLine(indent);
                if (isOmp) {
                    this.continuationChar = oldContinuationChar;
                }
                decompile(tree.down(3), indent, 0, context);
                break;
            }
            case ILLang.op_directive: {
                printComment("$");
                posX += commentChar.length() + 1;
                decompile(tree.down(1), posX - 6, 0, EMPTY_CONTEXT);
                boolean isOmp = ILUtils.isIdent(tree.down(1), "omp", false);
                printer.space(1);
                ++posX;
                String oldContinuationChar = null;
                if (isOmp) {
                    oldContinuationChar = this.continuationChar;
                    this.continuationChar = this.commentChar + "$OMP" + this.continuationChar;
                }
                decompile(tree.down(2), posX - 11, 0, isOmp ? INOMP_CONTEXT : EMPTY_CONTEXT);
                if (isOmp) {
                    this.continuationChar = oldContinuationChar;
                }
                break;
            }

            case ILLang.op_switch: {
//                 if (fortranStyle9x) {
                    print("SELECT", indent, "keyword");
                    space();
                    print("CASE", indent, "keyword");
//                     space();
//                 } else {
//                     print("SELECT CASE", indent, "keyword");
//                 }
                print(" (", indent, "plain");
                decompile(tree.down(1), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                newLine(indent);
                decompile(tree.down(2), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                newLine(indent);
//                 if (fortranStyle9x) {
                    print("END", indent, "keyword");
                    space();
                    print("SELECT", indent, "keyword");
//                 } else {
//                     print(KWENDSELECT, indent, "keyword");
//                 }
                break;
            }
            case ILLang.op_switchType: {
                print("SELECT", indent, "keyword");
                space();
                print("TYPE", indent, "keyword");
                space();
                print("(", indent, "plain");
                decompile(tree.down(1), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                print(")", indent, "plain");
                newLine(indent);
                decompile(tree.down(2), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                newLine(indent);
                print("END", indent, "keyword");
                space();
                print("SELECT", indent, "keyword");
                break;
            }
            case ILLang.op_switchCase: {
                if (tree.down(2).length() != 0 || tree.down(1).length() != 0) {
                    if (tree.down(2).length() == 0 && tree.rankInParent() != 1) {
                        newLine(indent);
                    }
                    print("CASE", indent, "keyword");
                    space();
                    if (tree.down(1).length() != 0) {
                        print("(", indent, "plain");
                        decompile(tree.down(1), indent, 0, contextPlus(context, RESULTEXPECTED_CTX));
                        print(")", indent, "plain");
                    } else {
                        print("DEFAULT", indent, "keyword");
                    }
                    newLine(indent);
                    decompile(tree.down(2), indent + 2, 0, INBODY_CONTEXT);
                }
                break;
            }
            case ILLang.op_typeCase: {
                if (tree.down(2).length() != 0 || tree.down(1).length() != 0) {
                    if (tree.down(2).length() == 0 && tree.rankInParent() != 1) {
                        newLine(indent);
                    }
                    Tree typeTree = tree.down(1) ;
                    boolean isClass = (typeTree.opCode()==ILLang.op_classType) ;
                    if (isClass) {typeTree = typeTree.down(1); }
                    boolean isDefault = (isClass && typeTree.opCode()==ILLang.op_none) ;
                    print((isClass ? "CLASS" : "TYPE"), indent, "keyword");
                    space();
                    if (isDefault) {
                        print("DEFAULT", indent, "keyword");
                    } else {
                        print("IS", indent, "keyword");
                        space();
                        print("(", indent, "plain");
                        decompile(typeTree, indent, 0, context);
                        print(")", indent, "plain");
                    }
                    newLine(indent);
                    decompile(tree.down(2), indent + 2, 0, INBODY_CONTEXT);
                }
                break;
            }
            case ILLang.op_pointerDeclarator:
                decompile(tree.down(1), indent, 0, context);
                break;
            case ILLang.op_address:
                print("C_LOC(", indent, "plain");
                decompile(tree.down(1), indent, precedence, context);
                print(")", indent, "plain");
                break;
            case ILLang.op_useDecl:
                print(KWUSE, indent, "keyword");
                space();
                decompile(tree.down(1), indent, precedence, contextPlus(context, ISFUNCNAME_CTX));
                if (!ILUtils.isNullOrNone(tree.down(2)) &&
                        (tree.down(2).opCode() == ILLang.op_onlyVisibles || tree.down(2).length() != 0)) {
                    print(",", indent, "plain");
                    space();
                    decompile(tree.down(2), indent, precedence, context);
                }
                break;
            case ILLang.op_importDecl:
                print(KWIMPORT, indent, "keyword");
                space();
                decompile(tree.down(1), indent, precedence, context);
                break ;
            case ILLang.op_renamedVisibles: {
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1) {
                        print(",", indent, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_renamed: {
                boolean isOperatorDef = ILUtils.isAFortranOverloadedOperator(tree.down(1));
                if (isOperatorDef) {
                    print(KWOPERATOR, indent, "keyword");
                    print("(", indent, "plain");
                }
                decompile(tree.down(1), indent, precedence, context);
                if (isOperatorDef) {
                    print(")", indent, "plain");
                }
                space();
                print("=>", indent, "plain");
                space();
                if (isOperatorDef) {
                    print(KWOPERATOR, indent, "keyword");
                    print("(", indent, "plain");
                }
                decompile(tree.down(2), indent, precedence, context);
                if (isOperatorDef) {
                    print(")", indent, "plain");
                }
                break;
            }
            case ILLang.op_onlyVisibles: {
                Tree[] children = tree.children();
                print(KWONLY, indent, "keyword");
                space();
                print(":", indent, "plain");
                space();
                for (int i = 0; i < children.length; i++) {
                    boolean isOperatorDefI = ILUtils.isAFortranOverloadedOperator(children[i]);
                    if (isOperatorDefI) {
                        if (children[i].opCode() == ILLang.op_assign) {
                            print(KWASSIGNMENT, indent, "keyword");
                        } else {
                            print(KWOPERATOR, indent, "keyword");
                        }
                        print("(", indent, "plain");
                    }
                    decompile(children[i], indent, 0, context);
                    if (isOperatorDefI) {
                        print(")", indent, "plain");
                    }
                    if (i < children.length - 1) {
                        print(",", indent, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_interfaceDecl: {
                boolean hasAbstractMark = (ILUtils.isNotNone(tree.down(1))
                                           && tree.down(1).opCode()==ILLang.op_abstract) ;
                if (hasAbstractMark) {
                    print(KWABSTRACT, indent, "keyword");
                    space();
                }
                print(KWINTERFACE, indent, "keyword");
                space();
                if (ILUtils.isNotNone(tree.down(1)) && !hasAbstractMark) {
                    if (tree.down(1).isAtom()) {
                        printAtomValue(tree.down(1), !allLC, indent, "funcname");
                    } else {
                        if (tree.down(1).opCode() == ILLang.op_assign) {
                            print(KWASSIGNMENT, indent, "keyword");
                            print("(", indent, "keyword");
                            print("=", indent, "keyword");
                            print(")", indent, "keyword");
                        } else {
                            boolean isAOp = ILUtils.isAFortranOverloadedOperator(tree.down(1));
                            if (isAOp) {
                                print(KWOPERATOR, indent, "keyword");
                                print("(", indent, "keyword");
                            }
                            decompile(tree.down(1), indent, precedence, context);
                            if (isAOp) {
                                print(")", indent, "keyword");
                            }
                        }
                    }
                }
                newLine(indent);
                decompile(tree.down(2), indent + 4, precedence, context);
                newLine(indent);
                print(KWEND, indent, "keyword");
                space();
                print(KWINTERFACE, indent, "keyword");
                if (ILUtils.isNotNone(tree.down(3))) {
                    space();
                    if (tree.down(3).isAtom()) {
                        printAtomValue(tree.down(3), !allLC, indent, "funcname");
                    }
                }
                newLine(indent);
                break;
            }
            case ILLang.op_interfaces: {
                Tree[] children = tree.children();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, context);
                    if (i < children.length - 1) {
                        newLine(indent);
                    }
                }
                break;
            }
            case ILLang.op_moduleProcedure: {
                Tree[] children = tree.children();
                print(KWMODULE, indent, "keyword");
                space();
                print(KWPROCEDURE, indent, "keyword");
                space();
                for (int i = 0; i < children.length; i++) {
                    decompile(children[i], indent, 0, contextPlus(context, ISFUNCNAME_CTX));
                    if (i < children.length - 1) {
                        print(",", indent, "plain");
                        space();
                    }
                }
                break;
            }
            case ILLang.op_interface: {
                decompile(tree.down(1), indent, precedence, contextPlus(context, ININTERFACE_CTX));
                break;
            }
            case ILLang.op_accessDecl: {
                if (tree.down(1).opCode() == ILLang.op_accessDecl
                        && tree.down(2).opCode() != ILLang.op_typeDeclaration) {
                    decompile(tree.down(1), indent, 0, context);
                    space();
                    print("::", indent, "plain");
                    space();
                    decompile(tree.down(2), indent + 2, 0, context);
                } else if (tree.down(2).opCode() == ILLang.op_typeDeclaration) {
                    print("TYPE", indent, "keyword");
                    print(",", indent, "plain");
                    space();
                    if (tree.down(1).opCode() == ILLang.op_accessDecl) {
                        decompile(tree.down(1), indent, 0, context);
                    } else {
                        String modifierText = tree.down(1).stringValue();
                        if (!allLC) {
                            modifierText = modifierText.toUpperCase();
                        }
                        printString(modifierText, true, indent, "plain");
                    }
                    space();
                    print("::", indent, "plain");
                    space();
                    tree = tree.down(2);
                    decompile(tree.down(1), indent + 1, 0, contextPlus(context, ISTYPENAME_CTX));
                    newLine(indent);
                    decompile(tree.down(2), indent + 4, 0, contextPlus(context, INUNITBODY_CTX));
                    if (fortranStyle9x && tree.down(2).opCode() != ILLang.op_recordType) {
                        newLine(indent);
                    }
                    print(KWEND, indent, "keyword");
                    space();
                    print("TYPE", indent, "keyword");
                    if (fortranStyle9x) {
                        space();
                        printAtomValue(tree.down(1), !allLC, indent, "plain");
                    }
                    if (tree.down(2).opCode() != ILLang.op_recordType) {
                        TapEnv.toolWarning(-1, "(Fortran decompiler) undefined type: " + tree.down(1).stringValue());
                    }
                } else {
                    String modifier = tree.down(1).stringValue();
                    String modifierText = modifier;
                    if (modifier.equals("in") ||
                            modifier.equals("out") ||
                            modifier.equals("inout")) {
                        if (!allLC) {
                            modifierText = modifier.toUpperCase();
                        }
                        modifierText = "INTENT(" + modifierText + ')';
                    }
                    printString(modifierText, true, indent, "plain");
                    Tree[] children = tree.down(2).children();
                    if (modifier.equals("bind")) {
                        print("(", indent, "plain");
                        context = contextPlus(context, INBINDC_CTX);
                    } else {
                        space();
                        if (children.length != 0) {
                            print("::", indent, "plain");
                            space();
                        }
                    }
                    for (int i = 0; i < children.length; i++) {
                        boolean hasAnnot = children[i].opCode() != ILLang.op_none
                                && children[i].opCode() != ILLang.op_binary
                                && !children[i].isAtom() && children[i].down(1).opCode() == ILLang.op_none;
                        if (hasAnnot) {
                            if (children[i].opCode() == ILLang.op_assign) {
                                print(KWASSIGNMENT, indent, "keyword");
                            } else {
                                print(KWOPERATOR, indent, "keyword");
                            }
                            print("(", indent, "plain");
                        }
                        decompile(children[i], indent, 0, contextPlus(context, DEEPEXPR_CTX));
                        if (hasAnnot) {
                            print(")", indent, "plain");
                        }
                        if (i < children.length - 1) {
                            print(", ", indent, "plain");
                        }
                    }
                    if (modifier.equals("bind")) {
                        print(")", indent, "plain");
                        context = contextMinus(context, INBINDC_CTX);
                    }
                }
                break;
            }
            case ILLang.op_nullify: {
                print(KWNULLIFY, indent, "keyword");
                print("(", indent, "plain");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "plain");
                break;
            }
            case ILLang.op_deallocate: {
                print(KWDEALLOCATE, indent, "keyword");
                print("(", indent, "keyword");
                context = contextPlus(context, RESULTEXPECTED_CTX);
                decompile(tree.down(1), indent, 2, context);
                if (tree.down(2).opCode() != ILLang.op_none) {
                    print(", ", indent + 1, "plain");
                    decompile(tree.down(2), indent, 2, context);
                }
                print(")", indent, "keyword");
                break;
            }
            case ILLang.op_mod:
                print(KWMOD, indent, "keyword");
                print("(", indent, "keyword");
                decompile(tree.down(1), indent, 2, context);
                print(", ", indent + 1, "plain");
                decompile(tree.down(2), indent, 2, context);
                print(")", indent, "keyword");
                break;
            case ILLang.op_procedureInterfaceName:
                print("PROCEDURE", indent, "keyword");
                if ( !ILUtils.isNullOrNoneOrEmptyList(tree.down(1)) ) {
                    print("(", indent, "keyword");
                    decompile(tree.down(1), indent, 2, context);
                    print(")", indent, "keyword");
                }
                break ;
            case ILLang.op_functionDeclarator:
            case ILLang.op_functionType:
                decompile(tree.down(1), indent, 2, context);
                break;
            case ILLang.op_break:
                print("EXIT", indent, "keyword");
                break;
            case ILLang.op_integer:
                print(allLC ? "integer" : "INTEGER", indent, "typename");
                break;
            case ILLang.op_float:
                print(allLC ? "real" : "REAL", indent, "typename");
                break;
            case ILLang.op_complex:
                print(allLC ? "complex" : "COMPLEX", indent, "typename");
                break;
            case ILLang.op_boolean:
                print(allLC ? "logical" : "LOGICAL", indent, "typename");
                break;
            case ILLang.op_character:
                print(allLC ? "character" : "CHARACTER", indent, "typename");
                break;
            case ILLang.op_classType:
                print("CLASS", indent, "keyword");
                print("(", indent, "plain");
                decompile(tree.down(1), indent, 0, contextPlus(context, ISTYPENAME_CTX));
                print(")", indent, "plain");
                break ;
            case ILLang.op_numThreads:
                print(allLC ? "num_threads" : "NUM_THREADS", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_schedule:
                print(allLC ? "schedule" : "SCHEDULE", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_sharedVars:
                print(allLC ? "shared" : "SHARED", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_privateVars:
                print(allLC ? "private" : "PRIVATE", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_firstPrivateVars:
                print(allLC ? "firstprivate" : "FIRSTPRIVATE", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_lastPrivateVars:
                print(allLC ? "lastprivate" : "LASTPRIVATE", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_reductionVars:
                print(allLC ? "reduction" : "REDUCTION", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(":", indent, "comment");
                decompile(tree.down(2), indent, 2, context);
                print(")", indent, "comment");
                break;
            case ILLang.op_defaultSharingAttribute:
                print(allLC ? "default" : "DEFAULT", indent, "comment");
                print("(", indent, "comment");
                decompile(tree.down(1), indent, 2, context);
                print(")", indent, "comment");
                break;
            default: {
                TapEnv.toolWarning(-1, "(Fortran decompiler) unexpected operator: " + tree.opName() + " IN " + tree.parent());
                Tree[] subTrees = tree.children();
                print(" [UNEXPECTED:"+tree.opName(),indent, "keyword") ;
                if (subTrees != null && tree.length() != 0) {
                    for (Tree subTree : subTrees) {
                        print(" | ",indent, "keyword") ;
                        decompile(subTree, indent, 0, context);
                    }
                }
                print("] ",indent, "keyword") ;
                break;
            }
        }
        postDecompile(tree, indent);
    }

    /** Copy from the given typeSpec, the list of its modifiers that should be
     * printed together with its above type declaration. Returns a list arity tree
     * with operator op_labels (arbitrary choice, to force a comma-separated list).
     * If some modifiers are *not* copied and therefore remain in their original
     * place, set modifiersRemain to true. */
    private Tree getModifiersForTypeDecl(Tree typeSpec, ToBool modifiersRemain) {
        Tree result = null ;
        if (typeSpec.opCode()==ILLang.op_recordType) {
            Tree[] modifiers = typeSpec.down(2).children() ;
            TapList<Tree> selectedModifiers = null ;
            for (int i=modifiers.length-1 ; i>=0 ; --i) {
                if (!modifiers[i].isAtom())
                    selectedModifiers = new TapList<>(ILUtils.copy(modifiers[i]), selectedModifiers) ;
                else {
                    //possibly select here atomic modifiers that must be printer one level up.
                    if (modifiersRemain!=null) modifiersRemain.set(true) ;
                }
            }
            if (selectedModifiers!=null) {
                result = ILUtils.build(ILLang.op_labels, selectedModifiers) ;
            }
        }
        return result ;
    }

    private void decompile9xType(Tree tree, int indent, int context) throws IOException {
        boolean mustWriteType = (tree.opCode()==ILLang.op_ident);
        boolean mustWriteClass = (tree.opCode()==ILLang.op_classType);
        if (mustWriteClass) {tree = tree.down(1) ;}
        if (mustWriteType) {
            print("TYPE", indent, "keyword");
            print("(", indent, "plain");
        } else if (mustWriteClass) {
            print("CLASS", indent, "keyword");
            print("(", indent, "plain");
        }
        decompile(tree, indent, 0, contextPlus(context, ISTYPENAME_CTX));
        if (mustWriteClass || mustWriteType) {
            print(")", indent, "plain");
        }
    }

    /**
     * Decompilation of a binary expression, taking care of precedence.
     *
     * @param curPrecedence   precedence strength required by the enclosing
     *                        context: The higher, the more parentheses around this binary expr.
     * @param maxPrecedence   precedence strength achieved by the current binary
     *                        expression: the lower, the more parentheses around this binary expr.
     * @param leftPrecedence  precedence required for the left child:
     *                        The higher, the more parentheses around this child
     * @param rightPrecedence precedence accepted for the left child:
     *                        The higher, the more parentheses around this child.
     */
    private void decompileBinaryExpr(Tree leftTree, Tree rightTree, int indent,
                                     int curPrecedence, int context, String op, int maxPrecedence,
                                     int leftPrecedence, int rightPrecedence) throws IOException {
        if (curPrecedence >= 8 || maxPrecedence >= 8) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        context = contextPlus(context, RESULTEXPECTED_CTX);
        if (curPrecedence > maxPrecedence) {
            print("(", indent, "plain");
        }
        decompile(leftTree, indent, leftPrecedence, context);
        if (!hasContext(context, DEEPEXPR_CTX) &&
                leftTree.opCode() != ILLang.op_none) {
            space();
        }
        print(op, indent, "plain");
        if (!hasContext(context, DEEPEXPR_CTX) &&
                rightTree.opCode() != ILLang.op_none) {
            space();
        }
        decompile(rightTree, indent, rightPrecedence, context);
        if (curPrecedence > maxPrecedence) {
            print(")", indent, "plain");
        }
    }

    private void decompileUnaryExpr(Tree subTree, int indent,
                                    int curPrecedence, int context, String op, int maxPrecedence,
                                    int childPrecedence) throws IOException {
        if (curPrecedence >= 8 || maxPrecedence >= 8) {
            context = contextPlus(context, DEEPEXPR_CTX);
        }
        context = contextPlus(context, RESULTEXPECTED_CTX);
        boolean inDataCtx = hasContext(context, INDATA_CTX);
        if (curPrecedence > maxPrecedence && !inDataCtx) {
            print("(", indent, "plain");
        }
        print(op, indent, "plain");
        decompile(subTree, indent, childPrecedence, context);
        if (curPrecedence > maxPrecedence && !inDataCtx) {
            print(")", indent, "plain");
        }
    }

    private void decompileIOSpec(Tree ioSpec, int rank, int indent,
                                 int context) throws IOException {
        if (rank == 2) {
            context = contextPlus(context, ISLABEL_CTX);
        }
        if (ioSpec.opCode() == ILLang.op_nameEq) {
            String key = ioSpec.down(1).stringValue();
            print(key, indent, "keyword");
            print("=", indent, "plain");
            if (key.equals("err") || key.equals("end") || key.equals("fmt")) {
                context = contextPlus(context, ISLABEL_CTX);
            } else {
                context = contextPlus(context, RESULTEXPECTED_CTX);
            }
            ioSpec = ioSpec.down(2);
            if (ioSpec.opCode() == ILLang.op_ident) {
                boolean inBindCCtx = hasContext(context, INBINDC_CTX);
                if (inBindCCtx) {
                    /*inside bind(c, name = ident):*/
                    ioSpec = ILUtils.build(ILLang.op_stringCst, ioSpec.stringValue());
                    context = contextMinus(context, INBINDC_CTX);
                }
            }
        }
        if (ioSpec.opCode() == ILLang.op_expressions &&
                allStringCst(ioSpec.children())) {
            /*Case of a multi-line string : */
            Tree[] strings = ioSpec.children();
            String oneString;
            for (int i = 0; i < strings.length; i++) {
                oneString = quote(strings[i].stringValue());
                if (i == 0) {
                    oneString = "'" + oneString;
                }
                if (i == strings.length - 1) {
                    oneString = oneString + "'";
                }
                print(oneString, indent, "string");
            }
        } else {
            //general case:
            decompile(ioSpec, indent, 0, contextPlus(context, DEEPEXPR_CTX));
        }
    }

    private boolean allStringCst(Tree[] strings) {
        boolean ok = true;
        int i = strings.length - 1;
        while (ok && i >= 0) {
            ok = strings[i].opCode() == ILLang.op_stringCst;
            i--;
        }
        return ok;
    }

    private boolean isSingleMergeableIf(Tree tree) {
        int opCode = tree.opCode();
        if (opCode == ILLang.op_blockStatement) {
            return tree.length() == 1 && isSingleMergeableIf(tree.down(1));
        } else {
            return opCode == ILLang.op_if;
        }
    }

    private boolean isSingleMergeableWhere(Tree tree) {
        boolean result;
        int opCode = tree.opCode();
        if (opCode == ILLang.op_blockStatement) {
            result = tree.length() == 1 && isSingleMergeableWhere(tree.down(1));
        } else {
            result = opCode == ILLang.op_where;
            if (result && !ILUtils.isNullOrNone(tree.down(4))) {
                Tree upperWhere = findUpperWhereWithSameLabel(tree, tree.down(4).stringValue());
                result = upperWhere != null;
            }
        }
        return result;
    }

    private Tree findUpperWhereWithSameLabel(Tree tree, String label) {
        Tree result = null;
        Tree father = tree.parent();
        while (father != null && result == null) {
            if (father.opCode() == ILLang.op_where
                    && !ILUtils.isNullOrNone(father.down(4))) {
                if (father.down(4).stringValue().equals(label)) {
                    result = father;
                }
            }
            father = father.parent();
        }
        return result;
    }

    /**
     * Tree Regeneration sometimes puts the "isFunctionName" annotation
     * to tell that this identifier "tree" must be understood (and displayed!)
     * as a subroutine name.
     */
    private boolean isMarkedAsFunctionName(Tree tree) {
        Boolean isFunctionNameAnnot =
                tree.getAnnotation("isFunctionName");
        return isFunctionNameAnnot != null && isFunctionNameAnnot;
    }

    /**
     * @return not null when the given tree is or contains one and only one
     * non-null-none instruction,
     * and this instruction is a simple-line instruction,
     * i.e. not a structured statement.
     * In that case returns this instruction, otherwise null
     */
    private Tree isSimpleExpr(Tree tree) {
        while (tree.opCode() == ILLang.op_blockStatement &&
                tree.length() == 1 &&
                hasNoAttachedComments(tree)) {
            tree = tree.down(1);
        }
        int opCode = tree.opCode();
        if (opCode != ILLang.op_none &&
                hasNoAttachedComments(tree) &&
                (TapIntList.contains(expressionPhylum, tree.opCode()) ||
                        opCode == ILLang.op_assign ||
                        opCode == ILLang.op_call ||
                        opCode == ILLang.op_break ||
                        opCode == ILLang.op_continue ||
                        opCode == ILLang.op_return ||
                        opCode == ILLang.op_throw ||
                        opCode == ILLang.op_exit ||
                        opCode == ILLang.op_cycle ||
                        opCode == ILLang.op_stop || opCode == ILLang.op_goto ||
                        opCode == ILLang.op_gotoLabelVar ||
                        opCode == ILLang.op_assignLabelVar ||
                        opCode == ILLang.op_compGoto ||
                        opCode == ILLang.op_ioCall)) {
            return tree;
        } else {
            return null;
        }
    }

    /**
     * Checks that "tree" is either a single op_parallelLoop, or a blockStatement with one op_parallelLoop.
     */
    private Tree getSingleParallelLoop(Tree tree) {
        if (tree != null && tree.opCode() == ILLang.op_blockStatement) {
            if (tree.length() != 1) {
                return null;
            }
            tree = tree.down(1);
        }
        if (tree != null && tree.opCode() == ILLang.op_parallelLoop) {
            return tree;
        } else {
            return null;
        }
    }

    private boolean hasNoAttachedComments(Tree tree) {
        return tree.getAnnotation("preComments") == null &&
                tree.getAnnotation("preCommentsBlock") == null &&
                tree.getAnnotation("postComments") == null &&
                tree.getAnnotation("postCommentsBlock") == null;
    }

    private String sortModifiers(Tree tree, TapList<Tree> modifiersBefore,
                                 TapList<Tree> typeSuffixes, TapList<Tree> modifiersAfter) {
        String basisTypeName = null ;
        TapList<Tree> modifiersList = tree.down(1).childrenList() ;
        Tree tree2 = tree.down(2) ;
        boolean peeled = false ;
        while (!peeled) {
          switch (tree2.opCode()) {
            case ILLang.op_integer:
                basisTypeName = "integer";
                peeled = true ;
                break;
            case ILLang.op_float:
                basisTypeName = "real" ;
                peeled = true ;
                break;
            case ILLang.op_complex:
                basisTypeName = "complex";
                peeled = true ;
                break;
            case ILLang.op_boolean:
                basisTypeName = "logical";
                peeled = true ;
                break;
            case ILLang.op_character:
                basisTypeName = "character";
                peeled = true ;
                break;
            case ILLang.op_ident:
                basisTypeName = tree2.stringValue();
                peeled = true ;
                if (basisTypeName == null) {
                    basisTypeName = "UNKNOWNTYPE";
                } else if (fortranStyle9x) {
                    basisTypeName = "TYPE" + "(" + basisTypeName + ')';
                }
                break;
            case ILLang.op_pointerType:
                modifiersList =
                    new TapList<>(ILUtils.build(ILLang.op_ident, "pointer"),
                                  modifiersList) ;
                tree2 = tree2.down(1) ;
                break ;
            case ILLang.op_arrayType:
                if (tree2.down(1).opCode()==ILLang.op_character/* && tree2.down(2).opCode()==ILLang.op_dimColons*/) {
                    // arrays of chars are pretty-printed differently:
                    basisTypeName = null ;
                    peeled = true ;
                } else {
                    modifiersList =
                        new TapList<>(tree2.down(2), modifiersList) ;
                    tree2 = tree2.down(1) ;
                }
                break ;
            case ILLang.op_modifiedType: {
                Tree[] modifierTrees = tree2.down(1).children() ;
                for (int i=modifierTrees.length-1 ; i>=0 ; --i) {
                    modifiersList =
                        new TapList<>(modifierTrees[i], modifiersList) ;
                }
                tree2 = tree2.down(2) ;
                break ;
            }
            default:
                basisTypeName = null;
                peeled = true ;
                break;
          }
        }
        modifiersList = TapList.nreverse(modifiersList) ;
        Tree modifierTree;
        while (modifiersList!=null) {
            modifierTree = modifiersList.head ;
            switch (modifierTree.opCode()) {
                case ILLang.op_dimColons:
                    modifiersAfter.head = modifierTree;
                    break;
                case ILLang.op_star:
                    typeSuffixes.placdl(modifierTree);
                    break;
                case ILLang.op_intCst:
                    if (basisTypeName!=null) {
                        if (basisTypeName.equals("integer") || basisTypeName.equals("real")
                            || basisTypeName.equals("complex") || basisTypeName.equals("character")) {
                            basisTypeName = basisTypeName + "*" + modifierTree.stringValue();
                        } else {
                            typeSuffixes.placdl(modifierTree);
                        }
                    }
                    break;
                default: {
                    String modifierText = ILUtils.toString(modifierTree);
                    if (modifierText != null && !modifierText.isEmpty()) {
                        if (modifierText.equals("constant")) {
                            modifierText = "parameter";
                            modifierTree = ILUtils.build(ILLang.op_ident, modifierText);
                        }
                        if ((fortranStyle9x
                             && (modifierText.equals("external")
                                 || modifierText.equals("extern")
                                 || modifierText.equals("intrinsic")
                                 || modifierText.equals("save")))
                            || modifierText.equals("parameter")
                            || modifierText.equals("target")
                            || modifierText.equals("pointer")
                            || modifierText.equalsIgnoreCase("public")
                            || modifierText.equalsIgnoreCase("private")
                            || modifierText.equals("allocatable")
                            || modifierText.equals("optional")
                            || modifierText.equals("in")
                            || modifierText.equals("out")
                            || modifierText.equals("inout")
                            || modifierText.equals("value")
                            || modifierText.equals("volatile")
                            || modifierText.equals("contiguous")
                            || modifierText.equals("nopass")
                            || modifierText.startsWith("bind")) {
                            modifiersAfter.placdl(modifierTree);
                        } else if (basisTypeName != null
                                && basisTypeName.equals("real")
                                && modifierText.equals("double")) {
                            basisTypeName = "double precision";
                        } else if (basisTypeName != null
                                && basisTypeName.equals("complex")
                                && modifierText.equals("double")) {
                            basisTypeName = "double complex";
                        } else if (modifierTree.opCode() != ILLang.op_ident
                                   || !((fortranStyle9x
                                         && (modifierText.equals("signed")
                                             || modifierText.equals("unsigned")
                                             || modifierText.equals("short")
                                             || modifierText.equals("long")
                                             || modifierText.equals("double")
                                             || modifierText.equals("const")
                                             || modifierText.equals("auto")
                                             || modifierText.equals("sequence")))
                                        || (language == TapEnv.FORTRAN
                                            && (modifierText.equals("double")
                                                || modifierText.equals("external")
                                                || modifierText.equals("extern"))))) {
                            typeSuffixes.placdl(modifierTree);
                        } else if (!fortranStyle9x) {
                            if (modifierTree.opCode() == ILLang.op_ident) {
                                modifiersBefore.placdl(modifierTree);
                            }
                        } else {
                            TapEnv.toolWarning(-1, "(Fortran decompiler) dropped modifier: " + modifierTree);
                        }
                    }
                }
            }
            modifiersList = modifiersList.tail ;
        }
        return basisTypeName;
    }

    /**
     * Prints the contents of an atomic Tree of typeName
     * Assumes that all atoms are provided in lowercase.
     */
    private void printAtomValueTypeName(Tree atomTree, boolean forceUpperCase,
                                        int indent) throws IOException {
        String atomValueString = atomTree.stringValue();
        if (atomValueString == null) {
            atomValueString = "UNKNOWNTYPE";
        }
        if (forceUpperCase) {
            print(atomValueString.toUpperCase(), indent, "typename");
        } else {
            print(atomValueString, indent, "typename");
        }
    }

    /**
     * Prints all waiting line-comments, and resets waiting comments to null.
     */
    @Override
    protected void tryFlushComments(int indent, boolean prefix) throws IOException {
        if (posX == 0) {
            while (waitingComments != null) {
                printComment(waitingComments.head);
                printer.newLine();
                posX = 0;
                posY++;
                waitingComments = waitingComments.tail;
            }
        }
    }

    /**
     * Prints a comment in the Fortran syntax.
     * We assume we are just after a newLine() was done.
     */
    private void printComment(String text) throws IOException {
        int length = text.length();
        if (length > LINE_LENGTH_MAX) {
            printOneCommentLine(text.substring(0, LINE_LENGTH_MAX));
            printer.newLine();
            printComment(text.substring(LINE_LENGTH_MAX));
        } else {
            printOneCommentLine(text);
        }
    }

    private void printOneCommentLine(String text) throws IOException {
        printer.printText(commentChar, "comment");
        printer.printText(text, "comment");
    }

    /**
     * Prints a label in columns 1 to 5.
     * We assume we are just after a newLine() was done.
     */
    private void printLabel(String label) throws IOException {
        if (label.length() >= 5) {
            printer.printText(label, "label");
            posX = label.length();
        } else {
            printer.space(1);
            printer.printText(label, "label");
            posX = label.length() + 1;
        }
        if (fortranStyle9x) {
            space();
        }
    }

    private String quote(String str) {
        StringBuilder strBuf = new StringBuilder(str);
        int i = 0;
        while (i < strBuf.length()) {
            if (strBuf.charAt(i) == '\'') {
                strBuf.insert(i + 1, '\'');
                i++;
            }
            i++;
        }
        return strBuf.toString();
    }

    /**
     * Prints the contents of an atomic Tree. Takes care of the case.
     * Assumes that all atoms are provided in lowercase.
     */
    private void printAtomValue(Tree atomTree, boolean forceUpperCase,
                                int indent, String kind) throws IOException {
        String atomValueString = atomTree.stringValue();

// Patch for a bug that is hard to track because looks non-deterministic on SOLPS !
// Remove surrounding '#' in "#var_d#" or "#NBDirsMax#" :
        if (atomValueString!=null && !atomValueString.isEmpty()
            && atomValueString.charAt(0)=='#'
            && atomValueString.charAt(atomValueString.length()-1)=='#') {
            TapEnv.toolWarning(-1, "(Fortran decompiler) unfinished diff identifier: "+atomTree+" @"+Integer.toHexString(atomTree.hashCode())) ;
//             atomValueString = atomValueString.substring(1, atomValueString.length()-1) ;
        }
        printString(atomValueString, forceUpperCase, indent, kind);
    }

    /**
     * Prints a String. Takes care of the case.
     * Assumes that all atoms are provided in lowercase.
     */
    private void printString(String atomValueString, boolean forceUpperCase,
                             int indent, String kind) throws IOException {
        if (atomValueString != null) {
            if (forceUpperCase) {
                print(atomValueString.toUpperCase(), indent, kind);
            } else {
                print(atomValueString, indent, kind);
            }
        } else {
            print("Error:nullTree", indent, kind);
        }
    }

}
