/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * An object around a boolean, which can thus be modified.
 */
public final class ToBool {
    private boolean value;

    public ToBool() {
        super();
    }

    /**
     * Creation, with initial value.
     */
    public ToBool(boolean value) {
        super();
        this.value = value;
    }

    /**
     * Set the value.
     */
    public void set(boolean value) {
        this.value = value;
    }

    /**
     * Switch the value.
     */
    public void not() {
        value = !value;
    }

    /**
     * @return the value.
     */
    public boolean get() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "]";
    }
}
