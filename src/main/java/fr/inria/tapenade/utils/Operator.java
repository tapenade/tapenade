/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import java.io.BufferedWriter;
import java.io.IOException;

/**
 * An operator of Abstract Syntax Trees.
 */
public class Operator {

    /**
     * The name of this Operator.
     */
    public final String name;

    /**
     * A unique integer code for this Operator,
     * which is its rank in the list of all the language's Operator's.
     */
    public final int code;

    /**
     * The arity of this operator.
     * <pre>
     * Convention: normal fixed arities range in [0,+inf[;
     * Arity LIST_ARITY = -1 stands for list arity;
     * Arity ATOM_INT_ARITY = -2 stands for atomic operator with an int;
     * Arity ATOM_STRING_ARITY = -3 stands for atomic operator with a String.
     * </pre>
     */
    public final int arity;

    /**
     * For list arity operator.
     */
    public static final int LIST_ARITY = -1;
    /**
     * For atomic operator with an int.
     */
    public static final int ATOM_INT_ARITY = -2;
    /**
     * For atomic operator with a String.
     */
    public static final int ATOM_STRING_ARITY = -3;

    /**
     * The required phyla for the children.
     */
    protected final int[] childrenPhyla;

    public Operator(String nameStr, int code, int arity, int[] phyla) {
        super();
        this.name = nameStr;
        this.code = code;
        this.arity = arity;
        this.childrenPhyla = phyla;
    }

    /**
     * Build a Tree with this Operator.
     *
     * @return a new, empty, Tree node with this Operator.
     */
    public Tree tree() {
        switch (arity) {
            case ATOM_STRING_ARITY:
                return new SAtomTree(this);
            case ATOM_INT_ARITY:
                return new IAtomTree(this);
            case LIST_ARITY:
                return new ListTree(this);
            default:
                return new FixedTree(this);
        }
    }

    /**
     * @return True when this is a fixed-arity Operator.
     */
    public boolean isFixed() {
        return arity > LIST_ARITY;
    }

    /**
     * @return True when this is a list-arity Operator.
     */
    public boolean isList() {
        return arity == LIST_ARITY;
    }

    /**
     * @return True when this Operator is an atom.
     */
    public boolean isAtom() {
        return arity < LIST_ARITY;
    }

    /**
     * @return True when this Operator is a String-valued atom.
     */
    public boolean isStringAtom() {
        return arity == ATOM_STRING_ARITY;
    }

    /**
     * @return True when this Operator is an integer-valued atom.
     */
    public boolean isIntAtom() {
        return arity == ATOM_INT_ARITY;
    }

    public void writeToILFile(BufferedWriter fileWriter) throws IOException {
        // The following trick to append ":"+name may be simply removed
        // when we decide that the IL file need not be human-readable
        fileWriter.write(code + ':' + name + System.lineSeparator());
    }

    @Override
    public String toString() {
        return name;
    }
}
