/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import java.io.IOException;

/**
 * An atom node of a Tree with an int value.
 */
class IAtomTree extends Tree {

    /**
     * The value of this Tree. Only atom Tree's hold values.
     */
    private int value;

    protected IAtomTree(Operator operator) {
        super(operator);
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public Tree copy() {
        IAtomTree newTree = (IAtomTree) operator().tree();
        newTree.value = value;
        newTree.copyAnnotations(this);
        return newTree;
    }

    @Override
    public boolean isAtom() {
        return true;
    }

    @Override
    public boolean isStringAtom() {
        return false;
    }

    @Override
    public boolean isIntAtom() {
        return true;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public Tree[] children() {
        return null;
    }

    @Override
    public int intValue() {
        return value;
    }

    @Override
    public String stringValue() {
        return String.valueOf(value);
    }

    @Override
    public boolean equalsTree(Tree other) {
        if (other == null || operator() != other.operator()) {
            return false;
        }
        return value == ((IAtomTree) other).value;
    }

    @Override
    public void writeToILFile(java.io.BufferedWriter fileWriter) throws IOException {
        operator().writeToILFile(fileWriter);
        fileWriter.write(value + System.lineSeparator());
    }

    @Override
    public String toString() {
        return operator() + ":" + value;
    }

}
