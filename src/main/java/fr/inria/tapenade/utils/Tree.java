/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * A node of an Abstract Syntax Tree.
 */
public abstract class Tree {

    /**
     * The operator of this node.
     */
    private Operator operator;

    private Tree parent;

    /**
     * The annotations on this node.
     */
    private Hashtable<String, Object> annotations;

    /**
     * Build a new Tree with given operator.
     */
    protected Tree(Operator operator) {
        this.operator = operator;
    }

    public void setOperator(Operator oper) {
        this.operator = oper;
    }

    /**
     * Adds a new child to this node.
     * Allowed only when this is a list node.
     * Inserts the added child so that its rank will be "rank" (rank of 1st child is 1).
     * Negative ranks are allowed, designating positions backward from the end,
     * e.g -1 is last child, -2 is last but one.
     */
    public void addChild(Tree addedChild, int rank) {
        TapEnv.toolError("add child tree not allowed on " + this);
    }

    /**
     * Adds a set of new children to this node.
     * Allowed only when this is a list node.
     * Inserts the addedChildren so that the rank of the first of them will be "rank" (rank of 1st child is 1).
     * Negative ranks are allowed, designating positions backward from the end,
     * e.g -1 is last child, -2 is last but one.
     * When rank is negative, it will be the position of the last of addedChildren.
     */
    public void addChildren(Tree[] addedChildren, int rank) {
        TapEnv.toolError("add children trees not allowed on " + this);
    }

    /**
     * Adds a set of new children to this node.
     * Allowed only when this is a list node.
     * Inserts the addedChildren so that the rank of the first of them will be "rank" (rank of 1st child is 1).
     * Negative ranks are allowed, designating positions backward from the end,
     * e.g -1 is last child, -2 is last but one.
     * When rank is negative, it will be the position of the last of addedChildren.
     */
    public void addChildren(TapList<Tree> addedChildren, int rank) {
        TapEnv.toolError("add children trees not allowed on " + this);
    }

    /**
     * Replaces this Tree's child of rank "rank" with the new child (rank of 1st child is 1).
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     * When this is a list node and rank is larger than its length,
     * this enlarges the list and pads with null children (is this really necessary?)
     */
    public void setChild(Tree newChild, int rank) {
        TapEnv.toolError("change child tree not allowed on " + this);
    }

    /**
     * Cuts off this Tree's child of rank "rank", and returns it (rank of 1st child is 1).
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     * Leaves a null in this Tree, at the location of the cut child.
     */
    public Tree cutChild(int rank) {
        TapEnv.toolError("cut child tree not allowed on " + this);
        return null;
    }

    /**
     * Cuts off this Tree's child of rank "rank" (rank of 1st child is 1).
     * Allowed only when this is a list arity node.
     * Leaves no hole in this Tree in place of the cut child.
     */
    public void removeChild(int rank) {
        TapEnv.toolError("remove child tree not allowed on " + this);
    }

    /**
     * Utility to branch a child Tree under this Tree, as its new index-th child,
     * with a correct full management of the "parent" links.
     *
     * @param parentNewChildren the future array of children of this parent tree,
     *                          which may be different from the previous array, for instance in the case of variable-arity lists.
     */
    protected void connectNewChild(Tree newChild, Tree[] parentNewChildren, int index) {
        if (newChild.parent != null) {
            newChild.parent.cutChild(newChild.rankInParent());
        }
        if (parentNewChildren[index] != null) {
            parentNewChildren[index].parent = null;
        }
        parentNewChildren[index] = newChild;
        newChild.parent = this;
    }

    /**
     * Sets the value attached to an integer atom Tree node.
     * Allowed only when this is an integer atom node.
     */
    public void setValue(int value) {
        TapEnv.toolError("set int value of leaf tree not allowed on " + this);
    }

    /**
     * Sets the value attached to a string atom Tree node.
     * Allowed only when this is a string atom node.
     */
    public void setValue(String value) {
        TapEnv.toolError("set String value of leaf tree not allowed on " + this);
    }

    /**
     * Sets the value of annotation "name" of this Tree.
     * If the annotation is not already present, creates it.
     *
     * @return the annotation pair, after setting is done.
     */
    public <T> TapPair<String, T> setAnnotation(String name, T val) {
        TapPair<String, T> annot = getAnnotationPair(name);
        if (annot != null) {
            annot.second = val;
        } else {
            if (annotations == null) {
                annotations = new Hashtable<>();
            }
            annot = new TapPair<>(name, val);
            annotations.put(name, annot);
        }
        return annot;
    }

    /**
     * Removes the annotation "name" of this Tree.
     */
    public void removeAnnotation(String name) {
        if (annotations != null) {
            annotations.remove(name);
        }
    }

    /**
     * @param name The name of the annotation.
     * @return the annotation pair found, null otherwise.
     */
    @SuppressWarnings("unchecked")
    public <T> TapPair<String, T> getAnnotationPair(String name) {
        return annotations == null ? null : (TapPair<String, T>) annotations.get(name);
    }

    /**
     * @return all annotations currently attached to this Tree.
     */
    public Enumeration<?> allAnnotations() {
        return annotations == null ? null : annotations.elements();
    }

    /**
     * @return the value of annotation "name" of this Tree, null if not present.
     */
    public <T> T getAnnotation(String name) {
        if (annotations == null) {
            return null;
        }
        @SuppressWarnings("unchecked")
        TapPair<String, T> annot = (TapPair<String, T>) annotations.get(name);
        return annot == null ? null : annot.second;
    }

    /**
     * @return the value of annotation "name" of this Tree, null if not present,
     * and removes it if it was present.
     */
    public <T> T getRemoveAnnotation(String name) {
        if (annotations == null) {
            return null;
        }
        @SuppressWarnings("unchecked")
        TapPair<String, T> annot = (TapPair<String, T>) annotations.get(name);
        annotations.remove(name);
        return annot == null ? null : annot.second;
    }

    /**
     * Copies the annotations of the model onto this Tree.
     * Also manages copies of unfinished identifiers such as #xd#.
     * TODO: annotations are dangerous: copyAnnotations() share them between copies
     * only when the annotation already exists, Some parts on the code rely on this!
     * This should be cleaned up, but meanwhile, we force explicit sharing upon copy
     * for some specific annotations (see annotationsToShareAlways).
     */
    @SuppressWarnings("unchecked")
    public void copyAnnotations(Tree model) {
        NewSymbolHolder.copySymbolHolderAnnotation(model, this, null);
        if (model.annotations != null) {
            TapPair<String, ?> annot;
            for (Enumeration<?> e = model.annotations.elements(); e.hasMoreElements(); ) {
                annot = (TapPair<String, ?>) e.nextElement();
                setAnnotation(annot.first, annot.second);
            }
        }
    }

    /**
     * @return a copy of this Tree, including a copy of all its annotations.
     * Also manages copies of unfinished identifiers such as #xd#.
     */
    public abstract Tree copy();

    /**
     * @param path Some string that will name this Tree in potential error messages.
     * @return true if the syntax of this Tree is correct, i.e. all children trees non null and in correct phylum.
     */
    public boolean checkSyntax(String path) {
        return checkSyntax(null, path);
    }

    /**
     * @return true if the syntax of this Tree is correct, i.e. all children trees non null and in correct phylum.
     */
    protected boolean checkSyntax(Phylum phylum, String path) {
        if (phylum == null) {
            return true;
        }
        boolean foundOp = false;
        for (int i = phylum.operators.length - 1; i >= 0 && !foundOp; --i) {
            foundOp = phylum.operators[i] == operator.code;
        }
        if (foundOp) {
            return true;
        } else {
            TapEnv.toolError("Checking Tree: operator " + operator.name + " not allowed at " + path + " .... " + this.parent());
            return false;
        }
    }

    /**
     * @return The Operator of this Tree node.
     */
    public Operator operator() {
        return operator;
    }

    /**
     * @return The integer code of the Operator of this Tree node.
     */
    public int opCode() {
        return operator.code;
    }

    /**
     * @return The name of the Operator of this Tree node.
     */
    public String opName() {
        return operator.name;
    }

    /**
     * @return True iff this Tree node is an atom.
     */
    public abstract boolean isAtom();

    /**
     * @return True iff this Tree node is an atom that contains a String.
     */
    public abstract boolean isStringAtom();

    /**
     * @return True iff this Tree node is an atom that contains an integer.
     */
    public abstract boolean isIntAtom();

    /**
     * @return True iff this Tree node is a list node.
     */
    public abstract boolean isList();

    /**
     * @return True iff this Tree node is a fixed-arity node.
     */
    public abstract boolean isFixed();

    /**
     * @return the parent node of this Tree node, or null if no parent.
     */
    public Tree parent() {
        return parent;
    }

    /**
     * Set the parent node.
     */
    public void setParent(Tree parent) {
        this.parent = parent;
    }

    /**
     * @return the rank of this node in its parent node.
     * Rank starts at 1. @return -1 if no parent.
     */
    public int rankInParent() {
        return parent == null ? -1 : parent.getRank(this);
    }

    /**
     * @return the number of children of this Tree node.
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     */
    public int length() {
        TapEnv.toolError("no length defined on " + this);
        return 0;
    }

    /**
     * @return the array of the children of this Tree node.
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     */
    public Tree[] children() {
        TapEnv.toolError("no children defined on " + this);
        return new Tree[0];
    }

    /**
     * @return the ordered list of children of this Tree node.
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     */
    public TapList<Tree> childrenList() {
        TapEnv.toolError("no children list defined on " + this);
        return null;
    }

    /**
     * @return the child of rank "rank" of this Tree node (rank of 1st child is 1).
     * Allowed only when this is either a fixed or list arity node, but not an atom node.
     */
    public Tree down(int rank) {
        TapEnv.toolError("Tree down not allowed on " + this);
        return null;
    }

    /**
     * @return the rank of the given child node in this node (rank of 1st child is 1).
     */
    protected int getRank(Tree child) {
        TapEnv.toolError("getRank not allowed on " + this);
        return -1;
    }

    /**
     * Used only for TopDownTreeWalk(). Remove some later day?
     */
    protected Tree right() {
        int rk = rankInParent();
        if (rk < 0 || rk >= parent.length()) {
            return null;
        }
        return (parent.children())[rk];
    }

    /**
     * @return the value contained in this atom node, in the form of a String.
     * Allowed only when this is an atom node.
     */
    public String stringValue() {
        TapEnv.toolError("no String value in " + this);
        return null;
    }

    /**
     * @return the integer value contained in this atom node.
     * Allowed only when this is an integer atom node.
     */
    public int intValue() {
        TapEnv.toolError("no integer value in " + this);
        return 0;
    }

    /**
     * @return True iff this Tree is equal to the other Tree.
     * Doesn't check for equality of annotations.
     */
    public abstract boolean equalsTree(Tree other);

    /**
     * Prints into a file, as an AST.
     *
     * @throws IOException if an input or output error is detected.
     */
    protected abstract void writeToILFile(java.io.BufferedWriter fileWriter) throws IOException;

    /**
     * @return a String representation of this Tree.
     */
    @Override
    public String toString() {
        return "?";
    }
}
