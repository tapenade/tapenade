/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import java.io.IOException;

/**
 * An atom node of a Tree with a String value.
 */
class SAtomTree extends Tree {

    /**
     * The value of this Tree. Only atom Tree's hold values.
     */
    private String value;

    protected SAtomTree(Operator operator) {
        super(operator);
        value = null;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Tree copy() {
        SAtomTree newTree = (SAtomTree) operator().tree();
        newTree.value = value;
        newTree.copyAnnotations(this);
        return newTree;
    }

    @Override
    public boolean isAtom() {
        return true;
    }

    @Override
    public boolean isStringAtom() {
        return true;
    }

    @Override
    public boolean isIntAtom() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public int length() {
        return 0;
    }

    @Override
    public Tree[] children() {
        return null;
    }

    @Override
    public String stringValue() {
        return value;
    }

    @Override
    public boolean equalsTree(Tree other) {
        if (other == null || operator() != other.operator()) {
            return false;
        }
        if (value == null) {
            return ((SAtomTree) other).value == null;
        } else {
            return value.equals(((SAtomTree) other).value);
        }
    }

    @Override
    public void writeToILFile(java.io.BufferedWriter fileWriter) throws IOException {
        operator().writeToILFile(fileWriter);
        fileWriter.write(value + System.lineSeparator());
    }

    @Override
    public String toString() {
        return operator()+":"+value ;
    }

}
