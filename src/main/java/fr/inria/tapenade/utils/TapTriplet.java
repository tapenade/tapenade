/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * Triplet of Objects.
 */
public final class TapTriplet<F, S, T> {

    /**
     * The first object in the triplet.
     */
    public F first;

    /**
     * The second object in the triplet.
     */
    public S second;

    /**
     * The third object in the triplet.
     */
    public T third;

    /**
     * Create a triplet containing "first", "second", and "third".
     */
    public TapTriplet(F first, S second, T third) {
        super();
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public String toString() {
        return "{1:" + first + " 2:" + second + " 3:" + third + '}';
    }
}
