/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;

/**
 * A vector of booleans, indexed by an integer rank from 0 up.
 */
public class BoolVector {

    protected static final short B_SET_SIZE = 32;
    protected static final int B_SET_ONE = 1;
    protected static final int B_SET_ZERO = 0;
    protected static final int B_SET_ALL = 0xFFFFFFFF; // == -1 !
    /**
     * Bitset version of this Boolvector's contents.
     * Array of int's that contain the booleans (32 each).
     */
    protected int[] bSets;

    // Unclean, rarely useful.
    public int grossLength() {return bSets.length*B_SET_SIZE ;}

    /**
     * The length of this BoolVector, i.e. its maximun index plus one.
     * This is useful only in sparse representation.
     * In the bitset case we assume it is bSets.length*B_SET_SIZE.
     */
    private int length = -1;

    /**
     * Sparse version of this Boolvector's contents.
     * Note: always make sure this list is ordered from 0 up.
     */
    private TapIntList contents;

    public final boolean isSparse() {
        return contents != null;
    }

    /**
     * Creates a new BoolVector containing "size" booleans.
     * Each boolean is initialized to false.
     */
    public BoolVector(int size) {
        super();
        int nbSet = ((size - 1) / B_SET_SIZE) + 1;
        bSets = new int[nbSet];
        setFalse();
    }

    /**
     * Creates a new BoolVector containing "size" booleans.
     * Each boolean is initialized to false.
     */
    public BoolVector(boolean sparse, int size) {
        super();
        if (sparse) {
            length = size;
            contents = new TapIntList(-1, null);
        } else {
            int nbSet = ((size - 1) / B_SET_SIZE) + 1;
            bSets = new int[nbSet];
            setFalse();
        }
    }

    /**
     * @return in the leftmost bits of the result a copy of the
     * bits [fromBit;toBit[ of the given int origInt.
     * fromBit and toBit-1 are between 0 and bSetSize-1 (e.g. 31).
     * The remaining bits of the result are set to 0.
     */
    protected static int getSubInt(int origInt, int fromBit, int toBit) {
        // Attention, for some unknown reason, shifts >>> and << seem to go in the opposite direction ?
        return origInt >>> fromBit & B_SET_ALL >>> B_SET_SIZE - toBit + fromBit;
    }

    /**
     * @return a bitwise copy of the given "origInt", in which
     * bits [fromBit;toBit[ have been replaced by the leftmost bits
     * of the given "cpInt".
     */
    protected static int setSubInt(int origInt, int cpInt, int fromBit, int toBit) {
        int mask = 0;
        if (fromBit > 0) {
            mask |= B_SET_ALL >>> B_SET_SIZE - fromBit;
        }
        if (toBit < B_SET_SIZE) {
            mask |= B_SET_ALL << toBit;
        }
        int result = origInt & mask;
        mask = cpInt << fromBit & B_SET_ALL >>> B_SET_SIZE - toBit;
        result = result | mask;
        return result;
    }

    /**
     * @return a bitwise copy of the given "origInt", in which
     * bits [fromBit;toBit[ have been OR'ed with the leftmost bits
     * of the given "orInt".
     */
    private static int orSubInt(int origInt, int orInt, int fromBit, int toBit) {
        return origInt | orInt << fromBit & B_SET_ALL << fromBit;
    }

    /**
     * Resets the whole BoolVector to false.
     */
    public final void setFalse() {
        if (isSparse()) {
            contents.tail = null;
        } else {
            for (int i = bSets.length - 1; i >= 0; i--) {
                bSets[i] = B_SET_ZERO;
            }
        }
    }

    /**
     * Resets the whole BoolVector to true.
     */
    public final void setTrue() {
        if (isSparse()) {
            TapEnv.toolWarning(-1, "BoolVector.setTrue() on a sparse representation");
            contents.tail = null;
            for (int i = length - 1; i >= 0; --i) {
                contents.placdl(i);
            }
        } else {
            for (int i = bSets.length - 1; i >= 0; i--) {
                bSets[i] = B_SET_ALL;
            }
        }
    }

    /**
     * Sets the boolean value at rank "rank" to "val".
     */
    public final void set(int rank, boolean val) {
        if (isSparse()) {
            TapIntList inContents = contents;
            while (inContents.tail != null && inContents.tail.head < rank) {
                inContents = inContents.tail;
            }
            if (val) {
                if (inContents.tail == null || inContents.tail.head != rank) {
                    inContents.placdl(rank);
                }
            } else {
                if (inContents.tail != null && inContents.tail.head == rank) {
                    inContents.tail = inContents.tail.tail;
                }
            }
        } else {
            if (rank >= 0) {
                int setRank = rank / B_SET_SIZE;
                int mask = /*maskOneLeft[rank%bSetSize]*/ B_SET_ONE << rank % B_SET_SIZE;
                if (val) {
                    bSets[setRank] = bSets[setRank] | mask;
                } else {
                    bSets[setRank] = bSets[setRank] & ~mask;
                }
            }
        }
    }

    /**
     * Sets all indexes in "indexes" to boolean "val".
     */
    public final void set(TapIntList indexes, boolean val) {
        while (indexes != null) {
            set(indexes.head, val);
            indexes = indexes.tail;
        }
    }

    /**
     * @return the boolean value at rank "rank".
     */
    public final boolean get(int rank) {
        if (isSparse()) {
            return TapIntList.contains(contents.tail, rank);
        } else {
            int setRank = (rank >= 0 ? rank / B_SET_SIZE : -1); // Negative rank will make get() crash, which is what we want!
            int mask = B_SET_ONE << rank % B_SET_SIZE;
            return (bSets[setRank] & mask) != B_SET_ZERO;
        }
    }

    /**
     * Creates a new copy of this BoolVector.
     */
    public final BoolVector copy() {
        BoolVector result =
                new BoolVector(this.isSparse(),
                        (this.isSparse() ? this.length : bSets.length * B_SET_SIZE));
        result.setCopy(this);
        return result;
    }

    /**
     * Same as copy(), but creates a Boolvector of length "finalLen",
     * and copies only the "len" first bits.
     * If finalLen is relatively smaller, truncates.
     * If finalLen is relatively larger, pads with 0's.
     */
    public final BoolVector copy(int len, int finalLen) {
        BoolVector result = new BoolVector(this.isSparse(), finalLen);
        result.setCopy(this, Math.min(len, finalLen));
        return result;
    }

    /**
     * Sets the boolean values just like in the "model" BoolVector.
     */
    public final void setCopy(BoolVector model) {
        if (isSparse()) {
            contents.tail = TapIntList.copy(model.contents.tail);
        } else {
            int maxIdx = StrictMath.min(bSets.length, model.bSets.length);
            if (maxIdx >= 0) {
                System.arraycopy(model.bSets, 0, bSets, 0, maxIdx);
            }
        }
    }

    /**
     * Sets the boolean values just like in the "model" BoolVector,
     * only for the "len" first bits. The remaining bits are set to false.
     */
    public final void setCopy(BoolVector model, int len) {
        if (isSparse()) {
            contents.tail = null;
            TapIntList inCopy = contents;
            TapIntList inContents = model.contents.tail;
            while (inContents != null) {
                if (inContents.head < len) {
                    inCopy = inCopy.placdl(inContents.head);
                    inContents = inContents.tail;
                } else {
                    inContents = null;
                }
            }
        } else {
            int maxk = len / B_SET_SIZE;
            if (len % B_SET_SIZE != 0) {
                int mask = ~(B_SET_ALL << len % B_SET_SIZE);
                bSets[maxk] = model.bSets[maxk] & mask;
            }
            while (--maxk >= 0) {
                bSets[maxk] = model.bSets[maxk];
            }
        }
    }

    /**
     * Or-accumulates BoolVector "other" into this BoolVector.
     */
    public final void cumulOr(final BoolVector other) {
        if (isSparse()) {
            if (other.isSparse()) {
                TapIntList inContents = contents;
                TapIntList inOther = other.contents.tail;
                while (inOther != null) {
                    while (inContents.tail != null && inContents.tail.head < inOther.head) {
                        inContents = inContents.tail;
                    }
                    if (inContents.tail == null || inContents.tail.head > inOther.head) {
                        inContents = inContents.placdl(inOther.head);
                    }
                    inOther = inOther.tail;
                }
            } else {
                TapIntList inContents = contents;
                for (int i = 0; i < length; ++i) {
                    if (inContents.tail == null || inContents.tail.head > i) {
                        if (other.get(i)) {
                            inContents = inContents.placdl(i);
                        }
                    } else {
                        inContents = inContents.tail;
                    }
                }
            }
        } else {
            if (other.isSparse()) {
                TapIntList inOther = other.contents.tail;
                while (inOther != null) {
                    set(inOther.head, true);
                    inOther = inOther.tail;
                }
            } else {
                for (int i = bSets.length - 1; i >= 0; --i) {
                    bSets[i] |= other.bSets[i];
                }
            }
        }
    }

    /**
     * Same as cumulOr(BoolVector), but only for the "len" first bits.
     */
    public final void cumulOr(final BoolVector other, int len) {
        if (isSparse()) {
            if (other.isSparse()) {
                TapIntList inContents = contents;
                TapIntList inOther = other.contents.tail;
                while (inOther != null) {
                    if (inOther.head >= len) {
                        inOther = null;
                    } else {
                        while (inContents.tail != null && inContents.tail.head < inOther.head) {
                            inContents = inContents.tail;
                        }
                        if (inContents.tail == null || inContents.tail.head > inOther.head) {
                            inContents = inContents.placdl(inOther.head);
                        }
                        inOther = inOther.tail;
                    }
                }
            } else {
                TapIntList inContents = contents;
                for (int i = 0; i < len; ++i) {
                    if (inContents.tail == null || inContents.tail.head > i) {
                        if (other.get(i)) {
                            inContents = inContents.placdl(i);
                        }
                    } else {
                        inContents = inContents.tail;
                    }
                }
            }
        } else {
            if (other.isSparse()) {
                TapIntList inOther = other.contents.tail;
                while (inOther != null && inOther.head < len) {
                    set(inOther.head, true);
                    inOther = inOther.tail;
                }
            } else {
                int maxk = len / B_SET_SIZE;
                if (len % B_SET_SIZE != 0) {
                    int mask = ~(B_SET_ALL << len % B_SET_SIZE);
                    bSets[maxk] |= other.bSets[maxk] & mask;
                }
                while (--maxk >= 0) {
                    bSets[maxk] |= other.bSets[maxk];
                }
            }
        }
    }

    /**
     * Or-accumulates "len" bits of BoolVector "addedVect", starting at bit "addedStart",
     * into this BoolVector, starting at bit "start".
     */
    public final void cumulOr(final BoolVector addedVect, int addedStart, int start, int len) {
        if (isSparse()) {
            TapEnv.toolError("cumulOr() (with offset) not yet implemented on sparse Boolvector");
        } else {
            if (addedVect.isSparse()) {
                TapEnv.toolError("dense.cumulOr(sparse) (with offset) not yet implemented");
            }
            int maxlen = bSets.length * B_SET_SIZE - start;
            if (len > maxlen) {
                len = maxlen;
            }
            maxlen = addedVect.bSets.length * B_SET_SIZE - addedStart;
            if (len > maxlen) {
                len = maxlen;
            }
            int sz;
            while (len > 0) {
                sz = B_SET_SIZE - start % B_SET_SIZE;
                if (sz > B_SET_SIZE - addedStart % B_SET_SIZE) {
                    sz = B_SET_SIZE - addedStart % B_SET_SIZE;
                }
                if (sz > len) {
                    sz = len;
                }
                if (sz > 0) {
                    bSets[start / B_SET_SIZE] =
                            orSubInt(bSets[start / B_SET_SIZE],
                                    getSubInt(addedVect.bSets[addedStart / B_SET_SIZE],
                                            addedStart % B_SET_SIZE,
                                            addedStart % B_SET_SIZE + sz),
                                    start % B_SET_SIZE,
                                    start % B_SET_SIZE + sz);
                    addedStart += sz;
                    start += sz;
                    len -= sz;
                }
            }
        }
    }

    /**
     * Or-accumulates BoolVector "other" into this BoolVector.
     *
     * @return true iff the accumulated BoolVector has more "true" elements.
     */
    public final boolean cumulOrGrows(BoolVector other) {
        boolean grows = false;
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.cumulOrGrows(dense) not yet implemented");
            }
            TapIntList inContents = contents;
            TapIntList inOther = other.contents.tail;
            while (inOther != null) {
                while (inContents.tail != null && inContents.tail.head < inOther.head) {
                    inContents = inContents.tail;
                }
                if (inContents.tail == null || inContents.tail.head > inOther.head) {
                    inContents = inContents.placdl(inOther.head);
                    grows = true;
                }
                inOther = inOther.tail;
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulOrGrows(sparse) not yet implemented");
            }
            int oldVal;
            int i = other.bSets.length;
            if (i > bSets.length) {
                i = bSets.length;
            }
            while (--i >= 0) {
                oldVal = bSets[i];
                bSets[i] |= other.bSets[i];
                if (bSets[i] != oldVal) {
                    grows = true;
                }
            }
        }
        return grows;
    }

    /**
     * Or-accumulates BoolVector "other" into this BoolVector.
     * Same as cumulOr(BoolVector), but only for the "len" first bits,
     * and returns true iff the accumulated BoolVector has more "true" elements,
     * again considering only the "len" first bits.
     */
    public final boolean cumulOrGrows(BoolVector other, int len) {
        boolean grows = false;
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.cumulOrGrows(dense, l) not yet implemented");
            }
            TapIntList inContents = contents;
            TapIntList inOther = other.contents.tail;
            while (inOther != null) {
                if (inOther.head >= len) {
                    inOther = null;
                } else {
                    while (inContents.tail != null && inContents.tail.head < inOther.head) {
                        inContents = inContents.tail;
                    }
                    if (inContents.tail == null || inContents.tail.head > inOther.head) {
                        inContents = inContents.placdl(inOther.head);
                        grows = true;
                    }
                    inOther = inOther.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulOrGrows(sparse, l) not yet implemented");
            }
            int maxk = len / B_SET_SIZE;
            int oldVal;
            if (len % B_SET_SIZE != 0) {
                int mask = ~(B_SET_ALL << len % B_SET_SIZE);
                oldVal = bSets[maxk] & mask;
                bSets[maxk] |= other.bSets[maxk] & mask;
                if ((bSets[maxk] & mask) != oldVal) {
                    grows = true;
                }
            }
            while (--maxk >= 0) {
                oldVal = bSets[maxk];
                bSets[maxk] |= other.bSets[maxk];
                if (bSets[maxk] != oldVal) {
                    grows = true;
                }
            }
        }
        return grows;
    }

    /**
     * And-accumulates BoolVector "other" into this BoolVector.
     */
    public final void cumulAnd(BoolVector other) {
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.cumulAnd(dense) not yet implemented");
            }
            TapIntList inContents = contents;
            TapIntList inOther = other.contents.tail;
            while (inOther != null) {
                while (inContents.tail != null && inContents.tail.head < inOther.head) {
                    inContents.tail = inContents.tail.tail;
                }
                if (inContents.tail == null) {
                    inOther = null;
                } else {
                    if (inContents.tail.head == inOther.head) {
                        inContents = inContents.tail;
                    } else {
                        inContents.tail = inContents.tail.tail;
                    }
                    inOther = inOther.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulAnd(sparse) not yet implemented");
            }
            for (int i = bSets.length - 1; i >= 0; i--) {
                bSets[i] &= other.bSets[i];
            }
        }
    }

    /**
     * Same as cumulAnd(BoolVector), but only for the "len" first bits.
     */
    public final void cumulAnd(BoolVector other, int len) {
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.cumulAnd(dense, l) not yet implemented");
            }
            TapIntList inContents = contents;
            TapIntList inOther = other.contents.tail;
            while (inOther != null) {
                if (inOther.head >= len) {
                    inOther = null;
                } else {
                    while (inContents.tail != null && inContents.tail.head < inOther.head) {
                        inContents.tail = inContents.tail.tail;
                    }
                    if (inContents.tail == null) {
                        inOther = null;
                    } else {
                        if (inContents.tail.head == inOther.head) {
                            inContents = inContents.tail;
                        } else {
                            inContents.tail = inContents.tail.tail;
                        }
                        inOther = inOther.tail;
                    }
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulAnd(sparse, l) not yet implemented");
            }
            int maxk = len / B_SET_SIZE;
            if (len % B_SET_SIZE != 0) {
                int mask = B_SET_ALL << len % B_SET_SIZE;
                bSets[maxk] &= other.bSets[maxk] | mask;
            }
            while (--maxk >= 0) {
                bSets[maxk] &= other.bSets[maxk];
            }
        }
    }

    /**
     * And-accumulates the negation of BoolVector "other" into this BoolVector.
     * In other words: "this"="this" AND NOT "other".
     */
    public final void cumulMinus(BoolVector other) {
        if (isSparse()) {
            TapEnv.toolError("cumulMinus() not yet implemented on sparse BoolVector");
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulMinus(sparse) not yet implemented");
            }
            for (int i = bSets.length - 1; i >= 0; i--) {
                bSets[i] &= ~other.bSets[i];
            }
        }
    }

    /**
     * Same as cumulMinus(BoolVector), but only for the "len" first bits.
     */
    public final void cumulMinus(BoolVector other, int len) {
        if (isSparse()) {
            TapEnv.toolError("cumulMinus() not yet implemented on sparse BoolVector");
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.cumulMinus(sparse, l) not yet implemented");
            }
            int maxk = len / B_SET_SIZE;
            if (len % B_SET_SIZE != 0) {
                int mask = B_SET_ALL << len % B_SET_SIZE;
                bSets[maxk] &= ~other.bSets[maxk] | mask;
            }
            while (--maxk >= 0) {
                bSets[maxk] &= ~other.bSets[maxk];
            }
        }
    }

    /**
     * @return a new BoolVector containing logical AND
     * between this BoolVector and the "other".
     */
    public final BoolVector and(BoolVector other) {
        BoolVector resVector = new BoolVector(isSparse(), (this.isSparse() ? this.length : 0));
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.and(dense) not yet implemented");
            }
            TapIntList inContents = contents.tail;
            TapIntList inOther = other.contents.tail;
            TapIntList inNewContents = resVector.contents;
            while (inContents != null && inOther != null) {
                while (inContents != null && inContents.head < inOther.head) {
                    inContents = inContents.tail;
                }
                if (inContents != null && inContents.head == inOther.head) {
                    inNewContents = inNewContents.placdl(inContents.head);
                    inContents = inContents.tail;
                }
                inOther = inOther.tail;
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.and(sparse) not yet implemented");
            }
            resVector.bSets = new int[bSets.length];
            for (int i = bSets.length - 1; i >= 0; i--) {
                resVector.bSets[i] = bSets[i] & other.bSets[i];
            }
        }
        return resVector;
    }

    /**
     * @return a new BoolVector containing logical OR
     * between this BoolVector and the "other".
     */
    public final BoolVector or(BoolVector other) {
        BoolVector resVector = new BoolVector(isSparse(), (this.isSparse() ? this.length : 0));
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.or(dense) not yet implemented");
            }
            TapIntList inContents = contents.tail;
            TapIntList inOther = other.contents.tail;
            TapIntList inNewContents = resVector.contents;
            while (inContents != null || inOther != null) {
                while (inContents != null && (inOther == null || inContents.head < inOther.head)) {
                    inNewContents = inNewContents.placdl(inContents.head);
                    inContents = inContents.tail;
                }
                if (inContents != null && inOther != null && inContents.head == inOther.head) {
                    inContents = inContents.tail;
                }
                if (inOther != null) {
                    inNewContents = inNewContents.placdl(inOther.head);
                    inOther = inOther.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.or(sparse) not yet implemented");
            }
            resVector.bSets = new int[bSets.length];
            for (int i = bSets.length - 1; i >= 0; i--) {
                resVector.bSets[i] = bSets[i] | other.bSets[i];
            }
        }
        return resVector;
    }

    /**
     * @return a new BoolVector containing logical MINUS (i.e. AND NOT)
     * between this BoolVector and the other.
     */
    public final BoolVector minus(BoolVector other) {
        BoolVector resVector = new BoolVector(isSparse(), (this.isSparse() ? this.length : 0));
        if (isSparse()) {
            TapEnv.toolError("minus() not yet implemented on sparse BoolVector");
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.minus(sparse) not yet implemented");
            }
            resVector.bSets = new int[bSets.length];
            for (int i = bSets.length - 1; i >= 0; --i) {
                resVector.bSets[i] = bSets[i] & ~(other.bSets[i]);
            }
        }
        return resVector;
    }

    /**
     * @return a new BoolVector containing logical XOR
     * between this BoolVector and other.
     */
    public final BoolVector xor(BoolVector other) {
        BoolVector resVector = new BoolVector(isSparse(), (this.isSparse() ? this.length : 0));
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.xor(dense) not yet implemented");
            }
            TapIntList inContents = contents.tail;
            TapIntList inOther = other.contents.tail;
            TapIntList inNewContents = resVector.contents;
            while (inContents != null || inOther != null) {
                while (inContents != null && (inOther == null || inContents.head < inOther.head)) {
                    inNewContents = inNewContents.placdl(inContents.head);
                    inContents = inContents.tail;
                }
                if (inContents != null && inOther != null) {
                    if (inContents.head == inOther.head) {
                        inContents = inContents.tail;
                    } else {
                        inNewContents = inNewContents.placdl(inOther.head);
                    }
                    inOther = inOther.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.xor(sparse) not yet implemented");
            }
            resVector.bSets = new int[bSets.length];
            for (int i = bSets.length - 1; i >= 0; i--) {
                resVector.bSets[i] = bSets[i] ^ other.bSets[i];
            }
        }
        return resVector;
    }

    /**
     * @return a new BoolVector containing logical NOT
     * of this BoolVector.
     */
    public final BoolVector not() {
        BoolVector resVector = new BoolVector(isSparse(), (this.isSparse() ? this.length : 0));
        if (isSparse()) {
            TapEnv.toolError("not() not yet implemented on sparse BoolVector");
        } else {
            resVector.bSets = new int[bSets.length];
            for (int i = bSets.length - 1; i >= 0; i--) {
                resVector.bSets[i] = bSets[i] ^ B_SET_ALL;
            }
        }
        return resVector;
    }

    /**
     * Converts into the TapIntList of all the indices
     * for which this BoolVector holds true.
     */
    public final TapIntList trueIndexList(int size) {
        TapIntList result = null;
        if (isSparse()) {
            result = TapIntList.copy(contents.tail);
        } else {
            for (int i = size - 1; i >= 0; i--) {
                if (get(i)) {
                    result = new TapIntList(i, result);
                }
            }
        }
        return result;
    }

    /**
     * @return true if for each index in "indices"
     * this BoolVector holds the value "true".
     */
    public final boolean contains(TapIntList indices) {
        if (isSparse()) {
            return TapIntList.sortedContains(contents.tail, indices);
        } else {
            boolean contains = true;
            while (contains && indices != null) {
                if (!get(indices.head)) {
                    contains = false;
                }
                indices = indices.tail;
            }
            return contains;
        }
    }

    /**
     * @return true if there exists one index in "indices" for
     * which this BoolVector holds the value "true".
     */
    public final boolean intersects(TapIntList indices) {
        if (isSparse()) {
            return TapIntList.sortedIntersects(contents.tail, indices);
        } else {
            boolean intersects = false;
            while (!intersects && indices != null) {
                if (get(indices.head)) {
                    intersects = true;
                }
                indices = indices.tail;
            }
            return intersects;
        }
    }

    /**
     * @param other The other BoolVector that must intersect this.
     * @param len   The number of booleans on which the test is restricted.
     * @return true if this intersects "other".
     */
    public final boolean intersects(final BoolVector other, final int len) {
        boolean intersects = false;
        if (isSparse()) {
            if (other.isSparse()) {
                TapIntList inContents = contents.tail;
                TapIntList inOther = other.contents.tail;
                while (!intersects && inContents != null && inOther != null && inContents.head < len) {
                    while (inContents != null && inContents.head < inOther.head) {
                        inContents = inContents.tail;
                    }
                    if (inContents != null && inContents.head < len && inContents.head == inOther.head) {
                        intersects = true;
                    }
                    inOther = inOther.tail;
                }
            } else {
                TapIntList inContents = contents.tail;
                while (!intersects && inContents != null && inContents.head < len) {
                    intersects = other.get(inContents.head);
                    inContents = inContents.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.intersects(sparse, l) not yet implemented");
            }
            final int lastBits = len % B_SET_SIZE;
            int i = len / B_SET_SIZE;
            if (lastBits != 0) {
                intersects = ((bSets[i] & other.bSets[i]) << (B_SET_SIZE - lastBits)) != B_SET_ZERO;
            }
            while (!intersects && (--i) >= 0) {
                intersects = (bSets[i] & other.bSets[i]) != B_SET_ZERO;
            }
        }
        return intersects;
    }

    /**
     * @param other The other BoolVector that must be contained in this.
     * @param len   The number of booleans on which the test is restricted.
     * @return true if this BoolVector contains (or is equal to) the "other"
     * BoolVector, considering only the "len" first bits.
     * Same semantics as: this.and(other).equals(other,len)
     */
    public final boolean contains(final BoolVector other, final int len) {
        boolean contains = true;
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.contains(dense, l) not yet implemented");
            }
            TapIntList inContents = contents.tail;
            TapIntList inOther = other.contents.tail;
            while (contains && inOther != null && inOther.head < len) {
                while (inContents != null && inContents.head < inOther.head) {
                    inContents = inContents.tail;
                }
                if (inContents == null || (inContents.head < len && inContents.head != inOther.head)) {
                    contains = false;
                }
                inOther = inOther.tail;
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.contains(sparse, l) not yet implemented");
            }
            final int lastBits = len % B_SET_SIZE;
            int i = len / B_SET_SIZE;
            if (lastBits != 0) {
                contains = (((bSets[i] & other.bSets[i]) << (B_SET_SIZE - lastBits))
                        == (other.bSets[i] << (B_SET_SIZE - lastBits)));
            }
            while (contains && (--i) >= 0) {
                contains = (bSets[i] & other.bSets[i]) == other.bSets[i];
            }
        }
        return contains;
    }

    /**
     * @param other The other BoolVector
     * @param len   The number of booleans on which the test is restricted.
     * @return The number of differences.
     */
    public final int distance(final BoolVector other, final int len) {
        BoolVector diffVector = xor(other);
        int count = 0;
        if (isSparse()) {
            count = TapIntList.length(diffVector.contents.tail);
        } else {
            int maxk = len / B_SET_SIZE;
            int reste = len % B_SET_SIZE;
            if (reste != 0) {
                count += nbOnes(diffVector.bSets[maxk], reste);
            }
            while (--maxk >= 0) {
                count += nbOnes(diffVector.bSets[maxk], B_SET_SIZE);
            }
        }
        return count;
    }

    /**
     * @return the number of "1" in the "len" first bits of
     * the binary representation of the given integer.
     */
    private static int nbOnes(int boolInt, int len) {
        int count = 0;
        while (--len >= 0) {
            if ((boolInt & B_SET_ONE) != B_SET_ZERO) {
                ++count;
            }
            boolInt = boolInt >> 1;
        }
        return count;
    }

    /**
     * @param other The other BoolVector that must be equal to this.
     * @param len   The number of booleans on which the test is restricted.
     * @return true if this is equal to "other".
     */
    public final boolean equals(final BoolVector other, final int len) {
        boolean areEqual = true;
        if (isSparse()) {
            if (!other.isSparse()) {
                TapEnv.toolError("sparse.equals(dense, l) not yet implemented");
            }
            TapIntList inContents = contents.tail;
            TapIntList inOther = other.contents.tail;
            while (areEqual
                    && !((inContents == null || inContents.head >= len)
                    && (inOther == null || inOther.head >= len))) {
                if (inContents == null || inContents.head >= len
                        || inOther == null || inOther.head >= len
                        || inContents.head != inOther.head) {
                    areEqual = false;
                } else {
                    inContents = inContents.tail;
                    inOther = inOther.tail;
                }
            }
        } else {
            if (other.isSparse()) {
                TapEnv.toolError("dense.equals(sparse, l) not yet implemented");
            }
            final int lastBits = len % B_SET_SIZE;
            int i = len / B_SET_SIZE;
            if (lastBits != 0) {
                areEqual = (bSets[i] << (B_SET_SIZE - lastBits)) == (other.bSets[i] << (B_SET_SIZE - lastBits));
            }
            while (areEqual && (--i) >= 0) {
                areEqual = bSets[i] == other.bSets[i];
            }
        }
        return areEqual;
    }

    /**
     * @param len The number of booleans on which the test is restricted.
     * @return true if this is full of 0.
     */
    public final boolean isFalse(final int len) {
        boolean isEmpty = true;
        if (isSparse()) {
            isEmpty = (contents.tail == null || contents.tail.head >= len);
        } else {
            final int lastBits = len % B_SET_SIZE;
            int i = len / B_SET_SIZE;
            if (lastBits != 0) {
                isEmpty = (bSets[i] << (B_SET_SIZE - lastBits)) == B_SET_ZERO;
            }
            while (isEmpty && (--i) >= 0) {
                isEmpty = bSets[i] == B_SET_ZERO;
            }
        }
        return isEmpty;
    }

    public final int maxTrueRank(int len) {
        if (isSparse()) {
            TapEnv.toolError("maxTrueRank() not yet implemented on sparse BoolVector");
            return -1;
        } else {
            while (len > 0 && !get(len - 1)) {
                --len;
            }
            return len - 1;
        }
    }

    // End methods a little too specific.

    /**
     * (for debugging) @return the number of "true" elements in this vector.
     */
    public final int numberOfOnes(int length) {
        if (isSparse()) {
            return TapIntList.length(contents.tail);
        } else {
            int number = 0;
            for (int i = length - 1; i >= 0; --i) {
                if (get(i)) {
                    ++number;
                }
            }
            return number;
        }
    }

    /**
     * Prints in detail the contents of this BoolVector, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    /**
     * Prints in detail the contents of this BoolVector, knowing its exact length,
     * onto TapEnv.curOutputStream().
     *
     * @param length the exact length of this BoolVector.
     */
    public void dump(int length) throws java.io.IOException {
        TapEnv.print(toString(length));
    }

    /**
     * Prints in detail the contents of this BoolVector, knowing its Map,
     * onto TapEnv.curOutputStream().
     *
     * @param map the map of this BoolVector.
     */
    public void dump(int[] map) throws java.io.IOException {
        TapEnv.print(toString(map));
    }

    /**
     * Builds a String describing the exact contents of this BoolVector,
     * knowing its map.
     */
    public final String toString(int[] map) {
        StringBuilder res = new StringBuilder("[");
        int offset;
        int ksz;
        for (int k = 1; k < map.length; ++k) {
            if (k > 1) {
                res.append('|');
            }
            offset = map[k - 1];
            ksz = map[k] - offset;
            for (int i = 0; i < ksz; ++i) {
                if (!isSparse() && i > 0 && i % 10 == 0) {
                    res.append('(').append(i / 10 % 10).append(')');
                }
                if (isSparse()) {
                    if (get(offset + i)) {
                        res.append(i).append(' ');
                    }
                } else {
                    res.append(get(offset + i) ? 'X' : '.');
                }
            }
        }
        return res + "]";
    }

    /**
     * Builds a String describing the exact contents of this BoolVector,
     * knowing its exact size.
     */
    public final String toString(int size) {
        if (size==0) {
            return "0vector" ;
        } else if (isSparse()) {
            return contents.tail.toString();
        } else {
            int len = size + 3 * (size / 10);
            int i;
            StringBuilder res = new StringBuilder(len);
            for (i = 0; i < size; i++) {
                if (i > 0 && i % 10 == 0) {
                    res.append('(');
                    res.append(i / 10 % 10);
                    res.append(')');
                }
                res.append((get(i) ? '1' : '.'));
            }
            return res.toString();
        }
    }

    /**
     * Builds a String describing the exact contents of this BoolVector,
     * without knowing its exact length or its map.
     * Therefore, this displays some multiple of 32 booleans,
     * including meaningless trailing booleans.
     */
    @Override
    public final String toString() {
        return toString(isSparse() ? length : B_SET_SIZE * bSets.length);
    }

}








//     /**
//      * Make a BoolVector larger or smaller, dropping stuff if necessary.
//      */
//     public final void resize(int size) {
//         if (isSparse()) {
//             TapEnv.toolError("resize() not yet implemented on sparse BoolVector");
//             return;
//         }
//         int nbSet = (size - 1) / B_SET_SIZE + 1;
//         if (nbSet != bSets.length) {
//             int nbCopy = Math.min(nbSet, bSets.length);
//             int[] oldBSets = bSets;
//             bSets = new int[nbSet];
//             System.arraycopy(oldBSets, 0, bSets, 0, nbCopy);
//             for (int i = nbCopy; i < nbSet; i++) {
//                 bSets[i] = B_SET_ZERO;
//             }
//         }
//     }

//     /**
//      * Copy the nBits starting at fromBit into location starting at toBit.
//      * The nBits are guaranteed to fit in a single int after fromBit and toBit.
//      */
//     private final void moveSubInt(int fromBit, int toBit, int nBits) {
//         bSets[toBit / B_SET_SIZE] =
//                 setSubInt(bSets[toBit / B_SET_SIZE],
//                         getSubInt(bSets[fromBit / B_SET_SIZE],
//                                 fromBit % B_SET_SIZE,
//                                 fromBit % B_SET_SIZE + nBits),
//                         toBit % B_SET_SIZE,
//                         toBit % B_SET_SIZE + nBits);
//     }

//     /**
//      * Move nBits bits starting at location fromBit
//      * into the location that starts at toBit.
//      */
//     public final void move(int fromBit, int toBit, int nBits) {
//         if (isSparse()) {
//             TapEnv.toolError("move() not yet implemented on sparse BoolVector");
//             return;
//         }
//         int fromSubInt;
//         int toSubInt;
//         int subIntSize;
//         int toIntSize;
//         int max = bSets.length * B_SET_SIZE;
//         if (nBits == -1) {
//             nBits = max - fromBit;
//         }
//         if (nBits > max - fromBit) {
//             nBits = max - fromBit;
//         }
//         if (nBits > max - toBit) {
//             nBits = max - toBit;
//         }
//         if (toBit > fromBit) {
//             /* if moving rightwards, start from the right end: */
//             fromSubInt = fromBit + nBits;
//             toSubInt = toBit + nBits;
//             while (nBits > 0) {
//                 subIntSize = fromSubInt % B_SET_SIZE;
//                 if (subIntSize == 0) {
//                     subIntSize = B_SET_SIZE;
//                 }
//                 toIntSize = toSubInt % B_SET_SIZE;
//                 if (toIntSize == 0) {
//                     toIntSize = B_SET_SIZE;
//                 }
//                 if (toIntSize < subIntSize) {
//                     subIntSize = toIntSize;
//                 }
//                 if (subIntSize > nBits) {
//                     subIntSize = nBits;
//                 }
//                 if (subIntSize > 0) {
//                     fromSubInt -= subIntSize;
//                     toSubInt -= subIntSize;
//                     nBits -= subIntSize;
//                     moveSubInt(fromSubInt, toSubInt, subIntSize);
//                 }
//             }
//         } else if (toBit < fromBit) {
//             /* if moving leftwards, start from the left end: */
//             fromSubInt = fromBit;
//             toSubInt = toBit;
//             while (nBits > 0) {
//                 subIntSize = B_SET_SIZE - fromSubInt % B_SET_SIZE;
//                 toIntSize = B_SET_SIZE - toSubInt % B_SET_SIZE;
//                 if (toIntSize < subIntSize) {
//                     subIntSize = toIntSize;
//                 }
//                 if (subIntSize > nBits) {
//                     subIntSize = nBits;
//                 }
//                 if (subIntSize > 0) {
//                     moveSubInt(fromSubInt, toSubInt, subIntSize);
//                     fromSubInt += subIntSize;
//                     toSubInt += subIntSize;
//                     nBits -= subIntSize;
//                 }
//             }
//         }
//     }

//     /**
//      * Move all bits after location "fromBit"
//      * into the location that starts at "toBit".
//      * The 1st bit is numbered "0".
//      */
//     public final void move(int fromBit, int toBit) {
//         move(fromBit, toBit, -1);
//     }

//     public void setTest(int zone, int[] curBlockMap, boolean val) {
//         if (zone >= 0) {
//             if (zone < curBlockMap[SymbolTableConstants.K_CLASS]) {
//                 set(zone, val);
//             }
//         }
//     }

//     public void setTest(TapIntList zones, int[] curBlockMap, boolean val) {
//         int zone;
//         while (zones != null) {
//             zone = zones.head;
//             if (zone >= 0) {
//                 if (zone < curBlockMap[SymbolTableConstants.K_CLASS]) {
//                     set(zone, val);
//                 }
//             }
//             zones = zones.tail;
//         }
//     }

//     /**
//      * Get the boolean info stored in this BoolVector for the "test" zone number "zone".
//      */
//     public boolean getTest(int zone, int[] curBlockMap) {
//         return get(zone);
//     }
