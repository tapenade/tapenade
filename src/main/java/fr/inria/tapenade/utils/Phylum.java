/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * A collection of operators, used by operators to specify the allowed operators of their children.
 */
public class Phylum {

    /**
     * The list of allowed operators represented by this Phylum.
     */
    protected final int[] operators;
    /**
     * The name of this Phylum.
     */
    private final String name;

    protected Phylum(String name, int code, int[] operators) {
        super();
        this.name = name;
        this.operators = operators;
    }

    @Override
    public String toString() {
        return "Phylum:" + name;
    }

}
