/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * A simple chronometer to time Tapenade algorithms.
 */
public final class Chrono {

    /**
     * The origin time in milliseconds.
     */
    private long startTime;

    /**
     * Create a new chronometer, initialized now.
     */
    public Chrono() {
        super();
        reset();
    }

    /**
     * Reinitialize this chronometer now.
     */
    public void reset() {
        startTime = System.currentTimeMillis();
    }

    /**
     * @return the elapsed time since initialization, in seconds dot hundred-ths.
     */
    public String elapsed() {
        long decis = (System.currentTimeMillis() - startTime) / 10L;
        long nbs = decis / 100L ;
        int nbds = Math.toIntExact(decis)%100 ;
        return nbs+"."+(nbds>=10?nbds:"0"+nbds) ;
    }
}
