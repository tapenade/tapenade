/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * Pair of Objects.
 */
public final class TapPair<F, S> {

    /**
     * The first object in the pair.
     */
    public F first;

    /**
     * The second object in the pair.
     */
    public S second;

    /**
     * Create a pair containing "first" and "second".
     */
    public TapPair(F first, S second) {
        super();
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "{1:" + first + " 2:" + second + "}";
    }
}
