/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.TapEnv;

/**
 * A matrix of booleans internally stored as
 * an array of BoolVector's (i.e. bit sets).
 * Use conventions to reduce storage:
 * "implicit-zero": {@code rows == null} means matrix is full of "false"
 * "implicit-identity": {@code rows[i] == null} means row i is an "identity" row.
 */
public class BoolMatrix {

    /**
     * The number of rows of this matrix.
     */
    public int nRows;

    /**
     * The number of columns of this matrix.
     */
    public int nCols;

    /**
     * The contents of the matrix, internally stored as an array
     * of BoolVector's, one per row.
     */
    public BoolVector[] rows;

    private boolean sparse = false ;

    /**
     * Creates a new BoolMatrix of size "rowNumber" times "colNumber".
     * This  matrix is initially empty, using "implicit-zero" convention.
     */
    public BoolMatrix(int rowNumber, int colNumber) {
        super();
        nCols = colNumber;
        nRows = rowNumber;
        rows = null;
    }

    public BoolMatrix(boolean sparse, int rowNumber, int colNumber) {
        super();
        this.sparse = sparse;
        nCols = colNumber;
        nRows = rowNumber;
        rows = null;
    }

    private static void displaySepLine(int[] colMap) throws java.io.IOException {
        int cl;
        for (int cc = 1; cc < colMap.length; ++cc) {
            if (cc > 1) {
                TapEnv.print("+");
            }
            cl = colMap[cc] - colMap[cc - 1];
            cl = cl + 3 * ((cl - 1) / 10);
            while (cl > 0) {
                TapEnv.print("-");
                cl--;
            }
        }
    }

    /**
     * Special for "Implicit-Identity" rows of BoolMatrix'es.
     */
    private static void displayIdentLine(int mapClass,
                                         int inClass, int[] colMap) throws java.io.IOException {
        int cl;
        int ci;
        for (int cc = 1; cc < colMap.length; cc++) {
            if (cc > 1) {
                TapEnv.print("|");
            }
            cl = colMap[cc] - colMap[cc - 1];
            cl = cl + 3 * ((cl - 1) / 10);
            if (cc == mapClass || cc == colMap.length - 1 && cc < mapClass) {
                for (ci = 0; ci < cl; ++ci) {
                    TapEnv.print(ci == inClass ? "i" : " ");
                }
            } else {
                for (ci = 0; ci < cl; ++ci) {
                    TapEnv.print(" ");
                }
            }
        }
    }

    private static String implicitIdToString(int boolNumber, int index) {
        int len = boolNumber + 3 * (boolNumber / 10);
        StringBuilder res = new StringBuilder(len);
        for (int i = 0; i < boolNumber; ++i) {
            if (i > 0 && i % 10 == 0) {
                res.append('(');
                res.append(i / 10 % 10);
                res.append(')');
            }
            res.append(i == index ? 'i' : ' ');
        }
        return res.toString();
    }

    /**
     * Special for rows of BoolMatrix'es.
     */
    private static void displayZoneVector(BoolVector row, int[] colMap) throws java.io.IOException {
        int cl;
        int ci;
        for (int cc = 1; cc < colMap.length; cc++) {
            if (cc > 1) {
                TapEnv.print("|");
            }
            cl = colMap[cc] - colMap[cc - 1];
            for (ci = 0; ci < cl; ci++) {
                if (ci > 0 && ci % 10 == 0) {
                    TapEnv.print("(" + ci / 10 % 10 + ')');
                }
                TapEnv.print(row.get(ci + colMap[cc - 1]) ? "X" : ".");
            }
        }
    }

    /**
     * @return the number of rows of this matrix.
     */
    public final int getNRows() {
        return nRows;
    }

    /**
     * @return the number of columns of this matrix.
     */
    public final int getNCols() {
        return nCols;
    }

    /**
     * Create a copy of this matrix.
     */
    public BoolMatrix copy() {
        BoolMatrix result = new BoolMatrix(this.sparse, nRows, nCols);
        if (rows == null) {
            return result;
        }
        result.rows = new BoolVector[nRows];
        for (int i = 0; i < nRows; i++) {
            if (rows[i] != null) {
                result.rows[i] = rows[i].copy();
            }
        }
        return result;
    }

    /**
     * Re-initializes this BoolMatrix explicitly to Zero.
     * Does not use the "implicit-zero" convention.
     */
    public final void setExplicitZero() {
        if (rows == null) {
            rows = new BoolVector[nRows];
        }
        for (int i = nRows - 1; i >= 0; i--) {
            rows[i] = new BoolVector(this.sparse, nCols);
        }
    }

    /**
     * Sets row i of this BoolMatrix explicitly to Zero.
     * Does not use the "implicit-zero" convention.
     * Crashes (rightfully!) if matrix is "implicit-zero".
     */
    public final void setExplicitZeroRow(int i) {
        rows[i] = new BoolVector(this.sparse, nCols);
    }

    /**
     * Re-initializes this BoolMatrix to Identity.
     * Each row of the matrix is initialized to null,
     * using the "implicit-identity" convention.
     */
    public final void setIdentity() {
        if (rows == null) {
            rows = new BoolVector[nRows];
        }
        for (int i = nRows - 1; i >= 0; i--) {
            rows[i] = null;
        }
    }

    /**
     * Re-initializes this BoolMatrix explicitly to Identity.
     * Does not use the "implicit-identity" convention.
     */
    final void setExplicitIdentity() {
        if (rows == null) {
            rows = new BoolVector[nRows];
        }
        for (int i = nRows - 1; i >= 0; i--) {
            rows[i] = new BoolVector(this.sparse, nCols);
            if (i < nCols) {
                rows[i].set(i, true);
            }
        }
    }

    /**
     * Re-initializes row "i" of this BoolMatrix to Identity.
     * Row "i" is set to null, using "implicit-identity" convention.
     * This function assumes that (rows != null).
     */
    public final void setIdentityRow(int i) {
        rows[i] = null;
    }

    /**
     * Re-initializes row "i" of this BoolMatrix explicitly to Identity.
     * Does not use the "implicit-identity" convention.
     * This function assumes that (rows != null).
     */
    public final void setExplicitIdentityRow(int i) {
        rows[i] = new BoolVector(this.sparse, nCols);
        if (i < nCols) {
            rows[i].set(i, true);
        }
    }

    /**
     * Sets row "i" of this BoolMatrix to "row".
     * This function assumes that (rows != null).
     */
    public final void setRow(int i, BoolVector row) {
        if (isImplicitZero()) {
            setExplicitZero();
        }
        rows[i] = row;
    }

    /**
     * Overwrites into this BoolMatrix, so that all rows in "rowIndices"
     * are now set to depend on the given "cumulZ". When "total" is true, this
     * dependence overwrites the previous one, otherwise it just accumulates to it.
     */
    public final void overwriteDeps(TapIntList rowIndices, BoolVector cumulZ, boolean total) {
        int index;
        while (rowIndices != null) {
            index = rowIndices.head;
            if (total) {
                setExplicitZeroRow(index);
            } else if (isImplicitIdentityRow(index)) {
                setExplicitIdentityRow(index);
            }
            if (cumulZ != null) {
                getRow(index).cumulOr(cumulZ);
            }
            rowIndices = rowIndices.tail;
        }
    }

    /**
     * Sets the (i,j) element of this matrix to "val".
     * This function assumes that (rows != null).
     */
    public final void set(int i, int j, boolean val) {
        if (i < nRows && rows[i] == null) {
            // then explicit this "implicit-identity" row:
            rows[i] = new BoolVector(this.sparse, nCols);
            if (i < nCols) {
                rows[i].set(i, true);
            }
        }
        if (i < nRows) {
            rows[i].set(j, val);
        }
    }

    /**
     * @return true if this BoolMatrix is implicitly zero,
     * i.e. using the "implicit-zero" convention.
     */
    public final boolean isImplicitZero() {
        return rows == null;
    }

    /**
     * @return true when this BoolMatrix is full of zero.
     */
    public final boolean isZero(int rowNumber) {
        if (rows == null) {
            return true;
        }
        BoolVector row = rows[rowNumber];
        return row != null && row.isFalse(nCols);
    }

    /**
     * @return true if the row "i" of this BoolMatrix is implicitly
     * identity, i.e. using the "implicit-identity" convention.
     */
    public final boolean isImplicitIdentityRow(int i) {
        return rows != null && rows[i] == null;
    }

    /**
     * @return the TapIntList of the row indices that are Implicit-Identity.
     */
    public final TapIntList getImplicitIdentityIndices() {
        TapIntList result = null;
        for (int i = nRows - 1; i >= 0; i--) {
            if (rows[i] == null) {
                result = new TapIntList(i, result);
            }
        }
        return result;
    }

    /**
     * @return a new BoolVector sized after the rows of this BoolMatrix, holding
     * true for rows that are not implicit-Identity (i.e. that are non null).
     */
    public final BoolVector notImplicitRows() {
        BoolVector result = new BoolVector(nRows);
        for (int i = nRows - 1; i >= 0; i--) {
            if (rows[i] != null) {
                result.set(i, true);
            }
        }
        return result;
    }

    /**
     * @return the i-th row of this BoolMatrix.
     * If rows==null, returns a new empty row.
     */
    public final BoolVector getRow(int i) {
        if (rows == null) {
            return new BoolVector(this.sparse, nCols);
        } else {
            return rows[i];
        }
    }

    /**
     * Same as getRow(), but when the row found is implicit-identity (i.e. null),
     * returns a new explicit identity row.
     */
    public final BoolVector getExplicitRow(int i) {
        BoolVector row = getRow(i);
        if (row == null) {
            row = new BoolVector(this.sparse, nCols);
            if (i < nCols) {
                row.set(i, true);
            }
        }
        return row;
    }

    /**
     * @return the (i,j) element of this BoolMatrix.
     */
    public final boolean get(int i, int j) {
        if (rows == null) {
            return false;
        }
        if (rows[i] == null) {
            return i == j;
        }
        return rows[i].get(j);
    }

    public final void cumulRows(TapIntList rowIndices, BoolVector orVector) {
        int i;
        while (rowIndices != null) {
            i = rowIndices.head;
            if (rows[i] == null) { // if implicit-identity, then explicit it.
                rows[i] = new BoolVector(this.sparse, nCols);
                if (i < nCols) rows[i].set(i, true); //make it identity
            }
            rows[i].cumulOr(orVector);
            rowIndices = rowIndices.tail;
        }
    }

    /**
     * OR-accumulates "orMatrix" into this BoolMatrix.
     * Does something reasonable when the sizes of matrices differ.
     * Takes care of NOT using the dangling bits in the last number of each row of "orMatrix".
     * Tries to preserve the "implicit-zero" and "implicit-identity" conventions.
     *
     * @param orMatrix           The BoolMatrix that must be OR-accumulated into this BoolMatrix.
     * @param explicitIdentities false when expliciting of identity is already done (e.g. for pointers).
     * @param len                Unless equals -1, restricts this accumulation to the "len" first rows.
     * @return                   true iff accumulation has added new "true" elements into this BoolMatrix.
     */
    public final boolean cumulOr(BoolMatrix orMatrix, boolean explicitIdentities, int len) {
        boolean grows = false ;
        if (!orMatrix.isImplicitZero()) {
            if (len == -1) {
                len = Math.min(nRows, orMatrix.nRows);
            }
            int numj = Math.min(nCols, orMatrix.nCols);
            BoolVector thisRow;
            BoolVector orRow;
            // if the accumulation matrix is implicit-zero, explicit it,
            // but remember it because this means that the rows are
            // actually free, and accumulation of an "implicit-identity"
            // into them preserves this "implicit-identity".
            boolean wasImplicitZero = isImplicitZero();
            if (wasImplicitZero) {
                setExplicitZero();
            }
            // now we are sure no matrix is "implicit-zero".
            for (int i = len - 1; i >= 0; --i) {
                thisRow = rows[i];
                orRow = orMatrix.rows[i];
                if (orRow == null) {
                    // If the new row to accumulate is implicit-identity, then:
                    // -- If the accumulator (thisRow) was indeed implicit-zero,
                    //   i.e. no information at all, set it to implicit-identity,
                    // -- Else if accumulator (thisRow) is implicit-identity do nothing,
                    // -- Else set true on the diagonal cell, except if
                    //   explicitIdentities==false, meaning the expliciting of
                    //   identity is already done (this happens for pointers).
                    if (wasImplicitZero) {
                        rows[i] = null;
                    } else if (thisRow != null) {
                        if (i < numj && explicitIdentities) {
                            if (!thisRow.get(i)) grows = true ;
                            thisRow.set(i, true);
                        }
                    }
                } else {
                    // if the new row to accumulate is explicit,
                    // first explicit the original row if it is implicit-identity.
                    if (thisRow == null) {
                        thisRow = rows[i] = new BoolVector(this.sparse, nCols);
                        if (i < numj && !wasImplicitZero && explicitIdentities) {
                            if (!thisRow.get(i)) grows = true ;
                            thisRow.set(i, true);
                        }
                    }
                    // and then accumulate normally the two (now explicit) rows.
                    if (thisRow.cumulOrGrows(orRow, numj)) grows = true ;
                }
            }
        }
        return grows ;
    }

    /**
     * Matrix multiplication into a cumulative matrix.
     * Multiplies the "leftMatrix" with the "rightMatrix", and
     * accumulates the result with a "or" into "this" BoolMatrix.
     * Takes care of preserving the "implicit-identity" convention.
     */
    protected void cumulOrTimes(BoolMatrix leftMatrix, BoolMatrix rightMatrix) {
        if (leftMatrix.rows != null && rightMatrix.rows != null) {
            // if any operand is "implicit-zero", so is the product => do nothing.
            int i;
            int mulNRows;
            BoolVector thisRow;
            BoolVector leftRow;
            BoolVector orRow;
            BoolVector rightRow;
            // if the accumulation matrix is implicit-zero, explicit it,
            // but remember it because this means that the rows are
            // actually free, and accumulation of an "implicit-identity"
            // into them preserves this "implicit-identity".
            boolean wasImplicitZero = isImplicitZero();
            int nColsLeft = leftMatrix.nCols;
            if (wasImplicitZero) {
                setExplicitZero();
            }
            //then build the product, row per row:
            mulNRows = Math.min(leftMatrix.nRows, nRows);
            for (i = mulNRows - 1; i >= 0; i--) {
                thisRow = rows[i];
                leftRow = leftMatrix.rows[i];
                if (leftRow == null) {
                    // this means the leftRow is "implicit-identity";
                    // Take the corresponding row in rightMatrix, to
                    // accumulate it into the accumulation matrix.
                    if (i < rightMatrix.nRows && i < nColsLeft) {
                        orRow = rightMatrix.rows[i];
                        if (orRow == null) {
                            // if the new row to accumulate is implicit-identity,
                            // then if the accumulating row was indeed implicit-zero,
                            // i.e. no information at all, set it to implicit-identity,
                            // otherwise if the accumulating row is implicit-identity, do
                            // nothing, else set true on the diagonal cell.
                            if (wasImplicitZero) {
                                rows[i] = null;
                            } else if (thisRow != null) {
                                if (i < nCols) {
                                    thisRow.set(i, true);
                                }
                            }
                        } else {
                            // if the row to accumulate is explicit, first explicit
                            // the accumulation row if it is implicit-identity:
                            if (thisRow == null) {
                                thisRow = rows[i] = new BoolVector(this.sparse, nCols);
                                if (i < nCols) {
                                    thisRow.set(i, true);
                                }
                            }
                            //and then accumulate normally the two (now explicit) rows:
                            thisRow.cumulOr(orRow);
                        }
                    }
                } else {
                    //else we are sure leftRow is explicit. In that case,
                    // it is good that the accumulation row "thisRow" is explicit too,
                    // even if it might be ultimately "identity", it must be
                    // explicit because this means that this zone is written.
                    if (thisRow == null) {
                        thisRow = rows[i] = new BoolVector(this.sparse, nCols);
                        if (i < nCols) {
                            thisRow.set(i, true);
                        }
                    }
                    for (int j = 0; j < nColsLeft; ++j) {
                        if (leftRow.get(j)) {
                            // then we know leftRow at index "j" is "true".
                            // Accumulate row "j" of rightMatrix (called "rightRow")
                            // into the result's "thisRow":
                            rightRow = rightMatrix.rows[j];
                            if (rightRow == null) {
                                // If the rightRow is just "implicit-identity".
                                // just add a "true" on the result's diagonal:
                                if (j < rightMatrix.nCols) {
                                    thisRow.set(j, true);
                                }
                            } else {
                                // else or-accumulate "rightRow" into "thisRow":
                                thisRow.cumulOr(rightRow);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @return a new BoolMatrix, product of this BoolMatrix with
     * the BoolMatrix "rightMatrix".
     */
    public final BoolMatrix times(BoolMatrix rightMatrix) {
        // The new BoolMatrix that holds the product is created "implicit-zero":
        BoolMatrix product = new BoolMatrix(rightMatrix.sparse, nRows, rightMatrix.nCols);
        // Use the efficient BoolMatrix.cumulOrTimes method:
        product.cumulOrTimes(this, rightMatrix);
        return product;
    }

    /**
     * BoolMatrix * BoolVector multiplication.
     *
     * @return a new BoolVector, containing the product of this BoolMatrix
     * with the BoolVector "rightVector".
     * It makes no sense to accept an "implicit-identity" rightVector,
     * nor to return an "implicit-identity" result, since these
     * vectors are disconnected from any BoolMatrix.
     */
    public final BoolVector times(BoolVector rightVector) {
        BoolVector product = new BoolVector(this.sparse, nRows);
        // if this BoolMatrix is "implicit-zero", the result is a
        // BoolVector of zeros => do nothing and return "product".
        if (rows != null) {
            BoolVector row;
            int i;
            boolean isTrue;
            for (i = 0; i < nRows; i++) {
                // Now compute the i-th element of the product:
                row = rows[i];
                if (row == null) {
                    // row is "implicit-identity" => return the
                    // corresponding element of rightVector.
                    isTrue = i < nCols && rightVector.get(i);
                } else {
                    // row is explicit. Do a bitwise AND of row and rightVector:
                    isTrue = row.intersects(rightVector, nCols);
                }
                if (isTrue) {
                    product.set(i, true);
                }
            }
        }
        return product;
    }

    /**
     * BoolVector * BoolMatrix multiplication.
     *
     * @return a new BoolVector, containing the product of
     * the BoolVector "leftVector" with this BoolMatrix.
     * It makes no sense to accept an "implicit-identity" leftVector,
     * nor to return an "implicit-identity" result, since these
     * vectors are not related to any BoolMatrix.
     */
    public final BoolVector leftTimes(BoolVector leftVector) {
        BoolVector result = new BoolVector(this.sparse, nCols);
        // if this BoolMatrix is "implicit-zero", the result is a
        // BoolVector of zeros => do nothing and return "product".
        if (rows != null) {
            BoolVector rightRow;
            int j;
            for (j = 0; j < nRows; j++) {
                // Here, we did not optimize to spare this "get" as we did
                // in "cumulOrTimes". We think it may not be worth it.
                if (leftVector.get(j)) {
                    rightRow = rows[j];
                    if (rightRow == null) {
                        // rightRow is "implicit-identity" => set "true" in the
                        // corresponding element of result.
                        if (j < nCols) {
                            result.set(j, true);
                        }
                    } else {
                        // rightRow is explicit. OR-accumulate it into result:
                        result.cumulOr(rightRow);
                    }
                }
            }
        }
        return result;
    }

    /**
     * @return a new matrix containing "this" times the transposed of "rightMatrix".
     */
    public final BoolMatrix timesTransposed(BoolMatrix rightMatrix) {
        // The new BoolMatrix that holds the product is created "implicit-zero":
        BoolMatrix product = new BoolMatrix(this.sparse, nRows, rightMatrix.nRows);
        product.cumulOrTimesTransposed(this, rightMatrix);
        return product;
    }

    /**
     * Transposed matrix multiplication into a cumulative matrix.
     * Multiplies the "leftMatrix" with the transposed of "rightMatrix",
     * and accumulates the result with a "or" into "this" BoolMatrix.
     * Usage shows that only the "rightMatrix" may use the "implicit-zero"
     * and we cannot preserve these conventions here. Therefore the
     * result will not use the conventions, and these conventions
     * will have to be restored later (setIdentityRow).
     */
    private void cumulOrTimesTransposed(BoolMatrix leftMatrix, BoolMatrix rightMatrix) {
        if (leftMatrix.rows != null && rightMatrix.rows != null) {
            // if any operand is "implicit-zero", so is the product => do nothing.
            int i;
            int j;
            boolean isTrue;
            BoolVector cumulRow;
            // if the accumulation matrix is implicit-zero, explicit it:
            if (isImplicitZero()) {
                setExplicitZero();
            }
            // For each left row "i":
            for (i = leftMatrix.nRows - 1; i >= 0; i--) {
                cumulRow = rows[i];
                // If cumulRow is "implicit-identity", explicit it:
                if (cumulRow == null) {
                    cumulRow = rows[i] = new BoolVector(this.sparse, nCols);
                    if (i < nCols) {
                        rows[i].set(i, true);
                    }
                }
                //For each right row "j":
                for (j = rightMatrix.nRows - 1; j >= 0; j--) {
                    if (rightMatrix.rows[j] == null) {
                        // If the right row is identity,
                        isTrue = j < leftMatrix.nCols && leftMatrix.rows[i].get(j);
                    } else {
                        // else the right row is explicit. try to find a 1
                        // in the bitwise and of left row i and right row j:
                        isTrue = leftMatrix.rows[i].intersects(rightMatrix.rows[j], leftMatrix.nCols);
                    }
                    // If isTrue, set element (i,j) of the cumul BoolMatrix to true:
                    if (isTrue) {
                        cumulRow.set(j, true);
                    }
                }
            }
        }
    }

    /**
     * @return true when this BoolMatrix is equal to "otherMatrix".
     */
    public final boolean equalsBoolMatrix(BoolMatrix otherMatrix) {
        if (nRows == otherMatrix.nRows && nCols == otherMatrix.nCols) {
            return equalsBoolMatrix(otherMatrix, nRows, nCols);
        } else {
            return false;
        }
    }

    /**
     * @return true when this BoolMatrix is equal to "otherMatrix",
     * on their upper-left square of size "nrows" by "ncols".
     */
    public final boolean equalsBoolMatrix(BoolMatrix otherMatrix, int nrows, int ncols) {
        if (rows == null) {
            return otherMatrix.rows == null;
        } else if (otherMatrix.rows == null) {
            return false;
        }
        boolean equals = true;
        int i = -1;
        while (equals && ++i < nrows) {
            if (rows[i] == null) {
                if (otherMatrix.rows[i] != null) {
                    equals = false;
                }
            } else {
                if (otherMatrix.rows[i] == null) {
                    equals = false;
                } else {
                    equals = rows[i].equals(otherMatrix.rows[i], ncols);
                }
            }
        }
        return equals;
    }

    /**
     * (for debugging) @return the number of "true" elements in this matrix.
     */
    public final int numberOfOnes() {
        int number = 0;
        if (rows != null) {
            for (int i = 0; i < nRows; ++i) {
                if (rows[i] == null) {
                    ++number;
                } else {
                    number += rows[i].numberOfOnes(nCols);
                }
            }
        }
        return number;
    }

    /**
     * Prints in detail the contents of this BoolMatrix, onto TapEnv.curOutputStream().
     * Note: contrary to usage, this method prints a final newLine!
     */
    public void dump() throws java.io.IOException {
        if (nRows==0 || nCols==0) {
            TapEnv.printlnOnTrace(nRows+"x"+nCols+"matrix") ;
        } else if (rows == null) {
            for (int i = 0; i < nRows; ++i) {
                TapEnv.printlnOnTrace("[" + TapEnv.str3(i) + "]: z");
            }
        } else {
            for (int i = 0; i < nRows; ++i) {
                TapEnv.printlnOnTrace("[" + TapEnv.str3(i) + "]: " +
                        (rows[i] == null ? implicitIdToString(nCols, i) : rows[i].toString(nCols)));
            }
        }
    }

    /**
     * Prints in detail the contents of this BoolMatrix, onto TapEnv.curOutputStream(),
     * knowing the Maps of its rows and columns.
     * Note: contrary to usage, this method prints a final newLine!
     * @param rowMap the map of the rows of this BoolMatrix
     * @param colMap the map of the columns of this BoolMatrix.
     */
    public void dump(int[] rowMap, int[] colMap) throws java.io.IOException {
        if (nRows==0 || nCols==0) {
            TapEnv.printlnOnTrace(nRows+"x"+nCols+"matrix") ;
        } else {
            for (int rc = 1; rc < rowMap.length; ++rc) {
                if (rc > 1) {
                    TapEnv.printOnTrace("       ");
                    displaySepLine(colMap);
                    TapEnv.printlnOnTrace();
                }
                for (int ri = rowMap[rc - 1]; ri < rowMap[rc]; ++ri) {
                    TapEnv.printOnTrace("[" + TapEnv.str3(ri - rowMap[rc - 1]) + "]: ");
                    BoolVector row = getRow(ri);
                    if (row == null) {
                        displayIdentLine(rc, ri - rowMap[rc - 1], colMap);
                    } else {
                        displayZoneVector(row, colMap);
                    }
                    TapEnv.printlnOnTrace();
                }
            }
        }
    }
}










//     /**
//      * Insert len cols starting at index idx.
//      */
//     final void insertCols(int idx, int len) {
//         nCols = nCols + len;
//         if (rows != null) {
//             for (int i = 0; i < nRows; i++) {
//                 BoolVector row = rows[i];
//                 if (row == null) {
//                     if (idx <= i) {
//                         rows[i] = new BoolVector(this.sparse, nCols);
//                         rows[i].set(i + len, true);
//                     }
//                 } else {
//                     row.resize(nCols);
//                     row.move(idx, idx + len);
//                 }
//             }
//         }
//     }

//     /**
//      * Delete len cols starting at index idx.
//      */
//     final void deleteCols(int idx, int len) {
//         nCols = nCols - len;
//         if (rows == null) {
//             return;
//         }
//         for (int i = 0; i < nRows; i++) {
//             BoolVector row = rows[i];
//             if (row == null) {
//                 if (i - idx > -1) {
//                     rows[i] = new BoolVector(this.sparse, nCols);
//                     if (i - idx >= len) {
//                         rows[i].set(i - len, true);
//                     }
//                 }
//             } else {
//                 row.move(idx + len, idx);
//             }
//         }
//     }

//     /**
//      * Insert len rows starting at index idx.
//      */
//     final void insertRows(int idx, int len) {
//         int oldNRows = nRows;
//         nRows = nRows + len;
//         if (rows != null) {
//             BoolVector[] newRows = new BoolVector[nRows];
//             System.arraycopy(rows, 0, newRows, 0, idx);
//             for (int i = idx; i < oldNRows; i++) {
//                 BoolVector row = rows[i];
//                 if (row == null) {
//                     newRows[i + len] = new BoolVector(this.sparse, nCols);
//                     newRows[i + len].set(i, true);
//                 } else {
//                     newRows[i + len] = rows[i];
//                 }
//             }
//             rows = newRows;
//         }
//     }

//     /**
//      * Delete len rows starting at index idx.
//      */
//     final void deleteRows(int idx, int len) {
//         nRows = nRows - len;
//         if (rows != null) {
//             for (int i = idx; i < nRows; i++) {
//                 BoolVector srcRow = rows[i + len];

//                 if (srcRow == null) {
//                     rows[i] = new BoolVector(this.sparse, nCols);
//                     rows[i].set(i + len, true);
//                 } else {
//                     rows[i] = srcRow;
//                 }
//             }
//         }
//     }

//     /**
//      * If the matrix is square, try to keep it square.
//      */
//     final void insertColsAndRows(int idx, int len) {
//         if (nRows != nCols) {
//             throw new Error("use of insertColAndRows on a non square matrix");
//         }
//         nRows = nRows + len;
//         nCols = nCols + len;
//         if (rows != null) {
//             BoolVector[] newRows = new BoolVector[nRows];
//             int i = 0;
//             for (; i < idx; i++) {
//                 BoolVector row = rows[i];
//                 if (row != null) {
//                     row.resize(nCols);
//                     row.move(idx, idx + len);
//                     newRows[i] = row;
//                 }
//             }
//             i = idx;
//             int j = idx + len;
//             for (; j < nRows; i++, j++) {
//                 BoolVector row = rows[i];
//                 if (row != null) {
//                     row.resize(nCols);
//                     row.move(idx, idx + len);
//                     newRows[j] = row;
//                 }
//             }
//             rows = newRows;
//         }
//     }

//     public final void deleteColsAndRows(int idx, int len) {
//         int oldNRows = nRows;
//         if (nRows != nCols) {
//             throw new Error("use of insertColAndRows on a non square matrix");
//         }
//         nRows = nRows - len;
//         nCols = nCols - len;
//         int i = idx;
//         int j = idx + len;
//         for (; j < oldNRows; j++, i++) {
//             rows[i] = rows[j];
//         }
//         for (i = 0; i < nRows; i++) {
//             BoolVector row = rows[i];
//             if (row != null) {
//                 row.move(idx + len, idx);
//             }
//         }
//     }

//     /**
//      * Transfer of this BoolMatrix from internal to external
//      * numerotation. The transformation between the two numerotations is defined
//      * by a transfer BoolMatrix "transferMatrix", which has as many rows
//      * as the external/public BoolMatrix, and as many columns as this
//      * (internal/private) BoolMatrix. The (i,j) cell in "transferMatrix" is 1 iff
//      * the i-th row in the new numerotation corresponds to the
//      * j-th side-effect zone or to the (j-nSERows)-th declared zone
//      * in the old numerotation.
//      * Given this BoolMatrix ZDM which must be square, and calling
//      * TM the "transferMatrix" BoolMatrix,
//      * this function computes TM.ZDM.TM* (TM* is TM transposed).
//      * The result is a square BoolMatrix of the row dimension of TM.
//      * Actually, the result is computed as R = TM.ZDM.TM* = TM.(TM.ZDM*)*
//      * which can be implemented with only the "_.(_)*" operation
//      * (i.e. BoolMatrix.TimesTransposed). This function takes
//      * care of preserving the  "implicit-identity" convention.
//      */
//     public BoolMatrix transfer(BoolMatrix transferMatrix) {
//         BoolMatrix result = transferMatrix.timesTransposed(
//                 transferMatrix.timesTransposed(this));
//         // Now reset implicit-Identity convention:
//         for (int i = transferMatrix.nRows - 1; i >= 0; i--) {
//             boolean allImplicitIdentity = true;
//             // For each private zone j corresponding to public zone i:
//             for (int j = transferMatrix.nCols - 1;
//                  j >= 0 && allImplicitIdentity;
//                  j--) {
//                 // if row j is not implicit Identity in private matrix:
//                 if (transferMatrix.get(i, j) && getRow(j) != null)
//                 // then row i cannot be implicit identity in public matrix:
//                 {
//                     allImplicitIdentity = false;
//                 }
//             }
//             if (allImplicitIdentity) {
//                 result.setIdentityRow(i);
//             }
//         }
//         return result;
//     }
