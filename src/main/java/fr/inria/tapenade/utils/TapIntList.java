/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.TapEnv;

/**
 * List of int's, simply chained forward.
 */
public final class TapIntList {

    /**
     * The head int in this list.
     */
    public int head;

    /**
     * The tail of this list.
     */
    public TapIntList tail;

    /**
     * Creates a new list cell with given "head" and "tail".
     */
    public TapIntList(int head, TapIntList tail) {
        super();
        this.head = head;
        this.tail = tail;
    }

    /**
     * @return the length of current list. Returns 0 if list is null.
     */
    public static int length(TapIntList list) {
        int result = 0;
        while (list != null) {
            ++result;
            list = list.tail;
        }
        return result;
    }

    /**
     * @return true if value "val" is found in the given "list".
     */
    public static boolean contains(TapIntList list, int val) {
        boolean found = false;
        while (!found && list != null) {
            found = list.head == val;
            list = list.tail;
        }
        return found;
    }

    /**
     * @return true if all given "vals" are contained in the given "list".
     */
    public static boolean contains(TapIntList list, TapIntList vals) {
        boolean result = true;
        while (vals != null && result) {
            result = contains(list, vals.head);
            vals = vals.tail;
        }
        return result;
    }

    public static boolean equalLists(TapIntList list1, TapIntList list2) {
        return contains(list1, list2) && contains(list2, list1);
    }

    /**
     * @return a new TapIntList, concatenation of "list1" followed by "list2".
     * Caution: the result shares structure with "list2".
     */
    public static TapIntList append(TapIntList list1, TapIntList list2) {
        if (list1 == null) {
            return list2;
        } else {
            TapIntList hdRes = new TapIntList(-1, null);
            TapIntList inRes = hdRes;
            while (list1 != null) {
                inRes = inRes.placdl(list1.head);
                list1 = list1.tail;
            }
            inRes.tail = list2;
            return hdRes.tail;
        }
    }

    /**
     * @return a copy of "list" with each number increased with "offset".
     */
    public static TapIntList addOffset(TapIntList list, int offset) {
        TapIntList hdResult = new TapIntList(-1, null);
        TapIntList tlResult = hdResult;
        while (list != null) {
            tlResult = tlResult.placdl(offset + list.head);
            list = list.tail;
        }
        return hdResult.tail;
    }

    /**
     * @return a copy of "list" with duplicated elements removed.
     */
    private static TapIntList convertToSet(TapIntList list) {
        TapIntList resList = null;
        for (TapIntList tmpList = list; tmpList != null; tmpList = tmpList.tail) {
            if (!contains(resList, tmpList.head)) {
                /* add it to the set*/
                resList = new TapIntList(tmpList.head, resList);
            }
        }
        return resList;
    }

    /**
     * @return the union of "list1" and "list2".
     * Does not add elements of list2 that are already in list1.
     */
    public static TapIntList quickUnion(TapIntList list1, TapIntList list2) {
        while (list2 != null) {
            if (!contains(list1, list2.head)) {
                list1 = new TapIntList(list2.head, list1);
            }
            list2 = list2.tail;
        }
        return list1;
    }

    /**
     * @return a new list, concatenation of "list1" followed by "list2", with duplicates removed.
     */
    public static TapIntList union(TapIntList list1, TapIntList list2) {
        return convertToSet(append(list1, list2));
    }

    /**
     * @return a copy of the given list.
     */
    public static TapIntList copy(TapIntList list) {
        TapIntList result = new TapIntList(-1, null);
        TapIntList tlResult = result;
        while (list != null) {
            tlResult = tlResult.placdl(list.head);
            list = list.tail;
        }
        return result.tail;
    }

    /**
     * @return a sorted (increasing order) copy of the given list.
     */
    public static TapIntList sort(TapIntList list) {
        TapIntList sortedList = new TapIntList(-1, null);
        TapIntList insortedList;
        int rank;
        while (list != null) {
            rank = list.head;
            insortedList = sortedList;
            while (insortedList.tail != null && insortedList.tail.head < rank) {
                insortedList = insortedList.tail;
            }
            insortedList.placdl(list.head);
            list = list.tail;
        }
        return sortedList.tail;
    }

    /**
     * Add a new int "elem" into the (already sorted) given list.
     */
    public static TapIntList addIntoSorted(TapIntList list, int elem) {
        list = new TapIntList(0, list);
        TapIntList toTail = list;
        while (toTail != null) {
            if (toTail.tail == null || elem <= toTail.tail.head) {
                if (toTail.tail == null || elem < toTail.tail.head) {
                    toTail.tail = new TapIntList(elem, toTail.tail);
                }
                toTail = null;
            } else {
                toTail = toTail.tail;
            }
        }
        return list.tail;
    }

    /**
     * @return true if "list1" contains "list2".
     * Both list1 and list2 must be sorted from 0 up, otherwise result is undefined.
     */
    public static boolean sortedContains(TapIntList list1, TapIntList list2) {
        boolean contains = true;
        while (contains && list2 != null) {
            while (list1 != null && list1.head < list2.head) {
                list1 = list1.tail;
            }
            if (list1 == null || list1.head != list2.head) {
                contains = false;
            }
            list2 = list2.tail;
        }
        return contains;
    }

    /**
     * @return true if "list1" intersects "list2".
     * Both list1 and list2 must be sorted from 0 up, otherwise result is undefined.
     */
    public static boolean sortedIntersects(TapIntList list1, TapIntList list2) {
        boolean intersects = false;
        while (!intersects && list1 != null && list2 != null) {
            while (list1 != null && list1.head < list2.head) {
                list1 = list1.tail;
            }
            if (list1 != null && list1.head == list2.head) {
                intersects = true;
            }
            list2 = list2.tail;
        }
        return intersects;
    }

    /**
     * @return true if "list1" and "list2" have at least one int in common.
     */
    public static boolean intersects(TapIntList list1, TapIntList list2) {
        boolean intersect = false;
        while (list1 != null && !intersect) {
            intersect = contains(list2, list1.head);
            list1 = list1.tail;
        }
        return intersect;
    }

    /**
     * @return the intersection of "list" and "interList".
     * Caution: "list" may be modified and the result shares structure with "list".
     */
    public static TapIntList intersection(TapIntList list, TapIntList interList) {
        TapIntList toRes = new TapIntList(-1, list);
        TapIntList inRes = toRes;
        while (inRes.tail != null) {
            if (!TapIntList.contains(interList, inRes.tail.head)) {
                inRes.tail = inRes.tail.tail;
            } else {
                inRes = inRes.tail;
            }
        }
        return toRes.tail;
    }

    /**
     * @return "list" without elements in "minusList".
     * Caution: "list" may be modified and the result shares structure with "list".
     */
    public static TapIntList minus(TapIntList list, TapIntList minusList) {
        TapIntList toRes = new TapIntList(-1, list);
        TapIntList inRes = toRes;
        while (inRes.tail != null) {
            if (TapIntList.contains(minusList, inRes.tail.head)) {
                inRes.tail = inRes.tail.tail;
            } else {
                inRes = inRes.tail;
            }
        }
        return toRes.tail;
    }

     /** Returns list with value removed if present */
    public static TapIntList remove(TapIntList list, int value) {
        TapIntList toRes = new TapIntList(-1, list);
        TapIntList inRes = toRes;
        while (inRes.tail != null) {
            if (inRes.tail.head==value) {
                inRes.tail = inRes.tail.tail;
            } else {
                inRes = inRes.tail;
            }
        }
        return toRes.tail;
    }

    /** Returns list with value added if not already present */
    public static TapIntList add(TapIntList list, int value) {
        if (contains(list, value)) {
            return list ;
        } else {
            return new TapIntList(value, list) ;
        }
    }

    /** Returns a copy of list, without all elements above max */
    public static TapIntList removeAbove(TapIntList list, int max) {
        TapIntList hdRes = new TapIntList(-1, null);
        TapIntList tlRes = hdRes;
        while (list!=null) {
            if (list.head<=max) {
                tlRes = tlRes.placdl(list.head) ;
            }
            list = list.tail ;
        }
        return hdRes.tail ;
    }

    /**
     * @return a reversed copy of the given "list".
     */
    public static TapIntList reverse(TapIntList list) {
        TapIntList result = null;
        while (list != null) {
            result = new TapIntList(list.head, result);
            list = list.tail;
        }
        return result;
    }

    /**
     * @return the minimal element of "list", and -1 if list is empty.
     */
    public static int minElem(TapIntList list) {
        if (list == null) {
            return -1;
        }
        int result = list.head;
        list = list.tail;
        while (list != null) {
            if (list.head < result) {
                result = list.head;
            }
            list = list.tail;
        }
        return result;
    }

    /**
     * @return the maximal element of "list", and -1 if list is empty.
     */
    public static int maxElem(TapIntList list) {
        if (list == null) {
            return -1;
        }
        int result = list.head;
        list = list.tail;
        while (list != null) {
            if (list.head > result) {
                result = list.head;
            }
            list = list.tail;
        }
        return result;
    }

    /**
     * Adds new int "val" at the tail of this list.
     */
    public void newR(int val) {
        TapIntList inList = this;
        while (inList.tail != null) {
            inList = inList.tail;
        }
        inList.tail = new TapIntList(val, null);
    }

    /**
     * Replaces the tail of this list by a new list cell, whose head
     * is "n", and whose tail is the previous tail of this list.
     *
     * @return this new list cell.
     */
    public TapIntList placdl(int n) {
        return tail = new TapIntList(n, tail);
    }

    /**
     * Adds n (positive or 0) into this (hatted) sorted list iff it is not already there.
     */
    public final void appendSortedIfAbsent(int n) {
        TapIntList toList = this;

        while (toList!=null) {
            if (toList.tail==null || toList.tail.head>n) {
                toList.tail = new TapIntList(n, toList.tail) ;
                toList = null ;
            } else if (toList.tail.head==n) {
                toList = null ;
            } else {
                toList = toList.tail;
            }
        }
    }

//         while (toList.tail != null && toList.tail.head != n) {
//             toList = toList.tail;
//         }
//         if (toList.tail == null) {
//             toList.tail = new TapIntList(n, null);
//         }


    /**
     * Prints the contents of this TapIntList onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append('<');
        result.append(head);
        TapIntList inList = tail;
        while (inList != null) {
            result.append(' ').append(inList.head);
            inList = inList.tail;
        }
        return result + ">";
    }
}
