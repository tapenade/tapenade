/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.ZoneInfo;

/**
 * Pair of int and ZoneInfo.
 */
public final class Int2ZoneInfo {
    private final int zoneNumber;
    private final ZoneInfo zoneInfo;

    public Int2ZoneInfo(int number, ZoneInfo zInfo) {
        super();
        zoneNumber = number;
        zoneInfo = zInfo;
    }

    public ZoneInfo getZoneInfo() {
        return zoneInfo;
    }

    public int getZoneNumber() {
        return zoneNumber;
    }

    @Override
    public String toString() {
        return "{" + zoneNumber + ": " + zoneInfo + "}";
    }
}
