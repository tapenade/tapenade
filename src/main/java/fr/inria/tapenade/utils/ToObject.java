/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * Container for one Object.
 */
public final class ToObject<T> {

    private T obj;

    /**
     * Creates a new container, initially containing the given Object.
     */
    public ToObject(T obj) {
        super();
        this.obj = obj;
    }

    /**
     * @return the Object in this container.
     */
    public T obj() {
        return obj;
    }

    /**
     * Fill this container with "obj".
     */
    public void setObj(T obj) {
        this.obj = obj;
    }

    public String toString() {
        return "[" + obj + "]";
    }
}
