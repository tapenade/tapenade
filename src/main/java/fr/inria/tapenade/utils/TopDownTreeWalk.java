/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * A class that implements a TopDownTreeWalk.
 * <p>
 * Example of use of the Iterator:<br>
 * <pre>
 * {@code
 * Tree currentTree;
 * for (TopDownTreeWalk treeWalk = new TopDownTreeWalk(tree);
 *      !treeWalk.atEnd();
 *      treeWalk.advance()) {
 *     currentTree = treeWalk.get();
 *     ...
 * }
 * }</pre>
 */
public class TopDownTreeWalk {
    private final Tree base;
    private Tree tree;

    /**
     * The TopDownTreeWalk constructor.
     *
     * @param root The root of the tree.
     */
    public TopDownTreeWalk(Tree root) {
        super();
        tree = root;
        base = root;
    }

    /**
     * @return The Tree at the current position.
     */
    public Tree get() {
        return tree;
    }

    /**
     * @return true if I'm positioned after the last item.
     */
    public boolean atEnd() {
        return tree == null;
    }

    /**
     * Advances to the next tree.
     */
    public void advance() {
        Tree next = null;
        if (!tree.isAtom()) {
            Tree[] sons = tree.children();
            if (sons.length > 0) {
                next = sons[0];
            }
        }
        if (next == null) {
            if (tree == base) {
                tree = null;
            } else if ((next = tree.right()) == null) {
                Tree nextr;
                boolean stop = false;
                while (!stop) {
                    next = tree.parent();
                    if (next == null || next == base) {
                        stop = true;
                        tree = null;
                    } else if ((nextr = next.right()) != null) {
                        tree = nextr;
                        stop = true;
                    } else {
                        tree = next;
                    }
                }
            } else {
                tree = next;
            }
        } else {
            tree = next;
        }
    }
}
