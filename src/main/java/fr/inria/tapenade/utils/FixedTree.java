/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.io.IOException;

/**
 * A fixed-arity node of a Tree.
 */
class FixedTree extends Tree {

    /**
     * The sons of this Tree.
     * We choose to make it an array, because we want to access them randomly.
     */
    private final Tree[] sons;

    protected FixedTree(Operator operator) {
        super(operator);
        this.sons = new Tree[operator.arity];
    }

    @Override
    public void setChild(Tree newChild, int rank) {
        this.connectNewChild(newChild, this.sons, rank - 1);
    }

    @Override
    public Tree cutChild(int rank) {
        Tree child = sons[rank - 1];
        if (child != null) {
            child.setParent(null);
            sons[rank - 1] = ILLangOps.ops(ILLang.op_none).tree(); // to be on the safe side...
        }
        return child;
    }

    @Override
    public Tree copy() {
        FixedTree newTree = (FixedTree) operator().tree();
        for (int i = sons.length - 1; i >= 0; --i) {
            if (sons[i] != null) {
                newTree.connectNewChild(sons[i].copy(), newTree.sons, i);
            } else {
                newTree.sons[i] = null;
            }
        }
        newTree.copyAnnotations(this);
        return newTree;
    }

    @Override
    protected boolean checkSyntax(Phylum phylum, String path) {
        boolean correct = super.checkSyntax(phylum, path);
        int[] childrenPhyla = operator().childrenPhyla;
        Phylum childPhylum;
        path = path + " " + operator().name + ":";
        for (int i = 0; i < sons.length; ++i) {
            childPhylum = ILLangOps.phyla(childrenPhyla[i]);
            if (sons[i] == null) {
                TapEnv.toolError("Checking Tree: null child at " + path + (i + 1) + " ... " + this.parent());
                correct = false;
            } else if (!sons[i].checkSyntax(childPhylum, path + (i + 1))) {
                correct = false;
            }
        }
        return correct;
    }

    @Override
    public boolean isAtom() {
        return false;
    }

    @Override
    public boolean isStringAtom() {
        return false;
    }

    @Override
    public boolean isIntAtom() {
        return false;
    }

    @Override
    public boolean isList() {
        return false;
    }

    @Override
    public boolean isFixed() {
        return true;
    }

    @Override
    public int length() {
        return sons.length;
    }

    @Override
    public Tree[] children() {
        return sons;
    }

    @Override
    public TapList<Tree> childrenList() {
        TapList<Tree> list = null;
        for (int index = sons.length - 1; index >= 0; --index) {
            list = new TapList<>(sons[index], list);
        }
        return list;
    }

    @Override
    public Tree down(int rank) {
        if (rank - 1 < sons.length) {
            return sons[rank - 1];
        }
        return null;
    }

    @Override
    public int getRank(Tree son) {
        for (int i = sons.length - 1; i >= 0; --i) {
            if (sons[i] == son) {
                return i + 1;
            }
        }
        return -1;
    }

    @Override
    public boolean equalsTree(Tree other) {
        if (other == null || operator() != other.operator()) {
            return false;
        }
        for (int i = sons.length - 1; i >= 0; --i) {
            if (sons[i] == null) {
                if (((FixedTree) other).sons[i] != null) {
                    return false;
                }
            } else {
                if (!sons[i].equalsTree(((FixedTree) other).sons[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void writeToILFile(java.io.BufferedWriter fileWriter) throws IOException {
        operator().writeToILFile(fileWriter);
        for (Tree son : sons) {
            if (son == null) {
                ILLangOps.ops(ILLang.op_none).writeToILFile(fileWriter);
            } else {
                son.writeToILFile(fileWriter);
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder childrenStr = new StringBuilder();
        for (int i = sons.length - 1; i >= 0; --i) {
            childrenStr.insert(0, sons[i]);
            if (i > 0) {
                childrenStr.insert(0, ", ");
            }
        }
        return operator() + "(" + childrenStr + ")";
    }
}
