/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

/**
 * An object around an int, which can thus be modified.
 */
public final class ToInt {
    private int value;

    /**
     * Creation, with initial value.
     */
    public ToInt(int value) {
        super();
        this.value = value;
    }

    /**
     * Set the value.
     */
    public void set(int value) {
        this.value = value;
    }

    /**
     * @return the value.
     */
    public int get() {
        return value;
    }

    @Override
    public String toString() {
        return "[" + value + "]";
    }

}
