/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.utils;

import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;

import java.io.IOException;

/**
 * A list-arity node of a Tree.
 */
class ListTree extends Tree {

    /**
     * The sons of this Tree.
     * We choose to make it an array, because we want to access them randomly.
     */
    private Tree[] sons;

    /**
     * The current number of sons.
     * Might be different from sons.length, but is probably equal in the present
     * implementation because we increase the list of sons by chunks of size one.
     */
    private int nbSons;

    protected ListTree(Operator operator) {
        super(operator);
        this.sons = new Tree[0];
        this.nbSons = 0;
    }

    @Override
    public void addChild(Tree addedChild, int rank) {
        if (rank < 0) {
            rank = rank + 2 + nbSons;
        }
        if (0 < rank && rank <= nbSons + 1) {
            ++nbSons;
            Tree[] newSons = new Tree[nbSons];
            for (int i = 0; i < rank - 1; ++i) {
                newSons[i] = sons[i];
            }
            this.connectNewChild(addedChild, newSons, rank - 1);
            for (int i = rank; i < nbSons; ++i) {
                newSons[i] = sons[i - 1];
            }
            sons = newSons;
        }
    }

    @Override
    public void addChildren(Tree[] addedChildren, int rank) {
        int nbNew = addedChildren.length;
        if (rank < 0) {
            rank = rank + 2 + nbSons;
        }
        if (0 < rank && rank <= nbSons + nbNew) {
            int index;
            Tree[] newSons = new Tree[nbSons + nbNew];
            for (index = rank - 2; index >= 0; --index) {
                newSons[index] = sons[index];
            }
            for (index = nbNew - 1; index >= 0; --index) {
                if (addedChildren[index] != null) {
                    this.connectNewChild(addedChildren[index], newSons, index + rank - 1);
                }
            }
            for (index = rank - 1; index < nbSons; ++index) {
                newSons[index + nbNew] = sons[index];
            }
            nbSons += nbNew;
            sons = newSons;
        }
    }

    @Override
    public void addChildren(TapList<Tree> addedChildren, int rank) {
        int nbNew = TapList.length(addedChildren);
        if (rank < 0) {
            rank = rank + 2 + nbSons;
        }
        if (0 < rank && rank <= nbSons + nbNew) {
            int index;
            Tree[] newSons = new Tree[nbSons + nbNew];
            for (index = rank - 2; index >= 0; --index) {
                newSons[index] = sons[index];
            }
            for (index = 0; index < nbNew; ++index) {
                if (addedChildren.head != null) {
                    this.connectNewChild(addedChildren.head, newSons, index + rank - 1);
                }
                addedChildren = addedChildren.tail;
            }
            for (index = rank - 1; index < nbSons; ++index) {
                newSons[index + nbNew] = sons[index];
            }
            nbSons += nbNew;
            sons = newSons;
        }
    }

    @Override
    public void setChild(Tree newChild, int rank) {
        if (rank <= nbSons) {
            this.connectNewChild(newChild, this.sons, rank - 1);
        } else {
            Tree[] newSons = new Tree[rank];
            for (int i = 0; i < nbSons; ++i) {
                newSons[i] = sons[i];
            }
            this.connectNewChild(newChild, newSons, rank - 1);
            nbSons = rank;
            sons = newSons;
        }
    }

    @Override
    public Tree cutChild(int rank) {
        Tree child = sons[rank - 1];
        if (child != null) {
            child.setParent(null);
            sons[rank - 1] = ILLangOps.ops(ILLang.op_none).tree(); // to be on the safe side...
        }
        return child;
    }

    @Override
    public void removeChild(int rank) {
        if (0 < rank && rank <= nbSons) {
            sons[rank - 1].setParent(null);
            Tree[] newSons = new Tree[nbSons - 1];
            for (int i = nbSons - 1; i > rank - 1; --i) {
                newSons[i - 1] = sons[i];
            }
            for (int i = rank - 2; i >= 0; --i) {
                newSons[i] = sons[i];
            }
            --nbSons;
            sons = newSons;
        }
    }

    @Override
    public Tree copy() {
        ListTree newTree = (ListTree) operator().tree();
        newTree.nbSons = nbSons;
        newTree.sons = new Tree[nbSons];
        for (int i = nbSons - 1; i >= 0; --i) {
            if (sons[i] != null) {
                newTree.connectNewChild(sons[i].copy(), newTree.sons, i);
            } else {
                newTree.sons[i] = null;
            }
        }
        newTree.copyAnnotations(this);
        return newTree;
    }

    @Override
    protected boolean checkSyntax(Phylum phylum, String path) {
        boolean correct = super.checkSyntax(phylum, path);
        int[] childrenPhyla = operator().childrenPhyla;
        Phylum childPhylum = childrenPhyla == null ? null : ILLangOps.phyla(childrenPhyla[0]);
        path = path + " " + operator().name + ":";
        for (int i = 0; i < nbSons; ++i) {
            if (sons[i] == null) {
                TapEnv.toolError("Checking Tree: null child at " + path + (i + 1) + "... " + this.parent());
                correct = false;
            } else if (!sons[i].checkSyntax(childPhylum, path + (i + 1))) {
                correct = false;
            }
        }
        return correct;
    }

    @Override
    public boolean isAtom() {
        return false;
    }

    @Override
    public boolean isStringAtom() {
        return false;
    }

    @Override
    public boolean isIntAtom() {
        return false;
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public boolean isFixed() {
        return false;
    }

    @Override
    public int length() {
        return nbSons;
    }

    @Override
    public Tree[] children() {
        return sons;
    }

    @Override
    public TapList<Tree> childrenList() {
        TapList<Tree> list = null;
        for (int index = nbSons - 1; index >= 0; --index) {
            list = new TapList<>(sons[index], list);
        }
        return list;
    }

    @Override
    public Tree down(int n) {
        if (n < 0) {
            n = nbSons + n + 1;
        }
        return 0 < n && n <= nbSons ? sons[n - 1] : null;
    }

    @Override
    public int getRank(Tree son) {
        for (int i = nbSons - 1; i >= 0; --i) {
            if (sons[i] == son) {
                return i + 1;
            }
        }
        return -1;
    }

    @Override
    public boolean equalsTree(Tree other) {
        if (other == null || operator() != other.operator() || nbSons != ((ListTree) other).nbSons) {
            return false;
        }
        for (int i = nbSons - 1; i >= 0; --i) {
            if (sons[i] == null) {
                if (((ListTree) other).sons[i] != null) {
                    return false;
                }
            } else {
                if (!sons[i].equalsTree(((ListTree) other).sons[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected void writeToILFile(java.io.BufferedWriter fileWriter) throws IOException {
        operator().writeToILFile(fileWriter);
        for (Tree son : sons) {
            if (son == null) {
                ILLangOps.ops(ILLang.op_none).writeToILFile(fileWriter);
            } else {
                son.writeToILFile(fileWriter);
            }
        }
        ILLangOps.ops(ILLang.op_endOfList).writeToILFile(fileWriter);
    }

    @Override
    public String toString() {
        StringBuilder childrenStr = new StringBuilder();
        for (int i = sons.length - 1; i >= 0; --i) {
            childrenStr.insert(0, sons[i]);
            if (i > 0) {
                childrenStr.insert(0, ", ");
            }
        }
        return operator() + "[" + childrenStr + "]";
    }

}

