/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.representation.TapList;

public class NewBlockGraphArrow {

    private NewBlockGraphNode origin;

    private NewBlockGraphNode destination;

    public NewBlockGraphNode origin() {
        return origin;
    }

    public NewBlockGraphNode destination() {
        return destination;
    }

    protected NewBlockGraphArrow(NewBlockGraphNode origin, NewBlockGraphNode destination) {
        super();
        this.origin = origin;
        origin.flow = new TapList<>(this, origin.flow);
        this.destination = destination;
        destination.backFlow = new TapList<>(this, destination.backFlow);
    }

    protected void redirectDestination(NewBlockGraphNode newDest) {
        if (destination != null) {
            destination.backFlow = TapList.delete(this, destination.backFlow);
        }
        if (newDest != null) {
            if (NewBlockGraph.existsArrow(origin, newDest)) {
                origin.flow = TapList.delete(this, origin.flow);
            } else {
                newDest.backFlow = new TapList<>(this, newDest.backFlow);
            }
        }
        destination = newDest;
    }

    protected void redirectOrigin(NewBlockGraphNode newOrig) {
        if (origin != null) {
            origin.flow = TapList.delete(this, origin.flow);
        }
        if (newOrig != null) {
            if (NewBlockGraph.existsArrow(newOrig, destination)) {
                destination.backFlow = TapList.delete(this, destination.backFlow);
            } else {
                newOrig.flow = new TapList<>(this, newOrig.flow);
            }
        }
        origin = newOrig;
    }

    @Override
    public String toString() {
        return "(" + origin + "->" + destination + ')';
    }
}
