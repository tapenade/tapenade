Differentiated IR essentially built - <span style="color:red">**[State 5]**</span>
----------------------------------------------------------------------------------

The differentiated IR is accessible through a variable (e.g.
diffCallGraph) of type `CallGraph`. It is a standalone `CallGraph`: it
is different from the original source `CallGraph`, and shares very
little with it. The differentiated `CallGraph` is ready for source
regeneration.

In the current Tapenade, the only thing done with the differentiated
`CallGraph` is source regeneration. No data-flow analysis will be run on
it. Therefore we don't need to prepare the data-flow infrastructure on
the differentiated `CallGraph`.
