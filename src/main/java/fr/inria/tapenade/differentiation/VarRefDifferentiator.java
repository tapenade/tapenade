/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.ArrayTypeSpec;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CompositeTypeSpec;
import fr.inria.tapenade.representation.FieldDecl;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.InterfaceDecl;
import fr.inria.tapenade.representation.IterDescriptor;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeDecl;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * Object that knows how to differentiate a variable.
 */
public class VarRefDifferentiator {

    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    /**
     * @return the global adEnv.callGraphDifferentiator.
     */
    private CallGraphDifferentiator callGraphDifferentiator() {
        return adEnv.callGraphDifferentiator;
    }

    /**
     * @return the global adEnv.BlockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * @return the global adEnv.multiDirMode.
     */
    public boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * @return the global adEnv.suffixes.
     */
    private String[][] suffixes() {
        return adEnv.suffixes;
    }

    /**
     * @return the global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * @return the global adEnv.curDiffUnitSort.
     */
    private int curDiffUnitSort() {
        return adEnv.curDiffUnitSort;
    }

    /**
     * @return the global adEnv.curDiffVarSort.
     */
    private int curDiffVarSort() {
        return adEnv.curDiffVarSort;
    }

    /**
     * To get vector/multiDir indices in derivative vars and loops around derivative instructions.
     */
    protected IterDescriptor multiDirIterDescriptor;
    protected IterDescriptor multiDirMaxIterDescriptor;
    protected NewSymbolHolder dirIndexSymbolHolder;
    protected NewSymbolHolder dirNumberSymbolHolder;
    protected ArrayDim multiDirDimensionMax;

    protected ArrayDim getMultiDirDimensionMax() {
        return curDiffUnit() == null ? multiDirDimensionMax : curDiffUnit().multiDirDimensionMax;
    }

    protected Tree buildDirNumberReference(SymbolTable usageSymbolTable) {
        return (TapEnv.get().fixedNbdirsTree!=null
                ?ILUtils.copy(TapEnv.get().fixedNbdirsTree)
                :dirNumberSymbolHolder.makeNewRef(usageSymbolTable)) ;
    }

    /**
     * The context in which variables must be differentiated.
     * It is a stack. Elements are in {INEXPR (default), INCALL, INHEADER}).
     */
    private TapIntList diffContexts = new TapIntList(DiffConstants.INEXPR, null);


    protected VarRefDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
    }

    protected void pushDiffContext(int newContext) {
        diffContexts = new TapIntList(newContext, diffContexts);
    }

    protected void popDiffContext() {
        diffContexts = diffContexts.tail;
    }

    protected TapList<Tree> mapDiffVarRef(ActivityPattern pattern, TapList<Tree> expressions,
                                          SymbolTable diffSymbolTable) {
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        Tree expression;
        while (expressions != null) {
            expression = expressions.head;
            tlResult = tlResult.placdl(diffVarRef(pattern, expression, diffSymbolTable,
                    false, expression, false, false, expression));
            expressions = expressions.tail;
        }
        return hdResult.tail;
    }

    /**
     * Differentiate a reference to a variable. When "identifyFunctionResult"
     * is true, we must take care of the case where the variable may be
     * the result of the current function. In this case, the diff variable
     * and the diff function must also have the same names.
     * Most of the complexity of this function is due to the "vector mode",
     * where derivatives receive an extra dimension wrt original variables.
     */
    protected Tree diffVarRef(ActivityPattern pattern, Tree expression, SymbolTable diffSymbolTable,
                              boolean identifyFunctionResult, Tree contextExpression,
                              boolean needMultiDirDim, boolean addNbDirsDimContext, Tree hintRootTree) {
        // TODO: the behavior of the function is driven both by the "contextExpression"
        // and by the flag stored in "diffContexts.head".
        // This is too complicated and should be simplified!
        Tree diffVarRef;
        if (TapEnv.associationByAddress()) {
            if (identifyFunctionResult) {
                diffVarRef = diffVarRefRec(pattern, expression, diffSymbolTable,
                        identifyFunctionResult, contextExpression, hintRootTree);
            } else {
                diffVarRef = ILUtils.copy(expression);
            }
            String name = ILUtils.baseName(expression);
            if (name != null) {
                buildAAInstructionVORD(expression, diffSymbolTable, diffVarRef, TapEnv.assocAddressValueSuffix(), true);
                VariableDecl varDecl = diffSymbolTable.getVariableDecl(name);
                if (varDecl != null) {
                    diffVarRef = ILUtils.build(ILLang.op_fieldAccess,
                                   ILUtils.copy(diffVarRef),
                                   ILUtils.build(ILLang.op_ident, TapEnv.assocAddressDiffSuffix()));
                }
            }
        } else {
            diffVarRef = diffVarRefRec(pattern, expression, diffSymbolTable,
                    identifyFunctionResult, contextExpression, hintRootTree);
        }
        if (multiDirMode() && diffVarRef != null) {
            // Multi-directional mode: we must add the [nbDirsMax] dimension index into diffVarRef:
            boolean addMultiDirAtThisLevel ;
            WrapperTypeSpec typeSpec ;
            if (expression.opCode() == ILLang.op_allocate) {
                typeSpec = new WrapperTypeSpec(null);
                addMultiDirAtThisLevel = ILUtils.acceptsMultiDirDimension(expression.down(3)) ;
            } else if (expression.opCode() == ILLang.op_deallocate) {
                typeSpec = diffSymbolTable.typeOf(expression.down(1));
                addMultiDirAtThisLevel = typeSpec.acceptsMultiDirDimension();
            } else {
                typeSpec = diffSymbolTable.typeOf(expression);
                addMultiDirAtThisLevel = typeSpec.acceptsMultiDirDimension();
            }
// System.out.println("ADDMULTIDIRATTHISLEVEL ON "+expression+" :: "+typeSpec+" ===> "+addMultiDirAtThisLevel+" CTXT:"+diffContexts.head+" CONTEXTEXPRESSION:"+contextExpression) ;
            if (addMultiDirAtThisLevel) {
//                 boolean addNbDirsDimExpr ;
                Tree origDiffAllocate = null;
                if (expression.opCode() == ILLang.op_allocate) {
                    // build a dummy variable reference, into which the vector dimension
                    // will be added as usual, and then which will be used to rebuild the allocate:
                    origDiffAllocate = diffVarRef;
                    diffVarRef = ILUtils.build(ILLang.op_ident, "diffallocvar");
                    if (!ILUtils.isNullOrNone(origDiffAllocate.down(2))) {
                        diffVarRef = ILUtils.build(ILLang.op_arrayAccess,
                                       diffVarRef,
                                       origDiffAllocate.cutChild(2));
                    }
                }
                NewSymbolHolder dirNumberMaxSH =
                    (curDiffUnit()!=null ? curDiffUnit().dirNumberMaxSymbolHolder : callGraphDifferentiator().rootDirNumberMaxSymbolHolder) ;
                int arrayDimMin =
                    curDiffUnit() == null ? 0 : curDiffUnit().arrayDimMin;
                if (contextExpression.opCode() == ILLang.op_varDeclaration
                    || contextExpression.opCode() == ILLang.op_varDimDeclaration
                    || contextExpression.opCode() == ILLang.op_varDeclarators
                    || contextExpression.opCode() == ILLang.op_pointerDeclarator
                    || contextExpression.opCode() == ILLang.op_arrayDeclarator
                    || contextExpression.opCode() == ILLang.op_common) {
                    if (diffContexts.head == DiffConstants.INEXPR) {
                        VariableDecl varDecl = diffSymbolTable.getVariableDecl(ILUtils.baseName(expression));
                        boolean insertMultiDirDimension = true;
                        if (typeSpec.wrappedType != null
                            && typeSpec.wrappedType.diffTypeSpec != null) {
                            insertMultiDirDimension = false;
                        } else if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            WrapperTypeSpec diffElemTypeSpec =
                                typeSpec.wrappedType.elementType().wrappedType.diffTypeSpec;
                            if (diffElemTypeSpec != null) {
                                insertMultiDirDimension = false;
                            }
                        }
                        if (insertMultiDirDimension
                            && (contextExpression.opCode() == ILLang.op_varDimDeclaration
                                || contextExpression.opCode() == ILLang.op_varDeclarators
                                || contextExpression.opCode() == ILLang.op_arrayDeclarator
                                || !varDecl.hasInstructionWithRootOperator(ILLang.op_varDimDeclaration, null))) {
                            Tree dimColonsTree = ILUtils.findDimColons(contextExpression);
                            Tree multiDirDimensionTree;
                            if (TapEnv.relatedLanguageIsFortran() && TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                                multiDirDimensionTree = ILUtils.build(ILLang.op_dimColon,
                                    ILUtils.build(ILLang.op_none), ILUtils.build(ILLang.op_none));
                            } else {
                                Tree nbDirsMaxExpr;
                                if (TapEnv.get().fixedNbdirsmaxString != null) {
                                    nbDirsMaxExpr = ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsmaxString);
                                } else {
                                    nbDirsMaxExpr = dirNumberMaxSH.makeNewRef(diffSymbolTable);
                                }
                                multiDirDimensionTree = ILUtils.buildDimColon(arrayDimMin, nbDirsMaxExpr);
                            }
                            if (contextExpression.opCode() == ILLang.op_varDeclaration
                                && dimColonsTree != null && dimColonsTree.opCode() == ILLang.op_dimColons) {

                            } else if (diffVarRef.opCode() == ILLang.op_arrayDeclarator) {
                                diffVarRef.down(2).addChild(multiDirDimensionTree, -1);
                            } else if (needMultiDirDim) {
                                diffVarRef = ILUtils.build(ILLang.op_arrayDeclarator, diffVarRef,
                                               ILUtils.build(ILLang.op_dimColons, multiDirDimensionTree));
                            }
                            if (contextExpression.opCode() == ILLang.op_varDimDeclaration) {
                                diffVarRef.down(2).addChild(multiDirDimensionTree, -1);
                            }
                        }
                    }
                } else if (contextExpression.opCode() != ILLang.op_save) {
                    Tree newVectIndex = null;
                    // Give a non-null value to "newVectIndex" only if we want
                    // to explicitly add a vectorial index to diffVarRef:
                    if (expression.opCode() == ILLang.op_allocate) {
                        if (TapEnv.get().fixedNbdirsmaxString != null) {
                            newVectIndex = ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsmaxString);
                        } else {
                            newVectIndex = dirNumberMaxSH.makeNewRef(diffSymbolTable);
                        }
                    } else if (contextExpression.opCode() == ILLang.op_equivalence) {
                        newVectIndex = ILUtils.build(ILLang.op_intCst, 1);
                    } else if (diffContexts.head == DiffConstants.INEXPR) {
                        // In "vector mode" for a "normal" variable reference,
                        // if X(i,j) was differentiated as Xd(i,j), we must return Xd(i,j,nd)
                        if (addNbDirsDimContext
                            && (expression.opCode() == ILLang.op_allocate
                            || expression.opCode() == ILLang.op_deallocate
                            || !mayAddSimplyColon(typeSpec))) {
                            newVectIndex = dirIndexSymbolHolder.makeNewRef(diffSymbolTable);
                        } else {
                            newVectIndex = ILUtils.build(ILLang.op_arrayTriplet);
                        }
                    } else {
                        // In "vector mode" for a parameter of a function call,
                        // if X(i,j) was differentiated as Xd(i,j), we must leave Xd(i,j) in C,
                        // Xd(i,j,1) in Fortran or Xd(i,j,:) in F90*/
                        if (diffVarRef.opCode() == ILLang.op_arrayAccess) {
                            if (adEnv.curUnit().language() == TapEnv.FORTRAN) {
                                newVectIndex = ILUtils.build(ILLang.op_intCst, 1);
                            } else if (adEnv.curUnit().language() == TapEnv.FORTRAN90 || adEnv.curUnit().language() == TapEnv.FORTRAN2003) {
                                newVectIndex = ILUtils.build(ILLang.op_arrayTriplet);
                            }
                        } else if (diffVarRef.opCode() == ILLang.op_pointerDeclarator) {
                            //vmp modif pour multiDirMode
                            newVectIndex = dirIndexSymbolHolder.makeNewRef(diffSymbolTable);
                        }
                    }
                    if (newVectIndex != null) {
                        // Peel off "op_address" levels and count them:
                        int addressOfs = 0;
                        while (diffVarRef.opCode() == ILLang.op_address) {
                            ++addressOfs;
                            diffVarRef = diffVarRef.down(1);
                        }
                        // Before adding the vector index, replace array ref A by vectorial array access A(:,:)
                        if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)
                            && diffVarRef.opCode() != ILLang.op_arrayAccess) {
                            ArrayTypeSpec arrayTypeSpec;
                            if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
                            } else {
                                arrayTypeSpec = (ArrayTypeSpec) ((PointerTypeSpec) typeSpec.wrappedType).
                                    destinationType.wrappedType;
                            }
                            Tree diffDimensions = ILUtils.build(ILLang.op_expressions);
                            int dim = arrayTypeSpec.dimensions().length;
                            Tree explicitedDim;
                            if (contextExpression.opCode() == ILLang.op_equivalence) {
                                explicitedDim = ILUtils.build(ILLang.op_intCst, 1);
                            } else {
                                explicitedDim = ILUtils.build(ILLang.op_arrayTriplet);
                            }
                            while (dim > 0) {
                                diffDimensions.addChild(ILUtils.copy(explicitedDim), 1);
                                dim = dim - 1;
                            }
                            diffVarRef = ILUtils.build(ILLang.op_arrayAccess,
                                           diffVarRef, diffDimensions);
                        }
                        if (diffVarRef.opCode() == ILLang.op_arrayAccess) {
                            diffVarRef.down(2).addChild(newVectIndex, -1);
                        } else if (curDiffUnit().isC()) {
                            if (expression.opCode() == ILLang.op_pointerAccess
                                && expression.down(2).opCode() == ILLang.op_none) {
                                diffVarRef = ILUtils.build(ILLang.op_pointerAccess,
                                               diffVarRef, newVectIndex);
                            } else if (expression.opCode() == ILLang.op_pointerDeclarator) {
                                diffVarRef = ILUtils.build(ILLang.op_arrayAccess,
                                               ILUtils.copy(diffVarRef.down(1)),
                                               ILUtils.build(ILLang.op_expressions, newVectIndex));
                            } else {
                                diffVarRef = ILUtils.build(ILLang.op_pointerAccess,
                                               diffVarRef, newVectIndex);
                            }
                        } else if (!TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                            diffVarRef = ILUtils.build(ILLang.op_arrayAccess, diffVarRef,
                                           ILUtils.build(ILLang.op_expressions, newVectIndex));
                        }
                        // Place back the "op_address" levels:
                        while (addressOfs > 0) {
                            diffVarRef = ILUtils.build(ILLang.op_address, diffVarRef);
                            --addressOfs;
                        }
                    }
                }
                if (origDiffAllocate != null) {
                    if (!ILUtils.isNullOrNone(origDiffAllocate.down(1))) {
                        Tree nbDirsMaxExpr;
                        if (TapEnv.get().fixedNbdirsmaxString != null) {
                            nbDirsMaxExpr = ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsmaxString);
                        } else {
                            nbDirsMaxExpr = dirNumberMaxSH.makeNewRef(diffSymbolTable);
                        }
                        origDiffAllocate.setChild(
                            ILUtils.build(ILLang.op_mul,
                                    origDiffAllocate.cutChild(1),
                                    nbDirsMaxExpr),
                            1);
                    }
                    origDiffAllocate.setChild(diffVarRef.cutChild(2), 2);
                    diffVarRef = origDiffAllocate;
                }
            }
        }
        return diffVarRef;
    }

    private Tree diffVarRefRec(ActivityPattern pattern, Tree expression, SymbolTable diffSymbolTable,
                               boolean identifyFunctionResult, Tree contextExpression, Tree hintRootTree) {
        switch (expression.opCode()) {
            case ILLang.op_allocate: {
                Tree diffTypeTree = differentiateTypeTree(expression.down(3), diffSymbolTable);
                Tree treeSIZEOF = ILUtils.copy(expression.down(1));
                Tree sizeofCall = ILUtils.findSizeofCall(treeSIZEOF);
                if (sizeofCall != null) {
                    sizeofCall.setChild(differentiateTypeTree(sizeofCall.cutChild(1), diffSymbolTable), 1);
                }
                return ILUtils.build(ILLang.op_allocate,
                        treeSIZEOF,
                        ILUtils.copy(expression.down(2)),
                        diffTypeTree,
                        ILUtils.copy(expression.down(4)),
                        ILUtils.build(ILLang.op_none));
            }
            case ILLang.op_deallocate:
            case ILLang.op_address:
            case ILLang.op_referenceDeclarator:
            case ILLang.op_pointerDeclarator: {
                return ILUtils.build(expression.opCode(),
                        diffVarRefRec(pattern, expression.down(1), diffSymbolTable,
                                identifyFunctionResult, contextExpression, hintRootTree));
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_sizedDeclarator: {
                return ILUtils.build(expression.opCode(),
                        diffVarRefRec(pattern, expression.down(1), diffSymbolTable,
                                identifyFunctionResult, contextExpression, hintRootTree),
                        ILUtils.copy(expression.down(2)));
            }
            case ILLang.op_arrayDeclarator: {
                // In F90, in the (rare) case where a parameter array A(* or :) is not
                // active as a parameter, but is locally active inside the subroutine,
                // the Ad variable is local and must be declared as Ad(ISIZEnOFA)
                Tree result;
                Tree[] oldDims = expression.down(2).children();
                TapList<Tree> newDims = null;
                Tree newDim;
                if (TapEnv.relatedLanguageIsFortran9x() &&
                        !WrapperTypeSpec.hasPointerOnTop(adEnv.curSymbolTable().typeOf(expression.down(1))) &&
                        paramHasOnlyLocalDiff(pattern, expression.down(1), null)) {
                    String hintNameInIdent = ILUtils.baseName(expression.down(1));
                    String hintNameInText = hintNameInIdent;
                    Tree hintTreeSize = ILUtils.baseTree(expression.down(1));
                    if (NewSymbolHolder.isANewSymbolRef(expression.down(1))) {
                        NewSymbolHolder exprSymbolHolder =
                                NewSymbolHolder.getNewSymbolHolder(expression.down(1));
                        Tree origHintRootTree = exprSymbolHolder.hintRootTree;
                        hintNameInIdent = ILUtils.buildNameInText(origHintRootTree);
                        hintNameInText = ILUtils.buildNameInText(origHintRootTree);
                        hintTreeSize = exprSymbolHolder.hintArrayTreeForCallSize;
                    }
                    for (int i = oldDims.length - 1; i >= 0; --i) {
                        if (ILUtils.isNullOrNoneOrStar(oldDims[i])
                                || oldDims[i].opCode() == ILLang.op_dimColon
                                && ILUtils.isNullOrNoneOrStar(oldDims[i].down(2))) {
                            Tree newInit = ILUtils.build(ILLang.op_none);
                            if (oldDims[i] != null && oldDims[i].opCode() == ILLang.op_dimColon) {
                                newInit = ILUtils.copy(oldDims[i].down(1));
                            }
                            IterDescriptor dimIterDescriptor =
                                    new IterDescriptor(0, diffSymbolTable, null, null,
                                            expression, hintNameInText, hintNameInIdent, hintTreeSize, i + 1, oldDims.length);
                            dimIterDescriptor.preciseDefaultLengthTree(curDiffUnit(), diffSymbolTable);
                            newDim = ILUtils.build(ILLang.op_dimColon,
                                    newInit,
                                    dimIterDescriptor.getLengthTree());
                        } else {
                            newDim = ILUtils.copy(oldDims[i]);
                        }
                        newDims = new TapList<>(newDim, newDims);
                    }
                }
                result = ILUtils.build(expression.opCode(),
                        diffVarRefRec(pattern, expression.down(1), diffSymbolTable,
                                identifyFunctionResult, contextExpression, hintRootTree),
                        newDims == null ?
                                ILUtils.copy(expression.down(2)) :
                                ILUtils.build(ILLang.op_dimColons, newDims));
                return result;
            }
            case ILLang.op_modifiedDeclarator: {
                return ILUtils.build(expression.opCode(),
                        ILUtils.copy(expression.down(1)),
                        diffVarRefRec(pattern, expression.down(2), diffSymbolTable,
                                identifyFunctionResult, contextExpression, hintRootTree));
            }
            case ILLang.op_assign: {
                return diffVarRefRec(pattern, expression.down(1), diffSymbolTable,
                        identifyFunctionResult, contextExpression, hintRootTree);
            }
            case ILLang.op_ident: {
                Tree result;
                result = diffSymbolTree(pattern, expression, diffSymbolTable,
                        true, identifyFunctionResult, false, contextExpression,
                        false, curDiffVarSort(), curDiffUnitSort(),
                        DiffConstants.VAR, hintRootTree);
                return result;
            }
            case ILLang.op_add:
            case ILLang.op_sub: {
                //This can happen with C pointer arithmetic
                boolean argIsPointer = TypeSpec.isA(adEnv.curSymbolTable().typeOf(expression.down(1)), SymbolTableConstants.POINTERTYPE);
                Tree diffRef1 = null;
                if (argIsPointer) {
                    diffRef1 = diffVarRefRec(pattern, expression.down(1), diffSymbolTable,
                            identifyFunctionResult, contextExpression, hintRootTree);
                }
                if (diffRef1 == null) {
                    diffRef1 = ILUtils.copy(expression.down(1));
                }
                argIsPointer = TypeSpec.isA(adEnv.curSymbolTable().typeOf(expression.down(2)), SymbolTableConstants.POINTERTYPE);
                Tree diffRef2 = null;
                if (argIsPointer) {
                    diffRef2 = diffVarRefRec(pattern, expression.down(2), diffSymbolTable,
                            identifyFunctionResult, contextExpression, hintRootTree);
                }
                if (diffRef2 == null) {
                    diffRef2 = ILUtils.copy(expression.down(2));
                }
                return ILUtils.build(expression.opCode(), diffRef1, diffRef2);
            }
            case ILLang.op_intCst: {
                //This can happen with C pointer arithmetic
                return null;
            }
            case ILLang.op_none: {
                return null;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast: {
                Tree diffRef2 = diffVarRefRec(pattern, expression.down(2), diffSymbolTable,
                        identifyFunctionResult, contextExpression, hintRootTree);
                if (diffRef2 == null) {
                    diffRef2 = ILUtils.copy(expression.down(2));
                }
                return ILUtils.build(expression.opCode(), ILUtils.copy(expression.down(1)), diffRef2);
            }
            case ILLang.op_functionDeclarator: {
                TapEnv.toolWarning(-1, "(Differentiate reference) Unexpected operator: " + expression.opName());
                return ILUtils.copy(expression);
            }
            case ILLang.op_call: {
                // TODO, cf C lh21
                TapEnv.toolWarning(-1, "(Differentiate reference) Unexpected operator: " + expression.opName());
                return null;
            }
            case ILLang.op_arrayConstructor: {
                // TODO, cf C v251
                TapEnv.toolWarning(-1, "(Differentiate reference) Unexpected operator: " + expression.opName());
                return null;
            }
            default:
                TapEnv.toolWarning(-1, "(Differentiate reference) Unexpected operator: " + expression.opName());
                return null;
        }
    }

    /**
     * For association by address.
     *
     * @param expression      source Tree.
     * @param diffSymbolTable differentiated SymbolTable.
     * @param diffVarRef      differentiated Tree.
     * @param vORd            value or diff
     * @param checkActive when true, does the work only after checking activity.
     *  Otherwise always does the work, which I believe should always be the case...
     *  TODO: check this and then simplify!
     * @return if expression is x, returns x.v or x.d.
     */
    protected Tree buildAAInstructionVORD(Tree expression, SymbolTable diffSymbolTable,
                                          Tree diffVarRef, String vORd, boolean checkActive) {

        String varName = ILUtils.baseName(expression);
        VariableDecl varDecl = diffSymbolTable.getVariableDecl(varName);
        Unit copyUnit = adEnv.getPrimalCopyOfOrigUnit(adEnv.curUnit());
        if (checkActive ? (varDecl != null && (varDecl.isActive() || !TapEnv.doActivity())) : true) {
            WrapperTypeSpec baseTypeSpec;
            WrapperTypeSpec diffTypeSpec = varDecl.type().wrappedType.diffTypeSpec;
            WrapperTypeSpec rootDiffTypeSpec;
            if (diffTypeSpec == null) {
                diffTypeSpec = varDecl.type() ;
            }
            rootDiffTypeSpec = diffTypeSpec;
            if (copyUnit != curDiffUnit() && diffTypeSpec != null) {
                baseTypeSpec = diffTypeSpec.baseTypeSpec(false);
            } else {
                baseTypeSpec = varDecl.type().baseTypeSpec(false);
                diffTypeSpec = baseTypeSpec.wrappedType.diffTypeSpec;
                if (diffTypeSpec == null) {
                    diffTypeSpec = baseTypeSpec ;
                }
                if (copyUnit != curDiffUnit() && diffTypeSpec != null) {
                    baseTypeSpec = diffTypeSpec;
                }
            }
            if (!TypeSpec.isA(baseTypeSpec, SymbolTableConstants.COMPOSITETYPE)) {
                WrapperTypeSpec modifiedBaseTypeSpec = varDecl.type().modifiedBaseTypeSpec();
                if (copyUnit != curDiffUnit() && modifiedBaseTypeSpec != null) {
                    baseTypeSpec = modifiedBaseTypeSpec.wrappedType.diffTypeSpec;
                }
            }
            if (checkActive
                ? (TypeSpec.isA(baseTypeSpec, SymbolTableConstants.COMPOSITETYPE)
                    && ((CompositeTypeSpec) baseTypeSpec.wrappedType).namedFieldDecl(vORd) != null)
                : true) {
                if (varDecl.type() != rootDiffTypeSpec && rootDiffTypeSpec != null) {
                    varDecl.setType(rootDiffTypeSpec);
                }
                return ILUtils.build(ILLang.op_fieldAccess,
                        ILUtils.copy(diffVarRef), ILUtils.build(ILLang.op_ident, vORd));
            } else {
                return ILUtils.copy(expression);
            }
        } else {
            return ILUtils.copy(expression);
        }
    }


    protected Tree differentiateTypeTree(Tree typeTree, SymbolTable diffSymbolTable) {
        WrapperTypeSpec diffTypeSpec = null;
        if (!ILUtils.isNullOrNone(typeTree)) {
            ToBool isPointer = new ToBool(false);
            WrapperTypeSpec typeSpec =
                    TypeSpec.build(typeTree, diffSymbolTable, adEnv.curInstruction(),
                            new TapList<>(null, null), new TapList<>(null, null),
                            new TapList<>(null, null), isPointer, null);
            diffTypeSpec =
                    typeSpec.differentiateTypeSpecMemo(diffSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                            suffixes()[curDiffUnitSort()][DiffConstants.TYPE],
                            false, multiDirMode(), getMultiDirDimensionMax(),
                            null, null, null, null);
            if (diffTypeSpec != null && isPointer.get()) {
                diffTypeSpec = new WrapperTypeSpec(new PointerTypeSpec(diffTypeSpec, null));
            }
        }
        return diffTypeSpec == null
                ? ILUtils.copy(typeTree)
                : diffTypeSpec.generateTree(diffSymbolTable, null, null, true, null);
    }

    /**
     * Same as diffVarRef, but modifies the given "expression" in place.
     */
    protected Tree diffVarRefNoCopy(ActivityPattern pattern, Tree expression, SymbolTable diffSymbolTable,
                                    boolean identifyFunctionResult, Tree hintRootTree) {
        switch (expression.opCode()) {
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
                diffVarRefNoCopy(pattern, expression.down(1), diffSymbolTable,
                        identifyFunctionResult, hintRootTree);
                break;
            case ILLang.op_arrayAccess: {
                boolean wasIdent = expression.down(1).opCode() == ILLang.op_ident;
                diffVarRefNoCopy(pattern, expression.down(1), diffSymbolTable,
                        identifyFunctionResult, hintRootTree);
                if (multiDirMode() && wasIdent) {
                    TypeSpec primalTypeSpec = diffSymbolTable.typeOf(expression) ;
                    if (primalTypeSpec.acceptsMultiDirDimension()) {
                        if (diffContexts.head == DiffConstants.INEXPR) {
                            //In "vector mode" for a "normal" variable reference,
                            // the X in X(i,j) has been replaced by Xd(nd)
                            //  => we must turn this Xd(i,j)(nd) into Xd(i,j,nd)
                            // on ne passe jamais ici
                            Tree diffName = expression.down(1).cutChild(1);
                            Tree index = expression.down(1).down(2).cutChild(1);
                            Tree indexes = expression.cutChild(2);
                            indexes.addChild(index, indexes.length() + 1);
                            expression.setChild(diffName, 1);
                            expression.setChild(indexes, 2);
                        } else {
                            //In "vector mode" for a parameter of a function call,
                            // the X in X(i,j) has been replaced by Xd
                            //  => we must turn this Xd(i,j)
                            //  into Xd(i,j,0) in C
                            //  into Xd(i,j,1) in Fortran
                            if (adEnv.curUnit().isC()) {
                                expression.down(2).addChild(ILUtils.build(ILLang.op_intCst, 0),
                                        expression.down(2).length() + 1);
                            } else {
                                expression.down(2).addChild(ILUtils.build(ILLang.op_intCst, 1),
                                        expression.down(2).length() + 1);
                            }
                        }
                    }
                }
                break;
            }
            case ILLang.op_ident: {
                Tree father = expression.parent();
                Tree diffSymbolTree = diffSymbolTree(pattern, expression, diffSymbolTable,
                        true, identifyFunctionResult, false,
                        father, false, curDiffVarSort(), curDiffUnitSort(),
                        DiffConstants.VAR, hintRootTree);
                WrapperTypeSpec type = diffSymbolTable.typeOf(expression);
                if (multiDirMode()
                    && diffContexts.head == DiffConstants.INEXPR
                    && type.acceptsMultiDirDimension()) {
                    // In "vector mode" for a "normal" variable reference V,
                    // V must be differentiated as Vd(nd) instead of Vd.
                    // However, pointers do not depend on nd: they are
                    // the same for every diff direction.
                    diffSymbolTree = ILUtils.build(ILLang.op_arrayAccess,
                            diffSymbolTree,
                            ILUtils.build(ILLang.op_expressions,
                                    dirIndexSymbolHolder.makeNewRef(diffSymbolTable)));
                }
                if (father == null) {
                    expression = diffSymbolTree;
                } else {
                    int rank = expression.rankInParent();
                    father.setChild(diffSymbolTree, rank);
                }
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(Differentiate reference 2) Unexpected operator: " + expression.opName());
                break;
        }
        return expression;
    }

    /**
     * @param symbolTree               The op_ident Tree of the original, non-differentiated symbol.
     * @param diffSymbolTable          The SymbolTable of the usage place of the future diff symbol
     *                                 in the future diff Unit. It must see the declaration of the diff symbol.
     * @param searchAsVariable         True means that the given "symbolTree" may stand for a
     *                                 variable (but also maybe the function's return value).
     * @param searchAsFunction         True means that the given "symbolTree" may stand for a
     *                                 procedure or procedure interface name.
     * @param searchAsModule           True means that the given "symbolTree" may stand for a
     *                                 module name.
     * @param callTree                 When symbolTree is a procedure name, holds the procedure call Tree.
     * @param skipImportedSymbolTables When true, method will not search for
     *                                 "sibling" declarations in symbol tables that come from the USE (i.e. import) of a module.
     * @param diffSort                 The sort of differentiated symbol
     * @param diffSuffixSort           the sort of differentiation, used to look for the correct suffix.
     * @param symbolSort               the sort of symbol, used to look for the correct suffix.
     * @param hintRootTree             The tree used in the hints for unknown dimensions.
     * @return a new Tree, that holds the differentiated symbol of the given "symbolTree".
     * Takes care of distinguishing function names and variable names, takes care
     * of the function return name, which in FORTRAN is equal to the function name.
     * The new Tree is not finished (looks like #xd#), but is registered into the
     * "diffSymbolTable", so that its definitive name will be chosen at the end and this
     * new Tree will then be updated.
     */
    protected Tree diffSymbolTree(ActivityPattern pattern, Tree symbolTree, SymbolTable diffSymbolTable,
                                  boolean searchAsVariable, boolean searchAsFunction, boolean searchAsModule,
                                  Tree callTree, boolean skipImportedSymbolTables,
                                  int diffSort, int diffSuffixSort, int symbolSort, Tree hintRootTree) {
        VariableDecl varDecl;
        String symbolName;
        Tree diffTree;
        NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(symbolTree);
        if (TapEnv.associationByAddress()
                && searchAsVariable
                && !searchAsFunction) {
            diffTree = ILUtils.copy(symbolTree);
            if (sHolder != null) {
                varDecl = sHolder.newVariableDecl();
            } else {
                symbolName = ILUtils.getIdentString(symbolTree);
                varDecl = diffSymbolTable.getVariableDecl(symbolName);
            }
            if (varDecl != null) {
                varDecl.setActive();
                WrapperTypeSpec diffTypeSpec = varDecl.type().wrappedType.diffTypeSpec;
                if (diffTypeSpec != null) {
                    varDecl.setType(diffTypeSpec);
                }
            }
            return diffTree;
        }

        if (sHolder != null) {
            // If this symbol is an unfinished NewSymbolHolder for a variable,
            // use its original declaration:
            varDecl = sHolder.newVariableDecl();
            NewSymbolHolder diffSymbolHolder = varDecl.getDiffSymbolHolder(diffSort, null, adEnv.iReplic);
            if (diffSymbolHolder == null) {
                Tree origHintRootTree = sHolder.hintRootTree;
                if (origHintRootTree == null) {
                    origHintRootTree = hintRootTree;
                }
                // It would be better to take the following decision earlier:
                if (symbolSort == DiffConstants.PROC && pattern != null && pattern.isContext() && diffSuffixSort == DiffConstants.ADJOINT_FWD) {
                    // There is no split mode for "context" procedures -> use standard suffix e.g. "_B".
                    diffSort = DiffConstants.ADJOINT;
                    diffSuffixSort = DiffConstants.ADJOINT;
                }
                String diffSymbolName = TapEnv.extendStringWithSuffix(sHolder.probableName, suffixes()[diffSuffixSort][symbolSort]) ;
                // Special diff var names for mode -diffReplica N
                diffSymbolName = TapEnv.extendReplica(diffSymbolName, adEnv.iReplic) ;
                diffSymbolHolder = new NewSymbolHolder(diffSymbolName, diffSort);
                diffSymbolHolder.isDerivationFrom(varDecl, suffixes()[diffSuffixSort][symbolSort]);
                diffSymbolHolder.accumulateSymbolDecl(
                        prepareDifferentiatedSymbolDecl(pattern, varDecl, sHolder, diffSymbolTable,
                                diffSort, origHintRootTree),
                        diffSymbolTable);
                varDecl.setDiffSymbolHolder(diffSort, null, adEnv.iReplic, diffSymbolHolder);
                varDecl.setActive();
            } else {
                diffSymbolHolder.accumulateTargetForSymbolDecl(varDecl, diffSymbolTable);
            }
            diffTree = diffSymbolHolder.makeNewRef(diffSymbolTable);
            // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
            if (isOnlyIncrementedDiff(pattern, varDecl)) {
                diffSymbolHolder.setUsesRefTo();
            }
            if (diffSymbolHolder.usesRefTo() && adEnv.curUnit().isC()) {
                if (diffContexts.head == DiffConstants.INHEADER
                        || (callTree != null
                        && callTree.opCode() == ILLang.op_arrayDeclarator)) {
                    diffTree = ILUtils.build(ILLang.op_pointerDeclarator, diffTree);
                } else {
                    diffTree = ILUtils.build(ILLang.op_pointerAccess, diffTree,
                            ILUtils.build(ILLang.op_none));
                }
            }
            // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
        } else {
            symbolName = ILUtils.getIdentString(symbolTree);
            if (symbolName==null) symbolName = "UnknownProcedureName" ;
            WrapperTypeSpec[] actualTypes = null;
            // prepare the actual types of this call :
            if (callTree != null && callTree.opCode() == ILLang.op_call) {
                Tree[] children = ILUtils.getArguments(callTree).children();
                actualTypes = new WrapperTypeSpec[children.length];
                for (int i = children.length - 1; i >= 0; --i) {
                    actualTypes[i] = diffSymbolTable.typeOf(children[i]);
                }
            }
            diffTree = diffSymbolName(pattern, symbolName, diffSymbolTable,
                    searchAsVariable, searchAsFunction, searchAsModule,
                    callTree, actualTypes, skipImportedSymbolTables,
                    diffSort, diffSuffixSort, symbolSort, hintRootTree);
        }
        return diffTree;
    }

    protected Tree diffSymbolName(ActivityPattern pattern, String symbolName, SymbolTable diffSymbolTable,
                                  boolean searchAsVariable, boolean searchAsFunction, boolean searchAsModule,
                                  Tree callTree, WrapperTypeSpec[] actualTypes, boolean skipImportedSymbolTables,
                                  int diffSort, int diffSuffixSort, int symbolSort, Tree hintRootTree) {
        // It would be better to take the following decision earlier:
        if (symbolSort == DiffConstants.PROC && pattern != null && pattern.isContext() && diffSuffixSort == DiffConstants.ADJOINT_FWD) {
            // There is no split mode for "context" procedures -> use standard suffix e.g. "_B".
            diffSort = DiffConstants.ADJOINT;
            diffSuffixSort = DiffConstants.ADJOINT;
        }
        boolean traceHere = false;
// traceHere = true ;
// traceHere = ("test_in_c".equals(symbolName) || "pi_".equals(symbolName)) ;
// if (traceHere) System.out.println("  DIFFSYMBOLNAME ("+diffSort+","+diffSuffixSort+") OF ("+symbolSort+")SYMBOLNAME:"+symbolName+" AS "+searchAsVariable+searchAsFunction+searchAsModule+" FOR ACTIVITY PATTERN:"+pattern+" STARTING FROM DIFFSYMBOLTABLE:"+diffSymbolTable.addressChain()) ;
// if (traceHere) new Throwable().printStackTrace() ;
        String origName = symbolName;
        CallArrow callArrow = null;
        if (searchAsFunction && callTree != null && callTree.opCode() == ILLang.op_call) {
            callArrow = callTree.getAnnotation("callArrow");
        }
// if (traceHere) System.out.println("  CALLTREE IS "+callTree+" AND CALLARROW IS "+callArrow);
        TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls =
                collectSiblingDecls(pattern, symbolName, diffSymbolTable,
                        searchAsVariable, searchAsFunction, searchAsModule,
                        callTree, actualTypes, skipImportedSymbolTables,
                        diffSort, diffSuffixSort, symbolSort, hintRootTree);
// if (traceHere) System.out.println("  SIBLINGDECLS FOR "+symbolName+" : "+siblingDecls);
        NewSymbolHolder diffSymbolHolder =
                uniqueDiffSymbolHolder(pattern, siblingDecls, symbolName,
                        diffSymbolTable, diffSort, diffSuffixSort, symbolSort,
                        hintRootTree, callTree, searchAsFunction);
// if (traceHere) System.out.println("Unique Diff SymbolHolder:"+diffSymbolHolder) ;
        Tree diffTree;
        if (searchAsFunction && callTree != null) {
            Unit originUnit;
            int originLanguage = TapEnv.UNDEFINED;
            int destinLanguage = TapEnv.UNDEFINED;
            String name = null;
            // si mixed-language:
            // - soit renommage calcule' a partir de MixedLanguageInfos
            //   on garde la meme convention pour les diffSymbolName
            // - soit renommage avec bind(c) ou bind(c, name = '...')
            //   on garde le nom de la curDiffUnit en minuscule partout
            //   et on change tous les bind en bind(c)
            if (callTree.opCode() == ILLang.op_call) {
                if (callArrow != null) {
                    originUnit = callArrow.origin();
                    if (originUnit == null) {
                        originLanguage = adEnv.curUnit().language();
                    } else {
                        originLanguage = originUnit.language();
                    }
                    destinLanguage = callArrow.destination().language();
                    name = symbolName;
                    TapTriplet<String, String, FunctionDecl> otherlangFuncDeclName =
                            adEnv.srcCallGraph().getOtherLangFunctionDeclFromBindC(symbolName, destinLanguage);
                    boolean throughBind = (otherlangFuncDeclName != null);
                    if (throughBind) {
                        name = symbolName;
                    } else {
                        name = callArrow.destination().name();
                    }
                } else if (adEnv.curUnit() != null && symbolName.equals(adEnv.curUnit().name())
                        && adEnv.curUnit().isC()
                        && adEnv.curUnit().isCalledFromOtherLanguage()) {
                    name = adEnv.srcCallGraph().getMixedLanguageFunctionName(symbolName, TapEnv.C,
                            TapEnv.FORTRAN).first;
                    if (name == null) {
                        name = symbolName;
                    }
                    originLanguage = TapEnv.C;
                    destinLanguage = TapEnv.FORTRAN;
                }
            } else if (callTree.opCode() == ILLang.op_varDeclaration) {
                TapList<FunctionDecl> funcDecls = diffSymbolTable.getFunctionDecl(symbolName, null, null, false);
                FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                // extern declaration from C ou varDeclaration fortran d'une fonction C
                if (funcDecl == null && TapEnv.inputLanguage() == TapEnv.MIXED) {
                    funcDecls = adEnv.diffCallGraph().fortranRootSymbolTable().getFunctionDecl(symbolName, null, null, false);
                    funcDecl = funcDecls == null ? null : funcDecls.head;
                    if (funcDecl == null) {
                        funcDecls = adEnv.diffCallGraph().cRootSymbolTable().getFunctionDecl(symbolName, null, null, false);
                        funcDecl = funcDecls == null ? null : funcDecls.head;
                    }
                }
                if (blockDifferentiator().mixedLanguage(funcDecl)) {
                    if (adEnv.curUnit() == null) {
                        originLanguage = TapEnv.C;
                    } else {
                        originLanguage = adEnv.curUnit().language();
                    }
                    destinLanguage = funcDecl.unit().language();
                    name = adEnv.srcCallGraph().getMixedLanguageFunctionName(symbolName, originLanguage, destinLanguage).first;
                    if (name == null) {
                        name = symbolName;
                    }

                }
            }
            String oldDiffSymbolName = diffSymbolHolder.probableName;

            if (originLanguage != TapEnv.UNDEFINED && originLanguage != destinLanguage) {
                if (adEnv.curUnit() != null && adEnv.curUnit().isFortran()) {
                    // pb si Fortran2003, on prend le nom C (avec bindC), sinon on garde le nom Fortran:
                    String otherName = adEnv.srcCallGraph().getMixedLanguageFunctionName(name, destinLanguage,
                            originLanguage).first;
                    if (otherName != null) {
                        name = otherName;
                    }
                    if (callTree.opCode() == ILLang.op_call
                            && adEnv.diffCallGraph().getOtherLangFunctionDeclFromBindC(name, TapEnv.FORTRAN) == null) {
                        name = origName;
                    }
                }
                String diffFuncName =
                        TapEnv.extendStringWithSuffix(name, suffixes()[diffSuffixSort][symbolSort]);
                if (adEnv.curUnit() == null || adEnv.curUnit().isC()) {
                    if (adEnv.diffCallGraph().getOtherLangFunctionDeclFromBindC(name, TapEnv.FORTRAN) == null) {
                        diffFuncName = adEnv.diffCallGraph().getMixedLanguageFunctionName(diffFuncName,
                                destinLanguage, originLanguage).first;
                    }
                }
                if (!diffFuncName.equals(diffSymbolHolder.probableName)) {
                    diffSymbolHolder.probableName = diffFuncName;
                    FunctionDecl diffFuncDecl = diffSymbolHolder.newFunctionDecl();
                    if (diffFuncDecl != null) {
                        diffFuncDecl.symbol = "#" + diffFuncName + "#";
                    }
                }
            }

            diffTree = diffSymbolHolder.makeNewRef(diffSymbolTable);
            diffSymbolHolder.probableName = oldDiffSymbolName;
        } else if (searchAsVariable) {
            VariableDecl cVarDecl = adEnv.srcCallGraph().cRootSymbolTable().getVariableDecl(symbolName);
            if (cVarDecl!=null && cVarDecl.underscoreToFortran) {
                String fName = cVarDecl.symbol ;
                fName = adEnv.diffCallGraph().getOtherLanguageName(fName, TapEnv.C, TapEnv.FORTRAN) ;
                // fName is actually a COMMON name: use diffSuffix for PROC:
                String diffVarName = fName+suffixes()[diffSuffixSort][DiffConstants.PROC];
                diffVarName = adEnv.diffCallGraph().getOtherLanguageName(diffVarName, TapEnv.FORTRAN, TapEnv.C);
                diffSymbolHolder.probableName = diffVarName;
            }
            diffTree = diffSymbolHolder.makeNewRef(diffSymbolTable);
        } else {
            diffTree = diffSymbolHolder.makeNewRef(diffSymbolTable);
        }

        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        if (diffSymbolHolder.usesRefTo() && adEnv.curUnit().isC()) {
            if (diffContexts.head == DiffConstants.INHEADER ||
                    callTree != null
                            && callTree.opCode() == ILLang.op_arrayDeclarator) {
                diffTree = ILUtils.build(ILLang.op_pointerDeclarator, diffTree);
            } else {
                diffTree = ILUtils.build(ILLang.op_pointerAccess, diffTree,
                        ILUtils.build(ILLang.op_none));
            }
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
// if (traceHere) System.out.println("  --> DIFFSYMBOLNAME: "+diffTree+" @"+Integer.toHexString(diffTree.hashCode())+" from diffSymbolHolder@"+Integer.toHexString(diffSymbolHolder.hashCode())+" : "+diffSymbolHolder+" registered in ST:"+diffSymbolTable) ;
        return diffTree;
    }

    /**
     * "Sibling" declarations of a given symbol name are SymbolDecls that
     * declare a symbol with this name and that really designate the same
     * object i.e., if we want to change the symbol name, all its siblings
     * must change accordingly.
     * Caution: SymbolDecl's are found in the diffSymbolTable's, not in the
     * origSymbolTable's of the original Units.
     *
     * @return The collection of "sibling" declarations of the "symbolName"
     * we are looking for. It is a TapList of TapPair, with
     * first element a SymbolTable (rootmost last), and  second element
     * the TapList of the SymbolDecls that come from this SymbolTable.
     */
    private TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> collectSiblingDecls(
            ActivityPattern pattern, String symbolName, SymbolTable diffSymbolTable,
            boolean searchAsVariable, boolean searchAsFunction, boolean searchAsModule,
            Tree callTree, WrapperTypeSpec[] actualTypes, boolean skipImportedSymbolTables,
            int diffSort, int diffSuffixSort, int symbolSort, Tree hintRootTree) {
// System.out.println("COLLECTING SIBLINGDECLS OF "+symbolName+" WITH DIFFST: "+diffSymbolTable) ;
// new Throwable().printStackTrace() ;
        TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls = new TapList<>(null, null);
        SymbolTable searchedST = diffSymbolTable;
        SymbolTable nextST = null;
        boolean searchInOtherRootLanguageST = TapEnv.inputLanguage() == TapEnv.MIXED;
        while (searchedST != null && (searchAsVariable || searchAsFunction || searchAsModule)) {
            VariableDecl variableDecl = null;
            FunctionDecl functionDecl = null;
            InterfaceDecl interfaceDecl = null;
            FunctionDecl moduleDecl = null; //[llh] TODO: should become a ModuleDecl.
            if (searchAsVariable) {
                variableDecl = searchedST.getTopVariableOrConstantDecl(symbolName);
            }
            if (searchAsFunction) {
                // First, search paying attention to argument types:
                TapList<FunctionDecl> functionDecls =
                    searchedST.getTopFunctionDecl(symbolName, null, actualTypes, null, true);
                // As a fallback, search only with function name:
                if (functionDecls == null) {
                    functionDecls = searchedST.getTopFunctionDecl(symbolName, null, null, null, false);
                }
                functionDecl = (functionDecls == null ? null : functionDecls.head);
                if (functionDecl != null && functionDecl.isVarFunction()) {
                    searchAsFunction = false;
                }
                interfaceDecl = searchedST.getTopInterfaceDecl(symbolName);
            }
            if (searchAsModule) {
                moduleDecl = searchedST.getTopModuleDecl(symbolName);
            }
            if (variableDecl != null) {
                SymbolDecl.collectSiblingDecl(siblingDecls, variableDecl, searchedST);
                if (searchedST.isImports) {
                    if (variableDecl.importedFrom!=null
                        && variableDecl.importedFrom.first!=null
                        && !symbolName.equals(variableDecl.importedFrom.first.symbol)) {
                        // if this symbol was renamed ("USE module localName=>globalName"),
                        // and we must ask to differentiate "globalName" in its definition module
                        // and differentiate "localName" only localy here at the USE'ing place:
                        diffSymbolName(pattern, variableDecl.importedFrom.first.symbol,
                                       variableDecl.importedFrom.second, searchAsVariable, searchAsFunction, searchAsModule,
                                       callTree, actualTypes, false, diffSort, diffSuffixSort, symbolSort, hintRootTree);
                    } else {
                        // ... else it was not renamed, and we must incorporate all original
                        // SymbolDecl's with this name into the current list of siblingDecls,
                        // and stop searching other sibling declarations in enclosing SymbolTables.
                        SymbolDecl.collectImportedSiblingsDecls(variableDecl, siblingDecls) ;
                    }
                    searchAsVariable = false;
                } else {
                    if (adEnv.curUnit() != null && adEnv.curUnit().isFortran() && symbolName.equals(adEnv.curUnit().name())
                            && (curDiffUnitSort() == DiffConstants.TANGENT || pattern != null && pattern.isContext() && adEnv.curUnit().isAContext())
                            && adEnv.procedureCallDifferentiator.diffFunctionIsFunction(adEnv.curUnit())) {
                        // If we are in the case where the future diff variable name must be
                        // the same as the future diff function name, i.e. if
                        // -- we are in FORTRAN
                        // -- and we are differentiating the body of a Function,
                        // -- and variable symbolName is the name (i.e. return value) of this Function
                        // -- and we are not in the reverse mode (reverse produces no functions)
                        // -- and not in vector tangent (a function cannot return an array)
                        // -- and more generally the future diff procedure can be a function
                        // then we must go on looking for the funcDecl:
                        searchAsFunction = true;
                        searchAsVariable = false;
                    } else if (variableDecl.isShared()) {
                        // If this variable is a C extern, we must go on looking till we find
                        // its main SymbolDecl, which is in the C language SymbolTable.
                        // Therefore all the searchAs* remain unchanged.
                    } else {
                        // Otherwise stop searching:
                        searchAsFunction = false;
                        searchAsVariable = false;
                    }
                }
            }
            if (interfaceDecl != null) {
                SymbolDecl.collectSiblingDecl(siblingDecls, interfaceDecl, searchedST);
                if (!searchedST.isImports) {
                    if (callTree != null && actualTypes != null) {
                        FunctionDecl funcDeclI =
                                interfaceDecl.findFunctionDecl(actualTypes, null, callTree);
                        // if symbolName is actually an alias to funcDeclI,...
                        if (funcDeclI != null
                                && !funcDeclI.symbol.equals(symbolName)
                                && !funcDeclI.symbol.equals(interfaceDecl.symbol)) {
                            // ...then differentiate the aliased name, and stop
                            //  searching for declarations of symbolName above here:
                            diffSymbolName(pattern, funcDeclI.symbol, searchedST, false, true, false,
                                    callTree, actualTypes, false, diffSort, diffSuffixSort, symbolSort, hintRootTree);
                            searchAsFunction = false;
                        }
                    }
                } else {

                    if (interfaceDecl.importedFrom!=null
                        && interfaceDecl.importedFrom.first!=null
                        && !symbolName.equals(interfaceDecl.importedFrom.first.symbol)) {
                        diffSymbolName(pattern, interfaceDecl.importedFrom.first.symbol,
                                interfaceDecl.importedFrom.second, searchAsVariable,
                                searchAsFunction, searchAsModule, callTree,
                                actualTypes, false, diffSort, diffSuffixSort, symbolSort, hintRootTree);
                    } else {
                        SymbolDecl.collectImportedSiblingsDecls(interfaceDecl, siblingDecls) ;
                    }
                    searchAsFunction = false;
                }
                if (TapEnv.inputLanguage() == TapEnv.MIXED) {
                    FunctionDecl cfuncDecl;
                    TapTriplet<String, String, FunctionDecl> otherFun =
                            adEnv.diffCallGraph().getOtherLangFunctionDeclFromBindC(symbolName, TapEnv.C);
                    if (otherFun != null) {
                        cfuncDecl = otherFun.third;
                        if (cfuncDecl != null) {
                            // ???
                            //SymbolDecl.collectSiblingDecl(siblingDecls, cfuncDecl, diffCallGraph().cRootSymbolTable);
                        }
                    }
                }
            }
            if (functionDecl != null) {
                FunctionDecl otherFuncDecl = null;
                SymbolDecl.collectSiblingDecl(siblingDecls, functionDecl, searchedST);
                if (!searchedST.isImports) {
                    if (functionDecl.unit() != null
                            && adEnv.curUnit() != null
                            && functionDecl.unit().language() != adEnv.curUnit().language()
                            && functionDecl.importedFrom != null && functionDecl.importedFrom.first != null
                            && !functionDecl.importedFrom.first.symbol.equals(symbolName)) {
                        //[llh 9Dec22] beware (e.g set10/mix37) originalUnit seems wrong, belongs to origCallGraph!
// System.out.println(" FUNCTIONDECL IS "+functionDecl+" FOUND IN "+searchedST) ;
// System.out.println("   IS IMPORTED FROM "+functionDecl.importedFrom.first+" FROM ST:"+functionDecl.importedFrom.second) ;
                        Unit originalUnit = ((FunctionDecl) functionDecl.importedFrom.first).unit();
                        diffSymbolName(pattern, functionDecl.importedFrom.first.symbol,
                                functionDecl.importedFrom.second, //was originalUnit.publicSymbolTable(),
                                searchAsVariable, searchAsFunction, searchAsModule, originalUnit.headTree(), //was callTree,
                                actualTypes, false, diffSort, diffSuffixSort, symbolSort, hintRootTree);

// [llh] Don't stop searching up for another FunctionDecl, otherwise you get wrong, too early solves of NewSymbolHolders!
//                         searchAsFunction = false ;
                    }
                    if (isTopDeclarationLevel(searchedST, functionDecl)
                            || searchedST.basisSymbolTable() == adEnv.diffCallGraph().globalRootSymbolTable()) {
                        searchAsFunction = false;
                    }
                } else {

                    if (functionDecl.importedFrom!=null
                        && functionDecl.importedFrom.first!= null
                        && !symbolName.equals(functionDecl.importedFrom.first.symbol)) {
                        diffSymbolName(pattern, functionDecl.importedFrom.first.symbol,
                                       functionDecl.importedFrom.second, searchAsVariable,
                                       searchAsFunction, searchAsModule, callTree,
                                       actualTypes, false, diffSort, diffSuffixSort, symbolSort, hintRootTree) ;
                    } else {
                        SymbolDecl.collectImportedSiblingsDecls(functionDecl, siblingDecls) ;
                    }
                    searchAsFunction = false;
                }
                if (TapEnv.inputLanguage() == TapEnv.MIXED) {
                    int otherLang = TapEnv.FORTRAN;
                    if (functionDecl.unit().isFortran()) {
                        otherLang = TapEnv.C;
                    }
                    TapTriplet<String, String, FunctionDecl> otherFun = adEnv.diffCallGraph().getOtherLangFunctionDeclFromBindC(symbolName, otherLang);
                    if (otherFun != null) {
                        otherFuncDecl = otherFun.third;
                        if (otherFuncDecl != null && otherFuncDecl.unit().isFortran2003()
                                && !otherFuncDecl.unit().isInterface()) {
                            SymbolDecl.collectSiblingDecl(siblingDecls, otherFuncDecl, adEnv.diffCallGraph().languageRootSymbolTable(otherLang));
                        }
                    }
                }
                if (!searchAsFunction && otherFuncDecl == null) {
                    if (callTree != null && callTree.opCode() == ILLang.op_interface) {
                        searchAsFunction = true;
                    }
                }
            }
            if (moduleDecl != null) {
                SymbolDecl.collectSiblingDecl(siblingDecls, moduleDecl, searchedST);
                if (searchedST != adEnv.diffCallGraph().languageRootSymbolTable(TapEnv.relatedLanguage())) {
                    nextST = adEnv.diffCallGraph().languageRootSymbolTable(TapEnv.relatedLanguage());
                }
            }
            // Continue searching in the SymbolTables above:
            if (nextST != null) {
                searchedST = nextST;
                nextST = null;
            } else {
                searchedST = searchedST.basisSymbolTable();
                if (skipImportedSymbolTables) {
                    while (searchedST != null && searchedST.isImports) {
                        searchedST = searchedST.basisSymbolTable();
                    }
                }
                if (searchedST == null && searchInOtherRootLanguageST) {
                    int lang = TapEnv.relatedLanguage();
                    if (lang == TapEnv.C) {
                        lang = TapEnv.FORTRAN;
                    } else {
                        lang = TapEnv.C;
                    }
                    searchedST = adEnv.diffCallGraph().languageRootSymbolTable(lang);
                    searchInOtherRootLanguageST = false;
                }
            }
        }
// System.out.println("DONE COLLECTING: "+siblingDecls.tail) ;
        return siblingDecls.tail;
    }

    /**
     * Really differentiate the (unique) symbol object referred to by all "siblingDecls".
     *
     * @return the NewSymbolHolder that holds the differentiated symbol.
     */
    private NewSymbolHolder uniqueDiffSymbolHolder(ActivityPattern pattern,
                                                   TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls,
                                                   String sName,
                                                   SymbolTable origUseSymbolTable, int diffSort,
                                                   int diffSuffixSort, int symbolSort, Tree hintRootTree,
                                                   Tree callTree, boolean searchAsFunction) {
        NewSymbolHolder diffSymbolHolder;
        TapPair<SymbolTable, TapList<SymbolDecl>> oneSTSymbolDecls;
        TapList<SymbolDecl> symbolDecls;
        SymbolDecl oneSymbolDecl, mainDecl;
        String suffix;
        TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> inSiblingDecls;
        SymbolTable origDeclSymbolTable, diffSeeingSymbolTable;
        // 1st sweep on siblingDecls: get the unique symbol name,
        //  suffix, and NewSymbolHolder if it already exists.
        TapTriplet<SymbolDecl, String, String> results =
                new TapTriplet<>(null, null, null);
        diffSymbolHolder = getCommonNewSymbolHolder(siblingDecls, diffSort, diffSuffixSort, symbolSort, pattern, results);
        mainDecl = results.first;
        String symbolName = results.second;
        suffix = results.third;
        // Create the NewSymbolHolder if doesn't exist yet:
        if (diffSymbolHolder == null) {
            if (symbolName == null) {
                symbolName = sName;
            }
            String diffSymbolName = TapEnv.extendStringWithSuffix(symbolName, suffix);
            if (TapEnv.inputLanguage() == TapEnv.MIXED && searchAsFunction && callTree != null) {
                // cas mixedLanguage, differentiation du nom d'une Unit C renomme'e:
                Unit calledUnit = null;
                if (adEnv.curSymbolTable() != null) {
                    TapList<FunctionDecl> funcDecls = adEnv.curSymbolTable().getFunctionDecl(symbolName, null, null, false);
                    FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                    if (funcDecl != null) {
                        calledUnit = funcDecl.unit();
                    }
                }
                if (adEnv.curUnit() != null
                        && ((symbolName.equals(adEnv.curUnit().name()) && adEnv.curUnit().isCalledFromOtherLanguage())
                        ||
                        (calledUnit != null && calledUnit.language() != adEnv.curUnit().language()))) {
                    CallArrow callArrow = callTree.getAnnotation("callArrow");
                    if (adEnv.curUnit().isC()) {
                        symbolName = adEnv.srcCallGraph().getMixedLanguageFunctionName(symbolName, adEnv.curUnit().language(),
                                TapEnv.FORTRAN).first;
                        if (symbolName == null) {
                            symbolName = adEnv.curUnit().name();
                        }
                    }
                    diffSymbolName = TapEnv.extendStringWithSuffix(symbolName, suffix);
                    if (adEnv.curUnit().isC()) {
                        if (adEnv.diffCallGraph().getOtherLangFunctionDeclFromBindC(symbolName, TapEnv.FORTRAN) == null) {
                            diffSymbolName = adEnv.diffCallGraph().getMixedLanguageFunctionName(diffSymbolName, TapEnv.FORTRAN,
                                    adEnv.curUnit().language()).first;
                        }
                    }
                }
            }
            if (mainDecl != null && mainDecl.isA(SymbolTableConstants.FUNCTION) && pattern != null && pattern.diffPattern() != null && pattern.diffPattern().casN() != null) {
                diffSymbolName = diffSymbolName + pattern.diffPattern().casN();
            }
            if (mainDecl != null && mainDecl.isA(SymbolTableConstants.VARIABLE)) {
                // Special diff var names for mode -diffReplica N
                diffSymbolName = TapEnv.extendReplica(diffSymbolName, adEnv.iReplic) ;
            }
            diffSymbolHolder = new NewSymbolHolder(diffSymbolName, diffSort);
            diffSymbolHolder.isDerivationFrom(mainDecl, suffix);
        }
        // 2nd sweep on siblingDecls: link the NewSymbolHolder "diffSymbolHolder"
        // to all sibling SymbolDecl's that don't have it yet, in both directions.
        inSiblingDecls = siblingDecls;
        while (inSiblingDecls != null) {
            oneSTSymbolDecls = inSiblingDecls.head;
            origDeclSymbolTable = oneSTSymbolDecls.first;
            symbolDecls = oneSTSymbolDecls.second;
            while (symbolDecls != null) {
                oneSymbolDecl = symbolDecls.head;
                boolean differentiatedSymbolDeclAlreadyExists =
                    (oneSymbolDecl.getDiffSymbolHolder(diffSort, pattern, adEnv.iReplic) == diffSymbolHolder) ;
                diffSeeingSymbolTable = origDeclSymbolTable;
                // Special case: for formal parameter variables, we only force
                //  the diff variable to be declared in the using scope of
                //  the orig variable, instead of in its declaration scope:
                //  this way, if a formal param is passive at entry&exit,
                //  its diff variable will be a local variable.
                if (origDeclSymbolTable.isFormalParamsLevel()
                        && oneSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                        && ((VariableDecl) oneSymbolDecl).formalArgRank > 0) {
                    diffSeeingSymbolTable = origUseSymbolTable;
                    //Caution with nested procedures cf F90:v11:
                    while (diffSeeingSymbolTable.basisSymbolTable() != null
                            && diffSeeingSymbolTable.unit != origDeclSymbolTable.unit) {
                        diffSeeingSymbolTable = diffSeeingSymbolTable.basisSymbolTable();
                    }
                }
                // If the diffSeeingSymbolTable (which is the largest SymbolTable that can see the
                // future differentiated symbol) is the public SymbolTable of a Module, then we must
                // enlarge it again to its enclosing SymbolTable (probably the Fortran root ST) because
                // the public symbols of a module are by definition visible outside this module.
                // Still, the diffSymbolHolder must remember this module, because when solving this
                // diffSymbolHolder's name, we must watch that USErs of the module don't use the
                // proposed new name:
                if (diffSeeingSymbolTable.unit != null && diffSeeingSymbolTable.unit.isModule() &&
                        diffSeeingSymbolTable.unit.publicSymbolTable() == diffSeeingSymbolTable) {
                    diffSymbolHolder.definitionModule = diffSeeingSymbolTable.unit;
                    diffSeeingSymbolTable = diffSeeingSymbolTable.basisNotImportedSymbolTable();
                    if (diffSeeingSymbolTable.isTranslationUnitSymbolTable()) {
                        diffSeeingSymbolTable = diffSeeingSymbolTable.basisSymbolTable();
                    }
                }
                if (differentiatedSymbolDeclAlreadyExists) {
                    diffSymbolHolder.accumulateTargetForSymbolDecl(
                            oneSymbolDecl, diffSeeingSymbolTable);
                } else {
                    diffSymbolHolder.accumulateSymbolDecl(
                            prepareDifferentiatedSymbolDecl(pattern, oneSymbolDecl, null, origDeclSymbolTable,
                                    diffSort, hintRootTree),
                            diffSeeingSymbolTable);
                }
                oneSymbolDecl.setDiffSymbolHolder(diffSort, pattern, adEnv.iReplic, diffSymbolHolder);
                oneSymbolDecl.setActive();
                // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                if (oneSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                        && isOnlyIncrementedDiff(pattern, (VariableDecl) oneSymbolDecl)) {
                    diffSymbolHolder.setUsesRefTo();
                }
                // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
                symbolDecls = symbolDecls.tail;
            }
            inSiblingDecls = inSiblingDecls.tail;
        }
        return diffSymbolHolder;
    }

    private NewSymbolHolder getCommonNewSymbolHolder(TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> inSiblingDecls,
                                                     int diffSort,
                                                     int diffSuffixSort, int symbolSort, ActivityPattern pattern,
                                                     TapTriplet<SymbolDecl, String, String> results) {
        NewSymbolHolder diffSymbolHolder = null;
        TapPair<SymbolTable, TapList<SymbolDecl>> oneSTSymbolDecls;
        TapList<SymbolDecl> symbolDecls;
        SymbolDecl oneSymbolDecl;
        String oneSymbolName;
        String symbolName = null;
        String suffix = suffixes()[diffSuffixSort][symbolSort];
        NewSymbolHolder oneDiffSymbolHolder;
        SymbolDecl mainDecl = null;
        while (inSiblingDecls != null) {
            oneSTSymbolDecls = inSiblingDecls.head;
            symbolDecls = oneSTSymbolDecls.second;
            while (symbolDecls != null) {
                oneSymbolDecl = symbolDecls.head;
                oneSymbolName = oneSymbolDecl.symbol;
                // find the (hopefully!) unique symbol name:
                if (symbolName == null) {
                    symbolName = oneSymbolName;
                } else if (!symbolName.equals(oneSymbolName)) {
                    TapEnv.toolWarning(-1, "(Differentiate variable reference): Incoherent symbols collected");
                }
                // find the (hopefully) unique NewSymbolHolder of the future diff symbol:
                oneDiffSymbolHolder = oneSymbolDecl.getDiffSymbolHolder(diffSort, pattern, adEnv.iReplic);
                if (oneDiffSymbolHolder != null) {
                    if (diffSymbolHolder == null) {
                        diffSymbolHolder = oneDiffSymbolHolder;
                    } else if (oneDiffSymbolHolder != diffSymbolHolder) {
                        diffSymbolHolder.absorb(oneDiffSymbolHolder);//TODO if happens!
                    }
                }
                if (oneSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                    mainDecl = oneSymbolDecl;
                } else if (oneSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                    if (mainDecl == null) {
                        mainDecl = oneSymbolDecl;
                    }
                } else if (oneSymbolDecl.isA(SymbolTableConstants.TYPE)) {
                    mainDecl = oneSymbolDecl;
                } else {
                    if (mainDecl == null) {
                        mainDecl = oneSymbolDecl;
                    }
                }
                //Specially for Fortran, the diff of a variable name which is the same as the function return value,
                // must be differentiated as a function name (e.g. *_B instead of *b) :
                if (symbolSort == DiffConstants.VAR && curDiffUnit() != null && curDiffUnit().isFortran()
                        && !oneSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                        && !oneSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
                    suffix = suffixes()[diffSuffixSort][DiffConstants.PROC];
                }
                symbolDecls = symbolDecls.tail;
            }
            inSiblingDecls = inSiblingDecls.tail;
        }
        results.first = mainDecl;
        results.second = symbolName;
        results.third = suffix;
        return diffSymbolHolder;
    }

    /**
     * Really differentiate "nonDiffSymbolDecl", which belongs to "declSymbolTable",
     * which is a SymbolTable of the differentiated code internal representation.
     * Creates the new SymbolDecl for the differentiated symbol.
     * Gives it an "extraInfo" which is cleaned from the one of "nonDiffSymbolDecl".
     * Creates and attaches to it its diff WrapperTypeSpec when appropriate.
     * Attaches to it its diff Unit or Units when appropriate.
     *
     * @return the corresponding differentiated SymbolDecl.
     */
    private SymbolDecl prepareDifferentiatedSymbolDecl(ActivityPattern pattern, SymbolDecl nonDiffSymbolDecl,
                                                       NewSymbolHolder nonDiffSymbolHolder, SymbolTable declSymbolTable,
                                                       int diffSort, Tree hintRootTree) {
        TapList<SymbolDecl> diffDependsOn = nonDiffSymbolDecl.dependsOn;
        TapList<String> diffExtraInfo = TapList.append(nonDiffSymbolDecl.extraInfo(), null);
        TapList<String> diffAccessInfo = TapList.append(nonDiffSymbolDecl.accessInfo, null);
        SymbolDecl diffSymbolDecl;
        if (nonDiffSymbolDecl.isA(SymbolTableConstants.VARIABLE) || nonDiffSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
            VariableDecl nonDiffVarDecl = (VariableDecl) nonDiffSymbolDecl;
            WrapperTypeSpec nonDiffTypeSpec = nonDiffVarDecl.type();
            Tree nonDiffTree = ILUtils.build(ILLang.op_ident, nonDiffVarDecl.symbol);
            boolean localDecl = !declSymbolTable.isFormalParamsLevel()
                    || paramHasOnlyLocalDiff(pattern, nonDiffTree, (VariableDecl) nonDiffSymbolDecl);
            Tree newSHTree = null;
            if (nonDiffSymbolHolder != null) {
                newSHTree = nonDiffSymbolHolder.makeNewRef(declSymbolTable);
            }
            String nameInText = nonDiffVarDecl.symbol;
            String nameInIdent = nonDiffVarDecl.symbol;
            if (hintRootTree != null) {
                nameInText = ILUtils.buildNameInText(hintRootTree);
                nameInIdent = ILUtils.buildNameInIdent(hintRootTree);
            }
            // There are cases where the function has a passive result, and still is differentiated
            // as a function (e.g. context foo_b). In that case the differentiated function's name
            // must not be declared with a differentiated type!
            boolean isPassiveReturnVariable = false;
            if (((VariableDecl) nonDiffSymbolDecl).isReturnVarFromFunction != null) {
                boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(pattern);
                isPassiveReturnVariable = formalArgsActivity != null && !formalArgsActivity[formalArgsActivity.length - 1];
            }
            WrapperTypeSpec diffTypeSpec = nonDiffTypeSpec;
            if (!isPassiveReturnVariable) {
                diffTypeSpec =
                        nonDiffTypeSpec.differentiateTypeSpecMemo(declSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                                suffixes()[curDiffUnitSort()][DiffConstants.TYPE], localDecl,
                                multiDirMode(), getMultiDirDimensionMax(),
                                nameInText, nameInIdent, hintRootTree, newSHTree);
            }
            if (diffTypeSpec != null) {
                updateDiffTypeSpec(declSymbolTable, nonDiffTypeSpec.wrappedType,
                        new TapList<>(null, null));
            } else {
                diffTypeSpec = nonDiffTypeSpec;
            }
            if (multiDirMode() && TapEnv.get().fixedNbdirsmaxString == null) {
                NewSymbolHolder dirNumberMaxSH =
                    (curDiffUnit()!=null ? curDiffUnit().dirNumberMaxSymbolHolder : callGraphDifferentiator().rootDirNumberMaxSymbolHolder) ;
                if (dirNumberMaxSH != null) { // may be null if using -nbdirsmax option.
                    // When in "vector mode", the diff var of X is X_d(nd),
                    //  except when in a declaration: X_d(nbdirsmax).
                    diffDependsOn = new TapList<>(dirNumberMaxSH.newVariableDecl(), diffDependsOn);
                }
            }
            // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
            if (isOnlyIncrementedDiff(pattern, nonDiffVarDecl) && adEnv.curUnit().isC()) {
                diffTypeSpec = new WrapperTypeSpec(new PointerTypeSpec(diffTypeSpec, null));
            }
            // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
            diffSymbolDecl = new VariableDecl(null, diffTypeSpec);
            if (nonDiffVarDecl.isReference()) diffSymbolDecl.setReference() ;
            if (localDecl) {
                diffExtraInfo = TapList.cleanExtraInfoValue(diffExtraInfo, "optional");
            }
        } else if (nonDiffSymbolDecl.isA(SymbolTableConstants.FUNCNAME)) {
            TapEnv.toolWarning(-1, "Not yet implemented: prepareDifferentiatedSymbolDecl() for a FUNCNAME decl");
            diffSymbolDecl = null;
        } else if (nonDiffSymbolDecl.isA(SymbolTableConstants.FUNCTION)
            // 2nd restriction should never trigger! :
//                   && !((FunctionDecl)nonDiffSymbolDecl).unit().isInterface()
        ) {
            Unit unitOfFunc = ((FunctionDecl) nonDiffSymbolDecl).unit();
            Unit diffUnitOfFunc = null;
            Unit origUnit = unitOfFunc.origUnit != null ? unitOfFunc.origUnit : unitOfFunc;
            if (unitOfFunc.isModule()) {
                diffUnitOfFunc = adEnv.getDiffOfModule(origUnit);
            } else if (diffSort == DiffConstants.ORIGCOPY) {
                diffUnitOfFunc = adEnv.getPrimalCopyOfOrigUnit(origUnit);
            } else if (pattern != null
                    && (pattern.unit().rank() != -1
                    || pattern.unit().isIntrinsic() && !pattern.unit().hasPredefinedDerivatives())) {
                diffUnitOfFunc = adEnv.getDiffOfUnit(origUnit, pattern, diffSort);
            }
            diffSymbolDecl = new FunctionDecl(null, diffUnitOfFunc);
        } else if (nonDiffSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
            InterfaceDecl nonDiffInterfDecl = (InterfaceDecl) nonDiffSymbolDecl;
            InterfaceDecl diffInterfDecl = new InterfaceDecl(null);
            diffInterfDecl.nameTree = ILUtils.copy(nonDiffInterfDecl.nameTree);
            diffInterfDecl.functionNames = nonDiffInterfDecl.functionNames;
            Unit diffUnitOfInterface;
            TapList<FunctionDecl> inFunctionDecls = nonDiffInterfDecl.functionDecls;
            TapList<FunctionDecl> hdFunctionDecls = new TapList<>(null, null);
            TapList<FunctionDecl> tlFunctionDecls = hdFunctionDecls;
            while (inFunctionDecls != null) {
                Unit interfacedUnit = inFunctionDecls.head.unit();
                Unit primalInterfacedUnit = adEnv.getOrigUnitOfPrimalCopy(interfacedUnit);
                // bizarre case? see e.g. set06/v292
                if (primalInterfacedUnit != null) {
                    interfacedUnit = primalInterfacedUnit;
                }
                TapList<Unit> diffUnits = callGraphDifferentiator().getUnitDiffInfo(interfacedUnit).getAllDiffs(diffSort);
                while (diffUnits != null) {
                    // TODO JH we must split interfaces to avoid ambiguities
                    diffUnitOfInterface = diffUnits.head;
                    tlFunctionDecls = tlFunctionDecls.placdl(new FunctionDecl(null, diffUnitOfInterface));
                    diffUnits = diffUnits.tail;
                }
                inFunctionDecls = inFunctionDecls.tail;
            }
            diffInterfDecl.functionDecls = hdFunctionDecls.tail;
            TapList<Unit> inInterfaceUnits = nonDiffInterfDecl.interfaceUnits();
            TapList<Unit> hdInterfaceUnits = new TapList<>(null, null);
            TapList<Unit> tlInterfaceUnits = hdInterfaceUnits;
            while (inInterfaceUnits != null) {
                Unit interfacedUnit = inInterfaceUnits.head;
                Unit primalInterfacedUnit = adEnv.getOrigUnitOfPrimalCopy(interfacedUnit);
                // bizarre case? see e.g. set06/v292
                if (primalInterfacedUnit != null) {
                    interfacedUnit = primalInterfacedUnit;
                }
                TapList<Unit> diffUnits = callGraphDifferentiator().getUnitDiffInfo(interfacedUnit).getAllDiffs(diffSort);
                while (diffUnits != null) {
                    // TODO JH we must split interfaces to avoid ambiguities
                    diffUnitOfInterface = diffUnits.head;
                    if (diffUnitOfInterface.rank() != -1) {
                        tlInterfaceUnits = tlInterfaceUnits.placdl(diffUnitOfInterface);
                    }
                    diffUnits = diffUnits.tail;
                }
                inInterfaceUnits = inInterfaceUnits.tail;
            }
            diffInterfDecl.setInterfaceUnits(hdInterfaceUnits.tail);
            diffInterfDecl.contents = nonDiffInterfDecl.contents;
            differentiateInterfaceDeclContents(pattern, diffInterfDecl, declSymbolTable);
            diffSymbolDecl = diffInterfDecl;
        } else if (nonDiffSymbolDecl.isA(SymbolTableConstants.TYPE)) {
            TapEnv.toolWarning(-1, "Not yet implemented: prepareDifferentiatedSymbolDecl() for a TYPE decl");
            diffSymbolDecl = null;
        } else {
            TapEnv.toolWarning(-1, "Not yet implemented: prepareDifferentiatedSymbolDecl() for " + nonDiffSymbolDecl);
            diffSymbolDecl = null;
        }
        if (curDiffUnitSort() == DiffConstants.ADJOINT) {
            diffAccessInfo = null; //In ADJOINT, the diff variables lose their "const" or "restrict".
            if (curDiffUnit() != null && curDiffUnit().isElemental()) {
                if (nonDiffSymbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                    if (adEnv.curUnit().otherReturnVar() != null
                            && adEnv.curUnit().otherReturnVar().symbol.equals(nonDiffSymbolDecl.symbol)) {
                        diffExtraInfo = new TapList<>("in", diffExtraInfo);
                        ((VariableDecl) nonDiffSymbolDecl).isReturnVar = false;
                    } else {
                        // For the adjoint of an elemental, both in or out become inout
                        TapList.replaceExtraInfoValue(diffExtraInfo, "in", "inout");
                        TapList.replaceExtraInfoValue(diffExtraInfo, "out", "inout");
                    }
                }
            } else {
                diffExtraInfo = TapList.cleanExtraInfoValue(diffExtraInfo, "in");
                diffExtraInfo = TapList.cleanExtraInfoValue(diffExtraInfo, "out");
            }
        }
        // If differentiating wrt a "constant" variable, its diff variable is not "constant":
        diffExtraInfo = TapList.cleanExtraInfoValue(diffExtraInfo, "constant");
        diffExtraInfo = TapList.cleanExtraInfoValue(diffExtraInfo, "pure");
        if (diffSymbolDecl != null) {
            diffSymbolDecl.setExtraInfo(diffExtraInfo);
            diffSymbolDecl.accessInfo = diffAccessInfo;
            diffSymbolDecl.dependsOn = diffDependsOn;
        }
        return diffSymbolDecl;
    }

    private void updateDiffTypeSpec(SymbolTable diffSymbolTable, TypeSpec actualTypeSpec,
                                    TapList<TypeSpec> alreadySeenTypeSpec) {
        if (TypeSpec.isA(actualTypeSpec, SymbolTableConstants.COMPOSITETYPE)) {
            updateRecordDiffTypeSpec(diffSymbolTable, (CompositeTypeSpec) actualTypeSpec, alreadySeenTypeSpec);
        } else if (TypeSpec.isA(actualTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
            updateDiffTypeSpec(diffSymbolTable, actualTypeSpec.elementType().wrappedType,
                    alreadySeenTypeSpec);
        } else if (TypeSpec.isA(actualTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            updateDiffTypeSpec(diffSymbolTable, actualTypeSpec.elementType().wrappedType,
                    alreadySeenTypeSpec);
        } else if (TypeSpec.isA(actualTypeSpec, SymbolTableConstants.POINTERTYPE)) {
            if (!TapList.contains(alreadySeenTypeSpec, actualTypeSpec)) {
                alreadySeenTypeSpec.placdl(actualTypeSpec);
                updateDiffTypeSpec(diffSymbolTable,
                        ((PointerTypeSpec) actualTypeSpec).destinationType.wrappedType,
                        alreadySeenTypeSpec);
            }
        }
    }

    private void updateRecordDiffTypeSpec(SymbolTable diffSymbolTable, CompositeTypeSpec actualTypeSpec,
                                          TapList<TypeSpec> alreadySeenTypeSpec) {
        String typeDeclName = actualTypeSpec.typeDeclName();
        /* Resolve renamed types */
        String actualName = actualTypeSpec.getRenamedTypeDeclName(diffSymbolTable.origUnit());
        if (actualName != null) {
            typeDeclName = actualName;
        }
        if (typeDeclName != null) {
            TypeDecl typeDecl = diffSymbolTable.getTypeDecl(typeDeclName);
            TypeDecl mainTypeDecl = typeDecl;
            if (typeDecl != null) {
                if (typeDecl.importedFrom != null) {
                    mainTypeDecl = (TypeDecl) typeDecl.importedFrom.first ;;
                }
                NewSymbolHolder typeDeclSymbolHolder = mainTypeDecl.getDiffSymbolHolder(0, null, 0);
                if (typeDeclSymbolHolder != null
                        && actualTypeSpec.diffTypeSpec != null) {
                    actualTypeSpec.diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder = typeDeclSymbolHolder;
                    ToObject<SymbolTable> toST = new ToObject<>(null);
                    SymbolDecl typeDeclAgain =
                        diffSymbolTable.getDecl(typeDecl.symbol, SymbolTableConstants.TYPE, false, toST);
                    SymbolTable definitionST = toST.obj();
                    if (typeDeclAgain.importedFrom!=null) {
                        definitionST = typeDeclAgain.importedFrom.second ;
                    }
                    if (definitionST.basisSymbolTable() != null) {
                        Unit tmpMod = adEnv.getDiffOfModule(definitionST.origUnit());
                        if (tmpMod != null) {
                            // attention definitionST est dans srcCallGraph
                            definitionST = tmpMod.publicSymbolTable();
                        }
                        toST.setObj(null) ;
                        diffSymbolTable.getDecl(typeDeclName, SymbolTableConstants.TYPE, false, toST);
                        SymbolTable importST = toST.obj();
                        if (importST != null && importST.isImports) {
                            typeDeclSymbolHolder.addTypeDeclSymbolTable(importST);
                        }
                    }
                    typeDeclSymbolHolder.addTypeDeclSymbolTable(definitionST);
                    typeDeclSymbolHolder.makeNewRef(diffSymbolTable);
                }
            }
        }
        CompositeTypeSpec recordType = actualTypeSpec;
        FieldDecl[] fields = recordType.fields;
        if (recordType.isRecordType() && !TapList.contains(alreadySeenTypeSpec, actualTypeSpec)
                || recordType.isUnionType()) {
            for (int i = fields.length - 1; i >= 0; i--) {
                if (fields[i] != null) {
                    alreadySeenTypeSpec.placdl(fields[i].type().wrappedType);
                    updateDiffTypeSpec(diffSymbolTable, fields[i].type().wrappedType, alreadySeenTypeSpec);
                }
            }
        }
    }

    /**
     * @return false when the multi-directional dimension added to the diff of a variable of type typeSpec
     * must be accessed with a new loop on dimension index instead of a vectorial access ":".
     */
    private static boolean mayAddSimplyColon(WrapperTypeSpec typeSpec) {
        // It is forbidden to use ":" if the typeSpec
        // is not a composite nor a pointer to a non-primitive type
        // [llh] very obscure ?!?
        return  (TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE)
                 || (TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)
                     && !TypeSpec.isA(((PointerTypeSpec) typeSpec.wrappedType).destinationType, SymbolTableConstants.PRIMITIVETYPE))) ;
    }

    private void differentiateInterfaceDeclContents(ActivityPattern pattern, InterfaceDecl interfaceDecl,
                                                    SymbolTable symbolTable) {
        FunctionDecl fDecl;
        TapList<FunctionDecl> fDecls;
        TapList<Tree> contents = interfaceDecl.contents;
        TapList<Tree> diffContents = null;
        TapList<ActivityPattern> calledPatterns;
        ActivityPattern calledPattern;
        Tree diffContentTree;
        Tree diffUnitNameTree;
        String procName;
        Unit fDeclUnit;
        while (contents != null) {
            if (contents.head.opCode() == ILLang.op_moduleProcedure) {
                Tree[] children = contents.head.children();
                diffContentTree = ILUtils.build(ILLang.op_moduleProcedure);
                for (Tree son : children) {
                    if (son.opCode() == ILLang.op_ident) {
                        procName = ILUtils.getIdentString(son);
                        fDecls = symbolTable.getFunctionDecl(procName, null, null, false);
                        fDecl = fDecls == null ? null : fDecls.head;
                        if (fDecl == null) {
                            Unit origUnit = symbolTable.origUnit();
                            if (origUnit != null) {
                                fDecls = origUnit.privateSymbolTable().getFunctionDecl(procName, null, null, false);
                                fDecl = fDecls == null ? null : fDecls.head;
                            }
                        }
                        fDeclUnit = fDecl == null ? null : fDecl.unit();
                        if (fDeclUnit != null && fDeclUnit.origUnit != null) {
                            fDeclUnit = fDeclUnit.origUnit;
                        }
                        if (fDecl != null && fDeclUnit != null) {
                            calledPatterns =
                                    BlockDifferentiator.patternsCalledByPattern(fDeclUnit, pattern);
                            while (calledPatterns != null) {
                                calledPattern = calledPatterns.head;
                                diffUnitNameTree = null;
                                NewSymbolHolder diffFuncNameSH = fDecl.getDiffSymbolHolder(curDiffUnitSort(), calledPattern, 0);
                                if (diffFuncNameSH != null && diffFuncNameSH.hasNewFunctionDecl()) {
                                    diffUnitNameTree = diffFuncNameSH.makeNewRef(symbolTable);
                                } else {
                                    adEnv.pushCurDiffUnit(adEnv.getDiffOfUnit(fDeclUnit, calledPattern, curDiffUnitSort()));
                                    if (curDiffUnit() != null) {
                                        diffUnitNameTree =
                                                findDiffFuncName(calledPattern, fDecl, curDiffUnitSort(),
                                                        curDiffUnit().publicSymbolTable(), null, true, false);
                                    }
                                    adEnv.popCurDiffUnit();
                                }
                                if (diffUnitNameTree != null) {
                                    diffContentTree.addChild(diffUnitNameTree, diffContentTree.length() + 1);
                                }
                                calledPatterns = calledPatterns.tail;
                            }
                        }
                    }
                }
                if (diffContentTree.length() != 0) {
                    diffContents = TapList.append(diffContents, new TapList<>(diffContentTree, null));
                }
            }
            contents = contents.tail;
        }
        interfaceDecl.contents = diffContents;
    }

    /**
     * Utility for diffDeclNames: differentiates a function name or interface name.
     * If the differentiated newSymbolHolder is available, uses it.
     * Otherwise differentiate the name through varRefDifferentiator().diffSymbolTree().
     */
    protected Tree findDiffFuncName(ActivityPattern pattern, FunctionDecl functionDecl,
                                    int diffSort, SymbolTable diffSymbolTable, Tree declTree,
                                    boolean asAFunction, boolean asAnInterface) {
        NewSymbolHolder diffSymbolHolder = functionDecl.getDiffSymbolHolder(diffSort, pattern, 0);
        //Sometimes this symbol, although active, has not been differentiated yet !!
        if (diffSymbolHolder != null) {
            String oldDiffSymbolName = diffSymbolHolder.probableName;
            String name = functionDecl.symbol;
            int originLanguage = adEnv.curUnit().language();
            int destinLanguage = functionDecl.unit().language();
            if (originLanguage != TapEnv.UNDEFINED
                    && originLanguage != destinLanguage) {
                String suffix = suffixes()[diffSort][SymbolTableConstants.FUNCTION];
                if (adEnv.curUnit() != null && adEnv.curUnit().isFortran()) {
                    name = adEnv.srcCallGraph().getMixedLanguageFunctionName(name, destinLanguage,
                            originLanguage).first;
                }
                String diffFuncName = name + suffix; // TODO ???
                if (adEnv.curUnit() == null || adEnv.curUnit().isC()) {
                    diffFuncName = adEnv.diffCallGraph().getMixedLanguageFunctionName(diffFuncName,
                            destinLanguage,
                            originLanguage
                    ).first;
                }
                diffSymbolHolder.probableName = diffFuncName;
            }
            Tree result = diffSymbolHolder.makeNewRef(diffSymbolTable);
            diffSymbolHolder.probableName = oldDiffSymbolName;
            return result;
        } else {
            return diffSymbolTree(pattern, ILUtils.build(ILLang.op_ident, functionDecl.symbol),
                    diffSymbolTable, false, asAFunction || asAnInterface, false,
                    declTree, false, diffSort, diffSort, DiffConstants.VAR, null);
        }
    }

    /**
     * @return true when the given "symbolTable" is the topmost declaration level
     * for the given FunctionDecl, i.e. "functionDecl" may occur in other SymbolTables,
     * but all under "symbolTable".
     */
    private boolean isTopDeclarationLevel(SymbolTable symbolTable, FunctionDecl functionDecl) {
        boolean ok = false;
        if (symbolTable.isImports && functionDecl.importedFrom != null) {
            FunctionDecl impFuncDecl = (FunctionDecl) functionDecl.importedFrom.first;
            ok = (impFuncDecl.unit()==functionDecl.unit()) ;
        }
        return (functionDecl != null
                && (symbolTable == functionDecl.definitionSymbolTable
                    || (symbolTable.isImports && ok)
                    || (functionDecl.definitionSymbolTable == null
                        && symbolTable.basisSymbolTable() == null))) ;
    }

    // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:

    /**
     * Only in reverse mode with a language that implements PASS-BY-VALUE,
     * returns true when the given original variable v (root of "expression")
     * is (at least partly) a formal PARAMETER which at the same time<br>
     * -- has a derivative vb that must be modified by the diff of curUnit
     * (=&gt; and therefore vb must use PASS-BY-REFERENCE)<br>
     * -- and is itself (v) only read and not overwritten by curUnit
     * (=&gt; and therefore vb will only be incremented)<br>
     * In this case, we can identify the outside vb with the inside copy vb,
     * using a PASS-BY-REFERENCE mechanism (i.e. pointer) for vb, and not introduce a vb0.
     * Therefore we shall add a pointer on top of every use or declaration of the vb.
     */
    private boolean isOnlyIncrementedDiff(ActivityPattern pattern, VariableDecl varDecl) {
        boolean isByRefOnlyRead = false;
        if (curDiffUnitSort() == DiffConstants.ADJOINT && pattern != null && varDecl.passesByValue(pattern.unit())) {
            TapIntList zonesList = ZoneInfo.listAllZones(varDecl.zones(), true);
            SymbolTable symbolTable = null;
            if (adEnv.curInstruction() != null) {
                symbolTable = adEnv.curInstruction().block.symbolTable;
            }
            if (symbolTable == null && adEnv.curUnit() != null) {
                symbolTable = adEnv.curUnit().privateSymbolTable();
            }
            if (symbolTable == null) {
                symbolTable = adEnv.srcCallGraph().languageRootSymbolTable(TapEnv.relatedLanguage());
            }
            while (!isByRefOnlyRead && zonesList != null) {
                if (0 <= zonesList.head && zonesList.head < symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)
                        && isOnlyIncrementedDiff(pattern, symbolTable.declaredZoneInfo(zonesList.head, SymbolTableConstants.ALLKIND))) {
                    isByRefOnlyRead = true;
                }
                zonesList = zonesList.tail;
            }
        }
        return isByRefOnlyRead;
    }

    protected boolean isOnlyIncrementedDiff(ActivityPattern pattern, ZoneInfo zoneInfo) {
        boolean isByRefOnlyRead = false;
        if (curDiffUnitSort() == DiffConstants.ADJOINT
                && zoneInfo != null && zoneInfo.kind() == SymbolTableConstants.PARAMETER
                && zoneInfo.passesByValue(adEnv.curUnit(), adEnv.curUnit().language())) {
            int paramRank = zoneInfo.index;
            Unit paramUnit = zoneInfo.declarationUnit;
            if (paramUnit != null) {
                UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(paramUnit);
                boolean[] diffArgsByValueNeedOverwrite = diffInfo.getUnitDiffArgsByValueNeedOverwriteInfoS(pattern);
                boolean[] diffArgsByValueNeedOnlyIncrement = diffInfo.getUnitDiffArgsByValueNeedOnlyIncrementInfoS(pattern);
                if (diffArgsByValueNeedOverwrite != null
                        && diffArgsByValueNeedOverwrite[paramRank]
                        && diffArgsByValueNeedOnlyIncrement[paramRank]) {
                    isByRefOnlyRead = true;
                }
            }
        }
        return isByRefOnlyRead;
    }

    // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.

    /**
     * @return true when the variable in "varBaseTree" (assumed active inside curUnit)
     * is a formal parameter of curUnit and is not active as a formal parameter
     * because this parameter is passive upon entry and upon exit from curUnit.
     */
    protected boolean paramHasOnlyLocalDiff(ActivityPattern pattern, Tree varBaseTree, VariableDecl variableDecl) {
        boolean result = false;
        if (pattern != null && adEnv.curUnit() != null && adEnv.curUnit().functionTypeSpec() != null
                && adEnv.curUnit().functionTypeSpec().argumentsTypes != null) {
            int nbArgs = adEnv.curUnit().functionTypeSpec().argumentsTypes.length;
            Unit origUnit = adEnv.curUnit();
            if (adEnv.curUnit().isInterface()) {
                origUnit = adEnv.srcCallGraph().getUnit(adEnv.curUnit().name(), adEnv.curUnit().functionTypeSpec());
            }
            if (origUnit == null) {
                origUnit = adEnv.curUnit();
            }
            if (origUnit.publicSymbolTable() != null) {
                VariableDecl varDecl;
                if (variableDecl != null) {
                    varDecl = variableDecl;
                } else {
                    varDecl = origUnit.publicSymbolTable().getVariableOrConstantDecl(ILUtils.baseName(varBaseTree));
                }
                if (varDecl != null) {
                    int rank = varDecl.formalArgRank;
                    if (varDecl.formalArgRankInOrigUnit != SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK) {
                        rank = varDecl.formalArgRankInOrigUnit;
                    }
                    if (1 <= rank && rank <= nbArgs) {
                        boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(pattern);
                        result = (formalArgsActivity != null && !formalArgsActivity[rank - 1]);
                    }
                }
            }
        }
        return result;
    }

    /**
     * @return true if the given "varName" is actually the name of the enclosing Fortran function.
     */
    protected boolean isCurFunctionName(String varName) {
        return adEnv.curUnit().isFortran()
                && adEnv.procedureCallDifferentiator.diffFunctionIsFunction(adEnv.curUnit())
                && varName.equals(adEnv.curUnit().name());
    }

    /**
     * Dumps the sibling declarations found for the given "symbolName"
     */
    private void dumpSiblingDecls(String symbolName,
                                  TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls) {
        TapEnv.printOnTrace(" DIFF SYMBOLNAME:" + symbolName + " => sibling decls:");
        while (siblingDecls != null) {
            TapPair<SymbolTable, TapList<SymbolDecl>> oneb = siblingDecls.head;
            TapEnv.printOnTrace("     " + oneb.first.addressChain()
                    + " : " + oneb.second);
            siblingDecls = siblingDecls.tail;
        }
    }
}
