/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.utils.TapPair;

//[llh 29/5/2015] TODO: kill this class by merging it into
// the ActivityPattern's accessible through the Unit's ".activityPatterns".

/**
 * Collection of diffUnits and other infos for a given Unit.
 */
public final class UnitDiffInfo {
    private final DifferentiationEnv adEnv;
    private final Unit unit;
    private Unit diffUnit;
    private final TapList<TapPair<ActivityPattern, Unit>> adjointUnits;
    private final TapList<TapPair<ActivityPattern, Unit>> bwdUnits;
    private final TapList<TapPair<ActivityPattern, Unit>> fwdUnits;
    private final TapList<TapPair<ActivityPattern, Unit>> tangentUnits;
    private final TapList<TapPair<ActivityPattern, boolean[]>> unitFormalArgsActivityS;
    private final TapList<TapPair<ActivityPattern, boolean[]>> unitDiffArgsByValueNeedOverwriteInfoS;
    private final TapList<TapPair<ActivityPattern, boolean[]>> unitDiffArgsByValueNeedOnlyIncrementInfoS;

    protected UnitDiffInfo(Unit unit, DifferentiationEnv adEnv) {
        super();
        this.unit = unit;
        this.adEnv = adEnv;
        adjointUnits = new TapList<>(null, null);
        bwdUnits = new TapList<>(null, null);
        fwdUnits = new TapList<>(null, null);
        tangentUnits = new TapList<>(null, null);
        unitFormalArgsActivityS = new TapList<>(null, null);
        unitDiffArgsByValueNeedOverwriteInfoS = new TapList<>(null, null);
        unitDiffArgsByValueNeedOnlyIncrementInfoS = new TapList<>(null, null);
    }

    /**
     * @return all differentiation units of this unit for the given activity pattern.
     * Therefore doesn't return the copied Unit.
     */
    protected TapList<Unit> getAllDiffs(ActivityPattern pattern) {
        TapList<Unit> result = null;
        Unit tempUnit;
        tempUnit = getSplitBackward(pattern);
        if (tempUnit != null) {
            result = new TapList<>(tempUnit, result);
        }
        tempUnit = getSplitForward(pattern);
        if (tempUnit != null) {
            result = new TapList<>(tempUnit, result);
        }
        tempUnit = getAdjoint(pattern);
        if (tempUnit != null) {
            result = new TapList<>(tempUnit, result);
        }
        tempUnit = getTangent(pattern);
        if (tempUnit != null) {
            result = new TapList<>(tempUnit, result);
        }
        if (diffUnit != null && diffUnit.isDiffPackage() && !TapList.contains(result, diffUnit)) {
            result = new TapList<>(diffUnit, result);
        }
        return result;
    }

    /**
     * @return all differentiation units of this unit for the given diffKind.
     */
    protected TapList<Unit> getAllDiffs(int diffKind) {
        TapList<Unit> hdResult = new TapList<>(null, null);
        TapList<Unit> tlResult = hdResult;
        switch (diffKind) {
            case DiffConstants.TANGENT:
                tlResult = addUnitsAtTail(tangentUnits.tail, tlResult, hdResult.tail);
                break;
            case DiffConstants.ADJOINT:
                tlResult = addUnitsAtTail(adjointUnits.tail, tlResult, hdResult.tail);
                break;
            case DiffConstants.ADJOINT_FWD:
                tlResult = addUnitsAtTail(fwdUnits.tail, tlResult, hdResult.tail);
                break;
            case DiffConstants.ADJOINT_BWD:
                tlResult = addUnitsAtTail(bwdUnits.tail, tlResult, hdResult.tail);
                break;
            case DiffConstants.ORIGCOPY: {
                Unit copyUnit = adEnv.getPrimalCopyOfOrigUnit(this.unit);
                if (copyUnit != null) {
                    tlResult = tlResult.placdl(copyUnit);
                }
                break;
            }
            default:
                break;
        }
        return hdResult.tail;
    }

    /**
     * @return all differentiation units of this unit, not geting the copied, non-diff Unit.
     */
    protected TapList<Unit> getAllDiffs() {
        TapList<Unit> hdResult = new TapList<>(null, null);
        TapList<Unit> tlResult = hdResult;
        tlResult = addUnitsAtTail(tangentUnits.tail, tlResult, hdResult.tail);
        tlResult = addUnitsAtTail(adjointUnits.tail, tlResult, hdResult.tail);
        tlResult = addUnitsAtTail(fwdUnits.tail, tlResult, hdResult.tail);
        tlResult = addUnitsAtTail(bwdUnits.tail, tlResult, hdResult.tail);
        hdResult = hdResult.tail;
        if (diffUnit != null && !TapList.contains(hdResult, diffUnit)) {
            hdResult = new TapList<>(diffUnit, hdResult);
        }
        return hdResult;
    }

    protected boolean hasDiffs() {
        return (tangentUnits.tail!=null || adjointUnits.tail!=null || fwdUnits.tail!=null || bwdUnits.tail!=null) ;
    }

    /**
     * @return all differentiation units and differentiation mode of this unit, not geting the copied, non-diff Unit.
     */
    protected TapList<Unit>[] getAllDiffsAndMode() {
        @SuppressWarnings("unchecked")
        TapList<Unit>[] result = new TapList[5];
        result[DiffConstants.TANGENT] = getAllDiffs(DiffConstants.TANGENT);
        result[DiffConstants.ADJOINT] = getAllDiffs(DiffConstants.ADJOINT);
        result[DiffConstants.ADJOINT_FWD] = getAllDiffs(DiffConstants.ADJOINT_FWD);
        result[DiffConstants.ADJOINT_BWD] = getAllDiffs(DiffConstants.ADJOINT_BWD);
        return result;
    }

    /**
     * @return all differentiation units of this unit, plus the copied Unit if exists.
     */
    protected TapList<Unit> getAllDiffsPlusCopy() {
        TapList<Unit> result = getAllDiffs();
        Unit copyUnit = adEnv.getPrimalCopyOfOrigUnit(this.unit);
        if (copyUnit != null && !TapList.contains(result, copyUnit)) {
            result = new TapList<>(copyUnit, result);
        }
        return result;
    }

    private TapList<Unit> addUnitsAtTail(TapList<TapPair<ActivityPattern, Unit>> newPatternUnits,
                                         TapList<Unit> tlIntoList, TapList<Unit> intoList) {
        while (newPatternUnits != null) {
            if (newPatternUnits.head != null) {
                TapPair<ActivityPattern, Unit> patternUnit =
                        newPatternUnits.head;
                if (!TapList.contains(intoList, patternUnit.second)) {
                    tlIntoList = tlIntoList.placdl(patternUnit.second);
                }
            }
            newPatternUnits = newPatternUnits.tail;
        }
        return tlIntoList;
    }

    protected Unit getDiffForModeAndActivity(int diffMode, ActivityPattern pattern) {
        switch (diffMode) {
            case DiffConstants.TANGENT:
                return getTangent(pattern);
            case DiffConstants.ADJOINT:
                return getAdjoint(pattern);
            case DiffConstants.ADJOINT_FWD:
                return getSplitForward(pattern);
            case DiffConstants.ADJOINT_BWD:
                return getSplitBackward(pattern);
            default:
                TapEnv.toolError("Error: getDiffForModeAndActivity called for illegal mode:" + diffMode);
                return null;
        }
    }

    protected TapList<ActivityPattern> getAllDiffPatterns(int diffMode) {
        TapList<TapPair<ActivityPattern, Unit>> aList;
        switch (diffMode) {
            case DiffConstants.TANGENT:
                aList = tangentUnits.tail;
                break;
            case DiffConstants.ADJOINT:
                aList = adjointUnits.tail;
                break;
            case DiffConstants.ADJOINT_FWD:
                aList = fwdUnits.tail;
                break;
            case DiffConstants.ADJOINT_BWD:
                aList = bwdUnits.tail;
                break;
            default:
                TapEnv.toolError("Error: getAllDiffPatterns called for illegal mode:" + diffMode);
                aList = null;
        }
        TapList<ActivityPattern> hdResult = new TapList<>(null, null);
        TapList<ActivityPattern> tlResult = hdResult;
        while (aList != null) {
            tlResult = tlResult.placdl(aList.head.first);
            aList = aList.tail;
        }
        return hdResult.tail;
    }

    protected TapList<TapPair<ActivityPattern, Unit>> getAllDiffPatternsAndUnits(int diffMode) {
        TapList<TapPair<ActivityPattern, Unit>> aList;
        switch (diffMode) {
            case DiffConstants.TANGENT:
                aList = tangentUnits.tail;
                break;
            case DiffConstants.ADJOINT:
                aList = adjointUnits.tail;
                break;
            case DiffConstants.ADJOINT_FWD:
                aList = fwdUnits.tail;
                break;
            case DiffConstants.ADJOINT_BWD:
                aList = bwdUnits.tail;
                break;
            default:
                TapEnv.toolError("Error: getAllDiffPatterns called for illegal mode:" + diffMode);
                aList = null;
        }
        return aList;
    }

    /**
     * @return the Adjoint Unit that belongs to the given ActivityPattern, or
     * null if no such Unit exists.
     */
    protected Unit getAdjoint(ActivityPattern pattern) {
        return retrieveForActivity(adjointUnits, pattern);
    }

    /**
     * Set the Adjoint Unit for the given pattern.
     */
    protected void setAdjoint(ActivityPattern pattern, Unit adjointUnit) {
        storeForActivity(adjointUnits, pattern, adjointUnit);
    }

    /**
     * @return the Adjoint Split mode backward Unit that belongs to the given
     * ActivityPattern, or null if no such Unit exists.
     */
    protected Unit getSplitBackward(ActivityPattern pattern) {
        return retrieveForActivity(bwdUnits, pattern);
    }

    /**
     * Set the Backward Unit for the given pattern.
     */
    protected void setSplitBackward(ActivityPattern pattern, Unit bwdUnit) {
        storeForActivity(bwdUnits, pattern, bwdUnit);
    }

    /**
     * @return the Adjoint Split mode forward Unit that belongs to the given
     * ActivityPattern, or null if no such Unit exists.
     */
    protected Unit getSplitForward(ActivityPattern pattern) {
        return retrieveForActivity(fwdUnits, pattern);
    }

    /**
     * Set the Forward Unit for the given pattern.
     */
    protected void setSplitForward(ActivityPattern pattern, Unit fwdUnit) {
        storeForActivity(fwdUnits, pattern, fwdUnit);
    }

    /**
     * @return the Tangent Unit that belongs to the given ActivityPattern, or
     * null if no such Unit exists.
     */
    protected Unit getTangent(ActivityPattern pattern) {
        return retrieveForActivity(tangentUnits, pattern);
    }

    /**
     * Set the Tangent Unit for the given pattern.
     */
    protected void setTangent(ActivityPattern pattern, Unit tangentUnit) {
        storeForActivity(tangentUnits, pattern, tangentUnit);
    }

    /**
     * @return the differentiated Unit (only for modules?).
     */
    protected Unit getDiff() {
        return diffUnit;
    }

    /**
     * Set the differentiated Unit (only for modules?).
     */
    protected void setDiff(Unit diffU) {
        diffUnit = diffU;
    }

    /**
     * @return For a package Unit, returns the corresponding package Unit for the differentiated code,
     * and for a procedure Unit, returns the copy of this Unit for the differentiated code.
     */
    protected Unit getCopyForDiff() {
        return unit.isPackage() ? diffUnit : adEnv.getPrimalCopyOfOrigUnit(this.unit);
    }

    /**
     * @return the diff unit return type if any differentiated version has a
     * non-void return type. Otherwise, returns null.
     */
    protected WrapperTypeSpec getDiffReturnType(int diffKind) {
        Unit oneDiffUnit;
        WrapperTypeSpec returnType = null;
        for (TapList<Unit> allDiffs = getAllDiffs(diffKind);
             returnType == null && allDiffs != null; allDiffs = allDiffs.tail) {
            oneDiffUnit = allDiffs.head;
            if (oneDiffUnit.functionTypeSpec() != null &&
                    !TypeSpec.isA(oneDiffUnit.functionTypeSpec().returnType, SymbolTableConstants.VOIDTYPE)) {
                returnType = oneDiffUnit.functionTypeSpec().returnType;
            }
        }
        return returnType;
    }

    /**
     * @return the most representative differentiated counterpart
     * of a given source unit, contained in a container unit.
     * This is somewhat arbitrary. If possible, we return the TANGENT, which also
     * happens to contain the unique diff for modules (DIFFERENTIATED==TANGENT).
     * Else the ADJOINT. Else the ADJOINT_BWD. Else the ORIGCOPY.
     */
    protected Unit getAnyContainingDiffUnit(Unit unit, Unit container) {
        Unit principalDiffContainer = null;
        UnitDiffInfo containerInfos =
                adEnv.callGraphDifferentiator.getUnitDiffInfo(container);
        if (container.isModule()) {
            principalDiffContainer = containerInfos.getDiff();
        } else {
            // we are dealing with a routine contained in another routine
            // Find some diff version of the container that has an
            // activityPattern that matches any activityPattern of our
            // contained routine. We will arbitrarily select any of those as
            // principalDiffContainer. TODO! this can lead to naming
            // conflicts: the contained unit should have several containers
            TapList<ActivityPattern> itCallingContainerPatterns =
                    getCallingContainerPatterns(unit, container);
            if (itCallingContainerPatterns == null) {
                itCallingContainerPatterns = container.activityPatterns;
            }
            while (principalDiffContainer == null && itCallingContainerPatterns != null) {
                ActivityPattern curContainerActivity = itCallingContainerPatterns.head;
                if (curContainerActivity != null) {
                    TapList<Unit> allDiffs = containerInfos.getAllDiffs(curContainerActivity);
                    if (allDiffs != null) {
                        principalDiffContainer = allDiffs.head;
                    }
                }
                itCallingContainerPatterns = itCallingContainerPatterns.tail;
            }
            if (principalDiffContainer == null) {
                principalDiffContainer = adEnv.getPrimalCopyOfOrigUnit(containerInfos.unit);
            }
        }
        return principalDiffContainer;
    }

    /**
     * @param contained is a procedure contained in procedure "container".
     * @return the list of activity patterns of "container" that can eventually call "contained".
     * NOTE: this is slightly bizarre. Can't we just return any pattern of "container" ??.
     */
    private TapList<ActivityPattern> getCallingContainerPatterns(Unit contained, Unit container) {
        TapList<ActivityPattern> patternsOfContained = contained.activityPatterns;
        TapList<ActivityPattern> patternsOfContainer = new TapList<>(null, null);
        TapList<ActivityPattern> dejaVu = new TapList<>(null, null);
        while (patternsOfContained != null) {
            ActivityPattern.walkUpToPatternOf(patternsOfContained.head,
                    container, patternsOfContainer, dejaVu);
            patternsOfContained = patternsOfContained.tail;
        }
        return patternsOfContainer.tail;
    }

    /**
     * @return the unitFormalArgsActivityS for the given pattern.
     * For each Pattern, a boolean array that tells if each formal
     * argument is differentiated.
     */
    protected boolean[] getUnitFormalArgsActivityS(ActivityPattern pattern) {
        return retrieveForActivity(unitFormalArgsActivityS, pattern);
    }

    /**
     * Set the unitFormalArgsActivityS for the given pattern.
     */
    protected void setUnitFormalArgsActivityS(ActivityPattern pattern, boolean[] args) {
        storeForActivity(unitFormalArgsActivityS, pattern, args);
    }

    /**
     * For each ActivityPattern of the unit
     *
     * @return an array of booleans indexed by rank as a formal parameter (0 for result),
     * which are true when this argument is passed by value (and therefore its diff too) or is the result,
     * and at the same time the corresponding diff argument (which is thus passed by value) needs
     * to be modified by the diff of the unit (for this ActivityPattern).
     * For instance in C, this will force to change the diff formal parameter from type T_B to T_B*,
     * and pass the address of v instead of actual parameter v.
     */
    protected boolean[] getUnitDiffArgsByValueNeedOverwriteInfoS(ActivityPattern pattern) {
        return retrieveForActivity(unitDiffArgsByValueNeedOverwriteInfoS, pattern);
    }

    /**
     * Set the info that can be retrieved by getUnitDiffArgsByValueNeedOverwriteInfoS().
     */
    protected void setUnitDiffArgsByValueNeedOverwriteInfoS(ActivityPattern pattern, boolean[] val) {
        storeForActivity(unitDiffArgsByValueNeedOverwriteInfoS, pattern, val);
    }

    /**
     * For each ActivityPattern of the unit.
     *
     * @return an array of booleans indexed by rank as a formal parameter (0 for result),
     * which are true when the diff of this argument is passed by value and yet need be overwritten
     * (cf getUnitDiffArgsByValueNeedOverwriteInfoS), but this overwriting of the diff is only
     * an increment of its value and therefore the special differentiation mechanism for
     * passed-by-value arguments may be simplified.
     */
    protected boolean[] getUnitDiffArgsByValueNeedOnlyIncrementInfoS(ActivityPattern pattern) {
        return retrieveForActivity(unitDiffArgsByValueNeedOnlyIncrementInfoS, pattern);
    }

    /**
     * Set the info that can be retrieved by getUnitDiffArgsByValueNeedOnlyIncrementInfoS().
     */
    protected void setUnitDiffArgsByValueNeedOnlyIncrementInfoS(ActivityPattern pattern, boolean[] val) {
        storeForActivity(unitDiffArgsByValueNeedOnlyIncrementInfoS, pattern, val);
    }

    /**
     * Internal lookup method to find the Unit that belongs to the given
     * ActivityPattern. Works for adjoint, tangent, bwd, fwd.
     * Will return null if the matching entry is not found.
     * In the future, this info might be stored in an array instead of
     * a list for faster access.
     */
    private <T> T retrieveForActivity(TapList<TapPair<ActivityPattern, T>> haystack, ActivityPattern needle) {
        if (haystack != null && haystack.head == null) {
            haystack = haystack.tail;
        }
        while (haystack != null) {
            if (haystack.head.first == needle || needle == null || needle.isDisconnected()) {
                return haystack.head.second;
            }
            haystack = haystack.tail;
        }
        return null;
    }

    /**
     * Internal setter method to set the Unit that belongs to the given
     * ActivityPattern. Works for adjoint, tangent, bwd, fwd.
     * In the future, this info might be stored in an array instead of
     * a list for faster access.
     */
    private <T> void storeForActivity(TapList<TapPair<ActivityPattern, T>> haystack, ActivityPattern needle, T val) {
        TapList<TapPair<ActivityPattern, T>> itHaystack = haystack.tail;
        while (itHaystack != null && itHaystack.head.first != needle) {
            itHaystack = itHaystack.tail;
        }
        if (itHaystack == null) {
            haystack.tail = new TapList<>(new TapPair<>(needle, val), haystack.tail);
        } else {
            itHaystack.head.second = val;
        }
    }

    @Override
    public String toString() {
        return "UnitDiffInfo" + '@' + Integer.toHexString(hashCode())
                + " of " + unit + " diff(Module)Unit:" + diffUnit +
                " ORIGCOPY:" + adEnv.getPrimalCopyOfOrigUnit(this.unit)
                + " TANGENTs:" + tangentUnits.tail + " ADJOINTs:" + adjointUnits.tail
                + " ADJOINT_FWDs:" + fwdUnits.tail + " ADJOINT_BWDs:" + bwdUnits.tail;
    }
}
