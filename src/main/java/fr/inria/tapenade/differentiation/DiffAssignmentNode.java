/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.InstructionMask;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec ;
import fr.inria.tapenade.utils.Tree;

/**
 * Object that stores an almost finished differentiated assignment, as returned
 * by ExpressionDifferentiator.adjointDifferentiateAssignedExpression().
 */
public class DiffAssignmentNode {

    /**
     * The differentiated variable that receives  the assignment.
     * May be null if this DiffAssignmentNode actually stands for an expression only.
     */
    protected Tree diffRecv = null ;

    /**
     * The primal variable whose diff is diffRecv.
     */
    protected Tree primRecv;
    /** The replica number that was used to build "diffRecv" from "primRecv". */
    protected int iReplic = 0 ;

    /**
     * Assignment action: SET_VARIABLE or INCREMENT_VARIABLE.
     */
    protected int action = DiffConstants.SET_VARIABLE;

    /**
     * The value that is set-to or added-into the receiving differentiated variable.
     */
    protected Tree diffValue;

    /** Alternative to (diffRecv ; diffValue), the complete assignment Tree.
     * It would be good to completely replace (diffRecv ; diffValue) with diffAssign. */
    protected Tree diffAssign = null ;

    /**
     * When there is a vector mask (e.g. in F90) that controls the assignment. Otherwise null.
     */
    protected InstructionMask mask;

    /**
     * Lists of elementary Trees read by the assignment.
     */
    protected TapList<Tree> primR;

    /**
     * Lists of elementary Trees written by the assignment.
     */
    protected TapList<Tree> primW;

    /**
     * Lists of elementary Trees whose diff is read by the assignment.
     */
    protected TapList<Tree> diffR;

    /**
     * Lists of elementary Trees whose diff is written by the assignment.
     */
    protected TapList<Tree> diffW;

    /** The type of the (primal) assigned value */
    protected TypeSpec type = null ;

    /**
     * Creates a new DiffAssignmentNode that represents an assignment (or incrementation if action==INCREMENT_VARIABLE), to
     * the diff variable described by "diffRecv", of expression "diffValue".
     */
    protected DiffAssignmentNode(Tree diffRecv, int action, Tree diffValue,
                                 InstructionMask mask, Tree primRecv, int iReplic,
                                 TapList<Tree> primR, TapList<Tree> primW, TapList<Tree> diffR, TapList<Tree> diffW) {
        this.diffRecv = diffRecv;
        this.primRecv = primRecv;
        this.iReplic = iReplic ;
        this.action = action;
        this.diffValue = diffValue;
        this.mask = mask;
        this.primR = primR;
        this.primW = primW;
        this.diffR = diffR;
        this.diffW = diffW;
    }

    /** Alternative creation, directly with a complete assignment Tree */
    protected DiffAssignmentNode(Tree diffAssign, int action,
                                 InstructionMask mask, Tree primRecv, int iReplic,
                                 TapList<Tree> primR, TapList<Tree> primW, TapList<Tree> diffR, TapList<Tree> diffW) {
        this.diffAssign = diffAssign ;
        this.primRecv = primRecv;
        this.iReplic = iReplic ;
        this.action = action;
        this.mask = mask;
        this.primR = primR;
        this.primW = primW;
        this.diffR = diffR;
        this.diffW = diffW;
    }

    @Override
    public String toString() {
        return "[DiffAssignmentNode]" + ILUtils.toString(diffRecv) +
                (action == DiffConstants.SET_VARIABLE ? " := " : " += ") + ILUtils.toString(diffValue)
                + " WHERE:" + mask + " primR:" + primR + " primW:" + primW + " diffR:" + diffR + " diffW:" + diffW;
    }
}
