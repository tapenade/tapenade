/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;


/**
 * Data-dependence graph on the Instructions of a future Block of the differentiated code.
 * This holds a local Data-Dependence Graph of the future instructions,
 * linked by Data-Dependences (NewBlockGraphArrow's). Nodes must be inserted either
 * at the head or at the tail of the existing set of nodes.
 * Data-dependences are set  when a new node is inserted: at that moment, we are sure
 * that all node's before (or after when atTail==false) it have been inserted, and only them.
 * The local uses and defs of the new NewBlockGraphNode are required, they are compared
 * to the lists of existing, visible defs and uses of the previous NewBlockGraphNode's,
 * which results in NewBlockGraphArrow's being set or not,
 * and they are finally added to these same lists.
 * A notion of Group level exists, so that whenever we are inside a group, all
 * successively added NewBlockGraphNodes must remain in their exact insertion order.
 */
public final class NewBlockGraph {

    public TapList<NewBlockGraphNode> nodes;
    private int nodeCount;
    protected boolean debug;
    protected DifferentiationEnv adEnv;
    private SymbolTable symbolTable;
    private final int nbZones;
    /**
     * when non-zero, all successively added nodes depend on each other.
     */
    private int inGroupLevel;
    private TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleSrcUses;
    private TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleSrcDefs;
    private TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleBisUses;
    private TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleBisDefs;
    private NewBlockGraphNode lastNodeInGroup;
    /**
     * Allows keeping relative order between some selected nodes.
     */
    private NewBlockGraphNode lastNodeInChain;

    protected NewBlockGraph(DifferentiationEnv adEnv, SymbolTable symbolTable, int nbZones) {
        this.adEnv = adEnv;
        this.symbolTable = symbolTable;
        this.nbZones = nbZones;
        // Zone ranks are offset by 1, to accomodate info
        //  for special zone "riskAlias==-1", at index 0:
        // When a pointer p may point to a variable that is the initial destination
        // of a pointer input param, and since we fear aliasing among the pointer
        // input params, then we say that p may point to a special zone (riskAlias==-1)
        // that will cause data deps to be placed between refs that use it and
        // any ref to an input param.
        visibleSrcUses = new TapList[nbZones + 1];
        visibleSrcDefs = new TapList[nbZones + 1];
        visibleBisUses = new TapList[nbZones + 1];
        visibleBisDefs = new TapList[nbZones + 1];
        reset();
    }

    protected static boolean existsArrow(NewBlockGraphNode node1, NewBlockGraphNode node2) {
        TapList<NewBlockGraphArrow> flow = node1.flow;
        boolean exists = false;
        while (!exists && flow != null) {
            exists = flow.head.destination() == node2;
            flow = flow.tail;
        }
        return exists;
    }

    protected void reset() {
        for (int i = nbZones; i >= 0; --i) {
            visibleSrcUses[i] = null;
            visibleSrcDefs[i] = null;
            visibleBisUses[i] = null;
            visibleBisDefs[i] = null;
        }
        inGroupLevel = 0;
        lastNodeInGroup = null;
        lastNodeInChain = null;
        nodes = null;
        nodeCount = 0;
    }

    protected void buildInstructions(SymbolTable srcSymbolTable, SymbolTable revSymbolTable,
                                     DifferentiationEnv adEnv, Unit diffUnit) {
        TapList<NewBlockGraphNode> inNodes = nodes;
        while (inNodes != null) {
            inNodes.head.buildInstruction(srcSymbolTable, revSymbolTable, adEnv, diffUnit);
            inNodes = inNodes.tail;
        }
    }

    private TapList<NewBlockGraphNode> notAllowedFuseNodesAfter(NewBlockGraphNode from) {
        TapList<NewBlockGraphArrow> justAfter = from.flow;
        TapList<NewBlockGraphArrow> justBefore = from.backFlow;
        TapList<NewBlockGraphNode> notAllowed = new TapList<>(from, null);
        NewBlockGraphNode next;
        TapList<NewBlockGraphArrow> justAfterNext;
        while (justAfter != null) {
            next = justAfter.head.destination();
            justAfterNext = next.flow;
            while (justAfterNext != null) {
                accumulateNodeAndSuccessors(justAfterNext.head.destination(), notAllowed);
                justAfterNext = justAfterNext.tail;
            }
            justAfter = justAfter.tail;
        }
        while (justBefore != null) {
            accumulateNodeAndPredecessors(justBefore.head.origin(),
                    notAllowed);
            justBefore = justBefore.tail;
        }
        return notAllowed;
    }

    private void accumulateNodeAndSuccessors(NewBlockGraphNode node,
                                             TapList<NewBlockGraphNode> accumulator) {
        if (node != null && !TapList.contains(accumulator, node)) {
            accumulator.placdl(node);
            TapList<NewBlockGraphArrow> justAfter = node.flow;
            while (justAfter != null) {
                accumulateNodeAndSuccessors(justAfter.head.destination(), accumulator);
                justAfter = justAfter.tail;
            }
        }
    }

    private void accumulateNodeAndPredecessors(NewBlockGraphNode node, TapList<NewBlockGraphNode> accumulator) {
        if (!TapList.contains(accumulator, node)) {
            accumulator.placdl(node);
            TapList<NewBlockGraphArrow> justBefore = node.backFlow;
            while (justBefore != null) {
                accumulateNodeAndPredecessors(justBefore.head.origin(), accumulator);
                justBefore = justBefore.tail;
            }
        }
    }

    /**
     * Condenses the nodes of this Graph, performing condensation of
     * the nodes when condensation is defined in their relative classes.
     * Hypotheses: The graph "nodes" is the list of all nodes (e.g. instructions)
     * ordered from the first executed to the last executed.
     * Between these nodes are arrows (e.g. data-deps in their normal direction),
     * i.e. they all go from a node to a node AFTER it in the list.
     * Therefore the graph is a DAG.
     * After condensation has begun, the graph is still a DAG,
     * BUT it may happen that some new arrows go UPWARDS !
     */
    protected void condense(Unit unit) {
        if (TapList.length(nodes) < 1000) {
            while (doOneCondensation()) {
                TapEnv.printlnOnTrace(35, "Condensing; " + TapList.length(nodes) + " nodes remaining...");
            }
        } else {
            TapEnv.fileWarning(TapEnv.MSG_WARN, unit.headTree(), "(ADxx) Too many nodes for instruction condensation:" + TapList.length(nodes) + " while differentiating " + unit.name());
        }
    }

    private boolean doOneCondensation() {
        TapList<NewBlockGraphNode> nodesList = nodes;
        TapList<NewBlockGraphNode> notAllowedFuseTargets;
        NewBlockGraphNode origin, target;
        boolean originDisappears;
        // For every "origin" in nodes, taken as the "origin" of the fusion...
        while (nodesList != null) {
            origin = nodesList.head;
            // ... compute the list of all other nodes of the graph that are
            // not allowed to fuse with "origin", i.e. there is a chain of deps
            // of length >1 from "origin" to "target", or a chain of deps
            // from "target" to "origin"...
            notAllowedFuseTargets = notAllowedFuseNodesAfter(origin);
            // ... then for every "target" which is NOT in "notAllowedFuseTargets",
            // taken as the "destination" of the fusion...
            TapList<NewBlockGraphNode> allNodes = nodes;
            while (allNodes != null) {
                target = allNodes.head;
                if (// ... if fusion of "origin" with "target" is possible ...
                        !TapList.contains(notAllowedFuseTargets, target)
                                // ... and this fusion is profitable ...
                                && origin.interestingFuseWith(target, this)) {
                    // ... fuse "target" with "origin"...
                    // (ATTENTION, we rely on the fact that there is
                    // certainly no arrow from "target" to "origin")
                    if (this.debug) {
                        TapEnv.printlnOnTrace("FUSE " + origin + " WITH " + target);
                    }
                    originDisappears = origin.fuseWith(target);
                    if (originDisappears) {
                        skipNodeInto(origin, target);
                        if (this.debug) {
                            TapEnv.printlnOnTrace("  origin disappearing into target " + target);
                        }
                    } else {
                        skipNodeInto(target, origin);
                        if (this.debug) {
                            TapEnv.printlnOnTrace("  target disappearing into origin " + origin);
                        }
                    }
                    // ... and restart condensation from the beginning.
                    return true;
                }
                allNodes = allNodes.tail;
            }
            nodesList = nodesList.tail;
        }
        return false;
    }

    /**
     * Try and condense the present new node newNode with some
     * node "fuseOrig" which is already in the dependence graph.
     *
     * @return the pair of the node that disappears and the node into which it was fused.
     */
    private TapPair<NewBlockGraphNode, NewBlockGraphNode> condenseNewNode(NewBlockGraphNode newNode, boolean atTail) {
        TapList<NewBlockGraphNode> notAllowedList = new TapList<>(newNode, null);
        NewBlockGraphNode fuseOrigin = null, fuseTarget = null;
        if (atTail) {
            fuseTarget = newNode;
            TapList<NewBlockGraphArrow> before = newNode.backFlow;
            while (before != null) {
                TapList<NewBlockGraphArrow> beforeBefore = before.head.origin().backFlow;
                while (beforeBefore != null) {
                    accumulateNodeAndPredecessors(beforeBefore.head.origin(), notAllowedList);
                    beforeBefore = beforeBefore.tail;
                }
                before = before.tail;
            }
        } else {
            fuseOrigin = newNode;
            TapList<NewBlockGraphArrow> after = newNode.flow;
            while (after != null) {
                TapList<NewBlockGraphArrow> afterAfter = after.head.destination().flow;
                while (afterAfter != null) {
                    accumulateNodeAndSuccessors(afterAfter.head.destination(), notAllowedList);
                    afterAfter = afterAfter.tail;
                }
                after = after.tail;
            }
        }
        boolean originDisappears;
        TapList<NewBlockGraphNode> allNodes = nodes;
        while (allNodes != null) {
            if (!TapList.contains(notAllowedList, allNodes.head)) {
                if (atTail) {
                    fuseOrigin = allNodes.head;
                } else {
                    fuseTarget = allNodes.head;
                }
                if (fuseOrigin.interestingFuseWith(fuseTarget, this)) {
                    if (this.debug) {
                        TapEnv.printlnOnTrace("FUSE " + fuseOrigin + " WITH " + fuseTarget);
                    }
                    originDisappears = fuseOrigin.fuseWith(fuseTarget);
                    if (originDisappears) {
                        skipNodeInto(fuseOrigin, fuseTarget);
                        if (this.debug) {
                            TapEnv.printlnOnTrace("  origin disappearing into target " + fuseTarget);
                        }
                        return new TapPair<>(fuseOrigin, fuseTarget);
                    } else {
                        skipNodeInto(fuseTarget, fuseOrigin);
                        if (this.debug) {
                            TapEnv.printlnOnTrace("  target disappearing into origin " + fuseOrigin);
                        }
                        return new TapPair<>(fuseTarget, fuseOrigin);
                    }
                }
            }
            allNodes = allNodes.tail;
        }
        return null;
    }

    private void skipNodeInto(NewBlockGraphNode skippedNode, NewBlockGraphNode intoNode) {
        TapList<NewBlockGraphArrow> arrows;
        NewBlockGraphArrow arrow;
        nodes = TapList.delete(skippedNode, nodes);
        arrows = skippedNode.backFlow;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.origin() == intoNode) {
                arrow.redirectOrigin(null);
            } else {
                arrow.redirectDestination(intoNode);
            }
            arrows = arrows.tail;
        }
        arrows = skippedNode.flow;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.destination() == intoNode) {
                arrow.redirectDestination(null);
            } else {
                arrow.redirectOrigin(intoNode);
            }
            arrows = arrows.tail;
        }
    }

    protected void openGroup() {
        if (inGroupLevel == 0) {
            lastNodeInGroup = null;
        }
        inGroupLevel++;
    }

    protected void closeGroup() {
        inGroupLevel--;
        if (inGroupLevel == 0) {
            lastNodeInGroup = null;
        }
    }

    protected void addDataDepNode(NewBlockGraphNode newNode, boolean atTail,
                                  boolean chain, boolean followPointers, boolean incrementalCondense,
                                  TapList<Tree> localUses, TapList<Tree> localDefs,
                                  TapList<Tree> localBisUses, TapList<Tree> localBisDefs) {
        if (atTail) {
            /* add the node at the end. Number it "nodeCount" */
            if (nodes == null) {
                nodes = new TapList<>(newNode, null);
            } else {
                TapList<NewBlockGraphNode> inNodes = nodes;
                while (inNodes.tail != null) {
                    inNodes = inNodes.tail;
                }
                inNodes.tail = new TapList<>(newNode, null);
            }
            newNode.subOrder = nodeCount;
        } else {
            /* add the node at the beginning. Number it "-nodeCount" */
            nodes = new TapList<>(newNode, nodes);
            newNode.subOrder = -nodeCount;
        }
        nodeCount++;
        /* Manage the nodes that must be chained because of a hidden dependency (hiddenDep): */
        if (chain) {
            if (lastNodeInChain != null) {
                if (atTail) {
                    new NewBlockGraphArrow(lastNodeInChain, newNode);
                } else {
                    new NewBlockGraphArrow(newNode, lastNodeInChain);
                }
            }
            lastNodeInChain = newNode;
        }
        /* Manage the nodes that must be grouped: */
        if (inGroupLevel > 0) {
            if (lastNodeInGroup != null) {
                if (atTail) {
                    new NewBlockGraphArrow(lastNodeInGroup, newNode);
                } else {
                    new NewBlockGraphArrow(newNode, lastNodeInGroup);
                }
            }
            lastNodeInGroup = newNode;
        }
        if (debug) {
            TapEnv.printlnOnTrace("ADDING NODE: " + newNode + (chain ? " CHAINED!" : ""));
            TapEnv.printlnOnTrace(" READS  primal: " + localUses + " AND diff: " + localBisUses + " following pointers:" + followPointers);
            TapEnv.printlnOnTrace(" WRITES primal: " + localDefs + " AND diff: " + localBisDefs + " following pointers:" + followPointers);
        }
        reallocateIfNeeded(localUses, localDefs, localBisUses, localBisDefs,
                newNode.srcInstruction, followPointers);
        setDepends(newNode, atTail, followPointers, false,
                localUses, localDefs, visibleSrcUses, visibleSrcDefs);
        setDepends(newNode, atTail, followPointers, true,
                localBisUses, localBisDefs, visibleBisUses, visibleBisDefs);
        if (incrementalCondense) {
            TapPair<NewBlockGraphNode, NewBlockGraphNode> mergedPair = condenseNewNode(newNode, atTail);
            if (mergedPair != null) {
                NewBlockGraphNode mergedNode = mergedPair.first;
                NewBlockGraphNode intoNode = mergedPair.second;
                replaceInUseDefs(mergedNode, intoNode, visibleSrcUses);
                replaceInUseDefs(mergedNode, intoNode, visibleBisUses);
                replaceInUseDefs(mergedNode, intoNode, visibleSrcDefs);
                replaceInUseDefs(mergedNode, intoNode, visibleBisDefs);
                if (lastNodeInChain == mergedNode) {
                    lastNodeInChain = intoNode;
                }
                if (lastNodeInGroup == mergedNode) {
                    lastNodeInGroup = intoNode;
                }
            }
        }
    }

    private void reallocateIfNeeded(TapList<Tree> nodeUses, TapList<Tree> nodeDefs,
                                    TapList<Tree> nodeBisUses, TapList<Tree> nodeBisDefs,
                                    Instruction instruction, boolean followPointers) {
        Tree newTree;
        TapIntList zones;
        int maxZone = -1;
        while (nodeDefs != null) {
            newTree = nodeDefs.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            while (zones != null) {
                if (zones.head > maxZone) {
                    maxZone = zones.head;
                }
                zones = zones.tail;
            }
            nodeDefs = nodeDefs.tail;
        }
        while (nodeUses != null) {
            newTree = nodeUses.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            while (zones != null) {
                if (zones.head > maxZone) {
                    maxZone = zones.head;
                }
                zones = zones.tail;
            }
            nodeUses = nodeUses.tail;
        }
        while (nodeBisDefs != null) {
            newTree = nodeBisDefs.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            while (zones != null) {
                if (zones.head > maxZone) {
                    maxZone = zones.head;
                }
                zones = zones.tail;
            }
            nodeBisDefs = nodeBisDefs.tail;
        }
        while (nodeBisUses != null) {
            newTree = nodeBisUses.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            while (zones != null) {
                if (zones.head > maxZone) {
                    maxZone = zones.head;
                }
                zones = zones.tail;
            }
            nodeBisUses = nodeBisUses.tail;
        }
        int oldMaxZone = visibleSrcUses.length - 2;
        if (maxZone > oldMaxZone) {
            int oldLen = visibleSrcUses.length;
            // Reallocate to size maxZone+5, + 1 for zone "riskAlias":
            int newLen = maxZone + 6;
            @SuppressWarnings("unchecked")
            TapList<TapPair<Tree, NewBlockGraphNode>>[] newVisibleSrcUses = new TapList[newLen];
            @SuppressWarnings("unchecked")
            TapList<TapPair<Tree, NewBlockGraphNode>>[] newVisibleBisUses = new TapList[newLen];
            @SuppressWarnings("unchecked")
            TapList<TapPair<Tree, NewBlockGraphNode>>[] newVisibleSrcDefs = new TapList[newLen];
            @SuppressWarnings("unchecked")
            TapList<TapPair<Tree, NewBlockGraphNode>>[] newVisibleBisDefs = new TapList[newLen];
            int i;
            for (i = newLen - 1; i >= oldLen; --i) {
                newVisibleSrcUses[i] = null;
                newVisibleBisUses[i] = null;
                newVisibleSrcDefs[i] = null;
                newVisibleBisDefs[i] = null;
            }
            for (i = oldLen - 1; i >= 0; --i) {
                newVisibleSrcUses[i] = visibleSrcUses[i];
                newVisibleBisUses[i] = visibleBisUses[i];
                newVisibleSrcDefs[i] = visibleSrcDefs[i];
                newVisibleBisDefs[i] = visibleBisDefs[i];
            }
            visibleSrcUses = newVisibleSrcUses;
            visibleBisUses = newVisibleBisUses;
            visibleSrcDefs = newVisibleSrcDefs;
            visibleBisDefs = newVisibleBisDefs;
        }
    }

    private void replaceInUseDefs(NewBlockGraphNode mergedNode, NewBlockGraphNode intoNode,
                                  TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleNodes) {
        TapList<TapPair<Tree, NewBlockGraphNode>> visib;
        NewBlockGraphNode oldNode;
        for (int i = visibleNodes.length - 1; i >= 0; i--) {
            visib = visibleNodes[i];
            while (visib != null) {
                oldNode = visib.head.second;
                if (oldNode == mergedNode) {
                    visib.head.second = intoNode;
                }
                visib = visib.tail;
            }
        }
    }

    private void setDepends(NewBlockGraphNode newNode, boolean atTail, boolean followPointers, boolean isDiff,
                            TapList<Tree> nodeUses, TapList<Tree> nodeDefs,
                            TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleUses,
                            TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleDefs) {
        Tree newTree;
        TapIntList zones;
        Instruction instruction = newNode.srcInstruction;
        Unit curUnit = instruction == null ? null : instruction.block.unit();
        int nbPublicZones = curUnit == null ? 0 : curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
        int zone;
        TapList<Tree> inNodeUses;
        TapList<Tree> inNodeDefs = nodeDefs;
        while (inNodeDefs != null) {
            newTree = inNodeDefs.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            if (debug) {
                TapEnv.printlnOnTrace((isDiff ? "Diff " : "Primal ") + newTree + " writes " + zones);
            }
            while (zones != null) {
                zone = zones.head;
                if (zone >= -1) {
                    setDepsForOneZone(newNode, newTree, zone + 1, visibleUses, isDiff ? "visibleBisUses" : "visibleSrcUses", nbPublicZones, atTail);
                    setDepsForOneZone(newNode, newTree, zone + 1, visibleDefs, isDiff ? "visibleBisDefs" : "visibleSrcDefs", nbPublicZones, atTail);
                }
                zones = zones.tail;
            }
            inNodeDefs = inNodeDefs.tail;
        }
        if (atTail) {
            inNodeUses = nodeUses;
            while (inNodeUses != null) {
                newTree = inNodeUses.head;
                zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
                if (debug) {
                    TapEnv.printlnOnTrace((isDiff ? "Diff " : "Primal ") + newTree + "  reads " + zones);
                }
                while (zones != null) {
                    zone = zones.head;
                    if (zone >= -1) {
                        setDepsForOneZone(newNode, newTree, zone + 1, visibleDefs, isDiff ? "visibleBisDefs" : "visibleSrcDefs", nbPublicZones, atTail);
                        if (debug) {
                            TapEnv.printlnOnTrace("                " + (isDiff ? "visibleBisUses" : "visibleSrcUses") + " of zone <" + zone + "> receive " + newTree + " from node " + newNode);
                        }
                        visibleUses[zone + 1] = new TapList<>(new TapPair<>(newTree, newNode), visibleUses[zone + 1]);
                    }
                    zones = zones.tail;
                }
                inNodeUses = inNodeUses.tail;
            }
        }
        inNodeDefs = nodeDefs;
        while (inNodeDefs != null) {
            newTree = inNodeDefs.head;
            zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
            while (zones != null) {
                zone = zones.head;
                if (zone >= -1) {
                    /* Until symbolTable.hidesRef returns something non-trivial,
                     * removeHiddenRefs does nothing !!!!
                     visibleDefs[zone+1] = removeHiddenRefs(visibleDefs[zone+1], newTree) ;
                     visibleUses[zone+1] = removeHiddenRefs(visibleUses[zone+1], newTree) ;
                     */
                    if (debug) {
                        TapEnv.printlnOnTrace("                " + (isDiff ? "visibleBisDefs" : "visibleSrcDefs") + " of zone <" + zone + "> receive " + newTree + " from node " + newNode);
                    }
                    visibleDefs[zone + 1] = new TapList<>(new TapPair<>(newTree, newNode), visibleDefs[zone + 1]);
                }
                zones = zones.tail;
            }
            inNodeDefs = inNodeDefs.tail;
        }
        if (!atTail) {
            inNodeUses = nodeUses;
            while (inNodeUses != null) {
                newTree = inNodeUses.head;
                zones = listAllZonesPossiblyFollowingPointers(newTree, instruction, followPointers);
                if (debug) {
                    TapEnv.printlnOnTrace((isDiff ? "Diff " : "Primal ") + newTree + "  reads " + zones);
                }
                while (zones != null) {
                    zone = zones.head;
                    if (zone >= -1) {
                        setDepsForOneZone(newNode, newTree, zone + 1, visibleDefs, isDiff ? "visibleBisDefs" : "visibleSrcDefs", nbPublicZones, atTail);
                        if (debug) {
                            TapEnv.printlnOnTrace("                " + (isDiff ? "visibleBisUses" : "visibleSrcUses") + " of zone <" + zone + "> receive " + newTree + " from node " + newNode);
                        }
                        visibleUses[zone + 1] = new TapList<>(new TapPair<>(newTree, newNode), visibleUses[zone + 1]);
                    }
                    zones = zones.tail;
                }
                inNodeUses = inNodeUses.tail;
            }
        }
    }

    private TapIntList listAllZonesPossiblyFollowingPointers(Tree expression, Instruction instruction, boolean followPointers) {
        TapList zonesTree = symbolTable.treeOfZonesOfValue(expression, null, instruction, null);
        if (followPointers) {
            zonesTree = TapList.copyTree(zonesTree);
            DataFlowAnalyzer.includePointedElementsInTree(zonesTree, null, null, true, symbolTable, instruction, false, false);
        }
        return ZoneInfo.listAllZones(zonesTree, true);
    }

    private void setDepsForOneZone(NewBlockGraphNode newNode, Tree newTree, int newZoneRk,
                                   TapList<TapPair<Tree, NewBlockGraphNode>>[] visibleRefs,
                                   String visibleName, int nbPublicZones, boolean atTail) {
        NewBlockGraphNode oldNode;
        Tree oldTree;
        boolean isUndefCase;
        TapList<TapPair<Tree, NewBlockGraphNode>> oldRefs;
        for (int zrk = visibleRefs.length - 1; zrk >= 0; --zrk) {
            // isUndefCase is true when one end of the dependence can reference an Undef place
            // (which means it can be any of the argument zones of the current Unit) AND the
            // other end of the dependence is actually one of the argument zones of the current Unit.
            isUndefCase = zrk == 0 && visibleRefs[0] != null && newZoneRk <= nbPublicZones || newZoneRk == 0 && zrk <= nbPublicZones;
            if (debug) {
                if (isUndefCase) {
                    TapEnv.printlnOnTrace("     Warning: undef case " + newZoneRk + "->" + zrk + " full deps!");
                }
            }
            if (isUndefCase || zrk == newZoneRk) {
                oldRefs = visibleRefs[zrk];
                if (debug) {
                    TapEnv.printlnOnTrace("                " + visibleName + " of zone <" + (zrk - 1) + "> are " + oldRefs);
                }
                while (oldRefs != null) {
                    oldNode = oldRefs.head.second;
                    oldTree = oldRefs.head.first;
                    if (newNode != oldNode) {
                        if (atTail) {
                            if (!existsArrow(oldNode, newNode)
                                    && (isUndefCase || refsMayOverlap(newTree, newNode.srcInstruction, oldTree, oldNode.srcInstruction))) {
                                new NewBlockGraphArrow(oldNode, newNode);
                                if (debug) {
                                    TapEnv.printlnOnTrace(" [" + oldNode.subOrder + "]->[" + newNode.subOrder + "]");
                                }
                            }
                        } else {
                            if (!existsArrow(newNode, oldNode)
                                    && (isUndefCase || refsMayOverlap(oldTree, oldNode.srcInstruction, newTree, newNode.srcInstruction))) {
                                new NewBlockGraphArrow(newNode, oldNode);
                                if (debug) {
                                    TapEnv.printlnOnTrace(" [" + newNode.subOrder + "]->[" + oldNode.subOrder + "]");
                                }
                            }
                        }
                    }
                    oldRefs = oldRefs.tail;
                }
            }
        }
    }

    private boolean refsMayOverlap(Tree tree1, Instruction instr1, Tree tree2, Instruction instr2) {
        //[llh] This test is far too weak (cf set08/lh013).
        // We may try refine it (polyhedra?), and/or introduce here a special treatment
        // for dependencies between increments, i.e. 2 successive increments are independent!
        return ILUtils.eqOrDisjointRef(tree1, tree2, instr1, instr2) != -1;
    }

    //[llh 07/02/2013] TODO: we are missing a mechanism to induce the topological order to
    // prefer orders that optimize the generated code, e.g. on F90:lh08
    // The merging that is done during condense() is too strong.
    // Here we would need to make nodes closer (but not fused)
    // e.g. when they are in the same where-elsewhere:

    /**
     * @return the ordered list of all NewBlockGraphNode's.
     */
    protected TapList<NewBlockGraphNode> topoSort() {
        TapList<NewBlockGraphNode> result = null;
        TapList<NewBlockGraphNode> inNodes = nodes;
        TapList<NewBlockGraphNode> freeList = new TapList<>(null, null);
        NewBlockGraphNode node, origNode;
        while (inNodes != null) {
            node = inNodes.head;
            node.nbDeps = TapList.length(node.flow);
            if (node.nbDeps == 0) {
                insertNodeIntoFreeList(node, freeList);
            }
            inNodes = inNodes.tail;
        }
        TapList<NewBlockGraphArrow> origins;
        while (freeList.tail != null) {
            node = freeList.tail.head;
            freeList.tail = freeList.tail.tail;
            result = new TapList<>(node, result);
            origins = node.backFlow;
            while (origins != null) {
                origNode = origins.head.origin();
                origNode.nbDeps = origNode.nbDeps - 1;
                if (origNode.nbDeps == 0) {
                    insertNodeIntoFreeList(origNode, freeList);
                }
                origins = origins.tail;
            }
        }
        if (TapList.length(result) != TapList.length(nodes)) {
            TapEnv.printlnOnTrace("WARNING lost nodes from " + nodes);
            TapEnv.printlnOnTrace("     TO " + result);
        }
        return result;
    }

    private void insertNodeIntoFreeList(NewBlockGraphNode node, TapList<NewBlockGraphNode> freeList) {
        while (freeList.tail != null
                && freeList.tail.head.subOrder > node.subOrder) {
            freeList = freeList.tail;
        }
        freeList.placdl(node);
    }
}
