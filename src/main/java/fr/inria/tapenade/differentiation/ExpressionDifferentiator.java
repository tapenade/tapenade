/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.ArrayTypeSpec;
import fr.inria.tapenade.representation.AtomFuncDerivative;
import fr.inria.tapenade.representation.CompositeTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.InstructionMask;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PrimitiveTypeSpec;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeDecl;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Operator;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * Object that knows how to differentiate an expression.
 */
public class ExpressionDifferentiator {

    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    /**
     * @return the global adEnv.blockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * @return the global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /** number of replicas of differentiated vars (cf -replicateDiff N) */
    private int replicas = 1 ;

    /**
     * @return the global adEnv.multiDirMode.
     */
    private boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * @return the global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * If true, we want to generate the tgt diff expression, else the adjoint diff expression.
     * Should be simply a shortcut for curDiffUnitSort()==TANGENT ?
     */
    private boolean tangentDiff = true;

    /**
     * Values for estimation of run-time costs.
     */
    private static final int CST_COST = 0;
    private static final int VAR_COST = 1;
    private static final int ASS_COST = 1;
    private static final int ADD_COST = 1;
    private static final int SUB_COST = 1;
    private static final int MUL_COST = 4;
    private static final int DIV_COST = 6;
    private static final int REDUC_COST = 10;
    private static final int POW_COST = 15;
    private static final int CALL_COST = 20;

    /**
     * The cost attached to introducing a new temporary variable.
     * The smaller this cost, the manier and smaller the sub-expressions that are cut in diff instructions.
     */
    private static final int NEW_TEMP_VAR_COST = 5;

    /**
     * Constructor.
     */
    protected ExpressionDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
        this.replicas = TapEnv.diffReplica() ;
    }

    /**
     * Tangent differentiation of expression from assignment lhs:=expression.
     * @return an array (one per replica, in general only one) of tangent diff assignment.
     */
    protected Tree[] tangentDifferentiateAssignedExpression(
            Tree lhs, Tree expression, TapList<NewBlockGraphNode> toPrecomputes,
            TapList<DiffAssignmentNode> toPrecomputesDiff, Tree copySrcAssignment,
            BoolVector beforeActiv,
            TapPair<TapList<Tree>, TapList<Tree>> primRW, TapPair<TapList<Tree>, TapList<Tree>> diffRW,
            SymbolTable diffSymbolTable) {
        TypeSpec lhsType = adEnv.curSymbolTable().typeOf(lhs);
        TapList<ArrayDim> dimensions = lhsType.getAllDimensions();
        Tree[] diffLhsR = new Tree[replicas] ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
            diffLhsR[adEnv.iReplic] =
                varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhs, diffSymbolTable,
                        varRefDifferentiator().isCurFunctionName(ILUtils.baseName(lhs)),
                        lhs, false, true, lhs);
        }
        LinLeaf linLhs =
                new LinLeaf(lhs, diffLhsR, adEnv.curInstruction().whereMask(), true, null, -1); // true here has no meaning (tangent mode!).
        LinAssign mainRoot = new LinAssign(linLhs, null, lhsType, dimensions != null, null, -1);
        TapList<LinAssign> toLinTrees = new TapList<>(mainRoot, null);
        // Rearrange expression to have less complex derivatives, and choose
        // sub-expressions that must be pre-computed into temporary vars :
        expression = rebalanceExp(expression, beforeActiv, adEnv.curSymbolTable(), new ToBool(false), 1);
        tangentDiff = true;
        boolean cutInPrimal = findDiffOptimalCuts(expression, dimensions);
        if (activeExpr(expression)) {
            linearize(expression, mainRoot, -1, dimensions, adEnv.curInstruction().whereMask(),
                      toLinTrees, toPrecomputes, diffSymbolTable);
        }
        if (adEnv.traceCurBlock == 2) {
            TapEnv.printlnOnTrace(" LINEARIZE RHS OF: " + ILUtils.toString(lhs) + " := " + ILUtils.toString(expression));
            TapEnv.printlnOnTrace("    PRIMAL PRECOMPUTES: " + toPrecomputes.tail);
            TapEnv.printlnOnTrace("    TANGENT PRECOMPUTES: " + toLinTrees.tail);
            TapEnv.printlnOnTrace("    LINEARIZATION TREE: " + mainRoot);
        }
        Tree[] tgtAssignTreeR = new Tree[replicas] ;
        Tree tgtAssignTree = null ;
        TapList<LinAssign> subLinTrees ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
            subLinTrees = toLinTrees.tail ;
            while (subLinTrees != null) {
                LinAssign subLinTree = subLinTrees.head ;
                TapPair<TapList<Tree>, TapList<Tree>> subPrimRW = new TapPair<>(null, null) ;
                TapPair<TapList<Tree>, TapList<Tree>> subDiffRW = new TapPair<>(null, null) ;
                Tree tangentTempTree = subLinTree.buildTreeForward(subPrimRW, subDiffRW) ;
                DiffAssignmentNode futureDiffNode =
                    new DiffAssignmentNode(tangentTempTree, DiffConstants.SET_VARIABLE,
                                           null, subLinTree.linLhs.ref, adEnv.iReplic,
                                           subPrimRW.first, subPrimRW.second, subDiffRW.first, subDiffRW.second) ;
                futureDiffNode.type = subLinTree.assignedType ;
                toPrecomputesDiff.placdl(futureDiffNode);
                subLinTrees = subLinTrees.tail ;
            }
            tgtAssignTree = mainRoot.buildTreeForward(primRW, diffRW);
            Tree zeroTree = lhsType.buildConstantZero(); //just in case
            if (ILUtils.isNullOrNone(tgtAssignTree.down(2)) && zeroTree != null) {
                tgtAssignTree.setChild(zeroTree, 2);
            }
            if (tgtAssignTree.down(2).opCode() == ILLang.op_ifExpression
                && ILUtils.isNullOrNone(tgtAssignTree.down(2).down(2)) && zeroTree != null) {
                tgtAssignTree.down(2).setChild(zeroTree, 2);
            }
            if (1 == ILUtils.eqOrDisjointRef(tgtAssignTree.down(1), tgtAssignTree.down(2),
                                             adEnv.curInstruction(), adEnv.curInstruction())) {
                // Generate nothing for no-op Xd:=Xd
                tgtAssignTree = null;
            }
            tgtAssignTreeR[adEnv.iReplic] = tgtAssignTree ;
        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        if (copySrcAssignment != null && tgtAssignTree != null && cutInPrimal) {
            copySrcAssignment.setChild(replacePrecomputedAndCustomize(ILUtils.copy(expression)), 2);
            copySrcAssignment.removeAnnotation("sourcetree");
        }
        if (adEnv.traceCurBlock == 2) {
            TapEnv.printlnOnTrace("    MAIN TANGENT ASSIGN TREE:" + ILUtils.toString(tgtAssignTreeR[0])
                                  +(tgtAssignTreeR.length==1 ? "" : " *"+tgtAssignTreeR.length+"replicas")) ;
            TapEnv.printlnOnTrace("     primR:" + primRW.first + " primW:" + primRW.second + " diffR:" + diffRW.first + " diffW:" + diffRW.second);
            TapEnv.printlnOnTrace("    PRIMAL ASSIGNMENT:" + copySrcAssignment);
        }
        return tgtAssignTreeR;
    }

    // [llh] Try merge with tangentDifferentiateAssignedExpression ?

    /**
     * Tangent differentiation of a standalone expression (given in toPrimalExp.obj).
     * @return an array (one per replica, in general only one) of tangent diff expression.
     */
    protected Tree[] tangentDifferentiatePlainExpression(
            ToObject<Tree> toPrimalExp, TapList<NewBlockGraphNode> toPrecomputes,
            TapList<DiffAssignmentNode> toPrecomputesDiff, BoolVector beforeActiv,
            TapPair<TapList<Tree>, TapList<Tree>> primRW, TapPair<TapList<Tree>, TapList<Tree>> diffRW,
            SymbolTable diffSymbolTable) {
        Tree expr = toPrimalExp.obj();
        TypeSpec exprType = adEnv.curSymbolTable().typeOf(expr);
        TapList<ArrayDim> dimensions = exprType.getAllDimensions();
        Tree[] dummyDiffLhsR = new Tree[replicas] ;
        // dummy assignment lhs
        LinLeaf linLhs = new LinLeaf(null, dummyDiffLhsR, adEnv.curInstruction().whereMask(), true, null, -1);
        // dummy assignment
        LinAssign mainRoot = new LinAssign(linLhs, null, exprType, false, null, -1);
        TapList<LinAssign> toLinTrees = new TapList<>(mainRoot, null);
        // Rearrange expression to have less complex derivatives, and choose
        // sub-expressions that must be pre-computed into temporary vars :
        Tree expression = rebalanceExp(expr, beforeActiv, adEnv.curSymbolTable(), new ToBool(false), 1);
        tangentDiff = true;
        boolean cutInPrimal = findDiffOptimalCuts(expression, dimensions);
        if (activeExpr(expression)) {
            linearize(expression, mainRoot, -1, dimensions, adEnv.curInstruction().whereMask(),
                    toLinTrees, toPrecomputes, diffSymbolTable);
        }
        if (adEnv.traceCurBlock == 2) {
            TapEnv.printlnOnTrace(" LINEARIZE EXPRESSION: " + expression + " (from expr: " + expr + ')');
            TapEnv.printlnOnTrace("    PRIMAL PRECOMPUTES: " + toPrecomputes.tail);
            TapEnv.printlnOnTrace("    TANGENT PRECOMPUTES: " + toLinTrees.tail);
            TapEnv.printlnOnTrace("    LINEARIZATION TREE: " + mainRoot);
        }
        if (multiDirMode()) {
            // force multi-dir index to be "nd" and not ":"
            varRefDifferentiator().multiDirIterDescriptor.setJustAnIndex(true);
        }
        Tree[] tgtAssignTreeR = new Tree[replicas] ;
        Tree tgtAssignTree ;
        TapList<LinAssign> subLinTrees ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
            subLinTrees = toLinTrees.tail ;
            while (subLinTrees != null) {
                LinAssign subLinTree = toLinTrees.tail.head ;
                TapPair<TapList<Tree>, TapList<Tree>> subPrimRW = new TapPair<>(null, null) ;
                TapPair<TapList<Tree>, TapList<Tree>> subDiffRW = new TapPair<>(null, null) ;
                Tree tangentTempTree = subLinTree.buildTreeForward(subPrimRW, subDiffRW) ;
                DiffAssignmentNode futureDiffNode =
                    new DiffAssignmentNode(tangentTempTree, DiffConstants.SET_VARIABLE,
                                       null, subLinTree.linLhs.ref, adEnv.iReplic,
                                       subPrimRW.first, subPrimRW.second, subDiffRW.first, subDiffRW.second) ;
                futureDiffNode.type = subLinTree.assignedType ;
                toPrecomputesDiff.placdl(futureDiffNode);
                subLinTrees = subLinTrees.tail ;
            }
            tgtAssignTree = mainRoot.buildTreeForward(primRW, diffRW);
            tgtAssignTree = tgtAssignTree.cutChild(2); // drop dummy assignment
            if (ILUtils.isNullOrNone(tgtAssignTree)) {
                tgtAssignTree = null;
            }
            tgtAssignTreeR[adEnv.iReplic] = tgtAssignTree ;
        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        if (multiDirMode()) {
            // undo forcing multi-dir index to be "nd" and not ":"
            varRefDifferentiator().multiDirIterDescriptor.setJustAnIndex(false);
        }
        // If we just decided to split a part of the primal expr, replace in the (copied) primal expr, and
        // remove the "sourcetree" annotation, so that code decompilation doesn't reuse the original expression:
        if (cutInPrimal) {
            toPrimalExp.setObj(replacePrecomputedAndCustomize(ILUtils.copy(expression)));
        }
        if (adEnv.traceCurBlock == 2) {
            TapEnv.printlnOnTrace(" TANGENT DIFF EXPRESSION: "+ILUtils.toString(tgtAssignTreeR[0])
                                  +(tgtAssignTreeR.length==1 ? "" : " *"+tgtAssignTreeR.length+"replicas")
                                  +" (PRIMAL EXPRESSION:" + toPrimalExp.obj() + ')');
        }
        return tgtAssignTreeR;
    }

    // [llh] Try merge with tangentDifferentiateAssignedExpression ?

    /**
     * Adjoint differentiation of expression from assignment lhs:=expression.
     *
     * @param needSetDiffLhs when true (general case, e.g. diffLhs is needed upstream)
     *                       we must generate an instruction that recomputes diffLhs.
     * @param needDiffRhs    when true (general case, e.g. diffLhs is active downstream)
     *                       we must generate the updates of the diff variables of the rhs expression.
     * @return an array (one per replica, in general only one) of lists of future differentiated assignments
     */
    protected TapList<DiffAssignmentNode>[] adjointDifferentiateAssignedExpression(
            Tree lhs, boolean needSetDiffLhs, Tree expression, boolean needDiffRhs,
            TapList<NewBlockGraphNode> toPrecomputes,
            BoolVector beforeActiv,
            SymbolTable diffSymbolTable) {
        TypeSpec lhsType = adEnv.curSymbolTable().typeOf(lhs);
        TapList<ArrayDim> dimensions = lhsType.getAllDimensions();
        Tree[] diffLhsR = new Tree[replicas] ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
            diffLhsR[adEnv.iReplic] =
                varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhs, diffSymbolTable,
                        false, lhs, false, true, lhs);
        }
        // If needSetDiffLhs==true here, this will force the coming "mainRoot" LinTree to
        // generate computation of the upstream diffLhs,
        // i.e. either a SET diffLhs:=xx*diffLhs or a RESET diffLhs:=0
        LinLeaf linLhs = new LinLeaf(lhs, diffLhsR, adEnv.curInstruction().whereMask(), needSetDiffLhs, null, -1);
        LinAssign mainRoot = new LinAssign(linLhs, null, lhsType, dimensions != null, null, -1);
        TapList<LinAssign> toLinTrees = new TapList<>(mainRoot, null);
        // In the (special) case where needDiffRhs==false, we only need to reset diffLhs:=0,
        //  which is already implied by the initial mainRoot.
        if (needDiffRhs) {
            // Rearrange expression to have less complex derivatives, and choose
            // sub-expressions that must be pre-computed into temporary vars :
            expression = rebalanceExp(expression, beforeActiv, adEnv.curSymbolTable(), new ToBool(false), 1);
            tangentDiff = false;
            boolean cutInPrimal = findDiffOptimalCuts(expression, dimensions);
            if (activeExpr(expression)) {
                linearize(expression, mainRoot, -1, dimensions, adEnv.curInstruction().whereMask(),
                        toLinTrees, toPrecomputes, diffSymbolTable);
            }
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace(" LINEARIZE RHS OF: " + ILUtils.toString(lhs) + " := " + ILUtils.toString(expression));
                TapEnv.printlnOnTrace("    LINEARIZATION TREE: " + mainRoot);
                TapEnv.printlnOnTrace("    ADDITIONAL LINEARIZATION TREES: " + toLinTrees.tail);
                TapEnv.printlnOnTrace("    PRIMAL PRECOMPUTES: " + toPrecomputes.tail);
            }
        }
        TapList<DiffAssignmentNode>[] adjAssignmentNodesR = new TapList[replicas] ;
        TapList<LinAssign> inLinTrees ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
            TapList<DiffAssignmentNode> adjAssignmentNodes = null;
            inLinTrees = toLinTrees ;
            while (inLinTrees != null) {
                adjAssignmentNodes = TapList.append(inLinTrees.head.buildTreesBackward(), adjAssignmentNodes);
                inLinTrees = inLinTrees.tail;
            }
            adjAssignmentNodesR[adEnv.iReplic] = adjAssignmentNodes ;
        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        return adjAssignmentNodesR;
    }

    /**
     * Replaces all occurrences of a precomputed sub-expression of expr with the corresponding
     * precomputed variable, then cleans expr of all its DiffCutSupport annotations.
     * May also customize as requested e.g. x^2 \rArr; x*x.
     */
    private Tree replacePrecomputedAndCustomize(Tree expr) {
        if (expr == null) {
            return null;
        }
        DiffCutSupport cutHere = getCutCosts(expr);
        if (cutHere != null) {
            expr.removeAnnotation("CutCosts");
        }
        if (cutHere != null && cutHere.cutPrimal) {
            NewSymbolHolder tmpVarHolder = cutHere.tmpVarHolder;
            expr = tmpVarHolder.makeNewRef(null);
        } else if (!expr.isAtom()) {
            Tree[] children = expr.children();
            for (int i = children.length - 1; i >= 0; --i) {
                expr.setChild(replacePrecomputedAndCustomize(children[i]), i + 1);
            }
            if (expr.opCode() == ILLang.op_power && expr.down(2).opCode() == ILLang.op_intCst
                //&& Block.isCheapDuplicable(expr.down(1), 2)
            ) {
                if (ILUtils.isIntCst(expr.down(2), 2)) {
                    expr = ILUtils.build(ILLang.op_mul,
                            ILUtils.copy(expr.down(1)),
                            ILUtils.copy(expr.down(1)));
                } else if (ILUtils.isIntCst(expr.down(2), 3)) {
                    expr = ILUtils.build(ILLang.op_mul,
                            ILUtils.build(ILLang.op_mul,
                                    ILUtils.copy(expr.down(1)),
                                    ILUtils.copy(expr.down(1))),
                            ILUtils.copy(expr.down(1)));
                }
            }
        }
        return expr;
    }

//------------------------------- REBALANCE:

    /**
     * Transforms an expression into a new equivalent expression that is better
     * balanced for AD, e.g. sums and multiplications and divisions are gathered
     * and put as well-balanced binary trees. The new expression shares nothing
     * with the original expression, except for the annotations.
     * "mode" may be 0:flat or 1:tree, specifies the regeneration shape of
     * commutative/associative operations. Arguments beforeActiv and
     * symbolTable are not used and may be null when mode=0=flat.
     */
    private Tree rebalanceExp(Tree tree, BoolVector beforeActiv,
                              SymbolTable symbolTable, ToBool toActive, int mode) {
        switch (tree.opCode()) {
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_minus:
                Tree rebalancedTree = rebalanceTerms(tree, beforeActiv,
                        symbolTable, toActive, mode);
                if (rebalancedTree == null) {
                    rebalancedTree = PrimitiveTypeSpec.buildRealConstantZero();
                }
                return rebalancedTree;
            case ILLang.op_mul:
            case ILLang.op_div: {
                ToBool positive = new ToBool(true);
                Tree bTree = rebalanceFactors(tree, positive, beforeActiv,
                        symbolTable, toActive, mode);
                boolean wasActive = ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), bTree, adEnv.curSymbolTable());
                if (!positive.get()) {
                    bTree = ILUtils.buildExpressionOpposite(bTree);
                }
                // Copy the activity annotation:
                if (wasActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), bTree);
                }
                return bTree;
            }
            case ILLang.op_ifExpression: {
                Tree rebalancedTrue = rebalanceExp(tree.down(2),
                        beforeActiv, symbolTable, toActive, mode);
                Tree rebalancedFalse = rebalanceExp(tree.down(3),
                        beforeActiv, symbolTable, toActive, mode);
                Tree newTree = ILUtils.build(ILLang.op_ifExpression,
                        ILUtils.copy(tree.down(1)),
                        rebalancedTrue,
                        rebalancedFalse);
                // Copy the activity annotation:
                if (ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable())) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                return newTree;
            }
            case ILLang.op_power:
            case ILLang.op_complexConstructor: {
                Tree result = ILUtils.build(tree.opCode(),
                        rebalanceExp(tree.down(1), beforeActiv, symbolTable, toActive,
                                mode),
                        rebalanceExp(tree.down(2), beforeActiv, symbolTable, toActive,
                                mode));
                // Copy the activity annotation:
                if (ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), result.down(1), adEnv.curSymbolTable())
                        || ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), result.down(2), adEnv.curSymbolTable())) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), result);
                }
                return result;
            }
            case ILLang.op_call: {
                Tree newTree = ILUtils.buildCall(
                        ILUtils.copy(ILUtils.getCalledName(tree)),
                        rebalanceExp(ILUtils.getArguments(tree), beforeActiv, symbolTable,
                                toActive, mode));
                // Copy the functionTypeSpec annotation:
                Object f = tree.getAnnotation("functionTypeSpec");
                if (f != null) {
                    newTree.setAnnotation("functionTypeSpec", f);
                }
                // Copy the arrayReturnTypeSpec annotation:
                f = tree.getAnnotation("arrayReturnTypeSpec");
                if (f != null) {
                    newTree.setAnnotation("arrayReturnTypeSpec", f);
                }
                // Copy the callArrow annotation:
                f = tree.getAnnotation("callArrow");
                if (f != null) {
                    newTree.setAnnotation("callArrow", f);
                }
                // Copy the activity annotation:
                if (ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable())) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                return newTree;
            }
            case ILLang.op_arrayConstructor:
            case ILLang.op_expressions: {
                Tree resultTree = tree.operator().tree();
                Tree[] subTrees = tree.children();
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    resultTree.setChild(rebalanceExp(subTrees[i], beforeActiv, symbolTable, toActive, mode), i + 1);
                }
                return resultTree;
            }
            case ILLang.op_ident:
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
                // HACK: 2 cases here: either rebalanceExp is called with mode==1 and
                // a real beforeActiv, and we must use the activity annotation
                // to separate active and inactive terms (for the future 2 lists
                // of active and passive terms). Or mode==0 and beforeActiv is
                // not given (i.e. is null) and we must always say that the variable
                // is passive because only the list of passive terms will be used.
                if (beforeActiv != null && ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable())) {
                    toActive.set(true);
                }
                return ILUtils.copy(tree);
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_concat:
            case ILLang.op_none:
                // Comparators
            case ILLang.op_lt:
            case ILLang.op_le:
            case ILLang.op_gt:
            case ILLang.op_ge:
            case ILLang.op_eq:
            case ILLang.op_neq:
                // Logical ops Bitwise
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_rightShift:
            case ILLang.op_leftShift:
                // Unary ops
                //  Arithmetic
            case ILLang.op_incr:
            case ILLang.op_decr:
                // Logical ops bitwise
            case ILLang.op_not:
            case ILLang.op_bitNot:
                //
            case ILLang.op_nameEq:
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_sizeof:
            case ILLang.op_allocate:
            case ILLang.op_mod:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_constructorCall:
                return ILUtils.copy(tree);
            default:
                TapEnv.toolWarning(-1, "(Balance expression before differentiation) Unexpected operator: " + tree.opName());
                return ILUtils.copy(tree);
        }
    }

    private Tree rebalanceTerms(Tree tree, BoolVector beforeActiv,
                                SymbolTable symbolTable, ToBool toActive, int mode) {
        TapList<Tree> toPTerms = new TapList<>(null, null);
        TapIntList toPFactors = new TapIntList(1, null);
        TapList<Tree> toATerms = new TapList<>(null, null);
        TapIntList toAFactors = new TapIntList(1, null);
        flattenBalancedTerms(tree, 1, toPTerms, toPFactors, toATerms,
                toAFactors, beforeActiv, symbolTable, mode);
        if (mode == 0) {
            /*flat mode */
            Tree numCstTerm = toPTerms.head;
            Tree allTerms = rebuildBalancedTerms(toPTerms, toPFactors, 0);
            if (numCstTerm != null) {
                allTerms = rebuildAddOneFlatTerm(allTerms, numCstTerm, 1);
            }
            return allTerms;
        } else {
            /*tree mode */
            Tree numCstTerm = toPTerms.head;
            Tree passiveTerms = rebuildBalancedTerms(toPTerms, toPFactors, 1);
            Tree activeTerms = rebuildBalancedTerms(toATerms, toAFactors, 1);
            Tree result;
            if (activeTerms != null) {
                toActive.set(true);
            }
            if (numCstTerm != null) {
                if (passiveTerms != null) {
                    passiveTerms = ILUtils.addTree(numCstTerm, passiveTerms);
                } else {
                    passiveTerms = numCstTerm;
                }
            }
            if (passiveTerms != null) {
                if (activeTerms != null) {
                    result = ILUtils.addTree(passiveTerms, activeTerms);
                } else {
                    result = passiveTerms;
                }
            } else {
                result = activeTerms;
            }
            setSubExpsActive(result);
            return result;
        }
    }

    /**
     * Looks very much like ILUtils.addTermsFrom ...
     */
    private void flattenBalancedTerms(Tree tree, int factor,
                                      TapList<Tree> toPTerms, TapIntList toPFactors, TapList<Tree> toATerms,
                                      TapIntList toAFactors, BoolVector beforeActiv,
                                      SymbolTable symbolTable, int mode) {
        switch (tree.opCode()) {
            case ILLang.op_add:
                flattenBalancedTerms(tree.down(1), factor, toPTerms, toPFactors,
                        toATerms, toAFactors, beforeActiv, symbolTable, mode);
                flattenBalancedTerms(tree.down(2), factor, toPTerms, toPFactors,
                        toATerms, toAFactors, beforeActiv, symbolTable, mode);
                break;
            case ILLang.op_sub:
                flattenBalancedTerms(tree.down(1), factor, toPTerms, toPFactors,
                        toATerms, toAFactors, beforeActiv, symbolTable, mode);
                flattenBalancedTerms(tree.down(2), -factor, toPTerms, toPFactors,
                        toATerms, toAFactors, beforeActiv, symbolTable, mode);
                break;
            case ILLang.op_minus:
                flattenBalancedTerms(tree.down(1), -factor, toPTerms, toPFactors,
                        toATerms, toAFactors, beforeActiv, symbolTable, mode);
                break;
            case ILLang.op_mul:
                if (ILUtils.isExpressionIntConstant(tree.down(1))) {
                    flattenBalancedTerms(tree.down(2),
                            factor * ILUtils.intConstantValue(tree.down(1)), toPTerms,
                            toPFactors, toATerms, toAFactors, beforeActiv, symbolTable,
                            mode);
                } else if (ILUtils.isExpressionIntConstant(tree.down(2))) {
                    flattenBalancedTerms(tree.down(1),
                            factor * ILUtils.intConstantValue(tree.down(2)), toPTerms,
                            toPFactors, toATerms, toAFactors, beforeActiv, symbolTable,
                            mode);
                } else {
                    ToBool positive = new ToBool(true);
                    ToBool toActive = new ToBool(false);
                    Tree bTree = rebalanceFactors(tree, positive, beforeActiv,
                            symbolTable, toActive, mode);
                    if (positive.get()) {
                        if (toActive.get()) {
                            insertBalancedTerm(bTree, factor, toATerms, toAFactors);
                        } else {
                            insertBalancedTerm(bTree, factor, toPTerms, toPFactors);
                        }
                    } else {
                        if (toActive.get()) {
                            insertBalancedTerm(bTree, -factor, toATerms, toAFactors);
                        } else {
                            insertBalancedTerm(bTree, -factor, toPTerms, toPFactors);
                        }
                    }
                }
                break;
            default: {
                ToBool toActive = new ToBool(false);
                Tree bTree = rebalanceExp(tree, beforeActiv, symbolTable, toActive,
                        mode);
                if (toActive.get()) {
                    insertBalancedTerm(bTree, factor, toATerms, toAFactors);
                } else {
                    insertBalancedTerm(bTree, factor, toPTerms, toPFactors);
                }
                break;
            }
        }
    }

    private void insertBalancedTerm(Tree tree, int factor,
                                    TapList<Tree> toTerms, TapIntList toFactors) {
        if (factor != 0 && !ILUtils.evalsToZero(tree)) {
            if (tree.opCode() == ILLang.op_intCst) {
                tree = ILUtils.build(ILLang.op_intCst,
                        factor * tree.intValue());
                factor = 1;
            }
            if (ILUtils.isExpressionNumConstant(tree) &&
                    (factor == -1 || factor == 1)) {
                if (factor == -1) {
                    tree = ILUtils.buildExpressionOpposite(tree);
                }
                if (toTerms.head == null) {
                    toTerms.head = tree;
                } else {
                    toTerms.head = ILUtils.addExpressionsNumConstants(toTerms.head,
                            tree);
                }
            } else {
                insertTimesTree(tree, factor, toTerms, toFactors);
            }
        }
    }

    private Tree rebuildBalancedTerms(TapList<Tree> toTerms,
                                      TapIntList toFactors, int mode) {
        TapList<Tree> inTerms = toTerms;
        TapIntList inFactors = toFactors;
        /*Remove null terms: */
        while (inTerms.tail != null) {
            if (inTerms.tail.head == null ||
                    inFactors.tail.head == 0) {
                inTerms.tail = inTerms.tail.tail;
                inFactors.tail = inFactors.tail.tail;
            } else {
                inTerms = inTerms.tail;
                inFactors = inFactors.tail;
            }
        }
        /*Really rebuild: */
        if (mode == 1) {
            return rebuildBalancedTermsRec(toTerms, toFactors,
                    TapList.length(toTerms) - 1);
        } else { /*mode==0 */
            return rebuildFlatTerms(toTerms.tail, toFactors.tail);
        }
    }

    private Tree rebuildBalancedTermsRec(TapList<Tree> toTerms,
                                         TapIntList toFactors, int length) {
        switch (length) {
            case 0:
                return null;
            case 1: {
                int factor = toFactors.tail.head;
                Tree term = toTerms.tail.head;
                if (factor < -1 || factor > 1) {
                    term = ILUtils.build(ILLang.op_mul,
                            ILUtils.build(ILLang.op_intCst, factor), term);
                } else if (factor == -1) {
                    term = ILUtils.build(ILLang.op_minus, term);
                }
                toFactors.tail = toFactors.tail.tail;
                toTerms.tail = toTerms.tail.tail;
                return term;
            }
            default: {
                Tree leftTree = rebuildBalancedTermsRec(toTerms, toFactors,
                        length - length / 2);
                Tree rightTree = rebuildBalancedTermsRec(toTerms, toFactors,
                        length / 2);
                return ILUtils.addTree(leftTree, rightTree);
            }
        }
    }

    private static Tree rebuildFlatTerms(TapList<Tree> terms, TapIntList factors) {
        Tree result = null;
        while (terms != null) {
            result = rebuildAddOneFlatTerm(result, terms.head,
                    factors.head);
            terms = terms.tail;
            factors = factors.tail;
        }
        return result;
    }

    private static Tree rebuildAddOneFlatTerm(Tree left, Tree right,
                                              int rightFactor) {
        if (left == null) {
            if (rightFactor < -1 || rightFactor > 1) {
                return ILUtils.build(ILLang.op_mul,
                        ILUtils.build(ILLang.op_intCst, rightFactor), right);
            } else if (rightFactor == -1) {
                return ILUtils.build(ILLang.op_minus, right);
            } else {
                return right;
            }
        } else {
            boolean isAdd = true;
            if (rightFactor < 0) {
                isAdd = !isAdd;
                rightFactor = -rightFactor;
            }
            if (ILUtils.isNegativeExpression(right)) {
                isAdd = !isAdd;
                right = ILUtils.buildNumericalOpposite(right);
            }
            if (rightFactor > 1) {
                right = ILUtils.build(ILLang.op_mul,
                        ILUtils.build(ILLang.op_intCst, rightFactor), right);
            }
            if (isAdd) {
                return ILUtils.build(ILLang.op_add, left, right);
            } else {
                return ILUtils.build(ILLang.op_sub, left, right);
            }
        }
    }

    private Tree rebalanceFactors(Tree tree, ToBool positive,
                                  BoolVector beforeActiv, SymbolTable symbolTable, ToBool toActive,
                                  int mode) {
        TapList<Tree> toPFactors = new TapList<>(null, null);
        TapIntList toPPowers = new TapIntList(1, null);
        TapList<Tree> toAFactors = new TapList<>(null, null);
        TapIntList toAPowers = new TapIntList(1, null);
        TapList<Tree> toDPFactors = new TapList<>(null, null);
        TapIntList toDPPowers = new TapIntList(0, null);
        TapList<Tree> toDAFactors = new TapList<>(null, null);
        TapIntList toDAPowers = new TapIntList(0, null);
        flattenBalancedFactors(tree, 1, positive, toPFactors, toPPowers,
                toAFactors, toAPowers, beforeActiv, symbolTable, mode);
        Tree numCstFactor = toPFactors.head;
        if (numCstFactor != null &&
                ILUtils.isNegativeExpression(numCstFactor)) {
            positive.not();
            numCstFactor = ILUtils.buildExpressionOpposite(numCstFactor);
        }
        if (mode == 0) {
            // flat mode (toAFactors is certainly empty!)
            separateDenominator(toPFactors, toPPowers, toDPFactors, toDPPowers);
            if (numCstFactor != null) {
                toPFactors.placdl(numCstFactor);
                toPPowers.placdl(1);
            }
            Tree result = rebuildFlatFactors(toPFactors.tail, toPPowers.tail);
            Tree denominator = rebuildFlatFactors(toDPFactors.tail,
                    toDPPowers.tail);
            if (denominator != null) {
                if (result == null) {
                    result = ILUtils.build(ILLang.op_realCst, "1.0");
                }
                result = ILUtils.build(ILLang.op_div, result, denominator);
            }
            return result;
        } else {
            // tree mode
            if (toAFactors.tail != null) {
                toActive.set(true);
            }
            separateDenominator(toPFactors, toPPowers, toDPFactors, toDPPowers);
            separateDenominator(toAFactors, toAPowers, toDAFactors, toDAPowers);
            Tree denominator;
            Tree result;

            denominator = rebuildBalancedFactorsRec(toDPFactors, toDPPowers,
                    null);
            if (denominator != null) {
                toDAFactors.placdl(denominator);
                toDAPowers.placdl(1);
            }
            denominator = rebuildBalancedFactorsRec(toDAFactors, toDAPowers,
                    null);
            if (numCstFactor != null) {
                toPFactors.newR(numCstFactor);
                toPPowers.newR(1);
            }
            if (toAFactors.tail == null) {
                result = rebuildBalancedFactorsRec(toPFactors, toPPowers,
                        denominator);
            } else {
                result = rebuildBalancedFactorsRec(toAFactors, toAPowers,
                        denominator);
                toPFactors.newR(result);
                toPPowers.newR(1);
                result = rebuildBalancedFactorsRec(toPFactors, toPPowers, null);
            }
            if (result == null) {
                result = ILUtils.build(ILLang.op_realCst, "1.0");
            } else {
                setSubExpsActive(result);
            }
            return result;
        }
    }

    private void flattenBalancedFactors(Tree tree, int power, ToBool positive,
                                        TapList<Tree> toPFactors, TapIntList toPPowers, TapList<Tree> toAFactors,
                                        TapIntList toAPowers, BoolVector beforeActiv,
                                        SymbolTable symbolTable, int mode) {
        switch (tree.opCode()) {
            case ILLang.op_mul:
                flattenBalancedFactors(tree.down(1), power, positive, toPFactors,
                        toPPowers, toAFactors, toAPowers, beforeActiv, symbolTable,
                        mode);
                flattenBalancedFactors(tree.down(2), power, positive, toPFactors,
                        toPPowers, toAFactors, toAPowers, beforeActiv, symbolTable,
                        mode);
                break;
            case ILLang.op_div:
                flattenBalancedFactors(tree.down(1), power, positive, toPFactors,
                        toPPowers, toAFactors, toAPowers, beforeActiv, symbolTable,
                        mode);
                flattenBalancedFactors(tree.down(2), -power, positive, toPFactors,
                        toPPowers, toAFactors, toAPowers, beforeActiv, symbolTable,
                        mode);
                break;
            case ILLang.op_minus:
                if (power % 2 != 0) {
                    positive.not();
                }
                flattenBalancedFactors(tree.down(1), power, positive, toPFactors,
                        toPPowers, toAFactors, toAPowers, beforeActiv, symbolTable,
                        mode);
                break;
            case ILLang.op_power:
                if (ILUtils.isExpressionIntConstant(tree.down(2))) {
                    flattenBalancedFactors(tree.down(1),
                            power * ILUtils.intConstantValue(tree.down(2)), positive,
                            toPFactors, toPPowers, toAFactors, toAPowers, beforeActiv,
                            symbolTable, mode);
                } else {
                    ToBool toActive = new ToBool(false);
                    Tree bTree = rebalanceExp(tree, beforeActiv, symbolTable,
                            toActive, mode);
                    if (toActive.get()) {
                        insertBalancedFactor(bTree, power, toAFactors, toAPowers);
                    } else {
                        insertBalancedFactor(bTree, power, toPFactors, toPPowers);
                    }
                }
                break;
            default: {
                ToBool toActive = new ToBool(false);
                Tree bTree = rebalanceExp(tree, beforeActiv, symbolTable, toActive,
                        mode);
                if (toActive.get()) {
                    insertBalancedFactor(bTree, power, toAFactors, toAPowers);
                } else {
                    insertBalancedFactor(bTree, power, toPFactors, toPPowers);
                }
                break;
            }
        }
    }

    private void insertBalancedFactor(Tree tree, int power,
                                      TapList<Tree> toFactors, TapIntList toPowers) {
        if (power != 0 && !ILUtils.evalsToOne(tree)) {
            /*This should be done better, like in insertBalancedTerm : */
            if (ILUtils.isExpressionNumConstant(tree) && power == 1 &&
                    toFactors.head == null) {
                toFactors.head = tree;
            } else {
                insertTimesTree(tree, power, toFactors, toPowers);
            }
        }
    }

    /**
     * Splits remaining factors into Numerator and Denominator:
     * Removes Factors that are "1".
     */
    private static void separateDenominator(TapList<Tree> toFactors,
                                            TapIntList toPowers, TapList<Tree> toDFactors, TapIntList toDPowers) {
        TapList<Tree> inFactors = toFactors;
        TapIntList inPowers = toPowers;
        TapList<Tree> inDFactors = toDFactors;
        TapIntList inDPowers = toDPowers;
        while (inFactors.tail != null) {
            if (inFactors.tail.head == null ||
                    inPowers.tail.head <= 0) {
                if (inFactors.tail.head != null &&
                        inPowers.tail.head < 0) {
                    inDFactors = inDFactors.placdl(inFactors.tail.head);
                    inDPowers = inDPowers.placdl(-inPowers.tail.head);
                }
                inFactors.tail = inFactors.tail.tail;
                inPowers.tail = inPowers.tail.tail;
            } else {
                inFactors = inFactors.tail;
                inPowers = inPowers.tail;
            }
        }
    }

    private static Tree rebuildBalancedFactorsRec(TapList<Tree> toFactors,
                                                  TapIntList toPowers, Tree denominator) {
        TapList<Tree> inFactors = toFactors;
        TapIntList toWeights = new TapIntList(-1, null);
        TapIntList inWeights = toWeights;
        int power;
        while (inFactors.tail != null) {
            power = toPowers.tail.head;
            if (power > 1) {
                inFactors.tail.head = ILUtils.build(ILLang.op_power,
                        inFactors.tail.head,
                        ILUtils.build(ILLang.op_intCst, power));
            }
            inWeights = inWeights.placdl(getCost(inFactors.tail.head));
            inFactors = inFactors.tail;
            toPowers = toPowers.tail;
        }
        if (denominator != null) {
            inFactors.tail = new TapList<>(denominator, null);
            inWeights.tail = new TapIntList(getCost(denominator), null);
        }
        TapList<Tree> toFactor1;
        TapList<Tree> toFactor2;
        TapIntList toWeight1;
        TapIntList toWeight2;
        boolean ordered;
        Tree combinedTree;
        int combinedWeight;
        if (toFactors.tail != null) {
            while (toFactors.tail.tail != null) {
                toFactor1 = null;
                toFactor2 = null;
                toWeight1 = null;
                toWeight2 = null;
                inFactors = toFactors;
                inWeights = toWeights;
                ordered = false;
                while (inFactors.tail != null) {
                    if (toWeight1 == null ||
                            inWeights.tail.head < toWeight1.tail.head) {
                        toFactor2 = toFactor1;
                        toWeight2 = toWeight1;
                        toFactor1 = inFactors;
                        toWeight1 = inWeights;
                        ordered = false;
                    } else if (toWeight2 == null ||
                            inWeights.tail.head < toWeight2.tail.head) {
                        toFactor2 = inFactors;
                        toWeight2 = inWeights;
                        ordered = true;
                    }
                    inFactors = inFactors.tail;
                    inWeights = inWeights.tail;
                }
                if (!ordered) {
                    TapList<Tree> tmp = toFactor2;
                    toFactor2 = toFactor1;
                    toFactor1 = tmp;
                    TapIntList tmpLInt = toWeight2;
                    toWeight2 = toWeight1;
                    toWeight1 = tmpLInt;
                }
                if (toFactor2.tail.head == denominator) {
                    combinedTree = ILUtils.build(ILLang.op_div,
                            toFactor1.tail.head,
                            toFactor2.tail.head);
                } else {
                    combinedTree = ILUtils.build(ILLang.op_mul,
                            toFactor1.tail.head,
                            toFactor2.tail.head);
                }
                combinedWeight = toWeight1.tail.head + toWeight2.tail.head +
                        MUL_COST;
                if (ordered) {
                    toFactor2.tail.head = combinedTree;
                    toWeight2.tail.head = combinedWeight;
                    toFactor1.tail = toFactor1.tail.tail;
                    toWeight1.tail = toWeight1.tail.tail;
                } else {
                    toFactor1.tail.head = combinedTree;
                    toWeight1.tail.head = combinedWeight;
                    toFactor2.tail = toFactor2.tail.tail;
                    toWeight2.tail = toWeight2.tail.tail;
                }
            }
        }
        if (toFactors.tail == null) {
            return null;
        } else if (toFactors.tail.head == denominator) {
            return ILUtils.build(ILLang.op_div,
                    ILUtils.build(ILLang.op_realCst, "1.0"), denominator);
        } else {
            return toFactors.tail.head;
        }
    }

    private static Tree rebuildFlatFactors(TapList<Tree> factors, TapIntList powers) {
        Tree result = null;
        while (factors != null) {
            result = rebuildMulOneFlatFactor(result, factors.head,
                    powers.head);
            factors = factors.tail;
            powers = powers.tail;
        }
        return result;
    }

    private static Tree rebuildMulOneFlatFactor(Tree left, Tree right,
                                                int rightPower) {
        if (rightPower > 1) {
            right = ILUtils.build(ILLang.op_power, right,
                    ILUtils.build(ILLang.op_intCst, rightPower));
        }
        if (left == null) {
            return right;
        } else {
            return ILUtils.build(ILLang.op_mul, left, right);
        }
    }

    private boolean setSubExpsActive(Tree expr) {
        if (expr != null) {
            switch (expr.opCode()) {
                case ILLang.op_add:
                case ILLang.op_sub:
                case ILLang.op_mul:
                case ILLang.op_div:
                case ILLang.op_power: {
                    boolean active1, active2;
                    active1 = setSubExpsActive(expr.down(1));
                    active2 = setSubExpsActive(expr.down(2));
                    // Copy the activity annotation:
                    if (active1 || active2) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), expr);
                    }
                    return active1 || active2;
                }

                case ILLang.op_minus: {
                    boolean active1;
                    active1 = setSubExpsActive(expr.down(1));
                    // Copy the activity annotation:
                    if (active1) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), expr);
                    }
                    return active1;
                }
                case ILLang.op_realCst:
                case ILLang.op_intCst:
                case ILLang.op_boolCst:
                case ILLang.op_bitCst:
                case ILLang.op_stringCst:
                case ILLang.op_strings:
                case ILLang.op_concat:
                case ILLang.op_none:
                case ILLang.op_lt:
                case ILLang.op_le:
                case ILLang.op_gt:
                case ILLang.op_ge:
                case ILLang.op_eq:
                case ILLang.op_neq:
                case ILLang.op_and:
                case ILLang.op_or:
                case ILLang.op_xor:
                case ILLang.op_bitAnd:
                case ILLang.op_bitOr:
                case ILLang.op_bitXor:
                case ILLang.op_rightShift:
                case ILLang.op_leftShift:
                case ILLang.op_incr:
                case ILLang.op_decr:
                case ILLang.op_not:
                case ILLang.op_bitNot:
                    // For constants: they are not active:
                    return false;
                default:
                    // The default case is for op_ident, op_arrayAccess etc...
                    // return true iff they have the active annotation on.
                    return ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), expr, adEnv.curSymbolTable());
            }
        } else {
            return false;
        }
    }

    private void insertTimesTree(Tree tree, int times,
                                 TapList<Tree> toTrees, TapIntList toTimes) {
        while (toTrees.tail != null &&
                !ILUtils.equalValues(toTrees.tail.head, tree, null, null)) {
            toTrees = toTrees.tail;
            toTimes = toTimes.tail;
        }
        if (toTrees.tail == null) {
            toTrees.tail = new TapList<>(tree, null);
            toTimes.tail = new TapIntList(0, null);
        }
        toTimes.tail.head = toTimes.tail.head + times;
    }

//-------------------------------- DIFF OPTIMAL CUTS:

    /**
     * A DiffCutSupport object is attached to an expression Tree (call it "T")
     * as an annotation named "CutCosts", to express the cost/benefit
     * of cutting this expression (or one of its derivatives) from its enclosing tree,
     * with respect to the complexity of derivatives computations.
     * For each of {the primal "T", its tangent diff "Td", its adjoint diff "Tb"},
     * a CutCost object contains {the cost of computing it, and the number of times it is used}.
     */
    private final class DiffCutSupport {
        /**
         * Execution cost of the current primal expression.
         */
        private int primalCost;
        /**
         * Number of times the current primal expression is needed in the diff code.
         */
        private int primalTimes;
        /**
         * Decision whether to split the current primal expression in a temporary variable.
         */
        private boolean cutPrimal;

        /**
         * Execution cost of the adjoint expression of the current expression.
         */
        private int adjCost;
        /**
         * Number of times the adjoint of current expression (which comes from
         * the *outside* use context) is needed in the diff code.
         */
        private int adjTimes;
        /**
         * Number of dimensions of the temporary variable needed if we cut here.
         */
        private int nbDimensions;
        /**
         * Decision whether to split the adjoint of current expression in a temporary variable.
         */
        private boolean cutAdj;

        private boolean isActive;

        // Special precomputations for op_power:
        private int dCostUp;
        private int dCostDiff1;
        private int dCostDiff2;

        // Memos of objects useful during actual differentiation in buildPartials():
        private NewSymbolHolder tmpVarHolder;
        private Tree tmpVar;
        private boolean tmpPrimalVarInitialized;
        private Tree[] tmpAdjVarR;  //one per replica

        private DiffCutSupport(Tree tree) {
            isActive = adEnv.activeRootCalledFromContext
                    || ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable())
                    || TapIntList.intersects(
                               ZoneInfo.listAllZones(
                                         adEnv.curSymbolTable().treeOfZonesOfValue(
                                                 tree, null, adEnv.curInstruction(), null),
                                         true),
                               adEnv.toActiveTmpZones.tail);
            switch (tree.opCode()) {
                case ILLang.op_add:
                case ILLang.op_sub:
                    dCostUp = ADD_COST;
                    break;
                case ILLang.op_mul:
                    dCostUp = MUL_COST;
                    break;
                case ILLang.op_div:
                    dCostUp = DIV_COST;
                    break;
                case ILLang.op_power:
                    if (ILUtils.isExpressionIntConstant(tree.down(2))) {
                        int intPower = ILUtils.intConstantValue(tree.down(2));
                        // The cost of a**2 should be only the cost of one multiplication!
                        if (intPower < 0) {
                            dCostUp = (-1 - intPower) * MUL_COST + DIV_COST;
                            dCostDiff1 = (1 - intPower) * MUL_COST + DIV_COST;
                        } else {
                            dCostUp = (intPower - 1) * MUL_COST;
                            dCostDiff1 = intPower * MUL_COST;
                        }
                        dCostDiff2 = 0;
                        if (dCostUp > POW_COST) {
                            dCostUp = POW_COST;
                        }
                        if (dCostDiff1 > 2 * MUL_COST + POW_COST + ADD_COST) {
                            dCostDiff1 = 2 * MUL_COST + POW_COST + ADD_COST;
                        }
                    } else {
                        dCostUp = POW_COST;
                        dCostDiff1 = 2 * MUL_COST + POW_COST + ADD_COST;
                        dCostDiff2 = 2 * MUL_COST + CALL_COST;
                    }
                    break;
                case ILLang.op_arrayConstructor:
                case ILLang.op_iterativeVariableRef:
                case ILLang.op_constructorCall:
                case ILLang.op_complexConstructor:
                    dCostUp = 1;
                    break;
                case ILLang.op_ifExpression:
                    dCostUp = 1;
                    break;
                default:
                    break;
            }
        }

        private int bestCutProfit() {
            int profit = (primalTimes - 1) * primalCost - (primalTimes + 1) * VAR_COST;
            int p3 = (adjTimes - 1) * adjCost - (adjTimes + 1) * VAR_COST;
            if (p3 > profit) {
                profit = p3;
            }
            // Arbitrary: we evaluate the extra cost of introducing a temporary array
            // versus a temporary scalar as 3 times the number of dimensions.
            return profit - NEW_TEMP_VAR_COST - 3 * nbDimensions;
        }

        /**
         * @return true when the choice is to cut in the primal expression.
         */
        private boolean doBestCut() {
            int p1 = (primalTimes - 1) * primalCost - (primalTimes + 1) * VAR_COST;
            int p3 = (adjTimes - 1) * adjCost - (adjTimes + 1) * VAR_COST;
            if (p1 > p3) {
                cutPrimal = true;
                primalCost = VAR_COST;
                primalTimes = 1;
                return true;
            } else {
                cutAdj = true;
                adjCost = VAR_COST;
                adjTimes = 1;
                return false;
            }
        }

        @Override
        public String toString() {
            return "{"+(cutPrimal?" ! ":"")+primalTimes + "*" + primalCost + (isActive ? " active" : " passive")
                    + (tangentDiff ? "" : " adj:" + adjTimes + "*" + adjCost)+"}";
        }
    }

    /**
     * @param dimensions The list of dimensions of the given tree's type.
     * @return true when one of the cuts is a cut in the primal expression.
     */
    private boolean findDiffOptimalCuts(Tree tree, TapList<ArrayDim> dimensions) {
        if (adEnv.traceCurBlock == 2) {
            TapEnv.printlnOnTrace("ANALYZING " + (tangentDiff ? "TANGENT" : "ADJOINT") + " CUTS IN " + tree + ':');
        }
        DiffCutSupport topCutCosts = getSetCutCosts(tree);
        computeBottomUpCosts(tree, topCutCosts, true);
        TapList<Tree> mostProfitableCut, treesAbove;
        boolean cutInPrimal = false;
        if (TapEnv.get().splitDiffExpressions) {
            while (true) {
                topCutCosts.primalTimes = tangentDiff ? 1 : 0;
//                 topCutCosts.tgtTimes = (tangentDiff&&topCutCosts.isActive ? 1 : 0) ;
                topCutCosts.adjCost = VAR_COST;
                computeTopDownCosts(tree, topCutCosts, dimensions);
                if (adEnv.traceCurBlock == 2) { // Prints useful info only if ILUtils.toStringRec prints the cutCostsString:
                    TapEnv.printlnOnTrace("    TREE WITH CUT SUPPORT: " + ILUtils.toString(tree, adEnv.curActivity()));
                }
                mostProfitableCut = mostProfitableCut(tree, null, null);
                if (mostProfitableCut == null) {
                    break;
                }
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("    -> CUT " + showCutDecision(getCutCosts(mostProfitableCut.head)) + "  OF  " + ILUtils.toString(mostProfitableCut.head));
                }
                boolean cutPrimal = getCutCosts(mostProfitableCut.head).doBestCut();
                if (cutPrimal) {
                    cutInPrimal = true;
                }
                treesAbove = mostProfitableCut.tail;
                while (treesAbove != null) {
                    computeBottomUpCosts(treesAbove.head, getSetCutCosts(treesAbove.head), false);
                    treesAbove = treesAbove.tail;
                }
            }
        }
        return cutInPrimal;
    }

    private String showCutDecision(DiffCutSupport cc) {
        int p1 = (cc.primalTimes - 1) * cc.primalCost - (cc.primalTimes + 1) * VAR_COST;
        int p3 = (cc.adjTimes - 1) * cc.adjCost - (cc.adjTimes + 1) * VAR_COST;
        if (p1 > p3) {
            return "PRIMAL";
        } else {
            return "ADJOINT";
        }
    }

    /**
     * Computes the number of times the adjoint of the given tree is reevaluated,
     * and the cost of (re)evaluating the primal given tree.
     * bottom-up inside "tree".
     *
     * @param tree       The expression on which to recompute, and recursively on all its sub-expressions.
     * @param cc         The cost information for "tree".
     */
    private void computeBottomUpCosts(Tree tree, DiffCutSupport cc, boolean recurse) {
        DiffCutSupport cc1, cc2;
        int childrenCost ;
        switch (tree.opCode()) {
            case ILLang.op_add:
            case ILLang.op_sub:
                // a' += r' ; b' += r' <-- r = a+b --> r' = a'+b'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                    computeBottomUpCosts(tree.down(2), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                childrenCost = cc1.primalCost + cc2.primalCost ;
                cc.primalCost = cc.cutPrimal ? VAR_COST : (childrenCost==0 ? CST_COST : childrenCost + ADD_COST) ;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : cc1.tgtCost + cc2.tgtCost + ADD_COST) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            case ILLang.op_mul:
            case ILLang.op_mod: {
                // a' += b*r' ; b' += a*r' <-- r = a*b --> r' = a*b' + b*a'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                    computeBottomUpCosts(tree.down(2), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                childrenCost = cc1.primalCost + cc2.primalCost ;
                cc.primalCost = cc.cutPrimal ? VAR_COST : (childrenCost==0 ? CST_COST : childrenCost + MUL_COST) ;
//             int leftCoeffCost = (cc1.tgtCost==0 ? 0 : cc1.tgtCost + cc2.primalCost + MUL_COST) ;
//             int rightCoeffCost = (cc2.tgtCost==0 ? 0 : cc2.tgtCost + cc1.primalCost + MUL_COST) ;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : leftCoeffCost+rightCoeffCost+ADD_COST) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            }
            case ILLang.op_div: {
                // a' += r'/b ; b' -= a*{r'/b}/b <-- r = a/b --> r' = (a' - b'*{r})/b
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                    computeBottomUpCosts(tree.down(2), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                childrenCost = cc1.primalCost + cc2.primalCost ;
                cc.primalCost = cc.cutPrimal ? VAR_COST : (childrenCost==0 ? CST_COST : childrenCost + DIV_COST) ;
//             int leftCoeffCost = (cc1.tgtCost==0 ? 0 : cc1.tgtCost) ;
//             int rightCoeffCost = (cc2.tgtCost==0 ? 0 : cc2.tgtCost + cc.primalCost + MUL_COST) ;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : leftCoeffCost+rightCoeffCost+cc2.primalCost+ADD_COST+DIV_COST) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            }
            case ILLang.op_power:
                // a' += b*a^(b-1)*r' ; b' += Ln(a)*a^b*r' <-- r = a^b --> r' = b*a^(b-1)*a' + Ln(a)*{r}*b'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                    computeBottomUpCosts(tree.down(2), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                childrenCost = cc1.primalCost + cc2.primalCost ;
                cc.primalCost = cc.cutPrimal ? VAR_COST : (childrenCost==0 ? CST_COST : childrenCost + cc.dCostUp) ;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : cc1.tgtCost + cc2.tgtCost + 2*cc1.primalCost + 2*cc2.primalCost + cc.primalCost + cc.dCostDiff1 + cc.dCostDiff2) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            case ILLang.op_complexConstructor:
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                    computeBottomUpCosts(tree.down(2), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                childrenCost = cc1.primalCost + cc2.primalCost ;
                cc.primalCost = cc.cutPrimal ? VAR_COST : childrenCost;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : cc1.tgtCost + cc2.tgtCost) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            case ILLang.op_ifExpression:
                cc1 = getSetCutCosts(tree.down(2));
                cc2 = getSetCutCosts(tree.down(3));
                if (recurse) {
                    computeBottomUpCosts(tree.down(2), cc1, true);
                    computeBottomUpCosts(tree.down(3), cc2, true);
                }
                cc.isActive = cc1.isActive || cc2.isActive;
                cc.primalCost = cc.cutPrimal ? VAR_COST : cc1.primalCost + cc2.primalCost + 1;
//             cc.tgtCost = (cc.cutTgt ? VAR_COST : cc1.tgtCost + cc2.tgtCost + 1) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes + cc2.adjTimes;
                break;
            case ILLang.op_minus:
                cc1 = getSetCutCosts(tree.down(1));
                if (recurse) {
                    computeBottomUpCosts(tree.down(1), cc1, true);
                }
                cc.isActive = cc1.isActive;
                cc.primalCost = cc.cutPrimal ? VAR_COST : cc1.primalCost;
//             cc.tgtCost = (cc.cutTgt?VAR_COST:cc1.tgtCost) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes;
                break;
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                cc1 = getSetCutCosts(tree.down(2));
                if (recurse) {
                    computeBottomUpCosts(tree.down(2), cc1, true);
                }
                cc.isActive = cc1.isActive;
                cc.primalCost = cc.cutPrimal ? VAR_COST : cc1.primalCost;
//             cc.tgtCost = (cc.cutTgt?VAR_COST:cc1.tgtCost) ;
                cc.adjTimes = tangentDiff ? 0 : cc.cutAdj ? 1 : cc1.adjTimes;
                break;
            case ILLang.op_call: {
                cc.primalCost = cc.cutPrimal ? VAR_COST : CALL_COST;
//             cc.tgtCost = (cc.cutTgt?VAR_COST:CALL_COST) ;
                cc.adjTimes = cc.cutAdj && !tangentDiff ? 1 : 0;
                Tree[] args = ILUtils.getArguments(tree).children();
                for (int i = args.length - 1; i >= 0; --i) {
                    cc1 = getSetCutCosts(args[i]);
                    if (recurse) {
                        computeBottomUpCosts(args[i], cc1, true);
                    }
                    if (cc1.isActive) {
                        cc.isActive = true;
                    }
                    if (!cc.cutPrimal) {
                        cc.primalCost += cc1.primalCost;
                    }
//                 if (!cc.cutTgt) cc.tgtCost += cc1.tgtCost;
                    if (!cc.cutAdj && !tangentDiff) {
                        cc.adjTimes += cc1.adjTimes;
                    }
                }
                break;
            }
            case ILLang.op_arrayConstructor:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_constructorCall: {
                Tree[] args;
                if (tree.opCode() == ILLang.op_iterativeVariableRef) {
                    args = tree.down(1).children();
                } else if (tree.opCode() == ILLang.op_constructorCall) {
                    args = tree.down(3).children();
                } else {
                    args = tree.children();
                }
                cc.primalCost = cc.cutPrimal ? args.length * VAR_COST : CALL_COST;
//             cc.tgtCost = (cc.cutTgt?VAR_COST:CALL_COST) ;
                cc.adjTimes = cc.cutAdj && !tangentDiff ? 1 : 0;
                cc.isActive = false;
                for (int i = args.length - 1; i >= 0; --i) {
                    cc1 = getSetCutCosts(args[i]);
                    if (recurse) {
                        computeBottomUpCosts(args[i], cc1, true);
                    }
                    if (cc1.isActive) {
                        cc.isActive = true;
                    }
                    if (!cc.cutPrimal) {
                        cc.primalCost += cc1.primalCost;
                    }
//                 if (!cc.cutTgt) cc.tgtCost += cc1.tgtCost;
                    if (!cc.cutAdj && !tangentDiff) {
                        cc.adjTimes += cc1.adjTimes;
                    }
                }
                break;
            }
            case ILLang.op_ident:
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
                cc.primalCost = cc.cutPrimal ? VAR_COST : getCost(tree);
//             cc.tgtCost = (!cc.isActive ? 0 : (cc.cutPrimal?VAR_COST:getCost(tree))) ;
                cc.adjTimes = tangentDiff || !cc.isActive ? 0 : 1;
                break;
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_concat:
            case ILLang.op_none:
                // Comparators
            case ILLang.op_lt:
            case ILLang.op_le:
            case ILLang.op_gt:
            case ILLang.op_ge:
            case ILLang.op_eq:
            case ILLang.op_neq:
                // Logical OR Bitwise
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_rightShift:
            case ILLang.op_leftShift:
                // Unary ops
                //  Arithmetic
            case ILLang.op_incr:
            case ILLang.op_decr:
                // Logical OR bitwise
            case ILLang.op_not:
                // other...
            case ILLang.op_nameEq:
            case ILLang.op_sizeof:
            case ILLang.op_allocate:
                cc.isActive = false;
                break;
            default:
                TapEnv.toolWarning(-1, "(Optimal cuts in reverse mode, bottom up) Unexpected operator: " + tree.opName());
                cc.isActive = false;
                break;
        }
    }

    /**
     * Computes the number of times a primal sub-expression is reevaluated,
     * and the cost of (re)evaluating the adjoint arriving on this sub-expression,
     * top-down for each sub-expression of "tree".
     *
     * @param tree       The expression on which to recompute, and recursively on all its sub-expressions.
     * @param cc         The cost information for "tree".
     * @param dimensions The list of dimensions of the adjoint expression arriving from above.
     */
    private void computeTopDownCosts(Tree tree, DiffCutSupport cc, TapList<ArrayDim> dimensions) {
        DiffCutSupport cc1, cc2;
        // If decision was taken to precompute the arriving adjoint into a temp variable,
        // then the cost of evaluating it becomes the cost of evaluating the temp variable:
        if (cc.cutAdj) {
            cc.adjCost = VAR_COST;
        }
        if (dimensions != null) {
            TapList<ArrayDim> treeDims = adEnv.curSymbolTable().typeOf(tree).getAllDimensions();
            // If tree has fewer dimensions than arriving adjoint expression,
            // then there will be a reduction here on the adjoint coming from above.
            if (TapList.length(dimensions) > TapList.length(treeDims)) {
                if (!cc.cutAdj) {
                    cc.adjCost += REDUC_COST;
                }
                dimensions = treeDims;
            }
        }
        switch (tree.opCode()) {
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mod:
            case ILLang.op_complexConstructor:
                // a' += r' ; b' += r' <-- r = a+b --> r' = a'+b'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes;
                cc2.primalTimes = cc2.cutPrimal ? 1 : cc.primalTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
//             cc2.tgtTimes = (!cc2.isActive ? 0 : (cc2.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost;
                cc2.adjCost = cc.adjCost;
                computeTopDownCosts(tree.down(1), cc1, dimensions);
                computeTopDownCosts(tree.down(2), cc2, dimensions);
                break;
            case ILLang.op_ifExpression:
                // if (t) {a' += r'} else {b' += r'} <-- r = (t ? a : b) --> r' = (t ? a' : b')
                cc1 = getSetCutCosts(tree.down(2));
                cc2 = getSetCutCosts(tree.down(3));
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes;
                cc2.primalTimes = cc2.cutPrimal ? 1 : cc.primalTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
//             cc2.tgtTimes = (!cc2.isActive ? 0 : (cc2.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost;
                cc2.adjCost = cc.adjCost;
                computeTopDownCosts(tree.down(2), cc1, dimensions);
                computeTopDownCosts(tree.down(3), cc2, dimensions);
                break;
            case ILLang.op_mul:
                // a' += b*r' ; b' += a*r' <-- r = a*b --> r' = a*b' + b*a'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && cc2.isActive ? replicas : 0) + cc2.adjTimes;
                cc2.primalTimes = cc2.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && cc1.isActive ? replicas : 0) + cc1.adjTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes));
//             cc2.tgtTimes = (!cc2.isActive ? 0 : (cc2.cutTgt ? 1 : cc.tgtTimes));
                cc1.adjCost = cc.adjCost + cc2.primalCost + MUL_COST;
                cc2.adjCost = cc.adjCost + cc1.primalCost + MUL_COST;
                computeTopDownCosts(tree.down(1), cc1, dimensions);
                computeTopDownCosts(tree.down(2), cc2, dimensions);
                break;
            case ILLang.op_div:
                // a' += r'/b ; b' -= a*{r'/b}/b <-- r = a/b --> r' = (a' - b'*{r})/b
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                // Special case: to encourage the precomputation of a/b rather than a or b,
                // we move a part of the "primalTimes" value from cost1 and cost2 back into the
                // "primalTimes" value of this CutCost. The reason is because the derivative of
                // a/b wrt b is -a/b^2, and therefore it is better to reuse a/b itself
                // than separately memorize a and b and then recompute a/b. If we do this way, then
                // the "rest" of the derivative wrt b is -1/b, which is the same as the derivative
                // wrt a (except sign). Therefore, the derivative coming from above should be
                // multiplied with 1/b, and therefore this division can also be put in factor.
                cc.primalTimes = cc.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && cc2.isActive ? replicas : 0) + cc2.adjTimes;
                cc.adjCost += cc.cutAdj ? 0 : cc2.primalCost + DIV_COST;
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes;
                cc2.primalTimes = cc2.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && cc1.isActive ? replicas : 0) + cc.adjTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
//             cc2.tgtTimes = (!cc2.isActive ? 0 : (cc2.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost;
                cc2.adjCost = cc.adjCost + cc.primalCost + 2 * MUL_COST;
                computeTopDownCosts(tree.down(1), cc1, dimensions);
                computeTopDownCosts(tree.down(2), cc2, dimensions);
                break;
            case ILLang.op_power: {
                // a' += b*a^(b-1)*r' ; b' += Ln(a)*a^b*r' <-- r = a^b --> r' = b*a^(b-1)*a' + Ln(a)*{r}*b'
                cc1 = getSetCutCosts(tree.down(1));
                cc2 = getSetCutCosts(tree.down(2));
                // Special case: to encourage the precomputation of a^b rather than a or b,
                // we move a part of the "times" value from cost1 and cost2 back into the
                // "times" value of this CutCost. The reason is because the derivative of
                // a^b wrt b is Ln(a)*a^b, and therefore it is better to reuse a^b itself
                // than separately memorize a and b and then recompute a^b.
                cc.primalTimes = cc.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && cc2.isActive ? replicas : 0) + cc2.adjTimes;
                boolean isComplex = adEnv.curSymbolTable().typeOf(tree).isComplexBase() ;
                // protectionA against: (a<=0 && (b==0 || b!=INT(b))) : costs 1 use of a and 3 of b
                boolean needsProtectionA = (!isComplex && cc1.isActive && !ILUtils.isExpressionNumConstant(tree.down(2))) ;
                // protectionB against: (a<=0): costs 1 use of a
                boolean needsProtectionB = (!isComplex && cc2.isActive && !ILUtils.evalsToGTZero(tree.down(1))) ;
                cc1.primalTimes = (cc1.cutPrimal ? 1 : (cc.primalTimes + cc1.adjTimes + cc2.adjTimes
                                                        + (tangentDiff && cc2.isActive ? replicas : 0) 
                                                        + (tangentDiff && cc1.isActive ? replicas : 0) 
                                                        + (needsProtectionA?1:0) + (needsProtectionB?1:0)));
                cc2.primalTimes = (cc2.cutPrimal ? 1 : (cc.primalTimes + 2 * cc1.adjTimes
                                                        + (tangentDiff && cc1.isActive ? replicas : 0)
                                                        + (needsProtectionA?2:0))) ;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
//             cc2.tgtTimes = (!cc2.isActive ? 0 : (cc2.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost + cc1.primalCost + 2 * cc2.primalCost + cc.dCostDiff1;
                cc2.adjCost = cc.adjCost + cc1.primalCost + cc.primalCost + cc.dCostDiff2;
                computeTopDownCosts(tree.down(1), cc1, dimensions);
                computeTopDownCosts(tree.down(2), cc2, dimensions);
                break;
            }
            case ILLang.op_minus:
                cc1 = getSetCutCosts(tree.down(1));
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost;
                computeTopDownCosts(tree.down(1), cc1, dimensions);
                break;
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                cc1 = getSetCutCosts(tree.down(2));
                cc1.primalTimes = cc1.cutPrimal ? 1 : cc.primalTimes;
//             cc1.tgtTimes = (!cc1.isActive ? 0 : (cc1.cutTgt ? 1 : cc.tgtTimes)) ;
                cc1.adjCost = cc.adjCost;
                computeTopDownCosts(tree.down(2), cc1, dimensions);
                break;
            case ILLang.op_call: {
                Tree intrinsicName = ILUtils.getCalledName(tree);
                TapList<DiffCutSupport> argCosts = null;
                DiffCutSupport costArg;
                Tree[] args = ILUtils.getArguments(tree).children();
                for (int i = args.length - 1; i >= 0; i--) {
                    argCosts = new TapList<>(getSetCutCosts(args[i]), argCosts);
                }
                if (ILUtils.isIdentOf(intrinsicName,
                        new String[]{"sqrt", "dsqrt", "csqrt"}, false)) {
                    costArg = argCosts.head;
                    // Special case: to encourage the precomputation of temp=sqrt(a) rather than a,
                    // to reuse it into its jacobian 1/(2*temp) instead of 1/(2*sqrt(a))
                    cc.primalTimes += costArg.adjTimes + (tangentDiff && costArg.isActive ? replicas : 0);
                    // TODO: count the cost of the protection against argument a==0
                    costArg.primalTimes = costArg.cutPrimal ? 1 : cc.primalTimes;
//                 costArg.tgtTimes = (!costArg.isActive ? 0 : (costArg.cutTgt ? 1 : cc.tgtTimes)) ;
                    costArg.adjCost = cc.adjCost + cc.primalCost + DIV_COST;
                    //TODO: missing case of asin, acos or similar.
                } else if (adEnv.curUnit().isFortran9x() && ILUtils.isIdentOf(intrinsicName, new String[]{"spread"}, false)
                           ||
                           (adEnv.curUnit().isFortran()
                            ? ILUtils.isIdentOf(intrinsicName, new String[]{"sngl", "dble", "real", "dreal", "aimag", "dimag", "cmplx", "dcmplx", "complex", "sum"}, false)
                            : ILUtils.isIdentOf(intrinsicName, new String[]{"creal", "cimag"}, true))) {
                    // "Linear" intrinsics, whose derivative involve only the differentiated argument(s) E.g.:
                    //  a' += REAL(r') ; b' += AIMAG(r')   <-- r = CMPLX(a,b) -->  r' = cmplx(a',b')
                    TapList<DiffCutSupport> inCostArgs = argCosts;
                    while (inCostArgs != null) {
                        costArg = inCostArgs.head;
                        costArg.primalTimes = costArg.cutPrimal ? 1 : cc.primalTimes;
                        costArg.adjCost = cc.adjCost + 1;
                        inCostArgs = inCostArgs.tail;
                    }
                } else { // case of any other (intrinsic) call: we assume diff call will need primal and differentiated arguments:
                    TapList<DiffCutSupport> inCostArgs = argCosts;
                    int argTimes = 0;
                    int argCostDiff = CALL_COST;
                    while (inCostArgs != null) {
                        costArg = inCostArgs.head;
                        argTimes += costArg.adjTimes;
                        argCostDiff += costArg.primalCost;
                        inCostArgs = inCostArgs.tail;
                    }
                    inCostArgs = argCosts;
                    while (inCostArgs != null) {
                        costArg = inCostArgs.head;
                        costArg.primalTimes = costArg.cutPrimal ? 1 : cc.primalTimes + (tangentDiff && costArg.isActive ? replicas : 0) + argTimes;
//                     costArg.tgtTimes = (!costArg.isActive ? 0 : (costArg.cutTgt ? 1 : cc.tgtTimes)) ;
                        costArg.adjCost = cc.adjCost + argCostDiff;
                        inCostArgs = inCostArgs.tail;
                    }
                }
                for (Tree arg : args) {
                    computeTopDownCosts(arg, argCosts.head,
                            adEnv.curSymbolTable().typeOf(arg).getAllDimensions());
                    argCosts = argCosts.tail;
                }
                break;
            }
            case ILLang.op_arrayConstructor:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_constructorCall: {
                Tree[] args;
                if (tree.opCode() == ILLang.op_iterativeVariableRef) {
                    args = tree.down(1).children();
                } else if (tree.opCode() == ILLang.op_constructorCall) {
                    args = tree.down(3).children();
                } else {
                    args = tree.children();
                }
                TapList<DiffCutSupport> argCosts = null;
                for (int i = args.length - 1; i >= 0; i--) {
                    argCosts = new TapList<>(getSetCutCosts(args[i]), argCosts);
                }
                TapList<DiffCutSupport> inCostArgs = argCosts;
                DiffCutSupport costArg;
                while (inCostArgs != null) {
                    costArg = inCostArgs.head;
                    costArg.primalTimes = costArg.cutPrimal ? 1 : cc.primalTimes;
//                 costArg.tgtTimes = (!costArg.isActive ? 0 : (costArg.cutTgt ? 1 : cc.tgtTimes)) ;
                    costArg.adjCost = cc.adjCost;
                    inCostArgs = inCostArgs.tail;
                }
                for (Tree arg : args) {
                    computeTopDownCosts(arg, argCosts.head, null);
                    argCosts = argCosts.tail;
                }
                break;
            }
            case ILLang.op_ident:
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_concat:
            case ILLang.op_none:
                // Comparators
            case ILLang.op_lt:
            case ILLang.op_le:
            case ILLang.op_gt:
            case ILLang.op_ge:
            case ILLang.op_eq:
            case ILLang.op_neq:
                // Logical OR Bitwise
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_rightShift:
            case ILLang.op_leftShift:
                // Unary ops
                //  Arithmetic
            case ILLang.op_incr:
            case ILLang.op_decr:
                // Logical OR bitwise
            case ILLang.op_not:
                // other...
            case ILLang.op_nameEq:
            case ILLang.op_sizeof:
            case ILLang.op_allocate:
                break;
            default:
                TapEnv.toolWarning(-1, "(Optimal cuts in reverse mode, top down) Unexpected operator: " + tree.opName());
                break;
        }
    }

    /**
     * @return a list of sub-trees of "tree",
     * whose first element is the sub-tree which is detected as the most profitable cut,
     * and the tail elements are the sub-trees that are parents of the first sub-tree, ordered bottom-up.
     * These tail elements are the sub-trees whose DiffCutSupport must be recomputed after the cut is done.
     * (notice that every sub-tree of the first sub-tree must also have their DiffCutSupport recomputed)
     */
    private TapList<Tree> mostProfitableCut(Tree tree, TapList<Tree> curPath, TapList<Tree> bestPath) {
        DiffCutSupport cutCosts = getCutCosts(tree);
        if (cutCosts != null) {
            curPath = new TapList<>(tree, curPath);
            int curCutProfit = cutCosts.bestCutProfit();
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CUTPROFIT FOR " + tree + " PRIMAL:" + cutCosts.primalTimes + "*" + cutCosts.primalCost + " ADJ:" + cutCosts.adjTimes + "*" + cutCosts.adjCost + " ===> " + curCutProfit);
            }
            if (curCutProfit > 0
                    && (bestPath == null ||
                    curCutProfit > getCutCosts(bestPath.head).bestCutProfit())) {
                bestPath = curPath;
            }
        }
        if (!tree.isAtom()) {
            Tree[] subTrees = tree.children();
            for (int i = subTrees.length - 1; i >= 0; --i) {
                bestPath = mostProfitableCut(subTrees[i], curPath, bestPath);
            }
        }
        return bestPath;
    }

    /**
     * Run-time cost of expression.
     *
     * @param expression expression to split if cost &gt; 30.
     * @return run-time cost.
     */
    protected static int getCost(Tree expression) {
        switch (expression.opCode()) {
            case ILLang.op_assign:
                return getCost(expression.down(1)) + ASS_COST +
                        getCost(expression.down(2));
            case ILLang.op_add:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift: {
                int cost1 = getCost(expression.down(1)) ;
                int cost2 = getCost(expression.down(2)) ;
                return (cost1==0 && cost2==0 ? 0 : cost1+cost2+ADD_COST) ;
            }
            case ILLang.op_sub: {
                int cost1 = getCost(expression.down(1)) ;
                int cost2 = getCost(expression.down(2)) ;
                return (cost1==0 && cost2==0 ? 0 : cost1+cost2+SUB_COST) ;
            }
            case ILLang.op_concat:
            case ILLang.op_mul:
            case ILLang.op_mod: {
                int cost1 = getCost(expression.down(1)) ;
                int cost2 = getCost(expression.down(2)) ;
                return (cost1==0 && cost2==0 ? 0 : cost1+cost2+MUL_COST) ;
            }
            case ILLang.op_div: {
                int cost1 = getCost(expression.down(1)) ;
                int cost2 = getCost(expression.down(2)) ;
                return (cost1==0 && cost2==0 ? 0 : cost1+cost2+DIV_COST) ;
            }
            case ILLang.op_minus:
            case ILLang.op_sizeof: {
                int cost1 = getCost(expression.down(1)) ;
                return (cost1==0 ? 0 : cost1+SUB_COST) ;
            }
            case ILLang.op_power: {
                int intPowerCost = POW_COST;
                if (ILUtils.isExpressionIntConstant(expression.down(2))) {
                    intPowerCost = ILUtils.intConstantValue(expression.down(2));
                    if (intPowerCost <= 0) {
                        intPowerCost = -intPowerCost + 1;
                    } else {
                        intPowerCost--;
                    }
                }
                if (intPowerCost * MUL_COST <= POW_COST) {
                    // Give a**2 the cost of a*a:
                    return getCost(expression.down(1)) + intPowerCost * MUL_COST;
                } else {
                    return getCost(expression.down(1)) + POW_COST +
                            getCost(expression.down(2));
                }
            }
            case ILLang.op_call: {
                int paramsCost = 0;
                Tree[] params = ILUtils.getArguments(expression).children();
                for (int i = params.length - 1; i >= 0; --i) {
                    paramsCost += getCost(params[i]);
                }
                return paramsCost + CALL_COST;
            }
            case ILLang.op_ifExpression:
                return getCost(expression.down(2))
                        + getCost(expression.down(3))
                        + DIV_COST;
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
                return 100 * CALL_COST;
            case ILLang.op_fieldAccess:
            case ILLang.op_address:
                return getCost(expression.down(1));
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                return getCost(expression.down(2));
            case ILLang.op_ident:
                return VAR_COST;
            case ILLang.op_arrayTriplet:
                return getCost(expression.down(1)) + getCost(expression.down(2)); // better + 1 ?
            case ILLang.op_pointerAccess:
                return getCost(expression.down(1)) + 1 + getCost(expression.down(2));
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_arrayAccess:
            case ILLang.op_arrayConstructor: {
                int finalCost = 0 ;
                Tree[] subTrees ;
                if (expression.opCode()==ILLang.op_iterativeVariableRef) {
                    subTrees = expression.down(1).children();
                } else if (expression.opCode()==ILLang.op_arrayAccess) {
                    subTrees = expression.down(2).children();
                    finalCost = getCost(expression.down(1)) /*not sure?:*/+subTrees.length ;
                } else { //ILLang.op_arrayConstructor
                    subTrees = expression.children();
                }
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    finalCost += getCost(subTrees[i]);
                }
                return finalCost ;
            }
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
                return CST_COST;
            case ILLang.op_label:
            case ILLang.op_nameEq:
            case ILLang.op_none:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return 0;
            default:
                TapEnv.toolWarning(-1, "(Get cost of expression) Unexpected operator: " + expression.opName());
                return 0;
        }
    }

    private DiffCutSupport getSetCutCosts(Tree tree) {
        DiffCutSupport cutCosts = tree.getAnnotation("CutCosts");
        if (cutCosts == null) {
            cutCosts = new DiffCutSupport(tree);
            TypeSpec treeType = adEnv.curSymbolTable().typeOf(tree);
            cutCosts.nbDimensions = treeType == null ? 0 : TapList.length(treeType.getAllDimensions());
            tree.setAnnotation("CutCosts", cutCosts);
        }
        return cutCosts;
    }

    private static DiffCutSupport getCutCosts(Tree tree) {
        return tree.getAnnotation("CutCosts");
    }

    // public only for debugging through ILUtils.toString(tree)
    public static String cutCostsString(Tree tree) {
        DiffCutSupport diffCutSupport = getCutCosts(tree);
        return diffCutSupport == null ? "" : diffCutSupport.toString();
    }

//------------------------ LINEARIZATION OF ARITHMETIC EXPRESSIONS:

    /**
     * Abstract class for all nodes of trees of vectorial masks.
     */
    private abstract static class MaskTree {
        private int nbDims;

        protected MaskTree(int nbDims) {
            this.nbDims = nbDims;
        }

        /**
         * @return the InstructionMask that implements this tree of masks.
         * If possible, return an existing InstructionMask, otherwise
         * build a new one, containing a new mask expression Tree.
         */
        protected abstract InstructionMask rebuildInstructionMask();

        /**
         * Similar to rebuildInstructionMask, but returns (a copy of) the mask expression Tree itself.
         * This method cannot guarantee to return an existing InstructionMask.
         */
        protected abstract Tree rebuildInstructionMaskTree();

        @Override
        public String toString() {
            return "MaskTree@" + Integer.toHexString(hashCode());
        }
    }

    /**
     * Elementary mask specified by the contained InstructionMask iMask.
     */
    private static class MaskLeaf extends MaskTree {
        protected InstructionMask iMask;

        protected MaskLeaf(InstructionMask iMask, int nbDims) {
            super(nbDims);
            this.iMask = iMask;
        }

        @Override
        protected InstructionMask rebuildInstructionMask() {
            return iMask;
        }

        @Override
        protected Tree rebuildInstructionMaskTree() {
            return iMask.rebuildTree();
        }

        @Override
        public String toString() {
            return "Mask" + iMask;
        }
    }

    /**
     * Combination of the two contained masks with a logical "AND".
     */
    private static class MaskAnd extends MaskTree {
        private MaskTree mask1, mask2;

        protected MaskAnd(MaskTree mask1, MaskTree mask2, int nbDims) {
            super(nbDims);
            this.mask1 = mask1;
            this.mask2 = mask2;
        }

        @Override
        protected InstructionMask rebuildInstructionMask() {
            return new InstructionMask(rebuildInstructionMaskTree(), null);
        }

        @Override
        protected Tree rebuildInstructionMaskTree() {
            Tree maskTree1 = mask1.rebuildInstructionMaskTree();
            Tree maskTree2 = mask2.rebuildInstructionMaskTree();
            if (maskTree1 == null) {
                return maskTree2;
            } else if (maskTree2 == null) {
                return maskTree1;
            } else {
                return ILUtils.build(ILLang.op_and, maskTree1, maskTree2);
            }
        }

        @Override
        public String toString() {
            return mask1 + " AND " + mask2;
        }
    }

    /**
     * Reduction of the contained mask through the "ANY" primitive.
     */
    private static class MaskAny extends MaskTree {
        private MaskTree mask;
        private int dimValue;

        protected MaskAny(MaskTree mask, int dimValue, int nbDims) {
            super(nbDims);
            this.mask = mask;
            this.dimValue = dimValue;
        }

        /**
         * It seems redundant to generate an "ANY(mask,dimValue)"
         * because the surrounding code will be guarded by "mask" itself.
         */
        @Override
        protected InstructionMask rebuildInstructionMask() {
            return null;
        }

        @Override
        protected Tree rebuildInstructionMaskTree() {
            return null;
        }

        @Override
        public String toString() {
            return "ANY:" + dimValue + "(" + mask + ')';
        }
    }

    /**
     * Expansion of the contained mask through a "SPREAD" along a new dimension.
     */
    private static class MaskSpread extends MaskTree {
        private MaskTree mask;
        private int dimValue;
        private Tree dimLength;

        protected MaskSpread(MaskTree mask, int dimValue, Tree dimLength, int nbDims) {
            super(nbDims);
            this.mask = mask;
            this.dimValue = dimValue;
            this.dimLength = dimLength;
        }

        @Override
        protected InstructionMask rebuildInstructionMask() {
            return new InstructionMask(rebuildInstructionMaskTree(), null);
        }

        @Override
        protected Tree rebuildInstructionMaskTree() {
            Tree maskTree = mask.rebuildInstructionMaskTree();
            if (maskTree == null) {
                return null;
            }
            return ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "spread"),
                    ILUtils.build(ILLang.op_expressions,
                            maskTree,
                            ILUtils.build(ILLang.op_intCst, dimValue),
                            ILUtils.copy(dimLength)));
        }

        @Override
        public String toString() {
            return "SPR:" + dimValue + ':' + ILUtils.toString(dimLength) + "(" + mask + ')';
        }
    }

    /**
     * depth level used for display of recursive calls to buildTreeBackward()
     */
    protected int levelBTB = 0;

    /**
     * Abstract class for all nodes of linearization trees.
     */
    private abstract class LinTree {
        /**
         * The parent node in the linearization tree.
         */
        protected LinTree parent;
        /**
         * The class of children of the parent node to which this node belongs.
         */
        protected int locInParent;

        /**
         * Constructor.
         */
        private LinTree(LinTree parent, int locInParent) {
            this.parent = parent;
            this.locInParent = locInParent;
        }

        /**
         * Adds newChild as a new child of this LinTree, more precisely into its locInThis-th set of childs
         * (when this makes sense i.e. for LinIf and LinCatenate). @return newChild.
         */
        protected abstract LinTree addNewChild(LinTree newChild, int locInThis);

        private LinLeaf addLinLeaf(Tree ref, Tree[] diffRefR, InstructionMask iMask, boolean normal, int locInThis) {
            return (LinLeaf) addNewChild(new LinLeaf(ref, diffRefR, iMask, normal, this, locInThis), locInThis);
        }

        private LinStd addLinStd(Tree deriv, TypeSpec derivType, int locInThis) {
            return (LinStd) addNewChild(new LinStd(deriv, derivType, null, this, locInThis), locInThis);
        }

        private LinConvert addLinConvert(Tree conversionExp, int locInThis) {
            return (LinConvert) addNewChild(new LinConvert(conversionExp, null, null, this, locInThis), locInThis);
        }

        private LinIf addLinIf(Tree test, int locInThis) {
            return (LinIf) addNewChild(new LinIf(test, null, null, this, locInThis), locInThis);
        }

        private LinCatenate addLinCatenate(Tree expression, Tree[] expressions, int locInThis) {
            return (LinCatenate) addNewChild(new LinCatenate(expression, expressions, this, locInThis), locInThis);
        }

        private LinSpread addLinSpread(TapList<ArrayDim> allDimensions, Tree dim, Tree nCopies, int locInThis) {
            return (LinSpread) addNewChild(new LinSpread(allDimensions, dim, nCopies, null, this, locInThis), locInThis);
        }

        private LinReduce addLinReduce(TapList<ArrayDim> allDimensions, Tree dim, Tree tMask, InstructionMask argMask, int locInThis) {
            return (LinReduce) addNewChild(new LinReduce(allDimensions, dim, tMask, argMask, null, this, locInThis), locInThis);
        }

        /**
         * top-down sweep on the linearized tree, to generate the tangent expression (utility).
         *
         * @return a Tree with the "ExprType" annotation.
         */
        protected Tree buildTreeForwardOnChildrenNodes(TapList<LinTree> linTrees,
                                                       TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                                       TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            Tree result = null;
            TypeSpec resultType = null;
            Tree newTree;
            TypeSpec newTreeType;
            while (linTrees != null) {
                newTree = linTrees.head.buildTreeForward(primRW, diffRW);
                newTreeType = newTree == null ? null : newTree.getRemoveAnnotation("ExprType");
                // addProtectedExprs() doesn't use the types yet!
                result = ILUtils.addProtectedExprs(result, newTree);
                resultType = resultType == null ? newTreeType
                        : newTreeType == null ? resultType
                        : adEnv.curSymbolTable().combineNumeric(resultType, newTreeType, result);
                linTrees = linTrees.tail;
            }
            if (result != null) {
                result.setAnnotation("ExprType", resultType);
            }
            return result;
        }

        /**
         * top-down sweep on the linearized tree, to generate the tangent expression.
         *
         * @return a Tree with the "ExprType" annotation.
         */
        protected abstract Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                                 TapPair<TapList<Tree>, TapList<Tree>> diffRW);

        /**
         * top-down sweep on the linearized tree, to generate all adjoint assignments (utility).
         */
        protected void buildTreesBackwardOnChildrenNodes(TapList<LinTree> linTrees,
                                                         MaskTree mask,
                                                         TapList<DiffAssignmentNode> toResult,
                                                         TapList<Tree> primR) {
            while (linTrees != null) {
                linTrees.head.buildTreesBackwardOnNode(mask, toResult, primR);
                linTrees = linTrees.tail;
            }
        }

        /**
         * top-down sweep on the linearized tree, to generate all adjoint assignments.
         */
        protected abstract void buildTreesBackwardOnNode(MaskTree mask,
                                                         TapList<DiffAssignmentNode> toResult,
                                                         TapList<Tree> primR);

        /**
         * bottom-up sweep on one branch of the linearized tree, to generate one adjoint assignment.
         * adjPrefix is the prefix of the future result Tree, i.e. the part of the adjoint result that
         * goes from the leaf of the linearized tree up to this LinTree node.
         * @param mayImplicitSpread initially true, becomes false after going through a LinReduce,
         *  to make sure that all the adjoint "SPREAD()" cannot be implicit.
         */
        protected abstract Tree buildTreeBackward(Tree adjPrefix, int locInThis, boolean mayImplicitSpread);

        protected String toStringOnChildrenNodes(TapList<LinTree> linTrees) {
            String result = "";
            while (linTrees != null) {
                result = result + linTrees.head;
                linTrees = linTrees.tail;
                if (linTrees != null) {
                    result = result + ",";
                }
            }
            return "{" + result + "}";
        }

        @Override
        public String toString() {
            return "LinTree@" + Integer.toHexString(hashCode());
        }

    }


    /**
     * A leaf node of a linearization tree, standing for a rhs (or lhs) ref-expression.
     */
    private class LinLeaf extends LinTree {
        /**
         * The primal (reference) expression at this leaf.
         */
        Tree ref;
        /**
         * The derivatives of the expression at this leaf (one per replica).
         */
        private Tree[] diffRefR;
        /**
         * When this leaf reads a temporary (i.e. split) variable, contains the split var linearized assignment tree.
         */
        private LinAssign correspondingAssign;
        /**
         * When this leaf is an array, the mask that applies to it.
         */
        private InstructionMask iMask;
        /**
         * When true, each diffRef is incremented and then reset to zero after use.
         * When false (e.g temporary variables), each diffRef is just set, and doesn't need resetting after use.
         */
        private boolean normal = true;

        protected LinLeaf(Tree ref, Tree[] diffRefR, InstructionMask iMask, boolean normal, LinTree parent, int locInParent) {
            super(parent, locInParent);
            this.ref = ref;
            this.iMask = iMask;
            this.diffRefR = diffRefR;
            this.normal = normal;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThis) {
            TapEnv.toolError("addNewChild() forbidden on a linearization leaf:" + this);
            return null;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            TapList<Tree> toNewDiffR = new TapList<>(null, null) ;
            primRW.first = ILUtils.usedVarsInDiffExp(ref, primRW.first, toNewDiffR, false);
            diffRW.first = ILUtils.addListTreeInList(toNewDiffR.tail, diffRW.first) ;
            diffRW.first = ILUtils.addTreeInList(ref, diffRW.first);
            Tree result = ILUtils.copy(diffRefR[adEnv.iReplic]);
            result.setAnnotation("ExprType", adEnv.curSymbolTable().typeOf(ref));
            return result;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            if (correspondingAssign != null) {
                correspondingAssign.assignmentMask = mask;
            }
            Tree backwardChain = parent.buildTreeBackward(null, locInParent, true);
            assert backwardChain != null;
            TypeSpec diffBackwardChainType = backwardChain.getRemoveAnnotation("ExprType");
            removeAllMarksAsVector(backwardChain);
            if (diffBackwardChainType != null && diffBackwardChainType.isComplexBase()
                    && !adEnv.curSymbolTable().typeOf(ref).isComplexBase()) {
                backwardChain = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, (adEnv.curUnit().isFortran() ? "real" : "creal")),
                        ILUtils.build(ILLang.op_expressions, backwardChain));
                backwardChain = ILUtils.pullProtectedArg(backwardChain, 1) ;
            }
            TapList<Tree> toNewDiffR = new TapList<>(null, null) ;
            primR = ILUtils.usedVarsInDiffExp(ref, primR, toNewDiffR, false);
            // toResult.head is the SET or RESET DiffAssignmentNode of the diff Lhs of the current assignment
            DiffAssignmentNode diffLhsNode = toResult.head;
            if (1 == ILUtils.eqOrDisjointRef(ref, diffLhsNode.primRecv, adEnv.curInstruction(), adEnv.curInstruction())
                    && InstructionMask.equalMasks(iMask, diffLhsNode.mask)) {
                // If this leaf variable in the Rhs is "the same as" the Lhs of the current assignment:
                diffLhsNode.primR = ILUtils.addListTreeInList(primR, diffLhsNode.primR);
                diffLhsNode.diffR = ILUtils.addListTreeInList(toNewDiffR.tail, diffLhsNode.diffR) ;
                diffLhsNode.diffR = ILUtils.addTreeInList(ref, diffLhsNode.diffR);
                diffLhsNode.diffValue = ILUtils.addProtectedExprs(diffLhsNode.diffValue, backwardChain);
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("       UPDATED ADJOINT NODE OF DIFF LHS:" + diffLhsNode);
                }
            } else {
                TapList<Tree> diffR = diffLhsNode.diffW;
                diffR = ILUtils.addListTreeInList(toNewDiffR.tail, diffR) ;
                if (normal) {
                    diffR = ILUtils.addTreeInList(ref, diffR);
                }
                InstructionMask rebuiltMask = mask == null ? null : mask.rebuildInstructionMask();
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("         <- Rebuilt mask " + rebuiltMask + " from mask:" + mask + " around assignment of diff " + this);
                }
                toResult.placdl(
                        new DiffAssignmentNode(ILUtils.copy(diffRefR[adEnv.iReplic]),
                                (normal ? DiffConstants.INCREMENT_VARIABLE : DiffConstants.SET_VARIABLE),
                                backwardChain, rebuiltMask, ref, adEnv.iReplic,
                                primR, null, diffR, new TapList<>(ref, null)));
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("       BUILT ADJOINT NODE TO UPDATE DIFF FROM RHS:" + toResult.tail.head);
                }
            }
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused, boolean mayImplicitSpread) {
            return null; // Never called. this.buildTreesBackwardOnNode() directly calls parent.buildTreeBackward().
        }

        @Override
        public String toString() {
            return "LinLeaf:"+ILUtils.toString(diffRefR[0])+(diffRefR.length==1 ? "" : " *"+diffRefR.length+"replicas");
        }
    }

    /**
     * A classic node of a linearization tree, multiplying the sum of the
     * "children" linearized derivatives with the elementary partial derivative "factor".
     */
    private class LinStd extends LinTree {
        /**
         * The common factor, i.e. partial derivative.
         */
        private Tree factor;
        /**
         * The type of the factor.
         */
        private TypeSpec factorType;
        /**
         * The collection of linearized trees whose sum is multiplied with the factor.
         */
        private TapList<LinTree> children;

        protected LinStd(Tree factor, TypeSpec factorType, TapList<LinTree> children, LinTree parent, int locInParent) {
            super(parent, locInParent);
            this.factor = factor;
            this.factorType = factorType;
            this.children = children;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThisUnused) {
            children = TapList.addLast(children, newChild);
            return newChild;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            primRW.first = ILUtils.usedVarsInExp(factor, primRW.first, true);
            Tree aaFactor = ILUtils.copy(factor);
            if (TapEnv.associationByAddress()) {
                aaFactor = blockDifferentiator().turnAssociationByAddressPrimal(aaFactor, adEnv.curSymbolTable(),
                                                                                adEnv.curFwdSymbolTable, null, true);
            }
            aaFactor.setAnnotation("ExprType", factorType);
            return mulProtectedTypedExprs(aaFactor,
                    buildTreeForwardOnChildrenNodes(children, primRW, diffRW), false);
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            primR = ILUtils.usedVarsInExp(factor, primR, true);
            buildTreesBackwardOnChildrenNodes(children, mask, toResult, primR);
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree aaFactor = ILUtils.copy(factor);
            markAsVectorInside(aaFactor);
            markAsVector(aaFactor, factorType != null && factorType.isArray());
            if (TapEnv.associationByAddress()) {
                aaFactor = blockDifferentiator().turnAssociationByAddressPrimal(aaFactor, adEnv.curSymbolTable(),
                                                                                adEnv.curFwdSymbolTable, null, true);
            }
            aaFactor.setAnnotation("ExprType", factorType);
            adjPrefix = (adjPrefix == null ? aaFactor : mulProtectedTypedExprs(adjPrefix, aaFactor, false));
            Tree adjExpr = parent.buildTreeBackward(adjPrefix, locInParent, mayImplicitSpread);
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            return "LinStd:" + ILUtils.toString(factor) + " * " + toStringOnChildrenNodes(children);
        }
    }

    /**
     * A special node of a linearization tree, standing for a type-conversion operation
     * E.g. a C "cast", or a FORTRAN conversion. In tangent, apply the same conversion,
     * In adjoint, apply some "reciprocal" conversion.
     */
    private class LinConvert extends LinTree {
        private Tree conversionExp;
        private TapList<LinTree> children1;
        private TapList<LinTree> children2;

        protected LinConvert(Tree conversionExp, TapList<LinTree> children1, TapList<LinTree> children2, LinTree parent, int locInParent) {
            super(parent, locInParent);
            this.conversionExp = conversionExp;
            this.children1 = children1;
            this.children2 = children2;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThis) {
            if (locInThis == 1) {
                children1 = TapList.addLast(children1, newChild);
            } else {
                children2 = TapList.addLast(children2, newChild);
            }
            return newChild;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            primRW.first = ILUtils.usedVarsInExp(conversionExp, primRW.first, true);
            Tree result;
            Tree diffArgument = buildTreeForwardOnChildrenNodes(children1, primRW, diffRW);
            // copy the primal conversion operation:
            if (conversionExp.opCode()==ILLang.op_cast || conversionExp.opCode()==ILLang.op_multiCast) {
                result = ILUtils.copy(conversionExp);
                TypeSpec diffArgType = //not used so far
                        diffArgument == null ? null : diffArgument.getRemoveAnnotation("ExprType");
                result.setChild(diffArgument, 2);
                //TODO: what if diffArgument is protected?
                result.setAnnotation("ExprType", adEnv.curSymbolTable().typeOf(conversionExp));
            } else if (conversionExp.opCode() == ILLang.op_call) {
                result = ILUtils.copy(conversionExp);
                TypeSpec diffArgType = //not used so far
                        diffArgument == null ? null : diffArgument.getRemoveAnnotation("ExprType");
                if (diffArgument == null) {
                    diffArgument = ILUtils.build(ILLang.op_realCst, "0.0");
                }
                ILUtils.getArguments(result).setChild(diffArgument, 1);
                if (children2 != null) { //If this is a conversion with 2 args, e.g. CMPLX(a,b)
                    diffArgument = buildTreeForwardOnChildrenNodes(children2, primRW, diffRW);
                    diffArgType = //not used so far
                            diffArgument == null ? null : diffArgument.getRemoveAnnotation("ExprType");
                    if (diffArgument == null) {
                        diffArgument = ILUtils.build(ILLang.op_realCst, "0.0");
                    }
                    ILUtils.getArguments(result).setChild(diffArgument, 2);
                }
                result = ILUtils.pullProtectedArg(result, 1) ; //TODO: should do it also on arg2 if CMPLX(a,b)
                result.setAnnotation("ExprType", adEnv.curSymbolTable().typeOf(conversionExp));
            } else { // Fallback: do no conversion!
                result = diffArgument;
            }
            return result;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            primR = ILUtils.usedVarsInExp(conversionExp, primR, true);
            buildTreesBackwardOnChildrenNodes(children1, mask, toResult, primR);
            if (children2 != null) { //If this is a conversion with 2 args, e.g. CMPLX(a,b)
                buildTreesBackwardOnChildrenNodes(children2, mask, toResult, primR);
            }
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThis, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree adjExpr = parent.buildTreeBackward(null, locInParent, mayImplicitSpread);
            assert adjExpr != null;
            TypeSpec adjExprType = //not used so far
                    adjExpr.getRemoveAnnotation("ExprType");
            Tree convertedArg;
            if (conversionExp.opCode() == ILLang.op_call) {
                convertedArg = ILUtils.getArguments(conversionExp).down(1);
            } else {
                convertedArg = conversionExp.down(1); //e.g. op_cast
            }
            TypeSpec arg1Type = adEnv.curSymbolTable().typeOf(convertedArg);
            // Insert the "adjoint" conversion around adjExpr.
            // In some cases, or when in doubt, do no conversion!
            // However, e.g. for a cast, we may have to think of a "reciprocal cast".
            boolean isFortran = (adEnv.curUnit().isFortran()) ;
            if (conversionExp.opCode() == ILLang.op_call) {
                String conversionName = ILUtils.getCalledNameString(conversionExp).toLowerCase();
                if (isFortran && "sngl".equals(conversionName)) {
                    adjExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "dble"),
                            ILUtils.build(ILLang.op_expressions, adjExpr));
                    adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                } else if (isFortran && "dble".equals(conversionName)) {
                    adjExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "sngl"),
                            ILUtils.build(ILLang.op_expressions, adjExpr));
                    adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                } else if (isFortran
                           ? ("conjg".equals(conversionName) || "dconjg".equals(conversionName))
                           : ("conj".equals(conversionName))) {
                    adjExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, conversionName),
                            ILUtils.build(ILLang.op_expressions, adjExpr));
                    adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                } else if (isFortran && "aimag".equals(conversionName)) {
                    adjExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "cmplx"),
                            ILUtils.build(ILLang.op_expressions,
                                    ILUtils.build(ILLang.op_realCst, "0.D0"),
                                    adjExpr,
                                    // Explicitly add precision 8 because CMPLX seems to default to precision 4?
                                    ILUtils.build(ILLang.op_intCst, 8)));
                    adjExpr = ILUtils.pullProtectedArg(adjExpr, 2) ;
                } else if (!isFortran && "cimag".equals(conversionName)) {
                    adjExpr = ILUtils.build(ILLang.op_mul,
                                ILUtils.build(ILLang.op_realCst, "1.0iF"),
                                adjExpr) ;
                    //TODO: what if adjExpr is protected?
                } else if (isFortran && "dimag".equals(conversionName)) {
                    adjExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "dcmplx"),
                            ILUtils.build(ILLang.op_expressions,
                                    ILUtils.build(ILLang.op_realCst, "0.D0"),
                                    adjExpr));
                    adjExpr = ILUtils.pullProtectedArg(adjExpr, 2) ;
                } else if (isFortran
                           ? ILUtils.isStringOf(conversionName, new String[]{"real", "dreal"})
                           : "creal".equals(conversionName)) {
                    if (arg1Type.isComplexBase()) {
// do nothing, assuming that CMPLX(X,0.D0) equivalent to X ?
//                     adjExpr = ILUtils.buildCall(
//                                ILUtils.build(ILLang.op_ident, "cmplx"),
//                                ILUtils.build(ILLang.op_expressions,
//                                  adjExpr,
//                                  ILUtils.build(ILLang.op_realCst, "0.D0"))) ;
//                     adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                    }
                } else if (isFortran && "dcmplx".equals(conversionName)) {
                    if (!arg1Type.isComplexBase()) {
                        adjExpr = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, (locInThis == 1 ? "dreal" : "dimag")),
                                ILUtils.build(ILLang.op_expressions, adjExpr));
                        adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                    }
                } else if (isFortran && ILUtils.isStringOf(conversionName, new String[]{"cmplx", "complex"})) {
                    if (!arg1Type.isComplexBase()) {
                        adjExpr = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident,
                                              (locInThis == 1
                                               ? (isFortran ? "real" : "creal")
                                               : (isFortran ? "aimag" : "cimag"))),
                                ILUtils.build(ILLang.op_expressions, adjExpr));
                        adjExpr = ILUtils.pullProtectedArg(adjExpr, 1) ;
                    }
                }
            }
            if (adjPrefix != null) {
                adjExpr = mulProtectedTypedExprs(adjPrefix, adjExpr, true);
            }
            adjExpr.setAnnotation("ExprType", arg1Type);
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            return "LinConvert:" + ILUtils.toString(conversionExp) + " * " + toStringOnChildrenNodes(children1)
                    + (children2 == null ? "" : " & " + toStringOnChildrenNodes(children2));
        }
    }

    /**
     * A special node of a linearization tree, standing for an if-expression.
     */
    private class LinIf extends LinTree {
        private Tree test;
        private TapList<LinTree> trueExp;
        private TapList<LinTree> falseExp;

        protected LinIf(Tree test, TapList<LinTree> trueExp, TapList<LinTree> falseExp, LinTree parent, int locInParent) {
            super(parent, locInParent);
            this.test = test;
            this.trueExp = trueExp;
            this.falseExp = falseExp;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThis) {
            if (locInThis == 1) {
                trueExp = TapList.addLast(trueExp, newChild);
            } else {
                falseExp = TapList.addLast(falseExp, newChild);
            }
            return newChild;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            primRW.first = ILUtils.usedVarsInExp(test, primRW.first, true);
            Tree aaTest = ILUtils.copy(test);
            if (TapEnv.associationByAddress()) {
                aaTest = blockDifferentiator().turnAssociationByAddressPrimal(aaTest, adEnv.curSymbolTable(),
                                                                              adEnv.curFwdSymbolTable, null, true);
            }
            Tree diffExpr1 = buildTreeForwardOnChildrenNodes(trueExp, primRW, diffRW);
            TypeSpec type1 = diffExpr1 == null ? null : diffExpr1.getRemoveAnnotation("ExprType");
            if (diffExpr1 == null) {
                diffExpr1 = ILUtils.build(ILLang.op_realCst, "0.0");
            }
            Tree diffExpr2 = buildTreeForwardOnChildrenNodes(falseExp, primRW, diffRW);
            TypeSpec type2 = diffExpr2 == null ? null : diffExpr2.getRemoveAnnotation("ExprType");
            if (diffExpr2 == null) {
                diffExpr2 = ILUtils.build(ILLang.op_realCst, "0.0");
            }
            Tree diffIfTree = ILUtils.build(ILLang.op_ifExpression,
                    ILUtils.copy(aaTest),
                    diffExpr1, diffExpr2);
            TypeSpec jointType = type1 == null ? type2
                    : type2 == null ? type1
                    : adEnv.curSymbolTable().combineNumeric(type1, type2, diffIfTree);
            diffIfTree.setAnnotation("ExprType", jointType);
            return diffIfTree;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            primR = ILUtils.usedVarsInExp(test, primR, true);
            buildTreesBackwardOnChildrenNodes(trueExp, mask, toResult, primR);
            buildTreesBackwardOnChildrenNodes(falseExp, mask, toResult, primR);
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThis, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree adjSuffix = parent.buildTreeBackward(null, locInParent, mayImplicitSpread);
            assert adjSuffix != null;
            TypeSpec diffResultType = adjSuffix.getRemoveAnnotation("ExprType");
            boolean wasVector = removeAllMarksAsVector(adjSuffix);
            Tree zeroTree = diffResultType.buildConstantZero();
            Tree aaTest = ILUtils.copy(test);
            if (TapEnv.associationByAddress()) {
                aaTest = blockDifferentiator().turnAssociationByAddressPrimal(aaTest, adEnv.curSymbolTable(),
                                                                              adEnv.curFwdSymbolTable, null, true);
            }
            adjSuffix = ILUtils.build(ILLang.op_ifExpression,
                    ILUtils.copy(aaTest),
                    locInThis == 1 ? adjSuffix : zeroTree,
                    locInThis == 1 ? zeroTree : adjSuffix);
            markAsVector(adjSuffix, wasVector);
            adjSuffix.setAnnotation("ExprType", diffResultType);
            Tree adjExpr = (adjPrefix == null ? adjSuffix
                    : mulProtectedTypedExprs(adjPrefix, adjSuffix, true));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            return "LinIf:" + ILUtils.toString(test) + " TT:" + toStringOnChildrenNodes(trueExp) + " FF:" + toStringOnChildrenNodes(falseExp);
        }
    }

    /**
     * A special node of a linearization tree, standing for operations that just catenate their arguments,
     * such as op_arrayConstructor, op_iterativeVariableRef, op_constructorCall...
     */
    private class LinCatenate extends LinTree {
        // [llh 20Jun19] TODO: op_constructorCall is different, it doesn't really catenate,
        //  doesn't care about unknown sizes, and should skip over passive components of the constructed structure.
        private Tree origExpression;
        private TapList<LinTree>[] elements;
        private int[] elementsOffset;

        protected LinCatenate(Tree expression, Tree[] expressions, LinTree parent, int locInParent) {
            super(parent, locInParent);
            origExpression = expression;
            int length = expressions.length;
            elements = (TapList<LinTree>[]) new TapList[length];
            elementsOffset = null;
            if (!tangentDiff) { // elementsOffset is needed only for adjoint diff:
                elementsOffset = new int[length + 1];
                int curOffset = curDiffUnit().arrayDimMin;
                for (int i = 0; i < length; ++i) {
                    elements[i] = null;
                    elementsOffset[i] = curOffset;
                    int elementLength = 1;
                    WrapperTypeSpec exprTypeSpec = adEnv.curSymbolTable().typeOf(expressions[i]);
                    TapList<ArrayDim> dimensions = exprTypeSpec.getAllDimensions();
                    while (dimensions != null) {
                        int dimSz = dimensions.head.size();
                        if (dimSz == -1) {
                            TapEnv.fileWarning(-1, expressions[i], "(Adjoint differentiation of catenation) unknown size of element: " + expressions[i]);
                            dimSz = 1;
                        }
                        elementLength *= dimSz;
                        dimensions = dimensions.tail;
                    }
                    curOffset += elementLength;
                }
                elementsOffset[length] = curOffset;
            }
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThis) {
            elements[locInThis] = TapList.addLast(elements[locInThis], newChild);
            return newChild;
        }

        /**
         * Utility for buildTreeForward().
         */
        private TapList<Tree> buildElementTrees(Tree[] elementTrees,
                                                TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                                TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            TapList<Tree> diffElementsTrees = null;
            for (int i = elements.length - 1; i >= 0; --i) {
                Tree diffElement = buildTreeForwardOnChildrenNodes(elements[i], primRW, diffRW);
                TypeSpec diffElementType = //not used so far
                        diffElement == null ? null : diffElement.getRemoveAnnotation("ExprType");
                if (diffElement == null && origExpression.opCode() != ILLang.op_constructorCall) {
                    diffElement = adEnv.curSymbolTable().typeOf(elementTrees[i]).buildConstantZero();
                }
                //Skip when diffElement is null. Makes sense only for constructorCall.
                if (diffElement != null) {
                    diffElementsTrees = new TapList<>(diffElement, diffElementsTrees);
                }
            }
            return diffElementsTrees;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            Tree diffExpr;
            if (origExpression.opCode() == ILLang.op_iterativeVariableRef) {
                diffExpr = ILUtils.build(ILLang.op_iterativeVariableRef,
                        ILUtils.build(ILLang.op_expressions,
                                buildElementTrees(origExpression.down(1).children(), primRW, diffRW)),
                        ILUtils.copy(origExpression.down(2)));
            } else if (origExpression.opCode() == ILLang.op_constructorCall) {
                WrapperTypeSpec typeSpec = adEnv.curSymbolTable().typeOf(origExpression);
                // [llh 20Jun19] not convinced by the following code to differentiate the type name...
                boolean eqType = typeSpec.equalsCompilDep(typeSpec.wrappedType.diffTypeSpec)
                        || typeSpec.wrappedType.diffTypeSpec == null
                        || ((CompositeTypeSpec) typeSpec.wrappedType.diffTypeSpec.wrappedType).isEmpty();
                Tree diffTypeName = ILUtils.copy(origExpression.down(1));
                if (!eqType) {
                    TypeDecl typeDecl = adEnv.curSymbolTable().getTypeDecl(origExpression.down(1).stringValue());
                    NewSymbolHolder newSymbolHolder = typeDecl.getDiffSymbolHolder(0, null, 0);
                    diffTypeName = newSymbolHolder.makeNewRef(null);
                }
                diffExpr = ILUtils.build(ILLang.op_constructorCall,
                        diffTypeName,
                        ILUtils.copy(diffTypeName),
                        ILUtils.build(ILLang.op_expressions,
                                buildElementTrees(origExpression.down(3).children(), primRW, diffRW)));
            } else {
                diffExpr = ILUtils.build(ILLang.op_arrayConstructor,
                        buildElementTrees(origExpression.children(), primRW, diffRW));
            }
            diffExpr.setAnnotation("ExprType", adEnv.curSymbolTable().typeOf(origExpression));
            return diffExpr;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            for (TapList<LinTree> element : elements) {
                buildTreesBackwardOnChildrenNodes(element, null, toResult, primR);
            }
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThis, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree adjSuffix = parent.buildTreeBackward(null, locInParent, mayImplicitSpread);
            assert adjSuffix != null;
            TypeSpec diffResultType = //not used so far
                    adjSuffix.getRemoveAnnotation("ExprType");
            removeAllMarksAsVector(adjSuffix);
            adjSuffix = ILUtils.buildArrayTripletAccess(adjSuffix,
                    ILUtils.build(ILLang.op_intCst, elementsOffset[locInThis]),
                    elementsOffset[locInThis + 1] - 1 > elementsOffset[locInThis]
                            ? ILUtils.build(ILLang.op_intCst, elementsOffset[locInThis + 1] - 1)
                            : null,
                    curDiffUnit().arrayDimMin);
            Tree[] elementTrees;
            if (origExpression.opCode() == ILLang.op_iterativeVariableRef) {
                elementTrees = origExpression.down(1).children();
            } else if (origExpression.opCode() == ILLang.op_constructorCall) {
                elementTrees = origExpression.down(3).children();
            } else {
                elementTrees = origExpression.children();
            }
            adjSuffix.setAnnotation("ExprType", adEnv.curSymbolTable().typeOf(elementTrees[locInThis]));
            Tree adjExpr = (adjPrefix == null ? adjSuffix
                    : mulProtectedTypedExprs(adjPrefix, adjSuffix, true));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            String result = "";
            for (int i = 0; i < elements.length; ++i) {
                result = result + (i == 0 ? "" : ", ") + toStringOnChildrenNodes(elements[i]);
            }
            String offsetsStr = "";
            if (!tangentDiff) {
                for (int i = 0; i <= elements.length; ++i) {
                    if (i != 0) {
                        offsetsStr = offsetsStr + ";";
                    }
                    offsetsStr = offsetsStr + elementsOffset[i];
                }
            }
            return "LinCatenate:[" + offsetsStr + "](/" + result + "/)";
        }
    }

    /**
     * A special node of a linearization tree, standing for an array SUM reduction, possibly with DIM and MASK.
     */
    private class LinReduce extends LinTree {
        private TapList<LinTree> exp;
        private Tree dim;
        private int dimValue = -1;
        private Tree tMask;
        private TapList<ArrayDim> allDimensions;
        private InstructionMask argMask;

        protected LinReduce(TapList<ArrayDim> allDimensions, Tree dim, Tree tMask, InstructionMask argMask,
                            TapList<LinTree> exp, LinTree parent, int locInParent) {
            super(parent, locInParent);
            if (ILUtils.isNullOrNone(dim)) {
                dim = null;
            }
            this.dim = dim;
            if (dim != null) {
                Integer dimI = adEnv.curSymbolTable().computeIntConstant(dim);
                dimValue = dimI == null ? -1 : dimI;
                if (!tangentDiff && dimValue == -1) {
                    TapEnv.fileWarning(-1, dim, "(Adjoint differentiation of SUM) statically unknown DIM rank: " + ILUtils.toString(dim));
                }
            }
            this.allDimensions = allDimensions;
            this.tMask = tMask;
            this.argMask = argMask;
            this.exp = exp;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThisUnused) {
            exp = TapList.addLast(exp, newChild);
            return newChild;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            TapList<Tree> diffSumArgs = null;
            if (tMask != null) {
                primRW.first = ILUtils.usedVarsInExp(tMask, primRW.first, true);
                diffSumArgs = new TapList<>(ILUtils.build(ILLang.op_nameEq,
                        ILUtils.build(ILLang.op_ident, "MASK"),
                        ILUtils.copy(tMask)),
                        diffSumArgs);
            }
            if (dim != null) {
                primRW.first = ILUtils.usedVarsInExp(dim, primRW.first, true);
                diffSumArgs = new TapList<>(ILUtils.build(ILLang.op_nameEq,
                        ILUtils.build(ILLang.op_ident, "DIM"),
                        ILUtils.copy(dim)),
                        diffSumArgs);
            }
            Tree diffReduceArg = buildTreeForwardOnChildrenNodes(exp, primRW, diffRW);
            TypeSpec diffReduceArgType =
                    diffReduceArg == null ? null : diffReduceArg.getRemoveAnnotation("ExprType");
            diffSumArgs = new TapList<>(diffReduceArg, diffSumArgs);
            TypeSpec diffReduceCallType =
                    diffReduceArgType == null ? null : diffReduceArgType.elementType();
            int nbDims = TapList.length(allDimensions);
            if (!ILUtils.isNullOrNone(dim) && nbDims > 1) { // reduce on only one dimension.
                int dimVal = (dimValue!=-1 ? dimValue : nbDims) ; // Arbitrary fallback dim = last dim!
                TapList<ArrayDim> allDimensionsButOne =
                        TapList.deleteNth(allDimensions, nbDims - dimVal); // Fortran array dimensions are reversed!
                diffReduceCallType = new WrapperTypeSpec(
                        new ArrayTypeSpec((WrapperTypeSpec) diffReduceCallType,
                                ArrayTypeSpec.createDimensions(allDimensionsButOne)));
            }
            Tree diffReduceCall = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "SUM"),
                    ILUtils.build(ILLang.op_expressions, diffSumArgs));
            diffReduceCall = ILUtils.pullProtectedArg(diffReduceCall, 1) ;
            diffReduceCall.setAnnotation("ExprType", diffReduceCallType);
            return diffReduceCall;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            primR = ILUtils.usedVarsInExp(dim, primR, true);
            primR = ILUtils.usedVarsInExp(tMask, primR, true);
            buildTreesBackwardOnChildrenNodes(exp, buildMaskOfSumArg(mask), toResult, primR);
        }

        /**
         * Utility for buildTreesBackwardOnNode().
         */
        private MaskTree buildMaskOfSumArg(MaskTree sumResultMask) {
            int nbDims = TapList.length(allDimensions);
            MaskTree result = null;
            if (argMask != null) {
                result = new ExpressionDifferentiator.MaskLeaf(argMask, nbDims);
            }
            if (sumResultMask != null) {
                // Arbitrary fallback dim = last dim!
                int dimVal = dimValue != -1 ? dimValue : nbDims;
                // Fortran array dimensions are reversed!
                ArrayDim arrayDim = (ArrayDim) TapList.nth(allDimensions, nbDims - dimVal);
                Tree dimLength = arrayDim.getSize();
                MaskTree mask2 = new ExpressionDifferentiator.MaskSpread(sumResultMask, dimVal, dimLength, nbDims);
                result = result == null ? mask2 : new ExpressionDifferentiator.MaskAnd(mask2, result, nbDims);
            }
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("         -> push mask " + result + " inside " + this);
            }
            return result;
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            int nbDims = TapList.length(allDimensions);
            // When this reduction does not directly produce a scalar but just removes one dimension,
            // then all SPREAD in the adjoint expression must be explicit.
            if (mayImplicitSpread && nbDims>1 && !ILUtils.isNullOrNone(dim)) {
                mayImplicitSpread = false ;
            }
            Tree adjSuffix = parent.buildTreeBackward(null, locInParent, mayImplicitSpread);
            assert adjSuffix != null;
            TypeSpec diffResultType =
                    adjSuffix.getRemoveAnnotation("ExprType");
            if (!mayImplicitSpread) { // Otherwise a SPREAD is implicit, and therefore useless.
                removeAllMarksAsVector(adjSuffix);
                int dimVal = (dimValue!=-1 ? dimValue : nbDims) ; // Arbitrary fallback dim = last dim!
                ArrayDim arrayDim = (ArrayDim) TapList.nth(allDimensions, nbDims - dimVal); // Fortran array dimensions are reversed!
                adjSuffix = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "SPREAD"),
                        ILUtils.build(ILLang.op_expressions,
                                adjSuffix, ILUtils.copy(dim), arrayDim.getSize()));
                adjSuffix = ILUtils.pullProtectedArg(adjSuffix, 1) ;
                markAsVector(adjSuffix, true);
            }
            TypeSpec diffArgType = new WrapperTypeSpec(
                    new ArrayTypeSpec(diffResultType.elementType(),
                            ArrayTypeSpec.createDimensions(allDimensions)));
            adjSuffix.setAnnotation("ExprType", diffArgType);
            if (adjPrefix==null /**TODO: OR IF adjPrefix is scalar*/) {
                // If nothing in adjPrefix gives us the reduction size, we must multiply it with an
                // artificial array of "1"'s sized like the reduction size. cf set12/lh16
                Tree dummyOne = ILUtils.build(ILLang.op_intCst, 1);
                TapList<ArrayDim> reducedDimensions = allDimensions ;
                if (dim!=null) {
                    int dimVal = (dimValue!=-1 ? dimValue : nbDims) ; // Arbitrary fallback dim = last dim!
                    ArrayDim arrayDim = (ArrayDim) TapList.nth(allDimensions, nbDims - dimVal); // Fortran array dimensions are reversed!
                    reducedDimensions = new TapList<>(arrayDim, null) ;
                }
                markAsVector(dummyOne, true) ;
                dummyOne.setAnnotation("ExprType", new WrapperTypeSpec(
                                           new ArrayTypeSpec(diffResultType.elementType(),
                                                             ArrayTypeSpec.createDimensions(reducedDimensions)))) ;
                adjPrefix = (adjPrefix==null ? dummyOne : ILUtils.build(ILLang.op_mul, adjPrefix, dummyOne)) ;
            }
            Tree adjExpr = (adjPrefix == null ? adjSuffix
                    : mulProtectedTypedExprs(adjPrefix, adjSuffix, true));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            return "LinReduce:" + toStringOnChildrenNodes(exp);
        }
    }

    /**
     * A special node of a linearization tree, standing for a SPREAD expansion of an array.
     */
    private class LinSpread extends LinTree {
        private TapList<LinTree> exp;
        private Tree dim;
        private int dimValue = -1;
        private Tree nCopies;
        private TapList<ArrayDim> allDimensions;
        private MaskTree spreadMask;

        protected LinSpread(TapList<ArrayDim> allDimensions, Tree dim, Tree nCopies,
                            TapList<LinTree> exp, LinTree parent, int locInParent) {
            super(parent, locInParent);
            if (ILUtils.isNullOrNone(dim)) {
                dim = null;
            }
            this.dim = dim;
            if (dim != null) {
                Integer dimI = adEnv.curSymbolTable().computeIntConstant(dim);
                dimValue = dimI == null ? -1 : dimI;
                if (!tangentDiff && dimValue == -1)
                // immediate DIM value is needed (only for adjoint diff):
                {
                    TapEnv.fileWarning(-1, dim, "(Adjoint differentiation of SPREAD) unknown DIM rank: " + dim);
                }
            }
            this.allDimensions = allDimensions;
            this.nCopies = nCopies;
            this.exp = exp;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThisUnused) {
            exp = TapList.addLast(exp, newChild);
            return newChild;
        }

        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                        TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            Tree diffSpreadArg = buildTreeForwardOnChildrenNodes(exp, primRW, diffRW);
            TypeSpec diffSpreadArgType =
                    diffSpreadArg == null ? null : diffSpreadArg.getRemoveAnnotation("ExprType");
            TapList<Tree> diffSpreadArgs = null;
            Tree diffSpreadCall;
            TypeSpec diffSpreadCallType;
            if (dim != null) {
                primRW.first = ILUtils.usedVarsInExp(nCopies, primRW.first, true);
                diffSpreadArgs = new TapList<>(ILUtils.copy(nCopies), diffSpreadArgs);
                primRW.first = ILUtils.usedVarsInExp(dim, primRW.first, true);
                diffSpreadArgs = new TapList<>(ILUtils.copy(dim), diffSpreadArgs);
                diffSpreadArgs = new TapList<>(diffSpreadArg, diffSpreadArgs);
                diffSpreadCall = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "SPREAD"),
                        ILUtils.build(ILLang.op_expressions, diffSpreadArgs));
                diffSpreadCall = ILUtils.pullProtectedArg(diffSpreadCall, 1) ;
                if (diffSpreadArgType != null && diffSpreadArgType.isArray()) {
                    diffSpreadArgType = diffSpreadArgType.elementType();
                }
                diffSpreadCallType = new WrapperTypeSpec(
                        new ArrayTypeSpec((WrapperTypeSpec) diffSpreadArgType,
                                ArrayTypeSpec.createDimensions(allDimensions)));
            } else { // If primal spread was implicit, tangent spread can remain implicit:
                diffSpreadCall = diffSpreadArg;
                diffSpreadCallType = diffSpreadArgType;
            }
            diffSpreadCall.setAnnotation("ExprType", diffSpreadCallType);
            return diffSpreadCall;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            primR = ILUtils.usedVarsInExp(dim, primR, true);
            primR = ILUtils.usedVarsInExp(nCopies, primR, true);
            buildTreesBackwardOnChildrenNodes(exp, buildMaskOfSpreadArg(mask), toResult, primR);
        }

        private MaskTree buildMaskOfSpreadArg(MaskTree spreadResultMask) {
            spreadMask = spreadResultMask;
            MaskTree result = null;
            // Push down a null Mask when on an implicit SPREAD of a scalar, or no arriving mask at all :
            if (dim != null && spreadResultMask != null) {
                int dimVal = dimValue != -1 ? dimValue : spreadResultMask.nbDims; // Arbitrary fallback dim = last dim!
                result = new ExpressionDifferentiator.MaskAny(spreadResultMask, dimVal, spreadResultMask.nbDims);
            }
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("         -> push mask " + result + " inside " + this);
            }
            return result;
        }

        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree adjSuffix = parent.buildTreeBackward(null, locInParent, mayImplicitSpread);
            assert adjSuffix != null;
            TypeSpec diffResultType =
                    adjSuffix.getRemoveAnnotation("ExprType");
            ToObject<Tree> takenOff = new ToObject<>(null);
            int nbDims = TapList.length(allDimensions);
            TapList<Tree> diffSumArgs = null;
            Tree rebuiltMaskTree = spreadMask == null ? null : spreadMask.rebuildInstructionMaskTree();
            // Partial recovery when the SUMmed expression is an ifExpression: (TODO: patch this better!)
            if (adjSuffix.opCode() == ILLang.op_ifExpression && ILUtils.isNullOrNone(adjSuffix.down(2))) {
                Tree additionalMask = ILUtils.build(ILLang.op_not,
                        ILUtils.copy(adjSuffix.down(1)));
                adjSuffix = adjSuffix.down(3);
                rebuiltMaskTree = rebuiltMaskTree == null
                        ? additionalMask
                        : ILUtils.build(ILLang.op_and, rebuiltMaskTree, additionalMask);
            }
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("         <- rebuilt mask Tree " + rebuiltMaskTree + " from mask:" + spreadMask + " around ADJ SUM " + this);
            }
            if (rebuiltMaskTree != null) {
                diffSumArgs = new TapList<>(ILUtils.build(ILLang.op_nameEq,
                        ILUtils.build(ILLang.op_ident, "MASK"),
                        rebuiltMaskTree),
                        diffSumArgs);
            }
            if (!ILUtils.isNullOrNone(dim) && nbDims > 1) { // Otherwise the SUM doesn't need a DIM argument.
                diffSumArgs = new TapList<>(ILUtils.build(ILLang.op_nameEq,
                        ILUtils.build(ILLang.op_ident, "DIM"),
                        ILUtils.copy(dim)),
                        diffSumArgs);
            }
            adjSuffix = takeScalarsOff(adjSuffix, 1, takenOff);
            if (adjSuffix!=null) {
                diffSumArgs = new TapList<>(adjSuffix, diffSumArgs);
                adjSuffix = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "SUM"),
                    ILUtils.build(ILLang.op_expressions, diffSumArgs));
                adjSuffix = ILUtils.pullProtectedArg(adjSuffix, 1) ;
            } else { // Special case e.g. adjoint of y = SUM(a(:)+x) must generate xb += SIZE(a(:)) * yb
                ArrayDim[] reducedDims = ((ArrayTypeSpec)diffResultType.wrappedType()).dimensions() ;
                Tree totalSize = ILUtils.build(ILLang.op_intCst, 1) ;
                for (int i=reducedDims.length-1 ; i>=0 ; --i) {
                    Tree dimSize = reducedDims[i].getSize() ;
                    totalSize = ILUtils.mulTree(totalSize, dimSize) ;
                }
                adjSuffix = totalSize ;
            }
            markAsVector(adjSuffix, !ILUtils.isNullOrNone(dim) && nbDims > 1);
            if (takenOff.obj() != null) {
                adjSuffix = ILUtils.mulProtectedExprs(adjSuffix, null, takenOff.obj(), null);
            }
            TypeSpec diffSpreadArgType = diffResultType;
            if (diffSpreadArgType != null && diffSpreadArgType.isArray()) {
                diffSpreadArgType = diffSpreadArgType.elementType();
            }
            if (dim != null) { // explicit SPREAD
                int dimVal = dimValue != -1 ? dimValue : nbDims; // Arbitrary fallback dim = last dim!
                TapList<ArrayDim> allDimensionsButOne =
                        TapList.deleteNth(allDimensions, nbDims - dimVal); // Fortran array dimensions are reversed!
                diffSpreadArgType = new WrapperTypeSpec(
                        new ArrayTypeSpec((WrapperTypeSpec) diffSpreadArgType,
                                ArrayTypeSpec.createDimensions(allDimensionsButOne)));
            }
            adjSuffix.setAnnotation("ExprType", diffSpreadArgType);
            Tree adjExpr = (adjPrefix == null ? adjSuffix
                    : mulProtectedTypedExprs(adjPrefix, adjSuffix, true));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        /**
         * Splits term "expression" as a product of two terms.
         *
         * @param power the exponent of current "expression" in the global initial expression.
         * @return the second term, built with only scalars, as the contents of "takenOff".
         */
        private Tree takeScalarsOff(Tree expression, int power, ToObject<Tree> takenOff) {
            Boolean isMarkedVector = expression.getAnnotation("MarkedAsVector");
            if (isMarkedVector == null) { // scalar status of "expression" is dubious.
                if (expression.opCode() == ILLang.op_mul) {
                    Tree new1 = takeScalarsOff(expression.down(1), power, takenOff);
                    Tree new2 = takeScalarsOff(expression.down(2), power, takenOff);
                    return new1 == null ? new2 : new2 == null ? new1 : ILUtils.build(ILLang.op_mul, new1, new2);
                } else if (expression.opCode() == ILLang.op_minus) {
                    Tree new1 = takeScalarsOff(expression.down(1), power, takenOff);
                    takenOff.setObj(takenOff.obj() == null ?
                            ILUtils.build(ILLang.op_realCst, "-1.0")
                            : ILUtils.build(ILLang.op_minus, takenOff.obj()));
                    return new1;
                } else if (expression.opCode() == ILLang.op_div) {
                    Tree new1 = takeScalarsOff(expression.down(1), power, takenOff);
                    Tree new2 = takeScalarsOff(expression.down(2), -power, takenOff);
                    if (new1 == null) {
                        return ILUtils.build(ILLang.op_div,
                                ILUtils.build(ILLang.op_realCst, "1.0"),
                                new2);
                    } else if (new2 == null) {
                        return new1;
                    } else {
                        return ILUtils.build(ILLang.op_div, new1, new2);
                    }
                } else if (expression.opCode() == ILLang.op_power && expression.down(2).opCode() == ILLang.op_intCst) {
                    int localPower = expression.down(2).intValue();
                    Tree new1 = takeScalarsOff(expression.down(1), power * localPower, takenOff);
                    return new1 == null ? null : ILUtils.build(ILLang.op_power, new1, ILUtils.copy(expression.down(2)));
                } else {
                    return expression;
                }
            } else if (isMarkedVector == Boolean.TRUE) { // scalar status of "expression" is "vector"
                removeAllMarksAsVector(expression);
                return expression;
            } else { // scalar status of "expression" is "scalar"
                Tree scalarExpr = expression;
                boolean divide = false;
                if (power < 0) {
                    divide = true;
                    power = -power;
                }
                if (power > 1) {
                    scalarExpr = ILUtils.build(ILLang.op_power, scalarExpr,
                            ILUtils.build(ILLang.op_intCst, power));
                }
                if (divide && takenOff.obj() == null) {
                    takenOff.setObj(ILUtils.build(ILLang.op_realCst, "1.0"));
                }
                takenOff.setObj(takenOff.obj() == null ? scalarExpr :
                        ILUtils.build(divide ? ILLang.op_div : ILLang.op_mul,
                                takenOff.obj(), scalarExpr));
                return null;
            }
        }

        @Override
        public String toString() {
            return "LinSpread:" + toStringOnChildrenNodes(exp);
        }
    }

    /**
     * A special (root) node of a linearization tree, standing for the (linearized) main assignment
     * i.e. compute the sum of all children contained in "linRhs", and assign it into "linLhs".
     */
    private class LinAssign extends LinTree {
        /**
         * The assignment's LHS.
         */
        LinLeaf linLhs;
        /**
         * Type of LHS.
         */
        private TypeSpec assignedType;
        /**
         * true when LHS is an array. This marker is used to move scalar factors out of SUM reductions.
         */
        private boolean isArrayLhs;
        /**
         * The linearized assignment's RHS.
         */
        private TapList<LinTree> linRhs;
        /**
         * The mask of the toplevel of this assignment. In general, comes from the instruction's WHERE mask,
         * but when this is the assignment of an internal temp variable, this mask is set by the mask of the
         * downstream *use* of this temp variable.
         */
        private MaskTree assignmentMask;

        protected LinAssign(LinLeaf linLhs, TapList<LinTree> linRhs, TypeSpec assignedType, boolean isArrayLhs,
                            LinTree parent, int locInParent) {
            super(parent, locInParent); // parent is probably null, (except maybe when nested assignments?)
            this.assignedType = assignedType;
            this.isArrayLhs = isArrayLhs;
            this.linLhs = linLhs;
            this.linRhs = linRhs;
        }

        @Override
        protected LinTree addNewChild(LinTree newChild, int locInThisUnused) {
            linRhs = TapList.addLast(linRhs, newChild);
            return newChild;
        }

        /**
         * Main entry point to generate the tangent assignment of this linearized primal assignment.
         */
        @Override
        protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW, TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
            Tree result = buildTreeForwardOnChildrenNodes(linRhs, primRW, diffRW);
            TypeSpec rhsType = //not used so far
                    result == null ? null : result.getRemoveAnnotation("ExprType");
            if (linLhs != null) {
                TapList<Tree> toNewDiffR = new TapList<>(null, null) ;
                primRW.first = ILUtils.usedVarsInDiffExp(linLhs.ref, primRW.first, toNewDiffR, false);
                diffRW.first = ILUtils.addListTreeInList(toNewDiffR.tail, diffRW.first) ;
                diffRW.second = ILUtils.addTreeInList(linLhs.ref, diffRW.second);
                Tree diffRefCopy = (linLhs.diffRefR==null ? null : ILUtils.copy(linLhs.diffRefR[adEnv.iReplic])) ;
                result = ILUtils.setToProtectedExpr(adEnv.curDiffUnit.language(), diffRefCopy, result, assignedType);
            }
            return result;
        }

        /**
         * Main entry point to generate the adjoint assignments of this linearized primal assignment.
         */
        protected TapList<DiffAssignmentNode> buildTreesBackward() {
            TapList<Tree> toNewDiffR = new TapList<>(null, null) ;
            TapList<Tree> primR = ILUtils.usedVarsInDiffExp(linLhs.ref, null, toNewDiffR, false);
            DiffAssignmentNode lhsDiffAssignNode =
                    new DiffAssignmentNode(ILUtils.copy(linLhs.diffRefR[adEnv.iReplic]),
                            (linLhs.normal ? DiffConstants.SET_VARIABLE : DiffConstants.INCREMENT_VARIABLE),
                            null/*means zero*/, adEnv.curInstruction().whereMask(), linLhs.ref, adEnv.iReplic,
                            primR, null, toNewDiffR.tail, new TapList<>(linLhs.ref, null));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("       INITIALIZED ADJOINT NODE TO RESET DIFF LHS:" + lhsDiffAssignNode);
            }
            TapList<DiffAssignmentNode> toResult = new TapList<>(lhsDiffAssignNode, null);
            buildTreesBackwardOnChildrenNodes(linRhs, buildMaskOfAssign(), toResult, primR);
            // If recomputation of diffLhs is not needed, then remove it:
            if (!linLhs.normal) {
                toResult = toResult.tail;
            }
            return toResult;
        }

        private MaskTree buildMaskOfAssign() {
            if (this.assignmentMask == null) {
                TapList<ArrayDim> dimensions = adEnv.curSymbolTable().typeOf(linLhs.ref).getAllDimensions();
                int nbDims = TapList.length(dimensions);
                InstructionMask iMask = adEnv.curInstruction().whereMask();
                this.assignmentMask = nbDims == 0 || iMask == null ? null : new ExpressionDifferentiator.MaskLeaf(iMask, nbDims);
            }
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("         -> Push mask " + this.assignmentMask + " inside " + this);
            }
            return this.assignmentMask;
        }

        @Override
        protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
            // Never called. this.buildTreesBackward() directly calls buildTreesBackwardOnChildrenNodes().
        }

        /**
         * This is NOT the method that builds the adjoint assignments (it is buildTreesBackward()).
         * This is an internal utility not supposed to be called from outside.
         */
        @Override
        protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused, boolean mayImplicitSpread) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("CALL BTB [" + (++levelBTB) + "] ON " + this + " WITH PREFIX " + adjPrefix);
            }
            Tree diffLhs = ILUtils.copy(linLhs.diffRefR[adEnv.iReplic]);
            markAsVector(diffLhs, this.isArrayLhs);
            diffLhs.setAnnotation("ExprType", assignedType);
            Tree adjExpr = (adjPrefix == null ? diffLhs
                    : mulProtectedTypedExprs(adjPrefix, diffLhs, true));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("EXIT BTB [" + (levelBTB--) + "] ON " + this + " RETURN " + adjExpr);
            }
            return adjExpr;
        }

        @Override
        public String toString() {
            return "LinAssign:" + linLhs + " := " + toStringOnChildrenNodes(linRhs);
        }
    }

    /**
     * Utility that gets (and removes) the types annotated on "expr1"
     * and "expr2" (annotation "ExprType"), then builds the multiplied
     * expression expr1*expr2, making use of their types, and returns
     * the resulting product Tree annotated with its type.
     *
     * @param mayConjugate When true, insert complex conjugate around "expr1" when complex
     */
    private Tree mulProtectedTypedExprs(Tree expr1, Tree expr2, boolean mayConjugate) {
        TypeSpec type1 = (expr1 == null ? null : expr1.getRemoveAnnotation("ExprType"));
        TypeSpec type2 = (expr2 == null ? null : expr2.getRemoveAnnotation("ExprType"));
        // In adjoint mode, if expr1 is a complex, conjugate it before multiplication:
        if (mayConjugate && type1 != null && type1.isComplexBase()) {
            expr1 = conjugateProtectedExpr(expr1);
        }
        Tree productTree = ILUtils.mulProtectedExprs(expr1, type1, expr2, type2);
        TypeSpec productType = type1 == null ? type2
                : type2 == null ? type1
                : adEnv.curSymbolTable().combineNumeric(type1, type2, productTree);
        if (productTree != null) {
            productTree.setAnnotation("ExprType", productType);
        }
        return productTree;
    }

    /**
     * Conjugates "expr", taking care of the case where "expr" is
     * protected i.e. is an ifExpression. In this case, builds a new protected expr,
     * i.e. with the conjugate pushed inside and the ifExpression pulled outside.
     */
    private Tree conjugateProtectedExpr(Tree expr) {
        String conjugationName = (adEnv.curUnit().isFortran() ? "conjg" : "conj") ;
        if (ILUtils.isIfExpression(expr)) {
            Tree case1 = expr.cutChild(2);
            if (!ILUtils.isNullOrNone(case1)) {
                case1 = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, conjugationName),
                        ILUtils.build(ILLang.op_expressions, case1));
            }
            Tree case2 = expr.cutChild(3);
            if (!ILUtils.isNullOrNone(case2)) {
                case2 = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, conjugationName),
                        ILUtils.build(ILLang.op_expressions, case2));
            }
            return ILUtils.build(ILLang.op_ifExpression,
                    expr.cutChild(1), case1, case2);
        } else {
            return ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, conjugationName),
                    ILUtils.build(ILLang.op_expressions, expr));
        }
    }

    /**
     * Annotates "expr" with a marker indicating whether it is a "vector" (F90 array).
     * This marker is used to move scalar factors out of SUM reductions.
     */
    private void markAsVector(Tree expr, boolean value) {
        expr.setAnnotation("MarkedAsVector", value ? Boolean.TRUE : Boolean.FALSE);
    }

    private void markAsVectorInside(Tree expr) {
        markAsVector(expr, checkIsArrayExpression(expr));
        if (expr.opCode() == ILLang.op_mul ||
                expr.opCode() == ILLang.op_div) {
            markAsVectorInside(expr.down(1));
            markAsVectorInside(expr.down(2));
        } else if (expr.opCode() == ILLang.op_minus ||
                expr.opCode() == ILLang.op_power && expr.down(2).opCode() == ILLang.op_intCst) {
            markAsVectorInside(expr.down(1));
        }
    }

    private boolean removeAllMarksAsVector(Tree expr) {
        if (ILUtils.isNullOrNone(expr)) {
            return false;
        } else {
            Boolean mark = expr.getAnnotation("MarkedAsVector");
            if (mark != null) {
                expr.removeAnnotation("MarkedAsVector");
                return mark == Boolean.TRUE;
            } else if (expr.opCode() == ILLang.op_mul) {
                boolean res1 = removeAllMarksAsVector(expr.down(1));
                boolean res2 = removeAllMarksAsVector(expr.down(2));
                return res1 || res2;
            } else {
                return false;
            }
        }
    }

    /**
     * Build under "parent" (at location "locInParent" when appropriate) the linearization (LinTree) of "expression".
     * Also fills "toPrecomputes" with possible pre-computations of primal sub-expressions into
     * temporary variables, as requested by DiffCutSupport.cutPrimal, and "toLinTrees" with linearizations
     * for split derivatives, as requested by DiffCutSupport.cutAdj.
     *
     * @param expression      the expression we want the partials of.
     * @param parent          inside the currently built partial derivative tree,
     *                        the current node under which the current expression will add new nodes.
     * @param locInParent     when appropriate, to which subset of the parent's children "this" belongs.
     * @param dimensions      the current dimensions of the usage location of "expression". null if scalar.
     * @param iMask           the current mask, i.e. the mask that applies to "expression". null if scalar.
     * @param toLinTrees      the list whose tail will receive all the partial derivative linearization trees built.
     *                        Since toLinTrees will hold a result, it must be initialized as a "hatted" TapList.
     * @param toPrecomputes   list of instructions that precompute common sub-expressions used in the partials.
     *                        Since toPrecomputes will hold a result, it must be initialized as a "hatted" TapList.
     * @param diffSymbolTable the SymbolTable of the differentiated Block that will contain the derivative instructions.
     */
    private void linearize(Tree expression, LinTree parent, int locInParent,
                           TapList<ArrayDim> dimensions, InstructionMask iMask,
                           TapList<LinAssign> toLinTrees, TapList<NewBlockGraphNode> toPrecomputes,
                           SymbolTable diffSymbolTable) {
        WrapperTypeSpec exprTypeSpec = adEnv.curSymbolTable().typeOf(expression);
        // Insert a spread node when an array is expected and a scalar is provided, meaning an implicit SPREAD.
        if (dimensions != null && exprTypeSpec.isScalar()) {
            parent = parent.addLinSpread(dimensions, null, null, locInParent);
            locInParent = -1;
            dimensions = null;
            iMask = null;
        }

        // New term to be multiplied to the current partial:
        Tree deriv;

        // In the special case of a division a/b, we want to modify the partial derivative "so far"
        // that arrives from above, to include the division by b into the partialDerivative:
        if (expression.opCode() == ILLang.op_div) {
            deriv = ILUtils.divProtectedExprs(buildConstantOne(expression, adEnv.curSymbolTable()),
                    cutPrimalIfRequired(expression.down(2), toPrecomputes, iMask, diffSymbolTable));
            parent = parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(expression.down(2)), locInParent);
            locInParent = -1;
        }

        DiffCutSupport cutHere = getCutCosts(expression);
        // If the adjoint of current expression must be put into a temporary variable:
        if (cutHere != null &&
            (cutHere.cutAdj ||
             // Only in tangent, force split of tangent diff of op_power when it needs protections.
             // Strictly speaking, this is not necessary because tangent diff expressions never
             // occur twice (DiffCutSupport.tgtTimes==1), so they need not be placed into a temp.
             // However for op-power, combining protections leads to long code hard to read and bugs.
             // If this is generalized, we may need to revive the cutTgt mechanism,
             // which is currently commented out in findDiffOptimalCuts() :
             (tangentDiff && !(parent instanceof LinAssign) && expression.opCode()==ILLang.op_power && powerNeedsProtection(expression)))) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("SPLITTING ADJOINT OF: " + ILUtils.toString(expression));
            }
            Tree tmpVar = cutHere.tmpVar;
            if (tmpVar == null) {
                NewSymbolHolder tmpVarHolder = cutHere.tmpVarHolder;
                if (tmpVarHolder == null) {
                    TapList<NewSymbolHolder> excludedReuse = blockDifferentiator().inUseNewSymbolHolders ;

                    ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                    tmpVar = adEnv.curBlock.buildTemporaryVariable("temp", expression, diffSymbolTable,
                                                              toNSH, true, excludedReuse, false, null, -1, null) ;
                    tmpVarHolder = toNSH.obj() ;
                    if (tmpVarHolder.zone==-1) {
                        tmpVarHolder.zone = adEnv.allocateTmpZone();
                        adEnv.toActiveTmpZones.tail =
                            new TapIntList(tmpVarHolder.zone, adEnv.toActiveTmpZones.tail);
                    }
                    if (TapEnv.doOpenMP() && adEnv.curBlock.parallelControls != null) {
                        tmpVarHolder.preparePrivateClause(adEnv.curBlock, adEnv.curUnit().privateSymbolTable(), false, true);
                    }

                    cutHere.tmpVarHolder = tmpVarHolder;
                    blockDifferentiator().addInUseNewSymbolHolder(tmpVarHolder);
                }
                tmpVar = tmpVarHolder.makeNewRef(diffSymbolTable);
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("     FINALLY USING NSH " + tmpVarHolder + " TO HOLD DIFF " + toLinTrees.head);
                }
                ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), tmpVar);
                cutHere.tmpVar = tmpVar;
            }
            Tree[] diffTmpVarR = cutHere.tmpAdjVarR ;
            if (diffTmpVarR==null) {
                diffTmpVarR = new Tree[replicas] ;
                for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
                    // Inhibit association-by-address temporarily, because "temp" must still give "tempd":
                    boolean withAA = TapEnv.associationByAddress() ;
                    if (withAA) TapEnv.setAssociationByAddress(false) ;
                    diffTmpVarR[adEnv.iReplic] = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tmpVar, diffSymbolTable,
                                                        false, tmpVar, false, true, tmpVar);
                    NewSymbolHolder diffTmpSymbolHolder =
                        NewSymbolHolder.getNewSymbolHolder(diffTmpVarR[adEnv.iReplic]) ;
                    // Restore association-by-address if inhibited:
                    if (withAA) {
                        diffTmpSymbolHolder.newVariableDecl.setType(cutHere.tmpVarHolder.newVariableDecl.type()) ;
                        TapEnv.setAssociationByAddress(true) ;
                    }
                }
                adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                cutHere.tmpAdjVarR = diffTmpVarR ;
            }
            LinLeaf linUseSplit =
                parent.addLinLeaf(tmpVar, diffTmpVarR, iMask, false, locInParent); // false here means SET diffTmpVar, don't INCR.
            LinLeaf linLhs = new LinLeaf(tmpVar, diffTmpVarR, iMask, false, null, -1); // false here means no RESET diffTmpVar:=0
            LinAssign linAssign = new LinAssign(linLhs, null, exprTypeSpec, exprTypeSpec.isArray(), null, -1);
            linUseSplit.correspondingAssign = linAssign;
            //linLhs.parent = linAssign ; // consistent, but probably useless
            toLinTrees = toLinTrees.placdl(linAssign);
            parent = linAssign;
            locInParent = -1;
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("   -- RESUME BUILDING PARTIALS FROM NEW PARTIALS TREENODE: " + parent);
            }
        }

        switch (expression.opCode()) {
            case ILLang.op_call: {
                String calledName = ILUtils.getCalledNameString(expression);
                Unit calledUnit = DataFlowAnalyzer.getCalledUnit(expression, adEnv.curSymbolTable());
                AtomFuncDerivative funcDerivative = (AtomFuncDerivative) calledUnit.diffInfo;
                if (calledName.equals("spread") && adEnv.curUnit().isFortran()) {
                    Tree[] srcParamExprs = ILUtils.getArguments(expression).children();
                    Tree mainExpr = srcParamExprs[0];
                    if (activeExpr(mainExpr)) {
                        TapList<ArrayDim> argDimensions =
                                adEnv.curSymbolTable().typeOf(mainExpr).getAllDimensions();
                        InstructionMask argMask = new InstructionMask(null, null);
                        if (adEnv.traceCurBlock == 2) {
                            TapEnv.printlnOnTrace("  GOING THROUGH SPREAD. ARG DIMS: " + argDimensions);
                        }
                        Tree splitVar = cutPrimalIfRequired(mainExpr, toPrecomputes, iMask, diffSymbolTable); // not used so far
                        linearize(mainExpr,
                                parent.addLinSpread(dimensions, srcParamExprs[1], srcParamExprs[2], locInParent),
                                -1, argDimensions, argMask, toLinTrees, toPrecomputes, diffSymbolTable);
                    }
                } else if (calledName.equals("sum") && adEnv.curUnit().isFortran()) {
                    Tree[] srcParamExprs = ILUtils.getArguments(expression).children();
                    Tree mainExpr = srcParamExprs[0];
                    if (activeExpr(mainExpr)) {
                        TapList<ArrayDim> argDimensions =
                                adEnv.curSymbolTable().typeOf(mainExpr).getAllDimensions();
                        Tree dim = ILUtils.getOptionalDim(srcParamExprs, adEnv.curSymbolTable());
                        Tree tMask = ILUtils.getOptionalMask(srcParamExprs, adEnv.curSymbolTable());
                        InstructionMask argMask = new InstructionMask(tMask, null);
                        if (adEnv.traceCurBlock == 2) {
                            TapEnv.printlnOnTrace("  GOING THROUGH SUM. ARG DIMS: " + argDimensions + " MASK: " + argMask);
                        }
                        Tree splitVar = cutPrimalIfRequired(mainExpr, toPrecomputes, iMask, diffSymbolTable); // not used so far
                        linearize(mainExpr,
                                parent.addLinReduce(argDimensions, dim, tMask, argMask, locInParent),
                                -1, argDimensions, argMask, toLinTrees, toPrecomputes, diffSymbolTable);
                    }
                } else if (adEnv.curUnit().isFortran() &&
                        ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                          new String[]{"cmplx", "dcmplx", "complex"}, false)) {
                    // Conversion functions with possibly 2 args (e.g. Fortran conversion to COMPLEX type):
                    Tree args = ILUtils.getArguments(expression);
                    Tree argTree1 = args.down(1);
                    Tree argTree2 = args.length() == 1 ? null : args.down(2);
                    if (argTree2 != null && argTree2.opCode() == ILLang.op_nameEq) {
                        argTree2 = null;
                    }
                    if (activeExpr(argTree1) || activeExpr(argTree2)) {
                        parent = parent.addLinConvert(expression, locInParent);
                        if (activeExpr(argTree1)) {
                            Tree splitVar = cutPrimalIfRequired(argTree1, toPrecomputes, iMask, diffSymbolTable); // not used so far
                            linearize(argTree1, parent, 1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                        }
                        if (argTree2 != null && activeExpr(argTree2)) {
                            Tree splitVar = cutPrimalIfRequired(argTree2, toPrecomputes, iMask, diffSymbolTable); // not used so far
                            linearize(argTree2, parent, 2, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                        }
                    }
                } else if (adEnv.curUnit().isFortran()
                           ? ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                          new String[]{"sngl", "dble", "real", "dreal", "aimag", "dimag", "conjg", "dconjg"}, false)
                           : ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                          new String[]{"creal", "cimag", "conj"}, true)) {
                    // Various Fortran conversion functions:
                    Tree argTree = ILUtils.getArguments(expression).down(1);
                    if (activeExpr(argTree)) {
                        Tree splitVar = cutPrimalIfRequired(argTree, toPrecomputes, iMask, diffSymbolTable); // not used so far
                        linearize(argTree, parent.addLinConvert(expression, locInParent), 1,
                                dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                    }
                } else if (funcDerivative == null) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "No derivatives defined for intrinsic function " + calledName);
                } else {
                    funcDerivative.matches(expression);
                    Tree[] paramExprs = funcDerivative.getParamExprs();
                    TapList<TapPair<Tree, Tree>> origKeyTreeList = funcDerivative.origKeyTreeList;
                    Tree origArg;
                    while (origKeyTreeList != null) {
                        origArg = origKeyTreeList.head.second;
                        if (origArg != null) {
                            origArg = cutPrimalIfRequired(origArg, toPrecomputes, iMask, diffSymbolTable);
                            origKeyTreeList.head.second = origArg;
                        }
                        origKeyTreeList = origKeyTreeList.tail;
                    }
                    for (int i = 0; i < paramExprs.length; i++) {
                        if (activeExpr(paramExprs[i])) {
                            Tree argi = ILUtils.getArguments(expression).down(i + 1);
                            deriv = funcDerivative.buildPartialDerivative(i);
                            // Protect partial derivative for SQRT(0.0):
                            if (i == 0 &&
                                    (calledName.equals("sqrt") || calledName.equals("dsqrt")
                                     || calledName.equals("csqrt"))) {
                                // Primal is:  X=SQRT(Y)
                                // Tangent is: if (Y==0) {XD=0} else {XD=YD/(2*SQRT(Y))}
                                //   Strictly speaking, this is wrong, but we prefer a wrong 0.0 to a right NaN !
                                //   TODO: install a runtime warning
                                deriv = protectExpr(deriv,
                                        ILUtils.build(ILLang.op_eq,
                                                cutPrimalIfRequired(argi, toPrecomputes, iMask, diffSymbolTable),
                                                PrimitiveTypeSpec.buildRealConstantZero()));
                                // When "domain of validity" check is activated (option -directValid, in TANGENT_MODE) :
                                if (TapEnv.valid() && adEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
                                    TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
                                    TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
                                    ToObject<Tree> toArg = new ToObject<>(argi);
                                    Tree[] diffArgR = blockDifferentiator().tangentDiffExpr(toArg, null,
                                            refsRW, diffRefsRW);
                                    Tree diffArg = diffArgR[0] ; // temporary during change for replica
                                    if (diffArg != null) {
                                        String typeName = adEnv.curSymbolTable().typeOf(expression).
                                                buildTypeNameForProcName(new TapList<>(null, null), adEnv.curSymbolTable());
                                        Tree validityTest =
                                                ILUtils.buildCall(
                                                        ILUtils.build(ILLang.op_ident, "validity_domain_" + typeName),
                                                        ILUtils.build(ILLang.op_expressions, ILUtils.copy(toArg.obj()), diffArg));
                                        blockDifferentiator().addFuturePlainNodeFwd(
                                                validityTest, adEnv.curInstruction().whereMask(), false,
                                                new TapList<>(argi, refsRW.first), null,
                                                diffRefsRW.first, null, false, false,
                                                "Domain of validity test for sqrt", false);
                                    }
                                }
                                // Protect partial derivative for ASIN(1.0) and ACOS(1.0):
                            } else if (i == 0 &&
                                    (calledName.equals("asin") || calledName.equals("dasin") ||
                                            calledName.equals("acos") || calledName.equals("dacos"))) {
                                // Primal is: X=ASIN(Y)
                                // Tangent is: if (Y==+/-1) {XD = 0.0} else {XD = YD/SQRT(1-Y^2)}
                                //   Strictly speaking, this is wrong, but we prefer a wrong 0.0 to a right NaN !
                                //   TODO: install a runtime warning
                                deriv =
                                        protectExpr(deriv,
                                                ILUtils.build(ILLang.op_or,
                                                        ILUtils.build(ILLang.op_eq,
                                                                cutPrimalIfRequired(argi, toPrecomputes, iMask, diffSymbolTable),
                                                                ILUtils.build(ILLang.op_realCst, "1.0")),
                                                        ILUtils.build(ILLang.op_eq,
                                                                cutPrimalIfRequired(argi, toPrecomputes, iMask, diffSymbolTable),
                                                                ILUtils.build(ILLang.op_realCst, "-1.0"))));
                            }
                            linearize(paramExprs[i], parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(argi), locInParent), -1,
                                    dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                        }
                    }
                }
                break;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_ident:
                Tree[] diffExpressionR = new Tree[replicas] ;  // one per replica
                if (activeExpr(expression)) {
                    for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
                        diffExpressionR[adEnv.iReplic] = varRefDifferentiator().diffVarRef(adEnv.curActivity(), expression, diffSymbolTable,
                                                                varRefDifferentiator().isCurFunctionName(ILUtils.baseName(expression)),
                                                                expression, false, true, expression) ;
                    }
                } else {
                    for (adEnv.iReplic=0 ; adEnv.iReplic<replicas ; ++adEnv.iReplic) {
                        diffExpressionR[adEnv.iReplic] = adEnv.curSymbolTable().typeOf(expression).buildConstantZero();
                    }
                }
                adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                parent = parent.addLinLeaf(expression, diffExpressionR, iMask, true, locInParent); // true here means INCR diffVar, don't SET.
                break;
            case ILLang.op_ifExpression: {
                if (activeExpr(expression.down(2)) || activeExpr(expression.down(3))) {
                    Tree testTree = cutPrimalIfRequired(expression.down(1), toPrecomputes, iMask, diffSymbolTable);
                    parent = parent.addLinIf(testTree, locInParent);
                    linearize(expression.down(2), parent, 1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                    linearize(expression.down(3), parent, 2, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            }
            case ILLang.op_arrayConstructor:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_constructorCall: {
                if (expression.opCode() == ILLang.op_constructorCall) {
                    varRefDifferentiator().pushDiffContext(DiffConstants.INCALL);
                }
                Tree[] expressions;
                if (expression.opCode() == ILLang.op_iterativeVariableRef) {
                    expressions = expression.down(1).children();
                } else if (expression.opCode() == ILLang.op_constructorCall) {
                    expressions = expression.down(3).children();
                } else {
                    expressions = expression.children();
                }
                boolean goInside = false;
                for (int i = expressions.length - 1; i >= 0 && !goInside; --i) {
                    goInside = activeExpr(expressions[i]);
                }
                if (goInside) {
                    parent = parent.addLinCatenate(expression, expressions, locInParent);
                    for (int i = 0; i < expressions.length; ++i) {
                        linearize(expressions[i], parent, i, null, null, toLinTrees, toPrecomputes, diffSymbolTable);
                    }
                }
                if (expression.opCode() == ILLang.op_constructorCall) {
                    varRefDifferentiator().popDiffContext();
                }
                break;
            }
            case ILLang.op_add:
                if (activeExpr(expression.down(1))) {
                    linearize(expression.down(1), parent, locInParent, dimensions, iMask,
                            toLinTrees, toPrecomputes, diffSymbolTable);
                }
                if (activeExpr(expression.down(2))) {
                    linearize(expression.down(2), parent, locInParent, dimensions, iMask,
                            toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_sub:
                if (activeExpr(expression.down(1))) {
                    linearize(expression.down(1), parent, locInParent, dimensions, iMask,
                            toLinTrees, toPrecomputes, diffSymbolTable);
                }
                if (activeExpr(expression.down(2))) {
                    deriv = ILUtils.minusProtectedExpr(buildConstantOne(expression, adEnv.curSymbolTable()));
                    linearize(expression.down(2), parent.addLinStd(deriv, adEnv.integerTypeSpec, locInParent),
                            -1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_mul:
                if (activeExpr(expression.down(1))) {
                    deriv = cutPrimalIfRequired(expression.down(2), toPrecomputes, iMask, diffSymbolTable);
                    linearize(expression.down(1),
                            parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(expression.down(2)), locInParent),
                            -1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                if (activeExpr(expression.down(2))) {
                    deriv = cutPrimalIfRequired(expression.down(1), toPrecomputes, iMask, diffSymbolTable);
                    linearize(expression.down(2), parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(expression.down(1)), locInParent),
                            -1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_div:
                // Warning: the following is not a bug :
                // parent has already received division by expression.down(2) at the beginning of this method !
                if (activeExpr(expression.down(1))) {
                    linearize(expression.down(1), parent, locInParent, dimensions, iMask,
                            toLinTrees, toPrecomputes, diffSymbolTable);
                }
                if (activeExpr(expression.down(2))) {
                    deriv = ILUtils.minusProtectedExpr(cutPrimalIfRequired(expression, toPrecomputes, iMask, diffSymbolTable));
                    linearize(expression.down(2), parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(expression), locInParent), -1,
                            dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_mod:
                // d(a%b)/da = 1
                if (activeExpr(expression.down(1))) {
                    linearize(expression.down(1), parent, locInParent, dimensions, iMask,
                            toLinTrees, toPrecomputes, diffSymbolTable);
                }
                // d(a%b)/db = -a/b
                if (activeExpr(expression.down(2))) {
                    deriv = ILUtils.minusProtectedExpr(
                            ILUtils.divProtectedExprs(
                                    cutPrimalIfRequired(expression.down(1), toPrecomputes, iMask, diffSymbolTable),
                                    cutPrimalIfRequired(expression.down(2), toPrecomputes, iMask, diffSymbolTable)));
                    linearize(expression.down(2), parent.addLinStd(deriv, adEnv.curSymbolTable().typeOf(expression.down(1)), locInParent), -1,
                            dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_minus:
                if (activeExpr(expression.down(1))) {
                    deriv = ILUtils.minusProtectedExpr(buildConstantOne(expression, adEnv.curSymbolTable()));
                    linearize(expression.down(1), parent.addLinStd(deriv, adEnv.integerTypeSpec, locInParent), -1,
                            dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                if (activeExpr(expression.down(2))) {
                    linearize(expression.down(2), parent.addLinConvert(expression, locInParent), 1,
                            dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            case ILLang.op_label:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_neq:
            case ILLang.op_eq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_not:
            case ILLang.op_bitNot:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_address:
            case ILLang.op_complexConstructor:
            case ILLang.op_sizeof:
            case ILLang.op_allocate:
                break;
            case ILLang.op_power: {
                // Primal is: r=x^y
                // Tangent is:
                // if (x<=0 && (y==0 || y!=INT(y))) {
                //     rd = 0.0
                // } else if (x<=0) {
                //     rd = y*x^(y-1)*xd
                // } else {
                //     rd = y*x^(y-1)*xd + r*LOG(x)*yd
                // }
                //   Strictly speaking, this is wrong, but we prefer a wrong 0.0 to a right NaN !
                Tree xTree =
                        cutPrimalIfRequired(expression.down(1), toPrecomputes, iMask, diffSymbolTable);
                if (activeExpr(expression.down(1))) {
                    Tree diffPower = xTree;
                    if (ILUtils.isExpressionNumConstant(expression.down(2))) {
                        // Suppose tree is R=X^N, with N a numerical constant. dR/dX = N*X^(N-1)
                        // We don't test on (X>0 and isInteger(N)) because if not, dR/dX crashes
                        // but R also crashes, so it's fine (garbage in, garbage out).
                        Tree newPower = ILUtils.addExpressionsNumConstants(expression.down(2),
                                            ILUtils.build(ILLang.op_intCst, -1));
                        if (!ILUtils.evalsToOne(newPower)) {
                            diffPower = ILUtils.powerTree(diffPower, newPower);
                        }
                    } else {
                        // Suppose tree is R=X^Y. In general, dR/dX = Y*X^(Y-1), except in the special case
                        // where (X<=0 AND (Y==0 OR not_integer(Y))) in which case we shall assume dR/dX = 0.0
                        Tree yTree =
                            cutPrimalIfRequired(expression.down(2), toPrecomputes, iMask, diffSymbolTable);
                        diffPower =
                            ILUtils.powerTree(diffPower,
                                    ILUtils.buildSmartAddSub(yTree, -1, ILUtils.build(ILLang.op_intCst, 1)));
                        if (!adEnv.curSymbolTable().typeOf(expression).isComplexBase()) {
                            Tree intTree;
                            if (adEnv.curUnit().isFortran()) {
                                intTree = ILUtils.buildCall(
                                            ILUtils.build(ILLang.op_ident, "INT"),
                                            ILUtils.build(ILLang.op_expressions,
                                              ILUtils.copy(yTree)));
                            } else {
                                intTree = ILUtils.build(ILLang.op_cast,
                                            ILUtils.build(ILLang.op_ident, "int"),
                                            ILUtils.copy(yTree));
                            }
                            Tree testExpInPower =
                                ILUtils.build(ILLang.op_and,
                                        ILUtils.build(ILLang.op_le,
                                                ILUtils.copy(xTree),
                                                PrimitiveTypeSpec.buildRealConstantZero()),
                                        ILUtils.build(ILLang.op_or,
                                                ILUtils.build(ILLang.op_eq,
                                                        ILUtils.copy(yTree),
                                                        PrimitiveTypeSpec.buildRealConstantZero()),
                                                ILUtils.build(ILLang.op_neq,
                                                        ILUtils.copy(yTree),
                                                        intTree)));
                            diffPower = protectExpr(diffPower, testExpInPower);
                        }
                    }
                    TypeSpec type1 = adEnv.curSymbolTable().typeOf(expression.down(2));
                    TypeSpec type2 = adEnv.curSymbolTable().typeOf(expression.down(1));
                    deriv = ILUtils.mulProtectedExprs(
                            cutPrimalIfRequired(expression.down(2), toPrecomputes, iMask, diffSymbolTable),
                            type1,
                            diffPower,
                            type2);
                    TypeSpec derivType = adEnv.curSymbolTable().combineNumeric(type1, type2, deriv);
                    linearize(expression.down(1), parent.addLinStd(deriv, derivType, locInParent),
                            -1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                if (activeExpr(expression.down(2))) {
                    // Suppose tree is R=X^Y. In general, dR/dY = Ln(X)*X^Y, except in the special case
                    // where X<=0, in which case we shall assume dR/dY = 0.0
                    Tree inLogExp = ILUtils.copy(xTree);
                    Tree testExpInLog = null;
                    if (!ILUtils.evalsToGTZero(inLogExp)
                            && !adEnv.curSymbolTable().typeOf(expression).isComplexBase()) {
                        testExpInLog = ILUtils.build(ILLang.op_le,
                                ILUtils.copy(inLogExp),
                                PrimitiveTypeSpec.buildRealConstantZero());
                    }
                    Tree subDerivTree =
                            ILUtils.buildCall(
                                    ILUtils.build(ILLang.op_ident, "log"),
                                    ILUtils.build(ILLang.op_expressions, inLogExp));
                    TypeSpec type1 = adEnv.curSymbolTable().typeOf(expression);
                    TypeSpec type2 = adEnv.realTypeSpec;
                    deriv = ILUtils.mulProtectedExprs(
                            cutPrimalIfRequired(expression, toPrecomputes, iMask, diffSymbolTable),
                            type1,
                            testExpInLog == null ? subDerivTree : protectExpr(subDerivTree, testExpInLog),
                            type2);
                    TypeSpec derivType = adEnv.curSymbolTable().combineNumeric(type1, type2, deriv);
                    linearize(expression.down(2), parent.addLinStd(deriv, derivType, locInParent),
                            -1, dimensions, iMask, toLinTrees, toPrecomputes, diffSymbolTable);
                }
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(Linearize expression) Unexpected operator: " + expression.opName() + " in " + ILUtils.toString(expression));
                break;
        }
    }

    private boolean checkIsArrayExpression(Tree expr) {
        WrapperTypeSpec exprType = adEnv.curSymbolTable().typeOf(expr);
        return exprType != null && exprType.isArray();
    }

    private boolean powerNeedsProtection(Tree expression) {
        return ((activeExpr(expression.down(1))
                 && !ILUtils.isExpressionNumConstant(expression.down(2)))
                ||
                (activeExpr(expression.down(2))
                 && !ILUtils.evalsToGTZero(expression.down(1))))
            && !adEnv.curSymbolTable().typeOf(expression).isComplexBase() ;
    }

    /**
     * Used to stop linearization inside sub-expressions that are passive.
     */
    private boolean activeExpr(Tree expression) {
        DiffCutSupport cutHere = getCutCosts(expression);
        return cutHere != null && cutHere.isActive;
    }

    /**
     * Copy expression "expression" for the Jacobian, taking care of "CutCosts"
     * directives that require cutting sub-expressions. Recursively in exp, each
     * time a "CutCosts" annotation tells that the current expression must be put
     * into a temporary variable, do it and return the temporary variable.
     *
     * @param expression      the expression we want to copy.
     * @param toPrecomputes   list of instructions that precompute common sub-expressions used in the partials.
     *                        Since toPrecomputes will hold a result, it must be initialized as a "hatted" TapList.
     * @param exprMask        The vectorial mask of the usage context of "expression".
     * @param diffSymbolTable the SymbolTable of the differentiated Block that will contain the derivative instructions.
     */
    private Tree cutPrimalIfRequired(Tree expression, TapList<NewBlockGraphNode> toPrecomputes,
                                     InstructionMask exprMask, SymbolTable diffSymbolTable) {
        Operator operator = expression.operator();
        Tree copiedExp;
        if (!expression.isAtom()) {
            copiedExp = operator.tree();
            Tree[] subTrees = expression.children();
            for (int i = subTrees.length - 1; i >= 0; i--) {
                copiedExp.setChild(
                        cutPrimalIfRequired(subTrees[i], toPrecomputes, exprMask, diffSymbolTable),
                        i + 1);
            }
        } else {
            copiedExp = ILUtils.copy(expression);
            copiedExp.removeAnnotation("CutCosts");
        }
        NewSymbolHolder.copySymbolHolderAnnotation(expression, copiedExp, null);
        DiffCutSupport cutHere = getCutCosts(expression);
        if (cutHere != null && cutHere.cutPrimal) {
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("SPLITTING PRIMAL: " + ILUtils.toString(expression));
            }
            if (!cutHere.tmpPrimalVarInitialized) {
                WrapperTypeSpec exprTypeSpec = adEnv.curSymbolTable().typeOf(expression);
                Tree tmpVar = cutHere.tmpVar;
                NewSymbolHolder tmpVarHolder;
                if (tmpVar == null) {
                    tmpVarHolder = cutHere.tmpVarHolder;
                    if (tmpVarHolder == null) {

                        ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                        TapList<NewSymbolHolder> excludedReuse = blockDifferentiator().inUseNewSymbolHolders ;
                        tmpVar = adEnv.curBlock.buildTemporaryVariable("temp", expression, diffSymbolTable,
                                                              toNSH, true, excludedReuse, true, null, -1, null) ;
                        tmpVarHolder = toNSH.obj() ;
                        if (tmpVarHolder.zone==-1) {
                            tmpVarHolder.zone = adEnv.allocateTmpZone();
                            adEnv.toActiveTmpZones.tail =
                                new TapIntList(tmpVarHolder.zone, adEnv.toActiveTmpZones.tail);
                        }
                        cutHere.tmpVarHolder = tmpVarHolder;
                        blockDifferentiator().addInUseNewSymbolHolder(tmpVarHolder);
                    }
                } else {
                    tmpVarHolder = NewSymbolHolder.getNewSymbolHolder(tmpVar);
                }
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("     FINALLY USING NSH " + tmpVarHolder + " TO HOLD " + copiedExp);
                }
                tmpVarHolder.solvingLevelMustInclude(diffSymbolTable);
                tmpVarHolder.declarationLevelMustInclude(diffSymbolTable);
                tmpVar = tmpVarHolder.makeNewRef(diffSymbolTable);
                ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), tmpVar);
                cutHere.tmpVar = tmpVar;

                copiedExp = rebalanceExp(copiedExp, null, null, new ToBool(false), 0);
                if (TapEnv.associationByAddress()) {
                    copiedExp =
                        blockDifferentiator().turnAssociationByAddressPrimal(copiedExp, adEnv.curSymbolTable(),
                                                                             adEnv.curFwdSymbolTable, null, true);
                }
                if (adEnv.traceCurBlock == 2) {
                    TapEnv.printlnOnTrace("   -- PRECOMPUTING " + ILUtils.toString(copiedExp) + " INTO " + tmpVar);
                }
                Tree setSplitVar =
                        ILUtils.build(ILLang.op_assign, tmpVar, replacePrecomputedAndCustomize(copiedExp));
                NewBlockGraphNode futureNode =
                        new NewBlockGraphNode(DiffConstants.PLAIN, setSplitVar, false, adEnv.curInstruction(), exprTypeSpec.isScalar() ? null : exprMask);
                toPrecomputes.placdl(futureNode);
                cutHere.tmpPrimalVarInitialized = true;
            }
            copiedExp = ILUtils.copy(cutHere.tmpVar);
            markAsVector(copiedExp, checkIsArrayExpression(expression));
            if (adEnv.traceCurBlock == 2) {
                TapEnv.printlnOnTrace("   -- USING PRECOMPUTED SUB-PRIMAL " + copiedExp);
            }
        }
        return copiedExp;
    }

    private Tree buildConstantOne(Tree expression, SymbolTable origSymbolTable) {
        WrapperTypeSpec typeSpec = origSymbolTable.typeOf(expression);
        Tree oneCstTree = null;
        if (typeSpec != null && typeSpec.wrappedType != null) {
            oneCstTree = typeSpec.wrappedType.buildConstantOne();
        }
        if (oneCstTree == null) {
            oneCstTree = ILUtils.build(ILLang.op_realCst, "1.0");
        }
        return oneCstTree;
    }

    /**
     * Create a protected factor, which will appear in a future product of partial derivatives.
     * One may see this as building: (test?0.0:factor).
     * When there is a protected factor, then the diff instruction
     * will check if the "test" is true. If it is true, then the
     * product of partial derivatives is considered equal to zero
     * Otherwise, the derivative uses the product of partial
     * derivatives in the standard manner.
     * If the test is null, the factor will be protected too, and
     * the final diff instruction will check whether the incoming
     * adjoint derivative is equal to 0.0.
     */
    private static Tree protectExpr(Tree factor, Tree test) {
        if (factor.opCode() != ILLang.op_ifExpression) {
            factor = ILUtils.build(ILLang.op_ifExpression, ILUtils.build(ILLang.op_none), null, factor);
        }
        if (test != null) {
            Tree curTest = factor.cutChild(1);
            if (curTest.opCode() == ILLang.op_none) {
                curTest = test;
            } else {
                curTest = ILUtils.build(ILLang.op_or, test, curTest);
            }
            factor.setChild(curTest, 1);
        }
        return factor;
    }
}
