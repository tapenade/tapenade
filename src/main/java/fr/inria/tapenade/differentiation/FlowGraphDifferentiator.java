/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.InOutAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.representation.AlignmentBoundary;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.BasicBlock;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.BlockStorage;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.EntryBlock;
import fr.inria.tapenade.representation.ExitBlock;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.ModifiedTypeSpec;
import fr.inria.tapenade.representation.FunctionTypeSpec;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.IterDescriptor;
import fr.inria.tapenade.representation.LoopBlock;
import fr.inria.tapenade.representation.MemoryMaps;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.RefDescriptor;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TemporaryBlock;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.*;

// TODOs:
//  -- Nouveau mode -db au niveau d'une Unit
//  -- Lutter contre tous les cas ou une fonctionnalite a
//     ete faite seulement pour le tangent et pas l'adjoint, ou vice-versa
//     (par exemple Assoc-by-Address ou splitForADReverse !)
//  -- Revoir l'index de stockage des NSH differenties dans *etDiffSymbolHolder(index)
//  -- Resoudre le probleme de l'activite qui depend un peu du mode Tgt vs Adj.
//  -- Refaire marcher le profiling de B. Dauvergne.
//  -- Refaire marcher AdolC, peut-etre en le supprimant/mergeant avec AssocByAddress.
//  -- Quand il n'y aura plus de regression, remettre en place la nouvelle
//     politique de nommage des diffs de modules, copies, types, etc...
//  -- Quand il n'y aura plus de regression, brancher le topo-sort des
//     instructions du fwdBlock en mode adjoint.
//  -- on 77:lh06, 77:lh99 77:lha28 77:lha46 77:v96, remove all PUSH/POPs that cancel upon
//     TURN, and useless control PUSHes on the exits of a NATURAL_LOOP.
//  -- fix the spurious GOTOs put by treeGen, e.g. on 77:lh06
//  -- detect why test "POPCONTROL1B(branch) line 99 in 77:lh06 is useless.
//  -- build a nonRegr test with a really wild loop.
//  -- connections made directly with *checkFreeThenRedirectDestination() seem less
//     complete than those made during connectFwdBwd()
//     => Improve, in the style of connectBwd() (or connectFwd()?).
//  -- check if all is needed in the FlowGraphDifferentiationEnv's and FlowGraphLevel's
//  -- invent a class for oneFwdBwd objects. Simplify and take useless fields out.
//  -- generalize class "FwdBwdVarTool" to handle any type and
//        manage all NewSymbolHolder and RefDescriptor.

/**
 * Differentiator for the flow of control given as a FlowGraph.
 */
public final class FlowGraphDifferentiator {

    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    /**
     * @return the global adEnv.callGraphDifferentiator.
     */
    private CallGraphDifferentiator callGraphDifferentiator() {
        return adEnv.callGraphDifferentiator;
    }

    /**
     * @return the global adEnv.blockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * @return the global adEnv.procedureCallDifferentiator.
     */
    private ProcedureCallDifferentiator procedureCallDifferentiator() {
        return adEnv.procedureCallDifferentiator;
    }

    /**
     * @return the global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /**
     * @return the global adEnv.multiDirMode.
     */
    private boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * @return the global adEnv.suffixes.
     */
    private String[][] suffixes() {
        return adEnv.suffixes;
    }

    /**
     * @return the global TapEnv.get().activityAnalyzer.
     */
    private ADActivityAnalyzer activityAnalyzer() {
        return TapEnv.adActivityAnalyzer();
    }

    /**
     * @return the global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * @return the global adEnv.curFwdDiffUnit.
     */
    private Unit curFwdDiffUnit() {
        return adEnv.curFwdDiffUnit;
    }

    /**
     * @return the global adEnv.curDiffUnitSort.
     */
    private int curDiffUnitSort() {
        return adEnv.curDiffUnitSort;
    }

    /**
     * @return the global adEnv.curDiffVarSort.
     */
    private int curDiffVarSort() {
        return adEnv.curDiffVarSort;
    }

    /**
     * Container for the NewSymbolHolder of the integer variable
     * used to store the POP'ped branch, plus its future type.
     */
    protected TapPair<NewSymbolHolder, WrapperTypeSpec> toBranchVariable;

    /**
     * Container for the NewSymbolHolder of the integer variable
     * used to store the allocated size.
     */
    protected TapList<NewSymbolHolder> toChunklengthVariable;

    /**
     * For ADJOINT_SPLIT_MODE, container for the NewSymbolHolder of the integer variable
     * used to store the allocated size in the split FWD sweep.
     * For other modes, this container is equal to toChunklengthVariable.
     */
    protected TapList<NewSymbolHolder> toFwdChunklengthVariable;

    /**
     * Container for the NewSymbolHolder of the address variable
     * used to store the address allocated during the forward sweep.
     */
    protected TapList<NewSymbolHolder> toChunkoldVariable;

    /**
     * Container for the NewSymbolHolder of the address variable
     * used to store the DIFF address allocated during the forward sweep.
     */
    protected TapList<NewSymbolHolder> toDiffchunkoldVariable;

    /**
     * Temporary variable(s) that hold OpenMP's chunk  start index value.
     */
    private FwdBwdVarTool adOmpChunkStartVarTool;

    /**
     * Temporary variable(s) that hold OpenMP's chunk end index value.
     */
    private FwdBwdVarTool adOmpChunkEndVarTool;

    /**
     * The number of OpenMP chunks.
     */
    private NewSymbolHolder ompChunksNumVariable;

    /**
     * The loop index over the OpenMP chunks.
     */
    private NewSymbolHolder ompChunksIndexVariable;

    /**
     * The primal loop index. To ensure it is declared "private".
     */
    private Tree ompChunksOriginalIndex;

    /**
     * For ADJOINT_SPLIT_MODE fwd loop, the primal loop index. To ensure it is declared "private".
     */
    private Tree ompChunksOriginalIndexFwd;

    /** Proposed names for special new variables */
    private String adToStr, adFromStr, adStrideStr, adCountStr, adOmpChunkStartStr, adOmpChunkEndStr;

    /**
     * True when we are in a (debug) mode such that empty diff Blocks must be kept.
     */
    protected boolean keepEmptyDiffBlocks;

    /**
     * True when the static analysis of curUnit has been altered to
     * analyze checkpointed sub-FlowGraphLevels.
     */
    private boolean alteredStaticAnalysis;

    /**
     * Counter used by BlockDifferentiator to number the various debug points.
     */
    protected int debugPointsCounter;

    /**
     * Debug memo of all FGArrow that have been searched for activity switches.
     * In general, all arrows must have been searched at the end of (tangent) diff.
     */
    private BoolVector tangentDiffReinitChecker;

    /**
     * ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
     */
    private TapList<TapPair<Tree, Tree>> toDuplicateVars;

    /**
     * The association from each Block of the current source Unit towards its immediately
     * enclosing FlowGraphLevel, which must be of levelKind "PLAIN_BLOCK".
     */
    private BlockStorage<FlowGraphLevel> blockToPlainLevel;
    /**
     * When new temporary source Block's and FlowGraphLevel's have been added to the
     * FlowGraphLevel's, this contains the corresponding additional correspondence
     * from new source Block to its immediately enclosing new FlowGraphLevel.
     */
    private TapList<TapPair<Block, FlowGraphLevel>> additionalBlockToPlainLevel;

    /**
     * For ADJOINT diff of a function Unit, communicates the new temporary variable
     * used to hold the returned value. Variable is created in BlockDifferentiator, and used here.
     */
    protected Tree returnedVariable;

    protected FlowGraphDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
    }

    /**
     * Differentiate the procedure "unit", i.e. for each differentiation mode required,
     * fill the empty diffUnit that has been created (empty) beforehand.
     */
    protected void differentiateProcedure() {
        Unit unit = adEnv.curActivity().unit();
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" ================== DIFFERENTIATION OF UNIT " + unit.name() + " : ==================");
            TapEnv.printlnOnTrace("                    FOR ACTIVITY PATTERN:" + adEnv.curActivity());
            TapEnv.setTraceIndent(0);
        }
        // Create control variables names:
        if (unit.publicSymbolTable().isCaseDependent()) {
            adFromStr = "adFrom";
            adStrideStr = "adStride";
            adToStr = "adTo";
            adCountStr = "adCount";
            adOmpChunkStartStr = "chunkStart";
            adOmpChunkEndStr = "chunkEnd";
        } else {
            adFromStr = "ad_from";
            adStrideStr = "ad_stride";
            adToStr = "ad_to";
            adCountStr = "ad_count";
            adOmpChunkStartStr = "chunk_start";
            adOmpChunkEndStr = "chunk_end";
        }

        adEnv.unitUsefulnesses = adEnv.curActivity().usefulnesses();
        adEnv.unitActivities = adEnv.curActivity().activities();
        adEnv.unitReqXs = adEnv.curActivity().reqXs();
        adEnv.unitAvlXs = adEnv.curActivity().avlXs();
        if (TapEnv.adTbrAnalyzer() != null) {
            adEnv.unitTBRs = adEnv.curActivity().tbrs();
            adEnv.unitRecomputations = adEnv.curActivity().recomps();
        } else {
            adEnv.unitTBRs = null;
            adEnv.unitRecomputations = null;
        }
        adEnv.setCurSymbolTable(unit.publicSymbolTable());
        adEnv.hiddenPushPopVariables.tail = null;
        adEnv.hiddenInstrumentedVariables.tail = null;

        blockToPlainLevel = new BlockStorage<>(unit);
        additionalBlockToPlainLevel = null;
        FlowGraphLevel topFlowGraphLevel =
                buildTreeOfDiffLevels(new TapList<>(unit.entryBlock(), unit.topBlocks()), DiffConstants.TOP_LEVEL);
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace(" ----------- Tree of FlowGraphLevel's: --------------------------");
            tracePrintFlowGraphLevel(topFlowGraphLevel, 0);
            TapEnv.printlnOnTrace();
        }
        if (TapEnv.mustTangent()) {
            callGraphDifferentiator().unitDiffTimer.reset();
            TapEnv.printOnTrace(15, "@@ Tangent" + (adEnv.curActivity().isContext() ? "-context" : "") + " differentiation of procedure: " + unit.name() + " for activity pattern " + adEnv.curActivity());
            adEnv.setCurDiffSorts(DiffConstants.TANGENT, DiffConstants.TANGENT);
            keepEmptyDiffBlocks = TapEnv.debugAdMode() == TapEnv.TGT_DEBUG || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG;
            differentiateProcedureInMode(DiffConstants.TANGENT_MODE, topFlowGraphLevel);
            TapEnv.printlnOnTrace(15, ", time " + callGraphDifferentiator().unitDiffTimer.elapsed() + " s");
        }
        if (TapEnv.mustAdjoint()) {
            adEnv.setCurDiffSorts(DiffConstants.ADJOINT, DiffConstants.ADJOINT);
            if (TapEnv.doOpenMP()) { // trigger FormAD analysis for current Unit:
                TapEnv.multithreadAnalyzer().analyzeSharedAdjointConflicts(adEnv.curUnit(), adEnv.curActivity(), adEnv.callGraphDifferentiator) ;
            }
            keepEmptyDiffBlocks = !adEnv.curActivity().isContext() &&
                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || !TapEnv.removeDeadControl() || !TapEnv.removeDeadPrimal());
            if (unit.mustDifferentiateSplit() && !adEnv.curActivity().isContext()) {
                callGraphDifferentiator().unitDiffTimer.reset();
                TapEnv.printOnTrace(15, "@@ Reverse differentiation (split) of procedure: " + unit.name() + " for activity pattern " + adEnv.curActivity());
                differentiateProcedureInMode(DiffConstants.ADJOINT_SPLIT_MODE, topFlowGraphLevel);
                TapEnv.printlnOnTrace(15, ", time " + callGraphDifferentiator().unitDiffTimer.elapsed() + " s");
            }
            // By default, topUnit's are differentiated in JOINT mode.
            if (unit.mustDifferentiateJoint() || !unit.mustDifferentiateSplit() || adEnv.curActivity().isContext()) {
                callGraphDifferentiator().unitDiffTimer.reset();
                TapEnv.printOnTrace(15, "@@ Reverse" + (adEnv.curActivity().isContext() ? "-context" : "") + " differentiation" + (adEnv.curActivity().isContext() ? "" : " (joint)") + " of procedure: " + unit.name() + " for activity pattern " + adEnv.curActivity());
                differentiateProcedureInMode(DiffConstants.ADJOINT_MODE, topFlowGraphLevel);
                TapEnv.printlnOnTrace(15, ", time " + callGraphDifferentiator().unitDiffTimer.elapsed() + " s");
            }
        }
        summarizeHiddenVariables(adEnv.hiddenDiffInitVariables, "initialize derivative of");
        summarizeHiddenVariables(adEnv.hiddenPushPopVariables, "save");
        summarizeHiddenVariables(adEnv.hiddenInstrumentedVariables, "instrument");
        blockToPlainLevel = null;
        additionalBlockToPlainLevel = null;
    }

    private void summarizeHiddenVariables(TapList<TapPair<Object, TapList<String>>> toHiddenVariables, String actionName) {
        Object key;
        TapList<String> rootNames;
        String fromPlace;
        String varNames;
        while (toHiddenVariables.tail != null) {
            rootNames = toHiddenVariables.tail.head.second;
            if (rootNames != null) {
                key = toHiddenVariables.tail.head.first;
                if (key instanceof String) {
                    fromPlace = "COMMON " + key;
                } else if (key instanceof Unit) {
                    if (((Unit) key).isModule()) {
                        fromPlace = "MODULE " + ((Unit) key).name();
                    } else if (((Unit) key).isStandard()) {
                        fromPlace = "PROCEDURE " + ((Unit) key).name();
                    } else {
                        fromPlace = "UNIT " + ((Unit) key).name();
                    }
                } else {
                    fromPlace = "?? " + key;
                }
                varNames = "";
                while (rootNames != null) {
                    if (!rootNames.head.equals("%all_io_streams%")
                        && !CallGraph.isMessagePassingChannelName(rootNames.head)) {
                        varNames = varNames + rootNames.head + ", ";
                    }
                    rootNames = rootNames.tail;
                }
                if (!varNames.isEmpty()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(AD14) Summary: differentiated procedure of " + adEnv.curUnit().name() + " needs to " + actionName + " hidden variables " + varNames + "from " + fromPlace);
                }
            }
            toHiddenVariables.tail = toHiddenVariables.tail.tail;
        }
    }

    // BEGIN PART ABOUT BUILDING THE TREE OF NESTED STRUCTURES FOR DIFFERENTIATION:

    /**
     * Build the tree of nested FlowGraphLevel's that define the structure of the
     * adjoint Unit. These FlowGraphLevel's follow nested loops, checkpoints,
     * and more generally every structure that deserves a special differentiation,
     * Also fills the BlockStorage "blockToPlainLevel" that goes from
     * each Block of the current Unit to its enclosing FlowGraphLevel.
     * WILD_LOOP's are discarded: they are inlined in their enclosing FlowGraphLevel.
     *
     * @param levelBlocks The list of Blocks to be placed at the current top level.
     * @param levelKind   The kind of the FlowGraphLevel to build, e.g. DO_LOOP, CHECKPOINT...
     * @return The complete FlowGraphLevel for this top level and all levels below.
     */
    private FlowGraphLevel buildTreeOfDiffLevels(TapList<Block> levelBlocks, int levelKind) {
        FlowGraphLevel result = new FlowGraphLevel(levelKind);
        Block block = levelBlocks.head;
        result.entryPoint =
                new FlowGraphLevel(block instanceof EntryBlock ? DiffConstants.ENTRY_BLOCK : DiffConstants.PLAIN_BLOCK);
        result.entryPoint.entryBlock = block;
        result.entryPoint.entryArrows = block.backFlow();
        result.entryPoint.exitArrows = block.flow();
        result.entryPoint.enclosing = result;
        result.entryPoint.trueEnclosing = result;
        result.entryBlock = block;
        blockToPlainLevel.store(block, result.entryPoint);
        levelBlocks = levelBlocks.tail;
        TapList<FlowGraphLevel> toContents = new TapList<>(null, null);
        TapList<FlowGraphLevel> tlContents = toContents;
        FlowGraphLevel subLevel;
        int subLevelKind;
        while (levelBlocks != null) {
            block = levelBlocks.head;
            // block is either a Block or a LoopBlock.
            if (block instanceof LoopBlock) {
                block = ((LoopBlock) block).inside.head;
            }
            // block is either a Block or a HeaderBlock.
            subLevelKind = findBlockLoopKind(block, adEnv.curUnitIsContext);
            if (subLevelKind == DiffConstants.PLAIN_BLOCK || subLevelKind == DiffConstants.WILD_LOOP) {
                subLevel = new FlowGraphLevel(DiffConstants.PLAIN_BLOCK);
                subLevel.entryBlock = block;
                subLevel.entryArrows = block.backFlow();
                subLevel.exitArrows = block.flow();
                blockToPlainLevel.store(block, subLevel);
                if (subLevelKind == DiffConstants.WILD_LOOP) {
                    levelBlocks = TapList.append(block.enclosingLoop().inside,
                            levelBlocks.tail);
                }
            } else {
                subLevel =
                        buildTreeOfDiffLevels(block.enclosingLoop().inside, subLevelKind);
            }
            subLevel.enclosing = result;
            subLevel.trueEnclosing = result;
            tlContents = tlContents.placdl(subLevel);
            levelBlocks = levelBlocks.tail;
        }
        result.contents = toContents.tail;
        insertLevelStructuredLevels(result);
        if (!adEnv.curUnitIsContext) insertLevelStartEndLevelsRec(result);
        collectLevelEntryExitArrows(result);
        return result;
    }

    /**
     * Refine the current level "containerLevel" of the tree of FlowGraphLevel's,
     * by inserting additional levels for non-loop syntactic structures that are
     * represented by a node in the AST (e.g. op_parallelRegion...).
     */
    private void insertLevelStructuredLevels(FlowGraphLevel containerLevel) {
        TapList<FlowGraphLevel> flatContents = containerLevel.contents;
        containerLevel.contents = null;
        for (TapList<FlowGraphLevel> inContents = flatContents;
             inContents != null;
             inContents = inContents.tail) {
            Block block = inContents.head.entryBlock;
            TapList<Instruction> blockControls = block.parallelControls;
            FlowGraphLevel controllerLevel =
                    getSetParallelLevels(blockControls, containerLevel, containerLevel.entryBlock.parallelControls);
            if (block.isParallelController()) {
                getSetParallelLevel(block, controllerLevel);
            } else {
                addOtherLevel(inContents.head, controllerLevel);
            }
        }
    }

    private FlowGraphLevel getSetParallelLevels(
            TapList<Instruction> blockControls,
            FlowGraphLevel containerLevel,
            TapList<Instruction> outsideBlockControls) {
        if (blockControls != outsideBlockControls) {
            containerLevel = getSetParallelLevels(blockControls.tail, containerLevel, outsideBlockControls);
            containerLevel = getSetParallelLevel(blockControls.head.block, containerLevel);
        }
        return containerLevel;
    }

    private FlowGraphLevel getSetParallelLevel(
            Block controlBlock,
            FlowGraphLevel containerLevel) {
        TapList<FlowGraphLevel> toContents =
                new TapList<>(null, containerLevel.contents);
        TapList<FlowGraphLevel> inContents = toContents;
        FlowGraphLevel found = null;
        while (found == null && inContents.tail != null) {
            if (inContents.tail.head.entryBlock == controlBlock) {
                found = inContents.tail.head;
            }
            inContents = inContents.tail;
        }
        if (found == null) {
            found = new FlowGraphLevel(DiffConstants.MULTITHREAD_REGION);
            found.entryPoint = blockToPlainLevel.retrieve(controlBlock);
            found.entryPoint.enclosing = found;
            found.entryPoint.trueEnclosing = found;
            found.entryBlock = controlBlock;
            found.enclosing = containerLevel;
            found.trueEnclosing = containerLevel;
            inContents.placdl(found);
            containerLevel.contents = toContents.tail;
        }
        return found;
    }

    private void addOtherLevel(
            FlowGraphLevel otherLevel,
            FlowGraphLevel containerLevel) {
        if (!TapList.contains(containerLevel.contents, otherLevel)) {
            containerLevel.contents = TapList.addLast(containerLevel.contents, otherLevel);
            otherLevel.enclosing = containerLevel;
            otherLevel.trueEnclosing = containerLevel;
        }
    }

    private boolean oneExitOutside(LoopBlock loopBlock) {
        TapList<FGArrow> exitFlow = loopBlock.flow() ;
        boolean exitsOutside = false ;
        while (exitFlow!=null && !exitsOutside) {
            exitsOutside = (exitFlow.head.topmostPop()!=loopBlock) ;
            exitFlow = exitFlow.tail ;
        }
        return exitsOutside ;
    }

    /**
     * @return the kind of differentiation strategy for the loop headed by block. Result in
     * {PLAIN_BLOCK, WILD_LOOP, NATURAL_LOOP, TIMES_LOOP, WHILE_LOOP, DO_LOOP,
     * II_LOOP, FIXEDPOINT_LOOP}. BINOMIAL_LOOP's will be detected later.
     */
    private int findBlockLoopKind(Block block, boolean unitIsContext) {
        int kind = DiffConstants.PLAIN_BLOCK;

        if (block instanceof HeaderBlock) {
            Instruction loopInstr = block.headInstr();
            LoopBlock loopBlock = block.enclosingLoop();
            if (loopBlock.entryBlocks == null
                || loopBlock.entryBlocks.tail != null
                || loopBlock.flow() == null
//ICI VOIR SI LA RESTRICTION SUIVANTE EST NECESSAIRE (ET SI ELLE MARCHE)
                /*|| oneExitOutside(loopBlock)*/) {
                // No entry OR many entries OR no exit OR exit several loop levels => WILD_LOOP:
                kind = DiffConstants.WILD_LOOP;
            } else if (loopBlock.exitBlocks.tail == null
                    && loopBlock.exitBlocks.head == block
                    && block.instructions != null
                    && block.instructions.tail == null
                    && block.headInstr().tree.opCode() == ILLang.op_loop) {
                // Single entry to header AND single exit from header AND loop operator => clean loop:
                switch (loopInstr.tree.down(3).opCode()) {
                    case ILLang.op_times:
                        // It is unlikely that the user declares a TIMES loop as II-LOOP,
                        // but it is their responsibility...
                        if (!unitIsContext && loopInstr.hasDirective(Directive.IILOOP) != null) {
                            kind = DiffConstants.II_LOOP;
                        } else {
                            kind = DiffConstants.TIMES_LOOP;
                        }
                        break;
                    case ILLang.op_while:
                    case ILLang.op_until:
                        // It is unlikely that the user declares a WHILE loop as II-LOOP,
                        // but it may happen for the O+ parallel loops of R&R Hydra.
                        if (!unitIsContext && loopInstr.hasDirective(Directive.IILOOP) != null) {
                            kind = DiffConstants.II_LOOP;
                        } else {
                            kind = DiffConstants.WHILE_LOOP;
                        }
                        break;
                    case ILLang.op_do:
                        if (!unitIsContext && loopInstr.hasDirective(Directive.IILOOP) != null) {
                            kind = DiffConstants.II_LOOP;
                        } else {
                            kind = DiffConstants.DO_LOOP;
                        }
                        break;
                    case ILLang.op_for:
                        // It may happen that a FOR loop is a II-LOOP. cf Cazals-ABS.
                        if (!unitIsContext && loopInstr.hasDirective(Directive.IILOOP) != null) {
                            kind = DiffConstants.II_LOOP;
                        } else {
                            kind = DiffConstants.NATURAL_LOOP;
                        }
                        break;
                    default:
                        kind = DiffConstants.WILD_LOOP;
                        break;
                }
            } else {
                // Single entry and several exits => NATURAL_LOOP or WILD_LOOP
                if (// Constraint (cf lh26) : the exiting block
                    // must be directly contained by this loop: it must
                    // not be inside another loop
                        loopBlock.exitBlocks.head.enclosingLoop() == loopBlock) {
                    kind = DiffConstants.NATURAL_LOOP;
                } else {
                    kind = DiffConstants.WILD_LOOP;
                }
            }
            if (TapEnv.mustAdjoint() && !unitIsContext && loopInstr != null && loopInstr.hasDirective(Directive.FIXEDPOINTLOOP) != null
                    && kind != DiffConstants.II_LOOP && kind != DiffConstants.WILD_LOOP
                    && !(kind == DiffConstants.NATURAL_LOOP && loopBlock.exitBlocks.tail != null)) {
                kind = DiffConstants.FIXEDPOINT_LOOP;
                loopBlock.isFixedPoint = true ;
            }
        }
        return kind;
    }

    /**
     * Refine the current level "fgLevel" of the tree of FlowGraphLevel's,
     * (as well as every sub-level of it inserted by insertLevelStructuredLevels())
     * by inserting additional levels for non-loop syntactic
     * structures that are represented by a START and an END markers
     * in the AST (e.g. CHECKPOINTSTART/END, NODIFFSTART/END, LABEL_START/END ...).
     */
    private void insertLevelStartEndLevelsRec(FlowGraphLevel fgLevel) {
        for (TapList<FlowGraphLevel> inContents = fgLevel.contents;
             inContents != null;
             inContents = inContents.tail) {
            FlowGraphLevel insideLevel = inContents.head;
            if (insideLevel.levelKind == DiffConstants.MULTITHREAD_REGION) {
                insertLevelStartEndLevelsRec(insideLevel);
            }
        }
        insertLevelStartEndLevels(fgLevel);
    }

    /**
     * Refine the current level "level" of the tree of FlowGraphLevel's,
     * by inserting additional levels for non-loop syntactic structures that are
     * represented by a START and an END markers
     * in the AST (e.g. CHECKPOINTSTART/END, NODIFFSTART/END, LABEL_START/END ...).
     * Propagate the list of nested enclosing structures using a simplified
     * workList algorithm (not sure it is efficient...).
     */
    private void insertLevelStartEndLevels(FlowGraphLevel level) {
        Block block, arrowOrig;
        TapList<FGArrow> backFlow;
        TapList<Directive> incomingRegionList, newRegionList;
        int regionEffect;
        TapList<FlowGraphLevel> flatContents = level.contents;
        level.contents = null;
        FlowGraphLevel curSubLevel, origSubLevel, destSubLevel;
        BlockStorage<TapList<Directive>> regionLists = new BlockStorage<>(adEnv.curUnit());
        TapList<FlowGraphLevel> workList = new TapList<>(level.entryPoint, null);
        // Fixpoint propagation of the stack of nested enclosing regions, with workList:
        while (workList != null) {
            curSubLevel = workList.head;
            block = curSubLevel.entryBlock;
            workList = workList.tail;
            if (curSubLevel == level.entryPoint) {
                incomingRegionList =
                        new TapList<>(null, null); // meaning: "in no region"
            } else {
                backFlow = curSubLevel.entryBlock.backFlow();
                // accumulate lists of enclosing regions incoming onto "curSubLevel":
                incomingRegionList = null; // meaning: "neutral element ; no info"
                while (backFlow != null) {
                    origSubLevel =
                            level.getRepresentantInLevel(backFlow.head.origin);
                    destSubLevel =
                            level.getRepresentantInLevel(backFlow.head.destination);
                    if (origSubLevel != null
                            // this test is to neglect arrows that will not be considered entryArrows of curSubLevel:
                            && !(destSubLevel == level.entryPoint && backFlow.head.isCyclingArrow())) {
                        arrowOrig = origSubLevel.entryBlock;
                        newRegionList = regionLists.retrieve(arrowOrig);
                        if (newRegionList != null) {
                            if (incomingRegionList == null) {
                                incomingRegionList = newRegionList;
                            } else {
                                incomingRegionList =
                                        intersectEnclosingStartEndRegions(incomingRegionList, newRegionList);
                            }
                        }
                    }
                    backFlow = backFlow.tail;
                }
                // take into account regions that start or end here:
                if (incomingRegionList!=null && block!=null && block.instructions!=null) {
                    Directive directive ;
                    TapList<Directive> directives = block.headInstr().directives ;
                    while (directives!=null) {
                        directive = directives.head ;
                        switch (directive.type) {
                            case Directive.CHECKPOINTSTART:
                            case Directive.NODIFFSTART:
                            case Directive.LABEL_START:
                                if (!TapList.contains(incomingRegionList, directive)) {
                                    incomingRegionList = new TapList<>(directive, incomingRegionList) ;
                                }
                                break ;
                            case Directive.CHECKPOINTEND:
                            case Directive.NODIFFEND:
                            case Directive.LABEL_END: {
                                Directive startDir = Directive.hasDirective(incomingRegionList,
                                                           Directive.startKind(directive.type), directive.label) ;
                                if (startDir!=null)
                                    incomingRegionList = TapList.safeDelete(startDir, incomingRegionList) ;
                                break ;
                            }
                            default:
                                break ;
                        }
                        directives = directives.tail ;
                    }
                }
            }
            // compare with previous list of enclosing regions
            // and update workList if list has changed:
            if (incomingRegionList != null &&
                    !equalEnclosingStartEndRegions(incomingRegionList,
                            regionLists.retrieve(block))) {
                regionLists.store(block, incomingRegionList);
                for (TapList<FGArrow> flow = curSubLevel.entryBlock.flow(); flow != null; flow = flow.tail) {
                    destSubLevel = level.getRepresentantInLevel(flow.head.destination);
                    if (destSubLevel != null) {
                        workList = new TapList<>(destSubLevel, workList);
                    }
                }
            }
        }
        // Emit warning messages if regions are not well nested:
        TapList<FlowGraphLevel> inContents = flatContents;
        while (inContents != null) {
            curSubLevel = inContents.head;
            block = curSubLevel.entryBlock;
            backFlow = block.backFlow();
            TapList<FGArrow> flow = block.flow();
            // enclosing regions incoming onto "curSubLevel" must all be equal:
            incomingRegionList = null; // meaning: "neutral element ; no info"
            boolean nestingPb = false;
            while (backFlow != null) {
                origSubLevel = level.getRepresentantInLevel(backFlow.head.origin);
                destSubLevel = level.getRepresentantInLevel(backFlow.head.destination);
                arrowOrig = origSubLevel == null ? null : origSubLevel.entryBlock;
                if (arrowOrig != null
                        // this test is to neglect arrows that will not be considered entryArrows of curSubLevel:
                        && !((destSubLevel == level.entryPoint || destSubLevel == origSubLevel) && backFlow.head.isCyclingArrow())) {
                    newRegionList = regionLists.retrieve(arrowOrig);
                    if (newRegionList != null) {
                        if (incomingRegionList == null) {
                            incomingRegionList = newRegionList;
                        } else {
                            nestingPb = !equalEnclosingStartEndRegions(incomingRegionList, newRegionList);
                            incomingRegionList =
                                    intersectEnclosingStartEndRegions(incomingRegionList, newRegionList);
                        }
                    }
                }
                backFlow = backFlow.tail;
            }
            if (nestingPb) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, block.headInstr().tree, "(AD18) Inconsistent program region nesting here");
            }
            // directive cannot enter (or exit) a region twice:
            if (incomingRegionList != null && block!=null && block.instructions!=null) {
                Directive directive ;
                TapList<Directive> directives = block.headInstr().directives ;
                while (directives!=null) {
                    directive = directives.head ;
                    switch (directive.type) {
                            case Directive.CHECKPOINTSTART:
                            case Directive.NODIFFSTART:
                            case Directive.LABEL_START:
                                if (!TapList.contains(incomingRegionList, directive))
                                    incomingRegionList = new TapList<>(directive, incomingRegionList) ;
                                else
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, block.headInstr().tree,
                                                       "(AD18) Cannot enter into program region "+directive+" twice") ;
                                break ;
                            case Directive.CHECKPOINTEND:
                            case Directive.NODIFFEND:
                            case Directive.LABEL_END: {
                                Directive startDir = Directive.hasDirective(incomingRegionList,
                                                           Directive.startKind(directive.type), directive.label) ;
                                if (startDir!=null) {
                                    if (startDir!=incomingRegionList.head) {
                                        TapEnv.fileWarning(TapEnv.MSG_WARN, block.headInstr().tree,
                                                       "(AD18) Wrong nesting while exiting "+directive);
                                    }
                                    incomingRegionList = TapList.safeDelete(startDir, incomingRegionList) ;
                                } else
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, block.headInstr().tree,
                                                       "(AD18) Cannot exit from program region "+directive+" twice");
                                break ;
                            }
                            default:
                                break ;
                    }
                    directives = directives.tail ;
                }
            }
            // one cannot exit the current level (or cycle to level entry) before
            //  exiting all regions:
            boolean exitingPb = false;
            if (incomingRegionList != null && incomingRegionList.head != null) {
                while (flow != null) {
                    destSubLevel = level.getRepresentantInLevel(flow.head.destination);
                    if (destSubLevel == null || destSubLevel == level.entryPoint) {
                        exitingPb = true;
                    }
                    flow = flow.tail;
                }
            }
            if (exitingPb) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, block.headInstr().tree, "(AD18) Program region not closed in enclosing level");
            }
            inContents = inContents.tail;
        }
        // Build utility correspondence from directive to entryBlock:
        TapList<TapPair<Directive, Block>> directiveEntryBlocks = null ;
        TapList<FlowGraphLevel> subLevels = new TapList<>(level.entryPoint, flatContents) ;
        while (subLevels != null) {
            curSubLevel = subLevels.head;
            block = curSubLevel.entryBlock;
            if (block.instructions!=null) {
                Directive directive ;
                TapList<Directive> directives = block.headInstr().directives ;
                while (directives!=null) {
                    directive = directives.head ;
                    switch (directive.type) {
                        case Directive.CHECKPOINTSTART:
                        case Directive.NODIFFSTART:
                        case Directive.LABEL_START:
                            directiveEntryBlocks = new TapList<>(new TapPair<>(directive, block), directiveEntryBlocks) ;
                    }
                    directives = directives.tail;
                }
            }
            subLevels = subLevels.tail ;
        }
        // Create and insert the nested regions structure:
        TapList<TapPair<Directive, FlowGraphLevel>> newRegionLevels = new TapList<>(null, null) ;
        Directive directive ;
        FlowGraphLevel aboveLevel ;
        TapList<FlowGraphLevel> reverseContents = TapList.reverse(flatContents);
        while (reverseContents != null) {
            curSubLevel = reverseContents.head;
            curSubLevel.enclosing = null ;
            incomingRegionList = regionLists.retrieve(curSubLevel.entryBlock);
            if (incomingRegionList==null) incomingRegionList = new TapList<>(null, null) ;
            while (incomingRegionList!=null && curSubLevel!=level && curSubLevel.enclosing==null) {
                directive = incomingRegionList.head ;
                aboveLevel = buildOrRetrieveStartEndLevel(directive, newRegionLevels, level, directiveEntryBlocks);
                if (aboveLevel!=level && TapList.contains(curSubLevel.entryBlock.headInstr().directives, directive)) {
                    aboveLevel.entryPoint = curSubLevel ;
                    aboveLevel.entryBlock = curSubLevel.entryBlock ;
                } else {
                    aboveLevel.contents =
                        new TapList<>(curSubLevel, aboveLevel.contents) ;
                }
                curSubLevel.enclosing = aboveLevel ;
                curSubLevel.trueEnclosing = aboveLevel ;
                curSubLevel = aboveLevel ;
                incomingRegionList = incomingRegionList.tail ;
            }
            reverseContents = reverseContents.tail;
        }
    }

    private static TapList<Directive> intersectEnclosingStartEndRegions(TapList<Directive> directives1,
                                                                    TapList<Directive> directives2) {
        TapList<Directive> hdResult = new TapList<>(null, null);
        TapList<Directive> tlResult = hdResult;
        while (directives1 != null) {
            if (TapList.contains(directives2, directives1.head)) {
                tlResult = tlResult.placdl(directives1.head);
            }
            directives1 = directives1.tail;
        }
        return hdResult.tail;
    }

    private static boolean equalEnclosingStartEndRegions(TapList<Directive> directives1,
                                                         TapList<Directive> directives2) {
        if (directives1 == null && directives2 == null) {
            return true;
        }
        if (directives1 == null || directives2 == null || directives1.head != directives2.head) {
            return false;
        }
        return equalEnclosingStartEndRegions(directives1.tail, directives2.tail);
    }

    private FlowGraphLevel buildOrRetrieveStartEndLevel(Directive directive,
                                                        TapList<TapPair<Directive, FlowGraphLevel>> newRegionLevels,
                                                        FlowGraphLevel outerLevel,
                                                        TapList<TapPair<Directive, Block>> directiveEntryBlocks) {
        if (directive==null) {
            return outerLevel ;
        } else {
            FlowGraphLevel result = TapList.cassq(directive, newRegionLevels) ;
            if (result == null) {
                int kind ;
                switch (directive.type) {
                    case Directive.LABEL_START:
                        kind = DiffConstants.LABELLED_REGION;
                        break ;
                    case Directive.NODIFFSTART:
                        kind = DiffConstants.NODIFF;
                        break ;
                    case Directive.CHECKPOINTSTART:
                        kind = DiffConstants.CHECKPOINT;
                        break ;
                    default:
                        kind = DiffConstants.LABELLED_REGION;
                }
                result = new FlowGraphLevel(kind);
                result.label = directive.label ;
                result.entryBlock = TapList.cassq(directive, directiveEntryBlocks) ;
                newRegionLevels.placdl(new TapPair<>(directive, result)) ;
            }
            return result ;
        }
    }

    /**
     * Computes the "entryArrows" and "exitArrows" of all levels
     * of the given FlowGraphLevel, recursively, bottom-up.
     * Assume entryArrows and exitArrows are already stored for leaves PLAIN_BLOCK and ENTRY_BLOCK.
     */
    private void collectLevelEntryExitArrows(FlowGraphLevel level) {
        TapList<FGArrow> toExitArrows = new TapList<>(null, null);
        TapList<FGArrow> toEntryArrows = new TapList<>(null, null);
        TapList<FlowGraphLevel> levelAllContents =
                new TapList<>(level.entryPoint, level.contents);
        FlowGraphLevel subLevel;
        while (levelAllContents != null) {
            subLevel = levelAllContents.head;
            // Recursive call if not on a leaf:
            if (subLevel.levelKind != DiffConstants.PLAIN_BLOCK
                    && subLevel.levelKind != DiffConstants.ENTRY_BLOCK) {
                collectLevelEntryExitArrows(subLevel);
            }
            collectArrowsEnteringAndExitingLevel(
                    subLevel == level.entryPoint ? subLevel.entryArrows : null,
                    subLevel.exitArrows, level, toExitArrows, toEntryArrows);
            levelAllContents = levelAllContents.tail;
        }
        level.exitArrows = toExitArrows.tail;
        level.entryArrows = toEntryArrows.tail;
    }

    private void collectArrowsEnteringAndExitingLevel(
            TapList<FGArrow> arrowsTo, TapList<FGArrow> arrowsFrom, FlowGraphLevel level,
            TapList<FGArrow> toExitArrows, TapList<FGArrow> toEntryArrows) {
        FGArrow arrow;
        FlowGraphLevel destSubLevel, origSubLevel;
        while (arrowsFrom != null) {
            arrow = arrowsFrom.head;
            destSubLevel = level.getRepresentantInLevel(arrow.destination);
            if (destSubLevel == null) {
                toExitArrows.placdl(arrow);
            } else if (destSubLevel == level.entryPoint) {
                if (!arrow.isCyclingArrow()) {
                    // arrow considered exit-reentry:
                    toEntryArrows.placdl(arrow);
                    toExitArrows.placdl(arrow);
                }
            }
            arrowsFrom = arrowsFrom.tail;
        }
        while (arrowsTo != null) {
            arrow = arrowsTo.head;
            origSubLevel = level.getRepresentantInLevel(arrow.origin);
            if (origSubLevel == null) {
                toEntryArrows.placdl(arrow);
            }
            arrowsTo = arrowsTo.tail;
        }
    }

    private void tracePrintFlowGraphLevel(FlowGraphLevel revDiffLevel, int indent) {
        TapEnv.indentOnTrace(indent);
        if (revDiffLevel.levelKind == DiffConstants.PLAIN_BLOCK) {
            TapEnv.printOnTrace("PLAIN_BLOCK: " + revDiffLevel.entryBlock);
        } else if (revDiffLevel.levelKind == DiffConstants.ENTRY_BLOCK) {
            TapEnv.printOnTrace("ENTRY_BLOCK: " + revDiffLevel.entryBlock);
        } else {
            switch (revDiffLevel.levelKind) {
                case DiffConstants.TOP_LEVEL:
                    TapEnv.printOnTrace("TOP LEVEL");
                    break;
                case DiffConstants.PLAIN_GROUP:
                    TapEnv.printOnTrace("PLAIN GROUP");
                    break;
                case DiffConstants.CHECKPOINT:
                    TapEnv.printOnTrace("CHECKPOINT LEVEL ["+revDiffLevel.label+"]");
                    break;
                case DiffConstants.NATURAL_LOOP:
                    TapEnv.printOnTrace("NATURAL_LOOP");
                    break;
                case DiffConstants.BINOMIAL_LOOP:
                    TapEnv.printOnTrace("BINOMIAL_LOOP");
                    break;
                case DiffConstants.TIMES_LOOP:
                    TapEnv.printOnTrace("TIMES_LOOP");
                    break;
                case DiffConstants.WHILE_LOOP:
                    TapEnv.printOnTrace("WHILE_LOOP");
                    break;
                case DiffConstants.DO_LOOP:
                    TapEnv.printOnTrace("DO_LOOP");
                    break;
                case DiffConstants.II_LOOP:
                    TapEnv.printOnTrace("II_LOOP");
                    break;
                case DiffConstants.FIXEDPOINT_LOOP:
                    TapEnv.printOnTrace("FIXEDPOINT_LOOP");
                    break;
                case DiffConstants.NODIFF:
                    TapEnv.printOnTrace("NODIFF_LEVEL ["+revDiffLevel.label+"]");
                    break;
                case DiffConstants.LABELLED_REGION:
                    TapEnv.printOnTrace("LABELLED_REGION ["+revDiffLevel.label+"]");
                    break;
                case DiffConstants.MULTITHREAD_REGION:
                    TapEnv.printOnTrace("MULTITHREAD_REGION");
                    break;
                default:
                    TapEnv.printOnTrace(revDiffLevel.levelKind + "?");
                    break;
            }
        }
        TapEnv.printOnTrace(" " + revDiffLevel + "   enclosed in:" + revDiffLevel.enclosing);
        if (revDiffLevel.levelKind == DiffConstants.PLAIN_BLOCK) {
            TapEnv.printOnTrace("  instructions: " + revDiffLevel.entryBlock.instructions);
        }
        TapEnv.printlnOnTrace();
        TapEnv.indentOnTrace(indent);
        TapEnv.printOnTrace(">>entryArrows: ");
        TapList<FGArrow> arrows = revDiffLevel.entryArrows;
        while (arrows != null) {
            TapEnv.printOnTrace(arrows.head + ", ");
            arrows = arrows.tail;
        }
        TapEnv.printlnOnTrace();
        if (revDiffLevel.levelKind != DiffConstants.PLAIN_BLOCK && revDiffLevel.levelKind != DiffConstants.ENTRY_BLOCK) {
            TapEnv.indentOnTrace(indent);
            TapEnv.printlnOnTrace(">>entryPoint:");
            tracePrintFlowGraphLevel(revDiffLevel.entryPoint, indent + 4);
            TapEnv.indentOnTrace(indent);
            TapEnv.printlnOnTrace(">>contents:");
            TapList<FlowGraphLevel> contents = revDiffLevel.contents;
            while (contents != null) {
                tracePrintFlowGraphLevel(contents.head, indent + 4);
                contents = contents.tail;
            }
        }
        TapEnv.indentOnTrace(indent);
        TapEnv.printOnTrace(">>exitArrows: ");
        arrows = revDiffLevel.exitArrows;
        while (arrows != null) {
            TapEnv.printOnTrace(arrows.head + ", ");
            arrows = arrows.tail;
        }
        TapEnv.printlnOnTrace();
    }

    // END PART ABOUT BUILDING THE TREE OF NESTED STRUCTURES FOR DIFFERENTIATION.

    private void tracePrintBlock(Block block) {
        TapEnv.printOnTrace(traceBlockName(block) + " [");
        TapList<FGArrow> arrows = block.backFlow();
        while (arrows != null) {
            if (arrows.head.origin == null || arrows.head.destination != block) {
                TapEnv.printOnTrace("!!PROBLEMATIC ARROW:" + arrows.head + "!! ");
            }
            tracePrintArrowTo(arrows.head, block);
            arrows = arrows.tail;
            if (arrows != null) {
                TapEnv.printOnTrace(", ");
            }
        }
        TapEnv.printOnTrace("] --> [");
        arrows = block.flow();
        while (arrows != null) {
            if (!(block instanceof ExitBlock) && (arrows.head.origin != block || arrows.head.destination == null)) {
                TapEnv.printOnTrace("!!PROBLEMATIC ARROW:" + arrows.head + "!! ");
            }
            tracePrintArrowFrom(arrows.head, block);
            arrows = arrows.tail;
            if (arrows != null) {
                TapEnv.printOnTrace(", ");
            }
        }
        TapEnv.printlnOnTrace("]");
        if (block.parallelControls != null) {
            TapEnv.printlnOnTrace("       Controlled by:" + block.parallelControls);
        }
        TapEnv.printOnTrace("       [");
        TapList<Instruction> instrs = block.instructions;
        while (instrs != null) {
            TapEnv.printOnTrace(ILUtils.toString(instrs.head.tree));
            instrs = instrs.tail;
            if (instrs != null) {
                TapEnv.printOnTrace(", ");
            }
        }
        TapEnv.printlnOnTrace("]" + block.symbolTable.addressChain());
        TapEnv.printlnOnTrace();
    }

    private void tracePrintArrowTo(FGArrow arrow, Block block) {
        String origStr = "()";
        if (arrow.origin != null) {
            origStr = traceBlockName(arrow.origin);
        }
        String correct = arrow.destination == block ? "." : "BUG" + arrow.destination;
        TapEnv.printOnTrace(origStr + "==" + arrow.test + arrow.cases + (arrow.inACycle ? "*" : "=") + "=>" + correct);
    }

    private void tracePrintArrowFrom(FGArrow arrow, Block block) {
        String correct = arrow.origin == block ? "." : "BUG" + arrow.origin;
        String destStr = "()";
        if (arrow.destination != null) {
            destStr = traceBlockName(arrow.destination);
        }
        TapEnv.printOnTrace(correct + "==" + arrow.test + arrow.cases + (arrow.inACycle ? "*" : "=") + "=>" + destStr);
    }

    private String traceBlockName(Block block) {
        if (block.symbolicRk != null) {
            return block.toString();
        } else {
            return "B@" + Integer.toHexString(block.hashCode());
        }
    }

    /**
     * Fills the differentiated counterpart(s) of the given "unit",
     * in differentiation mode "adDiffMode".
     * The differentiated counterpart(s) is provided, empty, in the diffUnits[] of "unit".
     * Upon return, this diff Unit(s) is filled with differentiated Blocks and control flow.
     * Also fills the diff unit(s) headers, including comments.
     * There may be more than one  diff unit(s) built simultaneously,
     * e.g. the ADJOINT_FWD and ADJOINT_BWD sorts in ADJOINT_SPLIT mode.<br>
     * Note for ADJOINT and ADJOINT_SPLIT modes:
     * Connects the entry of the ADJOINT_FWD Unit to the beginning of the fwd sweep, then
     * if (JOINT mode)
     * then the end of the fwd sweep to the beginning of the bwd sweep,
     * else the end of the fwd sweep to the end of ADJOINT_FWD Unit and
     * the beginning of ADJOINT_BWD Unit to the beginning of the bwd sweep
     * endif
     * and connects the end of the bwd sweep to the exit of curDiffUnit.
     *
     * @param adDiffMode        the current differentiation mode desired, one of
     *                          {TANGENT_MODE, ADJOINT_MODE, ADJOINT_SPLIT_MODE}
     * @param topFlowGraphLevel the precomputed representation of the given "unit" as
     *                          a tree of FlowGraphLevel's, which is convenient for hierarchical differentiation
     *                          of nested constructs and code regions.
     */
    private void differentiateProcedureInMode(int adDiffMode, FlowGraphLevel topFlowGraphLevel) {
        // For readability of the trace:
        if (adEnv.someUnitsAreTraced()) {
            TapEnv.printlnOnTrace(15);
        }
        boolean modeIsTangent = adDiffMode == DiffConstants.TANGENT_MODE;
        boolean modeIsSplit = !adEnv.curActivity().isContext() && adDiffMode==DiffConstants.ADJOINT_SPLIT_MODE;
        Unit unit = adEnv.curActivity().unit();
        UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(unit);
        alteredStaticAnalysis = false;
        returnedVariable = null;
        adEnv.adDiffMode = adDiffMode;
        adEnv.curDiffUnitPushesPointers = false;
        adEnv.setCurDiffUnit(modeIsTangent ? diffInfo.getTangent(adEnv.curActivity())
                : modeIsSplit ? diffInfo.getSplitBackward(adEnv.curActivity()) : diffInfo.getAdjoint(adEnv.curActivity()));
        adEnv.setCurFwdDiffUnit(modeIsSplit ? diffInfo.getSplitForward(adEnv.curActivity()) : curDiffUnit());
        toBranchVariable = new TapPair<>(null, (curDiffUnit().isFortran()?adEnv.integer4TypeSpec:adEnv.integerTypeSpec));
        if (adDiffMode==DiffConstants.ADJOINT_MODE || adDiffMode==DiffConstants.ADJOINT_SPLIT_MODE) {
            toChunklengthVariable = new TapList<>(null, null);
            toFwdChunklengthVariable = adDiffMode==DiffConstants.ADJOINT_SPLIT_MODE ? new TapList<>(null, null) : toChunklengthVariable;
            toChunkoldVariable = new TapList<>(null, null);
            toDiffchunkoldVariable = new TapList<>(null, null);
            // Reinitialize the holders for the additional temp vars needed by OpenMP
            adOmpChunkStartVarTool = null;
            adOmpChunkEndVarTool = null;
            ompChunksNumVariable = null;
            ompChunksIndexVariable = null;
            ompChunksOriginalIndex = null;
            ompChunksOriginalIndexFwd = null;
        }
        Directive scopingsDirective =
            Directive.hasDirective(unit.directives,
                                   (modeIsTangent? Directive.MULTITHREAD_TGT : Directive.MULTITHREAD_ADJ)) ;
        if (scopingsDirective != null) {
            TapList<Tree> toClausesGivenScoping = new TapList<>(null, null);
            TapList<String> toForcedAtomicScopingNames = new TapList<>(null, null);
            TapList<String> toForcedOtherScopingNames = new TapList<>(null, null);
            Directive.analyzeMultithreadScopings(scopingsDirective.arguments[0], !unit.isFortran(),
                    toClausesGivenScoping, toForcedAtomicScopingNames, toForcedOtherScopingNames);
            // Add zones of vars in "toForcedOtherScopingNames" into "onlyReadSmallActivePrimals",
            // which as a consequence will prevent their diff vars from asking "atomic" accesses.
            SymbolTable unitPrivST = unit.privateSymbolTable() ;
            int unitAllKindLength = unitPrivST.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
            BoolVector onlyReadSmallActivePrimals = new BoolVector(unitAllKindLength) ;
            TapList<String> forcedNotAtomic = toForcedOtherScopingNames.tail ;
            while (forcedNotAtomic!=null) {
                String varName = forcedNotAtomic.head ;
                TapIntList varZones =
                    unitPrivST.listOfZonesOfValue(ILUtils.build(ILLang.op_ident, varName),
                                                                 null, null) ;
                while (varZones!=null) {
                    onlyReadSmallActivePrimals.set(varZones.head, true) ;
                    ZoneInfo zi = unitPrivST.declaredZoneInfo(varZones.head, SymbolTableConstants.ALLKIND) ;
                    if (zi!=null) {
                        TapIntList pointerDestsList = ZoneInfo.listAllZones(zi.targetZonesTree, false) ;
                        onlyReadSmallActivePrimals.set(pointerDestsList, true) ;
                    }
                    varZones = varZones.tail ;
                }
                forcedNotAtomic = forcedNotAtomic.tail ;
            }
            unitPrivST.needNoAtomic = onlyReadSmallActivePrimals ;
        }

        curDiffUnit().privateSymbolTable().nestedIntIndexSymbolHolders = new TapList<>(null, null);
        if (modeIsSplit) {
            curFwdDiffUnit().privateSymbolTable().nestedIntIndexSymbolHolders = new TapList<>(null, null);
        }

        initializeMultiDirMode();
        TapEnv.resetPushPopNumber();
        resetIsDifferentiatedDeclInstructions(unit);
        debugPointsCounter = 1;

        if (adDiffMode == DiffConstants.TANGENT_MODE) {
            tangentDiffReinitChecker = new BoolVector(unit.nbArrows);
        }

        toDuplicateVars = new TapList<>(null, null);
        FlowGraphDifferentiationEnv diffEnv = new FlowGraphDifferentiationEnv(adDiffMode);
        diffEnv.initialize();
        realOrigBlockOfReturns = null;
        diffEnv.turningArrows = (modeIsSplit ? null : topFlowGraphLevel.exitArrows) ;
        diffEnv.middleSymbolTables =
                (modeIsSplit ? null
                        : collectMiddleSymbolTables((TapEnv.debugAdMode() != TapEnv.NO_DEBUG ? null : diffEnv.turningArrows), unit.allBlocks()));

        // ******************* MAIN CALLS: ******************
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace(" ----------- Differentiation of individual Blocks:");
        }
        precomputeDifferentiatedBlocksIn(topFlowGraphLevel, diffEnv);
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace(" ----------- Connecting differentiated Blocks:");
        }
        FlowGraphDifferentiation differentiation =
                differentiateFlowGraphLevel(topFlowGraphLevel, diffEnv);
        // *************************************************

        BoolVector reqXRequired = TapEnv.reqExplicitAnalyzer().diffZonesRequired(adEnv.curActivity());
        BoolVector reqXManaged = TapEnv.reqExplicitAnalyzer().diffZonesManaged(adEnv.curActivity());
        insertADBanner(unit, curDiffUnit(), adEnv.curActivity(),
                modeIsTangent ?
                        adEnv.curActivity().isContext() ?
                                " as a context to call tangent code" :
                                " in forward (tangent) mode" :
                        modeIsSplit ?
                                " in reverse (adjoint) mode, backward sweep" :
                                adEnv.curActivity().isContext() ?
                                        " as a context to call adjoint code" :
                                        " in reverse (adjoint) mode",
                modeIsTangent ?
                        "   variations   of useful results:" :
                        "   gradient     of useful results:",
                adEnv.curActivity().exitActivity(),
                "   with respect to varying inputs:",
                adEnv.curActivity().callActivity(),
                // "PointerActiveUsage":
                // In the bizarre case where there is nothing active,
                // but we differentiate anyway (e.g because of the ReqX variables
                // or because this unit contains an active unit)
                reqXRequired, reqXManaged, true);
        if (modeIsSplit) {
            insertADBanner(unit, curFwdDiffUnit(), adEnv.curActivity(),
                    " in reverse (adjoint) mode, forward sweep",
                    "   gradient     of useful results:",
                    adEnv.curActivity().exitActivity(),
                    "   with respect to varying inputs:",
                    adEnv.curActivity().callActivity(),
                    // "PointerActiveUsage":
                    reqXRequired, reqXManaged, false);
        }

        Block centerEntry = null;
        TapList<FGArrow> centerExits = null;
        if (!adEnv.curActivity().isContext()
            && (modeIsSplit || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG)) {
            // In these special cases, connect fwd and bwd sweeps through a central block:
            centerEntry = buildFwdBlock("centerTurn",
                                  curFwdDiffUnit().privateSymbolTable(), adEnv.curUnit().exitBlock(), diffEnv) ;
            Block incomingBwdBlock = centerEntry;
            if (modeIsSplit) {
                Block privateDeclBlock = curDiffUnit().privateSymbolTable().declarationsBlock;
                // This is cosmetic, to make the traceDifferentiation more readable:
                if (privateDeclBlock.symbolicRk == null) {
                    privateDeclBlock.symbolicRk = "PrivDeclBlock";
                }
                if (diffEnv.diffCtxt.toAllBlocks != null) {
                    diffEnv.diffCtxt.toAllBlocks.placdl(privateDeclBlock);
                }
                // end cosmetic.
                incomingBwdBlock = privateDeclBlock;
                checkFreeThenSetNewFGArrow(centerEntry, FGConstants.NO_TEST, 0, curFwdDiffUnit().exitBlock(), false);
                checkFreeThenSetNewFGArrow(curDiffUnit().entryBlock(), FGConstants.NO_TEST, 0, incomingBwdBlock, false);
            }
            centerExits =
                    new TapList<>(checkFreeThenSetNewFGArrow(incomingBwdBlock, FGConstants.NO_TEST, 0, null, false), null);
        }
        PushPopNode pushPopTree = buildFwdBwdTreeFromExitFwdBwd(differentiation.exitArrowsFwdBwds);
        connectFwdBwd(pushPopTree,
                centerEntry, centerExits, unit.exitBlock(), null, diffEnv, topFlowGraphLevel);

        TapList<SymbolTable> newDiffSymbolTables = curDiffUnit().symbolTablesBottomUp();
        SymbolTable newDiffSymbolTable;
        while (newDiffSymbolTables != null) {
            newDiffSymbolTable = newDiffSymbolTables.head;
            if (newDiffSymbolTable.declarationsBlock != null) {
                newDiffSymbolTable.declarationsBlock.symbolTable = newDiffSymbolTable;
            }
            newDiffSymbolTables = newDiffSymbolTables.tail;
        }

        // For each sub-Unit stored in the curUnit's Non-flow instructions,
        //  insert the needed differentiated sub-Units into the curDiffUnit's Non-flow instructions:
        TapList<Unit> toDiffSubUnitsFwd = new TapList<>(null, null);
        TapList<Unit> toDiffSubUnitsBwd = new TapList<>(null, null);
        TapList<Instruction> origNonFlow = unit.nonFlowInstructions;
        while (origNonFlow != null) {
            Unit subUnit = origNonFlow.head.tree.getAnnotation("Unit");
            if (subUnit != null) {
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("    --- differentiating non-flow definition of " + subUnit);
                }
                callGraphDifferentiator().collectDiffSubUnits(subUnit, adDiffMode,
                        toDiffSubUnitsFwd, toDiffSubUnitsBwd);
            }
            origNonFlow = origNonFlow.tail;
        }
        toDiffSubUnitsFwd = toDiffSubUnitsFwd.tail;
        toDiffSubUnitsBwd = toDiffSubUnitsBwd.tail;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("       Cumul contained Units for fwd:" + toDiffSubUnitsFwd);
            TapEnv.printlnOnTrace("       Cumul contained Units for bwd:" + toDiffSubUnitsBwd);
        }
        while (toDiffSubUnitsFwd != null) {
            curFwdDiffUnit().addNonFlowInstruction(Instruction.createUnitDefinitionStub(toDiffSubUnitsFwd.head, null));
            toDiffSubUnitsFwd = toDiffSubUnitsFwd.tail;
        }
        while (toDiffSubUnitsBwd != null) {
            curDiffUnit().addNonFlowInstruction(Instruction.createUnitDefinitionStub(toDiffSubUnitsBwd.head, null));
            toDiffSubUnitsBwd = toDiffSubUnitsBwd.tail;
        }

        // Insert debug AD instructions when needed
        if (TapEnv.debugAdMode() != TapEnv.NO_DEBUG && adEnv.unitActivities != null) {
            TapList<FGArrow> flow = curDiffUnit().entryBlock().flow();
            Block diffUnitFirstBlock = (flow==null ? null : flow.head.destination) ;
            Block upstreamDebugPoint, downstreamDebugPoint;
            if (modeIsTangent) {
                upstreamDebugPoint = diffUnitFirstBlock;
                downstreamDebugPoint = buildBwdBlock("traceExitBlock",
                                               curDiffUnit().privateSymbolTable(), adEnv.curUnit().exitBlock(), diffEnv) ;
                curDiffUnit().exitBlock().insertBlockBefore(downstreamDebugPoint);
            } else {
                upstreamDebugPoint = buildBwdBlock("traceEntryBlock",
                                             curDiffUnit().privateSymbolTable(), adEnv.curUnit().entryBlock(), diffEnv) ;
                curDiffUnit().exitBlock().insertBlockBefore(upstreamDebugPoint);
                downstreamDebugPoint = modeIsSplit ? diffUnitFirstBlock : centerEntry;
            }
            instrumentTraceOnUnit(unit, upstreamDebugPoint, downstreamDebugPoint, diffEnv);
            }

        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        // Insert the statements that update diffParams with local diffParams when PASS-BY-VALUE:
        if (toDuplicateVars != null && toDuplicateVars.tail != null) {
            Block updateBlock = buildBwdBlock("passByValueUpdate",
                                        curDiffUnit().privateSymbolTable(), adEnv.curUnit().exitBlock(), diffEnv) ;
            TapPair<Tree, Tree> publicLocal;
            Tree publicExpr, localExpr, assign;
            toDuplicateVars = toDuplicateVars.tail;
            while (toDuplicateVars != null) {
                publicLocal = toDuplicateVars.head;
                publicExpr = publicLocal.first;
                localExpr = publicLocal.second;
                // TODO VMP traiter le cas des tableaux pour C
                assign = ILUtils.build(ILLang.op_assign,
                        ILUtils.copy(publicExpr),
                        ILUtils.build(ILLang.op_add,
                                ILUtils.copy(publicExpr), localExpr));
                updateBlock.addInstrHdAfterDecls(assign);
                toDuplicateVars = toDuplicateVars.tail;
            }
            insertBlockBefore(curDiffUnit().exitBlock(), updateBlock);
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.

        // In (CONTEXT and ADJOINT) or in ADJOINT_SPLIT_MODE, for the FWD split function,
        //  if FORTRAN, reassign the original return value to the new,
        //    and create VariableDecl and declaration Instruction for the new "*_fwd" returned symbol.
        //  if C, return the variable with the diff of original function name, so that
        //    there's only *one* return at the end, *after* possible last push'es !

        if (unit.isAFunction() && returnedVariable == null && curDiffUnit().isFortran()) {
            returnedVariable =
                    ILUtils.build(ILLang.op_ident,
                            (unit.otherReturnVar() != null
                                    ? unit.otherReturnVar().symbol
                                    : unit.name()));
        }

        boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(adEnv.curActivity());
        boolean resultIsActive = formalArgsActivity[formalArgsActivity.length - 1];
        if (unit.isAFunction() && returnedVariable != null
                && (adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE
                || (adEnv.curActivity().isContext() && adDiffMode == DiffConstants.ADJOINT_MODE))) {
            NewSymbolHolder newReturnVarSH;
            VariableDecl fwdFuncNameDecl;
            Tree newReturnVarName, newAssignOrReturn = null;
            int nbArgs = unit.functionTypeSpec().argumentsTypes.length;
            boolean[] formalArgsPointerActivity = ReqExplicit.formalArgsPointerActivity(adEnv.curActivity(), nbArgs);
            boolean resultIsPtrActive = formalArgsPointerActivity[nbArgs];
            if (resultIsPtrActive) {
                newReturnVarName = varRefDifferentiator().diffSymbolTree(
                        adEnv.curActivity(), returnedVariable, curFwdDiffUnit().publicSymbolTable(),
                        true, false, false, null,
                        false, DiffConstants.ADJOINT, DiffConstants.ADJOINT_FWD, DiffConstants.VAR, null);
                newReturnVarSH = NewSymbolHolder.getNewSymbolHolder(newReturnVarName);
            } else {
                newReturnVarName = ILUtils.copy(returnedVariable);
                newReturnVarSH = null;
            }
            if (unit.isC()) {
                newAssignOrReturn = ILUtils.build(ILLang.op_return, newReturnVarName);
                if (newReturnVarSH != null && newReturnVarSH.typeSpec() == null) {
                    WrapperTypeSpec origReturnType = unit.functionTypeSpec().returnType;
                    newReturnVarSH.setAsVariable(new WrapperTypeSpec(new PointerTypeSpec(origReturnType, null)), null);
                    fwdFuncNameDecl = new VariableDecl(ILUtils.copy(newReturnVarName), newReturnVarSH.typeSpec());
                    fwdFuncNameDecl.formalArgRank = SymbolTableConstants.NOT_A_FORMAL_ARG;
                    newReturnVarSH.accumulateSymbolDecl(fwdFuncNameDecl, curFwdDiffUnit().privateSymbolTable());
                }
            } else {
                Tree fwdFuncName = ILUtils.copy(ILUtils.getCalledName(curFwdDiffUnit().entryBlock().headInstr().tree));
                fwdFuncName.setAnnotation("isFunctionName", Boolean.TRUE);
                NewSymbolHolder fwdFuncVarSH = NewSymbolHolder.getNewSymbolHolder(fwdFuncName);
                VariableDecl origDecl = unit.publicSymbolTable().getTopVariableDecl(ILUtils.getIdentString(returnedVariable));
                fwdFuncVarSH.setAsVariable(origDecl.type(), null);
                fwdFuncNameDecl = new VariableDecl(ILUtils.copy(fwdFuncName), origDecl.type());
                fwdFuncNameDecl.isReturnVar = true;
                fwdFuncVarSH.accumulateSymbolDecl(fwdFuncNameDecl, curFwdDiffUnit().privateSymbolTable());
            }
            if (newAssignOrReturn != null) {
                Block returnBlock = buildFwdBlock("fwdReturnBlock",
                                            curFwdDiffUnit().entryBlock().symbolTable, adEnv.curUnit().exitBlock(), diffEnv) ;
                returnBlock.addInstrHdAfterDecls(newAssignOrReturn);
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("     -> (FwdReturnBlock+) final return : "
                            + ILUtils.toString(newAssignOrReturn, adEnv.curActivity()));
                }
                insertBlockBefore(curFwdDiffUnit().exitBlock(), returnBlock);
            }
        }
        if (curDiffUnit().isAFunction() && resultIsActive && curDiffUnit().isFortran()
                && ((modeIsTangent && !multiDirMode()) || adEnv.curUnitIsContext)) {
            VariableDecl returnVarDecl = curDiffUnit().publicSymbolTable().getTopVariableDecl(unit.name());
            if (returnVarDecl == null) {
                String otherReturnVarName = unit.otherReturnVar().symbol;
                returnVarDecl =
                        curDiffUnit().publicSymbolTable().getTopVariableDecl(otherReturnVarName);
            }
            if (returnVarDecl != null) {
                declareDiffIsReturnVar(returnVarDecl, unit, curDiffUnit());
            }
        }

        blockDifferentiator().differentiateDeclarationsOfBlock(curDiffUnit().publicSymbolTable().declarationsBlock, adDiffMode,
                unit, unit.publicSymbolTable(), curDiffUnit().privateSymbolTable(), unit.language(), false);
        adEnv.toIndexSymbolHolders.tail = null;

        //Unit.disconnectStopsFromSequel(diffEnv.diffCtxt.toAllBlocks.tail) ;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace(" ----------- List of all differentiated Blocks created:");
            if (modeIsSplit) {
                TapEnv.printOnTrace("[FWD ENTRY:]");
                tracePrintBlock(curFwdDiffUnit().entryBlock());
                TapList<Block> inToAll = diffEnv.diffCtxtFwd.toAllBlocks.tail;
                while (inToAll != null) {
                    tracePrintBlock(inToAll.head);
                    inToAll = inToAll.tail;
                }
                TapEnv.printOnTrace("[FWD EXIT:]");
                tracePrintBlock(curFwdDiffUnit().exitBlock());
                TapEnv.printlnOnTrace();
            }
            TapEnv.printOnTrace("[ENTRY:]");
            tracePrintBlock(curDiffUnit().entryBlock());
            TapList<Block> inToAll = diffEnv.diffCtxt.toAllBlocks.tail;
            while (inToAll != null) {
                tracePrintBlock(inToAll.head);
                inToAll = inToAll.tail;
            }
            TapEnv.printOnTrace("[EXIT:]");
            tracePrintBlock(curDiffUnit().exitBlock());
            TapEnv.printlnOnTrace();
        }

        // Check that no diff reinitialization has been forgotten (in Tangent mode) :
        if (adDiffMode == DiffConstants.TANGENT_MODE) {
            TapList<FGArrow> fgArrows = unit.allArrows;
            FGArrow fgArrow;
            while (fgArrows != null) {
                fgArrow = fgArrows.head;
                if (!tangentDiffReinitChecker.get(fgArrow.rank) &&
                        mustCheckActivitySwitches(fgArrow.origin, fgArrow.destination)) {
                    // [llh 03/10/2013] TODO: it would be nice if this message gave the variable names to initialize:
                    TapEnv.toolWarning(-1, "(Tangent mode AD): Lost reinitialization of derivatives on flow arrow " + fgArrow);
                }
                fgArrows = fgArrows.tail;
            }
            tangentDiffReinitChecker = null;
        }

        // Terminate differentiated Unit:
        if (modeIsSplit) {
            differentiateSaveList(curFwdDiffUnit().privateSymbolTable());
            CallGraphDifferentiator.solveNewSymbolHoldersOfUnit(curFwdDiffUnit(), adEnv.emptyCopiedUnitSuffix);
            // Add the INTERFACE declaration for PUSH/POP POINTER() :
            if ((adEnv.curDiffUnitPushesPointers || adEnv.usesADMM || curFwdDiffUnit().usesISO_C_BINDING) && TapEnv.relatedLanguageIsFortran9x()) {
                declareStackPointerInterface(curFwdDiffUnit());
            }
            //Unit.disconnectStopsFromSequel(diffEnv.diffCtxtFwd.toAllBlocks.tail) ;
            curFwdDiffUnit().makeFlowGraph(curFwdDiffUnit().entryBlock(),
                    diffEnv.diffCtxtFwd.toAllBlocks.tail, curFwdDiffUnit().exitBlock(), true);
        }
        differentiateSaveList(curDiffUnit().privateSymbolTable());
        // Solve pending/waiting NewSymbolHolders in SymbolTables of the curDiffUnit,
        // from deepest (nested locals then private)
        // to rootmost (rootmost imported ST, or public ST of curDiffUnit).
        CallGraphDifferentiator.solveNewSymbolHoldersOfUnit(curDiffUnit(), adEnv.emptyCopiedUnitSuffix);
        // Add the INTERFACE declaration for PUSH/POP POINTER() :
        if ((adEnv.curDiffUnitPushesPointers || adEnv.usesADMM || curDiffUnit().usesISO_C_BINDING) && TapEnv.relatedLanguageIsFortran9x()) {
            declareStackPointerInterface(curDiffUnit());
        }
        adEnv.curDiffUnitPushesPointers = false;
        curDiffUnit().makeFlowGraph(curDiffUnit().entryBlock(),
                diffEnv.diffCtxt.toAllBlocks.tail, curDiffUnit().exitBlock(), true);
        if (alteredStaticAnalysis) {
            // Checkpoints or other special differentiations may have
            //  damaged static analysis of curUnit => repair this now.
            restoreStaticAnalysis(!modeIsSplit);
        }
        // Also (in Fortran), when differentiating as a context (or when associationByAddress),
        //  the function name becomes X_[D|B], therefore the return variable name (either X or
        // the original explicit return variable) must be specified explicitly (except if the
        // diff unit already has a specific otherReturn name):
        if ((adEnv.curActivity().isContext() || TapEnv.associationByAddress())
                && unit.isAFunction()
                && unit.isFortran()
                && curDiffUnit().isAFunction()
                && curDiffUnit().otherReturnVar() == null) {
            Tree returnTree = ILUtils.build(ILLang.op_ident, unit.name());
            if (unit.otherReturnVar() != null) {
                returnTree = ILUtils.build(ILLang.op_ident, unit.otherReturnVar().symbol);
            }
            Tree diffReturnVar ;
            if (TapEnv.associationByAddress()) {
                diffReturnVar = ILUtils.copy(ILUtils.getCalledName(curDiffUnit().headTree())) ;
            } else {
                diffReturnVar =
                    varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), returnTree, adEnv.curFwdSymbolTable,
                            true, false, false, unit.headTree(), true, curDiffVarSort(),
                            adEnv.curDiffUnitSort, DiffConstants.VAR, null);
            }
            diffReturnVar.setAnnotation("isFunctionName", Boolean.TRUE);
            NewSymbolHolder newSH = NewSymbolHolder.getNewSymbolHolder(diffReturnVar);
            if (newSH != null) {
                newSH.declarationLevelMustInclude(adEnv.curFwdSymbolTable);
            }
            callGraphDifferentiator().updateAllUsagesDiffFuncResultName(unit, ILUtils.baseName(returnTree),
                    curDiffUnit(), diffReturnVar);
        }
    }

//     /**
//      * Builds the new controls (or retrieves from the memo of already built
//      * new controls "toDoneTranslations") that correspond to the given "oldControls".
//      * These controls are lists of controlling Instructions of a given Unit,
//      * and they are attached to Blocks of the same Unit.
//      * The old Controls are related to some "old" unit, and we translate them
//      * into the equivalent controls in the "new" unit.
//      * Correspondence between "old" and "new" units is defined in diffEnv
//      */
//     private TapList<Instruction> translateParallelControls(
//             TapList<Instruction> oldControls,
//             BlockStorage<Block> fwdDiffBlocks,
//             TapList<TapPair<TapList<Instruction>, TapList<Instruction>>> toDoneTranslations) {
//         if (oldControls == null) {
//             return null;
//         }
//         TapPair<TapList<Instruction>, TapList<Instruction>> found = null;
//         TapList<TapPair<TapList<Instruction>, TapList<Instruction>>> inDoneTranslations = toDoneTranslations.tail;
//         while (found == null && inDoneTranslations != null) {
//             if (inDoneTranslations.head.first == oldControls) {
//                 found = inDoneTranslations.head;
//             }
//             inDoneTranslations = inDoneTranslations.tail;
//         }
//         if (found == null) {
//             Instruction oldControl = oldControls.head;
//             Instruction newControl = fwdDiffBlocks.retrieve(oldControl.block).lastInstr();
//             found = new TapPair<>(
//                     oldControls,
//                     new TapList<Instruction>(
//                             newControl,
//                             translateParallelControls(oldControls.tail,
//                                     fwdDiffBlocks, toDoneTranslations)));
//             toDoneTranslations.placdl(found);
//         }
//         return found.second;
//     }

    /**
     * Sweeps through the given FlowGraphLevel level, with the same control that
     * will be taken by differentiateFlowGraphLevel(), but only differentiating
     * the blocks by calling blockDifferentiator().differentiateBlock(), and
     * storing the differentiated Blocks into the given diffEnv.
     * It is necessary to do so, so that all diff Blocks are filled before
     * differentiateFlowGraphLevel(), and therefore filling the info in the diffEnv
     * that tells whether a Block must have a Fwd and Bwd diff Block or not.
     */
    private void precomputeDifferentiatedBlocksIn(FlowGraphLevel level, FlowGraphDifferentiationEnv diffEnv) {
        TapList<FlowGraphLevel> subLevels = new TapList<>(level.entryPoint, level.contents);
        while (subLevels != null) {
            precomputeDifferentiatedBlocks(subLevels.head, diffEnv);
            subLevels = subLevels.tail;
        }
    }

    private void precomputeDifferentiatedBlocks(FlowGraphLevel level, FlowGraphDifferentiationEnv diffEnv) {
        int kind = level.levelKind;
        if ((kind == DiffConstants.DO_LOOP || kind == DiffConstants.WHILE_LOOP || kind == DiffConstants.TIMES_LOOP
             || kind == DiffConstants.NATURAL_LOOP || kind == DiffConstants.BINOMIAL_LOOP)
            && level.entryBlock.headInstr() != null
            && level.entryBlock.headInstr().hasDirective(Directive.BINOMIALCKP) != null
            && diffEnv.adDiffMode==DiffConstants.ADJOINT_MODE
            && !adEnv.curUnitIsContext) {
            kind = DiffConstants.BINOMIAL_LOOP;
        }
        if (level.entryBlock.headInstr() != null
            && level.entryBlock.headInstr().hasDirective(Directive.BINOMIALCKP) != null
            && diffEnv.adDiffMode!=DiffConstants.TANGENT_MODE
            && kind!=DiffConstants.BINOMIAL_LOOP) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, level.entryBlock.headInstr().tree, "(AD19) Cannot use binomial checkpointing in adjoint split context");
        }
        // Kinds BINOMIAL_LOOP, II_LOOP, CHECKPOINT make no sense in TANGENT_MODE
        // => consider them as DO_LOOP, DO_LOOP, PLAIN_GROUP respectively.
        if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
            if (kind == DiffConstants.BINOMIAL_LOOP || kind == DiffConstants.II_LOOP) {
                kind = DiffConstants.DO_LOOP;
            }
            if (kind == DiffConstants.CHECKPOINT) {
                kind = DiffConstants.PLAIN_GROUP;
            }
        }
        switch (kind) {
            case DiffConstants.BINOMIAL_LOOP:
                // Don't go inside loop body for the "turn" case:
                //  this will be done by deeper FlowGraphDifferentiationEnv's
                //Otherwise do the same as for a NATURAL_LOOP => continue into next case:
            case DiffConstants.NATURAL_LOOP:
                precomputeDifferentiatedBlocksIn(level.createNoCycle(), diffEnv);
                //Debug.println("ici block d'entree natural :"+ level.entryBlock.instructions);
                // Creating level.createNoCycle() has damaged level => repair it now.
                level.restoreChildrenParent();
                break;
            case DiffConstants.TIMES_LOOP:
            case DiffConstants.WHILE_LOOP:
            case DiffConstants.DO_LOOP: {
                Block block = level.entryBlock;
                precomputeDifferentiatedBlocksIn(level.createLoopBody(), diffEnv);
                Block fwdDiffBlock = createFwdDiffBlock(block, diffEnv, true);
                Block bwdDiffBlock = createBwdDiffBlock(block, diffEnv, true);
                blockDifferentiator().differentiateBlock(block, fwdDiffBlock, bwdDiffBlock, diffEnv.adDiffMode, null);
                diffEnv.storeDiffBlocks(block, fwdDiffBlock, bwdDiffBlock, true);
                // Creating level.createLoopBody() has damaged level => repair it now.
                level.restoreChildrenParent();
                break;
            }
            case DiffConstants.FIXEDPOINT_LOOP:
            case DiffConstants.II_LOOP: {
                Block block = level.entryBlock;
                // Don't go inside loop body now: this will be done by deeper FlowGraphDifferentiationEnv's.
                // Nevertheless, since this II-LOOP annotation (or this OpenMP pragma that implied II-LOOP)
                // or this FP-LOOP annotation, were set by the end-user,
                // we assume that the contents are live, therefore we propagate activity up on controller blocks:
                diffEnv.markBwdControllersLive(block);
                Block fwdDiffBlock = createFwdDiffBlock(block, diffEnv, true);
                Block bwdDiffBlock = createBwdDiffBlock(block, diffEnv, true);
                blockDifferentiator().differentiateBlock(block, fwdDiffBlock, bwdDiffBlock, diffEnv.adDiffMode, null);
                diffEnv.storeDiffBlocks(block, fwdDiffBlock, bwdDiffBlock, true);
                break;
            }
            case DiffConstants.MULTITHREAD_REGION:
            case DiffConstants.CHECKPOINT:
            case DiffConstants.NODIFF:
                // Don't go inside ckp body now: this will be done by deeper FlowGraphDifferentiationEnv's.
                // Nevertheless, since this annotation was set by the end-user, we assume that
                // the contents are live, therefore we propagate activity up on controller blocks:
                diffEnv.markBwdControllersLive(level.entryBlock);
                break;
            case DiffConstants.PLAIN_GROUP:
            case DiffConstants.LABELLED_REGION:
                precomputeDifferentiatedBlocksIn(level, diffEnv);
                break;
            case DiffConstants.PLAIN_BLOCK: {
                Block block = level.entryBlock;
                boolean vanishingLoopHeader = false;
                if (block instanceof HeaderBlock && block == level.enclosing.headerBlockPlain) {
                    Instruction loopInstr = block.headInstr();
                    if (loopInstr.tree.opCode() == ILLang.op_loop) {
                        vanishingLoopHeader = true;
                    }
                }
                Block fwdDiffBlock = createFwdDiffBlock(block, diffEnv,
                        block instanceof HeaderBlock && block != level.enclosing.headerBlockPlain);
                Block bwdDiffBlock = createBwdDiffBlock(block, diffEnv, false);
//             // Keep track of bwdDiffBlock if needed by debug AD :
//             if (middleBlock==block)
//                 diffMiddleBlock = (diffEnv.adDiffMode==TANGENT_MODE?fwdDiffBlock:bwdDiffBlock) ;
                ToBool lastTestLive = null;
                // Prepare for checking if the last test is live only if this Block ends with a test:
                if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE || block.isControl()) {
                    lastTestLive = new ToBool(false);
                }
                blockDifferentiator().differentiateBlock(block, fwdDiffBlock, bwdDiffBlock, diffEnv.adDiffMode, lastTestLive);
                diffEnv.storeDiffBlocks(block, fwdDiffBlock, bwdDiffBlock, lastTestLive != null && lastTestLive.get());
                if (vanishingLoopHeader) {
                    fwdDiffBlock.instructions = TapList.removeLast(fwdDiffBlock.instructions);
                }
                // When adjoinning an OpenMP "$OMP DO", we apply a special mechanism on schedule
                // that will disconnect the bwd parallelLoop Block, and sometimes also the fwd.
                if (block.instructions!=null
                    && (diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE
                        || diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE)) {
                    Tree pragma = block.headInstr().tree;
                    if (pragma.opCode() == ILLang.op_parallelLoop
                        && ILUtils.isIdent(pragma.down(1), "omp", false)) {
                        Tree srcSchedule = pragma.getAnnotation("srcSchedule");
                        bwdDiffBlock.instructions = null ;
                        if (ILUtils.isStaticSchedule(srcSchedule))
                            fwdDiffBlock.instructions = null ;
                    }
                }
                break;
            }
            case DiffConstants.ENTRY_BLOCK: {
                EntryBlock entryBlock = (EntryBlock) level.entryBlock;
                if (adEnv.traceCurDifferentiation) {
                    adEnv.traceCurBlock = TapEnv.get().traceBlockRk == -99 ? 1 : entryBlock.rank == TapEnv.get().traceBlockRk ? 2 : 0;
                    if (adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace("*** NOW Differentiating EntryBlock:" + entryBlock + " : " + entryBlock.instructions);
                    }
                }
                SymbolTable sourcePublicSymbolTable = entryBlock.symbolTable;
                boolean modeIsSplit = !adEnv.curUnitIsContext && diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE;
                // Create the public SymbolTable of the curFwdDiffUnit:
                SymbolTable fwdDiffSymbolTable =
                        sourcePublicSymbolTable.smartCopy(diffEnv.diffCtxtFwd.listST, diffEnv.diffCtxtFwd.unit,
                                adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
                CallGraphDifferentiator.declareDiffSymbolTableIfNew(diffEnv.diffCtxtFwd.unit, fwdDiffSymbolTable);
                // Create the public SymbolTable of the curDiffUnit (when separate from curFwdDiffUnit()):
                SymbolTable diffSymbolTable = fwdDiffSymbolTable;
                if (modeIsSplit) {
                    diffSymbolTable =
                            sourcePublicSymbolTable.smartCopy(diffEnv.diffCtxt.listST, diffEnv.diffCtxt.unit,
                                    adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
                    CallGraphDifferentiator.declareDiffSymbolTableIfNew(diffEnv.diffCtxt.unit, diffSymbolTable);
                }
                // Create and fill the EntryBlock of the curFwdDiffUnit:
                EntryBlock entryOfFwdSweep = new EntryBlock(fwdDiffSymbolTable);
                entryOfFwdSweep.symbolicRk = "+E";
                Tree fwdDiffCallTree;
                adEnv.setCurSymbolTable(entryBlock.symbolTable);
                if (adEnv.curUnitIsActiveUnit) {
                    fwdDiffCallTree =
                            procedureCallDifferentiator().differentiateProcedureHeader(entryBlock.headTree(), null, null, null,
                                    fwdDiffSymbolTable, fwdDiffSymbolTable,
                                    (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE ? DiffConstants.TANGENT :
                                            (modeIsSplit ? DiffConstants.ADJOINT_FWD : DiffConstants.ADJOINT)),
                                    toDuplicateVars);
                } else {
                    fwdDiffCallTree = ILUtils.copy(entryBlock.headTree());
                }
                entryOfFwdSweep.addInstrTl(fwdDiffCallTree);
                curFwdDiffUnit().setEntryBlock(entryOfFwdSweep);
                curFwdDiffUnit().setName(ILUtils.getCalledNameString(fwdDiffCallTree));
                // Create the ExitBlock of the curDiffUnit:
                ExitBlock exitOfBwdSweep = new ExitBlock(diffSymbolTable);
                exitOfBwdSweep.symbolicRk = "-X";
                curDiffUnit().setExitBlock(exitOfBwdSweep);
                diffEnv.storeDiffBlocks(entryBlock, entryOfFwdSweep, exitOfBwdSweep, true);
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace(" -> fwdBlock:" + entryOfFwdSweep + " : " + entryOfFwdSweep.instructions);
                    TapEnv.printlnOnTrace(" -> bwdBlock:" + exitOfBwdSweep + " : " + exitOfBwdSweep.instructions);
                }
                if (modeIsSplit) {
                    // Create the ExitBlock of the curFwdDiffUnit:
                    ExitBlock exitOfFwdSweep = new ExitBlock(fwdDiffSymbolTable);
                    exitOfFwdSweep.symbolicRk = "+X";
                    curFwdDiffUnit().setExitBlock(exitOfFwdSweep);
                    // Create and fill the EntryBlock of the curDiffUnit:
                    EntryBlock entryOfBwdSweep = new EntryBlock(diffSymbolTable);
                    entryOfBwdSweep.symbolicRk = "-E";
                    Tree diffCallTree =
                            procedureCallDifferentiator().differentiateProcedureHeader(entryBlock.headTree(), null, null, null,
                                    diffSymbolTable, diffSymbolTable, DiffConstants.ADJOINT_BWD, toDuplicateVars);
                    entryOfBwdSweep.addInstrTl(diffCallTree);
                    curDiffUnit().setEntryBlock(entryOfBwdSweep);
                    curDiffUnit().setName(ILUtils.getCalledNameString(diffCallTree));
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace(" -> fwdBlock:" + exitOfFwdSweep + " : " + exitOfFwdSweep.instructions);
                        TapEnv.printlnOnTrace(" -> bwdBlock:" + entryOfBwdSweep + " : " + entryOfBwdSweep.instructions);
                    }
                }
                if (adEnv.traceCurDifferentiation) {
                    adEnv.traceCurBlock = 0;
                }
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(Precompute differentiated Blocks) Not implemented for groups of kind: " + kind);
        }
    }

    /**
     * Build the fwd and bwd sweeps that compose the (tangent or DifferentiationConstants.ADJOINT) differentiation
     * of the sub-FlowGraph specified by the given "level", in the context "diffEnv".
     *
     * @param level   The FlowGraph level to differentiate.
     * @param diffEnv The environment/context for this reverse differentiation.
     * @return The handles to the fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateFlowGraphLevel(FlowGraphLevel level,
                                                                 FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("BEGIN DIFF FGL "+level+" CONTAINING "+level.entryPoint+"+"+level.contents);

            TapEnv.incrTraceIndent(2);
        }

        // First phase: build and fill all needed Blocks for the
        // differentiated code corresponding to the Blocks at current level.
        // Assume all diff of elementary Blocks are already built and filled
        // by precomputeDifferentiatedBlocks(), so that we know for sure when
        // a Block must be considered live and/or active.
        // Recursively calls itself for the deeper levels (loops, checkpointed parts,...),
        // through the calls to differentiateBinomialLoop(), differentiateNaturalLoop(), etc
        // that build and fill the deeper diff blocks and connect them
        // to one another inside their deeper level:

        TapList<FlowGraphLevel> subLevels = new TapList<>(level.entryPoint, level.contents);
        TapList<FlowGraphLevel> inSubLevels = subLevels;
        TapList<FlowGraphDifferentiation> hdSubResults = new TapList<>(null, null);
        TapList<FlowGraphDifferentiation> tlSubResults = hdSubResults;
        FlowGraphLevel subLevel;
        FlowGraphDifferentiation subResult;
        FlowGraphLevel entrySubLevel = null;
        FlowGraphDifferentiation entrySubResult = null;
        int kind;
        while (inSubLevels != null) {
            subLevel = inSubLevels.head;
            kind = subLevel.levelKind;

            if (!TapEnv.associationByAddress()) {
                // Detect when kind is BINOMIAL_LOOP:
                if ((kind == DiffConstants.DO_LOOP || kind == DiffConstants.WHILE_LOOP || kind == DiffConstants.TIMES_LOOP
                     || kind == DiffConstants.NATURAL_LOOP || kind == DiffConstants.BINOMIAL_LOOP)
                        && subLevel.entryBlock.headInstr() != null
                        && subLevel.entryBlock.headInstr().hasDirective(Directive.BINOMIALCKP) != null
                        && diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE) {
                    subLevel.levelKind = DiffConstants.BINOMIAL_LOOP;
                    kind = subLevel.levelKind;
                }
                if (subLevel.entryBlock.headInstr() != null
                    && subLevel.entryBlock.headInstr().hasDirective(Directive.BINOMIALCKP) != null
                    && diffEnv.adDiffMode!=DiffConstants.TANGENT_MODE
                    && kind!=DiffConstants.BINOMIAL_LOOP) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, subLevel.entryBlock.headInstr().tree, "(AD19) Cannot use binomial checkpointing in adjoint split context");
                }
            } else if (kind == DiffConstants.FIXEDPOINT_LOOP) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, subLevel.entryBlock.headInstr().tree,
                        "(AD19) Cannot use FIXEDPOINT_LOOP with association by address");
                TapEnv.tapenadeExit(1);
            }
            // Reject II_LOOP kind when loop is not clean enough:
            if (subLevel.entryBlock != null && subLevel.entryBlock.instructions != null) {
                if (subLevel.entryBlock.headInstr().hasDirective(Directive.IILOOP) != null
                        && kind != DiffConstants.II_LOOP) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, subLevel.entryBlock.headInstr().tree, "(AD20) Cannot use II-LOOP directive here");
                }
            }
            // Kinds BINOMIAL_LOOP, II_LOOP, CHECKPOINT make no sense in TANGENT_MODE
            // => consider them as DO_LOOP, DO_LOOP, PLAIN_GROUP respectively.
            if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
                if (kind == DiffConstants.BINOMIAL_LOOP || kind == DiffConstants.II_LOOP) {
                    kind = DiffConstants.DO_LOOP;
                }
                if (kind == DiffConstants.CHECKPOINT) {
                    kind = DiffConstants.PLAIN_GROUP;
                }
            }

            switch (kind) {
                case DiffConstants.BINOMIAL_LOOP:
                    subResult = differentiateBinomialLoop(subLevel, diffEnv);
                    break;
                case DiffConstants.NATURAL_LOOP:
                    subResult = differentiateNaturalLoop(subLevel, diffEnv);
                    break;
                case DiffConstants.FIXEDPOINT_LOOP:
                    subResult = differentiateFixedPointLoop(subLevel, diffEnv);
                    break;
                case DiffConstants.TIMES_LOOP:
                case DiffConstants.WHILE_LOOP:
                case DiffConstants.DO_LOOP:
                    subResult = differentiateStructuredLoop(subLevel, diffEnv);
                    break;
                case DiffConstants.II_LOOP:
                    subResult = differentiateIILoop(subLevel, diffEnv);
                    break;
                case DiffConstants.MULTITHREAD_REGION:
                    subResult = differentiateMultithreadRegion(subLevel, diffEnv);
                    break;
                case DiffConstants.CHECKPOINT:
                    subResult = differentiateCkpPiece(subLevel, diffEnv);
                    break;
                case DiffConstants.NODIFF:
                    subResult = differentiateNoDiffPiece(subLevel, diffEnv);
                    break;
                case DiffConstants.LABELLED_REGION:
                    subResult = differentiateLabelledRegion(subLevel, diffEnv);
                    break;
                case DiffConstants.PLAIN_GROUP:
                    subResult = differentiateFlowGraphLevel(subLevel, diffEnv);
                    break;
                case DiffConstants.PLAIN_BLOCK:
                    subResult = differentiatePlainBlockLevel(subLevel, diffEnv);
                    break;
                case DiffConstants.ENTRY_BLOCK:
                    subResult = differentiateEntryBlockLevel(subLevel, diffEnv);
                    entrySubLevel = subLevel;
                    entrySubResult = subResult;
                    break;
                default:
                    TapEnv.toolWarning(-1, "(Differentiate groups of Blocks) Not implemented for groups of kind: " + kind);
                    subResult = new FlowGraphDifferentiation(subLevel);
            }
            tlSubResults = tlSubResults.placdl(subResult);
            inSubLevels = inSubLevels.tail;
        }
        hdSubResults = hdSubResults.tail;

        // Special treatment for the EntryBlock, to avoid placing push control
        // statements before the declarations (cf f77:lh26). We artificially set the
        // Push place of the entry to the danglingFwd of the NEXT Block !
        // Attention, in TANGENT, this creates wrong loops BB"0+"==0null==>BB"0+" e.g. on F90:lha02
        if (entrySubLevel != null && diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
            Block entryBlock = entrySubLevel.entryBlock;
            Block nextBlock = entryBlock.flow().head.destination;
            if (nextBlock.backFlow().tail == null && nextBlock.flow() != null && nextBlock.flow().tail == null) {
                FlowGraphLevel nextLevel = level.getRepresentantInLevel(nextBlock);
                FlowGraphDifferentiation nextResult = getSubLevelDifferentiation(nextLevel, level, hdSubResults);
                if (!nextResult.hasBwd() && nextResult.hasFwd()) {
                    TapPair<FGArrow, TapList<BwdSwitchCase>> entryOneFwdBwd = entrySubResult.getFwdBwdsOfExitArrow(entryBlock.flow().head).head;
                    BwdSwitchCase entryBwdSwitchCase =
                            entryOneFwdBwd.second.head;
                    TapPair<FGArrow, TapList<BwdSwitchCase>> nextOneFwdBwd = nextResult.getFwdBwdsOfExitArrow(nextBlock.flow().head).head;
                    entryBwdSwitchCase.fwdArrow = nextOneFwdBwd.first;
                }
            }
        }

        // Second phase: connect the differentiated blocks (at the current
        // level) with one another and with the differentiated deeper levels.
        // In general, this is done only for those "block" that are active,
        // i.e. that have a non-empty bwd block.
        // Must also be done for blocks that finish with a STOP.
        // This must be done for every "block" in TANGENT mode or in special (debug) situations:
        // dot-product test on this unit OR force diff reinitializations
        // OR keep dead control OR force all DifferentiationConstants.ADJOINTs.

        FlowGraphDifferentiation result = new FlowGraphDifferentiation(level);
        // The result's fwdBlockUS and bwdArrowsUS will be filled during
        // the calls to exploreConvergingBackFlow(),
        result.upstreamFwdBwdDefined = false;
        result.specialTopUsed = false;
        result.specialTopBwdArrow = null;
        result.specialTopFwdArrow = null;
        if (level.hasCyclingArrows()) {
            // If "level" (which is the enclosing level) has internal cycle
            // arrows reaching its entry, in addition to its standard entryArrows,
            // then if the level's entryPoint hasFwd() or hasBwd(), we must prepare
            // new (empty) fwdBlock and bwdBlock of the entryBlock to make the link
            // to the diff of outside levels upstream which are not built yet.
            subResult = hdSubResults.head;
            if (subResult.hasFwd()) {
                Block emptyFwdBlock = createFwdDiffBlock(level.entryBlock, diffEnv, false);
                emptyFwdBlock.symbolicRk = emptyFwdBlock.symbolicRk + "Cy";
                result.specialTopFwdArrow = checkFreeThenSetNewFGArrow(emptyFwdBlock, FGConstants.NO_TEST, 0, null, false);
                if (subResult.hasBwd()) {
                    Block emptyBwdBlock = createBwdDiffBlock(level.entryBlock, diffEnv, false);
                    emptyBwdBlock.symbolicRk = emptyBwdBlock.symbolicRk + "Cy";
                    result.specialTopBwdArrow = checkFreeThenSetNewFGArrow(emptyBwdBlock, FGConstants.NO_TEST, 0, null, false);
                }
            }
        }
        boolean[] arrowVisited = new boolean[adEnv.curUnit().nbArrows];
        TapList<FlowGraphDifferentiation> inSubResults = hdSubResults;
        TapList<FGArrow> entryArrows;
        TapList<BwdSwitchCase> totalBwdSwitchCases;
        TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
        inSubLevels = subLevels;
        while (inSubLevels != null) {
            subLevel = inSubLevels.head;
            subResult = inSubResults.head;
            // Starting (back) from each subLevel that has a bwd counterpart
            //  (to put it simply, this means the subLevel is "active")...
            if (keepEmptyDiffBlocks || subLevel.exitArrows == null
                    || subResult.hasBwd() || diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
                // follow the source control flow backwards, to reach
                // other subLevels with a bwd counterpart.
                totalBwdSwitchCases = null;
                entryArrows = subLevel.entryArrows;
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.indentOnTrace();
                    TapEnv.printlnOnTrace("Connecting entries " + entryArrows + " of " + subLevel
                            + " (upstream result for " + level
                            + " Fwd:" + result.fwdBlockUS + " Bwd:" + result.bwdArrowsUS + ')');
                }
                while (entryArrows != null) {
                    // Reset "arrowVisited" before this new traversal of the Flow Graph:
                    for (int i = arrowVisited.length - 1; i >= 0; --i) {
                        arrowVisited[i] = false;
                    }
                    manyFwdBwds =
                            exploreConvergingBackFlow(entryArrows.head, true, level, result, diffEnv, hdSubResults,
                                    arrowVisited, subResult.fwdBlockUS, subResult.bwdArrowsUS);
                    while (manyFwdBwds != null) {
                        oneFwdBwd = manyFwdBwds.head;
                        totalBwdSwitchCases =
                                TapList.union(totalBwdSwitchCases, oneFwdBwd.second);
                        manyFwdBwds = manyFwdBwds.tail;
                    }
                    entryArrows = entryArrows.tail;
                }
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.indentOnTrace();
                    TapEnv.printlnOnTrace("==> collected BwdSwitchCases: " + totalBwdSwitchCases);
                }
                // Detecting when subLevel.entryBlock is a WILD_LOOP "DO i", in which case we may
                //  need to save and restore its index "i" !
                // Notice that we cannot leave this task to the usual TBR mechanism
                // which is implemented in blockDifferentiator().differentiateBlock(),
                // because we can't place the PUSH before the Fwd do statement,
                // which must be alone in its own Fwd HeaderBlock.
                Block sourceDest = subLevel.entryBlock;
                Tree doIndexToSave = null;
                if (subLevel.levelKind == DiffConstants.PLAIN_BLOCK && sourceDest.instructions != null) {
                    Tree firstLoopInstr = sourceDest.headInstr().tree;
                    if (firstLoopInstr.opCode() == ILLang.op_loop
                            && firstLoopInstr.down(3).opCode() == ILLang.op_do
                            && ADTBRAnalyzer.isTBRindex(adEnv.curActivity(), firstLoopInstr.down(3).down(1))) {
                        doIndexToSave = firstLoopInstr.down(3).down(1);
                    }
                }
                connectFwdBwd(buildFwdBwdTreeFromOneFwdBwd(null, totalBwdSwitchCases),
                        subResult.fwdBlockUS, subResult.bwdArrowsUS, subLevel.entryBlock, doIndexToSave, diffEnv, level);
            } else {
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.indentOnTrace();
                    TapEnv.printlnOnTrace("Skipping connections detection up from subLevel:" + subLevel + " because its bwd is empty");
                }
            }
            inSubLevels = inSubLevels.tail;
            inSubResults = inSubResults.tail;
        }
        // Prepare the result for the Flow Graph building at the enclosing level:
        // starting from each individual FGArrow exiting the given "level",
        // follow the source control flow backwards, and attach the information
        // collected to the "exitArrowsFwdBwds" field of the result:
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                result.exitArrowsFwdBwds;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentOnTrace();
            TapEnv.printlnOnTrace("Exploring exits " + level.exitArrows + " of level " + level
                    + " (upstream result for " + level
                    + " Fwd:" + result.fwdBlockUS + " Bwd:" + result.bwdArrowsUS + ')');
        }
        FGArrow exitArrow;
        while (exitArrowsFwdBwds != null) {
            arrowFwdBwds = exitArrowsFwdBwds.head;
            exitArrow = arrowFwdBwds.first;
            // Don't explore through exitArrow if it comes from a vanishingLoopHeader
            // and therefore is temporarily neglected:
            if (!(exitArrow.origin instanceof HeaderBlock
                    && exitArrow.origin == level.headerBlockPlain
                    && exitArrow.origin.headInstr().tree.opCode() == ILLang.op_loop)) {
                // Reset "arrowVisited" before this new traversal of the Flow Graph:
                for (int i = arrowVisited.length - 1; i >= 0; --i) {
                    arrowVisited[i] = false;
                }
                arrowFwdBwds.second =
                        exploreConvergingBackFlow(exitArrow, false, level, result, diffEnv, hdSubResults,
                                arrowVisited, null, null);
            }
            exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>END DIFF FGL " + level);
            TapEnv.indentprintlnOnTrace("  -UPSTREAM: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -DNSTREAM: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build a copy of the FlowGraph level specified by the given "copyEnv".
     *
     * @param level   The FlowGraph level to copy.
     * @param copyEnv The environment/context for this copy.
     * @return The handles to the copy.
     */
    private FlowGraphCopy copyFlowGraphLevel(FlowGraphLevel level,
                                             FlowGraphCopyEnv copyEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("BEGIN COPY FGL " + level + " CONTAINING " + level.entryPoint + "+" + level.contents);
            TapEnv.incrTraceIndent(2);
        }

        // Flatten the tree of nested levels to be copied:
        TapList<Block> allBlocks = level.allBlocksInside();
        TapList<Block> inAllBlocks = allBlocks;
        TapList<TapPair<Block, Block>> toCopies = null;

        Block vanishingLoopHeader = null;
        if (level.headerBlockPlain != null) {
            Instruction loopInstr = level.headerBlockPlain.headInstr();
            if (loopInstr != null && loopInstr.tree.opCode() == ILLang.op_loop) {
                vanishingLoopHeader = level.headerBlockPlain;
            }
        }

        // First phase: build and fill all copied Blocks,
        // regardless of the nesting level of the "currentDiffLevel",
        // because inside a copy there can be only copies.
        Block block, copyBlock;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            copyBlock = createCopyBlock(block, copyEnv);
            copyBlock.copyInstructions(block, adEnv.copiedIncludes);
            setCopyCallArrows(block);
            // If block is a do-header which must become a plain block, make it a "if" :
            if (block == vanishingLoopHeader) {
                turnVanishingLoopIntoIf(copyBlock);
            }
            toCopies = new TapList<>(new TapPair<>(block, copyBlock), toCopies);
            inAllBlocks = inAllBlocks.tail;
        }
        FlowGraphCopy result = new FlowGraphCopy(level);
        result.copyBlockUS =
                TapList.cassq(level.entryBlock, toCopies);

        // Second phase: connect the copied Blocks with FGArrow's.
        TapList<FGArrow> arrows;
        FGArrow arrow, copyArrow;
        Block origCopy;
        inAllBlocks = allBlocks;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            copyBlock = TapList.cassq(block, toCopies);
            arrows = block.backFlow();
            while (arrows != null) {
                arrow = arrows.head;
                if (level.containsArrowFlow(arrow)) {
                    origCopy = TapList.cassq(arrow.origin, toCopies);
                    copyArrow = buildCopiedArrow(arrow, origCopy, copyBlock, vanishingLoopHeader);
                }
                arrows = arrows.tail;
            }
            inAllBlocks = inAllBlocks.tail;
        }
        TapPair<FGArrow, FGArrow> exitArrowCopy;
        TapList<TapPair<FGArrow, FGArrow>> inExitArrowsCopy = result.exitArrowsCopy;
        while (inExitArrowsCopy != null) {
            exitArrowCopy = inExitArrowsCopy.head;
            arrow = exitArrowCopy.first;
            origCopy = TapList.cassq(arrow.origin, toCopies);
            copyArrow = buildCopiedArrow(arrow, origCopy, null, vanishingLoopHeader);
            exitArrowCopy.second = copyArrow;
            inExitArrowsCopy = inExitArrowsCopy.tail;
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>END COPY FGL " + level + " copyEntry:" + result.copyBlockUS + " exitArrowsCopy:" + result.exitArrowsCopy);
        }

        return result;
    }

    /**
     * Place the CallArrow's in the diffCallGraph due to code in a block which is copied from source code.
     */
    private void setCopyCallArrows(Block sourceBlock) {
        Tree instrTree, callTree;
        Unit calledUnit;
        for (TapList<Instruction> inInstructions = sourceBlock.instructions; inInstructions != null; inInstructions = inInstructions.tail) {
            instrTree = inInstructions.head.tree;
            if (instrTree.opCode() == ILLang.op_call) {
                callTree = instrTree;
            } else if (instrTree.opCode() == ILLang.op_assign && instrTree.down(2).opCode() == ILLang.op_call) {
                callTree = instrTree.down(2);
            } else {
                callTree = null;
            }
            if (callTree != null) {
                calledUnit = DataFlowAnalyzer.getCalledUnit(callTree, sourceBlock.symbolTable);
                if (!calledUnit.isIntrinsic()) {
                    Unit primalCalledUnit = adEnv.getPrimalCopyOfOrigUnit(calledUnit);
                    // If the called routine has no primal copy, we place a CallArrow to the source called routine
                    // Danger [19Mar18]: this is the 1st example of a CallArrow between 2 different CallGraph's...
                    // [vmp] cf auroux-striation -nooptim activity
                    if (primalCalledUnit == null) {
                        primalCalledUnit = calledUnit;
                    }
                    CallGraph.addCallArrow(curDiffUnit(), SymbolTableConstants.CALLS, primalCalledUnit);
                }
            }
        }
    }

    /**
     * Special differentiation for the EntryBlock of a Unit.
     */
    private FlowGraphDifferentiation differentiateEntryBlockLevel(FlowGraphLevel entryBlockLevel,
                                                                  FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff ENTRY_BLOCK " + entryBlockLevel);
            TapEnv.incrTraceIndent(2);
        }
        // Prepare the result for the Flow Graph building at the enclosing level:
        EntryBlock entryBlock = (EntryBlock) entryBlockLevel.entryBlock;
        EntryBlock entryOfFwdSweep = (EntryBlock) diffEnv.fwdDiffBlocks.retrieve(entryBlock);
        ExitBlock exitOfBwdSweep = (ExitBlock) diffEnv.bwdDiffBlocks.retrieve(entryBlock);
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(entryBlockLevel);
        result.fwdBlockUS = entryOfFwdSweep;
        result.bwdArrowsUS = new TapList<>(checkFreeThenSetNewFGArrow(exitOfBwdSweep, FGConstants.NO_TEST, 0, null, false), null);
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds = result.exitArrowsFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        FGArrow exitArrow, fwdArrow;
        while (exitArrowsFwdBwds != null) {
            arrowFwdBwds = exitArrowsFwdBwds.head;
            exitArrow = arrowFwdBwds.first;
            fwdArrow = checkFreeThenSetNewFGArrow(entryOfFwdSweep, exitArrow.test, exitArrow.cases, null, false);
            insertTangentDiffReinits(fwdArrow, exitArrow,
                    curFwdDiffUnit().publicSymbolTable(), null, diffEnv);
            arrowFwdBwds.second =
                    new TapList<>(new TapPair<>(fwdArrow,
                            new TapList<>(new BwdSwitchCase(fwdArrow, exitOfBwdSweep,
                                    new TapList<>(entryBlock, null), exitArrow.destination),
                                    null)),
                            null);
            exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff ENTRY_BLOCK " + entryBlockLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the fwd and bwd diff blocks of a given plain Block.
     * Builds their exit arrows and store the entry and exit points
     * for the diff blocks into the given diffEnv.
     *
     * @param plainLeafLevel The leaf "PLAIN_BLOCK" FlowGraphLevel that contains block.
     * @param diffEnv        The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    @SuppressWarnings("checkstyle:MultipleVariableDeclarations")
    private FlowGraphDifferentiation differentiatePlainBlockLevel(FlowGraphLevel plainLeafLevel,
                                                                  FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff PLAIN_BLOCK " + plainLeafLevel + " instrs:" + plainLeafLevel.entryBlock.instructions);
            TapEnv.incrTraceIndent(2);
        }

        Block block = plainLeafLevel.entryBlock;
        boolean vanishingLoopHeader = false;
        if (block instanceof HeaderBlock && block == plainLeafLevel.enclosing.headerBlockPlain) {
            Instruction loopInstr = block.headInstr();
            if (loopInstr.tree.opCode() == ILLang.op_loop) {
                vanishingLoopHeader = true;
            }
        }

        Block fwdDiffBlock;
        Block bwdDiffBlock;
        boolean lastTestIsLive;
        if (block.rank == -99) {
            //This is a new temporary block, not prepared by precomputeDifferentiatedBlocks().
            fwdDiffBlock = createFwdDiffBlock(block, diffEnv,
                    block instanceof HeaderBlock && block != plainLeafLevel.enclosing.headerBlockPlain);
            bwdDiffBlock = createBwdDiffBlock(block, diffEnv, false);
            blockDifferentiator().differentiateBlock(block, fwdDiffBlock, bwdDiffBlock, diffEnv.adDiffMode, null);
            if (vanishingLoopHeader) {
                fwdDiffBlock.instructions = TapList.removeLast(fwdDiffBlock.instructions);
            }
            lastTestIsLive = true;
        } else {
            fwdDiffBlock = diffEnv.fwdDiffBlocks.retrieve(block);
            bwdDiffBlock = diffEnv.bwdDiffBlocks.retrieve(block);
            lastTestIsLive = adEnv.curUnitIsContext || diffEnv.blockHasLastTestLive(block);
        }

        boolean hasBwd = diffEnv.adDiffMode != DiffConstants.TANGENT_MODE
                && !adEnv.curUnitIsContext
                && (keepEmptyDiffBlocks
                || diffEnv.blockMustBeActive(block)
                || bwdDiffBlock.instructions != null
                || keepBwdBecauseOfTwoExits(block, fwdDiffBlock, diffEnv)
                || keepBwdBecauseOfDiffReinits(block));
        boolean hasFwd = hasBwd || diffEnv.blockMustBeLive(block)
                || diffEnv.adDiffMode == DiffConstants.TANGENT_MODE
                || adEnv.curUnitIsContext
                || fwdDiffBlock.instructions != null
                || fwdDiffBlock==fwdDiffBlock.symbolTable.declarationsBlock ;

        // Prepare the result for the Flow Graph building at the enclosing level:
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(plainLeafLevel);
        if (vanishingLoopHeader) {
            result.removeLoopExitArrow();
        }
        if (hasBwd) {
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdDiffBlock, FGConstants.NO_TEST, 0, null, false), null);
        }
        if (hasFwd) {
            result.fwdBlockUS = fwdDiffBlock;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
            FGArrow exitArrow, fwdArrow;
            FGArrow uniqueArrow = null;
            boolean diffInitDoneUnique = false;
            if (exitArrowsFwdBwds != null) {
                if (vanishingLoopHeader || !lastTestIsLive) {
                    uniqueArrow = checkFreeThenSetNewFGArrow(fwdDiffBlock, FGConstants.NO_TEST, 0, null, false);
                }
                while (exitArrowsFwdBwds != null) {
                    arrowFwdBwds = exitArrowsFwdBwds.head;
                    exitArrow = arrowFwdBwds.first;
                    if (uniqueArrow != null) {
                        fwdArrow = uniqueArrow;
                    } else {
                        fwdArrow = checkFreeThenSetNewFGArrow(fwdDiffBlock, exitArrow.test, exitArrow.cases,
                                null, exitArrow.inACycle);
                        fwdArrow.isAJumpIntoNextCase = exitArrow.isAJumpIntoNextCase;
                        // We may need to add code into a CASE:DEFAULT case e.g. to reinit diff vars:
                        fwdArrow.turnNomatchIntoDefault();
                    }
                    // If necessary, insert a block here to initialize tangent derivatives:
                    if (!diffInitDoneUnique) {
                        insertTangentDiffReinits(fwdArrow, exitArrow,
                                fwdDiffBlock.symbolTable, fwdDiffBlock, diffEnv);
                        if (uniqueArrow != null) {
                            diffInitDoneUnique = true;
                        }
                    }
                    arrowFwdBwds.second =
                            new TapList<>(
                                    new TapPair<>(fwdArrow,
                                            hasBwd ? new TapList<>(new BwdSwitchCase(fwdArrow, bwdDiffBlock, new TapList<>(block, null), exitArrow.destination), null) : null),
                                    null);
                    exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                }
            }
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff PLAIN_BLOCK " + plainLeafLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the differentiated control flow for a MULTITHREAD_REGION,
     * (which so far means an OpenMP parallel region or loop)
     * This builds:<br>
     * <pre>
     *   $OMP PARALLEL: FWD or COPY pragma clauses
     *   FWD or TGT of region body
     *   -------------------------
     *   $OMP PARALLEL: BWD pragma clauses
     *   BWD of region body
     * </pre>.
     *
     * @param regionLevel The sub tree for this II_LOOP in the tree of diff levels.
     * @param diffEnv     The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateMultithreadRegion(
            FlowGraphLevel regionLevel,
            FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff MULTITHREAD REGION " + regionLevel);
            TapEnv.incrTraceIndent(2);
        }

        Block block = regionLevel.entryBlock;
        Tree pragma = block.headInstr().tree;
        boolean modeIsAdjoint = diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE
                || diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE;
        FlowGraphDifferentiation result;
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;

        // Recursively differentiate the control flow of the enclosed code (including region's entryBlock!):
        precomputeDifferentiatedBlocksIn(regionLevel, diffEnv);
        FlowGraphDifferentiation regionBodyDifferentiation =
                differentiateFlowGraphLevel(regionLevel.createRegionBody(), diffEnv);

        // Creating regionLevel.createRegionBody() has damaged regionLevel => repair it now.
        regionLevel.restoreChildrenParent();
        Tree srcSchedule = pragma.getAnnotation("srcSchedule");

        // If we are adjoinning an OpenMP "$OMP DO", we apply a special mechanism
        //  to reuse the FWD thread schedule as the BWD thread schedule:
        if (modeIsAdjoint && regionBodyDifferentiation.hasBwd()
                && pragma.opCode() == ILLang.op_parallelLoop
                && ILUtils.isIdent(pragma.down(1), "omp", false)) {
            // Our choice about schedule: if primal is explicitly static, then adjoint is static, otherwise dynamic.
            if (ILUtils.isStaticSchedule(srcSchedule)) {
                result = differentiateOMPLoopScheduleAdjointStatic(regionLevel, diffEnv, regionBodyDifferentiation);
            } else {
                result = differentiateOMPLoopScheduleAdjointDynamic(regionLevel, diffEnv, regionBodyDifferentiation, srcSchedule);
            }
        } else {
            if (pragma.opCode() == ILLang.op_parallelLoop
                    && ILUtils.isIdent(pragma.down(1), "omp", false)) {
                result = differentiateOMPLoopTangent(regionLevel, diffEnv, regionBodyDifferentiation, srcSchedule);
            } else {
                Block fwdDiffBlock = diffEnv.fwdDiffBlocks.retrieve(block);
                Block bwdDiffBlock = diffEnv.bwdDiffBlocks.retrieve(block);
                SymbolTable fwdDiffSymbolTable = fwdDiffBlock.symbolTable;
                SymbolTable diffSymbolTable = (bwdDiffBlock == null ? null : bwdDiffBlock.symbolTable);
                Block firstBlockInRegion = block.flow().head.destination;

                // The fwd Block just before the exit from this region
                // Used to push private TBR primals before they "vanish".
                Block fwdPreExit = buildFwdBlock(block.rank+"+preX", fwdDiffSymbolTable, firstBlockInRegion, diffEnv) ;

                Block bwdPostExit = null;
                if (modeIsAdjoint) {
                    // The bwd Block just after exiting from the region
                    bwdPostExit = buildBwdBlock(block.rank+"-postX", diffSymbolTable, block, diffEnv) ;
                }

                // Put back the source schedule, if any
                if (srcSchedule != null && fwdDiffBlock.headInstr() != null) {
                    ILUtils.addSchedule(ILUtils.copy(srcSchedule), fwdDiffBlock.headInstr().tree);
                }

                // Prepare the result for the Flow Graph building at the enclosing level:
                result = new FlowGraphDifferentiation(regionLevel);
                if (regionBodyDifferentiation.hasBwd()) {
                    assert bwdDiffBlock != null;
                    // The most general case:
                    checkFreeThenSetNewFGArrow(fwdDiffBlock, FGConstants.NO_TEST, 0, regionBodyDifferentiation.fwdBlockUS, false);

                    // Insert requested pre- and post-initializations to zero of adjoint of private vars:
                    TapList<Tree> privatePreInits = null, privatePostInits = null;
                    if (bwdDiffBlock.headInstr() != null) {
                        TapPair<TapList<Tree>, TapList<Tree>> privateInitsAnnotation =
                                bwdDiffBlock.headInstr().tree.getRemoveAnnotation("bwdOMPPrivateInits");
                        if (privateInitsAnnotation != null) {
                            privatePreInits = privateInitsAnnotation.first;
                            privatePostInits = privateInitsAnnotation.second;
                        }
                        // Put back the source schedule, if any
                        if (srcSchedule != null) {
                            ILUtils.addSchedule(ILUtils.copy(srcSchedule), bwdDiffBlock.headInstr().tree);
                        }
                    }
                    while (privatePostInits != null) {
                        bwdPostExit.addInstrTl(privatePostInits.head);
                        privatePostInits = privatePostInits.tail;
                    }

                    Block afterBwdEntry = bwdDiffBlock;
                    if (privatePreInits != null
                            || pragma.opCode() == ILLang.op_parallelRegion) {
                        Block bwdPostEntry = buildBwdBlock(block.rank+"-postE", diffSymbolTable, firstBlockInRegion, diffEnv);
                        while (privatePreInits != null) {
                            bwdPostEntry.addInstrTl(privatePreInits.head);
                            privatePreInits = privatePreInits.tail;
                        }
                        checkFreeThenSetNewFGArrow(afterBwdEntry, FGConstants.NO_TEST, 0, bwdPostEntry, false);
                        savePrivatesBeforeTheyVanish(fwdPreExit, bwdPostEntry, regionLevel);
                        afterBwdEntry = bwdPostEntry;
                    }

                    if (pragma.opCode() == ILLang.op_parallelRegion) {
                        Tree fwdParallelRegion = fwdDiffBlock.headInstr().tree;
                        Tree bwdParallelRegion = bwdDiffBlock.headInstr().tree;
                        TapList<Tree> newPrivateVars, newSharedVars, newSumReductionVars;
                        // New shared clauses for the FWD sweep (primal variables only):
                        newSharedVars =
                                ILUtils.extractSharedTrees(pragma, fwdDiffSymbolTable, true, false);
                        if (newSharedVars != null) {
                            fwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_sharedVars,
                                            ILUtils.build(ILLang.op_idents, newSharedVars)),
                                    -1);
                        }
                        // New shared clauses for the BWD sweep, for primal variables:
                        newSharedVars =
                                ILUtils.extractSharedTrees(pragma, diffSymbolTable, false, false);
                        if (newSharedVars != null) {
                            bwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_sharedVars,
                                            ILUtils.build(ILLang.op_idents, newSharedVars)),
                                    -1);
                        }
                        // New private clauses for the FWD sweep (primal variables only):
                        if (ompChunksOriginalIndexFwd != null) {
                            makeSurePrivate(ompChunksOriginalIndexFwd, fwdParallelRegion.down(2));
                        }
                        newPrivateVars =
                                ILUtils.extractPrivateTrees(pragma, fwdDiffSymbolTable, true, false);
                        if (newPrivateVars != null) {
                            fwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_privateVars,
                                            ILUtils.build(ILLang.op_idents, newPrivateVars)),
                                    -1);
                        }
                        // New private clauses for the BWD sweep, for primal variables:
                        if (ompChunksOriginalIndex != null) {
                            makeSurePrivate(ompChunksOriginalIndex, bwdParallelRegion.down(2));
                        }
                        newPrivateVars =
                                ILUtils.extractPrivateTrees(pragma, diffSymbolTable, false, false);
                        if (newPrivateVars != null) {
                            bwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_privateVars,
                                            ILUtils.build(ILLang.op_idents, newPrivateVars)),
                                    -1);
                        }
                        // New private clauses for the BWD sweep, for diff variables:
                        newPrivateVars =
                                ILUtils.extractPrivateTrees(pragma, diffSymbolTable, false, true);
                        newPrivateVars = varRefDifferentiator().mapDiffVarRef(
                                adEnv.curActivity(), newPrivateVars, diffSymbolTable);
                        if (newPrivateVars != null) {
                            bwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_privateVars,
                                            ILUtils.build(ILLang.op_idents, newPrivateVars)),
                                    -1);
                        }
                        // New REDUCTION(+:...) clauses for the BWD sweep, for diff variables:
                        newSumReductionVars =
                                ILUtils.extractSumReductionTrees(pragma, diffSymbolTable);
                        // Note: these variables are already differentiated!
                        if (newSumReductionVars != null) {
                            bwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_reductionVars,
                                            ILUtils.build(ILLang.op_ident, "+"),
                                            ILUtils.build(ILLang.op_idents, newSumReductionVars)),
                                    -1);
                        }
                    }

                    Block newFwdEntry = fwdDiffBlock ;
                    Block newBwdEntry = bwdDiffBlock ;
                    // Only in the Cuda case, two blocks to push (resp. pop) snapshot before fwd (resp. bwd) Cuda call:
                    if (ILUtils.isIdent(pragma.down(1), "cuda", false)) {
                        Block srcCudaCallBlock = regionLevel.contents.head.entryBlock;
                        Block fwdPreCuda = buildFwdBlock(block.rank+"+preCuda", fwdDiffSymbolTable, block, diffEnv) ;
                        movePreCudaCode(diffEnv.fwdDiffBlocks.retrieve(srcCudaCallBlock), fwdPreCuda) ;
                        checkFreeThenSetNewFGArrow(fwdPreCuda, FGConstants.NO_TEST, 0, fwdDiffBlock, false) ;
                        newFwdEntry = fwdPreCuda ;
                        Block bwdPreCuda = buildBwdBlock(block.rank+"-preCuda", diffSymbolTable, block, diffEnv) ;
                        movePreCudaCode(diffEnv.bwdDiffBlocks.retrieve(srcCudaCallBlock), bwdPreCuda) ;
                        checkFreeThenSetNewFGArrow(bwdPreCuda, FGConstants.NO_TEST, 0, bwdDiffBlock, false) ;
                        newBwdEntry = bwdPreCuda ;
                    }

                    // [llh] TODO: make sure that the intermediate blocks created by connectFwdBwd
                    // receive the correct parallelControls, i.e. they are INSIDE the parallelRegion!
                    PushPopNode pushPopTree =
                            buildFwdBwdTreeFromExitFwdBwd(regionBodyDifferentiation.exitArrowsFwdBwds);
                    connectFwdBwd(pushPopTree, fwdPreExit,
                            new TapList<>(checkFreeThenSetNewFGArrow(afterBwdEntry, FGConstants.NO_TEST, 0, null, false), null),
                            // TODO: Next "null" used to be "block". Current choice makes ompl10 work, but breaks onera06
                            null, null, diffEnv, regionLevel);
                    // TODO: retrieve the source block, entry point of the inside region, and pass it instead of the 
                    // following  block/*NO*/, to enable checkActivitySwitches upon exit of this parallel region.
                    connectBwd(regionBodyDifferentiation.bwdArrowsUS, bwdPostExit, false, block, block/*NO*/, diffEnv);
                    result.fwdBlockUS = newFwdEntry;
                    result.bwdArrowsUS =
                            new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
                    exitArrowsFwdBwds = result.exitArrowsFwdBwds;
                    FGArrow fwdArrow = checkFreeThenSetNewFGArrow(fwdPreExit, FGConstants.NO_TEST, 0, null, false);
                    FGArrow exitArrow;
                    while (exitArrowsFwdBwds != null) {
                        arrowFwdBwds = exitArrowsFwdBwds.head;
                        exitArrow = arrowFwdBwds.first;
                        arrowFwdBwds.second =
                                new TapList<>(
                                        new TapPair<>(fwdArrow,
                                                new TapList<>(
                                                        new BwdSwitchCase(fwdArrow, newBwdEntry, new TapList<>(exitArrow.origin, null), exitArrow.destination),
                                                        null)),
                                        null);
                        exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                    }
                } else if (regionBodyDifferentiation.reallyHasFwd()) {
                    // else tangent case or degenerate case where only the Fwd sweep body is necessary:
                    checkFreeThenSetNewFGArrow(fwdDiffBlock, FGConstants.NO_TEST, 0, regionBodyDifferentiation.fwdBlockUS, false);
                    // A fwd Block just after this exit from the region: (TODO: This Block may be unnecessary. Remove it?)
                    Block fwdPostExit = buildFwdBlock(block.rank + "+postX", fwdDiffSymbolTable, block, diffEnv) ;

                    exitArrowsFwdBwds = regionBodyDifferentiation.exitArrowsFwdBwds;
                    TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
                    while (exitArrowsFwdBwds != null) {
                        arrowFwdBwds = exitArrowsFwdBwds.head;
                        manyFwdBwds = arrowFwdBwds.second;
                        while (manyFwdBwds != null) {
                            checkFreeThenRedirectDestination(manyFwdBwds.head.first, fwdPostExit, false);
                            manyFwdBwds = manyFwdBwds.tail;
                        }
                        exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                    }

                    if (pragma.opCode() == ILLang.op_parallelRegion) {
                        Tree fwdParallelRegion = fwdDiffBlock.headInstr().tree;
                        TapList<Tree> newPrivateVars, newSharedVars;
                        // New shared clauses for the FWD sweep (primal variables only):
                        newSharedVars =
                                ILUtils.extractSharedTrees(pragma, fwdDiffSymbolTable, true, false);
                        if (newSharedVars != null) {
                            fwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_sharedVars,
                                            ILUtils.build(ILLang.op_idents, newSharedVars)),
                                    -1);
                        }
                        // New private clauses for the FWD sweep (primal variables only):
                        if (ompChunksOriginalIndexFwd != null) {
                            makeSurePrivate(ompChunksOriginalIndexFwd, fwdParallelRegion.down(2));
                        }
                        newPrivateVars =
                                ILUtils.extractPrivateTrees(pragma, fwdDiffSymbolTable, true, false);
                        if (newPrivateVars != null) {
                            fwdParallelRegion.down(2).addChild(
                                    ILUtils.build(ILLang.op_privateVars,
                                            ILUtils.build(ILLang.op_idents, newPrivateVars)),
                                    -1);
                        }
                    }

                    result.fwdBlockUS = fwdDiffBlock;
                    exitArrowsFwdBwds = result.exitArrowsFwdBwds;
                    FGArrow fwdArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
                    FGArrow exitArrow;
                    while (exitArrowsFwdBwds != null) {
                        arrowFwdBwds = exitArrowsFwdBwds.head;
                        exitArrow = arrowFwdBwds.first;
                        arrowFwdBwds.second =
                                new TapList<>(new TapPair<>(fwdArrow, null), null);
                        exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                    }
                } else {
                    // else very degenerate case where Fwd body and therefore also Bwd body are empty:
                    // final result has empty fwd and bwd sweeps !
                    result.fwdBlockUS = null;
                    result.bwdArrowsUS = null;
                }
            }
        }
        // Clean OMP-related annotations added to the OMP clause tree:
        pragma.removeAnnotation("onlyReadSmallActivePrimals");
        pragma.removeAnnotation("forcedScopingNames");
        pragma.removeAnnotation("srcSchedule");

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff MULTITHREAD REGION " + regionLevel + " hasFwd:" + regionBodyDifferentiation.reallyHasFwd() + " hasBwd:" + regionBodyDifferentiation.hasBwd());
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /** Move to preBlock all code in cudaCallBlock that is before its last call (which is a Cuda call) */
    private void movePreCudaCode(Block cudaCallBlock, Block preBlock) {
        if (cudaCallBlock.instructions!=null) {
            Instruction moved ;
            while (cudaCallBlock.instructions.tail!=null) {
                moved = cudaCallBlock.instructions.head ;
                cudaCallBlock.instructions = cudaCallBlock.instructions.tail ;
                moved.block = preBlock ;
                preBlock.instructions = TapList.addLast(preBlock.instructions, moved) ;
            }
        }
    }

    /**
     * Special code for tangent diff of an OpenMP parallel loop.
     */
    private FlowGraphDifferentiation differentiateOMPLoopTangent(
            FlowGraphLevel regionLevel,
            FlowGraphDifferentiationEnv diffEnv,
            FlowGraphDifferentiation regionBodyDifferentiation,
            Tree srcSchedule) {
        // Note: rather than creating a new FlowGraphDifferentiation, this method instead modifies
        // and returns the given "regionBodyDifferentiation", which is the FlowGraphDifferentiation
        // built for the body of this OMPLoop (which must be a standard loop).
        // We assume regionBodyDifferentiation has only one "DownStream" arrow.
        Block block = regionLevel.entryBlock;
        Block fwdDiffBlock = diffEnv.fwdDiffBlocks.retrieve(block);

        // Put back the source schedule, if any
        if (srcSchedule != null && fwdDiffBlock != null && fwdDiffBlock.headInstr() != null) {
            ILUtils.addSchedule(ILUtils.copy(srcSchedule), fwdDiffBlock.headInstr().tree);
        }

        if (regionBodyDifferentiation.reallyHasFwd()) {
            HeaderBlock insideLoopHeader = (HeaderBlock) regionLevel.entryBlock.flow().head.destination;
            HeaderBlock insideLoopFwdDiffBlock = (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(insideLoopHeader);
            if (regionBodyDifferentiation.fwdBlockUS == insideLoopFwdDiffBlock) {
                // If the UpStream of regionBodyDifferentiation is directly the fwd loop header:
                checkFreeThenSetNewFGArrow(fwdDiffBlock, FGConstants.NO_TEST, 0,
                        insideLoopFwdDiffBlock, false);
                regionBodyDifferentiation.fwdBlockUS = fwdDiffBlock;
            } else {
                insideLoopFwdDiffBlock.insertBlockBeforeLoopEntry(fwdDiffBlock);
            }
        }
        return regionBodyDifferentiation;
    }

    /**
     * Special code for the adjoint (FWD and BWD sweeps) of an OpenMP parallel loop,
     * that "manually" computes a static schedule and applies it to both sweeps.
     */
    private FlowGraphDifferentiation differentiateOMPLoopScheduleAdjointStatic(
            FlowGraphLevel regionLevel,
            FlowGraphDifferentiationEnv diffEnv,
            FlowGraphDifferentiation regionBodyDifferentiation) {
        // Note: instead of creating a new FlowGraphDifferentiation, this method instead modifies
        // and returns the given "regionBodyDifferentiation", which is the FlowGraphDifferentiation
        // built for the body of this OMPLoop (which must be a standard loop).
        // We assume regionBodyDifferentiation has only one "DownStream" arrow.
        // This current method is called only if regionBodyDifferentiation has a nonempty bwd sweep.
        Block block = regionLevel.entryBlock;
        Block fwdDiffBlock = diffEnv.fwdDiffBlocks.retrieve(block); // will not be connected!
        Block bwdDiffBlock = diffEnv.bwdDiffBlocks.retrieve(block); // will not be connected!
        SymbolTable fwdDiffSymbolTable = fwdDiffBlock.symbolTable;
        SymbolTable diffSymbolTable = (bwdDiffBlock == null ? null : bwdDiffBlock.symbolTable);

        if (adOmpChunkStartVarTool == null) {
            adOmpChunkStartVarTool =
                    new FwdBwdVarTool(adOmpChunkStartStr, null, block,
                            curFwdDiffUnit().privateSymbolTable(), curDiffUnit().privateSymbolTable());
        }
        if (adOmpChunkEndVarTool == null) {
            adOmpChunkEndVarTool =
                    new FwdBwdVarTool(adOmpChunkEndStr, null, block,
                            curFwdDiffUnit().privateSymbolTable(), curDiffUnit().privateSymbolTable());
        }

        HeaderBlock insideLoopHeader = (HeaderBlock) regionLevel.entryBlock.flow().head.destination;
        HeaderBlock insideLoopFwdDiffBlock = (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(insideLoopHeader);
        Tree insideFwdLoopDoTree = insideLoopFwdDiffBlock.headInstr().tree.down(3);
        Tree staticScheduleCall =
                ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "getStaticSchedule"),
                        ILUtils.build(ILLang.op_expressions,
                                ILUtils.copy(insideFwdLoopDoTree.down(2)),
                                ILUtils.copy(insideFwdLoopDoTree.down(3)),
                                (ILUtils.isNullOrNone(insideFwdLoopDoTree.down(4))
                                        ? ILUtils.build(ILLang.op_intCst, 1)
                                        : ILUtils.copy(insideFwdLoopDoTree.down(4))),
                                adOmpChunkStartVarTool.makeFwdRef(block),
                                adOmpChunkEndVarTool.makeFwdRef(block)));
        insideFwdLoopDoTree.setChild(adOmpChunkStartVarTool.makeFwdRef(block), 2);
        insideFwdLoopDoTree.setChild(adOmpChunkEndVarTool.makeFwdRef(block), 3);
        ompChunksOriginalIndexFwd = insideFwdLoopDoTree.down(1);

        HeaderBlock insideLoopBwdDiffBlock = (HeaderBlock) diffEnv.bwdDiffBlocks.retrieve(insideLoopHeader);
        Tree insideBwdLoopDoTree = insideLoopBwdDiffBlock.headInstr().tree.down(3);
        insideBwdLoopDoTree.setChild(adOmpChunkEndVarTool.makeRef(block), 2);
        insideBwdLoopDoTree.setChild(adOmpChunkStartVarTool.makeRef(block), 3);
        ompChunksOriginalIndex = insideBwdLoopDoTree.down(1);

        // The fwd Block just before entry into the fwd of the contained DO loop:
        Block fwdPreEntry = buildFwdBlock(block.rank+"+preE", fwdDiffSymbolTable, block, diffEnv) ;
        fwdPreEntry.addInstrTl(staticScheduleCall);

        // The bwd Block just before entry into the bwd of the contained DO loop:
        Block bwdPreEntry = buildBwdBlock(block.rank+"-preE", diffSymbolTable, block, diffEnv) ;
        bwdPreEntry.addInstrTl(ILUtils.copy(staticScheduleCall));

        if (regionBodyDifferentiation.fwdBlockUS == insideLoopFwdDiffBlock) {
            // If the UpStream of regionBodyDifferentiation is directly the fwd loop header:
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0,
                    insideLoopFwdDiffBlock, false);
            regionBodyDifferentiation.fwdBlockUS = fwdPreEntry;
        } else {
            insideLoopFwdDiffBlock.insertBlockBeforeLoopEntry(fwdPreEntry);
        }
        // Since the region body must be a plain loop, it has only one exit arrow:
        BwdSwitchCase singleBwdSwitchCase =
                regionBodyDifferentiation.exitArrowsFwdBwds.head.second.head.second.head;
        if (singleBwdSwitchCase.bwdBlock == insideLoopBwdDiffBlock) {
            // If the DownStream of regionBodyDifferentiation is directly the bwd loop header:
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0,
                    insideLoopBwdDiffBlock, false);
            singleBwdSwitchCase.bwdBlock = bwdPreEntry;
        } else {
            insideLoopBwdDiffBlock.insertBlockBeforeLoopEntry(bwdPreEntry);
        }
        return regionBodyDifferentiation;
    }

    /**
     * Special code for the adjoint (FWD and BWD sweeps) of an OpenMP parallel loop,
     * that dynamically records the FWD thread schedule and reuses it in the BWD sweep.
     */
    private FlowGraphDifferentiation differentiateOMPLoopScheduleAdjointDynamic(
            FlowGraphLevel regionLevel,
            FlowGraphDifferentiationEnv diffEnv,
            FlowGraphDifferentiation regionBodyDifferentiation,
            Tree srcSchedule) {
        // Note: instead of creating a new FlowGraphDifferentiation, this method instead modifies
        // and returns the given "regionBodyDifferentiation", which is the FlowGraphDifferentiation
        // built for the body of this OMPLoop (which must be a standard loop).
        // We assume regionBodyDifferentiation has only one "DownStream" arrow.
        // This current method is called only if regionBodyDifferentiation has a nonempty bwd sweep.
        Block block = regionLevel.entryBlock;
        Block fwdDiffBlock = diffEnv.fwdDiffBlocks.retrieve(block);
        Block bwdDiffBlock = diffEnv.bwdDiffBlocks.retrieve(block); // will not be connected!
        SymbolTable fwdDiffSymbolTable = fwdDiffBlock.symbolTable;
        SymbolTable diffSymbolTable = (bwdDiffBlock == null ? null : bwdDiffBlock.symbolTable);
        // The primal "OMP PARALLEL" Instruction, used to gather new OMP private variables:
        Instruction parallelRegionInstr = TapList.last(block.parallelControls);

        if (ompChunksNumVariable == null) {
            ompChunksNumVariable =
                    new NewSymbolHolder("numchunks", curDiffUnit(), adEnv.integerTypeSpec, -1);
        }
        ompChunksNumVariable.declarationLevelMustInclude(curDiffUnit().privateSymbolTable());
        ompChunksNumVariable.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
//         ILUtils.addVarIntoFuturePrivates(ompChunksNumVariable, parallelRegionInstr.tree, false, false) ;

        if (ompChunksIndexVariable == null) {
            ompChunksIndexVariable =
                    new NewSymbolHolder("ichunk", curDiffUnit(), adEnv.integerTypeSpec, -1);
        }
        ompChunksIndexVariable.declarationLevelMustInclude(curDiffUnit().privateSymbolTable());
        ompChunksIndexVariable.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
//         ILUtils.addVarIntoFuturePrivates(ompChunksIndexVariable, parallelRegionInstr.tree, false, false) ;

        if (adOmpChunkStartVarTool == null) {
            adOmpChunkStartVarTool =
                    new FwdBwdVarTool(adOmpChunkStartStr, null, block,
                            null, curDiffUnit().privateSymbolTable());
        }
        if (adOmpChunkEndVarTool == null) {
            adOmpChunkEndVarTool =
                    new FwdBwdVarTool(adOmpChunkEndStr, null, block,
                            null, curDiffUnit().privateSymbolTable());
        }

        // The fwd Block just before entry into the fwd diff "OMP DO" control Block:
        Block fwdPreEntry = buildFwdBlock(block.rank+"+preE",
                                    fwdDiffSymbolTable, block, diffEnv);
        fwdPreEntry.addInstrTl(
                ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "initDynamicSchedule"),
                        ILUtils.build(ILLang.op_expressions)));

        HeaderBlock insideLoopHeader =
                (HeaderBlock) regionLevel.entryBlock.flow().head.destination;
        HeaderBlock insideLoopFwdDiffBlock =
                (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(insideLoopHeader);
        Tree insideFwdLoopIndex = insideLoopFwdDiffBlock.headInstr().tree.down(3).down(1);
        Tree strideTree = insideLoopFwdDiffBlock.headInstr().tree.down(3).down(4);
        if (ILUtils.isNullOrNone(strideTree)) {
            strideTree = ILUtils.build(ILLang.op_intCst, 1);
        }
        Tree newStrideTree;
        if (InOutAnalyzer.isCheapDuplicableConstant(strideTree, insideLoopHeader, insideLoopHeader.headInstr())) {
            newStrideTree = ILUtils.copy(strideTree);
        } else {
            // If "stride" expression is not duplicable, create a temporary variable:
            FwdBwdVarTool adStrideVarTool =
                    new FwdBwdVarTool(adStrideStr, null, insideLoopHeader,
                            fwdDiffSymbolTable, diffSymbolTable);
            fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                    adStrideVarTool.makeFwdRef(block),
                    ILUtils.copy(strideTree)));
            insideLoopFwdDiffBlock.headInstr().tree.down(3).setChild(adStrideVarTool.makeFwdRef(block), 4);
            newStrideTree = adStrideVarTool.makeFwdRef(block);
        }
        FGArrow loopNextArrow = insideLoopFwdDiffBlock.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT);
        Block insideFwdLoopPreNext = buildFwdBlock(block.rank+"+preIter",
                                             fwdDiffSymbolTable, insideLoopHeader, diffEnv);
        insideFwdLoopPreNext.addInstrTl(
                ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "recordDynamicSchedule"),
                        ILUtils.build(ILLang.op_expressions,
                                ILUtils.copy(insideFwdLoopIndex),
                                newStrideTree)));
        insertBlockAtDest(loopNextArrow, insideFwdLoopPreNext, FGConstants.NO_TEST, 0);

        HeaderBlock insideLoopBwdDiffBlock =
                (HeaderBlock) diffEnv.bwdDiffBlocks.retrieve(insideLoopHeader);
        Tree insideBwdLoopDoTree = insideLoopBwdDiffBlock.headInstr().tree.down(3);
        insideBwdLoopDoTree.setChild(adOmpChunkEndVarTool.makeRef(block), 2);
        insideBwdLoopDoTree.setChild(adOmpChunkStartVarTool.makeRef(block), 3);
        ompChunksOriginalIndex = insideBwdLoopDoTree.down(1);

        // The fwd Block just after exit from the fwd diff of the region body:
        Block fwdPostExit = buildFwdBlock(block.rank+"+postX", fwdDiffSymbolTable, block, diffEnv);
        fwdPostExit.addInstrTl(
                ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "finalizeDynamicSchedule"),
                        ILUtils.build(ILLang.op_expressions)));

        // The header of the new bwd loop
        HeaderBlock bwdNewHeaderBlock = buildBwdHeaderBlock(block.rank+"-NewHd", diffSymbolTable, block, diffEnv);
        bwdNewHeaderBlock.addInstrTl(
                ILUtils.build(ILLang.op_loop, null, null,
                        ILUtils.build(ILLang.op_do,
                                ompChunksIndexVariable.makeNewRef(diffSymbolTable),
                                ILUtils.build(ILLang.op_intCst, 1),
                                ompChunksNumVariable.makeNewRef(diffSymbolTable))));

        // The bwd Block just before the header of the new bwd loop
        Block bwdPreEntry = buildBwdBlock(block.rank+"-preE", diffSymbolTable, block, diffEnv);
        assert diffSymbolTable != null;
        RefDescriptor chunksNumRefDescriptor =
                new RefDescriptor(ompChunksNumVariable.makeNewRef(diffSymbolTable), null,
                        diffSymbolTable, diffSymbolTable, null,
                        null, false, null, curDiffUnit());
        blockDifferentiator().prepareRestoreOperations(
                chunksNumRefDescriptor, chunksNumRefDescriptor,
                1, diffSymbolTable, diffSymbolTable);
        bwdPreEntry.addInstrTl(chunksNumRefDescriptor.makePop());

        // The bwd Block at the start of each new iteration of the new bwd loop
        Block bwdPreNext = buildBwdBlock(block.rank+"-preNext", diffSymbolTable, block, diffEnv);
        bwdPreNext.addInstrTl(adOmpChunkEndVarTool.makePop(block));
        bwdPreNext.addInstrTl(adOmpChunkStartVarTool.makePop(block));

        if (regionBodyDifferentiation.fwdBlockUS == insideLoopFwdDiffBlock) {
            // If the UpStream of regionBodyDifferentiation is directly the fwd loop header:
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdDiffBlock, false);
            checkFreeThenSetNewFGArrow(fwdDiffBlock, FGConstants.NO_TEST, 0, insideLoopFwdDiffBlock, false);
            regionBodyDifferentiation.fwdBlockUS = fwdPreEntry;
        } else {
            insideLoopFwdDiffBlock.insertBlockBeforeLoopEntry(fwdPreEntry);
            insideLoopFwdDiffBlock.insertBlockBeforeLoopEntry(fwdDiffBlock);
        }
        insertBlockAtOrig(insideLoopFwdDiffBlock.getFGArrowTestCase(FGConstants.LOOP, FGConstants.EXIT),
                fwdPostExit, FGConstants.NO_TEST, 0);

        checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdNewHeaderBlock, false);
        checkFreeThenSetNewFGArrow(bwdNewHeaderBlock, FGConstants.LOOP, FGConstants.NEXT,
                bwdPreNext, false);
        insideLoopBwdDiffBlock
                .getFGArrowTestCase(FGConstants.LOOP, FGConstants.EXIT)
                .redirectOrigin(bwdNewHeaderBlock);
        checkFreeThenSetNewFGArrow(insideLoopBwdDiffBlock, FGConstants.LOOP, FGConstants.EXIT,
                bwdNewHeaderBlock, true);
        checkFreeThenSetNewFGArrow(bwdPreNext, FGConstants.NO_TEST, 0, insideLoopBwdDiffBlock, false);
        // Since the region body must be a plain loop, it has only one exit arrow:
        BwdSwitchCase singleBwdSwitchCase =
                regionBodyDifferentiation.exitArrowsFwdBwds.head.second.head.second.head;
        Block bodyBwdDS = singleBwdSwitchCase.bwdBlock;
        if (bodyBwdDS == insideLoopBwdDiffBlock) {
            // If the DownStream of regionBodyDifferentiation is directly the bwd loop header:
            singleBwdSwitchCase.bwdBlock = bwdPreEntry;
        } else {
            bodyBwdDS.getFGArrowTo(insideLoopBwdDiffBlock).redirectDestination(bwdPreEntry);
        }
        return regionBodyDifferentiation;
    }

    /**
     * Build the differentiated control of a checkpointed Piece of Flow Graph (for adjoint differentiation).
     * A checkpointed Piece must have a unique first Block "in" and a
     * unique first block "out" (maybe we can lift some of these restrictions ...).
     * This builds:<br>
     * <pre>
     *     push(sbk U snp) ;
     *     copy of Piece ;
     *     -------------------
     *     pop(snp \ sbk) ;
     *     look(snp ^ sbk) ;
     *        fwd sweep of Piece ;
     *        ==== TURN ====
     *        bwd sweep of Piece ;
     *     pop(sbk) ;
     * </pre>
     *
     * @param checkpointedPiece The FlowGraphLevel object of the checkpointed sub-FlowGraph
     * @param diffEnv           The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateCkpPiece(FlowGraphLevel checkpointedPiece,
                                                           FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff CHECKPOINT " + checkpointedPiece);
            TapEnv.incrTraceIndent(2);
        }

        Block block = checkpointedPiece.entryBlock;
        adEnv.setCurVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        adEnv.setCurPtrVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
        boolean modeIsJoint = diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE;

        // Recompute data flow sets for the checkpointed piece:
        Object[] allDataFlowSaves = new Object[]{null, null, null} ;
        TapPair<BoolVector[], BoolVector[]> dataFlowSetsWithDiffPtr7 =
            recomputeDataFlowCheckpointed(checkpointedPiece, null, modeIsJoint, allDataFlowSaves);
        ToBool needCopy = new ToBool(false);
        TapPair<BoolVector, BoolVector> checkpointingSets =
                computeCheckpointingSets(checkpointedPiece, dataFlowSetsWithDiffPtr7.first, needCopy, SymbolTableConstants.ALLKIND, adEnv.curVectorLen);
        TapPair<BoolVector, BoolVector> checkpointingSetsOnDiffPtr =
                computeCheckpointingSets(checkpointedPiece, dataFlowSetsWithDiffPtr7.second, needCopy, SymbolTableConstants.PTRKIND, adEnv.curPtrVectorLen);

        // If necessary, build a copy of the checkpointed piece:
        FlowGraphCopy ckpPieceCopy =
                needCopy.get() ? copyFlowGraphLevel(checkpointedPiece, new FlowGraphCopyEnv(diffEnv.diffCtxtFwd)) : null;

        // Recursively differentiate the control flow of the checkpointed piece:
        FlowGraphDifferentiationEnv jointReversalEnv = diffEnv.deriveForNewJointDiff();
        jointReversalEnv.turningArrows = checkpointedPiece.exitArrows;
        jointReversalEnv.middleSymbolTables =
                collectMiddleSymbolTables(jointReversalEnv.turningArrows, checkpointedPiece.allBlocksInside());
        precomputeDifferentiatedBlocksIn(checkpointedPiece, jointReversalEnv);
        FlowGraphDifferentiation ckpPieceReversal =
                differentiateFlowGraphLevel(checkpointedPiece, jointReversalEnv);

        SymbolTable fwdDiffSymbolTable =
                block.symbolTable.smartCopy(diffEnv.diffCtxtFwd.listST,
                        diffEnv.diffCtxtFwd.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
        block.symbolTable.cleanCopySymbolDecls();
        SymbolTable diffSymbolTable =
                block.symbolTable.smartCopy(diffEnv.diffCtxt.listST,
                        diffEnv.diffCtxt.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
        block.symbolTable.cleanCopySymbolDecls();

        // The fwd Block just before the copy ckp piece entry.
        // PUSH'es the snapshot for the checkpoint.
        Block fwdPreEntry = buildFwdBlock(block.rank+"+preCkpE", fwdDiffSymbolTable, block, diffEnv);

        // The bwd Block just before going into the adjoint ckp piece.
        // POPs (and sometimes LOOKs) the "snp" snapshot for the checkpoint.
        Block bwdPreEntry = buildBwdBlock(block.rank+"-preCkpE", diffSymbolTable, block, diffEnv);

        // The bwd Block just after exiting from the adjoint ckp piece.
        // POPs the backwards snapshot "sbk".
        Block bwdPostExit = buildBwdBlock(block.rank+"-postCkpX", diffSymbolTable, block, diffEnv);

        fillSnapshotInstructions(checkpointingSets.first,
                checkpointingSets.second, adEnv.curVectorLen,
                checkpointingSetsOnDiffPtr.first,
                checkpointingSetsOnDiffPtr.second, adEnv.curPtrVectorLen,
                block,
                new TapList<>(fwdPreEntry, null),
                new TapList<>(bwdPreEntry, null),
                new TapList<>(bwdPostExit, null),
                diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);

        boolean hasBwd;
        // NOTE: Each time a reversed FlowGraphLevel is going to be connected "ForwardToReverse",
        // this implies that its FlowGraphDifferentiation can't be only hasFwd() and not hasBwd().
        // The reason is that the fwd results of the "last" Block's before "turn", can be needed
        // only by the reversed of these "last" Block's.
        // Therefore here hasFwd=>hasBwd and it is enough to check FlowGraphDifferentiation.hasBwd() .
        if (ckpPieceReversal.hasBwd()) {
            mapConnectFwdBwd(ckpPieceReversal.exitArrowsFwdBwds, jointReversalEnv, checkpointedPiece);
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, ckpPieceReversal.fwdBlockUS, false);
            connectBwd(ckpPieceReversal.bwdArrowsUS, bwdPostExit, false, block, block, diffEnv);
            hasBwd = true;
        } else {
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdPostExit, false);
            hasBwd = fwdPreEntry.instructions != null;
        }

        FGArrow arrowFromPreEntry = null;
        boolean hasFwd;
        if (needCopy.get()) {
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, ckpPieceCopy.copyBlockUS, false);
            hasFwd = true;
        } else {
            arrowFromPreEntry = checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, null, false);
            hasFwd = hasBwd || fwdPreEntry.instructions != null;
        }

        // Prepare the result for the Flow Graph building at the enclosing level:
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(checkpointedPiece);
        if (hasBwd) {
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
        }
        if (hasFwd) {
            result.fwdBlockUS = fwdPreEntry;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
            FGArrow exitArrow, fwdArrow;
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                exitArrow = arrowFwdBwds.first;
                if (needCopy.get()) {
                    fwdArrow = ckpPieceCopy.getCopyOfExitArrow(exitArrow);
                } else {
                    fwdArrow = arrowFromPreEntry;
                }
                arrowFwdBwds.second =
                        new TapList<>(
                                new TapPair<>(fwdArrow,
                                        hasBwd ? new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(exitArrow.origin, null), exitArrow.destination), null) : null),
                                null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            }
        }

        restoreDataFlowCheckpointed(checkpointedPiece, null, modeIsJoint, allDataFlowSaves);

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff CHECKPOINT " + checkpointedPiece + " hasFwd:" + hasFwd + " hasBwd:" + hasBwd);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of a "not to be differentiated" Piece of
     * the (tangent or adjoint) differentiated Flow Graph.
     * These Pieces must have a unique first Block "in" and a
     * unique first block "out" (maybe we can lift some of these restrictions ...).
     * This builds:<br>
     * <pre>
     *     push(sbk U snp) ;
     *   C $FWD-OF DO-NOT-DIFF label
     *     copy of Piece ;
     *   C $END-FWD-OF DO-NOT-DIFF label
     *     -------------------
     *     pop(snp \ sbk) ;
     *     look(snp ^ sbk) ;
     *   C $BWD-OF DO-NOT-DIFF label
     *   C $END-BWD-OF DO-NOT-DIFF label
     *     pop(sbk) ;
     * </pre>
     *
     * @param noDiffPiece The FlowGraphLevel object of the non-differentiated sub-FlowGraph
     * @param diffEnv     The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateNoDiffPiece(FlowGraphLevel noDiffPiece,
                                                              FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff NODIFF " + noDiffPiece);
            TapEnv.incrTraceIndent(2);
        }

        Block block = noDiffPiece.entryBlock;
        adEnv.setCurVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        adEnv.setCurPtrVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
        String noDiffLabel = block.headInstr().hasDirective(Directive.NODIFFSTART).label;
        boolean modeIsJoint = diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE;

        // Recompute data flow sets for the non-differentiated piece:
        TapPair<BoolVector[], BoolVector[]> dataFlowSetsWithDiffPtr7 = null ;
        Object[] allDataFlowSaves = new Object[]{null, null, null} ;
        if (diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
            dataFlowSetsWithDiffPtr7 = recomputeDataFlowCheckpointed(noDiffPiece, null, modeIsJoint, allDataFlowSaves) ;
        }
        ToBool needCopy = new ToBool(false);
        TapPair<BoolVector, BoolVector> checkpointingSets = null;
        TapPair<BoolVector, BoolVector> checkpointingSetsOnDiffPtr = null;
        if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
            needCopy.set(true);
        } else {
            checkpointingSets =
                    computeCheckpointingSets(noDiffPiece, dataFlowSetsWithDiffPtr7.first, needCopy, SymbolTableConstants.ALLKIND, adEnv.curVectorLen);
            checkpointingSetsOnDiffPtr =
                    computeCheckpointingSets(noDiffPiece, dataFlowSetsWithDiffPtr7.second, needCopy, SymbolTableConstants.PTRKIND, adEnv.curPtrVectorLen);
        }

        // If necessary, build a copy of the non-differentiated piece:
        FlowGraphCopy noDiffPieceCopy =
                needCopy.get() ? copyFlowGraphLevel(noDiffPiece, new FlowGraphCopyEnv(diffEnv.diffCtxtFwd)) : null;

        SymbolTable fwdDiffSymbolTable =
                block.symbolTable.smartCopy(diffEnv.diffCtxtFwd.listST,
                        diffEnv.diffCtxtFwd.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
        block.symbolTable.cleanCopySymbolDecls();
        SymbolTable diffSymbolTable =
                block.symbolTable.smartCopy(diffEnv.diffCtxt.listST,
                        diffEnv.diffCtxt.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
        block.symbolTable.cleanCopySymbolDecls();

        boolean hasBwd = diffEnv.adDiffMode != DiffConstants.TANGENT_MODE && !adEnv.curUnitIsContext;

        // The fwd Block just before the copy ckp piece entry.
        // PUSH'es the snapshot for the checkpoint.
        Block fwdPreEntry = buildFwdBlock(block.rank+"+preNdE", fwdDiffSymbolTable, block, diffEnv);

        // The fwd Block just at the end of the copy ckp piece entry.
        // Will hold the $END-FWD-OF DO-NOT-DIFF comment.
        Block fwdPostCommentBlock = buildFwdBlock(block.rank+"+postNdX", fwdDiffSymbolTable, block, diffEnv);

        // The bwd Block just before going into the adjoint ckp piece.
        // POPs (and sometimes LOOKs) the "snp" snapshot for the checkpoint.
        Block bwdPreEntry = buildBwdBlock(block.rank+"-preNdE", diffSymbolTable, block, diffEnv);

        // The bwd Block just after exiting from the adjoint ckp piece.
        // POPs the backwards snapshot "sbk".
        Block bwdPostExit = null;

        if (hasBwd) {
            bwdPostExit = buildBwdBlock(block.rank+"-postNdX", diffSymbolTable, block, diffEnv);
            assert checkpointingSets != null;
            fillSnapshotInstructions(checkpointingSets.first,
                    checkpointingSets.second, adEnv.curVectorLen,
                    checkpointingSetsOnDiffPtr.first,
                    checkpointingSetsOnDiffPtr.second, adEnv.curPtrVectorLen,
                    block,
                    new TapList<>(fwdPreEntry, null),
                    new TapList<>(bwdPreEntry, null),
                    new TapList<>(bwdPostExit, null),
                    diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdPostExit, false);
            if (bwdPostExit.instructions == null) {
                bwdPostExit.addInstrHdAfterDecls(ILUtils.build(ILLang.op_none));
            }
            bwdPostExit.headInstr().addPreComments("$END-BWD-OF DO-NOT-DIFF " + noDiffLabel);
            bwdPostExit.headInstr().addPreComments("$BWD-OF DO-NOT-DIFF " + noDiffLabel);
        }

        fwdPostCommentBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_none));
        fwdPostCommentBlock.headInstr().addPreComments(
                (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE ? "$END-TGT" : "$END-FWD") + "-OF DO-NOT-DIFF " + noDiffLabel);
        FGArrow fwdPostCommentArrow = checkFreeThenSetNewFGArrow(fwdPostCommentBlock, FGConstants.NO_TEST, 0, null, false);

        FGArrow arrowFromPreEntry = null;
        if (needCopy.get()) {
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, noDiffPieceCopy.copyBlockUS, false);
            noDiffPieceCopy.copyBlockUS.headInstr().addPreComments(
                    (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE ? "$TGT" : "$FWD") + "-OF DO-NOT-DIFF " + noDiffLabel);
        } else {
            fwdPostCommentBlock.headInstr().addPreComments(
                    (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE ? "$TGT" : "$FWD") + "-OF DO-NOT-DIFF " + noDiffLabel);
            arrowFromPreEntry = checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, null, false);
        }
        boolean hasFwd = true;

        // Prepare the result for the Flow Graph building at the enclosing level:
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(noDiffPiece);
        if (hasBwd) {
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
        }
        if (hasFwd) {
            result.fwdBlockUS = fwdPreEntry;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
            FGArrow exitArrow, fwdArrow;
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                exitArrow = arrowFwdBwds.first;
                if (needCopy.get()) {
                    fwdArrow = noDiffPieceCopy.getCopyOfExitArrow(exitArrow);
                } else {
                    fwdArrow = arrowFromPreEntry;
                }
                checkFreeThenRedirectDestination(fwdArrow, fwdPostCommentBlock, false);
                fwdArrow = fwdPostCommentArrow;
                arrowFwdBwds.second =
                        new TapList<>(
                                new TapPair<>(fwdArrow,
                                        hasBwd ? new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(exitArrow.origin, null), exitArrow.destination), null) : null),
                                null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            }
        }

        if (diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
            restoreDataFlowCheckpointed(noDiffPiece, null, modeIsJoint, allDataFlowSaves);
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff NODIFF " + noDiffPiece);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of tangent- or adjoint-diff of a labelled piece of the primal Flow Graph.
     * The primal piece has been given a label through "$AD LABEL label" and "$AD END-LABEL label",
     * and must have a unique first Block "in" and a unique first block "out" (restriction can be lifted ?...)
     * The generated forward and backward pieces will also have unique entry and exit,
     * decorated with the corresponding label.
     * This builds:<br>
     * <pre>
     *   C $AD LABEL TGT-OF or FWD-OF label
     *     FWD or TGT of region body
     *   C $AD END-LABEL TGT-OF or FWD-OF label
     *     -------------------
     *   C $AD LABEL BWD-OF label
     *     BWD of region body
     *   C $AD END-LABEL BWD-OF label
     * </pre>
     *
     * @param labelledPiece The FlowGraphLevel object of the non-differentiated sub-FlowGraph
     * @param diffEnv     The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateLabelledRegion(FlowGraphLevel labelledPiece,
                                                              FlowGraphDifferentiationEnv diffEnv) {

        Block regionEntryBlock = labelledPiece.entryBlock ;
        String primalLabel = labelledPiece.label ;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff LABELLED ("+primalLabel+") " + labelledPiece);
            TapEnv.incrTraceIndent(2);
        }

        boolean modeIsAdjoint = (diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE
                                 || diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE);
        //boolean hasBwd = modeIsAdjoint && adEnv.curUnitIsContext ;
        String fwdDiffLabel = (modeIsAdjoint?"FWD":"TGT")+"-OF "+primalLabel ;

        // Recursively differentiate the control flow of the checkpointed piece:
        FlowGraphDifferentiation labelledBodyDifferentiation =
                differentiateFlowGraphLevel(labelledPiece, diffEnv);

        SymbolTable fwdDiffSymbolTable =
                regionEntryBlock.symbolTable.smartCopy(diffEnv.diffCtxtFwd.listST,
                        diffEnv.diffCtxtFwd.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
        regionEntryBlock.symbolTable.cleanCopySymbolDecls();
        SymbolTable diffSymbolTable =
                regionEntryBlock.symbolTable.smartCopy(diffEnv.diffCtxt.listST,
                        diffEnv.diffCtxt.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
        regionEntryBlock.symbolTable.cleanCopySymbolDecls();

        // The fwd Block just after the exit from the fwd region. Will contain the END-FWD/TGT label.
        Block fwdPostExit = buildFwdBlock(regionEntryBlock.rank+"+postXR", fwdDiffSymbolTable, regionEntryBlock, diffEnv);
        fwdPostExit.addInstrHd(ILUtils.build(ILLang.op_none)) ;
        fwdPostExit.headInstr().addPreComments("$AD END-LABEL "+fwdDiffLabel) ;

        FlowGraphDifferentiation result = new FlowGraphDifferentiation(labelledPiece);
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> bodyExitArrowsFwdBwds, exitArrowsFwdBwds ;
        bodyExitArrowsFwdBwds = labelledBodyDifferentiation.exitArrowsFwdBwds;

        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        FGArrow fwdArrow, exitArrow, uniqueArrow ;
        if (labelledBodyDifferentiation.hasBwd()) {
            // The most general case:
            // The bwd Block just at the entry into the bwd region. Will contain the BWD label.
            Block bwdPreEntry = buildBwdBlock(regionEntryBlock.rank+"-preER", diffSymbolTable, regionEntryBlock, diffEnv);
            bwdPreEntry.addInstrHd(ILUtils.build(ILLang.op_none)) ;
            bwdPreEntry.headInstr().addPreComments("$AD LABEL "+"BWD-OF "+primalLabel) ;
            // The bwd Block just after the exit from the bwd region. Will contain the END-BWD label.
            Block bwdPostExit = buildBwdBlock(regionEntryBlock.rank+"-postXR", diffSymbolTable, regionEntryBlock, diffEnv);
            bwdPostExit.addInstrHd(ILUtils.build(ILLang.op_none)) ;
            bwdPostExit.headInstr().addPreComments("$AD END-LABEL "+"BWD-OF "+primalLabel) ;
            PushPopNode pushPopTree = buildFwdBwdTreeFromExitFwdBwd(bodyExitArrowsFwdBwds);
            connectFwdBwd(pushPopTree, fwdPostExit,
                      new TapList<>(checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, null, false), null),
                      null, null, diffEnv, labelledPiece);
            result.fwdBlockUS = labelledBodyDifferentiation.fwdBlockUS ;
            if (result.fwdBlockUS.instructions==null)
                result.fwdBlockUS.addInstrHd(ILUtils.build(ILLang.op_none)) ;
            result.fwdBlockUS.headInstr().addPreComments("$AD LABEL "+fwdDiffLabel) ;
            connectBwd(labelledBodyDifferentiation.bwdArrowsUS, bwdPostExit, false, regionEntryBlock, regionEntryBlock, diffEnv);
            result.bwdArrowsUS =
                new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
            uniqueArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                exitArrow = arrowFwdBwds.first;
                arrowFwdBwds.second =
                    new TapList<>(new TapPair<>(uniqueArrow,
                                                new TapList<>(new BwdSwitchCase(uniqueArrow, bwdPreEntry,
                                                                                new TapList<>(exitArrow.origin, null),
                                                                                exitArrow.destination),
                                                              null)),
                              null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            }
        } else if (labelledBodyDifferentiation.hasFwd()) {
            // else tangent case or degenerate case where only the Fwd sweep code is necessary:
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            while (bodyExitArrowsFwdBwds != null) {
                arrowFwdBwds = bodyExitArrowsFwdBwds.head;
                manyFwdBwds = arrowFwdBwds.second;
                while (manyFwdBwds != null) {
                    checkFreeThenRedirectDestination(manyFwdBwds.head.first, fwdPostExit, false);
                    manyFwdBwds = manyFwdBwds.tail;
                }
                bodyExitArrowsFwdBwds = bodyExitArrowsFwdBwds.tail;
            }
            result.fwdBlockUS = labelledBodyDifferentiation.fwdBlockUS ;
            if (result.fwdBlockUS.instructions==null)
                result.fwdBlockUS.addInstrHd(ILUtils.build(ILLang.op_none)) ;
            result.fwdBlockUS.headInstr().addPreComments("$AD LABEL "+fwdDiffLabel) ;
            uniqueArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                exitArrow = arrowFwdBwds.first;
                arrowFwdBwds.second =
                    new TapList<>(new TapPair<>(uniqueArrow, null),
                                  null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            }
        } else {
            // else very degenerate case where Fwd sweep and therefore also Bwd sweep are empty:
            result.fwdBlockUS = null;
            result.bwdArrowsUS = null;
        }

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff LABELLED ("+primalLabel+") " + labelledPiece);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of the adjoint of an iterative loop using binomial checkpointing.
     * The iterative loop must be at least a NATURAL_LOOP, i.e. the only arrows
     * entering the loop enter through its HeaderBlock. <br>
     * NOTE: This construction is possible ONLY when fwd and bwd sweeps
     * are joint. Therefore we will use diffEnv.diffCtxt instead
     * of diffEnv.diffCtxtFwd, because they must be the same anyway.
     *
     * @param diffSubLevel The sub tree for this loop in the tree of diff levels.
     * @param diffEnv      The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateBinomialLoop(FlowGraphLevel diffSubLevel,
                                                               FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff BINOMIAL_LOOP " + diffSubLevel);
            TapEnv.incrTraceIndent(2);
        }

        FlowGraphDifferentiation result = new FlowGraphDifferentiation(diffSubLevel);
        HeaderBlock block = (HeaderBlock) diffSubLevel.entryBlock;
        adEnv.setCurVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        adEnv.setCurPtrVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
        // Binomial checkpointing cannot be used in ADJOINT_SPLIT mode => we are sure mode is ADJOINT:
        boolean modeIsJoint = true;

        Tree loopTree = null, newLoopControlTree, oldLoopControlTree = null;
        // If the loop header is a DO, we must transform it into a WHILE:
        boolean loopIsDo = block.isADoLoop();
        Tree initIndexTree = null;
        if (loopIsDo) {
            loopTree = block.instructions.head.tree;
            oldLoopControlTree = loopTree.down(3);
            initIndexTree = ILUtils.build(ILLang.op_assign,
                    ILUtils.copy(oldLoopControlTree.down(1)),
                    ILUtils.copy(oldLoopControlTree.down(2)));
            Tree stride = oldLoopControlTree.down(4);
            if (stride == null || stride.opCode() == ILLang.op_none) {
                stride = ILUtils.build(ILLang.op_intCst, 1);
            }
            Tree incrementTree = ILUtils.build(ILLang.op_assign,
                    ILUtils.copy(oldLoopControlTree.down(1)),
                    ILUtils.build(ILLang.op_add,
                            ILUtils.copy(oldLoopControlTree.down(1)),
                            ILUtils.copy(stride)));
            // To avoid dangerous sharing of the TBR annotation indexOnLoopEntry/Cycle
            // between the do index and the lhs of these new, standard, instructions:
            initIndexTree.down(1).setAnnotation("TBR", null);
            incrementTree.down(1).setAnnotation("TBR", null);
            incrementTree.down(2).down(1).setAnnotation("TBR", null);
            ActivityPattern.setAnnotationForActivityPattern(
                    incrementTree.down(1), adEnv.curActivity(), "TBR", new boolean[]{true, false});
            DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), incrementTree, true, false);
            Object diffLivenessAnnot = oldLoopControlTree.getAnnotation("RequiredInDiff");
            newLoopControlTree = ILUtils.build(ILLang.op_while,
                    buildTestFromDo(oldLoopControlTree));
            newLoopControlTree.setAnnotation("RequiredInDiff", diffLivenessAnnot);
            loopTree.setChild(newLoopControlTree, 3);
            FlowGraphLevel headerLevel = diffSubLevel.entryPoint;
            TapList<FGArrow> cycleArrows = TapList.append(block.backFlow(), null);
            while (cycleArrows != null) {
                FGArrow oldCycleArrow = cycleArrows.head;
                FGArrow newCycleArrow;
                if (oldCycleArrow.inACycle) {
                    Block origBlock = oldCycleArrow.origin;
                    TemporaryBlock incrementBlock = new TemporaryBlock(origBlock);
                    if (adEnv.unitActivities != null) {
                        TapList<BoolVector> la = adEnv.unitActivities.retrieve(origBlock);
                        // keep only the last 2:
                        for (int jj = TapList.length(la) - 2; jj > 0; --jj) {
                            la = la.tail;
                        }
                        incrementBlock.blockActivities = la;
                    }
                    if (adEnv.unitUsefulnesses != null) {
                        TapList<BoolVector> lu = adEnv.unitUsefulnesses.retrieve(origBlock);
                        // keep only the last 2:
                        for (int jj = TapList.length(lu) - 2; jj > 0; --jj) {
                            lu = lu.tail;
                        }
                        incrementBlock.blockUsefulnesses = lu;
                    }
                    if (adEnv.unitTBRs != null) {
                        TapList<TapPair<BoolVector, BoolVector>> lt = adEnv.unitTBRs.retrieve(origBlock);
                        // keep only the last 2:
                        for (int jj = TapList.length(lt) - 2; jj > 0; --jj) {
                            lt = lt.tail;
                        }
                        incrementBlock.blockTBRs = lt;
                    } else { // no-TBR case
                        incrementBlock.blockTBRs = null;
                    }
                    incrementBlock.blockReqXs = new TapList<>(null, new TapList<>(null, null));
                    incrementBlock.blockAvlXs = new TapList<>(null, new TapList<>(null, null));
                    incrementBlock.blockRecomputations = new TapList<>(null, new TapList<>(null, null));
                    Instruction incrementInstruction = new Instruction(ILUtils.copy(incrementTree));
                    incrementBlock.instructions = new TapList<>(incrementInstruction, null);
                    incrementInstruction.block = incrementBlock;
                    newCycleArrow = insertBlockAtDest(oldCycleArrow, incrementBlock, FGConstants.NO_TEST, 0);
                    newCycleArrow.iter = oldCycleArrow.iter;
                    FlowGraphLevel incrementLevel =
                            new FlowGraphLevel(DiffConstants.PLAIN_BLOCK);
                    incrementLevel.entryBlock = incrementBlock;
                    incrementLevel.entryArrows = new TapList<>(oldCycleArrow, null);
                    incrementLevel.exitArrows = new TapList<>(newCycleArrow, null);
                    incrementLevel.enclosing = diffSubLevel;
                    incrementLevel.trueEnclosing = diffSubLevel;
                    diffSubLevel.contents = new TapList<>(incrementLevel, diffSubLevel.contents);
                    additionalBlockToPlainLevel =
                            new TapList<>(new TapPair<>(incrementBlock, incrementLevel),
                                    additionalBlockToPlainLevel);
                    TapList.replace(headerLevel.entryArrows, oldCycleArrow, newCycleArrow);
                }
                cycleArrows = cycleArrows.tail;
            }
        }

        // If the loop header is a TIMES, we simply take it away:
        boolean loopIsTimes = block.isATimesLoop();
        if (loopIsTimes) {
            loopTree = block.instructions.head.tree;
            block.instructions.head.tree = ILUtils.build(ILLang.op_none);
        }

        // Since we have modified the instruction in the loop header, we must
        // recompute its precomputed differentiation which is in the diffEnv:
        if (loopIsDo || loopIsTimes) {
            precomputeDifferentiatedBlocks(diffSubLevel.entryPoint, diffEnv);
        }

        // The "FIRST_TURN" case is essentially the adjoint of a NATURAL_LOOP. So build it:
        diffSubLevel.levelKind = DiffConstants.NATURAL_LOOP;
        FlowGraphDifferentiation firstTurnDiffResult =
                differentiateNaturalLoop(diffSubLevel, diffEnv);
        diffSubLevel.levelKind = DiffConstants.BINOMIAL_LOOP;

        // The main SymbolTable of the whole adjoint BINOMIAL_LOOP:
        SymbolTable diffSymbolTable =
                block.symbolTable.smartCopy(diffEnv.diffCtxt.listST,
                        diffEnv.diffCtxt.unit, adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
        block.symbolTable.cleanCopySymbolDecls();

        // The new variables that control the binomial reversal:
        NewSymbolHolder actionVariable = new NewSymbolHolder("action", curDiffUnit(), adEnv.integerTypeSpec, -1);
        actionVariable.declarationLevelMustInclude(diffSymbolTable);
        actionVariable.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
        //         ILUtils.addVarIntoFuturePrivates(actionVariable, parallelRegionInstrTree, false, false) ;
        NewSymbolHolder stepVariable = new NewSymbolHolder("step", curDiffUnit(), adEnv.integerTypeSpec, -1);
        stepVariable.declarationLevelMustInclude(diffSymbolTable);
        stepVariable.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
        //         ILUtils.addVarIntoFuturePrivates(stepVariable, parallelRegionInstrTree, false, false) ;

        // Find the actual BINOMIAL_LOOP directive, to get the arguments:
        Instruction srcDoInstruction = block.headInstr();
        Directive binomialDirective = srcDoInstruction.hasDirective(Directive.BINOMIALCKP);
        Tree[] binomialArgs = binomialDirective.arguments;

        // The block that holds initialization of the binomial mechanism:
        Block trvInitBlock = buildBwdBlock(block.rank+"_TrvInit", diffSymbolTable, block, diffEnv);
        trvInitBlock.addInstrHdAfterDecls(ILUtils.buildCall(
                ILUtils.build(ILLang.op_ident, "adBinomial_init"),
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.copy(binomialArgs[0]),
                        ILUtils.copy(binomialArgs[1]),
                        ILUtils.copy(binomialArgs[2]))));
        if (initIndexTree != null) {
            trvInitBlock.addInstrHdAfterDecls(initIndexTree);
        }

        // The block that holds the loop on binomial next actions:
        HeaderBlock trvWhileBlock = buildBwdHeaderBlock(block.rank+"TrvLoop", diffSymbolTable, block, diffEnv);
        trvWhileBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                ILUtils.build(ILLang.op_while,
                        ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "adBinomial_next"),
                                ILUtils.build(ILLang.op_expressions,
                                        actionVariable.makeNewRef(diffSymbolTable),
                                        stepVariable.makeNewRef(diffSymbolTable)))),
                null));
        // Force future #include <adBinomial.h> into differentiated C file :
        Unit diffFile = curDiffUnit().translationUnit() ;
        if (diffFile!=null && !TapList.containsString(diffFile.newDiffCIncludes, "adBinomial.h", true)) {
            diffFile.newDiffCIncludes = new TapList<>("adBinomial.h", diffFile.newDiffCIncludes) ;
        }
        // Force declaration of boolean function adBinomial_next(action, step) :
        WrapperTypeSpec booleanTypeSpec = diffSymbolTable.getTypeDecl("boolean").typeSpec;
        forceDeclareDiffUtilityFunction("adBinomial_next", booleanTypeSpec) ;
        // The block that pops the Sbk snapshot upon exit from the binomial reversal:
        Block trvExitBlock = buildBwdBlock(block.rank+"_TrvExit", diffSymbolTable, block, diffEnv);

        // The block that switches according to the binomial next action:
        Block trvSwitchBlock = buildBwdBlock(block.rank+"_TrvSwitch", diffSymbolTable, block, diffEnv);
        Tree[] switchCases = new Tree[6];
        switchCases[0] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 1)), // PUSH SNAPSHOT
                ILUtils.build(ILLang.op_none));
        switchCases[0].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    Push Snapshot:")));
        switchCases[1] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 2)), // LOOK SNAPSHOT
                ILUtils.build(ILLang.op_none));
        switchCases[1].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    Look Snapshot:")));
        switchCases[2] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 3)), // POP SNAPSHOT
                ILUtils.build(ILLang.op_none));
        switchCases[2].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    Pop Snapshot:")));
        switchCases[3] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 4)), // ADVANCE ONE STEP
                ILUtils.build(ILLang.op_none));
        switchCases[3].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    Advance:")));
        switchCases[4] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 5)), // FIRST TURN
                ILUtils.build(ILLang.op_none));
        switchCases[4].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    First Turn:")));
        switchCases[5] = ILUtils.build(ILLang.op_switchCase,
                ILUtils.build(ILLang.op_expressions,
                        ILUtils.build(ILLang.op_intCst, 6)), // TURN
                ILUtils.build(ILLang.op_none));
        switchCases[5].down(1).setAnnotation("postComments",
                ILUtils.build(ILLang.op_comments, ILUtils.build(ILLang.op_stringCst, "    Turn:")));
        trvSwitchBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_switch,
                actionVariable.makeNewRef(diffSymbolTable),
                ILUtils.build(ILLang.op_switchCases, switchCases)));

        // The FlowGraphLevel object that represents the binomial loop minus its cycling arrows:
        FlowGraphLevel binomialLoopNoCycle = diffSubLevel.createNoCycle();
        // Force copy of "block", built for the "ADVANCE" case, to be a plain Block:
        binomialLoopNoCycle.headerBlockPlain = block;

        // Build the "ADVANCE" case:
        FlowGraphCopy loopBodyCopy =
                copyFlowGraphLevel(binomialLoopNoCycle, new FlowGraphCopyEnv(diffEnv.diffCtxtFwd));
        checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 3, loopBodyCopy.copyBlockUS, false);
        TapList<FGArrow> cyclingArrows = block.cyclingArrows();
        FGArrow cyclingArrow, copyCycling;
        while (cyclingArrows != null) {
            cyclingArrow = cyclingArrows.head;
            copyCycling = loopBodyCopy.getCopyOfExitArrow(cyclingArrow);
            checkFreeThenRedirectDestination(copyCycling, trvWhileBlock, true);
            cyclingArrows = cyclingArrows.tail;
        }

        // Build the "FIRST_TURN" case. It is essentially the same as
        // the adjoint of a NATURAL_LOOP, so we already called differentiateNaturalLoop().
        // This case also makes the link to the adjoint of the downstream
        // code, so it fills the exitArrowsFwdBwds field of the result.
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                firstTurnDiffResult.exitArrowsFwdBwds;
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> resultExitArrowsFwdBwds =
                result.exitArrowsFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> resultArrowFwdBwds;
        FGArrow exitArrow, fwdArrow, copyExit;
        TapList<Block> joiningBlocks = null;
        Block joiningBlock;
        Block reentryBlockBwd = buildBwdBlock(block.rank+"_TrvReentryTest", diffSymbolTable, block, diffEnv);
        if (firstTurnDiffResult.hasFwd()) {
            //In this particular case (binomial loop), we assume
            // that firstTurnDiffResult.hasFwd() => firstTurnDiffResult.hasBwd().
            checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 4, firstTurnDiffResult.fwdBlockUS, false);
            if (firstTurnDiffResult.hasBwd()) {
                mapCheckFreeThenRedirectDestination(firstTurnDiffResult.bwdArrowsUS, trvWhileBlock, true);//?replace by connectBwd()?
            }
            TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
            FGArrow pushCtArrow;
            Block popCtBlock = null, trvResizeBlock;
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            if (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                manyFwdBwds = arrowFwdBwds.second;
                while (popCtBlock == null && manyFwdBwds != null) {
                    oneFwdBwd = manyFwdBwds.head;
                    popCtBlock = oneFwdBwd.second.head.bwdBlock;
                    manyFwdBwds = manyFwdBwds.tail;
                }
                checkFreeThenSetNewFGArrow(reentryBlockBwd, FGConstants.IF, FGConstants.FALSE, popCtBlock, false);
                checkFreeThenSetNewFGArrow(reentryBlockBwd, FGConstants.IF, FGConstants.TRUE, trvWhileBlock, true);
            }
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                joiningBlock = buildBwdBlock(block.rank+"_TrvJoin", diffSymbolTable, block, diffEnv);
                joiningBlocks = new TapList<>(joiningBlock, joiningBlocks);
                exitArrow = arrowFwdBwds.first;
                manyFwdBwds = arrowFwdBwds.second;
                while (manyFwdBwds != null) {
                    oneFwdBwd = manyFwdBwds.head;
                    pushCtArrow = oneFwdBwd.first;
                    checkFreeThenRedirectDestination(pushCtArrow, joiningBlock, false);
                    manyFwdBwds = manyFwdBwds.tail;
                }
                copyExit = loopBodyCopy.getCopyOfExitArrow(exitArrow);
                trvResizeBlock = buildBwdBlock(block.rank+"_TrvResize", diffSymbolTable, block, diffEnv);
                trvResizeBlock.addInstrHdAfterDecls(ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "adBinomial_resize"),
                        ILUtils.build(ILLang.op_expressions)));
                checkFreeThenRedirectDestination(copyExit, trvResizeBlock, false);
                checkFreeThenSetNewFGArrow(trvResizeBlock, FGConstants.NO_TEST, 0, joiningBlock, false);
                resultArrowFwdBwds = resultExitArrowsFwdBwds.head;
                fwdArrow = checkFreeThenSetNewFGArrow(joiningBlock, FGConstants.NO_TEST, 0, null, false);
                resultArrowFwdBwds.second =
                        new TapList<>(new TapPair<>(fwdArrow,
                                new TapList<>(new BwdSwitchCase(fwdArrow, reentryBlockBwd, new TapList<>(exitArrow.origin, null), exitArrow.destination), null)),
                                null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                resultExitArrowsFwdBwds = resultExitArrowsFwdBwds.tail;
            }
        } else {
            // else degenerate case when the loop has empty Fwd and therefore empty Bwd sweeps !
            joiningBlock = buildBwdBlock(block.rank+"_TrvJoin", diffSymbolTable, block, diffEnv);
            joiningBlocks = new TapList<>(joiningBlock, null);
            checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 4, joiningBlock, false);
            Block trvResizeBlock = buildBwdBlock(block.rank+"_TrvResize", diffSymbolTable, block, diffEnv);
            trvResizeBlock.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adBinomial_resize"),
                    ILUtils.build(ILLang.op_expressions)));
            if (exitArrowsFwdBwds != null) {
                checkFreeThenSetNewFGArrow(trvResizeBlock, FGConstants.NO_TEST, 0, joiningBlock, false);
            }
            fwdArrow = checkFreeThenSetNewFGArrow(joiningBlock, FGConstants.NO_TEST, 0, null, false);
            checkFreeThenSetNewFGArrow(reentryBlockBwd, FGConstants.NO_TEST, 0, trvWhileBlock, true);
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                exitArrow = arrowFwdBwds.first;
                copyExit = loopBodyCopy.getCopyOfExitArrow(exitArrow);
                checkFreeThenRedirectDestination(copyExit, trvResizeBlock, false);
                resultArrowFwdBwds = resultExitArrowsFwdBwds.head;
                resultArrowFwdBwds.second =
                        new TapList<>(new TapPair<>(fwdArrow,
                                new TapList<>(new BwdSwitchCase(fwdArrow, reentryBlockBwd, new TapList<>(exitArrow.origin, null), exitArrow.destination), null)),
                                null);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                resultExitArrowsFwdBwds = resultExitArrowsFwdBwds.tail;
            }
        }

        // Creating diffSubLevel.createNoCycle() has damaged diffSubLevel => repair it now:
        diffSubLevel.restoreChildrenParent();

        // Restore the original DO header if it was transformed into a WHILE :
        if (loopIsDo) {
            // Undo the transformation of the DO into a WHILE :
            FlowGraphLevel headerLevel = diffSubLevel.entryPoint;
            TapList<FGArrow> cycleArrows = TapList.append(block.backFlow(), null);
            while (cycleArrows != null) {
                FGArrow newCycleArrow = cycleArrows.head;
                FGArrow oldCycleArrow;
                if (newCycleArrow.inACycle) {
                    Block incrementBlock = newCycleArrow.origin;
                    oldCycleArrow = incrementBlock.backFlow().head;
                    incrementBlock.removeAndShunt();
                    TapPair<Block, FlowGraphLevel> association =
                            TapList.assq(incrementBlock, additionalBlockToPlainLevel);
                    FlowGraphLevel incrementLevel = association.second;
                    additionalBlockToPlainLevel =
                            TapList.delete(association, additionalBlockToPlainLevel);
                    diffSubLevel.contents = TapList.delete(incrementLevel, diffSubLevel.contents);
                    TapList.replace(headerLevel.entryArrows, newCycleArrow, oldCycleArrow);
                }
                cycleArrows = cycleArrows.tail;
            }
            loopTree.setChild(ILUtils.copy(oldLoopControlTree), 3);
        }

        // Restore the original TIMES header if it was taken away:
        if (loopIsTimes) {
            block.instructions.head.tree = loopTree;
        }

        // Build the "TURN" case:
        // Again, the FlowGraphLevel object that represents the binomial loop minus its cycling arrows:
        binomialLoopNoCycle = diffSubLevel.createNoCycle();
        // Force fwd diff of "block", built for the "TURN" FGConstants.CASE, to be a plain Block:
        binomialLoopNoCycle.headerBlockPlain = block;
        Object[] allDataFlowSaves = new Object[]{null, null, null} ;
        TapPair<BoolVector[], BoolVector[]> dataFlowSetsWithDiffPtr7 =
                recomputeDataFlowCheckpointed(binomialLoopNoCycle, block, modeIsJoint, allDataFlowSaves);
        TapTriplet<BoolVector, BoolVector, BoolVector> checkpointingSets =
                computeBinomialSets(block, dataFlowSetsWithDiffPtr7.first, SymbolTableConstants.ALLKIND, adEnv.curVectorLen);
        TapTriplet<BoolVector, BoolVector, BoolVector> checkpointingSetsOnDiffPtr =
                computeBinomialSets(block, dataFlowSetsWithDiffPtr7.second, SymbolTableConstants.PTRKIND, adEnv.curPtrVectorLen);
        // Recursively differentiate the control flow of one iteration of the loop:
        FlowGraphDifferentiationEnv jointReversalEnv = diffEnv.deriveForNewJointDiff();
        jointReversalEnv.turningArrows = binomialLoopNoCycle.exitArrows;
        jointReversalEnv.middleSymbolTables =
                collectMiddleSymbolTables(jointReversalEnv.turningArrows, binomialLoopNoCycle.allBlocksInside());
        precomputeDifferentiatedBlocksIn(binomialLoopNoCycle, jointReversalEnv);
        FlowGraphDifferentiation loopBodySlicedReversal =
                differentiateFlowGraphLevel(binomialLoopNoCycle, jointReversalEnv);
        if (loopBodySlicedReversal.hasBwd()) {
            checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 5, loopBodySlicedReversal.fwdBlockUS, false);
            mapConnectFwdBwd(loopBodySlicedReversal.exitArrowsFwdBwds, jointReversalEnv, diffSubLevel);
            mapCheckFreeThenRedirectDestination(loopBodySlicedReversal.bwdArrowsUS, trvWhileBlock, true);//?replace by connectBwd()?
        } else {
            checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 5, trvWhileBlock, true);
        }


        // Build the "PUSH/LOOK/POP" snapshot cases. Fill them with the main snapshot "Snp":
        Block pushBlock = buildBwdBlock(block.rank+"_TrvPush", diffSymbolTable, block, diffEnv);
        checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 0, pushBlock, false); // PUSH
        checkFreeThenSetNewFGArrow(pushBlock, FGConstants.NO_TEST, 0, trvWhileBlock, true);
        Block lookBlock = buildBwdBlock(block.rank+"_TrvLook", diffSymbolTable, block, diffEnv);
        checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 1, lookBlock, false); // LOOK
        checkFreeThenSetNewFGArrow(lookBlock, FGConstants.NO_TEST, 0, trvWhileBlock, true);
        Block popBlock = buildBwdBlock(block.rank+"_TrvPop", diffSymbolTable, block, diffEnv);
        checkFreeThenSetNewFGArrow(trvSwitchBlock, FGConstants.CASE, 2, popBlock, false);  // POP
        checkFreeThenSetNewFGArrow(popBlock, FGConstants.NO_TEST, 0, trvWhileBlock, true);
        BoolVector snp = checkpointingSets.second;
        if (loopIsDo) {
            TapIntList indexZones =
                    block.symbolTable.listOfZonesOfValue(oldLoopControlTree.down(1), null, block.headInstr());
            snp.set(indexZones, true);
        }
        fillBinomialCheckpoint(snp, adEnv.curVectorLen,
                checkpointingSetsOnDiffPtr.second, adEnv.curPtrVectorLen,
                block, pushBlock, lookBlock, popBlock, diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);

        // Build the "PUSH/POP" of the backward snapshot "Sbk":
        fillSnapshotInstructions(checkpointingSets.first, null, adEnv.curVectorLen,
                checkpointingSetsOnDiffPtr.first, null, adEnv.curPtrVectorLen,
                block, new TapList<>(trvInitBlock, null), null, new TapList<>(trvExitBlock, null),
                diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);

        // Build the "PUSH/POP" of the forward snapshot "Sfw":
        if (firstTurnDiffResult.hasFwd()) {
            fillSnapshotInstructions(checkpointingSets.third, null, adEnv.curVectorLen,
                    checkpointingSetsOnDiffPtr.third, null, adEnv.curPtrVectorLen,
                    block, joiningBlocks, null, new TapList<>(reentryBlockBwd, null),
                    diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);
            // Place the test on action upon reentry in the bwd sweep:
            reentryBlockBwd.addInstrTl(ILUtils.build(ILLang.op_if,
                    ILUtils.build(ILLang.op_eq,
                            actionVariable.makeNewRef(diffSymbolTable),
                            ILUtils.build(ILLang.op_intCst, 4)), // 4=="ADVANCE"
                    null, null));
        }

        // Build FGArrow's around the main while loop:
        checkFreeThenSetNewFGArrow(trvInitBlock, FGConstants.NO_TEST, 0, trvWhileBlock, false);
        checkFreeThenSetNewFGArrow(trvWhileBlock, FGConstants.LOOP, FGConstants.NEXT, trvSwitchBlock, false);
        checkFreeThenSetNewFGArrow(trvWhileBlock, FGConstants.LOOP, FGConstants.EXIT, trvExitBlock, false);

        // Prepare for the Flow Graph building at the enclosing level:
        result.fwdBlockUS = trvInitBlock;
        result.bwdArrowsUS = new TapList<>(checkFreeThenSetNewFGArrow(trvExitBlock, FGConstants.LOOP, FGConstants.EXIT, null, false), null);

        // Creating diffSubLevel.createNoCycle() has damaged diffSubLevel => repair it now:
        diffSubLevel.restoreChildrenParent();
        restoreDataFlowCheckpointed(binomialLoopNoCycle, null, modeIsJoint, allDataFlowSaves);

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff BINOMIAL_LOOP " + diffSubLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            exitArrowsFwdBwds = result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of the adjoint of a II_LOOP.
     * The II_LOOP must be a clean DO_LOOP, i.e. the unique entry and
     * the unique exit must be through the HeaderBlock. This builds:<br>
     * <pre>
     *     push(i) ;
     *     adFrom := &lt;F&gt; ; adStride := &lt;S&gt; ; adTo := &lt;T&gt; ;
     *     push(sbk U snp) ;
     *   C $FWD-OF II-LOOP label
     *     do i=adFrom,adTo,adStride
     *        copy of &lt;loop Body&gt; ;
     *     enddo
     *     push(adFrom,adTo,adStride) ;
     *     -----------------------------------
     *     pop(adStride,adTo,adFrom) ;
     *     pop(snp \ sbk) ;
     *     look(snp ^ sbk) ;
     *   C $BWD-OF II-LOOP label
     *     do i=adFrom,adTo,adStride
     *        fwd sweep of &lt;loop Body&gt; ;
     *        ==== TURN ====
     *        bwd sweep of &lt;loop Body&gt; ;
     *     enddo
     *     pop(sbk) ;
     *     pop(i) ;
     * </pre>
     *
     * @param diffSubLevel The sub tree for this II_LOOP in the tree of diff levels.
     * @param diffEnv      The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateIILoop(FlowGraphLevel diffSubLevel,
                                                         FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff II_LOOP " + diffSubLevel);
            TapEnv.incrTraceIndent(2);
        }

        HeaderBlock block = (HeaderBlock) diffSubLevel.entryBlock;
        Instruction primalDoInstruction = block.headInstr();
        adEnv.setCurVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        adEnv.setCurPtrVectorLen(block.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
        String noDiffLabel = primalDoInstruction.hasDirective(Directive.IILOOP).label;
        boolean modeIsJoint = diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE;

        // The FlowGraphLevel object that represents the body of the II_LOOP without its header:
        FlowGraphLevel loopBodyPiece = diffSubLevel.createLoopBody();

        // Recompute data flow sets for the body of the II_LOOP:
        Object[] allDataFlowSaves = new Object[]{null, null, null} ;
        TapPair<BoolVector[], BoolVector[]> dataFlowSetsWithDiffPtr7 =
                recomputeDataFlowCheckpointed(loopBodyPiece, block, modeIsJoint, allDataFlowSaves);
        ToBool needCopy = new ToBool(false);
        TapPair<BoolVector, BoolVector> checkpointingSets =
                computeCheckpointingSets(loopBodyPiece, dataFlowSetsWithDiffPtr7.first, needCopy, SymbolTableConstants.ALLKIND, adEnv.curVectorLen);
        TapPair<BoolVector, BoolVector> checkpointingSetsOnDiffPtr =
                computeCheckpointingSets(loopBodyPiece, dataFlowSetsWithDiffPtr7.second, needCopy, SymbolTableConstants.PTRKIND, adEnv.curPtrVectorLen);

        // If necessary, build a copy of the loop body:
        FlowGraphCopy loopBodyCopy =
                needCopy.get() ? copyFlowGraphLevel(loopBodyPiece, new FlowGraphCopyEnv(diffEnv.diffCtxtFwd)) : null;

        // Recursively differentiate the control flow of the loop body:
        FlowGraphDifferentiationEnv jointReversalEnv = diffEnv.deriveForNewJointDiff();
        jointReversalEnv.turningArrows = loopBodyPiece.exitArrows;
        jointReversalEnv.middleSymbolTables =
                collectMiddleSymbolTables(jointReversalEnv.turningArrows, loopBodyPiece.allBlocksInside());
        precomputeDifferentiatedBlocksIn(loopBodyPiece, jointReversalEnv);
        FlowGraphDifferentiation loopBodyReversal =
                differentiateFlowGraphLevel(loopBodyPiece, jointReversalEnv);

        HeaderBlock fwdBlock = (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(block);
        HeaderBlock bwdBlock = (HeaderBlock) diffEnv.bwdDiffBlocks.retrieve(block);
        SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
        SymbolTable diffSymbolTable = bwdBlock == null ? null : bwdBlock.symbolTable;

        // The fwd Block just before the fwd loop's entry. When necessary,
        // prepares the "adFrom", "adTo", and "adStride" variables,
        // and also PUSH'es the snapshot for the checkpoint.
        Block fwdPreEntry = buildFwdBlock(block.rank+"+IIPreE", fwdDiffSymbolTable, block, diffEnv);

        // The fwdBlock is a loop header, therefore cannot accomodate additional declarations:
        if (fwdDiffSymbolTable.declarationsBlock == fwdBlock) {
            fwdDiffSymbolTable.declarationsBlock = fwdPreEntry;
        }

        // The fwd Block just after this exit from the fwd loop. When necessary,
        // PUSHes the "adFrom", "adTo", and "adStride" variables:
        Block fwdPostExit = buildFwdBlock(block.rank+"+IIPostX", fwdDiffSymbolTable, block, diffEnv);

        // The bwd Block just before the entry into the bwd loop. When necessary,
        // POPs the "adFrom", "adTo", and "adStride" variables,
        // and also POPs the "snp" snapshot for the checkpoint.
        Block bwdPreEntry = buildBwdBlock(block.rank+"-IIPreE", diffSymbolTable, block, diffEnv);

        // The bwdBlock is a loop header, therefore cannot accomodate additional declarations:
        if (diffSymbolTable != null && diffSymbolTable.declarationsBlock == bwdBlock) {
            diffSymbolTable.declarationsBlock = bwdPreEntry;
        }

        // The bwd Block just after exiting from the bwd loop.
        // When necessary (TBR), POPs the loop index and the backwards snapshot "sbk".
        Block bwdPostExit = buildBwdBlock(block.rank+"-IIPostX", diffSymbolTable, block, diffEnv);

        // Create the do-loops, and their from-to-stride variables:
        Instruction fwdDoInstruction = fwdBlock.headInstr();
        Tree bwdLoopControl = null;
        if (loopBodyCopy != null && fwdDoInstruction.tree.down(3).opCode() == ILLang.op_do) {
            Tree fwdLoopControl = fwdDoInstruction.tree.down(3);
            Tree fromTree = fwdLoopControl.down(2);
            bwdLoopControl = ILUtils.copy(fwdLoopControl);
            if (InOutAnalyzer.isCheapDuplicableConstant(fromTree, block, primalDoInstruction)
                    || !loopBodyReversal.hasBwd() || adEnv.curUnitIsContext) {
                bwdLoopControl.setChild(ILUtils.copy(fromTree), 2);
            } else {
                // If "from" expression is not duplicable, create a temporary variable:
                FwdBwdVarTool adFromVarTool =
                        new FwdBwdVarTool(adFromStr, null, block,
                                fwdDiffSymbolTable, diffSymbolTable);
                fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                        adFromVarTool.makeFwdRef(block),
                        ILUtils.copy(fromTree)));
                fwdLoopControl.setChild(adFromVarTool.makeFwdRef(block), 2);
                fwdPostExit.addInstrTl(adFromVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adFromVarTool.makePop(block));
                bwdLoopControl.setChild(adFromVarTool.makeRef(block), 2);
            }
            Tree toTree = fwdLoopControl.down(3);
            if (InOutAnalyzer.isCheapDuplicableConstant(toTree, block, primalDoInstruction)
                    || !loopBodyReversal.hasBwd() || adEnv.curUnitIsContext) {
                bwdLoopControl.setChild(ILUtils.copy(toTree), 3);
            } else {
                // If "to" expression is not duplicable, create a temporary variable:
                FwdBwdVarTool adToVarTool =
                        new FwdBwdVarTool(adToStr, null, block,
                                fwdDiffSymbolTable, diffSymbolTable);
                fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                        adToVarTool.makeFwdRef(block),
                        ILUtils.copy(toTree)));
                fwdLoopControl.setChild(adToVarTool.makeFwdRef(block), 3);
                fwdPostExit.addInstrTl(adToVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adToVarTool.makePop(block));
                bwdLoopControl.setChild(adToVarTool.makeRef(block), 3);
            }
            Tree strideTree = fwdLoopControl.down(4);
            if (InOutAnalyzer.isCheapDuplicableConstant(strideTree, block, primalDoInstruction)
                    || !loopBodyReversal.hasBwd() || adEnv.curUnitIsContext) {
                bwdLoopControl.setChild(ILUtils.copy(strideTree), 4);
            } else {
                // If "stride" expression is not duplicable, create a temporary variable:
                FwdBwdVarTool adStrideVarTool =
                        new FwdBwdVarTool(adStrideStr, null, block,
                                fwdDiffSymbolTable, diffSymbolTable);
                fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                        adStrideVarTool.makeFwdRef(block),
                        ILUtils.copy(strideTree)));
                fwdLoopControl.setChild(adStrideVarTool.makeFwdRef(block), 4);
                fwdPostExit.addInstrTl(adStrideVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adStrideVarTool.makePop(block));
                bwdLoopControl.setChild(adStrideVarTool.makeRef(block), 4);
            }
            checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.NEXT, loopBodyCopy.copyBlockUS, false);
            checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, fwdPostExit, false);
            fwdDoInstruction.addPreComments("$FWD-OF II-LOOP " + noDiffLabel);
        } else {
            fwdBlock.instructions = null;
            checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.NO_TEST, 0, fwdPostExit, false);
            if (loopBodyReversal.hasBwd()) {
                bwdLoopControl = ILUtils.copy(fwdDoInstruction.tree.down(3));
            }
        }

        if (loopBodyReversal.hasBwd()) {
            assert bwdBlock != null;
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdBlock, false);
            // Build the reverse loop header Instruction:
            Instruction bwdDoInstruction =
                    bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                            bwdLoopControl, null));
            // If the primal II-LOOP loop has the famous IVDEP pragma, copy it onto the BWD II-LOOP:
            String ivdepPragma = primalDoInstruction.containsPreComment("dir$ ivdep");
            if (ivdepPragma != null) {
                bwdDoInstruction.addPreComments(ivdepPragma);
            }

            bwdDoInstruction.addPreComments("$BWD-OF II-LOOP " + noDiffLabel);
            // Connect the body of the diff II-loop to its diff header:
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.NEXT, loopBodyReversal.fwdBlockUS, false);
            connectBwd(loopBodyReversal.bwdArrowsUS, bwdBlock, true, block, loopBodyPiece.entryBlock, diffEnv);
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.EXIT, bwdPostExit, false);
            // Connect the end of each iteration fwd sweep with the
            // beginning of the same iteration's bwd sweep :
            mapConnectFwdBwd(loopBodyReversal.exitArrowsFwdBwds, jointReversalEnv, diffSubLevel);
        } else {
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdPostExit, false);
        }

        //Connect the body of the copied loop to its copied loop header.
        if (loopBodyCopy != null) {
            TapList<FGArrow> cyclingArrows = block.cyclingArrows();
            while (cyclingArrows != null) {
                FGArrow copyCycling = loopBodyCopy.getCopyOfExitArrow(cyclingArrows.head);
                if (copyCycling != null) {
                    checkFreeThenRedirectDestination(copyCycling, fwdBlock, true);
                }
                cyclingArrows = cyclingArrows.tail;
            }
        }
        checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdBlock, false);
        fillSnapshotInstructions(checkpointingSets.first,
                checkpointingSets.second, adEnv.curVectorLen,
                checkpointingSetsOnDiffPtr.first,
                checkpointingSetsOnDiffPtr.second, adEnv.curPtrVectorLen,
                block,
                new TapList<>(fwdPreEntry, null),
                new TapList<>(bwdPreEntry, null),
                new TapList<>(bwdPostExit, null),
                diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);

        // insert saves and restores of the loop index, if any...
        Tree firstLoopInstr = primalDoInstruction.tree;
        if (firstLoopInstr.opCode() == ILLang.op_loop &&
                firstLoopInstr.down(3).opCode() == ILLang.op_do) {
            Tree doIndexToSave = firstLoopInstr.down(3).down(1);
            if (ADTBRAnalyzer.isTBRindexOnLoopEntry(adEnv.curActivity(), doIndexToSave) && !adEnv.curUnitIsContext) {
                RefDescriptor indexSaveRefDescriptor =
                        new RefDescriptor(ILUtils.copy(doIndexToSave), null,
                                block.symbolTable, fwdBlock.symbolTable, null,
                                null, false, null, curDiffUnit());
                adEnv.setCurBlock(block.enclosingLoop());
                blockDifferentiator().prepareRestoreOperations(indexSaveRefDescriptor, null,
                        1, fwdBlock.symbolTable, diffSymbolTable);
                fwdPreEntry.addInstrHdAfterDecls(indexSaveRefDescriptor.makePush());
                bwdPostExit.addInstrTl(indexSaveRefDescriptor.makePop());
            }
        }

        // Prepare for the Flow Graph building at the enclosing level:
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(diffSubLevel);
        // The FlowGraphDifferentiation of an II_LOOP has an empty Bwd sweep
        // only rarely, when there is no PUSH/LOOK/POP, no save index "i",
        // and an empty bwd (and therefore fwd) for the body of the II_LOOP.
        boolean hasBwd = loopBodyReversal.hasBwd()
                || bwdPreEntry.instructions != null
                || bwdPostExit.instructions != null;
        boolean hasFwd = hasBwd || loopBodyCopy != null;
        if (hasFwd) {
            result.fwdBlockUS = fwdPreEntry;
        }
        if (hasBwd) {
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
        }
        // NOTE: there can be only ONE FGArrow exiting from a II_LOOP (which is a DO or TIMES loop)
        //  => result.exitArrowsFwdBwds is a singleton !
        if (hasFwd) {
            TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> exitArrowFwdBwds = result.exitArrowsFwdBwds.head;
            FGArrow exitArrow = exitArrowFwdBwds.first;
            FGArrow fwdArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
            exitArrowFwdBwds.second =
                    new TapList<>(new TapPair<>(fwdArrow,
                            hasBwd ? new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(block, null), exitArrow.destination), null) : null),
                            null);
        }

        // Creating diffSubLevel.createLoopBody() has damaged diffSubLevel => repair it now.
        diffSubLevel.restoreChildrenParent();
        restoreDataFlowCheckpointed(loopBodyPiece, block, modeIsJoint, allDataFlowSaves);

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff II_LOOP " + diffSubLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /** This global is used only during differentiateNaturalLoop when there are sub-scopes inside the loop.
     * TODO: find a cleaner way to pass this value to where it is used. */
    private HeaderBlock naturalLoopHeader = null ;

    /**
     * Build the control of the (tangent or adjoint) differentiation
     * of a NATURAL_LOOP, meaning that the only arrows entering the loop
     * enter through its HeaderBlock. This builds:<br>
     * <pre>
     *     push(i) ;
     *     adCount := 1 ;
     *     &lt;source loop control i&gt; {
     *       fwd sweep of &lt;Body.a&gt; ;
     *       for each (&lt;exit&gt;) {
     *         push(&lt;exitControl&gt;) ;
     *         push(adCount) ;
     *         goto exit ;}
     *       fwd sweep of &lt;Body.b&gt; ;
     *       push(&lt;cycleControl&gt;) ;
     *       push(i) ;
     *       ++adCount ;
     *     }
     *     ----------------
     *     pop(adCount) ;
     *     do (i0=1,adCount) {
     *       if (i0!=1) {
     *         switch(pop(&lt;cycleControl&gt;)) bwd sweep of &lt;Body.b&gt; ; ..
     *       } else {
     *         switch(pop(&lt;exitControl&gt;)) ..
     *       }
     *       bwd sweep of &lt;Body.a&gt; ;
     *       pop(i) ;
     *     }
     * </pre>
     * <br> This is the building method used for C "for" loops, therefore it takes care
     * of the fwd and bwd differentiation of the 3 control statements in the for header.
     *
     * @param diffSubLevel The sub tree for this NATURAL_LOOP in the tree of diff levels.
     * @param diffEnv      The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateNaturalLoop(FlowGraphLevel diffSubLevel,
                                                              FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff NATURAL_LOOP " + diffSubLevel);
            TapEnv.incrTraceIndent(2);
        }

        HeaderBlock block = (HeaderBlock) diffSubLevel.entryBlock;
        SymbolTable primalSymbolTable = block.symbolTable ;

        // Recursively differentiate the control flow of the NATURAL_LOOP's body (header included!):
        FlowGraphDifferentiation loopNoCycleDifferentiation =
                differentiateFlowGraphLevel(diffSubLevel.createNoCycle(), diffEnv);

        SymbolTable fwdDiffSymbolTable =
                primalSymbolTable.smartCopy(diffEnv.diffCtxtFwd.listST, diffEnv.diffCtxtFwd.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
        primalSymbolTable.cleanCopySymbolDecls();
        SymbolTable diffSymbolTable =
                primalSymbolTable.smartCopy(diffEnv.diffCtxt.listST, diffEnv.diffCtxt.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
        primalSymbolTable.cleanCopySymbolDecls();

        // Detecting when we need to save and restore the index "i" of the original loop:
        Tree doIndexToSave = null;
        int ppLabelForIndex = -1;
        if (block.instructions != null) {
            Tree firstLoopInstr = block.headInstr().tree;
            if (firstLoopInstr.opCode() == ILLang.op_loop
                    && firstLoopInstr.down(3).opCode() == ILLang.op_do
                    && ADTBRAnalyzer.isTBRindex(adEnv.curActivity(), firstLoopInstr.down(3).down(1))
                    && !adEnv.curUnitIsContext) {
                doIndexToSave = firstLoopInstr.down(3).down(1);
                ppLabelForIndex = TapEnv.getNewPushPopNumber();
            }
        }

        // The fwd Block just before the fwd loop's entry.
        Block fwdPreEntry = buildFwdBlock(block.rank+"+NatPreE", fwdDiffSymbolTable, block, diffEnv);
        // If necessary save the loop index "i":
        if (doIndexToSave != null) {
            fwdPreEntry.addInstrTl(
                    RefDescriptor.makePush(curFwdDiffUnit(),
                            ILUtils.copy(doIndexToSave),
                            primalSymbolTable, fwdDiffSymbolTable, ppLabelForIndex));
        }

        // The bwd Block just before the entry in the bwd loop.
        Block bwdPreEntry = buildBwdBlock(block.rank+"-NatPreE", diffSymbolTable, block, diffEnv);

        // 3 bwd Blocks for the 3 control statements of the C for-loop:
        Block bwdInit = null, bwdTest = null, bwdIncr = null;
        if (block.instructions != null) {
            Tree firstLoopInstr = block.headInstr().tree;
            if (firstLoopInstr.opCode() == ILLang.op_loop
                    && firstLoopInstr.down(3).opCode() == ILLang.op_for) {
                HeaderBlock fwdHeaderBlock =
                        loopNoCycleDifferentiation.hasFwd() ? (HeaderBlock) loopNoCycleDifferentiation.fwdBlockUS : null;
                Tree fwdForHeader = (fwdHeaderBlock != null && fwdHeaderBlock.instructions != null ? fwdHeaderBlock.headInstr().tree.down(3) : null);
                TapList<Instruction> savedBlockInstructions = block.instructions;
                TapList<Tree> forInstrs;
                Block fwdForPartBlock;

                forInstrs = ILUtils.getListOfStatements(firstLoopInstr.down(3).down(1));
                block.instructions = null;
                while (forInstrs != null) {
                    block.addInstrTl(forInstrs.head);
                    forInstrs = forInstrs.tail;
                }
                //Attention: In the sequel, we will be copying the contents of temporary
                //  Block fwdForPartBlock into the new for-loop header. This happens 3 times.
                //  Unfortunately, potential tracing instructions registered for these
                //  fwdForPartBlock will be created and placed later, and therefore will be
                //  placed in fwdForPartBlock, and therefore will be lost because they will
                //  not be copied again into the new for-loop.
                fwdForPartBlock = createFwdDiffBlock(block, diffEnv, false);
                fwdForPartBlock.symbolicRk = block.rank + "+InitFor";
                bwdInit = createBwdDiffBlock(block, diffEnv, false);
                if (bwdInit != null) {
                    bwdInit.symbolicRk = block.rank + "-InitFor";
                }
                blockDifferentiator().differentiateBlock(block, fwdForPartBlock, bwdInit, diffEnv.adDiffMode, null);
                if (fwdForHeader != null) {
                    forInstrs = fwdForPartBlock.listOfStatements();
                    fwdForHeader.setChild(ILUtils.buildSingleStatement(forInstrs), 1);
                }

                forInstrs = ILUtils.getListOfStatements(firstLoopInstr.down(3).down(2));
                block.instructions = null;
                Tree testTree;
                while (forInstrs != null) {
                    testTree = forInstrs.head;
                    if (forInstrs.tail == null) {
                        // The last expression must be placed inside an op_if, so that splitForADReverse works,
                        // and make sure that this new op_if is marked as diff-live:
                        testTree = ILUtils.copy(testTree);
                        DiffLivenessAnalyzer.setRequiredInDiff(adEnv.curActivity(), testTree);
                        testTree = ILUtils.build(ILLang.op_if, testTree);
                    }
                    block.addInstrTl(testTree);
                    forInstrs = forInstrs.tail;
                }
                fwdForPartBlock = createFwdDiffBlock(block, diffEnv, false);
                fwdForPartBlock.symbolicRk = block.rank + "+TestFor";
                bwdTest = createBwdDiffBlock(block, diffEnv, false);
                if (bwdTest != null) {
                    bwdTest.symbolicRk = block.rank + "-TestFor";
                }
                blockDifferentiator().differentiateBlock(block, fwdForPartBlock, bwdTest, diffEnv.adDiffMode, null);
                if (fwdForHeader != null) {
                    forInstrs = fwdForPartBlock.listOfStatements();
                    // Remove the op_if around the last fwd-diff Block expression:
                    TapList<Tree> toTail = TapList.toLast(forInstrs);
                    if (toTail != null) {
                        testTree = toTail.head;
                        if (testTree != null && testTree.opCode() == ILLang.op_if) {
                            testTree = testTree.cutChild(1);
                        }
                        toTail.head = testTree;
                    }
                    fwdForHeader.setChild(ILUtils.buildSingleStatement(forInstrs), 2);
                }

                forInstrs = ILUtils.getListOfStatements(firstLoopInstr.down(3).down(3));
                block.instructions = null;
                while (forInstrs != null) {
                    block.addInstrTl(forInstrs.head);
                    forInstrs = forInstrs.tail;
                }
                fwdForPartBlock = createFwdDiffBlock(block, diffEnv, false);
                fwdForPartBlock.symbolicRk = block.rank + "+IncrFor";
                bwdIncr = createBwdDiffBlock(block, diffEnv, false);
                if (bwdIncr != null) {
                    bwdIncr.symbolicRk = block.rank + "-IncrFor";
                }
                blockDifferentiator().differentiateBlock(block, fwdForPartBlock, bwdIncr, diffEnv.adDiffMode, null);
                if (fwdForHeader != null) {
                    forInstrs = fwdForPartBlock.listOfStatements();
                    fwdForHeader.setChild(ILUtils.buildSingleStatement(forInstrs), 3);
                }

                block.instructions = savedBlockInstructions;
            }
        }


        FlowGraphDifferentiation result = new FlowGraphDifferentiation(diffSubLevel);
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> resultExitArrowsFwdBwds =
                result.exitArrowsFwdBwds;
        TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> loopArrowsFwdBwds =
                loopNoCycleDifferentiation.exitArrowsFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> resultExitArrowFwdBwds;
        FGArrow fwdArrow;

        if (loopNoCycleDifferentiation.hasFwd() && loopNoCycleDifferentiation.hasBwd()) {
            // General case.
            // For NATURAL_LOOPs, the HeaderBlock of the original loop has already
            // been differentiated into a HeaderBlock fwdHeaderBlock and a Block bwdBlock.
            HeaderBlock fwdHeaderBlock = (HeaderBlock) loopNoCycleDifferentiation.fwdBlockUS;

            // The fwdHeaderBlock cannot accomodate additional declarations:
            if (fwdDiffSymbolTable.declarationsBlock == fwdHeaderBlock) {
                fwdDiffSymbolTable.declarationsBlock = fwdPreEntry;
            }

            // Connect the new fwd control Blocks with the loop body Blocks :
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdHeaderBlock, false);

            // The NewSymbolHolder's and RefDescriptor's for the iteration counter "adCount":
            FwdBwdVarTool adCountVarTool =
                    new FwdBwdVarTool(adCountStr, null, block,
                            fwdDiffSymbolTable, diffSymbolTable);

            // The NewSymbolHolder for the new index "i0" for the reverse loop "do (i0=1,adCount)"
            NewSymbolHolder dummyIndex = new NewSymbolHolder("i", curDiffUnit(), adEnv.integerTypeSpec, -1);
            dummyIndex.declarationLevelMustInclude(diffSymbolTable);
            dummyIndex.preparePrivateClause(block,
                    adEnv.curUnit().privateSymbolTable(), false, false);
//             ILUtils.addVarIntoFuturePrivates(dummyIndex, parallelRegionInstrTree, false, false) ;

            // Initialize the iteration counter "adCount":
            fwdPreEntry.addInstrTl(ILUtils.build(ILLang.op_assign,
                    adCountVarTool.makeFwdRef(block),
                    ILUtils.build(ILLang.op_intCst, 1)));

            // The fwd Block between the end of a cycle and the header.
            Block fwdPostCycle = buildFwdBlock(block.rank+"+NatPostCy", fwdDiffSymbolTable, block, diffEnv);
            checkFreeThenSetNewFGArrow(fwdPostCycle, FGConstants.NO_TEST, 0, fwdHeaderBlock, true);

            // The bwd Block between the end of a cycle and the header.
            Block bwdPostCycle = buildBwdBlock(block.rank+"-NatPostCy", diffSymbolTable, block, diffEnv);
            mapCheckFreeThenRedirectDestination(loopNoCycleDifferentiation.bwdArrowsUS, bwdPostCycle, false);//?replace by connectBwd()?

            // Increment the iteration counter "adCount" and if necessary save the loop index "i":
            fwdPostCycle.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                    adCountVarTool.makeFwdRef(block),
                    ILUtils.build(ILLang.op_add,
                            adCountVarTool.makeFwdRef(block),
                            ILUtils.build(ILLang.op_intCst, 1))));

            // If necessary, save and restore the index "i" of the original loop:
            if (doIndexToSave != null) {
                fwdPostCycle.addInstrHdAfterDecls(
                        RefDescriptor.makePush(curFwdDiffUnit(),
                                ILUtils.copy(doIndexToSave),
                                primalSymbolTable, fwdDiffSymbolTable, ppLabelForIndex));
                bwdPostCycle.addInstrTl(
                        RefDescriptor.makePop(curDiffUnit(),
                                ILUtils.copy(doIndexToSave),
                                primalSymbolTable, diffSymbolTable,
                                ppLabelForIndex, toBranchVariable));
            }

            // When true, all exits from this NATURAL_LOOP are turning,
            // and therefore there is no need to Push/Pop the adCount.
            boolean allExitsTurn = adEnv.curUnitIsContext
                    || allExitsTurn(diffSubLevel.exitArrows, diffEnv.turningArrows);

            // POP the iteration counter "adCount":
            if (!allExitsTurn) {
                bwdPreEntry.addInstrHdAfterDecls(adCountVarTool.makePop(block));
            }

            // The bwd Block that will be the header of the bwd loop.
            // Runs "adCount" times:
            HeaderBlock bwdHeaderBlock = buildBwdHeaderBlock(block.rank+"-Hd", diffSymbolTable, block, diffEnv) ;

            bwdHeaderBlock.addInstrHdAfterDecls(
                    ILUtils.build(ILLang.op_loop,
                            null, null,
                            ILUtils.build(ILLang.op_do,
                                    dummyIndex.makeNewRef(diffSymbolTable),
                                    ILUtils.build(ILLang.op_intCst, 1),
                                    adCountVarTool.makeRef(block),
                                    null),
                            null));

            FGArrow loopArrow;
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> hdExitingArrowsFwdBwds =
                    new TapList<>(null, null);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> tlExitingArrowsFwdBwds =
                    hdExitingArrowsFwdBwds;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> hdCyclingArrowsFwdBwds =
                    new TapList<>(null, null);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> tlCyclingArrowsFwdBwds =
                    hdCyclingArrowsFwdBwds;
            boolean cycleExitsFromInsideScope = false ;
            boolean exitExitsFromInsideScope = false ;
            while (loopArrowsFwdBwds != null) {
                arrowFwdBwds = loopArrowsFwdBwds.head;
                loopArrow = arrowFwdBwds.first;
                resultExitArrowFwdBwds = TapList.assq(loopArrow, resultExitArrowsFwdBwds);
                if (resultExitArrowFwdBwds != null) {
                    // This loopArrow is in fact an exit from the natural loop.
                    // The fwd Block just after this exit from the fwd loop.
                    // PUSHes the iteration counter "adCount":
                    Block fwdPostExit = buildFwdBlock(block.rank+"+NatPostX", fwdDiffSymbolTable, block, diffEnv);
                    if (!allExitsTurn) {
                        fwdPostExit.addInstrHdAfterDecls(adCountVarTool.makePush(block));
                    }
                    fwdArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
                    manyFwdBwds = arrowFwdBwds.second;
                    while (manyFwdBwds != null) {
                        oneFwdBwd = manyFwdBwds.head;
                        if (fwdHeaderBlock.instructions!=null) {
                            checkFreeThenRedirectDestination(oneFwdBwd.first, fwdPostExit, false);
                        } else {
                            // Very special case (diff loop empty): don't complain if not free. This may happen...
                            if (adEnv.traceCurDifferentiation) {
                                 TapEnv.printlnOnTrace("!Reconnect (dangling?) arrow "+oneFwdBwd.first+" to "+fwdPostExit+"!");
                            }
                            oneFwdBwd.first.redirectDestination(fwdPostExit, false);
                        }
                        oneFwdBwd.first = fwdArrow;
                        manyFwdBwds = manyFwdBwds.tail;
                    }
                    // Prepare for the Flow Graph building at the enclosing level:
                    resultExitArrowFwdBwds.second =
                            new TapList<>(
                                    new TapPair<>(
                                            fwdArrow,
                                            new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(loopArrow.origin, null), loopArrow.destination), null)),
                                    null);
                    tlExitingArrowsFwdBwds = tlExitingArrowsFwdBwds.placdl(arrowFwdBwds);
                    if (loopArrow.origin.symbolTable!=primalSymbolTable) exitExitsFromInsideScope = true ;
                } else {
                    // else this loopArrow is in fact a cycle in the natural loop.
                    tlCyclingArrowsFwdBwds = tlCyclingArrowsFwdBwds.placdl(arrowFwdBwds);
                    if (loopArrow.origin.symbolTable!=primalSymbolTable) cycleExitsFromInsideScope = true ;
                }
                loopArrowsFwdBwds = loopArrowsFwdBwds.tail;
            }
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitingArrowsFwdBwds =
                    hdExitingArrowsFwdBwds.tail;
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> cyclingArrowsFwdBwds =
                    hdCyclingArrowsFwdBwds.tail;
            PushPopNode pushPopTree;
            if (cycleExitsFromInsideScope && exitExitsFromInsideScope) {
                // The difficult case where both the arrows that cycle in the NATURAL_LOOP and the arrows
                // that exit from it may go out of an internal scope. In that case, we must change the bwd
                // model so that there can be a single entry into the bwd diff internal scope.
                // Note: the problem may occur only in languages that permit local scopes.
                FGArrow bwdLoopNextArrow =
                    checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.NEXT, null, false) ;
                TapList<FGArrow> arrowsBwdDownstream = null ;
                // When it exists, place the bwd Block that reverses the "increment" part of the for-loop:
                if (bwdIncr != null && bwdIncr.instructions != null) {
                    Block bwdTestIter = buildBwdBlock(block.rank+"-NatTestIter", diffSymbolTable, block, diffEnv);
                    bwdTestIter.addInstrHdAfterDecls(
                            ILUtils.build(ILLang.op_if,
                              ILUtils.build(ILLang.op_eq,
                                dummyIndex.makeNewRef(diffSymbolTable),
                                ILUtils.build(ILLang.op_intCst, 1)),
                              null, null));
                    checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.NEXT, bwdTestIter, false);
                    checkFreeThenSetNewFGArrow(bwdTestIter, FGConstants.IF, FGConstants.FALSE, bwdIncr, false);
                    arrowsBwdDownstream = new TapList<>(
                                            checkFreeThenSetNewFGArrow(bwdIncr, FGConstants.NO_TEST, 0,
                                                                       null, false),
                                            new TapList<>(
                                              checkFreeThenSetNewFGArrow(bwdTestIter, FGConstants.IF, FGConstants.TRUE,
                                                                         null, false),
                                              null)) ;
                } else {
                    arrowsBwdDownstream = new TapList<>(bwdLoopNextArrow, null) ;
                }
                loopArrowsFwdBwds = loopNoCycleDifferentiation.exitArrowsFwdBwds;
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopArrowsFwdBwds);
                naturalLoopHeader = block ;
                connectFwdBwd(pushPopTree, fwdPostCycle, arrowsBwdDownstream,
                              block, null, diffEnv, diffSubLevel) ;
                naturalLoopHeader = null ;
            } else if (cyclingArrowsFwdBwds == null) {
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(exitingArrowsFwdBwds);
                connectFwdBwd(pushPopTree, null,
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.NEXT, null, false), null),
                        null, null, diffEnv, diffSubLevel);
            } else if (exitingArrowsFwdBwds == null ||
                    sameUniqueBwdDestAndSameActivitySwitches(cyclingArrowsFwdBwds, exitingArrowsFwdBwds)) {
                // Degenerate case with no exit OR with just 1 exit and 1 cycle and both from the same origin:
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(cyclingArrowsFwdBwds);
                connectFwdBwd(pushPopTree, fwdPostCycle,
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.NEXT, null, false), null),
                        block, null, diffEnv, diffSubLevel);
            } else {
                // General case when no internal scopes: the bwd loop starts with a test for 1st iteration:
                // The bwd Block that tests if we are on the bwd 1st iteration, i.e. "i0==1",
                // i.e. the bwd of last fwd iteration, then switches accordingly:
                Block bwdTestIter = buildBwdBlock(block.rank+"-NatTestIter", diffSymbolTable, block, diffEnv);
                bwdTestIter.addInstrHdAfterDecls(
                        ILUtils.build(ILLang.op_if,
                                ILUtils.build(ILLang.op_eq,
                                        dummyIndex.makeNewRef(diffSymbolTable),
                                        ILUtils.build(ILLang.op_intCst, 1)),
                                null, null));
                checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.NEXT, bwdTestIter, false);
                // Connect the loop exiting FGArrow(s) using the TRUE case of the bwdTestIter,
                // that means that this is the bwd of the last iteration:
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(exitingArrowsFwdBwds);
                connectFwdBwd(pushPopTree, null,
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdTestIter, FGConstants.IF, FGConstants.TRUE, null, false), null),
                        null, null, diffEnv, diffSubLevel);
                // Connect the loop cycling FGArrow(s) using the FALSE case of the bwdTestIter,
                // that means that this is NOT the bwd of the last iteration:
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(cyclingArrowsFwdBwds);
                FGArrow arrowToBwdCycling;
                if (bwdIncr != null && bwdIncr.instructions != null) {
                    checkFreeThenSetNewFGArrow(bwdTestIter, FGConstants.IF, FGConstants.FALSE, bwdIncr, false);
                    arrowToBwdCycling = checkFreeThenSetNewFGArrow(bwdIncr, FGConstants.NO_TEST, 0, null, false);
                } else {
                    arrowToBwdCycling = checkFreeThenSetNewFGArrow(bwdTestIter, FGConstants.IF, FGConstants.FALSE, null, false);
                }
                connectFwdBwd(pushPopTree, fwdPostCycle, new TapList<>(arrowToBwdCycling, null),
                        block, null, diffEnv, diffSubLevel);
            }
            // Connect the new bwd control Blocks with the loop body Blocks :
            if (bwdTest != null && bwdTest.instructions != null) {
                checkFreeThenSetNewFGArrow(bwdPostCycle, FGConstants.NO_TEST, 0, bwdTest, false);
                checkFreeThenSetNewFGArrow(bwdTest, FGConstants.NO_TEST, 0, bwdHeaderBlock, true);
            } else {
                checkFreeThenSetNewFGArrow(bwdPostCycle, FGConstants.NO_TEST, 0, bwdHeaderBlock, true);
            }
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdHeaderBlock, false);
            // Prepare for the Flow Graph building at the enclosing level:
            result.fwdBlockUS = fwdPreEntry;
            FGArrow arrowToBwdUS;
            if (bwdInit != null && bwdInit.instructions != null) {
                checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.EXIT, bwdInit, false);
                arrowToBwdUS = checkFreeThenSetNewFGArrow(bwdInit, FGConstants.NO_TEST, 0, null, false);
            } else {
                arrowToBwdUS = checkFreeThenSetNewFGArrow(bwdHeaderBlock, FGConstants.LOOP, FGConstants.EXIT, null, false);
            }
            result.bwdArrowsUS = new TapList<>(arrowToBwdUS, null);
        } else if (loopNoCycleDifferentiation.hasFwd() && !loopNoCycleDifferentiation.hasBwd()) {
            // else degenerate case when the loop has a Fwd sweep but an empty Bwd sweep !
            // For NATURAL_LOOPs, the HeaderBlock of the original loop has already
            // been differentiated into a HeaderBlock fwdHeaderBlock and a Block bwdBlock.
            HeaderBlock fwdHeaderBlock = (HeaderBlock) loopNoCycleDifferentiation.fwdBlockUS;

            // The fwdHeaderBlock cannot accomodate additional declarations:
            if (fwdDiffSymbolTable.declarationsBlock == fwdHeaderBlock) {
                fwdDiffSymbolTable.declarationsBlock = fwdPreEntry;
            }

            // Connect the new fwd control Blocks with the loop body Blocks :
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdHeaderBlock, false);

            if (doIndexToSave != null) {
                bwdPreEntry.addInstrTl(
                        RefDescriptor.makePop(curDiffUnit(),
                                ILUtils.copy(doIndexToSave),
                                primalSymbolTable, diffSymbolTable,
                                ppLabelForIndex, toBranchVariable));
            }
            boolean hasBwd = bwdPreEntry.instructions != null;
            FGArrow loopArrow;
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
            while (loopArrowsFwdBwds != null) {
                arrowFwdBwds = loopArrowsFwdBwds.head;
                loopArrow = arrowFwdBwds.first;
                resultExitArrowFwdBwds = TapList.assq(loopArrow, resultExitArrowsFwdBwds);
                if (resultExitArrowFwdBwds != null) {
                    resultExitArrowFwdBwds.second = null;
                }
                manyFwdBwds = arrowFwdBwds.second;
                while (manyFwdBwds != null) {
                    oneFwdBwd = manyFwdBwds.head;
                    if (resultExitArrowFwdBwds != null) {
                        if (hasBwd) {
                            // Build a copy of this exit FGArrow's oneFwdBwd, in which the
                            // "push" fwd FGArrow are set to each danglingFwd, with a
                            // "pop" bwd Block specified to be the "bwdPreEntry":
                            FGArrow danglingFwd = oneFwdBwd.first;
                            oneFwdBwd =
                                    new TapPair<>(danglingFwd,
                                            new TapList<>(new BwdSwitchCase(danglingFwd, bwdPreEntry,
                                                    new TapList<>(loopArrow.origin, null), loopArrow.destination),
                                                    null));
                        }
                        resultExitArrowFwdBwds.second = TapList.addLast(resultExitArrowFwdBwds.second, oneFwdBwd);
                    } else {
                        checkFreeThenRedirectDestination(oneFwdBwd.first, fwdHeaderBlock, true);
                    }
                    manyFwdBwds = manyFwdBwds.tail;
                }
                loopArrowsFwdBwds = loopArrowsFwdBwds.tail;
            }
            // Prepare for the Flow Graph building at the enclosing level:
            result.fwdBlockUS = fwdPreEntry;
            if (hasBwd) {
                result.bwdArrowsUS =
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, null, false), null);
            }
        } else if (doIndexToSave != null) {
            // else else very degenerate case when the loop has empty Fwd and therefore empty Bwd sweeps !
            bwdPreEntry.addInstrTl(
                    RefDescriptor.makePop(curDiffUnit(),
                            ILUtils.copy(doIndexToSave),
                            primalSymbolTable, diffSymbolTable,
                            ppLabelForIndex, toBranchVariable));
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, null, false), null);
            result.fwdBlockUS = fwdPreEntry;
            fwdArrow = checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, null, false);
            while (resultExitArrowsFwdBwds != null) {
                resultExitArrowFwdBwds = resultExitArrowsFwdBwds.head;
                FGArrow exitArrow = resultExitArrowFwdBwds.first;
                resultExitArrowFwdBwds.second =
                        new TapList<>(new TapPair<>(fwdArrow,
                                new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(block, null), exitArrow.destination), null)),
                                null);
                resultExitArrowsFwdBwds = resultExitArrowsFwdBwds.tail;
            }
        } else {
            // else else else extremely degenerate case: final result has empty fwd and bwd sweeps !
            result.fwdBlockUS = null;
            result.bwdArrowsUS = null;
        }

        // Creating diffSubLevel.createNoCycle() has damaged diffSubLevel => repair it now.
        diffSubLevel.restoreChildrenParent();

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff NATURAL_LOOP " + diffSubLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of the adjoint of a FIXEDPOINT loop. In adjoint modes, this builds:<br>
     * <pre>
     *     [fwdPreEntry]
     *     do "fwdControl" {
     *         "loop Body" ;
     *     }
     *     fwd sweep of "loop Body" ;
     *     [fwdEnd]
     *     ----------------
     *     [bwdPreEntry]:
     *      store adjoints of parameters
     *      adStack_startRepeat()
     *     while (cumul_residual is large) {
     *         [bwdPreCycle]
     *         bwd sweep (dz/dz) of "loop Body" ;
     *         [bwdPostCycle]:
     *          "compute cumul_residual"
     *          adStack_resetRepeat()
     *     }
     *     [bwdPostExit]:
     *      restore adjoints of parameters
     *      adStack_endRepeat()
     *     bwd sweep (dz/dx) of "loop Body" ;
     *     [bwdEnd]
     * </pre>
     * <br> where the contents of each control Block depends on the blockLoopKind of the loop,
     * and are filled in fillDoLoopAdjointControls(), fillWhileLoopAdjointControls(),
     * or fillTimesLoopAdjointControls() respectively.
     * In tangent modes, this builds and returns a simpler pattern, without the "bwd" stuff.
     *
     * @param diffSubLevel The sub tree for this loop in the tree of diff levels.
     * @param diffEnv      The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateFixedPointLoop(FlowGraphLevel diffSubLevel,
                                                                 FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff FIXEDPOINT_LOOP " + diffSubLevel);
            TapEnv.incrTraceIndent(2);
        }

        HeaderBlock block = (HeaderBlock) diffSubLevel.entryBlock;
        Directive fixedPointDirective = block.headInstr().hasDirective(Directive.FIXEDPOINTLOOP);
        boolean modeIsAdjoint =
            (diffEnv.adDiffMode==DiffConstants.ADJOINT_MODE || diffEnv.adDiffMode==DiffConstants.ADJOINT_SPLIT_MODE);

        // Recursively differentiate the control flow of the structured loop's body (header not included!)
        // Differentiate it twice, once for the repeated adjoint loop body, once for the trailing last adjoint.
        // For instance the differentiation of the loopBody is with respect to x and z
        FlowGraphLevel loopBody = diffSubLevel.createLoopBody();

        // Detect the "state" and "parameter" variables from the user-given directive
        // and from the in-out analysis of the loop:
        int nDZ = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        int nDRZ = block.symbolTable.declaredZonesNb(adEnv.diffKind);
        BoolVector stateVarsFromDirective = new BoolVector(nDRZ);
        BoolVector paramVars = new BoolVector(nDRZ);
        BoolVector tbrOverwritten = new BoolVector(nDZ);
        ToObject<Tree[]> toAltStateVarsArray = new ToObject<>(null) ;
        Tree[] stateVariables = fixedPointDirective.arguments;
        detectFixedPointParamsAndState(stateVarsFromDirective, paramVars,
                                       loopBody, block, block.symbolTable,
                                       stateVariables, toAltStateVarsArray, tbrOverwritten);
        // If directive did not specify the state variables, take probable ones:
        if (stateVariables.length==0) {
            stateVariables = toAltStateVarsArray.obj() ;
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("Special Re-differentiation of fixed-point body:");
            System.out.print(" state variables: [") ;
            for (int i=0 ; i<stateVariables.length ; ++i)
                System.out.print(" "+stateVariables[i]) ;
            System.out.println("]") ;
            TapEnv.indentprintlnOnTrace(" state:" + stateVarsFromDirective + " parameters:" + paramVars);
        }

        TapPair<TapList<BoolVector>, TapList<BoolVector>> loopBodyActivityMemo = null;
        if (TapEnv.doActivity()) {
            loopBodyActivityMemo = ADActivityAnalyzer.saveFrontierActivity(
                    loopBody.entryArrows, loopBody.exitArrows, adEnv.curActivity());
        }
        FlowGraphDifferentiationEnv diffEnvRepeated = diffEnv.deriveForNewSplitDiff();
        // Restart data-flow analyses after removing the parameters from the independents:
        if (TapEnv.doActivity()) {
            computeLocalActivity(loopBody, paramVars, loopBodyActivityMemo);
        }
        // Find adjoints of parameters actually modified by the "repeated" backward sweep:
        TapList<Tree> parameterVariablesDiffModified =
            collectActiveUsesOfParameters(new TapList<Block>(block, loopBody.allBlocksInside()),
                                          paramVars, null) ;
        if (diffSubLevel.staticSaves==null) {
            TapList<RefDescriptor> toStaticSavers = new TapList<>(null, null) ;
            diffSubLevel.staticSaves =
                new TapTriplet<>(Boolean.FALSE, toStaticSavers, toStaticSavers) ;
        } else {
            diffSubLevel.staticSaves.first = Boolean.TRUE ;
            diffSubLevel.staticSaves.third = diffSubLevel.staticSaves.second ;
        }
        blockDifferentiator().pushStaticSaves(diffSubLevel.staticSaves) ;
        precomputeDifferentiatedBlocksIn(loopBody, diffEnvRepeated);
        FlowGraphDifferentiation loopBodyDifferentiationRepeated =
                differentiateFlowGraphLevel(loopBody, diffEnvRepeated);
        // Restart data-flow analyses after removing the state from the independents:
        if (TapEnv.doActivity()) {
            computeLocalActivity(loopBody, stateVarsFromDirective, loopBodyActivityMemo);
        }
        diffSubLevel.staticSaves.first = Boolean.TRUE ;
        diffSubLevel.staticSaves.third = diffSubLevel.staticSaves.second ;
        precomputeDifferentiatedBlocksIn(loopBody, diffEnv);
        FlowGraphDifferentiation loopBodyDifferentiationFinal =
                differentiateFlowGraphLevel(loopBody, diffEnv);
        blockDifferentiator().popStaticSaves() ;
        // Restart data-flow analyses to restore the standard data-flow info.
        if (TapEnv.doActivity()) {
            computeLocalActivity(loopBody, null, loopBodyActivityMemo);
        }
        // Also build a copy of the loop body, for the fixed point loop of the forward sweep
        FlowGraphCopy loopBodyCopy =
                copyFlowGraphLevel(loopBody, new FlowGraphCopyEnv(diffEnv.diffCtxtFwd));
        HeaderBlock fwdBlock = (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(block);
        HeaderBlock bwdBlock = (HeaderBlock) diffEnv.bwdDiffBlocks.retrieve(block);
        SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
        SymbolTable diffSymbolTable = (bwdBlock==null ? null : bwdBlock.symbolTable) ;

        // The fwd Block just before the fwd loop's entry.
        // When necessary (TBR), saves the arriving value of the loop index and overwritten TBR values.
        Block fwdPreEntry = buildFwdBlock(block.rank+"+FPpreE", fwdDiffSymbolTable, block, diffEnv);
        // The fwdBlock is a loop header, therefore cannot accomodate additional declarations:
        if (fwdDiffSymbolTable.declarationsBlock == fwdBlock) {
            fwdDiffSymbolTable.declarationsBlock = fwdPreEntry;
        }

        // The fwd Block just after the Fwd of the Fixed-point loop.
        Block fwdEnd = buildFwdBlock(block.rank+"+FPend", fwdDiffSymbolTable, block, diffEnv);

        Block bwdPreEntry = null;
        Block bwdPostExit = null;
        Block bwdPreCycle = null;
        Block bwdPostCycle = null;
        Block bwdEnd = null;

        if (modeIsAdjoint) {
            // The bwd Block just before the entry into the bwd loop.
            // Initializes the new adjoint fixed point variable.
            // and marks the stack for repeated access.
            bwdPreEntry = buildBwdBlock(block.rank+"-FPpreE", diffSymbolTable, block, diffEnv);
            // The bwdBlock is a loop header, therefore cannot accomodate additional declarations:
            if (diffSymbolTable != null && diffSymbolTable.declarationsBlock == bwdBlock) {
                diffSymbolTable.declarationsBlock = bwdPreEntry;
            }

            // The bwd Block at the end of the backwards Fixed-point adjoint.
            // When necessary (TBR), POPs the loop index and overwritten TBR values :
            bwdEnd = buildBwdBlock(block.rank+"-FPend", diffSymbolTable, block, diffEnv);

            // The bwd Block just before the start of each bwd iteration.
            // Save the current value of state variables.
            bwdPreCycle = buildBwdBlock(block.rank+"-FPpreCy", diffSymbolTable, block, diffEnv);

            // The bwd Block just after the end of each iteration
            // Manage adjoint variables according to the method,
            // and prepares the stack for a new sweep of POP's
            bwdPostCycle = buildBwdBlock(block.rank+"-FPpostCy", diffSymbolTable, block, diffEnv);

            // The bwd Block just after exiting from the bwd loop.
            // May manage adjoint variables according to the method,
            // and tells the stack that no pops will be repeated again.
            bwdPostExit = buildBwdBlock(block.rank+"-FPpostX", diffSymbolTable, block, diffEnv);
        }

        // Even if fwdBlock is fwd-dead (i.e. empty), we force it to contain the original loop header
        // because it controls a copy of the original loop body.
        if (fwdBlock.instructions == null) {
            fwdBlock.addInstrTl(ILUtils.copy(block.lastInstr().tree));
        }

        checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdBlock, false);
        checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.NEXT, loopBodyCopy.copyBlockUS, false);
        // Connect the body of the copied loop to its copied loop header.
        TapList<FGArrow> cyclingArrows = block.cyclingArrows();
        while (cyclingArrows != null) {
            FGArrow copyCycling = loopBodyCopy.getCopyOfExitArrow(cyclingArrows.head);
            if (copyCycling != null) {
                checkFreeThenRedirectDestination(copyCycling, fwdBlock, true);
            }
            cyclingArrows = cyclingArrows.tail;
        }
        WrapperTypeSpec doubleType = new WrapperTypeSpec(new ModifiedTypeSpec(adEnv.realTypeSpec, ILUtils.build(ILLang.op_intCst, 8), null)) ;
        NewSymbolHolder cumulVar = new NewSymbolHolder("cumul", curDiffUnit(), doubleType, -1);
        cumulVar.declarationLevelMustInclude(diffSymbolTable);
        cumulVar.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
        //         ILUtils.addVarIntoFuturePrivates(cumulVar, parallelRegionInstrTree, false, false) ;

        PushPopNode pushPopTree;
        if (modeIsAdjoint) {
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdBlock, false);
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.NEXT, bwdPreCycle, false);

            checkFreeThenSetNewFGArrow(bwdPostCycle, FGConstants.NO_TEST, 0, bwdBlock, true);
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.EXIT, bwdPostExit, false);

            // insert saves and restores of the loop index, if any...
            Tree firstLoopInstr = block.headInstr().tree;
            if (firstLoopInstr.opCode() == ILLang.op_loop &&
                    firstLoopInstr.down(3).opCode() == ILLang.op_do) {
                Tree doIndexToSave = firstLoopInstr.down(3).down(1);
                if (ADTBRAnalyzer.isTBRindexOnLoopEntry(adEnv.curActivity(), doIndexToSave)) {
                    RefDescriptor indexSaveRefDescriptor =
                            new RefDescriptor(ILUtils.copy(doIndexToSave), null,
                                    block.symbolTable, fwdBlock.symbolTable, null,
                                    null, false, null, curDiffUnit());
                    adEnv.setCurBlock(block.enclosingLoop());
                    blockDifferentiator().prepareRestoreOperations(indexSaveRefDescriptor, null,
                            1, fwdBlock.symbolTable, diffSymbolTable);
                    fwdPreEntry.addInstrHdAfterDecls(indexSaveRefDescriptor.makePush());
                    bwdEnd.addInstrTl(indexSaveRefDescriptor.makePop());
                }
            }
            // Insert saves and restores of variables that are TBR upstream the FP-LOOP, and overwritten by the FP-LOOP:
            fillSnapshotInstructions(tbrOverwritten, null, nDZ,
                                     null, null,
                                     block.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND),
                                     block,
                                     new TapList<Block>(fwdPreEntry, null),
                                     null, new TapList<Block>(bwdEnd, null), false) ;

            String testFunctionName ;
            String testArg2 ;
            if (fixedPointDirective.adjReduction!=null) { // if directive specified residual reduction factor
                testFunctionName = "adFixedPoint_notReduced" ;
                testArg2 = fixedPointDirective.adjReduction.toString() ;
            } else if (fixedPointDirective.adjResidual!=null) { // if directive specified residual value
                testFunctionName = "adFixedPoint_tooLarge" ;
                testArg2 = fixedPointDirective.adjResidual.toString() ;
            } else { // if directive specified nothing, use a predefined residual value
                testFunctionName = "adFixedPoint_tooLarge" ;
                testArg2 = "1.0e-6" ;
            }
            bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                    ILUtils.build(ILLang.op_while,
                      ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, testFunctionName),
                        ILUtils.build(ILLang.op_expressions,
                          cumulVar.makeNewRef(diffSymbolTable),
                          ILUtils.build(ILLang.op_realCst, testArg2)))),
                    null)) ;
            // Force future #include <adFixedPoint.h> into differentiated C file :
            Unit diffFile = curDiffUnit().translationUnit() ;
            if (diffFile!=null && !TapList.containsString(diffFile.newDiffCIncludes, "adFixedPoint.h", true)) {
                diffFile.newDiffCIncludes = new TapList<>("adFixedPoint.h", diffFile.newDiffCIncludes) ;
            }
            // Force declaration of boolean function "testFunctionName"(cumul, "testArg2") :
            WrapperTypeSpec booleanTypeSpec = diffSymbolTable.getTypeDecl("boolean").typeSpec;
            forceDeclareDiffUtilityFunction(testFunctionName, booleanTypeSpec) ;
            // insert the calls that manage the "repeated look" behavior of the stack:
            bwdPreEntry.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_startRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            bwdPostCycle.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_resetRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            bwdPostExit.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_endRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            // When profiling is on, manage the "repeated" behavior for the profiling tool:
            if (TapEnv.get().profile) {
                bwdPreEntry.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_startRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
                bwdPostCycle.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_resetRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
                bwdPostExit.addInstrHdAfterDecls(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_endRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            }

            Tree[] diffStateVariables = new Tree[stateVariables.length];
            Tree[] initialDiffStateVariables = new Tree[stateVariables.length];
            Tree[] copyDiffStateVariables = new Tree[stateVariables.length];
            for (int i = 0; i < stateVariables.length; i++) {
                diffStateVariables[i] =
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), stateVariables[i], diffSymbolTable, false,
                                stateVariables[i], false, true, stateVariables[i]);
            }
            VariableDecl origVarDecl;
            for (int i = 0; i < stateVariables.length; i++) {
                adEnv.pushCurDiffSorts(DiffConstants.UNDEFSORT, DiffConstants.ALT_DIFF_VAR);
                initialDiffStateVariables[i] =
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), stateVariables[i], diffSymbolTable, false,
                                stateVariables[i], false, true, stateVariables[i]);
                adEnv.popCurDiffSorts();
                adEnv.pushCurDiffSorts(DiffConstants.UNDEFSORT, DiffConstants.COPY_DIFF_VAR);
                copyDiffStateVariables[i] =
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), stateVariables[i], diffSymbolTable, false,
                                stateVariables[i], false, true, stateVariables[i]);
                adEnv.popCurDiffSorts();
                // Cut the reference to these alternate diff vars, so that
                // nested FixedPoint loops will create yet a new diff var.
                origVarDecl = diffSymbolTable.getVariableDecl(ILUtils.baseName(stateVariables[i]));
                origVarDecl.setDiffSymbolHolder(DiffConstants.ALT_DIFF_VAR, null, 0, null);
                origVarDecl.setDiffSymbolHolder(DiffConstants.COPY_DIFF_VAR, null, 0, null);
            }
            IterDescriptor multiDirIterDescriptor =
                    multiDirMode() ? varRefDifferentiator().multiDirIterDescriptor : null;

            // Code about saving/restoring adjoints of parameters, around the main adjoint loop:
            // In principle the main adjoint loop shouldn't overwrite the adjoint parameters,
            // so we need not save them. However, due to e.g. calls with a generalized context
            // (cf set01/ala02, set01/ala04), or to possibly overapproximated activity analysis, some
            // adjoint parameters might be overwritten in the main adjoint loop, and we must save them.
            while (parameterVariablesDiffModified!=null) {
                saveRestoreDiffRefExpression(parameterVariablesDiffModified.head, bwdPreEntry, bwdPostExit,
                                             block.symbolTable, diffSymbolTable, multiDirIterDescriptor) ;
                parameterVariablesDiffModified = parameterVariablesDiffModified.tail ;
            }

            for (int i = 0; i < stateVariables.length; i++) {
                //[llh 10Aug23] Maybe it is not safe to use makeAssign, makeIncement, or
                // makeNormDiff as follows in this for-loop, because stateVariable may
                // be a complicated ref expression, whereas we just want
                // to store/retrieve its contents into a local storage array.
                // Maybe we should better follow the method of saveRestoreDiffRefExpression()
                // that defines an array "diffParams_save", here as "diffState_save" ?
                // Computation of the adjoint residual for adjoint stopping criterion:
                bwdPreCycle.addInstrHd(RefDescriptor.makeAssign(
                        curDiffUnit(), stateVariables[i], stateVariables[i],
                        block.symbolTable, diffSymbolTable,
                        copyDiffStateVariables[i], diffStateVariables[i],
                        true, multiDirIterDescriptor));
                bwdPostCycle.addInstrHd(RefDescriptor.makeNormDiff(curDiffUnit(),
                        cumulVar.makeNewRef(diffSymbolTable),
                        stateVariables[i], stateVariables[i],
                        block.symbolTable, diffSymbolTable,
                        diffStateVariables[i], copyDiffStateVariables[i],
                        true, multiDirIterDescriptor));

                // Manage the temporary save of initial statevar_b and its adding into adjoint statevar:
                bwdPreEntry.addInstrHdAfterDecls(RefDescriptor.makeAssign(
                        curDiffUnit(), stateVariables[i], stateVariables[i],
                        block.symbolTable, diffSymbolTable,
                        initialDiffStateVariables[i], diffStateVariables[i],
                        true, multiDirIterDescriptor));
                bwdPostCycle.addInstrHdAfterDecls(RefDescriptor.makeIncrement(
                        curDiffUnit(), stateVariables[i], stateVariables[i],
                        block.symbolTable, diffSymbolTable,
                        diffStateVariables[i], initialDiffStateVariables[i],
                        true, multiDirIterDescriptor));
                bwdEnd.addInstrHdAfterDecls(RefDescriptor.makeInitialize(
                        curDiffUnit(), null, stateVariables[i],
                        null, null, null,
                        block.symbolTable, diffSymbolTable, null,
                        diffStateVariables[i], true, multiDirIterDescriptor,
                        false, null, true));
            }
            bwdPreEntry.addInstrHd(
                    ILUtils.build(ILLang.op_assign,
                      cumulVar.makeNewRef(diffSymbolTable),
                      ILUtils.build(ILLang.op_realCst, "-1.0"))) ;
            bwdPostCycle.addInstrHd(ILUtils.build(ILLang.op_assign,
                    cumulVar.makeNewRef(diffSymbolTable),
                    ILUtils.build(ILLang.op_realCst, "0.0")));

            // if (loopBodyDifferentiationRepeated.hasBwd()) {
            //     mapCheckFreeThenRedirectDestination(loopBodyDifferentiationRepeated.bwdArrowsUS, bwdPostCycle, false);
            //     checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, loopBodyDifferentiationRepeated.fwdBlockUS, false);
            //     pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopBodyDifferentiationRepeated.exitArrowsFwdBwds);
            //     connectFwdBwd(pushPopTree, fwdEnd,
            //             new TapList<>(checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, null, false), null),
            //             block, null, diffEnv, diffSubLevel);
            // } else if (loopBodyDifferentiationRepeated.hasFwd()) {
            //     checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, loopBodyDifferentiationRepeated.fwdBlockUS, false);
            //     TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
            //             loopBodyDifferentiationRepeated.exitArrowsFwdBwds;
            //     TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> exitArrowFwdBwds;
            //     TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            //     while (exitArrowsFwdBwds != null) {
            //         exitArrowFwdBwds = exitArrowsFwdBwds.head;
            //         manyFwdBwds = exitArrowFwdBwds.second;
            //         while (manyFwdBwds != null) {
            //             checkFreeThenRedirectDestination(manyFwdBwds.head.first, fwdEnd, false);
            //             manyFwdBwds = manyFwdBwds.tail;
            //         }
            //         exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            //     }
            //     checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, bwdPostCycle, false);
            // } else {
            //     checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, fwdEnd, false);
            //     checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, bwdPostCycle, false);
            // }

            // // A dummy fwd Block that will not appear in the final code:
            // Block fwdDummy = buildFwdBlock(block.rank+"+FPdummy", fwdDiffSymbolTable, block, diffEnv);

            // if (loopBodyDifferentiationFinal.hasBwd()) {
            //     mapCheckFreeThenRedirectDestination(loopBodyDifferentiationFinal.bwdArrowsUS, bwdEnd, false);
            //     pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopBodyDifferentiationFinal.exitArrowsFwdBwds);
            //     connectFwdBwd(pushPopTree, fwdDummy,
            //             new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null),
            //             block, null, diffEnv, diffSubLevel);
            // } else {
            //     checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, bwdEnd, false);
            // }

            // if (loopBodyDifferentiationFinal.hasBwd()) {
            //     mapCheckFreeThenRedirectDestination(loopBodyDifferentiationFinal.bwdArrowsUS, bwdEnd, false);
            // }


            if (loopBodyDifferentiationFinal.hasBwd()) {
                mapCheckFreeThenRedirectDestination(loopBodyDifferentiationFinal.bwdArrowsUS, bwdEnd, false);
                checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, loopBodyDifferentiationFinal.fwdBlockUS, false);
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopBodyDifferentiationFinal.exitArrowsFwdBwds);
                connectFwdBwd(pushPopTree, fwdEnd,
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null),
                        block, null, diffEnv, diffSubLevel);
            } else if (loopBodyDifferentiationFinal.hasFwd()) {
                checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, loopBodyDifferentiationFinal.fwdBlockUS, false);
                TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                        loopBodyDifferentiationFinal.exitArrowsFwdBwds;
                TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> exitArrowFwdBwds;
                TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
                while (exitArrowsFwdBwds != null) {
                    exitArrowFwdBwds = exitArrowsFwdBwds.head;
                    manyFwdBwds = exitArrowFwdBwds.second;
                    while (manyFwdBwds != null) {
                        checkFreeThenRedirectDestination(manyFwdBwds.head.first, fwdEnd, false);
                        manyFwdBwds = manyFwdBwds.tail;
                    }
                    exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                }
                checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, bwdEnd, false);
            } else {
                checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, bwdEnd, false);
                checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, fwdEnd, false);
            }

            // A dummy fwd Block that will not appear in the final code:
            Block fwdDummy = buildFwdBlock(block.rank+"+FPdummy", fwdDiffSymbolTable, block, diffEnv);

            if (loopBodyDifferentiationRepeated.hasBwd()) {
                mapCheckFreeThenRedirectDestination(loopBodyDifferentiationRepeated.bwdArrowsUS, bwdPostCycle, false);
                pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopBodyDifferentiationRepeated.exitArrowsFwdBwds);
                connectFwdBwd(pushPopTree, fwdDummy,
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, null, false), null),
                        block, null, diffEnv, diffSubLevel);
            } else {
                checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, bwdPostCycle, false);
            }

        }

        // Creating diffSubLevel.createLoopBody() has damaged diffSubLevel => repair it now.
        diffSubLevel.restoreChildrenParent();

        // Prepare for the Flow Graph building at the enclosing level:
        FlowGraphDifferentiation result = new FlowGraphDifferentiation(diffSubLevel);
        // The FlowGraphDifferentiation of a FIXEDPOINT_LOOP
        // always has a nonempty bwd sweep and a nonempty fwd sweep.
        result.fwdBlockUS = fwdPreEntry;
        assert bwdEnd != null;
        result.bwdArrowsUS =
                new TapList<>(checkFreeThenSetNewFGArrow(bwdEnd, FGConstants.NO_TEST, 0, null, false), null);
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> exitArrowFwdBwds =
                result.exitArrowsFwdBwds.head;
        FGArrow exitArrow = exitArrowFwdBwds.first;
        FGArrow fwdArrow = checkFreeThenSetNewFGArrow(fwdEnd, FGConstants.NO_TEST, 0, null, false);
        exitArrowFwdBwds.second =
                new TapList<>(new TapPair<>(fwdArrow,
                        new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(block, null), exitArrow.destination), null)),
                        null);

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff FIXEDPOINT_LOOP " + diffSubLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Build the control of the (tangent or adjoint) differentiation
     * of a standard structured loop, i.e. either a DO_LOOP, a TIMES_LOOP,
     * or a WHILE_LOOP. In adjoint modes, this builds:<br>
     * <pre>
     *     &lt;fwdPreEntry&gt; ;
     *     do &lt;fwdControl&gt; {
     *       fwd sweep of &lt;loop Body&gt; ;
     *       push(&lt;cycleControl&gt;) ;
     *       &lt;fwdPostCycle&gt; ;
     *     }
     *     &lt;fwdPostExit&gt; ;
     *     ---------------------------
     *     &lt;bwdPreEntry&gt; ;
     *     do &lt;bwdControl&gt; {
     *       &lt;bwdPreCycle&gt; ;
     *       switch(pop(&lt;cycleControl&gt;)) bwd sweep of &lt;loop Body&gt; ;
     *     }
     *     &lt;bwdPostExit&gt; ;
     * </pre>
     * <br> where the contents of each control Block depends on the blockLoopKind of the loop,
     * and are filled in fillDoLoopAdjointControls(), fillWhileLoopAdjointControls(),
     * or fillTimesLoopAdjointControls() respectively.
     * In tangent modes, this builds and returns a simpler pattern, without the "bwd" stuff.
     *
     * @param diffSubLevel The sub tree for this loop in the tree of diff levels.
     * @param diffEnv      The environment for the differentiation of the enclosing context.
     * @return The handles to the differentiated fwd and bwd sweeps.
     */
    private FlowGraphDifferentiation differentiateStructuredLoop(FlowGraphLevel diffSubLevel,
                                                                 FlowGraphDifferentiationEnv diffEnv) {

        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintlnOnTrace("begin diff STRUCTURED_LOOP " + diffSubLevel);
            TapEnv.incrTraceIndent(2);
        }

        HeaderBlock block = (HeaderBlock) diffSubLevel.entryBlock;
        boolean modeIsAdjoint = (diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE || diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE);
        boolean modeIsJoint = (diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);

        // Recursively differentiate the control flow of the structured loop's body (header not included!):
        FlowGraphLevel loopBodyPiece = diffSubLevel.createLoopBody();
        FlowGraphDifferentiation loopBodyDifferentiation = differentiateFlowGraphLevel(loopBodyPiece, diffEnv);

        HeaderBlock fwdBlock = (HeaderBlock) diffEnv.fwdDiffBlocks.retrieve(block);
        HeaderBlock bwdBlock = (HeaderBlock) diffEnv.bwdDiffBlocks.retrieve(block);
        SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
        SymbolTable diffSymbolTable = (bwdBlock == null ? null : bwdBlock.symbolTable);
        // True when this loop is immediately inside a loop parallel control.
        // In that case fwdPreEntry, fwdPostExit, bwdPreEntry, and bwdPostExit must be outside this control.
        boolean opensCurrentControl = opensCurrentControl(block);

        // The fwd Block just before the fwd loop's entry. When necessary,
        // prepares the iteration counter "adCount" or the "adFrom" and "adStride" variables:
        Block fwdPreEntry = buildFwdBlock(block.rank+"+preE", fwdDiffSymbolTable, block, diffEnv);
        if (opensCurrentControl && fwdPreEntry.parallelControls.head.tree.opCode()==ILLang.op_parallelLoop) {
            fwdPreEntry.parallelControls = fwdPreEntry.parallelControls.tail;
        }

        // The fwdBlock is a loop header, therefore cannot accomodate additional declarations:
        if (fwdDiffSymbolTable.declarationsBlock == fwdBlock) {
            fwdDiffSymbolTable.declarationsBlock = fwdPreEntry;
        }

        // The fwd Block between the end of a cycle and the header.
        // When necessary, increments the iteration counter "adCount":
        Block fwdPostCycle = buildFwdBlock(block.rank+"+postCy", fwdDiffSymbolTable, block, diffEnv);

        // The fwd Block just after this exit from the fwd loop.
        // When necessary, PUSHes "adCount", or last index, "adFrom" and "adStride":
        Block fwdPostExit = buildFwdBlock(block.rank+"+postX", fwdDiffSymbolTable, block, diffEnv);
        if (opensCurrentControl && fwdPostExit.parallelControls.head.tree.opCode()==ILLang.op_parallelLoop) {
            fwdPostExit.parallelControls = fwdPostExit.parallelControls.tail;
        }

        Block bwdPreEntry = null;
        Block bwdPreCycle = null;
        Block bwdPostExit = null;

        if (modeIsAdjoint) {
            // The bwd Block just before the entry into the bwd loop.
            // When necessary, POPs "adCount", or last index, "adFrom" and "adStride":
            bwdPreEntry = buildBwdBlock(block.rank+"-preE", diffSymbolTable, block, diffEnv);
            if (opensCurrentControl && bwdPreEntry.parallelControls.head.tree.opCode()==ILLang.op_parallelLoop) {
                bwdPreEntry.parallelControls = bwdPreEntry.parallelControls.tail;
            }

            // The bwdBlock is a loop header, therefore cannot accomodate additional declarations:
            if (diffSymbolTable != null && diffSymbolTable.declarationsBlock == bwdBlock) {
                diffSymbolTable.declarationsBlock = bwdPreEntry;
            }

            // The bwd Block just before going to the next bwd iteration.
            // When necessary (TBR), POPs the loop index :
            bwdPreCycle = buildBwdBlock(block.rank+"-preCy", diffSymbolTable, block, diffEnv);

            // The bwd Block just after exiting from the bwd loop.
            // When necessary (TBR), POPs the loop index :
            bwdPostExit = buildBwdBlock(block.rank+"-postX", diffSymbolTable, block, diffEnv);
            if (opensCurrentControl && bwdPostExit.parallelControls.head.tree.opCode()==ILLang.op_parallelLoop) {
                bwdPostExit.parallelControls = bwdPostExit.parallelControls.tail;
            }
        }

        // If false, then the loop instruction is dead, and in this case treat
        // the block as a plainBlock, but only with the arrows entering the loop:
        boolean loopIsRequired =
            TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
            || (!TapEnv.removeDeadControl() && !adEnv.curUnitIsContext)
            || DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), block.lastInstr().tree, modeIsJoint, false) ;

        if (!modeIsAdjoint) {
            fillLoopTangentControls();
        } else if (loopIsRequired && !adEnv.curUnitIsContext && loopBodyDifferentiation.hasBwd()) {
            assert bwdBlock != null;
            int blockLoopKind = diffSubLevel.levelKind;
            boolean allExitsTurn = allExitsTurn(diffSubLevel.exitArrows, diffEnv.turningArrows);
            // Fills the control instructions that depend on the precise sort of loop.
            // Modifies fwdBlock, and adds control statements into the control Blocks
            if (blockLoopKind == DiffConstants.DO_LOOP) {
                fillDoLoopAdjointControls(block, fwdBlock, fwdPreEntry, fwdPostCycle,
                        fwdPostExit, bwdBlock, bwdPreEntry,
                        bwdPreCycle, bwdPostExit,
                        allExitsTurn);
            } else if (blockLoopKind == DiffConstants.TIMES_LOOP) {
                fillTimesLoopAdjointControls(block, fwdBlock, fwdPreEntry, fwdPostExit,
                        bwdBlock, bwdPreEntry,
                        allExitsTurn);
            } else if (blockLoopKind == DiffConstants.WHILE_LOOP) {
                fillWhileLoopAdjointControls(block, fwdBlock, fwdPreEntry, fwdPostCycle,
                        fwdPostExit, bwdBlock, bwdPreEntry, allExitsTurn);
            } else { // error case ; should never happen :
                bwdBlock.addInstrHdAfterDecls(
                        ILUtils.build(ILLang.op_loop, null, null, null, null));
            }
        }
        if (modeIsAdjoint && loopIsRequired && !adEnv.curUnitIsContext
                && diffSubLevel.levelKind == DiffConstants.DO_LOOP) {
            restoreInitialDoIndex(block, fwdBlock, bwdBlock, fwdPreEntry, bwdPostExit);
        }

        FlowGraphDifferentiation result = new FlowGraphDifferentiation(diffSubLevel);
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> exitArrowFwdBwds;
        FGArrow fwdArrow;
        PushPopNode pushPopTree;
        if (loopBodyDifferentiation.hasBwd()) {
            // The most general case:
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdBlock, false);
            fwdArrow = checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.NEXT, loopBodyDifferentiation.fwdBlockUS, false);
            insertTangentDiffReinits(fwdArrow, block.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT),
                    fwdDiffSymbolTable, null, diffEnv);
            checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, fwdPostExit, false);
            checkFreeThenSetNewFGArrow(fwdPostCycle, FGConstants.NO_TEST, 0, fwdBlock, true);
            pushPopTree = buildFwdBwdTreeFromExitFwdBwd(loopBodyDifferentiation.exitArrowsFwdBwds);
            connectFwdBwd(pushPopTree, fwdPostCycle,
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPreCycle, FGConstants.NO_TEST, 0, null, false), null),
                    block, null, diffEnv, diffSubLevel);
            checkFreeThenSetNewFGArrow(bwdPreEntry, FGConstants.NO_TEST, 0, bwdBlock, false);
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.NEXT, bwdPreCycle, false);
            checkFreeThenSetNewFGArrow(bwdBlock, FGConstants.LOOP, FGConstants.EXIT, bwdPostExit, false);
            connectBwd(loopBodyDifferentiation.bwdArrowsUS, bwdBlock, true, block, loopBodyPiece.entryBlock, diffEnv);
            result.fwdBlockUS = fwdPreEntry;
            result.bwdArrowsUS =
                    new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
            // NOTE: there can be only ONE FGArrow exiting from a DO/WHILE/TIMES loop
            //  => result.exitArrowsFwdBwds is a singleton !
            exitArrowFwdBwds = result.exitArrowsFwdBwds.head;
            FGArrow exitArrow = exitArrowFwdBwds.first;
            fwdArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
            insertTangentDiffReinits(fwdArrow, exitArrow, fwdDiffSymbolTable, null, diffEnv);
            exitArrowFwdBwds.second =
                    new TapList<>(new TapPair<>(fwdArrow,
                            new TapList<>(new BwdSwitchCase(fwdArrow, bwdPreEntry, new TapList<>(block, null), exitArrow.destination), null)),
                            null);
        } else if (loopBodyDifferentiation.hasFwd()) {
            // else tangent case or degenerate case where only the Fwd sweep loop is necessary:
            checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, fwdBlock, false);
            fwdArrow = checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.NEXT, loopBodyDifferentiation.fwdBlockUS, false);
            insertTangentDiffReinits(fwdArrow, block.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT),
                    fwdDiffSymbolTable, null, diffEnv);
            checkFreeThenSetNewFGArrow(fwdBlock, FGConstants.LOOP, FGConstants.EXIT, fwdPostExit, false);
            checkFreeThenSetNewFGArrow(fwdPostCycle, FGConstants.NO_TEST, 0, fwdBlock, true);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    loopBodyDifferentiation.exitArrowsFwdBwds;
            TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
            while (exitArrowsFwdBwds != null) {
                arrowFwdBwds = exitArrowsFwdBwds.head;
                manyFwdBwds = arrowFwdBwds.second;
                while (manyFwdBwds != null) {
                    checkFreeThenRedirectDestination(manyFwdBwds.head.first, fwdPostCycle, false);
                    manyFwdBwds = manyFwdBwds.tail;
                }
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
            }
            // hasBwd is true when there remains a POP of the do-loop index:
            boolean hasBwd = modeIsAdjoint && !adEnv.curUnitIsContext && bwdPostExit.instructions != null;
            result.fwdBlockUS = fwdPreEntry;
            if (hasBwd) {
                result.bwdArrowsUS =
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
            }
            exitArrowFwdBwds = result.exitArrowsFwdBwds.head;
            FGArrow exitArrow = exitArrowFwdBwds.first;
            fwdArrow = checkFreeThenSetNewFGArrow(fwdPostExit, FGConstants.NO_TEST, 0, null, false);
            insertTangentDiffReinits(fwdArrow, exitArrow, fwdDiffSymbolTable, null, diffEnv);
            exitArrowFwdBwds.second =
                    new TapList<>(new TapPair<>(fwdArrow,
                            (hasBwd ? new TapList<>(new BwdSwitchCase(fwdArrow, bwdPostExit, new TapList<>(block, null), exitArrow.destination), null) : null)),
                            null);
        } else {
            // else very degenerate case where Fwd sweep and therefore also Bwd sweep are empty:
            //[llh] TODO: Attention au cas ou la boucle fwd doit etre
            // conservee parce que l'indice de sortie est utilise ensuite !!!
            if (modeIsAdjoint && bwdPostExit.instructions != null) {
                // If there remains a PUSH/POP of the do-loop index:
                result.fwdBlockUS = fwdPreEntry;
                result.bwdArrowsUS =
                        new TapList<>(checkFreeThenSetNewFGArrow(bwdPostExit, FGConstants.NO_TEST, 0, null, false), null);
                exitArrowFwdBwds = result.exitArrowsFwdBwds.head;
                FGArrow exitArrow = exitArrowFwdBwds.first;
                fwdArrow = checkFreeThenSetNewFGArrow(fwdPreEntry, FGConstants.NO_TEST, 0, null, false);
                insertTangentDiffReinits(fwdArrow, exitArrow, fwdDiffSymbolTable, null, diffEnv);
                exitArrowFwdBwds.second =
                        new TapList<>(new TapPair<>(fwdArrow,
                                new TapList<>(new BwdSwitchCase(fwdArrow, bwdPostExit, new TapList<>(block, null), exitArrow.destination), null)),
                                null);
            } else {
                // extremely degenerate case: final result has empty fwd and bwd sweeps !
                result.fwdBlockUS = null;
                result.bwdArrowsUS = null;
            }
        }

        // Creating diffSubLevel.createLoopBody() has damaged diffSubLevel => repair it now.
        diffSubLevel.restoreChildrenParent();

        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
            TapEnv.indentprintlnOnTrace(">>end diff STRUCTURED_LOOP " + diffSubLevel);
            TapEnv.indentprintlnOnTrace("  -upstream: " + result.fwdBlockUS + " ; " + result.bwdArrowsUS);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds =
                    result.exitArrowsFwdBwds;
            TapEnv.indentprintOnTrace("  -dnstream: (");
            while (exitArrowsFwdBwds != null) {
                TapEnv.printlnOnTrace("-" + exitArrowsFwdBwds.head);
                exitArrowsFwdBwds = exitArrowsFwdBwds.tail;
                TapEnv.indentprintOnTrace("             " + (exitArrowsFwdBwds == null ? "" : ' '));
            }
            TapEnv.printlnOnTrace(")");
        }

        return result;
    }

    /**
     * Fill the control blocks for the adjoint of a DO_LOOP:
     * Very simple: just copies the do-loop header of the block into the fwdBlock.
     */
    private void fillLoopTangentControls() {
        // Nothing to be done: the do-loop header has already been copied upon creation of fwdBlock.
    }


    /**
     * Fill the control blocks for the adjoint of a DO_LOOP:
     * <pre>
     * {push(i) ;}
     * adFrom = &lt;F&gt; ; adStride = &lt;S&gt; ;
     * do i=adFrom,T,adStride
     * ...
     * {push(i) ;}
     * enddo ;
     * push(i-adStride, adFrom, adStride) ;
     * ------------------------------------
     * pop(adStride, adFrom, adTo) ;
     * do i=adTo,adFrom,-adStride
     * {pop(i) ;}
     * ...
     * enddo ;
     * {pop(i) ;}
     * </pre>
     */
    private void fillDoLoopAdjointControls(HeaderBlock block,
                                           HeaderBlock fwdBlock, Block fwdPreEntry,
                                           Block fwdPostCycle, Block fwdPostExit,
                                           HeaderBlock bwdBlock, Block bwdPreEntry,
                                           Block bwdPreCycle, Block bwdPostExit,
                                           boolean allExitsTurn) {
        Instruction primalDoInstruction = block.headInstr();
        SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
        SymbolTable diffSymbolTable = bwdBlock.symbolTable;
        Instruction fwdDoInstruction = fwdBlock.headInstr();
        Tree fwdLoopControl = fwdDoInstruction.tree.down(3);
        Tree bwdLoopControl =
                ILUtils.build(ILLang.op_do, ILUtils.copy(fwdLoopControl.down(1)));
        Tree fromTree = fwdLoopControl.down(2);
        if (InOutAnalyzer.isCheapDuplicableConstant(fromTree, block, primalDoInstruction)) {
            bwdLoopControl.setChild(ILUtils.copy(fromTree), 3);
        } else {
            // If "from" expression is not duplicable, create a temporary variable:
            FwdBwdVarTool adFromVarTool =
                    new FwdBwdVarTool(adFromStr, null, block,
                            fwdDiffSymbolTable, diffSymbolTable);
            fwdPreEntry.addInstrTl(ILUtils.build(ILLang.op_assign,
                    adFromVarTool.makeFwdRef(block),
                    ILUtils.copy(fromTree)));
            fwdLoopControl.setChild(adFromVarTool.makeFwdRef(block), 2);
            if (!allExitsTurn) {
                fwdPostExit.addInstrTl(adFromVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adFromVarTool.makePop(block));
            }
            bwdLoopControl.setChild(adFromVarTool.makeRef(block), 3);
        }
        Tree strideTree = fwdLoopControl.down(4);
        if (ILUtils.isNullOrNone(strideTree)) {
            strideTree = ILUtils.build(ILLang.op_intCst, 1);
        }
        if (InOutAnalyzer.isCheapDuplicableConstant(strideTree, block, primalDoInstruction)) {
            bwdLoopControl.setChild(ILUtils.minusTree(ILUtils.copy(strideTree)), 4);
        } else {
            // If "stride" expression is not duplicable, create a temporary variable:
            FwdBwdVarTool adStrideVarTool =
                    new FwdBwdVarTool(adStrideStr, null, block,
                            fwdDiffSymbolTable, diffSymbolTable);
            fwdPreEntry.addInstrTl(ILUtils.build(ILLang.op_assign,
                    adStrideVarTool.makeFwdRef(block),
                    ILUtils.copy(strideTree)));
            strideTree = adStrideVarTool.makeFwdRef(block);
            fwdLoopControl.setChild(adStrideVarTool.makeFwdRef(block), 4);
            if (!allExitsTurn) {
                fwdPostExit.addInstrTl(adStrideVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adStrideVarTool.makePop(block));
            }
            bwdLoopControl.setChild(
                    ILUtils.minusTree(adStrideVarTool.makeRef(block)), 4);
        }
        Tree lastIndexTree =
                ILUtils.hasDuplicableLastIndex(fwdLoopControl, false);
        if (lastIndexTree != null &&
                InOutAnalyzer.isCheapDuplicableConstant(lastIndexTree, block, primalDoInstruction)) {
            bwdLoopControl.setChild(lastIndexTree, 2);
        } else {
            fwdBlock.loopIndexUnusedAfterLoop = false;
            FwdBwdVarTool adToVarTool =
                    new FwdBwdVarTool(adToStr,
                            ILUtils.subTree(ILUtils.copy(fwdLoopControl.down(1)), strideTree),
                            block, fwdDiffSymbolTable, diffSymbolTable);
            if (!allExitsTurn) {
                fwdPostExit.addInstrHdAfterDecls(adToVarTool.makePush(block));
                bwdPreEntry.addInstrTl(adToVarTool.makePop(block));
            } else {
                fwdPostExit.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                        adToVarTool.makeRef(block),
                        ILUtils.subTree(ILUtils.copy(fwdLoopControl.down(1)), strideTree)));
            }
            bwdLoopControl.setChild(adToVarTool.makeRef(block), 2);
        }
        // When needed, save and restore the loop index value at the end of each iteration:
        Tree doIndexToSave = fwdLoopControl.down(1);
        if (ADTBRAnalyzer.isTBRindexOnLoopCycle(adEnv.curActivity(), doIndexToSave) && !adEnv.curUnitIsContext) {
            int ppLabelForIndex = TapEnv.getNewPushPopNumber();
            fwdPostCycle.addInstrTl(
                    RefDescriptor.makePush(curFwdDiffUnit(),
                            ILUtils.copy(doIndexToSave),
                            block.symbolTable, fwdDiffSymbolTable, ppLabelForIndex));
            bwdPreCycle.addInstrHdAfterDecls(
                    RefDescriptor.makePop(curDiffUnit(),
                            ILUtils.copy(doIndexToSave),
                            block.symbolTable, diffSymbolTable,
                            ppLabelForIndex, toBranchVariable));
        }
        bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                bwdLoopControl, null));
    }

    /* When needed, save and restore the loop index value just before original loop */
    private void restoreInitialDoIndex(HeaderBlock block, HeaderBlock fwdBlock, HeaderBlock bwdBlock,
                                       Block fwdPreEntry, Block bwdPostExit) {
        Tree doIndexToSave = fwdBlock.headInstr().tree.down(3).down(1);
        if (ADTBRAnalyzer.isTBRindexOnLoopEntry(adEnv.curActivity(), doIndexToSave)) {
            SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
            SymbolTable diffSymbolTable = bwdBlock.symbolTable;
            RefDescriptor indexSaveRefDescriptor =
                    new RefDescriptor(ILUtils.copy(doIndexToSave), null,
                            block.symbolTable, fwdDiffSymbolTable, null,
                            null, false, null, curDiffUnit());
            adEnv.setCurBlock(block.enclosingLoop());
            blockDifferentiator().prepareRestoreOperations(indexSaveRefDescriptor, null,
                    1, fwdDiffSymbolTable, diffSymbolTable);
            fwdPreEntry.addInstrTl(indexSaveRefDescriptor.makePush());
            bwdPostExit.addInstrHdAfterDecls(indexSaveRefDescriptor.makePop());
        }
    }

    /**
     * Fill the control blocks for the adjoint of a TIMES_LOOP:
     * <pre>
     * adCount = &lt;N&gt; ;
     * do adCount times
     * ...
     * enddo ;
     * push(adCount) ;
     * ---------------
     * adCount=pop() ;
     * do adCount times
     * ...
     * enddo ;
     * </pre>
     */
    private void fillTimesLoopAdjointControls(HeaderBlock block,
                                              HeaderBlock fwdBlock, Block fwdPreEntry, Block fwdPostExit,
                                              HeaderBlock bwdBlock, Block bwdPreEntry,
                                              boolean allExitsTurn) {
        Instruction fwdTimesInstruction = fwdBlock.headInstr();
        Tree fwdLoopControl = fwdTimesInstruction.tree.down(3);
        Tree countTree = fwdLoopControl.down(1);
        if (InOutAnalyzer.isCheapDuplicableConstant(countTree, block, fwdTimesInstruction)) {
            bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                    ILUtils.build(ILLang.op_times, ILUtils.copy(countTree)),
                    null));
        } else {
            SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
            SymbolTable diffSymbolTable = bwdBlock.symbolTable;
            // The NewSymbolHolder's and RefDescriptor's for the iteration counter "adCount":
            FwdBwdVarTool adCountVarTool =
                    new FwdBwdVarTool(adCountStr, null, block,
                            fwdDiffSymbolTable, diffSymbolTable);
            fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                    adCountVarTool.makeFwdRef(block),
                    ILUtils.copy(countTree)));
            fwdLoopControl.setChild(adCountVarTool.makeFwdRef(block), 1);
            if (!allExitsTurn) {
                fwdPostExit.addInstrHdAfterDecls(adCountVarTool.makePush(block));
                bwdPreEntry.addInstrHdAfterDecls(adCountVarTool.makePop(block));
            }
            bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                    ILUtils.build(ILLang.op_times, adCountVarTool.makeRef(block)),
                    null));
        }
    }

    /**
     * Fill the control blocks for the adjoint of a WHILE_LOOP:
     * <pre>
     * adCount = 0;
     * do &lt;control&gt;
     * ... ;
     * adCount = adCount+1;
     * enddo;
     * push(adCount);
     * ----------------------
     * adCount=pop();
     * do adCount times
     * ...
     * enddo;
     * </pre>
     */
    private void fillWhileLoopAdjointControls(HeaderBlock block,
                                              HeaderBlock fwdBlock, Block fwdPreEntry,
                                              Block fwdPostCycle, Block fwdPostExit,
                                              HeaderBlock bwdBlock, Block bwdPreEntry,
                                              boolean allExitsTurn) {
        SymbolTable fwdDiffSymbolTable = fwdBlock.symbolTable;
        SymbolTable diffSymbolTable = bwdBlock.symbolTable;

        // The NewSymbolHolder's and RefDescriptor's for the iteration counter "adCount":
        FwdBwdVarTool adCountVarTool =
                new FwdBwdVarTool(adCountStr, null, block,
                        fwdDiffSymbolTable, diffSymbolTable);
        fwdPreEntry.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                adCountVarTool.makeFwdRef(block),
                ILUtils.build(ILLang.op_intCst, 0)));
        fwdPostCycle.addInstrHdAfterDecls(ILUtils.build(ILLang.op_assign,
                adCountVarTool.makeFwdRef(block),
                ILUtils.build(ILLang.op_add,
                        adCountVarTool.makeFwdRef(block),
                        ILUtils.build(ILLang.op_intCst, 1))));
        if (!allExitsTurn) {
            fwdPostExit.addInstrHdAfterDecls(adCountVarTool.makePush(block));
            bwdPreEntry.addInstrHdAfterDecls(adCountVarTool.makePop(block));
        }
        NewSymbolHolder newSymbol = new NewSymbolHolder("i", curDiffUnit(), adEnv.integerTypeSpec, -1);
        newSymbol.declarationLevelMustInclude(diffSymbolTable);
        newSymbol.preparePrivateClause(block, adEnv.curUnit().privateSymbolTable(), false, false);
        bwdBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_loop, null, null,
                ILUtils.build(ILLang.op_do,
                        newSymbol.makeNewRef(diffSymbolTable),
                        ILUtils.build(ILLang.op_intCst, 1),
                        adCountVarTool.makeRef(block),
                        ILUtils.build(ILLang.op_none)),
                null));
    }

    /** Force declaration of a new utility function for differentiation */
    private void forceDeclareDiffUtilityFunction(String funcName, WrapperTypeSpec resultType) {
        SymbolTable diffSymbolTable = curDiffUnit().externalSymbolTable() ;
        TapList<FunctionDecl> funcDecls =
            diffSymbolTable.getFunctionDecl(funcName, null, null, false);
        FunctionDecl funcDecl = (funcDecls==null ? null : funcDecls.head) ;
        if (funcDecl == null) {
            Unit funcUnit = adEnv.diffCallGraph().createNewUnit(null, adEnv.curUnit().language());
            funcUnit.setExternalSymbolTable(adEnv.diffCallGraph().languageRootSymbolTable(TapEnv.relatedLanguage()));
            funcUnit.setExternal();
            funcUnit.setName(funcName);
            Tree nameTree = ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), funcName);
            WrapperTypeSpec booleanTypeSpec = diffSymbolTable.getTypeDecl("boolean").typeSpec;
            funcUnit.setFunctionTypeSpec(new FunctionTypeSpec(booleanTypeSpec));
            funcDecl = new FunctionDecl(nameTree, funcUnit);
            nameTree.setAnnotation("isFunctionName", Boolean.TRUE);
            Instruction declInstr = new Instruction(
                    ILUtils.build(ILLang.op_varDeclaration,
                      ILUtils.build(ILLang.op_modifiers),
                      booleanTypeSpec.generateTree(null, null, null, true, null),
                      ILUtils.build(ILLang.op_declarators, nameTree)));
            if (curDiffUnit().isFortran()) {
                curDiffUnit().privateSymbolTable().declarationsBlock.addInstrDeclTlBeforeUse(declInstr, null,
                                                                                             null, null, true);
            }
            funcDecl.setInstruction(declInstr);
            diffSymbolTable.addSymbolDecl(funcDecl) ;
        }
    }

    /**
     * Analyzes the "state" and "parameter" variables of a given FIXEDPOINT loop.
     * Fills vector "stateVarsFromDirective" with the zones of state variables according
     * to the user-given directive, "paramVars" with the parameter variables according to in-out
     * analysis.
     */
    private void detectFixedPointParamsAndState(BoolVector stateVarsFromDirective,
                                                BoolVector paramVars,
                                                FlowGraphLevel fixedPointBody,
                                                HeaderBlock loopHeader,
                                                SymbolTable symbolTable, Tree[] directiveStateVars,
                                                ToObject<Tree[]> toAltStateVarsArray,
                                                BoolVector tbrOverwritten) {
        stateVarsFromDirective.setFalse();
        TapIntList zones;
        boolean stateGiven = (directiveStateVars.length>0) ;
        for (int i = directiveStateVars.length - 1; i >= 0; --i) {
            zones = symbolTable.listOfZonesOfValue(directiveStateVars[i], null, loopHeader.headInstr());
            DataFlowAnalyzer.setKindZoneRks(stateVarsFromDirective, adEnv.diffKind,
                    zones, true, symbolTable);
        }

        // For the next calls to IN-OUT analysis, do as if loopHeader is included in fixedPointBody:
        FGArrow exitFromHeader = loopHeader.getFGArrowTestCase(FGConstants.LOOP, FGConstants.EXIT);
        TapList<Block> blocksInsideLoop = new TapList<>(loopHeader, fixedPointBody.allBlocksInside());
        TapList<FGArrow> entryFrontier = loopHeader.backFlow();
        TapList<FGArrow> exitFrontier = new TapList<>(exitFromHeader, fixedPointBody.exitArrows);
        TapList<FGArrow> entryFrontierNoCycle = removeCyclingFrom(entryFrontier, blocksInsideLoop);
        BoolVector unusedAfterLoopVars = exitFromHeader.destination.unusedZones;
        InOutAnalyzer inOutAnalyzer = TapEnv.inOutAnalyzer();
        // Detect all variables read:
        inOutAnalyzer.setCurUnitEtc(adEnv.curUnit());
        inOutAnalyzer.setFgPhase(InOutAnalyzer.INOUT);
        inOutAnalyzer.analyzeBackward(entryFrontier, blocksInsideLoop, exitFrontier);
        // The next getFrontierPossiblyReadZones() must skip arrows cycling from blocksInsideLoop:
        BoolVector readVars = inOutAnalyzer.getFrontierPossiblyReadZones(entryFrontierNoCycle);
        // Detect all variables modified:
        inOutAnalyzer.setFgPhase(InOutAnalyzer.OTHER);
        inOutAnalyzer.setFgSubPhase(InOutAnalyzer.CONSTANT);
        inOutAnalyzer.analyzeBackward(entryFrontier, blocksInsideLoop, exitFrontier);
        // The next getFrontierModifiedZones() must skip arrows cycling from blocksInsideLoop:
        BoolVector modifiedVars = inOutAnalyzer.getFrontierModifiedZones(entryFrontierNoCycle);
        // Detect TBR zones that must be PUSH/POPped upstream this FP-LOOP:
        tbrOverwritten.setCopy(modifiedVars) ;
        int nDZ = loopHeader.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        if (adEnv.unitTBRs != null) {
            BoolVector fpLoopTBR = new BoolVector(nDZ) ;
            TapList<FGArrow> entryArrows = loopHeader.backFlow() ;
            Block beforeEntry ;
            BoolVector beforeEntryTBR ;
            SymbolTable commonSymbolTable ;
            // Compute the union of TBR zones on all entries into the Fixed-Point loop:
            while (entryArrows!=null) {
                if (!entryArrows.head.isCyclingArrow()) {
                    beforeEntry = entryArrows.head.origin ;
                    beforeEntryTBR = TapList.last(adEnv.unitTBRs.retrieve(beforeEntry)).first ;
                    commonSymbolTable = loopHeader.symbolTable.getCommonRoot(beforeEntry.symbolTable) ;
                    fpLoopTBR.cumulOr(beforeEntryTBR,
                            commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
                }
                entryArrows = entryArrows.tail ;
            }
            tbrOverwritten.cumulAnd(fpLoopTBR) ;
        }
        // Recompute the original in-out analysis results
        inOutAnalyzer.setFgPhase(InOutAnalyzer.INOUT);
        inOutAnalyzer.analyzeBackward(null, null, null);
        inOutAnalyzer.setFgPhase(InOutAnalyzer.OTHER);
        inOutAnalyzer.setFgSubPhase(InOutAnalyzer.CONSTANT);
        inOutAnalyzer.analyzeBackward(null, null, null);
        inOutAnalyzer.setCurUnitEtc(null);

        //[TODO] We must probably restrict the following to active variables only:
        BoolVector paramVarsFromDataFlow = readVars.minus(modifiedVars);
        BoolVector stateVarsFromDataFlow = modifiedVars.minus(unusedAfterLoopVars);

        paramVars.setFalse();
        ZoneInfo zi;
        int diffIndex ;
        boolean isState, isParam;
        TapList<Tree> stateVarsList = null ;
        TapList<Tree> paramVarsList = null ;
        TapList<Tree> altStateVarsList = null ;
        for (int i=nDZ-1 ; i>=0 ; --i) {
            isState = stateVarsFromDataFlow.get(i);
            isParam = paramVarsFromDataFlow.get(i);
            if (isState || isParam) {
                zi = symbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zi.realZoneNb >= 0) {
                        diffIndex = zi.kindZoneNb(adEnv.diffKind);
                        Tree accessTree = ILUtils.stripArrayAccesses(zi.accessTree) ;
                        if (isState) {
                            stateVarsList = new TapList<>(accessTree, stateVarsList) ;
                        }
                        if (isParam) {
                            paramVarsList = new TapList<>(accessTree, paramVarsList) ;
                        }
                        if (adEnv.traceCurDifferentiation) {
                            System.out.println("Index (r)"+zi.realZoneNb+(isState?" state":"")+(isParam?" param":"")+" for Zone "+zi+" diffIndex:"+diffIndex) ;
                        }
                        if (stateVarsFromDirective.get(diffIndex)) {
                            if (!modifiedVars.get(i)) {
                                TapEnv.fileWarning(5, loopHeader.lastInstr().tree, "(AD27) Variable " + accessTree + " is only read, therefore possibly should not be a state variable of this FIXEDPOINT loop");
                            }
                        } else {
                            if (isState) {
                                TapEnv.fileWarning(5, loopHeader.lastInstr().tree, "(AD26) Variable " + accessTree + " is overwritten, therefore possibly should be a state variable of this FIXEDPOINT loop");
                                if (!stateGiven) {
                                    // If no state var is given by the directive, take the probable state:
                                    altStateVarsList = new TapList<>(accessTree, altStateVarsList) ;
                                    stateVarsFromDirective.set(diffIndex, true) ;
                                }
                            }
                        }
                        if (isParam) {
                            paramVars.set(diffIndex, true);
                        }
                }
            }
        }
        if (adEnv.traceCurDifferentiation) {
            System.out.println("State variables from data-flow: "+stateVarsList) ;
            System.out.println("Param variables from data-flow: "+paramVarsList) ;
        }
        if (!stateGiven) {
            // If no state var is given by the directive, then take the probable state vars found above:
            TapEnv.fileWarning(5, loopHeader.lastInstr().tree, "(AD26) taking default state variables: "+altStateVarsList) ;
            Tree[] treeArray = new Tree[TapList.length(altStateVarsList)] ;
            int index = 0 ;
            while (altStateVarsList!=null) {
                treeArray[index] = altStateVarsList.head ;
                altStateVarsList = altStateVarsList.tail ;
                ++index ;
            }
            toAltStateVarsArray.setObj(treeArray) ;
        }
    }

    private TapList<Tree> collectActiveUsesOfParameters(TapList<Block> blocksInsideLoop,
                                                        BoolVector parameterZones, TapList<Tree> paramUses) {
        while (blocksInsideLoop!=null) {
            Block fpBlock = blocksInsideLoop.head ;
            TapList<Instruction> fpInstrs = fpBlock.instructions ;
            SymbolTable fpSymbolTable = fpBlock.symbolTable ;
            while (fpInstrs!=null) {
                paramUses = collectActiveUsesOfParameters(fpInstrs.head.tree, fpInstrs.head, fpSymbolTable,
                                                          parameterZones, paramUses) ;
                fpInstrs = fpInstrs.tail ;
            }

            blocksInsideLoop = blocksInsideLoop.tail ;
        }
        return paramUses ;
    }

    /** Accumulates into paramUses the list of all differentiable uses of an expression which at the same time
     * is identified as a fixed-point parameter (its zone is in parameterZones) and is used differentiably. */
    private TapList<Tree> collectActiveUsesOfParameters(Tree tree, Instruction instr, SymbolTable symbolTable,
                                                        BoolVector parameterZones, TapList<Tree> paramUses) {
        switch (tree.opCode()) {
        case ILLang.op_call: {
            // Due to possible generalization of the call context, the adjoint call may modify the adjoint of a parameter argument,
            // even if this is not necessary because actual argument is passive.
            // In that case, the parameter must be returned as used differentiably:
            ActivityPattern curCalledActivity =
                (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(tree, adEnv.curActivity(), "multiActivityCalleePatterns");
            Unit calledUnit = DataFlowAnalyzer.getCalledUnit(tree, symbolTable);
            CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit) ;
            Tree[] actualArgs = ILUtils.getArguments(tree).children() ;
            boolean[] formalArgsActivity = ADActivityAnalyzer.formalArgsActivity(curCalledActivity);
            // formalArgsActivity can be null for  a passive call of an otherwise active procedure
            if (formalArgsActivity!=null) {
                for (int i=actualArgs.length-1 ; i>=0 ; --i) {
                    Tree argTree = actualArgs[i] ;
                    int argTreeOpCode = argTree.opCode() ;
                    if (formalArgsActivity[i]) {
                        if (!calledUnit.isIntrinsic()
                            // ^ Intrinsic calls never have a generalized call context: treat them as normal differentiable operations.
                            && (argTreeOpCode==ILLang.op_ident || argTreeOpCode==ILLang.op_arrayAccess
                                || argTreeOpCode==ILLang.op_fieldAccess || argTreeOpCode==ILLang.op_pointerAccess)) {
                            TapIntList argZones = symbolTable.listOfZonesOfValue(argTree, null, instr) ;
                            argZones = DataFlowAnalyzer.mapZoneRkToKindZoneRk(argZones, adEnv.diffKind, symbolTable) ;
                            if (parameterZones.intersects(argZones) && !TapList.containsTree(paramUses, argTree)) {
                                paramUses = new TapList<>(argTree, paramUses) ;
                            }
                        } else {
                            paramUses = collectActiveUsesOfParameters(argTree, instr, symbolTable, parameterZones, paramUses) ;
                        }
                    }
                }
            }
            // and now for globals:
            if (curCalledActivity!=null) {
                BoolVector formalCallActive = new BoolVector(symbolTable.freeDeclaredZone(adEnv.diffKind)) ;
                DataFlowAnalyzer.translateCalleeDataToCallSite(curCalledActivity.callActivity(), formalCallActive,
                        null, null, true,
                        tree, instr, callArrow, adEnv.diffKind, null, null, true, false) ;
                formalCallActive.cumulAnd(parameterZones) ;
                for (int i=symbolTable.freeDeclaredZone(adEnv.diffKind)-1 ; i>=0 ; --i) {
                    if (formalCallActive.get(i)) {
                        ZoneInfo zi = symbolTable.declaredZoneInfo(i, adEnv.diffKind) ;
                        Tree accessTree = ILUtils.stripArrayAccesses(zi.accessTree) ;
                        if (!TapList.containsTree(paramUses, accessTree)) {
                            paramUses = new TapList<>(accessTree, paramUses) ;
                        }
                    }
                }
            }
            return paramUses ;
        }
        case ILLang.op_assign :
        case ILLang.op_pointerAssign :
            return collectActiveUsesOfParameters(tree.down(2), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_ident:
        case ILLang.op_arrayAccess:
        case ILLang.op_fieldAccess:
        case ILLang.op_pointerAccess: {
            TapIntList treeZones = symbolTable.listOfZonesOfValue(tree, null, instr) ;
            treeZones = DataFlowAnalyzer.mapZoneRkToKindZoneRk(treeZones, adEnv.diffKind, symbolTable) ;
            if (parameterZones.intersects(treeZones) && ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, symbolTable)) {
                if (!TapList.containsTree(paramUses, tree)) {
                    paramUses = new TapList<>(tree, paramUses) ;
                }
            }
            return paramUses ;
        }
        case ILLang.op_add:
        case ILLang.op_sub:
        case ILLang.op_mul:
        case ILLang.op_div:
        case ILLang.op_power:
        case ILLang.op_mod:
        case ILLang.op_complexConstructor:
            paramUses = collectActiveUsesOfParameters(tree.down(1), instr, symbolTable, parameterZones, paramUses) ;
            return collectActiveUsesOfParameters(tree.down(2), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_binary:
            paramUses = collectActiveUsesOfParameters(tree.down(1), instr, symbolTable, parameterZones, paramUses) ;
            return collectActiveUsesOfParameters(tree.down(3), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_strings:
        case ILLang.op_expressions:
        case ILLang.op_arrayConstructor:
        case ILLang.op_blockStatement:
        case ILLang.op_dimColons:
        case ILLang.op_declarators: {
            Tree[] exps = tree.children();
            for (Tree exp : exps) {
                paramUses = collectActiveUsesOfParameters(exp, instr, symbolTable, parameterZones, paramUses) ;
            }
            return paramUses ;
        }
        case ILLang.op_address:
            return collectActiveUsesOfParameters(tree.down(1), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_minus:
            return collectActiveUsesOfParameters(tree.down(1), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_unary:
        case ILLang.op_cast:
        case ILLang.op_nameEq:
        case ILLang.op_return:
            return collectActiveUsesOfParameters(tree.down(2), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_constructorCall:
        case ILLang.op_varDeclaration:
             return collectActiveUsesOfParameters(tree.down(3), instr, symbolTable, parameterZones, paramUses) ;
        case ILLang.op_allocate:
        case ILLang.op_deallocate:
        case ILLang.op_nullify:
        case ILLang.op_sizeof:
        case ILLang.op_not:
        case ILLang.op_where:
        case ILLang.op_switch:
        case ILLang.op_arrayTriplet:
        case ILLang.op_if:
        case ILLang.op_ifExpression:
        case ILLang.op_iterativeVariableRef:
        case ILLang.op_loop:
        case ILLang.op_do:
        case ILLang.op_dimColon:
        case ILLang.op_neq:
        case ILLang.op_eq:
        case ILLang.op_and:
        case ILLang.op_or:
        case ILLang.op_xor:
        case ILLang.op_bitAnd:
        case ILLang.op_bitOr:
        case ILLang.op_bitXor:
        case ILLang.op_ge:
        case ILLang.op_le:
        case ILLang.op_gt:
        case ILLang.op_lt:
        case ILLang.op_concat:
        case ILLang.op_concatIdent:
        case ILLang.op_leftShift:
        case ILLang.op_rightShift:
        case ILLang.op_realCst:
        case ILLang.op_intCst:
        case ILLang.op_boolCst:
        case ILLang.op_bitCst:
        case ILLang.op_ioCall:
        case ILLang.op_stringCst:
        case ILLang.op_letter:
        case ILLang.op_label:
        case ILLang.op_stop:
        case ILLang.op_break:
        case ILLang.op_compGoto:
        case ILLang.op_assignLabelVar:
        case ILLang.op_gotoLabelVar:
        case ILLang.op_none:
        case ILLang.op_star:
        case ILLang.op_continue:
        case ILLang.op_varDimDeclaration:
        case ILLang.op_typeDeclaration:
        case ILLang.op_constDeclaration:
        case ILLang.op_external:
        case ILLang.op_useDecl:
        case ILLang.op_interfaceDecl:
        case ILLang.op_intrinsic:
        case ILLang.op_accessDecl:
        case ILLang.op_common:
        case ILLang.op_nameList:
        case ILLang.op_equivalence:
        case ILLang.op_include:
        case ILLang.op_save:
        case ILLang.op_data:
        case ILLang.op_arrayType:
        case ILLang.op_pointerType:
        case ILLang.op_enumType:
        case ILLang.op_unionType:
        case ILLang.op_recordType:
        case ILLang.op_functionType:
        case ILLang.op_modifiedType:
        case ILLang.op_void:
        case ILLang.op_integer:
        case ILLang.op_float:
        case ILLang.op_complex:
        case ILLang.op_boolean:
        case ILLang.op_character:
            return paramUses ;
        default:
            TapEnv.toolWarning(-1, "(Collect differentiable parameter uses) Unexpected operator: " + tree.opName());
            return paramUses ;
        }
    }

    /**
     * Run a new local activity analysis on the given flow-graph portion "portion",
     * but after modifying the independents at the beginning of the portion by removing the
     * zones in "removedIndeps. The original independents at the beginning of the portion
     * and the dependents at the exit of the portion are found in the given "portionMemo".
     * This allows one to reset to the results of the "normal" activity by passing a null
     * into "removedIndeps". This is used e.g. for fixed-point adjoints
     *
     * @param portionMemo a pair of the list of activity vectors at the exit
     *                    of the blocks just before the entry frontier, and of the list of usefulness
     *                    vectors at the entry of the blocks just after the exit frontier.
     */
    private void computeLocalActivity(FlowGraphLevel portion,
                                      BoolVector removedIndeps,
                                      TapPair<TapList<BoolVector>, TapList<BoolVector>> portionMemo) {
        TapList<Block> blocksInsideLoop = portion.allBlocksInside();
        TapList<FGArrow> entryFrontier = portion.entryArrows;
        TapList<FGArrow> exitFrontier = portion.exitArrows;
        TapList<BoolVector> entryActivities = portionMemo.first;
        TapList<BoolVector> exitActivities = portionMemo.second;
        BoolVector entryVector, exitVector;
        int width;
        activityAnalyzer().setCurUnitEtc(adEnv.curUnit());
        BoolVector variedEntries = null;
        while (entryFrontier != null) {
            entryVector = entryActivities.head;
            width = entryFrontier.head.commonSymbolTable().declaredZonesNb(adEnv.diffKind);
            if (variedEntries == null) {
                variedEntries = entryVector.copy(width, width);
            } else {
                variedEntries.cumulOr(entryVector, width);
            }
            entryActivities = entryActivities.tail;
            entryFrontier = entryFrontier.tail;
        }
        if (removedIndeps != null) {
            variedEntries.cumulMinus(removedIndeps);
        }
        entryFrontier = portion.entryArrows;
        BlockStorage<TapList<BoolVector>> curUnitVariednesses = adEnv.curActivity().activities();
        activityAnalyzer().setPhaseVaried(variedEntries, curUnitVariednesses);
        activityAnalyzer().analyzeForward(entryFrontier, blocksInsideLoop, exitFrontier);
        BoolVector usefulExits = null;
        while (exitFrontier != null) {
            exitVector = exitActivities.head;
            width = exitFrontier.head.commonSymbolTable().declaredZonesNb(adEnv.diffKind);
            if (usefulExits == null) {
                usefulExits = exitVector.copy(width, width);
            } else {
                usefulExits.cumulOr(exitVector, width);
            }
            exitActivities = exitActivities.tail;
            exitFrontier = exitFrontier.tail;
        }
        exitFrontier = portion.exitArrows;
        BlockStorage<TapList<BoolVector>> curUnitUsefulnesses = adEnv.curActivity().usefulnesses();
        activityAnalyzer().setPhaseUseful(usefulExits, curUnitUsefulnesses);
        activityAnalyzer().analyzeBackward(entryFrontier, blocksInsideLoop, exitFrontier);
        activityAnalyzer().reMergeAndAnnotate(blocksInsideLoop, adEnv.curActivity());
        // [llh 3/10/14] TODO: Also compute local diffliveness, to remove adjoint-dead code
        // from the FWD sweep, for instance the code that computes the fixed-point residual.
    }

    /** Utility for e.g. adjoint of fixed-point loops: places a storage and a restore of the diff of the
     * given refExpr, into storeBlock and restoreBlock respectively */
    private void saveRestoreDiffRefExpression(Tree refExpr, Block storeBlock, Block restoreBlock,
                                              SymbolTable symbolTable, SymbolTable diffSymbolTable,
                                              IterDescriptor multiDirIterDescriptor) {

        Tree diffRefBase =
            ILUtils.baseTree(
                varRefDifferentiator().diffVarRef(adEnv.curActivity(), refExpr, diffSymbolTable,
                                                  false, refExpr, false, true, refExpr)) ;
        RefDescriptor refDescriptor =
            new RefDescriptor(refExpr, null, null, symbolTable, diffSymbolTable,
                              null, diffRefBase, true, multiDirIterDescriptor, curDiffUnit()) ;
        WrapperTypeSpec refType = refDescriptor.completeType ;
        // TODO: take care of refType when some sizes inside are unknown statically !
        NewSymbolHolder saveVarHolder = new NewSymbolHolder("diffparam_save") ;
        saveVarHolder.setAsVariable(refType, null) ;
        saveVarHolder.declarationLevelMustInclude(storeBlock.symbolTable) ;
        saveVarHolder.declarationLevelMustInclude(restoreBlock.symbolTable) ;
        Tree saveVarRef = saveVarHolder.makeNewRef(diffSymbolTable);
        RefDescriptor saveVarRefDescriptor =
            new RefDescriptor(saveVarRef, null, null, refType, null, diffSymbolTable,
                              curDiffUnit().privateSymbolTable(), storeBlock.symbolTable,
                              null, false, null, curDiffUnit()) ;
        refDescriptor.forceStaticSave(saveVarRefDescriptor);
        refDescriptor.prepareForAssignOrNormDiff(saveVarRefDescriptor, curDiffUnit(), null, true) ;
        // Put the actual Save/Restore of the adjoints of refExpr into storeBlock and restoreBlock:
        storeBlock.addInstrHdAfterDecls(refDescriptor.makePush()) ;
        restoreBlock.addInstrHdAfterDecls(refDescriptor.makePop()) ;
    }

    /**
     * Fills the Blocks that manage snapshots in standard checkpointing.
     * Fill the 3 (lists of) Blocks pushBlocks, popBlocks, and popBackBlocks with
     * the instructions that store and restore the checkpoint defined by "sbk" and "snp".
     * In all cases, all variables (in sbk or snp) are PUSH'ed in the pushBlocks.<br>
     * If sbk is null or empty, then everything is POP'ed in the popBlocks,
     * and the popBackBlocks are unused and can be null.<br>
     * If snp is null or empty, then everything is POP'ed in the popBackBlocks,
     * and the popBlocks are unused and can be null.<br>
     * Otherwise variables in both snp and sbk are LOOK'ed in the popBlocks and
     * POP'ed in the popBackBlocks.
     */
    private void fillSnapshotInstructions(BoolVector sbk, BoolVector snp, int len,
                                          BoolVector sbkOnDiffPtr, BoolVector snpOnDiffPtr, int lenOnDiffPtr,
                                          Block sourceBlock, TapList<Block> pushBlocks, TapList<Block> popBlocks,
                                          TapList<Block> popBackBlocks, boolean inJointDiffCode) {
        SymbolTable sourceSymbolTable = sourceBlock.symbolTable;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("   Filling snapshot instructions for SNP:" + DataFlowAnalyzer.infoToStringWithDiffPtr(snp, len, snpOnDiffPtr, lenOnDiffPtr, adEnv.curUnit(), sourceSymbolTable) + " SBK:" + DataFlowAnalyzer.infoToStringWithDiffPtr(sbk, len, sbkOnDiffPtr, lenOnDiffPtr, adEnv.curUnit(), sourceSymbolTable) + " into Blocks " + pushBlocks + popBlocks + popBackBlocks);
        }
        int nDZ = sourceSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        ZoneInfo zoneInfo;
        boolean saveRestoreIO = false;
        boolean snpPrimal, snpDiffPtr, sbkPrimal, sbkDiffPtr;
        int rk, rkOnDiffPtr;
        TapList<TapPair<ZoneInfo, Integer>> hdSnpOnly = new TapList<>(null, null);
        TapList<TapPair<ZoneInfo, Integer>> hdSnpSbk = new TapList<>(null, null);
        TapList<TapPair<ZoneInfo, Integer>> hdSbkOnly = new TapList<>(null, null);
        TapList<TapPair<ZoneInfo, Integer>> tlSnpOnly = hdSnpOnly;
        TapList<TapPair<ZoneInfo, Integer>> tlSnpSbk = hdSnpSbk;
        TapList<TapPair<ZoneInfo, Integer>> tlSbkOnly = hdSbkOnly;
        for (int i = nDZ - 1; i >= 0; --i) {
            zoneInfo = sourceSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null) {
                rk = zoneInfo.kindZoneNb(SymbolTableConstants.ALLKIND);
                snpPrimal = snp != null && snp.get(rk);
                sbkPrimal = sbk != null && sbk.get(rk);
                rkOnDiffPtr = zoneInfo.kindZoneNb(SymbolTableConstants.PTRKIND);
                snpDiffPtr = rkOnDiffPtr > 0 && snpOnDiffPtr != null && snpOnDiffPtr.get(rkOnDiffPtr);
                sbkDiffPtr = rkOnDiffPtr > 0 && sbkOnDiffPtr != null && sbkOnDiffPtr.get(rkOnDiffPtr);
                if (snpPrimal || sbkPrimal || snpDiffPtr || sbkDiffPtr) {
                    if (zoneInfo.zoneNb == adEnv.srcCallGraph().zoneNbOfAllIOStreams) {
                        saveRestoreIO = true;
                    } else if (zoneInfo.isHidden) {
                        // Complain if zoneInfo must be saved and is hidden:
                        if (!zoneInfo.comesFromAllocate()
                                && !adEnv.srcCallGraph().isAMessagePassingChannelZone(zoneInfo.zoneNb)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, sourceBlock.lastInstr().tree, "(AD14) Checkpointing this piece of code needs to save " + ((snpDiffPtr || sbkDiffPtr) && !(snpPrimal || sbkPrimal) ? "the derivative of " : "") + "a hidden variable: " + zoneInfo.description + ((snpDiffPtr || sbkDiffPtr) && (snpPrimal || sbkPrimal) ? " and its derivative" : ""));
                            adEnv.addInHiddenPushPopVariables(zoneInfo);
                        }
                    } else {
                        // Caution: when doing push/look/pop, variables must be sorted in three groups
                        // to ensure correct sequence: sbk only, sbk and snp, snp only.
                        // Convention: 1:only_primal -1:only_diffptr 0:primal_and_diffptr
                        if (snpPrimal) {
                            if (sbkPrimal) {
                                if (snpDiffPtr) {
                                    if (sbkDiffPtr) {
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, 0));
                                    } else {
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    }
                                } else {
                                    if (sbkDiffPtr) {
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, 1));
                                    }
                                }
                            } else {
                                if (snpDiffPtr) {
                                    if (sbkDiffPtr) {
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, 0));
                                    }
                                } else {
                                    if (sbkDiffPtr) {
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, 1));
                                    }
                                }
                            }
                        } else {
                            if (sbkPrimal) {
                                if (snpDiffPtr) {
                                    if (sbkDiffPtr) {
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, 1));
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    }
                                } else {
                                    if (sbkDiffPtr) {
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, 0));
                                    } else {
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, 1));
                                    }
                                }
                            } else {
                                if (snpDiffPtr) {
                                    if (sbkDiffPtr) {
                                        tlSnpSbk = tlSnpSbk.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                        tlSnpOnly = tlSnpOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    }
                                } else {
                                    if (sbkDiffPtr) {
                                        tlSbkOnly = tlSbkOnly.placdl(new TapPair<>(zoneInfo, -1));
                                    } else {
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (saveRestoreIO) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, sourceBlock.lastInstr().tree, "(AD07) Checkpointing this piece of code needs to save the I-O state");
        }
        hdSnpOnly = hdSnpOnly.tail;
        int zoneCase;
        while (hdSnpOnly != null) {
            zoneInfo = hdSnpOnly.head.first;
            zoneCase = hdSnpOnly.head.second;
            placePushPopLookOfZone(zoneInfo, zoneCase >= 0, false, zoneCase <= 0, false, sourceBlock, 0,
                    pushBlocks, popBlocks, null, inJointDiffCode, null);
            hdSnpOnly = hdSnpOnly.tail;
        }
        hdSnpSbk = hdSnpSbk.tail;
        if (hdSnpSbk != null) {
            // Replace the old "LOOK" mechanism with the new "startRepeat/resetRepeat/endRepeat"
            TapList<Block> inPopBlocks = popBlocks;
            Block popBlock;
            // Remember the current location in each of popBlocks,
            // to place the adStack_startRepeat() there if needed.
            TapIntList hdRanksInPopBlocks = new TapIntList(-1, null);
            TapIntList tlRanksInPopBlocks = hdRanksInPopBlocks;
            while (inPopBlocks != null) {
                popBlock = inPopBlocks.head;
                tlRanksInPopBlocks = tlRanksInPopBlocks.placdl(TapList.length(popBlock.instructions));
                inPopBlocks = inPopBlocks.tail;
            }
            ToBool reallyNeedsToLook = new ToBool(false);
            while (hdSnpSbk != null) {
                zoneInfo = hdSnpSbk.head.first;
                zoneCase = hdSnpSbk.head.second;
                placePushPopLookOfZone(zoneInfo, zoneCase >= 0, zoneCase >= 0, zoneCase <= 0, zoneCase <= 0, sourceBlock, 0,
                        pushBlocks, popBlocks, popBackBlocks, inJointDiffCode, reallyNeedsToLook);
                hdSnpSbk = hdSnpSbk.tail;
            }
            // If at least one of the created "restore" is a LOOK, then we must put the "repeatStack" in place:
            if (reallyNeedsToLook.get()) {
                inPopBlocks = popBlocks;
                TapIntList ranksOfStartRepeat = hdRanksInPopBlocks.tail;
                int rankOfStartRepeat;
                while (inPopBlocks != null) {
                    popBlock = inPopBlocks.head;
                    rankOfStartRepeat = ranksOfStartRepeat.head;
                    popBlock.addInstructionAt(ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adStack_startRepeat"),
                            ILUtils.build(ILLang.op_expressions)),
                            rankOfStartRepeat);
                    popBlock.addInstrTl(ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adStack_resetRepeat"),
                            ILUtils.build(ILLang.op_expressions)));
                    popBlock.addInstrTl(ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adStack_endRepeat"),
                            ILUtils.build(ILLang.op_expressions)));
                    inPopBlocks = inPopBlocks.tail;
                    ranksOfStartRepeat = ranksOfStartRepeat.tail;
                }
            }
        }
        hdSbkOnly = hdSbkOnly.tail;
        while (hdSbkOnly != null) {
            zoneInfo = hdSbkOnly.head.first;
            zoneCase = hdSbkOnly.head.second;
            placePushPopLookOfZone(zoneInfo, false, zoneCase >= 0, false, zoneCase <= 0, sourceBlock, 0,
                    pushBlocks, null, popBackBlocks, inJointDiffCode, null);
            hdSbkOnly = hdSbkOnly.tail;
        }
    }

    /**
     * Fills the Blocks that manage snapshots in binomial checkpointing.
     * For each zone in checkpoint "ckp", places a PUSH of it into pushBlock,
     * a LOOK into lookBlock, and a POP into popBlock.
     */
    private void fillBinomialCheckpoint(BoolVector ckp, int len, BoolVector ckpOnDiffPtr, int lenOnDiffPtr,
                                        Block sourceBlock, Block pushBlock, Block lookBlock, Block popBlock, boolean inJointDiffCode) {
        SymbolTable sourceSymbolTable = sourceBlock.symbolTable;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("   Filling binomial snapshot instructions for CKP:" + DataFlowAnalyzer.infoToStringWithDiffPtr(ckp, len, ckpOnDiffPtr, lenOnDiffPtr, adEnv.curUnit(), sourceSymbolTable) + " into Blocks " + pushBlock + ";" + lookBlock + ";" + popBlock);
        }
        int nDZ = sourceSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        ZoneInfo zoneInfo;
        boolean saveRestoreIO = false;
        boolean ckpPrimal, ckpDiffPtr;
        boolean canHaveDiff;
        int rk, rkOnDiffPtr;
        ToBool reallyNeedsToLook = new ToBool(false);
        for (int i = nDZ - 1; i >= 0; --i) {
            zoneInfo = sourceSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null) {
                rk = zoneInfo.kindZoneNb(SymbolTableConstants.ALLKIND);
                ckpPrimal = ckp != null && ckp.get(rk);
                rkOnDiffPtr = zoneInfo.kindZoneNb(SymbolTableConstants.PTRKIND);
                canHaveDiff = TypeSpec.isDifferentiableType(zoneInfo.type) && zoneInfo.isOnceActive();
                ckpDiffPtr = canHaveDiff && ckpOnDiffPtr != null && rkOnDiffPtr > 0 && ckpOnDiffPtr.get(rkOnDiffPtr);
                if (ckpPrimal || ckpDiffPtr) {
                    if (zoneInfo.zoneNb == adEnv.srcCallGraph().zoneNbOfAllIOStreams) {
                        saveRestoreIO = true;
                    } else if (zoneInfo.isHidden /*WAS zoneInfo.isHiddenFrom(sourceSymbolTable)*/) {
                        // Complain if zoneInfo must be saved and is hidden:
                        if (!zoneInfo.comesFromAllocate()
                                && !adEnv.srcCallGraph().isAMessagePassingChannelZone(zoneInfo.zoneNb)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, sourceBlock.lastInstr().tree, "(AD14) Binomial checkpointing of this piece of code needs to save " + (ckpDiffPtr && !ckpPrimal ? "the derivative of " : "") + "a hidden variable: " + zoneInfo.description + (ckpDiffPtr && ckpPrimal ? " and its derivative" : ""));
                            adEnv.addInHiddenPushPopVariables(zoneInfo);
                        }
                    } else if (ckp.get(i)) {
                        placePushPopLookOfZone(zoneInfo, ckpPrimal, ckpPrimal, ckpDiffPtr, ckpDiffPtr, sourceBlock, 2,
                                new TapList<>(pushBlock, null), new TapList<>(lookBlock, null), new TapList<>(popBlock, null),
                                inJointDiffCode, reallyNeedsToLook);
                    }
                }
            }
        }
        // Replace the old "LOOK" mechanism with the new "startRepeat/resetRepeat/endRepeat"
        if (lookBlock.instructions != null && reallyNeedsToLook.get()) {
            lookBlock.addInstrHd(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_startRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            lookBlock.addInstrTl(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_resetRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
            lookBlock.addInstrTl(ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adStack_endRepeat"),
                    ILUtils.build(ILLang.op_expressions)));
        }
        if (saveRestoreIO) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, sourceBlock.lastInstr().tree, "(AD07) Binomial checkpointing of this piece of code needs to save the I-O state");
        }
    }

    /**
     * Places instructions to PUSH/LOOK/POP the given zone "zoneInfo" (or its derivative when "onDiffPtr")
     * into each of pushBlocks/popBlocks1/popBlocks2 respectively.
     */
    private void placePushPopLookOfZone(ZoneInfo zoneInfo, boolean onPrimal1, boolean onPrimal2,
                                        boolean onDiffPtr1, boolean onDiffPtr2, Block sourceBlock, int knownMultiplicity,
                                        TapList<Block> pushBlocks, TapList<Block> popBlocks1, TapList<Block> popBlocks2,
                                        boolean inJointDiffCode, ToBool reallyNeedsToLook) {
        if (!zoneInfo.isHidden
            && !zoneInfo.isChannelOrIO
            && !zoneInfo.isControl()
            && !zoneInfo.isAllocatable //NoPushAllocatable
            && !zoneInfo.comesFromAllocate()) {
          Tree zoneInfoVisibleAccessTree = sourceBlock.symbolTable.buildZoneVisibleAccessTree(zoneInfo);
          if (zoneInfoVisibleAccessTree==null) {
              TapEnv.fileWarning(10, null, "(ADxx) Can not access for push/pop "+zoneInfo) ;
          } else {
            TapList<Block> inBlocks;
            Tree restoreTree, rebaseTree;
            Tree baseVarTree, fwdDiffBaseVar = null, bwdDiffBaseVar = null;
            RefDescriptor fwdRefDescriptor, bwdRefDescriptor = null;
            SymbolTable pushingSymbolTable = pushBlocks.head.symbolTable;
            SymbolTable poppingSymbolTable = (popBlocks1 != null ? popBlocks1 : popBlocks2).head.symbolTable;
            if (onDiffPtr1 || onDiffPtr2) {
                baseVarTree = ILUtils.baseTree(zoneInfoVisibleAccessTree);
                fwdDiffBaseVar =
                        varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, pushingSymbolTable,
                                true, false, false, null, false,
                                curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
                bwdDiffBaseVar =
                        varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, poppingSymbolTable,
                                true, false, false, null, false,
                                curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
            }
            if (onPrimal1 || onPrimal2) {
                fwdRefDescriptor =
                        new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                sourceBlock.symbolTable, curFwdDiffUnit().privateSymbolTable(),
                                null, null, false, null, curFwdDiffUnit());
                bwdRefDescriptor =
                        new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                sourceBlock.symbolTable, curDiffUnit().privateSymbolTable(),
                                null, null, false, null, curDiffUnit());
                adEnv.setCurBlock(sourceBlock);
                blockDifferentiator().prepareRestoreOperations(fwdRefDescriptor, bwdRefDescriptor,
                        knownMultiplicity, pushingSymbolTable, poppingSymbolTable);
                for (inBlocks = pushBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
                    inBlocks.head.addInstrHdAfterDecls(fwdRefDescriptor.makePush());
                }
                if (onPrimal1) {
                    for (inBlocks = popBlocks1; inBlocks != null; inBlocks = inBlocks.tail) {
                        bwdRefDescriptor.toBranchVariable = toBranchVariable;
                        restoreTree = bwdRefDescriptor.makePop();
                        if (reallyNeedsToLook != null && !bwdRefDescriptor.usesStaticSave()) {
                            reallyNeedsToLook.set(true);
                        }
                        inBlocks.head.addInstrTl(restoreTree);
                    }
                }
                if (onPrimal2) {
                    for (inBlocks = popBlocks2; inBlocks != null; inBlocks = inBlocks.tail) {
                        bwdRefDescriptor.toBranchVariable = toBranchVariable;
                        restoreTree = bwdRefDescriptor.makePop();
                        inBlocks.head.addInstrTl(restoreTree);
                    }
                }
            }
            if (onDiffPtr1 || onDiffPtr2) {
                NewSymbolHolder fwdDiffSymbolHolder = NewSymbolHolder.getNewSymbolHolder(fwdDiffBaseVar);
                RefDescriptor fwdRefDescriptorOnDiff =
                        new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                sourceBlock.symbolTable, curFwdDiffUnit().privateSymbolTable(),
                                null, fwdDiffBaseVar, true, null, curFwdDiffUnit());
                NewSymbolHolder bwdDiffSymbolHolder = NewSymbolHolder.getNewSymbolHolder(bwdDiffBaseVar);
                RefDescriptor bwdRefDescriptorOnDiff =
                        new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                sourceBlock.symbolTable, curDiffUnit().privateSymbolTable(),
                                null, fwdDiffBaseVar, true, null, curDiffUnit());
                adEnv.setCurBlock(sourceBlock);
                blockDifferentiator().prepareRestoreOperations(fwdRefDescriptorOnDiff, bwdRefDescriptorOnDiff,
                        knownMultiplicity, pushingSymbolTable, poppingSymbolTable);
                for (inBlocks = pushBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
                    fwdDiffSymbolHolder.declarationLevelMustInclude(inBlocks.head.symbolTable);
                    inBlocks.head.addInstrHdAfterDecls(fwdRefDescriptorOnDiff.makePush());
                }
                if (onDiffPtr1) {
                    for (inBlocks = popBlocks1; inBlocks != null; inBlocks = inBlocks.tail) {
                        bwdDiffSymbolHolder.declarationLevelMustInclude(inBlocks.head.symbolTable);
                        bwdRefDescriptorOnDiff.toBranchVariable = toBranchVariable;
                        restoreTree = bwdRefDescriptorOnDiff.makePop();
                        if (reallyNeedsToLook != null && !bwdRefDescriptorOnDiff.usesStaticSave()) {
                            reallyNeedsToLook.set(true);
                        }
                        inBlocks.head.addInstrTl(restoreTree);
                    }
                }
                if (onDiffPtr2) {
                    for (inBlocks = popBlocks2; inBlocks != null; inBlocks = inBlocks.tail) {
                        bwdDiffSymbolHolder.declarationLevelMustInclude(inBlocks.head.symbolTable);
                        bwdRefDescriptorOnDiff.toBranchVariable = toBranchVariable;
                        restoreTree = bwdRefDescriptorOnDiff.makePop();
                        inBlocks.head.addInstrTl(restoreTree);
                    }
                }
            }
            if (zoneInfo.ptrZoneNb != -1) {
                ++blockDifferentiator().numRebase;
                Instruction firstInstruction = sourceBlock.headInstr();
                if (onPrimal1 || onDiffPtr1) {
                    Tree refTree = bwdRefDescriptor.refTree();
                    boolean rebasePrimal1 =
                            onPrimal1 && DataFlowAnalyzer.mayPointToRelocated(refTree, false, firstInstruction, sourceBlock.symbolTable, inJointDiffCode);
                    boolean rebaseDiffPtr1 =
                            onDiffPtr1 && DataFlowAnalyzer.mayPointToRelocated(refTree, true, firstInstruction, sourceBlock.symbolTable, inJointDiffCode);
                    if (rebasePrimal1 || rebaseDiffPtr1) {
                        adEnv.usesADMM = true;
                        for (inBlocks = popBlocks1; inBlocks != null; inBlocks = inBlocks.tail) {
                            rebaseTree = bwdRefDescriptor.makeRebase(rebasePrimal1 ? null : new TapList<>(Boolean.TRUE, null),
                                    rebaseDiffPtr1 ? null : new TapList<>(Boolean.TRUE, null),
                                    rebaseDiffPtr1 ? bwdDiffBaseVar : null,
                                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
                            if (rebaseTree != null) {
                                inBlocks.head.addInstrTl(rebaseTree);
                            }
                        }
                    }
                }
                if (onPrimal2 || onDiffPtr2) {
                    Tree refTree = bwdRefDescriptor.refTree();
                    boolean rebasePrimal2 =
                            onPrimal2 && DataFlowAnalyzer.mayPointToRelocated(refTree, false, firstInstruction, sourceBlock.symbolTable, inJointDiffCode);
                    boolean rebaseDiffPtr2 =
                            onDiffPtr2 && DataFlowAnalyzer.mayPointToRelocated(refTree, true, firstInstruction, sourceBlock.symbolTable, inJointDiffCode);
                    if (rebasePrimal2 || rebaseDiffPtr2) {
                        adEnv.usesADMM = true;
                        for (inBlocks = popBlocks2; inBlocks != null; inBlocks = inBlocks.tail) {
                            rebaseTree = bwdRefDescriptor.makeRebase(rebasePrimal2 ? null : new TapList<>(Boolean.TRUE, null),
                                    rebaseDiffPtr2 ? null : new TapList<>(Boolean.TRUE, null),
                                    rebaseDiffPtr2 ? bwdDiffBaseVar : null,
                                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
                            if (rebaseTree != null) {
                                inBlocks.head.addInstrTl(rebaseTree);
                            }
                        }
                    }
                }
            }
          }
        }
    }

    /**
     * Memo of the real origBlock of op_return instructions that have been moved at the end
     * of an activitySwitch Block. For use in debugTGT.
     */
    private TapList<TapPair<Instruction, Block>> realOrigBlockOfReturns;

    /**
     * For TANGENT differentiation, inserts at the origin of the given fwd diff FGArrow "fwdDiffArrow"
     * a new Block that initializes tangent derivatives because their activity status switches
     * when going through the corresponding source FGArrow "sourceArrow".
     *
     * @param fwdDiffSymbolTable is the symbol table in which the diff variables to be
     *                           reinitialized are living
     * @param returnDangerBlock  is a Block to check for a possible "return"
     *                           statement that must be moved after these reinitializations.
     */
    private void insertTangentDiffReinits(FGArrow fwdDiffArrow, FGArrow sourceArrow,
                                          SymbolTable fwdDiffSymbolTable, Block returnDangerBlock,
                                          FlowGraphDifferentiationEnv diffEnv) {
        if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
            // Special case: when sourceArrow jumps into an OpenMP parallel region or parallel loop,
            // the activity switch occurs on the following FGArrow that exits from the OMP pragma.
            // However, we think the diff initialization must be done now, before entering the OMP pragma
            FGArrow activityCheckArrow = sourceArrow;
            if (!activityCheckArrow.origin.isParallelController()
                    && activityCheckArrow.destination.isParallelController()) {
                while (activityCheckArrow.destination.isParallelController()) {
                    activityCheckArrow = activityCheckArrow.destination.flow().head;
                }
            }
            tangentDiffReinitChecker.set(activityCheckArrow.rank, true);
            TapList<TapPair<ZoneInfo, TapList<Tree>>> toNewDeclaredInits = new TapList<>(null, null);
            Block diffInitBlock = checkActivitySwitches(activityCheckArrow.origin, activityCheckArrow.destination,
                                                        fwdDiffSymbolTable, true, sourceArrow.origin, diffEnv, toNewDeclaredInits);
            // DUBIOUS: we neglect toNewDeclaredInits, that contains diff initializations that are here before their declaration.
            if (diffInitBlock != null) {
//[8Oct2021] useless?                diffInitBlock.parallelControls = fwdParallelControlsOf(sourceArrow.destination.parallelControls, diffEnv);
                // Move a possible "return" instruction AFTER the reinitializations block:
                if (returnDangerBlock != null && sourceArrow.destination instanceof ExitBlock) {
                    Instruction previousInstr = returnDangerBlock.lastInstr();
                    if (previousInstr != null && previousInstr.tree.opCode() == ILLang.op_return) {
                        returnDangerBlock.removeInstr(previousInstr);
                        diffInitBlock.addInstrTl(previousInstr);
                        // Memo of the real origBlock of this return, for use in debugTGT.
                        realOrigBlockOfReturns =
                                new TapList<>(new TapPair<>(previousInstr, returnDangerBlock), realOrigBlockOfReturns);
                    }
                }
                // When next Block is Block0, Move the initializations inside it, after the declarations:
                // (happens in mode -nooptim spareinit)
                if (activityCheckArrow.origin instanceof EntryBlock) {
                    Block block0 = sourceArrow.destination;
                    Block fwdDiffBlock0 = diffEnv.fwdDiffBlocks.retrieve(block0);
                    TapList<Instruction> addedInstrs = TapList.reverse(diffInitBlock.instructions);
                    while (addedInstrs != null) {
                        fwdDiffBlock0.addInstrHdAfterDecls(addedInstrs.head);
                        addedInstrs = addedInstrs.tail;
                    }
                    diffInitBlock = null;
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("      Initializations inserted directly inside " + fwdDiffBlock0);
                    }
                }
                if (diffInitBlock != null) {
                    insertBlockAtOrig(fwdDiffArrow, diffInitBlock, FGConstants.NO_TEST, 0);
                }
            }
        }
    }

    /**
     * @return an empty new (tangent- or adjointforward-)differentiated counterpart for "block".
     * When "createAsHeader" is true, create the new Block as a HeaderBlock.
     * The SymbolTable, host Unit, and blocks accumulator of the new
     * forward-differentiated block are given by the diffEnv.diffCtxtFwd.
     */
    private Block createFwdDiffBlock(Block block, FlowGraphDifferentiationEnv diffEnv, boolean createAsHeader) {
        SymbolTable fwdDiffSymbolTable;
        // detect that the scope of a sub-SymbolTable extends to the end of the fwd sweep,
        // in which case the fwd diff ST can be set to be the bwd diff ST,
        // thus saving much push/pop of vanishing local vars.
        // If the scope cannot extend to the end of the fwd sweep... :
        if (diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE
            || (diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE
                && TapList.contains(diffEnv.middleSymbolTables, block.symbolTable))) {
            // ...then build a separate SymbolTable for the fwd sweep diff Block of block:
            fwdDiffSymbolTable =
                    block.symbolTable.smartCopy(diffEnv.diffCtxtFwd.listST, diffEnv.diffCtxtFwd.unit,
                            adEnv.diffCallGraph(), adEnv.copiedUnits, "FwdDiff of ");
            block.symbolTable.cleanCopySymbolDecls();
        } else {
            // ...else use the same SymbolTable for the fwd and bwd sweep diff Blocks of block:
            fwdDiffSymbolTable =
                    block.symbolTable.smartCopy(diffEnv.diffCtxt.listST, diffEnv.diffCtxtFwd.unit,
                            adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
            block.symbolTable.cleanCopySymbolDecls();
            TapPair<SymbolTable, SymbolTable> diffSTFwd = TapList.assq(block.symbolTable, diffEnv.diffCtxtFwd.listST);
            if (diffSTFwd != null && diffSTFwd.second != fwdDiffSymbolTable) {
                TapEnv.toolWarning(-1, "Bug in createFwdDiffBlock: fwdDiffSymbolTable already here");
            }
            if (TapList.assq(block.symbolTable, diffEnv.diffCtxtFwd.listST)==null) {
                diffEnv.diffCtxtFwd.listST.placdl(new TapPair<>(block.symbolTable, fwdDiffSymbolTable)) ;
            }
        }
        CallGraphDifferentiator.declareDiffSymbolTableIfNew(diffEnv.diffCtxtFwd.unit, fwdDiffSymbolTable);
        Block fwdDiffBlock;
        if (createAsHeader) {
            HeaderBlock fwdDiffHeaderBlock = buildFwdHeaderBlock(block.rank+"+", fwdDiffSymbolTable, block, diffEnv) ;
            fwdDiffHeaderBlock.declaresItsIterator = ((HeaderBlock) block).declaresItsIterator;
            fwdDiffHeaderBlock.setOrigCycleLabel(((HeaderBlock) block).origCycleLabel());
            // Necessary to detect exit loop indices that are not used later:
            detectUnusedExitLoopIndices(fwdDiffHeaderBlock, block);
            fwdDiffBlock = fwdDiffHeaderBlock ;
        } else {
            fwdDiffBlock = buildFwdBlock(block.rank+"+", fwdDiffSymbolTable, block, diffEnv) ;
        }
        // When "createAsHeader" is true, the following is a fallback that should not happen:
        // The diff header is a header, and therefore cannot be used to accomodate declarations
        if (block.symbolTable.declarationsBlock == block) {
            fwdDiffSymbolTable.declarationsBlock = fwdDiffBlock;
        }
        fwdDiffBlock.setOrigLabel(block.origLabel());
        return fwdDiffBlock;
    }

    /**
     * @return an empty new (adjoint- or adjointbackward-)differentiated counterpart for "block".
     * When "createAsHeader" is true, block is a HeaderBlock, so create the new Block as a HeaderBlock.
     * The SymbolTable, host Unit, and blocks accumulator of the new
     * differentiated block are given by the diffEnv.diffCtxt.
     */
    private Block createBwdDiffBlock(Block block, FlowGraphDifferentiationEnv diffEnv, boolean createAsHeader) {
        if (diffEnv.adDiffMode == DiffConstants.TANGENT_MODE) {
            return null;
        } else {
            SymbolTable diffSymbolTable =
                    block.symbolTable.smartCopy(diffEnv.diffCtxt.listST, diffEnv.diffCtxt.unit,
                            adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
            block.symbolTable.cleanCopySymbolDecls();
            CallGraphDifferentiator.declareDiffSymbolTableIfNew(diffEnv.diffCtxt.unit, diffSymbolTable);
            Block diffBlock;
            if (createAsHeader) {
                HeaderBlock diffHeaderBlock = buildBwdHeaderBlock(block.rank+"-", diffSymbolTable, block, diffEnv) ;
                diffHeaderBlock.declaresItsIterator = ((HeaderBlock) block).declaresItsIterator;
                diffBlock = diffHeaderBlock ;
            } else {
                diffBlock = buildBwdBlock(block.rank+"-", diffSymbolTable, block, diffEnv) ;
            }
            return diffBlock;
        }
    }

    private TapList<Instruction> bwdParallelControlsOf(TapList<Instruction> sourceParallelControls,
                                                       FlowGraphDifferentiationEnv diffEnv) {
        return Unit.getSetNewParallelControls(sourceParallelControls,
                diffEnv.bwdDiffBlocks, diffEnv.bwdParallelControls);
    }

    private TapList<Instruction> fwdParallelControlsOf(TapList<Instruction> sourceParallelControls,
                                                       FlowGraphDifferentiationEnv diffEnv) {
        return Unit.getSetNewParallelControls(sourceParallelControls,
                diffEnv.fwdDiffBlocks, diffEnv.fwdParallelControls);
    }

    /**
     * @return an empty new copy of the given "block".
     * Stores the link from "block" to the new copy into "copyEnv".
     * The SymbolTable, host Unit, and blocks accumulator of the new
     * copied block is given by the copyEnv.diffCtxt.
     */
    private Block createCopyBlock(Block block, FlowGraphCopyEnv copyEnv) {
        SymbolTable copyDiffSymbolTable =
                block.symbolTable.smartCopy(copyEnv.copyCtxt.listST, copyEnv.copyCtxt.unit,
                        adEnv.diffCallGraph(), adEnv.copiedUnits, "Primal of ");
        block.symbolTable.cleanCopySymbolDecls();
        CallGraphDifferentiator.declareDiffSymbolTableIfNew(copyEnv.copyCtxt.unit, copyDiffSymbolTable);
        Block copyBlock;
        if (block instanceof HeaderBlock) {
            copyBlock = new HeaderBlock(copyDiffSymbolTable, null, copyEnv.copyCtxt.toAllBlocks);
            ((HeaderBlock) copyBlock).declaresItsIterator = ((HeaderBlock) block).declaresItsIterator;
        } else {
            copyBlock = new BasicBlock(copyDiffSymbolTable, null, copyEnv.copyCtxt.toAllBlocks);
        }
        if (block.symbolTable.declarationsBlock == block) {
            copyDiffSymbolTable.declarationsBlock = copyBlock;
        }
        copyBlock.symbolicRk = block.rank + "cp";
        // TODO: manage parallelControls in the copied code too.
        // copyBlock.parallelControls = block.parallelControls ;
        return copyBlock;
    }

    private boolean allExitsTurn(TapList<FGArrow> exitArrows, TapList<FGArrow> turningArrows) {
        boolean allTurn = true;
        FGArrow exitArrow, nextExitArrow ;
        while (allTurn && exitArrows != null) {
            exitArrow = exitArrows.head ;
            // Refinement to consider that an arrow that goes to an empty
            // block and then to a turning arrow is indeed a turning arrow.
            nextExitArrow = (exitArrow.destination.containsNoCode() ? exitArrow.destination.flow().head : null) ;
            if (!TapList.contains(turningArrows, exitArrow)
                && !(nextExitArrow!=null && TapList.contains(turningArrows, nextExitArrow))) {
                allTurn = false;
            }
            exitArrows = exitArrows.tail;
        }
        return allTurn;
    }

    /**
     * Collects the SymbolTables that are used in the given "allBlocks" and that cannot be
     * differentiated with a single differentiated SymbolTable for both the FWD and BWD sweeps,
     * because these sweeps will be separated by other code that doesn't see this SymbolTable.
     */
    private TapList<SymbolTable> collectMiddleSymbolTables(TapList<FGArrow> turningArrows, TapList<Block> allBlocks) {
        TapList<SymbolTable> result = null;
        Block origBlock, destBlock;
        SymbolTable origSymbolTable, destSymbolTable, privateSymbolTable;
        TapList<FGArrow> flow;
        boolean isMiddle;
        privateSymbolTable = adEnv.curUnit().privateSymbolTable();
        while (allBlocks != null) {
            origBlock = allBlocks.head;
            origSymbolTable = origBlock.symbolTable;
            if (origSymbolTable != privateSymbolTable && !TapList.contains(result, origSymbolTable)) {
                flow = origBlock.flow();
                isMiddle = false;
                while (!isMiddle && flow != null) {
                    if (!TapList.contains(turningArrows, flow.head) ||
                            flow.head.exitsFromLoopAndLoopSymbolTable()) {
                        destBlock = flow.head.destination;
                        destSymbolTable = destBlock.symbolTable;
                        if (!destSymbolTable.nestedIn(origSymbolTable)) {
                            isMiddle = true;
                        }
                    }
                    flow = flow.tail;
                }
                if (isMiddle) {
                    result = new TapList<>(origSymbolTable, result);
                }
            }
            allBlocks = allBlocks.tail;
        }
        return result;
    }

    private void turnVanishingLoopIntoIf(Block loopBlock) {
        Instruction loopInstr = loopBlock.headInstr();
        Tree ctrlTree = loopInstr.tree.down(3);
        if (ctrlTree.opCode() == ILLang.op_while) {
            loopInstr.tree = ILUtils.build(ILLang.op_if, ctrlTree.cutChild(1));
        } else {
            loopInstr.tree = ILUtils.build(ILLang.op_none);
        }
    }

    private Tree buildTestFromDo(Tree doTree) {
        if (doTree.down(4).opCode() == ILLang.op_none
                || ILUtils.evalsToGEZero(doTree.down(4))) {
            return ILUtils.build(ILLang.op_le,
                    ILUtils.copy(doTree.down(1)),
                    ILUtils.copy(doTree.down(3)));
        } else if (ILUtils.evalsToLTZero(doTree.down(4))) {
            return ILUtils.build(ILLang.op_ge,
                    ILUtils.copy(doTree.down(1)),
                    ILUtils.copy(doTree.down(3)));
        } else {
            return ILUtils.build(ILLang.op_le,
                    ILUtils.mulTree(
                            ILUtils.subTree(
                                    ILUtils.copy(doTree.down(1)),
                                    ILUtils.copy(doTree.down(3))),
                            ILUtils.copy(doTree.down(4))),
                    ILUtils.build(ILLang.op_intCst, 0));
        }
    }

    private FGArrow buildCopiedArrow(FGArrow sourceArrow, Block copyOrig, Block copyDest,
                                     Block vanishingLoopHeader) {
        int newTest = sourceArrow.test;
        TapIntList newCases = sourceArrow.cases;
        boolean newInACycle = sourceArrow.inACycle;
        if (sourceArrow.origin == vanishingLoopHeader) {
            if (copyOrig.instructions.head.tree.opCode() == ILLang.op_if) {
                newTest = FGConstants.IF;
                if (sourceArrow.containsCase(FGConstants.EXIT)) {
                    newCases = new TapIntList(FGConstants.FALSE, null);
                } else {
                    newCases = new TapIntList(FGConstants.TRUE, null);
                }
            } else {
                newTest = FGConstants.NO_TEST;
                if (sourceArrow.containsCase(FGConstants.EXIT)) {
                    newTest = -999; // trick that means: don't copy this sourceArrow !!
                } else {
                    newCases = null;
                }
            }
        }
        FGArrow copyArrow = null;
        if (newTest != -999) {
            copyArrow = checkFreeThenSetNewFGArrow(copyOrig, newTest, newCases, copyDest, newInACycle);
            copyArrow.isAJumpIntoNextCase = sourceArrow.isAJumpIntoNextCase;
        }
        return copyArrow;
    }

    /**
     * Explore the source Flow Graph, backFlow from the given "arrow" and remaining
     * inside the given FlowGraphLevel "enclosingLevel", to reach
     * the nearest fwd and bwd dangling points. Builds and returns the TapPair
     * that summarizes all danglingFwd and BwdSwitchCase points found upstream "arrow".
     * Also fills when found the fwdBlockUS and bwdArrowsUS of "enclosingLevelDifferentiation",
     * which is the final differentiation result of the enclosinglevel.
     * Takes care of avoiding infinite loops using the boolean array "arrowVisited".
     *
     * @param arrow                         the starting point of this exploration.
     * @param fromInside                    true when the destination of arrow is inside enclosingLevel.
     * @param enclosingLevel                The FlowGraphLevel inside which this exploration must remain.
     * @param enclosingLevelDifferentiation The future differentiation of the enclosingLevel.
     * @param diffEnv                       The environment for differentiation, used here to know if we diff TANGENT or REVERSE.
     * @param subDifferentiations           The list of FlowGraphDifferentiation objects for all sons of the enclosingLevel.
     * @param arrowVisited                  a dejaVu information to avoid infinite looping during exploration.
     * @param nextFwdBlock                  The fwdBlockUS of the differentiation of the destination of arrow, if known.
     * @param nextBwdArrows                 The bwdArrowsUS of the differentiation of the destination of arrow, if known.
     * @return The pairs of the danglingFwd's and BwdSwitchCase's arriving upstream the given "arrow".
     */
    private TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> exploreConvergingBackFlow(FGArrow arrow, boolean fromInside,
                                                                                        FlowGraphLevel enclosingLevel,
                                                                                        FlowGraphDifferentiation enclosingLevelDifferentiation,
                                                                                        FlowGraphDifferentiationEnv diffEnv,
                                                                                        TapList<FlowGraphDifferentiation> subDifferentiations,
                                                                                        boolean[] arrowVisited, Block nextFwdBlock,
                                                                                        TapList<FGArrow> nextBwdArrows) {
        TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> hdManyFwdBwds = new TapList<>(null, null);
        ecbfRec(new TapList<>(arrow, null), fromInside,
                enclosingLevel, enclosingLevelDifferentiation, diffEnv, subDifferentiations,
                arrowVisited, new TapList<>(null, hdManyFwdBwds), null,
                arrow, arrow, nextFwdBlock, nextBwdArrows, null);
        return hdManyFwdBwds.tail;
    }

    /**
     * Recursive phase of exploreConvergingBackFlow().
     * Important assumption: hasBwd =&gt; hasFwd, i.e. when a level has a Bwd, then
     * it is also assumed to have a Fwd. In this case, even if the Fwd is an empty
     * Block, it will be present in the Fwd sweep and we can connect to it.
     *
     * @param convergingArrows              The list of FGArrow's from which exploration must run (upstream).
     * @param fromInside                    true when the common destination of convergingArrows is inside enclosingLevel.
     * @param enclosingLevel                The FlowGraphLevel inside which this exploration must remain.
     * @param enclosingLevelDifferentiation The future differentiation of the enclosingLevel. Upon exit from all
     *                                      explorations inside enclosingLevel, it will contain its final fwdBlockUS and bwdArrowsUS.
     * @param diffEnv                       The environment for differentiation, used here to know if we diff TANGENT or REVERSE.
     * @param subDifferentiations           The list of FlowGraphDifferentiation objects for all sons of the enclosingLevel.
     * @param arrowVisited                  Boolean array to avoid looping in the Flow Graph.
     * @param toTlManyFwdBwds               the To-Tail of the accumulator of oneFwdBwd pairs.
     * @param currentOneFwdBwd              the current oneFwdBwd pair that must receive the coming BwdSwitchCase's
     * @param tailArrow                     the source FGArrow that goes to the source of nextFwdBlock.
     *                                      It is used to decide whether the fwd arrow to nextFwdBlock must be a cycle.
     * @param nextFwdBlock                  The closest FlowGraphLevel's FwdBlock downstream the convergingArrows, if any.
     * @param nextBwdArrows                 The closest FlowGraphLevel's BwdArrows downstream the
     *                                      convergingArrows, if any.
     */
    private void ecbfRec(TapList<FGArrow> convergingArrows, boolean fromInside,
                         FlowGraphLevel enclosingLevel, FlowGraphDifferentiation enclosingLevelDifferentiation,
                         FlowGraphDifferentiationEnv diffEnv, TapList<FlowGraphDifferentiation> subDifferentiations,
                         boolean[] arrowVisited,
                         TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> toTlManyFwdBwds,
                         TapPair<FGArrow, TapList<BwdSwitchCase>> currentOneFwdBwd,
                         FGArrow tailArrow, FGArrow srcArrow,
                         Block nextFwdBlock,
                         TapList<FGArrow> nextBwdArrows,
                         TapList<Block> sourcePath) {
        // [llh] TODO: quand sur le chemin qui va en remontant depuis une convergingArrow de depart jusqu'au
        // premier Block qui a un fwd non vide, on trouve une annotation "isAJumpIntoNextCase",
        // alors sur la dangling FGArrow forward qui est cree depuis ce fwd non vide,
        // il faut reporter cette annotation "isAJumpIntoNextCase" a true.
        FGArrow arrow, curTailArrow;
        FlowGraphLevel subLevel;
        FlowGraphDifferentiation subLevelDifferentiation;
        if (adEnv.traceCurDifferentiation) {
            TapEnv.incrTraceIndent(2);
        }
        TapPair<FGArrow, TapList<BwdSwitchCase>> subLevelOneFwdBwd;
        while (convergingArrows != null) {
            arrow = convergingArrows.head;
            curTailArrow = tailArrow != null ? tailArrow : arrow;
            if (adEnv.traceCurDifferentiation) {
                TapEnv.indentprintlnOnTrace("   explore through " + arrow + " (visited:" + arrowVisited[arrow.rank] + ')' + sourcePath);
            }
            if (!arrowVisited[arrow.rank]) {
                arrowVisited[arrow.rank] = true;
                subLevel = null;
                subLevelDifferentiation = null;
                TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds = null;
                boolean subLevelIsLive = false;

                // Find the subLevel that contains this arrow's origin in the enclosingLevel:
                // If the arrow goes out then back in, this arrow is considered coming from outside.
                if (!fromInside || enclosingLevel.containsArrowFlow(arrow)) {
                    subLevel = enclosingLevel.getRepresentantInLevel(arrow.origin);
                }
                // Find the differentiation of this subLevel, and more specifically for this arrow:
                if (subLevel != null) {
                    subLevelDifferentiation = getSubLevelDifferentiation(subLevel, enclosingLevel, subDifferentiations);
                    assert subLevelDifferentiation != null;
                    manyFwdBwds = subLevelDifferentiation.getFwdBwdsOfExitArrow(arrow);
                    subLevelIsLive = oneFirstIsNonNull(manyFwdBwds);
                }

                subLevelOneFwdBwd = currentOneFwdBwd;
                // Start explore cases according to the nature of the differentiated arrow origin:
                if (subLevel == null && enclosingLevelDifferentiation.specialTopFwdArrow != null) {
                    // If the arrow comes from outside AND the enclosing level has internal cycles:
                    // we must simulate the presence of a special Block that gathers all entries
                    // into the enclosing level. Fwd and Bwd of this special Block are precomputed.
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.indentprintlnOnTrace("    jumping outside of " + enclosingLevel + " through special Fwd:" +
                                enclosingLevelDifferentiation.specialTopFwdArrow + " and Bwd:" +
                                enclosingLevelDifferentiation.specialTopBwdArrow);
                    }
                    if (!enclosingLevelDifferentiation.specialTopUsed) {
                        // Do the following only once, even if there are several entering arrows:
                        enclosingLevelDifferentiation.specialTopUsed = true;
                        FGArrow specialTopFwdArrow = enclosingLevelDifferentiation.specialTopFwdArrow;
                        FGArrow specialTopBwdArrow = enclosingLevelDifferentiation.specialTopBwdArrow;
                        if (subLevelOneFwdBwd == null) {
                            subLevelOneFwdBwd = new TapPair<>(null, null);
                            toTlManyFwdBwds.tail = toTlManyFwdBwds.tail.placdl(subLevelOneFwdBwd);
                        }
                        if (nextFwdBlock != null) {
                            checkFreeThenRedirectDestination(specialTopFwdArrow, nextFwdBlock, false);
                        } else {
                            subLevelOneFwdBwd.first = specialTopFwdArrow;
                        }
                        if (specialTopBwdArrow != null) {
                            subLevelOneFwdBwd.second = TapList.addLast(subLevelOneFwdBwd.second,
                                    // Warning: make sure this BwdSwitchCase is built only once,
                                    // because we test == equality on them later, to count the
                                    // number of Push-Pop alternatives.
                                    new BwdSwitchCase(specialTopFwdArrow, specialTopBwdArrow.origin,
                                            // sourcePath appears to hold many duplicates: [TODO] fix that!
                                            new TapList<>(enclosingLevel.entryBlock, new TapList<>(arrow.origin, sourcePath)),
                                            srcArrow.destination));
                            nextBwdArrows = new TapList<>(specialTopBwdArrow, null);
                        }
                        enclosingLevelDifferentiation.accumulateFwdBwdUS(specialTopFwdArrow.origin, nextBwdArrows);
                    }
                } else if (subLevel == null) {
                    // ...else if the arrow comes from outside, stop exploration at this subLevel.
                    // Store (nextFwdBlock;nextBwdArrows) as the enclosing level's Fwd and Bwd upstream.
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.indentprintlnOnTrace("    jumping outside of " + enclosingLevel +
                                " setting its connections Fwd:" + nextFwdBlock + " Bwd:" + nextBwdArrows);
                    }
                    enclosingLevelDifferentiation.accumulateFwdBwdUS(nextFwdBlock, nextBwdArrows);
                    // by algorithm construction, this collects no danglingFwd's nor BwdSwitchCase's.
                } else if (keepEmptyDiffBlocks || subLevelIsLive) {
                    // ...else if the arrow comes from a subLevel that is live (active or not).
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.indentprintlnOnTrace("    reaching differentiation of " + subLevel +
                                " with manyFwdBwds:" + manyFwdBwds +
                                " and nextFwdBlock:" + nextFwdBlock);
                    }
                    while (manyFwdBwds != null) {
                        FGArrow danglingFwd = manyFwdBwds.head.first;
                        TapList<BwdSwitchCase> subLevelBwdSwitchCases = manyFwdBwds.head.second;
                        // We can first collect or connect this subLevel's danglingFwds:
                        if (subLevelOneFwdBwd == null) {
                            subLevelOneFwdBwd = new TapPair<>(null, null);
                            toTlManyFwdBwds.tail = toTlManyFwdBwds.tail.placdl(subLevelOneFwdBwd);
                        }
                        if (nextFwdBlock != null) {
                            if (danglingFwd.destination == null) {
                                checkFreeThenRedirectDestination(danglingFwd, nextFwdBlock, curTailArrow.inACycle);
                            }
                        } else {
                            subLevelOneFwdBwd.first = danglingFwd;
                        }
                        // then according to subLevel being active or not:
                        if (keepEmptyDiffBlocks && diffEnv.adDiffMode != DiffConstants.TANGENT_MODE && !adEnv.curUnitIsContext
                                || subLevelBwdSwitchCases != null) {
                            // if subLevel is active, collect its BwdSwitchCase's and stop this exploration branch:
                            if (adEnv.traceCurDifferentiation) {
                                TapEnv.indentprintlnOnTrace("    " + subLevel + " is active with BwdSwitchCases " + subLevelBwdSwitchCases);
                            }
                            BwdSwitchCase subLevelBwdSwitchCase;
                            while (subLevelBwdSwitchCases != null) {
                                subLevelBwdSwitchCase = subLevelBwdSwitchCases.head;
                                subLevelBwdSwitchCase.sourceOrigPath =
                                        TapList.append(subLevelBwdSwitchCase.sourceOrigPath, sourcePath);
                                subLevelOneFwdBwd.second = TapList.addLast(subLevelOneFwdBwd.second, subLevelBwdSwitchCase);
                                subLevelBwdSwitchCases = subLevelBwdSwitchCases.tail;
                            }
                        } else {
                            // else continue exploration backwards from this subLevel:
                            ecbfRec(subLevel.entryArrows, true, enclosingLevel, enclosingLevelDifferentiation, diffEnv,
                                    subDifferentiations, arrowVisited,
                                    toTlManyFwdBwds, subLevelOneFwdBwd,
                                    null, srcArrow, subLevelDifferentiation.fwdBlockUS, nextBwdArrows,
                                    new TapList<>(subLevel.entryBlock, new TapList<>(arrow.origin, sourcePath)));
                        }
                        manyFwdBwds = manyFwdBwds.tail;
                    }
                } else {
                    // ...else the arrow flows from a subLevel that is not live, and therefore not active.
                    // Ignore subLevel, and continue exploration backwards from this subLevel:
                    ecbfRec(subLevel.entryArrows, true, enclosingLevel, enclosingLevelDifferentiation, diffEnv,
                            subDifferentiations, arrowVisited,
                            toTlManyFwdBwds, subLevelOneFwdBwd,
                            curTailArrow, srcArrow, nextFwdBlock, nextBwdArrows,
                            new TapList<>(subLevel.entryBlock, new TapList<>(arrow.origin, sourcePath)));
                }
            }
            convergingArrows = convergingArrows.tail;
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(2);
        }
    }

    private FlowGraphDifferentiation getSubLevelDifferentiation(FlowGraphLevel subLevel,
                                                                FlowGraphLevel enclosingLevel,
                                                                TapList<FlowGraphDifferentiation> subDifferentiations) {
        FlowGraphDifferentiation result = null;
        TapList<FlowGraphLevel> subLevels = new TapList<>(enclosingLevel.entryPoint, enclosingLevel.contents);
        while (subLevels != null && result == null) {
            if (subLevels.head == subLevel) {
                result = subDifferentiations.head;
            }
            subDifferentiations = subDifferentiations.tail;
            subLevels = subLevels.tail;
        }
        return result;
    }

    private boolean sameUniqueBwdDestAndSameActivitySwitches(
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> arrowsFwdBwds1,
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> arrowsFwdBwds2) {
        if (arrowsFwdBwds1.tail == null && arrowsFwdBwds2.tail == null) {
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds1 = arrowsFwdBwds1.head.second;
            TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds2 = arrowsFwdBwds2.head.second;
            if (manyFwdBwds1.tail == null && manyFwdBwds2.tail == null) {
                TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd1 = manyFwdBwds1.head;
                TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd2 = manyFwdBwds2.head;
                if (oneFwdBwd1 != null && oneFwdBwd2 != null) {
                    TapList<BwdSwitchCase> bwdSwitchCases1 = oneFwdBwd1.second;
                    TapList<BwdSwitchCase> bwdSwitchCases2 = oneFwdBwd2.second;
                    if (bwdSwitchCases1 != null && bwdSwitchCases1.tail == null &&
                            bwdSwitchCases2 != null && bwdSwitchCases2.tail == null) {
                        return bwdSwitchCases1.head.bwdBlock == bwdSwitchCases2.head.bwdBlock
                                && sameActivitySwitches(bwdSwitchCases1.head.sourceOrigPath.head,
                                arrowsFwdBwds1.head.first,
                                arrowsFwdBwds2.head.first);
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Special case of connectFwdBwd() for the connections of the sort "turn:connect forward to reverse".
     */
    private void mapConnectFwdBwd(TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> arrowsFwdBwdsUpstream,
                                  FlowGraphDifferentiationEnv diffEnv,
                                  FlowGraphLevel enclosingLevel) {
        TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>> arrowFwdBwds;
        TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
        TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
        PushPopNode pushPopTree;
        while (arrowsFwdBwdsUpstream != null) {
            arrowFwdBwds = arrowsFwdBwdsUpstream.head;
            manyFwdBwds = arrowFwdBwds.second;
            while (manyFwdBwds != null) {
                oneFwdBwd = manyFwdBwds.head;
                if (oneFwdBwd != null) {
                    // arrowFwdBwds.second may be null when the arrow has been neglected because
                    // it exits from a vanishingLoopHeader (see also "headerBlockPlain").
                    pushPopTree = buildFwdBwdTreeFromOneFwdBwd(
                            oneFwdBwd.first, oneFwdBwd.second);
                    connectFwdBwd(pushPopTree, null, null,
                            arrowFwdBwds.first.destination, null, diffEnv, enclosingLevel);
                }
                manyFwdBwds = manyFwdBwds.tail;
            }
            arrowsFwdBwdsUpstream = arrowsFwdBwdsUpstream.tail;
        }
    }

    /**
     * Connects a set of upstream locations (i.e. some exits of the fwd sweep and
     * the matching entries of the bwd sweep, both described by "oneFwdBwd"), with a given
     * downstream location (i.e. a "blockFwdDownstream" and its matching
     * "arrowsBwdDownstream"). When non-null (e.g. when "blockFwdDownstream" and
     * "arrowsBwdDownstream" are given non-null, but this can also happen deeper inside
     * this method), this downstream location defines a "center"
     * that must be placed between the fwd and the bwd sweeps: for every Fwd sweep FGArrow that reaches
     * "blockFwdDownstream", we must PUSH a different control number, and on the way back
     * just after "arrowsBwdDownstream" we must POP this control number and switch
     * according to it to the correct Bwd sweep destination Block.
     * <p>
     * This builds :
     * <pre>
     * {@code
     * push(&lt;0,1,...&gt;) ;
     * fwd sweep of &lt;destBlock&gt; ;
     * -----------------
     * bwd sweep of &lt;destBlock&gt; ;
     * switch(pop(&lt;control&gt;)) ..}
     * </pre>
     * In the special case where only "blockFwdDownstream" is null, then the fwd sweep
     * is left unconnected, but possibly with control PUSH'es inserted, and the
     * "arrowsBwdDownstream" are connected to the bwd sweep, with the control
     * mechanism that POP's the control and uses it.<br>
     * In the case where both "blockFwdDownstream" and "arrowsBwdDownstream" are null,
     * the fwd sweep is connected directly to the bwd sweep, very often avoiding control storage.
     * This method unifies former buildBinaryBranch() and connectForwardToReverse().
     *
     * @param pushPopTree         A pair of (1) the list of the dangling Fwd FGArrows that must be
     *                            connected to the "blockFwdDownstream" and (2) the list of BwdSwitchCase's that
     *                            specify each matching upstream ("end of Fwd Sweep", "beginning of bwd sweep",
     *                            "end of source sweep" and "sourceDest") that reach the given downstream
     *                            triplet ("blockFwdDownstream", "arrowsBwdDownstream", "sourceDest").
     * @param blockFwdDownstream  The Block where all arriving branches of the Fwd sweep must arrive.
     * @param arrowsBwdDownstream The FGArrow's from which the given branches of the Bwd sweep
     *                            must leave, possibly starting with a switch on the control number.
     * @param sourceDest          When given, the source Block that corresponds to the downstream location.
     * @param doIndexToSave       When non-null, specifies that this tree is the index
     *                            of a WILD DO loop that must be PUSH/POP'ed, and the PUSH/POPs must be placed by
     *                            the present method on the connecting FGArrows (because we can't place them on
     *                            the "blockFwdDownstream").
     * @param diffEnv             The environment for the differentiation of the enclosing context.
     * @param enclosingLevel      The FlowGraphLevel that immediately encloses the pieces
     *                            of code that are going to be connected.
     */
    private void connectFwdBwd(PushPopNode pushPopTree, Block blockFwdDownstream,
                               TapList<FGArrow> arrowsBwdDownstream, Block sourceDest,
                               Tree doIndexToSave, FlowGraphDifferentiationEnv diffEnv, FlowGraphLevel enclosingLevel) {
        if (pushPopTree != null) {
            if (adEnv.traceCurDifferentiation) {
                if (blockFwdDownstream == null && arrowsBwdDownstream == null) {
                    TapEnv.indentprintlnOnTrace("*> This is a TURN point (connects FWD to BWD)");
                }
            }
            DownstreamConnector center =
                    new DownstreamConnector(blockFwdDownstream, sourceDest, arrowsBwdDownstream, null);
            // In the special case where center.entry==null and center.exits!=null,
            // which occurs in differentiateNaturalLoop(), this PUSH/POP pair
            // comes from an FGArrow which will be used again as a PUSH/POP pair later.
            // In this case, we must not duplicate the restoration and declaration blocks.
            boolean deferDeclAndRestore = arrowsBwdDownstream != null && blockFwdDownstream == null;
            // Also defer restoration at the exit point of a MULTITHREAD_REGION: restoration is done later.
            if (enclosingLevel.levelKind==DiffConstants.MULTITHREAD_REGION
                && enclosingLevel.entryBlock.headInstr().tree.opCode()==ILLang.op_parallelRegion) {
                deferDeclAndRestore = true ;
            }
            // Turn the raw pushPopTree into a binary tree (for the nested if-then-else's):
            if (adEnv.traceCurDifferentiation) {
                TapEnv.indentprintOnTrace("*> before turn binary : ");
                TapEnv.incrTraceIndent(24);
                pushPopTree.dump(new TapList<>(null, null));
                TapEnv.printlnOnTrace();
                TapEnv.decrTraceIndent(24);
            }
            turnPushPopTreeBinary(pushPopTree);
//             if (pushPopTree.uniqueBwdDest()) {
//                 pushPopTree.numberOfCases = 1 ; //Should be profitable, but doesn't work yet!
//             }
            if (adEnv.traceCurDifferentiation) {
                TapEnv.indentprintOnTrace("*> Connect pushPopTree: ");
                TapEnv.incrTraceIndent(24);
                pushPopTree.dump(new TapList<>(null, null));
                TapEnv.printlnOnTrace(" which has " + pushPopTree.numberOfCases + " cases,");
                TapEnv.decrTraceIndent(24);
                TapEnv.indentprintlnOnTrace("   "+(center.isATurn?"TURNING":"")+" THROUGH "+center);
                TapEnv.incrTraceIndent(2);
            }
            // Finally, connect the Blocks:
            connectPushPopTree(pushPopTree, center, deferDeclAndRestore, enclosingLevel, -1, 0, diffEnv,
                    null, -1, doIndexToSave, doIndexToSave == null ? -1 : TapEnv.getNewPushPopNumber());
            if (adEnv.traceCurDifferentiation) {
                TapEnv.decrTraceIndent(2);
            }
        }
    }

    private PushPopNode buildFwdBwdTreeFromOneFwdBwd(FGArrow danglingFwd,
                                                     TapList<BwdSwitchCase> bwdSwitchCases) {
        PushPopNode result = new PushPopNode(null, null, null, curDiffUnit().publicSymbolTable(), null, null, null);
        insertIntoPushPopTree(result, danglingFwd, bwdSwitchCases);
        return result;
    }

    private PushPopNode buildFwdBwdTreeFromExitFwdBwd(
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> arrowsFwdBwds) {
        // [llh] TODO Possible improvement: maybe the correct scope to place at the root of
        // the pushPopTree is not curDiffUnit().publicSymbolTable(), but rather the nested SymbolTable
        // of the bwd Block that will pop the control and switch according to it.
        PushPopNode result = new PushPopNode(null, null, null, curDiffUnit().publicSymbolTable(), null, null, null);
        TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> manyFwdBwds;
        TapPair<FGArrow, TapList<BwdSwitchCase>> oneFwdBwd;
        while (arrowsFwdBwds != null) {
            manyFwdBwds = arrowsFwdBwds.head.second;
            while (manyFwdBwds != null) {
                oneFwdBwd = manyFwdBwds.head;
                insertIntoPushPopTree(result,
                        oneFwdBwd.first, oneFwdBwd.second);
                manyFwdBwds = manyFwdBwds.tail;
            }
            arrowsFwdBwds = arrowsFwdBwds.tail;
        }
        return result;
    }

    private void insertIntoPushPopTree(PushPopNode pushPopNode,
                                       FGArrow danglingFwd, TapList<BwdSwitchCase> bwdSwitchCases) {
        SymbolTable commonRoot = pushPopNode.bwdScope;
        BwdSwitchCase oneBwdSwitchCase;
        TapList<SymbolTable> bwdStList;
        // insert the leaves into the pushPop tree, i.e.
        // insert each individual pair of pushArrow and bwdBlock:
        while (bwdSwitchCases != null) {
            PushPopNode currentNode = pushPopNode;
            oneBwdSwitchCase = bwdSwitchCases.head;
            // Insert nodes in the pushPop tree that go gradually from the commonSTBwd scope down
            // to this leaf's bwdSymbolTable. Also, if the danglingFwd arrow is not the fwd origin
            // of the current "oneBwdSwitchCase", insert it in the pushPop tree at the correct
            // scope level so that it will be used to connect to the sequel of the fwd sweep.
            if (oneBwdSwitchCase.bwdBlock.symbolTable == commonRoot) {
                if (danglingFwd != null && oneBwdSwitchCase.fwdArrow != danglingFwd) {
                    currentNode = getInsertDanglingFwdInPushPopTree(danglingFwd, currentNode);
                }
            }
            bwdStList = oneBwdSwitchCase.bwdBlock.symbolTable.allSymbolTableRoots(commonRoot);
            while (bwdStList != null) {
                currentNode = getInsertScopeNodeInPushPopTree(bwdStList.head, currentNode);
                if (danglingFwd != null && oneBwdSwitchCase.fwdArrow != danglingFwd
                        && bwdStList.head == oneBwdSwitchCase.bwdBlock.symbolTable) {
                    currentNode = getInsertDanglingFwdInPushPopTree(danglingFwd, currentNode);
                }
                bwdStList = bwdStList.tail;
            }
            // finally insert the pushPop leaf, taking care of putting a single node per bwd Block.
            insertOneBwdSwitchCaseInPushPopTree(oneBwdSwitchCase, currentNode);
            bwdSwitchCases = bwdSwitchCases.tail;
        }
    }

    /**
     * Looks for a direct child of the given pushPopTree that opens the scope
     * for the given "newScope". @return it if found.
     * Otherwise if not found, creates it and inserts it as a new child of pushPopTree
     * and returns it.
     */
    private PushPopNode getInsertScopeNodeInPushPopTree(SymbolTable newScope,
                                                        PushPopNode pushPopTree) {
        TapList<PushPopNode> subTrees = pushPopTree.subNodes;
        PushPopNode foundNode = null;
        while (subTrees != null && foundNode == null) {
            if (subTrees.head.bwdScope == newScope) {
                foundNode = subTrees.head;
            }
            subTrees = subTrees.tail;
        }
        if (foundNode == null) {
            foundNode = new PushPopNode(null, null, null, newScope, null, null, null);
            pushPopTree.subNodes = new TapList<>(foundNode, pushPopTree.subNodes);
        }
        return foundNode;
    }

    private PushPopNode getInsertDanglingFwdInPushPopTree(FGArrow danglingFwd,
                                                          PushPopNode pushPopTree) {
        TapList<PushPopNode> subTrees = pushPopTree.subNodes;
        PushPopNode foundNode = null;
        while (subTrees != null && foundNode == null) {
            if (TapList.contains(subTrees.head.fwdArrows, danglingFwd)) {
                foundNode = subTrees.head;
            }
            subTrees = subTrees.tail;
        }
        if (foundNode == null) {
            foundNode = new PushPopNode(new TapList<>(danglingFwd, null),
                    null, null, null, null, null, null);
            pushPopTree.subNodes = new TapList<>(foundNode, pushPopTree.subNodes);
        }
        return foundNode;
    }


    /**
     * Inserts the pushPop leaf defined by "oneBwdSwitchCase"
     * as a new child of PushPopNode "pushPopTree".
     * Takes care of having a single child for each bwd Block.
     */
    private void insertOneBwdSwitchCaseInPushPopTree(BwdSwitchCase oneBwdSwitchCase, PushPopNode pushPopTree) {
        TapList<PushPopNode> subTrees = pushPopTree.subNodes;
        PushPopNode foundNode = null;
        Block bwdBlock;
        while (subTrees != null && foundNode == null) {
            if (subTrees.head.bwdBlock == oneBwdSwitchCase.bwdBlock) {
                foundNode = subTrees.head;
            }
            subTrees = subTrees.tail;
        }
        if (foundNode == null) {
            bwdBlock = oneBwdSwitchCase.bwdBlock;
            foundNode =
                    new PushPopNode(null, bwdBlock, oneBwdSwitchCase, bwdBlock.symbolTable, oneBwdSwitchCase.sourceOrigPath, null, null);
            pushPopTree.subNodes = new TapList<>(foundNode, pushPopTree.subNodes);
        }
        if (!TapList.contains(foundNode.fwdArrows, oneBwdSwitchCase.fwdArrow)) {
            foundNode.fwdArrows =
                    new TapList<>(oneBwdSwitchCase.fwdArrow, foundNode.fwdArrows);
        }
        if (foundNode.sourceDest == null) {
            foundNode.sourceDest = oneBwdSwitchCase.sourceDest;
        }
    }

    private void turnPushPopTreeBinary(PushPopNode pushPopTree) {
        TapList<PushPopNode> subNodes = pushPopTree.subNodes;
        while (subNodes != null) {
            turnPushPopTreeBinary(subNodes.head);
            subNodes = subNodes.tail;
        }
        pushPopTree.subNodes = turnPushPopSubNodesBinary(pushPopTree.subNodes, pushPopTree.bwdScope);
        pushPopTree.computeNumberOfCases();
    }

    private TapList<PushPopNode> turnPushPopSubNodesBinary(
            TapList<PushPopNode> subNodes,
            SymbolTable scope) {
        int len = TapList.length(subNodes);
        if (len <= 2) {
            return subNodes;
        } else {
            // Cut the subNodes list in two, then recurse, and return a binary tree:
            TapList<PushPopNode> inSubNodes = subNodes;
            for (int i = len / 2; i > 1; --i) {
                inSubNodes = inSubNodes.tail;
            }
            TapList<PushPopNode> secondHalf = inSubNodes.tail;
            inSubNodes.tail = null;
            PushPopNode node1, node2;
            if (TapList.length(subNodes) == 1) {
                node1 = subNodes.head;
            } else {
                node1 = new PushPopNode(null, null, null, scope, null, null, turnPushPopSubNodesBinary(subNodes, scope));
            }
            node1.computeNumberOfCases();
            if (TapList.length(secondHalf) == 1) {
                node2 = secondHalf.head;
            } else {
                node2 = new PushPopNode(null, null, null, scope, null, null, turnPushPopSubNodesBinary(secondHalf, scope));
            }
            node2.computeNumberOfCases();
            return new TapList<>(node1, new TapList<>(node2, null));
        }
    }

    /**
     * Build all the Flow Graph connections following the given pushPopTree, placing the
     * given center between the exit of the fwd sweep and the entry into the bwd sweep.
     */
    private void connectPushPopTree(PushPopNode pushPopTree, DownstreamConnector center,
                                    boolean deferDeclAndRestore, FlowGraphLevel enclosingLevel, int offset,
                                    int totalNumberOfCases, FlowGraphDifferentiationEnv diffEnv,
                                    RefDescriptor branchSaveVarRefDescriptor, int ppLabel,
                                    Tree doIndexToSave, int ppLabelForIndex) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.indentprintOnTrace("[Knct:");
            TapEnv.incrTraceIndent(6);
            pushPopTree.dump(new TapList<>(null, null));
            TapEnv.decrTraceIndent(6);
            TapEnv.printlnOnTrace();
            TapEnv.indentprintlnOnTrace(" THROUGH " + center + " ; Save do index:" + doIndexToSave);
            TapEnv.incrTraceIndent(1);
        }
        Block sourceDest = center.sourceBlock;
        // Connect the dangling fwd arrows of this PushPopNode to the entry
        // of the current DownstreamConnector:
        TapList<FGArrow> fwdArrows = pushPopTree.fwdArrows;
        if (fwdArrows != null) {
            if (center.entry == null || (naturalLoopHeader!=null && pushPopTree.sourceDest!=naturalLoopHeader)) {
                if (center.exits == null) {
                    // Caution. special case (cf exit arrows in natural loop):
                    // when the center is non-empty, but its entry is hidden
                    // (i.e center.entry==null && center.exits!=null)
                    // then we don't want to connect the fwdArrows to the center !
                    center.exits = fwdArrows;
                }
            } else {
                // If this node has dangling fwd arrows and propagation provides a center.entry destination,
                // then connect them and propagate nothing upwards.
                // Also, take care of Push/Popping possible 'Do i" loop index (case of a DO WILD_LOOP).
                FGArrow fwdArrow;
                Block pushIndexBlock, popIndexBlock;
                while (fwdArrows != null) {
                    fwdArrow = fwdArrows.head;
                    if (fwdArrow.destination == null) {
                        checkFreeThenRedirectDestination(fwdArrow, center.entry, false);
                    }
                    // Insert PUSH of WILD_LOOP's DO indices:
                    if (doIndexToSave != null) {
                        pushIndexBlock = buildFwdBlock("+PushIndex", center.entry.symbolTable, center.sourceBlock, diffEnv);
                        pushIndexBlock.addInstrHdAfterDecls(RefDescriptor.makePush(curFwdDiffUnit(), ILUtils.copy(doIndexToSave),
                                sourceDest.symbolTable, center.entry.symbolTable, ppLabelForIndex));
                        insertBlockAtOrig(fwdArrow, pushIndexBlock, FGConstants.NO_TEST, 0);
                    }
                    fwdArrows = fwdArrows.tail;
                }
                //center.entry = null ;//bad for F77:lha67
                // Insert POPs of WILD_LOOP's DO indices:
                // Compute the common scope of all center.exits:
                TapList<FGArrow> centerExits = center.exits;
                SymbolTable minST = null, newST;
                while (centerExits != null) {
                    newST = centerExits.head.origin.symbolTable;
                    minST = minST == null ? newST : minST.getCommonRoot(newST);
                    centerExits = centerExits.tail;
                }
                center.bwdScope = minST;
                if (doIndexToSave != null) {
                    popIndexBlock = buildBwdBlock("-PopIndex", center.bwdScope, center.sourceBlock, diffEnv);
                    popIndexBlock.addInstrTl(RefDescriptor.makePop(curDiffUnit(), ILUtils.copy(doIndexToSave),
                            sourceDest.symbolTable, center.bwdScope,
                            ppLabelForIndex, toBranchVariable));
                    mapCheckFreeThenRedirectDestination(center.exits, popIndexBlock, false);
                    center.exits = new TapList<>(checkFreeThenSetNewFGArrow(popIndexBlock, FGConstants.NO_TEST, 0, null, false), null);
                }

            }
        }

        // Compute the common scope of all center.exits:
        TapList<FGArrow> centerExits = center.exits;
        SymbolTable minST = null, newST;
        while (centerExits != null) {
            newST = centerExits.head.origin.symbolTable;
            minST = minST == null ? newST : minST.getCommonRoot(newST);
            centerExits = centerExits.tail;
        }
        center.bwdScope = minST;

        // Connect the exits of the current DownstreamConnector to the the bwd declaration Block
        // of the new scope entered by this PushPopNode:
        if (!deferDeclAndRestore && center.bwdScope != null && pushPopTree.bwdScope != null
                && !center.bwdScope.nestedIn(pushPopTree.bwdScope)) {
            // if we are entering into a new scope of the reverse sweep, we place
            // here the declaration block of the SymbolTable of this new scope:
            Block localDeclBlock = pushPopTree.bwdScope.declarationsBlock ;
            if (localDeclBlock != null) {
                if (localDeclBlock.flow() == null) {
                    localDeclBlock.symbolicRk = "declBlockOf@"+Integer.toHexString(pushPopTree.bwdScope.hashCode()) ;
                    diffEnv.diffCtxt.toAllBlocks.placdl(localDeclBlock) ;
                    center.putBlockAtTail(localDeclBlock);
                    center.putArrowAtTail(FGConstants.NO_TEST, 0);
                } else {
                    Instruction headInstr = sourceDest == null ? null : sourceDest.headInstr();
                    TapEnv.fileWarning(TapEnv.MSG_WARN, headInstr == null ? null : headInstr.tree, "(AD22) Irreducible exits from the same scope in procedure " + adEnv.curUnit().name());
                }
            }
            center.bwdScope = pushPopTree.bwdScope;
        }
        if (pushPopTree.subNodes == null) {
            if (pushPopTree.fwdArrows != null && pushPopTree.bwdBlock != null) {
                // We are on a leaf of the PushPopTree that corresponds to a PUSH/POP pair.
                TapList<Block> sourceOrigPath = pushPopTree.sourceOrigPath;
                Block sourceOrig = sourceOrigPath.head;
                Block sourceOrigForSaveLocals = sourceOrig ;
                FlowGraphLevel upstreamLevel = enclosingLevel.getRepresentantInLevel(sourceOrig);
                // Exit arrows from a NATURAL_LOOP may come from a scope deep inside,
                // whose locals have been saved already, so replace sourceOrig with the
                // loop header, to restrict locals restoration to the loop's scope or higher:
                if (upstreamLevel.levelKind==DiffConstants.NATURAL_LOOP) {
                    sourceOrigForSaveLocals = upstreamLevel.entryBlock ;
                }
                int lenTBRVec = sourceOrig.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                int lenTBRVecOnDiffPtr = sourceOrig.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
                if (sourceDest == null) {
                    sourceDest = pushPopTree.sourceDest;
                }
                if (sourceDest == null) {
                    sourceDest = TapList.last(sourceOrigPath);
                }
                fwdArrows = pushPopTree.fwdArrows;
                // All the fwdArrows here share the same fwd origin, and therefore the same SymbolTable:
                SymbolTable fwdOrigBlockSymbolTable = fwdArrows.head.origin.symbolTable;
                SymbolTable passByValueSymbolTable =
                    ((adEnv.curUnit().isC()
                      && (!center.isATurn || fwdOrigBlockSymbolTable != pushPopTree.bwdBlock.symbolTable)
                      && sourceDest == adEnv.curUnit().exitBlock())
                     ? adEnv.curUnit().publicSymbolTable()
                     : null) ;
                // the local variables of fwdOrig do not risk vanishing if the destination bwdBlock
                // has the same symbolTable:
                SymbolTable fwdBwdSharedSymbolTable;
                if (center.isATurn) {
                    fwdBwdSharedSymbolTable = sourceOrigForSaveLocals.symbolTable;
                    // Walk up from sourceOrigForSaveLocals.symbolTable until fwd and bwd diff SymbolTables are the same,
                    // or until we reach the public symbolTable of current Unit:
                    while (fwdBwdSharedSymbolTable != adEnv.curUnit().publicSymbolTable()
                            && !pushPopTree.bwdBlock.symbolTable.nestedIn(
                            TapList.cassq(fwdBwdSharedSymbolTable, diffEnv.diffCtxtFwd.listST))) {
                        fwdBwdSharedSymbolTable = fwdBwdSharedSymbolTable.basisSymbolTable();
                    }
                } else {
                    fwdBwdSharedSymbolTable = sourceDest.symbolTable.getCommonRoot(sourceOrigForSaveLocals.symbolTable);
                }
                BoolVector constantZones = passByValueSymbolTable == null ? null : adEnv.curUnit().entryBlock().constantZones;
                // Note: unitTBRs may be null for a passive Unit that CONTAINs an active Unit:
                // (cf 90:v197) In that case there is no locals saving in the passive Unit.
                boolean mustSaveLocals =
                        (!deferDeclAndRestore
                         && !adEnv.curActivity().isContext()
                         && diffEnv.adDiffMode != DiffConstants.TANGENT_MODE
                         && mustSaveLocalsBeforeTheyVanish(sourceOrigForSaveLocals, sourceDest, fwdBwdSharedSymbolTable, lenTBRVec,
                                lenTBRVecOnDiffPtr, passByValueSymbolTable, constantZones));
                RefDescriptor branchVarRefDescriptor =
                        new RefDescriptor(ILUtils.build(ILLang.op_intCst, offset), null,
                                fwdOrigBlockSymbolTable, fwdOrigBlockSymbolTable, null,
                                null, false, null, curDiffUnit());
                if (!(TapEnv.get().staticTape
                      // force static taping on GPU code:
                      || (TapEnv.multithreadAnalyzer()!=null
                          && TapEnv.multithreadAnalyzer().isGPU(adEnv.curUnit())))
                    || diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE
                    // ^ static taping is not possible in ADJOINT_SPLIT_MODE
                    || branchSaveVarRefDescriptor == null) {
                    branchVarRefDescriptor.makeFixedControl(totalNumberOfCases);
                    branchVarRefDescriptor.prepareForStack(curDiffUnit(), ppLabel, null, true);
                } else {
                    branchVarRefDescriptor.forceStaticSave(branchSaveVarRefDescriptor);
                    branchVarRefDescriptor.prepareForAssignOrNormDiff(branchSaveVarRefDescriptor, curDiffUnit(), null, true);
                }
                FGArrow fwdChain;
                // Deal with all the fwd arrows on which a PUSH must possibly be placed:
                TapList<Block> pushLocalsBlocks = null;
                // The source Block that indicates the reference multithread-parallel control:
                Block parallelControlRef = sourceDest ;
                if (sourceDest.parallelControls==null && sourceOrig.parallelControls!=null
                    // CUDA calls contain a single call: whatever comes after in not a CUDA call (cf set12/onera06)
                    && !ILUtils.isCudaController(sourceOrig.parallelControls.head.tree))
                    parallelControlRef = sourceOrig ;
                while (fwdArrows != null) {
                    // [llh] TODO: Possible improvement: when all the arrows in "fwdArrows"
                    // are exactly all the arrows flowing from their common origin,
                    // then instead of adding the blocks for {Save_Local_Vars; PUSH(branch);}
                    // on each arrow, we should rather insert these instructions only once,
                    // on the fwdBlock of the origin, just before the last test.
                    fwdChain = fwdArrows.head;
                    // If necessary, insert a Block to hold the PUSH of vanishing local vars:
                    if (mustSaveLocals) {
                        Block pushLocalsBlock = buildFwdBlock("+PushLocals", fwdOrigBlockSymbolTable, parallelControlRef, diffEnv);
                        pushLocalsBlocks = new TapList<>(pushLocalsBlock, pushLocalsBlocks);
                        insertBlockAtOrig(fwdChain, pushLocalsBlock, FGConstants.NO_TEST, 0);
                    }
                    // If necessary, insert a Block to hold the PUSH of the branch number:
                    if (totalNumberOfCases >= 2) {
                        // If more than 1 arrow arrives here: must install a switch:
                        Block setBranchBlock = buildFwdBlock("+PushBranch", fwdOrigBlockSymbolTable, parallelControlRef, diffEnv);
                        setBranchBlock.addInstrTl(branchVarRefDescriptor.makePush());
                        insertBlockAtOrig(fwdChain, setBranchBlock, FGConstants.NO_TEST, 0);
                    }
                    fwdArrows = fwdArrows.tail;
                }
                // Now deal with the bwd sweep:
                Block bwdSweepEntry = pushPopTree.bwdBlock;
                // If necessary, insert a Block that initializes derivatives:
                // Attention: NATURAL_LOOPs have already placed their diff reinitializations
                // upon source loop exit, somewhere inside their diff loop.
                // So we don't want to duplicate them here (i.e. between the bwd sweep of the
                // downstream piece and the bwd diff of an upstream NATURAL_LOOP (cf F77:lh51 vs C:v217) :
                boolean deferReinit = upstreamLevel != null && upstreamLevel.levelKind == DiffConstants.NATURAL_LOOP;
                if (!deferReinit && diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
                    bwdSweepEntry = placeChainOfDiffReinits(sourceOrigPath, sourceDest, bwdSweepEntry, diffEnv);
                }
                // If necessary, insert a Block to hold the calls to AMPI_Turn(),
                //  for active variables whose primal is MPI_Irecv'ed:
                BoolVector zonesToTurn = adEnv.curActivity().zonesMPIiReceived();
                int zonesToTurnLength = adEnv.curActivity().zonesMPIiReceivedLength();
                int turnLength = sourceOrig.symbolTable.declaredZonesNb(adEnv.diffKind) ;
                // Temporary: until we have a better computation of the zonesMPIiReceived,
                //  we risk having a zonesMPIiReceived that is smaller than turnLength
                if (turnLength > zonesToTurnLength) {
                    turnLength = zonesToTurnLength;
                }
                if (center.isATurn
                    && (adEnv.adDiffMode == DiffConstants.ADJOINT_MODE || adEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE)
                    && !zonesToTurn.isFalse(turnLength)) {
                    Block ampiTurnsBlock = buildBwdBlock("-AMPITurns", bwdSweepEntry.symbolTable, sourceDest, diffEnv) ;
                    placeAMPITurns(zonesToTurn, ampiTurnsBlock, sourceOrig.symbolTable, diffEnv);
                    checkFreeThenSetNewFGArrow(ampiTurnsBlock, FGConstants.NO_TEST, 0, bwdSweepEntry, false);
                    bwdSweepEntry = ampiTurnsBlock;
                }
                // If necessary, insert a Block to hold the POP of vanishing local vars:
                if (mustSaveLocals) {
                    Block popLocalsBlock = buildBwdBlock("-PopLocals", bwdSweepEntry.symbolTable, parallelControlRef, diffEnv) ;
                    if (!adEnv.curUnitIsContext) {
                        saveLocalsBeforeTheyVanish(sourceOrigForSaveLocals, sourceDest, fwdBwdSharedSymbolTable, lenTBRVec, lenTBRVecOnDiffPtr,
                                pushLocalsBlocks, popLocalsBlock, passByValueSymbolTable, constantZones,
                                diffEnv.adDiffMode == DiffConstants.ADJOINT_MODE);
                    }
                    checkFreeThenSetNewFGArrow(popLocalsBlock, FGConstants.NO_TEST, 0, bwdSweepEntry, false);
                    bwdSweepEntry = popLocalsBlock;
                }
                // When profiling checkpoints, add call to adProfileAdj_turn() here at the stack maximum:
                if (center.isATurn
                    && adEnv.adDiffMode == DiffConstants.ADJOINT_MODE
                    && TapEnv.get().profile) {
                    Block profileTurnsBlock = buildBwdBlock("-PROFILETurn", bwdSweepEntry.symbolTable, sourceDest, diffEnv) ;
                    placeProfileTurn(profileTurnsBlock, sourceOrig.symbolTable, diffEnv);
                    checkFreeThenSetNewFGArrow(profileTurnsBlock, FGConstants.NO_TEST, 0, bwdSweepEntry, false);
                    bwdSweepEntry = profileTurnsBlock;
                }
                // Now connect fwd sweeps to bwd Block, possibly through the center:
                if (center.exits != null) {
                    // If there is a center, connect it to the bwd sweep entry,
                    // and the entry of the center becomes the new bwd sweep entry.
                    mapCheckFreeThenRedirectDestination(center.exits, bwdSweepEntry, false);
                    if (center.entry != null) {
                        bwdSweepEntry = center.entry;
                    } else {
                        bwdSweepEntry = center.exits.head.origin;
                    }
                }
                // Now connect any dangling fwd arrow to the bwd sweep entry:
                if (bwdSweepEntry != null) {
                    fwdArrows = pushPopTree.fwdArrows;
                    while (fwdArrows != null) {
                        if (fwdArrows.head.destination == null) {
                            checkFreeThenRedirectDestination(fwdArrows.head, bwdSweepEntry, false);
                        }
                        fwdArrows = fwdArrows.tail;
                    }
                }
            }
        } else {
            // We are on a non-leaf sub-tree of the PushPopTree, so
            // we will make recursive calls to connect the sub-PushPopTree's:

            // Connect any new bwd Block required by this PushPopNode
            // to the exit of the current DownstreamConnector:
            if (pushPopTree.bwdBlock != null) {
                // if there is a new bwd Block to be inserted here, insert it:
                center.putBlockAtTail(pushPopTree.bwdBlock);
                center.putArrowAtTail(FGConstants.NO_TEST, 0);
            }
            // Take care of the control PUSH/POP when needed.
            if (center.exits == null || diffEnv.adDiffMode == DiffConstants.TANGENT_MODE /*|| totalNumberOfCases==1*/) {
                // if the current DownstreamConnector is still empty, there is still no need for PUSH/POP
                // of the control, since each fwd sub-case can go directly to its bwd sub-case.
                // Also when all cases go to the same bwdBlock, there no need for PUSH/POP of the control.
                TapList<PushPopNode> inSubNodes = pushPopTree.subNodes;
                while (inSubNodes != null) {
                    connectPushPopTree(inSubNodes.head, new DownstreamConnector(center),
                            deferDeclAndRestore, enclosingLevel, -1, 0, diffEnv,
                            null, -1, doIndexToSave, ppLabelForIndex);
                    inSubNodes = inSubNodes.tail;
                }
            } else if (pushPopTree.subNodes.tail == null) {
                // else there is only one case, but there is a central code between fwd and bwd:
                connectPushPopTree(pushPopTree.subNodes.head, center,
                        deferDeclAndRestore, enclosingLevel, offset, totalNumberOfCases, diffEnv,
                        branchSaveVarRefDescriptor, ppLabel, doIndexToSave, ppLabelForIndex);
            } else {
                // else there is more than one sub-case to this PushPopNode.
                // Since on the other hand the current DownstreamConnector is not empty,
                // then we must insert an if-then-else on the PUSH/POP'ed control variable.
                // Morever, if this is the root of the switch,
                // one must insert here the POP of the control variable:
                boolean popHere = offset == -1;
                if (diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
                    if (sourceDest == null) {
                        // In rare situations, there is no given unique "sourceDest".
                        // However, we may need one below. Pick the first one in pushPopTree!
                        sourceDest = pushPopTree.pickFirstSourceDest();
                    }
                    if (popHere) {
                        SymbolTable fwdST = null;
                        if (center.entry != null) {
                            fwdST = center.entry.symbolTable;
                        }
                        branchSaveVarRefDescriptor =
                                blockDifferentiator().tryStaticTape(sourceDest, 0, "ad_branch", adEnv.integerTypeSpec,
                                        fwdST, center.bwdScope);
                        offset = 0;
                        totalNumberOfCases = pushPopTree.numberOfCases;
                    }
                    if (toBranchVariable.first == null) {
                        toBranchVariable.first = new NewSymbolHolder("branch", curDiffUnit(), toBranchVariable.second, -1) ;
                    }
                    toBranchVariable.first.declarationLevelMustInclude(curDiffUnit().privateSymbolTable());
                    toBranchVariable.first.preparePrivateClause(sourceDest, adEnv.curUnit().privateSymbolTable(), false, false);
                    //ILUtils.addVarIntoFuturePrivates(toBranchVariable.first, parallelRegionInstrTree, false, false) ;
                    Tree branchVarRef;
                    if (branchSaveVarRefDescriptor != null) {
                        branchSaveVarRefDescriptor.prepareForInitialize(curDiffUnit(), null, true);
                        // Should rather be something like "prepareForRef()" !
                        branchVarRef = branchSaveVarRefDescriptor.makeRef();
                    } else {
                        ppLabel = TapEnv.getNewPushPopNumber();
                        branchVarRef = toBranchVariable.first.makeNewRef(center.bwdScope);
                        toBranchVariable.first.declarationLevelMustInclude(center.bwdScope);
                    }
                    Block bwdIfBlock = buildBwdBlock((popHere ? "-PopSwPop" : "-SwPop"), center.bwdScope, sourceDest, diffEnv);
                    int cutRank = pushPopTree.subNodes.head.numberOfCases;
                    Tree test;
                    if (cutRank == 1) {
                        test = ILUtils.build(ILLang.op_eq, branchVarRef,
                                ILUtils.build(ILLang.op_intCst, offset));
                    } else {
                        test = ILUtils.build(ILLang.op_lt, branchVarRef,
                                ILUtils.build(ILLang.op_intCst, offset + cutRank));
                    }
                    bwdIfBlock.addInstrHdAfterDecls(ILUtils.build(ILLang.op_if, test));
                    // Only if this is the root of a control switch  with more than one sub-case,
                    // only when the tape is stored on a dynamic stack,
                    // then insert a POP of the control switch value:
                    if (popHere && branchSaveVarRefDescriptor == null) {
                        bwdIfBlock.addInstrHdAfterDecls(
                                RefDescriptor.makeFixedControlPop(curDiffUnit(),
                                        toBranchVariable.first.makeNewRef(center.bwdScope),
                                        totalNumberOfCases, center.bwdScope, center.bwdScope, ppLabel));
                        toBranchVariable.first.declarationLevelMustInclude(center.bwdScope);
                    }
                    center.putBlockAtTail(bwdIfBlock);
                    DownstreamConnector centerLeft =
                            new DownstreamConnector(center);
                    centerLeft.putArrowAtTail(FGConstants.IF, FGConstants.TRUE);
                    connectPushPopTree(pushPopTree.subNodes.head, centerLeft,
                            deferDeclAndRestore, enclosingLevel, offset, totalNumberOfCases, diffEnv,
                            branchSaveVarRefDescriptor, ppLabel, doIndexToSave, ppLabelForIndex);
                    DownstreamConnector centerRight =
                            new DownstreamConnector(center);
                    centerRight.putArrowAtTail(FGConstants.IF, FGConstants.FALSE);
                    connectPushPopTree(pushPopTree.subNodes.tail.head, centerRight,
                            deferDeclAndRestore, enclosingLevel, offset + cutRank, totalNumberOfCases, diffEnv,
                            branchSaveVarRefDescriptor, ppLabel, doIndexToSave, ppLabelForIndex);
                }
            }
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.decrTraceIndent(1);
            TapEnv.indentprintlnOnTrace("]K");
        }
    }

    /**
     * When some TBR variables fall out of scope when control flow
     * goes from a source "origBlock" to a source destination block that only shares
     * "sharedSymbolTable" with origBlock, save the values of these
     * variables (in each pushBlock) and restore them later (in popBlock).
     */
    private void saveLocalsBeforeTheyVanish(Block origBlock, Block destBlock, SymbolTable sharedSymbolTable,
                                            int lenTBRVec, int lenTBRVecOnDiffPtr,
                                            TapList<Block> pushBlocks, Block popBlock,
                                            SymbolTable passByValueSymbolTable, BoolVector constantZones, boolean inJointDiffCode) {
        TapList<TapPair<BoolVector, BoolVector>> origBlockTbr;
        if (origBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            origBlockTbr = ((TemporaryBlock) origBlock).blockTBRs;
        } else {
            origBlockTbr = (adEnv.unitTBRs == null ? null : adEnv.unitTBRs.retrieve(origBlock)) ;
        }
        TapPair<BoolVector, BoolVector> tbrPairAtTheEnd = TapList.last(origBlockTbr);
        BoolVector tbrAtTheEnd = tbrPairAtTheEnd == null ? null : tbrPairAtTheEnd.first;
        if (!TapEnv.doTBR() && tbrAtTheEnd == null) { // no-TBR case
            tbrAtTheEnd = new BoolVector(lenTBRVec);
            tbrAtTheEnd.setTrue();
        }
        // If origBlock is in an OpenMP multithread region, and destBlock is not,
        // then the private variables must recover their
        // TBR status upon entry into the parallel region:
        if (origBlock.parallelControls!=null && destBlock.parallelControls==null
            && ILUtils.isIdent(origBlock.parallelControls.head.tree.down(1), "omp", false)) {
            Block parallelRegionBlock = TapList.last(origBlock.parallelControls).block ;
            BoolVector tbrBeforeRegion = adEnv.unitTBRs.retrieve(parallelRegionBlock).head.first ;
            BoolVector sharedZones =
                TapEnv.multithreadAnalyzer().getSharedZonesOfBlock(parallelRegionBlock) ;
            tbrAtTheEnd = tbrAtTheEnd.and(sharedZones).or(tbrBeforeRegion.minus(sharedZones));
        }
        BoolVector tbrAtTheEndOnDiffPtr = tbrPairAtTheEnd == null ? null : tbrPairAtTheEnd.second;
        if (!TapEnv.doTBR() && tbrAtTheEndOnDiffPtr == null) { // no-TBR case
            tbrAtTheEndOnDiffPtr = new BoolVector(lenTBRVecOnDiffPtr);
            tbrAtTheEndOnDiffPtr.setTrue();
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("Save vanishing locals from " + origBlock + " to " + destBlock);
            TapEnv.printlnOnTrace("  TBR:" + DataFlowAnalyzer.infoToStringWithDiffPtr(tbrAtTheEnd, lenTBRVec, tbrAtTheEndOnDiffPtr, lenTBRVecOnDiffPtr, adEnv.curUnit(), origBlock.symbolTable));
        }
        SymbolTable origSymbolTable = origBlock.symbolTable;
        int maxDeclZonesNb = origSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        ZoneInfo zoneInfo;
        Tree zoneInfoVisibleAccessTree;
        TapList<Block> inPushBlocks;
        Block pushBlock;
        int i, ip, ppLabelForLocalVar;
        TapList<ZoneInfo> vanishingZones = null;
        // Order is important because pointers must be popped before pointees :
        if (passByValueSymbolTable != null) {
            // In a PASS-BY-VALUE language, the formal parameters that are modified and TBR will loose their value
            // at the end of the fwd sweep, because they are not sent to the calling actual parameter.
            // Therefore we must save them before exit of the split subroutine_FWD.
            for (i = passByValueSymbolTable.firstDeclaredZone(SymbolTableConstants.ALLKIND); i < passByValueSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); ++i) {
                zoneInfo = passByValueSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zoneInfo != null
                    // Don't push/pop the "virtual" variable for function result in C. cf set09/v139:
                    && !zoneInfo.isResult()
                    && !zoneInfo.isHidden && zoneInfo.targetZoneOf==null
                    && !zoneInfo.isControl()
                    && !constantZones.get(i)) {
                    vanishingZones = new TapList<>(zoneInfo, vanishingZones);
                }
            }
        }
        for (i = sharedSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); i < maxDeclZonesNb; ++i) {
            zoneInfo = origSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null && !zoneInfo.isHidden
                // Question is: do we need to push/pop allocatable pointers?
                // On one hand they are not modifiable, on the other hand
                // they might be DEALLOCATE'd and re-ALLOCATE'd.
                // If wanting to push/pop ALLOCATABLE pointers,
                // just comment the 8 occurrences of:
                //       ... && !zoneInfo.isAllocatable //NoPushAllocatable
                && !zoneInfo.isAllocatable //NoPushAllocatable
                && !zoneInfo.isConstant() // No push/pop of C "const" variables.
                && !zoneInfo.isControl()
                // Don't push/pop the "virtual" variable for function result in C. cf set09/v139
                && !(curDiffUnit().isC() && zoneInfo.isResult())
            ) {
                vanishingZones = new TapList<>(zoneInfo, vanishingZones);
            }
        }

        while (vanishingZones != null) {
            zoneInfo = vanishingZones.head;
            i = zoneInfo.zoneNb;
            ip = zoneInfo.ptrZoneNb;
            Instruction popDiffInstr, popInstr;
            zoneInfoVisibleAccessTree = origSymbolTable.buildZoneVisibleAccessTree(zoneInfo);
            Tree baseVarTree = ILUtils.baseTree(zoneInfoVisibleAccessTree);
            Tree diffBaseVar = null;
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("  - vanishing " + zoneInfo + " is TBR:" + tbrAtTheEnd.get(i)
                        + (ip != -1 ? " and on diffPtr:" + tbrAtTheEndOnDiffPtr.get(ip) : ""));
            }
            boolean varIsTBR = tbrAtTheEnd.get(i);
            boolean diffPointerIsTBR = false;
            // Place a "rebase" if we are going to pop some pointers:
            if (ip != -1) {
                diffPointerIsTBR = tbrAtTheEndOnDiffPtr.get(ip);
                if (diffPointerIsTBR) {
                    diffBaseVar = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, popBlock.symbolTable,
                            true, false, false, null, false,
                            curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
                }
                if (varIsTBR || diffPointerIsTBR) {
                    Instruction dummyInstruction = new Instruction();
                    dummyInstruction.block = origBlock;
                    boolean rebasePrimal = varIsTBR &&
                            DataFlowAnalyzer.mayPointToRelocated(zoneInfoVisibleAccessTree, false, dummyInstruction, origSymbolTable, inJointDiffCode);
                    boolean rebaseOnDiffPtr = diffPointerIsTBR &&
                            DataFlowAnalyzer.mayPointToRelocated(zoneInfoVisibleAccessTree, true, dummyInstruction, origSymbolTable, inJointDiffCode);
                    if (rebasePrimal || rebaseOnDiffPtr) {
                        // force F90 code to declare PUSH/LOOK/POP POINTER interfaces :
                        adEnv.curDiffUnitPushesPointers = true;
                        ++blockDifferentiator().numRebase;
                        RefDescriptor ptrRefDescriptor =
                                new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                        origSymbolTable, popBlock.symbolTable,
                                        null, null, false, null, curDiffUnit());
                        ptrRefDescriptor.prepareForStack(curDiffUnit(), -1, null, true);
                        ptrRefDescriptor.mayProtectAccesses = false;
                        Tree rebaseTree = ptrRefDescriptor.makeRebase(rebasePrimal ? null : new TapList<>(Boolean.TRUE, null),
                                rebaseOnDiffPtr ? null : new TapList<>(Boolean.TRUE, null),
                                rebaseOnDiffPtr ? diffBaseVar : null,
                                (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
                        if (rebaseTree != null) {
                            adEnv.usesADMM = true;
                            if (adEnv.traceCurDifferentiation) {
                                TapEnv.printlnOnTrace("     -> (+PopLocals) rebase: " + ILUtils.toString(rebaseTree));
                            }
                            popBlock.addInstrHdAfterDecls(new Instruction(rebaseTree, null, null));
                        }
                    }
                }
            }
            // Deal with push/pop of diff pointers:
            if (ip != -1 && diffPointerIsTBR) {
                ppLabelForLocalVar = TapEnv.getNewPushPopNumber();
                inPushBlocks = pushBlocks;
                Tree diffBaseVarForPush;
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("     -> (PushLocals+) push Diff pointer: " + zoneInfoVisibleAccessTree);
                }
                while (inPushBlocks != null) {
                    pushBlock = inPushBlocks.head;
                    diffBaseVarForPush = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, pushBlock.symbolTable,
                            true, false, false, null, false,
                            curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
                    pushBlock.addInstrTl(
                            RefDescriptor.makePushDiff(curFwdDiffUnit(), zoneInfoVisibleAccessTree, diffBaseVarForPush,
                                    origSymbolTable, pushBlock.symbolTable, ppLabelForLocalVar));
                    inPushBlocks = inPushBlocks.tail;
                }
                popDiffInstr =
                        RefDescriptor.makePopDiff(curDiffUnit(), zoneInfoVisibleAccessTree, diffBaseVar,
                                origSymbolTable, popBlock.symbolTable, ppLabelForLocalVar);
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("     -> (+PopLocals) pop Diff pointer: " + popDiffInstr);
                }
                popBlock.addInstrHdAfterDecls(popDiffInstr);
            }
            // Deal with normal push/pop of a TBR variable:
            if (varIsTBR) {
                ppLabelForLocalVar = TapEnv.getNewPushPopNumber();
                inPushBlocks = pushBlocks;
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("     -> (PushLocals+) push: " + zoneInfoVisibleAccessTree);
                }
                while (inPushBlocks != null) {
                    pushBlock = inPushBlocks.head;
                    pushBlock.addInstrTl(
                            RefDescriptor.makePush(curFwdDiffUnit(), zoneInfoVisibleAccessTree,
                                    origSymbolTable, pushBlock.symbolTable, ppLabelForLocalVar));
                    inPushBlocks = inPushBlocks.tail;
                }
                popInstr =
                        RefDescriptor.makePop(curDiffUnit(), zoneInfoVisibleAccessTree,
                                origSymbolTable, popBlock.symbolTable,
                                ppLabelForLocalVar, toBranchVariable);
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("     -> (+PopLocals) pop: " + popInstr);
                }
                popBlock.addInstrHdAfterDecls(popInstr);
            }
            vanishingZones = vanishingZones.tail;
        }
    }

    /**
     * When some TBR variables "sort-of" fall out of scope because they are OpenMP-private and
     * we are jumping out of the parallel region, we must save them and restore them at the
     * entry of the adjoint parallel region.
     */
    private void savePrivatesBeforeTheyVanish(Block pushBlock, Block popBlock,
                                              FlowGraphLevel regionLevel) {
        SymbolTable origSymbolTable = regionLevel.entryBlock.symbolTable;
        int allKindLength = regionLevel.entryBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        BoolVector exitTBR = new BoolVector(allKindLength);
        TapList<FGArrow> sourceExitArrows = regionLevel.exitArrows;
        Block origBlock;
        TapList<TapPair<BoolVector, BoolVector>> origBlockTbr;
        while (sourceExitArrows != null) {
            origBlock = sourceExitArrows.head.origin;
            if (origBlock.rank == -99) {
                // This is a TemporaryBlock. Its data flow info is attached directly!
                origBlockTbr = ((TemporaryBlock) origBlock).blockTBRs;
            } else {
                origBlockTbr = (adEnv.unitTBRs == null ? null : adEnv.unitTBRs.retrieve(origBlock));
            }
            TapPair<BoolVector, BoolVector> tbrPairAtTheEnd = TapList.last(origBlockTbr);
            BoolVector tbrAtTheEnd = (tbrPairAtTheEnd == null ? null : tbrPairAtTheEnd.first);
            if (!TapEnv.doTBR() && tbrAtTheEnd == null) { // no-TBR case
                exitTBR.setTrue();
            } else {
                exitTBR.cumulOr(tbrAtTheEnd, allKindLength);
            }
            sourceExitArrows = sourceExitArrows.tail;
        }
        BoolVector zonesToSave = exitTBR ;
        Instruction parallelControlInstr = regionLevel.entryBlock.lastInstr() ;
        if (parallelControlInstr!=null && parallelControlInstr.tree.opCode()==ILLang.op_parallelRegion
            && ILUtils.isIdent(parallelControlInstr.tree.down(1), "CUDA", true)) {
            zonesToSave.setFalse() ;
        } else if (TapEnv.multithreadAnalyzer()!=null) {
            BoolVector sharedZones =
                TapEnv.multithreadAnalyzer().getSharedZonesOfBlock(regionLevel.entryBlock);
            zonesToSave = zonesToSave.minus(sharedZones);
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("Save vanishing privates. Privates TBR at exit:"+zonesToSave) ;
        }
        TapList<ZoneInfo> vanishingZones = null;
        ZoneInfo zoneInfo;
        for (int i=0;
             i<regionLevel.entryBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
             ++i) {
            if (zonesToSave.get(i)) {
                zoneInfo = origSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zoneInfo != null && !zoneInfo.isHidden
                        && !zoneInfo.isAllocatable && !zoneInfo.isConstant()) {
                    vanishingZones = new TapList<>(zoneInfo, vanishingZones);
                }
            }
        }
        while (vanishingZones != null) {
            zoneInfo = vanishingZones.head;
            Tree zoneInfoVisibleAccessTree = origSymbolTable.buildZoneVisibleAccessTree(zoneInfo);
            Tree baseVarTree = ILUtils.baseTree(zoneInfoVisibleAccessTree);
            int ppLabelForPrivateVar = TapEnv.getNewPushPopNumber();
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("     -> (BB\""+pushBlock.symbolicRk+"\") push: " + zoneInfoVisibleAccessTree);
            }
            pushBlock.addInstrTl(
                    RefDescriptor.makePush(curFwdDiffUnit(), zoneInfoVisibleAccessTree,
                            origSymbolTable, pushBlock.symbolTable, ppLabelForPrivateVar));
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("     -> (BB\""+popBlock.symbolicRk+"\") pop: " + zoneInfoVisibleAccessTree);
            }
            popBlock.addInstrHdAfterDecls(
                    RefDescriptor.makePop(curDiffUnit(), zoneInfoVisibleAccessTree,
                            origSymbolTable, popBlock.symbolTable,
                            ppLabelForPrivateVar, toBranchVariable));
            vanishingZones = vanishingZones.tail;
        }
    }

    /**
     * Similar to saveLocalsBeforeTheyVanish() , but just returns true if
     * saveLocalsBeforeTheyVanish() would create some saving/restoring instructions.
     */
    private boolean mustSaveLocalsBeforeTheyVanish(Block origBlock, Block destBlock, SymbolTable sharedSymbolTable,
                                                   int lenTBRVec, int lenTBRVecOnDiffPtr,
                                                   SymbolTable passByValueSymbolTable, BoolVector constantZones) {
        TapList<TapPair<BoolVector, BoolVector>> origBlockTbr;
        if (origBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            origBlockTbr = ((TemporaryBlock) origBlock).blockTBRs;
        } else {
            origBlockTbr = (adEnv.unitTBRs == null ? null : adEnv.unitTBRs.retrieve(origBlock));
        }
        TapPair<BoolVector, BoolVector> tbrPairAtTheEnd = TapList.last(origBlockTbr);
        BoolVector tbrAtTheEnd = tbrPairAtTheEnd == null ? null : tbrPairAtTheEnd.first;
        if (adEnv.traceCurDifferentiation) {
            System.out.println("?SAVE LOCALS BEFORE THEY VANISH FROM "+origBlock+" TO "+destBlock) ;
            System.out.println("TBR AT THE END:"+tbrAtTheEnd) ;
        }
        if (!TapEnv.doTBR() && tbrAtTheEnd == null) { // no-TBR case
            tbrAtTheEnd = new BoolVector(lenTBRVec);
            tbrAtTheEnd.setTrue();
        }
        // If origBlock is in an OpenMP multithread region, and destBlock is not,
        // then the private variables must recover their
        // TBR status upon entry into the parallel region:
        if (origBlock.parallelControls!=null && destBlock.parallelControls==null
            && ILUtils.isIdent(origBlock.parallelControls.head.tree.down(1), "omp", false)) {
            Block parallelRegionBlock = TapList.last(origBlock.parallelControls).block ;
            BoolVector tbrBeforeRegion = adEnv.unitTBRs.retrieve(parallelRegionBlock).head.first ;
            BoolVector sharedZones =
                TapEnv.multithreadAnalyzer().getSharedZonesOfBlock(parallelRegionBlock) ;
            tbrAtTheEnd = tbrAtTheEnd.and(sharedZones).or(tbrBeforeRegion.minus(sharedZones));
        }
        BoolVector tbrAtTheEndOnDiffPtr = tbrPairAtTheEnd == null ? null : tbrPairAtTheEnd.second;
        if (!TapEnv.doTBR() && tbrAtTheEndOnDiffPtr == null) { // no-TBR case
            tbrAtTheEndOnDiffPtr = new BoolVector(lenTBRVecOnDiffPtr);
            tbrAtTheEndOnDiffPtr.setTrue();
        }
        SymbolTable origSymbolTable = origBlock.symbolTable;
        int maxDeclZonesNb = origSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        ZoneInfo zoneInfo;
        boolean mustSave = false;

        if (passByValueSymbolTable != null) {
            for (int i = passByValueSymbolTable.firstDeclaredZone(SymbolTableConstants.ALLKIND); !mustSave && i < passByValueSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); ++i) {
                zoneInfo = passByValueSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zoneInfo != null && !zoneInfo.isHidden && zoneInfo.targetZoneOf == null && !constantZones.get(i)) {
                    int ip = zoneInfo.ptrZoneNb;
                    if (tbrAtTheEnd.get(i) ||
                            ip != -1 && tbrAtTheEndOnDiffPtr.get(ip)) {
                        mustSave = true;
                    }
                }
            }
        }
        for (int i = sharedSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); !mustSave && i < maxDeclZonesNb; ++i) {
            zoneInfo = origSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null && !zoneInfo.isHidden
                    && !zoneInfo.isAllocatable //NoPushAllocatable
                    && !zoneInfo.isConstant() // No push/pop of C "const" variables.
            ) {
                int ip = zoneInfo.ptrZoneNb;
                if (tbrAtTheEnd.get(i) ||
                        ip != -1 && tbrAtTheEndOnDiffPtr.get(ip)) {
                    mustSave = true;
                }
            }
        }
        return mustSave;
    }

    /**
     * Runs a new series of data-flow analysis on the contents of "checkpointedPiece".
     * This is useful for CHECKPOINT's, II_LOOP's and BINOMIAL_LOOP's, whose body will be
     * differentiated JOINT, i.e. with the fwd sweep of the body immediately followed by
     * its bwd sweep, therefore allowing for more adjoint dead code and optimizations.
     *
     * @param checkpointedPiece the piece of code that will be run JOINT.
     * @param loopHeader iff non-null, means that the checkpointedPiece is a loop body but its
     *   "outside" is the outside of the complete loop headed by "loopHeader". E.g. for II_LOOPs !
     * @param allSaves used to save then restore the original data-flow info (before running the
     *   analysis restricted on the given checkpointedPiece.
     *   Presently only allSaves[1] is used, but who knows...

     * @return the 7-elements array:
     * <pre>
     * [0:Context req (i.e. TBR) upstream at piece entry,
     * 1:Local useCb through piece,
     * 2:Local outCb through piece,
     * 3:Local useC read through piece,
     * 4:Local outC modified through piece,
     * 5:Context useDb downstream piece exit,
     * 6:Context outDb downstream piece exit].
     * </pre>
     */
    private TapPair<BoolVector[], BoolVector[]> recomputeDataFlowCheckpointed(FlowGraphLevel checkpointedPiece,
                                                                              HeaderBlock loopHeader, boolean modeIsJoint, Object[] allSaves) {
        TapEnv.printOnTrace(20, "@@ Recomputing data-flow for checkpointed piece " + checkpointedPiece + ": ");
        // [llh] TODO: These frontiers are maybe wrong or incorrect,
        // especially for "createNoCycle()" levels, which may have several entry/exit points
        TapList<Block> blocksInside = checkpointedPiece.allBlocksInside();
        TapList<FGArrow> entryFrontier = checkpointedPiece.entryArrows;
        TapList<FGArrow> exitFrontier = checkpointedPiece.exitArrows;
        TapList<FGArrow> completeLoopEntryFrontier = loopHeader == null ? entryFrontier : loopHeader.enteringArrows();
        TapList<FGArrow> completeLoopExitFrontier = loopHeader == null ? exitFrontier : loopHeader.enclosingLoop().flow();
        TapList<FGArrow> entryFrontierNoCycle = removeCyclingFrom(entryFrontier, blocksInside);

        alteredStaticAnalysis = true;

        TapPair<BoolVector, BoolVector> useDb = null, useCb = null, outDb = null, outCb = null;
        if (TapEnv.diffLivenessAnalyzer() != null) {
            // diffLivenessAnalyzer may be null when using -nooptim adjointliveness
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(adEnv.curUnit());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(adEnv.curActivity());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(adEnv.curActivity(), modeIsJoint);
            //[llh 7/1/2016] TODO: now we should also use the ".second" parts (i.e. on diff pointers),
            // about diff pointers manipulated/precomputed by the FWD sweep :
            useDb = TapEnv.diffLivenessAnalyzer().getFrontierLiveness(completeLoopExitFrontier);
            outDb = TapEnv.diffLivenessAnalyzer().getFrontierOverwrite(completeLoopExitFrontier);
            TapEnv.diffLivenessAnalyzer().setPhaseSubGraph();
            TapEnv.diffLivenessAnalyzer().resetLivenessInfo(blocksInside);
//             allSaves[2] = TapEnv.diffLivenessAnalyzer().saveInsideInfo(blocksInside) ; //TODO?
            TapEnv.diffLivenessAnalyzer().analyzeBackward(entryFrontier, blocksInside, exitFrontier);
            // ... also, similarly, use the ".second" parts here (i.e. on diff pointers):
            // The next getFrontierLiveness() must skip arrows cycling from blocksInside:
            useCb = TapEnv.diffLivenessAnalyzer().getFrontierLiveness(entryFrontierNoCycle);
            // The next getFrontierOverwrite(), must skip arrows cycling from blocksInside:
            outCb = TapEnv.diffLivenessAnalyzer().getFrontierOverwrite(entryFrontierNoCycle);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(null, modeIsJoint);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(null);
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(null);
        }

        ADTBRAnalyzer tbrAnalyzer = TapEnv.adTbrAnalyzer();
        tbrAnalyzer.setCurUnitEtc(adEnv.curUnit());
        tbrAnalyzer.setCurUnitActivity(adEnv.curActivity());
        TapPair<BoolVector, BoolVector> req = tbrAnalyzer.getFrontierTBR(completeLoopEntryFrontier);
        tbrAnalyzer.setPhaseSubGraph();
        tbrAnalyzer.prepareStorageForUnit(adEnv.curUnit());
        allSaves[1] = tbrAnalyzer.saveInsideInfo(blocksInside, adEnv.unitTBRs) ;
        tbrAnalyzer.analyzeForward(entryFrontier, blocksInside, exitFrontier);
        BoolVector outC;
        BoolVector useC;
        InOutAnalyzer inOutAnalyzer = TapEnv.inOutAnalyzer();
        inOutAnalyzer.setCurUnitEtc(adEnv.curUnit());
        // The next two (modifiedZones and notUnusedZones) must be extracted from inOutAnalyzer now i.e. BEFORE
        //   the re-running of inOutAnalyzer, because we are looking for the info on the DOWNSTREAM part D */
        if (outDb == null) {
            BoolVector modifiedZones = inOutAnalyzer.getFrontierModifiedZones(completeLoopExitFrontier);
            outDb = new TapPair<>(modifiedZones, restrictToPointers(modifiedZones, completeLoopExitFrontier));
        }
        if (useDb == null) {
            BoolVector notUnusedZones = inOutAnalyzer.getFrontierUnusedZones(completeLoopExitFrontier).not();
            useDb = new TapPair<>(notUnusedZones, restrictToPointers(notUnusedZones, completeLoopExitFrontier));
        }

        inOutAnalyzer.setFgPhase(InOutAnalyzer.INOUT);
//         allSaves[0] = inOutAnalyzer.saveInsideInfo(blocksInside); //TODO?
        inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);
        // The next getFrontierPossiblyReadZones() must skip arrows cycling from blocksInside:
        useC = inOutAnalyzer.getFrontierPossiblyReadZones(entryFrontierNoCycle);

        inOutAnalyzer.setFgPhase(InOutAnalyzer.OTHER);
        inOutAnalyzer.setFgSubPhase(InOutAnalyzer.CONSTANT);
        inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);
        // The next getFrontierModifiedZones() must skip arrows cycling from blocksInside:
        outC = inOutAnalyzer.getFrontierModifiedZones(entryFrontierNoCycle);

        if (outCb == null) {
            outCb = new TapPair<>(outC, restrictToPointers(outC, entryFrontierNoCycle));
        }
        if (useCb == null) {
            inOutAnalyzer.setFgSubPhase(InOutAnalyzer.UNUSED);
            inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);
            // The next getFrontierUnusedZones() must skip arrows cycling from blocksInside:
            BoolVector notUnusedZones = inOutAnalyzer.getFrontierUnusedZones(entryFrontierNoCycle).not();
            useCb = new TapPair<>(notUnusedZones, restrictToPointers(notUnusedZones, entryFrontierNoCycle));
        }

        // Special for the Checkpoint analysis for the loop index of a II_LOOP:
        // -- it must be considered overwritten by both C and Cb,
        // -- it must be considered NOT needed by Cb,
        // -- if it is already PUSHED on loop entry, it must be removed from (the first half about primal vars of) req.
        if (loopHeader != null && loopHeader.isADoLoop()) {
            Instruction headerInstr = loopHeader.headInstr();
            adEnv.setCurInstruction(headerInstr);
            Tree doIndexTree = headerInstr.tree.down(3).down(1);
            TapIntList indexZones =
                    ZoneInfo.listAllZones(
                            loopHeader.symbolTable.treeOfZonesOfValue(doIndexTree, null, headerInstr, null),
                            true);
            if (indexZones != null && indexZones.tail == null && indexZones.head >= 0) {
                outC.set(indexZones.head, true);
                outCb.first.set(indexZones.head, true);
                useCb.first.set(indexZones.head, false);
                if (ADTBRAnalyzer.isTBRindexOnLoopEntry(adEnv.curActivity(), doIndexTree)) {
                    req.first.set(indexZones.head, false);
                }
            }
        }

        inOutAnalyzer.setCurUnitEtc(null);
        tbrAnalyzer.setCurUnitActivity(null);
        tbrAnalyzer.setCurUnitEtc(null);
        TapEnv.printlnOnTrace(20);

        BoolVector[] result = new BoolVector[7];
        result[0] = req.first;
        result[1] = useCb.first;
        result[2] = outCb.first;
        result[3] = useC;
        result[4] = outC;
        result[5] = useDb.first;
        result[6] = outDb.first;
        BoolVector[] resultOnDiffPtr = new BoolVector[7];
        resultOnDiffPtr[0] = req.second;
        resultOnDiffPtr[1] = useCb.second;
        resultOnDiffPtr[2] = outCb.second;
        resultOnDiffPtr[3] = restrictToPointers(useC, entryFrontierNoCycle);
        resultOnDiffPtr[4] = restrictToPointers(outC, entryFrontierNoCycle);
        resultOnDiffPtr[5] = useDb.second;
        resultOnDiffPtr[6] = outDb.second;
        return new TapPair<>(result, resultOnDiffPtr);
    }

    /**
     * @param allSaves used to save then restore the original data-flow info (before running the
     *   analysis restricted on the given checkpointedPiece.
     *   Presently only allSaves[1] is used, but who knows...
     */
    private void restoreDataFlowCheckpointed(FlowGraphLevel checkpointedPiece,
                                             HeaderBlock loopHeader, boolean modeIsJoint, Object[] allSaves) {

        TapList<Block> blocksInside = checkpointedPiece.allBlocksInside();
        TapList<FGArrow> entryFrontier = checkpointedPiece.entryArrows;
        TapList<FGArrow> exitFrontier = checkpointedPiece.exitArrows;
        if (TapEnv.diffLivenessAnalyzer() != null) {
            // diffLivenessAnalyzer may be null when using -nooptim adjointliveness
//TODO? :
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(adEnv.curUnit());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(adEnv.curActivity());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(adEnv.curActivity(), modeIsJoint);
            TapEnv.diffLivenessAnalyzer().setPhaseSubGraph();
//             TapEnv.diffLivenessAnalyzer().resetLivenessInfo(blocksInside);
//             TapEnv.diffLivenessAnalyzer().restoreInsideInfo(blocksInside, allSaves[2]) ;
//             TapEnv.diffLivenessAnalyzer().analyzeBackward(entryFrontier, blocksInside, exitFrontier);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(null, modeIsJoint);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(null);
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(null);
        }

        ADTBRAnalyzer tbrAnalyzer = TapEnv.adTbrAnalyzer();
        tbrAnalyzer.setCurUnitEtc(adEnv.curUnit());
        tbrAnalyzer.setCurUnitActivity(adEnv.curActivity());
        tbrAnalyzer.setPhaseSubGraph();
        tbrAnalyzer.prepareStorageForUnit(adEnv.curUnit());
        tbrAnalyzer.restoreInsideInfo(blocksInside, allSaves[1], adEnv.unitTBRs) ;
//         tbrAnalyzer.analyzeForward(entryFrontier, blocksInside, exitFrontier); // not needed if restoreInsideInfo restores everything inside.

        InOutAnalyzer inOutAnalyzer = TapEnv.inOutAnalyzer();
        inOutAnalyzer.setCurUnitEtc(adEnv.curUnit());
        inOutAnalyzer.setFgPhase(InOutAnalyzer.INOUT);
//TODO? :
//         inOutAnalyzer.restoreInsideInfo(blocksInside, allSaves[0]) ;
//         inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);
//         inOutAnalyzer.setFgPhase(InOutAnalyzer.OTHER);
//         inOutAnalyzer.setFgSubPhase(InOutAnalyzer.CONSTANT);
//         inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);
//         inOutAnalyzer.setFgSubPhase(InOutAnalyzer.UNUSED);
//         inOutAnalyzer.analyzeBackward(entryFrontier, blocksInside, exitFrontier);

        inOutAnalyzer.setCurUnitEtc(null);
        tbrAnalyzer.setCurUnitActivity(null);
        tbrAnalyzer.setCurUnitEtc(null);
    }

    private void restoreStaticAnalysis(boolean modeIsJoint) {

        TapEnv.printOnTrace(20, "@@ Restoring data-flow for unit " + adEnv.curUnit().name() + ": ");
        if (TapEnv.diffLivenessAnalyzer() != null) {
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(adEnv.curUnit());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(adEnv.curActivity());
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(adEnv.curActivity(), modeIsJoint);
            TapEnv.diffLivenessAnalyzer().setPhaseSubGraph();
            TapEnv.diffLivenessAnalyzer().resetLivenessInfo(adEnv.curUnit().allBlocks());
            TapEnv.diffLivenessAnalyzer().analyzeBackward(null, null, null);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivitiesLivenessesAndOverwrites(null, modeIsJoint);
            TapEnv.diffLivenessAnalyzer().setCurUnitActivity(null);
            TapEnv.diffLivenessAnalyzer().setCurUnitEtc(null);
        }

        ADTBRAnalyzer tbrAnalyzer = TapEnv.adTbrAnalyzer();
        tbrAnalyzer.setCurUnitEtc(adEnv.curUnit());
        tbrAnalyzer.setCurUnitActivity(adEnv.curActivity());
        tbrAnalyzer.setPhaseSubGraph();
        tbrAnalyzer.prepareStorageForUnit(adEnv.curUnit());
        tbrAnalyzer.analyzeForward(null, null, null);

        InOutAnalyzer inOutAnalyzer = TapEnv.inOutAnalyzer();
        inOutAnalyzer.setCurUnitEtc(adEnv.curUnit());
        inOutAnalyzer.setFgPhase(InOutAnalyzer.INOUT);
        inOutAnalyzer.analyzeBackward(null, null, null);
        inOutAnalyzer.setFgPhase(InOutAnalyzer.OTHER);
        inOutAnalyzer.setFgSubPhase(InOutAnalyzer.CONSTANT);
        inOutAnalyzer.analyzeBackward(null, null, null);
        inOutAnalyzer.setFgSubPhase(InOutAnalyzer.UNUSED);
        inOutAnalyzer.analyzeBackward(null, null, null);

        inOutAnalyzer.setCurUnitEtc(null);
        tbrAnalyzer.setCurUnitActivity(null);
        tbrAnalyzer.setCurUnitEtc(null);
        TapEnv.printlnOnTrace(20);
    }

    /**
     * Given an info concerning the given frontier, defined on ALLKIND zones,
     * restricts it to the same info but only on SymbolTableConstants.PTRKIND zones.
     */
    private BoolVector restrictToPointers(BoolVector info, TapList<FGArrow> frontier) {
        int commonNDZ = -1;
        int commonNDPZ = -1;
        Block destBlock = frontier.head.destination;
        SymbolTable origST;
        int newndz;
        while (frontier != null) {
            origST = frontier.head.origin.symbolTable;
            newndz = origST.declaredZonesNb(SymbolTableConstants.ALLKIND);
            if (commonNDZ == -1 || commonNDZ > newndz) {
                commonNDZ = newndz;
            }
            newndz = origST.declaredZonesNb(SymbolTableConstants.PTRKIND);
            if (commonNDPZ == -1 || commonNDPZ > newndz) {
                commonNDPZ = newndz;
            }
            frontier = frontier.tail;
        }
        return DataFlowAnalyzer.changeKind(info, commonNDZ, SymbolTableConstants.ALLKIND, commonNDPZ, SymbolTableConstants.PTRKIND, destBlock.symbolTable);
    }

    /**
     * Builds a copy of the given list of FGArrow "entryFrontier", where arrows that
     * come from a Block in "forbiddenOrigins" are removed.
     */
    private TapList<FGArrow> removeCyclingFrom(TapList<FGArrow> entryFrontier, TapList<Block> forbiddenOrigins) {
        TapList<FGArrow> hdResult = new TapList<>(null, null);
        TapList<FGArrow> tlResult = hdResult;
        while (entryFrontier != null) {
            if (!TapList.contains(forbiddenOrigins, entryFrontier.head.origin)) {
                tlResult = tlResult.placdl(entryFrontier.head);
            }
            entryFrontier = entryFrontier.tail;
        }
        return hdResult.tail;
    }

    /**
     * Computes the TapPair &lt;"sbk" ; "snp"&gt; of the "sbk" and "snp" sets used for
     * checkpointing the given "flowGraphPiece", and indicates whether this will
     * require execution of a copy of the "flowGraphPiece".
     * This is used for differentiation of FlowGraphLevel's of kind II_LOOP, CHECKPOINT, or NODIFF.
     * The choice here is to return no ReqC set.
     */
    private TapPair<BoolVector, BoolVector> computeCheckpointingSets(FlowGraphLevel flowGraphPiece,
                                                                     BoolVector[] dataFlowSets7, ToBool needCopy,
                                                                     int kind, int vectorLength) {
        BoolVector req = dataFlowSets7[0];
        BoolVector useCb = dataFlowSets7[1];
        BoolVector outCb = dataFlowSets7[2];
        BoolVector outC = dataFlowSets7[4];
        BoolVector useDb = dataFlowSets7[5];
        BoolVector outDb = dataFlowSets7[6];
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printOnTrace("FOR CHECKPOINTED FLOW-GRAPH PIECE " + flowGraphPiece);
            TapEnv.printlnOnTrace(" with entryArrows:" + flowGraphPiece.entryArrows + " and exitArrows:" + flowGraphPiece.exitArrows + ':');
            ZoneInfo zi;
            for (int i = 0; i<vectorLength; ++i) {
                zi = flowGraphPiece.entryBlock.symbolTable.declaredZoneInfo(i, kind);
                if (zi != null) {
                    TapEnv.printOnTrace(" [" + i + "]"
                            + zi.accessTreePrint(adEnv.curUnit().language()));
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" req  =" + req);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" useCb=" + useCb);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" outCb=" + outCb);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" outC =" + outC);
            TapEnv.printlnOnTrace(" useDb=" + useDb);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" outDb=" + outDb);
        }
        // Sbk  := req & out(Cb)
        BoolVector sbk = req.copy();
        sbk.cumulAnd(outCb);
        // Snp  := (out(C) & (use(Cb) + (req\out(Cb)))) + out(Db) & use(Cb) \ req \ out(C)
        BoolVector tmp2 = outDb.copy();
        tmp2.cumulAnd(useCb);
        tmp2.cumulMinus(req);
        tmp2.cumulMinus(outC);
        BoolVector snp = req.copy();
        snp.cumulMinus(outCb);
        snp.cumulOr(useCb);
        snp.cumulAnd(outC);
        snp.cumulOr(tmp2);
        if (useDb.intersects(outC, vectorLength)) {
            needCopy.set(true);
        } else {
            // make sure snp is empty when the original ckp piece is adjoint dead code:
            snp.setFalse();
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("==>  (needCopy:" + needCopy.get() + ')');
            TapEnv.printlnOnTrace(" sbk  =" + sbk);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" snp  =" + snp);
        }
        return new TapPair<>(sbk, snp);
    }

    /**
     * Computes the TapTriplet &lt;"sbk" ; "snp" ; "sfw"&gt; of the sets
     * used for the binomial checkpointing of the loop headed by "block".
     */
    private TapTriplet<BoolVector, BoolVector, BoolVector> computeBinomialSets(Block block,
                                          BoolVector[] dataFlowSets7, int kind, int vectorLength) {
        BoolVector sbk = new BoolVector(vectorLength);
        BoolVector snp = new BoolVector(vectorLength);
        BoolVector sfw = new BoolVector(vectorLength);
        BoolVector req = dataFlowSets7[0];
        BoolVector useCb = dataFlowSets7[1];
        BoolVector outCb = dataFlowSets7[2];
        BoolVector useC = dataFlowSets7[3];
        BoolVector outC = dataFlowSets7[4];
        BoolVector useDb = dataFlowSets7[5];
        BoolVector outDb = dataFlowSets7[6];
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printOnTrace("FOR BINOMIAL LOOP " + block);
            ZoneInfo zi;
            for (int i=0 ; i<vectorLength ; ++i) {
                zi = block.symbolTable.declaredZoneInfo(i, kind);
                if (zi != null) {
                    TapEnv.printOnTrace(" [" + i + "]"
                            + zi.accessTreePrint(adEnv.curUnit().language()));
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" req  =" + req);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" useCb=" + useCb);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" useC =" + useC);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" outC =" + outC);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" outDb=" + outDb);
        }
        // Define C == AB where A is before the exit test and B after.
        // Sbk := req & (outAB + outDb)   (could be better if the copy of AB was sliced!)
        BoolVector tmp2 = outDb.copy();
        tmp2.cumulOr(outC);
        sbk.setCopy(req);
        sbk.cumulAnd(tmp2);
        // Snp := (useAB + useAbBb) & outAB          (i.e. (useC + useCb) & outC)
        // This Snp is maybe overapproximated. Not all useC is needed, but only
        // what is needed for an "Advance*;Turn" sequence, i.e. for C;C;C;...;Cb
        snp.setCopy(useCb);
        snp.cumulOr(useC);
        snp.cumulAnd(outC);
        // Sfw := useAbBb & outDb \ outAB  (i.e. useCb & outDb \ outC)
        sfw.setCopy(useCb);
        sfw.cumulAnd(outDb);
        sfw.cumulMinus(outC);
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("==>");
            TapEnv.printlnOnTrace(" sbk  =" + sbk);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" snp  =" + snp);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" sfw  =" + sfw);
        }
        return new TapTriplet<>(sbk, snp, sfw);
    }

    private void detectUnusedExitLoopIndices(Block fwdBlock, Block block) {
        Instruction headInstr = block.headInstr();
        if (headInstr != null &&
                headInstr.tree.opCode() == ILLang.op_loop &&
                headInstr.tree.down(3).opCode() == ILLang.op_do) {
            ((HeaderBlock) fwdBlock).loopIndexUnusedAfterLoop =
                    loopIndexUnusedAfterLoop(headInstr.tree.down(3).down(1), block);
        }
    }

    private boolean loopIndexUnusedAfterLoop(Tree loopIndex, Block headerBlock) {
        // If the unusedZones analysis in InOut is not done, answer false!
        if (headerBlock.unusedZones == null) {
            return true;
        }
        TapList<FGArrow> arrows = headerBlock.flow();
        int nDZ = headerBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        FGArrow arrow;
        Block destination;
        SymbolTable commonSymbolTable;
        int commonLength;
        BoolVector exitUnused = new BoolVector(nDZ);
        exitUnused.setTrue();
        while (arrows != null) {
            arrow = arrows.head;
            if (TapIntList.contains(arrow.cases, FGConstants.EXIT)) {
                destination = arrow.destination;
                if (destination.unusedZones != null) {
                    commonSymbolTable = headerBlock.symbolTable.
                            getCommonRoot(destination.symbolTable);
                    commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                    exitUnused.cumulAnd(destination.unusedZones, commonLength);
                }
            }
            arrows = arrows.tail;
        }
        TapIntList indexZones =
                headerBlock.symbolTable.listOfZonesOfValue(loopIndex, null, headerBlock.lastInstr());
        return DataFlowAnalyzer.containsExtendedDeclared(exitUnused, SymbolTableConstants.ALLKIND,
                                                         indexZones, null, headerBlock.symbolTable, null);
    }

    /**
     * Same as mapCheckFreeThenRedirectDestination() but better because takes care of
     * inserting required Blocks for activitySwitches and local declaration Blocks.
     */
    private void connectBwd(TapList<FGArrow> bwdArrows, Block bwdDest,
                            boolean cycle, Block sourceOrig, Block sourceDest,
                            FlowGraphDifferentiationEnv diffEnv) {
        // If necessary, insert a Block that initializes derivatives:
        if (diffEnv.adDiffMode != DiffConstants.TANGENT_MODE) {
            Block diffInitBlock = checkActivitySwitches(sourceOrig, sourceDest,
                                                        bwdDest.symbolTable, false, null, diffEnv, null);
            if (diffInitBlock != null) {
//[8Oct2021] useless?                            diffInitBlock.parallelControls = bwdDest.parallelControls;
                checkFreeThenSetNewFGArrow(diffInitBlock, FGConstants.NO_TEST, 0, bwdDest, cycle);
                cycle = false;
                bwdDest = diffInitBlock;
            }
        }
        SymbolTable origScope = sourceOrig.symbolTable;
        SymbolTable commonScope = origScope.getCommonRoot(sourceDest.symbolTable);
        SymbolTable bwdOrigScope;
        if (origScope != commonScope) {
            bwdOrigScope = origScope.smartCopy(diffEnv.diffCtxt.listST, diffEnv.diffCtxt.unit,
                    adEnv.diffCallGraph(), adEnv.copiedUnits, "Diff of ");
            origScope.cleanCopySymbolDecls();
            if (bwdOrigScope != null && bwdOrigScope.declarationsBlock != null) {
                if (bwdOrigScope.declarationsBlock.flow() == null) {
                    checkFreeThenSetNewFGArrow(bwdOrigScope.declarationsBlock, FGConstants.NO_TEST, 0, bwdDest, cycle);
                    cycle = false;
                    bwdDest = bwdOrigScope.declarationsBlock;
                } else {
                    Instruction headInstr = sourceDest.headInstr();
                    TapEnv.fileWarning(TapEnv.MSG_WARN, headInstr == null ? null : headInstr.tree, "(AD22) Irreducible exits from the same scope in procedure " + adEnv.curUnit().name());
                }
            }
        }
        mapCheckFreeThenRedirectDestination(bwdArrows, bwdDest, cycle);
    }

    /**
     * Place into the bwd sweep (before bwdDest) a chain of blocks that reinitialize derivatives, due to
     * the activity switches that take place in the src code on the way along sourceOrigPath to sourceDest.
     *
     * @return the head of the chain (if any) as the new bwdDest.
     * Use and reset the "cycle" boolean flag if given.
     */
    private Block placeChainOfDiffReinits(TapList<Block> sourceOrigPath, Block sourceDest, Block bwdDest,
                                          FlowGraphDifferentiationEnv diffEnv) {
        Block diffInitBlock, sourceOrig;
        TapList<Block> blocksChain = new TapList<>(sourceDest, TapList.reverse(sourceOrigPath));
        while (blocksChain.tail != null) {
            sourceDest = blocksChain.head;
            sourceOrig = blocksChain.tail.head;
            diffInitBlock = checkActivitySwitches(sourceOrig, sourceDest,
                                                  bwdDest.symbolTable, false, null, diffEnv, null);
            if (diffInitBlock != null) {
//[8Oct2021] useless?                            diffInitBlock.parallelControls = bwdDest.parallelControls;
                checkFreeThenSetNewFGArrow(diffInitBlock, FGConstants.NO_TEST, 0, bwdDest, false);
                bwdDest = diffInitBlock;
            }
            blocksChain = blocksChain.tail;
        }
        return bwdDest;
    }

    private Block buildFwdBlock(String symbolicRk,
                          SymbolTable symbolTable, Block control, FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Build Fwd Block BB\"" + symbolicRk + "\"!");
        }
        Block fwdBlock = new BasicBlock(symbolTable, null, diffEnv.diffCtxtFwd.toAllBlocks);
        fwdBlock.symbolicRk = symbolicRk ;
        fwdBlock.parallelControls = fwdParallelControlsOf(control.parallelControls, diffEnv);
        return fwdBlock ;
    }

    private Block buildBwdBlock(String symbolicRk,
                          SymbolTable symbolTable, Block control, FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Build Bwd Block BB\"" + symbolicRk + "\"!");
        }
        Block bwdBlock = new BasicBlock(symbolTable, null, diffEnv.diffCtxt.toAllBlocks);
        bwdBlock.symbolicRk = symbolicRk ;
        bwdBlock.parallelControls = bwdParallelControlsOf(control.parallelControls, diffEnv);
        return bwdBlock ;
    }

    private HeaderBlock buildFwdHeaderBlock(String symbolicRk,
                                SymbolTable symbolTable, Block control, FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Build Fwd HeaderBlock HB\"" + symbolicRk + "\"!");
        }
        HeaderBlock fwdHeaderBlock = new HeaderBlock(symbolTable, null, diffEnv.diffCtxtFwd.toAllBlocks);
        fwdHeaderBlock.symbolicRk = symbolicRk ;
        fwdHeaderBlock.parallelControls = fwdParallelControlsOf(control.parallelControls, diffEnv);
        return fwdHeaderBlock ;
    }

    private HeaderBlock buildBwdHeaderBlock(String symbolicRk,
                                SymbolTable symbolTable, Block control, FlowGraphDifferentiationEnv diffEnv) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Build Bwd HeaderBlock HB\"" + symbolicRk + "\"!");
        }
        HeaderBlock bwdHeaderBlock = new HeaderBlock(symbolTable, null, diffEnv.diffCtxt.toAllBlocks);
        bwdHeaderBlock.symbolicRk = symbolicRk ;
        bwdHeaderBlock.parallelControls = bwdParallelControlsOf(control.parallelControls, diffEnv);
        return bwdHeaderBlock ;
    }

    private void mapCheckFreeThenRedirectDestination(TapList<FGArrow> arrows, Block newDest, boolean wantToCycle) {
        while (arrows != null) {
            checkFreeThenRedirectDestination(arrows.head, newDest, wantToCycle);
            arrows = arrows.tail;
        }
    }

    private void checkFreeThenRedirectDestination(FGArrow arrow, Block newDest, boolean wantToCycle) {
        // Complain if arrow has already a destination.
        // However, it may happen that redirectDestination is called twice for the same destination
        // (when the original flow splits then merges then splits again). No message then.
        assert arrow != null;
        if (arrow.destination != null && arrow.destination != newDest) {
            TapEnv.toolWarning(-1, "(Differentiation of a Flow Graph) Connecting a Flow Arrow twice! " + arrow + " now to " + newDest);
        }
        // This arrow will be labelled as "cycling" if the given destination "newDest"
        // is a "normal" op_loop that can enclose it.
        boolean inCycle = wantToCycle && newDest.isALoop();
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Connect dangling arrow " + arrow + (inCycle ? " cycling" : "") + " to " + newDest + "!");
        }
        arrow.redirectDestination(newDest, inCycle);
    }

    private FGArrow checkFreeThenSetNewFGArrow(Block origin, int test, int cas, Block destination, boolean inCycle) {
        checkFreeTestCase(origin, test, cas, destination);
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Set " + (destination == null ? "dangling " : "") + "arrow " + origin
                    + " (" + FGArrow.testAndCasesToString(test, new TapIntList(cas, null)) + ')' +
                    (inCycle ? " cycling" : "") + (destination == null ? "" : " to " + destination) + '!');
        }
        return new FGArrow(origin, test, cas, destination, inCycle);
    }

    private FGArrow checkFreeThenSetNewFGArrow(Block origin, int test, TapIntList cases, Block destination, boolean inCycle) {
        TapIntList inCases = cases;
        while (inCases != null) {
            checkFreeTestCase(origin, test, inCases.head, destination);
            inCases = inCases.tail;
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Set " + (destination == null ? "dangling " : "") + "arrow " + origin + " (" + FGArrow.testAndCasesToString(test, cases) + ')' + (inCycle ? " cycling" : "") + (destination == null ? "" : " to " + destination) + '!');
        }
        return new FGArrow(origin, test, cases, destination, inCycle);
    }

    private void checkFreeTestCase(Block origin, int test, int cas, Block destination) {
        FGArrow existingArrow = origin.getFGArrowTestCase(test, cas);
        // If we found only the "DEFAULT' case arrow, and the new "cas" is not DEFAULT, then it's ok:
        if (existingArrow != null && test == FGConstants.CASE && cas != FGConstants.DEFAULT
                && TapIntList.contains(existingArrow.cases, FGConstants.DEFAULT)) {
            existingArrow = null;
        }
        if (existingArrow != null && existingArrow.destination != null && existingArrow.destination != destination) {
            TapEnv.toolError("(Differentiation of a Flow Graph) Connecting a block destination twice! " + existingArrow + " now to " + destination);
        }
    }

    private void insertBlockBefore(Block block, Block newBlock) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Insert block " + newBlock + " before " + block + "!");
        }
        block.insertBlockBefore(newBlock);
    }

    private void insertBlockAtOrig(FGArrow arrow, Block newBlock, int test, int cas) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Insert block " + newBlock + " (" + FGArrow.testAndCasesToString(test, new TapIntList(cas, null)) + ") in the middle of " + arrow + "!");
        }
        arrow.insertBlockAtOrig(newBlock, test, cas);
    }

    private FGArrow insertBlockAtDest(FGArrow arrow, Block newBlock, int test, int cas) {
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("!Insert block " + newBlock + " (" + FGArrow.testAndCasesToString(test, new TapIntList(cas, null)) + ") in the middle of " + arrow + "!");
        }
        return arrow.insertBlockAtDest(newBlock, test, cas);
    }

    /**
     * @return the list of enclosing loops of the given "block" that will
     * "separate" block_FWD and block_BWD, i.e. block_FWD and block_BWD
     * will be each in its own separate nest of these loops, instead of
     * both inside the same loop nest (which may happen e.g. if we reach an
     * enclosing level which is checkpointed).
     * Returned list is ordered, outermost loop first.
     * Special case: when we reach the topmost FlowGraphLevel without having seen
     * any checkpoint, then the returned list starts with a null.
     */
    protected TapList<LoopBlock> getIterationsBetweenPushAndPop(Block block) {
        TapList<LoopBlock> result = null;
        FlowGraphLevel blockLevel = null;
        if (block instanceof LoopBlock) {
            block = ((LoopBlock) block).header();
        }
        if (additionalBlockToPlainLevel != null) {
            blockLevel = TapList.cassq(block, additionalBlockToPlainLevel);
        }
        if (block.rank >= 0 && blockLevel == null) {
            blockLevel = blockToPlainLevel.retrieve(block);
        }
        while (blockLevel != null) {
            LoopBlock loopAround;
            if (blockLevel.levelKind == DiffConstants.PLAIN_BLOCK) {
                loopAround = block.enclosingLoop();
            } else {
                loopAround = blockLevel.entryBlock.enclosingLoop().enclosingLoop();
            }
            blockLevel = blockLevel.trueEnclosing;
            // Recuperate the levels of "WILD_LOOP" that are not represented in the Tree of FlowGraphLevel's:
            // Walk up the LoopBlock's that enclose block,
            // until reaching a LoopBlock that corresponds to the current FlowGraphLevel blockLevel:
            while (!(loopAround == null ||
                    blockLevel != null &&
                            blockLevel.entryPoint.entryBlock == loopAround.header())) {
                result = new TapList<>(loopAround, result);
                loopAround = loopAround.enclosingLoop();
            }
            if (blockLevel != null) {
                switch (blockLevel.levelKind) {
                    case DiffConstants.NODIFF:
                    case DiffConstants.PLAIN_BLOCK:
                    case DiffConstants.PLAIN_GROUP:
                        // these cases above should never happen.
                        break;
                    case DiffConstants.TOP_LEVEL:
                        result = new TapList<>(null, result);
                        blockLevel = null;
                        break;
                    case DiffConstants.CHECKPOINT:
                    case DiffConstants.II_LOOP:
                    case DiffConstants.BINOMIAL_LOOP:
                        // in these cases, we have a checkpoint, therefore surrounding loops don't count.
                        blockLevel = null;
                        break;
                    default:
                        // all remaining cases are loop levels:
                        result = new TapList<>(blockLevel.entryBlock.enclosingLoop(), result);
                        break;
                }
            }
        }
        return result;
    }

// Small utilities and local classes:

    /**
     * This class is used to designate/contain a piece
     * of a Flow Graph that can be adjoint-differentiated independently.
     * These pieces are organized as nested "levels", i.e. this class is recursive.
     * We require each level has a single entry Block.
     * We don't require a unique exit Block because not necessary in general.
     */
    private final class FlowGraphLevel {
        /**
         * The kind of this FlowGraphLevel. Belongs to:
         * {TOP_LEVEL, PLAIN_BLOCK, CHECKPOINT, NODIFF, NATURAL_LOOP,
         * BINOMIAL_LOOP, TIMES_LOOP, WHILE_LOOP, DO_LOOP, II_LOOP,
         * FIXEDPOINT_LOOP, MULTITHREAD_REGION}.
         */
        public int levelKind = DiffConstants.TOP_LEVEL;
        /**
         * The entry point of this FlowGraphLevel. It is the first
         * sub-FlowGraphLevel which is executed in this FlowGraphLevel.
         * If this FlowGraphLevel is of kind "PLAIN_BLOCK", entryPoint is null,
         * If this FlowGraphLevel is of kind "TOP_LEVEL",
         * entryPoint is a PLAIN_BLOCK containing the unit's EntryBlock.
         */
        private FlowGraphLevel entryPoint;
        /**
         * The 1st Block of (recursively the 1st Block of...) this FlowGraphLevel.
         */
        private Block entryBlock;
        /**
         * Sublist of the arrows flowing to the entryPoint that must be
         * considered as OUTSIDE the level, i.e. entering this level.
         */
        private TapList<FGArrow> entryArrows;
        /**
         * The exits from this Flow Graph level, i.e. all arrows that go outside.
         * When this level is generated, they may point to null temporarily.
         * Often these arrows will go to the same place, but not necessarily.
         */
        private TapList<FGArrow> exitArrows;
        /**
         * The other contents of this Flow Graph level. Doesn't contain the entryPoint.
         * Elements are other FlowGraphLevel's.
         * If this FlowGraphLevel is of kind "PLAIN_BLOCK", contents is null,
         */
        private TapList<FlowGraphLevel> contents;
        /**
         * The FlowGraphLevel that immediately encloses this one.
         */
        private FlowGraphLevel enclosing;
        /**
         * The enclosing FlowGraphLevel, i.e. in general the same as "enclosing",
         * but this one is never modified (whereas "enclosing" may be modified
         * temporarily by a createNoCycle() or a createLoopBody().
         */
        private FlowGraphLevel trueEnclosing;
        /**
         * When non-null, contains the header of this FlowGraphLevel
         * when its fwd diff Block must be a plain Block.
         */
        private HeaderBlock headerBlockPlain;

        /** When this level comes from a directive with a label, this label. */
        public String label ;

        /** A memo of already created RefDescriptor's for the static save
         * variables (e.g. "ad_save" or "ad_branch") created during adjoint
         * of this FlowGraphLevel. This memo is used (and therefore non-null)
         * only for the adjoint of fixed-point loops, to reuse the same save
         * variables in both backward sweeps. It is a triplet of: <ul>
         * <li> a Boolean (FALSE:storing, TRUE:reusing),</li>
         * <li> the (hatted) ordered list of RefDescriptor's created,</li>
         * <li> some (hatted) pointer into this ordered list.</li></ul> */
        public TapTriplet<Boolean, TapList<RefDescriptor>, TapList<RefDescriptor>>
            staticSaves = null ;

        private FlowGraphLevel(int levelKind) {
            super();
            this.levelKind = levelKind;
        }

        /**
         * Create a new (temporary) FlowGraphLevel that is the same as
         * this FlowGraphLevel, except that the cycling arrows are
         * considered out of the new FlowGraphLevel.
         */
        private FlowGraphLevel createNoCycle() {
            FlowGraphLevel result =
                    new FlowGraphLevel(DiffConstants.PLAIN_GROUP);
            result.entryPoint = entryPoint;
            entryPoint.enclosing = result;
            result.entryBlock = entryBlock;
            TapList<FGArrow> levelCycleArrows = null;
            TapList<FGArrow> entryEntryArrows = entryPoint.entryArrows;
            while (entryEntryArrows != null) {
                if (!TapList.contains(entryArrows, entryEntryArrows.head)) {
                    levelCycleArrows = new TapList<>(entryEntryArrows.head, levelCycleArrows);
                }
                entryEntryArrows = entryEntryArrows.tail;
            }
            result.entryArrows = TapList.append(levelCycleArrows, entryArrows);
            result.exitArrows = TapList.append(levelCycleArrows, exitArrows);
            result.contents = contents;
            TapList<FlowGraphLevel> inContents = contents;
            while (inContents != null) {
                inContents.head.enclosing = result;
                inContents = inContents.tail;
            }
            result.enclosing = this;
            return result;
        }

        /**
         * Create a new (temporary) FlowGraphLevel that is the loop body part of
         * this FlowGraphLevel, which must be a normal do-like loop.
         */
        private FlowGraphLevel createLoopBody() {
            // createLoopBody() can be called only on a FlowGraphLevel with a unique
            // LOOP:NEXT arrow, and therefore a unique entry point into the loop body:
            FGArrow arrowToBody = entryBlock.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT);
            FlowGraphLevel newEntryPoint =
                    getRepresentantInLevel(arrowToBody.destination);
            FlowGraphLevel result = createNoCycle();
            result.entryPoint.enclosing = null;
            result.entryBlock = arrowToBody.destination;
            result.entryPoint = newEntryPoint;
            result.entryArrows = new TapList<>(arrowToBody, null);
            TapList<FGArrow> hdNewExitArrows = new TapList<>(null, null);
            TapList<FGArrow> tlNewExitArrows = hdNewExitArrows;
            TapList<FGArrow> inExitArrows = result.exitArrows;
            while (inExitArrows != null) {
                if (inExitArrows.head.origin != entryBlock) {
                    tlNewExitArrows = tlNewExitArrows.placdl(inExitArrows.head);
                }
                inExitArrows = inExitArrows.tail;
            }
            result.exitArrows = hdNewExitArrows.tail;
            TapList<FlowGraphLevel> hdNewContents = new TapList<>(null, null);
            TapList<FlowGraphLevel> tlNewContents = hdNewContents;
            TapList<FlowGraphLevel> inContents = contents;
            while (inContents != null) {
                if (inContents.head != newEntryPoint) {
                    tlNewContents = tlNewContents.placdl(inContents.head);
                }
                inContents = inContents.tail;
            }
            result.contents = hdNewContents.tail;
            return result;
        }

        /**
         * Create a new (temporary) FlowGraphLevel that is the same as
         * this FlowGraphLevel, except that the entryBlock is taken out
         * and the unique destination of entryBlock is the new entryBlock.
         */
        private FlowGraphLevel createRegionBody() {
            FGArrow arrowToBody = entryBlock.flow().head;
            FlowGraphLevel newEntryPoint =
                    getRepresentantInLevel(arrowToBody.destination);
            FlowGraphLevel result = new FlowGraphLevel(DiffConstants.PLAIN_GROUP);
            result.entryPoint = newEntryPoint;
            assert newEntryPoint != null;
            newEntryPoint.enclosing = result;
            result.entryBlock = arrowToBody.destination;
            result.entryPoint = newEntryPoint;
            result.entryArrows = new TapList<>(arrowToBody, null);
            result.exitArrows = TapList.append(exitArrows, null); // i.e. a 1st level copy
            TapList<FlowGraphLevel> hdNewContents = new TapList<>(null, null);
            TapList<FlowGraphLevel> tlNewContents = hdNewContents;
            TapList<FlowGraphLevel> inContents = contents;
            while (inContents != null) {
                inContents.head.enclosing = result;
                if (inContents.head != newEntryPoint) {
                    tlNewContents = tlNewContents.placdl(inContents.head);
                }
                inContents = inContents.tail;
            }
            result.contents = hdNewContents.tail;
            result.enclosing = this;
            return result;
        }


        /**
         * Restores the correct parenthood links in the given FlowGraphLevel.
         * This parenthood may have beed damaged when creating a new temporary
         * FlowGraphLevel using createNoCycle() or createLoopBody().
         */
        private void restoreChildrenParent() {
            entryPoint.enclosing = this;
            TapList<FlowGraphLevel> inContents = contents;
            while (inContents != null) {
                inContents.head.enclosing = this;
                inContents = inContents.tail;
            }
        }

        /**
         * @return the FlowGraphLevel that contains "block" and which
         * is a direct child of this FlowGraphLevel,
         * or null if "block" is not contained in this FlowGraphLevel.
         */
        private FlowGraphLevel getRepresentantInLevel(Block block) {
            FlowGraphLevel blockLevel = null;
            if (additionalBlockToPlainLevel != null) {
                blockLevel = TapList.cassq(block, additionalBlockToPlainLevel);
            }
            if (blockLevel == null) {
                blockLevel = blockToPlainLevel.retrieve(block);
            }
            while (blockLevel != null && blockLevel.enclosing != this) {
                blockLevel = blockLevel.enclosing;
            }
            return blockLevel;
        }

        /**
         * Builds the flat, ordered TapList of all the Blocks recursively
         * contained in this FlowGraphLevel.
         */
        private TapList<Block> allBlocksInside() {
            TapList<Block> toResult = new TapList<>(null, null);
            abiRec(toResult);
            return toResult.tail;
        }

        /**
         * Recursive phase of allBlocksInside().
         */
        private TapList<Block> abiRec(TapList<Block> tlResult) {
            if (levelKind == DiffConstants.PLAIN_BLOCK) {
                return tlResult.placdl(entryBlock);
            } else {
                tlResult = entryPoint.abiRec(tlResult);
                TapList<FlowGraphLevel> inContents = contents;
                while (inContents != null) {
                    tlResult = inContents.head.abiRec(tlResult);
                    inContents = inContents.tail;
                }
                return tlResult;
            }
        }

        private boolean hasCyclingArrows() {
            TapList<FGArrow> entryEntries =
                    entryPoint != null ? entryPoint.entryArrows : entryBlock.backFlow();
            boolean hasCyclingArrow = false;
            while (entryEntries != null && !hasCyclingArrow) {
                if (!TapList.contains(entryArrows, entryEntries.head)) {
                    hasCyclingArrow = true;
                }
                entryEntries = entryEntries.tail;
            }
            return hasCyclingArrow;
        }

        /**
         * @return true iff the arrow remains inside this FlowGraphLevel,
         * and does not go "out-then-back-into" this FlowGraphLevel.
         * This function assumes that the arrow.destination is in this FlowGraphLevel.
         */
        private boolean containsArrowFlow(FGArrow arrow) {
            return !TapList.contains(entryArrows, arrow);
        }

        private int getSubLevelRk(FlowGraphLevel subLevel) {
            if (subLevel == entryPoint) {
                return 0;
            } else {
                int ifound = -1;
                int i = 1;
                TapList<FlowGraphLevel> inContents = contents;
                while (inContents != null && ifound == -1) {
                    if (inContents.head == subLevel) {
                        ifound = i;
                    }
                    ++i;
                    inContents = inContents.tail;
                }
                return ifound;
            }
        }

        @Override
        public String toString() {
            if (trueEnclosing == null) {
                return "Top"; //+" @"+Integer.toHexString(hashCode());
            } else {
                int rank = trueEnclosing.getSubLevelRk(this);
                return trueEnclosing.toString() + "." + (rank == -1 ? "?" : rank); //+" @"+Integer.toHexString(hashCode());
            }
        }

    }

    /**
     * This class holds all that is necessary to differentiate a FlowGraphLevel.
     */
    private final class FlowGraphDifferentiationEnv {
        /**
         * The context to build the diff unit, that will contain the bwd sweep.
         * If mode is JOINT, then this will contain the fwd sweep as well.
         */
        private UnitCreationContext diffCtxt;
        /**
         * The context to build the fwd diff unit, that will contain the fwd sweep only.
         * If mode is JOINT, then "==equals" diffCtxt.
         */
        private UnitCreationContext diffCtxtFwd;
        /**
         * Current differentiation mode, in {TANGENT_MODE, ADJOINT_MODE, ADJOINT_SPLIT_MODE, ?ADOLC_MODE}.
         */
        private int adDiffMode;
        /**
         * The list of FGArrow's for which we are sure there will be a turn: Fwd to Bwd sweep.
         */
        private TapList<FGArrow> turningArrows;
        /**
         * The list of all internal symbolTables of curUnit whose scope
         * ends before exitBlock.
         */
        private TapList<SymbolTable> middleSymbolTables;

        /**
         * For each Block, when true =&gt; there must be a Fwd diff block, even empty.
         */
        private final boolean[] fwdLiveControl;
        /**
         * For each Block, when true =&gt; there must be a Bwd diff block, even empty.
         */
        private boolean[] bwdLiveControl;
        /**
         * For each Block, when true =&gt; the last test of the Block is live, i.e. the Fwd diff is also a test.
         */
        private final boolean[] lastTestLives;
        /**
         * For each Block, its fwdDiff Block.
         */
        private final BlockStorage<Block> fwdDiffBlocks;
        /**
         * For each Block, its bwdDiff Block.
         */
        private final BlockStorage<Block> bwdDiffBlocks;

        /**
         * For each source Block, the parallelControls of its
         * corresponding Block in the fwd/bwd sweep.
         * In other words, the corresponding parallelControls that must be shared
         * by all Blocks at a mirroring location in the fwd/bwd sweep.
         */
        private BlockStorage<TapList<Instruction>> fwdParallelControls;
        private BlockStorage<TapList<Instruction>> bwdParallelControls;

        /**
         * Build a new differentiation environment to reverse a FlowGraphLevel.
         */
        private FlowGraphDifferentiationEnv(int adDiffMode) {
            super();
            this.adDiffMode = adDiffMode;
            fwdLiveControl = new boolean[adEnv.curUnit().nbBlocks];
            if (adDiffMode != DiffConstants.TANGENT_MODE) {
                bwdLiveControl = new boolean[adEnv.curUnit().nbBlocks];
            }
            lastTestLives = new boolean[adEnv.curUnit().nbBlocks];
            for (int i = adEnv.curUnit().nbBlocks - 1; i >= 0; --i) {
                fwdLiveControl[i] = false;
                if (adDiffMode != DiffConstants.TANGENT_MODE) {
                    bwdLiveControl[i] = false;
                }
                lastTestLives[i] = false;
            }
            fwdDiffBlocks = new BlockStorage<>(adEnv.curUnit());
            bwdDiffBlocks = new BlockStorage<>(adEnv.curUnit());
            fwdParallelControls = new BlockStorage<>(adEnv.curUnit());
            bwdParallelControls = new BlockStorage<>(adEnv.curUnit());
        }

        /**
         * Builds the initial differentiation environment to differentiate curUnit into "curDiffUnit"
         * and into "curFwdDiffUnit" when necessary (i.e. split adjoint mode).
         */
        private void initialize() {
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("  Initialize SymbolTable's associations for diff unit "+curDiffUnit()+" of "+adEnv.curUnit()+" :") ;
                TapEnv.printlnOnTrace("   "+adEnv.curUnit().privateSymbolTable()) ;
                TapEnv.printlnOnTrace("   ==fwd==>> "+curFwdDiffUnit().privateSymbolTable()) ;
                TapEnv.printlnOnTrace("   =======>> "+curDiffUnit().privateSymbolTable()) ;
                TapEnv.printlnOnTrace("   "+adEnv.curUnit().publicSymbolTable()) ;
                TapEnv.printlnOnTrace("   ==fwd==>> "+curFwdDiffUnit().publicSymbolTable()) ;
                TapEnv.printlnOnTrace("   =======>> "+curDiffUnit().publicSymbolTable()) ;
            }
            TapList<Block> toAllDiffBlocks = new TapList<>(null, null);
            diffCtxt =
                    new UnitCreationContext(curDiffUnit(),
                            toAllDiffBlocks,
                            new TapList<>(null,
                                    new TapList<>(new TapPair<>(adEnv.curUnit().privateSymbolTable(),
                                            curDiffUnit().privateSymbolTable()),
                                            new TapList<>(new TapPair<>(adEnv.curUnit().publicSymbolTable(),
                                                    curDiffUnit().publicSymbolTable()),
                                                    null))));
            diffCtxtFwd =
                    new UnitCreationContext(curFwdDiffUnit(),
                            adDiffMode != DiffConstants.ADJOINT_SPLIT_MODE ? toAllDiffBlocks : new TapList<>(null, null),
                            new TapList<>(null,
                                    new TapList<>(new TapPair<>(adEnv.curUnit().privateSymbolTable(),
                                            curFwdDiffUnit().privateSymbolTable()),
                                            new TapList<>(new TapPair<>(adEnv.curUnit().publicSymbolTable(),
                                                    curFwdDiffUnit().publicSymbolTable()),
                                                    null))));
        }

        /**
         * From this FlowGraphDifferentiationEnv, derive a new FlowGraphDifferentiationEnv
         * in order to run a new differentiation of a sub-FlowGraphLevel,
         * checkpointed, i.e. in a nested JOINT mode.
         */
        private FlowGraphDifferentiationEnv deriveForNewJointDiff() {
            FlowGraphDifferentiationEnv result =
                    new FlowGraphDifferentiationEnv(DiffConstants.ADJOINT_MODE);
            // When one builds a new FlowGraphDifferentiationEnv for a new differentiation of some
            // FlowGraphLevel, the 2 differentiations of this FlowGraphLevel must create
            // different differentiated SymbolTables. Therefore, they must not
            // share the "SymbolTable correspondence" part of the diffCtxt.
            result.diffCtxt = UnitCreationContext.copyListST(diffCtxt);
            result.diffCtxtFwd = result.diffCtxt;
            return result;
        }

        /**
         * From this FlowGraphDifferentiationEnv, derive a new FlowGraphDifferentiationEnv
         * in order to run a new differentiation of a sub-FlowGraphLevel,
         * NOT checkpointed, i.e. in ADJOINT_SPLIT_MODE.
         */
        private FlowGraphDifferentiationEnv deriveForNewSplitDiff() {
            FlowGraphDifferentiationEnv result =
                    new FlowGraphDifferentiationEnv(DiffConstants.ADJOINT_SPLIT_MODE);
            // When one builds a new FlowGraphDifferentiationEnv for a new differentiation of some
            // FlowGraphLevel, the 2 differentiations of this FlowGraphLevel must create
            // different differentiated SymbolTables. Therefore, they must not
            // share the "SymbolTable correspondence" part of the diffCtxt.
            result.diffCtxt = UnitCreationContext.copyListST(diffCtxt);
            result.diffCtxtFwd = UnitCreationContext.copyListST(diffCtxtFwd);
            return result;
        }

        private void storeDiffBlocks(Block block, Block fwdDiffBlock, Block bwdDiffBlock, boolean lastTestLive) {
            if (fwdDiffBlock.instructions != null) {
                if (block.rank >= 0) {
                    fwdLiveControl[block.rank] = true;
                }
                markControllersLive(block, fwdLiveControl);
            }
            if (adDiffMode != DiffConstants.TANGENT_MODE) {
                if (bwdDiffBlock.instructions != null) {
                    if (block.rank >= 0) {
                        bwdLiveControl[block.rank] = true;
                    }
                    markControllersLive(block, bwdLiveControl);
                } else {
                    markLiveDueToDiffReinits(block);
                }
            }
            fwdDiffBlocks.store(block, fwdDiffBlock);
            bwdDiffBlocks.store(block, bwdDiffBlock);
            if (block.rank >= 0) {
                lastTestLives[block.rank] = lastTestLive;
            }
        }

        private Block retrieveOrigBlockOf(Block diffBlock) {
            TapList<Block> origBlocks =
                    new TapList<>(adEnv.curUnit().entryBlock(), new TapList<>(adEnv.curUnit().exitBlock(), adEnv.curUnit().allBlocks()));
            Block found = null;
            while (found == null && origBlocks != null) {
                if (fwdDiffBlocks.retrieve(origBlocks.head) == diffBlock
                        || bwdDiffBlocks.retrieve(origBlocks.head) == diffBlock) {
                    found = origBlocks.head;
                }
                origBlocks = origBlocks.tail;
            }
            return found;
        }

        private void markBwdControllersLive(Block block) {
            if (adDiffMode != DiffConstants.TANGENT_MODE) {
                markControllersLive(block, bwdLiveControl);
            }
        }

        /**
         * Mark "block" and all Blocks that control it as live.
         * This notion of liveness may regard the fwd sweep or
         * the bwd sweep, according to the provided "liveControl".
         * Also, when the marked block is an exit from a loop
         * that does not leave from its HeaderBlock, recursively
         * mark the HeaderBlock too, as well as all its controllers.
         * [llh] TODO: maybe this comes from a weakness of the
         * computation of controllers: the header of a "wild" or
         * "natural" loop should be a controller of the loop body ?
         */
        private void markControllersLive(Block block, boolean[] liveControl) {
            TapList<Block> controllers = block.controllers;
            LoopBlock enclosingLoop = block.enclosingLoop();
            Block controller;
            if (enclosingLoop != null) {
                controllers = new TapList<>(enclosingLoop.header(), controllers);
            }
            while (controllers != null) {
                controller = controllers.head;
                if (!liveControl[controller.rank]) {
                    liveControl[controller.rank] = true;
                    markControllersLive(controller, liveControl);
                }
                controllers = controllers.tail;
            }
        }

        /**
         * Dirty code by llh. Not sure it is correct. Tries to solve the
         * conflict F77:lh38 vs F77:lha25 between insertion of
         * reinitialization of diff variables and elimination of
         * superfluous taped control.
         */
        private void markLiveDueToDiffReinits(Block block) {
            TapList<FGArrow> arrows = block.backFlow();
            if (arrows != null) {
                boolean onlyArrow = arrows.tail == null;
                FGArrow arrow;
                while (arrows != null) {
                    arrow = arrows.head;
                    if (mustCheckActivitySwitches(arrow.origin, arrow.destination)) {
                        if (onlyArrow) {
                            bwdLiveControl[block.rank] = true;
                        }
                        markControllersLive(block, bwdLiveControl);
                    }
                    arrows = arrows.tail;
                }
            }
        }

        private boolean blockMustBeLive(Block block) {
            return block.rank >= 0 && fwdLiveControl[block.rank];
        }

        private boolean blockMustBeActive(Block block) {
            return block.rank >= 0 && bwdLiveControl[block.rank];
        }

        private boolean blockHasLastTestLive(Block block) {
            return block.rank >= 0 && lastTestLives[block.rank];
        }

    }

    /**
     * This class holds the result of a reverse differentiation, i.e. the handles
     * to the fwd and bwd sweeps created by differentiateFlowGraphLevel().
     */
    private static final class FlowGraphDifferentiation {

        /**
         * The unique UpStream (i.e. first/entry) Block of the diff fwd sweep.
         */
        private Block fwdBlockUS;
        /**
         * The (generally unique, but...) FGArrow's exiting from the diff bwd sweep to the outside.
         */
        private TapList<FGArrow> bwdArrowsUS;
        /**
         * When false, a null in fwdBlockUS;bwdArrowsUS means not known yet;
         * when true, a null in fwdBlockUS;bwdArrowsUS means really null info.
         */
        private boolean upstreamFwdBwdDefined;

        /**
         * The association from each source FGArrow in the exitArrows,
         * to its related connection handles in the diff fwd and bwd sweeps.
         * This is a TapList (called a "arrowsFwdBwds") of TapPair's (each of them called a "arrowFwdBwds").
         * Each "arrowFwdBwds" pair associates<br>
         * -1- one source exitArrow<br>
         * -2- with a TapList (called a "manyFwdBwds") of TapPair's (each of them called a "oneFwdBwd").<br>
         * Each "oneFwdBwd" pair associates:<br>
         * -1- a FGArrow, that we call a "danglingFwd", which is the
         * FGArrow of the fwd sweep that exit the fwd differentiated set of Blocks
         * and that must be connected to the head of the next fwd diff code.<br>
         * -2- with a TapList (called a "bwdSwitchCases") of BwdSwitchCase's,
         * each of them corresponding to an exit control that must be treated separately
         * by a PUSH/POP of this control in order to organize the control of the bwd sweep.<br>
         * Each BwdSwitchCase consists of:<br>
         * -1- a FGArrow of the fwd sweep, that may have to jump to the PUSH,<br>
         * -2- a Block of the bwd sweep, that may have to be reached following the test on POP,<br>
         * -3- a Block of the original source, from where starts the current switch case,<br>
         * -4- a Block of the original source, to where goes the current switch case.
         */
        private TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> exitArrowsFwdBwds;

        /**
         * When the FlowGraphLevel has an internal cycle, the FGArrow flowing from the
         * special Fwd block that gathers the entry arrows.
         */
        private FGArrow specialTopFwdArrow;
        /**
         * When the FlowGraphLevel has an internal cycle, the FGArrow flowing from the
         * special Bwd block that gathers all exits from the bwd diff level.
         */
        private FGArrow specialTopBwdArrow;
        /**
         * True when the specialTopFwdArrow and specialTopBwdArrow have been used in
         * exploreConvergingBackFlow(). Used to make sure they are not used twice.
         */
        private boolean specialTopUsed;

        private FlowGraphDifferentiation(FlowGraphLevel level) {
            super();
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> hdExitArrowsFwdBwds =
                    new TapList<>(null, null);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> tlExitArrowsFwdBwds =
                    hdExitArrowsFwdBwds;
            TapList<FGArrow> exitArrows = level.exitArrows;
            while (exitArrows != null) {
                tlExitArrowsFwdBwds = tlExitArrowsFwdBwds.placdl(new TapPair<>(exitArrows.head, null));
                exitArrows = exitArrows.tail;
            }
            exitArrowsFwdBwds = hdExitArrowsFwdBwds.tail;
        }

        private boolean hasFwd() {
            return fwdBlockUS != null;
        }

        /**
         * Tests whether the forward sweep in this FlowGraphDifferentiation is either empty
         * or is made only of empty Blocks, so that it will be empty after simplifications
         * done by the final makeFlowGraph().
         */
        private boolean reallyHasFwd() {
            if (fwdBlockUS == null) {
                return false;
            }
            return hasCodeAfter(fwdBlockUS, new TapList<>(null, null));
        }

        /**
         * Utility: sweeps through all Blocks reachable from "block".
         *
         * @return true as soon as some code is found (that will remain after simplifications).
         */
        private boolean hasCodeAfter(Block block, TapList<Block> toDejaVu) {
            if (block == null || TapList.contains(toDejaVu.tail, block)) {
                return false;
            }
            if (block instanceof HeaderBlock) {
                if (!block.isDoWithIndexUnusedAfterLoop()) {
                    return true;
                }
            } else {
                if (block.instructions != null) {
                    return true;
                }
            }
            toDejaVu.placdl(block);
            boolean hasCode = false;
            for (TapList<FGArrow> inFlow = block.flow(); inFlow != null && !hasCode; inFlow = inFlow.tail) {
                hasCode = hasCodeAfter(inFlow.head.destination, toDejaVu);
            }
            return hasCode;
        }

        private boolean hasBwd() {
            return bwdArrowsUS != null;

// [llh] Unfinished attempt of a more accurate hasBwd(), see e.g. set01/lh006.
// Maybe try use hasCodeAfter() instead.
//             if (bwdArrowUS==null) return false ;
//             boolean emptyReversalBwdUS =
//                 bwdArrowUS.origin.instructions==null ;
//             boolean doesSomeBwd = false ;
//             TapList oneFwdBwd, bwdSwitchCases ;
//             Block popPlace ;
//             TapList<TapPair> inExitArrowsFwdBwds = exitArrowsFwdBwds ;
//             while (inExitArrowsFwdBwds!=null && !doesSomeBwd) {
//                 oneFwdBwd = (TapList)inExitArrowsFwdBwds.head.second ;
//                 while (oneFwdBwd!=null) {
//                     bwdSwitchCases = (TapList)((TapPair)oneFwdBwd.head).second ;
//                     while (bwdSwitchCases!=null) {
//                         popPlace = (Block)((BwdSwitchCase)bwdSwitchCases.head).second ;
//                         if (!emptyReversalBwdUS || popPlace!=bwdArrowUS.origin)
//                             doesSomeBwd = true ;
//                         bwdSwitchCases = bwdSwitchCases.tail ;
//                     }
//                     oneFwdBwd = oneFwdBwd.tail ;
//                 }
//                 inExitArrowsFwdBwds = inExitArrowsFwdBwds.tail ;
//             }
//             return doesSomeBwd ;
        }

        private TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> getFwdBwdsOfExitArrow(FGArrow exitArrow) {
            return TapList.cassq(exitArrow, exitArrowsFwdBwds);
        }

        private void accumulateFwdBwdUS(Block newFwdBlockUS, TapList<FGArrow> newBwdArrowsUS) {
            if (!upstreamFwdBwdDefined) {
                upstreamFwdBwdDefined = true;
                fwdBlockUS = newFwdBlockUS;
                bwdArrowsUS = newBwdArrowsUS;
            } else {
                if (newFwdBlockUS != fwdBlockUS) {
                    TapEnv.toolWarning(-1, "(Differentiation of a Flow Graph) FlowGraphLevel with two Fwd entries");
                    if (newFwdBlockUS != null) {
                        fwdBlockUS = newFwdBlockUS;
                    }
                }
                while (newBwdArrowsUS != null) {
                    if (!TapList.contains(bwdArrowsUS, newBwdArrowsUS.head)) {
                        bwdArrowsUS = new TapList<>(newBwdArrowsUS.head, bwdArrowsUS);
                    }
                    newBwdArrowsUS = newBwdArrowsUS.tail;
                }
            }
        }

        private void removeLoopExitArrow() {
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> toExitArrowsFwdBwds =
                    new TapList<>(null, exitArrowsFwdBwds);
            TapList<TapPair<FGArrow, TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>>>> inExitArrowsFwdBwds = toExitArrowsFwdBwds;
            FGArrow exitArrow;
            while (inExitArrowsFwdBwds.tail != null) {
                exitArrow = inExitArrowsFwdBwds.tail.head.first;
                if (exitArrow.containsCase(FGConstants.EXIT)) {
                    inExitArrowsFwdBwds.tail = inExitArrowsFwdBwds.tail.tail;
                } else {
                    inExitArrowsFwdBwds = inExitArrowsFwdBwds.tail;
                }
            }
            exitArrowsFwdBwds = toExitArrowsFwdBwds.tail;
        }

    }

    /**
     * This class holds all that is necessary to create a new copy
     * of a FlowGraphLevel, at a given level in the nest of differentiated
     * Flow Graph levels.
     */
    private static final class FlowGraphCopyEnv {
        /**
         * The context to build the copied FlowGraphLevel.
         */
        private UnitCreationContext copyCtxt;

        private FlowGraphCopyEnv(UnitCreationContext copyCtxt) {
            super();
            // When one builds a new FlowGraphCopyEnv for a new copy of some
            // FlowGraphLevel, the new copy must create its own local copied
            // SymbolTables for the scopes inside the FlowGraphLevel. Therefore, they must not
            // share the "SymbolTable correspondence" part of the diffCtxt.
            this.copyCtxt = UnitCreationContext.copyListST(copyCtxt);
        }

    }

    /**
     * This class holds the handles to the copy of a FlowGraphLevel.
     * Objects of this class are created and filled by copyFlowGraphLevel().
     */
    private static final class FlowGraphCopy {
        /**
         * The copy of the entry (i.e. upstream) Block.
         */
        private Block copyBlockUS;
        /**
         * The copies of each exiting (i.e. downstream) FGArrow.
         */
        private TapList<TapPair<FGArrow, FGArrow>> exitArrowsCopy;

        private FlowGraphCopy(FlowGraphLevel level) {
            super();
            TapList<TapPair<FGArrow, FGArrow>> hdExitArrowsCopy =
                    new TapList<>(null, null);
            TapList<TapPair<FGArrow, FGArrow>> tlExitArrowsCopy = hdExitArrowsCopy;
            TapList<FGArrow> exitArrows = level.exitArrows;
            while (exitArrows != null) {
                tlExitArrowsCopy = tlExitArrowsCopy.placdl(new TapPair<>(exitArrows.head, null));
                exitArrows = exitArrows.tail;
            }
            exitArrowsCopy = hdExitArrowsCopy.tail;
        }

        private FGArrow getCopyOfExitArrow(FGArrow exitArrow) {
            return TapList.cassq(exitArrow, exitArrowsCopy);
        }
    }

    /**
     * One BwdSwitchCase stores one case of the backwards control switch
     * to go back to one possible origin of a join in the source code.
     */
    private static final class BwdSwitchCase {
        private FGArrow fwdArrow;
        private Block bwdBlock;
        private TapList<Block> sourceOrigPath;
        private Block sourceDest;

        private BwdSwitchCase(FGArrow fwdArrow, Block bwdBlock, TapList<Block> sourceOrigPath, Block sourceDest) {
            super();
            this.fwdArrow = fwdArrow;
            this.bwdBlock = bwdBlock;
            this.sourceOrigPath = sourceOrigPath;
            this.sourceDest = sourceDest;
        }

        @Override
        public String toString() {
            return fwdArrow + " @" + Integer.toHexString(hashCode()) + " ?-->" + bwdBlock + " src:" + sourceOrigPath + "-->" + sourceDest;
        }

    }

    private static final class PushPopNode {
        private TapList<FGArrow> fwdArrows;
        private Block bwdBlock;
        private SymbolTable bwdScope;
        private TapList<Block> sourceOrigPath;
        private Block sourceDest;
        private TapList<PushPopNode> subNodes;
        private int numberOfCases = -1;
        private BwdSwitchCase referenceBwdSwitchCase;

        private PushPopNode(TapList<FGArrow> fwdArrows, Block bwdBlock, BwdSwitchCase bwdSwitchCase,
                            SymbolTable bwdScope, TapList<Block> sourceOrigPath, Block sourceDest,
                            TapList<PushPopNode> subNodes) {
            super();
            this.fwdArrows = fwdArrows;
            this.bwdBlock = bwdBlock;
            this.bwdScope = bwdScope;
            this.referenceBwdSwitchCase = bwdSwitchCase;
            this.sourceOrigPath = sourceOrigPath;
            this.sourceDest = sourceDest;
            this.subNodes = subNodes;
        }

        private void computeNumberOfCases() {
            if (subNodes == null) {
                numberOfCases = 1;
            } else {
                int sum = 0;
                TapList<PushPopNode> inSubNodes = subNodes;
                while (inSubNodes != null) {
                    sum += inSubNodes.head.numberOfCases;
                    inSubNodes = inSubNodes.tail;
                }
                numberOfCases = sum;
            }
        }

        private boolean uniqueBwdDest() {
            TapList<Block> allBwdDests = collectBwdDests(null) ;
            return allBwdDests!=null && allBwdDests.tail==null ;
        }

        private TapList<Block> collectBwdDests(TapList<Block> collectedDests) {
            if (subNodes == null) {
                if (!TapList.contains(collectedDests, bwdBlock)) {
                    collectedDests = new TapList<>(bwdBlock, collectedDests) ;
                }
            } else {
                TapList<PushPopNode> inSubNodes = subNodes;
                while (inSubNodes != null) {
                    collectedDests = inSubNodes.head.collectBwdDests(collectedDests) ;
                    inSubNodes = inSubNodes.tail;
                }
            }
            return collectedDests ;
        }

        private Block pickFirstSourceDest() {
            Block found = sourceDest;
            TapList<PushPopNode> inSubNodes = subNodes;
            while (found == null && inSubNodes != null) {
                found = inSubNodes.head.pickFirstSourceDest();
                inSubNodes = inSubNodes.tail;
            }
            return found;
        }

        private void dump(TapList<PushPopNode> dejaVu) {
            if (TapList.contains(dejaVu.tail, this)) {
                TapEnv.printOnTrace("[DEJAVU]");
            } else {
                TapEnv.printOnTrace("[" + fwdArrows + " ; " + bwdBlock
                        + " ; bwdScope:" + (bwdScope == null ? "null" : "@" + Integer.toHexString(bwdScope.hashCode()))
                        + " src:" + sourceOrigPath + "-->" + sourceDest + " refBwdSwitchCase:"
                        + (referenceBwdSwitchCase == null ? "null" : "@" + Integer.toHexString(referenceBwdSwitchCase.hashCode())));
                dejaVu.placdl(this);
                if (subNodes != null) {
                    TapEnv.incrTraceIndent(2);
                    TapList<PushPopNode> inSubNodes = subNodes;
                    while (inSubNodes != null) {
                        TapEnv.printlnOnTrace();
                        TapEnv.indentOnTrace();
                        inSubNodes.head.dump(dejaVu);
                        inSubNodes = inSubNodes.tail;
                    }
                    TapEnv.decrTraceIndent(2);
                }
                TapEnv.printOnTrace("]");
            }
        }

        @Override
        public String toString() {
            return "[" + fwdArrows + " ; " + bwdBlock + " ; "
                    //+(bwdScope!=null?bwdScope.addressChain():null)
                    + " src:" + sourceOrigPath + "-->" + sourceDest
                    + " <" + subNodes + ">]";
        }

    }

    /**
     * Represents a matching pair of<br>
     * (1) the beginning (i.e. upstream) of a fwd piece of Flow Graph<br>
     * (2) the exits (i.e. upstream too!) of the matching bwd piece of Flow Graph.<br>
     * Used to connect to the fwd and bwd diffs of another piece of Flow Graph
     * that lies upstream. In particular, this object can represent the
     * end of the Flow Graph, in which case it is used to connect the end
     * of the upstream fwd sweep to the beginning of the upstream bwd sweep.
     */
    private final class DownstreamConnector {
        private Block entry;
        private Block sourceBlock;
        private TapList<FGArrow> exits;
        private SymbolTable bwdScope;
        /**
         * A Block from which all exit arrows must originate.
         * Used only in a temporary internal way: in standard, please use field "exits".
         */
        private Block exitBlock;
        /**
         * case where we connect the end of fwd directly to the start of bwd.
         */
        private boolean isATurn;

        private DownstreamConnector(Block entry, Block sourceBlock,
                                    TapList<FGArrow> exits, SymbolTable bwdScope) {
            super();
            this.entry = entry;
            this.sourceBlock = sourceBlock;
            this.exits = exits;
            this.bwdScope = bwdScope;
            this.exitBlock = null;
            isATurn = entry == null && exits == null;
        }

        private DownstreamConnector(DownstreamConnector model) {
            super();
            this.entry = model.entry;
            this.sourceBlock = model.sourceBlock;
            this.exits = model.exits;
            this.bwdScope = model.bwdScope;
            this.exitBlock = model.exitBlock;
            this.isATurn = model.isATurn;
        }

        /**
         * This method sets the connector into a special temporary state,
         * and must be followed immediately by a putArrowAtTail() to reach a clean state.
         */
        private void putBlockAtTail(Block newBlock) {
            if (exits == null) {
                // If DownstreamConnector is empty, "newBlock" becomes its new start:
                entry = newBlock;
            } else {
                // else connect all exits of the DownstreamConnector to "newBlock":
                mapCheckFreeThenRedirectDestination(exits, newBlock, false);
                exits = null;
            }
            // "newBlock" always becomes the exitBlock of the DownstreamConnector:
            exitBlock = newBlock;
        }

        /**
         * Sets the exit part of this DownstreamConnector to be an FGArrow that
         * leaves (with test and cas) from the previously stored exitBlock.
         */
        private void putArrowAtTail(int test, int cas) {
            exits = new TapList<>(checkFreeThenSetNewFGArrow(exitBlock, test, cas, null, false), null);
            exitBlock = null;
        }

        @Override
        public String toString() {
            return "[" + entry + " ; " + exits
                    + " src:"+sourceBlock
                    //+" ; "+(bwdScope!=null?bwdScope.addressChain():null)
                    + "]";
        }

    }

    /**
     * Objects that hold everything needed to create references to a
     * variable of a given "name", and also actions such as PUSH and
     * POP on these variables. These references and actions can
     * be built to occur either in the fwd sweep or in the bwd sweep.
     */
    private final class FwdBwdVarTool {

        /**
         * The SymbolTable of the fwd diff block that will hold
         * the new references to the variable.
         */
        private final SymbolTable fwdDiffST;

        /**
         * The SymbolTable of the bwd diff block that will hold
         * the new references to the variable.
         */
        private final SymbolTable diffST;

        /**
         * The symbol holder for the fwd sweep.
         */
        private NewSymbolHolder fwdHolder;

        /**
         * The symbol holder for the bwd sweep. Can be the same as
         * fwdHolder if ADJOINT mode.
         */
        private final NewSymbolHolder holder;

        /**
         * The RefDescriptor of the variable for the fwd sweep.
         * Used e.g. to PUSH this variable.
         */
        private RefDescriptor fwdRefDescriptor;

        /**
         * The RefDescriptor of the variable for the bwd sweep.
         * Used e.g. to POP this variable.
         * Can be the same as fwdRefDescriptor if ADJOINT mode.
         */
        private final RefDescriptor refDescriptor;

        /**
         * Creates a new FwdBwdVarTool for an integer variable named "name", to be used
         * in blocks whose source block is at the same level as "place", and to be declared at the
         * given "fwdDiffST" for the fwd diff Block and "diffST" for the rev diff Block.
         *
         * @param name      The name of the variable.
         * @param pushExpr  If non-null, the expression to be pushed.
         *                  If this argument is null, the forward RefDescriptor will be pushed.
         * @param place     The current "place" in the source code.
         * @param fwdDiffST The current receiving SymbolTable in the forward sweep.
         * @param diffST    The current receiving SymbolTable in the reverse sweep.
         */
        private FwdBwdVarTool(String name, Tree pushExpr, Block place,
                              SymbolTable fwdDiffST, SymbolTable diffST) {
            super();
            this.fwdDiffST = fwdDiffST;
            this.diffST = diffST;
            int holderSize = -1;
            WrapperTypeSpec pushExprType =
                    (pushExpr == null || fwdDiffST == null ? null : fwdDiffST.typeOf(pushExpr));
            if (TypeSpec.isA(pushExprType, SymbolTableConstants.MODIFIEDTYPE)) {
                assert pushExprType != null;
                if (((ModifiedTypeSpec) pushExprType.wrappedType).sizeModifierValue() == -1) {
                    // If the precision size of the pushed expr is unknown, turn it into an int*8
                    pushExpr = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "int"),
                            ILUtils.build(ILLang.op_expressions,
                                    ILUtils.copy(pushExpr),
                                    ILUtils.build(ILLang.op_intCst, 8)));
                    holderSize = 8;
                    pushExprType =
                            new WrapperTypeSpec(
                                    new ModifiedTypeSpec(
                                            adEnv.integerTypeSpec,
                                            ILUtils.build(ILLang.op_intCst, holderSize),
                                            fwdDiffST));
                } else {
                    holderSize = pushExprType.wrappedType.size();
                }
            }

            holder = new NewSymbolHolder(name, curDiffUnit(), adEnv.integerTypeSpec, holderSize);
            holder.declarationLevelMustInclude(diffST);
            fwdHolder = holder;
            if (fwdDiffST != null && fwdDiffST != diffST) {
                // In ADJOINT_SPLIT mode, we need two different NewSymbolHolder's for fwd and bwd sweeps:
                fwdHolder = new NewSymbolHolder(name, curFwdDiffUnit(), adEnv.integerTypeSpec, holderSize);
                fwdHolder.declarationLevelMustInclude(fwdDiffST);
            }
            refDescriptor =
                    new RefDescriptor(holder.makeNewRef(diffST), null,
                            diffST, diffST, null,
                            null, false, null, curDiffUnit());
            fwdRefDescriptor = refDescriptor;
            if (fwdDiffST != null) {
                if (pushExpr != null) {
                    fwdRefDescriptor =
                            new RefDescriptor(pushExpr, pushExprType, null,
                                    fwdDiffST, fwdDiffST, null,
                                    null, false, null, curFwdDiffUnit());
                } else if (fwdDiffST != diffST) {
                    // In ADJOINT_SPLIT mode, we need two different RefDescriptor's for fwd and bwd sweeps:
                    fwdRefDescriptor =
                            new RefDescriptor(fwdHolder.makeNewRef(fwdDiffST), null,
                                    fwdDiffST, fwdDiffST, null,
                                    null, false, null, curFwdDiffUnit());
                }
            }
            adEnv.setCurBlock(place.enclosingLoop());
            blockDifferentiator().prepareRestoreOperations(fwdRefDescriptor, refDescriptor, 1, fwdDiffST, diffST);
        }

        /**
         * creates and returns a new reference to the variable in the fwd sweep.
         *
         * @return a new reference to the variable to be used in the fwd sweep.
         */
        private Tree makeFwdRef(Block srcOfUsageBlock) {
            //[llh] TODO safer: pass the fwd usage ST, rather than using fwdDiffST
            fwdHolder.preparePrivateClause(srcOfUsageBlock, adEnv.curUnit().privateSymbolTable(),
                    true, false);
            return fwdHolder.makeNewRef(fwdDiffST);
        }

        /**
         * creates and returns a new reference to the variable in the bwd sweep.
         *
         * @return a new reference to the variable to be used in the bwd sweep.
         */
        private Tree makeRef(Block srcOfUsageBlock) {
            //[llh] TODO safer: pass the usage ST, rather than using diffST
            holder.preparePrivateClause(srcOfUsageBlock, adEnv.curUnit().privateSymbolTable(),
                    false, false);
            return holder.makeNewRef(diffST);
        }

        /**
         * creates and returns a PUSH operation (or equivalent) for the fwd sweep.
         *
         * @return a PUSH instruction (or equivalent) to be used in the fwd sweep.
         */
        private Tree makePush(Block srcOfUsageBlock) {
            fwdHolder.preparePrivateClause(srcOfUsageBlock, adEnv.curUnit().privateSymbolTable(),
                    true, false);
            return fwdRefDescriptor.makePush();
        }

        /**
         * creates and returns a POP operation (or equivalent) for the bwd sweep.
         *
         * @return a POP instruction (or equivalent) to be used in the bwd sweep.
         */
        private Tree makePop(Block srcOfUsageBlock) {
            holder.preparePrivateClause(srcOfUsageBlock, adEnv.curUnit().privateSymbolTable(),
                    false, false);
            return refDescriptor.makePop();
        }

    }

    /**
     * Dirty code by llh. Not sure it is correct. Tries to solve the
     * conflict F77:lh38 vs F77:lha25 between insertion of
     * reinitialization of diff variables and elimination of
     * superfluous taped control.
     */
    private boolean keepBwdBecauseOfDiffReinits(Block block) {
        TapList<FGArrow> arrows = block.backFlow();
        if (arrows == null || arrows.tail != null) {
            return false;
        }
        FGArrow arrow = arrows.head;
        return mustCheckActivitySwitches(arrow.origin, arrow.destination);
    }

    /**
     * With the current "FwdBwd" algorithm to connect the pieces of the Fwd and Bwd sweeps,
     * we must force the bwd block of a block with many exits and with a fwd diff, to be bwdLive,
     * even if it is empty. Otherwise we run into trouble when connecting the fwd and bwd
     * blocks, cf F90:lh14, F90:lha67, F90:lha68, F77:lh05, F77:lh06, F77:lha71 etc...
     */
    private boolean keepBwdBecauseOfTwoExits(Block block, Block fwdDiffBlock, FlowGraphDifferentiationEnv diffEnv) {
        return block.flow() != null && block.flow().tail != null
                && (fwdDiffBlock.instructions != null || diffEnv.blockMustBeLive(block));
    }

    private void declareStackPointerInterface(Unit diffUnit) {
        if (diffUnit != null && adEnv.curUnit().isStandard() &&
                diffUnit.privateSymbolTable() != null) {
            Block diffDeclarationsBlock = diffUnit.privateSymbolTable().declarationsBlock;
            if (diffDeclarationsBlock != null) {
                diffDeclarationsBlock.addUseDecl(ILUtils.build(ILLang.op_useDecl,
                        ILUtils.build(ILLang.op_ident, "ISO_C_BINDING"),
                        ILUtils.build(ILLang.op_renamedVisibles)));
                diffDeclarationsBlock.addUseDecl(ILUtils.build(ILLang.op_useDecl,
                        ILUtils.build(ILLang.op_ident, "ADMM_TAPENADE_INTERFACE"),
                        ILUtils.build(ILLang.op_renamedVisibles)));
            }
        }
    }

    /**
     * Inserts into turnBlock all the calls to AMPI_Turn() that declare
     * primal-to-adjoint correspondence between variables v and vb.
     * This declaration is needed when v is passed through an FW_AMPI_Irecv(v),
     * so that the matching BW_AMPI_Wait() will have to do a MPI_Isend(Vb).
     */
    private void placeAMPITurns(BoolVector zonesToTurn, Block turnBlock, SymbolTable destSymbolTable,
                                FlowGraphDifferentiationEnv diffEnv) {
        if (diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            turnBlock = curDiffUnit().entryBlock().flow().head.destination;
        }
        ZoneInfo zoneInfo;
        for (int i=destSymbolTable.declaredZonesNb(adEnv.diffKind)-1 ; i>=0 ; --i) {
            if (zonesToTurn.get(i)) {
                zoneInfo = destSymbolTable.declaredZoneInfo(i, adEnv.diffKind);
                if (diffEnv.adDiffMode == DiffConstants.ADJOINT_SPLIT_MODE
                        && i >= adEnv.curUnit().publicSymbolTable().declaredZonesNb(adEnv.diffKind)) {
                    // We cannot run AMPI_Turn(x,xb) when x is a local var of FOO_BWD(),
                    // because it has a different address from the x in FOO_FWD()
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(ADxx) Differentiated procedure of " + adEnv.curUnit().name() + " needs to run AMPI_Turn on renewed local variable " + ILUtils.toString(zoneInfo.accessTree));
                } else {
                    Tree varRef = ILUtils.copy(zoneInfo.accessTree);
                    if (curDiffUnit().isC()) {
                        varRef = ILUtils.addAddressOf(varRef);
                    }
                    Tree diffVarRef = varRefDifferentiator().diffVarRef(adEnv.curActivity(), varRef, turnBlock.symbolTable, false,
                            varRef, false, false, varRef);
                    Tree turnCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "ADTOOL_AMPI_Turn"),
                            ILUtils.build(ILLang.op_expressions, varRef, diffVarRef));
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("!Augment " + turnBlock + " with AMPI_Turn: " + ILUtils.toString(turnCall));
                    }
                    turnBlock.addInstrHdAfterDecls(turnCall);
                }
            }
        }
    }

    /**
     * Inserts into turnBlock for profiling
     */
    private void placeProfileTurn(Block turnBlock, SymbolTable destSymbolTable,
                                FlowGraphDifferentiationEnv diffEnv) {
        
        Tree turnCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_turn"),
                            ILUtils.build(ILLang.op_expressions, 
                                ILUtils.buildStringCst(adEnv.curUnit().name(), curDiffUnit().isFortran()),
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()))
                                );
        turnBlock.addInstrHdAfterDecls(turnCall);
    }

    /** Patch for adjoint mode: the reference controller for a (e.g. diff reinit) operation
     * from adjoint of "toBlock" back to adjoint of "fromBlock" is in general "toBlock".
     * Exception is when fromBlock is in fact controlled by toBlock. Should be clarified... */
    private Block deepestController(Block toBlock, Block fromBlock) {
        Instruction firstTo = toBlock.headInstr() ;
        return (firstTo!=null && TapList.contains(fromBlock.parallelControls, firstTo) ? fromBlock : toBlock) ;
    }

    /**
     * Creates a new diff Block containing derivative (re)initialization instructions for variables
     * whose activity switch from passive to active when primal control goes from "fromBlock" to "toBlock".
     * @param diffDestSymbolTable the symbol table in which the diff variables to be reinitialized are living.
     * @param inFwd true if the new Block is for the Fwd sweep (e.g. in tangent mode).
     * @param parallelControlRef a primal Block whose parallel control determines the parallel control of the new Block.
     *        May be null in general. Used only when it should not be the default (inFwd ? fromBlock : toBlock)
     * @param toNewDeclaredInits an A-list that holds the (re)initialization instructions that are needed
     *        but can't be placed in the new diff Block because they are before their declaration.
     * @return the new diff Block when reinitialization is required, null otherwise.
     */
    private Block checkActivitySwitches(Block fromBlock, Block toBlock, SymbolTable diffDestSymbolTable,
                                        boolean inFwd, Block parallelControlRef, FlowGraphDifferentiationEnv diffEnv,
                                        TapList<TapPair<ZoneInfo, TapList<Tree>>> toNewDeclaredInits) {
        if (!adEnv.curUnitIsActiveUnit || adEnv.curUnitIsContext) {
            return null;
        }
        SymbolTable commonSymbolTable = fromBlock.symbolTable.getCommonRoot(toBlock.symbolTable);
        if (diffDestSymbolTable == curDiffUnit().publicSymbolTable()) {
            // The future initialization Block must not be in the "public" scope.
            diffDestSymbolTable = curDiffUnit().privateSymbolTable();
        }
        int nDRZ = commonSymbolTable.declaredZonesNb(adEnv.diffKind);
        int originInfoLen = fromBlock.symbolTable.declaredZonesNb(adEnv.diffKind);
        int destInfoLen = toBlock.symbolTable.declaredZonesNb(adEnv.diffKind);
        int vectorIndex;
        TapList<BoolVector> destActivs = null ;
        if (toBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            destActivs = ((TemporaryBlock) toBlock).blockActivities;
        } else if (curDiffUnitSort()!=DiffConstants.ADJOINT || toBlock.backReachUp) {
            destActivs = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(toBlock)) ;
        }
        BoolVector destinationEntryActiv = destActivs == null ? null : destActivs.head;
        if (destinationEntryActiv == null) { // for instance no activity case
            destinationEntryActiv = new BoolVector(destInfoLen);
            destinationEntryActiv.setTrue();
        }
        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        if (curDiffUnitSort()==DiffConstants.ADJOINT && toBlock instanceof ExitBlock) {
            //cf missing adjoint initializations in set08/lh002
            destinationEntryActiv = removeActivePassedByValue(destinationEntryActiv, toBlock.symbolTable);
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
        TapList<BoolVector> originActivs = null ;
        if (fromBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            originActivs = ((TemporaryBlock) fromBlock).blockActivities;
        } else if (curDiffUnitSort()==DiffConstants.ADJOINT || fromBlock.reachDown) {
            originActivs = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(fromBlock)) ;
        }
        BoolVector originExitActiv = TapList.last(originActivs);
        if (originExitActiv == null) { // for instance no activity case, or if fromBlock unreachable
            originExitActiv = new BoolVector(originInfoLen);
            originExitActiv.setTrue(); // so that no reinit is done
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("    Checking diff reinitialization due to activity switches,");
            TapEnv.printlnOnTrace("     " + (curDiffUnitSort()==DiffConstants.ADJOINT ? "back to" : "from") + ' ' + fromBlock + ' ' + originExitActiv);
            TapEnv.printlnOnTrace("     " + (curDiffUnitSort()==DiffConstants.ADJOINT ? "   from" : "  to") + ' ' + toBlock + ' ' + destinationEntryActiv);
        }
        boolean entersDiffUnit = (curDiffUnitSort()==DiffConstants.ADJOINT ? toBlock instanceof ExitBlock : fromBlock instanceof EntryBlock) ;
        BoolVector activityBefore = (curDiffUnitSort()==DiffConstants.ADJOINT ? destinationEntryActiv : originExitActiv) ;
        BoolVector activityAfter = (curDiffUnitSort()==DiffConstants.ADJOINT ? originExitActiv : destinationEntryActiv) ;
        TapList<Tree> allDiffInitTrees = null;
        Tree[] diffInitTreeR;
        Tree diffInitTree;
        ZoneInfo zoneInfo;
        Tree zoneAccessTree;
        SymbolTable commonUpSymbolTable = (commonSymbolTable==adEnv.curUnit().publicSymbolTable()
                                           ? adEnv.curUnit().privateSymbolTable() //To see variables in COMMONs
                                           : commonSymbolTable) ;
        SymbolTable postSymbolTable = (curDiffUnitSort()==DiffConstants.ADJOINT ? fromBlock.symbolTable : toBlock.symbolTable) ;
        int postnDRZ = postSymbolTable.declaredZonesNb(adEnv.diffKind);
        // A refinement to avoid duplicate initialization when a variable accesses several zones:
        BoolVector alreadyInitialized = new BoolVector(curDiffUnitSort()==DiffConstants.ADJOINT ? originInfoLen : destInfoLen) ;
        for (int i = postnDRZ - 1; i >= 0; --i) {
            zoneInfo = postSymbolTable.declaredZoneInfo(i, adEnv.diffKind);
            if (zoneInfo != null) {
                vectorIndex = zoneInfo.kindZoneNb(adEnv.diffKind);
                if (zoneActivitySwitches(zoneInfo, vectorIndex, i >= nDRZ, entersDiffUnit, activityBefore, activityAfter)) {
                    // then insert an initialization of the derivative of the i-th zone:
                    if (zoneInfo.isHidden || zoneInfo.accessTree==null) {
                        // To avoid repetitions, this warning is done only on diff-root procedures.
                        if (TapList.contains(adEnv.rootUnits, adEnv.curUnit())) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(AD14) Differentiated procedure of " + adEnv.curUnit().name() + " needs to initialize derivative of hidden variable: " + zoneInfo.description);
                        }
                    } else if (!alreadyInitialized.get(i)) {
                        zoneAccessTree = zoneInfo.accessTree;
                        ToBool total = new ToBool(false) ;
                        TapIntList accessedZones = commonUpSymbolTable.listOfZonesOfValue(zoneAccessTree, total, null) ;
                        if (total.get()) {
                            // If access is not total, we must not assume initialization is already done.
                            accessedZones = DataFlowAnalyzer.mapZoneRkToKindZoneRk(accessedZones, adEnv.diffKind,
                                                                                              commonUpSymbolTable) ;
                            // Note: we may want to send a warning if accessedZones also accesses zones that must *not* be diff-initialized!
                            alreadyInitialized.set(accessedZones, true) ;
                        }
                        // [pbActiv] zoneAccessTree = DataFlowAnalyzer.modifyAccessTreeForUniqueAccess(zoneInfo.zoneNb, zoneAccessTree, fromBlock) ;

                        // Names of variables in COMMON are found in the unit's private SymbolTable:
                        SymbolTable postDeclSymbolTable =
                            (postSymbolTable == adEnv.curUnit().publicSymbolTable() ? adEnv.curUnit().privateSymbolTable() : postSymbolTable) ;
                        diffInitTreeR =
                            blockDifferentiator().makeDiffInitialization(zoneAccessTree, zoneInfo, postDeclSymbolTable, diffDestSymbolTable,
                                    null, true, null, false);
                        for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                            diffInitTree = diffInitTreeR[iReplic] ;
                            if (i >= nDRZ && toNewDeclaredInits != null && curDiffUnit().isC()) {
                                //[llh 01/10/15] For C only (?) we put aside diff initializations that occur before their declaration.
                                // We keep them but don't use them so far... Ref: bug Morlighem on initializations on 16/9/2015.
                                TapPair<ZoneInfo, TapList<Tree>> initsForThisZone = TapList.assq(zoneInfo, toNewDeclaredInits.tail);
                                if (initsForThisZone == null) {
                                    initsForThisZone = new TapPair<>(zoneInfo, diffInitTree == null ? null : new TapList<>(diffInitTree, null));
                                    toNewDeclaredInits.placdl(initsForThisZone);
                                }
                            } else {
                                if (diffInitTree != null) {
                                    allDiffInitTrees = new TapList<>(diffInitTree, allDiffInitTrees);
                                }
                            }
                        }
                    }
                }
            }
        }
        Block diffInitBlock = null;
        if (allDiffInitTrees != null) {
            String switchName = "ActivitySwitch"+fromBlock.rank+"->"+toBlock.rank ;
            if (inFwd) {
                if (parallelControlRef==null) parallelControlRef = fromBlock ;
                diffInitBlock = buildFwdBlock("+"+switchName, diffDestSymbolTable, parallelControlRef, diffEnv);
            } else {
                if (parallelControlRef==null) parallelControlRef = deepestController(fromBlock, toBlock) ;
                diffInitBlock = buildBwdBlock("-"+switchName, diffDestSymbolTable, parallelControlRef, diffEnv);
            }
            while (allDiffInitTrees != null) {
                diffInitBlock.addInstrTl(allDiffInitTrees.head);
                allDiffInitTrees = allDiffInitTrees.tail;
            }
        }

        if (adEnv.traceCurDifferentiation) {
            if (diffInitBlock != null) {
                TapEnv.printlnOnTrace("      -> " + (curDiffUnitSort()==DiffConstants.ADJOINT ? "(+BWD)" : "") + " Diff Initialization: " + diffInitBlock.instructions);
            } else {
                TapEnv.printlnOnTrace("       -> no reinitialization needed");
            }
            if (toNewDeclaredInits != null && toNewDeclaredInits.tail != null) {
                TapEnv.printlnOnTrace("       => initializations before declaration:" + toNewDeclaredInits.tail);
            }
        }
        return diffInitBlock;
    }

    /**
     * Dry-run of checkActivitySwitches().
     * @return true if checkActivitySwitches() would create some diff reinitialization instructions.
     */
    private boolean mustCheckActivitySwitches(Block fromBlock, Block toBlock) {
        if (!adEnv.curUnitIsActiveUnit || adEnv.curUnitIsContext) {
            return false;
        }
        SymbolTable commonSymbolTable = fromBlock.symbolTable.getCommonRoot(toBlock.symbolTable);
        int nDRZ = commonSymbolTable.declaredZonesNb(adEnv.diffKind);
        int vectorIndex;
        TapList<BoolVector> destActivs = null ;
        if (toBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            destActivs = ((TemporaryBlock) toBlock).blockActivities;
        } else if (curDiffUnitSort()!=DiffConstants.ADJOINT || toBlock.backReachUp) {
            destActivs = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(toBlock)) ;
        }
        BoolVector destinationEntryActiv = destActivs == null ? null : destActivs.head;
        if (destinationEntryActiv == null) { // for instance no activity case
            destinationEntryActiv = new BoolVector(toBlock.symbolTable.declaredZonesNb(adEnv.diffKind));
            destinationEntryActiv.setTrue();
        }
        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        if (curDiffUnitSort()==DiffConstants.ADJOINT && toBlock instanceof ExitBlock) {
            //cf missing adjoint initializations in set08/lh002
            destinationEntryActiv = removeActivePassedByValue(destinationEntryActiv, toBlock.symbolTable);
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
        TapList<BoolVector> originActivs = null ;
        if (fromBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            originActivs = ((TemporaryBlock) fromBlock).blockActivities;
        } else if (curDiffUnitSort()==DiffConstants.ADJOINT || fromBlock.reachDown) {
            originActivs = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(fromBlock)) ;
        }
        BoolVector originExitActiv = TapList.last(originActivs);
        if (originExitActiv == null) { // for instance no activity case, or if fromBlock unreachable
            originExitActiv = new BoolVector(fromBlock.symbolTable.declaredZonesNb(adEnv.diffKind)) ;
            originExitActiv.setTrue(); // so that no reinit is done
        }
        boolean entersDiffUnit = (curDiffUnitSort()==DiffConstants.ADJOINT ? toBlock instanceof ExitBlock : fromBlock instanceof EntryBlock) ;
        BoolVector activityBefore = (curDiffUnitSort()==DiffConstants.ADJOINT ? destinationEntryActiv : originExitActiv) ;
        BoolVector activityAfter = (curDiffUnitSort()==DiffConstants.ADJOINT ? originExitActiv : destinationEntryActiv) ;
        SymbolTable postSymbolTable = (curDiffUnitSort()==DiffConstants.ADJOINT ? fromBlock.symbolTable : toBlock.symbolTable) ;
        int postnDRZ = postSymbolTable.declaredZonesNb(adEnv.diffKind);
        ZoneInfo zoneInfo;
        for (int i = postnDRZ - 1; i >= 0; --i) {
            zoneInfo = postSymbolTable.declaredZoneInfo(i, adEnv.diffKind);
            if (zoneInfo != null && !zoneInfo.isHidden) {
                vectorIndex = zoneInfo.kindZoneNb(adEnv.diffKind);
                if (zoneActivitySwitches(zoneInfo, vectorIndex, i >= nDRZ, entersDiffUnit, activityBefore, activityAfter)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return true if the checkActivitySwitches will build the same set of
     * diff reinitialization instructions along arrow1 and along arrow2.
     */
    private boolean sameActivitySwitches(Block fromBlock, FGArrow arrow1, FGArrow arrow2) {
        if (curDiffUnitSort()!=DiffConstants.ADJOINT) {
            TapEnv.toolWarning(-1, "sameActivitySwitches() is implemented only for reverse mode");
            return false;
        }
        if (!adEnv.curUnitIsActiveUnit || adEnv.curUnitIsContext) {
            return true;
        }
        Block toBlock1 = arrow1.destination;
        Block toBlock2 = arrow2.destination;
        SymbolTable srcSymbolTable = fromBlock.symbolTable;
        SymbolTable commonSymbolTable1 = fromBlock.symbolTable.getCommonRoot(toBlock1.symbolTable);
        SymbolTable commonSymbolTable2 = fromBlock.symbolTable.getCommonRoot(toBlock2.symbolTable);
        int srcnDRZ = srcSymbolTable.declaredZonesNb(adEnv.diffKind);
        int nDRZ1 = commonSymbolTable1.declaredZonesNb(adEnv.diffKind);
        int nDRZ2 = commonSymbolTable2.declaredZonesNb(adEnv.diffKind);
        boolean entersDiffUnit1 = (curDiffUnitSort()==DiffConstants.ADJOINT ? toBlock1 instanceof ExitBlock : fromBlock instanceof EntryBlock) ;
        boolean entersDiffUnit2 = (curDiffUnitSort()==DiffConstants.ADJOINT ? toBlock2 instanceof ExitBlock : fromBlock instanceof EntryBlock) ;
        TapList<BoolVector> destActivs1 = null ;
        if (toBlock1.rank == -99)
        // This is a TemporaryBlock. Its data flow info is attached directly!
        {
            destActivs1 = ((TemporaryBlock) toBlock1).blockActivities;
        } else if (curDiffUnitSort()!=DiffConstants.ADJOINT || toBlock1.backReachUp) {
            destActivs1 = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(toBlock1)) ;
        }
        BoolVector destinationEntryActiv1 = (destActivs1==null ? null : destActivs1.head) ;
        if (destinationEntryActiv1 == null) { // for instance no activity case
            destinationEntryActiv1 = new BoolVector(toBlock1.symbolTable.declaredZonesNb(adEnv.diffKind));
            destinationEntryActiv1.setTrue();
        }
        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        if (curDiffUnitSort()==DiffConstants.ADJOINT && toBlock1 instanceof ExitBlock) {
            //cf missing adjoint initializations in set08/lh002
            destinationEntryActiv1 = removeActivePassedByValue(destinationEntryActiv1, toBlock1.symbolTable);
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
        TapList<BoolVector> destActivs2 = null ;
        if (toBlock2.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            destActivs2 = ((TemporaryBlock) toBlock2).blockActivities;
        } else if (curDiffUnitSort()!=DiffConstants.ADJOINT || toBlock2.backReachUp) {
            destActivs2 = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(toBlock2)) ;
        }
        BoolVector destinationEntryActiv2 = (destActivs2==null ? null : destActivs2.head) ;
        if (destinationEntryActiv2 == null) { // for instance no activity case
            destinationEntryActiv2 = new BoolVector(toBlock2.symbolTable.declaredZonesNb(adEnv.diffKind));
            destinationEntryActiv2.setTrue();
        }
        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        if (curDiffUnitSort()==DiffConstants.ADJOINT && toBlock2 instanceof ExitBlock) {
            //cf missing adjoint initializations in set08/lh002
            destinationEntryActiv2 = removeActivePassedByValue(destinationEntryActiv2, toBlock2.symbolTable);
        }
        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
        TapList<BoolVector> originActivs = null ;
        if (fromBlock.rank == -99) {
            // This is a TemporaryBlock. Its data flow info is attached directly!
            originActivs = ((TemporaryBlock) fromBlock).blockActivities;
        } else if (curDiffUnitSort()==DiffConstants.ADJOINT || fromBlock.reachDown) {
            originActivs = (adEnv.unitActivities==null ? null : adEnv.unitActivities.retrieve(fromBlock)) ;
        }
        BoolVector originExitActiv = TapList.last(originActivs);
        if (originExitActiv == null) { // for instance no activity case, or if fromBlock unreachable
            originExitActiv = new BoolVector(fromBlock.symbolTable.declaredZonesNb(adEnv.diffKind)) ;
            originExitActiv.setTrue(); // so that no reinit is done
        }
        ZoneInfo zoneInfo;
        boolean mustReinitialize1, mustReinitialize2;
        int vectorIndex;
        for (int i = srcnDRZ - 1; i >= 0; --i) {
            zoneInfo = srcSymbolTable.declaredZoneInfo(i, adEnv.diffKind);
            if (zoneInfo != null && !zoneInfo.isHidden) {
                vectorIndex = zoneInfo.kindZoneNb(adEnv.diffKind);
                mustReinitialize1 = zoneActivitySwitches(zoneInfo, vectorIndex, i >= nDRZ1, entersDiffUnit1,
                        destinationEntryActiv1, originExitActiv);
                mustReinitialize2 = zoneActivitySwitches(zoneInfo, vectorIndex, i >= nDRZ2, entersDiffUnit2,
                        destinationEntryActiv2, originExitActiv);
                if (mustReinitialize1 != mustReinitialize2) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Reverse differentiation of pass-by-value variables.
     */
    private BoolVector removeActivePassedByValue(BoolVector destinationEntryActiv, SymbolTable destSymbolTable) {
        destinationEntryActiv = destinationEntryActiv.copy();
        int nbFormalArgs = adEnv.curUnit().functionTypeSpec().argumentsTypes == null ? 0 : adEnv.curUnit().functionTypeSpec().argumentsTypes.length;
        Tree[] args = ILUtils.getArguments(adEnv.curUnit().headTree()).children();
        UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(adEnv.curUnit());
        boolean[] diffArgsByValueNeedOverwrite =
                diffInfo.getUnitDiffArgsByValueNeedOverwriteInfoS(adEnv.curActivity());
        boolean[] diffArgsByValueNeedOnlyIncrement =
                diffInfo.getUnitDiffArgsByValueNeedOnlyIncrementInfoS(adEnv.curActivity());
        for (int j = nbFormalArgs; j > 0; j--) {
            if (adEnv.curUnit().takesArgumentByValue(j, adEnv.curUnit().language())
                    && diffArgsByValueNeedOverwrite != null && diffArgsByValueNeedOverwrite[j]
                    && diffArgsByValueNeedOnlyIncrement != null && !diffArgsByValueNeedOnlyIncrement[j]) {
                TapIntList topZonesOfArg =
                        ZoneInfo.listAllZones(destSymbolTable.treeOfZonesOfValue(args[j - 1], null, null, null), false);
                while (topZonesOfArg != null) {
                    int index = DataFlowAnalyzer.zoneRkToKindZoneRk(topZonesOfArg.head, adEnv.diffKind,
                                                                               destSymbolTable);
                    // declare non-activity upon exit to force initialization to zero of the adjoint variable.
                    destinationEntryActiv.set(index, false);
                    topZonesOfArg = topZonesOfArg.tail;
                }
            }
        }
        return destinationEntryActiv;
    }

    /**
     * @return true if the given zone ("zoneInfo", having index "vectorIndex" in the 2 BoolVector's)
     * switches activity while going from activityBefore to activityAfter, from passive to active.
     */
    private boolean zoneActivitySwitches(ZoneInfo zoneInfo, int vectorIndex,
                                         boolean entersScope, boolean entersDiffUnit,
                                         BoolVector activityBefore, BoolVector activityAfter) {
        boolean switches = false;
        if (// don't try to initialize a variable which is an allocated global:
            !zoneInfo.comesFromAllocate()
            // don't try to initialize the Message-Passing channels nor an IO stream:
            && !zoneInfo.isChannelOrIO
            // don't try to initialize a diff that is never used:
            && (zoneInfo.isOnceActive() || !TapEnv.doActivity())
            // in C, the op_return takes care of the diff return value: don't reinitialize it!
            && !(curDiffUnit().language() == TapEnv.C && zoneInfo.isResult())) {
            if (!TapEnv.spareDiffReinitializations()) {// => special mode -nooptim "spareDiffInit" :
                // initialize all diff vars that enter into scope now:
                switches = (entersScope
                            ||
                        // also, upon entry into the diff subroutine (i.e. on the exit arrow if reverse!),
                        // initialize diff of passive formal params and COMMON globals, when there is such a (local!) diff:
                        // Checks: set11/lh019, set10/lh202, set06/v308 set11/lh043 set11/v02,
                        // and all validations with -nooptim spareinit
                            (entersDiffUnit && !activityBefore.get(vectorIndex)
                             && ((zoneInfo.isParameter() /*TODO?: && "paramHasOnlyLocalDiff"(zoneInfo)*/)
                                 ||
                                 (zoneInfo.isGlobal() && zoneInfo.commonName!=null
                                  &&
                                  !adEnv.commonActivityMemoryMap.getSetMemMap(zoneInfo.commonName)
                                      .isActiveRegion(zoneInfo.startOffset, zoneInfo.endOffset, zoneInfo.infiniteEndOffset)
                                  )
                                 ))) ;
            } else if (TapEnv.debugAdMode() != TapEnv.ADJ_DEBUG) {// => NORMAL MODE "spareDiffInit" && no -debugADJ :
                // if the variable's scope ends here, it's just as if it became passive (cf nonRegrF77:lh87)
                // Also, to play with ABS Linear Forms, every local derivative array must be fully zero-initialized! :
                switches = (entersScope || !activityBefore.get(vectorIndex)) &&
                    (activityAfter.get(vectorIndex) || (entersScope && TapEnv.get().absLinearForm));
            } else { // => normal mode "spareDiffInit" && -debugADJ :
                switches = entersScope
                        ||
                        !activityBefore.get(vectorIndex) && activityAfter.get(vectorIndex);
            }
        }
        return switches;
    }

    /**
     * Differentiate Fortran save attributes.
     *
     * @param diffSymbolTable curDiffUnit symbolTable.
     */
    protected void differentiateSaveList(SymbolTable diffSymbolTable) {
        if (diffSymbolTable != null && diffSymbolTable.saveList != null
            && (adEnv.curUnit().isModule() || adEnv.curUnit().isInterface()
                || (adEnv.curUnit().isStandard() && curDiffUnitSort()!=DiffConstants.ADJOINT))) {
            TapList<Tree> inNonDiffSaveList = diffSymbolTable.saveList;
            TapList<Tree> diffSaveList = null;
            Tree saveExpression;
            Tree[] expressions;
            TapList<Tree> diffExpressions;
            while (inNonDiffSaveList != null) {
                expressions = inNonDiffSaveList.head.children();
                diffExpressions = null;
                for (int i = expressions.length - 1; i >= 0; --i) {
                    saveExpression = differentiateSaveVarRef(expressions[i], diffSymbolTable);
                    if (saveExpression != null) {
                        diffExpressions = new TapList<>(saveExpression, diffExpressions);
                    }
                }
                // When we have a "save all" command (expressions.length==0),
                // then all variables, including the derivatives, will be "saved".
                // Therefore it is no use to repeat the "save all" declaration.
                if (diffExpressions != null) {
                    diffSaveList = new TapList<>(ILUtils.build(ILLang.op_save, diffExpressions), diffSaveList);
                }
                inNonDiffSaveList = inNonDiffSaveList.tail;
            }
            diffSymbolTable.saveList = TapList.append(diffSymbolTable.saveList, diffSaveList);
        }
    }

    private Tree differentiateSaveVarRef(Tree expression, SymbolTable diffSymbolTable) {
        switch (expression.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
                return ILUtils.build(expression.opCode(),
                        differentiateSaveVarRef(expression.down(1), diffSymbolTable),
                        ILUtils.copy(expression.down(2)));
            case ILLang.op_ident: {
                // When this symbol has been differentiated somewhere,
                // returns a new Tree containing the differentiated symbol. Otherwise returns null.
                VariableDecl varDecl = diffSymbolTable.getVariableDecl(ILUtils.getIdentString(
                        expression));
                NewSymbolHolder diffSymbolHolder = null;
                if (varDecl != null) {
                    diffSymbolHolder = varDecl.getDiffSymbolHolder(curDiffVarSort(), null, 0);
                }
                if (diffSymbolHolder != null &&
                        diffSymbolHolder.newVariableDecl() != null) {
                    diffSymbolHolder.declarationLevelMustInclude(diffSymbolTable);
                    return diffSymbolHolder.makeNewRef(diffSymbolTable);
                } else {
                    return null;
                }
            }
            default:
                TapEnv.toolWarning(-1, "(Differentiate reference in Save) Unexpected operator: " + expression.opName());
                return null;
        }
    }

    /**
     * Initialization if -multi option.
     */
    protected void initializeMultiDirMode() {
        if (multiDirMode()) {
            SymbolTable diffPublicSymbolTable = curDiffUnit().publicSymbolTable();
            NewSymbolHolder ndVar = new NewSymbolHolder("nd");
            varRefDifferentiator().dirIndexSymbolHolder = ndVar;
            ndVar.setAsVariable(adEnv.integerTypeSpec, null);
            ndVar.prepareDeclarationInstr(curDiffUnit());
            if (!adEnv.curUnit().isPackage()) {
                Tree oneNbDirsTree ;
                if (TapEnv.get().fixedNbdirsString != null) {
                    try {
                        int nbdirsint = Integer.parseInt(TapEnv.get().fixedNbdirsString) ;
                        TapEnv.get().fixedNbdirsTree = ILUtils.build(ILLang.op_intCst, nbdirsint) ;
                    } catch (NumberFormatException e) {
                        TapEnv.get().fixedNbdirsTree = ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsString);
                    }
                    oneNbDirsTree = ILUtils.copy(TapEnv.get().fixedNbdirsTree) ;
                } else {
                    NewSymbolHolder nbdirsVar = new NewSymbolHolder("nbdirs");
                    varRefDifferentiator().dirNumberSymbolHolder = nbdirsVar;
                    nbdirsVar.setAsVariable(adEnv.integerTypeSpec, null);
                    if (!curDiffUnit().isC()) {
                        nbdirsVar.prepareDeclarationInstr(curDiffUnit());
                    }
                    nbdirsVar.declarationLevelMustInclude(diffPublicSymbolTable);
                    oneNbDirsTree = nbdirsVar.makeNewRef(diffPublicSymbolTable) ;
                }
                varRefDifferentiator().multiDirIterDescriptor = new IterDescriptor(ndVar);
                varRefDifferentiator().multiDirIterDescriptor.setInitTree(
                        ILUtils.build(ILLang.op_intCst,
                                TapEnv.relatedLanguageIsC() ? 0 : 1));
                varRefDifferentiator().multiDirIterDescriptor.setLengthTree(oneNbDirsTree);
                //NOTE: isArray should be set to true in Fortran also in reverse mode, but it is not tested yet and still has some bugs:
                varRefDifferentiator().multiDirIterDescriptor.isArray =
                    (adEnv.adDiffMode==DiffConstants.TANGENT_MODE && TapEnv.relatedLanguageIsFortran9x());
                varRefDifferentiator().multiDirIterDescriptor.isImplicitCompleteDimension = false ;
            }
            SymbolTable diffPrivateSymbolTable = curDiffUnit().privateSymbolTable();
            if (!adEnv.curUnit().isInterface() && diffPrivateSymbolTable != null) {// i.e. not for Interfaces:
                ndVar.declarationLevelMustInclude(diffPrivateSymbolTable);
            }
            initializeMultiDirNumberMax();
            if (!adEnv.curUnit().isModule()) {
                varRefDifferentiator().multiDirMaxIterDescriptor = new IterDescriptor(ndVar);
                varRefDifferentiator().multiDirMaxIterDescriptor.setInitTree(
                        ILUtils.build(ILLang.op_intCst,
                                TapEnv.relatedLanguageIsC() ? 0 : 1));
                varRefDifferentiator().multiDirMaxIterDescriptor.setLengthTree(ILUtils.copy(curDiffUnit().dirNumberMaxTree));
                varRefDifferentiator().multiDirMaxIterDescriptor.isArray = TapEnv.relatedLanguageIsFortran9x();
                varRefDifferentiator().multiDirMaxIterDescriptor.isImplicitCompleteDimension = true ;
            }
        }
    }

    /**
     * Prepare the NewSymbolHolder that contains variable "NBDirsMax"
     * for the differentiated Unit "curDiffUnit".
     * This initialization will be done only once for each curDiffUnit.
     */
    protected void initializeMultiDirNumberMax() {
        // TODO: try refactor with CallGraphDifferentiator.initializeMultiDirNumberMax()
        if (TapEnv.get().fixedNbdirsmaxString != null) {
            try {
                int nbdirsmaxint = Integer.parseInt(TapEnv.get().fixedNbdirsmaxString) ;
                TapEnv.get().fixedNbdirsmaxTree = ILUtils.build(ILLang.op_intCst, nbdirsmaxint) ;
            } catch (NumberFormatException e) {
                TapEnv.get().fixedNbdirsmaxTree = ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsmaxString);
            }
            curDiffUnit().dirNumberMaxTree = TapEnv.get().fixedNbdirsmaxTree ;
            curDiffUnit().multiDirDimensionMax =
                    new ArrayDim(ILUtils.buildDimColon(curDiffUnit().arrayDimMin,
                                                       ILUtils.copy(curDiffUnit().dirNumberMaxTree)),
                                 null, null, null, -1, 0, 0);
        } else if (curDiffUnit().dirNumberMaxSymbolHolder == null) {
            NewSymbolHolder nbDirsMaxVar = new NewSymbolHolder("NBDirsMax");
            curDiffUnit().dirNumberMaxSymbolHolder = nbDirsMaxVar;
            nbDirsMaxVar.setAsConstantVariable(adEnv.integerTypeSpec, null,
                    ILUtils.build(ILLang.op_ident, "TO_BE_DEFINED"));
            nbDirsMaxVar.declarationLevelMustInclude(curDiffUnit().publicSymbolTable());
            nbDirsMaxVar.declare = false;
            if (adEnv.curUnit()!=null && adEnv.curUnit().isStandard()) { // If we do it on an Intrinsic, the NewSymbolHolder will never be solved!
                // This is only for use in CompositeTypeSpec.differentiateTypeSpec() :
                TapEnv.get().fixedNbdirsmaxTree = nbDirsMaxVar.makeNewRef(curDiffUnit().publicSymbolTable());
            }
            curDiffUnit().dirNumberMaxTree = nbDirsMaxVar.makeNewRef(curDiffUnit().publicSymbolTable()) ;
            curDiffUnit().multiDirDimensionMax =
                    new ArrayDim(ILUtils.buildDimColon(curDiffUnit().arrayDimMin,
                                                       ILUtils.copy(curDiffUnit().dirNumberMaxTree)),
                                 null, null, null, -1, 0, 0);
        }
    }

    private void resetIsDifferentiatedDeclInstructions(Unit unit) {
        TapList<Block> blocks = unit.allBlocks();
        TapList<Instruction> instructions;
        Block block;
        Instruction instruction;
        while (blocks != null) {
            block = blocks.head;
            instructions = block.instructions;
            while (instructions != null) {
                instruction = instructions.head;
                instruction.isDifferentiated = false;
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }
    }

    private void insertADBanner(Unit unit, Unit diffUnit,
                                ActivityPattern activityPattern, String modeString,
                                String line1, BoolVector exitActives,
                                String line2, BoolVector callActives,
                                BoolVector reqXRequired, BoolVector reqXManaged, boolean noFwd) {
        BoolVector killedZones = null;
        if (!TapEnv.doActivity() && !adEnv.curUnitIsContext) {
            callActives = unit.focusToKind(unit.unitInOutPossiblyR(), adEnv.diffKind) ;
            exitActives = unit.focusToKind(unit.unitInOutPossiblyW(), adEnv.diffKind) ;
            killedZones = unit.unitInOutCertainlyW() ;
        }
        BoolMatrix deps = activityAnalyzer().getDiffDeps(unit);
        if (exitActives != null && callActives != null && deps != null) {
            exitActives = exitActives.copy();
            callActives = callActives.copy();
            updateVariedUsefulWithDepsAndDiffMode(callActives, exitActives, unit, deps);
        }
        TapList<Tree> hdLines = new TapList<>(null, null);
        TapList<Tree> tlLines = hdLines;
        if (unit.language() == TapEnv.C) {
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, ""));
        }
        BoolVector diffFormalRequired =
                (!TapEnv.doActivity() || activityAnalyzer() == null
                        ? null
                        : ADActivityAnalyzer.functionDiffFormalRequired(adEnv.curActivity(), false));
        String str = "  Differentiation of " + unit.name() + modeString;
        if (!TapEnv.optionsString().isEmpty()) {
            str = str + " (with options" + TapEnv.optionsString() + ')';
        }
        str = str + ':';
        tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
        TapList<String> callActiveNames = null;
        TapList<String> exitActiveNames = null;
        TapList<String> reqXNames = null;
        TapList<String> rwStatus = null;
        int diffKindLength = unit.publicZonesNumber(adEnv.diffKind) ;
        ZoneInfo zoneInfo;
        int dki ;
// System.out.println("BUILDING BANNER FOR "+unit+" HAVING SHAPE:") ;
// for (int i = 0 ; i<nbPubZ ;++i) {
// System.out.println("  "+i+":"+unit.paramElemZoneInfo(i)) ;
// }
        for (int i=unit.paramElemsNb()-1 ; i>=0 ; --i) {
            zoneInfo = unit.paramElemZoneInfo(i);
            dki = zoneInfo.kindZoneNb(adEnv.diffKind) ;
            String varName = null;
            boolean indepz, depz, killedz;
            if (zoneInfo.zoneNb>=adEnv.srcCallGraph().numberOfDummyRootZones
                // was using test !adEnv.diffCallGraph().isAPtrUtilityZone(zoneInfo.zoneNb)
                && !zoneInfo.isChannelOrIO) {
                if (zoneInfo.description!=null &&
                    (zoneInfo.isHidden
                     // cosmetic: we want more detailled description when on a sub-procedure...
                     || (unit.upperLevelUnit!=null && !unit.upperLevelUnit.isPackage()))) {
                    varName = zoneInfo.description ;
                } else if (zoneInfo.accessTree!=null) {
                    varName = zoneInfo.accessTreePrint(unit.language());
                    if (zoneInfo.isHidden) varName = "(Hidden)"+varName ;
                }
// System.out.println(" PRODUCES NAME: "+varName) ;
            }
            if (varName!=null
                && !(zoneInfo.ownerSymbolDecl != null && zoneInfo.ownerSymbolDecl.isSystemPredefined())) {
                indepz = (callActives != null && dki!=-1 && callActives.get(dki)) ;
                depz = (exitActives != null && dki!=-1 && exitActives.get(dki)) ;
                killedz = (killedZones != null && killedZones.get(i));
                String inStatus = null, outStatus = null;
                if (dki!=-1) {
                  if (curDiffUnitSort()==DiffConstants.ADJOINT) {
                    if (TapEnv.doActivity()) {
                        if (depz) {
                            inStatus = "in";
                        }
                        int columnType = -1;
                        if (deps!=null) {
                            columnType = depsColumnType(deps, dki);
                        }
                        switch (columnType) {
                            case 0: // column full of 0
                                outStatus = indepz ? "zero" : depz ? "killed" : null;
                                break;
                            case 1: // column Identity + all 0
                                break;
                            case 2: // column Identity + not all 0
                                outStatus = (indepz ? (depz ? "incr" : "out") : (depz ? "killed" : null));
                                inStatus = null;
                                break;
                            default: // other column (non-Identity non-0)
                                outStatus = indepz ? "out" : depz ? "killed" : null;
                                break;
                        }
                    } else {// no activity case:
                        inStatus = depz ? "in" : indepz ? "incr" : null;
                        outStatus = depz ? !indepz && killedz ? "zero" : "out" : null;
                    }
                  } else {
                    if (TapEnv.doActivity()) {
                        if (indepz) {
                            inStatus = "in";
                        }
                        if (deps!=null && (deps.getRow(dki)==null || zoneInfo.passesByValue(unit, unit.language()))) {
                            // do nothing...
                        } else if (deps!=null && deps.getRow(dki).isFalse(diffKindLength)) {
                            outStatus = depz ? "zero" : indepz ? "killed" : null;
                        } else {
                            outStatus = depz ? "out" : indepz ? "killed" : null;
                        }
                    } else {// no activity case:
                        inStatus = indepz ? "in" : null;
                        outStatus = depz ? "out" : null;
                    }
                  }
                }
                if (indepz) {
                    callActiveNames = new TapList<>(varName, callActiveNames);
                }
                if (depz) {
                    exitActiveNames = new TapList<>(varName, exitActiveNames);
                }
                String statusTxt = null;
                if (inStatus != null || outStatus != null) {
                    statusTxt = (inStatus==null
                                 ? outStatus
                                 : (outStatus==null ? inStatus : inStatus+"-"+outStatus));
                } else if (diffFormalRequired!=null && diffFormalRequired.get(i) && !zoneInfo.comesFromAllocate()) {
                    statusTxt = "(loc)";
                }
                if (statusTxt != null) {
                    rwStatus = new TapList<>(varName + ':' + statusTxt, rwStatus);
                }
                // locz true iff the diff variable is needed neither in nor out,
                // but only as the location for future derivatives:
                boolean managz = reqXManaged != null && reqXManaged.get(i);
                boolean requrz = reqXRequired != null && reqXRequired.get(i);
                if (managz || requrz) {
                    statusTxt = (managz ? (requrz ? "in-out" : "out") : "in");
                    reqXNames = new TapList<>(varName + ':' + statusTxt, reqXNames);
                }
            }
        }
        if (!TapEnv.doActivity() && !adEnv.curUnitIsContext && exitActiveNames == null) {
            exitActiveNames = new TapList<>("(none)", null);
        }
        if (exitActiveNames != null) {
            str = line1;
            while (exitActiveNames != null) {
                if (str.length() > 60) {
                    tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
                    str = "               ";
                }
                str = str + ' ' + exitActiveNames.head;
                exitActiveNames = exitActiveNames.tail;
            }
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
        }
        if (!TapEnv.doActivity() && !adEnv.curUnitIsContext && callActiveNames == null) {
            callActiveNames = new TapList<>("(none)", null);
        }
        if (callActiveNames != null) {
            str = line2;
            while (callActiveNames != null) {
                if (str.length() > 60) {
                    tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
                    str = "               ";
                }
                str = str + ' ' + callActiveNames.head;
                callActiveNames = callActiveNames.tail;
            }
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
        }
        if (!TapEnv.doActivity() && !adEnv.curUnitIsContext && rwStatus == null) {
            rwStatus = new TapList<>("(none)", null);
        }
        if (noFwd && rwStatus != null &&
                (activityPattern.diffPattern() != null || TapList.contains(adEnv.rootUnits, unit))) {
            str = "   RW status of diff variables:";
            while (rwStatus != null) {
                if (str.length() > 60) {
                    tlLines = tlLines.placdl(ILUtils.build(
                            ILLang.op_stringCst, str));
                    str = "               ";
                }
                str = str + ' ' + rwStatus.head;
                rwStatus = rwStatus.tail;
            }
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
        }
        if (reqXNames != null) {
            str = "   Plus diff mem management of:";
            while (reqXNames != null) {
                if (str.length() > 60) {
                    tlLines = tlLines.placdl(ILUtils.build(
                            ILLang.op_stringCst, str));
                    str = "               ";
                }
                str = str + ' ' + reqXNames.head;
                reqXNames = reqXNames.tail;
            }
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, str));
        }
        if (unit.language() == TapEnv.C) {
            tlLines = tlLines.placdl(ILUtils.build(ILLang.op_stringCst, ""));
        }
        int commentsOperator = ILLang.op_comments;
        if (unit.language() == TapEnv.C) {
            commentsOperator = ILLang.op_commentsBlock;
        }
        Tree preComments = diffUnit.preComments;
        if (diffUnit.language() == TapEnv.C) {
            preComments = diffUnit.preCommentsBlock;
        }
        preComments = ILUtils.appendComments(ILUtils.build(commentsOperator, hdLines.tail),
                preComments);
        if (diffUnit.language() == TapEnv.C) {
            diffUnit.preCommentsBlock = preComments;
        } else {
            diffUnit.preComments = preComments;
        }
    }

    /**
     * Refine zones varied upon entry into the unit "variedOnCall" and zones
     * useful upon exit from the unit "usefulOnExit", considering the
     * dependencies "deps" and the differentiation mode.
     * This refinement is only used for the comments in the diff header,
     * so it may use the differentiation mode, and it is more "aggressive"
     * than the slightly weaker refinement truly used for actual differentiation,
     * defined in ADActivityAnalyzer.updateVariedUsefulWithDeps().
     * <p>
     * In TANGENT_MODE:<br>
     * -&gt; Remove from "variedOnCall" each zone whose derivative is not
     * necessary for any "usefulOnExit". Also remove zones
     * which are influencing only themselves as Identity.
     * Implementation: remove zones for which the column in deps
     * contains only zeros or Id in rows that go to a usefulOnExit.<br>
     * -&gt; Remove from "usefulOnExit" each zone whose derivative is not
     * modified depending on a "variedOnCall" (or only as Identity of itself)
     * Overwrite of 0.0 by 0.0 doesn't count as modified !
     * Implementation: remove zones that either have an Identity row
     * or (are not variedOnCall and have a row that depends on no variedOnCall zone).
     * In REVERSE_MODE (different because of transposition...):<br>
     * -&gt; Remove from "usefulOnExit" each zone whose derivative is not
     * necessary for any "variedOnCall".<br>
     * -&gt; Remove from "variedOnCall" each zone whose derivative is not
     * modified.
     */
    private void updateVariedUsefulWithDepsAndDiffMode(BoolVector variedOnCall, BoolVector usefulOnExit,
                                                       Unit unit, BoolMatrix deps) {
        if (curDiffUnitSort()==DiffConstants.ADJOINT) {
            BoolVector modifiedFromUseful = activityAnalyzer().getUpdateModifiedZonesT(deps, usefulOnExit);
            usefulOnExit.cumulAnd(getReadZonesT(deps, variedOnCall, usefulOnExit));
            variedOnCall.cumulAnd(modifiedFromUseful);
        } else {
            // => curDiffUnitSort()==TANGENT
            BoolVector modifiedFromVaried = activityAnalyzer().getUpdateModifiedZones(deps, variedOnCall, unit);
            variedOnCall.cumulAnd(getReadZones(deps, usefulOnExit));
            if (TapEnv.debugAdMode() != TapEnv.ADJ_DEBUG) {
                //[llh] maybe the next cumulAnd is useless in all cases?
                usefulOnExit.cumulAnd(modifiedFromVaried);
            }
        }
        activityAnalyzer().putBackSharingZones(variedOnCall, usefulOnExit, unit);
    }

    /**
     * @return the BoolVector of zones read by the derivative of a
     * subroutine of dependence matrix "deps". Sets true to each column index j
     * for which there is a non-identity line i in "deps" such that zone i is
     * usefulOnExit and deps(i,j) is true.
     */
    private BoolVector getReadZones(BoolMatrix deps, BoolVector usefulOnExit) {
        // Note: not sure we can use variedOnCall here ??
        BoolVector result = new BoolVector(deps.getNCols());
        for (int i = deps.getNRows() - 1; i >= 0; --i) {
            if (deps.getRow(i) != null && usefulOnExit.get(i)) {
                result.cumulOr(deps.getRow(i));
            }
        }
        return result;
    }

    /**
     * @return the BoolVector of zones read by the adjoint of a subroutine
     * of dependence matrix "deps". Sets true for all non-null index in a non-Id
     * row of deps (provided the row is usefulOnExit and the column
     * variedOnCall), and also for all Id row for which there is a non-null
     * element in the corresponding column.
     */
    private BoolVector getReadZonesT(BoolMatrix deps,
                                     BoolVector variedOnCall, BoolVector usefulOnExit) {
        BoolVector result = new BoolVector(deps.getNCols());
        BoolVector columnsRead = new BoolVector(deps.getNCols());
        BoolVector variedRow;

        for (int i = deps.getNRows() - 1; i >= 0; --i) {
            if (deps.getRow(i) != null && usefulOnExit.get(i)) {
                variedRow = deps.getRow(i).and(variedOnCall);

                if (!variedRow.isFalse(deps.getNCols())) {
                    result.set(i, true);
                    columnsRead.cumulOr(variedRow);
                }
            }
        }
        for (int j = deps.getNCols() - 1; j >= 0; --j) {
            if (deps.getRow(j) == null && columnsRead.get(j)) {
                result.set(j, true);
            }
        }
        return result;
    }

    private int depsColumnType(BoolMatrix deps, int j) {
        boolean hasId = false;
        boolean hasNonZero = false;
        for (int i = deps.getNRows() - 1; i >= 0; --i) {
            if (deps.isImplicitIdentityRow(i)) {
                if (i == j) {
                    hasId = true;
                }
            } else if (deps.get(i, j)) {
                hasNonZero = true;
            }
        }
        return hasNonZero ? hasId ? 2 : 3 : hasId ? 1 : 0;
    }

    /**
     * @return true when one of the TapPair's has a non-null first element.
     */
    private boolean oneFirstIsNonNull(TapList<TapPair<FGArrow, TapList<BwdSwitchCase>>> list) {
        boolean result = false;
        while (!result && list != null) {
            result = list.head.first != null;
            list = list.tail;
        }
        return result;
    }

    protected void declareDiffIsReturnVar(VariableDecl returnVarDecl, Unit unit, Unit diffUnit) {
        NewSymbolHolder symbolHolder = returnVarDecl.getDiffSymbolHolder(curDiffVarSort(), null, 0);
        if (symbolHolder == null) {
            varRefDifferentiator().diffSymbolName(adEnv.curActivity(), returnVarDecl.symbol,
                    diffUnit.publicSymbolTable(), true, false, false, null, null,
                    false, curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, null);
            symbolHolder = returnVarDecl.getDiffSymbolHolder(curDiffVarSort(), null, 0);
            symbolHolder.declarationLevelMustInclude(diffUnit.publicSymbolTable());
            symbolHolder.solvingLevelMustInclude(diffUnit.publicSymbolTable());
            symbolHolder.makeNewRef(diffUnit.publicSymbolTable());
        }
        VariableDecl diffReturnVarDecl = symbolHolder.newVariableDecl();
        if (diffReturnVarDecl != null) {
            diffReturnVarDecl.setReturnVar();
            if (unit.otherReturnVar() != null) {
                diffUnit.setOtherReturnVar(diffReturnVarDecl);
            }
        }
    }

    private void makeSurePrivate(Tree ident, Tree clauses) {
        ident = ILUtils.baseTree(ident);
        String name = ILUtils.getIdentString(ident);
        boolean found = false;
        Tree[] allClauses = clauses.children();
        for (int i = allClauses.length - 1; i >= 0 && !found; --i) {
            if (allClauses[i].opCode() == ILLang.op_privateVars) {
                found = ILUtils.contains(allClauses[i].down(1), name, false);
            }
        }
        if (!found) {
            clauses.addChild(
                    ILUtils.build(ILLang.op_privateVars,
                            ILUtils.build(ILLang.op_idents,
                                    ILUtils.copy(ident))),
                    -1);
        }
    }

    private boolean opensCurrentControl(HeaderBlock block) {
        if (block.parallelControls == null) {
            return false;
        }
        FGArrow entryArrow = null;
        for (TapList<FGArrow> arrivingArrows = block.backFlow();
             arrivingArrows != null && entryArrow == null;
             arrivingArrows = arrivingArrows.tail) {
            if (!arrivingArrows.head.inACycle) {
                entryArrow = arrivingArrows.head;
            }
        }
        return entryArrow != null && entryArrow.origin.isParallelController() &&
                entryArrow.origin.instructions.head == block.parallelControls.head;
    }

    /**
     * INSTRUMENTING DIFFERENTIATED CODE FOR DEBUG-AD:
     * Insert debug AD instructions at the beginning and end.
     */
    private void instrumentTraceOnUnit(Unit unit, Block upstreamBlock, Block downstreamBlock,
                                       FlowGraphDifferentiationEnv diffEnv) {
        int diffKindLength, allKindLength ;
        if (unit.publicZonesNumber4!=null) { //TODO: this should become the only case!
            diffKindLength = unit.publicZonesNumber(adEnv.diffKind) ;
            allKindLength =  unit.publicZonesNumber(SymbolTableConstants.ALLKIND) ;
        } else {
            diffKindLength = unit.publicSymbolTable().declaredZonesNb(adEnv.diffKind) ;
            allKindLength =  unit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        }
        SymbolTable symbolTable = unit.publicSymbolTable() ;
        Tree dbadTree;

        // Instrument variables active at the upstream point:
        if (upstreamBlock != null) {
            int entryBlockInfoLen = unit.entryBlock().symbolTable.declaredZonesNb(adEnv.diffKind);
            BoolVector activeAtBegin = adEnv.unitActivities == null ? null :
                    adEnv.unitActivities.retrieve(unit.entryBlock()).head;
            if (!TapEnv.doActivity() && activeAtBegin == null) { // no-activity case
                activeAtBegin = new BoolVector(entryBlockInfoLen);
                activeAtBegin.setTrue();
            }
            // For the DD test, trace active vars only if they are read !
            //  Special case: if "debugPassives" this patch would interfere as it would
            //   cause placing "passive" debug tests on really active variables!
            if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG && !TapEnv.get().debugPassives) {
                BoolVector read = DataFlowAnalyzer.changeKind(unit.unitInOutPossiblyR(),
                                    allKindLength, SymbolTableConstants.ALLKIND, diffKindLength, adEnv.diffKind, symbolTable) ;
                activeAtBegin = activeAtBegin.copy();
                activeAtBegin.cumulAnd(read);
            }
            adEnv.setCurInstruction(upstreamBlock.firstInstrAfterDecls());
            dbadTree = placeDebugADTests(curDiffUnit(), null, activeAtBegin, -1,
                    symbolTable, symbolTable.declaredZonesNb(adEnv.diffKind),
                    "entry", null,
                    ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE"),
                    ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE"));
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace(" using activity vector at begin:" + activeAtBegin);
                TapEnv.printlnOnTrace("!Augment " + upstreamBlock + " with debug-instrument actives at upstream end : " + ILUtils.toString(dbadTree, adEnv.curActivity()));
            }
            upstreamBlock.addInstrHdAfterDecls(dbadTree);
        }

        // Instrument variables active at the downstreampoint:
        if (downstreamBlock != null) {
            Block prevBlock;
            boolean needTerminationBlock = false;
            BoolVector overwritten = null;
            if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG) {
                overwritten = DataFlowAnalyzer.changeKind(unit.unitInOutPossiblyW(),
                                  allKindLength, SymbolTableConstants.ALLKIND, diffKindLength, adEnv.diffKind, symbolTable) ;
            }
            TapList<Block> lastNonEmptyBlocks =
                    collectLastNonEmptyBlocks(downstreamBlock.backFlow(), null, new TapList<>(null, null));
            while (lastNonEmptyBlocks != null) {
                prevBlock = lastNonEmptyBlocks.head;
                if (prevBlock.lastTest() == ILLang.op_return) {
                    // Ugly manipulation to retrieve the activity just before this op_return instruction:
                    Block origOfPrevBlock = diffEnv.retrieveOrigBlockOf(prevBlock);
                    if (origOfPrevBlock == null) // May happen if prevBlock is an activitySwitch Block.
                    {
                        origOfPrevBlock = TapList.cassq(prevBlock.lastInstr(), realOrigBlockOfReturns);
                    }
                    int returnBlockInfoLen = origOfPrevBlock.symbolTable.declaredZonesNb(adEnv.diffKind);
                    BoolVector activeAtReturn = null;
                    if (adEnv.unitActivities != null) {
                        TapList<BoolVector> blockActivities = adEnv.unitActivities.retrieve(origOfPrevBlock);
                        activeAtReturn = TapList.last(blockActivities);
                    }
                    activeAtReturn = patchActivityForDebug(activeAtReturn, returnBlockInfoLen,
                            overwritten, diffKindLength);
                    adEnv.setCurInstruction(prevBlock.lastInstr());
                    dbadTree = placeDebugADTests(curDiffUnit(), null, activeAtReturn, 1,
                            origOfPrevBlock.symbolTable, symbolTable.declaredZonesNb(adEnv.diffKind),
                            "exit", origOfPrevBlock.lastInstr(),
                            ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE"),
                            ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE"));
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace(" using activity vector at return:" + activeAtReturn);
                        TapEnv.printlnOnTrace("!Augment " + prevBlock + " with debug-instrument actives at downstream end, before return : " + ILUtils.toString(dbadTree, adEnv.curActivity()));
                    }
                    prevBlock.addInstructionAt(dbadTree, -2);
                } else {
                    needTerminationBlock = true;
                }
                lastNonEmptyBlocks = lastNonEmptyBlocks.tail;
            }
            if (needTerminationBlock) {
                BoolVector activeAtEnd = null;
                if (adEnv.unitActivities != null) {
                    activeAtEnd = adEnv.unitActivities.retrieve(unit.exitBlock()).head;
                }
                int exitBlockInfoLen = unit.exitBlock().symbolTable.declaredZonesNb(adEnv.diffKind);
                activeAtEnd = patchActivityForDebug(activeAtEnd, exitBlockInfoLen,
                        overwritten, diffKindLength);
                adEnv.setCurInstruction(downstreamBlock.lastInstr());
                dbadTree = placeDebugADTests(curDiffUnit(), null, activeAtEnd, 1,
                        symbolTable, symbolTable.declaredZonesNb(adEnv.diffKind),
                        "exit", null,
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE"),
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE"));
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace(" using activity vector at end:" + activeAtEnd);
                    TapEnv.printlnOnTrace("!Augment " + downstreamBlock + " with debug-instrument actives at downstream end : " + ILUtils.toString(dbadTree, adEnv.curActivity()));
                }
                downstreamBlock.addInstrHdAfterDecls(dbadTree);
            }
        }
        adEnv.setCurInstruction(null);
    }

    private BoolVector patchActivityForDebug(BoolVector activity, int activityLen,
                                             BoolVector overwritten, int overwrittenLen) {
        if (!TapEnv.doActivity() && activity == null) {
            // no-activity case
            activity = new BoolVector(activityLen);
            activity.setTrue();
        }
        // For the DD test, trace active vars only if they were overwritten !
        //  Special case: if "debugPassives" this patch would interfere as it would
        //   cause placing "passive" debug tests on really active variables!
        if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG && !TapEnv.get().debugPassives) {
            assert activity != null;
            activity = activity.copy();
            assert overwritten != null;
            activity.cumulAnd(overwritten, overwrittenLen);
        }
        return activity;
    }

    /**
     * Collects into collectList (which is returned) all the first non-empty Blocks that can be
     * reached backwards from the given backFlow. Uses a dejaVu mechanism.
     */
    private TapList<Block> collectLastNonEmptyBlocks(TapList<FGArrow> backFlow, TapList<Block> collectList,
                                                     TapList<Block> dejaVu) {
        Block prevBlock;
        while (backFlow != null) {
            prevBlock = backFlow.head.origin;
            if (prevBlock != null && !TapList.contains(dejaVu.tail, prevBlock)) {
                dejaVu.placdl(prevBlock);
                if (prevBlock.instructions == null) {
                    collectList = collectLastNonEmptyBlocks(prevBlock.backFlow(), collectList, dejaVu);
                } else {
                    collectList = new TapList<>(prevBlock, collectList);
                }
            }
            backFlow = backFlow.tail;
        }
        return collectList;
    }

    /**
     * @param diffUnit    the differentiated Unit that contains the instrumented place
     * @param callTree    when the instrumented place is a call, this call.
     * @param nature      indicates the nature of the corresponding point in the original source, i.e.
     *                    -1:sourceEntry ; 1:sourceExit ; -2:upstreamSourceCall ; 2:downstreamSourceCall ; 0:elsewhere
     * @param returnInstr the return instruction.
     * @return the instrumentation Instructions for this nature of location and for this debug AD mode.
     */
    //TODO: MOVE TO BlockDifferentiator ?
    protected Tree placeDebugADTests(Unit diffUnit, Tree callTree,
                                     BoolVector placeActiveZones, int nature,
                                     SymbolTable srcSymbolTable, int lastDeclaredZone,
                                     String placeName, Instruction returnInstr,
                                     Tree okHere, Tree forcedOkHere) {
        Tree stackInstruction, arg;
        SymbolTable diffSymbolTable = diffUnit.privateSymbolTable();

        // must be set to true for operations that overwrite the variable they operate on:
        // must be set to true for operations that overwrite the diff variable they operate on:
        boolean modifiesDiff = false;
        String traceFuncPrefix = "adDebug" + (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG ? "Tgt_" : "Adj_");
        if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG) {
            modifiesDiff = true;  // because of possible "nudge"
        } else if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
            String extraFuncPrefix;
            modifiesDiff = true;
            if (nature == 0) {
                extraFuncPrefix = "rw";
            } else if ((nature == -1 || nature == 2) && curDiffUnitSort()==DiffConstants.TANGENT
                    ||
                    (nature == 1 || nature == -2) && curDiffUnitSort()==DiffConstants.ADJOINT) {
                extraFuncPrefix = "w";
            } else {
                extraFuncPrefix = "r";
            }
            traceFuncPrefix = traceFuncPrefix + extraFuncPrefix;
        }
        TapList<Tree> stackInstructions =
                instructionsOnDeclaredZones(callTree, srcSymbolTable, diffUnit.privateSymbolTable(),
                        (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG ? traceFuncPrefix : traceFuncPrefix + "test"),
                        diffUnit, placeActiveZones, TapEnv.debugAdMode()==TapEnv.TGT_DEBUG,
                        false, modifiesDiff, lastDeclaredZone, returnInstr);

        // Add final DISPLAY instruction:
        int deltaIndent = 0;
        if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG && (nature == 2 || nature == -2)) {
            deltaIndent = -1;
        }
        TapList<Tree> displayArgs = null;
        if (TapEnv.debugAdMode() != TapEnv.TGT_DEBUG) {
            displayArgs = new TapList<>(ILUtils.build(ILLang.op_intCst, deltaIndent), displayArgs);
        }
        arg = ILUtils.build(ILLang.op_stringCst, placeName);
        if (diffUnit.isFortran()) {
            arg = ILUtils.concatWithNull(arg);
        }
        displayArgs = new TapList<>(arg, displayArgs);
        Tree lastCall = ILUtils.buildCall(
                ILUtils.tapenadeUtilityFunctionName(diffUnit,
                        traceFuncPrefix + (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG ? "Display" : "display")),
                ILUtils.build(ILLang.op_expressions, displayArgs));
        stackInstructions =
                TapList.append(stackInstructions,
                        new TapList<>(lastCall, null));
        stackInstruction =
                ILUtils.build(ILLang.op_blockStatement, stackInstructions);
        // In both modes "Divided-Differences" and "Dot-Product",
        // enclose all instructions into a controlling test:
        String testFuncName = TapEnv.debugAdMode() == TapEnv.TGT_DEBUG ? "adDebugTgt_here" :
                curDiffUnitSort() == DiffConstants.TANGENT ? "adDebugFwd_here" : "adDebugBwd_here";
        Tree testFuncIdent = ILUtils.tapenadeUtilityFunctionName(diffUnit, testFuncName);
        Unit debugUnit = adEnv.debugADHereUnit;
        if (debugUnit == null) {
            debugUnit = adEnv.diffCallGraph().createNewUnit(null, diffUnit.language());
            adEnv.debugADHereUnit = debugUnit;
            debugUnit.setExternalSymbolTable(adEnv.diffCallGraph().languageRootSymbolTable(diffUnit.language()));
            debugUnit.setExternal();
            debugUnit.setName(ILUtils.getIdentString(testFuncIdent));
            debugUnit.setFunctionTypeSpec(
                    new FunctionTypeSpec(diffSymbolTable.getTypeDecl("boolean").typeSpec));
        }
        diffSymbolTable.addSymbolDecl(new FunctionDecl(testFuncIdent, debugUnit));
        TapList<Tree> args = null;
        if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG) {
            args = new TapList<>(forcedOkHere, args);
        }
        arg = ILUtils.build(ILLang.op_stringCst, placeName);
        if (diffUnit.isFortran()) {
            arg = ILUtils.concatWithNull(arg);
        }
        args = new TapList<>(arg, args);
        Tree testCall = ILUtils.buildCall(testFuncIdent,
                ILUtils.build(ILLang.op_expressions, args));
        if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG
            || (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG && curDiffUnitSort()==DiffConstants.ADJOINT && nature == 0)) {
            testCall = ILUtils.build(ILLang.op_and, okHere, testCall);
        }
        stackInstruction = ILUtils.build(ILLang.op_if, testCall, stackInstruction);
        if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG && curDiffUnitSort()==DiffConstants.ADJOINT) {
            arg = ILUtils.build(ILLang.op_stringCst, placeName);
            if (diffUnit.isFortran()) {
                arg = ILUtils.concatWithNull(arg);
            }
            Tree elseSkip = ILUtils.buildCall(
                    ILUtils.tapenadeUtilityFunctionName(diffUnit, "adDebugAdj_skip"),
                    ILUtils.build(ILLang.op_expressions, arg));
            stackInstruction.setChild(elseSkip, 3);
        }
        return stackInstruction;
    }

    /**
     * Build the list of the Trees of trace, debug, or initialization calls to be
     * placed around the given instrumentedSourceTree (generally a call) for each
     * active declared zone in the given symbolTable "srcSymbolTable", all these trace etc calls
     * starting with the given trace function name prefix "traceFuncPrefix".
     *
     * @param instrumentedSourceTree the source Tree of the differentiated Tree that we are instrumenting.
     * @param srcSymbolTable         the source SymbolTable of the instrumentedSourceTree
     * @param diffSymbolTable        the SymbolTable of the future created trees
     * @param placeActiveZones       the vector of all the zones that we want to instrument.
     *                               This vector follows the numbering of the declared "diffKind" zones of the context/calling function.
     * @param modifiesPrimal         Pass "true" when the created operation needs to modify the primal variable.
     * @param modifiesDiff           Pass "true" when the created operation needs to modify the diff variable.
     * @param returnInstr            the return instruction, needed to find the returned expression.
     * @return the list of the Trees of trace, debug, or initialization calls for each
     * active declared zone in the given symbolTable "srcSymbolTable", all these calls
     * starting with the given trace function name prefix "traceFuncPrefix".
     **/
    protected TapList<Tree> instructionsOnDeclaredZones(Tree instrumentedSourceTree,
                                                        SymbolTable srcSymbolTable, SymbolTable diffSymbolTable,
                                                        String traceFuncPrefix, Unit diffUnit, BoolVector placeActiveZones,
                                                        boolean adaptForDD, boolean modifiesPrimal, boolean modifiesDiff,
                                                        int lastDeclaredZone, Instruction returnInstr) {
        //TODO: MOVE TO ProcedureCallDifferentiator ?
        TapList<Tree> stackInstructions = null;
        ZoneInfo zoneInfo;
        Tree zoneInfoVisibleAccessTree, stackInstruction;
        BoolVector remainToComplainAbout = (placeActiveZones == null ? null : placeActiveZones.copy()) ;
        Instruction instr = adEnv.curInstruction();
        if (instr == null || instr.block == null || instr.block.pointerInfosIn == null) {
            instr = null;
        }
        TapIntList seenZones, seenZonesRanks;
        int instrumentIt;
        for (int z = lastDeclaredZone - 1; z >= 0; --z) {
            zoneInfo = srcSymbolTable.declaredZoneInfo(z, adEnv.diffKind);
            if (zoneInfo != null && !zoneInfo.comesFromAllocate() && !zoneInfo.isHidden) {
                if (adEnv.curUnitIsContext && adEnv.activeRootCalledFromContext || instr == null) {
                    // case of activeRootCalledFromContext: the "artificial" activity is placed on the local destination zone of pointers
                    //  see comment of DataFlowAnalyzer.propagateDataToCallSite()
                    // case null instr: dubious fallback...
                    seenZones = new TapIntList(zoneInfo.zoneNb, null);
                } else {
                    // Standard case: follow again this zone's access Tree with the current pointer destiations.
                    //TODO: use the "upstream" info!!
                    seenZones = srcSymbolTable.listOfZonesOfValue(zoneInfo.accessTree, null, instr);
                }
                seenZonesRanks =
                        DataFlowAnalyzer.mapZoneRkToKindZoneRk(seenZones, adEnv.diffKind, srcSymbolTable);
                instrumentIt = 0;
                if (placeActiveZones != null) {
                    if (placeActiveZones.intersects(seenZonesRanks)) {
                        instrumentIt = 1; // means instrument as an active variable.
                    } else if (TapEnv.get().debugPassives && TapEnv.debugAdMode() == TapEnv.TGT_DEBUG && !adEnv.curUnitIsContext) {
                        instrumentIt = 2; // means instrument as a passive variable.
                    }
                }
                if (instrumentIt != 0) {
                    if (zoneInfo.isResult() && diffUnit.isC() && curDiffUnitSort() == DiffConstants.TANGENT) {
                        zoneInfoVisibleAccessTree = returnInstr.tree.down(1);
                    } else {
                        zoneInfoVisibleAccessTree = srcSymbolTable.buildZoneVisibleAccessTree(zoneInfo);
                    }
                    if (zoneInfoVisibleAccessTree != null) {
                        remainToComplainAbout.set(seenZonesRanks, false);
                        Tree baseVar = ILUtils.baseTree(zoneInfoVisibleAccessTree);
                        String baseName = ILUtils.getIdentString(baseVar);
                        stackInstruction = RefDescriptor.makeStack(diffUnit,
                                traceFuncPrefix + (instrumentIt == 1 ? "" : "passive"),
                                false, zoneInfo, zoneInfoVisibleAccessTree,
                                srcSymbolTable, diffSymbolTable, true, -1);
                        if (stackInstruction != null) {
                            if (instrumentIt == 1) {
                                Tree diffBaseVar =
                                        varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVar, diffSymbolTable,
                                                true, false, false, null, false,
                                                curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, null);
                                if (adaptForDD) {
                                    adaptForDD(stackInstruction, diffUnit, baseVar, diffBaseVar,
                                            baseName, modifiesPrimal, modifiesDiff);
                                } else {
                                    adaptForDP(stackInstruction, diffUnit, baseVar, diffBaseVar,
                                            modifiesPrimal, modifiesDiff);
                                }
                            } else if (instrumentIt == 2) {
                                adaptForPassiveDD(stackInstruction, diffUnit, baseVar, baseName);
                            }
                            stackInstructions = new TapList<>(stackInstruction, stackInstructions);
                        }
                    }
                }
            }
        }
        // complain about remaining zones that could not be instrumented:
        for (int z = lastDeclaredZone - 1; z >= 0; --z) {
            if (remainToComplainAbout != null && remainToComplainAbout.get(z)) {
                zoneInfo = srcSymbolTable.declaredZoneInfo(z, adEnv.diffKind);
                if (zoneInfo != null && !zoneInfo.isChannelOrIO && !zoneInfo.comesFromAllocate()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrumentedSourceTree, "(AD14) Instrumenting differentiated call of " + ILUtils.toString(instrumentedSourceTree) + " needs to access hidden variable: "+zoneInfo.description) ;
                    adEnv.addInHiddenInstrumentedVariables(zoneInfo);
                }
            }
        }

        return stackInstructions;
    }

    private void adaptForDP(Tree code, Unit diffUnit, Tree var, Tree diffVar,
                            boolean modifiesPrimal, boolean modifiesDiff) {
        switch (code.opCode()) {
            case ILLang.op_loop:
                adaptForDP(code.down(4), diffUnit, var, diffVar, modifiesPrimal, modifiesDiff);
                break;
            case ILLang.op_if:
                adaptForDP(code.down(2), diffUnit, var, diffVar, modifiesPrimal, modifiesDiff);
                break;
            case ILLang.op_blockStatement: {
                Tree[] statements = code.children();
                for (int i = statements.length - 1; i >= 0; i--) {
                    adaptForDP(statements[i], diffUnit, var, diffVar, modifiesPrimal, modifiesDiff);
                }
                break;
            }
            case ILLang.op_call: {
                Tree arg1 = ILUtils.getArguments(code).cutChild(1);
                arg1 = changeRootByDiff(arg1, var, diffVar);
                // if diffUnit is C, calls to adDebugAdj_[w|rw]*() on scalar variables
                // must pass the address of the variable, because it will be modified:
                if (diffUnit.isC() && !ILUtils.getCalledNameString(code).endsWith("Array") && modifiesDiff) {
                    arg1 = ILUtils.addAddressOf(arg1);
                }
                ILUtils.getArguments(code).setChild(arg1, 1);
                break;
            }
            default:
                break;
        }
    }

    private void adaptForDD(Tree code, Unit diffUnit, Tree var, Tree diffVar, String name,
                            boolean modifiesPrimal, boolean modifiesDiff) {
        switch (code.opCode()) {
            case ILLang.op_loop:
                adaptForDD(code.down(4), diffUnit, var, diffVar, name, modifiesPrimal, modifiesDiff);
                break;
            case ILLang.op_if:
                adaptForDD(code.down(2), diffUnit, var, diffVar, name, modifiesPrimal, modifiesDiff);
                break;
            case ILLang.op_blockStatement: {
                Tree[] statements = code.children();
                for (int i = statements.length - 1; i >= 0; i--) {
                    adaptForDD(statements[i], diffUnit, var, diffVar, name, modifiesPrimal, modifiesDiff);
                }
                break;
            }
            case ILLang.op_call: {
                Tree debugArguments = ILUtils.getArguments(code);
                if (!TapEnv.get().complexStep) {
                    Tree diffTree = ILUtils.copy(debugArguments.down(1));
                    Tree tree2 = changeRootByDiff(diffTree, var, diffVar);
                    if (TapEnv.associationByAddress()) {
                        Tree tree1 = debugArguments.down(1);
                        tree1 = varRefDifferentiator().buildAAInstructionVORD(tree1,
                                    diffUnit.privateSymbolTable(), tree1, TapEnv.assocAddressValueSuffix(), false);
                        debugArguments.setChild(tree1, 1);
                        tree2 = varRefDifferentiator().buildAAInstructionVORD(tree2,
                                    diffUnit.privateSymbolTable(), tree2, TapEnv.assocAddressDiffSuffix(), false);
                    }
                    debugArguments.addChild(tree2, 2);
                }
                // if diffUnit is C, calls (on scalar variables) to
                // adContext[Tgt|Adj]_init*() or to adDebugTgt_[init|r|w|rw]*()
                // and even to adDebugTgt_test* (because of the possibility to "nudge" the diff variable),
                // must pass the address of the differentiated variable, because it may be modified:                
                if (diffUnit.isC() && !ILUtils.getCalledNameString(code).endsWith("Array")) {
                    if (modifiesPrimal) {
                        debugArguments.setChild(ILUtils.addAddressOf(debugArguments.cutChild(1)), 1);
                    }
                    if (modifiesDiff && !TapEnv.get().complexStep) {
                        debugArguments.setChild(ILUtils.addAddressOf(debugArguments.cutChild(2)), 2);
                    }
                }
                Tree varName = ILUtils.build(ILLang.op_stringCst, name);
                if (diffUnit.isFortran()) {
                    varName = ILUtils.concatWithNull(varName);
                }
                debugArguments.addChild(varName, 1);
                break;
            }
            default:
                break;
        }
    }

    private void adaptForPassiveDD(Tree code, Unit diffUnit, Tree var, String name) {
        switch (code.opCode()) {
            case ILLang.op_loop:
                adaptForPassiveDD(code.down(4), diffUnit, var, name);
                break;
            case ILLang.op_if:
                adaptForPassiveDD(code.down(2), diffUnit, var, name);
                break;
            case ILLang.op_blockStatement: {
                Tree[] statements = code.children();
                for (int i = statements.length - 1; i >= 0; i--) {
                    adaptForPassiveDD(statements[i], diffUnit, var, name);
                }
                break;
            }
            case ILLang.op_call: {
                Tree varName = ILUtils.build(ILLang.op_stringCst, name);
                if (diffUnit.isFortran()) {
                    varName = ILUtils.concatWithNull(varName);
                }
                ILUtils.getArguments(code).addChild(varName, 1);
                break;
            }
            default:
                break;
        }
    }

    private static Tree changeRootByDiff(Tree expr, Tree var, Tree diffVar) {
        switch (expr.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return ILUtils.build(expr.opCode(),
                        changeRootByDiff(expr.down(1), var, diffVar),
                        ILUtils.copy(expr.down(2)));
            case ILLang.op_ident:
                if (ILUtils.getIdentString(expr).equals(ILUtils.getIdentString(var))) {
                    return ILUtils.copy(diffVar);
                } else {
                    return ILUtils.copy(expr);
                }
            default:
                return ILUtils.copy(expr);
        }
    }
}
