/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.InstructionMask;
import fr.inria.tapenade.representation.IterDescriptor;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.RefDescriptor;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.Tree;

public final class NewBlockGraphNode {
    public String text = "unspecified text";
    public TapList<NewBlockGraphArrow> flow;
    public TapList<NewBlockGraphArrow> backFlow;
    public int rank;
    public int subOrder;
    public int nbDeps;
    protected Instruction srcInstruction;
    /**
     * When srcInstruction comes from an include, the include'ing Instruction.
     */
    protected Instruction fromInclude;

    /**
     * The final Instruction that will be generated for this node.
     */
    protected Instruction diffInstruction;

    /**
     * When the trees are created at NewBlockGraphNode creation time
     * (e.g. for kind==PLAIN, as opposed to kind==SET_VARIABLE or INCREMENT_VARIABLE)
     * this field holds the list of these trees.
     * This holds only one Tree at the beginning, but may hold several trees after node's are merged.
     */
    protected TapList<Tree> plainTrees;

    /**
     * The final Tree of the instruction for this Node.
     * May be given at creation time (e.g. when kind==PLAIN)
     * Alternatively, may be null (e.g. when kind==SET_VARIABLE or INCREMENT_VARIABLE)
     * and will be deduced from the contents of whereMask, assigned, and diffValue.
     */
    protected Tree tree;

    /**
     * The where context that must be applied to the final tree.
     */
    protected InstructionMask whereMask;

    /**
     * The variable that must be assigned or incremented by the final instruction.
     */
    protected Tree assignedRef;

    /**
     * The primal variable whose diff is "assignedRef".
     */
    protected Tree primRecv;
    /** The replica number that was used to build "assignedRef" from "primRecv". */
    protected int iReplic = 0 ;

    /**
     * The general kind of the final Tree.
     * In {PLAIN, INCREMENT_VARIABLE, SET_VARIABLE}.
     */
    protected int kind;

    /**
     * True if this instruction reads a value that it then overwrites.
     * Default is true because this is safest.
     */
    protected boolean overwritesUsed = true;

    /**
     * true if node must run in multi-dir style, i.e. in a loop on directions.
     */
    protected boolean multiDir;

    /**
     * The expression that is assigned or added into the assigned tree.
     */
    protected Tree diffValue;

    /**
     * The origin Tree in the source Unit.
     */
    private Tree origTree;

    /**
     * The prefix comments that must be attached to the future Instruction of this node.
     * postfix comments are always null.
     */
    protected Tree preComments;

    /**
     * The prefix comments that must be attached to the future Instruction of this node.
     * postfix comments are always null.
     */
    protected Tree preCommentsBlock;

    protected NewBlockGraphNode(int kind, Tree tree, boolean multiDir, Instruction srcInstruction, InstructionMask whereMask) {
        this.kind = kind;
        this.tree = tree;
        if (tree != null) {
            plainTrees = new TapList<>(tree, null);
        }
        this.multiDir = multiDir;
        this.srcInstruction = srcInstruction;
        this.whereMask = whereMask;
        // srcInstruction may be null for the "Recompute forward" case:
        this.origTree = srcInstruction == null ? null : srcInstruction.tree;
    }


    /**
     * @return true if the fusion of "this"
     * followed by "otherNode" is possible and profitable.
     */
    protected boolean interestingFuseWith(NewBlockGraphNode otherNode, NewBlockGraph thisGraph) {
        if (diffInstruction == null) {
            // if diffInstruction is null, this means we are
            // in the 1st condensation phase, fusing increment assignments:
            //                   && (protectionNumber()+node2.protectionNumber() < 2)
            return
                // Fusion of increments/sets of adjoint variables:
                ((kind == DiffConstants.SET_VARIABLE || kind == DiffConstants.INCREMENT_VARIABLE)
                 && ((otherNode.kind == DiffConstants.SET_VARIABLE && !otherNode.overwritesUsed)
                     || otherNode.kind == DiffConstants.INCREMENT_VARIABLE)
                 // The following test must be verified: we compare diff variables, whose final name is still not chosen!
                 && assignedRef.equalsTree(otherNode.assignedRef)
                 && NewSymbolHolder.sameNSH(assignedRef, otherNode.assignedRef) //Don't fuse different "tempb" variables!
                 && multiDir == otherNode.multiDir
                 && InstructionMask.equalMasks(whereMask, otherNode.whereMask)
                 // In Fortran, we must not merge two "ifExpression"s, because there is no easy Fortran code for this:
                 && !(otherNode.kind == DiffConstants.INCREMENT_VARIABLE
                      && thisGraph.adEnv.curDiffUnit.isFortran()
                      && ILUtils.isIfExpression(this.diffValue)
                      && ILUtils.isIfExpression(otherNode.diffValue)))
                ||
                // Fusion of plain nodes, e.g. multi-directional tangent diff instructions:
                (kind == DiffConstants.PLAIN
                 && otherNode.kind == DiffConstants.PLAIN
                 && whereMask == otherNode.whereMask
//[llh 7/2/2013] following code is probably better, but breaks many regression tests (e.g. on #includes).
//                  && multiDir==node2.multiDir && (multiDir || (whereMask!=null && !whereMask.isEmpty()))
// ... so we keep the older code:
                 && multiDir && otherNode.multiDir);
        } else {
            // if diffInstruction is non-null, this means we are
            // in the 2nd condense phase, dealing with WHERE statements !!
            return diffInstruction.tree != null &&
                    otherNode.diffInstruction.tree != null &&
                    diffInstruction.whereMask() == otherNode.diffInstruction.whereMask() &&
                    multiDir == otherNode.multiDir;
        }
    }

    /**
     * Fuses this node with the "otherNode".
     *
     * @return true when this node must disappear and
     * be merged into "otherNode". @return false when "otherNode"
     * must disappear and be merged into this node.
     */
    protected boolean fuseWith(NewBlockGraphNode otherNode) {
        if (diffInstruction == null) {
            // if diffInstruction is null, this means we are
            // in the 1st condensation phase, which fuses increment assignments:
            if (otherNode.kind == DiffConstants.SET_VARIABLE) {
                return true;
            } else if (otherNode.kind == DiffConstants.INCREMENT_VARIABLE) {
                diffValue = ILUtils.addProtectedExprs(diffValue, otherNode.diffValue);
                recomputeText();
                return false;
            } else if (otherNode.kind == DiffConstants.PLAIN) {
                // As soon as we merge two nodes into a single node with 2 trees,
                //  we must attach comments stored in the each node to their own tree.
                this.attachCommentsToTree();
                otherNode.attachCommentsToTree();
                plainTrees = TapList.append(plainTrees, otherNode.plainTrees);
                text = otherNode.text + ";" + text;
                return false;
            }
            return false;
        } else {
            // if diffInstruction is non-null, this means we are
            // in the 2nd condense phase, which deals with WHERE statements.
            Tree diffTree1 = diffInstruction.tree;
            Tree diffTree2 = otherNode.diffInstruction.tree;
            TapList<Tree> listTrees = null;
            if (diffTree2.opCode() != ILLang.op_blockStatement) {
                listTrees = new TapList<>(diffTree2, null);
            } else {
                for (int i = diffTree2.length(); i > 0; --i) {
                    listTrees = new TapList<>(diffTree2.cutChild(i), listTrees);
                }
            }
            if (diffTree1.opCode() != ILLang.op_blockStatement) {
                listTrees = new TapList<>(diffTree1, listTrees);
            } else {
                for (int i = diffTree1.length(); i > 0; --i) {
                    listTrees = new TapList<>(diffTree1.cutChild(i), listTrees);
                }
            }
            diffInstruction.tree = ILUtils.build(ILLang.op_blockStatement, listTrees);
            return false;
        }
    }

    protected boolean hasManyInstructions() {
        return plainTrees != null && plainTrees.tail != null;
    }

    protected void attachCommentsToTree() {
        if (plainTrees != null && plainTrees.tail == null) {
            Tree plainTree = plainTrees.head;
            if (preComments != null) {
                plainTree.setAnnotation("preComments", preComments);
                preComments = null;
            }
            if (preCommentsBlock != null) {
                plainTree.setAnnotation("preCommentsBlock", preCommentsBlock);
                preCommentsBlock = null;
            }
        }
    }

    protected void gatherMultiInstructions(SymbolTable diffSymbolTable, Unit diffUnit,
                                           NewSymbolHolder dirIndexSymbolHolder, IterDescriptor multiDirIterDescriptor) {
        Tree gatheredTree;
        if (plainTrees == null) {
            gatheredTree = null;
        } else if (plainTrees.tail != null || multiDir) {
            gatheredTree = ILUtils.build(ILLang.op_blockStatement, plainTrees);
        } else {
            gatheredTree = plainTrees.head;
        }
        if (multiDir && gatheredTree != null) {
            gatheredTree = ILUtils.build(ILLang.op_loop,
                             ILUtils.build(ILLang.op_none),
                             ILUtils.build(ILLang.op_none),
                             multiDirIterDescriptor.buildDoHeaderTree(diffSymbolTable, diffUnit, true),
                             gatheredTree) ;
        }
        Instruction newI = new Instruction(gatheredTree, null);
        if (multiDir && dirIndexSymbolHolder != null) {
            dirIndexSymbolHolder.declareUsageInstruction(newI);
        }
        tree = gatheredTree;
    }

    protected void buildInstruction(SymbolTable srcSymbolTable, SymbolTable bwdSymbolTable,
                                    DifferentiationEnv adEnv, Unit diffUnit) {
        if (kind == DiffConstants.SET_VARIABLE || kind == DiffConstants.INCREMENT_VARIABLE) {
            VarRefDifferentiator varRefDifferentiator = adEnv.varRefDifferentiator;
            WrapperTypeSpec assignedType = srcSymbolTable.typeOf(primRecv);
            if (diffValue == null && kind == DiffConstants.INCREMENT_VARIABLE) {
                tree = null;
            } else if (diffValue == null) {
                if (multiDir) {
                    // Not very clean: it turns out that makeDiffInitialization will use
                    // the multiDirMaxIterDescriptor, so we force it here to produce a "nd" and not a ":" :
                    varRefDifferentiator.multiDirMaxIterDescriptor.setJustAnIndex(true);
                }
                // TODO: Create a DiffInitialization only for the current iReplic !!
                adEnv.iReplic = iReplic ;
                tree = adEnv.blockDifferentiator.makeDiffInitializationOneReplica(primRecv, srcSymbolTable, bwdSymbolTable,
                        null, false, null, true);
                adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                if (multiDir) {
                    varRefDifferentiator.multiDirMaxIterDescriptor.setJustAnIndex(false);
                }
            } else {
                // If the zones of (the primal of) lhs intersect the BoolVector of
                // zones that are analyzed as thread-shared here, then set "needAtomic",
                // thus forcing the ajoint increment instruction to be "atomic":
                boolean needAtomic = false;
                if (kind != DiffConstants.SET_VARIABLE && TapEnv.multithreadAnalyzer()!=null) {
                    TapIntList primalZones = adEnv.curSymbolTable().listOfZonesOfValue(primRecv, null, srcInstruction) ;
                    String primalSymbol = ILUtils.baseName(primRecv) ;
                    Tree diffBaseTree = ILUtils.baseTree(assignedRef);
                    NewSymbolHolder lhsNewSymbolHolder =
                            (diffBaseTree == null ? null
                                    : NewSymbolHolder.getNewSymbolHolder(diffBaseTree));
                    BoolVector sharedZones =
                        TapEnv.multithreadAnalyzer().getSharedZonesOfBlock(adEnv.curBlock);
                    BoolVector simplySharedZones =
                        TapEnv.multithreadAnalyzer().getConflictFreeSharedAdjointZones(adEnv.curBlock);
// System.out.println("NEEDATOMIC FOR "+primRecv+" Z:"+primalZones+" SHAREDZ:"+sharedZones+" SIMPLYSHARED:"+simplySharedZones) ;
                    needAtomic = (sharedZones!=null
                                  &&
                                  DataFlowAnalyzer.intersectsZoneRks(sharedZones, primalZones, null)
                                  &&
                                  (simplySharedZones==null
                                   ||
                                   !DataFlowAnalyzer.containsExtendedDeclared(
                                          simplySharedZones, SymbolTableConstants.ALLKIND,
                                          primalZones, null, adEnv.curSymbolTable(), null))) ;
// System.out.println("   --->"+needAtomic) ;
                    if (needAtomic) {
                            // retrieve "onlyReadSmallActivePrimals" zones from adEnv.curBlock:
                            //  in OpenMP, we chose a REDUCTION(+) scoping for these zones:
                            BoolVector reductionAdjointZones =
                                    BlockDifferentiator.getOnlyReadSmallActivePrimals(adEnv.curBlock);
                            if (reductionAdjointZones==null) {
                                // Cuda case: retrieve the zones that are forced to have no atomic:
                                reductionAdjointZones = adEnv.curBlock.unit().privateSymbolTable().needNoAtomic ;
                            }
// System.out.println("    REDUCTIONZ:"+reductionAdjointZones) ;
                            if (reductionAdjointZones != null
                                && DataFlowAnalyzer.containsExtendedDeclared(
                                           reductionAdjointZones, SymbolTableConstants.ALLKIND,
                                           primalZones, null, adEnv.curSymbolTable(), null)) {
                                needAtomic = false;
// System.out.println("      ---> set to false!") ;
                                // Don't add a REDUCTION(+) scoping clause if scoping
                                //  clause has been forced by a user directive anyway:
                                TapList<String> forcedScopingNames =
                                        BlockDifferentiator.getForcedScopingNames(adEnv.curBlock);
                                if (!TapList.containsString(forcedScopingNames, primalSymbol,
                                                            !adEnv.curUnit().isFortran())) {
                                    lhsNewSymbolHolder.prepareSumReductionClause(adEnv.curBlock);
                                }
                            }
                    }
                }
                if (TapEnv.associationByAddress() && kind!=DiffConstants.SET_VARIABLE) {
                    // INCREMENT_VARIABLE and associationByAddress: the next fallback code doesn't apply.
                    // TODO: find a way to merge the two cases!
                    tree = ILUtils.incrementByProtectedExpr(adEnv.curDiffUnit.language(),
                            assignedRef, diffValue, needAtomic);
                } else {
                    // The following code seems better for handling records and arrays assignements:
                    adEnv.iReplic = iReplic ;
                    Tree diffRootIdent = varRefDifferentiator.diffVarRef(adEnv.curActivity(), primRecv,
                                                                         bwdSymbolTable, false, primRecv,
                                                                         multiDir, multiDir, ILUtils.copy(primRecv)) ;
                    RefDescriptor lhsRefDescriptor =
                            new RefDescriptor(diffRootIdent, null, srcSymbolTable, bwdSymbolTable,
                                              null, null, true, null, diffUnit);
                    RefDescriptor rhsRefDescriptor =
                            new RefDescriptor(diffValue, null, srcSymbolTable, bwdSymbolTable,
                                              null, null, true, null, diffUnit);
                    lhsRefDescriptor.setCompanionVarDescriptor(rhsRefDescriptor);
                    if (multiDir) { // to force using index "nd" and not a ":" :
                        varRefDifferentiator.multiDirIterDescriptor.setJustAnIndex(true);
                    }
                    // Only in Fortran, assignment must go into pointers:
                    lhsRefDescriptor.prepareForAssignOrNormDiff(rhsRefDescriptor, diffUnit, null, !diffUnit.isFortran());
                    if (multiDir) {
                        varRefDifferentiator.multiDirIterDescriptor.setJustAnIndex(false);
                    }
                    lhsRefDescriptor.mayProtectAccesses = false;
                    if (kind==DiffConstants.INCREMENT_VARIABLE) {
                        tree = lhsRefDescriptor.makeIncrement(rhsRefDescriptor, needAtomic);
                    } else { //kind==DiffConstants.SET_VARIABLE
                        tree = lhsRefDescriptor.makeAssign(rhsRefDescriptor);
                    }
                    adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                }
            }
        }
        if (whereMask != null && whereMask.isEmpty()) {
            whereMask = null;
        }
        diffInstruction = new Instruction(tree);
        diffInstruction.preComments = preComments;
        diffInstruction.preCommentsBlock = preCommentsBlock;
        diffInstruction.setPosition(srcInstruction);
        if (whereMask != null && !whereMask.isEmpty()) {
            diffInstruction.setWhereMask(whereMask);
        }
        if (fromInclude != null) {
            diffInstruction.setFromIncludeCopy(fromInclude, true);
        }
        if (srcInstruction!=null && srcInstruction.isPhantom) {
            diffInstruction.isPhantom = true ;
        }
        if (origTree != null && tree != null) {
            TapEnv.setSourceCodeCorrespondence(origTree, tree, true, true);
        }
    }

    protected void recomputeText() {
        switch (kind) {
            case DiffConstants.PLAIN:
                text = ILUtils.toString(tree);
                break;
            case DiffConstants.INCREMENT_VARIABLE:
                text = ILUtils.toString(assignedRef) + "+=" + ILUtils.toString(diffValue);
                break;
            case DiffConstants.SET_VARIABLE:
                text = ILUtils.toString(assignedRef) + ":=" + ILUtils.toString(diffValue);
                break;
            default:
                break;
        }
        if (text.length() > 53) {
            text = text.substring(0, 50) + "...";
        }
    }

    @Override
    public String toString() {
        String contents;
        if (diffInstruction != null) {
            contents = "Diff Instruction:" + diffInstruction;
        } else if (tree != null) {
            contents = "Diff Tree:" + ILUtils.toString(tree);
        } else if (kind == DiffConstants.SET_VARIABLE || kind == DiffConstants.INCREMENT_VARIABLE) {
            contents =
                    (kind == DiffConstants.SET_VARIABLE ? "Set" : "Incr")
                            + " diff of " + ILUtils.toString(primRecv)
                            + " to " + ILUtils.toString(diffValue);
        } else {
            contents = "Empty!";
        }
        return "[" + subOrder + "], " + contents;
    }
}
