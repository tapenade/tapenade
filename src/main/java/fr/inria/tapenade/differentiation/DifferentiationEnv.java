/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.BlockStorage;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.MemoryMaps;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.UnitStorage;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ModifiedTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.ILLang;

import java.io.File;

/**
 * Differentiation environment.
 * <pre>
 *  Constant objects kept by the DifferentiationEnv:
 *     callGraphDifferentiator, FlowGraphDifferentiator, BlockDifferentiator,
 *     procedureCallDifferentiator, dynMemoryDifferentiator,
 *     expressionDifferentiator (soon), VarRefDifferentiator,
 *     diffKind, multiDirMode, suffixes
 *     srcCallGraph, diffCallGraph
 *
 *  Modifiable "current" objects during navigation inside IR and construction of IR:
 *                  curUnit
 *                     curActivity
 *                     curDiffUnit
 *                     curFwdDiffUnit
 *                       curDiffUnitSort curDiffVarSort
 *                     curBlock
 *                       curVectorLen, curDiffVectorLen, curPtrVectorLen, curSymbolTable
 *                       curInstruction
 *                       curFwdSymbolTable, curBwdSymbolTable
 *                       </pre>
 */
public final class DifferentiationEnv {

    /**
     * The source/original non-differentiated CallGraph.
     */
    protected CallGraph srcCallGraph() {
        return srcCallGraph;
    }

    private void setSrcCallGraph(CallGraph srcCallGraph) {
        this.srcCallGraph = srcCallGraph;
    }

    private CallGraph srcCallGraph;

    /**
     * The differentiated CallGraph.
     */
    protected CallGraph diffCallGraph() {
        return diffCallGraph;
    }

    protected void setDiffCallGraph(CallGraph diffCallGraph) {
        this.diffCallGraph = diffCallGraph;
    }

    private CallGraph diffCallGraph;

    protected TapList<Unit> rootUnits;
    protected TapList<Unit> callersOfRootUnits;

    /**
     * Global boolean true when "curUnit" is an active subroutine.
     */
    protected boolean curUnitIsActiveUnit;
    /**
     * Global boolean true when "curUnit" is currently differentiated as context.
     */
    protected boolean curUnitIsContext;

    /**
     * Association original Unit to copied original Unit in diff.
     */
    protected UnitStorage<Unit> copiedUnits;
    /**
     * Association copied original Unit in diff to original Unit.
     */
    protected UnitStorage<Unit> copiedUnitsBack;
    /**
     * Association original include instruction to include instruction in diff.
     */
    protected TapList<TapPair<Instruction, Instruction>> copiedIncludes =
            new TapList<>(null, null);

    /**
     * Activity info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<BoolVector>> unitActivities;
    /**
     * Usefulness info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<BoolVector>> unitUsefulnesses;
    /**
     * ReqX info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<BoolVector>> unitReqXs;
    /**
     * AvlX info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<BoolVector>> unitAvlXs;
    /**
     * TBR info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> unitTBRs;
    /**
     * Recomputation (see TBR) info for the current Unit (curUnit).
     */
    protected BlockStorage<TapList<TapPair<TapList<Instruction>, TapList<Instruction>>>> unitRecomputations;

    /**
     * List of hidden variables to be PUSH/POP'ed for the current unit.
     */
    protected TapList<TapPair<Object, TapList<String>>> hiddenPushPopVariables = new TapList<>(null, null);
    /**
     * List of hidden variables to be instrumented (for debug) for the current unit.
     */
    protected TapList<TapPair<Object, TapList<String>>> hiddenInstrumentedVariables = new TapList<>(null, null);
    /**
     * List of hidden variables to be diff-initialized for the current unit.
     */
    protected TapList<TapPair<Object, TapList<String>>> hiddenDiffInitVariables = new TapList<>(null, null);

    /**
     * List of already created newSymbolHolders for variables named "indexN",
     * used in sub-expressions in actual arguments of calls.
     * This list is ordered with increasing N's.
     */
    protected TapList<NewSymbolHolder> toIndexSymbolHolders = new TapList<>(null, null);

    /**
     * True when differentiation notices that there must be a F90
     * INTERFACE declaration for PUSH/LOOK/POP POINTER().
     */
    protected boolean curDiffUnitPushesPointers;

    /**
     * True when differentiation notices that ADMM is used.
     */
    protected boolean usesADMM;

    /**
     * True when the curInstruction being differentiated is
     * in context code and is a call to an active.
     */
    protected boolean activeRootCalledFromContext;

    /**
     * The array of all suffixes used during differentiation.
     */
    protected String[][] suffixes;
    /**
     * True when copied Units keep their original name.
     */
    protected boolean emptyCopiedUnitSuffix;

    /**
     * The current differentiation mode for the current Unit.
     */
    protected int adDiffMode = DiffConstants.TANGENT_MODE;

    /**
     * True to get vector loops around derivative instructions.
     */
    protected boolean multiDirMode;

    /**
     * The "INTEGER" WrapperTypeSpec, necessary to create new INTEGER variables.
     */
    protected WrapperTypeSpec integerTypeSpec;
    /** INTEGER*4 type */
    protected WrapperTypeSpec integer4TypeSpec;
    /**
     * The "REAL" WrapperTypeSpec, necessary to create new REAL variables.
     */
    protected WrapperTypeSpec realTypeSpec;

    /**
     * The kind of zones that are currently differentiated.
     * Usually, it is SymbolTableConstants.REALKIND, but in a special mode, it can be ALLKIND!...
     */
    protected int diffKind;

    protected TapIntList toActiveTmpZones = new TapIntList(-1, null);
    protected TapIntList toTotalZones = new TapIntList(0, null);

    protected MemoryMaps commonActivityMemoryMap ;

    /**
     * The Unit of the external boolean function "DEBUG_***_HERE()".
     */
    protected Unit debugADHereUnit;

    /**
     * Units for which we want to trace differentiation on-the-fly.
     */
    protected TapList<Unit> traceDifferentiationUnits;

    public boolean someUnitsAreTraced() {
        return traceDifferentiationUnits != null;
    }

    /**
     * Detailled trace of differentiation on the standard output:
     * True when we want to trace differentiation on-the-fly on the current Unit (curUnit).
     */
    protected boolean traceCurDifferentiation;
    /**
     * 1 means trace on current Block. 2 means trace in even more detail (when traceCurDifferentiation is true).
     */
    protected int traceCurBlock;
    /**
     * The object that knows how to differentiate a callGraph.
     */
    protected CallGraphDifferentiator callGraphDifferentiator;

    /**
     * The object that knows how to differentiate FlowGraphs.
     */
    protected FlowGraphDifferentiator flowGraphDifferentiator;
    /**
     * The object that knows how to differentiate Basic Blocks.
     */
    protected BlockDifferentiator blockDifferentiator;
    /**
     * The object that knows how to differentiate procedure calls.
     */
    protected ProcedureCallDifferentiator procedureCallDifferentiator;
    /**
     * The object that knows how to differentiate Dynamic Memory manipulations.
     */
    protected DynMemoryDifferentiator dynMemoryDifferentiator;
    /**
     * The object that knows how to differentiate expressions.
     */
    protected ExpressionDifferentiator expressionDifferentiator;
    /**
     * The object that knows how to differentiate memory references.
     */
    protected VarRefDifferentiator varRefDifferentiator;

    /**
     * @return The current source Unit being analyzed or differentiated.
     */
    protected Unit curUnit() {
        return curUnit;
    }

    private Unit curUnit;

    private ActivityPattern curActivity;

    /**
     * @return The activity pattern for which we are differentiating the current Unit.
     */
    protected ActivityPattern curActivity() {
        return curActivity;
    }

    private Instruction curInstruction;

    /**
     * @return The current (source) Instruction.
     */
    protected Instruction curInstruction() {
        return curInstruction;
    }

    /**
     * The current "standard" differentiated Unit of the current Unit. Contains:<br>
     * -- in TANGENT_MODE: the tangent diff Unit.<br>
     * -- in ADJOINT_MODE: the joint diff Unit (fwd and bwd sweeps together)<br>
     * -- in ADJOINT_SPLIT_MODE: the bwd sweep alone.
     */
    protected Unit curDiffUnit;
    /**
     * The current "standard" differentiated Unit of the current Unit. Contains:<br>
     * -- in TANGENT_MODE: the tangent diff Unit.<br>
     * -- in ADJOINT_MODE: the joint diff Unit (fwd and bwd sweeps together)<br>
     * -- in ADJOINT_SPLIT_MODE: the bwd sweep alone.
     */
    protected Unit curFwdDiffUnit;
    /**
     * The current sort of derivatives for procedures.
     */
    protected int curDiffUnitSort = DiffConstants.UNDEFSORT;
    /**
     * The current sort of derivatives for variables.
     */
    protected int curDiffVarSort = DiffConstants.UNDEFSORT;

    /**
     * The current replica number for diff variables.
     */
    protected int iReplic = 0 ;

    /**
     * The current source Block.
     */
    protected Block curBlock;
    private SymbolTable curSymbolTable;

    /**
     * @return The current source SymbolTable (i.e. of the current source Block).
     */
    protected SymbolTable curSymbolTable() {
        return curSymbolTable;
    }

    /**
     * The current fwd differentiated SymbolTable (i.e. of the current fwd diff Block).
     */
    protected SymbolTable curFwdSymbolTable;
    /**
     * The current bwd differentiated SymbolTable (i.e. of the current bwd diff Block).
     */
    protected SymbolTable curBwdSymbolTable;
    /**
     * The current vectorLen (i.e. of the current Unit+SymbolTable).
     */
    protected int curVectorLen;
    /**
     * The current diffVectorLen (i.e. of the current Unit+SymbolTable).
     */
    protected int curDiffVectorLen;

    /**
     * The current ptrVectorLen (i.e. of the current Unit+SymbolTable).
     */
    protected int curPtrVectorLen;

    private TapIntList hiddenActiveTmpZones;

    /**
     * A stack to push/pop curUnit's.
     */
    private TapList<Unit> stackCurUnits;
    /**
     * A stack to push/pop diffUnit's.
     */
    private TapList<Unit> stackCurDiffUnits;

    /**
     * A stack to push/pop curDiffUnitSort's.
     */
    private TapIntList stackCurDiffUnitSorts;
    /**
     * A stack to push/pop curDiffVarSort's.
     */
    private TapIntList stackCurDiffVarSorts;
    /**
     * A stack to push/pop curSymbolTable's.
     */
    private TapList<SymbolTable> stackCurSymbolTables;
    /**
     * A stack to push/pop vectorLen's.
     */
    private TapIntList stackCurVectorLengthes;

    /**
     * A stack to push/pop diffVectorLen's.
     */
    private TapIntList stackCurDiffVectorLengthes;

    protected DifferentiationEnv(CallGraph srcCallGraph, String[][] suffixes) {
        super();
        this.setSrcCallGraph(srcCallGraph);
        this.suffixes = suffixes;
        this.diffKind = TapEnv.diffKind();
        this.multiDirMode = TapEnv.get().multiDirDiffMode;
        // If copiedUnitSuffix (i.e. adEnv.suffixes[ORIGCOPY][PROC]) is empty,
        // this means we want the "copied" units to keep their original name.
        // [llh 17/7/2012] this doesn't work well because Tapenade
        // systematically adds a "0" to disambiguate.
        emptyCopiedUnitSuffix =
                suffixes[DiffConstants.ORIGCOPY][DiffConstants.PROC] == null
                        || suffixes[DiffConstants.ORIGCOPY][DiffConstants.PROC].isEmpty();
        commonActivityMemoryMap = new MemoryMaps();
        integerTypeSpec = srcCallGraph.languageRootSymbolTable(TapEnv.inputLanguage()).getTypeDecl("integer").typeSpec;
        integer4TypeSpec = new WrapperTypeSpec(new ModifiedTypeSpec(integerTypeSpec, ILUtils.build(ILLang.op_intCst, 4), null)) ;
        realTypeSpec = srcCallGraph.languageRootSymbolTable(TapEnv.inputLanguage()).getTypeDecl("float").typeSpec;

        // Prepare the list of Units for which differentiation must be debugged/traced.
        traceDifferentiationUnits =
                srcCallGraph.getUnits(TapEnv.get().traceDifferentiationUnitNames);
        // Correspondence source Unit --> copied Unit in diff
        copiedUnits = new UnitStorage<>(srcCallGraph);
        copiedUnitsBack = new UnitStorage<>(srcCallGraph);

        // Prepare the **Differentiator's
        callGraphDifferentiator = new CallGraphDifferentiator(this);
        flowGraphDifferentiator = new FlowGraphDifferentiator(this);
        blockDifferentiator = new BlockDifferentiator(this);
        procedureCallDifferentiator = new ProcedureCallDifferentiator(this);
        dynMemoryDifferentiator = new DynMemoryDifferentiator(this);
        expressionDifferentiator = new ExpressionDifferentiator(this);
        varRefDifferentiator = new VarRefDifferentiator(this);
    }

    protected Unit getPrimalCopyOfOrigUnit(Unit unit) {
        if (unit.rank() == -1 || copiedUnits == null) {
            return null;
        }
        return copiedUnits.retrieve(unit);
    }

    protected Unit getOrigUnitOfPrimalCopy(Unit unit) {
        if (unit.rank() == -1 || copiedUnitsBack == null) {
            return null;
        }
        return copiedUnitsBack.retrieve(unit);
    }

    protected Unit getDiffOfUnit(Unit unit, ActivityPattern pattern, int diffMode) {
        UnitDiffInfo diffInfo = callGraphDifferentiator.getUnitDiffInfo(unit);
        if (diffInfo == null) {
            return null;
        } else {
            return diffInfo.getDiffForModeAndActivity(diffMode, pattern);
        }
    }

    protected Unit getDiffOfModule(Unit unit) {
        if (unit == null || unit.rank() == -1 || unit.isPredefinedModuleOrTranslationUnit()) {
            return null;
        }
        UnitDiffInfo diffInfo = callGraphDifferentiator.getUnitDiffInfo(unit);
        return diffInfo.getDiff();
    }

    /**
     * @param callerPattern The activity pattern of this particular call site.
     * @param calledUnit    The called unit
     * @return the ActivityPatterns for a "calledUnit" that are used at a particular call site,
     * i.e. only the patterns of "calledUnit" that may be called here in the caller's pattern.
     * When the call site is a module, the callerPattern is null and all called patterns are returned.
     */
    protected TapList<ActivityPattern> getPatternsForCallingUnitPattern(ActivityPattern callerPattern, Unit calledUnit) {
        TapList<ActivityPattern> calledActivityPatterns = calledUnit.activityPatterns;
        TapList<ActivityPattern> retList = null;
        if (calledActivityPatterns != null) {
            for (ActivityPattern calledPattern : calledActivityPatterns) {
                // each activityPattern of the calling unit could be used. we need to find out if it actually is.
                if (callerPattern == null // Case of modules: all patterns must be returned!
                        || TapList.contains(calledPattern.callingPatterns(), callerPattern)) {
                    retList = new TapList<>(calledPattern, retList);
                }
            }
        }
        return retList;
    }

    protected void setDiffCallGraphAndRootUnits(CallGraph diffCG, TapList<Unit> rootUnits) {
        this.rootUnits = rootUnits;
        callersOfRootUnits = Unit.allCallersMulti(rootUnits);
        this.setDiffCallGraph(diffCG);
    }

    /**
     * Sets and redistributes the global current source Unit ("curUnit"),
     * and sets traceCurDifferentiation boolean accordingly,
     * and resets global curInstruction to null.
     */
    protected void setCurUnitEtc(Unit unit) {
        curUnit = unit;
        curInstruction = null;
        traceCurDifferentiation =
                (traceDifferentiationUnits != null
                        && (traceDifferentiationUnits.head == null
                        || (unit != null
                        && TapList.contains(traceDifferentiationUnits, unit))));
    }

    protected void pushCurUnitEtc(Unit unit) {
        stackCurUnits = new TapList<>(curUnit, stackCurUnits);
        setCurUnitEtc(unit);
    }

    protected void popCurUnitEtc() {
        setCurUnitEtc(stackCurUnits.head);
        stackCurUnits = stackCurUnits.tail;
    }

    /**
     * Sets and redistributes the global current activity pattern for the current Unit.
     */
    protected void setCurActivity(ActivityPattern activity) {
        curActivity = activity;
    }

    protected void setCurInstruction(Instruction instr) {
        curInstruction = instr;
    }

    protected void setCurDiffUnit(Unit diffUnit) {
        curDiffUnit = diffUnit;
    }

    protected void pushCurDiffUnit(Unit diffUnit) {
        stackCurDiffUnits = new TapList<>(curDiffUnit, stackCurDiffUnits);
        setCurDiffUnit(diffUnit);
    }

    protected void popCurDiffUnit() {
        setCurDiffUnit(stackCurDiffUnits.head);
        stackCurDiffUnits = stackCurDiffUnits.tail;
    }

    protected void setCurFwdDiffUnit(Unit fwdDiffUnit) {
        curFwdDiffUnit = fwdDiffUnit;
    }

    protected void setCurDiffSorts(int diffUnitSort, int diffVarSort) {
        if (diffUnitSort != DiffConstants.UNDEFSORT) { //TODO: why "if" ?
            curDiffUnitSort = diffUnitSort;
        }
        if (diffVarSort != DiffConstants.UNDEFSORT) { //TODO: why "if" ?
            curDiffVarSort = diffVarSort;
        }
    }

    protected void pushCurDiffSorts(int diffUnitSort, int diffVarSort) {
        stackCurDiffUnitSorts = new TapIntList(curDiffUnitSort, stackCurDiffUnitSorts);
        stackCurDiffVarSorts = new TapIntList(curDiffVarSort, stackCurDiffVarSorts);
        setCurDiffSorts(diffUnitSort, diffVarSort);
    }

    protected void popCurDiffSorts() {
        setCurDiffSorts(stackCurDiffUnitSorts.head, stackCurDiffVarSorts.head);
        stackCurDiffUnitSorts = stackCurDiffUnitSorts.tail;
        stackCurDiffVarSorts = stackCurDiffVarSorts.tail;
    }

    protected void setCurBlock(Block block) {
        curBlock = block;
    }

    protected void setCurSymbolTable(SymbolTable symbolTable) {
        curSymbolTable = symbolTable;
    }

    protected void pushCurSymbolTable(SymbolTable symbolTable) {
        stackCurSymbolTables = new TapList<>(curSymbolTable, stackCurSymbolTables);
        setCurSymbolTable(symbolTable);
    }

    protected void popCurSymbolTable() {
        setCurSymbolTable(stackCurSymbolTables.head);
        stackCurSymbolTables = stackCurSymbolTables.tail;
    }

    protected void setCurFwdSymbolTable(SymbolTable symbolTable) {
        curFwdSymbolTable = symbolTable;
    }

    protected void setCurBwdSymbolTable(SymbolTable symbolTable) {
        curBwdSymbolTable = symbolTable;
    }

    protected void setCurVectorLen(int vectorLen) {
        curVectorLen = vectorLen;
    }

    protected void pushCurVectorLen(int vectorLen) {
        stackCurVectorLengthes = new TapIntList(curVectorLen, stackCurVectorLengthes);
        setCurVectorLen(vectorLen);
    }

    protected void popCurVectorLen() {
        curVectorLen = stackCurVectorLengthes.head ;
        stackCurVectorLengthes = stackCurVectorLengthes.tail;
    }

    protected void setCurDiffVectorLen(int diffVectorLen) {
        curDiffVectorLen = diffVectorLen;
    }

    protected void pushCurDiffVectorLen(int diffVectorLen) {
        stackCurDiffVectorLengthes = new TapIntList(curDiffVectorLen, stackCurDiffVectorLengthes);
        setCurDiffVectorLen(diffVectorLen);
    }

    protected void popCurDiffVectorLen() {
        curDiffVectorLen = stackCurDiffVectorLengthes.head ;
        stackCurDiffVectorLengthes = stackCurDiffVectorLengthes.tail;
    }

    protected void setCurPtrVectorLen(int ptrVectorLen) {
        curPtrVectorLen = ptrVectorLen;
    }

    protected void hideActiveTmpZones() {
        hiddenActiveTmpZones = toActiveTmpZones.tail;
        toActiveTmpZones.tail = null;
    }

    protected void resetActiveTmpZones() {
        toActiveTmpZones.tail = hiddenActiveTmpZones;
        hiddenActiveTmpZones = null;
    }

    /**
     * Allocate a new zone number for a new temporary variable.
     * increments the head of "toTotalZones", that contains the last zone used.
     */
    protected int allocateTmpZone() {
        toTotalZones.head++;
        return toTotalZones.head - 1;
    }

    protected void addInHiddenPushPopVariables(ZoneInfo zoneInfo) {
        addInHiddenVariables(zoneInfo, hiddenPushPopVariables);
    }

    protected void addInHiddenInstrumentedVariables(ZoneInfo zoneInfo) {
        addInHiddenVariables(zoneInfo, hiddenInstrumentedVariables);
    }

    protected void addInHiddenDiffInitVariables(ZoneInfo zoneInfo) {
        addInHiddenVariables(zoneInfo, hiddenDiffInitVariables);
    }

    private void addInHiddenVariables(ZoneInfo zoneInfo, TapList<TapPair<Object, TapList<String>>> toHiddenVariables) {
        String rootVarName = null;
        if (zoneInfo.variableNames() != null) {
            rootVarName = zoneInfo.variableNames().head;
        }
        if (rootVarName == null) {
            rootVarName = zoneInfo.description ;
        }
        if (rootVarName != null) {
            Object key = zoneInfo.declarationUnit;
            TapPair<Object, TapList<String>> place ;
            if (key == null) {
                key = zoneInfo.commonName;
                // "assqString" does not work here !
                place = null ;
                TapList<TapPair<Object, TapList<String>>> inHiddenVariables = toHiddenVariables.tail ;
                while (place==null && inHiddenVariables!=null) {
                    if (inHiddenVariables.head.first instanceof String && ((String)inHiddenVariables.head.first).equals(key))
                        place = inHiddenVariables.head ;
                    inHiddenVariables = inHiddenVariables.tail ;
                }
            } else {
                place = TapList.assq(key, toHiddenVariables.tail);
            }
            if (place == null) {
                place = new TapPair<>(key, null);
                toHiddenVariables.placdl(place);
            }
            if (!TapList.containsEquals(place.second, rootVarName)) {
                place.second = new TapList<>(rootVarName, place.second);
            }
        }
    }

    /**
     * Get the filename in which we are differentiating at a given time
     * @param fullPath: true if the path to file is kept or simply the filename
     * @return either the filename where the current unit is located (fullPath = False) or path + filename
     */
    public String getCurrentFilename(boolean fullPath) {
        Unit parentUnit = curUnit();
        while(!parentUnit.isTranslationUnit()) {
            parentUnit = parentUnit.upperLevelUnit;
        }
        return fullPath ? parentUnit.name() : (parentUnit.name().substring(parentUnit.name().lastIndexOf(File.separator)+1));
    }
}
