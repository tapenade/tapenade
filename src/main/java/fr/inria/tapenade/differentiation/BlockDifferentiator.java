/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.ArrayTypeSpec;
import fr.inria.tapenade.representation.AtomFuncDerivative;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.CompositeTypeSpec;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FieldDecl;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.FunctionTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.InstructionMask;
import fr.inria.tapenade.representation.InterfaceDecl;
import fr.inria.tapenade.representation.IterDescriptor;
import fr.inria.tapenade.representation.LoopBlock;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.ModifiedTypeSpec;
import fr.inria.tapenade.representation.MemoryMaps;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.representation.AlignmentBoundary;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.RefDescriptor;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TemporaryBlock;
import fr.inria.tapenade.representation.TypeDecl;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.representation.BlockStorage;
import fr.inria.tapenade.utils.*;

/**
 * Object that knows how to differentiate Basic Blocks.
 * <p>
 * IMPORTANT NOTICE: It is necessary that a good instruction split
 * has been done beforehand, before the zone numbering, i.e. during the
 * unit.preprocess() in CallGraph.terminateTypesAndZones(). Otherwise, the beforeActiv and
 * afterActiv information may be insufficient for variables that are written
 * many times by a same instruction. <br>
 * OPEN PROBLEM: split, as defined here in
 * BlockDifferentiator.splitForADReverse(), was meant to make use of the
 * activity analysis, some day, probably by not splitting call arguments that
 * are passive. This will not be possible if splitting is done long before
 * activity analysis, in CallGraph.terminateTypesAndZones().
 * If this original instruction split was well done, then one could get rid of
 * the split defined here in BlockDifferentiator.splitForADReverse().
 */
public class BlockDifferentiator {

    /**
     * Temporary only for debug of ADMM.
     */
    protected int numRebase = 0;

    /**
     * Global environment for differentiation.
     */
    protected DifferentiationEnv adEnv;
    protected TapList<NewSymbolHolder> inUseNewSymbolHolders;
    /**
     * The data-dep graph of the future Instructions of the FWD differentiated Block.
     */
    private NewBlockGraph fwdInstructionsGraph;
    /**
     * The data-dep graph of the future Instructions of the BWD differentiated Block.
     */
    private NewBlockGraph bwdInstructionsGraph;
    /**
     * The list of instructions that must go to the declaration Block of the split-backwards DifferentiationConstants.ADJOINT code.
     */
    private TapList<TapTriplet<Tree, Instruction, Instruction>> hdRevDeclarations;
    private TapList<TapTriplet<Tree, Instruction, Instruction>> tlRevDeclarations;
    private TapList<TapTriplet<Tree, Instruction, Instruction>> toTlRevDeclarations;
    /**
     * Global memo whether this will be the first fwd diff instruction for the curInstruction.
     * Useful e.g. for placement of debug AD statements, or for placement of comments.
     */
    private boolean firstFwdDiff = true;
    /**
     * Global memo of the comments that are currently waiting to be attached to the next fwd instruction.
     */
    private Tree waitingComments;
    private Tree waitingCommentsBlock;

    /**
     * When curInstruction comes from an include, the include'ing Instruction.
     */
    private Instruction fromInclude;
    private TapList<Tree> tempVarsForDebug;

    protected BlockDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
    }

    protected static Tree debugLabel(String name, int num) {
        return ILUtils.build(ILLang.op_stringCst, name + (num < 10 ? "0" : "") + num);
    }

    private static boolean usesPassiveFields(Tree expression, SymbolTable symbolTable) {
        if (expression.opCode() == ILLang.op_fieldAccess
                && TapEnv.doActivity()
                && !isAnActiveField(expression, symbolTable)) {
            return true;
        }
        if (expression.opCode() == ILLang.op_fieldAccess ||
                expression.opCode() == ILLang.op_arrayAccess ||
                expression.opCode() == ILLang.op_pointerAccess) {
            return usesPassiveFields(expression.down(1), symbolTable);
        } else {
            return false;
        }
    }

    private static boolean isAnActiveField(Tree expression, SymbolTable symbolTable) {
        int i = ILUtils.getFieldRank(expression.down(2));
        WrapperTypeSpec typeSpec = symbolTable.typeOf(expression.down(1));
        if (typeSpec == null) {
            return false;
        }
        typeSpec = typeSpec.baseTypeSpec(false);
        if (TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE) && i >= 0) {
            if (TapEnv.doActivity()) {
                return ((CompositeTypeSpec) typeSpec.wrappedType).isDifferentiatedField(i);
            } else {
                FieldDecl[] fields = ((CompositeTypeSpec) typeSpec.wrappedType).fields;
                return TypeSpec.isDifferentiableType(fields[i].type());
            }
        } else {
            return false;
        }
    }

    private static boolean moduleNameInModulesList(TapList<Unit> modules, String moduleName) {
        boolean result = false;
        while (!result && modules != null) {
            result = modules.head.name().equals(moduleName);
            modules = modules.tail;
        }
        return result;
    }

    /**
     * @return the list of all ActivityPatterns of the given "calledUnit" that
     * are used/called by the given "callingPattern" of the current Unit.
     * If callingPattern is null (case of e.g. modules), then return all "active" activity patterns.
     * If the calledUnit has a "not connected" ActivityPattern, it is returned too.
     * Does NOT return those activity patterns that are not really active (i.e. that would cause no differentiation),
     * i.e. a pattern with null in/out activity, ReqX, and AvlX.
     */
    protected static TapList<ActivityPattern> patternsCalledByPattern(Unit calledUnit,
                                                                      ActivityPattern callingPattern) {
        TapList<ActivityPattern> calledPatterns = calledUnit.activityPatterns;
        TapList<ActivityPattern> hdResult = new TapList<>(null, null);
        TapList<ActivityPattern> tlResult = hdResult;
        while (calledPatterns != null) {
            ActivityPattern calledPattern = calledPatterns.head;
            if ((callingPattern == null
                    || callingPattern.isDisconnected()
                    || calledPattern.isDisconnected()
                    || TapList.contains(calledPattern.callingPatterns(), callingPattern))
                    && (calledPattern.isActive() || calledPattern.isContext() || calledUnit.isInterface())) {
                tlResult = tlResult.placdl(calledPattern);
            }
            calledPatterns = calledPatterns.tail;
        }
        return hdResult.tail;
    }

    private static void accumulateRW(TapPair<TapList<Tree>, TapList<Tree>> rw,
                                     TapPair<TapList<Tree>, TapList<Tree>> rwNew) {
        rw.first = TapList.append(rwNew.first, rw.first);
        rw.second = TapList.append(rwNew.second, rw.second);
    }

    protected static Tree replaceAllocateInExprToInitialize(Tree exprToInitialize) {
        switch (exprToInitialize.opCode()) {
            case ILLang.op_allocate:
                exprToInitialize = replaceAllocateInExprToInitialize(exprToInitialize.down(1));
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_fieldAccess: {
                Tree newSon1 = replaceAllocateInExprToInitialize(exprToInitialize.down(1));
                if (newSon1 != exprToInitialize.down(1)) {
                    exprToInitialize.setChild(ILUtils.copy(newSon1), 1);
                }
                break;
            }
            default:
                break;
        }
        return exprToInitialize;
    }

    private static boolean containsOpArrayDeclarator(VariableDecl varDecl, int operator) {
        boolean result = false;
        if (varDecl != null
                && varDecl.hasInstructionWithRootOperatorAndArrayDeclarator(operator)) {
            result = true;
        }
        return result;
    }

    private void removeDecl(TapList<Tree> trees) {
        Tree decl;
        while (trees != null) {
            decl = trees.head;
            decl.parent().removeChild(decl.rankInParent());
            trees = trees.tail;
        }
    }

    /**
     * @return the global adEnv.callGraphDifferentiator.
     */
    private CallGraphDifferentiator callGraphDifferentiator() {
        return adEnv.callGraphDifferentiator;
    }

    /**
     * @return the global adEnv.blockDifferentiator.
     */
    private FlowGraphDifferentiator flowGraphDifferentiator() {
        return adEnv.flowGraphDifferentiator;
    }

    /**
     * @return the global adEnv.procedureCallDifferentiator.
     */
    private ProcedureCallDifferentiator procedureCallDifferentiator() {
        return adEnv.procedureCallDifferentiator;
    }

    /**
     * @return the global adEnv.dynMemoryDifferentiator.
     */
    private DynMemoryDifferentiator dynMemoryDifferentiator() {
        return adEnv.dynMemoryDifferentiator;
    }

    /**
     * @return the global adEnv.expressionDifferentiator.
     */
    private ExpressionDifferentiator expressionDifferentiator() {
        return adEnv.expressionDifferentiator;
    }

    /**
     * @return the global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /**
     * @return the global TapEnv.get().activityAnalyzer.
     */
    private ADActivityAnalyzer activityAnalyzer() {
        return TapEnv.adActivityAnalyzer();
    }

    /**
     * @return the global adEnv.multiDirMode.
     */
    private boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * @return the global adEnv.suffixes.
     */
    private String[][] suffixes() {
        return adEnv.suffixes;
    }

    /**
     * @return the global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * @return the global adEnv.curFwdDiffUnit.
     */
    private Unit curFwdDiffUnit() {
        return adEnv.curFwdDiffUnit;
    }

    /**
     * @return the global adEnv.curDiffUnitSort.
     */
    private int curDiffUnitSort() {
        return adEnv.curDiffUnitSort;
    }

    /**
     * @return the global adEnv.curDiffVarSort.
     */
    private int curDiffVarSort() {
        return adEnv.curDiffVarSort;
    }

    /**
     * @return the global adEnv.curBlock.
     */
    private Block curBlock() {
        return adEnv.curBlock;
    }

    protected void addInUseNewSymbolHolder(NewSymbolHolder tmpVarHolder) {
        inUseNewSymbolHolders = new TapList<>(tmpVarHolder, inUseNewSymbolHolders);
    }

    /**
     * Append a new instruction ("tree") at the tail of
     * the list of future instructions of the FWD DIFF block of curBlock.
     * The new instruction will contain "tree", optionally controlled by mask "whereMask",
     * and repeated over multi-directions if "loopIfMultiDir" is true.
     * Future possible reordering is constrained by the list of (derivative) expressions possibly/partly
     * read by this new instruction "treesRead" ("treesReadBis"), and similarly possibly/partly
     * written expressions "treesWritten" ("treesWrittenBis"), plus possible hidden
     * dependencies (e.g. files read/write, stack push/pop) through "hiddenDep".
     *
     * @param followPointers      when true, consider that read/written trees also concern destination zones through pointers.
     * @param hiddenDep           when true, consider this tree also reads and/or writes some internal hidden variable, e.g. stack.
     * @param description         a description of this new instruction, for debug -traceDifferentiation.
     * @param isMainCorrespondent when true, HTML display shows this new instruction as the target of curInstruction.
     */
    protected void addFuturePlainNodeFwd(Tree tree, InstructionMask whereMask, boolean loopIfMultiDir,
                                         TapList<Tree> treesRead, TapList<Tree> treesWritten,
                                         TapList<Tree> treesReadBis, TapList<Tree> treesWrittenBis,
                                         boolean followPointers, boolean hiddenDep,
                                         String description, boolean isMainCorrespondent) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("      -> (FWD+) " + description + " : " + ILUtils.toString(tree, adEnv.curActivity()) + (loopIfMultiDir && multiDirMode() ? " (multi-dir)" : ""));
        }
        Tree refTree = adEnv.curInstruction().tree;
        TapEnv.setSourceCodeCorrespondence(refTree, tree, isMainCorrespondent, true);
        NewBlockGraphNode newNode = new NewBlockGraphNode(DiffConstants.PLAIN, tree, loopIfMultiDir && multiDirMode(), adEnv.curInstruction(), whereMask);
        newNode.overwritesUsed = true; // This is approximate, but doesn't matter so far.
        newNode.recomputeText();
        if (fromInclude != null && fromInclude.tree != null) {
            newNode.fromInclude = fromInclude;
            // This piece of code is new an lightly tested: used to generate diff include files that contain not only declarations!
            if (!TapEnv.get().expandDiffIncludeFile) {
                Instruction diffInclude = fromInclude ;
                boolean isPrimalCode = (treesReadBis==null && treesWrittenBis==null) ;
                if (!TapEnv.alreadyDiffInclude(diffInclude.tree.stringValue())) {
                    diffInclude = Instruction.differentiateChainedIncludes(
                                       diffInclude, suffixes()[curDiffUnitSort()][DiffConstants.PROC], isPrimalCode) ;
                }
                newNode.fromInclude = diffInclude ;
            }
        }
        if (firstFwdDiff) {
            firstFwdDiff = false;
            if (waitingComments != null) {
                newNode.preComments =
                        ILUtils.appendComments(newNode.preComments, waitingComments);
                waitingComments = null;
            }
            if (waitingCommentsBlock != null) {
                newNode.preCommentsBlock =
                        ILUtils.appendComments(newNode.preCommentsBlock, waitingCommentsBlock);
                waitingCommentsBlock = null;
            }
        }
        fwdInstructionsGraph.addDataDepNode(newNode, true, hiddenDep, followPointers,
                TapEnv.get().mergeDiffInstructions, treesRead, treesWritten, treesReadBis, treesWrittenBis);
    }

    /**
     * PREpend a new instruction ("tree") at the head of
     * the list of future instructions of the BWD DIFF block of curBlock.
     * The new instruction will contain "tree", optionally controlled by mask "whereMask",
     * and repeated over multi-directions if "loopIfMultiDir" is true.
     * Future possible reordering is constrained by  the list of (derivative) expressions possibly/partly
     * read by this new instruction "treesRead" ("treesReadBis"), and similarly possibly/partly
     * written expressions "treesWritten" ("treesWrittenBis"), plus possible hidden
     * dependencies (e.g. files read/write, stack push/pop) through "hiddenDep".
     *
     * @param followPointers      when true, consider that read/written trees also concern destination zones through pointers.
     * @param hiddenDep           when true, consider this tree also reads and/or writes some internal hidden variable, e.g. stack.
     * @param description         a description of this new instruction, for debug -traceDifferentiation.
     * @param isMainCorrespondent when true, HTML display shows this new instruction as the target of curInstruction.
     */
    protected void addFuturePlainNodeBwd(Tree tree, InstructionMask whereMask, boolean loopIfMultiDir,
                                         TapList<Tree> treesRead, TapList<Tree> treesWritten,
                                         TapList<Tree> treesReadBis, TapList<Tree> treesWrittenBis,
                                         boolean followPointers, boolean hiddenDep,
                                         String description, boolean isMainCorrespondent) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("      -> (+BWD) " + description + " : " + ILUtils.toString(tree, adEnv.curActivity()) + (loopIfMultiDir && multiDirMode() ? " (multi-dir)" : ""));
        }
        // CurInstruction may be null for the "Recompute forward" case:
        Tree refTree = adEnv.curInstruction() == null ? null : adEnv.curInstruction().tree;
        if (tree != null && refTree != null) {
            // May be null when using refDescriptors. Should place correspondence later!
            TapEnv.setSourceCodeCorrespondence(refTree, tree, isMainCorrespondent, true);
        }
        NewBlockGraphNode newNode = new NewBlockGraphNode(DiffConstants.PLAIN, tree, loopIfMultiDir && multiDirMode(), adEnv.curInstruction(), whereMask);
        newNode.overwritesUsed = true; // This is approximate, but doesn't matter so far.
        newNode.recomputeText();
        bwdInstructionsGraph.addDataDepNode(newNode, false, hiddenDep, followPointers, TapEnv.get().mergeDiffInstructions,
                treesRead, treesWritten, treesReadBis, treesWrittenBis);
    }

    /**
     * PREpend a new "diffvar=expression" instruction at the head of
     * the list of future instructions of the BWD DIFF block of curBlock.
     * The new instruction to add is described by "adjAssign" (see DiffAssignmentNode).
     *
     * @param adjAssign   the description of the new derivative instruction to be PREpend'ed.
     * @param description a textual description of the diff assigned variable, for debug -traceDifferentiation.
     */
    protected void addFutureSetDiffNodeBwd(DiffAssignmentNode adjAssign, String description) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("      -> (+BWD) " + description + ": " + adjAssign);
        }
        NewBlockGraphNode newNode = new NewBlockGraphNode(DiffConstants.SET_VARIABLE, null, multiDirMode(), adEnv.curInstruction(), adjAssign.mask);
        newNode.assignedRef = adjAssign.diffRecv;
        newNode.primRecv = adjAssign.primRecv;
        newNode.iReplic = adjAssign.iReplic;
        newNode.overwritesUsed = TapList.intersectTreeEquals(adjAssign.diffW, adjAssign.diffR);
        newNode.diffValue = adjAssign.diffValue;
        newNode.recomputeText();
        bwdInstructionsGraph.addDataDepNode(newNode, false, false, false, TapEnv.get().mergeDiffInstructions,
                adjAssign.primR, adjAssign.primW, adjAssign.diffR, adjAssign.diffW);
    }

    /**
     * PREpend a new "diffvar+=expression" instruction at the head of
     * the list of future instructions of the BWD DIFF block of curBlock.
     * The new instruction to add is described by "adjAssign" (see DiffAssignmentNode).
     *
     * @param adjAssign   the description of the new derivative instruction to be PREpend'ed.
     * @param description a textual description of the diff assigned variable, for debug -traceDifferentiation.
     */
    private void addFutureIncrDiffNodeBwd(DiffAssignmentNode adjAssign, String description) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("      -> (+BWD) " + description + ": " + adjAssign);
        }
        NewBlockGraphNode newNode = new NewBlockGraphNode(DiffConstants.INCREMENT_VARIABLE, null, multiDirMode(), adEnv.curInstruction(), adjAssign.mask);
        newNode.assignedRef = adjAssign.diffRecv;
        newNode.primRecv = adjAssign.primRecv;
        newNode.iReplic = adjAssign.iReplic;
        newNode.overwritesUsed = true;
        newNode.diffValue = adjAssign.diffValue;
        newNode.recomputeText();
        bwdInstructionsGraph.addDataDepNode(newNode, false, false, false, TapEnv.get().mergeDiffInstructions,
                adjAssign.primR, adjAssign.primW, adjAssign.diffR, adjAssign.diffW);
    }

    /**
     * Appends declaration "decl" to the list of declarations that must go to the declaration Block of
     * the separate backward sweep (case of mode DifferentiationConstants.ADJOINT_SPLIT_MODE or
     * of mode DifferentiationConstants.ADJOINT_MODE for a nested inside scope).
     */
    private void addFutureBwdSplitDecl(Tree decl, String description) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("      -> (BWD DECL+) " + description + " : " + decl);
        }
        // Note: using a TapTriplet here is dirty!
        toTlRevDeclarations.tail = toTlRevDeclarations.tail.placdl(new TapTriplet<>(decl, adEnv.curInstruction(), fromInclude));
    }

    /**
     * Fill "fwdBlock" and "bwdBlock" with the differentiation of "block" in the given "differentiationMode".
     *
     * @param differentiationMode in {TANGENT_MODE, DifferentiationConstants.ADJOINT_MODE, DifferentiationConstants.ADJOINT_SPLIT_MODE}
     * @param lastTestLive        upon return, will be filled with true iff
     *                            the last instruction of "block" is a test and it is live.
     */
    protected void differentiateBlock(Block block, Block fwdBlock, Block bwdBlock,
                                      int differentiationMode, ToBool lastTestLive) {
        adEnv.traceCurBlock = 0;
        assert fwdBlock != null;
        if (adEnv.traceCurDifferentiation) {
            adEnv.traceCurBlock = (TapEnv.get().traceBlockRk == -99 ? 1 : (block.rank == TapEnv.get().traceBlockRk ? 2 : 0));
            // traceCurBlock meanings: 0->no trace ; 1->light trace ; 2->full trace
            if (adEnv.traceCurBlock != 0) {
                TapEnv.printlnOnTrace("*** NOW Differentiating Block:" + block + " in mode "
                        + differentiationMode + " with pattern " + adEnv.curActivity() + ": " + block.instructions);
                TapEnv.printlnOnTrace("  PARALLELCONTROLS: block:" + block.parallelControls
                        + " fwdBlock:" + (fwdBlock == null ? "NULLBLOCK" : fwdBlock.parallelControls)
                        + " bwdBlock:" + (bwdBlock == null ? "NULLBLOCK" : bwdBlock.parallelControls));
            }
        }
        // isDebug true means we are in -debugTGT or -debugADJ mode, by opposition to plain -context mode.
        // isDebug false means we are just instrumenting the call to the root diff procedure
        //  from the outside context with nontrivial derivatives.
        boolean isDebug = TapEnv.debugAdMode() != TapEnv.NO_DEBUG;
        // isAdj true means, when isDebug: we are debugging the DifferentiationConstants.ADJOINT,
        //  and otherwise when not isDebug: we are instrumenting the call to root_B().
        // isAdj false means that we are debugging the tangent or instrumenting the call to root_D().
        boolean isAdj = (isDebug ? TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG : curDiffUnitSort() == DiffConstants.ADJOINT);
        String traceFuncPrefix =
            (isDebug
             ? "adDebug" + (isAdj
                            ? (differentiationMode == DiffConstants.TANGENT_MODE ? "Fwd_" : "Bwd_")
                            : "Tgt_")
             : "adContext" + (isAdj
                              ? "Adj_"
                              : (TapEnv.get().complexStep ? "Cpx_" : "Tgt_"))) ;
        // diffSymbolTable is the SymbolTable that will receive the most part of the differentiated code created:
        SymbolTable diffSymbolTable =
                ((bwdBlock == null || adEnv.curUnitIsContext || differentiationMode == DiffConstants.TANGENT_MODE)
                        ? fwdBlock.symbolTable : bwdBlock.symbolTable);
        TapList<BoolVector> blockActivities;
        TapList<BoolVector> blockUsefulnesses;
        TapList<BoolVector> blockReqXs;
        TapList<BoolVector> blockAvlXs;
        TapList<TapPair<BoolVector, BoolVector>> blockTBRs;
        TapList<TapPair<TapList<Instruction>, TapList<Instruction>>> blockRecomputations;
        if (block.rank == -99) {
            //This is a TemporaryBlock. Its data flow info is attached directly!
            blockActivities = ((TemporaryBlock) block).blockActivities;
            blockUsefulnesses = ((TemporaryBlock) block).blockUsefulnesses;
            blockReqXs = ((TemporaryBlock) block).blockReqXs;
            blockAvlXs = ((TemporaryBlock) block).blockAvlXs;
            blockTBRs = ((TemporaryBlock) block).blockTBRs;
            blockRecomputations = ((TemporaryBlock) block).blockRecomputations;
        } else {
            blockActivities = adEnv.unitActivities == null ? null : adEnv.unitActivities.retrieve(block);
            blockUsefulnesses = adEnv.unitUsefulnesses == null ? null : adEnv.unitUsefulnesses.retrieve(block);
            blockReqXs = adEnv.unitReqXs == null ? null : adEnv.unitReqXs.retrieve(block);
            blockAvlXs = adEnv.unitAvlXs == null ? null : adEnv.unitAvlXs.retrieve(block);
            blockTBRs = adEnv.unitTBRs == null ? null : adEnv.unitTBRs.retrieve(block);
            blockRecomputations = adEnv.unitRecomputations == null ? null : adEnv.unitRecomputations.retrieve(block);
        }

        adEnv.setCurBlock(block);
        adEnv.setCurSymbolTable(block.symbolTable);
        adEnv.setCurFwdSymbolTable(fwdBlock == null ? null : fwdBlock.symbolTable);
        adEnv.setCurBwdSymbolTable(bwdBlock == null ? null : bwdBlock.symbolTable);

        TapList<Instruction> instructions = block.instructions;
        adEnv.setCurVectorLen(adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND));
        adEnv.setCurDiffVectorLen(adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind));
        adEnv.setCurPtrVectorLen(adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.PTRKIND));
        BoolVector beforeActiv, afterActiv, beforeUseful, afterUseful,
                beforeReqX, afterReqX, beforeAvlX, afterAvlX, beforeTBR, beforeTBROnDiffPtr;
        beforeActiv = blockActivities == null ? null : blockActivities.head;
        if (!TapEnv.doActivity() && beforeActiv == null) { // no-activity case
            beforeActiv = new BoolVector(adEnv.curDiffVectorLen) ;
            beforeActiv.setTrue();
        }
        beforeUseful = blockUsefulnesses == null ? null : blockUsefulnesses.head;
        if (!TapEnv.doActivity() && beforeUseful == null) { // no-activity case
            beforeUseful = new BoolVector(adEnv.curDiffVectorLen) ;
            beforeUseful.setTrue();
        }
        beforeReqX = (blockReqXs == null ? null : blockReqXs.head);
        beforeAvlX = (blockAvlXs == null ? null : blockAvlXs.head);

        Tree srcFullTree, srcTree, srcDecl, srcAction, srcResult = null, srcCallTree = null;
        TapList resultPointerActivity = null;
        TapList<Tree> srcSplitTrees;
        // The future "contained" Unit's of the differentiated Unit being built:
        TapList<Unit> containedUnitsFwd = new TapList<>(null, null);
        TapList<Unit> containedUnitsBwd = new TapList<>(null, null);

        adEnv.toTotalZones.head = adEnv.curSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND);
        adEnv.toActiveTmpZones.tail = null;
        fwdInstructionsGraph =
                new NewBlockGraph(adEnv, adEnv.curSymbolTable(), adEnv.curSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND));
        // When we are more confident on the data-deps in fwdInstructionsGraph, we can remove this openGroup()/closeGroup()
        if (!multiDirMode() || !TapEnv.get().mergeDiffInstructions || differentiationMode != DiffConstants.TANGENT_MODE) {
            fwdInstructionsGraph.openGroup();
        }
        bwdInstructionsGraph =
                new NewBlockGraph(adEnv, adEnv.curSymbolTable(), adEnv.curSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND) + 5);
        if (adEnv.traceCurBlock == 2) {
            bwdInstructionsGraph.debug = true;
        }
        if (adEnv.traceCurBlock == 2 && multiDirMode()) {
            fwdInstructionsGraph.debug = true;
        }
        if (!TapEnv.get().mergeDiffInstructions) {
            bwdInstructionsGraph.openGroup();
        }
        hdRevDeclarations = new TapList<>(null, null);
        tlRevDeclarations = hdRevDeclarations;
        toTlRevDeclarations = new TapList<>(null, tlRevDeclarations);

        waitingComments = null;
        waitingCommentsBlock = null;
        boolean normalCheckpointedCall, splitAdjointCall, diffCallNeeded;
        boolean hasDeclaration, isMPICall, isIncrement,
            mustSaveTBROther, mustSaveTBR, mustSaveTBROnDiffPtr,
            remainsInFwdSweep, remainsInFwdSweepOnDiffPtr;
        String reasonForMustSaveTBROther, reasonForRemainsInFwdSweep ;
        Unit calledUnit, diffCalledUnit;
        ActivityPattern calledActivity;
        boolean modeIsJoint = (differentiationMode == DiffConstants.ADJOINT_MODE);
        boolean curUnitIsCopied = !adEnv.curActivity().isActive() &&
                callGraphDifferentiator().unitsHaveDiff.retrieve(adEnv.curUnit()) == Boolean.TRUE;
        TapList<Tree> beforeActivTrees = null, afterActivTrees = null;
        TapList<Tree> args, instructionsOnZones;
        Tree arg, dbadTree;

        // Add possible recomputations to be placed upstream the 1st instruction of this Bwd block.
        if (blockRecomputations != null && blockRecomputations.head!=null) {
            buildBwdRecomputations(blockRecomputations.head, beforeActiv,
                (blockActivities!=null && blockActivities.tail!=null ? blockActivities.tail.head : null),
                beforeAvlX,
                (blockReqXs!=null && blockReqXs.tail!=null ? blockReqXs.tail.head : null));
        }

        while (instructions != null) {
            if (blockActivities != null) {
                blockActivities = blockActivities.tail;
                afterActiv = blockActivities.head;
            } else {
                afterActiv = null;
            }
            if (!TapEnv.doActivity() && afterActiv == null) {
                afterActiv = beforeActiv; // no-activity case
            }
            if (blockUsefulnesses != null) {
                blockUsefulnesses = blockUsefulnesses.tail;
                afterUseful = blockUsefulnesses.head;
            } else {
                afterUseful = null;
            }
            if (!TapEnv.doActivity() && afterUseful == null) {
                afterUseful = beforeUseful; // no-activity case
            }
            if (blockReqXs != null) {
                blockReqXs = blockReqXs.tail;
                afterReqX = blockReqXs.head;
            } else {
                afterReqX = null;
            }
            if (blockAvlXs != null) {
                blockAvlXs = blockAvlXs.tail;
                afterAvlX = blockAvlXs.head;
            } else {
                afterAvlX = null;
            }
            if (blockTBRs != null) {
                beforeTBR = blockTBRs.head.first;
                beforeTBROnDiffPtr = blockTBRs.head.second;
                blockTBRs = blockTBRs.tail;
            } else {
                beforeTBR = null;
                beforeTBROnDiffPtr = null;
            }
            if (blockRecomputations != null) {
                blockRecomputations = blockRecomputations.tail;
            }
            tempVarsForDebug = null;
            adEnv.setCurInstruction(instructions.head);
            // Reset that the next fwd diff instruction will be
            // the first fwd diff instruction for this curInstruction:
            firstFwdDiff = true;
            srcFullTree = adEnv.curInstruction().tree;
            Unit definedUnit = srcFullTree.getAnnotation("Unit");
            if (definedUnit != null) { // Unit definition:
                if (adEnv.traceCurBlock != 0) {
                    TapEnv.printlnOnTrace("    --- differentiating definition location of " + definedUnit);
                }
                callGraphDifferentiator().collectDiffSubUnits(definedUnit, differentiationMode,
                        containedUnitsFwd, containedUnitsBwd);
                if (adEnv.traceCurBlock != 0) {
                    TapEnv.printlnOnTrace("       Cumul contained Units for fwd:" + containedUnitsFwd);
                    TapEnv.printlnOnTrace("       Cumul contained Units for bwd:" + containedUnitsBwd);
                }
            } else {

                // If curInstruction is labelled for debug, then place debug instructions here and now:
                Directive debugADHereDir = adEnv.curInstruction().hasDirective(Directive.DEBUGHERE);
                if (TapEnv.debugAdMode() != TapEnv.NO_DEBUG && debugADHereDir != null && !adEnv.curUnitIsContext) {
                    BoolVector activeHere = beforeActiv;
                    if (!TapEnv.doActivity() && activeHere == null) { // no-activity case
                        activeHere = new BoolVector(adEnv.curDiffVectorLen);
                        activeHere.setTrue();
                    }
                    TapList<Tree> activHereTrees = collectTrueTrees(activeHere, adEnv.curSymbolTable());
                    String placeName = "PP" + flowGraphDifferentiator().debugPointsCounter;
                    ++flowGraphDifferentiator().debugPointsCounter;
                    if (debugADHereDir.arguments.length > 0) {
                        placeName = ILUtils.getIdentString(debugADHereDir.arguments[0]);
                    }
                    Tree okHere = null;
                    if (debugADHereDir.arguments.length > 1) {
                        okHere = ILUtils.cleanBoolCopy(debugADHereDir.arguments[1], curDiffUnit());
                    }
                    if (okHere == null) {
                        okHere = ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE");
                    }
                    Tree forcedOkHere = null;
                    if (debugADHereDir.arguments.length > 2) {
                        forcedOkHere = ILUtils.cleanBoolCopy(debugADHereDir.arguments[2], curDiffUnit());
                    }
                    if (forcedOkHere == null) {
                        forcedOkHere = ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE");
                    }
                    dbadTree = flowGraphDifferentiator().placeDebugADTests(curDiffUnit(), srcFullTree, activeHere, 0,
                            adEnv.curSymbolTable(), adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind),
                            placeName, null, okHere, forcedOkHere);
                    if (differentiationMode == DiffConstants.TANGENT_MODE) {
                        addFuturePlainNodeFwd(dbadTree, null, false,
                                null, activHereTrees, null, activHereTrees, true, true,
                                "Debug-instrument actives before debug mark " + placeName + " in tangent", false);
                    } else {
                        addFuturePlainNodeBwd(dbadTree, null, false,
                                null, activHereTrees, null, activHereTrees, true, true,
                                "Debug-instrument actives after debug mark " + placeName + " in DifferentiationConstants.ADJOINT", false);
                    }
                }

                if (adEnv.traceCurBlock != 0) {
                    String blockDiffMapName = "[d" + block.symbolTable.rank() + ":r]";
                    TapEnv.printlnOnTrace("  === Differentiating source Instruction: " + ILUtils.toString(srcFullTree, adEnv.curActivity())
                            + "  " + blockDiffMapName + " " + (beforeActiv == null ? "null" : beforeActiv.toString(adEnv.curDiffVectorLen))
                            + "=>" + (afterActiv == null ? "null" : afterActiv.toString(adEnv.curDiffVectorLen)));
                }
                waitingComments = ILUtils.appendComments(waitingComments, adEnv.curInstruction().preComments);
                waitingCommentsBlock = ILUtils.appendComments(waitingCommentsBlock, adEnv.curInstruction().preCommentsBlock);
                adEnv.toActiveTmpZones.tail = null;
                if (differentiationMode == DiffConstants.TANGENT_MODE) {
                    srcSplitTrees = new TapList<>(srcFullTree, null);
                } else {
                    srcSplitTrees = new TapList<>(null, null);
                    splitForADReverse(srcFullTree, false, false, false, srcSplitTrees, modeIsJoint);
                    srcSplitTrees = TapList.nreverse(srcSplitTrees.tail);
                }

                // Detect the case of some active subroutine called from the context:
                adEnv.activeRootCalledFromContext = false;
                if (adEnv.curUnitIsContext) {
                    TapList<Tree> inSrcSplitTrees = srcSplitTrees;
                    while (!adEnv.activeRootCalledFromContext && inSrcSplitTrees != null) {
                        srcTree = inSrcSplitTrees.head;
                        srcResult = null;
                        srcCallTree = null;
                        if (srcTree != null && srcTree.opCode() == ILLang.op_varDeclaration && !adEnv.curUnit().isFortran9x()) {
                            TapPair<Tree, Tree> declAndInit = ILUtils.splitDeclInit(srcTree, false, adEnv.curUnit());
                            srcTree = declAndInit.second;
                        }
                        if (srcTree != null && (srcTree.opCode() == ILLang.op_assign
                                || srcTree.opCode() == ILLang.op_plusAssign
                                || srcTree.opCode() == ILLang.op_minusAssign
                                || srcTree.opCode() == ILLang.op_timesAssign
                                || srcTree.opCode() == ILLang.op_divAssign)) {
                            srcResult = srcTree.down(1);
                            srcTree = srcTree.down(2);
                        }
                        if (srcTree != null && srcTree.opCode() == ILLang.op_call) {
                            srcCallTree = srcTree;
                        }
                        if (srcCallTree != null) {
                            calledUnit = DataFlowAnalyzer.getCalledUnit(srcCallTree, adEnv.curSymbolTable());
                            if (calledUnit.isStandard()) {
                                calledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                        srcCallTree, adEnv.curActivity(), "multiActivityCalleePatterns");
                                adEnv.activeRootCalledFromContext =
                                        calledActivity != null && calledActivity.isActive()
                                                && !calledActivity.isContext() && calledActivity.diffPattern() != null;
                            }
                        }
                        inSrcSplitTrees = inSrcSplitTrees.tail;
                    }
                }

                // If we are in context code, and NOT on a call to a diff root from the context code, then
                // we don't want any splitForADReverse, so we reset srcSplitTrees to the primal instruction:
                // Also keep the "split" trees when on an op_return, because it is transformed temporarily
                // into an assign and we want to keep that
                if (adEnv.curUnitIsContext && !adEnv.activeRootCalledFromContext && srcFullTree.opCode() != ILLang.op_return) {
                    srcSplitTrees = new TapList<>(srcFullTree, null);
                }

                // When this is the diff code called from the context, build (temporary) activity info around diff code,
                // then build validation/debug initialization if requested:
                if (adEnv.curUnitIsContext && adEnv.activeRootCalledFromContext) {
                    // Note: for now, this is just a procedure call, but in future Tapenade it may be any code fragment.
                    // Dirty part: we don't have activity analysis at the diff call site, so we assume this is just a call,
                    //  and we reuse the activity info of the current srcCallTree
                    ActivityPattern curCalledActivity =
                            (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                    srcCallTree, adEnv.curActivity(), "multiActivityCalleePatterns");
                    Tree[] actualParamTreeS = ILUtils.getArguments(srcCallTree).children();
                    Instruction contextInstruction = adEnv.curInstruction() ;
                    beforeActiv = new BoolVector(adEnv.curDiffVectorLen) ;
                    afterActiv = new BoolVector(adEnv.curDiffVectorLen) ;
                    // The following trick ensures (temporarily) that the next two "translateCalleeDataToCallSite"
                    // will not try to expand pointer destinations (this would ruin the ADContext calls):
                    TapTriplet<Block,TapList<TapPair<TapIntList, BoolVector>>,TapList<TapPair<Integer, TapIntList>>[]>
                        memo = detachForPointers(contextInstruction, srcCallTree) ;
                    DataFlowAnalyzer.translateCalleeDataToCallSite(curCalledActivity.callActivity(), beforeActiv,
                                                                   srcResult, actualParamTreeS, true,
                                                                   srcCallTree, contextInstruction, srcCallTree.getAnnotation("callArrow"),
                                                                   adEnv.diffKind, null, null, //passesThroughCall?, passesAroundCall?,
                                                                   true, false) ;
                    DataFlowAnalyzer.translateCalleeDataToCallSite(curCalledActivity.exitActivity(), afterActiv,
                                                                   srcResult, actualParamTreeS, false,
                                                                   srcCallTree, contextInstruction, srcCallTree.getAnnotation("callArrow"),
                                                                   adEnv.diffKind, null, null, //passesThroughCall?, passesAroundCall?,
                                                                   true, false) ;
                    // The following undoes the effect of the previous call to detachForPointers():
                    reattachForPointers(contextInstruction, srcCallTree, memo) ;
                    beforeActivTrees = collectTrueTrees(beforeActiv, adEnv.curSymbolTable());
                    afterActivTrees = collectTrueTrees(afterActiv, adEnv.curSymbolTable());
                    args = null;
                    // Rank of process that must be debugged (used only in the MPI case):
                    if (isDebug && !isAdj) {
                        arg = ILUtils.build(ILLang.op_intCst, 0);
                        args = new TapList<>(arg, null);
                    }
                    // Seed for random X_dot and Y_bar:
                    arg = ILUtils.build(ILLang.op_realCst, (curDiffUnit().isC() ? "0.87" : "0.87_8"));
                    args = new TapList<>(arg, args);
                    // Error tolerance (obsolete ?):
                    if (isDebug && isAdj) {
                        // Error tolerance (obsolete ?):
                        arg = ILUtils.build(ILLang.op_realCst, (curDiffUnit().isC() ? "1.e-1" : "0.1_8"));
                        args = new TapList<>(arg, args);
                    }
                    // Epsilon for divided differences:
                    if (!isAdj) {
                        boolean singlePrecision =
                                hasActiveSinglePrecision(adEnv.curSymbolTable(), beforeActiv, afterActiv);
                        String epsValue;
                        if (singlePrecision) {
                            // [llh] why prefer (curDiffUnit().isFortran9x() ? "1.d-4" : "0.0001_8") over "1.e-4_8" for Fortran?
                            epsValue = (curDiffUnit().isC() ? "1.e-4" : "1.e-4_8") ;
                        } else {
                            // [llh] why prefer (curDiffUnit().isFortran9x() ? "1.d-8" : "0.00000001_8") over "1.e-8_8" for Fortran?
                            epsValue = (curDiffUnit().isC() ? "1.e-8" : "1.e-8_8") ;
                        }
                        arg = ILUtils.build(ILLang.op_realCst, epsValue);
                        args = new TapList<>(arg, args);
                    }
                    dbadTree = ILUtils.buildCall(
                            ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), traceFuncPrefix + "init"),
                            ILUtils.build(ILLang.op_expressions, args));
                    addFuturePlainNodeFwd(dbadTree, null, false,
                            null, null, null, null, true, true,
                            "Start instrumentation before entering diff fragment", false);
                    instructionsOnZones =
                            flowGraphDifferentiator().instructionsOnDeclaredZones(
                                    srcFullTree, adEnv.curSymbolTable(), diffSymbolTable,
                                    (isDebug && isAdj) ? "adDebugAdj_w" : traceFuncPrefix + "init", curDiffUnit(),
                                    (differentiationMode == DiffConstants.TANGENT_MODE ? beforeActiv : afterActiv),
                                    !(isDebug && isAdj), true, true,
                                    adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind), null);
                    if (isDebug && isAdj) {
                        arg = ILUtils.build(ILLang.op_intCst, 0);
                        args = new TapList<>(arg, null);
                        arg = ILUtils.build(ILLang.op_stringCst, differentiationMode == DiffConstants.TANGENT_MODE ? "start" : "end");
                        if (curDiffUnit().isFortran()) {
                            arg = ILUtils.concatWithNull(arg);
                        }
                        args = new TapList<>(arg, args);
                        dbadTree = ILUtils.buildCall(
                                ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), "adDebugAdj_wDisplay"),
                                ILUtils.build(ILLang.op_expressions, args));
                        instructionsOnZones = TapList.append(instructionsOnZones, new TapList<>(dbadTree, null));
                        arg = ILUtils.build(ILLang.op_stringCst, differentiationMode == DiffConstants.TANGENT_MODE ? "start" : "end");
                        if (curDiffUnit().isFortran()) {
                            arg = ILUtils.concatWithNull(arg);
                        }
                        args = new TapList<>(arg, null);
                        dbadTree = ILUtils.build(ILLang.op_if,
                                ILUtils.buildCall(
                                        ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), traceFuncPrefix + "here"),
                                        ILUtils.build(ILLang.op_expressions, args)),
                                ILUtils.build(ILLang.op_blockStatement, instructionsOnZones));
                        instructionsOnZones = new TapList<>(dbadTree, null);
                    }
                    while (instructionsOnZones != null) {
                        addFuturePlainNodeFwd(instructionsOnZones.head, null, false,
                                null, differentiationMode == DiffConstants.TANGENT_MODE ? beforeActivTrees : afterActivTrees,
                                null, differentiationMode == DiffConstants.TANGENT_MODE ? beforeActivTrees : afterActivTrees, true, true,
                                "Instrument actives before entering diff fragment", false);
                        instructionsOnZones = instructionsOnZones.tail;
                    }
                }

                // srcSplitTrees contains the split source trees, in the fwd order
                while (srcSplitTrees != null) {
                    // Copie, sinon partage entre le source et le differentie' et pb avec -html:
                    srcTree = ILUtils.copy(srcSplitTrees.head);
                    if (adEnv.traceCurBlock != 0) {
                        String oldActiveMark = TapEnv.activeMark();
                        String oldPointerActiveMark = TapEnv.pointerActiveMark();
                        TapEnv.setActiveMark("@");
                        TapEnv.setPointerActiveMark("#");
                        TapEnv.printlnOnTrace("    --- differentiating source Tree: " + ILUtils.toString(srcTree, adEnv.curActivity()));
                        TapEnv.setActiveMark(oldActiveMark);
                        TapEnv.setPointerActiveMark(oldPointerActiveMark);
                    }
                    inUseNewSymbolHolders = null; // reset NewSymbolHolder's already used.
                    fromInclude = adEnv.curInstruction().fromInclude();
                    hasDeclaration = false;
                    srcDecl = null;
                    srcAction = null;
                    srcResult = null;
                    // Caution: the srcAction is the imperative action done by the instruction, each time control reaches it.
                    // Actions that are done only once, i.e. initializations attached to a PARAMETER or SAVE declaration
                    // (beware that in FORTRAN9x, initialization inside a declaration makes it a SAVE!)
                    // are *not* treated as scrAction, and will be differentiated (if needed) during diff of the declaration
                    // and their diff action will remain combined with their diff declaration.
                    if (srcTree == null || (!ILUtils.isADeclaration(srcTree) && srcTree.opCode() != ILLang.op_data)) {
                        srcAction = srcTree;
                    } else if (srcTree.opCode() == ILLang.op_varDeclaration && !adEnv.curUnit().isFortran9x()) {
                        TapPair<Tree, Tree> declAndInit = ILUtils.splitDeclInit(srcTree, false, adEnv.curUnit());
                        srcDecl = declAndInit.first;
                        srcAction = declAndInit.second;
                        hasDeclaration = true;
                    } else {
                        srcDecl = srcTree;
                        hasDeclaration = true;
                    }
                    srcResult = null;
                    srcCallTree = null;
                    calledUnit = null;
                    calledActivity = null;
                    diffCallNeeded = false;
                    diffCalledUnit = null;
                    normalCheckpointedCall = true;
                    splitAdjointCall = false;
                    if (srcAction != null) {
                        if (srcAction.opCode() == ILLang.op_call) {
                            srcResult = null;
                            srcCallTree = srcAction;
                        } else if (srcAction.opCode() == ILLang.op_assign
                                   && srcAction.down(2).opCode() == ILLang.op_call) {
                            srcResult = srcAction.down(1);
                            srcCallTree = srcAction.down(2);
                        } else if (srcAction.opCode() == ILLang.op_assign
                                   || srcAction.opCode() == ILLang.op_plusAssign
                                   || srcAction.opCode() == ILLang.op_minusAssign
                                   || srcAction.opCode() == ILLang.op_timesAssign
                                   || srcAction.opCode() == ILLang.op_divAssign) {
                            srcResult = srcAction.down(1);
                        }
                    }
                    if (srcCallTree != null) {
                        calledUnit = DataFlowAnalyzer.getCalledUnit(srcCallTree, adEnv.curSymbolTable());
                        calledActivity =
                                (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(srcCallTree, adEnv.curActivity(), "multiActivityCalleePatterns");
                        resultPointerActivity = null;
                        if (srcResult != null) {
                            TapList writtenZonesTree =
                                    adEnv.curSymbolTable().treeOfZonesOfValue(srcResult, null, adEnv.curInstruction(), null);
                            // "PointerActiveUsage":
                            resultPointerActivity =
                                    DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZones(
                                            writtenZonesTree, afterReqX, null, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
                        }
                        diffCallNeeded = calledActivity!=null
                                && callGraphDifferentiator().diffCallNeeded(srcCallTree, adEnv.curActivity(),
                                           calledUnit, calledActivity, beforeAvlX, afterReqX,
                                           resultPointerActivity, block, adEnv.curInstruction());
                        diffCalledUnit = calledActivity == null ? null : adEnv.getDiffOfUnit(calledUnit, calledActivity, curDiffUnitSort());
                        splitAdjointCall = !adEnv.curInstruction().thisCallIsCheckpointed(calledUnit) ;
                        normalCheckpointedCall = !adEnv.curUnitIsContext && differentiationMode != DiffConstants.TANGENT_MODE && !splitAdjointCall;
                    }

                    isMPICall = calledUnit != null && MPIcallInfo.isMessagePassingFunction(calledUnit.name(), calledUnit.language());

                    isIncrement = false;
                    if (srcAction != null && srcAction.opCode() == ILLang.op_unary) {
                        String unaryName = ILUtils.getIdentString(srcAction.down(1));
                        if (unaryName != null &&
                                (unaryName.equals("++prefix") || unaryName.equals("++postfix") ||
                                        unaryName.equals("--prefix") || unaryName.equals("--postfix"))) {
                            isIncrement = true;
                        }
                    }
                    boolean assignsPointer =
                        (srcResult!=null && TypeSpec.isA(adEnv.curSymbolTable().typeOf(srcResult), SymbolTableConstants.POINTERTYPE)) ;

                    if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace("        SRCTREE:" + srcTree + " SRCDECL:" + srcDecl + " SRCACTION:" + srcAction + " CALLEDUNIT:"+calledUnit+" DIFFCALLNEEDED:"+diffCallNeeded);
                    }

                    // "mustSaveTBR" means that this location in the FWD sweep must be preceded
                    //  by PUSH'es of overwritten TBR values. Standard reason for this is that this
                    //  original instruction is diff-live, but this may also be true
                    //  (even if the instruction is not copied in the FWD sweep because
                    //  it will only be recomputed in the BWD sweep)
                    //  because TBR zones must still be saved now before it is too late.
                    //  (cf e.g. set08/lh013 or set08/lh061)

                    // "mustSaveTBROther" gathers other, non-standard reasons for "mustSaveTBR":
                    mustSaveTBROther =
                        (TapEnv.debugAdMode()==TapEnv.ADJ_DEBUG
                         || !TapEnv.removeDeadPrimal()
                         || adEnv.curUnitIsContext
                         // If this subroutine is passive but "contains" an active, we must at least copy its code:
                         || curUnitIsCopied
                         || TapEnv.diffLivenessAnalyzer()==null
                         // cf bug Bravington nonRegrF77:lha50. If call P() is marked "split", but this
                         // call is passive, there will be no call P_BWD() so there must be no call P_FWD():
                         || (diffCallNeeded && !normalCheckpointedCall)
                         // special case cf F90:v197
                         || !adEnv.curUnitIsActiveUnit
                         || isMPICall
                         || (!TapEnv.removeDeadControl() && !TapEnv.mustAdjoint())
                         || (hasDeclaration && srcAction != null && isAConstantDeclInit(srcAction))) ;
                    reasonForMustSaveTBROther = null ;
                    if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                        reasonForMustSaveTBROther =
                            (TapEnv.debugAdMode()==TapEnv.ADJ_DEBUG ? "T|" : "F|")
                            + (!TapEnv.removeDeadPrimal()  ? "T|" : "F|")
                            + (adEnv.curUnitIsContext  ? "T|" : "F|")
                            + (curUnitIsCopied  ? "T|" : "F|")
                            + (TapEnv.diffLivenessAnalyzer()==null  ? "T|" : "F|")
                            + ((diffCallNeeded && !normalCheckpointedCall)  ? "T|" : "F|")
                            + (!adEnv.curUnitIsActiveUnit  ? "T|" : "F|")
                            + (isMPICall  ? "T|" : "F|")
                            + ((!TapEnv.removeDeadControl() && !TapEnv.mustAdjoint()) ? "T|" : "F|")
                            + ((hasDeclaration && srcAction != null && isAConstantDeclInit(srcAction)) ? "T|" : "F|") ;
                    }

                    // [llh] TODO: test DiffLivenessAnalyzer.isTreeRequiredInDiff(curActivity(), srcAction, modeIsJoint, false)))?
                    //   for the case of phi = phi ... in validation test "imft-burgers".
                    // [llh] TODO: extract the following test to try to make it identical
                    //   in its 2 locations here and in ADTBRAnalyzer "test adjoint requires instr"
                    mustSaveTBR = mustSaveTBROther ||
                        // Standard reason for mustSaveTBR: instruction is diff-live:
                        (srcAction != null && DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), srcAction, modeIsJoint, false)) ;
                    mustSaveTBROnDiffPtr = (assignsPointer &&
                                            (mustSaveTBROther ||
                                             // Standard reason for mustSaveTBR: instruction (on differentiated pointer) is diff-live:
                                             (srcAction != null
                                              && DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), srcAction, modeIsJoint, true)))) ;
                    if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace(
                                "        >> MUSTSAVETBR: ("+reasonForMustSaveTBROther+"("
                                + (srcAction == null
                                   ? "F"
                                   : ("T&"
                                      + (DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), srcAction, modeIsJoint, false) ? "T" : "F")
                                      + (assignsPointer
                                         ? (" Ptr:"
                                            + (DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), srcAction, modeIsJoint, true) ? "T" : "F"))
                                         : "")))
                                + ")) => " + mustSaveTBR
                                +(assignsPointer ? (" Ptr:"+mustSaveTBROnDiffPtr) : "")) ;
                    }

                    // If this is the very last instruction (which may be a test),
                    //  and it is live, put this info into the output "lastTestLive"
                    if (lastTestLive != null && instructions.tail == null && srcSplitTrees.tail == null
                        && (mustSaveTBR || flowGraphDifferentiator().keepEmptyDiffBlocks)) {
                        lastTestLive.set(true);
                    }

                    // "remainsInFwdSweep" is true when the FWD sweep must have a copy of the
                    // original instruction (or the fwd diff of the call).
                    // Exceptionally, it may be false even when mustSaveTBR true, in the special
                    // case of recomputations (cf e.g. set08/lh013 or set08/lh061) :
                    reasonForRemainsInFwdSweep = null ;
                    remainsInFwdSweep = mustSaveTBR || (lastTestLive != null && lastTestLive.get());
                    remainsInFwdSweepOnDiffPtr = assignsPointer; //was true; //mustSaveTBROnDiffPtr || (lastTestLive != null && lastTestLive.get());
                    reasonForRemainsInFwdSweep = (mustSaveTBR ? "T" : "F") + "|"
                            + ((lastTestLive != null && lastTestLive.get()) ? "T" : "F");

                    if (differentiationMode != DiffConstants.TANGENT_MODE) {
                        BlockStorage<int[]> mayBeRecomputedS = adEnv.curActivity().mayBeRecomputed() ;
                        BlockStorage<int[]> butRemainsInFwdSweepS = adEnv.curActivity().butRemainsInFwdSweep() ;
                        if (mayBeRecomputedS != null) {
                            // extra refinement when using recomputation: this srcAction will disappear from
                            // the FWD sweep when this srcAction will be only (re)computed in the BWD sweep
                            // (rather than PUSH/POPping its result)
                            // and need not remain for other uses in the FWD sweep:
                            boolean primalNotInFwd =
                                (ActivityPattern.getInstructionBoolInfo(mayBeRecomputedS, adEnv.curInstruction(), 1) &&
                                 !ActivityPattern.getInstructionBoolInfo(butRemainsInFwdSweepS, adEnv.curInstruction(), 1)) ;
                            if (primalNotInFwd) remainsInFwdSweep = false;
                            boolean diffPtrNotInFwd =
                                (ActivityPattern.getInstructionBoolInfo(mayBeRecomputedS, adEnv.curInstruction(), 2) &&
                                 !ActivityPattern.getInstructionBoolInfo(butRemainsInFwdSweepS, adEnv.curInstruction(), 2)) ;
                            if (diffPtrNotInFwd) remainsInFwdSweepOnDiffPtr = false;
                            reasonForRemainsInFwdSweep = "(" + reasonForRemainsInFwdSweep + ")&("+(primalNotInFwd?"f":"t")+(assignsPointer?(diffPtrNotInFwd?"f":"t"):"")+")" ;
                        } else {
                            reasonForRemainsInFwdSweep = "(" + reasonForRemainsInFwdSweep + ")&T";
                        }
                    }

                    // If the "action" part of the Instr is not required,
                    // and there is a declaration part, we must at least
                    // keep this declaration part in the fwd sweep:
                    if (!remainsInFwdSweep && hasDeclaration) {
                        srcTree = srcDecl;
                        remainsInFwdSweep = true;
                        remainsInFwdSweepOnDiffPtr = true;
                        reasonForRemainsInFwdSweep = "(" + reasonForRemainsInFwdSweep + ")|T";
                    } else {
                        reasonForRemainsInFwdSweep = "(" + reasonForRemainsInFwdSweep + ")|F";
                    }

                    // In TANGENT mode the original call must be removed, since the tangent call does it all.
                    if (differentiationMode == DiffConstants.TANGENT_MODE
                        && (diffCalledUnit != null
                            || calledUnit != null && !calledUnit.hasPredefinedDerivatives())) {
                        remainsInFwdSweep =
                                calledUnit != null && !calledUnit.isIntrinsic() && calledUnit.hasPredefinedDerivatives();
                        remainsInFwdSweepOnDiffPtr = remainsInFwdSweep ;
                        reasonForRemainsInFwdSweep = "(" + reasonForRemainsInFwdSweep + ")/" + (remainsInFwdSweep ? "T" : "F");
                    }

                    if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace("        >> REMAINSINFWDSWEEP: " + reasonForRemainsInFwdSweep + " => " + remainsInFwdSweep
                                              +(assignsPointer ? (" Ptr:"+remainsInFwdSweepOnDiffPtr) : "")) ;
                    }

                    //TODO: Refactor and give better names:
                    if (remainsInFwdSweep) mustSaveTBR = true ;
                    if (remainsInFwdSweepOnDiffPtr) mustSaveTBROnDiffPtr = true ;

                    if (srcAction != null && srcAction.opCode() == ILLang.op_assign && srcAction.down(2).opCode() == ILLang.op_allocate) {
                        // Case of memory allocation instruction "<PointerRef> = allocate(<SizeExpr>, <NbElemExpr>, <Type>, <??>)" :
                        dynMemoryDifferentiator().buildDiffInstructionsOfAssignAllocate(srcAction, differentiationMode,
                                fwdBlock.symbolTable, bwdBlock == null ? null : bwdBlock.symbolTable,
                                beforeActiv, afterActiv, afterReqX, beforeAvlX);
                    } else if (srcAction != null && srcAction.opCode() == ILLang.op_deallocate) {
                        // Case of memory deallocation instruction "deallocate(<PointerExpr>, <??>)" :
                        dynMemoryDifferentiator().buildDiffInstructionsOfDeallocate(srcAction, differentiationMode, remainsInFwdSweep,
                                fwdBlock.symbolTable, bwdBlock == null ? null : bwdBlock.symbolTable,
                                beforeActiv, afterActiv, afterReqX, beforeAvlX, beforeTBR, beforeTBROnDiffPtr);
                    } else if (isMPICall) {
                        // Case of MPI calls :
                        procedureCallDifferentiator().buildDiffInstructionsOfMPICall(srcCallTree, srcResult, differentiationMode, calledUnit, calledActivity,
                                MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), srcCallTree, adEnv.curUnit().language(), block),
                                diffCallNeeded, fwdBlock.symbolTable, bwdBlock == null ? null : bwdBlock.symbolTable,
                                beforeActiv, afterActiv, beforeUseful, afterUseful);
                    } else if (calledUnit != null
                            && !isMPICall
                            && !calledUnit.hasPredefinedDerivatives()
                            && !calledUnit.name().equals("null")) {
                        // Case of standard procedure calls "<srcResult> = <srcCallTree>" :
                        // Since buildDiffInstructions will insert reinitializations when activity
                        //  switches, we must re-insert staticActiv into beforeActiv and afterActiv:
                        BoolVector fullBeforeActiv = beforeActiv ;
                        BoolVector fullAfterActiv = afterActiv ;
                        BoolVector staticActiv =
                            new BoolVector(adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind)) ;
                        if (adEnv.curActivity().staticActivity()!=null) {
                            // Question: should we also copy the static active zones for curUnit's public zones?
                            staticActiv.setCopy(adEnv.curActivity().staticActivity(),
                                         adEnv.curUnit().globalZonesNumber(adEnv.diffKind)) ;
                            if (beforeActiv!=null) {
                                fullBeforeActiv = beforeActiv.or(staticActiv) ;
                            }
                            if (afterActiv!=null) {
                                fullAfterActiv = afterActiv.or(staticActiv) ;
                            }
                        }
                        procedureCallDifferentiator().buildDiffInstructions(
                                srcCallTree, srcResult, differentiationMode, calledUnit, calledActivity,
                                diffCallNeeded, remainsInFwdSweep,
                                mustSaveTBR, normalCheckpointedCall, splitAdjointCall,
                                fwdBlock.symbolTable, (bwdBlock==null ? null : bwdBlock.symbolTable),
                                fullBeforeActiv, fullAfterActiv,
                                beforeUseful, afterUseful, beforeReqX, afterReqX, beforeAvlX);
                    } else if (srcAction != null && isAssignment(srcAction)) {
                        // Case of a simple assignment, possibly as the initializer inside a declaration, possibly containing intrinsic calls :
                        Tree[] diffDeclR = new Tree[TapEnv.diffReplica()] ;
                        if (hasDeclaration) {
                            // Build the differential of the declaration part of this assignment:
                            Instruction copyInstrInDiff = new Instruction(srcTree, null);
                            TapList<Instruction>[] diffDeclarationsR =
                                    differentiateInstructionDeclaration(adEnv.curInstruction(), differentiationMode,
                                            copyInstrInDiff, null, srcDecl, false,
                                            diffSymbolTable, block,
                                            beforeActiv, afterActiv, beforeUseful, afterUseful,
                                            beforeReqX, afterReqX, beforeAvlX, afterAvlX,
                                            block.symbolTable.unit.language(), true, false);
                            srcDecl = copyInstrInDiff.tree;
                            TapList<Instruction> errorLostInstructions = null ;
                            for (int iReplic=0 ; iReplic<diffDeclarationsR.length ; ++iReplic) {
                                if (diffDeclarationsR[iReplic]!=null) {
                                    diffDeclR[iReplic] = diffDeclarationsR[iReplic].head.tree;
                                    errorLostInstructions = TapList.append(errorLostInstructions, diffDeclarationsR[iReplic].tail) ;
                                } else {
                                    diffDeclR[iReplic] = null ;
                                }
                            }
                            if (errorLostInstructions!=null) {
                                TapEnv.toolError("Problem for buildDiffInstructionsOfPlainAssignment: remaining pieces of diff declaration are lost: "+errorLostInstructions) ;
                            }
                        }
                        if (diffDeclR.length>0 && diffDeclR[0]!=null && remainsInFwdSweep &&
                            // many special cases where we want to keep the declaration's assignment:
                            TapEnv.debugAdMode() != TapEnv.ADJ_DEBUG && TapEnv.removeDeadPrimal() && !adEnv.curUnitIsContext
                            && !curUnitIsCopied && TapEnv.diffLivenessAnalyzer() != null && adEnv.curUnitIsActiveUnit
                            // If this assignment is in fact not annotated as live,
                            // although the declaration is annotated as live because declarations always are
                            // (so far, see DiffLivenessAnalyzer, "We have no liveness analysis on declarations yet"),
                            && !DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), srcAction, modeIsJoint, false)) {
                            // in that case, we prefer to keep the declaration part and replace the assigned initial value with "zero":
                            remainsInFwdSweep = false;
                        }
                        buildDiffInstructionsOfPlainAssignment(srcAction, srcDecl, diffDeclR, differentiationMode,
                                remainsInFwdSweep, remainsInFwdSweepOnDiffPtr, mustSaveTBR, mustSaveTBROnDiffPtr,
                                beforeActiv, afterActiv, beforeReqX, afterReqX, beforeAvlX);
                    } else if (srcAction == null && hasDeclaration) {
                        // Case of a plain declaration, with no initialization nor action (except SAVE or PARAMETER(Fortran constants)):
                        Instruction copyInstrInDiff = new Instruction(srcTree, null);
                        ToObject<TapList<Instruction>> toDiffDeclarationsForFwd = null ;
                        if (separatedFwdBdwScopes(differentiationMode)) {
                            toDiffDeclarationsForFwd = new ToObject<>(null) ;
                        }
                        TapList<Instruction>[] diffDeclarationsR =
                                differentiateInstructionDeclaration(adEnv.curInstruction(), differentiationMode,
                                        copyInstrInDiff, toDiffDeclarationsForFwd, srcDecl, false,
                                        diffSymbolTable, block,
                                        beforeActiv, afterActiv, beforeUseful, afterUseful,
                                        beforeReqX, afterReqX, beforeAvlX, afterAvlX,
                                        block.symbolTable.unit.language(),
                                        // See "Caution" comment on scrAction:
                                        !adEnv.curUnit().isFortran9x(), false);
                        TapList<Instruction> diffDeclarationsForFwd =
                            (toDiffDeclarationsForFwd==null ? null : toDiffDeclarationsForFwd.obj()) ;
                        if (copyInstrInDiff.tree != null) {
                            buildDiffInstructionsOfPlainDeclaration(copyInstrInDiff.tree,
                                    diffDeclarationsR, diffDeclarationsForFwd, differentiationMode, remainsInFwdSweep);
                        }
                    } else if (isIncrement) {
                        // Case of increment/decrement operation:
                        buildDiffInstructionsOfIncrement(srcAction, differentiationMode,
                                remainsInFwdSweep, mustSaveTBR,
                                beforeActiv, afterActiv, afterReqX, beforeAvlX);
                    } else if (srcAction != null && srcAction.opCode() == ILLang.op_return) {
                        // Case of a final "return" instruction:
                        buildDiffInstructionsOfReturn(srcAction, differentiationMode,
                                remainsInFwdSweep,
                                beforeActiv, afterActiv, afterReqX, beforeAvlX);
                    } else if (ILUtils.isIORead(srcAction)) {
                        // Case of IO that overwrites values:
                        buildDiffInstructionsOfIORead(srcAction, differentiationMode,
                                remainsInFwdSweep, mustSaveTBR,
                                beforeActiv, afterActiv, beforeUseful, afterUseful);
                    } else if (ILUtils.isParallelController(srcTree)) {
                        buildDiffInstructionsOfParallelPragma(srcTree, differentiationMode, beforeActiv);
                    } else {
                        // Default case: just copy the primal instruction: (little tweak for if's when -valid)
                        buildDiffInstructionsOfOther((hasDeclaration ? srcDecl : srcAction), differentiationMode,
                                remainsInFwdSweep,
                                beforeActiv);
                    }

                    srcSplitTrees = srcSplitTrees.tail;
                }

                // In debug modes, make sure the DIFF of all temporary vars are initialized:
                if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG && !adEnv.curUnitIsContext && differentiationMode != DiffConstants.TANGENT_MODE) {
                    Tree tempRef, diffTempRef;
                    while (tempVarsForDebug != null) {
                        tempRef = tempVarsForDebug.head;
                        diffTempRef = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tempRef, fwdBlock.symbolTable,
                                false, tempRef, false, true, tempRef);
                        addFuturePlainNodeBwd(
                                ILUtils.build(ILLang.op_assign, diffTempRef, ILUtils.build(ILLang.op_realCst, "0.D0")), null, true,
                                null, null, null, new TapList<>(tempRef, null), false, false,
                                "Initialize DIFF OF local temporary " + tempRef + " (due to debugADJ)", false);
                        tempVarsForDebug = tempVarsForDebug.tail;
                    }
                }

                // For an active call from CONTEXT, transfer the bwd instructions back into the fwd instructions:
                if (adEnv.activeRootCalledFromContext && (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE)) {
                    bwdInstructionsGraph.buildInstructions(adEnv.curSymbolTable(), fwdBlock.symbolTable, adEnv, curDiffUnit());
                    TapList<NewBlockGraphNode> bwdNodes = bwdInstructionsGraph.nodes;
                    NewBlockGraphNode bwdNode;
                    while (bwdNodes != null) {
                        bwdNode = bwdNodes.head;
                        addFuturePlainNodeFwd(bwdNode.tree, bwdNode.whereMask, bwdNode.multiDir,
                                null, null, null, null, true, true, "Transferred from BWD", false);
                        bwdNodes = bwdNodes.tail;
                    }
                    bwdInstructionsGraph.reset();
                }

                // When this is the diff code called from the context, place validation/debug conclusion if requested:
                if (adEnv.curUnitIsContext && adEnv.activeRootCalledFromContext) {
                    if (!isDebug) {
                        dbadTree = ILUtils.buildCall(
                                ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), traceFuncPrefix + "startConclude"),
                                ILUtils.build(ILLang.op_expressions));
                        addFuturePlainNodeFwd(dbadTree, null, false,
                                null, null, null, null, true, true,
                                "Start-conclude instrumentation after exiting diff fragment", false);
                    }
                    instructionsOnZones =
                            flowGraphDifferentiator().instructionsOnDeclaredZones(
                                    srcFullTree, adEnv.curSymbolTable(), diffSymbolTable,
                                    (isDebug && isAdj ? "adDebugAdj_r" : traceFuncPrefix + "conclude"),
                                    curDiffUnit(),
                                    (differentiationMode == DiffConstants.TANGENT_MODE ? afterActiv : beforeActiv),
                                    !(isDebug && isAdj), (isDebug && isAdj), (isDebug && isAdj),
                                    adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind), null);
                    if (isDebug && isAdj) {
                        arg = ILUtils.build(ILLang.op_intCst, 0);
                        args = new TapList<>(arg, null);
                        arg = ILUtils.build(ILLang.op_stringCst, differentiationMode == DiffConstants.TANGENT_MODE ? "end" : "start");
                        if (curDiffUnit().isFortran()) {
                            arg = ILUtils.concatWithNull(arg);
                        }
                        args = new TapList<>(arg, args);
                        dbadTree = ILUtils.buildCall(
                                ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), "adDebugAdj_rDisplay"),
                                ILUtils.build(ILLang.op_expressions, args));
                        instructionsOnZones = TapList.append(instructionsOnZones, new TapList<>(dbadTree, null));
                        arg = ILUtils.build(ILLang.op_stringCst, differentiationMode == DiffConstants.TANGENT_MODE ? "end" : "start");
                        if (curDiffUnit().isFortran()) {
                            arg = ILUtils.concatWithNull(arg);
                        }
                        args = new TapList<>(arg, null);
                        dbadTree = ILUtils.build(ILLang.op_if,
                                ILUtils.buildCall(
                                        ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), traceFuncPrefix + "here"),
                                        ILUtils.build(ILLang.op_expressions, args)),
                                ILUtils.build(ILLang.op_blockStatement, instructionsOnZones));
                        instructionsOnZones = new TapList<>(dbadTree, null);
                    }
                    while (instructionsOnZones != null) {
                        addFuturePlainNodeFwd(instructionsOnZones.head, null, false,
                                null, differentiationMode == DiffConstants.TANGENT_MODE ? afterActivTrees : beforeActivTrees,
                                null, differentiationMode == DiffConstants.TANGENT_MODE ? afterActivTrees : beforeActivTrees, true, true,
                                "Instrument actives after exiting diff fragment", false);
                        instructionsOnZones = instructionsOnZones.tail;
                    }
                    traceFuncPrefix = (isDebug ? "adDebug" : "adContext")
                        + (isAdj ? "Adj_" : (TapEnv.get().complexStep ? "Cpx_" : "Tgt_"));
                    dbadTree = ILUtils.buildCall(
                            ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), traceFuncPrefix + "conclude"),
                            ILUtils.build(ILLang.op_expressions));
                    addFuturePlainNodeFwd(dbadTree, null, false,
                            null, null, null, null, true, true,
                            "Conclude instrumentation after exiting diff fragment", false);
                }

                // Add possible recomputations to be placed between curInstruction and the Instruction downstream:
                if (blockRecomputations != null && blockRecomputations.head!=null) {
                    buildBwdRecomputations(blockRecomputations.head, beforeActiv, afterActiv, beforeAvlX, afterReqX);
                }

                // Collect comments:
                waitingComments = ILUtils.appendComments(waitingComments, adEnv.curInstruction().postComments);
                waitingCommentsBlock = ILUtils.appendComments(waitingCommentsBlock, adEnv.curInstruction().postCommentsBlock);
            }

            beforeActiv = afterActiv;
            beforeUseful = afterUseful;
            beforeReqX = afterReqX;
            beforeAvlX = afterAvlX;
            instructions = instructions.tail;
        }

        adEnv.setCurInstruction(null);

        if (multiDirMode() && TapEnv.get().mergeDiffInstructions) {
            fwdInstructionsGraph.condense(adEnv.curUnit());
        } else {
            fwdInstructionsGraph.closeGroup();
        }
        TapList<NewBlockGraphNode> orderedFwdNodes = fwdInstructionsGraph.topoSort();
        NewBlockGraphNode curNode;
        TapList<Instruction> hdFwdInstructions = new TapList<>(null, null);
        TapList<Instruction> tlFwdInstructions = hdFwdInstructions;
        Instruction fwdInstruction = null;
        while (orderedFwdNodes != null) {
            curNode = orderedFwdNodes.head;
            if (curNode.multiDir || curNode.hasManyInstructions()) {
                curNode.gatherMultiInstructions(fwdBlock.symbolTable, curDiffUnit(),
                        varRefDifferentiator().dirIndexSymbolHolder, varRefDifferentiator().multiDirIterDescriptor);
                if (TapEnv.doOpenMP() && block.parallelControls != null) {
                    varRefDifferentiator().dirIndexSymbolHolder.preparePrivateClause(block, block.symbolTable, true, false) ;
                    varRefDifferentiator().dirIndexSymbolHolder.preparePrivateClause(block, block.symbolTable, false, false) ;
                    varRefDifferentiator().dirNumberSymbolHolder.prepareSharedClause(block, block.symbolTable, true, false) ;
                    varRefDifferentiator().dirNumberSymbolHolder.prepareSharedClause(block, block.symbolTable, false, false) ;
                }
            }
            curNode.buildInstruction(adEnv.curSymbolTable(), fwdBlock.symbolTable, adEnv, curDiffUnit());
            fwdInstruction = curNode.diffInstruction;
            fwdInstruction.block = fwdBlock;
            tlFwdInstructions = tlFwdInstructions.placdl(fwdInstruction);
            orderedFwdNodes = orderedFwdNodes.tail;
        }

        if (fwdInstruction != null && waitingComments != null) {
            fwdInstruction.postComments =
                    ILUtils.appendComments(fwdInstruction.postComments, waitingComments);
            waitingComments = null;
        }
        if (fwdInstruction != null && waitingCommentsBlock != null) {
            fwdInstruction.postCommentsBlock =
                    ILUtils.appendComments(fwdInstruction.postCommentsBlock, waitingCommentsBlock);
            waitingCommentsBlock = null;
        }

        while (containedUnitsFwd.tail != null) {
            curFwdDiffUnit().addNonFlowInstruction(
                    Instruction.createUnitDefinitionStub(containedUnitsFwd.tail.head, fwdBlock));
            containedUnitsFwd = containedUnitsFwd.tail;
        }
        // Put the sorted fwd differentiated instructions into the fwdBlock:
        fwdBlock.instructions = hdFwdInstructions.tail;
        flattenBlockStatements(fwdBlock);

        if (!adEnv.curUnitIsContext && differentiationMode != DiffConstants.TANGENT_MODE) {
            if (TapEnv.get().mergeDiffInstructions)
            // Condense the bwdInstructionsGraph:
            {
                bwdInstructionsGraph.condense(adEnv.curUnit());
            } else {
                bwdInstructionsGraph.closeGroup();
            }

            // // [llh] Please don't remove the present comment (5 lines)
            // // To view bwdInstructionsGraph graphically.
            // // Note: View.view(bwdInstructionsGraph) uses NewBlockGraphNode.recomputeText()
            // View.open();
            // View.view(bwdInstructionsGraph);

            bwdInstructionsGraph.buildInstructions(adEnv.curSymbolTable(), bwdBlock.symbolTable, adEnv, curDiffUnit());
            // BEWARE: this is a 2nd condense, dealing with WHERE statements :
            if (TapEnv.debugAdMode() == TapEnv.NO_DEBUG) {
                bwdInstructionsGraph.condense(adEnv.curUnit());
            }
            TapList<NewBlockGraphNode> orderedBwdNodes = bwdInstructionsGraph.topoSort();
            TapList<Instruction> hdRevInstructions = new TapList<>(null, null);
            TapList<Instruction> tlRevInstructions = hdRevInstructions;
            Instruction bwdInstruction;
            // Fill the bwdBlock with the new adjoint instructions:
            while (orderedBwdNodes != null) {
                bwdInstruction = orderedBwdNodes.head.diffInstruction;
                bwdInstruction.block = bwdBlock;
                if (bwdInstruction.tree != null) {
                    // Add loop on multiDir ("vector adjoint") if necessary:
                    //[llh] Warning: if there is a WHERE around this, it is not placed yet
                    // => when the "where" are placed, be careful to put the "where" INSIDE the do-nbdirs
                    // TODO: this is ugly, due to the fact that the "where" are placed much too late, in TreeGen!
                    if (orderedBwdNodes.head.multiDir) {
                        Tree loopBody = bwdInstruction.tree;
                        bwdInstruction.tree =
                                ILUtils.build(ILLang.op_loop,
                                        ILUtils.build(ILLang.op_none),
                                        ILUtils.build(ILLang.op_none),
                                        varRefDifferentiator().multiDirIterDescriptor.buildDoHeaderTree(bwdBlock.symbolTable, curDiffUnit(), true),
                                        loopBody);
                        if (TapEnv.doOpenMP() && block.parallelControls != null) {
                            varRefDifferentiator().dirIndexSymbolHolder.preparePrivateClause(block, block.symbolTable, true, false) ;
                            varRefDifferentiator().dirIndexSymbolHolder.preparePrivateClause(block, block.symbolTable, false, false) ;
                            varRefDifferentiator().dirNumberSymbolHolder.prepareSharedClause(block, block.symbolTable, true, false) ;
                            varRefDifferentiator().dirNumberSymbolHolder.prepareSharedClause(block, block.symbolTable, false, false) ;
                        }
                    }
                    tlRevInstructions = tlRevInstructions.placdl(bwdInstruction);
                }
                orderedBwdNodes = orderedBwdNodes.tail;
            }
            while (containedUnitsBwd.tail != null) {
                curDiffUnit().addNonFlowInstruction(
                        Instruction.createUnitDefinitionStub(containedUnitsBwd.tail.head, bwdBlock));
                containedUnitsBwd = containedUnitsBwd.tail;
            }
            bwdBlock.instructions = hdRevInstructions.tail;
            flattenBlockStatements(bwdBlock);

            Block revDeclBlock = bwdBlock.symbolTable.declarationsBlock;
            if (revDeclBlock != fwdBlock.symbolTable.declarationsBlock) {
                // if revDeclBlock is really a backward Block and not the same as the forward declaration Block.
                TapList<TapTriplet<Tree, Instruction, Instruction>> declarationsForRevDeclBlock = hdRevDeclarations.tail;
                Instruction revDecl;
                while (declarationsForRevDeclBlock != null) {
                    TapTriplet<Tree, Instruction, Instruction> nextDecl = declarationsForRevDeclBlock.head;
                    revDecl = new Instruction(nextDecl.first);
                    revDecl.setFromIncludeCopy(nextDecl.third, true);
                    revDecl.isPhantom = nextDecl.second.isPhantom;
                    revDecl.setPosition(nextDecl.second);
                    if (!revDeclBlock.alreadyPresent(revDecl)) { // because of FWD and BWD blocks glued together
                        revDeclBlock.addInstrTl(revDecl);
                    }
                    declarationsForRevDeclBlock = declarationsForRevDeclBlock.tail;
                }
            }
        }

        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace(" ==> fwdBlock:" + fwdBlock + " : " + fwdBlock.instructions
                    //+" lastTestLive:"+(lastTestLive==null?"null":lastTestLive.get())
                    + " SymbolTable:" + fwdBlock.symbolTable.addressChain()
            );
            if (bwdBlock != null) {
                TapEnv.printlnOnTrace(" ==> bwdBlock:" + bwdBlock + " : " + bwdBlock.instructions
                        + " SymbolTable:" + bwdBlock.symbolTable.addressChain()
                );
            }
            if (hdRevDeclarations.tail != null) {
                TapEnv.printlnOnTrace(" ==>+bwdDecls:" + hdRevDeclarations.tail +
                        (bwdBlock.symbolTable.declarationsBlock == fwdBlock.symbolTable.declarationsBlock
                                ? " discarded because FWD and BWD blocks share scope"
                                : " added at tail of " + bwdBlock.symbolTable.declarationsBlock + " declaration Block of " + bwdBlock.symbolTable.addressChain()));
            }
        }
    }

    /**
     * Build the needed recomputation instructions into the BWD sweep.
     * These recomputations of primal values allow Tapenade to spare some PUSH/POP's
     */
    private void buildBwdRecomputations(TapPair<TapList<Instruction>, TapList<Instruction>> recomputations,
                                        BoolVector beforeActiv, BoolVector afterActiv,
                                        BoolVector beforeAvlX, BoolVector afterReqX) {
        Instruction recomputationInstr;
        Tree recomputationTree, lhs, rhs;
        TapList<Instruction> recompPrimals = recomputations.first ;
        while (recompPrimals != null) {
            recomputationInstr = recompPrimals.head;
            recomputationTree = recomputationInstr.tree;
            // recomputationTree can only be an op_assign.
            lhs = recomputationTree.down(1) ;
            rhs = recomputationTree.down(2) ;
            addFuturePlainNodeBwd(ILUtils.copy(recomputationTree), recomputationInstr.whereMask(), false,
                    ILUtils.usedVarsInExp(recomputationTree, null, false),
                    new TapList<>(lhs, null), null, null, false, false,
                    "Recompute forward", false);
            recompPrimals = recompPrimals.tail;
        }
        TapList<Instruction> recompDiffPtrs = recomputations.second ;
        while (recompDiffPtrs != null) {
            recomputationInstr = recompDiffPtrs.head;
            recomputationTree = recomputationInstr.tree;
            // recomputationTree can only be an op_assign.
            lhs = recomputationTree.down(1) ;
            rhs = recomputationTree.down(2) ;
            WrapperTypeSpec type = adEnv.curSymbolTable().typeOf(lhs);
            boolean isActivePointer =
                TypeSpec.isA(type, SymbolTableConstants.POINTERTYPE)
                // ? remove following test and/or move it into ADTBRAnalyzer.placeRecomputationHere() ?
                && pointerIsActiveHere(lhs, beforeActiv, afterActiv, beforeAvlX, afterReqX);
            if (isActivePointer) {
                Tree diffPointerRecomputationTree =
                    ILUtils.build(ILLang.op_assign,
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhs, adEnv.curBwdSymbolTable,
                                                          false, lhs, false, true, null),
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), rhs, adEnv.curBwdSymbolTable,
                                                          false, rhs, false, true, null)) ;
                addFuturePlainNodeBwd(diffPointerRecomputationTree, recomputationInstr.whereMask(), false,
                                      ILUtils.usedVarsInExp(recomputationTree, null, false), null,
                                      new TapList<>(rhs, null), new TapList<>(lhs, null), false, false,
                                      "Recompute diff pointer forward", false) ;
            }
            recompDiffPtrs = recompDiffPtrs.tail ;
        }
    }

    /**
     * Build the derivative instructions that come from a standard assignment (i.e. NOT x = F(y), which is a call)
     * The assignment can also be a +=, *=, and the like...
     * When diffDecl is given non-null, meaning that the assignment is the initialization in a declaration,
     * diffDecl is the differentiated declaration, and this builds the differentiated declaration.
     * This builds the list that must go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order)
     */
    private void buildDiffInstructionsOfPlainAssignment(Tree assignment, Tree srcDecl, Tree[] diffDeclR,
                                                        int differentiationMode,
                                                        boolean assignmentIsLive, boolean assignmentOnDiffPtrIsLive,
                                                        boolean mustSaveTBRBeforeInstr, boolean mustSaveTBROnDiffPtrBeforeInstr,
                                                        BoolVector beforeActiv, BoolVector afterActiv,
                                                        BoolVector beforeReqX, BoolVector afterReqX,
                                                        BoolVector beforeAvlX) {
        if (TapEnv.associationByAddress()) {
            updateAAInstructionMask(adEnv.curInstruction(), adEnv.curSymbolTable(), adEnv.curFwdSymbolTable);
        }
        // TODO: Fuse (and then erase) buildAssignTangentNodes() and buildAssignAdjointNodes()
        // into this buildDiffInstructionsOfPlainAssignment()
        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            buildAssignTangentNodes(assignment, srcDecl, diffDeclR,
                    assignmentIsLive,
                    beforeActiv, afterActiv,
                    beforeReqX, afterReqX, beforeAvlX);
        } else if (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            buildAssignAdjointNodes(assignment, srcDecl, diffDeclR, differentiationMode,
                    assignmentIsLive, assignmentOnDiffPtrIsLive,
                    mustSaveTBRBeforeInstr, mustSaveTBROnDiffPtrBeforeInstr,
                    beforeActiv, afterActiv, beforeAvlX, afterReqX);
        }
    }

    private void buildAssignTangentNodes(Tree assignment, Tree srcDecl, Tree[] diffDeclR,
                                         boolean assignmentIsLive,
                                         BoolVector beforeActiv, BoolVector afterActiv,
                                         BoolVector beforeReqX, BoolVector afterReqX,
                                         BoolVector beforeAvlX) {
        TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
        TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);

        Tree result = assignment.down(1);
        Tree expression = assignment.down(2);
        SymbolTable srcSymbolTable = adEnv.curSymbolTable();

        // First, place some initializations of diff variables, needed in a few special cases:
        if (adEnv.curUnitIsActiveUnit && TapEnv.spareDiffReinitializations()) {
            buildDiffPreInitsForTangent(result, expression, srcDecl,
                                        beforeActiv, afterActiv, beforeReqX, afterReqX, false, null);
        }

        Tree assignedRef = (assignment.opCode() == ILLang.op_unary ? assignment.down(2) : assignment.down(1));
        WrapperTypeSpec lhsTypeSpec = srcSymbolTable.typeOf(assignedRef);
        boolean addNbDirsLoop = false;
        Tree[] diffAssignmentR = new Tree[TapEnv.diffReplica()] ;
        Tree copySrcAssignment = (assignmentIsLive || srcDecl != null ? ILUtils.copy(assignment) : null);
        if (!assignmentIsLive && srcDecl != null && lhsTypeSpec != null) {
            Tree zeroTree = lhsTypeSpec.buildConstantZero();
            if (zeroTree != null) {
                copySrcAssignment.setChild(zeroTree, 2);
            }
        }

        boolean diffDeclIsReady = false;  //"complicated mess"
        if ((TypeSpec.isA(lhsTypeSpec, SymbolTableConstants.POINTERTYPE) ||
             (TypeSpec.isA(lhsTypeSpec, SymbolTableConstants.COMPOSITETYPE)
              // if in multidir mode and F90, then we must go to the "else" case
              // to explode the assign on each field of the record!
              && !(multiDirMode() && adEnv.curUnit().isFortran9x())))
            // Constructor assignments must be treated like a standard assignment:
            && expression != null
            && expression.opCode() != ILLang.op_constructorCall
            && expression.opCode() != ILLang.op_arrayConstructor) {
            // Pointer assignment "P=...;" or "P++;"
            for (adEnv.iReplic=0 ; adEnv.iReplic<diffDeclR.length ; ++adEnv.iReplic) {
                if (diffDeclR[adEnv.iReplic] != null) {
                    // Special case of a declaration with initialization: buildDiffOfControlAssignment() produces a wrong result,
                    // whereas diffDecl already contains the right result. cf "complicated mess". TODO: simplify that mess!
                    diffDeclIsReady = true;
                    refsRW.first = ILUtils.usedVarsInExp(assignment, null, false);
                    diffRefsRW.first = ILUtils.usedVarsInExp(assignment, null, true);
                    diffRefsRW.second = new TapList<>(result, null);
                } else {
                    Tree diffAssignment = null;
                    if (writtenExpressionNeedsDiff(assignedRef, beforeActiv, afterActiv, beforeAvlX, afterReqX)
                        && expression.opCode()!=ILLang.op_ioCall  /*cf set09/v204*/) {
                        // If assignment is a pointer assignment we may need to differentiate it
                        // to set the differentiated pointers :
                        TapPair<Tree, TapList<Tree>[]> diffTreePlus =
                            buildDiffOfControlAssignment(assignment, DiffConstants.TANGENT_MODE, adEnv.curFwdSymbolTable,
                                    beforeActiv, afterActiv, beforeAvlX, afterReqX);
                        diffAssignment = (diffTreePlus == null ? null : diffTreePlus.first) ;
                        // Cleanup if diffAssignment is "X := ()". Should not be necessary, but just in case:
                        if (diffAssignment != null && diffAssignment.opCode() == ILLang.op_assign
                            && ILUtils.isNullOrNone(diffAssignment.down(2))) {
                            diffAssignment = null;
                        }
                        if (diffAssignment != null) {
                            //".second" contains the RWRW info that we should use instead of diffRefsRW!
                            TapList<Tree>[] rwrw = diffTreePlus.second;
                            refsRW.first = rwrw[0];
                            diffRefsRW.first = rwrw[2];
                            diffRefsRW.second = rwrw[3];
                        }
                    }
                    diffAssignmentR[adEnv.iReplic] = diffAssignment ;
                }
            }
            adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        } else {
            // else non-pointer assignment:
            addNbDirsLoop = lhsTypeSpec.acceptsMultiDirDimension();
            TapList<NewBlockGraphNode> toPrecomputes = new TapList<>(null, null);
            TapList<DiffAssignmentNode> toPrecomputesDiff = new TapList<>(null, null);
            if (writtenExpressionNeedsDiff(assignment.down(1), beforeActiv, afterActiv, beforeAvlX, afterReqX)) {
                diffAssignmentR =
                        expressionDifferentiator().tangentDifferentiateAssignedExpression(
                                assignment.down(1), assignment.down(2), toPrecomputes, toPrecomputesDiff,
                                copySrcAssignment, beforeActiv,
                                refsRW, diffRefsRW, adEnv.curFwdSymbolTable);
                // Detect the special case of F90 assignment between structs (rhs can only be a ref-expression),
                // in multidir mode: We must explode the assign on each field of the record, in order
                // to place the vectorial dimension at the deepest end. cf set06/v324 & v325
                // We do this with a call to RefDescriptor.makeAssign()
                if (multiDirMode() && adEnv.curUnit().isFortran9x()
                    && expression.opCode() != ILLang.op_arrayConstructor
                    && !WrapperTypeSpec.canReceiveVectorialDim(lhsTypeSpec, false)) {
                    for (int iReplic=0 ; iReplic<diffAssignmentR.length ; ++iReplic) {
                        RefDescriptor diffLhsRefDescriptor =
                            new RefDescriptor(assignedRef, null,
                                    srcSymbolTable, adEnv.curFwdSymbolTable, null,
                                    ILUtils.baseTree(diffAssignmentR[iReplic].down(1)), true,
                                    varRefDifferentiator().multiDirIterDescriptor,
                                    curDiffUnit());
                        RefDescriptor diffRhsRefDescriptor =
                            new RefDescriptor(assignment.down(2), null,
                                    srcSymbolTable, adEnv.curFwdSymbolTable, null,
                                    ILUtils.baseTree(diffAssignmentR[iReplic].down(2)), true,
                                    varRefDifferentiator().multiDirIterDescriptor,
                                    curDiffUnit());
                        diffLhsRefDescriptor.setCompanionVarDescriptor(diffRhsRefDescriptor);
                        diffLhsRefDescriptor.prepareForAssignOrNormDiff(diffRhsRefDescriptor, curDiffUnit(), null, false);
                        diffLhsRefDescriptor.mayProtectAccesses = false;
                        diffAssignmentR[iReplic] = diffLhsRefDescriptor.makeAssign(diffRhsRefDescriptor);
                    }
                }
            }
            // Insert primal pre-computations memorized in toPrecomputes :
            NewBlockGraphNode futureNode;
            toPrecomputes = TapList.nreverse(toPrecomputes.tail);
            while (toPrecomputes != null) {
                futureNode = toPrecomputes.head;
                // Add operation tmpVar = subSrcExpression :
                TapList<Tree> plainR = ILUtils.usedVarsInExp(futureNode.tree, null, true);
                if (futureNode.whereMask != null) {
                    plainR = ILUtils.usedVarsInExp(futureNode.whereMask.getControlTree(), plainR, true);
                }
                addFuturePlainNodeFwd(futureNode.tree, futureNode.whereMask, false,
                        plainR, new TapList<>(futureNode.tree.down(1), null), null, null, false, false,
                        "Assign split primal expression", false);
                toPrecomputes = toPrecomputes.tail;
            }
            // Insert diff (tangent) pre-computations memorized in toPrecomputesDiff :
            DiffAssignmentNode futureDiffNode ;
            toPrecomputesDiff = toPrecomputesDiff.tail ;
            while (toPrecomputesDiff != null) {
                futureDiffNode = toPrecomputesDiff.head;
                // Add a precompute of a diff sub-expression:
                addFuturePlainNodeFwd(futureDiffNode.diffAssign,
                                  (futureDiffNode.type.isScalar() ? null : adEnv.curInstruction().whereMask()),
                                  futureDiffNode.type.acceptsMultiDirDimension(),
                                  futureDiffNode.primR, futureDiffNode.primW, futureDiffNode.diffR, futureDiffNode.diffW,
                                  false, false, "Assign split diff expression", false);
                toPrecomputesDiff = toPrecomputesDiff.tail;
            }
        }

        // If original instruction was split, remove the "sourcetree" annotation
        // so that code decompilation doesn't reuse the original instruction tree:
        if (copySrcAssignment != null && copySrcAssignment.getAnnotation("hasBeenSplit") == Boolean.TRUE) {
            copySrcAssignment.removeAnnotation("sourcetree");
        }

        // When srcDecl, merge copySrcAssignment into srcDecl:
        if (srcDecl != null && copySrcAssignment != null && copySrcAssignment.opCode() == ILLang.op_assign) {
            // TODO: this connection srcDecl=copySrcAssignment.RHS is done according to the current IL order, which is wrong
            // This must be corrected when the IL order is corrected (assignment at the deepest (identifier) declarator level)
            srcDecl = ILUtils.copy(srcDecl);
            if (srcDecl.down(3).down(1).opCode() != ILLang.op_assign) {
                srcDecl.down(3).setChild(ILUtils.build(ILLang.op_assign, srcDecl.down(3).cutChild(1)), 1);
            }
            srcDecl.down(3).down(1).setChild(copySrcAssignment.cutChild(2), 2);
            copySrcAssignment = null;
        }
        // Emit copy of primal declaration (with initializer): (Note: we emit diff declaration AFTER primal copy declaration!)
        if (srcDecl != null) {
            addFuturePlainNodeFwd(srcDecl, null, false,
                ILUtils.usedVarsInExp(assignment, null, true),
                new TapList<>(assignment.down(1), null), null, null, false, false,
                "Copy of primal declaration with primal initializer", false);
        }

        for (int iReplic=0 ; iReplic<diffAssignmentR.length ; ++iReplic) {
            Tree diffAssignment = diffAssignmentR[iReplic] ;
            Tree diffDecl = diffDeclR[iReplic] ;
            // Temporary? Bizarre case where diffDecl was built with an assignment initialization,
            //  but we have now decided that there must be no initialization (i.e. diffAssignment==null):
            if (diffAssignment == null && !diffDeclIsReady && diffDecl != null && diffDecl.down(3).down(1).opCode() == ILLang.op_assign) {
                diffDecl = ILUtils.copy(diffDecl);
                diffDecl.down(3).setChild(diffDecl.down(3).down(1).cutChild(1), 1);
            }
            // When diffDecl, merge diffAssignment into diffDecl:
            if (diffDecl != null && diffAssignment != null && diffAssignment.opCode() == ILLang.op_assign) {
                // TODO: this connection diffDecl=diffAssignment.RHS is done according to the current IL order, which is wrong
                // This must be corrected when the IL order is corrected (assignment at the deepest (identifier) declarator level)
                diffDecl = ILUtils.copy(diffDecl);
                if (diffDecl.down(3).down(1).opCode() != ILLang.op_assign) {
                    diffDecl.down(3).setChild(ILUtils.build(ILLang.op_assign, diffDecl.down(3).cutChild(1)), 1);
                }
                diffDecl.down(3).down(1).setChild(diffAssignment.cutChild(2), 2);
                NewSymbolHolder diffVarFwdSH = NewSymbolHolder.getNewSymbolHolder(ILUtils.baseTree(diffAssignment.down(1)));
                diffVarFwdSH.setInstruction(new Instruction(diffDecl));
                diffVarFwdSH.declare = false;
                diffAssignment = null;
            }
            // Emit differentiated declaration (with differentiated initializer):
            if (diffDecl != null) {
                addFuturePlainNodeFwd(diffDecl, null, false,
                    refsRW.first, refsRW.second,
                    diffRefsRW.first, diffRefsRW.second, false, false,
                    "Differentiated declaration with differentiated initializer", true);
            }
            // Emit differentiated assignment(s) (if not already merged into differentiated declaration(s)):
            if (diffAssignment != null) {
                addFuturePlainNodeFwd(diffAssignment, adEnv.curInstruction().whereMask(), addNbDirsLoop,
                            refsRW.first, refsRW.second,
                            diffRefsRW.first, diffRefsRW.second, false, false,
                            "Differentiated assignment", true);
            }
        }
        // Emit copy of primal assignment (if not already merged into primal copy declaration):
        if (copySrcAssignment != null) {
            if (TapEnv.associationByAddress()) {
                copySrcAssignment = turnAssociationByAddressPrimal(copySrcAssignment, srcSymbolTable, adEnv.curFwdSymbolTable, null, false);
            }
            addFuturePlainNodeFwd(copySrcAssignment, adEnv.curInstruction().whereMask(), false,
                    ILUtils.usedVarsInExp(copySrcAssignment, null, true),
                    new TapList<>(assignment.down(1), null), null, null, false, false,
                    "Copy of primal assignment" + (TapEnv.associationByAddress() ? " (Assoc-by-Address)" : ""), false);
        }
        adEnv.curInstruction().isDifferentiated = true;
    }

    private void buildAssignAdjointNodes(Tree assignment, Tree srcDecl, Tree[] diffDeclR,
                                         int differentiationMode,
                                         boolean assignmentIsLive, boolean assignmentOnDiffPtrIsLive,
                                         boolean mustSaveTBRBeforeInstr, boolean mustSaveTBROnDiffPtrBeforeInstr,
                                         BoolVector beforeActiv, BoolVector afterActiv,
                                         BoolVector beforeAvlX, BoolVector afterReqX) {
        // temporary instruction before -diffReplica done. TODO: replicate if -diffReplica
        Tree diffDecl = (diffDeclR==null||diffDeclR.length<1 ? null : diffDeclR[0]) ;
        Tree lhs = assignment.down(1);
        Tree expression = assignment.down(2);
        SymbolTable srcSymbolTable = adEnv.curSymbolTable();
        boolean separateBwdScope = separatedFwdBdwScopes(differentiationMode) ;
        // When the assigned var is a complex declarator,
        // peel off all type decoration around the declared identifier:
        // This is ugly and results from wrong tree for assignment in declarator
        boolean removedDeclarator = true;
        while (removedDeclarator) {
            switch (lhs.opCode()) {
                case ILLang.op_pointerDeclarator:
                case ILLang.op_functionDeclarator:
                case ILLang.op_arrayDeclarator:
                case ILLang.op_bitfieldDeclarator:
                    lhs = lhs.down(1);
                    removedDeclarator = true;
                    break;
                case ILLang.op_modifiedDeclarator:
                    lhs = lhs.down(2);
                    removedDeclarator = true;
                    break;
                default:
                    removedDeclarator = false;
                    break;
            }
        }

        Tree diffLhs = null;
        Tree diffRhs = null;
        Tree diffDeclAndAssign = null;
        WrapperTypeSpec assignedType = srcSymbolTable.typeOf(lhs);
        boolean somePointerIsAssigned = TypeSpec.isA(assignedType, SymbolTableConstants.POINTERTYPE) ; //was assignedType != null assignedType.containsAPointer();
        boolean mustDiffOperation = pointerIsActiveHere(lhs, beforeActiv, afterActiv, beforeAvlX, afterReqX);
        SymbolTable symbolTableForLhs = adEnv.curFwdSymbolTable;
        if (assignment.getAnnotation("ComesFromAReturn") == Boolean.TRUE) {
            symbolTableForLhs = curFwdDiffUnit().publicSymbolTable();
        }
        if (diffDecl != null || (somePointerIsAssigned && mustDiffOperation)) {
            diffLhs = varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhs, symbolTableForLhs,
                    false, lhs, false, true, null);
        }
        boolean isAllocatablePointer = srcSymbolTable.varHasModifier(lhs, "allocatable") ;
        boolean restorePrimal =
            ADTBRAnalyzer.isTBR(adEnv.curActivity(), lhs) && mustSaveTBRBeforeInstr;
        boolean restoreDiff =
            somePointerIsAssigned && mustDiffOperation
            && !adEnv.curUnitIsContext && ADTBRAnalyzer.isTBROnDiffPtr(adEnv.curActivity(), lhs) && mustSaveTBROnDiffPtrBeforeInstr ;
        if ((restorePrimal || restoreDiff) && isAllocatablePointer) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, lhs, "(AD07) Data-Flow recovery (TBR) needs to restore allocatable pointer " + ILUtils.toString(lhs) + ", not implemented yet");
        }
        RefDescriptor refDescriptor = null;
        if (restorePrimal || restoreDiff) {
            refDescriptor = new RefDescriptor(lhs, null,
                    srcSymbolTable, curDiffUnit().privateSymbolTable(), null,
                    null, false, null, curDiffUnit());
            prepareRestoreOperations(refDescriptor, null, 0, adEnv.curFwdSymbolTable, adEnv.curBwdSymbolTable);
        }

        if (somePointerIsAssigned && refDescriptor != null) {
            boolean rebasePrimal = restorePrimal &&
                    DataFlowAnalyzer.mayPointToRelocated(lhs, false, adEnv.curInstruction(),
                            srcSymbolTable, differentiationMode == DiffConstants.ADJOINT_MODE);
            boolean rebaseDiff = restoreDiff &&
                    DataFlowAnalyzer.mayPointToRelocated(lhs, true, adEnv.curInstruction(),
                            srcSymbolTable, differentiationMode == DiffConstants.ADJOINT_MODE);
            if (rebasePrimal || rebaseDiff) {
                ++numRebase;
                //[llh 01Feb22] Note: rebase mechanism is not designed to work with diffReplica>1
                Tree rebaseTree = refDescriptor.makeRebase(rebasePrimal ? null : new TapList<>(Boolean.TRUE, null),
                        rebaseDiff ? null : new TapList<>(Boolean.TRUE, null),
                        rebaseDiff ? ILUtils.baseTree(diffLhs) : null,
                        (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? numRebase : -1);
                if (rebaseTree != null) {
                    adEnv.usesADMM = true;
                    addFuturePlainNodeBwd(rebaseTree, null, false,
                            ILUtils.usedVarsInExp(lhs, null, true),
                            rebasePrimal ? new TapList<>(lhs, null) : null, null,
                            rebaseDiff ? new TapList<>(lhs, null) : null, false, false,
                            "Rebase TBR previous value of assigned pointer " + lhs + " and/or DIFF", false);
                }
            }
        }

        if (somePointerIsAssigned && mustDiffOperation && (assignmentOnDiffPtrIsLive || srcDecl != null)) {
            if (restoreDiff) {
                // [llh] TODO: force preserving the order of this inside the fwd sweep (uses PUSH/POP stack!)
                RefDescriptor refDescriptorDiff = new RefDescriptor(lhs, null,
                        srcSymbolTable, curDiffUnit().privateSymbolTable(), null,
                        ILUtils.baseTree(diffLhs), true, null, curDiffUnit());
                refDescriptorDiff.prepareForStack(curFwdDiffUnit(), 0/*ppLabel*/, null, true);
                refDescriptorDiff.mayProtectAccesses = false;
                // TODO: replicate if -diffReplica
                addFuturePlainNodeFwd(refDescriptorDiff.makePush(), null, false,
                        ILUtils.usedVarsInExp(lhs, null, false),
                        null, new TapList<>(lhs, null), null, false, true,
                        "Store diff control " + ILUtils.toString(lhs, adEnv.curActivity()), false);

                refDescriptorDiff.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                // TODO: replicate if -diffReplica
                addFuturePlainNodeBwd(refDescriptorDiff.makePop(), null, false,
                        ILUtils.usedVarsInExp(lhs, null, false),
                        null, null, new TapList<>(lhs, null), false, true,
                        "Restore diff control " + ILUtils.toString(lhs, adEnv.curActivity()), false);
            }
            if (ILUtils.isCallingString(expression, "null", false)) {
                // Special case of Fortran90 instruction "p => NULL()"
                diffRhs = ILUtils.copy(expression);
            } else {
                diffRhs = varRefDifferentiator().diffVarRef(adEnv.curActivity(), expression, adEnv.curFwdSymbolTable,
                        false, expression, false, true, null);
            }

            if (diffDecl != null) {
                // TODO: this connection diffDecl=diffRhs is done according to the current IL order, which is wrong
                // This must be corrected when the IL order is corrected (assignment at the deepest (identifier) declarator level)
                diffDeclAndAssign = ILUtils.copy(diffDecl);
                if (diffRhs != null) {
                    if (diffDeclAndAssign.down(3).down(1).opCode() != ILLang.op_assign) {
                        diffDeclAndAssign.down(3).setChild(ILUtils.build(ILLang.op_assign, diffDeclAndAssign.down(3).cutChild(1)), 1);
                    }
                    diffDeclAndAssign.down(3).down(1).setChild(diffRhs, 2);
                }
                // We don't emit diffDeclAndAssign now. We defer that to AFTER the primal assignment (because it is in a declaration)
            } else if (diffRhs != null) {
                Tree diffAssign = ILUtils.build(assignment.opCode(), diffLhs, diffRhs);
                // TODO: replicate if -diffReplica
                addFuturePlainNodeFwd(diffAssign, adEnv.curInstruction().whereMask(), false,
                        ILUtils.usedVarsInExp(lhs, null, false), null,
                        new TapList<>(expression, null), new TapList<>(lhs, null), false, false,
                        "Diff pointer assignment", true);
            }
        } else if (diffDecl != null) {
            diffDeclAndAssign = ILUtils.copy(diffDecl);
        }
        if (somePointerIsAssigned && diffDecl != null) {
            // Say that the new differentiated variable has a declaration ready in the forward sweep:
            NewSymbolHolder diffVarFwdSH = NewSymbolHolder.getNewSymbolHolder(ILUtils.baseTree(diffLhs));
            diffVarFwdSH.setInstruction(new Instruction(diffDeclAndAssign != null ? diffDeclAndAssign : diffDecl));
            diffVarFwdSH.declare = false;
        }

        TapList writtenZonesTree =
                srcSymbolTable.treeOfZonesOfValue(lhs, null, adEnv.curInstruction(), null);
        // Don't includePointedElementsInTree() here in writtenZonesTree: we reinitialize
        //  the diffs only of the really assigned values, not their destinations !
        TapIntList writtenZones = ZoneInfo.listAllZones(writtenZonesTree, true);
        boolean writtenTmp = TapIntList.intersects(writtenZones, adEnv.toActiveTmpZones.tail);
        TapIntList writtenDiffKindVectorIndices =
                DataFlowAnalyzer.mapZoneRkToKindZoneRk(writtenZones, adEnv.diffKind, srcSymbolTable);
        TapList<ZoneInfo> zones = DataFlowAnalyzer.mapZoneRkToZoneInfo(writtenZones, srcSymbolTable);
        TapIntList argZones, argDiffKindVectorIndices;
        boolean argTmp;

        if (writtenExpressionNeedsDiff(lhs, beforeActiv, afterActiv, beforeAvlX, afterReqX)) {
            // TODO: refactor the above writtenExpressionNeedsDiff() and the two tests below "needSetDiffLhs" and "needDiffRhs"
            //Don't differentiate if we are really out of the to-be-differentiated part:
            boolean needSetDiffLhs =
                    (!adEnv.curActivity().isContext() || adEnv.activeRootCalledFromContext)
                            &&
                            // Differentiate if written zones are active:
                            ((beforeActiv != null && beforeActiv.intersects(writtenDiffKindVectorIndices))
                                    ||
                                    // ... or differentiate if written zones are of differentiable type:
                                    (writtenTmp || writtenDiffKindVectorIndices != null)
                                            &&
                                            // ... and we are in some special "force-differentiate" mode:
                                            (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
                                                    ||
                                                    adEnv.activeRootCalledFromContext
                                                    ||
                                                    !TapEnv.spareDiffReinitializations()
                                                            // In explicit-zero mode, we reset diff of the written variable
                                                            // only if this variable is differentiated at some point:
                                                            && ADActivityAnalyzer.haveDifferentiatedVariable(zones)));
            boolean needDiffRhs =
                    (ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), lhs, srcSymbolTable)
                            ||
                            adEnv.activeRootCalledFromContext)
                            &&
                            // do not emit adjoint-style assignments for active pointer assignments :
                            (writtenTmp || writtenDiffKindVectorIndices != null);
            TapList<NewBlockGraphNode> toPrecomputes = new TapList<>(null, null);
            TapList<DiffAssignmentNode>[] diffAssignmentNodesR =
                    expressionDifferentiator().adjointDifferentiateAssignedExpression(lhs, needSetDiffLhs, expression, needDiffRhs,
                            toPrecomputes, beforeActiv, adEnv.curBwdSymbolTable);
            TapList<DiffAssignmentNode> diffAssignmentNodes ;
            DiffAssignmentNode diffAssignmentNode;
            for (int iReplic=0 ; iReplic<diffAssignmentNodesR.length ; ++iReplic) {
                diffAssignmentNodes = diffAssignmentNodesR[iReplic] ;
                while (diffAssignmentNodes != null) {
                    diffAssignmentNode = diffAssignmentNodes.head;
                    if (diffAssignmentNode.action == DiffConstants.INCREMENT_VARIABLE) {
                        ToBool zonesReadTotally = new ToBool(false);
                        argZones = ZoneInfo.listAllZones(
                            srcSymbolTable.treeOfZonesOfValue(diffAssignmentNode.primRecv,
                                    zonesReadTotally, adEnv.curInstruction(), null),
                            true);
                        zones = DataFlowAnalyzer.mapZoneRkToZoneInfo(argZones, srcSymbolTable);
                        argTmp = TapIntList.intersects(argZones, adEnv.toActiveTmpZones.tail);
                        argDiffKindVectorIndices =
                            DataFlowAnalyzer.mapZoneRkToKindZoneRk(argZones, adEnv.diffKind, srcSymbolTable);
                        if (// IF we are not in special debug cases ...
                            !((argTmp || argDiffKindVectorIndices != null) &&
                              (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || !TapEnv.spareDiffReinitializations()
                               && ADActivityAnalyzer.haveDifferentiatedVariable(zones)))
                            // ... and the rhs var is not active downstream ...
                            && (afterActiv == null || !afterActiv.intersects(argDiffKindVectorIndices))
                            // ... and this is the last (i.e. first in the bwdBlock) assignment to the diff rhs var
                            //   for this source instruction (detected by looking in the rest of diffAssignmentNodes)
                            && !intersectsLhs(diffAssignmentNodes.tail, diffAssignmentNode.primRecv,
                                              argZones, adEnv.curInstruction(), srcSymbolTable)
                            // ... and the access is total (tmp vars are always accessed totally).
                            && (argTmp || zonesReadTotally.get())) {
                                // THEN TURN THIS INCREMENT INTO A SET_VARIABLE: rhs_var_bar:=diffValue
                                diffAssignmentNode.action = DiffConstants.SET_VARIABLE;
                            }
                    }
                    if (diffAssignmentNode.action == DiffConstants.INCREMENT_VARIABLE) {
                        // Generate nothing for no-op Xb+=0 :
                        if (!ILUtils.isNullOrNone(diffAssignmentNode.diffValue) && !ILUtils.isZero(diffAssignmentNode.diffValue)) {
                            addFutureIncrDiffNodeBwd(diffAssignmentNode, "Increment diff from rhs");
                        }
                    } else { // SET_VARIABLE:
                        // Generate nothing for no-op Xb:=Xb :
                        if (diffAssignmentNode.diffValue == null ||
                            1 != ILUtils.eqOrDisjointRef(diffAssignmentNode.diffRecv, diffAssignmentNode.diffValue,
                                                         adEnv.curInstruction(), adEnv.curInstruction())) {
                            addFutureSetDiffNodeBwd(diffAssignmentNode, "Set diff (of lhs?)");
                        }
                    }
                    diffAssignmentNodes = diffAssignmentNodes.tail;
                }
            }
            // insert pre-computations of primal values memorized in toPrecomputes :
            NewBlockGraphNode futureNode;
            toPrecomputes = toPrecomputes.tail;
            while (toPrecomputes != null) {
                futureNode = toPrecomputes.head;
                // Add operation tmpVar = subSrcExpression :
                TapList<Tree> plainR = ILUtils.usedVarsInExp(futureNode.tree, null, true);
                if (futureNode.whereMask != null) {
                    plainR = ILUtils.usedVarsInExp(futureNode.whereMask.getControlTree(), plainR, true);
                }
                addFuturePlainNodeBwd(futureNode.tree, futureNode.whereMask, false,
                        plainR, new TapList<>(futureNode.tree.down(1), null), null, null, false, false,
                        "Assign split primal expression", false);
                toPrecomputes = toPrecomputes.tail;
            }
        }

        if (restorePrimal) {
            // Add operation: save the overwritten value (because it is TBR):
            addFuturePlainNodeFwd(refDescriptor.makePush(), null, false,
                    ILUtils.usedVarsInExp(lhs, null, true), null, null, null, false, true,
                    "Store TBR value before assign", false);

            // Add operation: restore (e.g. POP) previous value of lhs :
            refDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
            addFuturePlainNodeBwd(refDescriptor.makePop(), null, false,
                    ILUtils.usedVarsInExp(lhs, null, false),
                    new TapList<>(lhs, null), null, null, false, true,
                    "Restore TBR value before adj assign", false);
        }

        if (!adEnv.activeRootCalledFromContext && (assignmentIsLive || srcDecl != null)) {
            // ^ In the special case of the context calling a diff-root, don't copy the split primal return value assignment.
            Tree copySrcAssignment = ILUtils.copy(assignment);
            if (!assignmentIsLive && srcDecl != null && assignedType != null) {
                Tree zeroTree = assignedType.buildConstantZero();
                if (zeroTree != null) {
                    copySrcAssignment.setChild(zeroTree, 2);
                }
            }
            // If original instruction was split, remove the "sourcetree" annotation
            // so that code decompilation doesn't reuse the original instruction tree:
            if (copySrcAssignment.getAnnotation("hasBeenSplit") == Boolean.TRUE) {
                copySrcAssignment.removeAnnotation("sourcetree");
            }
            // When srcDecl, merge copySrcAssignment into srcDecl:
            if (srcDecl != null && copySrcAssignment.opCode() == ILLang.op_assign) {
                // TODO: this connection srcDecl=copySrcAssignment.Rhs is done according to the current IL order, which is wrong.
                // This must be corrected when the IL order is corrected (assignment at the deepest (identifier) declarator level)
                Tree srcDeclForFwd = ILUtils.copy(srcDecl);
                if (srcDeclForFwd.down(3).down(1).opCode() != ILLang.op_assign) {
                    srcDeclForFwd.down(3).setChild(ILUtils.build(ILLang.op_assign, srcDeclForFwd.down(3).cutChild(1)), 1);
                }
                srcDeclForFwd.down(3).down(1).setChild(copySrcAssignment.cutChild(2), 2);
                copySrcAssignment = srcDeclForFwd;
            }
            // If this assigns through a pointer to the orig lhs in the SPLIT ADJOINT_FWD in C
            // (because the returned value is used for the diffRhs), then insert a pointerAccess on the assignment's lhs:
            if (diffRhs != null
                    && (adEnv.curUnitIsContext || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE)
                    && curFwdDiffUnit().isC()
                    && assignment.getAnnotation("ComesFromAReturn") == Boolean.TRUE) {
                copySrcAssignment.setChild(ILUtils.build(ILLang.op_pointerAccess, copySrcAssignment.cutChild(1)), 1);
            }
            if (TapEnv.associationByAddress()) {
                copySrcAssignment = turnAssociationByAddressPrimal(copySrcAssignment, srcSymbolTable, adEnv.curFwdSymbolTable, null, false);
            }
            addFuturePlainNodeFwd(copySrcAssignment, adEnv.curInstruction().whereMask(), false,
                    ILUtils.usedVarsInExp(copySrcAssignment, null, false),
                    new TapList<>(assignment.down(1), null), null, null, false, false,
                    "Copy of primal assignment", false);
        }

        if (separateBwdScope) {
            // Primal and diff declaration must go to a different place,
            // which is the declaration Block of the BWD sweep:
            if (srcDecl != null) {
                Tree splitBwdDecl = ILUtils.copy(srcDecl);
                // replace "Type Var=Exp" by "Type Var":
                if (splitBwdDecl.opCode() == ILLang.op_varDeclaration && splitBwdDecl.down(3).down(1).opCode() == ILLang.op_assign) {
                    splitBwdDecl.down(3).setChild(splitBwdDecl.down(3).down(1).cutChild(1), 1);
                }
                addFutureBwdSplitDecl(splitBwdDecl, "Copy of primal declaration for SPLIT BWD");
            }

            if (diffDecl != null) {
                // TODO: replicate if -diffReplica
                addFutureBwdSplitDecl(ILUtils.copy(diffDecl), "Differentiated declaration");
            }
        }

        if (!separateBwdScope || somePointerIsAssigned) {
            if (diffDeclAndAssign != null) {
                // This happens if the diff control assignment is a declaration initializer, in which case we
                // have deferred its emission to AFTER the primal declaration (with initializer), which is NOW!
                // TODO: replicate if -diffReplica
                addFuturePlainNodeFwd(diffDeclAndAssign, null, false,
                        ILUtils.usedVarsInExp(lhs, null, false),
                        null, new TapList<>(expression, null), new TapList<>(lhs, null), false, false,
                        "Diff assignment inside diff declarator", true);
            }
        }

        // TODO verifier ce test, rajouter isOnceActive?
        if (adEnv.curUnitIsActiveUnit && (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.spareDiffReinitializations())) {
            // Insert reinitializations of adjoints of ARRAYS whose activity status
            //  is switching from "1" upstream to "0" downstream. cf nonRegrF77:lh74 :
            //  (dual case in tangent: nonRegrF90:lh77)
            // TODO: This reinitialization is superfluous for arrays read "totally" (whatever it means...)
            TapIntList zonesReadTotally = null;
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryReInits = new TapList<>(null, null);
            checkArrayActivitySwitches(beforeActiv, afterActiv, zonesReadTotally,
                    toNecessaryReInits, srcSymbolTable, adEnv.curBwdSymbolTable, false, null);
            toNecessaryReInits = toNecessaryReInits.tail;
            Tree diffInitTree;
            TapList<Tree> diffW;
            while (toNecessaryReInits != null) {
                diffInitTree = toNecessaryReInits.head.second;
                diffW = toNecessaryReInits.head.first;
                addFuturePlainNodeBwd(diffInitTree, null, false,
                        null, null, null, diffW, false, false,
                        "Needed array diff init before adj assign (because array activity switches)", false);
                toNecessaryReInits = toNecessaryReInits.tail;
            }
            //[llh] TODO: Insert explicit diff initialization of implicit-diff-zero passive vars
            // when the address of the passive var is given to a pointer which is ReqExplicit:
            // (cf nonRegrF90:v58)
        }
    }

    /**
     * @return true when there is at least one assigned ref in diffAssignmentNodes
     * that at least partly intersects the memory zones of the given "tree" (cf set07/v471).
     */
    private boolean intersectsLhs(TapList<DiffAssignmentNode> diffAssignmentNodes, Tree tree, TapIntList treeZones,
                                  Instruction instr, SymbolTable symbolTable) {
        boolean intersects = false;
        TapIntList keyZones;
        Tree key;
        while (diffAssignmentNodes != null && !intersects) {
            key = diffAssignmentNodes.head.primRecv;
            keyZones = ZoneInfo.listAllZones(symbolTable.treeOfZonesOfValue(key, null, instr, null), true);
            intersects = TapIntList.intersects(treeZones, keyZones) &&
                    -1 != ILUtils.eqOrDisjointRef(tree, key, instr, instr);
            diffAssignmentNodes = diffAssignmentNodes.tail;
        }
        return intersects;
    }

    /** Returns true if "expression", which is written by the current primal instruction,
     * is active and will have a derivative for this instruction */
    private boolean writtenExpressionNeedsDiff(Tree expression,
                                               BoolVector beforeActiv, BoolVector afterActiv,
                                               BoolVector beforeAvlX, BoolVector afterReqX) {
        // Return false when the assignment is overloaded and assigns primal and tangent together:
        if (TapEnv.get().complexStep) return false ;
        // TODO: This test must be refactored!
        SymbolTable symbolTable = adEnv.curSymbolTable();
        TapList accessibleZoneTree =
                symbolTable.treeOfZonesOfValue(expression, new ToBool(false), adEnv.curInstruction(), null);
        if (accessibleZoneTree != null) {
            accessibleZoneTree = TapList.copyTree(accessibleZoneTree);
            DataFlowAnalyzer.includePointedElementsInTree(accessibleZoneTree, null, null, true,
                    symbolTable, adEnv.curInstruction(), false, false);
        }
        TapIntList accessibleZones = ZoneInfo.listAllZones(accessibleZoneTree, true);
        boolean accessibleTmp = TapIntList.intersects(accessibleZones, adEnv.toActiveTmpZones.tail);
        TapIntList accessibleVectorIndices = accessibleZones ;
        TapIntList accessibleDiffKindVectorIndices =
                DataFlowAnalyzer.mapZoneRkToKindZoneRk(accessibleZones, adEnv.diffKind, symbolTable);
        TapList<ZoneInfo> zones = DataFlowAnalyzer.mapZoneRkToZoneInfo(accessibleZones, symbolTable);
        boolean isAnnotatedActive = ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), expression, symbolTable);
        BoolVector contextDiffRequired = symbolTable.getRequiredDiffVars(adEnv.curActivity());
        boolean usesPassiveFields = usesPassiveFields(expression, symbolTable);
        WrapperTypeSpec expressionType = symbolTable.typeOf(expression);

// String varnamefordebug = ILUtils.baseName(expression);
// boolean printTraces = "vvv".equals(varnamefordebug) || "ooo".equals(varnamefordebug) ;
// if (printTraces) {
// TapEnv.printlnOnTrace(" WRITTEN EXPR NEEDS DIFF? "+expression+" isAnnotatedActive:"+isAnnotatedActive+" accessibleVectorIndices:"+accessibleVectorIndices+" and diffkind:"+accessibleDiffKindVectorIndices+" accessibleTmp:"+accessibleTmp) ;
// TapEnv.printlnOnTrace("     beforeActiv:"+beforeActiv+"  afterActiv:"+afterActiv) ;
// TapEnv.printlnOnTrace("     afterReqX:"+afterReqX+"  beforeAvlX:"+beforeAvlX+" contextDiffRequired:"+contextDiffRequired) ;
// TapEnv.printlnOnTrace("  Expression type:"+expressionType) ;
// TapEnv.printlnOnTrace("  zones:"+zones+" haveDifferentiatedVariable:"+ADActivityAnalyzer.haveDifferentiatedVariable(zones)+" curUnit():"+adEnv.curUnit()+" TapEnv.mustContext():"+TapEnv.mustContext()+" usesPassiveFields:"+usesPassiveFields);
// }

        // Commented out because we want to init the diff target when uninitialized (cf F90:cm02 vs v339;lh90):
        // Unfortunately, it gives a less nice result on C:lh19,lh42,v251  cf NOTE_init_diff
        //                            && (beforeActiv.intersects(accessibleDiffKindVectorIndices)
        //                               || afterActiv.intersects(accessibleDiffKindVectorIndices))
        //                     // We already know we are not active after (cf beginning of test);
        //                     // We must now test that we are not at the same time passive before:
        //                     && beforeActiv.intersects(accessibleDiffKindVectorIndices)
        // Also when we have the "-context" option to create a call context for the diff code,
        // and we are in such a non-active, context Unit, we must differentiate
        // the allocate and deallocate and pointer assignments:
        boolean expressionNeedsDiff =
            (!usesPassiveFields  // <- It is absolutely forbidden to differentiate a ref that uses a field which
                                 // is not active. This test should be useless. It is here only because of bugs!
             &&
             (// If activity analysis was deactivated, every adEnv.diffKind is active :
              (!TapEnv.doActivity() && (accessibleTmp || accessibleDiffKindVectorIndices != null))
              ||
              isAnnotatedActive
              ||
              // "PointerActiveUsage":
              ((afterReqX != null && (accessibleTmp || afterReqX.intersects(accessibleVectorIndices))
                ||
                beforeAvlX != null && (accessibleTmp || beforeAvlX.intersects(accessibleVectorIndices))
                ||
                TapEnv.mustContext() && beforeActiv == null && afterActiv == null
                &&
                (contextDiffRequired != null && contextDiffRequired.intersects(accessibleDiffKindVectorIndices)
                 ||
                 // Also, still in the "-context" case outside of the diff Call Tree, we must differentiate
                 // the allocate, deallocate, and pointer assignments of pointers that are onceActive:
                 ADActivityAnalyzer.haveDifferentiatedVariable(zones)
                 )
                )
               &&
               (expressionType.containsAPointer()
                || expression.opCode() == ILLang.op_allocate
                || expression.opCode() == ILLang.op_deallocate)
               )
              ||
              // or (cf F90:cm21) the written var is not active, but it is a non-pointer
              // that has been set to ReqX because some active pointer is going to take its address:
              (afterReqX != null
               && accessibleDiffKindVectorIndices != null
               && (accessibleTmp || afterReqX.intersects(accessibleVectorIndices))
               && !TypeSpec.isA(expressionType, SymbolTableConstants.POINTERTYPE))
              ||
              //Additional cases where the written variable,
              // although not annotated as active, must be differentiated:
              // it must write into some real, the enclosing unit must be active (i.e. activity
              // analysis has been run on it, giving the beforeActiv and afterActiv),
              // the variable must be active at some moment somewhere and
              // (either we are in the nooptim mode that explicits all diff assignments, OR the
              // variable uses a pointer and this assignment doesn't go from passive to passive)
              (accessibleDiffKindVectorIndices != null && beforeActiv != null && afterActiv != null
               && (adEnv.activeRootCalledFromContext || ADActivityAnalyzer.haveDifferentiatedVariable(zones))
               && ((adEnv.activeRootCalledFromContext
                    && (beforeActiv.intersects(accessibleDiffKindVectorIndices)
                        || afterActiv.intersects(accessibleDiffKindVectorIndices)))
                   ||
                   !TapEnv.spareDiffReinitializations()
                   ||
                   ILUtils.isAccessedThroughPointer(expression)
                   )
               )
              )
             ) ;
// if (printTraces) TapEnv.printlnOnTrace(expressionNeedsDiff?" YES!":" NO!") ;
        return expressionNeedsDiff;
    }

    /**
     * @param onlyOnLocals flag (not clearly justified) that restricts initialization
     *  to local arrays and possibly to the function result when it is an array.
     *  Currently, it is used to reduce the number of redundant diff arrays initialization
     *  in tangent mode, that are requested by buildDiffPreInitsForTangent(). 
     * @param toInitialized if given non-null, will contain upon exit the list of all ZoneInfo
     *  for which the diff has been initialized.
     */
    protected void buildDiffPreInitsForTangent(Tree lhs, Tree rhs, Tree srcDecl,
                                               BoolVector beforeActiv, BoolVector afterActiv,
                                               BoolVector beforeReqX, BoolVector afterReqX,
                                               boolean onlyOnLocals, TapList<ZoneInfo> toInitialized) {
        // Insert reinitializations of derivative of ARRAYS whose activity status
        //  is switching from "0" upstream to "1" downstream. cf nonRegrF90:lh77 :
        //  (dual case in reverse: nonRegrF77:lh74)
        // This reinitialization is superfluous for arrays overwritten totally:
        Tree[] initTreeR;
        Tree initTree ;
        SymbolTable srcSymbolTable = adEnv.curSymbolTable();
        if (srcDecl == null) {//Don't initialize diff array BEFORE it is ever declared!
            ToBool total = new ToBool(false);
            TapIntList zonesWrittenTotally =
                    srcSymbolTable.listOfZonesOfValue(lhs, total, adEnv.curInstruction());
            if (!total.get() || adEnv.curInstruction().getWhereMask() != null) {
                zonesWrittenTotally = null;
            }
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsUS = new TapList<>(null, null);
            checkArrayActivitySwitches(afterActiv, beforeActiv, zonesWrittenTotally,
                    toNecessaryInitsUS, srcSymbolTable, adEnv.curFwdSymbolTable, onlyOnLocals, toInitialized);
            toNecessaryInitsUS = toNecessaryInitsUS.tail;
            while (toNecessaryInitsUS != null) {
                initTree = toNecessaryInitsUS.head.second;
                TapList<Tree> diffW = toNecessaryInitsUS.head.first;
                addFuturePlainNodeFwd(initTree, null, false,
                        // TODO: the following refsR with "initTree" is bad: initTree is diff code, not primal code!
                        ILUtils.usedVarsInExp(initTree, null, false), null, null, diffW, false, false,
                        "Needed array diff init (because array activity switches)", false);
                toNecessaryInitsUS = toNecessaryInitsUS.tail;
            }
        }
        //Insert explicit diff initialization of implicit-diff-zero passive vars
        // when the address of the passive var is given to a pointer which is ReqExplicit:
        // (cf nonRegrF90:v56, v57, and v58)
        if (TypeSpec.isA(srcSymbolTable.typeOf(lhs), SymbolTableConstants.POINTERTYPE)) {
            TapIntList writtenIndices =
                    ZoneInfo.listAllZones(
                            srcSymbolTable.treeOfZonesOfValue(lhs, null, adEnv.curInstruction(), null), true);
            // if the receiving pointer is ReqExplicit ("PointerActiveUsage"):
            if (afterReqX != null && afterReqX.intersects(writtenIndices)) {
                TapList<ZoneInfo> zonesToCheck =
                        srcSymbolTable.collectAddressTakenZones(rhs, adEnv.curInstruction(), null);
                while (zonesToCheck != null) {
                    ZoneInfo zoneToCheck = zonesToCheck.head;
                    int rankInActive = zoneToCheck.kindZoneNb(adEnv.diffKind);
                    int rankInReqX = zoneToCheck.kindZoneNb(SymbolTableConstants.ALLKIND);
                    // and if the given address is implicit-diff-zero passive ("PointerActiveUsage"):
                    if (rankInActive > 0
                            && beforeActiv != null
                            && beforeReqX != null
                            && !beforeActiv.get(rankInActive)
                            && !beforeReqX.get(rankInReqX)) {
                        initTreeR =
                                makeDiffInitialization(zoneToCheck.accessTree, zoneToCheck, srcSymbolTable, adEnv.curFwdSymbolTable,
                                        null, true, null, true);
                        for (int iReplic=0 ; iReplic<initTreeR.length ; ++iReplic) {
                            initTree = initTreeR[iReplic] ;
                            if (initTree != null) {
                                addFuturePlainNodeFwd(initTree, null, false,
                                    ILUtils.usedVarsInExp(lhs,
                                            ILUtils.usedVarsInExp(rhs, null, false), false),
                                    null, null, new TapList<>(zoneToCheck.accessTree, null), false, false,
                                    "Needed pointed diff pre-init", false);
                            }
                        }
                    }
                    zonesToCheck = zonesToCheck.tail;
                }
            }
        }
    }

    /** Insert into place the derivative instructions for a plain declaration statement "srcTree",
     * i.e. a declaration without any initialization. These derivative instructions must have
     * been built previously by a call to differentiateInstructionDeclaration().<br>
     * -- Derivatives passed in diffDeclarationsR are inserted into the future backward sweep.<br>
     * -- Derivatives passed in diffDeclarationsForFwd are inserted into the declarations block of the SPLIT FWD code.<br>
     * -- Also, the primal declaration srcTree is inserted into the declarations block of the SPLIT FWD code.<br>
     */
    private void buildDiffInstructionsOfPlainDeclaration(Tree srcTree,
                                                         TapList<Instruction>[] diffDeclarationsR,
                                                         TapList<Instruction> diffDeclarationsForFwd,
                                                         int differentiationMode, boolean primalIsLive) {
        boolean separateBwdScope = separatedFwdBdwScopes(differentiationMode) ;
        // The following detection of RW trees in srcTree should be improved:
        TapList<Tree> readBySrcDecl = ILUtils.usedVarsInExp(srcTree, null, false);
        TapList<Tree> writtenBySrcDecl = null;
        if (srcTree.opCode() != ILLang.op_include &&
                srcTree.down(3) != null && srcTree.down(3).opCode() == ILLang.op_declarators) {
            Tree[] declarators = srcTree.down(3).children();
            for (int i = declarators.length - 1; i >= 0; --i) {
                Tree oneDeclarator = ILUtils.baseTree(declarators[i]);
                if (oneDeclarator != null && oneDeclarator.opCode() == ILLang.op_ident) {
                    writtenBySrcDecl = new TapList<>(oneDeclarator, writtenBySrcDecl);
                }
            }
        }

        if (primalIsLive && srcTree.opCode() != ILLang.op_useDecl) {
            if (srcTree.getAnnotation("hasBeenSplit") == Boolean.TRUE) {
                srcTree.removeAnnotation("sourcetree");
            }
            boolean addInFuturePlainNodeFwd = true;
            if (srcTree.opCode() == ILLang.op_varDeclaration) {
                Tree[] declarators = srcTree.down(3).children();
                for (int i = declarators.length - 1; i >= 0; --i) {
                    NewSymbolHolder diffVarFwdSH = NewSymbolHolder.getNewSymbolHolder(ILUtils.baseTree(declarators[i]));
                    if (diffVarFwdSH != null) {
                        diffVarFwdSH.setInstruction(new Instruction(srcTree));
                        diffVarFwdSH.declare = false;
                    }
                }
                if (declarators.length == 0 && TapEnv.associationByAddress()) {
                    addInFuturePlainNodeFwd = false;
                }
            }
            if (addInFuturePlainNodeFwd) {
                addFuturePlainNodeFwd(srcTree, null, false,
                        readBySrcDecl, writtenBySrcDecl, null, null, false, false,
                        "Copy of primal plain declaration", false);
            }
        }
        // Now emit the differentiated (plain) declaration:
        for (int iReplic=0 ; iReplic<diffDeclarationsR.length ; ++iReplic) {
            TapList<Instruction> diffDeclarations = diffDeclarationsR[iReplic] ;
            Tree diffDecl = null;
            if (diffDeclarations != null) {
                diffDecl = diffDeclarations.head.tree;
                diffDeclarations = diffDeclarations.tail;
            }
            if (separateBwdScope) {
                // Primal and diff declaration must go to a different place,
                // which is the declaration Block of the BWD sweep:
                if (srcTree.opCode() != ILLang.op_useDecl) {
                    addFutureBwdSplitDecl(ILUtils.copy(srcTree), "Copy of primal plain declaration for SPLIT BWD");
                } else {
                    addFuturePlainNodeFwd(ILUtils.copy(diffDecl), null, false,
                        readBySrcDecl, null, null, null, false, false,
                        "Differentiated use declaration", false);
                }
                if (diffDecl != null) {
                    addFutureBwdSplitDecl(diffDecl, "Differentiated plain declaration");
                }
            } else {
                if (diffDecl != null) {
                    addFuturePlainNodeFwd(diffDecl, null, false,
                        readBySrcDecl, null, null, writtenBySrcDecl, false, false,
                        "Differentiated plain declaration", true);
                }
            }
            while (diffDeclarations != null) {
                diffDecl = diffDeclarations.head.tree;
                if (separateBwdScope) {
                    // Primal and diff declaration must go to a different place,
                    // which is the declaration Block of the BWD sweep:
                    addFutureBwdSplitDecl(diffDecl, "Remaining part of differentiated declaration");
                } else {
                    addFuturePlainNodeFwd(diffDecl, null, false,
                        readBySrcDecl, null, null, writtenBySrcDecl, false, false,
                        "Remaining part of differentiated declaration", false);
                }
                diffDeclarations = diffDeclarations.tail;
            }
        }
        // Case of differentiated declarations that must go to the split-FWD declaration location:
        while (diffDeclarationsForFwd!=null) {
            Tree diffDecl = diffDeclarationsForFwd.head.tree ;
            addFuturePlainNodeFwd(diffDecl, null, false,
                readBySrcDecl, null, null, writtenBySrcDecl, false, false,
                "Differentiated plain declaration for SPLIT FWD", false);
            diffDeclarationsForFwd = diffDeclarationsForFwd.tail ;
        }
    }


    /** Returns true when the current location will give birth to two separated
     * adjoint diff locations (one in the forward sweep, one in the backward sweep)
     * and these locations have different declaration scopes
     * (e.g. (FOO_FWD() and FOO_BWD()), or one in two different {} scopes in C. */
    private boolean separatedFwdBdwScopes(int differentiationMode) {
        return (differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE
                || (differentiationMode == DiffConstants.ADJOINT_MODE
                    && !(adEnv.curUnitIsContext
                         || adEnv.curSymbolTable() == adEnv.curUnit().privateSymbolTable()
                         || adEnv.curSymbolTable() == adEnv.curUnit().publicSymbolTable()
                         || adEnv.curBwdSymbolTable == adEnv.curFwdSymbolTable))) ;
    }

    /**
     * Build the derivative instructions for an increment (or decrement) statement e.g. ++x;
     * This builds the new differentiated instructions that will go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order).
     */
    private void buildDiffInstructionsOfIncrement(Tree srcTree, int differentiationMode,
                                                  boolean primalIsLive, boolean mustSaveTBRBeforeInstr,
                                                  BoolVector beforeActiv, BoolVector afterActiv,
                                                  BoolVector afterReqX, BoolVector beforeAvlX) {
        if (TapEnv.associationByAddress()) {
            updateAAInstructionMask(adEnv.curInstruction(), adEnv.curSymbolTable(), adEnv.curFwdSymbolTable);
        }
        String unaryOper = ILUtils.getIdentString(srcTree.down(1));
        String bwdOp = unaryOper.equals("--prefix") || unaryOper.equals("--postfix") ? "++prefix" : "--prefix";
        Tree incrExp = srcTree.down(2);
        WrapperTypeSpec type = adEnv.curSymbolTable().typeOf(incrExp);
        boolean isActivePointer =
            TypeSpec.isA(type, SymbolTableConstants.POINTERTYPE)
            && pointerIsActiveHere(incrExp, beforeActiv, afterActiv, beforeAvlX, afterReqX);
        for (adEnv.iReplic=0 ; adEnv.iReplic<TapEnv.diffReplica() ; ++adEnv.iReplic) {
            Tree diffIncrExp = null;
            // TODO: this code is VERY similar to buildReturnTangentNodes() !!
            if (isActivePointer) {
                diffIncrExp = varRefDifferentiator().diffVarRef(adEnv.curActivity(), incrExp,
                    adEnv.curFwdSymbolTable, false, srcTree,
                    false, true, null);
                Tree diffTreeFwd = ILUtils.build(ILLang.op_unary,
                                     ILUtils.build(ILLang.op_ident, unaryOper),
                                     diffIncrExp);
                addFuturePlainNodeFwd(diffTreeFwd, null, false,
                    ILUtils.usedVarsInExp(incrExp, null, true), null,
                    new TapList<>(incrExp, null), new TapList<>(incrExp, null), false, false,
                    "fwd increment of diff pointer", false);
            }
            if ((differentiationMode == DiffConstants.ADJOINT_MODE
                 || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE)
                && (isActivePointer && !adEnv.curUnitIsContext)) {
                Tree diffTreeBwd = ILUtils.build(ILLang.op_unary,
                        ILUtils.build(ILLang.op_ident, bwdOp),
                        ILUtils.copy(diffIncrExp));
                addFuturePlainNodeBwd(diffTreeBwd, null, false,
                        ILUtils.usedVarsInExp(incrExp, null, true), null,
                        new TapList<>(incrExp, null), new TapList<>(incrExp, null), false, false,
                        "bwd decrement of diff pointer", false);
            }
        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        if (mustSaveTBRBeforeInstr
            && (differentiationMode == DiffConstants.ADJOINT_MODE
                || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE)) {
            Tree reverseIncremTree = ILUtils.build(ILLang.op_unary,
                                       ILUtils.build(ILLang.op_ident, bwdOp),
                                       ILUtils.copy(incrExp));
            addFuturePlainNodeBwd(reverseIncremTree, null, false,
                        new TapList<>(incrExp, null), new TapList<>(incrExp, null), null, null, false, false,
                        "bwd decrement of primal variable", true);
        }
        // Build the copy of the primal instruction:
        if (primalIsLive || differentiationMode == DiffConstants.TANGENT_MODE) {
            addFuturePlainNodeFwd(ILUtils.copy(srcTree), adEnv.curInstruction().whereMask(), false,
                    ILUtils.usedVarsInExp(incrExp, null, true),
                    new TapList<>(incrExp, null), null, null, false, false,
                    "Copy of primal increment instruction", false);
        }
    }

    /**
     * Build the derivative instructions for a return statement "srcReturn".
     * This builds the list that must go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order).
     */
    private void buildDiffInstructionsOfReturn(Tree srcReturn, int differentiationMode,
                                               boolean primalIsLive,
                                               BoolVector beforeActiv, BoolVector afterActiv,
                                               BoolVector afterReqX, BoolVector beforeAvlX) {
        Tree returnedExp = srcReturn.down(1);
        WrapperTypeSpec returnedType = adEnv.curSymbolTable().typeOf(returnedExp);
        boolean addNbDirsLoop = returnedType.acceptsMultiDirDimension();
        Tree[] diffTreeR = null ;
        Tree diffTree = null, copyTree = null;
        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            TapList<Tree> refsR = null, refsW = null, diffRefsR = null, diffRefsW = null;
            boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(adEnv.curActivity());
            boolean resultIsActive = formalArgsActivity[formalArgsActivity.length - 1];
            boolean mustDiffOperation = (resultIsActive && !ILUtils.isNullOrNone(returnedExp));
            if (returnedType != null && returnedType.containsAPointer()) {
                if (mustDiffOperation) {
                    diffTreeR = new Tree[TapEnv.diffReplica()] ;
                    for (adEnv.iReplic=0 ; adEnv.iReplic<diffTreeR.length ; ++adEnv.iReplic) {
                        Tree diffVar =
                            varRefDifferentiator().diffVarRef(adEnv.curActivity(), returnedExp, adEnv.curFwdSymbolTable,
                                    false, srcReturn, false, true, null);
                        if (TapEnv.diffReplica()>1) {
                            int tailIndexInDiffArgs = -TapEnv.diffReplica()+adEnv.iReplic ;
                            Tree diffResultVar = ILUtils.getArguments(curDiffUnit().entryBlock().headTree()).down(tailIndexInDiffArgs);
                            diffTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(diffResultVar), diffVar);
                        } else {
                            diffTree = ILUtils.build(ILLang.op_return,
                                          diffVar, ILUtils.copy(srcReturn.down(2)));
                        }
                        diffTreeR[adEnv.iReplic] = diffTree ;
                    }
                    adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                    refsR = ILUtils.usedVarsInExp(returnedExp, null, false);
                    refsW = null;
                    diffRefsR = new TapList<>(returnedExp, null);
                    diffRefsW = new TapList<>(returnedExp, null);
                }
            } else {
                TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
                TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
                ToObject<Tree> toReturnedExp = new ToObject<>(returnedExp);
                if (mustDiffOperation
                        && (multiDirMode() || TapEnv.diffReplica()>1 || curDiffUnit().functionTypeSpec().isAFunction())) {
                    diffTreeR = tangentDiffExpr(toReturnedExp, beforeActiv,
                            refsRW, diffRefsRW);
                    for (int iReplic=0 ; iReplic<diffTreeR.length ; ++iReplic) {
                        diffTree = diffTreeR[iReplic] ; // temporary during change for replica
                        if (adEnv.curUnit().isC() && diffTree == null) {
                            if (returnedType != null && returnedType.wrappedType != null) {
                                diffTree = returnedType.wrappedType.buildConstantZero();
                            } else {
                                diffTree = ILUtils.build(ILLang.op_realCst, "0.0");
                            }
                        }
                        if (TapEnv.diffReplica()>1) {
                            int tailIndexInDiffArgs = -TapEnv.diffReplica()+iReplic ;
                            Tree diffResultVar = ILUtils.getArguments(curDiffUnit().entryBlock().headTree()).down(tailIndexInDiffArgs);
                            diffTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(diffResultVar), diffTree) ;
                        } else if (multiDirMode()
                                   // skip if this is the main() routine (TODO: refactor this!):
                                   && adEnv.curUnit() != adEnv.srcCallGraph().getMainUnit()) {
                            Tree diffResultVar = ILUtils.getArguments(curDiffUnit().entryBlock().headTree()).down(-2);
                            Tree diffDimensions = ILUtils.build(ILLang.op_expressions,
                                varRefDifferentiator().dirIndexSymbolHolder.makeNewRef(adEnv.curFwdSymbolTable));
                            diffTree = ILUtils.build(ILLang.op_assign,
                                         ILUtils.build(ILLang.op_arrayAccess,
                                           ILUtils.copy(diffResultVar),
                                           diffDimensions),
                                         diffTree);
                        } else {
                            diffTree = ILUtils.build(ILLang.op_return, diffTree);
                        }
                        diffTreeR[iReplic] = diffTree ;
                    }
                }
                srcReturn = ILUtils.build(ILLang.op_return, toReturnedExp.obj(), ILUtils.copy(srcReturn.down(2))); // must be simplified !
                refsR = refsRW.first;
                refsW = refsRW.second;
                diffRefsR = diffRefsRW.first;
                diffRefsW = diffRefsRW.second;
            }
            copyTree = ILUtils.removeSourceCodeAnnot(ILUtils.copy(srcReturn));
            // Build the primal copy of the return instruction:
            if (primalIsLive && !ILUtils.isNullOrNone(returnedExp)) {
                // If there was a returned expression (e.g in C):
                boolean builtSomeDiffTree = (diffTreeR!=null && diffTreeR.length>0 && diffTreeR[0]!=null) ;
                if (!builtSomeDiffTree && !resultIsActive && curFwdDiffUnit().isFortran() && adEnv.curUnit().isAFunction()) {
                    String returnVarName =
                            (adEnv.curUnit().otherReturnVar() == null
                                    ? adEnv.curUnit().name()
                                    : adEnv.curUnit().otherReturnVar().symbol);
                    if ("main".equals(returnVarName)) {
                        returnVarName = "mainResult";
                    }
                    Tree returnVar = ILUtils.build(ILLang.op_ident, returnVarName);
                    // This will tell flowGraphDifferentiator() to place a return instruction at the real end:
                    flowGraphDifferentiator().returnedVariable = returnVar;
                    //returnVar.setAnnotation("isFunctionName", Boolean.TRUE);
                    Tree returnPrimalInDiff = ILUtils.build(ILLang.op_assign,
                            ILUtils.copy(returnVar),
                            ILUtils.copy(copyTree.cutChild(1)));
                    addFuturePlainNodeFwd(returnPrimalInDiff, null, false,
                            ILUtils.usedVarsInExp(srcReturn.down(1), null, true), null, null, null, false, false,
                            "Prepare return of primal result because no diff result", true);
                    copyTree.setChild(ILUtils.copy(returnVar), 1);
                }
                if (builtSomeDiffTree && adEnv.curUnit().isC() && !TapEnv.associationByAddress()
                        // skip if this is the main() routine (TODO: refactor this!):
                        && adEnv.curUnit() != adEnv.srcCallGraph().getMainUnit()) {
                    // If the primal result of foo() is needed e.g. return 2*x, and the diff of foo already has to return
                    // a derivative, our model chooses to return the primal result as an extra out argument "foo",
                    // so the primal return becomes *foo = 2*x. cf set08/lh010, set09/v162...
                    int tailIndexInDiffArgs = (TapEnv.diffReplica()>1 ? -1-TapEnv.diffReplica() : (multiDirMode() ? -3 : -1)) ;
                    Tree returnedTree = ILUtils.getArguments(curDiffUnit().entryBlock().headTree()).down(tailIndexInDiffArgs);
                    // TODO: using curDiffUnit().entryBlock().headTree() above, is rather dirty...
                    if (returnedTree != null) { // may be null for main()...
                        returnedTree = ILUtils.copy(returnedTree.down(1));
                        copyTree = ILUtils.build(ILLang.op_assign,
                                ILUtils.build(ILLang.op_pointerAccess, returnedTree),
                                copyTree.cutChild(1));
                    }
                }
            }
            if (primalIsLive) {
                addFuturePlainNodeFwd(copyTree, null, false,
                        ILUtils.usedVarsInExp(returnedExp, null, true), null, null, null, false, false,
                        "Copy primal return instruction", true);
            }
            if (diffTreeR!=null) {
                for (int iReplic=0 ; iReplic<diffTreeR.length ; ++iReplic) {
                    if (diffTreeR[iReplic]!=null) {
                        addFuturePlainNodeFwd(diffTreeR[iReplic], null, addNbDirsLoop,
                                refsR, refsW, diffRefsR, diffRefsW, false, false,
                                "Differentiated return instruction", true);
                    }
                }
            }
        } else if (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            // In adjoint mode, we never reach here because the return statement of
            // function foo() (e.g. "return 2*x;") has been temporarily split into an assignement
            // "foo = 2*x;" which is differentiated by buildDiffInstructionsOfPlainAssignment().
            // It will be followed by a "return foo;" directly added by flowGraphDifferentiator().
            if (adEnv.curUnitIsContext) {
                // In context subroutines only, preserve the original return statement:
                addFuturePlainNodeFwd(ILUtils.removeSourceCodeAnnot(ILUtils.copy(srcReturn)), null, false,
                        ILUtils.usedVarsInExp(returnedExp, null, true), null, null, null, false, false,
                        "Copy primal return instruction (context)", true);
            }
        }
    }

    private void buildDiffInstructionsOfIORead(Tree tree, int differentiationMode,
                                               boolean primalIsLive, boolean mustSaveTBRBeforeInstr,
                                               BoolVector beforeActiv, BoolVector afterActiv,
                                               BoolVector beforeUseful, BoolVector afterUseful) {
        TapList<IterDescriptor> iterators = multiDirMode() ? new TapList<>(varRefDifferentiator().multiDirIterDescriptor, null) : null;
        TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>>
            initTrees = reinitializeDiffIOVars(tree.down(3),
                           (differentiationMode == DiffConstants.TANGENT_MODE
                            ? adEnv.curFwdSymbolTable
                            : adEnv.curBwdSymbolTable),
                           iterators, beforeActiv, afterActiv, beforeUseful, afterUseful);
        TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>> diffTriplet;
        Tree diffTree;
        TapPair<TapList<Tree>, TapList<Tree>> refsRW, diffRefsRW;

        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            while (initTrees != null) {
                diffTriplet = initTrees.head;
                diffTree = diffTriplet.first;
                refsRW = diffTriplet.second;
                diffRefsRW = diffTriplet.third;
                addFuturePlainNodeFwd(diffTree, null, false,
                        refsRW.first, refsRW.second, diffRefsRW.first, diffRefsRW.second, false, false,
                        "Needed diff init before IO-read", false);
                initTrees = initTrees.tail;
            }
        } else if (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            // The TBR restore instructions (i.e. the POPs) of an IOCall must occur
            //  after the diff var reinitializations, because these diff reinitializations
            //  do not use these restored values and sometimes overwrite them (cf F77:lha69-70-71)
            // For IO-calls, we must restore TBR vars either if they are overwritten by
            //  the fwd sweep (mustSaveTBRBeforeInstr && hasTBRexps()), or by the bwd sweep
            //  which may overwrite do indices in op_iterativeVariableRef's (initTrees).
            TapList<Tree> overwrittenTBRExprs = mustSaveTBRBeforeInstr ? hasTBRexps(tree.down(3), null) : null;
            TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> inInitTrees = initTrees;
            TapList<Tree> refsW;
            while (inInitTrees != null) {
                diffTriplet = inInitTrees.head;
                refsW = diffTriplet.second.second;
                while (refsW != null) {
                    if (ADTBRAnalyzer.isTBR(adEnv.curActivity(), refsW.head) &&
                            !TapList.contains(overwrittenTBRExprs, refsW.head)) {
                        overwrittenTBRExprs = new TapList<>(refsW.head, overwrittenTBRExprs);
                    }
                    refsW = refsW.tail;
                }
                inInitTrees = inInitTrees.tail;
            }
            RefDescriptor refDescriptor;
            Tree overwrittenExp;
            TapList<Tree> subExpsRead;
            while (overwrittenTBRExprs != null) {
                overwrittenExp = overwrittenTBRExprs.head;
                refDescriptor =
                        new RefDescriptor(overwrittenExp, null,
                                adEnv.curSymbolTable(), curFwdDiffUnit().privateSymbolTable(), null,
                                null, false, null, curDiffUnit());
                if (refDescriptor.isChannelOrIO()) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(AD07) Data-Flow recovery (TBR) on this I-O call needs to save the I-O state"); //"or a channel"!!
                } else if (adEnv.curSymbolTable().varHasModifier(overwrittenExp, "allocatable")) {
                   TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(AD07) Data-Flow recovery (TBR) on this I-O call needs to restore allocatable pointer "+ILUtils.toString(overwrittenExp) + ", not implemented yet") ;
                } else {
                    prepareRestoreOperations(refDescriptor, null, 0, adEnv.curFwdSymbolTable, adEnv.curBwdSymbolTable);
                    subExpsRead = ILUtils.usedVarsInExp(overwrittenExp, null, false);
                    addFuturePlainNodeFwd(refDescriptor.makePush(), null, false,
                            new TapList<>(overwrittenExp, subExpsRead), null, null, null, false, true,
                            "Store TBR value before IO-read", false);
                    refDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                    addFuturePlainNodeBwd(refDescriptor.makePop(), null, false,
                            subExpsRead, new TapList<>(overwrittenExp, null), null, null, false, true,
                            "Restore TBR value before IO-read", false);
                }
                overwrittenTBRExprs = overwrittenTBRExprs.tail;
            }
            while (initTrees != null) {
                diffTriplet = initTrees.head;
                diffTree = diffTriplet.first;
                refsRW = diffTriplet.second;
                diffRefsRW = diffTriplet.third;
                addFuturePlainNodeBwd(diffTree, null, false,
                        refsRW.first, refsRW.second, diffRefsRW.first, diffRefsRW.second, false, false,
                        "Reinitialize diff due to IO-read", false);
                initTrees = initTrees.tail;
            }
        }
        // Build the primal copy of the IO statement:
        if (primalIsLive || differentiationMode == DiffConstants.TANGENT_MODE) {
            Tree copyTree = ILUtils.removeSourceCodeAnnot(ILUtils.copy(tree));
            refsRW = new TapPair<>(null, null);
            if (TapEnv.associationByAddress()) {
                copyTree = turnAssociationByAddressPrimal(copyTree, adEnv.curSymbolTable(), adEnv.curFwdSymbolTable, null, false);
            }
            fillRefsRW(tree, refsRW);
            addFuturePlainNodeFwd(copyTree, null, false,
                    refsRW.first, refsRW.second, null, null, false, false,
                    "Copy IO-read", true);
        }
    }

    /**
     * Turns a source expr into the "same" non-diff expr, but based on
     * the primal parts of AA diff variables. Example: turns x = 2*y into x.v = 2*y.v;
     * Turns foo(x, y) into foo(x.v, y,v) according to functionTypeSpec of foo in the
     * differentiated callGraph, turns x into x.v according to contextTypeSpec.
     * @param contextTypeSpec if the context is an argument of a function call, type of
     *                        the formal argument, else null.
     * @param onlyPrimalCode true if the intended behavior of this instruction is to compute only primal value.
     * @return The given expr, possibly modified.
     */
    protected Tree turnAssociationByAddressPrimal(Tree expr, SymbolTable srcSymbolTable,
                                                  SymbolTable destSymbolTable, WrapperTypeSpec contextTypeSpec,
                                                  boolean onlyPrimalCode) {
// System.out.println("CALLING turnAssociationByAddressPrimal ON "+expr+" onlyPrimal:"+onlyPrimalCode);
        switch (expr.opCode()) {
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_ident: {
// System.out.println("ON REF "+expr+" WITH ACTIVITY "+adEnv.curActivity()+" ANNOTATED_ACTIVE:"+ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), expr, srcSymbolTable));
                if (!onlyPrimalCode && TapEnv.get().complexStep
                    && adEnv.curActivity() != null
                    && ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), expr, srcSymbolTable)) {
                    break ;
                }
                WrapperTypeSpec exprType = srcSymbolTable.typeOf(expr);
                String varName = ILUtils.baseName(expr);
                VariableDecl varDecl = srcSymbolTable.getVariableOrConstantDecl(varName);
                // Turn reference to primal var "X" into "X.v",
                // only if X currently has a derivative counterpart
                // and contextType != diffTypeSpec(X)
                if (varDecl != null) {
                    WrapperTypeSpec baseExprType = exprType.baseTypeSpec(true);
                    VariableDecl diffVarDecl = destSymbolTable.getVariableOrConstantDecl(varName);
                    if (baseExprType != null
                        && baseExprType.isDifferentiablePlainType()
                        && varDecl.isActiveSymbolDecl()
                        // Do not turn "X" into "X.v", if the type expected as a result
                        //  (i.e. contextTypeSpec) is already the type of X:
                        && (contextTypeSpec == null
                            || (varDecl.type().wrappedType().diffTypeSpec != null
                                && !contextTypeSpec.equalsNeglectSizes(
                                                      varDecl.type().wrappedType().diffTypeSpec)))
                        ) {
                        boolean isVarDeclOfCopiedUnit = curDiffUnit().isAFunction()
                                && curDiffUnit().isFortran()
                                && curDiffUnit().name().equals(varName);
                        if (onlyPrimalCode || isVarDeclOfCopiedUnit) {
                            expr = ILUtils.build(ILLang.op_fieldAccess, ILUtils.copy(expr),
                                     ILUtils.build(ILLang.op_ident, TapEnv.assocAddressValueSuffix()));
                        } else {
                            expr = varRefDifferentiator().diffVarRef(adEnv.curActivity(), expr, destSymbolTable,
                                    varRefDifferentiator().isCurFunctionName(varName), expr, false, true, null);
                            if (expr.opCode() == ILLang.op_fieldAccess) {
                                expr.setChild(ILUtils.build(ILLang.op_ident, TapEnv.assocAddressValueSuffix()), 2);
                            }
                        }
                    } else if (contextTypeSpec != null
                            && diffVarDecl.type().wrappedType().diffTypeSpec != null
                            && !contextTypeSpec.equalsCompilDep(diffVarDecl.type().wrappedType().diffTypeSpec)) {
                        // TODO trouver l'instruction de declarations de diffVarDecl et la modifier:
                        // diffVarDecl.type = contextTypeSpec;
                        // TapEnv.toolError("Association by Address, check " + ILUtils.toString(expr)
                        //        + " in " + adEnv.curUnit().name);
                    }
                }
                break;
            }
            case ILLang.op_call: {
                Tree[] args = ILUtils.getArguments(expr).children();
                FunctionTypeSpec functionTypeSpec = null;
                Unit functionUnit = null ;
                Object callArrowAnnot = expr.getAnnotation("callArrow");
                if (callArrowAnnot != null) {
                    CallArrow callArrow = (CallArrow) callArrowAnnot;
                    functionUnit = callArrow.destination;
                    Unit copiedCalledUnit = adEnv.copiedUnits.retrieve(functionUnit);
                    if (copiedCalledUnit != null) {
                        functionTypeSpec = copiedCalledUnit.functionTypeSpec();
                    } else {
                        functionTypeSpec = functionUnit.functionTypeSpec();
                    }
                }
                boolean[] formalArgsActivity = null ;
                if (functionUnit!=null) {
                    ActivityPattern calledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                                       expr, adEnv.curActivity(), "multiActivityCalleePatterns");
                    UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(functionUnit);
                    formalArgsActivity = diffInfo.getUnitFormalArgsActivityS(calledActivity);
// TapEnv.printOnTrace("CURACTIVITY:"+adEnv.curActivity()+" CALLEDACTIVITY:"+calledActivity) ;
// TapEnv.printOnTrace("FORMALARGSACTIVITY: ");
// if (formalArgsActivity==null) {
//     TapEnv.printOnTrace("NULL") ;
// } else {
//   TapEnv.printOnTrace("[") ;
//   int nbArgs = Math.min(functionUnit.formalArgumentsNb(), formalArgsActivity.length - 1);
//   for (int j = 0; j < formalArgsActivity.length; ++j) {
//     if (formalArgsActivity.length == 1) {
//         TapEnv.printOnTrace("->");
//     }
//     TapEnv.printOnTrace("" + formalArgsActivity[j]);
//     if (j + 1 < formalArgsActivity.length) {
//         TapEnv.printOnTrace(j + 1 == nbArgs ? "->" : ", ");
//     }
//   }
//   TapEnv.printlnOnTrace("]");
// }
                }
                WrapperTypeSpec argType = null;
                for (int i = args.length - 1; i >= 0; --i) {
                    if (functionTypeSpec != null && functionTypeSpec.argumentsTypes.length > i) {
                        argType = functionTypeSpec.argumentsTypes[i];
                    }
                    // TODO: tester activity du param formel i dans functionUnit
                    // au lieu de tester les types
                    // TODO: verifier avec -context
                    String varName = ILUtils.baseName(args[i]) ;
                    VariableDecl varDecl = (varName==null ? null : srcSymbolTable.getVariableDecl(varName));
                    boolean actualActive = (varDecl==null ? false :  varDecl.isActiveSymbolDecl()) ;
// System.out.println(" ON ARG "+i+": "+args[i]+" formalArgT:"+argType+" actualArgT:"+srcSymbolTable.typeOf(args[i])) ;
// System.out.println(" ACTUAL ACTIVE:"+actualActive) ;
                    if (argType == null
                            || !argType.equalsCompilIndep(destSymbolTable.typeOf(args[i]))
                        || // better test?:  (TODO: check and simplify)
                        (TapEnv.get().complexStep && actualActive
                         && (formalArgsActivity==null || !formalArgsActivity[i+1]))) {
                        Tree newArg = turnAssociationByAddressPrimal(args[i], srcSymbolTable,
                                                                     destSymbolTable, argType, onlyPrimalCode);
                        ILUtils.getArguments(expr).setChild(newArg, i + 1);
                    }
                }
                break;
            }
            case ILLang.op_assign: {
                WrapperTypeSpec callResultType = null;
                if (expr.down(2).opCode() == ILLang.op_call
                    && curDiffUnit() == adEnv.copiedUnits.retrieve(adEnv.curUnit())) {
                    callResultType = destSymbolTable.typeOf(expr.down(2));
                }
                expr.setChild(turnAssociationByAddressPrimal(expr.down(2), srcSymbolTable,
                                                             destSymbolTable, callResultType, onlyPrimalCode),
                              2);
                // If -complexStep, better assign to the full complex rhs rather than only to rhs%re,
                // because %re would happen only in context code, and it's safer to remove the %re there
                // because this forces an implicit reset of the imaginary part to zero.
                if (!TapEnv.get().complexStep) {
                    expr.setChild(turnAssociationByAddressPrimal(expr.down(1), srcSymbolTable,
                                                             destSymbolTable, callResultType, onlyPrimalCode),
                              1);
                }
                break ;
            }
            default:
                if (!expr.isAtom()) {
                    Tree[] children = expr.children();
                    for (int i = children.length - 1; i >= 0; --i) {
                        expr.setChild(turnAssociationByAddressPrimal(children[i], srcSymbolTable,
                                                destSymbolTable, null, onlyPrimalCode),
                                      i + 1);
                    }
                }
        }
// System.out.println("RETURNING turnAssociationByAddressPrimal ON "+expr);
        return expr;
    }

    private void buildDiffInstructionsOfParallelPragma(Tree pragma, int differentiationMode,
                                                       BoolVector beforeActiv) {
        Directive scopingsDirective =
                adEnv.curInstruction().hasDirective(differentiationMode == DiffConstants.TANGENT_MODE
                        ? Directive.MULTITHREAD_TGT : Directive.MULTITHREAD_ADJ);
        TapList<Tree> toClausesGivenScoping = new TapList<>(null, null);
        TapList<String> toForcedAtomicScopingNames = new TapList<>(null, null);
        TapList<String> toForcedOtherScopingNames = new TapList<>(null, null);
        if (scopingsDirective != null) {
            Directive.analyzeMultithreadScopings(scopingsDirective.arguments[0], !adEnv.curUnit().isFortran(),
                    toClausesGivenScoping,
                    toForcedAtomicScopingNames, toForcedOtherScopingNames);
        }
        TapList<String> forcedScopingNames = TapList.append(toForcedAtomicScopingNames.tail, toForcedOtherScopingNames.tail);
        curBlock().headInstr().tree.setAnnotation("forcedScopingNames", forcedScopingNames);

        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            Tree newClausesTree = null ;
            if (ILUtils.isIdent(pragma.down(1), "OMP", true)) {
                Tree[] clauses = pragma.down(2).children();
                TapList<Tree> newClauses = null;
                Tree clause, diffClause, diffIdents;
                for (int i = clauses.length - 1; i >= 0; --i) {
                    clause = clauses[i];
                    diffClause = null;
                    switch (clause.opCode()) {
                        case ILLang.op_sharedVars:
                        case ILLang.op_privateVars:
                        case ILLang.op_firstPrivateVars:
                        case ILLang.op_lastPrivateVars:
                            diffIdents = diffClauseIdents(clause.down(1), adEnv.curFwdSymbolTable, null, null, null, null, forcedScopingNames);
                            if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                diffClause = ILUtils.build(clause.opCode(), diffIdents);
                            }
                            break;
                        case ILLang.op_reductionVars:
                            diffIdents = diffClauseIdents(clause.down(2), adEnv.curFwdSymbolTable, null, null, null, null, forcedScopingNames);
                            if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                if (!ILUtils.isIdent(clause.down(1), "+", true)) {
                                    TapEnv.fileError(clause, "Only OpenMP sum-reductions can be differentiated.");
                                } else {
                                    diffClause = ILUtils.build(ILLang.op_reductionVars,
                                            ILUtils.copy(clause.down(1)),
                                            diffIdents);
                                }
                            }
                            break;
                        case ILLang.op_schedule:
                        case ILLang.op_numThreads:
                        case ILLang.op_defaultSharingAttribute:
                            break;
                        default:
                            TapEnv.toolWarning(-1, "(Differentiate parallel clause, tangent) Unexpected operator: " + clause.opName());
                    }
                    if (diffClause != null) {
                        newClauses = new TapList<>(diffClause, newClauses);
                    }
                    newClauses = new TapList<>(ILUtils.copy(clause), newClauses);
                }
                newClauses = addGivenDiffScopings(toClausesGivenScoping.tail, newClauses, adEnv.curFwdSymbolTable);
                newClausesTree = ILUtils.build(ILLang.op_expressions, newClauses) ;
            } else if (ILUtils.isIdent(pragma.down(1), "CUDA", true)) {
                newClausesTree = ILUtils.copy(pragma.down(2)) ;
            }
            // TODO: replicate if -diffReplica
            Tree diffPragma = ILUtils.build(pragma.opCode(),
                                ILUtils.copy(pragma.down(1)), newClausesTree, null);
            addFuturePlainNodeFwd(diffPragma, null, false,
                    null, null, null, null, false, true,
                    "Tangent diff Parallel Pragma", true);
        } else { // ADJOINT_MODE:
            boolean isCuda = ILUtils.isIdent(pragma.down(1), "CUDA", true) ;
            // Rethink globally this building of the new OMP "scopings" for the future adjoint variables.

            // For OpenMP, compute "onlyReadSmallActivePrimals" the Boolvector of active
            //  zones that are possibly read in region, and not written in region, and either
            //  scalar or acceptably small array, and attach it to the current pragma Tree.
            // These zones are candidate for OpenMP reduction(+) clause rather then shared+atomic-increments.
            BoolVector onlyReadSmallActivePrimals = null;
            if (beforeActiv != null && !isCuda) {
                TapTriplet<TapList<FGArrow>, TapList<Block>, TapList<FGArrow>> frontier =
                    adEnv.curUnit().getFrontierOfParallelRegion(curBlock());
                int vectorLen = adEnv.curVectorLen;
                TapPair<BoolVector, BoolVector> inOuts =
                    TapEnv.inOutAnalyzer().getInOutOfRegion(frontier, adEnv.curUnit());
                BoolVector activesAllKind =
                    DataFlowAnalyzer.changeKind(beforeActiv, adEnv.curDiffVectorLen, adEnv.diffKind,
                        vectorLen, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
                onlyReadSmallActivePrimals = activesAllKind.and(inOuts.first).minus(inOuts.second);
                for (int i=0; i<vectorLen; ++i) {
                    if (onlyReadSmallActivePrimals.get(i)) {
                        ZoneInfo zi = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                        int knownSize = (zi == null ? -1 : zi.knownSize(adEnv.curSymbolTable()));
                        // The following test can be modified to change the cases where shared+atomic is replaced with reduction(+).
                        // When test returns true, then reduction(+) will be chosen, else shared+atomic will be chosen.
                        if (knownSize == -1 || knownSize > 99) {
                            onlyReadSmallActivePrimals.set(i, false);
                        }
                    }
                }
                // Remove from onlyReadSmallActivePrimals the zones of user-enforced given "ATOMIC" variables,
                // so that increments of these variables will be labelled ATOMIC, whatever Tapenade had decided:
                VariableDecl variableDecl;
                TapIntList varZones;
                toForcedAtomicScopingNames = toForcedAtomicScopingNames.tail;
                while (toForcedAtomicScopingNames != null) {
                    variableDecl = adEnv.curSymbolTable().getVariableDecl(toForcedAtomicScopingNames.head);
                    varZones = (variableDecl == null
                                ? null
                                : ZoneInfo.listAllZones(variableDecl.zones(), false));
                    onlyReadSmallActivePrimals.set(varZones, false);
                    toForcedAtomicScopingNames = toForcedAtomicScopingNames.tail;
                }
                // Add into onlyReadSmallActivePrimals the zones of user-enforced given non-ATOMIC variables,
                // so that increments of these variables will not be labelled ATOMIC, whatever Tapenade had decided:
                toForcedOtherScopingNames = toForcedOtherScopingNames.tail;
                while (toForcedOtherScopingNames != null) {
                    variableDecl = adEnv.curSymbolTable().getVariableDecl(toForcedOtherScopingNames.head);
                    varZones = (variableDecl == null
                                ? null
                                : ZoneInfo.listAllZones(variableDecl.zones(), false)) ;
                    onlyReadSmallActivePrimals.set(varZones, true);
                    toForcedOtherScopingNames = toForcedOtherScopingNames.tail;
                }
                curBlock().headInstr().tree.setAnnotation("onlyReadSmallActivePrimals", onlyReadSmallActivePrimals);
            }

            Tree fwdPragma ;
            if (isCuda) {
                fwdPragma = ILUtils.copy(pragma);
            } else {
                fwdPragma = ILUtils.copy(pragma);
                Tree srcSchedule = ILUtils.getRemoveSchedule(fwdPragma);
                curBlock().headInstr().tree.setAnnotation("srcSchedule", srcSchedule);
            }
            addFuturePlainNodeFwd(fwdPragma, null, false,
                    null, null, null, null, false, true,
                    "Copy of primal Parallel Pragma", false);

            Tree bwdPragma ;
            if (isCuda) {
                bwdPragma = ILUtils.copy(pragma);
            } else {
                TapList<Tree> newClauses = null;
                TapList<Tree> bwdPrivatePreInits = null;
                TapList<Tree> bwdPrivatePostInits = null;
                Tree clause, diffClause, diffIdents;
                Tree[] clauses = pragma.down(2).children();
                if (clauses.length>0) {
                    BoolVector conflictFreeAdjointSharedVars = null ;
                    if (
                        curBlock().headInstr().tree.opCode()==ILLang.op_parallelRegion
                        //containsShared(clauses) //too strong if shared is by default (see lbm, plenty of shared, no shared clause!)
                        ) {
                        conflictFreeAdjointSharedVars =
                            TapEnv.multithreadAnalyzer().getConflictFreeSharedAdjointZones(curBlock()) ;
                    }
                    for (int i = clauses.length - 1; i >= 0; i--) {
                        clause = clauses[i];
                        diffClause = null;
                        switch (clause.opCode()) {
                            case ILLang.op_sharedVars: {
                                ToObject<Tree> toReductionIdents = new ToObject<>(null);
                                // Keep as plain shared the adjoint vars that are in "conflictFreeAdjointSharedVars".
                                // Exclude from shared adjoint vars those that are in "onlyReadSmallActivePrimals",
                                // declare them instead as reduction(+)
                                diffIdents = diffClauseIdents(clause.down(1), adEnv.curBwdSymbolTable, null,
                                        conflictFreeAdjointSharedVars, onlyReadSmallActivePrimals,
                                         toReductionIdents, forcedScopingNames);
                                if (toReductionIdents.obj() != null) {
                                    newClauses = new TapList<>(ILUtils.build(ILLang.op_reductionVars,
                                                                 ILUtils.build(ILLang.op_ident, "+"),
                                                                 toReductionIdents.obj()),
                                                               newClauses);
                                }
                                if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                    diffClause = ILUtils.build(ILLang.op_sharedVars, diffIdents);
                                }
                                break;
                            }
                            case ILLang.op_privateVars: {
                                ToObject<Tree> toInits = new ToObject<>(null);
                                diffIdents = diffClauseIdents(clause.down(1), adEnv.curBwdSymbolTable, toInits, null, null, null, forcedScopingNames);
                                if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                    diffClause = ILUtils.build(ILLang.op_privateVars, diffIdents);
                                    bwdPrivatePreInits = TapList.append(bwdPrivatePreInits, listOfCutChildren(toInits.obj()));
                                }
                                break;
                            }
                            case ILLang.op_firstPrivateVars:
                                diffIdents = diffClauseIdents(clause.down(1), adEnv.curBwdSymbolTable, null, null, null, null, forcedScopingNames);
                                if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                    diffClause = ILUtils.build(ILLang.op_reductionVars,
                                                   ILUtils.build(ILLang.op_ident, "+"),
                                                   diffIdents);
                                }
                                break;
                            case ILLang.op_lastPrivateVars: {
                                ToObject<Tree> toInits = new ToObject<>(null);
                                diffIdents = diffClauseIdents(clause.down(1), adEnv.curBwdSymbolTable, toInits, null, null, null, forcedScopingNames);
                                if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                    diffClause = ILUtils.build(ILLang.op_firstPrivateVars, diffIdents);
                                    bwdPrivatePreInits = new TapList<>(ILUtils.build(ILLang.op_if,
                                                                         ILUtils.build(ILLang.op_ident, "isNotLastThread"),
                                                                         ILUtils.copy(toInits.obj())),
                                                                       bwdPrivatePreInits);
                                    bwdPrivatePostInits = TapList.append(bwdPrivatePostInits, listOfCutChildren(ILUtils.copy(toInits.obj())));
                                }
                                break;
                            }
                            case ILLang.op_reductionVars:
                                diffIdents = diffClauseIdents(clause.down(2), adEnv.curBwdSymbolTable, null, null, null, null, forcedScopingNames);
                                if (!ILUtils.isNullOrEmptyList(diffIdents)) {
                                    if (!ILUtils.isIdent(clause.down(1), "+", true)) {
                                        TapEnv.fileError(clause, "Only OpenMP sum-reductions can be differentiated.");
                                    } else {
                                        diffClause = ILUtils.build(ILLang.op_firstPrivateVars, diffIdents);
                                    }
                                    break;
                                }
                                break;
                            case ILLang.op_schedule:
                            case ILLang.op_numThreads:
                                break;
                            default:
                                TapEnv.toolWarning(-1, "(Differentiate parallel clause, adjoint) Unexpected operator: " + clause.opName());
                        }
                        if (diffClause != null) {
                            newClauses = new TapList<>(diffClause, newClauses);
                        }
                        if (clause.opCode() != ILLang.op_schedule) { //Schedules are rebuilt in FlowGraphDifferentiator
                            newClauses = new TapList<>(ILUtils.copy(clause), newClauses);
                        }
                    }
                }
                newClauses = addGivenDiffScopings(toClausesGivenScoping.tail, newClauses, adEnv.curBwdSymbolTable);
                bwdPragma = ILUtils.build(pragma.opCode(),
                                   ILUtils.copy(pragma.down(1)),
                                   ILUtils.build(ILLang.op_expressions, newClauses),
                                   null);
                bwdPragma.setAnnotation("bwdOMPPrivateInits",
                                        new TapPair<>(bwdPrivatePreInits, bwdPrivatePostInits));
            }
            // TODO: put this in some if block. Don't call this if adjoint is serial
            // TODO: replicate if -diffReplica
            addFuturePlainNodeBwd(bwdPragma, null, false,
                    null, null, null, null, false, true,
                    "Adjoint of Parallel Pragma", false);
        }
    }

    private boolean containsShared(Tree[] clauses) {
        boolean contains = false ;
        for (int i = clauses.length - 1; !contains && i >= 0; i--) {
            if (clauses[i].opCode()==ILLang.op_sharedVars) contains = true ;
        }
        return contains ;
    }

    /**
     * When usageBlock is inside the scope of an OMP PARALLEL region.
     *
     * @return the
     * BoolVector of the "onlyReadSmallActivePrimals" that has been attached to the
     * OMP PARALLEL Tree, which contains the zones for which we decided for a REDUCTION(+) adjoint.
     */
    protected static BoolVector getOnlyReadSmallActivePrimals(Block usageBlock) {
        Instruction parallelRegionInstr = TapList.last(usageBlock.parallelControls);
        return (parallelRegionInstr == null ? null
                : parallelRegionInstr.tree.getAnnotation("onlyReadSmallActivePrimals"));
    }

    /**
     * When usageBlock is inside the scope of an OMP PARALLEL region.
     *
     * @return the list of variable
     * names for which the adjoint scoping is forced by a user-given directive "$AD OMP ...".
     * This list has been attached to the controlling OMP PARALLEL Tree.
     */
    protected static TapList<String> getForcedScopingNames(Block usageBlock) {
        Instruction parallelRegionInstr = TapList.last(usageBlock.parallelControls);
        return (parallelRegionInstr == null ? null
                : parallelRegionInstr.tree.getAnnotation("forcedScopingNames"));
    }

    /**
     * @return the tree of the derivatives of the given list of primal variables "identTrees".
     * When one primal variable name is in "forcedScopingNames", puts aside its differentiation,
     * because the derivative scoping has been given by the user so it will be placed separately.
     * Otherwise, when zonesApart is given non-null, puts aside the derivatives of variables
     * that intersect these zonesApart, and returns them as a second Tree of derivatives
     * in toIdentsTreeApart.
     * Otherwise, when toInits is given non-null, this method fills it with
     * an op_blockStatement that initializes the derivatives to zero
     */
    private Tree diffClauseIdents(Tree identsTree, SymbolTable diffSymbolTable, ToObject<Tree> toInits,
                                  BoolVector zonesConflictFree, BoolVector zonesApart,
                                  ToObject<Tree> toIdentsTreeApart, TapList<String> forcedScopingNames) {
        Tree[] idents = identsTree.children();
        TapList<Tree> newIdents = null;
        TapList<Tree> newIdentsApart = null;
        TapList<Tree> zeroInits = null;
        Tree[] zeroInitR;
        Tree zeroInit;
        String baseName;
        VariableDecl variableDecl;
        Tree newTree;
        TapIntList varZones;
        for (int i = idents.length - 1; i >= 0; i--) {
            baseName = ILUtils.baseName(idents[i]);
            if (baseName != null &&
                    !TapList.containsString(forcedScopingNames, baseName, !adEnv.curUnit().isFortran())) {
                variableDecl = diffSymbolTable.getVariableDecl(baseName);
                if (variableDecl != null && variableDecl.isActiveSymbolDecl()) {
                    newTree = varRefDifferentiator().diffSymbolTree(
                                      adEnv.curActivity(), idents[i], diffSymbolTable,
                                      true, false, false, identsTree, false,
                                      curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, null) ;
                    varZones = (zonesApart == null ? null : ZoneInfo.listAllZones(variableDecl.zones(), false));
                    if (zonesConflictFree != null
                            && DataFlowAnalyzer.containsExtendedDeclared(zonesConflictFree,
                            SymbolTableConstants.ALLKIND, varZones, null, adEnv.curSymbolTable(), null)) {
                        newIdents = new TapList<>(newTree, newIdents);
                    } else if (zonesApart != null
                            && DataFlowAnalyzer.intersectsZoneRks(zonesApart, varZones, null)) {
                        newIdentsApart = new TapList<>(newTree, newIdentsApart);
                    } else {
                        newIdents = new TapList<>(newTree, newIdents);
                        if (toInits != null) {
                            zeroInitR = makeDiffInitialization(idents[i], null, adEnv.curSymbolTable(),
                                    diffSymbolTable, null,
                                    false, null, true);
                            for (int iReplic=0 ; iReplic<zeroInitR.length ; ++iReplic) {
                                zeroInit = zeroInitR[iReplic] ;
                                zeroInits = new TapList<>(zeroInit, zeroInits);
                            }
                        }
                    }
                }
            }
        }
        if (toInits != null && zeroInits != null) {
            toInits.setObj(ILUtils.build(ILLang.op_blockStatement, zeroInits));
        }
        if (toIdentsTreeApart != null) {
            toIdentsTreeApart.setObj(newIdentsApart == null
                    ? null
                    : ILUtils.build(ILLang.op_idents, newIdentsApart));
        }
        return (newIdents == null ? null : ILUtils.build(ILLang.op_idents, newIdents));
    }

    private static TapList<Tree> listOfCutChildren(Tree tree) {
        TapList<Tree> result = null;
        for (int i = tree.length(); i > 0; --i) {
            result = new TapList<>(tree.cutChild(i), result);
        }
        return result;
    }

    private TapList<Tree> addGivenDiffScopings(TapList<Tree> clausesGivenScoping, TapList<Tree> clauses, SymbolTable diffSymbolTable) {
        Tree givenClause;
        int rankOfIdents;
        Tree givenNames;
        Tree diffGivenNames;
        while (clausesGivenScoping != null) {
            givenClause = clausesGivenScoping.head;
            rankOfIdents = (givenClause.opCode() == ILLang.op_reductionVars ? 2 : 1);
            givenNames = givenClause.down(rankOfIdents);
            diffGivenNames = diffClauseIdents(givenNames, diffSymbolTable, null, null, null, null, null);
            if (!ILUtils.isNullOrEmptyList(diffGivenNames)) {
                givenClause.setChild(diffGivenNames, rankOfIdents);
                clauses = new TapList<>(givenClause, clauses);
            }
            clausesGivenScoping = clausesGivenScoping.tail;
        }
        return clauses;
    }

    private void buildDiffInstructionsOfOther(Tree tree, int differentiationMode,
                                              boolean primalIsLive,
                                              BoolVector beforeActiv) {
        if (TapEnv.associationByAddress()) {
            updateAAInstructionMask(adEnv.curInstruction(), adEnv.curSymbolTable(), adEnv.curFwdSymbolTable);
        }
        TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
        TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
        if (tree.opCode() == ILLang.op_if && TapEnv.valid() && differentiationMode == DiffConstants.TANGENT_MODE) {
            // In the case when "domain of validity" is activated (-directValid) :
            TapList<Tree> switchExpressions = collectSwitchExpressions(tree.down(1), null);
            Tree switchExpression, validityTest;
            while (switchExpressions != null) {
                switchExpression = switchExpressions.head;
                ToObject<Tree> toSwitchExp = new ToObject<>(switchExpression);
                Tree[] diffTreeR = tangentDiffExpr(toSwitchExp, beforeActiv,
                        refsRW, diffRefsRW);
                Tree diffTree = diffTreeR[0] ; // temporary during change for replica
                switchExpression = toSwitchExp.obj();
                if (diffTree != null) {
                    String typeName =
                            adEnv.curSymbolTable().typeOf(switchExpression).
                                    buildTypeNameForProcName(new TapList<>(null, null), adEnv.curSymbolTable());
                    validityTest = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "validity_domain_" + typeName),
                            ILUtils.build(ILLang.op_expressions, switchExpression, diffTree));
                    addFuturePlainNodeFwd(validityTest, null, false,
                            refsRW.first, refsRW.second, diffRefsRW.first, diffRefsRW.second, false, false,
                            "Domain of validity test for conditional", false);
                }
                switchExpressions = switchExpressions.tail;
            }
        }
        // Back to general case: copy the primal instruction.
        if (primalIsLive) {
            fillRefsRW(tree, refsRW);
            Tree copyTree = ILUtils.removeSourceCodeAnnot(ILUtils.copy(tree));
            if (TapEnv.associationByAddress()) {
                copyTree = turnAssociationByAddressPrimal(copyTree, adEnv.curSymbolTable(),
                                                          adEnv.curFwdSymbolTable, null, false);
            }
            if (copyTree.getAnnotation("hasBeenSplit") == Boolean.TRUE) {
                copyTree.removeAnnotation("sourcetree");
            }
            addFuturePlainNodeFwd(copyTree, adEnv.curInstruction().whereMask(), false,
                    refsRW.first, refsRW.second, null, null, false, false,
                    "Copy of primal tree", false);
        }
    }

    protected Tree[] tangentDiffExpr(ToObject<Tree> toPrimalExpr,
                                   BoolVector beforeActiv,
                                   TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                   TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
        TapList<NewBlockGraphNode> toPrecomputes = new TapList<>(null, null);
        TapList<DiffAssignmentNode> toPrecomputesDiff = new TapList<>(null, null);
        Tree[] diffExpressionR =
                expressionDifferentiator().tangentDifferentiatePlainExpression(
                        toPrimalExpr, toPrecomputes, toPrecomputesDiff,
                        beforeActiv,
                        primRW, diffRW,
                        adEnv.curFwdSymbolTable);
        // Insert primal pre-computations memorized in toPrecomputes :
        NewBlockGraphNode futureNode;
        toPrecomputes = TapList.nreverse(toPrecomputes.tail);
        while (toPrecomputes != null) {
            futureNode = toPrecomputes.head;
            // Add operation tmpVar = subSrcExpression :
            addFuturePlainNodeFwd(futureNode.tree, null, false,
                    ILUtils.usedVarsInExp(futureNode.tree, null, true),
                    new TapList<>(futureNode.tree.down(1), null), null, null, false, false,
                    "Assign split primal expression", false);
            toPrecomputes = toPrecomputes.tail;
        }
        // Insert diff (tangent) pre-computations memorized in toPrecomputesDiff :
        DiffAssignmentNode futureDiffNode ;
        toPrecomputesDiff = toPrecomputesDiff.tail ;
        while (toPrecomputesDiff != null) {
            futureDiffNode = toPrecomputesDiff.head;
            // Add a precompute of a diff sub-expression:
            addFuturePlainNodeFwd(futureDiffNode.diffAssign,
                                  (futureDiffNode.type.isScalar() ? null : adEnv.curInstruction().whereMask()),
                                  futureDiffNode.type.acceptsMultiDirDimension(),
                                  futureDiffNode.primR, futureDiffNode.primW, futureDiffNode.diffR, futureDiffNode.diffW,
                                  false, false, "Assign split diff expression", false);
            toPrecomputesDiff = toPrecomputesDiff.tail;
        }
        return diffExpressionR;
    }

    private boolean isAssignment(Tree srcAction) {
        int oper = srcAction.opCode();
        return oper == ILLang.op_assign || oper == ILLang.op_plusAssign || oper == ILLang.op_minusAssign;
    }

    protected boolean pointerIsActiveHere(Tree varRef, BoolVector beforeActiv, BoolVector afterActiv,
                                          BoolVector beforeAvlX, BoolVector afterReqX) {
        TapList accessibleZoneTree =
                adEnv.curSymbolTable().treeOfZonesOfValue(varRef, null, adEnv.curInstruction(), null);
        if (accessibleZoneTree != null && varRef.opCode() == ILLang.op_return) {
            // Do this only for op_return, to be coherent with ReqExplicit, e.g. lines "// Not sure why do this only for return..."
            accessibleZoneTree = TapList.copyTree(accessibleZoneTree);
            DataFlowAnalyzer.includePointedElementsInTree(accessibleZoneTree, null, null, true,
                    adEnv.curSymbolTable(), adEnv.curInstruction(), false, false);
        }
        TapIntList accessibleZones = ZoneInfo.listAllZones(accessibleZoneTree, true);
        boolean accessibleTmp = TapIntList.intersects(accessibleZones, adEnv.toActiveTmpZones.tail);
        TapIntList accessibleVectorIndices = accessibleZones ;
        TapIntList accessibleDiffKindVectorIndices =
                DataFlowAnalyzer.mapZoneRkToKindZoneRk(accessibleZones, adEnv.diffKind,
                                                                  adEnv.curSymbolTable());
        TapList<ZoneInfo> zonesList =
                DataFlowAnalyzer.mapZoneRkToZoneInfo(accessibleZones, adEnv.curSymbolTable());
        BoolVector contextDiffRequired = adEnv.curSymbolTable().getRequiredDiffVars(adEnv.curActivity());
        // "PointerActiveUsage":
        boolean isActivePointer =
            (afterReqX != null && (accessibleTmp || afterReqX.intersects(accessibleVectorIndices)))
            ||
            //Dubious test: F90:lh53 vs. several others...
            // Maybe the test should not use beforeAvlX but rather DiffLiveness OnDiffPtr!
            (beforeAvlX != null && (accessibleTmp || beforeAvlX.intersects(accessibleVectorIndices)))
            ||
            (beforeActiv == null && afterActiv == null && contextDiffRequired != null
             && contextDiffRequired.intersects(accessibleDiffKindVectorIndices)
             ||
             ADActivityAnalyzer.haveDifferentiatedVariable(zonesList));
        return isActivePointer;
    }

    /**
     * Builds the special differentiated instructions that come from non-diff ASSIGN statement "tree"
     * and that should go to the FORWARD sweep. Example: an active pointer assignment p=&gt;q
     * should generate a pb=&gt;qb in the FWD sweep.
     * This method may be called by tangent AD as well as adjoint AD.
     * When called in tangent, pass null into arguments bwdInstructionsGraph and bwdBlock:
     * this will cancel creation of TBR push/pops and other BWD-special code
     */
    private TapPair<Tree, TapList<Tree>[]> buildDiffOfControlAssignment(Tree tree, int differentiationMode,
                                                                        SymbolTable fwdSymbolTable,
                                                                        BoolVector beforeActiv, BoolVector afterActiv,
                                                                        BoolVector beforeAvlX, BoolVector afterReqX) {
        TapPair<Tree, TapList<Tree>[]> fwdDiffControlTree = null;
        // Preliminaries: compute assignedVar, assignedType, and the boolean mustDiffOperation that
        //  tells if this "control" operation must be extended to apply to the derivatives too:
        Tree assignedVar = tree.down(1);
        //When the assigned var is a complex declarator,
        // peel off all type decoration around the declared identifier:
        // This is ugly and results from wrong tree for assignment in declarator
        boolean removedDeclarator = true;
        while (removedDeclarator) {
            switch (assignedVar.opCode()) {
                case ILLang.op_pointerDeclarator:
                case ILLang.op_functionDeclarator:
                case ILLang.op_bitfieldDeclarator:
                    assignedVar = assignedVar.down(1);
                    removedDeclarator = true;
                    break;
                case ILLang.op_modifiedDeclarator:
                    assignedVar = assignedVar.down(2);
                    removedDeclarator = true;
                    break;
                default:
                    removedDeclarator = false;
                    break;
            }
        }
        WrapperTypeSpec assignedType = adEnv.curSymbolTable().typeOf(assignedVar);
        boolean somePointerIsAssigned = assignedType != null && assignedType.containsAPointer();
        boolean mustDiffOperation = pointerIsActiveHere(assignedVar, beforeActiv, afterActiv, beforeAvlX, afterReqX);

// TapEnv.printlnOnTrace("buildDiffOfControlAssignment on "+tree+" assigned type:"+assignedType+" somePointerIsAssigned:"+somePointerIsAssigned+" mustDiffOperation:"+mustDiffOperation) ;

        if (TypeSpec.isA(assignedType, SymbolTableConstants.COMPOSITETYPE) || somePointerIsAssigned && mustDiffOperation) {
// if (differentiationMode!=TANGENT_MODE) PBaseTrack.experimentAssignControlTree(tree, curSymbolTable())  ;
            // Now create the differentiated control instructions:
            // TODO: maybe we should do something for non-malloc cases e.g. p += offset ;
            // Following test added because allocate case is now treated separately,
            // and also because this code in the ADJOINT case has been moved into buildCallAdjointNodes():
            if (differentiationMode == DiffConstants.TANGENT_MODE || adEnv.curUnitIsContext) {
                Tree diffLHS = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tree.down(1), fwdSymbolTable,
                        false, tree.down(1), false, true, null);
                Tree diffRHS;
                if (ILUtils.isCallingString(tree.down(2), "null", false)) {
                //Special case of Fortran90 instruction "p => NULL()"
                    diffRHS = ILUtils.copy(tree.down(2));
                } else {
                    diffRHS = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tree.down(2), fwdSymbolTable,
                            false, tree.down(2), false, true, null);
                }
                Tree diffControlAssign = ILUtils.build(tree.opCode(), diffLHS, diffRHS);
                // If this assigns through a pointer to the orig result in the SPLIT ADJOINT_FWD in C,
                // then insert a pointerAccess on the assignment's lhs:
                if (differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE && !curFwdDiffUnit().isFortran()
                        && tree.getAnnotation("ComesFromAReturn") == Boolean.TRUE) {
                    diffControlAssign.setChild(
                            ILUtils.build(ILLang.op_pointerAccess, diffControlAssign.cutChild(1)), 1);
                }
                @SuppressWarnings("unchecked")
                TapList<Tree>[] rwrw = new TapList[]{ILUtils.usedVarsInExp(tree.down(1), null, false),
                        null,
                        new TapList<>(tree.down(2), null),
                        new TapList<>(assignedVar, null)};
                fwdDiffControlTree = new TapPair<>(diffControlAssign, rwrw);
//                   fwdDiffControlTrees = new TapList<>(fwdDiffControlTree, fwdDiffControlTrees) ;
//                   if (bwdInstructionsGraph!=null && !adEnv.curUnitIsContext
//                       && ADTBRAnalyzer.isTBROnDiffPtr(curActivity(), assignedVar)) {
//                       RefDescriptor refDescriptor =
//                           new RefDescriptor(assignedVar, null,
//                                 curSymbolTable(), fwdSymbolTable, null,
//                                 ILUtils.baseTree(diffLHS), true, null, curDiffUnit());
//                       if (!curSymbolTable().isAnAllocatablePointer(assignedVar, curInstruction)) {
//                           prepareRestoreOperations(refDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable) ;
//                           // [llh] TODO: force preserving the order of this inside the fwd sweep (uses PUSH/POP stack!)
//                           rwrw = new TapList[]{ILUtils.usedVarsInExp(assignedVar, null, false),
//                                                null,
//                                                new TapList<>(assignedVar, null),
//                                                null} ;
//                           fwdDiffControlTrees = new TapList<>(new TapPair(refDescriptor.makePush(), rwrw), fwdDiffControlTrees);
//                           if (TypeSpec.isA(refDescriptor.elementTypeSpec, POINTERTYPE) //maybe use refDescriptor.holdsAPointer
//                               && DataFlowAnalyzer.mayPointToRelocated(assignedVar, false, curInstruction, curSymbolTable(), differentiationMode==ADJOINT_MODE)) {
//                               ++numRebase ;
//                               Tree rebaseTree = refDescriptor.makeRebase(new TapList<>(Boolean.TRUE, null),
//                                                                          null, ILUtils.baseTree(diffLHS),
//                                                                          ((TapEnv.dbadMode()==-1 || TapEnv.debugADMM())?numRebase:-1)) ;
//                               if (rebaseTree!=null) {
//                                   adEnv.usesADMM = true ;
//                                   TapList writtenTrees = refDescriptor.collectTrees(null);
//                                   addFuturePlainNodeBwd(rebaseTree, null, false,
//                                       ILUtils.usedVarsInTreeOfExps(writtenTrees, null, false),
//                                       null, writtenTrees, writtenTrees, false, false,
//                                       "Rebase diff control: "+ILUtils.toString(assignedVar, curActivity()), false) ;
//                               }
//                           }
//                           refDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable ;
//                           addFuturePlainNodeBwd(refDescriptor.makePop(), null, false,
//                               ILUtils.usedVarsInExp(assignedVar, null, false),
//                               null, null, new TapList<Tree>(assignedVar, null), false, true,
//                               "Restore diff control:"+ILUtils.toString(assignedVar, curActivity()), false) ;
//                       }
//                   }
            }
        }
        return fwdDiffControlTree;
    }

    /**
     *
     */
    private Tree splitForADReverse(Tree tree, boolean resultIsExpected, boolean resultIsAssigned, boolean resultIsLive,
                                   TapList<Tree> toSplitTrees, boolean modeIsJoint) {
        boolean isRequiredInAdjoint =
            DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), tree, modeIsJoint, false);
        boolean isDiffPtrRequiredInAdjoint =
            DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), tree, modeIsJoint, true);
        boolean isAnnotatedActive = ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable());
        Object annotation;
        switch (tree.opCode()) {
            case ILLang.op_assign: {
                Tree newAssign;
                Tree lhs = splitForADReverse(tree.down(1), false, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint);
                Tree rhs = splitForADReverse(tree.down(2), true, true, isRequiredInAdjoint, toSplitTrees, modeIsJoint);
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree.down(1), lhs);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree.down(1), lhs);
                // Insert a temporary variable in case of random aliasing in assignment, i.e. A(exp1) = 2*A(exp2) + x

                TapIntList lhsZones =
                        ZoneInfo.listAllZones(adEnv.curSymbolTable().treeOfZonesOfValue(lhs, null, adEnv.curInstruction(), null), true);
                boolean splitHere = possibleAliasInAssign(lhs, lhsZones, adEnv.curInstruction().whereMask(),
                        rhs, adEnv.curInstruction().whereMask(), false);
//splitHere=false;//FOR DEBUGGING MHD ONLY !!
                if (splitHere) {
// TapEnv.printlnOnTrace(" !! POSSIBLE ALIAS IN "+ILUtils.toString(lhs)+" := "+ILUtils.toString(rhs)) ;
                    rhs = splitSubExpression(rhs, "tmp", adEnv.curSymbolTable().typeOf(lhs),
                            toSplitTrees, lhs, adEnv.curSymbolTable(),
			    isRequiredInAdjoint, isDiffPtrRequiredInAdjoint, modeIsJoint, null, -1);
                    registerTempVarForDebug(rhs);
                }
                newAssign = ILUtils.build(ILLang.op_assign, lhs, rhs);
                // Special: if shape is V=FUNC(...V...), and FUNC() was marked TBR to snapshot its result variable,
                // instead of "V=" being marked TBR "true", and if the instr is now split as tmp=FUNC() ; V=tmp;
                // we must remove the result-TBR on FUNC() and add TBR true on "V="
                TapPair completeCallTBRInfo = null;
                ActivityPattern curCalledActivity = null;
                if (splitHere && tree.down(2).opCode() == ILLang.op_call) {
                    completeCallTBRInfo = (TapPair) ActivityPattern.getAnnotationForActivityPattern(ILUtils.getArguments(tree.down(2)), adEnv.curActivity(), "TBR");
                    curCalledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(ILUtils.getArguments(tree.down(2)), adEnv.curActivity(), "multiActivityCalleePatterns");
                }
                if (curCalledActivity != null) {
                    ActivityPattern.setAnnotationForActivityPattern(newAssign.down(1), adEnv.curActivity(), "multiActivityCalleePatterns", curCalledActivity);
                }
                if (completeCallTBRInfo != null) {
                    TapList[][] tbrOnArgs = (TapList[][]) completeCallTBRInfo.first;
                    TapList[] snpArgs = tbrOnArgs[0];
                    TapList[] snpArgsOnDiffPtr = tbrOnArgs[1];
                    TapList[] sbkArgs = tbrOnArgs[2];
                    TapList[] sbkArgsOnDiffPtr = tbrOnArgs[3];
                    int resultIndex = snpArgs.length - 1;
                    boolean resultIsSnp = TapList.oneTrue(snpArgs[resultIndex]);
                    if (resultIsSnp) {
                        snpArgs[resultIndex] = new TapList<>(Boolean.FALSE, null);
                    }
                    boolean resultIsSnpOnDiffPtr = snpArgsOnDiffPtr != null && TapList.oneTrue(snpArgsOnDiffPtr[resultIndex]);
                    if (resultIsSnpOnDiffPtr) {
                        snpArgsOnDiffPtr[resultIndex] = new TapList<>(Boolean.FALSE, null);
                    }
                    boolean resultIsSbk = sbkArgs != null && TapList.oneTrue(sbkArgs[resultIndex]);
                    if (resultIsSbk) {
                        sbkArgs[resultIndex] = new TapList<>(Boolean.FALSE, null);
                    }
                    boolean resultIsSbkOnDiffPtr = sbkArgsOnDiffPtr != null && TapList.oneTrue(sbkArgsOnDiffPtr[resultIndex]);
                    if (resultIsSbkOnDiffPtr) {
                        sbkArgsOnDiffPtr[resultIndex] = new TapList<>(Boolean.FALSE, null);
                    }
                    if (resultIsSnp || resultIsSnpOnDiffPtr || resultIsSbk || resultIsSbkOnDiffPtr) {
                        ActivityPattern.setAnnotationForActivityPattern(newAssign.down(1), adEnv.curActivity(), "TBR",
                                new boolean[]{resultIsSnp || resultIsSbk, resultIsSnpOnDiffPtr || resultIsSbkOnDiffPtr});
                    }
                }
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newAssign, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newAssign, modeIsJoint, true);
                }
                // Copy the activity annotation:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newAssign);
                }
                toSplitTrees.placdl(newAssign);
                return ILUtils.copy(lhs);
            }
            case ILLang.op_call: {
                int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
                Unit calledUnit = DataFlowAnalyzer.getCalledUnit(tree, adEnv.curSymbolTable());
                ActivityPattern curCalledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(tree, adEnv.curActivity(), "multiActivityCalleePatterns");
                Tree[] args = ILUtils.getArguments(tree).children();
                int nbArgs = args.length;
                boolean functionIsIntrinsicWithSeparableDiff = calledUnit.isIntrinsic() && calledUnit.diffInfo != null;

                boolean[] formalArgsActivity = null;
                if (curCalledActivity != null && !functionIsIntrinsicWithSeparableDiff) {
                    formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(curCalledActivity);
                }

                FunctionTypeSpec functionTypeSpec =
                        tree.getAnnotation("functionTypeSpec");
                if (functionTypeSpec == null) {
                    functionTypeSpec = calledUnit.functionTypeSpec();
                }
                WrapperTypeSpec actualType;
                TapList<Tree> newArgs = null;
                Tree argTree, newCall;
                CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
                // compute the private "Write" signature of the called unit:
                BoolVector callW = new BoolVector(nDZ);
                TapList[] paramsW;
                if (calledUnit.unitInOutW() != null) {
                    paramsW = DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), callW, null, null, true,
                                                                             tree, adEnv.curInstruction(), callArrow,
                                                                             SymbolTableConstants.ALLKIND,
                                                                             null, null, false, false) ;
                } else {
                    paramsW = new TapList[nbArgs+1];
                    for (int i=nbArgs ; i>=0 ; --i) {
                        paramsW[i] = new TapList<>(Boolean.FALSE, null);
                    }
                }
                for (int i = nbArgs - 1; i >= 0; i--) {
                    argTree = splitForADReverse(args[i], true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint);
                    actualType = adEnv.curSymbolTable().typeOf(args[i]);
                    if (// Don't split args of numeric intrinsics for which we know the partial derivatives:
                            !functionIsIntrinsicWithSeparableDiff
                                    // Don't split an "optional" arg:
                                    && argTree.opCode() != ILLang.op_none
                                    // Don't split a "nameEq" arg:
                                    && argTree.opCode() != ILLang.op_nameEq
                                    // Split only when this formal arg is active:
                                    && formalArgsActivity != null && i < formalArgsActivity.length - 1 && formalArgsActivity[i]
                                    // Don't split if this arg's type shows that it will not be differentiated:
                                    && (actualType == null || actualType.wrappedType == null
                                    || actualType.wrappedType.isDifferentiated(null))
                                    // Don't split variable references, constants, addresses, except when expensive and overwritten:
                                    && (!ILUtils.isAWritableVarRef(argTree, adEnv.curSymbolTable())
                                    && !ILUtils.isExpressionNumConstant(argTree)
                                    && argTree.opCode() != ILLang.op_address
                                    ||
                                    ExpressionDifferentiator.getCost(argTree) > 30
                                            && !TapList.oneTrue(paramsW[i+1]))
                    ) {
                        argTree =
                                splitSubExpression(
					argTree, "arg"+(i+1),
                                        functionTypeSpec.argumentsTypes[i],
                                        toSplitTrees, argTree, adEnv.curSymbolTable(),
                                        DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), argTree, modeIsJoint, false),
                                        DiffLivenessAnalyzer.isTreeRequiredInDiff(adEnv.curActivity(), argTree, modeIsJoint, true),
                                        modeIsJoint, functionTypeSpec, i+1);
                        registerTempVarForDebug(argTree);
                    }
                    newArgs = new TapList<>(argTree, newArgs);
                }
                newCall = ILUtils.buildCall(
                        ILUtils.copy(ILUtils.getCalledName(tree)),
                        ILUtils.build(ILLang.op_expressions, newArgs));
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newCall);
                }
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newCall);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newCall);
                // Copy the calleeActivityPattern annotation:
                annotation = ActivityPattern.getAnnotationForActivityPattern(tree, adEnv.curActivity(), "multiActivityCalleePatterns");
                if (annotation != null) {
                    ActivityPattern.setAnnotationForActivityPattern(newCall, adEnv.curActivity(), "multiActivityCalleePatterns", annotation);
                }
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newCall, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newCall, modeIsJoint, true);
                }
                annotation = tree.getAnnotation("functionTypeSpec");
                if (annotation != null) {
                    newCall.setAnnotation("functionTypeSpec", annotation);
                }
                annotation = tree.getAnnotation("arrayReturnTypeSpec");
                if (annotation != null) {
                    newCall.setAnnotation("arrayReturnTypeSpec", annotation);
                }
                annotation = tree.getAnnotation("sourcetree");
                if (annotation != null) {
                    newCall.setAnnotation("sourcetree", annotation);
                }
                annotation = tree.getAnnotation("callArrow");
                if (annotation != null) {
                    newCall.setAnnotation("callArrow", annotation);
                }
                annotation = tree.getAnnotation("pointersActualInitialDests");
                if (annotation != null) {
                    newCall.setAnnotation("pointersActualInitialDests", annotation);
                }
                if (curDiffUnit().isFortran() && calledUnit.isAFunction() && calledUnit.isFortran() || resultIsExpected) {
                    // [llh] TODO: this "isActiveCall" should better be computed through diffCallNeeded(...) :
                    boolean isActiveCall = adEnv.curUnitIsActiveUnit &&
                            ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), tree, adEnv.curSymbolTable());
                    // Don't split if we are in any of the following cases:
                    if (!(// result of this tree is assigned to a variable and the parent of this tree is diff-live:
                            resultIsAssigned && resultIsLive ||
                                    // OR result of this tree is assigned to a variable and this tree is not diff-live:
                                    resultIsAssigned && !isRequiredInAdjoint ||
                                    // OR this call is passive (since Block.splitTree didn't split it already, let's not split it now!)
                                    !isActiveCall ||
                                    // OR called function is an intrinsic with simple predefined diff (e.g. "ABS" in F90:lh09):
                                    functionIsIntrinsicWithSeparableDiff)) {
                        WrapperTypeSpec localTypeSpec = tree.getAnnotation("arrayReturnTypeSpec");
                        if (localTypeSpec == null) {
                            localTypeSpec = functionTypeSpec.returnType;
                        }
                        if (formalArgsActivity != null && formalArgsActivity[formalArgsActivity.length - 1]) {
                            newCall = splitSubExpression(newCall, "res",
                                    localTypeSpec, toSplitTrees, null, adEnv.curSymbolTable(),
                                    isRequiredInAdjoint, isDiffPtrRequiredInAdjoint, modeIsJoint, functionTypeSpec, 0);
                            registerTempVarForDebug(newCall);
                        } else {
                            adEnv.hideActiveTmpZones();
                            newCall = splitSubExpression(newCall, "res",
                                    localTypeSpec, toSplitTrees, null, adEnv.curSymbolTable(),
                                    isRequiredInAdjoint, isDiffPtrRequiredInAdjoint, modeIsJoint, functionTypeSpec, 0);
                            adEnv.resetActiveTmpZones();
                        }
                        // Special: if this function call CALL() was marked TBR to snapshot its result variable,
                        //  since we are splitting it here into "res:=CALL() ; res", and this res is a temp variable
                        //  which is not TBR upstream, we must remove the result-TBR on the CALL().
                        TapPair completeCallTBRInfo =
                                (TapPair) ActivityPattern.getAnnotationForActivityPattern(tree, adEnv.curActivity(), "TBR");
                        if (completeCallTBRInfo != null) {
                            TapList[][] tbrOnArgs = (TapList[][]) completeCallTBRInfo.first;
                            TapList[] snpArgs = tbrOnArgs[0];
                            TapList[] snpArgsOnDiffPtr = tbrOnArgs[1];
                            TapList[] sbkArgs = tbrOnArgs[2];
                            TapList[] sbkArgsOnDiffPtr = tbrOnArgs[3];
                            int resultIndex = snpArgs.length - 1;
                            boolean resultIsSnp = TapList.oneTrue(snpArgs[resultIndex]);
                            if (resultIsSnp) {
                                snpArgs[resultIndex] = new TapList<>(Boolean.FALSE, null);
                            }
                            boolean resultIsSnpOnDiffPtr = snpArgsOnDiffPtr != null && TapList.oneTrue(snpArgsOnDiffPtr[resultIndex]);
                            if (resultIsSnpOnDiffPtr) {
                                snpArgsOnDiffPtr[resultIndex] = new TapList<>(Boolean.FALSE, null);
                            }
                            boolean resultIsSbk = sbkArgs != null && TapList.oneTrue(sbkArgs[resultIndex]);
                            if (resultIsSbk) {
                                sbkArgs[resultIndex] = new TapList<>(Boolean.FALSE, null);
                            }
                            boolean resultIsSbkOnDiffPtr = sbkArgsOnDiffPtr != null && TapList.oneTrue(sbkArgsOnDiffPtr[resultIndex]);
                            if (resultIsSbkOnDiffPtr) {
                                sbkArgsOnDiffPtr[resultIndex] = new TapList<>(Boolean.FALSE, null);
                            }
                        }
                    }
                    return newCall;
                } else {
                    toSplitTrees.placdl(newCall);
                    return ILUtils.build(ILLang.op_none);
                }
            }
            case ILLang.op_fieldAccess: {
                Tree fieldTreeCopy = ILUtils.copy(tree.down(2));
//             ILUtils.setFieldRank(fieldTreeCopy, ILUtils.getFieldRank(tree.down(2))) ;
                Tree newTree = ILUtils.build(ILLang.op_fieldAccess,
                        splitForADReverse(tree.down(1), false, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        fieldTreeCopy);
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newTree);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newTree);
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, true);
                }
                return newTree;
            }
            case ILLang.op_arrayAccess: {
                Tree newTree = ILUtils.build(ILLang.op_arrayAccess,
                        splitForADReverse(tree.down(1), false, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newTree);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newTree);
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, true);
                }
                return newTree;
            }
            case ILLang.op_pointerAccess: {
                Tree newTree = ILUtils.build(ILLang.op_pointerAccess,
                        splitForADReverse(tree.down(1), false, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newTree);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newTree);
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, true);
                }
                return newTree;
            }
            case ILLang.op_ident: {
                Tree newTree = ILUtils.copy(tree);
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newTree);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newTree);
                // Copy the liveness annotation:
                if (isRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, false);
                }
                if (isDiffPtrRequiredInAdjoint) {
                    DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newTree, modeIsJoint, true);
                }
                return newTree;
            }
            case ILLang.op_arrayTriplet:
                return ILUtils.build(ILLang.op_arrayTriplet,
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(3), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_complexConstructor: {
                Tree newTree = ILUtils.build(tree.opCode(),
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy annotations:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                // l'annotation "boolean" est utilisee pour decompile .eqv. a la place de .eq.:
                if (tree.getAnnotation("boolean") != null) {
                    newTree.setAnnotation("boolean", "boolean");
                }
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree, newTree);
                ReqExplicit.copyAnnotation(adEnv.curActivity(), tree, newTree);
                return newTree;
            }
            case ILLang.op_minus:
            case ILLang.op_sizeof:
            case ILLang.op_not: {
                Tree newTree = ILUtils.build(tree.opCode(),
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy the activity annotation:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                return newTree;
            }
            case ILLang.op_ifExpression: {
                Tree newTree = ILUtils.build(ILLang.op_ifExpression,
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        splitForADReverse(tree.down(3), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy the activity annotation:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                return newTree;
            }
            case ILLang.op_unary: {
                Tree newTree;
                String operName = ILUtils.getIdentString(tree.down(1));
                if (operName != null &&
                        (operName.equals("++prefix") || operName.equals("++postfix") ||
                                operName.equals("--prefix") || operName.equals("--postfix"))) {
                    Tree isolatedUnary = ILUtils.copy(tree);
                    // Copy the liveness annotation:
                    if (isRequiredInAdjoint) {
                        DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), isolatedUnary, modeIsJoint, false);
                    }
                    if (isDiffPtrRequiredInAdjoint) {
                        DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), isolatedUnary, modeIsJoint, true);
                    }
                    // Copy the activity annotation:
                    if (isAnnotatedActive) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), isolatedUnary);
                    }
                    toSplitTrees.placdl(isolatedUnary);
                    newTree = ILUtils.copy(tree.down(2));
                    // Copy the activity annotation:
                    if (isAnnotatedActive) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                    }
                    if (operName.equals("++postfix")) {
                        newTree = ILUtils.build(ILLang.op_add, newTree,
                                ILUtils.build(ILLang.op_intCst, 1));
                    } else if (operName.equals("--postfix")) {
                        newTree = ILUtils.build(ILLang.op_sub, newTree,
                                ILUtils.build(ILLang.op_intCst, 1));
                    }
                    // Copy the activity annotation:
                    if (isAnnotatedActive) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                    }
                } else {
                    newTree = ILUtils.build(ILLang.op_unary,
                            ILUtils.copy(tree.down(1)),
                            splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                    // Copy the activity annotation:
                    if (isAnnotatedActive) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                    }
                }
                return newTree;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast: {
                Tree newTree = ILUtils.build(tree.opCode(),
                        ILUtils.copy(tree.down(1)),
                        splitForADReverse(tree.down(2), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                // Copy the activity annotation:
                if (isAnnotatedActive) {
                    ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newTree);
                }
                return newTree;
            }
            case ILLang.op_ioCall: {
                Tree newTree = ILUtils.build(ILLang.op_ioCall,
                        ILUtils.copy(tree.down(1)),
                        ILUtils.copy(tree.down(2)),
                        splitForADReverse(tree.down(3), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint));
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, newTree);
                toSplitTrees.placdl(newTree);
                return newTree;
            }
            case ILLang.op_return: {
                if (adEnv.curUnit().isC()
                        && !ILUtils.isNullOrNone(tree.down(1))
                        && tree.down(1).opCode() != ILLang.op_intCst) {
                    Tree splitReturnedExpr = splitForADReverse(tree.down(1), true, true, isRequiredInAdjoint, toSplitTrees, modeIsJoint);
                    String returnVarName =
                            (adEnv.curUnit().otherReturnVar() == null
                                    ? adEnv.curUnit().name()
                                    : adEnv.curUnit().otherReturnVar().symbol);
                    if ("main".equals(returnVarName)) {
                        returnVarName = "mainResult";
                    }
                    Tree returnVar = ILUtils.build(ILLang.op_ident, returnVarName);
                    // Copy the activity annotation:
                    if (isAnnotatedActive) {
                        ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), returnVar);
                    }
                    // This will tell flowGraphDifferentiator() to place a return instruction at the real end:
                    flowGraphDifferentiator().returnedVariable = returnVar;
                    Tree returnAssign = ILUtils.build(ILLang.op_assign,
                            returnVar,
                            ILUtils.copy(splitReturnedExpr));
                    // To know this assigns through a pointer to the orig result in the SPLIT ADJOINT_FWD in C:
                    if (!curFwdDiffUnit().isFortran() /*  && !modeIsJoint */) {
                        returnAssign.setAnnotation("ComesFromAReturn", Boolean.TRUE);
                    }
                    if (isRequiredInAdjoint || !modeIsJoint) { // In Split mode, the return must be diffLive.
                        DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), returnAssign, modeIsJoint, false);
                    }
                    if (isDiffPtrRequiredInAdjoint || !modeIsJoint) {
                        DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), returnAssign, modeIsJoint, true);
                    }
                    toSplitTrees.placdl(returnAssign);
                } else {
                    toSplitTrees.placdl(ILUtils.copy(tree));
                }
                return null;
            }
            case ILLang.op_switch: {
                Tree newTree = ILUtils.build(ILLang.op_switch,
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        ILUtils.copy(tree.down(2)));
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, newTree);
                toSplitTrees.placdl(newTree);
                return null;
            }
            case ILLang.op_if: {
                Tree newTree = ILUtils.build(ILLang.op_if,
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        ILUtils.copy(tree.down(2)),
                        ILUtils.copy(tree.down(3)));
                // Copy the liveness annotation:
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, newTree);
                toSplitTrees.placdl(newTree);
                return null;
            }
            case ILLang.op_where: {
                Tree newTree = ILUtils.build(ILLang.op_where,
                        splitForADReverse(tree.down(1), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        ILUtils.copy(tree.down(2)),
                        ILUtils.copy(tree.down(3)));

                // Copy the liveness annotation:
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, newTree);
                toSplitTrees.placdl(newTree);
                return null;
            }
            case ILLang.op_loop: {
                Tree newTree = ILUtils.build(ILLang.op_loop,
                        ILUtils.copy(tree.down(1)),
                        ILUtils.copy(tree.down(2)),
                        splitForADReverse(tree.down(3), true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint),
                        ILUtils.copy(tree.down(4)));
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, newTree);
                toSplitTrees.placdl(newTree);
                return null;
            }
            case ILLang.op_do: {
                Tree result = ILUtils.copy(tree);
                ADTBRAnalyzer.copyAnnotation(adEnv.curActivity(), tree.down(1), result.down(1));
                DiffLivenessAnalyzer.copyTreeRequiredInDiff(adEnv.curActivity(), tree, result);
                return result;
            }
            case ILLang.op_parallelLoop:
            case ILLang.op_parallelRegion:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_compGoto:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
                toSplitTrees.placdl(ILUtils.copy(tree));
                return null;
            case ILLang.op_allocate:
            case ILLang.op_forall:
            case ILLang.op_for:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_strings:
            case ILLang.op_concat:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_nameEq:
                return ILUtils.copy(tree);
            case ILLang.op_arrayConstructor:
            case ILLang.op_expressions: {
                Tree[] expressions = tree.children();
                TapList<Tree> newExpressions = null;
                Tree newExpression;
                for (int i = expressions.length - 1; i >= 0; i--) {
                    newExpression = splitForADReverse(expressions[i], true, false, isRequiredInAdjoint, toSplitTrees, modeIsJoint);
                    newExpressions = new TapList<>(newExpression, newExpressions);
                }
                return ILUtils.build(tree.opCode(), newExpressions);
            }
            case ILLang.op_varDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_functionDeclarator:
            case ILLang.op_accessDecl:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_data:
            case ILLang.op_intrinsic:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_include:
            case ILLang.op_continue:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_nameList:
                toSplitTrees.placdl(ILUtils.copy(tree));
                return ILUtils.copy(tree);
            case ILLang.op_address:
            case ILLang.op_modifiedType:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return ILUtils.copy(tree);
            default:
                TapEnv.toolWarning(-1, "(Split expression for reverse AD) Unexpected operator: " + tree.opName());
                return ILUtils.copy(tree);
        }
    }

    private void registerTempVarForDebug(Tree tempVar) {
        if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
            tempVarsForDebug = new TapList<>(tempVar, tempVarsForDebug);
        }
    }

    /**
     * @return true if the variable assigned in "lhs" shares memory with one
     * variable read (and even more if written...) in "tree", in differentiable
     * position, and at the same time these are not exactly statically the same
     * variable reference.
     */
    private boolean possibleAliasInAssign(Tree lhs, TapIntList lhsZones,
                                          InstructionMask lhsMask,
                                          Tree tree, InstructionMask treeMask, boolean acrossCall) {
        switch (tree.opCode()) {
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_ident: {
                TapIntList treeZones =
                        ZoneInfo.listAllZones(adEnv.curSymbolTable().treeOfZonesOfValue(tree, null, adEnv.curInstruction(), null), true);
                TapIntList treeDiffKindVectorIndices =
                        DataFlowAnalyzer.mapZoneRkToKindZoneRk(treeZones, adEnv.diffKind,
                                                                          adEnv.curSymbolTable());
                TapIntList lhsDiffKindVectorIndices =
                        DataFlowAnalyzer.mapZoneRkToKindZoneRk(lhsZones, adEnv.diffKind,
                                                                          adEnv.curSymbolTable());
                boolean commonMemory = TapIntList.intersects(lhsDiffKindVectorIndices, treeDiffKindVectorIndices);
                WrapperTypeSpec exprTypeSpec = adEnv.curSymbolTable().typeOf(lhs);
                boolean differentMasks = !exprTypeSpec.isScalar() && !InstructionMask.equalMasks(lhsMask, treeMask);
                // intersection status: 1:exactly same ; -1:completely different ; 0: in-between
                int intersectionStatus =
                        ILUtils.eqOrDisjointRef(lhs, tree, adEnv.curInstruction(), adEnv.curInstruction());
                boolean imperfectIdentity = acrossCall ? intersectionStatus != -1 : intersectionStatus == 0;
                return commonMemory && (imperfectIdentity || differentMasks);
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_complexConstructor:
                return possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(1), treeMask, acrossCall)
                        || possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(2), treeMask, acrossCall);
            case ILLang.op_ifExpression:
                return possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(2), treeMask, acrossCall)
                        || possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(3), treeMask, acrossCall);
            case ILLang.op_minus:
            case ILLang.op_sizeof:
                return possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(1), treeMask, acrossCall);
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                return possibleAliasInAssign(lhs, lhsZones, lhsMask, tree.down(2), treeMask, acrossCall);
            case ILLang.op_call: {
                Unit calledUnit = DataFlowAnalyzer.getCalledUnit(tree, adEnv.curSymbolTable());
                Tree[] actualArgs = ILUtils.getArguments(tree).children();
                int nbArgs = actualArgs.length;
                if (calledUnit.unitInOutR() != null && !calledUnit.hasPredefinedDerivatives()) {
                    if (TapIntList.intersects(lhsZones, adEnv.toActiveTmpZones.tail)) {
                        return true;
                    }
                    CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
                    int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
                    BoolVector callAccess = new BoolVector(nDZ);
                    DataFlowAnalyzer.translateCalleeDataToCallSite(
                            calledUnit.unitInOutPossiblyRorW(), callAccess, null, actualArgs, true,
                            tree, adEnv.curInstruction(), callArrow, SymbolTableConstants.ALLKIND,
                            null, null, true, false) ;
                    if (callAccess.intersects(lhsZones)) {
                        return true;
                    }
                }
                if (calledUnit.isStandard() || calledUnit.isExternal() || calledUnit.isVarFunction() || calledUnit.isInterface()) {
                    acrossCall = true;
                }
                if ("sum".equals(calledUnit.name().toLowerCase())) {
                    Tree maskArg = ILUtils.getOptionalMask(actualArgs, adEnv.curSymbolTable());
                    treeMask = maskArg == null ? null : new InstructionMask(maskArg, null);
                } else if ("spread".equals(calledUnit.name().toLowerCase())) {
                    treeMask = null;
                }
                boolean possibleAlias = false;
                while (!possibleAlias && nbArgs > 0) {
                    nbArgs--;
                    possibleAlias = possibleAliasInAssign(lhs, lhsZones, lhsMask, actualArgs[nbArgs], treeMask, acrossCall);
                }
                return possibleAlias;
            }
            case ILLang.op_arrayConstructor: {
                boolean possibleAlias = false;
                boolean sonAlias;
                Tree[] expressions = tree.children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    sonAlias = possibleAliasInAssign(lhs, lhsZones, lhsMask, expressions[i], null, acrossCall);
                    if (sonAlias) {
                        possibleAlias = true;
                    }
                }
                return possibleAlias;
            }
            case ILLang.op_expressions:
            case ILLang.op_ioCall:
            case ILLang.op_return:
            case ILLang.op_switch:
            case ILLang.op_if:
            case ILLang.op_loop:
            case ILLang.op_do:
            case ILLang.op_stop:
            case ILLang.op_compGoto:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_arrayTriplet:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_not:
            case ILLang.op_forall:
            case ILLang.op_for:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_concat:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_nameEq:
            case ILLang.op_include:
            case ILLang.op_address:
            case ILLang.op_allocate:
            case ILLang.op_modifiedType:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return false;
            default:
                TapEnv.toolWarning(-1, "(Split aliases for reverse AD) Unexpected operator: " + tree.opName());
                return false;
        }
    }

    private Tree splitSubExpression(Tree expression, String name, WrapperTypeSpec type,
                                    TapList<Tree> toSplitTrees, Tree oldLhs, SymbolTable symbolTable,
                                    boolean requiredInAdjoint, boolean diffPtrRequiredInAdjoint, boolean modeIsJoint,
				    FunctionTypeSpec functionTypeSpec, int argRank) {
        boolean expressionIsActive = ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), expression, adEnv.curSymbolTable());

	ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
	Tree newRef =
	    curBlock().buildTemporaryVariable(name, expression, adEnv.curFwdSymbolTable,
							  toNSH, false, null, true, functionTypeSpec, argRank, oldLhs) ;
	NewSymbolHolder newSymbolHolder = toNSH.obj() ;
        newSymbolHolder.zone = adEnv.allocateTmpZone();
	if (TypeSpec.isDifferentiableType(newSymbolHolder.typeSpec())) {
	    adEnv.toActiveTmpZones.tail =
                new TapIntList(newSymbolHolder.zone, adEnv.toActiveTmpZones.tail);
	}

        // Copy the activity annotation:
        if (expressionIsActive) {
            ADActivityAnalyzer.setAnnotatedActive(adEnv.curActivity(), newRef);
        }
        // TODO vmp revoir ce test: F77 lh06 resb on declare des variables differentiees inutilisees
        if (expressionIsActive || requiredInAdjoint) {
            newSymbolHolder.newVariableDecl().setActive();
        }
        Tree newAssign = ILUtils.build(ILLang.op_assign, ILUtils.copy(newRef), expression);
        // Copy the liveness annotation:
        if (requiredInAdjoint) {
            DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newAssign, modeIsJoint, false);
        }
        if (diffPtrRequiredInAdjoint) {
            DiffLivenessAnalyzer.setTreeRequiredInDiff(adEnv.curActivity(), newAssign, modeIsJoint, true);
        }

        // Add the temporary definition to the splitted instructions:
        toSplitTrees.placdl(newAssign);

	return ILUtils.copy(newRef) ;
    }

    private Tree differentiateInitDeclarator(WrapperTypeSpec diffTypeSpec, int differentiationMode,
                                             Tree diffDecl, SymbolTable diffSymbolTable, BoolVector beforeActiv,
                                             BoolVector afterActiv,
                                             BoolVector afterReqX, BoolVector beforeAvlX,
                                             Tree declarator, boolean declarationOnly) {
        Tree rhsDiff;
        if (adEnv.curUnit() == null || adEnv.curUnit().isPackage()
                || differentiationMode != DiffConstants.TANGENT_MODE && !adEnv.curUnitIsContext) {
            // For module variables, initialization must be done to the "zero" of the diff type.
            rhsDiff = diffTypeSpec.wrappedType.buildConstantZero();
        } else {
            // When the diff initialization expression may be non-trivial (non-zero).
            // attention s'il n'y a pas de split, il faut mettre a jour
            // beforeActiv afterActiv beforeUseful afterUse pour chaque i
            WrapperTypeSpec lhsTypeSpec = adEnv.curSymbolTable().typeOf(declarator.down(1));
            if (adEnv.traceCurBlock != 0) {
                TapEnv.printlnOnTrace("   (sublevel)-- differentiating initialization instruction: "
                        + ILUtils.toString(declarator, adEnv.curUnit().language(), adEnv.curActivity())
                        + " lhsTypeSpec:" + lhsTypeSpec + " " + beforeActiv + "=>" + afterActiv);
            }
            if (TypeSpec.isA(lhsTypeSpec, SymbolTableConstants.POINTERTYPE)) {
                TapPair<Tree, TapList<Tree>[]> diffTreePlus =
                        buildDiffOfControlAssignment(declarator, differentiationMode,
                                diffSymbolTable, beforeActiv, afterActiv,
                                beforeAvlX, afterReqX);
                Tree diffDeclTree = (diffTreePlus == null ? null : diffTreePlus.first) ;
                rhsDiff = (diffDeclTree == null ? null : ILUtils.copy(diffDeclTree.down(2))) ;
            } else {
                boolean passiveDeclarationInitialization = false;
                // [llh] 5/10/2015: here I'm trying to detect that initialization of diff declaration is
                // not necessary in e.g. C:lh19,lh42,v251, but so far it doesn't work because cf NOTE_init_diff.
                int iReplicSave = adEnv.iReplic ; // temporary during change for replica
                // Temporary iReplicSave needed because the following calls tangentDiffExpr(), which again iterates
                // "diffReplica" times. tangentDiffExpr() should not be called here, as adEnv.iReplic is already set.
                if (declarator.opCode() == ILLang.op_assign) {
                    Tree declaratorLhs = declarator.down(1);
                    int arrayCount = 0;
                    while (declaratorLhs.opCode() == ILLang.op_arrayDeclarator) {
                        declaratorLhs = declaratorLhs.down(1);
                        ++arrayCount;
                    }
                    declaratorLhs = ILUtils.copy(declaratorLhs);
                    while (arrayCount != 0) {
                        declaratorLhs =
                                ILUtils.build(ILLang.op_pointerAccess, declaratorLhs, ILUtils.build(ILLang.op_intCst, 0));
                        --arrayCount;
                    }
                    // This looks like a very expensive way just to check that a variable is passive ? :
                    Tree[] diffLhsTreeR = tangentDiffExpr(new ToObject<>(declaratorLhs), beforeActiv,
                            new TapPair<>(null, null), new TapPair<>(null, null));
                    // Case of declarator with initialization, but passive : real T[0:5] = {0.0,...}
                    // The diff declaration must not have an initialization assignment:
                    passiveDeclarationInitialization = (diffLhsTreeR==null || diffLhsTreeR.length==0 || diffLhsTreeR[0] == null) ;
                }
                if (passiveDeclarationInitialization || declarationOnly) {
                    rhsDiff = null;
                } else {
                    Tree declaratorExp = declarator;
                    if (declaratorExp.opCode() == ILLang.op_assign) {
                        declaratorExp = declaratorExp.down(2);
                    }
                    Tree[] rhsDiffR = tangentDiffExpr(new ToObject<>(declaratorExp), beforeActiv,
                                              new TapPair<>(null, null), new TapPair<>(null, null));
                    rhsDiff = rhsDiffR[0] ;
                    if (rhsDiff == null) {
                        rhsDiff = lhsTypeSpec.buildConstantZero();
                    }
                }
                adEnv.iReplic = iReplicSave ; // temporary during change for replica
            }
            if (adEnv.traceCurBlock != 0) {
                TapEnv.printlnOnTrace("   (sublevel)-- returns assignment to " + diffDecl + " := " + rhsDiff);
            }
        }
        // If there is no initialization tree, don't build an initialization assignment:
        return ILUtils.isNullOrNone(rhsDiff) ? null : ILUtils.build(ILLang.op_assign, diffDecl, rhsDiff);
    }

    /**
     * For association by address differentiation mode.
     *
     * @param instruction source Instruction.
     * @param srcBlock    source Block.
     * @param copyBlock   Block in the differentiated callGraph.
     * @param rank        of the instruction in the source Block.
     */
    protected void updateAACopiedInstruction(Instruction instruction, Block srcBlock, Block copyBlock, int rank) {
        if (!instruction.isDifferentiated) {
            Tree tree = instruction.tree;
            if (tree != null && (ILUtils.isADeclaration(tree) || tree.opCode() == ILLang.op_data)) {
                if (tree.opCode() != ILLang.op_common
                        && tree.opCode() != ILLang.op_useDecl // cf set11/v425
                        && tree.opCode() != ILLang.op_include
                        && tree.opCode() != ILLang.op_function) {
                    // not sure what to do on the 3 cases above...
                    // Declarations in the copied (i.e. non-diff) Unit must change the types of
                    // active vars to the AA type, to be consistent with the differentiated Units.
                    // We do this by differentiating the declaration in TANGENT_MODE
                    // (even if this is not the differentiationMode, as we are building the copied Unit anyway)
                    Instruction copyInstrInDiff = new Instruction(tree, null) ;
                    TapList<Instruction>[] diffDeclarationsR =
                            differentiateInstructionDeclaration(instruction, DiffConstants.TANGENT_MODE,
                                    copyInstrInDiff, null, tree, false,
                                    copyBlock.symbolTable, srcBlock,
                                    null, null, null, null,
                                    null, null, null, null,
                                    copyBlock.symbolTable.unit.language(),
                                    // See "Caution" comment on scrAction:
                                    !adEnv.curUnit().isFortran9x(), false);
                    TapList<Instruction> diffDeclarations = diffDeclarationsR[0] ; // temporary during change for replica
                    Tree diffDecl = null;
                    if (diffDeclarations != null) {
                        diffDecl = diffDeclarations.head.tree;
                        diffDeclarations = diffDeclarations.tail;
                        if (diffDeclarations != null) {
                            TapEnv.toolError("  Problem for updateAACopiedInstruction: remaining pieces of diff declaration are lost: " + diffDeclarations);
                        }
                    }
                    if (diffDecl != null) {
                        copyBlock.addInstructionAt(diffDecl, rank);
                    }
                }
            } else {
                instruction.tree = turnAssociationByAddressPrimal(tree, srcBlock.symbolTable,
                                                                  copyBlock.symbolTable, null, true);
            }
            instruction.isDifferentiated = true;
        }
    }

    protected void updateAAInstructionMask(Instruction instruction, SymbolTable srcSymbolTable, SymbolTable destSymbolTable) {
        InstructionMask srcMask = instruction.whereMask();
        if (srcMask != null && !srcMask.controlInstruction.isDifferentiated) {
            srcMask.setControlTree(turnAssociationByAddressPrimal(srcMask.getControlTree(), srcSymbolTable,
                                                                  destSymbolTable, null, true));
            srcMask.controlInstruction.isDifferentiated = true;
        }
    }

    /**
     * Should be temporary until we find a better organization.
     * This function fills the srcRefsRW TapPair with the lists of all
     * Trees (R)read and (W)written by the given instruction srcTree
     * This is done in the fashion used in tangent differentiation to
     * place the data dependencies into the fwdInstructionsGraph of the current fwdBlock.
     */
    private void fillRefsRW(Tree srcTree, TapPair<TapList<Tree>, TapList<Tree>> srcRefsRW) {
        srcRefsRW.first = null;
        srcRefsRW.second = null;
        int opCode = srcTree.opCode();
        String unaryOper = null;
        if (opCode == ILLang.op_unary) {
            unaryOper = ILUtils.getIdentString(srcTree.down(1));
        }
        if (opCode == ILLang.op_assign) {
            // This is a simple assignment, possibly containing intrinsic calls.
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree.down(1), srcRefsRW.first, false);
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree.down(2), srcRefsRW.first, true);
            srcRefsRW.second = new TapList<>(srcTree.down(1), null);
        } else if (ILUtils.isIORead(srcTree)) {
            // This is an IO call that reads from a file, thus overwriting variables:
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree.down(2), srcRefsRW.first, true);
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree.down(3), srcRefsRW.first, false);
            srcRefsRW.second = ILUtils.addTreeInList(srcTree.down(3), srcRefsRW.second);
        } else if (opCode == ILLang.op_unary && unaryOper != null &&
                (unaryOper.equals("++prefix") || unaryOper.equals("++postfix") ||
                        unaryOper.equals("--prefix") || unaryOper.equals("--postfix"))) {
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree.down(2), srcRefsRW.first, false);
            srcRefsRW.second = new TapList<>(srcTree.down(2), null);
        } else {
            srcRefsRW.first = ILUtils.usedVarsInExp(srcTree, srcRefsRW.first, true);
        }
    }

    private TapList<Tree> hasTBRexps(Tree expression, TapList<Tree> listTrees) {
        if (ADTBRAnalyzer.isTBR(adEnv.curActivity(), expression)) {
            listTrees = new TapList<>(expression, listTrees);
        } else if (!expression.isAtom()) {
            Tree[] subTrees = expression.children();
            for (int i = subTrees.length - 1; i >= 0; i--) {
                listTrees = hasTBRexps(subTrees[i], listTrees);
            }
        }
        return listTrees;
    }

    /**
     * @return true when this code initializes a "CONSTANT"
     * Nota: in particular, a CONSTANT has no zone
     * [llh] TODO: this should change.
     */
    private boolean isAConstantDeclInit(Tree action) {
        Tree[] assignments;
        if (action.opCode() == ILLang.op_blockStatement) {
            assignments = action.children();
        } else {
            assignments = new Tree[1];
            assignments[0] = action;
        }
        boolean constantFound = false;
        for (int i = assignments.length - 1; !constantFound && i >= 0; --i) {
            if (assignments[i].opCode() == ILLang.op_assign) {
                String varName = ILUtils.baseName(assignments[i].down(1));
                if (adEnv.curSymbolTable().getConstantDecl(varName) != null) {
                    constantFound = true;
                }
            }
        }
        return constantFound;
    }

    /**
     * @param diffSymbolTable The SymbolTable of the future location of the differentiated instructions.
     * @param declarationOnly When true, do not differentiate a possible initialization assignment.
     *                        Note: ideally, should always be called with declarationOnly==true, and this argument should go away!
     * @param forInterface    true when we are differentiating an INTERFACE declaration, in which case we don't want to differentiate
     *                        formal arguments that are only active inside the diff routine and therefore have no diff formal argument.
     * @return the (array of for replicas) list of differentiated instructions, in their desired final appearance order.
     */
    private TapList<Instruction>[] differentiateInstructionDeclaration(Instruction instruction, int differentiationMode,
                                       Instruction copyInstrInDiff, ToObject<TapList<Instruction>> toDiffDeclarationsForFwd,
                                       Tree tree, boolean allDiffModesInside, SymbolTable diffSymbolTable,
                                       Block block,
                                       BoolVector beforeActiv, BoolVector afterActiv,
                                       BoolVector beforeUseful, BoolVector afterUseful,
                                       BoolVector beforeReqX, BoolVector afterReqX,
                                       BoolVector beforeAvlX, BoolVector afterAvlX,
                                       int origLanguage, boolean declarationOnly, boolean forInterface) {
        if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace("          Differentiate declaration instruction " + tree);
        }
        TapList<Instruction>[] diffDeclarationsR = null ;
        TapList<Instruction> diffDeclarations ;
        Instruction instructionResult;
        if (!instruction.isDifferentiated) {
            boolean copyInstrIsDiffSpecific = false;
            switch (tree.opCode()) {
                case ILLang.op_varDeclaration:
                  diffDeclarationsR = new TapList[TapEnv.diffReplica()] ;
                  for (adEnv.iReplic=0 ; adEnv.iReplic<diffDeclarationsR.length ; ++adEnv.iReplic) {
                    TapList<Instruction> hdDiffDeclarations =  new TapList<>(null, null);
                    TapList<Instruction> tlDiffDeclarations = hdDiffDeclarations;
                    // Call successive differentiateVarDeclaration in this order:_D, (_B or _FWD), _BWD
                    // so that they appear in this same order in the final differentiated code:
                    if (allDiffModesInside) {
                        if (TapEnv.mustTangent()) {
                            adEnv.pushCurDiffSorts(DiffConstants.TANGENT, DiffConstants.TANGENT);
                            tlDiffDeclarations = differentiateVarDeclaration(tree, tlDiffDeclarations,
                                    null, differentiationMode, block,
                                    diffSymbolTable, instruction, copyInstrInDiff,
                                    beforeActiv, afterActiv, beforeUseful, afterUseful,
                                    beforeReqX, afterReqX, beforeAvlX, afterAvlX, DiffConstants.TANGENT,
                                    origLanguage, declarationOnly, forInterface);
                            adEnv.popCurDiffSorts();
                        }
                        if (TapEnv.mustAdjoint()) {
                            adEnv.pushCurDiffSorts(DiffConstants.ADJOINT, DiffConstants.ADJOINT);
                            int adjointModeSplit = getAdjointModeSplit(tree, diffSymbolTable);
                            tlDiffDeclarations = differentiateVarDeclaration(tree, tlDiffDeclarations,
                                    null, differentiationMode, block,
                                    diffSymbolTable, instruction, copyInstrInDiff,
                                    beforeActiv, afterActiv, beforeUseful, afterUseful,
                                    beforeReqX, afterReqX, beforeAvlX, afterAvlX, adjointModeSplit,
                                    origLanguage, declarationOnly, forInterface);
                            if (adjointModeSplit == DiffConstants.ADJOINT_FWD) {
                                tlDiffDeclarations = differentiateVarDeclaration(tree, tlDiffDeclarations,
                                        null, differentiationMode, block,
                                        diffSymbolTable, instruction, copyInstrInDiff,
                                        beforeActiv, afterActiv, beforeUseful, afterUseful,
                                        beforeReqX, afterReqX, beforeAvlX, afterAvlX, DiffConstants.ADJOINT_BWD,
                                        origLanguage, declarationOnly, forInterface);
                            }
                            adEnv.popCurDiffSorts();
                        }
                    } else {
                        int adjointModeSplit = getAdjointModeSplit(tree, diffSymbolTable);
                        tlDiffDeclarations = differentiateVarDeclaration(tree, tlDiffDeclarations,
                                toDiffDeclarationsForFwd, differentiationMode, block,
                                diffSymbolTable, instruction, copyInstrInDiff,
                                beforeActiv, afterActiv, beforeUseful, afterUseful,
                                beforeReqX, afterReqX, beforeAvlX, afterAvlX, adjointModeSplit,
                                origLanguage, declarationOnly, forInterface);
                        if (adjointModeSplit == DiffConstants.ADJOINT_FWD) {
                            tlDiffDeclarations = differentiateVarDeclaration(tree, tlDiffDeclarations,
                                    null, differentiationMode, block,
                                    diffSymbolTable, instruction, copyInstrInDiff,
                                    beforeActiv, afterActiv, beforeUseful, afterUseful,
                                    beforeReqX, afterReqX, beforeAvlX, afterAvlX, DiffConstants.ADJOINT_BWD,
                                    origLanguage, declarationOnly, forInterface);
                        }
                    }
                    hdDiffDeclarations = hdDiffDeclarations.tail;
                    if (hdDiffDeclarations != null) {
                        instruction.isDifferentiated = true;
                    }
                    copyInstrIsDiffSpecific = willBeNodiff(tree, adEnv.curSymbolTable());
                    diffDeclarationsR[adEnv.iReplic] = hdDiffDeclarations ;
                  }
                  adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                  break;
                case ILLang.op_accessDecl: {
                    Tree result;
                    if (adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace(" Differentiating accessDecl declaration");
                        TapEnv.printlnOnTrace("      " + ILUtils.toString(tree, adEnv.curActivity()));
                    }
                    if (tree.down(2).opCode() == ILLang.op_typeDeclaration) {
                        //[llh 14/05/2013] strange special case for accessDecl(, typeDeclaration()) :
                        // --> Because IL syntax sometimes places a op_typeDeclaration as 2nd child
                        //     of a op_accessDecl (This is ugly and MUST be changed!)
                        result = differentiateTypeDeclaration(diffSymbolTable, tree.down(2));
                        if (result != null) {
                            result = ILUtils.build(ILLang.op_accessDecl,
                                    ILUtils.copy(tree.down(1)), result);
                        }
                    } else {
                        if (ILUtils.isIdentOf(tree.down(1), new String[]{"in", "out", "inout", "const", "constant", "optional", "allocatable"}, false)) {
                            // Don't differentiate these, because they will be integretated in the main declaration of the differentiated object:
                            result = null;
                            if (TapEnv.associationByAddress()) {
                                copyInstrInDiff.tree = ILUtils.build(ILLang.op_none);
                            }
                        } else {
                            TapList<Tree> diffDecls =
                                diffDeclNames(tree, allDiffModesInside, diffSymbolTable, null);
                            Tree diffBind = tree.down(1);
                            if (diffBind.opCode() == ILLang.op_accessDecl
                                    && diffBind.down(1).opCode() == ILLang.op_ident
                                    && diffBind.down(1).stringValue().equals("bind")) {
                                diffBind = diffBindCVarName(diffBind);
                            } else {
                                diffBind = ILUtils.copy(diffBind);
                            }
                            result = (diffDecls==null
                                      ? null
                                      : ILUtils.build(ILLang.op_accessDecl,
                                          diffBind,
                                          ILUtils.build(tree.down(2).opCode(), diffDecls)));
                        }
                    }
                    if (result==null) {
                        diffDeclarations = null ;
                    } else {
                        instructionResult = new Instruction();
                        instructionResult.tree = result;
                        instructionResult.isDifferentiated = true;
                        diffDeclarations = new TapList<>(instructionResult, null);
                        instruction.isDifferentiated = true;
                    }
                    if (adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace("   -->");
                        TapEnv.printlnOnTrace("      " + ILUtils.toString(result, adEnv.curActivity()));
                    }
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    break;
                }
                case ILLang.op_include:
                    if (!TapEnv.isStdIncludeName(tree.stringValue())) {
                        Instruction.differentiateChainedIncludes(instruction, null, true);
                    }
                    diffDeclarationsR = new TapList[]{null} ;
                    break;
                case ILLang.op_data: {
                    Tree diffData1 = diffListDecls(tree.down(1), tree, diffSymbolTable, null, null, null);
                    if (diffData1 != null) {
                        // Following code is wrong if data contains variables of different types:
                        Tree firstVar = ILUtils.baseTree(tree.down(1).down(1));
                        VariableDecl varDecl = adEnv.curSymbolTable().getVariableDecl(ILUtils.baseName(firstVar));
                        WrapperTypeSpec typeSpec = varDecl.type();
                        Tree result = typeSpec.wrappedType.buildConstantZero();
                        int length = ILUtils.buildIterativeLength(diffData1, adEnv.curFwdSymbolTable);
                        if (length > 1) {
                            result = ILUtils.build(ILLang.op_mul,
                                    ILUtils.build(ILLang.op_intCst, length),
                                    result);
                        }
                        result = ILUtils.build(ILLang.op_data,
                                diffData1,
                                ILUtils.build(ILLang.op_expressions, result));
                        instructionResult = new Instruction(result);
                        instructionResult.isDifferentiated = true;
                        diffDeclarations = new TapList<>(instructionResult, null);
                        diffDeclarationsR = new TapList[]{diffDeclarations} ;
                        instruction.isDifferentiated = true;
                        if (TapEnv.associationByAddress()) {
                            tree = turnAssociationByAddressPrimal(tree, adEnv.curSymbolTable(),
                                                                  adEnv.curFwdSymbolTable, null, false);
                            copyInstrInDiff.tree = tree;
                        }
                    } else {
                        diffDeclarationsR = new TapList[]{null} ;
                    }
                    break;
                }
                case ILLang.op_interfaceDecl: {
                    boolean splitInterface = false;
                    TapList<Unit> differentiatedInterfaces = new TapList<>(null, null);
                    diffDeclarations = null ;
                    if (allDiffModesInside) {
                        if (TapEnv.mustAdjoint()) {
                            adEnv.pushCurDiffSorts(DiffConstants.ADJOINT, DiffConstants.ADJOINT);
                            splitInterface = mustDifferentiateInterfaceSplit(tree);
                            Tree result;
                            if (splitInterface) {
                                result = differentiateInterfaceDeclaration(tree, DiffConstants.ADJOINT_BWD,
                                        block, diffSymbolTable,
                                        copyInstrInDiff, differentiatedInterfaces);
                                if (result != null) {
                                    instructionResult = new Instruction();
                                    instructionResult.tree = result;
                                    instructionResult.isDifferentiated = true;
                                    diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                                }
                                result = differentiateInterfaceDeclaration(tree, DiffConstants.ADJOINT_FWD,
                                        block, diffSymbolTable,
                                        copyInstrInDiff, differentiatedInterfaces);
                                if (result != null) {
                                    instructionResult = new Instruction();
                                    instructionResult.tree = result;
                                    instructionResult.isDifferentiated = true;
                                    diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                                }
                            } else {
                                result = differentiateInterfaceDeclaration(tree, DiffConstants.ADJOINT,
                                        block, diffSymbolTable,
                                        copyInstrInDiff, differentiatedInterfaces);
                                if (result != null) {
                                    instructionResult = new Instruction();
                                    instructionResult.tree = result;
                                    instructionResult.isDifferentiated = true;
                                    diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                                }
                            }
                            adEnv.popCurDiffSorts();
                        }
                        if (TapEnv.mustTangent()) {
                            adEnv.pushCurDiffSorts(DiffConstants.TANGENT, DiffConstants.TANGENT);
                            Tree result = differentiateInterfaceDeclaration(tree, DiffConstants.TANGENT,
                                    block, diffSymbolTable,
                                    copyInstrInDiff, differentiatedInterfaces);
                            if (result != null) {
                                instructionResult = new Instruction();
                                instructionResult.tree = result;
                                instructionResult.isDifferentiated = true;
                                diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                            }
                            adEnv.popCurDiffSorts();
                        }
                    } else {
                        if (differentiationMode == DiffConstants.ADJOINT_MODE) {
                            splitInterface = mustDifferentiateInterfaceSplit(tree);
                        }
                        Tree result;
                        if (splitInterface) {
                            result = differentiateInterfaceDeclaration(tree, DiffConstants.ADJOINT_BWD,
                                    block, diffSymbolTable,
                                    copyInstrInDiff, differentiatedInterfaces);
                            if (result != null) {
                                instructionResult = new Instruction();
                                instructionResult.tree = result;
                                instructionResult.isDifferentiated = true;
                                diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                            }
                            result = differentiateInterfaceDeclaration(tree, DiffConstants.ADJOINT_FWD,
                                    block, diffSymbolTable,
                                    copyInstrInDiff, differentiatedInterfaces);
                            if (result != null) {
                                instructionResult = new Instruction();
                                instructionResult.tree = result;
                                instructionResult.isDifferentiated = true;
                                if (toDiffDeclarationsForFwd!=null) {
                                    toDiffDeclarationsForFwd.setObj(
                                            new TapList<Instruction>(instructionResult, toDiffDeclarationsForFwd.obj())) ;
                                } else {
                                    diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                                }
                            }
                        } else {
                            int diffMode;
                            if (differentiationMode == DiffConstants.TANGENT_MODE) {
                                diffMode = DiffConstants.TANGENT;
                            } else {
                                diffMode = DiffConstants.ADJOINT;
                            }
                            result = differentiateInterfaceDeclaration(tree, diffMode,
                                    block, diffSymbolTable,
                                    copyInstrInDiff, differentiatedInterfaces);
                            if (result != null) {
                                instructionResult = new Instruction();
                                instructionResult.tree = result;
                                instructionResult.isDifferentiated = true;
                                diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                            }
                        }
                    }
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    if (diffDeclarations != null) {
                        instruction.isDifferentiated = true;
                    }
                    // Because differentiation inside the interfaceDecl has modified the global curInstruction:
                    adEnv.setCurInstruction(instruction);
                    break;
                }
                case ILLang.op_useDecl: {
                    Tree result = differentiateUseDeclaration(ILUtils.copy(tree), diffSymbolTable);
                    instructionResult = new Instruction(result);
                    instructionResult.isDifferentiated = true;
                    instructionResult.preComments = instruction.preComments;
                    instructionResult.postComments = instruction.postComments;
                    instructionResult.preCommentsBlock = instruction.preCommentsBlock;
                    instructionResult.postCommentsBlock = instruction.postCommentsBlock;
                    diffDeclarations = new TapList<>(instructionResult, null);
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    break;
                }
                case ILLang.op_none:
                    diffDeclarationsR = new TapList[]{null} ;
                    break;
                case ILLang.op_common:
                case ILLang.op_equivalence:
                    if (TapEnv.associationByAddress()) {
                        diffDeclarationsR = new TapList[]{null} ;
                    } else {
                        diffDeclarationsR = new TapList[TapEnv.diffReplica()] ;
                        for (adEnv.iReplic=0 ; adEnv.iReplic<diffDeclarationsR.length ; ++adEnv.iReplic) {
                            diffDeclarations = null ;
                            Tree result = null;
                            TapList<Tree> toExtraDiffDeclarations = new TapList<>(null, null);
                            if (tree.opCode()==ILLang.op_common) {
                                result = differentiateCommonDeclaration(diffSymbolTable, tree, toExtraDiffDeclarations);
                            } else if (tree.opCode()==ILLang.op_equivalence) {
                                result = differentiateEquivalenceDeclaration(diffSymbolTable, tree);
                            }
                            while (toExtraDiffDeclarations.tail != null) {
                                instructionResult = new Instruction(toExtraDiffDeclarations.tail.head);
                                instructionResult.isDifferentiated = true;
                                instruction.isDifferentiated = true;
                                diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                                toExtraDiffDeclarations = toExtraDiffDeclarations.tail;
                            }
                            if (result != null) {
                                instructionResult = new Instruction(result);
                                instructionResult.isDifferentiated = true;
                                instruction.isDifferentiated = true;
                                diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                            }
                            diffDeclarationsR[adEnv.iReplic] = diffDeclarations ;
                        }
                        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                    }
                    break ;
                case ILLang.op_save:
                case ILLang.op_varDimDeclaration: {
                    //[llh] These cases may need to be made more specific :
                    if (TapEnv.associationByAddress()) {
                        diffDeclarationsR = new TapList[]{null} ;
                    } else {
                        TapList<Tree> diffDecls = diffDeclNames(tree, allDiffModesInside, diffSymbolTable, null);
                        Tree result = (diffDecls == null ? null : ILUtils.build(tree.opCode(), diffDecls));
                        diffDeclarations = null ;
                        if (result != null) {
                            instructionResult = new Instruction(result);
                            instructionResult.isDifferentiated = true;
                            instruction.isDifferentiated = true;
                            diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                        }
                        diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    }
                    break ;
                }
                case ILLang.op_typeDeclaration: {
                    Tree result = differentiateTypeDeclaration(diffSymbolTable, tree);
                    diffDeclarations = null ;
                    if (result != null) {
                        instructionResult = new Instruction(result);
                        instructionResult.isDifferentiated = true;
                        instruction.isDifferentiated = true;
                        diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                    }
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    break ;
                }
                case ILLang.op_external:
                case ILLang.op_intrinsic: {
                    ToObject<TapList<Tree>> toDiffDeclaratorsForFwd =
                        (toDiffDeclarationsForFwd==null ? null : new ToObject<>(null)) ;
                    TapList<Tree> diffDeclarators =
                        diffDeclNames(tree, allDiffModesInside, diffSymbolTable, toDiffDeclaratorsForFwd);
                    Tree result = (diffDeclarators == null ? null : ILUtils.build(ILLang.op_external, diffDeclarators));
                    diffDeclarations = null ;
                    if (result != null) {
                        instructionResult = new Instruction(result);
                        instructionResult.isDifferentiated = true;
                        instruction.isDifferentiated = true;
                        diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                    }
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    if (toDiffDeclaratorsForFwd!=null) {
                        diffDeclarators = toDiffDeclaratorsForFwd.obj() ;
                        if (diffDeclarators!=null) {
                            result = ILUtils.build(ILLang.op_external, diffDeclarators);
                            instructionResult = new Instruction(result);
                            instructionResult.isDifferentiated = true;
                            instruction.isDifferentiated = true;
                            toDiffDeclarationsForFwd.setObj(
                                    new TapList<Instruction>(instructionResult, toDiffDeclarationsForFwd.obj())) ;
                        }
                    }
                    break ;
                }
                case ILLang.op_constDeclaration:
                case ILLang.op_nameList: {
                    //[llh] TODO: why are these cases not treated here?
                    diffDeclarationsR = new TapList[]{null} ;
                    break ;
                }
                default: {
                    TapEnv.toolWarning(-1, "(Differentiation of declaration) Unexpected operator: " + tree.opName());
                    // Approximative fallback. Produced code is probably wrong:
                    TapList<Tree> diffDecls = diffDeclNames(tree, allDiffModesInside, diffSymbolTable, null);
                    Tree result = (diffDecls == null ? null : ILUtils.build(tree.opCode(), diffDecls));
                    diffDeclarations = null ;
                    if (result != null) {
                        instructionResult = new Instruction(result);
                        instructionResult.isDifferentiated = true;
                        instruction.isDifferentiated = true;
                        diffDeclarations = new TapList<>(instructionResult, diffDeclarations);
                    }
                    diffDeclarationsR = new TapList[]{diffDeclarations} ;
                    break ;
                }
            }
            if (instruction.fromInclude() != null) {
                if (instruction.tree != null && instruction.tree.opCode() == ILLang.op_include
                        && instruction.fromInclude().tree == null) {
                    // This include has been inlined, e.g. because it contains a Fortran IMPLICIT declaration...
                    if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                        TapEnv.printlnOnTrace("          include was inlined!");
                    }
                    copyInstrInDiff.tree = null;
                } else {
                    instruction.fromInclude().setIncludeOfDiffInstructions(copyInstrInDiff,
                            copyInstrIsDiffSpecific, diffDeclarationsR,
                            suffixes()[curDiffUnitSort()][DiffConstants.PROC]);
                }
            }
        } else {
            diffDeclarationsR = new TapList[]{null} ;
        }
        if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
            if (diffDeclarationsR.length>0) {
                TapList<Instruction> inDiffDeclarations = diffDeclarationsR[0];
                while (inDiffDeclarations != null) {
                    TapEnv.printlnOnTrace("          --> differentiated decl: " + inDiffDeclarations.head.tree
                                          +(diffDeclarationsR.length==1 ? "" : " *"+diffDeclarationsR.length+"replicas")) ;
                    inDiffDeclarations = inDiffDeclarations.tail;
                }
            }
            if (toDiffDeclarationsForFwd!=null) {
                TapList<Instruction> inDiffDeclarations = toDiffDeclarationsForFwd.obj() ;
                while (inDiffDeclarations != null) {
                    TapEnv.printlnOnTrace("          --> differentiated decl for FWD sweep: "+inDiffDeclarations.head.tree) ;
                    inDiffDeclarations = inDiffDeclarations.tail;
                }
            }
            TapEnv.printlnOnTrace("          --> copied  source decl: " + copyInstrInDiff.tree);
        }
        return diffDeclarationsR;
    }

    private int getAdjointModeSplit(Tree tree, SymbolTable diffSymbolTable) {
        //[llh 04Jan2021] Method overly complex, apparently only to detect that
        // the procedure declared by declarator "tree" is differentiated in split-adjoint mode ?
        int adjointModeSplit = 0;
        if (TapEnv.mustAdjoint() && tree.down(3).length() != 0) {
            String symbolName = ILUtils.baseName(tree.down(3).down(1));
            SymbolDecl symbolDecl = diffSymbolTable.getSymbolDecl(symbolName);
            boolean seenHereAsAFunction = (symbolDecl != null && symbolDecl.isA(SymbolTableConstants.FUNCTION));
            if (seenHereAsAFunction) {
                TapList<FunctionDecl> funDecls = diffSymbolTable.getFunctionDecl(symbolName, null, null, false);
                FunctionDecl funDecl = (funDecls == null ? null : funDecls.head);
                if (funDecl != null) {
                    Unit declaredUnit = funDecl.unit();
                    if (declaredUnit.origUnit != null && declaredUnit.origUnit != declaredUnit) {
                        declaredUnit = declaredUnit.origUnit;
                    }
                    if (declaredUnit.mustDifferentiateSplit()) {
                        adjointModeSplit = DiffConstants.ADJOINT_FWD;
                    }
                }
            }
        }
        return adjointModeSplit;
    }

    /**
     * Returns true if the source tree will be declared as a new
     * (differentiation-specific) object in the differentiated declaration,
     * i.e. as "foo_nodiff" instead as a ref to the source "foo".
     * Triggers only on function declarations
     */
    private boolean willBeNodiff(Tree tree, SymbolTable sourceSymbolTable) {
        boolean result = false;
        Tree[] declarators = tree.down(3).children();
        Tree declarator;
        for (int i = declarators.length - 1; i >= 0 && !result; --i) {
            declarator = declarators[i];
            if (declarator != null) {
                if (declarator.opCode() == ILLang.op_assign) {
                    declarator = declarator.down(1);
                }
                String funcName = ILUtils.baseName(declarator);
                TapList<FunctionDecl> funcDecls = sourceSymbolTable.getFunctionDecl(funcName, null, null, false);
                FunctionDecl funcDecl = (funcDecls == null ? null : funcDecls.head);
                if (funcDecl != null && funcDecl.unit() != null && funcDecl.unit().isStandard()
                        && callGraphDifferentiator().unitsHavePrimal.retrieve(funcDecl.unit())) {
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * Adds elements to the list of differentiated instructions for the given (variable declaration) "tree".
     * Adds the elements corresponding to the given "differentiationMode" and "adjointSplitMode".
     * Adds those elements at the tail of newDeclarations, which is the tail of some list that collects the
     * differentiated instructions, so that the list itself is ordered in the final, desired appearance order.
     *
     * @param newDeclarations the tail (i.e the last cell) of a list designed to collect the differentiated instructions.
     * @param toDiffDeclarationsForFwd when given non-null, collects the diff declarations
     *  that must go to a separate declaration scope for the Fwd sweep.
     * @return the new tail of this list of differentiated instructions.
     */
    private TapList<Instruction> differentiateVarDeclaration(Tree tree, TapList<Instruction> newDeclarations,
                                                             ToObject<TapList<Instruction>> toDiffDeclarationsForFwd,
                                                             int differentiationMode, Block block,
                                                             SymbolTable diffSymbolTable, Instruction instruction,
                                                             Instruction copyInstrInDiff,
                                                             BoolVector beforeActiv, BoolVector afterActiv,
                                                             BoolVector beforeUseful, BoolVector afterUseful,
                                                             BoolVector beforeReqX, BoolVector afterReqX,
                                                             BoolVector beforeAvlX, BoolVector afterAvlX,
                                                             int adjointSplitMode, int origLanguage,
                                                             boolean declarationOnly, boolean forInterface) {
        Instruction diffInstruction;
        TapList<Instruction> toNewPrimalDeclarations = new TapList<>(null, null) ;
        Tree[] declarators = tree.down(3).children();
        TapList<Tree> hdSimpleDeclarators = new TapList<>(null, null);
        TapList<Tree> tlSimpleDeclarators = hdSimpleDeclarators;
        Tree declarator, splitTree;
        String varName;
        SymbolDecl varDecl;
        // Detect the declarators that have other declaration parts and differentiate them  separately:
        for (Tree value : declarators) {
            declarator = ILUtils.copy(value);
            varName = (declarator == null ? null : ILUtils.baseName(declarator)) ;
            varDecl = (varName == null ? null : diffSymbolTable.getVariableOrConstantDecl(varName)) ;
            if (varDecl != null && varDecl.isATrueSymbolDecl && varDecl.hasOtherCombinableDeclarationPart()) {
                splitTree = ILUtils.build(ILLang.op_varDeclaration,
                        ILUtils.copy(tree.down(1)),
                        ILUtils.copy(tree.down(2)),
                        ILUtils.build(ILLang.op_declarators, declarator));
                diffInstruction =
                        differentiateVarDeclarationOne(splitTree, differentiationMode, block,
                                diffSymbolTable, instruction, copyInstrInDiff, toNewPrimalDeclarations,
                                toDiffDeclarationsForFwd,
                                beforeActiv, afterActiv, afterReqX, beforeAvlX,
                                adjointSplitMode, origLanguage, declarationOnly, forInterface);
                while (toNewPrimalDeclarations.tail != null) {
                    newDeclarations = newDeclarations.placdl(toNewPrimalDeclarations.tail.head) ;
                    toNewPrimalDeclarations.tail = toNewPrimalDeclarations.tail.tail ;
                }
                if (diffInstruction != null) {
                    newDeclarations = newDeclarations.placdl(diffInstruction);
                }
            } else {
                tlSimpleDeclarators = tlSimpleDeclarators.placdl(declarator);
            }
        }
        // Differentiate the "simple declarators" remainder of original declaration:
        if (hdSimpleDeclarators.tail != null) {
            splitTree = ILUtils.build(ILLang.op_varDeclaration,
                    ILUtils.copy(tree.down(1)),
                    ILUtils.copy(tree.down(2)),
                    ILUtils.build(ILLang.op_declarators, hdSimpleDeclarators.tail));
            diffInstruction =
                    differentiateVarDeclarationOne(splitTree, differentiationMode, block,
                            diffSymbolTable, instruction, copyInstrInDiff, toNewPrimalDeclarations,
                            toDiffDeclarationsForFwd,
                            beforeActiv, afterActiv, afterReqX, beforeAvlX,
                            adjointSplitMode, origLanguage, declarationOnly, forInterface);
            while (toNewPrimalDeclarations.tail != null) {
                newDeclarations = newDeclarations.placdl(toNewPrimalDeclarations.tail.head) ;
                toNewPrimalDeclarations.tail = toNewPrimalDeclarations.tail.tail ;
            }
            if (diffInstruction != null) {
                newDeclarations = newDeclarations.placdl(diffInstruction);
            }
        }
        return newDeclarations;
    }

    /**
     * Utility class for differentiation of a varDeclaration. Just a record.
     */
    private final class AccessCheck {
        VariableDecl decl;
        VariableDecl primalDecl;
        boolean isLocalDeclared;
        NewSymbolHolder newSH;

        private AccessCheck(VariableDecl decl, VariableDecl primalDecl, boolean isLocalDeclared, NewSymbolHolder newSH) {
            this.decl = decl;
            this.primalDecl = primalDecl;
            this.isLocalDeclared = isLocalDeclared;
            this.newSH = newSH;
        }
    }

    /**
     * @param toDiffDeclarationsForFwd when given non-null, collects the diff declarations
     *  that must go to a separate declaration scope for the Fwd sweep (e.g. FOO_FWD).
     */
    private Instruction differentiateVarDeclarationOne(Tree tree, int differentiationMode, Block block,
                                                       SymbolTable diffSymbolTable, Instruction instruction,
                                                       Instruction copyInstrInDiff,
                                                       TapList<Instruction> toNewPrimalDeclarations,
                                                       ToObject<TapList<Instruction>> toDiffDeclarationsForFwd,
                                                       BoolVector beforeActiv, BoolVector afterActiv,
                                                       BoolVector afterReqX,
                                                       BoolVector beforeAvlX, int adjointSplitMode,
                                                       int origLanguage, boolean declarationOnly, boolean forInterface) {
        boolean traceHere = false;
// traceHere = true ;
// traceHere = traceHere ||
//     (tree.down(3).opCode()==ILLang.op_declarators
//      && tree.down(3).down(1)!=null
//      && tree.down(3).down(1).opCode()==ILLang.op_functionDeclarator
//      && tree.down(3).down(1).down(1).opCode()==ILLang.op_ident
//      && ("a".equals(tree.down(3).down(1).down(1).stringValue())
//          || "b".equals(tree.down(3).down(1).down(1).stringValue())));
// traceHere = traceHere ||
//     (tree.down(3).opCode()==ILLang.op_declarators
//      && tree.down(3).down(2)!=null
//      && "c2".equals(ILUtils.baseName(tree.down(3).down(2)))) ;
// if (traceHere) {System.out.println(); System.out.println("DIFFERENTIATEVARDECLARATIONONE "+tree+" curDiffUnitSort:"+curDiffUnitSort()+" adjointSplitMode:"+adjointSplitMode+" curActivity pattern:"+adEnv.curActivity());}
// // if (differentiationMode!=TANGENT_MODE) PBaseTrack.experimentVarDeclaration(tree, block.symbolTable)  ;
        Instruction instructionResult = new Instruction(ILUtils.build(ILLang.op_none));
        Tree mainTypeTree = tree.down(2);
        TypeSpec mainType = TypeSpec.build(mainTypeTree, block.symbolTable, instruction,
                new TapList<>(null, null), new TapList<>(null, null),
                new TapList<>(null, null), new ToBool(false), null);
        boolean mainTypeIsArray = mainType.containsArray();
        Tree declarators = tree.down(3);
        int nbDecls = declarators.length();
        Tree declarator, declaratorNoAssign;
        WrapperTypeSpec typeSpec = new WrapperTypeSpec(null);
        WrapperTypeSpec diffTypeSpec = null;
        WrapperTypeSpec resultDiffTypeSpec = null;
        WrapperTypeSpec prevDiffTypeSpec = null;
        String varName;
        VariableDecl varDecl;
        FunctionDecl funcDecl;
        FunctionDecl basisFuncDecl;
        TapList<FunctionDecl> funcDecls;
        TapList<FunctionDecl> basisFuncDecls;
        Unit funcDeclUnit;
        NewSymbolHolder newSH = null;
        boolean hasDiffNames;
        Tree proposedDiffTypeTree = null;
        TapList<Tree> hdNewDeclarators = new TapList<>(null, null);
        TapList<Tree> tlNewDeclarators = hdNewDeclarators;
        TapList<Tree> toRemoveDecls = null;
        Tree[] modifiers = null;
        boolean containsArrayDeclarator = false;
        boolean[] containsArrayDeclaratorS = new boolean[nbDecls];
        boolean containsPointerDeclarator = false;
        boolean containsSizedDeclarator = false;
        boolean containsDimDeclaration = false;
        // true pour une declaration en 2 parties avec un varDimDeclaration
        boolean[] containsDimDeclarationS = new boolean[nbDecls];
        // true pour une declaration en 2 parties avec un common contenant des arrayDeclarator
        boolean[] needDiffArrayDeclarationS = new boolean[nbDecls];
        VariableDecl[] varDecls = new VariableDecl[nbDecls];
        SymbolDecl diffSymbolDecl = null;
        boolean[] isDiffLocalDeclared = new boolean[nbDecls];
        WrapperTypeSpec[] diffTypeSpecs = new WrapperTypeSpec[nbDecls];
        TapList<AccessCheck> origAccessChecks = null, diffAccessChecks = null;
        Tree typeSpecTree = null;
        Tree varTree;
        WrapperTypeSpec funcDeclDiffReturnType = null;
        boolean runAgain = false;
        boolean[] formalArgsPointerActivity = null;
        if (adEnv.curUnit() != null && adEnv.curUnit().functionTypeSpec() != null) {
            if (adEnv.curActivity() != null) {
                formalArgsPointerActivity = ReqExplicit.formalArgsPointerActivity(adEnv.curActivity(),
                        adEnv.curUnit().functionTypeSpec().argumentsTypes.length);
            } else {
                formalArgsPointerActivity = new boolean[adEnv.curUnit().functionTypeSpec().argumentsTypes.length + 1];
                for (int i = formalArgsPointerActivity.length - 1; i >= 0; --i) {
                    formalArgsPointerActivity[i] = false;
                }
            }
        }
        boolean[] formalArgsActivity = null;
        if (adEnv.curUnit() != null
                && adEnv.curUnit().functionTypeSpec() != null && adEnv.curActivity() != null) {

            formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(adEnv.curActivity());
        }
        TapList<Tree> declaratorsForFwd = null ;
        for (int i = 1; i <= declarators.length(); i++) {
            declarator = declarators.down(i);
            if (declarator != null) {
                declaratorNoAssign = declarator;
                if (declarator.opCode() == ILLang.op_assign) {
                    declaratorNoAssign = declarator.down(1);
                }
                varName = ILUtils.baseName(declaratorNoAssign);
                varTree = ILUtils.build(ILLang.op_ident, varName);
                varDecl = diffSymbolTable.getVariableOrConstantDecl(varName);
                if (varDecl != null && !varDecl.isATrueSymbolDecl) {
                    varDecl = null;
                }
                containsDimDeclarationS[i - 1] = false;
                needDiffArrayDeclarationS[i - 1] = false;
                if ((varDecl == null || varDecl.isActiveSymbolDecl()) &&
                        declaratorNoAssign.opCode() == ILLang.op_arrayDeclarator) {
                    containsArrayDeclaratorS[i - 1] = true;
                    containsArrayDeclarator = true;
                }
                if (declaratorNoAssign.opCode() == ILLang.op_sizedDeclarator) {
                    containsSizedDeclarator = true;
                }
                if (declaratorNoAssign.opCode() == ILLang.op_pointerDeclarator) {
                    containsPointerDeclarator = true;
                }
                funcDecls = diffSymbolTable.getFunctionDecl(varName, null, null, false);
                if (funcDecls != null) {
                    funcDecl = funcDecls.head;
                } else {
                    funcDecl = null;
                }
                basisFuncDecls = diffSymbolTable.basisSymbolTable().getFunctionDecl(varName, null, null, false);
                if (basisFuncDecls != null) {
                    basisFuncDecl = basisFuncDecls.head;
                } else {
                    basisFuncDecl = null;
                }
                if (funcDecl != null && funcDecl.unit().isExternal() && basisFuncDecl.unit() != funcDecl.unit()) {
                    // dans le cas d'ExternalUnit utilise'e avant leur definition, le funcDecl dans diffSymbolTable
                    // est encore associe' a l'ExternalUnit:
                    TapEnv.toolError("Removing local external declaration of "+funcDecl.unit()
                                     +" in "+adEnv.curUnit()+" in "+diffSymbolTable.shortName());
                    diffSymbolTable.removeDecl(funcDecl.symbol, SymbolTableConstants.FUNCTION, true);
                    funcDecl = basisFuncDecl;
                }
                if (mixedLanguage(funcDecl)
                        && funcDecl.importedFrom != null
                        && funcDecl.importedFrom.first.hasDiffSymbolHolders()) {
                    newSH = funcDecl.importedFrom.first.getDiffSymbolHolder(curDiffUnitSort(), null, 0);
                }
                if (funcDecl == null) {
                    funcDeclUnit = null;
                } else {
                    funcDeclUnit = funcDecl.unit();
                    if (funcDeclUnit.origUnit != null) {
                        funcDeclUnit = funcDeclUnit.origUnit;
                    }
                }
                if (declaratorNoAssign.opCode() == ILLang.op_functionDeclarator) {
                    varDecl = null;
                    if (funcDecl != null) {
                        typeSpec = funcDeclUnit.functionTypeSpec().returnType;
                    }
                }
                containsDimDeclarationS[i - 1] =
                        varDecl != null
                                && (varDecl.hasInstructionWithRootOperator(ILLang.op_varDimDeclaration, null)
                                || containsOpArrayDeclarator(varDecl, ILLang.op_accessDecl));
                if (containsDimDeclarationS[i - 1] && varDecl.isActiveSymbolDecl()) {
                    containsDimDeclaration = true;
                }
                if (newSH == null) {
                    newSH = NewSymbolHolder.getNewSymbolHolder(declarator);
                }
                hasDiffNames = true;
                if (newSH != null && newSH.derivationFrom == null) {
                    varDecl = newSH.newVariableDecl();
                    hasDiffNames = varDecl.hasDiffSymbolHolders();
                    if (differentiationMode == DiffConstants.ADJOINT_FWD) {
                        hasDiffNames = formalArgsPointerActivity[varDecl.formalArgRankInOrigUnit - 1];
                    }
                    funcDecl = newSH.newFunctionDecl();
                    newSH.setInstruction(instructionResult);
                    varDecl.setInstruction(instructionResult);

                }
                // If we are differentiating an interface declaration, and if this variable's diff
                // does not appear in the formal args of the diff interface decl,
                // then don't differentiate its declaration:
                if (forInterface && hasDiffNames && formalArgsActivity != null
                        && varDecl != null && varDecl.formalArgRankInOrigUnit > 0
                        && varDecl.formalArgRankInOrigUnit < formalArgsActivity.length
                        && !formalArgsActivity[varDecl.formalArgRankInOrigUnit - 1]) {
                    hasDiffNames = false;
                }

                funcDeclDiffReturnType = null;
                if (funcDecl != null) {
                    UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(funcDeclUnit);
                    if (diffInfo != null) {
                        if (adjointSplitMode == DiffConstants.ADJOINT_FWD) {
                            funcDeclDiffReturnType = diffInfo.getDiffReturnType(adjointSplitMode);
                        } else {
                            funcDeclDiffReturnType = diffInfo.getDiffReturnType(curDiffUnitSort());
                        }
                    }
                    if (funcDeclDiffReturnType == null && declarator.opCode() != ILLang.op_functionDeclarator) {
                        funcDecl = null;
                    }
                    if (mainTypeTree.opCode() == ILLang.op_modifiedType /*&& funcDecl!=null*/) {
// [Duplic FunctionDecl mode:]
//                         if (funcDeclUnit.isC() || (curUnit() == null || curUnit().isC())) {
                        modifiers = mainTypeTree.down(1).children();
//                         }
// [Unique FunctionDecl mode:]
//                        modifiers = mainTypeTree.down(1).children();
//                        //in C, propagate modifiers. In F, forget them except external&intrinsic
//                        if (!funcDeclUnit.isC()) {
//                            Tree externalOrIntrinsic = getExternalOrIntrinsic(modifiers) ;
//                            if (externalOrIntrinsic==null)
//                                modifiers = null ;
//                            else {
//                                modifiers = new Tree[1] ;
//                                modifiers[0] = externalOrIntrinsic ;
//                            }
//                        }
// [End FunctionDecl mode.]
                    }
                }
                if ((varDecl != null && varDecl.isActiveSymbolDecl() && hasDiffNames)
                    || funcDeclDiffReturnType != null) {
                    boolean containsDeclaratorOrVarDimDecl =
                            containsArrayDeclaratorS[i - 1] || containsDimDeclaration || containsPointerDeclarator;
                    boolean localMultiDirMode =
                            multiDirMode() && !containsDeclaratorOrVarDimDecl && funcDecl == null;
                    boolean throughPointer = false;
                    isDiffLocalDeclared[i - 1] = false;
                    if (varDecl != null) {
                        typeSpec = varDecl.type();
                        // If varDecl is an array, but the current declarator is not an arrayDeclarator,
                        // then the diff declarator must not declare an array either, because this
                        // is left to the diff of the other declaration of varDecl that makes it an array.
// if (traceHere) System.out.println("VARDECL:"+varDecl+" TYPESPEC:"+typeSpec+" declarator:"+declarator+" mainTypeIsArray:"+mainTypeIsArray) ;
                        if (typeSpec.containsArray() && declarator.opCode() != ILLang.op_arrayDeclarator
                                && !mainTypeIsArray) {
                            typeSpec = (WrapperTypeSpec) typeSpec.peelDimensionsTo(typeSpec, typeSpec, new TapList<>(null, null));
                            localMultiDirMode = false;
                        }
                        // If the root type of varDecl comes from an implicit,
                        //  the decorations around the diff type will be defined by
                        //  the diff of the "other parts" of the declaration of varDecl.
                        // Therefore the present differentiated declaration must only declare
                        //  the diff of the part of the type that comes from the implicit:
                        if (varDecl.fromImplicitType() != null) {
                            typeSpec = (WrapperTypeSpec) varDecl.fromImplicitType();
                        }
                        if (adEnv.curUnit() != null && origLanguage != adEnv.curUnit().language()) {
                            TypeDecl otherTypeDecl = adEnv.diffCallGraph().getOtherBindType(typeSpec);
                            if (otherTypeDecl != null) {
                                typeSpec = otherTypeDecl.typeSpec;
                            } else if (adEnv.curUnit().isC()) {
                                // differentiation d'une interfaceDecl de Unit C dans un block Fortran:
                                if (TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                                    typeSpec = ((PointerTypeSpec) typeSpec.wrappedType).destinationType;
                                }
                                if (mainTypeTree.opCode() == ILLang.op_modifiedType) {
                                    modifiers = mainTypeTree.down(1).children();
                                }
                            }
                        }
                        isDiffLocalDeclared[i - 1] = varRefDifferentiator().paramHasOnlyLocalDiff(adEnv.curActivity(), varTree, varDecl);
                    } else if (funcDecl != null) {
                        typeSpec = funcDecl.returnTypeSpec();
                        if (funcDecl.fromImplicitType() != null) {
                            typeSpec = (WrapperTypeSpec) funcDecl.fromImplicitType();
                        }
                    }
                    if (containsDeclaratorOrVarDimDecl) {
                        while (TypeSpec.isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
                            typeSpec = ((PointerTypeSpec) typeSpec.wrappedType).destinationType;
                            throughPointer = true;
                        }
                    }
                    if (containsDeclaratorOrVarDimDecl) {
                        while (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            typeSpec = typeSpec.wrappedType.elementType();
                        }
                    }
                    if (containsSizedDeclarator) {
                        if (TypeSpec.isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                            typeSpec = typeSpec.wrappedType.elementType();
                        }
                    }
// This modif matters on set12/cmplxstep01-2: removes wrong messages "Source code uses COMPLEX",
//  but forgets to diff to REAL8_DIFF on set01/lh043 ... TODO: solve this!
//                     if (varDecl!=null) { // Don't diff the return type of funcDecl: it is already diff'ed.
                        diffTypeSpec = typeSpec.differentiateTypeSpecMemo(diffSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                            suffixes()[curDiffUnitSort()][DiffConstants.TYPE],
                            !throughPointer && isDiffLocalDeclared[i - 1], localMultiDirMode,
                            varRefDifferentiator().getMultiDirDimensionMax(),
                            varName, varName, varTree, null);
//                     } else {
//                         diffTypeSpec = typeSpec;
//                     }
// if (traceHere) System.out.println(" varDecl:"+varDecl+" TS:"+typeSpec+" DTS:"+diffTypeSpec) ;
                    if (diffTypeSpec == null) {
                        diffTypeSpec = typeSpec;
                        if (TapEnv.relatedLanguageIsFortran()
                            && throughPointer && varDecl != null
                            && !varDecl.hasInstructionWithRootOperator(ILLang.op_accessDecl, "allocatable")
                            && !varDecl.hasModifier("allocatable")) {
                            diffTypeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
                        }
                    }
                    if (mainTypeTree.opCode() == ILLang.op_modifiedType) {
                        if (varDecl != null && varDecl.extraInfo() != null && diffTypeSpec != null ||
                                varDecl == null && funcDecl != null && funcDeclDiffReturnType == null ||
                                TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE) ||
                                ILUtils.getBindCValue(tree, i) != null ||
                                ILUtils.containsModifier("extern", instruction.tree, true)) {
                            modifiers = mainTypeTree.down(1).children();
                        }
                    }
                    if (varDecl != null && typeSpec != varDecl.type()) {
                        resultDiffTypeSpec = diffTypeSpec;
                        if (diffTypeSpec == null) {
                            resultDiffTypeSpec = typeSpec;
                            proposedDiffTypeTree = mainTypeTree;
                        }
                    }
                    if (prevDiffTypeSpec != null && !diffTypeSpec.equalsCompilIndep(prevDiffTypeSpec)) {
                        if (TypeSpec.isA(diffTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            for (int j = 0; j < i; j++) {
                                needDiffArrayDeclarationS[j] = true;
                            }
                            if (TypeSpec.isA(prevDiffTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                prevDiffTypeSpec = prevDiffTypeSpec.wrappedType.elementType();
                                diffTypeSpec = diffTypeSpec.wrappedType.elementType();
                            }
                            resultDiffTypeSpec = prevDiffTypeSpec;
                        } else if (TypeSpec.isA(prevDiffTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            runAgain = true;
                        }
                    } else {
                        prevDiffTypeSpec = diffTypeSpec;
                    }
                    // In Fortran, when a function returns an array, do not declare its dimensions:
                    if (varDecl == null && funcDecl != null && funcDecl.unit().isFortran()
                            && TypeSpec.isA(diffTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                        resultDiffTypeSpec = diffTypeSpec.wrappedType.elementType();
                    }
                }
                //[llh] temporary: signal those local variables that must be registered for ADMM:
                if (varDecl != null) {
                    TapIntList lzones = ZoneInfo.listAllZones(varDecl.zones(), false);
                    ZoneInfo zoneInfo;
                    TapList<String> toRegister = null;
                    TapList<String> toRegisterDiff = null;
                    while (lzones != null) {
                        zoneInfo = block.symbolTable.declaredZoneInfo(lzones.head, SymbolTableConstants.ALLKIND);
                        if (zoneInfo != null) {
                            if (zoneInfo.relocated || zoneInfo.rebased) {
                                toRegister = new TapList<>(ILUtils.toString(zoneInfo.accessTree), toRegister);
                            }
                            if (zoneInfo.relocatedDiff || zoneInfo.rebasedDiff) {
                                toRegisterDiff = new TapList<>(ILUtils.toString(zoneInfo.accessTree), toRegisterDiff);
                            }
                        }
                        lzones = lzones.tail;
                    }
                    if (toRegister != null) {
                        TapEnv.toolError("  Problem in " + adEnv.curUnit().name() + ": local variable " + varDecl.symbol + " must be ADMM_register'ed due to " + toRegister);
                    }
                    if (toRegisterDiff != null) {
                        TapEnv.toolError("  Problem in " + adEnv.curUnit().name() + ": diff of local variable " + varDecl.symbol + " must be ADMM_register'ed due to " + toRegisterDiff);
                    }
                }
                varDecls[i - 1] = varDecl;
                diffTypeSpecs[i - 1] = diffTypeSpec;
            }
        }
// if (traceHere) System.out.println(" RUNAGAIN:"+runAgain) ;
        if (runAgain) {
            for (int i = 1; i <= declarators.length(); i++) {
                declarator = declarators.down(i);
                if (declarator != null) {
                    if (varDecls[i - 1] != null && varDecls[i - 1].isActiveSymbolDecl()) {
                        if (diffTypeSpec != diffTypeSpecs[i - 1]) {
                            if (TypeSpec.isA(diffTypeSpecs[i - 1], SymbolTableConstants.ARRAYTYPE)) {
                                needDiffArrayDeclarationS[i - 1] = true;
                            }
                        }
                    }
                }
            }
        }
// if (traceHere) {
// System.out.println("GLOBAL RESULTDIFFTYPESPEC:"+resultDiffTypeSpec+" OR IF NULL: "+diffTypeSpec+" CONTAINSARRAYDECLARATOR: "+containsArrayDeclarator) ;
// for (int i = 0; i < declarators.length(); i++) {
//   System.out.println("   Declarator#"+(i+1)+" "+declarators.down(i+1)+" :: "+diffTypeSpecs[i]+" needdiffarray:"+needDiffArrayDeclarationS[i]+" varDecl:"+varDecls[i]+" containsArrayDeclarator:"+containsArrayDeclaratorS[i]) ;
// }
// }
        // resultDiffTypeSpec est non null
        // et diffTypeSpec array of resultDiffTypeSpec
        // ou resultDiffTypeSpec est null
        if (resultDiffTypeSpec != null) {
            typeSpecTree = diffTypeSpec.generateTree(diffSymbolTable, null, null, true, null);
        }
        for (int i = 1; i <= declarators.length(); i++) {
            declarator = declarators.down(i);
            if (declarator != null) {
                varName = ILUtils.baseName(declarator);
                varTree = ILUtils.build(ILLang.op_ident, varName);
                Tree hintTreeSize = varTree;
                varDecl = diffSymbolTable.getVariableOrConstantDecl(varName);
                if (varDecl != null && !varDecl.isATrueSymbolDecl) {
                    varDecl = null;
                }
                funcDecls = diffSymbolTable.getFunctionDecl(varName, null, null, false);
                funcDecl = funcDecls == null ? null : funcDecls.head;
                if (declarator.opCode() == ILLang.op_functionDeclarator) {
                    varDecl = null;
                }
                if (TapEnv.associationByAddress() && varDecl!=null
                    && funcDecl!=null && isCurUnitReturn(funcDecl)) {
                    // In associationByAddress, if both funcDecl and VarDecl exist and refer
                    // to the same called function Unit, differentiation as
                    // a funcDecl works better for functions' returned types:
                    varDecl = null;
                }
// if (traceHere) System.out.println("VARDECL:"+varDecl+" active:"+(varDecl==null?"N/A":varDecl.isActiveSymbolDecl())+" FUNCDECL:"+funcDecl) ;
                if (varDecl != null) {
                    TapList<String> extraInfo = varDecl.extraInfo();
                    Tree copyVarTree = ILUtils.copy(tree);
                    Tree decli = declarator;
                    VariableDecl diffVarDecl = null;
                    hasDiffNames = true;
                    if (differentiationMode == DiffConstants.ADJOINT_FWD) {
                        if (varDecl.formalArgRankInOrigUnit > 0 && varDecl.formalArgRankInOrigUnit < formalArgsPointerActivity.length) {
                            hasDiffNames = formalArgsPointerActivity[varDecl.formalArgRankInOrigUnit - 1];
                        } else {
                            hasDiffNames = false;
                        }
                    }
                    if (hasDiffNames && forInterface && formalArgsActivity != null
                            && varDecl.formalArgRankInOrigUnit > 0
                            && varDecl.formalArgRankInOrigUnit < formalArgsActivity.length
                            && !formalArgsActivity[varDecl.formalArgRankInOrigUnit - 1]) {
                        hasDiffNames = false;
                    }
// if (traceHere) System.out.println("HASDIFFNAMES:"+hasDiffNames) ;
                    WrapperTypeSpec varDeclTypeSpec = varDecl.type();
                    if (adEnv.curUnit() != null && origLanguage != adEnv.curUnit().language()) {
                        // si on differentie une declaration dans une interface Fortran d'une procedure C
                        // il faut generer le type Fortran (et pas le type C):
                        TypeDecl otherTypeDecl = adEnv.diffCallGraph().getOtherBindType(varDeclTypeSpec);
                        if (otherTypeDecl != null) {
                            varDeclTypeSpec = otherTypeDecl.typeSpec;
                        }
                    }
                    if (needDiffArrayDeclarationS[i - 1]
                            && decli.opCode() != ILLang.op_arrayDeclarator
                            && decli.opCode() != ILLang.op_sizedDeclarator) {
                        typeSpecTree = varDeclTypeSpec.generateTree(diffSymbolTable, null, null, true, null);
                        if (TypeSpec.isA(varDecl.type(), SymbolTableConstants.ARRAYTYPE)) {
                            decli = ILUtils.buildArrayDeclarator(adEnv.curUnit().language(),
                                    ILUtils.copy(decli), ILUtils.copy(typeSpecTree.down(2)));
                        }
                    }
                    if (differentiationMode == DiffConstants.TANGENT_MODE
                            && varDecl.isActiveSymbolDecl() && hasDiffNames
                            && funcDecl != null
                            && adEnv.curUnit().isFortran()
                            && funcDecl.symbol.equals(adEnv.curUnit().name())
                            && funcDecl.isElemental()) {
                        varDecl.setExtraInfo(new TapList<>("out", varDecl.extraInfo()));
                        if (modifiers == null) {
                            modifiers = new Tree[1];
                            modifiers[0] = ILUtils.build(ILLang.op_ident, "out");
                        }
                    }
                    Tree diffDecl = decli;
                    TapList<String> diffExtraInfo = null;
                    // When this variable is the Fortran function name, it is the return value,
                    // so if its type is simple enough (no arrays!) we don't want to declare its
                    // derivative as a variable because it will be declared in a better way
                    // as the function header return type:
                    boolean isFuncReturnVar = false;
                    if (adEnv.curUnit().isFortran()
                        && ((differentiationMode == DiffConstants.TANGENT_MODE && !multiDirMode())
                            || differentiationMode == DiffConstants.ADJOINT_FWD
                            || (adEnv.curActivity() != null && adEnv.curActivity().isContext()))
                        && !(TypeSpec.isA(varDeclTypeSpec, SymbolTableConstants.ARRAYTYPE)
                             && !varDeclTypeSpec.isCharacter()
                             && !varDeclTypeSpec.isString())) {
                        String returnVarName =
                                (adEnv.curUnit().otherReturnVar() == null
                                        ? adEnv.curUnit().name()
                                        : adEnv.curUnit().otherReturnVar().symbol);
// if (traceHere) System.out.println(returnVarName+" vs "+varDecl) ;
                        isFuncReturnVar = varDecl.symbol.equals(returnVarName);
                    }
                    boolean diffFunctionIsFunction = (!multiDirMode() && !adEnv.curUnit().usedAsElementalResult);
                    boolean mustDifferentiateDeclarator =
                            (varDecl.isActiveSymbolDecl() && hasDiffNames && !(isFuncReturnVar && diffFunctionIsFunction));
// if (traceHere) System.out.println("ISFUNCRETURNVAR:"+isFuncReturnVar+" dFisF:"+diffFunctionIsFunction+" MustDiffDecl:"+mustDifferentiateDeclarator);
                    if (isFuncReturnVar && diffFunctionIsFunction && !(varDecl.isActiveSymbolDecl() && hasDiffNames)
                            && formalArgsActivity != null
                            && !(differentiationMode == DiffConstants.ADJOINT_FWD
                            ? formalArgsPointerActivity[formalArgsActivity.length - 1]
                            : formalArgsActivity[formalArgsActivity.length - 1])) {
                        // When the primal return variable will be the diff return, don't declare
                        // its type because we prefer to declare it as the return type of the function.
                        toRemoveDecls = new TapList<>(copyInstrInDiff.tree.down(3).down(i), toRemoveDecls);
                    }
                    if (mustDifferentiateDeclarator) {
                        if (!TapEnv.associationByAddress()) {
                            diffDecl =
                                    varRefDifferentiator().diffVarRef(adEnv.curActivity(),
                                            decli, diffSymbolTable, false, copyVarTree,
                                            ((containsArrayDeclarator && !containsDimDeclarationS[i - 1])
                                             || (resultDiffTypeSpec==null && diffTypeSpec==null)
                                             || diffTypeSpec==varDecl.type()),
                                            true, hintTreeSize);
                        }
                        if (varDecl.formalArgRank > 0 && varDecl.passesByValue(adEnv.curUnit())
                                && diffSymbolTable.basisSymbolTable().isFormalParamsLevel()) {
                            VariableDecl diffVarDecl2 = diffSymbolTable.getTopVariableDecl(ILUtils.baseName(diffDecl));
                            VariableDecl diffVarDeclPublic =
                                    diffSymbolTable.basisSymbolTable().getVariableDeclOfRank(varDecl.formalArgRank + 1);
                            if (diffVarDecl2 != null && diffVarDeclPublic != null) {
                                diffDecl = ILUtils.build(ILLang.op_ident, diffVarDeclPublic.symbol);
                            }
                        }
                        Tree diffDeclBaseTree = ILUtils.baseTree(diffDecl);
                        newSH = NewSymbolHolder.getNewSymbolHolder(diffDeclBaseTree);
                        if (newSH != null) {
                            if (newSH.newFunctionDecl() != null) {
                                diffDeclBaseTree.setAnnotation("isFunctionName", Boolean.TRUE);
                            }
                            if (diffSymbolDecl == null) {
                                diffSymbolDecl = newSH.newVariableDecl();
                                if (diffSymbolDecl == null) {
                                    diffSymbolDecl = newSH.newFunctionDecl();
                                }
                            }
                            newSH.setVarDeclTreeAlreadyPlacedFor(diffSymbolTable);
                            if (!TapEnv.relatedLanguageIsC() && funcDecl != null
                                    && funcDecl.symbol.equals(adEnv.curUnit().name()) && funcDecl.isElemental()) {
                                if (differentiationMode == DiffConstants.TANGENT_MODE) {
                                    newSH.newVariableDecl.setExtraInfo(new TapList<>("out", newSH.extraInfo()));
                                } else if (differentiationMode == DiffConstants.ADJOINT_MODE) {
                                    newSH.newVariableDecl.setExtraInfo(new TapList<>("in", newSH.extraInfo()));
                                }
                            }
                            newSH.setInstruction(instructionResult);
                            newSH.newVariableDecl().setInstruction(newSH.instruction());
                            newSH.newVariableDecl().setInstruction(varDecl);
                        }
                        diffVarDecl = (newSH != null
                                ? newSH.newVariableDecl()
                                : diffSymbolTable.getVariableOrConstantDecl(ILUtils.baseName(diffDecl)));
                        diffExtraInfo = (diffVarDecl != null ? diffVarDecl.extraInfo() : null);
                    }
                    boolean mustReviewModifiers = (
                            TapList.containsOneOfStrings(extraInfo,
                                    new String[]{"in", "out", "inout", "constant", "optional", "value", "allocatable"},
                                    !TapEnv.relatedLanguageIsFortran())
                                    || TapList.containsOneOfStrings(varDecl.accessInfo,
                                    new String[]{"const"},
                                    !TapEnv.relatedLanguageIsFortran())
                                    || TapList.containsOneOfStrings(diffExtraInfo,
                                    new String[]{"in", "out", "inout", "constant", "optional", "value", "allocatable"},
                                    !TapEnv.relatedLanguageIsFortran())
                                    || ILUtils.containsModifier("out", instruction.tree, false)
                                    || ILUtils.containsModifier("constant", instruction.tree, false)
                                    || ILUtils.containsModifier("const", instruction.tree, true));
                    if (mustReviewModifiers || varDecl.hasInstructionWithRootOperator(ILLang.op_accessDecl, "out")) {
// if (traceHere) System.out.println("VARDECL:"+varDecl+" HAS FORMAL RANK:"+varDecl.formalArgRankInOrigUnit+" undef="+SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK) ;
                        boolean isLocalDeclared =
                                (adEnv.curUnit().isStandard() &&
                                        varDecl.formalArgRankInOrigUnit == SymbolTableConstants.NOT_A_FORMAL_ARG);
                        origAccessChecks =
                                new TapList<>(new AccessCheck(varDecl, varDecl, isLocalDeclared, null),
                                        origAccessChecks);
                    }
                    if (mustDifferentiateDeclarator) {
                        if (mustReviewModifiers || ILUtils.containsModifier("optional", instruction.tree, false)) {
                            diffAccessChecks =
                                    new TapList<>(new AccessCheck(diffVarDecl, varDecl,
                                            isDiffLocalDeclared[i - 1], newSH),
                                            diffAccessChecks);
                        }

                        if (TypeSpec.isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)
                                || (TapEnv.associationByAddress()
                                && TypeSpec.isA(typeSpec, SymbolTableConstants.COMPOSITETYPE))) {
                            modifiers = ILUtils.removeSizeModifiers(modifiers);
                        }
                        if (declarator.opCode() == ILLang.op_assign) {
                            typeSpec = varDecl.type();
                            WrapperTypeSpec diffVarTypeSpec =
                                    typeSpec.differentiateTypeSpecMemo(diffSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                                            suffixes()[curDiffUnitSort()][DiffConstants.TYPE],
                                            isDiffLocalDeclared[i - 1], multiDirMode(),
                                            varRefDifferentiator().getMultiDirDimensionMax(),
                                            varName, varName, varTree, null);
                            if (diffVarTypeSpec == null) {
                                diffVarTypeSpec = typeSpec;
                            }
                            ILUtils.turnAssignFromInitDecl(declarator);
                            Tree diffDeclInit =
                                    differentiateInitDeclarator(diffVarTypeSpec,
                                            differentiationMode, diffDecl, diffSymbolTable,
                                            beforeActiv, afterActiv,
                                            afterReqX, beforeAvlX,
                                            declarator, declarationOnly);
                            ILUtils.resetAssignFromInitDecl(declarator);
                            if (diffDeclInit != null) {
                                diffDecl = diffDeclInit;
                            }
                        }
                        if (resultDiffTypeSpec != null
                                && !containsDimDeclarationS[i - 1]
                                && typeSpecTree != null
                                && typeSpecTree.opCode() == ILLang.op_arrayDeclarator) {
                            diffDecl = ILUtils.buildArrayDeclarator(adEnv.curUnit().language(),
                                    diffDecl, ILUtils.copy(typeSpecTree.down(2)));
                        }
                        if (diffDecl != null) {
                            tlNewDeclarators = tlNewDeclarators.placdl(diffDecl);
                        }
                        if (TapEnv.associationByAddress()) {
                            toRemoveDecls = new TapList<>(copyInstrInDiff.tree.down(3).down(i), toRemoveDecls);
                        }
                    }
                } else if (funcDecl != null) {
                    //&& curDiffUnitSort()!=DifferentiationConstants.ADJOINT
                    //&& procedureCallDifferentiator().diffFunctionIsFunction(funcDecl.unit)) {
                    funcDeclUnit = funcDecl.unit();
                    if (funcDeclUnit.origUnit != null) {
                        funcDeclUnit = funcDeclUnit.origUnit;
                    }
                    TapList<ActivityPattern> funcDiffPatterns =
                            adEnv.getPatternsForCallingUnitPattern(adEnv.curActivity(), funcDeclUnit);
                    // When curDiffUnit is a "_nodiff copy", it cannot call any diff Units.
                    // Happens only (?) for AssociationByAddress-like modes, through updateAAInCopiedUnits
                    if (TapEnv.associationByAddress() && adEnv.curDiffUnit==adEnv.copiedUnits.retrieve(adEnv.curUnit())) {
                        funcDiffPatterns = null ;
                    }
                    // When this is the declaration of the curUnit function's result itself,
                    // then the activity pattern is curActivity():
                    if (funcDeclUnit == adEnv.curUnit() && varDecl != null) {
                        //funcDecl is the declaration of the curUnit's function result.
                        // Don't differentiate it when the diff function has been given an alternative return name.
                        Tree headerTree = curDiffUnit().entryBlock().headInstr().tree;
                        if (headerTree == null || ILUtils.getCalledName(headerTree) == null
                                || ILUtils.getCalledName(headerTree).getAnnotation("explicitReturnVar") == null) {
                            funcDiffPatterns = new TapList<>(adEnv.curActivity(), funcDiffPatterns);
                        }
                    }
// if (traceHere) {
// System.out.println(" diff patterns of called Unit "+funcDeclUnit+" are:"+funcDeclUnit.activityPatterns) ;
// System.out.println("  and those specific to call activity "+adEnv.curActivity()+" are: "+funcDiffPatterns) ;
// }
                    // With associationByAddress, when there exists a "_nodiff copy" of the called Unit, it may have a different return type.
                    // we must remove the initial declaration of the primal called unit, and tell the sequel to insert
                    // the declaration of the "_nodiff copy"
                    if (TapEnv.associationByAddress() && adEnv.copiedUnits.retrieve(funcDeclUnit)!=null) {
                        toRemoveDecls = new TapList<>(copyInstrInDiff.tree.down(3).down(i), toRemoveDecls);
                        Unit funcDiffUnit = adEnv.copiedUnits.retrieve(funcDeclUnit) ;
                        Tree diffDecl = ILUtils.copy(varTree) ;
                        diffDecl.setAnnotation("isFunctionName", Boolean.TRUE);
                        diffDecl = copyAndAdaptDeclarator(declarator, diffDecl, funcDiffUnit, diffSymbolTable) ;
                        WrapperTypeSpec primalCopyResultTypeSpec = funcDiffUnit.functionTypeSpec().returnType;
                        Tree primalDeclarationTree = buildDiffVarDeclarationTree(diffSymbolTable,
                                                         primalCopyResultTypeSpec, primalCopyResultTypeSpec,
                                                         null, new TapList<Tree>(diffDecl, null),
                                                         removeDuplicateModifiers(modifiers, primalCopyResultTypeSpec),
                                                         null);
                        Instruction primalDeclarationInstruction = new Instruction(primalDeclarationTree) ;
// if (traceHere) {
// System.out.println(" Adding a separate declaration for nodiff copy:"+primalDeclarationInstruction);
// }
                        primalDeclarationInstruction.isDifferentiated = true ;
                        toNewPrimalDeclarations.placdl(primalDeclarationInstruction) ;

                    }
                    while (funcDiffPatterns != null) {
                        ActivityPattern curCalledPattern = funcDiffPatterns.head;
                        UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(funcDeclUnit);
                        int curDiffMode;
                        if (curDiffUnitSort() == DiffConstants.ADJOINT && funcDeclUnit.mustDifferentiateSplit()) {
                            curDiffMode = adjointSplitMode;
                        } else {
                            curDiffMode = curDiffUnitSort();
                        }
                        if (curCalledPattern!=null && curCalledPattern.isContext()
                            && adjointSplitMode == DiffConstants.ADJOINT_FWD) {
                            // There is no adjoint split mode for "context" code:
                            curDiffMode = DiffConstants.ADJOINT;
                        }
                        Unit funcDiffUnit = null ;
                        if (diffInfo != null) {
                            Tree diffDecl = null ;
                            if (curDiffMode != DiffConstants.ORIGCOPY) {
                                funcDiffUnit = diffInfo.getDiffForModeAndActivity(curDiffMode, curCalledPattern);
// if (traceHere) {System.out.println(" curDiffMode:"+curDiffMode+" curCalledPattern:"+curCalledPattern+" => "+funcDiffUnit);}
                                if (funcDiffUnit != null /* && !multiDirMode()*/) {
                                    newSH = funcDecl.getDiffSymbolHolder(curDiffMode, curCalledPattern, 0);
                                    boolean forceAddNewDeclaration = 
                                        (declarator.opCode() == ILLang.op_functionDeclarator
                                         && (newSH == null || newSH.instruction() == null
                                             // Since an include file may be shared, it must contain
                                             // all diffs of contained procedures declarations:
                                             || adEnv.curInstruction().fromInclude() != null));
// if (traceHere) {System.out.println("FORCEADDNEWDECLARATION:"+forceAddNewDeclaration+" FUNCDIFFUNIT.FUNCTIONTYPESPEC"+funcDiffUnit.functionTypeSpec());}
                                    if (forceAddNewDeclaration ||
                                        (funcDiffUnit.functionTypeSpec() != null
                                         && !TypeSpec.isA(funcDiffUnit.functionTypeSpec().returnType,
                                                          SymbolTableConstants.VOIDTYPE))) {
// if (traceHere) System.out.println("    FUNCDECL:"+funcDecl+" RENAMED FROM "+funcDecl.importedFrom.first);
                                        diffDecl = varRefDifferentiator().diffSymbolName(curCalledPattern, varName,
                                                                 diffSymbolTable, false, true, false, tree, null, false,
                                                                 curDiffMode, curDiffMode, DiffConstants.PROC, null);
// if (traceHere) {System.out.println("  DIFFSYMBOLNAME "+varName+" ==> "+diffDecl);}
                                    }
                                }
                            }
                            if (diffDecl != null) {
                                newSH = NewSymbolHolder.getNewSymbolHolder(ILUtils.baseTree(diffDecl));
                                if (newSH != null) {
                                    newSH.setVarDeclTreeAlreadyPlacedFor(diffSymbolTable);
                                    newSH.setInstruction(instructionResult);
                                    if (diffSymbolDecl == null) {
                                        diffSymbolDecl = newSH.newFunctionDecl();
                                    }
                                }
                                diffDecl.setAnnotation("isFunctionName", Boolean.TRUE);
                                diffDecl = copyAndAdaptDeclarator(declarator, diffDecl, funcDiffUnit, diffSymbolTable) ;

                                if (toDiffDeclarationsForFwd!=null && curDiffMode==DiffConstants.ADJOINT_FWD) {
                                    declaratorsForFwd = new TapList<>(diffDecl, declaratorsForFwd) ;
                                } else {
                                    tlNewDeclarators = tlNewDeclarators.placdl(diffDecl); // [jh] TODO: we need to append more here
                                }
                                if (funcDiffUnit.functionTypeSpec() != null) {
                                    resultDiffTypeSpec = funcDiffUnit.functionTypeSpec().returnType;
                                    // (again) In Fortran, when a function returns an array, do not declare its dimensions:
                                    if (varDecl == null && funcDecl != null && funcDecl.unit().isFortran()
                                        && TypeSpec.isA(resultDiffTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                        resultDiffTypeSpec = resultDiffTypeSpec.wrappedType.elementType();
                                    }
                                    modifiers = ILUtils.removeSizeModifiers(modifiers); //??C:v49
                                }
                                if (funcDiffUnit.getForwardDeclaration() == null) {
                                    funcDiffUnit.setForwardDeclaration(instructionResult);
                                }
                            }
                        }
                        funcDiffPatterns = funcDiffPatterns.tail;
                    }
                }
            }
        }

        // Because we have collected modifiers from the type of the declaration Tree
        // AND also from the type of the symbol in the SymbolTable,
        // we may have duplicates: remove the duplicates from modifiers:
        modifiers = removeDuplicateModifiers(modifiers, diffTypeSpec) ;

// if (traceHere) {
// System.out.println("Diff of orig varDeclaration:"+tree) ;
// System.out.print(" typeSpec:"+typeSpec+" diffTypeSpec:"+diffTypeSpec+" resultDiffTypeSpec:"+resultDiffTypeSpec+" modifiers:") ;
// if (modifiers==null)
//     System.out.print("null") ;
// else {
//     System.out.print("[") ;
//     for (int i=0 ; i<modifiers.length ; ++i) System.out.print(modifiers[i]+",") ;
//     System.out.print("]") ;
// }
// System.out.println(" proposedType:"+proposedDiffTypeTree) ;
// System.out.println(" diff declarators:"+hdNewDeclarators.tail);
// System.out.println(" declaratorsForFwd:"+declaratorsForFwd) ;
// System.out.println(" diffAccessChecks:"+diffAccessChecks);
// System.out.println(" origAccessChecks:"+origAccessChecks);
// }
        Tree diffVarDeclarationTree = null;
        if (hdNewDeclarators.tail != null) {
            diffVarDeclarationTree =
                    buildDiffVarDeclarationTree(diffSymbolTable, typeSpec,
                            (resultDiffTypeSpec == null ? diffTypeSpec : resultDiffTypeSpec),
                            proposedDiffTypeTree, hdNewDeclarators.tail,
                            modifiers, diffSymbolDecl);
        }
// if (traceHere) System.out.println("   diffVarDeclarationTree:"+diffVarDeclarationTree) ;
        if (diffVarDeclarationTree != null) {
            instructionResult.tree = diffVarDeclarationTree;
            instructionResult.isDifferentiated = true;
        }
        while (origAccessChecks != null) {
// if (traceHere) System.out.println(" Copy Instr befor being checked for modifiers:"+copyInstrInDiff) ;
            checkAccessModifiers(copyInstrInDiff.tree, origAccessChecks.head,
                    block, diffSymbolTable, true, differentiationMode);
            origAccessChecks = origAccessChecks.tail;
        }
// if (traceHere) System.out.println(" Copy Instr after being checked for modifiers:"+copyInstrInDiff) ;
        while (diffAccessChecks != null) {
// if (traceHere) System.out.println(" Diff Instr befor being checked for modifiers:"+instructionResult) ;
            checkAccessModifiers(instructionResult.tree, diffAccessChecks.head,
                    block, diffSymbolTable, false, differentiationMode);
            diffAccessChecks = diffAccessChecks.tail;
        }
// if (traceHere) System.out.println(" Diff Instr after being checked for modifiers:"+instructionResult) ;
        // If all declarators have been removed, set instructionResult to null:
        if (instructionResult.tree == null || ILUtils.isNullOrEmptyList(instructionResult.tree.down(3))) {
            instructionResult = null;
        }
        if (toRemoveDecls != null) {
            removeDecl(toRemoveDecls);
            if (ILUtils.isNullOrEmptyList(copyInstrInDiff.tree.down(3))) {
                copyInstrInDiff.tree = ILUtils.build(ILLang.op_none);
            }
        }
// if (traceHere) System.out.println(" ==> DIFFVARDECL: "+(instructionResult==null?null:instructionResult.tree)) ;
        if (declaratorsForFwd!=null) {
            Tree declarationForFwd = buildDiffVarDeclarationTree(diffSymbolTable, typeSpec,
                            (resultDiffTypeSpec == null ? diffTypeSpec : resultDiffTypeSpec),
                            proposedDiffTypeTree, declaratorsForFwd,
                            modifiers, diffSymbolDecl);
            Instruction instructionForFwd = new Instruction(declarationForFwd) ;
            toDiffDeclarationsForFwd.setObj(
                    new TapList<Instruction>(instructionForFwd, toDiffDeclarationsForFwd.obj())) ;
        }
        return (diffVarDeclarationTree == null ? null : instructionResult) ;
    }

    /** Build a new declarator Tree for the given function name newDecl, on the model of the given declarator */
    private Tree copyAndAdaptDeclarator(Tree declarator, Tree newDecl, Unit funcDiffUnit, SymbolTable diffSymbolTable) {
        // Peel off pointerDeclarators:
        while (declarator.opCode() == ILLang.op_pointerDeclarator) {
            declarator = declarator.down(1);
        }
        if (declarator.opCode() == ILLang.op_functionDeclarator) {
            Tree newFunDecl = ILUtils.copy(declarator);
            newFunDecl.setChild(newDecl, 1);
            // Neither choice of argsListSymbolTable is satisfactory.
            // It should rather be a special SymbolTable for the arguments list of this function declarator.
            SymbolTable argsListSymbolTable = funcDiffUnit.publicSymbolTable();
            if (argsListSymbolTable == null) {
                argsListSymbolTable = diffSymbolTable;
            }
            Tree diffCallTypedTree =
                ILUtils.build(ILLang.op_argDeclarations,
                              funcDiffUnit.generateHeaderParamsList(argsListSymbolTable));
            if (diffCallTypedTree.length() != 0) {
                newFunDecl.setChild(diffCallTypedTree, 2);
            } else {
                newFunDecl.setChild(ILUtils.build(ILLang.op_none), 2);
            }
            return newFunDecl ;
        } else {
            return newDecl ;
        }
    }

    /** True if the given funcDecl declares some derivative of the same origin Unit as the
     * current curDiffUnit. This in fact checks that this funcDecl is about (a diff of) curUnit. */
    private boolean isCurUnitReturn(FunctionDecl funcDecl) {
        return funcDecl.unit()!=null && funcDecl.unit().origUnit==curDiffUnit().origUnit ;
    }

    /**
     * Check the access modifiers (IN, OUT, INOUT, OPTIONAL, CONSTANT, CONST, VALUE, ALLOCATABLE)
     * used in this op_varDeclaration (if onPrimalDecl?copied-primal else differentiated) Instruction
     * because differentiation may need to change them. If onPrimalDecl, then we may only remove
     * modifiers (e.g. "out" may need to disappear from the primal decl in the adjoint case),
     * or add an "out" (for original function's result).
     * If not onPrimalDecl, we may have to remove and add modifiers.
     *
     * @param declaration   the op_varDeclaration instruction that will declare this variable.
     * @param accessToCheck essentially the VariableDecl to be checked for access modifiers.
     * @param onPrimalDecl  true if looking at declaration of primal copy variables.
     * @param diffMode      the current differentiation mode.
     */
    private static void checkAccessModifiers(Tree declaration, AccessCheck accessToCheck, Block inDiffBlock,
                                             SymbolTable diffSymbolTable, boolean onPrimalDecl, int diffMode) {
        boolean declRemoved = false;
        VariableDecl varDecl = accessToCheck.decl;
        VariableDecl origVarDecl = accessToCheck.primalDecl;
        // In adjoint (_B or _FWD or_BWD), remove intent(out) on the primal variable:
        if ((diffMode == DiffConstants.ADJOINT_MODE || diffMode == DiffConstants.ADJOINT_SPLIT_MODE)) {
            TapList<String> extraInfo = origVarDecl.extraInfo();
            TapList.replaceExtraInfoValue(extraInfo, "out", "inout");
            origVarDecl.setExtraInfo(extraInfo);
        }
        if (declaration != null) {
            if (declaration.down(3).length() == 1) {
                if (!onPrimalDecl) {
                    if (origVarDecl.hasOtherInstruction()) {
                        // As soon as the primal declaration was scattered over several instructions,
                        // we decide to make the diff declaration non-scattered, and regenerate
                        // its type from scratch from the differentiated VariableDecl.
                        declaration.setChild(varDecl.type().generateTree(diffSymbolTable, null, null, true, null),
                                2);
                    }
                }
                // First, add modifiers from extraInfo that are not yet in declaration:
                TapList<Tree> extraModifiers = null;
                TapList<String> extraInfo = varDecl.extraInfo();
                while (extraInfo != null) {
                    String extraModifier = extraInfo.head;
                    if (!(extraModifier.equals("save") && origVarDecl.hasInstructionWithRootOperator(ILLang.op_save, null))
                            && ILUtils.contains(declaration, ILLang.op_modifiers, extraModifier) == null
                            && !(diffMode == DiffConstants.ADJOINT_MODE && extraModifier.equals("out"))
                            && (!onPrimalDecl || (extraModifier.equals("out") && !origVarDecl.hasOtherInstruction()))) {
                        extraModifiers = new TapList<>(ILUtils.build(ILLang.op_ident, extraModifier), extraModifiers);
                    }
                    extraInfo = extraInfo.tail;
                }
                if (extraModifiers != null) {
                    declaration.setChild(ILUtils.build(ILLang.op_modifiedType,
                            ILUtils.build(ILLang.op_modifiers, TapList.reverse(extraModifiers)),
                            declaration.cutChild(2)),
                            2);
                }

                // Second, remove from declaration in-out modifiers thet are not in extraInfo:
                if (declaration.down(2).opCode() == ILLang.op_modifiedType) {
                    if (ILUtils.containsModifier("out", declaration, false)
                            && (diffMode == DiffConstants.ADJOINT_MODE || !TapList.containsEquals(varDecl.extraInfo(), "out"))) {
                        ILUtils.peelModifier(declaration, "out");
                    } else if (ILUtils.containsModifier("in", declaration, false)
                            && !TapList.containsEquals(varDecl.extraInfo(), "in")) {
                        ILUtils.peelModifier(declaration, "in");
                    } else if (ILUtils.containsModifier("inout", declaration, false)
                            && !TapList.containsEquals(varDecl.extraInfo(), "inout")) {
                        ILUtils.peelModifier(declaration, "inout");
                    } else if (ILUtils.containsModifier("constant", declaration, false)
                            && !TapList.containsEquals(varDecl.extraInfo(), "constant")) {
                        ILUtils.peelModifier(declaration, "constant");
                    } else if (ILUtils.containsModifier("const", declaration, true) && !varDecl.isCconst()) {
                        ILUtils.peelModifier(declaration, "const");
                    }
                }
                if (!TapList.containsEquals(origVarDecl.extraInfo(), "out")
                        && origVarDecl.hasInstructionWithRootOperator(ILLang.op_accessDecl, "out")) {
                    Instruction instr = inDiffBlock.getOperatorDeclarationInstruction(
                            varDecl, ILLang.op_accessDecl, diffSymbolTable);
                    if (instr != null) {
                        instr.tree = ILUtils.build(ILLang.op_none);
                    }
                }
            } else {
                if (declaration.down(2).opCode() != ILLang.op_modifiedType
                        && varDecl.extraInfo() != null) {
                    String modifier = varDecl.extraInfo().head;
                    if (TapEnv.relatedUnit() == inDiffBlock.unit() // ce n'est pas une interfaceDecl
                            && !varDecl.hasInstructionWithRootOperator(ILLang.op_accessDecl, modifier)) {
                        removeDeclFromInstruction(varDecl, declaration);
                        declRemoved = true;
                    }
                } else if (declaration.down(2).opCode() == ILLang.op_modifiedType
                        && ILUtils.containsModifier("out", declaration, false)
                        && !TapList.containsEquals(varDecl.extraInfo(), "out")) {
                    ILUtils.peelModifier(declaration, "out");
                }
                if (declaration.down(2).opCode() == ILLang.op_modifiedType
                        && ILUtils.containsModifier("optional", declaration, false)
                        && !TapList.containsEquals(varDecl.extraInfo(), "optional")) {
                    removeDeclFromInstruction(varDecl, declaration);
                    declRemoved = true;
                    declaration = null;
                } else if (declaration.down(2).opCode() == ILLang.op_modifiedType
                        && ILUtils.containsModifier("constant", declaration, false)
                        && !TapList.containsEquals(varDecl.extraInfo(), "constant")) {
                    removeDeclFromInstruction(varDecl, declaration);
                    declRemoved = true;
                    declaration = null;
                } else if (declaration.down(2).opCode() == ILLang.op_modifiedType
                        && ILUtils.containsModifier("const", declaration, true) && !varDecl.isCconst()) {
                    removeDeclFromInstruction(varDecl, declaration);
                    declRemoved = true;
                    declaration = null;
                }
            }
            if (accessToCheck.isLocalDeclared && declaration != null
                    && declaration.down(2).opCode() == ILLang.op_modifiedType) {
                varDecl.formalArgRank = SymbolTableConstants.NOT_A_FORMAL_ARG;
                if (declaration.down(3).length() == 1) {
                    if (!onPrimalDecl) {
                        Tree[] originalModifiers = declaration.down(2).down(1).children();
                        TapList<Tree> remainingModifiers = null;
                        for (int i = originalModifiers.length - 1; i >= 0; --i) {
                            if (!ILUtils.isIdentOf(originalModifiers[i],
                                    new String[]{"in", "out", "inout", "constant", "const", "optional"},
                                    false)) {
                                remainingModifiers = new TapList<>(originalModifiers[i], remainingModifiers);
                            }
                        }
                        if (remainingModifiers == null) {
                            declaration.setChild(ILUtils.copy(declaration.down(2).down(2)), 2);
                        } else {
                            declaration.down(2).setChild(ILUtils.build(ILLang.op_modifiers, remainingModifiers), 1);
                        }
                    }
                } else {
                    removeDeclFromInstruction(varDecl, declaration);
                    declRemoved = true;
                }
            }
        }
        if (declRemoved && accessToCheck.newSH != null) {
            accessToCheck.newSH.setInstruction(null);
        }
    }

    /**
     * Remove the declaration of varDecl from declaration.
     * Reset varDecl markers (and if present its NewSymbolHolder) to indicate that
     * varDecl has no declaration code.
     * The new declaration will be added when solving the NewSymbolHolder.
     */
    private static void removeDeclFromInstruction(VariableDecl varDecl, Tree declaration) {
        int rankDecl = 0;
        int i = 1;
        Tree curDecl;
        NewSymbolHolder varSymbolHolder = null;
        while (i <= declaration.down(3).length() && rankDecl == 0) {
            curDecl = ILUtils.baseTree(declaration.down(3).down(i));
            assert curDecl != null;
            if (curDecl.opCode() == ILLang.op_ident
                    && curDecl.stringValue().equals(varDecl.symbol)) {
                varSymbolHolder = NewSymbolHolder.getNewSymbolHolder(curDecl);
                rankDecl = i;
            }
            i = i + 1;
        }
        if (rankDecl > 0) {
            declaration.down(3).removeChild(rankDecl);
            if (varSymbolHolder != null) {
                varSymbolHolder.setVarDeclTreeNotAlreadyPlaced();
            }
        }
        varDecl.setNullInstruction();
    }

    /**
     * Tries to remove from modifiers the size modifiers already present in the given existingTypeSpec
     */
    private Tree[] removeDuplicateModifiers(Tree[] modifiers, TypeSpec existingTypeSpec) {
        Tree[] keptTrees = null;
        if (modifiers != null) {
            Tree presentModifier = null;
            if (TypeSpec.isA(existingTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                TypeSpec insideDiffType = existingTypeSpec;
                // peel off layers of WrapperTypeSpec's:
                while (insideDiffType instanceof WrapperTypeSpec) {
                    insideDiffType = ((WrapperTypeSpec) insideDiffType).wrappedType;
                }
                presentModifier = ((ModifiedTypeSpec) insideDiffType).sizeModifier ;
            }
            TapList<Tree> keptModifiers = null;
            for (int i = modifiers.length - 1; i >= 0; --i) {
                if (presentModifier==null || !modifiers[i].equalsTree(presentModifier)) {
                    keptModifiers = new TapList<>(modifiers[i], keptModifiers);
                }
            }
            if (keptModifiers != null) {
                keptTrees = new Tree[TapList.length(keptModifiers)];
                for (int i = 0; i < keptTrees.length; ++i) {
                    keptTrees[i] = keptModifiers.head;
                    keptModifiers = keptModifiers.tail;
                }
            }
        }
        return keptTrees;
    }

    // [End FunctionDecl mode.]

    private Tree buildDiffVarDeclarationTree(SymbolTable diffSymbolTable, WrapperTypeSpec typeSpec,
                                  WrapperTypeSpec diffTypeSpec, Tree proposedDiffTypeTree,
                                  TapList<Tree> newDeclarators, Tree[] modifiers, SymbolDecl diffSymbolDecl) {
        Tree diffTypeTree;
        if (ILUtils.isNullOrNone(proposedDiffTypeTree)) {
            diffTypeTree = genDiffTypeTree(diffSymbolTable, typeSpec, diffTypeSpec, modifiers);
        } else {
            diffTypeTree = ILUtils.copy(proposedDiffTypeTree);
        }
        if (curDiffUnitSort() == DiffConstants.ADJOINT || !TapEnv.doActivity()) {
            diffTypeTree = ILUtils.peelInOutConstValueModifier(diffTypeTree);
        }
        diffTypeTree = ILUtils.peelConstModifier(diffTypeTree);
        Tree diffVarDeclarationTree = ILUtils.build(ILLang.op_varDeclaration,
                ILUtils.build(ILLang.op_modifiers),
                diffTypeTree,
                ILUtils.build(ILLang.op_declarators, newDeclarators));
        NewSymbolHolder annot = NewSymbolHolder.getNewSymbolHolder(diffTypeTree);
        if (annot != null && diffSymbolDecl != null) {
            TypeDecl diffTypeDecl = annot.newTypeDecl();
            diffSymbolDecl.dependsOn = new TapList<>(diffTypeDecl, diffSymbolDecl.dependsOn);
        }
        return diffVarDeclarationTree;
    }

    /**
     * @param funcDecl interoperable procedure.
     * @return true if procedure definition and call belong to different languages.
     */
    protected boolean mixedLanguage(FunctionDecl funcDecl) {
        return funcDecl != null && funcDecl.unit() != null
                && (adEnv.curUnit() == null && funcDecl.unit().isFortran()
                || adEnv.curUnit() != null && funcDecl.unit().language() != adEnv.curUnit().language());
    }

    private Tree differentiateTypeDeclaration(SymbolTable diffSymbolTable, Tree tree) {
// if (differentiationMode!=TANGENT_MODE) PBaseTrack.experimentTypeDeclaration(tree, curSymbolTable())  ;
        Instruction instructionResult = new Instruction(ILUtils.build(ILLang.op_none));
        Tree result = null;
        Tree typeNameTree = tree.down(1);
        TypeDecl typeDecl = null;
        if (!ILUtils.isNullOrNone(typeNameTree)) {
            typeDecl = diffSymbolTable.getTypeDecl(ILUtils.getIdentString(typeNameTree));
        } else if (!ILUtils.isNullOrNone(tree.down(2).down(1))) {
            typeDecl = diffSymbolTable.getTypeDecl("struct " + ILUtils.getIdentString(tree.down(2).down(1)));
        }

        if (typeDecl != null && typeDecl.isATrueSymbolDecl
                && (multiDirMode() || typeDecl.typeSpec.needsADiffType(null))
                && typeDecl.typeSpec.isDifferentiated(null)) {
            WrapperTypeSpec diffTypeSpec =
                    typeDecl.typeSpec.differentiateTypeSpecMemo(diffSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                            suffixes()[curDiffUnitSort()][DiffConstants.TYPE], false, multiDirMode(),
                            varRefDifferentiator().getMultiDirDimensionMax(),
                            typeDecl.symbol, typeDecl.symbol, ILUtils.build(ILLang.op_ident, typeDecl.symbol), null);
            if (diffTypeSpec != null
                    && !diffTypeSpec.equalsCompilDep(typeDecl.typeSpec)
                    && !(TypeSpec.isA(diffTypeSpec, SymbolTableConstants.COMPOSITETYPE)
                    && ((CompositeTypeSpec) diffTypeSpec.wrappedType).isEmpty())) {
                diffTypeSpec.wrappedType.diffFromTypeDecl = typeDecl;
                Tree diffTypeSpecTree =
                        diffTypeSpec.wrappedType.generateTree(diffSymbolTable, null, null, false, null);
                NewSymbolHolder diffTypeSymbolHolder =
                        typeDecl.typeSpec.wrappedType.diffTypeDeclSymbolHolder;
                if (diffTypeSymbolHolder == null) {
                    diffTypeSymbolHolder = typeDecl.getDiffSymbolHolder(0, null, 0);
                }
                Tree diffTypeNameTree;
                //[llh 21/06/2013] faiblesse: on dirait qu'on ne construit pas le nom de
                // type differentie avec la methode standard varRefDifferentiator().diffSymbolName() ?!
                if (diffTypeSymbolHolder != null) {
                    diffTypeNameTree = diffTypeSymbolHolder.makeNewRef(diffSymbolTable);
                    diffTypeSymbolHolder.setTypeDeclTreeAlreadyPlacedFor(diffSymbolTable);
                    diffTypeSymbolHolder.setInstruction(instructionResult);
                    diffTypeSymbolHolder.newTypeDecl().setInstruction(instructionResult);
                } else {
                    String diffTypeName = diffTypeSpec.wrappedType.typeDeclName();
                    if (diffTypeName == null) {
                        diffTypeName =
                                TapEnv.extendStringWithSuffix(typeDecl.symbol,
                                        suffixes()[curDiffUnitSort()][DiffConstants.TYPE]);
                    }
                    diffTypeNameTree = ILUtils.build(ILLang.op_ident, diffTypeName);
                }
                if (diffTypeSpecTree.opCode() == ILLang.op_recordType
                        && diffTypeSpecTree.down(1).opCode() == ILLang.op_none
                        && typeDecl.symbol.startsWith("struct ")) {
                    diffTypeSpecTree.setChild(diffTypeNameTree, 1);
                    diffTypeNameTree = ILUtils.copy(diffTypeNameTree);
                }
                if (ILUtils.isNullOrNone(typeNameTree)) {
                    diffTypeNameTree = ILUtils.build(ILLang.op_none);
                }
                if (tree.down(2).opCode() == ILLang.op_recordType
                        && ILUtils.isNullOrNoneOrEmptyList(tree.down(2).down(3))) {
                    diffTypeSpecTree.setChild(ILUtils.build(ILLang.op_none), 3);
                }
                result = ILUtils.build(ILLang.op_typeDeclaration, diffTypeNameTree, diffTypeSpecTree);
            }
        }
        if (result != null) {
            instructionResult.tree = result;
        }
        return result;
    }

    private Tree differentiateUseDeclaration(Tree diffTree, SymbolTable diffSymbolTable) {
        String moduleName = ILUtils.getIdentString(diffTree.down(1));
        FunctionDecl moduleDecl = adEnv.curSymbolTable().getModuleDecl(moduleName);
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace(" Differentiating use declaration: " + ILUtils.toString(diffTree, adEnv.curActivity()));
            TapEnv.printlnOnTrace("    moduleDecl:" + moduleDecl + " found in " + adEnv.curSymbolTable());
        }
        if (moduleDecl != null) {
            Unit origModule = moduleDecl.unit();
            if (!origModule.isPredefinedModuleOrTranslationUnit()) {
                // Modules have only one differentiated counterpart ! :
                Unit diffModule = callGraphDifferentiator().getUnitDiffInfo(origModule).getDiff();
                if (diffModule != null && diffModule.isDiffPackage()) {
                    Tree diffModuleName =
                            varRefDifferentiator().diffSymbolName(null, moduleName, curDiffUnit().privateSymbolTable(),
                                    false, false, true, null, null, false,
                                    DiffConstants.DIFFERENTIATED, DiffConstants.DIFFERENTIATED,
                                    DiffConstants.MOD, null);
                    diffTree.setChild(diffModuleName, 1);
                    Tree restrictionsTree = diffTree.down(2);
                    if (!ILUtils.isNullOrNone(restrictionsTree)) {
                        Tree[] onlyVisibleTrees = restrictionsTree.children();
                        TapList<Tree> diffOnlyVisibleTrees = null;
                        Tree onlyVisibleTree;
                        Tree remoteExpr;
                        Tree localName;
                        Tree diffRemoteExpr = null;
                        Tree diffLocalName = null;
                        SymbolDecl localSymbolDecl;
                        boolean isRenamed;
                        for (int i = onlyVisibleTrees.length; i > 0; --i) {
                            onlyVisibleTree = restrictionsTree.cutChild(i);
                            isRenamed = onlyVisibleTree.opCode() == ILLang.op_renamed;
                            if (isRenamed) {
                                remoteExpr = onlyVisibleTree.down(2);
                                localName = onlyVisibleTree.down(1);
                            } else {
                                remoteExpr = onlyVisibleTree;
                                localName = onlyVisibleTree;
                            }
                            if (localName.opCode() == ILLang.op_ident) {
                                localSymbolDecl = curDiffUnit().publicSymbolTable().getSymbolDecl(ILUtils.baseName(localName));
                            } else if (!localName.isAtom() && localName.down(1).opCode() == ILLang.op_none) {
                                // operator overloading
                                localSymbolDecl = curDiffUnit().publicSymbolTable().getSymbolDecl(ILUtils.baseName(localName));
                            } else {
                                localSymbolDecl = null;
                            }
                            if (localSymbolDecl != null) {
                                if (localSymbolDecl.isA(SymbolTableConstants.TYPE)) {
                                    if (localSymbolDecl.isActiveSymbolDecl()) {
                                        // First create the differentiated type, if it doesn't exist yet:
                                        WrapperTypeSpec typeSpec = ((TypeDecl)localSymbolDecl).type() ;
                                        WrapperTypeSpec diffTypeSpec =
                                            typeSpec.differentiateTypeSpecMemo(
                                                diffSymbolTable, adEnv.curSymbolTable(), curDiffUnitSort(),
                                                suffixes()[curDiffUnitSort()][DiffConstants.TYPE],
                                                false, multiDirMode(),
                                                varRefDifferentiator().getMultiDirDimensionMax(),
                                                ILUtils.baseName(localName), ILUtils.baseName(localName), localName, null) ;
                                        if (diffTypeSpec!=null) {// i.e. when diffTypeSpec is not exactly the same as typeSpec!
                                            ((TypeDecl)localSymbolDecl).activeTypeDecl(diffSymbolTable, diffTypeSpec,
                                                                                       suffixes()[curDiffUnitSort()][DiffConstants.TYPE]) ;
                                            // build diff of remoteExpr:
                                            TypeDecl remoteTypeDecl = (TypeDecl) localSymbolDecl;
                                            if (isRenamed) {
                                                remoteTypeDecl = (TypeDecl) remoteTypeDecl.importedFrom.first;
                                            }
                                            NewSymbolHolder typeDeclSH = remoteTypeDecl.getDiffSymbolHolder(0, null, 0);
                                            if (typeDeclSH != null) {
                                                diffRemoteExpr = typeDeclSH.makeNewRef(diffModule.publicSymbolTable());
                                            }
                                            // build diff of localName:
                                            NewSymbolHolder sHolder = localSymbolDecl.getDiffSymbolHolder(0, null, 0);
                                            diffLocalName = null ;
                                            if (sHolder != null) {
                                                diffLocalName = sHolder.makeNewRef(curDiffUnit().publicSymbolTable());
                                                if (isRenamed) {
                                                    if (typeDeclSH != null) {
                                                        if (sHolder.newTypeDecl().importedFrom==null) {
                                                            sHolder.newTypeDecl().importedFrom = new TapPair<>(null, null) ;
                                                        }
                                                        sHolder.newTypeDecl().importedFrom.first =
                                                            remoteTypeDecl.getDiffSymbolHolder(0, null, 0).newTypeDecl();
                                                    }
                                                }
                                            } else if (typeDeclSH != null) {
                                                diffLocalName = ILUtils.build(ILLang.op_ident,
                                                        TapEnv.extendStringWithSuffix(
                                                                ILUtils.baseName(localName), suffixes()[curDiffUnitSort()][DiffConstants.TYPE]));
                                            }
                                            if (diffLocalName != null) {
                                                if (remoteExpr != localName) {
                                                    // If this was a rename
                                                    diffLocalName = ILUtils.build(ILLang.op_renamed, diffLocalName, diffRemoteExpr);
                                                }
                                                diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                            }
                                        }
                                    }
                                } else if (localSymbolDecl == null || localSymbolDecl.isA(SymbolTableConstants.VARIABLE)
                                        || !TapEnv.doActivity() && localSymbolDecl.isA(SymbolTableConstants.CONSTANT)) {
                                    if (localSymbolDecl.isActiveSymbolDecl()) {
                                        diffLocalName = diffLocalAndRemoteNames(adEnv.curActivity(), localName, remoteExpr,
                                                                curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                true, false, onlyVisibleTree, curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR) ;
                                        if (diffLocalName != null) {
                                            diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                        }
                                    }
                                } else if (localSymbolDecl.isA(SymbolTableConstants.INTERFACE)) {
                                    if (localSymbolDecl.isActiveSymbolDecl() ||
                                            ((InterfaceDecl) localSymbolDecl).mustHaveADiff(activityAnalyzer())) {
                                        diffLocalName = diffLocalAndRemoteNames(adEnv.curActivity(), localName, remoteExpr,
                                                                curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                false, true, onlyVisibleTree, curDiffVarSort(), curDiffUnitSort(), DiffConstants.PROC) ;
                                        if (diffLocalName != null) {
                                            diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                        }
                                    }
                                } else if (localSymbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                                    Unit calledUnit = ((FunctionDecl) localSymbolDecl).unit();
                                    if (calledUnit.origUnit != null) {
                                        calledUnit = calledUnit.origUnit;
                                    }
                                    TapList<ActivityPattern> patternsCalledHere =
                                            patternsCalledByPattern(calledUnit, adEnv.curActivity());
                                    ActivityPattern calledPattern;
                                    while (patternsCalledHere != null) {
                                        calledPattern = patternsCalledHere.head;
                                        if (callGraphDifferentiator().patternCreatesDiffCode(calledPattern, calledUnit)) {
                                            int diffUnitSort = curDiffUnitSort() ;
                                            if (diffUnitSort==DiffConstants.ADJOINT && calledUnit.mustDifferentiateSplit()) {
                                                diffLocalName = diffLocalAndRemoteNames(calledPattern, localName, remoteExpr,
                                                                    curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                    false, true, onlyVisibleTree, DiffConstants.ADJOINT_BWD, DiffConstants.ADJOINT_BWD, DiffConstants.PROC) ;
                                                if (diffLocalName != null) {
                                                    diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                                }
                                                diffLocalName = diffLocalAndRemoteNames(calledPattern, localName, remoteExpr,
                                                                    curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                    false, true, onlyVisibleTree, DiffConstants.ADJOINT_FWD, DiffConstants.ADJOINT_FWD, DiffConstants.PROC) ;
                                                if (diffLocalName != null) {
                                                    diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                                }
                                            }
                                            if (diffUnitSort==DiffConstants.ADJOINT && calledUnit.mustDifferentiateJoint()) {
                                                diffLocalName = diffLocalAndRemoteNames(calledPattern, localName, remoteExpr,
                                                                    curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                    false, true, onlyVisibleTree, DiffConstants.ADJOINT, DiffConstants.ADJOINT, DiffConstants.PROC) ;
                                                if (diffLocalName != null) {
                                                    diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                                }
                                            }
                                            if (diffUnitSort==DiffConstants.TANGENT) {
                                                diffLocalName = diffLocalAndRemoteNames(calledPattern, localName, remoteExpr,
                                                                    curDiffUnit().publicSymbolTable(), diffModule.publicSymbolTable(),
                                                                    false, true, onlyVisibleTree, DiffConstants.TANGENT, DiffConstants.TANGENT, DiffConstants.PROC) ;
                                                if (diffLocalName != null) {
                                                    diffOnlyVisibleTrees = new TapList<>(diffLocalName, diffOnlyVisibleTrees);
                                                }
                                            }
                                        }
                                        patternsCalledHere = patternsCalledHere.tail;
                                    }
                                }
                            }
                            diffOnlyVisibleTrees = new TapList<>(onlyVisibleTree, diffOnlyVisibleTrees);
                        }
                        diffTree.setChild(ILUtils.build(restrictionsTree.opCode(), diffOnlyVisibleTrees), 2);
                    }
                }
            }
        }
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace(" --> Differentiated use declaration: " + ILUtils.toString(diffTree, adEnv.curActivity()));
        }
        return diffTree;
    }

    private Tree diffLocalAndRemoteNames(ActivityPattern pattern, Tree local, Tree remote,
                                         SymbolTable localST, SymbolTable remoteST, boolean asVariable, boolean asFunction,
                                         Tree refTree, int diffSort, int diffSuffixSort, int symbolSort) {
        Tree diffClause = varRefDifferentiator().diffSymbolTree(
                                  pattern, remote, remoteST, asVariable, asFunction, false, refTree, false,
                                  diffSort, diffSuffixSort, symbolSort, null);
        Tree diffLocal = null ;
        if (local!=null && local!=remote) { // If this was a rename
            diffLocal = varRefDifferentiator().diffSymbolTree(
                                  pattern, local, localST, asVariable, asFunction, false, refTree, false,
                                  diffSort, diffSuffixSort, symbolSort, null);
            if (diffLocal!=null) {
                diffClause = ILUtils.build(ILLang.op_renamed, diffLocal, diffClause);
            }
        }
        return diffClause ;
    }

    private boolean mustDifferentiateInterfaceSplit(Tree tree) {
        boolean result = false;
        Tree interfaceFunction;
        String interfaceUnitName;
        FunctionDecl functionDecl;
        TapList<FunctionDecl> functionDecls;
        Unit interfaceUnit, diffInterfaceUnit;
        for (int i = tree.down(2).length(); i > 0; --i) {
            if (tree.down(2).down(i).opCode() == ILLang.op_interface) {
                interfaceFunction = tree.down(2).down(i).down(1);
                interfaceUnitName = ILUtils.getUnitName(interfaceFunction);
                if (interfaceUnitName != null
                        && !(interfaceUnitName.startsWith("pushPointer")
                        || interfaceUnitName.startsWith("popPointer")
                        || interfaceUnitName.startsWith("lookPointer"))) {
                    functionDecls = adEnv.curSymbolTable().getFunctionDecl(interfaceUnitName, null, null, false);
                    functionDecl = functionDecls == null ? null : functionDecls.head;
                    if (functionDecl != null) {
                        interfaceUnit = functionDecl.unit();
                        diffInterfaceUnit = adEnv.getDiffOfUnit(interfaceUnit, adEnv.curActivity(), curDiffUnitSort());
                        if (diffInterfaceUnit == null && interfaceUnit.mustDifferentiateSplit()) {
                            result = true;
                        }
                    }
                }
            } else if (tree.down(2).down(i).opCode() == ILLang.op_moduleProcedure) {
                Tree[] moduleNames = tree.down(2).down(i).children();
                for (int j = moduleNames.length - 1; j >= 0; --j) {
                    interfaceUnitName = ILUtils.getIdentString(moduleNames[j]);
                    functionDecls = adEnv.curSymbolTable().getFunctionDecl(interfaceUnitName, null, null, false);
                    if (functionDecls != null) {
                        functionDecl = functionDecls.head;
                        assert functionDecl != null;
                        interfaceUnit = functionDecl.unit();
                        diffInterfaceUnit = adEnv.getDiffOfUnit(interfaceUnit, adEnv.curActivity(), curDiffUnitSort());
                        if (callGraphDifferentiator().unitsHaveDiff.retrieve(interfaceUnit) == Boolean.TRUE
                                || TapEnv.mustContext() && interfaceUnit.isAContext()) {
                            if (diffInterfaceUnit == null && interfaceUnit.mustDifferentiateSplit()) {
                                result = true;
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private Tree differentiateInterfaceDeclaration(Tree tree, int interfaceDiffMode, Block block,
                                                   SymbolTable diffSymbolTable,
                                                   Instruction copyInstrInDiff,
                                                   TapList<Unit> differentiatedInterfaces) {
        if (adEnv.traceCurBlock != 0) {
            TapEnv.printlnOnTrace(" Differentiating interface declaration ");
            TapEnv.printlnOnTrace("      " + ILUtils.toString(tree, adEnv.curActivity()));
            TapEnv.printlnOnTrace("      for activity " + adEnv.curActivity() + ", in mode " + interfaceDiffMode);
        }
        int origLanguage = block.symbolTable.unit.language();
        Tree interfaceFunction;
        String interfaceUnitName;
        FunctionDecl functionDecl;
        TapList<FunctionDecl> functionDecls;
        TapList<Tree> diffInterfaces = null;
        TapList<NewSymbolHolder> diffNSHlist = null;
        ActivityPattern patternAround;
        Unit interfaceUnit, diffInterfaceUnit;
        // [llh] TODO: bug to fix: sometimes copyInstrInDiff.tree and tree are the same :-(
        copyInstrInDiff.tree = ILUtils.copy(copyInstrInDiff.tree);
        // change use M en use M_D dans l'interfaceDecl copiee
        updateModulesAndCopiedUnitsInCopiedInterface(copyInstrInDiff.tree, diffSymbolTable, DiffConstants.ORIGCOPY);
        TapList<Unit> generatedDiffInterfaces = differentiatedInterfaces.tail;
        for (int i = tree.down(2).length(); i > 0; --i) {
            Tree interfaceElement = tree.down(2).down(i);
            if (interfaceElement.opCode() == ILLang.op_interface) {
                interfaceFunction = interfaceElement.down(1);
                interfaceUnitName = ILUtils.getUnitName(interfaceFunction);
                if (interfaceUnitName != null
                        && !(interfaceUnitName.startsWith("pushPointer")
                        || interfaceUnitName.startsWith("popPointer")
                        || interfaceUnitName.startsWith("lookPointer"))) {
                    functionDecls = adEnv.curSymbolTable().getFunctionDecl(interfaceUnitName, null, null, false);
// System.out.println("GETFUNCTIONDECL "+interfaceUnitName+" IN ST "+adEnv.curSymbolTable()+" ==> "+functionDecls) ;
                    functionDecl = functionDecls == null ? null : functionDecls.head;

                    if (functionDecl != null) {
                        interfaceUnit = functionDecl.unit();
                        UnitDiffInfo interfaceUnitInfo = callGraphDifferentiator().getUnitDiffInfo(interfaceUnit);
// System.out.println("INTERFACE FUNCTION DECL:"+functionDecl+" UNIT:"+interfaceUnit+" DIFFINFOS:"+interfaceUnitInfo) ;
                        TapList<TapPair<ActivityPattern, Unit>> patternsAndUnitsCalledHere;
                        // Patch to work around a bug (on Boeing code), but this bug should rather be fixed!
                        if (interfaceUnitInfo == null) {
                            TapEnv.toolError("Empty diff info on interface Unit " + interfaceUnit + " on " + ILUtils.toString(interfaceElement));
                            patternsAndUnitsCalledHere = null;
                        } else if (adEnv.curActivity() != null && adEnv.curActivity().isDisconnected()) {
                            patternsAndUnitsCalledHere =
                                    interfaceUnitInfo.getAllDiffPatternsAndUnits(interfaceDiffMode);
                        } else if (adEnv.curActivity() == null) {
                            // cf set11/vpf12, interfaces declared in a module
                            patternsAndUnitsCalledHere =
                                    interfaceUnitInfo.getAllDiffPatternsAndUnits(interfaceDiffMode);
                            if (interfaceDiffMode == DiffConstants.ADJOINT) {
                                patternsAndUnitsCalledHere = TapList.append(patternsAndUnitsCalledHere,
                                        interfaceUnitInfo.getAllDiffPatternsAndUnits(DiffConstants.ADJOINT_FWD));
                                patternsAndUnitsCalledHere = TapList.append(patternsAndUnitsCalledHere,
                                        interfaceUnitInfo.getAllDiffPatternsAndUnits(DiffConstants.ADJOINT_BWD));
                            }
                        } else {
                            TapList<ActivityPattern> patternsCalledHere = patternsCalledByPattern(interfaceUnit, adEnv.curActivity());
// System.out.println("  patternsCalledHere by "+adEnv.curActivity()+" :"+patternsCalledHere) ;
                            if (patternsCalledHere == null) {
                                patternsAndUnitsCalledHere = null;
                            } else {
                                ActivityPattern patternCalledHere = patternsCalledHere.head;
                                Unit diffU = adEnv.getDiffOfUnit(interfaceUnit, patternCalledHere, interfaceDiffMode);
                                if (diffU == null) { // may happen if interfaceUnit is external
                                    patternsAndUnitsCalledHere = null;
                                } else {
                                    patternsAndUnitsCalledHere =
                                            new TapList<>(new TapPair<ActivityPattern, Unit>(patternCalledHere, diffU), null);
                                }
                                if (patternsCalledHere.tail != null) { // in principle, only one called pattern per calling pattern.
                                    TapEnv.toolError("There are two or more activity patterns for diff interface " + interfaceUnitName + " called by " + adEnv.curActivity());

                                }
                            }
                        }
                        patternAround = adEnv.curActivity();
                        if (adEnv.traceCurDifferentiation) {
                            TapEnv.printlnOnTrace("   -- differentiating interface of " + interfaceUnitName + " for activities-units " + patternsAndUnitsCalledHere);
                        }
                        while (patternsAndUnitsCalledHere != null) {
                            adEnv.setCurActivity(patternsAndUnitsCalledHere.head.first);
                            diffInterfaceUnit = patternsAndUnitsCalledHere.head.second;
                            Unit oldCurUnit = adEnv.curUnit();
                            adEnv.setCurUnitEtc(interfaceUnit);
                            adEnv.pushCurDiffUnit(diffInterfaceUnit);
                            Tree diffInterfaceNameTree = varRefDifferentiator().diffSymbolName(
                                    adEnv.curActivity(), interfaceUnitName, diffSymbolTable.basisSymbolTable(),
                                    false, true, false, interfaceElement, null, false,
                                    diffInterfaceUnit.sortOfDiffUnit, diffInterfaceUnit.sortOfDiffUnit, DiffConstants.PROC, null);
                            boolean mustDiffInterfaceTree = false;
                            if (interfaceDiffMode == DiffConstants.ADJOINT_FWD) {
                                mustDiffInterfaceTree = true;
                            } else if (diffInterfaceUnit != null &&
                                    (callGraphDifferentiator().unitsHaveDiff.retrieve(interfaceUnit) == Boolean.TRUE
                                            || TapEnv.mustContext() && interfaceUnit.isAContext())) {
                                mustDiffInterfaceTree = true;
                                // TODO a nettoyer ....
                                NewSymbolHolder diffInterfaceUnitNSH =
                                        NewSymbolHolder.getNewSymbolHolder(diffInterfaceNameTree);
                                diffNSHlist = new TapList<>(diffInterfaceUnitNSH, diffNSHlist);
                                if (interfaceDiffMode == DiffConstants.ADJOINT_BWD) {
                                    Tree diffInterfaceNameTreeFWD = varRefDifferentiator().diffSymbolName(
                                            adEnv.curActivity(), interfaceUnitName,
                                            diffSymbolTable.basisSymbolTable(),
                                            false, true, false, interfaceElement, null,
                                            false, DiffConstants.ADJOINT_FWD,
                                            DiffConstants.ADJOINT_FWD, DiffConstants.PROC, null);
                                    diffInterfaceUnitNSH = NewSymbolHolder.getNewSymbolHolder(diffInterfaceNameTreeFWD);
                                    diffNSHlist = new TapList<>(diffInterfaceUnitNSH, diffNSHlist);
                                }
                            }
                            if (mustDiffInterfaceTree && !TapList.contains(generatedDiffInterfaces, diffInterfaceUnit)) {
                                generatedDiffInterfaces = new TapList<>(diffInterfaceUnit, generatedDiffInterfaces);
                                if (adEnv.traceCurDifferentiation) {
                                    TapEnv.printlnOnTrace("BUILDDIFFINTERFACETREE " + interfaceElement + " DIFF OF UNIT " + interfaceUnit + " DIFF " + diffInterfaceUnit + " CURUNIT: " + adEnv.curUnit());
                                }
                                Tree diffInterfaceTree = buildDiffInterfaceTree(interfaceDiffMode, origLanguage,
                                        interfaceElement, interfaceUnit, diffInterfaceUnit,
                                        diffInterfaceNameTree);
                                if (adEnv.traceCurDifferentiation) {
                                    TapEnv.printlnOnTrace("====>DIFFINTERFACETREE " + diffInterfaceTree);
                                }
                                diffInterfaces = new TapList<>(diffInterfaceTree, diffInterfaces);
                            }
                            adEnv.setCurUnitEtc(oldCurUnit);
                            adEnv.popCurDiffUnit();
                            patternsAndUnitsCalledHere = patternsAndUnitsCalledHere.tail;
                        }
                        adEnv.setCurActivity(patternAround);
                    }
                }
                differentiatedInterfaces.tail = generatedDiffInterfaces;
            } else if (interfaceElement.opCode() == ILLang.op_moduleProcedure) {
                Tree[] moduleNames = interfaceElement.children();
                TapList<Tree> diffModuleNames = null;
                for (int j = moduleNames.length - 1; j >= 0; --j) {
                    interfaceUnitName = ILUtils.getIdentString(moduleNames[j]);
                    functionDecls = adEnv.curSymbolTable().getFunctionDecl(interfaceUnitName, null,
                            null, false);
                    if (functionDecls != null) {
                        functionDecl = functionDecls.head;
                        assert functionDecl != null;
                        interfaceUnit = functionDecl.unit();
                        UnitDiffInfo interfaceUnitInfo = callGraphDifferentiator().getUnitDiffInfo(interfaceUnit);
                        patternAround = adEnv.curActivity();
                        TapList<ActivityPattern> patternsCalledHere;
                        if (adEnv.curActivity() == null || adEnv.curActivity().isDisconnected()) {
                            patternsCalledHere =
                                    interfaceUnitInfo.getAllDiffPatterns(interfaceDiffMode);
                        } else {
                            patternsCalledHere =
                                    patternsCalledByPattern(interfaceUnit, adEnv.curActivity());
                        }
                        while (patternsCalledHere != null) {
                            adEnv.setCurActivity(patternsCalledHere.head);
                            diffInterfaceUnit = adEnv.getDiffOfUnit(interfaceUnit, adEnv.curActivity(), interfaceDiffMode);
                            if (callGraphDifferentiator().unitsHaveDiff.retrieve(interfaceUnit) == Boolean.TRUE
                                    || TapEnv.mustContext() && interfaceUnit.isAContext()) {
                                if (diffInterfaceUnit != null) {
                                    Tree diffInterfaceNameTree = varRefDifferentiator().diffSymbolName(
                                            adEnv.curActivity(), interfaceUnitName,
                                            diffSymbolTable.basisSymbolTable(),
                                            false, true, false, interfaceElement, null, false,
                                            interfaceDiffMode, interfaceDiffMode, DiffConstants.PROC, null);
                                    NewSymbolHolder diffInterfaceUnitNSH =
                                            NewSymbolHolder.getNewSymbolHolder(diffInterfaceNameTree);
                                    diffNSHlist = new TapList<>(diffInterfaceUnitNSH, diffNSHlist);
                                    diffModuleNames = new TapList<>(diffInterfaceNameTree, diffModuleNames);
                                }
                            }
                            patternsCalledHere = patternsCalledHere.tail;
                        }
                        adEnv.setCurActivity(patternAround);
                    }
                }
                if (diffModuleNames != null) {
                    Tree resulti = ILUtils.build(ILLang.op_moduleProcedure, diffModuleNames);
                    diffInterfaces = new TapList<>(resulti, diffInterfaces);
                }
            } else {
                // op_directive
                TapEnv.toolWarning(-1, "(Differentiating interface declaration) Differentiation of " + ILUtils.toString(interfaceElement, adEnv.curActivity()) + " ignored.");
            }
        }
        Tree newDiffInterface = null;
        if (diffInterfaces != null) {
            Tree diffInterfaceNameTree;
            if (ILUtils.isNullOrNone(tree.down(1))) {
                diffInterfaceNameTree = ILUtils.build(ILLang.op_none);
            } else {
                Tree nameTree = tree.down(1);
                if (ILUtils.getIdentString(nameTree) == null) {
                    // This may happen for interfaces of predefined operators e.g. mul, assign, etc
                    // Build a dummy interface name with the internal name of the operator.
                    nameTree = ILUtils.build(ILLang.op_ident, nameTree.opName());
                }
                diffInterfaceNameTree = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), nameTree, diffSymbolTable.basisSymbolTable(),
                        false, true, false, null, false,
                        interfaceDiffMode, interfaceDiffMode, DiffConstants.PROC, null);
                NewSymbolHolder diffInterfaceUnitNSH = NewSymbolHolder.getNewSymbolHolder(diffInterfaceNameTree);
                diffNSHlist = new TapList<>(diffInterfaceUnitNSH, diffNSHlist);
            }
            newDiffInterface = ILUtils.build(ILLang.op_interfaceDecl,
                    diffInterfaceNameTree,
                    ILUtils.build(ILLang.op_interfaces, diffInterfaces),
                    ILUtils.build(ILLang.op_none));
            // change use M en use M_D dans l'interfaceDecl differentiee:
            updateModulesAndCopiedUnitsInCopiedInterface(newDiffInterface, diffSymbolTable, interfaceDiffMode);
            // Pour dire a TreeGen que c'est ok, cette interface differentiee a bien sa declaration ! :
            Instruction tmpDiffInstruction = new Instruction(newDiffInterface);
            while (diffNSHlist != null) {
                NewSymbolHolder diffInterfaceUnitNSH = diffNSHlist.head;
                if (diffInterfaceUnitNSH != null) {
                    diffInterfaceUnitNSH.setInstruction(tmpDiffInstruction);
                }
                diffNSHlist = diffNSHlist.tail;
            }
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("   --> ");
            TapEnv.printlnOnTrace("      " + ILUtils.toString(newDiffInterface, adEnv.curActivity()));
        }
        return newDiffInterface;
    }

    private Tree buildDiffInterfaceTree(int interfaceDiffMode, int origLanguage,
                                        Tree interfaceFunction, Unit interfaceUnit, Unit diffInterfaceUnit,
                                        Tree diffInterfaceNameTree) {
        String funcName = adEnv.curUnit().name();
        Tree typeSpecTree =
                adEnv.curUnit().functionTypeSpec().returnType.generateTree(diffInterfaceUnit.publicSymbolTable(), null, null, true, null);
        Tree headTree = ILUtils.copy(adEnv.curUnit().headTree());
        Tree params;
        int paramsNb;
        Tree[] interfaceContents = interfaceFunction.down(1).down(6).children();
        // Will be true if interface contents declares the type of return value in its declarations:
        // (this is not very nice, should be simplified...)
        boolean origResultDeclaredInContents = false;
        if (interfaceFunction.down(1).down(5).length() != 0) {
            params = interfaceFunction.down(1).down(5).down(1).down(3);
            assert headTree != null;
            Tree headTreeVars = headTree.down(3);
            paramsNb = Math.min(headTreeVars.length(), params.length());
            Tree[] headTreeVar = new Tree[paramsNb];
            String[] paramsVar = new String[paramsNb];
            for (int j = 0; j < paramsNb; ++j) {
                headTreeVar[j] = headTreeVars.down(j + 1);
                paramsVar[j] = ILUtils.baseName(params.down(j + 1));
            }
            for (int j = interfaceContents.length - 1; j >= 0; --j) {
                // replace arg names InterfaceUnit/Unit:
                if (interfaceContents[j].opCode() == ILLang.op_varDeclaration) {
                    if (adEnv.curUnit().isAFunction() && ILUtils.declaresVariable(interfaceContents[j], funcName)) {
                        origResultDeclaredInContents = true;
                    }
                    for (int k = 0; k < paramsNb; ++k) {
                        ILUtils.replaceIdentStringOccurences(interfaceContents[j], paramsVar[k],
                                ILUtils.copy(headTreeVar[k]));
                    }
                }
            }
        }
        Tree diffCallTree =
                procedureCallDifferentiator().differentiateProcedureHeader(headTree, null, null, null, diffInterfaceUnit.publicSymbolTable(),
                        adEnv.curUnit().publicSymbolTable(), interfaceDiffMode, null);
        diffInterfaceUnit.parametersOrModuleNameTree = ILUtils.getArguments(diffCallTree);
        Tree diffArgs =
                ILUtils.build(ILLang.op_varDeclarations,
                        diffInterfaceUnit.generateHeaderParamsList(diffInterfaceUnit.publicSymbolTable()));
        if (interfaceUnit.functionTypeSpec() != null
                && interfaceUnit.functionTypeSpec().variableArgList) {
            diffArgs.addChild(ILUtils.build(ILLang.op_variableArgList),
                    diffArgs.length() + 1);
        }
        Block blockOfDeclarations = new Block(diffInterfaceUnit.publicSymbolTable(), null, null);

        for (int j = interfaceContents.length - 1; j >= 0; --j) {
            blockOfDeclarations.addInstrHd(ILUtils.copy(interfaceContents[j]));
        }

        differentiateDeclarationsOfBlock(blockOfDeclarations, interfaceDiffMode, interfaceUnit,
                interfaceUnit.publicSymbolTable(),
                diffInterfaceUnit.privateSymbolTable(),
                origLanguage, true);

        if (diffInterfaceUnit.isAFunction() && interfaceDiffMode == DiffConstants.ADJOINT_FWD) {
            TapList<Instruction> newDeclarations = blockOfDeclarations.instructions;
            while (newDeclarations != null) {
                Tree newDecl = newDeclarations.head.tree;
                TapList<SymbolDecl> symbolDecls = blockOfDeclarations.symbolDeclDeclared(newDecl,
                        diffInterfaceUnit.publicSymbolTable());
                while (symbolDecls != null) {
                    if (symbolDecls.head.symbol.equals(interfaceUnit.name())) {
                        ILUtils.replaceIdentStringOccurences(newDecl, interfaceUnit.name(),
                                ILUtils.copy(diffInterfaceNameTree));
                    }
                    symbolDecls = symbolDecls.tail;
                }
                newDeclarations = newDeclarations.tail;
            }
        }

        if (multiDirMode()) {
            Tree newNbdirsVar =
                    varRefDifferentiator().buildDirNumberReference(diffInterfaceUnit.publicSymbolTable());
            Tree newNbdirsDecl =
                    ILUtils.build(ILLang.op_varDeclaration,
                            ILUtils.build(ILLang.op_modifiers),
                            adEnv.integerTypeSpec.generateTree(null, null, null, true, null),
                            ILUtils.build(ILLang.op_declarators, newNbdirsVar));
            blockOfDeclarations.addInstrTl(newNbdirsDecl);
        }

        boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(adEnv.curActivity());

        Tree returnTypeTree = null;
        if (!diffInterfaceUnit.isAFunction()) {
            returnTypeTree = ILUtils.build(ILLang.op_void);
        } else {
            returnTypeTree =
                    diffInterfaceUnit.buildReturnTypeTree(null, null, new ToObject<>(null), null);
        }

        // When necessary, add declaration of the original return var:
        if (adEnv.curUnit().isAFunction() && !origResultDeclaredInContents
                && formalArgsActivity[formalArgsActivity.length - 1]
                && ((adEnv.curActivity().isContext() || curDiffUnitSort() != DiffConstants.ADJOINT)
                && curDiffUnitSort() != DiffConstants.ADJOINT_BWD)) {
            // "forward-style" diff header: original result becomes a new (reference) argument
            Tree varNameTree = ILUtils.build(ILLang.op_ident, funcName);
            Tree returnVarDeclaration = ILUtils.build(ILLang.op_varDeclaration,
                    ILUtils.build(ILLang.op_modifiers),
                    typeSpecTree,
                    ILUtils.build(ILLang.op_declarators, varNameTree));
            Instruction returnInstr = new Instruction(returnVarDeclaration);
            blockOfDeclarations.addInstrTl(returnInstr);
        }

        // When necessary, add declaration of the diff of the original return var:
        if (adEnv.curUnit().isAFunction()
                && interfaceFunction.down(1).down(2).opCode() != ILLang.op_none
                && (curDiffUnitSort() != DiffConstants.ADJOINT || formalArgsActivity[formalArgsActivity.length - 1])
                //The following tests if the function return type is already given, so that
                // we must not add a var declaration of the function name as a variable.
                && (//curDiffUnitSort() != DiffConstants.ADJOINT || interfaceDiffMode != DiffConstants.ADJOINT_FWD ||
                ILUtils.isNullOrNoneOrVoid(returnTypeTree))) {
            Tree varNameTree = ILUtils.build(ILLang.op_ident, funcName);
            if (curDiffUnitSort() == DiffConstants.ADJOINT) {
                if (interfaceDiffMode == DiffConstants.ADJOINT_FWD) {
                    // In ADJOINT_FWD, differentiate the function name (func) as a function (func_fwd)
                    varNameTree = varRefDifferentiator().diffSymbolName(
                            adEnv.curActivity(), funcName, diffInterfaceUnit.publicSymbolTable(),
                            false, true, false, interfaceFunction, null, false,
                            interfaceDiffMode, interfaceDiffMode, DiffConstants.PROC, null);
                } else {
                    // interfaceDiffMode==ADJOINT_BWD or ADJOINT
                    // then differentiate the function (func) name as a variable (funcb)
                    varNameTree = varRefDifferentiator().diffSymbolName(
                            adEnv.curActivity(), funcName, diffInterfaceUnit.publicSymbolTable(),
                            true, false, false, interfaceFunction, null, false,
                            curDiffVarSort(), interfaceDiffMode, DiffConstants.PROC, null);
                }
            }
            Tree returnVarDeclaration = ILUtils.build(ILLang.op_varDeclaration,
                    ILUtils.build(ILLang.op_modifiers),
                    ILUtils.copy(typeSpecTree),
                    ILUtils.build(ILLang.op_declarators, varNameTree));
            Instruction returnInstr = new Instruction(returnVarDeclaration);
            blockOfDeclarations.addInstrTl(returnInstr);
        }

        TapList<Tree> toDiffTrees = new TapList<>(null, null);
        TapList<Tree> headDiffTrees = toDiffTrees;
        Instruction instr;
        while (blockOfDeclarations.instructions != null) {
            instr = blockOfDeclarations.instructions.head;
            toDiffTrees = toDiffTrees.placdl(instr.tree);
            blockOfDeclarations.instructions = blockOfDeclarations.instructions.tail;
        }

        Tree bindTree = interfaceFunction.down(1).down(4).getAnnotation("bind");
        Tree diffBindTree = ILUtils.copy(bindTree); // TODO differentier bindTree
        if (diffBindTree != null) {
            if (diffBindTree.opCode() == ILLang.op_accessDecl) {
                Tree[] bindExprs = diffBindTree.down(2).children();
                Tree diffBindNameTree = null;
                for (int i = 0; i < bindExprs.length && diffBindNameTree == null; ++i) {
                    if (bindExprs[i].opCode() == ILLang.op_nameEq
                            && ILUtils.isIdent(bindExprs[i].down(1), "name", false)) {
                        diffBindNameTree = bindExprs[i];
                    }
                }
                if (diffBindNameTree != null) {
                    String otherLangName = diffBindNameTree.down(2).stringValue();
                    TapList<FunctionDecl> otherLangFuncDecls =
                            adEnv.srcCallGraph().cRootSymbolTable().getFunctionDecl(
                                    otherLangName, null, null, false);
                    Unit otherLangSourceUnit =
                            (otherLangFuncDecls == null ? null : otherLangFuncDecls.head.unit());
                    if (otherLangSourceUnit != null) {
                        UnitDiffInfo otherLangDiffInfo = callGraphDifferentiator().getUnitDiffInfo(otherLangSourceUnit);

                        Unit otherLangDiffUnit = otherLangDiffInfo.getDiffForModeAndActivity(interfaceDiffMode, adEnv.curActivity());
                        if (otherLangDiffUnit != null) {
                            Tree diffHead = otherLangDiffUnit.headTree();
                            // ATTENTION: diffHead might be null if the called unit is not yet differentiated
                            // -> we must precompute all differentiated headers before differentiation!
                            diffBindNameTree.setChild(ILUtils.copy(ILUtils.getCalledName(diffHead)), 2);
                        }
                    }
                }
            }
            diffInterfaceNameTree.setAnnotation("bind", diffBindTree);
        }

        Tree explicitReturnVar = ILUtils.getCalledName(diffCallTree).getAnnotation("explicitReturnVar");
        if (explicitReturnVar != null) {
            diffInterfaceNameTree.setAnnotation("explicitReturnVar", explicitReturnVar);
        }
        // useful e.g. to restrict updateAllUsagesInBlock() :
        diffInterfaceNameTree.setAnnotation("sourceUnit", interfaceUnit);

        return ILUtils.build(ILLang.op_interface,
                ILUtils.build(ILLang.op_function,
                        ILUtils.copy(interfaceFunction.down(1).down(1)),
                        returnTypeTree,
                        ILUtils.build(ILLang.op_none),
                        diffInterfaceNameTree,
                        diffArgs,
                        ILUtils.build(ILLang.op_blockStatement, headDiffTrees.tail)));
    }

    /**
     * diffBlock is a Block of the differentiated code, but it still contains only
     * the declarations copied from its original source block. This method inserts
     * the corresponding declarations for the differentiated variables,types,etc...
     *
     * @param originalST   The original SymbolTable of the original source Block
     * @param forInterface true when we are differentiating an INTERFACE declaration, in which case we don't want to differentiate
     *                     formal arguments that are only active inside the diff routine and therefore have no diff formal argument.
     */
    protected void differentiateDeclarationsOfBlock(Block diffBlock, int differentiationMode,
                                                    Unit unit, SymbolTable originalST,
                                                    SymbolTable curDiffST, int origLanguage, boolean forInterface) {
// [FEWER NONREGRESSION] LEFT-JUSTIFIED CODE ONLY FOR FEWER DIFFS IN NONREGRESSION. TODO: REMOVE AND ACCEPT AD:
// [llh 11Jun18] THE LEFT-JUSTIFIED CODE IS HERE ONLY TO LIMIT DIFFERENCES DUE TO THE ORDER OF MODULES IN GENERATED CODE:
// THE ORDER CURRENTLY "ACCEPTED" IS REVERSED. A BETTER ORDERING SHOULD FOLLOW THE ORIGINAL CODE ORDER
// THE BETTER ORDERING THAT WE WANT IS:
//    SAME AS ORIGINAL CODE, DIFF PACKAGES REPLACING THEIR PRIMAL, DIFF VAR DECLARATIONS AFTER THEIR PRIMAL,
//    DIFF PROCEDURES AFTER THEIR PRIMAL, *_NODIFF THEN *_D THEN *_B THEN *_FWD THEN *_BWD.
        adEnv.pushCurSymbolTable(originalST);
        boolean oldCurUnitIsContext = adEnv.curUnitIsContext;
        if (unit != null) {
            adEnv.pushCurUnitEtc(unit);
            adEnv.pushCurVectorLen(adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND));
            adEnv.pushCurDiffVectorLen(adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind));
            if (adEnv.curUnit().hasParamElemsInfo() && TapEnv.mustContext()) {
                if (adEnv.curActivity() != null) //may be null for Modules!
                {
                    adEnv.curUnitIsContext =
                            // [llh 10/6/2016] this unitIsContext() is very dubious!
                            activityAnalyzer().unitIsContext(adEnv.curActivity());
                }
            }
        }
        // True iff there is only one diff counterpart for this Unit,
        // that contains inside all the diff modes requested:
        boolean allDiffModesInside = unit == null || unit.isPackage();

        if (TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003) != null && adEnv.curUnit().isFortran()) {
            moduleImportsAATypesModule(curDiffST);
        }

        TapList<Instruction> hdDeclarations = new TapList<>(null, diffBlock.instructions);
        TapList<Instruction> inDeclarations = hdDeclarations;
        TapList<Instruction> newDeclarationsDone = null;
        int previousBlockTraceLevel = adEnv.traceCurBlock;
        adEnv.traceCurBlock = 0;
        if (adEnv.traceCurDifferentiation) {
            adEnv.traceCurBlock = 1;
            TapEnv.printlnOnTrace("");
            TapEnv.printlnOnTrace("*** NOW Differentiating Declarations of Block: @" + Integer.toHexString(diffBlock.hashCode()) + " " + diffBlock);
        }

        while (inDeclarations.tail != null) {
            adEnv.setCurInstruction(inDeclarations.tail.head);
            boolean removePrimalDeclaration = false;
            if (!TapList.contains(newDeclarationsDone, adEnv.curInstruction())) {
                TapList<Instruction> newDeclarations = null;
                Instruction newDeclaration;
                Unit definedUnit = adEnv.curInstruction().isUnitDefinitionStub();
                if (definedUnit != null) { // Differentiation of a Unit definition:
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("    --- differentiating definition location of " + definedUnit);
                    }
                    UnitDiffInfo defUnitDiffInfo =
                            callGraphDifferentiator().getUnitDiffInfo(definedUnit);
                    if (defUnitDiffInfo != null) {
                        Unit copiedDefUnit = callGraphDifferentiator().unitsHavePrimal.retrieve(definedUnit) == Boolean.TRUE ? defUnitDiffInfo.getCopyForDiff() : null;
                        if (copiedDefUnit == null) //no primal copy of the original Unit in the diff code
                        {
                            removePrimalDeclaration = true;
                        } else {
                            adEnv.curInstruction().updateUnitDefinitionStub(copiedDefUnit);
                        }
                        if (definedUnit.isModule()) {
                            removePrimalDeclaration = true;
                            if (copiedDefUnit != null) {
                                newDeclarations = new TapList<>(adEnv.curInstruction(), null);
                            }
                        } else {
                            if (!definedUnit.isPackage()) {
                                // A package stub (Module, Class) is just updated by its unique diff package:
                                // don't add a new copy of this diff package!
                                TapList<Unit> diffDefUnits;
                                if (allDiffModesInside) {
                                    diffDefUnits = defUnitDiffInfo.getAllDiffs();
                                } else {
                                    diffDefUnits = defUnitDiffInfo.getAllDiffs(differentiationMode);
                                }
                                diffDefUnits = TapList.nreverse(diffDefUnits); // if needed for non-regression consistent order...
                                newDeclarations = null;
                                while (diffDefUnits != null) {
                                    newDeclarations =
                                            new TapList<>(Instruction.createUnitDefinitionStub(diffDefUnits.head, diffBlock),
                                                    newDeclarations);
                                    diffDefUnits = diffDefUnits.tail;
                                }
                            }
                        }
                    }
                } else if (adEnv.curInstruction().isADeclaration()) {
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("    --- differentiating declaration " + adEnv.curInstruction());
                    }
                    TapList<Instruction>[] newDeclarationsR =
                        differentiateInstructionDeclaration(adEnv.curInstruction(), differentiationMode,
                                adEnv.curInstruction(), null, adEnv.curInstruction().tree, allDiffModesInside, curDiffST,
                                diffBlock, null, null, null, null, null, null, null, null, origLanguage, false, forInterface);
                    if (adEnv.curInstruction().tree == null
                        || adEnv.curInstruction().tree.opCode() == ILLang.op_none
                        || (adEnv.curInstruction().tree.opCode() == ILLang.op_useDecl
                            && !ILUtils.isIdent(adEnv.curInstruction().tree.down(1), "aatypes", false))) {
                        removePrimalDeclaration = true;
                    }
                    newDeclarations = null ;
                    for (int iReplic=0 ; iReplic<newDeclarationsR.length ; ++iReplic) {
                        newDeclarations = TapList.append(newDeclarations, newDeclarationsR[iReplic]) ;
                    }
                    //newDeclarations = TapList.nreverse(newDeclarations) ; // [FEWER NONREGRESSION] if needed for non-regression consistent order...
                }
                if (definedUnit == null) {
                    if (removePrimalDeclaration) {
                        inDeclarations.tail = inDeclarations.tail.tail;
                    } else {
                        inDeclarations = inDeclarations.tail;
                    }
                }
                while (newDeclarations != null) {
                    newDeclaration = newDeclarations.head;
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("      -> (DIFF DECL BLOCK+) " + newDeclaration);
                    }
// [FEWER NONREGRESSION] if needed for non-regression consistent order...
// if (unit!=null && unit.isModule()) {
// hdDeclarations.placdl(newDeclaration) ;
// } else {
                    inDeclarations = inDeclarations.placdl(newDeclaration);
// }
                    if (newDeclaration.block == null) {
                        newDeclaration.block = diffBlock;
                    }
                    newDeclarationsDone = new TapList<>(newDeclaration, newDeclarationsDone);
                    newDeclarations = newDeclarations.tail;
                }
                if (definedUnit != null) {
                    if (removePrimalDeclaration) {
                        inDeclarations.tail = inDeclarations.tail.tail;
                    } else {
                        inDeclarations = inDeclarations.tail;
                    }
                }
            } else {
                inDeclarations = inDeclarations.tail;
            }
        }
        diffBlock.instructions = hdDeclarations.tail;
        if (unit != null) {
            adEnv.popCurDiffVectorLen();
            adEnv.popCurVectorLen();
            adEnv.popCurUnitEtc();
            adEnv.curUnitIsContext = oldCurUnitIsContext;
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("*** DiffBlock @" + Integer.toHexString(diffBlock.hashCode()) + " ---> " + diffBlock.instructions);
        }
        adEnv.traceCurBlock = previousBlockTraceLevel;
        adEnv.popCurSymbolTable();
    }

    private void moduleImportsAATypesModule(SymbolTable symbolTable) {
        if (symbolTable.unit != null && symbolTable.unit.isFortran()) {
            Unit origDiffUnit = symbolTable.unit;
            symbolTable = symbolTable.unit.publicSymbolTable();
            while (symbolTable.unit != null
                    && symbolTable.unit.upperLevelUnit() != null
                    && !symbolTable.unit.isModule()
                    && !symbolTable.unit.isTranslationUnit()
                    && !symbolTable.unit.upperLevelUnit().isTranslationUnit()) {
                symbolTable = symbolTable.unit.upperLevelUnit().publicSymbolTable();
            }
            if (symbolTable.basisSymbolTable() != null
                    && symbolTable.unit.isModule() && symbolTable == symbolTable.unit.privateSymbolTable()) {
                symbolTable = symbolTable.basisSymbolTable();
            }
            if (symbolTable.unit != null && !symbolTable.isTranslationUnitSymbolTable()) {
                Tree useDeclTree = ILUtils.build(ILLang.op_useDecl,
                        ILUtils.build(ILLang.op_ident,
                                TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).name()),
                        ILUtils.build(ILLang.op_renamedVisibles));
                Instruction useInstr = new Instruction(useDeclTree);
                useInstr.isDifferentiated = true;
                symbolTable.declarationsBlock.addInstrHdIfNotPresent(useInstr);
                CallGraph.addCallArrow(symbolTable.unit, SymbolTableConstants.IMPORTS,
                        TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003));
                if (symbolTable.unit.language() != TapEnv.FORTRAN2003) {
                    TapEnv.commandWarning(-1, "-association by address option generates FORTRAN2003");
                    origDiffUnit.setLanguageAndUp(TapEnv.FORTRAN2003);
                    Unit origUnit = origDiffUnit.origUnit;
                    UnitDiffInfo diffInfo = callGraphDifferentiator().getUnitDiffInfo(origUnit);
                    TapList<Unit> diffUnits;
                    for (diffUnits = diffInfo.getAllDiffsPlusCopy(); diffUnits != null; diffUnits = diffUnits.tail) {
                        diffUnits.head.setLanguageAndUp(TapEnv.FORTRAN2003);
                    }
                }
            }
        }
    }

    protected void updateModulesAndCopiedUnitsInCopiedInterface(Tree copyTree, SymbolTable diffSymbolTable, int diffMode) {
        Tree newUnitNameTree;
        // curUnit contient la declaration d'interface copiee copyTree
        TapList<FunctionDecl> privateInterfaces = adEnv.curUnit().privateSymbolTable().getAllInterfacedFunctionDecls();
        TapList<FunctionDecl> publicInterfaces = adEnv.curUnit().publicSymbolTable().getAllInterfacedFunctionDecls();
        TapList<Unit> usedModulesInDefinition = null;
        usedModulesInDefinition = buildModulesList(usedModulesInDefinition, privateInterfaces);
        usedModulesInDefinition = buildModulesList(usedModulesInDefinition, publicInterfaces);
        TapList<String> usedModulesInInterface = ILUtils.getModulesFromInterfaceDecl(copyTree);

        while (usedModulesInInterface != null) {
            String moduleName = usedModulesInInterface.head;
            FunctionDecl moduleDecl = diffSymbolTable.getModuleDecl(moduleName);
            Unit moduleUnit = moduleDecl == null ? null : moduleDecl.unit();
            boolean isDifferentiated = moduleUnit != null && moduleUnit.origUnit != null;
            if (!(TapEnv.associationByAddress() && "AATypes".equals(moduleName))) {
                if (!isDifferentiated
                        && !moduleNameInModulesList(usedModulesInDefinition, moduleName)) {
                    ILUtils.removeUsedModuleFromInterfaceDecl(copyTree, moduleName);
                    TapEnv.fileWarning(TapEnv.MSG_WARN, copyTree, "(AD30) " + moduleName + " removed from interface declaration");
                } else {
                    Unit diffModule = callGraphDifferentiator().getUnitDiffInfo(moduleUnit).getDiff();
                    if (diffModule != null && diffModule.isDiffPackage()) {
                        newUnitNameTree =
                                varRefDifferentiator().diffSymbolName(null, moduleName, diffSymbolTable,
                                        false, false, true, null, null, false,
                                        DiffConstants.DIFFERENTIATED, DiffConstants.DIFFERENTIATED, DiffConstants.MOD, null);
                        ILUtils.replaceUsedModule(copyTree, moduleName, newUnitNameTree);
                    }
                }
            }
            usedModulesInInterface = usedModulesInInterface.tail;
        }
        // Now update names of copied procedures with their copied name (e.g. *_C[BD] or *_nodiff) when necessary:
        Tree[] interfaces = copyTree.down(2).children();
        Tree interfaceTree, functionInterfaceTree;
        String interfacedName;
        FunctionDecl interfacedDecl;
        TapList<FunctionDecl> interfacedDecls;
        Unit interfacedUnit, origCopyInterfacedUnit;
        for (int i = interfaces.length - 1; i >= 0; --i) {
            interfaceTree = interfaces[i];
            if (interfaceTree.opCode() == ILLang.op_interface &&
                    interfaceTree.down(1).opCode() == ILLang.op_function) {
                functionInterfaceTree = interfaceTree.down(1);
                interfacedName = ILUtils.getIdentString(functionInterfaceTree.down(4));
                interfacedDecls = adEnv.curUnit().privateSymbolTable().getFunctionDecl(interfacedName, null, null, false);
                interfacedDecl = interfacedDecls == null ? null : interfacedDecls.head;
                interfacedUnit = interfacedDecl == null ? null : interfacedDecl.unit();
// if (interfacedUnit != null) System.out.println("INTERFACEDUNIT:"+interfacedUnit+" ISTOPINFILE:"+interfacedUnit.isTopInFile()+" UPPER:"+interfacedUnit.upperLevelUnit()+" ISINTERFACE:"+interfacedUnit.isInterface());
                if (interfacedUnit != null &&
                    interfacedUnit.functionTypeSpec() != null &&
                    (interfacedUnit.isTopInFile() || interfacedUnit.isInterface()) &&
                    interfacedUnit.importedModules() != null) {
                    origCopyInterfacedUnit = adEnv.copiedUnits.retrieve(interfacedUnit);
                    if (origCopyInterfacedUnit != null) {
                        Tree oldUnitNameTree = functionInterfaceTree.cutChild(4);
                        newUnitNameTree = varRefDifferentiator().diffSymbolName(
                                adEnv.curActivity(), interfacedName,
                                diffSymbolTable, false, true, false, null,
                                null, false, DiffConstants.ORIGCOPY,
                                DiffConstants.ORIGCOPY, DiffConstants.PROC, null);
                        Tree existingExplicitReturn =
                                oldUnitNameTree.getAnnotation("explicitReturnVar");
                        if (interfacedUnit.isAFunction()) {
                            newUnitNameTree.setAnnotation("explicitReturnVar",
                                    ILUtils.isNullOrNone(existingExplicitReturn)
                                            ? ILUtils.build(ILLang.op_ident, interfacedName)
                                            : existingExplicitReturn);
                        }
                        functionInterfaceTree.setChild(newUnitNameTree, 4);
                    }
                }
                // Update the primal name in the bind(name) annotation:
                Tree bindTree = functionInterfaceTree.down(4).getAnnotation("bind");
                if (diffMode == DiffConstants.ORIGCOPY && bindTree != null) {
                    Tree primalBindTree = ILUtils.copy(bindTree);
                    functionInterfaceTree.down(4).setAnnotation("bind", primalBindTree);
                    if (primalBindTree.opCode() == ILLang.op_accessDecl) {
                        Tree[] bindExprs = primalBindTree.down(2).children();
                        Tree primalBindNameTree = null;
                        for (int ii = 0; ii < bindExprs.length && primalBindNameTree == null; ++ii) {
                            if (bindExprs[ii].opCode() == ILLang.op_nameEq
                                    && ILUtils.isIdent(bindExprs[ii].down(1), "name", false)) {
                                primalBindNameTree = bindExprs[ii];
                            }
                        }
                        if (primalBindNameTree != null) {
                            String otherLangName = primalBindNameTree.down(2).stringValue();
                            Tree newOtherLangNameTree = varRefDifferentiator().diffSymbolName(
                                    adEnv.curActivity(), otherLangName,
                                    diffSymbolTable, false, true, false, null,
                                    null, false, DiffConstants.ORIGCOPY,
                                    DiffConstants.ORIGCOPY, DiffConstants.PROC, null);
                            primalBindNameTree.setChild(newOtherLangNameTree, 2);
                        }
                    }
                }
                if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
                    Tree useDeclTree = ILUtils.build(ILLang.op_useDecl,
                            ILUtils.build(ILLang.op_ident,
                                    TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).name()),
                            ILUtils.build(ILLang.op_renamedVisibles));
                    functionInterfaceTree.down(6).addChild(useDeclTree, 1);
                }
            }
        }
    }

    private TapList<Unit> buildModulesList(TapList<Unit> modules, TapList<FunctionDecl> functions) {
        FunctionDecl func;
        while (functions != null) {
            func = functions.head;
            modules = TapList.union(modules, func.unit().importedModules());
            modules = TapList.union(modules, func.unit().otherImportedModules);
            functions = functions.tail;
        }
        return modules;
    }

    /**
     * Differentiate a list of declarators that appear in a declaration such as COMMON, EQUIVALENCE, DATA...
     *
     * @param tree                    the primal list of declarators
     * @param contextExpression       the complete primal declaration that contains tree
     * @param diffSymbolTable         the SymbolTable of the future location of the differentiated declaration.
     * @param activityMap             Specially for COMMONs, this COMMON's activity map.
     * @param offset                  Specially for COMMONs, the current offset in the activityMap.
     * @param toExtraDiffDeclarations a container for a possible list of extra differentiated declarations needed.
     * @return the corresponding list of differentiated declarators
     */
    private Tree diffListDecls(Tree tree, Tree contextExpression, SymbolTable diffSymbolTable,
                               MemMap activityMap, ToInt offset, TapList<Tree> toExtraDiffDeclarations) {
        switch (tree.opCode()) {
            case ILLang.op_iterativeVariableRef: {
                Tree result = diffListDecls(tree.down(1), contextExpression, diffSymbolTable, null, null, null);
                if (result != null) {
                    result = ILUtils.build(ILLang.op_iterativeVariableRef, result, ILUtils.copy(tree.down(2)));
                }
                return result;
            }
            case ILLang.op_equivalence:
            case ILLang.op_expressions:
            case ILLang.op_varDeclarators: {
                Tree[] refs = tree.children();
                TapList<Tree> diffRefs = null;
                Tree diffRef;
                for (int i = 0; i < refs.length; ++i) {
                    diffRef = diffListDecls(refs[i], contextExpression, diffSymbolTable, activityMap, offset, toExtraDiffDeclarations);
                    if (diffRef != null) {
                        diffRefs = new TapList<>(diffRef, diffRefs);
                    }
                }
                return diffRefs == null ? null : ILUtils.build(tree.opCode(), TapList.nreverse(diffRefs));
            }
            default: {
                String baseName = ILUtils.baseName(tree);
                SymbolDecl symbolDecl =
                        (baseName == null ? null : diffSymbolTable.getSymbolDecl(baseName));
                int size = (symbolDecl instanceof VariableDecl ? symbolDecl.type().size() : 0);
                Tree result = null;
                boolean activeRegion = false ;
                if (offset != null) {
                    activeRegion = activityMap.isActiveRegion(offset.get(), (size<=0 ? -1 : size), size<=0) ;
                }
                if (symbolDecl != null
                    && symbolDecl.isA(SymbolTableConstants.VARIABLE)
                    && (symbolDecl.isActiveSymbolDecl() || activeRegion)) {
                    Tree diffDeclarator =
                            varRefDifferentiator().diffVarRef(adEnv.curActivity(), tree, diffSymbolTable, false,
                                    contextExpression, false, true, null);
                    if (offset == null || activeRegion /*was (boundary != null && boundary.active())*/) {
                        result = diffDeclarator;
                    } else {
                        // The diff variable exists, but must not appear in the COMMON.
                        // When the original declaration in the primal common declared a dimension,
                        // we must create an additional DIMENSION declaration for the diff variable:
                        if (diffDeclarator.opCode() == ILLang.op_arrayDeclarator) {
                            toExtraDiffDeclarations.placdl(ILUtils.build(ILLang.op_varDimDeclaration, diffDeclarator));
                        }
                    }
                }
                if (offset != null) {
                    offset.set(offset.get() + size);
                }
                return result;
            }
        }
    }

    private Tree differentiateCommonDeclaration(SymbolTable diffSymbolTable, Tree commonTree, TapList<Tree> toExtraDiffDeclarations) {
        Tree diffCommonTree = null;
        String commonName = ILUtils.getIdentString(commonTree.down(1));
        String commonNameInMap = commonName;
        if (commonName == null || commonName.isEmpty()) {
            commonName = "//";
            commonNameInMap = "";
        }
        MemMap commonActivityMap = adEnv.commonActivityMemoryMap.getSetMemMap(commonNameInMap);
        Tree diffElements = diffListDecls(commonTree.down(2), commonTree, diffSymbolTable,
                                          commonActivityMap, new ToInt(0), toExtraDiffDeclarations);
        if (diffElements != null) {
            String diffCommonName = differentiateCommonName(commonName, curDiffUnitSort());
            diffCommonTree = ILUtils.build(ILLang.op_common,
                    ILUtils.build(ILLang.op_ident, diffCommonName),
                    diffElements);
        }
        return diffCommonTree;
    }

    /**
     * Generate a differentiated common name, avoiding common names already in use.
     *
     * @param commonName the original common name.
     * @param diffSort   the sort of differentiation (e.g. ORIGCOPY, TANGENT, ADJOINT).
     * @return the differentiated common name.
     */
    protected String differentiateCommonName(String commonName, int diffSort) {
        String primalRootCommonName = commonName.substring(1, commonName.length() - 1);
        if ("".equals(primalRootCommonName)) {
            primalRootCommonName = "blank";
        }
        String diffRootCommonName = TapEnv.extendStringWithSuffix(primalRootCommonName, suffixes()[diffSort][DiffConstants.PROC]) ;
        String diffCommonName = "/"+diffRootCommonName+"/" ;
        int counter = 0 ;
        while (TapList.containsEquals(adEnv.diffCallGraph().usedCommonNames, diffCommonName)) {
            diffCommonName = "/"+diffRootCommonName+counter+"/" ;
            ++counter ;
        }
        return diffCommonName ;
    }

    private Tree differentiateEquivalenceDeclaration(SymbolTable diffSymbolTable, Tree equivTree) {
        return diffListDecls(equivTree, equivTree, diffSymbolTable, null, null, null);
    }

    private TapList<Tree> diffDeclNames(Tree declTree, boolean allDiffModesInside, SymbolTable diffSymbolTable, ToObject<TapList<Tree>> toDiffdeclsForFwd) {
        TapList<Tree> result = null;
        if (allDiffModesInside) {
            if (TapEnv.mustTangent()) {
                adEnv.pushCurDiffSorts(DiffConstants.TANGENT, DiffConstants.TANGENT);
                result = diffDeclNames(declTree, diffSymbolTable, null);
                adEnv.popCurDiffSorts();
            }
            if (TapEnv.mustAdjoint()) {
                adEnv.pushCurDiffSorts(DiffConstants.ADJOINT, DiffConstants.ADJOINT);
                result = TapList.append(result, diffDeclNames(declTree, diffSymbolTable, null));
                adEnv.popCurDiffSorts();
            }
        } else {
            result = diffDeclNames(declTree, diffSymbolTable, toDiffdeclsForFwd);
        }
        return result;
    }

    /**
     * Differentiates a list of expressions that occur in a declaration.
     * If these are variables with dimensions, differentiates using diffVarRef
     * If these are procedure names, returns declarations of all diff versions of each procedure.
     * Takes care of the case where the interface or function is not differentiated yet.
     * @param toDiffdeclsForFwd when given non-null, a container to hold potential
     *  differentiated expressions that must go to the scope of the Fwd sweep instead of Bwd sweep.
     * @return the TapList of the differentiated expressions.
     */
    private TapList<Tree> diffDeclNames(Tree declTree, SymbolTable diffSymbolTable, ToObject<TapList<Tree>> toDiffdeclsForFwd) {
        boolean inAccessDecl = false;
        String accessDeclModifier = null;
        if (declTree.opCode() == ILLang.op_accessDecl) {
            if (ILUtils.isAccessDeclValue(declTree, "in")
                    || ILUtils.isAccessDeclValue(declTree, "out")
                    || ILUtils.isAccessDeclValue(declTree, "inout")
                    || ILUtils.isAccessDeclValue(declTree, "optional")) {
                inAccessDecl = true;
                accessDeclModifier = declTree.down(1).stringValue();
            }
            declTree = declTree.down(2);
        }
        Tree[] declNames = declTree.children();
        String baseName;
        SymbolDecl symbolDecl;
        Tree diffTree;
        TapList<Tree> result = null;
        Tree contextTree = declTree;

        for (int i = declNames.length - 1; i >= 0; i--) {
            baseName = ILUtils.baseName(declNames[i]);
            if (baseName != null) {
                symbolDecl = diffSymbolTable.getSymbolDecl(baseName);
                if (symbolDecl != null) {
                    if (symbolDecl.isA(SymbolTableConstants.VARIABLE) && symbolDecl.isActiveSymbolDecl()
                            && !(inAccessDecl && varRefDifferentiator().paramHasOnlyLocalDiff(adEnv.curActivity(), declNames[i], (VariableDecl) symbolDecl))) {
                        if (declTree.opCode() != ILLang.op_save) {
                            contextTree = declNames[i];
                        }
                        diffTree = varRefDifferentiator().diffVarRef(adEnv.curActivity(), declNames[i], diffSymbolTable,
                                false, contextTree, false, true, null);
                        if (inAccessDecl) {
                            NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(diffTree);
                            if (sHolder != null) {
                                // If this symbol is an unfinished NewSymbolHolder for a variable,
                                // use its original declaration:
                                VariableDecl diffVarDecl = sHolder.newVariableDecl();
                                if (diffVarDecl != null && !diffVarDecl.hasModifier(accessDeclModifier)) {
                                    diffTree = null;
                                }
                            }
                        }
                        if (diffTree != null) {
                            result = new TapList<>(diffTree, result);
                        }
                    }
                    if (symbolDecl.isA(SymbolTableConstants.INTERFACE)
                            && ((InterfaceDecl) symbolDecl).mustHaveADiff(activityAnalyzer())) {
//                     //Iterate on all patterns called in the current context:
//                     TapList<ActivityPattern> patternsCalledHere =
//                         null ; //[llh] toujours rien compris aux interfaces :-( patternsCalledByPattern(??interfaceUnit??, curActivity()) ;
//                     ActivityPattern calledPattern ;
//                     while (patternsCalledHere!=null) {
//                         calledPattern = patternsCalledHere.head ;
//                         diffTree = varRefDifferentiator().findDiffFuncName(calledPattern, symbolDecl, curDiffUnitSort(), diffSymbolTable,
//                                                                          declTree, false, true) ;
//                         result = new TapList<Tree>(diffTree, result) ;
//                         patternsCalledHere = patternsCalledHere.tail ;
//                     }
                        Tree interfaceNameTree = declNames[i];
                        if (ILUtils.getIdentString(interfaceNameTree) == null) {
                            // This may happen for interfaces of predefined operators e.g. mul, assign, etc
                            // Build a dummy interface name with the internal name of the operator.
                            interfaceNameTree = ILUtils.build(ILLang.op_ident, interfaceNameTree.opName());
                        }
                        Tree diffInterfaceNameTree =
                                varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), interfaceNameTree,
                                        diffSymbolTable.basisSymbolTable(), false, true, false, null, false,
                                        curDiffUnitSort(), curDiffUnitSort(), DiffConstants.PROC, null);
                        result = new TapList<>(diffInterfaceNameTree, result);
                    }
                    if (symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                        FunctionDecl functionDecl = (FunctionDecl)symbolDecl ;
                        Unit funcUnit = functionDecl.unit();
                        if (funcUnit.origUnit != null) {
                            funcUnit = funcUnit.origUnit;
                        }
                        // Check functions passed as arguments, e.g. FOO, GEE, VF, VF1 in set04:lh187
                        if (!MPIcallInfo.isMessagePassingFunction(funcUnit.name(), funcUnit.language())) {
                            // Message-Passing procedures have special derivatives...
                            if (symbolDecl.isActiveSymbolDecl()
                                    // the following because some intrinsics are overloaded and Tapenade may
                                    // have found an inactive "symbolDecl" whereas another one is active ! :
                                    || TapList.containsEquals(adEnv.curUnit().activeCalledNames, baseName)
                                    || adEnv.curUnit() == null && funcUnit.isFortran()
                                    || funcUnit.language() != adEnv.curUnit().language()) {
                                boolean intrinsicHasInlinedDerivatives = funcUnit.diffInfo != null;
                                if (!intrinsicHasInlinedDerivatives && functionDecl.isIntrinsic()
                                        && declTree.opCode() == ILLang.op_intrinsic) {
                                    Unit origIntrinsicFunctionUnit = adEnv.srcCallGraph().getAnyIntrinsicUnit(symbolDecl.symbol);
                                    intrinsicHasInlinedDerivatives = origIntrinsicFunctionUnit.diffInfo != null;
                                }
                                //Iterate on all patterns called in the current context:
                                TapList<ActivityPattern> patternsCalledHere =
                                        patternsCalledByPattern(funcUnit, adEnv.curActivity());
                                ActivityPattern calledPattern;
                                while (patternsCalledHere != null) {
                                    calledPattern = patternsCalledHere.head;
                                    if (calledPattern.isActive()) {
                                        if (curDiffUnitSort() == DiffConstants.ADJOINT
                                            && funcUnit.mustDifferentiateSplit()
                                            && !intrinsicHasInlinedDerivatives) {
                                            diffTree = varRefDifferentiator().findDiffFuncName(
                                                    calledPattern, functionDecl, DiffConstants.ADJOINT_BWD,
                                                    diffSymbolTable, declTree, true, false);
                                            result = new TapList<>(diffTree, result);
                                            diffTree = varRefDifferentiator().findDiffFuncName(
                                                    calledPattern, functionDecl, DiffConstants.ADJOINT_FWD,
                                                    diffSymbolTable, declTree, true, false);
                                            if (toDiffdeclsForFwd!=null) {
                                                toDiffdeclsForFwd.setObj(
                                                        new TapList<Tree>(diffTree, toDiffdeclsForFwd.obj()));
                                            } else {
                                                result = new TapList<>(diffTree, result);
                                            }
                                        }
                                        if ((curDiffUnitSort() != DiffConstants.ADJOINT
                                             || funcUnit.mustDifferentiateJoint()
                                             // [llh 22/05/2013] The following traps the case of a function passed as
                                             // an argument and a diff function must be passed. Then we pass function_B.
                                             // This is imperfect and may cause bugs until a cleaner mixture
                                             // is found between SPLIT mode and functions passed as arguments !
                                             // Check on set04:lh187 && ??
                                             || !funcUnit.mustDifferentiateSplit())
                                            && !intrinsicHasInlinedDerivatives) {
                                            diffTree = varRefDifferentiator().findDiffFuncName(
                                                    calledPattern, functionDecl, curDiffUnitSort(),
                                                    diffSymbolTable, declTree, true, false);
                                            result = new TapList<>(diffTree, result);
                                        }
                                    }
                                    patternsCalledHere = patternsCalledHere.tail;
                                }
                            }
                        }
                    }
                } else {
                    MemMap commonActivityMap = adEnv.commonActivityMemoryMap.getSetMemMap(baseName) ;
                    if (commonActivityMap!=null && commonActivityMap.boundaries.tail!=null
                        && !commonActivityMap.boundaries.tail.head.infiniteOffset) {
                        int startOffset = commonActivityMap.boundaries.tail.head.offset ;
                        if (commonActivityMap.isActiveRegion(startOffset,-1,true)) {
                            result = new TapList<>(ILUtils.build(ILLang.op_ident,
                                                     differentiateCommonName(baseName, curDiffUnitSort())),
                                                   result) ;
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Goes through the instruction Trees of Block to replace all Instructions
     * that contain an op_blockStatement by separated Instructions. The reason is
     * that these op_blockStatement are useless, dirty, and harmful for debug AD mode.
     */
    private void flattenBlockStatements(Block block) {
        TapList<Instruction> hdInstructions = new TapList<>(null, block.instructions);
        TapList<Instruction> inInstructions = hdInstructions;
        Instruction instruction, subInstr;
        Tree[] subTrees;
        Tree subTree;
        while (inInstructions.tail != null) {
            instruction = inInstructions.tail.head;
            if (instruction == null || instruction.tree == null) {
                // empty case: remove this useless empty Instruction:
                inInstructions.tail = inInstructions.tail.tail;
            } else if ((instruction.whereMask() == null || instruction.whereMask().isEmpty()) &&
                    instruction.tree.opCode() == ILLang.op_blockStatement) {
                // blockStatement case: remove it, and insert several new Instructions:
                subTrees = instruction.tree.children();
                inInstructions.tail = inInstructions.tail.tail;
                for (int i = subTrees.length; i > 0; --i) {
                    subTree = instruction.tree.cutChild(i);
                    subInstr = new Instruction(subTree, null, block);
                    inInstructions.tail = new TapList<>(subInstr, inInstructions.tail);
                }
            } else {
                // general case: do nothing, keep instruction here.
                inInstructions = inInstructions.tail;
            }
        }
        block.instructions = hdInstructions.tail;
    }

    /**
     * Builds the (ordered) list of instructions that reinitializes
     * to 0.0 the derivatives of all active variables present in "tree",
     * which are going to be overwritten by an I-O operation.
     * <p>
     * Each element of the returned list is in fact an TapTriplet of:<br>
     * -- (Tree) the actual instruction that does this initialization.<br>
     * -- (TapPair) the expressions which are going to be Read-Written.<br>
     * -- (TapPair) the expressions whose derivatives are going to be Read-Written.
     */
    private TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> reinitializeDiffIOVars(
            Tree tree,
            SymbolTable diffSymbolTable,
            TapList<IterDescriptor> iterators, BoolVector beforeActiv,
            BoolVector afterActiv, BoolVector beforeUseful, BoolVector afterUseful) {
        TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> hdInitTrees =
                new TapList<>(null, null);
        reinitializeDiffIOVars(tree, hdInitTrees, diffSymbolTable,
                iterators, beforeActiv, afterActiv, beforeUseful, afterUseful);
        return hdInitTrees.tail;
    }

    /**
     * Private utility function for reinitializeDiffIOVars(). Expects the tail
     * of the list of reinitialization instructions being built.
     * New initialization instructions are added at the tail of this list.
     *
     * @return the new tail of this list, after new initializations are added.
     */
    private TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>>
        reinitializeDiffIOVars(Tree tree,
                           TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> tlInitTrees,
                           SymbolTable diffSymbolTable,
                           TapList<IterDescriptor> iterators, BoolVector beforeActiv,
                           BoolVector afterActiv, BoolVector beforeUseful, BoolVector afterUseful) {
        switch (tree.opCode()) {
            case ILLang.op_expressions: {
                Tree[] expressions = tree.children();
                for (Tree expression : expressions) {
                    tlInitTrees = reinitializeDiffIOVars(expression, tlInitTrees, diffSymbolTable,
                            iterators, beforeActiv, afterActiv, beforeUseful, afterUseful);
                }
                break;
            }
            case ILLang.op_iterativeVariableRef: {
                Tree doTree = tree.down(2);
                IterDescriptor iterator = new IterDescriptor(doTree);
                TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> hdSubInitTrees =
                        new TapList<>(null, null);
                reinitializeDiffIOVars(tree.down(1), hdSubInitTrees, diffSymbolTable,
                        new TapList<>(iterator, iterators), beforeActiv, afterActiv, beforeUseful, afterUseful);
                hdSubInitTrees = hdSubInitTrees.tail;
                TapList<TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>>> inSubInitTrees =
                        hdSubInitTrees;
                TapTriplet<Tree, TapPair<TapList<Tree>, TapList<Tree>>, TapPair<TapList<Tree>, TapList<Tree>>> initTriplet;
                TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
                TapPair<TapList<Tree>, TapList<Tree>> refsRW =
                        new TapPair<>(ILUtils.usedVarsInExp(doTree, null, false),
                                new TapList<>(doTree.down(1), null));
                TapList<Tree> hdSubInitTreesResult = new TapList<>(null, null);
                TapList<Tree> inSubInitTreesResult = hdSubInitTreesResult;
                while (inSubInitTrees != null) {
                    initTriplet = inSubInitTrees.head;
                    accumulateRW(refsRW, initTriplet.second);
                    accumulateRW(diffRefsRW, initTriplet.third);
                    inSubInitTreesResult.head = initTriplet.first;
                    inSubInitTreesResult = inSubInitTreesResult.tail;
                    inSubInitTrees = inSubInitTrees.tail;
                }
                if (hdSubInitTrees != null) {
                    tlInitTrees = tlInitTrees.placdl(
                            new TapTriplet<>(
                                    ILUtils.build(ILLang.op_loop,
                                            ILUtils.build(ILLang.op_none),
                                            ILUtils.build(ILLang.op_none),
                                            iterator.buildDoHeaderTree(diffSymbolTable, curDiffUnit(), true),
                                            ILUtils.build(ILLang.op_blockStatement, hdSubInitTreesResult)),
                                    refsRW, diffRefsRW));
                }
                break;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_ident: {
                TapIntList writtenZones =
                        ZoneInfo.listAllZones(adEnv.curSymbolTable().treeOfZonesOfValue(tree, null, adEnv.curInstruction(), null), true);
                boolean writtenTmp = TapIntList.intersects(writtenZones, adEnv.toActiveTmpZones.tail);
                TapIntList writtenDiffKindVectorIndices =
                        DataFlowAnalyzer.mapZoneRkToKindZoneRk(writtenZones, adEnv.diffKind, adEnv.curSymbolTable());
                TapList<ZoneInfo> zones = DataFlowAnalyzer.mapZoneRkToZoneInfo(writtenZones, adEnv.curSymbolTable());
                Tree[] diffInitTreeR ;
                Tree diffInitTree;
                if (beforeActiv != null && beforeActiv.intersects(writtenDiffKindVectorIndices)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(AD05) Active variable " + ILUtils.toString(tree, adEnv.curUnit().language(), adEnv.curActivity()) + " overwritten by I-O");
                }

// String varnamefordebug = ILUtils.baseName(tree);
// boolean printTraces = (varnamefordebug!=null && varnamefordebug.equals("varname_to_trace")) ;
// if (printTraces) {
// System.out.print(" BASIC EXPR IN IO NEEDS REINIT? "+tree+" annotatedActive:"+ADActivityAnalyzer.isAnnotatedActive(curActivity(), tree, curSymbolTable())+" writtenZones:"+writtenZones+" and diffkind indices:"+writtenDiffKindVectorIndices+" writtenTmp:"+writtenTmp+"  zones:"+zones+" haveDifferentiatedVariable:"+ADActivityAnalyzer.haveDifferentiatedVariable(zones)) ;
// System.out.print("     afterUseful:"+afterUseful) ;
// }

                //[llh] TODO: extract the following test to make it identical in its 2 locations "test ioarg diffinit"
                if ((!TapEnv.spareDiffReinitializations() || TapEnv.debugAdMode() != TapEnv.NO_DEBUG || ILUtils.isAccessedThroughPointer(tree))
                        && ADActivityAnalyzer.haveDifferentiatedVariable(zones)
                        && (writtenTmp || writtenDiffKindVectorIndices != null)
                        || afterUseful != null
                        && ADActivityAnalyzer.haveDifferentiatedVariable(zones)
                        && (writtenTmp || afterUseful.intersects(writtenDiffKindVectorIndices))) {
// if (printTraces) System.out.println(" YES!") ;
                    diffInitTreeR =
                        makeDiffInitialization(tree, null, adEnv.curSymbolTable(),
                                    diffSymbolTable, adEnv.curFwdSymbolTable,
                                    false, null, true);
                    TapList<Tree> refsR = ILUtils.usedVarsInExp(tree, null, false);
                    for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                        diffInitTree = diffInitTreeR[iReplic] ;
                        if (diffInitTree != null) {
                            tlInitTrees = tlInitTrees.placdl(
                                new TapTriplet<>(diffInitTree,
                                        new TapPair<>(refsR, null),
                                        new TapPair<>(null, new TapList<>(tree, null))));
                        }
                    }
                } else {
// if (printTraces) System.out.println(" NO!") ;
                }
                break;
            }
            case ILLang.op_none:
                break;
            default:
                TapEnv.toolWarning(-1, "(Reinitializing diffs of variables overwritten by I-O) Unexpected operator: " + tree.opName());
                break;
        }
        return tlInitTrees;
    }

    /**
     * Look for arrays whose activity is switching from passive in "activHereZero" to active
     * in "activHereOne". In that case, when we are in the default "spareDiffReinitializations"
     * mode, this is the last chance to explicitly set the derivative array to zero before it is
     * (partly) set by the next derivative instruction.
     *
     * @param toNecessaryReInits container for the resulting required initialization instructions.
     * @param onlyOnLocals (not well justified) flag that restricts initialization to
     *  local arrays and possibly to the function result when it is an array.
     *  Currently, it is used to reduce the number of redundant diff arrays initialization
     *  in tangent mode, that are  requested by buildDiffPreInitsForTangent(). 
     */
    private void checkArrayActivitySwitches(
            BoolVector activHereOne, BoolVector activHereZero,
            TapIntList zonesAccessedTotally, TapList<TapPair<TapList<Tree>, Tree>> toNecessaryReInits,
            SymbolTable srcSymbolTable, SymbolTable diffSymbolTable,
            boolean onlyOnLocals, TapList<ZoneInfo> toInitialized) {
        if (activHereOne == null || activHereZero == null) {
            return;
        }
        int nDdZ = srcSymbolTable.declaredZonesNb(adEnv.diffKind);
        ZoneInfo zoneInfo;
        Tree exprToInit;
        TapList<ZoneInfo> zonesToInit = null;
        int localLimitZ = 0 ;
        SymbolTable paramsSymbolTable = adEnv.curUnit().publicSymbolTable() ;
        if (curDiffUnitSort()==DiffConstants.TANGENT && paramsSymbolTable!=null) {
            localLimitZ = paramsSymbolTable.declaredZonesNb(adEnv.diffKind) ;
        }
        TapList<Tree> selectedTrees = null ;
        for (int i = nDdZ-1 ; i>=0 ; --i) {
          if (!TapIntList.contains(zonesAccessedTotally,
                                   DataFlowAnalyzer.kindZoneRkToZoneRk(i, adEnv.diffKind, srcSymbolTable))
              && !activHereZero.get(i) && activHereOne.get(i)) {
              zoneInfo = srcSymbolTable.declaredZoneInfo(i, adEnv.diffKind);
              // Note: "onlyOnLocals" is true only when called from ProcedureCallDifferentiator.buildCallTangentNodes(),
              //  which restricts diff array initialization only for local arrays and possibly function result,
              //  thus avoiding multiple redundant initializing on nested calls.
              //  At other call sites, "onlyOnLocals" is false, so that any diff array can be initialized
              //  in case its activity switches from passive to active.
              if (!onlyOnLocals || i>=localLimitZ || zoneInfo.isResult()) {
                if (!varRefDifferentiator().isOnlyIncrementedDiff(adEnv.curActivity(), zoneInfo)) {
                    exprToInit = replaceAllocateInExprToInitialize(zoneInfo.accessTree) ;
                    //[pbActiv] exprToInit = DataFlowAnalyzer.modifyAccessTreeForUniqueAccess(zoneInfo.zoneNb, exprToInit, block) ;
                    WrapperTypeSpec typeOfExpr = srcSymbolTable.typeOf(exprToInit);
                    // If this exprToInit is an array (i.e. has dimensions), ask for initialization of derivative:
                    // Special case for C: zones accessed by a pointer deref may be arrays in general.
                    if (typeOfExpr != null
                        && (typeOfExpr.getAllDimensions() != null
                            || (adEnv.curUnit().isC() && ILUtils.isAccessedThroughPointer(exprToInit)))
                        && !TapList.contains(zonesToInit, zoneInfo)) {
                        TapIntList accessibleZones =
                                ZoneInfo.listAllZones(
                                        srcSymbolTable.treeOfZonesOfValue(exprToInit/*noalloc*/, null, adEnv.curInstruction(), null),
                                        false);
                        if (accessibleZones!=null && !TapList.containsTree(selectedTrees, exprToInit)) {
                            zonesToInit = new TapList<>(zoneInfo, zonesToInit);
                            // Patch to avoid initializing the same Tree twice (doesn't work on 04/lh142):
                            selectedTrees = new TapList<>(exprToInit, selectedTrees) ;
                        }
                    }
                }
              }
          }
        }
        while (zonesToInit != null) {
            zoneInfo = zonesToInit.head;
            Tree[] diffInitTreeR = makeDiffInitialization(zoneInfo.accessTree, zoneInfo, srcSymbolTable,
                    diffSymbolTable, null, true, null, true);
            Tree diffWrittenTree = ILUtils.copy(zoneInfo.accessTree);
            diffWrittenTree = replaceAllocateInExprToInitialize(diffWrittenTree);
            Tree diffInitTree ;
            for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                diffInitTree = diffInitTreeR[iReplic] ;
                if (diffInitTree != null) {
                    toNecessaryReInits.placdl(
                        new TapPair<>(new TapList<>(diffWrittenTree, null), diffInitTree));
                }
            }
            if (toInitialized!=null) toInitialized.placdl(zoneInfo) ;
            zonesToInit = zonesToInit.tail;
        }
//         if (toNecessaryReInits.tail!=null) {
//             TapEnv.printlnOnTrace("    Inserted diff reinitialization due to ARRAY activity switches in Block: "+block);
//             TapEnv.printlnOnTrace("     when active in "+activHereOne) ;
//             TapEnv.printlnOnTrace("     and passive in "+activHereZero) ;
//             TapEnv.printlnOnTrace("       ==>"+toNecessaryReInits.tail) ;
//         }
    }

    /**
     * Builds a (block of) trees that reset to zero
     * all memory cells of the derivative of "exprToInitialize".
     * Returns an array of such blocks, one per diff replica (in general 1).
     * @param fromZoneInfo When non-null, indicates that we want to initialize a complete zone.
     * @return an instruction that resets to zero all memory cells of the derivative of "exprToInitialize".
     */
    protected Tree[] makeDiffInitialization(Tree exprToInitialize, ZoneInfo fromZoneInfo,
                                          SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                          SymbolTable otherUsageSymbolTable,
                                          boolean protectAccesses, TapList ignoreStructure, boolean ignorePointed) {
        // When exprToInitialize comes from (contains) an allocate,
        // replace this allocate by the pointer-to-allocated variable.
        exprToInitialize = ILUtils.copy(exprToInitialize);
        exprToInitialize = replaceAllocateInExprToInitialize(exprToInitialize);
        String baseVarName = ILUtils.baseName(exprToInitialize);
        Tree baseVarTree = ILUtils.baseTree(exprToInitialize);
        if (baseVarName == null || baseVarTree == null) {
            TapEnv.toolWarning(-1, "(Build diff initialization) Unexpected expression: " + exprToInitialize);
            return null;
        }
        boolean isCurUnitName = baseVarName.equals(adEnv.curUnit().name());
        WrapperTypeSpec type = defSymbolTable.typeOf(exprToInitialize);
        Tree[] resultR = new Tree[TapEnv.diffReplica()] ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<resultR.length ; ++adEnv.iReplic) {

// unfinished attempt to do like in makeDiffInitializationOneReplica
//             Tree diffRootIdent = varRefDifferentiator().diffVarRef(adEnv.curActivity(), exprToInitialize,
//                                                                usageSymbolTable, false, exprToInitialize,
//                                                                multiDirMode(), multiDirMode(), ILUtils.copy(exprToInitialize)) ;
//             RefDescriptor lhsRefDescriptor =
//                             new RefDescriptor(diffRootIdent, null, defSymbolTable, usageSymbolTable,
//                                               null, null, true, null, curDiffUnit());
//             lhsRefDescriptor.setHintArrayTreeForCallSize(exprToInitialize) ;
//             lhsRefDescriptor.prepareForInitialize(curDiffUnit(), null, true) ;
//             resultR[adEnv.iReplic] = lhsRefDescriptor.makeInitialize() ;

            Tree diffBaseVar =
                varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, usageSymbolTable, true,
                        curDiffUnitSort() == DiffConstants.TANGENT && !multiDirMode() && isCurUnitName,
                        false, null, false,
                        curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
            resultR[adEnv.iReplic] = RefDescriptor.makeInitialize(curDiffUnit(), fromZoneInfo, exprToInitialize, type, type, baseVarTree,
                                             defSymbolTable, usageSymbolTable, otherUsageSymbolTable, diffBaseVar, true,
                                             (multiDirMode() ? varRefDifferentiator().multiDirMaxIterDescriptor : null),
                                             protectAccesses, ignoreStructure, ignorePointed);

        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        return resultR ;
    }

    /**
     * Same as makeDiffInitialization(), but builds only ONE replica of the diff initialization instruction,
     * i.e. the replica for the current adEnv.iReplic. Outside -diffReplica mode, this is equivalent to makeDiffInitialization().
     * This is used only in NewBlockGraphNode.buildInstruction, and it would be nicer that it does NOT call makeDiffInitialization() !
     */
    protected Tree makeDiffInitializationOneReplica(Tree exprToInitialize,
                                          SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                          SymbolTable otherUsageSymbolTable,
                                          boolean protectAccesses, TapList ignoreStructure, boolean ignorePointed) {
        // When exprToInitialize comes from (contains) an allocate,
        // replace this allocate by the pointer-to-allocated variable.
        exprToInitialize = ILUtils.copy(exprToInitialize);
        exprToInitialize = replaceAllocateInExprToInitialize(exprToInitialize);
        String baseVarName = ILUtils.baseName(exprToInitialize);
        Tree baseVarTree = ILUtils.baseTree(exprToInitialize);
        if (baseVarName == null || baseVarTree == null) {
            TapEnv.toolWarning(-1, "(Build diff initialization) Unexpected expression: " + exprToInitialize);
            return null;
        }
        boolean isCurUnitName = baseVarName.equals(adEnv.curUnit().name());
        WrapperTypeSpec type = defSymbolTable.typeOf(exprToInitialize);
        if (defSymbolTable==adEnv.curUnit().publicSymbolTable() && adEnv.curUnit().privateSymbolTable()!=null) {
            // Because vars in COMMON have their SymbolDecl visible only in the privateSymbolTable:
            defSymbolTable = adEnv.curUnit().privateSymbolTable() ;
        }

        Tree diffRootIdent = varRefDifferentiator().diffVarRef(adEnv.curActivity(), exprToInitialize,
                                                               usageSymbolTable, false, exprToInitialize,
                                                               multiDirMode(), multiDirMode(), ILUtils.copy(exprToInitialize)) ;
        RefDescriptor lhsRefDescriptor =
                            new RefDescriptor(diffRootIdent, null, defSymbolTable, usageSymbolTable,
                                              null, null, true, null, curDiffUnit());
        lhsRefDescriptor.setHintArrayTreeForCallSize(exprToInitialize) ;
        lhsRefDescriptor.prepareForInitialize(curDiffUnit(), null, true) ;
        return lhsRefDescriptor.makeInitialize() ;

//         Tree diffBaseVar =
//                 varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, usageSymbolTable, true,
//                         curDiffUnitSort() == DiffConstants.TANGENT && !multiDirMode() && isCurUnitName,
//                         false, null, false,
//                         curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
//         return RefDescriptor.makeInitialize(curDiffUnit(), null, exprToInitialize, type, type, baseVarTree,
//                                             defSymbolTable, usageSymbolTable, otherUsageSymbolTable, diffBaseVar, true,
//                                             (multiDirMode() ? varRefDifferentiator().multiDirMaxIterDescriptor : null),
//                                             protectAccesses, ignoreStructure, ignorePointed);

    }

    //JLB: Add comment at one point
    protected void prepareRestoreOperations(RefDescriptor refDescriptor, RefDescriptor refDescriptorRestore,
                                            int knownMultiplicity, SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable) {
        RefDescriptor saveVarRefDescriptor =
                tryStaticTape(curBlock(), knownMultiplicity, "ad_save",
                        refDescriptor.completeType, fwdSymbolTable, bwdSymbolTable);
        if (saveVarRefDescriptor != null && saveVarRefDescriptor.completeType != null
                && saveVarRefDescriptor.completeType.wrappedType != null) {
            // if taping can be static here, create a save variable and prepare for using it:
            refDescriptor.forceStaticSave(saveVarRefDescriptor);
            if (refDescriptorRestore != null) {
                refDescriptorRestore.forceStaticSave(saveVarRefDescriptor);
            }
            refDescriptor.prepareForAssignOrNormDiff(saveVarRefDescriptor,
                                                     (refDescriptorRestore == null ? curDiffUnit() : curFwdDiffUnit()), null, true);
            if (refDescriptorRestore != null) {
                refDescriptorRestore.prepareForAssignOrNormDiff(saveVarRefDescriptor, curDiffUnit(), null, true);
            }
        } else {
            int ppLabel = TapEnv.getNewPushPopNumber();
            // ... otherwise prepare to create a PUSH/POP pair:
            refDescriptor.prepareForStack(refDescriptorRestore == null ? curDiffUnit() : curFwdDiffUnit(), ppLabel, null, true);
            if (refDescriptorRestore != null) {
                refDescriptorRestore.prepareForStack(curDiffUnit(), ppLabel, null, true);
            }
            // force F90 code to declare PUSH/POP POINTER interfaces :
            if (refDescriptor.holdsAPointer || refDescriptorRestore != null && refDescriptorRestore.holdsAPointer) {
                adEnv.curDiffUnitPushesPointers = true;
            }
        }
    }

    /** Memo of already created RefDescriptor's for the static save
     * variables (e.g. "ad_save" or "ad_branch") in the surrounding
     * FIXEDPOINT FlowGraphLevel's. This is used to reuse the same
     * static save variables in both backward sweeps.
     * This is a stack, for each nested fixed-point loop
     * around the current point (deepest first) */
    private TapList<TapTriplet<Boolean, TapList<RefDescriptor>, TapList<RefDescriptor>>>
        curStaticSaves = null ;

    protected void pushStaticSaves(TapTriplet<Boolean, TapList<RefDescriptor>, TapList<RefDescriptor>> staticSaves) {
        curStaticSaves = new TapList<>(staticSaves, curStaticSaves) ;
    }

    protected void popStaticSaves() {
        curStaticSaves = curStaticSaves.tail ;
    }

    /**
     * If at the current place "block" we can use
     * static taping instead of a dynamic stack
     * to store a tape value of type "staticVarRootType".
     *
     * @param knownMultiplicity a conventional integer meaningful only when
     *                          block is the header of a loop L, and we are considering static taping:
     *                          1: even if L has dynamic length, this saving is done only once,
     *                          therefore we can accept static taping for L
     *                          2: even if L is basically checkpointed (e.g. BINOMIAL_LOOP), this saving is
     *                          done arbitrarily many times, therefore we must always reject static taping.
     *                          0: default, neutral value: when L has dynamic length, L will reject static taping as expected.
     * @param diffSymbolTable   the SymbolTable of the actual saving/restoring instruction.
     * @return a RefDescriptor for the local variable
     * named "staticVarName" that will contain the stored value or null.
     */
    protected RefDescriptor tryStaticTape(Block block, int knownMultiplicity, String staticVarName,
                                          WrapperTypeSpec staticVarRootType,
                                          SymbolTable fwdDiffSymbolTable, SymbolTable diffSymbolTable) {
        // reject static taping if no "-optim staticTape":
        if (block == null || knownMultiplicity == 2
            || !(TapEnv.get().staticTape
                 // force static taping on GPU code:
                 || (TapEnv.multithreadAnalyzer()!=null
                     && TapEnv.multithreadAnalyzer().isGPU(adEnv.curUnit())))) {
            return null;
        }
        // reject if variable type is weird:
        if (TypeSpec.isA(staticVarRootType, SymbolTableConstants.VOIDTYPE) || TypeSpec.isA(staticVarRootType, SymbolTableConstants.POINTERTYPE)) {
            return null;
        }
        // reject if variable is of size unknown statically:
        if (TypeSpec.isA(staticVarRootType, SymbolTableConstants.ARRAYTYPE)) {
            ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) staticVarRootType.wrappedType;
            ArrayDim[] checkedDimensions =
                    arrayTypeSpec.checkNoneDimensions(arrayTypeSpec.dimensions(),
                            !diffSymbolTable.isFormalParamsLevel(),
                            false, null, "",
			    null, null, null, diffSymbolTable, null);
            // TODO trouver les dimensions statiques si possible
            if (checkedDimensions != null) {
                return null;
            }
        }
        // Detect the iteration space between PUSH and POP:
        TapList<LoopBlock> iterationsBetween =
                flowGraphDifferentiator().getIterationsBetweenPushAndPop(block);
        if (iterationsBetween != null && iterationsBetween.head == null) {
            // No staticTape allowed if FWD and BWD units are SPLIT:
            if (curFwdDiffUnit() != curDiffUnit()) {
                return null;
            }
            iterationsBetween = iterationsBetween.tail;
        }
        // Check all surrounding iterations are of a static known length.
        // Discard loop levels that are fixed-point (because push'es are done only on the last iteration).
        // Also discard the deepest level when knownMultiplicity==1 :
        boolean hasStaticIterationSpace = true;
        TapList<LoopBlock> toIterationsBetween = new TapList<>(null, iterationsBetween);
        TapList<LoopBlock> inIterationsBetween = toIterationsBetween ;
        while (inIterationsBetween.tail!=null) {
            if (inIterationsBetween.tail.head.isFixedPoint) {
                inIterationsBetween.tail = inIterationsBetween.tail.tail ;
            } else if (knownMultiplicity==1 && inIterationsBetween.tail.tail==null) {
                inIterationsBetween.tail = inIterationsBetween.tail.tail ;
            } else {
                if (inIterationsBetween.tail.head.staticIterationLength()==null)
                    hasStaticIterationSpace = false ;
                inIterationsBetween = inIterationsBetween.tail ;
            }
        }
        iterationsBetween = toIterationsBetween.tail ;
        if (!hasStaticIterationSpace) {
            return null;
        }
        // Now we know we can apply static taping.
        RefDescriptor saveVarRefDescriptor = null ;
        if (curStaticSaves==null || curStaticSaves.head.first==Boolean.FALSE) {
            // "normal" case where we must create a new static save variable:
            NewSymbolHolder saveVarHolder = new NewSymbolHolder(staticVarName);
            WrapperTypeSpec saveVarType =
                addStaticIterationDimensions(staticVarRootType, iterationsBetween);
            saveVarHolder.setAsVariable(saveVarType, null);
            saveVarHolder.declarationLevelMustInclude(diffSymbolTable);
            Tree saveVarRef = saveVarHolder.makeNewRef(diffSymbolTable);
            saveVarRef = addStaticIterationIndices(saveVarRef, iterationsBetween, saveVarType);
            saveVarRefDescriptor =
                new RefDescriptor(saveVarRef, null, null, staticVarRootType, null,
                        diffSymbolTable, curDiffUnit().privateSymbolTable(), fwdDiffSymbolTable,
                        null, false, null, curDiffUnit());
            if (diffSymbolTable != fwdDiffSymbolTable) {
                saveVarHolder.setOtherDefSymbolTable(fwdDiffSymbolTable);
            }
            if (curStaticSaves!=null) {
                // if recording static save variables, record this one:
                curStaticSaves.head.third =
                    curStaticSaves.head.third.placdl(saveVarRefDescriptor) ;
            }
        } else {
            // cases where we must reuse a previously built static save variable
            // which is stored in curStaticSaves.head
            curStaticSaves.head.third = curStaticSaves.head.third.tail ;
            saveVarRefDescriptor = curStaticSaves.head.third.head ;
        }
        return saveVarRefDescriptor;
    }

    /**
     * Adds an ArrayTypeSpec around rootTypeSpec, corresponding to
     * the static iteration space surrounding block.
     */
    private WrapperTypeSpec addStaticIterationDimensions(
            WrapperTypeSpec rootTypeSpec, TapList<LoopBlock> iterationsBetween) {
        TapList<Integer> iterSpace = null;
        while (iterationsBetween != null) {
            iterSpace = new TapList<>(iterationsBetween.head.staticIterationLength(), iterSpace);
            iterationsBetween = iterationsBetween.tail;
        }
        if (iterSpace == null) {
            return rootTypeSpec;
        } else {
            TapList<ArrayDim> existingDimensions = rootTypeSpec.getAllDimensions();
            if (existingDimensions == null) {// There are no dimensions initially. Just wrap into a new ArrayTypeSpec:
                ArrayDim[] dimensions = new ArrayDim[TapList.length(iterSpace)];
                int i = dimensions.length-1;
                int diml;
                while (iterSpace != null) {
                    diml = iterSpace.head;
                    dimensions[i] =
                            new ArrayDim(null, 1, diml, null, -1, 0, 0);
                    --i;
                    iterSpace = iterSpace.tail;
                }
                return new WrapperTypeSpec(new ArrayTypeSpec(rootTypeSpec, dimensions));
            } else { // There are already existing dimensions: add the new dimensions to the existing:
                WrapperTypeSpec copyRootTypeSpec = (WrapperTypeSpec) rootTypeSpec.copy();
                TypeSpec arrayTypeSpecInside = copyRootTypeSpec;
                while (arrayTypeSpecInside.kind() != SymbolTableConstants.ARRAYTYPE) {
                    arrayTypeSpecInside = arrayTypeSpecInside.wrappedType();
                }
                ArrayTypeSpec existingArray = (ArrayTypeSpec) arrayTypeSpecInside;
                ArrayDim[] newDimensions = new ArrayDim[existingArray.dimensions().length + TapList.length(iterSpace)];
                int i = 0, diml, j = 0;
                while (iterSpace != null) {
                    diml = iterSpace.head;
                    newDimensions[i] =
                            new ArrayDim(null, 1, diml, null, -1, 0, 0);
                    ++i;
                    iterSpace = iterSpace.tail;
                }
                while (i < newDimensions.length) {
                    newDimensions[i] = existingArray.dimensions()[j];
                    ++i;
                    ++j;
                }
                existingArray.setDimensions(newDimensions);
                return copyRootTypeSpec;
            }
        }
    }

    /**
     * Adds an op_arrayAccess around rootTree, corresponding to
     * the indices of the iteration indexes surrounding block.
     */
    private Tree addStaticIterationIndices(Tree rootTree, TapList<LoopBlock> iterationsBetween, WrapperTypeSpec saveVarType) {
        TapList<Tree> indices = null;
        while (iterationsBetween != null) {
            indices = new TapList<>(iterationsBetween.head.staticNormalizedIndex(), indices);
            iterationsBetween = iterationsBetween.tail;
        }
        if (indices == null) {
            return rootTree;
        } else {
            // complete with as many "free" dimensions as necessary:
            TapList<ArrayDim> otherDims = saveVarType.getAllDimensions() ;
            int doneDims = TapList.length(indices) ;
            while (doneDims>0) {
                otherDims = otherDims.tail ;
                --doneDims;
            }
            while (otherDims!=null) {
                indices = new TapList<>(ILUtils.build(ILLang.op_arrayTriplet), indices) ;
                otherDims = otherDims.tail ;
            }
            indices = TapList.reverse(indices) ;
            return ILUtils.build(ILLang.op_arrayAccess, rootTree,
                    ILUtils.build(ILLang.op_expressions, indices));
        }
    }

    private TapList<Tree> collectSwitchExpressions(Tree test, TapList<Tree> expressions) {
        switch (test.opCode()) {
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_eq:
            case ILLang.op_neq:
                return new TapList<>(
                        ILUtils.buildSmartAddSub(ILUtils.copy(test.down(1)), -1,
                                ILUtils.copy(test.down(2))),
                        expressions);
            case ILLang.op_and:
            case ILLang.op_xor:
            case ILLang.op_or:
                expressions = collectSwitchExpressions(test.down(1), expressions);
                return collectSwitchExpressions(test.down(2), expressions);
            case ILLang.op_not:
                return collectSwitchExpressions(test.down(1), expressions);
            default:
                return expressions;
        }
    }

    private Tree genDiffTypeTree(SymbolTable diffSymbolTable,
                                 WrapperTypeSpec typeSpec, WrapperTypeSpec diffTypeSpec,
                                 Tree[] modifiers) {
        Tree typeTree;
        if (diffTypeSpec != null) {
            diffTypeSpec = diffTypeSpec.equalsDiffTypeSpecAndTypeSpec();
        }
        if (diffTypeSpec == null || diffTypeSpec == typeSpec) {
            diffTypeSpec = typeSpec;
        }
        if (diffTypeSpec.wrappedType != null && diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder != null) {
            if (typeSpec.wrappedType.getRenamedTypeDeclName(adEnv.curUnit()) == null) {
                typeTree = diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder.makeNewRef(diffSymbolTable);
            } else {
                TypeDecl typeDecl = diffSymbolTable.getTypeDecl(
                        typeSpec.wrappedType.getRenamedTypeDeclName(adEnv.curUnit()));
                typeTree = getDiffTypeDeclTree(diffSymbolTable, suffixes()[curDiffUnitSort()][DiffConstants.TYPE],
                        diffTypeSpec, typeDecl);
            }
        } else {
            typeTree = diffTypeSpec.generateTree(diffSymbolTable, null, null, true, null);
        }
        if (modifiers != null) {
            TapList<Tree> identModifiers = null;
            Tree dimensionModifier = null;
            for (int i = modifiers.length - 1; i >= 0; --i) {
                Tree tree = modifiers[i];
                if (tree.opCode() == ILLang.op_ident
                        && !tree.stringValue().equals("pointer")) {
                    //&& (curDiffUnitSort()!=ADJOINT || !ILUtils.aboutInOut(tree))) {
                    identModifiers = new TapList<>(ILUtils.copy(tree), identModifiers);
                } else if (tree.opCode() == ILLang.op_dimColons) {
                    dimensionModifier = ILUtils.copy(tree);
                } else if (tree.opCode() == ILLang.op_accessDecl
                        && tree.down(1).opCode() == ILLang.op_ident
                        && tree.down(1).stringValue().equals("bind")) {
                    identModifiers = new TapList<>(diffBindCVarName(tree), identModifiers);
                }
            }
            if (dimensionModifier != null && !multiDirMode()) {
                // (patch) Force diff interface decl to use same dimensions as primal interface decl:
                // Caution: this patch cannot work in multidirectional mode, because dimensions differ!
                putBackOriginalDimensions(typeTree, dimensionModifier);
            }
            if (identModifiers != null) {
                typeTree = ILUtils.build(ILLang.op_modifiedType,
                        ILUtils.build(ILLang.op_modifiers, identModifiers),
                        typeTree);
            }
        }
        return typeTree;
    }

    /**
     * Modifies typeTree in place, replacing the last dimension(s) by the given dimColonsBack.
     * This is a unsatisfactory patch needed due to the choice to build diff declarations from
     * the true type stored in the VariableDecl rather than from the primal declaration tree.
     */
    private void putBackOriginalDimensions(Tree typeTree, Tree dimColonsBack) {
        Tree inTypeTree = typeTree;
        Tree dimColonsFound = null;
        while (inTypeTree != null) {
            switch (inTypeTree.opCode()) {
                case ILLang.op_arrayType:
                    dimColonsFound = inTypeTree.down(2);
                    inTypeTree = inTypeTree.down(1);
                    break;
                case ILLang.op_pointerType:
                case ILLang.op_referenceType:
                    inTypeTree = inTypeTree.down(1);
                    break;
                case ILLang.op_modifiedType:
                    inTypeTree = inTypeTree.down(2);
                    break;
                default:
                    inTypeTree = null;
            }
        }
        if (dimColonsFound != null) {
            for (int i = dimColonsFound.length(); i > 0; --i) {
                dimColonsFound.removeChild(1);
            }
            for (int i = dimColonsBack.length(); i > 0; --i) {
                dimColonsFound.addChild(dimColonsBack.cutChild(i), 1);
            }
        }
    }

    private Tree diffBindCVarName(Tree tree) {
        if (tree.down(2).length() == 1) {
            return ILUtils.copy(tree);
        } else {
            Tree diffBind = ILUtils.copy(tree);
            String bindVarName = tree.down(2).down(2).down(2).stringValue();
            SymbolTable rootCSymbolTable = adEnv.diffCallGraph().cRootSymbolTable();
            if (rootCSymbolTable != null) {
                VariableDecl varDeclC = rootCSymbolTable.getVariableDecl(bindVarName);
                if (varDeclC != null && varDeclC.isActiveSymbolDecl()) {
                    Tree diffDecl = varRefDifferentiator().diffSymbolName(adEnv.curActivity(), bindVarName, rootCSymbolTable,
                            true, false, false,
                            null, null, false,
                            curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, null);
                    diffBind.down(2).down(2).setChild(diffDecl, 2);
                }
            }
            return diffBind;
        }
    }

    private Tree getDiffTypeDeclTree(SymbolTable diffSymbolTable,
                                     String fSuffix, WrapperTypeSpec differentiatedTypeSpec,
                                     TypeDecl typeDecl) {
        Tree diffTypeDeclTree = null;
        NewSymbolHolder nSH = typeDecl.getDiffSymbolHolder(0, null, 0);
        if (nSH == null) {
            typeDecl.activeTypeDecl(diffSymbolTable, differentiatedTypeSpec, fSuffix);
            nSH = typeDecl.getDiffSymbolHolder(0, null, 0);
        }
        if (nSH != null) {
            diffTypeDeclTree = nSH.makeNewRef(diffSymbolTable);
        }
        return diffTypeDeclTree;
    }

    /**
     * @return a list of expression Trees that represent all zones with a "true" in boolVector.
     */
    protected TapList<Tree> collectTrueTrees(BoolVector boolVector, SymbolTable srcSymbolTable) {
        if (boolVector == null) {
            return null;
        }
        TapList<Tree> result = null;
        ZoneInfo zoneInfo;
        Tree zoneInfoVisibleAccessTree;
        for (int z = srcSymbolTable.declaredZonesNb(adEnv.diffKind) - 1; z >= 0; --z) {
            if (boolVector.get(z)) {
                // check that this memory zone is accessible from
                // the current "srcSymbolTable" level:
                zoneInfo = srcSymbolTable.declaredZoneInfo(z, adEnv.diffKind);
                if (zoneInfo != null && !zoneInfo.isHidden && zoneInfo.accessTree!=null && !zoneInfo.comesFromAllocate()) {
                    zoneInfoVisibleAccessTree = srcSymbolTable.buildZoneVisibleAccessTree(zoneInfo);
                    if (zoneInfoVisibleAccessTree != null) {
                        result = ILUtils.addTreeInList(zoneInfoVisibleAccessTree, result);
                    }
                }
            }
        }
        return result;
    }

    /**
     * Detects that at least one of the dependents or independents is single precision,
     * in which case the epsilon for divided differences must be larger.
     */
    private boolean hasActiveSinglePrecision(SymbolTable srcSymbolTable, BoolVector activeAtEntry, BoolVector activeAtExit) {
        boolean foundSinglePrecision = false;
        int lastDeclaredZone = srcSymbolTable.declaredZonesNb(adEnv.diffKind);
        ZoneInfo zoneInfo;
        WrapperTypeSpec zoneType;
        int precision;
        for (int z = lastDeclaredZone - 1; z >= 0 && !foundSinglePrecision; --z) {
            if (activeAtEntry != null && activeAtEntry.get(z) ||
                    activeAtExit != null && activeAtExit.get(z)) {
                zoneInfo = srcSymbolTable.declaredZoneInfo(z, adEnv.diffKind);
                if (zoneInfo != null && !zoneInfo.isHidden && zoneInfo.accessTree!=null
                    && !zoneInfo.comesFromAllocate()) {
                    zoneType = zoneInfo.type;
                    precision = zoneInfo.typeSizeModifier;
                    if (precision == -1) {
                        precision = zoneType.precisionSize();
                    }
                    if (zoneType.isRealBase()) {
                        if (precision == -3 || precision > 0 && precision < 8) {
                            foundSinglePrecision = true;
                        }
                    } else if (zoneType.isComplexBase()) {
                        if (precision == -3 || precision > 0 && precision < 16) {
                            foundSinglePrecision = true;
                        }
                    }
// if (foundSinglePrecision) TapEnv.printlnOnTrace("FOUND ONE SINGLE PRECISION:"+zoneInfo+" t:"+zoneType+" p:"+precision) ;
                }
            }
        }
        return foundSinglePrecision;
    }

    /** Cuts the given Instruction from its flow-graph context, and puts it alone into a new dummy Block.
     * The pointer destination info of the dummy Block is a new "identity" destinations info
     * that is suitable for data-flow info propagation to a "context" call site. */
    private static TapTriplet<Block,TapList<TapPair<TapIntList, BoolVector>>,TapList<TapPair<Integer, TapIntList>>[]> detachForPointers(Instruction instr, Tree callTree) {
        Block origBlock = instr.block ;
        TapList<TapPair<TapIntList, BoolVector>> origDestsChange = instr.pointerDestsChanges ;
        TapList<TapPair<Integer, TapIntList>>[] origAnnotation = callTree.getAnnotation("pointersActualInitialDests") ;
        callTree.removeAnnotation("pointersActualInitialDests") ;
        SymbolTable symbolTable = origBlock.symbolTable ;
        Block dummyBlock = new Block(symbolTable, null, null) ;
        dummyBlock.addInstrHd(instr) ;
        instr.block = dummyBlock ;
        dummyBlock.pointerInfosIn = new BoolMatrix(symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND),
                                                   symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
        dummyBlock.pointerInfosIn.setIdentity();
        instr.pointerDestsChanges = null ;
        return new TapTriplet<>(origBlock, origDestsChange, origAnnotation) ;
    }

    /** Undoes the effect of a previous detachForPointers() */
    private static void reattachForPointers(Instruction instr, Tree callTree, TapTriplet<Block,TapList<TapPair<TapIntList, BoolVector>>,TapList<TapPair<Integer, TapIntList>>[]> memo) {
        instr.block = memo.first ;
        instr.pointerDestsChanges = memo.second ;
        callTree.setAnnotation("pointersActualInitialDests", memo.third) ;
    }
}
