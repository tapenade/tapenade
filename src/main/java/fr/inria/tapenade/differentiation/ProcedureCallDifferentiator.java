/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.ArrayTypeSpec;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.CompositeTypeSpec;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.FieldDecl;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.FunctionTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.IterDescriptor;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.PrimitiveTypeSpec;
import fr.inria.tapenade.representation.RefDescriptor;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.representation.MixedLanguageInfos;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

// CONVENTION: Differentiation model on the arguments (and result) of procedures (and functions)
//----------------------------------------------------------------------------------------------
// A procedure or function will be differentiated as follows.
// Suppose the original call is z = F(x,y). Variables xd,yd,zd (resp. xb,yb,zb) are
// the tangent (resp. adjoint) derivatives of (x,y,z). They need not be all present.
// In CONTEXT mode and ADJOINT_FWD mode, the derivatives are present only when they
// hold pointers to memory location of future derivatives. i.e. the original variable is ReqX.
// In other modes, the derivatives are (in addition) present when they hold derivative values,
// i.e. the original variable is Active or ReqX.
// Two cases occur:
// a) "forward-style" is for TANGENT, ADJOINT_FWD, and for all CONTEXT (TANGENT or ADJOINT):
//     When all derivatives are present, our model is to generate:
//        zd = F_D(x,xd,y,yd,z) or zb = F_FWD(x,xb,y,yb,z) or zb = F_B(x,xb,y,yb,z)
//     When some derivatives are absent (e.g. here x and z), model is to generate:
//        z = F_D(x,y,yd)       or z = F_FWD(x,y,yb)       or z = F_B(x,y,yb)
// b) "backward-style" is for ADJOINT_BWD, and for non-CONTEXT ADJOINT:
//     When all derivatives are present, our model is to generate:
//        F_B(x,xb,y,yb,zb)     or F_BWD(x,xb,y,yb,zb)
//     When some derivatives are absent (e.g. here x and z), model is to generate:
//        F_B(x,y,yb)           or F_BWD(x,y,yb)


/**
 * Object that differentiates procedure calls.
 * In cooperation with a BlockDifferentiator, for each given procedure call,
 * tells this BlockDifferentiator which instructions must be added
 * to the fwdBlock and (optional) bwdBlock of the current curBlock.
 */
public class ProcedureCallDifferentiator {

    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    /**
     * Access to global adEnv.callGraphDifferentiator.
     */
    private CallGraphDifferentiator callGraphDifferentiator() {
        return adEnv.callGraphDifferentiator;
    }

    /**
     * Access to global adEnv.blockDifferentiator.
     */
    private FlowGraphDifferentiator flowGraphDifferentiator() {
        return adEnv.flowGraphDifferentiator;
    }

    /**
     * Access to global adEnv.BlockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * Access to global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /**
     * Access to global adEnv.multiDirMode.
     */
    private boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * Access to global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * Access to global adEnv.curFwdDiffUnit.
     */
    private Unit curFwdDiffUnit() {
        return adEnv.curFwdDiffUnit;
    }

    /**
     * Access to global adEnv.curDiffUnitSort.
     */
    private int curDiffUnitSort() {
        return adEnv.curDiffUnitSort;
    }

    /**
     * Access to global adEnv.curDiffVarSort.
     */
    private int curDiffVarSort() {
        return adEnv.curDiffVarSort;
    }

    protected ProcedureCallDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
    }

    /**
     * Build the derivative instructions for a source procedure call "callTree",
     * with its result (when procedure is in fact a function) assigned to a source "resultTree".
     * The new derivative instructions are inserted into
     * -- the future block in the future forward sweep (in their future final order)
     * through addFuture*NodeFwd() calls,
     * -- the future block in the future backward sweep (in the inverse of their future final order)
     * through addFuture*NodeBwd() calls,
     * -- the block for derivative declarations for a SPLIT BWD code (in their future final order)
     * through addFutureSetDiffNodeBwd() calls (happens only for declaration instructions).
     *
     * @param differentiationMode    the differentiation mode requested in {TANGENT_MODE, ADJOINT_MODE, ADJOINT_SPLIT_MODE}.
     * @param calledUnit             the Unit of the called procedure
     * @param calledActivity         the activity pattern of the called Unit which is called here.
     * @param mustDiffCall           true when this call is active, i.e. we need to compute derivatives about it
     * @param callIsLive             true when the primal call is diff-live, i.e. cannot be simply removed from the diff code
     * @param mustSaveTBRBeforeInstr true when some TBR values must be PUSH/POPPED before this call
     * @param splitAdjointCall       true when the adjoint of this call must be SPLIT (call foo_fwd() ... call foo_bwd())
     * @param normalCheckpointedCall false if splitAdjointCall or in TANGENT_MODE or inside context code
     * @param fwdSymbolTable         the SymbolTable of the forward sweep where the derivative instructions will go
     * @param bwdSymbolTable         the SymbolTable of the backward sweep where the derivative instructions will go
     */
    protected void buildDiffInstructions(Tree callTree, Tree resultTree, int differentiationMode,
                                         Unit calledUnit, ActivityPattern calledActivity,
                                         boolean mustDiffCall, boolean callIsLive,
                                         boolean mustSaveTBRBeforeInstr, boolean normalCheckpointedCall, boolean splitAdjointCall,
                                         SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
                                         BoolVector beforeActiv, BoolVector afterActiv,
                                         BoolVector beforeUseful, BoolVector afterUseful,
                                         BoolVector beforeReqX, BoolVector afterReqX,
                                         BoolVector beforeAvlX) {
        // [llh 22Mar2019] TODO(?): Fuse (and then erase) buildCallTangentNodes() and buildCallAdjointNodes()
        //  into this buildDiffInstructionsOfProcedureCall(). Not sure... depends on level of similarity.
        if (TapEnv.associationByAddress()) {
            blockDifferentiator().updateAAInstructionMask(adEnv.curInstruction(), adEnv.curSymbolTable(), adEnv.curFwdSymbolTable);
        }

        TapList<Tree> beforeActivTrees = null, afterActivTrees = null;
        TapList<Tree> args;
        Tree dbadTree, debugCallArgs = null;
        boolean callContextToContext = adEnv.curUnitIsContext && !adEnv.activeRootCalledFromContext;
        // If in -debugTGT or -debugADJ mode, instrument before a call to a standard differentiated procedure:
        if (mustDiffCall && !callContextToContext && TapEnv.debugAdMode() != TapEnv.NO_DEBUG && calledUnit.isStandard()) {
            Directive tracedCallInstructionDir = adEnv.curInstruction().hasDirective(Directive.DEBUGCALL);
            args = null;
            if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG) {
                Tree forcedOkHere = null;
                if (tracedCallInstructionDir != null && tracedCallInstructionDir.arguments.length > 1) {
                    forcedOkHere = ILUtils.cleanBoolCopy(tracedCallInstructionDir.arguments[1], curDiffUnit());
                }
                if (forcedOkHere == null) {
                    forcedOkHere = ILUtils.build(ILLang.op_intCst, 0);
                }
                args = new TapList<>(forcedOkHere, args);
            }
            if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG || curDiffUnitSort() == DiffConstants.ADJOINT) {
                Tree okHere = null;
                if (tracedCallInstructionDir != null && tracedCallInstructionDir.arguments.length > 0) {
                    okHere = ILUtils.cleanBoolCopy(tracedCallInstructionDir.arguments[0], curDiffUnit());
                }
                if (okHere == null) {
                    okHere = ILUtils.build(ILLang.op_intCst, 0);
                }
                args = new TapList<>(okHere, args);
            }
            String calledName = ILUtils.getCalledNameString(callTree);
            if (calledName.charAt(0) == '#') {
                calledName = calledName.substring(1, calledName.length() - 1);
            }
            Tree funcName = ILUtils.build(ILLang.op_stringCst,
                    curDiffUnit().isFortran() ? calledName.toUpperCase() : calledName);
            if (curDiffUnit().isFortran()) {
                funcName = ILUtils.concatWithNull(funcName);
            }
            args = new TapList<>(funcName, args);
            debugCallArgs = ILUtils.build(ILLang.op_expressions, args);
            beforeActivTrees = blockDifferentiator().collectTrueTrees(beforeActiv, adEnv.curSymbolTable());
            afterActivTrees = blockDifferentiator().collectTrueTrees(afterActiv, adEnv.curSymbolTable());

            String utilityFunctionSuffix = TapEnv.debugAdMode() == TapEnv.TGT_DEBUG ? "Tgt_call" : differentiationMode == DiffConstants.TANGENT_MODE ? "Fwd_call" : "Bwd_exit";
            dbadTree = ILUtils.buildCall(
                    ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), "adDebug" + utilityFunctionSuffix),
                    TapEnv.debugAdMode() == TapEnv.TGT_DEBUG || differentiationMode == DiffConstants.TANGENT_MODE ? debugCallArgs : ILUtils.build(ILLang.op_expressions));
            if (differentiationMode == DiffConstants.TANGENT_MODE) {
                blockDifferentiator().addFuturePlainNodeFwd(dbadTree, null, false,
                        null, null, null, null, false, true,
                        "Debug-instrument before calling tangent-diff procedure", false);
            } else {
                blockDifferentiator().addFuturePlainNodeBwd(dbadTree, null, false,
                        null, null, null, null, false, true,
                        "Debug-instrument after calling adjoint-diff procedure", false);
            }
            if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
                dbadTree = flowGraphDifferentiator().placeDebugADTests(curDiffUnit(), callTree,
                        beforeActiv, -2, adEnv.curSymbolTable(),
                        adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind), "beforeCall", null,
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE"),
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE"));
                if (differentiationMode == DiffConstants.TANGENT_MODE) {
                    blockDifferentiator().addFuturePlainNodeFwd(dbadTree, null, false,
                            null, beforeActivTrees, null, beforeActivTrees, false, true,
                            "Debug-instrument actives before calling tangent-diff procedure", false);
                } else {
                    blockDifferentiator().addFuturePlainNodeBwd(dbadTree, null, false,
                            null, beforeActivTrees, null, beforeActivTrees, false, true,
                            "Debug-instrument actives after calling adjoint-diff procedure", false);
                }
            }
        }

        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            if (callContextToContext) {
                buildContextCallTangentNodes(callTree, resultTree, calledUnit, calledActivity,
                        mustDiffCall, fwdSymbolTable);
            } else {
                buildCallTangentNodes(callTree, resultTree, calledUnit, calledActivity,
                        mustDiffCall, fwdSymbolTable,
                        beforeActiv, afterActiv, afterUseful,
                        beforeReqX, afterReqX);
            }

        } else if (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            buildCallAdjointNodes(callTree, resultTree, calledUnit, calledActivity,
                    mustDiffCall, callIsLive,
                    mustSaveTBRBeforeInstr, normalCheckpointedCall, splitAdjointCall,
                    differentiationMode == DiffConstants.ADJOINT_MODE,
                    fwdSymbolTable, bwdSymbolTable,
                    beforeActiv, afterActiv);
        }

        // If in -debugTGT or -debugADJ mode, instrument after a call to a standard differentiated procedure:
        if (mustDiffCall && !callContextToContext && TapEnv.debugAdMode() != TapEnv.NO_DEBUG && calledUnit.isStandard()) {
            if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
                dbadTree = flowGraphDifferentiator().placeDebugADTests(curDiffUnit(), callTree,
                        afterActiv, 2, adEnv.curSymbolTable(),
                        adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind), "afterCall", null,
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "1" : "TRUE"),
                        ILUtils.build(ILLang.op_boolCst, curDiffUnit().isC() ? "0" : "FALSE"));
                if (differentiationMode == DiffConstants.TANGENT_MODE) {
                    blockDifferentiator().addFuturePlainNodeFwd(dbadTree, null, false,
                            null, afterActivTrees, null, afterActivTrees, false, true,
                            "Debug-instrument actives after calling tangent-diff procedure", false);
                } else {
                    blockDifferentiator().addFuturePlainNodeBwd(dbadTree, null, false,
                            null, afterActivTrees, null, afterActivTrees, false, true,
                            "Debug-instrument actives before calling adjoint-diff procedure", false);
                }
            }
            String utilityFunctionSuffix = TapEnv.debugAdMode() == TapEnv.TGT_DEBUG ? "Tgt_exit" : differentiationMode == DiffConstants.TANGENT_MODE ? "Fwd_exit" : "Bwd_call";
            dbadTree = ILUtils.buildCall(
                    ILUtils.tapenadeUtilityFunctionName(curDiffUnit(), "adDebug" + utilityFunctionSuffix),
                    TapEnv.debugAdMode() == TapEnv.TGT_DEBUG || differentiationMode == DiffConstants.TANGENT_MODE ? ILUtils.build(ILLang.op_expressions) : debugCallArgs);
            if (differentiationMode == DiffConstants.TANGENT_MODE) {
                blockDifferentiator().addFuturePlainNodeFwd(dbadTree, null, false,
                        null, null, null, null, false, true,
                        "Debug-instrument after calling tangent-diff procedure", false);
            } else {
                blockDifferentiator().addFuturePlainNodeBwd(dbadTree, null, false,
                        null, null, null, null, false, true,
                        "Debug-instrument before calling adjoint-diff procedure", false);
            }
        }
    }

    private void buildCallTangentNodes(Tree callTree, Tree resultTree,
                                       Unit calledUnit, ActivityPattern calledActivity, boolean mustDiffCall,
                                       SymbolTable fwdSymbolTable,
                                       BoolVector beforeActiv, BoolVector afterActiv,
                                       BoolVector afterUseful,
                                       BoolVector beforeReqX, BoolVector afterReqX) {
        boolean isActiveResult = false;
        boolean isActiveRecipient = false;
        UnitDiffInfo calledDiffInfo = callGraphDifferentiator().getUnitDiffInfo(calledUnit);
        Instruction curInstr = adEnv.curInstruction() ;
        if (resultTree != null) {
            TapList writtenZonesTree =
                    adEnv.curSymbolTable().treeOfZonesOfValue(resultTree, null, curInstr, null);
            BoolVector unitExitActivity = (calledActivity!=null ? calledActivity.exitActivity() : null) ;
            if (unitExitActivity == null) {
                WrapperTypeSpec resultType = calledUnit.functionTypeSpec().returnType;
                isActiveResult = resultType == null ||
                        TypeSpec.isDifferentiableType(resultType.wrappedType);
            } else {
                TapIntList dkResultZones = calledUnit.focusToKind(calledUnit.zonesOfResult(), adEnv.diffKind) ;
                while (!isActiveResult && dkResultZones!=null) {
                    isActiveResult =
                            ADActivityAnalyzer.isActiveArg(dkResultZones.head, unitExitActivity, null);
                    dkResultZones = dkResultZones.tail;
                }
            }
            TapIntList writtenZones = ZoneInfo.listAllZones(writtenZonesTree, true);
            boolean writtenTmp = TapIntList.intersects(writtenZones, adEnv.toActiveTmpZones.tail);
            TapIntList writtenDiffKindVectorIndices =
                    DataFlowAnalyzer.mapZoneRkToKindZoneRk(writtenZones, adEnv.diffKind,
                                                                      adEnv.curSymbolTable());
            isActiveRecipient =
                    afterActiv != null && !adEnv.activeRootCalledFromContext
                            && (writtenTmp || afterActiv.intersects(writtenDiffKindVectorIndices));
            // "PointerActiveUsage":
            DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZones(writtenZonesTree, afterReqX,
                    null, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
        }
        // Lists of the Trees that are Read and Written by the differentiated instruction:
        TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
        // Lists of the Trees whose derivatives are Read and Written by the differentiated instruction:
        TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
        // re-initializations of derivatives that must be done just before the differentiated call.
        // Each element of toNecessaryInitsUS is a TapPair of the Tree that re-initializes the derivative,
        // and of the TapList of the Trees whose derivatives are re-initialized:
        TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsUS = new TapList<>(null, null);
        // re-initializations of derivatives that must be done just after the differentiated call:
        TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsDS = new TapList<>(null, null);
        // (1 or 2) assignments that must be done immediately after the diff call,
        // because of aliasing problems on the function result:
        TapList<TapPair<TapList<Tree>, Tree>> toNecessaryTmpResultAssigns = new TapList<>(null, null);
        Tree diffCallTree, diffTree, initTree;
        // First, place some initializations of diff variables, needed for result and arguments that are
        // *partly* overwritten and that switch from passive to active, and therefore it is the last chance
        // to initialize the derivative to zero explicitly.
        // [llh 22jun2023] we removed restriction to resultTree!=null to add more diff-array initializations.
        // Missing these initializations sometimes leads to segfault (depends on the compiler).
        TapList<ZoneInfo> toInitialized = new TapList<>(null, null) ;
        if (adEnv.curUnitIsActiveUnit && TapEnv.spareDiffReinitializations() /*&& resultTree != null*/) {
            blockDifferentiator().buildDiffPreInitsForTangent(resultTree, callTree, null,
                    beforeActiv, afterActiv, beforeReqX, afterReqX, true, toInitialized);
        }
        if (mustDiffCall) { // When the diff of curUnit calls the diff of calledUnit:

            FunctionTypeSpec calledFunctionTypeSpec =
                    callTree.getAnnotation("functionTypeSpec");
            if (calledFunctionTypeSpec == null) {
                calledFunctionTypeSpec = calledUnit.functionTypeSpec();
            }
            CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
            int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            int nDdZ = adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind);
            Tree[] actualArgs = ILUtils.getArguments(callTree).children();
            Tree actualArg, newArg;
            int nbArgs = actualArgs.length;
// This fix in case of mismatching nbArgs was harmful. Better not do it and be more careful later:
//             nbArgs = Math.min(nbArgs, calledUnit.functionTypeSpec().argumentsTypes.length) ;

            boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(calledActivity);
            TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionInfos =
                    calledActivity == null ? null : calledActivity.varFunctionADActivities();
            BoolVector formalCallActive = new BoolVector(nDdZ);
            BoolVector formalExitActive = new BoolVector(nDdZ);
            BoolVector callR = new BoolVector(nDZ);
            BoolVector callW = new BoolVector(nDZ);
            TapList[] actualArgsCallActive =
                DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.callActivity(), formalCallActive,
                        null, null, true,
                        callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
            TapList[] actualArgsExitActive =
                DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.exitActivity(), formalExitActive,
                        null, null, false,
                        callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
            // For cases mixing pointers and implicit-zero diff, this part (11 lines) is an attempt to use
            // usefulness info to generate more diff initialization downstream (in fwd) the call, where it
            // reaches a place where the implicit-zero variables become useful. cf F90:cm22 & lha84.
            BoolVector actualAfterUseful = null;
            TapList actualResultUseful = null;
            TapList[] actualArgsAfterUseful = null;
            if (afterUseful != null) {
                actualAfterUseful = afterUseful.copy();
                // to split afterUseful into one part on the arguments, and one part on the globals:
                actualResultUseful = new TapList<>(Boolean.FALSE, null);
                actualArgsAfterUseful =
                    DataFlowAnalyzer.propagateCallSiteDataToActualArgs(actualAfterUseful, resultTree, actualArgs, actualResultUseful,
                                                                       adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind),
                                                                       adEnv.toActiveTmpZones.tail, curInstr, callArrow, true, adEnv.diffKind);
            }
            // end 11 lines

// TapEnv.printlnOnTrace("actualArgs(Call->Exit):formalArgsActive of "+calledUnit+":") ;
// for (int i=1 ; i<=nbArgs ; ++i)
//   TapEnv.printlnOnTrace("       "+i+": ("+actualArgsCallActive[i]+"->"+actualArgsExitActive[i]+") : "+formalArgsActivity[i-1]) ;
// TapEnv.printlnOnTrace("  result: ("+actualArgsCallActive[0]+"->"+actualArgsExitActive[0]+") : "+formalArgsActivity[nbArgs]) ;

// Useless for the moment, but may become useful when we refine diff explicit-zero on pointers:
//         BoolVector actualBeforeActive=null, actualAfterActive=null ;
//         TapList[] actualArgsBeforeActive=null, actualArgsAfterActive=null ;
//         TapList actualResultAfterActive = new TapList(new Boolean(false), null) ;
//         if (beforeActiv!=null && !adEnv.activeRootCalledFromContext) {
//             actualBeforeActive = beforeActiv.copy() ;
//             //Special call, only to split beforeActiv into one part on the arguments, and one part on the globals:
//             actualArgsBeforeActive =
//                 DataFlowAnalyzer.propagateCallSiteDataToActualArgs(actualBeforeActive, resultTree, actualArgs, null,
//                                                        adEnv.toActiveTmpZones.tail, curInstr, callArrow, true, adEnv.diffKind) ;
//         }
//         if (afterActiv!=null && !adEnv.activeRootCalledFromContext) {
//             actualAfterActive = afterActiv.copy() ;
//             //Special call, only to split afterActiv into one part on the arguments, and one part on the globals:
//             actualArgsAfterActive =
//                 DataFlowAnalyzer.propagateCallSiteDataToActualArgs(actualAfterActive, resultTree, actualArgs, actualResultAfterActive,
//                                                        adEnv.toActiveTmpZones.tail, curInstr, callArrow, true, adEnv.diffKind) ;
//         }

            TapList[] paramsR = DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyR(), callR, null, null, true,
                                                                               callTree, curInstr, callArrow,
                                                                               SymbolTableConstants.ALLKIND,
                                                                               null, null, true, false) ;
            TapList[] paramsW = DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), callW, null, null, true,
                                                                               callTree, curInstr, callArrow,
                                                                               SymbolTableConstants.ALLKIND,
                                                                               null, null, false, false) ;
            TapList<Tree> newArgs = null;
            varRefDifferentiator().pushDiffContext(DiffConstants.INCALL);
            WrapperTypeSpec formalArgType;
            Unit diffCalledUnit = calledDiffInfo == null ? null : calledDiffInfo.getTangent(calledActivity);
            int firstOrderingProblem = findNameEqOrNone(callTree);
            if (multiDirMode() && TapEnv.get().fixedNbdirsTree==null) {
                // Add an argument for the number of differentiation directions:
                newArg = varRefDifferentiator().buildDirNumberReference(fwdSymbolTable);
                if (firstOrderingProblem > -1) {
                    // this is wrong: the name of the arg must come from the called diff routine !
                    Tree argName = varRefDifferentiator().buildDirNumberReference(fwdSymbolTable);
                    newArg = ILUtils.build(ILLang.op_nameEq, argName, newArg);
                }
                newArgs = new TapList<>(newArg, newArgs);
            }
            Tree passAsResult = null;
            if (!TapEnv.associationByAddress()) {
                // When original result aliases with a written argument, then we must introduce a tmp result: (cf F90:lh87)
                Tree tmpResultTree = null;
                if (zonesOverlapCall(resultTree, callTree)
                        // if formal result passive, no need for tmpResultTree (see below)
                        && formalArgsActivity[formalArgsActivity.length - 1]) {
                    ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                    tmpResultTree =
                        adEnv.curBlock.buildTemporaryVariable("tmpresult", resultTree, fwdSymbolTable,
                                                      toNSH, false, null, true, calledFunctionTypeSpec, 0, resultTree) ;
                    NewSymbolHolder tmpVarHolder = toNSH.obj() ;
                    tmpVarHolder.zone = adEnv.allocateTmpZone();
                }
                // Now add the arguments (for diff of original result when diff is procedure and) for original result:
                if (resultTree != null && calledUnit.isAFunction()) {

                    if (formalArgsActivity[formalArgsActivity.length - 1]) {
                        diffRefsRW.second =
                                ILUtils.addTreeInList(
                                        resultTree, diffRefsRW.second);
                        if (passAsResult == null && diffFunctionIsFunction(calledUnit)) {
                            // We decide to pass diff result as the result of diff function.
                            newArg = varRefDifferentiator().diffVarRef(adEnv.curActivity(), resultTree,
                                    fwdSymbolTable, false, resultTree, false, true, null);
                            passAsResult = newArg;
                        } else {
                            // We decide to pass diff result as an (out) formal arg.
                            for (adEnv.iReplic=TapEnv.diffReplica()-1 ; adEnv.iReplic>=0 ; --adEnv.iReplic) {
                                // When required, insert a temporary "tmpresult"(_diff) instead of diff result (cf set03/lh087)
                                if (tmpResultTree != null) {
                                    varRefDifferentiator().pushDiffContext(DiffConstants.INEXPR); // to force usage of vector dim index.
                                    newArg = varRefDifferentiator().diffVarRef(adEnv.curActivity(), resultTree, fwdSymbolTable,
                                        false, resultTree, false, true, null);
                                    Tree tmpDiffResultTree =
                                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), tmpResultTree, fwdSymbolTable,
                                                false, tmpResultTree, false, true, null);
                                    toNecessaryTmpResultAssigns.placdl(
                                        new TapPair<>(
                                                new TapList<>(resultTree, null),
                                                ILUtils.build(ILLang.op_assign, newArg, tmpDiffResultTree)));
                                    varRefDifferentiator().popDiffContext();
                                }
                                newArg = varRefDifferentiator().diffVarRef(adEnv.curActivity(),
                                                                           (tmpResultTree != null ? tmpResultTree : resultTree),
                                                                           fwdSymbolTable, false, resultTree, false, true, null);
                                if (curDiffUnit().isC() && TapEnv.diffReplica()>1) {
                                    newArg = ILUtils.addAddressOf(newArg);
                                }
                                if (firstOrderingProblem > -1) {
                                    Tree argName = getNewParamName(-1, calledUnit, true, DiffConstants.TANGENT, diffCalledUnit);
                                    newArg = ILUtils.build(ILLang.op_nameEq, argName, newArg);
                                }
                                newArgs = new TapList<>(newArg, newArgs);
                            }
                            adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                        }
                    }

                    // TODO: change the strategy (by removing the next test "diffFunctionIsFunction")
                    // to let a tangent-multi ("DV") become a function that returns the primal result.
                    if (passAsResult == null && diffFunctionIsFunction(calledUnit)) {
                        // We decide to pass primal result as the result of diff function.
                        passAsResult = ILUtils.copy(resultTree);
                    } else {
                        // We decide to pass primal result as an (out) formal arg.                        
                        // When required, insert a temporary "tmpresult" instead of primal result (cf set03/lh087)
                        if (tmpResultTree != null) {
                            toNecessaryTmpResultAssigns.placdl(
                                    new TapPair<>(
                                            null,
                                            ILUtils.build(ILLang.op_assign, ILUtils.copy(resultTree), tmpResultTree)));
                        }
                        newArg = ILUtils.copy(tmpResultTree != null ? tmpResultTree : resultTree);
                        if (curDiffUnit().isC()) {
                            if (diffCalledUnit != null && diffCalledUnit.functionTypeSpec() != null) {
                                int length = diffCalledUnit.functionTypeSpec().argumentsTypes.length;
                                if (multiDirMode()) {
                                    length = length - 1; // last argument is "nbdirs"
                                }
                                if (TypeSpec.isA(diffCalledUnit.functionTypeSpec().argumentsTypes[length - 1], SymbolTableConstants.POINTERTYPE)
                                    || MixedLanguageInfos.passesByValue(adEnv.curUnit().language(), adEnv.curUnit().language())) {
                                    newArg = ILUtils.addAddressOf(newArg);
                                    String varName = ILUtils.baseName(resultTree);
                                    if (varName != null) {
                                        VariableDecl resultVar = fwdSymbolTable.getVariableDecl(varName);
                                        if (resultVar != null && resultVar.extraInfo() != null) {
                                            TapList<String> extraInfo = TapList.cleanExtraInfoValue(resultVar.extraInfo(), "register");
                                            resultVar.setExtraInfo(extraInfo);
                                            Instruction instr = fwdSymbolTable.declarationsBlock.getOperatorDeclarationInstruction(
                                                    resultVar, ILLang.op_varDeclaration, fwdSymbolTable);
                                            if (instr != null && instr.tree.down(2).opCode() == ILLang.op_modifiedType
                                                    && ILUtils.containsModifier("register", instr.tree, true)) {
                                                ILUtils.peelModifier(instr.tree, "register");
                                            }
                                        }
                                    }
                                }
                            } else {
                                // cf C:v104 avec declaration forward //[llh]: I think this case doesn't occur any more !?
                                if (calledUnit.functionTypeSpec().argumentsTypes.length != 0) {
                                    newArg = ILUtils.addAddressOf(newArg);
                                }
                            }
                        }
                        if (firstOrderingProblem > -1) {
                            Tree argName = getNewParamName(-1, calledUnit, false, -1, null);
                            newArg = ILUtils.build(ILLang.op_nameEq, argName, newArg);
                        }
                        newArgs = new TapList<>(newArg, newArgs);
                    }
                    refsRW.first =
                            ILUtils.usedVarsInExp(
                                    resultTree, refsRW.first, false);
                    refsRW.second =
                            ILUtils.addTreeInList(resultTree, refsRW.second);
                } else if (resultTree == null && calledUnit.isAFunction()) {
                    if (formalArgsActivity[formalArgsActivity.length - 1]) {
                        ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                        newArg = adEnv.curBlock.buildTemporaryVariable("result", callTree, fwdSymbolTable, toNSH,
                                                                        false, null, true, calledFunctionTypeSpec, 0, null) ;
                        NewSymbolHolder emptyResultSymbolHolder = toNSH.obj() ;
                        emptyResultSymbolHolder.zone = adEnv.allocateTmpZone();
                        if (curDiffUnit().isC()) {
                            newArg = ILUtils.addAddressOf(newArg);
                        }
                        newArgs = new TapList<>(newArg, newArgs);

                    }
                }
            }
            TapList argZonesTree;
            TapList<ZoneInfo> zonesToDiffInitUS = null;
            TapList<Tree> treesToDiffInitDS = null;
            for (int i = nbArgs - 1; i >= 0; --i) {
                actualArg = actualArgs[i];
                if (adEnv.curSymbolTable().identIsAFunction(actualArg)) {
                    if (!TapEnv.doActivity() || varFunctionInfos != null && varFunctionInfos[i] != null) {
                        // if this argument is a function name which is active:
                        TapList<ActivityPattern> functionActivities = null;
                        SymbolDecl funcDecl = adEnv.curSymbolTable().getSymbolDecl(ILUtils.getIdentString(actualArgs[i]));
                        if (funcDecl instanceof FunctionDecl) {
                            functionActivities =
                                    ((FunctionDecl) adEnv.curSymbolTable().getSymbolDecl(ILUtils.getIdentString(actualArgs[i])))
                                            .unit().activityPatterns;
                        }
                        // A function passed as argument is allowed only one (not connected) activity pattern:
                        newArg = varRefDifferentiator().diffSymbolTree(
                                functionActivities != null ? functionActivities.head : null,
                                actualArg, fwdSymbolTable, false, true, false,
                                null, false, DiffConstants.TANGENT, DiffConstants.TANGENT, DiffConstants.VAR, null);
                        newArg.setAnnotation("isFunctionName", Boolean.TRUE);
                        newArgs = new TapList<>(newArg, newArgs);
                    }
                } else if (i < formalArgsActivity.length - 1
                        && formalArgsActivity[i]
                        && actualArg.opCode() != ILLang.op_none) {
                    Tree[] diffTreeR = null ;
                    // If this argument is formal-active and actual-PRESENT (case of optional args):
                    formalArgType = calledFunctionTypeSpec.argumentsTypes[i];
                    // TODO ne pas rajouter .d pour les arguments si instrValue == null
                    Tree newActualArg = actualArg;
                    if (TypeSpec.isA(formalArgType, SymbolTableConstants.POINTERTYPE)
                            || ILUtils.isAWritableIdentVarRef(actualArg, adEnv.curSymbolTable())) {
                        diffTreeR = new Tree[TapEnv.diffReplica()] ;
                        for (adEnv.iReplic=0 ; adEnv.iReplic<diffTreeR.length ; ++adEnv.iReplic) {
                            diffTreeR[adEnv.iReplic] = varRefDifferentiator().diffVarRef(adEnv.curActivity(), actualArg, fwdSymbolTable,
                                                                                         false, actualArg, false, true, null);
                        }
                        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                    } else {
                        ToObject<Tree> toActualArg = new ToObject<>(actualArg);
                        diffTreeR = blockDifferentiator().tangentDiffExpr(toActualArg, beforeActiv,
                                refsRW, diffRefsRW);
                        newActualArg = toActualArg.obj();
                    }
                    boolean noDiffTree = (diffTreeR.length==0 || diffTreeR[0] == null) ;
                    boolean diffTreeIsDummyVar = false;
                    if (noDiffTree) {
                        diffTree = null ;
                        if (multiDirMode()
                                || ILUtils.isAWritableVarRef(actualArg, adEnv.curSymbolTable())
                                || formalArgType != null && !formalArgType.isScalar()) {
                            //When the formal arg is active, but the actual arg is passive
                            // and (we are in vector mode OR the actual arg is a complex reference expression
                            // OR the actual arg cannot be written here ("intent in" cf set07/v502)
                            // OR the formal arg's expected type is not a simple scalar),
                            // we must pass a dummy temp object of the formal arg's expected type,
                            // differentiated and initialized to 0.0 (cf set03/lh094)
                            if (TypeSpec.isA(formalArgType, SymbolTableConstants.METATYPE)) {
                                formalArgType = adEnv.realTypeSpec;
                            }
                            if (formalArgType != null && formalArgType.wrappedType != null) {
                                ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                                actualArg =
                                    adEnv.curBlock.buildTemporaryVariable("dummyzerodiff", actualArg, fwdSymbolTable,
                                                      toNSH, false, null, false, calledFunctionTypeSpec, i+1, newActualArg) ;
                                NewSymbolHolder dummySymbolHolder = toNSH.obj() ;
                                dummySymbolHolder.zone = adEnv.allocateTmpZone();
                                diffTree = varRefDifferentiator().diffVarRef(adEnv.curActivity(), actualArg, fwdSymbolTable,
                                        adEnv.curUnit().isFortran() && !multiDirMode(),
                                        actualArg, false, true, ILUtils.copy(actualArgs[i]));
                                diffRefsRW.first = ILUtils.addTreeInList(actualArg, diffRefsRW.first);
                                Tree baseInitTree = dummySymbolHolder.makeNewRef(fwdSymbolTable);
                                TapList<Tree> diffWrittenTrees = new TapList<>(baseInitTree, null);
                                Tree[] diffInitTreeR =
                                        makeDiffInitializationOfCallArgument(baseInitTree, actualArgs[i], fwdSymbolTable,
                                                                             dummySymbolHolder.typeSpec() ,
                                                                             formalArgType, false, null, true);
                                for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                                    Tree diffInitTree = diffInitTreeR[iReplic] ;
                                    if (diffInitTree != null) {
                                        toNecessaryInitsUS.placdl(new TapPair<>(diffWrittenTrees, diffInitTree));
                                    }
                                }
                                diffTreeIsDummyVar = true;
                            }
                        } else if (formalArgType != null && formalArgType.wrappedType != null) {
                            // For non-reference expressions: build a 0.0 derivative:
                            diffTree = formalArgType.wrappedType.buildConstantZero();
                        }
                        // When everything else fails: build a 0.0 derivative:
                        if (diffTree == null) {
                            diffTree = PrimitiveTypeSpec.buildRealConstantZero();
                        }
                        for (int iReplic=0 ; iReplic<diffTreeR.length ; ++iReplic) {
                            diffTreeR[iReplic] = ILUtils.copy(diffTree) ;
                        }
                    }
                    if (firstOrderingProblem != -1 &&
                            (i >= firstOrderingProblem ||
                                    actualArg.getAnnotation("sourcetree") != null)) {
                        for (adEnv.iReplic=0 ; adEnv.iReplic<diffTreeR.length ; ++adEnv.iReplic) {
                            Tree argName = getNewParamName(i, calledUnit, true, DiffConstants.TANGENT, diffCalledUnit);
                            diffTreeR[adEnv.iReplic] = ILUtils.build(ILLang.op_nameEq, argName, diffTreeR[adEnv.iReplic]);
                        }
                        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                    }
                    // Prepare the data dependence graph due to the diff actual argument:
                    if (!noDiffTree) {
                        refsRW.first =
                                ILUtils.usedVarsInExp(actualArg, refsRW.first, false);
                        if (TapList.oneTrue(paramsR[i+1])) {
                            diffRefsRW.first = ILUtils.addTreeInList(actualArg, diffRefsRW.first);
                        }
                        if (TapList.oneTrue(paramsW[i+1])) {
                            diffRefsRW.second = ILUtils.addTreeInList(actualArg, diffRefsRW.second);
                        }
                    }
                    if (!TapEnv.associationByAddress()) {
                        for (int iReplic=diffTreeR.length-1 ; iReplic>=0 ; --iReplic) {
                            newArgs = new TapList<>(diffTreeR[iReplic], newArgs);
                        }
                    }
                    if (TapEnv.spareDiffReinitializations() && beforeActiv != null && !adEnv.activeRootCalledFromContext
                            // [vmp 2015/10/15] diffTreeIsDummyVar is true if diff Tree is a "dummyzerodiff",
                            //  in which case the instruction that initializes it is already in toNecessaryInitsUS:
                            && !diffTreeIsDummyVar
                            && ILUtils.isAWritableVarRef(actualArg, adEnv.curSymbolTable())) {
                        //When the actual argument actualArg is not active before the call,
                        // but the formal argument is active, we must (re)initialize the differential
                        // of actualArg to zero, since it may be used inside the called function:
                        argZonesTree =
                                adEnv.curSymbolTable().treeOfZonesOfValue(actualArg, null, curInstr, null);
                        argZonesTree = TapList.copyTree(argZonesTree);
                        DataFlowAnalyzer.includePointedElementsInTree(argZonesTree,
                                null, null, true, adEnv.curSymbolTable(), curInstr, false, true);
                        TapList actualArgsCallActiveI = actualArgsCallActive == null ? null : actualArgsCallActive[1+i];
                        TapList actualArgsExitActiveI = actualArgsExitActive == null ? null : actualArgsExitActive[1+i];
                        zonesToDiffInitUS =
                                collectZonesFormalActiveAndActualPassive(argZonesTree, adEnv.toActiveTmpZones.tail,
                                        actualArgsCallActiveI, beforeActiv,
                                        calledUnit, zonesToDiffInitUS);
                        // [llh] COMMENTED OUT BECAUSE DOES NOT WORK: je voulais finasser pour rester implicit-zero
                        //   sur les derivees avec pointeurs non-mutables (e.g. allocate). cf set03/lh090.
                        //                     // Trees to be diff-initialized just after the call: (entry-active;exit-passive;after-active), and also
                        //                     // (entry-active;exit-passive) parts of an actual arg that uses a mutable pointer.
                        // TapEnv.printlnOnTrace("Arg DiffInit after call "+calledUnit.name+", arg"+i+"(+1):"+actualArg+" zonesTree:"+argZonesTree+" active_US:"+actualArgsCallActive[i]+" active_DS:"+actualArgsExitActive[i]+" active_after:"+actualArgsAfterActive[i]+" <=="+treesToDiffInitDS) ;
                        //                     treesToDiffInitDS = collectTreesToDiffInitAfterDiffCall(actualArg, isAccessedThroughMutablePointer(actualArg),
                        //                                                          curSymbolTable().typeOf(actualArg), argZonesTree,
                        //                                                          actualArgsCallActive[i], actualArgsExitActive[i], paramsW[i+1],
                        //                                                          actualArgsAfterActive[i], calledUnit, treesToDiffInitDS) ;
                        // TapEnv.printlnOnTrace("==>"+treesToDiffInitDS) ;

                        // Second try: do the same for all pointers, mutable or not !
                        // Trees to be diff-initialized just after the call:
                        // (entry-active;exit-passive) parts of an actual arg that uses a pointer.
                        // ATTENTION THIS CODE IS FRAGILE: It is not coherent with
                        // what is done in the adjoint mode. In adjoint mode, we DON'T
                        // nullify the 8th argument to make F77:lha56 work correctly !!
                        // TapEnv.printlnOnTrace("Collect (TGT) DiffInit after call "+calledUnit.name+", arg"+i+"(+1):"+actualArg+" zonesTree:"+argZonesTree+" formalActiveAfter:"+actualArgsExitActiveI+" formalActiveBefore:"+actualArgsCallActiveI+" actualActiveAfter:"+null+" <=="+treesToDiffInitDS) ;

                        //[llh 12/10/2015] cf bug Gugala F90:v500 (also F90:lh91,v509 C:v206): don't try to
                        // diff-init anything if the argument is not modified by the called procedure:
                        if (paramsW != null && TapList.oneTrue(paramsW[i+1])) {
                            WrapperTypeSpec formalType = i < nbArgs ? calledFunctionTypeSpec.argumentsTypes[i] : null;
                            treesToDiffInitDS = collectTreesToDiffInitAfterDiffCall(actualArg,
                                    isAccessedThroughPointerOrIndex(actualArg, adEnv.curSymbolTable(),
                                            adEnv.curUnit().language(), calledUnit, i + 1),
                                    adEnv.curSymbolTable().typeOf(actualArg), formalType,
                                    argZonesTree, actualArgsCallActiveI, actualArgsExitActiveI, paramsW[i+1],
                                    // for cases mix of pointers and implicit-zero diff, I have tried to replace
                                    // the next "null" with "actualArgAfterUseful":
                                    null, calledUnit, treesToDiffInitDS, null, null);
                            // TapEnv.printlnOnTrace("treesToDiffInitDS now "+treesToDiffInitDS) ;
                        }
                    }
                }
                actualArg = actualArgs[i];
                newArg = ILUtils.copy(actualArg);
                if (actualArg.getAnnotation("sourcetree") != null) {
                    newArg = ILUtils.copy(actualArg.getAnnotation("sourcetree"));
                } else if (firstOrderingProblem != -1 && i >= firstOrderingProblem
                        && actualArg.opCode() != ILLang.op_none) {
                    Tree argName;
                    if (newArg.opCode() == ILLang.op_nameEq) {
                        argName = newArg.cutChild(1);
                        newArg = newArg.cutChild(2);
                    } else {
                        argName = getNewParamName(i, calledUnit, false, -1, null);
                    }
                    newArg = ILUtils.build(ILLang.op_nameEq, argName, newArg);
                }
                if (adEnv.curSymbolTable().identIsAFunction(actualArg)) {
                    newArg.setAnnotation("isFunctionName", Boolean.TRUE);
                }
                // Prepare the data dependence graph due to the primal actual argument:
                refsRW.first =
                        ILUtils.usedVarsInExp(actualArg, refsRW.first, false);
                if (TapList.oneTrue(paramsR[i+1])) {
                    refsRW.first =
                            ILUtils.addTreeInList(actualArg, refsRW.first);
                }
                if (TapList.oneTrue(paramsW[i+1])) {
                    refsRW.second =
                            ILUtils.addTreeInList(actualArg, refsRW.second);
                }
                if (!TapEnv.associationByAddress() || !formalArgsActivity[i]) {
                    newArgs = new TapList<>(newArg, newArgs);
                } else if (TapEnv.associationByAddress()) {
                    newArgs = new TapList<>(newArg, newArgs);
                }
            }
            if (!calledUnit.isIntrinsic()) {
                ZoneInfo zoneInfo;
                Tree zoneTree;
                int iDiff;
                // Prepare the data dependence graph due to the global/side-effect arguments:
                for (int i = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) - 1; i >= 0; i--) {
                    zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                    if (zoneInfo != null) {
                        iDiff = zoneInfo.kindZoneNb(adEnv.diffKind);
                        zoneTree = zoneInfo.accessTree;
                        if (iDiff >= 0
                            && (formalCallActive.get(iDiff)
                                || formalExitActive.get(iDiff))) {
                            if (callR.get(i)) {
                                diffRefsRW.first =
                                        ILUtils.addTreeInList(
                                                zoneTree, diffRefsRW.first);
                            }
                            if (callW.get(i)) {
                                diffRefsRW.second =
                                        ILUtils.addTreeInList(
                                                zoneTree, diffRefsRW.second);
                            }
                        }
                        if (callR.get(i)) {
                            refsRW.first =
                                    ILUtils.addTreeInList(
                                            zoneTree, refsRW.first);
                        }
                        if (callW.get(i)) {
                            refsRW.second =
                                    ILUtils.addTreeInList(
                                            zoneTree, refsRW.second);
                        }
                    }
                }
            }
            // Check for global parameters that are not active upon call,
            // but are used as active in the call:
            // we must re-initialize their diff to 0.0
            ZoneInfo zoneInfo;
            if (TapEnv.spareDiffReinitializations()
                    && !calledUnit.isIntrinsic() && beforeActiv != null && !adEnv.activeRootCalledFromContext) {
                for (int i = nDdZ - 1; i >= 0; --i) {
                    if (!beforeActiv.get(i) && formalCallActive.get(i)) {
                        zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, adEnv.diffKind);
                        if (!zoneInfo.isConstant() && !TapList.contains(zonesToDiffInitUS, zoneInfo)) {
                            if (zoneInfo.isHidden) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(AD14) Differentiating this call to " + calledUnit.name() + " needs to initialize derivative of hidden variable: " + zoneInfo.description);
                                adEnv.addInHiddenDiffInitVariables(zoneInfo);
                            } else {
                                zonesToDiffInitUS = new TapList<>(zoneInfo, zonesToDiffInitUS);
                            }
                        }
                    }
                }
            }
            varRefDifferentiator().popDiffContext();
            Tree[] zoneReinitTreeR;
            Tree zoneReinitTree;
            while (zonesToDiffInitUS != null) {
              zoneInfo = zonesToDiffInitUS.head;
              // toInitialized contains zones that have already been initialized by blockDifferentiator().buildDiffPreInitsForTangent()
              if (!TapList.contains(toInitialized.tail, zoneInfo)) {
                zoneReinitTreeR = blockDifferentiator().makeDiffInitialization(zoneInfo.accessTree, zoneInfo, adEnv.curSymbolTable(), fwdSymbolTable,
                                                                               null, true, null, true);
                for (int iReplic=0 ; iReplic<zoneReinitTreeR.length ; ++iReplic) {
                    zoneReinitTree = zoneReinitTreeR[iReplic] ;
                    if (zoneReinitTree != null) {
                        toNecessaryInitsUS.placdl(new TapPair<>(new TapList<>(zoneInfo.accessTree, null), zoneReinitTree));
                    }
                }
              }
              zonesToDiffInitUS = zonesToDiffInitUS.tail;
            }
            while (treesToDiffInitDS != null) {
                Tree treeToDiffReinit = treesToDiffInitDS.head;
                // Don't reinit the diff if it has already been set as the diff or function result. cf F90:v268+v271.
                if (!zonesOverlapArg(resultTree, treeToDiffReinit)) {
                    zoneReinitTreeR = blockDifferentiator().makeDiffInitialization(treeToDiffReinit, null,
                                                                                   adEnv.curSymbolTable(), fwdSymbolTable,
                                                                                   null, false, null, true);
                    for (int iReplic=0 ; iReplic<zoneReinitTreeR.length ; ++iReplic) {
                        zoneReinitTree = zoneReinitTreeR[iReplic] ;
                        if (zoneReinitTree != null) {
                            toNecessaryInitsDS.placdl(new TapPair<>(new TapList<>(treeToDiffReinit, null), zoneReinitTree));
                        }
                    }
                }
                treesToDiffInitDS = treesToDiffInitDS.tail;
            }

            if (diffCalledUnit != null) {
                CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, diffCalledUnit);
            }
            Tree diffCalledUnitName =
                    varRefDifferentiator().diffSymbolTree(calledActivity,
                            ILUtils.copy(ILUtils.getCalledName(callTree)),
                            fwdSymbolTable, false, true, false,
                            callTree, false, DiffConstants.TANGENT, DiffConstants.TANGENT,
                            DiffConstants.PROC, null);
            diffCallTree = ILUtils.buildCall(diffCalledUnitName,
                    ILUtils.build(ILLang.op_expressions, newArgs));

            toNecessaryInitsUS = toNecessaryInitsUS.tail;
            diffTree = diffCallTree;
            if (resultTree != null
                    //&& (isActiveResult || TapList.oneTrue(resultPointerActivity))
                    && diffFunctionIsFunction(calledUnit)) {
                Unit diffFunctionUnit = calledDiffInfo == null ? null : calledDiffInfo.getTangent(calledActivity);
                FunctionTypeSpec diffFunctionTypeSpec = diffFunctionUnit == null ? null : diffFunctionUnit.functionTypeSpec();
                WrapperTypeSpec resultTypeSpecDiff = diffFunctionTypeSpec == null ? null : diffFunctionTypeSpec.returnType;
                if (diffFunctionUnit != null && formalArgsActivity[formalArgsActivity.length - 1] && WrapperTypeSpec.isNullOrVoid(resultTypeSpecDiff)) {
                    //TapEnv.toolWarning(-1, "(Tangent differentiation of procedure call) null-type for diff function result: "+callTree) ;
                } else {
                    if (TapEnv.associationByAddress()) {
                        passAsResult = ILUtils.copy(resultTree);
                    }
                    if (passAsResult != null) {
                        diffTree = ILUtils.build(ILLang.op_assign, passAsResult, diffCallTree);
                    }
                }
            }
            while (toNecessaryInitsUS != null) {
                initTree = toNecessaryInitsUS.head.second;
                blockDifferentiator().addFuturePlainNodeFwd(initTree, null, false,
                        null, null, null, toNecessaryInitsUS.head.first, false, false,
                        "Needed diff call pre-init", false);
                toNecessaryInitsUS = toNecessaryInitsUS.tail;
            }
        } else {
            // When the diff of curUnit only calls the original calledUnit:
            diffTree = ILUtils.copy(callTree);
            if (resultTree != null) {
                diffTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(resultTree), diffTree);
            }
            if (TapEnv.associationByAddress()) {
                diffTree = blockDifferentiator().turnAssociationByAddressPrimal(diffTree, adEnv.curSymbolTable(),
                        adEnv.curFwdSymbolTable, null, true);
            }
            Unit primalCalledUnit = adEnv.getPrimalCopyOfOrigUnit(calledUnit);
            // If the called routine has no primal copy, we place a CallArrow to the source called routine
            // Danger [19Mar18]: this is the 1st example of a CallArrow between 2 different CallGraph's...
            if (primalCalledUnit == null) {
                primalCalledUnit = calledUnit;
                // TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF a "+curFwdDiffUnit()+" TO ORIG "+primalCalledUnit) ;
                //                     } else {
                // TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF a "+curFwdDiffUnit()+" TO PRIMAL COPY "+primalCalledUnit) ;
            }
            if (primalCalledUnit != null) {
                CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, primalCalledUnit);
            }
            fillCallRefsRW(callTree, null, calledUnit, refsRW);
            if (resultTree != null) {
                refsRW.first =
                        ILUtils.usedVarsInExp(resultTree, refsRW.first, false);
                refsRW.second =
                        ILUtils.addTreeInList(resultTree, refsRW.second);
            }
        }
        blockDifferentiator().addFuturePlainNodeFwd(diffTree, curInstr.whereMask(), false,
                refsRW.first, refsRW.second,
                diffRefsRW.first, diffRefsRW.second, calledUnit.isExternal(), true,
                (mustDiffCall ? "Differentiated call" : "Copy of primal call"), true);

        toNecessaryTmpResultAssigns = toNecessaryTmpResultAssigns.tail;
        if (mustDiffCall && toNecessaryTmpResultAssigns != null) {
            TapPair<TapList<Tree>, Tree> postAssign = toNecessaryTmpResultAssigns.head;
            blockDifferentiator().addFuturePlainNodeFwd(postAssign.second, curInstr.whereMask(), false,
                    null, null, null, postAssign.first, false, false,
                    "Split aliased result", false);

            toNecessaryTmpResultAssigns = toNecessaryTmpResultAssigns.tail;
            while (toNecessaryTmpResultAssigns != null) {
                // If we need an assign to the (vectorial) diff result:
                postAssign = toNecessaryTmpResultAssigns.head;
                blockDifferentiator().addFuturePlainNodeFwd(postAssign.second, curInstr.whereMask(), true,
                        null, null, null, postAssign.first, false, false,
                        "Split aliased diff result", false);
                toNecessaryTmpResultAssigns = toNecessaryTmpResultAssigns.tail;
            }
        }
        if (resultTree != null && isActiveRecipient && !isActiveResult) {
            Tree[] resetTreeR;
            Tree resetTree;
            resetTreeR = blockDifferentiator().makeDiffInitialization(resultTree, null, adEnv.curSymbolTable(), fwdSymbolTable,
                                                                      null, false, null, true);
            for (int iReplic=0 ; iReplic<resetTreeR.length ; ++iReplic) {
                resetTree = resetTreeR[iReplic] ;
                if (resetTree != null) {
                    blockDifferentiator().addFuturePlainNodeFwd(resetTree, curInstr.whereMask(), false,
                        ILUtils.usedVarsInExp(resultTree, null, false), null,
                        null, new TapList<>(resetTree, null), false, false,
                        "Reset diff result", false);
                }
            }
        }
        if (mustDiffCall) {
            toNecessaryInitsDS = toNecessaryInitsDS.tail;
            while (toNecessaryInitsDS != null) {
                initTree = toNecessaryInitsDS.head.second;
                blockDifferentiator().addFuturePlainNodeFwd(initTree, null, false,
                        null, null, null, toNecessaryInitsDS.head.first, false, false,
                        "Needed diff call post-init", false);
                toNecessaryInitsDS = toNecessaryInitsDS.tail;
            }
        }
    }

    private void buildContextCallTangentNodes(Tree callTree, Tree lhs,
                                              Unit calledUnit, ActivityPattern calledActivity, boolean mustDiffCall,
                                              SymbolTable fwdSymbolTable) {

        TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
        fillCallRefsRW(callTree, lhs, calledUnit, refsRW);
        Tree fwdCallTree;
        Tree fwdTree;
        Tree[] args;
        if (mustDiffCall
            && (calledUnit.isStandard() || calledUnit.isExternal())
            && adEnv.curUnitIsContext) {
            // Build the call to subroutine_d:
            // Dirty trick: it turns out that differentiateProcedureHeader() does just what we want
            // but this is dirty because actually, we are not differentiating a header !
            Tree tgtTree2 = null;
            if (adEnv.activeRootCalledFromContext) {
                // Bizarre case where the context calls {rootUnit_FWD() ; rootUnit_BWD() ;}
                tgtTree2 = differentiateProcedureHeader(ILUtils.copy(callTree), lhs, null, calledActivity,
                        fwdSymbolTable, curFwdDiffUnit().privateSymbolTable(), DiffConstants.TANGENT, null);
                // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
                args = ILUtils.getArguments(tgtTree2).children();
                for (int i = args.length - 1; i >= 0; --i) {
                    if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                        args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                    }
                }
                blockDifferentiator().addFuturePlainNodeBwd(tgtTree2, null, false,
                        refsRW.first, refsRW.second, null, null, false, true,
                        "TANGENT (in context) call", false);
            }

            int thisDiffMode = DiffConstants.TANGENT;
            if (adEnv.curUnitIsContext && !adEnv.activeRootCalledFromContext) {
                thisDiffMode = DiffConstants.TANGENT;
            }
            ToObject<Tree> toDiffLhs = new ToObject<>(null);
            fwdCallTree = differentiateProcedureHeader(ILUtils.copy(callTree), lhs, toDiffLhs, calledActivity,
                    fwdSymbolTable, curFwdDiffUnit().privateSymbolTable(), thisDiffMode, null);
            fwdTree = fwdCallTree;
            // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
            args = ILUtils.getArguments(fwdCallTree).children();
            for (int i = args.length - 1; i >= 0; --i) {
                if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                    args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                }
            }

            boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(calledActivity);
            if (lhs != null
                    //There's no function result when context calls differentiated non-context function (cf F90:lha60)
                    && !adEnv.activeRootCalledFromContext) {
                // function call
                Tree resultDiffTree = toDiffLhs.obj();
                if (resultDiffTree == null) {
                    resultDiffTree = ILUtils.copy(lhs);
                }
                fwdTree = ILUtils.build(ILLang.op_assign, resultDiffTree, fwdTree);
            }
            if (adEnv.activeRootCalledFromContext) {
                blockDifferentiator().addFuturePlainNodeBwd(fwdTree, adEnv.curInstruction().whereMask(), false,
                        refsRW.first, refsRW.second, null, null, false, true,
                        "context tangent call", false);
            } else {
                blockDifferentiator().addFuturePlainNodeFwd(fwdTree, adEnv.curInstruction().whereMask(), false,
                        refsRW.first, refsRW.second, null, null, false, true,
                        "Tangent call", false);
            }

            Unit fwdDiffCalledUnit = adEnv.getDiffOfUnit(calledUnit, calledActivity, thisDiffMode);
            if (fwdDiffCalledUnit != null) {
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO FWD SPLIT OR CONTEXT "+fwdDiffCalledUnit) ;
                CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, fwdDiffCalledUnit);
            }
        } else {

//                 // If original instruction was split, remove the "sourcetree" annotation
//                 // so that code decompilation doesn't reuse the original instruction tree:
//                 if (callTree.getAnnotation("hasBeenSplit")==Boolean.TRUE)
//                     callTree.removeAnnotation("sourcetree") ;

            // Put back the original source call in case of nameEq or optional arguments:
            // So far, the similar mechanism for Tangent is done somewhere else ?
            Tree sourceTreeAnnotation = callTree.getAnnotation("sourcetree");
            if (sourceTreeAnnotation != null && sourceTreeAnnotation.opCode() == ILLang.op_call) {
                fwdCallTree = ILUtils.copy(sourceTreeAnnotation);
            } else {
                fwdCallTree = ILUtils.copy(callTree);
            }
            // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
            args = ILUtils.getArguments(fwdCallTree).children();
            for (int i = args.length - 1; i >= 0; --i) {
                if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                    args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                }
            }
            fwdTree = fwdCallTree;
            if (lhs != null) {
                fwdTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(lhs), fwdTree);
            }
            if (TapEnv.associationByAddress()) {
                fwdTree = blockDifferentiator().turnAssociationByAddressPrimal(fwdTree, adEnv.curSymbolTable(),
                        adEnv.curFwdSymbolTable, null, true);
            }
            blockDifferentiator().addFuturePlainNodeFwd(fwdTree, adEnv.curInstruction().whereMask(), false,
                    refsRW.first, refsRW.second, null, null, true, true,
                    "In-context copy of primal call", false);

            Unit primalCalledUnit = adEnv.getPrimalCopyOfOrigUnit(calledUnit);
            // If the called routine has no primal copy, we place a CallArrow to the source called routine
            // Danger [19Mar18]: this is the 1st example of a CallArrow between 2 different CallGraph's...
            if (primalCalledUnit == null) {
                primalCalledUnit = calledUnit;
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO ORIG "+primalCalledUnit) ;
//                 } else {
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO PRIMAL COPY "+primalCalledUnit) ;
            }
            if (primalCalledUnit != null) {
                CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, primalCalledUnit);
            }
        }
    }

    /**
     * Builds the adjoint instructions for a function call. Builds the
     * instructions that must be inserted before the call in the fwd sweep, and
     * returns the TapList of them. Builds one "NewBlockGraphNode" in the adjoint
     * graph for each required adjoint instruction. These nodes will be sorted
     * later to optimize memory access. For a general example, consider the
     * following source call:
     * R[exp5] = FUNC(A[exp1], C[exp2], B[exp3], D[exp4]) ;
     * where C and B must be saved because they are the inputs of
     * "FUNC" and "FUNC" will be executed twice ("checkpointed"), and R, A, and
     * C must also be saved because their before-call value is "TBR" and we
     * cannot force the TBR mechanism inside the source of "FUNC". Also, even if
     * D need not be saved, its memory space is used, and therefore its index
     * must be saved. The fwd and reverse instructions will then be:
     * PUSH(R[exp5]) ;
     * PUSH(A[exp1]) ;
     * PUSH(C[exp2]) ;
     * PUSH(exp5) ;
     * PUSH(exp1) ;
     * PUSH(exp2) ;
     * PUSH(B[exp3]) ;
     * PUSH(exp3) ;
     * PUSH(exp4) ;
     * R[exp5] = FUNC(A[exp1], C[exp2], B[exp3], D[exp4]) ;
     * ---------------------------------------------------------
     * POP(i4) ;
     * POP(i3) ;
     * POP(B[i3]) ;
     * adStack_startRepeat()
     * POP(i2) ;    // i.e. old LOOK(i2) ;
     * POP(i1) ;    // i.e. old LOOK(i1) ;
     * POP(i5) ;    // i.e. old LOOK(i5) ;
     * POP(C[i2]) ; // i.e. old LOOK(C[i2]) ;
     * adStack_resetRepeat()
     * adStack_endRepeat()
     * "explicitation of some implicit-zero derivatives"
     * FUNC_B(A[i1], Abar[i1], C[i2], Cbar[i2], B[i3], Bbar[i3], D[i4], Rbar[i5]) ; => Add profiling element during turn (requires a call to profiling in then AND else)
     * "explicitation of some implicit-zero derivatives"
     * POP(i2) ;
     * POP(i1) ;
     * POP(i5) ;
     * POP(C[i2]) ;
     * POP(A[i1]) ;
     * POP(R[i5]) ;
     */
    private void buildCallAdjointNodes(Tree callTree, Tree resultTree,
                                       Unit calledUnit, ActivityPattern calledActivity, boolean mustDiffCall, boolean callIsLive,
                                       boolean mustSaveTBRBeforeInstr, boolean normalCheckpointedCall, boolean splitAdjointCall, boolean isJoint,
                                       SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
                                       BoolVector beforeActiv, BoolVector afterActiv) {
        Instruction curInstr = adEnv.curInstruction() ;
        if (calledActivity == null && calledUnit == adEnv.curUnit()) {
            calledActivity = adEnv.curActivity();
        }
        if (!adEnv.curUnitIsContext) {
            UnitDiffInfo calledDiffInfo = callGraphDifferentiator().getUnitDiffInfo(calledUnit);
            int calledDiffSort = normalCheckpointedCall ? DiffConstants.ADJOINT : DiffConstants.ADJOINT_BWD;
            Unit diffCalledUnit = adEnv.getDiffOfUnit(calledUnit, calledActivity, calledDiffSort);
            varRefDifferentiator().pushDiffContext(DiffConstants.INCALL);
            Tree adjCall = null;
            //Unit calledUnit = DataFlowAnalyzer.getCalledUnit(callTree, bwdSymbolTable);
            FunctionTypeSpec functionTypeSpec =
                    callTree.getAnnotation("functionTypeSpec");
            if (functionTypeSpec == null) {
                functionTypeSpec = calledUnit.functionTypeSpec();
            }
            CallArrow callArrow =
                    CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
            int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            int nDdZ = adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind);
            Tree[] args = ILUtils.getArguments(callTree).children();
            int nbArgs = args.length;
// This fix in case of mismatching nbArgs was harmful. Better not do it and be more careful later:
//             nbArgs = Math.min(nbArgs, calledUnit.functionTypeSpec().argumentsTypes.length) ;
            Tree arg;
            TapList argZonesTree;
            ZoneInfo zoneInfo;
            WrapperTypeSpec actualArgType, formalArgType;
            BoolVector zonesWrittenByThisCall = new BoolVector(nDZ);
            TapList[] paramsW = null;
            if (calledUnit.unitInOutW() != null && callArrow != null) {

                // TODO: Try merge these 2 almost identical calls:
                DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), zonesWrittenByThisCall,
                                                               null, args, true,
                                                               callTree, curInstr, callArrow,
                                                               SymbolTableConstants.ALLKIND,
                                                               null, null, false, false) ;
                paramsW = DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), null, null, null, true,
                                                                         callTree, curInstr, callArrow,
                                                                         SymbolTableConstants.ALLKIND,
                                                                         null, null, false, false) ;
            }
            //Compute Use(C_b) :
            BoolVector callUseB = new BoolVector(nDZ);
            BoolVector unitDiffLiveness = null;
            if (TapEnv.diffLivenessAnalyzer() == null) {
                unitDiffLiveness = calledUnit.unitInOutPossiblyR();
            } else if (calledActivity != null) {
                BoolVector diffLiveOnDiffPtr = TapEnv.diffLivenessAnalyzer().getDiffLivenessOnDiffPtr(calledActivity) ;
                diffLiveOnDiffPtr = calledUnit.unfocusFromKind(diffLiveOnDiffPtr, SymbolTableConstants.PTRKIND);
                unitDiffLiveness =
                    TapEnv.diffLivenessAnalyzer().getDiffLiveness(calledActivity).or(diffLiveOnDiffPtr) ;
            }
            if (unitDiffLiveness != null && callArrow != null) {
                DataFlowAnalyzer.translateCalleeDataToCallSite(unitDiffLiveness, callUseB, null, args, true,
                                                               callTree, curInstr, callArrow,
                                                               SymbolTableConstants.ALLKIND,
                                                               null, null, true, false) ;
            }

            // Compute Sbk, the params that must be saved because of TBR, and
            // Snp, the params that must be photographed for re-execution.
            // Normally retrieve these from the "TBR" annotation on the call,
            // but when TBR was deactivated, we must compute Sbk and Snp explicitly.
            TapPair completeCallTBRInfo = null;
            if (mustSaveTBRBeforeInstr) {
                completeCallTBRInfo = (TapPair) ActivityPattern.getAnnotationForActivityPattern(callTree, adEnv.curActivity(), "TBR");
            }
            TapList[] snpArgs, snpArgsOnDiffPtr, sbkArgs, sbkArgsOnDiffPtr;
            BoolVector snp, snpOnDiffPtr, sbk, sbkOnDiffPtr;
            // The "checkpointing sets" info may be empty in the following two cases:
            //  -- When this call is "adjoint-dead", because Func is not called in the forward sweep
            //  -- When this call is not active, because Func_B is not called in the reverse sweep
            if (completeCallTBRInfo == null) {
                snpArgs = new TapList[nbArgs + 1];
                snpArgsOnDiffPtr = new TapList[nbArgs + 1];
                sbkArgs = new TapList[nbArgs + 1];
                sbkArgsOnDiffPtr = new TapList[nbArgs + 1];
                for (int i = nbArgs - 1; i >= 0; i--) {
                    snpArgs[i] = null;
                    snpArgsOnDiffPtr[i] = null;
                    sbkArgs[i] = null;
                    sbkArgsOnDiffPtr[i] = null;
                }
                snp = new BoolVector(nDZ);
                snpOnDiffPtr = new BoolVector(nDZ);
                sbk = new BoolVector(nDZ);
                sbkOnDiffPtr = new BoolVector(nDZ);
            } else if (normalCheckpointedCall) {
                // For now, a normal checkpointed call calls the original primal function F.
                // We know this may be wrong if the original function modifies pointers,
                // and Tapenade issues a warning message.
                // This should be fixed e.g by defining and calling a F_ADV nearly-copy of F.
                // Until this is done, this call to F cannot change any differentiated pointer,
                // so we don't use the "OnDiffPtr" infos, and use empty info instead.
                TapList[][] tbrOnArgs = (TapList[][]) completeCallTBRInfo.first;
                BoolVector[] tbrOnGlobs = (BoolVector[]) completeCallTBRInfo.second;
                snpArgs = tbrOnArgs[0];
                snp = tbrOnGlobs[0];
                sbkArgs = tbrOnArgs[2];
                sbk = tbrOnGlobs[2];
                snpArgsOnDiffPtr = new TapList[nbArgs + 1];
                snpOnDiffPtr = new BoolVector(nDZ);
                sbkArgsOnDiffPtr = new TapList[nbArgs + 1];
                sbkOnDiffPtr = new BoolVector(nDZ);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    snpArgsOnDiffPtr[i] = null;
                    sbkArgsOnDiffPtr[i] = null;
                }
            } else { //[llh 30/5/2016] now use also the OnDiffPtr pieces of TBR:
                TapList[][] tbrOnArgs = (TapList[][]) completeCallTBRInfo.first;
                BoolVector[] tbrOnGlobs = (BoolVector[]) completeCallTBRInfo.second;
                snpArgs = tbrOnArgs[0];
                snpArgsOnDiffPtr = tbrOnArgs[1];
                snp = tbrOnGlobs[0];
                snpOnDiffPtr = DataFlowAnalyzer.changeKind(tbrOnGlobs[1], adEnv.curPtrVectorLen, SymbolTableConstants.PTRKIND, adEnv.curVectorLen, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
                sbkArgs = tbrOnArgs[2];
                sbkArgsOnDiffPtr = tbrOnArgs[3];
                sbk = tbrOnGlobs[2];
                sbkOnDiffPtr = DataFlowAnalyzer.changeKind(tbrOnGlobs[3], adEnv.curPtrVectorLen, SymbolTableConstants.PTRKIND, adEnv.curVectorLen, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
            }
            if (adEnv.traceCurBlock != 0) {
                if (splitAdjointCall) {
                    TapEnv.printlnOnTrace("          no SNP sets because split adjoint mode") ;
                } else {
                    TapEnv.printOnTrace("          SNP sets: (");
                    for (int i = 0; i <= nbArgs; ++i) {
                        TapEnv.printOnTrace(snpArgs[i] + (i==nbArgs ? "" : (i==nbArgs-1 ? "=>" : ",")));
                    }
                    TapEnv.printOnTrace(") Ptr:(");
                    for (int i = 0; i <= nbArgs; ++i) {
                        TapEnv.printOnTrace(snpArgsOnDiffPtr[i] + (i == nbArgs ? "" : i == nbArgs - 1 ? "=>" : ","));
                    }
                    TapEnv.printlnOnTrace(") GLOBS:" + DataFlowAnalyzer.infoToStringWithDiffPtr(snp, adEnv.curVectorLen, snpOnDiffPtr, adEnv.curPtrVectorLen, adEnv.curUnit(), adEnv.curSymbolTable()));
                }
                TapEnv.printOnTrace("          SBK sets: (");
                for (int i = 0; i <= nbArgs; ++i) {
                    TapEnv.printOnTrace(sbkArgs[i] + (i == nbArgs ? "" : i == nbArgs - 1 ? "=>" : ","));
                }
                TapEnv.printOnTrace(") Ptr:(");
                for (int i = 0; i <= nbArgs; ++i) {
                    TapEnv.printOnTrace(sbkArgsOnDiffPtr[i] + (i == nbArgs ? "" : i == nbArgs - 1 ? "=>" : ","));
                }
                TapEnv.printlnOnTrace(") GLOBS:" + DataFlowAnalyzer.infoToStringWithDiffPtr(sbk, adEnv.curVectorLen, sbkOnDiffPtr, adEnv.curPtrVectorLen, adEnv.curUnit(), adEnv.curSymbolTable()));
            }
            // Patch: try replace inaccessible "allocate" zones with a "proxy". i.e. some zone accessible here
            // and that is equivalent, i.e. accesses the same memory. This is needed when pointer analysis
            // traces destinations back to some "allocate" command.
            TapList<TapPair<ZoneInfo, ZoneInfo>> proxyReplacements = null ;
            for (int i=nDZ-1 ; i>=2 ; --i) {  // Don't examine NULL and unknownDest.
              zoneInfo = adEnv.curUnit().publicSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
              if (zoneInfo!=null) {
                int pi = zoneInfo.ptrZoneNb ;
                if (zoneInfo.comesFromAllocate()
                    && (sbk.get(i) || snp.get(i)
                        || (pi>0
                            && (sbkOnDiffPtr.get(pi) || snpOnDiffPtr.get(pi))))) {
                    // The zone of an "allocate" is inaccessible. Try to replace it with an accessible proxy:
                    ZoneInfo proxyZoneInfo = adEnv.curSymbolTable().findProxyForHiddenZone(zoneInfo, curInstr, true) ;
                    if (proxyZoneInfo!=null) {
                        proxyReplacements = new TapList<>(new TapPair<>(zoneInfo, proxyZoneInfo), proxyReplacements) ;
//                         System.out.println("Replace hidden "+zoneInfo+" with visible "+proxyZoneInfo) ;
//                     } else {
//                         System.out.println("Could not find a proxy ZoneInfo to push pop hidden "+zoneInfo) ;
                    }
                }
              }
            }
            // Actually perform the scheduled replacements:
            if (proxyReplacements!=null) {
                int i, pi, j, pj ;
                ZoneInfo proxyZoneInfo ;
                snp = snp.copy() ;
                sbk = sbk.copy() ;
                snpOnDiffPtr = snpOnDiffPtr.copy() ;
                sbkOnDiffPtr = sbkOnDiffPtr.copy() ;
                while (proxyReplacements!=null) {
                    zoneInfo = proxyReplacements.head.first ;
                    i = zoneInfo.zoneNb ;
                    pi = zoneInfo.ptrZoneNb ;
                    proxyZoneInfo = proxyReplacements.head.second ;
                    j = proxyZoneInfo.zoneNb ;
                    pj = proxyZoneInfo.ptrZoneNb ;
                    if (snp.get(i)) {snp.set(i, false); snp.set(j, true);}
                    if (sbk.get(i)) {sbk.set(i, false); sbk.set(j, true);}
                    if (pi>0 && snpOnDiffPtr.get(pi)) {snpOnDiffPtr.set(pi, false); snpOnDiffPtr.set(pj, true);}
                    if (pi>0 && sbkOnDiffPtr.get(pi)) {sbkOnDiffPtr.set(pi, false); sbkOnDiffPtr.set(pj, true);}
                    proxyReplacements = proxyReplacements.tail ;
                }
            }
            // Complain if hidden zones must be saved:
            for (int i = nDZ - 1; i >= 2; --i) {  // Don't examine NULL and unknownDest.
                zoneInfo = adEnv.curUnit().publicSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zoneInfo!=null) {
                    int pi = zoneInfo.ptrZoneNb ;
                    if (sbk.get(i) || snp.get(i) || (pi>0 && (sbkOnDiffPtr.get(pi) || snpOnDiffPtr.get(pi)))) {
                        if (zoneInfo.zoneNb == adEnv.srcCallGraph().zoneNbOfAllIOStreams) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(AD07) Data-Flow recovery (TBR) on this call to " + calledUnit.name() + " needs to save the I-O state");
                        } else if (adEnv.srcCallGraph().isAMessagePassingChannelZone(zoneInfo.zoneNb)) {
                            //[llh] Maybe we should complain here about checkpointing on MPI calls?
                        } else if (zoneInfo.comesFromAllocate()) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(AD14) Checkpointing this call to " + calledUnit.name() + " needs to save hidden allocated chunk: " + zoneInfo.description);
                            adEnv.addInHiddenPushPopVariables(zoneInfo);
                        } else if (zoneInfo.isHidden) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(AD14) Checkpointing this call to " + calledUnit.name() + " needs to save hidden variable: " + zoneInfo.description);
                            adEnv.addInHiddenPushPopVariables(zoneInfo);
                        }
                    }
                }
            }
            // Compute Sbk\Snp ; Sbk&Snp ; Snp\Sbk (except if splitAdjointCall)
            TapList[] interArgs = new TapList[nbArgs + 1];
            TapList[] interArgsOnDiffPtr = new TapList[nbArgs + 1];
            BoolVector inter = sbk.copy();
            BoolVector interOnDiffPtr = sbkOnDiffPtr.copy();
            if (splitAdjointCall) {
                inter.setFalse() ;
                interOnDiffPtr.setFalse() ;
            } else {
              inter.cumulAnd(snp);
              interOnDiffPtr.cumulAnd(snpOnDiffPtr);
              // Pointers should not go in "inter" because this may lead to a wrong order, POP'ping pointer AFTER POP'ping pointer dest (cf set07:v461)
              removeForPointers(inter, interOnDiffPtr);
              sbk.cumulAnd(inter.not());
              sbkOnDiffPtr.cumulAnd(interOnDiffPtr.not());
              snp.cumulAnd(inter.not());
              snpOnDiffPtr.cumulAnd(interOnDiffPtr.not());
              for (int i = nbArgs; i >= 0; i--) {
                if (adEnv.traceCurBlock != 0) {
                    TapEnv.printlnOnTrace("for " + (i == nbArgs ? "result" : "arg" + (i + 1)) + "? sbk:" + sbkArgs[i] + " inter:" + interArgs[i] + " snp:" + snpArgs[i]);
                }
                interArgs[i] = TapList.copyTree(sbkArgs[i]);
                interArgsOnDiffPtr[i] = TapList.copyTree(sbkArgsOnDiffPtr[i]);
                interArgs[i] = TapList.cumulWithOper(interArgs[i], snpArgs[i], SymbolTableConstants.CUMUL_AND);
                interArgsOnDiffPtr[i] = TapList.cumulWithOper(interArgsOnDiffPtr[i], snpArgsOnDiffPtr[i], SymbolTableConstants.CUMUL_AND);
                // Pointers should not go in "inter":
                removeForPointers(interArgs[i], interArgsOnDiffPtr[i]);
                if (sbkArgs[i] != null) {
                    sbkArgs[i] = TapList.cumulWithOper(sbkArgs[i], interArgs[i], SymbolTableConstants.CUMUL_MINUS);
                }
                if (sbkArgsOnDiffPtr[i] != null) {
                    sbkArgsOnDiffPtr[i] = TapList.cumulWithOper(sbkArgsOnDiffPtr[i], interArgsOnDiffPtr[i], SymbolTableConstants.CUMUL_MINUS);
                }
                if (snpArgs[i] != null) {
                    snpArgs[i] = TapList.cumulWithOper(snpArgs[i], interArgs[i], SymbolTableConstants.CUMUL_MINUS);
                }
                if (snpArgsOnDiffPtr[i] != null) {
                    snpArgsOnDiffPtr[i] = TapList.cumulWithOper(snpArgsOnDiffPtr[i], interArgsOnDiffPtr[i], SymbolTableConstants.CUMUL_MINUS);
                }
                if (adEnv.traceCurBlock != 0) {
                    TapEnv.printlnOnTrace("for " + (i == nbArgs ? "result" : "arg" + (i + 1)) + "> sbk:" + sbkArgs[i] + " inter:" + interArgs[i] + " snp:" + snpArgs[i]);
                }
              }
            }
            // TODO: check the above mechanism on y = f(y) with result in Sbk
            // and arg1 in Snp: does it find that y goes to Sbk & Snp ??
            TapList<TapPair<RefDescriptor, RefDescriptor>> headSaveBeforeCall = new TapList<>(null, null);
            TapList<TapPair<RefDescriptor, RefDescriptor>> headRestoreBeforeAdjoint = new TapList<>(null, null);
            TapList<TapPair<RefDescriptor, RefDescriptor>> headLookBeforeAdjoint = new TapList<>(null, null);
            TapList<TapPair<RefDescriptor, RefDescriptor>> headRestoreAfterAdjoint = new TapList<>(null, null);
            TapList<TapPair<RefDescriptor, RefDescriptor>> toTailSaveBeforeCall = new TapList<>(null, headSaveBeforeCall);
            TapList<TapPair<RefDescriptor, RefDescriptor>> toTailRestoreBeforeAdjoint = new TapList<>(null, headRestoreBeforeAdjoint);
            TapList<TapPair<RefDescriptor, RefDescriptor>> toTailLookBeforeAdjoint = new TapList<>(null, headLookBeforeAdjoint);
            TapList<TapPair<RefDescriptor, RefDescriptor>> toTailRestoreAfterAdjoint = new TapList<>(null, headRestoreAfterAdjoint);
            TapList<TapPair<Tree, NewSymbolHolder>> toSpecialSubExpressions = new TapList<>(null, null);
            SplitForSave[] argsSplitForSave = new SplitForSave[nbArgs + 1];
            TapPair<Tree, NewSymbolHolder> splitIndex;
            Tree indexExpr;
            NewSymbolHolder indexSymbolHolder;
            RefDescriptor refDescriptorSave, refDescriptorRestore;
            TapIntList toNextIndexRank = new TapIntList(0, null);
            for (int i = nbArgs; i >= 0; i--) {
                argsSplitForSave[i] = new SplitForSave(adEnv.curSymbolTable(), toNextIndexRank,
                        toSpecialSubExpressions, adEnv.toIndexSymbolHolders,
                        zonesWrittenByThisCall, blockDifferentiator());
            }

            // Collect RefDescriptor's to be saved for Sbk \ Snp:
            collectRefDescriptorsToSave(sbk, sbkArgs,
                    sbkOnDiffPtr, sbkArgsOnDiffPtr,
                    toTailSaveBeforeCall, null, toTailRestoreAfterAdjoint, false,
                    fwdSymbolTable, bwdSymbolTable, calledUnit, functionTypeSpec, args, nbArgs, resultTree, argsSplitForSave);

            // Collect RefDescriptor's to be saved for Sbk & Snp:
            collectRefDescriptorsToSave(inter, interArgs,
                    interOnDiffPtr, interArgsOnDiffPtr,
                    toTailSaveBeforeCall, toTailLookBeforeAdjoint, toTailRestoreAfterAdjoint, true,
                    fwdSymbolTable, bwdSymbolTable, calledUnit, functionTypeSpec, args, nbArgs, resultTree, argsSplitForSave);

            // generate the push/pop's for the indexes inside all arguments in Sbk :
            while (toSpecialSubExpressions.tail != null) {
                splitIndex = toSpecialSubExpressions.tail.head;
                indexExpr = splitIndex.first;
                actualArgType = adEnv.curSymbolTable().typeOf(indexExpr);
                formalArgType = adEnv.integerTypeSpec;
                refDescriptorSave =
                    new RefDescriptor(indexExpr, null, actualArgType, formalArgType, null,
                                adEnv.curSymbolTable(), curFwdDiffUnit().privateSymbolTable(), null,
                                null, false, null, curDiffUnit());
                refDescriptorSave.setHintArrayTreeForCallSize(indexExpr);
                toTailSaveBeforeCall.tail =
                        toTailSaveBeforeCall.tail.placdl(new TapPair<>(refDescriptorSave, null));
                indexSymbolHolder = splitIndex.second;
                Tree restoredIndexExpr;
                if (indexSymbolHolder == null) {
                    restoredIndexExpr = ILUtils.copy(indexExpr);
                } else {
                    restoredIndexExpr = indexSymbolHolder.makeNewRef(bwdSymbolTable);
                    indexSymbolHolder.declarationLevelMustInclude(bwdSymbolTable);
                }
                refDescriptorRestore =
                    new RefDescriptor(restoredIndexExpr, null, actualArgType, formalArgType, null,
                                adEnv.curSymbolTable(), curDiffUnit().privateSymbolTable(), null,
                                null, false, null, curDiffUnit());
                refDescriptorRestore.setHintArrayTreeForCallSize(restoredIndexExpr);
                toTailLookBeforeAdjoint.tail =
                        toTailLookBeforeAdjoint.tail.placdl(new TapPair<>(refDescriptorRestore, null));
                toTailRestoreAfterAdjoint.tail =
                        toTailRestoreAfterAdjoint.tail.placdl(new TapPair<>(refDescriptorRestore, null));
                blockDifferentiator().prepareRestoreOperations(refDescriptorSave, refDescriptorRestore, 0, fwdSymbolTable, bwdSymbolTable);
                toSpecialSubExpressions.tail = toSpecialSubExpressions.tail.tail;
            }

            // Collect RefDescriptor's to be saved for Snp \ Sbk:
            collectRefDescriptorsToSave(snp, snpArgs,
                    snpOnDiffPtr, snpArgsOnDiffPtr,
                    toTailSaveBeforeCall, toTailRestoreBeforeAdjoint, null, true,
                    fwdSymbolTable, bwdSymbolTable, calledUnit, functionTypeSpec, args, nbArgs, resultTree, argsSplitForSave);

            // generate the push/pop's for the indexes inside all remaining arguments :
            while (toSpecialSubExpressions.tail != null) {
                splitIndex = toSpecialSubExpressions.tail.head;
                indexExpr = splitIndex.first;
                actualArgType = adEnv.curSymbolTable().typeOf(indexExpr);
                formalArgType = adEnv.integerTypeSpec;
                refDescriptorSave =
                    new RefDescriptor(indexExpr, null, actualArgType, formalArgType, null,
                                adEnv.curSymbolTable(), curFwdDiffUnit().privateSymbolTable(), null,
                                null, false, null, curFwdDiffUnit());
                refDescriptorSave.setHintArrayTreeForCallSize(indexExpr);
                toTailSaveBeforeCall.tail =
                        toTailSaveBeforeCall.tail.placdl(new TapPair<>(refDescriptorSave, null));
                indexSymbolHolder = splitIndex.second;
                Tree restoredIndexExpr;
                if (indexSymbolHolder == null) {
                    restoredIndexExpr = ILUtils.copy(indexExpr);
                } else {
                    restoredIndexExpr = indexSymbolHolder.makeNewRef(bwdSymbolTable);
                    indexSymbolHolder.declarationLevelMustInclude(bwdSymbolTable);
                }
                refDescriptorRestore =
                    new RefDescriptor(restoredIndexExpr, null, actualArgType, formalArgType, null,
                                adEnv.curSymbolTable(), curDiffUnit().privateSymbolTable(), null,
                                null, false, null, curDiffUnit());
                refDescriptorRestore.setHintArrayTreeForCallSize(restoredIndexExpr);
                toTailRestoreBeforeAdjoint.tail =
                        toTailRestoreBeforeAdjoint.tail.placdl(new TapPair<>(refDescriptorRestore, null));
                blockDifferentiator().prepareRestoreOperations(refDescriptorSave, refDescriptorRestore, 0, fwdSymbolTable, bwdSymbolTable);
                toSpecialSubExpressions.tail = toSpecialSubExpressions.tail.tail;
            }

            TapList<Tree> readRefs = null;
            TapList<Tree> writtenRefs = null;
            TapList<Tree> readAdjRefs = null;
            TapList<Tree> writtenAdjRefs = null;
            TapList<Tree> treesToDiffInitUS = null;
            TapList<TapTriplet<Tree, WrapperTypeSpec, Tree>> treesToDiffInitDS = null;
            TapList<ZoneInfo> zonesToDiffInitDS = null;

            if (mustDiffCall) {
                BoolVector actualBeforeActive = null;
                TapList[] actualArgsBeforeActive = null;
                if (beforeActiv != null && !adEnv.activeRootCalledFromContext) {
                    actualBeforeActive = beforeActiv.copy();
                    //Special call, only to split beforeActiv into one part on the arguments, and one part on the globals:
                    actualArgsBeforeActive =
                            DataFlowAnalyzer.propagateCallSiteDataToActualArgs(actualBeforeActive, null, args, null,
                                                                               adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind),
                                                                               adEnv.toActiveTmpZones.tail, curInstr, callArrow, true, adEnv.diffKind);
                }

                boolean[] formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(calledActivity);
                TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionInfos = calledActivity.varFunctionADActivities();
                TapList[] actualArgsCallActive;
                TapList[] actualArgsExitActive;
                TapList formalActiveDS, formalActiveUS;
                BoolVector formalExitActive = new BoolVector(nDdZ);

                if (calledUnit.isIntrinsic()) {
                    actualArgsCallActive = new TapList[1+nbArgs];
                    actualArgsExitActive = new TapList[1+nbArgs];
                    actualArgsCallActive[0] = new TapList<>(Boolean.FALSE, null);
                    actualArgsExitActive[0] = new TapList<>(Boolean.TRUE, null);
                    for (int i=nbArgs ; i>0 ; --i) {
                        actualArgsCallActive[i] = new TapList<>(Boolean.TRUE, null);
                        actualArgsExitActive[i] = new TapList<>(Boolean.TRUE, null);
                    }
                } else {
                    actualArgsCallActive =
                        DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.callActivity(), null,
                                null, null, true,
                                callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
                        // was DataFlowAnalyzer.propagateDataToCaller(calledActivity.callActivity(),
                        //            null, null, args.length, null, null, formalResultEntryActive,
                        //            curInstr, callArrow, true, true, adEnv.diffKind);
                    actualArgsExitActive =
                        DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.exitActivity(), formalExitActive,
                                null, null, false,
                                callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
                    // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                    // Especially for any PASS-BY-VALUE formal argument X, Tapenade's model is to introduce a diff argument Xb0
                    // and a local diff argument Xb, as soon as X is active upon entry OR exit, and to add a final statement
                    // *Xb0 = *Xb0 + Xb; In that case, we must tell the caller that X is "actualArgsExitActive", to make sure
                    // that the value passed to Xb0 is initialized to zero if the actual arg is passive.
                    for (int i=1 ; i<actualArgsCallActive.length && i<actualArgsExitActive.length ; ++i) {
                        if (calledUnit.takesArgumentByValue(i, adEnv.curUnit().language())) {
                            TapList argCallActive = TapList.copyTreeNoPointed(actualArgsCallActive[i]);
                            actualArgsExitActive[i] = TapList.cumulWithOper(actualArgsExitActive[i], argCallActive, SymbolTableConstants.CUMUL_OR);
                        }
                    }
                    // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                }

// TapEnv.printlnOnTrace("actualArgs(Call->Exit):formalArgsActive of "+calledUnit+":") ;
// for (int i=1 ; i<=nbArgs ; ++i)
//   TapEnv.printlnOnTrace("       "+i+": ("+actualArgsCallActive[i]+"->"+actualArgsExitActive[i]+") : "+formalArgsActivity[i-1]) ;
// TapEnv.printlnOnTrace("  result: ("+actualArgsCallActive[0]+"->"+actualArgsExitActive[0]+") : "+formalArgsActivity[nbArgs]) ;

                Tree argTree, argTreeBis;
                String varName = null;
                Tree ref;
                for (int i = nDZ - 1; i >= 0; i--) {
                    zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                    varName = (zoneInfo!=null && !zoneInfo.isHidden && zoneInfo.accessTree!=null ? zoneInfo.bestVarName() : null) ;
                    if (varName != null) {
                        ref = ILUtils.build(ILLang.op_ident, varName);
                        if (callUseB.get(i)) {
                            readRefs = new TapList<>(ref, readRefs);
                        }
                        if (zonesWrittenByThisCall.get(i)) {
                            writtenRefs = new TapList<>(ref, writtenRefs);
                        }
                    }
                }
                TapList<Tree> hdNewArgs = new TapList<>(null, null);
                Object annot = callTree.getAnnotation("sourcetree");
                TapList<Tree> tlNewArgs = hdNewArgs;
                boolean mustBeNameEq = false;
                boolean bewareNameEq =
                        annot != null && ((Tree) annot).opCode() == ILLang.op_call;
                // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                boolean[] diffArgsByValueNeedOverwrite = null;
                diffArgsByValueNeedOverwrite = calledDiffInfo.getUnitDiffArgsByValueNeedOverwriteInfoS(calledActivity);
                // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
// TapEnv.printlnOnTrace("buildCallAdjointNodes of "+callTree) ;
                for (int i = 0; i <= nbArgs; ++i) {
                    // Note: unfortunate old choice that represents "result" by i==nbArgs, whereas it is now more often i==0...
                    arg = (i==nbArgs ? resultTree : args[i]) ;
                    TapList formalArgWritten = (i==nbArgs ? paramsW[0] : paramsW[i+1]);
                    if (argsSplitForSave[i].tree == null) {
                        argTree = ILUtils.copy(arg);
                        argTreeBis = ILUtils.copy(arg);
                    } else if (TapEnv.relatedLanguageIsFortran9x()) {
                        argTree = argsSplitForSave[i].newSplitCopy(bwdSymbolTable);
                        argTreeBis = argsSplitForSave[i].newSplitCopy(bwdSymbolTable);
                    } else {
                        argTree = argsSplitForSave[i].newSplitNoColonCopy(bwdSymbolTable);
                        argTreeBis = argsSplitForSave[i].newSplitNoColonCopy(bwdSymbolTable);
                    }
                    if (bewareNameEq && i < nbArgs &&
                            (ILUtils.isNullOrNone(arg)
                                    || arg.opCode() == ILLang.op_nameEq
                                    || argTree.getAnnotation("sourcetree") != null)) {
                        // As soon as one arg is none or nameEq, all next args must be created nameEq:
                        mustBeNameEq = true;
                    }
                    // First, copy the original argument... :
                    Tree argTreeCopy = null;
                    if (i != nbArgs && argTree.opCode() != ILLang.op_label) {
                        argTreeCopy = argTree;
                        if (adEnv.curSymbolTable().identIsAFunction(argTree)) {
                            argTreeCopy.setAnnotation("isFunctionName", Boolean.TRUE);
                        }
                        readRefs = ILUtils.usedVarsInExp(argTree, readRefs, false);
                    }
                    if (!ILUtils.isNullOrNone(argTreeCopy)) {
                        assert argTreeCopy != null;
                        if (mustBeNameEq && argTreeCopy.opCode() != ILLang.op_nameEq) {
                            Tree argName =
                                    getNewParamName(i == nbArgs ? -1 : i, calledUnit, false, curDiffVarSort(), null);
                            argTreeCopy =
                                    ILUtils.build(ILLang.op_nameEq, argName, argTreeCopy);
                        }
                        tlNewArgs = tlNewArgs.placdl(argTreeCopy);
                    }
                    // ... second, append a differentiated argument if necessary (i.e. if active) :
                    Tree argTreeCopyDiff = null;
                    if (arg != null && adEnv.curSymbolTable().identIsAFunction(arg)) {
                        if (!TapEnv.doActivity() || varFunctionInfos != null && i < nbArgs && varFunctionInfos[i] != null) {
                            // if this argument is a function name which is active:
                            TapList<ActivityPattern> functionActivities =
                                    ((FunctionDecl) adEnv.curSymbolTable().getSymbolDecl(ILUtils.getIdentString(arg))).unit().activityPatterns;
                            // A function passed as argument is allowed only one (not connected) activity pattern:
                            argTreeCopyDiff = varRefDifferentiator().diffSymbolTree(
                                    functionActivities != null ? functionActivities.head : null,
                                    arg, bwdSymbolTable, false, true, false,
                                    null, false, curDiffVarSort(), DiffConstants.ADJOINT, DiffConstants.VAR, null);
                            argTreeCopyDiff.setAnnotation("isFunctionName", Boolean.TRUE);
                            if (!ILUtils.isNullOrNone(argTreeCopyDiff)) {
                                if (mustBeNameEq) {
                                    Tree argName = getNewParamName(i == nbArgs ? -1 : i, calledUnit,
                                                                   true, curDiffVarSort(), diffCalledUnit);
                                    argTreeCopyDiff =
                                        ILUtils.build(ILLang.op_nameEq, argName, argTreeCopyDiff);
                                }
                                tlNewArgs = tlNewArgs.placdl(argTreeCopyDiff);
                            }
                        }
                    } else if ((i < formalArgsActivity.length - 1 && formalArgsActivity[i]
                            ||
                            i == nbArgs && formalArgsActivity[formalArgsActivity.length - 1])
                            && arg != null && arg.opCode() != ILLang.op_none) {
                        //If this argument is formal-active and actual-PRESENT (case of optional args):
                        Tree argTreeInitBis = null;
                        argZonesTree = adEnv.curSymbolTable().treeOfZonesOfValue(arg, null, curInstr, null);
                        argZonesTree = TapList.copyTree(argZonesTree);
                        DataFlowAnalyzer.includePointedElementsInTree(argZonesTree,
                                null, null, true, adEnv.curSymbolTable(), curInstr, false, true);
                        formalArgType = (i == nbArgs ? functionTypeSpec.returnType : functionTypeSpec.argumentsTypes[i]);
                        if (argTreeBis.opCode() != ILLang.op_address) {
                            if (!ILUtils.isAWritableVarRef(argTreeBis, adEnv.curSymbolTable())) {
                                //When the formal param is active, then the called diff subroutine
                                // is going to write into it. If the actual arg is not a reference, this
                                // will cause segmentation faults ! => pass a temporary.
                                argTreeBis =
                                    adEnv.curBlock.buildTemporaryVariable("dummydiff", arg, bwdSymbolTable,
                                                               null, false, null, false, functionTypeSpec, (i+1), arg) ;
                            } else if (!ILUtils.isAWritableIdentVarRef(argTreeBis, adEnv.curSymbolTable())
                                    && !ADActivityAnalyzer.isAnnotatedActive(adEnv.curActivity(), arg, adEnv.curSymbolTable())
                                    && !ReqExplicit.isAnnotatedPointerActive(adEnv.curActivity(), arg)) {
                                //When the formal param is active, but the actual param is passive
                                // and is a complex reference expression, we must pass a dummy temp object of
                                // the formal arg's expected type, differentiated and initialized to 0.0 (cf set03/lh094)
                                ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
                                argTreeBis =
                                    adEnv.curBlock.buildTemporaryVariable("dummyzerodiff", arg, bwdSymbolTable,
                                                               toNSH, false, null, false, functionTypeSpec, (i+1), arg) ;
                                NewSymbolHolder dummySymbolHolder = toNSH.obj() ;
                                argTreeInitBis = ILUtils.copy(argTreeBis) ;
                                treesToDiffInitDS =
                                        new TapList<>(new TapTriplet<>(argTreeInitBis, dummySymbolHolder.newVariableDecl.type(), arg),
                                                treesToDiffInitDS);
                            }
                        }
                        writtenAdjRefs = new TapList<>(argTree, writtenAdjRefs);
                        readAdjRefs = new TapList<>(argTree, readAdjRefs);

                        for (adEnv.iReplic=0 ; adEnv.iReplic<TapEnv.diffReplica() ; ++adEnv.iReplic) {
                            argTreeCopyDiff =
                                varRefDifferentiator().diffVarRefNoCopy(adEnv.curActivity(),
                                        ILUtils.copy(argTreeBis), bwdSymbolTable, false, arg);
                            // Error message if adjoint argument has two nested indirections: cannot be written!
                            if (argTreeCopyDiff != null && formalArgType.isArray()
                                && arg.opCode() == ILLang.op_arrayAccess
                                && ILUtils.hasTwoLevelIndex(arg.down(2), adEnv.curSymbolTable())) {
                                TapEnv.fileWarning(TapEnv.MSG_ALWAYS, arg, "(AD23) Adjoint argument with two-level indexing. Please modify source or patch diff code.") ;
                            }
                            // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                            if (argTreeCopyDiff != null && adEnv.curUnit().isC()) {
                              boolean mustPassAddress;
                              if (i < nbArgs) {
                                //This is an argument:
                                mustPassAddress = diffArgsByValueNeedOverwrite != null
                                        && diffArgsByValueNeedOverwrite[i + 1]
                                        && calledUnit.takesArgumentByValue(i + 1, adEnv.curUnit().language());
// TapEnv.printlnOnTrace("    -- formalArg#"+(i+1)+" diff must be updated: "+calledUnit.takesArgumentByValue(i+1, curUnit().language())+"&"+(diffArgsByValueNeedOverwrite!=null && diffArgsByValueNeedOverwrite[i+1])+"  ==>mustPassAddress:"+mustPassAddress) ;
                              } else {
                                //This is the result:
                                // When called Unit is Fortran, it expects a reference passed from C,
                                // except for F2003 because we add modifier "VALUE" on the adjoint of the result.
                                mustPassAddress = calledUnit.isFortran() && !calledUnit.isFortran2003();
// TapEnv.printlnOnTrace("    -- Result: calledUnit is F but not 2003:"+(calledUnit.isFortran()&&!calledUnit.isFortran2003())+"  ==>mustPassAddress:"+mustPassAddress) ;
                              }
                              if (mustPassAddress) {
                                // Make sure we pass the address of argTreeCopyDiff:
                                argTreeCopyDiff = ILUtils.addAddressOf(argTreeCopyDiff);
                              }
                            }
                            // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
                            if (!ILUtils.isNullOrNone(argTreeCopyDiff)) {
                                if (mustBeNameEq) {
                                    Tree argName = getNewParamName(i == nbArgs ? -1 : i, calledUnit,
                                                                   true, curDiffVarSort(), diffCalledUnit);
                                    argTreeCopyDiff =
                                        ILUtils.build(ILLang.op_nameEq, argName, argTreeCopyDiff);
                                }
                                tlNewArgs = tlNewArgs.placdl(argTreeCopyDiff);
                            }
                        }
                        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                        if ((TapEnv.spareDiffReinitializations() || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) && ILUtils.isAWritableVarRef(arg, adEnv.curSymbolTable())) {
                            // When the formal argument is active at the downstream end
                            // of the called subroutine, but the actual argument "argTree"
                            // is not active just downstream the call, we must explicit
                            // the differential of "argTree" to zero just before the call
                            // to the diff subroutine, since the diff variable may
                            // be used in the call:
                            formalActiveDS = (i==nbArgs ? actualArgsExitActive[0] : actualArgsExitActive[1+i]) ;
                            if (argTreeInitBis == null) {
                                // if argTreeInitBis!=null, its initialization is already done in treesToDiffInitDS.
                                zonesToDiffInitDS = collectZonesFormalActiveAndActualPassive(argZonesTree, adEnv.toActiveTmpZones.tail,
                                        formalActiveDS, afterActiv,
                                        calledUnit, zonesToDiffInitDS);
                                // Also take care of initializing diff of tmp arguments introduced by splitting (F90:lha41):
                                TapIntList listZones = ZoneInfo.listAllZones(argZonesTree, false);
                                if (listZones != null && i < nbArgs
                                        && TapIntList.contains(adEnv.toActiveTmpZones.tail, listZones.head)
                                        && TapList.oneTrue(formalActiveDS)) {
                                    treesToDiffInitDS =
                                            new TapList<>(new TapTriplet<>(arg, formalArgType, arg),
                                                    treesToDiffInitDS);
                                }
                            }
                            // Conversely when (the upstream actual argument "argTree"
                            // is active OR we are arriving into a mode with explicit
                            // zeroing of passive vars, cf F77:lha46), and at the same time
                            // the formal argument is not active at the upstream
                            // end of the called subroutine and is active
                            // at the downstream end, we must explicit the
                            // differential of the actual argument to zero after the
                            // exit of the reverse diff subroutine, since it may be used
                            // upstream the diff subroutine call. (cf F77:lha15)
                            // ATTENTION THIS CODE IS FRAGILE: It is not consistent with
                            // what is done in the tangent mode. In tangent mode, we
                            // nullify the 8th argument to make F90:lh90 work correctly !!

                            //[llh 12/10/2015] cf bug Gugala F90:v500. Don't try to diff-init anything if the argument
                            // is not modified by the called procedure.
                            if (TapList.oneTrue(formalArgWritten)) {
                                formalActiveUS = (i==nbArgs ? actualArgsCallActive[0] : actualArgsCallActive[1+i]) ;
                                TapList actualArgBeforeActive = ((i==nbArgs || actualArgsBeforeActive==null) ? null : actualArgsBeforeActive[i]) ;
// TapEnv.printlnOnTrace("Collect (ADJ) DiffInit after call "+calledUnit.name+", arg"+i+"(+1):"+arg+" zonesTree:"+argZonesTree+" formalActiveDS:"+formalActiveDS+" formalActiveUS:"+formalActiveUS+" actualActiveUS:"+actualArgsBeforeActive+" <=="+treesToDiffInitUS) ;
                                WrapperTypeSpec formalType = i < nbArgs ? formalArgType : null;
                                treesToDiffInitUS = collectTreesToDiffInitAfterDiffCall(arg,
                                        isAccessedThroughPointerOrIndex(arg, adEnv.curSymbolTable(),
                                                adEnv.curUnit().language(), calledUnit, i + 1),
                                        adEnv.curSymbolTable().typeOf(arg), formalType,
                                        argZonesTree, formalActiveDS, formalActiveUS, formalArgWritten,
                                        actualArgBeforeActive, calledUnit, treesToDiffInitUS, null, null);
                            }
//TapEnv.printlnOnTrace("==>"+treesToDiffInitUS) ;
                        }
                    }
                }
                if (multiDirMode()) {
                    Tree nbDirsVar = varRefDifferentiator().buildDirNumberReference(bwdSymbolTable);
                    tlNewArgs = tlNewArgs.placdl(nbDirsVar);
                }

                // Check for global parameters active at the end of the
                // called subroutine, but passive just after the call, or vice versa
                // (cf comments above for subroutine arguments) :
                // TODO verifier ce test, rajouter isOnceActive?
                if ((TapEnv.spareDiffReinitializations() || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG)
                        && !calledUnit.isIntrinsic() && afterActiv != null && !adEnv.activeRootCalledFromContext) {
                    for (int i = nDdZ - 1; i >= 0; --i) {
                        if (!afterActiv.get(i) && formalExitActive.get(i)) {
                            zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, adEnv.diffKind);
                            if (zoneInfo.isHidden) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(AD14) Differentiating this call to " + calledUnit.name() + " needs to initialize derivative of hidden variable: " + zoneInfo.description);
                                adEnv.addInHiddenDiffInitVariables(zoneInfo);
                            } else {
                                if (!TapList.contains(zonesToDiffInitDS, zoneInfo)) {
                                    zonesToDiffInitDS = new TapList<>(zoneInfo, zonesToDiffInitDS);
                                }
                            }
                        }
                    }
                }

                // Create "adjCall", the call to the adjoint:
                Tree diffCalledUnitName = varRefDifferentiator().diffSymbolTree(calledActivity,
                        ILUtils.copy(ILUtils.getCalledName(callTree)),
                        bwdSymbolTable, false, true, false, callTree,
                        false, calledDiffSort, calledDiffSort, DiffConstants.PROC, null);
                if (diffCalledUnit != null) {
                    CallGraph.addCallArrow(curDiffUnit(), SymbolTableConstants.CALLS, diffCalledUnit);
                }
                // THIS CREATES THE CENTRAL CALL TO THE ADJOINT :
                adjCall = ILUtils.buildCall(diffCalledUnitName, ILUtils.build(ILLang.op_expressions, hdNewArgs.tail));
                // Copy line number annotations :
                int lineNumber = ILUtils.getLineNumber(callTree);
                if (lineNumber != 0) {
                    ILUtils.getCalledName(adjCall).setAnnotation("line", ILUtils.build(ILLang.op_intCst, lineNumber));
                }
            }
            varRefDifferentiator().popDiffContext();
            placePopRebases(headRestoreAfterAdjoint.tail, false, isJoint);
            Tree[] diffInitTreeR = null;
            Tree diffInitTree;
            if (mustDiffCall) {
                // Tree[] diffInitTreeR;
                // Tree diffInitTree;
                while (treesToDiffInitUS != null) {
                    Tree treeToDiffReinit = treesToDiffInitUS.head;
                    diffInitTreeR = blockDifferentiator().makeDiffInitialization(treeToDiffReinit, null, adEnv.curSymbolTable(),
                                                                                 bwdSymbolTable, fwdSymbolTable, false, null, true);
                    for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                        diffInitTree = diffInitTreeR[iReplic] ;
                        if (diffInitTree != null) {
                            blockDifferentiator().addFuturePlainNodeBwd(diffInitTree, null, false,
                                ILUtils.usedVarsInExp(treeToDiffReinit, null, false),
                                null, null, new TapList<>(treeToDiffReinit, null), false, false,
                                "Reinit tdiff after adj-call", false);
                        }
                    }
                    treesToDiffInitUS = treesToDiffInitUS.tail;
                }
            }

            if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
                Tree profileParameters = 
                    ILUtils.build(ILLang.op_expressions, 
                        ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                        ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                        // ILUtils.build(ILLang.op_intCst, ILUtils.getLineNumber(callTree)));
                        ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                Tree profileCCall = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_endReverse"),
                    profileParameters
                    );
                    blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "End reverse profiling", false); 
            }

            if (mustDiffCall) {
                blockDifferentiator().addFuturePlainNodeBwd(adjCall, null, false,
                        readRefs, writtenRefs, readAdjRefs, writtenAdjRefs, calledUnit.isExternal(), true,
                        (splitAdjointCall ? "BWD split adjoint call" : "Adjoint call"), true);
            }

            if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
                Tree profileParameters = 
                    ILUtils.build(ILLang.op_expressions, 
                        ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                        ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                        // ILUtils.build(ILLang.op_intCst, ILUtils.getLineNumber(callTree)));
                        ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                Tree profileCCall = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_beginReverse"),
                    profileParameters
                    );
                blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "Begin reverse profiling", false); 
            }
            
            if (mustDiffCall) {
                while (zonesToDiffInitDS != null) {
                    zoneInfo = zonesToDiffInitDS.head;
                    diffInitTreeR = blockDifferentiator().makeDiffInitialization(zoneInfo.accessTree, zoneInfo, adEnv.curSymbolTable(),
                                                                bwdSymbolTable, fwdSymbolTable, true, null, true);
                    for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                        diffInitTree = diffInitTreeR[iReplic] ;
                        if (diffInitTree != null) {
                            blockDifferentiator().addFuturePlainNodeBwd(diffInitTree, null, false,
                                ILUtils.usedVarsInExp(zoneInfo.accessTree, null, false),
                                null, null, new TapList<>(zoneInfo.accessTree, null), false, false,
                                "Reinit-zero diff before adj-call", false);
                        }
                    }
                    zonesToDiffInitDS = zonesToDiffInitDS.tail;
                }
                while (treesToDiffInitDS != null) {
                    TapTriplet<Tree, WrapperTypeSpec, Tree> toDiffInitDS = treesToDiffInitDS.head;
                    Tree treeToDiffReinit = toDiffInitDS.first;
                    Tree srcArg = toDiffInitDS.third;
                    diffInitTreeR = makeDiffInitializationOfCallArgument(treeToDiffReinit, srcArg,
                                                                         bwdSymbolTable,
                                                                         adEnv.curSymbolTable().typeOf(srcArg),
                                                                         toDiffInitDS.second,
                                                                         false, null, true);
                    for (int iReplic=0 ; iReplic<diffInitTreeR.length ; ++iReplic) {
                        diffInitTree = diffInitTreeR[iReplic] ;
                        if (diffInitTree != null) {
                            blockDifferentiator().addFuturePlainNodeBwd(diffInitTree, null, false,
                                ILUtils.usedVarsInExp(treeToDiffReinit, null, false),
                                null, null, new TapList<>(treeToDiffReinit, null), false, false,
                                "Reinit tdiff before adj-call", false);
                        }
                    }
                    treesToDiffInitDS = treesToDiffInitDS.tail;
                }
            } else if (resultTree != null && beforeActiv != null) {
                TapList resZonesTree =
                        adEnv.curSymbolTable().treeOfZonesOfValue(resultTree, null, curInstr, null);
                resZonesTree = TapList.copyTree(resZonesTree);
                DataFlowAnalyzer.includePointedElementsInTree(resZonesTree,
                        null, null, true, adEnv.curSymbolTable(), curInstr, false, true);
                TapIntList resZones = ZoneInfo.listAllZones(resZonesTree, true);
                boolean resTmp = TapIntList.intersects(resZones, adEnv.toActiveTmpZones.tail);
                TapIntList resDiffKindVectorIndices =
                        DataFlowAnalyzer.mapZoneRkToKindZoneRk(resZones, adEnv.diffKind, adEnv.curSymbolTable());
                if (resTmp || beforeActiv.intersects(resDiffKindVectorIndices)) {
                    for (adEnv.iReplic=TapEnv.diffReplica()-1 ; adEnv.iReplic>=0 ; --adEnv.iReplic) {
                        Tree diffResultTree =
                            varRefDifferentiator().diffVarRef(adEnv.curActivity(), resultTree, bwdSymbolTable,
                                    adEnv.curUnit().isFortran(), resultTree, false, true, null);
                        blockDifferentiator().addFutureSetDiffNodeBwd(
                            new DiffAssignmentNode(diffResultTree, DiffConstants.SET_VARIABLE,
                                    null, null, resultTree, adEnv.iReplic,
                                    ILUtils.usedVarsInExp(resultTree, null, false), null,
                                    null, new TapList<>(resultTree, null)),
                            "Zero-reset diff of call result");
                    }
                    adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                }
            }

            if (normalCheckpointedCall) {
                if (headLookBeforeAdjoint.tail != null) {
                    boolean reallyNeedsToLook = usesTheStack(headLookBeforeAdjoint.tail);
                    Tree treeForLook;
                    if (reallyNeedsToLook) {
                        treeForLook = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "adStack_endRepeat"),
                                ILUtils.build(ILLang.op_expressions));
                        blockDifferentiator().addFuturePlainNodeBwd(treeForLook, null, false,
                                null, null, null, null, false, true,
                                "End repeated stack access", false);
                        treeForLook = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "adStack_resetRepeat"),
                                ILUtils.build(ILLang.op_expressions));
                        blockDifferentiator().addFuturePlainNodeBwd(treeForLook, null, false,
                                null, null, null, null, false, true,
                                "Reset repeated stack access", false);
                    }
                    placePopRebases(headLookBeforeAdjoint.tail, true, isJoint);
                    if (reallyNeedsToLook) {
                        treeForLook = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "adStack_startRepeat"),
                                ILUtils.build(ILLang.op_expressions));
                        blockDifferentiator().addFuturePlainNodeBwd(treeForLook, null, false,
                                null, null, null, null, false, true,
                                "Start repeated stack access", false);
                    }
                }
                placePopRebases(headRestoreBeforeAdjoint.tail, true, isJoint);
            }

            // === Now fill the code for the FWD sweep ===
            // Add the "save" instructions into the fwd sweep :
            TapList<TapPair<RefDescriptor, RefDescriptor>> tailSaveBeforeCall = headSaveBeforeCall;

            // Add profiling information
            if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
                Tree profileParameters = 
                            ILUtils.build(ILLang.op_expressions,  
                                ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                                ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                Tree profileCCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_SNPWrite"),
                            profileParameters
                                );
                        blockDifferentiator().addFuturePlainNodeFwd(profileCCall, null, false, null, null, null, null, false, true, "Start snapshot profiling", false);
                profileParameters = 
                    ILUtils.build(ILLang.op_expressions,  
                        ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                        ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                        ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                profileCCall = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "adProfileAdj_SNPRead"),
                    ILUtils.copy(profileParameters)
                        );
                blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "End snapshot profiling", false);
            }

            while (tailSaveBeforeCall.tail != null) {
                TapPair<RefDescriptor, RefDescriptor> nextSaves = tailSaveBeforeCall.tail.head;
                tailSaveBeforeCall.tail = tailSaveBeforeCall.tail.tail;
                if (nextSaves.second != null) {
                    Tree saveDiff = nextSaves.second.makePush();
                    // saveDiff == op_call ou op_assign si -nooptim-nostatictape
                    // ou bien if (allocated ) :
                    Tree rwrwTree = null;
                    if (saveDiff.opCode() == ILLang.op_call) {
                        rwrwTree = ILUtils.getArguments(saveDiff).down(1);
                    } else if (saveDiff.opCode() == ILLang.op_assign) {
                        rwrwTree = saveDiff.down(2);
                    } else if (saveDiff.opCode() == ILLang.op_if) {
                        rwrwTree = ILUtils.getArguments(saveDiff.down(2).down(1)).down(1);
                    }
                    // TODO: replicate if -diffReplica ?
                    blockDifferentiator().addFuturePlainNodeFwd(saveDiff, null, false,
                            null, null, new TapList<>(rwrwTree, null), null, false, true,
                            "Take DIFF snapshot", false);
                }
                if (nextSaves.first != null) {
                    Tree savePrimal = nextSaves.first.makePush();
                    Tree rwrwTree = null;
                    if (savePrimal.opCode() == ILLang.op_call) {
                        rwrwTree = ILUtils.getArguments(savePrimal).down(1);
                    } else if (savePrimal.opCode() == ILLang.op_assign) {
                        rwrwTree = savePrimal.down(2);
                    } else if (savePrimal.opCode() == ILLang.op_if) {
                        rwrwTree = ILUtils.getArguments(savePrimal.down(2).down(1)).down(1);
                    }
                    blockDifferentiator().addFuturePlainNodeFwd(savePrimal, null, false,
                            new TapList<>(rwrwTree, null), null, null, null, false, true,
                            "Take snapshot", false);
                }
            }

            
        }

        // Add profiling information
        if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
            Tree profileParameters = 
                            ILUtils.build(ILLang.op_expressions,  
                                ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                                ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
            Tree profileCCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_SNPWrite"),
                            profileParameters
                                );
            if(adEnv.activeRootCalledFromContext) blockDifferentiator().addFuturePlainNodeFwd(profileCCall, null, false, null, null, null, null, false, true, "Start snapshot profiling", false);
            
            profileParameters = ILUtils.build(ILLang.op_expressions, 
                    ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                    ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                    ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
            profileCCall = ILUtils.buildCall(
                ILUtils.build(ILLang.op_ident, "adProfileAdj_beginAdvance"),
                profileParameters
                );
            blockDifferentiator().addFuturePlainNodeFwd(profileCCall, null, false, null, null, null, null, false, true, "Start advance profiling", false);
        }

        if (callIsLive) {
            TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
            fillCallRefsRW(callTree, resultTree, calledUnit, refsRW);
//             blockDifferentiator().fillRefsRW(callTree, refsRW, callTree, calledUnit, resultTree) ;
            Tree fwdCallTree;
            Tree fwdTree;
            Tree[] args;
            
            if (mustDiffCall
                    && (calledUnit.isStandard() || calledUnit.isExternal())
                    && (adEnv.curUnitIsContext || splitAdjointCall)) {
                // Build the call to subroutine_fwd (also to subroutine_b from the calling context) :
                // Dirty trick: it turns out that differentiateProcedureHeader() does just what we want
                // but this is dirty because actually, we are not differentiating a header !
                Tree fwdTree2 = null;
                if (adEnv.activeRootCalledFromContext && splitAdjointCall) {
                    // Bizarre case where the context calls {rootUnit_FWD() ; rootUnit_BWD() ;}
                    // This builds rootUnit_BWD(). rootUnit_FWD() will be built later.
                    fwdTree2 = differentiateProcedureHeader(ILUtils.copy(callTree), resultTree, null, calledActivity,
                            fwdSymbolTable, curFwdDiffUnit().privateSymbolTable(), DiffConstants.ADJOINT_BWD, null);
                    // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
                    args = ILUtils.getArguments(fwdTree2).children();
                    for (int i = args.length - 1; i >= 0; --i) {
                        if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                            args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                        }
                    }
                    blockDifferentiator().addFuturePlainNodeBwd(fwdTree2, null, false,
                            refsRW.first, refsRW.second, null, null, true, true,
                            "BWD split (in context) adjoint call", false);

                    Unit bwdDiffCalledUnit = adEnv.getDiffOfUnit(calledUnit, calledActivity, DiffConstants.ADJOINT_BWD);
                    if (bwdDiffCalledUnit != null) {
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO BWD SPLIT IN CONTEXT "+bwdDiffCalledUnit) ;
                        CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, bwdDiffCalledUnit);
                    }
                }

                int thisDiffMode = (splitAdjointCall ? DiffConstants.ADJOINT_FWD : DiffConstants.ADJOINT);
                ToObject<Tree> toDiffLhs = new ToObject<>(null);
                fwdCallTree = differentiateProcedureHeader(ILUtils.copy(callTree),
                        (resultTree == null ? ILUtils.build(ILLang.op_none) : resultTree),
                        toDiffLhs, calledActivity, fwdSymbolTable,
                        curFwdDiffUnit().privateSymbolTable(), thisDiffMode, null);
                fwdTree = fwdCallTree;
                // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
                args = ILUtils.getArguments(fwdCallTree).children();
                for (int i = args.length - 1; i >= 0; --i) {
                    if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                        args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                    }
                }
                if (resultTree != null
                        //There's no function result when context calls differentiated non-context function (cf F90:lha60)
                        && !adEnv.activeRootCalledFromContext) {
                    // function call
                    Tree resultDiffTree = toDiffLhs.obj();
                    if (resultDiffTree == null) {
                        resultDiffTree = ILUtils.copy(resultTree);
                    }
                    fwdTree = ILUtils.build(ILLang.op_assign, resultDiffTree, fwdTree);
                }
                if (adEnv.activeRootCalledFromContext) {
                    if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
                        Tree profileParameters = 
                            ILUtils.build(ILLang.op_expressions, 
                                    ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                                // ILUtils.build(ILLang.op_intCst, ILUtils.getLineNumber(callTree)));
                                ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                        Tree profileCCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_endReverse"),
                            profileParameters
                            );
                            blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "End reverse profiling", false); 
                    }
                    blockDifferentiator().addFuturePlainNodeBwd(fwdTree, curInstr.whereMask(), false,
                                refsRW.first, refsRW.second, null, null, true, true,
                                "context adjoint call", false);
                    if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
                        Tree profileParameters = 
                            ILUtils.build(ILLang.op_expressions, 
                                ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                                // ILUtils.build(ILLang.op_intCst, ILUtils.getLineNumber(callTree)));
                                ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                        Tree profileCCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_beginReverse"),
                            profileParameters
                            );
                        blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "Begin reverse profiling", false); 

                        profileParameters = 
                            ILUtils.build(ILLang.op_expressions,  
                                ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                                ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()),
                                ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
                        profileCCall = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "adProfileAdj_SNPRead"),
                            ILUtils.copy(profileParameters)
                                );
                        blockDifferentiator().addFuturePlainNodeBwd(profileCCall, null, false, null, null, null, null, false, true, "End snapshot profiling", false);
                    }
                    
                } else {
                    blockDifferentiator().addFuturePlainNodeFwd(fwdTree, curInstr.whereMask(), false,
                            refsRW.first, refsRW.second, null, null, calledUnit.isExternal(), true,
                            "FWD split adjoint call", false);
                }

                Unit fwdDiffCalledUnit = adEnv.getDiffOfUnit(calledUnit, calledActivity, thisDiffMode);
                if (fwdDiffCalledUnit != null) {
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO FWD SPLIT OR CONTEXT "+fwdDiffCalledUnit) ;
                    CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, fwdDiffCalledUnit);
                }

            } else {

//                 // If original instruction was split, remove the "sourcetree" annotation
//                 // so that code decompilation doesn't reuse the original instruction tree:
//                 if (callTree.getAnnotation("hasBeenSplit")==Boolean.TRUE)
//                     callTree.removeAnnotation("sourcetree") ;

                // Put back the original source call in case of nameEq or optional arguments:
                // So far, the similar mechanism for Tangent is done somewhere else ?
                Tree sourceTreeAnnotation = callTree.getAnnotation("sourcetree");
                if (sourceTreeAnnotation != null && sourceTreeAnnotation.opCode() == ILLang.op_call) {
                    fwdCallTree = ILUtils.copy(sourceTreeAnnotation);
                } else {
                    fwdCallTree = ILUtils.copy(callTree);
                }
                // Add the annotation that labels Fortran arguments that are functions, for upper-case display:
                args = ILUtils.getArguments(fwdCallTree).children();
                for (int i = args.length - 1; i >= 0; --i) {
                    if (adEnv.curSymbolTable().identIsAFunction(args[i])) {
                        args[i].setAnnotation("isFunctionName", Boolean.TRUE);
                    }
                }
                fwdTree = fwdCallTree;
                if (resultTree != null) {
                    fwdTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(resultTree), fwdTree);
                }



                blockDifferentiator().addFuturePlainNodeFwd(fwdTree, curInstr.whereMask(), false,
                        refsRW.first, refsRW.second, null, null, calledUnit.isExternal(), true,
                        "Copy of primal call", false);


                Unit primalCalledUnit = adEnv.getPrimalCopyOfOrigUnit(calledUnit);
                // If the called routine has no primal copy, we place a CallArrow to the source called routine
                // Danger [19Mar18]: this is the 1st example of a CallArrow between 2 different CallGraph's...
                if (primalCalledUnit == null) {
                    primalCalledUnit = calledUnit;
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO ORIG "+primalCalledUnit) ;
//                 } else {
// TapEnv.printlnOnTrace("ADDING CALLARROW FROM DIFF "+curFwdDiffUnit()+" TO PRIMAL COPY "+primalCalledUnit) ;
                }
                if (primalCalledUnit != null) {
                    CallGraph.addCallArrow(curFwdDiffUnit(), SymbolTableConstants.CALLS, primalCalledUnit);
                }
            }

        }

        if (mustDiffCall && TapEnv.profile() && !splitAdjointCall) {
            Tree profileParameters = 
                ILUtils.build(ILLang.op_expressions, 
                    ILUtils.buildStringCst(calledUnit.name(), curDiffUnit().isFortran()), 
                    ILUtils.buildStringCst(adEnv.getCurrentFilename(false), curDiffUnit().isFortran()), 
                    // ILUtils.build(ILLang.op_intCst, ILUtils.getLineNumber(callTree)));
                    ILUtils.build(ILLang.op_intCst, curInstr.lineNo));
            Tree profileCCall = ILUtils.buildCall(
                ILUtils.build(ILLang.op_ident, "adProfileAdj_endAdvance"),
                profileParameters
                );
            blockDifferentiator().addFuturePlainNodeFwd(profileCCall, null, false, null, null, null, null, false, true, "End advance profiling", false);
        }
    }

    private void placePopRebases(TapList<TapPair<RefDescriptor, RefDescriptor>> refsToPop, boolean beforeCall, boolean isJoint) {
        RefDescriptor refDescriptor, refDescriptorDiff;
        Tree rebaseTree;
        TapList<Tree> writtenTrees;
        while (refsToPop != null) {
            refDescriptor = refsToPop.head.first;
            refDescriptorDiff = refsToPop.head.second;
            RefDescriptor nonNullRefDescriptor = refDescriptor != null ? refDescriptor : refDescriptorDiff;
            if (TypeSpec.isA(nonNullRefDescriptor.elementType, SymbolTableConstants.POINTERTYPE)) {//maybe use refDescriptor.holdsAPointer
                Tree refTree = nonNullRefDescriptor.refTree();
                boolean rebasePrimal = refDescriptor != null &&
                        DataFlowAnalyzer.mayPointToRelocated(refTree, false, adEnv.curInstruction(), adEnv.curSymbolTable(), isJoint);
                boolean rebaseOnDiffPtr = refDescriptorDiff != null &&
                        DataFlowAnalyzer.mayPointToRelocated(refTree, true, adEnv.curInstruction(), adEnv.curSymbolTable(), isJoint);
                if (rebasePrimal || rebaseOnDiffPtr) {
                    ++blockDifferentiator().numRebase;
                    rebaseTree = nonNullRefDescriptor.makeRebase(rebasePrimal ? null : new TapList<>(Boolean.TRUE, null),
                            rebaseOnDiffPtr ? null : new TapList<>(Boolean.TRUE, null),
                            rebaseOnDiffPtr ? refDescriptorDiff.altRootIdent : null,
                            (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
                    if (rebaseTree != null) {
                        adEnv.usesADMM = true;
                        writtenTrees = nonNullRefDescriptor.collectTrees(null);
                        blockDifferentiator().addFuturePlainNodeBwd(rebaseTree, null, false,
                                ILUtils.usedVarsInTreeOfExps(writtenTrees, null, true),
                                rebasePrimal ? writtenTrees : null,
                                rebaseOnDiffPtr ? writtenTrees : null,
                                rebaseOnDiffPtr ? writtenTrees : null, false, false,
                                "Rebase pointer(s) after their restoration", false);
                    }
                }
            }
            if (refDescriptorDiff != null) {
                refDescriptorDiff.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                Tree popTree = refDescriptorDiff.makePop();
                Tree refTree = refDescriptorDiff.refTree();
                // TODO: replicate if -diffReplica ?
                blockDifferentiator().addFuturePlainNodeBwd(popTree, null, false,
                        ILUtils.usedVarsInExp(refTree, null, false),
                        null, null, new TapList<>(refTree, null), false, true,
                        "Restore DIFF snapshot " + (beforeCall ? "before" : "after") + " adjoint call", false);
            }
            if (refDescriptor != null) {
                refDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                Tree popTree = refDescriptor.makePop();
                Tree refTree = refDescriptor.refTree();
                blockDifferentiator().addFuturePlainNodeBwd(popTree, null, false,
                        ILUtils.usedVarsInExp(refTree, null, false),
                        new TapList<>(refTree, null), null, null, false, true,
                        "Restore snapshot " + (beforeCall ? "before" : "after") + " adjoint call", false);
            }
            refsToPop = refsToPop.tail;
        }
    }

    /**
     * Build the derivative instructions that come from the given source "tree",
     * which is an MPI call. This builds the list that must go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order)
     */
    protected void buildDiffInstructionsOfMPICall(Tree callTree, Tree result, int differentiationMode,
                                                  Unit calledUnit, ActivityPattern calledActivity, MPIcallInfo messagePassingInfo,
                                                  boolean mustDiffOperation,
                                                  SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
                                                  BoolVector beforeActiv, BoolVector afterActiv,
                                                  BoolVector beforeUseful, BoolVector afterUseful) {
        TapIntList channelZones = null;
        if (messagePassingInfo != null) // messagePassingInfo is null for "administrative" Message-Passing calls.
        {
            channelZones = messagePassingInfo.findMessagePassingChannelZones(adEnv.srcCallGraph());
        }
//[llh] TODO: ATTENTION A:
// -- mettre en place le calcul fin des channels
// -- raffiner les channels pour les champs des types structures
// -- bien remplir les *refsRW pour preserver l'ordre, y compris p/r a channelZones i.e. refsRW+=channelZones
// -- en C, les MPI_CALL sont des fonctions => resultTree !!
// -- effacer le code mort de differentiateMessagePassingCall()

        if (mustDiffOperation) {
            Tree initTree;
            TapList<Tree> readTrees, writtenDiffTrees;

            // Build the call to the MPI FWD-adjoint or tangent:
            TapPair<TapList<Tree>, TapList<Tree>> refsRW = new TapPair<>(null, null);
            TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW = new TapPair<>(null, null);
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsUS = new TapList<>(null, null);
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsDS = new TapList<>(null, null);
            Tree fwdCallTree = differentiateMessagePassingCall(callTree, calledUnit, calledActivity,
                    differentiationMode == DiffConstants.TANGENT_MODE ? DiffConstants.TANGENT : DiffConstants.ADJOINT_FWD,
                    fwdSymbolTable, refsRW, diffRefsRW,
                    beforeActiv, afterActiv,
                    toNecessaryInitsUS, toNecessaryInitsDS, messagePassingInfo);
            // TODO: utiliser srcTree pour remettre les nameEq et les arguments fonction dans fwdCallTree !

            // Build possibly needed diff init before the FWD-diff MPI call:
            toNecessaryInitsUS = toNecessaryInitsUS.tail;
            while (toNecessaryInitsUS != null) {
                initTree = toNecessaryInitsUS.head.second;
                writtenDiffTrees = toNecessaryInitsUS.head.first;
                readTrees = ILUtils.usedVarsInTreeOfExps(writtenDiffTrees, null, false);
                blockDifferentiator().addFuturePlainNodeFwd(initTree, null, false,
                        readTrees, null, null, writtenDiffTrees, false, false,
                        "Needed diff MP pre-init", false);
                toNecessaryInitsUS = toNecessaryInitsUS.tail;
            }

            if (fwdCallTree != null) {
                // TODO: replicate if -diffReplica
                blockDifferentiator().addFuturePlainNodeFwd(fwdCallTree, null, false,
                        refsRW.first, refsRW.second,
                        diffRefsRW.first, diffRefsRW.second, false, true,
                        (differentiationMode == DiffConstants.TANGENT_MODE ? "Tangent" : "FWD Adjoint") + " MPI call", differentiationMode == DiffConstants.TANGENT_MODE);
            }

            // Build possibly needed diff init after the FWD-diff MPI call:
            toNecessaryInitsDS = toNecessaryInitsDS.tail;
            while (toNecessaryInitsDS != null) {
                initTree = toNecessaryInitsDS.head.second;
                writtenDiffTrees = toNecessaryInitsDS.head.first;
                readTrees = ILUtils.usedVarsInTreeOfExps(writtenDiffTrees, null, false);
                blockDifferentiator().addFuturePlainNodeFwd(initTree, null, false,
                        readTrees, null, null, writtenDiffTrees, false, false,
                        "Needed diff MP post-init", false);
                toNecessaryInitsDS = toNecessaryInitsDS.tail;
            }

            if (differentiationMode == DiffConstants.ADJOINT_MODE || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
                // Build the call to the MPI BWD-adjoint:
                refsRW = new TapPair<>(null, null);
                diffRefsRW = new TapPair<>(null, null);
                toNecessaryInitsUS = new TapList<>(null, null);
                toNecessaryInitsDS = new TapList<>(null, null);
                varRefDifferentiator().pushDiffContext(DiffConstants.INCALL);
                Tree bwdCallTree = differentiateMessagePassingCall(callTree, calledUnit, calledActivity, DiffConstants.ADJOINT_BWD,
                        bwdSymbolTable, refsRW, diffRefsRW,
                        beforeActiv, afterActiv,
                        toNecessaryInitsUS, toNecessaryInitsDS, messagePassingInfo);
                varRefDifferentiator().popDiffContext();
                // TODO: utiliser srcTree pour remettre les nameEq et les arguments fonction dans bwdCallTree !

                // Build possibly needed diff init after the BWD-diff MPI call:
                toNecessaryInitsUS = toNecessaryInitsUS.tail;
                while (toNecessaryInitsUS != null) {
                    initTree = toNecessaryInitsUS.head.second;
                    writtenDiffTrees = toNecessaryInitsUS.head.first;
                    readTrees = ILUtils.usedVarsInTreeOfExps(writtenDiffTrees, null, false);
                    blockDifferentiator().addFuturePlainNodeBwd(initTree, null, false,
                            readTrees, null, null, writtenDiffTrees, false, false,
                            "Needed diff MP post-init", false);
                    toNecessaryInitsUS = toNecessaryInitsUS.tail;
                }

                if (bwdCallTree != null) {
                    // Copy line number annotations :
                    int lineNumber = ILUtils.getLineNumber(callTree);
                    if (lineNumber != 0) {
                        ILUtils.getCalledName(bwdCallTree).setAnnotation("line", ILUtils.build(ILLang.op_intCst, lineNumber));
                    }
                    // TODO: replicate if -diffReplica
                    blockDifferentiator().addFuturePlainNodeBwd(bwdCallTree, null, false,
                            refsRW.first, refsRW.second,
                            diffRefsRW.first, diffRefsRW.second, false, true,
                            "BWD Adjoint MPI call", true);
                }

                // Build possibly needed diff init before the BWD-diff MPI call:
                toNecessaryInitsDS = toNecessaryInitsDS.tail;
                while (toNecessaryInitsDS != null) {
                    initTree = toNecessaryInitsDS.head.second;
                    writtenDiffTrees = toNecessaryInitsDS.head.first;
                    readTrees = ILUtils.usedVarsInTreeOfExps(writtenDiffTrees, null, false);
                    blockDifferentiator().addFuturePlainNodeBwd(initTree, null, false,
                            readTrees, null, null, writtenDiffTrees, false, false,
                            "Needed diff MP pre-init", false);
                    toNecessaryInitsDS = toNecessaryInitsDS.tail;
                }

                // Detect the need for AMPI_Turn:
                // (i.e. when ADJOINT_MODE, and adjoint is a BW_AMPI_Irecv(vb) => *v is active)
                if (ILUtils.isCallingString(bwdCallTree, "bw_ampi_irecv", false)) {
                    Tree passedRef = ILUtils.getArguments(callTree).down(1);
                    TapList passedZonesTree = adEnv.curSymbolTable().treeOfZonesOfValue(passedRef, null, adEnv.curInstruction(), null);
                    // When in C, AMPI_Irecv is given a pointer. Here we extract the pointer destination zones:
                    if (adEnv.curUnit().isC() && passedZonesTree != null) {
                        passedZonesTree = TapList.copyTree(passedZonesTree);
                        DataFlowAnalyzer.includePointedElementsInTree(passedZonesTree, null, null, false,
                                adEnv.curSymbolTable(), adEnv.curInstruction(), false, true);
                        passedZonesTree = passedZonesTree.tail;
                    }
                    TapIntList passedZonesList = ZoneInfo.listAllZones(passedZonesTree, false);
                    //TODO: maybe distribute these requirements at the calling level ?
                    TapEnv.fileWarning(-1, callTree, "(Adjoint of AMPI_Irecv) an AMPI_Turn will be required on " + ILUtils.toString(passedRef) + " before its BW_AMPI_Wait");
// TapEnv.printlnOnTrace("ADD TO zonesMPIiReceived OF "+curUnit()+" VAR:"+passedRef+" ZONES:"+passedZonesTree+" -> "+passedZonesList) ;
                    DataFlowAnalyzer.setKindZoneRks(adEnv.curActivity().zonesMPIiReceived(), adEnv.diffKind, passedZonesList, true, adEnv.curSymbolTable());
                }

            }
        } else {
            // Build a copy of the original MPI call:
            blockDifferentiator().addFuturePlainNodeFwd(ILUtils.removeSourceCodeAnnot(ILUtils.copy(callTree)), null, false,
                    null, null, null, null, false, true,
                    "Copy of primal MPI call", true);
        }
    }

    /**
     * Differentiates a call to a Message-Passing procedure, given by "callTree"
     * Fills refsRW (resp. diffRefsRW) with the trees
     * that (resp. whose derivatives) are Read and Written by the differentiated call.
     * Fills toNecessaryInitsUS with the re-initializations of derivatives
     * that must be done UPSTREAM the call. Each element of toNecessaryInitsUS
     * is an TapPair of the Tree that re-initializes the derivative, and of
     * the TapList of the Trees whose derivatives are re-initialized.
     * Similarly fill toNecessaryInitsDS with the re-initializations that must be done DOWNSTREAM the call.
     *
     * @return the Tree of the differentiated call (or null if no diff for this diffMode).
     */
    private Tree differentiateMessagePassingCall(
            Tree callTree, Unit calledUnit, ActivityPattern calledActivity, int diffMode,
            SymbolTable diffSymbolTable,
            TapPair<TapList<Tree>, TapList<Tree>> refsRW,
            TapPair<TapList<Tree>, TapList<Tree>> diffRefsRW,
            BoolVector beforeActiv, BoolVector afterActiv,
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsUS,
            TapList<TapPair<TapList<Tree>, Tree>> toNecessaryInitsDS,
            MPIcallInfo mpiCallInfo) {
        UnitDiffInfo calledDiffInfo = callGraphDifferentiator().getUnitDiffInfo(calledUnit);
        FunctionTypeSpec calledFunctionTypeSpec =
                callTree.getAnnotation("functionTypeSpec");
        if (calledFunctionTypeSpec == null) {
            calledFunctionTypeSpec = calledUnit.functionTypeSpec();
        }
        Instruction curInstr = adEnv.curInstruction() ;
        CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
        int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
        int nDdZ = adEnv.curSymbolTable().declaredZonesNb(adEnv.diffKind);
        Tree[] actualArgs = ILUtils.getArguments(callTree).children();
        Tree actualArg, copiedArg;
        int nbArgs = actualArgs.length;
// This fix in case of mismatching nbArgs was harmful. Better not do it and be more careful later:
//         nbArgs = Math.min(nbArgs, calledUnit.functionTypeSpec().argumentsTypes.length) ;
        boolean[] formalArgsActivity = null;
        BoolVector formalCallActive = new BoolVector(nDdZ);
        BoolVector formalExitActive = new BoolVector(nDdZ);
        BoolVector callR = new BoolVector(nDZ);
        BoolVector callW = new BoolVector(nDZ);
        TapList[] actualArgsCallActive = null;
        TapList[] actualArgsExitActive = null;
        if (calledDiffInfo != null && calledActivity != null) {
            formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(calledActivity);
            actualArgsCallActive =
                DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.callActivity(), formalCallActive,
                        null, null, true,
                        callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
            actualArgsExitActive =
                DataFlowAnalyzer.translateCalleeDataToCallSite(calledActivity.exitActivity(), formalExitActive,
                        null, null, false,
                        callTree, curInstr, callArrow, adEnv.diffKind, null, null, true, false) ;
        }
        TapList[] paramsR = DataFlowAnalyzer.translateCalleeDataToCallSite(
                                    calledUnit.unitInOutPossiblyR(), callR, null, null, true,
                                    callTree, curInstr, callArrow,
                                    SymbolTableConstants.ALLKIND,
                                    null, null, true, false) ;
        TapList[] paramsW = DataFlowAnalyzer.translateCalleeDataToCallSite(
                                    calledUnit.unitInOutPossiblyW(), callW, null, null, true,
                                    callTree, curInstr, callArrow,
                                    SymbolTableConstants.ALLKIND,
                                    null, null, false, false) ;
        TapList<Tree> newArgs = null;
        Tree diffTree;
        WrapperTypeSpec argType;
        Unit diffCalledUnit = calledDiffInfo.getTangent(calledActivity);
        int firstOrderingProblem = findNameEqOrNone(callTree);
        TapList argZonesTree;
        TapList<ZoneInfo> zonesToDiffInitUS = null;
        TapList<ZoneInfo> zonesToDiffInitDS = null;
        TapList<ZoneInfo> tagsToDiffInit = null;
        for (int i = nbArgs - 1; i >= 0; i--) {
            actualArg = actualArgs[i];
            copiedArg = null;
            if (mpiCallInfo != null && (mpiCallInfo.argumentIsABuffer(i + 1) || mpiCallInfo.argumentIsAReduceOp(i + 1) || mpiCallInfo.argumentIsAType(i + 1))) {
                // ^ Only the buffer and the type (and the reduction Op) deserve a special treatment.
                if (diffMode != DiffConstants.ADJOINT_FWD) {
                    // ^ No diff buffer nor diff type in the FW_MPI diff calls
                    if (mpiCallInfo.argumentIsABuffer(i + 1)
                            && ILUtils.isIdent(actualArg, "mpi_in_place", !adEnv.curUnit().isFortran())) {
                        diffTree = ILUtils.copy(actualArg);
                        newArgs = new TapList<>(diffTree, newArgs);
                    } else if (mpiCallInfo.argumentIsAType(i + 1)
                            || mpiCallInfo.argumentIsAReduceOp(i + 1)
                            || i < formalArgsActivity.length - 1 && formalArgsActivity[i]) {
                        if (diffMode == DiffConstants.TANGENT) {
                            ToObject<Tree> toActualArg = new ToObject<>(actualArg);
                            Tree[] diffTreeR = blockDifferentiator().tangentDiffExpr(toActualArg, beforeActiv,
                                    refsRW, diffRefsRW);
                            diffTree = diffTreeR[0] ; // temporary during change for replica
                            actualArg = toActualArg.obj();
                        } else {
                            diffTree = null;
                        }
                        if (diffTree == null) {
                            if (mpiCallInfo.argumentIsAType(i + 1)) {
                                //Special case: differentiate the MPI type into itself.
                                // TODO: improve by putting a differentiated MPI type.
                                diffTree = ILUtils.copy(actualArg);
                            } else if (mpiCallInfo.argumentIsAReduceOp(i + 1)) {
                                diffTree = ILUtils.build(ILLang.op_intCst, 0);
                            } else if (ILUtils.isAWritableVarRef(actualArg, adEnv.curSymbolTable())) {
                                diffTree =
                                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), actualArg, diffSymbolTable,
                                                false, actualArg, false, true, null);
                            } else {
                                // For non-reference expressions: build a 0.0 derivative:
                                argType = calledFunctionTypeSpec.argumentsTypes[i];
                                if (argType != null && argType.wrappedType != null) {
                                    diffTree = argType.wrappedType.buildConstantZero();
                                }
                            }
                            if (diffTree == null)
                            // When everything else fails: build a 0.0 derivative:
                            {
                                diffTree = PrimitiveTypeSpec.buildRealConstantZero();
                            }
                        }
                        if (TapList.oneTrue(paramsR[i+1])) {
                            diffRefsRW.first = ILUtils.addTreeInList(actualArg, diffRefsRW.first);
                        }
                        if (TapList.oneTrue(paramsW[i+1])) {
                            diffRefsRW.second = ILUtils.addTreeInList(actualArg, diffRefsRW.second);
                        }
                        if (firstOrderingProblem != -1 &&
                                (i >= firstOrderingProblem ||
                                        actualArg.getAnnotation("sourcetree") != null)) {
                            Tree argName = getNewParamName(i, calledUnit, true, DiffConstants.TANGENT, diffCalledUnit);
                            diffTree = ILUtils.build(ILLang.op_nameEq, argName, diffTree);
                        }
                        newArgs = new TapList<>(MPIcallInfo.replaceMPIconstantsIfPossible(diffTree), newArgs);
                        //When the actual expression passed in the buffer is not active before the call,
                        // but the formal buffer is active, and the passed value will be used in the MPI call,
                        // we must (re)initialize the tangent diff of the passed expression.
                        if (TapEnv.spareDiffReinitializations() && ILUtils.isAWritableVarRef(actualArg, adEnv.curSymbolTable())
                                && mpiCallInfo.argumentIsABuffer(i + 1) && mpiCallInfo.argumentIsRead(i + 1)) {
                            argZonesTree =
                                    adEnv.curSymbolTable().treeOfZonesOfValue(actualArg, null, curInstr, null);
                            argZonesTree = TapList.copyTree(argZonesTree);
                            DataFlowAnalyzer.includePointedElementsInTree(argZonesTree,
                                    null, null, true, adEnv.curSymbolTable(), curInstr, false, true);
                            zonesToDiffInitUS =
                                    collectZonesFormalActiveAndActualPassive(argZonesTree, adEnv.toActiveTmpZones.tail,
                                            actualArgsCallActive[1+i], beforeActiv,
                                            calledUnit, zonesToDiffInitUS);
                            zonesToDiffInitDS =
                                    collectZonesFormalActiveAndActualPassive(argZonesTree, adEnv.toActiveTmpZones.tail,
                                            actualArgsExitActive[1+i], afterActiv,
                                            calledUnit, zonesToDiffInitDS);
                        }
                    }
                }
                if (diffMode != DiffConstants.ADJOINT_BWD || mpiCallInfo.isReduceCall()) {
                    // Don't pass the primal buffer and types to the BW_MPI diff calls, (except for reductions):
                    copiedArg = ILUtils.copy(actualArg);
                }
            } else {
                // ^ ... the other arguments are simply copied:
                copiedArg = ILUtils.copy(actualArg);
            }

            if (copiedArg != null) {
                if (actualArg.getAnnotation("sourcetree") != null) {
                    copiedArg = ILUtils.copy(actualArg.getAnnotation("sourcetree"));
                } else if (firstOrderingProblem != -1 && i >= firstOrderingProblem
                        && actualArg.opCode() != ILLang.op_none) {
                    Tree argName;
                    if (copiedArg.opCode() == ILLang.op_nameEq) {
                        argName = copiedArg.cutChild(1);
                        copiedArg = copiedArg.cutChild(2);
                    } else {
                        argName = getNewParamName(i, calledUnit, false, -1, null);
                    }
                    copiedArg = ILUtils.build(ILLang.op_nameEq, argName, copiedArg);
                }
                newArgs = new TapList<>(MPIcallInfo.replaceMPIconstantsIfPossible(copiedArg), newArgs);
            }

            refsRW.first = ILUtils.usedVarsInExp(actualArg, refsRW.first, false);
            if (TapList.oneTrue(paramsR[i+1])) {
                refsRW.first = ILUtils.addTreeInList(actualArg, refsRW.first);
            }
            if (TapList.oneTrue(paramsW[i+1])) {
                refsRW.second = ILUtils.addTreeInList(actualArg, refsRW.second);
            }
        }
        Tree[] zoneReinitTreeR;
        Tree zoneReinitTree;
        ZoneInfo zoneInfo;
        while (zonesToDiffInitUS != null) {
            zoneInfo = zonesToDiffInitUS.head;
            zoneReinitTreeR = blockDifferentiator().makeDiffInitialization(zoneInfo.accessTree, zoneInfo,
                                                                           adEnv.curSymbolTable(), diffSymbolTable, null,
                                                                           true, null, true);
            for (int iReplic=0 ; iReplic<zoneReinitTreeR.length ; ++iReplic) {
                zoneReinitTree = zoneReinitTreeR[iReplic] ;
                if (zoneReinitTree != null) {
                    toNecessaryInitsUS.placdl(new TapPair<>(new TapList<>(zoneInfo.accessTree, null),
                        zoneReinitTree));
                }
            }
            zonesToDiffInitUS = zonesToDiffInitUS.tail;
        }
        // TODO tagsToDiffInit ne contient qu'un element, integer a initialiser a une nouvelle valeur
        while (tagsToDiffInit != null) {
            zoneInfo = tagsToDiffInit.head;
            zoneReinitTree = makeDiffTagInitialization(zoneInfo.accessTree, diffSymbolTable);
            toNecessaryInitsUS.placdl(new TapPair<>(new TapList<>(zoneInfo.accessTree, null),
                    zoneReinitTree));
            tagsToDiffInit = tagsToDiffInit.tail;
        }
        while (zonesToDiffInitDS != null) {
            zoneInfo = zonesToDiffInitDS.head;
            zoneReinitTreeR = blockDifferentiator().makeDiffInitialization(zoneInfo.accessTree, zoneInfo, adEnv.curSymbolTable(), diffSymbolTable,
                                                                           null, true, null, true);
            for (int iReplic=0 ; iReplic<zoneReinitTreeR.length ; ++iReplic) {
                zoneReinitTree = zoneReinitTreeR[iReplic] ;
                if (zoneReinitTree != null) {
                    toNecessaryInitsDS.placdl(new TapPair<>(new TapList<>(zoneInfo.accessTree, null),
                        zoneReinitTree));
                }
            }
            zonesToDiffInitDS = zonesToDiffInitDS.tail;
        }
        String modePrefix = "";
        if (mpiCallInfo != null) {
            if (diffMode == DiffConstants.TANGENT) {
                modePrefix = "TLS_";
            } else if (diffMode == DiffConstants.ADJOINT_FWD) {
                modePrefix = mpiCallInfo.isCommCreation() ? "" : "FW_";
            } else {
                modePrefix = mpiCallInfo.isReduceCall() ? "BWS_" : "BW_";
            }
        }
        if (mpiCallInfo == null && diffMode == DiffConstants.ADJOINT_BWD) {
            // MPI calls that are pure control must not be repeated in the BWD part
            return null;
        } else if (mpiCallInfo != null && mpiCallInfo.isCommFree()
                && (diffMode == DiffConstants.ADJOINT_FWD || diffMode == DiffConstants.ADJOINT_BWD)) {
            // Communicators should not be freed between FWD and BWD sweeps:
            return null;
        } else if (mpiCallInfo != null && diffMode == DiffConstants.ADJOINT_BWD && mpiCallInfo.isCommCreation()) {
            // BW of Communicator creation should be turned into a Comm_free:
            String lcFuncName = mpiCallInfo.funcName().toLowerCase();
            if (lcFuncName.endsWith("mpi_comm_split")) {
                newArgs = newArgs.tail.tail.tail;
            } else if (lcFuncName.endsWith("mpi_comm_create")) {
                newArgs = newArgs.tail.tail;
            } else if (lcFuncName.endsWith("mpi_comm_dup")) {
                newArgs = newArgs.tail;
            }
            return ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, "AMPI_Comm_free"),
                    ILUtils.build(ILLang.op_expressions, newArgs));
        } else {
            return ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, modePrefix + ILUtils.getCalledNameString(callTree)),
                    ILUtils.build(ILLang.op_expressions, newArgs));
        }
    }

    /**
     * Fills refsRW with the trees that are possibly or partly read/overritten by callTree.
     */
    private void fillCallRefsRW(Tree callTree, Tree resultTree, Unit calledUnit,
                                TapPair<TapList<Tree>, TapList<Tree>> refsRW) {
        CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
        int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
        Tree[] actualArgs = ILUtils.getArguments(callTree).children();
        int nbArgs = actualArgs.length;
        boolean conservativeWriteIfNoRWinfo = true;
        BoolVector zonesReadByThisCall = new BoolVector(nDZ);
        BoolVector zonesWrittenByThisCall = new BoolVector(nDZ);
        boolean hasRWinfo = (calledUnit.unitInOutR()!=null) ;
        if (hasRWinfo) {
            DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyR(), zonesReadByThisCall,
                                                           null, actualArgs, true,
                                                           callTree, adEnv.curInstruction(), callArrow,
                                                           SymbolTableConstants.ALLKIND,
                                                           null, null, true, false) ;
            DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), zonesWrittenByThisCall,
                                                           null, actualArgs, true,
                                                           callTree, adEnv.curInstruction(), callArrow,
                                                           SymbolTableConstants.ALLKIND,
                                                           null, null, false, false) ;
        }
        if (!calledUnit.isIntrinsic()) {
            ZoneInfo zoneInfo;
            Tree zoneTree = null;
            for (int i = nDZ - 1; i >= 0; --i) {
                zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zoneInfo != null) {
                    zoneTree = zoneInfo.accessTree;
                    if (zoneTree != null) {
                        if (!hasRWinfo || zonesReadByThisCall.get(i)) {
                            refsRW.first = ILUtils.addTreeInList(zoneTree, refsRW.first);
                        }
                        if (hasRWinfo ? zonesWrittenByThisCall.get(i) : conservativeWriteIfNoRWinfo) {
                            refsRW.second = ILUtils.addTreeInList(zoneTree, refsRW.second);
                        }
                    }
                }
            }
        }
        if (resultTree != null) {
            refsRW.first = ILUtils.usedVarsInExp(resultTree, refsRW.first, false);
            refsRW.second = ILUtils.addTreeInList(resultTree, refsRW.second);
        }
        for (int i = nbArgs - 1; i >= 0; --i) {
            refsRW.first = ILUtils.usedVarsInExp(actualArgs[i], refsRW.first, false);
        }
    }

    /**
     * Collects the RefDescriptor's of all variables that must
     * be save/restored because they are in the given toSave* objects.
     * The collected RefDescriptor's are accumulated in the Push/Pop/Look
     * lists when they are provided, depending on the calling case.
     */
    private void collectRefDescriptorsToSave(
            BoolVector toSave, TapList[] toSaveArgs,
            BoolVector toSaveOnDiffPtr, TapList[] toSaveArgsOnDiffPtr,
            TapList<TapPair<RefDescriptor, RefDescriptor>> toVarsToPush,
            TapList<TapPair<RefDescriptor, RefDescriptor>> toVarsToPop1,
            TapList<TapPair<RefDescriptor, RefDescriptor>> toVarsToPop2, boolean isSbk,
            SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
            Unit calledUnit, FunctionTypeSpec functionTypeSpec, Tree[] args, int nbArgs,
            Tree resultTree, SplitForSave[] argsSplitForSave) {
        RefDescriptor refDescriptor, refDescriptorDiffPtr;
        RefDescriptor refDescriptorSave, refDescriptorSaveDiffPtr;
        RefDescriptor refDescriptorRestore, refDescriptorRestoreDiffPtr;
        Tree zoneInfoVisibleAccessTree, baseVarTree, diffBaseVar;
        ZoneInfo zoneInfo;
        int rk, rkOnDiffPtr;
        int nDZ = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
        for (int i = nDZ - 1; i >= 0; --i) {
            zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (// Bizarre zones cannot be PUSH/POP'ed, in particular IO streams and MPI channels:
                zoneInfo != null && !zoneInfo.isHidden && !zoneInfo.isChannelOrIO
                && !zoneInfo.isAllocatable //NoPushAllocatable
                // Special zones created on the "allocate" instructions cannot be PUSH/POP'ed:
                && !zoneInfo.comesFromAllocate()) {
                zoneInfoVisibleAccessTree = adEnv.curSymbolTable().buildZoneVisibleAccessTree(zoneInfo);
                rkOnDiffPtr = zoneInfo.kindZoneNb(SymbolTableConstants.PTRKIND);
                // Collect RefDescriptor's of DIFF zones to save because they are pointer zones and true in toSaveOnDiffPtr:
                if (zoneInfoVisibleAccessTree != null && rkOnDiffPtr >= 0 && toSaveOnDiffPtr.get(rkOnDiffPtr)) {
                    baseVarTree = ILUtils.baseTree(zoneInfoVisibleAccessTree);
                    diffBaseVar = null;
                    if (baseVarTree != null) {
                        diffBaseVar = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, fwdSymbolTable,
                                true, false, false, null, false,
                                curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree);
                    }
                    refDescriptorDiffPtr =
                            new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                    adEnv.curSymbolTable(), curFwdDiffUnit().privateSymbolTable(), null,
                                    diffBaseVar, true, null, curDiffUnit());
                    blockDifferentiator().prepareRestoreOperations(refDescriptorDiffPtr, null, 0, fwdSymbolTable, bwdSymbolTable);
                } else {
                    refDescriptorDiffPtr = null;
                }
                rk = zoneInfo.kindZoneNb(SymbolTableConstants.ALLKIND);
                // Collect RefDescriptor's of primal zones to save because they are in toSave:
                if (zoneInfoVisibleAccessTree != null && toSave.get(rk)) {
                    refDescriptor =
                            new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                                    adEnv.curSymbolTable(), curFwdDiffUnit().privateSymbolTable(), null,
                                    null, false, null, curDiffUnit());
                    blockDifferentiator().prepareRestoreOperations(refDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
                } else {
                    refDescriptor = null;
                }
                if (refDescriptor != null || refDescriptorDiffPtr != null) {
                    TapPair<RefDescriptor, RefDescriptor> twoRefDescriptors = new TapPair<>(refDescriptor, refDescriptorDiffPtr);
                    if (toVarsToPush != null) {
                        toVarsToPush.tail = toVarsToPush.tail.placdl(twoRefDescriptors);
                    }
                    if (toVarsToPop1 != null) {
                        toVarsToPop1.tail = toVarsToPop1.tail.placdl(twoRefDescriptors);
                    }
                    if (toVarsToPop2 != null) {
                        toVarsToPop2.tail = toVarsToPop2.tail.placdl(twoRefDescriptors);
                    }
                }
            }
        }
        Tree arg, subArgSave, subArgSaveDiff, subArgRestore, subArgRestoreDiff;
        Tree diffBaseVarFwd, diffBaseVarBwd;
        TapList argZonesTree;
        WrapperTypeSpec actualArgType = null, formalArgType = null;
        boolean saveInPrimal, saveInDiff, primalAlreadySaved, diffptrAlreadySaved, argZonesAreSpecial;
        for (int i = nbArgs; i >= 0; --i) {
            arg = (i == nbArgs ? resultTree : args[i]);
            baseVarTree = ((arg == null || arg.opCode() == ILLang.op_label || arg.opCode() == ILLang.op_arrayConstructor) ? null : ILUtils.baseTree(arg));
            // Collect RefDescriptor's of DIFF zones to save because they are actual args with toSaveArgsOnDiffPtr:
            saveInPrimal = TapList.oneTrue(toSaveArgs[i])
                    || !isSbk && i == nbArgs && arg != null && ADTBRAnalyzer.isTBR(adEnv.curActivity(), arg);
            saveInDiff = TapList.oneTrue(toSaveArgsOnDiffPtr[i])
                    || !isSbk && i == nbArgs && arg != null && ADTBRAnalyzer.isTBROnDiffPtr(adEnv.curActivity(), arg);
            if (saveInPrimal || saveInDiff) {
                argZonesTree = adEnv.curSymbolTable().treeOfZonesOfValue(arg, null, adEnv.curInstruction(), null);
                argZonesTree = TapList.copyTree(argZonesTree);
                DataFlowAnalyzer.includePointedElementsInTree(argZonesTree,
                        null, null, true, adEnv.curSymbolTable(), adEnv.curInstruction(), false, true);
                TapIntList argZones = ZoneInfo.listAllZones(argZonesTree, true);
                argZonesAreSpecial = argZones == null || argZones.head >= adEnv.curSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND);
                // If arg is in toSave, it was already saved:
                //  saving it as an actual argument would only save it one time too many:
                primalAlreadySaved =
                        DataFlowAnalyzer.containsExtendedDeclared(toSave, SymbolTableConstants.ALLKIND,
                                argZones, null, adEnv.curSymbolTable(), calledUnit);
                // If DIFF arg is in toSaveOnDiffPtr, it was already saved:
                //  saving it as an actual argument would only save it one time too many:
                diffptrAlreadySaved =
                        DataFlowAnalyzer.containsExtendedDeclared(toSaveOnDiffPtr, SymbolTableConstants.PTRKIND,
                                argZones, null, adEnv.curSymbolTable(), calledUnit);
                actualArgType = adEnv.curSymbolTable().typeOf(arg);
                formalArgType = (i == nbArgs ? functionTypeSpec.returnType : functionTypeSpec.argumentsTypes[i]);
                // Peel off possible pointer type layers during cross-language calls:
                // [llh] TODO: refine when we have more than one level of pointers (cf set10/mix57)
                if (adEnv.curUnit().isFortran() && calledUnit.isC()) {
                    if (!TypeSpec.isA(actualArgType, SymbolTableConstants.POINTERTYPE) && TypeSpec.isA(formalArgType, SymbolTableConstants.POINTERTYPE)) {
                        formalArgType = ((PointerTypeSpec) formalArgType.wrappedType).destinationType;
                    }
                } else if (adEnv.curUnit().isC() && calledUnit.isFortran()) {
                    if (TypeSpec.isA(actualArgType, SymbolTableConstants.POINTERTYPE) && !TypeSpec.isA(formalArgType, SymbolTableConstants.POINTERTYPE)) {
                        formalArgType = new WrapperTypeSpec(new PointerTypeSpec(formalArgType, null));
                    }
                }
                Tree arg2 = RefDescriptor.explicitWhenImplicitArray(arg, actualArgType,
                        formalArgType, adEnv.curSymbolTable());
                argsSplitForSave[i].initIndexes(arg2, arg2 != arg, adEnv);
            } else {
                argZonesAreSpecial = false;
                primalAlreadySaved = false;
                diffptrAlreadySaved = false;
            }
            if (argZonesAreSpecial || (saveInDiff && !diffptrAlreadySaved) || (saveInPrimal && !primalAlreadySaved)) {
                TapList<Tree[]> subArgsSaveRestore =
                        collectSubTreesToRestore(ILUtils.copy(argsSplitForSave[i].tree),
                                argsSplitForSave[i].newSplitCopy(bwdSymbolTable),
                                formalArgType, actualArgType, toSaveArgs[i], toSaveArgsOnDiffPtr[i], null, null);
                diffBaseVarFwd =
                    (baseVarTree == null || !saveInDiff
                     ? null
                     : varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, fwdSymbolTable,
                               true, false, false, null, false,
                               curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree));
                diffBaseVarBwd =
                     (baseVarTree == null || !saveInDiff
                      ? null
                      : varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, bwdSymbolTable,
                                true, false, false, null, false,
                                curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, baseVarTree));
                while (subArgsSaveRestore != null) {
                    subArgSave = subArgsSaveRestore.head[0];
                    subArgRestore = subArgsSaveRestore.head[1];
                    subArgSaveDiff = subArgsSaveRestore.head[2];
                    subArgRestoreDiff = subArgsSaveRestore.head[3];
                    if (subArgSaveDiff != null) {
                        refDescriptorSaveDiffPtr =
                                new RefDescriptor(subArgSaveDiff, null, adEnv.curSymbolTable(),
                                        curDiffUnit().privateSymbolTable(),
                                        curFwdDiffUnit().privateSymbolTable(),
                                        diffBaseVarFwd, true, null, curDiffUnit());
                        refDescriptorRestoreDiffPtr =
                                new RefDescriptor(subArgRestoreDiff, null, adEnv.curSymbolTable(),
                                        curDiffUnit().privateSymbolTable(), null,
                                        diffBaseVarBwd, true, null, curDiffUnit());
                        // Set zoneInfo to force detection for warning "(AD15) Not sure how to push/pop":
                        if (i != nbArgs) {
                            allowForUnsureWarningMessage(subArgSaveDiff, refDescriptorSaveDiffPtr, refDescriptorRestoreDiffPtr);
                        }
                        blockDifferentiator().prepareRestoreOperations(refDescriptorSaveDiffPtr, refDescriptorRestoreDiffPtr,
                                0, fwdSymbolTable, bwdSymbolTable);
                    } else {
                        refDescriptorSaveDiffPtr = null;
                        refDescriptorRestoreDiffPtr = null;
                    }
                    if (subArgSave != null) {
                        refDescriptorSave =
                                new RefDescriptor(subArgSave, null, adEnv.curSymbolTable(),
                                        curDiffUnit().privateSymbolTable(),
                                        curFwdDiffUnit().privateSymbolTable(),
                                        null, false, null, curDiffUnit());
                        refDescriptorRestore =
                                new RefDescriptor(subArgRestore, null, adEnv.curSymbolTable(),
                                        curDiffUnit().privateSymbolTable(), null,
                                        null, false, null, curDiffUnit());
                        // Set zoneInfo to force detection for warning "(AD15) Not sure how to push/pop":
                        if (i != nbArgs) {
                            allowForUnsureWarningMessage(subArgSave, refDescriptorSave, refDescriptorRestore);
                        }
                        // mixed-language call:
                        if (calledUnit.language() != curDiffUnit().language()
                                && !calledUnit.takesArgumentByValue(i + 1, curDiffUnit().language())) {
                            if (TypeSpec.isA(refDescriptorSave.completeType, SymbolTableConstants.POINTERTYPE)
                                    && TypeSpec.isA(refDescriptorSave.elementType, SymbolTableConstants.POINTERTYPE)) {
                                refDescriptorSave.completeType =
                                        ((PointerTypeSpec) refDescriptorSave.completeType.wrappedType).destinationType;
                                refDescriptorSave.elementType =
                                        ((PointerTypeSpec) refDescriptorSave.elementType.wrappedType).destinationType;
                                refDescriptorSave.addDeref();
                                refDescriptorSave.mayProtectAccesses = true;
                                refDescriptorRestore.completeType =
                                        ((PointerTypeSpec) refDescriptorRestore.completeType.wrappedType).destinationType;
                                refDescriptorRestore.elementType =
                                        ((PointerTypeSpec) refDescriptorRestore.elementType.wrappedType).destinationType;
                                refDescriptorRestore.addDeref();
                                refDescriptorRestore.mayProtectAccesses = true;
                            }
                        }
                        blockDifferentiator().prepareRestoreOperations(refDescriptorSave, refDescriptorRestore,
                                0, fwdSymbolTable, bwdSymbolTable);
                    } else {
                        refDescriptorSave = null;
                        refDescriptorRestore = null;
                    }
                    if (toVarsToPush != null) {
                        toVarsToPush.tail =
                                toVarsToPush.tail.placdl(new TapPair<>(refDescriptorSave, refDescriptorSaveDiffPtr));
                    }
                    if (toVarsToPop1 != null) {
                        toVarsToPop1.tail =
                                toVarsToPop1.tail.placdl(new TapPair<>(refDescriptorRestore, refDescriptorRestoreDiffPtr));
                    }
                    if (toVarsToPop2 != null) {
                        toVarsToPop2.tail =
                                toVarsToPop2.tail.placdl(new TapPair<>(refDescriptorRestore, refDescriptorRestoreDiffPtr));
                    }
                    subArgsSaveRestore = subArgsSaveRestore.tail;
                }
            }
        }
    }

    /**
     * Finds the list of sub-tree accesses (similarly from roots "treeForSave" and "treeForRestore")
     * that reach a zone which is tbr (or tbrOnDiffPtr).<br>
     * Builds and returns a list of Tree[4], one for each such tbr sub-tree.
     * <p>
     * The 4 elements of this Tree[4] are:<br>
     * 0: Tree built on top of treeForSave which is tbr<br>
     * 1: Tree built on top of treeForRestore which is tbr<br>
     * 2: Tree built on top of treeForSave which is tbrOnDiffPtr<br>
     * 3: Tree built on top of treeForRestore which is tbrOnDiffPtr<br>
     * Takes care of variables with an infinite cycling type.
     *
     * @param treeForSave    the given root Tree upon which every returned expression to save will be constructed.
     * @param treeForRestore the given root Tree upon which every returned expression to restore will be constructed.
     * @param type           the (common) type of the two given root trees.
     * @param tbr            the tree of Booleans giving tbr on components of the given tree.
     * @param tbrOnDiffPtr   the tree of Booleans giving tbr on DIFF pointer components of the given tree.
     * @param collection     the list that accumulates the result. Pass it null.
     * @param dejaVu         the dejaVu mechanism. Pass it null.
     * @return the components of the given tree that must be saved/restored X onPrimal/onDiffPtr.
     */
    private TapList<Tree[]> collectSubTreesToRestore(Tree treeForSave, Tree treeForRestore,
                                                     WrapperTypeSpec type, WrapperTypeSpec actualType,
                                                     TapList tbr, TapList tbrOnDiffPtr,
                                                     TapList<Tree[]> collection, TapList<WrapperTypeSpec> dejaVu) {
        if (type == null || type.wrappedType == null) {
            collection = collectOneSubTreeToRestore(collection, treeForSave, treeForRestore, tbr, tbrOnDiffPtr);
        } else {
            switch (type.wrappedType.kind()) {
                case SymbolTableConstants.ARRAYTYPE: {
                    WrapperTypeSpec actualElementType =
                            TypeSpec.isA(actualType, SymbolTableConstants.ARRAYTYPE)
                                    ? actualType.wrappedType.elementType() : null;
                    collection = collectSubTreesToRestore(treeForSave, treeForRestore,
                            type.wrappedType.elementType(), actualElementType,
                            tbr, tbrOnDiffPtr, collection, dejaVu);
                    break;
                }
                case SymbolTableConstants.MODIFIEDTYPE: {
                    WrapperTypeSpec actualElementType =
                            TypeSpec.isA(actualType, SymbolTableConstants.MODIFIEDTYPE)
                                    ? actualType.wrappedType.elementType() : null;
                    collection = collectSubTreesToRestore(treeForSave, treeForRestore,
                            type.wrappedType.elementType(), actualElementType,
                            tbr, tbrOnDiffPtr, collection, dejaVu);
                    break;
                }
                case SymbolTableConstants.COMPOSITETYPE:
                    if (TapList.contains(dejaVu, type)) {
                        collection = collectOneSubTreeToRestore(collection, treeForSave, treeForRestore, tbr, tbrOnDiffPtr);
                    } else {
                        dejaVu = new TapList<>(type, dejaVu);
                        CompositeTypeSpec compositeType;
                        if (TypeSpec.isA(actualType, SymbolTableConstants.COMPOSITETYPE)) {
                            compositeType = (CompositeTypeSpec) actualType.wrappedType;
                        } else {
                            compositeType = (CompositeTypeSpec) type.wrappedType;
                        }
                        FieldDecl[] fields = compositeType.fields;
                        int maxi = fields.length;
                        Boolean fallback;
                        if (tbr == null || !(tbr.head instanceof TapList)) {
                            fallback = TapList.oneTrue(tbr) ? Boolean.TRUE : Boolean.FALSE;
                            for (int i = maxi; i > 0; --i) {
                                tbr = new TapList<>(new TapList<>(fallback, null), tbr);
                            }
                        }
                        if (tbrOnDiffPtr == null || !(tbrOnDiffPtr.head instanceof TapList)) {
                            fallback = TapList.oneTrue(tbrOnDiffPtr) ? Boolean.TRUE : Boolean.FALSE;
                            for (int i = maxi; i > 0; --i) {
                                tbrOnDiffPtr = new TapList<>(new TapList<>(fallback, null), tbrOnDiffPtr);
                            }
                        }
                        TapList nthTbr, nthTbrOnDiffPtr;
                        Object nthTmp;
                        for (int i = maxi - 1; i >= 0; --i) {

                            nthTmp = TapList.nth(tbr, i);
                            if (nthTmp instanceof TapList) {
                                nthTbr = (TapList) nthTmp;
                            } else
                            //Fallback case: should not happen:
                            {
                                nthTbr = tbr;
                            }
                            nthTmp = TapList.nth(tbrOnDiffPtr, i);
                            if (nthTmp instanceof TapList) {
                                nthTbrOnDiffPtr = (TapList) nthTmp;
                            } else
                            //Fallback case: should not happen:
                            {
                                nthTbrOnDiffPtr = tbrOnDiffPtr;
                            }
                            if (fields[i] != null && !(nthTbr == null && nthTbrOnDiffPtr == null)) {
                                Tree buildTreeSave = ILUtils.build(ILLang.op_fieldAccess,
                                        ILUtils.copy(treeForSave),
                                        ILUtils.build(ILLang.op_ident, fields[i].symbol));
                                ILUtils.setFieldRank(buildTreeSave.down(2), i);
                                Tree buildTreeRestore = ILUtils.build(ILLang.op_fieldAccess,
                                        ILUtils.copy(treeForRestore),
                                        ILUtils.build(ILLang.op_ident, fields[i].symbol));
                                ILUtils.setFieldRank(buildTreeRestore.down(2), i);
                                collection = collectSubTreesToRestore(
                                        buildTreeSave,
                                        buildTreeRestore,
                                        fields[i].type(), null, nthTbr, nthTbrOnDiffPtr, collection, dejaVu);
                            } else { //Fallback case: should not happen:
                                TapEnv.fileWarning(TapEnv.MSG_SEVERE, treeForSave, "(PUSH/POP of a structured object) problem for field " + (i + 1) + " of ref " + treeForSave + " of type " + compositeType.showType());
                            }
                        }
                    }
                    break;
                case SymbolTableConstants.POINTERTYPE: {
                    WrapperTypeSpec actualDestinationType =
                            TypeSpec.isA(actualType, SymbolTableConstants.POINTERTYPE) ? ((PointerTypeSpec) actualType.wrappedType).destinationType : null;
                    PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) type.wrappedType;
                    Tree arrayDimension;
                    int dimSize = -1;
                    if (pointerTypeSpec.offsetLength != null) {
                        dimSize = pointerTypeSpec.offsetLength.size();
                    }
                    if (dimSize == -1 && TypeSpec.isA(actualType, SymbolTableConstants.POINTERTYPE) && ((PointerTypeSpec) actualType.wrappedType).offsetLength != null) {
                        dimSize = ((PointerTypeSpec) actualType.wrappedType).offsetLength.size();
                    }
                    if (dimSize != -1) {
                        arrayDimension = ILUtils.build(ILLang.op_arrayTriplet,
                                ILUtils.build(ILLang.op_intCst, 0),
                                ILUtils.build(ILLang.op_intCst, dimSize - 1),
                                ILUtils.build(ILLang.op_none));
                    } else {
                        arrayDimension = ILUtils.build(ILLang.op_none);
                    }
                    boolean cleanTree = tbr != null && tbr.head instanceof Boolean;
                    TapList pointerTbr =
                            cleanTree ? new TapList<>(tbr.head, null) : new TapList<>(TapList.oneTrue(tbr) ? Boolean.TRUE : Boolean.FALSE, null);
                    TapList pointedTbr =
                            cleanTree ? tbr.tail : new TapList<>(TapList.oneTrue(tbr) ? Boolean.TRUE : Boolean.FALSE, null);
                    cleanTree = tbrOnDiffPtr != null && tbrOnDiffPtr.head instanceof Boolean;
                    TapList pointerTbrOnDiffPtr =
                            cleanTree ? new TapList<>(tbrOnDiffPtr.head, null) : new TapList<>(TapList.oneTrue(tbrOnDiffPtr) ? Boolean.TRUE : Boolean.FALSE, null);
                    TapList pointedTbrOnDiffPtr =
                            cleanTree ? tbrOnDiffPtr.tail : new TapList<>(TapList.oneTrue(tbrOnDiffPtr) ? Boolean.TRUE : Boolean.FALSE, null);
                    //It is important that the pointer is collected BEFORE its destination,
                    // so that the POP(P) comes before the POP(*P) !!
                    collection = collectOneSubTreeToRestore(collection, treeForSave, treeForRestore, pointerTbr, pointerTbrOnDiffPtr);
                    collection = collectSubTreesToRestore(ILUtils.build(ILLang.op_pointerAccess, treeForSave, ILUtils.copy(arrayDimension)),
                            ILUtils.build(ILLang.op_pointerAccess, treeForRestore, arrayDimension),
                            pointerTypeSpec.destinationType, actualDestinationType, pointedTbr, pointedTbrOnDiffPtr,
                            collection, dejaVu);
                    break;
                }
                case SymbolTableConstants.PRIMITIVETYPE:
                default:
                    collection = collectOneSubTreeToRestore(collection, treeForSave, treeForRestore, tbr, tbrOnDiffPtr);
                    break;
            }
        }
        return collection;
    }

    /**
     * Reset to "false" all parts of "inter" that correspond to a pointer (cf always POP pointer BEFORE POP pointer dest, cf set07:v461)
     * Do the same for "interOnDiffPtr", which here amounts to completely setting it to false, but this may be refined later.
     */
    private void removeForPointers(BoolVector inter, BoolVector interOnDiffPtr) {
        ZoneInfo zi;
        int index;
        for (int i = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) - 1; i >= 0; --i) {
            zi = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zi != null && zi.ptrZoneNb != -1) {
                index = zi.kindZoneNb(SymbolTableConstants.ALLKIND);
                inter.set(index, false);
            }
        }
        for (int i = adEnv.curSymbolTable().declaredZonesNb(SymbolTableConstants.PTRKIND) - 1; i >= 0; --i) {
            zi = adEnv.curSymbolTable().declaredZoneInfo(i, SymbolTableConstants.PTRKIND);
            if (zi != null && zi.ptrZoneNb != -1) { // yes, we know it is always true...
                index = zi.kindZoneNb(SymbolTableConstants.PTRKIND);
                interOnDiffPtr.set(index, false);
            }
        }
    }

    /**
     * Reset to "false" all parts of "inter" that correspond to a pointer (cf always POP pointer BEFORE POP pointer dest, cf set07:v461).
     * Do the same for "interOnDiffPtr".
     */
    private void removeForPointers(TapList inter, TapList interOnDiffPtr) {
        removeForPointersRec(inter, new TapList<>(null, null));
        removeForPointersRec(interOnDiffPtr, new TapList<>(null, null));
    }

    private void removeForPointersRec(TapList inter, TapList<TapList> dejaVu) {
        while (inter != null) {
            if (TapList.contains(dejaVu.tail, inter)) {
                inter = null;
            } else {
                dejaVu.placdl(inter);
                if (inter.head instanceof TapList) {
                    removeForPointersRec((TapList) inter.head, dejaVu);
                } else if (inter.tail != null) { // we are on a pointer
                    // Rationale: each time the TapList is about a pointer,
                    // if currently holds TRUE and not all pointed zones are also TRUE,
                    // then reset to FALSE both pointer AND all pointed zones.
                    if (inter.head == Boolean.TRUE) {
                        if (TapList.oneFalse(inter.tail)) {
                            inter.head = Boolean.FALSE;
                            inter.tail = null;
                        }
                    }
                }
                inter = inter.tail;
            }
        }
    }

    private static TapList<Tree[]> collectOneSubTreeToRestore(TapList<Tree[]> collection,
                                                              Tree treeForSave, Tree treeForRestore, TapList tbr, TapList tbrOnDiffPtr) {
        boolean isTbr = TapList.oneTrue(tbr);
        boolean isTbrOnDiffPtr = TapList.oneTrue(tbrOnDiffPtr);
        if (isTbr || isTbrOnDiffPtr) {
            Tree[] component = {isTbr ? treeForSave : null, isTbr ? treeForRestore : null,
                    isTbrOnDiffPtr ? treeForSave : null, isTbrOnDiffPtr ? treeForRestore : null};
            return new TapList<>(component, collection);
        } else {
            return collection;
        }
    }

    /**
     * Fill the "zoneInfo" field of the pushed and popped RefDescriptors,
     * so that Tapenade may emit a warning message (AD15) about "Not sure how to push/pop"
     * because the subArgTree may or may not be actually an array.
     * (e.g. in C, a float* x may be a pointer to a float or to an array of floats)
     */
    private void allowForUnsureWarningMessage(Tree subArgTree, RefDescriptor savedDescriptor, RefDescriptor restoredDescriptor) {
        TapIntList lz = adEnv.curSymbolTable().listOfZonesOfValue(subArgTree, null, adEnv.curInstruction());
        if (lz != null) {
            ZoneInfo zoneInfoOfSubArg =
                    adEnv.curSymbolTable().declaredZoneInfo(lz.head, SymbolTableConstants.ALLKIND);
            savedDescriptor.zoneInfo = zoneInfoOfSubArg;
            restoredDescriptor.zoneInfo = zoneInfoOfSubArg;
        }
    }

    /**
     * Compute a differentiated call, for the header Tree in the EntryBlock of a diffUnit
     * (OR for a differentiated call in the special case of the ADJOINT_SPLIT call FWD
     * OR for a differentiated call from a context procedure).
     *
     * @param headerTree      The primal header. Its head operator is ILLang.op_call.
     * @param lhs             The actual variable that receives the result of the call. Should be non-null
     *                        ONLY when differentiateProcedureHeader() is called on a FUNCTION CALL.
     * @param toDiffLhs       When given non-null, holds upon exit the future lhs of the differentiated call.
     *                        Should be non-null ONLY when differentiateProcedureHeader() is called on a FUNCTION CALL.
     * @param calledActivity  The ActivityPattern of the routine refered to by this function call.
     *                        Should be non-null ONLY when differentiateProcedureHeader() is called on a function CALL
     *                        otherwise should be null and curActivity will be used.
     * @param diffSymbolTable The SymbolTable that will host the returned differentiated Tree.
     * @param diffHeaderMode  The current differentiation mode, one of
     *                        {ORIGCOPY, TANGENT, ADJOINT, ADJOINT_FWD, ADJOINT_BWD}
     */
    protected Tree differentiateProcedureHeader(Tree headerTree, Tree lhs, ToObject<Tree> toDiffLhs,
                                                ActivityPattern calledActivity,
                                                SymbolTable diffSymbolTable, SymbolTable interfaceSymbolTable,
                                                int diffHeaderMode, TapList<TapPair<Tree, Tree>> toDuplicateVars) {
        if (calledActivity == null) {
            //i.e. always except when this is not really a header, but a call (very special case):
            calledActivity = adEnv.curActivity();
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("          (Differentiate Header): " + headerTree + " in diff mode " + diffHeaderMode + " for activity:" + calledActivity + " isActive:" + calledActivity.isActive() + " given diffSymbolTable:" + diffSymbolTable.addressChain());
            TapEnv.printlnOnTrace("             pretty-printed as : " + ILUtils.toString(headerTree));
        }
        Tree calledNameTree = ILUtils.getCalledName(headerTree);
        String calledNameString = ILUtils.getIdentString(calledNameTree);
        Unit sourceCalledUnit = calledActivity.unit();
        if (sourceCalledUnit == null) {
            sourceCalledUnit = adEnv.curUnit();
        }
        FunctionTypeSpec functionTypeSpec = sourceCalledUnit.functionTypeSpec();
        WrapperTypeSpec[] argumentsTypes = functionTypeSpec.argumentsTypes;
        boolean isActive = calledActivity.isActive();
        boolean isMainProcedure = sourceCalledUnit == adEnv.srcCallGraph().getMainUnit();
        UnitDiffInfo calledDiffInfo = callGraphDifferentiator().getUnitDiffInfo(sourceCalledUnit);
        Unit diffCalledUnit = (diffHeaderMode == DiffConstants.ORIGCOPY
                ? adEnv.getPrimalCopyOfOrigUnit(sourceCalledUnit)
                : adEnv.getDiffOfUnit(sourceCalledUnit, calledActivity, diffHeaderMode));
        SymbolTable symbolTable = adEnv.curUnit().publicSymbolTable();
        Tree diffHeader;
        Tree explicitDiffReturnVar = null;
        boolean[] formalArgsActivity = null;
        if (isMainProcedure) {
            diffHeader = ILUtils.copy(headerTree);
            // Except in C, we may give a new, differentiated name to the differentiated main procedure
            // In C, the (context) differentiated main() must always be called "main".
            if (!sourceCalledUnit.isC()) {
                Tree unitName = varRefDifferentiator().diffSymbolTree(calledActivity, calledNameTree, diffSymbolTable,
                        false, true, false, headerTree,
                        false, diffHeaderMode, diffHeaderMode, DiffConstants.PROC, null);
                diffHeader.setChild(unitName, 1);
            }
        } else {
            Tree[] args = ILUtils.getArguments(headerTree).children();
            int nbArgs = args.length;
            // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
            boolean[] diffArgsByValueNeedOverwrite = null;
            boolean[] diffArgsByValueNeedOnlyIncrement = null;
            if (diffHeaderMode != DiffConstants.TANGENT) {
                diffArgsByValueNeedOverwrite = calledDiffInfo.getUnitDiffArgsByValueNeedOverwriteInfoS(calledActivity);
                diffArgsByValueNeedOnlyIncrement = calledDiffInfo.getUnitDiffArgsByValueNeedOnlyIncrementInfoS(calledActivity);
            }
            // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.

            formalArgsActivity = callGraphDifferentiator().getUnitFormalArgsActivityS(calledActivity);
// This fix in case of mismatching nbArgs was harmful. Better not do it and be more careful later:
//             nbArgs = Math.min(nbArgs, formalArgsActivity.length - 1);
            boolean[] formalArgsPointerActivity = ReqExplicit.formalArgsPointerActivity(calledActivity, nbArgs);
            if (isActive && adEnv.traceCurDifferentiation) {
                boolean[] formalArgsInfo;
                TapEnv.printOnTrace("          formalArgsActivity: [");
                formalArgsInfo = formalArgsActivity;
                for (int j = 0; j < formalArgsInfo.length; ++j) {
                    if (formalArgsInfo.length == 1) {
                        TapEnv.printOnTrace("->");
                    }
                    TapEnv.printOnTrace("" + formalArgsInfo[j]);
                    if (j + 1 < formalArgsInfo.length) {
                        TapEnv.printOnTrace(j + 1 == nbArgs ? "->" : ", ");
                    }
                }
                TapEnv.printlnOnTrace("]");
                TapEnv.printOnTrace("          formalArgsPointerActivity: [");
                formalArgsInfo = formalArgsPointerActivity;
                for (int j = 0; j < formalArgsInfo.length; ++j) {
                    if (formalArgsInfo.length == 1) {
                        TapEnv.printOnTrace("->");
                    }
                    TapEnv.printOnTrace("" + formalArgsInfo[j]);
                    if (j + 1 < formalArgsInfo.length) {
                        TapEnv.printOnTrace(j + 1 == nbArgs ? "->" : ", ");
                    }
                }
                TapEnv.printlnOnTrace("]");
            }
            int firstOrderingProblem = findNameEqOrNone(headerTree);
            TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionInfos = calledActivity.varFunctionADActivities();
            TapList<Tree> newArgs = null;
            if (isActive && multiDirMode() && TapEnv.get().fixedNbdirsTree==null) {
                Tree newArg = varRefDifferentiator().buildDirNumberReference(diffSymbolTable);
                if (firstOrderingProblem > -1) {
                    // this is wrong: the name of the arg must come from the called diff routine !
                    Tree argName = varRefDifferentiator().buildDirNumberReference(diffSymbolTable);
                    newArg = ILUtils.build(ILLang.op_nameEq, argName, newArg);
                }
                newArgs = new TapList<>(newArg, newArgs);
            }
            // Differentiating the return argument/value:
            if (sourceCalledUnit.isAFunction() && !TapEnv.associationByAddress()) {
                Tree returnTree;
                if (lhs != null) {
                    //Case where this is not really a header, but a call (this is the dirty case):
                    returnTree = ILUtils.copy(lhs);
                } else if (sourceCalledUnit.otherReturnVar() != null) {
                    // Note: the "otherReturnVar" is a Fortran-specific thing. Still, it seems it appears
                    //  in a C example (set09/v162), probably wrongly but this is not yet fixed.
                    // So we did not restrict the test above to the Fortran case.
                    returnTree = ILUtils.build(ILLang.op_ident, sourceCalledUnit.otherReturnVar().symbol);
                } else {
                    returnTree = ILUtils.copy(calledNameTree);
                }
                // Cases where there is a derivative of the function return:
                boolean returnIsActive =
                        ((diffHeaderMode == DiffConstants.ADJOINT_FWD || calledActivity.isContext())
                                ? formalArgsPointerActivity[nbArgs]
                                : formalArgsActivity[nbArgs]);
                boolean diffFunctionIsFunction = diffFunctionIsFunction(adEnv.curUnit());
                if (adEnv.traceCurDifferentiation) {
                    System.out.println("          returnTree:" + returnTree + (returnIsActive ? " is ACTIVE" : " is PASSIVE")
                            + " for curUnit:" + adEnv.curUnit() + " in diffMode:" + diffHeaderMode + "(multi:" + multiDirMode() + " ,context:" + calledActivity.isContext() + ") dFisF:" + diffFunctionIsFunction);
                }
                if (returnIsActive) {
                  Tree[] diffReturnVarTreeR = new Tree[TapEnv.diffReplica()] ;
                  for (adEnv.iReplic=0 ; adEnv.iReplic<diffReturnVarTreeR.length ; ++adEnv.iReplic) {
                    Tree diffReturnVarTree = null;
                    if (ILUtils.isNullOrNone(returnTree) && functionTypeSpec.isAFunction()) {
                        diffReturnVarTree = functionTypeSpec.returnType.wrappedType.buildConstantZero();
                    } else {
                        NewSymbolHolder diffReturnVarSH = null;
                        // The present diffReturnVarTree is not always used in the sequel. Better Create it only if needed:
                        if (diffHeaderMode != DiffConstants.TANGENT
                            || lhs != null
                            || !diffFunctionIsFunction
                            || (sourceCalledUnit.isFortran() && sourceCalledUnit.otherReturnVar() != null)) {
                            if (returnTree.opCode() == ILLang.op_ident) {
                                diffReturnVarTree = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(),
                                        returnTree, diffSymbolTable, true, false, false, headerTree, true, curDiffVarSort(),
                                        (lhs != null ? DiffConstants.ADJOINT : diffHeaderMode), DiffConstants.VAR, null);
                            } else {
                                diffReturnVarTree = varRefDifferentiator().diffVarRef(adEnv.curActivity(), returnTree, diffSymbolTable,
                                        false, returnTree, false, false, returnTree);
                            }
                            diffReturnVarSH = NewSymbolHolder.getNewSymbolHolder(diffReturnVarTree);
                            if (diffReturnVarSH != null && diffReturnVarSH.newVariableDecl() != null) {
                                // Various labelings on the diff return variable:
                                if (diffHeaderMode == DiffConstants.TANGENT) {
                                    // Only in tangent, the diff return tree may be labelled as "OUT":
                                    if (sourceCalledUnit.isFortran9x() && sourceCalledUnit.otherReturnVar() != null) {
                                        if (!diffCalledUnit.isAFunction()) {
                                            // Don't declare the future official function return as "OUT":
                                            diffReturnVarSH.setExtraInfo(
                                                    SymbolDecl.addInfosInList(diffReturnVarSH.extraInfo(),
                                                            new TapList<>("out", null), null));
                                            diffReturnVarSH.newVariableDecl().addExtraInfo(diffReturnVarSH.extraInfo());
                                        }
                                        diffReturnVarSH.newVariableDecl().formalArgRank = 0;
                                        diffReturnVarSH.derivationFrom.addExtraInfo(new TapList<>("out", null));
                                    }
                                } else if (sourceCalledUnit.isFortran2003()) {
                                    // ajoute le modifier "VALUE" pour le parametre "return adjoint":
                                    diffReturnVarSH.setExtraInfo(
                                            SymbolDecl.addInfosInList(diffReturnVarSH.extraInfo(),
                                                    new TapList<>("value", null), null));
                                    diffReturnVarSH.newVariableDecl().addExtraInfo(diffReturnVarSH.extraInfo());
                                }
                            }
                        }
                        if (adEnv.traceCurDifferentiation) {
                            System.out.println("          RETURN IS ACTIVE. CURUNIT:" + adEnv.curUnit()
                                    + " otherReturnVar:" + sourceCalledUnit.otherReturnVar() + " diffReturnVarTree:" + diffReturnVarTree);
                        }
                        // Dealing with the Fortran-style alternate result name:
                        if (diffReturnVarTree != null && adEnv.curUnit().isAFunction() && sourceCalledUnit.isFortran()//  &&  sourceCalledUnit.otherReturnVar() != null
                                && diffCalledUnit.isAFunction()) {
                            explicitDiffReturnVar = ILUtils.copy(diffReturnVarTree);
                            if (diffReturnVarSH != null) {
                                diffCalledUnit.setOtherReturnVar(diffReturnVarSH.newVariableDecl());
                                // Just to tell there is no need for a declaration instruction because the header does it:
                                diffReturnVarSH.newVariableDecl().setInstruction(new Instruction(ILUtils.build(ILLang.op_call)));
                            }
                        }
                    }
                    diffReturnVarTreeR[adEnv.iReplic] = diffReturnVarTree ;
                  }
                  adEnv.iReplic=0;//Temporary until replicas are treated everywhere...

                  if ((calledActivity.isContext() || diffHeaderMode != DiffConstants.ADJOINT)
                            && diffHeaderMode != DiffConstants.ADJOINT_BWD) {
                      // "forward-style" diff header: result becomes a new (reference) argument, diffResult is the new result.
                      if (diffFunctionIsFunction) {
                            if (toDiffLhs != null) {
                                toDiffLhs.setObj(formalArgsPointerActivity[nbArgs]
                                        ? diffReturnVarTreeR[0]
                                        : ILUtils.copy(returnTree));
                            }
                      } else {
                          // If we nevertheless decide that the diffResult cannot be returned as the result
                          // (this happens e.g. in multi-dir modes), then we must pass it as an argument:
                          for (int iReplic=diffReturnVarTreeR.length-1 ; iReplic>=0 ; --iReplic) {
                            Tree diffReturnVarTree = diffReturnVarTreeR[iReplic] ;
                            if (curDiffUnit().isC() && diffReturnVarTreeR.length>1) {
                                NewSymbolHolder diffReturnVarSH =
                                    NewSymbolHolder.getNewSymbolHolder(diffReturnVarTree);
                                if (diffReturnVarSH != null) {
                                    VariableDecl diffReturnVarDecl = diffReturnVarSH.newVariableDecl() ;
                                    if (diffReturnVarDecl!= null) {
                                        diffReturnVarDecl.turnTypeIntoPointer() ;
                                    }
                                }
                                diffReturnVarTree = ILUtils.build(ILLang.op_pointerDeclarator, diffReturnVarTree) ;
                            }
                            if (firstOrderingProblem > -1 && lhs != null) {
                                Tree diffResultArgName =
                                        getNewParamName(-1, sourceCalledUnit, true, diffHeaderMode, diffCalledUnit);
                                diffReturnVarTree = ILUtils.build(ILLang.op_nameEq, diffResultArgName, diffReturnVarTree);
                            }
                            newArgs = new TapList<>(diffReturnVarTree, newArgs);
                          }
                      }
                      Tree treeForSourceResult = returnTree;
                      if (curDiffUnit().isC()) {
                            if (lhs != null) {
                                if (TypeSpec.isA(adEnv.curSymbolTable().typeOf(lhs), SymbolTableConstants.POINTERTYPE)) {
                                    // Case of a function that returns a pointer, i.e. returns some "control":
                                    // the fwd diff function must return the differentiated pointer,
                                    // PLUS the original pointer as a new "&" argument:
                                    treeForSourceResult = ILUtils.addAddressOf(treeForSourceResult);
                                }
                            } else {
                                if (treeForSourceResult != null) {
                                    treeForSourceResult = ILUtils.build(ILLang.op_pointerDeclarator, treeForSourceResult);
                                } else {
                                    treeForSourceResult = functionTypeSpec.returnType.wrappedType.buildConstantZero();
                                }
                                String returnName = ILUtils.baseName(returnTree);
                                VariableDecl returnVarDecl = diffSymbolTable.getTopVariableDecl(returnName);
                                if (returnVarDecl == null) {
                                    returnVarDecl = interfaceSymbolTable.getTopVariableDecl(returnName);
                                }
                                if (returnVarDecl != null) {
                                    returnVarDecl.turnTypeIntoPointer();
                                }
                            }
                        }
                        if (firstOrderingProblem > -1 && lhs != null) {
                            Tree diffResultArgName =
                                    getNewParamName(-1, sourceCalledUnit, false, diffHeaderMode, diffCalledUnit);
                            treeForSourceResult = ILUtils.build(ILLang.op_nameEq, diffResultArgName, treeForSourceResult);
                        }
                        newArgs = new TapList<>(treeForSourceResult, newArgs);
                  } else {
                        // "backward-style" diff header: result disappears, diffResult becomes a new argument:
                      for (int iReplic=diffReturnVarTreeR.length-1 ; iReplic>=0 ; --iReplic) {
                        Tree diffReturnVarTree = diffReturnVarTreeR[iReplic] ;
                        if (firstOrderingProblem > -1 && lhs != null) {
                            Tree diffResultArgName =
                                    getNewParamName(-1, sourceCalledUnit, true, diffHeaderMode, diffCalledUnit);
                            diffReturnVarTree = ILUtils.build(ILLang.op_nameEq, diffResultArgName, diffReturnVarTree);
                        }
                        newArgs = new TapList<>(diffReturnVarTree, newArgs);
                      }
                  }
                } else {
                    if (adEnv.traceCurDifferentiation) {
                        System.out.println("          RETURN IS PASSIVE. CURUNIT:" + adEnv.curUnit()
                                + " otherReturnVar:" + sourceCalledUnit.otherReturnVar() + " returnTree:" + returnTree);
                    }
                    if (adEnv.curUnit().isAFunction() && diffCalledUnit.isAFunction()
                        // Don't check declarations of return variable for the "dirty" case of a _fwd or a context _b call
                        //  (the returnTree==lhs may be anything)
                        && lhs==null) {
                        String primalReturnVarName = returnTree.stringValue();
                        VariableDecl primalReturnVarDecl =
                            (primalReturnVarName==null ? null :diffSymbolTable.getVariableDecl(primalReturnVarName));
                        if (primalReturnVarDecl != null) {
                            if (diffCalledUnit.isFortran()) {
                                explicitDiffReturnVar = ILUtils.copy(returnTree);
                                diffCalledUnit.setOtherReturnVar(primalReturnVarDecl);
                                // Just to tell there is no need for a declaration instruction because the header does it:
                                primalReturnVarDecl.setInstruction(new Instruction(ILUtils.build(ILLang.op_call)));
                            } else {
                                // C case: in reverse mode, the return instruction will be split
                                // (as an assignment to a new variable, then finally return this variable,
                                //  cf splitForADReverse()). Therefore we must add a local var declaration
                                // with the original name of the primal function.
                                if (diffHeaderMode != DiffConstants.TANGENT) {
                                    Tree primalReturnDeclTree =
                                      ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        functionTypeSpec.returnType.wrappedType.generateTree(null, null, null, true, null),
                                        ILUtils.build(ILLang.op_declarators,
                                          ILUtils.copy(returnTree))) ;
                                    Instruction primalReturnDeclaration = new Instruction(primalReturnDeclTree) ;
                                    diffSymbolTable.declarationsBlock.addInstrTl(primalReturnDeclaration) ;
                                    // Declare primal return if diffHeaderMode==DiffConstants.ADJOINT_FWD
                                    primalReturnVarDecl.setInstruction(primalReturnDeclaration) ;
                                }
                            }
                        }
                    }
                }
            }
            varRefDifferentiator().pushDiffContext(DiffConstants.INHEADER);
            for (int i = nbArgs - 1; i >= 0; i--) {
                Tree diffArg = null;
                Tree[] diffArgR ;
                if (symbolTable.identIsAFunction(args[i])) {
                    if (diffHeaderMode != DiffConstants.ADJOINT_FWD &&
                            (!TapEnv.doActivity() || varFunctionInfos != null && varFunctionInfos[i] != null)) {
                        // If this argument is a function name which is active,
                        // use DifferentiationConstants.ADJOINT because a VarFunction cannot be adjoined in split mode.
                        int subDiffSort = diffHeaderMode == DiffConstants.ADJOINT_BWD ? DiffConstants.ADJOINT : diffHeaderMode;
                        TapList<ActivityPattern> functionActivities =
                                ((FunctionDecl) symbolTable.getSymbolDecl(ILUtils.getIdentString(args[i]))).unit().activityPatterns;
                        // A function passed as argument is allowed only one (not connected) activity pattern:
                        diffArg = varRefDifferentiator().diffSymbolTree(
                                functionActivities != null ? functionActivities.head : null,
                                args[i], diffSymbolTable, false, true, false,
                                null, false, curDiffUnitSort(), subDiffSort, DiffConstants.VAR, null);
                    }
                    if (diffArg==null) {
                        diffArgR = new Tree[0] ;
                    } else {
                        diffArgR = new Tree[]{diffArg} ;
                    }
                } else if (diffHeaderMode == DiffConstants.ADJOINT_FWD ? formalArgsPointerActivity[i] : formalArgsActivity[i]) {
                    // TODO: compute the new INTENT of this argument.
                    // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                    if (ILUtils.isNullOrNone(args[i])) {
                        // This may happen for absent active optional args cf F90:lha60
                        diffArg = ILUtils.build(ILLang.op_none);
                        diffArgR = new Tree[]{diffArg} ;
                    } else if (TapEnv.associationByAddress()) {
                        diffArg = ILUtils.copy(args[i]);
                        diffArgR = new Tree[]{diffArg} ;
                    } else if (diffArgsByValueNeedOverwrite != null
                               && diffArgsByValueNeedOverwrite[i + 1]
                               && sourceCalledUnit.takesArgumentByValue(i + 1, adEnv.curUnit().language())) {
                      diffArgR = new Tree[TapEnv.diffReplica()] ;
                      for (adEnv.iReplic=0 ; adEnv.iReplic<diffArgR.length ; ++adEnv.iReplic) {
                        if (diffArgsByValueNeedOnlyIncrement[i + 1] || adEnv.curUnitIsContext) {
                            diffArg = varRefDifferentiator().diffVarRef(adEnv.curActivity(), args[i], diffSymbolTable,
                                    false, args[i], false, true, args[i]);
                            if (adEnv.curUnit().isFortran2003()) {
                                cleanExtraInfoValue(diffArg);
                            }
                            if (adEnv.curUnitIsContext) {
                                diffArg = ILUtils.addAddressOf(diffArg);
                            }
                        } else {
                            SymbolTable diffPrivateSymbolTable = curDiffUnit().privateSymbolTable();
                            //Done NOW to force that the DEFAULT diffVar is named BEFORE the
                            // ALT_DIFF_VAR, so that the DEFAULT is "xd" and the ALT_DIFF_VAR is "xd0" !!
                            Tree diffArgLocal =
                                    varRefDifferentiator().diffVarRef(adEnv.curActivity(), args[i], diffPrivateSymbolTable,
                                            false, args[i], false, true, args[i]);
                            Tree newDeclTree = ILUtils.baseTree(diffArgLocal);
                            Instruction instrDecl = new Instruction();
                            instrDecl.setPosition(adEnv.curInstruction());
                            WrapperTypeSpec typeSpec = diffPrivateSymbolTable.typeOf(newDeclTree);
                            Tree typeSpecTree = null;
                            // typeSpec == null si interface et procedure header different
                            if (typeSpec != null) {
                                typeSpecTree =
                                        typeSpec.wrappedType.generateTree(diffSymbolTable, null, null, true, null);
                            }
                            instrDecl.tree = ILUtils.build(ILLang.op_varDeclaration,
                                    ILUtils.build(ILLang.op_modifiers),
                                    typeSpecTree,
                                    ILUtils.build(ILLang.op_declarators,
                                            ILUtils.copy(newDeclTree)));
                            // on n'est pas en train de differentier un op_interfaceDecl
                            if (diffSymbolTable == interfaceSymbolTable) {
                                NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(newDeclTree);
                                sHolder.setInstruction(instrDecl);
                                sHolder.setVarDeclTreeAlreadyPlacedFor(adEnv.curUnit().publicSymbolTable());
                            }
                            diffArgLocal = ILUtils.variableDeclaratorToVariableRef(diffArgLocal);
                            //Temporary to force PASS-BY-REFERENCE style for the (ALT) formal parameter:
                            diffArgsByValueNeedOnlyIncrement[i + 1] = true;
                            adEnv.pushCurDiffSorts(DiffConstants.UNDEFSORT, DiffConstants.ALT_DIFF_VAR);
                            diffArg =
                                    varRefDifferentiator().diffVarRef(adEnv.curActivity(), args[i], diffSymbolTable,
                                            false, args[i], false, true, args[i]);
                            Tree diffArgForLastUpdate =
                                    varRefDifferentiator().diffVarRef(adEnv.curActivity(), args[i], diffPrivateSymbolTable,
                                            false, args[i], false, true, args[i]);
                            if (adEnv.curUnit().isFortran2003()) {
                                cleanExtraInfoValue(diffArgLocal);
                                cleanExtraInfoValue(diffArgForLastUpdate);
                            }
                            adEnv.popCurDiffSorts();
                            diffArgForLastUpdate = ILUtils.variableDeclaratorToVariableRef(diffArgForLastUpdate);
                            if (toDuplicateVars != null) {
                                toDuplicateVars.placdl(new TapPair<>(diffArgForLastUpdate, diffArgLocal));
                            }
                            diffArgsByValueNeedOnlyIncrement[i + 1] = false;
                        }
                        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
                        diffArgR[adEnv.iReplic] = diffArg ;
                      }
                    } else {
                      diffArgR = new Tree[TapEnv.diffReplica()] ;
                      for (adEnv.iReplic=0 ; adEnv.iReplic<diffArgR.length ; ++adEnv.iReplic) {
                        diffArg = varRefDifferentiator().diffVarRef(adEnv.curActivity(), args[i], diffSymbolTable,
                                false, args[i], false, true, args[i]);
                        if (diffHeaderMode != DiffConstants.TANGENT
                                && adEnv.curUnit().isFortran2003()
                                && sourceCalledUnit.takesArgumentByValue(i + 1, adEnv.curUnit().language())) {
                            cleanExtraInfoValue(diffArg);
                        }
                        diffArgR[adEnv.iReplic] = diffArg ;
                      }
                    }
                } else {
                    diffArgR = new Tree[0] ;
                }
                for (adEnv.iReplic=diffArgR.length-1 ; adEnv.iReplic>=0 ; --adEnv.iReplic) {
                  diffArg = diffArgR[adEnv.iReplic] ;
                  if (!ILUtils.isNullOrNone(diffArg)) {
                    if (firstOrderingProblem != -1 &&
                            (i >= firstOrderingProblem || args[i].getAnnotation("sourcetree") != null)) {
                        Tree argName = getNewParamName(i, sourceCalledUnit, true,
                                diffHeaderMode, diffCalledUnit);
                        diffArg = ILUtils.build(ILLang.op_nameEq, argName, diffArg);
                    }
                    newArgs = new TapList<>(diffArg, newArgs);
                  }
                }
                adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
                if (diffHeaderMode == DiffConstants.ADJOINT_FWD
                        ||
                        !(TapEnv.associationByAddress()
                                && formalArgsActivity[i]
                                ||
                                (diffHeaderMode == DiffConstants.ADJOINT || diffHeaderMode == DiffConstants.ADJOINT_BWD)
                                        && args[i].opCode() == ILLang.op_star
                        )
                ) {
                    Tree copyArg = ILUtils.copy(args[i]);
                    // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                    if (adEnv.curUnit().isC() && !ILUtils.isNullOrNone(copyArg)
                            && diffArgsByValueNeedOverwrite != null && diffArgsByValueNeedOverwrite[i + 1]
                            && TypeSpec.isA(argumentsTypes[i], SymbolTableConstants.POINTERTYPE)) {
                        copyArg = ILUtils.build(ILLang.op_pointerDeclarator, copyArg);
                    }
                    // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.
                    if (args[i].getAnnotation("sourcetree") != null) {
                        copyArg = ILUtils.copy(args[i].getAnnotation("sourcetree"));
                    } else if (firstOrderingProblem != -1 && i >= firstOrderingProblem
                            && args[i].opCode() != ILLang.op_none) {
                        Tree argName;
                        if (copyArg.opCode() == ILLang.op_nameEq) {
                            argName = copyArg.cutChild(1);
                            copyArg = copyArg.cutChild(2);
                        } else {
                            argName = getNewParamName(i, sourceCalledUnit, false, -1, null);
                        }
                        copyArg = ILUtils.build(ILLang.op_nameEq, argName, copyArg);
                    }
                    // TODO: compute the new INTENT of this argument.
                    newArgs = new TapList<>(copyArg, newArgs);
                }
            }
            varRefDifferentiator().popDiffContext();
            boolean alsoAsVariable =  TapEnv.associationByAddress() ;
            Tree unitName = varRefDifferentiator().diffSymbolTree(calledActivity, calledNameTree, diffSymbolTable,
                    alsoAsVariable, true, false, headerTree,
                    false, diffHeaderMode, diffHeaderMode, DiffConstants.PROC, null);
            NewSymbolHolder newSH = NewSymbolHolder.getNewSymbolHolder(unitName);
            // NOTE: newSH should NOT be null. newSH==null means that the final unitName was solved too early!
            if (newSH != null) {
                if (diffCalledUnit != null) {
                    newSH.declarationLevelMustInclude(diffCalledUnit.publicSymbolTable());
                } else {
                    newSH.declarationLevelMustInclude(diffSymbolTable);
                }
            }

            if (explicitDiffReturnVar != null) {
                unitName.setAnnotation("explicitReturnVar", explicitDiffReturnVar);
            }

            // Differentiate the corresponding C diff name, when BIND(C,name="name_in_c")
            Tree bind = calledNameTree.getAnnotation("bind");
            if (!ILUtils.isNullOrNone(bind)) {
                Tree diffBind = ILUtils.copy(bind);
                Tree diffNameC = null;
                if (diffBind.opCode() == ILLang.op_accessDecl) {
                    Tree[] diffBindExprs = diffBind.down(2).children();
                    Tree diffBindNameTree = null;
                    for (int i = 0; i < diffBindExprs.length && diffBindNameTree == null; ++i) {
                        if (diffBindExprs[i].opCode() == ILLang.op_nameEq
                                && ILUtils.isIdent(diffBindExprs[i].down(1), "name", false)) {
                            diffBindNameTree = diffBindExprs[i];
                        }
                    }
                    if (diffBindNameTree != null) {
                        diffNameC =
                                varRefDifferentiator().diffSymbolName(calledActivity,
                                        diffBindNameTree.down(2).stringValue(),
                                        adEnv.diffCallGraph().cRootSymbolTable(),
                                        false, true, false, null, null,
                                        false, diffHeaderMode, diffHeaderMode, DiffConstants.PROC, null);
                        diffBindNameTree.setChild(diffNameC, 2);
                    }
                }
                unitName.setAnnotation("bind", diffBind);

                // Do the same about the diff Unit modifiers:
                // [llh] why this redundancy between Unit.modifiers and annotation:"bind" ?
                if (diffCalledUnit != null && !ILUtils.isNullOrNone(diffCalledUnit.modifiers)) {
                    bind = diffCalledUnit.modifiers;
                    diffBind = ILUtils.copy(bind);
                    if (diffBind.opCode() == ILLang.op_accessDecl) {
                        Tree[] diffBindExprs = diffBind.down(2).children();
                        Tree diffBindNameTree = null;
                        for (int i = 0; i < diffBindExprs.length && diffBindNameTree == null; ++i) {
                            if (diffBindExprs[i].opCode() == ILLang.op_nameEq
                                    && ILUtils.isIdent(diffBindExprs[i].down(1), "name", false)) {
                                diffBindNameTree = diffBindExprs[i];
                            }
                        }
                        if (diffBindNameTree != null) {
                            diffBindNameTree.setChild(ILUtils.copy(diffNameC), 2);
                        }
                    }
                    diffCalledUnit.modifiers = diffBind;
                }
            }

            VariableDecl funVar = diffSymbolTable.getTopVariableDecl(calledNameString);
            if (funVar != null) {
                if (funVar.formalArgRank == SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK) {
                    funVar.formalArgRank = SymbolTableConstants.NOT_A_FORMAL_ARG;
                }
            }
            if (lhs == null) {
                // ^ Don't update formalArgRank's if not really differentiating a header
                updateFormalArgRankS(newArgs, diffSymbolTable);
            }
            diffHeader = ILUtils.buildCall(unitName,
                    ILUtils.build(ILLang.op_expressions, newArgs));
        }
        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
            // [llh] This doesn't look like a right place to update the types in the differentiated public SymbolTable,
            // since these types may have already been updated before for some of these public VariableDecl's.
            Tree[] args = ILUtils.getArguments(headerTree).children();
            int nbArgs = args.length;
            for (int i = 0; i < nbArgs; i++) {
                Tree diffArgTree = diffHeader.down(3).down(i + 1);
                if (diffArgTree != null && !ILUtils.isNullOrNoneOrStar(args[i])) {
                    String diffArgName = ILUtils.baseName(diffArgTree);
                    if (diffArgName != null) {
                        SymbolDecl diffSymbolDecl = diffSymbolTable.getSymbolDecl(diffArgName);
                        if (diffSymbolDecl instanceof VariableDecl) {
                            VariableDecl diffVarDecl = (VariableDecl) diffSymbolDecl;
                            VariableDecl srcVarDecl = null;
                            WrapperTypeSpec diffTypeSpec = null;
                            if (formalArgsActivity != null && formalArgsActivity[i]) {
                                srcVarDecl = (VariableDecl) interfaceSymbolTable.getSymbolDecl(diffArgName);
                                diffTypeSpec =
                                        srcVarDecl.type().differentiateTypeSpecMemo(
                                                diffSymbolTable, interfaceSymbolTable, curDiffUnitSort(),
                                                adEnv.suffixes[DiffConstants.DIFFERENTIATED][DiffConstants.TYPE], false, multiDirMode(),
                                                varRefDifferentiator().getMultiDirDimensionMax(),
                                                adEnv.curUnit().name() + "'s arg" + (i + 1), adEnv.curUnit().name() + "_arg" + (i + 1), null, null);

                                if (diffTypeSpec != null) {
                                    diffVarDecl.setType(diffTypeSpec);
                                }
                            } else {
                                diffVarDecl.setType(diffCalledUnit.functionTypeSpec().argumentsTypes[i]);
                            }
                        } else if (diffSymbolDecl instanceof FunctionDecl) {
                            FunctionDecl diffFunctionDecl = (FunctionDecl) diffSymbolDecl;
                            // TODO?
                        }
                    }
                }
            }
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("          -> (Differentiated Header): " + diffHeader + " toDiffLhs:" + toDiffLhs);
            TapEnv.printlnOnTrace("             pretty-printed as : " + ILUtils.toString(diffHeader));
            TapEnv.printlnOnTrace();
        }
        return diffHeader;
    }

    /**
     * @return the name of the (differentiated if "diff" is true) argument number i
     * (starting at 0, and -1 means function result) as it is named in the called procedure.
     */
    private Tree getNewParamName(int i, Unit calledUnit,
                                 boolean diff, int diffSort, Unit diffCalledUnit) {
        String paramName = "UndefinedArgumentName";
        Tree nameTree;
        Tree headTree = calledUnit.entryBlock() == null ? null : calledUnit.entryBlock().headTree();
        if (i == -1) {
            if (!diff || curDiffUnitSort() == DiffConstants.TANGENT && !multiDirMode()) {
                if (calledUnit.isFortran() && calledUnit.otherReturnVar() != null) {
                    paramName = calledUnit.otherReturnVar().symbol;
                } else if (headTree != null) {
                    paramName = ILUtils.getCalledNameString(headTree);
                } else {
                    paramName = calledUnit.name();
                }
            } else if (curDiffUnitSort() == DiffConstants.ADJOINT || multiDirMode()) {
                if (diffCalledUnit == null) {
                    paramName = "UndefinedArgumentName";
                } else {
                    if (diffCalledUnit.entryBlock() != null) {
                        Tree diffHeadTree = diffCalledUnit.entryBlock().headTree();
                        int rank = ILUtils.getArguments(diffHeadTree).length();
                        if (multiDirMode()) {
                            rank = rank - 1;
                        }
                        paramName = ILUtils.getIdentString(
                                ILUtils.getArguments(diffHeadTree).down(rank));
                    } else {
                        paramName = diffCalledUnit.name();
                    }
                }
            }
        } else if (headTree != null) {
            paramName = ILUtils.getIdentString(ILUtils.getArguments(headTree).down(i + 1));
        } else {
            paramName = "UndefinedArgumentName";
        }

        nameTree = ILUtils.build(ILLang.op_ident, paramName);
        if (diff && diffCalledUnit != null && i != -1) {
            Tree diffHeadTree;
            if (diffCalledUnit.entryBlock() != null) {
                diffHeadTree = diffCalledUnit.entryBlock().headTree();
                VariableDecl paramDecl = diffCalledUnit.publicSymbolTable().getTopVariableDecl(paramName);
                if (paramDecl.hasDiffSymbolHolders()) {
                    if (!TapEnv.associationByAddress()) {
                        boolean found = false;
                        int rank = 1;
                        while (!found && rank <= ILUtils.getArguments(diffHeadTree).length()) {
                            found = paramName.equals(ILUtils.getArguments(diffHeadTree).down(rank).stringValue());
                            if (!found) {
                                rank = rank + 1;
                            }
                        }
                        if (found) {
                            nameTree = ILUtils.getArguments(diffHeadTree).down(rank + 1);
                        }
                    } else {
                        NewSymbolHolder nSH = paramDecl.getDiffSymbolHolder(diffSort, null, 0);
                        if (nSH != null && nSH.newVariableDecl() != null) {
                            nameTree = ILUtils.build(ILLang.op_ident, nSH.newVariableDecl().symbol);
                        }
                    }
                }
            } else {
                // TODO a modifier:
                // ne pas faire diffSymbol de nameTree car calledUnit a deja ete
                // differentiee et les NewSymbolHolder resolus
                nameTree = varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), nameTree, diffCalledUnit.publicSymbolTable(),
                        true, false, false, null, false, diffSort, curDiffUnitSort(), DiffConstants.VAR, null);
            }
            if (ILUtils.isNullOrNone(nameTree)) {
                nameTree = ILUtils.build(ILLang.op_ident, "UndefinedArgumentName");
            }
        }
        if (nameTree.parent() != null) {
            nameTree = ILUtils.copy(nameTree);
        }
        return nameTree;
    }

    //[llh] TODO: merge with reinitializeDiffIOVars() ?

    /**
     * Adds into zonesToReinit all the ZoneInfo of the elements in argZonesTree that are PASSIVE in actualActivity,
     * but are active in formalArgActivityTree. Since this method is called only on an an actual parameter tree,
     * collects only the ZoneInfo that are accessible through this parameter.
     */
    private TapList<ZoneInfo> collectZonesFormalActiveAndActualPassive(TapList argZonesTree, TapIntList tmpZones,
                                                                       TapList formalArgActivityTree, BoolVector actualActivity,
                                                                       Unit calledUnit, TapList<ZoneInfo> zonesToReinit) {
        while (argZonesTree != null && formalArgActivityTree != null) {
            if (formalArgActivityTree.head instanceof TapList) {
                if (argZonesTree.head instanceof TapList) {
                    zonesToReinit = collectZonesFormalActiveAndActualPassive((TapList) argZonesTree.head, tmpZones,
                            (TapList) formalArgActivityTree.head, actualActivity,
                            calledUnit, zonesToReinit);
                } else {
                    // Weird case where argZonesTree and formalArgActivityTree do not match!
                    // Here actual is non-record and formal is record.
                    boolean formalActive = TapList.oneTrueInHead(formalArgActivityTree);
                    if (formalActive) {
                        zonesToReinit = collectVisiblePassiveZones((TapIntList) argZonesTree.head, zonesToReinit,
                                tmpZones, actualActivity, calledUnit);
                    }
                }
            } else if (formalArgActivityTree.head != null) {
                TapIntList zones;
                if (argZonesTree.head instanceof TapList) {
                    // Weird case where argZonesTree and formalArgActivityTree do not match!
                    // Here actual is record and formal is non-record.
                    zones = ZoneInfo.listAllZones((TapList) argZonesTree.head, true);
                } else {
                    zones = (TapIntList) argZonesTree.head;
                }
                boolean formalActive = (Boolean) formalArgActivityTree.head;
                if (formalActive) {
                    zonesToReinit = collectVisiblePassiveZones(zones, zonesToReinit,
                            tmpZones, actualActivity, calledUnit);
                }
            }
            argZonesTree = argZonesTree.tail;
            formalArgActivityTree = formalArgActivityTree.tail;
        }
        return zonesToReinit;
    }

    private TapList<ZoneInfo> collectVisiblePassiveZones(TapIntList zones, TapList<ZoneInfo> zonesToReinit,
                                                         TapIntList tmpZones, BoolVector actualActivity, Unit calledUnit) {
        int zone;
        ZoneInfo zoneInfo;
        while (zones != null) {
            zone = zones.head;
            if (!TapIntList.contains(tmpZones, zone)) {
                zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(zone, SymbolTableConstants.ALLKIND);
                // Don't zero-initialize diff of globals (set11/lh006) except do it for COMMON vars.
                if (zoneInfo != null && !zoneInfo.isHidden
                    && (!zoneInfo.isGlobal() || zoneInfo.commonName!=null)
                    && !TapList.contains(zonesToReinit, zoneInfo)) {
                    zone = zoneInfo.kindZoneNb(adEnv.diffKind);
                    if (zone >= 0 && !actualActivity.get(zone)) {
                        zonesToReinit = new TapList<>(zoneInfo, zonesToReinit);
                    }
                }
            }
            zones = zones.tail;
        }
        return zonesToReinit;
    }

    /**
     * Collects the expression trees that must be diff-initialized just after a call
     * to the diff of "calledUnit", in relation to some actual argument "argTree" of this call.
     * The diff of a component of "argTree" must be reset to zero just after the call if
     * (This case has been removed!:
     * -- either this component is passive upon exit from "calledUnit" (cf "active2")
     * but possibly active just after the call (cf "afterActiv"). In principle, this should
     * not happen, but it does happen in rare cases, due to approximation in the analysis
     * (cf the exit of subr "changes" in F90:lh90). )
     * -- or this component was active at the beginning of "calledUnit" (cf "active1"), and becomes
     * passive upon exit from "calledUnit" (cf "active2"), and the actual argument uses a
     * pointer. In this case, the formal argument (or at least its part "not through pointer")
     * has not been diff-initialized in the call (because it is not through pointer), and the
     * actual arg must be diff-initialized because it is through a pointer.
     * In summary, collects sub-exprs of argTree that are
     * modified AND NOT active2 AND ((active1 AND throughMutable) OR (afterActiv)).
     * Adds the collected trees into "treesToReinit" and returns the new "treesToReinit"
     */
    private TapList<Tree> collectTreesToDiffInitAfterDiffCall(Tree argTree, boolean argThroughPointer,
                                                              WrapperTypeSpec argType,
                                                              WrapperTypeSpec formalType, TapList argZonesTree,
                                                              TapList active1, TapList active2, TapList modified,
                                                              TapList actualArgAfterActiv, Unit calledUnit,
                                                              TapList<Tree> treesToReinit, TapList<TypeSpec> dejaVu,
                                                              String message) {
        if (argType != null
                && TypeSpec.isA(formalType, SymbolTableConstants.ARRAYTYPE)
                && argTree.opCode() == ILLang.op_arrayAccess
                && !TypeSpec.isA(argType, SymbolTableConstants.ARRAYTYPE)) {
            message = "(AD17) Type mismatch in argument " + argTree.rankInParent()
                    + " of procedure " + calledUnit.name() + ", was "
                    + formalType.showType() + ", is here "
                    + argType.showType() + ", please check initialization after diff call";
            //argTree = RefDescriptor.explicitWhenImplicitArray(argTree, argType, formalType, curSymbolTable()); ???
        }
        TypeSpec actualTypeSpec = argType == null ? null : argType.wrappedType;
        if (actualTypeSpec == null || TapList.contains(dejaVu, actualTypeSpec)) {
            return treesToReinit;
        }
        switch (actualTypeSpec.kind()) {
            case SymbolTableConstants.ARRAYTYPE: {
                WrapperTypeSpec elementType = actualTypeSpec.elementType();
                return collectTreesToDiffInitAfterDiffCall(argTree, argThroughPointer, elementType,
                        null, argZonesTree, active1, active2, modified,
                        actualArgAfterActiv, calledUnit,
                        treesToReinit, new TapList<>(actualTypeSpec, dejaVu), message);
            }
            case SymbolTableConstants.MODIFIEDTYPE: {
                WrapperTypeSpec elementType = actualTypeSpec.elementType();
                return collectTreesToDiffInitAfterDiffCall(argTree, argThroughPointer, elementType,
                        null, argZonesTree, active1, active2, modified,
                        actualArgAfterActiv, calledUnit,
                        treesToReinit, new TapList<>(actualTypeSpec, dejaVu), message);
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec) actualTypeSpec;
                FieldDecl[] fieldDecls = compositeTypeSpec.fields;
                for (int i = 0; i < fieldDecls.length - 1; ++i) {
                    Tree fieldTree = ILUtils.build(ILLang.op_fieldAccess,
                            ILUtils.copy(argTree),
                            ILUtils.build(ILLang.op_ident, fieldDecls[i].symbol));
                    ILUtils.setFieldRank(fieldTree.down(2), i);
                    TapList fieldActive1 = active1;
                    if (active1 != null && active1.head instanceof TapList) {
                        fieldActive1 = (TapList) active1.head;
                        active1 = active1.tail;
                    }
                    TapList fieldActive2 = active2;
                    if (active2 != null && active2.head instanceof TapList) {
                        fieldActive2 = (TapList) active2.head;
                        active2 = active2.tail;
                    }
                    TapList fieldModified = modified;
                    if (modified != null && modified.head instanceof TapList) {
                        fieldModified = (TapList) modified.head;
                        modified = modified.tail;
                    }
                    TapList fieldAfterActiv = actualArgAfterActiv;
                    if (actualArgAfterActiv != null && actualArgAfterActiv.head instanceof TapList) {
                        fieldAfterActiv = (TapList) actualArgAfterActiv.head;
                        actualArgAfterActiv = actualArgAfterActiv.tail;
                    }
                    TapList fieldZonesTree = argZonesTree;
                    if (argZonesTree != null && argZonesTree.head instanceof TapList) {
                        fieldZonesTree = (TapList) argZonesTree.head;
                        argZonesTree = argZonesTree.tail;
                    }
                    treesToReinit = collectTreesToDiffInitAfterDiffCall(fieldTree, argThroughPointer, fieldDecls[i].type(),
                            null, fieldZonesTree, fieldActive1, fieldActive2, fieldModified,
                            fieldAfterActiv, calledUnit,
                            treesToReinit, new TapList<>(actualTypeSpec, dejaVu), message);
                }
                return treesToReinit;
            }
            case SymbolTableConstants.POINTERTYPE: {
                PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) actualTypeSpec;
                Tree pointedTree = ILUtils.build(ILLang.op_pointerAccess,
                        ILUtils.copy(argTree),
                        ILUtils.build(ILLang.op_none));
                TapList destActive1 = active1 == null ? null : active1.tail;
                TapList destActive2 = active2 == null ? null : active2.tail;
                TapList destModified = modified == null ? null : modified.tail;
                TapList destAfterActiv = actualArgAfterActiv == null ? null : actualArgAfterActiv.tail;
                TapList destZonesTree = argZonesTree == null ? null : argZonesTree.tail;
                treesToReinit = collectTreesToDiffInitAfterDiffCall(pointedTree, false, pointerTypeSpec.destinationType,
                        null, destZonesTree, destActive1, destActive2, destModified,
                        destAfterActiv, calledUnit,
                        treesToReinit, new TapList<>(actualTypeSpec, dejaVu), message);
                return treesToReinit;
            }
            case SymbolTableConstants.PRIMITIVETYPE: {
// TapEnv.printlnOnTrace("active1:"+active1+" active2:"+active2+" modified:"+modified+" actualAfterUsef:"+actualArgAfterActiv) ;
                if (TapList.oneTrueInHead(active1) && !TapList.oneTrueInHead(active2) && TapList.oneTrueInHead(modified)
                        && (argThroughPointer
                        || TapList.oneTrueInHead(actualArgAfterActiv))) {
                    if (message != null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, argTree, message);
                    }
                    treesToReinit = new TapList<>(argTree, treesToReinit);
                }
                return treesToReinit;
            }
            default:
                return treesToReinit;
        }
    }

    /**
     * Find the rank of the first argument of this call which is
     * either a nameEq or a missing optional.
     */
    private static int findNameEqOrNone(Tree callTree) {
        Tree[] sons = ILUtils.getArguments(callTree).children();
        Tree[] sourceSons = new Tree[0];
        Tree sourceTree = callTree.getAnnotation("sourcetree");
        if (sourceTree != null
                && sourceTree.opCode() == ILLang.op_call) {
            sourceSons = ILUtils.getArguments(sourceTree).children();
        }
        int found = -1, i = 0;
        while (found == -1 && i < sons.length) {
            if (sons[i].opCode() == ILLang.op_none ||
                    i < sourceSons.length &&
                            sourceSons[i].opCode() == ILLang.op_nameEq) {
                found = i;
            }
            ++i;
        }
        return found;
    }

    /**
     * According to their appearance order in the formal arguments list "args" of the new Unit "diffUnit",
     * update the formalArgRank's of the VariableDecl's in the representation of "diffUnit".
     * NewUnit is a unit of the differentiated CallGraph, so that changing the formalArgRank's in it
     * does not affect the formalArgRank's in the source CallGraph.
     * Keep the corresponding formalArgRank in the source Unit in field "formalArgRankInOrigUnit"
     */
    private void updateFormalArgRankS(TapList<Tree> args, SymbolTable diffPublicSymbolTable) {
        Tree arg;
        int rank = 1;
        while (args != null) {
            arg = args.head;
            if (arg.opCode() != ILLang.op_star) {
                Tree argBaseTree = ILUtils.baseTree(arg);
                String argBaseName = (argBaseTree == null ? null : ILUtils.baseName(argBaseTree));
                VariableDecl varDecl = null;
                FunctionDecl funcDecl = null;
                if (argBaseName != null) {
                    NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(argBaseTree);
                    if (sHolder != null) {
                        varDecl = sHolder.newVariableDecl();
                        if (varDecl == null) {
                            funcDecl = sHolder.newFunctionDecl();
                        }
                    } else {
                        varDecl = diffPublicSymbolTable.getVariableDecl(argBaseName);
                        if (varDecl == null) {
                            TapList<FunctionDecl> funcDecls =
                                    diffPublicSymbolTable.getFunctionDecl(argBaseName, null, null, false);
                            funcDecl = (funcDecls == null ? null : funcDecls.head);
                        }
                    }
                    if (varDecl != null) {
                        // Warning: this method updateFormalArgRankS() may be called repeatedly.
                        // but only the 1st call must do something. Therefore we change the
                        // formalArgRankInOrigUnit only when it is empty (==UNDEFINED_FORMAL_ARG_RANK)
                        if (varDecl.formalArgRankInOrigUnit == SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK) {
                            varDecl.formalArgRankInOrigUnit = varDecl.formalArgRank;
                        }
                        varDecl.formalArgRank = rank;
                    } else if (funcDecl != null) {
                        funcDecl.formalArgRank = rank;
                    }
                }
            }
            args = args.tail;
            rank = rank + 1;
        }
    }

    private void cleanExtraInfoValue(Tree diffArg) {
        NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(diffArg);
        if (sHolder != null) {
            TapList<String> modifiers = sHolder.extraInfo();
            modifiers = TapList.cleanExtraInfoValue(modifiers, "value");
            sHolder.setExtraInfo(modifiers);
            VariableDecl varDecl = sHolder.newVariableDecl;
            modifiers = varDecl.extraInfo();
            modifiers = TapList.cleanExtraInfoValue(modifiers, "value");
            varDecl.setExtraInfo(modifiers);
        }
    }

    /**
     * Builds a (block of) trees that reset to zero
     * all memory cells of the derivative of "exprToInitialize".
     * Returns an array of such blocks, one per diff replica (in general 1).
     * Special case of BlockDifferentiator.makeDiffInitialization()
     * that is used for arguments of procedure calls,
     * and therefore uses 2 given types (type of actual arg and
     * type of formal arg for "exprToInitialize"), along with an
     * optional tree "hintRootTree" to build the name and hint
     * messages of possible unknown array dimensions.
     */
    private Tree[] makeDiffInitializationOfCallArgument(Tree exprToInitialize, Tree hintRootTree,
                                                      SymbolTable usageSymbolTable,
                                                      WrapperTypeSpec actualArgType,
                                                      WrapperTypeSpec formalArgType,
                                                      boolean protectAccesses, TapList ignoreStructure, boolean ignorePointed) {
        // When exprToInitialize comes from (contains) an allocate,
        // replace this allocate by the pointer-to-allocated variable.
        exprToInitialize = ILUtils.copy(exprToInitialize);
        exprToInitialize = BlockDifferentiator.replaceAllocateInExprToInitialize(exprToInitialize);
        String baseVarName = ILUtils.baseName(exprToInitialize);
        Tree baseVarTree = ILUtils.baseTree(exprToInitialize);
        if (baseVarName == null || baseVarTree == null) {
            TapEnv.toolWarning(-1, "(Build diff initialization) Unexpected expression: " + exprToInitialize);
            return null;
        }
        boolean isCurUnitName = baseVarName.equals(adEnv.curUnit().name());
        Tree[] resultR = new Tree[TapEnv.diffReplica()] ;
        for (adEnv.iReplic=0 ; adEnv.iReplic<resultR.length ; ++adEnv.iReplic) {
            Tree diffBaseVar =
                varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, usageSymbolTable, true,
                        (curDiffUnitSort() == DiffConstants.TANGENT && !multiDirMode() && isCurUnitName),
                        false, null, false,
                        curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, hintRootTree) ;
            if (TapEnv.associationByAddress()) { // Warning, this is incompatible with replicas>1 !
                diffBaseVar = ILUtils.build(ILLang.op_fieldAccess,
                                ILUtils.copy(diffBaseVar),
                                ILUtils.build(ILLang.op_ident, TapEnv.assocAddressDiffSuffix())) ;
            }
            resultR[adEnv.iReplic] = RefDescriptor.makeInitialize(curDiffUnit(), null, exprToInitialize,
                                             actualArgType, formalArgType, hintRootTree,
                                             adEnv.curSymbolTable(), usageSymbolTable, null, diffBaseVar, true,
                                             (multiDirMode() ? varRefDifferentiator().multiDirMaxIterDescriptor : null),
                                             protectAccesses, ignoreStructure, ignorePointed) ;
        }
        adEnv.iReplic=0;//Temporary until replicas are treated everywhere...
        return resultR ;
    }

    private Tree makeDiffTagInitialization(Tree exprToInitialize, SymbolTable usageSymbolTable) {
        // When exprToInitialize comes from (contains) an allocate,
        // replace this allocate by the pointer-to-allocated variable.
        exprToInitialize = ILUtils.copy(exprToInitialize);
        exprToInitialize = BlockDifferentiator.replaceAllocateInExprToInitialize(exprToInitialize);
        String baseVarName = ILUtils.baseName(exprToInitialize);
        Tree baseVarTree = ILUtils.baseTree(exprToInitialize);
        if (baseVarName == null || baseVarTree == null) {
            TapEnv.toolWarning(-1, "(Build diff initialization) Unexpected expression: " + exprToInitialize);
            return null;
        }
        boolean isCurUnitName = baseVarName.equals(adEnv.curUnit().name());
        Tree diffBaseVar =
                varRefDifferentiator().diffSymbolTree(adEnv.curActivity(), baseVarTree, usageSymbolTable, true,
                        curDiffUnitSort() == DiffConstants.TANGENT && !multiDirMode() && isCurUnitName,
                        false, null, false,
                        curDiffVarSort(), curDiffUnitSort(), DiffConstants.VAR, null);
        NewSymbolHolder tagMessagePassingSymbolHolder = curDiffUnit().tagMessagePassingSymbolHolder;
        if (tagMessagePassingSymbolHolder == null) {
            tagMessagePassingSymbolHolder = new NewSymbolHolder("UNDEFINED_TAG");
            tagMessagePassingSymbolHolder.setAsConstantVariable(adEnv.integerTypeSpec, null, ILUtils.build(ILLang.op_intCst, 1000));
            tagMessagePassingSymbolHolder.declarationLevelMustInclude(usageSymbolTable);
            curDiffUnit().tagMessagePassingSymbolHolder = tagMessagePassingSymbolHolder;
        }
        Tree undefTagTree = tagMessagePassingSymbolHolder.makeNewRef(usageSymbolTable);
        return ILUtils.build(ILLang.op_assign, diffBaseVar,
                ILUtils.build(ILLang.op_add, ILUtils.copy(exprToInitialize), undefTagTree));
    }

    /**
     * @return true when the zones of the value of "resultTree" overlap the zones
     * read or written during the call to "callTree". In this case, there is
     * a danger of aliasing in the diff of callTree, because the "resultTree" will
     * become a parameter of the diff callTree.
     */
    private boolean zonesOverlapCall(Tree resultTree, Tree callTree) {
        TapIntList resultZones = adEnv.curSymbolTable().listOfZonesOfValue(resultTree, null, adEnv.curInstruction());
        Unit calledUnit = DataFlowAnalyzer.getCalledUnit(callTree, adEnv.curSymbolTable());
        CallArrow callArrow = CallGraph.getCallArrow(adEnv.curUnit(), calledUnit);
        BoolVector privateRW = new BoolVector(adEnv.curVectorLen);
        Tree[] args = ILUtils.getArguments(callTree).children() ;
        DataFlowAnalyzer.translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyRorW(), privateRW, null, args, true,
                                                       callTree, adEnv.curInstruction(), callArrow,
                                                       SymbolTableConstants.ALLKIND,
                                                       null, null, true, false) ;
        return DataFlowAnalyzer.intersectsZoneRks(privateRW, resultZones, null);
    }

    private boolean zonesOverlapArg(Tree resultTree, Tree argTree) {
        TapIntList resultZones = adEnv.curSymbolTable().listOfZonesOfValue(resultTree, null, adEnv.curInstruction());
        TapIntList argZones = adEnv.curSymbolTable().listOfZonesOfValue(argTree, null, adEnv.curInstruction());
        return TapIntList.intersects(resultZones, argZones);
    }

    /**
     * @return true if expression "expr" dereferences at least one pointer or array index.
     */
    private static boolean isAccessedThroughPointerOrIndex(Tree expr, SymbolTable symbolTable,
                                                           int language, Unit calledUnit, int rank) {
        switch (expr.opCode()) {
            case ILLang.op_fieldAccess:
                return isAccessedThroughPointerOrIndex(expr.down(1), symbolTable, language, calledUnit, rank);
            case ILLang.op_arrayAccess:
                return isAccessedThroughPointerOrIndex(expr.down(1), symbolTable, language, calledUnit, rank)
                        || !fullArray(expr, symbolTable);
            case ILLang.op_pointerAccess:
                return true;
            default:
                if (calledUnit.isFortran2003()) {
                    return calledUnit.takesArgumentByValue(rank, language);
                } else {
                    return false;
                }
        }
    }

    /**
     * @return true if the given Tree, which is an array access,
     * effectively selects the complete array.
     */
    private static boolean fullArray(Tree arrayAccess, SymbolTable symbolTable) {
        boolean isFull = false;
        WrapperTypeSpec typeSpec = symbolTable.typeOf(arrayAccess.down(1));
        if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
            ArrayDim[] dims = ((ArrayTypeSpec) typeSpec.wrappedType).dimensions();
            Tree[] sons = arrayAccess.down(2).children();
            if (dims.length == sons.length) {
                isFull = true;
                for (int i = sons.length - 1; isFull && i >= 0; --i) {
                    isFull = dims[i].fullArrayTriplet(sons[i]);
                }
            }
        }
        return isFull;
    }

    private boolean usesTheStack(TapList<TapPair<RefDescriptor, RefDescriptor>> refsToPop) {
        boolean reallyUsesTheStack = false;
        RefDescriptor refDescriptor, refDescriptorDiff;
        while (refsToPop != null) {
            refDescriptor = refsToPop.head.first;
            refDescriptorDiff = refsToPop.head.second;
            if (refDescriptorDiff != null && !refDescriptorDiff.usesStaticSave()) {
                reallyUsesTheStack = true;
            }
            if (refDescriptor != null && !refDescriptor.usesStaticSave()) {
                reallyUsesTheStack = true;
            }
            refsToPop = refsToPop.tail;
        }
        return reallyUsesTheStack;
    }

    /**
     * @return true if the diff of unit will be a function.
     */
    protected boolean diffFunctionIsFunction(Unit unit) {
        // [llh 26/9/2013] I thought a Fortran function couldn't return an array, but it seems
        // it can. So I removed the test that was preventing that. Cf F90:lh87 and F90:v56
        return !multiDirMode() && TapEnv.diffReplica()<2 && !unit.usedAsElementalResult /*&& !unit.returnsAnArray()*/;
    }

}
