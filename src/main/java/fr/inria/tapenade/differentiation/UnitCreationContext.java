/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.utils.TapPair;

/**
 * An environment that holds context info useful to build a new Unit.
 */
public class UnitCreationContext {
    /**
     * The new Unit being built.
     */
    protected Unit unit;
    /**
     * The list of all Blocks created for this new Unit being built.
     */
    protected TapList<Block> toAllBlocks;
    /**
     * The association from original SymbolTable's to their
     * counterpart in the new Unit being built.
     */
    protected TapList<TapPair<SymbolTable, SymbolTable>> listST;

    /**
     * Standard creation.
     */
    protected UnitCreationContext(Unit unit, TapList<Block> toAllBlocks,
                                  TapList<TapPair<SymbolTable, SymbolTable>> listST) {
        super();
        this.unit = unit;
        this.toAllBlocks = toAllBlocks;
        this.listST = listST;
    }

    /**
     * @return a shallow copy of the listST. This is used so that new SymbolTable
     * correspondences will not be seen by the given model UnitCreationContext.
     */
    protected static UnitCreationContext copyListST(UnitCreationContext modelCtxt) {
        UnitCreationContext result = new UnitCreationContext(modelCtxt.unit, modelCtxt.toAllBlocks, null);
        result.listST = TapList.append(modelCtxt.listST, null);
        return result;
    }
}
