/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.RefDescriptor;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VoidTypeSpec;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.Tree;

// TODO: replicate if -diffReplica

/**
 * Object that differentiates Dynamic Memory manipulations (e.g. malloc's and free's).
 * In cooperation with a BlockDifferentiator, for each given procedure call,
 * tells this BlockDifferentiator which instructions must be added
 * to the fwdBlock and (optional) bwdBlock of the current curBlock.
 */
public class DynMemoryDifferentiator {

    //Temporary only for debug of ADMM.
    private int numAlloc = 0;
    private int numDealloc = 0;

    /**
     * Differentiation environment.
     */
    private final DifferentiationEnv adEnv;

    /**
     * Access to global adEnv.blockDifferentiator.
     */
    private FlowGraphDifferentiator flowGraphDifferentiator() {
        return adEnv.flowGraphDifferentiator;
    }

    /**
     * Access to global adEnv.BlockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * Access to global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /**
     * Access to global adEnv.curDiffUnit.
     */
    private Unit curDiffUnit() {
        return adEnv.curDiffUnit;
    }

    /**
     * Access to global adEnv.curFwdDiffUnit.
     */
    private Unit curFwdDiffUnit() {
        return adEnv.curFwdDiffUnit;
    }

    /**
     * Access to global adEnv.curBlock.
     */
    private Block curBlock() {
        return adEnv.curBlock;
    }

    /**
     * For C generated code, the Tree for NULL i.e. (void*)0.
     */
    private final Tree c_NULL = ILUtils.build(ILLang.op_cast,
            ILUtils.build(ILLang.op_pointerType,
                    ILUtils.build(ILLang.op_void)),
            ILUtils.build(ILLang.op_intCst, 0));

    protected DynMemoryDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
    }

    /**
     * Build the derivative instructions that come from the given source "tree",
     * which is a malloc-like assignment. This builds the list that must go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order).
     * Iff there is a backward sweep, then we use ADMM to register memory chunks.
     */
    protected void buildDiffInstructionsOfAssignAllocate(Tree tree, int differentiationMode,
                                                         SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
                                                         BoolVector beforeActiv, BoolVector afterActiv, BoolVector afterReqX, BoolVector beforeAvlX) {
// if (differentiationMode!=TANGENT_MODE) PBaseTrack.experimentAssignAllocate(tree, curBlock().symbolTable)  ;
        Tree lhsTree = tree.down(1);
        Tree mallocTree = tree.down(2);
        boolean mustDiffOperation =
                blockDifferentiator().pointerIsActiveHere(lhsTree, beforeActiv, afterActiv, beforeAvlX, afterReqX);
        // Use ADMM only when there will be a backward sweep:
        boolean hasBwdSweep = differentiationMode == DiffConstants.ADJOINT_MODE
                || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE;
        boolean tbrPrimal = false;
        boolean rebasePrimal = false, rebaseOnDiffPtr = false;
        Tree diffVarRefFwd = null, diffVarRefBwd = null;
        RefDescriptor fwdPrimalRefDescriptor = null;
        Tree diffMalloc, newTree;

        boolean lhsIsTBR = ADTBRAnalyzer.isTBR(adEnv.curActivity(), lhsTree);
        boolean isFortranAllocatable = curBlock().symbolTable.varHasModifier(lhsTree, "allocatable");
            //was: curBlock().symbolTable.isAnAllocatablePointer(lhsTree, adEnv.curInstruction());
        TapIntList allocatedZones = ZoneInfo.listAllZones(mallocTree.getAnnotation("allocatedZones"), false);
        if (hasBwdSweep && (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM())) {
            ++numAlloc;
        }

        if (hasBwdSweep) {
            if (flowGraphDifferentiator().toChunklengthVariable.head == null) {
                flowGraphDifferentiator().toChunklengthVariable.head =
                        new NewSymbolHolder("chunklength", curDiffUnit(), adEnv.integerTypeSpec, -1);
            }
            flowGraphDifferentiator().toChunklengthVariable.head.declarationLevelMustInclude(bwdSymbolTable);
        }

        if (mustDiffOperation) {
            diffVarRefFwd = varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhsTree, fwdSymbolTable,
                    false, lhsTree, false, true, null);
            if (hasBwdSweep && !adEnv.curUnitIsContext) {
                diffVarRefBwd = varRefDifferentiator().diffVarRef(adEnv.curActivity(), lhsTree, bwdSymbolTable,
                        false, lhsTree, false, true, null);
                if (ADTBRAnalyzer.isTBROnDiffPtr(adEnv.curActivity(), lhsTree)
                        && !isFortranAllocatable) {
                    rebaseOnDiffPtr = DataFlowAnalyzer.mayPointToRelocated(lhsTree, true, adEnv.curInstruction(),
                            adEnv.curSymbolTable(), differentiationMode == DiffConstants.ADJOINT_MODE);
                }
            }
        }

        if (hasBwdSweep && !adEnv.curUnitIsContext && lhsIsTBR) {
            fwdPrimalRefDescriptor =
                    new RefDescriptor(lhsTree, null,
                            curBlock().symbolTable, fwdSymbolTable, null,
                            null, false, null, curDiffUnit());
            blockDifferentiator().prepareRestoreOperations(fwdPrimalRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
            if (!isFortranAllocatable) {
                rebasePrimal = DataFlowAnalyzer.mayPointToRelocated(lhsTree, false, adEnv.curInstruction(),
                        adEnv.curSymbolTable(), differentiationMode == DiffConstants.ADJOINT_MODE);
            }
        }

        // Rebase the upstream values of primal (and diff) pointers after restoring them:
        // TODO: we never tested how to do the following in Fortran:
        if ((rebasePrimal || rebaseOnDiffPtr) && curDiffUnit().isC()) {
            ++blockDifferentiator().numRebase;
            newTree = fwdPrimalRefDescriptor.makeRebase(rebasePrimal ? null : new TapList<>(Boolean.TRUE, null),
                    rebaseOnDiffPtr ? null : new TapList<>(Boolean.TRUE, null),
                    rebaseOnDiffPtr ? ILUtils.baseTree(diffVarRefBwd) : null,
                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
            blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                    new TapList<>(lhsTree, null), new TapList<>(lhsTree, null),
                    null, rebaseOnDiffPtr ? new TapList<>(lhsTree, null) : null, false, false,
                    "Rebase TBR previous value " + (rebaseOnDiffPtr ? "and DIFF " : "")
                            + "of allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
        }

        if (mustDiffOperation && !TapEnv.associationByAddress()) {
            // Build store/restore of the previous contents of THE DIFF OF assigned lhsTree, if it is TBR OnDiffPtr:
            if (hasBwdSweep && !adEnv.curUnitIsContext && ADTBRAnalyzer.isTBROnDiffPtr(adEnv.curActivity(), lhsTree)) {
                RefDescriptor fwdDiffRefDescriptor =
                        new RefDescriptor(lhsTree, null,
                                curBlock().symbolTable, fwdSymbolTable, null,
                                ILUtils.baseTree(diffVarRefFwd), true, null, curDiffUnit());
                if (!isFortranAllocatable) {
                    blockDifferentiator().prepareRestoreOperations(fwdDiffRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
                    blockDifferentiator().addFuturePlainNodeFwd(fwdDiffRefDescriptor.makePush(), null, false,
                            null, null, new TapList<>(lhsTree, null), null, false, true,
                            "Store TBR previous value of DIFF OF allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
                    fwdDiffRefDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                    blockDifferentiator().addFuturePlainNodeBwd(fwdDiffRefDescriptor.makePop(), null, false,
                            ILUtils.usedVarsInExp(lhsTree, null, false),
                            null, null, new TapList<>(lhsTree, null), false, true,
                            "Restore TBR previous value of DIFF OF allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
                } else {
                    TapEnv.toolError("Not yet implemented: FWD+ and +BWD push/pop for allocatable " + lhsTree);
                }
            }

            // Build the allocation for the differentiated allocated chunk:
            diffMalloc = varRefDifferentiator().diffVarRef(adEnv.curActivity(), mallocTree, fwdSymbolTable,
                    false, mallocTree, false, true, null);
            annotateWithSameSourceTree(diffMalloc, mallocTree) ; //To keep Cuda style
            newTree = ILUtils.build(ILLang.op_assign, diffVarRefFwd, diffMalloc);
            blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                    null, null, null, new TapList<>(lhsTree, null), false, false,
                    "Allocate DIFF OF var " + ILUtils.toString(lhsTree, adEnv.curActivity()), true);

            // Build zero-initialization of the differentiated allocated chunk:
            // Do that if EITHER the zone of the allocated var is active just after the allocation,
            // which may happen in bizarre/buggy cases (cf F90:lha50, bug in activity analysis) (cf hasActivePointedZone())
            // (but we'd better check that the zone is active BEFORE the DEALLOCATE !)
            // OR if we have option "-nooptim spareinit", which forces diff initializations,
            // OR this is a "context" procedure
            TapIntList allocatedZonesDiffKind =
                    DataFlowAnalyzer.mapZoneRkToKindZoneRk(allocatedZones, adEnv.diffKind,
                                                           adEnv.curSymbolTable());
            if (adEnv.curUnitIsContext
                    || !TapEnv.spareDiffReinitializations()
                    || differentiationMode == DiffConstants.ADJOINT_MODE && !TapEnv.removeDeadPrimal() //cf set10/lh208
                    // Always initialize in adjoint because there may be no deallocate to do it later. cf C:lh35 :
                    || hasBwdSweep
                    //  cf hasActivePointedZone() : Let's play it safe and initialize :
                    || afterActiv != null && afterActiv.intersects(allocatedZonesDiffKind)) {
                Tree newlyAllocatedDiff;
                //Potential problem: building explicitly an arrayAccess like below is a way to force the coming
                // makeDiffInitialization() to use the correct size (as we know it from the recent allocation).
                // But this would cause the access protection to be on the arrayAccess, which is wrong.
                //This problem is masked as long as we don't ask for access protection
                // (cf the last "false" in makeDiffInitialization()).
                if (curDiffUnit().isC()) {
                    Tree iterationSpace;
                    if (ILUtils.isNullOrNone(mallocTree.down(2))) {
                        iterationSpace = ILUtils.build(ILLang.op_none); // meaning there's just one pointed object.
                    } else {
                        iterationSpace = ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, 0), // This is C: indices start at 0!
                                ILUtils.build(ILLang.op_sub,
                                        ILUtils.copy(mallocTree.down(2).down(1)),
                                        ILUtils.build(ILLang.op_intCst, 1)));
                    }
                    newlyAllocatedDiff = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(tree.down(1)), iterationSpace);
                } else {
                    newlyAllocatedDiff = ILUtils.build(ILLang.op_arrayAccess,
                            ILUtils.build(ILLang.op_pointerAccess,
                                    ILUtils.copy(tree.down(1)),
                                    ILUtils.build(ILLang.op_intCst, 0)),
                            buildFullDimColons(mallocTree.down(2), true)); // This is Fortran: indices start at 1!
                }
                Tree[] fwdDiffControlTreeR =
                        blockDifferentiator().makeDiffInitialization(newlyAllocatedDiff, null, adEnv.curSymbolTable(), fwdSymbolTable,
                                null, false, null, true);
                Tree fwdDiffControlTree ;
                for (int iReplic=0 ; iReplic<fwdDiffControlTreeR.length ; ++iReplic) {
                    fwdDiffControlTree = fwdDiffControlTreeR[iReplic] ;
                    if (fwdDiffControlTree != null) {
                        blockDifferentiator().addFuturePlainNodeFwd(fwdDiffControlTree, null, false,
                            null, null, null, new TapList<>(lhsTree, null), false, false,
                            "Initialize DIFF OF allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
                    }
                }
            }

            // Build the BWD sweep free/deallocate of the differentiated allocated chunk:
            if (hasBwdSweep && !adEnv.curUnitIsContext) {
                newTree = ILUtils.build(ILLang.op_deallocate, ILUtils.copy(diffVarRefBwd), null);
                annotateWithSameSourceTree(newTree, mallocTree) ; //To keep Cuda style
                blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                        null, null, null, new TapList<>(lhsTree, null), false, false,
                        "Deallocate DIFF OF var " + ILUtils.toString(lhsTree, adEnv.curActivity()), true);
            }

        }

        // Build store/restore of the previous contents of assigned lhsTree, if it is TBR:
        if (hasBwdSweep && !adEnv.curUnitIsContext && lhsIsTBR) {
            if (!isFortranAllocatable) {
                tbrPrimal = true;
                blockDifferentiator().prepareRestoreOperations(fwdPrimalRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
                blockDifferentiator().addFuturePlainNodeFwd(fwdPrimalRefDescriptor.makePush(), null, false,
                        new TapList<>(lhsTree, null), null, null, null, false, true,
                        "Store TBR previous value of allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
            } else {
                TapEnv.toolError("  Not implemented yet: store/restore before allocate of allocatable " + lhsTree + " in " + adEnv.curUnit());
            }
            if (tbrPrimal) {
                fwdPrimalRefDescriptor.toBranchVariable = flowGraphDifferentiator().toBranchVariable;
                Tree popTree = fwdPrimalRefDescriptor.makePop();
                blockDifferentiator().addFuturePlainNodeBwd(popTree, null, false,
                        ILUtils.usedVarsInExp(lhsTree, null, false),
                        new TapList<>(lhsTree, null), null, null, false, true,
                        "Restore TBR previous value of allocated var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
            }
        }

        // Build the copied allocation for the source allocated chunk:
        newTree = ILUtils.copy(tree) ;
        annotateWithSameSourceTree(newTree.down(2), mallocTree) ; //To keep Cuda style
        blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                null, new TapList<>(lhsTree, null), null, null, false, false,
                "Allocate source var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);

        // Build the BWD sweep free/deallocate of the source allocated chunk:
        if (hasBwdSweep) {
            if (!adEnv.curUnitIsContext) {
                newTree = ILUtils.build(ILLang.op_deallocate, ILUtils.copy(lhsTree), null);
                annotateWithSameSourceTree(newTree, mallocTree) ; //To keep Cuda style
                blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                        null, new TapList<>(lhsTree, null), null, null, false, false,
                        "Deallocate source var " + ILUtils.toString(lhsTree, adEnv.curActivity()), false);
            }

            boolean mustRegister = false;
            boolean mustRegisterDiff = false;
            if (!TapEnv.get().refineADMM) {
                //Disconnects "relocated" refinement
                mustRegister = true;
                mustRegisterDiff = true;
            }
            while (!(mustRegister && mustRegisterDiff) && allocatedZones != null) {
                ZoneInfo zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(allocatedZones.head, SymbolTableConstants.ALLKIND);
                if (zoneInfo != null) {
                    mustRegister = zoneInfo.relocated || zoneInfo.rebased;
                    mustRegisterDiff = zoneInfo.relocatedDiff || zoneInfo.rebasedDiff;
                }
                allocatedZones = allocatedZones.tail;
            }
            // TODO: we never tested how to do the following in Fortran:
            if ((mustRegister || mustRegisterDiff) && curDiffUnit().isC()) {
                adEnv.usesADMM = true;
                // Build the registering of both the source allocated chunk and its DIFF (if it exists):
                Tree dimensionsTree = mallocTree.down(2);
                if (!ILUtils.isNullOrNone(dimensionsTree)) {
                    if (dimensionsTree.length() > 1) {
                        TapEnv.toolWarning(-1, "(Differentiate allocation) ADMM_register of multi-dimensional array " + dimensionsTree + " not implemented yet");
                        dimensionsTree = ILUtils.build(ILLang.op_intCst, "0");
                        dimensionsTree.setAnnotation("preComments", ILUtils.build(ILLang.op_comments,
                                ILUtils.build(ILLang.op_stringCst, "UnknownSize")));
                    } else if (dimensionsTree.length() == 0) {
                        dimensionsTree = null;
                    } else {
                        dimensionsTree = dimensionsTree.down(1);
                        if (dimensionsTree.opCode() == ILLang.op_arrayTriplet) {
                            if (dimensionsTree.down(1).opCode() != ILLang.op_intCst || dimensionsTree.down(1).intValue() != 1
                                    || !ILUtils.isNullOrNone(dimensionsTree.down(3))) {
                                TapEnv.toolWarning(-1, "(Differentiate allocation) cannot manage triplet dimension size: " + dimensionsTree);
                            }
                            dimensionsTree = dimensionsTree.down(2);
                        } else if (dimensionsTree.opCode() == ILLang.op_dimColon) {
                            if (dimensionsTree.down(1).opCode() != ILLang.op_intCst || dimensionsTree.down(1).intValue() != 1) {
                                TapEnv.toolWarning(-1, "(Differentiate allocation) cannot manage colon dimension size: " + dimensionsTree);
                            }
                            dimensionsTree = dimensionsTree.down(2);
                        }
                    }
                }
                if (adEnv.traceCurDifferentiation && adEnv.traceCurBlock != 0) {
                    TapEnv.printlnOnTrace("        >> " + lhsTree + " FORTRAN ALLOCATABLE POINTER:"
                            + isFortranAllocatable + " ALLOCATABLE AND TARGET: "
                            + curBlock().symbolTable.isAllocatableAndTarget(lhsTree));
                }
                if (!isFortranAllocatable || curBlock().symbolTable.isAllocatableAndTarget(lhsTree)) {
                    if (adEnv.curUnit().isFortran9x()) {
                        lhsTree = ILUtils.buildCLoc(ILUtils.copy(lhsTree), "C_LOC");
                        if (diffVarRefFwd != null) {
                            diffVarRefFwd = ILUtils.buildCLoc(ILUtils.copy(diffVarRefFwd), "C_LOC");
                        }
                        if (diffVarRefBwd != null) {
                            diffVarRefBwd = ILUtils.buildCLoc(ILUtils.copy(diffVarRefBwd), "C_LOC");
                        }
                        adEnv.curDiffUnitPushesPointers = true;
                    }
                    if (mustDiffOperation) {
                        Tree mallocDiffSize = ILUtils.copy(mallocTree.down(1));
                        Tree sizeofCall = ILUtils.findSizeofCall(mallocDiffSize);
                        if (sizeofCall != null) {
                            sizeofCall.setChild(varRefDifferentiator().differentiateTypeTree(sizeofCall.cutChild(1), fwdSymbolTable), 1);
                        }
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_registerShadowed"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(lhsTree),
                                        ILUtils.copy(curDiffUnit().isC() ? c_NULL : lhsTree),
                                        ILUtils.isNullOrNone(mallocTree.down(1)) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(mallocTree.down(1)),
                                        ILUtils.copy(diffVarRefFwd),
                                        ILUtils.build(ILLang.op_cast,
                                                ILUtils.build(ILLang.op_pointerType,
                                                        ILUtils.build(ILLang.op_pointerType,
                                                                ILUtils.build(ILLang.op_void))),
                                                ILUtils.addAddressOf(ILUtils.copy(diffVarRefFwd))),
                                        ILUtils.copy(curDiffUnit().isC() ? c_NULL : diffVarRefFwd),
                                        ILUtils.isNullOrNone(mallocDiffSize) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                mallocDiffSize,
                                        ILUtils.isNullOrNone(dimensionsTree) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(dimensionsTree)));
                    } else {
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_register"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(lhsTree),
                                        ILUtils.build(ILLang.op_cast,
                                                ILUtils.build(ILLang.op_pointerType,
                                                        ILUtils.build(ILLang.op_pointerType,
                                                                ILUtils.build(ILLang.op_void))),
                                                ILUtils.addAddressOf(ILUtils.copy(lhsTree))),
                                        ILUtils.copy(curDiffUnit().isC() ? c_NULL : lhsTree),
                                        ILUtils.isNullOrNone(mallocTree.down(1)) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(mallocTree.down(1)),
                                        ILUtils.isNullOrNone(dimensionsTree) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(dimensionsTree)));
                    }
                    if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) {
                        ILUtils.getArguments(newTree).addChild(BlockDifferentiator.debugLabel((adEnv.curUnitIsContext ? "Ctx" : "FW") + "Register", numAlloc), -1);
                    }
                    blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                            new TapList<>(lhsTree, null), null,
                            mustDiffOperation ? new TapList<>(lhsTree, null) : null, null, false, false,
                            "Register allocated source " + (mustDiffOperation ? "and DIFF vars " : "var ") + ILUtils.toString(lhsTree, adEnv.curActivity()), true);
                }

                // Build the unregistering of both the source allocated chunk and its DIFF (if it exists):
                if (!adEnv.curUnitIsContext) {
                    Tree refChunklength = flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable);
                    if (curDiffUnit().isC()) {
                        refChunklength = ILUtils.build(ILLang.op_address, refChunklength);
                    }
                    if (mustDiffOperation) {
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_unregisterShadowed"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(lhsTree),
                                        ILUtils.copy(diffVarRefBwd),
                                        refChunklength));
                    } else {
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_unregister"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(lhsTree),
                                        refChunklength));
                    }
                    if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) {
                        ILUtils.getArguments(newTree).addChild(BlockDifferentiator.debugLabel("BWRegister", numAlloc), -1);
                    }
                    blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                            new TapList<>(lhsTree, null), null,
                            mustDiffOperation ? new TapList<>(lhsTree, null) : null, null, false, false,
                            "Un-Register allocated source " + (mustDiffOperation ? "and DIFF vars " : "var ") + ILUtils.toString(lhsTree, adEnv.curActivity()), true);
                }
            }
        }
    }

    /**
     * Build the derivative instructions that come from the given source "tree",
     * which is a deallocation call. This builds the list that must go into
     * the block of the forward sweep (in their future final execution order)
     * and the list that must go into the block of the backward sweep
     * (in the inverse of their future final execution order)
     * Iff there is a backward sweep, then we use ADMM to register memory chunks.
     */
    protected void buildDiffInstructionsOfDeallocate(Tree tree, int differentiationMode, boolean deallocateIsLive,
                                                     SymbolTable fwdSymbolTable, SymbolTable bwdSymbolTable,
                                                     BoolVector beforeActiv, BoolVector afterActiv,
                                                     BoolVector afterReqX, BoolVector beforeAvlX,
                                                     BoolVector beforeTBR, BoolVector beforeTBROnDiffPtr) {
        // Quick implementation of: "if dealloc is not Live, generate nothing in FWD and BWD"
        if (!deallocateIsLive) {
            return;
        }
        boolean mustDiffOperation =
                blockDifferentiator().pointerIsActiveHere(tree.down(1), beforeActiv, afterActiv, beforeAvlX, afterReqX);
        // Use ADMM only when there will be a backward sweep:
        boolean hasBwdSweep = differentiationMode == DiffConstants.ADJOINT_MODE
                || differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE;
        Tree diffVarRefFwd = null, diffVarRefBwd = null, diffBaseFwd = null, diffBaseBwd = null;
        Tree newMallocTree = null, newMallocDiffTree = null;
        Tree rootForPush = null, rootForPop = null;
        Tree newTree;
        TapList treeOfNonTBRPointers = null, treeOfNonTBRDiffPointers = null, treeOfNonTBRZones = null;
        Tree pushTreePrimal = null, pushTreeOnDiffPtr = null;
        boolean tbrPrimal = false, tbrOnDiffPtr = false;
        boolean rebasePrimal = false, rebaseOnDiffPtr = false;
        RefDescriptor refDescriptor;

        boolean isAllocatable = adEnv.curSymbolTable().varHasModifier(tree.down(1), "allocatable");
        if (bwdSymbolTable != null && isAllocatable && !adEnv.curUnitIsContext) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(AD07) Differentiating de-allocation needs to restore allocatable pointer " + ILUtils.toString(tree) + ", not implemented yet");
        }

        Tree destinationTree = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(tree.down(1)));
        if (!curDiffUnit().isC()) {
            destinationTree = ILUtils.build(ILLang.op_arrayAccess, destinationTree);
        }
        TapList exprZonesTree = adEnv.curSymbolTable().treeOfZonesOfValue(destinationTree, null, adEnv.curInstruction(), null);
        TapIntList deallocatedZones = ZoneInfo.listAllZones(exprZonesTree, false);

        if (hasBwdSweep && (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM())) {
            ++numDealloc;
        }

        if (hasBwdSweep) {
            if (flowGraphDifferentiator().toChunklengthVariable.head == null) {
                flowGraphDifferentiator().toChunklengthVariable.head =
                        new NewSymbolHolder("chunklength", curDiffUnit(), adEnv.integerTypeSpec, -1);
            }
            flowGraphDifferentiator().toChunklengthVariable.head.declarationLevelMustInclude(bwdSymbolTable);
            if (flowGraphDifferentiator().toFwdChunklengthVariable.head == null) {
                flowGraphDifferentiator().toFwdChunklengthVariable.head =
                        new NewSymbolHolder("chunklength", curFwdDiffUnit(), adEnv.integerTypeSpec, -1);
            }
            flowGraphDifferentiator().toFwdChunklengthVariable.head.declarationLevelMustInclude(fwdSymbolTable);
        }

        // Prepare utility objects for the coming fwd-bwd instruction pairs:
        if (hasBwdSweep && !adEnv.curUnitIsContext) {
            if (flowGraphDifferentiator().toChunkoldVariable.head == null) {
                flowGraphDifferentiator().toChunkoldVariable.head =
                        new NewSymbolHolder("chunkold", curDiffUnit(),
                                new WrapperTypeSpec(new PointerTypeSpec(
                                        curDiffUnit().isC() ? new WrapperTypeSpec(new VoidTypeSpec()) : adEnv.realTypeSpec,
                                        null)), -1);
            }
            flowGraphDifferentiator().toChunkoldVariable.head.declarationLevelMustInclude(bwdSymbolTable);
            if (mustDiffOperation) {
                if (flowGraphDifferentiator().toDiffchunkoldVariable.head == null) {
                    flowGraphDifferentiator().toDiffchunkoldVariable.head =
                            new NewSymbolHolder("diffchunkold", curDiffUnit(),
                                    new WrapperTypeSpec(new PointerTypeSpec(
                                            curDiffUnit().isC() ? new WrapperTypeSpec(new VoidTypeSpec()) : adEnv.realTypeSpec,
                                            null)), -1);
                }
                flowGraphDifferentiator().toDiffchunkoldVariable.head.declarationLevelMustInclude(bwdSymbolTable);
            }
            WrapperTypeSpec allocType = adEnv.curSymbolTable().typeOf(tree.down(1));
            if (TypeSpec.isA(allocType, SymbolTableConstants.POINTERTYPE)) {
                allocType = ((PointerTypeSpec) allocType.wrappedType).destinationType;
            }
            if (TypeSpec.isA(allocType, SymbolTableConstants.ARRAYTYPE)) {
                allocType = allocType.wrappedType.elementType();
            }
            Tree typeTree = allocType == null ? null : allocType.generateTree(bwdSymbolTable, null, null, true, null);
            int typeSize = allocType == null ? 0 : allocType.size();
            if (curDiffUnit().isC() && TypeSpec.isA(allocType, SymbolTableConstants.COMPOSITETYPE))
            //in C, don't trust Tapenade's estimation of the size of a composite type:
            {
                typeSize = 0;
            }
            newMallocTree = ILUtils.build(ILLang.op_allocate,
                    ILUtils.build(ILLang.op_mul,
                            typeSize == 0
                                    ? ILUtils.build(ILLang.op_sizeof, ILUtils.copy(typeTree))
                                    : ILUtils.build(ILLang.op_intCst, typeSize),
                            flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable)),
                    ILUtils.build(ILLang.op_expressions,
                            flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable)),
                    ILUtils.copy(typeTree),
                    null);
            if (mustDiffOperation) {
                newMallocDiffTree =
                        varRefDifferentiator().diffVarRef(adEnv.curActivity(), newMallocTree, bwdSymbolTable,
                                false, newMallocTree, false, true, null);
            }
        }

        if (hasBwdSweep && !adEnv.curUnitIsContext) {
            //Potential problem: building explicitly an arrayAccess like below is a way to force the coming
            // makePush/Pop/DiffInitialization() to use the correct size (as we know it from the recent allocation).
            // But this would cause the access protection to be on the arrayAccess, which is wrong.
            //This problem is masked as long as we don't ask for access protection (cf mayProtectAccesses below).
            if (curDiffUnit().isC()) {
                rootForPush = ILUtils.build(ILLang.op_pointerAccess,
                        ILUtils.copy(tree.down(1)),
                        ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, 0), // This is C: indices start at 0!
                                ILUtils.build(ILLang.op_sub,
                                        flowGraphDifferentiator().toFwdChunklengthVariable.head.makeNewRef(fwdSymbolTable),
                                        ILUtils.build(ILLang.op_intCst, 1))));
                rootForPop = ILUtils.build(ILLang.op_pointerAccess,
                        ILUtils.copy(tree.down(1)),
                        ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, 0), // This is C: indices start at 0!
                                ILUtils.build(ILLang.op_sub,
                                        flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable),
                                        ILUtils.build(ILLang.op_intCst, 1))));
            } else {
                rootForPush = ILUtils.build(ILLang.op_arrayAccess,
                        ILUtils.build(ILLang.op_pointerAccess,
                                ILUtils.copy(tree.down(1)),
                                ILUtils.build(ILLang.op_intCst, 0)),
                        buildFullDimColons(
                                ILUtils.build(ILLang.op_expressions,
                                        flowGraphDifferentiator().toFwdChunklengthVariable.head.makeNewRef(fwdSymbolTable)),
                                true)); // This is F90: indices start at 1!
                rootForPop = ILUtils.build(ILLang.op_arrayAccess,
                        ILUtils.build(ILLang.op_pointerAccess,
                                ILUtils.copy(tree.down(1)),
                                ILUtils.build(ILLang.op_intCst, 0)),
                        buildFullDimColons(
                                ILUtils.build(ILLang.op_expressions,
                                        flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable)),
                                true)); // This is F90: indices start at 1!
            }
        }

        if (mustDiffOperation) {
            diffVarRefFwd = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tree.down(1), fwdSymbolTable,
                    false, tree, false, true, null);
            diffBaseFwd = ILUtils.baseTree(diffVarRefFwd);
            if (hasBwdSweep && !adEnv.curUnitIsContext) {
                diffVarRefBwd = varRefDifferentiator().diffVarRef(adEnv.curActivity(), tree.down(1), bwdSymbolTable,
                        false, tree, false, true, null);
                diffBaseBwd = ILUtils.baseTree(diffVarRefBwd);
            }
        }

        if (hasBwdSweep && !adEnv.curUnitIsContext) {
            treeOfNonTBRZones =
                    DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZonesAll(exprZonesTree, beforeTBR.not(),
                            null, SymbolTableConstants.ALLKIND, adEnv.curSymbolTable());
            refDescriptor = new RefDescriptor(rootForPush, null,
                    adEnv.curSymbolTable(), fwdSymbolTable, null,
                    null, false, null, curFwdDiffUnit());
            refDescriptor.prepareForStack(curFwdDiffUnit(), 0/*ppLabel*/, treeOfNonTBRZones, true);
            refDescriptor.mayProtectAccesses = false;
            pushTreePrimal = refDescriptor.makePush();
            if (pushTreePrimal != null) {
                tbrPrimal = true;
                rebasePrimal = DataFlowAnalyzer.mayPointToRelocated(tree.down(1), false, adEnv.curInstruction(),
                        adEnv.curSymbolTable(), differentiationMode == DiffConstants.ADJOINT_MODE);
            }

            if (mustDiffOperation) {
                treeOfNonTBRDiffPointers =
                        DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZonesAll(exprZonesTree, beforeTBROnDiffPtr.not(),
                                null, SymbolTableConstants.PTRKIND, adEnv.curSymbolTable());
                refDescriptor = new RefDescriptor(rootForPush, null,
                        adEnv.curSymbolTable(), fwdSymbolTable, null,
                        diffBaseFwd, true, null, curFwdDiffUnit());
                refDescriptor.prepareForStack(curFwdDiffUnit(), 0/*ppLabel*/, treeOfNonTBRDiffPointers, true);
                refDescriptor.mayProtectAccesses = false;
                pushTreeOnDiffPtr = refDescriptor.makePush();
                if (pushTreeOnDiffPtr != null) {
                    tbrOnDiffPtr = true;
                    rebaseOnDiffPtr = DataFlowAnalyzer.mayPointToRelocated(tree.down(1), true, adEnv.curInstruction(),
                            adEnv.curSymbolTable(), differentiationMode == DiffConstants.ADJOINT_MODE);
                }
            }

            if (tbrPrimal || tbrOnDiffPtr) {
                BoolVector beforeTBRonPointers =
                        DataFlowAnalyzer.changeKind(beforeTBR, adEnv.curVectorLen, SymbolTableConstants.ALLKIND, adEnv.curPtrVectorLen, SymbolTableConstants.PTRKIND, adEnv.curSymbolTable());
                treeOfNonTBRPointers =
                        DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZonesAll(exprZonesTree, beforeTBRonPointers.not(),
                                null, SymbolTableConstants.PTRKIND, adEnv.curSymbolTable());
            }

        }

        // Build the un-registering of both the source deallocated chunk and its DIFF (if it exists):
        TapIntList inDeallocatedZones = deallocatedZones;
        boolean mustRegister = false;
        boolean mustRegisterDiff = false;
        if (!TapEnv.get().refineADMM || differentiationMode == DiffConstants.ADJOINT_MODE && !TapEnv.removeDeadPrimal()) {
            mustRegister = true;
            mustRegisterDiff = true; //Disconnects "relocated" refinement
        }
        while (!(mustRegister && mustRegisterDiff) && inDeallocatedZones != null) {
            ZoneInfo zoneInfo = adEnv.curSymbolTable().declaredZoneInfo(inDeallocatedZones.head, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null) {
                mustRegister = zoneInfo.relocated || zoneInfo.rebased;
                mustRegisterDiff = zoneInfo.relocatedDiff || zoneInfo.rebasedDiff;
            }
            inDeallocatedZones = inDeallocatedZones.tail;
        }
        if (hasBwdSweep && (mustRegister || mustRegisterDiff)) {
            Tree tree1 = tree.down(1);
            Tree refChunklength = flowGraphDifferentiator().toFwdChunklengthVariable.head.makeNewRef(fwdSymbolTable);
            if (curDiffUnit().isC()) {
                adEnv.usesADMM = true;
                Tree diffVarRefFwdCp = diffVarRefFwd == null ? null : ILUtils.copy(diffVarRefFwd);
                Tree diffVarRefBwdCp = diffVarRefBwd == null ? null : ILUtils.copy(diffVarRefBwd);
                if (adEnv.curUnit().isFortran9x()
                    && (!curBlock().symbolTable.varHasModifier(tree1, "allocatable")
                        || curBlock().symbolTable.isAllocatableAndTarget(tree1))) {
                    tree1 = ILUtils.buildCLoc(ILUtils.copy(tree1), "C_LOC");
                    if (diffVarRefFwd != null) {
                        diffVarRefFwdCp = ILUtils.buildCLoc(diffVarRefFwd, "C_LOC");
                    }
                    if (diffVarRefBwd != null) {
                        diffVarRefBwdCp = ILUtils.buildCLoc(diffVarRefBwd, "C_LOC");
                    }
                }
                if (diffVarRefBwdCp == null) {
                    diffVarRefBwdCp = ILUtils.build(ILLang.op_ident, adEnv.curUnit().isFortran9x() ? "C_NULL_PTR" : "NULL");
                }
                if (curDiffUnit().isC()) {
                    refChunklength = ILUtils.build(ILLang.op_address, refChunklength);
                }
                if (mustDiffOperation) {
                    newTree = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "ADMM_unregisterShadowed"),
                            ILUtils.build(ILLang.op_expressions,
                                    ILUtils.copy(tree1),
                                    ILUtils.copy(diffVarRefFwdCp),
                                    refChunklength));
                } else {
                    newTree = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "ADMM_unregister"),
                            ILUtils.build(ILLang.op_expressions,
                                    ILUtils.copy(tree1),
                                    refChunklength));
                }
                if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) {
                    ILUtils.getArguments(newTree).addChild(BlockDifferentiator.debugLabel((adEnv.curUnitIsContext ? "Ctx" : "FW") + "Unregister", numDealloc), -1);
                }
                blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                        new TapList<>(tree.down(1), null), null,
                        mustDiffOperation ? new TapList<>(tree.down(1), null) : null, null, false, false,
                        "Un-Register allocated source " + (mustDiffOperation ? "and DIFF vars " : "var ") + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);

                // Build the re-registering of both the source deallocated chunk and its DIFF (if it exists):
                if (!adEnv.curUnitIsContext) {
                    adEnv.usesADMM = true;
                    Tree toChunkTree = flowGraphDifferentiator().toChunkoldVariable.head.makeNewRef(bwdSymbolTable);
                    boolean isAllocatableArray = adEnv.curSymbolTable().varHasModifier(tree, "allocatable");
                    if (adEnv.curUnit().isFortran9x() && !isAllocatableArray) {
                        toChunkTree = ILUtils.buildCLoc(toChunkTree, "C_LOC");
                    }
                    if (mustDiffOperation) {
                        Tree toDiffChunkTree = flowGraphDifferentiator().toDiffchunkoldVariable.head.makeNewRef(bwdSymbolTable);
                        if (adEnv.curUnit().isFortran9x() && !isAllocatableArray) {
                            toDiffChunkTree = ILUtils.buildCLoc(toDiffChunkTree, "C_LOC");
                        }
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_registerShadowed"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(tree1),
                                        toChunkTree,
                                        ILUtils.isNullOrNone(newMallocTree.down(1)) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(newMallocTree.down(1)),
                                        ILUtils.copy(diffVarRefBwdCp),
                                        ILUtils.build(ILLang.op_cast,
                                                ILUtils.build(ILLang.op_pointerType,
                                                        ILUtils.build(ILLang.op_pointerType,
                                                                ILUtils.build(ILLang.op_void))),
                                                ILUtils.addAddressOf(ILUtils.copy(diffVarRefBwdCp))),
                                        toDiffChunkTree,
                                        ILUtils.isNullOrNone(newMallocDiffTree.down(1)) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(newMallocDiffTree.down(1)),
                                        flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable)));
                    } else {
                        newTree = ILUtils.buildCall(
                                ILUtils.build(ILLang.op_ident, "ADMM_register"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(tree1),
                                        ILUtils.build(ILLang.op_cast,
                                                ILUtils.build(ILLang.op_pointerType,
                                                        ILUtils.build(ILLang.op_pointerType,
                                                                ILUtils.build(ILLang.op_void))),
                                                ILUtils.addAddressOf(ILUtils.copy(tree1))),
                                        toChunkTree,
                                        ILUtils.isNullOrNone(newMallocTree.down(1)) ?
                                                ILUtils.build(ILLang.op_intCst, 1) :
                                                ILUtils.copy(newMallocTree.down(1)),
                                        flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable)));
                    }
                    if (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) {
                        ILUtils.getArguments(newTree).addChild(BlockDifferentiator.debugLabel("BWReregister", numDealloc), -1);
                    }
                    blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                            new TapList<>(tree.down(1), null), null,
                            mustDiffOperation ? new TapList<>(tree.down(1), null) : null, null, false, false,
                            "Re-Register allocated source " + (mustDiffOperation ? "and DIFF vars " : "var ") + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);
                }
            } else { // Fortran case
                // in Fortran, we use sizeof to retrieve the chunk length before deallocating it:
                newTree = ILUtils.build(ILLang.op_assign,
                        refChunklength,
                        ILUtils.buildCall(ILUtils.build(ILLang.op_ident, "size"),
                                ILUtils.build(ILLang.op_expressions,
                                        ILUtils.copy(tree1))));
                blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                        new TapList<>(tree.down(1), null), null, null, null, false, false,
                        "Query for length of variable " + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);
            }
        }

        // TODO: we never tested how to do the following in Fortran:
        if (hasBwdSweep && mustDiffOperation && !adEnv.curUnitIsContext && tbrOnDiffPtr && curDiffUnit().isC()) {
            // Build the save of any pointer field of the DIFF OF the deallocated chunk that is TBR OnDiffPtr:
            blockDifferentiator().addFuturePlainNodeFwd(pushTreeOnDiffPtr, null, false,
                    null, null, new TapList<>(rootForPush, null), null, false, true,
                    "Push TBR DIFF pointers before deallocation", false);
        }

        // TODO: we never tested how to do the following in Fortran:
        if (hasBwdSweep && !adEnv.curUnitIsContext && (rebasePrimal || rebaseOnDiffPtr) && curDiffUnit().isC()) {
            // Build the rebase of any pointer field of the deallocated chunk that is TBR,
            //  together with the rebase of its DIFF pointer if it is TBR OnDiffPtr:
            BoolVector beforeTBRonPointers =
                    DataFlowAnalyzer.changeKind(beforeTBR, adEnv.curVectorLen, SymbolTableConstants.ALLKIND, adEnv.curPtrVectorLen, SymbolTableConstants.PTRKIND, adEnv.curSymbolTable());
//             treeOfNonTBRPointers =
//                 DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZonesAll(exprZonesTree, beforeTBRonPointers.not(),
//                                                                      null, PTRKIND, curSymbolTable()) ;
            TapList excludableTree =
                    DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZonesAll(exprZonesTree, beforeTBRonPointers.or(beforeTBROnDiffPtr).not(),
                            null, SymbolTableConstants.PTRKIND, adEnv.curSymbolTable());
            refDescriptor = new RefDescriptor(rootForPop, null,
                    adEnv.curSymbolTable(), bwdSymbolTable, null,
                    null, false, null, curDiffUnit());
            refDescriptor.prepareForStack(curDiffUnit(), 0/*ppLabel*/, null/*excludableTree*/, true);
            refDescriptor.mayProtectAccesses = false;
            ++blockDifferentiator().numRebase;
            Tree rebaseTree = refDescriptor.makeRebase(treeOfNonTBRPointers, treeOfNonTBRDiffPointers,
                    rebaseOnDiffPtr ? diffBaseBwd : null,
                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG || TapEnv.debugADMM()) ? blockDifferentiator().numRebase : -1);
            if (rebaseTree != null) {
                blockDifferentiator().addFuturePlainNodeBwd(rebaseTree, null, false,
                        null, new TapList<>(rootForPop, null), null,
                        rebaseOnDiffPtr ? new TapList<>(rootForPop, null) : null, false, false,
                        "Rebase TBR pointers " + (rebaseOnDiffPtr ? "and their DIFFs " : " ") + "after Re-allocation", false);
            }
        }

        if (mustDiffOperation) {
            if (hasBwdSweep && !adEnv.curUnitIsContext) {
                // Build the restore of any pointer field of the DIFF OF the deallocated chunk that is TBR OnDiffPtr:
                // TODO: we never tested how to do the following in Fortran:
                if (curDiffUnit().isC()) {
                    refDescriptor = new RefDescriptor(rootForPop, null,
                            adEnv.curSymbolTable(), bwdSymbolTable, null,
                            diffBaseBwd, true, null, curDiffUnit());
                    refDescriptor.prepareForStack(curDiffUnit(), 0/*ppLabel*/, treeOfNonTBRDiffPointers, true);
                    refDescriptor.mayProtectAccesses = false;
                    Tree popTree = refDescriptor.makePop();
                    if (popTree != null) {
                        blockDifferentiator().addFuturePlainNodeBwd(popTree, null, false,
                                null, null, null, new TapList<>(rootForPop, null), false, true,
                                "Pop TBR DIFF pointers after Re-allocation", false);
                    }
                }

                // Build zero-initialization of the differentiated re-allocated chunk (except for pointer fields):
                // We have the same problem as after an allocation in tangent mode: we must consider the deallocated
                // zone active if the place where it points to *now* is active (cf hasActivePointedZone()) :
                deallocatedZones = DataFlowAnalyzer.mapZoneRkToKindZoneRk(deallocatedZones, adEnv.diffKind,
                                                                          adEnv.curSymbolTable());
                TapList pointersToIgnore =
                        DataFlowAnalyzer.buildInfoBoolTreeOfPointers(exprZonesTree, adEnv.curSymbolTable());
                if (!TapEnv.spareDiffReinitializations()
                        || !TapEnv.removeDeadPrimal() //cf set10/lh208
                        || beforeActiv != null && beforeActiv.intersects(deallocatedZones)) {
                    Tree[] newTreeR = blockDifferentiator().makeDiffInitialization(rootForPop, null, adEnv.curSymbolTable(), bwdSymbolTable,
                            null, false, pointersToIgnore, true);
                    for (int iReplic=0 ; iReplic<newTreeR.length ; ++iReplic) {
                        newTree = newTreeR[iReplic] ;
                        if (newTree != null) {
                            blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                                null, null, null, new TapList<>(tree.down(1), null), false, false,
                                "Initialize DIFF OF Re-allocated var " + ILUtils.toString(tree.down(1), adEnv.curActivity()), false);
                        }
                    }
                }

                // TODO: we never tested how to do the following in Fortran:
                if (curDiffUnit().isC()) {
                    // Build the save/restore of the old base of the DIFF OF the deallocated chunk:
                    RefDescriptor diffdeallocRefDescriptor =
                            new RefDescriptor(tree.down(1),
                                    null, curBlock().symbolTable, fwdSymbolTable, null,
                                    diffBaseFwd, true, null, curDiffUnit());
                    blockDifferentiator().prepareRestoreOperations(diffdeallocRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
                    diffdeallocRefDescriptor.mayProtectAccesses = false;
                    blockDifferentiator().addFuturePlainNodeFwd(diffdeallocRefDescriptor.makePush(), null, false,
                            null, null, new TapList<>(tree.down(1), null), null, false, true,
                            "Store deallocated DIFF chunk address", false);
                    RefDescriptor diffchunkoldRefDescriptor =
                            new RefDescriptor(flowGraphDifferentiator().toDiffchunkoldVariable.head.makeNewRef(bwdSymbolTable),
                                    null, curBlock().symbolTable, bwdSymbolTable, null,
                                    null, false, null, curDiffUnit());
                    blockDifferentiator().prepareRestoreOperations(diffchunkoldRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
                    diffchunkoldRefDescriptor.mayProtectAccesses = false;
                    blockDifferentiator().addFuturePlainNodeBwd(diffchunkoldRefDescriptor.makePop(), null, false,
                            null, null, null, null, false, true,
                            "Retrieve deallocated DIFF chunk address", true);
                }
            }

            if (!TapEnv.associationByAddress()) {
                // Build the un-allocation for the DIFF OF the deallocated chunk:
                newTree = ILUtils.build(ILLang.op_deallocate, ILUtils.copy(diffVarRefFwd), null);
                if (curDiffUnit().isFortran9x()) {
                    newTree = ILUtils.build(ILLang.op_if,
                                ILUtils.buildCall(
                                  ILUtils.build(ILLang.op_ident, isAllocatable ? "ALLOCATED" : "ASSOCIATED"),
                                  ILUtils.build(ILLang.op_expressions,
                                    ILUtils.copy(diffVarRefFwd))),
                                newTree);
                }
                annotateWithSameSourceTree(newTree, tree) ; //To keep Cuda style
                blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                        null, null, null, new TapList<>(tree.down(1), null), false, false,
                        "Un-Allocate DIFF OF var " + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);

                // Build the re-allocation for the DIFF OF the deallocated chunk:
                if (hasBwdSweep && !adEnv.curUnitIsContext) {
                    annotateWithSameSourceTree(newMallocDiffTree, tree) ; //To keep Cuda style
                    newTree = ILUtils.build(ILLang.op_assign, diffVarRefBwd, newMallocDiffTree);
                    blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                            null, null, null, new TapList<>(tree.down(1), null), false, false,
                            "Re-Allocate DIFF OF var " + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);
                }
            }
        }

        // Build the save/restore of any field of the source deallocated chunk that is TBR:
        if (hasBwdSweep && !adEnv.curUnitIsContext) {
            if (tbrPrimal) {
                blockDifferentiator().addFuturePlainNodeFwd(pushTreePrimal, null, false,
                        new TapList<>(rootForPush, null), null, null, null, false, true,
                        "Push TBR before deallocation", false);
            }

            refDescriptor = new RefDescriptor(rootForPop, null,
                    adEnv.curSymbolTable(), bwdSymbolTable, null,
                    null, false, null, curDiffUnit());
            refDescriptor.prepareForStack(curDiffUnit(), 0/*ppLabel*/, treeOfNonTBRZones, true);
            refDescriptor.mayProtectAccesses = false;
            Tree popTree = refDescriptor.makePop();
            if (popTree != null) {
                blockDifferentiator().addFuturePlainNodeBwd(popTree, null, false,
                        null, new TapList<>(rootForPop, null), null, null, false, true,
                        "Pop TBR after Re-allocation", false);
            }
        }

        // TODO: we never tested how to do the following in Fortran:
        // Build the save/restore of the old base of the source deallocated chunk:
        if (hasBwdSweep && !adEnv.curUnitIsContext && curDiffUnit().isC()) {
            RefDescriptor deallocRefDescriptor =
                    new RefDescriptor(tree.down(1),
                            null, curBlock().symbolTable, fwdSymbolTable, null,
                            null, false, null, curDiffUnit());
            blockDifferentiator().prepareRestoreOperations(deallocRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
            deallocRefDescriptor.mayProtectAccesses = false;
            blockDifferentiator().addFuturePlainNodeFwd(deallocRefDescriptor.makePush(), null, false,
                    new TapList<>(tree.down(1), null), null, null, null, false, true,
                    "Store deallocated source chunk address", false);
            RefDescriptor chunkoldRefDescriptor =
                    new RefDescriptor(flowGraphDifferentiator().toChunkoldVariable.head.makeNewRef(bwdSymbolTable),
                            null, curBlock().symbolTable, bwdSymbolTable, null,
                            null, false, null, curDiffUnit());
            blockDifferentiator().prepareRestoreOperations(chunkoldRefDescriptor, null, 0, fwdSymbolTable, bwdSymbolTable);
            chunkoldRefDescriptor.mayProtectAccesses = false;
            blockDifferentiator().addFuturePlainNodeBwd(chunkoldRefDescriptor.makePop(), null, false,
                    null, null, null, null, false, true,
                    "Retrieve deallocated source chunk address", true);
        }

        // Build the un-allocation for the source deallocated chunk:
        newTree = ILUtils.copy(tree) ;
        annotateWithSameSourceTree(newTree, tree) ; //To keep Cuda style
        blockDifferentiator().addFuturePlainNodeFwd(newTree, null, false,
                ILUtils.usedVarsInExp(tree.down(1), null, true),
                new TapList<>(tree.down(1), null), null, null, false, false,
                "Un-Allocate source var " + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);

        // Build the re-allocation for the source deallocated chunk:
        if (hasBwdSweep && !adEnv.curUnitIsContext) {
            newTree = ILUtils.build(ILLang.op_assign, ILUtils.copy(tree.down(1)), newMallocTree);
            annotateWithSameSourceTree(newMallocTree, tree) ; //To keep Cuda style
            blockDifferentiator().addFuturePlainNodeBwd(newTree, null, false,
                    null, new TapList<>(tree.down(1), null), null, null, false, false,
                    "Re-Allocate source var " + ILUtils.toString(tree.down(1), adEnv.curActivity()), true);

            // Build the save/restore of the size of the source deallocated chunk:
            RefDescriptor fwdLengthRefDescriptor =
                    new RefDescriptor(flowGraphDifferentiator().toFwdChunklengthVariable.head.makeNewRef(fwdSymbolTable),
                            null, curBlock().symbolTable, fwdSymbolTable, null,
                            null, false, null, curDiffUnit());
            RefDescriptor bwdLengthRefDescriptor =
                    new RefDescriptor(flowGraphDifferentiator().toChunklengthVariable.head.makeNewRef(bwdSymbolTable),
                            null, curBlock().symbolTable, bwdSymbolTable, null,
                            null, false, null, curDiffUnit());
            blockDifferentiator().prepareRestoreOperations(fwdLengthRefDescriptor, bwdLengthRefDescriptor, 0, fwdSymbolTable, bwdSymbolTable);
            fwdLengthRefDescriptor.mayProtectAccesses = false;
            blockDifferentiator().addFuturePlainNodeFwd(fwdLengthRefDescriptor.makePush(), null, false,
                    null, null, null, null, false, true,
                    "Store deallocated chunk length", false);
            bwdLengthRefDescriptor.mayProtectAccesses = false;
            blockDifferentiator().addFuturePlainNodeBwd(bwdLengthRefDescriptor.makePop(), null, false,
                    null, null, null, null, false, true,
                    "Retrieve deallocated chunk length", true);
        }
    }

    /**
     * @return a list of indices that are indeed op_dimColon, to tell RefDescriptor that
     * these are full dimensions.
     */
    private Tree buildFullDimColons(Tree indices, boolean firstIndexOne) {
        Tree[] oldIndices = indices.children();
        TapList<Tree> newIndices = null;
        Tree oldIndex, newIndex;
        for (int i = oldIndices.length - 1; i >= 0; --i) {
            oldIndex = oldIndices[i];
            if (oldIndex == null) {
                newIndex = null;
            } else {
                switch (oldIndex.opCode()) {
                    case ILLang.op_dimColon:
                        newIndex = ILUtils.copy(oldIndex);
                        break;
                    case ILLang.op_arrayTriplet:
                        //This case should never happen !!
                        newIndex = ILUtils.build(ILLang.op_dimColon,
                                ILUtils.copy(oldIndex.down(1)),
                                ILUtils.copy(oldIndex.down(2)));
                        break;
                    default:
                        newIndex = ILUtils.copy(oldIndex);
                        if (!firstIndexOne) {
                            if (newIndex.opCode() == ILLang.op_intCst) {
                                newIndex = ILUtils.build(ILLang.op_intCst, newIndex.intValue() - 1);
                            } else {
                                newIndex = ILUtils.build(ILLang.op_sub, newIndex, ILUtils.build(ILLang.op_intCst, 1));
                            }
                        }
                        newIndex = ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, firstIndexOne ? 1 : 0),
                                newIndex);
                }
            }
            newIndices = new TapList<>(newIndex, newIndices);
        }
        return ILUtils.build(ILLang.op_expressions, newIndices);
    }

    /** When oldTree is a malloc or a free, newTree is also a malloc or a free (possibly exchanged).
     * Attach to newTree the "sourcetree" annotation from oldTree, which is used to detect
     * those newTree's that must be decompiled as cudaMalloc's or cudaFree's */
    private void annotateWithSameSourceTree(Tree newTree, Tree oldTree) {
        Tree sourceTree = oldTree.getAnnotation("sourcetree") ;
        if (sourceTree!=null) {
            newTree.setAnnotation("sourcetree", sourceTree) ;
        }
    }

}
