/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

/**
 * Tapenade differentiation.
 * Builds a new, differentiated internal representation,
 * from the given internal representation.
 *
 * @author Inria - Ecuador team, tapenade@inria.fr
 */
package fr.inria.tapenade.differentiation;
