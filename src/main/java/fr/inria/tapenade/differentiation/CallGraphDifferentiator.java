/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.AlignmentBoundary;
import fr.inria.tapenade.representation.ArrayDim;
import fr.inria.tapenade.representation.BasicBlock;
import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.EntryBlock;
import fr.inria.tapenade.representation.ExitBlock;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.FunctionDecl;
import fr.inria.tapenade.representation.FunctionTypeSpec;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.InterfaceDecl;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.PointerTypeSpec;
import fr.inria.tapenade.representation.PrimitiveTypeSpec;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeDecl;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.UnitStorage;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.VoidTypeSpec;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.representation.MixedLanguageInfos;
import fr.inria.tapenade.representation.MemoryMaps;
import fr.inria.tapenade.representation.MemMap;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.Chrono;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Int2ZoneInfo;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * The top level of Differentiation at the Call-Graph level.
 */
public class CallGraphDifferentiator {

    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    /**
     * Access to global adEnv.flowGraphDifferentiator.
     */
    private FlowGraphDifferentiator flowGraphDifferentiator() {
        return adEnv.flowGraphDifferentiator;
    }

    /**
     * Access to global adEnv.blockDifferentiator.
     */
    private BlockDifferentiator blockDifferentiator() {
        return adEnv.blockDifferentiator;
    }

    /**
     * Access to global adEnv.procedureCallDifferentiator.
     */
    private ProcedureCallDifferentiator procedureCallDifferentiator() {
        return adEnv.procedureCallDifferentiator;
    }

    /**
     * Access to global adEnv.varRefDifferentiator.
     */
    private VarRefDifferentiator varRefDifferentiator() {
        return adEnv.varRefDifferentiator;
    }

    /**
     * Access to global adEnv.multiDirMode.
     */
    private boolean multiDirMode() {
        return adEnv.multiDirMode;
    }

    /**
     * Access to global adEnv.suffixes.
     */
    private String[][] suffixes() {
        return adEnv.suffixes;
    }

    /**
     * Access to global TapEnv.get().activityAnalyzer.
     */
    private ADActivityAnalyzer activityAnalyzer() {
        return TapEnv.adActivityAnalyzer();
    }

    /**
     * Local pointer to adEnv.srcCallGraph. Make sure it remains in sync!.
     */
    private CallGraph srcCallGraph;

    /**
     * Local pointer to adEnv.diffCallGraph. Make sure it remains in sync!.
     */
    private CallGraph diffCallGraph;

    /**
     * Local shortcut to adEnv.curUnit. Make sure it remains in sync!
     */
    private Unit curUnit;

    /**
     * Set the curUnit in adEnv, the curUnit in and its local copy "curUnit" here.
     * In adEnv, this also sets traceCurDifferentiation.
     * In this also sets the related language.
     *
     * @param unit The new current Unit.
     */
    private void setCurUnitEtc(Unit unit) {
        curUnit = unit;
        TapEnv.setRelatedUnit(unit);
        adEnv.setCurUnitEtc(unit);
    }

    /**
     * Local shortcut to adEnv.curDiffUnit. Make sure it remains in sync!.
     */
    private Unit curDiffUnit;

    /**
     * Set the curDiffUnit in adEnv and its local copy "curDiffUnit" here.
     *
     * @param diffUnit The new current diffUnit.
     */
    private void setCurDiffUnit(Unit diffUnit) {
        curDiffUnit = diffUnit;
        adEnv.curDiffUnit = diffUnit;
    }

    /**
     * Access to global adEnv.curDiffUnitSort.
     */
    private int curDiffUnitSort() {
        return adEnv.curDiffUnitSort;
    }

    private final UnitStorage<UnitDiffInfo> unitDiffInfos;

    private TapList<TapPair<Unit, UnitDiffInfo>> intrinsicUnitDiffInfos;

    /**
     * Stores decision to (re)generate a primal version of a Unit.
     */
    protected UnitStorage<Boolean> unitsHavePrimal;
    /**
     * Stores decision to generate a differentiated version of a Unit.
     */
    protected UnitStorage<Boolean> unitsHaveDiff;

    /**
     * The NewSymbolHolder that contains variable "NBDirsMax"
     * for the root SymbolTable context.
     */
    protected NewSymbolHolder rootDirNumberMaxSymbolHolder;

    /**
     * Timer for differentiation of each individual Unit.
     */
    protected final Chrono unitDiffTimer = new Chrono();

    protected CallGraphDifferentiator(DifferentiationEnv adEnv) {
        super();
        this.adEnv = adEnv;
        unitDiffInfos = new UnitStorage<>(adEnv.srcCallGraph());
        for (int i = adEnv.srcCallGraph().nbUnits() - 1; i >= 0; --i) {
            Unit unit = adEnv.srcCallGraph().sortedUnit(i);
            assert unit != null;
            unitDiffInfos.store(unit, new UnitDiffInfo(unit, adEnv));
        }
        for (TapList<Unit> intrinsicUnits = TapList.append(adEnv.srcCallGraph().dummyUnits, adEnv.srcCallGraph().intrinsicUnits())
             ; intrinsicUnits != null; intrinsicUnits = intrinsicUnits.tail) {
            intrinsicUnitDiffInfos = new TapList<>(
                    new TapPair<>(intrinsicUnits.head,
                            new UnitDiffInfo(intrinsicUnits.head, adEnv)),
                    intrinsicUnitDiffInfos);
        }
    }

    /**
     * Main entry point: Given a source CallGraph and a list of rootUnits to differentiate,
     * creates the differentiated CallGraph, equipped in field "backAssociations" with
     * the correspondence from source Units to differentiated Units.
     */
    public static CallGraph run(CallGraph origCallGraph, TapList<Unit> rootUnits,
                                String[][] suffixes) {
        DifferentiationEnv newAdEnv = new DifferentiationEnv(origCallGraph, suffixes);
        newAdEnv.callGraphDifferentiator.differentiateDownFrom(rootUnits);
        return newAdEnv.diffCallGraph();
    }

    /**
     * Sweep the callgraph down from rootUnits, generating associated
     * diffUnits for all the encountered units that are active.
     *
     * @param rootUnits the root units for differentiation
     */
    private void differentiateDownFrom(TapList<Unit> rootUnits) {
        Chrono diffTimer = new Chrono();
        TapList<Unit> units;
        BoolVector unitCallActive, unitExitActive;
        TapList<ActivityPattern> activityPatterns;
        TapList<Unit> calledPrimalFormUnits = null;

        srcCallGraph = adEnv.srcCallGraph();
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            activityPatterns = curUnit.activityPatterns;
            while (activityPatterns != null) {
                adEnv.setCurActivity(activityPatterns.head);
                computeCommonActivities(adEnv.curActivity());
                if (!curUnit.hasSource()) {
                    unitCallActive = adEnv.curActivity().callActivity();
                    unitExitActive = adEnv.curActivity().exitActivity();
                    if (unitCallActive != null && unitExitActive != null) {
                        augmentExternalEntryExitActivities(unitCallActive, unitExitActive);
                    }
                }
                activityPatterns = activityPatterns.tail;
            }
            if (curUnit.isModule()) {
                computeModuleCommonActivities(curUnit);
            }
        }
        adEnv.setCurActivity(null);
        setCurUnitEtc(null);

        boolean codeContainsSomethingActive = false;
        // TapList of original Units ordered with USE'd and CONTAIN'ing Units first:
        TapList<Unit> orderedUnitsRootSTFirst = srcCallGraph.buildSortedUnitsList(0, -1, 1);

        diffCallGraph = new CallGraph("Differentiation of " + srcCallGraph.name() + " under " + rootUnits);
        adEnv.setDiffCallGraph(diffCallGraph);
        diffCallGraph.createRootSymbolTables();
        adEnv.setDiffCallGraphAndRootUnits(diffCallGraph, rootUnits);
        diffCallGraph.usedCommonNames = srcCallGraph.usedCommonNames ;
        diffCallGraph.setBindFortranCType(srcCallGraph.bindFortranCType());
        diffCallGraph.zoneNbOfNullDest = srcCallGraph.zoneNbOfNullDest ;
        diffCallGraph.zoneNbOfUnknownDest = srcCallGraph.zoneNbOfUnknownDest ;
        diffCallGraph.zoneNbOfAllIOStreams = srcCallGraph.zoneNbOfAllIOStreams ;

        // For each procedural Unit U, for each of its activity patterns A,
        // at each call site inside U that must be differentiated, for each argument
        // that needs a derivative for activity A, annotate the actual argument
        // by raising the "isActive" flag of its SymbolDecl.
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            if (curUnit != null && curUnit.rank() >= 0 && !curUnit.isModule()) {
                activityPatterns = curUnit.activityPatterns;
                while (activityPatterns != null) {
                    adEnv.setCurActivity(activityPatterns.head);
                    annotateActiveCallArgumentsInCurUnit(adEnv.curActivity());
                    activityPatterns = activityPatterns.tail;
                }
            }
        }

        //Precompute the formal args activity of each Unit
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            if (curUnit != null && curUnit.rank() >= 0 && !curUnit.isModule()) {
                activityPatterns = curUnit.activityPatterns;
                if ((adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) && activityPatterns != null) {
                    TapEnv.printlnOnTrace(" Precompute formalArgsActivity for each activityPattern of Unit " + curUnit + ':' + curUnit.headTree());
                }
                boolean[] formalArgsActivity;
                UnitDiffInfo diffInfo = getUnitDiffInfo(curUnit);
                while (activityPatterns != null) {
                    adEnv.setCurActivity(activityPatterns.head);
                    formalArgsActivity = ADActivityAnalyzer.formalArgsActivity(adEnv.curActivity());
                    diffInfo.setUnitFormalArgsActivityS(adEnv.curActivity(), formalArgsActivity);
                    if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                        TapEnv.printOnTrace(" - wrt activity " + adEnv.curActivity() + ", formalArgsActivity is ");
                        if (formalArgsActivity == null) {
                            TapEnv.printOnTrace("null");
                        } else {
                            TapEnv.printOnTrace("[");
                            int nbArgs = Math.min(curUnit.formalArgumentsNb(), formalArgsActivity.length - 1);
                            for (int j = 0; j < formalArgsActivity.length; ++j) {
                                if (formalArgsActivity.length == 1) {
                                    TapEnv.printOnTrace("->");
                                }
                                TapEnv.printOnTrace("" + formalArgsActivity[j]);
                                if (j + 1 < formalArgsActivity.length) {
                                    TapEnv.printOnTrace(j + 1 == nbArgs ? "->" : ", ");
                                }
                            }
                            TapEnv.printlnOnTrace("]");
                        }
                    }

                    // Collect into "calledPrimalFormUnits" the Units that are called as passive, from an active calling Unit:
                    // Side-effect: may remove some actually passive ActivityPattern's
                    if (TapEnv.doActivity()
                            && (adEnv.curActivity().isActive()
                            || (adEnv.curActivity().isContext() && curUnit == srcCallGraph.getMainUnit()))) {
                        calledPrimalFormUnits = collectAllCalledPrimal(adEnv.curActivity(), calledPrimalFormUnits);
                    }

                    activityPatterns = activityPatterns.tail;
                }
            }
        }

        adEnv.setCurActivity(null);
        adEnv.setCurSymbolTable(null);
        adEnv.setCurInstruction(null);
        setCurUnitEtc(null);

        if (TapEnv.doActivity() && calledPrimalFormUnits != null) {
            calledPrimalFormUnits = Unit.allCalleesMulti(calledPrimalFormUnits);
        }
        TapList<Unit> userUnitsRootSTLast = null;
        for (units = orderedUnitsRootSTFirst; units != null; units = units.tail) {
            Unit userUnit = units.head;
            if (!userUnit.isOutside()) {
                userUnitsRootSTLast = new TapList<>(userUnit, userUnitsRootSTLast);
            }
        }
        futureUnitsDecision(userUnitsRootSTLast, calledPrimalFormUnits);

        // Specific preparation work on units before starting actual differentiation.
        // Currently used for the adjoint/reverse mode to set
        // differentiation mode on rootUnits if nothing is set: default is JOINT.
        TapList<Unit> inRootUnits = rootUnits;
        while (inRootUnits != null) {
            Unit rootUnit = inRootUnits.head;
            if (!rootUnit.maybeCheckpointed && !rootUnit.maybeNoCheckpointed) {
                rootUnit.maybeCheckpointed = true;
            }
            inRootUnits = inRootUnits.tail;
        }
        // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
        // Detect parameters that must emulate PASS-BY-REFERENCE,
        //  by filling arrays UnitDiffArgsByValueNeedOverwriteInfoS and UnitDiffArgsByValueNeedOnlyIncrementInfoS
        boolean[] diffArgsByValueNeedOverwrite, diffArgsByValueNeedOnlyIncrement;
        boolean unitHasInOut;
        int nbFormalArgs;
        BoolVector unitPublicZonesWithDiff, unitPublicZonesWritten, unitPublicZonesUsed, unitPublicZonesRead;
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            //[llh] TODO: attention danger: we rely on the fact that curUnit.unitW() still contains
            // true for the formal parameters that are passed by value => these W are filtered out
            // only when using this unitW in a calling routine. If we change our mind and filter
            // the unitW itself, then we must find the "unitPublicZonesWritten" info from somewhere else !!!
            unitHasInOut = (curUnit.unitInOutR() != null) ;
            if (unitHasInOut) {
                unitPublicZonesRead = curUnit.unitInOutPossiblyR();
                unitPublicZonesWritten = curUnit.unitInOutPossiblyW();
                unitPublicZonesUsed = unitPublicZonesRead.or(unitPublicZonesWritten) ;
            } else {
                unitPublicZonesWritten = new BoolVector(curUnit.paramElemsNb());
                unitPublicZonesWritten.setTrue();
                unitPublicZonesUsed = new BoolVector(curUnit.paramElemsNb());
                unitPublicZonesUsed.setTrue();
                unitPublicZonesRead = null ;
            }
            UnitDiffInfo diffInfo = getUnitDiffInfo(curUnit);
            activityPatterns = curUnit.activityPatterns;
            while (activityPatterns != null) {
                adEnv.setCurActivity(activityPatterns.head);
                if (activityAnalyzer().isActiveUnit(adEnv.curActivity()) || !TapEnv.doActivity()) {
                    unitPublicZonesWithDiff = adEnv.curActivity().callActivity().or(adEnv.curActivity().exitActivity());
                    unitPublicZonesWithDiff = curUnit.unfocusFromKind(unitPublicZonesWithDiff, adEnv.diffKind) ;
                    if (unitHasInOut && adEnv.curActivity().entryReqX() != null) {
                        // still this disturbing asymmetry between ReqX and AvlX... :
                        // Check consistency with ReqExplicit.isPointerActiveArg()!
                        unitPublicZonesWithDiff.cumulOr(adEnv.curActivity().entryReqX().and(unitPublicZonesRead));
                        unitPublicZonesWithDiff.cumulOr(adEnv.curActivity().entryAvlX().or(adEnv.curActivity().exitReqX()).and(unitPublicZonesWritten));
                    }
// System.out.println(curUnit+" FTS:"+curUnit.functionTypeSpec()+" argtypes.length:"+(curUnit.functionTypeSpec().argumentsTypes==null?"zero":curUnit.functionTypeSpec().argumentsTypes.length)) ;
                    if (curUnit.functionTypeSpec().argumentsTypes != null) {
                        nbFormalArgs = 1 + curUnit.functionTypeSpec().argumentsTypes.length; // one more for the function's result.
                    } else {
                        nbFormalArgs = 1; // for the function's result.
                    }
                    BoolVector nonZeroAdjointComing =
                        (curUnit.isStandard() ? adEnv.curActivity().nonZeroAdjointComing() : null) ;
// System.out.println("unitPublicZonesUsed:"+unitPublicZonesUsed+" unitPublicZonesWritten:"+unitPublicZonesWritten+" unitPublicZonesWithDiff:"+unitPublicZonesWithDiff+" nonZeroAdjointComing:"+nonZeroAdjointComing) ;
                    diffArgsByValueNeedOverwrite = new boolean[nbFormalArgs];
                    diffArgsByValueNeedOnlyIncrement = new boolean[nbFormalArgs];
                    for (int j = nbFormalArgs - 1; j >= 0; j--) {
                        diffArgsByValueNeedOverwrite[j] = false;
                        diffArgsByValueNeedOnlyIncrement[j] = true;
// System.out.println((j==0 ? "RESULT" : "ARG "+j)+" byvalue?"+(j==0 || curUnit.takesArgumentByValue(j, curUnit.language()))) ;
                        if (// unitPublicZonesWithDiff!=null &&
                                j == 0 || curUnit.takesArgumentByValue(j, curUnit.language())) {
// System.out.println((j==0 ? "RESULT" : "ARG "+j)+" ARGTREE:"+curUnit.argsPublicRankTrees[j]) ;
                            fillPassByValueInfos(curUnit.argsPublicRankTrees[j], j, curUnit,
                                    unitPublicZonesUsed, unitPublicZonesWritten, unitPublicZonesWithDiff, nonZeroAdjointComing,
                                    diffArgsByValueNeedOverwrite, diffArgsByValueNeedOnlyIncrement);
                        }
                    }
                    diffInfo.setUnitDiffArgsByValueNeedOverwriteInfoS(adEnv.curActivity(), diffArgsByValueNeedOverwrite);
                    diffInfo.setUnitDiffArgsByValueNeedOnlyIncrementInfoS(adEnv.curActivity(), diffArgsByValueNeedOnlyIncrement);
// System.out.print("DIFFARGSBYVALUE NEED OVERWRITE/ONLYINCREMENT OF "+curUnit+"(AP@"+Integer.toHexString(adEnv.curActivity().hashCode())+") [");
// for (int j=1 ; j<nbFormalArgs ; ++j) System.out.print(diffArgsByValueNeedOverwrite[j]+"/"+diffArgsByValueNeedOnlyIncrement[j]+" ") ;
// TapEnv.printlnOnTrace("]=> "+diffArgsByValueNeedOverwrite[0]+"/"+diffArgsByValueNeedOnlyIncrement[0]) ;
                }
                activityPatterns = activityPatterns.tail;
            }
        }
        adEnv.setCurActivity(null);
        setCurUnitEtc(null);

        // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.

        // STEP A: create all the Units of the differentiated CallGraph,
        // respecting inclusion (contains), but leaving them mostly empty.
        // Must follow an order such that CONTAIN'ing Units are done first.
        diffCallGraph.topBlock = new BasicBlock(diffCallGraph.globalRootSymbolTable(), null, null);
        diffCallGraph.topBlock.copyInstructions(srcCallGraph.topBlock, adEnv.copiedIncludes);

        if (srcCallGraph.nbUnits() > 0) {
            Unit topUnit = srcCallGraph.sortedUnit(0);
            assert topUnit != null;
            if (!topUnit.isFortran()) {
                diffCallGraph.topPreComments =
                        srcCallGraph.topPreComments == null
                                ? null : srcCallGraph.topPreComments.copy();
                diffCallGraph.topPostComments =
                        srcCallGraph.topPostComments == null
                                ? null : srcCallGraph.topPostComments.copy();
                diffCallGraph.topPreCommentsBlock =
                        srcCallGraph.topPreCommentsBlock == null
                                ? null : srcCallGraph.topPreCommentsBlock.copy();
                diffCallGraph.topPostCommentsBlock =
                        srcCallGraph.topPostCommentsBlock == null
                                ? null : srcCallGraph.topPostCommentsBlock.copy();
            }
        }

        int packageDiffSort = (TapEnv.mustTangent() ? DiffConstants.TANGENT : (TapEnv.mustAdjoint() ? DiffConstants.ADJOINT : DiffConstants.ORIGCOPY));
        for (units = orderedUnitsRootSTFirst; units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            UnitDiffInfo diffInfo = getUnitDiffInfo(curUnit);
            if (curUnit.isPackage() && !curUnit.isUndefined()) {
                Unit enclosingUnit = curUnit.upperLevelUnit();
                Unit diffEnclosingUnitOfDiff = null;
                if (enclosingUnit != null) {
                    diffEnclosingUnitOfDiff = getUnitDiffInfo(enclosingUnit).getDiff();
                }
                Unit diffPackage = curUnit.copyUnitExceptSymbolTables(diffCallGraph, diffEnclosingUnitOfDiff, null);
                initializeDiffUnit(diffPackage, curUnit, packageDiffSort, -1);
                if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                    TapEnv.printlnOnTrace(" =Prepared diff package for " + curUnit + " => " + diffPackage + ", contained in " + diffPackage.upperLevelUnit());
                }
                diffInfo.setDiff(diffPackage);
                if (curUnit.isPredefinedModuleOrTranslationUnit()) {
                    diffPackage.setName(curUnit.name());
                    diffPackage.setPredefinedModuleOrTranslationUnit();
                } else if (unitsHaveDiff.retrieve(curUnit) == Boolean.FALSE && unitsHavePrimal.retrieve(curUnit) == Boolean.FALSE) {
                    // pour detecter les modules non differenties:
                    diffPackage.setName(curUnit.name());
                    diffPackage.setIsDiffPackage(!TapEnv.get().stripPrimalModules);
                } else {
                    diffPackage.setIsDiffPackage(true);
                }
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace(" ---> " + curUnit + " => " + diffPackage + " isDiffPackage?:" + diffPackage.isDiffPackage());
                }
            }
        }

        for (units = orderedUnitsRootSTFirst; units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            UnitDiffInfo diffInfo = getUnitDiffInfo(curUnit);
            if (!curUnit.isPackage() && unitsHavePrimal.retrieve(curUnit) == Boolean.TRUE) {
                Unit container = curUnit.upperLevelUnit();
                Unit containerOfInterface = curUnit.enclosingUnitOfInterface;
                Unit principalDiffContainer = container == null ? null :
                        container.isPackage() ?
                                getUnitDiffInfo(container).getDiff() :
                                diffInfo.getAnyContainingDiffUnit(curUnit, container);
                Unit principalDiffInterface = containerOfInterface == null ? null :
                        containerOfInterface.isPackage() ?
                                getUnitDiffInfo(containerOfInterface).getDiff() :
                                diffInfo.getAnyContainingDiffUnit(curUnit, containerOfInterface);
                Unit copiedUnit =
                        curUnit.copyUnitExceptSymbolTables(diffCallGraph, principalDiffContainer, principalDiffInterface);
                copiedUnit.origUnit = curUnit;
                copiedUnit.setKind(curUnit);
                copiedUnit.sortOfDiffUnit = DiffConstants.ORIGCOPY;
                copiedUnit.arrayDimMin = curUnit.isFortran() ? 1 : 0;
                if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                    TapEnv.printlnOnTrace(" =Prepared copied unit for " + curUnit + " => " + copiedUnit + ", contained in " + copiedUnit.upperLevelUnit());
                }
                adEnv.copiedUnits.store(curUnit, copiedUnit);
                adEnv.copiedUnitsBack.store(copiedUnit, curUnit);
                TapList<Unit> diffsOfContainer =
                        container == null ? null : getUnitDiffInfo(container).getAllDiffsPlusCopy();
                Unit diffContainer;
                while (diffsOfContainer != null) {
                    // The copiedUnit must be shared between all differentiated versions of
                    // the original container, and therefore must appear in the
                    // lowerLevelUnits lists of each of them. cf F90:v304.
                    diffContainer = diffsOfContainer.head;
                    if (diffContainer != principalDiffContainer) {
                        diffContainer.addLowerLevelUnit(copiedUnit);
                        CallGraph.addCallArrow(diffContainer, SymbolTableConstants.CONTAINS, copiedUnit);
                    }
                    diffsOfContainer = diffsOfContainer.tail;
                }
            } else {
                curUnit.origUnit = curUnit;
            }
            if (curUnit.isProcedure() || curUnit.isVarFunction() || curUnit.isInterface()) {
                activityPatterns = curUnit.activityPatterns;
                if (activityPatterns == null
                        && unitsHaveDiff.retrieve(curUnit) == Boolean.TRUE) {
                    ActivityPattern dummyActivityPattern = new ActivityPattern(curUnit, true, false, adEnv.diffKind);
                    int nbArgs = curUnit.functionTypeSpec().argumentsTypes.length;
                    boolean[] allFalseActivity = new boolean[nbArgs + 1];
                    for (int i = nbArgs; i >= 0; --i) {
                        allFalseActivity[i] = false;
                    }
                    diffInfo.setUnitFormalArgsActivityS(dummyActivityPattern, allFalseActivity);
                    curUnit.activityPatterns = new TapList<>(dummyActivityPattern, null);
                    activityPatterns = curUnit.activityPatterns;
                }
                while (activityPatterns != null) {
                    adEnv.setCurActivity(activityPatterns.head);
                    if (patternCreatesDiffCode(adEnv.curActivity(), curUnit)) {
                        createDiffUnits(curUnit, adEnv.curActivity());
                        codeContainsSomethingActive = true;
                    }
                    activityPatterns = activityPatterns.tail;
                }
                adEnv.setCurActivity(null);
            }
        }
        for (TapList<Unit> intrinsicUnits = TapList.append(srcCallGraph.dummyUnits, srcCallGraph.intrinsicUnits())
             ; intrinsicUnits != null; intrinsicUnits = intrinsicUnits.tail) {
            Unit intrinsicUnit = intrinsicUnits.head;
            if (!intrinsicUnit.hasPredefinedDerivatives()) {
                setCurUnitEtc(intrinsicUnit);
                activityPatterns = curUnit.activityPatterns;
                while (activityPatterns != null) {
                    if (patternCreatesDiffCode(activityPatterns.head, curUnit)) {
                        createDiffUnits(curUnit, activityPatterns.head);
                    }
                    activityPatterns = activityPatterns.tail;
                }
            }
        }

        // STEP B: put the CallArrow's between the Units of the differentiated CallGraph.
        // Order is not important, since all these units are now created.
        Unit destUnit, diffDestUnit;
        CallArrow origCallArrow, newCallArrow;
        TapList<CallArrow> arrows;
        TapList<Unit> diffUnits, destDiffUnits;
        UnitDiffInfo origDiffInfo, destDiffInfo;
        boolean diffOrigIsCopy, diffDestIsCopy;
        for (units = srcCallGraph.units(); units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            if (curUnit.rank() >= 0) {
                origDiffInfo = getUnitDiffInfo(curUnit);
                // First place IMPORT (i.e. USE) and CONTAINS arrows of the diff Call Graph:
                for (diffUnits = origDiffInfo.getAllDiffsPlusCopy();
                     diffUnits != null; diffUnits = diffUnits.tail) {
                    setCurDiffUnit(diffUnits.head);
                    diffOrigIsCopy = curDiffUnit == adEnv.getPrimalCopyOfOrigUnit(curUnit);
                    for (arrows = curUnit.callees(); arrows != null; arrows = arrows.tail) {
                        origCallArrow = arrows.head;
                        destUnit = origCallArrow.destination;
                        if ((origCallArrow.isImport() || origCallArrow.isContain()) && destUnit.rank() >= 0) {
                            destDiffInfo = getUnitDiffInfo(destUnit);
                            for (destDiffUnits = destDiffInfo.getAllDiffsPlusCopy();
                                 destDiffUnits != null; destDiffUnits = destDiffUnits.tail) {
                                diffDestUnit = destDiffUnits.head;
                                diffDestIsCopy = diffDestUnit == adEnv.getPrimalCopyOfOrigUnit(destUnit);
                                if (!diffOrigIsCopy || diffDestIsCopy || origCallArrow.isImport()) {
                                    newCallArrow = CallGraph.addCallArrow(curDiffUnit, -1, diffDestUnit);
                                    if (origCallArrow.isImport()) {
                                        newCallArrow.setImport(true);
                                    }
                                    if (origCallArrow.isContain()) {
                                        newCallArrow.setContain(true);
                                    }
                                }
                            }
                        }
                    }
                }
                // Now place CALL arrows from primal copy to either source or primal copy:
                // (the other CALL arrows are placed during BlockDifferentiation)
                setCurDiffUnit(adEnv.getPrimalCopyOfOrigUnit(curUnit));
                if (curDiffUnit != null) {
                    for (arrows = curUnit.callees(); arrows != null; arrows = arrows.tail) {
                        origCallArrow = arrows.head;
                        destUnit = origCallArrow.destination;
                        if (origCallArrow.isCall() && destUnit.rank() >= 0) {
                            diffDestUnit = adEnv.getPrimalCopyOfOrigUnit(destUnit);
                            // If the called routine has no primal copy, we place a CallArrow to the source called routine
                            // Danger [19Mar18]: this is the 1st example of a CallArrow between 2 different CallGraph's...
                            if (diffDestUnit == null) {
                                diffDestUnit = destUnit;
//                                 TapEnv.printlnOnTrace(20, "ADDING CALLARROW FROM PRIMAL COPY " + curDiffUnit + " TO ORIG " + diffDestUnit);
                            } else {
//                                 TapEnv.printlnOnTrace(20, "ADDING CALLARROW FROM PRIMAL COPY " + curDiffUnit + " TO PRIMAL COPY " + diffDestUnit);
                            }
                            CallGraph.addCallArrow(curDiffUnit, SymbolTableConstants.CALLS, diffDestUnit);
                        }
                    }
                }
            }
        }

	//[llh 10Dec2021] TODO: rewrite this step C to handle all the cyclic data in these SymbolTables.

        // STEP C: Fill the SymbolTables and connect them,
        // connect various links in the SymbolTables e.g., FunctionDecl->Unit, ...
        // Still no creation of the differentiated body inside.
        // Follows an order such that USE'd and CONTAIN'ing Units are done first,
        // so that rootmost SymbolTables are copied first.
        TapList<TapPair<SymbolTable, SymbolTable>> rootAssociationsST = new TapList<>(null, null);

        diffCallGraph.setGlobalRootSymbolTable(srcCallGraph.globalRootSymbolTable().smartCopy(rootAssociationsST,
                diffCallGraph, diffCallGraph, adEnv.copiedUnits, "Diff of "));
        diffCallGraph.topBlock.symbolTable = diffCallGraph.globalRootSymbolTable();
        diffCallGraph.globalRootSymbolTable().declarationsBlock = diffCallGraph.topBlock;

        diffCallGraph.setFortranRootSymbolTable(srcCallGraph.fortranRootSymbolTable().smartCopy(rootAssociationsST,
                diffCallGraph, diffCallGraph, adEnv.copiedUnits, "Diff of "));
        diffCallGraph.fortranRootSymbolTable().declarationsBlock =
                new BasicBlock(diffCallGraph.fortranRootSymbolTable(), null, null);
        diffCallGraph.fortranRootSymbolTable().declarationsBlock.copyInstructions(
                srcCallGraph.fortranRootSymbolTable().declarationsBlock, adEnv.copiedIncludes);

        diffCallGraph.setCRootSymbolTable(srcCallGraph.cRootSymbolTable().smartCopy(rootAssociationsST,
                diffCallGraph, diffCallGraph, adEnv.copiedUnits, "Diff of "));
        diffCallGraph.cRootSymbolTable().declarationsBlock =
                new BasicBlock(diffCallGraph.cRootSymbolTable(), null, null);
        diffCallGraph.cRootSymbolTable().declarationsBlock.copyInstructions(
                srcCallGraph.cRootSymbolTable().declarationsBlock, adEnv.copiedIncludes);

        diffCallGraph.setCudaRootSymbolTable(srcCallGraph.cudaRootSymbolTable().smartCopy(rootAssociationsST,
                diffCallGraph, diffCallGraph, adEnv.copiedUnits, "Diff of "));
        diffCallGraph.cudaRootSymbolTable().declarationsBlock =
                new BasicBlock(diffCallGraph.cudaRootSymbolTable(), null, null);
        diffCallGraph.cudaRootSymbolTable().declarationsBlock.copyInstructions(
                srcCallGraph.cudaRootSymbolTable().declarationsBlock, adEnv.copiedIncludes);

        diffCallGraph.setCPlusPlusRootSymbolTable(srcCallGraph.cPlusPlusRootSymbolTable().smartCopy(rootAssociationsST,
                diffCallGraph, diffCallGraph, adEnv.copiedUnits, "Diff of "));
        diffCallGraph.cPlusPlusRootSymbolTable().declarationsBlock =
                new BasicBlock(diffCallGraph.cPlusPlusRootSymbolTable(), null, null);
        diffCallGraph.cPlusPlusRootSymbolTable().declarationsBlock.copyInstructions(
                srcCallGraph.cPlusPlusRootSymbolTable().declarationsBlock, adEnv.copiedIncludes);

        for (TapList<Unit> topUnits = srcCallGraph.topUnits(); topUnits != null; topUnits = topUnits.tail) {
            Unit topUnit = topUnits.head;
            if (topUnit.isTranslationUnit()) {
                setCurUnitEtc(topUnit) ;
                SymbolTable tuST = topUnit.publicSymbolTable();
                SymbolTable diffTUST =
                    tuST.smartCopy(rootAssociationsST, getUnitDiffInfo(topUnit).getDiff(), diffCallGraph, adEnv.copiedUnits, "Diff of ");
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("   TrUnit:"+tuST) ;
                    TapEnv.printlnOnTrace("   ---->> diff TrUnit:"+diffTUST) ;
                }
                diffTUST.containerFileName = tuST.containerFileName + TapEnv.get().diffFileSuffix;
                diffTUST.isPublic = true ;
                diffTUST.setShortName("SymbolTable of Translation Unit (i.e. file) " + diffTUST.containerFileName);
                diffCallGraph.addTranslationUnitSymbolTable(diffTUST);
// A virer (peut-etre seulement en C?) pour que les forwards decls soient rajoutes correctement:
//                 diffTUST.declarationsBlock.copyInstructions(tuST.declarationsBlock, adEnv.copiedIncludes);
                Unit diffTopUnit = getUnitDiffInfo(topUnit).getDiff();
                diffTopUnit.setTranslationUnitSymbolTable(diffTUST);
                diffTopUnit.setPublicSymbolTable(diffTUST);
                diffTopUnit.setOtherName(topUnit);
                topUnit.setOtherName(diffTopUnit);
            }
        }

        // Detect the need for additional file(s) to define the Assoc-by-Address special type:
        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
            boolean needsCaaType = false ;
            boolean needsFaaType = false ;
            for (units = srcCallGraph.units(); units != null; units = units.tail) {
                setCurUnitEtc(units.head);
                if (curUnit.rank() >= 0
                    && (// adEnv.getPrimalCopyOfOrigUnit(curUnit) || //[llh] dubious borderline case
                        getUnitDiffInfo(curUnit).hasDiffs())) {
                    if (curUnit.isC())
                        needsCaaType = true ;
                    else if (curUnit.isFortran())
                        needsFaaType = true ;
                }
            }
            if (needsCaaType)
                createAssocAddressDiffTypesUnit(TapEnv.C);
            if (needsFaaType)
                createAssocAddressDiffTypesUnit(TapEnv.FORTRAN2003);
        }

	// First, do only Packages. Don't adapt importsSymbolTable's :
        for (units = orderedUnitsRootSTFirst; units != null; units = units.tail) {
            setCurUnitEtc(units.head);
	    if (curUnit.isPackage()) {
		setCurDiffUnit(getUnitDiffInfo(curUnit).getDiff());
		if (curDiffUnit!=null) {
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("  Creating SymbolTables for diff package "+curDiffUnit+" of "+curUnit) ;
                        TapEnv.printlnOnTrace("    given static SymbolTable associations:") ;
                        SymbolTable.traceShowAssociationsST(rootAssociationsST) ;
                    }
		    if (curUnit.importsSymbolTable() != null) {
			curDiffUnit.setImportsSymbolTable(
			    curUnit.importsSymbolTable().smartCopy(rootAssociationsST, diffCallGraph, diffCallGraph,
                                                                   adEnv.copiedUnits, "Diff of ")) ;
                        if (adEnv.traceCurDifferentiation) {
                            TapEnv.printlnOnTrace("   import:"+curUnit.importsSymbolTable()) ;
                            TapEnv.printlnOnTrace("   ---->> diff import:"+curDiffUnit.importsSymbolTable()) ;
                        }
		    }
		    if (curUnit.translationUnitSymbolTable() != null) {
			curDiffUnit.setTranslationUnitSymbolTable(
			     curUnit.translationUnitSymbolTable().getExistingCopy(rootAssociationsST));
		    }
		    createDiffUnitSymbolTables(rootAssociationsST);
		    curDiffUnit.orig2copiedBlocks = null;
		    curDiffUnit.origPublicSTDeclarationBlock = null;
		    curDiffUnit.copyPublicSTDeclarationBlock = null;
		}
	    }
	}

	// Second, do non-Packages, plus adaptation of importsSymbolTable's for all Units
        for (units = orderedUnitsRootSTFirst; units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("  Creating SymbolTables for all diff units of "+curUnit) ;
            }
	    if (curUnit.isPackage()) {
		setCurDiffUnit(getUnitDiffInfo(curUnit).getDiff());
		if (curDiffUnit.importsSymbolTable()!=null) {
		    curDiffUnit.importsSymbolTable().updateImportedFroms(rootAssociationsST) ;
		} else if (curUnit.isTranslationUnit() && curUnit.isC()) {
                    curDiffUnit.publicSymbolTable().updateImportedFroms(rootAssociationsST) ;
                }
	    } else if (curUnit.hasSource() || curUnit.isInterface()) { //i.e. exclude curUnit with no code?
		// All diff Units of a given primal Unit share a single diff importsSymbolTable:
		// (i.e. smartCopy is done with the global rootAssociationsST) :
                SymbolTable importsSymbolTable = curUnit.importsSymbolTable() ;
                SymbolTable diffImportsSymbolTable = null ;
                SymbolTable contextSymbolTable = (importsSymbolTable==null
                                                  ? curUnit.publicSymbolTable().basisSymbolTable()
                                                  : importsSymbolTable.basisSymbolTable()) ;
                SymbolTable diffContextSymbolTable = contextSymbolTable.getExistingCopy(rootAssociationsST) ;
                UnitDiffInfo diffInfo = getUnitDiffInfo(curUnit);
                if (diffInfo.getAllDiffsPlusCopy()!=null) { // Don't prepare diff imports ST if there is no diff code anyway.
                  // Recovery of issues due to poor creation order of deeply nested diff Units.
                  // [TODO] All this construction of the differentiated tree of Unit's and SymbolTable's
                  // should be written in a better, more recursive manner.
                  if (diffContextSymbolTable==null) {
                    Unit enclosingUnit = curUnit.upperLevelUnit() ;
                    TapList<Unit> diffEnclosingUnits = getUnitDiffInfo(enclosingUnit).getAllDiffs() ;
                    Unit diffEnclosingUnit = (diffEnclosingUnits!=null ? diffEnclosingUnits.head : adEnv.getPrimalCopyOfOrigUnit(enclosingUnit));
                    diffContextSymbolTable = diffEnclosingUnit.privateSymbolTable() ;
                  }
                  if (importsSymbolTable!=null) {
                    // beware that in C, procedure Units have no importsSymbolTable:
                    TapList<TapPair<SymbolTable, SymbolTable>> contextAssociationsST =
                        new TapList<>(null, new TapList<>(new TapPair<>(contextSymbolTable, diffContextSymbolTable), null)) ;
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("  Creating import SymbolTable for all diff units of "+curUnit) ;
                        TapEnv.printlnOnTrace("    using context SymbolTable association:") ;
                        SymbolTable.traceShowAssociationsST(contextAssociationsST) ;
                        TapEnv.printlnOnTrace("   import:"+importsSymbolTable) ;
                    }
                    diffImportsSymbolTable =
                        importsSymbolTable.smartCopy(contextAssociationsST, diffCallGraph, diffCallGraph,
                                                     adEnv.copiedUnits, "Diff of ");
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("   ---->> diff import:"+diffImportsSymbolTable) ;
                    }
                    diffImportsSymbolTable.updateImportedFroms(rootAssociationsST) ;
                  }

                  for (diffUnits = diffInfo.getAllDiffsPlusCopy(); diffUnits != null; diffUnits = diffUnits.tail) {
		    setCurDiffUnit(diffUnits.head);
		    if (curUnit.translationUnitSymbolTable() != null) {
			curDiffUnit.setTranslationUnitSymbolTable(
                            curUnit.translationUnitSymbolTable().getExistingCopy(rootAssociationsST));
		    }
                    curDiffUnit.setImportsSymbolTable(diffImportsSymbolTable) ;
                    SymbolTable unitContextSymbolTable = (importsSymbolTable!=null ? importsSymbolTable : contextSymbolTable) ;
                    SymbolTable diffUnitContextSymbolTable = (diffImportsSymbolTable!=null ? diffImportsSymbolTable : diffContextSymbolTable) ;
                    TapList<TapPair<SymbolTable, SymbolTable>> localRootAssociationsST =
                        new TapList<>(null, new TapList<>(new TapPair<>(unitContextSymbolTable, diffUnitContextSymbolTable), null)) ;
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("  Creating SymbolTables for diff unit "+curDiffUnit+" of "+curUnit) ;
                        TapEnv.printlnOnTrace("    keeping local SymbolTable associations:") ;
                        SymbolTable.traceShowAssociationsST(localRootAssociationsST) ;
                    }
		    createDiffUnitSymbolTables(localRootAssociationsST);
                    if (curDiffUnit.privateSymbolTable()!=null) {
                        curDiffUnit.privateSymbolTable().updateImportedFroms(rootAssociationsST) ;
                    }
		    curDiffUnit.orig2copiedBlocks = null;
		    curDiffUnit.origPublicSTDeclarationBlock = null;
		    curDiffUnit.copyPublicSTDeclarationBlock = null;
		    // if curUnit has 2 diffUnits
		    // each diffUnit must have its own copied symbolDecls 
		    // therefore we must clean the link to copySymbolDecl.
		    // This must not be done for modules because 2 differentiations
		    // of the same subroutine must share their USE'd diffModule
		    // DANGER !!! The present mechanism will not work if we maintain a link
		    // from a contained subroutine to the symbolDecls of its container
		    if (curUnit.isStandard() || curUnit.isInterface()) {
			TapList<SymbolTable> localSymbolTables = curUnit.symbolTablesBottomUp();
			while (localSymbolTables != null) {
			    localSymbolTables.head.cleanCopySymbolDecls();
			    localSymbolTables = localSymbolTables.tail;
			}
		    }
                  }
                }
	    }
	}
        diffCallGraph.fortranRootSymbolTable().updateImportedFroms(rootAssociationsST) ;
        diffCallGraph.cRootSymbolTable().updateImportedFroms(rootAssociationsST) ;
        diffCallGraph.globalRootSymbolTable().updateImportedFroms(rootAssociationsST) ;

        // Clean the SymbolDecl.copySymbolDecl fields, that were used to transpose the
        // links between original SymbolDecl's into links between copied SymbolDecl's.
        if (TapEnv.inputLanguage() == TapEnv.C) {
            srcCallGraph.cRootSymbolTable().cleanCopySymbolDecls();
        } else if (TapEnv.inputLanguage() != TapEnv.MIXED) {
            srcCallGraph.fortranRootSymbolTable().cleanCopySymbolDecls();
        } else {
            srcCallGraph.globalRootSymbolTable().cleanCopySymbolDecls();
            srcCallGraph.fortranRootSymbolTable().cleanCopySymbolDecls();
            srcCallGraph.cRootSymbolTable().cleanCopySymbolDecls();
        }

        if (srcCallGraph.bindFortranCFunctions() != null) {
            diffCallGraph.setBindCFortranCFunctions(srcCallGraph.bindFortranCFunctions());
        }

        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            TapList<SymbolTable> localSymbolTables = curUnit.symbolTablesBottomUp();
            while (localSymbolTables != null) {
                localSymbolTables.head.cleanCopySymbolDecls();
                localSymbolTables = localSymbolTables.tail;
            }
        }
        setCurUnitEtc(null);

        // Complain if there is nothing to differentiate
        if (!codeContainsSomethingActive) {
            String rootNames = null;
            inRootUnits = adEnv.rootUnits;
            while (inRootUnits != null) {
                if (rootNames == null) {
                    rootNames = inRootUnits.head.name();
                } else {
                    rootNames = rootNames + ' ' + inRootUnits.head.name();
                }
                inRootUnits = inRootUnits.tail;
            }
            TapEnv.fileWarning(TapEnv.MSG_SEVERE, TapEnv.NO_POSITION, "(AD06) Differentiation root procedures (" + rootNames + ") have no active input nor output");
        }


        if (TapEnv.modeIsOverloading()) {
            solveNewSymbolHoldersOfUnits(diffCallGraph.topUnits(), adEnv.emptyCopiedUnitSuffix);
            blockDifferentiator().differentiateDeclarationsOfBlock(diffCallGraph.topBlock, DiffConstants.TANGENT_MODE,
                    null, srcCallGraph.cRootSymbolTable(), diffCallGraph.cRootSymbolTable(), TapEnv.C, false);
            solveNewSymbolHoldersOfCallGraphTop(diffCallGraph, adEnv.emptyCopiedUnitSuffix);
        }

        // STEP D: Prepare interfaces before actual differentiation
        // In particular, build the FunctionTypeSpec of diff Units.
        // This must be done beforehand because of recursivity (cf set09/v104).
        // Also, varFunctions and interfaceUnits must have their FunctionTypeSpec prepared now
        //  before any of their callers is differentiated (cf set05/v60,v61)
        if (multiDirMode()) {
            initializeRootMultiDirNumberMax(diffCallGraph.globalRootSymbolTable());
        }
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            if (curUnit.isStandard() || curUnit.isVarFunction() || curUnit.isInterface()
                    || curUnit.isOutside()) {
                buildFunctionTypeSpecOfDiffUnitsOfCurUnit();
                if (TapEnv.associationByAddress() && curUnit.isStandard()) {
                    buildFunctionTypeSpecOfCopyUnit();
                }
            }
        }
        for (TapList<Unit> intrinsicUnits = TapList.append(srcCallGraph.dummyUnits, srcCallGraph.intrinsicUnits())
             ; intrinsicUnits != null; intrinsicUnits = intrinsicUnits.tail) {
            Unit intrinsicUnit = intrinsicUnits.head;
            if (!intrinsicUnit.hasPredefinedDerivatives()) {
                setCurUnitEtc(intrinsicUnit);
                buildFunctionTypeSpecOfDiffUnitsOfCurUnit();
            }
        }
        adEnv.setCurActivity(null);

        // STEP E: Really fill the differentiated Units:
        // Must be done from deep SymbolTable's up, because of NewSymbolHolder's
        // Also packages must be done last, because they may use the header of diff routines,
        // which therefore must be prepared beforehand.
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            if (!curUnit.isPackage()) {
                // For each sub-Unit stored in the curUnit's Non-flow instructions or as a
                // definition Instruction in some Block,
                //  insert the needed copied sub-Unit into the copied Unit's Non-flow instructions:
                Unit copiedUnit = adEnv.getPrimalCopyOfOrigUnit(curUnit);
                if (copiedUnit != null) {
                    TapList<Instruction> origNonFlow = curUnit.nonFlowInstructions;
                    while (origNonFlow != null) {
                        Unit definedSrcUnit = origNonFlow.head.tree.getAnnotation("Unit");
                        if (definedSrcUnit != null) {
                            UnitDiffInfo subUnitDiffInfo = getUnitDiffInfo(definedSrcUnit);
                            if (subUnitDiffInfo != null) {
                                Unit copySubUnit = subUnitDiffInfo.getCopyForDiff();
                                if (copySubUnit != null) {
                                    copiedUnit.nonFlowInstructions =
                                            TapList.addLast(copiedUnit.nonFlowInstructions,
                                                    Instruction.createUnitDefinitionStub(copySubUnit, null));
                                }
                            }
                        }
                        origNonFlow = origNonFlow.tail;
                    }
                    Block block;
                    Instruction instr;
                    for (TapList<Block> blocks = copiedUnit.allBlocks(); blocks != null; blocks = blocks.tail) {
                        block = blocks.head;
                        for (TapList<Instruction> instructions = block.instructions; instructions != null; instructions = instructions.tail) {
                            instr = instructions.head;
                            Unit definedSrcUnit = instr.isUnitDefinitionStub();
                            if (definedSrcUnit != null) {
                                UnitDiffInfo subUnitDiffInfo = getUnitDiffInfo(definedSrcUnit);
                                if (subUnitDiffInfo != null) {
                                    Unit copySubUnit = subUnitDiffInfo.getCopyForDiff();
                                    if (copySubUnit != null) {
                                        instr.updateUnitDefinitionStub(copySubUnit);
                                    }
                                }
                            }
                        }
                    }
                }

                // If debugging and no debug point is set in this Unit, place at least one at the "middle".
                if (curUnit.isStandard() && TapEnv.debugAdMode() != TapEnv.NO_DEBUG) {
                    placeDebugMidPointIfNone(curUnit);
                }

                activityPatterns = curUnit.activityPatterns;
                while (activityPatterns != null) {
                    adEnv.setCurActivity(activityPatterns.head);
                    adEnv.curUnitIsActiveUnit = patternCreatesDiffCode(adEnv.curActivity(), curUnit);
                    // If curUnitIsContext we must only build a context from which to call the diff:
                    adEnv.curUnitIsContext = adEnv.curActivity().isContext();
                    if (curUnit.isInterface() //[llh] This test is maybe too laxist, and should go away!
                            || unitsHaveDiff.retrieve(curUnit) == Boolean.TRUE) {
                        if (curUnit.isStandard()) {
                            if (patternCreatesDiffCode(adEnv.curActivity(), curUnit)) {
                                flowGraphDifferentiator().differentiateProcedure();
                            }
                        } else if (curUnit.isExternal()) {
                            // Differentiation was done earlier (see Step D:buildFunctionTypeSpecOfDiffUnitsOfCurUnit())
                        } else if (curUnit.isVarFunction()) {
                            differentiateVarFunctionUnit(adEnv.curActivity());
                        } else if (curUnit.isInterface()) {
                            if (codeContainsSomethingActive) {
                                differentiateInterface();
                            }
                        } else {
                            TapEnv.fileWarning(15, null, "Unit not differentiated: " + curUnit);
                        }
                    }
                    activityPatterns = activityPatterns.tail;
                }
                adEnv.setCurActivity(null);
                adEnv.curUnitIsActiveUnit = false;
                adEnv.curUnitIsContext = false;
            }
        }
        // Now we can do packages:
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(srcCallGraph.sortedUnit(i));
            if (curUnit.isPackage()) {
                differentiatePackage(curUnit, rootAssociationsST);
            }
        }
        setCurDiffUnit(null);
        setCurUnitEtc(null);

        if (TapEnv.get().oldContext) {
            differentiateAllocateInCopiedUnits();
        }

        if (!TapEnv.modeIsOverloading()) {
            // Solve pending/waiting NewSymbolHolders in SymbolTables of topUnits,
            //  from deepest (private) to rootmost (root of the diffCallGraph).
            solveNewSymbolHoldersOfUnits(diffCallGraph.topUnits(), adEnv.emptyCopiedUnitSuffix);
            int topBlockMode =
                    TapEnv.mustTangent() ? DiffConstants.TANGENT :
                            TapEnv.mustAdjoint() ? DiffConstants.ADJOINT : DiffConstants.ORIGCOPY;
            adEnv.pushCurDiffSorts(topBlockMode, topBlockMode);
            // TODO: si TapEnv.mustTangent() && TapEnv.mustAdjoint()?
            if (diffCallGraph.topBlock != null && TapList.length(diffCallGraph.topBlock.instructions) != 0) {
                if (adEnv.traceCurDifferentiation) {
                    //[llh] Ce message semble inexact?
                    TapEnv.printlnOnTrace("");
                    TapEnv.printlnOnTrace("*** NOW Differentiating declarations of " + srcCallGraph.cRootSymbolTable().shortName());
                }
                TapEnv.setRelatedLanguage(TapEnv.C);
                blockDifferentiator().differentiateDeclarationsOfBlock(
                        diffCallGraph.topBlock, topBlockMode, null, srcCallGraph.cRootSymbolTable(),
                        diffCallGraph.cRootSymbolTable(), TapEnv.C, false);
                TapEnv.setRelatedLanguage(null);
            }
            TapEnv.setRelatedLanguage(null);
            solveNewSymbolHoldersOfCallGraphTop(diffCallGraph, adEnv.emptyCopiedUnitSuffix);
            if (TapEnv.associationByAddress() && TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003) != null) {
                TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable().solveNewSymbolHolders(adEnv.emptyCopiedUnitSuffix
                );
            }
            adEnv.popCurDiffSorts();
        }
        for (units = srcCallGraph.units(); units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            for (int diffSort = 0; diffSort < TapEnv.MAX_DIFF_SORTS; ++diffSort) {
                diffUnits = getUnitDiffInfo(curUnit).getAllDiffs(diffSort);
                if (curUnit.isModule()) {
                    Unit diffModule = getUnitDiffInfo(curUnit).getDiff();
                    diffUnits = new TapList<>(diffModule, null);
                }
                while (diffUnits != null) {
                    setCurDiffUnit(diffUnits.head);
                    if (curDiffUnit.isInterface() && curDiffUnit.entryBlock() != null) {
                        Instruction hdInstr = curDiffUnit.entryBlock().headInstr();
                        if (hdInstr != null && hdInstr.tree != null) {
                            curDiffUnit.setName(ILUtils.getCallFunctionNameString(hdInstr.tree));
                        }
                    }
                    if (curUnit.otherName() == null || curUnit.otherName().equals(curUnit)) {
                        curUnit.setOtherName(curDiffUnit);
                    }
                    curDiffUnit.setOtherName(curUnit);
                    if (curDiffUnit.isStandard() || curDiffUnit.isInterface()) {
                        if (diffSort == DiffConstants.ORIGCOPY) { //attempt to solve USE M instead of USE M_DIFF
                            updateModulesAndCopiedUnitsInCopiedUnit(curDiffUnit.privateSymbolTable());
                            updateModulesAndCopiedUnitsInCopiedUnit(curDiffUnit.publicSymbolTable());
                        }
                        updateRenamedTypeDecl(curDiffUnit);
                    }
                    flowGraphDifferentiator().differentiateSaveList(curDiffUnit.privateSymbolTable());
                    //Solve pending/waiting NewSymbolHolders created by the above update*()
                    solveNewSymbolHoldersOfUnit(curDiffUnit, adEnv.emptyCopiedUnitSuffix);
                    diffUnits = diffUnits.tail;
                }
            }
        }
        setCurDiffUnit(null);
        setCurUnitEtc(null);

        // AGAIN solve pending/waiting NewSymbolHolders in SymbolTables of topUnits,
        //  from deepest (private) to rootmost (root of the diffCallGraph).
        solveNewSymbolHoldersOfUnits(diffCallGraph.topUnits(), adEnv.emptyCopiedUnitSuffix);
        solveNewSymbolHoldersOfCallGraphTop(diffCallGraph, adEnv.emptyCopiedUnitSuffix);

        setCurUnitEtc(null);

        // When outputting C, add the necessary #include directives
        //  e.g. for adBuffer, admm, debugAD, or contextAD:
        for (TapList<Unit> topUnits = diffCallGraph.topUnits(); topUnits != null; topUnits = topUnits.tail) {
            Unit topUnit = topUnits.head;
            if (topUnit.isTranslationUnit() && topUnit.isC() && topUnit.allBlocks() != null) {
                Block topBlock = topUnit.allBlocks().head;
                if (TapEnv.profile() && TapEnv.mustAdjoint()) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<adProfile.h>"), 0);
                }
                if (TapEnv.debugAdMode() != TapEnv.NO_DEBUG) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<adDebug.h>"), 0);
                } else if (TapEnv.mustContext()) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<adContext.h>"), 0);
                }
                TapList<String> newDiffCIncludes = topUnit.newDiffCIncludes ;
                while (newDiffCIncludes!=null) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<"+newDiffCIncludes.head+">"), 0);
                    newDiffCIncludes = newDiffCIncludes.tail ;
                }
                if (adEnv.usesADMM) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<admm.h>"), 0);
                }
                if (TapEnv.mustAdjoint()) {
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "<adStack.h>"), 0);
                }
                if (TapEnv.associationByAddress() && !"AATypes.h".equals(topUnit.name())) {
                    String aaTypesUnitName = "AATypes"
                            + TapEnv.get().diffFileSuffix
                            + ".c";
                    topBlock.addInstructionAt(ILUtils.build(ILLang.op_include, "\"" + aaTypesUnitName + "\""), 0);
                }
            }
        }

        // Each time differentiation decided to create inside the diff code
        // a primal copy (e.g. "foo_nodiff" or "foo_cd"...) of a source procedure "foo"
        // (instead of always referring to the source procedure "foo"),
        // sweep through the diff code to replace "foo" with "foo"+copiedUnitSuffix
        // First sweep everywhere except on interfaces:
        updateCopiedUnits(false);
        // Second sweep on interfaces:
        updateCopiedUnits(true);

        if (TapEnv.associationByAddress()) {
            updateAAInCopiedUnits();
        }

        // met a jour les interfaces des units qui ont ete copiees
        // et les interfaces contenant des units copiees 
        for (units = srcCallGraph.units(); units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            diffUnits = getUnitDiffInfo(curUnit).getAllDiffsPlusCopy();
            while (diffUnits != null) {
                setCurDiffUnit(diffUnits.head);
                if (curUnit.publicSymbolTable() != null && curDiffUnit.publicSymbolTable() != null) {
                    updateCopiedInterfaceAndExternalDecls(curUnit,
                            curUnit.publicSymbolTable().getAllTopFunctionDecls(), true);
                }
                if (curUnit.privateSymbolTable() != null && curDiffUnit.privateSymbolTable() != null) {
                    updateCopiedInterfaceAndExternalDecls(curUnit,
                            curUnit.privateSymbolTable().getAllTopFunctionDecls(), false);
                }
                diffUnits = diffUnits.tail;
            }
        }

        updateCopiedExternalDecls(srcCallGraph.cRootSymbolTable().getAllTopFunctionDecls(),
                srcCallGraph.cRootSymbolTable(), diffCallGraph.cRootSymbolTable());
        TapList<SymbolTable> srcTuSymbolTables = srcCallGraph.getTranslationUnitSymbolTables();
        while (srcTuSymbolTables != null) {
            updateCopiedExternalDecls(srcTuSymbolTables.head.getAllTopFunctionDecls(),
                    srcTuSymbolTables.head,
                    srcTuSymbolTables.head.getExistingCopy(rootAssociationsST));
            srcTuSymbolTables = srcTuSymbolTables.tail;
        }

        // Tries to remove declaration of original function's result "f"
        // in the case of units renamed as F_nodiff
        for (units = srcCallGraph.units(); units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            if (curUnit.functionTypeSpec() != null && curUnit.isAFunction() &&
                    // I don't know why, but in C, the otherReturnVar is filled in all cases with the function's name!
                    (curUnit.otherReturnVar() == null || curUnit.otherReturnVar().symbol.equals(curUnit.name()))) {
                String returnName = curUnit.name();
                for (int diffSort = 0; diffSort < TapEnv.MAX_DIFF_SORTS; ++diffSort) {
                    diffUnits = getUnitDiffInfo(curUnit).getAllDiffs(diffSort);
                    if (diffSort == 0) { //So far, only for copied units!!
                        while (diffUnits != null) {
                            Unit copiedUnit = diffUnits.head;
                            if (copiedUnit != null && !copiedUnit.name().equals(returnName)) {
                                SymbolTable pubST = copiedUnit.publicSymbolTable();
                                SymbolDecl oldReturnDecl = pubST.getTopVariableDecl(returnName);
                                if (oldReturnDecl != null) {
                                    pubST.removeDecl(returnName, SymbolTableConstants.VARIABLE, true);
                                }
                            }
                            diffUnits = diffUnits.tail;
                        }
                    }
                }
            }
        }

        setCurUnitEtc(null);
        TapEnv.resetRelatedUnit();

        buildDifferentiationAssociations();
        TapEnv.printlnOnTrace(10, "@@ Differentiation total time " + diffTimer.elapsed() + " s");

    }

    /**
     * Sweep through all procedure calls inside curUnit, and annotate the actual arguments that
     * are differentiated for the given callingActivity, raising the argument's SymbolDecl "isActive" flag.
     */
    private void annotateActiveCallArgumentsInCurUnit(ActivityPattern callingActivity) {
        TapList<Block> inAllBlocks = curUnit.allBlocks();
        UnitDiffInfo diffInfo;
        Block block;
        SymbolTable symbolTable;
        TapList<Instruction> instructions;
        TapList<BoolVector> blockReqXs, blockAvlXs;
        BoolVector beforeReqX, afterReqX, beforeAvlX, afterAvlX;
        Tree instr, lhsTree, callTree;
        TapList resultPointerActivity;
        TapList writtenZonesTree;
        Unit calledUnit;
        ActivityPattern calledActivity;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            symbolTable = block.symbolTable;
            instructions = inAllBlocks.head.instructions;
            blockReqXs = (callingActivity.reqXs() == null ? null : callingActivity.reqXs().retrieve(block));
            blockAvlXs = (callingActivity.avlXs() == null ? null : callingActivity.avlXs().retrieve(block));
            beforeReqX = (blockReqXs == null ? null : blockReqXs.head);
            beforeAvlX = (blockAvlXs == null ? null : blockAvlXs.head);
            while (instructions != null) {
                instr = instructions.head.tree;
                if (blockReqXs != null) {
                    blockReqXs = blockReqXs.tail;
                    afterReqX = blockReqXs.head;
                } else {
                    afterReqX = null;
                }
                if (blockAvlXs != null) {
                    blockAvlXs = blockAvlXs.tail;
                    afterAvlX = blockAvlXs.head;
                } else {
                    afterAvlX = null;
                }
                lhsTree = null;
                callTree = null;
                if (instr != null && instr.opCode() == ILLang.op_varDeclaration) {
                    TapPair<Tree, Tree> declAndInit = ILUtils.splitDeclInit(instr, false, curUnit);
                    instr = declAndInit.second;
                }
                if (instr != null && (instr.opCode() == ILLang.op_assign
                        || instr.opCode() == ILLang.op_plusAssign
                        || instr.opCode() == ILLang.op_minusAssign
                        || instr.opCode() == ILLang.op_timesAssign
                        || instr.opCode() == ILLang.op_divAssign)) {
                    lhsTree = instr.down(1);
                    instr = instr.down(2);
                }
                if (instr != null && instr.opCode() == ILLang.op_call) {
                    callTree = instr;
                }
                if (callTree != null) {
                    resultPointerActivity = null;
                    if (lhsTree != null) {
                        writtenZonesTree =
                                symbolTable.treeOfZonesOfValue(lhsTree, null, instructions.head, null);
                        // "PointerActiveUsage":
                        resultPointerActivity =
                                DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                        afterReqX, null, SymbolTableConstants.ALLKIND, symbolTable);
                    }
                    calledUnit = DataFlowAnalyzer.getCalledUnit(callTree, symbolTable);
                    calledActivity =
                            (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                    callTree, callingActivity, "multiActivityCalleePatterns");
                    if (diffCallNeeded(callTree, callingActivity, calledUnit, calledActivity,
                            beforeAvlX, afterReqX, resultPointerActivity, block, instructions.head)) {
                        annotateActiveCallArgumentsForCall(callTree, lhsTree, CallGraph.getCallArrow(curUnit, calledUnit),
                                ILUtils.getArguments(callTree).children(), calledActivity, instructions.head, callingActivity);
                    }
                }
                instructions = instructions.tail;
                beforeReqX = afterReqX;
                beforeAvlX = afterAvlX;
            }
            inAllBlocks = inAllBlocks.tail;
        }
    }

    /**
     * Utility for annotateActiveCallArgumentsInCurUnit.
     * Given a procedure call "lhs" := "expression", and knowing the ActivityPattern's of the caller and callee Units,
     * raise the "isActive" flag of all SymbolDecl's of the caller Unit
     * that are involved in a differentiated argument of this call.
     */
    private void annotateActiveCallArgumentsForCall(Tree expression, Tree lhs,
                                                    CallArrow arrow, Tree[] actualParams, ActivityPattern calledActivity,
                                                    Instruction instr, ActivityPattern callerActivity) {
        SymbolTable symbolTable = instr.block.symbolTable;
        Unit callingUnit = instr.block.unit();
        BoolVector calleeDiffParamsRequired =
                calledActivity == null ? null : ADActivityAnalyzer.functionDiffFormalRequired(calledActivity, false);
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("    Annotating caller SymbolDecl's as active because active or required in call: "
                    + ILUtils.toString(expression));
            TapEnv.printlnOnTrace("        with calledActivity: " + calledActivity);
            TapEnv.printlnOnTrace("        calleeDiffParamsRequired:" + calleeDiffParamsRequired);
        }
        if (calleeDiffParamsRequired != null) {
            BoolVector privateRequired = new BoolVector(symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
            TapList actualResultRequired = new TapList<>(null, null);
            // Remove from actualParams those actual arguments for which the passed derivative will be a dummyzerodiff:
            Tree[] actuallyDiffActualParams = new Tree[actualParams.length];
            for (int i = actualParams.length - 1; i >= 0; --i) {
                // cf the tests in ProcedureCallDifferentiator that trigger creation of dummyzerodiff:
                if ((ADActivityAnalyzer.isAnnotatedActive(callerActivity, actualParams[i], symbolTable)
                        || ILUtils.isAWritableIdentVarRef(actualParams[i], symbolTable))
                        //[llh 19/10/2015] This test varIsIntentIn() is dangerous: needed only for tangent mode ?
                        && !symbolTable.varIsIntentIn(actualParams[i])) {
                    actuallyDiffActualParams[i] = actualParams[i];
                } else {
                    actuallyDiffActualParams[i] = null;
                }
            }
            // [llh] TODO here maybe do something when the differentiated actual argument
            // or the actual result are not a reference and therefore some tmp var
            // must be created and declared with the correct type:
            DataFlowAnalyzer.translateCalleeDataToCallSite(calleeDiffParamsRequired, privateRequired,
                    lhs, actuallyDiffActualParams, true,
                    expression, instr, arrow, SymbolTableConstants.ALLKIND, null, null, true, false) ;
            // Set the isActive flag on the SymbolDecl's of the new privateRequired zones:
            TapList<SymbolDecl> allVariableDecls = symbolTable.getAllVariableDecls();
            VariableDecl varDecl;
            TapIntList varZones;
            while (allVariableDecls != null) {
                varDecl = (VariableDecl) allVariableDecls.head;
                varZones = ZoneInfo.listAllZones(varDecl.zones(), true);
                if (varZones != null) {
                    if (privateRequired.intersects(varZones)) {
                        if (adEnv.traceCurDifferentiation) {
                            TapEnv.printlnOnTrace("      => set active " + varDecl
                                    + " (zones ranks in caller-private active zones:" + varZones + ')');
                        }
                        varDecl.setActive();
                        ZoneInfo zoneInfo;
                        boolean modified;
                        int varRk;
                        while (varZones != null) {
                            zoneInfo = symbolTable.declaredZoneInfo(varZones.head, SymbolTableConstants.ALLKIND);
                            varRk = varZones.head ;
                            if (varRk > 0 && privateRequired.get(varRk)) {
                                modified = varDecl.cumulActiveField(zoneInfo.accessTree);
                                if (modified) {
                                    varDecl.type().cumulActiveParts(varDecl.activityInfo, symbolTable);
                                }
                            }
                            varZones = varZones.tail;
                        }
                    }
                }
                allVariableDecls = allVariableDecls.tail;
            }
        }
    }

    /**
     * Assuming curUnit has a differentiated Unit (for current callingActivity),
     * sweeps all call sites inside curUnit and finds their called ActivityPattern,
     * and removes this called ActivityPattern if it turns out to call the primal called procedure.
     * In addition, collects into "collected" and returns the called Units that are called as primal.
     */
    private TapList<Unit> collectAllCalledPrimal(ActivityPattern callingActivity, TapList<Unit> collected) {
        TapList<Block> inAllBlocks = curUnit.allBlocks();
        Block block;
        SymbolTable symbolTable;
        TapList<Instruction> instructions;
        TapList<BoolVector> blockReqXs, blockAvlXs;
        BoolVector beforeReqX, afterReqX, beforeAvlX, afterAvlX;
        Tree instr, lhsTree, callTree;
        TapList resultPointerActivity;
        TapList writtenZonesTree;
        Unit calledUnit;
        ActivityPattern calledActivity;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            symbolTable = block.symbolTable;
            instructions = inAllBlocks.head.instructions;
            blockReqXs = (callingActivity.reqXs() == null ? null : callingActivity.reqXs().retrieve(block));
            blockAvlXs = (callingActivity.avlXs() == null ? null : callingActivity.avlXs().retrieve(block));
            beforeReqX = (blockReqXs == null ? null : blockReqXs.head);
            beforeAvlX = (blockAvlXs == null ? null : blockAvlXs.head);
            while (instructions != null) {
                instr = instructions.head.tree;
                if (blockReqXs != null) {
                    blockReqXs = blockReqXs.tail;
                    afterReqX = blockReqXs.head;
                } else {
                    afterReqX = null;
                }
                if (blockAvlXs != null) {
                    blockAvlXs = blockAvlXs.tail;
                    afterAvlX = blockAvlXs.head;
                } else {
                    afterAvlX = null;
                }
                lhsTree = null;
                callTree = null;
                if (instr != null && instr.opCode() == ILLang.op_varDeclaration) {
                    TapPair<Tree, Tree> declAndInit = ILUtils.splitDeclInit(instr, false, curUnit);
                    instr = declAndInit.second;
                }
                if (instr != null && (instr.opCode() == ILLang.op_assign
                        || instr.opCode() == ILLang.op_plusAssign
                        || instr.opCode() == ILLang.op_minusAssign
                        || instr.opCode() == ILLang.op_timesAssign
                        || instr.opCode() == ILLang.op_divAssign)) {
                    lhsTree = instr.down(1);
                    instr = instr.down(2);
                }
                if (instr != null && instr.opCode() == ILLang.op_call) {
                    callTree = instr;
                }
                if (callTree != null) {
                    resultPointerActivity = null;
                    if (lhsTree != null) {
                        writtenZonesTree =
                                symbolTable.treeOfZonesOfValue(lhsTree, null, instructions.head, null);
                        // "PointerActiveUsage":
                        resultPointerActivity =
                                DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                        afterReqX, null, SymbolTableConstants.ALLKIND, symbolTable);
                    }
                    calledUnit = DataFlowAnalyzer.getCalledUnit(callTree, symbolTable);
                    if (calledUnit.isStandard()) {
                        calledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                callTree, callingActivity, "multiActivityCalleePatterns");
                        // if calledActivity will result in no diff code (i.e. calling only the primal calledUnit),
                        // then remove calledActivity from stored "multiActivityCalleePatterns"
                        if (calledActivity != null
                                && calledActivity.isActive()
                                && !diffCallNeeded(callTree, callingActivity, calledUnit, calledActivity,
                                beforeAvlX, afterReqX, resultPointerActivity, block, instructions.head)) {
                            // Here remove calledActivity FROM this call site's "multiActivityCalleePatterns" for pattern callingActivity
                            ActivityPattern.removeAnnotationForActivityPattern(callTree, callingActivity, "multiActivityCalleePatterns");
                            calledActivity = null;
                        }
                        if (calledActivity == null || !calledActivity.isActive()
                                // Even if calledUnit is differentiated, checkpointed adjoint diff of curUnit calls the primal calledUnit:
                                || (TapEnv.mustAdjoint() && calledUnit.mustDifferentiateJoint())) {
                            collected = TapList.addUnlessPresent(collected, calledUnit);
                        }
                    }
                }
                instructions = instructions.tail;
                beforeReqX = afterReqX;
                beforeAvlX = afterAvlX;
            }
            inAllBlocks = inAllBlocks.tail;
        }
        return collected;
    }

    // TODO: make sure this is consistent with "diffCallPresent" in DiffLivenessAnalyzer!
    public boolean diffCallNeeded(Tree callTree, ActivityPattern callingActivity, Unit calledUnit, ActivityPattern calledActivity,
                                  BoolVector beforeAvlX, BoolVector afterReqX, TapList resultPointerActivity, Block block, Instruction instr) {
        int shapeLength = 0 ;
        //Patch needed for variable-functions (cf set09/v103). Wrong, but does not crash...
        if (calledUnit.externalShape!=null) {
            shapeLength = calledUnit.publicZonesNumber(adEnv.diffKind) ;//was calledUnit.paramElemsNb();
        }
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printOnTrace("          in " + curUnit + " with ActivityPattern@" + (callingActivity == null ? "null" : Integer.toHexString(callingActivity.hashCode())) + ", is this call differentiated " + callTree + " of unit " + calledUnit + " for called ActivityPattern@" + (calledActivity == null ? "null" : Integer.toHexString(calledActivity.hashCode())) + " ?");
            if (calledActivity == null) {
                TapEnv.printlnOnTrace(": no!");
            } else {
                TapEnv.printlnOnTrace(
                        ": (" + (patternCreatesDiffCode(callingActivity, curUnit) && !calledActivity.isDontDiff())
                        + "&" + !(callingActivity.isContext() && calledUnit.isExternal())
                        + "&(" + (ADActivityAnalyzer.isAnnotatedActive(callingActivity, callTree, block.symbolTable)
                                  && ((calledActivity.callActivity() != null && !calledActivity.callActivity().isFalse(shapeLength))
                                      ||
                                      (calledActivity.exitActivity() != null && !calledActivity.exitActivity().isFalse(shapeLength))))
                        + "|" + (beforeAvlX != null && afterReqX != null
                                         && ReqExplicit.isAnnotatedPointerActive(callingActivity, ILUtils.getCalledName(callTree)))
                        + "|" + (!callingActivity.isContext() && MPIcallInfo.isNonBlockingMPI(callTree, calledUnit, block))
                        + "|" + (calledActivity.isContext() && TapList.contains(adEnv.callersOfRootUnits, calledUnit))
                        + "|" + (callingActivity.isContext() && TapList.contains(adEnv.rootUnits, calledUnit)) + "))");
            }
        }
        return (patternCreatesDiffCode(callingActivity, curUnit) && calledActivity != null && !calledActivity.isDontDiff()
                &&
                // When in the "context" part, do not differentiate calls to externals
                !(callingActivity.isContext() && calledUnit.isExternal())
                &&
                // Even if this call has an ActivityPattern, we must make sure this pattern is not still passive:
                ((ADActivityAnalyzer.isAnnotatedActive(callingActivity, callTree, block.symbolTable)
                  && ((calledActivity.callActivity() != null && !calledActivity.callActivity().isFalse(shapeLength))
                      ||
                      (calledActivity.exitActivity() != null && !calledActivity.exitActivity().isFalse(shapeLength))))
                 ||
                 (beforeAvlX != null && afterReqX != null
                  //[llh 24/6/2016] the following re-injects beforeAvlX and afterReqX to retrieve the ReqX info
                  // on this call site. Instead, we might just as well use the ReqX info stored in the calledActivity.
                  // TODO: test that!
                  && ReqExplicit.isAnnotatedPointerActive(callingActivity, ILUtils.getCalledName(callTree)))
                 ||
                 !callingActivity.isContext() && MPIcallInfo.isNonBlockingMPI(callTree, calledUnit, block)
                 ||
                 calledActivity.isContext() && TapList.contains(adEnv.callersOfRootUnits, calledUnit)
                 ||
                 callingActivity.isContext() && TapList.contains(adEnv.rootUnits, calledUnit)));
    }

    /**
     * @return the precomputed formalArgsActivity info of the Unit of the given "activityPattern",
     * for this particular "activityPattern" given.
     */
    public boolean[] getUnitFormalArgsActivityS(ActivityPattern activityPattern) {
        UnitDiffInfo diffInfo = getUnitDiffInfo(activityPattern.unit());

        boolean[] formalArgsActivity = null;
        if (diffInfo != null) {
            formalArgsActivity = diffInfo.getUnitFormalArgsActivityS(activityPattern);
        }
        if (formalArgsActivity == null) { //case of intrinsics:
            formalArgsActivity = ADActivityAnalyzer.formalArgsActivity(activityPattern);
        }
        return formalArgsActivity;
    }

    private void createAssocAddressDiffTypesUnit(int aaUnitLang) {
        Unit aaTUUnit = diffCallGraph.createNewUnit(null,
                aaUnitLang);
        String suffixLang = ".f03";
        if (aaUnitLang == TapEnv.C) {
            suffixLang = ".h";
        }
        aaTUUnit.setTranslationUnit("AATypes" + suffixLang);
        SymbolTable tuST = new SymbolTable(diffCallGraph.languageRootSymbolTable(aaUnitLang));
        aaTUUnit.setTranslationUnitSymbolTable(tuST);
        aaTUUnit.setPublicSymbolTable(tuST);
        tuST.containerFileName = "AATypes";
        tuST.unit = aaTUUnit;
        tuST.setShortName("AssocByAddress public tuST");
        Block declBlock = new BasicBlock(tuST, null, null);
        tuST.declarationsBlock = declBlock;
        TapList<Block> allBlocks;
        aaTUUnit.setEntryBlock(new EntryBlock(tuST));
        aaTUUnit.setExitBlock(new ExitBlock(tuST));
        diffCallGraph.addTranslationUnitSymbolTable(tuST);
        new FGArrow(aaTUUnit.entryBlock(), FGConstants.ENTRY, FGConstants.MAIN, declBlock);
        new FGArrow(declBlock, FGConstants.NO_TEST, 0, aaTUUnit.exitBlock());
        allBlocks = new TapList<>(declBlock, null);
        if (aaUnitLang == TapEnv.C || aaUnitLang == TapEnv.CPLUSPLUS) {
            declBlock.instructions = new TapList<>(
                    new Instruction(ILUtils.build(ILLang.op_none)),
                    null);
            aaTUUnit.makeFlowGraph(aaTUUnit.entryBlock(), allBlocks, aaTUUnit.exitBlock(), false);
            TapEnv.setAssocAddressDiffTypesUnit(aaTUUnit);
        } else {
            Unit aaDiffTypeUnit = diffCallGraph.createNewUnit(aaTUUnit, aaUnitLang);
            declBlock.instructions = new TapList<>(
                    Instruction.createUnitDefinitionStub(aaDiffTypeUnit,
                            declBlock), null);
            aaTUUnit.makeFlowGraph(aaTUUnit.entryBlock(), allBlocks, aaTUUnit.exitBlock(), false);
            TapEnv.setAssocAddressDiffTypesUnit(aaDiffTypeUnit);
            aaDiffTypeUnit.setName("AATypes");
            aaDiffTypeUnit.setTranslationUnitSymbolTable(tuST);
            SymbolTable st = new SymbolTable(tuST);
            st.setShortName("AssocByAddress public ST");
            st.unit = aaDiffTypeUnit;
            aaDiffTypeUnit.setPublicSymbolTable(st);
            st = new SymbolTable(st);
            st.setShortName("AssocByAddress private ST");
            st.unit = aaDiffTypeUnit;
            aaDiffTypeUnit.setPrivateSymbolTable(st);
            aaDiffTypeUnit.setModule();
            Block declBlockModule =
                    new BasicBlock(aaDiffTypeUnit.publicSymbolTable(), null, null);
            declBlockModule.instructions = new TapList<>(new Instruction(
                    ILUtils.build(ILLang.op_useDecl, ILUtils.build(ILLang.op_ident, "ISO_C_BINDING"),
                            ILUtils.build(ILLang.op_renamedVisibles)))
                    , null);
            aaDiffTypeUnit.setEntryBlock(new EntryBlock(
                    aaDiffTypeUnit.publicSymbolTable()));
            aaDiffTypeUnit.setExitBlock(new ExitBlock(
                    aaDiffTypeUnit.publicSymbolTable()));
            aaDiffTypeUnit.publicSymbolTable().declarationsBlock = declBlockModule;
            new FGArrow(aaDiffTypeUnit.entryBlock(), FGConstants.ENTRY, FGConstants.MAIN, declBlockModule);
            new FGArrow(declBlockModule, FGConstants.NO_TEST, 0, aaDiffTypeUnit.exitBlock());
            allBlocks = new TapList<>(declBlockModule, null);
            aaDiffTypeUnit.makeFlowGraph(
                    aaDiffTypeUnit.entryBlock(), allBlocks,
                    aaDiffTypeUnit.exitBlock(), false);
            aaTUUnit.lowerLevelUnits =
                    new TapList<>(aaDiffTypeUnit, null);
            CallGraph.addCallArrow(aaTUUnit, SymbolTableConstants.CONTAINS, aaDiffTypeUnit);
        }
    }

    /**
     * Fills "diffCallGraph.backAssociations" with an A-list associating each Unit
     * of srcCallGraph to the list of all its corresponding Units in diffCallGraph.
     * The lists of Units of diffCallGraph are ordered as (and when each component exist):
     * {diffUnit (i.e. when package), tangents, adjoints, fwd's, bwd's, primalCopy}
     */
    private void buildDifferentiationAssociations() {
        Unit unit, copyUnit;
        UnitDiffInfo diffInfo;
        TapList<Unit> orderedDiffUnits;
        diffCallGraph.backAssociations = null;
        for (TapList<Unit> units = srcCallGraph.units(); units != null; units = units.tail) {
            unit = units.head;
            if (unit.rank() >= 0) {
                diffInfo = getUnitDiffInfo(unit);
                orderedDiffUnits = diffInfo.getAllDiffs();
                copyUnit = adEnv.getPrimalCopyOfOrigUnit(unit);
                if (copyUnit != null && !TapList.contains(orderedDiffUnits, copyUnit)) {
                    orderedDiffUnits = TapList.addLast(orderedDiffUnits, copyUnit);
                }
                diffCallGraph.backAssociations =
                        new TapList<>(new TapPair<>(unit, orderedDiffUnits),
                                diffCallGraph.backAssociations);
            }
        }
    }

    protected UnitDiffInfo getUnitDiffInfo(Unit unit) {
        if (unit.isIntrinsic() || unit.isDummy()) {
            return TapList.cassq(unit, intrinsicUnitDiffInfos);
        } else {
            return unitDiffInfos.retrieve(unit);
        }
    }

    /**
     * Build the FunctionTypeSpec's for each of the diff Units of curUnit.
     * Also prepare the differentiated Unit's of external and intrinsic units
     */
    private void buildFunctionTypeSpecOfDiffUnitsOfCurUnit() {

        // Add or remove declarations of the diff external Units:
        if (curUnit.isExternal() || curUnit.isIntrinsic() || curUnit.isDummy()) {
            String unitName = curUnit.name();
            Tree functionNameTree = ILUtils.build(ILLang.op_ident, unitName);
            TapList<FunctionDecl> diffFuncDecls =
                    diffCallGraph.languageRootSymbolTable(curUnit.language())
                            .getFunctionDecl(unitName, null, null, false);
            FunctionDecl diffFuncDecl = (diffFuncDecls == null ? null : diffFuncDecls.head);
            if (diffFuncDecl == null) {
                diffFuncDecl = new FunctionDecl(functionNameTree, curUnit);
                diffCallGraph.languageRootSymbolTable(curUnit.language()).addSymbolDecl(diffFuncDecl);
            }
            TapList<FunctionDecl> origFuncDecls =
                    srcCallGraph.languageRootSymbolTable(curUnit.language())
                            .getFunctionDecl(unitName, null, null, false);
            FunctionDecl origFuncDecl = (origFuncDecls == null ? null : origFuncDecls.head);
            if (origFuncDecl != null) {
                diffFuncDecl.setInstruction(origFuncDecl);
            } else {
                diffFuncDecl.setExternalInstr(true);
            }
        }

        TapList<ActivityPattern> activityPatterns;
        for (int mode = TapEnv.MAX_DIFF_SORTS-1; mode > 0; --mode) {
            activityPatterns = curUnit.activityPatterns;
            while (activityPatterns != null) {
                adEnv.setCurActivity(activityPatterns.head);
                //  Don't do the following if this curActivity is a dummy pattern created for an interface which is in fact not active
                setCurDiffUnit(adEnv.getDiffOfUnit(curUnit, adEnv.curActivity(), mode));
                if (curDiffUnit != null) {
                    // Build the FunctionTypeSpec of the curDiffUnit
                    if (multiDirMode()) {
                        flowGraphDifferentiator().initializeMultiDirNumberMax();
                    }
                    FunctionTypeSpec diffTypeSpec = curUnit.functionTypeSpec();
                    boolean isMainProcedure = curUnit == srcCallGraph.getMainUnit();
                    if ((mode == DiffConstants.TANGENT
                            || mode == DiffConstants.ADJOINT
                            || mode == DiffConstants.ADJOINT_FWD
                            || mode == DiffConstants.ADJOINT_BWD)
                            && !isMainProcedure) {
                        diffTypeSpec = buildFunctionTypeSpecOfCurDiffUnit(adEnv.curActivity(), mode);
                    }
                    if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                         TapEnv.printlnOnTrace("Built FunctionTypeSpec of diff Unit:"+curDiffUnit+" : "+diffTypeSpec) ;
                    }
                    curDiffUnit.setFunctionTypeSpec(diffTypeSpec);
                    // For the differentiated main(), remove declaration of main in the public ST:
                    if (isMainProcedure && curDiffUnit.isC()) {
                        curDiffUnit.publicSymbolTable().removeDecl("main", SymbolTableConstants.SYMBOL, true);
                    }
                    if (curUnit.isExternal() || curUnit.isIntrinsic() || curUnit.isDummy()) {
                        curDiffUnit.setExternal();
                        // Prepare a dummy list of formal arguments
                        FunctionTypeSpec diffFunctionTypeSpec = curDiffUnit.functionTypeSpec();
                        int nbDiffArgs = (diffFunctionTypeSpec.argumentsTypes == null
                                ? 0
                                : diffFunctionTypeSpec.argumentsTypes.length);
                        TapList<Tree> dummyArgsList = null;
                        for (int i = nbDiffArgs; i > 0; --i) {
                            dummyArgsList = new TapList<>(ILUtils.build(ILLang.op_ident, "_"), dummyArgsList);
                        }
                        curDiffUnit.parametersOrModuleNameTree = ILUtils.build(ILLang.op_expressions, dummyArgsList);
                    }
                }
                activityPatterns = activityPatterns.tail;
            }
        }
        adEnv.setCurActivity(null);
    }

    private void buildFunctionTypeSpecOfCopyUnit() {
        Unit copiedUnit = adEnv.copiedUnits.retrieve(curUnit);
        if (copiedUnit != null) {
            FunctionTypeSpec diffTypeSpec = copiedUnit.functionTypeSpec();
            WrapperTypeSpec[] paramsTypes = diffTypeSpec.argumentsTypes;
            TapList<ActivityPattern> activityPatterns = curUnit.activityPatterns;
            WrapperTypeSpec returnType = diffTypeSpec.returnType;
            WrapperTypeSpec paramType;
            WrapperTypeSpec diffParamType;
            WrapperTypeSpec diffReturnType = returnType;
            TapList<WrapperTypeSpec> diffArgTypes = null;
            // [llh] TODO: I don't understand this code any more:
            //  why should the FunctionTypeSpec of the *copied* Unit depend of the activity patterns
            //  of any of the differentiates of Unit? Moreover, this seems to overwrite it
            //  by the last of the curUnit.activityPatterns list !
            while (activityPatterns != null) {
                adEnv.setCurActivity(activityPatterns.head);
                boolean[] formalArgsActivity = getUnitFormalArgsActivityS(adEnv.curActivity());
                boolean[] formalArgsPointerActivity =
                        ReqExplicit.formalArgsPointerActivity(adEnv.curActivity(), paramsTypes.length);
                if (formalArgsActivity != null) {
                    if (curUnit.isAFunction()) {
                        if (formalArgsActivity[0] || formalArgsPointerActivity[0]) {
                            diffReturnType = returnType.differentiateTypeSpecMemo(copiedUnit.publicSymbolTable(), curUnit.publicSymbolTable(), curDiffUnitSort(),
                                suffixes()[DiffConstants.DIFFERENTIATED][DiffConstants.TYPE], false, multiDirMode(),
                                varRefDifferentiator().getMultiDirDimensionMax(),
                                curUnit.name(), curUnit.name(), null, null);
                        } else {
                            diffReturnType = returnType ;
                        }
                    }
                    for (int i = paramsTypes.length - 1; i >= 0; i--) {
                        paramType = paramsTypes[i];
                        if (formalArgsActivity[i] || formalArgsPointerActivity[i]) {
                            diffParamType =
                                    paramType.differentiateTypeSpecMemo(copiedUnit.publicSymbolTable(), curUnit.publicSymbolTable(), curDiffUnitSort(),
                                            suffixes()[DiffConstants.DIFFERENTIATED][DiffConstants.TYPE], false, multiDirMode(),
                                            varRefDifferentiator().getMultiDirDimensionMax(),
                                            curUnit.name() + "'s arg" + (i + 1),
                                            curUnit.name() + "_arg" + (i + 1), null, null);
                            if (diffParamType == null) {
                                diffParamType = paramType;
                            } else if (diffParamType.equalsLiterally(paramType)) {
                                diffParamType = paramType;
                            }
                        } else {
                            diffParamType = paramType;
                        }
                        diffArgTypes = new TapList<>(diffParamType, diffArgTypes);
                    }
                    diffTypeSpec = new FunctionTypeSpec(diffReturnType, diffArgTypes);
                    if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                         TapEnv.printlnOnTrace("Built FunctionTypeSpec of copy Unit:"+copiedUnit+" : "+diffTypeSpec) ;
                    }
                    copiedUnit.setFunctionTypeSpec(diffTypeSpec);
                }
                activityPatterns = activityPatterns.tail;
            }
        }
    }

    /**
     * For external's and intrinsic's, we fear that the user might make mistakes
     * when writing the derivative function for variables which are inactive, i.e.
     * their derivative is implicitely 0.0, but only implicitly: the input value
     * of the derivative must be reset to 0.0 in the diff function. If the user
     * forgets this reset, the whole code is false.    Example:
     * x=5.0 ; call F(x,y)      with     F(x,y): x=x+y end.
     * The diff code could be
     * x=5.0 ; call FD(x,xd,y,yd)
     * and the user should define
     * FD(x,xd,y,yd): xd=yd ; x=x+y end.
     * But the user will probably write xd=xd+yd !
     * =&gt; therefore, we ST insert xd=0.0 before the call FD.
     * To force that, we set x to active upon call, even if this is not really the case.
     */
    private void augmentExternalEntryExitActivities(BoolVector unitCallActive, BoolVector
            unitExitActive) {
        if (TapEnv.modeIsAdjoint()) {
            unitExitActive.cumulOr(unitCallActive);
        } else {
            // TapEnv.modeIsTangent()
            unitCallActive.cumulOr(unitExitActive);
            TapIntList resultRks = curUnit.focusToKind(curUnit.zonesOfResult(), adEnv.diffKind) ;
            while (resultRks != null) {
                unitCallActive.set(resultRks.head, false);
                resultRks = resultRks.tail;
            }
        }
    }

    private void futureUnitsDecision
            (TapList<Unit> unitsRootSTLast, TapList<Unit> calledPrimalFormUnits) {
        boolean traceFutureUnitsDecisions = (TapEnv.get().traceDifferentiationUnitNames != null);
        if (traceFutureUnitsDecisions) {
            TapEnv.printlnOnTrace(" Future Units: Units called in Primal form:" + calledPrimalFormUnits);
        }
        Unit unit, importedUnit = null;
        TapList<Unit> inUnits;
        TapList<CallArrow> importingArrows;
        boolean stabilized, refersToActiveOrImporting, refersToOrCallsPrimalOrImporting;
        unitsHavePrimal = new UnitStorage<>(srcCallGraph);
        unitsHaveDiff = new UnitStorage<>(srcCallGraph);
        UnitStorage<Boolean> unitsImportActive = new UnitStorage<>(srcCallGraph);
        UnitStorage<Boolean> unitsImportPrimal = new UnitStorage<>(srcCallGraph);
        for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
            unit = inUnits.head;
            unitsHavePrimal.store(unit, Boolean.FALSE);
            unitsHaveDiff.store(unit, Boolean.FALSE);
            unitsImportActive.store(unit, Boolean.FALSE);
            unitsImportPrimal.store(unit, Boolean.FALSE);
        }
        stabilized = false;
        while (!stabilized) {
          stabilized = true;
          for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
            unit = inUnits.head;
            boolean isActive = false;
            if (unitsHaveDiff.retrieve(unit) == Boolean.FALSE) {
              // test if unit is "Active" :
              if (unit.isModule() || unit.isClass()) {
                SymbolDecl activeSymbol;
                activeSymbol = symbolTableContainsActiveSymbolDecl(unit.publicSymbolTable());
                if (activeSymbol == null && unit.protectedSymbolTable() != null) {
                    activeSymbol = symbolTableContainsActiveSymbolDecl(unit.protectedSymbolTable());
                }
                if (activeSymbol == null) {
                    activeSymbol = symbolTableContainsActiveSymbolDecl(unit.privateSymbolTable());
                }
                isActive = activeSymbol != null;
                if (traceFutureUnitsDecisions && isActive) {
                    TapEnv.printlnOnTrace(" Future Units: package " + unit + " must have Diff because it contains active object " + activeSymbol);
                }
              } else if (unit.isTranslationUnit()) {
                SymbolDecl activeSymbol = symbolTableContainsActiveSymbolDecl(unit.publicSymbolTable());
                isActive = activeSymbol != null;
                if (traceFutureUnitsDecisions && isActive) {
                    TapEnv.printlnOnTrace(" Future Units: file " + unit + " must have Diff because it contains an active object " + activeSymbol);
                }
              } else {
                TapList<ActivityPattern> activityPatterns = unit.activityPatterns;
                while (!isActive && activityPatterns != null) {
                    isActive = ((activityPatterns.head.isContext()
                                 || activityAnalyzer().isActiveUnit(activityPatterns.head)
                                 || (TapEnv.reqExplicitAnalyzer() != null
                                     && TapEnv.reqExplicitAnalyzer().isPointerActiveUnit(activityPatterns.head)))
                                && patternCreatesDiffCode(activityPatterns.head, unit)) ;
                    if (traceFutureUnitsDecisions && isActive) {
                        TapEnv.printlnOnTrace(" Future Units: Unit " + unit + " must have Diff because it is context or active or pointerActive");
                    }
                    activityPatterns = activityPatterns.tail;
                }
              }
              // now extend "Active" to all units that "Contain Active":
              if (isActive) {
                unitsHaveDiff.store(unit, Boolean.TRUE);
                stabilized = false ;
                while ((unit = unit.upperLevelUnit()) != null) {
                    unitsHaveDiff.store(unit, Boolean.TRUE);
                    if (traceFutureUnitsDecisions) {
                        TapEnv.printlnOnTrace(" Future Units: " + unit + " must have Diff because it contains the previous one");
                    }
                }
              }
            }
          }
        }

        // now run a fixed point to detect the (Class or Package) units that "Import Active":
        stabilized = false;
        while (!stabilized) {
            stabilized = true;
            for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
                unit = inUnits.head;
                if (unitsImportActive.retrieve(unit) == Boolean.FALSE) {
                    importingArrows = unit.callees(); //TODO:inherited classes
                    refersToActiveOrImporting = false;
                    while (importingArrows != null && !refersToActiveOrImporting) {
                        if (importingArrows.head.isImport()) {
                            importedUnit = importingArrows.head.destination;
                            refersToActiveOrImporting =
                                    unitsHaveDiff.retrieve(importedUnit) == Boolean.TRUE
                                            || unitsImportActive.retrieve(importedUnit) == Boolean.TRUE;
                            if (traceFutureUnitsDecisions && refersToActiveOrImporting) {
                                TapEnv.printlnOnTrace(" Future Units: Unit " + unit + " imports active because imports " + importedUnit);
                            }
                        }
                        importingArrows = importingArrows.tail;
                    }
                    if (refersToActiveOrImporting) {
                        unitsImportActive.store(unit, Boolean.TRUE);
                        while ((unit = unit.upperLevelUnit()) != null) {
                            unitsImportActive.store(unit, Boolean.TRUE);
                            if (traceFutureUnitsDecisions) {
                                TapEnv.printlnOnTrace(" Future Units: Unit " + unit + " imports active because it contains the previous one");
                            }
                        }
                        stabilized = false;
                    }
                }
            }
        }

        // now find package Units that must have a copy "Primal code", and consequently their inside Units too:
        for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
            unit = inUnits.head;
            if (unit.isPackage()) {
                if (unitsImportActive.retrieve(unit) == Boolean.TRUE) {
                    unitsHaveDiff.store(unit, Boolean.TRUE);
                    if (traceFutureUnitsDecisions) {
                        TapEnv.printlnOnTrace(" Future Units: package " + unit + " must have Diff because it imports active");
                    }
                }
                if (!unit.isTranslationUnit()
                        && (!TapEnv.get().stripPrimalModules || unitsHaveDiff.retrieve(unit) == Boolean.TRUE)
                        && unitsHavePrimal.retrieve(unit) != Boolean.TRUE) {
                    unitsHavePrimal.store(unit, Boolean.TRUE);
                    if (traceFutureUnitsDecisions) {
                        TapEnv.printlnOnTrace(" Future Units: package " + unit + " must have Primal because "
                                + (TapEnv.get().stripPrimalModules ? "it must have Diff" : "-nooptim stripprimalmodules"));
                    }
                    setAllContainedHavePrimalRec(unit, traceFutureUnitsDecisions, unit);
                }
            }
        }

        // now find procedures that must have a copy "Primal code" for other reasons:
        for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
            unit = inUnits.head;
            if (!unit.isPackage() && unitsHavePrimal.retrieve(unit) != Boolean.TRUE) {
                Unit topContainer = unit.upperLevelUnit();
                while (topContainer != null && !topContainer.isTopInFile()) {
                    topContainer = topContainer.upperLevelUnit();
                }
                if (traceFutureUnitsDecisions) {
                    if (TapEnv.get().stripPrimalCode && TapEnv.get().stripPrimalEvenIfImportsActive
                            && unitsImportActive.retrieve(unit) == Boolean.TRUE) {
                        TapEnv.printlnOnTrace(" Future Units: NOTE: non-package " + unit + " should have primal because imports active, but option disables this");
                    }
                }
                boolean reason1 = !TapEnv.get().stripPrimalCode;
                boolean reason2 = !reason1
                        && unitsImportActive.retrieve(unit) == Boolean.TRUE && !TapEnv.get().stripPrimalEvenIfImportsActive;
                boolean reason3 = !reason2
                        && TapList.contains(calledPrimalFormUnits, unit)
                        // je restreins au cas C !?! (il ne faut peut-etre pas restreindre, et donc virer le test suivant:)
                        // [VMP] cf mix57 unit bar dans module m
                        && (unit.isC()
                        || (unit.isFortran2003() && unit.getNameFromBindC() != null && topContainer != null));
                // && topContainer est copied ou differentie', pb c'est fait apres pour mix57 module m
                boolean reason4 = !reason3 &&
                        topContainer != null && unitsHaveDiff.retrieve(topContainer) == Boolean.TRUE;
                // [llh 20Feb2018] Commented out (temporarily?) because triggers too often on C. To be improved.
                // When translationUnits are container Packages,this "reason5" should be subsumed by "reason4".
                // boolean reason5 = !reason4 &&
                //     unit.translationUnitSymbolTable()!=null && !unit.translationUnitSymbolTable().isEmptyTranslationUnitST();
                if (reason1 || reason2 || reason3 || reason4) {
                    if (traceFutureUnitsDecisions) {
                        TapEnv.printlnOnTrace(" Future Units: non-package " + unit + " must have Primal because " +
                                (reason1 ? "-nooptim stripprimalcode" :
                                        (reason2 ? "imports active" :
                                                (reason3 ? "differentiated code calls primal and is C or bindC" :
                                                        "is contained in active"))));
                    }
                    unitsHavePrimal.store(unit, Boolean.TRUE);

                    String boundUnitName = unit.getNameFromBindC();
                    if (boundUnitName != null) {
                        TapList<FunctionDecl> cFunctionDecls =
                                srcCallGraph.cRootSymbolTable().getFunctionDecl(boundUnitName, null, null, false);
                        if (cFunctionDecls != null) {
                            Unit boundUnit = cFunctionDecls.head.unit();
                            if (traceFutureUnitsDecisions) {
                                TapEnv.printlnOnTrace(" Future Units: " + boundUnit + " must have Primal because it is bound to the previous one");
                            }
                            unitsHavePrimal.store(boundUnit, Boolean.TRUE);
                        }
                    }

                    // If unit has primal (i.e. causes a _nodiff) then enclosing packages (e.g. files) must have a diff
                    while ((unit = unit.upperLevelUnit()) != null) {
                        if (unit.isTranslationUnit()) {
                            unitsHaveDiff.store(unit, Boolean.TRUE);
                            if (traceFutureUnitsDecisions) {
                                TapEnv.printlnOnTrace(" Future Units: package " + unit + " must have Diff because it contains the previous one");
                            }
                        }
                    }

                }
            }
        }

        // now run a fixed point to detect the units that "Import Primal":
        stabilized = false;
        while (!stabilized) {
            stabilized = true;
            for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
                unit = inUnits.head;
                if (unitsImportPrimal.retrieve(unit) == Boolean.FALSE) {
                    importingArrows = unit.callees(); //TODO:inherited classes
                    refersToOrCallsPrimalOrImporting = false;
                    // Special non-recommended option to force stripping even when importing active module:
                    if (TapEnv.get().stripPrimalEvenIfImportsActive) {
                        importingArrows = null;
                    }
                    while (importingArrows != null && !refersToOrCallsPrimalOrImporting) {
                        if (importingArrows.head.isImport()) {
                            importedUnit = importingArrows.head.destination;
                            refersToOrCallsPrimalOrImporting =
                                    unitsHavePrimal.retrieve(importedUnit) == Boolean.TRUE
                                            || unitsImportPrimal.retrieve(importedUnit) == Boolean.TRUE;
                        }
                        if (traceFutureUnitsDecisions && refersToOrCallsPrimalOrImporting) {
                            TapEnv.printlnOnTrace(" Future Units: " + unit + " must have Primal because imports " + importedUnit
                                    + (unitsHavePrimal.retrieve(importedUnit) ? " that must have Primal" : " that imports Primal"));
                        }
                        importingArrows = importingArrows.tail;
                    }
                    // [llh] TODO:Beware of complexity: should precompute the allCallers and allCallees !!
                    // Beware that decisions are different for C: we arbitrarily create
                    //  more _nodiff code for C, so that the C differentiated code is more "standalone".
                    if (!refersToOrCallsPrimalOrImporting) {
                        // Mark as "Import Primal" units that are in a "call" chain between two "Primal"s:
                        TapList<Unit> allCallers = unit.allCallers().tail;
                        boolean primalCaller = TapList.contains(calledPrimalFormUnits, unit);
                        Unit callerUnit = null, calleeUnit = null;
                        while (!primalCaller && allCallers != null) {
                            callerUnit = allCallers.head;
                            primalCaller = (unitsHavePrimal.retrieve(callerUnit) == Boolean.TRUE
                                            || (unit.isC() && TapEnv.get().standaloneDiff
                                                && callerUnit.isTranslationUnit()
                                                && unitsHaveDiff.retrieve(callerUnit) == Boolean.TRUE)
                                            || unitsImportPrimal.retrieve(callerUnit) == Boolean.TRUE
                                            || TapList.contains(calledPrimalFormUnits, callerUnit)) ;
                            allCallers = allCallers.tail;
                        }
                        if (primalCaller) {
                            TapList<Unit> allCallees = unit.allCallees().tail;
                            boolean primalCallee = false;
                            while (!primalCallee && allCallees != null) {
                                calleeUnit = allCallees.head;
                                primalCallee = unitsHavePrimal.retrieve(calleeUnit) == Boolean.TRUE
                                        || unitsImportPrimal.retrieve(calleeUnit) == Boolean.TRUE;
                                allCallees = allCallees.tail;
                            }
                            if (unit.isC() || primalCallee || TapEnv.get().standaloneDiff) {
                                refersToOrCallsPrimalOrImporting = true;
                                if (traceFutureUnitsDecisions) {
                                    TapEnv.printlnOnTrace(
                                            " Future Units: " + unit + " must have Primal because "
                                                    + (callerUnit == null
                                                    ? "it is itself called in Primal form"
                                                    : "it is called by Primal (or called in Primal form) " + callerUnit)
                                                    + (unit.isC()
                                                       ? " and it is C"
                                                       : (primalCallee
                                                          ? " and it calls Primal " + calleeUnit
                                                          : " and one wants a standalone diff code")));
                                }
                            }
                        }
                    }
                    if (refersToOrCallsPrimalOrImporting) {
                        unitsImportPrimal.store(unit, Boolean.TRUE);
                        setAllContainedHavePrimalRec(unit, traceFutureUnitsDecisions, unit);
                        unit = unit.upperLevelUnit();
                        while (unit != null && !unit.isTranslationUnit()) {
                            unitsImportPrimal.store(unit, Boolean.TRUE);
                            if (traceFutureUnitsDecisions) {
                                TapEnv.printlnOnTrace(" Future Units: " + unit + " must have Primal because it contains the previous one");
                            }
                            unit = unit.upperLevelUnit();
                        }
                        stabilized = false;
                    }
                }
            }
        }

        // now inject "Import Primal" into the units that have a copy "Primal code"
        for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
            unit = inUnits.head;
            if (unitsImportPrimal.retrieve(unit) == Boolean.TRUE) {
                unitsHavePrimal.store(unit, Boolean.TRUE);
            }
        }

        if (traceFutureUnitsDecisions) {
            for (inUnits = unitsRootSTLast; inUnits != null; inUnits = inUnits.tail) {
                unit = inUnits.head;
                TapEnv.printlnOnTrace(" Future Units Summary: " + unit + " must have Primal:" + unitsHavePrimal.retrieve(unit) + " must have Diff:" + unitsHaveDiff.retrieve(unit));
            }
        }

    }

    private void setAllContainedHavePrimalRec(Unit unit, boolean traceFutureUnitsDecisions, Unit containerUnit) {
        TapList<Unit> insideUnits = unit.lowerLevelUnits;
        while (insideUnits != null) {
            unitsHavePrimal.store(insideUnits.head, Boolean.TRUE);
            if (traceFutureUnitsDecisions) {
                TapEnv.printlnOnTrace(" Future Units: " + insideUnits.head + " must have Primal because it is contained in " + containerUnit + ", which must have Primal");
            }
            setAllContainedHavePrimalRec(insideUnits.head, traceFutureUnitsDecisions, containerUnit);
            insideUnits = insideUnits.tail;
        }
    }

    private SymbolDecl symbolTableContainsActiveSymbolDecl(SymbolTable symbolTable) {
        boolean result = false;
        SymbolDecl symbolDecl = null;
        TapList<SymbolDecl> symbolDecls = symbolTable.getAllTopSymbolDecls();
        while (symbolDecls != null && !result) {
            symbolDecl = symbolDecls.head;
            if (symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                Unit unit = ((FunctionDecl) symbolDecl).unit();
                result = (unitsHaveDiff.retrieve(unit) == Boolean.TRUE) ;
            } else {
                result = symbolDecl.isActive();
            }
            symbolDecls = symbolDecls.tail;
        }
        return result ? symbolDecl : null;
    }

    private void updateCopiedUnits(boolean updateInterfaceUnits) {
        Unit origCopyUnit;
        TapList<Unit> units;
        TapList<CallArrow> arrows;
        units = srcCallGraph.units();
        while (units != null) {
            setCurUnitEtc(units.head);
            Unit origUnitOfInterface = curUnit.getOrigUnitOfInterface(srcCallGraph);
// System.out.println("UPDATECOPIEDUNIT(s) "+curUnit+" BOOL:"+updateInterfaceUnits+" ORIG:"+origUnitOfInterface) ;
// if (curUnit.functionTypeSpec() != null) System.out.println("T21:"+curUnit.getUnitOfInterface()+" T22:"+curUnit.importedModules()) ;
            if ((!updateInterfaceUnits && !curUnit.isInterface()
                 || updateInterfaceUnits && curUnit.isInterface())
                && curUnit.functionTypeSpec() != null
                // Don't create a new copied name (e.g. name_CD or name_NODIFF) if the copied Unit
                //  is inside another level. The enclosing level should be enough
                //  to disambiguate, and therefore we may keep the original name.
                && (curUnit.isTopInFile()
                    ||
                    (curUnit.isInterface()
                     &&
                     ((origUnitOfInterface != null
                       && origUnitOfInterface.importedModules() != null
                       && (origUnitOfInterface.isTopInFile() || origUnitOfInterface.isInterface()))
                      ||
                      ((curUnit.getUnitOfInterface() == curUnit || curUnit.getUnitOfInterface() == null)
                       && curUnit.importedModules() != null))))) {
                origCopyUnit = adEnv.copiedUnits.retrieve(curUnit);
                if (origCopyUnit != null) {
                    String oldUnitName = curUnit.name();
                    String newUnitName = origCopyUnit.name();
                    String oldUnitNameInOrig = null;
                    String newUnitNameInOrig = null;
                    if (oldUnitName.equals(newUnitName)) {
                        boolean origCopyUnitNameIsSet = false;
                        if (origUnitOfInterface != null
                            && (origUnitOfInterface.isTopInFile()
                                || origUnitOfInterface.isInterface()
                                || origUnitOfInterface.importedModules() != null)) {
                            Unit diffTopUnit = adEnv.copiedUnits.retrieve(origUnitOfInterface);
                            if (diffTopUnit != null) {
                                origCopyUnit.setName(diffTopUnit.name());
                                origCopyUnitNameIsSet = true;
                            }
                        }
                        if (!origCopyUnitNameIsSet) {
                            Tree callTree = ILUtils.build(ILLang.op_call, ILUtils.build(ILLang.op_ident, oldUnitName));
                            Tree newUnitNameTree =
                                    varRefDifferentiator().diffSymbolName(null, oldUnitName, origCopyUnit.publicSymbolTable(),
                                            false, true, false, callTree, null, false,
                                            DiffConstants.ORIGCOPY,
                                            DiffConstants.ORIGCOPY,
                                            DiffConstants.PROC, null);
                            NewSymbolHolder newSymbolHolder = NewSymbolHolder.getNewSymbolHolder(newUnitNameTree);
                            if (newSymbolHolder != null && newSymbolHolder.hasNewFunctionDecl()) {
                                newSymbolHolder.newFunctionDecl().setUnit(origCopyUnit);
                            }

                            SymbolTable upperSymbolTable = origCopyUnit.publicSymbolTable();
                            while (upperSymbolTable != null) {
                                upperSymbolTable.solveNewSymbolHolders(adEnv.emptyCopiedUnitSuffix
                                );
                                upperSymbolTable = upperSymbolTable.basisSymbolTable();
                            }
                            newUnitName = ILUtils.getIdentString(newUnitNameTree);
                            origCopyUnit.setName(newUnitName);
                            curUnit.setOtherName(origCopyUnit);
                            origCopyUnit.setOtherName(curUnit);
                        }
                    }
                    if (origCopyUnit.isAFunction()) {
                        origCopyUnit.setOtherReturnVar(origCopyUnit.publicSymbolTable()
                                .getTopVariableDecl(curUnit.otherReturnVar() != null ? curUnit.otherReturnVar().symbol : oldUnitName));
                    }
                    for (arrows = curUnit.callers(); arrows != null; arrows = arrows.tail) {
                        UnitDiffInfo origDiffInfo = getUnitDiffInfo(arrows.head.origin);
                        for (TapList<Unit> diffUnits = origDiffInfo.getAllDiffsPlusCopy(); diffUnits != null; diffUnits = diffUnits.tail) {
                            Unit callerUnit = diffUnits.head;
                            updateAllUsages(callerUnit, oldUnitName, origCopyUnit);
                            if (!arrows.head.origin().sameLanguage(arrows.head.destination.language())) {
                                oldUnitNameInOrig = srcCallGraph.getOtherLanguageName(oldUnitName,
                                        arrows.head.destination().language(), arrows.head.origin().language());
                                newUnitNameInOrig = diffCallGraph.getOtherLanguageName(newUnitName,
                                        arrows.head.destination().language(), arrows.head.origin().language());
                                updateFunctionDeclName(origCopyUnit, oldUnitNameInOrig, newUnitNameInOrig,
                                        callerUnit.translationUnitSymbolTable());
                            } else {
                                updateFunctionDeclName(origCopyUnit, oldUnitName, newUnitName, callerUnit.translationUnitSymbolTable());
                            }
                            if (arrows.head.isContain() && callerUnit.isModule()) {
                                updateAllAccessDecls(callerUnit.publicSymbolTable().declarationsBlock.instructions,
                                        oldUnitName, origCopyUnit);
                                updateAllAccessDecls(callerUnit.privateSymbolTable().declarationsBlock.instructions,
                                        oldUnitName, origCopyUnit);
                            }
                            // todo update varFunction parameters
                        }
                    }

                    updateAllUsagesInBlock(diffCallGraph.topBlock, oldUnitName, origCopyUnit, origCopyUnit.name());
                    TapList<SymbolTable> diffTUSTs = diffCallGraph.getTranslationUnitSymbolTables();
                    while (diffTUSTs != null) {
                        updateAllUsagesInBlock(diffTUSTs.head.declarationsBlock, oldUnitName, origCopyUnit, origCopyUnit.name());
                        if (oldUnitNameInOrig != null) {
                            updateAllUsagesInBlock(diffTUSTs.head.declarationsBlock, oldUnitNameInOrig, origCopyUnit, newUnitNameInOrig);
                        }
                        diffTUSTs = diffTUSTs.tail;
                    }

                    TapList<Unit> importedModules = curUnit.importedModules();
//                     TapEnv.printlnOnTrace("IMPORTEDMODULES of " + curUnit + " are " + importedModules);
                    Unit importedModule, diffImportedModule;
                    while (importedModules != null) {
                        importedModule = importedModules.head;
                        diffImportedModule = getUnitDiffInfo(importedModule).getDiff();
                        if (diffImportedModule != null) {
//                             TapEnv.printlnOnTrace("UPDATEALLUSAGES OF " + importedModule.name() + " -> " + diffImportedModule + " IN " + origCopyUnit);
                            updateAllUsages(origCopyUnit, importedModule.name(), diffImportedModule);
                        }
                        importedModules = importedModules.tail;
                    }

                    updateFunctionDeclName(origCopyUnit, oldUnitName, newUnitName,
                            diffCallGraph.languageRootSymbolTable(curUnit.language()));
                    updateFunctionDeclName(origCopyUnit, oldUnitName, newUnitName,
                            origCopyUnit.translationUnitSymbolTable());
                }
            }
            units = units.tail;
        }
        setCurUnitEtc(null);
    }


    private void updateFunctionDeclName(Unit origCopyUnit, String oldUnitName, String newUnitName,
                                        SymbolTable symbolTable) {
        if (symbolTable != null && oldUnitName != null) {
            TapList<FunctionDecl> funcDecls = symbolTable.getFunctionDecl(oldUnitName, null, null, false);
            FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
            if (funcDecl != null) {
                FunctionDecl newFunDecl;
                SymbolDecl oldSymbolDecl = symbolTable.getDecl(funcDecl.symbol, funcDecl.kind, true);
                if (oldSymbolDecl != null) {
                    Tree treeName = ILUtils.build(ILLang.op_ident, newUnitName);
                    newFunDecl = new FunctionDecl(treeName, origCopyUnit);
                    newFunDecl.setInstruction(oldSymbolDecl);
                    // Don't remove (or do it later) because the oldUnitName may be searched by successive ILUtils.replaceAllCallName():
                    //symbolTable.removeDecl(funcDecl.symbol, funcDecl.kind, true);
                    oldSymbolDecl.isConsideredRemoved = true;
                    if (funcDecl.unit().enclosingUnitOfInterface != null) {
                        newFunDecl.unit().enclosingUnitOfInterface = funcDecl.unit().enclosingUnitOfInterface;
                    }
                    symbolTable.addSymbolDecl(newFunDecl);
                    //TapEnv.printlnOnTrace("TODO rename " + newFunDecl + " with ... " + oldRenamedSymbolDecl);
                }
            }
        }
    }

    // TODO; Used only by the "-oldContext" option.
    // Remove this code when the "-context" option is systematically better ! (at last...)
    private void differentiateAllocateInCopiedUnits() {
        TapList<Unit> units = srcCallGraph.units();
        Block block;
        Instruction instr, diffInstr;
        Tree tree;
        TapList<Block> allBlocks;
        TapList<Instruction> instructions;
        Tree diffVarTree;
        while (units != null) {
            setCurUnitEtc(units.head);
            if (!curUnit.isOutside() && !curUnit.isInterface()) {
                setCurDiffUnit(adEnv.getPrimalCopyOfOrigUnit(curUnit));
                boolean needInitMultiDirMode = multiDirMode();
                if (curDiffUnit != null) {
                    allBlocks = curDiffUnit.allBlocks();
                    while (allBlocks != null) {
                        block = allBlocks.head;
                        instructions = block.instructions;
                        TapList<Instruction> hdCopiedInstrs = new TapList<>(null, null);
                        TapList<Instruction> tlCopiedInstrs = hdCopiedInstrs;
                        boolean modified = false;
                        while (instructions != null) {
                            instr = instructions.head;
                            tree = instr.tree;
                            if (tree.opCode() == ILLang.op_assign
                                    && tree.down(2).opCode() == ILLang.op_allocate
                                    && hasActiveGlobalVarDecl(
                                    ILUtils.baseName(tree.down(1)),
                                    block.symbolTable,
                                    curDiffUnit.publicSymbolTable().basisSymbolTable())) {
                                modified = true;
                                if (needInitMultiDirMode) {
                                    flowGraphDifferentiator().initializeMultiDirMode();
                                    needInitMultiDirMode = false;
                                }
                                diffVarTree = varRefDifferentiator().diffVarRef(null, tree.down(1),
                                        block.symbolTable, false, tree, multiDirMode(), true, null);
                                Tree diffRhs = varRefDifferentiator().diffVarRef(null, tree.down(2),
                                        block.symbolTable, false, tree, multiDirMode(), true, null);
                                diffInstr = new Instruction(ILUtils.build(ILLang.op_assign,
                                        diffVarTree,
                                        // si struct, -> struc_d
                                        diffRhs));
                                tlCopiedInstrs = tlCopiedInstrs.placdl(diffInstr);
                            } else if (tree.opCode() == ILLang.op_deallocate
                                    && hasActiveGlobalVarDecl(
                                    ILUtils.baseName(tree.down(1)),
                                    block.symbolTable,
                                    curDiffUnit.publicSymbolTable().basisSymbolTable())) {
                                modified = true;
                                if (needInitMultiDirMode) {
                                    flowGraphDifferentiator().initializeMultiDirMode();
                                    needInitMultiDirMode = false;
                                }
                                diffVarTree = varRefDifferentiator().diffVarRef(null, tree.down(1),
                                        block.symbolTable, false, tree, multiDirMode(), true, null);
                                diffInstr = new Instruction(ILUtils.build(ILLang.op_deallocate,
                                        diffVarTree,
                                        ILUtils.copy(tree.down(2))));
                                tlCopiedInstrs = tlCopiedInstrs.placdl(diffInstr);
                            }
//                             if (diffVarTree != null) {
//                                 NewSymbolHolder sHolder = NewSymbolHolder.getNewSymbolHolder(diffVarTree) ;
//                                 if (sHolder != null) {
//                                     VariableDecl var = curDiffUnit.publicSymbolTable().getVariableDecl(ILUtils.baseName(tree.down(1)));
//                                     if (var != null) {
//                                         curDiffUnit.addGlobalAllocatedVariable(var);
//                                     }
//                                 }
//                             }
                            tlCopiedInstrs = tlCopiedInstrs.placdl(instr);
                            instructions = instructions.tail;
                        }
                        if (modified) {
                            block.instructions = hdCopiedInstrs.tail;
                        }
                        allBlocks = allBlocks.tail;
                    }
                }
            }
            units = units.tail;
        }
        setCurUnitEtc(null);
        setCurDiffUnit(null);
    }

    /**
     * @return true when the variable "name" is active and is a global of the enclosing unit.
     * This test is to prevent differentiation of allocates of local vars or formal params
     * of a copied procedure, in mode "-oldContext". cf F90:lha16 and lha17.
     */
    private static boolean hasActiveGlobalVarDecl(String name, SymbolTable localST, SymbolTable
            globalST) {
        VariableDecl varDecl = localST.getVariableDecl(name);
        if (varDecl == null || !varDecl.isActiveSymbolDecl()) {
            return false;
        }
        VariableDecl globalVarDecl = globalST.getVariableDecl(name);
        return varDecl == globalVarDecl;
    }

    /**
     * "met a jour" les op_interfaceDecl et op_externalDecl a l'interieur
     * des units qui ont ete copiees
     * et les interfaces contenant des units copiees.
     */
    private void updateCopiedInterfaceAndExternalDecls(Unit unit,
                                                       TapList<SymbolDecl> functionDecls,
                                                       boolean inPublicSymbolTable) {
        while (functionDecls != null) {
            SymbolTable searchInSymbolTable;
            FunctionDecl functionDecl = (FunctionDecl) functionDecls.head;
            Unit functionUnit = functionDecl.unit();
            if (!functionDecl.symbol.equals(functionUnit.name())) {
                // when mixed language, functionDecl "foo_F" has Unit "foo_C",
                // and we want to retrieve the interfaceUnit of "foo_F":
                TapList<Unit> srcInterfaceUnits = srcCallGraph.interfaceUnits();
                while (srcInterfaceUnits != null) {
                    if (functionDecl.symbol.equals(srcInterfaceUnits.head.name())) {
                        functionUnit = srcInterfaceUnits.head;
                    }
                    srcInterfaceUnits = srcInterfaceUnits.tail;
                }
            }
            Unit copiedFunctionUnit = adEnv.copiedUnits.retrieve(functionUnit);
            String copiedUnitName =
                    (copiedFunctionUnit != null ? copiedFunctionUnit.name() : functionUnit.name());
            boolean change = !copiedUnitName.equals(functionUnit.name());
            if (change) {
                if (inPublicSymbolTable) {
                    searchInSymbolTable = unit.publicSymbolTable();
                } else {
                    searchInSymbolTable = unit.privateSymbolTable();
                }
                if (functionDecl.hasInterfaceInstruction()) {
                    // le nom dans l'interface a deja ete change en "NoDiffCopyName"
                } else if (functionDecl.isExternalDeclared(curDiffUnit.publicSymbolTable().declarationsBlock.instructions)) {
                    Instruction externalDecl = curDiffUnit.publicSymbolTable().declarationsBlock.
                            getOperatorDeclarationInstruction(functionDecl, ILLang.op_external,
                                    searchInSymbolTable);
                    if (externalDecl != null) {
                        ILUtils.replaceAllIdentsNamed(externalDecl.tree, functionDecl.symbol, copiedUnitName);
                    }
                    Instruction varDecl = curDiffUnit.publicSymbolTable().declarationsBlock.
                            getOperatorDeclarationInstruction(functionDecl, ILLang.op_varDeclaration,
                                    searchInSymbolTable);
                    if (varDecl != null) {
                        ILUtils.replaceAllIdentsNamed(varDecl.tree, functionDecl.symbol, copiedUnitName);
                        varDecl.isDifferentiated = true;
                    }
                }
                // changer dans les call:
                TapList<Block> blocks = curDiffUnit.allBlocks();
                TapList<Instruction> instructions;
                Block block;
                Instruction instruction;
                while (blocks != null) {
                    block = blocks.head;
                    instructions = block.instructions;
                    while (instructions != null) {
                        instruction = instructions.head;
                        ILUtils.replaceAllCallName(instruction.tree, functionDecl.symbol, copiedUnitName,
                                null, block.symbolTable);
                        instructions = instructions.tail;
                    }
                    blocks = blocks.tail;
                }
            }
            functionDecls = functionDecls.tail;
        }
    }

    /**
     * "met a jour" les op_externalDecl des units qui ont ete copiees.
     */
    private void updateCopiedExternalDecls(TapList<SymbolDecl> functionDecls,
                                           SymbolTable srcSymbolTable, SymbolTable diffSymbolTable) {
        while (functionDecls != null) {
            FunctionDecl functionDecl = (FunctionDecl) functionDecls.head;
            Unit copiedFunctionUnit = adEnv.copiedUnits.retrieve(functionDecl.unit());
            String copiedUnitName = functionDecl.unit().name();
            if (copiedFunctionUnit != null) {
                copiedUnitName = copiedFunctionUnit.name();
                if (functionDecl.unit().language() != TapEnv.C) {
                    copiedUnitName = diffCallGraph.getMixedLanguageFunctionName(copiedUnitName, TapEnv.FORTRAN,
                            TapEnv.C).first;
                }
            }
            boolean change = !copiedUnitName.equals(functionDecl.unit().name());
            if (change) {
                if (functionDecl.isExternalDeclared(srcSymbolTable.declarationsBlock.instructions)) {
                    updateExternalDecl(functionDecl, srcSymbolTable, diffSymbolTable, copiedUnitName);
                }
            }
            functionDecls = functionDecls.tail;
        }
    }

    private void updateExternalDecl(FunctionDecl functionDecl, SymbolTable srcSymbolTable,
                                    SymbolTable diffSymbolTable, String copiedUnitName) {
        Instruction externalDecl = diffSymbolTable.declarationsBlock.
                getOperatorDeclarationInstruction(functionDecl, ILLang.op_external,
                        srcSymbolTable);
        if (externalDecl == null) {
            externalDecl = diffSymbolTable.declarationsBlock.
                    getOperatorDeclarationInstruction(functionDecl, ILLang.op_varDeclaration,
                            srcSymbolTable);
        }
        if (externalDecl != null) {
            ILUtils.replaceAllIdentsNamed(externalDecl.tree, functionDecl.symbol, copiedUnitName);
        }
    }

    // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
    private void fillPassByValueInfos(TapList publicRanksTree, int argIndex, Unit unit,
                                      BoolVector unitPublicZonesUsed, BoolVector unitPublicZonesWritten,
                                      BoolVector unitPublicZonesWithDiff, BoolVector nonZeroAdjointComing,
                                      boolean[] diffArgsByValueNeedOverwrite, boolean[] diffArgsByValueNeedOnlyIncrement) {
        if (publicRanksTree != null) {
            if (publicRanksTree.head instanceof TapList) {
                // We are on a structure: recursive case.
                while (publicRanksTree != null) {
                    fillPassByValueInfos((TapList) publicRanksTree.head, argIndex, unit,
                            unitPublicZonesUsed, unitPublicZonesWritten, unitPublicZonesWithDiff, nonZeroAdjointComing,
                            diffArgsByValueNeedOverwrite, diffArgsByValueNeedOnlyIncrement);
                    publicRanksTree = publicRanksTree.tail;
                }
            } else {
                // We are above a leaf. Use the info for this public rank.
                // If this leaf is a pointer, the pointed part is passed by reference by definition,
                // so we don't want to look at the pointed part, so we don't iterate on the tail of publicRanksTree.
                TapIntList publicRanks = (TapIntList) publicRanksTree.head;
                int publicRank;
                while (publicRanks != null) {
                    publicRank = publicRanks.head;
                    if (unitPublicZonesWithDiff.get(publicRank)) {
                        if (TypeSpec.isA(unit.paramElemZoneInfo(publicRank).type, SymbolTableConstants.POINTERTYPE)) {
                            // Rationale for a POINTER argument:
                            //  Analysis comes here only for passed-by-value primal arguments, AND for the function result.
                            //   -- When this is the result, the diff pointer will be passed as an argument, and should
                            //       be returned. Therefore we must force PASS-BY-REFERENCE => diffArgsByValueNeedOverwrite=true
                            //   -- For a standard arguments (not the result), the diff argument is also a pointer
                            //       and it may inherit the same passing as the primal argument => ...NeedOverwrite untouched.
                            if (argIndex == 0) {
                                diffArgsByValueNeedOverwrite[argIndex] = true;
                            }
                            // Pointer parts of an argument should not interfere with the "only incremented" detection.
                        } else {
                            // Rationale for a NUMERIC argument:
                            // Any sort of usage (read or written) of the primal arg causes an overwrite of the adjoint:
                            // (What we should return for the result is still unclear...)
                            if (unitPublicZonesUsed.get(publicRank)) {
                                diffArgsByValueNeedOverwrite[argIndex] = true;
                            }
                            // If the formal argument is written, then its adjoint will be "more than" just incremented
                            // therefore mark this argument so that the adjoint routine will place
                            // 2 different adjoints, one local plus one global formal arg:
                            ZoneInfo zi = unit.paramElemZoneInfo(publicRank) ;
                            int dki = zi.kindZoneNb(adEnv.diffKind) ;
                            if (dki!=-1 && unitPublicZonesWritten.get(publicRank)
                                && (nonZeroAdjointComing==null || nonZeroAdjointComing.get(dki))) {
                                diffArgsByValueNeedOnlyIncrement[argIndex] = false;
                            }
                        }
                    }
                    publicRanks = publicRanks.tail;
                }
            }
        }
    }
    // END ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES.

    /**
     * Create and initialize the units that will contain the required
     * differentiated Units of the given "unit" for its given ActivityPattern.
     *
     * @param unit    The (procedure) unit for which to create new differentiated units
     * @param pattern The activity pattern for this differentiation of this unit
     */
    private void createDiffUnits(Unit unit, ActivityPattern pattern) {
        Unit enclosingUnit = unit.upperLevelUnit();
        UnitDiffInfo unitDiffInfo = getUnitDiffInfo(unit);
        Unit diffEnclosingUnitOfTangent = null;
        Unit diffEnclosingUnitOfAdjoint = null;
        if (enclosingUnit != null) {
            UnitDiffInfo enclosingDiffInfo = getUnitDiffInfo(enclosingUnit);
            if (enclosingUnit.isPackage()) {
                diffEnclosingUnitOfTangent = enclosingDiffInfo.getDiff();
                diffEnclosingUnitOfAdjoint = diffEnclosingUnitOfTangent;
            } else if (enclosingUnit.activityPatterns == null) {//Enclosing Unit is passive
                diffEnclosingUnitOfTangent = adEnv.getPrimalCopyOfOrigUnit(enclosingUnit);
                diffEnclosingUnitOfAdjoint = diffEnclosingUnitOfTangent;
            } else {
                ActivityPattern enclosingPattern =
                        pattern.getCallingActivityPattern(enclosingUnit, new TapList<>(pattern, null));
                if (TapEnv.mustTangent()) {
                    diffEnclosingUnitOfTangent = enclosingDiffInfo.getTangent(enclosingPattern);
                }
                if (TapEnv.mustAdjoint()) {
                    diffEnclosingUnitOfAdjoint = enclosingDiffInfo.getAdjoint(enclosingPattern);
                    if (diffEnclosingUnitOfAdjoint == null) {
                        diffEnclosingUnitOfAdjoint = enclosingDiffInfo.getSplitBackward(enclosingPattern);
                    }
                }
            }
        }
        if (TapEnv.mustTangent()) {
            initializeDiffUnit(
                    diffCallGraph.createNewUnit(diffEnclosingUnitOfTangent, unit.language()),
                    unit, DiffConstants.TANGENT, -4);
            if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                TapEnv.printlnOnTrace(" =Prepared tangent unit for " + unit + " wrt activity " + pattern + " => " + curDiffUnit + ", contained in " + curDiffUnit.upperLevelUnit());
            }
            unitDiffInfo.setTangent(pattern, curDiffUnit);
        }
        if (!unit.isModule() && TapEnv.mustAdjoint()) {
            if (unit.mustDifferentiateJoint() || !unit.mustDifferentiateSplit() || pattern.isContext()) {
                initializeDiffUnit(
                        diffCallGraph.createNewUnit(diffEnclosingUnitOfAdjoint, unit.language()),
                        unit, DiffConstants.ADJOINT, -3);
                if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                    TapEnv.printlnOnTrace(" =Prepared" + (pattern.isContext() ? " context" : "") + " adjoint unit for " + unit + " wrt activity " + pattern + " => " + curDiffUnit + ", contained in " + curDiffUnit.upperLevelUnit());
                }
                unitDiffInfo.setAdjoint(pattern, curDiffUnit);
            }
            //[llh] Note: with this algorithm, we cannot safely play SPLIT/JOINT
            // on a procedure that is CONTAIN'ed in a SPLIT procedure !!!
            if (unit.mustDifferentiateSplit() && !pattern.isContext()) {
                initializeDiffUnit(
                        diffCallGraph.createNewUnit(diffEnclosingUnitOfAdjoint, unit.language()),
                        unit, DiffConstants.ADJOINT_FWD, -2);
                if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                    TapEnv.printlnOnTrace(" =Prepared forward split adjoint unit for " + unit + " wrt activity " + pattern + " => " + curDiffUnit + ", contained in " + curDiffUnit.upperLevelUnit());
                }
                unitDiffInfo.setSplitForward(pattern, curDiffUnit);
                initializeDiffUnit(
                        diffCallGraph.createNewUnit(diffEnclosingUnitOfAdjoint, unit.language()),
                        unit, DiffConstants.ADJOINT_BWD, -1);
                if (adEnv.traceCurDifferentiation || TapEnv.traceActivity() != null) {
                    TapEnv.printlnOnTrace(" =Prepared backward split adjoint unit for " + unit + " wrt activity " + pattern + " => " + curDiffUnit + ", contained in " + curDiffUnit.upperLevelUnit());
                }
                unitDiffInfo.setSplitBackward(pattern, curDiffUnit);
            }
        }
    }

    /**
     * @param orderOffset controls the order of appearance in the generated code
     */
    private void initializeDiffUnit(Unit createdDiffUnit, Unit unit, int sortOfDiffUnit, int orderOffset) {
        // TODO JH use new UnitDiffInfo structure
        setCurDiffUnit(createdDiffUnit);
        curDiffUnit.setKind(unit);
        curDiffUnit.sortOfDiffUnit = sortOfDiffUnit;
        curDiffUnit.origUnit = unit;
        curDiffUnit.arrayDimMin = unit.isFortran() ? 1 : 0;
        if (unit == srcCallGraph.getMainUnit()) {
            diffCallGraph.setMainUnit(curDiffUnit);
        }
        curDiffUnit.userHelp = new TapList<>(null, unit.userHelp.tail);
        curDiffUnit.formats = unit.formats;
        if (unit.isCalledFromOtherLanguage()) {
            curDiffUnit.setCalledFromOtherLanguage();
        }
        if (ILUtils.isNullOrEmptyList(unit.modifiers)) {
            curDiffUnit.modifiers = ILUtils.copy(unit.modifiers);
        } else if (unit.modifiers.opCode() == ILLang.op_accessDecl) {
            // F2003 bind:
            curDiffUnit.modifiers = ILUtils.copy(unit.modifiers);
            diffCallGraph.addBindFortranCDiffUnit(curDiffUnit);
        } else {
            Tree[] origModifiers = unit.modifiers.children();
            TapList<Tree> newModifiers = null;
            for (int i = origModifiers.length - 1; i >= 0; --i) {
                if (origModifiers[i] != null &&
                        !ILUtils.getIdentString(origModifiers[i]).equals("pure")) {
                    newModifiers = new TapList<>(ILUtils.copy(origModifiers[i]), newModifiers);
                }
            }
            curDiffUnit.modifiers = ILUtils.build(ILLang.op_modifiers, newModifiers);
        }
        curDiffUnit.setLanguage(unit.language());
        curDiffUnit.setLostComments(unit.lostComments());
        curDiffUnit.position = unit.getPosition() + orderOffset;
        curDiffUnit.preComments =
                unit.preComments == null ? null : unit.preComments.copy();
        curDiffUnit.postComments =
                unit.postComments == null ? null : unit.postComments.copy();
        curDiffUnit.preCommentsBlock =
                unit.preCommentsBlock == null ? null : unit.preCommentsBlock.copy();
        curDiffUnit.postCommentsBlock =
                unit.postCommentsBlock == null ? null : unit.postCommentsBlock.copy();
        if (unit.isInterface()) {
            curDiffUnit.setEntryBlock(new EntryBlock(null));
            curDiffUnit.setExitBlock(new ExitBlock(null));
        }
        if (unit.hasArrayNotation()) {
            curDiffUnit.setHasArrayNotation();
        }
    }

    public boolean patternCreatesDiffCode(ActivityPattern activityPattern, Unit srcUnit) {
        return (!activityPattern.isDontDiff()
                // Don't create standard diff routines for MPI externals, because
                // Tapenade creates calls to specific AMPI diff routines:
                && !MPIcallInfo.isMessagePassingFunction(srcUnit.name(), srcUnit.language())
                && (
                    isOrContainsActingPattern(srcUnit, activityPattern, activityAnalyzer(), TapEnv.reqExplicitAnalyzer())
                    ||
                    (TapEnv.mustContext() && TapList.contains(adEnv.rootUnits, srcUnit))
                    ||
                    (activityPattern.isContext() && TapList.contains(adEnv.callersOfRootUnits, srcUnit))
                    ));
    }

    private static boolean isOrContainsActingPattern(Unit unit, ActivityPattern activityPattern,
                                                     ADActivityAnalyzer activityAnalyzer, ReqExplicit reqExplicitAnalyzer) {
        boolean active = isReallyActivePattern(activityPattern, activityAnalyzer, reqExplicitAnalyzer);
        if (!active) {
            TapList<Unit> containedUnits = unit.collectContainedUnits(null);
            Unit containedUnit;
            ActivityPattern containedUnitActivityPattern;
            while (!active && containedUnits != null) {
                containedUnit = containedUnits.head;
                TapList<ActivityPattern> containedUnitActivityPatterns = containedUnit.activityPatterns;
                while (!active && containedUnitActivityPatterns != null) {
                    containedUnitActivityPattern = containedUnitActivityPatterns.head;
                    if (isReallyActivePattern(containedUnitActivityPattern, activityAnalyzer, reqExplicitAnalyzer)
                            && (activityPattern.isDisconnected() ||
                            containedUnitActivityPattern.isCalledBy(unit, activityPattern,
                                    new TapList<>(containedUnitActivityPattern, null)))) {
                        active = true;
                    }
                    containedUnitActivityPatterns = containedUnitActivityPatterns.tail;
                }
                containedUnits = containedUnits.tail;
            }
        }
        return active;
    }

    /**
     * returns true if this ActivityPattern is "acting", which means it causes creation of a differentiated
     * Unit, because either it computes active values or manages active memory locations.
     */
    private static boolean isReallyActivePattern(ActivityPattern activity,
                                                 ADActivityAnalyzer activityAnalyzer, ReqExplicit reqExplicitAnalyzer) {
        boolean isReallyActive = false;
        if (activity != null) {
            isReallyActive = activityAnalyzer.isActiveUnit(activity)
                || (reqExplicitAnalyzer != null &&
                    reqExplicitAnalyzer.isPointerActiveUnit(activity));
        }
        return isReallyActive;
    }

    private void createDiffUnitSymbolTables (TapList<TapPair<SymbolTable, SymbolTable>> contextAssociationsST) {
        if (curUnit.isStandard() || curUnit.isPackage() || curUnit.isInterface() || curUnit.isExternal()
                || curUnit.isVarFunction() && curUnit.publicSymbolTable() != null) {
            SymbolTable diffPublicSymbolTable = null;
            SymbolTable diffPrivateSymbolTable;
            if (curUnit.publicSymbolTable() != null) {
                diffPublicSymbolTable =
                        curUnit.publicSymbolTable().smartCopy(contextAssociationsST, curDiffUnit, diffCallGraph,
                                                              adEnv.copiedUnits, "Diff of ");
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("   public:"+curUnit.publicSymbolTable()) ;
                    TapEnv.printlnOnTrace("   ---->> diff public:"+diffPublicSymbolTable) ;
                }
                if (curUnit.isProcedure() || curUnit.isVarFunction()) {
                    diffPublicSymbolTable.declareFormalParamsLevel();
                }
                curDiffUnit.setPublicSymbolTable(diffPublicSymbolTable);
                declareDiffSymbolTableIfNew(curDiffUnit, diffPublicSymbolTable);
            }
            if (curUnit.privateSymbolTable() != null) {
                diffPrivateSymbolTable =
                        curUnit.privateSymbolTable().smartCopy(contextAssociationsST, curDiffUnit, diffCallGraph,
                                                               adEnv.copiedUnits, "Diff of ");
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("   private:"+curUnit.privateSymbolTable()) ;
                    TapEnv.printlnOnTrace("   ---->> diff private:"+diffPrivateSymbolTable) ;
                }
                curDiffUnit.setPrivateSymbolTable(diffPrivateSymbolTable);
                declareDiffSymbolTableIfNew(curDiffUnit, diffPrivateSymbolTable);
                diffPrivateSymbolTable.saveList = curUnit.privateSymbolTable().saveList;
                diffPrivateSymbolTable.nameListList = curUnit.privateSymbolTable().nameListList;
            }
            if (curDiffUnit.entryBlock() != null) {
                curDiffUnit.entryBlock().symbolTable = diffPublicSymbolTable;
            }
            if (curDiffUnit.exitBlock() != null) {
                curDiffUnit.exitBlock().symbolTable = diffPublicSymbolTable;
            }
            if (curUnit.isStandard() || curUnit.isInterface() || curUnit.isPackage()) {
                TapList<Block> copyBodyBlocks = curDiffUnit.allBlocks();
                Block copyBodyBlock;
                SymbolTable origST ;
                while (copyBodyBlocks != null) {
                    copyBodyBlock = copyBodyBlocks.head;
                    origST = copyBodyBlock.symbolTable ;
                    copyBodyBlock.symbolTable = origST.smartCopy(
                            contextAssociationsST, curDiffUnit, diffCallGraph, adEnv.copiedUnits, "Diff of ");
                    if (adEnv.traceCurDifferentiation) {
                        TapEnv.printlnOnTrace("   inside:"+origST) ;
                        TapEnv.printlnOnTrace("   ---->> diff inside:"+copyBodyBlock.symbolTable+" for Block "+copyBodyBlock) ;
                    }
                    declareDiffSymbolTableIfNew(curDiffUnit, copyBodyBlock.symbolTable);
                    copyBodyBlocks = copyBodyBlocks.tail;
                }
            }
            TapList<SymbolTable> newDiffSymbolTablesTopDown = TapList.reverse(curDiffUnit.symbolTablesBottomUp());
            // Important to sweep newDiffSymbolTable's top-down because some may share the same declarationsBlock,
            // and this shared declarationsBlock must have the deepest symbolTable.
            SymbolTable newDiffSymbolTable;
            while (newDiffSymbolTablesTopDown != null) {
                newDiffSymbolTable = newDiffSymbolTablesTopDown.head;
                if (newDiffSymbolTable.declarationsBlock != null) {
                    newDiffSymbolTable.declarationsBlock.symbolTable = newDiffSymbolTable;
                }
                newDiffSymbolTablesTopDown = newDiffSymbolTablesTopDown.tail;
            }
        }
    }

//     /**
//      * Builds a new SymbolTable associations list "localAssociationsST" to be used to
//      * create the SymbolTables of the current derivative curDiffUnit of curUnit, i.e. the derivative
//      * for the current differentiation mode. This construction uses existing associations
//      * for the context Symbol Tables, given in "contextAssociationsST", but this context
//      * info must not be modified, and must not share with the built "localAssociationsST",
//      * because this context association info must remain independent from the diff mode.
//      */
//     private TapList<TapPair<SymbolTable, SymbolTable>> buildLocalAssociationsST
//     (TapList<TapPair<SymbolTable, SymbolTable>> contextAssociationsST) {
//         TapList<TapPair<SymbolTable, SymbolTable>> localAssociationsST = new TapList<>(null, null);
//         SymbolTable origContextST = curUnit.importsSymbolTable() ;
// //         if (origContextST==null) origContextST = curUnit.externalSymbolTable(); // useless protection ?
// //         Unit diffContextUnit = curDiffUnit.upperLevelUnit();
// //         SymbolTable differentiatedContextST;
// //         if (diffContextUnit != null) {
// //             differentiatedContextST = diffContextUnit.bodySymbolTable();
// //         } else if (origContextST == curUnit.translationUnitSymbolTable()) {
// //             differentiatedContextST = curUnit.translationUnitSymbolTable().getExistingCopy(contextAssociationsST);
// //         } else {
// //             differentiatedContextST = diffCallGraph.languageRootSymbolTable(curUnit.language());
// //         }

//         localAssociationsST.placdl(new TapPair<>(origContextST, TapList.cassq(origContextST, contextAssociationsST))) ;
// //WAS         localAssociationsST.placdl(new TapPair<>(origContextST, differentiatedContextST));
//         return localAssociationsST;
//     }

// [vmp] remarque sur solveNewSymbolHolders...() : necessaire sur privateSymbolTable et publicSymbolTable
//   car on ne peut pas faire "solveNewSymbolHolders" sur les modules a la fin de differentiatePackage:
//   c'est trop tot a cause des  utilisations de ces modules dans des units non traitees. cf nonRegrF90/REFERENCES/v29

    private void solveNewSymbolHoldersOfCallGraphTop(CallGraph diffCallGraph,
                                                            boolean emptyCopiedUnitSuffix) {
        TapList<SymbolTable> tuSTs = diffCallGraph.getTranslationUnitSymbolTables();
        while (tuSTs != null) {
            tuSTs.head.solveNewSymbolHolders(emptyCopiedUnitSuffix);
            tuSTs = tuSTs.tail;
        }
        diffCallGraph.cudaRootSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        diffCallGraph.cRootSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        diffCallGraph.fortranRootSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        diffCallGraph.globalRootSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        if (rootDirNumberMaxSymbolHolder!=null) {
            rootDirNumberMaxSymbolHolder.solve(diffCallGraph.globalRootSymbolTable(), adEnv.emptyCopiedUnitSuffix) ;
        }
        if (TapEnv.associationByAddress()) {
            diffCallGraph.globalRootSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        }
    }

    /**
     * Solve pending/waiting NewSymbolHolders in SymbolTables of each Unit in units.
     */
    private static void solveNewSymbolHoldersOfUnits(TapList<Unit> units,
                                                     boolean emptyCopiedUnitSuffix) {
        while (units != null) {
            if (units.head.isTranslationUnit()) {
                solveNewSymbolHoldersOfUnits(units.head.lowerLevelUnits, emptyCopiedUnitSuffix);
            }
            solveNewSymbolHoldersOfUnit(units.head, emptyCopiedUnitSuffix);
            units = units.tail;
        }
    }

    protected static void solveNewSymbolHoldersOfUnit(Unit unit, boolean emptyCopiedUnitSuffix) {
        boolean dirNumberMaxSymbolHolderIsUsed =
                unit.dirNumberMaxSymbolHolder != null && unit.dirNumberMaxSymbolHolder.isUsed();
        TapList<SymbolTable> listST = unit.symbolTablesBottomUp();
        while (listST != null) {
            listST.head.solveNewSymbolHolders(emptyCopiedUnitSuffix);
            listST = listST.tail;
        }
        if (unit.importsSymbolTable()!=null) {
            unit.importsSymbolTable().solveNewSymbolHolders(emptyCopiedUnitSuffix);
        }
        if (dirNumberMaxSymbolHolderIsUsed) {
            unit.dirNumberMaxSymbolHolder.solve(unit.publicSymbolTable(),
                    emptyCopiedUnitSuffix);
            String nbDirsMaxVarName = unit.dirNumberMaxSymbolHolder.finalName();
            // In Fortran, attach this message to procedures and modules ; In C, attach it to files :
            if (unit.isFortran() != unit.isTranslationUnit()) {
                TapEnv.pushRelatedUnit(unit); // Attach this message only to the diff package!
                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(AD10) User help requested: constant " + nbDirsMaxVarName + " must hold the maximum number of differentiation directions");
                TapEnv.popRelatedUnit();
                String msg = "the maximum number of differentiation directions";
                unit.userHelp.placdl(new TapPair<>(msg, nbDirsMaxVarName));
            }
        }
    }

    /**
     * Compute the activity of each component of each COMMON/EQUIVALENCE,
     * considering all uses in the given "unit".
     */
    private void computeCommonActivities(ActivityPattern activity) {
        Unit unit = activity.unit();
        // There may be no info on elementary parameters for VarFunction,
        // i.e. the units of functions passed as argument :
        if (unit.hasParamElemsInfo()) {
            //Unit sourceUnit = null; // TODO JH how to deal with interfaces? Do they have activityPatterns?
            //if (unit.isInterface())
            //    sourceUnit = srcCallGraph.getUnit(unit.name(), unit.functionTypeSpec());
            BoolVector diffFormalRequired = ADActivityAnalyzer.functionDiffFormalRequired(activity, true);
            if (diffFormalRequired != null) {
                TapList<AlignmentBoundary> activeBoundaries;
                MemMap commonMap ;
                for (int i = unit.paramElemsNb() - 1; i >= 0; --i) {
                    ZoneInfo zoneInfo = unit.paramElemZoneInfo(i);
                    if (zoneInfo != null
                        && zoneInfo.commonName!=null
                        && diffFormalRequired.get(i)) {
                        commonMap = adEnv.commonActivityMemoryMap.getSetMemMap(zoneInfo.commonName);
                        commonMap.setActiveRegion(zoneInfo.startOffset, zoneInfo.endOffset, zoneInfo.infiniteEndOffset) ;
                        TapList<String> varNames = zoneInfo.variableNames();
                        while (varNames != null) {
                            VariableDecl varDecl = null;
                            if (unit.privateSymbolTable() != null) {
                                varDecl = unit.privateSymbolTable().getVariableDecl(varNames.head);
                            }
                            if (varDecl != null) {
                                varDecl.setActive();
                            }
                            varNames = varNames.tail;
                        }
                    }
                }
            }
        }
    }

    private void computeModuleCommonActivities(Unit unit) {
        TapList<AlignmentBoundary> activeBoundaries;
        TapList<Int2ZoneInfo> zoneInfos =
                unit.publicSymbolTable().getAdditionalDuplicatedDeclaredZoneInfos()[SymbolTableConstants.ALLKIND];
        MemMap commonMap ;
        Int2ZoneInfo iZoneInfo;
        ZoneInfo zoneInfo;
        while (zoneInfos != null) {
            iZoneInfo = zoneInfos.head;
            zoneInfo = iZoneInfo.getZoneInfo();
            if (zoneInfo != null
                && zoneInfo.kind() == SymbolTableConstants.GLOBAL
                && zoneInfo.commonName != null) {
//TODO: we should probably at least check that zoneInfo is active ?
                commonMap = adEnv.commonActivityMemoryMap.getSetMemMap(zoneInfo.commonName);
                commonMap.setActiveRegion(zoneInfo.startOffset, zoneInfo.endOffset, zoneInfo.infiniteEndOffset) ;
                TapList<String> varNames = zoneInfo.variableNames();
                while (varNames != null) {
                    VariableDecl varDecl = null;
                    if (unit.privateSymbolTable() != null) {
                        varDecl = unit.privateSymbolTable().getVariableDecl(varNames.head);
                    }
                    if (varDecl != null) {
                        varDecl.setActive();
                    }
                    varNames = varNames.tail;
                }
            }
            zoneInfos = zoneInfos.tail;
        }
    }

    /**
     * TODO JH
     * Some common bugs in the new code (if something is weird, check this first):<br>
     * - check that the right activityPattern is used. Sometimes there is curActivity instead of calledActivity.
     * I might have missed some places where the code did not work on curUnit (which usually needs curActivity),
     * but instead used something like origUnit, tree.down(2).something etc., and this is especially untested for
     * C code, code that works on derived datatypes and who knows what else.<br>
     * - all places with getAllDiffs() are suspicious. The old code sometimes put special meanings in the
     * position of a diffUnit in the array of allDiffUnits, e.g. a tangent unit was always in position 1.
     * This is no longer the case, so watch out for nasty bugs.<br>
     * Other known bugs:<br>
     * - Interfaces are broken. We have to sometimes split interfaces, and in general, I had to cut some corners
     * whenever interfaces are involved. I am not even sure if interface units currently have activityPatterns,
     * and if so, what values they carry.<br>
     * - Functions passed as arguments are broken. consider a call foo(x,e) where e is a function passed as arg.
     * If e is used within foo with two different activities, we need to generate foo(x,xd,e,e_d1,e_d2) where
     * e_d1 and e_d2 point to the correct versions of e_d. I haven't even tried to implement this<br>
     * - "specialize" directives at subroutine headers or subroutine calls are not implemented yet.
     */

    /**
     * Build the FunctionTypeSpec of the current curDiffUnit, which is the differentiate of the
     * current "curUnit" in differentiation mode "mode" (in {TANGENT, ADJOINT, ADJOINT_FWD, ADJOINT_BWD})
     * and for activity pattern "pattern"
     */
    private FunctionTypeSpec buildFunctionTypeSpecOfCurDiffUnit(ActivityPattern pattern, int mode) {
        FunctionTypeSpec functionTypeSpec = curUnit.functionTypeSpec();
        if (adEnv.traceCurDifferentiation) {
            TapEnv.printlnOnTrace("  Differentiating FunctionTypeSpec of " + curUnit + " (of type:" + functionTypeSpec + ") in mode:" + mode + " wrt ActivityPattern:" + pattern);
        }
        boolean vectorDiffOfOutside = (multiDirMode() && curUnit.isOutside());
        // Hide the "NBDirsMax" variable from the functionTypeSpec of differentiated externals+intrinsics:
        ArrayDim extraMultiDirArrayDim = (vectorDiffOfOutside
                ? new ArrayDim(ILUtils.build(ILLang.op_dimColon), 0, null, null, -1, 0, 0)
                : varRefDifferentiator().getMultiDirDimensionMax());
        SymbolTable origArgsSymbolTable = curUnit.publicSymbolTable();
        // publicSymbolTable may be null e.g. for externals:
        if (origArgsSymbolTable == null) {
            origArgsSymbolTable = curUnit.externalSymbolTable();
        }
        SymbolTable diffArgsSymbolTable = curDiffUnit.publicSymbolTable();
        // publicSymbolTable may be null e.g. for externals:
        if (diffArgsSymbolTable == null) {
            diffArgsSymbolTable = curDiffUnit.externalSymbolTable();
        }
        // Hack to dump away into root SymbolTable all the AssocAddress derivatives of
        // types used by Intrinsics or Externals. These types are often unfinished,
        //  and their diff types should not be shown in the "AATYPES"-like modules:
        if (curDiffUnit.isOutside()) {
            diffArgsSymbolTable = diffCallGraph.globalRootSymbolTable();
        }
        WrapperTypeSpec[] paramsTypes = functionTypeSpec.argumentsTypes;
        FunctionTypeSpec diffFuncType;
        if (paramsTypes == null) {
            diffFuncType = new FunctionTypeSpec(new WrapperTypeSpec(new VoidTypeSpec()), new WrapperTypeSpec[0]);
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("  [No info]--> result diff type:" + diffFuncType);
            }
            return diffFuncType;
        } else {
            WrapperTypeSpec returnType = functionTypeSpec.returnType;
            if (WrapperTypeSpec.isNullOrVoid(returnType)) {
                returnType = null;
            }
            WrapperTypeSpec diffReturnType = null;
            boolean[] formalArgsActivity = getUnitFormalArgsActivityS(pattern);
            boolean[] formalArgsPointerActivity =
                    ReqExplicit.formalArgsPointerActivity(pattern, paramsTypes.length);
            boolean[] diffArgsByValueNeedOverwrite = null;
            if (mode == DiffConstants.ADJOINT || mode == DiffConstants.ADJOINT_BWD) {
                UnitDiffInfo unitDiffInfo = getUnitDiffInfo(curUnit);
                diffArgsByValueNeedOverwrite = unitDiffInfo.getUnitDiffArgsByValueNeedOverwriteInfoS(pattern);
            }
            if (adEnv.traceCurDifferentiation) {
                int i;
                TapEnv.printOnTrace("    argsActivities: [");
                for (i = 0; i < paramsTypes.length; ++i) {
                    if (i > 0) {
                        TapEnv.printOnTrace(", ");
                    }
                    TapEnv.printOnTrace((formalArgsActivity == null ? "?" : formalArgsActivity[i]) + ":ptr_" + (formalArgsPointerActivity == null ? "?" : formalArgsPointerActivity[i]) + ":ow_" + (diffArgsByValueNeedOverwrite == null ? "?" : diffArgsByValueNeedOverwrite[i + 1]));
                }
                i = paramsTypes.length;
                TapEnv.printlnOnTrace("  --> " + (formalArgsActivity == null ? "?" : formalArgsActivity[i]) + ":ptr_" + (formalArgsPointerActivity == null ? "?" : formalArgsPointerActivity[i]) + "]");
            }
            TapList<WrapperTypeSpec> diffArgTypes = null;
            if (multiDirMode()) {
                diffArgTypes = new TapList<>(new WrapperTypeSpec(new PrimitiveTypeSpec("integer")), diffArgTypes);
            }

            // If return value is active, differentiate the original result type:
            if (returnType != null
                    && (formalArgsActivity == null
                    || formalArgsActivity[paramsTypes.length]
                    || formalArgsPointerActivity[paramsTypes.length])) {
                // Copy to avoid annotating returnType with its diffReturnType for this special case:
                WrapperTypeSpec returnTypeToDiff = (vectorDiffOfOutside ? (WrapperTypeSpec) returnType.copy() : returnType);
                diffReturnType =
                        returnTypeToDiff.differentiateTypeSpecMemo(diffArgsSymbolTable, origArgsSymbolTable,
                                curDiffUnitSort(), suffixes()[DiffConstants.DIFFERENTIATED][DiffConstants.TYPE],
                                false, multiDirMode(), extraMultiDirArrayDim,
                                curUnit.name() + "'s result", curUnit.name() + "_res", null, null);
                if (diffReturnType == null
                        // diffReturnType "null" means that the diff return type is the same as the orig return Type
                        || diffReturnType.equalsCompilDep(returnType)) {
                    diffReturnType = returnType;
                } else if (WrapperTypeSpec.isNullOrVoid(diffReturnType)) {
                    diffReturnType = null;
                }
            }

            if ((!pattern.isContext() && mode == DiffConstants.ADJOINT) || mode == DiffConstants.ADJOINT_BWD) {
                // All cases of "backward-style" differentiated procedures:
                // Forget primal return. Pass adjoint result as a (IN) argument:
                if (diffReturnType != null) {
                    for (int iReplic=TapEnv.diffReplica()-1 ; iReplic>=0 ; --iReplic) {
                        diffArgTypes = new TapList<>(diffReturnType, diffArgTypes);
                    }
                }
                diffReturnType = new WrapperTypeSpec(new VoidTypeSpec());
            } else {
                // All cases of "forward-style" differentiated procedures:
                if (diffReturnType != null) {
                    // in general diff return becomes return.
                    // Exception cases where we think it's better to turn diff of return into a formal argument:
                    if (!pattern.isContext()
                            && !procedureCallDifferentiator().diffFunctionIsFunction(curUnit)) {
                        // When needed, turn return type into a ref to pointer type:
                        if (MixedLanguageInfos.passesByValue(curUnit.language(), curUnit.language())) {
                            diffReturnType = new WrapperTypeSpec(new PointerTypeSpec(diffReturnType, null));
                        }
                        for (int iReplic=TapEnv.diffReplica()-1 ; iReplic>=0 ; --iReplic) {
                            diffArgTypes = new TapList<>(diffReturnType, diffArgTypes);
                        }
                        diffReturnType = new WrapperTypeSpec(new VoidTypeSpec());
                    }
                    // Primal return becomes a (OUT) argument:
                    if (returnType != null && !TapEnv.associationByAddress()) {
                        if (MixedLanguageInfos.passesByValue(curUnit.language(), curUnit.language())) {
                            returnType = new WrapperTypeSpec(new PointerTypeSpec(returnType, null));
                        }
                        diffArgTypes = new TapList<>(returnType, diffArgTypes);
                    }
                } else {
                    // When no need to return a diff value, return the orig value:
                    if (!pattern.isContext()
                            && !procedureCallDifferentiator().diffFunctionIsFunction(curUnit)) {
                        if (returnType != null) {
                            diffArgTypes = new TapList<>(returnType, diffArgTypes);
                        }
                        diffReturnType = new WrapperTypeSpec(new VoidTypeSpec());
                    } else {
                        diffReturnType = (returnType != null ? returnType : new WrapperTypeSpec(new VoidTypeSpec()));
                    }
                }
            }

            // Differentiation of formal arguments:
            if (formalArgsActivity != null) {
                WrapperTypeSpec paramType;
                WrapperTypeSpec diffParamType;
                for (int i = paramsTypes.length - 1; i >= 0; i--) {
                    paramType = paramsTypes[i];
                    // Differentiate of original param:
                    //[llh] TODO: the base type should be differentiated e.g. RECORD to RECORD_D !!
                    if (formalArgsActivity[i] || formalArgsPointerActivity[i]) {
                        // Copy to avoid annotating paramType with its diffParamType for this special case:
                        WrapperTypeSpec paramTypeToDiff =
                                (vectorDiffOfOutside ? (WrapperTypeSpec) paramType.copy() : paramType);
                        diffParamType =
                                paramTypeToDiff.differentiateTypeSpecMemo(diffArgsSymbolTable, origArgsSymbolTable,
                                        curDiffUnitSort(), suffixes()[DiffConstants.DIFFERENTIATED][DiffConstants.TYPE],
                                        false, multiDirMode(), extraMultiDirArrayDim,
                                        curUnit.name() + "'s arg" + (i + 1), curUnit.name() + "_arg" + (i + 1), null, null);
                        // "null" means that the diffType is the same as the orig Type !
                        if (diffParamType == null) {
                            diffParamType = paramType;
                        } else if (diffParamType.equalsLiterally(paramType)) {
                            diffParamType = paramType;
                        }
                        if (multiDirMode() && !TypeSpec.isA(diffParamType, SymbolTableConstants.FUNCTIONTYPE)) {
                            // ABOUT REVERSE DIFF OF PASS-BY-VALUE VARIABLES:
                            if (curUnit.takesArgumentByValue(i + 1, curUnit.language())
                                    && (mode == DiffConstants.ADJOINT || mode == DiffConstants.ADJOINT_BWD)
                                    && diffArgsByValueNeedOverwrite[i + 1]) {
                                diffParamType = new WrapperTypeSpec(new PointerTypeSpec(diffParamType, null));
                            }
                        }
                        for (int iReplic=TapEnv.diffReplica()-1 ; iReplic>=0 ; --iReplic) {
                            diffArgTypes = new TapList<>(diffParamType, diffArgTypes);
                        }
                    }
                    // Original param:
                    if (!TapEnv.associationByAddress() || !(formalArgsActivity[i] || formalArgsPointerActivity[i])) {
                        diffArgTypes = new TapList<>(paramType, diffArgTypes);
                    }
                }
            }
            diffFuncType = new FunctionTypeSpec(diffReturnType, diffArgTypes);
            if (adEnv.traceCurDifferentiation) {
                TapEnv.printlnOnTrace("  --> result diff type:" + diffFuncType);
            }
            return diffFuncType;
        }
    }

    /**
     * Differentiate a var function Unit "sourceUnit",
     * i.e. fill each of its differentiated counterparts for each diff mode.
     */
    private void differentiateVarFunctionUnit(ActivityPattern activity) {
        Unit sourceUnit = activity.unit();
        UnitDiffInfo sourceDiffInfo = getUnitDiffInfo(sourceUnit);
        TapList<Unit> diffUnits;
        for (diffUnits = sourceDiffInfo.getAllDiffs(activity); diffUnits != null; diffUnits = diffUnits.tail) {
            //ICI REVOIR LE PARCOURS DES MODES:
            setCurDiffUnit(diffUnits.head);
            curDiffUnit.setVarFunction();
            flowGraphDifferentiator().initializeMultiDirMode();
            if (curDiffUnit == sourceDiffInfo.getAdjoint(activity))
            // En mode inverse il n'y a plus de fonction: si F est une fonction,
            // Fbar sera une subroutine
            {
                curDiffUnit.functionTypeSpec().returnType = new WrapperTypeSpec(new VoidTypeSpec());
            }
        }
    }

    /**
     * Differentiate a Unit that only has declarative role, i.e. no procedural code.
     * This means that the control flow between the Blocks inside only materializes the
     * ordering of the declarations/definitions inside, and the nesting of
     * scopes or namespaces. This is the case e.g. for a file, a module or a class.
     * This also implies that there is only one diff Unit created, not even a copy Unit.
     */
    private void differentiatePackage(Unit sourceUnit, TapList<TapPair<SymbolTable, SymbolTable>> rootAssociationsST) {
        setCurDiffUnit(getUnitDiffInfo(sourceUnit).getDiff());
        if (curDiffUnit.isDiffPackage()) {
            String sourceName = sourceUnit.name();
            unitDiffTimer.reset();
            if (sourceUnit.isFortran() && sourceUnit.isModule()) {
                TapEnv.printOnTrace(15, "@@ Differentiation of module: " + sourceName + (TapEnv.mustTangent() ? " in TGT" : (TapEnv.mustAdjoint() ? " in ADJ" : " in COPY")));
            } else {
                TapEnv.printOnTrace(15, "@@ Differentiation of package: " + sourceName + (TapEnv.mustTangent() ? " in TGT" : (TapEnv.mustAdjoint() ? " in ADJ" : " in COPY")));
            }
            // For readability of the trace:
            if (adEnv.someUnitsAreTraced()) {
                TapEnv.printlnOnTrace(15);
            }
            flowGraphDifferentiator().initializeMultiDirMode();
            int sort = (TapEnv.mustTangent() ? DiffConstants.TANGENT : (TapEnv.mustAdjoint() ? DiffConstants.ADJOINT : DiffConstants.ORIGCOPY));
            adEnv.setCurDiffSorts(sort, sort);
            if (sourceUnit.isTranslationUnit()) {
                curDiffUnit.setName(sourceName);
            } else {
                curDiffUnit.parametersOrModuleNameTree = varRefDifferentiator().diffSymbolName(null, sourceName, curDiffUnit.publicSymbolTable(),
                        false, false, true, null, null, true,
                        DiffConstants.DIFFERENTIATED, DiffConstants.DIFFERENTIATED,
                        DiffConstants.MOD, null);
                curDiffUnit.publicSymbolTable().saveList = sourceUnit.publicSymbolTable().saveList;
                flowGraphDifferentiator().differentiateSaveList(curDiffUnit.privateSymbolTable());
                flowGraphDifferentiator().differentiateSaveList(curDiffUnit.protectedSymbolTable());
                flowGraphDifferentiator().differentiateSaveList(curDiffUnit.publicSymbolTable());
            }

            Block diffBlock;
            for (TapList<Block> diffBlocks = curDiffUnit.allBlocks(); diffBlocks != null; diffBlocks = diffBlocks.tail) {
                diffBlock = diffBlocks.head;
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("----------- DIFFERENTIATION OF " + sourceName + " package's Block: " + diffBlock);
                    TapEnv.printlnOnTrace("  Block contents before differentiation:");
                    TapList<Instruction> diffInstrs = diffBlock.instructions;
                    int i = 1;
                    while (diffInstrs != null) {
                        System.out.println("       " + i + " :: " + diffInstrs.head);
                        diffInstrs = diffInstrs.tail;
                        ++i;
                    }
                }
                SymbolTable diffST = diffBlock.symbolTable;
                SymbolTable origST = diffST.getExistingOrig(rootAssociationsST);
                blockDifferentiator().differentiateDeclarationsOfBlock(
                        diffBlock, curDiffUnitSort() == DiffConstants.TANGENT ? DiffConstants.TANGENT_MODE : DiffConstants.ADJOINT_MODE,
                        sourceUnit, origST, diffST, sourceUnit.language(), false);
                if (adEnv.traceCurDifferentiation) {
                    TapEnv.printlnOnTrace("  Block contents after differentiation:");
                    TapList<Instruction> diffInstrs = diffBlock.instructions;
                    int i = 1;
                    while (diffInstrs != null) {
                        System.out.println("       " + i + " :: " + diffInstrs.head);
                        diffInstrs = diffInstrs.tail;
                        ++i;
                    }
                }
            }
            solveNewSymbolHoldersOfUnit(curDiffUnit, adEnv.emptyCopiedUnitSuffix);
            TapEnv.printlnOnTrace(15, ", time " + unitDiffTimer.elapsed() + " s");
        } else {
            TapEnv.printlnOnTrace(15, "@@ " + (sourceUnit.isClass() ? "Class " : sourceUnit.isModule() ? "Module " : "File ") + sourceUnit.name() + " not differentiated");
            diffCallGraph.deleteUnit(curDiffUnit);
        }
    }

    /**
     * Differentiate an INTERFACE Unit.
     * In reverse, the mode cannot be ADJOINT_SPLIT_MODE.
     */
    private void differentiateInterface() {
        String unitName = curUnit.name();
        TapEnv.printlnOnTrace(15, "@@ Differentiation of interface: " + unitName);
        Tree origCallTree = curUnit.headTree();
        for (int mode = TapEnv.MAX_DIFF_SORTS-1; mode > 0; --mode) {
            int dSort = mode == DiffConstants.TANGENT ? DiffConstants.TANGENT : DiffConstants.ADJOINT;
            adEnv.setCurDiffSorts(dSort, dSort);
            for (TapList<ActivityPattern> activityPatterns = curUnit.activityPatterns;
                 activityPatterns != null; activityPatterns = activityPatterns.tail) {
                ActivityPattern pattern = activityPatterns.head;
                setCurDiffUnit(adEnv.getDiffOfUnit(curUnit, pattern, mode));
                if (curDiffUnit != null) {
                    curDiffUnit.setInterface();
                    flowGraphDifferentiator().initializeMultiDirMode();
                    SymbolTable diffPublicSymbolTable = curDiffUnit.publicSymbolTable();
                    Tree diffCallTree =
                            procedureCallDifferentiator().differentiateProcedureHeader(origCallTree, null, null, null,
                                    curDiffUnit.publicSymbolTable(), curUnit.publicSymbolTable(), mode, null);
                    Instruction diffInstruction = new Instruction(diffCallTree);
                    diffInstruction.setPosition(curUnit.entryBlock().headInstr());
                    curDiffUnit.entryBlock().addInstrTl(diffInstruction);
                    curDiffUnit.parametersOrModuleNameTree = ILUtils.getArguments(diffCallTree);
//                     flowGraphDifferentiator().diffMemoryMap(curUnit, curDiffUnit);
                    if (mode == DiffConstants.ADJOINT && curUnit.isAFunction() && !curUnit.isAContext()) {
                        // en mode inverse il n'y a plus de fonction: si F est une fonction,
                        // Fbar sera une subroutine
                        curDiffUnit.functionTypeSpec().returnType = new WrapperTypeSpec(new VoidTypeSpec());
                    }
                    if (!multiDirMode()) {
                        VariableDecl returnVarNameDecl =
                                diffPublicSymbolTable.getTopVariableDecl(curUnit.name());
                        if (returnVarNameDecl == null) {
                            if (curUnit.otherReturnVar() != null) {
                                String otherReturnVarName = curUnit.otherReturnVar().symbol;
                                returnVarNameDecl =
                                        diffPublicSymbolTable.getTopVariableDecl(otherReturnVarName);
                                flowGraphDifferentiator().declareDiffIsReturnVar(returnVarNameDecl, curUnit, curDiffUnit);
                            }
                        }
                    }
                    Block diffBlock0 = diffPublicSymbolTable.declarationsBlock;
                    diffBlock0.rank = 0;
                    curDiffUnit.addBlock(diffBlock0);
                    Block tmpBlock;
                    // Copy the original Block0 declarations into diffBlock0:
                    tmpBlock = new BasicBlock(null, null, null);
                    tmpBlock.copyInstructions(curUnit.allBlocks().head, adEnv.copiedIncludes);
                    diffBlock0.addInstrDeclTl(tmpBlock.instructions);
                    // Copy the original additional declarations, if any, into diffBlock0:
                    if (curUnit.publicSymbolTable().declarationsBlock.instructions != null) {
                        tmpBlock = new BasicBlock(null, null, null);
                        tmpBlock.copyInstructions(curUnit.publicSymbolTable().declarationsBlock, adEnv.copiedIncludes);
                        diffBlock0.addInstrDeclTl(tmpBlock.instructions);
                    }
                    // Actually differentiate the contents of diffBlock0:
                    blockDifferentiator().differentiateDeclarationsOfBlock(diffBlock0,
                            curDiffUnitSort() == DiffConstants.TANGENT ? DiffConstants.TANGENT_MODE : DiffConstants.ADJOINT_MODE,
                            curUnit, curUnit.publicSymbolTable(),
                            curDiffUnit.privateSymbolTable(), curUnit.language(), false);
                    new FGArrow(curDiffUnit.entryBlock(), FGConstants.NO_TEST, null, curDiffUnit.allBlocks().head, false);
                    //solveNewSymbolHoldersOfUnit(curDiffUnit, adEnv.emptyCopiedUnitSuffix) ;
                }
            }
        }
    }

    protected void collectDiffSubUnits(Unit srcSubUnit, int differentiationMode,
                                       TapList<Unit> toDiffSubUnitsFwd, TapList<Unit> toDiffSubUnitsBwd) {
        // If this srcUnit is differentiated as a context, there is only a forward sweep:
        if (adEnv.curUnitIsContext) {
            toDiffSubUnitsBwd = toDiffSubUnitsFwd;
        }
        UnitDiffInfo subUnitDiffInfo = getUnitDiffInfo(srcSubUnit);
        Unit copySubUnit = unitsHavePrimal.retrieve(srcSubUnit) == Boolean.TRUE ? adEnv.getPrimalCopyOfOrigUnit(srcSubUnit) : null;
        if (differentiationMode == DiffConstants.TANGENT_MODE) {
            toDiffSubUnitsFwd.tail = TapList.union(toDiffSubUnitsFwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.TANGENT));
            if (copySubUnit != null) {
                toDiffSubUnitsFwd.tail = TapList.addLast(toDiffSubUnitsFwd.tail, copySubUnit);
            }
        } else if (differentiationMode == DiffConstants.ADJOINT_MODE) {
            toDiffSubUnitsBwd.tail = TapList.union(toDiffSubUnitsBwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT));
            toDiffSubUnitsBwd.tail = TapList.union(toDiffSubUnitsBwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT_FWD));
            toDiffSubUnitsBwd.tail = TapList.union(toDiffSubUnitsBwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT_BWD));
            if (copySubUnit != null) {
                toDiffSubUnitsBwd.tail = TapList.addLast(toDiffSubUnitsBwd.tail, copySubUnit);
            }
        } else if (differentiationMode == DiffConstants.ADJOINT_SPLIT_MODE) {
            toDiffSubUnitsFwd.tail = TapList.union(toDiffSubUnitsFwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT_FWD));
            if (copySubUnit != null) {
                toDiffSubUnitsFwd.tail = TapList.addLast(toDiffSubUnitsFwd.tail, copySubUnit);
            }
            toDiffSubUnitsBwd.tail = TapList.union(toDiffSubUnitsBwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT));
            toDiffSubUnitsBwd.tail = TapList.union(toDiffSubUnitsBwd.tail, subUnitDiffInfo.getAllDiffs(DiffConstants.ADJOINT_BWD));
        }
    }

    /**
     * Prepare the NewSymbolHolder that contains variable "NBDirsMax"
     * for the root SymbolTable context.
     */
    private void initializeRootMultiDirNumberMax(SymbolTable diffRootST) {
        // TODO: try refactor with FlowGraphDifferentiator.initializeMultiDirNumberMax()
        int arrayDimMin = TapEnv.relatedLanguageIsC() ? 0 : 1;
        if (TapEnv.get().fixedNbdirsmaxString != null) {
            varRefDifferentiator().multiDirDimensionMax =
                    new ArrayDim(ILUtils.buildDimColon(arrayDimMin,
                            ILUtils.build(ILLang.op_ident, TapEnv.get().fixedNbdirsmaxString)),
                            null, null, null, -1, 0, 0);
        } else if (rootDirNumberMaxSymbolHolder == null) {
            rootDirNumberMaxSymbolHolder = new NewSymbolHolder("NBDirsMax");
            rootDirNumberMaxSymbolHolder.setAsConstantVariable(adEnv.integerTypeSpec, null,
                    ILUtils.build(ILLang.op_ident, "TO_BE_DEFINED"));
            rootDirNumberMaxSymbolHolder.declarationLevelMustInclude(diffRootST);
            rootDirNumberMaxSymbolHolder.declare = false;
            varRefDifferentiator().multiDirDimensionMax =
                    new ArrayDim(ILUtils.buildDimColon(arrayDimMin,
                            rootDirNumberMaxSymbolHolder.makeNewRef(diffRootST)),
                            null, null, null, -1, 0, 0);
        }
    }

    protected static void declareDiffSymbolTableIfNew(Unit diffUnit,
                                                      SymbolTable diffSymbolTable) {
        if (!TapList.contains(diffUnit.symbolTablesBottomUp(), diffSymbolTable)) {
            diffUnit.addDerivedSymbolTable(diffSymbolTable);
        }
    }

    private void updateAllUsages(Unit unit, String oldUnitName, Unit diffUnit) {
        TapList<Block> blocks = unit.allBlocks();
        Block block;
        Instruction instruction;
        String diffFuncName = diffUnit.name();
        if (!unit.sameLanguage(diffUnit.language())) {
            if (diffCallGraph.getOtherLangFunctionDeclFromBindC(oldUnitName, TapEnv.FORTRAN) == null) {
                diffFuncName = diffCallGraph.getMixedLanguageFunctionName(diffFuncName,
                        diffUnit.language(), unit.language()).first;
                oldUnitName = diffCallGraph.getMixedLanguageFunctionName(oldUnitName,
                        diffUnit.language(), unit.language()).first;
            }
        }

        if (unit.entryBlock() != null && unit.entryBlock().instructions != null) {
            instruction = unit.entryBlock().instructions.head;
            ILUtils.replaceAllIdentsNamed(ILUtils.getArguments(instruction.tree), oldUnitName, diffFuncName);
        }
        while (blocks != null) {
            block = blocks.head;
            updateAllUsagesInBlock(block, oldUnitName, diffUnit, diffFuncName);
            blocks = blocks.tail;
        }
        // on peut avoir a la fois functionDecl et interfaceDecl avec le meme nom: NON TODO A MODIFIER
        SymbolTable funcInPrivateOrPublicST = unit.bodySymbolTable();
        FunctionDecl funDecl = null ;
        if (funcInPrivateOrPublicST!=null) {
            TapList<FunctionDecl> funDecls = funcInPrivateOrPublicST.getTopFunctionDecl(oldUnitName, null, null, false);
            funDecl = (funDecls == null ? null : funDecls.head) ;
        }
        if (funDecl == null && unit.publicSymbolTable()!=null) {
            funcInPrivateOrPublicST = unit.publicSymbolTable();
            TapList<FunctionDecl> funDecls = funcInPrivateOrPublicST.getTopFunctionDecl(oldUnitName, null, null, false);
            funDecl = funDecls == null ? null : funDecls.head;
        }

        SymbolTable intfInPrivateOrPublicST = unit.bodySymbolTable();
        InterfaceDecl intfDecl = null ;
        if (intfInPrivateOrPublicST!=null) {
            intfDecl = intfInPrivateOrPublicST.getTopInterfaceDecl(oldUnitName);
        }
        if (intfDecl == null && unit.publicSymbolTable()!=null) {
            intfInPrivateOrPublicST = unit.publicSymbolTable();
            intfDecl = intfInPrivateOrPublicST.getTopInterfaceDecl(oldUnitName);
        }

        Tree treeName = ILUtils.build(ILLang.op_ident, diffFuncName);
        FunctionDecl newFunDecl = new FunctionDecl(treeName, diffUnit);
        if (funDecl != null) {
            if (funDecl.unit().enclosingUnitOfInterface != null) {
                newFunDecl.unit().enclosingUnitOfInterface = funDecl.unit().enclosingUnitOfInterface;
            }
            TapList<String> extraInfo = funDecl.extraInfo();
            newFunDecl.setInstruction(funDecl);
            // Don't remove (or do it later) because the oldUnitName may be searched by successive ILUtils.replaceAllCallName():
            //funcInPrivateOrPublicST.removeDecl(oldUnitName, funDecl.kind, true);
            funDecl.isConsideredRemoved = true;
            funcInPrivateOrPublicST.addSymbolDecl(newFunDecl);
            newFunDecl.setExtraInfo(extraInfo);
        }
        if (intfDecl != null) {
            InterfaceDecl newIntfDecl = intfInPrivateOrPublicST.getTopInterfaceDecl(diffUnit.name());
            if (newIntfDecl != null) {
                intfInPrivateOrPublicST.removeDecl(oldUnitName, intfDecl.kind, true);
            }
        }
    }

    private void updateAllUsagesInBlock(Block block, String oldUnitName, Unit diffUnit,
                                        String diffUnitCallName) {
        TapList<Instruction> instructions;
        Instruction instruction;
        instructions = block.instructions;
        while (instructions != null) {
            instruction = instructions.head;
            if (instruction.tree.opCode() == ILLang.op_useDecl) {
                ILUtils.replaceAllIdentsNamed(instruction.tree.down(1), oldUnitName, diffUnitCallName);
            } else if (instruction.tree.opCode() == ILLang.op_varDeclaration) {
                ILUtils.replaceAllIdentsNamed(instruction.tree.down(3), oldUnitName, diffUnitCallName);
            } else if (instruction.tree.opCode() == ILLang.op_external) {
                // This might occur only in the bizarre case where user's code
                // declares a user subroutine as an EXTERNAL !
                ILUtils.replaceAllIdentsNamed(instruction.tree, oldUnitName, diffUnitCallName);
            } else if (instruction.tree.opCode() == ILLang.op_interfaceDecl) {
                Tree interfaces = instruction.tree.down(2);
                Tree oneInterface;
                for (int i = 1; i <= interfaces.length(); i++) {
                    oneInterface = interfaces.down(i).down(1);
                    if (oneInterface.opCode() == ILLang.op_function) {
                        if (ILUtils.getIdentString(oneInterface.down(4)).equals(oldUnitName)) {
                            oneInterface.down(4).setValue(diffUnitCallName);
                            if (diffUnit.isAFunction()) {
                                oneInterface.down(4).setAnnotation("explicitReturnVar",
                                        ILUtils.build(ILLang.op_ident, oldUnitName));
                            }
                        } else {
                            // This mechanism to avoid replacing foo by foo_nodiff inside
                            //  INTERFACES that define derivatives of foo or foo_nodiff itself,
                            //  because inside those foo_d,foo_b,etc foo is not the function
                            //  but the return variable. cf set07/v454.
                            // This mechanism is approximate because foo_b might *call* foo.
                            // This is an unfortunate consequence of the design choice to replace
                            // names blindly through the update*() that call replaceAllIdentsNamed().
                            Unit sourceUnit = oneInterface.down(4).getAnnotation("sourceUnit");
                            if (!(sourceUnit != null && oldUnitName.equals(sourceUnit.name()))
                                    && !ILUtils.getIdentString(oneInterface.down(4)).equals(diffUnitCallName)
                            ) {
                                ILUtils.replaceAllIdentsNamed(oneInterface, oldUnitName, diffUnitCallName);
                            }
                        }
                    } else {
                        ILUtils.replaceAllIdentsNamed(oneInterface, oldUnitName, diffUnitCallName);
                    }
                }
            }
            ILUtils.replaceAllCallName(instruction.tree, oldUnitName, diffUnitCallName,
                    null, block.symbolTable);
            // If this is a copy of a source instruction, the original source instruction will be reused. Replace in it too:
            Tree sourceTree = instruction.tree.getAnnotation("sourcetree");
            if (sourceTree != null) {
                ILUtils.replaceAllCallName(sourceTree, oldUnitName, diffUnitCallName,
                        null, block.symbolTable);
            }
            instructions = instructions.tail;
        }
    }

    /**
     * Inside diffUnit, replace all occurrences of "oldUnitName" with (a copy of)
     * the given "diffFuncResultName", which is the name of the diff function
     * and therefore (Fortran style) the name of the variable holding the return value.
     */
    protected void updateAllUsagesDiffFuncResultName(Unit unit, String oldUnitName,
                                                     Unit diffUnit, Tree diffFuncResultName) {
        String diffFuncName = diffUnit.name();
        if (!unit.sameLanguage(diffUnit.language())) {
            if (diffCallGraph.getOtherLangFunctionDeclFromBindC(oldUnitName, TapEnv.FORTRAN) == null) {
                diffFuncName = diffCallGraph.getMixedLanguageFunctionName(diffFuncName,
                        diffUnit.language(), unit.language()).first;
                oldUnitName = diffCallGraph.getMixedLanguageFunctionName(oldUnitName,
                        diffUnit.language(), unit.language()).first;
            }
        }
        for (TapList<Block> diffBlocks = diffUnit.allBlocks();
             diffBlocks != null;
             diffBlocks = diffBlocks.tail) {
            for (TapList<Instruction> diffInstrs = diffBlocks.head.instructions;
                 diffInstrs != null;
                 diffInstrs = diffInstrs.tail) {
                ILUtils.replaceAllCallName(diffInstrs.head.tree, oldUnitName, diffFuncName,
                        diffFuncResultName, diffBlocks.head.symbolTable);
            }
        }
    }

    private void updateAllAccessDecls(TapList<Instruction> instructions, String oldUnitName, Unit
            diffUnit) {
        Instruction instr;
        while (instructions != null) {
            instr = instructions.head;
            if (instr.tree.opCode() == ILLang.op_accessDecl) {
                ILUtils.replaceAllIdentsNamed(instr.tree.down(2), oldUnitName, diffUnit.name());
            }
            instructions = instructions.tail;
        }
    }

    /**
     * Replaces the typeDecl.importedFrom.first (holding the original TypeDecl before importation)
     * of differentiated imported types.
     * This prevents declare the diff of the original type at the location which imports it.
     */
    private void updateRenamedTypeDecl(Unit diffUnit) {
        if (diffUnit.privateSymbolTable() != null) {
            TapList<Instruction> useDecls = diffUnit.privateSymbolTable().declarationsBlock.instructions;
            Tree useDeclTree;
            Tree infoTree;
            SymbolDecl symbolDecl;
            while (useDecls != null && useDecls.head.tree.opCode() == ILLang.op_useDecl) {
                useDeclTree = useDecls.head.tree;
                infoTree = useDeclTree.down(2);
                Tree[] onlyVisibleTrees = infoTree.children();
                Tree onlyVisibleTree;
                for (Tree visibleTree : onlyVisibleTrees) {
                    onlyVisibleTree = visibleTree;
                    if (onlyVisibleTree.opCode() == ILLang.op_renamed) {
                        symbolDecl =
                                diffUnit.publicSymbolTable().getSymbolDecl(ILUtils.baseName(onlyVisibleTree.down(1)));
                        if (symbolDecl != null && symbolDecl.isA(SymbolTableConstants.TYPE)) {
                            NewSymbolHolder sHolder = symbolDecl.getDiffSymbolHolder(0, null, 0);
                            if (sHolder != null) {
                                TypeDecl fromTypeDecl = (TypeDecl) symbolDecl.importedFrom.first;
                                TypeDecl diffTypeDecl = sHolder.newTypeDecl();
                                if (diffTypeDecl.importedFrom==null) diffTypeDecl.importedFrom = new TapPair<>(null, null) ;
                                diffTypeDecl.importedFrom.first = fromTypeDecl.getDiffSymbolHolder(0, null, 0).newTypeDecl();
                            }
                        }
                    }
                }
                useDecls = useDecls.tail;
            }
        }
    }

    /**
     * Goes through all the declaration Instructions of the declarationsBlock of
     * copiedSymbolTable, which is either the public or the private SymbolTable of
     * a copied Unit. Each time a USE of a module is found, either as the
     * declaration or inside an interface declaration, replace the module
     * used with its differentiated counterpart.
     */
    private void updateModulesAndCopiedUnitsInCopiedUnit(SymbolTable copiedSymbolTable) {
        if (copiedSymbolTable != null && copiedSymbolTable.declarationsBlock != null) {
            TapList<Instruction> declarationInstructions =
                    copiedSymbolTable.declarationsBlock.instructions;
            Tree declarationTree;
            while (declarationInstructions != null) {
                declarationTree = declarationInstructions.head.tree;
                if (declarationTree != null) {
                    if (declarationTree.opCode() == ILLang.op_useDecl) {
                        String moduleName = ILUtils.getIdentString(declarationTree.down(1));
                        FunctionDecl moduleDecl = copiedSymbolTable.getModuleDecl(moduleName);
                        if (moduleDecl != null && moduleDecl.unit().isDiffPackage()) {
                            Unit diffModule = getUnitDiffInfo(moduleDecl.unit()).getDiff();
                            if (diffModule != null) {
                                declarationTree.setChild(ILUtils.build(ILLang.op_ident, diffModule.name()), 1);
                            }
                        }
                    } else if (declarationTree.opCode() == ILLang.op_interfaceDecl) {
                        blockDifferentiator().updateModulesAndCopiedUnitsInCopiedInterface(declarationTree,
                                copiedSymbolTable, DiffConstants.ORIGCOPY);
                    }
                }
                declarationInstructions = declarationInstructions.tail;
            }
        }
    }

    /**
     * Looks in a copied unit for references to variables that must be
     * replaced with references in the Association-by-Address style.
     */
    private void updateAAInCopiedUnits() {
        Unit origCopyUnit;
        TapList<Block> allOrigBlocks;
        TapList<Block> allCopyBlocks;
        for (int i = srcCallGraph.nbUnits() - 1; i >= 0; --i) {
            setCurUnitEtc(adEnv.srcCallGraph().sortedUnit(i));
            origCopyUnit = adEnv.copiedUnits.retrieve(curUnit);
            if (!curUnit.isExternal() && origCopyUnit != null) {
                TapEnv.printlnOnTrace(15, "@@ Update copied unit: " + curUnit);
                setCurDiffUnit(origCopyUnit);
                allOrigBlocks = curUnit.allBlocks();
                allCopyBlocks = origCopyUnit.allBlocks();
                while (allOrigBlocks != null) {
                    updateAAInCopiedBlock(allOrigBlocks.head, allCopyBlocks.head);
                    allOrigBlocks = allOrigBlocks.tail;
                    allCopyBlocks = allCopyBlocks.tail;
                }
            }
        }
    }

    private void updateAAInCopiedBlock(Block origBlock, Block copyBlock) {
        TapList<Instruction> instructions = copyBlock.instructions;
        Instruction instruction;
        int rank = 0;
        while (instructions != null) {
            instruction = instructions.head;
            adEnv.setCurInstruction(instruction);
            blockDifferentiator().updateAACopiedInstruction(instruction, origBlock, copyBlock, rank);
            blockDifferentiator().updateAAInstructionMask(instruction, origBlock.symbolTable, copyBlock.symbolTable);
            instructions = instructions.tail;
            rank = rank + 1;
        }
        adEnv.setCurInstruction(null);
    }

    private void placeDebugMidPointIfNone(Unit unit) {
        boolean hasDebugPoint = false;
        TapList<Block> allBlocks = unit.allBlocks();
        TapList<Instruction> instructions;
        while (!hasDebugPoint && allBlocks != null) {
            instructions = allBlocks.head.instructions;
            while (!hasDebugPoint && instructions != null) {
                hasDebugPoint = instructions.head.hasDirective(Directive.DEBUGHERE) != null;
                instructions = instructions.tail;
            }
            allBlocks = allBlocks.tail;
        }
        if (!hasDebugPoint) {
            Block middleBlock = unit.middleNormalBlock();
            if (middleBlock != null && middleBlock.instructions != null) {
                Instruction middleInstruction;
                middleInstruction = middleBlock.headInstr();
                Directive debugADHereDir = new Directive(Directive.DEBUGHERE);
                debugADHereDir.arguments = new Tree[3];
                debugADHereDir.arguments[0] = ILUtils.build(ILLang.op_ident, "middle");
                // It is better to default-initialize the trigger to false at "middle" location:
                debugADHereDir.arguments[1] = ILUtils.build(ILLang.op_ident, ".FALSE.");
                debugADHereDir.arguments[2] = ILUtils.build(ILLang.op_ident, ".FALSE.");
                middleInstruction.directives = new TapList<>(debugADHereDir, middleInstruction.directives);
            }
        }
    }
}
