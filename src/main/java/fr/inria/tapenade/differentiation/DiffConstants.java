/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

/**
 * Constants used during differentiation.
 */
public final class DiffConstants {

    /**
     * The possible values for the (stacked) contexts
     * in which variables must be differentiated.
     * May be INEXPR (default), INCALL, INHEADER
     */
    protected static final int INEXPR = 0;
    protected static final int INCALL = 1;
    protected static final int INHEADER = 2;

    /**
     * The different sorts of differentiated Units or Variables that we may create.
     * This integer is also the rank in the arrays of all such differentiated objects.
     * Reserve -1 for UNDEFINED.
     */
    protected static final int UNDEFSORT = -1;
    protected static final int ORIGCOPY = 0;
    protected static final int TANGENT = 1;
    protected static final int DIFFERENTIATED = 1;
    protected static final int ADJOINT = 2;
    protected static final int ADJOINT_FWD = 3;
    protected static final int ADJOINT_BWD = 4;

    /**
     * The index where we store an alternate diff name of a given variable.
     * For instance for v, the std diff var may be vb, and me may need another diff var vb0
     * for instance for storing a previous value of vb...
     */
    protected static final int ALT_DIFF_VAR = 3;
    protected static final int COPY_DIFF_VAR = 4;

    /**
     * Ranks in suffixes array.
     */
    protected static final int VAR = 0;
    protected static final int PROC = 1;
    protected static final int MOD = 2;
    protected static final int TYPE = 3;

    //[llh 7/8/2013] maybe this is becoming subsumed by curDiffUnitSort ?...
    /**
     * The current mode of differentiation for the current unit.<br>
     * - in TANGENT_MODE we generate procedure FOO_D.<br>
     * - in ADJOINT_SPLIT_MODE we generate the two procedures FOO_FWD and FOO_BWD.<br>
     * - in ADJOINT_MODE we generate procedure FOO_B.<br>
     * ADJOINT_MODE also includes "CONTEXT" mode, where we only build
     * a context from which to call the differentiated code.
     */
    protected static final int TANGENT_MODE = 1;
    protected static final int ADJOINT_MODE = -1;
    protected static final int ADJOINT_SPLIT_MODE = -2;

    /**
     * Constants for the "kind" of differentiated statement.
     */
    protected static final int INCREMENT_VARIABLE = 2;
    protected static final int SET_VARIABLE = 3;
    protected static final int PLAIN = 4;

    /**
     * Constants for the "kind" of FlowGraphLevel.
     */
    protected static final int PLAIN_BLOCK = 0;
    protected static final int ENTRY_BLOCK = 1;
    protected static final int TOP_LEVEL = 2;
    protected static final int PLAIN_GROUP = 3;
    protected static final int CHECKPOINT = 4;
    protected static final int WILD_LOOP = 5;
    protected static final int NATURAL_LOOP = 6;
    protected static final int BINOMIAL_LOOP = 7;
    protected static final int TIMES_LOOP = 8;
    protected static final int WHILE_LOOP = 9;
    protected static final int DO_LOOP = 10;
    protected static final int II_LOOP = 11;
    protected static final int NODIFF = 12;
    protected static final int FIXEDPOINT_LOOP = 13;
    protected static final int MULTITHREAD_REGION = 14;
    protected static final int LABELLED_REGION = 15;

    private DiffConstants() {
    }
}
