/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.differentiation;

import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.NewSymbolHolder;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.WrapperTypeSpec;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

public final class SplitForSave {
    /**
     * Global environment for differentiation.
     */
    private final DifferentiationEnv adEnv;

    protected Tree tree;
    private boolean wasImplicitArray;
    private final SymbolTable origSymbolTable;
    private final TapIntList toNextIndexRank;
    private final TapList<TapPair<Tree, NewSymbolHolder>> toSpecialSubExpressions;
    private final TapList<NewSymbolHolder> toIndexSymbolHolders;
    private final BoolVector zonesWrittenByThisCall;
    private TapList<TapPair<Tree, NewSymbolHolder>> indexesForTree;
    private TapList<Tree> toTreesToRestore;
    private TapList<Tree> toTreesToSplitValue;

    protected SplitForSave(SymbolTable origSymbolTable, TapIntList toNextIndexRank,
                           TapList<TapPair<Tree, NewSymbolHolder>> toSpecialSubExpressions,
                           TapList<NewSymbolHolder> toIndexSymbolHolders,
                           BoolVector zonesWrittenByThisCall,
                           BlockDifferentiator blockDifferentiator) {
        super();
        this.origSymbolTable = origSymbolTable;
        this.toNextIndexRank = toNextIndexRank;
        this.toSpecialSubExpressions = toSpecialSubExpressions;
        this.toIndexSymbolHolders = toIndexSymbolHolders;
        this.zonesWrittenByThisCall = zonesWrittenByThisCall;
        this.adEnv = blockDifferentiator.adEnv;
    }

    protected void initIndexes(Tree tree, boolean wasImplicitArray, DifferentiationEnv adEnv) {
        this.tree = tree;
        this.wasImplicitArray = wasImplicitArray;
        if (tree != null) {
            toTreesToRestore = new TapList<>(null, null);
            toTreesToSplitValue = new TapList<>(null, null);
            analyzePushPopOfExpression(tree, true, true, true);
            toTreesToRestore = toTreesToRestore.tail;
            while (toTreesToRestore != null) {
                Tree treeToRestore = toTreesToRestore.head;
                addToBeRestored(treeToRestore);
                toTreesToRestore = toTreesToRestore.tail;
            }
            toTreesToSplitValue = toTreesToSplitValue.tail;
            while (toTreesToSplitValue != null) {
                Tree treeToSplitValue = toTreesToSplitValue.head;
                NewSymbolHolder indexSymbolHolder = allocateAnIndexVarForSubTree(treeToSplitValue, adEnv);
                indexesForTree = new TapList<>(new TapPair<>(treeToSplitValue, indexSymbolHolder),
                        indexesForTree);
                toTreesToSplitValue = toTreesToSplitValue.tail;
            }
        }
    }

    private void addToBeRestored(Tree treeToRestore) {
        TapList<TapPair<Tree, NewSymbolHolder>> inSpecialSubExpressions = toSpecialSubExpressions;
        while (inSpecialSubExpressions.tail != null &&
                !inSpecialSubExpressions.tail.head.first.
                        equalsTree(treeToRestore)) {
            inSpecialSubExpressions = inSpecialSubExpressions.tail;
        }
        if (inSpecialSubExpressions.tail == null) {
            /*if the same index sub-expr was already marked to be restored, do nothing */
            inSpecialSubExpressions.tail =
                    new TapList<>(new TapPair<>(treeToRestore, null), null);
        }
    }

    private NewSymbolHolder allocateAnIndexVarForSubTree(Tree treeToSplitValue, DifferentiationEnv adEnv) {
        /*"treeToSplitValue" is the index sub-expression we want to split and save. */
        TapList<TapPair<Tree, NewSymbolHolder>> inSpecialSubExpressions = toSpecialSubExpressions;
        /*find if there is already the same index sub-expression saved: */
        while (inSpecialSubExpressions.tail != null &&
                !inSpecialSubExpressions.tail.head.first.
                        equalsTree(treeToSplitValue)) {
            inSpecialSubExpressions = inSpecialSubExpressions.tail;
        }
        NewSymbolHolder indexSymbolHolder;
        if (inSpecialSubExpressions.tail != null)
            /*if the same index sub-expr was already saved,
             * keep the same save index var.*/ {
            indexSymbolHolder =
                    inSpecialSubExpressions.tail.head.second;
        } else {
            /*otherwise create a new save index var,
             * named (if possible) "index"+(lastIndex+1) */
            toNextIndexRank.head = toNextIndexRank.head + 1;
            /*find if there is already a created index variable
             * named "index"+(lastIndex) */
            TapList<NewSymbolHolder> inIndexSymbolHolders = toIndexSymbolHolders;
            int times = toNextIndexRank.head - 1;
            while (inIndexSymbolHolders.tail != null && times > 0) {
                times--;
                inIndexSymbolHolders = inIndexSymbolHolders.tail;
            }
            /*if it is not created yet, create it and insert it */
            if (inIndexSymbolHolders.tail == null) {
                indexSymbolHolder =
                        new NewSymbolHolder("index" + toNextIndexRank.head);
                indexSymbolHolder.setAsVariable(adEnv.integerTypeSpec, null);
                indexSymbolHolder.zone =
                        adEnv.allocateTmpZone();
                inIndexSymbolHolders.placdl(indexSymbolHolder);
            } else /*declare the new NewSymbolHolder for declaration at final stage */ {
                indexSymbolHolder = inIndexSymbolHolders.tail.head;
            }
            /* add new index var into the list of all index vars to save */
            inSpecialSubExpressions.tail =
                    new TapList<>(new TapPair<>(treeToSplitValue, indexSymbolHolder), null);
        }
        return indexSymbolHolder;
    }

    /**
     * Analyzes recursively the expression "tree" to find out how it can be
     * PUSH/POP'ed, what sub-indices inside "tree" can/must be precomputed
     * in order to keep their value only, and what sub-expressions
     * must be PUSH/POP'ed recursively.
     * <p>
     * Example: To PUSH/POP {@code A(i,T(j))%B(x*y*z,e1:e2)}, which is an array
     * expression {@code x*y*z} can/must be precomputed and saved. If T(j)
     * is not precomputed-and-saved, then this requires that T(j) gets
     * PUSH/POP'ed too. e1:e2 cannot be precomputed and stored,
     * and therefore in total variables i,j,T(j),e1,e2 must be PUSH/POP'ed
     * except if we can make sure they are not modified in the sequel.
     *
     * @param rootArg   true iff the current expression is the complete argument tree.
     *                  becomes false as soon as we recurse down.
     * @param usedAsRef true iff the current expression is used by reference,
     *                  i.e. the memory location is important, not only the value !
     * @param allUsed   true iff the current expression is completely needed,
     *                  not only some index or field of it: i.e. its value must be preserved.
     * @return cost of evaluating the expression.
     */
    private int analyzePushPopOfExpression(Tree tree, boolean usedAsRef, boolean allUsed, boolean rootArg) {
        int cost;
        TapList<Tree> needRestoreBefore = toTreesToRestore.tail;
        TapList<Tree> saveValueBefore = toTreesToSplitValue.tail;
        if (tree == null) {
            tree = ILUtils.build(ILLang.op_none);
        }
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess: {
                cost = analyzePushPopOfExpression(tree.down(1), usedAsRef, false, false);
                Tree[] indexes = tree.down(2).children();
                for (int i = indexes.length - 1; i >= 0; i--) {
                    int cost2 = analyzePushPopOfExpression(indexes[i], false, true, false);
                    cost = cost + cost2 + 1;
                }
                if (allUsed && !rootArg && possiblyChangedByThisCall(tree)) {
                    addTreeUnique(tree, toTreesToRestore);
                }
                break;
            }
            case ILLang.op_fieldAccess:
                cost = analyzePushPopOfExpression(tree.down(1), usedAsRef, false, false);
                if (allUsed && !rootArg && possiblyChangedByThisCall(tree)) {
                    addTreeUnique(tree, toTreesToRestore);
                }
                cost++;
                break;
            case ILLang.op_ident:
                if (allUsed && !rootArg && possiblyChangedByThisCall(tree)) {
                    addTreeUnique(tree, toTreesToRestore);
                }
                cost = 1;
                break;
            case ILLang.op_arrayTriplet: {
                cost = analyzePushPopOfExpression(tree.down(1), false, true, false);
                int cost2 = analyzePushPopOfExpression(tree.down(2), false, true, false);
                int cost3 = analyzePushPopOfExpression(tree.down(3), false, true, false);
                cost = cost + cost2 + cost3 + 3;
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power: {
                cost = analyzePushPopOfExpression(tree.down(1), false, true, false);
                int cost2 = analyzePushPopOfExpression(tree.down(2), false, true, false);
                cost = cost + cost2 + 1;
                break;
            }
            case ILLang.op_minus:
                cost = analyzePushPopOfExpression(tree.down(1), false, true, false);
                break;
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_none:
            default:
                cost = 0;
                break;
        }
        WrapperTypeSpec treeType = origSymbolTable.typeOf(tree);
        if (/* if this tree is not the entiere argument expression, and ... */
                !rootArg &&
                        /* this tree is not used as a reference, only its value counts, and ... */
                        !usedAsRef &&
                        /* this tree has a scalar value (array regions are too large), and ... */
                        treeType != null && treeType.getAllDimensions() == null &&
                        /* this tree is expensive to evaluate and requires many values that must be saved... */
                        cost + 3 * additionalElems(toTreesToRestore.tail, needRestoreBefore) + 5 * additionalElems(toTreesToSplitValue.tail, saveValueBefore) > 15) {
            /* then it's possible and better to split this tree and snapshot only its value. */
            toTreesToSplitValue.tail = new TapList<>(tree, saveValueBefore);
            toTreesToRestore.tail = needRestoreBefore;
            cost = 1;
        }
        return cost;
    }

    private boolean possiblyChangedByThisCall(Tree tree) {
        TapIntList valueZonesList =
                ZoneInfo.listAllZones(origSymbolTable.treeOfZonesOfValue(tree, null,
                        adEnv.curInstruction(), null), true);
        return DataFlowAnalyzer.intersectsZoneRks(zonesWrittenByThisCall, valueZonesList, null);
    }

    private void addTreeUnique(Tree newTree, TapList<Tree> trees) {
        if (!TapList.containsTreeEquals(trees.tail, newTree)) {
            trees.tail = new TapList<>(newTree, trees.tail);
        }
    }

    private int additionalElems(TapList<Tree> list, TapList<Tree> refList) {
        int result = 0;
        while (list != null && list != refList) {
            result++;
            list = list.tail;
        }
        return result;
    }

    /**
     * When the original tree was explicited (from T(i) e.g. to T(i:i+6)).
     *
     * @return a splitcopy of the tree with the i:i+6 replaced back to i
     * This is useful in F77 which does not support array notation.
     */
    protected Tree newSplitNoColonCopy(SymbolTable usageSymbolTable) {
        Tree copyTree = newSplitCopy(usageSymbolTable);
        if (wasImplicitArray && copyTree.opCode() == ILLang.op_arrayAccess) {
            int rankOfArrayTriplet = 1;
            while (copyTree.down(2).down(rankOfArrayTriplet).operator().
                    code != ILLang.op_arrayTriplet) {
                rankOfArrayTriplet = rankOfArrayTriplet + 1;
            }
            copyTree.down(2).setChild(copyTree.down(2).down(rankOfArrayTriplet).cutChild(1), rankOfArrayTriplet);
        }
        return copyTree;
    }

    protected Tree newSplitCopy(SymbolTable usageSymbolTable) {
        TapList<TapPair<Tree, Tree>> hdCopied = new TapList<>(null, null);
        TapList<TapPair<Tree, Tree>> inCopied = hdCopied;
        TapList<TapPair<Tree, NewSymbolHolder>> inReplacedSubTrees = indexesForTree;
        TapPair<Tree, NewSymbolHolder> origPair;
        TapPair<Tree, Tree> copiedPair;
        Tree splitIndexTree;
        NewSymbolHolder symbolHolder;
        while (inReplacedSubTrees != null) {
            origPair = inReplacedSubTrees.head;
            symbolHolder = origPair.second;
            if (symbolHolder != null) {
                splitIndexTree = symbolHolder.makeNewRef(usageSymbolTable);
                symbolHolder.declarationLevelMustInclude(usageSymbolTable);
                copiedPair = new TapPair<>(origPair.first, splitIndexTree);
                inCopied = inCopied.placdl(copiedPair);
            }
            inReplacedSubTrees = inReplacedSubTrees.tail;
        }
        Tree copyTree =
                ILUtils.copyWatchingEqualsSubtrees(tree, hdCopied.tail);
        NewSymbolHolder.copySymbolHolderAnnotation(
                tree, copyTree, usageSymbolTable);
        return copyTree;
    }
}
