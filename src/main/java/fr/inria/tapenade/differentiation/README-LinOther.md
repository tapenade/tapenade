Template for new linearization class
------------------------------------

```
/**
 * Use it as a template for new classes.
 * A special node of a linearization tree,
 * standing for an operation that we don't quite know how to linearize...
 **/
private class LinOther extends LinTree {
    private Tree parentExp;
    private TapList<LinTree> children;

    protected LinOther(Tree parentExp, TapList<LinTree> children, LinTree parent, int locInParent) {
        super(parent, locInParent);
        this.parentExp = parentExp;
        this.children = children;
    }

    @Override
    protected LinTree addNewChild(LinTree newChild, int locInThisUnused) {
        children = TapList.addLast(children, newChild);
        return newChild;
    }

    @Override
    protected Tree buildTreeForward(TapPair<TapList<Tree>, TapList<Tree>> primRW,
                                    TapPair<TapList<Tree>, TapList<Tree>> diffRW) {
        primRW.first = ILUtils.usedVarsInExp(parentExp, primRW.first, true);
        Tree diffChildren = buildTreeForwardOnChildrenNodes(children, primRW, diffRW);
        TypeSpec diffChildrenType =
                (diffChildren == null ? null : diffChildren.getRemoveAnnotation("ExprType"));
        // This is a fallback:just copy the primal tree !
        Tree result = ILUtils.copy(parentExp);
        result.setChild(diffChildren, 2);
        result.setAnnotation("ExprType", "???");
        return result;
    }

    @Override
    protected void buildTreesBackwardOnNode(MaskTree mask, TapList<DiffAssignmentNode> toResult, TapList<Tree> primR) {
        primR = ILUtils.usedVarsInExp(parentExp, primR, true);
        buildTreesBackwardOnChildrenNodes(children, mask, toResult, primR);
    }

    @Override
    protected Tree buildTreeBackward(Tree adjPrefix, int locInThisUnused) {
        // This is a fallback.We don 't know what to do, so do nothing!
        Tree result = parent.buildTreeBackward(adjPrefix, locInParent);
        TypeSpec diffResultType =
                (result == null ? null : result.getRemoveAnnotation("ExprType"));
        result.setAnnotation("ExprType", ???);
        return result;
    }

    public String toString() {
        return "LinOther:" + ILUtils.toString(parentExp) + " * " + toStringOnChildrenNodes(children);
    }
}
```