/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * Tapenade graphical user interface.
 */
class InputLanguagePanel extends JPanel {
    private static final long serialVersionUID = 4367299994054L;
    private ButtonGroup inputLanguageButtons;
    private JRadioButton suffixButton;
    private JRadioButton fortran77Button;
    private JRadioButton fortran95Button;
    private JRadioButton cButton;

    protected InputLanguagePanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Select the input language :"),
                BorderFactory.createEmptyBorder(2, 2, 2, 2)));

        getInputLanguageButtons();
        add(getSuffixButton());
        add(getFortran77Button());
        add(getFortran95Button());
        add(getCButton());
    }

    protected ButtonGroup getInputLanguageButtons() {
        if (inputLanguageButtons == null) {
            inputLanguageButtons = new ButtonGroup();
            inputLanguageButtons.add(getSuffixButton());
            inputLanguageButtons.add(getFortran77Button());
            inputLanguageButtons.add(getFortran95Button());
        }
        return inputLanguageButtons;
    }

    private JRadioButton getSuffixButton() {
        if (suffixButton == null) {
            suffixButton = new JRadioButton("given by files extensions");
            TapenadeFrame.setFont(suffixButton);
            suffixButton.setOpaque(false);
            suffixButton.setName("suffix");
            suffixButton.setSelected(true);
        }
        return suffixButton;
    }

    private JRadioButton getFortran77Button() {
        if (fortran77Button == null) {
            fortran77Button = new JRadioButton("Fortran77");
            TapenadeFrame.setFont(fortran77Button);
            fortran77Button.setOpaque(false);
            fortran77Button.setName("fortran77");
        }
        return fortran77Button;
    }

    private JRadioButton getFortran95Button() {
        if (fortran95Button == null) {
            fortran95Button = new JRadioButton("Fortran95");
            TapenadeFrame.setFont(fortran95Button);
            fortran95Button.setOpaque(false);
            fortran95Button.setName("fortran95");
        }
        return fortran95Button;
    }

    private JRadioButton getCButton() {
        if (cButton == null) {
            cButton = new JRadioButton("C");
            TapenadeFrame.setFont(cButton);
            cButton.setOpaque(false);
            cButton.setName("C");
        }
        return cButton;
    }
}
