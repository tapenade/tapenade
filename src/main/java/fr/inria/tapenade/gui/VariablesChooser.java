/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * Tapenade graphical user interface.
 */
class VariablesChooser extends JDialog {

    private static final long serialVersionUID = 1234099994876L;
    private JList<String> varsList;
    private JScrollPane scroll;
    private JPanel buttonPane;
    private JButton okButton;
    private JButton cancelButton;
    private boolean ok;

    protected VariablesChooser(JFrame owner) {
        super(owner, "Add Variables", true);
        initialize();
    }

    private final void initialize() {
        setBackground(this);

        JPanel pane = new JPanel();
        pane.setLayout(new GridBagLayout());
        setBackground(pane);

        // add the scrollPane
        java.awt.GridBagConstraints constraintsScrollPane = new java.awt.GridBagConstraints();
        constraintsScrollPane.gridx = 0;
        constraintsScrollPane.gridy = 0;
        constraintsScrollPane.anchor = java.awt.GridBagConstraints.NORTH;
        constraintsScrollPane.fill = java.awt.GridBagConstraints.BOTH;
        constraintsScrollPane.insets = new java.awt.Insets(4, 4, 4, 4);
        pane.add(getJScrollPane(), constraintsScrollPane);

        // add the button pane
        java.awt.GridBagConstraints constraintsJButtonPane = new java.awt.GridBagConstraints();
        constraintsJButtonPane.gridx = 0;
        constraintsJButtonPane.gridy = 1;
        constraintsJButtonPane.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsJButtonPane.insets = new java.awt.Insets(4, 4, 4, 4);
        pane.add(getJButtonPane(), constraintsJButtonPane);

        this.setContentPane(pane);
        this.setSize(277, 300);

        int[] center = TapenadeFrame.center(this);
        this.setLocation(center[0], center[1]);
        this.addWindowListener(new WindowListener() {
            @Override
            public void windowActivated(WindowEvent e) {
            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }

            @Override
            public void windowClosing(WindowEvent e) {
            }

            @Override
            public void windowOpened(WindowEvent e) {
            }

            @Override
            public void windowClosed(WindowEvent e) {
                VariablesChooser.this.setVisible(false);
            }

            @Override
            public void windowIconified(WindowEvent e) {
            }

            @Override
            public void windowDeiconified(WindowEvent e) {
            }
        });
    }

    private void setBackground(Component c) {
        c.setBackground(new Color(204, 204, 255));
    }

    private JScrollPane getJScrollPane() {
        if (scroll == null) {
            scroll = new JScrollPane(getJList());
            scroll.setOpaque(false);
        }
        return scroll;
    }

    private JPanel getJButtonPane() {
        if (buttonPane == null) {
            buttonPane = new JPanel();
            buttonPane.setOpaque(false);
            FlowLayout buttonPaneLayout = new FlowLayout(FlowLayout.RIGHT);
            buttonPane.setLayout(buttonPaneLayout);
            buttonPane.add(getOkButton());
            buttonPane.add(getCancelButton());
        }
        return buttonPane;
    }

    private JList<String> getJList() {
        if (varsList == null) {
            varsList = new JList<>();
            varsList.setFont(new Font("dialog", Font.BOLD, 12));
            varsList.setModel(new DefaultListModel<>());
            varsList.setVisibleRowCount(8);
        }
        return varsList;
    }

    private JButton getOkButton() {
        if (okButton == null) {
            okButton = new JButton();
            okButton.setText("add Variables");
            okButton.setFont(new Font("dialog", Font.PLAIN, 11));
            okButton.setOpaque(false);
            okButton.addActionListener(e -> {
                VariablesChooser.this.setVisible(false);
                ok = true;
            });
        }
        return okButton;
    }

    private JButton getCancelButton() {
        if (cancelButton == null) {
            cancelButton = new JButton();
            cancelButton.setText("cancel");
            cancelButton.setFont(new Font("dialog", Font.PLAIN, 11));
            cancelButton.setOpaque(false);
            cancelButton.addActionListener(e -> VariablesChooser.this.setVisible(false));
        }
        return cancelButton;
    }

    protected void showDialog(Iterable<String> vars) {
        this.update(this.getGraphics());
        DefaultListModel<String> model = (DefaultListModel<String>) getJList().getModel();
        model.removeAllElements();
        for (String var : vars) {
            model.addElement(var);
        }
        this.setVisible(true);
    }

    protected Iterable<String> getVariables() {
        List vars = new ArrayList();
        if (ok) {
            Object[] values = getJList().getSelectedValuesList().toArray();
            Collections.addAll(vars, values);
            ok = false;
        }
        return vars;
    }
}
