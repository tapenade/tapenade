/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;

/**
 * Tapenade graphical user interface.
 */
final class ExtFilesPanel extends JPanel {
    private static final long serialVersionUID = 4448889994054L;
    protected static final int EXT = 0;
    protected static final int EXTAD = 1;
    private final int mode;
    private JList<File> filesList;
    private JScrollPane scrollFiles;
    private JButton addButton;
    private JButton removeButton;
    private JCheckBox noLib;
    private JSplitPane splitPanel;

    protected ExtFilesPanel(int mode) {
        super();
        this.mode = mode;
        setPreferredSize(new Dimension(230, 120));
        initialize();
    }

    private void initialize() {
        setOpaque(false);
        setLayout(new GridBagLayout());
        String title;
        if (mode == EXT) {
            title = "Ext Files :";
        } else {
            title = "Ext AD Files :";
        }
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(title),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        java.awt.GridBagConstraints constraintsSplitPanel = new java.awt.GridBagConstraints();
        constraintsSplitPanel.gridx = 0;
        constraintsSplitPanel.gridy = 0;
        constraintsSplitPanel.gridheight = 4;
        constraintsSplitPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsSplitPanel.weightx = 1.0;
        constraintsSplitPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getSplitPanel(), constraintsSplitPanel);
    }

    protected JList<File> getFilesList() {
        if (filesList == null) {
            filesList = new JList<>(new DefaultListModel<>());
            TapenadeFrame.setFont(getFilesList());
            filesList.setVisibleRowCount(3);
        }
        return filesList;
    }

    private JScrollPane getScrollFiles() {
        if (scrollFiles == null) {
            scrollFiles = new JScrollPane(getFilesList());
        }
        return scrollFiles;
    }

    private JButton getAddButton() {
        if (addButton == null) {
            addButton = new JButton();
            addButton.setText("add");
            TapenadeFrame.setFont(addButton);
            addButton.setOpaque(false);
            addButton.addActionListener(new ActionListener() {
                private final JFileChooser chooser = new JFileChooser(System.getProperty(
                        "user.dir"));

                @Override
                public void actionPerformed(ActionEvent e) {
                    chooser.setMultiSelectionEnabled(true);

                    int result = chooser.showDialog(TapenadeFrame.getInstance(),
                            "Add Files");

                    if (result == JFileChooser.APPROVE_OPTION) {
                        File[] files = chooser.getSelectedFiles();
                        DefaultListModel<File> model = (DefaultListModel<File>) filesList.getModel();

                        for (File file : files) {
                            boolean presence = false;
                            Enumeration<?> enumeration = model.elements();

                            while (enumeration.hasMoreElements()) {
                                if (enumeration.nextElement().toString().equals(file.toString())) {
                                    presence = true;
                                }
                            }

                            if (!presence) {
                                model.addElement(file);
                            }
                        }
                    }
                }
            });
        }
        return addButton;
    }

    private JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton = new JButton();
            removeButton.setText("remove");
            TapenadeFrame.setFont(removeButton);
            removeButton.setOpaque(false);
            removeButton.addActionListener(e -> {
                Object[] files = ExtFilesPanel.this.getFilesList()
                        .getSelectedValuesList()
                        .toArray();
                DefaultListModel<File> model;

                model = (DefaultListModel<File>) ExtFilesPanel.this.getFilesList()
                        .getModel();

                for (Object file : files) {
                    model.removeElement(file);
                }
            });
        }
        return removeButton;
    }

    protected JCheckBox getNoLibCheckBox() {
        if (noLib == null) {
            String title;
            if (mode != EXT) {
                title = "noADlib";
            } else {
                title = "nolib";
            }
            noLib = new JCheckBox(title);
            noLib.setOpaque(false);
            noLib.setSelected(false);
            TapenadeFrame.setFont(noLib);
            noLib.addActionListener(e -> {
                if (!noLib.isSelected()) {
                    filesList.setEnabled(true);
                    scrollFiles.setEnabled(true);
                    addButton.setEnabled(true);
                    removeButton.setEnabled(true);
                } else {
                    filesList.setEnabled(false);
                    scrollFiles.setEnabled(false);
                    addButton.setEnabled(false);
                    removeButton.setEnabled(false);
                }
            });
        }
        return noLib;
    }

    private JSplitPane getSplitPanel() {
        if (splitPanel == null) {
            splitPanel = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
            splitPanel.setOpaque(false);
            splitPanel.setBorder(null);
            splitPanel.setDividerSize(0);

            JPanel top = new JPanel();
            top.setOpaque(false);
            top.setLayout(new GridBagLayout());

            java.awt.GridBagConstraints constraintsAddButton = new java.awt.GridBagConstraints();
            constraintsAddButton.gridx = 1;
            constraintsAddButton.gridy = 0;
            constraintsAddButton.anchor = java.awt.GridBagConstraints.SOUTH;
            constraintsAddButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
            constraintsAddButton.insets = new java.awt.Insets(4, 4, 4, 4);
            top.add(getAddButton(), constraintsAddButton);

            java.awt.GridBagConstraints constraintsRemoveButton = new java.awt.GridBagConstraints();
            constraintsRemoveButton.gridx = 1;
            constraintsRemoveButton.gridy = 1;
            constraintsRemoveButton.anchor = java.awt.GridBagConstraints.NORTH;
            constraintsRemoveButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
            constraintsRemoveButton.insets = new java.awt.Insets(4, 4, 4, 4);
            top.add(getRemoveButton(), constraintsRemoveButton);

            java.awt.GridBagConstraints constraintsList = new java.awt.GridBagConstraints();
            constraintsList.gridx = 0;
            constraintsList.gridy = 0;
            constraintsList.gridheight = 3;
            constraintsList.fill = java.awt.GridBagConstraints.BOTH;
            constraintsList.weightx = 1.0;
            constraintsList.weighty = 1.0;
            constraintsList.insets = new java.awt.Insets(4, 4, 4, 4);
            top.add(getScrollFiles(), constraintsList);

            JPanel bottom = new JPanel();
            bottom.setOpaque(false);
            bottom.setLayout(new GridBagLayout());

            java.awt.GridBagConstraints constraintsNoLib = new java.awt.GridBagConstraints();
            constraintsNoLib.gridx = 0;
            constraintsNoLib.gridy = 0;
            constraintsNoLib.gridheight = 1;
            constraintsNoLib.fill = java.awt.GridBagConstraints.BOTH;
            constraintsNoLib.insets = new java.awt.Insets(4, 4, 4, 4);
            bottom.add(getNoLibCheckBox(), constraintsNoLib);

            splitPanel.setOrientation(JSplitPane.VERTICAL_SPLIT);
            splitPanel.setTopComponent(top);
            splitPanel.setBottomComponent(bottom);
        }
        return splitPanel;
    }
}
