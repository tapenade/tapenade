/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Enumeration;

/**
 * Tapenade graphical user interface.
 */
class FilesPanel extends JPanel {
    private static final long serialVersionUID = 42624299994054L;
    private JList<File> filesList;
    private DefaultListModel<File> model;
    private JScrollPane scrollFiles;
    private JButton addButton;
    private JButton removeButton;
    private JButton parseButton;

    protected FilesPanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Select Files :"),
                BorderFactory.createEmptyBorder(2, 2, 2, 2)));

        java.awt.GridBagConstraints constraintsAddButton = new java.awt.GridBagConstraints();
        constraintsAddButton.gridx = 1;
        constraintsAddButton.gridy = 0;

        constraintsAddButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsAddButton.insets = new Insets(4, 4, 4, 4);
        add(getAddButton(), constraintsAddButton);

        java.awt.GridBagConstraints constraintsRemoveButton = new java.awt.GridBagConstraints();
        constraintsRemoveButton.gridx = 1;
        constraintsRemoveButton.gridy = 1;

        constraintsRemoveButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsRemoveButton.insets = new Insets(4, 4, 4, 4);
        add(getRemoveButton(), constraintsRemoveButton);

        java.awt.GridBagConstraints constraintsValidateButton = new java.awt.GridBagConstraints();
        constraintsValidateButton.gridx = 1;
        constraintsValidateButton.gridy = 2;

        constraintsValidateButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsValidateButton.insets = new Insets(4, 4, 4, 4);
        add(getParseButton(), constraintsValidateButton);

        java.awt.GridBagConstraints constraintsList = new java.awt.GridBagConstraints();
        constraintsList.gridx = 0;
        constraintsList.gridy = 0;
        constraintsList.gridheight = 3;
        constraintsList.gridwidth = 1;

        constraintsList.fill = java.awt.GridBagConstraints.BOTH;
        constraintsList.insets = new Insets(4, 4, 4, 4);
        add(getScrollFiles(), constraintsList);
        desactivateOptions();
    }

    protected JList<File> getFilesList() {
        if (filesList == null) {
            model = new DefaultListModel<>();
            filesList = new JList<>(model);
            filesList.setVisibleRowCount(3);
            filesList.setPreferredSize(null);
            TapenadeFrame.setFont(filesList);
        }
        return filesList;
    }

    private JScrollPane getScrollFiles() {
        if (scrollFiles == null) {
            scrollFiles = new JScrollPane(getFilesList());
        }
        return scrollFiles;
    }

    private JButton getParseButton() {
        if (parseButton == null) {
            parseButton = new JButton();
            parseButton.setText("parse");
            TapenadeFrame.setFont(parseButton);
            parseButton.setOpaque(false);
            parseButton.setEnabled(false);
            parseButton.setMargin(new Insets(4, 4, 4, 4));
            parseButton.addActionListener(e -> {
                TapenadeFrame.getInstance().getTopUnitPanel()
                        .getUnits().removeAllItems();

                if (model.size() < 0) {
                    TapenadeFrame.getInstance().setTopUnitName("");
                    ((DefaultListModel) TapenadeFrame.getInstance()
                            .getInputVariablesPanel()
                            .getVariablesList()
                            .getModel()).removeAllElements();
                    ((DefaultListModel) TapenadeFrame.getInstance()
                            .getOutputVariablesPanel()
                            .getVariablesList()
                            .getModel()).removeAllElements();
                }

                TapenadeFrame.getInstance().getTopUnitPanel()
                        .setRecompute(true);
                TapenadeFrame.getInstance().preprocess();
                TapenadeFrame.getInstance().getTopUnitPanel()
                        .setRecompute(false);

                if (!model.isEmpty()) {
                    activateOptions();
                }

                TapenadeFrame.getInstance().pack();
            });
        }
        return parseButton;
    }

    private JButton getAddButton() {
        if (addButton == null) {
            addButton = new JButton();
            addButton.setText("add");
            TapenadeFrame.setFont(addButton);
            addButton.setOpaque(false);
            addButton.setMargin(new Insets(4, 4, 4, 4));
            addButton.addActionListener(new ActionListener() {
                private JFileChooser chooser;

                public ActionListener initialyse() {
                    chooser = new JFileChooser(System.getProperty(
                            "user.dir"));
                    chooser.setMultiSelectionEnabled(true);
                    TapenadeFrame.setFont(chooser);

                    // fortran77 File filter
                    TapFileFilter fortran77Filter = new TapFileFilter();
                    fortran77Filter.setDescription("Fortran77");
                    fortran77Filter.addExtension("f");
                    fortran77Filter.addExtension("fortran");
                    fortran77Filter.addExtension("for");

                    // fortran98 File filter
                    TapFileFilter fortran95Filter = new TapFileFilter();
                    fortran95Filter.setDescription("Fortran95");
                    fortran95Filter.addExtension("f95");
                    fortran95Filter.addExtension("f90");

                    // C File filter
                    TapFileFilter cFilter = new TapFileFilter();
                    cFilter.setDescription("C");
                    cFilter.addExtension("c");

                    // *.* File filter
                    TapFileFilter fortranCFilter = new TapFileFilter();
                    fortranCFilter.setDescription("*.*");
                    fortranCFilter.addExtension("f");
                    fortranCFilter.addExtension("fortran");
                    fortranCFilter.addExtension("for");
                    fortranCFilter.addExtension("f95");
                    fortranCFilter.addExtension("f90");
                    fortranCFilter.addExtension("c");

                    chooser.addChoosableFileFilter(fortran77Filter);
                    chooser.addChoosableFileFilter(fortran95Filter);
                    chooser.addChoosableFileFilter(cFilter);
                    chooser.setFileFilter(fortranCFilter);

                    return this;
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    int result = chooser.showDialog(
                            TapenadeFrame.getInstance(), "Add Files");

                    if (result == JFileChooser.APPROVE_OPTION) {
                        File[] files = chooser.getSelectedFiles();

                        for (File file : files) {
                            boolean presence = false;
                            Enumeration<File> enumeration = model.elements();

                            while (enumeration.hasMoreElements()) {
                                if (enumeration.nextElement().toString().equals(file.toString())) {
                                    presence = true;
                                }
                            }

                            if (!presence) {
                                model.addElement(file);
                            }
                        }

                        if (!model.isEmpty()) {
                            getParseButton().setEnabled(true);
                        }
                    }
                }
            }.initialyse());
        }
        return addButton;
    }

    private JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton = new JButton();
            removeButton.setText("remove");
            TapenadeFrame.setFont(removeButton);
            removeButton.setOpaque(false);
            addButton.setMargin(new Insets(4, 4, 4, 4));
            removeButton.addActionListener(e -> {
                for (int i = 0; i < model.getSize(); i++) {
                    model.removeElement(model.getElementAt(i));
                }

                if (model.getSize() == 0) {
                    getParseButton().setEnabled(false);
                }
            });
        }
        return removeButton;
    }

    private void activateOptions() {
        TapenadeFrame.getInstance().getTopUnitPanel().getUnits()
                .setEnabled(true);
        TapenadeFrame.getInstance().getInputVariablesPanel().getAddButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getInputVariablesPanel().getRemoveButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getOutputVariablesPanel().getAddButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getOutputVariablesPanel().getRemoveButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getAnalyseModePanel().getTangentModeButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getAnalyseModePanel().getReverseModeButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getAnalyseModePanel()
                .getTangentVectorModeButton().setEnabled(true);
        TapenadeFrame.getInstance().getAnalyseModePanel()
                .getReverseVectorModeButton().setEnabled(true);
        TapenadeFrame.getInstance().getControlPanel().getDifferentiateButton()
                .setEnabled(true);
        TapenadeFrame.getInstance().getControlPanel().getHtmlCheckBox()
                .setEnabled(true);
    }

    private void desactivateOptions() {
        TapenadeFrame.getInstance().getTopUnitPanel().getUnits()
                .setEnabled(false);
        TapenadeFrame.getInstance().getInputVariablesPanel().getAddButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getInputVariablesPanel().getRemoveButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getOutputVariablesPanel().getAddButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getOutputVariablesPanel().getRemoveButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getAnalyseModePanel().getTangentModeButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getAnalyseModePanel().getReverseModeButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getAnalyseModePanel()
                .getTangentVectorModeButton().setEnabled(false);
        TapenadeFrame.getInstance().getAnalyseModePanel()
                .getReverseVectorModeButton().setEnabled(false);
        TapenadeFrame.getInstance().getControlPanel().getDifferentiateButton()
                .setEnabled(false);
        TapenadeFrame.getInstance().getControlPanel().getHtmlCheckBox()
                .setEnabled(false);
    }
}
