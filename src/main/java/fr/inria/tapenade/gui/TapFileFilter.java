/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Tapenade graphical user interface.
 */
class TapFileFilter extends javax.swing.filechooser.FileFilter {
    private final List<String> extensions = new ArrayList<>();
    private String description = "";

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String descr) {
        description = descr;
    }

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = getExtension(f);

        for (String ext : extensions) {
            if (extension != null && extension.equals(ext)) {
                return true;
            }
        }
        return false;
    }

    private String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');

        if (i > 0 && i < s.length() - 1) {
            ext = s.substring(i + 1).toLowerCase();
        }

        return ext;
    }

    public void addExtension(String ext) {
        extensions.add(ext);
    }
}
