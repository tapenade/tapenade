/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * Tapenade graphical user interface.
 */
class OutputLanguagePanel extends JPanel {
    private static final long serialVersionUID = 1234099994054L;
    private ButtonGroup outputLanguageButtons;
    private JRadioButton fortranButton;
    private JRadioButton fortran90Button;
    private JRadioButton fortran95Button;
    private JRadioButton cButton;

    protected OutputLanguagePanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Output language :"),
                BorderFactory.createEmptyBorder(2, 2, 2, 2)));

        getOutputLanguageButtons();
        add(getFortranButton());
        add(getFortran90Button());
        add(getFortran95Button());
        add(getCButton());
    }

    protected ButtonGroup getOutputLanguageButtons() {
        if (outputLanguageButtons == null) {
            outputLanguageButtons = new ButtonGroup();
            outputLanguageButtons.add(getCButton());
            outputLanguageButtons.add(getFortran90Button());
            outputLanguageButtons.add(getFortran95Button());
            outputLanguageButtons.add(getFortranButton());
        }
        return outputLanguageButtons;
    }

    private JRadioButton getFortranButton() {
        if (fortranButton == null) {
            fortranButton = new JRadioButton("Fortran");
            TapenadeFrame.setFont(fortranButton);
            fortranButton.setOpaque(false);
            fortranButton.setName("fortran");
        }
        return fortranButton;
    }

    private JRadioButton getFortran90Button() {
        if (fortran90Button == null) {
            fortran90Button = new JRadioButton("Fortran90");
            TapenadeFrame.setFont(fortran90Button);
            fortran90Button.setOpaque(false);
            fortran90Button.setName("fortran90");
        }
        return fortran90Button;
    }

    private JRadioButton getFortran95Button() {
        if (fortran95Button == null) {
            fortran95Button = new JRadioButton("Fortran95");
            TapenadeFrame.setFont(fortran95Button);
            fortran95Button.setOpaque(false);
            fortran95Button.setName("fortran95");
        }
        return fortran95Button;
    }

    private JRadioButton getCButton() {
        if (cButton == null) {
            cButton = new JRadioButton("c");
            TapenadeFrame.setFont(cButton);
            cButton.setOpaque(false);
            cButton.setName("c");
        }
        return cButton;
    }
}
