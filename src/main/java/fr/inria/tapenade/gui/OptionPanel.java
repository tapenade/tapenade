/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

/**
 * Tapenade graphical user interface.
 */
class OptionPanel extends JPanel {
    private static final long serialVersionUID = 1243299994054L;
    private JCheckBox preprocessCheckBox;
    private JCheckBox viewCheckBox;
    private JSplitPane msgLevelPanel;
    private JCheckBox msgLevelCheckBox;
    private JComboBox<Integer> msgLevelComboBox;
    private JSplitPane dumpPanel;
    private JCheckBox dump;
    private JTextField dumpFile;

    protected OptionPanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setLayout(new GridBagLayout());
        setOpaque(false);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Options"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        GridBagConstraints constraintsPreprocessCheckBox = new GridBagConstraints();
        constraintsPreprocessCheckBox.gridx = 0;
        constraintsPreprocessCheckBox.gridy = 4;
        constraintsPreprocessCheckBox.fill = GridBagConstraints.HORIZONTAL;
        add(getPreprocessCheckBox(), constraintsPreprocessCheckBox);

        GridBagConstraints constraintsMsgLevelPanel = new GridBagConstraints();
        constraintsMsgLevelPanel.gridx = 0;
        constraintsMsgLevelPanel.gridy = 5;
        constraintsMsgLevelPanel.fill = GridBagConstraints.HORIZONTAL;
        add(getMsgLevelPanel(), constraintsMsgLevelPanel);

        GridBagConstraints constraintsViewCheckBox = new GridBagConstraints();
        constraintsViewCheckBox.gridx = 0;
        constraintsViewCheckBox.gridy = 6;
        constraintsViewCheckBox.fill = GridBagConstraints.HORIZONTAL;
        add(getViewCheckBox(), constraintsViewCheckBox);

        GridBagConstraints constraintsDump = new GridBagConstraints();
        constraintsDump.gridx = 0;
        constraintsDump.gridy = 7;
        constraintsDump.fill = GridBagConstraints.HORIZONTAL;
        add(getDumpPanel(), constraintsDump);
    }

    protected JCheckBox getPreprocessCheckBox() {
        if (preprocessCheckBox == null) {
            preprocessCheckBox = new JCheckBox("Preprocess");
            TapenadeFrame.setFont(preprocessCheckBox);
            preprocessCheckBox.setOpaque(false);
        }
        return preprocessCheckBox;
    }

    private JSplitPane getMsgLevelPanel() {
        if (msgLevelPanel == null) {
            msgLevelPanel = new JSplitPane();
            msgLevelPanel.setBorder(null);
            msgLevelPanel.setDividerSize(0);
            msgLevelPanel.setOpaque(false);
            msgLevelPanel.setLeftComponent(getMsgLevelCheckBox());
            msgLevelPanel.setRightComponent(getMsgLevelComboBox());
        }
        return msgLevelPanel;
    }

    protected JCheckBox getViewCheckBox() {
        if (viewCheckBox == null) {
            viewCheckBox = new JCheckBox("View");
            TapenadeFrame.setFont(viewCheckBox);
            viewCheckBox.setOpaque(false);
        }
        return viewCheckBox;
    }

    private JSplitPane getDumpPanel() {
        if (dumpPanel == null) {
            dumpPanel = new JSplitPane();
            dumpPanel.setBorder(null);
            dumpPanel.setDividerSize(0);
            dumpPanel.setOpaque(false);
            dumpPanel.setLeftComponent(getDump());
            dumpPanel.setRightComponent(getDumpFile());
        }
        return dumpPanel;
    }

    protected JCheckBox getMsgLevelCheckBox() {
        if (msgLevelCheckBox == null) {
            msgLevelCheckBox = new JCheckBox("MsgLevel");
            msgLevelCheckBox.setOpaque(false);
            TapenadeFrame.setFont(msgLevelCheckBox);
            msgLevelCheckBox.addActionListener(e -> {
                msgLevelComboBox.setEnabled(msgLevelCheckBox.isSelected());
            });
        }
        return msgLevelCheckBox;
    }

    protected JComboBox<Integer> getMsgLevelComboBox() {
        if (msgLevelComboBox == null) {
            Integer[] levels = new Integer[12];
            for (int i = 0; i < 12; i++) {
                levels[i] = i + 1;
            }
            msgLevelComboBox = new JComboBox<>(levels);
            TapenadeFrame.setFont(msgLevelComboBox);
            msgLevelComboBox.setSelectedIndex(4);
            msgLevelComboBox.setOpaque(false);
            msgLevelComboBox.setEnabled(false);
        }
        return msgLevelComboBox;
    }

    protected JCheckBox getDump() {
        if (dump == null) {
            dump = new JCheckBox("Dump");
            dump.setOpaque(false);
            TapenadeFrame.setFont(dump);
            dump.addActionListener(e -> {
                dumpFile.setEnabled(dump.isSelected());
            });
        }
        return dump;
    }

    protected JTextField getDumpFile() {
        if (dumpFile == null) {
            dumpFile = new JTextField(8);
            TapenadeFrame.setFont(dumpFile);
            dumpFile.setEnabled(false);
        }
        return dumpFile;
    }
}
