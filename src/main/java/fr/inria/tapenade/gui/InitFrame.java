/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
import java.io.InputStream;

/**
 * Tapenade graphical user interface.
 */
final class InitFrame extends JWindow {

    private static final long serialVersionUID = 7484299994054L;

    protected InitFrame() {
        InputStream in =
                this.getClass().getResourceAsStream("/images/tapenadelogo.gif");
        byte[] buffer = null;
        try {
            buffer = new byte[in.available()];

            for (int i = 0, n = in.available(); i < n; i++) {
                buffer[i] = (byte) in.read();
            }
        } catch (java.io.IOException e) {
            System.out.println("File not found: tapenadelogo.gif "+e);
        }
        if (buffer != null) {
            JLabel label = new JLabel(new ImageIcon(buffer));
            getContentPane().add(label);
            pack();
            int[] center = TapenadeFrame.center(this);
            setLocation(center[0], center[1]);
            setVisible(true);
        }
    }
}
