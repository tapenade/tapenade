/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import java.awt.GridBagLayout;

/**
 * Tapenade graphical user interface.
 */
class TopUnitPanel extends JPanel {
    private static final long serialVersionUID = 7774299994054L;
    private JComboBox<String> units;
    private boolean recompute;

    protected TopUnitPanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Top Procedure :"),
                BorderFactory.createEmptyBorder(2, 2, 2, 2)));

        java.awt.GridBagConstraints constraintsUnits = new java.awt.GridBagConstraints();
        constraintsUnits.gridx = 0;
        constraintsUnits.gridy = 0;
        constraintsUnits.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsUnits.weightx = 0.2;
        constraintsUnits.weighty = 1.0;
        constraintsUnits.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getUnits(), constraintsUnits);
    }

    protected JComboBox<String> getUnits() {
        if (units == null) {
            units = new JComboBox<>();
            TapenadeFrame.setBackground(units);
            TapenadeFrame.setFont(units);
            units.setModel(new DefaultComboBoxModel<>());
            units.setPrototypeDisplayValue("Empty  ");
            units.addActionListener(e -> {
                if (!recompute) {
                    ((DefaultListModel) TapenadeFrame.getInstance()
                            .getInputVariablesPanel()
                            .getVariablesList()
                            .getModel()).removeAllElements();
                    ((DefaultListModel) TapenadeFrame.getInstance()
                            .getOutputVariablesPanel()
                            .getVariablesList()
                            .getModel()).removeAllElements();
                    Object obj = TapenadeFrame.getInstance()
                            .getTopUnitPanel()
                            .getUnits()
                            .getSelectedItem();
                    if (obj != null) {
                        String unitName = obj.toString();
                        TapenadeFrame.getInstance().setTopUnitName(unitName);
                    }
                }
            });
        }

        return units;
    }

    protected void setRecompute(boolean recompute) {
        this.recompute = recompute;
    }
}
