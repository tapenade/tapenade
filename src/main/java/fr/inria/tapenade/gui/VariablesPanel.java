/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import fr.inria.tapenade.representation.Unit;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

/**
 * Tapenade graphical user interface.
 */
final class VariablesPanel extends JPanel {
    private static final long serialVersionUID = 4554299994054L;
    protected static final int OUTPUT = 0;
    protected static final int INPUT = 1;
    private final int mode;
    private JList<String> variablesList;
    private JScrollPane scrollFiles;
    private JButton addButton;
    private JButton removeButton;

    protected VariablesPanel(int mode) {
        super();
        this.mode = mode;
        setPreferredSize(new Dimension(230, 120));
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setLayout(new GridBagLayout());

        String title;

        if (mode == OUTPUT) {
            title = "Output Variables :";
        } else {
            title = "Input Variables :";
        }

        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(title),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));

        java.awt.GridBagConstraints constraintsAddButton = new java.awt.GridBagConstraints();
        constraintsAddButton.gridx = 1;
        constraintsAddButton.gridy = 0;
        constraintsAddButton.anchor = java.awt.GridBagConstraints.SOUTH;
        constraintsAddButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsAddButton.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getAddButton(), constraintsAddButton);

        java.awt.GridBagConstraints constraintsRemoveButton = new java.awt.GridBagConstraints();
        constraintsRemoveButton.gridx = 1;
        constraintsRemoveButton.gridy = 1;
        constraintsRemoveButton.anchor = java.awt.GridBagConstraints.NORTH;
        constraintsRemoveButton.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsRemoveButton.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getRemoveButton(), constraintsRemoveButton);

        java.awt.GridBagConstraints constraintsList = new java.awt.GridBagConstraints();
        constraintsList.gridx = 0;
        constraintsList.gridy = 0;
        constraintsList.gridheight = 3;
        constraintsList.fill = java.awt.GridBagConstraints.BOTH;
        constraintsList.weightx = 1.0;
        constraintsList.weighty = 1.0;
        constraintsList.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getScrollFiles(), constraintsList);
    }

    protected JList<String> getVariablesList() {
        if (variablesList == null) {
            variablesList = new JList<>(new DefaultListModel<>());
            TapenadeFrame.setFont(getVariablesList());
            variablesList.setVisibleRowCount(3);
        }
        return variablesList;
    }

    protected JScrollPane getScrollFiles() {
        if (scrollFiles == null) {
            scrollFiles = new JScrollPane(getVariablesList());
        }
        return scrollFiles;
    }

    protected JButton getAddButton() {
        if (addButton == null) {
            addButton = new JButton();
            addButton.setText("add");
            TapenadeFrame.setFont(addButton);
            addButton.setOpaque(false);
            addButton.addActionListener(new ActionListener() {
                private VariablesChooser chooser;

                public ActionListener initialyse() {
                    chooser = new VariablesChooser(
                            TapenadeFrame.getInstance());

                    return this;
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    String unitName = (String) TapenadeFrame.getInstance()
                            .getTopUnitPanel()
                            .getUnits()
                            .getSelectedItem();
                    Unit unit = TapenadeFrame.getInstance().getCallGraph().getAnyUnit(unitName);
                    TapenadeFrame.getInstance().buildExternalContext(unit);

                    DefaultListModel<String> model;

                    if (mode == OUTPUT) {
                        model = (DefaultListModel<String>) TapenadeFrame.getInstance()
                                .getOutputVariablesPanel()
                                .getVariablesList()
                                .getModel();
                    } else {
                        model = (DefaultListModel<String>) TapenadeFrame.getInstance()
                                .getInputVariablesPanel()
                                .getVariablesList()
                                .getModel();
                    }

                    Enumeration<?> enumeration = model.elements();

                    while (enumeration.hasMoreElements()) {
                        TapenadeFrame.getInstance().getVariables().remove(enumeration.nextElement());
                    }

                    chooser.showDialog(TapenadeFrame.getInstance()
                            .getVariables());

                    for (Object o : chooser.getVariables()) {
                        model.addElement((String) o);
                    }
                }
            }.initialyse());
        }
        return addButton;
    }

    protected JButton getRemoveButton() {
        if (removeButton == null) {
            removeButton = new JButton();
            removeButton.setText("remove");
            TapenadeFrame.setFont(removeButton);
            removeButton.setOpaque(false);
            removeButton.addActionListener(e -> {
                Object[] vars;

                if (mode == OUTPUT) {
                    vars = TapenadeFrame.getInstance()
                            .getOutputVariablesPanel()
                            .getVariablesList()
                            .getSelectedValuesList()
                            .toArray();
                } else {
                    vars = TapenadeFrame.getInstance()
                            .getInputVariablesPanel()
                            .getVariablesList()
                            .getSelectedValuesList()
                            .toArray();
                }

                DefaultListModel<String> model;

                if (mode == OUTPUT) {
                    model = (DefaultListModel<String>) TapenadeFrame.getInstance()
                            .getOutputVariablesPanel()
                            .getVariablesList()
                            .getModel();
                } else {
                    model = (DefaultListModel<String>) TapenadeFrame.getInstance()
                            .getInputVariablesPanel()
                            .getVariablesList()
                            .getModel();
                }

                for (Object var : vars) {
                    model.removeElement(var);
                }
            });
        }
        return removeButton;
    }
}
