/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import java.awt.FlowLayout;

/**
 * Tapenade graphical user interface.
 */
class ControlPanel extends JPanel {
    private static final long serialVersionUID = 44937299994054L;
    private JCheckBox htmlBox;
    private JButton changeModeButton;
    private JButton differentiateButton;
    private boolean advancedMode;

    protected ControlPanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setLayout(new FlowLayout(FlowLayout.RIGHT));
        add(getHtmlCheckBox());
        add(getDifferentiateButton());
        add(getChangeModeButton());
    }

    protected JCheckBox getHtmlCheckBox() {
        if (htmlBox == null) {
            // create checkBox
            htmlBox = new JCheckBox();
            htmlBox.setText("HTML view");
            TapenadeFrame.setFont(htmlBox);
            TapenadeFrame.setBackground(htmlBox);
            htmlBox.setSelected(true);
        }

        return htmlBox;
    }

    protected JButton getDifferentiateButton() {
        if (differentiateButton == null) {
            differentiateButton = new JButton();
            differentiateButton.setText("Differentiate");
            TapenadeFrame.setFont(differentiateButton);
            differentiateButton.setOpaque(false);
            differentiateButton.addActionListener(e -> TapenadeFrame.getInstance().runTapenade());
        }

        return differentiateButton;
    }

    private JButton getChangeModeButton() {
        if (changeModeButton == null) {
            changeModeButton = new JButton();
            changeModeButton.setText("Advanced Mode");
            TapenadeFrame.setFont(changeModeButton);
            changeModeButton.setOpaque(false);
            changeModeButton.addActionListener(e -> {
                if (advancedMode) {
                    advancedMode = false;
                    changeModeButton.setText("Advanced Mode");
                } else {
                    advancedMode = true;
                    changeModeButton.setText("Standard Mode");
                }

                TapenadeFrame.getInstance().changeMode(advancedMode);
            });
        }

        return changeModeButton;
    }
}
