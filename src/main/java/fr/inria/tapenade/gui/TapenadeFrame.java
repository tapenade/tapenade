/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.toplevel.Tapenade;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.Rectangle;
import java.awt.Window;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Objects;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.border.BevelBorder;

/**
 * Tapenade graphical user interface.
 */
public final class TapenadeFrame extends JFrame {

    private static final long serialVersionUID = 43734299994054L;
    private static TapenadeFrame frame;
    private InitFrame tapenadeInit;
    /**
     * Panels.
     */
    private InputLanguagePanel inputLanguagePanel;
    private FilesPanel filesPanel;
    private TopUnitPanel topUnitPanel;
    private VariablesPanel outputVariablesPanel;
    private VariablesPanel inputVariablesPanel;
    private AnalyseModePanel analyseModePanel;
    private ControlPanel controlPanel;
    private JMenu menu;
    private JPanel advancedPanel;
    private JPanel mainPanel;
    private CallGraph callGraph;
    private String topUnitName = "";
    private List<String> variables;
    private JPanel advancedFilesPanel;
    private JPanel advancedVariablesPanel;
    private JPanel advancedAnalysePanel;
    private JPanel integerPanel;
    private JPanel realPanel;
    private JPanel doublePanel;
    private OutputPanel outputPanel;
    private OptionPanel optionPanel;
    private JPanel outputLanguagePanel;
    private ExtFilesPanel extFilePanel;
    private ExtFilesPanel extAdFilePanel;
    private JTabbedPane tabs;
    private JSplitPane split;
    private JRadioButton integerTwoBytesButton;
    private JRadioButton integerEightBytesButton;
    private JRadioButton realEightBytesButton;
    private JRadioButton doubleFourBytesButton;
    private JRadioButton doubleEightBytesButton;

    /**
     * Index of the first file arg in the args array.
     */
    private int filesArgsIndex;

    private TapenadeFrame() {
        super();
        System.setProperty("java.util.prefs.syncInterval", "2000000");
        tapenadeInit = new InitFrame();
    }

    public static TapenadeFrame getInstance() {
        if (frame == null) {
            frame = new TapenadeFrame();
            frame.init();
        }
        return frame;
    }

    protected static void setBackground(Component c) {
        c.setBackground(new Color(204, 204, 255));
    }

    protected static void setFont(Component c) {
        c.setFont(new Font("dialog", Font.PLAIN, 11));
    }

    protected static int[] center(Window window) {
        int[] i = new int[2];
        Dimension dim = window.getToolkit().getScreenSize();
        Rectangle abounds = window.getBounds();
        i[0] = (dim.width - abounds.width) / 2;
        i[1] = (dim.height - abounds.height) / 2;
        return i;
    }

    private void init() {
        initialize();
        this.toFront();
        tapenadeInit.setVisible(false);
        tapenadeInit = null;
    }

    private void initialize() {
        setBackground(this);
        setTitle("Tapenade");
        setName("Tapenade");
        setFont(this);
        setSize(450, 500);
        setResizable(false);
        this.setJMenuBar(new JMenuBar());
        this.getJMenuBar().setBorder(BorderFactory.createBevelBorder(
                BevelBorder.RAISED));
        this.getJMenuBar().add(getMenu());
        setBackground(this.getJMenuBar());
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        // set Icon image
        InputStream in =
                this.getClass().getResourceAsStream("/images/tapenadelogo.gif");
        byte[] buffer = null;
        try {
            buffer = new byte[in.available()];
            for (int i = 0, n = in.available(); i < n; i++) {
                buffer[i] = (byte) in.read();
            }
        } catch (java.io.IOException e) {
            System.out.println("File not found: tapenadelogo.gif "+e);
        }
        if (buffer != null) {
            setIconImage(new ImageIcon(buffer).getImage());
        }
        // end set icon image
        getAdvancedPanel();
        setContentPane(getMainPanel());
        int[] center = center(this);
        setLocation(center[0], center[1]);
        setVisible(true);
        pack();
        validate();
    }

    private JMenu getMenu() {
        if (menu == null) {
            menu = new JMenu();
            menu.setText("File");
            menu.setOpaque(false);
            JMenuItem exitItem = new JMenuItem();
            exitItem.setText("Quit");
            setBackground(exitItem);
            exitItem.addActionListener(new MyActionListener());
            menu.add(exitItem);
        }
        return menu;
    }

    private JPanel getAdvancedPanel() {
        if (advancedPanel == null) {
            advancedPanel = new JPanel();
            tabs = new JTabbedPane();
            setBackground(advancedPanel);
            setBackground(tabs);
            advancedPanel.setLayout(new GridBagLayout());
        }
        tabs.addTab("Files", getAdvancedFilesPanel());
        tabs.addTab("Variables", getAdvancedVariablesPanel());
        tabs.addTab("Analysis", getAdvancedAnalysePanel());
        java.awt.GridBagConstraints constraintsTabsPanel = new java.awt.GridBagConstraints();
        constraintsTabsPanel.gridx = 0;
        constraintsTabsPanel.gridy = 0;
        constraintsTabsPanel.gridwidth = 1;
        constraintsTabsPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsTabsPanel.weightx = 1.0;
        constraintsTabsPanel.weighty = 1.0;
        constraintsTabsPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedPanel.add(tabs, constraintsTabsPanel);
        java.awt.GridBagConstraints constraintsControlPanel = new java.awt.GridBagConstraints();
        constraintsControlPanel.gridx = 0;
        constraintsControlPanel.gridy = 1;
        constraintsControlPanel.gridwidth = 1;
        constraintsControlPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsControlPanel.weightx = 1.0;
        constraintsControlPanel.weighty = 0.3;
        constraintsControlPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedPanel.add(getControlPanel(), constraintsControlPanel);
        advancedPanel.updateUI();
        return advancedPanel;
    }

    private JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel();
            mainPanel.setLayout(new GridBagLayout());
            setBackground(mainPanel);
            setFont(mainPanel);
            split.setDividerSize(0);
            split.setOpaque(false);
        }
        // add panels
        java.awt.GridBagConstraints constraintsInputLanguagePanel = new java.awt.GridBagConstraints();
        constraintsInputLanguagePanel.gridx = 0;
        constraintsInputLanguagePanel.gridy = 0;
        constraintsInputLanguagePanel.gridwidth = 2;
        constraintsInputLanguagePanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsInputLanguagePanel.weightx = 1.0;
        constraintsInputLanguagePanel.weighty = 0.3;
        constraintsInputLanguagePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(getInputLanguagePanel(), constraintsInputLanguagePanel);
        java.awt.GridBagConstraints constraintsTopUnitPanel = new java.awt.GridBagConstraints();
        constraintsTopUnitPanel.gridx = 0;
        constraintsTopUnitPanel.gridy = 1;
        constraintsTopUnitPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsTopUnitPanel.weightx = 0.2;
        constraintsTopUnitPanel.weighty = 0.9;
        constraintsTopUnitPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        split.setRightComponent(getTopUnitPanel());
        java.awt.GridBagConstraints constraintsIncludesPanel = new java.awt.GridBagConstraints();
        constraintsIncludesPanel.gridx = 1;
        constraintsIncludesPanel.gridy = 1;
        constraintsIncludesPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsIncludesPanel.weightx = 1.8;
        constraintsIncludesPanel.weighty = 0.9;
        constraintsIncludesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        split.setLeftComponent(getFilesPanel());
        java.awt.GridBagConstraints constraintsSplitPanel = new java.awt.GridBagConstraints();
        constraintsSplitPanel.gridx = 0;
        constraintsSplitPanel.gridy = 1;
        constraintsSplitPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsSplitPanel.gridwidth = 2;
        constraintsSplitPanel.weightx = 1;
        constraintsSplitPanel.weighty = 1;
        constraintsSplitPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(split, constraintsSplitPanel);
        java.awt.GridBagConstraints constraintsInputVariablesPanel = new java.awt.GridBagConstraints();
        constraintsInputVariablesPanel.gridx = 0;
        constraintsInputVariablesPanel.gridy = 2;
        constraintsInputVariablesPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsInputVariablesPanel.weightx = 0.5;
        constraintsInputVariablesPanel.weighty = 0.9;
        constraintsInputVariablesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(getInputVariablesPanel(), constraintsInputVariablesPanel);
        java.awt.GridBagConstraints constraintsOutputVariablesPanel = new java.awt.GridBagConstraints();
        constraintsOutputVariablesPanel.gridx = 1;
        constraintsOutputVariablesPanel.gridy = 2;
        constraintsOutputVariablesPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsOutputVariablesPanel.weightx = 0.5;
        constraintsOutputVariablesPanel.weighty = 0.9;
        constraintsOutputVariablesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(getOutputVariablesPanel(), constraintsOutputVariablesPanel);
        java.awt.GridBagConstraints constraintsAnalyseModePanel = new java.awt.GridBagConstraints();
        constraintsAnalyseModePanel.gridx = 0;
        constraintsAnalyseModePanel.gridy = 3;
        constraintsAnalyseModePanel.gridwidth = 2;
        constraintsAnalyseModePanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsAnalyseModePanel.weightx = 1.0;
        constraintsAnalyseModePanel.weighty = 0.3;
        constraintsAnalyseModePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(getAnalyseModePanel(), constraintsAnalyseModePanel);
        java.awt.GridBagConstraints constraintsControlPanel = new java.awt.GridBagConstraints();
        constraintsControlPanel.gridx = 0;
        constraintsControlPanel.gridy = 4;
        constraintsControlPanel.gridwidth = 2;
        constraintsControlPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsControlPanel.weightx = 1.0;
        constraintsControlPanel.weighty = 0.3;
        constraintsControlPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        mainPanel.add(getControlPanel(), constraintsControlPanel);
        return mainPanel;
    }

    private JPanel getAdvancedFilesPanel() {
        if (advancedFilesPanel == null) {
            advancedFilesPanel = new JPanel();
            setBackground(advancedFilesPanel);
            advancedFilesPanel.setLayout(new GridBagLayout());
            split = new JSplitPane();
            split.setDividerSize(0);
            split.setOpaque(false);
        }
        java.awt.GridBagConstraints constraintsInputLanguagePanel = new java.awt.GridBagConstraints();
        constraintsInputLanguagePanel.gridx = 0;
        constraintsInputLanguagePanel.gridy = 0;
        constraintsInputLanguagePanel.gridwidth = 2;
        constraintsInputLanguagePanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsInputLanguagePanel.weightx = 1.0;
        constraintsInputLanguagePanel.weighty = 0.3;
        constraintsInputLanguagePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedFilesPanel.add(getInputLanguagePanel(),
                constraintsInputLanguagePanel);
        split.setLeftComponent(getTopUnitPanel());
        split.setRightComponent(getFilesPanel());
        java.awt.GridBagConstraints constraintsSplitPanel = new java.awt.GridBagConstraints();
        constraintsSplitPanel.gridx = 0;
        constraintsSplitPanel.gridy = 1;
        constraintsSplitPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsSplitPanel.gridwidth = 2;
        constraintsSplitPanel.weightx = 1;
        constraintsSplitPanel.weighty = 1;
        constraintsSplitPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedFilesPanel.add(split, constraintsSplitPanel);
        java.awt.GridBagConstraints constraintsExtFilesPanel = new java.awt.GridBagConstraints();
        constraintsExtFilesPanel.gridx = 0;
        constraintsExtFilesPanel.gridy = 2;
        constraintsExtFilesPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsExtFilesPanel.gridwidth = 1;
        constraintsExtFilesPanel.weightx = 1;
        constraintsExtFilesPanel.weighty = 1;
        constraintsExtFilesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedFilesPanel.add(getExtFilesPanel(), constraintsExtFilesPanel);
        java.awt.GridBagConstraints constraintsExtAdFilesPanel = new java.awt.GridBagConstraints();
        constraintsExtAdFilesPanel.gridx = 1;
        constraintsExtAdFilesPanel.gridy = 2;
        constraintsExtAdFilesPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsExtAdFilesPanel.gridwidth = 1;
        constraintsExtAdFilesPanel.weightx = 1;
        constraintsExtAdFilesPanel.weighty = 1;
        constraintsExtAdFilesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedFilesPanel.add(getExtAdFilesPanel(), constraintsExtAdFilesPanel);
        return advancedFilesPanel;
    }

    private JPanel getAdvancedVariablesPanel() {
        if (advancedVariablesPanel == null) {
            advancedVariablesPanel = new JPanel();
            advancedVariablesPanel.setLayout(new GridBagLayout());
            setBackground(advancedVariablesPanel);
        }
        java.awt.GridBagConstraints constraintsInputVariablesPanel = new java.awt.GridBagConstraints();
        constraintsInputVariablesPanel.gridx = 0;
        constraintsInputVariablesPanel.gridy = 0;
        constraintsInputVariablesPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsInputVariablesPanel.weightx = 0.5;
        constraintsInputVariablesPanel.weighty = 0.9;
        constraintsInputVariablesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedVariablesPanel.add(getInputVariablesPanel(),
                constraintsInputVariablesPanel);
        java.awt.GridBagConstraints constraintsOutputVariablesPanel = new java.awt.GridBagConstraints();
        constraintsOutputVariablesPanel.gridx = 1;
        constraintsOutputVariablesPanel.gridy = 0;
        constraintsOutputVariablesPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsOutputVariablesPanel.weightx = 0.5;
        constraintsOutputVariablesPanel.weighty = 0.9;
        constraintsOutputVariablesPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedVariablesPanel.add(getOutputVariablesPanel(),
                constraintsOutputVariablesPanel);
        java.awt.GridBagConstraints constraintsIntegerPanel = new java.awt.GridBagConstraints();
        constraintsIntegerPanel.gridx = 0;
        constraintsIntegerPanel.gridy = 1;
        constraintsIntegerPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsIntegerPanel.weightx = 1;
        constraintsIntegerPanel.weighty = 0.3;
        constraintsIntegerPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedVariablesPanel.add(getIntegerPanel(), constraintsIntegerPanel);
        java.awt.GridBagConstraints constraintsRealPanel = new java.awt.GridBagConstraints();
        constraintsRealPanel.gridx = 1;
        constraintsRealPanel.gridy = 1;
        constraintsRealPanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsRealPanel.weightx = 1;
        constraintsRealPanel.weighty = 0.3;
        constraintsRealPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedVariablesPanel.add(getRealPanel(), constraintsRealPanel);
        java.awt.GridBagConstraints constraintsDoublePanel = new java.awt.GridBagConstraints();
        constraintsDoublePanel.gridx = 0;
        constraintsDoublePanel.gridy = 2;
        constraintsDoublePanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsDoublePanel.weightx = 1;
        constraintsDoublePanel.weighty = 0.3;
        constraintsDoublePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedVariablesPanel.add(getDoublePanel(), constraintsDoublePanel);
        return advancedVariablesPanel;
    }

    private JPanel getAdvancedAnalysePanel() {
        if (advancedAnalysePanel == null) {
            advancedAnalysePanel = new JPanel();
            setBackground(advancedAnalysePanel);
            advancedAnalysePanel.setLayout(new GridBagLayout());
        }
        java.awt.GridBagConstraints constraintsAnalyseModePanel = new java.awt.GridBagConstraints();
        constraintsAnalyseModePanel.gridx = 0;
        constraintsAnalyseModePanel.gridy = 0;
        constraintsAnalyseModePanel.gridwidth = 1;
        constraintsAnalyseModePanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsAnalyseModePanel.weightx = 1;
        constraintsAnalyseModePanel.weighty = 0.9;
        constraintsAnalyseModePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedAnalysePanel.add(getAnalyseModePanel(),
                constraintsAnalyseModePanel);
        java.awt.GridBagConstraints constraintsOutputLanguagePanel = new java.awt.GridBagConstraints();
        constraintsOutputLanguagePanel.gridx = 0;
        constraintsOutputLanguagePanel.gridy = 1;
        constraintsOutputLanguagePanel.gridwidth = 1;
        constraintsOutputLanguagePanel.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsOutputLanguagePanel.weightx = 1;
        constraintsOutputLanguagePanel.weighty = 0.9;
        constraintsOutputLanguagePanel.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedAnalysePanel.add(getOutputLanguagePanel(),
                constraintsOutputLanguagePanel);
        JSplitPane splitPane = new JSplitPane();
        splitPane.setDividerSize(0);
        splitPane.setOpaque(false);
        splitPane.setBorder(null);
        splitPane.setLeftComponent(getOutputPanel());
        splitPane.setRightComponent(getOptionPanel());
        java.awt.GridBagConstraints constraintsSplit = new java.awt.GridBagConstraints();
        constraintsSplit.gridx = 0;
        constraintsSplit.gridy = 2;
        constraintsSplit.gridwidth = 1;
        constraintsSplit.gridheight = 1;
        constraintsSplit.fill = java.awt.GridBagConstraints.HORIZONTAL;
        constraintsSplit.weightx = 1;
        constraintsSplit.weighty = 1;
        constraintsSplit.insets = new java.awt.Insets(4, 4, 4, 4);
        advancedAnalysePanel.add(splitPane, constraintsSplit);
        return advancedAnalysePanel;
    }

    protected ControlPanel getControlPanel() {
        if (controlPanel == null) {
            controlPanel = new ControlPanel();
        }
        return controlPanel;
    }

    private InputLanguagePanel getInputLanguagePanel() {
        if (inputLanguagePanel == null) {
            inputLanguagePanel = new InputLanguagePanel();
        }
        return inputLanguagePanel;
    }

    protected TopUnitPanel getTopUnitPanel() {
        if (topUnitPanel == null) {
            topUnitPanel = new TopUnitPanel();
        }
        return topUnitPanel;
    }

    private FilesPanel getFilesPanel() {
        if (filesPanel == null) {
            filesPanel = new FilesPanel();
        }
        return filesPanel;
    }

    protected VariablesPanel getInputVariablesPanel() {
        if (inputVariablesPanel == null) {
            inputVariablesPanel = new VariablesPanel(VariablesPanel.INPUT);
        }
        return inputVariablesPanel;
    }

    protected VariablesPanel getOutputVariablesPanel() {
        if (outputVariablesPanel == null) {
            outputVariablesPanel = new VariablesPanel(VariablesPanel.OUTPUT);
        }
        return outputVariablesPanel;
    }

    protected AnalyseModePanel getAnalyseModePanel() {
        if (analyseModePanel == null) {
            analyseModePanel = new AnalyseModePanel();
        }
        return analyseModePanel;
    }

    private JPanel getExtFilesPanel() {
        if (extFilePanel == null) {
            extFilePanel = new ExtFilesPanel(ExtFilesPanel.EXT);
        }
        return extFilePanel;
    }

    private JPanel getExtAdFilesPanel() {
        if (extAdFilePanel == null) {
            extAdFilePanel = new ExtFilesPanel(ExtFilesPanel.EXTAD);
        }
        return extAdFilePanel;
    }

    private JPanel getIntegerPanel() {
        if (integerPanel == null) {
            integerPanel = new JPanel();
            integerPanel.setOpaque(false);
            integerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            integerPanel.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Integer Precision"),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5)));
            integerTwoBytesButton = new JRadioButton("2 bytes");
            JRadioButton integerFourBytesButton = new JRadioButton("4 bytes");
            integerEightBytesButton = new JRadioButton("8 bytes");
            TapenadeFrame.setFont(integerTwoBytesButton);
            TapenadeFrame.setFont(integerFourBytesButton);
            TapenadeFrame.setFont(integerEightBytesButton);
            integerFourBytesButton.setSelected(true);
            integerTwoBytesButton.setSelected(false);
            integerEightBytesButton.setSelected(false);
            integerFourBytesButton.setOpaque(false);
            integerTwoBytesButton.setOpaque(false);
            integerEightBytesButton.setOpaque(false);
            ButtonGroup integerButtonGroup = new ButtonGroup();
            integerButtonGroup.add(integerTwoBytesButton);
            integerButtonGroup.add(integerFourBytesButton);
            integerButtonGroup.add(integerEightBytesButton);
            integerPanel.add(integerTwoBytesButton);
            integerPanel.add(integerFourBytesButton);
            integerPanel.add(integerEightBytesButton);
        }
        return integerPanel;
    }

    private JPanel getRealPanel() {
        if (realPanel == null) {
            realPanel = new JPanel();
            realPanel.setOpaque(false);
            realPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            realPanel.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Real Precision"),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5)));
            JRadioButton realFourBytesButton = new JRadioButton("4 bytes");
            realEightBytesButton = new JRadioButton("8 bytes");
            TapenadeFrame.setFont(realFourBytesButton);
            TapenadeFrame.setFont(realEightBytesButton);
            realFourBytesButton.setSelected(true);
            realEightBytesButton.setSelected(false);
            realFourBytesButton.setOpaque(false);
            realEightBytesButton.setOpaque(false);
            ButtonGroup realButtonGroup = new ButtonGroup();
            realButtonGroup.add(realFourBytesButton);
            realButtonGroup.add(realEightBytesButton);
            realPanel.add(realFourBytesButton);
            realPanel.add(realEightBytesButton);
        }
        return realPanel;
    }

    private JPanel getDoublePanel() {
        if (doublePanel == null) {
            doublePanel = new JPanel();
            doublePanel.setOpaque(false);
            doublePanel.setLayout(new FlowLayout(FlowLayout.CENTER));
            doublePanel.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createTitledBorder("Double Precision"),
                    BorderFactory.createEmptyBorder(5, 5, 5, 5)));
            doubleFourBytesButton = new JRadioButton("4 bytes");
            doubleEightBytesButton = new JRadioButton("8 bytes");
            JRadioButton doubleSixteenBytesButton = new JRadioButton("16 bytes");
            TapenadeFrame.setFont(doubleSixteenBytesButton);
            TapenadeFrame.setFont(doubleFourBytesButton);
            TapenadeFrame.setFont(doubleEightBytesButton);
            doubleFourBytesButton.setSelected(false);
            doubleSixteenBytesButton.setSelected(false);
            doubleEightBytesButton.setSelected(true);
            doubleFourBytesButton.setOpaque(false);
            doubleSixteenBytesButton.setOpaque(false);
            doubleEightBytesButton.setOpaque(false);
            ButtonGroup doubleButtonGroup = new ButtonGroup();
            doubleButtonGroup.add(doubleFourBytesButton);
            doubleButtonGroup.add(doubleEightBytesButton);
            doubleButtonGroup.add(doubleSixteenBytesButton);
            doublePanel.add(doubleFourBytesButton);
            doublePanel.add(doubleEightBytesButton);
            doublePanel.add(doubleSixteenBytesButton);
        }
        return doublePanel;
    }

    private JPanel getOutputLanguagePanel() {
        if (outputLanguagePanel == null) {
            outputLanguagePanel = new OutputLanguagePanel();
        }
        return outputLanguagePanel;
    }

    private JPanel getOutputPanel() {
        if (outputPanel == null) {
            outputPanel = new OutputPanel();
        }
        return outputPanel;
    }

    private JPanel getOptionPanel() {
        if (optionPanel == null) {
            optionPanel = new OptionPanel();
        }
        return optionPanel;
    }

    protected void setTopUnitName(String name) {
        topUnitName = name;
    }

    protected CallGraph getCallGraph() {
        return callGraph;
    }

    protected void runTapenade() {
        // build the properties
        ArrayList<String> args = new ArrayList<>();
        args.add("-parserfileseparator");
        args.add(File.separator);
        selectOutput(args);
        selectInputLanguage(args);
        selectOutputVariables(args);
        selectInputVariables(args);
        selectTopUnit(args);
        selectAnalyseMode(args);
        selectOutputLanguage(args);
        selectPreprocess(args);
        selectOutputFileName(args);
        selectOutputDirectory(args);
        selectExtFiles(args);
        selectExtAdFiles(args);
        selectNoLib(args);
        selectNoAdLib(args);
        selectDiffVarName(args);
        selectDiffFuncName(args);
        selectInteger(args);
        selectDouble(args);
        selectReal(args);
        selectMsgLevel(args);
        selectView(args);
        selectDump(args);
        selectFiles(args);
        StringBuilder tapCommand = new StringBuilder("tapenade ");
        String prefix = null;
        String suffix = null;
        for (int i = 0; i < args.size(); i++) {
            if (prefix == null) {
                if (i < filesArgsIndex) {
                    if (args.get(i).startsWith("-")
                            && !args.get(i + 1).startsWith("-")
                            && i + 1 < filesArgsIndex) {
                        prefix = "\"";
                        suffix = "\"";
                    }
                }
                tapCommand.append(args.get(i));
                tapCommand.append(' ');
            } else {
                tapCommand.append(prefix);
                tapCommand.append(args.get(i));
                tapCommand.append(suffix);
                tapCommand.append(' ');
                suffix = null;
                prefix = null;
            }
        }
        System.out.println(tapCommand);
        Tapenade.main(args.toArray(new String[0]));
    }

    private void selectOutput(List<String> args) {
        if (getControlPanel().getHtmlCheckBox().isSelected()) {
            args.add("-html");
        }
    }

    /**
     * Input language.
     */
    private void selectInputLanguage(List<String> args) {
        Enumeration<?> enumeration = getInputLanguagePanel().getInputLanguageButtons()
                .getElements();
        while (enumeration.hasMoreElements()) {
            JRadioButton button = (JRadioButton) enumeration.nextElement();
            if (button.isSelected()) {
                if (!button.getName().equals("suffix")) {
                    args.add("-inputlanguage");
                    args.add(button.getText());
                }
                break;
            }
        }
    }

    /**
     * Output variables.
     */
    private void selectOutputVariables(List<String> args) {
        StringBuilder outputVariables = new StringBuilder();
        DefaultListModel<?> model = (DefaultListModel) getOutputVariablesPanel()
                .getVariablesList()
                .getModel();
        Enumeration<?> e = model.elements();
        while (e.hasMoreElements()) {
            outputVariables.append((String) e.nextElement());
            if (e.hasMoreElements()) {
                outputVariables.append(' ');
            }
        }
        if (outputVariables.length() != 0) {
            args.add("-outvars");
            args.add(outputVariables.toString());
        }
    }

    /**
     * Input variables.
     */
    private void selectInputVariables(List<String> args) {
        StringBuilder inputVariables = new StringBuilder();
        DefaultListModel<?> model = (DefaultListModel) getInputVariablesPanel()
                .getVariablesList()
                .getModel();
        Enumeration<?> e = model.elements();
        while (e.hasMoreElements()) {
            inputVariables.append((String) e.nextElement());
            if (e.hasMoreElements()) {
                inputVariables.append(' ');
            }
        }
        if (inputVariables.length() != 0) {
            args.add("-vars");
            args.add(inputVariables.toString());
        }
    }

    private void selectTopUnit(List<String> args) {
        //   top unit
        String unitName = (String) getTopUnitPanel().getUnits()
                .getSelectedItem();
        if (!Objects.requireNonNull(unitName).isEmpty()) {
            args.add("-head");
            args.add(unitName);
        }
    }

    /**
     * Analyse mode.
     */
    private void selectAnalyseMode(List<String> args) {
        Enumeration<?> differentiateEnum = getAnalyseModePanel()
                .getDifferentiateButtons()
                .getElements();
        while (differentiateEnum.hasMoreElements()) {
            JRadioButton button = (JRadioButton) differentiateEnum.nextElement();
            if (button.isSelected()) {
                switch (button.getName()) {
                    case "tangent":
                        args.add("-tangent");
                        break;
                    case "reverse":
                        args.add("-reverse");
                        break;
                    case "tangentVector":
                        args.add("-tangent");
                        args.add("-multi");
                        break;
                    case "reverseVector":
                        args.add("-reverse");
                        args.add("-multi");
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void selectOutputLanguage(List<String> args) {
        Enumeration<?> outputLanguageEnum = ((OutputLanguagePanel) getOutputLanguagePanel()).getOutputLanguageButtons()
                .getElements();
        while (outputLanguageEnum.hasMoreElements()) {
            JRadioButton button = (JRadioButton) outputLanguageEnum.nextElement();
            if (button.isSelected()) {
                args.add("-outputlanguage");
                args.add(button.getName());
                break;
            }
        }
    }

    private void selectPreprocess(List<String> args) {
        if (((OptionPanel) getOptionPanel()).getPreprocessCheckBox().isSelected()) {
            args.add("-p");
        }
    }

    private void selectOutputFileName(List<String> args) {
        if (((OutputPanel) getOutputPanel()).getOutputFileNameCheckBox()
                .isSelected()) {
            String filename = ((OutputPanel) getOutputPanel()).getOutputFileNameTextField()
                    .getText();
            if (!filename.isEmpty()) {
                args.add("-o");
                args.add(filename);
            }
        }
    }

    private void selectOutputDirectory(List<String> args) {
        if (!((OutputPanel) getOutputPanel()).getOutputDirectoryTextField()
                .getText().isEmpty()) {
            args.add("-O");
            args.add(((OutputPanel) getOutputPanel()).getOutputDirectoryTextField()
                    .getText());
        }
    }

    private void selectExtFiles(List<String> args) {
        Enumeration<?> e = ((DefaultListModel) ((ExtFilesPanel) getExtFilesPanel()).getFilesList()
                .getModel()).elements();
        if (!e.hasMoreElements()) {
            return;
        }
        args.add("-ext");
        StringBuilder files = new StringBuilder();
        while (e.hasMoreElements()) {
            File file = (File) e.nextElement();
            files.append(file.getPath());
            if (e.hasMoreElements()) {
                files.append('+');
            }
        }
        args.add(files.toString());
    }

    private void selectExtAdFiles(List<String> args) {
        Enumeration<?> e = ((DefaultListModel) ((ExtFilesPanel) getExtAdFilesPanel()).getFilesList()
                .getModel()).elements();
        if (!e.hasMoreElements()) {
            return;
        }
        args.add("-ext");
        StringBuilder files = new StringBuilder();
        while (e.hasMoreElements()) {
            File file = (File) e.nextElement();
            files.append(file.getPath());
            if (e.hasMoreElements()) {
                files.append('+');
            }
        }
        args.add(files.toString());
    }

    private void selectNoLib(List<String> args) {
        if (((ExtFilesPanel) getExtFilesPanel()).getNoLibCheckBox().isSelected()) {
            args.add("-nolib");
        }
    }

    private void selectNoAdLib(List<String> args) {
        if (((ExtFilesPanel) getExtFilesPanel()).getNoLibCheckBox().isSelected()) {
            args.add("-noADlib");
        }
    }

    private void selectDiffVarName(List<String> args) {
        if (((OutputPanel) getOutputPanel()).getDiffVarNameCheckBox()
                .isSelected()) {
            String diffVarname = ((OutputPanel) getOutputPanel()).getdiffVarNameTextField()
                    .getText();
            if (!diffVarname.isEmpty()) {
                args.add("-diffvarname");
                args.add(diffVarname);
            }
        }
    }

    private void selectDiffFuncName(List<String> args) {
        if (((OutputPanel) getOutputPanel()).getDiffFuncNameCheckBox()
                .isSelected()) {
            String diffFuncname = ((OutputPanel) getOutputPanel()).getDiffFuncNameTextField()
                    .getText();
            if (!diffFuncname.isEmpty()) {
                args.add("-difffuncname");
                args.add(diffFuncname);
            }
        }
    }

    private void selectInteger(List<String> args) {
        String integer;
        if (integerEightBytesButton.isSelected()) {
            integer = "8";
        } else if (integerTwoBytesButton.isSelected()) {
            integer = "2";
        } else {
            integer = "4";
        }
        args.add("-i" + integer);
    }

    private void selectDouble(List<String> args) {
        String doubleString;
        if (doubleEightBytesButton.isSelected()) {
            doubleString = "8";
        } else if (doubleFourBytesButton.isSelected()) {
            doubleString = "4";
        } else {
            doubleString = "16";
        }
        args.add("-dr" + doubleString);
    }

    private void selectReal(List<String> args) {
        String real;
        if (realEightBytesButton.isSelected()) {
            real = "8";
        } else {
            real = "4";
        }
        args.add("-r" + real);
    }

    private void selectMsgLevel(List<String> args) {
        if (((OptionPanel) getOptionPanel()).getMsgLevelCheckBox().isSelected()) {
            args.add("-msglevel");
            String level = Objects.requireNonNull(((OptionPanel) getOptionPanel()).getMsgLevelComboBox()
                    .getSelectedItem()).toString();
            args.add(level);
        }
    }

    private void selectView(List<String> args) {
        if (((OptionPanel) getOptionPanel()).getViewCheckBox().isSelected()) {
            args.add("-view");
        }
    }

    private void selectDump(List<String> args) {
        if (((OptionPanel) getOptionPanel()).getDump().isSelected()) {
            String filename = ((OptionPanel) getOptionPanel()).getDumpFile()
                    .getText();
            if (!filename.isEmpty()) {
                args.add("-dump");
                args.add(filename);
            }
        }
    }

    /**
     * Files.
     */
    private void selectFiles(List<String> args) {
        Enumeration<?> filesEnum = ((DefaultListModel) getFilesPanel()
                .getFilesList()
                .getModel()).elements();
        filesArgsIndex = args.size();
        while (filesEnum.hasMoreElements()) {
            File file = (File) filesEnum.nextElement();
            args.add(file.getPath());
        }
    }

    protected void preprocess() {
        //   Select args
        ArrayList<String> args = new ArrayList<>();
        selectInputLanguage(args);
        selectFiles(args);
        // Build the Call Graph in preprocess mode
        callGraph = Tapenade.preProcess(args.toArray(
                new String[0]));
        // add units in the top Unit comboBox
        TapList<Unit> units = callGraph.units();
        Unit unit;
        while (units != null) {
            unit = units.head;
            if (!unit.isExternal() && !unit.isTranslationUnit()) {
                getTopUnitPanel().getUnits().addItem(units.head.name());
            }
            units = units.tail;
        }
        if (!topUnitName.isEmpty()) {
            // there was a topUnit selected before
            DefaultComboBoxModel<?> model = (DefaultComboBoxModel) getTopUnitPanel()
                    .getUnits()
                    .getModel();
            int size = model.getSize();
            boolean presence = false;
            for (int i = 0; i < size; i++) {
                String unitName = (String) model.getElementAt(i);
                if (unitName.equals(topUnitName)) {
                    // The top Unit is present
                    getTopUnitPanel().getUnits().setSelectedIndex(i);
                    presence = true;
                    break;
                }
            }
            if (!presence) {
                ((DefaultListModel) getInputVariablesPanel().getVariablesList()
                        .getModel()).removeAllElements();
                ((DefaultListModel) getOutputVariablesPanel().getVariablesList()
                        .getModel()).removeAllElements();
            }
        } else {
            String unitName = "";
            TapList<Unit> topUnits = callGraph.topUnits();
            while (topUnits != null) {
                Unit topUnit = topUnits.head;
                if (topUnit.isStandard()) {
                    unitName = topUnit.name();
                    break;
                }
                topUnits = topUnits.tail;
            }
            this.topUnitName = unitName;
        }
    }

    protected void buildExternalContext(Unit unit) {
        List<String> vars = getVariables();
        vars.clear();
        ZoneInfo zi;
        for (int i = 0; i < unit.paramElemsNb(); ++i) {
            zi = unit.paramElemZoneInfo(i);
            if (zi.isCommon() || zi.isParameter() || zi.isGlobal()) {
                TapList<String> varNames = zi.variableNames();
                while (varNames != null) {
                    vars.add(varNames.head);
                    varNames = varNames.tail;
                }
            }
        }
    }

    protected List<String> getVariables() {
        if (variables == null) {
            variables = new ArrayList<>();
        }
        return variables;
    }

    protected void changeMode(boolean mode) {
        if (mode) {
            // advanced mode
            this.setContentPane(getAdvancedPanel());
        } else {
            // standard mode
            this.setContentPane(getMainPanel());
        }
        pack();
    }
}
