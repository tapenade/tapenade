/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 * Tapenade graphical user interface.
 */
class AnalyseModePanel extends JPanel {
    private static final long serialVersionUID = 1243228594054L;
    private JRadioButton tangentModeButton;
    private JRadioButton tangentVectorModeButton;
    private JRadioButton reverseModeButton;
    private JRadioButton reverseVectorModeButton;
    private ButtonGroup differentiateButtons;

    protected AnalyseModePanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setOpaque(false);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder(
                        "Select differentiation method :"),
                BorderFactory.createEmptyBorder(2, 2, 2, 2)));

        getDifferentiateButtons();
        add(getTangentModeButton());
        add(getTangentVectorModeButton());
        add(getReverseModeButton());
        add(getReverseVectorModeButton());
    }

    protected ButtonGroup getDifferentiateButtons() {
        if (differentiateButtons == null) {
            differentiateButtons = new ButtonGroup();
            differentiateButtons.add(getTangentModeButton());
            differentiateButtons.add(getTangentVectorModeButton());
            differentiateButtons.add(getReverseModeButton());
            differentiateButtons.add(getReverseVectorModeButton());
        }
        return differentiateButtons;
    }

    public JRadioButton getTangentModeButton() {
        if (tangentModeButton == null) {
            tangentModeButton = new JRadioButton("Tangent Mode");
            TapenadeFrame.setFont(tangentModeButton);
            tangentModeButton.setOpaque(false);
            tangentModeButton.setName("tangent");
            tangentModeButton.setSelected(true);
        }
        return tangentModeButton;
    }

    public JRadioButton getTangentVectorModeButton() {
        if (tangentVectorModeButton == null) {
            tangentVectorModeButton = new JRadioButton(
                    "Vector Tangent Mode");
            TapenadeFrame.setFont(tangentVectorModeButton);
            tangentVectorModeButton.setOpaque(false);
            tangentVectorModeButton.setName("tangentVector");
        }
        return tangentVectorModeButton;
    }

    public JRadioButton getReverseModeButton() {
        if (reverseModeButton == null) {
            reverseModeButton = new JRadioButton("Adjoint Mode");
            TapenadeFrame.setFont(reverseModeButton);
            reverseModeButton.setOpaque(false);
            reverseModeButton.setName("reverse");
        }
        return reverseModeButton;
    }

    public JRadioButton getReverseVectorModeButton() {
        if (reverseVectorModeButton == null) {
            reverseVectorModeButton = new JRadioButton(
                    "Vector Adjoint Mode");
            TapenadeFrame.setFont(reverseVectorModeButton);
            reverseVectorModeButton.setOpaque(false);
            reverseVectorModeButton.setName("reverseVector");
        }
        return reverseVectorModeButton;
    }
}
