/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.gui;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Tapenade graphical user interface.
 */
class OutputPanel extends JPanel {
    private static final long serialVersionUID = 1243299456054L;
    private JPanel directoryPanel;
    private JTextField outputDirectoryTextField;
    private JCheckBox outputFileNameCheckBox;
    private JTextField outputFileNameTextField;
    private JCheckBox diffVarNameCheckBox;
    private JTextField diffVarNameTextField;
    private JCheckBox diffFuncNameCheckBox;
    private JTextField diffFuncNameTextField;
    private JButton setDirectoryButton;

    protected OutputPanel() {
        super();
        initialize();
    }

    private final void initialize() {
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createTitledBorder("Output"),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        setOpaque(false);

        setLayout(new GridBagLayout());

        java.awt.GridBagConstraints constraintsSplitPanel = new java.awt.GridBagConstraints();
        constraintsSplitPanel.gridx = 0;
        constraintsSplitPanel.gridy = 0;
        constraintsSplitPanel.fill = java.awt.GridBagConstraints.BOTH;
        constraintsSplitPanel.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getDirectoryPanel(), constraintsSplitPanel);

        java.awt.GridBagConstraints constraintsOutputOneFile = new java.awt.GridBagConstraints();
        constraintsOutputOneFile.gridx = 0;
        constraintsOutputOneFile.gridy = 1;
        constraintsOutputOneFile.fill = java.awt.GridBagConstraints.BOTH;
        constraintsOutputOneFile.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getOutputFileNameCheckBox(), constraintsOutputOneFile);

        java.awt.GridBagConstraints constraintsOutputFileNameTextField = new java.awt.GridBagConstraints();
        constraintsOutputFileNameTextField.gridx = 1;
        constraintsOutputFileNameTextField.gridy = 1;
        constraintsOutputFileNameTextField.fill = java.awt.GridBagConstraints.BOTH;
        constraintsOutputFileNameTextField.insets = new java.awt.Insets(4, 4,
                4, 4);
        add(getOutputFileNameTextField(), constraintsOutputFileNameTextField);

        java.awt.GridBagConstraints constraintsDiffVarNameCheckBox = new java.awt.GridBagConstraints();
        constraintsDiffVarNameCheckBox.gridx = 0;
        constraintsDiffVarNameCheckBox.gridy = 2;
        constraintsDiffVarNameCheckBox.fill = java.awt.GridBagConstraints.BOTH;
        constraintsDiffVarNameCheckBox.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getDiffVarNameCheckBox(), constraintsDiffVarNameCheckBox);

        java.awt.GridBagConstraints constraintsDiffVarNameTextField = new java.awt.GridBagConstraints();
        constraintsDiffVarNameTextField.gridx = 1;
        constraintsDiffVarNameTextField.gridy = 2;
        constraintsDiffVarNameTextField.fill = java.awt.GridBagConstraints.BOTH;
        constraintsDiffVarNameTextField.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getdiffVarNameTextField(), constraintsDiffVarNameTextField);

        java.awt.GridBagConstraints constraintsDiffFuncNameCheckBox = new java.awt.GridBagConstraints();
        constraintsDiffFuncNameCheckBox.gridx = 0;
        constraintsDiffFuncNameCheckBox.gridy = 3;
        constraintsDiffFuncNameCheckBox.fill = java.awt.GridBagConstraints.BOTH;
        constraintsDiffFuncNameCheckBox.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getDiffFuncNameCheckBox(), constraintsDiffFuncNameCheckBox);

        java.awt.GridBagConstraints constraintsDiffFuncNameTextField = new java.awt.GridBagConstraints();
        constraintsDiffFuncNameTextField.gridx = 1;
        constraintsDiffFuncNameTextField.gridy = 3;
        constraintsDiffFuncNameTextField.fill = java.awt.GridBagConstraints.BOTH;
        constraintsDiffFuncNameTextField.insets = new java.awt.Insets(4, 4, 4, 4);
        add(getDiffFuncNameTextField(), constraintsDiffFuncNameTextField);
    }

    private JPanel getDirectoryPanel() {
        if (directoryPanel == null) {
            directoryPanel = new JPanel();
            directoryPanel.setOpaque(false);
            add(getSetDirectoryButton());
            add(getOutputDirectoryTextField());
        }
        return directoryPanel;
    }

    protected JCheckBox getOutputFileNameCheckBox() {
        if (outputFileNameCheckBox == null) {
            outputFileNameCheckBox = new JCheckBox("Output filename");
            TapenadeFrame.setFont(outputFileNameCheckBox);
            outputFileNameCheckBox.setOpaque(false);
            outputFileNameCheckBox.addActionListener(e -> {
                outputFileNameTextField.setEnabled(outputFileNameCheckBox.isSelected());
            });
        }
        return outputFileNameCheckBox;
    }

    protected JTextField getOutputFileNameTextField() {
        if (outputFileNameTextField == null) {
            outputFileNameTextField = new JTextField(10);
            TapenadeFrame.setFont(outputFileNameTextField);
            outputFileNameTextField.setEnabled(false);
        }
        return outputFileNameTextField;
    }

    protected JCheckBox getDiffVarNameCheckBox() {
        if (diffVarNameCheckBox == null) {
            diffVarNameCheckBox = new JCheckBox("Output varname");
            TapenadeFrame.setFont(diffVarNameCheckBox);
            diffVarNameCheckBox.setOpaque(false);
            diffVarNameCheckBox.addActionListener(e -> {
                diffVarNameTextField.setEnabled(diffVarNameCheckBox.isSelected());
            });
        }
        return diffVarNameCheckBox;
    }

    protected JTextField getdiffVarNameTextField() {
        if (diffVarNameTextField == null) {
            diffVarNameTextField = new JTextField(10);
            TapenadeFrame.setFont(diffVarNameTextField);
            diffVarNameTextField.setEnabled(false);
        }
        return diffVarNameTextField;
    }

    protected JCheckBox getDiffFuncNameCheckBox() {
        if (diffFuncNameCheckBox == null) {
            diffFuncNameCheckBox = new JCheckBox("Output funcname");
            TapenadeFrame.setFont(diffFuncNameCheckBox);
            diffFuncNameCheckBox.setOpaque(false);
            diffFuncNameCheckBox.addActionListener(e -> {
                diffFuncNameTextField.setEnabled(diffFuncNameCheckBox.isSelected());
            });
        }
        return diffFuncNameCheckBox;
    }

    protected JTextField getDiffFuncNameTextField() {
        if (diffFuncNameTextField == null) {
            diffFuncNameTextField = new JTextField(10);
            TapenadeFrame.setFont(diffFuncNameTextField);
            diffFuncNameTextField.setEnabled(false);
        }
        return diffFuncNameTextField;
    }

    private JButton getSetDirectoryButton() {
        if (setDirectoryButton == null) {
            setDirectoryButton = new JButton();
            setDirectoryButton.setText("Output Directory");
            TapenadeFrame.setFont(setDirectoryButton);
            setDirectoryButton.addActionListener(new ActionListener() {
                private JFileChooser chooser;

                public ActionListener initialyse() {
                    chooser = new JFileChooser(System.getProperty(
                            "user.dir"));
                    chooser.setMultiSelectionEnabled(false);
                    chooser.setFileSelectionMode(
                            JFileChooser.DIRECTORIES_ONLY);
                    TapenadeFrame.setFont(chooser);

                    return this;
                }

                @Override
                public void actionPerformed(ActionEvent e) {
                    int result = chooser.showDialog(
                            TapenadeFrame.getInstance(),
                            "Select Directory");

                    if (result == JFileChooser.APPROVE_OPTION) {
                        File file = chooser.getSelectedFile();

                        if (file.isDirectory()) {
                            getOutputDirectoryTextField().setText(file.getPath());
                        }
                    }
                }
            }.initialyse());
        }
        return setDirectoryButton;
    }

    protected JTextField getOutputDirectoryTextField() {
        if (outputDirectoryTextField == null) {
            outputDirectoryTextField = new JTextField(10);
            TapenadeFrame.setFont(outputDirectoryTextField);
            outputDirectoryTextField.setEnabled(false);
        }
        return outputDirectoryTextField;
    }
}
