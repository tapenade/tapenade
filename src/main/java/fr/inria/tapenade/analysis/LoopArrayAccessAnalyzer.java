/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.representation.*;

/**
 * Utility analysis that detects loop-localized array zones,
 * and array zones completely swept by a loop.
 **/
public final class LoopArrayAccessAnalyzer extends DataFlowAnalyzer {

    /**
     * Constants that code for actions "read", "write", and "no-action".
     */
    public static final int NOACT = 0;
    public static final int WRITE = 1;
    public static final int READ = -1;
    /**
     * For each HeaderBlock, collects (zone per zone) the
     * variable references made inside the corresponding loop.
     */
    private BlockStorage<TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>>[]> accesses;
    /**
     * For each HeaderBlock, collects the BoolVector of all declared
     * zones that are overwritten by this loop's body.
     */
    private BlockStorage<BoolVector> writtenZones;

    public LoopArrayAccessAnalyzer(CallGraph cg) {
        super(cg, "Analysis of array accesses in loops", null);
    }

    public static void runAnalysis(CallGraph callGraph, TapList<Unit> rootUnits) {
        new LoopArrayAccessAnalyzer(callGraph).run(rootUnits);
    }

    /**
     * Runs detection of loop-localized array zones on all Units.
     * As a result, fills the "localizedZones" field
     * of each clean DO-loop HeaderBlock.
     */
    @Override
    public void run(TapList<Unit> rootUnits) {
        runBottomUpAnalysis(rootUnits);
    }

    @Override
    protected Object initializeCGForUnit() {
        return null;
    }

// boolean traceHere = false ;

    @Override
    public boolean analyze() {
// traceHere = "b2stbc_phys".equals(curUnit.name()) ;
// Chrono ccc = null ;
// if (traceHere) {
// System.out.println("ANALYZE "+curUnit) ;
// ccc = new Chrono();
// }
        Block block;
        TapList<Block> inAllBlocks = curUnit.allBlocks;
        TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>>[] thisLoopAccesses;
        // initialize each loop's array "accesses" that will
        // collect accesses zone per zone:
        accesses = new BlockStorage<>(curUnit);
        writtenZones = new BlockStorage<>(curUnit);
        Tree loopIndex;
        BoolVector allWrittenZones;
        TapIntList loopIndexZones;
        TapIntList localizedZones;
        boolean loopIndexIsDirty;
        while (inAllBlocks != null) {
            curBlock = inAllBlocks.head;
            if (curBlock instanceof HeaderBlock && curBlock.isACleanDoLoop()) {
                nDZ = curBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                accesses.store(curBlock, (TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>>[]) new TapList[nDZ]);
                writtenZones.store(curBlock, new BoolVector(nDZ));
            }
            inAllBlocks = inAllBlocks.tail;
        }
        // Sweep through all Blocks and fill the enclosing loops' arrays:
        inAllBlocks = curUnit.allBlocks;
        while (inAllBlocks != null) {
            setCurBlockEtc(inAllBlocks.head);
            collectAccesses();
            inAllBlocks = inAllBlocks.tail;
        }
// if (traceHere) System.out.println(" TTT1:"+ccc.elapsed()) ;
        // Decide from "accesses" the localizedZones of each HeaderBlock:
        inAllBlocks = curUnit.allBlocks;
        while (inAllBlocks != null) {
            setCurBlockEtc(inAllBlocks.head);
            if (curBlock instanceof HeaderBlock) {
                localizedZones = null;
                if (curBlock.isACleanDoLoop()) {
                    loopIndex = curBlock.headInstr().tree.down(3).down(1);
                    loopIndexZones =
                            curSymbolTable.listOfZonesOfValue(loopIndex, null, curBlock.headInstr());
                    allWrittenZones = writtenZones.retrieve(curBlock);
                    loopIndexIsDirty =
                            intersectsZoneRks(allWrittenZones, loopIndexZones, null);
                    if (!loopIndexIsDirty) {
                        thisLoopAccesses = accesses.retrieve(curBlock);
                        for (int i=nDZ-1 ; i>=0 ; --i) {
                            if (isLocalizedZone(loopIndex, thisLoopAccesses[i],
                                    allWrittenZones, curSymbolTable)) {
                                localizedZones =
                                        new TapIntList(i, localizedZones);
                            }
                        }
                    }
                }
                ((HeaderBlock) curBlock).localizedZones = localizedZones;
            }
            inAllBlocks = inAllBlocks.tail;
        }
// if (traceHere) System.out.println(" TTT2:"+ccc.elapsed()) ;
// traceHere = false ;
        // Now decide the "uniqueAccessZones" and "completely accessed zones" of each HeaderBlock:
        TapList<HeaderBlock> commonLocalized;
        BoolVector thisLoopWrittenZones;
        BoolVector loopConstantZones;
        Tree uniqueAccess;
        Tree oneAccess;
        boolean isUniqueAccess;
        boolean accumulateInside;
        TapList<HeaderBlock> headersInside;
        TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>> zoneAccesses;
        HeaderBlock headerBlockAround;
        TapPair<WrapperTypeSpec, TapList<HeaderBlock>> context;
        TapIntList uniqueAccessZones;
        TapIntList topUniqueAccessZones;
        TapList<Tree> uniqueAccessTrees;
        TapIntList loopCompleteZones;
        inAllBlocks = curUnit.allBlocks;
        // Algorithm: top-down on the tree of nested loops,...
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            if (block instanceof HeaderBlock) {
                setCurBlockEtc(block);
                //...the "uniqueAccessZones" of a loop contain those of the enclosing loop...
                uniqueAccessZones = null;
                uniqueAccessTrees = null;
                if (block.enclosingLoop().enclosingLoop() != null) {
                    uniqueAccessZones =
                            block.enclosingLoop().enclosingLoop().header().uniqueAccessZones;
                    uniqueAccessTrees =
                            block.enclosingLoop().enclosingLoop().header().uniqueAccessTrees;
                }
                topUniqueAccessZones = null;
                loopCompleteZones = null;
                if (block.isACleanDoLoop()) {
                    //...dirty loops cannot create new "uniqueAccess" zones...
                    thisLoopAccesses = accesses.retrieve(block);
                    thisLoopWrittenZones = writtenZones.retrieve(block);
                    for (int i=nDZ-1 ; i>=0 ; --i) {
                        if (!TapIntList.contains(uniqueAccessZones, i)) {
                            //...for each zone that has not already inherited "uniqueAccess"
                            // from its enclosing loop...
                            commonLocalized = getCommonCleanLocalizedHeaders(thisLoopAccesses[i],
                                    (HeaderBlock) block, i);
                            loopConstantZones = thisLoopWrittenZones.not();
                            accumulateInside = true;
                            headersInside = null;
                            //...add to the set of loop constants the indices of all surrounding
                            // clean do-loops such that this zone i is local to this do-loop...
                            while (commonLocalized != null) {
                                headerBlockAround = commonLocalized.head;
                                if (accumulateInside) {
                                    headersInside =
                                            new TapList<>(headerBlockAround, headersInside);
                                }
                                if (headerBlockAround == block) {
                                    accumulateInside = false;
                                }
                                loopIndex = headerBlockAround.headInstr().tree.down(3).down(1);
                                loopIndexZones =
                                        headerBlockAround.symbolTable.listOfZonesOfValue(loopIndex, null, headerBlockAround.headInstr());
                                if (loopIndexZones != null && loopIndexZones.tail == null) {
                                    loopConstantZones.set(loopIndexZones.head, true);
                                }
                                commonLocalized = commonLocalized.tail;
                            }
                            //...and now with this augmented set of loop constants,
                            // check that all accesses really go to the same cell...
                            uniqueAccess = null;
                            isUniqueAccess = true;
                            zoneAccesses = thisLoopAccesses[i];
                            while (isUniqueAccess && zoneAccesses != null) {
                                oneAccess = zoneAccesses.head.first;
                                if (uniqueAccess == null) {
                                    uniqueAccess = oneAccess;
                                } else {
                                    isUniqueAccess = sameAccessTree(oneAccess, uniqueAccess);
                                }
                                zoneAccesses = zoneAccesses.tail;
                            }
                            if (isUniqueAccess && uniqueAccess != null && isIndexedAccess(uniqueAccess)
                                    && allIndicesConstant(uniqueAccess, loopConstantZones, curSymbolTable, block.headInstr())) {
                                uniqueAccessZones = new TapIntList(i, uniqueAccessZones);
                                uniqueAccessTrees = new TapList<>(uniqueAccess, uniqueAccessTrees);
                                // mark the topmost loop for which accesses to zone i are "unique-access"
                                topUniqueAccessZones = new TapIntList(i, topUniqueAccessZones);
                                //...finally, only for the topmost uniqueAccess loop, find whether
                                // the access is complete i.e. after all iterations, all memory cells
                                // of zone i will have been swept.
                                context = new TapPair<>(null, headersInside);
                                if (isCompleteAccess(uniqueAccess, context, curSymbolTable)
                                        && context.first != null
                                        && context.first.getAllDimensions() == null) {
                                    loopCompleteZones = new TapIntList(i, loopCompleteZones);
                                }
                            }
                        }
                    }
                }
                ((HeaderBlock) block).uniqueAccessZones = uniqueAccessZones;
                ((HeaderBlock) block).uniqueAccessTrees = uniqueAccessTrees;
                ((HeaderBlock) block).topUniqueAccessZones = topUniqueAccessZones;
                ((HeaderBlock) block).loopCompleteZones = loopCompleteZones;
            }
            inAllBlocks = inAllBlocks.tail;
        }
        setCurBlockEtc(null);
        // We don't want a second sweep on the call graph:
        return false;
    }

    private void collectAccesses() {
        TapList<LoopBlock> enclosingLoops = curBlock.enclosingLoops();
        //When on a do-header Block, remove it from the enclosingLoops:
        if (enclosingLoops != null && enclosingLoops.head.header() == curBlock
                && curBlock.isACleanDoLoop()) {
            enclosingLoops = enclosingLoops.tail;
        }
        TapList<Instruction> instructions;
        Instruction instruction;
        if (enclosingLoops != null) {
            instructions = curBlock.instructions;
            while (instructions != null) {
                instruction = instructions.head;
                collectAccessesInTree(instruction.tree, NOACT,
                        instruction, enclosingLoops, curSymbolTable);
                instructions = instructions.tail;
            }
        }
    }

    /**
     * Sweeps through expression "expr", which is only read if "act" is READ,
     * written if "act" is WRITE, accessed but not used if "act" is NOACT.
     * For each sub-Tree which is a variable use,
     * for each enclosing loop in "enclosingLoops", adds into "accesses" that
     * this sub-tree is an access that occurs inside the enclosing loop.
     * Each time a variable may be overwritten and this may affect some loop
     * index, remember it, so that accesses that use this loop index make the
     * accessed variable "not localized".
     */
    private void collectAccessesInTree(Tree expr, int act,
                                       Instruction instruction,
                                       TapList<LoopBlock> enclosingLoops, SymbolTable symbolTable) {
        switch (expr.opCode()) {
            case ILLang.op_assign:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(1), WRITE,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(1), WRITE,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expr, symbolTable);
                Tree[] args = ILUtils.getArguments(expr).children();
                for (int i = args.length - 1; i >= 0; i--) {
                    collectAccessesInTree(args[i], WRITE, instruction,
                            enclosingLoops, symbolTable);
                }
                //When the calledUnit is sufficiently known, take care of commons:
                if (calledUnit != null && !calledUnit.isIntrinsic()) {
                        for (int i=calledUnit.paramElemsNb()-1 ; i>=0 ; --i) {
                            ZoneInfo zi = calledUnit.paramElemZoneInfo(i) ;
                            if (zi!=null && zi.kind()==SymbolTableConstants.GLOBAL) {
                                collectOneAccessForZones(expr, new TapIntList(zi.zoneNb, null),
                                                         WRITE, enclosingLoops, instruction) ;
                            }
                        }
                }
                break;
            }
            case ILLang.op_constructorCall: {
                Tree[] args = expr.down(3).children();
                for (int i = args.length - 1; i >= 0; i--) {
                    collectAccessesInTree(args[i], READ, instruction,
                            enclosingLoops, symbolTable);
                }
                break;
            }
            case ILLang.op_unary:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_arrayDeclarator: {
                Tree[] indexes = expr.down(2).children();
                for (int i = indexes.length - 1; i >= 0; i--) {
                    collectAccessesInTree(indexes[i], READ,
                            instruction, enclosingLoops, symbolTable);
                }
                //WARNING: this case continues into next case:
            }
            case ILLang.op_fieldAccess:
                collectAccessesInTree(expr.down(1), NOACT,
                        instruction, enclosingLoops, symbolTable);
                //WARNING: this case continues into next case:
            case ILLang.op_ident:
                if (act != NOACT) {
                    collectOneAccess(expr, act, instruction,
                            enclosingLoops, symbolTable);
                }
                break;
            case ILLang.op_pointerAccess:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                if (act != NOACT) {
                    collectOneAccess(expr, act, instruction,
                            enclosingLoops, symbolTable);
                }
                break;
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_complexConstructor:
            case ILLang.op_return:
            case ILLang.op_dimColon:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_ifExpression:
            case ILLang.op_arrayTriplet:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(3), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_switch:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_nameEq:
                collectAccessesInTree(expr.down(2), act,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
                collectAccessesInTree(expr.down(1), WRITE,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_loop:
                collectAccessesInTree(expr.down(3), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_ioCall: {
                String ioOper = ILUtils.getIdentString(expr.down(1));
                Tree[] ioSpecs = expr.down(2).children();
                Tree targets;
                ToBool written = new ToBool(false);
                ToBool totally = new ToBool(false);
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    targets = InOutAnalyzer.getIOeffectOfIOspec(ioSpecs[i], i, ioOper,
                            written, totally, symbolTable);
                    collectAccessesInTree(targets, written.get() ? WRITE : READ,
                            instruction, enclosingLoops, symbolTable);
                }
                collectAccessesInTree(expr.down(3), ILUtils.isIORead(expr) ? WRITE : READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            }
            case ILLang.op_iterativeVariableRef:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(1), act,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] exprs = expr.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    collectAccessesInTree(exprs[i], act,
                            instruction, enclosingLoops, symbolTable);
                }
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] exprs = expr.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    collectAccessesInTree(exprs[i], READ,
                            instruction, enclosingLoops, symbolTable);
                }
                break;
            }
            case ILLang.op_forall:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(3), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(4), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(1), WRITE,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_forallRangeSet:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_for:
                collectAccessesInTree(expr.down(1), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(2), READ,
                        instruction, enclosingLoops, symbolTable);
                collectAccessesInTree(expr.down(3), WRITE,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_varDeclaration:
                Tree decl;
                for (int i = 1; i <= expr.down(3).length(); ++i) {
                    decl = expr.down(3).down(i);
                    collectAccessesInTree(decl, READ, instruction, enclosingLoops, symbolTable);
                }
                break;
            case ILLang.op_pointerDeclarator:
                collectAccessesInTree(expr.down(1), act,
                        instruction, enclosingLoops, symbolTable);
                break;
            case ILLang.op_allocate:
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_bitCst:
            case ILLang.op_stop:
            case ILLang.op_continue:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_strings:
            case ILLang.op_formatElem:
            case ILLang.op_address:
            case ILLang.op_break:
            case ILLang.op_pointerType:
            case ILLang.op_sizeof:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_include:
                break;
            default:
                TapEnv.toolWarning(-1, "(Localized zones analysis) Unexpected operator: " + expr.opName());
                break;
        }
    }

    private void collectOneAccess(Tree ref, int act, Instruction instruction,
                                  TapList<LoopBlock> enclosingLoops, SymbolTable symbolTable) {
        collectOneAccessForZones(ref,
                symbolTable.listOfZonesOfValue(ref, null, instruction),
                act, enclosingLoops, instruction);
    }

    private void collectOneAccessForZones(Tree ref, TapIntList zones, int act,
                                          TapList<LoopBlock> enclosingLoops, Instruction instruction) {
        HeaderBlock enclosingHeader;
        TapIntList inZones;
        TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>>[] thisLoopAccesses;
        BoolVector allWrittenZones;
        TapTriplet<Tree, Instruction, TapList<LoopBlock>> refAndLoops = new TapTriplet<>(ref, instruction, enclosingLoops);
        while (enclosingLoops != null) {
            enclosingHeader = enclosingLoops.head.header();
            if (enclosingHeader.isACleanDoLoop()) {
                thisLoopAccesses = accesses.retrieve(enclosingHeader);
                if (thisLoopAccesses != null) {
                    // thisLoopAccesses is left null for unclean non-do loops
                    inZones = zones;
                    while (inZones != null) {
                        // We may go up to a scope where one of "zones" is undefined:
                        if (inZones.head < enclosingHeader.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) {
                            thisLoopAccesses[inZones.head] =
                                    new TapList<>(refAndLoops, thisLoopAccesses[inZones.head]);
                        }
                        inZones = inZones.tail;
                    }
                }
                if (act == WRITE) {
                    allWrittenZones =
                            writtenZones.retrieve(enclosingHeader);
                    // allWrittenZones may be null for unclean non-do loops
                    if (allWrittenZones != null) {
                        inZones = zones;
                        while (inZones != null) {
                            // We may go up to a scope where one of "zones" is undefined:
                            if (inZones.head < enclosingHeader.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) {
                                allWrittenZones.set(inZones.head, true);
                            }
                            inZones = inZones.tail;
                        }
                    }
                }
            }
            enclosingLoops = enclosingLoops.tail;
        }
    }

    /**
     * @return true iff all accesses to this zone, which are gathered
     * in the TapList of Tree's "accesses", show that this zone is
     * accessed as localized by the current loop (of index "index").
     * "allWrittenZones" contains the BoolVector of all zones possibly
     * overwritten during one iteration of the current loop.
     * Method: the result is true if all accesses are clean:
     * no op_call or other side-effect accesses, all accesses are
     * combinations of arrayAccesses and fieldAccesses on the same
     * variable, in the same order, and there is one of these array
     * indexes (always the same) that uses "index" in a regular way
     * i.e. such that if "index" changes, the reference changes too.
     */
    private boolean isLocalizedZone(Tree index, TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>> accesses,
                                    BoolVector allWrittenZones, SymbolTable symbolTable) {
        TapPair<Tree, Tree> referencePair = new TapPair<>(null, null);
        TapIntList referenceRk = new TapIntList(-1, null);
        TapPair<Tree, Tree> thisPair = new TapPair<>(null, null);
        TapIntList thisRk = new TapIntList(-1, null);
        boolean cleanAccesses = true;
        TapTriplet<Tree, Instruction, TapList<LoopBlock>> refAndLoops;
        while (cleanAccesses && accesses != null) {
            thisPair.first = null;
            thisPair.second = null;
            thisRk.head = -1;
            refAndLoops = accesses.head;
            cleanAccesses = canBeLocalizedAccess(refAndLoops.first,
                    refAndLoops.second, index,
                    thisPair, thisRk, 0,
                    allWrittenZones, symbolTable);
            cleanAccesses = cleanAccesses && thisRk.head != -1;
            if (cleanAccesses) {
                if (referenceRk.head == -1) {
                    referenceRk.head = thisRk.head;
                    referencePair.first = thisPair.first;
                    referencePair.second = thisPair.second;
                } else {
                    cleanAccesses =
                        (referenceRk.head == thisRk.head
                         && referencePair.first.equalsTree(thisPair.first)
                         && isSameIndexExpr(referencePair.second, thisPair.second)) ;
                }
            }
            accesses = accesses.tail;
        }
        return cleanAccesses && referenceRk.head != -1;
    }

    private boolean canBeLocalizedAccess(Tree access, Instruction accessInstr, Tree index,
                                         TapPair<Tree, Tree> resultPair, TapIntList resultRk, int rk,
                                         BoolVector allWrittenZones, SymbolTable symbolTable) {
        switch (access.opCode()) {
            case ILLang.op_ident:
                resultPair.first = access;
                return true;
            case ILLang.op_arrayAccess:
                Tree[] indexes = access.down(2).children();
                if (resultPair.second == null) {
                    for (int i = indexes.length - 1; i >= 0; i--) {
                        if (isRegularIndexUse(indexes[i], accessInstr, index,
                                allWrittenZones, symbolTable)) {
                            resultPair.second = indexes[i];
                            resultRk.head = rk + i;
                        }
                    }
                }
                return canBeLocalizedAccess(access.down(1), accessInstr, index, resultPair,
                        resultRk, rk + indexes.length,
                        allWrittenZones, symbolTable);
            case ILLang.op_fieldAccess:
                return canBeLocalizedAccess(access.down(1), accessInstr, index, resultPair,
                        resultRk, rk,
                        allWrittenZones, symbolTable);
            default:
                return false;
        }
    }

    private boolean isRegularIndexUse(Tree index, Instruction accessInstr, Tree loopIndex,
                                      BoolVector allWrittenZones, SymbolTable symbolTable) {
        TapPair<Tree, Tree> factorAndConstant = new TapPair<>(null, null);
        matchLinearForm(index, loopIndex, factorAndConstant);
        Tree factor = factorAndConstant.first;
        Tree constant = factorAndConstant.second;
        boolean regular = false;
        if (!ILUtils.evalsToZero(factor)) {
            //[llh] 4th argument curInstruction=null for listOfZonesOfValue ?
            TapIntList loopIndexZones = symbolTable.listOfZonesOfValue(loopIndex, null, null);
            BoolVector factorZones = symbolTable.zonesUsedByExp(factor, accessInstr);
            regular = !intersectsZoneRks(factorZones, loopIndexZones, null)
                      && !factorZones.intersects(allWrittenZones, nDZ);
            if (regular && constant != null) {
                BoolVector constantZones = symbolTable.zonesUsedByExp(constant, accessInstr);
                regular = !intersectsZoneRks(constantZones, loopIndexZones, null)
                          && !constantZones.intersects(allWrittenZones, nDZ);
            }
        }
        return regular;
    }

    /**
     * Tries to force expression "expr" into the shape
     * A*variable+B, where A and B should not contain "variable".
     * Since this is not always possible,
     * A and B may turn out to contain "variable"!
     * "factorAndConstant" is an TapPair that contains the final A and B
     */
    private void matchLinearForm(Tree expr, Tree variable,
                                 TapPair<Tree, Tree> factorAndConstant) {
        TapPair<Tree, Tree> factorAndConstant1;
        TapPair<Tree, Tree> factorAndConstant2;
        Tree tmpTree;
        switch (expr.opCode()) {
            case ILLang.op_mul:
                factorAndConstant1 = new TapPair<>(null, null);
                matchLinearForm(expr.down(1), variable, factorAndConstant1);
                factorAndConstant2 = new TapPair<>(null, null);
                matchLinearForm(expr.down(2), variable, factorAndConstant2);
                if (factorAndConstant1.first != null) {
                    if (factorAndConstant2.first != null) {
                        tmpTree =
                                ILUtils.buildSmartMulDiv(factorAndConstant1.first, 1,
                                        factorAndConstant2.first);
                        tmpTree = ILUtils.build(ILLang.op_mul,
                                ILUtils.copy(variable), tmpTree);
                        factorAndConstant.first =
                                ILUtils.buildSmartAddSub(factorAndConstant.first, 1, tmpTree);
                    }
                    if (factorAndConstant2.second != null) {
                        tmpTree =
                                ILUtils.buildSmartMulDiv(factorAndConstant1.first, 1,
                                        factorAndConstant2.second);
                        factorAndConstant.first =
                                ILUtils.buildSmartAddSub(factorAndConstant.first, 1, tmpTree);
                    }
                }
                if (factorAndConstant1.second != null) {
                    if (factorAndConstant2.first != null) {
                        tmpTree =
                                ILUtils.buildSmartMulDiv(factorAndConstant1.second, 1,
                                        factorAndConstant2.first);
                        factorAndConstant.first =
                                ILUtils.buildSmartAddSub(factorAndConstant.first, 1, tmpTree);
                    }
                    if (factorAndConstant2.second != null) {
                        factorAndConstant.second =
                                ILUtils.buildSmartMulDiv(factorAndConstant1.second, 1,
                                        factorAndConstant2.second);
                    }
                }
                break;
            case ILLang.op_add:
                matchLinearForm(expr.down(1), variable, factorAndConstant);
                factorAndConstant2 = new TapPair<>(null, null);
                matchLinearForm(expr.down(2), variable, factorAndConstant2);
                factorAndConstant.first =
                        ILUtils.buildSmartAddSub(factorAndConstant.first, 1,
                                factorAndConstant2.first);
                factorAndConstant.second =
                        ILUtils.buildSmartAddSub(factorAndConstant.second, 1,
                                factorAndConstant2.second);
                break;
            case ILLang.op_sub:
                matchLinearForm(expr.down(1), variable, factorAndConstant);
                factorAndConstant2 = new TapPair<>(null, null);
                matchLinearForm(expr.down(2), variable, factorAndConstant2);
                factorAndConstant.first =
                        ILUtils.buildSmartAddSub(factorAndConstant.first, -1,
                                factorAndConstant2.first);
                factorAndConstant.second =
                        ILUtils.buildSmartAddSub(factorAndConstant.second, -1,
                                factorAndConstant2.second);
                break;
            case ILLang.op_minus:
                matchLinearForm(expr.down(1), variable, factorAndConstant);
                if (factorAndConstant.first != null) {
                    factorAndConstant.first =
                            ILUtils.minusTree(factorAndConstant.first);
                }
                if (factorAndConstant.second != null) {
                    factorAndConstant.second =
                            ILUtils.minusTree(factorAndConstant.second);
                }
                break;
            default:
                if (expr.equalsTree(variable)) {
                    factorAndConstant.first =
                            ILUtils.build(ILLang.op_intCst, 1);
                } else {
                    factorAndConstant.second =
                            ILUtils.copy(expr);
                }
                break;
        }
    }

    private boolean isSameIndexExpr(Tree index1, Tree index2) {
        return ILUtils.evalsToZero(ILUtils.subTree(index1, index2));
    }

    /**
     * @return this sub-list in bottom-up order.
     */
    private TapList<HeaderBlock> getCommonCleanLocalizedHeaders(TapList<TapTriplet<Tree, Instruction, TapList<LoopBlock>>> refs,
                                                                HeaderBlock containerHeader,
                                                                int arrayZone) {
        if (refs == null) {
            return null;
        }
        TapList<LoopBlock> commonLoops = refs.head.third;
        TapList<LoopBlock> newLoops;
        HeaderBlock commonHeader;
        refs = refs.tail;
        while (refs != null) {
            newLoops = refs.head.third;
            while (newLoops != null &&
                    !TapList.contains(commonLoops, newLoops.head)) {
                newLoops = newLoops.tail;
            }
            commonLoops = newLoops;
            refs = refs.tail;
        }
        TapList<HeaderBlock> hdResult = new TapList<>(null, null);
        TapList<HeaderBlock> tlResult = hdResult;
        boolean outsideContainer = false;
        while (commonLoops != null) {
            commonHeader = commonLoops.head.header();
            if (commonHeader.isACleanDoLoop() &&
                    (outsideContainer || TapIntList.contains(commonHeader.localizedZones, arrayZone))) {
                tlResult = tlResult.placdl(commonHeader);
            } else {
                hdResult = tlResult;
            }
            if (commonHeader == containerHeader) {
                outsideContainer = true;
            }
            commonLoops = commonLoops.tail;
        }
        return hdResult.tail;
    }

    private boolean sameAccessTree(Tree expr, Tree expr2) {
        switch (expr.opCode()) {
            case ILLang.op_ident:
                return expr.equalsTree(expr2);
            case ILLang.op_arrayAccess: {
                boolean same = expr2.opCode() == ILLang.op_arrayAccess
                        && sameAccessTree(expr.down(1), expr2.down(1));
                if (same) {
                    Tree[] sons = expr.down(2).children();
                    Tree[] sons2 = expr2.down(2).children();
                    int len = sons.length;
                    if (same = sons2.length == len) {
                        int i = len - 1;
                        while (same && i >= 0) {
                            same = isSameIndexExpr(sons[i], sons2[i]);
                            i--;
                        }
                    }
                }
                return same;
            }
            case ILLang.op_fieldAccess:
                return expr2.opCode() == ILLang.op_fieldAccess
                        && expr.down(2).equalsTree(expr2.down(2))
                        && sameAccessTree(expr.down(1), expr2.down(1));
            default:
                return false;
        }
    }

    private boolean isIndexedAccess(Tree expr) {
        switch (expr.opCode()) {
            case ILLang.op_ident:
                return false;
            case ILLang.op_arrayAccess:
                return true;
            case ILLang.op_fieldAccess:
                return isIndexedAccess(expr.down(1));
            default:
                return false;
        }
    }

    private boolean allIndicesConstant(Tree expr, BoolVector loopConstantZones,
                                       SymbolTable symbolTable, Instruction headerInstr) {
        switch (expr.opCode()) {
            case ILLang.op_ident:
                return true;
            case ILLang.op_arrayAccess: {
                boolean allConstant = allIndicesConstant(expr.down(1), loopConstantZones, symbolTable, headerInstr);
                Tree[] indexes = expr.down(2).children();
                for (int i = indexes.length - 1; i >= 0; i--) {
                    allConstant = allConstant &&
                            indexIsConstant(indexes[i], loopConstantZones, symbolTable, headerInstr);
                }
                return allConstant;
            }
            case ILLang.op_fieldAccess:
                return allIndicesConstant(expr.down(1), loopConstantZones, symbolTable, headerInstr);
            default:
                return false;
        }
    }

    private boolean indexIsConstant(Tree expr, BoolVector loopConstantZones,
                                    SymbolTable symbolTable, Instruction headerInstr) {
        BoolVector exprZones = symbolTable.zonesUsedByExp(expr, headerInstr);
        return loopConstantZones.contains(exprZones, nDZ);
    }

    /**
     * @return true when the expression "expr" certainly accesses ALL cells of
     * its memory zone when the loops that command its indices sweep through
     * their iteration space. This is used for the simplified array-kill analysis.
     * The "context" is a bottom-up info, a TapPair whose head is the
     * WrapperTypeSpec of the expression and the tail is the list of all HeaderBlocks
     * that control the indices and that have not yet been used by the expression.
     */
    private boolean isCompleteAccess(Tree expr, TapPair<WrapperTypeSpec, TapList<HeaderBlock>> context, SymbolTable symbolTable) {
        switch (expr.opCode()) {
            case ILLang.op_ident:
                context.first = symbolTable.typeOf(expr);
                return true;
            case ILLang.op_arrayAccess: {
                boolean isComplete = isCompleteAccess(expr.down(1), context, symbolTable);
                WrapperTypeSpec type = context.first;
                Tree[] indexes = expr.down(2).children();
                if (isComplete && TypeSpec.isA(type, SymbolTableConstants.ARRAYTYPE)) {
                    ArrayTypeSpec arrayType = (ArrayTypeSpec) type.wrappedType;
                    int len = arrayType.dimensions().length;
                    if (indexes.length == len) {
                        for (int i = len - 1; i >= 0; i--) {
                            isComplete = isComplete
                                    && indexIsComplete(indexes[i], arrayType.dimensions()[i],
                                    context);
                        }
                    }
                    context.first = arrayType.elementType();
                    return isComplete;
                } else {
                    return false;
                }
            }
            case ILLang.op_fieldAccess: {
                boolean isComplete = isCompleteAccess(expr.down(1), context, symbolTable);
                WrapperTypeSpec type = context.first;
                if (isComplete && TypeSpec.isA(type, SymbolTableConstants.COMPOSITETYPE)) {
                    context.first =
                            ((CompositeTypeSpec) type.wrappedType).getFieldType(expr.down(2));
                    return true;
                } else {
                    return false;
                }
            }
            default:
                return false;
        }
    }

    /**
     * @return true when index expression "expr" ranges through ALL
     * elements in the current "dimension" when one of the loops in
     * the (tail of the) current context.
     * Searches through the context for a loop whose index is used
     * in "expr". When it finds one, removes it from the context.
     * Then checks that this "expr" combined with the iteration space
     * covers completely the "dimension".
     */
    private boolean indexIsComplete(Tree expr, ArrayDim dimension,
                                    TapPair<WrapperTypeSpec, TapList<HeaderBlock>> context) {
        boolean foundLoop = false;
        boolean result = false;
        HeaderBlock headerBlock;
        Tree loopIndex;
        Tree loopFrom;
        Tree loopTo;
        Tree loopStride;
        Tree coeff;
        Tree offset;
        int icoeff;
        int istride;
        Tree dimFrom;
        Tree dimTo;
        //Final Difference of Bounds. Must eval to 0 in the end.
        Tree db1;
        Tree db2;
        if (dimension.tree() == null || dimension.tree().opCode() != ILLang.op_dimColon) {
            return false;
        }
        dimFrom = ILUtils.copy(dimension.tree().down(1));
        dimTo = ILUtils.copy(dimension.tree().down(2));
        if (ILUtils.isNullOrNone(dimFrom)) {
            dimFrom = ILUtils.build(ILLang.op_intCst, 1);
        }
        if (ILUtils.isNullOrNone(dimTo) || dimTo.opCode() == ILLang.op_star) {
            return false;
        }
        while (!foundLoop && context.second != null) {
            headerBlock = context.second.head;
            loopIndex = headerBlock.headInstr().tree.down(3).down(1);
            loopFrom = headerBlock.headInstr().tree.down(3).down(2);
            loopTo = headerBlock.headInstr().tree.down(3).down(3);
            loopStride = headerBlock.headInstr().tree.down(3).down(4);
            TapPair<Tree, Tree> factorAndConstant = new TapPair<>(null, null);
            matchLinearForm(expr, loopIndex, factorAndConstant);
            if (factorAndConstant.first != null) {
                foundLoop = true;
                context.second = context.second.tail;
                coeff = factorAndConstant.first;
                if (ILUtils.evalsToOne(coeff)) {
                    icoeff = 1;
                } else if (ILUtils.evalsToMinusOne(coeff)) {
                    icoeff = -1;
                } else {
                    icoeff = 0;
                }
                if (icoeff != 0) {
                    if (ILUtils.isNullOrNone(loopStride) ||
                            ILUtils.evalsToOne(loopStride)) {
                        istride = 1;
                    } else if (ILUtils.evalsToMinusOne(loopStride)) {
                        istride = -1;
                    } else {
                        istride = 0;
                    }
                    if (istride != 0) {
                        offset = factorAndConstant.second;
                        dimFrom = ILUtils.subTree(dimFrom, ILUtils.copy(offset));
                        dimTo = ILUtils.subTree(dimTo, ILUtils.copy(offset));
                        if (icoeff == 1) {
                            if (istride == 1) {
                                db1 = ILUtils.subTree(ILUtils.copy(loopFrom), dimFrom);
                                db2 = ILUtils.subTree(ILUtils.copy(loopTo), dimTo);
                            } else {
                                db1 = ILUtils.subTree(ILUtils.copy(loopTo), dimFrom);
                                db2 = ILUtils.subTree(ILUtils.copy(loopFrom), dimTo);
                            }
                        } else {
                            if (istride == 1) {
                                db1 = ILUtils.addTree(ILUtils.copy(loopTo), dimFrom);
                                db2 = ILUtils.addTree(ILUtils.copy(loopFrom), dimTo);
                            } else {
                                db1 = ILUtils.addTree(ILUtils.copy(loopFrom), dimFrom);
                                db2 = ILUtils.addTree(ILUtils.copy(loopTo), dimTo);
                            }
                        }
                        result = ILUtils.evalsToZero(db1) && ILUtils.evalsToZero(db2);
                    }
                }
            } else {
                context.second = context.second.tail;
            }
        }
        return result;
    }
}
