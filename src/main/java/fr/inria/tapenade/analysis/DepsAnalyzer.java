/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.BlockStorage;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.FGArrow;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * Analyzer in charge of the plain dependency analysis,
 * from every input to every output whose value may change
 * if the input's value changes.
 *
 * <pre>
 * Feature: derives from DataFlowAnalyzer : OK
 * Feature: able to analyze recursive code: OK
 * Feature: distinguishes structure elemts: OK
 * Feature: takes  care  of  pointer dests: OK
 * Feature: refine on loop-local variables: no
 * Feature: std method for call arguments : OK
 * Feature: able to analyze sub-flow-graph: OK
 * </pre>
 */
public final class DepsAnalyzer extends DataFlowAnalyzer {

    /**
     * For each Block in the currently analyzed Unit, the
     * BoolMatrix dependence matrix just before the Block, wrt
     * the initial values of the zones at the beginning of the current
     * Unit.
     */
    private BlockStorage<BoolMatrix> depsIn;

    /**
     * For each Block in the currently analyzed Unit, the
     * BoolMatrix dependence matrix just after the Block, wrt
     * the initial values of the zones at the beginning of the current
     * Unit.
     */
    private BlockStorage<BoolMatrix> depsOut;

    /**
     * For each Block in the currently analyzed Unit, the
     * BoolMatrix dependence matrix just after the Block, wrt
     * the values of the zones at the beginning of the Block.
     */
    private BlockStorage<BoolMatrix> depsThrough;

    /**
     * Temporary dependency matrices during propagation.
     */
    private BoolMatrix tmpDep;

    /**
     * Number of declared zones at the EntryBlock.
     */
    private int nUDZ = -9;

    /**
     * Creates a DepsAnalyzer, given the
     * CallGraph "cg" on which it will work.
     */
    private DepsAnalyzer(CallGraph cg) {
        super(cg, "Plain dependency analysis", TapEnv.traceDeps());
    }

    /**
     * Main entry point. Builds a Dependencies Analyzer, stores it and runs it.
     */
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<Unit> rootUnits) {
        TapEnv.setDepsAnalyzer(new DepsAnalyzer(callGraph));
        TapEnv.depsAnalyzer().run(rootUnits);
    }

    private static BoolMatrix buildDefaultExternalDependencies(Unit calledUnit) {
        BoolMatrix result ;
        BoolVector unitN = calledUnit.unitInOutN();
        BoolVector unitR = calledUnit.unitInOutPossiblyR();
        BoolVector unitW = calledUnit.unitInOutPossiblyW();
        // externalShape may be null for VarFunction (the units of
        // functions passed as argument) :
        if (calledUnit.hasParamElemsInfo() && unitW!=null) {
            int n = calledUnit.paramElemsNb();
            result = new BoolMatrix(n, n);
            result.setIdentity();
            BoolVector depLine = new BoolVector(n);
            TapIntList returnIndices = calledUnit.returnIndices(SymbolTableConstants.ALLKIND) ;
            boolean maybeDiffAndModified;
            for (int i = 0; i < n; ++i) {
                if (calledUnit.paramElemZoneInfo(i) != null) {
                    maybeDiffAndModified = (calledUnit.paramElemZoneInfo(i).zoneNb!=-1) ;
                } else {
                    maybeDiffAndModified = false;
                }
                if (maybeDiffAndModified && !TapIntList.contains(returnIndices, i) && unitR.get(i)) {
                    depLine.set(i, true);
                }
            }
            for (int i = 0; i < n; ++i) {
                if (calledUnit.paramElemZoneInfo(i) != null) {
                    maybeDiffAndModified = (calledUnit.paramElemZoneInfo(i).zoneNb!=-1) ;
                } else {
                    maybeDiffAndModified = false;
                }
                if (maybeDiffAndModified && unitW.get(i)) {
                    BoolVector depLineCopy = depLine.copy();
                    // Moreover, if zone may be not-read-nor-written,
                    //  then its output value depends also on its input value:
                    if (!TapIntList.contains(returnIndices, i) && unitN.get(i)) {
                        depLineCopy.set(i, true);
                    }
                    result.setRow(i, depLineCopy);
                }
            }
        } else {
            result = new BoolMatrix(0, 0);
        }
        return result;
    }

    protected static BoolMatrix buildDefaultIntrinsicDependencies(Unit calledUnit) {
        BoolMatrix result;
        BoolVector unitR = calledUnit.unitInOutPossiblyR();
        if (calledUnit.hasParamElemsInfo() && unitR!=null) {
            int n = calledUnit.paramElemsNb();
            result = new BoolMatrix(n, n);
            result.setIdentity();
            BoolVector depLine = new BoolVector(n);
            TapIntList returnIndices = calledUnit.returnIndices(SymbolTableConstants.ALLKIND) ;
            for (int i = 0; i < n; ++i) {
                boolean isInDep = calledUnit.paramElemZoneInfo(i).zoneNb != -1;
                if (isInDep && !TapIntList.contains(returnIndices, i) && unitR.get(i)) {
                    depLine.set(i, true);
                }
            }
            while (returnIndices!=null) {
                result.setRow(returnIndices.head, depLine);
                returnIndices = returnIndices.tail ;
            }
        } else {
            result = new BoolMatrix(0, 0);
        }
        return result;
    }


    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        if (unit == null || unit.publicSymbolTable() == null) {
            nUDZ = -9;
        } else {
            nUDZ = unit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
        }
    }

    /**
     * During initialization for plain dependency analysis on the
     * whole CallGraph, do whatever initialization is needed for "unit".
     */
    @Override
    protected Object initializeCGForUnit() {
        if (curUnit.isExternal() || curUnit.isVarFunction() || curUnit.isInterface()
                || curUnit.isRenamed() || curUnit.isModule()) {
            if (curUnit.plainDependencies==null) {
                curUnit.plainDependencies =
                    buildDefaultExternalDependencies(curUnit) ;
            }
        } else if (curUnit.isIntrinsic()) {
            if (curUnit.plainDependencies==null) {
                curUnit.plainDependencies =
                    buildDefaultIntrinsicDependencies(curUnit) ;
            }
        } else {
            curUnit.plainDependencies = null;
        }
        return null;
    }

    /**
     * Initialize just before plain dependency analysis on "curUnit".
     */
    @Override
    protected void initializeFGForUnit() {
        depsIn = new BlockStorage<>(curUnit);
        depsOut = new BlockStorage<>(curUnit);
        depsThrough = new BlockStorage<>(curUnit);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" =============  PLAIN DEPS ANALYSIS OF UNIT " + curUnit.name() + " : ============");
            curSymbolTable = curUnit.privateSymbolTable();
            if (curSymbolTable == null) {
                curSymbolTable = curUnit.publicSymbolTable();
            }
            nDZ = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            ZoneInfo zi;
            if (nDZ > 0) {
                TapEnv.printOnTrace(" D:");
            }
            for (int i=0 ; i<nDZ ; ++i) {
                zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zi != null) {
                    TapEnv.printOnTrace(" [" + i + "]" + zi.description);
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace();
        }
    }

    @Override
    protected void initializeFGForInitBlock() {
        BoolMatrix initIdentity = new BoolMatrix(nDZ, nDZ);
        initIdentity.setIdentity();
        TapIntList returnIndices = curUnit.returnIndices(SymbolTableConstants.ALLKIND) ;
        while (returnIndices!=null) {
            initIdentity.setRow(returnIndices.head, new BoolVector(nDZ)) ;
            returnIndices = returnIndices.tail ;
        }
        depsOut.store(curBlock, initIdentity);
    }

    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
    }

    /**
     * Initializes dependency info before propagation. Mostly, this
     * fills the depsThrough "BlockStorage", which is the
     * BoolMatrix plain dependence matrix just after each Block, wrt
     * the values of the zones at the beginning of the Block.
     */
    @Override
    protected void initializeFGForBlock() {
        BoolMatrix deps = new BoolMatrix(nDZ, nDZ);
        deps.setIdentity();
        TapList<Instruction> instructions = curBlock.instructions;
        ToBool arrives = new ToBool(true);
        while (instructions != null) {
            curInstruction = instructions.head;
            depsThroughExpression(deps, curInstruction.tree, arrives);
            instructions = instructions.tail;
        }
        if (!arrives.get())
            /* The curBlock is probably part of a recursive subgraph, a
             * function that it is calling hasn't been analyzed so we
             * suppose this curBlock is not reached for the moment, and
             * it follows that it has null dependencies   */ {
            deps = null;
        }
        curInstruction = null;
        depsThrough.store(curBlock, deps);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" == Plain Deps through " + curBlock + " is:");
            TapEnv.dumpBoolMatrixOnTrace(deps);
            TapEnv.printlnOnTrace();
        }
    }

    /**
     * Collects the data-flow information from the incoming flow arrows.
     */
    @Override
    protected boolean accumulateValuesFromUpstream() {
        tmpDep = accumulateDepIn(curBlock);
        return tmpDep != null;
    }

    /**
     * Compares data-flow info with the collected values of previous sweep.
     * returns true when something has changed and therefore another
     * Flow Graph sweep is  probably necessary.
     */
    @Override
    protected boolean compareUpstreamValues() {
        BoolMatrix depOld = depsIn.retrieve(curBlock);
        if (depOld == null || !tmpDep.equalsBoolMatrix(depOld)) {
            depsIn.store(curBlock, tmpDep);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Propagates the data-flow information forwards through "curBlock".
     */
    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        BoolMatrix depThrough = depsThrough.retrieve(curBlock);
        if (depThrough != null) {
            tmpDep.rows = depThrough.times(tmpDep).rows;
            return true;
        } else {
            tmpDep = null;
            return false;
        }
    }

    /**
     * Compares the downstream data-flow info
     * with the collected values of previous sweep.
     *
     * @return true when something has changed and therefore another
     * Flow Graph sweep is  probably necessary.
     */
    @Override
    protected boolean compareDownstreamValues() {
        BoolMatrix depOld = depsOut.retrieve(curBlock);
        if (depOld == null || !tmpDep.equalsBoolMatrix(depOld)) {
            depsOut.store(curBlock, tmpDep);
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        BoolMatrix refDep = depsOut.retrieve(refBlock);
        BoolVector refRow = refDep.getRow(mpZone);
        BoolVector newRow = tmpDep.getRow(mpZone);
        if (newRow != null && (refRow == null || !refRow.contains(newRow, nUDZ))) {
            refDep.overwriteDeps(new TapIntList(mpZone, null), newRow, false);
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected boolean terminateFGForUnit() {
        tmpDep = accumulateDepIn(curUnit.exitBlock);
        /* Clean the intermediate results for this curUnit: */
        depsIn = null;
        depsOut = null;
        depsThrough = null;
        // When this routine does not reach the end (e.g recursion),
        // this exit "tmpDep" is implicit-zero. In this case, we don't
        // want this curUnit to raise a recompute on all the call graph,
        // so we return false meaning this curUnit's result didn't change!
        if (tmpDep == null) {
            return false;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" == Plain Deps entering exit Block :");
            TapEnv.dumpBoolMatrixOnTrace(tmpDep);
            TapEnv.printlnOnTrace();
        }

        // Before terminating, we must add "static" dependencies coming from declarations:
        BoolVector row;
        for (int i = tmpDep.nRows - 1; i >= 0; i--) {
            row = tmpDep.getRow(i);
            if (row != null) {
                row.cumulOr(curUnit.zonesUsedInDeclarations) ;
            }
        }
        BoolMatrix oldPlainDep = curUnit.plainDependencies ;
        if (oldPlainDep!=null && tmpDep.equalsBoolMatrix(oldPlainDep)) {
            return false ;
        } else {
            curUnit.plainDependencies = tmpDep ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace(" == Plain Deps effect stored for unit " + curUnit.name() + " is:");
                TapEnv.dumpOnTrace(tmpDep);
                TapEnv.printlnOnTrace();
            }
            return true ;
        }
    }

    /**
     * Analyzes plain dependency through "expression".
     * "deps" is the BoolMatrix of the dependency matrix just
     * before evaluation of Tree "expression". This function returns
     * the tree of the BoolVector's upon which each of the zones
     * of the expression depends. This dependence is
     * expressed wrt the values at the origin of deps. It may also
     * return null, which means that "expression" does not depend on
     * any known zone. As a side-effect, this function pushes "deps"
     * through expression, i.e. when it terminates, "deps" represents
     * the dependency matrix just after evaluation of "expression".
     */
    private TapList depsThroughExpression(BoolMatrix deps, Tree expression, ToBool arrives) {
        switch (expression.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                TapList rhsDeps =
                        depsThroughExpression(deps, expression.down(2), arrives);
                /* A null rhsDeps means a dependency on nothing:
                 * we must now make it explicit: */
                if (rhsDeps == null) {
                    rhsDeps = new TapList<>(new BoolVector(nDZ), null);
                }
                BoolVector controlDeps = curUnit.buildControlDepsOf(curInstruction, curBlock) ;
                if (controlDeps!=null) orInfoPRZVTrees(controlDeps, rhsDeps);
                Tree lhs = expression.down(1);
                /* For the very rare case where the lhs contains
                 * expressions that modify deps...   */
                depsThroughExpression(deps, lhs, arrives);
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree) &&
                                expression.opCode() == ILLang.op_assign &&
                                //In Fortran, a initDeclarator is done only once
                                // and makes its declared-initialized variable "save":
                                !(curUnit.isFortran() && inADeclaration);
                if (TapEnv.traceCurAnalysis()) {
                    TapIntList rksList = ZoneInfo.listAllZones(writtenZonesTree, true);
                    TapEnv.printlnOnTrace("Upon " + ILUtils.toString(expression) + ", " + writtenZonesTree + " (=>rks:" + rksList + ") receives (total:" + totalAccess + ") " + rhsDeps.toString(new int[]{0, nDZ}));
                }
                setInfoPRZVTree(deps, writtenZonesTree, rhsDeps,
                                null, totalAccess, SymbolTableConstants.ALLKIND);
                return rhsDeps;
            }
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expression, curSymbolTable);
                BoolMatrix calleeDeps = getPlainDeps(calledUnit);
                if (calleeDeps == null) {
                    if (arrives != null) {
                        arrives.set(false);
                    }
                    return null;
                }
                calleeDeps = filterByValue(calleeDeps, SymbolTableConstants.ALLKIND, calledUnit);
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                TapList[] paramDeps = new TapList[1+actualParams.length];
                for (int i=actualParams.length ; i>0 ; --i) {
                    paramDeps[i] = depsThroughExpression(deps, actualParams[i-1], arrives);
                }

                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ---------- Plain Deps Analysis of procedure call " + ILUtils.toString(expression) + " : ----------");
                    TapEnv.printlnOnTrace("    incoming plain deps Upstream:");
                    TapEnv.dumpBoolMatrixOnTrace(deps);
                    for (int i=0 ; i<paramDeps.length ; ++i) {
                        TapEnv.printlnOnTrace("    paramDeps["+i+(i==0 ? "=result" : "")+"] = "+paramDeps[i]) ;
                    }
                    TapEnv.printlnOnTrace();
                }
                BoolVector passesThroughCall = new BoolVector(deps.nRows) ;
                translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), passesThroughCall, null, actualParams, true,
                                              expression, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                              null, null, false, false) ;
                BoolVector callSiteKilled = new BoolVector(deps.nRows) ;
                translateCalleeDataToCallSite(calledUnit.unitInOutCertainlyW(), callSiteKilled, null, actualParams, true,
                                              expression, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                              null, null, false, true) ;
                BoolMatrix depsOnCallee =
                    translateCallSiteDataToCallee(deps, paramDeps, null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND) ;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    propagated on callee entry:");
                    TapEnv.dumpOnTrace(depsOnCallee);
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    composed with callee plain Deps:");
                    TapEnv.dumpOnTrace(calleeDeps);
                    TapEnv.printlnOnTrace();
                }
                depsOnCallee = calleeDeps.times(depsOnCallee) ;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("    gives on callee exit:");
                    TapEnv.dumpOnTrace(depsOnCallee);
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    passesThroughCall: "+passesThroughCall) ;
                    TapEnv.printlnOnTrace("    passesAroundCall:  "+callSiteKilled.not()) ;
                    TapEnv.printlnOnTrace();
                }
                TapList[] paramDataS = translateCalleeDataToCallSite(depsOnCallee, deps, null, actualParams, false,
                                                                     expression, curInstruction, arrow,
                                                                     SymbolTableConstants.ALLKIND,
                                                                     passesThroughCall, callSiteKilled.not(), false, null) ;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("    outgoing plain deps Downstream:") ;
                    TapEnv.dumpOnTrace(deps) ;
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    and outgoing for result:"+paramDataS[0]) ;
                }
                return paramDataS[0];
            }
            case ILLang.op_fieldAccess: {
                TapList valueDependency =
                        depsThroughExpression(deps, expression.down(1), arrives);
                Object result = TapList.nth(valueDependency,
                        ILUtils.getFieldRank(expression.down(2)));
                if (result instanceof BoolVector) {
                    return new TapList<>(result, null);
                } else {
                    return (TapList) result;
                }
            }
            case ILLang.op_arrayAccess: {
                Tree[] indexes = expression.down(2).children();
                BoolVector indexesDep = new BoolVector(nDZ);
                for (int i = indexes.length - 1; i >= 0; i--) {
                    cumulOr(depsThroughExpression(deps, indexes[i], arrives), indexesDep, false);
                }
                TapList result = depsThroughExpression(deps, expression.down(1), arrives);
                orInfoPRZVTrees(indexesDep, result);
                return result;
            }
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_bitfieldDeclarator:
                // These op_*Declarator trees occur only in declarations.
                // The part that we must analyze is only the possible assignment to
                // the root variable of the declarator:
                return null;
            case ILLang.op_ident: {
                TapList zones = curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null);
                if (zones == null) {
                    return null;
                }
                zones = TapList.copyTree(zones);
                includePointedElementsInTree(zones, null, null, true, false, true);
                return buildInfoPRZVTree(zones, deps, SymbolTableConstants.ALLKIND);
            }
            case ILLang.op_pointerAccess: {
                BoolVector offsetDep = new BoolVector(nDZ);
                cumulOr(depsThroughExpression(deps, expression.down(2), arrives), offsetDep, false);
                cumulOr(depsThroughExpression(deps, expression.down(1), arrives), offsetDep, false);
                TapList zones = curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null);
                if (zones == null) {
                    return null;
                }
                zones = TapList.copyTree(zones);
                includePointedElementsInTree(zones, null, null, true, false, true);
                TapList result = buildInfoPRZVTree(zones, deps, SymbolTableConstants.ALLKIND);
                orInfoPRZVTrees(offsetDep, result);
                return result;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_mod:
                return TapList.cumulWithOper(
                        depsThroughExpression(deps, expression.down(1), arrives),
                        depsThroughExpression(deps, expression.down(2), arrives),
                        SymbolTableConstants.CUMUL_OR);
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_sizeof:
                return depsThroughExpression(deps, expression.down(1), arrives);
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
            case ILLang.op_unary:
                return depsThroughExpression(deps, expression.down(2), arrives);
            case ILLang.op_ioCall: {
                int zoneIO = curCallGraph.zoneNbOfAllIOStreams;
                String ioAction = ILUtils.getIdentString(expression.down(1));
                boolean writesInVars = ioAction.equals("read")
                        || ioAction.equals("accept")
                        || ioAction.equals("decode");
                BoolVector usedZones = curUnit.buildControlDepsOf(curInstruction, curBlock) ;
                if (usedZones==null) usedZones = new BoolVector(nDZ);
                usedZones.set(zoneIO, true);
                BoolVector writtenZones = new BoolVector(nDZ);
                writtenZones.set(zoneIO, true);
                BoolVector totalWrittenZones = new BoolVector(nDZ);
                ToBool total = new ToBool(false);
                ToBool totally = new ToBool(false);
                ToBool written = new ToBool(false);
                Tree ioSpecTree;
                TapList writtenZonesTree;
                TapIntList writtenZonesList;
                Tree[] ioArgs = expression.down(3).children();
                for (int i = ioArgs.length - 1; i >= 0; i--) {
                    if (writesInVars) {
                        writtenZonesTree =
                                curSymbolTable.treeOfZonesOfValue(ioArgs[i], total, curInstruction, null);
                        writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, true);
                        writtenZones.set(writtenZonesList, true);
                        if (total.get()) {
                            totalWrittenZones.set(writtenZonesList, true);
                        }
                    } else {
                    /* pb avec cette ligne dans C v154
                      depsThroughExpression(deps, ioArgs[i], arrives) renvoie un truc bizarre */
                        cumulOr(depsThroughExpression(deps, ioArgs[i], arrives), usedZones, false);
                    }
                }
                /* Run the same read/write analysis as above, this time for the IO specs */
                Tree[] ioSpecs = expression.down(2).children();
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    ioSpecTree = InOutAnalyzer.getIOeffectOfIOspec(ioSpecs[i], i, ioAction,
                            written, totally, curSymbolTable);
                    if (written.get()) {
                        writtenZonesTree =
                                curSymbolTable.treeOfZonesOfValue(ioSpecTree, total, curInstruction, null);
                        writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, true);
                        writtenZones.set(writtenZonesList, true);
                        if (total.get() && totally.get()) {
                            totalWrittenZones.set(writtenZonesList, true);
                        }
                    } else {
                        cumulOr(depsThroughExpression(deps, ioSpecTree, arrives), usedZones, false);
                    }
                }
                for (int i = nDZ - 1; i >= 0; i--) {
                    if (writtenZones.get(i)) {
                        if (totalWrittenZones.get(i)) {
                            deps.setExplicitZeroRow(i);
                        }
                        if (deps.isImplicitIdentityRow(i)) {
                            deps.setExplicitIdentityRow(i);
                        }
                        deps.getRow(i).cumulOr(usedZones);
                    }
                }
                return null;
            }
            case ILLang.op_complexConstructor:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_arrayTriplet:
            case ILLang.op_forallRanges:
            case ILLang.op_arrayConstructor:
            case ILLang.op_constructorCall:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_expressions: {
                /* TODO: Ceci est le comportement Fortran,
                 * mais en C "u = a, b;" est valide et vaut b
                 * il faudrait donc rendre des deps p/r a la
                 * derniere expression seulement ! */
                Tree[] exprs = expression.children();
                BoolVector resultDep = new BoolVector(nDZ);
                for (int i = exprs.length - 1; i >= 0; i--) {
                    cumulOr(depsThroughExpression(deps, exprs[i], arrives), resultDep, false);
                }
                return new TapList<>(resultDep, null);
            }
            case ILLang.op_if:
            case ILLang.op_times:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_switch: {
                BoolVector conditionDep = new BoolVector(nDZ);
                cumulOr(depsThroughExpression(deps, expression.down(1), arrives), conditionDep, false);
                /* Find the test number associated with this control block: */
                int testZone = curUnit.testZoneOfControllerBlock(curBlock) ;
                if (testZone >= 0) {
                    deps.setRow(testZone, conditionDep);
                }
                return null;
            }
            case ILLang.op_allocate:
                return depsThroughExpression(deps, expression.down(2), arrives);
            case ILLang.op_for:
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                depsThroughExpression(deps, expression.down(3), arrives);
                return null;
            case ILLang.op_ifExpression:
                return TapList.cumulWithOper(
                        TapList.copyTree(
                                depsThroughExpression(deps, expression.down(2), arrives)),
                        depsThroughExpression(deps, expression.down(3), arrives),
                        SymbolTableConstants.CUMUL_OR);
            case ILLang.op_where: {
                BoolVector conditionDep = new BoolVector(nDZ);
                cumulOr(depsThroughExpression(deps, expression.down(1), arrives), conditionDep, false);
                /* Find the test number associated with this where test: */
                int testZone = curUnit.testZoneOfControllerWhere(curInstruction) ;
                deps.setRow(testZone, conditionDep);
                return null;
            }
            case ILLang.op_switchCases:
                depsThroughExpression(deps, expression.down(2), arrives);
                return null;
            case ILLang.op_loop:
                depsThroughExpression(deps, expression.down(3), arrives);
                return null;
            case ILLang.op_forall:
                // vmp a verifier ...
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                return null;
            case ILLang.op_forallRangeSet:
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                return null;
            case ILLang.op_forallRangeTriplet:
            case ILLang.op_do: {
                BoolVector boundsDep = new BoolVector(nDZ);
                TapIntList indexZones =
                        curSymbolTable.listOfZonesOfValue(expression.down(1), null, curInstruction);
                cumulOr(depsThroughExpression(deps, expression.down(2), arrives), boundsDep, false);
                cumulOr(depsThroughExpression(deps, expression.down(3), arrives), boundsDep, false);
                cumulOr(depsThroughExpression(deps, expression.down(4), arrives), boundsDep, false);
                /* do i=1,2,3 => i depends on i */
                boundsDep.set(indexZones, true);
                deps.overwriteDeps(indexZones, boundsDep, true);
                /* Find the test number associated with this control block: */
                int testZone = curUnit.testZoneOfControllerBlock(curBlock) ;
                if (testZone >= 0) {
                    deps.setRow(testZone, boundsDep);
                }
                return null;
            }
            case ILLang.op_iterativeVariableRef: {
                BoolVector resultDep = new BoolVector(nDZ);
                TapIntList indexZones =
                        curSymbolTable.listOfZonesOfValue(expression.down(2).down(1), null, curInstruction);
                cumulOr(depsThroughExpression(deps, expression.down(2).down(2), arrives), resultDep, false);
                cumulOr(depsThroughExpression(deps, expression.down(2).down(3), arrives), resultDep, false);
                cumulOr(depsThroughExpression(deps, expression.down(2).down(4), arrives), resultDep, false);
                deps.overwriteDeps(indexZones, resultDep, true);
                cumulOr(depsThroughExpression(deps, expression.down(1), arrives), resultDep, false);
                return new TapList<>(resultDep, null);
            }
            case ILLang.op_stop:
                if (arrives != null) {
                    arrives.set(false);
                }
                return null;
            case ILLang.op_return: {
                TapList result = null;
                depsThroughExpression(deps, expression.down(2), arrives);
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    String returnVarName = curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    Tree assignTree = ILUtils.turnReturnIntoAssign(expression, returnVarName);
                    result = depsThroughExpression(deps, assignTree, arrives);
                    ILUtils.resetReturnFromAssign(expression, assignTree);
                }
                return result;
            }
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                return null;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                return null;
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_letter:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_address:
            case ILLang.op_break:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_pointerType:
            case ILLang.op_arrayType:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return null;
            case ILLang.op_varDeclaration: {
                Tree[] decls = expression.down(3).children();
                inADeclaration = true;
                for (int i = decls.length - 1; i >= 0; i--) {
                    if (decls[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(decls[i]);
                        depsThroughExpression(deps, decls[i], arrives);
                        ILUtils.resetAssignFromInitDecl(decls[i]);
                    }
                }
                inADeclaration = false;
                return null;
            }
            default:
                TapEnv.toolWarning(-1, "(Compute plain dependencies) Unexpected operator : " + expression.opName());
                return null;
        }
    }

    /**
     * @return the dependency at the beginning of "block", by making
     * the union of all dependencies that might arrive through each
     * incoming control-flow arrow (FGArrow).
     */
    private BoolMatrix accumulateDepIn(Block block) {
        Block origin;
        BoolMatrix depIncoming;
        BoolMatrix depIn = null;
        TapList<FGArrow> arrows = block.backFlow();
        SymbolTable origSymbolTable;
        // OR-accumulate the dependence incoming through each of "arrows" into "depIn":
        while (arrows != null) {
            origin = arrows.head.origin;
            depIncoming = depsOut.retrieve(origin);
            if (depIncoming != null) {
                // if still null, initialize depIn to an "implicit-zero" BoolMatrix :
                if (depIn == null) {
                    depIn = new BoolMatrix(nDZ, nUDZ);
                }
                BoolVector saveDep = null;
                int rowIdx = -1;
                Tree expression;
                boolean doCaseAndNotInACycle;
                origSymbolTable = origin.symbolTable;
                if (block.instructions != null) {
                    expression = TapList.last(block.instructions).tree;
                    // op_do are encapsulated in op_loop so look for op_loop,
                    // then for its 3rd son which should be an op_do
                    doCaseAndNotInACycle = block instanceof HeaderBlock
                            && expression.opCode() == ILLang.op_loop
                            && expression.down(3).opCode() == ILLang.op_do
                            && !arrows.head.inACycle;
                    // If the arrow is a cycle arrow the header block is
                    // just an incrementation of the iterator, so the dep
                    // on the iterator are kept. If the arrow comes into
                    // the loop then the op_do is doing an initialization
                    // of the iterator so we should forget abouts its dependencies
                    if (doCaseAndNotInACycle) {
                        TapIntList listOfZones =
                                origSymbolTable.listOfZonesOfValue(expression.down(3).down(1), null, curInstruction);
                        if (listOfZones != null) {
                            rowIdx = listOfZones.head ;
                            saveDep = depIncoming.getRow(rowIdx);
                            if (saveDep != null) {
                                saveDep = saveDep.copy();
                            }
                            depIncoming.setExplicitZeroRow(rowIdx);
                        }
                    }
                }
                // Only variables common to the two Block's scopes may be transferred through this arrow:
                SymbolTable commonSymbolTable = block.symbolTable.getCommonRoot(origSymbolTable);
                depIn.cumulOr(depIncoming, true,
                        commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
                if (rowIdx != -1) {
                    depIncoming.setRow(rowIdx, saveDep);
                }
            }
            arrows = arrows.tail;
        }
        return depIn;
    }

    private BoolMatrix getPlainDeps(Unit calledUnit) {
        BoolMatrix unitDependency;
        if (calledUnit == null) {
            return null;
        }
        // Retrieve the callee dependencies:
        if (calledUnit.isIntrinsic()) {
            unitDependency = calledUnit.plainDependencies;
            if (unitDependency == null) {
                unitDependency = buildDefaultIntrinsicDependencies(calledUnit);
                calledUnit.plainDependencies = unitDependency;
            }
        } else if (calledUnit.isVarFunction()) {
            unitDependency = buildDefaultExternalDependencies(calledUnit);
        } else {
            unitDependency = calledUnit.plainDependencies;
            if (unitDependency == null && calledUnit.isExternal()) {
                unitDependency = buildDefaultExternalDependencies(calledUnit);
                calledUnit.plainDependencies = unitDependency;
            }
        }
        return unitDependency;
    }

}
