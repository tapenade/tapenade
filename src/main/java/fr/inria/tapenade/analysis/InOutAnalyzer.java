/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.representation.*;

/**
 * In-out analysis (aka read-write).
 */
public final class InOutAnalyzer extends DataFlowAnalyzer {
    /**
     * Constant that codes for the INOUT phase.
     */
    public static final int INOUT = 1;
    /**
     * Constant for the three other phases.
     */
    public static final int OTHER = 0;
    /**
     * Constant that codes for the CONSTANT sub phase.
     */
    public static final int CONSTANT = 2;
    /**
     * Constant that codes for the UNUSED sub phase.
     */
    public static final int UNUSED = 3;
    /**
     * Constant that codes for the USED_UNDEF sub phase.
     */
    public static final int USED_UNDEF = 4;

    /**
     * Set the current phase of the analysis at the flow graph level.
     */
    public void setFgPhase(int fgPhase) {
        this.fgPhase = fgPhase;
    }

    /**
     * Set the current subphase of the analysis at the flow graph level, when fgPhase==OTHER.
     */
    public void setFgSubPhase(int fgSubPhase) {
        this.fgSubPhase = fgSubPhase;
    }

    private int fgPhase;
    private int fgSubPhase;
    /**
     * The RW information before each Block of the current Unit.
     */
    private BlockStorage<BoolVector> beforeN;
    private BlockStorage<BoolVector> beforeR;
    private BlockStorage<BoolVector> beforeW;
    private BlockStorage<BoolVector> beforeRW;
    /**
     * The RW information before each HeaderBlock of the current Unit, coming from previous iteration.
     */
    private BlockStorage<BoolVector> beforeCycleN;
    private BlockStorage<BoolVector> beforeCycleR;
    private BlockStorage<BoolVector> beforeCycleW;
    private BlockStorage<BoolVector> beforeCycleRW;
    /**
     * The local RW effect of each Block of the current Unit.
     */
    private BlockStorage<BoolVector> throughN;
    private BlockStorage<BoolVector> throughR;
    private BlockStorage<BoolVector> throughW;
    private BlockStorage<BoolVector> throughRW;
    /**
     * The RW information after each Block of the current Unit.
     */
    private BlockStorage<BoolVector> afterN;
    private BlockStorage<BoolVector> afterR;
    private BlockStorage<BoolVector> afterW;
    private BlockStorage<BoolVector> afterRW;
    /**
     * The RW information after each HeaderBlock of the current Unit, going to next iteration.
     */
    private BlockStorage<BoolVector> afterCycleN;
    private BlockStorage<BoolVector> afterCycleR;
    private BlockStorage<BoolVector> afterCycleW;
    private BlockStorage<BoolVector> afterCycleRW;
    /**
     * Initialized Zones info before and after each Block of the current Unit.
     */
    private BlockStorage<BoolVector> beforeIZ;
    private BlockStorage<BoolVector> afterIZ;
    /**
     * RW information at the "current" point, whatever it means.
     */
    private BoolVector tmpN;
    private BoolVector tmpR;
    private BoolVector tmpW;
    private BoolVector tmpRW;
    /**
     * RW information that reaches the "current" point through a "cycle".
     */
    private BoolVector tmpCycleN;
    private BoolVector tmpCycleR;
    private BoolVector tmpCycleW;
    private BoolVector tmpCycleRW;
    /**
     * RW information that will be accumulated where control flow merges.
     */
    private BoolVector additionalN;
    private BoolVector additionalR;
    private BoolVector additionalW;
    private BoolVector additionalRW;
    /**
     * Information about Unused zones, Constant zones, and Initialized Zones.
     */
    private BoolVector tmpU;
    private BoolVector tmpC;
    private BoolVector tmpIZ;
    /**
     * Additional information about Unused zones, Constant zones, and Initialized Zones.
     */
    private BoolVector additionalU;
    private BoolVector additionalC;
    private BoolVector additionalIZ;

    /**
     * a global flag set to false by code that does not return (e.g. stop or calls that may stall)
     */
    private ToBool mayGoThrough = new ToBool(true) ;

    /**
     * Creates an InOutAnalyzer, given the
     * CallGraph "cg" on which it will work.
     */
    private InOutAnalyzer(CallGraph cg) {
        super(cg, "In-Out analysis", TapEnv.traceInOut());
    }

    /**
     * Main trigger method. Runs In-Out analysis under the rootUnits.
     * If the context for calling the differentiated root must be created,
     * then pass rootUnits==null and the analysis will run on the complete CallGraph,
     * including the parts that are not under the root differentiation Units.
     */
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<Unit> rootUnits) {
        TapEnv.setInOutAnalyzer(new InOutAnalyzer(callGraph));
        TapEnv.inOutAnalyzer().run(rootUnits);
    }

    /**
     * For the IO specification element "ioSpec", which is at
     * rank "i" in the list of IO specificators of an IO call to
     * IO operation "ioOper".
     *
     * @return the Tree of expressions that are affected
     * for this ioSpec, and whether they are read or written, totally or not.<br>
     * VERY specific to FORTRAN (77?) IO syntax. cf VAX-Fortran manual.
     */
    public static Tree getIOeffectOfIOspec(Tree ioSpec, int i,
                                           String ioOper,
                                           ToBool written, ToBool totally,
                                           SymbolTable symbolTable) {
        String key = "";
        Tree value = ioSpec;
        written.set(false);
        totally.set(false);
        if (ioSpec.opCode() == ILLang.op_nameEq) {
            key = ILUtils.getIdentString(ioSpec.down(1));
            value = ioSpec.down(2);
        } else if (i == 0) {
            if (!ioOper.equals("encode") && !ioOper.equals("decode")) {
                key = "unit";
                value = ioSpec;
            }
        } else if (i == 1) {
            key = "fmt";
            // TODO: If ioSpec is of "type" namelist, key should be 'nml'
            // and in this case we should be able to get the list of varnames.
        }
        if (key.equals("unit")) {
            // if the IO unit is indeed a character string:
            WrapperTypeSpec typeOfTarget = null;
            String typeBaseName = null;
            if (!ILUtils.isNullOrNone(ioSpec)) {
                typeOfTarget = symbolTable.typeOf(ioSpec);
            }
            if (typeOfTarget != null) {
                typeBaseName = typeOfTarget.baseTypeName();
            }
            if ("character".equals(typeBaseName)) {
                key = "ifile";
            } else {
                key = "lunit";
            }
        }
        /* Now keyword "key" and value "value"
           are known without ambiguity.
           We can decide whether the target "value" is read or written. */
        if (key.equals("iostat") ||
                key.equals("size") ||
                ioOper.equals("inquire") &&
                        (key.equals("access") ||
                                key.equals("blank") ||
                                key.equals("carriagecontrol") ||
                                key.equals("direct") ||
                                key.equals("exist") ||
                                key.equals("form") ||
                                key.equals("formatted") ||
                                key.equals("keyed") ||
                                key.equals("name") ||
                                key.equals("named") ||
                                key.equals("nextrec") ||
                                key.equals("number") ||
                                key.equals("opened") ||
                                key.equals("organization") ||
                                key.equals("recl") ||
                                key.equals("recordtype") ||
                                key.equals("sequential") ||
                                key.equals("unformatted") ||
                                key.equals("iolength")) ||
                key.equals("ifile") &&
                        (ioOper.equals("write") ||
                                ioOper.equals("rewrite"))) {
            written.set(true);
            totally.set(true);
        }
        if (key.equals("nml") &&
                (ioOper.equals("read") ||
                        ioOper.equals("accept") ||
                        ioOper.equals("decode")) ||
                i == 2 &&
                        ioOper.equals("encode")) {
            written.set(true);
            totally.set(false);
        }
        return value;
    }

    /**
     * @return true when the "expression" is cheap and is not modified
     * from the beginning of "block" till the end of the enclosing Unit.
     */
    public static boolean isCheapDuplicableConstant(Tree expression,
                                                    Block block, Instruction instruction) {
        /* Attention: in case of func_calls in expression, they may depend on
         * side-effect variables that are overwritten later, and this test
         * will not find them and may reply that the expression
         * is constant whereas it is not */
        int nDZ = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        BoolVector usedZones;
        usedZones = block.symbolTable.zonesUsedByExp(expression, instruction);
        if (block.constantZones != null) {
            usedZones.cumulAnd(block.constantZones.not());
        }
        return usedZones.isFalse(nDZ);
    }

    /**
     * Runs in-out analysis, upwards from the leaves of the CallGraph.
     * As a result, this function fills the inOut fields of each Unit.
     */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        String topAnalysisName = curAnalysisName;
        fgPhase = INOUT;
        curAnalysisName = "In-Out analysis (main run)";
        runBottomUpAnalysis(rootUnits);
        fgPhase = OTHER;
        curAnalysisName = "In-Out analysis (secondary run)";
        runBottomUpAnalysis(rootUnits);
        curAnalysisName = topAnalysisName;
    }

    /**
     * Set the current unit "curUnit", and various other globals specific to curUnit.
     * @param unit The new current Unit.
     */
    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        if (unit == null) {
            curSymbolTable = null;
        } else {
            curSymbolTable = unit.privateSymbolTable();
            if (curSymbolTable == null) {
                curSymbolTable = unit.publicSymbolTable();
            }
            nDZ = (curSymbolTable==null ? 0 : curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        }
    }

    /**
     * During initialization on the whole CallGraph, do whatever initialization
     * is needed for "unit", i.e. set the results to null for normal units
     * and to the default values for intrinsics, externals, etc...
     */
    @Override
    protected Object initializeCGForUnit() {
        if (fgPhase == INOUT) {
            if (curUnit.isOutside() || curUnit.isInterface() || curUnit.isVarFunction()
                || curUnit.isModule() || curUnit.isRenamed()) {
                if (curUnit.unitInOutN() == null) {
                    setExternalOrIntrinsicDefaultInOutInfo(curUnit);
                }
            } else {
                curUnit.unitInOutN = null;
                curUnit.unitInOutR = null;
                curUnit.unitInOutW = null;
                curUnit.unitInOutRW = null;
            }
        }
        return null;
    }

    /**
     * Runs in-out analysis on "curUnit", plus related analyses.
     *
     * @return true if the analysis result for this curUnit
     * has changed since the previous time.
     */
    @Override
    protected boolean analyze() {
        // In-Out analysis strictly speaking
        if (fgPhase == INOUT) {
            // Additional analysis for every checkpointed fragment of curUnit
            // (so far, we compute this only for II-Loops...)
            TapList<Block> insideBlocks = curUnit.allBlocks;
            while (insideBlocks!=null) {
                if (insideBlocks.head instanceof HeaderBlock && insideBlocks.head.instructions!=null) {
                    HeaderBlock loopHeader = (HeaderBlock)insideBlocks.head ;
                    if (loopHeader.headInstr().hasDirective(Directive.IILOOP) != null) {
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace(" Run local in-out analysis on checkpointed "+loopHeader+" OF UNIT "+curUnit.name()+" :") ;
                        }
                        TapList<FGArrow> bodyEntryArrows =
                            new TapList<>(loopHeader.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT),
                                          null) ;
                        TapList<FGArrow> bodyExitArrows = loopHeader.cyclingArrows() ;
                        TapList<Block> bodyBlocks = null ;
                        TapList<Block> allBlocks = curUnit.allBlocks ;
                        while (allBlocks!=null) {
                            if (allBlocks.head!=loopHeader && allBlocks.head.enclosedIn(loopHeader.enclosingLoop())) {
                                bodyBlocks = new TapList<>(allBlocks.head, bodyBlocks) ;
                            }
                            allBlocks = allBlocks.tail ;
                        }
                        bodyBlocks = TapList.nreverse(bodyBlocks) ;
                        analyzeBackward(bodyEntryArrows, bodyBlocks, bodyExitArrows) ;
                        BoolVector fragmentR = getFrontierPossiblyReadZones(bodyEntryArrows) ;
                        BoolVector fragmentW = getFrontierPossiblyWrittenZones(bodyEntryArrows) ;
                        // Add the effect of the loop header:
                        setCurBlockEtc(loopHeader);
                        initializeFGForBlock();
                        fragmentR.cumulOr(throughR.retrieve(loopHeader).or(throughRW.retrieve(loopHeader)), nDZ) ;
                        fragmentW.cumulOr(throughW.retrieve(loopHeader).or(throughRW.retrieve(loopHeader)), nDZ) ;
                        curUnit.setFragmentInOutCkp(loopHeader, new TapPair<>(fragmentR, fragmentW)) ;
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace(" returns local possiblyR:["+fragmentR.toString(nDZ)+"] possiblyW:["+fragmentW.toString(nDZ)+"]") ;
                        }
                    }
                }
                insideBlocks = insideBlocks.tail ;
            }

            return analyzeBackward(null, null, null);
            // TODO: put detection of aliasing here rather than in inOutThroughExpression !!
        } else if (fgPhase == OTHER) {
            fgSubPhase = CONSTANT;
            // Sweeps through the flow-graph of a Unit which has just been analyzed
            // for In-Out variables, and for each Block computes the zones which
            // are constant from (the beginning of) this Block to the exit of this curUnit.
            // Puts the result into the field Block.constantZones of each Block.
            analyzeBackward(null, null, null);
            fgSubPhase = UNUSED;
            // Sweeps through the flow-graph of a Unit which has just been analyzed
            // for In-Out variables, and for each Block computes the zones which
            // are Unused, from (the beginning of) this Block to the exit of this curUnit.
            // Puts the result into the field Block.unusedZones of each Block.
            analyzeBackward(null, null, null);
            fgSubPhase = USED_UNDEF;
            // Sweeps through the flow-graph of the Unit which has just been analyzed
            // for In-Out variables, and computes the zones of local variables
            // which are not initialized.
            // Then when a zone is used before it is initialized, complains!
            // SymbolTable trick needed to make entry Block initialize DATA and SAVE's !!
            curUnit.entryBlock().symbolTable = curUnit.privateSymbolTable();
            analyzeForward(null, null, null);
            curUnit.entryBlock().symbolTable = curUnit.publicSymbolTable();
            // TODO: Just like analysis USED_UNDEF, implement analyzeDefUnused, i.e.
            //      detection of defs that are never used later.
            // notice that this should be done after a fixpoint on the whole call graph !!
            return false;
        }
        return false;
    }

    @Override
    protected void terminateCGForUnit() {
        if (stoppedUnitInOut(curUnit)) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DF06) Procedure "
                    + curUnit.name() + " does not terminate properly");
            setExternalOrIntrinsicDefaultInOutInfo(curUnit);
        } else {
            if (erroredUnitInOut(curUnit)) {
                TapEnv.toolWarning(-1, "(In-out analysis) error in N R W RW status");
            }
        }
    }

    /**
     * pour une unit qui fait stop
     * tous les unitInOutX sont a false.
     */
    private boolean stoppedUnitInOut(Unit unit) {
        return (unit.unitInOutN() == null
                || (!unit.unitInOutN().get(0)
                    && !unit.unitInOutR().get(0)
                    && !unit.unitInOutW().get(0)
                    && !unit.unitInOutRW().get(0))) ;
    }

    private boolean erroredUnitInOut(Unit unit) {
        if (unit.hasParamElemsInfo()) {
            int shapeLength = unit.paramElemsNb();
            return unit.unitInOutN() == null
                    || unit.unitInOutN.isFalse(shapeLength)
                    && unit.unitInOutR.isFalse(shapeLength)
                    && unit.unitInOutW.isFalse(shapeLength)
                    && unit.unitInOutRW.isFalse(shapeLength);
        } else {
            return false;
        }
    }

    /**
     * Initialize before one sweep on the curUnit.
     */
    @Override
    protected void initializeFGForUnit() {
        if (fgPhase == INOUT || (fgPhase==OTHER && fgSubPhase==CONSTANT)) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace(" ===================== IN-OUT ANALYSIS OF UNIT "
                        + curUnit.name() + " : ==================== "
                        + (fgPhase == INOUT ? "INOUT" : "CONSTANT") + " phase");
                ZoneInfo zi;
                for (int i = 0; i < nDZ; ++i) {
                    zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                    if (zi != null) {
                        TapEnv.printOnTrace(" [" + i + "]" + zi.accessTreePrint(curUnit.language()));
                    }
                }
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace();
            }
            beforeN = new BlockStorage<>(curUnit);
            beforeR = new BlockStorage<>(curUnit);
            beforeW = new BlockStorage<>(curUnit);
            beforeRW = new BlockStorage<>(curUnit);
            beforeCycleN = new BlockStorage<>(curUnit);
            beforeCycleR = new BlockStorage<>(curUnit);
            beforeCycleW = new BlockStorage<>(curUnit);
            beforeCycleRW = new BlockStorage<>(curUnit);
            throughN = new BlockStorage<>(curUnit);
            throughR = new BlockStorage<>(curUnit);
            throughW = new BlockStorage<>(curUnit);
            throughRW = new BlockStorage<>(curUnit);
            afterN = new BlockStorage<>(curUnit);
            afterR = new BlockStorage<>(curUnit);
            afterW = new BlockStorage<>(curUnit);
            afterRW = new BlockStorage<>(curUnit);
            afterCycleN = new BlockStorage<>(curUnit);
            afterCycleR = new BlockStorage<>(curUnit);
            afterCycleW = new BlockStorage<>(curUnit);
            afterCycleRW = new BlockStorage<>(curUnit);
            if (fgPhase==OTHER && fgSubPhase==CONSTANT) {
                beforeIZ = new BlockStorage<>(curUnit);
                afterIZ = new BlockStorage<>(curUnit);
            }
        }
    }

    /**
     * Set curBlock, curSymbolTable, nDZ, nDRZ
     *
     * @param block The new current Block.
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        nDZ = (block==null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
    }

    /**
     * Set an identity information on the given initial block.
     */
    @Override
    protected void initializeFGForInitBlock() {
        if (fgPhase == INOUT) {
            // Initialize the in-out information upstream this curBlock:
            BoolVector tmpVect = new BoolVector(nDZ);
            tmpVect.setTrue();
            beforeN.store(curBlock, tmpVect);
            beforeR.store(curBlock, new BoolVector(nDZ));
            beforeW.store(curBlock, new BoolVector(nDZ));
            beforeRW.store(curBlock, new BoolVector(nDZ));
            if (runSpecialCycleAnalysis(curBlock)) {
                // This is for running on only a part of the flow graph:
                beforeCycleN.store(curBlock, beforeN.retrieve(curBlock));
                beforeCycleR.store(curBlock, beforeR.retrieve(curBlock));
                beforeCycleW.store(curBlock, beforeW.retrieve(curBlock));
                beforeCycleRW.store(curBlock, beforeRW.retrieve(curBlock));
            }
        } else {
            if (fgSubPhase == CONSTANT) {
                curBlock.constantZones = new BoolVector(nDZ);
                curBlock.constantZones.setTrue();
            } else if (fgSubPhase == UNUSED) {
                curBlock.unusedZones = new BoolVector(nDZ);
            } else if (fgSubPhase == USED_UNDEF) {
                int nDZ2 = curUnit.privateSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
                BoolVector afterEntryIZ = new BoolVector(nDZ2);
                // The input parameters have an initial value ... :
                afterEntryIZ.setTrue();
                // ... but not the "local" variables ... :
                for (int i = curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND); i < nDZ2; i++) {
                    afterEntryIZ.set(i, false);
                }
                // ... however, all "save" and "data" zones do have an initial value:
                afterEntryIZ.set(curUnit.entryBlock().initializedLocalZones(), true);
                afterIZ.store(curBlock, afterEntryIZ);
            }
        }
    }

    /**
     * Initializes the in-out information through "curBlock".
     */
    @Override
    protected void initializeFGForBlock() {
        if (fgPhase == INOUT || (fgPhase==OTHER && fgSubPhase==CONSTANT)) {
            Tree tree;
            tmpN = new BoolVector(nDZ);
            tmpN.setTrue();
            tmpR = new BoolVector(nDZ);
            tmpW = new BoolVector(nDZ);
            tmpRW = new BoolVector(nDZ);
            setUniqueAccessZones(curBlock);
            TapList<Instruction> instructions = curBlock.instructions;
            instructions = TapList.reverse(instructions);
            boolean goesThrough = true ;
            while (instructions != null && goesThrough) {
                curInstruction = instructions.head;
                tree = curInstruction.tree;
                if (tree != null) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("   init through instruction:" + tree);
                    }
                    mayGoThrough.set(true) ;
                    inOutThroughExpression(tree, NOACT, true);
                    if (!mayGoThrough.get()) {
                        goesThrough = false ;
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("--------------inout upstream " + tree + " :::");
                        TapEnv.printlnOnTrace("          N:[" + tmpN.toString(nDZ) + "] R:[" + tmpR.toString(nDZ) + "] W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                    }
                }
                instructions = instructions.tail;
            }
            if (TapEnv.traceCurAnalysis()) {
                if (!goesThrough) {
                    TapEnv.printlnOnTrace(" Block does not go through: in-out info is empty") ;
                }
            }
            curInstruction = null;
            uniqueAccessZones = null;
            throughN.store(curBlock, (goesThrough ? tmpN : null));
            throughR.store(curBlock, (goesThrough ? tmpR : null));
            throughW.store(curBlock, (goesThrough ? tmpW : null));
            throughRW.store(curBlock, (goesThrough ? tmpRW : null));
            if (fgPhase == INOUT) {
                checkOverwrittenLoopIndices(curBlock, tmpW.or(tmpRW));
            }
        }
        if (fgPhase == OTHER) {
            if (fgSubPhase == CONSTANT) {
                curBlock.constantZones = null;
            } else if (fgSubPhase == UNUSED) {
                curBlock.unusedZones = null;
            }
        }
    }

    /**
     * Compares the downstream info with the result of previous sweep.
     * Sometimes (e.g. for INOUT) we think comparison might take too long,
     * so we don't compare and we return the default "true".
     */
    @Override
    protected boolean compareDownstreamValues() {
        if (fgPhase == INOUT) {
            afterN.store(curBlock, tmpN);
            afterR.store(curBlock, tmpR);
            afterW.store(curBlock, tmpW);
            afterRW.store(curBlock, tmpRW);
            if (runSpecialCycleAnalysis(curBlock)) {
                afterCycleN.store(curBlock, tmpCycleN);
                afterCycleR.store(curBlock, tmpCycleR);
                afterCycleW.store(curBlock, tmpCycleW);
                afterCycleRW.store(curBlock, tmpCycleRW);
            }
            return true;
        } else if (fgSubPhase == CONSTANT) {
            //  curBlock.constantZones = tmpC ; // Get rid of this line: it breaks the fixed-point mechanism.
            return true;
        } else if (fgSubPhase == UNUSED) {
            return true;
        } else if (fgSubPhase == USED_UNDEF) {
            BoolVector afterBlockIZ = afterIZ.retrieve(curBlock);
            if (afterBlockIZ == null || !tmpIZ.equals(afterBlockIZ, nDZ)) {
                afterIZ.store(curBlock, tmpIZ);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Compares the in-out data flow information with the
     * same info of the previous sweep. Overwrites the previous
     * info if different. @return true when the new result
     * is different from the previous one,
     * thus asking for a new FG sweep.
     */
    @Override
    protected boolean compareUpstreamValues() {
        if (fgPhase == INOUT) {
            boolean modN = compareWithStorage(tmpN, tmpCycleN, nDZ, beforeN, beforeCycleN);
            boolean modR = compareWithStorage(tmpR, tmpCycleR, nDZ, beforeR, beforeCycleR);
            boolean modW = compareWithStorage(tmpW, tmpCycleW, nDZ, beforeW, beforeCycleW);
            boolean modRW = compareWithStorage(tmpRW, tmpCycleRW, nDZ, beforeRW, beforeCycleRW);
            return modN || modR || modW || modRW;
        } else if (fgSubPhase == CONSTANT) {
            boolean blockGoesThrough = throughW.retrieve(curBlock) != null;
            if (blockGoesThrough &&
                    (curBlock.constantZones == null
                            || !tmpC.equals(curBlock.constantZones, nDZ))) {
                curBlock.constantZones = tmpC;
                return true;
            }
        } else if (fgSubPhase == UNUSED) {
            boolean blockGoesThrough = throughW.retrieve(curBlock) != null;
            if (blockGoesThrough &&
                    (curBlock.unusedZones == null
                            || !tmpU.equals(curBlock.unusedZones, nDZ))) {
                curBlock.unusedZones = tmpU;
                return true;
            }
        } else if (fgSubPhase == USED_UNDEF) {
            BoolVector beforeBlockIZ = beforeIZ.retrieve(curBlock);
            if (beforeBlockIZ == null || !tmpIZ.equals(beforeBlockIZ, nDZ)) {
                beforeIZ.store(curBlock, tmpIZ.copy());
                return true;
            }
        }
        return false;
    }

    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        return false;
    }

    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        return false;
    }

    /**
     * Gets and stores the final InOut result on the "curUnit".
     * The result is computed from the InOut value propagated
     * back till the Entry Block. @return true when the result has
     * changed since previous time.
     */
    @Override
    protected boolean terminateFGForUnit() {
        if (fgPhase == INOUT) {
            BoolVector unitInOutN = afterN.retrieve(curUnit.entryBlock()) ;
            BoolVector unitInOutR = afterR.retrieve(curUnit.entryBlock()) ;
            BoolVector unitInOutW = afterW.retrieve(curUnit.entryBlock()) ;
            BoolVector unitInOutRW = afterRW.retrieve(curUnit.entryBlock()) ;
            // Find if curUnit's in-out signature has changed since last time:
            boolean hasChanged = false ;
            if (unitInOutN != null) {
                int unitInOutInfoLength = curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                hasChanged = (curUnit.unitInOutN() == null
                              || !unitInOutN.equals(curUnit.unitInOutN(), unitInOutInfoLength)
                              || !unitInOutR.equals(curUnit.unitInOutR(), unitInOutInfoLength)
                              || !unitInOutW.equals(curUnit.unitInOutW(), unitInOutInfoLength)
                              || !unitInOutRW.equals(curUnit.unitInOutRW(), unitInOutInfoLength)) ;
            }
            if (hasChanged) {
                curUnit.unitInOutN = unitInOutN.copy() ;
                curUnit.unitInOutR = unitInOutR.copy() ;
                curUnit.unitInOutW = unitInOutW.copy() ;
                curUnit.unitInOutRW = unitInOutRW.copy() ;
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("terminateFGForUnit "+curUnit+" (has"+(hasChanged?"":" not")+" changed):");
                TapEnv.printlnOnTrace("N  [" + curUnit.unitInOutN().toString(nDZ)+"]");
                TapEnv.printlnOnTrace("R  [" + curUnit.unitInOutR().toString(nDZ)+"]");
                TapEnv.printlnOnTrace("W  [" + curUnit.unitInOutW().toString(nDZ)+"]");
                TapEnv.printlnOnTrace("RW [" + curUnit.unitInOutRW().toString(nDZ)+"]");
                TapEnv.printlnOnTrace();
            }
            return hasChanged;
        } else if (fgSubPhase == CONSTANT) {
            setCurBlockEtc(curUnit.entryBlock());
            accumulateValuesFromDownstream();
            curUnit.entryBlock().constantZones = tmpC;
            setCurBlockEtc(null);
            return true;
        } else if (fgSubPhase == USED_UNDEF) {
            TapList<Block> inAllBlocks = curUnit.allBlocks;
            while (inAllBlocks != null) {
                setCurBlockEtc(inAllBlocks.head);
                detectUsedUndef(curBlock, beforeIZ.retrieve(curBlock));
                inAllBlocks = inAllBlocks.tail;
            }
            setCurBlockEtc(null);
            /* Clean the intermediate results for this curUnit: */
            beforeN = null;
            beforeR = null;
            beforeW = null;
            beforeRW = null;
            beforeCycleN = null;
            beforeCycleR = null;
            beforeCycleW = null;
            beforeCycleRW = null;
            throughN = null;
            throughR = null;
            throughW = null;
            throughRW = null;
            afterN = null;
            afterR = null;
            afterW = null;
            afterRW = null;
            afterCycleN = null;
            afterCycleR = null;
            afterCycleW = null;
            afterCycleRW = null;
            return true;
        } else {
            return true;
        }
    }

    @Override
    protected boolean propagateValuesBackwardThroughBlock() {
        if (fgPhase == INOUT) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("   === Propagating upwards through Block " + curBlock + " ===");
            }
            if (runSpecialCycleAnalysis(curBlock)) {
                BoolVector localizedZones = new BoolVector(nDZ);
                localizedZones.set(((HeaderBlock) curBlock).localizedZones, true);
                BoolVector directEntryExitMask = directEntryExitMask((HeaderBlock) curBlock, nDZ, SymbolTableConstants.ALLKIND);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("         OUT N:[" + tmpN.toString(nDZ) + "] R:[" + tmpR.toString(nDZ) + "] W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                    if (tmpCycleN!=null) {
                        TapEnv.printlnOnTrace("   CYCLE OUT N:[" + tmpCycleN.toString(nDZ) + "] R:[" + tmpCycleR.toString(nDZ) + "] W:[" + tmpCycleW.toString(nDZ) + "] RW:[" + tmpCycleRW.toString(nDZ)+"]");
                    }
                    TapEnv.printlnOnTrace("   directEntryExitMask [" + directEntryExitMask.toString(nDZ)+"]");
                    TapEnv.printlnOnTrace("   localizedZones      [" + localizedZones.toString(nDZ)+"]");
                }
                BoolVector outN = tmpN;
                BoolVector outR = tmpR;
                BoolVector outW = tmpW;
                BoolVector outRW = tmpRW;
                BoolVector outCycleN = tmpCycleN;
                BoolVector outCycleR = tmpCycleR;
                BoolVector outCycleW = tmpCycleW;
                BoolVector outCycleRW = tmpCycleRW;
                BoolVector thruN;
                BoolVector thruR;
                BoolVector thruW;
                BoolVector thruRW;
                TapIntList indexZones = null;
                ToBool indexTotal = new ToBool(false);
                if (curBlock.isACleanDoLoop()) {
                    curInstruction = curBlock.headInstr();
                    Tree doTree = curInstruction.tree.down(3);
                    indexZones =
                            curSymbolTable.listOfZonesOfValue(doTree.down(1), indexTotal, curInstruction);
                    if (outN != null || outCycleN != null) {
                        tmpN = new BoolVector(nDZ);
                        tmpN.setTrue();
                        tmpR = new BoolVector(nDZ);
                        tmpW = new BoolVector(nDZ);
                        tmpRW = new BoolVector(nDZ);
                        inOutThroughExpression(doTree.down(3), READ, true);
                        if (outN != null) {
                            cumulThen(tmpN, tmpR, tmpW, tmpRW, outN, outR, outW, outRW);
                        }
                        if (outCycleN != null) {
                            cumulThen(tmpN, tmpR, tmpW, tmpRW, outCycleN, outCycleR, outCycleW, outCycleRW);
                        }
                    }
                    tmpN = new BoolVector(nDZ);
                    tmpN.setTrue();
                    tmpR = new BoolVector(nDZ);
                    tmpW = new BoolVector(nDZ);
                    tmpRW = new BoolVector(nDZ);
                    inOutThroughExpression(doTree.down(1), WRITE, true);
                    inOutThroughExpression(doTree.down(2), READ, true);
                    inOutThroughExpression(doTree.down(4), READ, true);
                    thruN = tmpN;
                    thruR = tmpR;
                    thruW = tmpW;
                    thruRW = tmpRW;
                    curInstruction = null;
                } else {
                    thruN = throughN.retrieve(curBlock);
                    thruR = throughR.retrieve(curBlock);
                    thruW = throughW.retrieve(curBlock);
                    thruRW = throughRW.retrieve(curBlock);
                }

                if (outCycleN != null) {
                    tmpCycleN = outCycleN.or(localizedZones);
                    tmpCycleR = outCycleR.minus(localizedZones);
                    tmpCycleW = outCycleW.minus(localizedZones);
                    tmpCycleRW = outCycleRW.minus(localizedZones);
                } else {
                    tmpCycleN = new BoolVector(nDZ);
                    tmpCycleR = new BoolVector(nDZ);
                    tmpCycleW = new BoolVector(nDZ);
                    tmpCycleRW = new BoolVector(nDZ);
                }
                if (outN != null) {
                    tmpCycleN.cumulOr(outN);
                    tmpCycleR.cumulOr(outR);
                    tmpCycleW.cumulOr(outW);
                    tmpCycleRW.cumulOr(outRW);
                }
                if (outCycleN != null) {
                    tmpN = outCycleN;
                    tmpR = outCycleR;
                    tmpW = outCycleW;
                    tmpRW = outCycleRW;
                } else {
                    tmpN = new BoolVector(nDZ);
                    tmpR = new BoolVector(nDZ);
                    tmpW = new BoolVector(nDZ);
                    tmpRW = new BoolVector(nDZ);
                }
                if (outN != null) {
                    // Even if there is no direct connection from loop entry to loop exit
                    // the exit info for arrays used only partially in the loop
                    // must be added to the loop entry info.
                    tmpN.cumulOr(outN.and(directEntryExitMask));
                    tmpR.cumulOr(outR.and(directEntryExitMask));
                    tmpW.cumulOr(outW.and(directEntryExitMask));
                    tmpRW.cumulOr(outRW.and(directEntryExitMask));
                }

                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("     THROUGH N:[" + thruN.toString(nDZ) + "] R:[" + thruR.toString(nDZ) + "] W:[" + thruW.toString(nDZ) + "] RW:[" + thruRW.toString(nDZ)+"]");
                }
                cumulThen(thruN, thruR, thruW, thruRW,
                        tmpCycleN, tmpCycleR, tmpCycleW, tmpCycleRW);
                if (indexZones != null) {
                    setExtendedDeclaredZonesRead(indexZones, tmpCycleN, tmpCycleR, tmpCycleW, tmpCycleRW, indexTotal.get());
                }
                cumulThen(thruN, thruR, thruW, thruRW,
                        tmpN, tmpR, tmpW, tmpRW);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("          IN N:[" + tmpN.toString(nDZ) + "] R:[" + tmpR.toString(nDZ) + "] W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                    if (tmpCycleN!=null) {
                        TapEnv.printlnOnTrace("   CYCLE  IN N:[" + tmpCycleN.toString(nDZ) + "] R:[" + tmpCycleR.toString(nDZ) + "] W:[" + tmpCycleW.toString(nDZ) + "] RW:[" + tmpCycleRW.toString(nDZ)+"]");
                    }
                }
                return true;
            } else {
                if (throughN.retrieve(curBlock) == null) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    NO THROUGH!");
                    }
                    tmpN = tmpR = tmpW = tmpRW = null;
                    return false;
                } else {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("         OUT N:[" + tmpN.toString(nDZ) + "] R:[" + tmpR.toString(nDZ) + "] W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                        TapEnv.printlnOnTrace("     THROUGH N:[" + throughN.retrieve(curBlock).toString(nDZ) + "] R:[" + throughR.retrieve(curBlock).toString(nDZ) + "] W:[" + throughW.retrieve(curBlock).toString(nDZ) + "] RW:[" + throughRW.retrieve(curBlock).toString(nDZ)+"]");
                    }
                    cumulThen(throughN.retrieve(curBlock), throughR.retrieve(curBlock), throughW.retrieve(curBlock), throughRW.retrieve(curBlock),
                            tmpN,
                            tmpR,
                            tmpW,
                            tmpRW);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("          IN N:[" + tmpN.toString(nDZ) + "] R:[" + tmpR.toString(nDZ) + "] W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                    }
                    return true;
                }
            }
        } else if (fgSubPhase == CONSTANT) {
            // Possibly Written
            BoolVector blockPW = throughW.retrieve(curBlock);
            if (blockPW != null) {
                blockPW = blockPW.or(throughRW.retrieve(curBlock));
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   === Propagating constants upwards through Block " + curBlock + " ===");
                    TapEnv.printOnTrace("      [" + tmpC.toString(nDZ) + "] throughW:[" + blockPW.toString(nDZ)+"]");
                }
                tmpC.cumulMinus(blockPW);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" ==> [" + tmpC.toString(nDZ)+"]");
                }
                return true;
            }
        } else if (fgSubPhase == UNUSED) {
            BoolVector blockN = throughN.retrieve(curBlock);
            BoolVector blockR = throughR.retrieve(curBlock);
            BoolVector blockRW = throughRW.retrieve(curBlock);
            // tmpU = (tmpU or ((not N) minus R)) minus (R ou RW)
            // tmpU = (tmpU ou not N) minus R minus RW
            if (blockN != null) {
                tmpU.cumulOr(blockN.not());
                tmpU.cumulMinus(blockR.or(blockRW));
                return true;
            }
        }
        return false;
    }

    /**
     * Only during USED_UNDEF phase, combine the "Initialized zones" info before "curBlock" with
     * in-out infos through "curBlock", to obtain and store the  "Initialized zones" after "curBlock".
     */
    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        if (fgSubPhase == USED_UNDEF) {
            BoolVector blockW = throughW.retrieve(curBlock);
            BoolVector blockRW = throughRW.retrieve(curBlock);

            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("   === Propagating initialized zones downwards through Block " + curBlock + " ===");
                TapEnv.printlnOnTrace("      [" + tmpIZ.toString(nDZ) + "] plus [" + blockW.toString(nDZ) + "] plus [" + blockRW.toString(nDZ)+"]");
            }
            if (blockW != null) {
                tmpIZ.cumulOr(blockW);
                tmpIZ.cumulOr(blockRW);
                return true;
            }
        }
        return false;
    }

    private void inOutThroughExpression(Tree expression, int act, boolean total) {
        switch (expression.opCode()) {
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expression, curSymbolTable);
                if (calledUnit == null) {
                    TapEnv.toolError("Could not find called Unit for main InOut analysis on " + expression);
                } else {
                    CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
                    if (callArrow == null) {
                        TapEnv.toolError("Could not find call arrow from " + curUnit + " to " + calledUnit);
                        break;
                    }
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    int nbActualParams = actualParams.length;

                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace();
                        TapEnv.printlnOnTrace(" ---------- In-Out Analysis of procedure call " + ILUtils.toString(expression) + " : ----------");
                        TapEnv.printlnOnTrace("    downcoming In-Out       : N:[" + tmpN.toString(nDZ) + "]  R:[" + tmpR.toString(nDZ)+"]");
                        TapEnv.printlnOnTrace("                              W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                        TapEnv.printlnOnTrace("    Called unit public In-Out info:");
                        ZoneInfo zi;
                        int nzu = calledUnit.paramElemsNb() ;
                        for (int i = 0; i < calledUnit.paramElemsNb(); i++) {
                            zi = calledUnit.paramElemZoneInfo(i);
                            if (zi != null) {
                                TapEnv.printOnTrace(" [" + i + "]" + zi.accessTreePrint(curUnit.language()));
                            }
                        }
                        TapEnv.printlnOnTrace();
                        TapEnv.printlnOnTrace("         N:[" + calledUnit.unitInOutN.toString(nzu) + "]  R:[" + calledUnit.unitInOutR.toString(nzu)+"]");
                        TapEnv.printlnOnTrace("         W:[" + calledUnit.unitInOutW.toString(nzu) + "] RW:[" + calledUnit.unitInOutRW.toString(nzu)+"]");
                    }

                    // It may happen that varFunction and interface Units didn't get their default in-out signature:
                    if (calledUnit.unitInOutN == null && (calledUnit.isVarFunction() || calledUnit.isInterface())) {
                        setExternalOrIntrinsicDefaultInOutInfo(calledUnit);
                    }

                    if (calledUnit.unitInOutN == null) {
                        // If calledUnit.unitInOutN is null, it means calledUnit either stops, never returns, or is not yet analyzed.
                        // In that case we do as if control flow cannot reach here yet, and we stop propagation.
                        mayGoThrough.set(false) ;
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("    Called unit does not return: control cannot go through") ;
                        }
                    } else {
                        if (fgPhase==OTHER && fgSubPhase==CONSTANT) {
                            detectAliasing(calledUnit, actualParams, expression);
                            detectSegmentationFaults(callArrow, actualParams);
                        }
                        // Special case for the "N" (not read nor written) info. For "translateCalleeDataToCallSite",
                        // its default value is "1" instead of "0" and combination must be done with "and" instead of "or".
                        // Therefore we negate the info before (and after) calling translateCalleeDataToCallSite()
                        BoolVector unitN = new BoolVector(nDZ);
                        TapList[] paramsN = translateCalleeDataToCallSite(calledUnit.unitInOutN.not(), unitN, null, null, true,
                                                expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                null, null, true, false) ;
                        for (int i=paramsN.length-1 ; i>=0 ; --i) TapList.negateBooleans(paramsN[i]) ;
                        unitN = unitN.not() ;

                        BoolVector unitR = new BoolVector(nDZ);
                        TapList[] paramsR = translateCalleeDataToCallSite(calledUnit.unitInOutR, unitR, null, null, true,
                                                expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                null, null, true, false) ;

                        BoolVector unitW = new BoolVector(nDZ);
                        TapList[] paramsW = translateCalleeDataToCallSite(calledUnit.unitInOutW, unitW, null, null, true,
                                                expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                null, null, true, false) ;

                        BoolVector unitRW = new BoolVector(nDZ);
                        TapList[] paramsRW = translateCalleeDataToCallSite(calledUnit.unitInOutRW, unitRW, null, null, true,
                                                expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                null, null, true, false) ;

                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("     made private for the caller:");
                            TapEnv.printlnOnTrace("         N:[" + unitN.toString(nDZ) + "]  R:[" + unitR.toString(nDZ)+"]");
                            TapEnv.printlnOnTrace("         W:[" + unitW.toString(nDZ) + "] RW:[" + unitRW.toString(nDZ)+"]");
                        }

                        TapList[] actualParamZoneTrees = new TapList[nbActualParams];
                        boolean[] actualParamTotals = new boolean[nbActualParams];
                        ToBool paramTotal = new ToBool();
                        for (int i = nbActualParams - 1; i >= 0; --i) {
                            paramTotal.set(false);
                            actualParamZoneTrees[i] =
                                    curSymbolTable.treeOfZonesOfValue(actualParams[i], paramTotal, curInstruction, null);
                            if (actualParamZoneTrees[i] != null) {
                                actualParamZoneTrees[i] = TapList.copyTree(actualParamZoneTrees[i]);
                                includePointedElementsInTree(actualParamZoneTrees[i], paramTotal, null, true,
                                        curSymbolTable, curInstruction, true, true);
                                actualParamTotals[i] = paramTotal.get();

                                if (calledUnit.takesArgumentByValue(i+1, curUnit.language()) && !argumentAlreadyBehindPointer(curUnit, calledUnit, i+1)) {
                                    if (TapEnv.traceCurAnalysis()) {
                                        TapEnv.printlnOnTrace(" .. passedByValue " + i + ":" + ILUtils.toString(actualParams[i])
                                                + " (total:" + actualParamTotals[i] + ") zones:" + actualParamZoneTrees[i]
                                                + "  N:" + paramsN[i+1] + " R:" + paramsR[i+1] + " W:" + paramsW[i+1] + " RW:" + paramsRW[i+1]);
                                    }
                                    modifyByValueInfo(paramsN[i+1], paramsW[i+1]);
                                    modifyByValueInfo(paramsR[i+1], paramsRW[i+1]);
                                }

                                // If some contents are accessed (R, W, or RW), and they are accessed
                                // through a pointer, then this pointer must be considered accessed R:
                                modifyPointersRead(paramsR[i+1], paramsW[i+1], paramsRW[i+1]);

                                if (TapEnv.traceCurAnalysis()) {
                                    TapEnv.printlnOnTrace("     for argument " + i + ":" + ILUtils.toString(actualParams[i]) + " (total:" + actualParamTotals[i] + ") zones:" + actualParamZoneTrees[i] + "  N:" + paramsN[i+1] + " R:" + paramsR[i+1] + " W:" + paramsW[i+1] + " RW:" + paramsRW[i+1]);
                                }

                                setActualArgZoneTreeInOut(actualParamZoneTrees[i], tmpN, tmpR, tmpW, tmpRW,
                                        paramsN[i+1], paramsR[i+1], paramsW[i+1], paramsRW[i+1],
                                        actualParamTotals[i], WRITE, new TapList<>(null, null));
                            } else if (!calledUnit.sameLanguage(curUnit.language()) && calledUnit.isFortran()) {
                                TapIntList zones = ZoneInfo.listAllZones(calledUnit.argsPublicRankTrees[i + 1], true);
                                while (zones != null) {
                                    ZoneInfo zoneInfo = calledUnit.paramElemZoneInfo(zones.head);
                                    SymbolDecl symbolDecl = zoneInfo.ownerSymbolDecl;
                                    if (symbolDecl instanceof VariableDecl) {
                                        symbolDecl.addExtraInfo(new TapList<>("value", null));
                                    }
                                    zones = zones.tail;
                                }
                            }
                        }

                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("    ==> before cumulThen N:[" + tmpN.toString(nDZ) + "]  R:[" + tmpR.toString(nDZ)+"]");
                            TapEnv.printlnOnTrace("                         W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                        }

                        cumulThen(unitN, unitR, unitW, unitRW,
                                tmpN, tmpR, tmpW, tmpRW);

                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("    ==> after cumulThen  N:[" + tmpN.toString(nDZ) + "]  R:[" + tmpR.toString(nDZ)+"]");
                            TapEnv.printlnOnTrace("                         W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                        }

                        for (int i = nbActualParams - 1; i >= 0; --i) {
                            if (actualParamZoneTrees[i] != null) {
                                setActualArgZoneTreeInOut(actualParamZoneTrees[i], tmpN, tmpR, tmpW, tmpRW,
                                        paramsN[i+1], paramsR[i+1], paramsW[i+1], paramsRW[i+1],
                                        actualParamTotals[i], READ, new TapList<>(null, null));
                                inOutThroughExpression(actualParams[i], NOACT, actualParamTotals[i]);
                            } else {
                                boolean paramRead = TapList.oneTrue(paramsR[i+1]) || TapList.oneTrue(paramsRW[i+1]);
                                inOutThroughExpression(actualParams[i], paramRead ? READ : NOACT, actualParamTotals[i]);
                            }
                        }
                    }

                    MPIcallInfo messagePassingInfo =
                            MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), expression, curUnit.language(), curBlock);
                    if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                        TapIntList messagePassingChannels =
                                messagePassingInfo.findMessagePassingChannelZones(curCallGraph);
                        setExtendedDeclaredZonesWritten(messagePassingChannels, tmpN, tmpR, tmpW, tmpRW, false);
                        setExtendedDeclaredZonesRead(messagePassingChannels, tmpN, tmpR, tmpW, tmpRW, false);
                    }

                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    ==> final            N:[" + tmpN.toString(nDZ) + "]  R:[" + tmpR.toString(nDZ)+"]");
                        TapEnv.printlnOnTrace("                         W:[" + tmpW.toString(nDZ) + "] RW:[" + tmpRW.toString(nDZ)+"]");
                    }
                }
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_pointerAssign: {
                //In Fortran, a initDeclarator is done only once
                //and makes its declared-initialized variable "save":
                if (inADeclaration && curUnit.isFortran()) {
                    total = false;
                }
                inOutThroughExpression(expression.down(1), WRITE, total);
                inOutThroughExpression(expression.down(2), READ, total);
                break;
            }
            case ILLang.op_unary:
                String operName = ILUtils.getIdentString(expression.down(1));
                if (operName != null &&
                        (operName.equals("++prefix") || operName.equals("++postfix") ||
                                operName.equals("--prefix") || operName.equals("--postfix"))) {
                    ToBool expTotal = new ToBool(false);
                    TapList valueZonesTree =
                            curSymbolTable.treeOfZonesOfValue(expression.down(2), expTotal, curInstruction, null);
                    boolean totalAccess = total && referenceIsTotal(expTotal.get(), valueZonesTree);
                    TapIntList valueZones = ZoneInfo.listAllZones(valueZonesTree, true);
                    setExtendedDeclaredZonesWritten(valueZones, tmpN, tmpR, tmpW, tmpRW, totalAccess);
                    setExtendedDeclaredZonesRead(valueZones, tmpN, tmpR, tmpW, tmpRW, totalAccess);
                    inOutThroughExpression(expression.down(2), NOACT, total);
                } else {
                    inOutThroughExpression(expression.down(2), READ, total);
                }
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_ident: {
                if (act != NOACT) {
                    ToBool expTotal = new ToBool(false);
                    TapList valueZonesTree =
                            curSymbolTable.treeOfZonesOfValue(expression, expTotal, curInstruction, null);
                    if (curUnit.isFortran() &&
                            TypeSpec.isA(curSymbolTable.typeOf(expression), SymbolTableConstants.COMPOSITETYPE)) {
                        //In Fortran, assignment from/on a record is allowed, and
                        // it recursively reads/writes the destinations of pointer fields !!
                        valueZonesTree =
                                curSymbolTable.includePointedElementsInZonesTree(valueZonesTree,
                                        curInstruction, null, true);
                    }
                    boolean totalAccess = total && referenceIsTotal(expTotal.get(), valueZonesTree);
                    TapIntList valueZones = ZoneInfo.listAllZones(valueZonesTree, true);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    detecting a " + (totalAccess ? "total" : "partial") + (act == WRITE ? " write" : " read") + " of zones " + valueZonesTree + " of expression " + expression + " => vectorIndices:" + valueZones);
                    }
                    if (act == WRITE) {
                        setExtendedDeclaredZonesWritten(valueZones, tmpN, tmpR, tmpW, tmpRW, totalAccess);
                    } else {
                        setExtendedDeclaredZonesRead(valueZones, tmpN, tmpR, tmpW, tmpRW, totalAccess);
                    }
                }
                switch (expression.opCode()) {
                    case ILLang.op_arrayAccess: {
                        inOutThroughExpression(
                                expression.down(1), NOACT, false);
                        Tree[] indexes = expression.down(2).children();
                        for (int i = indexes.length - 1; i >= 0; i--) {
                            inOutThroughExpression(indexes[i], READ, true);
                        }
                        break;
                    }
                    case ILLang.op_fieldAccess:
                        inOutThroughExpression(
                                expression.down(1), NOACT, total);
                        break;
                    case ILLang.op_pointerAccess:
                        // An access through a pointer makes a read of the pointer variable itself:
                        inOutThroughExpression(expression.down(1), READ, true);
                        inOutThroughExpression(expression.down(2), READ, true);
                        break;
                    default:
                        break;
                }
                break;
            }
            case ILLang.op_address:
                // Taking the address doesn't read the value,
                // but reads everything needed to reach the address:
                inOutThroughExpression(expression.down(1), NOACT, total);
                break;
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_dimColon:
            case ILLang.op_complexConstructor:
            case ILLang.op_forall:
                inOutThroughExpression(expression.down(2), act, total);
                inOutThroughExpression(expression.down(1), act, total);
                break;
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor: {
                // Special handling for the case where code may not go through:
                // and, or, and xor may choose to *not* execute 2nd child.
                // Therefore if 1st child may go through, so does the parent expression.
                inOutThroughExpression(expression.down(2), act, total);
                boolean mayGoThroughFirst = mayGoThrough.get() ;
                if (mayGoThroughFirst) {
                    inOutThroughExpression(expression.down(1), act, total);
                    mayGoThrough.set(true) ;
                }
                break;
            }
            case ILLang.op_binary:
                // TODO so far we only assume binary reads its arguments:
                inOutThroughExpression(expression.down(3), act, total);
                inOutThroughExpression(expression.down(1), act, total);
                break;
            case ILLang.op_arrayTriplet:
                inOutThroughExpression(expression.down(3), READ, total);
                inOutThroughExpression(expression.down(2), READ, total);
                inOutThroughExpression(expression.down(1), READ, total);
                break;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_pointerDeclarator:
                inOutThroughExpression(expression.down(1), act, total);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
                inOutThroughExpression(expression.down(2), READ, total);
                break;
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_switch:
            case ILLang.op_sizeof:
                inOutThroughExpression(expression.down(1), READ, total);
                break;
            case ILLang.op_ifExpression:
                inOutThroughExpression(expression.down(3), act, total);
                inOutThroughExpression(expression.down(2), act, total);
                inOutThroughExpression(expression.down(1), READ, total);
                break;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                // [llh] TODO: detect uses inside the OpenMP clauses
                break;
            case ILLang.op_loop:
                inOutThroughExpression(expression.down(3), NOACT, total);
                break;
            case ILLang.op_ioCall: {
                String ioOper = ILUtils.getIdentString(expression.down(1));
                Tree[] ioSpecs = expression.down(2).children();
                Tree targets;
                ToBool written = new ToBool(false);
                ToBool totally = new ToBool(false);
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    targets = InOutAnalyzer.getIOeffectOfIOspec(ioSpecs[i], i, ioOper,
                            written, totally, curSymbolTable);
                    act = written.get() ? WRITE : READ;
                    inOutThroughExpression(targets, act, totally.get());
                }
                inOutThroughExpression(expression.down(3), ILUtils.isIORead(expression) ? WRITE : READ,
                        true);
                int zoneIO = curCallGraph.zoneNbOfAllIOStreams;
                tmpR.set(zoneIO, true);
                tmpW.set(zoneIO, true);
                break;
            }
            case ILLang.op_constructorCall: {
                Tree[] args = expression.down(3).children();
                for (int i = args.length - 1; i >= 0; i--) {
                    inOutThroughExpression(args[i], READ, true);
                }
                break;
            }
            case ILLang.op_iterativeVariableRef:
                inOutThroughExpression(expression.down(1), act, total);
                inOutThroughExpression(expression.down(2), READ, true);
                break;
            case ILLang.op_forallRanges:
            case ILLang.op_dimColons:
            case ILLang.op_argDeclarations:
            case ILLang.op_expressions: {
                Tree[] exprs = expression.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    inOutThroughExpression(exprs[i], act, total);
                }
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] exprs = expression.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    inOutThroughExpression(exprs[i], READ, total);
                }
                break;
            }
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                inOutThroughExpression(expression.down(1), WRITE, total);
                inOutThroughExpression(expression.down(2), READ, total);
                inOutThroughExpression(expression.down(3), READ, total);
                inOutThroughExpression(expression.down(4), READ, total);
                break;
            case ILLang.op_forallRangeSet:
                inOutThroughExpression(expression.down(2), READ, total);
                inOutThroughExpression(expression.down(1), READ, total);
                break;
            case ILLang.op_for:
                inOutThroughExpression(expression.down(3), WRITE, total);
                inOutThroughExpression(expression.down(2), READ, total);
                inOutThroughExpression(expression.down(1), READ, total);
                break;
            case ILLang.op_return:
                inOutThroughExpression(expression.down(2), READ, total);
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    // child 1 is the returned expression (C style).
                    String returnName = curUnit.name();
                    if (curUnit.otherReturnVar() != null) {
                        returnName = curUnit.otherReturnVar().symbol;
                    }
                    Tree returnTree = ILUtils.build(ILLang.op_ident, returnName);
                    inOutThroughExpression(returnTree, WRITE, total);
                    inOutThroughExpression(expression.down(1), READ, total);
                }
                break;
            case ILLang.op_stop:
                tmpN = new BoolVector(nDZ);
                tmpN.setTrue();
                tmpR = new BoolVector(nDZ);
                tmpW = new BoolVector(nDZ);
                tmpRW = new BoolVector(nDZ);
                break;
            case ILLang.op_deallocate:
            case ILLang.op_nullify: {
                // as soon as a chunk is freed (not sure about op_nullify?), someone else may
                // take it and overwrite it, so this implies a potential WRITE of the contents:
                Tree access =
                        ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(expression.down(1)));
                inOutThroughExpression(access, WRITE, false);
                inOutThroughExpression(expression.down(1), WRITE, total);
                break;
            }
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_bitCst:
            case ILLang.op_break:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_strings:
            case ILLang.op_formatElem:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_interfaceDecl:
            case ILLang.op_useDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_allocate:
                break;
            case ILLang.op_varDeclaration: {
                Tree[] exprs = expression.down(3).children();
                Tree declarator;
                for (int i = exprs.length - 1; i >= 0; --i) {
                    declarator = exprs[i];
                    if (declarator.opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(declarator);
                    }
                    inOutThroughExpression(declarator, NOACT, total);
                    if (declarator.opCode() == ILLang.op_assign) {
                        ILUtils.resetAssignFromInitDecl(declarator);
                    }
                }
                break;
            }
            case ILLang.op_data: {
                inOutThroughExpression(expression.down(1), WRITE, false);
                break;
            }
            case ILLang.op_functionDeclarator:
            case ILLang.op_sizedDeclarator:
                inOutThroughExpression(expression.down(1), act, total);
                inOutThroughExpression(expression.down(2), READ, total);
                break;
            case ILLang.op_arrayDeclarator:
                if (act == WRITE && curUnit.isC()) {
                    //In C, initializing the array pointer initializes the destination too:
                    Tree access =
                            ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(expression.down(1)));
                    inOutThroughExpression(access, WRITE, true);
                }
                if (curUnit.isC() && !ILUtils.isNullOrNoneOrEmptyList(expression.down(2)))
                // in C, a declaration float p[n] initializes p (to *p).
                {
                    inOutThroughExpression(expression.down(1), WRITE, true);
                } else {
                    inOutThroughExpression(expression.down(1), act, true);
                }
                inOutThroughExpression(expression.down(2), READ, false);
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            default:
                TapEnv.toolWarning(-1, "(In-out analysis) Unexpected operator: " + expression.opName());
                break;
        }
    }

    /** Returns true only in the very specific case of Fortran calling C and
     * Fortran actual arg is a type T and C formal arg is a type T* */
    private boolean argumentAlreadyBehindPointer(Unit callingUnit, Unit calledUnit, int argRank) {
        if (TapEnv.isFortran(callingUnit.language()) && TapEnv.isC(calledUnit.language())
            && calledUnit.functionTypeSpec()!=null
            && calledUnit.functionTypeSpec().argumentsTypes[argRank-1]!=null
            && calledUnit.functionTypeSpec().argumentsTypes[argRank-1].isPointer()) {
            return true ;
        }
        return false ;
    }

    /**
     * setExtendedDeclaredZonesRead
     * <pre>
     * {@code
     * x =
     *
     *
     *
     *              N  R  W RW     N  R  W RW
     *
     * R total    + 0  0  0  0  => 0  1  0  0   (existe pas, N tjs 1)
     * R pastotal + 0  0  0  0  => 1  1  0  0    idem
     * R total    + 0  0  0  1  => 0  0  0  1          ne change pas
     * R pastotal + 0  0  0  1  => 0  0  0  1          ne change pas
     *
     * R total    + 0  0  1  0  => 0  0  0  1          W -> RW (10 -> 01)
     * R pastotal + 0  0  1  0  => 0  0  1  1          +RW
     * R total    + 0  0  1  1  => 0  0  0  1          -W
     * R pastotal + 0  0  1  1  => 0  0  1  1          ne change pas
     *
     * R total    + 0  1  0  0  => 0  1  0  0          ne change pas
     * R pastotal + 0  1  0  0  => 0  1  0  0          ne change pas
     * R total    + 0  1  0  1  => 0  1  0  1          ne change pas
     * R pastotal + 0  1  0  1  => 0  1  0  1          ne change pas
     *
     * R total    + 0  1  1  0  => 0  1  0  1          W -> RW (10 -> 01)
     * R pastotal + 0  1  1  0  => 0  1  1  1          +RW
     * R total    + 0  1  1  1  => 0  1  0  1          -W
     * R pastotal + 0  1  1  1  => 0  1  1  1          ne change pas
     *
     * R total    + 1  0  0  0  => 0  1  0  0          -N +R
     * R pastotal + 1  0  0  0  => 1  1  0  0          +R
     * R total    + 1  0  0  1  => 0  0  0  1          -N
     * R pastotal + 1  0  0  1  => 1  0  0  1          ne change pas
     *
     * R total    + 1  0  1  0  => 0  0  0  1          -N -W +RW
     * R pastotal + 1  0  1  0  => 1  0  1  1          +RW
     * R total    + 1  0  1  1  => 0  0  0  1          -N -W
     * R pastotal + 1  0  1  1  => 1  0  1  1          ne change pas
     *
     * R total    + 1  1  0  0  => 0  1  0  0          -N
     * R pastotal + 1  1  0  0  => 1  1  0  0          ne change pas
     * R total    + 1  1  0  1  => 0  1  0  1          -N
     * R pastotal + 1  1  0  1  => 1  1  0  1          ne change pas
     *
     * R total    + 1  1  1  0  => 0  1  0  1          -N -W +RW
     * R pastotal + 1  1  1  0  => 1  1  1  1          +RW
     * R total    + 1  1  1  1  => 0  1  0  1          -N -W
     * R pastotal + 1  1  1  1  => 1  1  1  1          ne change pas
     *
     * equivalent a cumulThen avec W1 et RW1 faux et N1 == non Total et R1 vrai
     * N2 = non Total et N2
     * R2 = (non Total et R2) ou (N2) ou (R2)
     * W2 = non Total et W2
     * RW2 = (W2) ou (RW2) ou (non Total et RW2)
     *
     * total true           total false
     * N2 = false             N2 = N2
     * R2 = R2 ou N2          R2 = R2 ou N2
     * W2 = false             W2 = W2
     * RW2 = W2 ou RW2        RW2 = W2 ou RW2
     * }</pre>
     */
    private void setExtendedDeclaredZonesRead(TapIntList allZones, BoolVector tmpN,
                                              BoolVector tmpR, BoolVector tmpW,
                                              BoolVector tmpRW, boolean totalAccess) {
        int z;
        while (allZones != null) {
            z = allZones.head;
            if (z > 0 && tmpN.get(z)) {
                tmpR.set(z, true);
                if (totalAccess) {
                    tmpN.set(z, false);
                }
            }
            if (z > 0 && tmpW.get(z)) {
                tmpRW.set(z, true);
                if (totalAccess) {
                    tmpW.set(z, false);
                }
            }
            allZones = allZones.tail;
        }
    }

    /**
     * setExtendedDeclaredZonesWritten
     * <pre>
     * {@code
     * X =
     * puis...
     * tmp      =>    tmp
     *              N  R  W RW     N  R  W RW
     *
     * W total    + 0  0  0  0  => 0  0  1  0   (existe pas, N tjs 1)
     * W pastotal + 0  0  0  0  => 1  0  1  0    idem
     * W total    + 0  0  0  1  => 0  0  1  0
     * W pastotal + 0  0  0  1  => 0  0  1  1
     *
     * W total    + 0  0  1  0  => 0  0  1  0
     * W pastotal + 0  0  1  0  => 0  0  1  0
     * W total    + 0  0  1  1  => 0  0  1  0
     * W pastotal + 0  0  1  1  => 0  0  1  1
     *
     * W total    + 0  1  0  0  => 0  0  1  0
     * W pastotal + 0  1  0  0  => 0  1  1  0
     * W total    + 0  1  0  1  => 0  0  1  0
     * W pastotal + 0  1  0  1  => 0  1  1  1
     *
     * W total    + 0  1  1  0  => 0  0  1  0
     * W pastotal + 0  1  1  0  => 0  1  1  0
     * W total    + 0  1  1  1  => 0  0  1  0
     * W pastotal + 0  1  1  1  => 0  1  1  1
     *
     * W total    + 1  0  0  0  => 0  0  1  0
     * W pastotal + 1  0  0  0  => 1  0  1  0
     * W total    + 1  0  0  1  => 0  0  1  0
     * W pastotal + 1  0  0  1  => 1  0  1  1
     *
     * W total    + 1  0  1  0  => 0  0  1  0
     * W pastotal + 1  0  1  0  => 1  0  1  0
     * W total    + 1  0  1  1  => 0  0  1  0
     * W pastotal + 1  0  1  1  => 1  0  1  1
     *
     * W total    + 1  1  0  0  => 0  0  1  0
     * W pastotal + 1  1  0  0  => 1  1  1  0
     * W total    + 1  1  0  1  => 0  0  1  0
     * W pastotal + 1  1  0  1  => 1  1  1  1
     *
     * W total    + 1  1  1  0  => 0  0  1  0
     * W pastotal + 1  1  1  0  => 1  1  1  0
     * W total    + 1  1  1  1  => 0  0  1  0
     * W pastotal + 1  1  1  1  => 1  1  1  1
     *
     * equivalent a cumulThen avec W1 true, R1 et RW1 false, N1 == non Total
     *
     * N2 = non Total et N2
     * R2 = (non Total et R2)
     * W2 = true
     * RW2 = (non Total et RW2)
     *
     * total true           total false
     * N2 = false             N2 = N2
     * R2 = false             R2 = R2
     * W2 = true              W2 = true
     * RW2 = false            RW2 = RW2
     * }</pre>
     */
    private void setExtendedDeclaredZonesWritten(TapIntList allZones, BoolVector tmpN, BoolVector tmpR,
                                                 BoolVector tmpW, BoolVector tmpRW, boolean totalAccess) {
        int z;
        while (allZones != null) {
            z = allZones.head ;
            tmpW.set(z, true);
            if (totalAccess) {
                tmpN.set(z, false);
                tmpR.set(z, false);
                tmpRW.set(z, false);
            }
            allZones = allZones.tail;
        }
    }

    /** Set the INOUT info for the zones of the zones-tree of an actual argument of a call.
     * When action is READ, set to "read" all zones from zonesTree for which tmpR and tmpRW
     * indicate that they are read.
     * When action is WRITE, set to "written" all zones from zonesTree for which tmpW and tmpRW
     * indicate that they are written.
     * This setting is done by modifying tmpX in place, by adding the READ or WRITE action
     * before the existing tmpX.
     */
    private void setActualArgZoneTreeInOut(TapList zonesTree, BoolVector tmpN, BoolVector tmpR,
                                  BoolVector tmpW, BoolVector tmpRW,
                                  TapList treeN, TapList treeR, TapList treeW, TapList treeRW,
                                  boolean total, int action, TapList<TapList> dejaVu) {
        // TODO(?) (cf argument *r of set10/mix16): maybe the "total" info should return to false
        //  when going across a pointer deref, in a mixed-language call (C calls F).
        while (zonesTree != null) {
            if (TapList.contains(dejaVu.tail, zonesTree)) {
                zonesTree = new TapList<>(ZoneInfo.listAllZones(zonesTree, true), null);
            } else {
                dejaVu.placdl(zonesTree);
            }
            if (zonesTree.head instanceof TapList) {
                setActualArgZoneTreeInOut((TapList) zonesTree.head, tmpN, tmpR, tmpW, tmpRW,
                        treeN != null && treeN.head instanceof TapList ? (TapList) treeN.head : treeN,
                        treeR != null && treeR.head instanceof TapList ? (TapList) treeR.head : treeR,
                        treeW != null && treeW.head instanceof TapList ? (TapList) treeW.head : treeW,
                        treeRW != null && treeRW.head instanceof TapList ? (TapList) treeRW.head : treeRW,
                        total, action, dejaVu);
            } else {
                boolean oneN = TapList.oneTrueInHead(treeN);
                boolean oneR = TapList.oneTrueInHead(treeR);
                boolean oneW = TapList.oneTrueInHead(treeW);
                boolean oneRW = TapList.oneTrueInHead(treeRW);
                if (action == WRITE) {
                    if (oneW || oneRW) {
                        setExtendedDeclaredZonesWritten((TapIntList) zonesTree.head,
                                tmpN, tmpR, tmpW, tmpRW, total && !oneN && !oneR);
                    }
                } else {
                    if (oneR || oneRW) {
                        setExtendedDeclaredZonesRead((TapIntList) zonesTree.head,
                                tmpN, tmpR, tmpW, tmpRW, total && !oneN && !oneW);
                    }
                }
            }
            zonesTree = zonesTree.tail;
            if (treeN != null) {
                treeN = treeN.tail;
            }
            if (treeR != null) {
                treeR = treeR.tail;
            }
            if (treeW != null) {
                treeW = treeW.tail;
            }
            if (treeRW != null) {
                treeRW = treeRW.tail;
            }
        }
    }

    /**
     * Assuming that paramWithoutWrite and paramWithWrite are two TapList trees of Boolean
     * (either N and W or R and RW) that apply to one argument of a procedure call,
     * assuming also that this argument is passed by value,
     * transforms paramWithoutWrite and paramWithWrite so that if some part of the
     * argument was written, it is now considered not written.
     */
    private void modifyByValueInfo(TapList paramWithoutWrite, TapList paramWithWrite) {
        while (paramWithoutWrite != null && paramWithWrite != null) {
            if (paramWithoutWrite.head instanceof TapList
                    && paramWithWrite.head instanceof TapList) {
                modifyByValueInfo((TapList) paramWithoutWrite.head, (TapList) paramWithWrite.head);
                paramWithoutWrite = paramWithoutWrite.tail;
                paramWithWrite = paramWithWrite.tail;
            } else if (paramWithoutWrite.head instanceof Boolean
                    && paramWithWrite.head instanceof Boolean) {
                if ((Boolean) paramWithWrite.head) {
                    paramWithWrite.head = Boolean.FALSE;
                    paramWithoutWrite.head = Boolean.TRUE;
                }
                paramWithoutWrite = null;
                paramWithWrite = null;
            }
        }
    }

    private void modifyPointersRead(TapList paramR, TapList paramW, TapList paramRW) {
        if (paramR.head instanceof TapList) {//recursive case
            while (paramR != null) {
                modifyPointersRead((TapList) paramR.head, (TapList) paramW.head, (TapList) paramRW.head);
                paramR = paramR.tail;
                paramW = paramW.tail;
                paramRW = paramRW.tail;
            }
        } else {
            if (TapList.oneTrue(paramR.tail) || TapList.oneTrue(paramW.tail) || TapList.oneTrue(paramRW.tail)) {
                paramR.head = Boolean.TRUE;
            }
        }
    }

    /**
     * First initialize the cumul values if they are null,
     * then accumulate the additional value into the cumul value.
     */
    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        if (fgPhase == INOUT) {
            if (tmpN == null) {
                tmpN = new BoolVector(nDZ);
            }
            if (additionalN != null) {
                tmpN.cumulOr(additionalN, commonLength);
            }
            if (tmpR == null) {
                tmpR = new BoolVector(nDZ);
            }
            if (additionalR != null) {
                tmpR.cumulOr(additionalR, commonLength);
            }
            if (tmpW == null) {
                tmpW = new BoolVector(nDZ);
            }
            if (additionalW != null) {
                tmpW.cumulOr(additionalW, commonLength);
            }
            if (tmpRW == null) {
                tmpRW = new BoolVector(nDZ);
            }
            if (additionalRW != null) {
                tmpRW.cumulOr(additionalRW, commonLength);
            }
        } else if (fgSubPhase == CONSTANT) {
            if (tmpC == null) {
                tmpC = new BoolVector(nDZ);
                tmpC.setTrue();
            }
            tmpC.cumulAnd(additionalC, commonLength);
        } else if (fgSubPhase == UNUSED) {
            if (tmpU == null) {
                tmpU = new BoolVector(nDZ);
                tmpU.setTrue();
            }
            tmpU.cumulAnd(additionalU, commonLength);
        } else if (fgSubPhase == USED_UNDEF) {
            if (tmpIZ == null) {
                tmpIZ = new BoolVector(nDZ);
            }
            tmpIZ.cumulOr(additionalIZ, commonLength);
        }
    }

    /**
     * First initialize the cumul cycle-values if they are null,
     * then accumulate the additional value into the cumul cycle-value.
     */
    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        if (fgPhase == INOUT) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("cumulCycleValueWithAdditional");
                if (tmpCycleN!=null) {
                    TapEnv.printlnOnTrace(" tmpCycle    N:[" + tmpCycleN.toString(nDZ) + "] R:[" + tmpCycleR.toString(nDZ) + "] W:[" + tmpCycleW.toString(nDZ) + "] RW:[" + tmpCycleRW.toString(nDZ)+"]");
                }
                TapEnv.printlnOnTrace(" additional  N:[" + additionalN.toString(nDZ) + "] R:[" + additionalR.toString(nDZ) + "] W:[" + additionalW.toString(nDZ) + "] RW:[" + additionalRW.toString(nDZ)+"]");
            }
            if (tmpCycleN == null) {
                tmpCycleN = new BoolVector(nDZ);
            }
            if (additionalN != null) {
                tmpCycleN.cumulOr(additionalN, commonLength);
            }
            if (tmpCycleR == null) {
                tmpCycleR = new BoolVector(nDZ);
            }
            if (additionalR != null) {
                tmpCycleR.cumulOr(additionalR, commonLength);
            }
            if (tmpCycleW == null) {
                tmpCycleW = new BoolVector(nDZ);
            }
            if (additionalW != null) {
                tmpCycleW.cumulOr(additionalW, commonLength);
            }
            if (tmpCycleRW == null) {
                tmpCycleRW = new BoolVector(nDZ);
            }
            if (additionalRW != null) {
                tmpCycleRW.cumulOr(additionalRW, commonLength);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("cumulCycleValueWithAdditional-->");
                TapEnv.printlnOnTrace(" tmpCycl>>   N:[" + tmpCycleN.toString(nDZ) + "] R:[" + tmpCycleR.toString(nDZ) + "] W:[" + tmpCycleW.toString(nDZ) + "] RW:[" + tmpCycleRW.toString(nDZ)+"]");
            }
        } else {
            cumulValueWithAdditional(commonSymbolTable);
        }
    }

    /**
     * Accumulates the given addedN addedR addedW and addedRW before the
     * given propagated infos cumulN cumulR cumulW and cumulRW.
     * cumulN cumulR cumulW and cumulRW are modified by side effect to
     * hold the result.
     * <p>
     * <pre>
     * {@code
     * table des cas: 1 (added) suivi de 2 (cumul)
     * de 2: N  R  W RW
     * suivi
     * 1      N  N  R  W RW
     * R  R  R RW RW
     * W  W  W  W  W
     * RW RW RW RW RW
     *
     * RW2 = RW1 ou (R1 et W2) ou (R1 et RW2) ou (N1 et RW2)
     * = RW1 ou (R1 et W2) ou ((R1 ou N1) et RW2)
     *
     * tmp = r1.or(n1);
     * rw2.cumulAnd(r1.or(n1);        ou bien   rw2.cumulAndNot(w1) ?
     * rw2.cumulOr(r1.and(w2);
     * rw2.cumulOr(rw1);
     *
     * W2 = W1 ou (N1 et W2)
     *
     * r2.cumulAnd(n1);
     * w2.cumulOr(w1);
     *
     * R2 = (N1 et R2) ou (R1 et N2) ou (R1 et R2)
     * = ((N1 ou R1) et R2) ou (R1 et N2)
     *
     * r2.cumulAnd(r1.or(n1);
     * r2.cumulOr(r1.and(n2));
     *
     * N2 = N1 et N2
     *
     * n2.cumulAnd(n1);.
     * }</pre>
     */
    private void cumulThen(BoolVector addedN, BoolVector addedR,
                           BoolVector addedW, BoolVector addedRW,
                           BoolVector cumulN, BoolVector cumulR,
                           BoolVector cumulW, BoolVector cumulRW) {
        BoolVector tmpVect;

        tmpVect = addedR.or(addedN);

        cumulRW.cumulAnd(tmpVect);
        cumulRW.cumulOr(addedR.and(cumulW));
        cumulRW.cumulOr(addedRW);

        cumulW.cumulAnd(addedN);
        cumulW.cumulOr(addedW);

        cumulR.cumulAnd(tmpVect);
        cumulR.cumulOr(addedR.and(cumulN));

        cumulN.cumulAnd(addedN);
    }

    @Override
    protected void setEmptyCumulAndCycleValues() {
        if (fgPhase == INOUT) {
            tmpN = tmpR = tmpW = tmpRW = null;
            tmpCycleN = tmpCycleR = tmpCycleW = tmpCycleRW = null;
        } else if (fgSubPhase == CONSTANT) {
            tmpC = null;
        } else if (fgSubPhase == UNUSED) {
            tmpU = null;
        } else if (fgSubPhase == USED_UNDEF) {
            tmpIZ = null;
        }
    }

    @Override
    protected boolean getValueFlowingBack() {
        Block destination = curArrow.destination;
        if (fgPhase == INOUT) {
            boolean result;
            if (runSpecialCycleAnalysis(destination) && curArrow.finalCycle() == destination.enclosingLoop()) {
                additionalN = beforeCycleN.retrieve(destination);
                if (additionalN != null) {
                    additionalR = beforeCycleR.retrieve(destination);
                    additionalW = beforeCycleW.retrieve(destination);
                    additionalRW = beforeCycleRW.retrieve(destination);
                    result = true;
                } else {
                    result = false;
                }
            } else {
                additionalN = beforeN.retrieve(destination);
                if (additionalN != null) {
                    additionalR = beforeR.retrieve(destination);
                    additionalW = beforeW.retrieve(destination);
                    additionalRW = beforeRW.retrieve(destination);
                    result = true;
                } else {
                    result = false;
                }
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printOnTrace("   === Flowing across " + curArrow + " (cycle:" + (runSpecialCycleAnalysis(destination) && curArrow.finalCycle() == destination.enclosingLoop()) + "): ");
                if (result) {
                    TapEnv.printOnTrace("N:[" + additionalN.toString(nDZ) + "] R:[" + additionalR.toString(nDZ) + "] W:[" + additionalW.toString(nDZ) + "] RW:[" + additionalRW.toString(nDZ)+"]");
                } else {
                    TapEnv.printOnTrace("null");
                }
                TapEnv.printlnOnTrace();
            }
            return result;
        } else if (fgSubPhase == CONSTANT) {
            additionalC = destination.constantZones;
            return additionalC != null;
        } else if (fgSubPhase == UNUSED) {
            additionalU = destination.unusedZones;
            return additionalU != null;
        } else {
            return false;
        }
    }

    @Override
    protected boolean getValueFlowingThrough() {
        Block origin = curArrow.origin;
        if (fgSubPhase == USED_UNDEF) {
            additionalIZ = afterIZ.retrieve(origin);
            return additionalIZ != null;
        } else {
            return false;
        }
    }

    @Override
    protected void initializeCumulValue() {
        if (fgPhase == INOUT) {
            tmpN = new BoolVector(nDZ);
            tmpR = new BoolVector(nDZ);
            tmpW = new BoolVector(nDZ);
            tmpRW = new BoolVector(nDZ);
        } else if (fgSubPhase == CONSTANT) {
            tmpC = new BoolVector(nDZ);
        } else if (fgSubPhase == UNUSED) {
            tmpU = new BoolVector(nDZ);
        } else if (fgSubPhase == USED_UNDEF) {
            tmpIZ = new BoolVector(nDZ);
        }
    }

    private void detectUsedUndef(Block block, BoolVector beforeBlockIZ) {
        if (beforeBlockIZ != null) {
            TapList<Instruction> instructions = block.instructions;
            Tree tree;
            curSymbolTable = block.symbolTable;
            while (instructions != null) {
                tree = instructions.head.tree;
                if (tree != null) {
                    curInstruction = instructions.head;
                    usedUndefThroughExpression(tree, beforeBlockIZ, NOACT);
                    curInstruction = null;
                }
                instructions = instructions.tail;
            }
        }
    }

    private void usedUndefThroughExpression(Tree expression,
                                            BoolVector beforeExprIZ, int act) {
        switch (expression.opCode()) {
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expression, curSymbolTable);
                if (calledUnit == null) {
                    TapEnv.toolError("Could not find called Unit for usedUndef InOut analysis on " + expression);
                } else {
                    CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    int nbArgs = actualParams.length;
                    if (callArrow != null && calledUnit.unitInOutN() != null) {
                        //Certainly Read:
                        BoolVector callCR = new BoolVector(nDZ);
                        TapList[] paramsCR = translateCalleeDataToCallSite(calledUnit.unitInOutCertainlyR(), callCR, null, null, true,
                                                 expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                 null, null, true, false) ;
                        //Possibly Written:
                        BoolVector callPW = new BoolVector(nDZ);
                        TapList[] paramsPW = translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), callPW, null, null, true,
                                                 expression, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                 null, null, false, false) ;
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace();
                            TapEnv.printlnOnTrace(" Unit called " + calledUnit);
                            int nzu = calledUnit.paramElemsNb() ;
                            TapEnv.printlnOnTrace("    Unit Certainly Read   [" + calledUnit.unitInOutCertainlyR().toString(nzu)+"]");
                            TapEnv.printlnOnTrace("    Unit Possibly Written [" + calledUnit.unitInOutPossiblyW().toString(nzu)+"]");
                        }
                        TapList argZones;
                        for (int i = 0; i < nbArgs; i++) {
                            usedUndefThroughExpression(actualParams[i], beforeExprIZ, NOACT);
                            argZones = curSymbolTable.treeOfZonesOfValue(
                                    actualParams[i], null, curInstruction, null);
                            usedUndefThroughArgZonesTree(argZones,
                                                         paramsCR[i+1], paramsPW[i+1],
                                    beforeExprIZ, actualParams[i]);
                        }
                        ZoneInfo zoneInfo;
                        for (int dz = nDZ - 1; dz > 0; dz--) {
                            if (!beforeExprIZ.get(dz) && callCR.get(dz)) {
                                zoneInfo = curBlock.symbolTable.declaredZoneInfo(dz, SymbolTableConstants.ALLKIND);
                                TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF03) variable " + zoneInfo.publicName() + " is used before initialized");
                            }
                        }
                        beforeExprIZ.cumulOr(callPW);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace(" beforeExprIZ " + beforeExprIZ);
                        }
                    }
                }
                break;
            }
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                // Warning: this case continues into next case:
            case ILLang.op_assign:
                // Detect and mark situations where this assignment is an initialization:
                //  it overwrites an undefined value, which need not be saved.
                // This detection is rudimentary: it might be refined to find more initializations
                TapIntList lhsZones =
                                ZoneInfo.listAllZones(
                                        curSymbolTable.treeOfZonesOfValue(expression.down(1), null, curInstruction, null),
                                        false) ;
                if (!beforeExprIZ.intersects(lhsZones)) {
                    expression.down(1).setAnnotation("IsInitialization", Boolean.TRUE);
                }

                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, WRITE);
                break;
            case ILLang.op_unary:
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                break;
            case ILLang.op_pointerAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_ident: {
                switch (expression.opCode()) {
                    case ILLang.op_arrayAccess: {
                        Tree[] indexes = expression.down(2).children();
                        for (int i = indexes.length - 1; i >= 0; i--) {
                            usedUndefThroughExpression(indexes[i], beforeExprIZ, READ);
                        }
                        usedUndefThroughExpression(expression.down(1), beforeExprIZ, NOACT);
                        break;
                    }
                    case ILLang.op_fieldAccess:
                        usedUndefThroughExpression(expression.down(1), beforeExprIZ, NOACT);
                        break;
                    case ILLang.op_pointerAccess:
                        usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                        usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                        break;
                    default:
                        break;
                }
                if (act != NOACT) {
                    TapIntList valueZonesList =
                            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null), true);
                    if (act == READ) {
                        if (valueZonesList != null) {
                            if (valueZonesList.head == curCallGraph.zoneNbOfNullDest && valueZonesList.tail == null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF03) expression " + ILUtils.toString(expression, curUnit.language()) + " derefs a NULL pointer");
                            } else if (valueZonesList.head == curCallGraph.zoneNbOfUnknownDest && valueZonesList.tail == null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF03) expression " + ILUtils.toString(expression, curUnit.language()) + " is undefined");
                            } else if (!intersectsZoneRks(beforeExprIZ, valueZonesList, null)) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF03) variable " + ILUtils.toString(expression, curUnit.language()) + " is used before initialized");
                            }
                        }
                    } else if (act == WRITE) {
                        beforeExprIZ.set(valueZonesList, true);
                    }
                }
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_dimColon:
            case ILLang.op_complexConstructor:
            case ILLang.op_return:
            case ILLang.op_forallRangeSet:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                break;
            case ILLang.op_binary:
                // pour C
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, READ);
                break;
            case ILLang.op_arrayTriplet:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, READ);
                break;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_sizeof:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_switch:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                break;
            case ILLang.op_ifExpression:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, act);
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, act);
                break;
            case ILLang.op_nameEq:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, act);
                break;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                // [llh] TODO: detect uses inside the OpenMP clauses
                break;
            case ILLang.op_loop:
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, NOACT);
                break;
            case ILLang.op_ioCall: {
                String ioOper = ILUtils.getIdentString(expression.down(1));
                Tree[] ioSpecs = expression.down(2).children();
                Tree targets;
                ToBool written = new ToBool(false);
                ToBool totally = new ToBool(false);
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    targets = getIOeffectOfIOspec(ioSpecs[i], i, ioOper,
                            written, totally, curSymbolTable);
                    act = written.get() ? WRITE : READ;
                    usedUndefThroughExpression(targets, beforeExprIZ, act);
                }
                usedUndefThroughExpression(expression.down(3), beforeExprIZ,
                        ILUtils.isIORead(expression) ? WRITE : READ);
                break;
            }
            case ILLang.op_constructorCall: {
                Tree[] args = expression.down(3).children();
                for (int i = args.length - 1; i >= 0; i--) {
                    usedUndefThroughExpression(args[i], beforeExprIZ, READ);
                }
                break;
            }
            case ILLang.op_iterativeVariableRef:
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, act);
                break;
            case ILLang.op_forallRanges:
            case ILLang.op_dimColons:
            case ILLang.op_argDeclarations:
            case ILLang.op_expressions: {
                Tree[] exprs = expression.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    usedUndefThroughExpression(exprs[i], beforeExprIZ, act);
                }
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] exprs = expression.children();
                for (int i = exprs.length - 1; i >= 0; i--) {
                    usedUndefThroughExpression(exprs[i], beforeExprIZ, READ);
                }
                break;
            }
            case ILLang.op_forall:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, act);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, act);
                break;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(4), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, WRITE);
                break;
            case ILLang.op_for:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                usedUndefThroughExpression(expression.down(3), beforeExprIZ, WRITE);
                break;
            case ILLang.op_pointerDeclarator:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, act);
                break;
            case ILLang.op_functionDeclarator:
            case ILLang.op_sizedDeclarator:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, act);
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                break;
            case ILLang.op_arrayDeclarator:
                usedUndefThroughExpression(expression.down(2), beforeExprIZ, READ);
                if (curUnit.isC() && !ILUtils.isNullOrNoneOrEmptyList(expression.down(2)))
                // in C, a declaration float p[n] initializes p (to *p).
                {
                    usedUndefThroughExpression(expression.down(1), beforeExprIZ, WRITE);
                } else {
                    usedUndefThroughExpression(expression.down(1), beforeExprIZ, act);
                }
                if (act == WRITE && curUnit.isC()) {
                    //In C, initializing the array pointer initializes the destination too:
                    Tree access =
                            ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(expression.down(1)));
                    usedUndefThroughExpression(access, beforeExprIZ, WRITE);
                }
                break;
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_bitCst:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_strings:
            case ILLang.op_formatElem:
            case ILLang.op_address:
            case ILLang.op_nullify:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_interfaceDecl:
            case ILLang.op_useDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            case ILLang.op_allocate: {
                // ALLOCATE(T) returns a new, non-initialized *T
                TapList annot = expression.getAnnotation("allocatedZones");
                beforeExprIZ.set(ZoneInfo.listAllZones(annot, true), false);
                initializeArrayPointers(annot, beforeExprIZ);
                break;
            }
            case ILLang.op_deallocate:
                usedUndefThroughExpression(expression.down(1), beforeExprIZ, WRITE);
                break;
            case ILLang.op_varDeclaration: {
                Tree[] exprs = expression.down(3).children();
                Tree declarator;
                for (int i = exprs.length - 1; i >= 0; --i) {
                    declarator = exprs[i];
                    if (declarator.opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(declarator);
                    }
                    usedUndefThroughExpression(declarator, beforeExprIZ, NOACT);
                    if (declarator.opCode() == ILLang.op_assign) {
                        ILUtils.resetAssignFromInitDecl(declarator);
                    }
                }
                break;
            }
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            default:
                TapEnv.toolWarning(-1, "(Use-Def analysis) Unexpected operator: " + expression.opName());
                break;
        }
    }

    /**
     * When declaring a new variable, it is in general considered uninitialized.
     * However, in C, a new variable of array type TT[size] is in fact a pointer,
     * but initialized to its default destination array.
     */
    private void initializeArrayPointers(TapList zones, BoolVector beforeExprIZ) {
        ZoneInfo zoneInfo;
        int index;
        while (zones != null) {
            if (zones.head == null) {
            } else if (zones.head instanceof TapList) {
                initializeArrayPointers((TapList) zones.head, beforeExprIZ);
            } else {
                TapIntList intZones = (TapIntList) zones.head;
                while (intZones != null) {
                    zoneInfo = curSymbolTable.declaredZoneInfo(intZones.head, SymbolTableConstants.ALLKIND);
                    if (zoneInfo.type != null && TypeSpec.isA(zoneInfo.type.wrappedType, SymbolTableConstants.POINTERTYPE)
                            && ((PointerTypeSpec) zoneInfo.type.wrappedType).isInFactArray()) {
                        index = zoneInfo.kindZoneNb(SymbolTableConstants.ALLKIND);
                        beforeExprIZ.set(index, true);
                    }
                    intZones = intZones.tail;
                }
            }
            zones = zones.tail;
        }
    }

    private void usedUndefThroughArgZonesTree(TapList valueZones,
                                              TapList valueCR, TapList valuePW,
                                              BoolVector beforeExprIZ, Tree argTree) {
        while (valueZones != null) {
            if (valueCR != null && valueCR.head instanceof TapList) {
                if (valueZones.head instanceof TapList) {
                    /* recursion case: go down in the tree structure on each side. */
                    usedUndefThroughArgZonesTree((TapList) valueZones.head,
                            (TapList) valueCR.head,
                            (TapList) valuePW.head, beforeExprIZ, argTree);
                } else {
                    /* Weird case where valueZones and valueCR do not match!
                     * Here call with "record" formal arg and "non-record" actual arg */
                    TapIntList valueZonesList = (TapIntList) valueZones.head;
                    boolean boolCR = TapList.oneTrue((TapList) valueCR.head);
                    boolean boolPW = TapList.oneTrue((TapList) valuePW.head);
                    if (boolCR
                            && valueZonesList != null
                            && !intersectsZoneRks(beforeExprIZ, valueZonesList, null)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, argTree, "(DF03) variable " + ILUtils.toString(argTree) + " is used before initialized");
                    }
                    if (boolPW) {
                        beforeExprIZ.set(valueZonesList, true);
                    }
                }
            } else {
                TapIntList valueZonesList;
                if (valueZones.head instanceof TapList) {
                    /* Weird case where valueZones and valueCR do not match!
                     * Here call with "non-record" formal arg and "record" actual arg */
                    valueZonesList = ZoneInfo.listAllZones((TapList) valueZones.head, true);
                } else {
                    valueZonesList = (TapIntList) valueZones.head;
                }
                boolean boolCR = false;
                boolean boolPW = false;
                if (valueCR != null && valueCR.head != null) {
                    boolCR = (Boolean) valueCR.head;
                }
                if (valuePW != null && valuePW.head != null) {
                    boolPW = (Boolean) valuePW.head;
                }
                if (boolCR
                        && valueZonesList != null
                        && !intersectsZoneRks(beforeExprIZ, valueZonesList, null)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, argTree, "(DF03) variable " + ILUtils.toString(argTree) + " is used before initialized");
                }
                if (boolPW) {
                    beforeExprIZ.set(valueZonesList, true);
                }
            }
            valueZones = valueZones.tail;
            if (valueCR != null) {
                valueCR = valueCR.tail;
            }
            if (valuePW != null) {
                valuePW = valuePW.tail;
            }
        }
    }

    /** Run in-out analyzer locally on the piece of code defined by "region",
     * and return the pair of the possibly read and oberwritten BoolVectors
     * TODO: try use possiblyW info of the INOUT phase instead of MODIFIED of the CONSTANT phase. */
    public TapPair<BoolVector, BoolVector> getInOutOfRegion(
            TapTriplet<TapList<FGArrow>, TapList<Block>, TapList<FGArrow>> region,
            Unit unit) {
        setCurUnitEtc(unit);
        fgPhase = INOUT;
        // Start a local in-out run on the region:
        analyzeBackward(region.first, region.second, region.third);
        BoolVector readVars = getFrontierPossiblyReadZones(region.first);
        BoolVector modifiedVars = getFrontierPossiblyWrittenZones(region.first);

//         fgPhase = OTHER;
//         fgSubPhase = CONSTANT;
//         // Detect all variables modified:
//         analyzeBackward(region.first, region.second, region.third);
//         BoolVector modifiedVars = getFrontierModifiedZones(region.first);

        // Recompute the original in-out analysis results
        fgPhase = INOUT;
        analyzeBackward(null, null, null);
//         fgPhase = OTHER;
//         fgSubPhase = CONSTANT;
//         analyzeBackward(null, null, null);
        setCurUnitEtc(null);
        return new TapPair<>(readVars, modifiedVars);
    }

    /**
     * When "frontier" is a list of converging flow arrows that define
     * the entry into a fragment of the current Unit.
     *
     * @return the vector of
     * the zones that are read/used by this fragment.
     */
    public BoolVector getFrontierPossiblyReadZones(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        BoolVector result = new BoolVector(resNDZ);
        FGArrow arrow;
        int commonLength;
        while (frontier != null) {
            arrow = frontier.head;
            commonLength = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            result.cumulOr(beforeR.retrieve(arrow.destination), commonLength);
            result.cumulOr(beforeRW.retrieve(arrow.destination), commonLength);
            frontier = frontier.tail;
        }
        return result;
    }

    /**
     * When "frontier" is a list of converging flow arrows that define
     * the entry into a fragment of the current Unit.
     *
     * @return the vector of
     * the zones that are possibly overwritten by this fragment.
     */
    public BoolVector getFrontierPossiblyWrittenZones(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        BoolVector result = new BoolVector(resNDZ);
        FGArrow arrow;
        int commonLength;
        while (frontier != null) {
            arrow = frontier.head;
            commonLength = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            result.cumulOr(beforeW.retrieve(arrow.destination), commonLength);
            result.cumulOr(beforeRW.retrieve(arrow.destination), commonLength);
            frontier = frontier.tail;
        }
        return result;
    }

    /**
     * When "frontier" is a list of converging flow arrows that define
     * the entry into a fragment of the current Unit.
     *
     * @return the vector of
     * the zones that are modified by this fragment.
     */
    public BoolVector getFrontierModifiedZones(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        BoolVector result = new BoolVector(resNDZ);
        FGArrow arrow;
        int commonLength;
        while (frontier != null) {
            arrow = frontier.head;
            commonLength = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            result.cumulOr(arrow.destination.constantZones.not(), commonLength);
            frontier = frontier.tail;
        }
        return result;
    }

    /**
     * When "frontier" is a list of converging flow arrows that define
     * the entry into a fragment of the current Unit.
     *
     * @return the vector of
     * the zones that are unused by this fragment.
     */
    public BoolVector getFrontierUnusedZones(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        BoolVector result = new BoolVector(resNDZ);
        result.setTrue();
        FGArrow arrow;
        int commonLength;
        while (frontier != null) {
            arrow = frontier.head;
            commonLength = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            result.cumulAnd(arrow.destination.unusedZones, commonLength);
            frontier = frontier.tail;
        }
        return result;
    }

    private void detectAliasing(Unit callDestination, Tree[] actualParams, Tree expression) {
        if (callDestination.isDummy()) return ;
        int nPubZones = callDestination.paramElemsNb();
        // Build in accessorPubZones, for each zone in the caller routine,
        // the BoolVector of the public zones of the callee that use this caller zone:
        BoolMatrix accessorPubZones = new BoolMatrix(nDZ, nPubZones);
        accessorPubZones.setExplicitZero();
        for (int i = 2; i < nPubZones; i++) {
            ZoneInfo zi = callDestination.paramElemZoneInfo(i) ;
            if (zi!=null && zi.kind()==SymbolTableConstants.GLOBAL && !zi.comesFromAllocate()) {
                accessorPubZones.set(zi.zoneNb, i, true) ;
            }
        }
        TapList actualZones;
        TapList formalZones;
        Unit calledUnit = callDestination;
        SymbolTable calleeSymbolTable = calledUnit.publicSymbolTable() ;
        for (int i = actualParams.length - 1; i >= 0; i--) {
            if (i + 1 < calledUnit.argsPublicRankTrees.length) {
                formalZones = calledUnit.argsPublicRankTrees[i + 1];
                actualZones =
                        curSymbolTable.treeOfZonesOfValue(actualParams[i], null, curInstruction, null);
                if (actualZones!=null) {
                    actualZones = TapList.copyTree(actualZones);
                    includePointedElementsInTree(actualZones, null, null, true,
                                                 curSymbolTable, curInstruction, false, true);
                }
                matchActualToFormalRec(accessorPubZones, actualZones, formalZones,
                                       calleeSymbolTable, new TapList<>(null, null));
            }
        }
        // Examine each row of accessorPubZones for potential aliasing:
        TapList toConflicts = new TapList<>(null, null);
        BoolVector aliasRanks;
        TapIntList aliasRanksUsed;
        BoolVector tmpPubW = calledUnit.unitInOutPossiblyW();
        BoolVector tmpPubRW = calledUnit.unitInOutPossiblyRorW();
        for (int i = 2; i < nDZ; i++) { // Don't look for aliasing on NULL and UNDEF.
            aliasRanks = accessorPubZones.getRow(i);
            if (aliasRanks.intersects(tmpPubW, nPubZones)) {
                // If the memory zone of this row is written in the call...
                aliasRanksUsed = aliasRanks.and(tmpPubRW).trueIndexList(nPubZones);
                if (TapIntList.length(aliasRanksUsed) >= 2) {
                    // ...and this memory zone is passed as more than 1 argument,
                    //  then this a potential aliasing. Insert it, removing duplicates:
                    TapList inConflicts = toConflicts;
                    while (inConflicts.tail != null && aliasRanksUsed != null) {
                        if (TapIntList.contains((TapIntList) inConflicts.tail.head, aliasRanksUsed))
                        // if previous conflict subsumes new conflict, stop:
                        {
                            aliasRanksUsed = null;
                        } else if (TapIntList.contains(aliasRanksUsed, (TapIntList) inConflicts.tail.head))
                        // else if new conflict subsumes old, remove old:
                        {
                            inConflicts.tail = inConflicts.tail.tail;
                        } else
                        // else advance in list of previous conflicts:
                        {
                            inConflicts = inConflicts.tail;
                        }
                    }
                    if (inConflicts.tail == null && aliasRanksUsed != null) {
                        inConflicts.tail = new TapList<>(aliasRanksUsed, null);
                    }
                }
            }
        }
        // Print out each aliasing problem found:
        toConflicts = toConflicts.tail;
        while (toConflicts != null) {
            TapIntList conflict = (TapIntList) toConflicts.head;
            TapIntList conflictParams = new TapIntList(-1, null);
            TapList<String> conflictGlobals = null;
            while (conflict != null) {
                ZoneInfo zoneInfo = calledUnit.paramElemZoneInfo(conflict.head);
                switch (zoneInfo.kind()) {
                    case SymbolTableConstants.PARAMETER:
                        TapIntList.addIntoSorted(conflictParams, zoneInfo.index);
                        break;
                    case SymbolTableConstants.LOCAL:
                    case SymbolTableConstants.GLOBAL: {
                        String conflictName = zoneInfo.publicName() ;
                        if (zoneInfo.isCommon()) {
                            conflictName = "common" + zoneInfo.commonName + ILUtils.toString(zoneInfo.accessTree) ;
                        }
                        conflictGlobals = new TapList<>(conflictName, conflictGlobals);
                        break;
                    }
                    default:
                        break;
                }
                conflict = conflict.tail;
            }
            conflictParams = conflictParams.tail;
            if (TapIntList.length(conflictParams)+TapList.length(conflictGlobals)>1) {
                // Protect from cases where there remains only one "visible" variable in conflict.
                int firstParam =
                    conflictParams == null ? actualParams.length : conflictParams.head;
                String conflictArgsString = "";
                while (conflictParams != null) {
                    conflictArgsString = conflictArgsString + ' ' + conflictParams.head;
                    conflictParams = conflictParams.tail;
                }
                if (conflictGlobals != null) {
                    conflictArgsString = conflictArgsString + " and";
                }
                while (conflictGlobals != null) {
                    conflictArgsString = conflictArgsString + ' ' + conflictGlobals.head;
                    conflictGlobals = conflictGlobals.tail;
                }

                if (TapEnv.mustAdjoint()) {
                  TapEnv.fileWarning(TapEnv.MSG_ALWAYS, (firstParam < actualParams.length ? actualParams[firstParam] : expression), "(DF02) Potential aliasing (serious issue for adjoint) in calling function " + calledUnit.name() + ", between arguments" + conflictArgsString);
                } else {
                  TapEnv.fileWarning(TapEnv.MSG_WARN, (firstParam < actualParams.length ? actualParams[firstParam] : expression), "(DF02) Potential aliasing in calling function " + calledUnit.name() + ", between arguments" + conflictArgsString);
                }
            }
            toConflicts = toConflicts.tail;
        }
    }

    /** Adds into matrix the correspondence from zones at a call site (actualZonesTree)
     * to the public zones of the callee Unit (formalZonesTree). Specifically, adds "true"
     * at row i and column j when the actual zone index i is contained at some position
     * in the actualZoneTree and the formal zone index j is contained at the "same"
     * or "matching" position in the formalZonesTree */
    public static void matchActualToFormalRec(BoolMatrix matrix,
                                        TapList actualZonesTree, TapList formalZonesTree,
                                        SymbolTable calleeSymbolTable, TapList<TapList> dejaVu) {
        TapIntList actuals;
        TapIntList formals;
        TapIntList inFormals;
        int actual;
        int formal;
        ZoneInfo fzi ;
        while (actualZonesTree != null && formalZonesTree != null) {
            if (TapList.contains(dejaVu.tail, actualZonesTree)) {
                actualZonesTree = null;
            } else {
                dejaVu.placdl(actualZonesTree);
                if (formalZonesTree.head instanceof TapIntList && formalZonesTree.tail==null) {
                    // If this leaf of the formal tree is about one or more pointers,
                    // expand pointers formal destinations:
                    TapList formalDestinations = null ;
                    TapIntList lfz = (TapIntList)formalZonesTree.head ;
                    if (calleeSymbolTable!=null) {
                        while (lfz!=null) {
                            fzi = calleeSymbolTable.declaredZoneInfo(lfz.head, SymbolTableConstants.ALLKIND) ;
                            if (fzi!=null && fzi.ptrZoneNb!=-1) {
                                formalDestinations =
                                    TapList.cumulWithOper(formalDestinations, fzi.targetZonesTree,
                                                          SymbolTableConstants.CUMUL_OR) ;
                            }
                            lfz = lfz.tail ;
                        }
                        formalZonesTree.tail = formalDestinations ;
                    }
                }
                if (actualZonesTree.head instanceof TapList) {
                    if (formalZonesTree.head instanceof TapList) {
                        matchActualToFormalRec(matrix, (TapList) actualZonesTree.head,
                                (TapList) formalZonesTree.head, calleeSymbolTable, dejaVu);
                        actuals = null;
                        formals = null;
                    } else {
                        actuals = ZoneInfo.listAllZones((TapList) actualZonesTree.head, true);
                        formals = (TapIntList) formalZonesTree.head;
                    }
                } else {
                    actuals = (TapIntList) actualZonesTree.head;
                    if (formalZonesTree.head instanceof TapList) {
                        formals = ZoneInfo.listAllZones((TapList) formalZonesTree.head, true);
                    } else {
                        formals = (TapIntList) formalZonesTree.head;
                    }
                }
                while (actuals != null) {
                    actual = actuals.head;
                    if (actual>=2) { // Ignore NULL or Undef destinations
                        inFormals = formals;
                        while (inFormals != null) {
                            formal = inFormals.head;
                            if (formal>=2) { // Ignore NULL or Undef destinations
                                fzi = (calleeSymbolTable==null ? null
                                       :calleeSymbolTable.declaredZoneInfo(formal, SymbolTableConstants.ALLKIND)) ;
                                if (fzi==null || !fzi.comesFromAllocate()) { // Ignore zones of "allocate" commands.
                                    matrix.set(actual, formal, true);
                                }
                            }
                            inFormals = inFormals.tail;
                        }
                    }
                    actuals = actuals.tail;
                }
                actualZonesTree = actualZonesTree.tail;
                formalZonesTree = formalZonesTree.tail;
            }
        }
    }

    /**
     * Sends an error message for each call actual parameter which is
     * certainly overwritten by the call and which is not a var reference.
     */
    private void detectSegmentationFaults(CallArrow callArrow, Tree[] actualParams) {
        Unit calledUnit = callArrow.destination;
        BoolVector certainlyWrittenZones = calledUnit.unitInOutCertainlyW();
        boolean[] certainlyWrittenParams = new boolean[actualParams.length + 1];
        ZoneInfo zoneInfo;
        for (int i = calledUnit.paramElemsNb() - 1; i >= 0; i--) {
            zoneInfo = calledUnit.paramElemZoneInfo(i);
            if (zoneInfo != null
                    && zoneInfo.kind() == SymbolTableConstants.PARAMETER
                    && zoneInfo.index <= actualParams.length
                    && certainlyWrittenZones.get(i)) {
                certainlyWrittenParams[zoneInfo.index] = true;
            }
        }
        TapIntList argZones;
        for (int i = 0; i < actualParams.length; i++) {
            if (!ILUtils.isNullOrNone(actualParams[i])
                    && actualParams[i].opCode() != ILLang.op_nameEq) {
                argZones =
                        curSymbolTable.listOfZonesOfValue(actualParams[i], null, curInstruction);
                if (argZones == null && certainlyWrittenParams[i + 1]
                        && !calledUnit.takesArgumentByValue(i + 1, callArrow.origin.language())) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, actualParams[i], "(DF01) Illegal non-reference value for output argument " + (i + 1) + " of procedure " + calledUnit.name());
                }
            }
        }
    }

    private void checkOverwrittenLoopIndices(Block block, BoolVector writtenZones) {
        if (writtenZones != null) {
            LoopBlock enclosingLoopBlock = block.enclosingLoop();
            HeaderBlock enclosingLoopHeader;
            Instruction doInstruction;
            Tree indexTree;
            TapIntList indexZonesList;
            while (enclosingLoopBlock != null) {
                enclosingLoopHeader = enclosingLoopBlock.header();
                if (enclosingLoopHeader != block
                        // The write of i done by "DO i=..." itself is not dirty!
                        && enclosingLoopHeader.isADoLoop()) {
                    doInstruction = enclosingLoopHeader.instructions.head;
                    indexTree = doInstruction.tree.down(3).down(1);
                    indexZonesList = ZoneInfo.listAllZones(
                            curSymbolTable.treeOfZonesOfValue(indexTree, null,
                                    doInstruction, null), true);
                    if (intersectsZoneRks(writtenZones, indexZonesList, null)) {
                        enclosingLoopHeader.dirtyOverwrittenIndex = true;
                    }
                }
                enclosingLoopBlock = enclosingLoopBlock.enclosingLoop();
            }
        }
    }

    /**
     * Computes the default in-out signature for an intrinsic, an external or a varFunction:<br>
     * All vars possibly read and possibly written, except for the
     * function result, which is never read and completely written,
     * and for the I-O streams and all system-predefined symbols, which are never read nor written.
     * [llh: the following is arbitrary and may be discussed:]
     * We also assume that variables in COMMON are not read nor writen,
     * i.e. that the external routine does not touch the COMMON, even if it might see it.
     */
    public static void setExternalOrIntrinsicDefaultInOutInfo(Unit unit) {
        int n = unit.paramElemsNb();
        unit.unitInOutN = new BoolVector(n);
        unit.unitInOutR = new BoolVector(n);
        unit.unitInOutN.setTrue();
        unit.unitInOutR.setTrue();
        unit.unitInOutW = new BoolVector(n);
        unit.unitInOutRW = new BoolVector(n);
        if (unit.isIntrinsic()) {  // => parameters not overwritten
            unit.unitInOutW.setFalse();
            unit.unitInOutRW.setFalse();
        } else { //external or varFunction => parameters may be overwritten
            unit.unitInOutW.setTrue();
            unit.unitInOutRW.setTrue();
        }
        boolean isMpiRoutine = MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language()) ;
        TapIntList ioIndices = unit.callGraph().zoneRksOfIO() ;
        TapIntList ioAndChannelIndices = unit.callGraph().zoneRksOfChannelOrIO() ;
        TapIntList inIOAndChannelIndices = ioAndChannelIndices ;
        while (inIOAndChannelIndices!=null) {
            int ioIndex = inIOAndChannelIndices.head ;
            boolean touchesChannel = (isMpiRoutine && !TapIntList.contains(ioIndices, ioIndex)) ;
            unit.unitInOutN.set(ioIndex, true);
            unit.unitInOutR.set(ioIndex, touchesChannel) ;
            unit.unitInOutW.set(ioIndex, touchesChannel);
            unit.unitInOutRW.set(ioIndex, touchesChannel);
            inIOAndChannelIndices = inIOAndChannelIndices.tail ;
        }
        SymbolTable languageRootSymbolTable = unit.callGraph().languageRootSymbolTable(unit.language());
        TapIntList systemPredefinedZones = languageRootSymbolTable.systemPredefinedZones();
        int systemZone;
        while (systemPredefinedZones != null) {
            systemZone = systemPredefinedZones.head;
            unit.unitInOutN.set(systemZone, true);
            unit.unitInOutR.set(systemZone, unit.isExternal()); // Externals (only) may read system zones.
            unit.unitInOutW.set(systemZone, false);
            unit.unitInOutRW.set(systemZone, false);
            systemPredefinedZones = systemPredefinedZones.tail;
        }
        // Choice is to assume that zones in COMMON are not read nor written by an external or intrinsic
        TapIntList inCommonZones = languageRootSymbolTable.inCommonZones();
        // Choice is to assume that C variables declared "const" at the global level
        // are constants and no one is supposed to write them
        TapIntList globalConstantZones = null ;
        if (unit.callGraph().cRootSymbolTable() != null) {
            globalConstantZones = unit.callGraph().cRootSymbolTable().constantZones();
        }
        for (int pi=unit.paramElemsNb()-1 ; pi>=0 ; --pi) {
            ZoneInfo zi = unit.paramElemZoneInfo(pi) ;
            if (zi!=null && !TapIntList.contains(ioAndChannelIndices, zi.zoneNb)) {
                if ((zi.isHidden
                     // In C, we consider that external routines may touch extern variables:
                     && !(unit.isC() && unit.isExternal()
                          && zi.zoneNb>=languageRootSymbolTable.firstDeclaredZone(SymbolTableConstants.ALLKIND)
                          && zi.zoneNb<languageRootSymbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND)))
                    || TapIntList.contains(inCommonZones, zi.zoneNb)) {
                    unit.unitInOutN.set(pi, true);
                    unit.unitInOutR.set(pi, false);
                    unit.unitInOutW.set(pi, false);
                    unit.unitInOutRW.set(pi, false);
                } else if (TapIntList.contains(globalConstantZones, zi.zoneNb)) {
                    unit.unitInOutN.set(pi, true);
                    unit.unitInOutR.set(pi, true);
                    unit.unitInOutW.set(pi, false);
                    unit.unitInOutRW.set(pi, false);
                } else if (zi.isResult()) {
                    unit.unitInOutN.set(pi, false);
                    unit.unitInOutR.set(pi, false);
                    unit.unitInOutW.set(pi, true);
                    unit.unitInOutRW.set(pi, false);
                }
            }
        }
        if (!unit.isIntrinsic() && unit.callersThatCall()!=null && !unit.isMadeForInline) {
            String unitSort;
            if (unit.isInterface()) {
                unitSort = "Interface ";
            } else {
                unitSort = "External routine ";
            }
            Unit oneCallerUnit = unit.callersThatCall().head.origin ;
            TapEnv.pushRelatedUnit(oneCallerUnit) ;
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DF05) "
                    + unitSort + unit.name() + ", called by " + unit.callerUnitNamesThatCall()
                    + ", is assumed default In:" + unit.unitInOutR.or(unit.unitInOutRW).toString(unit.paramElemsNb())
                    + " and Out:" + unit.unitInOutW.or(unit.unitInOutRW).toString(unit.paramElemsNb()));
            TapEnv.popRelatedUnit();
        }
    }

}
