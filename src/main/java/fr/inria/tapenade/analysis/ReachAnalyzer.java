/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.*;
import fr.inria.tapenade.utils.*;

/** A very simple analyzer to find places that the control flow can reach.
 * The analysis works by setting each Block's fields "reachUp"
 * and "reachDown", which are initially "false". */
public final class ReachAnalyzer extends DataFlowAnalyzer {

    private static final int FLOW = 1 ;
    private static final int BACKFLOW = 2 ;

    /** Whether we are propagating FLOW or BACKFLOW */
    private int fgPhase;

    /** Create a ReachAnalyzer, given the CallGraph "cg" on which it will run. */
    private ReachAnalyzer(CallGraph cg) {
        super(cg, "Reach analysis", null);
    }

    /** Main trigger method. Runs reach analysis under the given rootUnits. */ 
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<Unit> rootUnits) {
        new ReachAnalyzer(callGraph).run(rootUnits);
    }

    /** Runs reach analysis, bottom-up from the leaves of the CallGraph.
     * As a result, fills the backReachUp and reachDown fields of each Block. */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        runBottomUpAnalysis(rootUnits);
    }

    @Override
    protected Object initializeCGForUnit() {
        return null;
    }

    @Override
    protected void initializeCGForRootUnit() {
    }

    @Override
    protected boolean analyze() {
        fgPhase = BACKFLOW ;
        analyzeBackward(null, null, null);
        fgPhase = FLOW ;
        return analyzeForward(null, null, null);
    }

    @Override
    protected void initializeFGForUnit() {
    }

    @Override
    protected void initializeFGForInitBlock() {
        if (fgPhase==BACKFLOW) {
            curBlock.backReachDown = true ;
            if (curBlock instanceof ExitBlock) curBlock.backReachUp = true ;
        } else {
            curBlock.reachUp = true ;
            if (curBlock instanceof EntryBlock) curBlock.reachDown = true ;
        }
    }

    @Override
    protected void initializeFGForBlock() {
        if (fgPhase==BACKFLOW) {
            curBlock.backReachUp = false ;
            curBlock.backReachDown = false ;
        } else {
            curBlock.reachUp = false ;
            curBlock.reachDown = false ;
        }
    }

    @Override
    protected boolean accumulateValuesFromUpstream() {
        boolean reaches = false ;
        Block fromBlock ;
        TapList<FGArrow> arrows = curBlock.backFlow();
// System.out.println(" ACCUMULATE FROM UPSTREAM "+curBlock+" backflow:"+arrows) ;
        while (arrows!=null && !reaches) {
            fromBlock = arrows.head.origin ;
// System.out.println("   FROMBLOCK:"+fromBlock+" reachDown:"+fromBlock.reachDown) ;
            if (fromBlock.reachDown) {
                reaches = true ;
            }
            arrows = arrows.tail ;
        }
// System.out.println(" ACCUMULATED:"+reaches+" TO COMPARE WITH:"+curBlock.reachUp) ;
        return reaches ;
    }

    @Override
    protected boolean accumulateValuesFromDownstream() {
        boolean reaches = false ;
        Block toBlock ;
        TapList<FGArrow> arrows = curBlock.flow();
        while (arrows!=null && !reaches) {
            toBlock = arrows.head.destination ;
            if (toBlock.backReachUp) {
                reaches = true ;
            }
            arrows = arrows.tail ;
        }
        return reaches ;
    }

    @Override
    protected boolean compareUpstreamValues() {
        if (fgPhase==BACKFLOW) {
            boolean alreadyReached = curBlock.backReachUp ;
            curBlock.backReachUp = true ;
            return !alreadyReached ;
        } else {
            boolean alreadyReached = curBlock.reachUp ;
            curBlock.reachUp = true ;
            return !alreadyReached ;
        }
    }

    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        return propagateThroughBlock() ;
    }

    @Override
    protected boolean propagateValuesBackwardThroughBlock() {
        return propagateThroughBlock() ;
    }

    /** @return true iff control is not blocked through expression */
    private boolean propagateThroughBlock() {
// System.out.println(" PROPAGATE THROUGH "+curBlock) ;
        TapList<Instruction> instructions = curBlock.instructions;
        boolean arrives = true;
        while (arrives && instructions != null) {
            curInstruction = instructions.head;
            arrives = controlGoesThrough(curInstruction.tree) ;
// System.out.println("   THROUGH "+curInstruction.tree+" ==> "+arrives) ;
            instructions = instructions.tail;
        }
        curInstruction = null;
// System.out.println(" END PROPAGATE THROUGH "+curBlock+" ARRIVES:"+arrives) ;
        return arrives;
    }

    private boolean controlGoesThrough(Tree expression) {
        boolean goesThrough = true ;
        switch (expression.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign:
                if (!controlGoesThrough(expression.down(2)))
                    goesThrough = false ;
                break ;
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expression, curSymbolTable);
// System.out.println("    CALLED UNIT:"+calledUnit);
                if (calledUnit.isStandard() && !calledUnit.exitBlock.reachUp)
                    goesThrough = false ;
                break ;
            }
            case ILLang.op_stop:
                goesThrough = false ;
                break ;
        }
        return goesThrough ;
    }

    @Override
    protected boolean compareDownstreamValues() {
        if (fgPhase==BACKFLOW) {
            boolean alreadyReached = curBlock.backReachDown ;
            curBlock.backReachDown = true ;
            return !alreadyReached ;
        } else {
            boolean alreadyReached = curBlock.reachDown ;
            curBlock.reachDown = true ;
            return !alreadyReached ;
        }
    }

    @Override
    protected boolean terminateFGForUnit() {
        if (fgPhase==BACKFLOW) {
            boolean reachesEntry = curUnit.entryBlock.backReachDown ;
            boolean changed = (reachesEntry!=curUnit.entryBlock.backReachUp) ;
            curUnit.entryBlock.backReachUp = reachesEntry ;
// System.out.println("TERMINATE UNIT "+curUnit+" changed:"+changed) ;
            return changed;
        } else {
            boolean reachesEnd = curUnit.exitBlock.reachUp ;
            boolean changed = (reachesEnd!=curUnit.exitBlock.reachDown) ;
            curUnit.exitBlock.reachDown = reachesEnd ;
// System.out.println("TERMINATE UNIT "+curUnit+" changed:"+changed) ;
            return changed;
        }
    }

}
