/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.*;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

/**
 * Class that holds one activity pattern of a Unit and its blocks, together with
 * the results of several data-flow analysis on this Unit for this activity pattern.
 */
public final class ActivityPattern {

    private Unit unit;
    private DiffPattern diffPattern;
    private TapList<ActivityPattern> callingPatterns;
    private BoolVector callActivity;
    private BoolVector exitActivity;
    private BoolVector callUsefulness;
    private BoolVector exitUsefulness;
    private BoolVector nonZeroAdjointComing;
    private TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionADActivities;
    private BoolVector zonesOfVarsHaveDiff;
    //TODO: It is wrong to have only one zonesMPIiReceived per Unit
    // and even worse, sized after the Unit's private SymbolTable:
    // these vectors vary according to the execution path,
    // and they should also vary with local scopes being opened and closed!
    // It's the reason why we (temporarily) need to store its length in zonesMPIiReceivedLength
    private BoolVector zonesMPIiReceived;
    private int zonesMPIiReceivedLength;
    private boolean generalizable;
    private BoolVector staticActivity;
    private BlockStorage<TapList<BoolVector>> activities;
    private BlockStorage<TapList<BoolVector>> usefulnesses;
    private BlockStorage<TapList<BoolVector>> reqXs;
    private BlockStorage<TapList<BoolVector>> avlXs;
    private BoolVector entryReqX;
    private BoolVector exitReqX;
    private BoolVector entryAvlX;
    private BoolVector exitAvlX;
    private BoolMatrix reqXEffects;
    private BoolVector reqXEffectsAdded;
    private BoolVector avlXEffectsVisible;
    private BoolVector avlXEffectsAdded;
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenesses;
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenessesCkp;
    private BoolVector entryDiffLivenessesCkp;
    private BoolVector entryDiffLivenessesCkpOnDiffPtr;
    private TapList<TapPair<Block, TapPair<BoolVector, BoolVector>>> fragmentDiffLivenessesCkp = null ;
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwrites;
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwritesCkp;
    private BoolVector entryDiffOverwritesCkp;
    private BoolVector entryDiffOverwritesCkpOnDiffPtr;
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> tbrs;
    private BoolVector localExitTBR;
    private BoolVector localExitTBROnDiffPtr;
    private BlockStorage<TapList<TapPair<TapList<Instruction>, TapList<Instruction>>>> recomps;
    private BoolVector adTBRTopDownContext;
    private BoolVector adTBRTopDownContextOnDiffPtr;
    private BlockStorage<int[]> mayBeRecomputed;
    private BlockStorage<int[]> butRemainsInFwdSweep;
    private boolean isForcedActive;
    private boolean dontDiff;
    private boolean isContext;
    private DiffLivenessAnalyzer.Context diffLivenessTopDownContext;

    /**
     * Useful on Exit of this unit.
     */
    private BoolVector usefulOnExit;
    private BoolVector variedOnCall;

    public ActivityPattern(Unit unit, boolean generalizable, boolean forceActive, int diffKind) {
        super();
        this.unit = unit;
        this.generalizable = generalizable;
        this.isForcedActive = forceActive;
        initTypes(diffKind);
// if (unit!=null && ("VecFree".equals(unit.name) || "sum".equals(unit.name))) {System.out.println("NEW ACTIVITYPATTERN (1) @"+Integer.toHexString(hashCode())+" :: "+this) ; new Throwable().printStackTrace();}
    }

    public ActivityPattern(Unit unit, BoolVector variedOnCall, BoolVector usefulOnExit,
                           boolean generalizable, int diffKind) {
        super();
        this.unit = unit;
	//REMARK-MVO::statement to be removed.
        // this.diffPattern = diffPattern;
        this.variedOnCall = variedOnCall;
        this.usefulOnExit = usefulOnExit;
        this.generalizable = generalizable;
        initTypes(diffKind);
// if (unit!=null && ("VecFree".equals(unit.name) || "sum".equals(unit.name))) {System.out.println("NEW ACTIVITYPATTERN (2) @"+Integer.toHexString(hashCode())+" :: "+this) ; new Throwable().printStackTrace();}
    }

    /**
     * Used in ReqExplicit analysis to initialize a standard ActivityPattern with its corresponding context ActivityPattern.
     */
    public void copyReqExplicitInfos(ActivityPattern model) {
        if (model.reqXEffects != null) {
            this.reqXEffects = model.reqXEffects.copy();
        }
        if (model.reqXEffectsAdded != null) {
            this.reqXEffectsAdded = model.reqXEffectsAdded.copy();
        }
        if (model.avlXEffectsVisible != null) {
            this.avlXEffectsVisible = model.avlXEffectsVisible.copy();
        }
        if (model.avlXEffectsAdded != null) {
            this.avlXEffectsAdded = model.avlXEffectsAdded.copy();
        }
        if (model.entryReqX != null) {
            this.entryReqX = model.entryReqX.copy();
        }
        if (model.exitReqX != null) {
            this.exitReqX = model.exitReqX.copy();
        }
        if (model.entryAvlX != null) {
            this.entryAvlX = model.entryAvlX.copy();
        }
        if (model.exitAvlX != null) {
            this.exitAvlX = model.exitAvlX.copy();
        }
    }

    public static void walkUpToPatternOf(ActivityPattern pattern, Unit unit,
                                         TapList<ActivityPattern> collected, TapList<ActivityPattern> dejaVu) {
        if (!TapList.contains(dejaVu.tail, pattern)) {
            dejaVu.placdl(pattern);
            if (pattern.unit == unit) {
                collected.placdl(pattern);
            } else {
                TapList<ActivityPattern> inCallingPatterns = pattern.callingPatterns;
                while (inCallingPatterns != null) {
                    walkUpToPatternOf(inCallingPatterns.head, unit, collected, dejaVu);
                    inCallingPatterns = inCallingPatterns.tail;
                }
            }
        }
    }

    /**
     * Sets to "val" the value of annotation "name" for ActivityPattern "pattern" on this "tree".
     *
     * @param tree    The tree on which we set the annotation.
     * @param name    The name of the annotation we set.
     * @param pattern The ActivityPattern for which we set the annotation.
     * @param val     The value we set for this annotation.
     */
    public static void setAnnotationForActivityPattern(Tree tree, ActivityPattern pattern, String name,
                                                       Object val) {
// if ("PointerActiveExpr".equals(name)
//     && "initx".equals(ILUtils.getIdentString(tree))
//     && "comp_mtc".equals(pattern.unit.name())
//     && val!=null && "sum".equals(((ActivityPattern)val).unit.name())
//     ) {
//   System.out.println("SET CALLED ACTIVITY ON "+tree+" FROM "+pattern+" TO "+val) ;
//   new Throwable().printStackTrace() ;
// }
        TapPair<String, Object> annotPair = tree.getAnnotationPair(name);
        if (annotPair == null) {
            annotPair = tree.setAnnotation(name, null);
        }
        TapPair<ActivityPattern, Object> patternPair =
                TapList.assq(pattern, (TapList<TapPair<ActivityPattern, Object>>) annotPair.second);
        if (patternPair == null) {
            patternPair = new TapPair<>(pattern, null);
            annotPair.second = new TapList<>(patternPair, (TapList<TapPair>) annotPair.second);
        }
        patternPair.second = val;
    }

    /**
     * Removes the annotation "name" for ActivityPattern "pattern" on this "tree".
     *
     * @param tree    The tree on which we remove the annotation.
     * @param name    The name of the annotation we remove.
     * @param pattern The ActivityPattern for which we remove the annotation.
     */
    public static void removeAnnotationForActivityPattern(Tree tree, ActivityPattern pattern, String name) {
        TapPair<String, Object> annotPair = tree.getAnnotationPair(name);
        if (annotPair != null) {
            TapList<TapPair<ActivityPattern, Object>> toPatternAList =
                    new TapList<>(null, (TapList<TapPair<ActivityPattern, Object>>) annotPair.second);
            TapList<TapPair<ActivityPattern, Object>> inPatternAList = toPatternAList;
            while (inPatternAList.tail != null) {
                if (inPatternAList.tail.head.first == pattern) {
                    inPatternAList.tail = inPatternAList.tail.tail;
                } else {
                    inPatternAList = inPatternAList.tail;
                }
            }
            annotPair.second = toPatternAList.tail;
        }
    }

    /**
     * @param tree    The tree on which we search the annotation.
     * @param name    The name of the annotation we search.
     * @param pattern The ActivityPattern for which we search the annotation.
     * @return The value found for this annotation, otherwise null.
     */
    public static Object getAnnotationForActivityPattern(Tree tree, ActivityPattern pattern, String name) {
        TapPair<String, Object> annotPair = tree.getAnnotationPair(name);
        if (annotPair != null) {
            return TapList.cassq(pattern, (TapList<TapPair<ActivityPattern, Object>>) annotPair.second);
        } else {
            return null;
        }
    }

    public static void setInstructionBoolInfo(BlockStorage<int[]> info, Instruction instruction, int infoRk, boolean value) {
        Block block = instruction.block;
        if (block.rank >= 0) {
            int[] intArray = info.retrieve(block);
            int rank = block.instructions.rank(instruction);
            int code = intArray[rank] ;
            if (infoRk==1) {
                if (value && code%2==0)
                    code = code+1 ;
                else if (!value && code%2!=0)
                    code = code+1 ;
            } else if (infoRk==2) {
                if (value && code%4<2)
                    code = code+2 ;
                else if (!value && code%4>=2)
                    code = code -2 ;
            } else {
                TapEnv.toolWarning(-1, "(setInstructionBoolInfo) infoRk="+infoRk+" not implemented ") ;
            }
            intArray[rank] = code ;
        }
    }

    public static boolean getInstructionBoolInfo(BlockStorage<int[]> info, Instruction instruction, int infoRk) {
        Block block = instruction.block;
        if (block.rank < 0) {
            return false;
        }
        int[] intArray = info.retrieve(block);
        int rank = block.instructions.rank(instruction);
        int code = intArray[rank] ;
        if (infoRk==1) {
            return code%2!=0 ;
        } else if (infoRk==2) {
            return code%4>=2 ;
        } else {
            TapEnv.toolWarning(-1, "(setInstructionBoolInfo) infoRk="+infoRk+" not implemented ") ;
            return false ;
        }
    }

    /**
     * @return The Unit that this activity pattern refers/applies to.
     */
    public Unit unit() {
        return unit;
    }

    /**
     * @return The diffPattern that lead to the creation of this activity pattern.
     */
    public DiffPattern diffPattern() {
        return diffPattern;
    }

    public void setDiffPattern(DiffPattern diffPattern) {
        this.diffPattern = diffPattern;
    }

    /**
     * @return the list of calling contexts that will use this activity pattern.
     */
    public TapList<ActivityPattern> callingPatterns() {
        return callingPatterns;
    }

    /**
     * Result of activity analysis: Active zones upon entry into each subroutine.
     * Storage, for each "procedure" Unit, of a BoolVector describing
     * active zones upon call of the Unit.
     * This is the "or" of all activities just before each call to the Unit.
     * This is not redundant with the "internal" analysis result
     * ((BoolVector[])topDownContexts.retrieve(unit))[VoC]
     * because this is the real "activity" and that was only the "varied" info.
     * Upon finishing activity analysis of the Unit, this value may be manipulated
     * to force the differentiate call sites to perform diff var initialization
     * upstream the diff call.
     * In debugActivity mode only, contains the variedness upon call.
     */
    public BoolVector callActivity() {
        return callActivity;
    }

    public void setCallActivity(BoolVector callActivity) {
        this.callActivity = callActivity;
    }

    /**
     * Result of activity analysis: Active zones upon exit from each subroutine.
     * Storage, for each "procedure" Unit, of a BoolVector describing
     * active zones upon exit from the Unit.
     * In other words, this should be equal to the Unit's "callActivities"
     * times the Unit's dependence matrix.
     * Upon finishing activity analysis of the Unit, this value may be manipulated
     * to force the differentiate call sites to perform diff var initialization
     * downstream the diff call.
     * In debugActivity mode only, contains the variedness upon return.
     */
    public BoolVector exitActivity() {
        return exitActivity;
    }

    public void setExitActivity(BoolVector exitActivity) {
        this.exitActivity = exitActivity;
    }

    /**
     * Result of activity analysis, useful only in debugActivity mode,
     * contains the usefulness upon call.
     */
    public BoolVector callUsefulness() {
        return callUsefulness;
    }

    public void setCallUsefulness(BoolVector callUsefulness) {
        this.callUsefulness = callUsefulness;
    }

    /**
     * Result of activity analysis, useful only in debugActivity mode,
     * contains the usefulness upon return.
     */
    public BoolVector exitUsefulness() {
        return exitUsefulness;
    }

    public void setExitUsefulness(BoolVector exitUsefulness) {
        this.exitUsefulness = exitUsefulness;
    }

    /**
     * Result of activity analysis, BoolVector of the (union of) public usefulness
     * at the calling location(s) of this Unit. A true in this BoolVector means that
     * there will be a non-zero adjoint value arriving into the downstream of
     * the adjoint of this Unit. This is useful for the adjoint mechanism for
     * by-value arguments: if there is a non-zero adjoint coming, then the
     * adjoint Unit needs a second adjoint (local) derivative that will be
     * eventually added into the adjoint derivative actual argument.
     */
    public BoolVector nonZeroAdjointComing() {
        return nonZeroAdjointComing;
    }

    public void setNonZeroAdjointComing(BoolVector nonZeroAdjointComing) {
        this.nonZeroAdjointComing = nonZeroAdjointComing;
    }

    /**
     * Result of activity analysis: about functions passed as arguments.
     * For each Unit, contains an array of the same length as the arguments list
     * which contains, for each argument which is a function name and
     * which is "active", the callActivity and exitUsefulness of this function.
     */
    public TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionADActivities() {
        return varFunctionADActivities;
    }

    /**
     * Result of activity analysis: BoolVector of the (diffKind) zones
     * for which there will be a differentiated variable.
     */
    public BoolVector zonesOfVarsHaveDiff() {
        return zonesOfVarsHaveDiff;
    }

    /**
     * BoolVector of the zones for which this Unit does a AMPI_Irecv(),
     * and therefore (if the zone is active) the adjoint must run a AMPI_Turn().
     */
    public BoolVector zonesMPIiReceived() {
        return zonesMPIiReceived;
    }

    public int zonesMPIiReceivedLength() {
        return zonesMPIiReceivedLength;
    }

    /**
     * false if this pattern was created to exactly match a user request,
     * and should therefore not be generalized. This should only happen
     * for diff roots.
     */
    public boolean isGeneralizable() {
        return generalizable;
    }

    /**
     * Zones that are active all through the analyzed curUnit, and that are not
     * even mentioned in the [before|after]Activ vectors because they are unused, unmodified
     * and are therefore filtered out by activity analysis.
     * Cf results*.results[index] in ResultsSet() in validation code SEISM.
     */
    public BoolVector staticActivity() {
        return staticActivity;
    }

    public void setStaticActivity(BoolVector staticActivity) {
        this.staticActivity = staticActivity;
    }

    /**
     * Activities for all blocks in this unit with this activity Pattern.
     */
    public BlockStorage<TapList<BoolVector>> activities() {
        return activities;
    }

    public void setActivities(BlockStorage<TapList<BoolVector>> activities) {
        this.activities = activities;
    }

    /**
     * Usefulnesses for all blocks in this unit with this activity Pattern.
     */
    public BlockStorage<TapList<BoolVector>> usefulnesses() {
        return usefulnesses;
    }

    public void setUsefulnesses(BlockStorage<TapList<BoolVector>> usefulnesses) {
        this.usefulnesses = usefulnesses;
    }

    /**
     * The published result of ReqX analysis: vectors of
     * ReqX zones between each pair of successive instructions.
     */
    public BlockStorage<TapList<BoolVector>> reqXs() {
        return reqXs;
    }

    public void setReqXs(BlockStorage<TapList<BoolVector>> reqXs) {
        this.reqXs = reqXs;
    }

    /**
     * The other published result of ReqX analysis: vectors of
     * AvlX zones between each pair of successive instructions.
     */
    public BlockStorage<TapList<BoolVector>> avlXs() {
        return avlXs;
    }

    public void setAvlXs(BlockStorage<TapList<BoolVector>> avlXs) {
        this.avlXs = avlXs;
    }

    /**
     * A secondary published result of ReqX analysis:
     * BoolVector of ReqX zones (public) upon entry into the Unit.
     */
    public BoolVector entryReqX() {
        return entryReqX;
    }

    public void setEntryReqX(BoolVector entryReqX) {
        this.entryReqX = entryReqX;
    }

    /**
     * A secondary published result of ReqX analysis:
     * BoolVector of ReqX zones (public) upon exit from the Unit.
     */
    public BoolVector exitReqX() {
        return exitReqX;
    }

    public void setExitReqX(BoolVector exitReqX) {
        this.exitReqX = exitReqX;
    }

    /**
     * A secondary published result of ReqX analysis:
     * BoolVector of AvlX zones (public) upon entry into the Unit.
     */
    public BoolVector entryAvlX() {
        return entryAvlX;
    }

    public void setEntryAvlX(BoolVector entryAvlX) {
        this.entryAvlX = entryAvlX;
    }

    /**
     * A secondary published result of ReqX analysis:
     * BoolVector of AvlX zones (public) upon exit from the Unit.
     */
    public BoolVector exitAvlX() {
        return exitAvlX;
    }

    public void setExitAvlX(BoolVector exitAvlX) {
        this.exitAvlX = exitAvlX;
    }

    /**
     * Temporary result, only for communication from BOTTOMUP_1
     * cgPhase to TOPDOWN_2 cgPhase: BoolMatrix of the effect of
     * each Unit on its ReqX info, upwards any call.
     */
    public BoolMatrix reqXEffects() {
        return reqXEffects;
    }

    public void setReqXEffects(BoolMatrix reqXEffects) {
        this.reqXEffects = reqXEffects;
    }

    /**
     * Temporary result, only for communication from BOTTOMUP_1
     * cgPhase to TOPDOWN_2 cgPhase: BoolVector of the zones
     * added unconditionally to ReqX, upwards any call.
     */
    public BoolVector reqXEffectsAdded() {
        return reqXEffectsAdded;
    }

    public void setReqXEffectsAdded(BoolVector reqXEffectsAdded) {
        this.reqXEffectsAdded = reqXEffectsAdded;
    }

    /**
     * Temporary result, only for communication from BOTTOMUP_3
     * cgPhase to TOPDOWN_4 cgPhase: BoolVector of the zones
     * NOT completely masked by each Unit to the AvlX analysis.
     */
    public BoolVector avlXEffectsVisible() {
        return avlXEffectsVisible;
    }

    public void setAvlXEffectsVisible(BoolVector avlXEffectsVisible) {
        this.avlXEffectsVisible = avlXEffectsVisible;
    }

    /**
     * Temporary result, only for communication from BOTTOMUP_3
     * cgPhase to TOPDOWN_4 cgPhase: BoolVector of the zones
     * added unconditionally to AvlX, downwards any call.
     */
    public BoolVector avlXEffectsAdded() {
        return avlXEffectsAdded;
    }

    public void setAvlXEffectsAdded(BoolVector avlXEffectsAdded) {
        this.avlXEffectsAdded = avlXEffectsAdded;
    }

    /**
     * Result of diffLiveness analysis: for each Unit that may be
     * differentiated in reverse nocheckpoint mode ("split") or in
     * tangent mode, for each Block, a TapList of diffLiveness
     * BoolVector's between each pair of successive instructions.
     */
    public BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenesses() {
        return diffLivenesses;
    }

    public void setDiffLivenesses(BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenesses) {
        this.diffLivenesses = diffLivenesses;
    }

    /**
     * Result of diffLiveness analysis: for each Unit that may be
     * differentiated in reverse checkpoint mode ("joint"),
     * for each Block, a TapList of diffLiveness BoolVector's
     * between each pair of successive instructions.
     */
    public BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenessesCkp() {
        return diffLivenessesCkp;
    }

    public void setDiffLivenessesCkp(BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffLivenessesCkp) {
        this.diffLivenessesCkp = diffLivenessesCkp;
    }

    /**
     * Result of 1st (BOTTOMUP) sweep of diffLiveness analysis:
     * for each Unit, the BoolVector of its live (i.e. required)
     * arguments upon call in the reverse-checkpointed mode.
     * In other words, these are the variables of the Unit that are diff-live
     * upon entry, if we suppose the diff-live set upon exit is emptyset.
     */
    public BoolVector entryDiffLivenessesCkp() {
        return entryDiffLivenessesCkp;
    }

    public void setEntryDiffLivenessesCkp(BoolVector entryDiffLivenessesCkp) {
        this.entryDiffLivenessesCkp = entryDiffLivenessesCkp;
    }

    public BoolVector entryDiffLivenessesCkpOnDiffPtr() {
        return entryDiffLivenessesCkpOnDiffPtr;
    }

    public void setEntryDiffLivenessesCkpOnDiffPtr(BoolVector entryDiffLivenessesCkpOnDiffPtr) {
        this.entryDiffLivenessesCkpOnDiffPtr = entryDiffLivenessesCkpOnDiffPtr;
    }

    /** Returns the diff liveness info about one given checkpointed code fragment,
     * here represented by its "main" Block "ckpBlock" */
    public TapPair<BoolVector, BoolVector> fragmentDiffLivenessesCkp(Block ckpBlock) {
        return TapList.cassq(ckpBlock, fragmentDiffLivenessesCkp) ;
    }

    public void setFragmentDiffLivenessesCkp(Block ckpBlock, TapPair<BoolVector, BoolVector> info) {
        TapPair<Block, TapPair<BoolVector, BoolVector>> found =
            TapList.assq(ckpBlock, fragmentDiffLivenessesCkp) ;
        if (found==null) {
            found = new TapPair<>(ckpBlock, null) ;
            fragmentDiffLivenessesCkp = new TapList<>(found, fragmentDiffLivenessesCkp) ;
        }
        found.second = info ;
    }

    /**
     * Result of diffLiveness analysis: for each Unit that may be
     * differentiated in reverse nocheckpoint mode ("split") or in
     * tangent mode, for each Block, a TapList of diffOverwrite
     * BoolVector's between each pair of successive instructions.
     */
    public BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwrites() {
        return diffOverwrites;
    }

    public void setDiffOverwrites(BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwrites) {
        this.diffOverwrites = diffOverwrites;
    }

    /**
     * Result of diffLiveness analysis: for each Unit that may be
     * differentiated in reverse checkpoint mode ("joint"),
     * for each Block, a TapList of diffOverwrite BoolVector's
     * between each pair of successive instructions.
     */
    public BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwritesCkp() {
        return diffOverwritesCkp;
    }

    public void setDiffOverwritesCkp(BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> diffOverwritesCkp) {
        this.diffOverwritesCkp = diffOverwritesCkp;
    }

    /**
     * Result of 1st (BOTTOMUP) sweep of diffLiveness analysis:
     * for the current Unit, the BoolVector of its diff-overwritten
     * arguments upon call in the reverse-checkpointed mode.
     * In other words, these are the variables of the Unit that are diff-overwritten
     * upon entry, if we suppose the diff-overwritten set upon exit is emptyset.
     */
    public BoolVector entryDiffOverwritesCkp() {
        return entryDiffOverwritesCkp;
    }

    public void setEntryDiffOverwritesCkp(BoolVector entryDiffOverwritesCkp) {
        this.entryDiffOverwritesCkp = entryDiffOverwritesCkp;
    }

    public BoolVector entryDiffOverwritesCkpOnDiffPtr() {
        return entryDiffOverwritesCkpOnDiffPtr;
    }

    public void setEntryDiffOverwritesCkpOnDiffPtr(BoolVector entryDiffOverwritesCkpOnDiffPtr) {
        this.entryDiffOverwritesCkpOnDiffPtr = entryDiffOverwritesCkpOnDiffPtr;
    }

    /**
     * Main result of TBR analysis.
     * TBR set just before each instruction, for each Block of
     * each Unit of the CallGraph.
     * Actually, the TBR result is also stored as a Tree annotation,
     * therefore this field is rarely used, e.g. for the save of local vars
     * at the end of fwd sweeps (split mode), or for dumps.
     */
    public BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> tbrs() {
        return tbrs;
    }

    public void setTbrs(BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> tbrs) {
        this.tbrs = tbrs;
    }

    /**
     * Intermediate result of the 1st, bottom-up phase:
     * TBR set upon exit, due only to operations inside the Unit.
     */
    public BoolVector localExitTBR() {
        return localExitTBR;
    }

    public void setLocalExitTBR(BoolVector localExitTBR) {
        this.localExitTBR = localExitTBR;
    }

    public BoolVector localExitTBROnDiffPtr() {
        return localExitTBROnDiffPtr;
    }

    public void setLocalExitTBROnDiffPtr(BoolVector localExitTBROnDiffPtr) {
        this.localExitTBROnDiffPtr = localExitTBROnDiffPtr;
    }

    /**
     * Another result of TBR analysis.
     * For each Unit of the CallGraph, for each Block, the TapList
     * of the recomputations that we want to place on the way backwards
     * between successive instructions. Each of these recomputations
     * is itself a TapList of Instruction's.
     */
    public BlockStorage<TapList<TapPair<TapList<Instruction>, TapList<Instruction>>>> recomps() {
        return recomps;
    }

    public void setRecomps(BlockStorage<TapList<TapPair<TapList<Instruction>, TapList<Instruction>>>> recomps) {
        this.recomps = recomps;
    }

    /**
     * Initialization for TBR analysis.
     * For the current Unit, the boolvector of the zones that are already TBR
     * at the point of entry into the current Unit.
     */
    public BoolVector adTBRTopDownContext() {
        return adTBRTopDownContext;
    }

    public void setAdTBRTopDownContext(BoolVector adTBRTopDownContext) {
        this.adTBRTopDownContext = adTBRTopDownContext;
    }

    public BoolVector adTBRTopDownContextOnDiffPtr() {
        return adTBRTopDownContextOnDiffPtr;
    }

    public void setAdTBRTopDownContextOnDiffPtr(BoolVector adTBRTopDownContextOnDiffPtr) {
        this.adTBRTopDownContextOnDiffPtr = adTBRTopDownContextOnDiffPtr;
    }

    /**
     * Another result of TBR analysis.
     * For each Instruction, true if this Instruction is considered for recomputing its
     * result rather than storing it (Store on kill).
     */
    public BlockStorage<int[]> mayBeRecomputed() {
        return mayBeRecomputed;
    }

    /**
     * Another result of TBR analysis.
     * For each Instruction, true if, although this instruction may be recomputed,
     * it must also be kept in the forward sweep.
     */
    public BlockStorage<int[]> butRemainsInFwdSweep() {
        return butRemainsInFwdSweep;
    }

    /**
     * This boolean is used to force active the "dummy" ActivityPattern that we create
     * for a VarFunction that is called only in hidden places (external units) so that we
     * must suppose it is active but we don't know in which way! cf F90:v459.
     */
    public boolean isForcedActive() {
        return isForcedActive;
    }

    public void setForcedActive(boolean forcedActive) {
        isForcedActive = forcedActive;
    }

    /**
     * Conversely to "isForcedActive", this boolean is used to force non-differentiation
     * of this Unit with respect to this ActivityPattern. cf F77:lha87 and ADActivityAnalyzer.
     */
    public boolean isDontDiff() {
        return dontDiff;
    }

    public void setDontDiff(boolean dontDiff) {
        this.dontDiff = dontDiff;
    }

    /**
     * True when this activity pattern is for "context" differentiation,
     * i.e. outside the differentiated sub-call-tree.
     */
    public boolean isContext() {
        return isContext;
    }

    public void setContext(boolean context) {
        isContext = context;
    }

    /**
     * The context object used in the top-down phase of DiffLiveness analysis
     * to accumulate the contexts of the call sites of a function
     * into a location that analysis of this function will retrieve.
     */
    public DiffLivenessAnalyzer.Context diffLivenessTopDownContext() {
        return diffLivenessTopDownContext;
    }

    public void setDiffLivenessTopDownContext(DiffLivenessAnalyzer.Context diffLivenessTopDownContext) {
        this.diffLivenessTopDownContext = diffLivenessTopDownContext;
    }

    /**
     * Varied on Call of this unit.
     */
    public BoolVector variedOnCall() {
        return variedOnCall;
    }

    /**
     * Useful on Exit of this unit.
     */
    public BoolVector usefulOnExit() {
        return usefulOnExit;
    }

    private void initTypes(int diffKind) {
        if (unit != null && unit.hasParamElemsInfo()) {
            int shapeLength = unit.publicZonesNumber(TapEnv.diffKind()) ;
            callActivity = new BoolVector(shapeLength);
            exitActivity = new BoolVector(shapeLength);
            if (unit.privateSymbolTable() != null) {
                int privateLength = unit.privateSymbolTable().declaredZonesNb(diffKind) ;
                zonesOfVarsHaveDiff = (privateLength<=0 ? null : new BoolVector(privateLength));
                zonesMPIiReceived = (privateLength<=0 ? null : new BoolVector(privateLength));
                zonesMPIiReceivedLength = privateLength;
            }
            int nbArgs = unit.formalArgumentsNb();
            varFunctionADActivities = new TapTriplet[nbArgs];
            for (int i = nbArgs - 1; i >= 0; --i) {
                varFunctionADActivities[i] = null;
            }
        }
    }

    public void registerCallingActivityPattern(ActivityPattern callingPattern) {
        if (!TapList.contains(callingPatterns, callingPattern)) {
            callingPatterns = new TapList<>(callingPattern, callingPatterns);
        }
    }

    public boolean isCalledBy(Unit caller, ActivityPattern callerActivityPattern, TapList<ActivityPattern> dejaVu) {
        TapList<ActivityPattern> inCallingPatterns = callingPatterns;
        ActivityPattern callingPattern;
        boolean called = false;
        while (!called && inCallingPatterns != null) {
            callingPattern = inCallingPatterns.head;
            if (callingPattern != null && !TapList.contains(dejaVu, callingPattern)) {
                called = callingPattern == callerActivityPattern && callingPattern.unit == caller ||
                        callingPattern.isCalledBy(caller, callerActivityPattern, new TapList<>(callingPattern, dejaVu));
            }
            inCallingPatterns = inCallingPatterns.tail;
        }
        return called;
    }

    public ActivityPattern getCallingActivityPattern(Unit caller, TapList<ActivityPattern> dejaVu) {
        TapList<ActivityPattern> inCallingPatterns = callingPatterns;
        ActivityPattern callingPattern;
        ActivityPattern found = null;
        while (found == null && inCallingPatterns != null) {
            callingPattern = inCallingPatterns.head;
            if (callingPattern != null && !TapList.contains(dejaVu, callingPattern)) {
                if (callingPattern.unit == caller) {
                    found = callingPattern;
                } else {
                    found = callingPattern.getCallingActivityPattern(caller, new TapList<>(callingPattern, dejaVu));
                }
            }
            inCallingPatterns = inCallingPatterns.tail;
        }
        return found;
    }

    public void initializeMayBeRecomputed() {
        mayBeRecomputed = new BlockStorage<>(unit);
        int[] intArray;
        Block block;
        for (TapList<Block> blocks = unit.allBlocks(); blocks != null; blocks = blocks.tail) {
            block = blocks.head;
            intArray = new int[TapList.length(block.instructions)];
            for (int i = intArray.length - 1; i >= 0; --i) {
                intArray[i] = 0 ;
            }
            mayBeRecomputed.store(block, intArray);
        }
    }

    public void initializeButRemainsInFwdSweep() {
        butRemainsInFwdSweep = new BlockStorage<>(unit);
        int[] intArray;
        Block block;
        for (TapList<Block> blocks = unit.allBlocks(); blocks != null; blocks = blocks.tail) {
            block = blocks.head;
            intArray = new int[TapList.length(block.instructions)];
            for (int i = intArray.length - 1; i >= 0; --i) {
                intArray[i] = 0 ;
            }
            butRemainsInFwdSweep.store(block, intArray);
        }
    }

    public boolean isDisconnected() {
        return diffPattern == null && callingPatterns == null;
    }

    /**
     * @return true if this activityPattern is active,
     * ie not all variables are inactive.
     */
    public boolean isActive() {
        if (isForcedActive) {
            return true;
        }
        int shapeLength = unit.publicZonesNumber(TapEnv.diffKind()) ;//was unit.paramElemsNb();
        return ((callActivity != null)
                && (exitActivity != null)
                && !callActivity.isFalse(shapeLength)
                //[llh 24/6/2016] I'm not sure of this &&. Shouldn't it be a || ? TODO: test that!
                && !exitActivity.isFalse(shapeLength))
                ||
                ((TapEnv.reqExplicitAnalyzer() != null) && TapEnv.reqExplicitAnalyzer().isPointerActiveUnit(this));
    }

    public void dump() throws java.io.IOException {
        int activityLen = unit.publicZonesNumber(TapEnv.diffKind()) ;
        String activityMapName = (unit.publicSymbolTable()==null ? "[x"+unit.rank() : "[d"+unit.publicSymbolTable().rank())+":r]" ;

        int unitPublicLen = unit.paramElemsNb() ;
        String unitPublicMapName = "[x" + unit.rank() + ":a]";
        TapEnv.print("     " + this.toString() + " called by {");
        TapList<ActivityPattern> callers = this.callingPatterns();
        while (callers != null) {
            TapEnv.print(" @" + Integer.toHexString(callers.head.hashCode()));
            callers = callers.tail;
        }
        TapEnv.println(this.diffPattern() != null ? "} also a diff root: " + this.diffPattern() : "}");
        TapEnv.print("       activity:");
        TapEnv.dumpBoolVector(this.callActivity(), activityLen, activityMapName);
        TapEnv.print("=>");
        TapEnv.dumpBoolVector(this.exitActivity(), activityLen, activityMapName);
        TapEnv.println((this.isForcedActive() ? " forcedActive" : "") + (this.isDontDiff() ? " dontDiff" : ""));
        TapEnv.print("       ReqX:    ");
        TapEnv.dumpBoolVector(this.entryReqX(), unitPublicLen, unitPublicMapName);
        TapEnv.print("=>");
        TapEnv.dumpBoolVector(this.exitReqX(), unitPublicLen, unitPublicMapName);
        TapEnv.println();
        TapEnv.print("       AvlX:    ");
        TapEnv.dumpBoolVector(this.entryAvlX(), unitPublicLen, unitPublicMapName);
        TapEnv.print("=>");
        TapEnv.dumpBoolVector(this.exitAvlX(), unitPublicLen, unitPublicMapName);
    }

    @Override
    public String toString() {
        String result = "AP@" + Integer.toHexString(hashCode()) + " {";
        if (callingPatterns != null) {
            result = result + "called, ";
        }
        if (diffPattern != null) {
            result = result + "diffRoot, ";
        }
        if (isContext) {
            result = result + "context, ";
        }
        if (diffPattern == null && callingPatterns == null) {
            result = result + "not connected, ";
        }
        if (isForcedActive) {
            result = result + "forcedActive, ";
        }
        if (dontDiff) {
            result = result + "but don't diff, ";
        }
        return result + "on " + unit + "}";
    }
}
