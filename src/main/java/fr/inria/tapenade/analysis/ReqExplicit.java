/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.Block;
import fr.inria.tapenade.representation.BlockStorage;
import fr.inria.tapenade.representation.CallArrow;
import fr.inria.tapenade.representation.CallGraph;
import fr.inria.tapenade.representation.DiffRoot;
import fr.inria.tapenade.representation.Directive;
import fr.inria.tapenade.representation.FGConstants;
import fr.inria.tapenade.representation.HeaderBlock;
import fr.inria.tapenade.representation.ILUtils;
import fr.inria.tapenade.representation.Instruction;
import fr.inria.tapenade.representation.SymbolDecl;
import fr.inria.tapenade.representation.SymbolTable;
import fr.inria.tapenade.representation.SymbolTableConstants;
import fr.inria.tapenade.representation.TapEnv;
import fr.inria.tapenade.representation.TapList;
import fr.inria.tapenade.representation.TypeSpec;
import fr.inria.tapenade.representation.Unit;
import fr.inria.tapenade.representation.VariableDecl;
import fr.inria.tapenade.representation.ZoneInfo;
import fr.inria.tapenade.representation.MPIcallInfo;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * Storage activity analyzer.
 * <p>
 * This analysis finds, for each location in the code, the pointer variables
 * whose derivatives must be computed, assigned, allocated, or deallocated,
 * even if they are not (yet) active. This analysis must be done after
 * activity is completed, because it uses activity (cf 1st case of reqX).
 * All usages of this analysis can be found by grep "PointerActiveUsage".
 * <p>
 * Example, with pointers:<br>
 * {@code allocate(V) ; V=0.0 ; P=&V ; ... V=X ; Y=*P ; deallocate(V)}
 * <br>
 * P is not active at the time of {@code P=&V}. However this must be differentiated
 * into {@code Pd = &Vd} because later {@code Y=*P} will be active and differentiated into
 * Yd=*Pd which requires Pd to be explicitly pointing to Vd. Moreover, Vd
 * must be allocated and deallocated together with V.
 * <br>
 * The "Required" Storage Activity info is a BoolVector of ALLKIND
 * (but applies only to pointers?), with a boolean true when the zone's
 * derivative may be needed (allocated) downstream.
 * <br>
 * When this boolean is true downstream an allocate for this pointer,
 * then we must add an allocate for the differentiated pointer.
 * Conversely, there is an "avlX" info analysis ("maybe-present") that
 * propagates forward the zones of the pointers for which the derivative
 * pointer may be present (allocated) at the current location.
 * When this boolean is true upstream a "free" of this pointer,
 * then we must add a free of for the differentiated pointer.
 *
 * <pre>
 * Feature: derives from DataFlowAnalyzer : OK
 * Feature: able to analyze recursive code: OK
 * Feature: distinguishes structure elemts: OK
 * Feature: takes  care  of  pointer dests: OK
 * Feature: refine on loop-local variables: only during TOPDOWN_2 phase
 * Feature: std method for call arguments : OK
 * Feature: able to analyze sub-flow-graph: OK
 * </pre>
 */
public final class ReqExplicit extends DataFlowAnalyzer {

    /**
     * Constant that codes for the 1st cgPhase.
     */
    private static final int BOTTOMUP_1 = 1;
    /**
     * Constant that codes for the 2nd cgPhase.
     */
    private static final int TOPDOWN_2 = 2;
    /**
     * Constant that codes for the 3rd cgPhase.
     */
    private static final int BOTTOMUP_3 = 3;
    /**
     * Constant that codes for the 4th cgPhase.
     */
    private static final int TOPDOWN_4 = 4;

    /**
     * Current info (reqX or avlX) on the downstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<BoolVector> infosDown;
    /**
     * Same as infosDown (reqX or avlX), but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<BoolVector> infosCycleDown;
    /**
     * Current info (reqX or avlX) on the upstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<BoolVector> infosUp;
    /**
     * Same as infosUp (reqX or avlX), but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<BoolVector> infosCycleUp;

    /**
     * Current reqX effect info on the downstream end of each Block.
     * Temporarily used inside the BOTTOMUP_1 cgPhase.
     */
    private BlockStorage<BoolMatrix> reqXEffectsDown;
    /**
     * Current reqX effect info on the upstream end of each Block.
     * Temporarily used inside the BOTTOMUP_1 cgPhase.
     */
    private BlockStorage<BoolMatrix> reqXEffectsUp;
    /**
     * Current reqX "added" effect info on the downstream end of each Block.
     * Temporarily used inside the BOTTOMUP_1 cgPhase.
     */
    private BlockStorage<BoolVector> reqXEffectsAddedDown;
    /**
     * Current reqX "added" effect info on the upstream end of each Block.
     * Temporarily used inside the BOTTOMUP_1 cgPhase.
     */
    private BlockStorage<BoolVector> reqXEffectsAddedUp;
    /**
     * Current avlX "Masked" effect info on the downstream end of each Block.
     * Temporarily used inside the BOTTOMUP_3 cgPhase.
     */
    private BlockStorage<BoolVector> avlXEffectsVisibleDown;
    /**
     * Current avlX "Masked" effect info on the upstream end of each Block.
     * Temporarily used inside the BOTTOMUP_3 cgPhase.
     */
    private BlockStorage<BoolVector> avlXEffectsVisibleUp;
    /**
     * Current avlX "added" effect info on the downstream end of each Block.
     * Temporarily used inside the BOTTOMUP_3 cgPhase.
     */
    private BlockStorage<BoolVector> avlXEffectsAddedDown;
    /**
     * Current avlX "added" effect info on the upstream end of each Block.
     * Temporarily used inside the BOTTOMUP_3 cgPhase.
     */
    private BlockStorage<BoolVector> avlXEffectsAddedUp;
    /**
     * Current info (reqX or avlX) being propagated
     * through a Block by the current analysis.
     */
    private BoolVector infoTmp;
    /**
     * Same as "infoTmp" (reqX or avlX), but for "cycling" info.
     */
    private BoolVector infoTmpCycle;
    /**
     * Current reqX effect info being propagated upwards
     * through a Block by the BOTTOMUP_1 effect analysis.
     */
    private BoolMatrix reqXEffectTmp;
    /**
     * Current reqX "added" effect info being propagated upwards
     * through a Block by the BOTTOMUP_1 effect analysis.
     */
    private BoolVector reqXEffectAddedTmp;

    /**
     * Current avlX "masked" effect info being propagated upwards
     * through a Block by the BOTTOMUP_3 effect analysis.
     */
    private BoolVector avlXEffectVisibleTmp;
    /**
     * Current avlX "added" effect info being propagated upwards
     * through a Block by the BOTTOMUP_3 effect analysis.
     */
    private BoolVector avlXEffectAddedTmp;
    /**
     * Info (reqX or avlX) that will be accumulated where control flow merges.
     */
    private BoolVector additionalInfo;

    /**
     * Info (reqXEffect) that will be accumulated where control flow merges.
     */
    private BoolMatrix additionalReqXEffect;
    /**
     * Info (reqXEffectAdded) that will be accumulated where control flow merges.
     */
    private BoolVector additionalReqXEffectAdded;

    /**
     * Info (avlXEffectAdded) that will be accumulated where control flow merges.
     */
    private BoolVector additionalAvlXEffectAdded;
    /**
     * Info (avlXEffectMasked) that will be accumulated where control flow merges.
     */
    private BoolVector additionalAvlXEffectVisible;
    /**
     * The "diffKind" of the activity analyzer.
     */
    private final int diffKind;
    /**
     * The current phase of this ReqX analysis at the call graph level.
     * One of {BOTTOMUP_1, TOPDOWN_2, BOTTOMUP_3, TOPDOWN_4}.
     */
    private int cgPhase;

    /** Number of declared zones, excluding local variables */
    private int nDZexit = -9 ;

    /** Number of declared zones of kind "diffKind" */
    private int nDRZ = -9 ;

    /**
     * Legend to find the zones in ReqX, AvlX, and effect vectors, and in the rows of the effect matrices.
     */
    private String vecLegend;

    /**
     * Legend to find the zones in the columns of the "effect" matrices (i.e. wrt the exit Block).
     */
    private String vecExitLegend;

    /**
     * The result of activity analysis for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> unitActivities;

    /**
     * The result of usefulness analysis for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> unitUsefulnesses;

    /**
     * The result of the TOPDOWN_2 phase (ReqX) for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> unitReqXInfos;

    /**
     * The result of the current Top-Down analyses (reqX or avlX) for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> curUnitInfos;

    /**
     * Creates a Storage Activity Analyzer.
     */
    private ReqExplicit(CallGraph callGraph) {
        super(callGraph, "Storage Activity analysis", TapEnv.traceReqExplicit());
        this.conservativeValue = true;
        diffKind = TapEnv.diffKind();
    }

    /**
     * Main trigger method. Runs Storage Activity analysis under the rootUnits.
     */
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<DiffRoot> rootUnits) {
        TapEnv.setReqExplicitAnalyzer(new ReqExplicit(callGraph));
        TapEnv.reqExplicitAnalyzer().run(DiffRoot.collectUnits(rootUnits));
    }

    /**
     * @return an array of booleans, one for each formal argument,
     * true when the differentiated formal argument is needed
     * because its memory location is reqX either upon entry or upon exit.
     */
    public static boolean[] formalArgsPointerActivity(ActivityPattern pattern, int nbArgs) {
        Unit unit = pattern.unit();
        boolean[] result = new boolean[nbArgs + 1];
        for (int i = result.length - 1; i >= 0; --i) {
            result[i] = false;
        }
        if (unit.hasParamElemsInfo()) {
            ZoneInfo zoneInfo;
            for (int i = unit.paramElemsNb() - 1; i >= 0; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                if (zoneInfo != null) {
                    switch (zoneInfo.kind()) {
                        case SymbolTableConstants.PARAMETER:
                            if (isPointerActiveArg(pattern, i)) {
                                result[zoneInfo.index - 1] = true;
                            }
                            break;
                        case SymbolTableConstants.RESULT:
                            if (isPointerActiveArg(pattern, i)) {
                                result[nbArgs] = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        return result;
    }

// Primitives for the topmost fixpoint analysis on the complete CallGraph:

    /**
     * @return true when the given FORMAL elementary argument to the Unit must have a
     * differentiated counterpart (although it may be non-active) because
     * this formal argument (either parameter or global or side-effect...)
     * is a pointer and is either
     * both overwritten during the call and reqX upon exit from the call
     * or is both reqX upon entry in the call and accessed (read or written) during the call.
     * Also returns true for non-pointer formal args that are marked reqX
     * upon call entry, which is the case for variables whose address
     * is assigned into a reqX pointer inside the call (cf F90:cm22).
     */
    public static boolean isPointerActiveArg(ActivityPattern curActivity, int rankInShape) {
        Unit unit = curActivity.unit();
        if (!unit.hasParamElemsInfo()) {
            return false;
        }
        BoolVector exitReqX = curActivity.exitReqX();
        BoolVector entryAvlX = curActivity.entryAvlX();
        if (exitReqX == null && entryAvlX == null) {
            return false;
        }
        boolean result;
        BoolVector entryReqX = curActivity.entryReqX();
        if (TypeSpec.isA(unit.paramElemZoneInfo(rankInShape).type, SymbolTableConstants.POINTERTYPE)) {
            boolean rkW = true;
            boolean rkRorW = true;
            if (unit.unitInOutR != null) {
                rkW = unit.unitInOutW.get(rankInShape) || unit.unitInOutRW.get(rankInShape);
                rkRorW = rkW || unit.unitInOutR.get(rankInShape);
            }
            result =
                    // test for "reqX on exit and overwritten":
                    (rkW && exitReqX != null && exitReqX.get(rankInShape))
                            ||
                            // test for "avlX on entry and overwritten":
                            (rkW && entryAvlX != null && entryAvlX.get(rankInShape))
                            ||
                            // test for "reqX on entry and (read or written)":
                            (rkRorW && entryReqX != null && entryReqX.get(rankInShape));
        } else {
            // A plain formal argument is reqX if its address is assigned into a reqX pointer:
            // Maybe this is only a patch until the code is cleaned up...
            result = entryReqX != null && entryReqX.get(rankInShape);
        }
        return result;
    }

    /**
     * Annotate this tree as ReqX for the given activity context.
     */
    public static void setAnnotatedPointerActive(ActivityPattern pattern, Tree tree) {
        if (tree != null) {
            ActivityPattern.setAnnotationForActivityPattern(tree, pattern, "PointerActiveExpr", Boolean.TRUE);
        }
    }

    /**
     * True when this tree has been annotated pointer-active for the given activity context,
     * meaning that it will have a derivative because of pointers to active variables.
     */
    public static boolean isAnnotatedPointerActive(ActivityPattern pattern, Tree tree) {
        if (tree == null) {
            return false;
        }
        Object annotationPointerActive =
            ActivityPattern.getAnnotationForActivityPattern(tree, pattern, "PointerActiveExpr");
        return annotationPointerActive != null && (Boolean)annotationPointerActive;
    }

// Primitives for the fixpoint analysis "Analyze()" on one Flow Graph:

    /**
     * If there is an annotation about pointer-active on "fromTree" for activity "curActivity",
     * copies it onto "toTree".
     */
    public static void copyAnnotation(ActivityPattern curActivity, Tree fromTree, Tree toTree) {
        Object annotationPointerActive =
            ActivityPattern.getAnnotationForActivityPattern(fromTree, curActivity, "PointerActiveExpr");
        if (annotationPointerActive != null) {
            ActivityPattern.setAnnotationForActivityPattern(toTree, curActivity, "PointerActiveExpr", annotationPointerActive);
        }
    }

    /**
     * Run Storage Activity analysis on the CallGraph under "rootUnits"
     * or on the complete CallGraph if "rootUnits" is null.
     * Runs bottom-up, then top-down, then bottom-up, then top-down.
     */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        String topAnalysisName = curAnalysisName;
        // Force a ReqX analysis on units in the differentiated call tree, even if they are not
        // active in the sense of ADActivityAnalyzer, but they might be in the sense of ReqX:
        TapList<Unit> allCallees =
                (rootUnits == null ? curCallGraph.sortedComputationalUnits() : Unit.allCalleesMulti(rootUnits));
        while (allCallees != null) {
            curUnit = allCallees.head;
            if ((curUnit.isStandard() || curUnit.isExternal()) && curUnit.activityPatterns == null) {
                ActivityPattern passiveActivityPattern = new ActivityPattern(curUnit, true, false, diffKind);
                curUnit.activityPatterns = new TapList<>(passiveActivityPattern, curUnit.activityPatterns);
            }
            allCallees = allCallees.tail;
        }

        if (TapEnv.mustContext()) {
            // In "-context" mode, all procedures reachable without going through a diffRoot may be
            // differentiated as contexts so we add a new ActivityPattern for that before the beginning
            // of the 1st cgPhase. This context ActivityPattern is passive.
            TapList<Unit> callersOfRootUnits = Unit.allCallersMulti(rootUnits);
            TapList<Unit> contextUnits = Unit.allCalleesMultiAvoiding(callersOfRootUnits, rootUnits);
            while (contextUnits != null) {
                curUnit = contextUnits.head;
                if (curUnit.isStandard()
                        // [llh] why avoid recursive Units ?
                        && !TapList.contains(curCallGraph.getRecursivityUnits(), curUnit)) {
                    // Replacing "false" below with "true" fixes problem on set07/v480,
                    //  but breaks validations opa-gyre and opa-nemo!
                    ActivityPattern contextActivityPattern = new ActivityPattern(curUnit, true, false, diffKind);
                    curUnit.activityPatterns = new TapList<>(contextActivityPattern, curUnit.activityPatterns);
                    contextActivityPattern.setContext(true);
                }
                contextUnits = contextUnits.tail;
            }
            // Then set rootUnits to null, to force the coming analyses to run on the full CallGraph:
            rootUnits = null;
        }

        cgPhase = BOTTOMUP_1;
        curAnalysisName = "Bottom-up Required Active Storage Analysis";
        runBottomUpAnalysis(rootUnits);
        cgPhase = TOPDOWN_2;
        curAnalysisName = "Top-down Required Active Storage Analysis";
        runTopDownAnalysis(rootUnits);
        // The following were needed only during the TOPDOWN_2 cgPhase:
        curActivity.setReqXEffects(null);
        curActivity.setReqXEffectsAdded(null);
        cgPhase = BOTTOMUP_3;
        curAnalysisName = "Bottom-up Present Active Storage Analysis";
        runBottomUpAnalysis(rootUnits);
        cgPhase = TOPDOWN_4;
        curAnalysisName = "Top-down Present Active Storage Analysis";
        runTopDownAnalysis(rootUnits);
        // The following were needed only during the TOPDOWN_4 cgPhase:
        curActivity.setAvlXEffectsVisible(null);
        curActivity.setAvlXEffectsAdded(null);
        // The topDownContexts is used only inside each top-down analysis:
        topDownContexts = null;
        curAnalysisName = topAnalysisName;
        // Remove called activity annotations inserted by this ReqX analysis, and that are indeed passive:
        cleanEmptyActivities();

    }

    /**
     * Initializes the context of Unit "unit" for the current cgPhase of this analysis.
     */
    @Override
    protected Object initializeCGForUnit() {
        // externalShape may be null for Modules, or sometimes for VarFunction's
        // (i.e. the units of functions passed as arguments), but I [llh] am
        // not sure this is not a bug...
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        while (activities != null) {
            curActivity = activities.head;
            switch (cgPhase) {
                case BOTTOMUP_1:
                    if (curUnit.rank() >= 0) {
                        if (curUnit.hasParamElemsInfo() && (curUnit.isExternal() || curUnit.isVarFunction())) {
                            if (curActivity.reqXEffects() == null) {
                                curActivity.setReqXEffects(buildDefaultReqXEffect(curUnit));
                                curActivity.setReqXEffectsAdded(buildDefaultReqXEffectAdded(curUnit));
                            }
                        } else {
                            curActivity.setReqXEffects(null);
                            curActivity.setReqXEffectsAdded(null);
                        }
                        if (curUnit.hasParamElemsInfo()) {
                            int shapeLength = curUnit.paramElemsNb();
                            curActivity.setEntryReqX(new BoolVector(shapeLength));
                            curActivity.setExitReqX(new BoolVector(shapeLength));
                            if (curUnit.publicSymbolTable() != null) {
                                nDZ = (curUnit.publicZonesNumber4!=null
                                       ? curUnit.publicZonesNumber(SymbolTableConstants.ALLKIND)
                                       : curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
                                vecLegend = "[d" + curUnit.publicSymbolTable().rank() + ":a]";
                            }
                        }
                    }
                    break;
                case TOPDOWN_2:
                    break;
                case BOTTOMUP_3:
                    if (curUnit.rank() >= 0) {
                        if (curUnit.hasParamElemsInfo() && (curUnit.isExternal() || curUnit.isVarFunction())) {
                            if (curActivity.avlXEffectsVisible() == null) {
                                curActivity.setAvlXEffectsVisible(buildDefaultAvlXEffectVisible(curUnit));
                                curActivity.setAvlXEffectsAdded(buildDefaultAvlXEffectAdded(curUnit));
                            }
                        } else {
                            curActivity.setAvlXEffectsVisible(null);
                            curActivity.setAvlXEffectsAdded(null);
                        }
                    }
                    break;
                default: //TOPDOWN_4
                    if (curUnit.hasParamElemsInfo()) {
                        int shapeLength = curUnit.paramElemsNb();
                        curActivity.setEntryAvlX(new BoolVector(shapeLength));
                        curActivity.setExitAvlX(new BoolVector(shapeLength));
                    }
            }
            activities = activities.tail;
        }
        return null;
    }

    /**
     * Run Storage Activity analysis on the curUnit. Runs backwards towards pointers'
     * definitions and malloc, then forwards towards pointers' free().
     * Return value is meaningless when we are in a runTopDownAnalysis().
     */
    @Override
    protected boolean analyze() {
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        int activityNb = TapList.length(activities);
        int activityRk = 0;
        boolean analysisIsOutOfDate = false;
        while (activities != null) {
            curActivity = activities.head;
            ++activityRk;
            if (curUnit.hasSource()) {
                TapEnv.printOnTrace(15, " (ActivityPattern " + activityRk + "/" + activityNb + " @" + Integer.toHexString(curActivity.hashCode()) + ":)");
            }
            switch (cgPhase) {
                case BOTTOMUP_1:
                    analysisIsOutOfDate = analyzeBackward(null, null, null) || analysisIsOutOfDate;
                    break;
                case TOPDOWN_2:
                    curUnitInfos = curActivity.reqXs();
                    if (curUnitInfos == null) {
                        curUnitInfos = new BlockStorage<>(curUnit);
                        curActivity.setReqXs(curUnitInfos);
                    }
                    analyzeBackward(null, null, null);
                    break;
                case BOTTOMUP_3:
                    unitReqXInfos = curActivity.reqXs();
                    if (unitReqXInfos == null) {
                        unitReqXInfos = new BlockStorage<>(curUnit);
                        curActivity.setReqXs(unitReqXInfos);
                    }
                    analysisIsOutOfDate = analyzeForward(null, null, null) || analysisIsOutOfDate;
                    break;
                default: //TOPDOWN_4
                    unitReqXInfos = curActivity.reqXs();
                    if (unitReqXInfos == null) {
                        unitReqXInfos = new BlockStorage<>(curUnit);
                        curActivity.setReqXs(unitReqXInfos);
                    }
                    curUnitInfos = curActivity.avlXs();
                    if (curUnitInfos == null) {
                        curUnitInfos = new BlockStorage<>(curUnit);
                        curActivity.setAvlXs(curUnitInfos);
                    }
                    analyzeForward(null, null, null);
            }
            activities = activities.tail;
        }
        return analysisIsOutOfDate;
    }

    /**
     * Concluding operations on the given curUnit,
     * called at the end of each bottom-up or top-down cgPhase.
     */
    @Override
    protected void terminateCGForUnit() {
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        while (activities != null) {
            curActivity = activities.head;
            if (cgPhase == TOPDOWN_2) {
                curUnitInfos = curActivity.reqXs();
                BoolVector entryReqX =
                        curUnitInfos.retrieve(curUnit.entryBlock()).head;
                if (entryReqX != null) {
                    if (curActivity.entryReqX() == null) {
                        curActivity.setEntryReqX(entryReqX);
                    } else {
                        curActivity.entryReqX().cumulOr(entryReqX);
                    }
                }
            } else if (cgPhase == TOPDOWN_4) {
                curUnitInfos = curActivity.avlXs();
                BoolVector exitAvlX =
                        curUnitInfos.retrieve(curUnit.exitBlock()).head;
                if (exitAvlX != null) {
                    if (curActivity.exitAvlX() == null) {
                        curActivity.setExitAvlX(exitAvlX);
                    } else {
                        curActivity.exitAvlX().cumulOr(exitAvlX);
                    }
                }
                // Precompute annotation "PointerActiveExpr" (here on call trees only, so far...) :
                if (curUnit.hasSource()) {
                    if (curActivity.reqXs()!=null && curActivity.avlXs()!=null) {
                        presetPointerActivityAnnotations();
                    }
                }
            }
            activities = activities.tail;
        }
    }

    private void presetPointerActivityAnnotations() {
        BlockStorage<TapList<BoolVector>> unitReqXs = curActivity.reqXs() ;
        BlockStorage<TapList<BoolVector>> unitAvlXs = curActivity.avlXs() ;
        TapList<Block> inBlocks;
        Block block;
        inBlocks = curUnit.allBlocks();
        while (inBlocks != null) {
            block = inBlocks.head;
            presetPointerActivityAnnotations(block,
                    unitReqXs.retrieve(block),
                    unitAvlXs.retrieve(block));
            inBlocks = inBlocks.tail;
        }
    }

    private void presetPointerActivityAnnotations(Block block,
                          TapList<BoolVector> blockReqXs, TapList<BoolVector> blockAvlXs) {
        setCurBlockEtc(block);
        TapList<Instruction> instructions = curBlock.instructions;
        blockReqXs = blockReqXs.tail ;
        while (instructions != null) {
            curInstruction = instructions.head;
            presetPointerActivityAnnotations(curInstruction.tree, null,
                                             blockReqXs.head, blockAvlXs.head) ;
            blockReqXs = blockReqXs.tail ;
            blockAvlXs = blockAvlXs.tail ;
            instructions = instructions.tail;
        }
        curInstruction = null;
    }

    private void presetPointerActivityAnnotations(Tree tree, Tree lhs,
                                   BoolVector afterReqX, BoolVector beforeAvlX) {
        if (tree != null) {
            switch (tree.opCode()) {
                case ILLang.op_plusAssign:
                case ILLang.op_minusAssign:
                case ILLang.op_timesAssign:
                case ILLang.op_divAssign:
                case ILLang.op_assign:
                    presetPointerActivityAnnotations(tree.down(1), lhs, afterReqX, beforeAvlX) ;
                    presetPointerActivityAnnotations(tree.down(2), tree.down(1), afterReqX, beforeAvlX) ;
                    break ;
                case ILLang.op_call: {
                    TapList resultPointerActivity = null ;
                    if (lhs != null) {
                        TapList writtenZonesTree =
                                curSymbolTable.treeOfZonesOfValue(lhs, null, curInstruction, null);
                        resultPointerActivity =
                                DataFlowAnalyzer.buildInfoBoolTreeOfDeclaredZones(
                                        writtenZonesTree, afterReqX,
                                        null, SymbolTableConstants.ALLKIND, curSymbolTable);
                    }
                    // TODO: (Re/Pre)computing the "PointerActiveExpr" annotation on this call is necessary to
                    // avoid some "twice-connected" bugs in FlowGraphDifferentiator, by ensuring consistency between
                    // "diffCallPresent" in DiffLivenessAnalyzer and "diffCallNeeded()" in CallGraphDifferentiator
                    // but it considerably slows down the present "Top-down Present Active Storage Analysis"
                    // (from 0.2s to 10.3s on vossbeck-beps!). TODO: why is it so expensive?
                    if (isPointerActiveCall(tree, curUnit, getCalledUnit(tree, curSymbolTable),
                                            curSymbolTable, curInstruction,
                                            beforeAvlX, afterReqX, resultPointerActivity)) {
                        setAnnotatedPointerActive(curActivity, ILUtils.getCalledName(tree)) ;
                    } else {
                        // the annotation may have been set to true by ReqX. Reset it now to false:
                        ActivityPattern.removeAnnotationForActivityPattern(
                                   ILUtils.getCalledName(tree), curActivity, "PointerActiveExpr") ;
                    }
                    Tree[] arguments = ILUtils.getArguments(tree).children();
                    for (int i = arguments.length - 1; i >= 0; i--) {
                        presetPointerActivityAnnotations(arguments[i], null, afterReqX, beforeAvlX) ;
                    }
                    break ;
                }
                default:
                    if (!tree.isAtom()) {
                        Tree[] subTrees = tree.children();
                        for (int i = subTrees.length - 1; i >= 0; i--) {
                            presetPointerActivityAnnotations(subTrees[i], null, afterReqX, beforeAvlX) ;
                        }
                    }
                    break;
            }
        }
    }

// Primitives for the analysis propagation either "backflow up" or "flow down" each Block:

    /**
     * Set curBlock, curSymbolTable, nDZ, nDRZ
     *
     * @param block The new current Block.
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        if (block == null) {
            vecExitLegend = "[?]";
            vecLegend = "[?]";
        } else {
            if (cgPhase == BOTTOMUP_1 || cgPhase == BOTTOMUP_3) {
                nDZexit = curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                vecExitLegend = "[d" + curUnit.publicSymbolTable().rank() + ":a]";
            }
            vecLegend = "[d" + curSymbolTable.rank() + ":a]";
            nDRZ = curSymbolTable.declaredZonesNb(diffKind) ;
        }
    }

    /**
     * Initializations before each ReqX analysis of "curUnit".
     * Creates the BlockStorage's that will store the info and infoCycle
     * propagated by the current analysis, at the two ends of each Block.
     */
    @Override
    protected void initializeFGForUnit() {
        setCurBlockEtc(curUnit.entryBlock());
        if (cgPhase == BOTTOMUP_1 || cgPhase == TOPDOWN_2) {
            unitActivities = curActivity.activities();
            unitUsefulnesses = curActivity.usefulnesses();
        }
        switch (cgPhase) {
            case BOTTOMUP_1:
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ============== REQUIRED ACTIVE STORAGE ANALYSIS [bottom-up] OF UNIT "
                            + curUnit.name() + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode())
                            + ") : ==============");
                    traceDisplayPrivateZones(curUnit, nDZ, SymbolTableConstants.ALLKIND);
                    TapEnv.printlnOnTrace();
                }
                reqXEffectsUp = new BlockStorage<>(curUnit);
                reqXEffectsDown = new BlockStorage<>(curUnit);
                reqXEffectsAddedUp = new BlockStorage<>(curUnit);
                reqXEffectsAddedDown = new BlockStorage<>(curUnit);
                break;
            case TOPDOWN_2:
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ============== REQUIRED ACTIVE STORAGE ANALYSIS [top-down] OF UNIT "
                            + curUnit.name() + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode())
                            + ") : ==============");
                    traceDisplayPrivateZones(curUnit, nDZ, SymbolTableConstants.ALLKIND);
                    TapEnv.printlnOnTrace();
                }
                infosUp = new BlockStorage<>(curUnit);
                infosDown = new BlockStorage<>(curUnit);
                infosCycleUp = new BlockStorage<>(curUnit);
                infosCycleDown = new BlockStorage<>(curUnit);
                break;
            case BOTTOMUP_3:
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ============== PRESENT ACTIVE STORAGE ANALYSIS [bottom-up] OF UNIT "
                            + curUnit.name() + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode())
                            + ") : ==============");
                    traceDisplayPrivateZones(curUnit, nDZ, SymbolTableConstants.ALLKIND);
                    TapEnv.printlnOnTrace();
                }
                avlXEffectsVisibleUp = new BlockStorage<>(curUnit);
                avlXEffectsVisibleDown = new BlockStorage<>(curUnit);
                avlXEffectsAddedUp = new BlockStorage<>(curUnit);
                avlXEffectsAddedDown = new BlockStorage<>(curUnit);
                break;
            default: //TOPDOWN_4
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ============== PRESENT ACTIVE STORAGE ANALYSIS [top-down] OF UNIT "
                            + curUnit.name() + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode())
                            + ") : ==============");
                    traceDisplayPrivateZones(curUnit, nDZ, SymbolTableConstants.ALLKIND);
                    TapEnv.printlnOnTrace();
                }
                infosUp = new BlockStorage<>(curUnit);
                infosDown = new BlockStorage<>(curUnit);
                infosCycleUp = new BlockStorage<>(curUnit);
                infosCycleDown = new BlockStorage<>(curUnit);
                break;
        }
        setCurBlockEtc(null);
    }

    @Override
    protected void initializeFGForInitBlock() {
        switch (cgPhase) {
            case BOTTOMUP_1: {
                BoolMatrix initMatrix;
                BoolVector initVector;
                initMatrix = new BoolMatrix(curUnit.hasTooManyZones(), nDZ, nDZexit);
                initMatrix.setIdentity();
                reqXEffectsUp.store(curBlock, initMatrix);
                initVector = new BoolVector(nDZ);
                reqXEffectsAddedUp.store(curBlock, initVector);
                break;
            }
            case TOPDOWN_2: {
                BoolVector initReqX = curActivity.exitReqX();
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("   initialized ReqX of :" + curBlock + " to " + initReqX);
                }
                infosUp.store(curBlock, initReqX);
                break;
            }
            case BOTTOMUP_3: {
                BoolVector initVector;
                initVector = new BoolVector(nDZ);
                initVector.setTrue();
                avlXEffectsVisibleDown.store(curBlock, initVector);
                initVector = new BoolVector(nDZ);
                avlXEffectsAddedDown.store(curBlock, initVector);
                break;
            }
            default: { //TOPDOWN_4
                BoolVector initAvlX = curActivity.entryAvlX();
                if (curActivity.entryReqX() != null) {
                    initAvlX.cumulOr(curActivity.entryReqX());
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("   initialized AvlX of :" + curBlock + " to " + initAvlX);
                }
                infosDown.store(curBlock, initAvlX);
                break;
            }
        }
    }

    @Override
    protected void terminateFGForBlock() {
        switch (cgPhase) {
            case TOPDOWN_2: {
                TapList<BoolVector> toReqXs = new TapList<>(null, null);
                infoTmp = infosDown.retrieve(curBlock);
                if (infoTmp == null) {
                    infoTmp = new BoolVector(nDZ);
                }
                infoTmpCycle = infosCycleDown.retrieve(curBlock);
                propagateAndStoreReqXThroughBlock(curBlock, toReqXs);
                curUnitInfos.store(curBlock, toReqXs.tail);
                break;
            }
            case TOPDOWN_4: {
                TapList<BoolVector> toAvlXs = new TapList<>(null, null);
                infoTmp = infosUp.retrieve(curBlock);
                if (infoTmp == null) {
                    infoTmp = new BoolVector(nDZ);
                }
                infoTmpCycle = infosCycleUp.retrieve(curBlock);
                propagateAndStoreAvlXThroughBlock(curBlock, toAvlXs);
                curUnitInfos.store(curBlock, toAvlXs.tail);
                break;
            }
            default:
                break;
        }
    }

    /**
     * Terminates ReqX analysis on "curUnit".
     * Return value is meaningful only in the bottom-up cgPhases:
     * true means that the analysis result has changed since last time.
     */
    @Override
    protected boolean terminateFGForUnit() {
        switch (cgPhase) {
            case BOTTOMUP_1: {
                setCurBlockEtc(curUnit.entryBlock());
                accumulateValuesFromDownstream();
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  == Private Effect info at the entry Block:");
                    TapEnv.printlnOnTrace("    reqXEffect: " + vecLegend + "*" + vecExitLegend);
                    TapEnv.dumpBoolMatrixOnTrace(reqXEffectTmp);
                    TapEnv.printlnOnTrace("    reqXEffectAdded:  " + reqXEffectAddedTmp);
                    TapEnv.printlnOnTrace();
                }
                if (/*reqXEffectNNNTmp == null || */reqXEffectTmp == null) {
                    initializeCumulValue();
                }
                boolean resultChanged = false;
                BoolMatrix oldUnitReqXEffect = curActivity.reqXEffects() ;
                if (oldUnitReqXEffect == null || !reqXEffectTmp.equalsBoolMatrix(oldUnitReqXEffect)) {
                    curActivity.setReqXEffects(reqXEffectTmp);
                    resultChanged = true;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" == NEW reqXEffect for unit " + curUnit.name()
                                + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode()) + ") is:");
                        TapEnv.dumpOnTrace(reqXEffectTmp);
                        TapEnv.printlnOnTrace();
                    }
                }

                BoolVector unitReqXEffectAdded = reqXEffectAddedTmp ;
                BoolVector oldUnitReqXEffectAdded = curActivity.reqXEffectsAdded();
                if (oldUnitReqXEffectAdded == null ||
                    !unitReqXEffectAdded.equals(oldUnitReqXEffectAdded, nDZ)) {
                    curActivity.setReqXEffectsAdded(unitReqXEffectAdded);
                    resultChanged = true;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" == NEW reqXEffectAdded for unit " + curUnit.name()
                                + "(ActivityPattern @" + Integer.toHexString(curActivity.hashCode())
                                + ") is:" + unitReqXEffectAdded);
                    }
                }
                setCurBlockEtc(null);
                // Clean the intermediate results for this curUnit:
                reqXEffectsUp = null;
                reqXEffectsDown = null;
                reqXEffectsAddedUp = null;
                reqXEffectsAddedDown = null;
                return resultChanged;
            }
            case TOPDOWN_2:
                curUnitInfos.store(curUnit.exitBlock(),
                        new TapList<>(
                                infosUp.retrieve(curUnit.exitBlock()),
                                null));
                curUnitInfos.store(curUnit.entryBlock(),
                        new TapList<>(
                                infosDown.retrieve(curUnit.entryBlock()),
                                null));
                infosUp = null;
                infosDown = null;
                infosCycleUp = null;
                infosCycleDown = null;
                return true;
            case BOTTOMUP_3: {
                setCurBlockEtc(curUnit.exitBlock());
                accumulateValuesFromUpstream();
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  == Private Effect info at the exit Block:");
                    TapEnv.printlnOnTrace("    avlXEffectVisible: " + avlXEffectVisibleTmp);
                    TapEnv.printlnOnTrace("    avlXEffectAdded:   " + avlXEffectAddedTmp);
                }
                if (avlXEffectVisibleTmp == null) {
                    initializeCumulValue();
                }
                boolean resultChanged = false;
                BoolVector oldUnitAvlXEffectVisible = curActivity.avlXEffectsVisible();
                if (oldUnitAvlXEffectVisible == null || !avlXEffectVisibleTmp.equals(oldUnitAvlXEffectVisible, nDZ)) {
                    curActivity.setAvlXEffectsVisible(avlXEffectVisibleTmp);
                    resultChanged = true;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" == NEW avlXEffectVisible for unit " + curUnit.name() + " is:" + avlXEffectVisibleTmp);
                    }
                }
                BoolVector oldUnitAvlXEffectAdded = curActivity.avlXEffectsAdded();
                if (oldUnitAvlXEffectAdded == null || !avlXEffectAddedTmp.equals(oldUnitAvlXEffectAdded, nDZ)) {
                    curActivity.setAvlXEffectsAdded(avlXEffectAddedTmp);
                    resultChanged = true;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" == NEW avlXEffectAdded   for unit " + curUnit.name() + " is:" + avlXEffectAddedTmp);
                    }
                }
                setCurBlockEtc(null);
                // Clean the intermediate results for this curUnit:
                avlXEffectsVisibleUp = null;
                avlXEffectsVisibleDown = null;
                avlXEffectsAddedUp = null;
                avlXEffectsAddedDown = null;
                return resultChanged;
            }
            default: //TOPDOWN_4:
                curUnitInfos.store(curUnit.entryBlock(),
                        new TapList<>(
                                infosDown.retrieve(curUnit.entryBlock()),
                                null));
                curUnitInfos.store(curUnit.exitBlock(),
                        new TapList<>(
                                infosUp.retrieve(curUnit.exitBlock()),
                                null));
                unitActivities = null;
                unitUsefulnesses = null;
                infosUp = null;
                infosDown = null;
                infosCycleUp = null;
                infosCycleDown = null;
                return true;
        }
    }

    @Override
    protected void setEmptyCumulAndCycleValues() {
        switch (cgPhase) {
            case BOTTOMUP_1:
                reqXEffectTmp = null;
                reqXEffectAddedTmp = null;
                break;
            case BOTTOMUP_3:
                avlXEffectVisibleTmp = null;
                avlXEffectAddedTmp = null;
                break;
            default: //TOPDOWN_2 or TOPDOWN_4
                infoTmp = null;
                infoTmpCycle = null;
                break;
        }
    }

    @Override
    protected boolean getValueFlowingThrough() {
        Block origin = curArrow.origin;
        if (cgPhase == BOTTOMUP_3) {
            additionalAvlXEffectVisible = avlXEffectsVisibleDown.retrieve(origin);
            additionalAvlXEffectAdded = avlXEffectsAddedDown.retrieve(origin);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("   === Flowing across " + curArrow + ": v" + additionalAvlXEffectVisible + " a" + additionalAvlXEffectAdded);
            }
            return additionalAvlXEffectVisible != null;
        } else { //TOPDOWN_4
            if (runSpecialCycleAnalysis(origin) && curArrow.containsCase(FGConstants.NEXT)) {
                additionalInfo = infosCycleDown.retrieve(origin);
            } else {
                additionalInfo = infosDown.retrieve(origin);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("   === Flowing across " + curArrow + ": " + additionalInfo);
            }
            return additionalInfo != null;
        }
    }

    @Override
    protected boolean getValueFlowingBack() {
        Block destination = curArrow.destination;
        if (cgPhase == BOTTOMUP_1) {
            additionalReqXEffect = reqXEffectsUp.retrieve(destination);
            additionalReqXEffectAdded = reqXEffectsAddedUp.retrieve(destination);
            return additionalReqXEffect != null;
        } else { //TOPDOWN_2
            if (runSpecialCycleAnalysis(destination)
                    && curArrow.finalCycle() == destination.enclosingLoop()) {
                additionalInfo = infosCycleUp.retrieve(destination);
            } else {
                additionalInfo = infosUp.retrieve(destination);
            }
            if (additionalInfo == null) {
                return false;
            } else {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("   === Flowing back across " + curArrow + ": " + additionalInfo);
                }
                return true;
            }
        }
    }

    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        switch (cgPhase) {
            case BOTTOMUP_1:
                if (reqXEffectTmp == null) {
                    reqXEffectTmp = new BoolMatrix(curUnit.hasTooManyZones(), nDZ, nDZexit);
                }
                reqXEffectTmp.cumulOr(additionalReqXEffect, true, commonLength);
                if (reqXEffectAddedTmp == null) {
                    reqXEffectAddedTmp = new BoolVector(nDZ);
                }
                reqXEffectAddedTmp.cumulOr(additionalReqXEffectAdded, commonLength);
                break;
            case BOTTOMUP_3:
                if (avlXEffectVisibleTmp == null) {
                    avlXEffectVisibleTmp = new BoolVector(nDZ);
                }
                avlXEffectVisibleTmp.cumulOr(additionalAvlXEffectVisible, commonLength);
                if (avlXEffectAddedTmp == null) {
                    avlXEffectAddedTmp = new BoolVector(nDZ);
                }
                avlXEffectAddedTmp.cumulOr(additionalAvlXEffectAdded, commonLength);
                break;
            default: //TOPDOWN_2 or TOPDOWN_4
                if (infoTmp == null) {
                    infoTmp = new BoolVector(nDZ);
                }
                infoTmp.cumulOr(additionalInfo, commonLength);
                break;
        }
    }

    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        switch (cgPhase) {
            case BOTTOMUP_1:
            case BOTTOMUP_3:
                // As long as we don't refine on loop-local variables, we must
                // merge cycle values together with non-cycle values:
                cumulValueWithAdditional(commonSymbolTable);
                break;
            default: { //TOPDOWN_2 or TOPDOWN_4
                int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                if (infoTmpCycle == null) {
                    infoTmpCycle = new BoolVector(nDZ);
                }
                infoTmpCycle.cumulOr(additionalInfo, commonLength);
            }
        }
    }

// Primitives for the analysis propagation through one Block:

    @Override
    protected void initializeCumulValue() {
        //This is called for Blocks that go nowhere (e.g. stops or infinite loops)
        if (cgPhase == BOTTOMUP_1) {
            reqXEffectTmp = new BoolMatrix(curUnit.hasTooManyZones(), nDZ, nDZexit);
            reqXEffectTmp.setExplicitZero();
            reqXEffectAddedTmp = new BoolVector(nDZ);
        } else if (cgPhase == TOPDOWN_2) {
            infoTmp = new BoolVector(nDZ);
        } else if (cgPhase == BOTTOMUP_3) {
            avlXEffectVisibleTmp = new BoolVector(nDZ);
            avlXEffectAddedTmp = new BoolVector(nDZ);
        }
    }

    /**
     * Compares ReqX info arriving from downstream the entry of this curBlock
     * with same info stored here at the previous sweep. If different,
     * updates the stored value and returns modified status "true".
     * For loops, comparison is done also on the infoTmpCycle value.
     * For REQUIRED fgPhase (analyzeBackward()), no copy is required here
     * because this is the last use of "infoTmp" and "infoTmpCycle".
     */
    @Override
    protected boolean compareUpstreamValues() {
        switch (cgPhase) {
            case BOTTOMUP_1:
                //comparison may take too long,
                // so we choose to compare only at the downstream end.
                // Here at the upstream end, we don't compare and always return true.
                reqXEffectsUp.store(curBlock, reqXEffectTmp.copy());
                reqXEffectsAddedUp.store(curBlock, reqXEffectAddedTmp.copy());
                return true;
            case BOTTOMUP_3: {
                boolean modified = false;
                BoolVector oldAvlXEffectVisible = avlXEffectsVisibleUp.retrieve(curBlock);
                int compareLength = nDZ;
                if (avlXEffectVisibleTmp != null &&
                        (oldAvlXEffectVisible == null || !avlXEffectVisibleTmp.equals(oldAvlXEffectVisible, compareLength))) {
                    avlXEffectsVisibleUp.store(curBlock, avlXEffectVisibleTmp.copy());
                    modified = true;
                }
                BoolVector oldAvlXEffectAdded = avlXEffectsAddedUp.retrieve(curBlock);
                if (avlXEffectAddedTmp != null &&
                        (oldAvlXEffectAdded == null || !avlXEffectAddedTmp.equals(oldAvlXEffectAdded, compareLength))) {
                    avlXEffectsAddedUp.store(curBlock, avlXEffectAddedTmp.copy());
                    modified = true;
                }
                return modified;
            }
            default: //TOPDOWN_2 or TOPDOWN_4
                return compareWithStorage(infoTmp, infoTmpCycle, nDZ, infosUp, infosCycleUp);
        }
    }

    /**
     * Compares ReqX info arriving from downstream the exit of this curBlock
     * with same info stored here at the previous sweep. If different,
     * updates the stored value and returns modified status "true".
     * For loops, comparison is done also on the infoTmpCycle value.
     * For AVAILABLE fgPhase (analyzeForward()), no copy is required here
     * because this is the last use of "infoTmp" and "infoTmpCycle".
     */
    @Override
    protected boolean compareDownstreamValues() {
        switch (cgPhase) {
            case BOTTOMUP_1: {
                boolean modified = false;
                int compareLength = nDZ;
                BoolMatrix oldReqXEffect = reqXEffectsDown.retrieve(curBlock);
                if (reqXEffectTmp != null &&
                        (oldReqXEffect == null || !reqXEffectTmp.equalsBoolMatrix(oldReqXEffect))) {
                    reqXEffectsDown.store(curBlock, reqXEffectTmp.copy());
                    modified = true;
                }
                BoolVector oldReqXEffectAdded = reqXEffectsAddedDown.retrieve(curBlock);
                if (reqXEffectAddedTmp != null &&
                        (oldReqXEffectAdded == null || !reqXEffectAddedTmp.equals(oldReqXEffectAdded, compareLength))) {
                    reqXEffectsAddedDown.store(curBlock, reqXEffectAddedTmp.copy());
                    modified = true;
                }
                return modified;
            }
            case BOTTOMUP_3: {
                boolean modified = false;
                int compareLength = nDZ;
                BoolVector oldAvlXEffectVisible = avlXEffectsVisibleDown.retrieve(curBlock);
                if (avlXEffectVisibleTmp != null &&
                        (oldAvlXEffectVisible == null || !avlXEffectVisibleTmp.equals(oldAvlXEffectVisible, compareLength))) {
                    avlXEffectsVisibleDown.store(curBlock, avlXEffectVisibleTmp.copy());
                    modified = true;
                }
                BoolVector oldAvlXEffectAdded = avlXEffectsAddedDown.retrieve(curBlock);
                if (avlXEffectAddedTmp != null &&
                        (oldAvlXEffectAdded == null || !avlXEffectAddedTmp.equals(oldAvlXEffectAdded, compareLength))) {
                    avlXEffectsAddedDown.store(curBlock, avlXEffectAddedTmp.copy());
                    modified = true;
                }
                return modified;
            }
            default: //TOPDOWN_2 or TOPDOWN_4
                return compareWithStorage(infoTmp, infoTmpCycle, nDZ, infosDown, infosCycleDown);
        }
    }

    /**
     * Special comparison for ReqX info attached to a message-passing zone.
     * Dummy implementation, may need to be revised...
     */
    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        return false;
    }

    /**
     * Special comparison for ReqX info attached to a message-passing zone.
     * Dummy implementation, may need to be revised...
     */
    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        return false;
    }

    /**
     * Propagates info through "curBlock", upstream (i.e. backwards).
     * This occurs only for the BOTTOMUP_1 and TOPDOWN_2 cgPhases.
     */
    @Override
    protected boolean propagateValuesBackwardThroughBlock() {
        if (cgPhase == BOTTOMUP_1) {
            return propagateReqXEffectsThroughBlock(curBlock);
        } else //TOPDOWN_2
        {
            return propagateAndStoreReqXThroughBlock(curBlock, null);
        }
    }

// Primitives for the propagation of each analysis through one expression:

    /**
     * Propagates info through "curBlock", downstream (i.e. forwards).
     * This occurs only for the BOTTOMUP_3 and TOPDOWN_4 cgPhases.
     */
    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        if (cgPhase == BOTTOMUP_3) {
            return propagateAvlXEffectsThroughBlock(curBlock);
        } else //TOPDOWN_4
        {
            return propagateAndStoreAvlXThroughBlock(curBlock, null);
        }
    }

    /**
     * Propagates the "ReqXEffects" through "block", upstream (i.e. backwards).
     * Always returns true, meaning that the analysis made it across block,
     * because a "stop' instruction probably can't stop a backwards data-flow analysis.
     * This is called during the BOTTOMUP_1 cgPhase.
     */
    private boolean propagateReqXEffectsThroughBlock(Block block) {
        BoolVector beforeActiv = null;
        BoolVector afterActiv = null;
        BoolVector afterUseful = null;
        TapList<BoolVector> blockActivities = null;
        TapList<BoolVector> blockUsefulnesses = null;
        if (unitActivities != null) {
            blockActivities =
                    TapList.reverse(unitActivities.retrieve(block));
            beforeActiv = blockActivities.head;
        }
        if (unitUsefulnesses != null) {
            blockUsefulnesses =
                    TapList.reverse(unitUsefulnesses.retrieve(block));
            afterUseful = blockUsefulnesses.head;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going up through Block " + block + " ===");
            TapEnv.printlnOnTrace("    reqXEffect: " + vecLegend + "*" + vecExitLegend);
            TapEnv.dumpBoolMatrixOnTrace(reqXEffectTmp);
            TapEnv.printlnOnTrace("    reqXEffectAdded:   " + reqXEffectAddedTmp);
            TapEnv.printlnOnTrace();
        }
        TapList<Instruction> instructions = TapList.reverse(block.instructions);
        while (instructions != null) {
            curInstruction = instructions.head;
            if (blockActivities != null) {
                afterActiv = beforeActiv;
                blockActivities = blockActivities.tail;
                beforeActiv = blockActivities.head;
            }
            reqXEffectThroughExpression(curInstruction.tree, READ, null, null,
                    reqXEffectTmp, reqXEffectAddedTmp,
                    beforeActiv, afterActiv, afterUseful);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace(">>> Now gone upstream across instruction:" + ILUtils.toString(curInstruction.tree));
                TapEnv.printlnOnTrace("    reqXEffect: " + vecLegend + "*" + vecExitLegend);
                TapEnv.dumpBoolMatrixOnTrace(reqXEffectTmp);
                TapEnv.printlnOnTrace("    reqXEffectAdded:   " + reqXEffectAddedTmp);
                TapEnv.printlnOnTrace();
            }
            if (blockUsefulnesses != null) {
                blockUsefulnesses = blockUsefulnesses.tail;
                afterUseful = blockUsefulnesses.head;
            }
            instructions = instructions.tail;
        }
        curInstruction = null;
        return true;
    }

    /**
     * Propagates "ReqX" info through "block", upstream (i.e. backwards).
     * Always returns true, indicating that the analysis could sweep the block
     * completely, because a "stop' instruction probably can't stop a
     * backwards data-flow analysis.
     * This is called during the TOPDOWN_2 cgPhase.
     */
    private boolean propagateAndStoreReqXThroughBlock(Block block, TapList<BoolVector> recordList) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going up through Block " + block + " ===");
        }
        BoolVector beforeActiv = null;
        BoolVector afterActiv = null;
        BoolVector afterUseful = null;
        TapList<BoolVector> blockActivities = null;
        TapList<BoolVector> blockUsefulnesses = null;
        if (unitActivities != null) {
            blockActivities =
                    TapList.reverse(unitActivities.retrieve(block));
            beforeActiv = blockActivities.head;
        }
        if (unitUsefulnesses != null) {
            blockUsefulnesses =
                    TapList.reverse(unitUsefulnesses.retrieve(block));
            afterUseful = blockUsefulnesses.head;
        }
        setUniqueAccessZones(block);
        TapList<Instruction> instructions = block.instructions;
        Instruction initialDo = null;
        boolean runSpecialCycleAnalysis = runSpecialCycleAnalysis(block);
        Tree doTree = null;
        if (runSpecialCycleAnalysis) {
            initialDo = instructions.head;
            if (initialDo != null) {
                doTree = initialDo.tree.down(3);
            }
            instructions = instructions.tail;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    ReqX:" + vecLegend + " " + infoTmp.toString(nDZ)
                    + " (cycling:" + (infoTmpCycle == null ? "null" : infoTmpCycle.toString(nDZ)) + ")");
        }
        if (recordList != null) {
            BoolVector recordInfo = infoTmp.copy();
            if (infoTmpCycle != null) {
                recordInfo.cumulOr(infoTmpCycle);
            }
            recordList.tail = new TapList<>(recordInfo, null);
            //[TODO] the following 3 calls to setOnceActiveZonesAndAnnotateDiffDeclsAndTypes are a waste of time:
            // ?maybe first build the union of all infoTmp(Cycle), then call setOnceActive... once, on the union?
            ADActivityAnalyzer.setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(
                                       infoTmp, curSymbolTable, SymbolTableConstants.ALLKIND, nDZ);
        }
        instructions = TapList.reverse(instructions);
        // First make (REQUIRED) "infoTmp" (and possibly also "infoTmpCycle" if we
        // are in the runSpecialCycleAnalysis) sweep upstream across the instructions.
        // If the 1st instruction of the Block is a normal clean do loop,
        // do not consider the "do-from" tree, which is read only at loop entry.
        while (instructions != null) {
            curInstruction = instructions.head;
            if (blockActivities != null) {
                afterActiv = beforeActiv;
                blockActivities = blockActivities.tail;
                beforeActiv = blockActivities.head;
            }
            // Send the messages about diff routines that might create and return diff pointers:
            //  these routines must be differentiated SPLIT !
            if (recordList != null     // meaning: only on the last sweep through this Block.
                    && TapEnv.modeIsAdjoint()) {
                Tree callExpr = curInstruction.tree;
                Tree lhsExpr = null;
                if (callExpr.opCode() == ILLang.op_assign || callExpr.opCode() == ILLang.op_plusAssign
                        || callExpr.opCode() == ILLang.op_minusAssign || callExpr.opCode() == ILLang.op_timesAssign
                        || callExpr.opCode() == ILLang.op_divAssign) {
                    lhsExpr = callExpr.down(1);
                    callExpr = callExpr.down(2);
                }
                if (callExpr.opCode() == ILLang.op_call) {
                    // Check if some pointer is defined in this call C and reqX downstream this call C:
                    // In that case in adjoint mode, this call C must not be checkpointed.
                    // (Or else instead of calling C, we should call some C_ADVANCE() that sets the diff pointers)
                    curCalledUnit = getCalledUnit(callExpr, curSymbolTable);
                    if (curCalledUnit != null && !"null".equals(curCalledUnit.name())) {
                        CallArrow arrow = CallGraph.getCallArrow(curUnit, curCalledUnit);
                        BoolVector callSiteW = new BoolVector(nDZ);
                        translateCalleeDataToCallSite(curCalledUnit.unitInOutPossiblyW(), callSiteW,
                                    lhsExpr, ILUtils.getArguments(callExpr).children(), true,
                                    callExpr, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                    null, null, false, false) ;
                        if (!curActivity.isContext()
                            && infoTmp.intersects(callSiteW, nDZ)
                            && curInstruction.thisCallIsCheckpointed(curCalledUnit)) {
                            // When the called subroutine FOO sets a ReqX pointer, then its adjoint must also set the
                            // diff pointer during the FWD sweep (because we choose to prepare the diff memory
                            // during the FWD sweep). Therefore the FWD sweep must call FOO_FWD() and not FOO()
                            // This may change when checkpointing FOO calls FOO_ADV() instead of FOO().
                            // Don't send this warning for "context" code.
                            String diffPointersSet = null;
                            ZoneInfo zoneInfo;
                            boolean mustSendMessage = false;
                            callSiteW.cumulAnd(infoTmp);
                            for (int i=nDZ-1; i>=0 ; --i) {
                                if (callSiteW.get(i)) {
                                    zoneInfo = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                                    if (zoneInfo.ptrZoneNb != -1) {
                                        mustSendMessage = true;
                                        diffPointersSet =
                                                zoneInfo.accessTreePrint(curUnit.language())
                                                        + (diffPointersSet == null ? "" : ", " + diffPointersSet);
                                    }
                                }
                            }
                            if (mustSendMessage) {
                                TapEnv.fileWarning(TapEnv.MSG_SEVERE, callExpr, "(AD21) This call to " + curCalledUnit.name() + " might set pointers that are differentiated (" + diffPointersSet + "). If this is really the case, then this call must not be checkpointed.");
                            }
                        }
                    }
                }
            }

            reqXThroughExpression(curInstruction.tree, NOACT, null,
                    infoTmp, beforeActiv, afterActiv, afterUseful, recordList != null);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                reqXThroughExpression(curInstruction.tree, NOACT, null,
                        infoTmpCycle, beforeActiv, afterActiv, afterUseful, recordList != null);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  Now gone upstream across instruction:" + ILUtils.toString(curInstruction.tree));
                TapEnv.printlnOnTrace("    ReqX:" + vecLegend + " " + infoTmp.toString(nDZ)
                        + " (cycling:" + (infoTmpCycle == null ? "null" : infoTmpCycle.toString(nDZ)) + ")");
            }
            if (recordList != null) {
                BoolVector recordInfo = infoTmp.copy();
                if (infoTmpCycle != null) {
                    recordInfo.cumulOr(infoTmpCycle);
                }
                recordList.tail = new TapList<>(recordInfo, recordList.tail);
                ADActivityAnalyzer.setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(
                                           infoTmp, curSymbolTable, SymbolTableConstants.ALLKIND, nDZ);
            }
            if (blockUsefulnesses != null) {
                blockUsefulnesses = blockUsefulnesses.tail;
                afterUseful = blockUsefulnesses.head;
            }
            instructions = instructions.tail;
        }
        if (initialDo != null) {
            curInstruction = initialDo;
            if (blockActivities != null) {
                afterActiv = beforeActiv;
                blockActivities = blockActivities.tail;
                beforeActiv = blockActivities.head;
            }
            reqXThroughExpression(doTree.down(1), WRITE, null,
                    infoTmp, beforeActiv, afterActiv, afterUseful, recordList != null);
            reqXThroughExpression(doTree.down(4), READ, null,
                    infoTmp, beforeActiv, afterActiv, afterUseful, recordList != null);
            reqXThroughExpression(doTree.down(3), READ, null,
                    infoTmp, beforeActiv, afterActiv, afterUseful, recordList != null);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                reqXThroughExpression(doTree.down(1), WRITE, null,
                        infoTmpCycle, beforeActiv, afterActiv, afterUseful, recordList != null);
                reqXThroughExpression(doTree.down(4), READ, null,
                        infoTmpCycle, beforeActiv, afterActiv, afterUseful, recordList != null);
                reqXThroughExpression(doTree.down(3), READ, null,
                        infoTmpCycle, beforeActiv, afterActiv, afterUseful, recordList != null);
            }
        }
        // Second compute the (REQUIRED) "infoTmpCycle" that will be propagated upstream
        // through the loop's cycling arrows. It is the downcoming (REQUIRED) "infoTmp" PLUS
        // only the downcoming (REQUIRED) "infoTmpCycle" which are not localized in the loop.
        BoolVector infoTmpCycleUp = null;
        if (runSpecialCycleAnalysis) {
            if (infoTmpCycle != null) {
                infoTmpCycleUp = infoTmpCycle.copy();
                infoTmpCycleUp.set(((HeaderBlock) block).localizedZones, false);
            } else {
                infoTmpCycleUp = new BoolVector(nDZ);
            }
            if (infoTmp != null) {
                infoTmpCycleUp.cumulOr(infoTmp);
            }
// Replaced by code below because direct use of loopRunsAtLeastOnce is wrong (see directEntryExitMask()). Needs nonregression testing.
//         // Third compute the (REQUIRED) "infoTmp" that will be propagated upstream
//         // through the loop's entering arrows. It is the downcoming (REQUIRED) "infoTmp"
//         // (except if the loop runs certainly at least once),
//         // PLUS the downcoming (REQUIRED) "infoTmpCycle".
//             if (infoTmpCycle!=null) {
//                 if (infoTmp==null || loopRunsAtLeastOnce((HeaderBlock)block))
//                     infoTmp = infoTmpCycle.copy() ;
//                 else
//                     infoTmp.cumulOr(infoTmpCycle) ;
//             }
            // Third compute the (REQUIRED) "infoTmp" that will be propagated upstream
            // through the loop's entering arrows. It is the downcoming (REQUIRED) "infoTmpCycle" PLUS
            // the part of the downcoming (REQUIRED) "infoTmp" defined by "directEntryExitMask()".
            if (infoTmpCycle != null) {
                if (infoTmp == null) {
                    infoTmp = infoTmpCycle.copy();
                } else {
                    infoTmp.cumulAnd(directEntryExitMask((HeaderBlock) block, nDZ, SymbolTableConstants.ALLKIND));
                }
                infoTmp.cumulOr(infoTmpCycle);
            }
        }
        infoTmpCycle = infoTmpCycleUp;
        // Fourth add the effect of reading the do-from into the (REQUIRED) "infoTmp"
        if (initialDo != null) {
            reqXThroughExpression(doTree.down(2), READ, null,
                    infoTmp, beforeActiv, afterActiv, afterUseful, recordList != null);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  Now gone upstream across initial DO:" + ILUtils.toString(doTree));
                TapEnv.printlnOnTrace("    ReqX:" + vecLegend + " " + infoTmp.toString(nDZ)
                        + " (cycling:" + (infoTmpCycle == null ? "null" : infoTmpCycle.toString(nDZ)) + ")");
            }
            if (recordList != null) {
                BoolVector recordInfo = infoTmp.copy();
                if (infoTmpCycle != null) {
                    recordInfo.cumulOr(infoTmpCycle);
                }
                recordList.tail = new TapList<>(recordInfo, recordList.tail);
                ADActivityAnalyzer.setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(
                                       infoTmp, curSymbolTable, SymbolTableConstants.ALLKIND, nDZ);
            }
        }
        curInstruction = null;
        uniqueAccessZones = null;
        return true;
    }

    /**
     * Propagates the "AvlXEffects" through "block", downstream (i.e. forwards).
     * This is called during the BOTTOMUP_3 cgPhase.
     * Always returns true because we don't care about STOP's.
     */
    private boolean propagateAvlXEffectsThroughBlock(Block block) {
        //The AvlX analysis doesn't really need a special treatment for cycles.
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going down through Block " + block + " ===");
            TapEnv.printlnOnTrace("        avlXEffectVisible: " + avlXEffectVisibleTmp);
            TapEnv.printlnOnTrace("        avlXEffectAdded:   " + avlXEffectAddedTmp);
        }
        TapList<BoolVector> blockReqXs = unitReqXInfos.retrieve(block);
        if (blockReqXs != null) {
            blockReqXs = blockReqXs.tail;
        }
        TapList<Instruction> instructions = block.instructions;
        while (instructions != null) {
            curInstruction = instructions.head;
            avlXEffectThroughExpression(curInstruction.tree, null,
                    avlXEffectVisibleTmp, avlXEffectAddedTmp,
                    blockReqXs == null ? null : blockReqXs.head);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("        Now gone downstream across instruction:" + ILUtils.toString(curInstruction.tree));
                TapEnv.printlnOnTrace("        avlXEffectVisible: " + avlXEffectVisibleTmp);
                TapEnv.printlnOnTrace("        avlXEffectAdded:   " + avlXEffectAddedTmp);
            }
            if (blockReqXs != null) {
                blockReqXs = blockReqXs.tail;
            }
            instructions = instructions.tail;
        }
        curInstruction = null;
        return true;
    }

    /**
     * Propagates "AvlX" info through "block", downstream (i.e. forwards).
     * This is called during the TOPDOWN_4 cgPhase.
     * Always returns true because we don't care about STOP's.
     */
    private boolean propagateAndStoreAvlXThroughBlock(Block block, TapList<BoolVector> recordList) {
        //The AvlX analysis is simple: it doesn't need a special treatment for cycles:
        if (infoTmp != null) {
            if (infoTmpCycle != null) {
                infoTmp.cumulOr(infoTmpCycle);
            }
        } else {
            infoTmp = infoTmpCycle;
        }
        TapList<BoolVector> blockReqXs = unitReqXInfos.retrieve(block);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going down through Block " + block + " ===");
            TapEnv.printlnOnTrace("        AvlX:" + infoTmp);
        }
        if (blockReqXs != null) {
            infoTmp.cumulOr(blockReqXs.head);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    --> AvlX:" + infoTmp + " after adding ReqX:" + blockReqXs.head);
            }
            blockReqXs = blockReqXs.tail;
        }
        if (recordList != null) {
            BoolVector recordInfo = infoTmp.copy();
            recordList = recordList.placdl(recordInfo);
        }
        TapList<Instruction> instructions = block.instructions;
        while (instructions != null) {
            curInstruction = instructions.head;
            avlXThroughExpression(curInstruction.tree, null, infoTmp,
                    blockReqXs == null ? null : blockReqXs.head);
            // Probably redundant but true: if P' is "Required" here, it must also be "Present":
            if (blockReqXs != null) {
                infoTmp.cumulOr(blockReqXs.head);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("        Now gone across instruction:" + ILUtils.toString(curInstruction.tree));
                TapEnv.printlnOnTrace("        AvlX:" + vecLegend + " " + infoTmp.toString(nDZ));
            }
            if (blockReqXs != null) {
                blockReqXs = blockReqXs.tail;
            }
            if (recordList != null) {
                recordList = recordList.placdl(infoTmp.copy());
            }
            instructions = instructions.tail;
        }
        //The AVAILABLE analysis is so simple that
        // it doesn't need a special treatment for cycles:
        if (runSpecialCycleAnalysis(block)) {
            infoTmpCycle = infoTmp.copy();
        }
        curInstruction = null;
        return true;
    }

    /**
     * Propagate upstream through the given "expression"
     * the "reqXEffect" info consisting in reqXEffect and reqXEffectAdded.
     */
    private void reqXEffectThroughExpression(
            Tree expression, int act, TapList treeRE, TapList treeREA,
            BoolMatrix reqXEffect, BoolVector reqXEffectAdded,
            BoolVector beforeActiv, BoolVector afterActiv, BoolVector afterUseful) {
        switch (expression.opCode()) {
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_return:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                boolean deallocOrNullify = (expression.opCode() == ILLang.op_deallocate
                        || expression.opCode() == ILLang.op_nullify);
                boolean isReturn = (expression.opCode() == ILLang.op_return);
                Tree lhs;
                if (isReturn) {
                    String returnVarName =
                            (curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol);
                    lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                } else {
                    lhs = expression.down(1);
                }
                Tree rhs;
                if (isReturn) {
                    rhs = expression.down(1);
                } else if (deallocOrNullify) {
                    rhs = null;
                } else {
                    rhs = expression.down(2);
                }
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                if (isReturn && writtenZonesTree != null) { // Not sure why do this only for return...
                    writtenZonesTree = TapList.copyTree(writtenZonesTree);
                    includePointedElementsInTree(writtenZonesTree, null, null, true, false, false);
                    TapList.removeNonWritableZones(writtenZonesTree) ;
                }
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  Start propagating ReqX Effect upstream through: " + ILUtils.toString(expression) + " written zones: " + writtenZonesTree + (totalAccess ? "(total)" : "(non-total)"));
                }
                if (!deallocOrNullify) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  downcoming ReqX effect trees          treeRE: " + treeRE + " and treeREA: " + treeREA);
                    }
                    treeRE = TapList.cumulWithOper(treeRE,
                            buildInfoPRZVTree(writtenZonesTree, reqXEffect, SymbolTableConstants.ALLKIND),
                            SymbolTableConstants.CUMUL_OR);
                    treeREA = TapList.cumulWithOper(treeREA,
                            buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                    reqXEffectAdded, null, SymbolTableConstants.ALLKIND, curSymbolTable),
                            SymbolTableConstants.CUMUL_OR);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  modified ReqX effect trees with lhs treeRE: " + treeRE + " and treeREA: " + treeREA);
                    }
                }
                reqXEffectThroughExpression(lhs, WRITE, treeRE, treeREA,
                        reqXEffect, reqXEffectAdded, beforeActiv, afterActiv, afterUseful);
                if (totalAccess) {
                    setInfoPRZVTree(reqXEffect, writtenZonesTree, new TapList<>(new BoolVector(nDZexit), null),
                                    null, true, SymbolTableConstants.ALLKIND);
                    setInfoBoolTreeToExtendedDeclaredZones(reqXEffectAdded, writtenZonesTree,
                            false, null, true, SymbolTableConstants.ALLKIND);
                } else {
                    // Even if write is non-total, make sure the reqXEffect line does not remain identity:
                    setInfoPRZVTree(reqXEffect, writtenZonesTree, new TapList<>(new BoolVector(nDZexit), null),
                                    null, false, SymbolTableConstants.ALLKIND);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  propagating to rhs: treeRE: " + treeRE + " and treeREA: " + treeREA);
                }
                if (!deallocOrNullify && !ILUtils.isNullOrNone(rhs)) {
                    reqXEffectThroughExpression(rhs, READ, treeRE, treeREA,
                            reqXEffect, reqXEffectAdded, beforeActiv, afterActiv, afterUseful);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  >Done propagating reqX Effect upstream through: " + ILUtils.toString(expression));
                }
                break;
            }
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_ident:
                if (act != NOACT) {
                    boolean diffExprExistsA = diffExprExists(expression, treeREA, act, beforeActiv, afterActiv, afterUseful);
                    BoolVector conditionDiffExpr = new BoolVector(reqXEffect.nCols);
                    cumulOr(treeRE, conditionDiffExpr, true);
                    boolean diffExprExists = !conditionDiffExpr.isFalse(reqXEffect.nCols);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     " + (act == READ ? "READ" : act == WRITE ? "WRITTEN" : "UNUSED") + " var reference " + ILUtils.toString(expression) + " differentiated:" + diffExprExistsA + " or " + conditionDiffExpr);
                    }
                    if (diffExprExists || diffExprExistsA) {
                        TapIntList usedPointerRanks = collectUsedPointerRanks(expression, null, false);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("          usedPointerRanks of " + expression + " : " + usedPointerRanks);
                        }
                        if (diffExprExistsA) {
                            reqXEffectAdded.set(usedPointerRanks, true);
                        }
                        if (diffExprExists) {
                            reqXEffect.cumulRows(usedPointerRanks, conditionDiffExpr);
                        }
                    }
                }
                if (act == READ) {
                    if (treeRE != null || treeREA != null) {
                        TapList accessedZonesTree =
                                curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null);
                        if (accessedZonesTree != null) {
                            accessedZonesTree = TapList.copyTree(accessedZonesTree);
                            includePointedElementsInTree(accessedZonesTree, null, null, true, false, true);
                        }
                        if (treeRE != null) {
                            setInfoPRZVTree(reqXEffect, accessedZonesTree, treeRE,
                                            null, false, SymbolTableConstants.ALLKIND);
                        }
                        if (treeREA != null) {
                            setInfoBoolTreeToExtendedDeclaredZones(reqXEffectAdded,
                                    accessedZonesTree, treeREA, null, false, SymbolTableConstants.ALLKIND);
                        }
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("  RHS of assignment (zones:" + accessedZonesTree + ')');
                            TapEnv.printlnOnTrace("   received treeRE: " + treeRE + " and treeREA: " + treeREA);
                        }
                    }
                }
                if (expression.opCode() != ILLang.op_ident) {
                    reqXEffectThroughExpression(expression.down(1), NOACT, null, null,
                            reqXEffect, reqXEffectAdded,
                            beforeActiv, afterActiv, afterUseful);
                    // For the very rare case where the index or offset expressions contain
                    // differentiable assignments:
                    if (expression.opCode() != ILLang.op_fieldAccess) {
                        reqXEffectThroughExpression(expression.down(2), NOACT, null, null,
                                reqXEffect, reqXEffectAdded,
                                beforeActiv, afterActiv, afterUseful);
                    }
                }
                break;
            case ILLang.op_address:
                // Checks: set03/cm09, set04/lh138, set04/ptr03, set10/lh207, set11/lh006...
                if (act != NOACT) {
                    Tree inside = expression.down(1);
                    int insideOp = inside.opCode();
                    if (insideOp == ILLang.op_pointerAccess) {
                        // This is really for the C pointer case (not Fortran's array access):
                        reqXEffectThroughExpression(inside.down(1), act, treeRE, treeREA,
                                reqXEffect, reqXEffectAdded,
                                beforeActiv, afterActiv, afterUseful);
                        // For the very rare case where the index expression contains assignments:
                        reqXEffectThroughExpression(inside.down(2), NOACT, null, null,
                                reqXEffect, reqXEffectAdded,
                                beforeActiv, afterActiv, afterUseful);
                    } else {
                        if (treeRE != null || treeREA != null) {
                            TapList tmpAccessedZonesTree;
                            if (insideOp == ILLang.op_fieldAccess) {
                                TapList accessedZonesTreeOfStruct =
                                        curSymbolTable.treeOfZonesOfValue(inside.down(1), null, curInstruction, null);
                                if (accessedZonesTreeOfStruct != null) {
                                    accessedZonesTreeOfStruct = TapList.copyTree(accessedZonesTreeOfStruct);
                                    includePointedElementsInTree(accessedZonesTreeOfStruct, null, null, true, false, true);
                                }
                                int fieldRank = ILUtils.getFieldRank(inside.down(2));
                                //Build a temporary tree of zones for expression &(var.field) :
                                tmpAccessedZonesTree =
                                        new TapList<>(null,
                                                new TapList<>(TapList.nth(accessedZonesTreeOfStruct, fieldRank),
                                                        null));
                                if (TapEnv.traceCurAnalysis()) {
                                    TapEnv.printlnOnTrace("     (address of field) ref-expression:" + expression
                                            + " with tmpZones:" + tmpAccessedZonesTree
                                            + "   receives treeRE: " + treeRE + " and treeREA: " + treeREA);
                                }
                            } else {
                                tmpAccessedZonesTree = curSymbolTable.treeOfZonesOfValue(inside, null, curInstruction, null);
                                if (TapEnv.traceCurAnalysis()) {
                                    TapEnv.printlnOnTrace("     (address of ref) ref-expression:" + expression
                                            + " with zones:" + tmpAccessedZonesTree
                                            + "   receives treeRE: " + treeRE + " and treeREA: " + treeREA);
                                }
                            }
                            if (treeRE != null) {
                                setInfoPRZVTree(reqXEffect, tmpAccessedZonesTree, treeRE,
                                                null, false, SymbolTableConstants.ALLKIND);
                            }
                            if (treeREA != null) {
                                setInfoBoolTreeToExtendedDeclaredZones(reqXEffectAdded,
                                        tmpAccessedZonesTree, treeREA, null, false, SymbolTableConstants.ALLKIND);
                            }
                        }
                    }
                }
                break;
            case ILLang.op_call:
                curCalledUnit = getCalledUnit(expression, curSymbolTable);
                ActivityPattern curCalledActivity =
                        (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(expression, curActivity, "multiActivityCalleePatterns");
                // It may happen that curCalledActivity==null, when Activity analysis didn't find
                // curCalledUnit active (or when activity analysis was turned off), but now ReqX
                // analysis might find it "active" in the sense that it does something to memory
                // and pointers. In that case, there should now exist an ActivityPattern for curCalledUnit
                // and the present call must be annotated now with it as its called activity.
                if (curCalledActivity == null) {
                    if (curCalledUnit.isExternal() || curCalledUnit.isIntrinsic()) {
                        // Build a dummy empty ActivityPattern. This is a bottom-up analysis,
                        // so we have no context info to add into the called ActivityPattern.
                        // Do not reuse a possibly existing ActivityPattern of the called external/intrinsic,
                        // because activity here is passive and should not be polluted by an
                        // existing active pattern from somewhere else.
                        curCalledActivity = createTemporaryDummyEmptyActivityPattern(curCalledUnit);
                    } else {
                        curCalledActivity = getOrCreateCalledActivityPattern(curActivity, curCalledUnit, expression);
                    }
                }
                // curCalledActivity now certainly non-null.
                if (curCalledUnit != null && curCalledUnit.hasParamElemsInfo()) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  Start propagating ReqX Effect upstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    called Activity Pattern:" + curCalledActivity);
                        TapEnv.printlnOnTrace("    reqXEffect arriving from downstream: " + vecLegend + "*" + vecExitLegend);
                        TapEnv.dumpBoolMatrixOnTrace(reqXEffect);
                        TapEnv.printlnOnTrace("    and reqXEffectAdded: " + vecLegend + " " + reqXEffectAdded);
                    }
                    CallArrow arrow = CallGraph.getCallArrow(curUnit, curCalledUnit);
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    int nbActualParams = actualParams.length;
                    BoolMatrix calleeRE = getReqXEffect(curCalledActivity);
                    BoolVector calleeREA = getReqXEffectAdded(curCalledActivity);
                    BoolVector calleeTouched = curCalledUnit.unitInOutPossiblyRorW() ;
                    if (calleeTouched!=null) removeNotUsedAtAll(calleeTouched, calleeRE, calleeREA);
                    BoolVector touched = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(calleeTouched, touched,
                                                  null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, false) ;

                    TapList[] paramDataS = new TapList[1+nbActualParams] ;
                    for (int i=nbActualParams ; i>=1 ; --i) {paramDataS[i] = null ;}
                    paramDataS[0] = treeRE ;
                    BoolMatrix onCalleeRE =
                        translateCallSiteDataToCallee(reqXEffect, paramDataS, null, actualParams, false,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND) ;
                    paramDataS[0] = treeREA ;
                    BoolVector onCalleeREA =
                        translateCallSiteDataToCallee(reqXEffectAdded, paramDataS, null, actualParams, false,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND/*, false*/) ;
                    // Only keep in onCalleeREA those zones that are touched by curCalledUnit (i.e. read or written):
                    onCalleeREA.cumulAnd(calleeTouched) ;
                    String vecCalledLegend = "[x" + curCalledUnit.rank() + ":a]";
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    downcoming ReqX Effect info on callee: " + vecCalledLegend + "*" + vecCalledLegend);
                        TapEnv.dumpOnTrace(onCalleeRE);
                        TapEnv.printlnOnTrace("     and REA on callee: " + vecCalledLegend + " " + onCalleeREA);
                        TapEnv.printlnOnTrace();
                    }
                    // Store in advance the called unit's PARTIAL exit ReqX, to make the coming formalArgsActivity() work:
                    if (curCalledActivity.exitReqX() != null) {
                        //TODO: The protection above should go away: even on externals, we must cumulOr into something !!
                        curCalledActivity.exitReqX().cumulOr(onCalleeREA);
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  (note touched:" + touched);
                        TapEnv.printlnOnTrace("        and RoW:" + curCalledUnit.unitInOutPossiblyRorW() + ")");
                        TapEnv.printlnOnTrace("    pushed through calleeRE: " + vecCalledLegend + "*" + vecCalledLegend);
                        TapEnv.dumpOnTrace(calleeRE);
                        TapEnv.printlnOnTrace("     and calleeREA: " + vecCalledLegend + " " + calleeREA);
                        TapEnv.printlnOnTrace();
                    }

                    onCalleeRE = calleeRE.times(onCalleeRE);
                    onCalleeREA = calleeRE.times(onCalleeREA).or(calleeREA);

                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    gives upgoing ReqX Effect info on callee: " + vecCalledLegend + "*" + vecCalledLegend);
                        TapEnv.dumpOnTrace(onCalleeRE);
                        TapEnv.printlnOnTrace("     and REA on callee: " + vecCalledLegend + " " + onCalleeREA);
                        TapEnv.printlnOnTrace();
                    }
                    // Store in advance the called unit's PARTIAL entry ReqX, to make the coming formalArgsActivity() work:
                    if (curCalledActivity.entryReqX() != null) {
                        //TODO: The protection above should go away: even on externals, we must cumulOr into something !!
                        curCalledActivity.entryReqX().cumulOr(onCalleeREA);
                    }

                    BoolVector callSiteKilled = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(curCalledUnit.unitInOutCertainlyW(), callSiteKilled, null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, true) ;
                    TapList[] argsRE =
                        translateCalleeDataToCallSite(onCalleeRE, reqXEffect, null, null, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND,
                                                      touched, callSiteKilled.not(), true, calleeTouched) ;
                    TapList[] argsREA =
                        translateCalleeDataToCallSite(onCalleeREA, reqXEffectAdded, null, null, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND,
                                                      // Note: next "null" could/should indeed be replaced with "touched"
                                                      //  but this causes a bug in set04/lh189
                                                      // (?maybe because missing zone for "address of cx"?)
                                                      null, callSiteKilled.not(), true, false) ;

                    if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression, curSymbolTable) || TapEnv.mustContext()) {
                        propagateDiffParamsRequired(expression, null, arrow, actualParams, curCalledActivity, curInstruction, curActivity);
                    }

                    boolean[] formalArgsActivity =
                            ADActivityAnalyzer.formalArgsActivity(curCalledActivity);
                    if (TapEnv.traceCurAnalysis() && nbActualParams > 0) {
                        TapEnv.printlnOnTrace("     now pushing up through actual args:");
                    }
                    for (int i = nbActualParams - 1; i >= 0; --i) {
                        boolean diffExprExistsA =
                                (TapList.oneTrue(argsREA[1+i])
                                        || (formalArgsActivity != null && i < formalArgsActivity.length && formalArgsActivity[i])) ;
                        BoolVector conditionDiffExpr = new BoolVector(reqXEffect.nCols);
                        cumulOr(argsRE[1+i], conditionDiffExpr, true);
                        boolean diffExprExists = !conditionDiffExpr.isFalse(reqXEffect.nCols);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("       #" + (i+1) + ": " + argsRE[1+i] + ", unconditional:" + argsREA[1+i]);
                        }
                        if (diffExprExists || diffExprExistsA) {
                            // mixed call pointer C - variable Fortran:
                            // si le parametre formel fortran est actif, le pointer passe' en argument est reqx
                            // cf mix49
                            if (formalArgsActivity != null && i < formalArgsActivity.length && formalArgsActivity[i]
                                    && curUnit.language() != curCalledUnit.language()
                                    && !arrow.takesArgumentByValue(i + 1)) {
                                if (argsREA[1+i].tail != null) {
                                    argsREA[1+i].head = Boolean.TRUE;
                                }
                            }
                        }
                        reqXEffectThroughExpression(actualParams[i], READ, argsRE[1+i], argsREA[1+i],
                                reqXEffect, reqXEffectAdded,
                                beforeActiv, afterActiv, afterUseful);
                        // Moreover, if this call is active and this formal arg is active,
                        // and if the actual arg is a pointerAccess, the accessed pointers
                        // must become reqX, even if the actual arg is passive, because the
                        // diff call will need some diff argument passed there.
                        if (diffExprExists || diffExprExistsA) {
                            TapIntList usedPointerRanks = collectUsedPointerRanks(actualParams[i], null, false);
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace("       also arg is pointer-active, usedPointerRanks=" + usedPointerRanks);
                            }
                            if (diffExprExistsA) {
                                reqXEffectAdded.set(usedPointerRanks, true);
                            }
                            if (diffExprExists) {
                                reqXEffect.cumulRows(usedPointerRanks, conditionDiffExpr);
                            }
                        }
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace();
                        TapEnv.printlnOnTrace("  >Done propagating ReqX Effect upstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    reqXEffect propagated upstream: " + vecLegend + "*" + vecExitLegend);
                        TapEnv.dumpBoolMatrixOnTrace(reqXEffect);
                        TapEnv.printlnOnTrace("     and reqXEffectAdded: " + vecLegend + " " + reqXEffectAdded);
                    }
                }
                break;
            case ILLang.op_add:
            case ILLang.op_sub: {
                // Except in the case of pointer arithmetic, the "reqXTree" reqX requirement
                // on the operation's result does not propagate to the args:
                boolean argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(1)), SymbolTableConstants.POINTERTYPE);
                reqXEffectThroughExpression(expression.down(1), READ,
                        argIsPointer ? treeRE : null, argIsPointer ? treeREA : null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(2)), SymbolTableConstants.POINTERTYPE);
                reqXEffectThroughExpression(expression.down(2), READ,
                        argIsPointer ? treeRE : null, argIsPointer ? treeREA : null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            }
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_complexConstructor:
            case ILLang.op_iterativeVariableRef:
                // There is no binary combination on pointers
                // therefore the "treeRE" and "treeREA" requirements on the
                // operation's result do not propagate on the args:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_sizeof:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_switch:
            case ILLang.op_forall:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_arrayDeclarator:
                reqXEffectThroughExpression(expression.down(1), WRITE, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_unary: {
                // Except in the case of pointer arithmetic, the "reqXTree" reqX requirement
                // on the operation's result does not propagate to the argument:
                boolean argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(2)), SymbolTableConstants.POINTERTYPE);
                reqXEffectThroughExpression(expression.down(2), act,
                        (argIsPointer ? treeRE : null), (argIsPointer ? treeREA : null),
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
            case ILLang.op_constructorCall:
                reqXEffectThroughExpression(expression.down(2), act, treeRE, treeREA,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_loop:
                reqXEffectThroughExpression(expression.down(3), act, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                reqXEffectThroughExpression(expression.down(1), WRITE, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(3), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(4), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_forallRangeSet:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_for:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(3), WRITE, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_ifExpression:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(3), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_arrayTriplet:
                reqXEffectThroughExpression(expression.down(1), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(2), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                reqXEffectThroughExpression(expression.down(3), READ, null, null,
                        reqXEffect, reqXEffectAdded,
                        beforeActiv, afterActiv, afterUseful);
                break;
            case ILLang.op_arrayConstructor:
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    reqXEffectThroughExpression(sons[i], act, null, null,
                            reqXEffect, reqXEffectAdded,
                            beforeActiv, afterActiv, afterUseful);
                }
                break;
            }
            case ILLang.op_varDeclaration: {
                Tree[] decls = expression.down(3).children();
                inADeclaration = true;
                for (int i = decls.length - 1; i >= 0; i--) {
                    if (decls[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(decls[i]);
                        reqXEffectThroughExpression(decls[i],
                                READ, null, null, reqXEffect, reqXEffectAdded,
                                beforeActiv, afterActiv, afterUseful);
                        ILUtils.resetAssignFromInitDecl(decls[i]);
                    }
                }
                inADeclaration = false;
                break;
            }
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_allocate:
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_strings:
            case ILLang.op_formatElem:
            case ILLang.op_ioCall:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            default:
                TapEnv.toolWarning(-1, "(ReqX Effect analysis) Unexpected operator: " + expression.opName());
                break;
        }
    }

    /**
     * ReqX propagation upstream Tree "expression". @return nothing.
     *
     * @param lastSweep when this argument is true, an annotation will be set on ReqX expressions.
     */
    private void reqXThroughExpression(Tree expression, int act,
                                       TapList reqXTree, BoolVector reqX,
                                       BoolVector beforeActiv, BoolVector afterActiv,
                                       BoolVector afterUseful, boolean lastSweep) {

        boolean exprIsPointer = false;
        if (lastSweep) {
            exprIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression), SymbolTableConstants.POINTERTYPE);
        }
        if (lastSweep && exprIsPointer && TapList.oneTrue(reqXTree)) {
            setAnnotatedPointerActive(curActivity, expression);
        }

        switch (expression.opCode()) {
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_return:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  Start propagating ReqX upstream through: " + ILUtils.toString(expression));
                    TapEnv.printlnOnTrace("    downcoming ReqX:" + vecLegend + " " + reqX.toString(nDZ));
                }
                boolean deallocOrNullify = (expression.opCode() == ILLang.op_deallocate
                        || expression.opCode() == ILLang.op_nullify);
                boolean isReturn = (expression.opCode() == ILLang.op_return) ;
                Tree lhs;
                if (isReturn) {
                    String returnVarName =
                            curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                } else {
                    lhs = expression.down(1);
                }
                Tree rhs;
                if (isReturn) {
                    // child 1 is the returned expression (C style).
                    rhs = expression.down(1);
                } else if (deallocOrNullify) {
                    rhs = null;
                } else {
                    rhs = expression.down(2);
                }
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                if (isReturn && writtenZonesTree != null) { // Not sure why do this only for return...
                    writtenZonesTree = TapList.copyTree(writtenZonesTree);
                    includePointedElementsInTree(writtenZonesTree, null, null, true, false, false);
                }
                TapList.removeNonWritableZones(writtenZonesTree) ;
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (!deallocOrNullify) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    and downcoming ReqX tree     : " + reqXTree);
                    }
                    reqXTree = TapList.cumulWithOper(reqXTree,
                            buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                    reqX, null, SymbolTableConstants.ALLKIND, curSymbolTable),
                            SymbolTableConstants.CUMUL_OR);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    modified ReqX tree with lhs: " + writtenZonesTree + " => " + reqXTree);
                    }
                }
                if (!isReturn) {
                    reqXThroughExpression(lhs, WRITE, reqXTree, reqX, beforeActiv, afterActiv, afterUseful, lastSweep);
                    if (totalAccess) {
                        setInfoBoolTreeToExtendedDeclaredZones(reqX,
                                writtenZonesTree, false, null, true, SymbolTableConstants.ALLKIND);
                    }
                }
                if (lastSweep && exprIsPointer && TapList.oneTrue(reqXTree)) {
                    if (!isReturn) {
                        setAnnotatedPointerActive(curActivity, lhs);
                    } else {
                        setAnnotatedPointerActive(curActivity, rhs);
                    }
                    setAnnotatedPointerActive(curActivity, expression);
                }
                if (!deallocOrNullify) {
                    reqXThroughExpression(rhs, READ, reqXTree, reqX,
                            beforeActiv, afterActiv, afterUseful, lastSweep);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  >Done propagating ReqX upstream through: " + ILUtils.toString(expression));
                }
                break;
            }
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_ident:
                if (act != NOACT) {
                    boolean diffExprExists = diffExprExists(expression, reqXTree, act, beforeActiv, afterActiv, afterUseful);
                    if (diffExprExists) {
                        TapIntList usedPointerRanks = collectUsedPointerRanks(expression, null, true);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("          usedPointerRanks of " + expression + " : " + usedPointerRanks);
                        }
                        reqX.set(usedPointerRanks, true);
                    }
                }
                if (act == READ) {
                    if (reqXTree != null) {
                        TapList accessedZonesTree =
                                curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null);
                        if (accessedZonesTree != null) {
                            accessedZonesTree = TapList.copyTree(accessedZonesTree);
                            includePointedElementsInTree(accessedZonesTree, null, null, true, false, true);
                        }
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("     ref-expression:" + expression + " with zones:" + accessedZonesTree + " accessed " + (act == READ ? "Read" : "Write") + " to provide for " + reqXTree);
                        }
                        setInfoBoolTreeToExtendedDeclaredZones(reqX,
                                accessedZonesTree, reqXTree, null, false, SymbolTableConstants.ALLKIND);
                        if (lastSweep && TapList.oneTrue(reqXTree)) {
                            setRefAnnotatedPointerActive(curActivity, expression);
                        }
                    }
                }
                if (expression.opCode() != ILLang.op_ident) {
                    reqXThroughExpression(expression.down(1), NOACT, null, reqX,
                            beforeActiv, afterActiv, afterUseful, lastSweep);
                    // For the very rare case where the index or offset expressions contain
                    // differentiable assignments:
                    if (expression.opCode() != ILLang.op_fieldAccess) {
                        reqXThroughExpression(expression.down(2), NOACT, null, reqX,
                                beforeActiv, afterActiv, afterUseful, lastSweep);
                    }
                }
                break;
            case ILLang.op_address:
                // Checks: set03/cm09 set04/lh138 set04/ptr03 set10/lh207 set11/lh006...
                if (act != NOACT && TapList.oneTrue(reqXTree)) {
                    Tree inside = expression.down(1);
                    int insideOp = inside.opCode();
                    if (insideOp == ILLang.op_pointerAccess) {
                        // This is really for the C pointer case (not Fortran's array access):
                        reqXThroughExpression(inside.down(1), act, reqXTree, reqX,
                                beforeActiv, afterActiv, afterUseful, lastSweep);
                        // For the very rare case where the index expression contains assignments:
                        reqXThroughExpression(inside.down(2), NOACT, null, reqX,
                                beforeActiv, afterActiv, afterUseful, lastSweep);
                    } else {
                        reqXThroughExpression(inside, act, reqXTree.tail, reqX,
                                              beforeActiv, afterActiv, afterUseful, lastSweep);
                        // IT WOULD BE GOOD TO REMOVE THE FOLLOWING 5 LINES:
                        // These lines have a fuzzy, unclear justification: they force some
                        // non-pointer zones to be ReqX, and as a consequence will force initialization
                        // of their differentiated variables, that the diff pointer will point to.
                        // This may cause useless code created, and several diff structs keeping
                        // useless fields (see 10/mix55)!
                        // But, in a few cases (04/lh121), this solves a bug: as we did not
                        // (re-)initialize the passive derivative ("implicit-zero" optim), and the
                        // pointer points to it but also maybe to an active var, it is then too late
                        // to initialize the derivative of the pointed variable. This looks a little
                        // like the issue addressed by checkArrayActivitySwitches().
                        TapList accessedZonesTree =
                                curSymbolTable.treeOfZonesOfValue(ILUtils.baseTree(inside),
                                                                  null, curInstruction, null);
                        setInfoBoolTreeToExtendedDeclaredZones(reqX, accessedZonesTree,
                                      true, null, false, SymbolTableConstants.ALLKIND);

                    }
                }
                break;
            case ILLang.op_call:
                curCalledUnit = getCalledUnit(expression, curSymbolTable);
                if (curCalledUnit != null && curCalledUnit.hasParamElemsInfo()) {
                    ActivityPattern curCalledActivity =
                            (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(expression, curActivity,
                                    "multiActivityCalleePatterns");
                    if (curCalledActivity == null) {
                        curCalledActivity = getOrCreateCalledActivityPattern(curActivity, curCalledUnit, expression);
                    }
                    // curCalledActivity now certainly non-null.
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  Start propagating ReqX upstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    called Activity Pattern:" + curCalledActivity);
                        TapEnv.printlnOnTrace("    downcoming ReqX:" + vecLegend + " " + reqX.toString(nDZ));
                    }
                    CallArrow arrow = CallGraph.getCallArrow(curUnit, curCalledUnit);
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    int nbActualParams = actualParams.length;
                    BoolMatrix calleeRE = getReqXEffect(curCalledActivity) ;
                    BoolVector calleeREA = getReqXEffectAdded(curCalledActivity) ;
                    BoolVector calleeTouched = curCalledUnit.unitInOutPossiblyRorW() ;
                    if (calleeTouched!=null) {
                        removeNotUsedAtAll(calleeTouched, calleeRE, calleeREA);
                    }
                    BoolVector touched = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(calleeTouched, touched,
                                                  null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, false) ;

                    TapList[] paramDataS = new TapList[1+nbActualParams] ;
                    for (int i=nbActualParams ; i>=1 ; --i) {paramDataS[i] = null;}
                    paramDataS[0] = reqXTree ;
                    String vecCalledLegend = "[x" + curCalledUnit.rank() + ":a]";
                    BoolVector onCalleeReqX =
                        translateCallSiteDataToCallee(reqX, paramDataS, null, actualParams, false,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND) ;
                    // Only keep in onCalleeReqX those zones that are touched by curCalledUnit (i.e. read or written):
                    onCalleeReqX.cumulAnd(calleeTouched) ;

                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  (note touched:" + vecCalledLegend + " " + calleeTouched + " )");
                        TapEnv.printlnOnTrace("  expressed on callee:" + vecCalledLegend + " " + onCalleeReqX);
                    }
                    if (!onCalleeReqX.isFalse(curCalledUnit.paramElemsNb())) {
                        if (lastSweep) {
                            setAnnotatedPointerActive(curActivity, ILUtils.getCalledName(expression));
                        }
                    }
                    if (!curCalledUnit.isIntrinsic() && !curCalledUnit.isInterface() && !curCalledUnit.isVarFunction()) {
                        // Accumulate the new ReqX into the ReqX context at the exit of the called unit:
                        BoolVector calledContext = curCalledActivity.exitReqX();
                        if (calledContext == null) {
                            curCalledActivity.setExitReqX(onCalleeReqX.copy());
                            curCalledUnit.analysisIsOutOfDateUp = true;
                        } else if (calledContext.cumulOrGrows(onCalleeReqX, curCalledUnit.paramElemsNb())) {
                            curCalledUnit.analysisIsOutOfDateUp = true;
                        }
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("    accumulated into the callee's context --> " + vecCalledLegend + " " + curCalledActivity.exitReqX());
                        }
                    } else { // if is it necessary (?) to accumulate also for intrinsics, interfaces, varFunctions...
                        if (curCalledActivity.exitReqX() != null) {
                            //TODO: The protection above should go away: even on externals, we must cumulOr into something !!
                            curCalledActivity.exitReqX().cumulOr(onCalleeReqX);
                        }
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    pushed through calleeRE:");
                        TapEnv.dumpOnTrace(calleeRE);
                        TapEnv.printlnOnTrace("     and calleeREA:" + vecCalledLegend + " " + calleeREA);
                    }
                    // On a call U, propagation (upstream) multiplies the calleeRE of U on
                    // the right with the downstream ReqX, then adding calleeREA of U.
                    onCalleeReqX = calleeRE.times(onCalleeReqX).or(calleeREA);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    gives upgoing ReqX:" + vecCalledLegend + " " + onCalleeReqX);
                    }
                    // Store in advance the called unit's entry ReqX, to make formalArgsActivity() run correctly:
                    if (curCalledActivity.entryReqX() != null) {
                        //TODO: The protection above should go away: even on externals, we must cumulOr into something !!
                        curCalledActivity.entryReqX().cumulOr(onCalleeReqX);
                    }

                    // transform the onCalleeReqX upstream into the caller's private ReqX upstream the call:
                    BoolVector callSiteKilled = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(curCalledUnit.unitInOutCertainlyW(), callSiteKilled, null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, true) ;
                    TapList[] argsReqX =
                        translateCalleeDataToCallSite(onCalleeReqX, reqX, null, null, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND,
                                                      // Note: next "null" could/should indeed be replaced with "touched"
                                                      //  but this causes a bug in set04/lh189
                                                      // (?maybe because missing zone for "address of cx"?)
                                                      null, callSiteKilled.not(), true, false) ;

                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("    expressed as ReqX on call site:" + vecLegend + " " + reqX.toString(nDZ));
                        if (nbActualParams > 0) {
                            TapEnv.printlnOnTrace("     and pushing up through actual args:");
                        }
                    }
                    boolean[] formalArgsActivity =
                        ADActivityAnalyzer.formalArgsActivity(curCalledActivity);
                    for (int i = nbActualParams - 1; i >= 0; --i) {
                        boolean diffExprExists =
                            (TapList.oneTrue(argsReqX[1+i])
                             || (formalArgsActivity!=null && i<formalArgsActivity.length && formalArgsActivity[i]))
                            && ILUtils.isAVarRef(actualParams[i], curSymbolTable);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("       #"+(i+1)+" : "+actualParams[i]+" : "+argsReqX[1+i]);
                        }
                        reqXThroughExpression(actualParams[i], READ, argsReqX[1+i], reqX,
                                    beforeActiv, afterActiv, afterUseful, lastSweep);
                        // Moreover, if this call is active and this formal arg is active,
                        // and if the actual arg is a pointerAccess, the accessed pointers
                        // must become reqX, even if the actual arg is passive, because the
                        // diff call will need some diff argument passed there.
                        if (diffExprExists) {
                            TapIntList usedPointerRanks = collectUsedPointerRanks(actualParams[i], null, true);
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace("       also arg is pointer-active, usedPointerRanks=" + usedPointerRanks);
                            }
                            reqX.set(usedPointerRanks, true);
                        }
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  >Done propagating ReqX upstream through call: " + ILUtils.toString(expression)
                                + " ==> " + vecLegend + " " + reqX.toString(nDZ));
                    }
                }
                break;
            case ILLang.op_add:
            case ILLang.op_sub: {
                // Except in the case of pointer arithmetic, the "reqXTree" reqX requirement
                // on the operation's result does not propagate to the args:
                boolean argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(1)), SymbolTableConstants.POINTERTYPE);
                reqXThroughExpression(expression.down(1), READ, argIsPointer ? reqXTree : null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(2)), SymbolTableConstants.POINTERTYPE);
                reqXThroughExpression(expression.down(2), READ, argIsPointer ? reqXTree : null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            }
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_complexConstructor:
            case ILLang.op_iterativeVariableRef:
                // There is no binary combination on pointers
                // therefore the "reqXTree" reqX requirement on the
                // operation's result does not propagate on the args:
                reqXThroughExpression(expression.down(1), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(2), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_sizeof:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_switch:
            case ILLang.op_forall:
                reqXThroughExpression(expression.down(1), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_arrayDeclarator:
                reqXThroughExpression(expression.down(1), WRITE, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_unary: {
                // Except in the case of pointer arithmetic, the "reqXTree" reqX requirement
                // on the operation's result does not propagate to the argument:
                boolean argIsPointer = TypeSpec.isA(curSymbolTable.typeOf(expression.down(2)), SymbolTableConstants.POINTERTYPE);
                reqXThroughExpression(expression.down(2), READ, (argIsPointer ? reqXTree : null), reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
            case ILLang.op_constructorCall:
                reqXThroughExpression(expression.down(2), act, reqXTree, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_loop:
                reqXThroughExpression(expression.down(3), act, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                reqXThroughExpression(expression.down(1), WRITE, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(2), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(3), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(4), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_forallRangeSet:
                reqXThroughExpression(expression.down(1), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(2), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_for:
                reqXThroughExpression(expression.down(1), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(2), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(3), WRITE, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_ifExpression:
            case ILLang.op_arrayTriplet:
                reqXThroughExpression(expression.down(1), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(2), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                reqXThroughExpression(expression.down(3), READ, null, reqX,
                        beforeActiv, afterActiv, afterUseful, lastSweep);
                break;
            case ILLang.op_arrayConstructor:
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    reqXThroughExpression(sons[i], act, null, reqX,
                            beforeActiv, afterActiv, afterUseful, lastSweep);
                }
                break;
            }
            case ILLang.op_varDeclaration: {
                Tree[] decls = expression.down(3).children();
                inADeclaration = true;
                for (int i = decls.length - 1; i >= 0; i--) {
                    if (decls[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(decls[i]);
                        reqXThroughExpression(decls[i],
                                READ, null, reqX, beforeActiv, afterActiv, afterUseful, lastSweep);
                        ILUtils.resetAssignFromInitDecl(decls[i]);
                    }
                }
                inADeclaration = false;
                break;
            }
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_allocate:
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_strings:
            case ILLang.op_formatElem:
            case ILLang.op_ioCall:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            default:
                TapEnv.toolWarning(-1, "(ReqX analysis) Unexpected operator: " + expression.opName());
                break;
        }
    }

    /**
     * Removes extra unused zones from the given "publicUsed" vector of "used" zones.
     * An extra zone is detected unused when it is neither read nor modified according to
     * the given "publicRE' and "publicREA" information.
     * I.e. specifically when no row of publicRE (nor publicREA) has a 1 in this column.
     * "Identity" rows do not count as a "1" because identity means not changed.
     */
    private void removeNotUsedAtAll(BoolVector publicUsed, BoolMatrix publicRE, BoolVector publicREA) {
        if (publicREA != null && publicRE != null && publicRE.rows != null) {
            BoolVector used = publicREA.copy();
            BoolVector rowi;
            for (int i = publicRE.nRows - 1; i >= 0; --i) {
                rowi = publicRE.rows[i];
                if (rowi != null) {
                    used.cumulOr(rowi);
                    used.set(i, true);
                }
            }
            publicUsed.cumulAnd(used);
        }
    }

    /**
     * Finds if this expression will be differentiated.
     */
    private boolean diffExprExists(Tree expression, TapList reqXTree, int action,
                                   BoolVector beforeActiv, BoolVector afterActiv, BoolVector afterUseful) {
        // This is inspired from ADDirectDifferentiator.diffExpr() (case op_ident etc...)
        // These 2 pieces of code should ideally be merged, but how?
        if (TapList.oneTrue(reqXTree)) {
            return true;
        }
        Tree expr = expression;
        boolean exprIsPointer =
                TypeSpec.isA(curSymbolTable.typeOf(expression), SymbolTableConstants.POINTERTYPE);
        if (exprIsPointer) {
            expr = ILUtils.build(ILLang.op_pointerAccess,
                    ILUtils.copy(expression),
                    ILUtils.build(ILLang.op_none));
        }
        TapList exprZonesTree =
                curSymbolTable.treeOfZonesOfValue(expr, null, curInstruction, null);
        if (exprZonesTree != null) {
            exprZonesTree = TapList.copyTree(exprZonesTree);
            includePointedElementsInTree(exprZonesTree, null, null, true, false, true);
        }
        TapIntList exprZonesList = ZoneInfo.listAllZones(exprZonesTree, true);
        TapIntList diffKindZonesList = mapZoneRkToKindZoneRk(exprZonesList, diffKind);
        TapIntList diffKindPublicZonesList =
            TapIntList.removeAbove(diffKindZonesList, curUnit.publicZonesNumber(diffKind)-1) ;
        BoolVector staticActiv = curActivity.staticActivity();
        boolean diffExprExists =
                (!TapEnv.doActivity() && diffKindZonesList != null)
                        ||
                        (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression, curSymbolTable)
                                &&
                                ((action == READ &&
                                  ((beforeActiv != null && beforeActiv.intersects(diffKindZonesList))
                                   || (staticActiv != null && staticActiv.intersects(diffKindPublicZonesList))))
                                 ||
                                 (action == WRITE &&
                                  ((afterActiv != null && afterActiv.intersects(diffKindZonesList))
                                   || (staticActiv != null && staticActiv.intersects(diffKindPublicZonesList))
                                   || (afterUseful != null && afterUseful.intersects(diffKindZonesList))))));
        return diffExprExists;
    }

    /**
     * Propagate downstream (i.e. forwards) through the given "expression",
     * the AvlX Effect info consisting in avlXEffectVisible and avlXEffectAdded.
     *
     * @param lhs When expression is a function call, the actual variable that
     *            receives the result of the call. Can be null otherwise.
     */
    private void avlXEffectThroughExpression(Tree expression, Tree lhs,
                                             BoolVector avlXEffectVisible, BoolVector avlXEffectAdded, BoolVector reqX) {
        switch (expression.opCode()) {
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_return:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                boolean deallocOrNullify = (expression.opCode() == ILLang.op_deallocate
                        || expression.opCode() == ILLang.op_nullify);
                boolean isReturn = expression.opCode() == ILLang.op_return;
                if (isReturn) {
                    String returnVarName =
                            curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                } else {
                    lhs = expression.down(1);
                }
                Tree rhs;
                if (isReturn) {
                    rhs = expression.down(1);
                } else if (deallocOrNullify) {
                    rhs = null;
                } else {
                    rhs = expression.down(2);
                }
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  Start propagating AvlX effect downstream through: " + ILUtils.toString(expression) + " written zones: " + writtenZonesTree + (totalAccess ? "(total)" : "(non-total)"));
                }
                if (!deallocOrNullify) {
                    avlXEffectThroughExpression(rhs, lhs, avlXEffectVisible, avlXEffectAdded, reqX);
                }
                if (!isReturn) {
                    avlXEffectThroughExpression(lhs, null, avlXEffectVisible, avlXEffectAdded, reqX);
                    if (totalAccess) {
                        setInfoBoolTreeToExtendedDeclaredZones(avlXEffectVisible,
                                writtenZonesTree, false, null, true, SymbolTableConstants.ALLKIND);
                        setInfoBoolTreeToExtendedDeclaredZones(avlXEffectAdded,
                                writtenZonesTree, false, null, true, SymbolTableConstants.ALLKIND);
                    }
                }
                if (!deallocOrNullify) {
                    TapIntList writtenIndicesList = ZoneInfo.listAllZones(writtenZonesTree, true) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  writtenIndicesList: " + writtenIndicesList + " intersects ReqX: " + reqX + (reqX.intersects(writtenIndicesList) ? " yes!" : " no!"));
                    }
                    if (reqX.intersects(writtenIndicesList)) {
                        avlXEffectAdded.set(writtenIndicesList, true);
                    }
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  >Done propagating AvlX effect downstream through: " + ILUtils.toString(expression));
                    TapEnv.printlnOnTrace("   -->avlXEffectVisible:" + avlXEffectVisible);
                    TapEnv.printlnOnTrace("   -->avlXEffectAdded  :" + avlXEffectAdded);
                }
                break;
            }
            case ILLang.op_call:
                curCalledUnit = getCalledUnit(expression, curSymbolTable);
                ActivityPattern curCalledActivity =
                        (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(expression, curActivity,
                                "multiActivityCalleePatterns");
                if (curCalledUnit != null && curCalledActivity != null && curCalledUnit.hasParamElemsInfo()) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  Start propagating AvlX effect downstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    called Activity Pattern:" + curCalledActivity);
                        TapEnv.printlnOnTrace("    avlXEffectVisible:" + avlXEffectVisible);
                        TapEnv.printlnOnTrace("    avlXEffectAdded  :" + avlXEffectAdded);
                    }
                    CallArrow arrow = CallGraph.getCallArrow(curUnit, curCalledUnit);
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    // Save curCalledUnit because next avlXEffectThroughExpression() may change it !!
                    Unit savedCalledUnit = curCalledUnit;
                    for (int i = actualParams.length - 1; i >= 0; --i) {
                        avlXEffectThroughExpression(actualParams[i], null, avlXEffectVisible, avlXEffectAdded, reqX);
                    }
                    curCalledUnit = savedCalledUnit;
                    BoolVector onCalleeAEV =
                        translateCallSiteDataToCallee(avlXEffectVisible, null, null, actualParams, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND) ;
                    BoolVector onCalleeAEA =
                        translateCallSiteDataToCallee(avlXEffectAdded, null, null, actualParams, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     expressed on callee as Visible:   " + onCalleeAEV);
                        TapEnv.printlnOnTrace("                         and Added:    " + onCalleeAEA);
                    }
                    BoolVector calleeAEV = getAvlXEffectVisible(curCalledActivity);
                    BoolVector calleeAEA = getAvlXEffectAdded(curCalledActivity);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     keep only Visible through callee: " + calleeAEV);
                        TapEnv.printlnOnTrace("     add Added by callee:              " + calleeAEA);
                    }
                    onCalleeAEV.cumulAnd(calleeAEV);
                    onCalleeAEA.cumulAnd(calleeAEV);
                    onCalleeAEA.cumulOr(calleeAEA);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     --> gives upstream Visible:       " + onCalleeAEV);
                        TapEnv.printlnOnTrace("     -->              and Added:       " + onCalleeAEA);
                    }
                    // Translate back into the info downstream the call site:
                    translateCalleeDataToCallSite(onCalleeAEV, avlXEffectVisible, null, actualParams, false,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, false) ;
                                               // passesThroughCall:touched? passesAroundCall:callSiteKilled.not()?, valuesCarryInfo:true?
                    translateCalleeDataToCallSite(onCalleeAEA, avlXEffectAdded, null, actualParams, false,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, false) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  >Done propagating AvlX effect downstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    avlXEffectVisible:" + avlXEffectVisible);
                        TapEnv.printlnOnTrace("    avlXEffectAdded  :" + avlXEffectAdded);
                    }

                    // At this moment, we are sure activity analysis is done, and also
                    // the "maybe_required" phases (BOTTOMUP_1 and TOPDOWN_2).
                    // So it is time to propagate, bottom-up
                    // on the call graph, the info about required differentiated variables.
                    // The goal is to propagate into the calling context, above the
                    // differentiated "expression", the fact that the differentiated memory should be managed.
                    // This phase is not data-flow, as propagation is neither forwards nor backwards.
                    // This info is not the same as maybe_required, because it applies to
                    // "diffKind" variables instead of applying to pointers.
                    // This info is written into each SymbolTable of the curUnit and is
                    // finally used by method ADActivityAnalyzer.functionDiffFormalRequired()
                    SymbolTable calleePublicSymbolTable = curCalledUnit.publicSymbolTable();
                    if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression, curSymbolTable)
                            || (TapEnv.mustContext()
                            // The following test is related to "diffCallNeeded". Requires cleanup.
                            && (calleePublicSymbolTable != null || MPIcallInfo.isNonBlockingMPI(expression, curCalledUnit, curBlock)))) {
                        propagateDiffParamsRequired(expression, lhs, arrow, actualParams, curCalledActivity, curInstruction, curActivity);
                    }
                }
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_allocate:
                break;
            default: {
                if (!expression.isAtom()) {
                    Tree[] subTrees = expression.children();
                    for (int i = subTrees.length - 1; i >= 0; --i) {
                        avlXEffectThroughExpression(subTrees[i], null, avlXEffectVisible, avlXEffectAdded, reqX);
                    }
                }
            }
        }
    }

    /**
     * Translate the callee Unit's info "calleeDiffParamsRequired"
     * (about its public-elementary-args that are differentiated)
     * as the same info "privateRequiredDiffk" based on the diffKind private zones at the call site.
     * Then accumulate this call site info into the "RequiredDiffVars" of the calling Unit's SymbolTable's.
     */
    private static void propagateDiffParamsRequired(Tree expression, Tree lhs,
                                                    CallArrow arrow, Tree[] actualParams, ActivityPattern calledActivity,
                                                    Instruction instr, ActivityPattern callingActivity) {
        SymbolTable symbolTable = instr.block.symbolTable;
        Unit callingUnit = instr.block.unit();
        BoolVector calleeDiffParamsRequired =
                calledActivity == null ? null : ADActivityAnalyzer.functionDiffFormalRequired(calledActivity, false);
        if (calleeDiffParamsRequired != null) {
            TapList actualResultRequired = new TapList<>(null, null);
            // Remove from actualParams those actual arguments for which the passed derivative will be a dummyzerodiff:
            Tree[] actuallyDiffActualParams = new Tree[actualParams.length];
            for (int i = actualParams.length - 1; i >= 0; --i) {
                // cf the tests in ProcedureCallDifferentiator that trigger creation of dummyzerodiff:
                if ((ADActivityAnalyzer.isAnnotatedActive(callingActivity, actualParams[i], symbolTable)
                        || ILUtils.isAWritableIdentVarRef(actualParams[i], symbolTable))
                        //[llh 19/10/2015] This test varIsIntentIn() is dangerous: needed only for tangent mode ?
                        && !symbolTable.varIsIntentIn(actualParams[i])) {
                    actuallyDiffActualParams[i] = actualParams[i];
                } else {
                    actuallyDiffActualParams[i] = null;
                }
            }
            // Need a copy only on diffKind zones, to be accumulated in the SymbolTables' RequiredDiffVars:
            int ndrz = symbolTable.declaredZonesNb(TapEnv.diffKind());
            BoolVector privateRequiredDiffk = new BoolVector(ndrz);
            calleeDiffParamsRequired = arrow.destination.focusToKind(calleeDiffParamsRequired, TapEnv.diffKind()) ;
            translateCalleeDataToCallSite(calleeDiffParamsRequired, privateRequiredDiffk,
                    lhs, actuallyDiffActualParams, true,
                    expression, instr, arrow, TapEnv.diffKind(), null, null, true, false) ;
            SymbolTable callerSymbolTable = symbolTable;
            SymbolTable callerContextSymbolTable = callingUnit.externalSymbolTable();
            while (callerSymbolTable != callerContextSymbolTable) {
                callerSymbolTable.setOrCumulRequiredDiffVars(callingActivity, privateRequiredDiffk);
                // Go up to the SymbolTable level above :
                callerSymbolTable = callerSymbolTable.basisSymbolTable();
                ndrz = callerSymbolTable.declaredZonesNb(TapEnv.diffKind());
                BoolVector tmpzv = new BoolVector(ndrz);
                tmpzv.cumulOr(privateRequiredDiffk, ndrz);
                privateRequiredDiffk = tmpzv;
            }
        }
    }

    /**
     * Propagate the avlX info forwards downstream through the given "expression".
     *
     * @param reqX The value of the reqX info just downstream the current instruction
     */
    private void avlXThroughExpression(Tree expression, Tree lhs, BoolVector avlX, BoolVector reqX) {
        switch (expression.opCode()) {
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_return:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                boolean deallocOrNullify = (expression.opCode() == ILLang.op_deallocate
                        || expression.opCode() == ILLang.op_nullify);
                boolean isReturn = expression.opCode() == ILLang.op_return;
                if (isReturn) {
                    String returnVarName =
                            curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                } else {
                    lhs = expression.down(1);
                }
                Tree rhs;
                if (isReturn) {
                    rhs = expression.down(1);
                } else if (deallocOrNullify) {
                    rhs = null;
                } else {
                    rhs = expression.down(2);
                }
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  Start propagating AvlX downstream through: " + ILUtils.toString(expression) + " written zones: " + writtenZonesTree + (totalAccess ? "(total)" : "(non-total)"));
                }
                if (!deallocOrNullify) {
                    avlXThroughExpression(rhs, lhs, avlX, reqX);
                }
                if (!isReturn) {
                    avlXThroughExpression(lhs, null, avlX, reqX);
                    if (totalAccess) {
                        setInfoBoolTreeToExtendedDeclaredZones(avlX,
                                writtenZonesTree, false, null, true, SymbolTableConstants.ALLKIND);
                    }
                }
                if (!deallocOrNullify) {
                    TapIntList writtenIndicesList = ZoneInfo.listAllZones(writtenZonesTree, true) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  writtenIndicesList: " + writtenIndicesList + " intersects ReqX:" + vecLegend + " " + (reqX != null ? reqX.toString(nDZ) : "Null") + (reqX != null && reqX.intersects(writtenIndicesList) ? " yes!" : " no!"));
                    }
                    if (reqX != null && reqX.intersects(writtenIndicesList)) {
                        avlX.set(writtenIndicesList, true);
                    }
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  >Done propagating AvlX downstream through: " + ILUtils.toString(expression));
                    TapEnv.printlnOnTrace("   -->AvlX:" + vecLegend + " " + avlX.toString(nDZ));
                }
                break;
            }
            case ILLang.op_call:
                curCalledUnit = getCalledUnit(expression, curSymbolTable);
                ActivityPattern curCalledActivity =
                        (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(expression, curActivity, "multiActivityCalleePatterns");
                if (curCalledUnit != null && curCalledActivity == null) {
                    curCalledActivity = getOrCreateCalledActivityPattern(curActivity, curCalledUnit, expression);
                }
                // curCalledActivity now certainly non-null.
                if (curCalledUnit != null && curCalledUnit.hasParamElemsInfo()) {
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  Start propagating AvlX downstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("    called Activity Pattern:" + curCalledActivity);
                        TapEnv.printlnOnTrace("    AvlX:" + vecLegend + " " + avlX.toString(nDZ));
                    }
                    CallArrow arrow = CallGraph.getCallArrow(curUnit, curCalledUnit);
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    // Save curCalledUnit because recursion in next avlXThroughExpression() may change it !!
                    Unit savedCalledUnit = curCalledUnit;
                    for (int i = actualParams.length - 1; i >= 0; --i) {
                        avlXThroughExpression(actualParams[i], null, avlX, reqX);
                    }
                    curCalledUnit = savedCalledUnit;
                    BoolVector onCalleeAvlX =
                        translateCallSiteDataToCallee(avlX, null, null, actualParams, true,
                                                      expression, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND) ;
                    String vecCalledLegend = "[x" + curCalledUnit.rank() + ":a]";
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     expressed on callee as:" + vecCalledLegend + " " + onCalleeAvlX);
                    }
                    if (!curCalledUnit.isIntrinsic() && !curCalledUnit.isInterface() && !curCalledUnit.isVarFunction()) {
                        // Accumulate this AvlX into the AvlX context at the entry of the called unit:
                        BoolVector calledContext = curCalledActivity.entryAvlX();
                        if (calledContext == null) {
                            curCalledActivity.setEntryAvlX(onCalleeAvlX.copy());
                            curCalledUnit.analysisIsOutOfDateDown = true;
                        } else if (calledContext.cumulOrGrows(onCalleeAvlX, curCalledUnit.paramElemsNb())) {
                            curCalledUnit.analysisIsOutOfDateDown = true;
                        }
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("     accumulated callee's context  --> " + curCalledActivity.entryAvlX());
                        }
                    }
                    BoolVector calleeAEV = getAvlXEffectVisible(curCalledActivity);
                    BoolVector calleeAEA = getAvlXEffectAdded(curCalledActivity);
                    onCalleeAvlX.cumulAnd(calleeAEV);
                    onCalleeAvlX.cumulOr(calleeAEA);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("     keep only visible through callee: " + calleeAEV);
                        TapEnv.printlnOnTrace("     add added by callee:              " + calleeAEA);
                        TapEnv.printlnOnTrace("     --> gives:                        " + onCalleeAvlX);
                    }

                    if (curCalledActivity.exitAvlX() == null) {
                        curCalledActivity.setExitAvlX(onCalleeAvlX);
                    } else {
                        curCalledActivity.exitAvlX().cumulOr(onCalleeAvlX);
                    }

                    // Compute the "touched" mask forthe coming propagation of onCalleeAvlX into avlX:
                    BoolVector touched = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(curCalledUnit.unitInOutPossiblyRorW(), touched,
                                                  null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  null, null, false, false) ;
                    // Translate back into the info downstream the call site:
                    translateCalleeDataToCallSite(onCalleeAvlX, avlX, null, actualParams, false,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND,
                                                  touched, null, false, false) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  >Done propagating AvlX downstream through call: " + ILUtils.toString(expression));
                        TapEnv.printlnOnTrace("   -->avlX:" + vecLegend + " " + avlX.toString(nDZ));
                    }
                }
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_allocate:
                break;
            default: {
                if (!expression.isAtom()) {
                    Tree[] subTrees = expression.children();
                    for (int i = subTrees.length - 1; i >= 0; --i) {
                        avlXThroughExpression(subTrees[i], null, avlX, reqX);
                    }
                }
            }
        }
    }

    /**
     * Look for an existing ActivityPattern for the "calledUnit" that matches
     * the "callingActivity" of the call site. If none exists, creates a new ActivityPattern
     */
    private ActivityPattern getOrCreateCalledActivityPattern(ActivityPattern callingActivity, Unit calledUnit, Tree callExpression) {
        ActivityPattern calledActivity = null, existingActivity;
        TapList<ActivityPattern> calledActivityPatternS = calledUnit.activityPatterns;
        while (calledActivity == null && calledActivityPatternS != null) {
            existingActivity = calledActivityPatternS.head;
            if (// either both activityPatterns are context, or both non-context:
                callingActivity.isContext() == existingActivity.isContext()
                // or this is a context activity calling a diffRoot:
                || (callingActivity.isContext() && existingActivity.diffPattern() != null)) {
                calledActivity = existingActivity;
            }
            calledActivityPatternS = calledActivityPatternS.tail;
        }
        if (calledActivity == null) {
            calledActivity = new ActivityPattern(calledUnit, null, null, true, diffKind);
            calledActivity.registerCallingActivityPattern(callingActivity);
            calledActivity.setContext(callingActivity.isContext());
            if (!callingActivity.isContext()) {
                // If we are now creating a new non-context ActivityPattern, we must at least give it
                // a copy of the info we have for the corresponding context ActivityPattern (which is unique).
                ActivityPattern contextPattern = null;
                calledActivityPatternS = calledUnit.activityPatterns;
                while (contextPattern == null && calledActivityPatternS != null) {
                    existingActivity = calledActivityPatternS.head;
                    if (existingActivity.isContext()) {
                        contextPattern = existingActivity;
                    }
                    calledActivityPatternS = calledActivityPatternS.tail;
                }
                if (contextPattern != null) {
                    calledActivity.copyReqExplicitInfos(contextPattern);
                }
            }
            calledUnit.activityPatterns = new TapList<>(calledActivity, calledUnit.activityPatterns);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("     in passing, created an ActivityPattern for " + calledUnit + ":" + calledActivity);
            }
        }
        calledActivity.registerCallingActivityPattern(callingActivity);
        ActivityPattern.setAnnotationForActivityPattern(callExpression, callingActivity, "multiActivityCalleePatterns", calledActivity);
        return calledActivity;
    }

    private ActivityPattern createTemporaryDummyEmptyActivityPattern(Unit calledUnit) {
        int size = calledUnit.paramElemsNb();
        BoolVector emptyVaried = new BoolVector(size);
        BoolVector emptyUseful = new BoolVector(size);
        ActivityPattern dummyActivity = new ActivityPattern(calledUnit, emptyVaried, emptyUseful, true, diffKind);
        return dummyActivity;
    }

    private TapIntList collectUsedPointerRanks(Tree expression, TapIntList accumulator, boolean annotateReqX) {
        switch (expression.opCode()) {
            case ILLang.op_address:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_minus:
                return collectUsedPointerRanks(expression.down(1), accumulator, annotateReqX);
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
                return collectUsedPointerRanks(expression.down(2), accumulator, annotateReqX);
            case ILLang.op_pointerAccess: {
                if (annotateReqX) {
                    setAnnotatedPointerActive(curActivity, expression.down(1));
                }
                TapIntList pointerRanks = ZoneInfo.listAllZones(
                                              curSymbolTable.treeOfZonesOfValue(expression.down(1),
                                                                                null, curInstruction, null),
                                              true) ;
                accumulator = TapIntList.quickUnion(accumulator, pointerRanks);
                return collectUsedPointerRanks(expression.down(1), accumulator, annotateReqX);
            }
	    case ILLang.op_arrayConstructor:
	    {
		//get all children
		Tree[] children = expression.children();
		// iterator on the children
		for(int i=0; i<children.length; ++i) {
		    accumulator = collectUsedPointerRanks(children[i], accumulator, annotateReqX);
		}
		return accumulator;
	    }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_complexConstructor:
            case ILLang.op_concat:
                accumulator = collectUsedPointerRanks(expression.down(1), accumulator, annotateReqX);
                return collectUsedPointerRanks(expression.down(2), accumulator, annotateReqX);
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                return accumulator;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                return accumulator;
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_ident:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_label:
            case ILLang.op_sizeof:
            case ILLang.op_none:
                return accumulator;
            case ILLang.op_call:
            case ILLang.op_ifExpression:
                // ^ These are complicated, rare cases. Hoping it's not necessary to handle them.
                return accumulator;
            default:
                TapEnv.toolWarning(-1, "(Collect used pointer ranks) Unexpected operator: " + expression.opName());
                return accumulator;
        }
    }

// Public primitives that retrieve the analysis'result :


    private BoolMatrix getReqXEffect(ActivityPattern calledActivity) {
        Unit calledUnit = null;
        if (calledActivity != null) {
            calledUnit = calledActivity.unit();
        }
        if (calledUnit == null) {
            return null;
        }
        if (calledUnit.rank() <= 0) {
            return buildDefaultReqXEffect(calledUnit);
        } else {
            BoolMatrix reqXEffect = calledActivity.reqXEffects();
            if (reqXEffect == null) {
                reqXEffect = buildDefaultReqXEffect(calledUnit);
            }
            return reqXEffect;
        }
    }

    private BoolVector getReqXEffectAdded(ActivityPattern calledActivity) {
        Unit calledUnit = null;
        if (calledActivity != null) {
            calledUnit = calledActivity.unit();
        }
        if (calledUnit == null) {
            return null;
        }
        if (calledUnit.rank() <= 0) {
            return buildDefaultReqXEffectAdded(calledUnit);
        } else {
            BoolVector reqXEffectAdded = calledActivity.reqXEffectsAdded();
            if (reqXEffectAdded == null) {
                reqXEffectAdded = buildDefaultReqXEffectAdded(calledUnit);
            }
            return reqXEffectAdded;
        }
    }

    private BoolVector getAvlXEffectVisible(ActivityPattern calledActivity) {
        Unit calledUnit = null;
        if (calledActivity != null) {
            calledUnit = calledActivity.unit();
        }
        if (calledUnit == null) {
            return null;
        }
        if (calledUnit.rank() <= 0) {
            return buildDefaultAvlXEffectVisible(calledUnit);
        } else {
            BoolVector avlXEffectVisible = calledActivity.avlXEffectsVisible();
            if (avlXEffectVisible == null) {
                avlXEffectVisible = buildDefaultAvlXEffectVisible(calledUnit);
            }
            return avlXEffectVisible;
        }
    }

    private BoolVector getAvlXEffectAdded(ActivityPattern calledActivity) {
        Unit calledUnit = null;
        if (calledActivity != null) {
            calledUnit = calledActivity.unit();
        }
        if (calledUnit == null) {
            return null;
        }
        if (calledUnit.rank() <= 0) {
            return buildDefaultAvlXEffectAdded(calledUnit);
        } else {
            BoolVector avlXEffectAdded = calledActivity.avlXEffectsAdded();
            if (avlXEffectAdded == null) {
                avlXEffectAdded = buildDefaultAvlXEffectAdded(calledUnit);
            }
            return avlXEffectAdded;
        }
    }

    private BoolMatrix buildDefaultReqXEffect(Unit calledUnit) {
        // As a 1st approximation, we build an Identity matrix,
        // except that each pointer zone (of a differentiated type)
        // which is totally overwritten in the call becomes non reqX.
        int n = calledUnit.paramElemsNb();
        BoolMatrix result = new BoolMatrix(n, n);
        result.setIdentity();
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(calledUnit);
        BoolVector tmpPubK = calledUnit.unitInOutCertainlyW();
        BoolVector nonReqX = pointerZonesMask.and(tmpPubK) ;
        for (int i = n - 1; i >= 0; --i) {
            if (nonReqX.get(i)) {
                result.rows[i] = new BoolVector(n);
            }
        }
        return result;
    }

    private BoolVector buildDefaultReqXEffectAdded(Unit calledUnit) {
        // As a 1st approximation, we build an empty vector,
        // except that each pointer zone (of a differentiated type)
        // which is possibly read in the call becomes reqX.
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(calledUnit);

        // [llh] patched on 17sep21. Special case "associated" and "allocated" must not introduce ReqX effect added
        // Patch hopefully unnecessary when the next alternative (more accurate?) code is in place.
        if (curUnit.isFortran9x()
            && ("associated".equals(calledUnit.name()) || "allocated".equals(calledUnit.name())))
            pointerZonesMask.setFalse() ;

//         // [llh 9Aug18] TODO: Alternative code, trying to solve bug in 10/mix35,
//         // but introduces regressions on 04/lh129 04/lh144? 06/v260 09/v204
//         // Restrict to pointer zones that point upon calledUnit entry or exit to some effectively active variable!
//         if (calledActivity.callActivity!=null && calledActivity.exitActivity!=null) {
//             BoolVector pointToActive = new BoolVector(calledUnit.paramElemsNb()) ;
//             ZoneInfo zoneInfo ;
//             for (int i=calledUnit.paramElemsNb()-1 ; i>=0 ; --i) {
//                 if (calledActivity.callActivity.get(i) && calledActivity.exitActivity.get(i)) {
//                     zoneInfo = calledUnit.externalShape[i] ;
//                     if (zoneInfo!=null) zoneInfo = zoneInfo.targetZoneOf ;
//                     while (zoneInfo!=null && !pointToActive.get(zoneInfo.zoneNb)) {
//                         pointToActive.set(zoneInfo.zoneNb, true) ;
//                         zoneInfo = zoneInfo.targetZoneOf ;
//                     }
//                 }
//             }
//             pointerZonesMask.cumulAnd(pointToActive) ;
//         }

        BoolVector tmpPubR = calledUnit.unitInOutPossiblyR();
        return pointerZonesMask.and(tmpPubR);
    }

    private BoolVector buildDefaultAvlXEffectVisible(Unit calledUnit) {
        // As a 1st approximation, we build an full vector,
        // except that each pointer zone (of a differentiated type)
        // which is totally overwritten in the call becomes masked i.e. not visible.
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(calledUnit);
        pointerZonesMask.setTrue();
        BoolVector tmpPubK = calledUnit.unitInOutCertainlyW();
        return pointerZonesMask.minus(tmpPubK) ;
    }

    private BoolVector buildDefaultAvlXEffectAdded(Unit calledUnit) {
        // As a 1st approximation, we build an empty vector:
        int n = calledUnit.paramElemsNb();
        return new BoolVector(n);
    }

    /**
     * @return true when "callTree", a particular call to a Unit (although it may be non-active)
     * must be differentiated because one of its ACTUAL arguments
     * (either parameter or global or side-effect...) is a pointer and
     * is both reqX JUST DOWNSTREAM the call and overwritten during the call.
     * NOTE: we believe we must not make the call ReqX when
     * a pointer arg is reqX UPSTREAM the call and read during the call,
     * (although this makes the arg itself reqX when the call is reqX, cf set03/cm04)
     * because this is true only when the arg is read to fill another pointer arg
     * which is itself active of reqX, and therefore the call will be found ReqX anyway.
     * Moreover, this would find too many reqX calls.
     * Also, the call must be differentiated if one of its actual arguments is a
     * pointer and is avlX JUST UPSTREAM the call and overwritten during the call.
     */
    private boolean isPointerActiveCall(Tree callTree, Unit callingUnit, Unit calledUnit,
                                       SymbolTable callSymbolTable, Instruction callInstruction,
                                       BoolVector beforeAvlX, BoolVector afterReqX, TapList reqXResult) {
        // Quick exit on intrinsics (should not use pointers?) ?[llh] NOT SURE THIS IS RIGHT?
        if (calledUnit==null || calledUnit.isIntrinsic()) {
            return false ;
        }
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(calledUnit);
        // Quick exit if no pointers involved.
        if (pointerZonesMask.isFalse(calledUnit.paramElemsNb())) {
            return false ;
        }
        CallArrow arrow = CallGraph.getCallArrow(callingUnit, calledUnit);

        Unit memoUnit = curUnit ;
        Unit memoCalledUnit = curCalledUnit ;
        SymbolTable memoSymbolTable = curSymbolTable ;
        Instruction memoInstruction = curInstruction ;

        setCurUnitEtc(callingUnit);
        curCalledUnit = calledUnit;
        curSymbolTable = callSymbolTable;
        nDZ = callSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        vecLegend = "[||d" + callSymbolTable.rank() + ":a]";
        Tree[] actualParams = ILUtils.getArguments(callTree).children();
        // Compute the public ReqX info just downstream the call:
        curInstruction = callInstruction;
        setUniqueAccessZones(curInstruction.block);
        BoolVector reqOrAvl = null;
        if (afterReqX != null) {
            int nbActualParams = actualParams.length ;
            TapList[] paramDataS = new TapList[1+nbActualParams] ;
            for (int i=nbActualParams ; i>=1 ; --i) {paramDataS[i] = null ;}
            paramDataS[0] = reqXResult ;
            BoolVector downstreamCallReqX =
                translateCallSiteDataToCallee(afterReqX, paramDataS, null, actualParams, false,
                                              callTree, callInstruction, arrow, SymbolTableConstants.ALLKIND) ;

            reqOrAvl = downstreamCallReqX;
        }
        if (beforeAvlX != null) {
            BoolVector upstreamCallAvlX =
                    translateCallSiteDataToCallee(beforeAvlX, null, null, actualParams, true,
                                                  callTree, callInstruction, arrow, SymbolTableConstants.ALLKIND) ;

            reqOrAvl = (reqOrAvl==null ? upstreamCallAvlX : reqOrAvl.or(upstreamCallAvlX));
        }
        reqOrAvl = reqOrAvl.and(pointerZonesMask);
        if (calledUnit.unitInOutR != null) {
            BoolVector tmpPubW = calledUnit.unitInOutPossiblyW();
            reqOrAvl = reqOrAvl.and(tmpPubW) ;
        }
        uniqueAccessZones = null;

        setCurUnitEtc(memoUnit);
        curCalledUnit = memoCalledUnit ;
        curSymbolTable = memoSymbolTable ;
        curInstruction = memoInstruction ;
        return !reqOrAvl.isFalse(calledUnit.paramElemsNb()) ;
    }

    /**
     * @return true when the given Unit (although it may be non-active)
     * must be differentiated because one of its FORMAL arguments
     * (either parameter or global or side-effect...) is a pointer and
     * is overwritten during the Unit and (it is either reqX upon exit
     * from some call or avlX upon entry into some call).
     * This happens when at least one call to it is ReqX or AvlX.
     * (cf isPointerActiveCall() ; Read the NOTE on method isPointerActiveCall() ).
     * Also, when both reqX and avlX are empty, which may mean that the Unit is not called
     * by the call tree that contains the differentiated part (at least in the
     * given code), the Unit is considered PointerActive when at least one of
     * the pointers it overwrites is active some time in the differentiated part.
     * We do that so that routines that are outside the call tree but do prepare
     * pointers, get differentiated and can be used to prepare the diff pointers.
     */
    public boolean isPointerActiveUnit(ActivityPattern curActivity) {
        Unit unit = curActivity.unit();
        if (!unit.hasParamElemsInfo()) {
            return false;
        }
        int len = unit.paramElemsNb();
        BoolVector exitReqX = curActivity.exitReqX();
        BoolVector entryAvlX = curActivity.entryAvlX();
        BoolVector activePointers;
        if (exitReqX == null || exitReqX.isFalse(len)) {
            if (entryAvlX == null || entryAvlX.isFalse(len)) {
                //Do the following only when option "-context" is on.
                if (!TapEnv.mustContext()) {
                    return false;
                }
                activePointers = new BoolVector(len);
                ZoneInfo zi;
                for (int i = len - 1; i >= 0; --i) {
                    zi = unit.paramElemZoneInfo(i);
                    if (zi != null && (zi.isOnceActive() || zi.from != null && zi.from.isOnceActive())) {
                        activePointers.set(i, true);
                    }
                }
            } else {
                activePointers = entryAvlX;
            }
        } else {
            if (entryAvlX == null) {
                activePointers = exitReqX;
            } else {
                activePointers = entryAvlX.or(exitReqX);
            }
        }
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(unit);
        BoolVector reqXVect = activePointers.and(pointerZonesMask);
        if (unit.unitInOutR != null) {
            BoolVector tmpPubW = unit.unitInOutPossiblyW();
            reqXVect = reqXVect.and(tmpPubW) ;
        }
        return !reqXVect.isFalse(len);
    }

    private void setRefAnnotatedPointerActive(ActivityPattern pattern, Tree tree) {
        setAnnotatedPointerActive(pattern, tree);
        if (tree.opCode() == ILLang.op_arrayAccess
                || tree.opCode() == ILLang.op_pointerAccess
                || tree.opCode() == ILLang.op_fieldAccess
                || tree.opCode() == ILLang.op_address) {
            setRefAnnotatedPointerActive(pattern, tree.down(1));
        }
    }

    /**
     * @return the BoolVector of the pointer public zones indices whose
     * differentials are managed during Unit, i.e. they are
     * AvlX at exit of Unit and they are possibly overwritten during the Unit.
     * This should be true in particular if the unit makes the pointer point
     * to some new address and this address'es memory is still allocated, which
     * implies it is the context's responsibility to free it afterwards.
     */
    public BoolVector diffZonesManaged(ActivityPattern pattern) {
        if (!pattern.unit().hasParamElemsInfo()) {
            return null;
        }
        BoolVector exitReqX = pattern.exitReqX();
        BoolVector entryAvlX = pattern.entryAvlX();
        if (exitReqX == null && entryAvlX == null) {
            return null;
        }
        BoolVector pointerZonesMask = buildUnitPointerZoneMask(pattern.unit());
        BoolVector activePointers =       // exitReqX OR entryAvlX :
                exitReqX == null ? entryAvlX : entryAvlX == null ? exitReqX : entryAvlX.or(exitReqX);
        BoolVector result = activePointers.and(pointerZonesMask);
        if (pattern.unit().unitInOutR != null) {
            BoolVector tmpPubW = pattern.unit().unitInOutPossiblyW();
            result = result.and(tmpPubW) ;
        }
        return result;
    }

    /**
     * @return the BoolVector of the pointer public zones indices whose
     * differentials are required at Unit entry, i.e. they are
     * ReqX at entry of Unit and they are possibly read or written during the Unit.
     * This should be true in particular if the unit derefs the destination
     * (upon entry into "unit") of the diff of a pointer, which must therefore
     * point upon entry to some allocated diff memory location.
     */
    public BoolVector diffZonesRequired(ActivityPattern pattern) {
        Unit unit = pattern.unit();
        BoolVector reqXVect = null;
        BoolVector entryReqX = pattern.entryReqX();
        if (entryReqX != null) {
            // entryReqX may be null for Units that stop, modules,
            //  external, non-diff routines...
            BoolVector pointerZonesMask = buildUnitPointerZoneMask(unit);
            reqXVect = pointerZonesMask.and(entryReqX);
            if (unit.unitInOutR != null) {
                BoolVector tmpPubRorW = unit.unitInOutPossiblyRorW();
                reqXVect = reqXVect.and(tmpPubRorW) ;
            }
        }
        return reqXVect;
    }

    private void cleanEmptyActivities() {
        for (int i = curCallGraph.nbUnits() - 1; i >= 0; i--) {
            setCurUnitEtc(curCallGraph.sortedUnit(i));
            if (curUnit.isStandard()) {

            }
        }
    }

}
