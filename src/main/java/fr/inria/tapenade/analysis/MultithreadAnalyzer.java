/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import java.io.*;
import com.microsoft.z3.*;
import java.util.HashMap;

import fr.inria.tapenade.utils.* ;
import fr.inria.tapenade.representation.* ;

import fr.inria.tapenade.differentiation.CallGraphDifferentiator ; //TODO: remove this circular dependency, needed by FormAD.

/**
 * Analysis of multithread clauses for e.g. OpenMP or Cuda.
 * At present, computes two things:
 * <ul>
 *  <li> For every Block, whether this block is run multithread,
 *    and if so the (zones of) variables that are shared </li>
 *  <li> For every multithread region, a collection of indexes used
 *    in the primal code to refer to shared variables, which may be used
 *    to prove that accesses to differentiated shared variables are conflict-free. </li>
 * </ul>
 */
public final class MultithreadAnalyzer extends DataFlowAnalyzer {

    public MultithreadAnalyzer(CallGraph cg) {
        super(cg, "Analysis of multithread clauses", TapEnv.traceMultithread());
    }

    /**
     * The current phase of this analysis.
     * In {FIND_SHARED_1, other?}
     */
    private int phase;

    /**
     * Code for the phase of OpemMP analysis that propagates shared variables.
     */
    private static final int FIND_SHARED_1 = 1;

    /**
     * One result of this analysis, containing the multithread info
     * upon entry into each Unit of the CallGraph.
     */
    private UnitStorage<ParallelInfoOfUnit> parallelInfosOfUnits = null;

    /**
     * One result of this analysis, containing the multithread info
     * for each Block of each Unit of the CallGraph.
     */
    private UnitStorage<BlockStorage<ParallelInfoOfBlock>> parallelInfosOfUnitsBlocks = null;

    /** For each Block of the current Unit, its "context" inside its enclosing parallel-region. */
    private BlockStorage<MTContext> blockContexts ;

    /** The list of all created contexts of the current Unit */
    private TapList<MTContext> existingContexts ;

    /** For each Block of the current Unit, between successive Instructions, the "instance" numbers of each variable. */
    private BlockStorage<TapList<MTInstances>> blockInstances ;

    public static void runAnalysis(CallGraph callGraph) {
        MultithreadAnalyzer analyzer = new MultithreadAnalyzer(callGraph);
        TapEnv.setMultithreadAnalyzer(analyzer);
        analyzer.run();
    }

    /**
     * Runs detection.
     */
    public void run() {
        phase = FIND_SHARED_1 ;
        parallelInfosOfUnits = new UnitStorage<>(curCallGraph);
        parallelInfosOfUnitsBlocks = new UnitStorage<>(curCallGraph);
        // Heuristic: we force arguments of the top "not-called" Units of the code to be
        // considered shared. Arguments of other Units will inherit from their call sites:
        TapList<Unit> topUnits = nonCalledUnits(curCallGraph.sortedComputationalUnits()) ;
        runTopDownAnalysis(topUnits);
    }

    private TapList<Unit> nonCalledUnits(TapList<Unit> units) {
        TapList<Unit> hdRes = new TapList<>(null, null) ;
        TapList<Unit> tlRes = hdRes ;
        while (units!=null) {
            if (units.head.isStandard() && units.head.callersThatCall()==null)
                tlRes = tlRes.placdl(units.head) ;
            units = units.tail ;
        }
        return hdRes.tail ;
    }

    public boolean isGPU(Unit unit) {
        ParallelInfoOfUnit unitResult = parallelInfosOfUnits.retrieve(unit) ;
        return (unitResult!=null && unitResult.isGPU) ;
    }

    /**
     * Returns the detected shared zones for the given Unit.
     */
    public BoolVector getSharedZonesOfUnit(Unit unit) {
        ParallelInfoOfUnit unitResult = parallelInfosOfUnits.retrieve(unit) ;
        return (unitResult==null ? null : unitResult.sharedVariables) ;
    }

    /**
     * Returns the detected shared zones for the given Block.
     */
    public BoolVector getSharedZonesOfBlock(Block block) {
        BlockStorage<ParallelInfoOfBlock> unitBlocksResult = parallelInfosOfUnitsBlocks.retrieve(block.unit()) ;
        ParallelInfoOfBlock blockResults = (unitBlocksResult==null ? null : unitBlocksResult.retrieve(block));
        return ((blockResults==null || !blockResults.inParallelContext) ? null : blockResults.sharedVariables) ;
    }
    public BoolVector getConflictFreeSharedAdjointZones(Block block) {
        Block parallelRegionBlock = block;
        if (block.parallelControls!=null) {
            parallelRegionBlock = TapList.last(block.parallelControls).block ;
        }
        Unit unit = parallelRegionBlock.unit();
        ParallelInfoOfBlock blockResults = parallelInfosOfUnitsBlocks.retrieve(unit).retrieve(parallelRegionBlock);
        return (blockResults==null ? null : blockResults.conflictFreeSharedAdjointVariables) ;
    }

    /**
     * The analyzed info for one Unit.
     */
    private static final class ParallelInfoOfUnit {

        public boolean inParallelContext = false;

        public boolean isGPU = false ;

        private BoolVector sharedVariables;

        private ParallelInfoOfUnit(Unit unit) {
            int shapeLength = unit.paramElemsNb();
            sharedVariables = new BoolVector(shapeLength);
        }

        public String toString() {
            return "//info of U:"+inParallelContext+isGPU+" shared:"+sharedVariables ;
        }
    }

    /**
     * The analyzed info for one Block. Many fields of this object are shared between
     * the ParallelInfoOfBlock of all Blocks inside a same parallel region.
     */
    private final class ParallelInfoOfBlock {

        private boolean inParallelContext = false ;

        public boolean isGPU = false ;

        private BoolVector sharedVariables;

        private BoolVector conflictFreeSharedAdjointVariables = null ;

        /** Collection of the header Instructions of all multithread loops
         * inside the current parallel region */
        private TapList<Instruction> toParallelLoopHeaders ;

        private int nbVars ;

        private SharedVarUses[] sharedPrimalUses ;

        private SharedVarUses[] sharedAdjointUses ;

        private ParallelInfoOfBlock copyAndAddClauses(Tree parallelPragma) {
            boolean isOpenMP = ILUtils.isIdent(parallelPragma.down(1), "OMP", true) ;
            ParallelInfoOfBlock newInfo = new ParallelInfoOfBlock() ;
            newInfo.toParallelLoopHeaders = this.toParallelLoopHeaders ;
            newInfo.inParallelContext = true ;
            newInfo.nbVars = nbVars ;
            if (!inParallelContext && isOpenMP) {
                newInfo.sharedVariables = new BoolVector(nbVars) ;
                // When entering an OpenMP parallel region, every variable becomes shared by default
                newInfo.sharedVariables.setTrue();
            } else {
                // In e.g. Cuda, shared/private variables may have been declared as such outside the region
                newInfo.isGPU = true ;
                newInfo.sharedVariables = sharedVariables.copy() ;
            }
            if (isOpenMP) {
                Tree[] clauses = parallelPragma.down(2).children();
                Tree clause;
                for (int i = clauses.length - 1; i >= 0; --i) {
                    clause = clauses[i];
                    switch (clause.opCode()) {
                        case ILLang.op_privateVars:
                        case ILLang.op_firstPrivateVars:
                        case ILLang.op_lastPrivateVars:
                        case ILLang.op_reductionVars: {
                            Tree[] privateIdents = clause.down(1).children();
                            if (privateIdents != null) {
                                for (int j = privateIdents.length - 1; j >= 0; --j) {
                                    VariableDecl variableDecl =
                                        curSymbolTable.getVariableDecl(
                                                ILUtils.getIdentString(privateIdents[j]));
                                    TapIntList variableZones =
                                        ZoneInfo.listAllZones(
                                                (variableDecl == null ? null : variableDecl.zones()),
                                                true);
                                    newInfo.sharedVariables.set(variableZones, false);
                                }
                            }
                            break;
                        }
                        default:
                            break;
                    }
                }
                // Also declare as private the new tmp variables introduced by Block.splitTree() :
                TapList<NewSymbolHolder> additionalPrivates = parallelPragma.getAnnotation("futurePrivatesFwd");
                while (additionalPrivates != null) {
                    NewSymbolHolder nsh = additionalPrivates.head;
                    if (nsh.newSolvedVariableDecl != null) {
                        TapIntList variableZones =
                            ZoneInfo.listAllZones(nsh.newSolvedVariableDecl.zones(), true);
                        newInfo.sharedVariables.set(variableZones, false);
                    }
                    additionalPrivates = additionalPrivates.tail;
                }
            }
            // About collecting primal parallel uses and future diff (adjoint) uses of arrays:
            newInfo.sharedPrimalUses = new SharedVarUses[nbVars] ;
            newInfo.sharedAdjointUses = new SharedVarUses[nbVars] ;
            for (int i=0 ; i<nbVars ; ++i) {
                if (sharedVariables.get(i) && sharedPrimalUses[i]!=null) {
                    // if this zone was already shared outside:
                    newInfo.sharedPrimalUses[i] = sharedPrimalUses[i] ;
                    newInfo.sharedAdjointUses[i] = sharedAdjointUses[i] ;
                } else if (newInfo.sharedVariables.get(i)) {
                    // if this zone becomes shared just now:
                    ZoneInfo zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
                    newInfo.sharedPrimalUses[i] = new SharedVarUses(zi) ;
                    newInfo.sharedAdjointUses[i] = new SharedVarUses(zi) ;
                } else {
                    // else this zone is not shared:
                    newInfo.sharedPrimalUses[i] = null ;
                    newInfo.sharedAdjointUses[i] = null ;
                }
            }
            return newInfo ;
        }

        private ParallelInfoOfBlock copyAndAddScope(SymbolTable symbolTable) {
            ParallelInfoOfBlock newInfo = new ParallelInfoOfBlock() ;
            newInfo.toParallelLoopHeaders = this.toParallelLoopHeaders ;
            newInfo.inParallelContext = this.inParallelContext ;
            newInfo.isGPU = this.isGPU ;
            int nZ = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
            newInfo.nbVars = nZ ;
            newInfo.sharedVariables = sharedVariables.copy(nbVars, newInfo.nbVars) ;
            // New variable coming into scope are considered "private" by default,
            // except (in Cuda) when they are declared with __device__, __shared__, or __managed__
            for (int i=nbVars ; i<newInfo.nbVars ; ++i) {
                ZoneInfo zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
                while (zi!=null && zi.targetZoneOf != null) {
                    zi = zi.targetZoneOf;
                }
                if (!inParallelContext || isCudaDeclaredShared(zi)) {
                    newInfo.sharedVariables.set(i, true) ;
                }
            }
            // About collecting primal parallel uses and future diff (adjoint) uses of arrays:
            newInfo.sharedPrimalUses = new SharedVarUses[newInfo.nbVars] ;
            newInfo.sharedAdjointUses = new SharedVarUses[newInfo.nbVars] ;
            for (int i=0 ; i<newInfo.nbVars ; ++i) {
                if (i<nbVars) {
                    // if this zone existed already outside:
                    newInfo.sharedPrimalUses[i] = sharedPrimalUses[i] ;
                    newInfo.sharedAdjointUses[i] = sharedAdjointUses[i] ;
                } else {
                    // else this zone is new, therefore not shared:
                    newInfo.sharedPrimalUses[i] = null ;
                    newInfo.sharedAdjointUses[i] = null ;
                }
            }
            return newInfo ;
        }

        public String toString() {
            String primalUsesString = "" ;
            if (sharedPrimalUses==null) {
                primalUsesString = "empty!";
            } else {
                for (int i=0 ; i<sharedPrimalUses.length ; ++i) {
                    primalUsesString = primalUsesString+(sharedPrimalUses[i]==null?"_":sharedPrimalUses[i])+", " ;
                }
            }
            String adjointUsesString = "" ;
            if (sharedPrimalUses==null) {
                adjointUsesString = "empty!";
            } else {
                for (int i=0 ; i<sharedPrimalUses.length ; ++i) {
                    adjointUsesString = adjointUsesString+(sharedPrimalUses[i]==null?"_":sharedPrimalUses[i])+", " ;
                }
            }
            return "ParallelInfoOfBlock (//:"+inParallelContext+")(GPU:"+isGPU+") shared:"+sharedVariables
                +" ConflictFree:"+conflictFreeSharedAdjointVariables
                +"\n   primalUses :"+primalUsesString
                +"\n   adjointUses:"+adjointUsesString ;
        }
    }

    @Override
    protected Object initializeCGForUnit() {
        if (phase == FIND_SHARED_1) {
            parallelInfosOfUnitsBlocks.store(curUnit, new BlockStorage<>(curUnit));
            ParallelInfoOfUnit parallelInfoOfUnit = new ParallelInfoOfUnit(curUnit);
            parallelInfosOfUnits.store(curUnit, parallelInfoOfUnit) ;
            return parallelInfoOfUnit ;
        } else {
            return null ;
        }
    }

    @Override
    protected void initializeCGForRootUnit() {
        if (phase == FIND_SHARED_1) {
            ParallelInfoOfUnit parallelInfoOfRootUnit = parallelInfosOfUnits.retrieve(curUnit) ;
            int shapeLength = curUnit.paramElemsNb();
            BoolVector allShared = new BoolVector(shapeLength) ;
            allShared.setTrue() ;
            parallelInfoOfRootUnit.sharedVariables.setCopy(allShared, shapeLength) ;
        }
    }

    @Override
    protected boolean analyze() {
        boolean hasChanged = false;
        if (phase == FIND_SHARED_1) {
            TapList<ActivityPattern> activities = curUnit.activityPatterns;
            if (activities!=null) {
                if (activities.tail==null) {
                    curActivity = activities.head;
                    hasChanged = analyzeStatically(new TapList<>(curUnit.entryBlock(), null), null) ;
                } else {
                    System.out.println("NOT YET IMPLEMENTED: multithread analysis when unit has several ActivityPattern's");
                }
            }
        }
        return hasChanged ;
    }

    @Override
    protected void terminateCGForUnit() {
    }

    @Override
    protected boolean terminateFGForUnit() {
        return true ;
    }

    /**
     * Only in case we want to run on a code fragment,
     * this must be initialized with the parallel info at the
     * entry into the first Block of the fragment.
     */
    private ParallelInfoOfBlock entryFrontierParallelInfo = null;

    /**
     * The parallel Infos of each Block of the current curUnit.
     */
    private BlockStorage<ParallelInfoOfBlock> unitBlocksParallelInfos;

    @Override
    protected void initializeFGForUnit() {
        if (phase == FIND_SHARED_1) {
            unitBlocksParallelInfos = parallelInfosOfUnitsBlocks.retrieve(curUnit);
        }
    }

    @Override
    protected void initializeFGForInitBlock() {
        if (phase == FIND_SHARED_1) {
            unitBlocksParallelInfos.store(curBlock,
                (curBlock == curUnit.entryBlock()
                        ? unitParallelInfoToEntryBlockInfo(curUnit)
                        : entryFrontierParallelInfo));
        }
    }

    private ParallelInfoOfBlock unitParallelInfoToEntryBlockInfo(Unit unit) {
        ParallelInfoOfUnit unitParallelInfo = (ParallelInfoOfUnit) topDownContext(unit);
//         if (unit.language() == TapEnv.CUDA) {
//             ZoneInfo zi ;
//             // In Cuda (=>C), passed-by-value arguments become private in callee:
//             for (int i=unit.externalShape.length-1 ; i>=0 ; --i) {
//                 zi = unit.externalShape[i] ;
//                 if (zi!=null && zi.passesByValue(unit, TapEnv.C))
//                     unitParallelInfo.sharedVariables.set(i, false) ;
//             }
//         }
        nDZ = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector entrySharedVariables = unitParallelInfo.sharedVariables.copy() ;
        ParallelInfoOfBlock newInfo = new ParallelInfoOfBlock() ;
        newInfo.inParallelContext = unitParallelInfo.inParallelContext ;
        newInfo.isGPU = unitParallelInfo.isGPU ;
        newInfo.nbVars = nDZ ;
        newInfo.sharedVariables = entrySharedVariables ;
        newInfo.sharedPrimalUses = new SharedVarUses[newInfo.nbVars] ;
        newInfo.sharedAdjointUses = new SharedVarUses[newInfo.nbVars] ;
        for (int i=0 ; i<newInfo.nbVars ; ++i) {
            if (newInfo.inParallelContext && entrySharedVariables.get(i)) {
                // if this zone is shared in some calling context:
                // TODO: the info should travel from caller to callee!
                ZoneInfo zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND) ;
                newInfo.sharedPrimalUses[i] = new SharedVarUses(zi) ;
                newInfo.sharedAdjointUses[i] = new SharedVarUses(zi) ;
            } else {
                // else this zone is not shared:
                newInfo.sharedPrimalUses[i] = null ;
                newInfo.sharedAdjointUses[i] = null ;
            }
        }
        return newInfo ;
    }

    @Override
    protected void propagateValuesStaticallyThroughBlock() {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" PROPAGATE THROUGH "+curBlock);
        }
        if (phase == FIND_SHARED_1) {
            // Look for the closest enclosing scope or parallel control:
            // (With OpenMP syntax, these are always different blocks)
            Block parallelControllerBlock =
                (curBlock.parallelControls == null
                        ? null
                        : curBlock.parallelControls.head.block);
            SymbolTable controllerScope = curBlock.symbolTable;
            if (controllerScope.declarationsBlock == curBlock) {
                controllerScope = controllerScope.basisSymbolTable();
            }
            Block scopeControllerBlock =
                (controllerScope == curUnit.publicSymbolTable()
                        ? curUnit.entryBlock()
                        : controllerScope.declarationsBlock);
            Block controllerBlock =
                (parallelControllerBlock != null
                        && parallelControllerBlock.symbolTable.nestedIn(controllerScope)
                        ? parallelControllerBlock
                        : scopeControllerBlock);
            ParallelInfoOfBlock controllerParallelInfo = unitBlocksParallelInfos.retrieve(controllerBlock);
            ParallelInfoOfBlock currentParallelInfo = controllerParallelInfo;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("   parallel info before block entry:"+currentParallelInfo) ;
            }
            if (curBlock.isParallelController()) {
                // If curBlock is an OpenMP parallel pragma, introducing new OpenMP clauses:
                currentParallelInfo = currentParallelInfo.copyAndAddClauses(curBlock.headInstr().tree);
                if (curBlock.instructions.head.tree.opCode()==ILLang.op_parallelRegion) {
                    // Initiate the collection of all parallel loop headers inside:
                    currentParallelInfo.toParallelLoopHeaders = new TapList<>(null, null) ;
                } else if (curBlock.instructions.head.tree.opCode()==ILLang.op_parallelLoop) {
                    // collect the parallel loop header inside:
                    HeaderBlock loopHeader = (HeaderBlock)curBlock.flow().head.destination ;
                    currentParallelInfo.toParallelLoopHeaders.placdl(loopHeader.instructions.head) ;
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   new parallel info of parallel controller :"+currentParallelInfo) ;
                }
            } else if (curBlock == curBlock.symbolTable.declarationsBlock) {
                // If curBlock opens scope for new local variables
                currentParallelInfo = currentParallelInfo.copyAndAddScope(curBlock.symbolTable);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   new parallel info entering new scope :"+currentParallelInfo) ;
                }
            }
            unitBlocksParallelInfos.store(curBlock, currentParallelInfo);
            TapList<Instruction> instrs = curBlock.instructions;
            Tree instrTree;
            int opCode;
            while (instrs != null) {
                curInstruction = instrs.head;
                instrTree = curInstruction.tree;
                opCode = instrTree.opCode();
                Tree callTree = null ;
                if (opCode==ILLang.op_call) {
                    callTree = instrTree;
                } else if (opCode==ILLang.op_assign && instrTree.down(2).opCode()==ILLang.op_call) {
                    callTree = instrTree.down(2) ;
                }
                if (callTree!=null) {
                    addBlockParallelInfoIntoCalledUnitInfo(currentParallelInfo, callTree) ;
                }
                instrs = instrs.tail;
            }
        }
    }

    private void addBlockParallelInfoIntoCalledUnitInfo(ParallelInfoOfBlock blockParallelInfo,
                                                   Tree callTree) {
        Unit calledUnit = getCalledUnit(callTree, curSymbolTable);
        assert calledUnit != null;
        if (!calledUnit.isIntrinsic()) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("   Accumulate "+blockParallelInfo+" into "+calledUnit+" called by "+callTree) ;
            }
            CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
            Tree[] actualParams = ILUtils.getArguments(callTree).children();
            int nbArgs = actualParams.length;
            TapList argZones ;
            TapList[] argsParallelInfo = new TapList[1+nbArgs];
            argsParallelInfo[0] = null;
            for (int i=nbArgs ; i>0 ; --i) {
                Tree argTree = actualParams[i-1];
                argZones =
                        curSymbolTable.treeOfZonesOfValue(argTree, null, curInstruction, null);
                argZones = TapList.copyTree(argZones);
                includePointedElementsInTree(
                        argZones, null, null, true,
                        curSymbolTable, curInstruction, true, true);
                TapList sharedInfoTree =
                        buildInfoBoolTreeOfDeclaredZones(argZones,
                                blockParallelInfo.sharedVariables, null,
                                SymbolTableConstants.ALLKIND, curSymbolTable);
                // (Cuda special?) If the top of sharedInfoTree (not through pointer) says it is shared,
                // it means the top variable is declared as shared, and we believe it means that the
                // current destinations of pointers in argTree must be considered shared too.
                TapList inArgZonesTop = argZones ;
                // Now look for the declaration of the variable who owns this actual argument:
                while (inArgZonesTop!=null && inArgZonesTop.head instanceof TapList) inArgZonesTop = (TapList)inArgZonesTop.head ;
                TapIntList rootZones = (inArgZonesTop==null ? null : (TapIntList)inArgZonesTop.head) ;
                boolean declaredShared = false ;
                while (rootZones!=null && !declaredShared) {
                    ZoneInfo zi = curSymbolTable.declaredZoneInfo(rootZones.head, SymbolTableConstants.ALLKIND) ;
                    if (isCudaDeclaredShared(zi)) {
                        declaredShared = true ;
                    }
                    rootZones = rootZones.tail ;
                }
                if (declaredShared) {
                    // if this varable is a pointer declared as shared, then assume its destinations are shared.
                    setPointedLevelTrue(sharedInfoTree, 1) ;
                }
                argsParallelInfo[i] = sharedInfoTree ;
            }
            BoolVector calleeNewSharedVariables =
                translateCallSiteDataToCallee(blockParallelInfo.sharedVariables, argsParallelInfo,
                                              null, null, true, callTree, curInstruction, callArrow,
                                              SymbolTableConstants.ALLKIND) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("   Accumulated shared zones: "+calleeNewSharedVariables) ;
            }
            ParallelInfoOfUnit callerUnitParallelInfo = (ParallelInfoOfUnit) topDownContext(curUnit);
            ParallelInfoOfUnit calledUnitParallelInfo = (ParallelInfoOfUnit) topDownContext(calledUnit);
            if (blockParallelInfo.inParallelContext && !calledUnitParallelInfo.inParallelContext) {
                calledUnitParallelInfo.inParallelContext = true;
                calledUnitParallelInfo.isGPU = (callerUnitParallelInfo.isGPU || blockParallelInfo.isGPU) ;
                calledUnit.analysisIsOutOfDateDown = true;
            }
            if (calledUnitParallelInfo.sharedVariables.cumulOrGrows(calleeNewSharedVariables,
                    calledUnit.paramElemsNb())) {
                calledUnit.analysisIsOutOfDateDown = true;
            }
        }
    }

    private boolean isCudaDeclaredShared(ZoneInfo zi) {
        SymbolDecl zoneSymbolDecl = (zi==null ? null : zi.ownerSymbolDecl) ;
        return (zoneSymbolDecl!=null
                && (zoneSymbolDecl.hasModifier("__device__")
                    || zoneSymbolDecl.hasModifier("__shared__")
                    || zoneSymbolDecl.hasModifier("__managed__"))) ;
    }

    private void setPointedLevelTrue(TapList boolTree, int depth) {
        if (boolTree!=null) {
            if (boolTree.head instanceof TapList) {
                setPointedLevelTrue((TapList)boolTree.head, depth) ;
                setPointedLevelTrue(boolTree.tail, depth) ;
            } else { // boolTree.head is a Boolean leaf:
                if (depth==0)
                    boolTree.head = Boolean.TRUE ;
                if (depth>0)
                    setPointedLevelTrue(boolTree.tail, depth-1) ;
            }
        }
    }

//============ SECTION ON DETECTION OF CONFLICT-FREE SHARED ADJOINT VARIABLES (FormAD) ============
//=================================================================================================
    private Expr[] treeToZ3Exprs(Tree t, HashMap<String, FuncDecl> idents, Context ctx) {
        // This function handles only opcodes with lists of children, such as
        // function argument lists or multi-dimensional array indices. The
        // result is an array of expressions. For the single-expression version
        // see treeToZ3Expr(...)
        Tree[] sons;
        switch (t.opCode()) {
            case ILLang.op_expressions:
                sons = t.children();
                break;
            default:
                System.out.println("WARNING: Unexpected opcode for "+t+" in treeToZ3Exprs. This is probably going to crash.");
                return null;
        }
        Expr[] operands = new Expr[sons.length];
        for(int i=0; i<sons.length; i++) {
            operands[i] = treeToZ3Expr(sons[i], idents, ctx);
        }
        return operands;
    }

    private int sliceID = 0;

    private Expr treeToZ3Expr(Tree t, HashMap<String, FuncDecl> idents, Context ctx) {
        // This function handles only opcodes with a fixed number of children,
        // such as unary or binary operations. The result is always a (single)
        // expression. For the multi-expression version see treeToZ3Exprs(...)
        Expr operand1, operand2;
        switch (t.opCode()) {
            case ILLang.op_add:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkAdd(operand1, operand2);
            case ILLang.op_sub:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkSub(operand1, operand2);
            case ILLang.op_minus:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                return ctx.mkSub(ctx.mkInt(0), operand1); // TODO find out how to make unary minus
            case ILLang.op_mul:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkMul(operand1, operand2);
            case ILLang.op_mod:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkMod(operand1, operand2);
            case ILLang.op_div:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkDiv(operand1, operand2);
            case ILLang.op_power:
                operand1 = treeToZ3Expr(t.down(1), idents, ctx);
                operand2 = treeToZ3Expr(t.down(2), idents, ctx);
                return ctx.mkPower(operand1, operand2);
            case ILLang.op_intCst:
                return ctx.mkInt(t.intValue());
            case ILLang.op_realCst:
                // TODO this should become a realExpression. Does Z3 model these correctly?
                // Roundoff may affect the final result. Using real's in index expressions
                // is bad style anyway. For now we just print a warning and hope for the best.
                System.out.println("Warning: floating point constants in index expressions may lead to incorrect parallelism");
                return ctx.mkReal(t.stringValue());
            case ILLang.op_arrayTriplet:
                // We don't explicitly model array triplets / slices (e.g. u(1:n)). Instead,
                // we just create a new symbolic constant k and model it as u(k). Since each
                // instance will get a new symbol, no information is gained here.
                Symbol symbol = ctx.mkSymbol("slice_"+sliceID++);
                return ctx.mkFuncDecl(symbol, new Sort[0], ctx.getIntSort()).apply();
            case ILLang.op_arrayAccess:
                Expr[] indicesByDimension = treeToZ3Exprs(t.down(2), idents, ctx);
                return makeZ3FuncDecl(t.down(1), indicesByDimension, idents, ctx, "");
            case ILLang.op_ident:
                return makeZ3FuncDecl(t, null, idents, ctx, "");
            case ILLang.op_fieldAccess:
                return makeZ3FuncDecl(t.down(2), null, idents, ctx, t.down(1).stringValue()+"%");
            default:
                 System.out.println("WARNING: Unexpected opcode for "+t+" in treeToZ3Expr. This is probably going to crash.");
                return null;
        }
    }

    private Expr makeZ3FuncDecl(Tree t, Expr[] indices, HashMap<String, FuncDecl> idents, Context ctx, String prefix) {
        // Create a Z3 function declaration. We use these to represent all
        // symbols in our index expression. For scalar variables we create
        // a function with no arguments. For array variables we create a
        // function with an argument for each array dimension. This allows
        // us to reason about expressions with indirections and multiple
        // index dimensions, e.g. `u[c[i,j],k+i]` will eventually result in
        // Z3 functions i(), j(), k(), c(int, int), u(int, int)

        // We cache all functions in a hashmap so that two instances of a
        // variable can be represented with the same function (as opposed to
        // two distinct functions with the same name). One caveat (TODO):
        // If we encounter a variable first without index (e.g. because it
        // is assigned a value in its entirety), we create a function with
        // no arguments. This will crash if the same variable is then later
        // used with explicit indices.
        FuncDecl funcd;
        String funcname = prefix+t.stringValue();
        if(!idents.containsKey(t.stringValue())) {
            Symbol symbol = ctx.mkSymbol(funcname);
            Sort[] domain;
            if(indices != null) {
                domain = new Sort[indices.length];
                for(int i=0; i<indices.length; i++) {
                    domain[i] = ctx.getIntSort();
                }
            }
            else domain = new Sort[0];
            funcd = ctx.mkFuncDecl(symbol, domain, ctx.getIntSort());
            idents.put(funcname, funcd);
        }
        else funcd = idents.get(funcname);

        if(indices == null) return funcd.apply();
        else return funcd.apply(indices);
    }


    /** Main method: for the given "unit", builds all contexts and all instance numbers,
     * then collects all future (read or writes) of multithread shared (primal and adjoint)
     * variables, then, from these collections, fills each context
     * with its list of conflict-free knowledge (from the primal)
     * and its list of conflict-free questions (for the adjoint),
     * finally on each context, (hopefully) calls Z3 with knowledge and questions,
     * and consequently fills the resulting BoolVector of conflict-free conflict-free shared adjoint
     * variables, which is attached to the ParallelInfoOfBlock of the parallel region header Block. */
    public void analyzeSharedAdjointConflicts(Unit unit, ActivityPattern activity, CallGraphDifferentiator cgDifferentiator) {
        setCurUnitEtc(unit) ;
        curActivity = activity ;
        buildContexts() ;
        if (TapEnv.traceCurAnalysis()) {
            System.out.println("BUILT CONTEXTS OF UNIT: "+curUnit) ;
            dumpExistingContexts();
            TapList<Block> bl1 = curUnit.allBlocks() ;
            while (bl1!=null) {
                MTContext context = blockContexts.retrieve(bl1.head) ;
                System.out.println("Block "+bl1.head+" has context "+(context==null ? "NONE" : context.rank)) ;
                bl1 = bl1.tail ;
            }
        }
        buildInstances() ;
        if (TapEnv.traceCurAnalysis()) {
            System.out.println("BUILT INSTANCES OF UNIT: "+curUnit) ;
            TapList<Block> lb2 = curUnit.allBlocks() ;
            while (lb2!=null) {
                System.out.println("Instances through Block "+lb2.head) ;
                TapList<MTInstances> instancesList = blockInstances.retrieve(lb2.head) ;
                while (instancesList!=null) {
                    System.out.println("    "+instancesList.head) ;
                    instancesList = instancesList.tail ;
                }
                lb2 = lb2.tail ;
            }
        }
        if (TapEnv.traceCurAnalysis()) {
            System.out.println("ENUMERATE PRIMAL AND DIFF USES OF MULTITHREAD SHARED VARIABLES OF UNIT: "+curUnit) ;
        }
        collectAllSharedUses(cgDifferentiator) ;
        // Loop on parallel regions of curUnit:
        for (TapList<Block> allBlocks = curUnit.allBlocks() ;
             allBlocks!=null ;
             allBlocks = allBlocks.tail) {
            Block block = allBlocks.head ;
            if (block.isParallelController() && block.instructions.head.tree.opCode() == ILLang.op_parallelRegion) {
                setCurBlockEtc(block) ;
                nDZ = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                ParallelInfoOfBlock parallelRegionInfo =
                    parallelInfosOfUnitsBlocks.retrieve(unit).retrieve(block);
                TapList<String> parallelLoopIndicesWithInstance = null ;
                TapList<Instruction> parallelLoopHeaders = parallelRegionInfo.toParallelLoopHeaders.tail;
                while (parallelLoopHeaders!=null) {
                    Instruction parallelLoopInstr = parallelLoopHeaders.head ;
                    // make sure the loop index is private (should be the case already, but...):
                    Tree loopIndex = parallelLoopInstr.tree.down(3).down(1) ;
                    TapIntList indexZones = curSymbolTable.listOfZonesOfValue(loopIndex, null, null) ;
                    parallelRegionInfo.sharedVariables.set(indexZones, false) ;
                    Tree loopIndexCopy = ILUtils.copy(loopIndex) ;
                    MTInstances loopHeaderInstances =
                        blockInstances.retrieve(parallelLoopInstr.block).tail.head ;
                    attachInstances(loopIndexCopy, loopHeaderInstances, parallelLoopInstr, true) ;
                    parallelLoopIndicesWithInstance =
                        new TapList<>(loopIndexCopy.stringValue(), parallelLoopIndicesWithInstance) ;
                    parallelLoopHeaders = parallelLoopHeaders.tail ;
                }
                if (TapEnv.traceCurAnalysis()) {
                    System.out.println("BUILD CONFLICT-FREE KNOWLEDGE FOR PARALLEL REGION "+block) ;
                }
                buildConflicts(parallelRegionInfo, false);
                if (TapEnv.traceCurAnalysis()) {
                    System.out.println("BUILD CONFLICT-FREE QUESTIONS FOR PARALLEL REGION "+block) ;
                }
                buildConflicts(parallelRegionInfo, true);
                if (TapEnv.traceCurAnalysis()) {
                    System.out.println("PARALLEL LOOPS IN PARALLEL REGION ARE:"+parallelRegionInfo.toParallelLoopHeaders.tail) ;
                    dumpExistingContexts() ;
                }
                
                BoolVector conflictFreeZones = new BoolVector(nDZ) ;
                conflictFreeZones.setFalse();

                if (TapEnv.doOpenMPZ3()) {
                  try {
                    HashMap<String, String> cfg = new HashMap<String, String>();
                    cfg.put("model", "true");
                    Context ctx = new Context(cfg);
                    Solver z3Solver = ctx.mkSolver();
                    conflictFreeZones.setTrue();
                    int z3KnowledgeModelSize = 0 ;
                    int z3QuestionQueries = 0 ;

                    // Outer loop over contexts
                    for (TapList<MTContext> inExistingContexts = existingContexts ;
                         inExistingContexts!=null ;
                         inExistingContexts = inExistingContexts.tail) {
                        // TODO: only for contexts included in the current parallel region!!
                        MTContext context ;

                        HashMap<String, FuncDecl> variableIdentsInZ3 = new HashMap<String, FuncDecl>() ;

                        // sendToZ3 index != index_prime
                        while(parallelLoopIndicesWithInstance != null) {
                            String index = parallelLoopIndicesWithInstance.head ;
                            String jndex = index + "_prime" ;
                            Symbol indexSymbol = ctx.mkSymbol(index) ;
                            Symbol jndexSymbol = ctx.mkSymbol(jndex) ;
                            Sort[] indexDomain = { } ;
                            FuncDecl indexFunc = ctx.mkFuncDecl(indexSymbol, indexDomain, ctx.getIntSort()) ;
                            FuncDecl jndexFunc = ctx.mkFuncDecl(jndexSymbol, indexDomain, ctx.getIntSort()) ;
                            Expr loopIndicesDistinct = ctx.mkNot(ctx.mkEq(indexFunc.apply(),jndexFunc.apply())) ;
                            if (TapEnv.traceCurAnalysis()) {
                                System.out.println("adding inequality (loop counters): "+loopIndicesDistinct) ;
                            }
                            z3Solver.add(loopIndicesDistinct) ;
                            z3KnowledgeModelSize++ ;
                            variableIdentsInZ3.put(index, indexFunc) ;
                            variableIdentsInZ3.put(jndex, jndexFunc) ;
                            parallelLoopIndicesWithInstance = parallelLoopIndicesWithInstance.tail ;
                        }

                        // Inner loop over parent contexts of current context (as well as the context itself)
                        // We will add knowledge from the current context as well as its parent to the Z3 model.
                        if (TapEnv.traceCurAnalysis()) {
                            System.out.println("ASSEMBLE KNOWLEDGE ABOUT CONTEXT (WITH PARENTS):"+inExistingContexts.head) ;
                        }
                        for (TapList<MTContext> parentContexts = inExistingContexts ;
                             parentContexts != null ;
                             parentContexts = parentContexts.head.largerContexts) {
                            context = parentContexts.head ;
                            TapList<TapTriplet<Tree, Tree, ZoneInfo>> knowledge = context.knowledge ;
                            while(knowledge != null) {
                                TapTriplet<Tree, Tree, ZoneInfo> fact = knowledge.head ;
                                Expr[] lhs = treeToZ3Exprs(fact.first, variableIdentsInZ3, ctx) ;
                                Expr[] rhs = treeToZ3Exprs(fact.second, variableIdentsInZ3, ctx) ;
                                Expr distinct = ctx.mkFalse() ;
                                for(int i=0; i<lhs.length; i++) {
                                    distinct = ctx.mkOr(ctx.mkNot(ctx.mkEq(lhs[i], rhs[i])), distinct) ;
                                }
                                if (TapEnv.traceCurAnalysis()) {
                                    System.out.println("adding inequality: "+distinct) ;
                                }
                                z3Solver.add(distinct) ;
                                z3KnowledgeModelSize++ ;
                                knowledge = knowledge.tail ;
                            }
                        }

                        Status status = z3Solver.check() ;
                        if (TapEnv.traceCurAnalysis()) {
                            if (status == Status.SATISFIABLE) {
                                System.out.println("The knowledge model is SATISFIABLE") ;
                            } else if (status == Status.UNKNOWN) {
                                System.out.println("The knowledge model is UNKNOWN") ;
                            } else {
                                System.out.println("The knowledge model is UNSATISFIABLE") ;
                            }
                        }
                        // Now we set context to only the current MTContext (without iterating over parents),
                        // and we ask questions for this context.
                        context = inExistingContexts.head ;
                        TapList<TapTriplet<Tree, Tree, ZoneInfo>> questions = context.questions ;
                        while(questions != null) {
                            TapTriplet<Tree, Tree, ZoneInfo> question = questions.head ;
                            if (TapEnv.traceCurAnalysis()) {
                                System.out.println("asking question: "+question) ;
                            }
                            if (question.first==null || question.second==null) {
                                if (TapEnv.traceCurAnalysis()) {
                                    System.out.println("The question involves one total access, cannot be shared") ;
                                }
                                conflictFreeZones.set(question.third.zoneNb, false) ;
                            } else {
                                Expr[] lhs = treeToZ3Exprs(question.first, variableIdentsInZ3, ctx) ;
                                Expr[] rhs = treeToZ3Exprs(question.second, variableIdentsInZ3, ctx) ;
                                Expr equal = ctx.mkTrue() ;
                                for(int i=0; i<lhs.length; i++) {
                                    equal = ctx.mkAnd(ctx.mkEq(lhs[i], rhs[i]),equal) ;
                                }
                                if (TapEnv.traceCurAnalysis()) {
                                    System.out.println("adding equality: "+equal) ;
                                }
                                z3Solver.push() ;
                                z3Solver.add(equal) ;
                                status = z3Solver.check() ;
                                z3QuestionQueries++ ;
                                if (status == Status.SATISFIABLE) {
                                    if (TapEnv.traceCurAnalysis()) {System.out.println("The question model is SATISFIABLE") ;}
                                    conflictFreeZones.set(question.third.zoneNb, false) ;
                                } else if (status == Status.UNKNOWN) {
                                    if (TapEnv.traceCurAnalysis()) {System.out.println("The question model is UNKNOWN") ;}
                                    conflictFreeZones.set(question.third.zoneNb, false) ;
                                } else {
                                    if (TapEnv.traceCurAnalysis()) {System.out.println("The question model is UNSATISFIABLE") ;}
                                }
                                z3Solver.pop() ;
                            }
                            questions = questions.tail ;
                        }

                        parallelRegionInfo.conflictFreeSharedAdjointVariables = conflictFreeZones ;
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        System.out.println("CONFLICT-FREE SHARED ADJOINT ZONES FOR PARALLEL REGION "+block+" ARE "+conflictFreeZones) ;
                        System.out.println("Z3 STATS: knowledge model size "+z3KnowledgeModelSize+" numQuestionQueries "+z3QuestionQueries) ;
                    }
                  } catch (UnsatisfiedLinkError e) {
                      System.out.println("PROBLEM WITH Z3 INSTALLATION: "+e) ;
                      TapEnv.setDoOpenMPZ3(false) ;
                      conflictFreeZones.setFalse();
                  }
                }
            }
        }
        setCurBlockEtc(null) ;
    }

    public void collectAllSharedUses(CallGraphDifferentiator cgDifferentiator) {
        Block block ;
        int blockNbZones ;
        Tree instrTree, callResult, functionCall, sharedExpr ;
        int opCode ;
        Unit calledUnit ;
        BoolVector sharedZones ;
        ParallelInfoOfBlock blockResults ;
        SharedVarUses[] sharedPrimalUses, sharedAdjointUses ;
        TapList<MTInstances> instancesList ;
        MTInstances instances ;
        for (TapList<Block> allBlocks = curUnit.allBlocks() ;
             allBlocks!=null ;
             allBlocks = allBlocks.tail) {
            block = allBlocks.head ;
            curSymbolTable = block.symbolTable;
            nDZ = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
            sharedZones = getSharedZonesOfBlock(block);
            if (block.parallelControls != null && sharedZones!=null
                && !sharedZones.isFalse(nDZ)) {
                // if block is in a parallel region with some variables declared "shared":
                blockResults = parallelInfosOfUnitsBlocks.retrieve(curUnit).retrieve(block);
                sharedPrimalUses = blockResults.sharedPrimalUses ;
                sharedAdjointUses = blockResults.sharedAdjointUses ;
                instancesList = blockInstances.retrieve(block) ;
                for (TapList<Instruction> instrs = block.instructions ;
                     instrs!=null ;
                     instrs = instrs.tail) {
                    curInstruction = instrs.head ;
                    instrTree = curInstruction.tree ;
                    instances = instancesList.head ;
                    opCode = instrTree.opCode();
                    callResult = null;
                    functionCall = null;
                    calledUnit = null;
                    if (opCode == ILLang.op_call) {
                        functionCall = instrTree;
                    } else if (opCode == ILLang.op_assign
                               && instrTree.down(2).opCode() == ILLang.op_call) {
                        functionCall = instrTree.down(2);
                        callResult = instrTree.down(1);
                    }
                    if (functionCall != null) {
                        calledUnit = DataFlowAnalyzer.getCalledUnit(functionCall, curSymbolTable);
                    }
                    if (calledUnit != null &&
                        !calledUnit.hasPredefinedDerivatives()) {
                        // call to a non-intrinsic function or subroutine.
                        ActivityPattern calledActivity =
                                (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                                                          functionCall, curActivity, "multiActivityCalleePatterns");
                        boolean diffCallNeeded = (calledActivity != null) ;
                        // TODO: refine using "cgDifferentiator.diffCallNeeded(...)"
                        Tree[] args = ILUtils.getArguments(functionCall).children();
                        boolean[] formalArgsActivity = cgDifferentiator.getUnitFormalArgsActivityS(calledActivity);
                        // TODO: the following could be refined, cf ProcedureCallDifferentiator.
                        if (callResult!=null
                            && refExpressionIsShared(callResult, sharedZones)) {
                            collectOneSharedVarUse(callResult, instances, curInstruction, false, sharedPrimalUses);
                            if (diffCallNeeded
                                && formalArgsActivity[formalArgsActivity.length-1]
                                && ADActivityAnalyzer.isAnnotatedActive(curActivity, callResult, curSymbolTable)) {
                                collectOneSharedVarUse(callResult, instances, curInstruction, false, sharedAdjointUses);
                            }
                        }
                        for (int i = args.length-1 ; i>=0 ; --i) {
                            //TODO: adapt if R, W, or RW!
                            collectAllSharedVarUsesInExpression(args[i], sharedZones, instances,
                                                                   (diffCallNeeded && formalArgsActivity[i]),
                                                                   null, sharedPrimalUses, sharedAdjointUses) ;
                            if (ILUtils.isAVarRef(args[i], curSymbolTable)
                                && refExpressionIsShared(args[i], sharedZones)
                                /*&& arg is W */) {
                                collectOneSharedVarUse(args[i], instances, curInstruction, false, sharedPrimalUses);
                            }
                        }
                    } else if (opCode == ILLang.op_assign) {
                        // plain assignment:
                        Tree lhs = instrTree.down(1) ;
                        Tree rhs = instrTree.down(2) ;
                        ToObject<Tree> toAddedTree = new ToObject<>(null) ;
                        boolean increments = detectIncrement(lhs, rhs, toAddedTree) ;
                        if (increments) {
                            rhs = toAddedTree.obj() ;
                        }
                        boolean lhsIsShared = refExpressionIsShared(lhs, sharedZones) ;
                        //TODO: unless primal "atomic" increment! :
                        if (lhsIsShared) collectOneSharedVarUse(lhs, instances, curInstruction, false, sharedPrimalUses);
                        boolean activeResult =
                            ADActivityAnalyzer.isAnnotatedActive(curActivity, lhs, curSymbolTable) ;
                        ToBool oneAdjointInRhs = new ToBool(false) ;
                        collectAllSharedVarUsesInExpression(rhs, sharedZones, instances, activeResult,
                                                               oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                        if (activeResult && lhsIsShared) {
                            // [llh] TODO: clarify this detection of simply read adjoints:
                            if (!increments) {
                                collectOneSharedVarUse(lhs, instances, curInstruction, false, sharedAdjointUses);
                            } else if (oneAdjointInRhs.get()) {
                                collectOneSharedVarUse(lhs, instances, curInstruction, true, sharedAdjointUses);
                            }
                        }
                    }
                    instancesList = instancesList.tail ;
                }
            }
        }
        curSymbolTable = null ;
        curInstruction = null ;
    }

    /** Sweeps through primal "expression" to collect all active references to a shared variable.
     * @param expression the expression to explore
     * @param sharedZones the zones of all variables which are "shared" in the current parallel region.
     * @param activeResult true when the expression result is assigned to some active variable.
     * @param oneAdjointInRhs boolean container, set to true when at least one adjoint of the expression will be written
     * @param sharedPrimalUses the object that will accumulate the primal reads and writes found
     * @param sharedAdjointUses the object that will accumulate the adjoint reads and writes found */
    private void collectAllSharedVarUsesInExpression(Tree expression, BoolVector sharedZones, MTInstances instances,
                                                        boolean activeResult, ToBool oneAdjointInRhs,
                                                        SharedVarUses[] sharedPrimalUses, SharedVarUses[] sharedAdjointUses) {
        if (!ILUtils.isNullOrNone(expression)) {
            switch (expression.opCode()) {
                case ILLang.op_call : {
                    // Call to an instrinsic function. Sweep through arguments.
                    Tree[] args = ILUtils.getArguments(expression).children();
                    for (int i = args.length-1 ; i>=0 ; --i) {
                        collectAllSharedVarUsesInExpression(args[i], sharedZones, instances, activeResult,
                                                               oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    }
                    break ;
                }
                case ILLang.op_ident:
                case ILLang.op_fieldAccess:
                case ILLang.op_arrayAccess:
                case ILLang.op_pointerAccess:
                    if (refExpressionIsShared(expression, sharedZones)) {
                        collectOneSharedVarUse(expression, instances, curInstruction, true, sharedPrimalUses);
                        if (activeResult && ADActivityAnalyzer.isAnnotatedActive(curActivity, expression, curSymbolTable)) {
                            collectOneSharedVarUse(expression, instances, curInstruction, false, sharedAdjointUses);
                            if (oneAdjointInRhs!=null) oneAdjointInRhs.set(true) ;
                        }
                    }
                    break ;
                case ILLang.op_add :
                case ILLang.op_sub :
                case ILLang.op_mul :
                case ILLang.op_div :
                case ILLang.op_power :
                case ILLang.op_mod :
                    collectAllSharedVarUsesInExpression(expression.down(1), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    collectAllSharedVarUsesInExpression(expression.down(2), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    break ;
                case ILLang.op_minus :
                    collectAllSharedVarUsesInExpression(expression.down(1), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    break ;
                case ILLang.op_unary:
                case ILLang.op_cast:
                case ILLang.op_multiCast:
                    collectAllSharedVarUsesInExpression(expression.down(2), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    break ;
                case ILLang.op_ifExpression:
                    collectAllSharedVarUsesInExpression(expression.down(2), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    collectAllSharedVarUsesInExpression(expression.down(3), sharedZones, instances, activeResult,
                                                           oneAdjointInRhs, sharedPrimalUses, sharedAdjointUses) ;
                    break ;
                case ILLang.op_arrayConstructor:
                case ILLang.op_iterativeVariableRef:
                case ILLang.op_constructorCall:
                    // TODO
                    break ;
                default:
                    break ;
            }
        }
    }

    public void collectOneSharedVarUse(Tree expr, MTInstances instances, Instruction instr, boolean justRead, SharedVarUses[] sharedVarUses) {
        TapIntList exprZones =
            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(expr, null, instr, null),
                                  false) ;
        Tree instancedExpr = ILUtils.copy(expr) ;
        attachInstances(instancedExpr, instances, instr, false) ;
        while (exprZones!=null) {
            int rank = exprZones.head ;
            if (rank>=0 && sharedVarUses[rank]!=null) {
                sharedVarUses[rank].collectOneUse(instancedExpr, instr.block, justRead) ;
            }
            exprZones = exprZones.tail ;
        }
    }

    /** Returns true if "lhs:=rhs" is an increment of lhs.
     * If so, fills the container "toAddedTree" with the value added into lhs. */
    private boolean detectIncrement(Tree lhs, Tree rhs, ToObject<Tree> toAddedTree) {
        toAddedTree.setObj(null) ;
        if (rhs.opCode()==ILLang.op_add) {
            if (lhs.equalsTree(rhs.down(1))) {
                toAddedTree.setObj(rhs.down(2)) ;
            } else if (lhs.equalsTree(rhs.down(2))) {
                toAddedTree.setObj(rhs.down(1)) ;
            }
        } else if (rhs.opCode()==ILLang.op_sub) {
            if (lhs.equalsTree(rhs.down(1))) {
                toAddedTree.setObj(rhs.down(2)) ; // We don't care about the sign
            }
        }
        return toAddedTree.obj()!=null ;
    }

    private boolean refExpressionIsShared(Tree expr, BoolVector sharedZones) {
        TapIntList exprZones =
            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(expr, null, curInstruction, null),
                                  false) ;
        return DataFlowAnalyzer.intersectsZoneRks(sharedZones, exprZones, null) ;
    }

    public static void attachInstances(Tree expr, MTInstances instances, Instruction instr, boolean inIndex) {
        if (expr==null) return ;
        switch (expr.opCode()) {
        // Main case of an identifier, that we must extend with its instance number:
        case ILLang.op_ident:
            if (inIndex) {
                TapIntList zones = instr.block.symbolTable.listOfZonesOfValue(expr, null, instr);
                if (zones!=null) {
                    if (zones.tail!=null) {
                        System.out.println("Searching instance for Z3: identifier "+expr+" has more than one zone "+zones) ;
                    }
                    expr.setAnnotation("withoutInstance", expr.stringValue()) ;
                    expr.setValue(expr.stringValue()+"_"+instances.getInstance(zones.head)) ;
                }
            }
            break ;
        // Cases where we need to recurse in only a few children:
        case ILLang.op_fieldAccess:
            attachInstances(expr.down(1), instances, instr, inIndex) ;
            break ;
        case ILLang.op_scopeAccess:
            attachInstances(expr.down(2), instances, instr, inIndex) ;
            break ;
        case ILLang.op_call:
            attachInstances(ILUtils.getArguments(expr), instances, instr, inIndex) ;
            break ;
        // Cases where it makes no sense to go inside.
        case ILLang.op_address:
        case ILLang.op_comments:
            break ;
        case ILLang.op_arrayAccess:
        case ILLang.op_pointerAccess:
            attachInstances(expr.down(1), instances, instr, inIndex) ;
            attachInstances(expr.down(2), instances, instr, true) ;
            break ;
        // Cases where we just need to go recursively inside.
        case ILLang.op_expressions:
        default: {
            Tree[] subTrees = expr.children();
            if (subTrees!=null) {
                for (int i=subTrees.length-1 ; i>=0 ; --i) {
                    attachInstances(subTrees[i], instances, instr, inIndex) ;
                }
            }
            break ;
        }
        }
    }

    public void buildConflicts(ParallelInfoOfBlock parallelRegionInfo, boolean isQuestion) {
        SharedVarUses[] sharedUses =
            (isQuestion ? parallelRegionInfo.sharedAdjointUses : parallelRegionInfo.sharedPrimalUses) ;
        for (int i=nDZ-1 ; i>=0 ; --i) {
            if (sharedUses[i]!=null && sharedUses[i].writeUses!=null
                /*&& sharedUses[i].zoneInfo.accessIndexes!=null*/) {
                sharedUses[i].storeConflictsIntoContexts(parallelRegionInfo, isQuestion) ;
            }
        }
    }

    /** Contains the collection of plain-read uses and possibly-overwrite uses
     * of a given zone (primal or adjoint, depending on the context) */
    private final class SharedVarUses {
        ZoneInfo zoneInfo ;
        TapList<TapPair<Tree, MTContext>> readUses = null ;
        TapList<TapPair<Tree, MTContext>> writeUses = null ;

        public SharedVarUses(ZoneInfo zi) {
            zoneInfo = zi ;
        }

        public void collectOneUse(Tree newUse, Block block, boolean justRead) {
            TapList<TapPair<Tree, MTContext>> uses = (justRead ? readUses : writeUses) ;
            MTContext newContext = blockContexts.retrieve(block) ;
            boolean alreadyPresent = false ;
            // try combine new use+context with an already present use+context:
            while (uses!=null && !alreadyPresent) {
                if (uses.head.first.equalsTree(newUse)) {
                    MTContext oldContext = uses.head.second ;
                    if (newContext==oldContext || TapList.contains(newContext.largerContexts, oldContext)) {
                        alreadyPresent = true ;
                    } else if (TapList.contains(oldContext.largerContexts, newContext)) {
                        uses.head.second = newContext ;
                        alreadyPresent = true ;
                    }
                }
                uses = uses.tail ;
            }
            if (!alreadyPresent) {
                if (justRead) {
                    readUses = new TapList<>(new TapPair<>(newUse, newContext), readUses) ;
                } else {
                    writeUses = new TapList<>(new TapPair<>(newUse, newContext), writeUses) ;
                }
            }
        }

        private void attachPrimesTo(Tree expression, ParallelInfoOfBlock parallelRegionInfo) {
            if (expression==null) return ;
            switch (expression.opCode()) {
                case ILLang.op_ident: {
                    if (TapEnv.traceCurAnalysis()) {
                        System.out.println("ATTACHPRIMES TO " + expression + " SHARED "+parallelRegionInfo.sharedVariables) ;
                    }
                    String nameWithoutInstance = expression.getAnnotation("withoutInstance") ;
                    if (nameWithoutInstance==null) nameWithoutInstance = expression.stringValue() ;
                    // Note: curSymbolTable now is the SymbolTable of the parallelRegion Block.
                    TapIntList argZonesList =
                        curSymbolTable.listOfZonesOfValue(ILUtils.build(ILLang.op_ident, nameWithoutInstance),
                                                          null, null);
                    nDZ = curBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                    if (!parallelRegionInfo.sharedVariables.intersects(argZonesList)) {
                        expression.setValue(expression.stringValue()+"_prime");
                    }
                    break ;
                }
                // Special cases where some children must not be visited:
                case ILLang.op_fieldAccess:
                    attachPrimesTo(expression.down(1), parallelRegionInfo) ;
                    break ;
                case ILLang.op_scopeAccess:
                    attachPrimesTo(expression.down(2), parallelRegionInfo) ;
                    break ;
                case ILLang.op_call:
                    attachPrimesTo(expression.down(3), parallelRegionInfo) ;
                    break ;
                case ILLang.op_address:
                case ILLang.op_comments:
                    break ;
                default: {
                    Tree[] subTrees = expression.children();
                    if (subTrees != null) {
                        for (int i = subTrees.length - 1; i >= 0; i--) {
                            attachPrimesTo(subTrees[i], parallelRegionInfo) ;
                        }
                    }
                }
            }
        }

        public void storeConflictsIntoContexts(ParallelInfoOfBlock parallelRegionInfo, boolean isQuestion) {
            TapList<TapPair<Tree, MTContext>> inWriteUses = writeUses ;
            TapList<TapPair<Tree, MTContext>> inWriteUsesOther, inReadUsesOther ;
            Tree writeUse, otherUse;
            MTContext writeContext, otherContext ;
            while (inWriteUses!=null) {
                writeUse = inWriteUses.head.first ;
                writeContext = inWriteUses.head.second ;
                if (writeUse.opCode()==ILLang.op_arrayAccess) {
                    writeUse = writeUse.down(2) ;
                    storeConflictWrtOthers(parallelRegionInfo, writeUse, writeContext, inWriteUses, isQuestion) ;
                    storeConflictWrtOthers(parallelRegionInfo, writeUse, writeContext, readUses, isQuestion) ;
                } else {
                    if (isQuestion) {
                        storeConflictWrtOthers(parallelRegionInfo, null, writeContext, inWriteUses, isQuestion) ;
                        storeConflictWrtOthers(parallelRegionInfo, null, writeContext, readUses, isQuestion) ;
                    }
                }
                inWriteUses = inWriteUses.tail ;
            }
        }

        private void storeConflictWrtOthers(ParallelInfoOfBlock parallelRegionInfo, Tree oneUse, MTContext oneContext,
                                            TapList<TapPair<Tree, MTContext>> otherUses, boolean isQuestion) {
            Tree otherUse ;
            Tree otherUsePrime;
            Tree oneUsePrime = ILUtils.copy(oneUse) ;
            attachPrimesTo(oneUsePrime, parallelRegionInfo) ;
            MTContext otherContext ;
            while (otherUses!=null) {
                otherUse = otherUses.head.first ;
                otherContext = otherUses.head.second ;
                if (otherUse.opCode()==ILLang.op_arrayAccess) {
                    otherUse = otherUse.down(2) ;
                    otherUsePrime = ILUtils.copy(otherUse) ;
                    attachPrimesTo(otherUsePrime, parallelRegionInfo) ;
                    MTContext.storeConflictPair(oneUsePrime, oneContext, otherUse, otherContext, zoneInfo, isQuestion) ;
                    MTContext.storeConflictPair(oneUse, oneContext, otherUsePrime, otherContext, zoneInfo, isQuestion) ;
                } else {
                    if (isQuestion) {
                        MTContext.storeConflictPair(oneUse, oneContext, null, otherContext, zoneInfo, true) ;
                        MTContext.storeConflictPair(null, oneContext, oneUse, otherContext, zoneInfo, true) ;
                    }
                }
                otherUses = otherUses.tail ;
            }
        }

        public String toString() {
            return "r"+TapList.length(readUses)+"w"+TapList.length(writeUses);
        }

    }

    //============ SECTION ON CONTEXTS DETECTION ============

    public void buildContexts() {
        blockContexts = new BlockStorage<>(curUnit) ;
        TapList<MTContext> toExistingContexts = new TapList<>(null, null) ;
        Block block ;
        int numberOfContexts = 0 ;
        for (TapList<Block> allBlocks = curUnit.allBlocks() ;
             allBlocks!=null ;
             allBlocks = allBlocks.tail) {
            block = allBlocks.head ;
            if (block.parallelControls!=null) {
                // Context C0 is larger than C1 iff running C1 implies running C0, e.g. C1 is in a branch under C0
                // Context C0 is smaller than C1 iff C1 is larger than C0.
                // Otherwise contexts may also be uncomparable, e.g. in separate branches.
                TapList<MTContext> toLargerContexts = new TapList<>(null, null) ;
                TapList<MTContext> toSmallerContexts = new TapList<>(null, null) ;
                MTContext blockContext =
                    findSameContext(block, toExistingContexts.tail, toLargerContexts, toSmallerContexts) ;
                if (blockContext==null) {
                    blockContext = new MTContext(curUnit) ;
                    blockContext.rank = numberOfContexts ;
                    ++numberOfContexts ;
                    toExistingContexts.placdl(blockContext) ;
                    blockContext.largerContexts = toLargerContexts.tail ;
                    toSmallerContexts = toSmallerContexts.tail ;
                    while (toSmallerContexts!=null) {
                        toSmallerContexts.head.largerContexts =
                            new TapList<>(blockContext, toSmallerContexts.head.largerContexts) ;
                        toSmallerContexts = toSmallerContexts.tail ;
                    }
                }
                blockContext.addBlock(block) ;
                blockContexts.store(block, blockContext) ;
            }
        }
        existingContexts = TapList.nreverse(toExistingContexts.tail);
    }

    /** Finds into "existingContexts" a context equilalent to that of "block".
     * If no such context is found, returns null and also fills
     * "toLargerContexts" with the existing contexts that are larger than "block",
     * and "toSmallerContexts" with the existing contexts that are smaller than "block". */
    private MTContext findSameContext(Block block, TapList<MTContext> inExistingContexts,
                                      TapList<MTContext> toLargerContexts, TapList<MTContext> toSmallerContexts) {
        MTContext context, found = null ;
        while (found==null && inExistingContexts!=null) {
            context = inExistingContexts.head ;
            if (context.isPredecessorOf(block) || context.isSuccessorOf(block)) {
                if (context.isSucceededBy(block) || context.isPrecededBy(block)) {
                    found = context ;
                } else {
                    toLargerContexts.placdl(context) ;
                }
            } else if (context.isSucceededBy(block) || context.isPrecededBy(block)) {
                toSmallerContexts.placdl(context) ;
            }
            inExistingContexts = inExistingContexts.tail ;
        }
        return found ;
    }

    public void dumpExistingContexts() {
        TapList<MTContext> inExistingContexts = existingContexts ;
        while (inExistingContexts!=null) {
            System.out.println("Context "+inExistingContexts.head) ;
            inExistingContexts = inExistingContexts.tail; 
        }
    }

    private static final class MTContext {
        int rank ;
        BoolVector contents ;
        BoolVector predecessors ;
        BoolVector successors ;
        TapList<MTContext> largerContexts ;
        TapList<TapTriplet<Tree, Tree, ZoneInfo>> knowledge ;
        TapList<TapTriplet<Tree, Tree, ZoneInfo>> questions ;

        MTContext(Unit unit) {
            contents = new BoolVector(unit.nbBlocks+1) ;
            predecessors = new BoolVector(unit.nbBlocks+1) ;
            successors = new BoolVector(unit.nbBlocks+1) ;
            largerContexts = null ;
            knowledge = null ;
            questions = null ;
        }

        void addBlock(Block block) {
            contents.set(block.rank+1, true) ;
            predecessors.cumulOr(block.dominator) ;
            successors.cumulOr(block.postDominator) ; 
        }

        boolean isPredecessorOf(Block block) {
            return contents.intersects(block.dominator, block.unit().nbBlocks+1) ;
        }

        boolean isPrecededBy(Block block) {
            return predecessors.get(block.rank+1) ;
        }

        boolean isSuccessorOf(Block block) {
            return contents.intersects(block.postDominator, block.unit().nbBlocks+1) ;
        }

        boolean isSucceededBy(Block block) {
            return successors.get(block.rank+1) ;
        }

        public static void storeConflictPair(Tree access1, MTContext context1,
                                             Tree access2, MTContext context2,
                                             ZoneInfo zoneInfo, boolean isQuestion) {
            MTContext targetContext = null ;
            if (isQuestion) { // in "question" mode, question is stored at the "smallest" including context:
                if (context1==context2 || TapList.contains(context1.largerContexts, context2)) {
                    targetContext = context2 ;
                } else if (TapList.contains(context2.largerContexts, context1)) {
                    targetContext = context1 ;
                } else {
                    targetContext = commonEnclosing(context1, context2) ;
                }
                targetContext.questions =
                    new TapList<>(new TapTriplet<>(access1, access2, zoneInfo), targetContext.questions) ;
            } else { // in "knowledge" mode, knowledge is stored at the deepest level, and only if it is included in the other:
                if (context1==context2 || TapList.contains(context1.largerContexts, context2)) {
                    targetContext = context1 ;
                } else if (TapList.contains(context2.largerContexts, context1)) {
                    targetContext = context2 ;
                }
                if (targetContext!=null) {
                    targetContext.knowledge =
                        new TapList<>(new TapTriplet<>(access1, access2, zoneInfo), targetContext.knowledge) ;
                }
            }
        }

        // Heuristic: contexts do not form a tree for inclusion!
        public static MTContext commonEnclosing(MTContext context1, MTContext context2) {
            MTContext result = null ;
            TapList<MTContext> enclosing2 ;
            TapList<MTContext> enclosing1 = context1.largerContexts ;
            while (enclosing1!=null) {
                if (TapList.contains(context2.largerContexts, enclosing1.head)) {
                    if (result==null || result.rank<enclosing1.head.rank) result=enclosing1.head ;
                }
                enclosing1 = enclosing1.tail ;
            }
            return result ;
        }

        public String toString() {
            String largerStr = "" ;
            TapList<MTContext> inLargerContexts = largerContexts ;
            while (inLargerContexts!=null) {
                largerStr = largerStr+","+inLargerContexts.head.rank ;
                inLargerContexts = inLargerContexts.tail ;
            }
            String knowledgeText = "" ;
            if (knowledge!=null) {
                TapList<TapTriplet<Tree, Tree, ZoneInfo>> inKnowledge = knowledge ;
                while (inKnowledge!=null) {
                    knowledgeText = knowledgeText
                        +ILUtils.toString(inKnowledge.head.first)+" =/= "
                        +ILUtils.toString(inKnowledge.head.second) ;
                    inKnowledge = inKnowledge.tail ;
                    if (inKnowledge!=null) knowledgeText = knowledgeText+" ; " ;
                }
                knowledgeText = " Knowledge: "+knowledgeText ;
            }
            String questionsText = "" ;
            if (questions!=null) {
                TapList<TapTriplet<Tree, Tree, ZoneInfo>> inQuestions = questions ;
                while (inQuestions!=null) {
                    questionsText = questionsText+"[zone#"+inQuestions.head.third.zoneNb+"]"
                        +ILUtils.toString(inQuestions.head.first)+" =/= "
                        +ILUtils.toString(inQuestions.head.second) ;
                    inQuestions = inQuestions.tail ;
                    if (inQuestions!=null) questionsText = questionsText+" ; " ;
                }
                questionsText = " Questions: "+questionsText ;
            }
            return rank+":"+contents+" largerContexts:{"+largerStr+"}"+knowledgeText+questionsText ;
        }

    }

    //============ SECTION ON VARIABLE INSTANCES DETECTION ============

    // Utilities during "instance" detection. Erased afterwards.
    MTInstances freeInstances ;
    BlockStorage<MTInstances> blockInstancesBefore ;
    BlockStorage<MTInstances> blockInstancesAfter ;
    BlockStorage<BoolVector> loopModifiedZones ;

    public void buildInstances() {
        blockInstances = new BlockStorage<>(curUnit) ;
        blockInstancesBefore = new BlockStorage<>(curUnit) ;
        blockInstancesAfter = new BlockStorage<>(curUnit) ;
        loopModifiedZones = new BlockStorage<>(curUnit) ;
        Block block ;
        int nbZones ;
        // First sweep. Pre-detect loop-modified variables. Ignore arrows cycling back.
        for (TapList<Block> allBlocks = curUnit.allBlocks() ;
             allBlocks!=null ;
             allBlocks = allBlocks.tail) {
            block = allBlocks.head ;
            nbZones = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            if (block.isParallelController()
                && block.instructions.head.tree.opCode() == ILLang.op_parallelRegion) {
                MTInstances curInstances = new MTInstances(nbZones, 0) ;
                freeInstances = new MTInstances(nbZones, 1) ;
                blockInstancesAfter.store(block, curInstances) ;
            } else if (block.parallelControls!=null) {
                MTInstances curInstances = joinInstances(fromBlocksAbove(block.backFlow()), nbZones) ;
                blockInstancesBefore.store(block, curInstances) ;
                TapList<MTInstances> instancesList = new TapList<>(curInstances, null) ;
                blockInstances.store(block, instancesList) ;
                TapList<MTInstances> tlInstancesList = instancesList ;
                for (TapList<Instruction> instructions = block.instructions ;
                     instructions!=null ;
                     instructions = instructions.tail) {
                    curInstances = propagateInstances(curInstances, instructions.head, nbZones) ;
                    tlInstancesList = tlInstancesList.placdl(curInstances) ;
                }
                blockInstancesAfter.store(block, curInstances) ;
            }
        }
        // Second sweep. Use arrows cycling back to decide a new instance for loop-modified variables.
        for (TapList<Block> allBlocks = curUnit.allBlocks() ;
             allBlocks!=null ;
             allBlocks = allBlocks.tail) {
            block = allBlocks.head ;
            nbZones = block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            if (block.isParallelController()
                && block.instructions.head.tree.opCode() == ILLang.op_parallelRegion) {
                MTInstances curInstances = new MTInstances(nbZones, 0) ;
                freeInstances = new MTInstances(nbZones, 1) ;
                blockInstancesAfter.store(block, curInstances) ;
            } else if (block.parallelControls!=null) {
                MTInstances curInstances = joinInstances(fromBlocksAbove(block.backFlow()), nbZones) ;
                TapList<FGArrow> loopingArrows = fromBlocksBelow(block.backFlow()) ;
                if (loopingArrows!=null) {
                    MTInstances beforeOnFirstSweep = blockInstancesBefore.retrieve(block) ;
                    MTInstances loopingOnFirstSweep = joinInstances(loopingArrows, nbZones) ;
                    BoolVector loopOverwrites = differentInstances(beforeOnFirstSweep, loopingOnFirstSweep, nbZones) ;
                    loopModifiedZones.store(block, loopOverwrites) ;
                    newInstanceIfLoopOverwrites(curInstances, loopOverwrites, nbZones) ;
                }
                TapList<MTInstances> instancesList = new TapList<>(curInstances, null) ;
                blockInstances.store(block, instancesList) ;
                TapList<MTInstances> tlInstancesList = instancesList ;
                for (TapList<Instruction> instructions = block.instructions ;
                     instructions!=null ;
                     instructions = instructions.tail) {
                    curInstances = propagateInstances(curInstances, instructions.head, nbZones) ;
                    tlInstancesList = tlInstancesList.placdl(curInstances) ;
                }
                blockInstancesAfter.store(block, curInstances) ;
            }
        }
        blockInstancesBefore = null ;
        blockInstancesAfter = null ;
        loopModifiedZones = null ;
        freeInstances = null ;
    }

    private TapList<FGArrow> fromBlocksAbove(TapList<FGArrow> backFlow) {
        TapList<FGArrow> result = null ;
        while (backFlow!=null) {
            if (backFlow.head.origin.rank < backFlow.head.destination.rank)
                result = new TapList<>(backFlow.head, result) ;
            backFlow = backFlow.tail ;
        }
        return result ;
    }

    private TapList<FGArrow> fromBlocksBelow(TapList<FGArrow> backFlow) {
        TapList<FGArrow> result = null ;
        while (backFlow!=null) {
            if (backFlow.head.origin.rank >= backFlow.head.destination.rank)
                result = new TapList<>(backFlow.head, result) ;
            backFlow = backFlow.tail ;
        }
        return result ;
    }

    private MTInstances joinInstances(TapList<FGArrow> incomings, int nbZones) {
        //use incoming.topmostPop()
        MTInstances result = new MTInstances(nbZones, -1) ;
        MTInstances arriving ;
        while (incomings!=null) {
            Block origin = incomings.head.origin ;
            arriving = blockInstancesAfter.retrieve(origin) ;
            Block topmostPopped = incomings.head.topmostPop() ;
            if (topmostPopped!=origin) {
                // incoming arrow is popping out of a loop level:
                HeaderBlock exitedHeader = ((LoopBlock)topmostPopped).header() ;
                BoolVector loopOverwrites = loopModifiedZones.retrieve(exitedHeader) ;
                if (loopOverwrites!=null) {
                    for (int i=0 ; i<nbZones ; ++i) {
                        if (loopOverwrites.get(i)) {
                            // This incoming arrow exits from a loop that overwrites this zone: force a new instance!
                            result.setInstance(i, -2) ;
                        }
                    }
                }
            }
            for (int i=0 ; i<nbZones ; ++i) {
                switch (result.getInstance(i)) {
                case -2: //it is already decided to use a new instance
                    break ;
                case -1: //nothing was joined yet
                    result.setInstance(i, arriving.getInstance(i)) ;
                    break ;
                default: //some instance value was joined already:
                    if (result.getInstance(i)==arriving.getInstance(i)) {
                        // We can still keep the arriving instance number
                    } else {
                        // incompatible instances from different origins: force a new instance!
                        result.setInstance(i, -2) ;
                    }
                }
            }
            incomings = incomings.tail ;
        }
        for (int i=0 ; i<nbZones ; ++i) {
            if (result.getInstance(i)==-2 || result.getInstance(i)==-1) {
                // We have decided to build a new instance number
                int freeI = freeInstances.getInstance(i) ;
                freeInstances.setInstance(i, freeI+1) ;
                result.setInstance(i, freeI) ;
            }
        }
        return result ;
    }

    private MTInstances propagateInstances(MTInstances instancesBefore, Instruction instr, int nbZones) {
        BoolVector zonesWrittenHere = new BoolVector(nbZones) ;
        zonesWrittenHere.setFalse() ;
        collectZonesWrittenByExpression(instr.tree, NOACT, zonesWrittenHere, instr) ;
        MTInstances result = new MTInstances(nbZones, -1) ;
        for (int i=0 ; i<nbZones ; ++i) {
            if (zonesWrittenHere.get(i)) {
                // This zone is overwritten: force a new instance!
                int freeI = freeInstances.getInstance(i) ;
                freeInstances.setInstance(i, freeI+1) ;
                result.setInstance(i, freeI) ;
            } else {
                result.setInstance(i, instancesBefore.getInstance(i)) ;
            }
        }
        return result ;
    }

    private BoolVector differentInstances(MTInstances before, MTInstances looping, int nbZones) {
        BoolVector result = new BoolVector(nbZones) ;
        for (int i=0 ; i<nbZones ; ++i) {
            if (before.getInstance(i)!=looping.getInstance(i)) {
                result.set(i, true) ;
            }
        }
        return result ;
    }

    private void newInstanceIfLoopOverwrites(MTInstances instances, BoolVector loopOverwrites, int nbZones) {
        for (int i=0 ; i<nbZones ; ++i) {
            if (loopOverwrites.get(i)) {
                // From the first sweep, we know that the "loop body" modifies this zone: force a new instance!
                int freeI = freeInstances.getInstance(i) ;
                freeInstances.setInstance(i, freeI+1) ;
                instances.setInstance(i, freeI) ;
            }
        }
    }

    private static final class MTInstances {
        int[] insts ;

        public MTInstances(int nbZones, int initVal) {
            insts = new int[nbZones] ;
            for (int i=nbZones-1 ; i>=0 ; --i) {
                insts[i] = initVal ;
            }
        }

        public int getInstance(int zone) {
            return insts[zone] ;
        }

        public void setInstance(int zone, int newVal) {
            insts[zone] = newVal ;
        }

        public String toString() {
            if (insts==null) return "?" ;
            String res = "]" ;
            for (int i=insts.length-1 ; i>=0 ; --i) {
                res = i+"->"+insts[i]+" "+res ;
            }
            return "["+res ;
        }

    }

}


//     public BoolVector detectConflictFreeSharedAdjointZones(Block parallelControlBlock) {
//         Unit unit = parallelControlBlock.unit() ;
//         ParallelInfoOfBlock blockResults = parallelInfosOfUnitsBlocks.retrieve(unit).retrieve(parallelControlBlock);
//         SharedVarUses[] sharedAdjointUses = blockResults.sharedAdjointUses ;
//         int scopeLen = parallelControlBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
//         BoolVector conflictFreeZones = new BoolVector(scopeLen) ;
//         conflictFreeZones.setFalse();
//         String questionRef = unit.name()+"_line"+parallelControlBlock.instructions.head.lineNo ;
//         String questionFileName = "questionToZ3_"+questionRef+".json" ;
//         try {
//             OutputStreamWriter questionOutputStream = new FileWriter(questionFileName) ;
//             questionOutputStream.write("[");
//             boolean firstQuestion = true;
//             for (int i=scopeLen-1 ; i>=0 ; --i) {
//                 if (sharedAdjointUses[i]!=null && sharedAdjointUses[i].writeUses!=null
//                     && sharedAdjointUses[i].zoneInfo.accessIndexes!=null) {
//                     if(!firstQuestion) questionOutputStream.write(",");
//                     firstQuestion = false;
//                     sharedAdjointUses[i].emitQuestionToZ3(questionOutputStream, parallelControlBlock) ;
//                     questionOutputStream.write(System.lineSeparator());
//                 }
//             }
//             questionOutputStream.write("]");
//             questionOutputStream.flush();
//             questionOutputStream.close();
//         } catch (final IOException e) {
//             TapEnv.printlnOnTrace("IO error while writing questions to Z3: "+e);
//         }
//         Unit translationUnit = unit ;
//         while (translationUnit!=null && !translationUnit.isTranslationUnit()) {
//             translationUnit = translationUnit.upperLevelUnit() ;
//         }
//         int z3nbArgs = 3 ;
//         final String[] z3commands = new String[1+z3nbArgs] ;
//         z3commands[0] = "python" ;
//         z3commands[1] = "run_formad.py" ;
//         z3commands[2] = translationUnit.name() ;
//         z3commands[3] = questionFileName ;

//         System.out.print("Z3 COMMAND:") ;
//         for (String commandPart : z3commands) System.out.print(" "+commandPart) ;
//         System.out.println() ;

//         Runtime rt = Runtime.getRuntime();
//         try {
//             final Process z3p = rt.exec(z3commands);
//             final BufferedReader replyZ3p =
//                 new BufferedReader(new InputStreamReader(z3p.getInputStream()));
//             final BufferedReader errorZ3p =
//                 new BufferedReader(new InputStreamReader(z3p.getErrorStream()));
//             new Thread(() -> {
//                     try {
//                         String line;
//                         while ((line = errorZ3p.readLine()) != null) {
//                             TapEnv.printlnOnTrace("Z3 error: "+line) ;
//                         }
//                     } catch (IOException e) {
//                         TapEnv.printlnOnTrace("Z3 process interrupted on " + questionFileName);
//                     }
//             }).start();
//             try {
//                 int returnValue = z3p.waitFor();
//                 if (returnValue != 0) {
//                     TapEnv.printlnOnTrace("Z3 error return value " + returnValue);
//                 }
//             } catch (InterruptedException e) {
//                 Thread.currentThread().interrupt();
//             }
//             z3p.getOutputStream().close();

//             String line = replyZ3p.readLine(); //Skip initial "{"
//             line = replyZ3p.readLine();
//             while (line!=null) {
//                 if (!line.isEmpty() && !line.startsWith("}")) {
//                     //line must look like "varName" : True
//                     int posQuote1 = line.indexOf("\"") ;
//                     line = line.substring(posQuote1+1) ;
//                     int posQuote2 = line.indexOf("\"") ;
//                     String varName = line.substring(0, posQuote2) ;
//                     line = line.substring(posQuote2+1) ;
//                     int posColon = line.indexOf(":") ;
//                     line = line.substring(posColon+1) ;
//                     while (line.startsWith(" ")) line = line.substring(1) ;
//                     if (varName!=null && line.toLowerCase().startsWith("true")) {
//                         for (int i=scopeLen-1 ; i>=0 ; --i) {
//                             if (sharedAdjointUses[i]!=null && sharedAdjointUses[i].writeUses!=null
//                                 && varName.equals(ILUtils.baseName(sharedAdjointUses[i].zoneInfo.accessTree))) {
// System.out.println("Variable "+varName+" (zoneInfo:"+sharedAdjointUses[i].zoneInfo+") is found conflict-free") ;
//                                 conflictFreeZones.set(i, true) ;
// System.out.println("UNDERSTOOD THAT "+varName+" IS CONFLICT FREE ("+sharedAdjointUses[i].zoneInfo+")") ;
//                             }
//                         }
//                     }
//                 }
//                 line = replyZ3p.readLine();
//             }

//         } catch (IOException e) {
//             TapEnv.printlnOnTrace("Command not found: "+z3commands[0]) ;
//         }

// System.out.println("CONFLICTFREEADJOINTSHAREDVARS ON CONTROL BLOCK "+parallelControlBlock+" ARE :"+conflictFreeZones) ;

//         blockResults.conflictFreeSharedAdjointVariables = conflictFreeZones ;
//         return conflictFreeZones ;
//     }

//[{"name":"cr", "read":[], "write":[{"line":48,"idx":["idu","j"]},{"line":48,"idx":["iud","j"]}]}, {"name":"xt", "read":[], "write":[{"line":48,"idx":["idu","j"]},{"line":48,"idx":["iud","j"]}]}]

//         public void emitQuestionToZ3(OutputStreamWriter outputStream, Block parallelControlBlock) throws IOException {
//             // Print something like: {"u": [{"i-1+j" : 13}, {"i+1" : 42}]}
//             outputStream.write("{\"name\":\"") ;
//             outputStream.write(ILUtils.baseName(zoneInfo.accessTree)) ;
//             // pure read accesses:
//             outputStream.write("\", \"read\":") ;
//             outputStream.write("[") ;
//             emitQuestionUses(outputStream, readUses) ;
//             outputStream.write("]") ;
//             // other accesses:
//             outputStream.write(", \"write\":") ;
//             outputStream.write("[") ;
//             emitQuestionUses(outputStream, writeUses) ;
//             outputStream.write("]") ;
//             outputStream.write("}") ;
//         }

//         public void emitQuestionUses(OutputStreamWriter outputStream, TapList<TapPair<Tree, MTContext>> uses) throws IOException {
//             while (uses!=null) {
//                 Tree accessTree = uses.head.first ;
//                 outputStream.write("{") ;
//                 TapList<Tree> indexes = ILUtils.collectIndexes(accessTree, null) ;
//                 indexes = TapList.nreverse(indexes) ; // Fortran indexes are reversed compared to C
//                 outputStream.write("\"idx\":[") ;
//                 while (indexes!=null) {
//                     outputStream.write("\"");
//                     outputStream.write(ILUtils.toString(indexes.head)) ;
//                     outputStream.write("\"");
//                     indexes = indexes.tail ;
//                     if (indexes!=null) outputStream.write(", ") ;
//                 }
//                 outputStream.write("],") ;
//                 outputStream.write("\"context\": "+uses.head.second) ;
//                 outputStream.write("}") ;
//                 uses = uses.tail ;
//                 if (uses!=null) outputStream.write(", ") ;
//             }
//         }
