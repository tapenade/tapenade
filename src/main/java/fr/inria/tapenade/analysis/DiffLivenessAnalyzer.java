/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.* ;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToInt;
import fr.inria.tapenade.utils.Tree;

/**
 * Differentiated liveness analyzer. Finds out which code
 * from the original program is useful for the derivative program.
 * Assumes that activity analysis and plain dependence analysis are done.
 * <p>
 * The results of this analysis are:
 * <p>
 * -- An annotation "RequiredInDiff" placed on subtrees
 * of the original code whose result is necessary for the derivatives
 * These annotations are accessed through the *RequiredInDiff()
 * and *RequiredInDiffCkp()* methods.
 * <p>
 * -- A UnitStorage of BlockStorage of TapList of successive
 * diffLiveness infos between each successive instructions.
 * <p>
 * -- A UnitStorage of BlockStorage of TapList of successive
 * diffOverwrite infos between each successive instructions.
 * <p>
 * TODO: this diffOverwrite (i.e. outB) info is apparently not as good as
 * it could be, because this analysis comes too early. If it came after
 * TBR, then it could implement the data-flow formula from the paper:
 * <p>
 * {@code W_bar([I;D]) = (W_bar(D) \cup out(I)_{if adj-live(I)})
 * \setminus (kill(I) \cap (TBR_before(I) \cup use(I')))}
 * <p>
 * whereas here, as we don't have TBR, the "minus second part" is not done.
 * On the other hand, TBR with recomputation now needs outB. So there is a cycle!
 *
 * <pre>
 * Feature: derives from DataFlowAnalyzer : OK
 * Feature: able to analyze recursive code: OK
 * Feature: distinguishes structure elemts: no see NOTE in diffLivenessThroughExpression()
 * Feature: takes  care  of  pointer dests: OK
 * Feature: refine on loop-local variables: OK
 * Feature: std method for call arguments : OK
 * Feature: able to analyze sub-flow-graph: OK
 * </pre>
 */
public final class DiffLivenessAnalyzer extends DataFlowAnalyzer {

    /**
     * Index for the BOTTOMUP phase that computes the info for CKP calls.
     */
    private static final int BOTTOMUP_CKP = 0;
    /**
     * Index for the TOPDOWN phase that computes the info for NOCKP calls.
     */
    private static final int TOPDOWN_NOCKP = 1;
    /**
     * The AD activity analyser, which must have been run beforehand.
     */
    private final ADActivityAnalyzer adActivityAnalyzer;

    /**
     * Current liveness infos on the downstream end of each Block.
     * Used during propagation through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosDown;
    /**
     * Same as infosDown, but for "cycling" info.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosCycleDown;
    /**
     * Current liveness infos on the upstream end of each Block.
     * Used during propagation through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosUp;
    /**
     * Same as infosUp, but for "cycling" info.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosCycleUp;
    /**
     * Current liveness info being propagated through a Block by the current analysis.
     */
    private BoolVector infoTmp;
    private BoolVector infoTmpOnDiffPtr;
    /**
     * Same as "infoTmp", but for "cycling" info.
     */
    private BoolVector infoTmpCycle;
    private BoolVector infoTmpCycleOnDiffPtr;
    /**
     * Liveness info that will be accumulated where control flow merges.
     */
    private TapPair<BoolVector, BoolVector> additionalInfoPair;
    private BoolVector additionalInfo;
    private BoolVector additionalInfoOnDiffPtr;
    /**
     * Current diff-overwrite infos on the downstream end of each Block.
     * Used during propagation through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosWDown;
    /**
     * Same as infosWDown, but for "cycling" info.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosWCycleDown;
    /**
     * Current diff-overwrite infos on the upstream end of each Block.
     * Used during propagation through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosWUp;
    /**
     * Same as infosWUp, but for "cycling" info.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> infosWCycleUp;
    /**
     * Current diff-overwrite info being propagated through a Block by the current analysis.
     */
    private BoolVector infoWTmp;
    private BoolVector infoWTmpOnDiffPtr;
    /**
     * Same as "infoTmp", but for "cycling" info.
     */
    private BoolVector infoWTmpCycle;
    private BoolVector infoWTmpCycleOnDiffPtr;
    /**
     * Diff-overwrite info that will be accumulated where control flow merges.
     */
    private TapPair<BoolVector, BoolVector> additionalInfoWPair;
    private BoolVector additionalInfoW;
    private BoolVector additionalInfoWOnDiffPtr;
    /**
     * The exit DiffLiveness from the Context, used as the starting value
     * for the DiffLiveness analysis on the current Unit.
     */
    private BoolVector unitExitLiveness;
    private BoolVector unitExitLivenessOnDiffPtr;
    /**
     * The exit diff-overwrite from the Context, used as the starting value
     * for the diff-overwrite analysis on the current Unit.
     */
    private BoolVector unitExitOverwrite;
    private BoolVector unitExitOverwriteOnDiffPtr;

    /**
     * The activity information for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> curUnitActivities;
    /**
     * The reqX information for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> curUnitReqXs;
    /**
     * The avlX information for the current Unit.
     */
    private BlockStorage<TapList<BoolVector>> curUnitAvlXs;
    /**
     * Global that holds the liveness result at the end of
     * one analyzeBackward() for the current analyzed Unit.
     */
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> curUnitInfos;
    /**
     * Global that holds the diff-overwrites result at the end of
     * one analyzeBackward() for the current analyzed Unit.
     */
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> curUnitInfosW;
    /**
     * The current number of declared Pointer zones.
     */
    private int nDPZ;

    /**
     * Current phase of the analysis.
     * can be BOTTOMUP_CKP then TOPDOWN_NOCKP.
     */
    private int phase;


    /**
     * Creates a DiffLiveness analyzer, given the CallGraph "cg"
     * on which it will work.
     * We assume plain dependency analysis has been done.
     */
    private DiffLivenessAnalyzer(CallGraph cg) {
        super(cg, "Diff liveness analysis", TapEnv.traceDiffLiveness());
        this.conservativeValue = true;
        this.adActivityAnalyzer = TapEnv.adActivityAnalyzer();
    }

    /**
     * Main entry point. Builds a DiffLivenessAnalyzer, stores it and runs it.
     */
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<Unit> rootUnits) {
        TapEnv.setDiffLivenessAnalyzer(new DiffLivenessAnalyzer(callGraph));
        TapEnv.diffLivenessAnalyzer().run(rootUnits);
    }

    /**
     * Sets the annotation, at the right place, that tells that this tree
     * computes a value that is required by the differentiated computations.
     * When "checkpoint" is false, the annotation means that this tree is
     * required only in split mode (more vars required).
     * When "onDiffPtr" is true, the annotation is about the diff pointer of this tree
     * (and this tree is necessarily a pointer).
     */
    public static void setTreeRequiredInDiff(ActivityPattern activity, Tree tree, boolean checkpoint, boolean onDiffPtr) {
        Tree annotatedTree;
        if (tree.opCode() == ILLang.op_loop) {
            annotatedTree = tree.down(3);
        } else if (tree.opCode() == ILLang.op_do) {
            annotatedTree = tree;
        } else if (tree.opCode() == ILLang.op_call) {
            annotatedTree = ILUtils.getCalledName(tree);
        } else if (!tree.isAtom() && tree.down(1) != null) {
            annotatedTree = tree.down(1);
        } else {
            annotatedTree = tree;
        }
        if (checkpoint) {
            if (onDiffPtr) {
                setDiffPtrRequiredInDiff(activity, annotatedTree);
            } else {
                setRequiredInDiff(activity, annotatedTree);
            }
        } else {
            if (onDiffPtr) {
                setDiffPtrRequiredInDiffNockp(activity, annotatedTree);
            } else {
                setRequiredInDiffNockp(activity, annotatedTree);
            }
        }
    }

    /**
     * @return true if tree is annotated as required because it
     * computes a value that is required by the differentiated computations.
     */
    public static boolean isTreeRequiredInDiff(ActivityPattern activity, Tree tree, boolean checkpoint, boolean onDiffPtr) {
        Tree annotatedTree;
        if (tree.opCode() == ILLang.op_loop) {
            annotatedTree = tree.down(3);
        } else if (tree.opCode() == ILLang.op_do) {
            annotatedTree = tree;
        } else if (tree.opCode() == ILLang.op_call) {
            annotatedTree = ILUtils.getCalledName(tree);
        } else if (!tree.isAtom() && tree.down(1) != null) {
            annotatedTree = tree.down(1);
        } else {
            annotatedTree = tree;
        }
        if (checkpoint) {
            if (onDiffPtr) {
                return isDiffPtrRequiredInDiffCkp(activity, annotatedTree);
            } else {
                return isRequiredInDiffCkp(activity, annotatedTree);
            }
        } else {
            if (onDiffPtr) {
                return isDiffPtrRequiredInDiffNockp(activity, annotatedTree);
            } else {
                return isRequiredInDiffNockp(activity, annotatedTree);
            }
        }
    }

    /**
     * @return true if tree, or any subtree inside, is annotated as required because it
     * computes a value that is required by the differentiated computations.
     */
    public static boolean isTreeOrSubtreeRequiredInDiff(ActivityPattern activity, Tree tree, boolean checkpoint) {
        //return true if this tree is annotated as "primal copy required in the diff code":
        if (checkpoint) {
            if (isRequiredInDiffCkp(activity, tree)) {
                return true;
            }
        } else {
            if (isRequiredInDiffNockp(activity, tree)) {
                return true;
            }
        }
        // else search inside:
        if (tree.isAtom()) {
            return false;
        } else {
            Tree[] subTrees = tree.children();
            boolean found = false;
            int i = subTrees.length - 1;
            while (!found && i >= 0) {
                found = isTreeOrSubtreeRequiredInDiff(activity, subTrees[i], checkpoint);
                i--;
            }
            return found;
        }
    }

    /**
     * Sets the annotation that tells that this tree computes a value that is
     * required by the differentiated computations in all cases (CKP or NOPCKP).
     */
    public static void setRequiredInDiff(ActivityPattern activity, Tree tree) {
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "RequiredInDiff");
        if (annot == null) {
            annot = new ToInt(0);
            ActivityPattern.setAnnotationForActivityPattern(tree, activity, "RequiredInDiff", annot);
        }
        annot.set(2);
    }

    /**
     * Reinitializes the annotation that tells that this tree computes
     * a value that is required by the differentiated computations.
     */
    private static void setNotRequiredInDiff(ActivityPattern activity, Tree tree) {
        ToInt annot = (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "RequiredInDiff");
        if (annot != null) {
            annot.set(0);
        }
        annot = (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        if (annot != null) {
            annot.set(0);
        }
    }

    /**
     * Sets the annotation that tells that this tree computes a value that is
     * required by the differentiated computations, specifically when the
     * unit is differentiated in NOCKP mode.
     */
    private static void setRequiredInDiffNockp(ActivityPattern activity, Tree tree) {
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "RequiredInDiff");
        if (annot == null) {
            annot = new ToInt(0);
            ActivityPattern.setAnnotationForActivityPattern(tree, activity, "RequiredInDiff", annot);
        }
        if (annot.get() == 0) {
            annot.set(1);
        }
    }

    /**
     * @return true if the annotation "RequiredInDiff" tells
     * that this "tree" computes a value that is required by
     * the differentiated computation in CKP mode.
     */
    private static boolean isRequiredInDiffCkp(ActivityPattern activity, Tree tree) {
        boolean result ;
        if (tree == null) {
            result = true ;
        } else {
            ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "RequiredInDiff");
            result = (annot!=null && annot.get()==2) ;
        }
        return result ;
    }

    /**
     * @return true if the annotation "RequiredInDiff" tells
     * that this "tree" computes a value that is required by
     * the differentiated computation in NOCKP mode.
     */
    private static boolean isRequiredInDiffNockp(ActivityPattern activity, Tree tree) {
        boolean result ;
        if (tree == null) {
            result = true;
        } else {
            ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "RequiredInDiff");
            result = (annot!=null && annot.get()>=1) ;
        }
        return result ;
    }

    /**
     * Sets the annotation that tells that the diff pointer of this tree computes a value that is
     * required by the differentiated computations in all cases (CKP or NOPCKP).
     */
    public static void setDiffPtrRequiredInDiff(ActivityPattern activity, Tree tree) {
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        if (annot == null) {
            annot = new ToInt(0);
            ActivityPattern.setAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff", annot);
        }
        annot.set(2);
    }

    /**
     * Reinitializes the annotation that tells that the diff pointer of this tree computes
     * a value that is required by the differentiated computations.
     */
    private static void setDiffPtrNotRequiredInDiff(ActivityPattern activity, Tree tree) {
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        if (annot != null) {
            annot.set(0);
        }
    }

    /**
     * Sets the annotation that tells that the diff pointer of this tree computes a value that is
     * required by the differentiated computations, specifically when the
     * unit is differentiated in NOCKP mode.
     */
    private static void setDiffPtrRequiredInDiffNockp(ActivityPattern activity, Tree tree) {
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        if (annot == null) {
            annot = new ToInt(0);
            ActivityPattern.setAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff", annot);
        }
        if (annot.get() == 0) {
            annot.set(1);
        }
    }

    /**
     * @return true if the annotation "RequiredInDiff" tells
     * that the diff pointer of this "tree" computes a value that is required by
     * the differentiated computation in CKP mode.
     */
    private static boolean isDiffPtrRequiredInDiffCkp(ActivityPattern activity, Tree tree) {
        if (tree == null) {
            return true;
        }
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        return annot != null && annot.get() == 2;
    }

    /**
     * @return true if the annotation "RequiredInDiff" tells
     * that the diff pointer of this "tree" computes a value that is required by
     * the differentiated computation in NOCKP mode.
     */
    private static boolean isDiffPtrRequiredInDiffNockp(ActivityPattern activity, Tree tree) {
        if (tree == null) {
            return true;
        }
        ToInt annot =
                (ToInt) ActivityPattern.getAnnotationForActivityPattern(tree, activity, "DiffPtrRequiredInDiff");
        return annot != null && annot.get() >= 1;
    }

    /**
     * If oldTree holds a "LIVENESS" annotation, copy it to newTree.
     *
     * @param oldTree the old tree giving its liveness
     * @param newTree the tree gaining it.
     */
    public static void copyTreeRequiredInDiff(ActivityPattern activity, Tree oldTree, Tree newTree) {
        Tree annotatedOldTree;
        if (oldTree.opCode() == ILLang.op_loop) {
            annotatedOldTree = oldTree.down(3);
        } else if (oldTree.opCode() == ILLang.op_do) {
            annotatedOldTree = oldTree;
        } else if (!oldTree.isAtom() && oldTree.down(1) != null) {
            annotatedOldTree = oldTree.down(1);
        } else {
            annotatedOldTree = oldTree;
        }
        if (annotatedOldTree != null) {
            Object annot = ActivityPattern.getAnnotationForActivityPattern(annotatedOldTree, activity, "RequiredInDiff");
            Object annotOnDiffPtr = ActivityPattern.getAnnotationForActivityPattern(annotatedOldTree, activity, "DiffPtrRequiredInDiff");
            if (annot!=null || annotOnDiffPtr!=null) {
                Tree annotatedNewTree;
                if (newTree.opCode() == ILLang.op_loop) {
                    annotatedNewTree = newTree.down(3);
                } else if (newTree.opCode() == ILLang.op_do) {
                    annotatedNewTree = newTree;
                } else if (!newTree.isAtom() && newTree.down(1) != null) {
                    annotatedNewTree = newTree.down(1);
                } else {
                    annotatedNewTree = newTree;
                }
                if (annotatedNewTree != null) {
                    if (annot!=null) {
                        ActivityPattern.setAnnotationForActivityPattern(annotatedNewTree, activity, "RequiredInDiff", annot);
                    }
                    if (annotOnDiffPtr!=null) {
                        ActivityPattern.setAnnotationForActivityPattern(annotatedNewTree, activity, "DiffPtrRequiredInDiff", annotOnDiffPtr);
                    }
                }
            }
        }
    }

    /**
     * @return true when the diff of this "tree" (although active) is an empty code.
     * This happens for e.g. x=x+y with only x active, because diff is a trivial x'=x'.
     */
    private static boolean trivialIdentDiff(ActivityPattern activity, Tree tree, SymbolTable symbolTable) {
        if (tree.opCode() == ILLang.op_assign) {
            if (tree.down(2).opCode() == ILLang.op_add) {
                Tree t1 = tree.down(2).down(1);
                Tree t2 = tree.down(2).down(2);
                if (tree.down(1).equalsTree(t1)) {
                    return !ADActivityAnalyzer.isAnnotatedActive(activity, t2, symbolTable);
                } else if (tree.down(1).equalsTree(t2)) {
                    return !ADActivityAnalyzer.isAnnotatedActive(activity, t1, symbolTable);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if (tree.opCode() == ILLang.op_plusAssign) {
            return !ADActivityAnalyzer.isAnnotatedActive(activity, tree.down(2), symbolTable);
        } else {
            return false;
        }
    }

    /**
     * Runs DiffLiveness analysis bottom-up then top-down from the given Unit's "rootUnits".
     */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        String topAnalysisName = curAnalysisName;
        phase = BOTTOMUP_CKP;
        curAnalysisName = "Bottom-up (joint/CKP) diff liveness analysis";
        runBottomUpAnalysis(rootUnits);
        phase = TOPDOWN_NOCKP;
        curAnalysisName = "Top-down  (split/NOCKP or TGT) diff liveness analysis";
        runTopDownAnalysis(rootUnits);
        curAnalysisName = topAnalysisName;
    }

    public void setCurUnitActivity(ActivityPattern activity) {
        curActivity = activity;
    }

    /**
     * Forces the next diff-liveness runs to analyze only a sub-flow-graph of the current Unit.
     */
    public void setPhaseSubGraph() {
        phase = BOTTOMUP_CKP;
    }

    /**
     * In addition to super.setCurUnitEtc(), sets nDPZ.
     */
    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        nDPZ = curSymbolTable == null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
    }

    /**
     * Initializes the context of current Unit "curUnit" for this analysis.
     */
    @Override
    protected Object initializeCGForUnit() {
        // curUnit may have no info on elementary parameters, in case it is the
        //  Unit of a VarFunction, i.e. a function variable passed as argument.
        if (curUnit.hasParamElemsInfo()) {
            TapList<ActivityPattern> activities = curUnit.activityPatterns;
            while (activities != null) {
                curActivity = activities.head;
                if (phase == BOTTOMUP_CKP) {
                    curActivity.setDiffLivenessesCkp(null);
//                     curActivity.setExitDiffLivenesses(new BoolVector(zoneNb));
//                     curActivity.setExitDiffLivenessesOnDiffPtr(new BoolVector(ptrZoneNb));
                    curActivity.setDiffOverwritesCkp(null);
//                     curActivity.setExitDiffOverwrites(new BoolVector(zoneNb));
//                     curActivity.setExitDiffOverwritesOnDiffPtr(new BoolVector(ptrZoneNb));
                    curActivity.setDiffLivenessTopDownContext(new Context(curUnit));
                } else { //phase==TOPDOWN_NOCKP
//                     Context context = curActivity.diffLivenessTopDownContext ;
//                     context.addNockpExitLiveness(curActivity.exitDiffLivenesses,
//                                              curActivity.exitDiffLivenessesOnDiffPtr) ;
//                     context.addNockpExitOverwrite(curActivity.exitDiffOverwrites,
//                                               curActivity.exitDiffOverwritesOnDiffPtr) ;
                    curActivity.setDiffLivenesses(null);
                    curActivity.setDiffOverwrites(null);

                }
                activities = activities.tail;
            }
        }
        return null;
    }

    /**
     * Only in the TOPDOWN_NOCKP phase, cleans up and
     * initializes the context of the root Unit "curUnit".
     */
    @Override
    protected void initializeCGForRootUnit() {
        // TODO JH this interface does not really apply any more. Why are we passing Objects
        // around in the first place? We should remove the topDownContexts UnitStorage in
        // DataFlowAnalyzer, since only ActivityAnalyzer will use this in the long run.
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        while (activities != null) {
            curActivity = activities.head;
            activities = activities.tail;
        }
    }

    /**
     * Analyze DiffLiveness for the "curUnit".
     * Algorithm: In the 1st BOTTOMUP phase, runs one backward sweep.
     * In the 2nd TOPDOWN phase, runs up to 2 backward sweeps on curUnit's Flow Graph.
     * If the Context for "curUnit" is "ckp", then run one analysis with
     * the context's ckpExitLiveness liveness on the ExitBlock.
     * If the Context for "curUnit" is "nockp", then run one analysis with
     * the context's nockpExitLiveness liveness on the ExitBlock.
     * The context may be BOTH "ckp" and "nockp".
     * When any of the 2 runs encounters a call, then if the call is
     * reverse-mode checkpointed, then the called routine's Context
     * is set to ckp=true, else either it's a tangent diff or a
     * non checkpointed call, and the called routine's Context is set to
     * nockp=true and the current DiffLiveness vector is accumulated
     * into the called routine's Context "nockpExitLiveness".
     * @return true, only in the BOTTOMUP phase, if
     * this analysis has built a different result since last time.
     */
    @Override
    protected boolean analyze() {
        boolean hasChanged = false;
        if (curUnit.hasParamElemsInfo()) {
            int zoneNb = curUnit.publicZonesNumber(SymbolTableConstants.ALLKIND);
            int ptrZoneNb = curUnit.publicZonesNumber(SymbolTableConstants.PTRKIND);
            TapList<ActivityPattern> activities = curUnit.activityPatterns;
            while (activities != null) {
                curActivity = activities.head;
                if (curUnit.hasSource()) {
                    TapEnv.printOnTrace(15, " (ActivityPattern @" + Integer.toHexString(curActivity.hashCode()) + ":)");
                }
                curUnitActivities = curActivity.activities();
                curUnitReqXs = curActivity.reqXs();
                curUnitAvlXs = curActivity.avlXs();
                if (phase == BOTTOMUP_CKP) {
                    curUnitInfos = curActivity.diffLivenessesCkp();
                    if (curUnitInfos == null) {
                        curUnitInfos = new BlockStorage<>(curUnit);
                        curActivity.setDiffLivenessesCkp(curUnitInfos);
                    }
                    curUnitInfosW = curActivity.diffOverwritesCkp();
                    if (curUnitInfosW == null) {
                        curUnitInfosW = new BlockStorage<>(curUnit);
                        curActivity.setDiffOverwritesCkp(curUnitInfosW);
                    }
                    unitExitLiveness = new BoolVector(zoneNb);
                    unitExitLivenessOnDiffPtr = new BoolVector(ptrZoneNb);
                    unitExitOverwrite = new BoolVector(zoneNb);
                    unitExitOverwriteOnDiffPtr = new BoolVector(ptrZoneNb);
                    // Additional analysis for every checkpointed fragment of curUnit
                    // (so far, we compute this only for II-Loops...)
                    TapList<Block> insideBlocks = curUnit.allBlocks;
                    while (insideBlocks!=null) {
                        if (insideBlocks.head instanceof HeaderBlock && insideBlocks.head.instructions!=null) {
                            HeaderBlock loopHeader = (HeaderBlock)insideBlocks.head ;
                            if (loopHeader.headInstr().hasDirective(Directive.IILOOP) != null) {
                                if (TapEnv.traceCurAnalysis()) {
                                    TapEnv.printlnOnTrace(" Run local diff liveness analysis on checkpointed "+loopHeader+" OF UNIT "+curUnit.name()+" :") ;
                                }
                                TapList<FGArrow> bodyEntryArrows =
                                    new TapList<>(loopHeader.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT),
                                                  null) ;
                                TapList<FGArrow> bodyExitArrows = loopHeader.cyclingArrows() ;
                                TapList<Block> bodyBlocks = null ;
                                TapList<Block> allBlocks = curUnit.allBlocks ;
                                while (allBlocks!=null) {
                                    if (allBlocks.head!=loopHeader && allBlocks.head.enclosedIn(loopHeader.enclosingLoop())) {
                                        bodyBlocks = new TapList<>(allBlocks.head, bodyBlocks) ;
                                    }
                                    allBlocks = allBlocks.tail ;
                                }
                                bodyBlocks = TapList.nreverse(bodyBlocks) ;
                                setPhaseSubGraph();
                                resetLivenessInfo(bodyBlocks);
                                curUnitInfos.store(loopHeader,
                                    new TapList<>(null, new TapList<>(null, null))) ;
                                curUnitInfosW.store(loopHeader,
                                    new TapList<>(null, new TapList<>(null, null))) ;
                                analyzeBackward(bodyEntryArrows, bodyBlocks, bodyExitArrows) ;
                                TapPair<BoolVector, BoolVector> useCb = getFrontierLiveness(bodyEntryArrows);
                                curActivity.setFragmentDiffLivenessesCkp(loopHeader, useCb) ;
                                if (TapEnv.traceCurAnalysis()) {
                                    SymbolTable loopSymbolTable = loopHeader.symbolTable ;
                                    int loopLen =
                                        loopSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                                    int loopLenOnDiffPtr =
                                        loopSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND) ;
                                    TapEnv.printlnOnTrace(" returns local L:" + infoToStringWithDiffPtr(useCb.first, loopLen, useCb.second, loopLenOnDiffPtr));
                                }
                            }
                        }
                        insideBlocks = insideBlocks.tail ;
                    }

                    boolean hasChangedHere = analyzeBackward(null, null, null) ;
                    hasChanged = hasChanged || hasChangedHere;
                    setCurBlockEtc(curUnit.entryBlock);
                    BoolVector oldResult;
                    BoolVector oldResultOnDiffPtr;
                    BoolVector newResult;
                    BoolVector newResultOnDiffPtr;
                    TapPair<BoolVector, BoolVector> callLivenessPair = curUnitInfos.retrieve(curUnit.entryBlock).head;
                    TapPair<BoolVector, BoolVector> callOverwritePair = curUnitInfosW.retrieve(curUnit.entryBlock).head;
                    // The next 4 blocks (19 lines each) are very similar, but hard to turn into a subroutine:
                    oldResult = curActivity.entryDiffLivenessesCkp();
                    newResult = (callLivenessPair == null ? null : callLivenessPair.first);
                    oldResultOnDiffPtr = curActivity.entryDiffLivenessesCkpOnDiffPtr();
                    newResultOnDiffPtr = (callLivenessPair == null ? null : callLivenessPair.second);
                    if ((newResult != null
                         && (oldResult == null
                             || !oldResult.equals(newResult, zoneNb)))
                        ||
                        (newResultOnDiffPtr != null
                         && (oldResultOnDiffPtr == null
                             || !oldResultOnDiffPtr.equals(newResultOnDiffPtr, ptrZoneNb)))) {
                        hasChanged = true;
                        curActivity.setEntryDiffLivenessesCkp(newResult);
                        curActivity.setEntryDiffLivenessesCkpOnDiffPtr(newResultOnDiffPtr);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("  Updated input L:"+newResult.toString(zoneNb)
                                                  +" Ptr:"+newResultOnDiffPtr.toString(ptrZoneNb));
                        }
                    }
                    oldResult = curActivity.entryDiffOverwritesCkp();
                    newResult = (callOverwritePair == null ? null : callOverwritePair.first);
                    oldResultOnDiffPtr = curActivity.entryDiffOverwritesCkpOnDiffPtr();
                    newResultOnDiffPtr = (callOverwritePair == null ? null : callOverwritePair.second);
                    if ((newResult != null
                         && (oldResult == null
                             || !oldResult.equals(newResult, zoneNb)))
                        ||
                        (newResultOnDiffPtr != null
                         && (oldResultOnDiffPtr == null
                             || !oldResultOnDiffPtr.equals(newResultOnDiffPtr, ptrZoneNb)))) {
                        hasChanged = true;
                        curActivity.setEntryDiffOverwritesCkp(newResult);
                        curActivity.setEntryDiffOverwritesCkpOnDiffPtr(newResultOnDiffPtr);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("  Updated input W:"+newResult.toString(zoneNb)
                                                  +" Ptr:"+newResultOnDiffPtr.toString(ptrZoneNb));
                        }
                    }
                    setCurBlockEtc(null);
                } else { //phase==TOPDOWN_NOCKP
                    Context context = curActivity.diffLivenessTopDownContext();
                    if (context.nockpOutOfDate) {
                        context.nockpOutOfDate = false;
                        curUnitInfos = curActivity.diffLivenesses();
                        if (curUnitInfos == null) {
                            curUnitInfos = new BlockStorage<>(curUnit);
                            curActivity.setDiffLivenesses(curUnitInfos);
                        }
                        curUnitInfosW = curActivity.diffOverwrites();
                        if (curUnitInfosW == null) {
                            curUnitInfosW = new BlockStorage<>(curUnit);
                            curActivity.setDiffOverwrites(curUnitInfosW);
                        }
                        unitExitLiveness = context.nockpExitLiveness;
                        unitExitLivenessOnDiffPtr = context.nockpExitLivenessOnDiffPtr;
                        unitExitOverwrite = context.nockpExitOverwrite;
                        unitExitOverwriteOnDiffPtr = context.nockpExitOverwriteOnDiffPtr;
                        analyzeBackward(null, null, null);
                    }
                }
                activities = activities.tail;
            }
        }
        return hasChanged;
    }

    public void setCurUnitActivitiesLivenessesAndOverwrites(ActivityPattern activity,
                                                            boolean isJointMode) {
        curActivity = activity;
        if (activity != null) {
            curUnitActivities = curActivity.activities();
            curUnitReqXs = curActivity.reqXs();
            curUnitAvlXs = curActivity.avlXs();
            curUnitInfos = isJointMode ? curActivity.diffLivenessesCkp() : curActivity.diffLivenesses();
            curUnitInfosW = isJointMode ? curActivity.diffOverwritesCkp() : curActivity.diffOverwrites();
            Context context = curActivity.diffLivenessTopDownContext();
            unitExitLiveness = context.nockpExitLiveness;
            if (unitExitLiveness == null) {
                unitExitLiveness = new BoolVector(context.zoneNb);
            }
            unitExitLivenessOnDiffPtr = context.nockpExitLivenessOnDiffPtr;
            if (unitExitLivenessOnDiffPtr == null) {
                unitExitLivenessOnDiffPtr = new BoolVector(context.ptrZoneNb);
            }
            unitExitOverwrite = context.nockpExitOverwrite;
            if (unitExitOverwrite == null) {
                unitExitOverwrite = new BoolVector(context.zoneNb);
            }
            unitExitOverwriteOnDiffPtr = context.nockpExitOverwriteOnDiffPtr;
            if (unitExitOverwriteOnDiffPtr == null) {
                unitExitOverwriteOnDiffPtr = new BoolVector(context.ptrZoneNb);
            }
        } else {
            curUnitActivities = null;
            curUnitReqXs = null;
            curUnitAvlXs = null;
            curUnitInfos = null;
            curUnitInfosW = null;
            unitExitLiveness = null;
            unitExitLivenessOnDiffPtr = null;
            unitExitOverwrite = null;
            unitExitOverwriteOnDiffPtr = null;
        }
    }

    /**
     * Initialization routine before DiffLiveness analysis on "curUnit".
     * Called once at the beginning of each analyzeBackward().
     * Sets the DiffLiveness on the ExitBlock to the vector found
     * in the Context, which has been stored in "unitExitLiveness".
     */
    @Override
    protected void initializeFGForUnit() {
        infosUp = new BlockStorage<>(curUnit);
        infosDown = new BlockStorage<>(curUnit);
        infosCycleUp = new BlockStorage<>(curUnit);
        infosCycleDown = new BlockStorage<>(curUnit);
        infosWUp = new BlockStorage<>(curUnit);
        infosWDown = new BlockStorage<>(curUnit);
        infosWCycleUp = new BlockStorage<>(curUnit);
        infosWCycleDown = new BlockStorage<>(curUnit);
        curBlock = null;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" ============== DIFF LIVENESS ANALYSIS OF UNIT " + curUnit.name() + " : ==============");
            TapEnv.printlnOnTrace(" Activity context of this analysis: " + curActivity);
            curSymbolTable = curUnit.privateSymbolTable();
            if (curSymbolTable == null) {
                curSymbolTable = curUnit.publicSymbolTable();
            }
            int nDZloc = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            ZoneInfo zi;
            if (nDZloc > 0) {
                TapEnv.printOnTrace(" D:");
            }
            for (int i = 0; i < nDZloc; i++) {
                zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zi != null) {
                    TapEnv.printOnTrace(" ["+i+"]" + zi.accessTreePrint(curUnit.language()));
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace();
        }
    }

    /**
     * In addition to super.setCurBlockEtc(), sets nDPZ
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        nDPZ = (curSymbolTable==null ? 0 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
    }

    @Override
    protected void initializeFGForInitBlock() {
        if (curBlock == curUnit.exitBlock) {
            // This is the standard case, propagating on the full flow graph:
            setCurBlockEtc(curUnit.exitBlock);
            infosUp.store(curBlock, new TapPair<>(unitExitLiveness, unitExitLivenessOnDiffPtr)) ;
            infosWUp.store(curBlock, new TapPair<>(unitExitOverwrite, unitExitOverwriteOnDiffPtr)) ;
            setCurBlockEtc(null);
        } else {
            // This is for running on only a part of the flow graph:
            int lenSED = curBlock.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            int lenSEDOnDiffPtr = curBlock.symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
            BoolVector initLive = new BoolVector(lenSED);
            BoolVector initLiveOnDiffPtr = new BoolVector(lenSEDOnDiffPtr);
            BoolVector initOverwr = new BoolVector(lenSED);
            BoolVector initOverwrOnDiffPtr = new BoolVector(lenSEDOnDiffPtr);
            infosUp.store(curBlock, new TapPair<>(initLive, initLiveOnDiffPtr));
            infosWUp.store(curBlock, new TapPair<>(initOverwr, initOverwrOnDiffPtr));
            if (runSpecialCycleAnalysis(curBlock)) {
                infosCycleUp.store(curBlock, new TapPair<>(initLive.copy(), initLiveOnDiffPtr.copy()));
                infosWCycleUp.store(curBlock, new TapPair<>(initOverwr.copy(), initOverwrOnDiffPtr.copy()));
            }
            // In the present context, we are running the analysis on only a fragment of the code,
            // and the curBlock is one of the immediate successor blocks after this fragment.
            // In that case we must store these init[Live|Overwr][OnDiffPtr] also as the
            // info at the entry of curBlock, because this info might be retrieved for
            // repeated analysis on other fragments inside the current one.
            if (curUnitInfos != null) {
                TapList<TapPair<BoolVector, BoolVector>> termBlockInfos = curUnitInfos.retrieve(curBlock);
                termBlockInfos.head = new TapPair<>(initLive.copy(), initLiveOnDiffPtr.copy());
            }
            if (curUnitInfosW != null) {
                TapList<TapPair<BoolVector, BoolVector>> termBlockInfosW = curUnitInfosW.retrieve(curBlock);
                termBlockInfosW.head = new TapPair<>(initOverwr.copy(), initOverwrOnDiffPtr.copy());
            }
        }
    }

    /**
     * Empty the info(W)Tmp and info(W)TmpCycle that are used
     * to propagate diff-liveness and diff-overwrite upwards.
     */
    @Override
    protected void setEmptyCumulAndCycleValues() {
        infoTmp = null;
        infoTmpOnDiffPtr = null;
        infoTmpCycle = null;
        infoTmpCycleOnDiffPtr = null;
        infoWTmp = null;
        infoWTmpOnDiffPtr = null;
        infoWTmpCycle = null;
        infoWTmpCycleOnDiffPtr = null;
    }

    /**
     * Get the DiffLiveness info that flows back through the curArrow.
     */
    @Override
    protected boolean getValueFlowingBack() {
        Block destination = curArrow.destination;
        if (runSpecialCycleAnalysis(destination)
                && curArrow.finalCycle() == destination.enclosingLoop()) {
            additionalInfoPair = infosCycleUp.retrieve(destination);
            additionalInfoWPair = infosWCycleUp.retrieve(destination);
        } else {
            additionalInfoPair = infosUp.retrieve(destination);
            additionalInfoWPair = infosWUp.retrieve(destination);
        }
        if (additionalInfoPair == null) {
            // this curArrow does not reach the Unit exit:
            return false;
        } else {
            additionalInfo = additionalInfoPair.first;
            additionalInfoOnDiffPtr = additionalInfoPair.second;
            additionalInfoW = additionalInfoWPair.first;
            additionalInfoWOnDiffPtr = additionalInfoWPair.second;
            if (curUnitActivities != null && checkActivityThroughArrow(curArrow)) {
                // If some zones switch from active upstream to passive downstream,
                // (and in particular if they fall out of scope cf nonRegrF77:lh87)
                // then the destination Block can't be dead because there will
                // be differentiated (e.g. adjoint) code generated for it.
                // (Q: Maybe we should just look at the usefulness?)
                // Therefore the destination's controlling zones become live.
                // This solves a bug in the differentiation of the last blocks of
                // subroutine kgeom in raytrans.for in lauvernet-stics:
                // without this, kbb is not reset to 0.0 .
                additionalInfo = additionalInfo.copy();
                //(useless!) additionalInfoOnDiffPtr = additionalInfoOnDiffPtr.copy() ;
                setControllingLive(null, destination, additionalInfo, true);
            }
            if (TapEnv.traceCurAnalysis()) {
                SymbolTable destSymbolTable = destination.symbolTable;
                int destLen = destSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
                int destLenOnDiffPtr = destSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND) ;
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("   === Flowing back across " + curArrow);
                TapEnv.printlnOnTrace("    L:" + infoToStringWithDiffPtr(additionalInfo, destLen, additionalInfoOnDiffPtr, destLenOnDiffPtr, curUnit, destSymbolTable));
                TapEnv.printlnOnTrace("    W:" + infoToStringWithDiffPtr(additionalInfoW, destLen, additionalInfoWOnDiffPtr, destLenOnDiffPtr, curUnit, destSymbolTable));
            }
            return true;
        }
    }

    @Override
    protected void initializeCumulValue() {
        infoTmp = new BoolVector(nDZ);
        infoTmpOnDiffPtr = new BoolVector(nDPZ);
        infoWTmp = new BoolVector(nDZ);
        infoWTmpOnDiffPtr = new BoolVector(nDPZ);
    }

    /**
     * Accumulate the additional(W)Info, which is not cycling, into info(W)Tmp.
     */
    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        if (infoTmp == null) {
            infoTmp = new BoolVector(nDZ);
        }
        if (infoTmpOnDiffPtr == null) {
            infoTmpOnDiffPtr = new BoolVector(nDPZ);
        }
        if (infoWTmp == null) {
            infoWTmp = new BoolVector(nDZ);
        }
        if (infoWTmpOnDiffPtr == null) {
            infoWTmpOnDiffPtr = new BoolVector(nDPZ);
        }
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        infoTmp.cumulOr(additionalInfo, commonLength);
        infoWTmp.cumulOr(additionalInfoW, commonLength);
        int commonLengthOnDiffPtr = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
        infoTmpOnDiffPtr.cumulOr(additionalInfoOnDiffPtr, commonLengthOnDiffPtr);
        infoWTmpOnDiffPtr.cumulOr(additionalInfoWOnDiffPtr, commonLengthOnDiffPtr);
        //Zones that go out of scope vanish, i.e. are overwritten,
        // except if this is the Unit's exit in CKP mode.
        if (phase == TOPDOWN_NOCKP || curArrow.destination != curUnit.exitBlock) {
            for (int i = commonLength; i < nDZ; ++i) {
                infoWTmp.set(i, true);
            }
            for (int i = commonLengthOnDiffPtr; i < nDPZ; ++i) {
                infoWTmpOnDiffPtr.set(i, true);
            }
        }
    }

    /**
     * Accumulate the additional(W)Info, which is cycling, into info(W)TmpCycle.
     */
    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        if (infoTmpCycle == null) {
            infoTmpCycle = new BoolVector(nDZ);
        }
        if (infoTmpCycleOnDiffPtr == null) {
            infoTmpCycleOnDiffPtr = new BoolVector(nDPZ);
        }
        if (infoWTmpCycle == null) {
            infoWTmpCycle = new BoolVector(nDZ);
        }
        if (infoWTmpCycleOnDiffPtr == null) {
            infoWTmpCycleOnDiffPtr = new BoolVector(nDPZ);
        }
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        infoTmpCycle.cumulOr(additionalInfo, commonLength);
        infoWTmpCycle.cumulOr(additionalInfoW, commonLength);
        int commonLengthOnDiffPtr = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
        infoTmpCycleOnDiffPtr.cumulOr(additionalInfoOnDiffPtr, commonLengthOnDiffPtr);
        infoWTmpCycleOnDiffPtr.cumulOr(additionalInfoWOnDiffPtr, commonLengthOnDiffPtr);
        //Zones that go out of scope vanish, i.e. are overwritten,
        // except if this is the Unit's exit in CKP mode.
        if (phase == TOPDOWN_NOCKP || curArrow.destination != curUnit.exitBlock) {
            for (int i = commonLength; i < nDZ; ++i) {
                infoWTmpCycle.set(i, true);
            }
            for (int i = commonLengthOnDiffPtr; i < nDPZ; ++i) {
                infoWTmpCycleOnDiffPtr.set(i, true);
            }
        }
    }

    /**
     * Compare values downstream "curBlock" with the values found on previous sweep.
     */
    @Override
    protected boolean compareDownstreamValues() {
        boolean modified = compareWithStorage(infoTmp, infoTmpOnDiffPtr, infoTmpCycle, infoTmpCycleOnDiffPtr,
                nDZ, nDPZ,
                infosDown, infosCycleDown);
        boolean modifiedW = compareWithStorage(infoWTmp, infoWTmpOnDiffPtr, infoWTmpCycle, infoWTmpCycleOnDiffPtr,
                nDZ, nDPZ,
                infosWDown, infosWCycleDown);
        return modified || modifiedW;
    }

    /**
     * Compare values upstream "curBlock" with the values found on previous sweep.
     */
    @Override
    protected boolean compareUpstreamValues() {
        boolean modified = compareWithStorage(infoTmp, infoTmpOnDiffPtr, infoTmpCycle, infoTmpCycleOnDiffPtr,
                nDZ, nDPZ,
                infosUp, infosCycleUp);
        boolean modifiedW = compareWithStorage(infoWTmp, infoWTmpOnDiffPtr, infoWTmpCycle, infoWTmpCycleOnDiffPtr,
                nDZ, nDPZ,
                infosWUp, infosWCycleUp);
        return modified || modifiedW;
    }

    //[llh 2012/11/06] TODO: Adding the following method compareUpstreamChannelValuesWithInitial()
    // forces liveness on many I-O calls. It solves bugs lha85 and lha86 but gives less efficient
    // adjoint code on e.g. F77:lh05,lh06,lh67,lha69,lha70,lha71, F90:lh14, and cea-margaret,
    // lauvernet-stics, migliore-andra, opa-nemo are still correct but become bigger and slower !
    // ==> Try and correct that ?

    /**
     * Local redefinition of "compareUpstreamChannelValuesWithInitial" that looks at
     * Message-Passing channels BUT ALSO AT the %all_io_streams% zone. This is a patch
     * for the I-O problems in lauvernet-multisail with nooptim:activity, reproduced
     * in set01/lh085 and set01/lh086. When the %all_io_streams% zone becomes live, it
     * must become live right from the initBlocks (i.e. the exit!), to force liveness
     * of the IO "close". Otherwise we may get files opened twice.
     */
    @Override
    protected boolean compareUpstreamChannelValuesWithInitial(TapList<Block> initBlocks) {
        boolean initHasIncreased = false;
        TapIntList allMessagePassingChannelZones = curCallGraph.getAllMessagePassingChannelZones();
        if (!curUnit.callGraph().hasCuda) {
            // ^ Patch: don't bother about IO zone when on CUDA code (cf set12/onera07)
// ATTENTION LA LIGNE SUIVANTE EMPECHE diffLiveness->dead du call CostFunction dans seism
// Mais on dirait qu'il faut la garder pour set01/lh085 et set01/lh086 ... A revoir!
            allMessagePassingChannelZones =
                new TapIntList(curCallGraph.zoneNbOfAllIOStreams, allMessagePassingChannelZones);
        }
        TapIntList inAllChannelZones;
        Block initBlock;
        while (initBlocks != null) {
            initBlock = initBlocks.head;
            inAllChannelZones = allMessagePassingChannelZones;
            while (inAllChannelZones != null) {
                if (compareChannelZoneDataUpstream(inAllChannelZones.head, initBlock)) {
                    initHasIncreased = true;
                }
                inAllChannelZones = inAllChannelZones.tail;
            }
            initBlocks = initBlocks.tail;
        }
        return initHasIncreased;
    }

    /**
     * Compares the info for the Message-Passing zone "mpZone", upstream curBlock
     * compared with upstream "refBlock", which is one of the exit Blocks.
     * For this analysis, "mpZone" may also be the %all_io_streams% zone.
     * If curBlock's is larger, accumulates it into refBlock's and returns true.
     */
    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        boolean result = false;
        BoolVector infoTmpCopy = infoTmp;
        if (runSpecialCycleAnalysis(curBlock)) {
            infoTmpCopy = infoTmp.copy();
            infoTmpCopy.cumulOr(infoTmpCycle);
        }
        BoolVector refInfo = infosUp.retrieve(refBlock).first;
        if (infoTmpCopy.get(mpZone) && !refInfo.get(mpZone)) {
            refInfo.set(mpZone, true);
            result = true;
        }
        return result;
    }

    /**
     * Propagate the DiffLiveness info through "CurBlock".
     */
    @Override
    protected boolean propagateValuesBackwardThroughBlock() {
        return propagateAndStoreDiffLivenessThroughBlock(curBlock, null, null);
    }

    /**
     * When "recordList" is non-null, fill it with the intermediate infos.
     */
    private boolean propagateAndStoreDiffLivenessThroughBlock(Block block,
                                                              TapList<TapPair<BoolVector, BoolVector>> recordList,
                                                              TapList<TapPair<BoolVector, BoolVector>> recordListW) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going up through Block " + block + " ===");
        }
        int blockTestZone = curUnit.testZoneOfControllerBlock(block);
        boolean liveTest = (blockTestZone>=0
                            && (infoTmp.get(blockTestZone)
                                || (infoTmpCycle != null && infoTmpCycle.get(blockTestZone))));
        TapList<Instruction> instructions = block.instructions;
        TapList<BoolVector> blockReqXs = curUnitReqXs == null ? null : curUnitReqXs.retrieve(block);
        TapList<BoolVector> blockAvlXs = curUnitAvlXs == null ? null : curUnitAvlXs.retrieve(block);
        blockReqXs = TapList.reverse(blockReqXs);
        blockAvlXs = TapList.reverse(blockAvlXs);
        if (blockAvlXs!=null) {blockAvlXs = blockAvlXs.tail ;}
        BoolVector afterReqX, beforeAvlX;
        setUniqueAccessZones(block);
        Instruction initialDo = null;
        boolean runSpecialCycleAnalysis = runSpecialCycleAnalysis(block);
        Tree loopDoTree = null;
        ToBool arrives = new ToBool(true);
        ToBool arrivesC = new ToBool(true);
        boolean isLastInstr = true;
        if (runSpecialCycleAnalysis) {
            initialDo = instructions.head;
            if (initialDo != null) {
                loopDoTree = initialDo.tree;
            }
            instructions = instructions.tail;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    L:" + infoToStringWithDiffPtrWithCycle(infoTmp, nDZ, infoTmpOnDiffPtr, nDPZ, infoTmpCycle, infoTmpCycleOnDiffPtr));
            TapEnv.printlnOnTrace("    W:" + infoToStringWithDiffPtrWithCycle(infoWTmp, nDZ, infoWTmpOnDiffPtr, nDPZ, infoWTmpCycle, infoWTmpCycleOnDiffPtr));
        }
        if (recordList != null) {
            recordInfosInto(infoTmp, infoTmpCycle, infoTmpOnDiffPtr, infoTmpCycleOnDiffPtr, recordList);
        }
        if (recordListW != null) {
            recordInfosInto(infoWTmp, infoWTmpCycle, infoWTmpOnDiffPtr, infoWTmpCycleOnDiffPtr, recordListW);
        }
        instructions = TapList.reverse(instructions);
        // First make "infoTmp" (and possibly also "infoTmpCycle" if we
        // are in the runSpecialCycleAnalysis) sweep upstream across the
        // instructions, except for the initial do tree of a clean do loop.
        while (instructions != null) {
            curInstruction = instructions.head;
            afterReqX = (blockReqXs==null ? null : blockReqXs.head) ;
            beforeAvlX = (blockAvlXs==null ? null : blockAvlXs.head) ;
            // needVal == true if the block is a control block, and the instruction
            // considered is the control instruction i.e the last instruction of
            // the block. In other words needVal = isLastInstr && livetest
            diffLivenessThroughExpression(curInstruction.tree, isLastInstr && liveTest, false, false,
                    infoTmp, infoTmpOnDiffPtr, infoWTmp, infoWTmpOnDiffPtr,
                    beforeAvlX, afterReqX, arrives);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                diffLivenessThroughExpression(curInstruction.tree, isLastInstr && liveTest, false, false,
                        infoTmpCycle, infoTmpCycleOnDiffPtr, infoWTmpCycle, infoWTmpCycleOnDiffPtr,
                        beforeAvlX, afterReqX, arrivesC);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  Now gone upstream across instruction:" + ILUtils.toString(curInstruction.tree));
                TapEnv.printlnOnTrace("    L:" + infoToStringWithDiffPtrWithCycle(infoTmp, nDZ, infoTmpOnDiffPtr, nDPZ, infoTmpCycle, infoTmpCycleOnDiffPtr));
                TapEnv.printlnOnTrace("    W:" + infoToStringWithDiffPtrWithCycle(infoWTmp, nDZ, infoWTmpOnDiffPtr, nDPZ, infoWTmpCycle, infoWTmpCycleOnDiffPtr));
            }
            if (recordList != null) {
                recordInfosInto(infoTmp, infoTmpCycle, infoTmpOnDiffPtr, infoTmpCycleOnDiffPtr, recordList);
            }
            if (recordListW != null) {
                recordInfosInto(infoWTmp, infoWTmpCycle, infoWTmpOnDiffPtr, infoWTmpCycleOnDiffPtr, recordListW);
            }
            isLastInstr = false;
            instructions = instructions.tail;
            if (blockReqXs != null) {
                blockReqXs = blockReqXs.tail;
            }
            if (blockAvlXs != null) {
                blockAvlXs = blockAvlXs.tail;
            }
        }
        // If the 1st instruction of the Block is a normal
        // clean do loop, do not consider the "do-from" tree,
        // which is read only at loop entry.
        afterReqX = (blockReqXs==null ? null : blockReqXs.head) ;
        beforeAvlX = (blockAvlXs==null ? null : blockAvlXs.head) ;
        if (initialDo != null) {
            curInstruction = initialDo;
            diffLivenessThroughExpression(loopDoTree, isLastInstr && liveTest, false, false,
                    infoTmp, infoTmpOnDiffPtr, infoWTmp, infoWTmpOnDiffPtr,
                    beforeAvlX, afterReqX, arrives);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                diffLivenessThroughExpression(loopDoTree, isLastInstr && liveTest, false, false,
                        infoTmpCycle, infoTmpCycleOnDiffPtr, infoWTmpCycle, infoWTmpCycleOnDiffPtr,
                        beforeAvlX, afterReqX, arrivesC);
            }
        }
        // Second compute the "infoTmpCycle" that will be propagated upstream
        // through the loop's cycling arrows. It is the upcoming "infoTmp" PLUS
        // only the upcoming "infoTmpCycle" which are not localized in the loop.
        BoolVector infoTmpCycleUp = null;
        BoolVector infoTmpCycleUpOnDiffPtr = null;
        BoolVector infoWTmpCycleUp = null;
        BoolVector infoWTmpCycleUpOnDiffPtr = null;
        if (runSpecialCycleAnalysis) {
            TapIntList localizedZones =
                    curSymbolTable.getDeclaredKindZones(((HeaderBlock) block).localizedZones, SymbolTableConstants.ALLKIND, true);
            TapIntList localizedZonesOnDiffPtr =
                    curSymbolTable.getDeclaredKindZones(((HeaderBlock) block).localizedZones, SymbolTableConstants.PTRKIND, true);
            if (infoTmpCycle != null) {
                infoTmpCycleUp = infoTmpCycle.copy();
                infoTmpCycleUp.set(localizedZones, false);
            } else {
                infoTmpCycleUp = new BoolVector(nDZ);
            }
            if (infoTmpCycleOnDiffPtr != null) {
                infoTmpCycleUpOnDiffPtr = infoTmpCycleOnDiffPtr.copy();
                infoTmpCycleUpOnDiffPtr.set(localizedZonesOnDiffPtr, false);
            } else {
                infoTmpCycleUpOnDiffPtr = new BoolVector(nDPZ);
            }
            if (infoWTmpCycle != null) {
                infoWTmpCycleUp = infoWTmpCycle.copy();
                infoWTmpCycleUp.set(localizedZones, false);
            } else {
                infoWTmpCycleUp = new BoolVector(nDZ);
            }
            if (infoWTmpCycleOnDiffPtr != null) {
                infoWTmpCycleUpOnDiffPtr = infoWTmpCycleOnDiffPtr.copy();
                infoWTmpCycleUpOnDiffPtr.set(localizedZonesOnDiffPtr, false);
            } else {
                infoWTmpCycleUpOnDiffPtr = new BoolVector(nDPZ);
            }
            if (infoTmp != null) {
                infoTmpCycleUp.cumulOr(infoTmp);
            }
            if (infoTmpOnDiffPtr != null) {
                infoTmpCycleUpOnDiffPtr.cumulOr(infoTmpOnDiffPtr);
            }
            if (infoWTmp != null) {
                infoWTmpCycleUp.cumulOr(infoWTmp);
            }
            if (infoWTmpOnDiffPtr != null) {
                infoWTmpCycleUpOnDiffPtr.cumulOr(infoWTmpOnDiffPtr);
            }
            // Third compute the "infoTmp" that will be propagated upstream
            // through the loop's entering arrows. It is the upcoming "infoTmpCycle" PLUS the part of
            // the upcoming "infoTmp" defined by "directEntryExitMask()" (i.e. either the loop may run 0 times,
            // or the zone has been treated as localized by the loop body and has been fully accessed by the
            // loop and will not be "localized" at the above level).
            if (infoTmp != null) {
                if (infoTmpCycle == null) {
                    infoTmpCycle = new BoolVector(nDZ);
                }
                infoTmpCycle.cumulOr(
                        infoTmp.and(directEntryExitMask((HeaderBlock) block, nDZ, SymbolTableConstants.ALLKIND)));
            }
            infoTmp = infoTmpCycle;
            if (infoTmpOnDiffPtr != null) {
                if (infoTmpCycleOnDiffPtr == null) {
                    infoTmpCycleOnDiffPtr = new BoolVector(nDPZ);
                }
                infoTmpCycleOnDiffPtr.cumulOr(
                        infoTmpOnDiffPtr.and(directEntryExitMask((HeaderBlock) block, nDPZ, SymbolTableConstants.PTRKIND)));
            }
            infoTmpOnDiffPtr = infoTmpCycleOnDiffPtr;
            if (infoWTmp != null) {
                if (infoWTmpCycle == null) {
                    infoWTmpCycle = new BoolVector(nDZ);
                }
                infoWTmpCycle.cumulOr(
                        infoWTmp.and(directEntryExitMask((HeaderBlock) block, nDZ, SymbolTableConstants.ALLKIND)));
            }
            infoWTmp = infoWTmpCycle;
            if (infoWTmpOnDiffPtr != null) {
                if (infoWTmpCycleOnDiffPtr == null) {
                    infoWTmpCycleOnDiffPtr = new BoolVector(nDPZ);
                }
                infoWTmpCycleOnDiffPtr.cumulOr(
                        infoWTmpOnDiffPtr.and(directEntryExitMask((HeaderBlock) block, nDPZ, SymbolTableConstants.PTRKIND)));
            }
            infoWTmp = infoWTmpCycle;
        }
        infoTmpCycle = infoTmpCycleUp;
        infoTmpCycleOnDiffPtr = infoTmpCycleUpOnDiffPtr;
        infoWTmpCycle = infoWTmpCycleUp;
        infoWTmpCycleOnDiffPtr = infoWTmpCycleUpOnDiffPtr;
        // Fourth: For a do-loop, "infoTmp" is the info that goes backwards
        // through the entry into the do-loop. Add into this "infoTmp" the effect
        // of reading the do-from followed by overwriting the do-index.
        if (initialDo != null) {
            ToBool total = new ToBool(false);
            TapIntList indexZones =
                    curSymbolTable.listOfZonesOfValue(loopDoTree.down(3).down(1), total, initialDo);
            if (total.get()) {
                infoTmp.set(indexZones, false);
                if (infoTmpCycle != null) {
                    infoTmpCycle.set(indexZones, false);
                }
            }
            infoWTmp.set(indexZones, true);
            diffLivenessThroughExpression(loopDoTree.down(3).down(2), isLastInstr && liveTest, false, false,
                    infoTmp, infoTmpOnDiffPtr, infoWTmp, infoWTmpOnDiffPtr,
                    beforeAvlX, afterReqX, arrives);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  Now gone upstream across initial DO:" + ILUtils.toString(loopDoTree));
                TapEnv.printlnOnTrace("    L:" + infoToStringWithDiffPtrWithCycle(infoTmp, nDZ, infoTmpOnDiffPtr, nDPZ, infoTmpCycle, infoTmpCycleOnDiffPtr));
                TapEnv.printlnOnTrace("    W:" + infoToStringWithDiffPtrWithCycle(infoWTmp, nDZ, infoWTmpOnDiffPtr, nDPZ, infoWTmpCycle, infoWTmpCycleOnDiffPtr));
            }
            if (recordList != null) {
                recordInfosInto(infoTmp, infoTmpCycle, infoTmpOnDiffPtr, infoTmpCycleOnDiffPtr, recordList);
            }
            if (recordListW != null) {
                recordInfosInto(infoWTmp, infoWTmpCycle, infoWTmpOnDiffPtr, infoWTmpCycleOnDiffPtr, recordListW);
            }
        }
        if (!TapEnv.removeDeadControl() && curBlock.isControl() && curBlock.lastInstr() != null) {
            setTreeRequiredInDiff(curActivity, curBlock.lastInstr().tree, phase == BOTTOMUP_CKP, false);
        }
        curInstruction = null;
        return true;
    }

    /**
     * Stores the current info into the recording structure, which is a TapList&lt;TapPair&lt;BoolVector, BoolVector&gt;&gt;.
     */
    private void recordInfosInto(BoolVector info, BoolVector infoCycle,
                                 BoolVector infoOnDiffPtr, BoolVector infoCycleOnDiffPtr,
                                 TapList<TapPair<BoolVector, BoolVector>> record) {
        BoolVector recordInfo = info.copy();
        if (infoCycle != null) {
            recordInfo.cumulOr(infoCycle);
        }
        BoolVector recordInfoOnDiffPtr = infoOnDiffPtr.copy();
        if (infoCycleOnDiffPtr != null) {
            recordInfoOnDiffPtr.cumulOr(infoCycleOnDiffPtr);
        }
        record.tail = new TapList<>(new TapPair<>(recordInfo, recordInfoOnDiffPtr), record.tail);
    }

    /**
     * Build and store the TapList of the propagated
     * DiffLiveness info between successive Instruction's of this curBlock.
     * This TapList has N+1 infos if curBlock has N Instruction's,
     * and it is stored into the global "curUnitInfos".
     */
    @Override
    protected void terminateFGForBlock() {
        TapList<TapPair<BoolVector, BoolVector>> toInfos = new TapList<>(null, null);
        TapList<TapPair<BoolVector, BoolVector>> toInfosW = new TapList<>(null, null);
        TapPair<BoolVector, BoolVector> infoDown = infosDown.retrieve(curBlock);
        infoTmp = (infoDown == null ? null : infoDown.first);
        infoTmpOnDiffPtr = (infoDown == null ? null : infoDown.second);
        TapPair<BoolVector, BoolVector> infoWDown = infosWDown.retrieve(curBlock);
        infoWTmp = (infoWDown == null ? null : infoWDown.first);
        infoWTmpOnDiffPtr = (infoWDown == null ? null : infoWDown.second);
        if (infoTmp == null) {
            infoTmp = new BoolVector(nDZ);
        }
        if (infoTmpOnDiffPtr == null) {
            infoTmpOnDiffPtr = new BoolVector(nDPZ);
        }
        if (infoWTmp == null) {
            infoWTmp = new BoolVector(nDZ);
        }
        if (infoWTmpOnDiffPtr == null) {
            infoWTmpOnDiffPtr = new BoolVector(nDPZ);
        }
        if (runSpecialCycleAnalysis(curBlock)) {
            infoTmpCycle = infosCycleDown.retrieve(curBlock).first;
            infoTmpCycleOnDiffPtr = infosCycleDown.retrieve(curBlock).second;
            infoWTmpCycle = infosWCycleDown.retrieve(curBlock).first;
            infoWTmpCycleOnDiffPtr = infosWCycleDown.retrieve(curBlock).second;
        } else {
            infoTmpCycle = null;
            infoTmpCycleOnDiffPtr = null;
            infoWTmpCycle = null;
            infoWTmpCycleOnDiffPtr = null;
        }
        propagateAndStoreDiffLivenessThroughBlock(curBlock, toInfos, toInfosW);
        curUnitInfos.store(curBlock, toInfos.tail);
        curUnitInfosW.store(curBlock, toInfosW.tail);
    }

    @Override
    protected void terminateFGForTermBlock() {
        boolean hasCyclingInfo = runSpecialCycleAnalysis(curBlock);
        TapList<TapPair<BoolVector, BoolVector>> termBlockInfos = curUnitInfos.retrieve(curBlock);
        if (termBlockInfos == null) {
            termBlockInfos = new TapList<>(null, new TapList<>(null, null));
            curUnitInfos.store(curBlock, termBlockInfos);
        }
        TapList<TapPair<BoolVector, BoolVector>> toLastInfo = TapList.toLast(termBlockInfos);
        if (toLastInfo != null) {
            TapPair<BoolVector, BoolVector> exitInfoPair = infosDown.retrieve(curBlock);
            BoolVector termInfo = (exitInfoPair==null ? null : exitInfoPair.first) ;
            BoolVector termInfoOnDiffPtr = (exitInfoPair==null ? null : exitInfoPair.second) ;
            if (termInfo == null) {
                termInfo = new BoolVector(nDZ);
            } else {
                termInfo = termInfo.copy();
            }
            if (termInfoOnDiffPtr == null) {
                termInfoOnDiffPtr = new BoolVector(nDPZ);
            } else {
                termInfoOnDiffPtr = termInfoOnDiffPtr.copy();
            }
            if (hasCyclingInfo) {
                exitInfoPair = infosCycleDown.retrieve(curBlock);
                BoolVector termCycleInfo = exitInfoPair == null ? null : exitInfoPair.first;
                if (termCycleInfo != null) {
                    termInfo.cumulOr(termCycleInfo);
                }
                BoolVector termCycleInfoOnDiffPtr = exitInfoPair == null ? null : exitInfoPair.second;
                if (termCycleInfoOnDiffPtr != null) {
                    termInfoOnDiffPtr.cumulOr(termCycleInfoOnDiffPtr);
                }
            }
            toLastInfo.head = new TapPair<>(termInfo, termInfoOnDiffPtr);
        }
        TapList<TapPair<BoolVector, BoolVector>> termBlockInfosW = curUnitInfosW.retrieve(curBlock);
        if (termBlockInfosW == null) {
            termBlockInfosW = new TapList<>(null, new TapList<>(null, null));
            curUnitInfosW.store(curBlock, termBlockInfosW);
        }
        TapList<TapPair<BoolVector, BoolVector>> toLastInfoW = TapList.toLast(termBlockInfosW);
        if (toLastInfoW != null) {
            TapPair<BoolVector, BoolVector> exitInfoWPair = infosWDown.retrieve(curBlock);
            BoolVector termInfoW = (exitInfoWPair==null ? null : exitInfoWPair.first) ;
            BoolVector termInfoWOnDiffPtr = (exitInfoWPair==null ? null : exitInfoWPair.second) ;
            if (termInfoW == null) {
                termInfoW = new BoolVector(nDZ);
            } else {
                termInfoW = termInfoW.copy();
            }
            if (termInfoWOnDiffPtr == null) {
                termInfoWOnDiffPtr = new BoolVector(nDPZ);
            } else {
                termInfoWOnDiffPtr = termInfoWOnDiffPtr.copy();
            }
            if (hasCyclingInfo) {
                exitInfoWPair = infosWCycleDown.retrieve(curBlock);
                BoolVector termCycleInfoW = (exitInfoWPair==null ? null : exitInfoWPair.first) ;
                if (termCycleInfoW != null) {
                    termInfoW.cumulOr(termCycleInfoW);
                }
                BoolVector termCycleInfoWOnDiffPtr = (exitInfoWPair==null ? null : exitInfoWPair.second) ;
                if (termCycleInfoWOnDiffPtr != null) {
                    termInfoWOnDiffPtr.cumulOr(termCycleInfoWOnDiffPtr);
                }
            }
            toLastInfoW.head = new TapPair<>(termInfoW, termInfoWOnDiffPtr);
        }
    }

    /**
     * Terminates analysis on the given curUnit.
     * Always returns false, but anyway this result is
     * not used by top-down data-flow analyses.
     */
    @Override
    protected boolean terminateFGForUnit() {
        curUnitInfos.store(curUnit.entryBlock,
                new TapList<>(infosDown.retrieve(curUnit.entryBlock), null));
        curUnitInfos.store(curUnit.exitBlock,
                new TapList<>(infosUp.retrieve(curUnit.exitBlock), null));
        curUnitInfosW.store(curUnit.entryBlock,
                new TapList<>(infosWDown.retrieve(curUnit.entryBlock), null));
        curUnitInfosW.store(curUnit.exitBlock,
                new TapList<>(infosWUp.retrieve(curUnit.exitBlock), null));
        return false;
    }

    /**
     * @return the "useBar" info for the code from the given frontier
     * to the end of the enclosing Checkpoint.
     * ATTENTION: takes away the bits about test zones !
     * Requires that "curUnitInfos" is set beforehand to the
     * correct diffLivenessesCkp or diffLivenesses of curUnit.
     */
    public TapPair<BoolVector, BoolVector> getFrontierLiveness(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        int resNDPZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.PTRKIND);
        BoolVector result = new BoolVector(resNDZ);
        getFrontierInfo(frontier, curUnitInfos, result, 0, false);
        BoolVector resultOnDiffPtr = new BoolVector(resNDPZ);
        getFrontierInfo(frontier, curUnitInfos, resultOnDiffPtr, 0, true);
        return new TapPair<>(result, resultOnDiffPtr);
    }

    /**
     * @return the "outBar" info for the code from the given frontier
     * to the end of the enclosing Checkpoint.
     * Requires that "curUnitInfosW" is set beforehand to the
     * correct diffOverwritesCkp or diffOverwrites of curUnit.
     */
    public TapPair<BoolVector, BoolVector> getFrontierOverwrite(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.ALLKIND);
        int resNDPZ = commonNDZofFrontierOrigins(frontier, SymbolTableConstants.PTRKIND);
        BoolVector result = new BoolVector(resNDZ);
        getFrontierInfo(frontier, curUnitInfosW, result, 0, false);
        BoolVector resultOnDiffPtr = new BoolVector(resNDPZ);
        getFrontierInfo(frontier, curUnitInfosW, resultOnDiffPtr, 0, true);
        return new TapPair<>(result, resultOnDiffPtr);
    }

    /**
     * Utility for getFrontierLiveness() and getFrontierOverwrite().
     */
    private void getFrontierInfo(TapList<FGArrow> frontier,
                                 BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> infos,
                                 BoolVector result, int offset, boolean onDiffPtr) {
        FGArrow arrow;
        int commonLength;
        TapPair<BoolVector, BoolVector> infoPair;
        while (frontier != null) {
            arrow = frontier.head;
            commonLength = offset + arrow.commonSymbolTable().declaredZonesNb(onDiffPtr ? SymbolTableConstants.PTRKIND : SymbolTableConstants.ALLKIND);
            infoPair = infos.retrieve(arrow.destination).head;
            result.cumulOr(onDiffPtr ? infoPair.second : infoPair.first, commonLength);
            frontier = frontier.tail;
        }
    }

    /**
     * Propagates the current DiffLiveness info "diffLives" backwards through
     * a given expression "tree". "diffLives" is modified by side-effect to
     * hold the new DiffLiveness info just upstream the expression.
     * Assumes that activities of sub-expressions have been
     * precomputed and stored as annotations in the tree.
     * Implements the formula from the paper:
     * <pre>
     * {@code
     * N_bar([I;D]) = N(I') \cup ( N_bar(D) \times Dep(I) )
     * W_bar([I;D]) = W_bar(D) \cup out(I)_{if adj-live(I)}
     * -- I is the considered expression "tree",
     * -- I' is the derivative of I, computed implicitly using activity,
     * -- N(I') can be computed by sweeping through "tree",
     * -- N_bar(D) is the initial given "livenessTmp",
     * -- Dep(I) is also computed implicitly during this method,
     * -- N_bar([I;D]) is returned as the resulting final value of  "livenessTmp"
     * }
     * </pre>
     *
     * @param tree      the expression currently analyzed
     * @param needVal   true if the expression's value is needed by the differentiated code.
     * @param needLoc   true if the expression's location is needed, i.e. all indices
     *                  and pointers used to compute it are "needVal".
     * @param needDiff  if the differentiated expression will be used in the differentiated code
     *                  and therefore all variables in its partial derivatives will become needed,
     *                  and also if the expression is a pointer, its diff pointer is needed (OnDiffPtr).
     * @param diffLives the DiffLiveness info that is passed (backwards)
     *                  through the current expression "tree".
     * @param arrives   is set to false when the control does not go through the
     *                  tree (e.g. "stop"). If arrives==null, this "stop" checking is just not done.
     */
    private void diffLivenessThroughExpression(Tree tree,
                                               boolean needVal, boolean needLoc, boolean needDiff,
                                               BoolVector diffLives, BoolVector diffLivesOnDiffPtr,
                                               BoolVector diffOwrts, BoolVector diffOwrtsOnDiffPtr,
                                               BoolVector beforeAvlX, BoolVector afterReqX, ToBool arrives) {

//[llh 12/01/2016] TODO: here use the new diffLivesOnDiffPtr and diffOwrtsOnDiffPtr for liveness info on diff pointers

        // Special cases where the original instruction "tree" will cause a differentiated
        // version added in the diff code, either in the tangent diff code,
        // or in the forward sweep of the reverse mode !
        // This happens for (cf BlockDifferentiator.buildFwdDiffControlTrees()) statements
        // that overwrite a pointer which is reqX (e.g. "p = &v + i*offset" when some later
        // diff statement will use p'). In this case, we must create a "p' = &v' + i*offset".
        // For the present analysis, this implies
        //   1) that all primal expressions used in the diff
        //       statement (in the example i and offset) become diffLive.
        //   2) that the control around is live.
        Tree assignedTree = null;
        Tree rhsTree = null;
        if (tree.opCode() == ILLang.op_assign ||
                tree.opCode() == ILLang.op_plusAssign ||
                tree.opCode() == ILLang.op_minusAssign) {
            assignedTree = tree.down(1);
            rhsTree = tree.down(2);
        } else if (tree.opCode() == ILLang.op_unary) {
            String unaryOper = ILUtils.getIdentString(tree.down(1));
            if (unaryOper != null &&
                    (unaryOper.equals("++prefix") || unaryOper.equals("++postfix") ||
                            unaryOper.equals("--prefix") || unaryOper.equals("--postfix"))) {
                assignedTree = tree.down(2);
            }
        }
        if (assignedTree != null) {
            WrapperTypeSpec assignedType = curSymbolTable.typeOf(assignedTree);
            if (assignedType != null && assignedType.containsAPointer()) {
                TapIntList assignedZonesList =
                        ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(assignedTree,
                                null, curInstruction, null), true);
                if (// "PointerActiveUsage":
                    // Rationale: Suppose there is a pointer assignment, and the pointer
                    // being set is ReqX, which means that the differentiated code will
                    // have to add here an assignment that sets the derivative of the pointer.
                    // Then all values needed to compute what is set in the diff pointer
                    // must become diff-live.
                    // Note that we could find these "active" pointer assignments by using
                    // the set diffLivesOnDiffPtr that is computed precisely here, but
                    // it looks as though we can use ReqX just as well...
                    afterReqX == null || intersectsZoneRks(afterReqX, assignedZonesList, null)) {
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                    diffLivenessThroughPointerExpression(assignedTree, diffLives, diffOwrts);
                    if (rhsTree != null) {
                        diffLivenessThroughPointerExpression(rhsTree, diffLives, diffOwrts);
                    }
                }
            }
        }

        // NOTE: [llh, jan 2008] I think it doesn't make sense to turn the booleans *need*
        // into TapList trees of Booleans (to distinguish structure components)
        // because e.g. if a statement is needed because one of the components
        // of the lhs is needed, then the complete statement will become live
        // (because we are not going to split it in several statements)
        // and thus even the computations that result in the not-needed
        // parts will be made, and their arguments therefore become needed.
        // Yet we need to do something about that for calls...
        boolean needValSon;
        boolean needDiffSon;
        switch (tree.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                Tree lhs = tree.down(1);
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && tree.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                TapIntList writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, true);
                needValSon = intersectsZoneRks(diffLives, writtenZonesList, null);
                // An allocate is always live :
                if (!needValSon && tree.down(2).opCode() == ILLang.op_allocate) {
                    needValSon = true;
                }

                // This mapZoneRkToZoneInfo is only to check that lhs is of differentiable type:
                TapIntList writtenDiffKindVectorIndices = mapZoneRkToKindZoneRk(writtenZonesList, TapEnv.diffKind()) ;
                TapList<ZoneInfo> zones = DataFlowAnalyzer.mapZoneRkToZoneInfo(writtenZonesList, curSymbolTable);
// System.out.println("lhs:"+lhs+" wzl:"+writtenZonesList+" diffkIndices:"+writtenDiffKindVectorIndices+" intersects "+diffLivesOnDiffPtr+" replies:"+intersectsKindZoneRks(diffLivesOnDiffPtr, SymbolTableConstants.PTRKIND, writtenZonesList, null, curSymbolTable))  ;
                needDiffSon = ((ADActivityAnalyzer.isAnnotatedActive(curActivity, lhs, curSymbolTable)
                                || intersectsKindZoneRks(diffLivesOnDiffPtr, SymbolTableConstants.PTRKIND, writtenZonesList, null, curSymbolTable)
                                || (writtenDiffKindVectorIndices != null &&
                                    (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
                                     || (!TapEnv.spareDiffReinitializations()
                                         && ADActivityAnalyzer.haveDifferentiatedVariable(zones)))))
                               // When lhs, although active, generates no diff code, e.g. in x = x+2.0,
                               // there is no need for the diff of lhs:
                               && !(adActivityAnalyzer != null && trivialIdentDiff(curActivity, tree, curSymbolTable))) ;
                if (needValSon) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setRefValue(lhs, SymbolTableConstants.ALLKIND, diffOwrts, true, true);
                }
                boolean isPointerAssignment = TypeSpec.isA(curSymbolTable.typeOf(lhs), SymbolTableConstants.POINTERTYPE) ;
                if (needDiffSon && isPointerAssignment) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, true);
                    setRefValue(lhs, SymbolTableConstants.PTRKIND, diffOwrtsOnDiffPtr, true, true);
                }
                if (needValSon || needDiffSon) {
                    //Si on met "true" il y a un mini bug dans f90:v64
                    //Si on met "needValSon", ca corrige v64, mais ca regresse beaucoup sur
                    // f90:lh07,lh21,v197,v199,v21,v213 !!!
                    setControllingLive(curInstruction, curBlock, diffLives, true/*needValSon*/);
                }
                diffLivenessThroughExpression(lhs,
                        needValSon, needLoc, needValSon || needDiffSon,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
// System.out.println(" INTERMEDIATE AFTER LHS "+diffLivesOnDiffPtr) ;
                // TODO: [llh, jul 2020] The following reset of liveness of the overwritten lhs should
                //  be done inside the above diffLivenessThroughExpression(lhs, ...)
                if (needValSon && totalAccess) {
                    setRefValue(lhs, SymbolTableConstants.ALLKIND, diffLives, false, false);
                }
                if (/*needDiffSon &&*/ totalAccess) {  // Why restrict needDiffSon ? (causes infinite looping on set11/lh055...)
                    setRefValue(lhs, SymbolTableConstants.PTRKIND, diffLivesOnDiffPtr, false, false);
                }
// System.out.println(" RESULTS IN "+diffLivesOnDiffPtr) ;
                diffLivenessThroughExpression(tree.down(2),
                        needVal || needValSon, needLoc, needDiffSon,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            }
            case ILLang.op_return:
                diffLivenessThroughExpression(tree.down(2),
                        true, true, false,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                if (!ILUtils.isNullOrNone(tree.down(1))) {
                    // child 1 is the returned expression (C style).
                    String returnVarName = curUnit.otherReturnVar() == null ?
                            curUnit.name() : curUnit.otherReturnVar().symbol;
                    Tree assignTree = ILUtils.turnReturnIntoAssign(tree, returnVarName);
                    boolean activeResult =
                            ADActivityAnalyzer.isAnnotatedActive(curActivity, assignTree.down(2), curSymbolTable);
                    //If the returned value is annotated active,
                    // then this equivalent assignment must be annotated active too.
                    if (activeResult) {
                        ADActivityAnalyzer.setAnnotatedActive(curActivity, assignTree.down(1));
                    }
                    diffLivenessThroughExpression(assignTree,
                            false, false, activeResult,
                            diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                    ILUtils.resetReturnFromAssign(tree, assignTree);
                    copyTreeRequiredInDiff(curActivity, assignTree, tree);
                }
                break;
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_complexConstructor:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_mul:
            case ILLang.op_mod:
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(2), curSymbolTable);
                needDiffSon = needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(1), curSymbolTable);
                diffLivenessThroughExpression(tree.down(1),
                        needValSon, needLoc, needDiffSon,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(1), curSymbolTable);
                needDiffSon = needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(2), curSymbolTable);
                diffLivenessThroughExpression(tree.down(2),
                        needValSon, needLoc, needDiffSon,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_div:
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(2), curSymbolTable);
                needDiffSon = needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree.down(1), curSymbolTable);
                diffLivenessThroughExpression(tree.down(1),
                        needValSon, needLoc, needDiffSon,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree, curSymbolTable);
                diffLivenessThroughExpression(tree.down(2),
                        needValSon, needLoc, needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_power:
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree, curSymbolTable);
                diffLivenessThroughExpression(tree.down(1),
                        needValSon, needLoc, needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needValSon,
                        needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_minus:
            case ILLang.op_sizeof:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_not:
            case ILLang.op_times:
            case ILLang.op_while:
            case ILLang.op_until:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_compGoto: {
                // Upstream of this test, this test is not needVal:
                int blockTestZone = curUnit.testZoneOfControllerBlock(curBlock);
                if (blockTestZone>=0) diffLives.set(blockTestZone, false);
                if (needVal) {//mvo
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            }
            case ILLang.op_gotoLabelVar:
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_switch: {
                // Upstream of this test, this test is not needVal:
                int blockTestZone = curUnit.testZoneOfControllerBlock(curBlock);
                if (blockTestZone>=0) diffLives.set(blockTestZone, false);
                if (needVal) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            }
            case ILLang.op_for:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(3),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_ifExpression:
                needValSon = needVal ||
                        needDiff && ADActivityAnalyzer.isAnnotatedActive(curActivity, tree, curSymbolTable);
                diffLivenessThroughExpression(tree.down(1),
                        needValSon, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(3),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_call: {
                // find the called activity pattern:
                ActivityPattern curCalledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "multiActivityCalleePatterns");
                // Find the Unit of the called function:
                Unit calledUnit = getCalledUnit(tree, curSymbolTable);
                MPIcallInfo messagePassingInfo =
                        MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), tree, curUnit.language(), curBlock);
                // MPI's isend, irecv, and wait must always be differentiated, even if apparently inactive,
                // because we can't know it for sure statically. Therefore these calls are "origCallPresent" and "diffCallPresent".
                // Other MPI calls may be not "diffCallPresent", but are always "origCallPresent" (to be discussed...)
                boolean mpiOrigCallPresent = messagePassingInfo != null;
                boolean mpiDiffCallPresent = messagePassingInfo != null && messagePassingInfo.isNonBlocking();
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                Tree[] argTrees = ILUtils.getArguments(tree).children();
                int nbArgs = argTrees.length ;
//                 BoolMatrix calleePublicDeps = calledUnit.unitDependencies;
//                 if (calleePublicDeps == null) {
//                     calleePublicDeps = DepsAnalyzer.buildDefaultIntrinsicDependencies(calledUnit);
//                 }
                BoolMatrix calleeDeps = calledUnit.plainDependencies ;
                if (calleeDeps == null) {
                    calleeDeps = DepsAnalyzer.buildDefaultIntrinsicDependencies(calledUnit);
                }
                boolean thisCallIsCheckpointed =
                    TapEnv.modeIsAdjoint() && curInstruction.thisCallIsCheckpointed(calledUnit) ;
                // Translate the diffLiveness downstream call into a downstream
                // diffLiveness that will go through the called unit.
                TapList[] callSiteParamsInfo = new TapList[1+nbArgs] ;
                for (int i=nbArgs ; i>0 ; --i) {
                    callSiteParamsInfo[i] = null ;
                }
                callSiteParamsInfo[0] = new TapList<>(needVal, null) ;
                BoolVector outputDiffLiveness =
                    translateCallSiteDataToCallee(diffLives, callSiteParamsInfo,
                                                  null, argTrees, false, tree, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND) ;
                callSiteParamsInfo[0] = new TapList<>(Boolean.FALSE, null) ;
                BoolVector outputDiffLivenessOnDiffPtr =
                    translateCallSiteDataToCallee(diffLivesOnDiffPtr, callSiteParamsInfo,
                                                  null, argTrees, false, tree, curInstruction, arrow,
                                                  SymbolTableConstants.PTRKIND) ;
                // We also need to prepare a copy of diffOwrts to be added into the callee's context
                callSiteParamsInfo[0] = new TapList<>(Boolean.FALSE, null) ;
                BoolVector outputDiffOverwrite =
                    translateCallSiteDataToCallee(diffOwrts, callSiteParamsInfo,
                                                  null, argTrees, false, tree, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND) ;
                callSiteParamsInfo[0] = new TapList<>(Boolean.FALSE, null) ;
                BoolVector outputDiffOverwriteOnDiffPtr =
                    translateCallSiteDataToCallee(diffOwrtsOnDiffPtr, callSiteParamsInfo,
                                                  null, argTrees, false, tree, curInstruction, arrow,
                                                  SymbolTableConstants.PTRKIND) ;
                //calleeDeps may be null when calledUnit does not terminate properly
                boolean origCallPresent =
                    mpiOrigCallPresent || (calleeDeps != null && someOutputIsLive(outputDiffLiveness, calleeDeps)) ;
                BoolVector inputDiffLiveness;
                BoolVector inputDiffLivenessOnDiffPtr;
                if (calleeDeps != null) {
                    BoolVector killed = calledUnit.unitInOutCertainlyW() ;
                    inputDiffLiveness = outputDiffLiveness.minus(killed) ;
                    BoolVector killedPtr = calledUnit.focusToKind(killed, SymbolTableConstants.PTRKIND) ;
                    inputDiffLivenessOnDiffPtr = outputDiffLivenessOnDiffPtr.minus(killedPtr) ;
                } else {
                    inputDiffLiveness = new BoolVector(calledUnit.paramElemsNb()) ;
                    inputDiffLivenessOnDiffPtr = new BoolVector(calledUnit.paramElemsNb()) ;
                    if (arrives != null) {
                        arrives.set(false);
                    }
                }
// The following would be faster, because it uses the value precomputed in PointerAnalyzer,
// BUT it is sometimes null (TODO Fix this). Meanwhile, we instead recompute actualParamZonesTreeS from scratch...
//                 TapList[] actualParamZonesTreeS = tree.getAnnotation("actualParamZonesTreeS") ;
                TapList[] actualParamZonesTreeS = new TapList[nbArgs];
                for (int i=nbArgs-1 ; i>=0 ; --i) {
                    actualParamZonesTreeS[i] = TapList.copyTree(
                            curSymbolTable.treeOfZonesOfValue(argTrees[i], null, curInstruction, null));
                    includePointedElementsInTree(actualParamZonesTreeS[i], null, null, true, false, true);
                }

                Context calledContext = (curCalledActivity==null ? null : curCalledActivity.diffLivenessTopDownContext()) ;
                // If this call is not checkpointed, we accumulate the "outputPublicDiffLiveness"
                // into the accumulated exit liveness of the called Unit:
                if (!thisCallIsCheckpointed && calledUnit.rank() >= 0 && calledUnit.hasSource() && curCalledActivity != null) {
                    if (TapList.contains(tracedUnits, calledUnit)) {
                        TapEnv.printOnTrace(" Call to "+calledUnit+" receives new liveness:"+outputDiffLiveness+" Ptr:"+outputDiffLivenessOnDiffPtr) ;
                        TapEnv.printOnTrace("               and also receives new overwrite:"+outputDiffOverwrite+" Ptr:"+outputDiffOverwriteOnDiffPtr) ;
                    }
// TapEnv.printOnTrace("   CURCALLEDACTIVITY OF "+calledUnit+" HAS EXITDIFFLIVENESSES:"+curCalledActivity.exitDiffLivenesses()+" Ptr:"+curCalledActivity.exitDiffLivenessesOnDiffPtr()) ;
                    calledContext.addNockpExitLiveness(outputDiffLiveness, outputDiffLivenessOnDiffPtr);
                    calledContext.addNockpExitOverwrite(outputDiffOverwrite, outputDiffOverwriteOnDiffPtr);


                    if (calledContext.nockpOutOfDate) {
                        calledUnit.analysisIsOutOfDateDown = true;
                        setAllCallersOutOfDateUpto(calledUnit, curUnit);
                        // if we schedule calledUnit for analysis,
                        // we must make sure the Overwrite info is non-null:
                        if (calledContext.nockpExitOverwrite == null) {
                            calledContext.nockpExitOverwrite = new BoolVector(calledContext.zoneNb);
                        }
                        if (calledContext.nockpExitOverwriteOnDiffPtr == null) {
                            calledContext.nockpExitOverwriteOnDiffPtr = new BoolVector(calledContext.ptrZoneNb);
                        }
                    }
                }
                if (origCallPresent) {
                    setTreeRequiredInDiff(curActivity, tree, phase==BOTTOMUP_CKP, false);
                    // Add into diffOwrts all the zones that may be overwritten by this call:
                    BoolVector functionW = new BoolVector(nDZ);
                    TapList[] outCallArgs =
                        translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), functionW, null, null, true,
                                                      tree, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                                      null, null, false, false) ;
                    for (int i=nbArgs-1 ; i>=0 ; --i) {
                        setInfoBoolTreeToExtendedDeclaredZones(functionW, actualParamZonesTreeS[i], true,
                                                               outCallArgs[i+1], true, SymbolTableConstants.ALLKIND);
                    }
                    diffOwrts.cumulOr(functionW);
                    // Add into "inputDiffLiveness" the arguments that become live
                    // upon entry because they are used by the call to produce a live output
                    // When "takeRisks" is true, it means we are ready to call a unit
                    // with some "random" argument if we know that these arguments are used
                    // only to compute values we don't need. This is risky because e.g. if "y"
                    // is not needed, "y := a/b" may be called with any "b", including zero,
                    // Same for "y :=A[b]" may be called with "b" out of bounds.
                    // Actually, "takeRisks" will be safely true only when we regenerate
                    // a sliced version of the original called subroutine !
                    boolean takeRisks = false;
                    // When takeRisks==false, then when "takeSmallerRisks" is true,
                    // it means we are ready, in the no-checkpointed case, to use
                    // calledContext.nockpExitLiveness as the envelope exit liveness for
                    // calledUnit. This is risky because this value is being built now,
                    // and will not be fully converged until all call sites are analyzed.
                    boolean takeSmallerRisks = true;
                    BoolVector liveDueToLiveOutputs;
                    if (takeRisks) {
                        liveDueToLiveOutputs =
                            calleeDeps.leftTimes(outputDiffLiveness);
                    } else if (!thisCallIsCheckpointed && calledContext != null && takeSmallerRisks) {
                        liveDueToLiveOutputs =
                            calleeDeps.leftTimes(calledContext.nockpExitLiveness!=null ? calledContext.nockpExitLiveness : outputDiffLiveness);
                    } else {
                        // take no risk at all:
                        liveDueToLiveOutputs = calledUnit.unitInOutPossiblyR();
                        // Neglect possible usage of IO streams:
                        int zoneIO = curCallGraph.zoneNbOfAllIOStreams ;
                        if (liveDueToLiveOutputs.get(zoneIO)) {
                            liveDueToLiveOutputs = liveDueToLiveOutputs.copy() ;
                            liveDueToLiveOutputs.set(zoneIO, false) ;
                        }
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("  adding public Diff Live inputs of " + calledUnit + " because needed for Diff Live outputs:" + liveDueToLiveOutputs);
                    }
                    inputDiffLiveness.cumulOr(liveDueToLiveOutputs);
                }
                TapList[] diffArgsNeeded;
                // TODO: make sure this is consistent with "diffCallNeeded()" in CallGraphDifferentiator!
                boolean diffCallPresent =
                    (mpiDiffCallPresent
                     || ADActivityAnalyzer.isAnnotatedActive(curActivity, tree, curSymbolTable)
                     || ReqExplicit.isAnnotatedPointerActive(curActivity, ILUtils.getCalledName(tree))) ;
                if (diffCallPresent) {
                    BoolVector calleeDiffNeeded;
                    BoolVector calleeEntryLivenessCkp;
                    if (curUnit.isC()
                        && ("memcpy".equals(calledUnit.name()) || "cudaMemcpy".equals(calledUnit.name()))) {
                        // Special case of memcpy-style calls: just a copy, so primal values are not needed!
                        int calleePublicSize = calledUnit.paramElemsNb() ;
                        calleeDiffNeeded = new BoolVector(calleePublicSize) ;
                        calleeEntryLivenessCkp = new BoolVector(calleePublicSize) ;
                    } else if (curCalledActivity == null || !calledUnit.hasSource() || calledUnit.rank() <= 0) {
                        calleeDiffNeeded =
                                ADActivityAnalyzer.buildDefaultIntrinsicCallActivity(calledUnit, TapEnv.diffKind());
                        calleeEntryLivenessCkp = calledUnit.unitInOutPossiblyR();
                    } else {
                        calleeDiffNeeded = calledUnit.unfocusFromKind(curCalledActivity.callActivity(), TapEnv.diffKind()).or(curCalledActivity.entryReqX());
                        calleeEntryLivenessCkp = curCalledActivity.entryDiffLivenessesCkp();
                    }
                    diffArgsNeeded =
                        translateCalleeDataToCallSite(calleeDiffNeeded, null,
                                null, null, true,
                                tree, curInstruction, arrow, SymbolTableConstants.ALLKIND, null, null, true, false) ;
                        // was propagateDataToCaller(calleeDiffNeeded, null, null, nbArgs, null, null, null,
                        //            curInstruction, arrow, true, true, TapEnv.diffKind());
                    if (!thisCallIsCheckpointed) {
                        setTreeRequiredInDiff(curActivity, tree, phase==BOTTOMUP_CKP, false);
                    }
                    // Add into inputDiffLiveness the variables that are required to
                    // compute the derivatives found inside the diff of calledUnit:
                    // In case of recursive code, calleeEntryLivenessCkp may be null !
                    if (calleeEntryLivenessCkp != null) {
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("  adding public Diff Live inputs of " + calledUnit + " (due to internal diff computations) :" + calleeEntryLivenessCkp);
                        }
                        inputDiffLiveness.cumulOr(calleeEntryLivenessCkp);
                    }
                } else {
                    diffArgsNeeded = new TapList[1+nbArgs];
                    for (int i=nbArgs ; i>=0 ; --i) {
                        diffArgsNeeded[i] = new TapList<>(Boolean.FALSE, null);
                    }
                }
                if (origCallPresent || diffCallPresent) {
                    // now translate the upstream public diffLiveness into the private diffLiveness
                    // that will be propagated upstream, and at the same time compute
                    // the diffLiveness that must be propagated upstream each actual argument.
                    BoolVector callSiteKilled = new BoolVector(nDZ);
                    translateCalleeDataToCallSite(calledUnit.unitInOutCertainlyW(), callSiteKilled, null, argTrees, true,
                                                  tree, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                                  null, null, false, true) ;
                    TapList[] argLivenesses =
                        translateCalleeDataToCallSite(inputDiffLiveness, diffLives,
                                                      null, null, true, tree, curInstruction, arrow,
                                                      SymbolTableConstants.ALLKIND,
                                                      null, callSiteKilled.not(), true, false) ;

                    BoolVector callSiteKilledPtr = curSymbolTable.focusToKind(callSiteKilled, SymbolTableConstants.PTRKIND) ;
                    translateCalleeDataToCallSite(inputDiffLivenessOnDiffPtr, diffLivesOnDiffPtr,
                                                  null, null, true, tree, curInstruction, arrow,
                                                  SymbolTableConstants.PTRKIND,
                                                  null, callSiteKilledPtr.not(), true, false) ;
                    // Continue diff liveness analysis for the actual arguments of the call:
                    for (int i=nbArgs-1 ; i>=0 ; --i) {
                        setInfoBoolTreeToExtendedDeclaredZones(diffLives,
                                actualParamZonesTreeS[i], argLivenesses[1+i], null, false, SymbolTableConstants.ALLKIND);
                        boolean argIsLive = TapList.oneTrue(argLivenesses[1+i]);
                        if (argIsLive) {
// ATTENTION  LA LIGNE SUIVANTE EMPECHE diffLiveness->dead du call CostFunction dans morlighem-issm
// Mais on dirait qu'il faut la garder pour set04/lh141 (seulement!!)... A revoir!
                            setTreeRequiredInDiff(curActivity, argTrees[i], phase==BOTTOMUP_CKP, false);
                        }
                        // If actualParamZonesTreeS[i]==null, the argument comes from the stack
                        // otherwise it is a reference to (a part of) a variable,
                        // and the previous "setInfoBoolTreeToExtendedDeclaredZones" has done the job.
                        // [vmp 2015/10/13] original arguments are kept as arguments of the differentiated call,
                        // so they must be analyzed with needLoc=true. cf set07/v501 :
                        diffLivenessThroughExpression(argTrees[i], actualParamZonesTreeS[i]==null && argIsLive,
                                true, TapList.oneTrue(diffArgsNeeded[1+i]),
                                diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                                beforeAvlX, afterReqX, arrives);
                    }
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                } else {
                    for (int i=nbArgs-1 ; i>=0 ; --i) {
                        diffLivenessThroughExpression(argTrees[i], false, false, false,
                                diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                                beforeAvlX, afterReqX, arrives);
                    }
                }
                break;
            }
            case ILLang.op_ioCall: {
                // If this IO defines a switch, then its testZone is not needed upstream:
                int blockTestZone = curUnit.testZoneOfControllerBlock(curBlock);
                if (blockTestZone>=0 && curBlock.lastInstr().tree == tree) diffLives.set(blockTestZone, false);
                int zoneIO = curCallGraph.zoneNbOfAllIOStreams;
                String ioAction = ILUtils.getIdentString(tree.down(1));
                boolean writesInVars = ioAction.equals("read") ||
                        ioAction.equals("accept") || ioAction.equals("decode");
                boolean thisIOcallIsLive = needVal;
                if (diffLives.get(zoneIO)) {
                    thisIOcallIsLive = true;
                }
                ToBool total = new ToBool(false);
                ToBool totally = new ToBool(false);
                ToBool written = new ToBool(false);
                Tree ioSpecTree;
                TapList<Tree> usedTrees = null;
                TapList<Tree> treesResetInDiff = null;
                Tree[] ioArgs = tree.down(3).children();
                BoolVector ioWritten = new BoolVector(nDZ);
                // Look for definitions of implicit-do indices that are adjoint-live:
                for (int i = ioArgs.length - 1; i >= 0; i--) {
                    TapList<Tree> implicitDoIndices = collectImplicitDoIndices(ioArgs[i], null);
                    while (implicitDoIndices != null) {
                        Tree index = implicitDoIndices.head;
                        if (intersectsExtendedDeclared(diffLives, SymbolTableConstants.ALLKIND,
                                index, total, curInstruction,
                                null, curSymbolTable)) {
                            thisIOcallIsLive = true;
                            if (total.get()) {
                                setRefValue(index, SymbolTableConstants.ALLKIND, diffLives, false, false);
                            }
                        }
                        setRefValue(index, SymbolTableConstants.ALLKIND, ioWritten, true, true);
                        implicitDoIndices = implicitDoIndices.tail;
                    }
                }
                // Look for IO that defines an adjoint-live value:
                for (int i = ioArgs.length - 1; i >= 0; i--) {
                    if (writesInVars) {
                        if (intersectsExtendedDeclared(diffLives, SymbolTableConstants.ALLKIND,
                                ioArgs[i], total, curInstruction,
                                null, curSymbolTable)) {
                            thisIOcallIsLive = true;
                            if (total.get()) {
                                setRefValue(ioArgs[i], SymbolTableConstants.ALLKIND, diffLives, false, false);
                            }
                        }
                        WrapperTypeSpec argType = curSymbolTable.typeOf(ioArgs[i]);

//[llh] TODO: extract the following test to make it identical in its 2 locations "test ioarg diffinit"
                        if (ADActivityAnalyzer.hasOneAnnotatedActive(curActivity, ioArgs[i], curSymbolTable)
                                || ILUtils.isAccessedThroughPointer(ioArgs[i])
                                && (argType == null || argType.wrappedType == null
                                || argType.wrappedType.isDifferentiated(null))) {
                            treesResetInDiff = new TapList<>(ioArgs[i], treesResetInDiff);
                        }
                        setRefValue(ioArgs[i], SymbolTableConstants.ALLKIND, ioWritten, true, true);
                    } else {
                        usedTrees = new TapList<>(ioArgs[i], usedTrees);
                    }
                }
                // Run the same read/write analysis as above, this time for the IO specs:
                Tree[] ioSpecs = tree.down(2).children();
                for (int i = ioSpecs.length - 1; i >= 0; i--) {
                    ioSpecTree = InOutAnalyzer.getIOeffectOfIOspec(ioSpecs[i], i, ioAction,
                            written, totally, curSymbolTable);
                    if (written.get()) {
                        if (intersectsExtendedDeclared(diffLives, SymbolTableConstants.ALLKIND,
                                ioSpecTree, total, curInstruction,
                                null, curSymbolTable)) {
                            thisIOcallIsLive = true;
                            if (total.get() && totally.get()) {
                                setRefValue(ioSpecTree, SymbolTableConstants.ALLKIND, diffLives, false, false);
                            }
                        }
                        if (ADActivityAnalyzer.hasOneAnnotatedActive(curActivity, ioSpecTree, curSymbolTable)) {
                            treesResetInDiff = new TapList<>(ioSpecTree, treesResetInDiff);
                        }
                        setRefValue(ioSpecTree, SymbolTableConstants.ALLKIND, ioWritten, true, true);
                    } else {
                        usedTrees = new TapList<>(ioSpecTree, usedTrees);
                    }
                }
                if (thisIOcallIsLive) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                    while (usedTrees != null) {
                        diffLivenessThroughExpression(usedTrees.head,
                                true, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                                beforeAvlX, afterReqX, arrives);
                        usedTrees = usedTrees.tail;
                    }
                    diffLives.set(zoneIO, true);
                    diffOwrts.cumulOr(ioWritten);
                } else if (ADActivityAnalyzer.isAnnotatedActive(curActivity, tree, curSymbolTable) || treesResetInDiff != null) {
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                while (treesResetInDiff != null) {
                    Tree toResetDiff = treesResetInDiff.head;
                    diffLivenessThroughExpression(toResetDiff, false, true, false,
                            diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                    treesResetInDiff = treesResetInDiff.tail;
                }
                break;
            }
            case ILLang.op_iterativeVariableRef:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, true, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                setRefValue(tree.down(1), SymbolTableConstants.ALLKIND, diffOwrts, true, true);
                break;
            case ILLang.op_arrayConstructor:
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] sons = tree.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    diffLivenessThroughExpression(sons[i],
                            needVal, needLoc, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                }
                break;
            }
            case ILLang.op_address:
                diffLivenessThroughExpression(tree.down(1),
                        false, needVal, needVal || needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break ;
            case ILLang.op_arrayAccess:
                if (needVal) {
                    setRefValue(tree, SymbolTableConstants.ALLKIND, diffLives, true, false);
                }
                diffLivenessThroughExpression(tree.down(1),
                        false, needVal, needVal || needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal || needLoc || needDiff, false, false,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_fieldAccess:
                if (needVal) {
                    setRefValue(tree, SymbolTableConstants.ALLKIND, diffLives, true, false);
                }
                diffLivenessThroughExpression(tree.down(1),
                        false, needVal || needLoc, needVal || needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_pointerAccess:
                if (needVal) {
                    setRefValue(tree, SymbolTableConstants.ALLKIND, diffLives, true, false);
                }
                diffLivenessThroughExpression(tree.down(1),
                        needVal || needLoc, false, needDiff,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal || needLoc || needDiff, false, false,
                        diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_ident:
                if (needVal) {
                    setRefValue(tree, SymbolTableConstants.ALLKIND, diffLives, true, false);
                }
                if (needDiff) {
                    setRefValue(tree, SymbolTableConstants.PTRKIND, diffLivesOnDiffPtr, true, false);
                }
                break;
            case ILLang.op_loop: {
                if (needVal) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                diffLivenessThroughExpression(tree.down(3),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                // Upstream of this loop the loop is not needed:
                int blockTestZone = curUnit.testZoneOfControllerBlock(curBlock);
                if (blockTestZone>=0) diffLives.set(blockTestZone, false);
                break;
            }
            case ILLang.op_forall:
                break;
            case ILLang.op_forallRangeSet:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_forallRangeTriplet:
            case ILLang.op_do:
                // If the index of the do-loop is needed, then the do statement is live too.
                // except if this is a clean do (since the index can be recomputed in the BWD sweep)
                // TODO: in case of wild do-loop we should consider all the exit arrows
                // from the loop block.
                if (diffLives != null &&
                        intersectsExtendedDeclared(diffLives, SymbolTableConstants.ALLKIND,
                                tree.down(1), null, curInstruction,
                                null, curSymbolTable)) {
                    needVal = true;
                    if (!(curBlock instanceof HeaderBlock && curBlock.isACleanDoLoop())) {
                        setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    }
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                //[llh] convention/hack: needDiff is set to true iff we come from an
                // op_iterativeVariableRef, in which case we must consider that the loop index is overwritten:
                if (needDiff) {
                    ToBool total = new ToBool(false);
                    TapIntList indexZones =
                            curSymbolTable.listOfZonesOfValue(tree.down(1), total, curInstruction);
                    if (total.get()) {
                        diffLives.set(indexZones, false);
                    }
                }
                // The do-from part of the do is used only once, at the loop entry, and
                //  this is taken into account in propagateAndStoreDiffLivenessThroughBlock(...)
                diffLivenessThroughExpression(tree.down(2),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(3),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(4),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_arrayTriplet:
                diffLivenessThroughExpression(tree.down(1),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(3),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_break:
            case ILLang.op_stop:
                // A STOP or a BREAK is live: it must be reproduced in the
                // diff code, even in reverse mode! (cf nonRegrF90:lh45)
                setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                setControllingLive(curInstruction, curBlock, diffLives, true);
                if (arrives != null) {
                    arrives.set(false);
                }
                break;
            case ILLang.op_allocate: {
                diffLivenessThroughExpression(tree.down(1),
                        true, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                diffLivenessThroughExpression(tree.down(2),
                        true, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                // ALLOCATE's are always live; they must always be kept!
                setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                setControllingLive(curInstruction, curBlock, diffLives, true);
                // ALLOCATE's are considered to overwrite the allocated destination:
                TapList allocatedZones = tree.getAnnotation("allocatedZones");
                diffOwrts.set(ZoneInfo.listAllZones(allocatedZones, false), true);
                break;
            }
//[llh 5May2015] TODO: FOR allocate and deallocate:
// improve the mechanism to detect the "last" deallocate's before the turn point.
// These deallocates can be considered adjoint dead.
// The "last" deallocate could be defined as those that are
//  never followed by an allocate before turn point.
// The present mechanism that detects a possible overwrite of the pointed destination is dubious!
            case ILLang.op_deallocate:
            case ILLang.op_nullify: {
                // Special behavior for deallocate and nullify: if the pointed destination is not overwritten
                // until turn point, then we don't mark the instruction as live, with the effect that
                // the instruction will not be copied into the adjoint, and nothing will be added to the
                // bwd sweep either. The effect is that we spare a useless free/realloc pair.
                Tree destinationTree = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(tree.down(1)));
                if (TapEnv.modeIsTangent()
                        || intersectsExtendedDeclared(diffOwrts, SymbolTableConstants.ALLKIND,
                                                      destinationTree, null, curInstruction, null, curSymbolTable)
                        // Also mark dealloc instruction as live if the destination, although not overwritten,
                        // may become unreachable because the dealloc'ed pointer is itself overwritten later:
                        || intersectsExtendedDeclared(diffOwrts, SymbolTableConstants.ALLKIND,
                                                      tree.down(1), null, curInstruction, null, curSymbolTable)) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    if (!TapEnv.modeIsTangent() && tree.opCode() == ILLang.op_deallocate) {
                        if (phase == BOTTOMUP_CKP ? curUnit.mustDifferentiateJoint() : curUnit.mustDifferentiateSplit()) {
                            // This is a deallocate, and it is diff-live in at least one of the adjoint
                            // diffs (JOINT or SPLIT) of this unit, so there will be a deallocate/reallocate.
                            // All zones freed here (i.e. not through pointers) must be marked as relocated:
                            TapIntList freedZones =
                                    ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(destinationTree, null, curInstruction, null),
                                            false);
                            ZoneInfo zone;
                            int firstZoneRkOutOfScope = phase == BOTTOMUP_CKP
                                    ? curUnit.privateSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND)
                                    : curUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.ALLKIND);
                            while (freedZones != null) {
                                if (freedZones.head >= firstZoneRkOutOfScope) {
                                    zone = curSymbolTable.declaredZoneInfo(freedZones.head, SymbolTableConstants.ALLKIND);
                                    if (zone != null) {
                                        // For the time being, when a zone is relocated, so is its diff:
                                        zone.relocated = true;
                                        zone.relocatedDiff = true;
                                        if (freedZones.head < curUnit.publicSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND)) {
                                            // i.e. if this zone is a formal argument of the current Unit:
                                            TapEnv.toolError("  Problem on " + ILUtils.toString(tree)
                                                    + " in " + curUnit.name() + ": relocated info on formal argument "
                                                    + ILUtils.toString(zone.accessTree)
                                                    + " must be attached to its actual memory zone. Requires -nooptim refineADMM");
                                        }
                                    }
                                }
                                freedZones = freedZones.tail;
                            }
                        }
                    }
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                }
                break;
            }
            case ILLang.op_assignLabelVar:
                if (intersectsExtendedDeclared(diffLives, SymbolTableConstants.ALLKIND,
                                               tree.down(2), null, curInstruction,
                                               null, curSymbolTable)) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives,
                            true);
                    setRefValue(tree.down(2), SymbolTableConstants.ALLKIND, diffLives, false, false);
                    setRefValue(tree.down(2), SymbolTableConstants.ALLKIND, diffOwrts, true, true);
                }
                break;
            case ILLang.op_nameEq:
                diffLivenessThroughExpression(tree.down(2),
                        needVal, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                diffLivenessThroughExpression(tree.down(2),
                        needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_unary: {
                String operName = ILUtils.getIdentString(tree.down(1));
                if (operName != null &&
                        (operName.equals("++prefix") || operName.equals("++postfix") ||
                                operName.equals("--prefix") || operName.equals("--postfix"))) {
                    // These unary operators modify their argument:
                    if (needVal) {
                        needValSon = true;
                    } else {
                        TapIntList writtenZonesList =
                                ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(tree.down(2), null,
                                        curInstruction, null), true);
                        needValSon = intersectsZoneRks(diffLives, writtenZonesList, null);
                    }
                    if (needValSon) {
                        setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                        setRefValue(tree.down(2), SymbolTableConstants.ALLKIND, diffOwrts, true, true);
                        setControllingLive(curInstruction, curBlock, diffLives, true);
                    }
                    diffLivenessThroughExpression(tree.down(2),
                            needValSon, needLoc, false,
                            diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                } else {
                    //Default: we assume unary operator just reads its argument:
                    diffLivenessThroughExpression(tree.down(2),
                            needVal, needLoc, needDiff, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                }
                break;
            }
            case ILLang.op_varDeclaration: {
                Tree[] sons = tree.down(3).children();
                Tree declarator;
                inADeclaration = true;
                for (int i = 0; i <= sons.length - 1; i++) {
                    declarator = sons[i];
                    if (declarator.opCode() == ILLang.op_assign) {
                        // because of annotations to set,
                        // run on the non-copied declarator to make sure...
                        diffLivenessThroughExpression(
                                declarator, needVal, false, false,
                                diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                                beforeAvlX, afterReqX, arrives);
                        ILUtils.turnAssignFromInitDecl(declarator);
                    }
                    diffLivenessThroughExpression(
                            declarator, needVal, false, false,
                            diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                    if (declarator.opCode() == ILLang.op_assign) {
                        ILUtils.resetAssignFromInitDecl(declarator);
                    }
                }
                inADeclaration = false;
            }
            //WARNING: this case continues into next case:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
                //We have no liveness analysis on declarations yet: ([llh]TODO:build one!)
                // Meanwhile, we consider a declaration is always live, and
                // therefore it always makes its control live!
                setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                setControllingLive(curInstruction, curBlock, diffLives, true);
                break;
            case ILLang.op_sizedDeclarator:
                diffLivenessThroughExpression(tree.down(2),
                        true, false, false, diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                        beforeAvlX, afterReqX, arrives);
                break;
            case ILLang.op_parallelRegion:
                if (ILUtils.isIdent(tree.down(1), "CUDA", true)) {
                    setTreeRequiredInDiff(curActivity, tree, phase == BOTTOMUP_CKP, false);
                    setControllingLive(curInstruction, curBlock, diffLives, true);
                    diffLivenessThroughExpression(tree.down(2),
                            needVal, false, false,
                            diffLives, diffLivesOnDiffPtr, diffOwrts, diffOwrtsOnDiffPtr,
                            beforeAvlX, afterReqX, arrives);
                }
                break ;
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_switchCases:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_strings:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_formatElem:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_include:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            default:
                TapEnv.toolWarning(-1, "(Diff Liveness analysis) Unexpected operator: " + tree.opName());
                break;
        }
    }

    /**
     * Adds into "diffLives" the zones used by the derivative of the pointer expression "expr".
     * Example: the zones used by the derivative of the pointer arithmetic assignment:
     * {@code str.ptrarr[i] = &v[j] + k*offset}
     * are the zones of i,j,k, and offset. The zones of str and v are not added because the
     * diff expression uses only their derivatives str{d or b} and v{d or b}.
     */
    private void diffLivenessThroughPointerExpression(Tree expr, BoolVector diffLives, BoolVector diffOwrts) {
        switch (expr.opCode()) {
            case ILLang.op_ident:
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
                diffLivenessThroughPointerExpression(expr.down(1), diffLives, diffOwrts);
                setLiveAllZonesUsed(expr.down(2), diffLives);
                break;
            case ILLang.op_fieldAccess:
            case ILLang.op_address:
                diffLivenessThroughPointerExpression(expr.down(1), diffLives, diffOwrts);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                diffLivenessThroughPointerExpression(expr.down(2), diffLives, diffOwrts);
                break;
            case ILLang.op_allocate:
                diffLivenessThroughPointerExpression(expr.down(2), diffLives, diffOwrts);
                break;
            default:
                if (!expr.isAtom()) {
                    WrapperTypeSpec childType;
                    Tree[] subTrees = expr.children();
                    for (int i = subTrees.length - 1; i >= 0; i--) {
                        childType = curSymbolTable.typeOf(subTrees[i]);
                        if (TypeSpec.isA(childType, SymbolTableConstants.POINTERTYPE)) {
                            diffLivenessThroughPointerExpression(subTrees[i], diffLives, diffOwrts);
                        } else {
                            setLiveAllZonesUsed(subTrees[i], diffLives);
                        }
                    }
                }
                break;
        }
    }

    /**
     * Utility for diffLivenessThroughPointerExpression().
     */
    private void setLiveAllZonesUsed(Tree tree, BoolVector diffLives) {
        BoolVector zonesUsedByExp =
                curSymbolTable.zonesUsedByExp(tree, curInstruction);
        diffLives.cumulOr(zonesUsedByExp) ;
    }

    private TapList<Tree> collectImplicitDoIndices(Tree ioArg, TapList<Tree> collectList) {
        if (ioArg.opCode() == ILLang.op_iterativeVariableRef) {
            collectList = new TapList<>(ioArg.down(2).down(1), collectList);
        } else if (ioArg.opCode() == ILLang.op_expressions) {
            Tree[] sons = ioArg.children();
            for (int i = sons.length - 1; i >= 0; --i) {
                collectList = collectImplicitDoIndices(sons[i], collectList);
            }
        }
        return collectList;
    }

    /**
     * Wipes off all diff liveness annotations inside the given blocks.
     */
    public void resetLivenessInfo(TapList<Block> blocks) {
        Block block;
        TapList<Instruction> instructions;
        while (blocks != null) {
            block = blocks.head;
            instructions = block.instructions;
            while (instructions != null) {
                resetTreeOrSubtreeRequirementInDiff(
                        instructions.head.tree);
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }
    }

    private void resetTreeOrSubtreeRequirementInDiff(Tree tree) {
        if (tree != null) {
            setNotRequiredInDiff(curActivity, tree);
            if (!tree.isAtom()) {
                Tree[] subTrees = tree.children();
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    resetTreeOrSubtreeRequirementInDiff(subTrees[i]);
                }
            }
        }
    }

    /**
     * If some variable is active before an arrow but is not after, the
     * destination block cannot be "dead": there will be diff code generated for it !
     * TODO: Maybe we should just look at the usefulness.
     * BUG resolution: see the differentiation of last blocks of subroutine kgeom in
     * raytrans.for of lauvernet-stics. without this kbb is not reset to 0.0 .
     */
    private boolean checkActivityThroughArrow(FGArrow arrow) {
        if (arrow.destination == curUnit.exitBlock) {
            return false;
        }
        SymbolTable originSymbolTable = arrow.origin.symbolTable;
        SymbolTable commonSymbolTable = originSymbolTable
                .getCommonRoot(arrow.destination.symbolTable);
        int nDRZ = commonSymbolTable.declaredZonesNb(TapEnv.diffKind());
        int originnDRZ = originSymbolTable.declaredZonesNb(TapEnv.diffKind());
        BoolVector destinationEntryActiv =
                curUnitActivities.retrieve(arrow.destination).head;
        BoolVector originExitActiv =
                TapList.last(curUnitActivities.retrieve(arrow.origin));
        for (int i = originnDRZ - 1; i >= 0; i--) {
            int indexInActivs = i; //Not very clean!
            if (originExitActiv.get(indexInActivs) &&
                    (i >= nDRZ || !destinationEntryActiv.get(indexInActivs)))
            // if the variable's scope ends here, it's just as if it became
            // passive (cf test lh87)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Set all callers of "unit" as "out of date" for this analysis,
     * when they are "before" uptoUnit.
     * This is a little weird in a top-down analysis!
     * The reason is when A and B both call C, A may be analyzed first,
     * thus calling C in exit context CCA, and the analysis in A goes on
     * upstream the call C with context deps(C).times.CCA.
     * Then B may be analyzed, calling C in context CCB, and the context in B
     * upstream the call C should be deps(C).times.(CCA+CCB).
     * This is because there will be only one C_FWD.
     * For the same reason the context in A upstream the call C should also be
     * deps(C).times.(CCA+CCB), and therefore the analysis of A becomes out of date !
     */
    private void setAllCallersOutOfDateUpto(Unit unit, Unit uptoUnit) {
        Unit caller;
        TapList<CallArrow> arrows = unit.callers();
        while (arrows != null) {
            caller = arrows.head.origin;
            if (caller.rank() < uptoUnit.rank()) {
                caller.analysisIsOutOfDateDown = true;
            }
            arrows = arrows.tail;
        }
    }

    /**
     * @return the liveness info for the given Unit,
     * as stored in entryDiffLivenessesCkp. Refines the result using unitR.
     */
    public BoolVector getDiffLiveness(ActivityPattern pattern) {
        BoolVector unitDiffLiveness = null;
        if (TapEnv.debugAdMode() == TapEnv.NO_DEBUG && TapEnv.removeDeadPrimal()) {
            unitDiffLiveness = pattern.entryDiffLivenessesCkp();
        }
        BoolVector unitPossiblyR = pattern.unit().unitInOutPossiblyR() ;
        if (unitDiffLiveness == null) {
            unitDiffLiveness = unitPossiblyR.copy() ;
        } else {
            //TODO: This is temporary: IN-OUT has been improved and finds smaller sets,
            // due to array-loops management. Until the other analyses get improved too,
            // it may happen that use(Cb) is not smaller than use(C) !!
            // so we re-intersect use(Cb) with use(C):
            unitDiffLiveness = unitDiffLiveness.and(unitPossiblyR) ;
        }
        return unitDiffLiveness;
    }

    /**
     * @return the liveness info on differentiated pointers for the given Unit,
     * as stored in entryDiffLivenessesCkpOnDiffPtr. Refines the result using unitR.
     */
    public BoolVector getDiffLivenessOnDiffPtr(ActivityPattern pattern) {
        BoolVector unitDiffLivenessOnDiffPtr = null;
        if (TapEnv.debugAdMode() == TapEnv.NO_DEBUG && TapEnv.removeDeadPrimal()) {
            unitDiffLivenessOnDiffPtr = pattern.entryDiffLivenessesCkpOnDiffPtr();
        }
        BoolVector unitPossiblyR =
            pattern.unit().focusToKind(pattern.unit().unitInOutPossiblyR(), SymbolTableConstants.PTRKIND);
        if (unitDiffLivenessOnDiffPtr == null) {
            unitDiffLivenessOnDiffPtr = unitPossiblyR.copy() ;
        } else {
            //TODO: This is temporary: IN-OUT has been improved and finds smaller sets,
            // due to array-loops management. Until the other analyses get improved too,
            // it may happen that use(Cb) is not smaller than use(C) !!
            // so we re-intersect use(Cb) with use(C):
            unitDiffLivenessOnDiffPtr = unitDiffLivenessOnDiffPtr.and(unitPossiblyR) ;
        }
        return unitDiffLivenessOnDiffPtr;
    }

    /**
     * Assume "outputLiveness" is the liveness of the output actual arguments of
     * a procedure, and "functionDeps" is the plain dependencies of this
     * procedure. @return true when at least some live value is modified by the
     * procedure, and therefore the procedure is live, i.e. required to compute
     * live values.
     */
    private boolean someOutputIsLive(BoolVector outputLiveness,
                                     BoolMatrix functionDeps) {
        boolean result = false;
        int i = functionDeps.nRows;
        while (!result && i > 0) {
            --i;
            result = outputLiveness.get(i) && !functionDeps.isImplicitIdentityRow(i);
        }
        return result;
    }

    /**
     * Set the zones controlling block as live. When "forFwdSweep" is false,
     * enclosing clean DO loops are not made live because they will be easily
     * reproduced in the BWD sweep, thus requiring no saving from the FWD sweep.
     *
     * @param forFwdSweep true when the FWD control is needed to run a statement inside it,
     *                    and false when the FWD control is needed only to be reproduced in the BWD sweep.
     */
    private void setControllingLive(Instruction instruction, Block block,
                                    BoolVector liveZones, boolean forFwdSweep) {
        if (instruction != null && instruction.getWhereMask() != null) {
            // Collect into usedZones all zones used by the mask of instruction:
            BoolVector usedZones = new BoolVector(nDZ);
            InstructionMask curMask = instruction.getWhereMask();
            while (curMask != null) {//mvo:nested where
                //MVO-???: replace block.symbolTable by curSymbolTable
                usedZones.cumulOr(block.symbolTable.zonesUsedByExp(curMask.getControlTree(), instruction));
                //FIX-20230530:must step into the control tree as well
                setTreeRequiredInDiff(curActivity, curMask.getControlTree(), phase == BOTTOMUP_CKP, false);
                curMask = curMask.enclosingMask;
            }
            liveZones.cumulOr(usedZones) ;
        }
        // TODO: if one can manage to fuse this with Unit.buildControlDepsOf(),
        // then curUnit.testZoneInfos can become private, which would be nice.
        TapList<Block> ctrBlocks = block.controllers;
        Block ctrBlock;
        while (ctrBlocks != null) {
            ctrBlock = ctrBlocks.head;
            if (ctrBlock.testZone>=0 && (forFwdSweep || !(ctrBlock instanceof HeaderBlock && ctrBlock.isACleanDoLoop()))) {
                liveZones.set(curUnit.testZoneInfos[ctrBlock.testZone], true);
            }
            ctrBlocks = ctrBlocks.tail;
        }
    }

    /**
     * Private class that holds the initial context of the DiffLiveness
     * analysis for each Unit, This analysis context is determined by the
     * call sites of this Unit, i.e. top-down. It is made of:
     * <pre>
     * -- A boolean whether the Unit may be called in NOCKP mode,
     * i.e. with some of its outputs diff-live.<br>
     * -- A boolean that means that the NOCKP analysis is out of date
     * and must be run again.<br>
     * -- A BoolVector holding those output zones that are
     * diff-live in the NOCKP mode.<br>
     * -- A BoolVector holding those output zones that are going
     * to be diff-overwritten in the NOCKP mode.
     * </pre>
     */
    protected static final class Context {
        private final int zoneNb ;
        private final int ptrZoneNb ;
        private boolean nockpOutOfDate;
        private BoolVector nockpExitLiveness;
        private BoolVector nockpExitLivenessOnDiffPtr;
        private BoolVector nockpExitOverwrite;
        private BoolVector nockpExitOverwriteOnDiffPtr;

        private Context(Unit unit) {
            super();
            this.zoneNb = unit.publicZonesNumber(SymbolTableConstants.ALLKIND) ;
            this.ptrZoneNb = unit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
        }

        /**
         * Adds the given "additionalLiveness" to the exit liveness that
         * is used to initiate the analysis in NOCKP mode.
         * If the "additionalLiveness" brings nothing new, does nothing
         * Otherwise, sets the "nockpOutOfDate" marker to true,
         * meaning that the Unit must be analyzed again in NOCKP mode.
         */
        private void addNockpExitLiveness(BoolVector additionalLiveness, BoolVector additionalLivenessOnDiffPtr) {
            boolean modified = false;
            if (additionalLiveness != null) {
                if (nockpExitLiveness == null) {
                    nockpExitLiveness = additionalLiveness.copy();
                    modified = true;
                } else {
                    modified = nockpExitLiveness.cumulOrGrows(additionalLiveness, zoneNb);
                }
            }
            if (additionalLivenessOnDiffPtr != null) {
                if (nockpExitLivenessOnDiffPtr == null) {
                    nockpExitLivenessOnDiffPtr = additionalLivenessOnDiffPtr.copy();
                    modified = true;
                } else {
                    modified = nockpExitLivenessOnDiffPtr.cumulOrGrows(additionalLivenessOnDiffPtr, ptrZoneNb);
                }
            }
            if (modified) {
                nockpOutOfDate = true;
            }
        }

        /**
         * Adds the given "additionalOverwrite" to the exit overwrite that
         * is used to initiate the analysis in NOCKP mode.
         * If the "additionalOverwrite" brings nothing new, does nothing
         * Otherwise, sets the "nockpOutOfDate" marker to true,
         * meaning that the Unit must be analyzed again in NOCKP mode.
         */
        private void addNockpExitOverwrite(BoolVector additionalOverwrite, BoolVector additionalOverwriteOnDiffPtr) {
            boolean modified = false;
            if (additionalOverwrite != null) {
                if (nockpExitOverwrite == null) {
                    nockpExitOverwrite = additionalOverwrite.copy();
                    modified = true;
                } else {
                    modified = nockpExitOverwrite.cumulOrGrows(additionalOverwrite, zoneNb);
                }
            }
            if (additionalOverwriteOnDiffPtr != null) {
                if (nockpExitOverwriteOnDiffPtr == null) {
                    nockpExitOverwriteOnDiffPtr = additionalOverwriteOnDiffPtr.copy();
                    modified = true;
                } else {
                    modified = nockpExitOverwriteOnDiffPtr.cumulOrGrows(additionalOverwriteOnDiffPtr, ptrZoneNb);
                }
            }
            if (modified) {
                nockpOutOfDate = true;
            }
        }
    }
}
