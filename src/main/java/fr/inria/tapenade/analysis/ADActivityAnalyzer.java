/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.*;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Operator;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * Analyzer in charge of activity analysis on a CallGraph.
 * This is useful for Automatic Differentiation.
 * The results of this analysis are: <ul>
 * <li>An annotation "ActiveExpr" placed on subtrees
 * of the original code whose value is active.
 * These annotations are accessed through the *AnnotatedActive()* methods.</li>
 * <li>A UnitStorage "activities" of BlockStorage of TapList of
 * successive activity infos between each successive instructions.</li>
 * <li>A UnitStorage "usefulnesses", same for usefulnesses.</li>
 * <li>A UnitStorage "callActivities" of activity at the entry point of each unit.</li>
 * <li>A UnitStorage "exitActivities" of activity at the exit  point of each unit.</li>
 * <li>A UnitStorage "varFunctionADActivities" of callActivity and
 * exitUsefulness for each "function name" argument of each unit.</li> </ul>
 *
 * <pre>
 * Feature: derives from DataFlowAnalyzer : OK
 * Feature: able to analyze recursive code: OK
 * Feature: distinguishes structure elemts: OK
 * Feature: takes  care  of  pointer dests: ?? for BOTTOMUP_1, OK for TOPDOWN_2
 * Feature: refine on loop-local variables: not for BOTTOMUP_1, disconnected for TOPDOWN_2 cf [pbLoopArrays]
 * Feature: std method for call arguments : OK
 * Feature: able to analyze sub-flow-graph: OK
 * Feature: specialized wrt each activity : OK obviously only for TOPDOWN_2
 * </pre>
 */
public final class ADActivityAnalyzer extends DataFlowAnalyzer {

    /**
     * Constant that codes for the 1st cgPhase.
     */
    private static final int BOTTOMUP_1 = 1;
    /**
     * Constant that codes for the 2nd cgPhase.
     */
    private static final int TOPDOWN_2 = 2;
    /**
     * Constant that codes for the VARIED phase.
     */
    private static final int VARIED = 1;
    /**
     * Constant that codes for the USEFUL phase.
     */
    private static final int USEFUL = 2;
    /**
     * Constant that codes for the STATICACTIVITY phase.
     */
    private static final int STATICACTIVITY = 3;
    /**
     * Index for the Varied-on-Call info.
     */
    private static final int VARIED_ON_CALL = 0;
    /**
     * Index for the Useful-on-Exit info.
     */
    private static final int USEFUL_ON_EXIT = 1;
    /**
     * Index for the static activity info, constant all through the Unit.
     */
    private static final int STATIC_ACTIVE = 2;
    /**
     * Index for the Useful(_on_exit) on the calling side.
     */
    private static final int NONZERO_ADJOINT_COMING = 3;
    /**
     * For each Block in the currently analyzed Unit, the BoolMatrix dependence matrix just
     * before the Block, wrt the initial values of the zones at the beginning of the current Unit.
     */
    private BlockStorage<BoolMatrix> depsIn;
    /**
     * For each Block in the currently analyzed Unit, the BoolMatrix dependence matrix just
     * after the Block, wrt the initial values of the zones at the beginning of the current Unit.
     */
    private BlockStorage<BoolMatrix> depsOut;
    /**
     * For each Block in the currently analyzed Unit, the BoolMatrix dependence matrix just
     * after the Block, wrt the values of the zones at the beginning of the Block.
     */
    private BlockStorage<BoolMatrix> depsThrough;
    /**
     * Temporary dependency matrices during propagation.
     */
    private BoolMatrix tmpDep;
    /**
     * zonesOfVarsHaveDiff info for the current Unit.
     * This info is maybe obsolete, only used by old AdolC mode ?
     */
    private BoolVector curUnitZonesOfVarsHaveDiff;
    /**
     * Current infos on the downstream end of each Block.
     * Used during VARIED or USEFUL phase through a given Unit.
     */
    private BlockStorage<BoolVector> infosDown;
    /**
     * Same as infosDown, but for "cycling" info.
     */
    private BlockStorage<BoolVector> infosCycleDown;
    /**
     * Current infos on the upstream end of each Block.
     * Used during VARIED or USEFUL phase through a given Unit.
     */
    private BlockStorage<BoolVector> infosUp;
    /**
     * Same as infosUp, but for "cycling" info.
     */
    private BlockStorage<BoolVector> infosCycleUp;
    /**
     * Current info being propagated through a Block by the current analysis.
     */
    private BoolVector infoTmp;
    /**
     * Same as "infoTmp", but for "cycling" info.
     */
    private BoolVector infoTmpCycle;
    /**
     * Info that will be accumulated where control flow merges.
     */
    private BoolVector additionalInfo;
    /**
     * The static activity through curUnit, i.e. the zones that are active
     * but not modified nor used during interpretation of curUnit.
     */
    private BoolVector curStaticActivity;
    /**
     * The root differentiation Units, along with their respective
     * independent and dependent zones.
     */
    private final TapList<DiffRoot> diffRoots;
    /**
     * The kind of zones that are currently differentiated. Usually, it is
     * REALKIND, but in a very special mode, it can be ALLKIND!...
     */
    private final int diffKind;
    /**
     * For the current Block, number of declared "diffKind" zones.
     */
    private int nDRZ = -9;
    /**
     * For the current Unit, number of declared "diffKind" zones at the EntryBlock.
     */
    private int nUDRZ = -9;
    /**
     * For each Unit, its AD dependency matrix.
     */
    private final UnitStorage<BoolMatrix> dependencies;
    /**
     * Current phase of the analysis at the call graph level.
     * Can be BOTTOMUP_1 or TOPDOWN_2.
     */
    private int cgPhase;
    /**
     * Current phase of the analysis at the flow graph level.
     * Can be VARIED or USEFUL.
     */
    private int fgPhase;
    /**
     * Global that holds the initial and cleaned
     * Varied-upon-Call and Useful-on-Exit infos for
     * the current unit's forward and backward analyses.
     */
    private BoolVector[] filteredContext = new BoolVector[2];
    /**
     * Global that holds the result of either the
     * VARIED analysis or the USEFUL analysis
     * for the current analyzed Unit.
     */
    private BlockStorage<TapList<BoolVector>> curUnitInfos;
    /**
     * Global that holds, for the presently analyzed calling unit, the
     * A-list of, for each called Unit, the various contexts in which
     * this called unit is called. This global is a buffer: at the end
     * of the analysis of the calling unit, this buffer is accumulated
     * into the calling contexts of the called units, and then emptied.
     */
    private TapList<TapPair<Unit, TapList<TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>>>>> callContextsBuffer = null;
    /**
     * Global that holds the varied upon frontier entry,
     * specifically for activity analysis on a Unit fragment.
     */
    private BoolVector entryFrontierVariedZones;
    /**
     * Global that holds the useful upon frontier exit,
     * specifically for activity analysis on a Unit fragment.
     */
    private BoolVector exitFrontierUsefulZones;

    /**
     * Creates an ADActivityAnalyzer, given the CallGraph "cg"
     * on which it will work.
     */
    ADActivityAnalyzer(CallGraph cg, TapList<DiffRoot> diffRoots, int diffKind) {
        super(cg, "AD activity analysis", TapEnv.traceActivity());
        this.conservativeValue = true;
        this.diffRoots = diffRoots;
        this.diffKind = diffKind;
        Unit unit;
        this.dependencies = new UnitStorage<>(cg);
        // Recupere des dependences connues et les integre a l'analyse avant de lancer celle-ci:
        for (TapList<Unit> allUnits = cg.sortedUnits(); allUnits != null; allUnits = allUnits.tail) {
            unit = allUnits.head;
            BoolMatrix unitDeps = unit.unitADDependencies ;
            if (unitDeps!=null) {
                unitDeps = unit.focusToKind(unitDeps, diffKind) ;
            }
            this.dependencies.store(unit, unitDeps);
        }
    }

    /**
     * @return the activity info on the entry arrows plus the usefulness info on the exit arrows.
     */
    public static TapPair<TapList<BoolVector>, TapList<BoolVector>> saveFrontierActivity(TapList<FGArrow> entryArrows,
                                                                                         TapList<FGArrow> exitArrows,
                                                                                         ActivityPattern curActivity) {
        TapPair<TapList<BoolVector>, TapList<BoolVector>> result = new TapPair<>(null, null);
        Block block;
        TapList<BoolVector> hdResult = new TapList<>(null, null);
        TapList<BoolVector> tlResult = hdResult;
        while (entryArrows != null) {
            block = entryArrows.head.origin;
            BoolVector entryActiv = TapList.last(curActivity.activities().retrieve(block));
            tlResult = tlResult.placdl(entryActiv);
            entryArrows = entryArrows.tail;
        }
        result.first = hdResult.tail;
        hdResult.tail = null;
        tlResult = hdResult;
        while (exitArrows != null) {
            block = exitArrows.head.destination;
            BoolVector exitUseful = curActivity.usefulnesses().retrieve(block).head;
            tlResult = tlResult.placdl(exitUseful);
            exitArrows = exitArrows.tail;
        }
        result.second = hdResult.tail;
        return result;
    }

    /**
     * Main trigger method. Runs activity analysis analysis under the rootUnits.
     */
    public static void runAnalysis(CallGraph callGraph,
                                   TapList<DiffRoot> diffRoots, int diffKind) {
        TapEnv.setADActivityAnalyzer(new ADActivityAnalyzer(callGraph, diffRoots, diffKind));
        if (TapEnv.doActivity()) {
            TapEnv.adActivityAnalyzer().run(DiffRoot.collectUnits(diffRoots));
        } else {
            TapEnv.adActivityAnalyzer().runNoActivity(DiffRoot.collectUnits(diffRoots));
        }
    }

    protected static boolean hasOneAnnotatedActive(ActivityPattern curActivity, Tree ioArg, SymbolTable symbolTable) {
        boolean result;
        if (ILUtils.isAVarRef(ioArg, symbolTable)) {
            result = isAnnotatedActive(curActivity, ioArg, symbolTable);
        } else if (ioArg.opCode() == ILLang.op_iterativeVariableRef) {
            result = hasOneAnnotatedActive(curActivity, ioArg.down(1), symbolTable);
        } else if (ioArg.opCode() == ILLang.op_expressions) {
            boolean oneActive = false;
            Tree[] sons = ioArg.children();
            for (int i = sons.length - 1; !oneActive && i >= 0; --i) {
                oneActive = hasOneAnnotatedActive(curActivity, sons[i], symbolTable);
            }
            result = oneActive;
        } else {
            result = false;
        }
        return result;
    }

    /**
     * Sets the annotation that tells that this subtree is active.
     */
    public static void setAnnotatedActive(ActivityPattern pattern, Tree tree) {
        if (tree != null) {
            ActivityPattern.setAnnotationForActivityPattern(tree, pattern, "ActiveExpr", Boolean.TRUE);
        }
    }

    /**
     * @return true if the annotation "ActiveExpr" is present,
     * which means that this "tree" returns or sets an active value.
     */
    public static boolean isAnnotatedActive(ActivityPattern pattern, Tree tree, SymbolTable symbolTable) {
        if (tree == null) {
            return false;
        }
        if (symbolTable == null || TapEnv.doActivity()) {
            // symbolTable may be null when this method is called from ILUtils.toString().
            // (ILUtils.toString uses isAnnotatedActive() to write a mark on annotated-active trees)
            if (tree.opCode() == ILLang.op_call) {
                // [llh 29/7/15] Danger: this is mixing activity of function and activity of its result !
                return isAnnotatedActive(pattern, ILUtils.getCalledName(tree));
            } else {
                return isAnnotatedActive(pattern, tree);
            }
        } else if (tree.opCode() == ILLang.op_call) {
            // When "no-activity", a Unit is active if its types and InOut allow a nonempty ActivityPattern:
            Unit calledUnit = DataFlowAnalyzer.getCalledUnit(tree, symbolTable);
            if (calledUnit.activityPatterns != null) {
                // use the "noActivity" ActivityPattern built by ADActivityAnalyzer when '-nooptim activity':
                ActivityPattern noActivityPattern = calledUnit.activityPatterns.head;
                return noActivityPattern.isActive();
            } else {
                // very weird recovery case, cf set02/v124
                if (!TapEnv.isDistribVersion()) {
                    TapEnv.toolError("Recovery case: -nooptim activity but no default ActivityPattern set for "
                            + calledUnit + ", should not happen");
                }
                boolean oneDifferentiableParam = false;
                ZoneInfo zoneInfo;
                for (int i = calledUnit.paramElemsNb() - 1; !oneDifferentiableParam && i >= 0; --i) {
                    zoneInfo = calledUnit.paramElemZoneInfo(i);
                    if (zoneInfo.realZoneNb != -1) {
                        oneDifferentiableParam = true;
                    }
                }
                return oneDifferentiableParam;
            }
        } else {
            WrapperTypeSpec exprType = symbolTable.typeOf(tree);
            return exprType == null || exprType.wrappedType == null ||
                    TypeSpec.isDifferentiableType(exprType.wrappedType);
        }
    }

    private static boolean isAnnotatedActive(ActivityPattern pattern, Tree tree) {
        if (tree == null) {
            return false;
        }
        Object annotationActiveExpr = ActivityPattern.getAnnotationForActivityPattern(tree, pattern, "ActiveExpr");
        return annotationActiveExpr != null && (Boolean) annotationActiveExpr;
    }

    public static boolean oneSymbolDeclIsActive(TapList<SymbolDecl> symbolDecls) {
        boolean oneIsActive = false;
        while (symbolDecls != null && !oneIsActive) {
            oneIsActive = symbolDecls.head != null && symbolDecls.head.isActiveSymbolDecl();
            symbolDecls = symbolDecls.tail;
        }
        return oneIsActive;
    }

    //TODO: try simplify by just checking that zoneInfo.kindZoneNb(diffkind) != -1
    private static boolean isDifferentiableZoneInfo(ZoneInfo zoneInfo, int diffKind, boolean diffConstants) {
        if (zoneInfo.ownerSymbolDecl != null) {
            return zoneInfo.ownerSymbolDecl.isDifferentiableSymbolDecl(diffKind, diffConstants);
        } else {
            WrapperTypeSpec typeSpec = zoneInfo.type;
            return typeSpec == null
                    || typeSpec.wrappedType == null
                    || TypeSpec.isDifferentiableType(typeSpec.wrappedType);
        }
    }

    protected static BoolVector buildDefaultIntrinsicCallActivity(Unit intrinsicUnit, int diffKind) {
        BoolVector result;
        BoolVector unitR = intrinsicUnit.unitInOutPossiblyR();

        if (intrinsicUnit.hasParamElemsInfo()) {
            boolean isDifferentiable;
            int n = intrinsicUnit.paramElemsNb();
            result = new BoolVector(n);

            for (int i = 0; i < n; i++) {
                isDifferentiable = (intrinsicUnit.paramElemZoneInfo(i).kindZoneNb(diffKind) != -1) ;

                if (isDifferentiable && unitR.get(i)) {
                    result.set(i, true);
                }
            }
        } else {
            result = new BoolVector(0);
        }

        return result;
    }

    /**
     * @return the ordered TapIntList of public zone numbers of "unit"
     * (i.e. indexes in the externalShape of "unit") that correspond to
     * all variables designated by the list of identifier paths "varPathNames".
     * This search is done in the pointer context of the Block "location".
     * Also fills the (tail of the) list tlPathZones with the public zones for
     * each of the identifiers in varPathNames, in the same order.
     */
    private static TapIntList buildRootVarZones(TapList<TapList<String>> varPathNames, Unit unit,
                                                TapList<TapIntList> tlPathZones, Block location, int diffKind) {
        TapIntList resultZones = null;
        SymbolTable curST;
        Unit curUnit;
        TapList curZones;
        TypeSpec curType;
        int curTypeKind;
        String curPath;
        SymbolDecl symbolDecl;
        String curString;
        TapList<String> varPathName;
        while (varPathNames != null) {
            varPathName = varPathNames.head;
            curST = unit.privateSymbolTable();
            if (curST == null) {
                curST = unit.publicSymbolTable();
            }
            curUnit = unit;
            curZones = null;
            curType = null;
            curTypeKind = SymbolTableConstants.FUNCTIONTYPE;
            curPath = null;
            // First phase: compute curZones, the TapList-tree of zones
            //  designated by this varPathName, in the context of curUnit,
            //  the Unit in which the varPathName designates it:
            while (varPathName != null) {
                curPath = curPath != null ? curPath + "." : "";
                curString = varPathName.head;
                if (!curST.isCaseDependent()) {
                    curString = curString.toLowerCase();
                }
                if (curTypeKind == SymbolTableConstants.FUNCTIONTYPE) {
                    symbolDecl = curST.getSymbolDecl(curString);
                    if (symbolDecl == null) {
                        TapEnv.commandWarning(-1, curPath + curString + " is not defined for procedure " + unit.name());
                        curPath = curPath + DiffRoot.rebuildIdentifier(varPathName);
                        varPathName = null; //exit from the while !
                    } else {
                        switch (symbolDecl.kind()) {
                            case SymbolTableConstants.FUNCTION:
                                curUnit = ((FunctionDecl) symbolDecl).unit();
                                if (curUnit.isOutside()) {
                                    TapEnv.commandWarning(-1, curPath + curString + " is not defined for procedure " + unit.name());
                                    varPathName = null;
                                }
                                curTypeKind = SymbolTableConstants.FUNCTIONTYPE;
                                curST = curUnit.privateSymbolTable();
                                if (curST == null) {
                                    curST = curUnit.publicSymbolTable();
                                }
                                curPath = curPath + curString;
                                break;
                            case SymbolTableConstants.VARIABLE:
                                curZones = symbolDecl.zones();
                                curType = symbolDecl.type().wrappedType;
                                if (curType != null) {
                                    curTypeKind = curType.kind();
                                } else {
                                    curTypeKind = SymbolTableConstants.PRIMITIVETYPE;
                                }
                                curPath = curPath + curString;
                                break;
                            default:
                                TapEnv.commandWarning(-1, curPath + curString + " is not a variable for procedure " + unit.name());
                                curPath = curPath + DiffRoot.rebuildIdentifier(varPathName);
                                varPathName = null; //exit from the while !
                        }
                    }
                } else {
                    if (curTypeKind != SymbolTableConstants.COMPOSITETYPE) {
                        TapEnv.commandWarning(-1, "Variable " + curPath + " for procedure " + unit.name() + " is not of structured type");
                        curPath = curPath + DiffRoot.rebuildIdentifier(varPathName);
                        varPathName = null; //exit from the while !
                    } else {
                        CompositeTypeSpec recordType = (CompositeTypeSpec) curType;
                        assert recordType != null;
                        int fieldRank = recordType.namedFieldRank(curString);
                        if (fieldRank < 0) {
                            TapEnv.commandWarning(-1, "No component named " + curString + " in variable " + curPath + " for procedure " + unit.name());
                            curPath = curPath + DiffRoot.rebuildIdentifier(varPathName);
                            varPathName = null; //exit from the while !
                        } else {
                            curType = recordType.fields[fieldRank].type().wrappedType;
                            curTypeKind = curType.kind();
                            while (curZones.head instanceof TapIntList) {
                                curZones = curZones.tail;
                            }
                            Object subList = TapList.nth(curZones, fieldRank);
                            if (subList instanceof TapList) {
                                curZones = (TapList) subList;
                            } else {
                                // Weird case occurring when the command-line active var is wrong.
                                // cf bug report Harmen.van.der.Ven@nlr.nl 16Oct2019.
                                TapEnv.commandWarning(-1, "Could not analyze component named " + curString + " in variable " + curPath + " for procedure " + unit.name());
                                curPath = curPath + DiffRoot.rebuildIdentifier(varPathName);
                                //exit from the while !
                                varPathName = null;
                            }
                            curPath = curPath + curString;
                        }
                    }
                }
                // Modify curZones to make it dive into pointers
                // (because we don't mention pointers in the spec of (in)dependent variables):
                while (curTypeKind == SymbolTableConstants.ARRAYTYPE
                       || curTypeKind == SymbolTableConstants.MODIFIEDTYPE
                       || curTypeKind == SymbolTableConstants.POINTERTYPE) {
                    if (curTypeKind == SymbolTableConstants.POINTERTYPE) {
                        assert curType != null;
                        // Find the zones of the destination:
                        curZones = curUnit.publicSymbolTable().
                                getPointerDestinationZonesTreeHere(
                                        ZoneInfo.listAllZones(curZones, true),
                                        null, location, null,
                                        ((PointerTypeSpec) curType).destinationType, null, true);
                        curType = ((PointerTypeSpec) curType).destinationType.wrappedType;
                    } else {
                        assert curType != null;
                        curType = curType.baseTypeSpec(true).wrappedType;
                    }
                    assert curType != null;
                    curTypeKind = curType.kind();
                }
                if (varPathName != null) {
                    varPathName = varPathName.tail;
                }
            }
            // Second phase: from curZones, find public zones of the same zones, in the
            //  context of  the current differentiation root "unit".
            //  Add these public zones into the final result (provided they are differentiable)
            curZones = TapList.copyTree(curZones);
            // The Instruction at which pointer unrolling must be made:
            Instruction locInstr;
            if (location.rank == -1) {
                // EntryBlock
                FGArrow entryArrow = location.getFGArrowTestCase(FGConstants.ENTRY, FGConstants.MAIN);
                locInstr = entryArrow == null ? null : entryArrow.destination.headInstr();
            } else {
                locInstr = location.headInstr();
            }
            if (curST != null) {
                includePointedElementsInTree(curZones, null, null, true, curST, locInstr, false, true);
            }
            TapIntList zonesInDefUnit = ZoneInfo.listAllZones(curZones, true);
            if (zonesInDefUnit == null) {
                TapEnv.commandWarning(-1, curPath + " is not a variable for procedure " + unit.name());
            } else {
                // Keep only the zones that are public outside curUnit,
                // and also don't keep "alloc" zones.
                zonesInDefUnit = keepOnlyPublicZones(curUnit, zonesInDefUnit);
                TapIntList zonesInRootUnit;
                if (curUnit == unit) {
                    zonesInRootUnit = zonesInDefUnit;
                } else {
                    //If these zones were designated in another Unit than the current root Unit,
                    // then try and translate them into published zones of the root Unit.
                    // Discard the zones for which no correspondence was found.
                    zonesInRootUnit = curUnit.translateToCallerZones(zonesInDefUnit, unit);
                }
                zonesInRootUnit = TapIntList.remove(zonesInRootUnit, curUnit.callGraph().zoneNbOfNullDest) ;
                zonesInRootUnit = TapIntList.remove(zonesInRootUnit, curUnit.callGraph().zoneNbOfUnknownDest) ;
                if (zonesInRootUnit == null) {
                    TapEnv.commandWarning(-1, curPath + " is not a visible argument at "+(location==curUnit.entryBlock() ? "entry" : "exit")+" of procedure " + unit.name());
                } else {
                    //Keep only the zones which actually correspond to a variable of differentiable type:
                    TapIntList diffKindZonesInRootUnit = unit.focusToKind(zonesInRootUnit, diffKind);
                    if (diffKindZonesInRootUnit == null) {
                        TapEnv.commandWarning(-1, curPath + " is not of a differentiable type in procedure " + unit.name());
                    } else {
                        //Add the resulting zones into the final list of (in)dependent
                        // public zones for the current root Unit:
                        tlPathZones = tlPathZones.placdl(diffKindZonesInRootUnit);
                        while (diffKindZonesInRootUnit != null) {
                            resultZones =
                                    TapIntList.addIntoSorted(resultZones, diffKindZonesInRootUnit.head);
                            diffKindZonesInRootUnit = diffKindZonesInRootUnit.tail;
                        }
                    }
                }
            }
            varPathNames = varPathNames.tail;
        }
        return resultZones;
    }

    /**
     * Translate a TapIntList of declared zones indices for Unit "refUnit" into
     * the TapIntList of corresponding published zone numbers for refUnit.
     * Zones which are local to refUnit are not public, and are therefore discarded.
     */
    private static TapIntList keepOnlyPublicZones(Unit refUnit, TapIntList declaredZones) {
        TapIntList hdPublicZones = new TapIntList(-1, null);
        TapIntList tlPublicZones = hdPublicZones;
        int publicZone;
        int firstLocalIndex = refUnit.publicZonesNumber4[SymbolTableConstants.ALLKIND] ;
        while (declaredZones!=null) {
            if (declaredZones.head<firstLocalIndex) {
                ZoneInfo zi =
                    refUnit.publicSymbolTable().declaredZoneInfo(declaredZones.head, SymbolTableConstants.ALLKIND) ;
                tlPublicZones = tlPublicZones.placdl(declaredZones.head);
            }
            declaredZones = declaredZones.tail;
        }
        return hdPublicZones.tail;
    }

    /**
     * @param zones the list of ZoneInfo's to look from.
     * @return true iff one of the "zones" is known to have a differentiated variable,
     * i.e. is active at some time.
     */
    public static boolean haveDifferentiatedVariable(TapList<ZoneInfo> zones) {
        boolean haveDiff = false;
        ZoneInfo zi;
        while (!haveDiff && zones != null) {
            zi = zones.head;
            haveDiff = zi.isOnceActive() || zi.from != null && zi.from.isOnceActive();
            zones = zones.tail;
        }
        return haveDiff;
    }

    /**
     * For a given activity pattern, finds whether each formal argument of
     * the concerned Unit has a derivative or not.
     * @param pattern the given activity pattern
     * @return an array of booleans, one for each formal argument plus a
     * last one for the function's result, where the boolean of rank k (0 to nbArgs)
     * is true iff the (1+k)'th argument has a derivative argument,
     * either because an input derivative is expected, or an output derivative
     * will be returned, or both.
     * Here, a variable's derivative is expected either if the variable is an "active" value
     * or if it is a ReqX or AvlX pointer
     * Special case for the function's result, which is never expected as an input.
     */
    public static boolean[] formalArgsActivity(ActivityPattern pattern) {
        Unit unit = (pattern==null ? null : pattern.unit()) ;
        if (unit==null || unit.functionTypeSpec()==null || unit.functionTypeSpec().argumentsTypes==null) {
            return null;
        }
        FunctionTypeSpec functionTypeSpec = unit.functionTypeSpec();
        int nbArgs = functionTypeSpec.argumentsTypes.length;
        // Note: functionDiffParamsRequired is a BoolVector on ALLKIND zones.
        BoolVector functionDiffParamsRequired = functionDiffFormalRequired(pattern, false);
        boolean[] resultActivity = new boolean[nbArgs + 1];
        ZoneInfo zoneInfo;
        WrapperTypeSpec argTypeSpec;
        for (int i = resultActivity.length - 1; i >= 0; --i) {
            resultActivity[i] = false;
        }
        if (unit.hasParamElemsInfo()) {
            int shapeLength = unit.paramElemsNb();
            for (int i = shapeLength - 1; i >= 0; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                if (zoneInfo != null) {
                    switch (zoneInfo.kind()) {
                        case SymbolTableConstants.PARAMETER:
                            // protect against functions called with a wrong argument number
                            // and don't look for activity if argument was already found active.
                            if (zoneInfo.index-1 < nbArgs && !resultActivity[zoneInfo.index-1]) {
                                argTypeSpec = functionTypeSpec.argumentsTypes[zoneInfo.index-1];
                                if (!TapEnv.doActivity()) {
                                    if (argTypeSpec != null && TypeSpec.isDifferentiableType(argTypeSpec.wrappedType)) {
                                        resultActivity[zoneInfo.index-1] = true;
                                    }
                                } else {
                                    if (zoneInfo.ptrZoneNb != -1 && unit.exitBlock() != null) {
                                        //This elem arg is a pointer:
                                        if (functionDiffParamsRequired.get(i)) {
                                            resultActivity[zoneInfo.index-1] = true;
                                        }
                                    } else if (isActiveArg(i, functionDiffParamsRequired, argTypeSpec)) {
                                        resultActivity[zoneInfo.index-1] = true;
                                    }
                                }
                                if (!resultActivity[zoneInfo.index-1] &&
                                        ReqExplicit.isPointerActiveArg(pattern, i)) {
                                    //[llh] TODO:blend this activity with pointer activity (i.e. "Required Explicit").
                                    resultActivity[zoneInfo.index-1] = true;
                                }
                            }
                            break;
                        case SymbolTableConstants.RESULT:
                            // Don't look for activity if result was already found active:
                            if (!resultActivity[nbArgs]) {
                                argTypeSpec = functionTypeSpec.returnType;
                                if (!TapEnv.doActivity()) {
                                    if (argTypeSpec != null && TypeSpec.isDifferentiableType(argTypeSpec.wrappedType)) {
                                        resultActivity[nbArgs] = true;
                                    }
                                } else {
                                    if (zoneInfo.ptrZoneNb != -1 && unit.exitBlock() != null && unit.pointerEffect != null) {
                                        //This elem-result is a pointer:
                                        if (functionDiffParamsRequired.get(i)) {
                                            resultActivity[nbArgs] = true;
                                        }
                                    } else if (isActiveArg(i, functionDiffParamsRequired, argTypeSpec)) {
                                        resultActivity[nbArgs] = true;
                                    }
                                }
                                if (!resultActivity[nbArgs] &&
                                        ReqExplicit.isPointerActiveArg(pattern, i)) {
                                    //[llh] TODO:blend this activity with pointer activity (i.e. "Required Explicit").
                                    resultActivity[nbArgs] = true;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
// if (unit!=null && "intersect".equals(unit.name())) {
// System.out.println("Computed formalArgsActivity for unit "+pattern+" :") ;
// System.out.println("functionDiffParamsRequired:"+functionDiffParamsRequired) ;
// System.out.print(" ====> [") ;
// for (int i=0 ; i<nbArgs ; ++i) System.out.print(" "+resultActivity[i]) ;
// System.out.print(" -> "+resultActivity[nbArgs]+"]") ;
// System.out.println() ;
// System.out.println("-------------------------------------") ;
// }
        return resultActivity;
    }

    /**
     * Returns the info whether the arguments of a Unit require a differentiated counterpart,
     * for the given activity pattern of this Unit. The info is expressed in terms of each elementary argument
     * of the Unit, i.e. as a BoolVector following the Unit's public shape.
     * An elementary argument "arg" requires a differentiated counterpart when
     * either this arg is active upon entry or exit of this unit, or because
     * (a) (it is relatively hard to declare locally (e.g. dynamic sizes in Fortran)
     *      OR its derivative is already available at (one of) the call sites)
     * AND
     * (b) (it becomes active somewhere inside unit,
     *      OR the memory location of its differentiated counterpart will be needed
     *         by a (to-be-active) pointer somewhere inside unit).
     *
     * @param pattern      an activity pattern of this unit
     * @param withPointers if true, count also zones that have a diff because of ReqX analysis.
     * @return the unit's zones (of kind ALLKIND) that require a differentiated counterpart,
     */
    public static BoolVector functionDiffFormalRequired(ActivityPattern pattern, boolean withPointers) {
        Unit unit = pattern.unit();
        SymbolTable unitSymbolTable = unit.publicSymbolTable();
        BoolVector functionDiffParamsRequired = null;
        if (unitSymbolTable!=null && unit.externalShape!=null) {
            functionDiffParamsRequired = unitSymbolTable.getRequiredDiffVars(pattern);
            if (functionDiffParamsRequired!=null) functionDiffParamsRequired = functionDiffParamsRequired.copy() ;
            BoolVector patternZonesOfVarsHaveDiff = pattern.zonesOfVarsHaveDiff();
            if (patternZonesOfVarsHaveDiff != null) {
                if (functionDiffParamsRequired == null) {
                    functionDiffParamsRequired = patternZonesOfVarsHaveDiff;
                } else {
                    functionDiffParamsRequired.cumulOr(patternZonesOfVarsHaveDiff);
                }
            }
            if (functionDiffParamsRequired != null) {
                functionDiffParamsRequired = unit.unfocusFromKind(functionDiffParamsRequired, TapEnv.diffKind()) ;
                removeEasyLocalDeclaredAndUnavailableOutside(functionDiffParamsRequired, unit);
            } else {
                functionDiffParamsRequired = new BoolVector(unit.externalShape.length);
            }
        }

        BoolVector callActivityUnfocused = pattern.callActivity() ;
        BoolVector exitActivityUnfocused = pattern.exitActivity() ;
        callActivityUnfocused = unit.unfocusFromKind(callActivityUnfocused, TapEnv.diffKind()) ;
        exitActivityUnfocused = unit.unfocusFromKind(exitActivityUnfocused, TapEnv.diffKind()) ;
        // [llh 27Aug2020] The following adds into functionDiffParamsRequired the public zones of pointer formal args
        // that are ReqX either upon entry or upon exit. This is maybe redundant with the following "formalsWithDiff" part
        // and with the part on pointers in this.formalArgsActivity().
        if (unit.hasParamElemsInfo() && TapEnv.doActivity() && functionDiffParamsRequired != null) {
            BoolVector functionZonesWithDiffOnCall = callActivityUnfocused;
            BoolVector possiblyW = unit.unitInOutPossiblyW() ;
            BoolVector functionZonesWritten =
                (possiblyW==null ? null :unit.unitInOutPossiblyW());
            if (pattern.entryReqX() != null) {
                BoolVector reqXAndRead = pattern.entryReqX();
                // Attention: we would like to find those zones that are entry reqX and read in the call
                // However, in Fortran, we don't distinguish arguments and their reference (cf set03/cm09)
                // so addresses may be used and not marked read! Therefore we must ignore functionZonesRead.
                if (unit.isC()) {
                    BoolVector functionZonesRead = unit.unitInOutPossiblyR();
                    reqXAndRead = reqXAndRead.and(functionZonesRead);
                }
                functionZonesWithDiffOnCall = functionZonesWithDiffOnCall.or(reqXAndRead);
            }
            if (pattern.entryAvlX() != null) {
                functionZonesWithDiffOnCall = functionZonesWithDiffOnCall.or(pattern.entryAvlX().and(functionZonesWritten));
            }
            BoolVector functionZonesWithDiffOnExit = exitActivityUnfocused;
            if (pattern.exitReqX() != null) {
                functionZonesWithDiffOnExit = functionZonesWithDiffOnExit.or(pattern.exitReqX().and(functionZonesWritten));
            }
            if (pattern.exitAvlX() != null) {
                functionZonesWithDiffOnExit = functionZonesWithDiffOnExit.or(pattern.exitAvlX().and(functionZonesWritten));
            }
            int shapeLengthPtr = unit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
            ZoneInfo zoneInfo;
            for (int i=unit.paramElemsNb()-1 ; i>=0 ; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                if (zoneInfo != null) {
                    switch (zoneInfo.kind()) {
                        case SymbolTableConstants.PARAMETER:
                            if (zoneInfo.ptrZoneNb != -1 && unit.exitBlock() != null) {
                                //This elem arg is a pointer:
                                TapIntList entryDestsList = ZoneInfo.listAllZones(zoneInfo.targetZonesTree, true);
                                if (functionZonesWithDiffOnCall != null && functionZonesWithDiffOnCall.intersects(entryDestsList)) {
                                    //If this pointer elem arg points upon entry to a zone
                                    // which is active upon entry, then this (part of) formal arg has a diff:
                                    functionDiffParamsRequired.set(i, true);
                                } else if (unit.pointerEffect != null) {
                                    //Else if this pointer elem arg points upon exit to a zone
                                    // which is active upon exit, then this (part of) formal arg has a diff:
                                    TapIntList destsList = new TapIntList(-1, null);
                                    BoolVector dejaVuVector = new BoolVector(shapeLengthPtr);
                                    followExitPointers(zoneInfo, zoneInfo.ptrZoneNb, destsList, unit, dejaVuVector, shapeLengthPtr);
                                    if (functionZonesWithDiffOnExit != null && functionZonesWithDiffOnExit.intersects(destsList.tail)) {
                                        functionDiffParamsRequired.set(i, true);
                                    }
                                }
                            }
                            break;
                        case SymbolTableConstants.RESULT:
                            if (zoneInfo.ptrZoneNb != -1 && unit.exitBlock() != null && unit.pointerEffect != null) {
                                //This elem-result is a pointer: if it points upon exit to a zone
                                // which is active upon exit, then (this part of) the result has a diff:
                                TapIntList destsList = new TapIntList(-1, null);
                                BoolVector dejaVuVector = new BoolVector(shapeLengthPtr);
                                followExitPointers(zoneInfo, zoneInfo.ptrZoneNb, destsList, unit, dejaVuVector, shapeLengthPtr);
                                if (functionZonesWithDiffOnExit != null && functionZonesWithDiffOnExit.intersects(destsList.tail)) {
                                    functionDiffParamsRequired.set(i, true);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        BoolVector formalsWithDiff = callActivityUnfocused.or(exitActivityUnfocused);
        if (withPointers) {
            if (pattern.entryReqX() != null) {
                formalsWithDiff.cumulOr(pattern.entryReqX());
            }
            if (pattern.exitReqX() != null) {
                formalsWithDiff.cumulOr(pattern.exitReqX());
            }
            if (pattern.entryAvlX() != null) {
                formalsWithDiff.cumulOr(pattern.entryAvlX());
            }
            if (pattern.exitAvlX() != null) {
                formalsWithDiff.cumulOr(pattern.exitAvlX());
            }
        }
        functionDiffParamsRequired = (functionDiffParamsRequired==null
                                      ? formalsWithDiff
                                      : functionDiffParamsRequired.or(formalsWithDiff));
        return functionDiffParamsRequired;
    }

    private static void removeEasyLocalDeclaredAndUnavailableOutside(BoolVector requiredDiffParams, Unit unit) {
        FunctionTypeSpec functionTypeSpec = unit.functionTypeSpec();
        if (unit.hasParamElemsInfo() && functionTypeSpec != null) {
            ZoneInfo zoneInfo;
            for (int i = unit.paramElemsNb() - 1; i >= 0; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                if (zoneInfo!=null
                    && (zoneInfo.isCommon()
                        ||
                        (zoneInfo.isParameter()
                         && (zoneInfo.index <= functionTypeSpec.argumentsTypes.length)
                         && (functionTypeSpec.argumentsTypes[zoneInfo.index-1] != null)
                         && functionTypeSpec.argumentsTypes[zoneInfo.index-1].easilyDeclared())
                        )) {
                    requiredDiffParams.set(i, false);
                }
            }
        }
    }

    /**
     * Accumulates into result the list of the public ranks of all zones that may be accessed through
     * the pointer zone "zoneInfo", by following all possible pointer destinations
     * at the time of exit from the given "unit". These destinations at the time of exit are
     * found in unit.pointerEffect. This method goes recursively through chains of pointers
     * till the leaf zones that are not pointers.
     */
    private static void followExitPointers(ZoneInfo zoneInfo, int indexPtr, TapIntList result, Unit unit, BoolVector dejaVuVector, int dejaVuLength) {
        if (indexPtr < dejaVuLength && !dejaVuVector.get(indexPtr)) {
            dejaVuVector.set(indexPtr, true);
            BoolVector exitDests = unit.pointerEffect.getRow(indexPtr);
            TapIntList exitDestsList;
            if (exitDests == null) {
                //"Identity" pointer effect: the pointer was not modified, it points to its destinations upon entry:
                exitDestsList = ZoneInfo.listAllZones(zoneInfo.targetZonesTree, true);
            } else {
                exitDestsList = exitDests.trueIndexList(unit.paramElemsNb()) ;
            }
            while (exitDestsList != null) {
                if (exitDestsList.head>=2) { // Neglect NULL and Undef
                    ZoneInfo zi  = unit.publicSymbolTable().declaredZoneInfo(exitDestsList.head,
                                                                             SymbolTableConstants.ALLKIND) ;
                    if (zi!=null && zi.ptrZoneNb!=-1) {
                        followExitPointers(zi, zi.ptrZoneNb, result, unit, dejaVuVector, dejaVuLength);
                    } else {
                        result.placdl(exitDestsList.head) ;
                    }
                }
                exitDestsList = exitDestsList.tail;
            }
        }
    }

    /**
     * Tells whether a zone is active with respect to a given activity and therefore must have a derivative.
     * Both zoneRk and activeZones nust use the same "kind" i.e. ALLKIND or diffKind 
     * When activeZones is not given, an approximate answer is built by checking that
     * the given argType of this zone is "differentiable".
     * @param zoneRk      The rank of the considered zone.
     * @param activeZones The considered activity.
     * @param argType     The type of the considered zone.
     */
    public static boolean isActiveArg(int zoneRk, BoolVector activeZones, WrapperTypeSpec argType) {
        if (activeZones==null) {
            return argType==null || TypeSpec.isDifferentiableType(argType.wrappedType); // cf F90 bd05
        } else {
            return activeZones.get(zoneRk);
        }
    }

    /**
     * @return the String containing all the variable names in "names" for which
     * every public zone is "true" in the BoolVector "property".
     */
    private static String namesWithProperty(TapList<TapList<String>> names, TapList publicZonesList,
                                            BoolVector property) {
        String result = null;
        TapIntList publicZones;
        while (publicZonesList != null) {
            publicZones = (TapIntList) publicZonesList.head;
            if (publicZones != null && property.contains(publicZones)) {
                String name = DiffRoot.rebuildIdentifier(names.head);
                if (result == null) {
                    result = name;
                } else {
                    result = result + ", " + name;
                }
            }
            publicZonesList = publicZonesList.tail;
            names = names.tail;
        }
        return result;
    }

    private static BoolMatrix buildDefaultADDependencies(Unit unit, int diffKind) {
        BoolMatrix result;
        BoolVector unitW = unit.unitInOutPossiblyW() ;
        if (unit.hasParamElemsInfo() && unitW!=null) {
            int diffKindLength = unit.publicZonesNumber(diffKind) ;
            BoolVector unitR = unit.unitInOutPossiblyR();
            BoolVector unitN = unit.unitInOutN();
            result = new BoolMatrix(diffKindLength, diffKindLength);
            result.setIdentity();
            BoolVector depLine = new BoolVector(diffKindLength);
            depLine.setFalse();
            TapList<ZoneInfo> writtenZones = null;
            ZoneInfo zi;
            int i, dki ;
            for (i=0 ; i<unit.externalShape.length ; ++i) {
                zi = unit.externalShape[i] ;
                if (zi!=null) {
                    dki = zi.kindZoneNb(diffKind) ;
                    if (dki!=-1) {
                        if (unitW.get(i)) {
                            writtenZones = new TapList<>(zi, writtenZones);
                        }
                        if (unitR.get(i)) {
                            depLine.set(dki, true);
                        }
                    }
                }
            }
            while (writtenZones != null) {
                zi = writtenZones.head;
                BoolVector depLineCopy = depLine.copy();
                // Moreover, if zone may be not-read-nor-written,
                //  then its output value depends also on its input value:
                if (unitN.get(zi.zoneNb)) {
                    depLineCopy.set(zi.kindZoneNb(diffKind), true);
                }
                result.setRow(zi.kindZoneNb(diffKind), depLineCopy);
                writtenZones = writtenZones.tail;
            }
        } else {
            // externalshape or unitW may be null for varfunction,
            // i.e. the units of functions passed as argument
            result = new BoolMatrix(0, 0);
        }
        return result;

//         BoolVector unitW = unit.unitInOutPossiblyW();
//         if (!unit.hasParamElemsInfo() || unitW == null) {
//             // externalshape or unitW may be null for varfunction,
//             // i.e. the units of functions passed as argument) :
//             return new BoolMatrix(0, 0);
//         } else {
//             int n = unit.paramElemsNb();
//             BoolVector unitR = unit.unitInOutPossiblyR();
//             BoolVector unitN = unit.getTmpALLKINDN();
//             BoolMatrix result = new BoolMatrix(n, n);
//             result.setIdentity();
//             BoolVector depLine = new BoolVector(n);
//             depLine.setFalse();
//             BoolVector depLineZero = new BoolVector(n);
//             depLineZero.setFalse();
//             TapIntList returnIndices = null;
//             ZoneInfo zi;
//             for (int i = 0; i < n; i++) {
//                 zi = unit.paramElemZoneInfo(i);
//                 if (zi != null) {
//                     if (zi.kindZoneNb(diffKind) != -1) {
//                         if (unitW.get(i)) {
//                             returnIndices = new TapIntList(i, returnIndices);
//                         }
//                         if (unitR.get(i)) {
//                             depLine.set(i, true);
//                         }
//                     } else {
//                         if (unitW.get(i)) {
//                             result.setRow(i, depLineZero);
//                         }
//                     }
//                 }
//             }
//             while (returnIndices != null) {
//                 int ri = returnIndices.head;
//                 BoolVector depLineCopy = depLine.copy();
//                 // Moreover, if zone may be not-read-nor-written,
//                 //  then its output value depends also on its input value:
//                 if (unitN.get(ri)) {
//                     depLineCopy.set(ri, true);
//                 }
//                 result.setRow(ri, depLineCopy);
//                 returnIndices = returnIndices.tail;
//             }
//             return result;
//         }

    }

    /**
     * Same as getDiffDeps(Unit), but takes the result from the
     * given UnitStorage "dependencies". Also uses the given "diffKind".
     */
    private static BoolMatrix getDiffDeps(Unit unit, UnitStorage<BoolMatrix> dependencies, int diffKind) {
        BoolMatrix result;
        if (unit.isIntrinsic() || unit.isDummy()) {
            result = unit.unitADDependencies;
            if (result!=null) {
                result = unit.focusToKind(result, diffKind) ;
            } else {
                result = buildDefaultADDependencies(unit, diffKind);
            }
        } else {
            result = dependencies.retrieve(unit);
            if (result == null &&
                    (unit.isExternal() || unit.isVarFunction() || unit.isInterface())) {
                result = buildDefaultADDependencies(unit, diffKind);
                dependencies.store(unit, result);
            }
        }
        return result;
    }

    /**
     * Prepares for the next analyzeForward() to run only a "VARIED" analysis
     * and only on a sub-flow-graph of the current Unit.
     */
    public void setPhaseVaried(BoolVector variedEntries, BlockStorage<TapList<BoolVector>> curUnitVariednesses) {
        cgPhase = TOPDOWN_2;
        fgPhase = VARIED;
        entryFrontierVariedZones = variedEntries;
        curUnitInfos = curUnitVariednesses;
    }

    /**
     * Prepares for the next analyzeBackward() to run only a "USEFUL" analysis
     * and only on a sub-flow-graph of the current Unit.
     */
    public void setPhaseUseful(BoolVector usefulExits, BlockStorage<TapList<BoolVector>> curUnitUsefulnesses) {
        cgPhase = TOPDOWN_2;
        fgPhase = USEFUL;
        exitFrontierUsefulZones = usefulExits;
        curUnitInfos = curUnitUsefulnesses;
    }

    /**
     * After a local recomputation of ADActivity on a sub-flow-graph of the current Unit, does again the merging of
     * Usefulness into Variedness to obtain activity, and also recomputes the Activity annotations.
     * Does this on all the Blocks inside the sub-flow-graph.
     *
     * @param portionBlocks The list of all Blocks inside the sub-flow-graph.
     * @param activity      The current ActivityPattern for which we have been recomputing this Unit's activity.
     */
    public void reMergeAndAnnotate(TapList<Block> portionBlocks, ActivityPattern activity) {
        Block block;
        curActivity = activity ;
        while (portionBlocks != null) {
            block = portionBlocks.head;
            TapList<BoolVector> blockUsefulnesses = activity.usefulnesses().retrieve(block);
            TapList<BoolVector> blockVariednesses = activity.activities().retrieve(block);
            TapList<BoolVector> blockU = blockUsefulnesses;
            TapList<BoolVector> blockV = blockVariednesses;
            while (blockU != null && blockV != null) {
                blockV.head.cumulAnd(blockU.head);
                blockU = blockU.tail;
                blockV = blockV.tail;
            }
            removeActivityAnnotations(block);
            assert blockVariednesses != null;
            assert blockUsefulnesses != null;
            curUnitZonesOfVarsHaveDiff = activity.zonesOfVarsHaveDiff();
            presetActivityAnnotations(block, false, blockVariednesses, blockUsefulnesses);
            portionBlocks = portionBlocks.tail;
        }
    }

    /**
     * Stub method for the no-activity option.
     */
    private void runNoActivity(TapList<Unit> rootUnits) {
        // For each unit, attach exactly one ActivityPattern, whose activity part
        // is empty, which will mean that we are in the no-activity case.
        ActivityPattern noActivityPattern;
        ZoneInfo zoneInfo;
        int dki ;
        TapList<Unit> allUnits = TapList.append(curCallGraph.sortedUnits(),
                                   TapList.append(curCallGraph.dummyUnits,
                                     curCallGraph.intrinsicUnits()));
        TapList<Unit> calleeUnits = rootUnits == null ? null : Unit.allCalleesMulti(rootUnits);
        BoolVector diffTypeZones;
        for (Unit unit : allUnits) {
            if (unit.isIntrinsic() || unit.isExternal() || unit.isInterface()
                    || unit.isStandard() && (calleeUnits == null || TapList.contains(calleeUnits, unit))) {
                DiffRoot unitDiffRoot = null;
                for (TapList<DiffRoot> inDiffRoots = diffRoots;
                     unitDiffRoot == null && inDiffRoots != null;
                     inDiffRoots = inDiffRoots.tail) {
                    if (inDiffRoots.head.unit() == unit) {
                        unitDiffRoot = inDiffRoots.head;
                    }
                }
                noActivityPattern = new ActivityPattern(unit, true, false, diffKind);
                if (unitDiffRoot != null && unitDiffRoot.diffPatterns() != null) {
                    noActivityPattern.setDiffPattern(unitDiffRoot.diffPatterns().head);
                }
                int shapeLen = unit.paramElemsNb();
                diffTypeZones = new BoolVector(shapeLen);
                for (int i = shapeLen - 1; i >= 0; --i) {
                    zoneInfo = unit.paramElemZoneInfo(i);
                    diffTypeZones.set(i, isDifferentiableZoneInfo(zoneInfo, TapEnv.diffKind(), true));
                }
                if (unit.unitInOutPossiblyW() != null) {
                    BoolVector tmpPubW = unit.unitInOutPossiblyW();
                    if (!tmpPubW.and(diffTypeZones).isFalse(shapeLen)) {
                        // ^ only if this Unit may write some differentiable zone,
                        BoolVector unitPossiblyRorW = unit.unitInOutPossiblyRorW();
                        for (int i = shapeLen - 1; i >= 0; --i) {
                            // [vmp] + isCommon, cf F77 v118
                            if ((unitPossiblyRorW.get(i) || unit.paramElemZoneInfo(i).isCommon()) && diffTypeZones.get(i)) {
                                // ^ only if this Unit may read or write this particular differentiable zone
                                zoneInfo = unit.paramElemZoneInfo(i);
                                dki = zoneInfo.kindZoneNb(TapEnv.diffKind()) ;
                                if (dki!=-1) {
                                    noActivityPattern.callActivity().set(dki, true);
                                    noActivityPattern.exitActivity().set(dki, true);
                                }
                            }
                        }
                    }
                }
                unit.activityPatterns = new TapList<>(noActivityPattern, null);
            }
        }
    }

    /**
     * Runs activity analysis from the given Unit's "rootUnits".
     * First, a bottom-up sweep builds the differentiable dependency matrices,
     * Second, a top-down sweep detects activity everywhere under rootUnits.
     *
     * @param rootUnits The list of topmost Units that must be analyzed.
     */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        String topAnalysisName = curAnalysisName;
        cgPhase = BOTTOMUP_1;
        curAnalysisName = "Bottom-up AD dependency analysis";
        tracedUnits = curCallGraph.getUnits(TapEnv.traceADDeps());
        runBottomUpAnalysis(rootUnits);
        cgPhase = TOPDOWN_2;
        curAnalysisName = "Top-down AD activity analysis";
        tracedUnits = curCallGraph.getUnits(TapEnv.traceActivity());
        runTopDownAnalysis(rootUnits);
        // Compute the activity information for the called intrinsic units:
        TapList<Unit> calledIntrinsics = TapList.append(curCallGraph.dummyUnits, curCallGraph.intrinsicUnits());
        TapList<ActivityPattern> activityPatterns;
        while (calledIntrinsics != null) {
            setCurUnitEtc(calledIntrinsics.head);
            activityPatterns = curUnit.activityPatterns;
            while (activityPatterns != null) {
                curActivity = activityPatterns.head;
                computeUnitFrontierActivities(curUnit, curActivity, curActivity.variedOnCall(), curActivity.usefulOnExit());
                activityPatterns = activityPatterns.tail;
            }
            calledIntrinsics = calledIntrinsics.tail;
        }
        // Bottom-up sweep on CallGraph, to propagate information upwards, about
        // "activity" of functions passed as arguments. TODO: Also, here would
        // be a correct place to propagate info about each array that is not
        // active, but whose declaration should be "differentiated" because
        // later, when it actually becomes active, its size has become dynamic,
        // and its differentiated array therefore cannot be declared. Also about
        // pointers which must be differentiated because the variable they point
        // to, although not active at the beginning, will become active later.
        TapList<ActivityPattern> activities;
        TapList<Unit> inUnits =
                TapList.reverse(rootUnits == null ? curCallGraph.units() : Unit.allCalleesMulti(rootUnits));
        while (inUnits != null) {
            setCurUnitEtc(inUnits.head);
            activities = curUnit.activityPatterns;
            while (activities != null) {
                curActivity = activities.head;
                if (curUnit.hasSource()) {
                    checkCalledFunctionsActivities();
                }
                activities = activities.tail;
            }
            inUnits = inUnits.tail;
        }
        inUnits = rootUnits;
        while (inUnits != null) {
            setCurUnitEtc(inUnits.head);
            activities = curUnit.activityPatterns;
            while (activities != null) {
                curActivity = activities.head;
                if (curUnit.entryBlock() != null) {
                    setCurBlockEtc(curUnit.entryBlock());
                    Tree rootCallTree = curUnit.entryBlock().headTree();
                    if (rootCallTree != null) {
                        Unit restoreCurUnit = curUnit;
                        curUnit = null;
                        checkCalledFunctionsActivitiesInTree(rootCallTree);
                        curUnit = restoreCurUnit;
                    }
                    setCurBlockEtc(null);
                }
                activities = activities.tail;
            }
            inUnits = inUnits.tail;
        }
        setCurUnitEtc(null);
        curAnalysisName = topAnalysisName;
    }

    /**
     * Set {@link #curUnit}.
     * In addition, sets nDRZ.
     */
    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        nDRZ = (curSymbolTable==null ? -9 : curSymbolTable.declaredZonesNb(diffKind));
        if (curUnit == null || curUnit.publicSymbolTable() == null) {
            nUDRZ = -9;
        } else {
            nUDRZ = curUnit.publicSymbolTable().declaredZonesNb(diffKind);
        }
    }

    /**
     * Initializes the context of Unit "unit" for this sweep on the Call-Graph.
     */
    @Override
    protected Object initializeCGForUnit() {
        if (cgPhase == BOTTOMUP_1) {
            if (curUnit.isExternal() || curUnit.isVarFunction() || curUnit.isInterface()
                    || curUnit.isRenamed() || curUnit.isModule()
                    // If a unit has no source, treat it as external (except if it is intrinsic) :
                    || !curUnit.isIntrinsic() && !curUnit.hasSource()) {
                if (curUnit.rank() > 0 && dependencies.retrieve(curUnit) == null) {
                    BoolMatrix result = buildDefaultADDependencies(curUnit, diffKind);
                    dependencies.store(curUnit, result);
                }
            } else if (!curUnit.isIntrinsic() && curUnit.rank() > 0 && dependencies.retrieve(curUnit) == null) {
                dependencies.store(curUnit, null);
            }
            return null;
        } else {  //cgPhase==TOPDOWN_2
            if (curUnit.hasParamElemsInfo()) {
                // elementary params info may be null for VarFunction
                // (the units of functions passed as argument)
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" initializing AD activity for unit: " + curUnit);
                }
                curUnit.activityPatterns = null;
                return null;
            } else {
                return null;
            }
        }
    }

    /**
     * Cleans up and initializes the context of curUnit, which is a root Unit.
     */
    @Override
    protected void initializeCGForRootUnit() {
        int shapeLength = curUnit.paramElemsNb(); //equivalent curUnit.publicZonesNumber(SymbolTableConstants.ALLKIND) ;
        nDRZ = curUnit.publicZonesNumber(diffKind) ;
        boolean canGeneralizeActivity = curUnit.hasDirective(Directive.SPECIALIZEACTIVITY) == null;
        TapList<DiffRoot> inDiffRoots = diffRoots;
        while (inDiffRoots != null) {
            DiffRoot thisDiffRoot = inDiffRoots.head;
            if (thisDiffRoot.unit()==curUnit) {
                TapList<DiffPattern> diffPatterns = thisDiffRoot.diffPatterns();
                while (diffPatterns!=null) {
                    DiffPattern diffPattern = diffPatterns.head ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" initializing AD activity for rootUnit: " + curUnit + " with diffPattern " + diffPattern);
                    }
                    TapList<TapIntList> hdPathZones = new TapList<>(null, null);
                    diffPattern.setIndepsZones(buildRootVarZones(diffPattern.indepsPathNames(), curUnit,
                                                                 hdPathZones, curUnit.entryBlock(), diffKind));
                    diffPattern.setIndepsPathZones(hdPathZones.tail);
                    BoolVector rootVarieds = new BoolVector(nDRZ);
                    if (diffPattern.indepsZones() != null) {
                        rootVarieds.set(diffPattern.indepsZones(), true);
                    } else {
                        rootVarieds.setTrue();
                    }
                    hdPathZones.tail = null;
                    diffPattern.setDepsZones(buildRootVarZones(diffPattern.depsPathNames(), curUnit,
                                                               hdPathZones, curUnit.exitBlock(), diffKind));
                    diffPattern.setDepsPathZones(hdPathZones.tail);
                    BoolVector rootUsefuls = new BoolVector(nDRZ);
                    if (diffPattern.depsZones() != null) {
                        rootUsefuls.set(diffPattern.depsZones(), true);
                    } else {
                        rootUsefuls.setTrue();
                    }
                    for (int i=shapeLength-1 ; i>=0 ; --i) {
                        // Zones of non-differentiable type are certainly not
                        // varied upon call nor useful upon exit:
                        ZoneInfo zoneInfo = curUnit.paramElemZoneInfo(i);
                        if (zoneInfo!=null) {
                            int dki = zoneInfo.kindZoneNb(diffKind) ;
                            if (dki!=-1) {
                                if (diffKind != SymbolTableConstants.ALLKIND &&
                                    !isDifferentiableZoneInfo(zoneInfo, diffKind, !TapEnv.doActivity())) {
                                    rootVarieds.set(dki, false);
                                    rootUsefuls.set(dki, false);
                                }
                                // The zone of the result is certainly never varied upon call:
                                if (zoneInfo.isResult()) {
                                    rootVarieds.set(dki, false);
                                }
                            }
                        }
                    }
                    warnAboutRemanentVars(rootVarieds, rootUsefuls, curUnit);
                    BoolVector staticActivity = new BoolVector(nDRZ);
                    filterVariedUsefulWithDeps(rootVarieds, rootUsefuls, curUnit, diffPattern, staticActivity);
                    BoolVector rootNonZeroAdjointComing = rootUsefuls.copy() ;
                    ActivityPattern thisActivity =
                        new ActivityPattern(curUnit, rootVarieds, rootUsefuls, canGeneralizeActivity, diffKind);
                    thisActivity.setDiffPattern(diffPattern);
                    thisActivity.setStaticActivity(staticActivity);
                    thisActivity.setNonZeroAdjointComing(rootNonZeroAdjointComing);
                    curUnit.activityPatterns = new TapList<>(thisActivity, curUnit.activityPatterns);
                    diffPatterns = diffPatterns.tail ;
                }
            }
            inDiffRoots = inDiffRoots.tail;
        }
    }

    /**
     * If the given differentiation-head Unit unit has remanent variables (e.g. "SAVE") of differentiable type,
     * and they can form a dependency chain from independants to dependents across repeated calls to unit,
     * we must warn the end-user that they might need to be considered as (in)dependents too (cf set10/lh209).
     */
    private void warnAboutRemanentVars(BoolVector rootVarieds, BoolVector rootUsefuls, Unit unit) {
        int shapeLength = curUnit.paramElemsNb();
        BoolVector remanentVarsMask = new BoolVector(nDRZ);
        for (int i=shapeLength-1 ; i>=0 ; --i) {
            ZoneInfo zoneInfo = curUnit.paramElemZoneInfo(i);
            if (zoneInfo != null
                && zoneInfo.isRemanentLocal()
                // or (warns across any global) && !zoneInfo.isParameter() && !zoneInfo.isResult()
                && (diffKind == SymbolTableConstants.ALLKIND || isDifferentiableZoneInfo(zoneInfo, diffKind, !TapEnv.doActivity()))) {
                remanentVarsMask.set(zoneInfo.kindZoneNb(diffKind), true);
            }
        }
        BoolMatrix unitDiffDeps = getDiffDeps(unit, dependencies, diffKind);
        BoolVector dangerousRemanents = remanentVarsMask.copy();
        if (unitDiffDeps != null) {
            // Saturate varied remanent variables through repeated calls of unit:
            BoolVector variedRemanents = unitDiffDeps.times(rootVarieds).and(remanentVarsMask);
            while (variedRemanents.cumulOrGrows(unitDiffDeps.times(variedRemanents).and(remanentVarsMask), nDRZ)) {
            }
            dangerousRemanents.cumulAnd(variedRemanents);
            // Saturate useful remanent variables through repeated calls of unit:
            BoolVector usefulRemanents = unitDiffDeps.leftTimes(rootUsefuls).and(remanentVarsMask);
            while (usefulRemanents.cumulOrGrows(unitDiffDeps.leftTimes(usefulRemanents).and(remanentVarsMask), nDRZ)) {
            }
            dangerousRemanents.cumulAnd(usefulRemanents);
        }
        // dangerousRemanents are dangerous because they form a chain of remanent variables that link an independent to a dependent
        // and therefore the user may want to add them to the (in)dependents to manage activity through successive calls.
        String dangerousRemanentNames = null;
        for (int i=shapeLength-1 ; i>=0 ; --i) {
            ZoneInfo zoneInfo = curUnit.paramElemZoneInfo(i);
            int dki = zoneInfo.kindZoneNb(diffKind) ;
            if (dki!=-1 && dangerousRemanents.get(dki) && !(rootVarieds.get(dki) && rootUsefuls.get(dki))) {
                dangerousRemanentNames =
                        (dangerousRemanentNames == null ? "" : dangerousRemanentNames + ", ")
                                + ILUtils.toString(curUnit.paramElemZoneInfo(i).accessTree);
            }
        }
        if (dangerousRemanentNames != null) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, unit.headTree(), "(AD08) Head routine " + unit.name() + " has remanent variables " + dangerousRemanentNames + " that one may want to declare as (in)dependents in case of repeated calls");
        }
    }

    /**
     * Analyze activities for the "curUnit".
     * For the BOTTOMUP_1 phase, returns true when the resulting dependencies
     * for "curUnit" were modified, false otherwise.
     * For the TOPDOWN_2 phase, always returns false, but anyway this result is
     * not used by top-down data-flow analyses.
     */
    @Override
    protected boolean analyze() {
        if (cgPhase == BOTTOMUP_1) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace(" ============== AD DEPENDENCY ANALYSIS OF UNIT " + curUnit.name() + " : ==============");
                traceDisplayPrivateZones(curUnit,
                        curUnit.privateSymbolTable().declaredZonesNb(diffKind),
                        diffKind);
                TapEnv.printlnOnTrace();
            }
            return analyzeForward(null, null, null);
        } else {  //cgPhase==TOPDOWN_2
            BoolMatrix unitDiffDeps = getDiffDeps(curUnit, dependencies, diffKind);
            if (unitDiffDeps != null && curUnit.hasParamElemsInfo()) {
                unitDiffDeps = filterByValue(unitDiffDeps, diffKind, curUnit) ;
                TapList<ActivityPattern> activityPatterns = curUnit.activityPatterns;
                while (activityPatterns != null) {
                    curActivity = activityPatterns.head;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" analysis of " + curUnit + " for ActivityPattern " + curActivity + " with diffPattern " + curActivity.diffPattern());
                    }
                    curStaticActivity = curActivity.staticActivity() ;
                    // wipe out the last iteration's activity and usefulness results from the blocks
                    filteredContext = new BoolVector[2];
                    filteredContext[VARIED_ON_CALL] = curActivity.variedOnCall();
                    filteredContext[USEFUL_ON_EXIT] = curActivity.usefulOnExit();
                    BoolVector unitCallVariedness = filteredContext[VARIED_ON_CALL];
                    BoolVector unitExitUsefulness = filteredContext[USEFUL_ON_EXIT];
                    int diffKindLength = curUnit.publicZonesNumber(diffKind) ;
                    if (!(unitCallVariedness.isFalse(diffKindLength)
                            && unitExitUsefulness.isFalse(diffKindLength))) {
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace();
                            TapEnv.printlnOnTrace(" =============  AD ACTIVITY ANALYSIS OF UNIT " + curUnit.name() + " : ============");
                            TapEnv.printlnOnTrace(" Activity context of this analysis: " + curActivity);
                            TapEnv.printlnOnTrace();
                            ZoneInfo zi;
                            int shapeLength = curUnit.paramElemsNb(), dki ;
                            for (int i=0 ; i<shapeLength ; ++i) {
                                zi = curUnit.paramElemZoneInfo(i);
                                if (zi != null) {
                                    dki = zi.kindZoneNb(diffKind) ;
                                    if (dki!=-1) {
                                        TapEnv.printOnTrace(" [" + dki + "]" + zi.accessTreePrint(curUnit.language()));
                                    }
                                }
                            }
                            TapEnv.printlnOnTrace();
                            TapEnv.printlnOnTrace("   UNIT's AD Deps:");
                            TapEnv.dumpOnTrace(unitDiffDeps);
                            TapEnv.printlnOnTrace("    CONTEXT: [" + unitCallVariedness.toString(diffKindLength) + "]->[" + unitExitUsefulness.toString(diffKindLength) + "] + STATIC: [" + (curStaticActivity == null ? "Null" : curActivity.staticActivity().toString(diffKindLength)+"]"));
                            TapEnv.printlnOnTrace();
                        } else if (curUnit.hasSource()) {
                            TapEnv.printOnTrace(15, "context [" + unitCallVariedness.toString(diffKindLength) + "]->[" + unitExitUsefulness.toString(diffKindLength) + "] ");
                        }
                        if (curUnit.hasSource()) {
                            if (TapEnv.traceCurAnalysis()) {
//                                 Block refBlock = (curUnit.allBlocks!=null ? curUnit.allBlocks.head : curUnit.entryBlock()) ;
//                                 SymbolTable refST = (refBlock!=null ? refBlock.symbolTable : curUnit.publicSymbolTable()) ;
                                traceDisplayPrivateZones(curUnit, /*was refST*/curUnit.privateSymbolTable().declaredZonesNb(diffKind), diffKind);
                            }
                            BlockStorage<TapList<BoolVector>> curUnitVariednesses;
                            BlockStorage<TapList<BoolVector>> curUnitUsefulnesses;
                            // Compute "varied" information:
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace("       =====================  FORWARD VARIED: =====================");
                            }
                            curUnitInfos = curActivity.activities();
                            if (curUnitInfos == null) {
                                curUnitInfos = new BlockStorage<>(curUnit);
                                curActivity.setActivities(curUnitInfos);
                            }
                            fgPhase = VARIED;
                            analyzeForward(null, null, null);
                            curUnitVariednesses = curUnitInfos;
                            // Compute "usefulness" information:
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace("       =====================  BACKWARD USEFUL: ====================");
                            }
                            curUnitInfos = curActivity.usefulnesses();
                            if (curUnitInfos == null) {
                                curUnitInfos = new BlockStorage<>(curUnit);
                                curActivity.setUsefulnesses(curUnitInfos);
                            }
                            fgPhase = USEFUL;
                            analyzeBackward(null, null, null);
                            curUnitUsefulnesses = curUnitInfos;
                            // Accumulate the bufferized activity contexts into their
                            // respective called Units:
                            accumulateBufferizedCallContexts();
                            // Combine variedness & usefulness, building final "activity" :
                            mergeUsefulnessIntoVariedness(curUnit, curUnitUsefulnesses, curUnitVariednesses);
                            // Now "variednesses" actually contains activities !
                            curActivity.setActivities(curUnitVariednesses);
                            curActivity.setUsefulnesses(curUnitUsefulnesses);
                        }
                        // Compute the activities that will be stored at the entry and exit
                        // frontiers of the current "curUnit", and that will determine the
                        // possible initializations of diff-variables both before and
                        // after each call to the diff of "curUnit" and also at
                        // both upstream and downstream ends of the diff of "curUnit".
                        computeUnitFrontierActivities(curUnit, curActivity, unitCallVariedness, unitExitUsefulness);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace();
                            TapEnv.printlnOnTrace("  Boundary activities of "+curUnit+" :") ;
                            TapEnv.printlnOnTrace("  ------------------------------------") ;
                            TapEnv.printlnOnTrace("    Unit    outside  call activity: [" + curActivity.callActivity().toString(diffKindLength)+"]");
                            if (curUnit.hasSource()) {
                                BoolVector unitEntryActivityInside =
                                        curActivity.activities().retrieve(curUnit.entryBlock()).head;
                                TapEnv.printlnOnTrace("    Expected inside entry activity: [" + unitEntryActivityInside.toString(diffKindLength)+"]");
                                BoolVector unitExitActivityInside =
                                        curActivity.activities().retrieve(curUnit.exitBlock()).head;
                                TapEnv.printlnOnTrace("    Expected inside  exit activity: [" + unitExitActivityInside.toString(diffKindLength)+"]");
                            }
                            TapEnv.printlnOnTrace("    Unit    outside  exit activity: [" + curActivity.exitActivity().toString(diffKindLength)+"]");
                            TapEnv.printlnOnTrace();
                        }
                    } else {
                        //TODO JH: what is this doing and why? :
                        curActivity.callActivity().setCopy(unitCallVariedness);
                        // Even if curUnit does nothing to active variables (which is visible by the fact that after
                        // filterVariedUsefulWithDeps(), unitCallVariedness and unitExitUsefulness are both empty),
                        // we must propagate to all (recursively) called routines the set of currently
                        // active variables, so that the coming ReqExplicit analysis can find out that pointers
                        // to these variables must be considered active too.
                        if (curUnit.hasSource() && curStaticActivity != null) {
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace();
                                TapEnv.printlnOnTrace(" =============  AD ACTIVITY STATIC PROPAGATION OF UNIT " + curUnit.name() + " : ============");
                                TapEnv.printlnOnTrace(" Activity context of this analysis: " + curActivity);
                                TapEnv.printlnOnTrace();
                            }
                            fgPhase = STATICACTIVITY;
                            analyzeStatically(null, null);
                            accumulateBufferizedCallContexts();
                        }
                    }
                    activityPatterns = activityPatterns.tail;
                }
                curStaticActivity = null;
            }
            return false;
        }
    }

    @Override
    protected void propagateValuesStaticallyThroughExpression() {
        Tree expression = curInstruction.tree;
        Tree lhs = null;
        int expressionCode = expression.opCode();
        if (expression.opCode() == ILLang.op_assign
                || expression.opCode() == ILLang.op_plusAssign
                || expression.opCode() == ILLang.op_minusAssign
                || expression.opCode() == ILLang.op_timesAssign
                || expression.opCode() == ILLang.op_divAssign) {
            lhs = expression.down(1);
            expression = expression.down(2);
        }
        if (expression.opCode() == ILLang.op_call) {
            Unit calledUnit = getCalledUnit(expression, curSymbolTable);
            MPIcallInfo messagePassingInfo =
                    MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), expression, curUnit.language(), curBlock);
            if (messagePassingInfo == null) { // ignore MPI calls.
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" ---------- Static propagation of activity on procedure call " + ILUtils.toString(expression) + " : ----------");
                }
                bufferizeCallContext(calledUnit, expression, null, null, null, STATICACTIVITY);
            }
        }
    }

    private void computeUnitFrontierActivities(
            Unit unit, ActivityPattern curActivity, BoolVector unitCallVariedness, BoolVector unitExitUsefulness) {
        // We must copy to avoid modifying filteredContext inside
        // topDownContexts by side-effect :
        BoolVector unitCallActivity = unitCallVariedness.copy();
        BoolVector unitExitActivity = unitExitUsefulness.copy();
        // The "Outside" versions are those that will be stored in
        // the callActivities and exitActivities for this unit,
        // i.e. the info that is used to differentiate the call sites of "unit":
        BoolVector unitCallActivityOutside = unitCallVariedness.copy();
        BoolVector unitExitActivityOutside = unitExitUsefulness.copy();
        // Only keep in unitCallActivity (which at the moment is simply the call variedness),
        //  the zones that are useful for some variable useful upon exit.
        // Only keep in unitExitActivity (which at the moment is simply the exit usefulness),
        //  the zones that are varied due to some variable varied upon entry.
        // Don't do that for a Message-Passing call that uses some channel, because
        //  dependence goes through the channel, and therefore activity must be kept.
        // Don't do that for a root Unit, because we must suppose that all vars active
        //  upon exit may be used later, and for the reverse mode that all vars useful upon
        //  entry may be active, i.e. their adjoint is required upon exit from the diff unit.
        //  Example: in direct mode, if TOP has active input x, and xd is overwritten,
        //  and x upon exit is either useless or useful but proved inactive, we force an
        //  explicitation of xd at the end of TOP_D (because the user might use it,
        //  or might forget that it is implicitly 0.0 !).
        // Likewise, don't do that for zones that are side-effect in some immediate caller,
        //  because this may be the last chance to explicit an implicit-zero derivative.
        // For a routine instrumented with the DD test i.e. tangent mode,
        //  remove these zones from unitCallActivity only if they are not read in unit,
        //  and remove these zones from unitExitActivity only if they are not written.
        //  (cf comments in CallGraphDifferentiator.instrumentTraceOnUnit() )
        BoolVector sideEffectInUnit = null;
        BoolVector sideEffectInCaller = null;
        if (unit.hasSource()) {
            sideEffectInUnit = sideEffectInUnit(unit);
            sideEffectInCaller = sideEffectInCaller(unit);
            sideEffectInCaller.cumulMinus(sideEffectInUnit);
        }
        boolean isMessagePassing =
                MPIcallInfo.isMessagePassingFunction(unit.name(), unit.language());
        BoolMatrix unitDiffDeps = getDiffDeps(unit, dependencies, diffKind);
        BoolVector callUsefuls = unitDiffDeps.leftTimes(unitExitActivity);
        BoolVector exitVarieds = unitDiffDeps.times(unitCallActivity);
        if (DiffRoot.findUnit(diffRoots, unit) == null && !isMessagePassing) {
            BoolVector callUsefulsModified = callUsefuls.copy();
            BoolVector exitVariedsModified = exitVarieds.copy();
            // Special modifications for TGT and ADJ debug modes:
            if (TapEnv.debugAdMode() == TapEnv.TGT_DEBUG) {
//                 BoolVector tmpPubR = unit.unitInOutPossiblyR();
                callUsefulsModified.cumulOr(unit.focusToKind(unit.unitInOutPossiblyR(), diffKind)) ;
//                 BoolVector tmpPubW = unit.unitInOutPossiblyW();
                exitVariedsModified.cumulOr(unit.focusToKind(unit.unitInOutPossiblyW(), diffKind)) ;
            }

            // doUsefulness is false in the special case of debugTGT with option -debugPassives
            if (!TapEnv.debugActivity() && TapEnv.doUsefulness()) {
                unitCallActivityOutside.cumulAnd(callUsefulsModified);
                unitExitActivityOutside.cumulAnd(exitVariedsModified);
            }

            if (unit.hasSource()) {
                callUsefulsModified.cumulOr(sideEffectInCaller);
                exitVariedsModified.cumulOr(sideEffectInCaller);
            }
            if (!TapEnv.debugActivity() && TapEnv.doUsefulness()) {
                unitCallActivity.cumulAnd(callUsefulsModified);
                unitExitActivity.cumulAnd(exitVariedsModified);
            }
        }
        if (unit.hasSource()) {
            // Variables which are declared in this "unit", but
            // might be side-effect in some immediate caller, and
            // which in addition have a derivative which is modified
            // in this unit, must be forced "active" upon exit (upon
            // entry for the reverse), so that if their derivatives
            // are implicit, they can be explicited here before it
            // is too late
            BoolVector modifiedInUnit =
                    getFinalModifiedZones(unitDiffDeps, unitCallActivity, unitExitActivity);
            BoolVector modifiedInUnitT =
                    getFinalModifiedZonesT(unitDiffDeps, unitCallActivity, unitExitActivity);
            unitCallActivity.cumulOr(sideEffectInCaller.and(modifiedInUnitT));
            unitExitActivity.cumulOr(sideEffectInCaller.and(modifiedInUnit));
            // conversely, variables which are side-effect for this "unit"
            // must be made passive on the entry and exit Block
            // so that diff of "unit" doesn't try to initialize their diffs.
            unitCallActivity.cumulMinus(sideEffectInUnit);
            unitExitActivity.cumulMinus(sideEffectInUnit);
            setCurBlockEtc(unit.entryBlock());
            if (!TapEnv.debugActivity() && TapEnv.doUsefulness()) {
                BlockStorage<TapList<BoolVector>> curUnitActivities = curActivity.activities();
                curUnitActivities.retrieve(unit.entryBlock()).head = unitCallActivity ;
                curUnitActivities.retrieve(unit.exitBlock()).head = unitExitActivity ;
            }
            setCurBlockEtc(null);
        }
        curActivity.callActivity().setCopy(unitCallActivityOutside);
        if (TapEnv.debugActivity() || !TapEnv.doUsefulness()) {
            curActivity.setExitUsefulness(unitExitActivityOutside);
            curActivity.setCallUsefulness(callUsefuls);
            curActivity.setExitActivity(exitVarieds);
        } else {
            curActivity.setExitActivity(unitExitActivityOutside);
        }
    }

    /**
     * Initialization routine before fixpoint propagation on the given "curUnit".
     */
    @Override
    protected void initializeFGForUnit() {
        if (cgPhase == BOTTOMUP_1) {
            depsIn = new BlockStorage<>(curUnit);
            depsOut = new BlockStorage<>(curUnit);
            depsThrough = new BlockStorage<>(curUnit);
        } else { //cgPhase==TOPDOWN_2
            infosUp = new BlockStorage<>(curUnit);
            infosDown = new BlockStorage<>(curUnit);
            infosCycleUp = new BlockStorage<>(curUnit);
            infosCycleDown = new BlockStorage<>(curUnit);
        }
    }

    /**
     * Set {@link #curBlock}, {@link #curSymbolTable}, {@link #nDZ}.
     * In addition, sets nDRZ
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        nDRZ = (curSymbolTable==null ? -9 : curSymbolTable.declaredZonesNb(diffKind)) ;
    }

    @Override
    protected void initializeFGForInitBlock() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix initIdentity = new BoolMatrix(nDRZ, nDRZ);
            initIdentity.setIdentity();
            TapIntList returnIndices = curUnit.returnIndices(diffKind) ;
            while (returnIndices!=null) {
                initIdentity.setRow(returnIndices.head, new BoolVector(nDRZ)) ;
                returnIndices = returnIndices.tail ;
            }
            depsOut.store(curBlock, initIdentity);
        } else { //cgPhase==TOPDOWN_2
            if (fgPhase == VARIED) {
                if (curBlock == curUnit.entryBlock()) {
                    // This is the standard case, propagating on the full flow graph:
                    infosDown.store(curBlock, filteredContext[VARIED_ON_CALL].copy()) ;
                } else {
                    // This is for running on only a part of the flow graph:
                    infosDown.store(curBlock, entryFrontierVariedZones.copy());
                    if (runSpecialCycleAnalysis(curBlock)) {
                        // This is for running on only a part of the flow graph:
                        infosCycleDown.store(curBlock, entryFrontierVariedZones.copy());
                    }
                }
            } else { // fgPhase==USEFUL
                if (curBlock == curUnit.exitBlock()) {
                    // This is the standard case, propagating on the full flow graph:
                    infosUp.store(curBlock, filteredContext[USEFUL_ON_EXIT]) ;
                } else {
                    // This is for running on only a part of the flow graph:
                    infosUp.store(curBlock, exitFrontierUsefulZones);
                    if (runSpecialCycleAnalysis(curBlock)) {
                        // This is for running on only a part of the flow graph:
                        infosCycleUp.store(curBlock, exitFrontierUsefulZones);
                    }
                }
            }
        }
    }

    @Override
    protected void initializeFGForBlock() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix deps = new BoolMatrix(nDRZ, nDRZ);
            deps.setIdentity();
            TapList<Instruction> instructions = curBlock.instructions;
            ToBool arrives = new ToBool(true);
            while (instructions != null) {
                curInstruction = instructions.head;
                depsThroughExpression(deps, curInstruction.tree, arrives);
                instructions = instructions.tail;
            }
            if (!arrives.get()) {
                // The curBlock is probably part of a recursive subgraph, a
                // function that it is calling has not been analyzed so we
                // suppose this curBlock is not reached for the moment,
                // and so it follows that it has null dependencies
                deps = null;
            }
            curInstruction = null;
            depsThrough.store(curBlock, deps);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace(" == AD Dependencies through " + curBlock + " is:");
                TapEnv.dumpBoolMatrixOnTrace(deps);
                TapEnv.printlnOnTrace();
            }
        }
    }

    /**
     * Collects the data-flow information from the incoming flow arrows.
     *
     * @return true when the control flow effectively reaches this point.
     */
    @Override
    protected boolean accumulateValuesFromUpstream() {
        if (cgPhase == BOTTOMUP_1) {
            tmpDep = accumulateDepIn(curBlock);
            return tmpDep != null;
        } else { //cgPhase==TOPDOWN_2
            return super.accumulateValuesFromUpstream();
        }
    }

    /**
     * Empty the infoTmp and infoTmpCycle that are used to propagate
     * variedness upwards and usefulness downwards.
     */
    @Override
    protected void setEmptyCumulAndCycleValues() {
        infoTmp = null;
        infoTmpCycle = null;
    }

    /**
     * @return the (variedness) value that flows down through curArrow.
     */
    @Override
    protected boolean getValueFlowingThrough() {
        Block origin = curArrow.origin;
        if (runSpecialCycleAnalysis(origin)
                && curArrow.containsCase(FGConstants.NEXT)) {
            additionalInfo = infosCycleDown.retrieve(origin);
        } else {
            additionalInfo = infosDown.retrieve(origin);
        }
        return additionalInfo != null;
    }

    /**
     * @return the (usefulness) value that flows back through curArrow.
     */
    @Override
    protected boolean getValueFlowingBack() {
        Block destination = curArrow.destination;
        if (runSpecialCycleAnalysis(destination)
                && curArrow.finalCycle() == destination.enclosingLoop()) {
            additionalInfo = infosCycleUp.retrieve(destination);
        } else {
            additionalInfo = infosUp.retrieve(destination);
        }
        return additionalInfo != null;
    }

    @Override
    protected void initializeCumulValue() {
        infoTmp = new BoolVector(nDRZ);
    }

    /**
     * Accumulate the additional info, which is not cycling, into infoTmp.
     */
    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        if (infoTmp == null) {
            infoTmp = new BoolVector(nDRZ);
        }
        int commonLength =
                commonSymbolTable.declaredZonesNb(diffKind);
        infoTmp.cumulOr(additionalInfo, commonLength);
    }

    /**
     * Accumulate the additional info, which is cycling, into infoTmpCycle.
     */
    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        if (infoTmpCycle == null) {
            infoTmpCycle = new BoolVector(nDRZ);
        }
        int commonLength =
                commonSymbolTable.declaredZonesNb(diffKind);
        infoTmpCycle.cumulOr(additionalInfo, commonLength);
    }

    /**
     * Compare value downstream "curBlock", with the value on previous sweep.
     */
    @Override
    protected boolean compareDownstreamValues() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix oldDep = depsOut.retrieve(curBlock);
            if (oldDep == null || !tmpDep.equalsBoolMatrix(oldDep)) {
                depsOut.store(curBlock, tmpDep);
                return true;
            } else {
                return false;
            }
        } else { //cgPhase==TOPDOWN_2
            return compareWithStorage(infoTmp, infoTmpCycle, nDRZ,
                    infosDown, infosCycleDown);
        }
    }

    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        boolean result = false;
        int mpIndex = zoneRkToKindZoneRk(mpZone, diffKind);
        if (mpIndex >= 0) {
            if (cgPhase == BOTTOMUP_1) {
                BoolMatrix refDep = depsOut.retrieve(refBlock);
                BoolVector refRow = refDep.getRow(mpIndex);
                BoolVector newRow = tmpDep.getRow(mpIndex);
                if (newRow != null && (refRow == null || !refRow.contains(newRow, nUDRZ))) {
                    refDep.overwriteDeps(new TapIntList(mpIndex, null), newRow, false);
                    result = true;
                }
            } else { //cgPhase==TOPDOWN_2
                BoolVector infoTmpCopy = infoTmp.copy();
                if (runSpecialCycleAnalysis(curBlock)) {
                    infoTmpCopy.cumulOr(infoTmpCycle);
                }
                BoolVector refInfo = infosDown.retrieve(refBlock);
                if (infoTmpCopy.get(mpIndex) && !refInfo.get(mpIndex)) {
                    refInfo.set(mpIndex, true);
                    result = true;
                }
            }
        }
        return result;
    }

    /**
     * Compare data-flow value upstream "curBlock", with the value on previous sweep.
     *
     * @return true when something has changed and therefore another
     * Flow Graph sweep is probably necessary.
     */
    @Override
    protected boolean compareUpstreamValues() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix oldDep = depsIn.retrieve(curBlock);
            if (oldDep == null || !tmpDep.equalsBoolMatrix(oldDep)) {
                // Must store a copy, because tmpDep will be
                // modified in place when propagated through curBlock.
                depsIn.store(curBlock, tmpDep.copy());
                return true;
            } else {
                return false;
            }
        } else { //cgPhase==TOPDOWN_2
            return compareWithStorage(infoTmp, infoTmpCycle, nDRZ,
                    infosUp, infosCycleUp);
        }
    }

    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        boolean result = false;
        int mpIndex = zoneRkToKindZoneRk(mpZone, diffKind);
        if (mpIndex >= 0 && cgPhase == TOPDOWN_2) {
            BoolVector infoTmpCopy = infoTmp.copy();
            if (runSpecialCycleAnalysis(curBlock)) {
                infoTmpCopy.cumulOr(infoTmpCycle);
            }
            BoolVector refInfo = infosUp.retrieve(refBlock);
            if (infoTmpCopy.get(mpIndex) && !refInfo.get(mpIndex)) {
                refInfo.set(mpIndex, true);
                result = true;
            }
        }
        return result;
    }

    /**
     * Propagate the (ad-deps or variedness) info through "curBlock".
     */
    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix depThrough = depsThrough.retrieve(curBlock);
            if (depThrough != null) {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   propagate AD Dependencies entering "+curBlock) ;
                    TapEnv.dumpOnTrace(tmpDep) ;
                    TapEnv.printlnOnTrace("   through local AD Dependencies of "+curBlock) ;
                    TapEnv.dumpOnTrace(depThrough) ;
                }
                tmpDep.rows = depThrough.times(tmpDep).rows;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   returns AD Dependencies exiting "+curBlock) ;
                    TapEnv.dumpOnTrace(tmpDep) ;
                    TapEnv.printlnOnTrace("  ---------------------") ;
                }
                return true;
            } else {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   control cannot (yet) go through "+curBlock) ;
                }
                tmpDep = null;
                return false;
            }
        } else { //cgPhase==TOPDOWN_2
            return propagateAndStoreVariednessThroughBlock(null);
        }
    }

    /**
     * When "recordList" is non-null, call last checks and
     * fill recordList with the intermediate infos.
     */
    private boolean propagateAndStoreVariednessThroughBlock(TapList<BoolVector> recordList) {
        TapList<Instruction> instructions = curBlock.instructions;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("--- Going down through Block " + curBlock + " ---");
        }
        if (infoTmpCycle != null) {
            infoTmp.cumulOr(infoTmpCycle);
            infoTmpCycle = null;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    Varied:[" + infoTmp.toString(nDRZ)+"]");
        }
        if (recordList != null) {
            BoolVector recordInfo = infoTmp.copy();
            recordList = recordList.placdl(recordInfo);
        }
        // If there is a special treatment for cycling data:
        if (runSpecialCycleAnalysis(curBlock)) {
            curInstruction = instructions.head;
            Tree doTree = curInstruction.tree.down(3);
            // Make variedness infoTmp sweep through the initial read
            // of the a DO loop's "from" expression:
            if (infoTmp != null) {
                variednessThroughExpression(doTree.down(2), infoTmp, null, false);
            }
            // Compute the variedness infoTmpCycle that will be
            // propagated downstream towards the loop's cycling arrows.
            // It is the arriving variedness infoTmp PLUS only the arriving
            // variedness infoTmpCycle which are not localized in the loop.
            if (infoTmpCycle != null) { // assert infoTmpCycle == null;
                BoolVector infoTmpCycleMemo =
                        infoTmpCycle.copy();
                TapIntList localizedZones =
                        curSymbolTable.getDeclaredKindZones(((HeaderBlock) curBlock).localizedZones, diffKind, true);
                infoTmpCycle.set(localizedZones, false);
                if (infoTmp != null) {
                    infoTmpCycle.cumulOr(infoTmp);
                }
                // update the variedness infoTmp that will be propagated downstream
                // through the loop's exiting arrows. It is the arriving
                // "infoTmp" (except if the loop runs certainly at least once),
                // PLUS the arriving "infoTmpCycle".
                if (infoTmp != null) {
                    infoTmpCycleMemo.cumulOr(
                            infoTmp);
                }
                infoTmp = infoTmpCycleMemo;
            } else {
                infoTmpCycle = infoTmp.copy();
            }
        }

        ToBool arrives = new ToBool(true);
        ToBool arrivesC = new ToBool(true);
        // Make variedness infoTmp (if non null) and possibly also
        // variedness infoTmpCycle (if non null) sweep downstream "curBlock".
        while (instructions != null) {
            curInstruction = instructions.head;
            if (recordList != null) {
                // Run these checks only on the last sweep (when recordList!=null)
                checkVariedIO(infoTmp, curInstruction.tree);
                checkVariedBadCast(infoTmp, curInstruction.tree, curUnit);
            }
            if (infoTmp != null) {
                variednessThroughExpression(curInstruction.tree, infoTmp, arrives, false);
            }
            if (infoTmpCycle != null) {
                variednessThroughExpression(curInstruction.tree, infoTmpCycle, arrivesC, false);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Varied:[" + infoTmp.toString(nDRZ) +"]"+ (infoTmpCycle == null ? "" : " (cycling:]" + infoTmpCycle.toString(nDRZ) + "])"));
            }
            if (recordList != null) {
                assert infoTmp != null;
                BoolVector recordInfo = infoTmp.copy();
                if (infoTmpCycle != null) {
                    recordInfo.cumulOr(infoTmpCycle);
                }
                recordList = recordList.placdl(recordInfo);
            }
            instructions = instructions.tail;
        }
        curInstruction = null;
        return arrives.get() && arrivesC.get();
    }

    /**
     * Propagate the (usefulness) info through "curBlock".
     */
    @Override
    protected boolean propagateValuesBackwardThroughBlock() {
        return propagateAndStoreUsefulnessThroughBlock(null);
    }

    /**
     * When "recordList" is non-null, fill it with the intermediate infos.
     */
    private boolean propagateAndStoreUsefulnessThroughBlock(TapList<BoolVector> recordList) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("--- Going up through Block " + curBlock + " ---");
        }
        if (infoTmpCycle != null) {
            infoTmp.cumulOr(infoTmpCycle);
            infoTmpCycle = null;
        }
        TapList<Instruction> instructions = curBlock.instructions;
        Instruction initialDo = null;
        boolean runSpecialCycleAnalysis = runSpecialCycleAnalysis(curBlock);
        Tree doTree = null;
        if (runSpecialCycleAnalysis) {
            initialDo = instructions.head;
            if (initialDo != null) {
                doTree = initialDo.tree.down(3);
            }
            instructions = instructions.tail;
        }
        instructions = TapList.reverse(instructions);
        // First make "infoTmp" (and possibly also "infoTmpCycle" if we
        // are in the runSpecialCycleAnalysis) sweep upstream across the
        // instructions. If the 1st instruction of the Block is a normal
        // clean do loop, do not consider the "do-from" tree,
        // which is read only at loop entry.
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    Useful:[" + infoTmp.toString(nDRZ)+"]");
        }
        if (recordList != null) {
            BoolVector recordInfo = infoTmp.copy();
            recordList.tail = new TapList<>(recordInfo, null);
        }
        ToBool arrives = new ToBool(true);
        ToBool arrivesC = new ToBool(true);
        while (instructions != null) {
            curInstruction = instructions.head;
            if (recordList != null) {
                // Run these checks only on the last sweep (when recordList!=null)
                checkUsefulIO(infoTmp, curInstruction.tree);
                checkUsefulBadCast(infoTmp, curInstruction.tree, curUnit);
            }
            usefulnessThroughExpression(curInstruction.tree, null, infoTmp, arrives, false);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                usefulnessThroughExpression(curInstruction.tree, null, infoTmpCycle, arrivesC, false);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Useful:["+infoTmp.toString(nDRZ)+"]"+(infoTmpCycle == null ? "" : " (cycling:[" + infoTmpCycle.toString(nDRZ) + "])"));
            }
            if (recordList != null) {
                BoolVector recordInfo = infoTmp.copy();
                if (infoTmpCycle != null) {
                    recordInfo.cumulOr(infoTmpCycle);
                }
                recordList.tail = new TapList<>(recordInfo, recordList.tail);
            }
            instructions = instructions.tail;
        }
        if (initialDo != null) {
            curInstruction = initialDo;
            usefulnessThroughExpression(doTree.down(1), null,
                    infoTmp, arrives, false);
            usefulnessThroughExpression(doTree.down(3), null,
                    infoTmp, arrives, false);
            usefulnessThroughExpression(doTree.down(4), null,
                    infoTmp, arrives, false);
            if (runSpecialCycleAnalysis && infoTmpCycle != null) {
                usefulnessThroughExpression(doTree.down(1), null,
                        infoTmpCycle, arrivesC, false);
                usefulnessThroughExpression(doTree.down(3), null,
                        infoTmpCycle, arrivesC, false);
                usefulnessThroughExpression(doTree.down(4), null,
                        infoTmpCycle, arrivesC, false);
            }
        }
        // Second compute the "infoTmpCycle" that will be propagated upstream
        // through the loop's cycling arrows. It is the upcoming "infoTmp" PLUS
        // only the upcoming "infoTmpCycle" which are not localized in the loop.
        BoolVector infoTmpCycleUp = null;
        if (runSpecialCycleAnalysis) {
            if (infoTmpCycle != null) {
                infoTmpCycleUp = infoTmpCycle.copy();
                TapIntList localizedZones =
                        curSymbolTable.getDeclaredKindZones(((HeaderBlock) curBlock).localizedZones, diffKind, true);
                infoTmpCycleUp.set(localizedZones, false);
            } else {
                infoTmpCycleUp = new BoolVector(nDRZ);
            }
            if (infoTmp != null) {
                infoTmpCycleUp.cumulOr(infoTmp);
            }
            // Third compute the "infoTmp" that will be propagated upstream
            // through the loop's entering arrows. It is the upcoming "infoTmp"
            // (except if the loop runs certainly at least once),
            // PLUS the upcoming "infoTmpCycle".
            if (infoTmp != null) {
                if (infoTmpCycle == null) {
                    infoTmpCycle = new BoolVector(nDRZ);
                }
                infoTmpCycle.cumulOr(infoTmp);
            }
            infoTmp = infoTmpCycle;
        }
        infoTmpCycle = infoTmpCycleUp;
        // Fourth add the effect of reading the do-from into the "infoTmp"
        if (initialDo != null) {
            usefulnessThroughExpression(doTree.down(2), null,
                    infoTmp, arrives, false);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Useful:[" + infoTmp.toString(nDRZ) + "]"+(infoTmpCycle == null ? "" : " (cycling:[" + infoTmpCycle.toString(nDRZ) + "])"));
            }
            if (recordList != null) {
                BoolVector recordInfo = infoTmp.copy();
                if (infoTmpCycle != null) {
                    recordInfo.cumulOr(infoTmpCycle);
                }
                recordList.tail = new TapList<>(recordInfo, recordList.tail);
            }
        }
        curInstruction = null;
        return true;
    }

    /**
     * Refines the propagated info (VARIED of USEFUL) for every
     * interval between successive Instruction's in curBlock.
     * The result is built and stored into the global
     * "curUnitInfos", which is a BlockStorage of TapList's
     * of infos, with N+1 infos if curBlock has N Instruction's.
     */
    @Override
    protected void terminateFGForBlock() {
        if (cgPhase == TOPDOWN_2) {
            TapList<BoolVector> toInfos = new TapList<>(null, null);
            if (fgPhase == VARIED) {
                infoTmp = infosUp.retrieve(curBlock);
                infoTmpCycle = infosCycleUp.retrieve(curBlock);
            } else { //fgPhase==USEFUL
                infoTmp = infosDown.retrieve(curBlock);
                infoTmpCycle = infosCycleDown.retrieve(curBlock);
            }
            if (infoTmp == null) {
                infoTmp = new BoolVector(nDRZ);
            }
            if (fgPhase == VARIED) {
                propagateAndStoreVariednessThroughBlock(toInfos);
            } else { //fgPhase==USEFUL
                propagateAndStoreUsefulnessThroughBlock(toInfos);
            }
            curUnitInfos.store(curBlock, toInfos.tail);
        }
    }

    @Override
    protected void terminateFGForTermBlock() {
        if (cgPhase == TOPDOWN_2) {
            TapList<BoolVector> termBlockInfos = curUnitInfos.retrieve(curBlock);
            if (termBlockInfos == null) {
                termBlockInfos = new TapList<>(null, new TapList<>(null, null));
                curUnitInfos.store(curBlock, termBlockInfos);
            }
            if (fgPhase == VARIED) {
                BoolVector termInfo = infosUp.retrieve(curBlock);
                if (termInfo == null) {
                    termInfo = new BoolVector(nDRZ);
                } else {
                    termInfo = termInfo.copy();
                }
                BoolVector termCycleInfo =
                        infosCycleUp.retrieve(curBlock);
                if (termCycleInfo != null) {
                    termInfo.cumulOr(termCycleInfo);
                }
                termBlockInfos.head = termInfo;
            } else { //fgPhase==USEFUL
                TapList<BoolVector> toLastInfo = TapList.toLast(termBlockInfos);
                if (toLastInfo != null) {
                    BoolVector termInfo = infosDown.retrieve(curBlock);
                    if (termInfo == null) {
                        termInfo = new BoolVector(nDRZ);
                    } else {
                        termInfo = termInfo.copy();
                    }
                    BoolVector termCycleInfo =
                            infosCycleDown.retrieve(curBlock);
                    if (termCycleInfo != null) {
                        termInfo.cumulOr(termCycleInfo);
                    }
                    toLastInfo.head = termInfo;
                }
            }
        }
    }

    /**
     * Terminates analysis on the given curUnit.
     * For the BOTTOMUP_1 phase, MUST return true iff the result has changed since previous time.
     * For the TOPDOWN_2 phase, always returns false, but anyway this result is
     * not used by top-down data-flow analyses.
     */
    @Override
    protected boolean terminateFGForUnit() {
        if (cgPhase == BOTTOMUP_1) {
            BoolMatrix unitDep = accumulateDepIn(curUnit.exitBlock());
            // Clean the intermediate results for this curUnit:
            depsIn = null;
            depsOut = null;
            depsThrough = null;
            // When this routine does not reach the end (e.g recursion),
            // this exit "unitDep" is implicit-zero. In this case, we don't
            // want this curUnit to raise a recompute on all the call graph,
            // so we return false meaning this curUnit's result didn't change!
            if (unitDep == null) {
                return false;
            }
            BoolMatrix oldUnitDep = dependencies.retrieve(curUnit);
            if (oldUnitDep != null && unitDep.equalsBoolMatrix(oldUnitDep)) {
                return false;
            } else {
                dependencies.store(curUnit, unitDep);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" == AD Dependency effect stored for unit " + curUnit.name() + " is:");
                    TapEnv.dumpOnTrace(unitDep);
                    TapEnv.printlnOnTrace();
                }
                return true;
            }
        } else { //cgPhase==TOPDOWN_2
            if (fgPhase != STATICACTIVITY) {
                curUnitInfos.store(curUnit.entryBlock(),
                        new TapList<>(infosDown.retrieve(curUnit.entryBlock()), null));
                curUnitInfos.store(curUnit.exitBlock(),
                        new TapList<>(infosUp.retrieve(curUnit.exitBlock()), null));
            }
            return false;
        }
    }

    /**
     * Set the activity annotations on the trees of Unit "curUnit".
     * This is done after all sweeps on the call graph are completed,
     * therefore this is done only once.
     */
    @Override
    protected void terminateCGForUnit() {
        if (curUnit.hasSource()) {
            TapList<ActivityPattern> activityPatterns = curUnit.activityPatterns;
            while (activityPatterns != null) {
                curActivity = activityPatterns.head;
                curUnit.activeCalledNames = null;
                if (curActivity.activities() != null) {
                    presetActivityAnnotations(curUnit, curActivity);
                }
                activityPatterns = activityPatterns.tail;
            }
        }
    }

    /**
     * Compute the (diffKind) global (non-argument) zones that are hidden in some caller of the given "unit".
     */
    private BoolVector sideEffectInCaller(Unit unit) {
        BoolVector sideEffectInCaller = new BoolVector(unit.publicZonesNumber(diffKind));
        TapList<CallArrow> callers = unit.callers();
        CallArrow callerArrow;
        ZoneInfo[] externalShape;
        ZoneInfo zoneInfo ;
        while (callers != null) {
            callerArrow = callers.head;
            // If the caller unit is also differentiated:
            boolean somethingActive = false;
            TapList<ActivityPattern> activityPatterns = callerArrow.origin.activityPatterns;
            while (activityPatterns != null && !somethingActive) {
                if (activityPatterns.head.activities() != null) {
                    somethingActive = true;
                }
                activityPatterns = activityPatterns.tail;
            }
            if (somethingActive) {
                SymbolTable callerSymbolTable = callerArrow.origin.privateSymbolTable();
                if (callerSymbolTable == null) {
                    callerSymbolTable = callerArrow.origin.publicSymbolTable();
                }
                externalShape = unit.externalShape ;
                for (int i=externalShape.length-1 ; i>=0 ; --i) {
                    zoneInfo = unit.paramElemZoneInfo(i);
                    // Insert zones that are not "declared" in the calling Unit,
                    // or that, although declared, are inaccessible in the calling unit.
                    if (zoneInfo.kindZoneNb(diffKind)!=-1
                        && zoneInfo.isGlobal()
                        && isHiddenAllocatable(
                                   callerSymbolTable.declaredZoneInfo(zoneInfo.zoneNb, SymbolTableConstants.ALLKIND),
                                   callerSymbolTable)) {
                        sideEffectInCaller.set(zoneInfo.kindZoneNb(diffKind), true);
                    }
                }
            }
            callers = callers.tail;
        }
        return sideEffectInCaller;
    }

    /**
     * Compute the (diffKind) global (non-argument) zones that are hidden in this Unit,
     * or by extension, zones that cannot be accessed here e.g. because
     * they are allocatable zones with no access tree declared here.
     */
    private BoolVector sideEffectInUnit(Unit unit) {
        BoolVector sideEffectInUnit = new BoolVector(unit.publicZonesNumber(diffKind)) ;
        ZoneInfo[] externalShape = unit.externalShape ;
        if (externalShape == null) {
            // case of intrinsics or externals: set to neutral value:
            sideEffectInUnit.setTrue();
        } else {
            SymbolTable symbolTable = unit.privateSymbolTable();
            if (symbolTable == null) {
                symbolTable = unit.publicSymbolTable();
            }
            ZoneInfo zoneInfo ;
            for (int i=externalShape.length-1 ; i>=0 ; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                if (zoneInfo.kindZoneNb(diffKind)!=-1
                    && zoneInfo.isGlobal()
                    && isHiddenAllocatable(zoneInfo, symbolTable)) {
                    sideEffectInUnit.set(zoneInfo.kindZoneNb(diffKind), true);
                }
            }
        }
        return sideEffectInUnit;
    }

    /**
     * @return true if "zoneInfo" cannot be accessed in the given "symbolTable"
     * because, although declared, there is no variable name to access it
     * (example: allocatable but no declared way to access it here cf F90:lha26).
     */
    private boolean isHiddenAllocatable(ZoneInfo zoneInfo, SymbolTable symbolTable) {
        boolean hidden = false;
        if (zoneInfo==null || zoneInfo.isHidden) {
            hidden = true;
        } else {
            // TODO Doesn't work cf set03/cm02
            // if (zoneinfo.comesFromAllocate()) return true;
            // il faut tester si allocVar est la meme VariableDecl que celle de la zoneInfo:
            Tree allocVar = ILUtils.getAllocatedRef(zoneInfo.accessTree);
            if (allocVar != null) {
                WrapperTypeSpec allocatedType = symbolTable.typeOf(allocVar);
                if (allocatedType == null
                    || allocatedType.baseTypeName() == null
                    || allocatedType.baseTypeName().equals("Undefined")) {
                    hidden = true;
                }
            }
        }
        return hidden ;
    }

    /**
     * Filters the given context (varieds;usefuls) through the given "unit".
     * Modifies the given "varieds" and "usefuls" in-place.
     * When thisDiffPattern is non-null, this indicates "unit" is a root Unit and we want to
     * treat it in a special way for root Units, i.e. set more varieds and usefuls.
     *
     * @param collectedFilteredActives when given non-null, accumulates the zones that were
     *   initially both varied and useful but have been filtered out because not used in unit.
     */
    private void filterVariedUsefulWithDeps(BoolVector varieds, BoolVector usefuls,
                                            Unit unit, DiffPattern thisDiffPattern,
                                            BoolVector collectedFilteredActives) {
        BoolMatrix unitDiffDeps = getDiffDeps(unit, dependencies, diffKind);
        unitDiffDeps = filterByValue(unitDiffDeps, diffKind, unit) ;
        boolean isRoot = thisDiffPattern != null;
        int diffKindLength = unit.publicZonesNumber(diffKind) ;
        if (!(//Cases where one must not filter the context-given varied+useful:
              unitDiffDeps == null
              // ^ either unit has no deps (e.g. unit STOP's) that can be used for filtering, ...
              || (isRoot && TapEnv.get().fixInterface)
              // ^ ...or we have the "-fixinterface" option and this is a root unit...
              || TapEnv.debugActivity() || !TapEnv.doUsefulness()
              // ^ ...or we are debugging a code where activity doesn't go through
              )) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  NOW FILTERING:[" + varieds.toString(diffKindLength) +
                                      "] -> [" + usefuls.toString(diffKindLength) +
                                      "] through unit " + unit + " (root:" + isRoot + ')');
                TapEnv.printlnOnTrace("   UNIT's AD Dependencies:");
                TapEnv.dumpOnTrace(unitDiffDeps);
            }
            BoolVector initVarieds = isRoot ? varieds.copy() : null;
            BoolVector initUsefuls = isRoot ? usefuls.copy() : null;

            updateVariedUsefulWithDeps(varieds, usefuls, unit, unitDiffDeps, collectedFilteredActives);

            if (isRoot) {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("interm.result:  [" + varieds.toString(diffKindLength) + "] -> [" + usefuls.toString(diffKindLength) + "]");
                    TapEnv.printlnOnTrace("    now adding diff vars in filtered because root unit!");
                }
                addDiffVarsForRootUnit(varieds, usefuls, unit.focusToKind(unit.zonesOfResult(), diffKind), unitDiffDeps, unit);
                // Send messages if dependents and independents sets of a root Unit were modified:
                String messageNames;
                BoolVector removed = varieds.not();
                removed.cumulAnd(initVarieds);
                messageNames =
                        namesWithProperty(thisDiffPattern.indepsPathNames(), thisDiffPattern.indepsPathZones(), removed);
                if (messageNames != null) {
                    TapEnv.commandWarning(13, "Input variable(s) " + messageNames + " have no differentiable influence in " + unit.name() + ": removed from independents");
                }
                removed = usefuls.not();
                removed.cumulAnd(initUsefuls);
                messageNames =
                        namesWithProperty(thisDiffPattern.depsPathNames(), thisDiffPattern.depsPathZones(), removed);
                if (messageNames != null) {
                    TapEnv.commandWarning(13, "Output variable(s) " + messageNames + " are not influenced differentiably in " + unit.name() + ": removed from dependents");
                }
                BoolVector added = initVarieds.not();
                added.cumulAnd(varieds);
                messageNames = namesOfZones(added, diffKind, unit);
                if (messageNames != null) {
                    TapEnv.commandWarning(13, "Input variable(s) " + messageNames + " have their derivative modified in " + unit.name() + ": added to independents");
                }
                added = initUsefuls.not();
                added.cumulAnd(usefuls);
                messageNames = namesOfZones(added, diffKind, unit);
                if (messageNames != null) {
                    TapEnv.commandWarning(13, "Output variable(s) " + messageNames + " have their derivative modified in " + unit.name() + ": added to dependents");
                }
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("       FILTERED:[" + varieds.toString(diffKindLength) + "] -> [" + usefuls.toString(diffKindLength) + "]" + (collectedFilteredActives == null ? "" : " plus static actives: [" + collectedFilteredActives.toString(diffKindLength) + "]"));
            }
        }
    }

    /**
     * Refine the outside activity context given by "variedOnCall" and "usefulOnExit",
     * with respect to the inside activity behavior given by "deps".
     * The refinement rules are built to be independent from the
     * differentiation mode, therefore they are weaker (less aggressive) than
     * in FlowGraphDifferentiator.updateVariedUsefulWithDepsAndDiffMode().
     * <pre>
     * Rules:
     * - Remove i from UoE
     * if i is not in VoC and
     * row i in deps (except diagonal) doesn't intersect VoC
     * - Remove i from VoC
     * if i is not in UoE and
     * column i in deps (except diagonal) doesn't intersect UoE
     * - Remove i both from VoC and from UoE
     * if diagonal i in deps is Identity and
     * row i in deps (except diagonal) doesn't intersect VoC and
     * column i in deps (except diagonal) doesn't intersect UoE.
     * </pre>
     */
    private void updateVariedUsefulWithDeps(BoolVector variedOnCall, BoolVector usefulOnExit,
                                            Unit unit, BoolMatrix deps, BoolVector collectedFilteredActives) {
        if (collectedFilteredActives != null) {
            collectedFilteredActives.setCopy(variedOnCall);
// // This is here only to help me debug on Niels Horsten code. Remove when done!!
// if (usefulOnExit==null) {
//     System.out.println("Mystery: usefulOnExit is "+usefulOnExit+" in updateVariedUsefulWithDeps()") ;
//     TapEnv.toolError(  "Mystery: usefulOnExit is "+usefulOnExit+" in updateVariedUsefulWithDeps()") ;
//     if (TapEnv.traceCurAnalysis()) {
//         TapEnv.printlnOnTrace("Mystery: usefulOnExit is "+usefulOnExit+" in updateVariedUsefulWithDeps()") ;
//     }
// }
            collectedFilteredActives.cumulAnd(usefulOnExit);
        }
        BoolMatrix depsNoDiag = deps.copy();
        for (int i = deps.nRows - 1; i >= 0; --i) {
            depsNoDiag.set(i, i, false);
        }
        BoolVector rowsFromNoVaried = depsNoDiag.times(variedOnCall);
        BoolVector colsToNoUseful = depsNoDiag.leftTimes(usefulOnExit);
        for (int i = deps.nRows - 1; i >= 0; --i) {
            if (!colsToNoUseful.get(i) && !usefulOnExit.get(i)) {
                variedOnCall.set(i, false);
            }
            if (!rowsFromNoVaried.get(i) && !variedOnCall.get(i)) {
                usefulOnExit.set(i, false);
            }
            if (deps.isImplicitIdentityRow(i) && !rowsFromNoVaried.get(i) && !colsToNoUseful.get(i)) {
                variedOnCall.set(i, false);
                usefulOnExit.set(i, false);
            }
        }
        putBackSharingZones(variedOnCall, usefulOnExit, unit);
        if (collectedFilteredActives != null) {
            collectedFilteredActives.cumulMinus(variedOnCall);
            collectedFilteredActives.cumulMinus(usefulOnExit);
        }
        // Upon exit, collectedFilteredActives contains exactly those zones that are active
        // throughout interpretation of "unit", but are not touched nor used by "unit".
    }

    /**
     * Put back zones that share a variable with another zone
     * which is varied or useful: (cf lh91).
     */
    public void putBackSharingZones(BoolVector variedOnCall, BoolVector usefulOnExit,
                                    Unit unit) {
        if (unit != null && unit.publicSymbolTable() != null && unit.hasSource()) {
            SymbolTable symbolTable = unit.publicSymbolTable();
            TapList<String> otherNames;
            TapList otherZones;
            TapIntList zz2;
            VariableDecl variableDecl;
            ZoneInfo zoneInfo;
            BoolVector extendedVaried = variedOnCall.copy();
            BoolVector extendedUseful = usefulOnExit.copy();
            int shapeLength = unit.paramElemsNb() ;
            for (int i=shapeLength-1 ; i>=0 ; --i) {
                zoneInfo = unit.paramElemZoneInfo(i);
                int dki = zoneInfo.kindZoneNb(diffKind) ;
                if (dki!=-1 && (variedOnCall.get(dki) || usefulOnExit.get(dki))) {
                    otherNames = (zoneInfo==null ? null : zoneInfo.variableNames()) ;
                    while (otherNames != null) {
                        variableDecl = symbolTable.getVariableDecl(otherNames.head);
                        if (variableDecl != null) {
                            otherZones = TapList.getSetFieldLocation(
                                    variableDecl.zones(), zoneInfo.accessTree, false);
                            zz2 = unit.focusToKind(ZoneInfo.listAllZones(otherZones, false), diffKind) ;
                            if (variedOnCall.get(dki)) {
                                extendedVaried.set(zz2, true);
                            }
                            if (usefulOnExit.get(dki)) {
                                extendedUseful.set(zz2, true);
                            }
                        }
                        otherNames = otherNames.tail;
                    }
                }
            }
            variedOnCall.cumulOr(extendedVaried) ;
            usefulOnExit.cumulOr(extendedUseful) ;
        }
    }

    /**
     * Specially for a root Unit (or for a unit which is
     * being instrumented for debug).
     * In tangent mode, add into the useful exit variables the
     * entry varied vars whose diff var may be overwritten.
     * In reverse mode, add into the varied entry variables the
     * useful exit vars whose diff var may be overwritten.
     * This is necessary because user calls of the diff root unit
     * may think that these derivatives are only read, and they
     * may not be aware that they are also modified.
     * <p>
     * Example: in tangent mode, if TOP has
     * active input x, and xd is overwritten, and x upon
     * exit is useless, we force x useful upon exit anyway,
     * so that the user knows x is modified.
     * <p>
     * For -debugADJ mode (dbadMode==-1) activities must be identical for TGT and ADJ.
     */
    private void addDiffVarsForRootUnit(BoolVector variedOnCall, BoolVector usefulOnExit,
                                        TapIntList resultRks, BoolMatrix deps, Unit unit) {
        //[llh 05/03/2014] Find a way to get rid of this (last) dependence on diffMode:
        if (TapEnv.modeIsTangent() || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
            usefulOnExit.cumulOr(variedOnCall.and(getRootModifiedZones(deps, variedOnCall, unit)));
        }
        if (TapEnv.modeIsAdjoint() || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG) {
            variedOnCall.cumulOr(
                    usefulOnExit.and(getRootModifiedZonesT(deps, usefulOnExit)));
            while (resultRks != null) {
                variedOnCall.set(resultRks.head, false);
                resultRks = resultRks.tail;
            }
        }
    }

    /**
     * Variant of getModifiedZones special for the initial update through the Unit's deps.
     *
     * @return a vector with true at index i if diff of zone i will be
     * modified by the tangent of the routine whose dependencies are "deps",
     * and called with the given varied&rarr;useful context.
     * Diff of zone i is NOT modified when both input and output diffs
     * are 0.0, or also when the row i of "deps" is "implicit-identity".
     * Assumes the matrix is not "implicit-zero".
     * Not very well understood yet why this must be different from getFinalModifiedZones(): TODO.
     */
    public static BoolVector getUpdateModifiedZones(BoolMatrix deps,
                                                    BoolVector variedOnCall,
                                                    Unit unit) {
        // Current justification: 2 cases where a zone will be modified by tangent diff:
        //  1) vd is active upon call and
        //     will be given a new derivative or a passive vd=0 upon exit
        //    --> detected by ".or(variedOnCall)" and vd's row not identity.
        //  2) vd may be passive upon call, but receives a new non-null derivative upon exit
        //     which is useful after the call
        //    --> detected by deps.times(variedOnCall) and row not identity.
        BoolVector result = deps.times(variedOnCall).or(variedOnCall);

        ZoneInfo zoneInfo ;
        int dki ;
        // Zones passed by value must not be considered modified:
        for (int i=unit.externalShape.length-1 ; i>=0 ; --i) {
            zoneInfo = unit.externalShape[i] ;
            if (zoneInfo!=null) {
                dki = zoneInfo.kindZoneNb(TapEnv.diffKind()) ;
                if (dki!=-1 && result.get(dki) && zoneInfo.passesByValue(unit, unit.language()))
                    result.set(dki, false);
            }
        }

        for (int i = deps.nRows - 1; i >= 0; --i) {
            if (deps.rows[i] == null) {
                result.set(i, false);
            }
        }
        return result;
    }

    /**
     * Variant of getModifiedZones special for the cleaning after analysis.
     *
     * @return a vector with true at index i if diff of zone i will be
     * modified by the tangent of the routine whose dependencies are "deps".
     */
    private BoolVector getFinalModifiedZones(BoolMatrix deps,
                                             BoolVector variedOnCall, BoolVector usefulOnExit) {
        // The 2 cases of explicitly overwritten diff vars that we want to detect are:
        //  1) vd is varied upon entry, and becomes passive upon exit (i.e. there should be a vd=0.0)
        //    --> detected by "true in variedOnCall" and "row is zero"
        //  2) vd may be passive or not, upon entry, but is useful upon exit and will have received
        //    some modified non-null derivative upon exit (i.e. there will be a vd = something like C*wd.)
        //    --> detected by "true in usefulOnExit" and "row is non-zero-non-identity"
        BoolVector rowZero = new BoolVector(deps.nRows);
        BoolVector rowNonZeroNonId = new BoolVector(deps.nRows);
        rowZero.setFalse();
        rowNonZeroNonId.setFalse();
        for (int i = deps.nRows - 1; i >= 0; --i) {
            if (deps.rows[i] != null) {
                if (deps.rows[i].isFalse(deps.nRows)) {
                    rowZero.set(i, true);
                } else {
                    rowNonZeroNonId.set(i, true);
                }
            }
        }
        return variedOnCall.and(rowZero).or(usefulOnExit.and(rowNonZeroNonId));
    }

    /**
     * Variant of getModifiedZones special for the root Unit or for debugged Units.
     * Not very well understood yet.
     * So far, this one is the same as getUpdateModifiedZones, but this might change...
     */
    private static BoolVector getRootModifiedZones(BoolMatrix deps, BoolVector variedOnCall, Unit unit) {
        return getUpdateModifiedZones(deps, variedOnCall, unit);
    }

    /**
     * Variant of getModifiedZonesT special for the initial update through the Unit's deps.
     *
     * @return a vector with true at index i if diff of zone i will be modified
     * by the adjoint (in the "variedOnCall"&rarr;"usefulOnExit" calling context)
     * of the routine whose dependencies are "deps".
     * This is a sort of transposed equivalent of getUpdateModifiedZones().
     * Assumes the matrix is not "implicit-zero".
     * Diff of zone i is NOT modified when both input and output diffs
     * are 0.0, or also when the column i of "deps" is identity,
     * i.e. all-zero except for "implicit-identity" on the diagonal.
     * Not very well understood yet why this must be different from getFinalModifiedZonesT() : TODO .
     */
    public static BoolVector getUpdateModifiedZonesT(BoolMatrix deps,
                                              BoolVector usefulOnExit) {
        // Current justification: 2 cases where a zone will be modified by adjoint diff:
        //  1) vb is active at downstream entry and
        //     will be given a new derivative or a passive vb=0 upon upstream exit
        //    --> detected by ".or(usefulOnExit)" and column not identity.
        //  2) vd may be passive at downstream entry, but receives a new non-null derivative upon upstream exit
        //     which is used upstream the call (i.e. it is varied upstream)
        //    --> detected by deps.leftTimes(usefulOnExit) and column not identity.
        BoolVector columnNotIdentity = new BoolVector(deps.nCols);
        for (int i = deps.nRows - 1; i >= 0; --i) {
            if (deps.rows[i] != null) {
                columnNotIdentity.set(i, true);
                if (usefulOnExit.get(i)) {
                    columnNotIdentity.cumulOr(deps.rows[i]);
                }
            }
        }
        BoolVector result = deps.leftTimes(usefulOnExit).or(usefulOnExit);
        result.cumulAnd(columnNotIdentity);
        return result;
    }

    /**
     * Variant of getModifiedZonesT special for the cleaning after analysis.
     *
     * @return a vector with true at index i if diff of zone i will be
     * modified by the adjoint of the routine whose dependencies are "deps".
     * This is a sort of transposed equivalent of getFinalModifiedZones().
     */
    private BoolVector getFinalModifiedZonesT(BoolMatrix deps,
                                              BoolVector variedOnCall, BoolVector usefulOnExit) {
        // The 2 cases of explicitly overwritten diff vars that we want to detect are:
        //  1) vb is useful upon exit, and becomes passive upon entry (i.e. there should be a vb=0.0)
        //    --> detected by "true in usefulOnExit" and at the same time "all-zero column"
        //  2) vb may be passive or not, upon exit, but is varied upon entry and will have received
        //    some non-null derivative upon entry (i.e. there will be a vb = (vb +) C*wb)
        //    --> detected by "there is a non-null (and non Id) element in vb's column"
        //       and this element is on a row which is active upon exit (i.e. wb non null).
        //[llh] small doubt: what if v is passive at both ends, with a column that is
        //  all zero, but in the middle of curUnit, v becomes temporarily active ?
        //  => vb will have received a value, and maybe this values hasn't been cleaned to 0.0 ???
        //  then this method should add v in the result, and I'm afraid that it doesn't...
        BoolVector activeColumns = new BoolVector(deps.nCols);
        BoolVector nonZeroColumns = new BoolVector(deps.nCols);
        for (int i = deps.nRows - 1; i >= 0; --i) {
            if (deps.rows[i] == null) {
                nonZeroColumns.set(i, true);
            } else {
                nonZeroColumns.cumulOr(deps.rows[i]);
                if (usefulOnExit.get(i)) {
                    activeColumns.cumulOr(deps.rows[i]);
                }
            }
        }
        return nonZeroColumns.and(usefulOnExit).or(activeColumns.and(variedOnCall));
    }

    /**
     * Variant of getModifiedZonesT special for the root Unit or for debugged Units.
     * Not very well understood yet : TODO !
     * So far, this one is the same as getUpdateModifiedZonesT, but this might change...
     */
    private static BoolVector getRootModifiedZonesT(BoolMatrix deps, BoolVector usefulOnExit) {
        return getUpdateModifiedZonesT(deps, usefulOnExit);
    }

    /**
     * For every zone for which the info in "activeZones" is true,
     * (knowing that "activeZones" is a vector on zone ranks of kind "kind"),
     * marks this zone as "active" (i.e. active at least once).
     * Also finds the VariableDecl that corresponds to this zone and
     * annotates it as "differentiated" (i.e. there will be a diff VariableDecl).
     * Also, if this VariableDecl uses a structured type, declares
     * the corresponding parts of this type as "differentiated" too,
     * which is necessary to build the correct final differentiated type.
     */
    protected static void setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(BoolVector activeZones, SymbolTable symbolTable, int kind, int thisKindNDZ) {
        ZoneInfo zoneInfo;
        if (activeZones != null) {
            for (int i = thisKindNDZ; i >= 0; --i) {
                zoneInfo = symbolTable.declaredZoneInfo(i, kind);
                if (zoneInfo != null && activeZones.get(i)) {
                    zoneInfo.setOnceActive();
                }
            }
        }
        VariableDecl localVarDecl, ownerVarDecl;
        String accessName;
        boolean modified;
        Tree accessTree;
        for (int i = symbolTable.declaredZonesNb(kind) - 1; i >= 0; --i) {
            zoneInfo = symbolTable.declaredZoneInfo(i, kind);
            if (zoneInfo!=null && zoneInfo.accessTree!=null
                && (zoneInfo.realZoneNb != -1 || zoneInfo.ptrZoneNb != -1 || TapEnv.diffKind() == SymbolTableConstants.ALLKIND)
                && zoneInfo.isOnceActive()) {
                accessTree = zoneInfo.accessTree;
                accessName = ILUtils.baseName(accessTree);
                // accessName may be null for internal allocation zones (alloc(X) from X=malloc(8);)
                if (accessName == null || !(zoneInfo.ownerSymbolDecl instanceof VariableDecl)) {
                    if (zoneInfo.rootAccessType != null) {
                        TapList newActivityInfo = new TapList<>(null, null);
                        TapList toLeaf =
                            TapList.getSetFieldLocation(newActivityInfo, zoneInfo.accessTree, true);
                        toLeaf.head = Boolean.TRUE;
                        zoneInfo.rootAccessType.cumulActiveParts(newActivityInfo, symbolTable);
                    }
                } else {
                    localVarDecl = symbolTable.getVariableDecl(accessName) ;
                    ownerVarDecl = (VariableDecl)zoneInfo.ownerSymbolDecl ;
                    if (ownerVarDecl != null) {
                        if (!zoneInfo.isHidden) ownerVarDecl.setActive();
                        modified = ownerVarDecl.cumulActiveField(accessTree);
                        if (modified) {
                            setActiveTypeDecl(ownerVarDecl, symbolTable);
                            ownerVarDecl.type().cumulActiveParts(ownerVarDecl.activityInfo, symbolTable);
                        }
                    }
                    if (localVarDecl!=null && ownerVarDecl!=null && localVarDecl!=ownerVarDecl
                        && TapList.sameTree(localVarDecl.zones(), ownerVarDecl.zones())) {
                        // cf set08/lh068: 2 declarations with different types of the same C extern variable.
                        if (!zoneInfo.isHidden) localVarDecl.setActive();
                        modified = localVarDecl.cumulActiveField(accessTree);
                        if (modified) {
                            setActiveTypeDecl(localVarDecl, symbolTable);
                            localVarDecl.type().cumulActiveParts(localVarDecl.activityInfo, symbolTable);
                        }
                    }
                }
            }
        }
    }

    private static void setActiveTypeDecl(VariableDecl variableDecl, SymbolTable symbolTable) {
        WrapperTypeSpec typeSpec = variableDecl.type().baseTypeSpec(false);
        VariableDecl cRootVarDecl = TapEnv.get().origCallGraph().cRootSymbolTable().getVariableDecl(variableDecl.symbol);
        if (cRootVarDecl != null) {
            cRootVarDecl.setActive();
        }
        if (variableDecl.importedFrom != null) {
            variableDecl.importedFrom.first.setActive();
        }
        if (TypeSpec.isA(typeSpec.wrappedType, SymbolTableConstants.COMPOSITETYPE)) {
            String typeName = typeSpec.wrappedType.typeDeclName();
            if (typeName == null) {
                typeName = ((CompositeTypeSpec) typeSpec.wrappedType).name;
            }
            if (typeName != null) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeName);
                if (typeDecl == null) {
                    // dans le cas ou on n'importe que la variableDecl et pas son typeDecl
                    // chercher dans les modules importes:
                    typeDecl = getTypeDeclInModules(symbolTable.unit.importedModules(), typeName);
                    if (typeDecl == null) {
                        typeDecl = getTypeDeclInModules(symbolTable.unit.otherImportedModules, typeName);
                    }
                }
                if (typeDecl != null) {
                    typeDecl.setActive();
                }
            }
        }
    }

    private static TypeDecl getTypeDeclInModules(TapList<Unit> modules, String typeName) {
        TypeDecl result = null;
        while (modules != null && result == null) {
            result = modules.head.publicSymbolTable().getTypeDecl(typeName);
            if (result == null) {
                result = modules.head.privateSymbolTable().getTypeDecl(typeName);
            }
            modules = modules.tail;
        }
        return result;
    }

    private void mergeUsefulnessIntoVariedness(Unit unit,
                                               BlockStorage<TapList<BoolVector>> usefulnessesOut,
                                               BlockStorage<TapList<BoolVector>> variednessesIn) {
        TapList<Block> blocks = unit.allBlocks();
        Block block;
        setCurBlockEtc(unit.entryBlock());
        mergeUsefulnessIntoVariedness(
                usefulnessesOut.retrieve(unit.entryBlock()),
                variednessesIn.retrieve(unit.entryBlock()),
                unit.entryBlock().symbolTable);
        while (blocks != null) {
            block = blocks.head;
            setCurBlockEtc(block);
            mergeUsefulnessIntoVariedness(
                    usefulnessesOut.retrieve(block),
                    variednessesIn.retrieve(block),
                    block.symbolTable);
            blocks = blocks.tail;
        }
        setCurBlockEtc(unit.exitBlock());
        mergeUsefulnessIntoVariedness(
                usefulnessesOut.retrieve(unit.exitBlock()),
                variednessesIn.retrieve(unit.exitBlock()),
                unit.exitBlock().symbolTable);
        setCurBlockEtc(null);
    }

    private void mergeUsefulnessIntoVariedness(TapList<BoolVector> usefulList,
                                               TapList<BoolVector> variedList, SymbolTable symbolTable) {
        while (variedList != null && usefulList != null) {
            if (!TapEnv.debugActivity() && TapEnv.doUsefulness()) {
                variedList.head.cumulAnd(usefulList.head);
            }
            // TODO: improve efficiency by doing this only once, on the union of all BoolVector's in variedList:
            setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(variedList.head, symbolTable, diffKind, nDRZ);
            variedList = variedList.tail;
            usefulList = usefulList.tail;
        }
    }

    /**
     * Set the activity annotations on the current Unit.
     */
    private void presetActivityAnnotations(Unit unit, ActivityPattern pattern) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" Preset activity annotations on unit:" + curUnit + " with diffPattern " + pattern);
        }
        BlockStorage<TapList<BoolVector>> unitActivities = pattern.activities();
        BlockStorage<TapList<BoolVector>> unitUsefulnesses = pattern.usefulnesses();
        TapList<Block> inBlocks;
        Block block;
        curUnitZonesOfVarsHaveDiff = pattern.zonesOfVarsHaveDiff();
        inBlocks = new TapList<>(unit.exitBlock(),
                new TapList<>(unit.entryBlock(),
                        unit.allBlocks()));
        // disable trace temporarily (otherwise too verbose trace):
        boolean curAnalysisWasTraced = TapEnv.traceCurAnalysis();
        if (curAnalysisWasTraced) {
            TapEnv.setTraceCurAnalysis(false) ;
        }
        while (inBlocks != null) {
            block = inBlocks.head;
            presetActivityAnnotations(block, curAnalysisWasTraced,
                    unitActivities.retrieve(block),
                    unitUsefulnesses.retrieve(block));
            inBlocks = inBlocks.tail;
        }
        if (curAnalysisWasTraced) {
            TapEnv.setTraceCurAnalysis(true) ;
        }

        setFormalArgsActivity(pattern);

        //Add activity info into the types of the variables that will be differentiated
        // because of artificial activity added in the entry and exit blocks.
        setCurBlockEtc(unit.entryBlock());
        BoolVector activityAtBoundary =
                unitActivities.retrieve(unit.entryBlock()).head.copy();
        activityAtBoundary.cumulOr(unitActivities.retrieve(unit.exitBlock()).head);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" Activity at boundary of " + unit);
            TapEnv.printlnOnTrace("entry :" + unitActivities.retrieve(unit.entryBlock()).head);
            TapEnv.printlnOnTrace(" exit :" + unitActivities.retrieve(unit.exitBlock()).head);
        }
        setOnceActiveZonesAndAnnotateDiffDeclsAndTypes(activityAtBoundary, curSymbolTable, diffKind, nDRZ);
        setCurBlockEtc(null);
    }

    /**
     * Sets the activity annotations inside the Tree's of this Block.
     */
    private void presetActivityAnnotations(Block block, boolean traceResults,
                                           TapList<BoolVector> blockActivities, TapList<BoolVector> blockUsefulnesses) {
        setCurBlockEtc(block);
        TapList<Instruction> instructions = curBlock.instructions;
        // Initialize the traversal of the activity list:
        BoolVector downstreamActive = blockActivities.head;
        BoolVector downstreamUseful;
        BoolVector upstreamActive;
        Tree tree;
        blockActivities = blockActivities.tail;
        blockUsefulnesses = blockUsefulnesses.tail;
        int nbZonesForZonesOfVarsHaveDiff = // Because (obsolete) curUnitZonesOfVarsHaveDiff is poor design.
                (block instanceof EntryBlock || block instanceof ExitBlock
                        ? block.unit().publicSymbolTable().zonesNb(diffKind)
                        : block.unit().privateSymbolTable().zonesNb(diffKind));
        curUnitZonesOfVarsHaveDiff.cumulOr(downstreamActive, nbZonesForZonesOfVarsHaveDiff);
        if (traceResults) {
            TapEnv.printlnOnTrace("   Block " + curBlock);
        }
        while (instructions != null) {
            // Don't go inside for entry or exit Block:
            if (blockActivities != null) {
                curInstruction = instructions.head;
                upstreamActive = downstreamActive;
                downstreamActive = blockActivities.head;
                blockActivities = blockActivities.tail;
                downstreamUseful = blockUsefulnesses.head;
                blockUsefulnesses = blockUsefulnesses.tail;
                if (curSymbolTable.isFormalParamsLevel()
                        || curSymbolTable.basisSymbolTable().isFormalParamsLevel()) {
                    curUnitZonesOfVarsHaveDiff.cumulOr(downstreamActive);
                }
                tree = curInstruction.tree;
                variednessThroughExpression(tree, upstreamActive.copy(), null, true);
                usefulnessThroughExpression(tree, null, downstreamUseful.copy(), null, true);
                presetActivityAnnotations(tree, false, false, false, upstreamActive, downstreamActive,
                        downstreamUseful, false);
                if (traceResults) {
                    TapEnv.printlnOnTrace("     => " + ILUtils.toString(tree, curActivity));
                }
            }
            instructions = instructions.tail;
        }
        curInstruction = null;
    }

    /**
     * Set isActive to true on SymbolDecl's that are active because they are active formal args.
     */
    private void setFormalArgsActivity(ActivityPattern pattern) {
        Tree headerTree = pattern.unit().entryBlock().headTree();
        assert headerTree != null;
        Tree[] formalArgs = ILUtils.getArguments(headerTree).children();
        boolean[] formalArgsActivity = formalArgsActivity(pattern);
        SymbolDecl argDecl;
        String argName;
        for (int i = formalArgs.length - 1; i >= 0; --i) {
            if (formalArgsActivity[i]) {
                argName = ILUtils.getIdentString(formalArgs[i]);
                if (argName != null) {
                    argDecl = pattern.unit().privateSymbolTable().getSymbolDecl(argName);
                    if (argDecl != null) {
                        argDecl.setActive();
                    }
                }
            }
        }
    }

    /**
     * Presets Boolean "ActiveExpr" annotations on trees that contain active expressions. In
     * particular, for written expressions (e.g. left hand sides) sets the
     * annotation iff the new value written contains some active. In particular, for
     * procedure calls, sets the annotation on the procedure name tree iff the
     * differentiated program will differentiate this call, i.e. there will be a
     * call to the diff procedure.
     *
     * @param tree             The tree to be analyzed and annotated.
     * @param diffNeeded       is true when the diff of the result of the expression is needed
     * @param read             is true when the expression is a variable ref which is read
     * @param written          is true when it is a variable ref which is written
     *                         For instance in a(i)%x = b%y, only a(i)%x
     *                         is written (not a), and only i and b%y are read (not b).
     * @param upstreamActive   activity private zone vector upstream the current instruction
     * @param downstreamActive activity private zone vector downstream the current instruction
     * @param downstreamUseful usefulness private zone vector downstream the current instruction
     * @param insideRef        true when current tree is already a (1st son) subtree of
     *                         another reference expression. In that case, don't consider all zones of this tree.
     * @return The activity tree for this tree, that has just been computed
     * and used to set the "ActiveExpr" boolean annotation.
     */
    private TapList presetActivityAnnotations(Tree tree, boolean diffNeeded,
                                              boolean read, boolean written,
                                              BoolVector upstreamActive, BoolVector downstreamActive,
                                              BoolVector downstreamUseful, boolean insideRef) {
        if (tree != null) {
            TapList variedAnnot =
                    (TapList) ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "Varied");
            TapList usefulAnnot =
                    (TapList) ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "Useful");
            ActivityPattern.removeAnnotationForActivityPattern(tree, curActivity, "Varied");
            ActivityPattern.removeAnnotationForActivityPattern(tree, curActivity, "Useful");
            TapList activTree = null;
            if (TapList.oneTrue(variedAnnot) && TapList.oneTrue(usefulAnnot)) {
                activTree = TapList.copyTree(variedAnnot);
                activTree = TapList.cumulWithOper(activTree, usefulAnnot, SymbolTableConstants.CUMUL_AND);
            }
            // Special for validity test: diff of tests becomes needed.
            if (TapEnv.valid() && (tree.opCode()==ILLang.op_if)) {
                diffNeeded = true ;
            }
            TapIntList referencedZones = null;
            boolean active = false;
            switch (tree.opCode()) {
                case ILLang.op_plusAssign:
                case ILLang.op_minusAssign:
                case ILLang.op_timesAssign:
                case ILLang.op_divAssign:
                case ILLang.op_assign:
                    presetActivityAnnotations(tree.down(1), false, false, true, upstreamActive,
                            downstreamActive, downstreamUseful, false);
                    presetActivityAnnotations(tree.down(2),
                            diffNeeded || isAnnotatedActive(curActivity, tree.down(1)), true, false,
                            upstreamActive, downstreamActive, downstreamUseful, false);
                    if (isAnnotatedActive(curActivity, tree.down(2))) {
                        setAnnotatedActive(curActivity, tree);
                    }
                    break;
                case ILLang.op_ident:
                case ILLang.op_arrayAccess:
                case ILLang.op_arrayDeclarator:
                case ILLang.op_fieldAccess:
                case ILLang.op_pointerAccess: {
                    ToBool total = new ToBool(false);
                    if (!insideRef) {
                        referencedZones = curSymbolTable.listOfZonesOfValue(tree, total, curInstruction);
                    }
                    TapList accessibleZoneTree =
                            curSymbolTable.treeOfZonesOfValue(tree, total, curInstruction, null);
                    if (accessibleZoneTree != null) {
                        accessibleZoneTree = TapList.copyTree(accessibleZoneTree);
                        includePointedElementsInTree(accessibleZoneTree, null, null, true, false, true);
                    }
                    TapIntList accessibleZones = ZoneInfo.listAllZones(accessibleZoneTree, true);
                    TapIntList accessibleDiffKindVectorIndices =
                            mapZoneRkToKindZoneRk(accessibleZones, diffKind, curSymbolTable);
                    active = ((read
                               // If diffNeeded==false, this variable has a derivative but it is not
                               // used in the derivative of this statement => don't annotate as active:
                               && diffNeeded
                               && upstreamActive != null
                               && upstreamActive.intersects(accessibleDiffKindVectorIndices)
                               // Reject active vars used in a passive place (e.g. NINT(x)):
                               && TapList.oneTrue(activTree))
                              ||
                              (written
                               // for cases mix of pointers and implicit-zero diff, I have tried to replace
                               // the next 2 uses of "downstreamActive" with "downstreamUseful":
                               && downstreamActive != null
                               && downstreamActive.intersects(accessibleDiffKindVectorIndices)));
                    //Recurse on sub-trees if necessary:
                    if (tree.opCode() != ILLang.op_ident) {
                        presetActivityAnnotations(tree.down(1), false,
                                false, false, upstreamActive, downstreamActive, downstreamUseful, true);
                    }
                    if (tree.opCode() == ILLang.op_arrayAccess
                            || tree.opCode() == ILLang.op_pointerAccess) {
                        presetActivityAnnotations(tree.down(2), false,
                                true, false, upstreamActive, downstreamActive, downstreamUseful, false);
                    }

                    if (active) {
                        setAnnotatedActive(curActivity, tree);
                        Tree activeRoot = tree ;
                        if (tree.opCode()==ILLang.op_pointerAccess) {
                            activeRoot = tree.down(1) ;
                            if (activeRoot!=null &&
                                ((activeRoot.opCode()==ILLang.op_add || activeRoot.opCode()==ILLang.op_sub)
                                 && TypeSpec.isA(curSymbolTable.typeOf(activeRoot.down(1)),
                                                 SymbolTableConstants.POINTERTYPE))) {
                                activeRoot = activeRoot.down(1) ;
                            }
                            if (activeRoot!=null &&
                                (activeRoot.opCode()==ILLang.op_add
                                 && TypeSpec.isA(curSymbolTable.typeOf(activeRoot.down(2)),
                                                 SymbolTableConstants.POINTERTYPE))) {
                                activeRoot = activeRoot.down(2) ;
                            }
                        }
                        if (activeRoot!=null) {
                            String pointerName = ILUtils.baseName(activeRoot) ;
                            if (pointerName!=null) {
                                VariableDecl varDecl = curSymbolTable.getVariableDecl(pointerName);
                                if (varDecl != null) {
                                    varDecl.setActive();
                                }
                            }
                        }
                        TapIntList extendedZones = curSymbolTable.listOfZonesOfValue(tree, null, curInstruction);
                        while (extendedZones != null) {
                            if (extendedZones.head < curUnit.privateSymbolTable().declaredZonesNb(diffKind)) {
                                int diffKindRank = zoneRkToKindZoneRk(extendedZones.head, diffKind);
                                curUnitZonesOfVarsHaveDiff.set(diffKindRank, true);
                            }
                            extendedZones = extendedZones.tail;
                        }
                    }
                    break;
                }
                case ILLang.op_return:
                    diffNeeded = true ;
                    if (!ILUtils.isNullOrNone(tree.down(1))) {
                        String returnVarName = curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                        Tree identReturn = ILUtils.build(ILLang.op_ident, returnVarName);
                        referencedZones = curSymbolTable.listOfZonesOfValue(identReturn, null, curInstruction);
                    }
                    presetActivityAnnotations(tree.down(1), true, read, written, upstreamActive, downstreamActive, downstreamUseful, false);
                    presetActivityAnnotations(tree.down(2), false, read, written, upstreamActive, downstreamActive, downstreamUseful, false);
                    break;
                case ILLang.op_stop:
                    //Since a stop strongly influences the control, and therefore the computation
                    // of the derivatives, we must say that its controlling blocks are active.
                    break;
                case ILLang.op_call: {
                    Unit calledUnit = getCalledUnit(tree, curSymbolTable);
                    ActivityPattern curCalledActivity =
                            (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "multiActivityCalleePatterns");
                    MPIcallInfo messagePassingInfo =
                            MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), tree, curUnit.language(), curBlock);
                    int diffKindLength = calledUnit.publicZonesNumber(diffKind) ;
                    CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                    Tree[] actualParams = ILUtils.getArguments(tree).children();
                    TapList[] paramsCallActivities = new TapList[1+actualParams.length];
                    TapList[] paramsCallUsefulnesses = new TapList[1+actualParams.length];
                    TapList[] argsRead =
                        translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyR(), null, null, null, true,
                                                      tree, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                                      null, null, true, false);
                    TapList[] argsWritten =
                        translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), null, null, null, true,
                                                      tree, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                                      null, null, false, false);
                    BoolVector globCallPublicActivity = null;
                    BoolVector globExitPublicActivity = null;
                    if (curCalledActivity != null) {
                        globCallPublicActivity = curCalledActivity.callActivity();
                        globExitPublicActivity = curCalledActivity.exitActivity();
                    }
                    boolean callIsPassive =
                        (globCallPublicActivity!=null && globCallPublicActivity.isFalse(diffKindLength) &&
                         globExitPublicActivity!=null && globExitPublicActivity.isFalse(diffKindLength)) ;
                    boolean[] formalArgsActivity = formalArgsActivity(curCalledActivity);
                    paramsCallActivities[0] = null ;
                    paramsCallUsefulnesses[0] = activTree ;
                    for (int i=actualParams.length ; i>0 ; --i) {
                        //[llh] Note that the following "read" and "written" arguments are over-approximations
                        // as they blur together the RW status of each individual zone in the formal argument.
                        if (formalArgsActivity == null || i<=formalArgsActivity.length) {
                            boolean diffChildNeeded =
                                (diffNeeded
                                 || (!callIsPassive && formalArgsActivity != null && formalArgsActivity[i-1])) ;
                            paramsCallActivities[i] =
                                    presetActivityAnnotations(actualParams[i-1], diffChildNeeded,
                                            TapList.oneTrue(argsRead[i]), TapList.oneTrue(argsWritten[i]),
                                            upstreamActive, downstreamActive, downstreamUseful, false);
                        } else {
                            paramsCallActivities[i] = null ;
                        }
                        paramsCallUsefulnesses[i] = null ;
                    }

                    BoolVector publicContextVoC =
                        translateCallSiteDataToCallee(upstreamActive, paramsCallActivities, null, actualParams, true,
                                                      tree, curInstruction, arrow, diffKind);
                    BoolVector publicContextUoE =
                        translateCallSiteDataToCallee(downstreamActive, paramsCallUsefulnesses,
                                                      null, actualParams, false,
                                                      tree, curInstruction, arrow, diffKind);
                    if (calledUnit.rank() >= 0 &&
                            (messagePassingInfo == null || !messagePassingInfo.isPointToPoint())) {
                        if (globCallPublicActivity != null) {
                            publicContextVoC.cumulAnd(globCallPublicActivity);
                        }
                        if (globExitPublicActivity != null) {
                            publicContextUoE.cumulAnd(globExitPublicActivity);
                        }
                    }
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" ON CALL :" + tree + " [" + upstreamActive + "]->[" + downstreamActive + "]");
                        TapEnv.printlnOnTrace("    privatized as: [" + publicContextVoC + "]=>[" + publicContextUoE + "]");
                    }
                    filterVariedUsefulWithDeps(publicContextVoC, publicContextUoE, calledUnit, null, null);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("      filtered as: [" + publicContextVoC + "]=>[" + publicContextUoE + "]");
                    }
                    boolean thisCallActive =
                            !publicContextVoC.isFalse(diffKindLength) && !publicContextUoE.isFalse(diffKindLength);
                    // A Message-Passing call must be considered active
                    // if it uses channels and one of these channels is active and it passes differentiable-type values.
                    if (!thisCallActive && messagePassingInfo != null && messagePassingInfo.isOnDifferentiableType()) {
                        TapIntList channelZones =
                                messagePassingInfo.findMessagePassingChannelZones(curCallGraph);
                        if (intersectsKindZoneRks(upstreamActive, diffKind,
                                channelZones, null, curSymbolTable)) {
                            thisCallActive = true;
                        }
                    }
                    if (thisCallActive) {
                        setAnnotatedActive(curActivity, ILUtils.getCalledName(tree));
                        // To know when a func name in a declaration (e.g. external...) must be differentiated:
                        if (!TapList.containsEquals(curUnit.activeCalledNames, calledUnit.name())) {
                            curUnit.activeCalledNames =
                                    new TapList<>(calledUnit.name(), curUnit.activeCalledNames);
                        }
                    }
                    // si param formal is active et ILUtils.isAVarRef(actualArg, symbolTable)
                    // alors il faudra declarer une variable differentiee
                    if (thisCallActive && !calledUnit.isIntrinsic()) {
                        int nbArgs = actualParams.length;
                        if (nbArgs > calledUnit.functionTypeSpec().argumentsTypes.length) {
                            nbArgs = calledUnit.functionTypeSpec().argumentsTypes.length;
                        }
                        BoolVector privateFormalActivity = new BoolVector(nDRZ);
                        if (globCallPublicActivity != null) {
                                translateCalleeDataToCallSite(
                                        globCallPublicActivity, privateFormalActivity,
                                        null, actualParams, true,
                                        tree, curInstruction, arrow, diffKind,
                                        null, null, true, false) ;
                        }
                        if (globExitPublicActivity != null) {
                                translateCalleeDataToCallSite(
                                        globExitPublicActivity, privateFormalActivity,
                                        null, actualParams, false,
                                        tree, curInstruction, arrow, diffKind,
                                        null, null, false, false) ;
                        }
                        if (formalArgsActivity != null) {
                            for (int i = nbArgs - 1; i >= 0; --i) {
                                if (i < formalArgsActivity.length - 1
                                        && formalArgsActivity[i]
                                        && ILUtils.isAVarRef(actualParams[i], curSymbolTable)
                                        // cf BlockDifferentiator.tangentDifferentiateCall() & F90:v502
                                        // This last test varIsIntentIn() is dangerous: needed only for tangent mode ?
                                        && !curSymbolTable.varIsIntentIn(actualParams[i])) {
                                    VariableDecl actualArgDecl =
                                            curSymbolTable.getVariableDecl(ILUtils.baseName(actualParams[i]));
                                    // isDifferentiableVarDecl?
                                    if (actualArgDecl.isDifferentiableSymbolDecl(diffKind, !TapEnv.doActivity())) {
                                        actualArgDecl.setActive();
                                    }
                                    if (ILUtils.isAWritableIdentVarRef(actualParams[i], curSymbolTable)) {
                                        //Only if the actual arg is a writable "identVarRef"
                                        // (i.e. if actual arg contains only array or pointer accesses on op_ident)
                                        // and assuming that the actual arg is passive here,
                                        // we know that the the diff expression of that actual arg will be passed anyway,
                                        // which implies that the formal active zones of the actual arg must
                                        // be labelled as "once active":
                                        TapIntList zones =
                                                curSymbolTable.listOfZonesOfValue(actualParams[i], null, curInstruction);
                                        ZoneInfo zoneInfo;
                                        int diffIndex;
                                        while (zones != null) {
                                            zoneInfo = curSymbolTable.declaredZoneInfo(zones.head, SymbolTableConstants.ALLKIND);
                                            if (zoneInfo != null) {
                                                diffIndex = zoneInfo.kindZoneNb(diffKind);
                                                if (diffIndex >= 0 && privateFormalActivity.get(diffIndex)) {
                                                    zoneInfo.setOnceActive();
                                                }
                                            }
                                            zones = zones.tail;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
                case ILLang.op_ioCall: {
                    String ioAction = ILUtils.getIdentString(tree.down(1));
                    boolean writesInVars = ioAction.equals("read")
                            || ioAction.equals("accept") || ioAction.equals("decode");
                    // TODO: add the analysis of the ioSpec part expression.down(2)
                    presetActivityAnnotations(tree.down(3), false, !writesInVars, writesInVars,
                            upstreamActive, downstreamActive, downstreamUseful, false);
                    if (writesInVars) {
                        if (hasOneAnnotatedActive(curActivity, tree.down(3), curSymbolTable)) {
                            setAnnotatedActive(curActivity, tree);
                        }
                        setDifferentiatedIOActive(tree.down(3), upstreamActive);
                    }
                    break;
                }
                // (de)allocation is about pointers: wait for ReqExplicit analysis to decide for activity, if needed.
                case ILLang.op_allocate:
                case ILLang.op_deallocate:
                    break;
                case ILLang.op_address:
                    // C'est un peu trop grossier de dire written == true dans tous les cas
                    // mais ca marche... [TODO] essayer de faire + propre avec les annotations
                    // "varied" et "useful" des &x en argument de call, cf C:v109
                    presetActivityAnnotations(tree.down(1),
                            diffNeeded, read, true, upstreamActive,
                            downstreamActive, downstreamUseful, false);

                    if (isAnnotatedActive(curActivity, tree.down(1))) {
                        setAnnotatedActive(curActivity, tree);
                    }
                    break;
                case ILLang.op_varDeclaration:
                    Tree[] declarators = tree.down(3).children();
                    for (int i = declarators.length - 1; i >= 0; --i) {
                        presetActivityAnnotations(declarators[i], false, true, false, upstreamActive,
                                downstreamActive, downstreamUseful, false);
                    }
                    break;
                case ILLang.op_arrayConstructor: {
                    Tree[] values = tree.children();
                    for (int i = values.length - 1; i >= 0; --i) {
                        presetActivityAnnotations(values[i], diffNeeded, true, false, upstreamActive,
                                downstreamActive, downstreamUseful, false);
                    }
                    break;
                }
                default:
                    if (!tree.isAtom()) {
                        Tree[] subTrees = tree.children();
                        for (int i = subTrees.length - 1; i >= 0; i--) {
                            presetActivityAnnotations(subTrees[i], diffNeeded, read, written, upstreamActive,
                                    downstreamActive, downstreamUseful, false);
                        }
                    }
                    break;
            }
            if ((diffNeeded && TapList.oneTrue(activTree)) || active) {
                setAnnotatedActive(curActivity, tree);
            }
            if (TapList.oneTrue(activTree) || active) {
                // If current tree is an identifier (or so..), and it is active,
                // remember that for the declaration for the Adol-C mode.
                TapIntList extendedZones = referencedZones;
                while (extendedZones != null) {
                    if (extendedZones.head < curUnit.privateSymbolTable().declaredZonesNb(diffKind)) {
                        int diffKindRank = zoneRkToKindZoneRk(extendedZones.head, diffKind);
                        curUnitZonesOfVarsHaveDiff.set(diffKindRank, true);
                    }
                    extendedZones = extendedZones.tail;
                }
            }
            return activTree;
        } else {
            return null;
        }
    }

    private void setDifferentiatedIOActive(Tree ioArg, BoolVector zonesActiveHere) {
        if (ILUtils.isAVarRef(ioArg, curSymbolTable)) {
            if (intersectsExtendedDeclared(zonesActiveHere, diffKind,
                    ioArg, null, curInstruction,
                    null, curSymbolTable)) {
                curSymbolTable.getVariableDecl(ILUtils.baseName(ioArg)).setActive();
            }
        } else if (ioArg.opCode() == ILLang.op_iterativeVariableRef) {
            setDifferentiatedIOActive(ioArg.down(1), zonesActiveHere);
        } else if (ioArg.opCode() == ILLang.op_expressions) {
            for (int i = 1; i <= ioArg.length(); i++) {
                setDifferentiatedIOActive(ioArg.down(i), zonesActiveHere);
            }
        }
    }

    /**
     * Removes all activity annotations inside the Tree's of this Block.
     */
    private void removeActivityAnnotations(Block block) {
        TapList<Instruction> instructions = block.instructions;
        while (instructions != null) {
            removeActivityAnnotations(instructions.head.tree);
            instructions = instructions.tail;
        }
    }

    private void removeActivityAnnotations(Tree tree) {
        if (tree != null) {
            if (!tree.isAtom()) {
                Tree[] sons = tree.children();
                for (int i = sons.length - 1; i >= 0; --i) {
                    removeActivityAnnotations(sons[i]);
                }
            }
            ActivityPattern.removeAnnotationForActivityPattern(tree, curActivity, "ActiveExpr");
        }
    }

    /**
     * @param deps       the BoolMatrix of the dependency matrix just
     *                   before evaluation of "expression".
     * @param expression the expression to analyze
     *                   for differentiable dependencies.
     * @param arrives    If non-null, will be set to false upon exit if
     *                   evaluation of the expression does not terminate.
     * @return a TapList-tree of BoolVector's, with one branch
     * for each component of the structure returned by the expression, and
     * at the end of the branch the zones whose value at "origin" time
     * influence the expression component value in a differentiable way.
     * A "null" returned value means no dependence.
     */
    private TapList depsThroughExpression(BoolMatrix deps,
                                          Tree expression, ToBool arrives) {
        TapList result = null ;
        switch (expression.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                TapList rhsDeps =
                        depsThroughExpression(deps, expression.down(2), arrives);
                Tree lhs = expression.down(1);
                // For the very rare case where the lhs contains
                // expressions that modify deps...
                depsThroughExpression(deps, lhs, arrives);
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                if (curUnit.isFortran() &&
                        TypeSpec.isA(curSymbolTable.typeOf(lhs), SymbolTableConstants.COMPOSITETYPE)
                ) {
                    //In Fortran, assignment between records is allowed, and
                    // it recursively assigns the destinations of pointer fields !!
                    writtenZonesTree =
                            curSymbolTable.includePointedElementsInZonesTree(writtenZonesTree,
                                    curInstruction, null, true);
                }
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                && curInstruction.getWhereMask() == null
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                // A null rhsDeps means a dependency on nothing:
                // we must now make it explicit:
                if (rhsDeps == null) {
                    rhsDeps = new TapList<>(new BoolVector(nDRZ), null);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapIntList rksList =
                            mapZoneRkToKindZoneRk(ZoneInfo.listAllZones(writtenZonesTree, true), diffKind);
                    TapEnv.printlnOnTrace("Upon " + ILUtils.toString(expression) + ", " + writtenZonesTree + " (=>rks:" + rksList + ") receives (total:" + totalAccess + ") " + rhsDeps.toString(new int[]{0, nDRZ}));
                }
                setInfoPRZVTree(deps, writtenZonesTree, rhsDeps, null, totalAccess, diffKind);
                result = rhsDeps;
                break ;
            }
            case ILLang.op_call: {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace(" ---------- AD Dependency Analysis of procedure call "
                                          +ILUtils.toString(expression) + " : ----------");
                    TapEnv.printlnOnTrace("    incoming AD Dependencies Upstream:");
                    TapEnv.dumpBoolMatrixOnTrace(deps);
                }
                TapList depsOfResult = null ;
                TapList<Unit> calledUnits = getCalledUnits(expression);
                MPIcallInfo messagePassingInfo = null ;
                if (calledUnits!=null && calledUnits.tail==null) {
                    messagePassingInfo =
                        MPIcallInfo.getMessagePassingMPIcallInfo(calledUnits.head.name(), expression, curUnit.language(), curBlock);
                }
                if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                    depsThroughMPICall(expression, messagePassingInfo, calledUnits.head, deps, arrives) ;
                } else {
                    Tree[] actualParams = ILUtils.getArguments(expression).children();
                    TapList[] paramDeps = new TapList[1+actualParams.length];
                    for (int i=actualParams.length ; i>0 ; --i) {
                        paramDeps[i] = depsThroughExpression(deps, actualParams[i-1], arrives);
                    }
                    paramDeps[0] = null ;
                    if (TapEnv.traceCurAnalysis()) {
                        for (int i=0 ; i<paramDeps.length ; ++i) {
                            TapEnv.printlnOnTrace("    paramDeps["+i+(i==0 ? "=result" : "")+"] = "+paramDeps[i]) ;
                        }
                        TapEnv.printlnOnTrace();
                    }

                    if (calledUnits == null) {
                        TapEnv.toolError("Could not find called Unit for diff deps analysis on " + expression);
                    } else if (calledUnits.tail==null) {
                        depsOfResult = depsThroughSingleUnitCall(expression, calledUnits.head, deps, paramDeps, arrives) ;
                    } else {
                        // if this call is overloaded (to several Units),
                        //  arrange parallel propagation through each of them:
                        depsOfResult = depsThroughOverloadedCalls(expression, calledUnits, deps, paramDeps, arrives) ;
                    }
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" ---------- End AD Dependency Analysis of procedure call "
                                          +ILUtils.toString(expression) + " -------- dependency of result:"+depsOfResult);
                }
                result = depsOfResult;
                break ;
            }
            case ILLang.op_fieldAccess: {
                TapList valueDependency =
                        depsThroughExpression(deps, expression.down(1), arrives);
                Object res = TapList.nth(valueDependency,
                        ILUtils.getFieldRank(expression.down(2)));
                if (res instanceof BoolVector) {
                    result = new TapList<>(res, null);
                } else {
                    result = (TapList) res;
                }
                break ;
            }
            case ILLang.op_arrayAccess:
                depsThroughExpression(deps, expression.down(2), arrives);
                result = depsThroughExpression(deps, expression.down(1), arrives);
                break ;
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_bitfieldDeclarator:
                // These op_*Declarator trees occur only in declarations.
                // The part that we must analyze is only the possible assignment to
                // the root variable of the declarator:
                break ;
            case ILLang.op_pointerAccess:
                depsThroughExpression(deps, expression.down(2), arrives);
                //WARNING: this case continues into next case !!
            case ILLang.op_ident: {
                TapList zones = TapList.copyTree(
                        curSymbolTable.treeOfZonesOfValue(expression,
                                null, curInstruction, null));
                if (zones != null) {
                    includePointedElementsInTree(zones, null, null, true, false, true);
                    result = buildInfoPRZVTree(zones, deps, diffKind);
                }
                break ;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_complexConstructor:
                result = TapList.cumulWithOper(
                        depsOnlyForReal(
                                depsThroughExpression(deps, expression.down(1), arrives),
                                expression.down(1)),
                        depsOnlyForReal(
                                depsThroughExpression(deps, expression.down(2), arrives),
                                expression.down(2)),
                        SymbolTableConstants.CUMUL_OR);
                break ;
            case ILLang.op_minus:
                result = depsOnlyForReal(
                        depsThroughExpression(deps, expression.down(1), arrives),
                        expression.down(1));
                break ;
            case ILLang.op_ioCall: {
                ToBool exprTotal = new ToBool(false);
                String ioAction = ILUtils.getIdentString(expression.down(1));
                boolean writesInVars = ioAction.equals("read")
                        || ioAction.equals("accept")
                        || ioAction.equals("decode");
                Tree[] writtenRefs = expression.down(3).children();
                TapIntList writtenZones;
                for (int i = writtenRefs.length - 1; i >= 0; i--) {
                    depsThroughExpression(deps, writtenRefs[i], arrives);
                    if (writesInVars) {
                        writtenZones = curSymbolTable.listOfZonesOfValue(writtenRefs[i], exprTotal, curInstruction);
                        writtenZones = mapZoneRkToKindZoneRk(writtenZones, diffKind);
                        deps.overwriteDeps(writtenZones, null, exprTotal.get());
                    }
                }
                break ;
            }
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] indexes = expression.children();
                for (int i = indexes.length - 1; i >= 0; i--) {
                    depsThroughExpression(deps, indexes[i], arrives);
                }
                break ;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] expressions = expression.children();
                TapList cumulDeps = null;
                for (int i = expressions.length - 1; i >= 0; i--) {
                    cumulDeps = TapList.cumulWithOper(cumulDeps,
                            depsThroughExpression(deps, expressions[i], arrives), SymbolTableConstants.CUMUL_OR);
                }
                result = cumulDeps;
                break ;
            }
            case ILLang.op_constructorCall: {
                Tree[] expressions = expression.down(3).children();
                result = null;
                for (int i = expressions.length - 1; i >= 0; i--) {
                    result = new TapList<>(depsThroughExpression(deps, expressions[i], arrives), result);
                }
                break ;
            }
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_times:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_not:
            case ILLang.op_switch:
                depsThroughExpression(deps, expression.down(1), arrives);
                break ;
            case ILLang.op_ifExpression:
                depsThroughExpression(deps, expression.down(1), arrives);
                result = TapList.cumulWithOper(
                        TapList.copyTree(
                                depsOnlyForReal(depsThroughExpression(deps, expression.down(2), arrives),
                                        expression.down(2))),
                        depsOnlyForReal(depsThroughExpression(deps, expression.down(3), arrives),
                                expression.down(3)),
                        SymbolTableConstants.CUMUL_OR);
                break ;
            case ILLang.op_switchCases:
            case ILLang.op_nameEq:
                depsThroughExpression(deps, expression.down(2), arrives);
                break ;
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                result = depsOnlyForReal(
                        depsThroughExpression(deps, expression.down(2), arrives),
                        expression.down(2));
                break ;
            case ILLang.op_loop:
                depsThroughExpression(deps, expression.down(3), arrives);
                break ;
            case ILLang.op_forall:
                // vmp a verifier ...
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                break ;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                depsThroughExpression(deps, expression.down(2), arrives);
                depsThroughExpression(deps, expression.down(3), arrives);
                depsThroughExpression(deps, expression.down(4), arrives);
                break ;
            case ILLang.op_forallRangeSet:
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                break ;
            case ILLang.op_for:
            case ILLang.op_arrayTriplet:
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                depsThroughExpression(deps, expression.down(3), arrives);
                break ;
            case ILLang.op_iterativeVariableRef: {
                depsThroughExpression(deps, expression.down(2), arrives);
                Tree[] expressions = expression.down(1).children();
                result = null;
                for (int i = expressions.length - 1; i >= 0; i--) {
                    result = TapList.cumulWithOper(result,
                            depsThroughExpression(deps, expressions[i], arrives), SymbolTableConstants.CUMUL_OR);
                }
                break ;
            }
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
                depsThroughExpression(deps, expression.down(1), arrives);
                depsThroughExpression(deps, expression.down(2), arrives);
                break ;
            case ILLang.op_return: {
                depsThroughExpression(deps, expression.down(2), arrives);
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    String returnVarName = curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    Tree assignTree = ILUtils.turnReturnIntoAssign(expression, returnVarName);
                    result = depsThroughExpression(deps, assignTree, arrives);
                    ILUtils.resetReturnFromAssign(expression, assignTree);
                }
                break ;
            }
            case ILLang.op_stop:
                if (arrives != null) {
                    arrives.set(false);
                }
                break ;
            case ILLang.op_address:
                result = new TapList<>(null, depsThroughExpression(deps, expression.down(1), arrives));
                break ;
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_strings:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_break:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_allocate:
                // Here, in many cases, we would like to "reinitialize" the allocated zone(s) to non-dependent,
                // which would seem more accurate and consistent,
                // but (sorry) we can't because in general the allocated zone(s) is MULTI and therefore some
                // "parts" or "clones" of it (already allocated) may be active and they must remain so.
                // The same holds for deallocate.
                break ;
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
                // For the very rare case where the inside expression contains expressions that modify deps...
                depsThroughExpression(deps, expression.down(1), arrives);
                break ;
            case ILLang.op_sizeof:
                result = depsThroughExpression(deps, expression.down(1), arrives);
                break ;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break ;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break ;
            case ILLang.op_varDeclaration: {
                Tree[] decls = expression.down(3).children();
                inADeclaration = true;
                for (int i = decls.length - 1; i >= 0; i--) {
                    if (decls[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(decls[i]);
                        depsThroughExpression(deps, decls[i], arrives);
                        ILUtils.resetAssignFromInitDecl(decls[i]);
                    }
                }
                inADeclaration = false;
                break ;
            }
            default:
                TapEnv.toolWarning(-1, "(Compute AD Dependencies) Unexpected operator : " + expression.opName());
        }
        return result ;
    }

    private TapList depsThroughOverloadedCalls(Tree expression, TapList<Unit> calledUnits,
                                               BoolMatrix deps, TapList[] paramDeps, ToBool arrives) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("  Call "+ILUtils.toString(expression)+" is overloaded to: "+calledUnits) ;
        }
        BoolMatrix accumulatedFinalDeps = null ;
        TapList accumulatedResult = null ;
        arrives.set(false) ;
        while (calledUnits!=null) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- Overloaded call: "+calledUnits.head+" ----------------") ;
            }
            BoolMatrix copyOfDeps = deps.copy() ;
            ToBool oneArrives = new ToBool(true) ;
            TapList functionResultDepsTree =
                depsThroughSingleUnitCall(expression, calledUnits.head, copyOfDeps, paramDeps, oneArrives) ;
            if (oneArrives.get()) arrives.set(true) ;

            if (accumulatedResult==null) {
                accumulatedResult = functionResultDepsTree ;
            } else {
                accumulatedResult =
                    TapList.cumulWithOper(accumulatedResult, functionResultDepsTree, SymbolTableConstants.CUMUL_OR) ;
            }
            if (accumulatedFinalDeps==null) {
                accumulatedFinalDeps = copyOfDeps ;
            } else {
                accumulatedFinalDeps.cumulOr(copyOfDeps, true, -1) ;
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- End overloaded call: "+calledUnits.head+" ----------------") ;
            }
            calledUnits = calledUnits.tail ;
        }
        deps.rows = accumulatedFinalDeps.rows ;
        return accumulatedResult ;
    }

    private TapList depsThroughSingleUnitCall(Tree expression, Unit calledUnit,
                                              BoolMatrix deps, TapList[] paramDeps, ToBool arrives) {
        TapList depsOfResult;
        BoolMatrix calleeDiffDeps = getDiffDeps(calledUnit, dependencies, diffKind);
        if (calleeDiffDeps == null) {
            if (arrives != null) {
                arrives.set(false);
            }
            depsOfResult = null;
        } else {
            Tree[] actualParams = ILUtils.getArguments(expression).children();
            CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
            calleeDiffDeps = filterByValue(calleeDiffDeps, diffKind, calledUnit) ;

            // Translate the dependency info upstream the call into a
            // dependency info at the entry of the callee:
            BoolMatrix depsOnCallee =
                translateCallSiteDataToCallee(deps, paramDeps, null, actualParams, true,
                                              expression, curInstruction, arrow,
                                              diffKind) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("    propagated on callee entry:");
                TapEnv.dumpOnTrace(depsOnCallee);
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("    composed with callee AD Dependencies:");
                TapEnv.dumpOnTrace(calleeDiffDeps);
                TapEnv.printlnOnTrace();
            }
            depsOnCallee = calleeDiffDeps.times(depsOnCallee) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Gives AD Dependencies on callee exit:");
                TapEnv.dumpOnTrace(depsOnCallee);
                TapEnv.printlnOnTrace();
            }
            BoolVector passesThroughCall = new BoolVector(deps.nRows) ;
            translateCalleeDataToCallSite(calledUnit.focusToKind(calledUnit.unitInOutPossiblyW(), diffKind),
                                          passesThroughCall, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind,
                                          null, null, false, false) ;
            BoolVector callSiteKilled = new BoolVector(deps.nRows) ;
            translateCalleeDataToCallSite(calledUnit.focusToKind(calledUnit.unitInOutCertainlyW(), diffKind),
                                          callSiteKilled, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind,
                                          null, null, false, true) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    passesThroughCall: "+passesThroughCall) ;
                TapEnv.printlnOnTrace("    passesAroundCall:  "+callSiteKilled.not()) ;
                TapEnv.printlnOnTrace();
            }
            TapList[] paramDataS =
                translateCalleeDataToCallSite(depsOnCallee, deps, null, actualParams, false,
                                              expression, curInstruction, arrow,
                                              diffKind,
                                              passesThroughCall, callSiteKilled.not(), false, null) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    outgoing AD dependencies Downstream:") ;
                TapEnv.dumpOnTrace(deps) ;
                TapEnv.printlnOnTrace();
                TapEnv.printlnOnTrace("    and outgoing for result:"+paramDataS[0]) ;
            }
            depsOfResult = paramDataS[0];
        }
        return depsOfResult;
    }

    private void depsThroughMPICall(Tree expression, MPIcallInfo messagePassingInfo, Unit calledUnit, BoolMatrix deps, ToBool arrives) {
        TapList messagePassingChannels =
            new TapList<>(messagePassingInfo.findMessagePassingChannelZones(curCallGraph),
                          null);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" ---------- AD Dependency Analysis of message-passing call " + ILUtils.toString(expression) + " : ----------");
            TapEnv.printlnOnTrace("    incoming AD dependencies Upstream:");
            TapEnv.dumpBoolMatrixOnTrace(deps);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("    channels:" + messagePassingChannels + " from channel info " + messagePassingInfo);
        }
        Tree exprToChannels = messagePassingInfo.sentExprToChannel();
        if (exprToChannels != null) {
            if (calledUnit.isC() &&
                TypeSpec.isA(curSymbolTable.typeOf(exprToChannels), SymbolTableConstants.POINTERTYPE)) {
                exprToChannels =
                    ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprToChannels));
            }
            TapList sentDeps = depsThroughExpression(deps, exprToChannels, arrives);
            if (sentDeps == null) {
                sentDeps = new TapList<>(new BoolVector(nDRZ), null);
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Deps sent to channel: " + sentDeps);
            }
            setInfoPRZVTree(deps, messagePassingChannels, sentDeps, null, false, diffKind);
        }
        Tree exprFromChannels = messagePassingInfo.receivedExprFromChannel();
        if (exprFromChannels != null) {
            ToBool toTotal = new ToBool(false);
            TapList writtenZonesTree;
            if (calledUnit.isC() &&
                TypeSpec.isA(curSymbolTable.typeOf(exprFromChannels), SymbolTableConstants.POINTERTYPE)) {
                exprFromChannels =
                    ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprFromChannels));
            }
            writtenZonesTree =
                curSymbolTable.treeOfZonesOfValue(exprFromChannels, toTotal, curInstruction, null);
            boolean totalAccess = referenceIsTotal(toTotal.get(), writtenZonesTree);
            //[llh 9Jan17] TODO: ref is total ONLY IF the passed length is the same as the buffer's actual size!!
            TapList receivedDeps =
                buildInfoPRZVTree(messagePassingChannels, deps, diffKind);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Deps received from channel: " + receivedDeps);
            }
            setInfoPRZVTree(deps, writtenZonesTree, receivedDeps, null, totalAccess, diffKind);
            // Any channel used cannot be "Identity" any more:
            TapIntList channelZones = ZoneInfo.listAllZones(messagePassingChannels, true);
            while (channelZones != null) {
                int chIndex = zoneRkToKindZoneRk(channelZones.head, diffKind);
                if (deps!=null && deps.rows[chIndex]==null) {
                    deps.rows[chIndex] = new BoolVector(nDRZ);
                    deps.rows[chIndex].set(chIndex, true);
                }
                channelZones = channelZones.tail;
            }
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    outgoing AD dependencies Downstream:");
            TapEnv.dumpBoolMatrixOnTrace(deps);
            TapEnv.printlnOnTrace();
        }
    }

    /**
     * Analyzes activity through "expression". "varieds" is the
     * BoolVector of the varied zones just before evaluation of Tree
     * "expression". This function returns the activity "boolean-tree" of the value
     * of the curent expression. By side-effect, this function
     * pushes "varied" through "expression", i.e. when this function terminates,
     * "varieds" represents the varied zones just after evaluation of
     * "expression" .
     * "arrives" is set to false when the control does not reach
     * the end of the expression (e.g. "stop")
     * If "arrives" is null, this "stop" checking is just not done.
     *
     * @param annotate true to force final creation of activity annotations.
     */
    private TapList variednessThroughExpression(Tree expression,
                                                BoolVector varieds, ToBool arrives, boolean annotate) {
        TapList resultVariedness = null;
        switch (expression.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                Tree lhs = expression.down(1);
                resultVariedness =
                        variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                if (resultVariedness == null) {
                    resultVariedness = new TapList<>(Boolean.FALSE, null);
                }
                TapList lhsVariedness = variednessThroughExpression(lhs, varieds, arrives, annotate);
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                if (curUnit.isFortran() &&
                        TypeSpec.isA(curSymbolTable.typeOf(lhs), SymbolTableConstants.COMPOSITETYPE)
                ) {
                    //In Fortran, assignment between records is allowed, and
                    // it recursively assigns the destinations of pointer fields !!
                    writtenZonesTree =
                            curSymbolTable.includePointedElementsInZonesTree(writtenZonesTree,
                                    curInstruction, null, true);
                }
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (TapEnv.traceCurAnalysis()) {
                    TapIntList rksList = mapZoneRkToKindZoneRk(ZoneInfo.listAllZones(writtenZonesTree, true), diffKind);
                    TapEnv.printlnOnTrace("     through " + ILUtils.toString(expression) + ", " + writtenZonesTree + " (=>rks:" + rksList + ") receives (total:" + totalAccess + ") " + resultVariedness.toString(null));
                }
                setInfoBoolTreeToExtendedDeclaredZones(varieds, writtenZonesTree,
                        resultVariedness, null, totalAccess, diffKind);
                if (annotate) {
                    lhsVariedness = TapList.copyTree(lhsVariedness);
                    lhsVariedness = TapList.cumulWithOper(lhsVariedness, resultVariedness, SymbolTableConstants.CUMUL_OR);
                    ActivityPattern.setAnnotationForActivityPattern(lhs, curActivity, "Varied", lhsVariedness);
                }
                break;
            }
            case ILLang.op_call: {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    ---------- Fwd Varied through procedure call "
                                          +ILUtils.toString(expression)+" : ----------");
                    TapEnv.printlnOnTrace("    Incoming fwd varied: "+varieds.toString(nDRZ));
                }
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                TapList[] paramVariednesses = new TapList[1+actualParams.length];
                for (int i=actualParams.length ; i>0 ; --i) {
                    paramVariednesses[i] =
                        variednessThroughExpression(actualParams[i-1], varieds, arrives, annotate);
                }
                paramVariednesses[0] = null ;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printOnTrace("    and for parameters:[");
                    for (int i=0 ; i<paramVariednesses.length ; ++i) {
                        TapEnv.printOnTrace((i==0 ? "result:" : ", ")+paramVariednesses[i]);
                    }
                    TapEnv.printlnOnTrace("]");
                }
                TapList<Unit> calledUnits = getCalledUnits(expression);
                MPIcallInfo messagePassingInfo = null ;
                if (calledUnits!=null && calledUnits.tail==null) {
                    messagePassingInfo =
                        MPIcallInfo.getMessagePassingMPIcallInfo(calledUnits.head.name(), expression, curUnit.language(), curBlock);
                }
                if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                    variednessThroughMPICall(expression, messagePassingInfo, calledUnits.head,
                                             varieds, paramVariednesses, arrives, annotate) ;
                } else {
                    if (calledUnits == null) {
                        TapEnv.toolError("Could not find called Unit for varied analysis on " + expression);
                    } else if (calledUnits.tail==null) {
                        variednessThroughSingleUnitCall(expression, calledUnits.head, varieds,
                                                        paramVariednesses, arrives, annotate) ;
                    } else {
                        // if this call is overloaded (to several Units),
                        //  arrange parallel propagation through each of them:
                        variednessThroughOverloadedCalls(expression, calledUnits, varieds,
                                                         paramVariednesses, arrives, annotate) ;
                    }
                    resultVariedness = paramVariednesses[0] ;
                }
                break;
            }
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] expressions = expression.children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    variednessThroughExpression(expressions[i], varieds, arrives, annotate);
                }
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] expressions = expression.children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    resultVariedness =
                            TapList.cumulWithOper(resultVariedness,
                                    variednessThroughExpression(expressions[i], varieds, arrives, annotate), SymbolTableConstants.CUMUL_OR);
                }
                break;
            }
            case ILLang.op_constructorCall: {
                Tree[] expressions = expression.down(3).children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    resultVariedness = new TapList<>(variednessThroughExpression(expressions[i],
                            varieds, arrives, annotate),
                            resultVariedness);
                }
                break;
            }
            case ILLang.op_fieldAccess: {
                TapList valueVariedness =
                        variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                Object result = TapList.nth(valueVariedness,
                        ILUtils.getFieldRank(expression.down(2)));
                if (result instanceof Boolean) {
                    resultVariedness = new TapList<>(result, null);
                } else {
                    resultVariedness = (TapList) result;
                }
                break;
            }
            case ILLang.op_address: {
                resultVariedness =
                        new TapList<>(null, variednessThroughExpression(expression.down(1), varieds, arrives, annotate));
                break;
            }
            case ILLang.op_pointerAccess: {
                // For the very rare case where the pointer contains sub-exprs that modify varieds:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                TapList zones = TapList.copyTree(
                        curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null));
                if (zones != null) {
                    includePointedElementsInTree(zones, null, null, true, false, true);
                    resultVariedness = buildInfoBoolTreeOfDeclaredZones(zones, varieds, null, diffKind, curSymbolTable);
                }
                break;
            }
            case ILLang.op_arrayAccess:
                // For the very rare case where the indexes contain sub-exprs that modify varieds:
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                resultVariedness = variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                break;
            case ILLang.op_ident: {
                TapList zones = TapList.copyTree(
                        curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null));
                if (zones != null) {
                    includePointedElementsInTree(zones, null, null, true, false, true);
                    resultVariedness = buildInfoBoolTreeOfDeclaredZones(zones, varieds, null, diffKind, curSymbolTable);
                }
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_complexConstructor:
                resultVariedness = TapList.copyTree(
                        activityOnlyForReal(
                                variednessThroughExpression(expression.down(1), varieds, arrives, annotate),
                                expression.down(1)));
                resultVariedness = TapList.cumulWithOper(resultVariedness,
                        activityOnlyForReal(
                                variednessThroughExpression(expression.down(2), varieds, arrives, annotate),
                                expression.down(2)),
                        SymbolTableConstants.CUMUL_OR);
                break;
            case ILLang.op_minus:
                resultVariedness = activityOnlyForReal(variednessThroughExpression(expression.down(1), varieds, arrives, annotate), expression.down(1));
                break;
            case ILLang.op_ioCall: {
                Tree expr;
                ToBool writtenTotal = new ToBool(false);
                boolean isIORead = ILUtils.isIORead(expression);
                Tree[] exprs = expression.down(3).children();
                TapList writtenZonesTree;
                for (int i = exprs.length - 1; i >= 0; i--) {
                    expr = exprs[i];
                    variednessThroughExpression(expr, varieds, arrives, annotate);
                    if (isIORead) {
                        writtenZonesTree = curSymbolTable.treeOfZonesOfValue(
                                expr, writtenTotal, curInstruction, null);
                        if (referenceIsTotal(writtenTotal.get(), writtenZonesTree)) {
                            setInfoBoolTreeToExtendedDeclaredZones(varieds, writtenZonesTree, false, null, true, diffKind);
                            if (annotate) {
                                ActivityPattern.setAnnotationForActivityPattern(expr, curActivity, "Varied", null);
                            }
                        }
                    }
                }
                break;
            }
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_not:
            case ILLang.op_switch:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                break;
            case ILLang.op_ifExpression:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                resultVariedness = TapList.copyTree(
                        activityOnlyForReal(
                                variednessThroughExpression(expression.down(2), varieds, arrives, annotate),
                                expression.down(2)));
                resultVariedness = TapList.cumulWithOper(resultVariedness,
                        activityOnlyForReal(
                                variednessThroughExpression(expression.down(3), varieds, arrives, annotate),
                                expression.down(3)),
                        SymbolTableConstants.CUMUL_OR);
                break;
            case ILLang.op_nameEq:
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_unary:
                resultVariedness = variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                break;
            case ILLang.op_loop:
                variednessThroughExpression(expression.down(3), varieds, arrives, annotate);
                break;
            case ILLang.op_forall:
                // vmp a verifier ...
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                break;
            case ILLang.op_do:
            case ILLang.op_forallRangeTriplet:
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(3), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(4), varieds, arrives, annotate);
                break;
            case ILLang.op_forallRangeSet:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                break;
            case ILLang.op_for:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(3), varieds, arrives, annotate);
                break;
            case ILLang.op_arrayTriplet:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(3), varieds, arrives, annotate);
                break;
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
                variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                break;
            case ILLang.op_iterativeVariableRef: {
                Tree[] expressions = expression.down(1).children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    resultVariedness =
                            TapList.cumulWithOper(resultVariedness,
                                    variednessThroughExpression(expressions[i], varieds, arrives, annotate), SymbolTableConstants.CUMUL_OR);
                }

                break;
            }
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_strings:
            case ILLang.op_label:
            case ILLang.op_none:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_break:
                break;
            case ILLang.op_stop:
                if (arrives != null) {
                    arrives.set(false);
                }
                break;
            case ILLang.op_return:
                variednessThroughExpression(expression.down(2), varieds, arrives, annotate);
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    resultVariedness =
                            variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                    if (resultVariedness == null) {
                        resultVariedness = new TapList<>(Boolean.FALSE, null);
                    }
                    String returnVarName =
                            curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    Tree lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                    ToBool lhsTotal = new ToBool(false);
                    TapList writtenZonesTree =
                            curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                    setInfoBoolTreeToExtendedDeclaredZones(varieds, writtenZonesTree,
                            resultVariedness, null, true, diffKind);
                }
                break;
            case ILLang.op_allocate:
                // Here, in many cases, we would like to "reinitialize" the allocated zone(s) to non-varied,
                // which would seem more accurate and consistent, avoiding reset of allocated diff to 0.0,
                // (See e.g. u in bspline in set11/vmp17)
                // but (sorry) we can't because in general the allocated zone(s) is MULTI and therefore some
                // "parts" or "clones" of it (already allocated) may be active and they must remain so.
                // The same holds for deallocate.
                break;
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
                break;
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_bitfieldDeclarator:
                // These op_*Declarator trees occur only in declarations.
                // The part that we must analyze is only the possible assignment to
                // the root variable of the declarator:
            case ILLang.op_sizeof:
                resultVariedness = variednessThroughExpression(expression.down(1), varieds, arrives, annotate);
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            case ILLang.op_common:
                break;
            case ILLang.op_equivalence:
                break;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_varDeclaration: {
                Tree[] expressions = expression.down(3).children();
                TapList[] varied = new TapList[expressions.length];
                int rankFirstInit = -1;
                inADeclaration = true;
                for (int i = 0; i <= expressions.length - 1; i++) {
                    if (expressions[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(expressions[i]);
                        varied[i] = variednessThroughExpression(expressions[i], varieds, arrives, annotate);
                        ILUtils.resetAssignFromInitDecl(expressions[i]);
                        if (rankFirstInit == -1) {
                            rankFirstInit = i;
                        }
                    }
                }
                inADeclaration = false;
                if (rankFirstInit != -1) {
                    resultVariedness = TapList.copyTree(activityOnlyForReal(varied[rankFirstInit], expressions[rankFirstInit]));
                    for (int i = rankFirstInit+1; i <= expressions.length - 1; i++) {
                        if (varied[i] != null) {
                            resultVariedness = TapList.cumulWithOper(
                                    resultVariedness, activityOnlyForReal(varied[i], expressions[i]), SymbolTableConstants.CUMUL_OR);
                        }
                    }
                }
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(Compute AD variedness) Unexpected operator: " + expression.opName());
                break;
        }
        if (annotate) {
            ActivityPattern.setAnnotationForActivityPattern(expression, curActivity, "Varied", resultVariedness);
        }
        return resultVariedness;
    }

    private void variednessThroughOverloadedCalls(Tree expression, TapList<Unit> calledUnits, BoolVector varieds,
                                                  TapList[] paramVariednesses, ToBool arrives, boolean annotate) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("  Call "+ILUtils.toString(expression)+" is overloaded to: "+calledUnits) ;
        }
        BoolVector accumulatedFinalVarieds = null ;
        TapList accumulatedResult = null ;
        if (arrives!=null) arrives.set(false) ;
        while (calledUnits!=null) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- Overloaded call: "+calledUnits.head+" ----------------") ;
            }
            BoolVector copyOfVarieds = varieds.copy() ;
            ToBool oneArrives = new ToBool(true) ;
            variednessThroughSingleUnitCall(expression, calledUnits.head, copyOfVarieds,
                                            paramVariednesses, oneArrives, annotate) ;
            if (arrives!=null && oneArrives.get()) arrives.set(true) ;

            if (accumulatedResult==null) {
                accumulatedResult = paramVariednesses[0] ;
            } else {
                accumulatedResult =
                    TapList.cumulWithOper(accumulatedResult, paramVariednesses[0], SymbolTableConstants.CUMUL_OR) ;
            }
            if (accumulatedFinalVarieds==null) {
                accumulatedFinalVarieds = copyOfVarieds ;
            } else {
                accumulatedFinalVarieds.cumulOr(copyOfVarieds) ;
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- End overloaded call: "+calledUnits.head+" ----------------") ;
            }
            calledUnits = calledUnits.tail ;
        }
        varieds.setCopy(accumulatedFinalVarieds) ;
        paramVariednesses[0] = accumulatedResult ;
    }

    private void variednessThroughSingleUnitCall(Tree expression, Unit calledUnit, BoolVector varieds,
                                                 TapList[] paramVariednesses, ToBool arrives, boolean annotate) {
        BoolMatrix calleeDiffDeps = getDiffDeps(calledUnit, dependencies, diffKind);
        calleeDiffDeps = filterByValue(calleeDiffDeps, diffKind, calledUnit) ;
        int calleeNDRZ = calledUnit.publicZonesNumber(diffKind) ;
        Tree[] actualParams = ILUtils.getArguments(expression).children();
        CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);

        // Translate the variedness info upstream the call into a
        // variedness info at the entry of the callee:
        BoolVector variedsOnCallee =
            translateCallSiteDataToCallee(varieds, paramVariednesses, null, actualParams, true,
                                          expression, curInstruction, arrow,
                                          diffKind) ;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    propagated and bufferized on callee entry: "+variedsOnCallee.toString(calleeNDRZ));
        }
        // Accumulate this call site's upstream variedness into
        // the general variedness context upon entry in the called Unit:
        bufferizeCallContext(calledUnit, expression, variedsOnCallee, null, null, VARIED_ON_CALL);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    composed with callee AD Dependencies:");
            TapEnv.dumpOnTrace(calleeDiffDeps);
            TapEnv.printlnOnTrace();
        }
        // Compute variedness at the exit of the called Unit:
        if (calleeDiffDeps != null) {
            variedsOnCallee = calleeDiffDeps.times(variedsOnCallee);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Gives varied on callee exit: "+variedsOnCallee.toString(calleeNDRZ));
            }
            BoolVector passesThroughCall = new BoolVector(nDRZ) ;
            translateCalleeDataToCallSite(calledUnit.focusToKind(calledUnit.unitInOutPossiblyW(), diffKind),
                                          passesThroughCall, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind,
                                          null, null, false, false) ;
            BoolVector callSiteKilled = new BoolVector(nDRZ) ;
            translateCalleeDataToCallSite(calledUnit.focusToKind(calledUnit.unitInOutCertainlyW(), diffKind),
                                          callSiteKilled, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind,
                                          null, null, false, true) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    passesThroughCall: "+passesThroughCall.toString(nDRZ)) ;
                TapEnv.printlnOnTrace("    passesAroundCall:  "+callSiteKilled.not().toString(nDRZ)) ;
                TapEnv.printlnOnTrace();
            }
            // Translate and accumulate variedness at the exit from the called Unit
            // into variedness downstream the call site.
            // Only the info on variables written by the call must be translated and accumulated.
            // Previous info on variables killed by the call do not survive through the call.
            TapList[] paramDataS =
                translateCalleeDataToCallSite(variedsOnCallee, varieds, null, actualParams, false,
                                              expression, curInstruction, arrow,
                                              diffKind,
                                              passesThroughCall, callSiteKilled.not(), false, false) ;
            paramVariednesses[0] = paramDataS[0] ;
        } else {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Called Unit does not return. Reset fwd varied.") ;
            }
            varieds.setFalse() ;
            if (arrives != null) {
                arrives.set(false);
            }
            paramVariednesses[0] = new TapList<>(Boolean.FALSE, null);
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    Gives outgoing fwd varied: "+varieds.toString(nDRZ));
            TapEnv.printlnOnTrace("    Plus result's fwd varied: "+paramVariednesses[0]);
            TapEnv.printlnOnTrace("    --------------------------------------------------------------------------");
            TapEnv.printlnOnTrace();
        }
    }

    private void variednessThroughMPICall(Tree expression, MPIcallInfo messagePassingInfo,
                                          Unit calledUnit, BoolVector varieds, TapList[] paramVariednesses,
                                          ToBool arrives, boolean annotate) {
        TapList messagePassingChannels =
            new TapList<>(messagePassingInfo.findMessagePassingChannelZones(curCallGraph), null);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    ---------- Message-Passing call " + ILUtils.toString(expression) + " on channels " + messagePassingChannels);
        }
        Tree[] actualParams = ILUtils.getArguments(expression).children();
        CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
        BoolVector inputPublicVariedness =
            translateCallSiteDataToCallee(varieds, paramVariednesses, null, actualParams, true,
                                          expression, curInstruction, arrow,
                                          diffKind) ;
        boolean channelsVaried = TapList.oneTrue(buildInfoBoolTreeOfDeclaredZones(messagePassingChannels, varieds,
                                                                                  null, diffKind, curSymbolTable));
        // Accumulate this call site's upstream variedness into the general
        // variedness context upon entry in the called Message-Passing procedure:
        bufferizeCallContext(calledUnit, expression, inputPublicVariedness, channelsVaried, null, VARIED_ON_CALL);
        Tree exprToChannels = messagePassingInfo.sentExprToChannel();
        if (exprToChannels != null) {
            if (calledUnit.isC()
                && TypeSpec.isA(curSymbolTable.typeOf(exprToChannels), SymbolTableConstants.POINTERTYPE)) {
                exprToChannels =
                    ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprToChannels));
            }
            TapList resultVariedness =
                variednessThroughExpression(exprToChannels, varieds, arrives, annotate);
            if (resultVariedness == null) {
                resultVariedness = new TapList<>(Boolean.FALSE, null);
            }
            setInfoBoolTreeToExtendedDeclaredZones(varieds, messagePassingChannels,
                                                   resultVariedness, null, false, diffKind);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    set channels to " + resultVariedness);
                TapEnv.printlnOnTrace("    --> " + varieds);
            }
        }
        Tree exprFromChannels = messagePassingInfo.receivedExprFromChannel();
        if (exprFromChannels != null) {
            if (calledUnit.isC() &&
                TypeSpec.isA(curSymbolTable.typeOf(exprFromChannels), SymbolTableConstants.POINTERTYPE)) {
                exprFromChannels =
                    ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprFromChannels));
            }
            TapList receivedVarieds =
                buildInfoBoolTreeOfDeclaredZones(messagePassingChannels, varieds,
                                                 null, diffKind, curSymbolTable);
            ToBool toTotal = new ToBool(false);
            TapList writtenZonesTree =
                curSymbolTable.treeOfZonesOfValue(exprFromChannels, toTotal,
                                                  curInstruction, null);
            boolean totalAccess = referenceIsTotal(toTotal.get(), writtenZonesTree);
            //[llh 9Jan17] TODO: ref is total ONLY IF the passed length is the same as the buffer's actual size
            setInfoBoolTreeToExtendedDeclaredZones(varieds, writtenZonesTree,
                                                   receivedVarieds, null, totalAccess, diffKind);
            if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("    received " + receivedVarieds + " from channels, put into " + writtenZonesTree);
                    TapEnv.printlnOnTrace("    --> " + varieds);
                }
        }
    }

    /**
     * Propagates the "usefulness" information (which means whether
     * the given "expression" is actually useful or not) upwards across the
     * given "expression". As a result, side-effect updates "usefuls",
     * which is the complete usefulness info just after/downstream the
     * "expression", transforming it into the usefulness info before/upstream.
     *
     * @param expression the expression through which we must propagate usefulness (backwards)
     * @param usefuls    the vector of useful zones just downstream the expression
     * @param usefulness the TapList-tree of the result of expression (if any) that are useful downstream
     * @param arrives    when provided non-null, will be set to false when the control
     *                   does not reach back the start of the expression (e.g. "stop").
     *                   This is just not used in the present state of the code.
     * @param annotate   true on final creation of activity annotations.
     */
    private void usefulnessThroughExpression(Tree expression, TapList usefulness,
                                             BoolVector usefuls, ToBool arrives, boolean annotate) {
        if (annotate) {
            ActivityPattern.setAnnotationForActivityPattern(expression, curActivity, "Useful", usefulness);
        }
        switch (expression.opCode()) {
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign: {
                Tree lhs = expression.down(1);
                ToBool lhsTotal = new ToBool(false);
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                if (curUnit.isFortran() &&
                        TypeSpec.isA(curSymbolTable.typeOf(lhs), SymbolTableConstants.COMPOSITETYPE)
                ) {
                    //In Fortran, assignment between records is allowed, and
                    // it recursively assigns the destinations of pointer fields !!
                    writtenZonesTree =
                            curSymbolTable.includePointedElementsInZonesTree(writtenZonesTree,
                                    curInstruction, null, true);
                }
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), writtenZonesTree)
                                && expression.opCode() == ILLang.op_assign
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                usefulness = TapList.cumulWithOper(usefulness,
                        buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                usefuls, null, diffKind, curSymbolTable),
                        SymbolTableConstants.CUMUL_OR);
                if (annotate) {
                    ActivityPattern.setAnnotationForActivityPattern(expression, curActivity, "Useful", usefulness);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapIntList rksList = mapZoneRkToKindZoneRk(ZoneInfo.listAllZones(writtenZonesTree, true), diffKind);
                    TapEnv.printlnOnTrace("     up through " + ILUtils.toString(expression) + ", " + writtenZonesTree + " (=>rks:" + rksList + ") provides (total:" + totalAccess + ") " + (usefulness==null ? "null usefulness" : usefulness.toString(null)));
                }
                if (totalAccess) {
                    setInfoBoolTreeToExtendedDeclaredZones(usefuls, writtenZonesTree, false, null, true, diffKind);
                }
                usefulnessThroughExpression(expression.down(2), usefulness,
                        usefuls, arrives, annotate);
                usefulnessThroughExpression(lhs, null, usefuls, arrives, annotate);
                if (annotate) {
                    ActivityPattern.setAnnotationForActivityPattern(lhs, curActivity, "Useful", usefulness);
                }
                break;
            }
            case ILLang.op_call: {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace();
                    TapEnv.printlnOnTrace("    ---------- Bwd Useful up through procedure call "
                                          +ILUtils.toString(expression) + " : ----------");
                    TapEnv.printlnOnTrace("    Downcoming bwd useful: " + usefuls.toString(nDRZ));
                    TapEnv.printlnOnTrace("    Plus result's bwd useful:" + (usefulness == null ? "null" : usefulness.toString(null)));
                }
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                TapList[] paramUsefulnesses = new TapList[1+actualParams.length];
                paramUsefulnesses[0] = usefulness ;
                for (int i=1 ; i<paramUsefulnesses.length ; ++i) paramUsefulnesses[i] = null ;

                // Find the Unit(s) of the called function:
                TapList<Unit> calledUnits = getCalledUnits(expression);
                MPIcallInfo messagePassingInfo = null ;
                if (calledUnits!=null && calledUnits.tail==null) {
                    messagePassingInfo =
                        MPIcallInfo.getMessagePassingMPIcallInfo(calledUnits.head.name(), expression, curUnit.language(), curBlock);
                }
                if (messagePassingInfo != null && messagePassingInfo.isPointToPoint()) {
                    usefulnessThroughMPICall(expression, messagePassingInfo, calledUnits.head,
                                             paramUsefulnesses, usefuls, arrives, annotate) ;
                } else {
                    if (calledUnits == null) {
                        TapEnv.toolError("Could not find called Unit for useful analysis on " + expression);
                    } else if (calledUnits.tail==null) {
                        usefulnessThroughSingleUnitCall(expression, calledUnits.head, usefuls,
                                                        paramUsefulnesses, arrives, annotate) ;
                    } else {
                        // if this call is overloaded (to several Units),
                        //  arrange parallel propagation through each of them:
                        usefulnessThroughOverloadedCalls(expression, calledUnits,usefuls,
                                                         paramUsefulnesses, arrives, annotate) ;
                    }
                }
                for (int i=actualParams.length ; i>0 ; --i) {
                    usefulnessThroughExpression(actualParams[i-1], paramUsefulnesses[i],
                                                usefuls, arrives, annotate);
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("    Final after arguments   : " + usefuls);
                    TapEnv.printlnOnTrace("    --------------------------------------------------------------------------");
                    TapEnv.printlnOnTrace();
                }
                break;
            }
            case ILLang.op_pointerAccess: {
                // For the very rare case where the "index" subtree contain differentiable assignments:
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                TapList upperUsefulness = new TapList(Boolean.FALSE, usefulness) ;
                usefulnessThroughExpression(expression.down(1), upperUsefulness, usefuls, arrives, annotate);
                break ;
            }
            case ILLang.op_fieldAccess: {
                // Look for the composite type on which expression is accessing a field:
                WrapperTypeSpec exprType = curSymbolTable.typeOf(expression.down(1));
                // Peel off possible array types (due to F90 array notation):
                while (TypeSpec.isA(exprType, SymbolTableConstants.ARRAYTYPE)) {
                    exprType = ((ArrayTypeSpec)exprType.wrappedType()).elementType() ;
                }
                TapList upperUsefulness = usefulness ;
                if (TypeSpec.isA(exprType, SymbolTableConstants.COMPOSITETYPE)) {
                    // Rebuild a usefulness for the full record, containing only the given "usefulness" for the current field
                    CompositeTypeSpec recordType = (CompositeTypeSpec)exprType.nestedLevelType() ;
                    int nbFields = recordType.fields.length ;
                    upperUsefulness = null ;
                    String fieldName = ILUtils.getIdentString(expression.down(2)) ;
                    if (!curSymbolTable.isCaseDependent()) {
                        fieldName = fieldName.toLowerCase();
                    }
                    int fieldRank = recordType.namedFieldRank(fieldName) ;
                    for (int i=nbFields-1 ; i>=0 ; --i) {
                        if (i==fieldRank)
                            upperUsefulness = new TapList(usefulness, upperUsefulness) ;
                        else
                            upperUsefulness = new TapList(new TapList(Boolean.FALSE, null), upperUsefulness) ;
                    }
                } else if (TypeSpec.isA(exprType, SymbolTableConstants.CLASSTYPE)) {
// !! Classes not implemented yet:
//                     // Rebuild a usefulness for the full object, containing only the given "usefulness" for the current field
//                     ClassType classType = (ClassTypeSpec)exprType.nestedLevelType() ;
                }
                usefulnessThroughExpression(expression.down(1), upperUsefulness, usefuls, arrives, annotate);
                break ;
            }
            case ILLang.op_arrayAccess:
                // For the very rare case where the "index" subtrees contain differentiable assignments:
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                // WARNING: this case continues into next case !!
            case ILLang.op_ident: {
                TapList zonesTree = TapList.copyTree(
                        curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null));
                includePointedElementsInTree(zonesTree, null, null, true, false, true);
                setInfoBoolTreeToExtendedDeclaredZones(usefuls, zonesTree,
                        usefulness, null, false, diffKind);
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_complexConstructor:
                usefulnessThroughExpression(expression.down(1),
                        usefulness, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(2),
                        usefulness, usefuls, arrives, annotate);
                break;
            case ILLang.op_minus:
                usefulnessThroughExpression(expression.down(1),
                        usefulness, usefuls, arrives, annotate);
                break;
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                usefulnessThroughExpression(expression.down(2),
                        usefulness, usefuls, arrives, annotate);
                break;
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_not:
            case ILLang.op_switch: {
                TapList usefulTest = null;
                // In the case when "domain of validity" is activated (-directValid),
                // every test expression is useful:
                if (TapEnv.valid()) {
                    usefulTest = new TapList<>(Boolean.TRUE, null);
                }
                usefulnessThroughExpression(expression.down(1), usefulTest, usefuls,
                        arrives, annotate);
                break;
            }
            case ILLang.op_ifExpression:
                usefulnessThroughExpression(expression.down(1), null, usefuls,
                        arrives, annotate);
                usefulnessThroughExpression(expression.down(2),
                        usefulness, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(3),
                        usefulness, usefuls, arrives, annotate);
                break;
            case ILLang.op_ioCall: {
                Tree expr;
                ToBool writtenTotal = new ToBool(false);
                boolean isIORead = ILUtils.isIORead(expression);
                Tree[] exprs = expression.down(3).children();
                TapList writtenZonesTree;
                for (int i = exprs.length - 1; i >= 0; i--) {
                    expr = exprs[i];
                    if (isIORead) {
                        writtenZonesTree = curSymbolTable.treeOfZonesOfValue(
                                expr, writtenTotal, curInstruction, null);
                        if (referenceIsTotal(writtenTotal.get(), writtenZonesTree)) {
                            setInfoBoolTreeToExtendedDeclaredZones(usefuls, writtenZonesTree, false, null, true, diffKind);
                        }
                    }
                    usefulnessThroughExpression(expr, null, usefuls, arrives, annotate);
                }
                break;
            }
            case ILLang.op_loop:
                /* For the very rare case where the inside contain expressions
                 * that modify usefuls:*/
                usefulnessThroughExpression(expression.down(3), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_forall:
                // vmp a verifier ...
                usefulnessThroughExpression(expression.down(1), null, usefuls,
                        arrives, annotate);
                usefulnessThroughExpression(expression.down(2), null, usefuls,
                        arrives, annotate);
                break;
            case ILLang.op_forallRangeTriplet:
            case ILLang.op_do:
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(3), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(4), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_forallRangeSet:
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_for:
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(3), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_arrayTriplet:
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(3), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_iterativeVariableRef: {
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                Tree[] expressions = expression.down(1).children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    usefulnessThroughExpression(expressions[i], usefulness, usefuls, arrives, annotate);
                }
                break;
            }
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat: {
                TapList childUsefulness = null;
                // In the case when "domain of validity" is activated (-directValid),
                // every test expression may be useful:
                if (TapEnv.valid()) {
                    childUsefulness = new TapList<>(Boolean.TRUE, null);
                }
                usefulnessThroughExpression(expression.down(1), childUsefulness, usefuls, arrives, annotate);
                usefulnessThroughExpression(expression.down(2), childUsefulness, usefuls, arrives, annotate);
                break;
            }
            case ILLang.op_forallRanges:
            case ILLang.op_expressions: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    usefulnessThroughExpression(sons[i], null, usefuls, arrives, annotate);
                }
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    usefulnessThroughExpression(sons[i], usefulness, usefuls, arrives, annotate);
                }
                break;
            }
            case ILLang.op_constructorCall: {
                Tree[] expressions = expression.down(3).children();
                TapList nextUsefulness;
                for (Tree tree : expressions) {
                    if (usefulness == null) {
                        nextUsefulness = null;
                    } else if (usefulness.head instanceof TapList) {
                        nextUsefulness = (TapList) usefulness.head;
                        usefulness = usefulness.tail;
                    } else    //error case: non-record := constructorCall(record,...)
                    {
                        nextUsefulness = usefulness;
                    }
                    usefulnessThroughExpression(tree, nextUsefulness, usefuls, arrives, annotate);
                }
                break;
            }
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_strings:
            case ILLang.op_none:
            case ILLang.op_label:
            case ILLang.op_nameEq:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
                break;
            case ILLang.op_stop:
                if (arrives != null) {
                    arrives.set(false);
                }
                break;
            case ILLang.op_return:
                usefulnessThroughExpression(expression.down(2), null, usefuls, arrives, annotate);
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    String returnVarName =
                            curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
                    Tree lhs = ILUtils.build(ILLang.op_ident, returnVarName);
                    ToBool lhsTotal = new ToBool(false);
                    TapList writtenZonesTree =
                            curSymbolTable.treeOfZonesOfValue(lhs, lhsTotal, curInstruction, null);
                    if (writtenZonesTree != null) {
                        includePointedElementsInTree(writtenZonesTree, null, null, true, false, false);
                    }
                    usefulness = TapList.cumulWithOper(usefulness,
                            buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                    usefuls, null, diffKind, curSymbolTable),
                            SymbolTableConstants.CUMUL_OR);
                    if (annotate) {
                        ActivityPattern.setAnnotationForActivityPattern(expression, curActivity, "Useful", usefulness);
                    }
                    usefulnessThroughExpression(expression.down(1), usefulness,
                            usefuls, arrives, annotate);
                }
                break;
            case ILLang.op_address: {
                TapList zonesTree =
                        curSymbolTable.treeOfZonesOfValue(expression, null, curInstruction, null);
                setInfoBoolTreeToExtendedDeclaredZones(usefuls, zonesTree, usefulness, null, false, diffKind);
                // Recovery on wrong code: if taking the address of a non-ref expression e.g. a call:
                usefulnessThroughExpression(expression.down(1), (usefulness == null ? null : usefulness.tail),
                        usefuls, arrives, annotate);
                break;
            }
            case ILLang.op_allocate:
                // Here, in many cases, we would like to "reinitialize" the allocated zone(s) to non-useful,
                // which would seem more accurate and consistent,
                // but (sorry) we can't because in general the allocated zone(s) is MULTI and therefore some
                // "parts" or "clones" of it (not yet deallocated) may be useful and they must remain so.
                break;
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
                // For the very rare case where the inside expression contains expressions that modify deps...
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_sizeof:
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                break;
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_bitfieldDeclarator:
                // ^ These op_*Declarator trees occur only in declarations.
                // The part that we must analyze is only the possible assignment to
                // the root variable of the declarator:
                usefulnessThroughExpression(expression.down(1), null, usefuls, arrives, annotate);
                break;
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_break:
            case ILLang.op_continue:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                break;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                break;
            case ILLang.op_varDeclaration: {
                Tree[] sons = expression.down(3).children();
                inADeclaration = true;
                for (int i = 0; i <= sons.length - 1; i++) {
                    if (sons[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(sons[i]);
                        usefulnessThroughExpression(sons[i], null, usefuls, arrives, annotate);
                        ILUtils.resetAssignFromInitDecl(sons[i]);
                    }
                }
                inADeclaration = false;
                break;
            }
            default:
                TapEnv.toolWarning(-1, "(Compute AD usefulness) Unexpected operator: " + expression.opName());
                break;
        }
    }

    private void usefulnessThroughOverloadedCalls(Tree expression, TapList<Unit> calledUnits, BoolVector usefuls,
                                                 TapList[] paramUsefulnesses, ToBool arrives, boolean annotate) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("  Call "+ILUtils.toString(expression)+" is overloaded to: "+calledUnits) ;
        }
        BoolVector accumulatedFinalUsefuls = null ;
        TapList[] accumulatedResult = new TapList[paramUsefulnesses.length] ;
        for (int i=accumulatedResult.length-1 ; i>=0 ; --i) {
            accumulatedResult[i] = null ;
        }
        if (arrives!=null) arrives.set(false) ;
        while (calledUnits!=null) {
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- Overloaded call: "+calledUnits.head+" ----------------") ;
            }
            BoolVector copyOfUsefuls = usefuls.copy() ;
            ToBool oneArrives = new ToBool(true) ;
            usefulnessThroughSingleUnitCall(expression, calledUnits.head, copyOfUsefuls,
                                            paramUsefulnesses, arrives, annotate) ;
            if (arrives!=null && oneArrives.get()) arrives.set(true) ;

            for (int i=accumulatedResult.length-1 ; i>0 ; --i) {
                if (accumulatedResult[i]==null) {
                    accumulatedResult[i] = paramUsefulnesses[i] ;
                } else {
                    accumulatedResult[i] =
                        TapList.cumulWithOper(accumulatedResult[i], paramUsefulnesses[i], SymbolTableConstants.CUMUL_OR) ;
                }
            }
            if (accumulatedFinalUsefuls==null) {
                accumulatedFinalUsefuls = copyOfUsefuls ;
            } else {
                accumulatedFinalUsefuls.cumulOr(copyOfUsefuls) ;
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  -------------- End overloaded call: "+calledUnits.head+" ----------------") ;
            }
            calledUnits = calledUnits.tail ;
        }
        usefuls.setCopy(accumulatedFinalUsefuls) ;
        for (int i=accumulatedResult.length-1 ; i>0 ; --i) {
            paramUsefulnesses[i] = accumulatedResult[i] ;
        }
    }

    private void usefulnessThroughSingleUnitCall(Tree expression, Unit calledUnit, BoolVector usefuls,
                                                 TapList[] paramUsefulnesses, ToBool arrives, boolean annotate) {
        BoolMatrix calleeDiffDeps = getDiffDeps(calledUnit, dependencies, diffKind);
        calleeDiffDeps = filterByValue(calleeDiffDeps, diffKind, calledUnit) ;
        int calleeNDRZ = calledUnit.publicZonesNumber(diffKind) ;
        Tree[] actualParams = ILUtils.getArguments(expression).children();
        CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);

//CODE THAT USED TO WORK WELL BUT NOW ERRORS ON set10/mix25:
//         BoolVector usefulsOnCallee =
//              translateCallSiteDataToCallee(usefuls, paramUsefulnesses, null, actualParams, false,
//                                            expression, curInstruction, arrow,
//                                            diffKind, true) ;
//         // Prepare the special usefulness info to be sent to the downstream end of the callee.
//         // It is the same as usefulsOnCallee, but removing info on zones that are passed by value,
//         // because this info does not communicate between
//         // the end of callee and the sequel of call site.
//         // EXCEPTION specific to Useful analysis: even if passed by value, some arguments
//         // will have their diff passed by reference. This happens as an optimization in
//         // reverse mode for variables that are only read inside the call (cf set08/lh003).
//         // This implies that the corresponding Usefulness info (which actually concerns
//         // the diff) must *not* be removed when prepared for the callee.
//         BoolVector usefulsForCallee = usefulsOnCallee.and(zonesByRefForUsefuls(arrow, diffKind)) ;
//         // Accumulate this call site's downstream public usefulness into
//         // the general usefulness context upon exit for the called Unit:
//         bufferizeCallContext(calledUnit, expression, usefulsForCallee, null, usefulsOnCallee/*Seems useless, should rather disappear, but apparently this changes but changes set10/mix33, set10/mix34, set11/vpc02, set11/vpf02 ? TODO:clarify that */, USEFUL_ON_EXIT);
//END OLD CODE

//OLD CODE PRINTS FOR COMPARISON/UNDERSTANDING:
        BoolVector oldvect =
             translateCallSiteDataToCallee(usefuls, paramUsefulnesses, null, actualParams, false,
                                           expression, curInstruction, arrow,
                                           diffKind, true) ;
        BoolVector oldforbuff = oldvect.and(zonesByRefForUsefuls(arrow, diffKind)) ;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    OLD vector propagated:  ["+oldvect.toString(calleeNDRZ)+"]");
            TapEnv.printlnOnTrace("    OLD vector bufferized:  ["+oldforbuff.toString(calleeNDRZ)+"]");
        }


        // Prepare usefulness info to be composed locally with the calleeDiffDeps:
        BoolVector usefulsOnCallee =
            translateCallSiteDataToCallee(usefuls, paramUsefulnesses, null, actualParams, false,
                                          expression, curInstruction, arrow, diffKind) ;
        // Detect zones of the callee that (even if they are passed by value and therefore do not inherit
        // usefulness from the callee downstream this call) will receive an adjoint value *by reference*
        // from the adjoint of the downstream part, and therefore the adjoint of the callee must use two
        // distinct adjoint values to implement the adjoint arithmetic correctly (cf e.g. set10/mix25, set08/lh003).
        BoolVector nonZeroAdjointComing =
            translateCallSiteDataToCallee(usefuls, paramUsefulnesses, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind) ;

        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace(" FUTURE vector propagated:  ["+usefulsOnCallee.toString(calleeNDRZ)+"]");
            TapEnv.printlnOnTrace(" FUTURE vector bufferized:  ["+usefulsOnCallee.toString(calleeNDRZ)+"]");
            TapEnv.printlnOnTrace("MEMORIZED nonZero adjoint:  ["+nonZeroAdjointComing.toString(calleeNDRZ)+"]");
        }

        // Accumulate this call site's downstream public usefulness into
        // the general usefulness context upon exit for the called Unit:
        bufferizeCallContext(calledUnit, expression, oldforbuff/*usefulsOnCallee*/, null, nonZeroAdjointComing, USEFUL_ON_EXIT);
        usefulsOnCallee = oldvect ;//Was not present!!
//END NEW CODE

        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    composed with callee AD Dependencies:");
            TapEnv.dumpOnTrace(calleeDiffDeps);
        }
        // Compute usefulness at the entry of the called Unit.
        TapList[] paramDataS = null ;
        if (calleeDiffDeps != null) {
            usefulsOnCallee = calleeDiffDeps.leftTimes(usefulsOnCallee) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Gives useful on callee entry: " + usefulsOnCallee.toString(calleeNDRZ));
            }
            BoolVector callSiteKilled = new BoolVector(nDRZ) ;
            translateCalleeDataToCallSite(calledUnit.focusToKind(calledUnit.unitInOutCertainlyW(), diffKind),
                                          callSiteKilled, null, actualParams, true,
                                          expression, curInstruction, arrow, diffKind,
                                          null, null, false, true) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    passesAroundCall:  "+callSiteKilled.not().toString(nDRZ)) ;
                TapEnv.printlnOnTrace();
            }
            // Translate and accumulate usefulness at the entry of the called Unit
            // into usefulness upstream the call site.
            // Only the info on variables ?USED? written? by the call must be translated and accumulated.
            // Previous info on variables killed by the call do not survive through the call.
            paramDataS =
                translateCalleeDataToCallSite(usefulsOnCallee, usefuls, null, null, true,
                                              expression, curInstruction, arrow,
                                              diffKind,
                                              null, callSiteKilled.not(), true, false) ;
        } else { // This means that called unit stops: reinitialize the usefulness upstream.
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Called Unit does not return. Reset bwd useful.") ;
            }
            if (arrives != null) {
                arrives.set(false);
            }
            usefuls.setFalse() ;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    Gives upgoing bwd useful: " + usefuls);
            if (paramDataS!=null) {
                TapEnv.printOnTrace("    Plus params bwd useful: [");
                for (int i=0 ; i<paramDataS.length ; ++i) {
                    TapEnv.printOnTrace((i==0?"(result:0)":", "+i)+":"+paramDataS[i]);
                }
                TapEnv.printlnOnTrace("]");
            }
        }
        for (int i=paramUsefulnesses.length-1 ; i>0 ; --i) {
            paramUsefulnesses[i] = (paramDataS==null ? null : paramDataS[i]) ;
        }
    }

    private void usefulnessThroughMPICall(Tree expression, MPIcallInfo messagePassingInfo, Unit calledUnit,
                                          TapList[] paramUsefulnesses, BoolVector usefuls,
                                          ToBool arrives, boolean annotate) {
        TapList<TapIntList> messagePassingChannels =
            new TapList<>(messagePassingInfo.findMessagePassingChannelZones(curCallGraph), null);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("    ---------- Message-Passing call " + ILUtils.toString(expression) + " on channels " + messagePassingChannels);
        }
        Tree[] actualParams = ILUtils.getArguments(expression).children();
        CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
        BoolVector outputPublicUsefulness =
            translateCallSiteDataToCallee(usefuls, paramUsefulnesses, null, actualParams, false,
                                          expression, curInstruction, arrow,
                                          diffKind) ;
        boolean channelsUseful = TapList.oneTrue(buildInfoBoolTreeOfDeclaredZones(messagePassingChannels, usefuls,
                                                                                  null, diffKind, curSymbolTable));
        // Accumulate this call site's downstream public usefulness into the general
        // usefulness context upon exit for the called Message-Passing procedure:
        bufferizeCallContext(calledUnit, expression, outputPublicUsefulness, channelsUseful, null, USEFUL_ON_EXIT);
        Tree exprFromChannels = messagePassingInfo.receivedExprFromChannel();
        if (exprFromChannels != null) {
            if (calledUnit.isC() &&
                TypeSpec.isA(curSymbolTable.typeOf(exprFromChannels), SymbolTableConstants.POINTERTYPE)) {
                exprFromChannels =
                    ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprFromChannels));
            }
            ToBool toTotal = new ToBool(false);
            TapList writtenZonesTree =
                curSymbolTable.treeOfZonesOfValue(exprFromChannels, toTotal, curInstruction, null);
            TapList receivedUsefuls = buildInfoBoolTreeOfDeclaredZones(writtenZonesTree,
                                                                       usefuls, null, diffKind, curSymbolTable);
            if (referenceIsTotal(toTotal.get(), writtenZonesTree)) {
                //[llh 9Jan17] TODO: ref is total ONLY IF the passed length is the same as the buffer's actual size
                setInfoBoolTreeToExtendedDeclaredZones(usefuls, writtenZonesTree,
                                                       false, null, true, diffKind);
            }
            setInfoBoolTreeToExtendedDeclaredZones(usefuls, messagePassingChannels,
                                                   receivedUsefuls, null, false, diffKind);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    set channels to " + receivedUsefuls + " ( computed from " + writtenZonesTree + ')');
                TapEnv.printlnOnTrace("    --> " + usefuls);
            }
        }
        Tree exprToChannels = messagePassingInfo.sentExprToChannel();
        if (exprToChannels != null) {
            if (calledUnit.isC() &&
                TypeSpec.isA(curSymbolTable.typeOf(exprToChannels), SymbolTableConstants.POINTERTYPE)) {
                exprToChannels = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(exprToChannels));
            }
            TapList usefulness = buildInfoBoolTreeOfDeclaredZones(messagePassingChannels,
                                                                  usefuls, null, diffKind, curSymbolTable) ;
//WAS             TapList usefulness = TapList.cumulWithOper(usefulness,
//                                  buildInfoBoolTreeOfDeclaredZones(messagePassingChannels,
//                                                                   usefuls, null, diffKind, curSymbolTable),
//                                  SymbolTableConstants.CUMUL_OR);
            usefulnessThroughExpression(exprToChannels, usefulness, usefuls, arrives, annotate);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    propagating " + usefulness + " back from channels into " + exprToChannels);
                TapEnv.printlnOnTrace("    --> " + usefuls);
            }
        }
    }

    /** Returns the vector of the "kind" zones of the destination Unit
     * that are passed by reference during a call following the CallArrow arrow.
     * EXCEPTION: zones not overwritten by the destination Unit are considered
     * "by-reference" because their derivative will be passed by reference. */
    private static BoolVector zonesByRefForUsefuls(CallArrow arrow, int kind) {
        Unit unit = arrow.destination ;
        BoolVector result = new BoolVector(unit.publicZonesNumber(kind)) ;
        result.setTrue() ;
        ZoneInfo zi;
        int i, ki ;
        for (i=unit.paramElemsNb()-1 ; i>=0 ; --i) {
            zi = unit.paramElemZoneInfo(i);
            ki = zi.kindZoneNb(kind) ;
            if (ki!=-1 && zi.isParameter()
                && !(zi.accessTree!=null && ILUtils.isAccessedThroughPointer(zi.accessTree))
                && arrow.takesArgumentByValue(zi.index)
                && unit.unitInOutPossiblyW().get(i)) {
                result.set(ki, false) ;
            }
        }
        return result ;
    }

    /**
     * Store into the global buffer "callContextsBuffer" the present input variedness or output usefulness
     * (following argument "contextSort") context for a given call. This will be
     * accumulated in the final call context of the called unit only when
     * both variedness and usefulness contexts for this call site are known.
     * Also stores the current STATICACTIVITY info "curStaticActivity".
     * Special case when contextSort==STATICACTIVITY (must pass context==null and channelContext==null):
     * then only the STATICACTIVITY info is set.
     */
    private void bufferizeCallContext(Unit calledUnit, Tree callExpression,
                                      BoolVector context, Boolean channelContext,
                                      BoolVector nonZeroAdjointComing, int contextSort) {
        TapPair<Unit, TapList<TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>>>> unitContexts =
                TapList.assq(calledUnit, callContextsBuffer);
        if (unitContexts == null) {
            unitContexts = new TapPair<>(calledUnit, null);
            callContextsBuffer = new TapList<>(unitContexts, callContextsBuffer);
        }
        TapList<TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>>> callSitesContexts = unitContexts.second;
        TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>> callSiteContextCell = TapList.assq(callExpression, callSitesContexts);
        BoolVector[] callSiteContext;
        Boolean[] channelContextPair;
        if (callSiteContextCell == null) {
            callSiteContext = new BoolVector[4];
            for (int i=3 ; i>=0 ; --i) callSiteContext[i] = null ;
            if (channelContext != null) {
                channelContextPair = new Boolean[2];
            } else {
                channelContextPair = null;
            }
            TapTriplet<BoolVector[], Boolean[], Instruction> ctxt3 = new TapTriplet<>(callSiteContext, channelContextPair, curInstruction);
            unitContexts.second = new TapList<>(new TapPair<>(callExpression, ctxt3), callSitesContexts);
        } else {
            TapTriplet<BoolVector[], Boolean[], Instruction> ctxt3 = callSiteContextCell.second;
            callSiteContext = ctxt3.first;
            channelContextPair = ctxt3.second;
        }
        if (contextSort == STATICACTIVITY) {
            // when contextSort is STATICACTIVITY, the VARIED_ON_CALL and USEFUL_ON_EXIT infos are always empty.
            if (callSiteContext[VARIED_ON_CALL] == null) {
                callSiteContext[VARIED_ON_CALL] = new BoolVector(calledUnit.publicZonesNumber(diffKind));
            }
            if (callSiteContext[USEFUL_ON_EXIT] == null) {
                callSiteContext[USEFUL_ON_EXIT] = new BoolVector(calledUnit.publicZonesNumber(diffKind));
            }
        } else {
            if (callSiteContext[contextSort] == null) {
                callSiteContext[contextSort] = context;
            } else {
                callSiteContext[contextSort].cumulOr(context, calledUnit.publicZonesNumber(diffKind));
            }
            if (channelContext != null && channelContextPair != null && channelContext) {
                channelContextPair[contextSort] = Boolean.TRUE;
            }
        }
        if (nonZeroAdjointComing!=null) {
            if (callSiteContext[NONZERO_ADJOINT_COMING] == null) {
                callSiteContext[NONZERO_ADJOINT_COMING] = nonZeroAdjointComing ;
            } else {
                callSiteContext[NONZERO_ADJOINT_COMING].cumulOr(nonZeroAdjointComing) ;
            }
        }
        if (curStaticActivity != null && callSiteContext[STATIC_ACTIVE] == null) {
            CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
            BoolVector extendedStaticActivity = curStaticActivity.copy(nUDRZ, nDRZ);
            TapList[] actualParamDataS = new TapList<?>[1+ILUtils.getArguments(callExpression).length()];
            // Set actualParamDataS to dummy "false" values.
            // We will not use them anyway because curStaticActivity is only about globals.
            for (int i = actualParamDataS.length - 1; i >= 0; --i) {
                actualParamDataS[i] = new TapList<>(null, null);
            }
            callSiteContext[STATIC_ACTIVE] =
                translateCallSiteDataToCallee(extendedStaticActivity, actualParamDataS, null, null, true,
                                              callExpression, curInstruction, callArrow, diffKind);
        }
    }

    /**
     * Accumulates the bufferized variedness and usefulness contexts into the
     * contexts of the corresponding called units. This must be done only when
     * both variedness and usefulness are done for the current unit, which is
     * calling these called units. Leaves the buffer empty when done.
     */
    private void accumulateBufferizedCallContexts() {
        TapPair<Unit, TapList<TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>>>> unitContexts;
        Unit calledUnit;
        int diffKindLength;
        TapList<TapPair<Tree, TapTriplet<BoolVector[], Boolean[], Instruction>>> callSitesContexts;
        TapTriplet<BoolVector[], Boolean[], Instruction> ctxt3;
        BoolVector[] callSiteContext;
        Tree callSiteTree;
        boolean callSiteMustSpecialize;
        TapList<ActivityPattern> existingContexts;
        ActivityPattern existingGeneralizableContext;
        ActivityPattern existingContainingContext;
        ActivityPattern existingEqualContext;
        int distanceFromContext;
        int distanceFromGeneralizable;
        int distanceFromContaining;
        ActivityPattern totalActivity;
        MPIcallInfo messagePassingInfo;
        boolean traceCalledUnitAnalysis;
        if (TapEnv.traceCurAnalysis() && callContextsBuffer != null) {
            TapEnv.printlnOnTrace("       =======  CALL CONTEXTS SUMMARY: ======");
        }
        while (callContextsBuffer != null) {
            unitContexts = callContextsBuffer.head;
            calledUnit = unitContexts.first;
            if (getDiffDeps(calledUnit, dependencies, diffKind) != null) {
                diffKindLength = calledUnit.publicZonesNumber(diffKind) ;
                callSitesContexts = unitContexts.second;
                while (callSitesContexts != null) {
                    callSiteTree = callSitesContexts.head.first;
                    ctxt3 = callSitesContexts.head.second;
                    callSiteContext = ctxt3.first;
                    traceCalledUnitAnalysis = TapList.contains(tracedUnits, calledUnit);
                    messagePassingInfo =
                            MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), callSiteTree, curUnit.language(),
                                    ctxt3.third.block);
                    if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                        TapEnv.printlnOnTrace();
                        TapEnv.printlnOnTrace("         encountered new context for "+calledUnit
                                              +" at "+ILUtils.toString(callSiteTree)+" in "
                                              +ctxt3.third.block+" of "+ctxt3.third.block.unit());
                        TapEnv.printlnOnTrace("                " + (callSiteContext[VARIED_ON_CALL]==null ? "null" : "["+callSiteContext[VARIED_ON_CALL].toString(diffKindLength)+"]") + " -> "+(callSiteContext[USEFUL_ON_EXIT]==null ? "null" : "[" + callSiteContext[USEFUL_ON_EXIT].toString(diffKindLength) + "]")+ " plus static actives: [" + callSiteContext[STATIC_ACTIVE].toString(diffKindLength) + "]"+" and messagePassingInfo:" + messagePassingInfo);
                    }
                    if (messagePassingInfo == null) {
                        // The additional static active zones are the active zones that are not used nor modified
                        // in the calledUnit, and are therefore in both given VARIED_ON_CALL and USEFUL_ON_EXIT
                        // but are removed from these two sets by the filtering through the calledUnit's AD deps:
                        BoolVector additionalStaticActivity = new BoolVector(diffKindLength) ;
                        filterVariedUsefulWithDeps(callSiteContext[VARIED_ON_CALL], callSiteContext[USEFUL_ON_EXIT],
                                calledUnit, null, additionalStaticActivity);
                        callSiteContext[STATIC_ACTIVE].cumulOr(additionalStaticActivity);
                    }
                    if (callSiteContext[VARIED_ON_CALL].isFalse(diffKindLength)
                        && callSiteContext[USEFUL_ON_EXIT].isFalse(diffKindLength)) {
                        // If no activity needs to be propagated, don't create an ActivityPattern for this call (cf set11/lh006)
                        if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                            TapEnv.printlnOnTrace("            new filtered context is inactive: don't create a new activity pattern");
                        }
                    } else {
                        // Sweep through existing activity patterns, looking for existing "similar" patterns:
                        callSiteMustSpecialize = calledUnit.hasDirective(Directive.SPECIALIZEACTIVITY) != null ||
                                ctxt3.third.hasDirective(Directive.SPECIALIZEACTIVITY) != null;
                        existingGeneralizableContext = null;
                        distanceFromGeneralizable = 2 * diffKindLength;
                        existingContainingContext = null;
                        distanceFromContaining = 2 * diffKindLength;
                        existingEqualContext = null;
                        existingContexts = calledUnit.activityPatterns;
                        if (existingContexts != null) {
                            for (ActivityPattern existingContext : existingContexts) {
                                if (existingContext.variedOnCall().equals(callSiteContext[VARIED_ON_CALL], diffKindLength)
                                    && existingContext.usefulOnExit().equals(callSiteContext[USEFUL_ON_EXIT], diffKindLength)) {
                                    // If we find same varieds and same usefuls, then we must merge with the existing pattern,
                                    // regardless of the "specialization" choice, even if the "static" activities differ:
                                    if (existingContext.staticActivity().equals(callSiteContext[STATIC_ACTIVE], diffKindLength)) {
                                        // If this existing pattern is exactly the current pattern, remember it:
                                        existingEqualContext = existingContext;
                                    } else if (existingContext.staticActivity().contains(callSiteContext[STATIC_ACTIVE], diffKindLength)) {
                                        existingContainingContext = existingContext ;
                                    } else {
                                        existingGeneralizableContext = existingContext ;
                                    }
                                } else if (!callSiteMustSpecialize) {
                                    // else, if we allow for generalization of contexts
                                    // Compute distanceFromContext, a gross estimate of the discrepancy
                                    // between this existing pattern and the new pattern:
                                    distanceFromContext =
                                            existingContext.variedOnCall().distance(callSiteContext[VARIED_ON_CALL], diffKindLength)
                                                    + existingContext.usefulOnExit().distance(callSiteContext[USEFUL_ON_EXIT], diffKindLength);
                                    // if the existing pattern contains the new pattern, remember it:
                                    if (existingContext.variedOnCall().contains(callSiteContext[VARIED_ON_CALL], diffKindLength)
                                            && existingContext.usefulOnExit().contains(callSiteContext[USEFUL_ON_EXIT], diffKindLength)
                                            && existingContext.staticActivity().contains(callSiteContext[STATIC_ACTIVE], diffKindLength)) {
                                        if (distanceFromContext <= distanceFromContaining) {
                                            existingContainingContext = existingContext;
                                            distanceFromContaining = distanceFromContext;
                                        }
                                    }
                                    // if the existing pattern can be enlarged, remember it:
                                    if (existingContext.isGeneralizable()) {
                                        if (distanceFromContext <= distanceFromGeneralizable) {
                                            existingGeneralizableContext = existingContext;
                                            distanceFromGeneralizable = distanceFromContext;
                                        }
                                    }
                                }
                            }
                        }
                        if (existingEqualContext != null) {
                            // getting here means there is an existing pattern exactly equal to the new pattern.
                            // We can therefore use the existing pattern:
                            totalActivity = existingEqualContext;
                            existingEqualContext.registerCallingActivityPattern(curActivity);
                            existingEqualContext.staticActivity().cumulOr(callSiteContext[STATIC_ACTIVE]);
                            if (callSiteContext[NONZERO_ADJOINT_COMING] != null) {
                                if (existingEqualContext.nonZeroAdjointComing() == null) {
                                    existingEqualContext.setNonZeroAdjointComing(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                } else {
                                    existingEqualContext.nonZeroAdjointComing().cumulOr(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                }
                            }
                            if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                                TapEnv.printlnOnTrace("            found existing same activity context @"
                                        + Integer.toHexString(existingEqualContext.hashCode()) + " for " + calledUnit + ":");
                                TapEnv.printlnOnTrace("                ["
                                        + existingEqualContext.variedOnCall().toString(diffKindLength) + "] -> ["
                                        + existingEqualContext.usefulOnExit().toString(diffKindLength)
                                        + "] plus static actives: ["
                                        + existingEqualContext.staticActivity().toString(diffKindLength) + "]");
                            }
                        } else if (existingContainingContext != null) {
                            // getting here means there is an exisiting pattern that contains the new pattern,
                            // and we do not want to specialize. We can therefore use the existing pattern:
                            totalActivity = existingContainingContext;
                            existingContainingContext.registerCallingActivityPattern(curActivity);
                            existingContainingContext.staticActivity().cumulOr(callSiteContext[STATIC_ACTIVE]);
                            if (callSiteContext[NONZERO_ADJOINT_COMING] != null) {
                                if (existingContainingContext.nonZeroAdjointComing() == null) {
                                    existingContainingContext.setNonZeroAdjointComing(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                } else {
                                    existingContainingContext.nonZeroAdjointComing().cumulOr(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                }
                            }
                            if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                                TapEnv.printlnOnTrace("            see it as included in existing context @"
                                        + Integer.toHexString(existingContainingContext.hashCode()) + " for "
                                        + calledUnit + ":");
                                TapEnv.printlnOnTrace("                ["
                                        + existingContainingContext.variedOnCall().toString(diffKindLength) + "] -> ["
                                        + existingContainingContext.usefulOnExit().toString(diffKindLength)
                                        + "] plus static actives: ["
                                        + existingContainingContext.staticActivity().toString(diffKindLength) + "]");
                            }
                        } else if (existingGeneralizableContext != null) {
                            // getting here means there is an existing pattern that may be enlarged to contain the new pattern,
                            // therefore we enlarge the existing pattern and use it:
                            totalActivity = existingGeneralizableContext;
                            existingGeneralizableContext.registerCallingActivityPattern(curActivity);
                            if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                                TapEnv.printlnOnTrace("            accumulate it into existing activity context @"
                                        + Integer.toHexString(existingGeneralizableContext.hashCode()) + " for "
                                        + calledUnit + ":");
                                TapEnv.printlnOnTrace("                ["
                                        + existingGeneralizableContext.variedOnCall().toString(diffKindLength) + "] -> ["
                                        + existingGeneralizableContext.usefulOnExit().toString(diffKindLength)
                                        + "] plus static actives: ["
                                        + existingGeneralizableContext.staticActivity().toString(diffKindLength) + "]");
                            }
                            if (existingGeneralizableContext.variedOnCall()
                                    .cumulOrGrows(callSiteContext[VARIED_ON_CALL], diffKindLength)) {
                                calledUnit.analysisIsOutOfDateDown = true;
                            }
                            if (existingGeneralizableContext.usefulOnExit()
                                    .cumulOrGrows(callSiteContext[USEFUL_ON_EXIT], diffKindLength)) {
                                calledUnit.analysisIsOutOfDateDown = true;
                            }
                            if (existingGeneralizableContext.staticActivity()
                                    .cumulOrGrows(callSiteContext[STATIC_ACTIVE], diffKindLength)) {
                                calledUnit.analysisIsOutOfDateDown = true;
                            }
                            if (callSiteContext[NONZERO_ADJOINT_COMING] != null) {
                                if (existingGeneralizableContext.nonZeroAdjointComing() == null) {
                                    existingGeneralizableContext.setNonZeroAdjointComing(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                } else {
                                    existingGeneralizableContext.nonZeroAdjointComing().cumulOr(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                                }
                            }
                            if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                                TapEnv.printlnOnTrace("            that "
                                        + (calledUnit.analysisIsOutOfDateDown ? "becomes" : "remains") + ":");
                                TapEnv.printlnOnTrace("                ["
                                        + existingGeneralizableContext.variedOnCall().toString(diffKindLength) + "] -> ["
                                        + existingGeneralizableContext.usefulOnExit().toString(diffKindLength)
                                        + "] plus static actives: ["
                                        + existingGeneralizableContext.staticActivity().toString(diffKindLength) + "]");
                                if (calledUnit.analysisIsOutOfDateDown) {
                                    TapEnv.printlnOnTrace("            => larger context, requires reanalysis of "
                                            + calledUnit);
                                }
                            }
                            // Intrinsic Units will not be reanalyzed: compute their frontiers now.
                            if (calledUnit.isIntrinsic()) {
                                computeUnitFrontierActivities(calledUnit, totalActivity,
                                        callSiteContext[VARIED_ON_CALL], callSiteContext[USEFUL_ON_EXIT]);
                            }
                        } else {
                            // getting here means we found no existing pattern that contains (or may be enlarged
                            // to contain) the new pattern. Therefore we create a new pattern:
                            ActivityPattern newContext = new ActivityPattern(calledUnit,
                                    callSiteContext[VARIED_ON_CALL], callSiteContext[USEFUL_ON_EXIT],
                                    !callSiteMustSpecialize, diffKind);
                            newContext.registerCallingActivityPattern(curActivity);
                            newContext.setStaticActivity(callSiteContext[STATIC_ACTIVE]);
                            if (callSiteContext[NONZERO_ADJOINT_COMING] != null) {
                                newContext.setNonZeroAdjointComing(callSiteContext[NONZERO_ADJOINT_COMING]) ;
                            }
                            calledUnit.analysisIsOutOfDateDown = true;
                            calledUnit.activityPatterns = new TapList<>(newContext, calledUnit.activityPatterns);
                            if (TapEnv.traceCurAnalysis() || traceCalledUnitAnalysis) {
                                TapEnv.printlnOnTrace("            prepend it as a new activity context @"
                                        + Integer.toHexString(newContext.hashCode()) + " for " + calledUnit + ":");
                                TapEnv.printlnOnTrace("                ["
                                        + newContext.variedOnCall().toString(diffKindLength) + "] -> ["
                                        + newContext.usefulOnExit().toString(diffKindLength)
                                        + "] plus static actives: ["
                                        + newContext.staticActivity().toString(diffKindLength) + "]");
                                TapEnv.printlnOnTrace("             => requires reanalysis of " + calledUnit);
                            }
                            // Intrinsic Units will not be reanalyzed: compute their frontiers now.
                            if (calledUnit.isIntrinsic()) {
                                computeUnitFrontierActivities(calledUnit, newContext,
                                        callSiteContext[VARIED_ON_CALL], callSiteContext[USEFUL_ON_EXIT]);
                            }
                            totalActivity = newContext;
                        }
                        ActivityPattern.setAnnotationForActivityPattern(callSiteTree, curActivity,
                                "multiActivityCalleePatterns", totalActivity);
                    }
                    callSitesContexts = callSitesContexts.tail;
                }
            }
            callContextsBuffer = callContextsBuffer.tail;
        }
    }

    /**
     * Checks varied variables that are passed as argument to a subroutine in
     * which they are declared as a non-differentiable type, e.g. integer. Warns
     * the user that this will loose activity and derivatives of varied
     * variables.
     */
    private void checkVariedBadCast(BoolVector varieds,
                                    Tree expression, Unit curUnit) {
        Operator operator = expression.operator();
        if (operator.code == ILLang.op_call) {
            Unit calledUnit = getCalledUnit(expression, curSymbolTable);
            if (calledUnit != null && calledUnit.unitInOutR() != null) {
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                BoolVector copyVarieds = varieds.copy() ;
                // Unfocusing lets us propagate activity on non-real types:
                BoolVector variedsUnfocused = curSymbolTable.unfocusFromKind(varieds, diffKind);
                TapList[] paramVariednesses = new TapList[1+actualParams.length];
                for (int i=actualParams.length; i>0; --i) {
                    paramVariednesses[i] =
                        variednessThroughExpression(actualParams[i-1], copyVarieds, null, false);
                }
                paramVariednesses[0] = null ;
                BoolVector publicVariedness =
                    translateCallSiteDataToCallee(variedsUnfocused, paramVariednesses, null, actualParams, true,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND) ;
                ZoneInfo zi;
                for (int i=calledUnit.paramElemsNb()-1 ; i>=0 ; --i) {
                    zi = calledUnit.paramElemZoneInfo(i);
                    if (publicVariedness.get(i) && calledUnit.unitInOutPossiblyR().get(i)) {
                        TypeSpec formalType = zi.type.wrappedType;
                        if (formalType == null && !zi.isHidden && zi.accessTree!=null
                            && zi.accessTree.opCode()!=ILLang.op_ident && calledUnit.publicSymbolTable()!=null) {
                            WrapperTypeSpec typeSpec =
                                    calledUnit.publicSymbolTable().typeOf(ILUtils.baseTree(zi.accessTree));
                            if (typeSpec != null) {
                                formalType = typeSpec.wrappedType;
                            }
                        }
                        if (!TypeSpec.isDifferentiableType(formalType)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(AD01) Formal input argument " + zi.publicName() + " of " + calledUnit.name() + " is non-differentiable, and is passed a varied actual argument");
                        }
                    }
                }
            }
        }
        if (!expression.isAtom()) {
            Tree[] subTrees = expression.children();
            for (int i = subTrees.length - 1; i >= 0; i--) {
                checkVariedBadCast(varieds, subTrees[i], curUnit);
            }
        }
    }

    /**
     * Checks useful variables that are returned as argument to a subroutine in
     * which they are declared as a non-differentiable type, e.g. integer. Warns
     * the user that this might loose activity and derivatives of active
     * variables.
     */
    private void checkUsefulBadCast(BoolVector usefuls,
                                    Tree expression, Unit curUnit) {
        Operator operator = expression.operator();
        if (operator.code == ILLang.op_call) {
            Unit calledUnit = getCalledUnit(expression, curSymbolTable);
            if (calledUnit != null && calledUnit.unitInOutW() != null) {
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                BoolVector copyUsefuls = usefuls.copy();
                // Unfocusing lets us propagate activity on non-real types:
                BoolVector usefulsUnfocused = curSymbolTable.unfocusFromKind(usefuls, diffKind);
                TapList[] paramUsefulnesses = new TapList[1+actualParams.length];
                //TODO: approximate code: we don't have the usefulness of
                // the function's result at hand, and we would like
                // to put it here into paramUsefulnesses[0] !
                paramUsefulnesses[0] = new TapList<>(Boolean.FALSE, null);
                for (int i=1 ; i<paramUsefulnesses.length ; ++i) paramUsefulnesses[i] = null ;
                BoolVector publicUsefulness =
                    translateCallSiteDataToCallee(usefulsUnfocused, paramUsefulnesses, null, actualParams, false,
                                                  expression, curInstruction, arrow,
                                                  SymbolTableConstants.ALLKIND) ;
                ZoneInfo zi;
                for (int i=calledUnit.paramElemsNb()-1 ; i>=0 ; --i) {
                    zi = calledUnit.paramElemZoneInfo(i);
                    if (publicUsefulness.get(i) && calledUnit.unitInOutPossiblyW().get(i)) {
                        TypeSpec formalType = zi.type.wrappedType;
                        if (formalType == null && !zi.isHidden && zi.accessTree!=null && zi.accessTree.opCode() != ILLang.op_ident && calledUnit.publicSymbolTable() != null) {
                            WrapperTypeSpec typeSpec = calledUnit.publicSymbolTable()
                                    .typeOf(ILUtils.baseTree(zi.accessTree));
                            if (typeSpec != null) {
                                formalType = typeSpec.wrappedType;
                            }
                        }
                        if (!TypeSpec.isDifferentiableType(formalType)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(AD02) Formal output argument " + zi.publicName() + " of " + calledUnit.name() + " is non-differentiable, and is passed a useful actual argument");
                        }
                    }
                }
            }
        }
        if (!expression.isAtom()) {
            Tree[] subTrees = expression.children();
            for (int i = subTrees.length - 1; i >= 0; i--) {
                checkUsefulBadCast(usefuls, subTrees[i], curUnit);
            }
        }
    }

    /**
     * Checks varied variables that are printed into a file: Warns the user that
     * this will loose activity and derivatives of active variables.
     */
    private void checkVariedIO(BoolVector varieds, Tree tree) {
        if (ILUtils.isIOWrite(tree)) {
            Tree fileTree = accessesToFile(tree.down(2));

            if (fileTree != null) {
                Tree expr;
                TapIntList readZonesList;
                tree = tree.down(3);

                for (int i = tree.length(); i > 0; i--) {
                    expr = tree.down(i);
                    readZonesList =
                            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(expr, null, curInstruction, null), true);
                    if (intersectsKindZoneRks(varieds, diffKind, readZonesList, null, curSymbolTable)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, expr, "(AD03) Varied variable " + ILUtils.toString(expr, curUnit.language()) + " written by I-O to file " + ILUtils.toString(fileTree));
                    }
                }
            }
        }
    }

    /**
     * Checks variables that are read from a file: Warns the user that this will
     * loose activity and derivatives.
     */
    private void checkUsefulIO(BoolVector usefuls, Tree tree) {
        if (ILUtils.isIORead(tree)) {
            Tree fileTree = accessesToFile(tree.down(2));

            if (fileTree != null) {
                Tree expr;
                TapIntList writtenZonesList;
                tree = tree.down(3);

                for (int i = tree.length(); i > 0; i--) {
                    expr = tree.down(i);
                    writtenZonesList =
                            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(expr, null, curInstruction, null), true);
                    if (intersectsKindZoneRks(usefuls, diffKind, writtenZonesList, null, curSymbolTable)) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, expr, "(AD04) Useful variable " + ILUtils.toString(expr, curUnit.language()) + " read by I-O from file " + ILUtils.toString(fileTree));
                    }
                }
            }
        }
    }

    /**
     * @return true when this ioSpecs indicates that its ioCall may go to a file
     * (instead of std_in or std_out).
     */
    private Tree accessesToFile(Tree ioSpecs) {
        Tree[] specs = ioSpecs.children();

        if (specs.length == 0) {
            return null;
        } else {
            int destination = specs[0].opCode();

            if (destination != ILLang.op_none &&
                    destination != ILLang.op_star) {
                return specs[0];
            } else {
                return null;
            }
        }
    }

    private String diffParamsOf(BoolVector unitCallVariedness,
                                BoolVector unitExitUsefulness,
                                ZoneInfo[] externalShape) {
        String result = "";
        if (externalShape!=null) {
            int shapeLength = externalShape.length ;
            ZoneInfo zoneInfo;
            String inout;
            int dki ;
            for (int i = 0; i < shapeLength; i++) {
                zoneInfo = externalShape[i];
                dki = zoneInfo.kindZoneNb(diffKind) ;
                if (dki!=-1 && (unitCallVariedness.get(dki) || unitExitUsefulness.get(dki))) {
                    if (unitCallVariedness.get(dki)) {
                        if (unitExitUsefulness.get(dki)) {
                            inout = "(in;out)";
                        } else {
                            inout = "(in)";
                        }
                    } else {
                        inout = "(out)";
                    }
                    result = result + ' ' + zoneInfo.publicName() + '=' + inout;
                }
            }
        }
        return result;
    }

    /**
     * Sweep through the instructions in "curUnit", looking for function calls. At
     * each function call, look for uses of formal arguments of "curUnit" which are
     * function names, either used as the called function name, or recursively
     * as a function name passed as argument. Collect the activity of this
     * function name, required by these uses, and accumulate it into the
     * varFunctionADActivities for the formal args of this "curUnit".
     * Also, for the "-context" option, sets activity annotation on actual
     * arguments of calls to the root diff routines.
     */
    private void checkCalledFunctionsActivities() {
        TapList allBlocks = curUnit.allBlocks;
        TapList<Instruction> instructions;
        while (allBlocks != null) {
            setCurBlockEtc((Block) allBlocks.head);
            instructions = curBlock.instructions;
            while (instructions != null) {
                curInstruction = instructions.head;
                if (curInstruction.tree != null) {
                    checkCalledFunctionsActivitiesInTree(curInstruction.tree);
                }
                instructions = instructions.tail;
            }
            allBlocks = allBlocks.tail;
        }
        setCurBlockEtc(null);
    }

    /**
     * @return true when the given Unit is in the outside "context" of the differentiated region.
     */
    public boolean unitIsContext(ActivityPattern pattern) {
        //[llh 15/6/2015] TODO: We must get rid of this somehow. The fact that a Unit is a context
        // should not be deduced from its activity vectors, but rather from the location of the
        // unit in the Call Graph !
        Unit unit = pattern.unit();
        int diffKindLength = unit.publicZonesNumber(diffKind);
        BoolVector callActivity = pattern.callActivity();
        BoolVector exitActivity = pattern.exitActivity();
        return (callActivity == null || callActivity.isFalse(diffKindLength))
                && (exitActivity == null || exitActivity.isFalse(diffKindLength));
    }

    /**
     * Checks arguments of calls that are function names passed as arguments.
     * This is also a utility for checkCalledFunctionsActivities().
     */
    private void checkCalledFunctionsActivitiesInTree(Tree tree) {
        if (tree.opCode() == ILLang.op_call) {
            Unit calledUnit = getCalledUnit(tree, curSymbolTable);
            ActivityPattern curCalledActivity = (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "multiActivityCalleePatterns");
            // Check if we are on a CALL to a function passed as argument:
            if (calledUnit != null && curCalledActivity != null
                    && calledUnit.hasParamElemsInfo() && !calledUnit.hasSource()
                    && !MPIcallInfo.isMessagePassingFunction(calledUnit.name(), curUnit.language())
                // If debug AD mode is on, we might get unnecessary messages !
                // && TapEnv.dbadMode()==0
            ) {
                BoolVector unitCallActivity = curCalledActivity.callActivity();
                BoolVector unitExitActivity = curCalledActivity.exitActivity();
                int diffKindLength = calledUnit.publicZonesNumber(diffKind);
                if (diffKindLength > 0 && unitCallActivity != null && unitExitActivity != null
                        && !(unitCallActivity.isFalse(diffKindLength) &&
                        unitExitActivity.isFalse(diffKindLength))) {
                    TapList<FunctionDecl> funcDecls = curSymbolTable.getFunctionDecl(calledUnit.name(), null, null, false);
                    FunctionDecl funcDecl = funcDecls == null ? null : funcDecls.head;
                    if (funcDecl != null) {
                        funcDecl.setActive();
                    }
                    if (calledUnit.isVarFunction() || calledUnit.isInterface()) {
                        accumulateVarFunctionADActivity(curUnit, curActivity, null,
                                ILUtils.getCalledNameString(tree),
                                new TapTriplet<>(calledUnit, unitCallActivity, unitExitActivity));
                    }
                    if (!calledUnit.isInterface() && !calledUnit.hasPredefinedDerivatives()) {
                        String diffVars = diffParamsOf(unitCallActivity, unitExitActivity, calledUnit.externalShape);
                        if (!diffVars.contains("%message_passing_channel_")) { //[llh] one should not test this !!
                            TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "(AD09) Please provide a differential of function " + calledUnit.name() + " for arguments" + diffVars);
                        }
                    }
                }
            }
            Tree[] actualParams = ILUtils.getArguments(tree).children();
            TapTriplet<Unit, BoolVector, BoolVector>[] calledVarFunctionInfos = null;
            if (calledUnit != null && curCalledActivity != null) {
                calledVarFunctionInfos =
                        curCalledActivity.varFunctionADActivities();
            }
            String passedName;
            if (calledVarFunctionInfos != null) {
                int minArgsLength = Math.min(actualParams.length, calledVarFunctionInfos.length);

                for (int i = minArgsLength - 1; i >= 0; --i) {
                    // Ckeck if this CALL is passing an argument which is actually a function:
                    if (actualParams[i].opCode() == ILLang.op_ident) {
                        passedName = ILUtils.getIdentString(actualParams[i]);
                        TapList<FunctionDecl> passedDecls = curSymbolTable.getFunctionDecl(passedName, null, null, false);
                        FunctionDecl passedDecl = passedDecls == null ? null : passedDecls.head;
                        SymbolDecl passedNameSymbolDecl = curSymbolTable.getSymbolDecl(passedName);
                        if (passedDecl != null
                                // In addition, check that the function name is not masked by a local symbol decl:
                                && passedNameSymbolDecl != null
                                && (passedNameSymbolDecl.isA(SymbolTableConstants.FUNCTION)
                                || passedNameSymbolDecl.isA(SymbolTableConstants.FUNCNAME)
                                || passedNameSymbolDecl.isA(SymbolTableConstants.INTERFACE))) {
                            Unit passedUnit = passedDecl.unit();
                            TapTriplet<Unit, BoolVector, BoolVector> calledVarFunctionInfo = calledVarFunctionInfos[i];
                            int calledUnitLength =
                                    calledUnit.hasParamElemsInfo() ? calledUnit.paramElemsNb() : -1;
                            // Case of jac argument to call pjac in F77:lh61  :
                            if (calledVarFunctionInfo == null &&
                                    !calledUnit.hasSource() &&
                                    calledUnitLength != -1 &&
                                    (!curCalledActivity.callActivity().isFalse(calledUnitLength) ||
                                            !curCalledActivity.exitActivity().isFalse(calledUnitLength)) &&
                                    (passedUnit == null || !passedUnit.hasSource())) {
                                calledVarFunctionInfo = new TapTriplet<>(passedUnit,
                                        new BoolVector(0),
                                        new BoolVector(0));
                                calledVarFunctionInfos[i] = calledVarFunctionInfo;
                            }
                            if (calledVarFunctionInfo != null) {
                                passedDecl.setActive();
                                setAnnotatedActive(curActivity, actualParams[i]);
                                if (curUnit != null) {
                                    if (passedUnit != null) {
                                        // Case of passed function "g" in set05/v060 :
                                        ActivityPattern passedUnitActivityPattern;
                                        if (passedUnit.activityPatterns == null) {
                                            passedUnitActivityPattern = new ActivityPattern(passedUnit, true, true, diffKind);
                                            if (passedUnit.isStandard()) {
                                                // This is the case of e.g. foo and gee in set02/lh187
                                                // These functions are not called explicitly, only passed as function pointers
                                                // Activity analysis has not been called on them, so their diff Unit
                                                // would be wrong. Better not create them and send a AD09 message.
                                                passedUnitActivityPattern.setDontDiff(true);
                                            }
                                            passedUnit.activityPatterns =
                                                    new TapList<>(passedUnitActivityPattern, null);
                                        } else {
                                            passedUnitActivityPattern = passedUnit.activityPatterns.head;
                                            passedUnitActivityPattern.setForcedActive(true);
                                        }
                                        passedUnitActivityPattern.registerCallingActivityPattern(curActivity);
                                        BoolVector cumulInfo;
                                        BoolVector newInfo;
                                        newInfo = convertInfoFromUnitToUnit(calledVarFunctionInfo.second,
                                                          calledVarFunctionInfo.first, passedUnit, diffKind);

                                        cumulInfo = passedUnitActivityPattern.callActivity();
                                        if (cumulInfo == null) {
                                            passedUnitActivityPattern.setCallActivity(newInfo);
                                        } else {
                                            cumulInfo.cumulOr(newInfo);
                                        }
                                        newInfo = convertInfoFromUnitToUnit(calledVarFunctionInfo.third,
                                                          calledVarFunctionInfo.first, passedUnit, diffKind);
                                        cumulInfo = passedUnitActivityPattern.exitActivity();
                                        if (cumulInfo == null) {
                                            passedUnitActivityPattern.setExitActivity(newInfo);
                                        } else {
                                            cumulInfo.cumulOr(newInfo);
                                        }
                                    }
                                    accumulateVarFunctionADActivity(curUnit, curActivity,
                                            null,
                                            passedName,
                                            calledVarFunctionInfo);
                                    accumulateVarFunctionADActivity(calledUnit, curCalledActivity,
                                            tree,
                                            passedName,
                                            calledVarFunctionInfo);
                                }
                                if (passedUnit != null && !passedUnit.isInterface()/* && !passedUnit.isVarFunction()*/) {
                                    String diffVars =
                                        diffParamsOf(calledVarFunctionInfo.second,
                                                     calledVarFunctionInfo.third, calledVarFunctionInfo.first.externalShape);
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, actualParams[i], "(AD09) Please provide a differential of function " + passedUnit.name() + (diffVars.isEmpty() ? "" : " for arguments" + diffVars));
                                }
                            }
                        }
                    }
                }
            }
        } else if (!tree.isAtom()) {
            Tree[] subTrees = tree.children();
            for (int i = subTrees.length - 1; i >= 0; i--) {
                checkCalledFunctionsActivitiesInTree(subTrees[i]);
            }
        }
    }

    /**
     * Accumulate into the info about activity of "functions passed as argument"
     * for this "unit" together with its particular ActivityPattern "unitActivity",
     * that the argument named "argName", which is a function
     * passed as argument, must be differentiated at least with the given
     * "activityUsefulness".
     */
    private void accumulateVarFunctionADActivity(Unit unit, ActivityPattern unitActivity,
                                                 Tree callTree, String argName,
                                                 TapTriplet<Unit, BoolVector, BoolVector> activityUsefulness) {
        // Find the rank of formal argument of "unit" which is called "argName".
        // When found, pick the corresponding "varFunctionInfos[i]" and
        // OR-accumulate into it the given "activityUsefulness".
        Tree[] formalParams;
        if (callTree == null) {
            callTree = unit.entryBlock().headTree();
        }
        assert callTree != null;
        formalParams = ILUtils.getArguments(callTree).children();
        TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionInfos = unitActivity.varFunctionADActivities();

        Tree arg;
        for (int i = formalParams.length - 1; i >= 0; --i) {
            arg = formalParams[i];
            if (arg.opCode() == ILLang.op_ident
                    && ILUtils.getIdentString(arg).equals(argName)) {
                if (varFunctionInfos[i] == null) {
                    varFunctionInfos[i] = activityUsefulness;
                } else {
                    varFunctionInfos[i].second.cumulOr(activityUsefulness.second);
                    varFunctionInfos[i].third.cumulOr(activityUsefulness.third);
                }
            }
        }
    }

    /**
     * @return true iff the given "unit" is active,
     * which means it must have a differentiated counterpart.
     */
    public boolean isActiveUnit(Unit unit) {
        TapList<ActivityPattern> activityPatterns = unit.activityPatterns;
        while (activityPatterns != null) {
            ActivityPattern curActivity = activityPatterns.head;
            if (isActiveUnit(curActivity)) {
                return true;
            }
            activityPatterns = activityPatterns.tail;
        }
        return false;
    }

    public boolean isActiveUnit(ActivityPattern pattern) {
        Unit unit = pattern.unit();
        if (unit.isModule()) {
            return false;
        } else if (unit.rank() < 0) {
            if (pattern.isActive()) {
                // If this unit is an intrinsic, differentiated as an external
                //  because nobody cared to define its derivatives...
                //  In that case function is active if its result is of a
                //  differentiable type and if its deps matrix shows that it may
                //  propagate activity from one of its inputs to one of its outputs.

//                 BoolMatrix unitDiffDeps = getDiffDeps(unit, dependencies, diffKind);

                int diffKindLength = unit.publicZonesNumber(diffKind);
                BoolVector dummyVoC = new BoolVector(diffKindLength);
                dummyVoC.setTrue();
                BoolVector dummyUoE = new BoolVector(diffKindLength);
                dummyUoE.setTrue();
                filterVariedUsefulWithDeps(dummyVoC, dummyUoE, unit, null, null);
                return TypeSpec.isDifferentiableType(unit.functionTypeSpec())
                    && !dummyVoC.isFalse(diffKindLength)
                    && !dummyUoE.isFalse(diffKindLength);
            } else {
                return false ;
            }
        } else if (!unit.hasParamElemsInfo()) {
            //  In the very special case where we can't see the calls to
            //  the function because they are hidden in hidden functions,
            //  then we return true if some caller function is itself active:
            TapList<CallArrow> callers = unit.callers();
            boolean activeCaller = false;
            Unit callerUnit;
            TapList<ActivityPattern> callerActivities;
            while (!activeCaller && callers != null) {
                if (callers.head.isCall()) {
                    callerUnit = callers.head.origin;
                    callerActivities = callerUnit.activityPatterns;
                    while (!activeCaller && callerActivities != null) {
                        activeCaller = isActiveUnit(callerActivities.head);
                        callerActivities = callerActivities.tail;
                    }
                }
                callers = callers.tail;
            }
            return activeCaller;
        } else {
            // This is a normal user's unit.
            //  In that case function is active if some call was made
            //  with an active formal param or global.
            return pattern.isActive();
        }
    }

    /**
     * In the VERY SPECIAL case where we differentiate also INTEGER's
     * (i.e. diffKind != REALKIND), e.g. for programs that
     * do real-int conversion tricks, we propagate activity on INTEGERS.
     * BUT, when these integers are involved in arithmetic operations,
     * we do not propagate activity through the arithmetic operation.
     */
    private TapList activityOnlyForReal(TapList exprActivity, Tree expr) {
        if (diffKind == SymbolTableConstants.REALKIND ||
                curSymbolTable.typeOf(expr).isRealOrComplexBase()) {
            return exprActivity;
        } else {
            return null;
        }
    }

    /**
     * In the VERY SPECIAL case where we differentiate also INTEGER's
     * (i.e. diffKind != REALKIND), e.g. for programs that
     * do real-int conversion tricks, we propagate activity on INTEGERS.
     * BUT, when these integers are involved in arithmetic operations,
     * we do not propagate activity through the arithmetic operation.
     */
    private TapList depsOnlyForReal(TapList exprDeps, Tree expr) {
        if (diffKind == SymbolTableConstants.REALKIND
                || curSymbolTable.typeOf(expr).isRealOrComplexBase()) {
            return exprDeps;
        } else {
            return null;
        }
    }

    /**
     * @return the dependency at the beginning of "block", by making
     * the union of all dependencies that might arrive through each
     * incoming control-flow arrow (FGArrow).
     */
    private BoolMatrix accumulateDepIn(Block block) {
        Block origin;
        BoolMatrix depIncoming;
        BoolMatrix depIn = null;
        TapList<FGArrow> arrows = block.backFlow();
        // OR-accumulate the dependence incoming through each of "arrows" into "depIn":
        while (arrows != null) {
            origin = arrows.head.origin;
            depIncoming = depsOut.retrieve(origin);
            if (depIncoming != null) {
                // if still null, initialize depIn to an "implicit-zero" BoolMatrix :
                if (depIn == null) {
                    int nUDRZ = curUnit.publicSymbolTable().declaredZonesNb(diffKind);
                    depIn = new BoolMatrix(nDRZ, nUDRZ);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("   start accumulate AD Dependencies entering "+block+" into initial:") ;
                        TapEnv.dumpOnTrace(depIn) ;
                    }
                }
                // Only variables common to the two Block's scopes may be transferred through this arrow:
                SymbolTable commonSymbolTable = block.symbolTable.getCommonRoot(origin.symbolTable);
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("   will or-accumulate from "+origin+" :") ;
                    TapEnv.dumpOnTrace(depIncoming) ;
                }
                depIn.cumulOr(depIncoming, true,
                        commonSymbolTable.declaredZonesNb(diffKind));
            }
            arrows = arrows.tail;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("   accumulated AD Dependencies entering "+block);
            if (depIn==null) {
                TapEnv.printlnOnTrace("    NULL because unreachable") ;
            } else {
                TapEnv.dumpOnTrace(depIn) ;
            }
            TapEnv.printlnOnTrace("  ---------------------") ;
        }
        return depIn;
    }

    /**
     * @return the published or built result, for "unit", of the AD dependencies analysis,
     * i.e. the dependencies signature, of type BoolMatrix , of the Unit.
     */
    public BoolMatrix getDiffDeps(Unit unit) {
        return getDiffDeps(unit, dependencies, diffKind);
    }

}
