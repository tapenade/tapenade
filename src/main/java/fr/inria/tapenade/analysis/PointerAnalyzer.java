/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.representation.*;

/**
 * Analyzer in charge of detection of possible pointer destinations.
 *
 * <pre>
 * Feature: derives from DataFlowAnalyzer : OK
 * Feature: able to analyze recursive code: OK
 * Feature: distinguishes structure elemts: OK
 * Feature: takes  care  of  pointer dests: OK
 * Feature: refine on loop-local variables: OK
 * Feature: std method for call arguments : OK
 * Feature: able to analyze sub-flow-graph: OK
 * </pre>
 */
public final class PointerAnalyzer extends DataFlowAnalyzer {

    /** There is now a second, TOPDOWN_2, phase on the call graph.
     * This might provide an absolute pointer destinations info on the source program,
     * but only partially because a callee routine cannot refer to the local zones
     * of its calling routine (their numbers are recycled in the callee).
     * Therefore, the current TOPDOWN_2 is approximate, a pointer parameter "p" of
     * a routine will often be considered to point to its own "*p", meaning anything it points
     * to upon entry of the routine, instead of a more elaborate list of possible destinations.
     * The result of AD surely depends on the accuracy of pointer destinations info,
     * but differences should only be about efficiency and derivatives should remain correct.
     * Still, this TOPDOWN_2 phase is required for propagating destinations of
     * pointers-to-functions, and in this case it is good to have an accurate propagation.
     * Parameter PROPAGATE_ALL_POINTERS_DESTINATIONS, when set to true, requests TOPDOWN_2
     * analysis to propagate absolute destinations of all pointers, whenever possible.
     * Otherwise, if set to false, absolute propagation is limited to pointers-to-functions.
     */
    private static final boolean PROPAGATE_ALL_POINTERS_DESTINATIONS = true ;
    // TESTED on nonRegressions: all accepted versions are with "true". Codes produced with "false", although sometimes different, are also correct (as of Dec 2024).
    /** When true, tries to avoid using too much heap memory by splitting the TOPDOWN_2
     * phase in two parts. The run-time cost of that is that
     * we are obliged to do forward propagation once more. */
    private boolean VERY_BIG_CG = false ;
    // TESTED: when true, minor diffs on 04/ptr05 04/ptr08 04/ptr10 (as of Dec 2024).
    /** When true, tries to avoid using too much heap memory by not storing destination
     * matrices both before and after each Block, but instead only after each Block.
     * The run-time cost of that is that one early detection of fixed-point is lost. */
    private boolean VERY_BIG_FG = false ;
    // TESTED: when true, minor diffs on 04/lh114 04/lh140 11/v425 12/complxstep03 migliore-andra (as of Dec 2024).

    /**
     * Constant that codes for the 1st cgPhase.
     */
    private static final int BOTTOMUP_1 = 1;
    /**
     * Constant that codes for the 2nd cgPhase.
     */
    private static final int TOPDOWN_2 = 2;
    /**
     * Current phase of the analysis at the call graph level.
     * Can be BOTTOMUP_1 or TOPDOWN_2.
     */
    private int cgPhase;
    /**
     * Temporary during analysis of one Unit: dests matrices before each block.
     */
    private BlockStorage<BoolMatrix> beforeDests;
    /**
     * Temporary during analysis of one Unit: dests matrices after each block.
     */
    private BlockStorage<BoolMatrix> afterDests;
    /**
     * Temporary during analysis of one Unit: pointer effect matrix for each block.
     */
    private BlockStorage<BoolMatrix> throughDests;
    /**
     * Same as beforeDests, but for "cycling" info.
     */
    private BlockStorage<BoolMatrix> beforeDestsCycle;
    /**
     * Same as afterDests, but for "cycling" info.
     */
    private BlockStorage<BoolMatrix> afterDestsCycle;
    /**
     * For each unit, the tree of the zones of the returned value, if any.
     */
    private final UnitStorage returnTrees;
    /**
     * Tree that accumulates the tree of zones possibly returned by the curUnit.
     */
    private TapList returnedZoneTree;
    /**
     * The current number of declared Pointer zones.
     */
    private int nDPZ;
    /**
     * Global flag used to mark that we are initializing the current Block
     */
    private boolean initializingUnit = false ;
    /**
     * Global flag used to tell that it is termination time, and therefore
     * - getValueFlowingThrough() must emit "dangling" messages,
     * - propagateDestsThroughBlock() must create the "points to" comments
     * - pointersThroughExpression() must precompute the destinations of globals in calls.
     */
    private boolean terminatingUnit = false;
    /** Global flag used to tell that we are in the termination phase on the Call Graph,
     * i.e. we are inside terminateCGForUnit() */
    private boolean terminatingCallGraph = false;
    /**
     * Temporary pointers'destinations matrix during propagation.
     */
    private BoolMatrix tmpDests;
    /**
     * Same as tmpDests, but for "cycling" info.
     */
    private BoolMatrix tmpDestsCycle;
    /**
     * Pointers'destinations info that will be accumulated where control flow merges.
     */
    private BoolMatrix additionalDests;

    /**
     * Used for dealing with the chaining of "&***&"" in C language for function pointers.
     */
    private Boolean functionPointerFlag = false;

    /**
     * Creates a Pointer Destinations analyzer.
     */
    private PointerAnalyzer(CallGraph callGraph) {
        super(callGraph, "Pointer Destinations analysis", TapEnv.tracePointer());
        returnTrees = new UnitStorage(callGraph);
    }

    /**
     * Main entry point. Builds a PointerAnalyzer, stores it and runs it.
     */
    public static void runAnalysis(CallGraph callgraph,
                                   TapList<Unit> rootUnits) {
        TapEnv.setPointerAnalyzer(new PointerAnalyzer(callgraph));
        TapEnv.pointerAnalyzer().run(rootUnits);
    }

    @Override
    protected void run(TapList<Unit> rootUnits) {
        cgPhase = BOTTOMUP_1;
        curAnalysisName = "Bottom-up Pointer Destinations analysis";
        tracedUnits = curCallGraph.getUnits(TapEnv.tracePointer());
        runBottomUpAnalysis(rootUnits);
        cgPhase = TOPDOWN_2;
        curAnalysisName = "Top-down Pointer Destinations analysis";
        tracedUnits = curCallGraph.getUnits(TapEnv.tracePointer());
        runTopDownAnalysis(rootUnits);
        setCurUnitEtc(null);
    }

    /**
     * Build a BoolVector dests row representing explicit-Identity for row number "rowIndex".
     * This does not work when the rowIndex is about a formal parameter!
     *
     * @param rowIndex    The given row index.
     * @param symbolTable The SymbolTable of the current location in the current Unit.
     * @return The BoolVector that explicitly represents identity.
     */
    public static BoolVector buildExplicitIdentityDestsRow(int rowIndex, SymbolTable symbolTable) {
        int nZ = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector result = new BoolVector(symbolTable.unit.hasTooManyZones(), nZ);
        if (rowIndex < symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) {
            ZoneInfo pZI = symbolTable.declaredZoneInfo(rowIndex, SymbolTableConstants.PTRKIND);
            if (pZI != null) {
                TapIntList targets = ZoneInfo.listAllZones(pZI.targetZonesTree, true);
                result.set(targets, true);
            }
        }
        return result;
    }

    @Override
    protected Object initializeCGForUnit() {
        if (cgPhase==BOTTOMUP_1) {
            // We must initialize the pointers effect of an external, otherwise
            // future propagation algo will think it makes a STOP inside !
            if (curUnit.isExternal() || curUnit.isIntrinsic() || curUnit.isInterface()) {
                int nbPtrz = curUnit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
                int nbz = curUnit.paramElemsNb() ;
                curUnit.pointerEffect = new BoolMatrix(curUnit.hasTooManyZones(), nbPtrz, nbz);
                curUnit.pointerEffect.setIdentity();
            }
            return null;
        } else { //cgPhase==TOPDOWN_2
            return null;
        }
    }

    @Override
    protected void initializeCGForRootUnit() {
        if (cgPhase==TOPDOWN_2 && curUnit.hasSource()) {
            nDZ = curUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND) ;
            setTopDownContext(curUnit, identityPointerMatrix());
        }
    }

    /**
     * Places the comments about pointers into the code or curUnit,
     * then cleans last bits of memory.
     * Done only once for each Unit, when all sweeps on the CG are finished.
     */
    @Override
    protected void terminateCGForUnit() {
        if (cgPhase==TOPDOWN_2) {
            if (VERY_BIG_CG) {
                terminatingCallGraph = true ;
                // Field tmp4 will be true when this Block's pointerInfosIn info is
                // finally computed. When all successors of some Block have their
                // pointerInfosIn info computed, then this Block's afterDests may be erased.
                curUnit.entryBlock().tmp4 = false ;
                Chrono conclusionTimer = new Chrono();
                TapList<Block> allBlocks = curUnit.allBlocks ;
                while (allBlocks!=null) {
                    allBlocks.head.tmp4 = false ;
                    allBlocks = allBlocks.tail;
                }
                curUnit.exitBlock().tmp4 = false ;
                TapEnv.printOnTrace(15, "@@ Pointer Destinations conclusion for unit " + curUnit.name() + " ");
                conclusionTimer.reset();
                resetSweeps() ;
                analyzeForward(null, null, null) ;
                TapEnv.printlnOnTrace(15, sweeps()+" sweeps, " + conclusionTimer.elapsed() + " s");
                setTopDownContext(curUnit, null) ;
                terminatingCallGraph = false ;
            }
            if (TapEnv.modeIsNoDiff() && !TapEnv.isDistribVersion() && curUnit.hasSource()) {
                placePointerDebugComments();
            }
        }
        beforeDests = null;
        beforeDestsCycle = null;
        afterDests = null;
        afterDestsCycle = null;
        throughDests = null;
        tmpDests = null;
        tmpDestsCycle = null;
    }

    /**
     * In addition to super.setCurUnitEtc(), sets nDPZ.
     */
    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        curSymbolTable = ((unit == null) ? null : unit.publicSymbolTable());
        nDPZ = (curSymbolTable==null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
    }

    /**
     * Initialize just before pointer analysis on "curUnit".
     * This is re-done once per Unit during each sweep on the CG.
     */
    @Override
    protected void initializeFGForUnit() {
        if (!VERY_BIG_FG) {
            // If code is too "big", do not store pointer destinations *before* a Block:
            beforeDests = new BlockStorage<>(curUnit);
            beforeDestsCycle = new BlockStorage<>(curUnit);
        }
        afterDests = new BlockStorage<>(curUnit);
        afterDestsCycle = new BlockStorage<>(curUnit);
        throughDests = new BlockStorage<>(curUnit);
        returnedZoneTree = null;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            if (terminatingCallGraph) {
                TapEnv.printlnOnTrace("========= POINTER ANALYSIS: split termination pass on UNIT " + curUnit.name() + " : ==========");
            } else {
                TapEnv.printlnOnTrace("============== " + curAnalysisName.toUpperCase() + " OF UNIT " + curUnit.name() + " : ==============");
            }
            curSymbolTable = curUnit.bodySymbolTable();
            if (curSymbolTable == null) {
                curSymbolTable = curUnit.publicSymbolTable();
            }
            nDZ = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            TapEnv.printOnTrace("Zones:");
            for (int i = 0; i < nDZ; i++) {
                ZoneInfo zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zi != null) {
                    TapEnv.printOnTrace(" [" + i + ":" + (zi.ptrZoneNb!=-1 ? zi.ptrZoneNb : " ") + "]" + zi.accessTreePrint(curUnit.language()));
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace();
            TapEnv.incrTraceIndent(2) ;
        }
    }

    /**
     * In addition to super.setCurBlockEtc(), sets nDPZ
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        nDPZ = (curSymbolTable==null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
    }

    @Override
    protected void initializeFGForInitBlock() {
        if (cgPhase==BOTTOMUP_1) {
            afterDests.store(curBlock, identityPointerMatrix());
        } else { // cgPhase==TOPDOWN_2
            tmpDests = (BoolMatrix)topDownContext(curUnit) ;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("Cumulated dests context retrieved for current Unit:") ;
                TapEnv.dumpBoolMatrixOnTrace(tmpDests) ;
                TapEnv.printlnOnTrace() ;
            }
            afterDests.store(curBlock, tmpDests);
        }
    }

    /**
     * Initializes for curBlock before analysis of its Unit.
     * Fills the BoolMatrix of the pointer effect of curBlock.
     * If no pointer around, does nothing and exits quickly.
     */
    @Override
    protected void initializeFGForBlock() {
        initializingUnit = true ;
        tmpDests = identityPointerMatrix();
        tmpDestsCycle = null;
        boolean arrives = propagateDestsThroughBlock(curBlock);
        if (!arrives) {
            tmpDests = null;
        }
        throughDests.store(curBlock, tmpDests);
        initializingUnit = false ;
    }

    @Override
    protected boolean compareUpstreamValues() {
        if (VERY_BIG_FG) {
            // If code is too "big", don't look for fixed-point of pointer destinations *before* a Block:
            return true ;
        } else {
//DEBUGGING:
// if (curBlock.rank==3) {
// TapEnv.printlnOnTrace("COMPARE UPSTREAM "+curBlock+" :") ;
// TapEnv.dumpBoolMatrixOnTrace(tmpDests) ;
// TapEnv.printlnOnTrace("WITH STORAGE:") ;
// TapEnv.dumpBoolMatrixOnTrace(beforeDests.retrieve(curBlock)) ;
// }
            return compareWithStorage(tmpDests, tmpDestsCycle, beforeDests, beforeDestsCycle);
//DEBUGGING:
//             boolean reply = compareWithStorage(tmpDests, tmpDestsCycle, beforeDests, beforeDestsCycle);
// if (curBlock.rank==3) {
// TapEnv.printlnOnTrace(reply+", NOW STORAGE:") ;
// TapEnv.dumpBoolMatrixOnTrace(beforeDests.retrieve(curBlock)) ;
// }
//             return reply ;
        }
    }

    @Override
    protected boolean compareDownstreamValues() {
        return compareWithStorage(tmpDests, tmpDestsCycle, afterDests, afterDestsCycle);
    }

    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        return false;
    }

    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        return false ;
    }

    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        return propagateDestsThroughBlock(curBlock);
    }

    @Override
    protected void terminateFGForBlock() {
        terminatingUnit = true;
        if (VERY_BIG_FG) {
            accumulateValuesFromUpstream(); // accumulates into tmpDests (and tmpDestsCycle)
        } else {
            tmpDests = beforeDests.retrieve(curBlock);
            tmpDestsCycle = beforeDestsCycle.retrieve(curBlock);
        }
        propagateDestsThroughBlock(curBlock);
        terminatingUnit = false;
    }

    /**
     * Stores the condensed pointer dests info after analysis of "curUnit".
     * Prepares the pointer destination comments when requested.
     * Makes removeDanglingPointers() emit messages for dangling pointers.
     * Builds the final pointer dests info for curUnit,
     * to be used during analysis of calling units.
     * This is re-done once per Unit during each sweep on the CG.
     */
    @Override
    protected boolean terminateFGForUnit() {
        TapList<Tree> comments;
        boolean somethingChanged = false;
        //Build and store the "condensed" pointer dests info after analysis of the
        // curUnit. The condensed info is, for each Block, the BoolMatrix at
        // the block's entry (stored into block.pointerInfosIn), plus the deltas
        // after each instruction (stored in instruction.pointerDestsChanges).
        //Prepare the comments about pointer destinations for debug.
        //Prepare the complaints for dangling pointers.
        EntryBlock entryBlock = curUnit.entryBlock ;
        tmpDests = afterDests.retrieve(entryBlock);
        if (tmpDests!=null) {
             wipeIdentityRows(tmpDests, curUnit.publicSymbolTable()) ;
        }
        entryBlock.pointerInfosIn = tmpDests ;
        entryBlock.tmp4 = true ;
        ExitBlock exitBlock = curUnit.exitBlock ;
        setCurBlockEtc(exitBlock);
        terminatingUnit = true; // To force detection of dangling pointers
        accumulateValuesFromUpstream(); // accumulates into tmpDests
        terminatingUnit = false;
        if (tmpDests!=null) {
             wipeIdentityRows(tmpDests, curUnit.publicSymbolTable()) ;
        }
        exitBlock.pointerInfosIn = tmpDests ;
        exitBlock.tmp4 = true ;

        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("== Dests entering exit Block :");
            TapEnv.dumpBoolMatrixOnTrace(exitBlock.pointerInfosIn);
        }
        if (cgPhase==TOPDOWN_2) {
            if (TapEnv.modeIsNoDiff() && !TapEnv.isDistribVersion() && (!VERY_BIG_CG || terminatingCallGraph)) {
                TapList<TapPair<TapIntList, BoolVector>> pointerDestsChanges = findDestsChangesAfterFlowMerge(exitBlock) ;
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace(" -- placing "+TapList.length(pointerDestsChanges)+" pointer comments before exit Block");
                }
                comments = buildCommentsForPointerDests(pointerDestsChanges) ;
                //pre-Attach comments on the appropriate Tree:
                if (comments != null) {
                    exitBlock.setPointerComments(comments);
                }
            }
        } else { // cgPhase==BOTTOMUP_1
            //Build the final pointer destinations info for the current curUnit,
            // and store it into the curUnit. Return true if something changed.
            if (tmpDests != null) {
                returnTrees.store(curUnit, returnedZoneTree);
                //Check for changes of the curUnit's pointerEffect matrix since last time computed.
                BoolMatrix oldUnitEffect = curUnit.pointerEffect;
                if (oldUnitEffect==null || !tmpDests.equalsBoolMatrix(oldUnitEffect)) {
                    somethingChanged = true;
                    curUnit.pointerEffect = tmpDests;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("== Pointer dests effect stored for unit " + curUnit.name() + " is:");
                        TapEnv.dumpBoolMatrixOnTrace(tmpDests);
                        TapEnv.printlnOnTrace("Result Zones Tree: " + returnedZoneTree);
                        TapEnv.printlnOnTrace();
                    }
                }
            }
        }
//DEBUGGING:
//ONLY FOR TESTING RETURN ZONES TREE
// returnTrees.store(curUnit, returnedZoneTree); // DON'T KEEP!
        if (TapEnv.traceCurAnalysis()) {
// TapEnv.printlnOnTrace("OVERWRITING Result Zones Tree: " + returnedZoneTree);
// TapEnv.printlnOnTrace();
            TapEnv.decrTraceIndent(2) ;
        }
        setCurBlockEtc(null);
        return somethingChanged ;
    }

    @Override
    protected void setEmptyCumulAndCycleValues() {
        tmpDests = null;
        tmpDestsCycle = null;
    }

    @Override
    protected boolean getValueFlowingThrough() {
        Block origin = curArrow.origin;
        if (runSpecialCycleAnalysis(origin) && curArrow.containsCase(FGConstants.NEXT)) {
            additionalDests = afterDestsCycle.retrieve(origin);
        } else {
            additionalDests = afterDests.retrieve(origin);
        }
//DEBUGGING:
// TapEnv.printlnOnTrace("ARRIVING DOWN "+origin);
// TapEnv.dumpBoolMatrixOnTrace(additionalDests) ;
// TapEnv.printlnOnTrace() ;
        additionalDests = removeDanglingPointers(additionalDests, curArrow);
//DEBUGGING:
// TapEnv.printlnOnTrace("IE "+origin);
// TapEnv.dumpBoolMatrixOnTrace(additionalDests) ;
// TapEnv.printlnOnTrace() ;
        return additionalDests != null;
    }

    @Override
    protected void initializeCumulValue() {
        tmpDests = zeroPointerMatrix();
    }

    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        if (tmpDests == null) {
            tmpDests = zeroPointerMatrix();
        }
        accumulateNewDestsInto(additionalDests, tmpDests,
                commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND),
                commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
//DEBUGGING:
// TapEnv.printlnOnTrace("-> ACCUMULATED:");
// TapEnv.dumpBoolMatrixOnTrace(tmpDests) ;
// TapEnv.printlnOnTrace() ;
    }

    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        if (tmpDestsCycle == null) {
            tmpDestsCycle = zeroPointerMatrix();
        }
        accumulateNewDestsInto(additionalDests, tmpDestsCycle,
                commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND),
                commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
    }

    /**
     * Compare a newly computed dests info "newInfo+newInfoCycle" with the reference
     * info computed at a previous stage. When the new info is different, updates the
     * reference info and return true. Otherwise return false.
     *
     * @param newInfo          The new dests info for the current Block.
     * @param newInfoCycle     The new dests info in case of cycle, when applicable.
     * @param storedInfos      The storage of reference infos.
     * @param storedInfosCycle The storage of reference cycle infos.
     * @return True when the new info represents an update.
     */
    private boolean compareWithStorage(BoolMatrix newInfo, BoolMatrix newInfoCycle,
                                       BlockStorage<BoolMatrix> storedInfos,
                                       BlockStorage<BoolMatrix> storedInfosCycle) {
        boolean modified = false;
        BoolMatrix oldInfo = storedInfos.retrieve(curBlock);
        if (newInfo != null && (oldInfo == null || !newInfo.equalsBoolMatrix(oldInfo))) {
            modified = true;
            storedInfos.store(curBlock, newInfo.copy());
        }
        if (runSpecialCycleAnalysis(curBlock)) {
            oldInfo = storedInfosCycle.retrieve(curBlock);
            if (newInfoCycle != null
                    && (oldInfo == null || !newInfoCycle.equalsBoolMatrix(oldInfo))) {
                modified = true;
                storedInfosCycle.store(curBlock, newInfoCycle.copy());
            }
        }
        return modified;
    }

    /**
     * Accumulate (OR) the matrix "newDests" into "cumulDests", taking into account
     * only the ("commonRows" * "commonCols") upper left rows and columns of "newDests".
     * Also set possible Undef dest for pointers that start their scope on this arrow.
     *
     * @param newDests   The new dests matrix to accumulate into cumulDests.
     * @param cumulDests The dests matrix into which accumulation is made.
     * @param commonRows The number of rows of newDests that will go into cumulDests.
     * @param commonCols The number of columns of newDests that will go into cumulDests.
     */
    private void accumulateNewDestsInto(BoolMatrix newDests, BoolMatrix cumulDests,
                                        int commonRows, int commonCols) {
        boolean firstAccumulation = false;
        int undefColumn = curCallGraph.zoneNbOfUnknownDest ;
        if (cumulDests.rows == null) {
            firstAccumulation = true;
            cumulDests.rows = new BoolVector[cumulDests.nRows];
        }
        for (int i = 0; i < commonRows; i++) {
            if (firstAccumulation) {
                if (newDests.rows[i] == null) {
                    cumulDests.rows[i] = null;
                } else {
                    cumulDests.rows[i] = newDests.rows[i].copy(commonCols, nDZ);
                }
            } else {
                if (cumulDests.rows[i] == null) {
                    if (newDests.rows[i] != null) {
                        cumulDests.rows[i] = newDests.rows[i].copy(commonCols, nDZ);
                        cumulDests.rows[i].set(identityColumnRanks(i, curSymbolTable), true);
                    }
                } else {
                    if (newDests.rows[i] == null) {
                        cumulDests.rows[i].set(identityColumnRanks(i, curSymbolTable), true);
                    } else {
                        cumulDests.rows[i].cumulOr(newDests.rows[i], commonCols);
                    }
                }
            }
        }
        for (int i = cumulDests.nRows - 1; i >= commonRows; i--) {
            if (firstAccumulation) {
                cumulDests.rows[i] = new BoolVector(curUnit.hasTooManyZones(), cumulDests.nCols);
            } else if (cumulDests.rows[i] == null) {
                cumulDests.rows[i] = new BoolVector(curUnit.hasTooManyZones(), cumulDests.nCols);
                cumulDests.rows[i].set(identityColumnRanks(i, curSymbolTable), true);
            }
            ZoneInfo pointerZoneInfo = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.PTRKIND);
            if (pointerZoneInfo != null && ((PointerTypeSpec) pointerZoneInfo.type.wrappedType).offsetLength != null) {
                // if this zone was declared as a sized array, it already points to this array:
                TapIntList colRks = ZoneInfo.listAllZones(pointerZoneInfo.targetZonesTree, true);
                cumulDests.rows[i].set(colRks, true);
            } else {
                // this zone is a free pointer which points to Undef after its declaration:
                cumulDests.rows[i].set(undefColumn, true);
            }
        }
    }

    /**
     * Updates the dests matrix downstream the given "block".
     * Given the dests info before "block" (in tmpDests+tmpDestsCycle), modifies it
     * into the dests info after "block". Takes care of flow-graph cycle headers.
     * Except for the 1st and last sweeps, and for cycle header blocks, this updating is
     * done by a simple composition with a pre-computed block's effect stored in "throughDests".
     *
     * @param block     The current Block.
     * @param init      True if this is the initialization sweep.
     * @return True iff the control flow reaches the end of the block.
     */
    private boolean propagateDestsThroughBlock(Block block) {
        if (TapEnv.traceCurAnalysis()) {
            if (initializingUnit) {
                TapEnv.printlnOnTrace("== Initialize dests effect through " + block + " to identity :");
            } else if (!terminatingUnit) {
                TapEnv.printlnOnTrace() ;
                TapEnv.printlnOnTrace("== Dests entering " + block + " :");
            } else {
                TapEnv.printlnOnTrace() ;
                TapEnv.printlnOnTrace("== Termination sweep through " + block + " with entering dests :") ;
            }
            TapEnv.dumpBoolMatrixOnTrace(tmpDests);
        }
        boolean specialCycle = runSpecialCycleAnalysis(block);
        if (TapEnv.traceCurAnalysis()) {
            if (specialCycle) {
                TapEnv.printlnOnTrace("++ entering Cycling Dests:") ;
                TapEnv.dumpBoolMatrixOnTrace(tmpDestsCycle);
            }
            TapEnv.incrTraceIndent(2) ;
        }
        boolean reachesEnd = true ; // true if control flow may reach the end of block.
        if (!specialCycle && !initializingUnit && !terminatingUnit) {
            //Compose with the summarized block's effect:
            BoolMatrix blockPrivateEffect = throughDests.retrieve(block);
            if (blockPrivateEffect == null) {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("== Control doesn't go through "+block);
                }
                reachesEnd = false ;
            } else {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("== Composed with :");
                    TapEnv.dumpBoolMatrixOnTrace(blockPrivateEffect);
                }
                if (tmpDests != null) {
                    tmpDests = composePointerDests(tmpDests, pointerDestsClosure(tmpDests),
                                       blockPrivateEffect, nDPZ, nDZ, null, null, null);
                } else {
                    reachesEnd = false ;
                }
            }
        } else {
            //Propagate instruction per instruction:
            TapList<Instruction> instructions = block.instructions;
            TapList<Tree> comments;
            setUniqueAccessZones(block);
            BoolMatrix previousDests = null;
            // true iff it is time to place final "pointer-destination change" field and pointer comments:
            boolean placeFinalInfo =
                (terminatingUnit && cgPhase==TOPDOWN_2 && (!VERY_BIG_CG || terminatingCallGraph)) ;
            if (specialCycle && !terminatingUnit) {
                curInstruction = instructions.head;
                Tree doTree = curInstruction.tree.down(3);
                if (tmpDests != null) {
                    // Dry-run on doTree.down(2), just to get possible "calledUnits" annotation right:
                    pointersThroughExpression(doTree.down(2), tmpDests, null, null);
                    pointersThroughExpression(ILUtils.buildInitAssign(doTree), tmpDests, null, null);
                }
                if (tmpDestsCycle != null) {
                    BoolMatrix tmpDestsCycleMemo = tmpDestsCycle.copy();
                    TapIntList localizedZones = ((HeaderBlock) block).localizedZones;
                    mapExplicitZeroDeclAllkZoneRk(tmpDestsCycle, localizedZones);
                    if (tmpDests != null) {
                        explicitIdentitiesBeforeCumulOr(tmpDestsCycle, tmpDests, curSymbolTable);
                        tmpDestsCycle.cumulOr(tmpDests, false, -1);
                        TapIntList entryToExitMask =
                                declaredZonesNotFromEntryToExit((HeaderBlock) block, SymbolTableConstants.ALLKIND);
                        mapExplicitZeroDeclAllkZoneRk(tmpDests, entryToExitMask);
                        explicitIdentitiesBeforeCumulOr(tmpDestsCycleMemo, tmpDests, curSymbolTable);
                        tmpDestsCycleMemo.cumulOr(tmpDests, false, -1);
                    }
                    tmpDests = tmpDestsCycleMemo;
                } else {
                    tmpDestsCycle = (tmpDests == null ? null : tmpDests.copy()) ;
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("== Dests after loop control:") ;
                    TapEnv.dumpBoolMatrixOnTrace(tmpDests);
                    TapEnv.printlnOnTrace("++ Cycling Dests:") ;
                    TapEnv.dumpBoolMatrixOnTrace(tmpDestsCycle);
                }
            }

            if (placeFinalInfo) {
                BoolMatrix currentDests = (tmpDests == null ? null : tmpDests.copy());
                if (tmpDestsCycle != null) {
                    explicitIdentitiesBeforeCumulOr(currentDests, tmpDestsCycle, curSymbolTable);
                    currentDests.cumulOr(tmpDestsCycle, false, -1);
                }
                if (currentDests!=null) {
                    wipeIdentityRows(currentDests, block.symbolTable) ;
                }
                block.pointerInfosIn = currentDests;
                block.tmp4 = true ;
                previousDests = currentDests;
                if (TapEnv.modeIsNoDiff() && !TapEnv.isDistribVersion() && (!VERY_BIG_CG || terminatingCallGraph)) {
                    TapList<TapPair<TapIntList, BoolVector>> pointerDestsChanges = findDestsChangesAfterFlowMerge(block) ;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" -- placing "+TapList.length(pointerDestsChanges)+" pointer comments before "+block);
                    }
                    comments = buildCommentsForPointerDests(pointerDestsChanges) ;
                    //pre-Attach comments on the appropriate Tree:
                    if (comments != null && block.instructions != null) {
                        tempAttachPreComments(block.headInstr(), comments);
                    }
                }
                if (VERY_BIG_FG) {
                    // For each predecessor pred of this block, when all successors of pred have their
                    // pointerInfosIn info computed, then pred's afterDests info may be erased immediately:
                    TapList<FGArrow> backFlow = block.backFlow() ;
                    Block pred ;
                    TapList<FGArrow> flow ;
                    boolean allSuccessorsReady ;
                    while (backFlow!=null) {
                        pred = backFlow.head.origin ;
                        if (!(pred instanceof EntryBlock)) { // Don't erase afterDests of EntryBlock yet!
                            flow = pred.flow() ;
                            allSuccessorsReady = true ;
                            while (flow!=null) {
                                if (!flow.head.destination.tmp4 || flow.head.destination instanceof ExitBlock) {
                                    allSuccessorsReady = false ;
                                }
                                flow = flow.tail ;
                            }
                            if (allSuccessorsReady) {
                                afterDests.store(pred, null) ;
                            }
                        }
                        backFlow = backFlow.tail ;
                    }
                }
            }

            ToBool arrives = new ToBool(true);
            ToBool arrivesC = new ToBool(true);
            while (instructions != null) {
                curInstruction = instructions.head;
                if (tmpDests != null) {
                    pointersThroughExpression(curInstruction.tree, tmpDests, null, arrives);
                }
                if (tmpDestsCycle != null) {
                    pointersThroughExpression(curInstruction.tree, tmpDestsCycle, null, arrivesC);
                }
                if (TapEnv.traceCurAnalysis()) {
                    if (reachesEnd && !(arrives.get() && arrivesC.get())) {
                        TapEnv.printlnOnTrace("Control doesn't go through instruction "+curInstruction);
                    }
                }
                reachesEnd = reachesEnd && arrives.get() && arrivesC.get();
                // The following post-processing needs to be done only at the end of
                // analysis of a Unit, only during the TOPDOWN_2 phase:
                // (i.e. when called by terminateFGForBlock())
                if (placeFinalInfo) {

                    BoolMatrix currentDests = (tmpDests==null ? null : tmpDests.copy()) ;
                    if (tmpDestsCycle != null) {
                        explicitIdentitiesBeforeCumulOr(currentDests, tmpDestsCycle, curSymbolTable);
                        currentDests.cumulOr(tmpDestsCycle, false, -1);
                    }
                    //Compute and store the delta info on pointers:
                    curInstruction.pointerDestsChanges =
                            findDestsChangesAfterInstruction(previousDests, currentDests);
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace(" -- placing "+TapList.length(curInstruction.pointerDestsChanges)+" pointer changes after Instruction "+curInstruction);
                    }
                    previousDests = currentDests;
                    if (TapEnv.modeIsNoDiff() && !TapEnv.isDistribVersion()) {
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace(" -- placing "+TapList.length(curInstruction.pointerDestsChanges)+" pointer comments after Instruction "+curInstruction);
                        }
                        comments = buildCommentsForPointerDests(curInstruction.pointerDestsChanges);
                        //pre-Attach comments on the appropriate Tree:
                        if (comments != null) {
                            tempAttachPostComments(curInstruction, comments);
                        }
                    }
                }
                instructions = instructions.tail;
            }
            uniqueAccessZones = null;
            curInstruction = null;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.decrTraceIndent(2) ;
            if (!reachesEnd) {
                TapEnv.printlnOnTrace("== NO dests "+(initializingUnit ? "effect for " : "propagated through ")+block) ;
            } else {
                if (initializingUnit) {
                    TapEnv.printlnOnTrace("== Final dests effect through " + block + " :");
                    TapEnv.dumpBoolMatrixOnTrace(tmpDests);
                } else {
                    TapEnv.printlnOnTrace("== End "+(terminatingUnit?"termination ":"")+"sweep through " + block+", exiting Dests:") ; 
                    TapEnv.dumpBoolMatrixOnTrace(tmpDests);
                }
                if (specialCycle) {
                    TapEnv.printlnOnTrace("++ exiting Cycling Dests:") ;
                    TapEnv.dumpBoolMatrixOnTrace(tmpDestsCycle);
                }
            }
            if (initializingUnit) {
                TapEnv.printlnOnTrace() ;
            }

        }
        return reachesEnd ;
    }

    /**
     * Propagates pointer destinations information "dests" forwards through expression "expression".
     * Updates "dests" on the fly when some operation changes the destination of some pointer.
     * Recurses everywhere inside expressions because of the (very rare) cases
     * where there is a side-effect assignment inside.
     * <p>
     * The resulting TapList tree of zones+dests of the given expression contains:<br>
     * -- the zones of the expression each time they exist
     * (they don't exist e.g. for temp values on the stack (a+b) or immediate constants)<br>
     * -- the zones pointed by the pointers contained in the expression, when
     * these pointed zones cannot be retrieved from the expression zones (e.g. &amp;x).
     * <p>
     * In addition, on each procedure call, this method sets the precomputed
     * zones of the actual arguments of the call.
     *
     * @param expression The expression tree to analyze.
     * @param dests      The pointer destinations information to be propagated.
     * @param total      If non-null, contains false upon exit if "expression" covers only a part of its zones.
     * @param arrives    Contains false upon exit when execution does not reach the end of "expression".
     * @return the TapList-tree (of TapIntList) of (extended declared) zones
     * of "expression", extended (when "expression" contains pointers) with the
     * tree (in the cdr parts) of the corresponding present pointer destinations.
     */
    private TapList pointersThroughExpression(Tree expression, BoolMatrix dests,
                                              ToBool total, ToBool arrives) {
        switch (expression.opCode()) {
            case ILLang.op_assign: {
                ToBool lhsTotal = new ToBool(false);
                TapList rhsZones = pointersThroughExpression(expression.down(2), dests, null, arrives);
                functionPointerFlag = false;
                if (rhsZones != null && rhsZones.tail != null
                        && rhsZones.tail.head instanceof TapIntList
                        && ((TapIntList) rhsZones.tail.head).head == curCallGraph.zoneNbOfUnknownDest) {
                    rhsZones = null;
                }
                if (rhsZones != null) {
                    //rhsZones can be null in the case of an Undef destination.
                    rhsZones = TapList.copyTree(rhsZones);
                    includePointedElementsInTree(rhsZones, null, dests, false, true, true);
                }
                TapList lhsZones = pointersThroughExpression(expression.down(1), dests, lhsTotal, arrives);
                boolean isPointerAssign =
                        TypeSpec.isA(curSymbolTable.typeOf(expression.down(1)), SymbolTableConstants.POINTERTYPE);
                if (!isPointerAssign && expression.down(2).opCode() == ILLang.op_allocate) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF10) No allocation allowed for " + ILUtils.toString(expression.down(1)));
                    return null;
                }
                // Special for C: assigning 0 to a pointer means NULL pointer i.e. extended zone curCallGraph.zoneNbOfNullDest:
                if (isPointerAssign && rhsZones == null && curUnit.isC()) {
                    Integer rhsIntVal = curSymbolTable.computeIntConstant(expression.down(2));
                    if (rhsIntVal != null && rhsIntVal == 0) {
                        rhsZones = new TapList<>(null, new TapList<>(new TapIntList(curCallGraph.zoneNbOfNullDest, null), null));
                    }
                }
                if (inADeclaration && rhsZones == null) {
                    //Case of init-decl "float *p = {1.2 2.0 -2.5} ;"
                    TapIntList lhsZonesList = ZoneInfo.listAllZones(lhsZones, false);
                    ZoneInfo pointerZone;
                    while (lhsZonesList != null) {
                        pointerZone = curSymbolTable.declaredZoneInfo(lhsZonesList.head, SymbolTableConstants.ALLKIND);
                        if (pointerZone != null && pointerZone.ptrZoneNb != -1) {
                            rhsZones = TapList.cumulWithOper(rhsZones,
                                    pointerZone.targetZonesTree, SymbolTableConstants.CUMUL_OR);
                        }
                        lhsZonesList = lhsZonesList.tail;
                    }
                    if (rhsZones != null) {
                        rhsZones = new TapList<>(null, rhsZones);
                    }
                }
                //TODO: put this into parent class referenceIsTotal():
                boolean totalAccess =
                        referenceIsTotal(lhsTotal.get(), lhsZones)
                                //In Fortran, a initDeclarator is done only once:
                                && !(inADeclaration && curUnit.isFortran());
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("ASSIGNMENT " + ILUtils.toString(expression) + " : " + lhsZones + " RECEIVES (total:" + totalAccess + ") " + rhsZones);
                }
                if (expression.down(1).opCode() == ILLang.op_ident &&
                        (ILUtils.getIdentString(expression.down(1)).equals(curUnit.name()) ||
                                curUnit.otherReturnVar() != null &&
                                        ILUtils.getIdentString(expression.down(1)).equals(curUnit.otherReturnVar().symbol))) {
                    //This statement sets a new possible returned value:
                    returnedZoneTree = TapList.cumulWithOper(returnedZoneTree, rhsZones, SymbolTableConstants.CUMUL_OR);
                }
                return pointerAssignmentRec(dests, lhsZones, rhsZones, totalAccess, null);
            }
            case ILLang.op_call: {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("PROCEDURE CALL " + ILUtils.toString(expression)) ;
                    TapEnv.incrTraceIndent(4) ;
                    TapEnv.printlnOnTrace("-------------- Pointer analysis of procedure call " + expression + " : --------------");
                }
                TapList result ;
                // First, propagate through each actual parameter of the call:
                Tree[] actualParams = ILUtils.getArguments(expression).children();
                ToBool paramTotal = new ToBool(false);
                boolean[] actualParamTotalS = new boolean[actualParams.length];
                TapList[] actualParamZonesTreeS = new TapList[actualParams.length];
                for (int i = actualParams.length - 1; i >= 0; --i) {
                    actualParamZonesTreeS[i] = pointersThroughExpression(actualParams[i], dests, paramTotal, arrives);
                    functionPointerFlag = false;
                    actualParamTotalS[i] = paramTotal.get();
                    if (actualParamZonesTreeS[i] != null) {
                        actualParamZonesTreeS[i] = TapList.copyTree(actualParamZonesTreeS[i]);
                        includePointedElementsInTree(actualParamZonesTreeS[i], null, dests, true, true, true);
                    } else {
                        actualParamZonesTreeS[i] = new TapList<>(null, null);
                    }
                }
                if (TapEnv.traceCurAnalysis()) {
                    for (int i=0 ; i<actualParamZonesTreeS.length ; ++i) {
                        TapEnv.printlnOnTrace("- zones of actual param#"+(i+1)+" are (total="+actualParamTotalS[i]+")"+actualParamZonesTreeS[i]) ;
                    }
                }
                // Keep those as an annotation, useful for most following analysis:
                expression.setAnnotation("actualParamZonesTreeS", actualParamZonesTreeS) ;
                expression.setAnnotation("actualParamTotalS", actualParamTotalS) ;

                if (total != null) {
                    total.set(false);
                }
                // Detect the special case of an overloaded call (e.g. through a pointer-to-function)
                TapList<Unit> calledUnits = getCalledUnitsInPointerAnalysis(expression, dests) ;
                if (calledUnits == null) {
                    TapEnv.toolError("Could not find called Unit for Pointer analysis on " + expression);
                    result = null ;
                } else if (calledUnits.tail==null) {
                    result = pointersThroughSingleUnitCall(expression, calledUnits.head, dests, arrives) ;
                } else {
                    // if this call is overloaded (to several Units),
                    //  arrange parallel propagation through each of them:
                    result = pointersThroughOverloadedCalls(expression, calledUnits, dests, arrives) ;
                }
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("-------------------- end pointer analysis of call " + expression + " ----------------");
                    TapEnv.decrTraceIndent(4) ;
                }
                return result ;
            }
            case ILLang.op_fieldAccess: {
                TapList zt = pointersThroughExpression(expression.down(1), dests, total, arrives);
                int fieldRank = ILUtils.getFieldRank(expression.down(2));
                if (fieldRank != -1 && TapList.nth(zt, fieldRank) instanceof TapList) {
                    return (TapList) TapList.nth(zt, fieldRank);
                } else { // Weird cases or Error cases:
                    if (total != null) {
                        total.set(false);
                    }
                    return zt;
                }
            }
            case ILLang.op_arrayAccess: {
                TapList zt = pointersThroughExpression(expression.down(1), dests, total, arrives);
                WrapperTypeSpec typeSpec = curSymbolTable.typeOf(expression.down(1));
                ArrayDim[] dims = null;
                if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                    dims = ((ArrayTypeSpec) typeSpec.wrappedType).dimensions();
                }
                Tree[] indexes = expression.down(2).children();
                boolean tot = total != null && total.get() && dims != null && dims.length == indexes.length;
                for (int i = indexes.length - 1; i >= 0; i--) {
                    pointersThroughExpression(indexes[i], dests, null, arrives);
                    tot = tot && dims[i] != null && dims[i].fullArrayTriplet(indexes[i]);
                }
                if (total != null) {
                    total.set(tot);
                }
                return zt;
            }
            case ILLang.op_pointerAccess: {
                TapList dest1 = pointersThroughExpression(expression.down(1), dests, total, arrives);
                if(functionPointerFlag) {
                    return dest1;
                } else {
                    TapList res = curSymbolTable.zonesOfDestination(
                            dest1,
                            curSymbolTable.typeOf(expression.down(1)),
                            curInstruction, dests);                
                    if (total != null) {
                        total.set(false);
                    }
                    return res;
                }   
            }
            case ILLang.op_address: {
                TapList result = pointersThroughExpression(expression.down(1), dests, total, arrives);
                if (functionPointerFlag) {
                    return result;
                }
                return new TapList<>(null, result);
            }
            case ILLang.op_ident: {
                if (total != null) {
                    total.set(true);
                }
                VariableDecl variableDecl =
                        curSymbolTable.getVariableDecl(expression.stringValue());
                TapList zones = null;
                // variableDecl may be null for CONSTANTs or functions
                if (variableDecl!=null) {
                    zones = variableDecl.zones();
                } else {
                    // We look for a function
                    TapList<FunctionDecl> funcDecls = curSymbolTable.getFunctionDecl(expression.stringValue(), null, null, false);
                    if (funcDecls!=null) {
                        zones = new TapList(null, funcDecls.head.zones());
                        functionPointerFlag = true;
                    }
                }
                return zones;
            }
            case ILLang.op_varDeclaration: {
                Tree[] expressions = expression.down(3).children();
                inADeclaration = true;
                for (int i = expressions.length - 1; i >= 0; i--) {
                    if (expressions[i].opCode() == ILLang.op_assign) {
                        ILUtils.turnAssignFromInitDecl(expressions[i]);
                        pointersThroughExpression(expressions[i], dests, null, arrives);
                        ILUtils.resetAssignFromInitDecl(expressions[i]);
                    }
                }
                inADeclaration = false;
                return null;
            }
            case ILLang.op_allocate: {
                TapList allocatedZones = expression.getAnnotation("allocatedZones");
                // when allocating a structure containing pointers itself, these
                // pointers are initialized to Undef:
                TapIntList subPointerRows =
                    curSymbolTable.getDeclaredKindZones(ZoneInfo.listAllZones(allocatedZones, false), SymbolTableConstants.PTRKIND, true);
                int subPtrRowIndex;
                BoolVector subPtrRow;
                while (subPointerRows != null) {
                    subPtrRowIndex = subPointerRows.head;
                    subPtrRow = dests.getRow(subPtrRowIndex);
                    if (subPtrRow == null) {
                        subPtrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                        subPtrRow.set(identityColumnRanks(subPtrRowIndex, curSymbolTable),
                                true);
                        dests.setRow(subPtrRowIndex, subPtrRow);
                    }
                    subPtrRow.set(curCallGraph.zoneNbOfUnknownDest, true);
                    subPointerRows = subPointerRows.tail;
                }
                return new TapList<>(null, allocatedZones);
            }
            case ILLang.op_deallocate: {
                Tree variable = expression.down(1);
                WrapperTypeSpec variableType = curSymbolTable.typeOf(variable);
                if (!TypeSpec.isA(variableType, SymbolTableConstants.POINTERTYPE)) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, expression, "(DF10) No deallocation allowed for " + ILUtils.toString(variable));
                } else {
                    ToBool expTotal = new ToBool(false);
                    TapList addressOfVariable =
                            curSymbolTable.treeOfZonesOfValue(variable, expTotal, null, dests);
                    TapIntList deallocZones = ZoneInfo.listAllZones(addressOfVariable, false);
                    //[llh 17/02/2016] this is almost ready to be replaced by referenceIsTotal():
                    boolean totalDealloc =
                            curInstruction.getWhereMask() == null && !oneZoneIsMultiple(deallocZones)
                                    && expTotal.get() && deallocZones != null && deallocZones.tail == null;
                    deallocZones =
                            mapZoneRkToKindZoneRk(deallocZones, SymbolTableConstants.PTRKIND);
                    int ptrRowIndex;
                    ZoneInfo ptrZ;
                    BoolVector ptrRow;
                    if (TapEnv.traceCurAnalysis()) {
                        TapEnv.printlnOnTrace("DEALLOCATE " + ILUtils.toString(variable) + " : " + deallocZones);
                    }
                    while (deallocZones != null) {
                        ptrRowIndex = deallocZones.head;
                        ptrZ = curSymbolTable.declaredZoneInfo(ptrRowIndex, SymbolTableConstants.PTRKIND);
                        ptrRow = dests.getRow(ptrRowIndex);
                        if (!totalDealloc || ptrZ.multiple) {
                            // then deallocation is only partial:
                            if (ptrRow == null) {
                                ptrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                                ptrRow.set(identityColumnRanks(ptrRowIndex, curSymbolTable),
                                        true);
                                dests.setRow(ptrRowIndex, ptrRow);
                            }
                        } else {
                            if (ptrRow == null) {
                                ptrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                                dests.setRow(ptrRowIndex, ptrRow);
                            } else {
                                ptrRow.setFalse();
                            }
                        }
                        ptrRow.set(curCallGraph.zoneNbOfUnknownDest, true);
                        deallocZones = deallocZones.tail;
                    }
                }
                return null;
            }
            case ILLang.op_nullify: {
                // Pointer "nullification" operator: sets passed pointers to NULL !
                Tree[] expressions = expression.children();
                TapList expZones;
                TapIntList expPZones;
                TapIntList expPRanks;
                ToBool totalNullify = new ToBool(false);
                int ptrRowIndex;
                ZoneInfo ptrZ;
                BoolVector ptrRow;
                for (Tree tree : expressions) {
                    expZones = pointersThroughExpression(tree, dests, totalNullify, arrives);
                    expPZones = ZoneInfo.listAllZones(expZones, false);
                    expPRanks = mapZoneRkToKindZoneRk(expPZones, SymbolTableConstants.PTRKIND);
                    while (expPRanks != null) {
                        ptrRowIndex = expPRanks.head;
                        ptrZ = curSymbolTable.declaredZoneInfo(ptrRowIndex, SymbolTableConstants.PTRKIND);
                        ptrRow = dests.getRow(ptrRowIndex);
                        if (!totalNullify.get() || ptrZ.multiple) {
                            // then nullification is only partial:
                            if (ptrRow == null) {
                                ptrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                                ptrRow.set(identityColumnRanks(ptrRowIndex, curSymbolTable), true);
                                dests.setRow(ptrRowIndex, ptrRow);
                            }
                        } else {
                            if (ptrRow == null) {
                                ptrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                                dests.setRow(ptrRowIndex, ptrRow);
                            } else {
                                ptrRow.setFalse();
                            }
                        }
                        ptrRow.set(curCallGraph.zoneNbOfNullDest, true);
                        if (TapEnv.traceCurAnalysis()) {
                            TapEnv.printlnOnTrace("NULLIFY " + ILUtils.toString(tree) + " : " + expPZones + " TO " + ptrRow);
                        }
                        expPRanks = expPRanks.tail;
                    }
                }
                return null;
            }
            case ILLang.op_ioCall:
                // We assume that only the IO expressions might (and this is dirty coding) modify pointers:
                pointersThroughExpression(expression.down(3), dests, null, arrives) ;
                return null;
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_if:
            case ILLang.op_times:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_switch:
            case ILLang.op_where:
                pointersThroughExpression(expression.down(1), dests, null, arrives);
                return null;
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power:
            case ILLang.op_mod:
            case ILLang.op_complexConstructor:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_forallRangeSet:
            case ILLang.op_forall:
            case ILLang.op_iterativeVariableRef:
                pointersThroughExpression(expression.down(1), dests, null, arrives);
                pointersThroughExpression(expression.down(2), dests, null, arrives);
                return null;
            case ILLang.op_binary:
                pointersThroughExpression(expression.down(1), dests, null, arrives);
                pointersThroughExpression(expression.down(3), dests, null, arrives);
                return null;
            case ILLang.op_add:
            case ILLang.op_sub: {
                ///Special case here because of pointer arithmetic
                TapList zt1 = pointersThroughExpression(expression.down(1), dests, null, arrives);
                TapList zt2 = pointersThroughExpression(expression.down(2), dests, null, arrives);
                if (total != null) {
                    total.set(false);
                }
                if (zt1 != null && zt1.head instanceof TapIntList && zoneIsPointer(((TapIntList) zt1.head).head)) {
                    // if zt1 is about a pointer, return it,
                    return zt1;
                } else if (zt2 != null && zt2.head instanceof TapIntList && zoneIsPointer(((TapIntList) zt2.head).head)) {
                    // if zt2 is about a pointer, return it,
                    return zt2;
                } else {
                    return null;
                }
            }
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign: {
                // if the lhs is a pointer, it still points to the same place
                pointersThroughExpression(expression.down(2), dests, null, arrives);
                TapList zt = pointersThroughExpression(expression.down(1), dests, null, arrives);
                if (total != null) {
                    total.set(false);
                }
                return zt;
            }
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
            case ILLang.op_unary:
            case ILLang.op_modifiedDeclarator:
                return pointersThroughExpression(expression.down(2), dests, total, arrives);
            case ILLang.op_arrayTriplet:
            case ILLang.op_for:
                pointersThroughExpression(expression.down(1), dests, null, arrives);
                pointersThroughExpression(expression.down(2), dests, null, arrives);
                pointersThroughExpression(expression.down(3), dests, null, arrives);
                return null;
            case ILLang.op_ifExpression: {
                pointersThroughExpression(expression.down(1), dests, null, arrives);
                TapList zt = TapList.copyTree(
                        pointersThroughExpression(expression.down(2), dests, null, arrives));
                zt = TapList.cumulWithOper(zt,
                        pointersThroughExpression(expression.down(3), dests, null, arrives),
                        SymbolTableConstants.CUMUL_OR);
                if (total != null) {
                    total.set(false);
                }
                return zt;
            }
            case ILLang.op_forallRanges:
            case ILLang.op_arrayConstructor: {
                Tree[] expressions = expression.children();
                for (int i = expressions.length - 1; i >= 0; i--) {
                    pointersThroughExpression(expressions[i], dests, null, arrives);
                }
                return null;
            }
            case ILLang.op_constructorCall: {
                return pointersThroughExpression(expression.down(2), dests, null, arrives);
            }
            case ILLang.op_expressions: {
                Tree[] expressions = expression.children();
                TapList lastExprZones = null;
                for (Tree tree : expressions) {
                    lastExprZones = pointersThroughExpression(tree, dests, total, arrives);
                }
                return lastExprZones;
            }
            case ILLang.op_switchCases:
                pointersThroughExpression(expression.down(2), dests, null, arrives);
                return null;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
            case ILLang.op_compGoto:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_letter:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_label:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_break:
            case ILLang.op_continue:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_sizeof:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_pointerType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return null;
            case ILLang.op_loop:
                pointersThroughExpression(expression.down(3), dests, null, arrives);
                return null;
            case ILLang.op_forallRangeTriplet:
            case ILLang.op_do:
                pointersThroughExpression(expression.down(3), dests, null, arrives);
                pointersThroughExpression(expression.down(4), dests, null, arrives);
                return null;
            case ILLang.op_stop:
                if (arrives != null) {
                    arrives.set(false);
                }
                return null;
            case ILLang.op_return: {
                TapList result = null;
                if (!ILUtils.isNullOrNone(expression.down(1))) {
                    Tree assignTree = ILUtils.turnReturnIntoAssign(expression, curUnit.name());
                    result = pointersThroughExpression(assignTree, dests, null, arrives);
                    ILUtils.resetReturnFromAssign(expression, assignTree);
                }
                return result;
            }
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
                return pointersThroughExpression(expression.down(1), dests, total, arrives);
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                return null;
            default:
                TapEnv.toolWarning(-1, "(Compute pointer destinations) Unexpected operator : " + expression.opName());
                return null;
        }
    }

    /**
     * Apply the effect of assignment "lhsZones := rhsZones" upon pointer dests matrix "dests".
     * Assigns top-level pointers, but not pointers across pointers, i.e. X := Y
     * makes *X%f1 contain same as *Y%f1, but not *(*X%f2) contain same as *(*Y%f2).
     *
     * @param dests    The current pointer dests matrix.
     * @param lhsZones The TapList Tree of zones+dests of the left hand side of the assign.
     * @param rhsZones The TapList Tree of zones+dests of the right hand side of the assign.
     * @param lhsTotal True when the left hand side covers its zone totally .
     * @return The TapList Tree of zones+dests of the expression returned by the assign.
     */
    private TapList pointerAssignmentRec(BoolMatrix dests,
                                         TapList lhsZones, TapList rhsZones,
                                         boolean lhsTotal, TapList<TapList> dejaVu) {
        if (lhsZones != null && !TapList.contains(dejaVu, lhsZones)) {
            if (lhsZones.head instanceof TapList) {  //recursive case:
                dejaVu = new TapList<>(lhsZones, dejaVu);
                return new TapList<>(
                        pointerAssignmentRec(dests, (TapList) lhsZones.head,
                                rhsZones != null && rhsZones.head instanceof TapList ?
                                        //null stands for the "undefined" pointer destination !
                                        (TapList) rhsZones.head : null,
                                lhsTotal, dejaVu),
                        pointerAssignmentRec(dests, lhsZones.tail,
                                rhsZones != null ? rhsZones.tail : null,
                                lhsTotal, dejaVu));
            } else {
                //leaf case: we may be on a pointer assignment
                TapIntList assignedZones = getPointerZones((TapIntList) lhsZones.head);
                if (assignedZones != null) {
                    TapIntList inAssignedZones = assignedZones;
                    boolean totalAssign;
                    int assignedZone;
                    int ptrRowIndex;
                    ZoneInfo ptrZone;
                    if (inAssignedZones.tail != null) {
                        lhsTotal = false;
                    }
                    TapIntList valueZones =
                        (rhsZones==null ? null : ZoneInfo.listAllZonesWithNU(rhsZones.tail, false));

                    if (valueZones == null) {
                        valueZones = new TapIntList(curCallGraph.zoneNbOfUnknownDest, null);
                    }
                    while (inAssignedZones != null) {
                        assignedZone = inAssignedZones.head;
                        ptrZone = curSymbolTable.declaredZoneInfo(assignedZone, SymbolTableConstants.ALLKIND);
                        ptrRowIndex = zoneRkToKindZoneRk(assignedZone, SymbolTableConstants.PTRKIND);
                        if (ptrRowIndex >= 0) {
                            totalAssign = curInstruction.getWhereMask() == null &&
                                    !ptrZone.multiple &&
                                    (lhsTotal || uniqueAccessZones != null && assignedZone >= 0
                                            && uniqueAccessZones.get(assignedZone));
                            BoolVector ptrRow = dests.getRow(ptrRowIndex);
                            if (ptrRow == null) {
                                ptrRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                                dests.setRow(ptrRowIndex, ptrRow);
                                if (!totalAssign) {
                                    //keep the identity that was implicitly here:
                                    ptrRow.set(identityColumnRanks(ptrRowIndex, curSymbolTable), true);
                                }
                            } else {
                                if (totalAssign) {
                                    ptrRow.setFalse();
                                }
                            }
                            ptrRow.set(valueZones, true);
                        }
                        inAssignedZones = inAssignedZones.tail;
                    }
                }
                return new TapList<>(assignedZones,
                        rhsZones == null ? new TapList<>(new TapIntList(curCallGraph.zoneNbOfUnknownDest, null), null) : rhsZones.tail);
            }
        } else if (rhsZones == null) {
            return new TapList<>(null, new TapList<>(new TapIntList(curCallGraph.zoneNbOfUnknownDest, null), null));
        } else {
            return new TapList<>(null, rhsZones.tail);
        }
    }

    TapList<Unit> getCalledUnitsInPointerAnalysis(Tree expression, BoolMatrix dests) {
        Unit immediateCalledUnit = getCalledUnit(expression, curSymbolTable) ;
        TapList<Unit> calledUnits = null ;
        if (terminatingUnit && cgPhase==TOPDOWN_2) {
            Tree calledName = ILUtils.getCalledName(expression) ;
            TypeSpec funcNameType = curSymbolTable.typeOf(calledName) ;
            if (calledName==null || calledName.opCode()!=ILLang.op_ident || immediateCalledUnit.isDummy()) {
                if (funcNameType!=null && funcNameType.isPointerToFunction()) {
                    calledName = ILUtils.build(ILLang.op_pointerAccess,
                                   ILUtils.copy(calledName),
                                   null) ;
                }
                Unit calledUnit ;
                TapList zonesTree = curSymbolTable.treeOfZonesOfValue(calledName, null, curInstruction, dests) ;
                TapIntList zones = ZoneInfo.listAllZones(zonesTree, false) ;
                while (zones!=null) {
                    ZoneInfo zi = curSymbolTable.declaredZoneInfo(zones.head, SymbolTableConstants.ALLKIND) ;
                    Instruction unitHeadInstr = (zi.accessTree==null ? null : zi.accessTree.getAnnotation("instruction")) ;
                    if (unitHeadInstr!=null) { //case of zi designating a possibly-pointed-to user function:
                        calledUnit = unitHeadInstr.block.unit() ;
                        CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
                        if (callArrow==null) {
                            CallGraph.addCallArrow(curUnit, SymbolTableConstants.CALLS, calledUnit) ;
                        }
                    } else if (zi.targetZoneOf!=null) { //case of zi designating some unknown "initially" pointed function:
                        calledUnit = immediateCalledUnit ;
                    } else {
                        calledUnit = null ;
                    }
                    if (calledUnit!=null)
                        calledUnits = new TapList<>(calledUnit, calledUnits) ;
                    zones = zones.tail ;
                }
            }
        }
        if (calledUnits==null && immediateCalledUnit!=null) { // fallback:
            calledUnits = new TapList<>(immediateCalledUnit, null) ;
        }
        expression.setAnnotation("calledUnits", calledUnits) ;
        return calledUnits ;
    }

    private TapList pointersThroughOverloadedCalls(Tree expression, TapList<Unit> calledUnits,
                                                   BoolMatrix dests, ToBool arrives) {
        BoolMatrix accumulatedFinalDests = null ;
        TapList accumulatedResult = null ;
        BoolMatrix savedInitialDests = dests.copy() ;
        arrives.set(false) ;
        while (calledUnits!=null) {
            ToBool oneArrives = new ToBool(true) ;
            TapList functionResultDestsTree =
                pointersThroughSingleUnitCall(expression, calledUnits.head, dests, oneArrives) ;
            if (oneArrives.get()) arrives.set(true) ;

            if (accumulatedResult==null) {
                accumulatedResult = functionResultDestsTree ;
            } else {
                accumulatedResult =
                    TapList.cumulWithOper(accumulatedResult, functionResultDestsTree, SymbolTableConstants.CUMUL_OR) ;
            }
            if (accumulatedFinalDests==null) {
                accumulatedFinalDests = dests ;
            } else {
                accumulatedFinalDests.cumulOr(dests, false, -1) ;
            }
            calledUnits = calledUnits.tail ;
            if (calledUnits!=null) {
                dests = savedInitialDests.copy() ;
            }
        }
        dests = accumulatedFinalDests ;
        return accumulatedResult ;
    }

    private TapList pointersThroughSingleUnitCall(Tree expression, Unit calledUnit,
                                                  BoolMatrix dests, ToBool arrives) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("Actual called Unit: "+calledUnit);
        }
//DEBUGGING:
// TapEnv.printOnTrace("CALLER Zones:");
// for (int i = 0; i < nDZ; i++) {
//   ZoneInfo zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
//   if (zi != null) {
//     TapEnv.printOnTrace(" [" + i + ":" + (zi.ptrZoneNb!=-1 ? zi.ptrZoneNb : " ") + "]" + zi.accessTreePrint(curUnit.language()));
//   }
// }
// TapEnv.printlnOnTrace();
// if (calledUnit.publicSymbolTable()!=null) {
// TapEnv.printOnTrace("CALLED Zones:");
// int cnDZ = calledUnit.publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
// for (int i = 0; i < cnDZ; i++) {
//   ZoneInfo zi = calledUnit.publicSymbolTable().declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
//   if (zi != null) {
//     TapEnv.printOnTrace(" [" + i + ":" + (zi.ptrZoneNb!=-1 ? zi.ptrZoneNb : " ") + "]" + zi.accessTreePrint(curUnit.language()));
//   }
// }
// }
// TapEnv.printlnOnTrace();
// TapEnv.printlnOnTrace("ARRIVING DESTS :");
// TapEnv.dumpBoolMatrixOnTrace(dests);
        TapList[] actualParamZonesTreeS = expression.getAnnotation("actualParamZonesTreeS") ;
        boolean[] actualParamTotalS = expression.getAnnotation("actualParamTotalS") ;
        curCalledUnit = calledUnit ; //set the global curCalledUnit, useful later
        SymbolTable calledParamsST = calledUnit.publicSymbolTable() ;
        BoolMatrix calleeEffect = calledUnit.pointerEffect;
        if (calleeEffect==null && (calledUnit.isVarFunction() || calledUnit.isInterface() || calledUnit.isDummy())) {
            // VarFunction and Interface have not been initialized by default.
            // We must initialize them, otherwise propagation algo will think they do a STOP inside !
            int nbPtrz = calledUnit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
            int nbz = calledUnit.paramElemsNb() ;
            calleeEffect = new BoolMatrix(calledUnit.hasTooManyZones(), nbPtrz, nbz);
            calleeEffect.setIdentity();
            calledUnit.pointerEffect = calleeEffect;
        }
        if (calleeEffect == null) {
            if (arrives != null && calledUnit.rank() >= 0) {
                arrives.set(false);
            }
            if (calledUnit.isFortran() && calledUnit.name().equals("null")) {
                return new TapList<>(null, new TapList<>(new TapIntList(curCallGraph.zoneNbOfNullDest, null), null));
            } else {
                return null;
            }
        }
        CallArrow callArrow = CallGraph.getCallArrow(curUnit, calledUnit);
        // Patch to work around a bug (on Boeing code), but this bug should rather be fixed!
        if (callArrow == null) {
            TapEnv.toolError("Could not find CallArrow from " + curUnit + " to " + calledUnit + " on " + expression);
            return null;
        }

        //Note: this piece should ? be moved back at the beginning of case op_call:
        if (terminatingUnit && cgPhase==TOPDOWN_2 && calledUnit.hasSource()) {
            TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
                expression.getAnnotation("pointersActualInitialDests") ;
            if (actualInitials4==null) {
                actualInitials4 = new TapList[]{null, null, null, null} ;
                expression.setAnnotation("pointersActualInitialDests", actualInitials4) ;
            }
            collectActualInitialDests(dests, calledUnit, actualInitials4) ;
        }
        //end Note.

        int nbFormalElemArgs;
        int nbFormalElemArgsP;
        if (calledParamsST==null) { // Case of externals, or any calledUnit without source code.
            nbFormalElemArgs = 0;
            nbFormalElemArgsP = 0;
            ZoneInfo shapeElem;
            for (int i = calledUnit.paramElemsNb() - 1; i >= 0; --i) {
                shapeElem = calledUnit.paramElemZoneInfo(i);
                if (shapeElem.isParameter() || shapeElem.isResult()) {
                    ++nbFormalElemArgs;
                    if (shapeElem.ptrZoneNb != -1) {
                        ++nbFormalElemArgsP;
                    }
                }
            }
        } else {
            nbFormalElemArgs =
                calledParamsST.freeDeclaredZone(SymbolTableConstants.ALLKIND)
                - calledParamsST.firstDeclaredZone(SymbolTableConstants.ALLKIND);
            nbFormalElemArgsP =
                calledParamsST.freeDeclaredZone(SymbolTableConstants.PTRKIND)
                - calledParamsST.firstDeclaredZone(SymbolTableConstants.PTRKIND);
        }
        int nbRows = nDPZ+nbFormalElemArgsP ;
        int nbCols = nDZ+nbFormalElemArgs ;
        TapIntList[] elemParamFolders = new TapIntList[nbFormalElemArgs];
        TapIntList[] ptrElemParamFolders = new TapIntList[nbFormalElemArgsP];
        // build the version-for-caller form of the pointer-effect matrix of
        // the called Unit, extended with parameters:
        BoolMatrix callDests =
            translateCalleeDestsToCallSite(calleeEffect, nbRows, nbCols, calledUnit);
        // extend the (closed) previous dests matrix with the dests of parameters:
        BoolMatrix destsClosedPlusParams = completeWithParams(pointerDestsClosure(dests), actualParamZonesTreeS,
                                                              callArrow, elemParamFolders, ptrElemParamFolders,
                                                              nbRows, nbCols);
        // translate the tree of the zones of the returned value, from callee to caller:
        TapList functionResultDestsTree =
            translateCalleeDataToCallSite((TapList)returnTrees.retrieve(calledUnit),
                                          calledUnit, actualParamZonesTreeS) ;
        if (TapEnv.traceCurAnalysis()) {
            ZoneInfo zi;
            if (nbFormalElemArgs > 0) {
                TapEnv.printOnTrace("Zones of called Unit's formal args:");
            }
            int firstFormalArgRk;
            if (calledParamsST != null) {
                firstFormalArgRk = calledParamsST.firstDeclaredZone(SymbolTableConstants.ALLKIND);
            } else {
                //This happens if calledUnit is an external => null publicSymbolTable !
                firstFormalArgRk = calledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND);
            }
            for (int i=0 ; i<nbFormalElemArgs ; ++i) {
                zi = formalElemArgRkToZoneInfo(i + firstFormalArgRk, SymbolTableConstants.ALLKIND, calledUnit);
                assert zi != null;
                TapEnv.printOnTrace(" [" + i + ":"+(zi.ptrZoneNb!=-1 ? zi.ptrZoneNb : " ") + "]" + zi.accessTreePrint(curUnit.language()));
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("Incoming dests, closed and extended :");
            TapEnv.dumpBoolMatrixOnTrace(destsClosedPlusParams, nDPZ, nDZ);
            TapEnv.printlnOnTrace("composed with callee's pointer dests effect:") ;
            TapEnv.dumpBoolMatrixOnTrace(calleeEffect);
            TapEnv.printlnOnTrace("and result: "+returnTrees.retrieve(calledUnit)) ;
            TapEnv.printlnOnTrace("pointer dests effect translated for this call site, as :");
            TapEnv.dumpBoolMatrixOnTrace(callDests, nDPZ, nDZ);
            TapEnv.printlnOnTrace("and result translated for this call site: "+functionResultDestsTree) ;
            for (int i = 0; i < nbFormalElemArgs; ++i) {
                TapEnv.printlnOnTrace("elemParamFolders[" + i + "] ="+ elemParamFolders[i]);
            }
            for (int i = 0; i < nbFormalElemArgsP; ++i) {
                TapEnv.printlnOnTrace("ptrElemParamFolders[" + i + "] =" + ptrElemParamFolders[i]);
            }
        }
        if (terminatingUnit && cgPhase==TOPDOWN_2 && calledUnit.hasSource() && !terminatingCallGraph) {
            BoolMatrix calleeNewContext =
                translateCallSiteDestsToCallee(dests, actualParamZonesTreeS, callArrow) ;
            if (calleeNewContext!=null) {
                wipeIdentityRows(calleeNewContext, calledUnit.publicSymbolTable()) ;
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("------------------------------") ;
                TapEnv.incrTraceIndent(4) ;
                TapEnv.printlnOnTrace("Pointer dests context propagated top-down to "+calledUnit) ;
                TapEnv.dumpBoolMatrixOnTrace(calleeNewContext);
            }
            BoolMatrix calleeExistingContext = (BoolMatrix)topDownContext(calledUnit) ;
            if (calleeExistingContext==null) {
                setTopDownContext(calledUnit, calleeNewContext) ;
                calledUnit.analysisIsOutOfDateDown = true ;
            } else {
                explicitIdentitiesBeforeCumulOr(calleeExistingContext, calleeNewContext, calledParamsST) ;
                if (calleeExistingContext.cumulOr(calleeNewContext, false , -1)) {
                    calledUnit.analysisIsOutOfDateDown = true ;
                }
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("Cumulated dests context propagated top-down to "+calledUnit) ;
                TapEnv.dumpBoolMatrixOnTrace((BoolMatrix)topDownContext(calledUnit));
                if (calledUnit.analysisIsOutOfDateDown) {
                    TapEnv.printlnOnTrace(" has changed => requires new analysis through "+calledUnit) ;
                }
                TapEnv.decrTraceIndent(4) ;
                TapEnv.printlnOnTrace("------------------------------") ;
            }
        }
        // perform the actual composition to get the dests just after the call:
        BoolMatrix destsAfterCall =
            composePointerDests(dests, destsClosedPlusParams, callDests,
                      nbRows, nbCols, ptrElemParamFolders, elemParamFolders, actualParamTotalS);
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("returns (extended with FP) outgoing dests, folded :");
            TapEnv.dumpBoolMatrixOnTrace(destsAfterCall, nDPZ, nDZ);
        }
        if (functionResultDestsTree != null) {
            // [llh 19dec24] This call to includePointedElementsInTree() is dubious. It may not do what we hope for,
            // which is to extend destinations with respect to the entry of the calling (i.e. current) Unit.
            // Actually it might do nothing at all. See for example set04/lh120.
            // I think this extension should rather be done through calls to composeDestsRow()
            includePointedElementsInTree(functionResultDestsTree, null, destsAfterCall, true, true, false);
        }
        // set the resulting "dests" to the non-FP_CLASS rows of "destsAfterCall",
        // clipped to the non-FP_CLASS columns:
        for (int i=nDPZ-1 ; i>=0 ; --i) {
            BoolVector afterRow = destsAfterCall.rows[i];
            if (afterRow != null) {
                afterRow = afterRow.copy(dests.nCols, dests.nCols);
            }
            dests.rows[i] = afterRow;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("plus result: " + functionResultDestsTree);
        }
        curCalledUnit = null;
        return functionResultDestsTree ;
    }

    /** Builds an almost-copy of "calleeDests", which is the pointer effect matrix
     * of the given "calledUnit". Returns a form suitable for usage in the calling
     * Unit to propagate pointer destinations forward across the call. The returned
     * matrix is almost the same as calleeDests, except that it is has extra rows
     * and columns (initially empty), and all rows that deal with (pointer) arguments
     * of the call are pushed down and all columns that deal with arguments are pushed
     * right, so that they now belong to the FP_CLASS ("Formal Parameters") of the
     * extended resulting matrix.
     * @param calleeDests  The matrix holding the pointer effect of the called unit.
     * @param nbRows The number of rows of the resulting matrix.
     * @param nbCols The number of columns of the resulting matrix.
     * @param calledUnit   The called Unit.
     * @return The pointer destinations matrix for the call in the calling Unit,
     *    with extra rows and columns for the formal parameters and the result.
     */
    private BoolMatrix translateCalleeDestsToCallSite(BoolMatrix calleeDests,
                                                      int nbRows, int nbCols, Unit calledUnit) {
        BoolMatrix result = new BoolMatrix(curUnit.hasTooManyZones(), nbRows, nbCols);
        result.setIdentity();
        int firstFormalArgRk, firstFormalArgRkPtr ;
        if (calledUnit.isStandard() || calledUnit.isInterface()) {
            firstFormalArgRk = calledUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.ALLKIND) ;
            firstFormalArgRkPtr = calledUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.PTRKIND) ;
        } else {
            //This happens if calledUnit is an external => null publicSymbolTable !
            firstFormalArgRk = calledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND) ;
            firstFormalArgRkPtr = calledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.PTRKIND) ;
        }
        int offsetPtr = nDPZ - firstFormalArgRkPtr ;
        // the number of bits that must be moved after the hole:
        int offsetNb = calleeDests.nCols - firstFormalArgRk ;
        for (int rowi=0 ; rowi<firstFormalArgRkPtr ; ++rowi) {
            if (calleeDests.rows[rowi]!=null) {
                result.rows[rowi] =
                    copyRowWithHole(calleeDests.rows[rowi],
                                    firstFormalArgRk, offsetNb, result.nCols) ;
            }
        }
        for (int rowi=firstFormalArgRkPtr ; rowi<calleeDests.nRows ; ++rowi) {
            if (calleeDests.rows[rowi]!=null) {
                result.rows[rowi+offsetPtr] =
                    copyRowWithHole(calleeDests.rows[rowi],
                                    firstFormalArgRk, offsetNb, result.nCols) ;
            }
        }
        return result ;
    }

    /** Translates the given callSiteDests, which is the pointer destinations matrix at some procedure's call site,
     * into the same information expressed at the called procedure's entry point */
    private BoolMatrix translateCallSiteDestsToCallee(BoolMatrix callSiteDests, TapList[] paramZones,
                                                     CallArrow callArrow) {
        Unit calledUnit = callArrow.destination ;
        int nbz = calledUnit.publicZonesNumber(SymbolTableConstants.ALLKIND) ;//was calledUnit.paramElemsNb() ;
        int nbPtrz = calledUnit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
        SymbolTable calledParamsST = calledUnit.publicSymbolTable();
        int firstParamIndex = calledParamsST.firstDeclaredZone(SymbolTableConstants.ALLKIND) ;
        int firstParamIndexPtr = calledParamsST.firstDeclaredZone(SymbolTableConstants.PTRKIND) ;
        int i ;
        Tree zoneAccessTree ;
        TapList actualArgZoneTree ;
        TapIntList actualArgZones ;
        ZoneInfo formalArgZI ;
        BoolVector callerRow ;
        boolean pointsElsewhere ;
        BoolMatrix result = new BoolMatrix(calledUnit.hasTooManyZones(), nbPtrz, nbz) ;
        result.rows = new BoolVector[nbPtrz] ;
        for (i=0 ; i<nbPtrz ; ++i) {
            formalArgZI = calledParamsST.declaredZoneInfo(i, SymbolTableConstants.PTRKIND) ;
            pointsElsewhere = false ;
            if (!PROPAGATE_ALL_POINTERS_DESTINATIONS && !formalArgZI.type.isPointerToFunction()) {
                // Then don't propagate info on variables:
                result.rows[i] = new BoolVector(nbz) ;
                pointsElsewhere = true ;
            } else if (i<firstParamIndexPtr) { // i is a row about a global (pointer) variable.
                callerRow = callSiteDests.rows[i] ;
                if (callerRow==null) {
                    callerRow = new BoolVector(callSiteDests.nCols) ;
                    pointsElsewhere = true ;
                } else {
                    for (int j=firstParamIndex ; j<callSiteDests.nCols && !pointsElsewhere ; ++j) {
                        if (callerRow.get(i)) pointsElsewhere = true ;
                    }
                }
                result.rows[i] = callerRow.copy(firstParamIndex, nbz) ;
            } else { // i is a row about a formal parameter.
                result.rows[i] = new BoolVector(nbz) ;
                actualArgZones = null ;
                if (formalArgZI.isParameter()) { // We don't care about the result.
                    // This is *almost* like ZoneInfo.crossLanguageAccessTree():
                    zoneAccessTree = formalArgZI.accessTree ;
                    if (callArrow.origin.isC() && callArrow.destination.isFortran()
                        && !formalArgZI.passesByValue(callArrow.destination, TapEnv.C)) {
// This case is not well tested yet:
                        zoneAccessTree = ILUtils.addPointerAccessAtBase(ILUtils.copy(zoneAccessTree));
                    } else if (callArrow.origin.isFortran() && callArrow.destination.isC()) {
                        zoneAccessTree = ILUtils.addAddressOfAtBase(ILUtils.copy(zoneAccessTree));
                    }
                    actualArgZoneTree = paramZones[formalArgZI.index-1] ;
                    actualArgZoneTree = TapList.getSetFieldLocation(actualArgZoneTree, zoneAccessTree, false) ;
                    if (actualArgZoneTree!=null && actualArgZoneTree.tail!=null) {
                        // deref this pointer to obtain the tree of its destination zones:
                        actualArgZones = ZoneInfo.listAllZonesWithNU(actualArgZoneTree.tail, false) ;
                    }
                }
                while (actualArgZones!=null) {
                    if (actualArgZones.head<firstParamIndex) {
                        result.rows[i].set(actualArgZones.head, true) ;
                    } else {
                        pointsElsewhere = true ;
                    }
                    actualArgZones = actualArgZones.tail ;
                }
            }
            // Conservative (?) approximation: if this pointer (ptrZoneNb==i), at call site,
            // may point to a non-global zone, then we are not able to find the corresponding
            // non-global zone in the callee. Consequently, in the callee, we make
            // this pointer point to its "initial destination", which we
            // will understand as an "unspecified" possible destination.
            if (pointsElsewhere) {
                result.rows[i].set(identityColumnRanks(i, calledParamsST), true);
            }                
        }
        return result ;
    }

    /** Copy the given "model" into a new BoolVector of size "finalLength", by inserting
     * a hole (filled with 0's) starting at index "startHole", and by pushing all "offsetNb" bits
     * of the model after index "startHole" to the right of this hole. */
    private BoolVector copyRowWithHole(BoolVector model, int startHole, int offsetNb, int finalLength) {
        BoolVector result = model.copy(startHole, finalLength) ;
        result.cumulOr(model, startHole, finalLength-offsetNb, offsetNb) ;
        return result ;
    }

    /**
     * Build an extended copy of the dests matrix "dests", adding new rows and columns about
     * extra dummy zones that stand for a given set of formal parameters.
     * Given the "dests" matrix just before a subroutine call, plus the array for each actual parameter
     * of its current pointer destinations as returned by pointersThroughExpression(),
     * build a copy of "dests" with as many extra rows and columns as elementary formal parameters,
     * and fill the extra rows with the corresponding given pointer destinations.
     * Also fills the given "elemParamFolders" and "ptrElemParamFolders", which will be used later,
     * when folding the afterCall dests matrix to eliminate the dummy zones about formal params.
     * This takes care of PASS-BY-VALUE/PASS-BY-REFERENCE (also PASS-BY-VALUE-RETURN): When a zone
     * passed through a parameter is actually passed-by-value i.e. only a copy is passed,
     * this zone does not appear in the corresponding "elemParamFolders" and "ptrElemParamFolders".
     *
     * @param dests               The original dests matrix, just before a subroutine call.
     * @param paramZones          The array of pointer destinations for each actual parameter.
     * @param elemParamFolders    filled upon exit with the column indices of each elementary actual param.
     * @param ptrElemParamFolders filled upon exit with the row indices of each elementary actual ptr param.
     * @return the extended copy of the dests matrix.
     */
    private BoolMatrix completeWithParams(BoolMatrix dests, TapList[] paramZones, CallArrow callArrow,
                                          TapIntList[] elemParamFolders, TapIntList[] ptrElemParamFolders,
                                          int nbRows, int nbCols) {
        BoolMatrix result = new BoolMatrix(curUnit.hasTooManyZones(), nbRows, nbCols);
        result.setIdentity();
        // first, copy original "dests" into "result":
        BoolVector origRow;
        int destsColLength = dests.nCols;
        for (int i = dests.nRows - 1; i >= 0; i--) {
            origRow = dests.getRow(i);
            if (origRow != null) {
                result.setExplicitZeroRow(i);
                result.getRow(i).cumulOr(origRow, destsColLength);
            }
        }
        ZoneInfo elemArgZoneInfo;
        TapList elemArgZones;
        TapIntList elemArgDests;
        int rowi;
        int j;
        int firstFormalArgRk, firstFormalArgRkPtr;
        Tree zoneAccessTree ;
        if (curCalledUnit.publicSymbolTable() != null) {
            firstFormalArgRk = curCalledUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.ALLKIND);
            firstFormalArgRkPtr = curCalledUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.PTRKIND);
        } else {
            //This happens if curCalledUnit is an external => null publicSymbolTable !
            firstFormalArgRk = curCalledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND);
            firstFormalArgRkPtr = curCalledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.PTRKIND);
        }
        for (int i=elemParamFolders.length-1 ; i>=0 ; --i) {
            elemArgZoneInfo = formalElemArgRkToZoneInfo(i + firstFormalArgRk, SymbolTableConstants.ALLKIND, curCalledUnit);

            if (elemArgZoneInfo == null) {
                elemParamFolders[i] = null;
            } else {
                rowi = -1;
                if (elemArgZoneInfo.ptrZoneNb != -1) {
                    rowi = offsetZoneRkFromCalleeToCaller(elemArgZoneInfo.ptrZoneNb, firstFormalArgRkPtr, nDPZ) ;
                }
                if (rowi >= 0) {
                    result.setExplicitZeroRow(rowi);
                }
                if (elemArgZoneInfo.kind()==SymbolTableConstants.PARAMETER
                    && elemArgZoneInfo.index<=paramZones.length) {
                    zoneAccessTree = elemArgZoneInfo.accessTree ;
                    // This is *almost* like ZoneInfo.crossLanguageAccessTree():
                    if (callArrow.origin.isC() && callArrow.destination.isFortran()
                        && !elemArgZoneInfo.passesByValue(callArrow.destination, TapEnv.C)) {
// This case is not well tested yet:
                        zoneAccessTree = ILUtils.addPointerAccessAtBase(ILUtils.copy(zoneAccessTree));
                    } else if (callArrow.origin.isFortran() && callArrow.destination.isC()) {
                        zoneAccessTree = ILUtils.addAddressOfAtBase(ILUtils.copy(zoneAccessTree));
                    }
                    elemArgZones = TapList.getSetFieldLocation(paramZones[elemArgZoneInfo.index - 1],
                            zoneAccessTree, false);
                    elemArgDests = ZoneInfo.listAllZonesWithNU(elemArgZones, false);
                    if (rowi >= 0) {
                        // When the parameter is passed by value and not by reference,
                        //  we set ptrElemParamFolders[] to null to inhibit param update upon exit:
                        if (elemArgZoneInfo.passesByValue(curCalledUnit, curUnit.language())
                            && elemArgZoneInfo.targetZoneOf == null) {
                            ptrElemParamFolders[rowi-nDPZ] = null;
                        } else {
                            ptrElemParamFolders[rowi-nDPZ] = mapZoneRkToElemArgPtrZoneRk(elemArgDests);
                        }
                    }
                    if (elemArgDests == null) {
                        elemArgDests = new TapIntList(curCallGraph.zoneNbOfUnknownDest, null);
                    }
                    // When the parameter is purely passed by value, only a copy of it it passed
                    // and therefore no one in the called procedure can see the zone of the actual argument:
                    if (elemArgZoneInfo.passesByValue(curCalledUnit, curUnit.language()) && elemArgZoneInfo.targetZoneOf == null) {
                        elemParamFolders[i] = null;
                    } else {
                        elemParamFolders[i] = elemArgDests;
                    }
                    if (rowi >= 0) {
                        // For each pointer elementary formal parameter, fill "its" extra row
                        // with the actual destinations of the corresponding actual parameter:
                        elemArgDests =
                            (elemArgZones==null ? null : ZoneInfo.listAllZonesWithNU(elemArgZones.tail, false)) ;
                        if (elemArgDests == null) {
                            elemArgDests = new TapIntList(curCallGraph.zoneNbOfUnknownDest, null);
                        }
                        result.rows[rowi].set(elemArgDests, true) ;
                    }
                } else {
                    elemParamFolders[i] = null;
                }
            }
        }
        return result;
    }

    /** Build the "closed" form of a given pointer destination info "dests".
     * The closed form is a destination matrix of the same shape as "dests",
     * but if a row of "dests" gives the destinations of some zone that is
     * itself the default destination of another pointer, this destination
     * info is moved to the actual destinations of this "root" pointer.
     * Example: if p is declared as "float **p", 3 zones are created for
     * p, *p, and **p. If "dests" gives destinations of *p as its j-th row Rj,
     * but at the same time dests states that p actually points to zones
     * i1 or i2 (pointer ranks pi1 and pi2), then the closed form will hold
     * (a copy of) Rj in rows pi1 and pi2
     * (possibly accumulated with other destinations), and row j will
     * not hold Rj any more, but most likely an "Identity" row.
     * @param dests The original, non-closed dests info. Not modified.
     * @return A new BoolMatrix holding the closed form of "dests".
     */
    private BoolMatrix pointerDestsClosure(BoolMatrix dests) {
        BoolMatrix closure = new BoolMatrix(curUnit.hasTooManyZones(), nDPZ, nDZ);
        closure.setIdentity();
        int rowNb = dests.nRows;
        BoolVector closureRowComputed = new BoolVector(rowNb);
        for (int rowIndex = rowNb - 1; rowIndex >= 0; rowIndex--) {
            pointerDestsClosureRec(closure, rowIndex, closureRowComputed, dests);
        }
        return closure;
    }

    /**
     * Recursive utility for pointerDestsClosure().
     * Computes one row of the closure, only if it is not computed yet.
     * If subpointers occur, this may require computing other rows beforehand.
     *
     * @param closure            The closure dests matrix which is being built.
     * @param rowIndex           The index of the row in "closure" that must be built.
     * @param closureRowComputed true for rows whose closure is already computed.
     * @param dests              The non-closed original dests matrix.
     */
    private void pointerDestsClosureRec(BoolMatrix closure, int rowIndex,
                                        BoolVector closureRowComputed, BoolMatrix dests) {
        if (!closureRowComputed.get(rowIndex)) {
            closureRowComputed.set(rowIndex, true);
            ZoneInfo pointerInfo = curSymbolTable.declaredZoneInfo(rowIndex, SymbolTableConstants.PTRKIND);
            if (pointerInfo != null) {
                ZoneInfo subPointerInfo = pointerInfo.targetZoneOf;
                BoolVector closedRow = null;
                if (subPointerInfo == null) {
                    if (dests.getRow(rowIndex) != null) {
                        closedRow = dests.getRow(rowIndex).copy();
                    }
                } else {
                    int subIndex = subPointerInfo.kindZoneNb(SymbolTableConstants.PTRKIND);
                    pointerDestsClosureRec(closure, subIndex, closureRowComputed, dests);
                    closedRow = new BoolVector(curUnit.hasTooManyZones(), dests.nCols);
                    TapIntList aliases = aliasesOfPointedZoneInfo(pointerInfo, closure, nDZ);
                    int rowi;
                    BoolVector newRow;
                    while (aliases != null) {
                        rowi = zoneRkToKindZoneRk(aliases.head, SymbolTableConstants.PTRKIND);
                        if (rowi >= 0) {
                            newRow = dests.getRow(rowi);
                            if (newRow != null) {
                                closedRow.cumulOr(newRow);
                            } else {
                                closedRow.set(identityColumnRanks(rowi, curSymbolTable), true);
                            }
                        }
                        aliases = aliases.tail;
                    }
                }
                closure.setRow(rowIndex, closedRow);
            }
        }
    }

    /**
     * Composition of two successive pointerDests infos.
     * This method returns the dests info of code part (A;B), given the
     * dests infos "preDests" of code part A, and "newDests" of code part B.
     * This method can be used when part B is a procedure call, in which case the dests
     * matrices must have been extended with extra zones for the formal parameters.
     * <p>
     * Two phases: <br>
     * (1) rearrange and recombine the rows of "newDests", using the
     * present destinations given in "preDestsClosed" for the subpointers, and<br>
     * (2) compose each recombined row of newDests with the destinations
     * at the end of part A, i.e. "preDests" and "preDestsClosed".
     * <p>
     * "preDests" is used for any pointer touched by A that B doesn't
     * touch because B doesn't overwrite it or because it is made
     * unreachable in B due to A cutting off every way to access it :
     * such a pointer must retain its destination after A.
     * Takes care of preserving implicit-Identity.
     * <p>
     * For parameters (if applicable) OR-folds a copy of each param row into the rows
     * of each pointer zone which is being aliased by the corresponding actual parameter,
     * and similarly for columns, OR-folds a copy of each param column into the columns
     * of each zone which is being aliased by the corresponding actual parameter.
     * Also takes care of PASS-BY-VALUE vs PASS-BY-VALUE-RETURN: When a parameter is
     * purely PASS-BY-VALUE, and therefore not modifiable, its cell in "ptrElemParamFolders" is empty.
     *
     * @param preDests            The dests info of A.
     * @param preDestsClosed      The closed dests info of A.
     * @param newDests            The dests info of B.
     * @param nbRows              The number of rows of preDestsClosed, newDests, and of the result
     * @param nbCols              The number of columns of preDestsClosed, newDests, and of the result
     * @param ptrElemParamFolders For each pointer elementary formal parameter, the list of the
     *                            row indices of the zones that it aliases through the corresponding actual parameter.
     * @param elemParamFolders    For each elementary formal parameter, the list of the
     *                            column indices of the zones that it aliases through the corresponding actual parameter.
     * @param paramTotals         Total actual parameters. Used to choose override vs OR-accumulation.
     * @return The new, composed dests matrix.
     */
    private BoolMatrix composePointerDests(BoolMatrix preDests,
                                           BoolMatrix preDestsClosed, BoolMatrix newDests, int nbRows, int nbCols,
                                           TapIntList[] ptrElemParamFolders, TapIntList[] elemParamFolders, boolean[] paramTotals) {

//DEBUGGING:
// TapEnv.printlnOnTrace("COMPOSEPOINTERDESTS:") ;
// TapEnv.dumpBoolMatrixOnTrace(preDests) ;
// TapEnv.printlnOnTrace("CLOSED AS:") ;
// TapEnv.dumpBoolMatrixOnTrace(preDestsClosed) ;
// TapEnv.printlnOnTrace("WITH FOLLOWING:") ;
// TapEnv.dumpBoolMatrixOnTrace(newDests) ;

        BoolMatrix result = new BoolMatrix(curUnit.hasTooManyZones(), nbRows, nbCols);
        result.setIdentity();
        int rowNb = newDests.nRows;
        BoolVector resultInitialized = new BoolVector(rowNb);
        BoolVector resultHasIdentity = new BoolVector(rowNb);
        resultHasIdentity.setTrue();
        BoolVector newRow;
        ZoneInfo pointerInfo;
        TapIntList aliases;
        int j;
        int firstFormalArgRk = -1 ;
        if (nbRows>nDPZ) { //i.e. when on a function call (next j may be >= nDPZ) :
            if (curCalledUnit.isStandard() || curCalledUnit.isInterface()) {
                firstFormalArgRk = curCalledUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.ALLKIND);
            } else {
                //This happens if curCalledUnit is an external => null publicSymbolTable !
                firstFormalArgRk = curCalledUnit.externalSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND);
            }
        }
        // First phase: rearrange and recombine the rows of "newDests" into "result",
        //  watching the subpointers' aliases given by preDestsClosed and
        //  by ptrElemParamFolders for formal parameters:
        for (j=nbRows-1 ; j>=0 ; --j) {
                newRow = newDests.getRow(j);
                if (newRow != null) {
                    if (j<nDPZ) {
                        pointerInfo = curSymbolTable.declaredZoneInfo(j, SymbolTableConstants.PTRKIND) ;
                    } else {
                        SymbolTable calledST = curCalledUnit.publicSymbolTable() ;
                        pointerInfo = calledST.localDeclaredZoneInfo(
                                             j - nDPZ + calledST.firstDeclaredZone(SymbolTableConstants.PTRKIND),
                                             SymbolTableConstants.PTRKIND);
                    }
                    if (pointerInfo != null) {
                        if (j>=nDPZ) {
                            result.setRow(j, newRow.copy(newDests.nCols, nbCols));
                            if (j-nDPZ >= ptrElemParamFolders.length
                                || pointerInfo.index <= 0 || pointerInfo.index > paramTotals.length) {
                                // protect vs missing or extra actual parameters. Also discard "result" parameters
                                aliases = null;
                            } else {
                                aliases = ptrElemParamFolders[j-nDPZ];
                            }
                        } else {
                            aliases = getRowsAliasedThroughSubpointer(pointerInfo, j, preDestsClosed, nbCols);
                        }
                        if (aliases != null) {
                            TapIntList identityColumns = ZoneInfo.listAllZonesWithNU(pointerInfo.targetZonesTree, true) ;
                            if (j>=nDPZ) {
                                identityColumns = mapOffsetZoneRkFromCalleeToCaller(identityColumns, firstFormalArgRk, nDZ) ;
                            }
                            cumulOrDestsRowAtTargetIndices(newRow, identityColumns,
                                    (j<nDPZ || paramTotals[pointerInfo.index-1]),
                                    aliases, result, resultInitialized, resultHasIdentity, null);
                        }
                    }
                }
        }

//DEBUGGING:
// TapEnv.printlnOnTrace("RESULT AFTER PHASE 1:") ;
// TapEnv.dumpBoolMatrixOnTrace(result) ;

        // Second phase: compose the rearranged newDests matrix with the preDestsClosed (on the right).
        int i;
        int rk;
        BoolVector preDestsRow;
        BoolVector resultRow;
        for (j = rowNb - 1; j >= 0; j--) {
            newRow = result.getRow(j);
            if (newRow == null) {
                // Only use of "preDests": when the intermediate newRow is Implicit-Identity,
                // e.g. the current pointer is inaccessible during the "new" portion, then it is
                // unmodified and points to the same place as at the end of the "pre" portion.
                preDestsRow = (j<preDests.nRows ? preDests.getRow(j) : preDestsClosed.getRow(j));
                if (preDestsRow == null) {
                    // If Implicit-Identity composed with Implicit-Identity, remain Implicit-Identity !
                    resultRow = null;
                } else {
                    resultRow = preDestsRow.copy(preDests.nCols, nbCols);
                }
            } else {
                resultRow = composeDestsRow(newRow, preDestsClosed, nbCols, elemParamFolders) ;
                if (j<nDPZ && resultInitialized.get(j) && resultHasIdentity.get(j)) {
                    preDestsRow = preDestsClosed.getRow(j);
                    if (preDestsRow != null) {
                        resultRow.cumulOr(preDestsRow);
                    } else {
                        resultRow.set(identityColumnRanks(j, curSymbolTable), true);
                    }
                }
            }
            result.setRow(j, resultRow);
        }

//DEBUGGING:
// TapEnv.printlnOnTrace("RESULT AFTER PHASE 2:") ;
// TapEnv.dumpBoolMatrixOnTrace(result) ;

        return result;
    }

    private BoolVector composeDestsRow(BoolVector row, BoolMatrix preDestsClosed, int nbCols, TapIntList[] elemParamFolders) {
        BoolVector resultRow = new BoolVector(curUnit.hasTooManyZones(), nbCols);
        for (int i=nbCols-1 ; i>=0 ; --i) {
            if (row.get(i)) {
                if (i>=nDZ) {
                    // Fold the extra columns about formal parameters in order to remove them:
                    // for each destination to a formal parameter, insert destinations
                    // to each zone which is being aliased by the corresponding actual parameter.
                    row.set(i, false);
                    row.set(elemParamFolders[i-nDZ], true);
                } else {
                    ZoneInfo destinationInfo = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                    if (destinationInfo == null || destinationInfo.targetZoneOf == null) {
                        // NULL or Undef pointer destination or fixed address (not a subpointer):
// TapEnv.printlnOnTrace("       just keep "+i+":"+destinationInfo) ;
                        resultRow.set(i, true);
                    } else if (i==curCallGraph.zoneNbOfNullDest || i==curCallGraph.zoneNbOfUnknownDest) {
// TapEnv.printlnOnTrace("       keep  NU  "+i) ;
                        resultRow.set(i, true);
                    } else {
                        TapIntList destDests = aliasesOfPointedZoneInfo(destinationInfo, preDestsClosed, nbCols);
// TapEnv.printlnOnTrace("      aliases of "+i+":"+destinationInfo+" ARE "+destDests) ;
                        while (destDests!=null) {
                            // Don't set NULL as a destination, because we are looking for aliases
                            // of destinationInfo, and those aliasing paths can't go through NULL.
                            if (destDests.head != curCallGraph.zoneNbOfNullDest) {
                                resultRow.set(destDests.head, true);
                            }
                            destDests = destDests.tail ;
                        }
                    }
                }
            }
        }
        return resultRow ;
    }

    /**
     * OR-accumulates a given dests row "newRow" from a dests matrix  at each of the
     * given target row indices "targetRowIndices" in another dests matrix "target".
     * This OR-accumulation is special because
     * it is a "OR" except for Identity. For Identity only, it is an "AND" !!!
     * RATIONALE: suppose pointer p is at the same time passed as a global "p"
     * and as a parameter "q", or can be accessed by 2 different aliases.
     * If the next piece of code makes p (resp. q) point to destination D1 or identity,
     * and makes q (resp. p) point to destination D2 but not identity,
     * then in the end p may point to D1 or D2, but not to its previous destinations!
     * Also keep in mind that Identity for dests matrices is nontrivial.
     * This method will provide indication about which rows must be completed with
     * Identity in the end. To this end, it uses and updates three BoolVectors indexed
     * by row indices, to remember which rows are already initialized with a dests,
     * which rows have received Identity, and which rows' Identity may have changed.
     *
     * @param newRow           The given dests row to be copied and merged.
     * @param identityColumns  The non-zero columns of (explicit) identity for newRow.
     * @param total            False iff newRow corresponds to an actual parameter which is not total.
     * @param targetRowIndices The dests matrix row indices that will receive a copy of newRow.
     * @param target           The receiving dests matrix.
     * @param initializedRows  For each row of "target", is its present dests row non-empty ?
     *                         Must be correctly initialized before accumulation starts
     * @param withIdRows       For each row of "target", do all the cumulated newRow's imply Identity ?
     *                         Must be initialized to True before accumulation starts
     * @param chIdRows         For each row of "target", does accumulation possibly change Identity ?
     *                         May be null when accumulation starts on null-empty target matrix rows.
     *                         otherwise must be initialized to False before accumulation starts.
     */
    private void cumulOrDestsRowAtTargetIndices(BoolVector newRow, TapIntList identityColumns,
                                                boolean total, TapIntList targetRowIndices, BoolMatrix target,
                                                BoolVector initializedRows, BoolVector withIdRows, BoolVector chIdRows) {
        boolean newRowHasId = newRow.intersects(identityColumns);
        if (newRowHasId) {
            newRow = newRow.copy();
            newRow.set(identityColumns, false);
        }
        //If the accumulated row comes from a non-total parameter or is aliased to many
        // target rows, then this accumulated row must be considered WITH Identity:
        if (!total || targetRowIndices.tail != null) {
            newRowHasId = true;
        }
        int targetRowIndex;
        BoolVector targetRow;
        while (targetRowIndices != null) {
            targetRowIndex = targetRowIndices.head;
            targetRow = target.getRow(targetRowIndex);
            if (targetRow == null || !initializedRows.get(targetRowIndex)) {
                // case of a target row containing Identity or Nothing:
                target.setRow(targetRowIndex, newRow.copy());
                if (!newRowHasId) {
                    withIdRows.set(targetRowIndex, false);
                } else {
                    if (chIdRows != null) {
                        chIdRows.set(targetRowIndex, true);
                    }
                }
                initializedRows.set(targetRowIndex, true);
            } else {
                // case of a target row already containing some explicit dests:
                targetRow.cumulOr(newRow);
                if (!newRowHasId) {
                    withIdRows.set(targetRowIndex, false);
                    if (chIdRows != null) {
                        chIdRows.set(targetRowIndex, true);
                    }
                }
            }
            targetRowIndices = targetRowIndices.tail;
        }
    }

    /**
     * @param pointerInfo          The ZoneInfo of the pointer considered.
     * @param rowIndex             The original row index of "pointerInfo",
     *                             returned when the address of "pointerInfo" has not changed from "t1" to "t2".
     * @param preDestsClosed       The effect on pointer dests from "t1" to "t2".
     * @param rowLength            The length of a row in dests (i.e. the number of columns).
     * @return The row indices at time "t1" aliased by "pointerInfo" at time "t2".
     */
    private TapIntList getRowsAliasedThroughSubpointer(ZoneInfo pointerInfo, int rowIndex,
                                                       BoolMatrix preDestsClosed, int rowLength) {
        //Return rapidly in the frequent case of no subpointer:
        if (pointerInfo.targetZoneOf == null) {
            return new TapIntList(rowIndex, null);
        }
        TapIntList pointedDestsList =
            aliasesOfPointedZoneInfo(pointerInfo, preDestsClosed, rowLength);
        //See if the address of "pointerInfo" has not been changed through preDestsClosed
        // It may have been changed if the subpointers of pointerInfo have been redefined.
        // We detect it unchanged when the current aliases "pointedDestsList" of pointerInfo
        // is a singleton containing the (extended declared) zone of pointerInfo.
        if (pointedDestsList != null
            && pointedDestsList.tail == null
            && pointedDestsList.head == pointerInfo.zoneNb) {
            //If the address of "pointerInfo" has not changed, then simply keep this row index...
            return new TapIntList(rowIndex, null);
        } else {
            //...else return the pointer's destinations at time "t2", as row indices
            // in the pointer dests matrices.
            return mapZoneRkToElemArgPtrZoneRk(pointedDestsList);
        }
    }

    /**
     * Given a ZoneInfo "pointerInfo" that is a zone accessed through a pointer deref,
     * return the TapIntList of the extended declared zone ranks that "pointerInfo" aliases to,
     * through the given pointer destinations info "destsClosed" (which is closed).
     *
     * @param pointerInfo       The ZoneInfo of a pointer that may be aliased because it has a subpointer.
     * @param destsClosed       The pointer dests info that may have changed the subpointer.
     * @param rowLength         The length of a row in destsClosed (i.e. the number of columns).
     * @return The list of extended declared zone ranks that pointerInfo is aliased to.
     */
    private TapIntList aliasesOfPointedZoneInfo(ZoneInfo pointerInfo, BoolMatrix destsClosed,
                                                int rowLength) {
        ZoneInfo rootPointerInfo = pointerInfo.targetZoneOf;
        BoolVector rootPointerDestsRow =
                destsClosed.getRow(rootPointerInfo.kindZoneNb(SymbolTableConstants.PTRKIND));
//DEBUGGING:
// System.out.print(rootPointerInfo.kindZoneNb(SymbolTableConstants.PTRKIND)+">"+rootPointerDestsRow+" ") ;
        if (rootPointerDestsRow == null) {   // implicit identity case:
            int indexAsDeclared = pointerInfo.zoneNb;
            return new TapIntList(indexAsDeclared, null);
        } else {
            TapList pointedDestsTree =
                curSymbolTable.subTreeOfPointedZones(rootPointerDestsRow, rowLength,
                            rootPointerInfo.pointerDestType(),
                            ILUtils.pathFromPointerAccess(pointerInfo.accessTree));
            TapIntList result = ZoneInfo.listAllZones(pointedDestsTree, true);
            // For a better propagation of NULL and Undef:
            if (rootPointerDestsRow.get(1)) result = new TapIntList(1, result) ;
            if (rootPointerDestsRow.get(0)) result = new TapIntList(0, result) ;
            return result ;
        }
    }

    /**
     * Builds a pointer dests matrix of the correct shape for curBlock,
     * initialized with implicit identity, i.e. each row is null.
     *
     * @return The new pointer dests matrix.
     */
    private BoolMatrix identityPointerMatrix() {
        BoolMatrix result = zeroPointerMatrix();
        result.setIdentity();
        return result;
    }

    /**
     * Builds an empty pointer dests matrix of the correct shape for curBlock.
     *
     * @return The new empty pointer dests matrix.
     */
    private BoolMatrix zeroPointerMatrix() {
        return new BoolMatrix(curUnit.hasTooManyZones(), nDPZ, nDZ);
    }

    /**
     * Sets to explicit-zero all the rows in dests that correspond to one
     * of the given declared zones "declZones".
     *
     * @param dests     The dests matrix in which rows will be set to explicit-zero
     * @param declZones The declared zones among which those which are pointers will
     *                  cause an explicit-zero row in dests.
     */
    private void mapExplicitZeroDeclAllkZoneRk(BoolMatrix dests, TapIntList declZones) {
        int rowIndex;
        ZoneInfo rowZoneInfo;
        while (declZones != null) {
            rowZoneInfo = curSymbolTable.declaredZoneInfo(declZones.head, SymbolTableConstants.ALLKIND);
            if (rowZoneInfo != null) {
                rowIndex = rowZoneInfo.kindZoneNb(SymbolTableConstants.PTRKIND);
                if (rowIndex >= 0) {
                    dests.setExplicitZeroRow(rowIndex);
                }
            }
            declZones = declZones.tail;
        }
    }

    /**
     * @param rowIndex The given row index.
     * @return the column ranks that would represent the "Identity" dests
     * info for a given row index "rowIndex' inside a dests matrix, and
     * in the context of the given SymbolTable.
     */
    private static TapIntList identityColumnRanks(int rowIndex, SymbolTable symbolTable) {
        ZoneInfo pointerInfo = symbolTable.declaredZoneInfo(rowIndex, SymbolTableConstants.PTRKIND);
        return (pointerInfo==null ? null : ZoneInfo.listAllZones(pointerInfo.targetZonesTree, true)) ;
    }

    /**
     * @param zones The given list of extended declared zone ranks.
     * @return The sub list of the extended declared zone ranks that designate pointers.
     */
    private TapIntList getPointerZones(TapIntList zones) {
        TapIntList result = null;
        while (zones != null) {
            if (zoneIsPointer(zones.head)) {
                result = new TapIntList(zones.head, result);
            }
            zones = zones.tail;
        }
        return result;
    }

// All about detection of dangling pointers.
//==========================================

    /**
     * Detects dangling pointers travelling across "arrow".
     * When a pointer points ONLY to destinations that fall out of scope
     * across "arrow", creates and returns a copy of the given "dests" matrix
     * in which this pointer points to "Undef".
     *
     * @param dests The dests matrix that travels across "arrow".
     * @param arrow The flow arrow across which "dests" is travelling.
     * @return Either the input dests if no dangling detected, or a new modified dests matrix.
     */
    private BoolMatrix removeDanglingPointers(BoolMatrix dests, FGArrow arrow) {
        BoolMatrix destsWithoutDangling = null;
        Block origBlock = arrow.origin;
        Block destBlock = arrow.destination;
        SymbolTable origSymbolTable = origBlock.symbolTable;
        SymbolTable commonSymbolTable = destBlock.symbolTable.getCommonRoot(origSymbolTable);
        if (dests != null && commonSymbolTable != origSymbolTable &&
                commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) < origSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) {
            // In this case there are variables that fall out of scope.
            int cutRow;
            int cutCol;
            int irow;
            int icol;
            boolean mayNotDangle;
            String dangleTo;
            ZoneInfo zoneInfo;
            Instruction headInstr = destBlock.headInstr();
            Tree attachTree = headInstr == null ? null : headInstr.tree;
            int nPZ = origSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND) ;
            int nZ = origSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
            cutRow = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
            cutCol = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            for (irow = cutRow - 1; irow >= 0; irow--) {
                mayNotDangle = false;
                for (icol = cutCol - 1; icol >= 0; icol--) {
                    if (icol != -1 && dests.get(irow, icol)) {
                        mayNotDangle = true;
                    }
                }
                dangleTo = "";
                for (icol=nZ-1 ; icol>=cutCol ; --icol) {
                    if (dests.get(irow, icol)) {
                        if (destsWithoutDangling == null) {
                            destsWithoutDangling = dests.copy();
                        }
                        destsWithoutDangling.set(irow, curCallGraph.zoneNbOfUnknownDest, true);
                        if (terminatingUnit && (!VERY_BIG_CG || terminatingCallGraph)) {
                            zoneInfo = origSymbolTable.declaredZoneInfo(icol, SymbolTableConstants.ALLKIND);
                            if (zoneInfo != null) {
                                if (!dangleTo.isEmpty()) {
                                    dangleTo = " or " + dangleTo;
                                }
                                dangleTo = zoneInfo.accessTreePrint(curUnit.language()) + dangleTo;
                            }
                        }
                    }
                }
                if (terminatingUnit && (!VERY_BIG_CG || terminatingCallGraph) && !dangleTo.isEmpty()) {
                    zoneInfo = curSymbolTable.declaredZoneInfo(irow, SymbolTableConstants.PTRKIND);
                    if (mayNotDangle) {
                        TapEnv.fileWarning(30, attachTree, "(DF12) Pointer " + zoneInfo.accessTreePrint(curUnit.language()) + " may be dangling, pointing to " + dangleTo);
                    } else {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, attachTree, "(DF12) Pointer " + zoneInfo.accessTreePrint(curUnit.language()) + " is dangling, pointing to " + dangleTo);
                    }
                }
            }
        }
        return destsWithoutDangling != null ? destsWithoutDangling : dests;
    }

// All about building and attaching the comments on pointer destinations into the code.
//=====================================================================================

    /**
     * Builds the list of all changes in pointer dests from the end of the incoming
     * flow arrows to the beginning of the given block. This list will be used to
     * place comments about pointers into the code, using buildCommentsForPointerDests().
     *
     * @param block Pointer changes are listed at the beginning of this Block.
     * @return The list of pointer destination changes.
     */
    private TapList<TapPair<TapIntList, BoolVector>> findDestsChangesAfterFlowMerge(Block block) {
        BoolMatrix destsAfter = block.pointerInfosIn;
        if (destsAfter == null) {
            return null;
        }
        BoolMatrix destsBefore;
        BoolMatrix destsBeforeCycle;
        int nColsAfter = destsAfter.nCols;
        int nColsBefore;
        int nCols;
        int j;
        int commonSize;
        BoolVector changedRows = new BoolVector(destsAfter.nRows);
        BoolVector rowBefore;
        BoolVector rowAfter;
        Block origBlock;
        SymbolTable commonSymbolTable;
        TapList<FGArrow> arrows = block.backFlow();
        while (arrows != null) {
            origBlock = arrows.head.origin;
            destsBefore = afterDests.retrieve(origBlock);
            if (destsBefore != null) {
                SymbolTable origSymbolTable = origBlock.symbolTable;
                destsBeforeCycle = afterDestsCycle.retrieve(origBlock);
                // we may modify "destsBefore" in place, because it will be erased immediately after:
                if (destsBeforeCycle != null) {
                    explicitIdentitiesBeforeCumulOr(destsBefore, destsBeforeCycle, curSymbolTable);
                    destsBefore.cumulOr(destsBeforeCycle, false, -1);
                }
                nColsBefore = destsBefore.nCols;
                nCols = Math.min(nColsBefore, nColsAfter);
                commonSymbolTable = curSymbolTable.getCommonRoot(origSymbolTable);
                commonSize = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
                for (j = destsAfter.nRows - 1; j >= commonSize; j--) {
                    changedRows.set(j, true);
                }
                for (j = commonSize - 1; j >= 0; j--) {
                    rowBefore = destsBefore.getRow(j);
                    rowAfter = destsAfter.getRow(j);

                    if (rowBefore!=null && isExplicitPointerIdentity(rowBefore, j, origSymbolTable)) rowBefore = null ;
                    if (rowAfter!=null && isExplicitPointerIdentity(rowAfter, j, curSymbolTable)) rowAfter = null ;
                    if (origBlock instanceof EntryBlock && rowAfter!=null) {
                        // When just below EntryBlock, and the pointer destinations are
                        // not a trivial "Identity", force printing pointer destinations:
                        rowBefore = null ;
                    }

                    if (rowAfter!=null
                        && (rowBefore==null
                            || !rowAfter.equals(rowBefore, nCols))) {
                        changedRows.set(j, true);
                    }
                }
            }
            arrows = arrows.tail;
        }
        TapList<TapPair<TapIntList, BoolVector>> destsChanges = null;
        for (j = destsAfter.nRows - 1; j >= 0; j--) {
            if (changedRows.get(j)) {
                rowAfter = destsAfter.getRow(j);

                if (rowAfter==null) { // If implicit-identity, make it explicit for the coming buildCommentsForPointerDests()
                    rowAfter = new BoolVector(destsAfter.nCols) ;
                    rowAfter.set(identityColumnRanks(j, curSymbolTable), true);
                }

                destsChanges =
                    new TapList<>(new TapPair<>(new TapIntList(j, null), rowAfter),
                                  destsChanges);
            }
        }
        return destsChanges;
    }

    private static boolean isExplicitPointerIdentity(BoolVector row, int i, SymbolTable symbolTable) {
        int maxi = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector mask = new BoolVector(false, maxi) ;
        mask.set(identityColumnRanks(i, symbolTable), true) ;
        return row.minus(mask).isFalse(maxi) ;
    }

    /** Turn all rows that are an "explicit" pointer-Identity line to an implicit-Identity (i.e. null) */
    private void wipeIdentityRows(BoolMatrix dests, SymbolTable symbolTable) {
        if (symbolTable!=null) {
            for (int i=dests.nRows-1 ; i>=0 ; --i) {
                if (dests.rows[i]!=null && isExplicitPointerIdentity(dests.rows[i], i, symbolTable)) {
                    dests.rows[i] = null ;
                }
            }
        }
    }

    /**
     * This must be called before calling cumulMatrix.cumulOr(orMatrix, false, -1),
     * because the default manner used in BoolMatrix to explicit implicit-identity rows
     * is wrong for pointer dests matrices, so it must be done here beforehand.
     * @param symbolTable the SymbolTable of the zones that the given Matrices speak of.
     */
    private static void explicitIdentitiesBeforeCumulOr(BoolMatrix cumulMatrix, BoolMatrix orMatrix, SymbolTable symbolTable) {
        if (!orMatrix.isImplicitZero()) {
            BoolVector cumulRow;
            int numi = Math.min(cumulMatrix.nRows, orMatrix.nRows);
            boolean cumulWasImplicitZero = cumulMatrix.isImplicitZero();
            if (cumulWasImplicitZero) {
                cumulMatrix.setExplicitZero();
            }
            for (int i = numi - 1; i >= 0; --i) {
                cumulRow = cumulMatrix.rows[i];
                if ((cumulRow == null && !cumulWasImplicitZero && orMatrix.rows[i] != null)
                    ||
                    (cumulRow != null && orMatrix.rows[i] == null)) {
                    // if either the cumul or the new row is implicit-identity, but not both!
                    // then accumulate identity into the cumul row.
                    if (cumulRow == null) {
                        cumulRow = cumulMatrix.rows[i] =
                                new BoolVector(false, cumulMatrix.nCols);
                    }
                    cumulRow.set(identityColumnRanks(i, symbolTable), true);
                }
            }
        }
    }

    /** Collects (for later use in DataFlowAnalyzer) the current destinations of each global pointer
     * at the current location (which is a call site). These destinations are stored
     * in correspondence with the zone that represents the "initial" destination of the global pointer.
     * Example: p is a pointer to a struct with 2 fields, and its "initial" destination is (([4]) ([5])).
     * At the call site, due to code executed before reaching the call, p actually points to (([12]) ([7 14])).
     * The info collected is: 4 maps to [12] and 5 maps to [7 14].
     * One such set of info is built for each "kind" (int, real, ptr...) plus at index 0 for kind ALLKIND.
     * @param destsAtCallSite the matrix of pointer destinations at the current call site.
     * @param destsInCallee the matrix of pointer destinations at the callee entry point (should be an "Identity").
     * @param globalPtrsNb the rank (plus 1) of the last pointer zone which passes as a global to this call.
     */
    private void collectActualInitialDests(BoolMatrix destsAtCallSite, Unit calledUnit,
                                           TapList<TapPair<Integer, TapIntList>>[] actualInitials4) {
        BoolVector destRow ;
        ZoneInfo pZI ;
        TapList zonesTreeAtCallSite, zonesTreeInCallee ;
        BoolMatrix destsInCallee = calledUnit.entryBlock.pointerInfosIn ;
        int globalPtrsNb = calledUnit.globalZonesNumber(SymbolTableConstants.PTRKIND) ;
        for (int j=globalPtrsNb-1 ; j>=0 ; --j) {
            pZI = curSymbolTable.declaredZoneInfo(j, SymbolTableConstants.PTRKIND);
            //Always start caller/callee correspondence from the "root" pointers, not from their initial dests:
            if (pZI.targetZoneOf==null) {
                zonesTreeAtCallSite = new TapList(new TapIntList(pZI.zoneNb, null), null) ;
                includePointedElementsInTree(zonesTreeAtCallSite, null, destsAtCallSite, true, false, true) ;
                zonesTreeInCallee = new TapList(new TapIntList(pZI.zoneNb, null), null) ;
                includePointedElementsInTree(zonesTreeInCallee, null, destsInCallee, true, calledUnit.publicSymbolTable(), null, false, true) ;
                collectActualInitialRec(zonesTreeInCallee.tail, zonesTreeAtCallSite.tail,
                                        actualInitials4, new TapList<>(null, null)) ;
            }
        }
//         for (int j=globalPtrsNb-1 ; j>=0 ; --j) {
//             destRow = destsAtCallSite.rows[j] ;
//             if (destRow!=null) {
//                 pZI = curSymbolTable.declaredZoneInfo(j, SymbolTableConstants.PTRKIND);
//                 zonesTreeAtCallSite = new TapList(new TapIntList(pZI.zoneNb, null), null) ;
//                 includePointedElementsInTree(zonesTreeAtCallSite, null, destsAtCallSite, false, false, true) ;
//                 collectActualInitialRec(pZI.targetZonesTree, zonesTreeAtCallSite.tail,
//                                         actualInitials4, new TapList<>(null, null)) ;
//             }
//         }
    }
    private void collectActualInitialRec(TapList initialDestinations, TapList currentDestinations,
                                         TapList<TapPair<Integer, TapIntList>>[] actualInitials4,
                                         TapList<TapList> dejaVu) {
        while (initialDestinations!=null && currentDestinations!=null) {
          if (TapList.contains(dejaVu.tail, initialDestinations)) {
              initialDestinations = null ;
          } else {
            dejaVu.placdl(initialDestinations);
            if (initialDestinations.head instanceof TapList) {
                if (currentDestinations.head instanceof TapList) {
                    collectActualInitialRec((TapList)initialDestinations.head, (TapList)currentDestinations.head, actualInitials4, dejaVu) ;
                } else {
                    // Recovery case where structure of currentDestinations doesn't match that of initialDestinations.
                    // May happen when currentDestinations is NULL or Undef.
                    collectActualInitialRec((TapList)initialDestinations.head, currentDestinations, actualInitials4, dejaVu) ;
                }
            } else {
                TapIntList destList ;
                if (currentDestinations.head instanceof TapIntList) {
                    destList = (TapIntList)currentDestinations.head ;
                } else {
                    destList = ZoneInfo.listAllZones((TapList)currentDestinations.head, false) ;
                }
                if (!TapIntList.equalLists((TapIntList)initialDestinations.head, destList)) {
                    collectActualInitialLeaf((TapIntList)initialDestinations.head, destList, actualInitials4) ;
                }
            }
            initialDestinations = initialDestinations.tail ;
            currentDestinations = currentDestinations.tail ;
          }
        }
    }
    private void collectActualInitialLeaf(TapIntList initials, TapIntList currents,
                                          TapList<TapPair<Integer, TapIntList>>[] actualInitials4) {
        if (initials!=null) {
            int initialz = initials.head ;
            ZoneInfo initialZoneInfo = curSymbolTable.declaredZoneInfo(initialz, SymbolTableConstants.ALLKIND);
            TapList<ZoneInfo> currentZoneInfos = null, inCurrentZoneInfos ;
            int curz ;
            while (currents!=null) {
                // Discard current destinations in the root dummy zones (NULL, Undef, IO, channels):
                if (currents.head >= curCallGraph.numberOfDummyRootZones) {
                    currentZoneInfos =
                        new TapList<>(curSymbolTable.declaredZoneInfo(currents.head, SymbolTableConstants.ALLKIND),
                                      currentZoneInfos) ;
                }
                currents = currents.tail ;
            }
            for (int kind=0 ; kind<4 ; ++kind) {
                TapList<TapPair<Integer, TapIntList>> hdActualInitials =
                    new TapList<>(null, actualInitials4[kind]) ;
                TapList<TapPair<Integer, TapIntList>> inActualInitials = hdActualInitials ;
                initialz = initialZoneInfo.kindZoneNb(kind) ;
                if (initialz!=-1) {
                    TapPair<Integer, TapIntList> foundCell = null ;
                    while (foundCell==null && inActualInitials.tail!=null && inActualInitials.tail.head.first<=initialz) {
                        if (inActualInitials.tail.head.first==initialz) foundCell = inActualInitials.tail.head ;
                        inActualInitials = inActualInitials.tail;
                    }
                    if (foundCell==null) {
                        foundCell = new TapPair<>(initialz, null);
                        inActualInitials.tail = new TapList<>(foundCell, inActualInitials.tail) ;
                    }
                    currents = null ;
                    inCurrentZoneInfos = currentZoneInfos ;
                    while (inCurrentZoneInfos!=null) {
                        curz = inCurrentZoneInfos.head.kindZoneNb(kind) ;
                        if (curz!=-1)
                            currents = new TapIntList(curz, currents) ;
                        inCurrentZoneInfos = inCurrentZoneInfos.tail ;
                    }
                    foundCell.second = TapIntList.quickUnion(foundCell.second, currents) ;
                    actualInitials4[kind] = hdActualInitials.tail ;
                }
            }
        }
    }

    /** Given a data flow info "infoVector" where some info concerns zones that
     * are indeed "initial destinations" of global pointers at the entry into a
     * called procedure, changes the "infoVector" so that it contains the same
     * info, but based on the actual destinations of these global pointers at
     * the call site "callTree" in the calling subroutine. This uses the
     * "pointersActualInitialDests" annotation that was precomputed
     * for this call site at the end of pointer analysis. */
    public static void replaceInitialsByActuals(BoolVector infoVector, Tree callTree, int whichKind, int globNb) {
        TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
            callTree.getAnnotation("pointersActualInitialDests") ;
        TapList<TapPair<Integer, TapIntList>> actualInitials =
            (actualInitials4==null ? null : actualInitials4[whichKind]) ;
        BoolVector replacements = new BoolVector(globNb) ;
        while (actualInitials!=null) {
            if (infoVector.get(actualInitials.head.first)) {
                infoVector.set(actualInitials.head.first, false) ;
                replacements.set(actualInitials.head.second, true) ;
            }
            actualInitials = actualInitials.tail ;
        }
        infoVector.cumulOr(replacements, globNb) ;
    }

    // Similar to replaceInitialsByActuals(BoolVector, ...), but now on a BoolMatrix.
    public static void replaceInitialsByActuals(BoolMatrix infoMatrix, Tree callTree, int whichKind) {
        TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
            callTree.getAnnotation("pointersActualInitialDests") ;
        TapList<TapPair<Integer, TapIntList>> actualInitials =
            (actualInitials4==null ? null : actualInitials4[whichKind]) ;
        while (actualInitials!=null) {
            BoolVector initialDestInfo = infoMatrix.rows[actualInitials.head.first] ;
            if (initialDestInfo!=null) {
                // Not sure what to do about new contents of infoMatrix.rows[actualInitials.head.first] ?
                infoMatrix.overwriteDeps(actualInitials.head.second, initialDestInfo, false) ;
            }
            actualInitials = actualInitials.tail ;
        }
    }

    // After setting to true the data-flow info of some initial pointer destination,
    // we probably can NOT reset to false the info of its actual destination because this destination
    // may be accessed directly by the callee and for it, the data-flow info is still true.
    /** Reciprocal of replaceInitialsByActuals().
     * Given a data flow info "infoVector" which exists in some (calling) subroutine just before
     * calling some (called) subroutine, changes the "infoVector" so that it contains the same
     * info, but based on the global pointers' initial destination zones at the entry into
     * the called subroutine.  This uses the "pointersActualInitialDests" annotation
     * that was precomputed for this call site at the end of pointer analysis. */
    public static void replaceActualsByInitials(BoolVector infoVector, Tree callTree, int whichKind) {
        TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
            callTree.getAnnotation("pointersActualInitialDests") ;
        TapList<TapPair<Integer, TapIntList>> actualInitials =
            (actualInitials4==null ? null : actualInitials4[whichKind]) ;
        while (actualInitials!=null) {
            if (infoVector.intersects(actualInitials.head.second)) {
                infoVector.set(actualInitials.head.first, true) ;
            }
            actualInitials = actualInitials.tail ;
        }
    }

    // Similar to replaceActualsByInitials(BoolVector, ...), but now on a BoolMatrix.
    public static void replaceActualsByInitials(BoolMatrix infoMatrix, Tree callTree, int whichKind) {
        TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
            callTree.getAnnotation("pointersActualInitialDests") ;
        TapList<TapPair<Integer, TapIntList>> actualInitials =
            (actualInitials4==null ? null : actualInitials4[whichKind]) ;
        while (actualInitials!=null) {
            TapIntList actualDests = actualInitials.head.second ;
            BoolVector sumVector = new BoolVector(infoMatrix.nCols) ;
            BoolVector oneVector ;
            // OR-accumulate all rows of indices in actualDests into a single sumVector
            while (actualDests!=null) {
                oneVector = infoMatrix.rows[actualDests.head] ;
                if (oneVector!=null) {
                    sumVector.cumulOr(oneVector) ;
                } else if (actualDests.head<infoMatrix.nCols) {
                    // oneVector is an identity row:
                    sumVector.set(actualDests.head, true) ;
                }
                actualDests = actualDests.tail ;
            }
            infoMatrix.rows[actualInitials.head.first] = sumVector ;
            actualInitials = actualInitials.tail ;
        }
    }

    /**
     * Builds the list of all changes on pointer dests from "destsBefore"
     * to "destsAfter". This list will be used to build comments about pointers
     * into the code, using buildCommentsForPointerDests().
     *
     * @param destsBefore Pointer dests before.
     * @param destsAfter  Pointers dests after.
     * @return The list of pointer destination changes.
     */
    private TapList<TapPair<TapIntList, BoolVector>> findDestsChangesAfterInstruction(BoolMatrix destsBefore, BoolMatrix destsAfter) {
        if (destsBefore == null || destsAfter == null) {
            return null;
        }
        int nCols = destsAfter.nCols;
        BoolVector rowBefore;
        BoolVector rowAfter;
        TapList<TapPair<TapIntList, BoolVector>> destsChanges = null;
        for (int j = destsAfter.nRows - 1; j >= 0; j--) {
            rowBefore = destsBefore.getRow(j);
            rowAfter = destsAfter.getRow(j);
            if (rowAfter!=null && isExplicitPointerIdentity(rowAfter, j, curSymbolTable)) rowAfter = null ;
            if (rowBefore!=null && isExplicitPointerIdentity(rowBefore, j, curSymbolTable)) rowBefore = null ;

            if (rowAfter != null &&
                    (rowBefore == null || !rowAfter.equals(rowBefore, nCols))) {
                destsChanges =
                        new TapList<>(new TapPair<>(new TapIntList(j, null), destsAfter.getRow(j)), destsChanges);
            }
        }
        return destsChanges;
    }

    /**
     * Builds a list of comments that show in a readable way
     * the pointer dests info contained in destsList.
     *
     * @param destsList The pointer dests to be shown.
     *                  It must be a TapList of TapPair's with:
     *                  -- first : a singleton TapIntList of pointer row index
     *                  -- second: a BoolVector of the possible destinations.
     * @return The list of op_stringCst comments Trees.
     */
    private TapList<Tree> buildCommentsForPointerDests(TapList<TapPair<TapIntList, BoolVector>> destsList) {
        if (destsList == null || curBlock.pointerInfosIn == null) {
            return null;
        }
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        TapList<String> pointedDestinations = null;
        int rowIndex;
        BoolVector destinations;
        ZoneInfo zoneInfo;
        String line;
        boolean firstDest;
        while (destsList != null) {
            rowIndex = destsList.head.first.head;
            destinations = destsList.head.second;
            zoneInfo = curSymbolTable.declaredZoneInfo(rowIndex, SymbolTableConstants.PTRKIND);
            if (zoneInfo != null) {
                line = "     " + zoneInfo.accessTreePrint(curUnit.language()) + " points to ";
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("      Built comment "+line+destinations+" (for "+zoneInfo+")") ;
                }
            } else {
                line = "";
            }
            if (zoneInfo != null) {
                pointedDestinations =
                    curSymbolTable.listOfPointedDestinations(destinations, nDZ, zoneInfo.pointerDestType());
            }
            firstDest = true;
            while (pointedDestinations != null) {
                line = line + (firstDest ? "" : " or ") + pointedDestinations.head;
                firstDest = false;
                pointedDestinations = pointedDestinations.tail;
            }
            tlResult = tlResult.placdl(ILUtils.build(ILLang.op_stringCst, line));
            destsList = destsList.tail;
        }
        return hdResult.tail;
    }

    /**
     * Final placement of the pointer comments that have been precomputed
     * and stored during terminateFGForUnit() on the current "curUnit".
     */
    private void placePointerDebugComments() {
        TapList<Block> blocks = curUnit.allBlocks;
        TapList<Instruction> instructions;
        TapList<Tree> comments;
        setCurBlockEtc(curUnit.entryBlock);
        //attach final comments at top of curUnit.entryBlock:
        curInstruction = curBlock.headInstr();
        if (curInstruction != null) {
            comments = detachPreComments(curInstruction);
            curInstruction.appendPreComments(comments);
        }
        while (blocks != null) {
            setCurBlockEtc(blocks.head);
            curInstruction = curBlock.headInstr();
            //attach final comments at top of curBlock:
            if (curInstruction != null) {
                comments = detachPreComments(curInstruction);
                if (curBlock instanceof HeaderBlock)
                //For loop headers, always place pointer comments AFTER!
                {
                    curInstruction.appendPostComments(comments);
                } else {
                    curInstruction.appendPreComments(comments);
                }
            }
            instructions = curBlock.instructions;
            while (instructions != null) {
                curInstruction = instructions.head;
                //attach final comments after instruction:
                comments = detachPostComments(curInstruction);
                curInstruction.appendPostComments(comments);
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }
        setCurBlockEtc(curUnit.exitBlock);
        //attach final comments at top of curBlock:
        curUnit.appendPostComments(curUnit.exitBlock.pointerComments());
        curUnit.exitBlock.setPointerComments(null);
        setCurBlockEtc(null);
    }

    private final int offsetZoneRkFromCalleeToCaller(int zoneRk, int offsetInCallee, int offsetInCaller) {
        if (zoneRk<offsetInCallee) return -1;
        return zoneRk-offsetInCallee+offsetInCaller ;
    }
    private final TapIntList mapOffsetZoneRkFromCalleeToCaller(TapIntList zoneRks, int offsetInCallee, int offsetInCaller) {
        TapIntList hdResult = new TapIntList(-1, null);
        TapIntList tlResult = hdResult;
        int index;
        while (zoneRks != null) {
            index = offsetZoneRkFromCalleeToCaller(zoneRks.head, offsetInCallee, offsetInCaller);
            if (index >= 0) {
                tlResult = tlResult.placdl(index);
            }
            zoneRks = zoneRks.tail;
        }
        return hdResult.tail;
    }

    private final ZoneInfo formalElemArgRkToZoneInfo(int zoneRk, int zoneKind, Unit calledUnit) {
        if (calledUnit.isStandard() || calledUnit.isInterface()) {
            return calledUnit.publicSymbolTable().declaredZoneInfo(zoneRk, zoneKind);
        } else { //This happens if calledUnit is an external => null publicSymbolTable !
            ZoneInfo zi = null;
            int i = calledUnit.paramElemsNb() - 1;
            while (zi == null && i >= 0) {
                if (calledUnit.paramElemZoneInfo(i).kindZoneNb(zoneKind) == zoneRk) {
                    zi = calledUnit.paramElemZoneInfo(i);
                }
                --i;
            }
            return zi;
        }
    }

    /** Modified mapZoneRkToKindZoneRk, specific for PointerAnalyzer.
     * New specific behavior added for extendedRk's greater than nDZ.
     */
    private final TapIntList mapZoneRkToElemArgPtrZoneRk(TapIntList extendedRks) {
        TapIntList hdResult = new TapIntList(-1, null);
        TapIntList tlResult = hdResult;
        int index;
        while (extendedRks != null) {
            index = zoneRkToElemArgPtrZoneRk(extendedRks.head);
            if (index >= 0) {
                tlResult = tlResult.placdl(index);
            }
            extendedRks = extendedRks.tail;
        }
        return hdResult.tail;
    }

    /** Modified zoneRkToKindZoneRk, specific for PointerAnalyzer.
     * New specific behavior added for extendedRk's greater than nDZ, and converted to PTRKIND indices.
     */
    private final int zoneRkToElemArgPtrZoneRk(int extendedRk) {
        if (extendedRk < 0) return -1 ;
        if (extendedRk < nDZ) {  // Declared zone:
            // Make sure that this is not one of the root dummy zones (NULL, Undef, IO, channels), because
            // there cannot be any data-flow info attached to them (if there is one, it is wrong!)
            // This filtering should not be necessary, but sometimes it is...
            if (extendedRk<curCallGraph.numberOfDummyRootZones) return -1 ;
            ZoneInfo zi = curSymbolTable.declaredZoneInfo(extendedRk, SymbolTableConstants.ALLKIND);
            int kZoneNb = (zi==null ? -1 : zi.kindZoneNb(SymbolTableConstants.PTRKIND));
            return (kZoneNb==-1 ? -1 : kZoneNb);
        } else { // Formal params or result zone: new Behavior compared to DataFlowAnalyzer.zoneRkToKindZoneRk().
            SymbolTable calledST = curCalledUnit.publicSymbolTable() ;
            ZoneInfo zi = calledST.localDeclaredZoneInfo(extendedRk - nDZ +
                              calledST.firstDeclaredZone(SymbolTableConstants.ALLKIND), SymbolTableConstants.ALLKIND);
            int kZoneNb = (zi==null ? -1 : zi.kindZoneNb(SymbolTableConstants.PTRKIND));
            return (kZoneNb==-1 ? -1 : kZoneNb + curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND));
        }
    }

    /**
     * Attach the given list of pre-comment Trees at a temporary place close to the given
     * Instruction. The final location of these comments will be before the Instruction.
     *
     * @param instruction The Instruction that will be prefixed by the comments .
     * @param ptrComments The comments about pointer destinations.
     */
    private void tempAttachPreComments(Instruction instruction, TapList<Tree> ptrComments) {
        if (instruction.tree != null) {
            instruction.tree.setAnnotation("ptrPreComments", ptrComments);
        }
    }

    /**
     * Attach the given list of post-comment Trees at a temporary place close to the given
     * Instruction. The final location of these comments will be after the Instruction.
     *
     * @param instruction The Instruction that will be suffixed by the comments .
     * @param ptrComments The comments about pointer destinations.
     */
    private void tempAttachPostComments(Instruction instruction, TapList<Tree> ptrComments) {
        if (instruction.tree != null) {
            instruction.tree.setAnnotation("ptrPostComments", ptrComments);
        }
    }

    /**
     * Detach and return the list of pre-comment Trees from their temporary storage place.
     *
     * @param instruction The Instruction that contains the temporary storage place.
     * @return The list of comment Trees.
     */
    private TapList<Tree> detachPreComments(Instruction instruction) {
        if (instruction.tree == null) {
            return null;
        } else {
            TapList<Tree> result = instruction.tree.getAnnotation("ptrPreComments");
            instruction.tree.removeAnnotation("ptrPreComments");
            return result;
        }
    }

    /**
     * Detach and return the list of post-comment Trees from their temporary storage place.
     *
     * @param instruction The Instruction that contains the temporary storage place.
     * @return The list of comment Trees.
     */
    private TapList<Tree> detachPostComments(Instruction instruction) {
        if (instruction.tree == null) {
            return null;
        } else {
            TapList<Tree> result = instruction.tree.getAnnotation("ptrPostComments");
            instruction.tree.removeAnnotation("ptrPostComments");
            return result;
        }
    }
}
