/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.representation.* ;
import fr.inria.tapenade.utils.* ;

/**
 * "To Be Recorded" (TBR) analyzer.
 * A Variable is TBR at some program location if its value has been
 * used by an instruction upstream, and this usage is such that
 * the value will be used by the derivative adjoint instructions too.
 * Therefore, if a variable is TBR at some location, and is overwritten
 * later, then it must be saved (e.g. PUSH'ed) just before this overwrite.
 */
public final class ADTBRAnalyzer extends DataFlowAnalyzer {

    /**
     * Constant that codes for a bottom-up analysis.
     */
    private static final int BOTTOMUP_1 = 1;
    /**
     * Constant that codes for a top-down analysis
     */
    private static final int TOPDOWN_2 = 2;

    /**
     * The result of this TBR analysis, between each
     * successive instructions, for the current Unit.
     */
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> curUnitTBRs;
    /**
     * Current TBR on the downstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> tbrsDown;
    /**
     * Same as tbrsDown, but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> tbrsCycleDown;
    /**
     * Current TBR on the upstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> tbrsUp;
    /**
     * Same as tbrsUp, but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<TapPair<BoolVector, BoolVector>> tbrsCycleUp;
    /**
     * Current TBR info being propagated
     * through a Block by the current analysis.
     */
    private BoolVector tbrTmp;
    private BoolVector tbrTmpOnDiffPtr;
    /**
     * Same as "tbrTmp", but for "cycling" info.
     */
    private BoolVector tbrTmpCycle;
    private BoolVector tbrTmpCycleOnDiffPtr;
    /**
     * TBR info that will be accumulated where control flow merges.
     */
    private BoolVector additionalTBR;
    private BoolVector additionalTBROnDiffPtr;
    /**
     * The list of clean do-loop index zones around each Block.
     */
    private BlockStorage<TapIntList> cleanLoopIndexZones;
    /**
     * Used during the recomputation analysis:
     * for the current curUnit and its current curActivity,
     * between each successive instructions,
     * the pair of the selected (1) list of primal recomputations
     *                      and (2) list of diff pointer recomputations.
     */
    private BlockStorage<TapList<TapPair<TapList<Instruction>,TapList<Instruction>>>> curUnitRecomps;
    /**
     * A part of the result of the recomputation analysis:
     * for the current curUnit and its current curActivity,
     * for each instruction, an integer combining two booleans,
     * number one true if instruction is considered for recomputing its primal,
     * number two true if same property applies to its diff pointer.
     */
    private BlockStorage<int[]> curUnitMayBeRecomputeds;
    /**
     * A part of the result of the recomputation analysis:
     * for the current curUnit and its current curActivity,
     * for each instruction, an integer combining two booleans,
     * number one true if instruction, although considered for recomputing
     * its primal in the backward sweep, must in addition remain in the forward sweep,
     * number two true if same property applies to its diff pointer.
     */
    private BlockStorage<int[]> curUnitButRemainsInFwdSweeps;
    /**
     * Current recomputability on the downstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<ADTBRAnalyzer.RecompInfo[]> recompsDown;
    /**
     * Same as recompsDown, but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<ADTBRAnalyzer.RecompInfo[]> recompsCycleDown;
    /**
     * Current recomputability on the upstream end of each Block.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<ADTBRAnalyzer.RecompInfo[]> recompsUp;
    /**
     * Same as recompsUp, but for "cycling" info.
     * Used during analysis through a given Unit.
     */
    private BlockStorage<ADTBRAnalyzer.RecompInfo[]> recompsCycleUp;
    /**
     * Current "recomputability" info being propagated
     * through a Block by the current analysis.
     */
    private ADTBRAnalyzer.RecompInfo[] recompTmp;
    /**
     * Same as "recompTmp", but for "cycling" info.
     */
    private ADTBRAnalyzer.RecompInfo[] recompTmpCycle;
    /**
     * Recomputability info that will be accumulated where control flow merges.
     */
    private ADTBRAnalyzer.RecompInfo[] additionalRecomp;

    /**
     * The result of diff liveness analysis for the current Unit.
     */
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> curUnitDiffLivenesses;
    /**
     * The result of diff-overwrite analysis for the current Unit.
     */
    private BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> curUnitDiffOverwrites;
    /**
     * The current number of declared Pointer zones.
     */
    private int nDPZ;
    /**
     * The current phase of TBR analysis
     * (bottom-up (BOTTOMUP_1) or top-down (TOPDOWN_2)).
     */
    private int phase;

    /**
     * Creates a TBR Analyzer.
     */
    private ADTBRAnalyzer(CallGraph callGraph) {
        super(callGraph, "TBR Analysis", TapEnv.traceTBR());
        this.conservativeValue = true;
    }

    /**
     * Run TBR analysis on the given Call Graph, under the given root Units.
     */
    public static void runAnalysis(CallGraph callgraph,
                                   TapList<Unit> rootUnits) {
        TapEnv.setADTBRAnalyzer(new ADTBRAnalyzer(callgraph));
        TapEnv.adTbrAnalyzer().run(rootUnits);
    }

    /**
     * Local specialized equivalent of
     * FlowGraphDifferentiator.findBlockLoopKind(block, false)==DO_LOOP.
     */
    private static boolean findBlockLoopKindIsDoLoop(Block block) {
        if (block instanceof HeaderBlock) {
            Instruction loopInstr = block.headInstr();
            LoopBlock loopBlock = block.enclosingLoop();
            return loopBlock.entryBlocks != null
                    && loopBlock.entryBlocks.tail == null
                    && loopBlock.exitBlocks.tail == null
                    && loopBlock.exitBlocks.head == block
                    && loopBlock.exitArrows != null
                    && block.instructions != null
                    && block.instructions.tail == null
                    && block.headInstr().tree.opCode() == ILLang.op_loop
                    // Single entry to header AND single exit from header AND loop operator => clean loop:
                    && loopInstr.tree.down(3).opCode() == ILLang.op_do
                    && loopInstr.hasDirective(Directive.IILOOP) == null;
        } else {
            return false;
        }
    }

    /**
     * @return true if the diff of the given tree (which is a pointer) is marked "To Be Recorded".
     */
    public static boolean isTBROnDiffPtr(ActivityPattern curActivity, Tree tree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "TBR");
        return annotationTBR != null && ((boolean[]) annotationTBR)[1];
    }

    /**
     * @return true if the given tree is marked "To Be Recorded".
     */
    public static boolean isTBR(ActivityPattern curActivity, Tree tree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(tree, curActivity, "TBR");
        return annotationTBR != null && ((boolean[]) annotationTBR)[0];
    }

    /**
     * @return true if the given tree, which is a DO-loop index,
     * is marked "To Be Recorded", either upon loop entry or upon loop cycle.
     */
    public static boolean isTBRindex(ActivityPattern curActivity, Tree indexTree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(indexTree, curActivity, "TBR");
        return annotationTBR != null;
    }

    /**
     * @return true if the given tree, which is a DO-loop index,
     * is marked "To Be Recorded" upon loop entry.
     */
    public static boolean isTBRindexOnLoopEntry(ActivityPattern curActivity, Tree indexTree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(indexTree, curActivity, "TBR");
        return annotationTBR != null && ((TapIntList) annotationTBR).head == 1;
    }

    /**
     * @return true if the given tree, which is a DO-loop index,
     * is marked "To Be Recorded" upon loop cycle.
     */
    public static boolean isTBRindexOnLoopCycle(ActivityPattern curActivity, Tree indexTree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(indexTree, curActivity, "TBR");
        return annotationTBR != null && ((TapIntList) annotationTBR).tail.head == 1;
    }

    /**
     * If there is an annotation about "TBR" on "fromTree" for activity "curActivity",
     * copies it onto "toTree".
     */
    public static void copyAnnotation(ActivityPattern curActivity, Tree fromTree, Tree toTree) {
        Object annotationTBR = ActivityPattern.getAnnotationForActivityPattern(fromTree, curActivity, "TBR");
        if (annotationTBR != null) {
            ActivityPattern.setAnnotationForActivityPattern(toTree, curActivity, "TBR", annotationTBR);
        }
    }

    //TEMPORARY UNTIL TBR IS REWRITTEN TAKING INTO ACCOUNT THE JOINT/SPLIT MODES.
    private static boolean isTreeRequiredInDiff(ActivityPattern curActivity, Tree tree, boolean onDiffPtr) {
        return tree == null ||      // for Unit definitions
            DiffLivenessAnalyzer.isTreeRequiredInDiff(curActivity, tree, true, onDiffPtr) ||
            DiffLivenessAnalyzer.isTreeRequiredInDiff(curActivity, tree, false, onDiffPtr);
    }

    /**
     * Run TBR analysis on a CallGraph. Bottom-Up and Top-Down are mixed because
     * Top-Down needs the summaries of called procedures provided by Bottom-Up, and
     * conversely PUSH/POP decided during Top-Down may require Bottom-Up to run again.
     * Bottom-Up sweeps compute the TBR info upon exit from each Unit, restricted
     * to TBR due to uses from inside the Unit (in papers: Use(\overleftarrow{Unit})).
     * Top-Down sweeps compute the full interprocedural TBR, using the result of Bottom-Up.
     */
    @Override
    protected void run(TapList<Unit> rootUnits) {
        runBottomUpTopDownAnalysis(rootUnits);
    }

    public void setCurUnitActivity(ActivityPattern activity) {
        curActivity = activity;
    }

    /**
     * Forces the next TBR runs to analyze only a sub-flow-graph of the current Unit.
     */
    public void setPhaseSubGraph() {
        phase = TOPDOWN_2;
    }

    /**
     * In addition to super.setCurUnitEtc(), sets nDPZ.
     */
    @Override
    public void setCurUnitEtc(Unit unit) {
        super.setCurUnitEtc(unit);
        nDPZ = curSymbolTable == null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
    }

    /**
     * Initializes the context of Unit "unit" for TBR analysis,
     * before the sweeps on the CallGraph.
     * Before a BOTTOMUP_1 phase, does nothing and returns null.
     * Before a TOPDOWN_2 phase, places into each ActivityPattern's of curUnit,
     * an initial, empty, top-down context, which is a TBR BoolVector.
     */
    @Override
    protected Object initializeCGForUnit() {
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        while (activities != null) {
            curActivity = activities.head;
            int lengthAll = curUnit.publicZonesNumber(SymbolTableConstants.ALLKIND) ;
            int lengthPtr = curUnit.publicZonesNumber(SymbolTableConstants.PTRKIND) ;
            curActivity.setAdTBRTopDownContext(new BoolVector(lengthAll)) ;
            curActivity.setAdTBRTopDownContextOnDiffPtr(new BoolVector(lengthPtr)) ;
            activities = activities.tail;
        }
        return null;
    }

    /**
     * Only for the degenerate case where we don't want to run full TBR analysis,
     * AND we neither want to restrict TBR saving to used variables, we must
     * set the initial tbr true for all variables, provided they have been initialized.
     */
    @Override
    protected void initializeCGForRootUnit() {
        // For the degenerate "alwaysSave" noOptim case, here we should
        // initialize tbr to all variables except uninitialized ones.
        // We can't do that because we don't know them in general,
        // so we set tbr true for all public vars (i.e. all of the public shape) of curUnit.
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        while (activities != null) {
            curActivity = activities.head;
            if (!TapEnv.doTBR() && !TapEnv.get().dontSaveUnused) {
                curActivity.adTBRTopDownContext().setTrue();
                // Never set TBR true for NULL and Undef destinations, IO, and MPI channels:
                for (int i=curCallGraph.numberOfDummyRootZones-1 ; i>=0 ; --i) {
                    curActivity.adTBRTopDownContext().set(i, false) ;
                }
                // Commented out because it forces differentiation of all non-differentiable pointers
                // cf opa-nemo -nooptim saveonlyused:
                //curActivity.ADTBRTopDownContextOnDiffPtr.setTrue() ;
            }
            activities = activities.tail;
        }
    }

    /**
     * Run TBR analysis on the curUnit, forward.
     * Run one analysis for each ActivityPattern of curUnit.
     * Propagates up the result of analyzeForward(), which is
     * "true" if the analysis up-result is modified since previous run.
     */
    @Override
    protected boolean analyze() {
        TapList<ActivityPattern> activities = curUnit.activityPatterns;
        boolean hasChanged = false;
        while (activities != null) {
            curActivity = activities.head;
            if (curUnit.hasSource()) {
                TapEnv.printOnTrace(15, " (ActivityPattern @" + Integer.toHexString(curActivity.hashCode()) + ":)");
            }
            prepareStorageForUnit(curUnit);
            hasChanged = analyzeForward(null, null, null) || hasChanged;
            activities = activities.tail;
        }
        return hasChanged;
    }

    @Override
    protected boolean analyzeInBottomUp() {
        phase = BOTTOMUP_1;
        return analyze();
    }

    @Override
    protected boolean analyzeInTopDown() {
        phase = TOPDOWN_2;
        return analyze();
    }

    /**
     * Creates the storage locations for the results of TBR analysis
     * for the given Unit, if they are not already created.
     */
    public void prepareStorageForUnit(Unit unit) {
        curUnitTBRs = curActivity.tbrs();
        if (curUnitTBRs == null) {
            curUnitTBRs = new BlockStorage<>(unit);
            curActivity.setTbrs(curUnitTBRs);
        }
        curUnitRecomps = curActivity.recomps();
        if (curUnitRecomps == null) {
            curUnitRecomps = new BlockStorage<>(unit);
            curActivity.setRecomps(curUnitRecomps);
        }
        if (curActivity.mayBeRecomputed() == null) {
            curActivity.initializeMayBeRecomputed();
        }
        curUnitMayBeRecomputeds = curActivity.mayBeRecomputed();
        if (curActivity.butRemainsInFwdSweep() == null) {
            curActivity.initializeButRemainsInFwdSweep();
        }
        curUnitButRemainsInFwdSweeps = curActivity.butRemainsInFwdSweep();
    }

    /**
     * Initializations before each TBR analysis of "curUnit".
     * For a top-down analysis, retrieves and uses the call context
     * from the global "topDownContexts".
     */
    @Override
    protected void initializeFGForUnit() {
        tbrsUp = new BlockStorage<>(curUnit);
        tbrsDown = new BlockStorage<>(curUnit);
        tbrsCycleUp = new BlockStorage<>(curUnit);
        tbrsCycleDown = new BlockStorage<>(curUnit);
        curSymbolTable = curUnit.privateSymbolTable();
        if (curSymbolTable == null) {
            curSymbolTable = curUnit.publicSymbolTable();
        }
        nDZ = (curSymbolTable==null ? 0 : curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
        nDPZ = (curSymbolTable==null ? 0 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace(" ========== " + (phase == BOTTOMUP_1 ? "BOTTOM-UP" : "TOP-DOWN") + " REVERSE RESTORATION (==TBR) ANALYSIS OF UNIT " + curUnit.name() + " : ==========");
            TapEnv.printlnOnTrace(" Activity context of this analysis: " + curActivity);
            ZoneInfo zi;
            for (int i = 0; i < nDZ; ++i) {
                zi = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zi != null) {
                    TapEnv.printOnTrace(" [" + i + "]" + zi.accessTreePrint(curUnit.language()));
                }
            }
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace();
        }
        curUnitDiffLivenesses = null;
        curUnitDiffOverwrites = null;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            recompsUp = new BlockStorage<>(curUnit);
            recompsDown = new BlockStorage<>(curUnit);
            recompsCycleUp = new BlockStorage<>(curUnit);
            recompsCycleDown = new BlockStorage<>(curUnit);
            cleanLoopIndexZones = new BlockStorage<>(curUnit);
            if (TapEnv.diffLivenessAnalyzer() != null) {
                curUnitDiffLivenesses = curActivity.diffLivenesses();
                if (curUnitDiffLivenesses == null && curActivity.diffLivenessesCkp() != null) {
                    curUnitDiffLivenesses = curActivity.diffLivenessesCkp();
                }
                curUnitDiffOverwrites = curActivity.diffOverwrites();
                if (curUnitDiffOverwrites == null && curActivity.diffOverwritesCkp() != null) {
                    curUnitDiffOverwrites = curActivity.diffOverwritesCkp();
                }
            }
        }
    }

    /**
     * Set curBlock, curSymbolTable, nDZ, nDPZ.
     *
     * @param block The new current Block.
     */
    @Override
    protected void setCurBlockEtc(Block block) {
        super.setCurBlockEtc(block);
        nDPZ = (curSymbolTable==null ? -9 : curSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND)) ;
    }

    @Override
    protected void initializeFGForInitBlock() {
        BoolVector initTBR = null;
        BoolVector initTBROnDiffPtr = null;
        ADTBRAnalyzer.RecompInfo[] initRecomp = null;
        if (phase == BOTTOMUP_1) {
            initTBR = new BoolVector(nDZ);
            initTBROnDiffPtr = new BoolVector(nDPZ);
        } else if (phase == TOPDOWN_2) {
            if (TapEnv.doRecompute()) {
                initRecomp = initialRecompInfoArray();
                recompsDown.store(curBlock, initRecomp);
            }
            if (curBlock == curUnit.entryBlock) {
                // This is the standard case, propagating on the full flow graph:
                initTBR = curActivity.adTBRTopDownContext() ;
                initTBROnDiffPtr = curActivity.adTBRTopDownContextOnDiffPtr() ;
                // For the degenerate "alwaysSave" noOptim case, we add into tbr
                // all "save" and "data" zones, because they do have an initial value
                if (!TapEnv.doTBR() && !TapEnv.get().dontSaveUnused) {
                    initTBR.set(curUnit.entryBlock.initializedLocalZones(), true);
                    setKindZoneRks(initTBROnDiffPtr, SymbolTableConstants.PTRKIND,
                            curUnit.entryBlock.initializedLocalZones(), true);
                }
            } else {
                // This is for running on only a part of the flow graph:
                initTBR = new BoolVector(nDZ);
                initTBROnDiffPtr = new BoolVector(nDPZ);
                // For the degenerate "alwaysSave" noOptim case, here we should
                // initialize tbr to all variables except uninitialized ones.
                // We can't do that because we don't know them in general, but we assume that if
                // we are in the degenerate noOptim case, we won't run TBR on fragments anyway !
                if (runSpecialCycleAnalysis(curBlock)) {
                    tbrsCycleDown.store(curBlock, new TapPair<>(initTBR, initTBROnDiffPtr));
                    if (TapEnv.doRecompute()) {
                        recompsCycleDown.store(curBlock, initRecomp);
                    }
                }
            }
        }
        tbrsDown.store(curBlock, new TapPair<>(initTBR, initTBROnDiffPtr));
    }

    /**
     * Precomputes and stores into "cleanLoopIndexZones" the TapIntList of the
     * loop index zones of the clean do-loops around the given "curBlock".
     * Also initializes the boolean attached to each Instruction that tells that this
     * Instruction must remain in the FWD sweep even if recomputed in the BWD sweep.
     */
    @Override
    protected void initializeFGForBlock() {
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            TapIntList indexZones = null;
            LoopBlock enclosingLoop = curBlock.enclosingLoop();
            HeaderBlock headerBlock;
            Instruction doInstr;
            Tree index;
            TapIntList izones;
            while (enclosingLoop != null) {
                headerBlock = enclosingLoop.header();
                if (headerBlock != null && headerBlock.isACleanDoLoop()
                        && !headerBlock.dirtyOverwrittenIndex) {
                    doInstr = headerBlock.instructions.head;
                    index = doInstr.tree.down(3).down(1);
                    ToBool total = new ToBool(true);
                    izones = curSymbolTable.listOfZonesOfValue(index, total, doInstr);
                    if (total.get() && izones != null && izones.tail == null) {
                        indexZones = new TapIntList(izones.head, indexZones);
                    }
                }
                enclosingLoop = enclosingLoop.enclosingLoop();
            }
            cleanLoopIndexZones.store(curBlock, indexZones);
            TapList<Instruction> instructions = curBlock.instructions;
            while (instructions != null) {
                ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, instructions.head, 1, false);
                ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, instructions.head, 2, false);
                ActivityPattern.setInstructionBoolInfo(curUnitButRemainsInFwdSweeps, instructions.head, 1, false);
                ActivityPattern.setInstructionBoolInfo(curUnitButRemainsInFwdSweeps, instructions.head, 2, false);
                instructions = instructions.tail;
            }
        }
    }

    @Override
    protected void terminateFGForBlock() {
        if (phase == TOPDOWN_2) {
            TapPair<BoolVector, BoolVector> tbrUp = tbrsUp.retrieve(curBlock);
            tbrTmp = tbrUp == null ? null : tbrUp.first;
            tbrTmpOnDiffPtr = tbrUp == null ? null : tbrUp.second;
            if (tbrTmp == null) {
                tbrTmp = new BoolVector(nDZ);
            }
            if (tbrTmpOnDiffPtr == null) {
                tbrTmpOnDiffPtr = new BoolVector(nDPZ);
            }
            TapPair<BoolVector, BoolVector> tbrCycleUp = tbrsCycleUp.retrieve(curBlock);
            tbrTmpCycle = tbrCycleUp == null ? null : tbrCycleUp.first;
            tbrTmpCycleOnDiffPtr = tbrCycleUp == null ? null : tbrCycleUp.second;
            if (TapEnv.doRecompute()) {
                recompTmp = recompsUp.retrieve(curBlock);
                if (recompTmp == null) {
                    recompTmp = initialRecompInfoArray();
                }
                recompTmpCycle = recompsCycleUp.retrieve(curBlock);
            }
            TapList<TapPair<BoolVector, BoolVector>> hdListTBRs = new TapList<>(null, null);
            TapList<TapPair<TapList<Instruction>,TapList<Instruction>>> hdListRecomps = new TapList<>(null, null);
            propagateAndStoreThroughBlock(curBlock, hdListTBRs, hdListRecomps);
            curUnitTBRs.store(curBlock, hdListTBRs.tail);
            curUnitRecomps.store(curBlock, hdListRecomps.tail);
        }
    }

    @Override
    protected void terminateFGForTermBlock() {
        if (phase == TOPDOWN_2) {
            TapList<TapPair<BoolVector, BoolVector>> termBlockInfos = curUnitTBRs.retrieve(curBlock);
            if (termBlockInfos == null) {
                termBlockInfos = new TapList<>(null, new TapList<>(null, null));
                curUnitTBRs.store(curBlock, termBlockInfos);
            }
            TapPair<BoolVector, BoolVector> tbrUp = tbrsUp.retrieve(curBlock);
            BoolVector termInfo = tbrUp == null ? null : tbrUp.first;
            if (termInfo == null) {
                termInfo = new BoolVector(nDZ);
            } else {
                termInfo = termInfo.copy();
            }
            BoolVector termInfoOnDiffPtr = tbrUp == null ? null : tbrUp.second;
            if (termInfoOnDiffPtr == null) {
                termInfoOnDiffPtr = new BoolVector(nDPZ);
            } else {
                termInfoOnDiffPtr = termInfoOnDiffPtr.copy();
            }
            TapPair<BoolVector, BoolVector> tbrCycleUp = tbrsCycleUp.retrieve(curBlock);
            BoolVector termCycleInfo = tbrCycleUp == null ? null : tbrCycleUp.first;
            if (termCycleInfo != null) {
                termInfo.cumulOr(termCycleInfo);
            }
            BoolVector termCycleInfoOnDiffPtr = tbrCycleUp == null ? null : tbrCycleUp.second;
            if (termCycleInfoOnDiffPtr != null) {
                termInfoOnDiffPtr.cumulOr(termCycleInfoOnDiffPtr);
            }
            termBlockInfos.head = new TapPair<>(termInfo, termInfoOnDiffPtr);

            //If we reach a termination curBlock, this is the return point where
            // the fwd sweep ends and the bwd sweep starts. Therefore the local vars
            // don't fall out of scope, and there is no need to recompute or push them,
            // provided their original computation remains in the fwd sweep.
            // [llh] TODO: this is wrong and must be corrected in the case of "NOCKP" calls !!
            if (TapEnv.doRecompute()) {
                recompTmp = recompsUp.retrieve(curBlock);
                recompTmpCycle = recompsCycleUp.retrieve(curBlock);
                if (recompTmp != null) {
                    for (int i = recompTmp.length - 1; i >= 0; --i) {
                        if (recompTmp[i] != null) {  //recompTmp[i] can be empty only for it "primal" or "diffPtr" case.
                            if (!recompTmp[i].usedForPrimal)
                                setInstructionRemainsInFwdSweep(recompTmp, i, 1);
                            if (!recompTmp[i].usedForDiffPtr)
                                setInstructionRemainsInFwdSweep(recompTmp, i, 2);
                        }
                    }
                }
                if (recompTmpCycle != null) {
                    for (int i = recompTmpCycle.length - 1; i >= 0; --i) {
                        if (recompTmpCycle[i] != null) {
                            if (!recompTmpCycle[i].usedForPrimal)
                                setInstructionRemainsInFwdSweep(recompTmpCycle, i, 1);
                            if (!recompTmpCycle[i].usedForDiffPtr)
                                setInstructionRemainsInFwdSweep(recompTmpCycle, i, 2);
                        }
                    }
                }
            }
        }
    }

    @Override
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        return false;
    }

    @Override
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        return false;
    }

    /**
     * Terminates TBR analysis on "curUnit".
     * Done after each analysis of "curUnit", i.e. more than once if recursion.
     * For a TOPDOWN_2 phase, builds the final result that
     * contains the TBR status between each pair of successive Instruction's.
     *
     * @return true if the result propagated up (unitLocalTBR + unitLocalTBROnDiffPtr)
     * has changed since last sweep.
     */
    @Override
    protected boolean terminateFGForUnit() {
        boolean hasChangedUp = false;
        TapPair<BoolVector, BoolVector> tbrUp = tbrsUp.retrieve(curUnit.exitBlock);
        BoolVector exitTBR = (tbrUp==null ? null : tbrUp.first) ;
// TODO: ideally, exitTBR should be only the *new* contribution to TBR added by curUnit,
//  with respect to TBR upon entry into curUnit. The next minus() is supposed to compute that.
//  However, this is commented for exitTBR (and not for exitTBROnDiffPtr!) due to pbs on morlighem-issm.
//         if (exitTBR!=null)
//             exitTBR = exitTBR.minus(curActivity.adTBRTopDownContext()) ;
        BoolVector exitTBROnDiffPtr = tbrUp == null ? null : tbrUp.second;
        if (exitTBROnDiffPtr!=null) {
            exitTBROnDiffPtr = exitTBROnDiffPtr.minus(curActivity.adTBRTopDownContextOnDiffPtr());
        }

        // Detect if result (exitTBR+exitTBROnDiffPtr) has increased since last time:
        if (curActivity.localExitTBR() == null) {
            hasChangedUp = (exitTBR != null || exitTBROnDiffPtr != null) ;
        } else {
            hasChangedUp =
                !(curActivity.localExitTBR().contains(exitTBR, nDZ) &&
                  curActivity.localExitTBROnDiffPtr().contains(exitTBROnDiffPtr, nDPZ));
        }

        if (hasChangedUp) {
            curActivity.setLocalExitTBR(curActivity.localExitTBR()==null
                                        ? exitTBR
                                        : exitTBR.or(curActivity.localExitTBR()));
            curActivity.setLocalExitTBROnDiffPtr(curActivity.localExitTBROnDiffPtr()==null
                                                 ? exitTBROnDiffPtr
                                                 : exitTBROnDiffPtr.or(curActivity.localExitTBROnDiffPtr()));
        }
        if (phase == TOPDOWN_2) {
            //TODO: to avoid duplicate work for recursive call graphs,
            // this should be done only once, in terminateCGForUnit() !
            // but it's not easy because the local
            // results tbrsUp,etc... are by then lost
            curUnitTBRs.store(curUnit.entryBlock,
                    new TapList<>(tbrsDown.retrieve(curUnit.entryBlock), null));
            curUnitTBRs.store(curUnit.exitBlock,
                    new TapList<>(tbrsUp.retrieve(curUnit.exitBlock), null));
            tbrsUp = null;
            tbrsDown = null;
            tbrsCycleUp = null;
            tbrsCycleDown = null;
        }
        return hasChangedUp;
    }

    /**
     * When "frontier" is a list of converging flow arrows that define
     * the entry into a fragment of the current Unit, returns the vector of
     * the zones that are "To Be Recorded" upon entry into this fragment,
     * by collecting the info coming from the frontier's origins.
     */
    public TapPair<BoolVector, BoolVector> getFrontierTBR(TapList<FGArrow> frontier) {
        int resNDZ = commonNDZofFrontierDestinations(frontier, SymbolTableConstants.ALLKIND);
        int resNDPZ = commonNDZofFrontierDestinations(frontier, SymbolTableConstants.PTRKIND);
        BoolVector result = new BoolVector(resNDZ);
        BoolVector resultOnDiffPtr = new BoolVector(resNDPZ);
        FGArrow arrow;
        int commonLength;
        int commonLengthOnDiffPtr;
        curUnitTBRs = curActivity.tbrs();
        TapList<TapPair<BoolVector, BoolVector>> tbrS;
        TapPair<BoolVector, BoolVector> lastTBRinfo;
        while (frontier != null) {
            arrow = frontier.head;
            tbrS = curUnitTBRs.retrieve(arrow.origin);
            if (tbrS != null) {
                commonLength = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
                commonLengthOnDiffPtr = arrow.commonSymbolTable().declaredZonesNb(SymbolTableConstants.PTRKIND);
                lastTBRinfo = TapList.last(tbrS);
                result.cumulOr(lastTBRinfo.first, commonLength);
                resultOnDiffPtr.cumulOr(lastTBRinfo.second, commonLengthOnDiffPtr);
            }
            frontier = frontier.tail;
        }
        return new TapPair<>(result, resultOnDiffPtr);
    }

    public Object saveInsideInfo(TapList<Block> blocksInside, BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> unitTBRs) {
        Block blockInside ;
        TapList<TapList<TapPair<BoolVector, BoolVector>>> hdInfo = new TapList<>(null, null) ;
        TapList<TapList<TapPair<BoolVector, BoolVector>>> tlInfo = hdInfo ;
        TapList<TapPair<BoolVector, BoolVector>> blockInfo ;
        while (blocksInside != null) {
            blockInside = blocksInside.head ;
            blockInfo = unitTBRs.retrieve(blockInside);
            tlInfo = tlInfo.placdl(blockInfo) ;
            blocksInside = blocksInside.tail;
        }
        return hdInfo.tail ;
    }

    public void restoreInsideInfo(TapList<Block> blocksInside, Object savedInfo, BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> unitTBRs) {
        Block blockInside ;
        TapList<TapList<TapPair<BoolVector, BoolVector>>> inInfo =
            (TapList<TapList<TapPair<BoolVector, BoolVector>>>)savedInfo ;
        TapList<TapPair<BoolVector, BoolVector>> blockInfo ;
        while (blocksInside != null) {
            blockInside = blocksInside.head ;
            blockInfo = inInfo.head ;
            unitTBRs.store(blockInside, blockInfo) ;
            blocksInside = blocksInside.tail;
            inInfo = inInfo.tail ;
        }
    }

    /**
     * Empty the global variables tbrTmp and tbrTmpCycle that are
     * used to accumulate the TBR info that arrive from FGArrows.
     */
    @Override
    protected void setEmptyCumulAndCycleValues() {
        tbrTmp = null;
        tbrTmpOnDiffPtr = null;
        tbrTmpCycle = null;
        tbrTmpCycleOnDiffPtr = null;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            recompTmp = null;
            recompTmpCycle = null;
        }
    }

    /**
     * Get the TBR info that arrives down-flow through the curArrow.
     * Must return false iff no value arrives, i.e. the control hasn't
     * reached to this point yet.
     * If curArrow cycles from a loop header to its next iteration, returns
     * the special cycles-related TBR info downstream from the loop header.
     * Stores the info into the global additionalTBR, plus when appropriate additionalRecomp.
     */
    @Override
    protected boolean getValueFlowingThrough() {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Flowing across " + curArrow + ':');
        }
        Block origin = curArrow.origin;
        SymbolTable arrowSymbolTable = curArrow.commonSymbolTable() ;
        int nDZcommon = arrowSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        int nDZcommonOnDiffPtr = arrowSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
        TapPair<BoolVector, BoolVector> retrievedFromOrig;
        if (runSpecialCycleAnalysis(origin)
                && curArrow.containsCase(FGConstants.NEXT)) {
            retrievedFromOrig = tbrsCycleDown.retrieve(origin);
            if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                additionalRecomp = recompsCycleDown.retrieve(origin);
            }
        } else {
            retrievedFromOrig = tbrsDown.retrieve(origin);
            if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                additionalRecomp = recompsDown.retrieve(origin);
            }
        }
        additionalTBR = (retrievedFromOrig==null ? null : retrievedFromOrig.first) ;
        additionalTBROnDiffPtr = (retrievedFromOrig==null ? null : retrievedFromOrig.second) ;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            int nDZorig = origin.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            int nDZdest = curArrow.destination.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            if (additionalRecomp != null) {
                TapList<TapPair<BoolVector, BoolVector>> diffOverwritesList = null;
                if (curUnitDiffOverwrites != null) {
                    diffOverwritesList = curUnitDiffOverwrites.retrieve(origin);
                }
                BoolVector diffOverwritesAfter = null;
                if (diffOverwritesList != null) {
                    TapPair<BoolVector, BoolVector> lastDiffOverwrites =
                            TapList.last(diffOverwritesList);
                    if (lastDiffOverwrites != null) {
                        diffOverwritesAfter = lastDiffOverwrites.first;
                    }
                }
                additionalRecomp = removeRecompOutOfScope(additionalRecomp,
                        nDZorig, nDZcommon, nDZdest,
                        cleanLoopIndexZones.retrieve(origin),
                        diffOverwritesAfter, additionalTBR);
            }
        }
        // Now detect the pointers that are going to be push-popped.
        // Note: there is no falling out of scope if this is the exit arrow and we are differentiating JOINT.
        if (phase == TOPDOWN_2 && additionalTBR != null && (curArrow.destination!=curUnit.exitBlock || curUnit.mustDifferentiateSplit())) {
            markDestinationsRebased(additionalTBR, additionalTBROnDiffPtr, origin, arrowSymbolTable);
        }
        if (TapEnv.traceCurAnalysis()) {
            if (additionalTBR != null) {
                TapEnv.printlnOnTrace("      TBR:" + infoToStringWithDiffPtr(additionalTBR, nDZcommon, additionalTBROnDiffPtr, nDZcommonOnDiffPtr));
            }
            if (phase == TOPDOWN_2 && TapEnv.doRecompute() && additionalRecomp != null) {
                TapEnv.printlnOnTrace("      Available recomputations:");
                for (int k = 0; k < additionalRecomp.length; ++k) {
                    if (additionalRecomp[k] != null) {
                        TapEnv.printlnOnTrace((k > 9 ? "        " : "         ") + k + " >> " + additionalRecomp[k]);
                    }
                }
            }
        }
        // Recompute propagated additionalTBR in the special case where we are exiting from an II-LOOP
        // because an II-LOOP is checkpointed and therefore the tbr propagation rule is different:
        // Note: if piece of code C in U;C;D is *not* checkpointed, tbrD is computed by:
        //  tbrD := (tbr + use(C')) \ (kill(C) if adj_live(C,D))
        // whereas if C is checkpointed (with the standard Lazy snapshot rules),
        //  tbrD := (tbr + use(Cb)) \ out(C)
        if (additionalTBR!=null && origin instanceof HeaderBlock && origin.instructions!=null
            && curArrow==origin.getFGArrowTestCase(FGConstants.LOOP, FGConstants.EXIT)
            && ((HeaderBlock)origin).headInstr().hasDirective(Directive.IILOOP) != null) {
            TapPair<BoolVector, BoolVector> useCb = curActivity.fragmentDiffLivenessesCkp(origin) ;
            TapPair<BoolVector, BoolVector> rwC = curUnit.fragmentInOutCkp(origin) ;
            if (rwC!=null) {
                TapList<FGArrow> flowToII = origin.backFlow() ;
                BoolVector tbrBeforeII = new BoolVector(nDZcommon) ;
                BoolVector tbrBeforeIIOnDiffPtr = new BoolVector(nDZcommonOnDiffPtr) ;
                while (flowToII!=null) {
                    if (!flowToII.head.isCyclingArrow()) {
                        retrievedFromOrig = tbrsDown.retrieve(flowToII.head.origin);
                        if (retrievedFromOrig!=null) {
                            if (retrievedFromOrig.first!=null) {
                                tbrBeforeII.cumulOr(retrievedFromOrig.first) ;
                            }
                            if (retrievedFromOrig.second!=null) {
                                tbrBeforeIIOnDiffPtr.cumulOr(retrievedFromOrig.second) ;
                            }
                        }
                    }
                    flowToII = flowToII.tail ;
                }
                BoolVector newAdditionalTBR = tbrBeforeII.copy() ;
                newAdditionalTBR.cumulOr(useCb!=null ? useCb.first : rwC.first) ;
                newAdditionalTBR.cumulMinus(rwC.second) ;
                BoolVector newAdditionalTBROnDiffPtr = tbrBeforeIIOnDiffPtr.copy() ;
                // Cannot use cumulOr and cumulMinus because rwC is not on diff pointers => "pedestrian" implementation:
                for (int iptr=0 ; iptr<nDZcommonOnDiffPtr ; ++iptr) {
                    int i = arrowSymbolTable.declaredZoneInfo(iptr, SymbolTableConstants.PTRKIND)
                                                  .kindZoneNb(SymbolTableConstants.ALLKIND) ;
                    if (rwC.second.get(i)) {
                        newAdditionalTBROnDiffPtr.set(iptr, false) ;
                    } else if (useCb!=null ? useCb.second.get(iptr) : rwC.first.get(i)) {
                        newAdditionalTBROnDiffPtr.set(iptr, true) ;
                    }
                }

//                 additionalTBR = newAdditionalTBR ;
//                 additionalTBROnDiffPtr = newAdditionalTBROnDiffPtr ;
                // We could replace additionalTBR with the new (see above), but we prefer to cumulAnd (see below).
                // In principle, newAdditionalTBR is always smaller than additionalTBR, but in practice maybe not...
                additionalTBR.cumulAnd(newAdditionalTBR, nDZcommon) ;
                additionalTBROnDiffPtr.cumulAnd(newAdditionalTBROnDiffPtr, nDZcommonOnDiffPtr) ;

                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("      TBR recomputed because origin "+origin+" is checkpointed") ;
                    TapEnv.printlnOnTrace("      TBR:" + infoToStringWithDiffPtr(additionalTBR, nDZcommon, additionalTBROnDiffPtr, nDZcommonOnDiffPtr));
                }
            }
        }
        return (additionalTBR!=null || (phase == TOPDOWN_2 && TapEnv.doRecompute() && additionalRecomp!=null)) ;
    }

    /**
     * Accumulates the TBR info present in the "additionalTBR" global
     * variable into the global variable "tbrTmp", which contains the
     * non-cycle-related TBR info.
     * The "commonSymbolTable" determines which zones are common
     * between the origin block and the destination Block.
     * For the other zones, the info must vanish.
     * Must initialize the tbrTmp and recompTmp global variables if
     * either is null, meaning "additionalXXX" is the first accumulated info.
     */
    @Override
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        if (tbrTmp == null) {
            tbrTmp = new BoolVector(nDZ);
        }
        if (tbrTmpOnDiffPtr == null) {
            tbrTmpOnDiffPtr = new BoolVector(nDPZ);
        }
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        int commonLengthOnDiffPtr = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
        if (additionalTBR != null) {
            tbrTmp.cumulOr(additionalTBR, commonLength);
        }
        if (additionalTBROnDiffPtr != null) {
            tbrTmpOnDiffPtr.cumulOr(additionalTBROnDiffPtr, commonLengthOnDiffPtr);
        }
        if (phase == TOPDOWN_2 && TapEnv.doRecompute() && additionalRecomp != null) {
            recompTmp = recompCumulOr(recompTmp, additionalRecomp, commonLength);
        }
    }

    /**
     * Same as cumulValueWithAdditional(), but for "cycle-related" info.
     * Must initialize the tbrTmpCycle variable if it is is null.
     */
    @Override
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        if (tbrTmpCycle == null) {
            tbrTmpCycle = new BoolVector(nDZ);
        }
        if (tbrTmpCycleOnDiffPtr == null) {
            tbrTmpCycleOnDiffPtr = new BoolVector(nDPZ);
        }
        int commonLength = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        int commonLengthOnDiffPtr = commonSymbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
        if (additionalTBR != null) {
            tbrTmpCycle.cumulOr(additionalTBR, commonLength);
        }
        if (additionalTBROnDiffPtr != null) {
            tbrTmpCycleOnDiffPtr.cumulOr(additionalTBROnDiffPtr, commonLengthOnDiffPtr);
        }
        if (phase == TOPDOWN_2 && TapEnv.doRecompute() && additionalRecomp != null) {
            recompTmpCycle = recompCumulOr(recompTmpCycle, additionalRecomp, commonLength);
        }
    }

    /**
     * Compares tbrTmp info arriving to the exit of this curBlock
     * with same info stored here at the previous sweep,
     * found in tbrsDown.retrieve(curBlock).
     * If new info not null and old info is null or different,
     * must update the stored info with (possibly a copy of) the
     * arriving info, and must return "true" for "modified".
     * If "curBlock" is a loop header and we have a special treatment
     * for cycles, i.e. if runSpecialCycleAnalysis(curBlock) is "true",
     * comparison is done also on the tbrTmpCycle value, compared with
     * the stored tbrsCycleDown.retrieve(curBlock).
     */
    @Override
    protected boolean compareDownstreamValues() {
        boolean tbrChanged =
                compareWithStorage(tbrTmp, tbrTmpOnDiffPtr, tbrTmpCycle, tbrTmpCycleOnDiffPtr, nDZ, nDPZ,
                        tbrsDown, tbrsCycleDown);
        boolean recompChanged = false;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            recompChanged = compareWithStorage(recompTmp, recompTmpCycle, recompsDown, recompsCycleDown);
        }
        return tbrChanged || recompChanged;
    }

    /**
     * Compares tbrTmp info arriving to the entry of this curBlock with same
     * info stored here at the previous sweep, found in tbrsUp.retrieve(curBlock).
     * If new info not null and old info is null or different,
     * must update the stored info with (possibly a copy of) the arriving info,
     * and must return "true" for "modified".
     * If "curBlock" is a loop header and we have a special treatment for cycles,
     * i.e. if runSpecialCycleAnalysis(curBlock) is "true",
     * comparison is done also on the tbrTmpCycle value, compared with
     * the stored tbrsCycleUp.retrieve(curBlock).
     */
    @Override
    protected boolean compareUpstreamValues() {
        boolean tbrChanged =
                compareWithStorage(tbrTmp, tbrTmpOnDiffPtr, tbrTmpCycle, tbrTmpCycleOnDiffPtr, nDZ, nDPZ,
                        tbrsUp, tbrsCycleUp);
        boolean recompChanged = false;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
            recompChanged = compareWithStorage(recompTmp, recompTmpCycle, recompsUp, recompsCycleUp);
        }
        return tbrChanged || recompChanged;
    }

    /**
     * Special case of compareWithStorage for recompInfo's.
     *
     * @return true if the new info is different from the previous one.
     * In that case, replaces the old info with the new.
     */
    private boolean compareWithStorage(ADTBRAnalyzer.RecompInfo[] newInfo, ADTBRAnalyzer.RecompInfo[] newInfoCycle,
                                       BlockStorage<ADTBRAnalyzer.RecompInfo[]> storedInfos,
                                       BlockStorage<ADTBRAnalyzer.RecompInfo[]> storedInfosCycle) {
        boolean modified = false;
        ADTBRAnalyzer.RecompInfo[] oldInfo = storedInfos.retrieve(curBlock);
        if (newInfo != null && (oldInfo == null || !equalRecompInfos(newInfo, oldInfo))) {
            modified = true;
            storedInfos.store(curBlock, recompCopy(newInfo));
        }
        if (runSpecialCycleAnalysis(curBlock)) {
            oldInfo = storedInfosCycle.retrieve(curBlock);
            if (newInfoCycle != null && (oldInfo == null || !equalRecompInfos(newInfoCycle, oldInfo))) {
                modified = true;
                storedInfosCycle.store(curBlock, recompCopy(newInfoCycle));
            }
        }
        return modified;
    }

    /**
     * Propagates the TBR info tbrTmp forward through "curBlock", upstream.
     * Must return "false" iff the
     * control doesn't reach the curBlock exit (e.g. because of a "stop").
     * If "curBlock" is a loop header and we have a special treatment for cycles,
     * i.e. if runSpecialCycleAnalysis(curBlock) is "true",
     * propagation must also propagate the tbrTmpCycle value.
     * The propagation can modify tbrTmp and tbrTmpCycle if needed,
     * it is not necessary to copy them.
     */
    @Override
    protected boolean propagateValuesForwardThroughBlock() {
        return propagateAndStoreThroughBlock(curBlock, null, null);
    }

    /**
     * When "tbrRecordList" is non-null, fill it with the intermediate tbrTmp.
     * When "recompRecordList" is non-null, fill it with the intermediate recomputation lists
     */
    private boolean propagateAndStoreThroughBlock(Block block,
                                                  TapList<TapPair<BoolVector, BoolVector>> tbrRecordList,
                                                  TapList<TapPair<TapList<Instruction>,TapList<Instruction>>> recompRecordList) {
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("   === Going down through Block " + block + " ===");
        }
        TapList<Instruction> instructions = block.instructions;
        TapList<TapPair<BoolVector, BoolVector>> diffLivenessesList = null;
        if (curUnitDiffLivenesses != null) {
            diffLivenessesList = curUnitDiffLivenesses.retrieve(block);
        }
        TapList<TapPair<BoolVector, BoolVector>> diffOverwritesList = null;
        if (curUnitDiffOverwrites != null) {
            diffOverwritesList = curUnitDiffOverwrites.retrieve(block);
        }
        TapPair<BoolVector, BoolVector> diffLivenessesBefore;
        TapPair<BoolVector, BoolVector> diffOverwritesBefore;
        TapPair<BoolVector, BoolVector> diffLivenessesAfter =
            (diffLivenessesList == null ? new TapPair(null, null) : diffLivenessesList.head) ;
        TapPair<BoolVector, BoolVector> diffOverwritesAfter =
            (diffOverwritesList == null ? new TapPair(null, null) : diffOverwritesList.head) ;
        setUniqueAccessZones(block);
        // If there is a special treatment for cycling data:
        if (runSpecialCycleAnalysis(block)) {
            curInstruction = instructions.head;
            Tree doTree = curInstruction.tree.down(3);
            // Make tbrTmp sweep through the initial read
            // of the a DO loop's "from" expression:
            if (tbrTmp != null) {
                tbrAndRecomputabilityThroughExpression(doTree.down(2), tbrTmp, tbrTmpOnDiffPtr, recompTmp,
                        diffOverwritesList == null ? null : diffOverwritesList.tail.head.first,
                        new ToBool(true));
                Tree doIndexTree = doTree.down(1);
                ToBool total = new ToBool(false);
                TapIntList indexZones =
                        ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(doIndexTree, total,
                                curInstruction, null), true);
                if (requiredZones(indexZones, tbrTmp)) {
                    setTBRindexOnLoopEntry(doIndexTree);
                }
                // Also, the loop index is now overwritten, so we must remove all available recomputations that use it:
                if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                    BoolVector zonesNotUsableInRecompute = new BoolVector(nDZ);
                    zonesNotUsableInRecompute.set(indexZones, true);
                    fixpointDependents(recompTmp, zonesNotUsableInRecompute,
                            cleanLoopIndexZones.retrieve(block), diffOverwritesAfter.first, tbrTmp, nDZ);
                    if (recompTmpCycle != null) {
                        fixpointDependents(recompTmpCycle, zonesNotUsableInRecompute,
                                cleanLoopIndexZones.retrieve(block), diffOverwritesAfter.first, tbrTmp, nDZ);
                    }
                }
            }
            // Compute the tbrTmpCycle that will be
            // propagated downstream towards the loop's cycling arrows.
            // It is the arriving tbrTmp PLUS only the arriving
            // tbrTmpCycle which are not localized in the loop.
            // When the do-loop index is overwritten during the loop
            // (which is dirty style), then a special mark is set on the
            // loop index and the arriving tbrTmpCycle is reset to false for
            // the loop index. [See: overwrittenLoopIndex] cf nonRegrF77:lha13
            if (tbrTmpCycle != null) {
                if (findBlockLoopKindIsDoLoop(block)) {
                    if (((HeaderBlock) block).dirtyOverwrittenIndex) {
                        setTBRindexOnLoopCycle(doTree.down(1));
                    }
                    ToBool total = new ToBool(false);
                    TapIntList writtenZonesList =
                            ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(doTree.down(1), total, curInstruction, null), true);
                    if (total.get()) {
                        tbrTmpCycle.set(writtenZonesList, false);
                        if (phase == TOPDOWN_2 && TapEnv.doRecompute() && recompTmpCycle != null) {
                            setRecomputationInfo(recompTmpCycle, writtenZonesList, null);
                        }
                    }
                }
                BoolVector tbrTmpCycleMemo = tbrTmpCycle.copy();
                BoolVector tbrTmpCycleMemoOnDiffPtr = tbrTmpCycleOnDiffPtr.copy();
                tbrTmpCycle.set(((HeaderBlock) block).localizedZones, false);
                ADTBRAnalyzer.RecompInfo[] recompTmpCycleMemo = null;
                TapIntList loopLocalZones = ((HeaderBlock) block).localizedZones;
                if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                    recompTmpCycleMemo = recompCopy(recompTmpCycle);
                    if (recompTmpCycle != null) {
                        while (loopLocalZones != null) {
                            //Set this zone's RecompInfo to the neutral element for OR:
                            recompTmpCycle[loopLocalZones.head] = new ADTBRAnalyzer.RecompInfo(null, null);
                            loopLocalZones = loopLocalZones.tail;
                        }
                    }
                }
                if (tbrTmp != null) {
                    tbrTmpCycle.cumulOr(tbrTmp);
                }
                if (tbrTmpOnDiffPtr != null) {
                    tbrTmpCycleOnDiffPtr.cumulOr(tbrTmpOnDiffPtr);
                }
                if (phase == TOPDOWN_2 && TapEnv.doRecompute() && recompTmp != null) {
                    recompTmpCycle = recompCumulOr(recompTmpCycle, recompTmp);
                }
                // Update the tbrTmp that will be propagated downstream through
                // the loop's exiting arrows. It is the arriving "tbrTmpCycle", PLUS the part of
                // the arriving "tbrTmp" defined by "directEntryExitMask()" (i.e. either the loop may run 0 times,
                // or the zone has been treated as localized by the loop body and has been fully accessed by the
                // loop and will not be "localized" at the above level).
                if (tbrTmp == null) {
                    tbrTmp = tbrTmpCycleMemo;
                    tbrTmpOnDiffPtr = tbrTmpCycleMemoOnDiffPtr;
                    recompTmp = recompTmpCycleMemo;
                } else {
                    tbrTmp.cumulAnd(directEntryExitMask((HeaderBlock) block, nDZ, SymbolTableConstants.ALLKIND));
                    tbrTmp.cumulOr(tbrTmpCycleMemo);
                    tbrTmpOnDiffPtr.cumulAnd(directEntryExitMask((HeaderBlock) block, nDPZ, SymbolTableConstants.PTRKIND));
                    tbrTmpOnDiffPtr.cumulOr(tbrTmpCycleMemoOnDiffPtr);
                    if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                        recompTmp = recompCumulOr(recompTmp, recompTmpCycleMemo);
                    }
                }
            } else {
                tbrTmpCycle = tbrTmp.copy();
                tbrTmpCycleOnDiffPtr = tbrTmpOnDiffPtr.copy();
                if (phase == TOPDOWN_2 && TapEnv.doRecompute()) {
                    recompTmpCycle = recompCopy(recompTmp);
                }
            }
        } else if (block instanceof HeaderBlock && block.isADoLoop()) {
            // if block starts by a "loop do i=....", but it can't be analyzed with
            // the "SpecialCycleAnalysis", then the tbrTmpCycle is certainly null
            // and the tbrTmp contains all arriving tbr.
            curInstruction = instructions.head;
            Tree doIndexTree = curInstruction.tree.down(3).down(1);
            ToBool total = new ToBool(false);
            TapIntList indexZones =
                    ZoneInfo.listAllZones(curSymbolTable.treeOfZonesOfValue(doIndexTree, total,
                            curInstruction, null), true);
            if (tbrTmp != null &&
                    (requiredZones(indexZones, tbrTmp) ||
                            ((HeaderBlock) block).dirtyOverwrittenIndex
                                    // refinement cf F90:v474 : if do-loop header is dead, then loop is dead, then no dirty write inside!
                                    && DiffLivenessAnalyzer.isTreeOrSubtreeRequiredInDiff(curActivity, curInstruction.tree, true))) {
                setTBRindexOnLoopEntry(doIndexTree);
                setTBRindexOnLoopCycle(doIndexTree);
            }
        }

        TapPair<TapList<Instruction>, TapList<Instruction>> recomputationList;
        ToBool arrives = new ToBool(true);
        ToBool arrivesC = new ToBool(true);
        TapPair<BoolVector, BoolVector> lastChanceZones;
        // Make tbrTmp (if non null) and possibly also tbrTmpCycle (if non null),
        //  and also recompTmp sweep downstream "block".
        while (instructions != null) {
            curInstruction = instructions.head;
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("      TBR=" + infoToStringWithDiffPtrWithCycle(tbrTmp, nDZ, tbrTmpOnDiffPtr, nDPZ, tbrTmpCycle, tbrTmpCycleOnDiffPtr));
            }
            // Now is when the algorithm may decide to insert a recomputation:
            diffLivenessesBefore = diffLivenessesAfter;
            if (diffLivenessesList != null) {
                diffLivenessesList = diffLivenessesList.tail;
                if (diffLivenessesList != null) {
                    diffLivenessesAfter = diffLivenessesList.head ;
                }
            }
            diffOverwritesBefore = diffOverwritesAfter;
            if (diffOverwritesList != null) {
                diffOverwritesList = diffOverwritesList.tail;
                if (diffOverwritesList != null) {
                    diffOverwritesAfter = diffOverwritesList.head ;
                }
            }
            if (phase == TOPDOWN_2 && TapEnv.doRecompute()
                    && recompTmp != null && recompTmpCycle == null
                    && diffLivenessesAfter.first != null) {
                lastChanceZones = new TapPair<>(diffLivenessesAfter.first.copy(), diffLivenessesAfter.second) ;
                // Select (and record) recomputation list immediately before (upstream) this curInstruction:
                recomputationList = selectAvailableRecomputations(diffOverwritesBefore, diffLivenessesBefore, lastChanceZones) ;
                if (recompRecordList != null) {
                    recompRecordList = recompRecordList.placdl(recomputationList) ;
                }
            }
            if (tbrRecordList != null) {
                // Record TBR immediately before (upstream) this curInstruction:
                BoolVector recordInfo = tbrTmp.copy();
                if (tbrTmpCycle != null) {
                    recordInfo.cumulOr(tbrTmpCycle);
                }
                BoolVector recordInfoOnDiffPtr = tbrTmpOnDiffPtr.copy();
                if (tbrTmpCycleOnDiffPtr != null) {
                    recordInfoOnDiffPtr.cumulOr(tbrTmpCycleOnDiffPtr);
                }
                tbrRecordList = tbrRecordList.placdl(new TapPair<>(recordInfo, recordInfoOnDiffPtr));
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("    Now going downstream across instruction:" + ILUtils.toString(curInstruction.tree));
            }
            if (tbrTmp != null) {
                tbrAndRecomputabilityThroughExpression(curInstruction.tree, tbrTmp, tbrTmpOnDiffPtr, recompTmp,
                        diffOverwritesAfter.first, arrives);
            }
            if (tbrTmpCycle != null) {
                tbrAndRecomputabilityThroughExpression(curInstruction.tree, tbrTmpCycle, tbrTmpCycleOnDiffPtr, recompTmpCycle,
                        diffOverwritesAfter.first, arrivesC);
            }
            instructions = instructions.tail;
        }
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("      TBR=" + infoToStringWithDiffPtrWithCycle(tbrTmp, nDZ, tbrTmpOnDiffPtr, nDPZ, tbrTmpCycle, tbrTmpCycleOnDiffPtr));
            TapEnv.printlnOnTrace("    Now at the end of Block:" + block);
        }
        curInstruction = null;
        if (phase == TOPDOWN_2 && TapEnv.doRecompute()
                && recompTmp != null && recompTmpCycle == null
                && diffLivenessesAfter.first != null) {
            lastChanceZones = new TapPair<>(new BoolVector(nDZ), new BoolVector(nDPZ)) ;
            //It is the last chance for all zones thet are going to vanish out of scope,
            // and for ALL zones if this block can jump to the exitBlock:
            TapList<FGArrow> arrows = block.flow();
            boolean goesToExit = false;
            int minDZ = nDZ;
            int minDPZ = nDPZ;
            while (arrows != null) {
                if (arrows.head.destination == curUnit.exitBlock) {
                    goesToExit = true;
                }
                int commonDZ = arrows.head.commonSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
                if (commonDZ < minDZ) {
                    minDZ = commonDZ;
                }
                int commonDPZ = arrows.head.commonSymbolTable().declaredZonesNb(SymbolTableConstants.PTRKIND);
                if (commonDPZ < minDPZ) {
                    minDPZ = commonDPZ;
                }
                arrows = arrows.tail;
            }
            if (goesToExit) {
                lastChanceZones.first.setTrue();
                lastChanceZones.second.setTrue();
            } else {
                for (int i = minDZ; i < nDZ; ++i) {
                    lastChanceZones.first.set(i, true);
                }
                for (int i = minDPZ; i < nDPZ; ++i) {
                    lastChanceZones.second.set(i, true);
                }
            }
            // Select (and record) recomputation list at the end of this Block:
            recomputationList = selectAvailableRecomputations(diffOverwritesAfter, diffLivenessesAfter, lastChanceZones) ;
            if (recompRecordList != null) {
                recompRecordList = recompRecordList.placdl(recomputationList) ;
            }
        }
        if (tbrRecordList != null) {
            // Record TBR at the end of this Block:
            BoolVector recordInfo = tbrTmp.copy();
            if (tbrTmpCycle != null) {
                recordInfo.cumulOr(tbrTmpCycle);
            }
            BoolVector recordInfoOnDiffPtr = tbrTmpOnDiffPtr.copy();
            if (tbrTmpCycleOnDiffPtr != null) {
                recordInfoOnDiffPtr.cumulOr(tbrTmpCycleOnDiffPtr);
            }
            tbrRecordList = tbrRecordList.placdl(new TapPair<>(recordInfo, recordInfoOnDiffPtr));
        }
        uniqueAccessZones = null;
        return arrives.get() && arrivesC.get();
    }

    /** Final placement decision for available recomputations.
     * Looks into recompTmp to select the available recomputations that must be placed at the current location,
     * because they compute a TBR value for which there is a recomputation available and this is the last chance
     * to use this recomputation instruction before it becomes obsolete.
     * When an available recomputation is selected. it is added to the returned list of instructions
     * and removed from recompTmp. tbrTmp and tbrTmpOnDiffPtr may be modified accordingly.
     * @return the list of recomputation instructions that will be placed "here". */
    TapPair<TapList<Instruction>, TapList<Instruction>> selectAvailableRecomputations(
                          TapPair<BoolVector, BoolVector> diffOverwrites,
                          TapPair<BoolVector, BoolVector> diffLivenesses,
                          TapPair<BoolVector, BoolVector> lastChanceZones) {
//[llh 22/06/22] TODO: also select recomputations "onDiffPtr" !
        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("      Available recomputations are:");
            for (int k = 0; k < recompTmp.length; ++k) {
                if (recompTmp[k] != null) {
                    TapEnv.printlnOnTrace((k > 9 ? "        " : "         ") + k + " >> " + recompTmp[k]);
                }
            }
            TapEnv.printlnOnTrace(
                "       overWritten=" + infoToStringWithDiffPtr(diffOverwrites.first, nDZ, diffOverwrites.second, nDPZ)
                + " diffLiveness=" + infoToStringWithDiffPtr(diffLivenesses.first, nDZ, diffLivenesses.second, nDPZ)
                + " lastChance=" + infoToStringWithDiffPtr(lastChanceZones.first, nDZ, lastChanceZones.second, nDPZ));
        }
        TapList<Instruction> recomputationListPrimal = null, recomputationListOnDiffPtr = null ;
        TapIntList recomp3z = null ;
        while ((recomp3z = placeRecomputationHere(recompTmp, diffOverwrites, diffLivenesses, lastChanceZones))
               != null) {
            int recompRk = recomp3z.head ;
            int primalZ = recomp3z.tail.head ;
            Instruction recompInstr = recompTmp[recompRk].instruction ;
            tbrTmp.cumulOr(recompTmp[recompRk].neededAvailableZones);
            if (primalZ!=-1) {
                tbrTmp.set(primalZ, false);
                recomputationListPrimal = new TapList<>(recompInstr, recomputationListPrimal);
                recompTmp[recompRk].usedForPrimal = true ;
            }
            int diffPtrZ = -1 ;
            // aboutAPointer is true when this recomputation may be about a diffPtr.
            boolean aboutAPointer = (recomp3z.tail.tail!=null) ;
            if (aboutAPointer) {
                diffPtrZ = recomp3z.tail.tail.head ;
                if (diffPtrZ!=-1) {
                    tbrTmpOnDiffPtr.set(diffPtrZ, false);
                    recomputationListOnDiffPtr = new TapList<>(recompInstr, recomputationListOnDiffPtr);
                    recompTmp[recompRk].usedForDiffPtr = true ;
                }
            }
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("  Place recomputation here: " + ILUtils.toString(recompInstr.tree)
                                      + (aboutAPointer ? (primalZ==-1 ? "" : ", for primal") + (diffPtrZ==-1 ? "" : ", for diffPtr") : ""));
                TapEnv.printlnOnTrace("    => and TBR becomes:" + infoToStringWithDiffPtr(tbrTmp, nDZ, tbrTmpOnDiffPtr, nDPZ));
            }
            BoolVector zonesNotUsableInRecompute = new BoolVector(nDZ);
            zonesNotUsableInRecompute.set(recompRk, true);
            fixpointDependents(recompTmp, zonesNotUsableInRecompute,
                               cleanLoopIndexZones.retrieve(curBlock), diffOverwrites.first, tbrTmp, nDZ);
            if (recompTmp[recompRk].usedForPrimal &&
                (recompTmp[recompRk].usedForDiffPtr || !aboutAPointer)) {
                recompTmp[recompRk] = null;
            }
        }
        return new TapPair<>(TapList.nreverse(recomputationListPrimal), TapList.nreverse(recomputationListOnDiffPtr)) ;
    }

    /**
     * We choose to place the available recomputation for declared zone z
     * (i.e. recompInfos[z]) here downstream the current curInstruction iff
     * <p>
     * 1) there is an available recomputation instruction for z (recompInfos[z]), and<br>
     * 2) z is TBR downstream current instruction, and<br>
     * 3) z may be overwritten from here to the end of the current CKP, and<br>
     * 4) z is adjoint dead downstream current instruction, and<br>
     * 5) either (a) there is no other waiting recomputation in recompInfos that needs z,
     *        or (b) this is the last chance to recompute z because next stmt
     *               is live and overwrites it or z is falling out of scope.
     * @return null if no recomputation placed here, otherwise a list of two or three integers:(
     *   the rank of the chosen recomputation in recompInfos ;
     *   if recomputation of primal value, the standard index of this value (same as the "rank" above), else -1 ;
     *   if recomputation of pointer diff, the pointer index of this value, else -1 )
     */
    private TapIntList placeRecomputationHere(ADTBRAnalyzer.RecompInfo[] recompInfos,
                                              TapPair<BoolVector, BoolVector> diffOverwrites,
                                              TapPair<BoolVector, BoolVector> diffLivenesses,
                                              TapPair<BoolVector, BoolVector> lastChanceZones) {
        TapIntList result = null ;
        int indexFound = -1 ;
        int rkFound = -1 ;
        int rkFoundOnDiffPtr = -1 ;
        int indexPtr = -1 ;
        for (int index = recompInfos.length-1 ; indexFound==-1 && index>=0 ; --index) {
            indexPtr = -1 ;
            if (recompInfos[index] != null) {
                if (tbrTmp.get(index)
                    && diffOverwrites.first.get(index)
                    && !diffLivenesses.first.get(index)
                    && (!otherNeeds(recompInfos, index)
                        ||
                        (lastChanceZones.first != null && lastChanceZones.first.get(index)))) {
                    indexFound = index ;
                    rkFound = index ;
                }
                if (index>=0) {
                    ZoneInfo zi = curSymbolTable.declaredZoneInfo(index, SymbolTableConstants.ALLKIND) ;
                    if (zi!=null && zi.ptrZoneNb!=-1) {
                        indexPtr = zi.ptrZoneNb ;
                    }
                }
                if (indexPtr!=-1
                    && tbrTmpOnDiffPtr.get(indexPtr)
                    && diffOverwrites.second.get(indexPtr)
                    && !diffLivenesses.second.get(indexPtr)
                    && (!otherNeeds(recompInfos, index)
                        ||
                        (lastChanceZones.second != null && lastChanceZones.second.get(indexPtr)))) {
                    indexFound = index ;
                    rkFoundOnDiffPtr = indexPtr ;
                }
            }
        }
        if (indexFound!=-1) {
            if (indexPtr==-1) {// TODO: restrict also when pointer is not active!
                result = new TapIntList(indexFound, new TapIntList(rkFound, null)) ;
            } else {
                result = new TapIntList(indexFound, new TapIntList(rkFound, new TapIntList(rkFoundOnDiffPtr, null))) ;
            }
        }
        return result;
    }

    /**
     * Given "tbr", the "(ToBeRecorded) variables required by adjoint"
     * BEFORE instruction Tree "instruction", this function modifies it
     * so that it finally holds this TBR information AFTER "instruction".
     * The formula used is [cf AD2004 paper]:
     * TBR(U;I) = (Use(I') UNION TBR(U)) MINUS (KILL(I) when diff-live(I,D))
     * Also sets the "TBR" annotation on sub-trees of "instruction", about
     * variables which are overwritten and should be saved.
     * -- On an op_call, the "TBR" annotation holds a TapPair containing the
     * "snp" and "sbk" snapshots, first on the call arguments, second on the call globals.
     * -- On the op_ident of some loop index, the "TBR" annotation holds the
     * info whether the loop index must be saved, first on the loop entry, second on the loop cycle.
     * -- On any other variable reference which is overwritten (e.g. lhs or written argument of an IO call)
     * the "TBR" annotation holds an array of 2 booleans, the first true when
     * the written variable must be saved, the second when the diff of the
     * written variable (which is a pointer) must be saved.
     */
    private void tbrAndRecomputabilityThroughExpression(Tree instrTree, BoolVector tbr, BoolVector tbrOnDiffPtr,
                                                        ADTBRAnalyzer.RecompInfo[] recomp, BoolVector diffOverwritesAfter, ToBool arrives) {
        TapIntList writtenZonesList = null;
        //[llh] TODO: extract the following test to try and make it identical
        // in its 2 locations here and in BlockDifferentiator "test adjoint requires instr"
        // adjRequiresInstr=true iff the original instruction must be present in the (fwd sweep of the) diff code:
        boolean adjRequiresInstr =
            TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
            ||
            !TapEnv.removeDeadPrimal()
            ||
            TapEnv.diffLivenessAnalyzer() == null
            ||
            isTreeRequiredInDiff(curActivity, instrTree, false);

        ADTBRAnalyzer.RecompInfo newRecompInfo = null;
        if (instrTree.opCode() == ILLang.op_return && !ILUtils.isNullOrNone(instrTree.down(1))) {
            // temporarily transform the return into an assignment:
            // child 1 is the returned expression (C style).
            String returnVarName = curUnit.otherReturnVar() == null ? curUnit.name() : curUnit.otherReturnVar().symbol;
            Tree assignReturn =
                    ILUtils.build(ILLang.op_assign,
                            ILUtils.build(ILLang.op_ident, returnVarName),
                            ILUtils.copy(instrTree.down(1)));
            //If the returned value is annotated active,
            // then this equivalent assignment must be annotated active too.
            if (ADActivityAnalyzer.isAnnotatedActive(curActivity, instrTree.down(1), curSymbolTable)) {
                ADActivityAnalyzer.setAnnotatedActive(curActivity, assignReturn.down(1));
            }
            instrTree = assignReturn;
        }
        int opCode = instrTree.opCode();
        Tree callResult = null;
        Tree functionCall = null;
        ToBool total = new ToBool(false);
        Unit calledUnit = null;
        if (opCode == ILLang.op_call) {
            functionCall = instrTree;
        } else if (opCode == ILLang.op_assign
                && instrTree.down(2).opCode() == ILLang.op_call) {
            functionCall = instrTree.down(2);
            callResult = instrTree.down(1);
        }
        if (functionCall != null) {
            calledUnit = getCalledUnit(functionCall, curSymbolTable);
        }
        if (calledUnit != null &&
                !calledUnit.hasPredefinedDerivatives()) {
            // This is a call to a non-intrinsic function or subroutine F.
            // This means that there is probably a Fbar subroutine:
            if (calledUnit.unitInOutW == null) {
                // when the called unit makes a "stop", do nothing and say we don't arrive at the end:
                // NewInOut: ce cas ne doit plus se produire
                arrives.set(false);
            } else {
                TapList writtenZonesTree =
                        curSymbolTable.treeOfZonesOfValue(callResult, null, curInstruction, null);
                writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, true);
                CallArrow arrow = CallGraph.getCallArrow(curUnit, calledUnit);
                Tree[] actualParams = ILUtils.getArguments(functionCall).children();
                int nbArgs = actualParams.length;
                boolean[] argsTotal = new boolean[nbArgs + 1];
                TapList[] argsZones = new TapList[nbArgs + 1];
                Tree argTree;
                // Copy tbr before it gets modified by the TBR analysis
                // of the actual call arguments. The non-modified TBR
                // is used as the "Req" info i.e. the TBR before the call.
                BoolVector tbrUpstreamCall = tbr.copy();
                BoolVector tbrUpstreamCallOnDiffPtr = tbrOnDiffPtr.copy();
                for (int i = nbArgs; i >= 0; --i) {
                    argTree = i == nbArgs ? callResult : actualParams[i];
                    if (argTree != null) {
                        boolean argHasDiff =
                                ADActivityAnalyzer.isAnnotatedActive(curActivity, argTree, curSymbolTable)
                                        || ReqExplicit.isAnnotatedPointerActive(curActivity, argTree);
                        argsZones[i] =
                                findRequiredZonesAndTBR(argTree, true, argHasDiff, tbr, tbrOnDiffPtr, total, curSymbolTable);
                        //The result target variable is assigned (e.g. y := F(X))
                        // therefore only the variable is modified and not its pointed destinations:
                        if (i != nbArgs) {
                            argsZones[i] = TapList.copyTree(argsZones[i]);
                            includePointedElementsInTree(
                                    argsZones[i], total, null, true,
                                    curSymbolTable, curInstruction, false, true);
                        }
                        argsTotal[i] = referenceIsTotal(total.get(), argsZones[i]);
                    } else {
                        argsZones[i] = null;
                        argsTotal[i] = false;
                    }
                }
                // Update the "recomputability" info: all vars written by the call become non-recomputable:
                if (phase == TOPDOWN_2 && TapEnv.doRecompute() && recomp != null) {
                    BoolVector functionW = new BoolVector(nDZ);
                    TapList[] outCArgs =
                        translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), functionW, null, null, true,
                                                      functionCall, curInstruction, arrow, SymbolTableConstants.ALLKIND,
                                                      null, null, false, false) ;
                    for (int i = nbArgs; i >= 0; i--) {
                        setInfoBoolTreeToExtendedDeclaredZones(functionW, argsZones[i], true,
                                (i==nbArgs ? new TapList<>(Boolean.TRUE, null) : outCArgs[i+1]),
                                true, SymbolTableConstants.ALLKIND);
                    }
                    setZonesNonRecomputable(recomp, functionW,
                            cleanLoopIndexZones.retrieve(curBlock), diffOverwritesAfter, tbr, nDZ);
                }

                // For a function call e.g. x=F(y,z), adjRequiresInstr must be true even if the
                // assignment to x is not required, in the case where y or z are overwritten and required:
                if (isTreeRequiredInDiff(curActivity, functionCall, false)) {
                    adjRequiresInstr = true;
                }
                // An MPI call is always required in the FWD sweep
                if (MPIcallInfo.isMessagePassingFunction(calledUnit.name(), calledUnit.language())) {
                    adjRequiresInstr = true;
                }

                ActivityPattern calledActivity =
                        (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(functionCall, curActivity, "multiActivityCalleePatterns");
                BoolVector sbk = new BoolVector(nDZ);
                BoolVector sbkOnDiffPtr = new BoolVector(nDPZ);
                TapList[] sbkArgs = new TapList[nbArgs + 1];
                TapList[] sbkArgsOnDiffPtr = new TapList[nbArgs + 1];
                BoolVector snp = new BoolVector(nDZ);
                BoolVector snpOnDiffPtr = new BoolVector(nDPZ);
                TapList[] snpArgs = new TapList[nbArgs + 1];
                TapList[] snpArgsOnDiffPtr = new TapList[nbArgs + 1];
                BoolVector tbrC = new BoolVector(nDZ);
                BoolVector tbrCOnDiffPtr = new BoolVector(nDPZ);
                TapList[] tbrCArgs = new TapList[nbArgs+1];
                TapList[] tbrCArgsOnDiffPtr = new TapList[nbArgs+1];
                BoolVector tbrD = new BoolVector(nDZ);
                BoolVector tbrDOnDiffPtr = new BoolVector(nDPZ);
                BoolVector popped = new BoolVector(nDZ);
                BoolVector poppedOnDiffPtr = new BoolVector(nDPZ);
                computeCheckpointingSetsWithDiffPtrForCall(
                        tbr, tbrOnDiffPtr,
                        tbrUpstreamCall, tbrUpstreamCallOnDiffPtr,
                        snp, snpOnDiffPtr, snpArgs, snpArgsOnDiffPtr,
                        sbk, sbkOnDiffPtr, sbkArgs, sbkArgsOnDiffPtr,
                        tbrC, tbrCOnDiffPtr, tbrCArgs, tbrCArgsOnDiffPtr,
                        tbrD, tbrDOnDiffPtr,
                        popped, poppedOnDiffPtr,
                        arrow, functionCall,
                        argsZones, argsTotal,
                        adjRequiresInstr);
                // TBR requirements upon return from called function:
                tbr.setCopy(tbrD);
                tbrOnDiffPtr.setCopy(tbrDOnDiffPtr);
                if (phase == TOPDOWN_2) {
                    // Attach the snapshot and snapshot-back info for BlockDifferentiator:
                    TapList[][] tbrOnArgs = {snpArgs, snpArgsOnDiffPtr, sbkArgs, sbkArgsOnDiffPtr};
                    BoolVector[] tbrOnGlobs = {snp, snpOnDiffPtr, sbk, sbkOnDiffPtr};
                    ActivityPattern.setAnnotationForActivityPattern(
                            functionCall, curActivity, "TBR", new TapPair(tbrOnArgs, tbrOnGlobs));
                    markDestinationsRebased(popped, poppedOnDiffPtr);
                    // Acumulate into the TBR requirements at called function's entry:
                    if (calledActivity != null && !calledUnit.isIntrinsic()) {
                        BoolVector calledContext = calledActivity.adTBRTopDownContext();
                        if (calledContext != null &&
                                calledContext.cumulOrGrows(
                                        translateCallSiteDataToCallee(tbrC, tbrCArgs,
                                                                      null, null, true,
                                                                      functionCall, curInstruction, arrow,
                                                                      SymbolTableConstants.ALLKIND),
                                        calledUnit.publicZonesNumber(SymbolTableConstants.ALLKIND))) {
                            calledUnit.analysisIsOutOfDateDown = true;
                        }
                        BoolVector calledContextOnDiffPtr = calledActivity.adTBRTopDownContextOnDiffPtr();
                        if (calledContextOnDiffPtr != null &&
                                calledContextOnDiffPtr.cumulOrGrows(
                                        translateCallSiteDataToCallee(tbrCOnDiffPtr, tbrCArgsOnDiffPtr,
                                                                      null, null, true,
                                                                      functionCall, curInstruction, arrow,
                                                                      SymbolTableConstants.PTRKIND),
                                        calledUnit.publicZonesNumber(SymbolTableConstants.PTRKIND))) {
                            calledUnit.analysisIsOutOfDateDown = true;
                        }
                    }
                }
                if (callResult != null && adjRequiresInstr) {
                    // TBR analysis of return variable:
                    TapIntList resultZones = ZoneInfo.listAllZones(argsZones[nbArgs], true);
                    boolean mustRestore = requiredZones(resultZones, tbr);
                    boolean mustRestoreOnDiffPtr = requiredZonesOnDiffPtr(resultZones, tbrOnDiffPtr);
                    if (phase == TOPDOWN_2) {
                        ActivityPattern.setAnnotationForActivityPattern(
                                callResult, curActivity, "TBR", new boolean[]{mustRestore, mustRestoreOnDiffPtr});
                        markDestinationsRebased(resultZones, mustRestore, mustRestoreOnDiffPtr);
                    }
                    if (argsTotal[nbArgs]) {
                        tbr.set(resultZones, false);
                        setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, resultZones, false);
                    }
                }
            }
        } else if (opCode == ILLang.op_ioCall) {
            Tree expr;
            ToBool writtenTotal = new ToBool(false);
            boolean isIORead = ILUtils.isIORead(instrTree);
            Tree[] exprs = instrTree.down(3).children();
            BoolVector zonesOverwritten = new BoolVector(nDZ);
            TapList exprWrittenZonesTree;
            TapIntList exprWrittenZonesList;
            for (int i = exprs.length - 1; i >= 0; --i) {
                expr = exprs[i];
                if (isIORead) {
                    exprWrittenZonesTree =
                            curSymbolTable.treeOfZonesOfValue(expr, writtenTotal, curInstruction, null);
                    exprWrittenZonesList = ZoneInfo.listAllZones(exprWrittenZonesTree, false);
                    boolean mustRestore = requiredZones(exprWrittenZonesList, tbr);
                    if (phase == TOPDOWN_2) {
                        ActivityPattern.setAnnotationForActivityPattern(
                                expr, curActivity, "TBR", new boolean[]{mustRestore, false});
                        markDestinationsRebased(exprWrittenZonesList, mustRestore, false);
                        if (TapEnv.doRecompute() && recomp != null) {
                            zonesOverwritten.set(exprWrittenZonesList, true);
                        }
                    }
                    if (adjRequiresInstr && referenceIsTotal(writtenTotal.get(), exprWrittenZonesTree)) {
                        tbr.set(exprWrittenZonesList, false);
                    }
                    writtenZonesList = TapIntList.union(writtenZonesList, exprWrittenZonesList);
                }
                tbrAndRecomputabilityThroughExpression(expr, tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
            }
            if (phase == TOPDOWN_2 && TapEnv.doRecompute() && recomp != null) {
                // Update the "recomputability" info: all vars overwritten by the IO-call become non-recomputable !
                setZonesNonRecomputable(recomp, zonesOverwritten,
                        cleanLoopIndexZones.retrieve(curBlock), diffOverwritesAfter, tbr, nDZ);
            }
        } else if (opCode == ILLang.op_assign) {
            // This is a standard assignment:
            Tree lhs = instrTree.down(1);
            Tree rhs = instrTree.down(2);
            TapIntList valueZonesList;
            TapList writtenZonesTree = curSymbolTable.treeOfZonesOfValue(lhs, total, curInstruction, null);
            writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, false);
            // only to check that lhs is of differentiable type:
            TapIntList writtenDiffKindVectorIndices =
                    mapZoneRkToKindZoneRk(writtenZonesList, TapEnv.diffKind());
            // Even for an instruction dead for the forward sweep,
            // we look for its variables needed later in the reverse sweep.
            if (ADActivityAnalyzer.isAnnotatedActive(curActivity, lhs, curSymbolTable)
                || (writtenDiffKindVectorIndices!=null && TapEnv.debugAdMode()==TapEnv.ADJ_DEBUG)) {
                if (!TapEnv.doTBR() && TapEnv.get().dontSaveUnused) {
                    // if we don't want to apply TBR optim, i.e. we don't
                    // restrict to saving variables that are used in the
                    // derivatives, but nevertheless we want to restrict to
                    // variables that are used in the original instruction at
                    // least! In particular, if the original program is clean,
                    // this prevents saving uninitialized variables.
                    valueZonesList =
                            ZoneInfo.listAllZones(findRequiredZonesAndTBR(rhs, true, true, tbr, tbrOnDiffPtr, null, curSymbolTable), true);
                    tbr.set(valueZonesList, true);
                    setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, valueZonesList, true);
                    findRequiredZonesAndTBR(lhs, false, true, tbr, tbrOnDiffPtr, null, curSymbolTable);
                } else {
                    // Otherwise standard behavior, i.e. mark as tbr only those variables (and diff pointers)
                    // which will effectively be used in the differentiated statements:
                    WrapperTypeSpec assignedType = curSymbolTable.typeOf(lhs);
                    if (assignedType != null && assignedType.containsAPointer()) {
                        // if this is a pointer assignment, a priori there will be no adjoint code in the bwd sweep
                        // so nothing to be added in tbr.
                        // (except if pointer recomputation is triggered, but this must not be done here)
                    } else {
                        // else standard assignment: we must study what its adjoint will look like:
                        ToInt lhsDiffUsage = new ToInt(0);
                        if (ADActivityAnalyzer.isAnnotatedActive(curActivity, rhs, curSymbolTable)) {
                            collectZonesUsedByDiffRhs(rhs, lhs, lhsDiffUsage, false, true, tbr, tbrOnDiffPtr);
                        }
                        if (lhsDiffUsage.get() != 1) {
                            collectAllDiffExpression(lhs, tbr, tbrOnDiffPtr);
                        }
                    }
                }
            }
            boolean mustRestore = requiredZones(writtenZonesList, tbr);
            boolean mustRestoreOnDiffPtr = requiredZonesOnDiffPtr(writtenZonesList, tbrOnDiffPtr);
            boolean adjRequiresInstrOnDiffPtr =
                TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
                ||
                !TapEnv.removeDeadPrimal()
                ||
                TapEnv.diffLivenessAnalyzer() == null
                ||
                isTreeRequiredInDiff(curActivity, instrTree, true);
            // Detect initialization cases. Do not push/pop the contents of lhs before initialization:
            Boolean isInitialization = (Boolean) lhs.getAnnotation("IsInitialization");
            if (isInitialization != null && isInitialization) {
                if (TapEnv.traceCurAnalysis()) {
                    TapEnv.printlnOnTrace("  Assignment " + ILUtils.toString(instrTree) + " is an initialization: no restoration needed.");
                }
                mustRestore = false;
                mustRestoreOnDiffPtr = false;
            }
            if (phase == TOPDOWN_2) {
                ActivityPattern.setAnnotationForActivityPattern(
                        lhs, curActivity, "TBR", new boolean[]{mustRestore, mustRestoreOnDiffPtr});
                markDestinationsRebased(writtenZonesList, mustRestore, mustRestoreOnDiffPtr);
                if (TapEnv.doRecompute() && recomp != null) {
                    // If we allow for recomputation of cheap intermediate variables, first
                    //  remove all recomputables that use the variable which is gonna be overwritten ...
                    BoolVector zonesOverwritten = new BoolVector(nDZ);
                    zonesOverwritten.set(writtenZonesList, true);
                    setZonesNonRecomputable(recomp, zonesOverwritten,
                            cleanLoopIndexZones.retrieve(curBlock), diffOverwritesAfter, tbr, nDZ);
                    if (total.get() && curInstruction.getWhereMask() == null
                        && writtenZonesList != null && writtenZonesList.tail == null
                        && (adjRequiresInstr || adjRequiresInstrOnDiffPtr)
                        && approxRecomputeCost(instrTree) < 20) {
                        BoolVector usedZones = curSymbolTable.zonesUsedByExp(instrTree, curInstruction);
                        int writtenZone = writtenZonesList.head;
                        // ... then if this assignment is total, writes into a single zone, is diff-live, and is cheap ...
                        newRecompInfo = new ADTBRAnalyzer.RecompInfo(curInstruction, usedZones);
                        if (!usedZones.get(writtenZone) &&
                                newRecompInfo.isValidNow(recomp, cleanLoopIndexZones.retrieve(curBlock),
                                        diffOverwritesAfter, tbr, nDZ)) {
                            // ... and if each zone used to recompute this assignment is either
                            // a surrounding clean loop index, or unmodified, or recomputable, or
                            // already TBR, then register this assignment as a possible recomputation:
                            setRecomputationInfo(recomp, writtenZone, newRecompInfo);
                            ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, curInstruction, 1, adjRequiresInstr);
                            ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, curInstruction, 2, adjRequiresInstrOnDiffPtr);
                            if (TapEnv.traceCurAnalysis()) {
                                TapEnv.printlnOnTrace("      New Available recomp: " + writtenZone + " >> " + newRecompInfo);
                            }
                        } else {
                            ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, curInstruction, 1, false);
                            ActivityPattern.setInstructionBoolInfo(curUnitMayBeRecomputeds, curInstruction, 2, false);
                            newRecompInfo = null;
                        }
                    }
                }
            }
            if (mustRestore || mustRestoreOnDiffPtr) {
                // The restoration POP statement itself will require all subexpressions inside lhs:
                collectAllPrimalExpression(lhs, tbr);
            }
            // If the lhs is fully overwritten, reset its TBR status to false:
            if (adjRequiresInstr && referenceIsTotal(total.get(), writtenZonesTree)) {
                tbr.set(writtenZonesList, false);
                setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, writtenZonesList, false);
            }
            // Quite on the contrary, in our model an allocation of "P" will cause
            // a ADMM_register[Shadowed](P[, P']), so the lhs must be set TBR immediately:
            if (rhs.opCode() == ILLang.op_allocate) {
                valueZonesList =
                        ZoneInfo.listAllZones(findRequiredZonesAndTBR(lhs, true, true, tbr, tbrOnDiffPtr, null, curSymbolTable), true);
                tbr.set(valueZonesList, true);
                if (ReqExplicit.isAnnotatedPointerActive(curActivity, lhs)) {
                    setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, valueZonesList, true);
                }
            }
        } else if (opCode == ILLang.op_do) {
            tbrAndRecomputabilityThroughExpression(instrTree.down(2), tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
            tbrAndRecomputabilityThroughExpression(instrTree.down(3), tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
            tbrAndRecomputabilityThroughExpression(instrTree.down(4), tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
            Tree index = instrTree.down(1);
            TapList writtenZonesTree = curSymbolTable.treeOfZonesOfValue(index, total, curInstruction, null);
            writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, true);
            // If the loop index is fully overwritten, reset its TBR status to false:
            if (adjRequiresInstr && referenceIsTotal(total.get(), writtenZonesTree)) {
                tbr.set(writtenZonesList, false);
            }
            // Mark loop bounds TBR if they are not automatically saved
            // and the loop is not dead.
            for (int i = 2; i <= 4; ++i) {
                Tree bound = instrTree.down(i);
                if (InOutAnalyzer.isCheapDuplicableConstant(bound, curBlock, curInstruction)) {
                    BoolVector usedZones = curBlock.symbolTable.zonesUsedByExp(bound, curInstruction);
                    tbr.cumulOr(usedZones);
                }
            }
        } else if (opCode == ILLang.op_where) {
            TapIntList valueZonesList =
                    ZoneInfo.listAllZones(findRequiredZonesAndTBR(instrTree.down(1), true, false, tbr, null, total, curSymbolTable), true);
            tbr.set(valueZonesList, true);
        } else if (opCode == ILLang.op_iterativeVariableRef) {
            // Implements a special treatment for the op_do in the 2nd son.
            tbrAndRecomputabilityThroughExpression(instrTree.down(1), tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
            // Marks the do-index TBR if needed
            Tree index = instrTree.down(2).down(1);
            TapList writtenZonesTree = curSymbolTable.treeOfZonesOfValue(index, total, curInstruction, null);
            writtenZonesList = ZoneInfo.listAllZones(writtenZonesTree, false);
            if (requiredZones(writtenZonesList, tbr) && phase == TOPDOWN_2) {
                ActivityPattern.setAnnotationForActivityPattern(
                        index, curActivity, "TBR", new boolean[]{true, false});
                markDestinationsRebased(writtenZonesList, true, false);
            }
            tbrAndRecomputabilityThroughExpression(instrTree.down(2), tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
        } else if (opCode == ILLang.op_deallocate) {
            // With our AD model, a deallocate(exp) will give birth to a exp = ADMM_unregister[Shadowed](P[, P'])
            // therefore addresses and indices needed to evaluate exp become TBR :
            TapIntList valueZonesList =
                    ZoneInfo.listAllZones(findRequiredZonesAndTBR(instrTree.down(1), true, true, tbr, tbrOnDiffPtr, total, curSymbolTable), true);
            // on the other hand, the deallocated value itself (and its diff if pointer) becomes non-TBR:
            if (total.get()) {
                tbr.set(valueZonesList, false);
                setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, valueZonesList, false);
            }
        } else if (opCode == ILLang.op_if) {
            // The test inside an if is certainly a simple enough expression with no differentiable computation
            // (otherwise it would have been split out). Moreover it will not be repeated in the backward sweep
            // because we are going to push/pop the control instead. So there is no need to run TBR through the test.
            // TODO: run TBR on the test iff it is going to be recomputed/duplicated instead of control-stored
        } else {
            // otherwise there is no adjoint to build for this instruction,
            // but there may be assignments or calls inside it.
            Tree[] sons = instrTree.children();
            if (sons != null) {
                for (int i = sons.length - 1; i >= 0; --i) {
                    tbrAndRecomputabilityThroughExpression(sons[i], tbr, tbrOnDiffPtr, null, diffOverwritesAfter, arrives);
                }
            }
        }
        //Special bookkeeping about "recomputation" possibilities:
        // If this curInstruction must be present in the adjoint fwd sweep and it is not going
        // to be recomputed in the bwd sweep, then for all zone z that it uses,
        // any available recomputation for z must be kept in the adjoint fwd sweep.
        // And recursively for all zones used by this recomputation...
        if (phase == TOPDOWN_2 && TapEnv.doRecompute() && recomp != null
                && adjRequiresInstr && newRecompInfo == null) {
            BoolVector usedZones = curSymbolTable.zonesUsedByExp(instrTree, curInstruction);
            // When Instruction is guarded by masks, one must also
            //  look for zones used in the mask expressions:
            InstructionMask curMask = curInstruction.getWhereMask();
            while (curMask != null) {//loop through potentially nested where conditions
                usedZones.cumulOr(curSymbolTable.zonesUsedByExp(curMask.getControlTree(), curInstruction));
                curMask = curMask.enclosingMask;
            }
            setInstructionRemainsInFwdSweepOnZones(recomp, usedZones, 1);
        }
        // In the degenerate "alwaysSave" noOptim case (i.e. no full TBR,
        // AND we neither want to restrict TBR saving to used variables) we must
        // add into tbr every variable overwritten by the current instruction,
        // so that we avoid saving a variable that has not been at least initialized.
        if (!TapEnv.doTBR() && !TapEnv.get().dontSaveUnused) {
            tbr.set(writtenZonesList, true);
            setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, writtenZonesList, true);
        }
    }

    /* Sweeps through "expression", which is the active rhs of an active assignment "lhs := expression",
     * to find all variables that will be needed to evaluate its (actually adjoint-) derivative.
     * Adds the zones of these needed variables to the given "tbr" vector of zones
     * (and to "tbrOnDiffPtr" for needed diff pointers).
     * Special treatment for the "lhs": returns in lhsDiffUsage an integer representing
     * the future usage of the diff of "lhs" (i.e. lhsb) in the diff statements:
     * 1 means that the only diff statement is the no-op instruction "lhsb := lhsb"
     * 0 means that there will be at least one diff statement "lhsb := 0.0"
     * 999 means that there will be diff statements that use lhsb.
     * This method must be equivalent to (but quicker than) computing the adjoint code of the assignment
     * (as in ExpressionDifferentiator.java) and then collecting all zones used in this adjoint code.
     * @param expression the rhs of the containing assignment, of recursively one of its subtrees.
     * By construction, we assume that "expression" is active i.e., it has a derivative.
     * @param lhs the lhs of the containing assignment.
     * @param lhsDiffUsage container for the future usage of "lhsb"
     * @param allPrimal true when "expression" is already totally needed for the derivative expression,
     * and therefore it is needless to say that any sub-expression is itself needed. Suggested init: false.
     * @param partialIsOne true when the partial derivative of the starting, full expression
     * wrt the current expression is "1", implying that if current expression is the "lhs",
     * the final diff on the lhs may be a no-op instruction "lhsB := lhsB". Suggested init: true. */
    private void collectZonesUsedByDiffRhs(Tree expression, Tree lhs, ToInt lhsDiffUsage,
                                           boolean allPrimal, boolean partialIsOne,
                                           BoolVector tbr, BoolVector tbrOnDiffPtr) {
        // Choice to make "partialIsOne" a simple boolean is weak, e.g. it doesn't
        // see that the diff on x of "x := a - (b - x)" is a no-op "xb := xb".
        switch (expression.opCode()) {
            case ILLang.op_call: {
                Unit calledUnit = getCalledUnit(expression, curSymbolTable);
                AtomFuncDerivative funcDerivative = (AtomFuncDerivative) calledUnit.diffInfo;
                Tree[] paramExprs = ILUtils.getArguments(expression).children();
                if ("sum".equals(calledUnit.name()) || "spread".equals(calledUnit.name())) {
                    // mark as TBR all variables used in DIM, MASK, or SIZE:
                    for (int i = paramExprs.length - 1; i > 0; --i) {
                        collectAllPrimalExpression(paramExprs[i], tbr);
                    }
                }
                // Special for classic conversion functions: they have a predefined diff code built in
                // ExpressionDifferentiator, that uses their argument, but they have an empty
                // "paramExprs" in their predefined funcDerivetive in the "lib" file: we must force all
                // sub-expressions of (the diff of) their argument(s) to be TBR! cf set11/lh047
                if (funcDerivative!=null
                    && (curUnit.isFortran()
                        ? ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                new String[]{"complex", "cmplx", "dcmplx", "sngl", "dble",
                                             "real", "dreal", "aimag", "dimag", "conjg", "dconjg"},
                                false)
                        : ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                new String[]{"creal", "cimag", "conj"},
                                true))) {
                    collectZonesUsedByDiffRhs(paramExprs[0], lhs, lhsDiffUsage, allPrimal,
                                              false, tbr, tbrOnDiffPtr);
                    int doneIndex = 0 ;
                    if (paramExprs.length>=2
                        && ILUtils.isIdentOf(ILUtils.getCalledName(expression),
                                             new String[]{"complex", "cmplx", "dcmplx"}, false)) {
                        collectZonesUsedByDiffRhs(paramExprs[1], lhs, lhsDiffUsage, allPrimal,
                                              false, tbr, tbrOnDiffPtr);
                        doneIndex = 1 ;
                    }
                    for (int i=doneIndex+1 ; i<paramExprs.length ; ++i) {
                        collectAllPrimalExpression(paramExprs[i], tbr);
                    }
                }
                if (funcDerivative != null) {
                    funcDerivative.matches(expression);
                    paramExprs = funcDerivative.getParamExprs();
                }
                for (int i = paramExprs.length - 1; i >= 0; --i) {
                    boolean exprAllPrimal = allPrimal;
                    if (!allPrimal) {
                        if (funcDerivative == null) {
                            collectAllPrimalExpression(paramExprs[i], tbr);
                            exprAllPrimal = true;
                        } else {
                            collectAllPrimalExpression(funcDerivative.buildPartialDerivative(i), tbr);
                        }
                    }
                    if (ADActivityAnalyzer.isAnnotatedActive(curActivity, paramExprs[i], curSymbolTable)) {
                        collectZonesUsedByDiffRhs(paramExprs[i], lhs, lhsDiffUsage, exprAllPrimal,
                                                  false, tbr, tbrOnDiffPtr);
                    }
                }
                break;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_ident:
                if (1 == ILUtils.eqOrDisjointRef(expression, lhs, curInstruction, curInstruction)) {
                    lhsDiffUsage.set(lhsDiffUsage.get() == 0 && partialIsOne ? 1 : 999);
                } else {
                    lhsDiffUsage.set(999);
                    collectAllDiffExpression(expression, tbr, tbrOnDiffPtr);
                }
                break;
            case ILLang.op_add:
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal, partialIsOne, tbr, tbrOnDiffPtr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, allPrimal, partialIsOne, tbr, tbrOnDiffPtr);
                }
                break;
            case ILLang.op_sub:
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal, partialIsOne, tbr, tbrOnDiffPtr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                }
                break;
            case ILLang.op_mul: {
                boolean active1 =
                        ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable);
                boolean active2 =
                        ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable);

                if (active1) {
                    if (!allPrimal) {
                        collectAllPrimalExpression(expression.down(2), tbr);
                    }
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal || active2, false, tbr, tbrOnDiffPtr);
                }
                if (active2) {
                    if (!allPrimal) {
                        collectAllPrimalExpression(expression.down(1), tbr);
                    }
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, allPrimal || active1, false, tbr, tbrOnDiffPtr);
                }
                break;
            }
            case ILLang.op_div: {
                boolean active1 =
                        ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable);
                boolean active2 =
                        ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable);
                if (!allPrimal) {
                    collectAllPrimalExpression(expression.down(2), tbr);
                }
                if (active1) {
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal || active2, false, tbr, tbrOnDiffPtr);
                }
                if (active2) {
                    if (!allPrimal) {
                        collectAllPrimalExpression(expression.down(1), tbr);
                    }
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, true, false, tbr, tbrOnDiffPtr);
                }
                break;
            }
            case ILLang.op_power:
            case ILLang.op_mod:
                if (!allPrimal) {
                    collectAllPrimalExpression(expression, tbr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, true, false, tbr, tbrOnDiffPtr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, true, false, tbr, tbrOnDiffPtr);
                }
                break;
            case ILLang.op_minus:
            case ILLang.op_unary:
                collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                break;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_nameEq:
                collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                break;
            case ILLang.op_arrayConstructor: {
                Tree[] expressions = expression.children();
                for (int i = expressions.length - 1; i >= 0; --i) {
                    collectZonesUsedByDiffRhs(expressions[i], lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                }
                break;
            }
            case ILLang.op_ifExpression:
                if (!allPrimal) {
                    collectAllPrimalExpression(expression.down(1), tbr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(1), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(1), lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                }
                if (ADActivityAnalyzer.isAnnotatedActive(curActivity, expression.down(2), curSymbolTable)) {
                    collectZonesUsedByDiffRhs(expression.down(2), lhs, lhsDiffUsage, allPrimal, false, tbr, tbrOnDiffPtr);
                }
                break;
            case ILLang.op_sizeof:
            case ILLang.op_label:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_strings:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_neq:
            case ILLang.op_eq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_complexConstructor:
            case ILLang.op_address:
            case ILLang.op_allocate:
            case ILLang.op_modifiedType:
                break;
            default:
                TapEnv.toolWarning(-1, "(Expressions used in adjoint) Unexpected operator: " + expression.opName() + " in " + ILUtils.toString(expression));
                break;
        }
    }

    /**
     * Accumulates into "tbr" all the zones needed to evaluate "expression".
     */
    private void collectAllPrimalExpression(Tree expression, BoolVector tbr) {
        TapIntList valueZonesList =
                ZoneInfo.listAllZones(findRequiredZonesAndTBR(expression, true, false,
                        tbr, null, null, curSymbolTable),
                        true);
        tbr.set(valueZonesList, true);
    }

    /**
     * Accumulates into "tbrOnDiffPtr" all the diff pointer zones needed
     * to evaluate the diff of "expression", and in addition accumulates
     * into "tbr" all the primal zones needed to evaluate this diff.
     */
    private void collectAllDiffExpression(Tree expression, BoolVector tbr, BoolVector tbrOnDiffPtr) {
        TapIntList valueZonesList =
                ZoneInfo.listAllZones(findRequiredZonesAndTBR(expression, false, true,
                        tbr, tbrOnDiffPtr, null, curSymbolTable),
                        true);
        setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, valueZonesList, true);
    }

    /**
     * Computes all sets necessary for checkpointing the given call
     * "callResTree" := "callTree", corresponding to "callArrow".
     * cf paper "The Data-Flow Equations of Checkpointing in reverse Automatic Differentiation".
     * Given the incoming TBR, computes Sbk, Snp, TBRD, and TBRC.
     */
    private void computeCheckpointingSetsWithDiffPtrForCall(
            BoolVector tbr, BoolVector tbrOnDiffPtr,
            BoolVector tbrUC, BoolVector tbrUCOnDiffPtr,
            BoolVector snp, BoolVector snpOnDiffPtr, TapList[] snpArgs, TapList[] snpArgsOnDiffPtr,
            BoolVector sbk, BoolVector sbkOnDiffPtr, TapList[] sbkArgs, TapList[] sbkArgsOnDiffPtr,
            BoolVector tbrC, BoolVector tbrCOnDiffPtr, TapList[] tbrCArgs, TapList[] tbrCArgsOnDiffPtr,
            BoolVector tbrD, BoolVector tbrDOnDiffPtr,
            BoolVector popped, BoolVector poppedOnDiffPtr,
            CallArrow callArrow, Tree callTree,
            TapList[] argsZones, boolean[] argsTotal,
            boolean adjRequiresCall) {
        Unit calledUnit = callArrow.destination;
        // When true, this call is differentiated in split mode, therefore not really checkpointed,
        //  but we still have a couple of "checkpointing sets" to compute:
        boolean thisCallIsSplitAdj = !curInstruction.thisCallIsCheckpointed(calledUnit) ;
        // Only for non-split mode:
        // if true (default) use "lazy snapshot" strategy else "eager snapshot". Cf paper.
        boolean useLazySnapshot = true;
        // true when this call is active, i.e. has a differentiated call.
        boolean thisCallIsActive =
                ADActivityAnalyzer.isAnnotatedActive(curActivity, callTree, curSymbolTable);
        // true when the diff of the callee may accept to handle those of the callee's inputs
        // that are TBR, for instance by push/pop'ing these input values when they are overwritten.
        // This is true if the callee is passive (why?!) or we have its source.
        // TODO: might also be true for an external if the end-used promises to preserve incoming TBR's
        boolean calleeAcceptsTBR = !thisCallIsActive || !calledUnit.isExternal();

        ActivityPattern calledActivity =
                (ActivityPattern) ActivityPattern.getAnnotationForActivityPattern(
                        callTree, curActivity, "multiActivityCalleePatterns");
        // If the call is active and it is not forced to be "no-checkpointed <=> SPLIT"
        //  set maybeCheckpointed to true for the called Unit:
        if (thisCallIsActive) {
            if (thisCallIsSplitAdj) {
                calledUnit.maybeNoCheckpointed = true;
            } else {
                calledUnit.maybeCheckpointed = true;
            }
        }
        Tree[] argTrees = ILUtils.getArguments(callTree).children();
        int nbArgs = argTrees.length;

        // Initialize pwC "possibly written C" (only when necessary later)
        BoolVector pwC = null;
        BoolVector pwCOnDiffPtr = null;
        TapList[] pwCArgs = null;
        if (calledUnit.unitInOutW!=null) {
            pwC = new BoolVector(nDZ);
            pwCArgs = translateCalleeDataToCallSite(calledUnit.unitInOutPossiblyW(), pwC, null, null, true,
                                                    callTree, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                                    null, null, false, false) ;
            pwCOnDiffPtr = curSymbolTable.focusToKind(pwC, SymbolTableConstants.PTRKIND) ;
            for (int i=nbArgs ; i>0 ; --i) {
                // In case argument is aliased with a another global param, extend the signature
                //  of the argument with the info on the global param:
                pwCArgs[i] = TapList.cumulWithOper(pwCArgs[i],
                                     buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], pwC,
                                             null, SymbolTableConstants.ALLKIND, curSymbolTable),
                                     SymbolTableConstants.CUMUL_OR);
                // If PASS-BY-VALUE (e.g. C), actual arguments cannot be "out" if not through reference:
                if (callArrow.takesArgumentByValue(i)
                    // On a F->C CallArrow, the info on the wrapper pointer was removed by translateCalleeDataToCallSite()!
                    && !(callArrow.origin.isFortran() && callArrow.destination.isC())) {
                    eraseInfoOnTopLevel(pwCArgs[i], Boolean.FALSE);
                }
            }
        }
        // Initialize tbrArgs (only when necessary later)
        TapList[] tbrArgs = new TapList[nbArgs];
        TapList[] tbrArgsOnDiffPtr = new TapList[nbArgs];
        if (!useLazySnapshot || !calleeAcceptsTBR) {
            for (int i = nbArgs - 1; i >= 0; --i) {
                tbrArgs[i] =
                        buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                tbr, null, SymbolTableConstants.ALLKIND, curSymbolTable);
                tbrArgsOnDiffPtr[i] =
                        buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                tbrOnDiffPtr, null, SymbolTableConstants.PTRKIND, curSymbolTable);
            }
        }

        // Initialize tbrUCArgs (only when necessary later)
        TapList[] tbrUCArgs = new TapList[nbArgs];
        TapList[] tbrUCArgsOnDiffPtr = new TapList[nbArgs];
        if (useLazySnapshot && !calleeAcceptsTBR) {
            for (int i = nbArgs - 1; i >= 0; --i) {
                tbrUCArgs[i] =
                        buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                tbrUC, null, SymbolTableConstants.ALLKIND, curSymbolTable);
                tbrUCArgsOnDiffPtr[i] =
                        buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                tbrUCOnDiffPtr, null, SymbolTableConstants.PTRKIND, curSymbolTable);
            }
        }

        // Initialize killC and tbrF (only when necessary later)
        // tbrF is the additional TBR intrinsically required by the called function.
        BoolVector killC = null;
        BoolVector killCOnDiffPtr = null;
        BoolVector tbrF = null;
        BoolVector tbrFOnDiffPtr = null;
        if (thisCallIsSplitAdj) {
            killC = new BoolVector(nDZ);
            translateCalleeDataToCallSite(calledUnit.unitInOutCertainlyW(), killC, null, argTrees, true,
                                          callTree, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                          null, null, false, true) ;
            killCOnDiffPtr = curSymbolTable.focusToKind(killC, SymbolTableConstants.PTRKIND) ;
            tbrF = new BoolVector(nDZ);
            tbrFOnDiffPtr = new BoolVector(nDPZ);
            BoolVector localExitTBR = null ;
            BoolVector localExitTBROnDiffPtr = null ;
            if (calledActivity != null) {
                localExitTBR  = calledActivity.localExitTBR() ;
                localExitTBROnDiffPtr = calledActivity.localExitTBROnDiffPtr() ;
            }
            if (localExitTBR==null && calledUnit.isExternal()) {
                localExitTBR = calledUnit.unitTBR() ;  //given in the Lib file.
                // Otherwise, gross over-estimation: any read or written:
                if (localExitTBR==null) {
                    localExitTBR = calledUnit.unitInOutPossiblyRorW() ;
                }
                localExitTBROnDiffPtr = calledUnit.focusToKind(localExitTBR, SymbolTableConstants.PTRKIND) ;
            }
            if (localExitTBR != null) {
                translateCalleeDataToCallSite(localExitTBR, tbrF, null, argTrees, true,
                                              callTree, curInstruction, callArrow, SymbolTableConstants.ALLKIND,
                                              null, null, true, false) ;
                translateCalleeDataToCallSite(localExitTBROnDiffPtr, tbrFOnDiffPtr, null, argTrees, true,
                                              callTree, curInstruction, callArrow, SymbolTableConstants.PTRKIND,
                                              null, null, true, false) ;
            }
        }

        // Initialize useCb (only when necessary later)
        BoolVector useCb = null;
        BoolVector useCbOnDiffPtr = null;
        TapList[] useCbArgs = null;
        TapList[] useCbArgsOnDiffPtr = null;
        if (!thisCallIsSplitAdj) {
            BoolVector calleeDiffLiveness = null;
            if (TapEnv.diffLivenessAnalyzer() == null || !TapEnv.removeDeadPrimal()) {
                calleeDiffLiveness = calledUnit.unitInOutPossiblyR() ;
            } else if (calledActivity != null) {
                calleeDiffLiveness = TapEnv.diffLivenessAnalyzer().getDiffLiveness(calledActivity);
            }
            useCb = new BoolVector(nDZ);
            if (thisCallIsActive && calleeDiffLiveness != null) {
                useCbArgs = translateCalleeDataToCallSite(calleeDiffLiveness, useCb, null, null, true,
                                                          callTree, curInstruction, callArrow,
                                                          SymbolTableConstants.ALLKIND,
                                                          null, null, true, false) ;
                for (int i=nbArgs ; i>0 ; --i) {
                    // In case argument is aliased with a another global param, extend the signature
                    //  of the argument with the info on the global param:
                    useCbArgs[i] = TapList.cumulWithOper(useCbArgs[i],
                            buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], useCb,
                                    null, SymbolTableConstants.ALLKIND, curSymbolTable),
                            SymbolTableConstants.CUMUL_OR);
                }
            } else {
                useCbArgs = new TapList[nbArgs+1];
                for (int i=nbArgs ; i>0 ; --i) {
                    useCbArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], null,
                            null, SymbolTableConstants.ALLKIND, curSymbolTable);
                }
                useCbArgs[0] = null ;
            }
            BoolVector calleeDiffLivenessOnDiffPtr = null;
            if (TapEnv.diffLivenessAnalyzer() == null || !TapEnv.removeDeadPrimal()) {
                calleeDiffLivenessOnDiffPtr =
                    calledUnit.focusToKind(calledUnit.unitInOutPossiblyR(), SymbolTableConstants.PTRKIND);
            } else if (calledActivity != null) {
                calleeDiffLivenessOnDiffPtr = TapEnv.diffLivenessAnalyzer().getDiffLivenessOnDiffPtr(calledActivity);
            }
            useCbOnDiffPtr = new BoolVector(nDPZ);
            if (thisCallIsActive && calleeDiffLivenessOnDiffPtr != null) {
                useCbArgsOnDiffPtr = translateCalleeDataToCallSite(calleeDiffLivenessOnDiffPtr, useCbOnDiffPtr,
                                                                       null, null, true,
                                                                       callTree, curInstruction, callArrow,
                                                                       SymbolTableConstants.PTRKIND,
                                                                       null, null, true, false) ;
                for (int i=nbArgs ; i>0 ; --i) {
                    // In case argument is aliased with a another global param, extend the signature
                    //  of the argument with the info on the global param:
                    useCbArgsOnDiffPtr[i] = TapList.cumulWithOper(useCbArgsOnDiffPtr[i],
                            buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], useCbOnDiffPtr,
                                    null, SymbolTableConstants.PTRKIND, curSymbolTable),
                            SymbolTableConstants.CUMUL_OR);
                }
            } else {
                useCbArgsOnDiffPtr = new TapList[nbArgs+1];
                for (int i=nbArgs ; i>0 ; --i) {
                    useCbArgsOnDiffPtr[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], null,
                            null, SymbolTableConstants.PTRKIND, curSymbolTable);
                }
                useCbArgsOnDiffPtr[0] = null ;
            }
        }

        // Initialize outC
        BoolVector outC = null;
        BoolVector outCOnDiffPtr = null;
        TapList[] outCArgs = null;
        TapList[] outCArgsOnDiffPtr = null;

        outC = new BoolVector(nDZ);
        outCOnDiffPtr = new BoolVector(nDPZ);
        if ((adjRequiresCall || thisCallIsSplitAdj) && pwC!=null) {
            outC = pwC ;
            outCArgs = pwCArgs ;
            outCOnDiffPtr = pwCOnDiffPtr ;
            outCArgsOnDiffPtr = pwCArgs ;
        } else {
            outCArgs = new TapList[nbArgs+1];
            outCArgsOnDiffPtr = new TapList[nbArgs+1];
            for (int i = nbArgs - 1; i >= 0; --i) {
                outCArgs[i+1] = buildInfoBoolTreeOfDeclaredZones(argsZones[i], null,
                        null, SymbolTableConstants.ALLKIND, curSymbolTable);
                outCArgsOnDiffPtr[i+1] = buildInfoBoolTreeOfDeclaredZones(argsZones[i], null,
                        null, SymbolTableConstants.PTRKIND, curSymbolTable);
            }
            outCArgs[0] = null ;
            outCArgsOnDiffPtr[0] = null ;
        }

        // Initialize argsNonConstant (only when necessary later)
        boolean[] argsNonConstant = new boolean[nbArgs + 1];
        if (!thisCallIsSplitAdj) {
            for (int i = nbArgs; i >= 0; --i) {
                argsNonConstant[i] = ZoneInfo.listAllZones(argsZones[i], true) != null;
            }
        }

        // Initialize outCb (only when necessary later)
        BoolVector outCb = null;
        BoolVector outCbOnDiffPtr = null;
        TapList[] outCbArgs = null;
        TapList[] outCbArgsOnDiffPtr = null;
        if (!thisCallIsSplitAdj && !calleeAcceptsTBR) {
            outCb = new BoolVector(nDZ);
            outCbOnDiffPtr = new BoolVector(nDPZ);
            if (thisCallIsActive && pwC!=null) {
                //[llh] TODO: now that outB analysis is available (done by DiffLivenessAnalyzer),
                //  we should use the correct outB sets instead of approximation by the W sets.
                outCb = pwC ;
                outCbArgs = pwCArgs ;
                outCbOnDiffPtr = pwCOnDiffPtr ;
                outCbArgsOnDiffPtr = pwCArgs ;
            } else {
                outCbArgs = new TapList[nbArgs+1];
                outCbArgsOnDiffPtr = new TapList[nbArgs+1];
                for (int i = nbArgs - 1; i >= 0; --i) {
                    outCbArgs[i+1] = buildInfoBoolTreeOfDeclaredZones(
                            argsZones[i], null, null, SymbolTableConstants.ALLKIND, curSymbolTable);
                    outCbArgsOnDiffPtr[i+1] = buildInfoBoolTreeOfDeclaredZones(
                            argsZones[i], null, null, SymbolTableConstants.PTRKIND, curSymbolTable);
                }
                outCbArgs[0] = null ;
                outCbArgsOnDiffPtr[0] = null ;
            }
        }

        // Initialize outDb (only when necessary later)
        BoolVector outDb = null;
        BoolVector outDbOnDiffPtr = null;
        if (!thisCallIsSplitAdj && !useLazySnapshot) {
            if (curBlock.constantZones != null) {
                outDb = curBlock.constantZones.not();
            } else {
                outDb = callArrow.origin.unitInOutPossiblyW() ;
            }
            outDbOnDiffPtr = changeKind(outDb, nDZ, SymbolTableConstants.ALLKIND, nDPZ, SymbolTableConstants.PTRKIND, curSymbolTable);
        }

        if (TapEnv.traceCurAnalysis()) {
            TapEnv.printlnOnTrace("        On call " + callArrow);
            TapEnv.printOnTrace("          tbr  = " + infoToStringWithDiffPtr(tbr, nDZ, tbrOnDiffPtr, nDPZ) + " ");
            TapEnv.dumpArgInfoOnTrace(tbrArgs);
            TapEnv.printOnTrace(" Ptr:");
            TapEnv.dumpArgInfoOnTrace(tbrArgsOnDiffPtr);
            TapEnv.printlnOnTrace();
            if (thisCallIsSplitAdj) {
                TapEnv.printlnOnTrace("          killC= " + infoToStringWithDiffPtr(killC, nDZ, killCOnDiffPtr, nDPZ));
                TapEnv.printlnOnTrace("          tbrF = " + infoToStringWithDiffPtr(tbrF, nDZ, tbrFOnDiffPtr, nDPZ));
            }
            TapEnv.printOnTrace("          outC = " + infoToStringWithDiffPtr(outC, nDZ, outCOnDiffPtr, nDPZ) + " ");
            TapEnv.dumpArgInfoOnTrace(outCArgs);
            TapEnv.printOnTrace(" Ptr:");
            TapEnv.dumpArgInfoOnTrace(outCArgsOnDiffPtr);
            TapEnv.printlnOnTrace();
            if (!thisCallIsSplitAdj) {
                TapEnv.printOnTrace("          useCb= " + infoToStringWithDiffPtr(useCb, nDZ, useCbOnDiffPtr, nDPZ) + " ");
                TapEnv.dumpArgInfoOnTrace(useCbArgs);
                TapEnv.printOnTrace(" Ptr:");
                TapEnv.dumpArgInfoOnTrace(useCbArgsOnDiffPtr);
                TapEnv.printlnOnTrace();
                if (!useLazySnapshot || !calleeAcceptsTBR) {
                    TapEnv.printOnTrace("          outCb= " + infoToStringWithDiffPtr(outCb, nDZ, outCbOnDiffPtr, nDPZ) + " ");
                    TapEnv.dumpArgInfoOnTrace(outCbArgs);
                    TapEnv.printOnTrace(" Ptr:");
                    TapEnv.dumpArgInfoOnTrace(outCbArgsOnDiffPtr);
                    TapEnv.printlnOnTrace();
                }
                if (useLazySnapshot && !calleeAcceptsTBR) {
                    TapEnv.printOnTrace("          tbrUC= " + infoToStringWithDiffPtr(tbrUC, nDZ, tbrUCOnDiffPtr, nDPZ) + " ");
                    TapEnv.dumpArgInfoOnTrace(tbrUCArgs);
                    TapEnv.printOnTrace(" Ptr:");
                    TapEnv.dumpArgInfoOnTrace(tbrUCArgsOnDiffPtr);
                    TapEnv.printlnOnTrace();
                }
                if (!useLazySnapshot) {
                    TapEnv.printlnOnTrace("          outDb= " + infoToStringWithDiffPtr(outDb, nDZ, outDbOnDiffPtr, nDPZ));
                }
            }
        }

        computeCheckpointingSets(tbr, tbrArgs,
                outC, outCArgs,
                useCb, useCbArgs,
                outCb, outCbArgs, outDb,
                tbrUC, tbrUCArgs,
                killC, tbrF,
                snp, snpArgs,
                sbk, sbkArgs,
                tbrC, tbrCArgs,
                tbrD,
                popped,
                SymbolTableConstants.ALLKIND,
                argTrees, argsZones, argsTotal, argsNonConstant,
                adjRequiresCall, thisCallIsSplitAdj, useLazySnapshot, calleeAcceptsTBR
        );
        computeCheckpointingSets(tbrOnDiffPtr, tbrArgsOnDiffPtr,
                outCOnDiffPtr, outCArgsOnDiffPtr,
                useCbOnDiffPtr, useCbArgsOnDiffPtr,
                outCbOnDiffPtr, outCbArgsOnDiffPtr, outDbOnDiffPtr,
                tbrUCOnDiffPtr, tbrUCArgsOnDiffPtr,
                killCOnDiffPtr, tbrFOnDiffPtr,
                snpOnDiffPtr, snpArgsOnDiffPtr,
                sbkOnDiffPtr, sbkArgsOnDiffPtr,
                tbrCOnDiffPtr, tbrCArgsOnDiffPtr,
                tbrDOnDiffPtr,
                poppedOnDiffPtr,
                SymbolTableConstants.PTRKIND,
                argTrees, argsZones, argsTotal, argsNonConstant,
                adjRequiresCall, thisCallIsSplitAdj, useLazySnapshot, calleeAcceptsTBR
        );

        // When the diff of calledUnit is needed, will be written by hand, and accepts to do push/pop,
        // accumulate the TBR variables that the diff of calledUnit must preserve (by push/pops):
        if (calleeAcceptsTBR && thisCallIsActive && !calledUnit.hasSource()) {
            BoolVector newMustHandleTBR =
                translateCallSiteDataToCallee(tbrC, tbrCArgs,
                                              null, null, true,
                                              callTree, curInstruction, callArrow,
                                              SymbolTableConstants.ALLKIND) ;
            if (calledUnit.mustHandleTBR==null) {
                calledUnit.mustHandleTBR = newMustHandleTBR ;
            } else {
                calledUnit.mustHandleTBR.cumulOr(newMustHandleTBR) ;
            }
        }

        if (TapEnv.traceCurAnalysis()) {
            if (!thisCallIsSplitAdj) {
                TapEnv.printOnTrace("          >snp = " + infoToStringWithDiffPtr(snp, nDZ, snpOnDiffPtr, nDPZ) + " ");
                TapEnv.dumpArgInfoOnTrace(snpArgs);
                TapEnv.printOnTrace(" Ptr:");
                TapEnv.dumpArgInfoOnTrace(snpArgsOnDiffPtr);
                TapEnv.printlnOnTrace();
            }
            TapEnv.printOnTrace("          >sbk = " + infoToStringWithDiffPtr(sbk, nDZ, sbkOnDiffPtr, nDPZ) + " ");
            TapEnv.dumpArgInfoOnTrace(sbkArgs);
            TapEnv.printOnTrace(" Ptr:");
            TapEnv.dumpArgInfoOnTrace(sbkArgsOnDiffPtr);
            TapEnv.printlnOnTrace();
            TapEnv.printOnTrace("          >tbrC= " + infoToStringWithDiffPtr(tbrC, nDZ, tbrCOnDiffPtr, nDPZ) + " ");
            TapEnv.dumpArgInfoOnTrace(tbrCArgs);
            TapEnv.printOnTrace(" Ptr:");
            TapEnv.dumpArgInfoOnTrace(tbrCArgsOnDiffPtr);
            TapEnv.printlnOnTrace();
            TapEnv.printlnOnTrace("          >tbrD= " + infoToStringWithDiffPtr(tbrD, nDZ, tbrDOnDiffPtr, nDPZ));
        }
    }

    private void computeCheckpointingSets(BoolVector tbr, TapList[] tbrArgs,
                                          BoolVector outC, TapList[] outCArgs,
                                          BoolVector useCb, TapList[] useCbArgs,
                                          BoolVector outCb, TapList[] outCbArgs, BoolVector outDb,
                                          BoolVector tbrUC, TapList[] tbrUCArgs,
                                          BoolVector killC, BoolVector tbrF,
                                          BoolVector snp, TapList[] snpArgs,
                                          BoolVector sbk, TapList[] sbkArgs,
                                          BoolVector tbrC, TapList[] tbrCArgs,
                                          BoolVector tbrD,
                                          BoolVector poppedZones,
                                          int setKind,
                                          Tree[] argTrees, TapList[] argsZones, boolean[] argsTotal, boolean[] argsNonConstant,
                                          boolean adjRequiresCall, boolean thisCallIsSplitAdj,
                                          boolean useLazySnapshot, boolean calleeAcceptsTBR) {
        int nbArgs = argTrees.length;
        TapList argInfo;
        if (thisCallIsSplitAdj) {
            // Split mode <=> call C is not checkpointed, therefore Snp := ()
            snp.setFalse();
            for (int i = nbArgs; i >= 0; i--) {
                snpArgs[i] = null;
            }
            if (calleeAcceptsTBR) {
                // The future hand-written C_fwd/C_bwd "accept" to do push/pop to preserve the incoming tbr.
                // Therefore Sbk := (), except for the callResult.
                sbk.setFalse();
                for (int i = nbArgs; i >= 0; i--) {
                    sbkArgs[i] = null;
                }
                // tbrD := tbr \ kill(C)  +  tbrF
                tbrD.setCopy(tbr);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    if (argsNonConstant[i]) {
                        argInfo = TapList.copyTree(tbrArgs[i]);
                        setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], true, argInfo, true, setKind);
                    }
                }
                tbrD.cumulMinus(killC);
                tbrD.cumulOr(tbrF);
                // tbrC := tbr & out(C_fwd)  , with out(C_fwd) overapproximated as outC.
                // note: it seems overkill to intersect with out(C_fwd) as tbrC will
                //  cause a push/pop only at places where "C" does overwriting.
                tbrC.setCopy(tbr);
                tbrC.cumulAnd(outC);
                for (int i=nbArgs ; i>0 ; --i) {
                    tbrCArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1],
                            tbr, null, setKind, curSymbolTable);
                    tbrCArgs[i] = TapList.cumulWithOper(tbrCArgs[i], outCArgs[i], SymbolTableConstants.CUMUL_AND);
                }
            } else {
                // else we can't require C_fwd/C_bwd to handle tbr => tbrC must be empty => use Sbk !
                // Sbk := tbr & out(C_fwd)  , where out(C_fwd) is overapproximated as outC.
                sbk.setCopy(tbr);
                sbk.cumulAnd(outC);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    sbkArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                            tbr, null, setKind, curSymbolTable);
                    if (sbkArgs[i]!=null) { // may be null if actual arg is not a variable reference.
                        sbkArgs[i] = TapList.cumulWithOper(sbkArgs[i], outCArgs[i+1], SymbolTableConstants.CUMUL_AND);
                    }
                }
                // tbrD := tbr \ out(C_fwd)  +  tbrF  , with out(C_fwd) overapproximated as outC.
                tbrD.setCopy(tbr);
                tbrD.cumulMinus(outC);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    if (argsNonConstant[i]) {
                        argInfo = TapList.copyTree(tbrArgs[i]);
                        argInfo = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                        setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], true, argInfo, true, setKind);
                    }
                }
                tbrD.cumulOr(tbrF);
                // tbrC := ()
                tbrC.setFalse();
                for (int i=nbArgs ; i>0 ; --i) {
                    tbrCArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], null,
                                    null, setKind, curSymbolTable);
                }
            }
            // Special for the call's result "argument": it is overwritten here by the fwd adjoint call.
            sbkArgs[nbArgs] = buildInfoBoolTreeOfDeclaredZones(
                    argsZones[nbArgs], tbrUC, null, setKind, curSymbolTable);
            snpArgs[nbArgs] = new TapList<>(Boolean.FALSE, null);
        } else {
            if (useLazySnapshot) {
                // Standard Lazy snapshot, keep Sbk and Snp smallest
                if (calleeAcceptsTBR) {
                    // The future hand-written C_fwd/C_bwd "accept" to do push/pop to preserve the incoming tbr.
                    // tbrC := tbr
                    tbrC.setCopy(tbr);
                    for (int i=nbArgs ; i>0 ; --i) {
                        tbrCArgs[i] =
                                buildInfoBoolTreeOfDeclaredZones(argsZones[i-1],
                                        tbr, null, setKind, curSymbolTable);
                    }
                    // Sbk  := ()
                    sbk.setFalse();
                    for (int i = nbArgs; i >= 0; --i) {
                        sbkArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i], null,
                                null, setKind, curSymbolTable);
                    }
                    // Snp  := out(C) & (tbr + use(Cb))
                    snp.setCopy(useCb);
                    snp.cumulOr(tbr);
                    snp.cumulAnd(outC);
                    for (int i = nbArgs - 1; i >= 0; --i) {
                        if (argsNonConstant[i]) {
                            argInfo = TapList.copyTree(useCbArgs[i+1]);
                            argInfo = TapList.cumulWithOper(argInfo, tbrCArgs[i+1], SymbolTableConstants.CUMUL_OR);
                            snpArgs[i] = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_AND);
                        } else { //For the special case of actual parameters that are immediate constants:
                            snpArgs[i] = null;
                        }
                    }
                    // tbrD := (tbr + use(Cb)) \ out(C)
                    tbrD.setCopy(tbr);
                    tbrD.cumulOr(useCb);
                    tbrD.cumulMinus(outC);
                    for (int i = nbArgs - 1; i >= 0; --i) {
                        if (argsNonConstant[i]) {
                            argInfo = TapList.copyTree(tbrCArgs[i+1]);
                            argInfo = TapList.cumulWithOper(argInfo, useCbArgs[i+1], SymbolTableConstants.CUMUL_OR);
                            argInfo = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                            setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], true, argInfo, true, setKind);
                            //The following is in theory useless. In practice it may be useful if snpArgs was
                            // overapproximated, and therefore we can take advantage of this approx to reduce tbrD:
                            if (TapEnv.doTBR() && argsTotal[i]) {
                                setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], false, snpArgs[i], true, setKind);
                            }
                        }
                    }
                } else {
                    // else we can't require any TBR on Cb => tbrC must be empty!
                    // Sbk := tbr & out(Cb)
                    sbk.setCopy(tbr);
                    sbk.cumulAnd(outCb);
                    for (int i = nbArgs - 1; i >= 0; --i) {
                        if (argsNonConstant[i]) {
                            argInfo = TapList.copyTree(tbrUCArgs[i]);
                            sbkArgs[i] = TapList.cumulWithOper(argInfo, outCbArgs[i+1], SymbolTableConstants.CUMUL_AND);
                        } else {
                            sbkArgs[i] = null;
                        }
                    }
                    // Snp := out(C) & (tbr\out(Cb) + use(Cb))
                    snp.setCopy(tbr);
                    snp.cumulMinus(outCb);
                    snp.cumulOr(useCb);
                    snp.cumulAnd(outC);
                    for (int i = nbArgs - 1; i >= 0; --i) {
                        if (argsNonConstant[i]) {
                            argInfo = TapList.copyTree(tbrArgs[i]);
                            argInfo = TapList.cumulWithOper(argInfo, outCbArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                            argInfo = TapList.cumulWithOper(argInfo, sbkArgs[i], SymbolTableConstants.CUMUL_MINUS);
                            argInfo = TapList.cumulWithOper(argInfo, useCbArgs[i+1], SymbolTableConstants.CUMUL_OR);
                            snpArgs[i] = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_AND);
                        } else {
                            snpArgs[i] = null;
                        }
                    }
                    // tbrC := ()
                    tbrC.setFalse();
                    for (int i=nbArgs ; i>0 ; --i) {
                        tbrCArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], null,
                                null, setKind, curSymbolTable);
                    }
                    // tbrD := (use(Cb) + tbr) \ out(C)
                    tbrD.setCopy(tbr);
                    tbrD.cumulOr(useCb);
                    tbrD.cumulMinus(outC);
                    for (int i = nbArgs - 1; i >= 0; --i) {
                        if (argsNonConstant[i]) {
                            argInfo = TapList.copyTree(tbrArgs[i]);
                            argInfo = TapList.cumulWithOper(argInfo, useCbArgs[i+1], SymbolTableConstants.CUMUL_OR);
                            argInfo = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                            setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], true, argInfo, true, setKind);
                            if (TapEnv.doTBR() && argsTotal[i]) {
                                argInfo = TapList.copyTree(snpArgs[i]);
                                setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], false, argInfo, true, setKind);
                            }
                        }
                    }
                }
            } else { // Eager snapshots
                //[llh] TODO: now that outB analysis is available (done by DiffLivenessAnalyzer),
                //  we should use the correct outB sets instead of approximation by the W sets.
                // Snapshot "Eager-", make tbrC empty and tbrD small :
                TapList argInfo2;
                // Sbk  := tbr & out(Cb)
                sbk.setCopy(tbr);
                sbk.cumulAnd(outCb);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    if (argsNonConstant[i]) {
                        argInfo = TapList.copyTree(tbrArgs[i]);
                        sbkArgs[i] = TapList.cumulWithOper(argInfo, outCbArgs[i+1], SymbolTableConstants.CUMUL_AND);
                    } else {
                        sbkArgs[i] = null;
                    }
                }
                // Snp  := (out(C) & (use(Cb) + (tbr\out(Cb)))) + out(Db) & use(Cb) \ tbr \ out(C)
                BoolVector tmp2 = outDb.copy();
                tmp2.cumulAnd(useCb);
                tmp2.cumulMinus(tbr);
                tmp2.cumulMinus(outC);
                snp.setCopy(tbr);
                snp.cumulMinus(outCb);
                snp.cumulOr(useCb);
                snp.cumulAnd(outC);
                snp.cumulOr(tmp2);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    if (argsNonConstant[i]) {
                        argInfo = TapList.copyTree(tbrArgs[i]);
                        argInfo = TapList.cumulWithOper(argInfo, outCbArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                        argInfo = TapList.cumulWithOper(argInfo, sbkArgs[i], SymbolTableConstants.CUMUL_MINUS);
                        argInfo = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_AND);
                        argInfo2 = buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                outDb, null, setKind, curSymbolTable);
                        argInfo2 = TapList.cumulWithOper(argInfo2, useCbArgs[i+1], SymbolTableConstants.CUMUL_AND);
                        argInfo2 = TapList.cumulWithOper(argInfo2, tbrArgs[i], SymbolTableConstants.CUMUL_MINUS);
                        argInfo2 = TapList.cumulWithOper(argInfo2, sbkArgs[i], SymbolTableConstants.CUMUL_MINUS);
                        argInfo2 = TapList.cumulWithOper(argInfo2, outCArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                        snpArgs[i] = TapList.cumulWithOper(argInfo, argInfo2, SymbolTableConstants.CUMUL_OR);
                    } else {
                        snpArgs[i] = null;
                    }
                }
                // tbrC := ()
                tbrC.setFalse();
                for (int i=nbArgs ; i>0 ; --i) {
                    tbrCArgs[i] = buildInfoBoolTreeOfDeclaredZones(argsZones[i-1], null,
                            null, setKind, curSymbolTable);
                }
                // tbrD := out(Db) & tbr \ out(C)
                tbrD.setCopy(tbr);
                tbrD.cumulAnd(outDb);
                tbrD.cumulMinus(outC);
                for (int i = nbArgs - 1; i >= 0; --i) {
                    if (argsNonConstant[i]) {
                        argInfo = buildInfoBoolTreeOfDeclaredZones(argsZones[i],
                                outDb, null, setKind, curSymbolTable);
                        argInfo = TapList.cumulWithOper(argInfo, tbrArgs[i], SymbolTableConstants.CUMUL_AND);
                        argInfo = TapList.cumulWithOper(argInfo, outCArgs[i+1], SymbolTableConstants.CUMUL_MINUS);
                        setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], true, argInfo, true, setKind);
                        if (argsTotal[i]) {
                            argInfo = TapList.copyTree(snpArgs[i]);
                            argInfo = TapList.cumulWithOper(argInfo, sbkArgs[i], SymbolTableConstants.CUMUL_OR);
                            setInfoBoolTreeToExtendedDeclaredZones(tbrD, argsZones[i], false, argInfo, true, setKind);
                        }
                    }
                }
            }
            // Special for the call's result "argument": whatever the mode, if the instruction
            // is adjoint-live, the result arg is not read and always overwritten.
            // Therefore use(Cb) and out(Cb) are empty and out(C) is full.
            // Therefore if the result arg is in tbr, it must be in snp and it
            // must be removed from tbrD if total:
            if (adjRequiresCall && argsNonConstant[nbArgs]) {
                snpArgs[nbArgs] = buildInfoBoolTreeOfDeclaredZones(argsZones[nbArgs],
                        tbr, null, setKind, curSymbolTable);
                if (TapEnv.doTBR() && argsTotal[nbArgs]) {
                    setInfoBoolTreeToExtendedDeclaredZones(tbrD,
                            argsZones[nbArgs], false, snpArgs[nbArgs], true, setKind);
                }
            } else {
                snpArgs[nbArgs] = new TapList<>(Boolean.FALSE, null);
            }
        }
        // Compute the boolvector of all zones that will be popped.
        // This info is used to find future calls to poppointer, and therefore future needs for ADMM.
        for (int i = nbArgs; i >= 0; --i) {
            setInfoBoolTreeToExtendedDeclaredZones(poppedZones, argsZones[i], snpArgs[i], null, false, setKind);
            setInfoBoolTreeToExtendedDeclaredZones(poppedZones, argsZones[i], sbkArgs[i], null, false, setKind);
        }
        poppedZones.cumulOr(snp);
        poppedZones.cumulOr(sbk);
        // Also, if we just decided to push/pop a component of an argument (whether snpArgs or 
        // sbkArgs), the pop instruction itself will need all (probably pointer) sub-expressions
        // on the path from the argument root to the argument component.
        // Therefore these sub-expressions must now become TBR (tbrD).
        for (int i = nbArgs; i >= 0; --i) {
            TapList<?> snpArgi = snpArgs[i];
            if (i == nbArgs && snpArgi != null && snpArgi.tail == null && snpArgi.head == Boolean.FALSE) {
                snpArgi = null;
            }
            setTBRinPathToRestored(argsZones[i], snpArgi, sbkArgs[i], tbrD, setKind, new TapList<>(null, null));
        }
    }

    /**
     * Set to true in "tbr" all zones used to reach some "true" components in "path1" or "path2".
     *
     * @return true if some component of either "path1" or "path2" is true.
     */
    private boolean setTBRinPathToRestored(TapList<?> zones, TapList<?> path1, TapList<?> path2,
                                           BoolVector tbr, int kind, TapList<TapList<?>> dejaVu) {
        boolean containsTrue = false;
        if (zones != null && !TapList.contains(dejaVu, zones)) {
            if (zones.head == null || zones.head instanceof TapIntList) {
                dejaVu.placdl(zones);
                containsTrue = setTBRinPathToRestored(zones.tail,
                        (path1 == null ? null : path1.tail), (path2 == null ? null : path2.tail),
                        tbr, kind, dejaVu);
                if (containsTrue) {
                    setKindZoneRks(tbr, kind, (TapIntList) zones.head, true);
                }
                containsTrue = containsTrue
                        || TapList.oneTrueInHead(path1)
                        || TapList.oneTrueInHead(path2);
            } else {
                while (zones != null && !TapList.contains(dejaVu, zones)) {
                    dejaVu.placdl(zones);
                    if (zones.head != null) {
                        boolean trueBelow = setTBRinPathToRestored((TapList) zones.head,
                                (path1 == null ? null : (TapList) path1.head),
                                (path2 == null ? null : (TapList) path2.head),
                                tbr, kind, dejaVu);
                        containsTrue = containsTrue || trueBelow;
                    }
                    zones = zones.tail;
                    path1 = (path1 == null ? null : path1.tail);
                    path2 = (path2 == null ? null : path2.tail);
                }
            }
        }
        return containsTrue;
    }

    /**
     * Sweeps through "expression" to find out all the zones
     * whose value is required to execute it, and adds
     * these zones into the "tbr" (or "tbrOnDiffPtr" if onDiff) BoolVector by side-effect.
     *
     * @param onPrimal when true, means that the current expression will appear in the
     *                 derivatives computation (BWD sweep)
     * @param onDiff   when true, means that the derivative of the current expression
     *                 will appear in the derivatives computation (BWD sweep)
     * @return The tree of zones of the returned value (if returned value is a reference).
     * These zones of the returned value are not added into "tbr":
     * this must be done by the calling site, if necessary.
     */
    private TapList findRequiredZonesAndTBR(Tree expression, boolean onPrimal, boolean onDiff,
                                            BoolVector tbr, BoolVector tbrOnDiffPtr, ToBool total, SymbolTable symbolTable) {
        TapList valueZones;
        switch (expression.opCode()) {
            case ILLang.op_call: {
                // This should be a call to an intrinsic. Otherwise it has been split
                Tree[] realArgs = ILUtils.getArguments(expression).children();
                for (Tree realArg : realArgs) {
                    valueZones =
                            findRequiredZonesAndTBR(realArg, onPrimal, false, tbr, tbrOnDiffPtr, null, symbolTable);
                    tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                }
                return null;
            }
            case ILLang.op_ifExpression: {
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), onPrimal, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                TapList valueZonesTrue =
                        findRequiredZonesAndTBR(expression.down(2), onPrimal, false, tbr, tbrOnDiffPtr, total, symbolTable);
                valueZones =
                        findRequiredZonesAndTBR(expression.down(3), onPrimal, false, tbr, tbrOnDiffPtr, total, symbolTable);
                if (total != null) {
                    total.set(false);
                }
                return TapList.cumulWithOper(
                        TapList.copyTree(valueZones),
                        valueZonesTrue, SymbolTableConstants.CUMUL_OR);
            }
            case ILLang.op_arrayAccess: {
                Tree[] indexes = expression.down(2).children();
                for (int i = indexes.length - 1; i >= 0; --i) {
                    valueZones =
                            findRequiredZonesAndTBR(indexes[i], true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                    tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                }
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), onPrimal, onDiff, tbr, tbrOnDiffPtr, total, symbolTable);
                if (total != null) {
                    total.set(false);
                }
                return valueZones;
            }
            case ILLang.op_strings:
            case ILLang.op_arrayConstructor:
            case ILLang.op_arrayTriplet: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; --i) {
                    valueZones =
                            findRequiredZonesAndTBR(sons[i], true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                    tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                }
                return null;
            }
            case ILLang.op_fieldAccess:
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), onPrimal, onDiff, tbr, tbrOnDiffPtr, total, symbolTable);
                Object result = TapList.nth(valueZones, ILUtils
                        .getFieldRank(expression.down(2)));
                if (result instanceof TapIntList) {
                    return new TapList<>(result, null);
                } else {
                    return (TapList) result;
                }
            case ILLang.op_pointerAccess: {
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), onPrimal, onDiff, tbr, tbrOnDiffPtr, total, symbolTable);
                if (onPrimal) {
                    tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                }
                if (onDiff) {
                    setKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, ZoneInfo.listAllZones(valueZones, true), true);
                }
                valueZones =
                        findRequiredZonesAndTBR(expression.down(2), true, false, tbr, tbrOnDiffPtr, total, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                return symbolTable.treeOfZonesOfValue(expression, total, curInstruction, null);
            }
            case ILLang.op_ident: {
                VariableDecl variableDecl =
                        symbolTable.getVariableDecl(ILUtils.getIdentString(expression));
                /* If expression is a constant: */
                if (variableDecl == null) {
                    return null;
                }
                if (total != null) {
                    total.set(true);
                }
                return variableDecl.zones();
            }
            case ILLang.op_address: {
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), onPrimal, onDiff, tbr, tbrOnDiffPtr, total, symbolTable);
                return new TapList<>(null, valueZones);
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
            case ILLang.op_complexConstructor:
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                valueZones =
                        findRequiredZonesAndTBR(expression.down(2), true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                return null;
            case ILLang.op_minus:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
            case ILLang.op_not:
            case ILLang.op_sizeof:
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                return null;
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_unary:
            case ILLang.op_nameEq:
                valueZones =
                        findRequiredZonesAndTBR(expression.down(2), true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                return null;
            case ILLang.op_if:
                //In IL, an expression may be a "if".
                // Moreover, the "protected expression" mechanism creates if-expressions even for Fortran.
                valueZones =
                        findRequiredZonesAndTBR(expression.down(1), true, false, tbr, tbrOnDiffPtr, null, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                valueZones =
                        findRequiredZonesAndTBR(expression.down(2), true, false, tbr, tbrOnDiffPtr, total, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                valueZones =
                        findRequiredZonesAndTBR(expression.down(3), true, false, tbr, tbrOnDiffPtr, total, symbolTable);
                tbr.set(ZoneInfo.listAllZones(valueZones, true), true);
                return null;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                return null;
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_none:
            case ILLang.op_label:
            case ILLang.op_float:
            case ILLang.op_integer:
                return null;
            default:
                TapEnv.toolWarning(-1, "(Find zones required by expression) Unexpected operator: " + expression.opName());
                return null;
        }
    }

    private void setTBRindexOnLoopEntry(Tree indexTree) {
        Object tbrAnnot = ActivityPattern.getAnnotationForActivityPattern(indexTree, curActivity, "TBR");
        TapIntList annotValue;
        if (tbrAnnot == null) {
            annotValue = new TapIntList(1, new TapIntList(0, null));
            ActivityPattern.setAnnotationForActivityPattern(indexTree, curActivity, "TBR", annotValue);
        } else {
            annotValue = (TapIntList) tbrAnnot;
            annotValue.head = 1;
        }
    }

    private void setTBRindexOnLoopCycle(Tree indexTree) {
        Object tbrAnnot = ActivityPattern.getAnnotationForActivityPattern(indexTree, curActivity, "TBR");
        TapIntList annotValue;
        if (tbrAnnot == null) {
            annotValue = new TapIntList(0, new TapIntList(1, null));
            ActivityPattern.setAnnotationForActivityPattern(indexTree, curActivity, "TBR", annotValue);
        } else {
            annotValue = (TapIntList) tbrAnnot;
            annotValue.tail.head = 1;
        }
    }

    /**
     * True iff the zones are required by the adjoint of the upstream.
     * In fact, checks that the zones intersect the current "tbr" set,
     * which may be larger in the degenerate cases where we don't run full TBR
     */
    private boolean requiredZones(TapIntList zones, BoolVector tbr) {
        if (tbr == null || zones == null || zones.head >= nDZ) {
            return false;
        } else {
            return intersectsZoneRks(tbr, zones, null);
        }
    }

    /**
     * True if the diff pointers corresponding to these zones (which represent primal pointers)
     * are required by the adjoint of the upstream. Looks up for the zones in the "tbrOnDiffPtr" set.
     */
    private boolean requiredZonesOnDiffPtr(TapIntList zones, BoolVector tbrOnDiffPtr) {
        if (tbrOnDiffPtr == null || zones == null || zones.head >= nDZ) {
            return false;
        } else {
            return intersectsKindZoneRks(tbrOnDiffPtr, SymbolTableConstants.PTRKIND, zones, null, curSymbolTable);
        }
    }

// STUFF FOR THE "recomputability" ANALYSIS:
// The "recomputability" analysis tries to find which TBR variables may be
// recomputed instead of stored, and by means of which recomputation statements.

    private ADTBRAnalyzer.RecompInfo[] recompCumulOr(ADTBRAnalyzer.RecompInfo[] recompCumul, ADTBRAnalyzer.RecompInfo[] additionalRecomp) {
        if (additionalRecomp == null) {
            return recompCumul;
        }
        if (recompCumul == null) {
            return recompCopy(additionalRecomp);
        }
        return recompCumulOr(recompCumul, additionalRecomp, recompCumul.length);
    }

    private ADTBRAnalyzer.RecompInfo[] recompCumulOr(ADTBRAnalyzer.RecompInfo[] recompCumul, ADTBRAnalyzer.RecompInfo[] additionalRecomp, int commonLength) {
        if (additionalRecomp == null) {
            return recompCumul;
        }
        if (recompCumul == null) {
            recompCumul = new ADTBRAnalyzer.RecompInfo[nDZ];
            for (int i = nDZ - 1; i >= commonLength; --i) {
                // does nothing since recompCumul[i] is null: setInstructionRemainsInFwdSweep(recompCumul, i);
                recompCumul[i] = null;
            }
            if (commonLength >= 0) {
                System.arraycopy(additionalRecomp, 0, recompCumul, 0, commonLength);
            }
        } else {
            for (int i = commonLength - 1; i >= 0; --i) {
                if (recompCumul[i] == null) {
                    // Here, null means the absorbing, default case of a variable for which
                    // there is no known statement to recompute it  => change nothing !
                    setInstructionRemainsInFwdSweep(additionalRecomp, i, 1);
                    setInstructionRemainsInFwdSweep(additionalRecomp, i, 2);
                } else if (recompCumul[i].instruction == null) {
                    // Special case of loop-localized variables: the loop-carried recomp
                    // has received instruction=null, meaning that no recomp info comes this way.
                    // In other words, this is a neutral element for cumulOr:
                    recompCumul[i] = additionalRecomp[i];
                } else if (additionalRecomp[i] != null && additionalRecomp[i].instruction == null) {
                    // If the additionalRecomp[i] is the neutral element, do nothing.
                } else {
                    // If the two arriving recomputation instructions are different,
                    // neither of them can be used in the sequel for recomputation:
                    ADTBRAnalyzer.RecompInfo newRecompCumul = recompCumul[i].or(additionalRecomp[i]);
                    if (newRecompCumul == null) {
                        setInstructionRemainsInFwdSweep(additionalRecomp, i, 1);
                        setInstructionRemainsInFwdSweep(additionalRecomp, i, 2);
                        setInstructionRemainsInFwdSweep(recompCumul, i, 1);
                        setInstructionRemainsInFwdSweep(recompCumul, i, 2);
                    }
                    recompCumul[i] = newRecompCumul;
                }
            }
        }
        return recompCumul;
    }

    private ADTBRAnalyzer.RecompInfo[] recompCopy(ADTBRAnalyzer.RecompInfo[] recompInfos) {
        if (recompInfos == null) {
            return null;
        }
        ADTBRAnalyzer.RecompInfo[] copy = new ADTBRAnalyzer.RecompInfo[recompInfos.length];
        System.arraycopy(recompInfos, 0, copy, 0, copy.length - 1 + 1);
        return copy;
    }

    private boolean equalRecompInfos(ADTBRAnalyzer.RecompInfo[] recompInfosNew, ADTBRAnalyzer.RecompInfo[] recompInfosRef) {
        if (recompInfosRef.length != recompInfosNew.length) {
            return false;
        }
        boolean equ = true;
        int i = recompInfosRef.length - 1;
        while (equ && i >= 0) {
            if (recompInfosRef[i] == null) {
                equ = recompInfosNew[i] == null;
            } else if (recompInfosNew[i] == null) {
                equ = false;
            } else if (recompInfosRef[i].instruction == null) {
                equ = recompInfosNew[i].instruction == null;
            } else {
                equ = recompInfosRef[i].instruction == recompInfosNew[i].instruction;
            }
            --i;
        }
        return equ;
    }

    private void setZonesNonRecomputable(ADTBRAnalyzer.RecompInfo[] recompInfos, BoolVector zonesOverwritten,
                                         TapIntList cleanLoopIndexDeclaredZones, BoolVector modifiedTillEndCkpZones,
                                         BoolVector tbr, int zonesNb) {
        // first, set to "non-recomputable" all the zones in "zonesOverwritten",
        //  plus all the zones that share the same recomputation statement:
        TapList<Instruction> recompInstructions = null;
        for (int i = recompInfos.length - 1; i >= 0; --i) {
            if (recompInfos[i] != null && zonesOverwritten.get(i)) {
                recompInstructions =
                        new TapList<>(recompInfos[i].instruction, recompInstructions);
            }
        }
        for (int i = recompInfos.length - 1; i >= 0; --i) {
            if (recompInfos[i] != null
                    && TapList.contains(recompInstructions,
                    recompInfos[i].instruction)) {
                setInstructionRemainsInFwdSweep(recompInfos, i, 1);
                setInstructionRemainsInFwdSweep(recompInfos, i, 2);
                recompInfos[i] = null;
            }
        }
        // second, fixpoint to remove all recomputations that are not valid any more.
        fixpointDependents(recompInfos, zonesOverwritten, cleanLoopIndexDeclaredZones, modifiedTillEndCkpZones, tbr, zonesNb);
    }

    /**
     * Builds a new recomputation info from the given "recompInfos", keeping only
     * the recomputations that remain valid when crossing a border between an origin
     * Block and a destination Block. All recomputations about, or that depend
     * on, a zone which doesn't make it through the border, are removed.
     */
    private ADTBRAnalyzer.RecompInfo[] removeRecompOutOfScope(ADTBRAnalyzer.RecompInfo[] recompInfos,
                                                              int origNZ, int interNZ, int destNZ,
                                                              TapIntList cleanLoopIndexDeclaredZones,
                                                              BoolVector modifiedTillEndCkpZones,
                                                              BoolVector tbr) {
        ADTBRAnalyzer.RecompInfo[] result = new ADTBRAnalyzer.RecompInfo[destNZ];
        for (int i = interNZ; i < origNZ; ++i) {
            setInstructionRemainsInFwdSweep(recompInfos, i, 1);
            setInstructionRemainsInFwdSweep(recompInfos, i, 2);
        }
        // first, remove all recomputations that use a zone that is about to fall out of scope
        for (int i = interNZ; i < destNZ; ++i) {
            result[i] = null;
        }
        for (int i = interNZ - 1; i >= 0; --i) {
            if (recompInfos[i] == null) {
                result[i] = null;
            } else {
                BoolVector oldNeeded = recompInfos[i].neededAvailableZones;
                boolean neededOutOfScope = false;
                for (int j = interNZ; j < origNZ; ++j) {
                    if (oldNeeded.get(j)) {
                        neededOutOfScope = true;
                    }
                }
                if (neededOutOfScope) {
                    setInstructionRemainsInFwdSweep(recompInfos, i, 1);
                    setInstructionRemainsInFwdSweep(recompInfos, i, 2);
                    result[i] = null;
                } else {
                    BoolVector newNeeded = new BoolVector(destNZ);
                    newNeeded.cumulOr(oldNeeded, interNZ);
                    result[i] = new ADTBRAnalyzer.RecompInfo(recompInfos[i].instruction, newNeeded);
                }
            }
        }
        // second, fixpoint to remove all recomputations that are not valid any more.
        fixpointDependents(result, null, cleanLoopIndexDeclaredZones, modifiedTillEndCkpZones, tbr, interNZ);
        return result;
    }

    /**
     * Fixpoint that removes all recomputations that are not valid any more
     * or that depend on a zone which can't be used in a recompute (e.g. because its value
     * has changed or its backwards recomputation will only occur upstream)
     */
    private void fixpointDependents(ADTBRAnalyzer.RecompInfo[] recompInfos, BoolVector zonesNotUsableInRecompute,
                                    TapIntList cleanLoopIndexDeclaredZones, BoolVector modifiedTillEndCkpZones,
                                    BoolVector tbr, int zonesNb) {
        boolean fixpoint = false;
        while (!fixpoint) {
            fixpoint = true;
            for (int i = recompInfos.length - 1; i >= 0; --i) {
                if (recompInfos[i] != null &&
                    (!recompInfos[i].isValidNow(recompInfos, cleanLoopIndexDeclaredZones,
                                                modifiedTillEndCkpZones, tbr, zonesNb)
                     || (zonesNotUsableInRecompute != null
                         && zonesNotUsableInRecompute.intersects(recompInfos[i].neededAvailableZones,
                                                                 recompInfos.length))
                        )
                ) {
                    setInstructionRemainsInFwdSweep(recompInfos, i, 1);
                    setInstructionRemainsInFwdSweep(recompInfos, i, 2);
                    recompInfos[i] = null;
                    fixpoint = false;
                }
            }
        }
    }

    private void setInstructionRemainsInFwdSweepOnZones(ADTBRAnalyzer.RecompInfo[] recompInfos, BoolVector zones, int infoRk) {
        for (int i = recompInfos.length - 1; i >= 0; --i) {
            if (zones.get(i)) {
                setInstructionRemainsInFwdSweep(recompInfos, i, infoRk);
            }
        }
    }

    /**
     * @param infoRk 2 if this "remainsInFwdSweep" is about a diff pointer, 1 otherwise.
     */
    private void setInstructionRemainsInFwdSweep(ADTBRAnalyzer.RecompInfo[] recompInfos, int zoneRecompIndex, int infoRk) {
        if (recompInfos[zoneRecompIndex] != null && recompInfos[zoneRecompIndex].instruction != null
//                 && !ActivityPattern.getInstructionBoolInfo(curUnitButRemainsInFwdSweeps, recompInfos[zoneRecompIndex].instruction)
            ) {
            ActivityPattern.setInstructionBoolInfo(curUnitButRemainsInFwdSweeps, recompInfos[zoneRecompIndex].instruction, infoRk, true);
            if (TapEnv.traceCurAnalysis()) {
                TapEnv.printlnOnTrace("      => REMAINS in fwd sweep (" + zoneRecompIndex + ") for "+(infoRk==1?"primal":"diffPtr")+": " + recompInfos[zoneRecompIndex].instruction);
            }
            setInstructionRemainsInFwdSweepOnZones(recompInfos, recompInfos[zoneRecompIndex].neededAvailableZones, 1);
            if (infoRk==2) {
                setInstructionRemainsInFwdSweepOnZones(recompInfos, recompInfos[zoneRecompIndex].neededAvailableZones, 2);
            }
        }
    }

    private void setRecomputationInfo(ADTBRAnalyzer.RecompInfo[] recompInfos,
                                     TapIntList extendedDeclaredZones, ADTBRAnalyzer.RecompInfo value) {
        while (extendedDeclaredZones != null) {
            setRecomputationInfo(recompInfos, extendedDeclaredZones.head, value);
            extendedDeclaredZones = extendedDeclaredZones.tail;
        }
    }

    private void setRecomputationInfo(ADTBRAnalyzer.RecompInfo[] recompInfos,
                                     int index, ADTBRAnalyzer.RecompInfo value) {
        if (index >= 0) {
            recompInfos[index] = value;
        }
    }

    private ADTBRAnalyzer.RecompInfo[] initialRecompInfoArray() {
        ADTBRAnalyzer.RecompInfo[] recompInfos = new ADTBRAnalyzer.RecompInfo[nDZ];
        for (int i = nDZ - 1; i >= 0; --i) {
            recompInfos[i] = null;
        }
        return recompInfos;
    }

    private boolean otherNeeds(ADTBRAnalyzer.RecompInfo[] recompInfos, int neededRank) {
        for (int i = recompInfos.length - 1; i >= 0; --i) {
            if (recompInfos[i] != null && recompInfos[i].neededAvailableZones != null
                    && recompInfos[i].neededAvailableZones.get(neededRank)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Mark as "Rebased" the destinations of the pointers in "poppedZones", which are the zones of
     * variables that are TBR and that are about to be overwritten &rArr; they will be push/popped.
     */
    private void markDestinationsRebased(TapIntList poppedZones, boolean poppedPtr, boolean poppedDiffPtr) {
        ZoneInfo zone;
        BoolVector destinationZones = new BoolVector(nDZ);
        BoolVector destinationDiffZones = new BoolVector(nDZ);
        while (poppedZones != null) {
            if (poppedZones.head >= 0) {
                zone = curSymbolTable.declaredZoneInfo(poppedZones.head, SymbolTableConstants.ALLKIND);
                if (zone != null && zone.ptrZoneNb != -1 && (poppedPtr || poppedDiffPtr)) {
                    cumulPointerDestinations(zone, poppedPtr, poppedDiffPtr, destinationZones, destinationDiffZones,
                            curBlock, curInstruction, true);
                }
            }
            poppedZones = poppedZones.tail;
        }
        // Now destinationZones describes the zones that may be pointed to by a pointer which is popped, and
        // destinationDiffZones describes the diff zones that may be pointed to by a diff pointer which is popped.
        markAllocatableZonesRebased(destinationZones, destinationDiffZones, curSymbolTable);
    }

    /**
     * Mark as "Rebased" the destinations of the pointers in "popped" and in "poppedOnDiffPtr",
     * which are the zones that must be push/popped before a procedure call.
     */
    private void markDestinationsRebased(BoolVector popped, BoolVector poppedOnDiffPtr) {
        ZoneInfo zone;
        int rk;
        BoolVector destinationZones = new BoolVector(nDZ);
        BoolVector destinationDiffZones = new BoolVector(nDZ);
        for (int i = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) - 1; i >= 0; --i) {
            zone = curSymbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zone != null && zone.ptrZoneNb != -1) {
                rk = zone.kindZoneNb(SymbolTableConstants.ALLKIND);
                boolean poppedPtr = rk > 0 && popped.get(rk);
                rk = zone.kindZoneNb(SymbolTableConstants.PTRKIND);
                boolean poppedDiffPtr = rk > 0 && poppedOnDiffPtr.get(rk);
                if (poppedPtr || poppedDiffPtr) {
                    cumulPointerDestinations(zone, poppedPtr, poppedDiffPtr, destinationZones, destinationDiffZones,
                            curBlock, curInstruction, true);
                }
            }
        }
        // Now destinationZones describes the zones that may be pointed to by a pointer which is popped, and
        // destinationDiffZones describes the diff zones that may be pointed to by a diff pointer which is popped.
        markAllocatableZonesRebased(destinationZones, destinationDiffZones, curSymbolTable);
    }

    /**
     * Mark as "Rebased" the destinations of the pointers that are TBR and are falling out of scope.
     */
    private void markDestinationsRebased(BoolVector tbr, BoolVector tbrOnDiffPtr,
                                         Block origin, SymbolTable commonSymbolTable) {
        ZoneInfo zone;
        int rk;
        int onDZ = origin.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector destinationZones = new BoolVector(onDZ);
        BoolVector destinationDiffZones = new BoolVector(onDZ);
        for (int i = commonSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); i < origin.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND); ++i) {
            zone = origin.symbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zone != null && zone.ptrZoneNb != -1) {
                rk = zone.kindZoneNb(SymbolTableConstants.ALLKIND);
                boolean poppedPtr = rk > 0 && tbr.get(rk);
                rk = zone.kindZoneNb(SymbolTableConstants.PTRKIND);
                boolean poppedDiffPtr = rk > 0 && tbrOnDiffPtr.get(rk);
                if (poppedPtr || poppedDiffPtr) {
                    cumulPointerDestinations(zone, poppedPtr, poppedDiffPtr, destinationZones, destinationDiffZones,
                            origin, origin.lastInstr(), false);
                }
            }
        }
        // Now destinationZones describes the zones that may be pointed to by a pointer which is popped, and
        // destinationDiffZones describes the diff zones that may be pointed to by a diff pointer which is popped.
        markAllocatableZonesRebased(destinationZones, destinationDiffZones, origin.symbolTable);
    }

    private void cumulPointerDestinations(ZoneInfo zone, boolean poppedPtr, boolean poppedDiffPtr,
                                          BoolVector destinationZones, BoolVector destinationDiffZones,
                                          Block atBlock, Instruction atInstruction, boolean upstream) {
        // rowRk is the index of the row of the pointer destination matrix that holds the destinations of zone:
        int rowRk = zone.ptrZoneNb;
        if (rowRk >= 0) {
            BoolVector zoneDestinations =
                    curSymbolTable.getPointerDestinationVector(rowRk, atInstruction, atBlock, null, upstream);
            if (zoneDestinations != null) {
                if (poppedPtr) {
                    destinationZones.cumulOr(zoneDestinations);
                }
                if (poppedDiffPtr) {
                    destinationDiffZones.cumulOr(zoneDestinations);
                }
            }
        }
    }

    /**
     * Mark as "Rebased" the zones in destinationZones and destinationDiffZones, which are (should be) zones
     * of allocated chunks or of local variables. A memory zone is Rebased when there is a push/pop of a
     * pointer to that zone, and therefore this pointer may have to be rebased after it is popped.
     */
    private void markAllocatableZonesRebased(BoolVector destinationZones, BoolVector destinationDiffZones,
                                             SymbolTable symbolTable) {
        ZoneInfo zone;
        int rk;
        boolean oneDestinationIsRelocated = false;
        for (int i = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) - 1; i >= 0; --i) {
            zone = symbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
            if (zone != null) {
                if (zone.relocated && destinationZones.get(i) || zone.relocatedDiff && destinationDiffZones.get(i)) {
                    oneDestinationIsRelocated = true;
                }
            }
        }
        if (oneDestinationIsRelocated) {
            for (int i = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) - 1; i >= 0; --i) {
                zone = symbolTable.declaredZoneInfo(i, SymbolTableConstants.ALLKIND);
                if (zone != null) {
                    if (destinationZones.get(i)) {
                        zone.rebased = true;
                    }
                    if (destinationDiffZones.get(i)) {
                        zone.rebasedDiff = true;
                    }
                    if (destinationZones.get(i) || destinationDiffZones.get(i)) {
                        if (curUnit.publicSymbolTable().firstDeclaredZone(SymbolTableConstants.ALLKIND) <= i && i < curUnit.publicSymbolTable().freeDeclaredZone(SymbolTableConstants.ALLKIND)) {
                            // i.e. if this zone is a formal argument of the current Unit:
                            TapEnv.toolError("  Problem on " + ILUtils.toString(curInstruction.tree) + " in " + curUnit.name() + ": rebased info on formal argument " + ILUtils.toString(zone.accessTree) + " must be attached to its actual memory zone. Requires -nooptim refineADMM");
                        }
                    }
                }
            }
        }
    }

    private int approxRecomputeCost(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_assign:
                return 1 + approxRecomputeCost(tree.down(1)) + approxRecomputeCost(tree.down(2));
            case ILLang.op_ident:
                return NewSymbolHolder.isANewSymbolRef(tree) ? 1000 : 1;
            case ILLang.op_arrayAccess:
                return approxRecomputeCost(tree.down(1)) + approxRecomputeCost(tree.down(2));
            case ILLang.op_fieldAccess:
                return approxRecomputeCost(tree.down(1));
            case ILLang.op_address:
                return approxRecomputeCost(tree.down(1));
            case ILLang.op_pointerAccess:
                return 1 + approxRecomputeCost(tree.down(1)) + approxRecomputeCost(tree.down(2));
            case ILLang.op_power:
            case ILLang.op_complexConstructor:
                return 2 + approxRecomputeCost(tree.down(1)) + approxRecomputeCost(tree.down(2));
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_div:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_concat:
                return 1 + approxRecomputeCost(tree.down(1)) + approxRecomputeCost(tree.down(2));
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_sizeof:
                return 1 + approxRecomputeCost(tree.down(1));
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_unary:
                return 1 + approxRecomputeCost(tree.down(2));
            case ILLang.op_arrayTriplet:
            case ILLang.op_arrayConstructor:
            case ILLang.op_expressions: {
                Tree[] expressions = tree.children();
                int cost = 0;
                for (int i = expressions.length - 1; i >= 0; --i) {
                    cost += approxRecomputeCost(expressions[i]);
                }
                return cost;
            }
            case ILLang.op_ifExpression:
                return approxRecomputeCost(tree.down(1))
                        + approxRecomputeCost(tree.down(2)) + approxRecomputeCost(tree.down(3));
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_none:
            case ILLang.op_label:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_modifiedType:
            case ILLang.op_pointerType:
                return 0;
            case ILLang.op_call:
            case ILLang.op_nameEq:
            case ILLang.op_if:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_while:
            case ILLang.op_until:
            case ILLang.op_times:
                // these subtrees are considered not recomputable !
                return 1000;
            default:
                TapEnv.toolWarning(-1, "(Evaluating cost of recomputable statement) Unexpected operator: " + tree.opName());
                return 1000;
        }
    }

    public static void summarizeMustTBR(CallGraph cg) {
        TapList<Unit> allUnits = cg.sortedComputationalUnits() ;
        Unit unit ;
        while (allUnits != null) {
            unit = allUnits.head ;
            if (!unit.hasSource() && unit.mustHandleTBR!=null) {
                String tbrVars = prettyPrintParams(unit.mustHandleTBR, unit.externalShape);
                TapEnv.fileWarning(TapEnv.MSG_ALWAYS, null, "(AD09) Please make sure diff of external "+unit.name()+" preserves input values of"+tbrVars) ;
            }
            allUnits = allUnits.tail ;
        }
    }

    private static String prettyPrintParams(BoolVector zones, ZoneInfo[] externalShape) {
        String result = "";
        if (externalShape!=null) {
            int shapeLength = externalShape.length ;
            ZoneInfo zoneInfo;
            for (int i = 0; i < shapeLength; i++) {
                zoneInfo = externalShape[i];
                if (zones.get(zoneInfo.zoneNb)) {
                    result = result + ' ' + zoneInfo.publicName() ;
                }
            }
        }
        return result ;
    }

    /**
     * Class that holds an available (bwd) recomputation method for
     * a given zone (i.e. a given variable).
     * Holds the Instruction that is able to do the recomputing,
     * and the zones of the variables that this recomputation needs.<br>
     * Conventions:<br>
     * -- A null RecompInfo means that there is no permitted
     * recomputation Instruction here, i.e. an absorbing element for "or".<br>
     * -- A RecompInfo with null Instruction means that there
     * is no info to be added, i.e. a neutral element for "or".<br>
     * -- A RecompInfo with a non-null Instruction means a valid
     * recomputation Instruction permitted here.
     */
    private final class RecompInfo {
        private final Instruction instruction;
        private final BoolVector neededAvailableZones;
        boolean usedForPrimal = false ;
        boolean usedForDiffPtr = false ;

        private RecompInfo(Instruction instruction, BoolVector neededAvailableZones) {
            super();
            this.instruction = instruction;
            this.neededAvailableZones = neededAvailableZones;
        }

        private RecompInfo or(ADTBRAnalyzer.RecompInfo info2) {
            if (info2 == null || info2.instruction == null
                    || info2.instruction != instruction) {
                return null;
            } else {
                BoolVector orNeededAvailableZones = null;
                if (neededAvailableZones == null) {
                    if (info2.neededAvailableZones != null) {
                        orNeededAvailableZones = info2.neededAvailableZones.copy();
                    }
                } else {
                    orNeededAvailableZones = neededAvailableZones.copy();
                    if (info2.neededAvailableZones != null) {
                        orNeededAvailableZones.cumulOr(info2.neededAvailableZones);
                    }
                }
                return new ADTBRAnalyzer.RecompInfo(instruction, orNeededAvailableZones);
            }
        }

        /**
         * @return true if each zone used to recompute this assignment either<br>
         * -- (a) is a surrounding clean loop index, or<br>
         * -- (b) is unmodified by the present forward sweep, till the end of the current CKP, or<br>
         * -- (c) is recomputable, or<br>
         * -- (d) is already TBR<br>
         * In this case this assignment is a recomputation method that is valid at
         * the corresponding place in the backwards sweep.
         * [TODO] (d) could be improved as "is TBR, or will become TBR before overwritten". cf F90:lha04
         */
        private boolean isValidNow(ADTBRAnalyzer.RecompInfo[] recompInfos,
                                   TapIntList cleanLoopIndexDeclaredZones,
                                   BoolVector modifiedTillEndCkpZones, BoolVector tbr, int zonesNb) {
            int rank;
            BoolVector availableNow = new BoolVector(nDZ);
            // Sometimes (because of C local scopes, cf set09/v189) modifiedTillEndCkpZones and tbr are shorter
            // than the current zones number. We must restrict cumulOr to their actual size, given in "zonesNb"
            if (modifiedTillEndCkpZones != null) {
                availableNow.cumulOr(modifiedTillEndCkpZones.not(), zonesNb);
            }
            if (tbr != null) {
                availableNow.cumulOr(tbr, zonesNb);
            }
            availableNow.set(cleanLoopIndexDeclaredZones, true);
            for (rank = recompInfos.length - 1; rank >= 0; --rank) {
                if (recompInfos[rank] != null) {
                    availableNow.set(rank, true);
                }
            }
            return availableNow.contains(neededAvailableZones, nDZ);
        }

        @Override
        public String toString() {
            return "RECOMP:" + instruction + (usedForPrimal?", not for primal":"") + (usedForDiffPtr?", not for diffPtr":"") + ", depsOn:" + neededAvailableZones;
        }
    }

}
