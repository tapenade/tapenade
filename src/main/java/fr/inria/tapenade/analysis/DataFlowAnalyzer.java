/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 */

package fr.inria.tapenade.analysis;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.representation.*;

/**
 * Base class for data-flow analyses.
 * <p>
 * To define an actual data-flow analysis, build a derived class that overloads at least
 * constructor {@link #DataFlowAnalyzer(CallGraph, String, TapList)} and
 * method {@link #run(TapList)}.
 * <p>
 * Whenever user-provided implementation uses a
 * predefined utility methods "M" of the present class, one may need to overload recursively
 * other utility methods as indicated in the documentation of "M".
 */
public class DataFlowAnalyzer {

    /**
     * Code for action "read".
     */
    protected static final int READ = -1;
    /**
     * Code for action "write".
     */
    protected static final int WRITE = 1;
    /**
     * Code for no "write" nor "read" action.
     */
    protected static final int NOACT = 0;
    /**
     * The current CallGraph on which this analysis is currently run.
     * One may assume that curCallGraph is available in any method of this class that one chooses to overload.
     */
    protected final CallGraph curCallGraph;
    /**
     * Starting time of the analysis of the current Unit.
     */
    private final Chrono unitAnalysisTimer = new Chrono();
    /**
     * The current Unit (representing in particular a Flow Graph) on which this analysis is currently run.
     * One may assume that curUnit is available in defining the following methods (if one chooses to overload them):<ul>
     * <li>{@link #initializeCGForUnit()},</li>
     * <li>{@link #initializeCGForRootUnit()},</li>
     * <li>{@link #analyze()},</li>
     * <li>{@link #terminateCGForUnit()}</li>
     * <li>{@link #initializeFGForUnit()},</li>
     * <li>{@link #initializeFGForInitBlock()},</li>
     * <li>{@link #setCurBlockEtc(Block)},</li>
     * <li>{@link #initializeFGForBlock()},</li>
     * <li>{@link #accumulateValuesFromUpstream()},</li>
     * <li>{@link #accumulateValuesFromDownstream()},</li>
     * <li>{@link #compareUpstreamValues()},</li>
     * <li>{@link #compareDownstreamValues()},</li>
     * <li>{@link #propagateValuesBackwardThroughBlock()},</li>
     * <li>{@link #propagateValuesForwardThroughBlock()},</li>
     * <li>{@link #terminateFGForBlock()},</li>
     * <li>{@link #terminateFGForTermBlock()},</li>
     * <li>{@link #terminateFGForUnit()}</li>
     * <li>{@link #propagateValuesForwardThroughExpression()},</li>
     * <li>{@link #propagateValuesBackwardThroughExpression()},</li>
     * <li>{@link #setEmptyCumulAndCycleValues()},</li>
     * <li>{@link #getValueFlowingThrough()},</li>
     * <li>{@link #getValueFlowingBack()},</li>
     * <li>{@link #cumulValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #cumulCycleValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #initializeCumulValue()}</li>
     * </ul><br> Also, one <b>must make sure</b> that curUnit is set to the correct value before using the utilities:<ul>
     * <li>{@link #referenceIsTotal(boolean, TapList)},</li>
     * </ul>
     */
    protected Unit curUnit;
    /**
     * The current ActivityPattern for which we analyze the current Unit.
     */
    protected ActivityPattern curActivity;
    /**
     * When analyzing a call, the current called Unit.
     * One <b>must make sure</b> that curCalledUnit is set to the correct value before using the utilities:<ul>
     * </ul>
     */
    protected Unit curCalledUnit;
    /**
     * Total number of zones (i.e. Tapenade's elementary chunks of memory) in the current Block.
     */
    protected int nDZ = -9;
    /**
     * The current Block of the current Unit on which analysis is run.
     * One may assume that curBlock is available in defining the following methods (if one chooses to overload them):<ul>
     * <li>{@link #initializeFGForInitBlock()},</li>
     * <li>{@link #initializeFGForBlock()},</li>
     * <li>{@link #accumulateValuesFromUpstream()},</li>
     * <li>{@link #accumulateValuesFromDownstream()},</li>
     * <li>{@link #compareUpstreamValues()},</li>
     * <li>{@link #compareDownstreamValues()},</li>
     * <li>{@link #propagateValuesBackwardThroughBlock()},</li>
     * <li>{@link #propagateValuesForwardThroughBlock()},</li>
     * <li>{@link #terminateFGForBlock()},</li>
     * <li>{@link #terminateFGForTermBlock()}</li>
     * <li>{@link #propagateValuesForwardThroughExpression()},</li>
     * <li>{@link #propagateValuesBackwardThroughExpression()},</li>
     * <li>{@link #setEmptyCumulAndCycleValues()},</li>
     * <li>{@link #getValueFlowingThrough()},</li>
     * <li>{@link #getValueFlowingBack()},</li>
     * <li>{@link #cumulValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #cumulCycleValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #initializeCumulValue()}</li>
     * </ul>
     */
    protected Block curBlock;
    /**
     * The current Flow Graph arrow through which the analysis is propagating.
     * One may assume that curArrow is available in defining the following methods (if one chooses to overload them):<ul>
     * <li>{@link #getValueFlowingThrough()},</li>
     * <li>{@link #getValueFlowingBack()},</li>
     * <li>{@link #cumulValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #cumulCycleValueWithAdditional(SymbolTable)}</li>
     * </ul>
     */
    protected FGArrow curArrow;
    /**
     * The current instruction on which analysis is run.
     * One may assume that curInstruction is available in defining the following methods (if one chooses to overload them):<ul>
     * <li>{@link #propagateValuesForwardThroughExpression()},</li>
     * <li>{@link #propagateValuesBackwardThroughExpression()}</li>
     * </ul><br> Also, one <b>must make sure</b> that curInstruction is set to the correct value before using the utilities:<ul>
     * <li>{@link #referenceIsTotal(boolean, TapList)},</li>
     * <li>{@link #includePointedElementsInTree(TapList, ToBool, BoolMatrix, boolean, boolean, boolean)},</li>
     * <li>{@link #setRefValue(Tree, int, BoolVector, int[], boolean, boolean)}</li>
     * </ul>
     */
    protected Instruction curInstruction;
    /**
     * The SymbolTable of the currently analyzed Block.
     * One may assume that curSymbolTable is available in defining the following methods (if one chooses to overload them):<ul>
     * <li>{@link #initializeFGForInitBlock()},</li>
     * <li>{@link #initializeFGForBlock()},</li>
     * <li>{@link #accumulateValuesFromUpstream()},</li>
     * <li>{@link #accumulateValuesFromDownstream()},</li>
     * <li>{@link #compareUpstreamValues()},</li>
     * <li>{@link #compareDownstreamValues()},</li>
     * <li>{@link #propagateValuesBackwardThroughBlock()},</li>
     * <li>{@link #propagateValuesForwardThroughBlock()},</li>
     * <li>{@link #terminateFGForBlock()},</li>
     * <li>{@link #terminateFGForTermBlock()}</li>
     * <li>{@link #propagateValuesForwardThroughExpression()},</li>
     * <li>{@link #propagateValuesBackwardThroughExpression()},</li>
     * <li>{@link #setEmptyCumulAndCycleValues()},</li>
     * <li>{@link #getValueFlowingThrough()},</li>
     * <li>{@link #getValueFlowingBack()},</li>
     * <li>{@link #cumulValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #cumulCycleValueWithAdditional(SymbolTable)},</li>
     * <li>{@link #initializeCumulValue()}</li>
     * </ul><br> Also, one <b>must make sure</b> that curSymbolTable is set to the correct value before using the utilities:<ul>
     * <li>{@link #includePointedElementsInTree(TapList, ToBool, BoolMatrix, boolean, boolean, boolean)},</li>
     * <li>{@link #setRefValue(Tree, int, BoolVector, int[], boolean, boolean)},</li>
     * <li>{@link #setInfoBoolTreeToExtendedDeclaredZones(BoolVector, TapList, boolean, TapList, boolean, int)},</li>
     * <li>{@link #setInfoBoolTreeToExtendedDeclaredZones(BoolVector, TapList, TapList, TapList, boolean, int)},</li>
     * </ul>
     */
    protected SymbolTable curSymbolTable;
    /**
     * The name of the present analysis.
     */
    protected String curAnalysisName;
    /**
     * Units for which we want to trace analysis "on the fly".
     */
    protected TapList<Unit> tracedUnits;

    /**
     * True when we are analyzing a tree which is inside a declaration.
     * Useful to distinguish assignments from initializations.
     */
    protected boolean inADeclaration;
    /**
     * Context for analyses that are top-down on the Call Graph.
     */
    protected UnitStorage topDownContexts;

    /**
     * Context for analyses that are top-down on the Call Graph.
     *
     * @param unit current Unit.
     * @return context for analyses that are top-down on the Call Graph.
     */
    protected Object topDownContext(Unit unit) {
        return topDownContexts.retrieve(unit);
    }

    /**
     * Set context for analyses that are top-down on the Call Graph.
     *
     * @param unit    current Unit.
     * @param context context Object.
     */
    protected void setTopDownContext(Unit unit, Object context) {
        topDownContexts.store(unit, context);
    }

    /**
     * BoolVector of declared zones that have a special "unique" access in the current loop.
     */
    protected BoolVector uniqueAccessZones;
    /**
     * The conservative info value for this analysis.
     */
    protected boolean conservativeValue = true;
    /**
     * Number of Flow Graph sweeps of the analysis of the current Unit.
     */
    private int sweeps;
    public void resetSweeps() {
        sweeps = 0 ;
    }
    public int sweeps() {
        return sweeps ;
    }

    /**
     * Creation of a DataFlowAnalyzer.
     */
    protected DataFlowAnalyzer(CallGraph callGraph,
                               String analysisName, TapList<String> traceAnalysisUnitNames) {
        super();
        curCallGraph = callGraph;
        curAnalysisName = analysisName;
        tracedUnits = callGraph.getUnits(traceAnalysisUnitNames);
    }

    /**
     * @return true iff this Unit is called by another Unit
     * whose rank indicates it is deeper in the call graph (&rarr;cycle!).
     */
    private static boolean isCGCycleHead(Unit unit) {
        Unit caller;
        TapList<CallArrow> arrows = unit.callers();
        boolean isCGCycleHead = false;
        while (!isCGCycleHead && arrows != null) {
            caller = arrows.head.origin;
            isCGCycleHead = caller.rank() >= 0 && caller.rank() >= unit.rank();
            arrows = arrows.tail;
        }
        return isCGCycleHead;
    }

    /**
     * Marks all caller Units of this Unit "unit" as needing to be
     * re-analyzed (because analysis of "unit" returned a modified result).
     */
    private static void markCallersOutOfDate(Unit unit) {
        Unit caller;
        TapList<CallArrow> arrows = unit.callers();
        while (arrows != null) {
            caller = arrows.head.origin;
            caller.analysisIsOutOfDateUp = true;
            arrows = arrows.tail;
        }
    }

    /**
     * @return true iff this Unit calls another Unit
     * whose rank indicates it is higher in the call graph (&rarr;cycle!).
     */
    private static boolean isCGCycleTail(Unit unit) {
        Unit callee;
        TapList<CallArrow> arrows = unit.callees();
        boolean isCGCycleTail = false;
        while (!isCGCycleTail && arrows != null) {
            callee = arrows.head.destination;
            isCGCycleTail = callee.rank() >= 0 && callee.rank() <= unit.rank();
            arrows = arrows.tail;
        }
        return isCGCycleTail;
    }

// NOTES ON THE FRONTIERS SPECIFYING SUB- FLOW GRAPHS:
//----------------------------------------------------
// [llh, 6 May 2010] We consider that a subpart of a given Flow Graph must be
// designated by means of 2 "frontiers", which in turn determine an "inside".
// The entry frontier designates the entry into the subpart, by designating
// the list of FGArrows that jump from outside to inside the subpart.
// All arrows in the entry frontier MUST have the same destination Block,
// called the "1st block in".
// The exit frontier designates the exit from the subpart, by designating
// the list of FGArrows that jump from inside to outside the subpart.
// All arrows in the exit frontier MUST have the same destination Block,
// called the "1st block out".
// The subpart must be cleanly structured, i.e. there must be no FGArrow that
// jumps from outside to inside with a destination different from "1st block in",
// and likewise there must be no FGArrow that jumps from inside to outside
// with a destination different from "1st block out".
// This constraint is essential e.g. for a recursive construction of the
// reverse-diff Flow Graph in II_LOOP's, BINOMIAL_LOOP's, or CHECKPOINT's.
// Given this constraint, the "inside" can be easily computed.
// The inside typically contains the "1st block in" and not the "1st block out".
// [llh] 14/09/2010: We slightly relax this: for NATURAL_LOOPs, we try to accept
// that the exit frontier goes to several different destination Blocks !!
//--------------------------------------------------------------

    /**
     * Marks all callee Units of this Unit "unit" as needing to be
     * re-analyzed (because analysis of "unit" returned a modified result).
     */
    private static void markCalleesOutOfDate(Unit unit) {
        Unit callee;
        TapList<CallArrow> arrows = unit.callees();
        while (arrows != null) {
            callee = arrows.head.destination;
            callee.analysisIsOutOfDateDown = true;
            arrows = arrows.tail;
        }
    }

    /**
     * Marks as "out of date" the destination of each FGArrow in "arrows".
     * When a Block is "out of date", it needs to be re-analyzed
     * (because the (upstream) input for its analysis has been modified).
     * Do not mark Blocks that flow outside the exit frontier.
     */
    private static void markFlowOutOfDate(TapList<FGArrow> arrows,
                                          TapList<FGArrow> excludedExitArrows) {
        while (arrows != null) {
            if (!TapList.contains(excludedExitArrows, arrows.head)) {
                arrows.head.destination.analysisIsOutOfDate = true;
            }
            arrows = arrows.tail;
        }
    }

    /**
     * Marks as "out of date" the origin of each FGArrow in "arrows".
     * When a Block is "out of date", it needs to be re-analyzed
     * (because the (downstream) input for its analysis has been modified).
     * Do not mark Blocks that back-flow outside the entry frontier.
     */
    private static void markBackFlowOutOfDate(TapList<FGArrow> arrows,
                                              TapList<FGArrow> excludedEntryArrows) {
        while (arrows != null) {
            if (!TapList.contains(excludedEntryArrows, arrows.head)) {
                arrows.head.origin.analysisIsOutOfDate = true;
            }
            arrows = arrows.tail;
        }
    }

    /** If called after PointerAnalyzer was run, returns the precomputed list of
     * the Units called by "expression" (which is an op_call). There may be more
     * than one unit e.g. in case of overloading or when using function pointers. */
    public static TapList<Unit> getCalledUnits(Tree expression) {
        return (TapList<Unit>)expression.getAnnotation("calledUnits") ;
    }

    /**
     * When "expression" is a procedure or function call,
     * returns the Unit of the called procedure.
     */
    public static Unit getCalledUnit(Tree expression, SymbolTable symbolTable) {
        Object annot = expression.getAnnotation("callArrow");
        if (annot != null) {
            return ((CallArrow) annot).destination;
        } else {
            // si c'est une intrinsic ou si on n'a pas l'annotation callArrow
            FunctionDecl functionDecl = null;
            if (symbolTable != null) {
                String unitName = ILUtils.getCallFunctionNameString(expression);
                if (unitName != null) {
                    WrapperTypeSpec[] types = symbolTable.typesOf(ILUtils.getArguments(expression));
                    functionDecl =
                            symbolTable.getTypedFunctionDecl(unitName, null, types, false, expression);
                    if (functionDecl == null) {
                        TapList<FunctionDecl> functionDecls = symbolTable.getFunctionDecl(unitName, null, null, false);
                        functionDecl = functionDecls == null ? null : functionDecls.head;
                    }
                }
            }
            return functionDecl == null ? null : functionDecl.unit();
        }
    }

    /**
     * Converts a public info Boolvector, based on the external shape of fromUnit,
     * into the equivalent public info Boolvector, but based on the external shape of toUnit.
     */
    public static BoolVector convertInfoFromUnitToUnit(BoolVector dataOnFromUnit, Unit fromUnit, Unit toUnit, int diffKind) {
        ZoneInfo fromZone, toZone ;
        int fdki, tdkj ;
        if (fromUnit.externalShape == null || toUnit.externalShape == null) {
            return dataOnFromUnit.copy();
        }
        BoolVector dataOnToUnit = new BoolVector(toUnit.paramElemsNb());
        for (int i=fromUnit.paramElemsNb()-1 ; i>=0 ; --i) {
            fromZone = fromUnit.paramElemZoneInfo(i);
            fdki = fromZone.kindZoneNb(diffKind) ;
            if (fdki!=-1 && dataOnFromUnit.get(fdki)) {
                for (int j=toUnit.paramElemsNb()-1 ; j>=0 ; --j) {
                    toZone = toUnit.paramElemZoneInfo(j) ;
                    if (matchingZoneInfos(fromZone, toZone)) {
                        tdkj = toZone.kindZoneNb(diffKind) ;
                        if (tdkj!=-1) {
                            dataOnToUnit.set(tdkj, true);
                        }
                    }
                }
            }
        }
        return dataOnToUnit;
    }

    private static boolean matchingZoneInfos(ZoneInfo info1, ZoneInfo info2) {
        if (info1 == null || info2 == null || info1.kind() != info2.kind()) {
            return false;
        }
        if (info1==info2) return true ;
        switch (info1.kind()) {
            case SymbolTableConstants.PARAMETER:
                return info1.index == info2.index && !ILUtils.disjointPath(info1.accessTree, info2.accessTree);
            case SymbolTableConstants.RESULT:
                return !ILUtils.disjointPath(info1.accessTree, info2.accessTree);
            case SymbolTableConstants.LOCAL:
            case SymbolTableConstants.GLOBAL:
                if (info1.isCommon()) {
                    return info1.commonName.equals(info2.commonName) &&
                        (info1.containsOffset(info2.startOffset) || info2.containsOffset(info1.startOffset));
                } else {
                    return info1 == info2;
                }
            default:
                TapEnv.toolError("(matchingZoneInfos) failing to compare " + info1 + " and " + info2);
                return false;
        }
    }

    /**
     * Propagates a private info on the caller's side onto the actual parameters of
     * the call. The given private info "privateData" is propagated into the data trees
     * for the call actual arguments. Info totally propagated onto an actual argument is
     * removed from the "privateData", which is therefore modified !
     * Similarly, info concerning the "actualResultTree" is propagated into the data
     * tree for the result "actualResultData", and removed from "privateData" when total.
     *
     * @param privateData      The given private info in the caller, nearby the call.
     * @param actualResultTree The expression Tree that receives the actual
     *                         call result, if any. null otherwise.
     * @param actualParamTreeS The expression trees of the actual arguments of the call.
     * @param actualResultData The tree of Boolean of the info on
     *                         the actual result (output). Must be given non-null if actualResultTree is non-null.
     * @param nbDiffZ          The number of diffKind Zones
     * @param tmpZones         the list of extra zones introduced by splitting. Their info is considered True.
     * @param instruction      The Instruction of the current procedure call.
     * @param arrow            The current CallArrow from the current Instruction to the called Unit (useless?).
     * @param valuesCarryInfo  Not sure if necessary ?
     * @param whichKind        The kind of the info as stored in the privateData.
     * @return The tree of Boolean of the info on each actual argument (output).
     */
    public static TapList[] propagateCallSiteDataToActualArgs(BoolVector privateData,
                                                  Tree actualResultTree, Tree[] actualParamTreeS, TapList actualResultData,
                                                  int nbDiffZ, TapIntList tmpZones, Instruction instruction, CallArrow arrow /*TODO: useless!?*/,
                                                  boolean valuesCarryInfo, int whichKind) {
        int nbParams = actualParamTreeS.length;
        SymbolTable symbolTable = instruction.block.symbolTable;
        ToBool total = new ToBool();
        TapList actualParamZonesTree;
        TapIntList totalAccessZones;
        BoolVector transitOutsideParams = new BoolVector(nbDiffZ);
        transitOutsideParams.setTrue();
        TapList[] callerParamDataS = new TapList[nbParams];
        for (int i = nbParams - 1; i >= 0; --i) {
            actualParamZonesTree =
                    symbolTable.treeOfZonesOfValue(actualParamTreeS[i], total, instruction, null);
            if (actualParamZonesTree != null) {
                actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                includePointedElementsInTree(actualParamZonesTree, total, null, true,
                        symbolTable, instruction, false, true);
                // Parameters passed by value do not propagate their exit status forwards :
                if (arrow.takesArgumentByValue(i + 1) && !valuesCarryInfo) {
                    eraseInfoOnTopLevel(actualParamZonesTree);
                }
                //[llh] TODO: the following is not perfect for zone-trees extended with pointer destinations:
                // the "total" info can be different for different elements of the zonesTree.
                if (total.get()) {
                    totalAccessZones = ZoneInfo.listAllZones(actualParamZonesTree, true);
                    setKindZoneRks(transitOutsideParams, whichKind, totalAccessZones, false, symbolTable);
                }
            }
            callerParamDataS[i] =
                    buildInfoBoolTreeOfDeclaredZones(actualParamZonesTree, privateData,
                                                     tmpZones, whichKind, symbolTable);
        }
        if (actualResultTree != null && actualResultData != null) {
            actualParamZonesTree =
                    symbolTable.treeOfZonesOfValue(actualResultTree, total, instruction, null);
            if (actualParamZonesTree != null) {
                actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                includePointedElementsInTree(actualParamZonesTree, total, null, true,
                        symbolTable, instruction, false, true);
                //[llh] TODO: the following is not perfect for zone-trees extended with pointer destinations:
                // the "total" info can be different for different elements of the zonesTree.
                if (total.get()) {
                    totalAccessZones = ZoneInfo.listAllZones(actualParamZonesTree, true);
                    setKindZoneRks(transitOutsideParams, whichKind, totalAccessZones, false, symbolTable);
                }
            }
            TapList resultData =
                    buildInfoBoolTreeOfDeclaredZones(actualParamZonesTree, privateData,
                                                     tmpZones, whichKind, symbolTable);
            actualResultData.head = resultData != null ? resultData.head : Boolean.FALSE;
        }
        privateData.cumulAnd(transitOutsideParams);
        return callerParamDataS;
    }

    /** Propagates a boolean data-flow info callSiteData, which applies in some calling
     * procedure immediately before/after a call site (i.e. a call to some callee procedure),
     * to the same info translated to apply at the entry/exit of the callee.
     * In order to deal with the call arguments, either param callSiteParamDataS is passed non-null
     * and it is assumed to contain the TapList-tree of the data-flow info for each actual
     * argument (and for the actual result at index 0) before/after the call site,
     * or callSiteParamDataS is passed null (or some element of it is null for some index) and then
     * params actualResultTree and actualParamTreeS must be passed non-null, and are used to
     * retrieve the part of the data-flow that concerns the formal parameters by looking up
     * in callSiteData for what concerns the actual arguments.
     * @param callSiteData the given data-flow info, expressed in terms of the zones that
     *   exist at the call site. This method does not modify it.
     * @param callSiteParamDataS the given data-flow info about the actual parameters of the call.
     *   If passed null or some index is null, then the corresponding actualResultTree or
     *   element of actualParamTreeS should be passed non-null.
     *   Value at index 0 is about the actual result of the call,
     *   and it is used only when onEntry==false, otherwise may be null
     * @param actualResultTree The tree of the (actual) result.
     *   Needed only  for a function and when callSiteParamDataS is null or null at index 0.
     *   TODO: argument should disappear when precomputed actualResultTree is stored on the callTree.
     * @param actualParamTreeS The trees of the actual arguments.
     *   Needed only when callSiteParamDataS is null or null at the corresponding index.
     *   TODO: argument should disappear when precomputed actualParamTreeS are stored on the callTree.
     * @param onEntry if true, propagate the info from before the call, to the callee entry,
     *   else propagate the info from after the call, back to the callee exit.
     * @param callTree the call tree (operator op_call)
     * @param callInstruction the call instruction
     * @param callArrow the call arrow from the calling procedure to the callee.
     * @param whichKind the kind of zones on which the data-flow info is focused.
     * @return the corresponding data-flow info at the entry/exit,
     *   expressed in terms of the zones that exist at the root of the callee. */
    public static BoolVector translateCallSiteDataToCallee(BoolVector callSiteData, TapList[] callSiteParamDataS,
                                                  Tree actualResultTree, Tree[] actualParamTreeS, boolean onEntry,
                                                  Tree callTree, Instruction callInstruction, CallArrow callArrow,
                                                  int whichKind) {
        Unit calledUnit = callArrow.destination ;
        boolean traceHere = TapEnv.traceCurAnalysis() && TapEnv.traceCalledUnit(calledUnit); // Using options -tracefromunit <UName> and -tracetounit <UName>
        if (traceHere) {
            TapEnv.printlnOnTrace("------------------- start translateCallSiteDataToCallee (BoolVector) -----------") ;
            TapEnv.printlnOnTrace("  translateCallSiteDataToCallee ON "+(onEntry?"ENTRY":"EXIT")+", callSiteData:"+callSiteData) ;
            TapEnv.printlnOnTrace("    Instruction:" + callInstruction + " CallArrow:" + callArrow) ;
        }

        int calleeZoneNb = calledUnit.publicZonesNumber(whichKind) ;
//TODO: coversFurther must be precomputed and made specific to the particular call site, not only to the callArrow:
        BoolVector coversFurther = callArrow.coversFurther ;
        if (coversFurther!=null) {
            // Useful to force do nothing if an elementary argument is neither read nor written
            // (otherwise propagation might propagate too much if publicZoneInfo.coversFurther)
            BoolVector calleeZonesPossiblyRorW = calledUnit.unitInOutPossiblyRorW();
            coversFurther.cumulAnd(calleeZonesPossiblyRorW) ;
        }

        SymbolTable callSiteSymbolTable = callInstruction.block.symbolTable;
        int nbParams = (callSiteParamDataS!=null ? callSiteParamDataS.length-1 : actualParamTreeS.length) ;
        // Retrieve precomputed data about actual arguments:
        TapList[] actualParamZonesTreeS = callTree.getAnnotation("actualParamZonesTreeS") ;
        boolean[] actualParamTotalS = callTree.getAnnotation("actualParamTotalS") ;
        TapList actualResultZonesTree = null ;
        // TODO: paramZoneAnnotation should never be absent. Fix this when it is absent!
        boolean paramZoneAnnotationAbsent = (actualParamZonesTreeS==null || actualParamTotalS==null) ;
        if (paramZoneAnnotationAbsent) {
            actualParamZonesTreeS = new TapList[nbParams] ;
            actualParamTotalS = new boolean[nbParams] ;
        }
        int paramRk, ki ;
        ZoneInfo zi ;

        for (int i=0 ; i<=nbParams ; ++i) {
            if (callSiteParamDataS==null || callSiteParamDataS[i]==null) {
                if (i==0) {
                    if (actualResultTree!=null) {
                        // TODO: use rather the precomputed value from annotations?
                        actualResultZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualResultTree, null, callInstruction, null) ;
                        // Note: we do not "includePointedElementsInTree" for the actualResultTree. Maybe we should ?
                        // actualResultZonesTree = TapList.copyTree(actualResultZonesTree);
                        // includePointedElementsInTree(actualResultZonesTree, total, null, true,
                        //                              callSiteSymbolTable, callInstruction, false, true);
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualResultZonesTree is "+actualResultZonesTree) ;
                    }
                } else {
                    if (paramZoneAnnotationAbsent && actualParamTreeS!=null && actualParamTreeS[i-1]!=null) {
                        // TODO: use rather the precomputed value from annotations!
                        TapList actualParamZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualParamTreeS[i-1], null, callInstruction, null);
                        if (actualParamZonesTree!=null && i!=0) { // Don't include pointer dests for result. Why?
                            actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                            includePointedElementsInTree(actualParamZonesTree, null, null, true,
                                         callSiteSymbolTable, callInstruction, false, true);
                        }
                        actualParamZonesTreeS[i-1] = actualParamZonesTree ;
                        actualParamTotalS[i-1] = false ;
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualParamZonesTree of arg#"+i+" is "
                                          +actualParamZonesTreeS[i-1]+" total:"+actualParamTotalS[i-1]) ;
                    }
                }
            } else {
                if (traceHere) {
                    TapEnv.printlnOnTrace("    callSiteParamDataS["+i+(i==0?"=result":"")+"] = "+callSiteParamDataS[i]);
                }
            }
        }

        // Copy info on globals. Their ordering is the same for caller and for callee
        // (except for pointer's initial destinations, see "replaceActualsByInitials()")
        int globalKindZoneNb = calledUnit.globalZonesNumber(whichKind) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
        BoolVector dataCopy = callSiteData.copy() ;
        PointerAnalyzer.replaceActualsByInitials(dataCopy, callTree, whichKind) ;
        BoolVector calleeData = dataCopy.copy(globalKindZoneNb, calleeZoneNb) ;
        if (traceHere) {
            TapEnv.printlnOnTrace("   calleeData after copying "+globalKindZoneNb+" globals:"+calleeData) ;
        }

        // Now transfer info specifically about actual arguments:
        int globalZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ;
        Tree zoneAccessTree ;
        ToBool toSkipZI = new ToBool(false) ;
        for (int i=calledUnit.paramElemsNb()-1 ; i>=globalZoneNb ; --i) {
            zi = calledUnit.paramElemZoneInfo(i) ;
            ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zi.kindZoneNb(whichKind)) ;
            if (ki!=-1) {
              if (traceHere) System.out.println("      INDEX "+i+" ZONEINFO:"+zi) ; 
              paramRk = zi.index ;
              if ((zi.isParameter() && 0<paramRk && paramRk<=nbParams)
                  ||
                  (zi.isResult() && !onEntry)) {
                if (paramRk>0) { // Probably (?) don't do cross-language change for the function result:
                    zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                } else {
                    zoneAccessTree = zi.accessTree ;
                    toSkipZI.set(false) ;
                }
                if (!toSkipZI.get()) {
                    if (callSiteParamDataS!=null && callSiteParamDataS[paramRk]!=null) {
                        if (TapList.oneTrue(
                                    TapList.getSetFieldLocation(
                                            callSiteParamDataS[paramRk],
                                            zoneAccessTree, false))) {
                            calleeData.set(ki, true) ;
                        }
                    } else {
                        TapList actualParamZonesTree ;
                        if (paramRk==0) {
                            actualParamZonesTree = actualResultZonesTree;
                        } else {
                            actualParamZonesTree = actualParamZonesTreeS[paramRk-1] ;
                        }
                        // At a call exit point, exclude passing the info on the top part of
                        //  a formal argument that is passed by value:
                        // ATTENTION special trick: even if PASS-BY-VALUE, some arguments will have their
                        // diff passed by reference. This happens as an optimization in reverse mode for
                        // variables that are only read inside the call. This implies that an info that actually
                        // applies to the diff (e.g. usefulness) must propagate backwards to the calledUnit.
                        if (onEntry || paramRk==0
                            || (zi.accessTree==null || ILUtils.isAccessedThroughPointer(zi.accessTree))
                            || !callArrow.takesArgumentByValue(paramRk)) {
                            TapList actualParamElement =
                                TapList.getSetFieldLocation(actualParamZonesTree,
                                                            zoneAccessTree, false) ;
                            if (traceHere) {
                                System.out.println("        zoneAccessTree:"+zoneAccessTree+" through zones tree:"+actualParamZonesTree+" reaches subtree:"+actualParamElement) ;
                            }
                            TapIntList callSiteAccessedIndices =
                                mapZoneRkToKindZoneRk(
                                        ZoneInfo.listAllZones(actualParamElement,
                                                (paramRk>0 && coversFurther!=null && coversFurther.get(i))),
                                        whichKind, callSiteSymbolTable) ;
                            if (traceHere) {
                                System.out.println("          leading to vector indices:"+callSiteAccessedIndices) ;
                            }
                            if (callSiteData.intersects(callSiteAccessedIndices)) {
                                calleeData.set(ki, true) ;
                            }
                        } else {
                            if (traceHere) {
                                System.out.println("        top part ("+zoneAccessTree+") of actual parameter "+actualParamZonesTree+" is only passed by value") ;
                            }
                        }
                    }
                }
              }
            }
        }

        if (traceHere) {
            TapEnv.printlnOnTrace("   final calleeData:"+calleeData) ;
            TapEnv.printlnOnTrace("------------------- end translateCallSiteDataToCallee (BoolVector) -------------") ;
        }
        return calleeData ;
    }

//TEMPORARY(?) COPY OF translateCallSiteDataToCallee() WITH ARGUMENT propagateEvenByValue
//      * @param propagateEvenByValue when true, forces propagation of info upon call exit
//      *   on top level of parameters, even in the call-by-value case. Most often passed "false".
//      *   Typically passed "true" for a backwards analysis (Usefulness, AvlX...) across call exit.
    public static BoolVector translateCallSiteDataToCallee(BoolVector callSiteData, TapList[] callSiteParamDataS,
                                                  Tree actualResultTree, Tree[] actualParamTreeS, boolean onEntry,
                                                  Tree callTree, Instruction callInstruction, CallArrow callArrow,
                                                  int whichKind, boolean propagateEvenByValue) {
        Unit calledUnit = callArrow.destination ;
        boolean traceHere = TapEnv.traceCurAnalysis() && TapEnv.traceCalledUnit(calledUnit); // Using option -traceCalledUnit <UName>
        if (traceHere) {
            TapEnv.printlnOnTrace("------------------- start TEMP translateCallSiteDataToCallee (BoolVector) -----------") ;
            TapEnv.printlnOnTrace("  translateCallSiteDataToCallee ON "+(onEntry?"ENTRY":"EXIT")+", callSiteData:"+callSiteData) ;
            TapEnv.printlnOnTrace("    Instruction:" + callInstruction + " CallArrow:" + callArrow) ;
        }

        int calleeZoneNb = calledUnit.publicZonesNumber(whichKind) ;
//TODO: coversFurther must be precomputed and made specific to the particular call site, not only to the callArrow:
        BoolVector coversFurther = callArrow.coversFurther ;
        if (coversFurther!=null) {
            // Useful to force do nothing if an elementary argument is neither read nor written
            // (otherwise propagation might propagate too much if publicZoneInfo.coversFurther)
            BoolVector calleeZonesPossiblyRorW = calledUnit.unitInOutPossiblyRorW();
            coversFurther.cumulAnd(calleeZonesPossiblyRorW) ;
        }

        SymbolTable callSiteSymbolTable = callInstruction.block.symbolTable;
        int nbParams = (callSiteParamDataS!=null ? callSiteParamDataS.length-1 : actualParamTreeS.length) ;
        // Retrieve precomputed data about actual arguments:
        TapList[] actualParamZonesTreeS = callTree.getAnnotation("actualParamZonesTreeS") ;
        boolean[] actualParamTotalS = callTree.getAnnotation("actualParamTotalS") ;
        TapList actualResultZonesTree = null ;
        // TODO: paramZoneAnnotation should never be absent. Fix this when it is absent!
        boolean paramZoneAnnotationAbsent = (actualParamZonesTreeS==null || actualParamTotalS==null) ;
        if (paramZoneAnnotationAbsent) {
            actualParamZonesTreeS = new TapList[nbParams] ;
            actualParamTotalS = new boolean[nbParams] ;
        }
        int paramRk, ki ;
        ZoneInfo zi ;

        for (int i=0 ; i<=nbParams ; ++i) {
            if (callSiteParamDataS==null || callSiteParamDataS[i]==null) {
                if (i==0) {
                    if (actualResultTree!=null) {
                        // TODO: use rather the precomputed value from annotations?
                        actualResultZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualResultTree, null, callInstruction, null) ;
                        // Note: we do not "includePointedElementsInTree" for the actualResultTree. Maybe we should ?
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualResultZonesTree is "+actualResultZonesTree) ;
                    }
                } else {
                    if (paramZoneAnnotationAbsent && actualParamTreeS!=null && actualParamTreeS[i-1]!=null) {
                        // TODO: use rather the precomputed value from annotations!
                        TapList actualParamZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualParamTreeS[i-1], null, callInstruction, null);
                        if (actualParamZonesTree!=null && i!=0) { // Don't include pointer dests for result. Why?
                            actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                            includePointedElementsInTree(actualParamZonesTree, null, null, true,
                                         callSiteSymbolTable, callInstruction, false, true);
                        }
                        actualParamZonesTreeS[i-1] = actualParamZonesTree ;
                        actualParamTotalS[i-1] = false ;
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualParamZonesTree of arg#"+i+" is "
                                          +actualParamZonesTreeS[i-1]+" total:"+actualParamTotalS[i-1]) ;
                    }
                }
            } else {
                if (traceHere) {
                    TapEnv.printlnOnTrace("    callSiteParamDataS["+i+(i==0?"=result":"")+"] = "+callSiteParamDataS[i]);
                }
            }
        }

        // Copy info on globals. Their ordering is the same for caller and for callee
        // (except for pointer's initial destinations, see "replaceActualsByInitials()")
        int globalKindZoneNb = calledUnit.globalZonesNumber(whichKind) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
        BoolVector dataCopy = callSiteData.copy() ;
        PointerAnalyzer.replaceActualsByInitials(dataCopy, callTree, whichKind) ;
        BoolVector calleeData = dataCopy.copy(globalKindZoneNb, calleeZoneNb) ;
        if (traceHere) {
            TapEnv.printlnOnTrace("   calleeData after copying "+globalKindZoneNb+" globals:"+calleeData) ;
        }

        // Now transfer info specifically about actual arguments:
        int globalZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ;
        Tree zoneAccessTree ;
        ToBool toSkipZI = new ToBool(false) ;
        for (int i=calledUnit.paramElemsNb()-1 ; i>=globalZoneNb ; --i) {
            zi = calledUnit.paramElemZoneInfo(i) ;
            ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zi.kindZoneNb(whichKind)) ;
            if (ki!=-1) {
              if (traceHere) System.out.println("      INDEX "+i+" ZONEINFO:"+zi) ; 
              paramRk = zi.index ;
              if ((zi.isParameter() && 0<paramRk && paramRk<=nbParams)
                  ||
                  (zi.isResult() && !onEntry)) {
                if (paramRk>0) { // Probably (?) don't do cross-language change for the function result:
                    zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                } else {
                    zoneAccessTree = zi.accessTree ;
                    toSkipZI.set(false) ;
                }
                if (!toSkipZI.get()) {
                    if (callSiteParamDataS!=null && callSiteParamDataS[paramRk]!=null) {
                        if (TapList.oneTrue(
                                    TapList.getSetFieldLocation(
                                            callSiteParamDataS[paramRk],
                                            zoneAccessTree, false))) {
                            calleeData.set(ki, true) ;
                        }
                    } else {
                        TapList actualParamZonesTree ;
                        if (paramRk==0) {
                            actualParamZonesTree = actualResultZonesTree;
                        } else {
                            actualParamZonesTree = actualParamZonesTreeS[paramRk-1] ;
                        }
                        // At a call exit point, exclude passing the info on the top part of
                        //  a formal argument that is passed by value:
                        // ATTENTION special trick: even if PASS-BY-VALUE, some arguments will have their
                        // diff passed by reference. This happens as an optimization in reverse mode for
                        // variables that are only read inside the call. This implies that an info that actually
                        // applies to the diff (e.g. usefulness) must propagate backwards to the calledUnit.
                        if (onEntry || paramRk==0 || propagateEvenByValue
                            || (zi.accessTree==null || ILUtils.isAccessedThroughPointer(zi.accessTree))
                            || !callArrow.takesArgumentByValue(paramRk)) {
                            TapList actualParamElement =
                                TapList.getSetFieldLocation(actualParamZonesTree,
                                                            zoneAccessTree, false) ;
                            if (traceHere) {
                                System.out.println("        zoneAccessTree:"+zoneAccessTree+" through zones tree:"+actualParamZonesTree+" reaches subtree:"+actualParamElement) ;
                            }
                            TapIntList callSiteAccessedIndices =
                                mapZoneRkToKindZoneRk(
                                        ZoneInfo.listAllZones(actualParamElement,
                                                (paramRk>0 && coversFurther!=null && coversFurther.get(i))),
                                        whichKind, callSiteSymbolTable) ;
                            if (traceHere) {
                                System.out.println("          leading to vector indices:"+callSiteAccessedIndices) ;
                            }
                            if (callSiteData.intersects(callSiteAccessedIndices)) {
                                calleeData.set(ki, true) ;
                            }
                        } else {
                            if (traceHere) {
                                System.out.println("        top part ("+zoneAccessTree+") of actual parameter "+actualParamZonesTree+" is only passed by value") ;
                            }
                        }
                    }
                }
              }
            }
        }

        if (traceHere) {
            TapEnv.printlnOnTrace("   final calleeData:"+calleeData) ;
            TapEnv.printlnOnTrace("------------------- end translateCallSiteDataToCallee (BoolVector) -------------") ;
        }
        return calleeData ;
    }

    /** Propagates some data-flow information tree of zones, which applies at the exit of
     * some callee procedure, to the place immediately after some call site to this procedure.
     * Returns basically a copy of the given "zonesTree", in which all leaves, which are
     * TapIntList's of declared zones ranks in the callee procedure, are replaced with
     * TapIntList's of declared zones ranks in the calling procedure context. */
    public static TapList<?> translateCalleeDataToCallSite(TapList<?> zonesTree, Unit calledUnit, TapList[] paramZones) {
        return pcdtcsRec(zonesTree, calledUnit, paramZones, new TapList<>(null, null)) ;
    }
    /** Recursive utility for translateCalleeDataToCallSite() on zones trees. */
    private static TapList<?> pcdtcsRec(TapList<?> zonesTree, Unit calledUnit, TapList[] paramZones, TapList<TapPair<TapList<?>, TapList<?>>> dejaVu) {
        TapList toResult = new TapList<>(null, null);
        TapList inResult = toResult;
        TapPair<TapList<?>, TapList<?>> foundDejaVu;
        while (zonesTree != null) {
            if ((foundDejaVu = TapList.assq(zonesTree, dejaVu)) != null) {
                inResult.tail = foundDejaVu.second;
                zonesTree = null;
            } else {
                inResult.tail = new TapList<>(null, null);
                inResult = inResult.tail;
                dejaVu.placdl(new TapPair<>(zonesTree, inResult));
                if (zonesTree.head != null) {
                    if (zonesTree.head instanceof TapList) {
                        inResult.head = pcdtcsRec((TapList)zonesTree.head, calledUnit, paramZones, dejaVu);
                    } else { // zonesTree.head must be a TapIntList
                        inResult.head = translateCalleeDataToCallSite2((TapIntList)zonesTree.head, calledUnit, paramZones);
                    }
                }
                zonesTree = zonesTree.tail;
            }
        }
        return toResult.tail;
    }

    // I'd like to call it simply translateCalleeDataToCallSite(...), but this doesn't compile...
    /** Propagates some list of extended declared ranks that refer to the exit context of some procedure,
     * into a list of corresponding extended declared ranks, in the context
     * immediately after some call site to this procedure. */
    public static TapIntList translateCalleeDataToCallSite2(TapIntList calleeZones, Unit calledUnit, TapList[] paramZones) {
        int globalKindZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
        TapIntList hdCallerZones = new TapIntList(-1, null) ;
        TapIntList tlCallerZones = hdCallerZones ;
        while (calleeZones!=null) {
            int iii = calleeZones.head ;
            if (iii<globalKindZoneNb) {
                // ? It seems we don't want to reflect NULL and Undef destinations in callerZones ?
                // if (iii>=curCallGraph.numberOfDummyRootZones) {
                    tlCallerZones = tlCallerZones.placdl(iii) ;
                // }
            } else {
                ZoneInfo calleeZI = calledUnit.publicSymbolTable().declaredZoneInfo(iii, SymbolTableConstants.ALLKIND);
                if (calleeZI!=null && calleeZI.isParameter()) {
                    int paramRk = calleeZI.index ;
                    Tree accessTree = calleeZI.accessTree ;
                    TapList elemArgZones =
                        TapList.getSetFieldLocation(paramZones[paramRk - 1], accessTree, false) ;
                    TapIntList newElemZones = ZoneInfo.listAllZones(elemArgZones, false);
                    while (newElemZones!=null) {
                        if (!TapIntList.contains(hdCallerZones.tail, newElemZones.head)) {
                            tlCallerZones = tlCallerZones.placdl(newElemZones.head) ;
                        }
                        newElemZones = newElemZones.tail ;
                    }
                }
            }
            calleeZones = calleeZones.tail ;
        }
        return hdCallerZones.tail ;
    }

    // NOTE: if you pass a non-null Tree in actualParamTreeS or actualResultTree, then this method will
    // use the zones-tree of this Tree to directly update callSiteData with the info on these zones.
    // Otherwise, the info-tree is returned in the result.
    /** Propagates some data-flow information calleeData, which applies at the
     * entry (resp. exit) of some called procedure (entry iff onEntry==true, otherwise exit),
     * to the place immediately before (resp. after) some call site that calls this procedure,
     * by accumulating this info into the given callSiteData.
     * If actualResultTree is given non-null, then the info on the result's zones is integrated into
     * callSiteData. Similarly if actualParamTreeS is given non-null and actualParamTreeS[N-1] is non-null,
     * the info on parameter N is integrated into callSiteData. Otherwise, the info on the result and formal params is
     * returned (for the non-given actual result and actual params) into the returned array of TapList.
     * @param calleeData the given data, expressed in terms of the zones of the callee.
     * @param callSiteData the data, shaped after the call site, that will accumulate the translation of the calleeData.
     *   It may be passed null in which case only arguments and result data are translated.
     *   Very often, it contains the data at the other end of the call site, e.g. before the call if onEntry==false.
     * @param actualResultTree the variable receiving the call's result at the call site. May be not given, i.e. null.
     * @param actualParamTreeS the call site actual arguments. May be not given, i.e. null.
     *   Some elements may be not given, i.e. null.
     * @param onEntry true when propagation goes from called unit entry back to immediately before the call site.
     *   Otherwise, it means that propagation goes from called unit exit forward to immediately after the call site.
     * @param callTree the call tree at the call site.
     * @param instruction the instruction which contains the call site. It is used to
     *   obtain the call site's SymbolTable and pointer destination info.
     * @param arrow the call arrow for this call site.
     * @param whichKind the kind of zones on which the data-flow info is focused.
     * @param passesThroughCall When given, the mask of zones of the call site whose
     *   data in callSiteData may be modified by this call.
     *   TODO: improve this code so that this mask is given following the zones of the *callee*
     * @param passesAroundCall When given, the mask of zones of the call site whose
     *   data in callSiteData may pass untouched by this call.
     *   TODO: improve this code so that this mask is given following the zones of the *callee*
     * @param valuesCarryInfo when false and parameters are passed by value,
     *   info on the parameter is not propagated (except through pointers).
     * @param onlyWhenTotal when true, propagation is done through an argument *only* when this
     *   access to the actual argument is total. For instance onlyWhenTotal should be true when
     *   propagating a "killed" info, since a non-total actual argument will not be fully killed.
     * @return the list (indexed on [0,nbArgs]) of the data on the call's actual arguments
     *   (with index 0 representing the function result), except when actualResultTree is given and
     *   all actualParamTreeS are given, in which case returns null. */
    public static TapList[] translateCalleeDataToCallSite(BoolVector calleeData, BoolVector callSiteData,
                                                 Tree actualResultTree, Tree[] actualParamTreeS, boolean onEntry,
                                                 Tree callTree, Instruction instruction, CallArrow callArrow,
                                                 int whichKind,
                                                 BoolVector passesThroughCall, BoolVector passesAroundCall,
                                                 boolean valuesCarryInfo, boolean onlyWhenTotal) {
        Unit calledUnit = callArrow.destination ;
        boolean traceHere = TapEnv.traceCurAnalysis() && TapEnv.traceCalledUnit(calledUnit); // Using option -traceCalledUnit <UName>
        if (traceHere) {
            TapEnv.printlnOnTrace("------------------- start translateCalleeDataToCallSite (BoolVector) -----------") ;
            TapEnv.printlnOnTrace("translateCalleeDataToCallSite ON "+(onEntry?"ENTRY":"EXIT")+", calleeData :"+calleeData) ;
            TapEnv.printlnOnTrace("  INTO callSiteData:"+callSiteData) ;
            TapEnv.printlnOnTrace("  Instruction:" + instruction + " CallArrow:" + callArrow);
            TapEnv.printlnOnTrace("  passesThroughCall:"+passesThroughCall) ;
            TapEnv.printlnOnTrace("  passesAroundCall: "+passesAroundCall) ;
        }
        //TODO: coversFurther must be made specific to the particular call site, not only to the callArrow:
        BoolVector coversFurther = callArrow.coversFurther ;
        // Useful to force do nothing if an elementary argument is neither read nor written
        // (otherwise propagation might propagate too much if arrowZoneInfo.coversFurther)
        BoolVector calleeZonesPossiblyRorW = calledUnit.unitInOutPossiblyRorW();
        SymbolTable callSiteSymbolTable = instruction.block.symbolTable;
        int callSiteZoneNb = callSiteSymbolTable.freeDeclaredZone(whichKind) ;
        int nbParams = ILUtils.getArguments(callTree).length() ;
        boolean allTreesGiven = (actualResultTree!=null && actualParamTreeS!=null) ;
        if (allTreesGiven) {
            for (int i=nbParams-1 ; allTreesGiven && i>=0 ; --i) {
                if (actualParamTreeS[i]==null) allTreesGiven = false ;
            }
        }
        TapList[] callSiteParamDataS = (allTreesGiven ? null : new TapList[nbParams+1]) ;
        // TODO: use rather the precomputed value! (but this method changes it, see "valuesCarryInfo"...)
        // TapList[] actualParamZonesTreeS = callTree.getAnnotation("actualParamZonesTreeS") ;
        // boolean[] actualParamTotalS = callTree.getAnnotation("actualParamTotalS") ;
        TapList[] actualParamZonesTreeS = new TapList[nbParams];
        boolean[] actualParamTotalS = new boolean[nbParams];
        TapList actualResultZonesTree = null;
        ToBool total = new ToBool(false) ;

        // Prepare data about actual arguments, useful for their transfer from callee to call site:
        TapList actualParamZonesTree ;
        for (int i=nbParams ; i>0 ; --i) {
            if (actualParamTreeS==null || actualParamTreeS[i-1]==null) {
                callSiteParamDataS[i] = new TapList<>(Boolean.FALSE, null);
            } else {
                Tree argTree = actualParamTreeS[i-1] ;
                // If F(T)->C(T*), add an op_addressOf around argTree:
                if (callArrow.origin.isFortran() && calledUnit.isC()
                    && calleeExpectsAddress(calledUnit.formalArgumentType(i))) {
                    argTree = ILUtils.build(ILLang.op_address, ILUtils.copy(argTree)) ;
                }
                total.set(false) ;
                actualParamZonesTree =
                    callSiteSymbolTable.treeOfZonesOfValue(argTree, total, instruction, null);
                if (actualParamZonesTree!=null) {
                    actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                    includePointedElementsInTree(actualParamZonesTree, total, null, true,
                            callSiteSymbolTable, instruction, false, true);
                    // Parameters passed by value do not propagate their exit status forward :
                    if (callArrow.takesArgumentByValue(i) && !valuesCarryInfo) {
                        eraseInfoOnTopLevel(actualParamZonesTree);
                    }
                }
                actualParamZonesTreeS[i-1] = actualParamZonesTree;
                actualParamTotalS[i-1] = total.get() ;
                if (traceHere) {
                    TapEnv.printlnOnTrace("   actualParamZonesTreeS["+(i-1)+"] == " + actualParamZonesTreeS[i-1] + " (total:"+actualParamTotalS[i-1]+") for " + actualParamTreeS[i-1]);
                }
            }
        }

        if (actualResultTree==null) {
            callSiteParamDataS[0] = new TapList<>(Boolean.FALSE, null);
        } else {
            total.set(false) ;
            actualResultZonesTree =
                callSiteSymbolTable.treeOfZonesOfValue(actualResultTree, total, instruction, null);
            // Don't include pointer dests for result zones tree. Why?
            if (traceHere) {
                TapEnv.printlnOnTrace("   actualResultZonesTree == "+actualResultZonesTree+" for result") ;
            }
        }

        if (callSiteData!=null) {
            // Wipe out all info in callSiteData (transit) that cannot reach because "killed by call" (i.e. does not "pass around call")
            if (passesAroundCall!=null) {
                callSiteData.cumulAnd(passesAroundCall, callSiteZoneNb) ;
            }
            // Copy info on globals. Their ordering is the same for caller and for callee
            // (except for pointer's initial destinations, see "replaceInitialsByActuals()")
            int globalKindZoneNb = calledUnit.globalZonesNumber(whichKind) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
            BoolVector dataOnGlobals = calleeData.copy(globalKindZoneNb, callSiteZoneNb) ;
            if (globalKindZoneNb!=0) {
                PointerAnalyzer.replaceInitialsByActuals(dataOnGlobals, callTree, whichKind, callSiteZoneNb) ;
            }
            // Use masks passesThroughCall and passesAroundCall:
            if (passesThroughCall!=null) {
                dataOnGlobals.cumulAnd(passesThroughCall) ;
            }
            callSiteData.cumulOr(dataOnGlobals, callSiteZoneNb) ;
            if (traceHere) {
                TapEnv.printlnOnTrace("  callSiteData after wiping killed and copying "+globalKindZoneNb+" globals:"+callSiteData) ;
            }
        }

        // Now transfer info specifically about actual arguments:
        TapList toLocation;
        int globalZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ;
        Tree zoneAccessTree ;
        ToBool toSkipZI = new ToBool(false) ;
        for (int i=calledUnit.paramElemsNb()-1 ; i>=globalZoneNb ; --i) {
          ZoneInfo zi = calledUnit.paramElemZoneInfo(i) ;
          int ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zi.kindZoneNb(whichKind)) ;
          boolean calleeZoneData = false ;
          boolean accumulateData = false ;
          if (ki!=-1) {
              calleeZoneData = calleeData.get(ki);
              accumulateData = true ;
          }
          if (accumulateData) {
            switch (zi.kind()) {
                case SymbolTableConstants.PARAMETER: {
                    if (traceHere) {
                        TapEnv.printlnOnTrace("   callee data is "+calleeZoneData+" for elementary param #"+i+" from param#"+zi.index+", ZoneInfo:"+zi+" kindZone:"+ki) ;
                    }
                    int paramRk = zi.index ;
                    if (0<paramRk && paramRk<=nbParams && (!onlyWhenTotal || actualParamTotalS[paramRk-1])) {
                        if (callArrow.origin.isFortran() && calledUnit.isC()) {
                            zoneAccessTree = zi.accessTree ;
                        } else {
                            zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, onlyWhenTotal) ;
                        }
                        if (!toSkipZI.get()) {
                          if (actualParamTreeS==null || actualParamTreeS[paramRk-1]==null) {
                            if (traceHere) {
                                TapEnv.printlnOnTrace("    getSetFieldLocation of accessTree of "+zi
                                                      +" INSIDE "+callSiteParamDataS[paramRk]);
                            }
                            toLocation =
                                TapList.getSetFieldLocation(callSiteParamDataS[paramRk], zoneAccessTree, true);
                            if (traceHere) {
                                TapEnv.printlnOnTrace("     -> "+toLocation+" INSIDE "
                                                      +callSiteParamDataS[paramRk]+" WILL RECEIVE "+calleeZoneData);
                            }
                            if (calleeZonesPossiblyRorW.get(i)) {
                                if (calleeZoneData) {
                                    toLocation.head = Boolean.TRUE;
                                } else if (toLocation.head == null) {
                                    toLocation.head = Boolean.FALSE;
                                }
                            }
                          } else if (callSiteData != null) {
                            if (traceHere) {
                                TapEnv.printlnOnTrace("    zone:"+zi+" coversFurther:"+(coversFurther!=null && coversFurther.get(i)));
                                TapEnv.printlnOnTrace("    accessTree:" + zoneAccessTree);
                            }
                            TapIntList callSiteAccessedIndices =
                                mapZoneRkToKindZoneRk(
                                        ZoneInfo.listAllZones(
                                                TapList.getSetFieldLocation(
                                                        actualParamZonesTreeS[paramRk-1],
                                                        zoneAccessTree, false),
                                                coversFurther!=null && coversFurther.get(i)),
                                        whichKind, callSiteSymbolTable) ;
                            if (traceHere) {
                                TapEnv.printlnOnTrace("    callSiteAccessedIndices:" + callSiteAccessedIndices);
                            }
                            // Use masks passesThroughCall and passesAroundCall:
                            while (callSiteAccessedIndices!=null) {
                                updateDataInTransit(callSiteData, callSiteAccessedIndices.head, calleeZoneData,
                                                    passesThroughCall, passesAroundCall) ;
                                callSiteAccessedIndices = callSiteAccessedIndices.tail ;
                            }
                          }
                        }
                    }
                    break ;
                }
                case SymbolTableConstants.RESULT: {
                    if (traceHere) {
                        TapEnv.printlnOnTrace("   callee data is "+calleeZoneData+" for elementary param #"+i+" from RESULT, ZoneInfo:"+zi+" kindZone:"+ki) ;
                    }
                    if (actualResultTree == null) {
                        toLocation = TapList.getSetFieldLocation(callSiteParamDataS[0], zi.accessTree, true);
                        if (calleeZoneData) {
                            toLocation.head = Boolean.TRUE;
                        } else if (toLocation.head == null) {
                            toLocation.head = Boolean.FALSE;
                        }
                    } else if (callSiteData != null) {
                        TapIntList callSiteAccessedIndices =
                            mapZoneRkToKindZoneRk(
                                    ZoneInfo.listAllZones(
                                            TapList.getSetFieldLocation(
                                                    actualResultZonesTree,
                                                    zi.accessTree, false),
                                            false),
                                    whichKind, callSiteSymbolTable);
                        // Use masks passesThroughCall and passesAroundCall:
                        while (callSiteAccessedIndices!=null) {
                            updateDataInTransit(callSiteData, callSiteAccessedIndices.head, calleeZoneData,
                                                passesThroughCall, passesAroundCall) ;
                            callSiteAccessedIndices = callSiteAccessedIndices.tail ;
                        }
                    }
                    break ;
                }
            }
          } else {
              // Accumulate no data, but still the resulting callSiteParamDataS[] must hold the
              // structure of the argument's zone tree. Otherwise setInfoPRZVTree() might not work properly.
              switch (zi.kind()) {
                  case SymbolTableConstants.RESULT: {
                      if (actualResultTree==null)
                          TapList.getSetFieldLocation(callSiteParamDataS[0], zi.accessTree, true);
                      break ;
                  }
                  case SymbolTableConstants.PARAMETER: {
                      int paramRk = zi.index ;
                      if (0<paramRk && paramRk<=nbParams
                          && (actualParamTreeS==null || actualParamTreeS[paramRk-1]==null)) {
                          if (callArrow.origin.isFortran() && calledUnit.isC()) {
                              zoneAccessTree = zi.accessTree ;
                          } else {
                              zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                          }
                          TapList.getSetFieldLocation(callSiteParamDataS[paramRk], zoneAccessTree, true);
                      }
                      break ;
                  }
              }
          }
        }
        if (callSiteParamDataS!=null) {
            for (int i=1 ; i<callSiteParamDataS.length ; ++i) {
                // If info is not "naturally" carried and this is PASS-BY-VALUE, erase top-level of info:
                if (!valuesCarryInfo && callArrow.takesArgumentByValue(i)) {
                    eraseInfoOnTopLevel(callSiteParamDataS[i]);
                }
                // If F(T)->C(T*), remove the pointer level on top of info:
                if (callSiteParamDataS[i]!=null && callArrow.origin.isFortran() && calledUnit.isC()
                    && calleeExpectsAddress(calledUnit.formalArgumentType(i))) {
                    callSiteParamDataS[i] = callSiteParamDataS[i].tail ;
                }
            }
        }
        if (traceHere) {
            TapEnv.printlnOnTrace("final callSiteData:"+callSiteData) ;
            if (callSiteParamDataS!=null) {
                for (int i = 0; i < callSiteParamDataS.length; ++i) {
                    if (callSiteParamDataS[i]!=null) {
                        TapEnv.printlnOnTrace("callSiteParamDataS["+i+(i==0?"=result":"")+"] = "+callSiteParamDataS[i]);
                    }
                }
            }
            TapEnv.printlnOnTrace("------------------- end translateCalleeDataToCallSite (BoolVector) -------------") ;
        }
        return callSiteParamDataS;
    }

    // Propagation of BoolMatrix'es instead of BoolVector's
    /**
     * @param callSiteData the call site data matrix.
     *   This method does not modify it, but some of its rows may be shared as rows of the result callee data matrix.
     * @param callSiteParamDataS an array (for each argument, index 0 for result, i for argument i)
     *   of a tree of BoolVector's containing the call site data for the result or arguments.
     */
    public static BoolMatrix translateCallSiteDataToCallee(BoolMatrix callSiteData, TapList[] callSiteParamDataS,
                                                  Tree actualResultTree, Tree[] actualParamTreeS, boolean onEntry,
                                                  Tree callTree, Instruction callInstruction, CallArrow callArrow,
                                                  int whichKind/*, boolean propagateEvenByValue*/) {
        Unit calledUnit = callArrow.destination ;
        boolean traceHere = TapEnv.traceCurAnalysis() && TapEnv.traceCalledUnit(calledUnit); // Using option -traceCalledUnit <UName>
        if (traceHere) {
            TapEnv.printlnOnTrace("------------------- start translateCallSiteDataToCallee (BoolMatrix) -----------") ;
            TapEnv.printlnOnTrace("  translateCallSiteDataToCallee ON "+(onEntry?"ENTRY":"EXIT")+", callSiteData:") ;
            TapEnv.dumpOnTrace(callSiteData) ;
            TapEnv.printlnOnTrace("    Instruction:" + callInstruction + " CallArrow:" + callArrow) ;
        }

        int calleeZoneNb = calledUnit.publicZonesNumber(whichKind) ;

//TODO: coversFurther must be made specific to the particular call site, not only to the callArrow:
        BoolVector coversFurther = callArrow.coversFurther ;
        if (coversFurther!=null) {
            // Useful to force do nothing if an elementary argument is neither read nor written
            // (otherwise propagation might propagate too much if publicZoneInfo.coversFurther)
            BoolVector calleeZonesPossiblyRorW = calledUnit.unitInOutPossiblyRorW();
            coversFurther.cumulAnd(calleeZonesPossiblyRorW) ;
        }

        SymbolTable callSiteSymbolTable = callInstruction.block.symbolTable;
        int nbParams = (callSiteParamDataS!=null ? callSiteParamDataS.length-1 : actualParamTreeS.length) ;
        // Retrieve precomputed data about actual arguments:
        TapList[] actualParamZonesTreeS = callTree.getAnnotation("actualParamZonesTreeS") ;
        boolean[] actualParamTotalS = callTree.getAnnotation("actualParamTotalS") ;

        TapList actualResultZonesTree = null ;
        // TODO: paramZoneAnnotation should never be absent. Fix this when it is absent!
        boolean paramZoneAnnotationAbsent = (actualParamZonesTreeS==null || actualParamTotalS==null) ;
        if (paramZoneAnnotationAbsent) {
            actualParamZonesTreeS = new TapList[nbParams] ;
            actualParamTotalS = new boolean[nbParams] ;
        }

        int paramRk, ki ;
        ZoneInfo zi ;

        for (int i=0 ; i<=nbParams ; ++i) {
            if (callSiteParamDataS==null || callSiteParamDataS[i]==null) {
                if (i==0) {
                    if (actualResultTree!=null) {
                        // TODO: use rather the precomputed value from annotations?
                        actualResultZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualResultTree, null, callInstruction, null) ;
                        // Note: we do not "includePointedElementsInTree" for the actualResultTree. Maybe we should ?
                        // actualResultZonesTree = TapList.copyTree(actualResultZonesTree);
                        // includePointedElementsInTree(actualResultZonesTree, total, null, true,
                        //                              callSiteSymbolTable, callInstruction, false, true);
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualResultZonesTree is "+actualResultZonesTree) ;
                    }
                } else {
                    if (paramZoneAnnotationAbsent && actualParamTreeS!=null && actualParamTreeS[i-1]!=null) {
                        // TODO: use rather the precomputed value from annotations!
                        TapList actualParamZonesTree =
                            callSiteSymbolTable.treeOfZonesOfValue(actualParamTreeS[i-1], null, callInstruction, null);
                        if (actualParamZonesTree!=null && i!=0) { // Don't include pointer dests for result. Why?
                            actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                            includePointedElementsInTree(actualParamZonesTree, null, null, true,
                                         callSiteSymbolTable, callInstruction, false, true);
//                             // Parameters passed by value do not propagate their exit status forward :
//                             if (callArrow.takesArgumentByValue(i) && !valuesCarryInfo) {
//                                 eraseInfoOnTopLevel(actualParamZonesTree);
//                             }
                        }
                        actualParamZonesTreeS[i-1] = actualParamZonesTree ;
                        actualParamTotalS[i-1] = false ;
                    }
                    if (traceHere) {
                        TapEnv.printlnOnTrace("    actualParamZonesTree of arg#"+i+" is "
                                          +actualParamZonesTreeS[i-1]+" total:"+actualParamTotalS[i-1]) ;
                    }
                }
            } else {
                if (traceHere) {
                    TapEnv.printlnOnTrace("    callSiteParamDataS["+i+(i==0?"=result":"")+"] = "+callSiteParamDataS[i]);
                }
            }
        }

        // Copy info on globals. Their ordering is the same for caller and for callee
        // (except for pointer's initial destinations, see "replaceActualsByInitials()")
        int globalKindZoneNb = calledUnit.globalZonesNumber(whichKind) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
        BoolMatrix dataCopy = new BoolMatrix(callSiteData.nRows, callSiteData.nCols) ;
        dataCopy.rows = new BoolVector[callSiteData.nRows] ;
        for (int i=0 ; i<callSiteData.nRows ; ++i) {
            dataCopy.rows[i] = callSiteData.rows[i] ;
        }

        if (traceHere) {
            TapList<TapPair<Integer, TapIntList>>[] actualInitials4 =
                callTree.getAnnotation("pointersActualInitialDests") ;
            TapList<TapPair<Integer, TapIntList>> actualInitials =
                (actualInitials4==null ? null : actualInitials4[whichKind]) ;
            while (actualInitials!=null) {
                TapEnv.printlnOnTrace("      Using initial "+actualInitials.head.first+" for actuals "+actualInitials.head.second) ;
                actualInitials = actualInitials.tail ;
            }
        }

        PointerAnalyzer.replaceActualsByInitials(dataCopy, callTree, whichKind) ;
        BoolMatrix calleeData = new BoolMatrix(calleeZoneNb, callSiteData.nCols) ;
        calleeData.rows = new BoolVector[calleeZoneNb] ;
        for (int i=0 ; i<calleeZoneNb ; ++i) {
            if (i<globalKindZoneNb) {
                calleeData.rows[i] = dataCopy.rows[i] ;
            } else {
                calleeData.rows[i] = null ;
            }
        }

        if (traceHere) {
            TapEnv.printlnOnTrace("  calleeData after copying "+globalKindZoneNb+" globals:") ;
            TapEnv.dumpOnTrace(calleeData) ;
        }

        // Now transfer info specifically about actual arguments:
        int globalZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ;
        Tree zoneAccessTree ;
        ToBool toSkipZI = new ToBool(false) ;
        for (int i=calledUnit.paramElemsNb()-1 ; i>=globalZoneNb ; --i) {
            zi = calledUnit.paramElemZoneInfo(i) ;
            ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zi.kindZoneNb(whichKind)) ;
            if (ki!=-1) {
              if (traceHere) System.out.println("      INDEX "+i+" ZONEINFO:"+zi) ;
              paramRk = zi.index ;
              if ((zi.isParameter() && 0<paramRk && paramRk<=nbParams)
                  ||
                  (zi.isResult() && !onEntry)) {
                if (paramRk>0) { // Probably (?) don't do cross-language change for the function result:
                    zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                } else {
                    zoneAccessTree = zi.accessTree ;
                    toSkipZI.set(false) ;
                }
                if (!toSkipZI.get()) {
                    if (callSiteParamDataS!=null && callSiteParamDataS[paramRk]!=null) {
                        TapList<?> zoneData =
                            TapList.getSetFieldLocation(callSiteParamDataS[paramRk], zoneAccessTree, false) ;
                        if (zoneData!=null) {
                            BoolVector cumul = new BoolVector(callSiteData.nCols) ;
                            cumulOr(zoneData, cumul, (coversFurther!=null && coversFurther.get(i))) ;
                            calleeData.rows[ki] = cumul ;
                        }
                    } else {
                        TapList actualParamZonesTree ;
                        if (paramRk==0) {
                            actualParamZonesTree = actualResultZonesTree;
                        } else {
                            actualParamZonesTree = actualParamZonesTreeS[paramRk-1] ;
                        }
                        BoolVector calleeRow = calleeData.rows[ki] ;
                        if (calleeRow==null) {
                            calleeRow = new BoolVector(callSiteData.nCols) ;
                        }
                        // At a call exit point, exclude passing the info on the top part of
                        //  a formal argument that is passed by value:
                        // ATTENTION special trick: even if PASS-BY-VALUE, some arguments will have their
                        // diff passed by reference. This happens as an optimization in reverse mode for
                        // variables that are only read inside the call. This implies that an info that actually
                        // applies to the diff (e.g. usefulness) must propagate backwards to the calledUnit.
                        if (onEntry || paramRk==0 /*|| propagateEvenByValue*/
                            || (zi.accessTree==null || ILUtils.isAccessedThroughPointer(zi.accessTree))
                            || !callArrow.takesArgumentByValue(paramRk)
                            // || (diffMaybePassedByRef && argIsOnlyRead(calledUnit, paramRk))
                            ) {
                            TapList actualParamElement =
                                TapList.getSetFieldLocation(actualParamZonesTree,
                                                            zoneAccessTree, false) ;
                            if (traceHere) {
                                System.out.println("        zoneAccessTree:"+zoneAccessTree+" through zones tree:"+actualParamZonesTree+" reaches subtree:"+actualParamElement) ;
                            }
                            TapIntList callSiteAccessedIndices =
                                mapZoneRkToKindZoneRk(
                                        ZoneInfo.listAllZones(actualParamElement,
                                                (paramRk>0 && coversFurther!=null && coversFurther.get(i))),
                                        whichKind, callSiteSymbolTable) ;
                            if (traceHere) {
                                System.out.println("          leading to vector indices:"+callSiteAccessedIndices) ;
                            }
                            BoolVector callSiteRow ;
                            while (callSiteAccessedIndices!=null) {
                                callSiteRow = callSiteData.rows[callSiteAccessedIndices.head] ;
                                if (callSiteRow!=null) {
                                    calleeRow.cumulOr(callSiteRow) ;
                                } else if (callSiteAccessedIndices.head<callSiteData.nCols) {
                                    // callSiteRow is an identity row:
                                    calleeRow.set(callSiteAccessedIndices.head, true) ;
                                }
                                callSiteAccessedIndices = callSiteAccessedIndices.tail ;
                            }
                        } else {
                            if (traceHere) {
                                System.out.println("        top part ("+zoneAccessTree+") of actual parameter "+actualParamZonesTree+" is only passed by value") ;
                            }
                        }
                        calleeData.rows[ki] = calleeRow ;
                    }
                }
              }
            }
        }
        if (traceHere) {
            TapEnv.printlnOnTrace("   final calleeData:") ;
            TapEnv.dumpOnTrace(calleeData) ;
            TapEnv.printlnOnTrace("------------------- end translateCallSiteDataToCallee (BoolMatrix) -------------") ;
        }
        return calleeData ;
    }

    // Propagation of BoolMatrix'es instead of BoolVector's
    /**
     * @param calleeMask a mask on the zones of the callee. All info outside this mask is not propagated.
     *  Used in ReqExplicit.
     *  TODO: rationalize this with passesThroughCall, which may be the same info expressed on the call site zones?.
     */
    public static TapList[] translateCalleeDataToCallSite(BoolMatrix calleeData, BoolMatrix callSiteData,
                                                 Tree actualResultTree, Tree[] actualParamTreeS, boolean onEntry,
                                                 Tree callTree, Instruction instruction, CallArrow callArrow,
                                                 int whichKind,
                                                 BoolVector passesThroughCall, BoolVector passesAroundCall,
                                                 boolean valuesCarryInfo, BoolVector calleeMask) {
        Unit calledUnit = callArrow.destination ;
        boolean traceHere = TapEnv.traceCurAnalysis() && TapEnv.traceCalledUnit(calledUnit); // Using option -traceCalledUnit <UName>
        if (traceHere) {
            TapEnv.printlnOnTrace("------------------- start translateCalleeDataToCallSite (BoolMatrix) -----------") ;
            TapEnv.printlnOnTrace("  translateCalleeDataToCallSite ON "+(onEntry?"ENTRY":"EXIT")+", calleeData :") ;
            TapEnv.dumpOnTrace(calleeData) ;
            TapEnv.printlnOnTrace("    INTO callSiteData:") ;
            if (callSiteData==null) {
                TapEnv.printlnOnTrace("     null, not requested.") ;
            } else {
                TapEnv.dumpOnTrace(callSiteData) ;
            }
            TapEnv.printlnOnTrace("    Instruction:" + instruction + " CallArrow:" + callArrow);
            TapEnv.printlnOnTrace("    passesThroughCall:"+passesThroughCall) ;
            TapEnv.printlnOnTrace("    passesAroundCall: "+passesAroundCall) ;
            TapEnv.printlnOnTrace("    Callee Mask:      "+calleeMask) ;
        }
        //TODO: coversFurther must be made specific to the particular call site, not only to the callArrow:
        BoolVector coversFurther = callArrow.coversFurther ;
        // Useful to force do nothing if an elementary argument is neither read nor written
        // (otherwise propagation might propagate too much if arrowZoneInfo.coversFurther)
        BoolVector calleeZonesPossiblyRorW = calledUnit.unitInOutPossiblyRorW();
        SymbolTable callSiteSymbolTable = instruction.block.symbolTable;
        int nbParams = ILUtils.getArguments(callTree).length() ;
        boolean allTreesGiven = (actualResultTree!=null && actualParamTreeS!=null) ;
        if (allTreesGiven) {
            for (int i=nbParams-1 ; allTreesGiven && i>=0 ; --i) {
                if (actualParamTreeS[i]==null) allTreesGiven = false ;
            }
        }
        TapList[] callSiteParamDataS = (allTreesGiven ? null : new TapList[nbParams+1]) ;
        // TODO: use rather the precomputed value! (but this method changes it, see "valuesCarryInfo"...)
        // TapList[] actualParamZonesTreeS = callTree.getAnnotation("actualParamZonesTreeS") ;
        // boolean[] actualParamTotalS = callTree.getAnnotation("actualParamTotalS") ;
        TapList[] actualParamZonesTreeS = new TapList[nbParams];
        boolean[] actualParamTotalS = new boolean[nbParams];
        TapList actualResultZonesTree = null;
        ToBool total = new ToBool(false) ;

        // Prepare data about actual arguments, useful for their transfer from callee to call site:
        TapList actualParamZonesTree;
        for (int i=nbParams ; i>0; --i) {
            if (actualParamTreeS==null) {
                callSiteParamDataS[i] = new TapList<>(null, null);
            } else {
                Tree argTree = actualParamTreeS[i-1] ;
                // If F(T)->C(T*), add an op_addressOf around argTree:
                if (callArrow.origin.isFortran() && calledUnit.isC()
                    && calleeExpectsAddress(calledUnit.formalArgumentType(i))) {
                    argTree = ILUtils.build(ILLang.op_address, ILUtils.copy(argTree)) ;
                }
                total.set(false) ;
                actualParamZonesTree =
                    callSiteSymbolTable.treeOfZonesOfValue(argTree, total, instruction, null);
                if (actualParamZonesTree != null) {
                    actualParamZonesTree = TapList.copyTree(actualParamZonesTree);
                    includePointedElementsInTree(actualParamZonesTree, total, null, true,
                            callSiteSymbolTable, instruction, false, true);
                    // Parameters passed by value do not propagate their exit status forward :
                    if (callArrow.takesArgumentByValue(i) && !valuesCarryInfo) {
                        eraseInfoOnTopLevel(actualParamZonesTree);
                    }
                }
                actualParamZonesTreeS[i-1] = actualParamZonesTree;
                actualParamTotalS[i-1] = total.get() ;
                if (traceHere) {
                    TapEnv.printlnOnTrace("actualParamZonesTreeS[" +(i-1)+ "] == " + actualParamZonesTreeS[i-1] + " (total:"+actualParamTotalS[i-1]+") for " + actualParamTreeS[i-1]);
                }
            }
        }
        if (actualResultTree == null) {
            callSiteParamDataS[0] = new TapList<>(null, null);
        } else {
            actualResultZonesTree =
                    callSiteSymbolTable.treeOfZonesOfValue(actualResultTree, null, instruction, null);
        }

        if (callSiteData!=null) {
            // Wipe out all info in callSiteData (transit) that cannot reach because "killed by call" (i.e. does not "pass around call")
            if (passesAroundCall!=null) {
                for (int i=callSiteData.nRows-1 ; i>=0 ; --i) {
                    if (!passesAroundCall.get(i)) {
                        callSiteData.setRow(i, new BoolVector(callSiteData.nCols)) ;
                    }
                }
            }
            // Copy info on globals. Their ordering is the same for caller and for callee
            // (except for pointer's initial destinations, see "replaceInitialsByActuals()")
            int globalKindZoneNb = calledUnit.globalZonesNumber(whichKind) ; // ?could it be curCallGraph.globalZonesNumber4[whichKind]?
            BoolMatrix dataCopy = new BoolMatrix(callSiteData.nRows, calleeData.nCols) ;
            dataCopy.rows = new BoolVector[dataCopy.nRows] ;
            for (int i=0 ; i<dataCopy.nRows ; ++i) {
                if (i<calleeData.nRows && (calleeMask==null || calleeMask.get(i))) {
                    dataCopy.rows[i] = calleeData.rows[i] ;
                } else {
                    dataCopy.rows[i] = null ;
                }
            }
            PointerAnalyzer.replaceInitialsByActuals(dataCopy, callTree, whichKind) ;
            for (int i=0 ; i<callSiteData.nRows ; ++i) {
                if (i<globalKindZoneNb && dataCopy.rows[i]!=null) {
                    updateDataInTransit(callSiteData, i, dataCopy.rows[i], passesThroughCall, passesAroundCall) ;
                }
            }
            if (traceHere) {
                TapEnv.printlnOnTrace("  callSiteData after wiping killed and copying "+globalKindZoneNb+" globals:") ;
                TapEnv.dumpOnTrace(callSiteData) ;
            }
        }

        // Now transfer info specifically about actual arguments:
        TapList toLocation;
        int globalZoneNb = calledUnit.globalZonesNumber(SymbolTableConstants.ALLKIND) ;
        Tree zoneAccessTree ;
        ToBool toSkipZI = new ToBool(false) ;
        for (int i=calledUnit.paramElemsNb()-1 ; i>=globalZoneNb ; --i) {
          ZoneInfo zi = calledUnit.paramElemZoneInfo(i) ;
          int ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zi.kindZoneNb(whichKind)) ;
          BoolVector calleeZoneData = null ;
          boolean accumulateData = false ;
          if (ki!=-1 && (calleeMask==null || calleeMask.get(ki))) {
              calleeZoneData = calleeData.rows[ki];
              accumulateData = true ;
          } else if (zi.isResult()) {
              // Special case (cf set11/aaf08): the result can never depend on its "entry" value.
              // therefore we must build a default info which is explicitly empty:
              calleeZoneData = new BoolVector(calleeData.nCols) ;
              accumulateData = true ;
          }
          if (accumulateData) {
            switch (zi.kind()) {
                case SymbolTableConstants.PARAMETER: {
                    if (traceHere) {
                        TapEnv.printlnOnTrace("  callee data is "+calleeZoneData+" for elementary param #"+i+" from param#"+zi.index+", ZoneInfo:"+zi+" kindZone:"+ki) ;
                    }
                    int paramRk = zi.index ;
                    if (0<paramRk && paramRk<=nbParams) {
                        if (callArrow.origin.isFortran() && calledUnit.isC()) {
                            zoneAccessTree = zi.accessTree ;
                        } else {
                            zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                        }
                        if (!toSkipZI.get()) {
                          if (actualParamTreeS == null) {
                            if (traceHere) {
                                TapEnv.printlnOnTrace("   getSetFieldLocation of accessTree of "+zi
                                                      +" INSIDE "+callSiteParamDataS[paramRk]);
                            }
                            toLocation =
                                TapList.getSetFieldLocation(callSiteParamDataS[paramRk], zoneAccessTree, true);
                            if (traceHere) {
                                TapEnv.printlnOnTrace("   -> "+toLocation+" INSIDE "
                                                      +callSiteParamDataS[paramRk]+" WILL RECEIVE "+calleeZoneData);
                            }
                            if (calleeZonesPossiblyRorW.get(i)) {
                                if (toLocation.head instanceof BoolVector) {
                                    ((BoolVector)toLocation.head).cumulOr(calleeZoneData) ;
                                } else {
                                    toLocation.head = calleeZoneData ;
                                }
                            }
                          } else if (callSiteData != null) {
                            if (traceHere) {
                                TapEnv.printlnOnTrace("   zone:"+zi+" coversFurther:"+(coversFurther!=null && coversFurther.get(i)));
                                TapEnv.printlnOnTrace("   accessTree:" + zoneAccessTree);
                            }
                            TapIntList callSiteAccessedIndices =
                                mapZoneRkToKindZoneRk(
                                        ZoneInfo.listAllZones(
                                                TapList.getSetFieldLocation(
                                                        actualParamZonesTreeS[paramRk-1],
                                                        zoneAccessTree, false),
                                                coversFurther!=null && coversFurther.get(i)),
                                        whichKind, callSiteSymbolTable) ;
                            if (traceHere) {
                                TapEnv.printlnOnTrace("   callSiteAccessedIndices:" + callSiteAccessedIndices);
                            }
                            while (callSiteAccessedIndices!=null) {
                                updateDataInTransit(callSiteData, callSiteAccessedIndices.head, calleeZoneData,
                                                    passesThroughCall, passesAroundCall) ;
                                callSiteAccessedIndices = callSiteAccessedIndices.tail ;
                            }

                          }
                        }
                    }
                    break ;
                }
                case SymbolTableConstants.RESULT: {
                    if (traceHere) {
                        TapEnv.printlnOnTrace("  callee data is "+calleeZoneData+" for elementary param #"+i+" from RESULT, ZoneInfo:"+zi+" kindZone:"+ki) ;
                    }
                    if (actualResultTree == null) {
                        toLocation = TapList.getSetFieldLocation(callSiteParamDataS[0], zi.accessTree, true);
                        if (toLocation.head instanceof BoolVector) {
                            ((BoolVector)toLocation.head).cumulOr(calleeZoneData) ;
                        } else {
                            toLocation.head = calleeZoneData ;
                        }
                    } else if (callSiteData != null) {
                        TapIntList callSiteAccessedIndices =
                            mapZoneRkToKindZoneRk(
                                    ZoneInfo.listAllZones(
                                            TapList.getSetFieldLocation(
                                                    actualResultZonesTree,
                                                    zi.accessTree, false),
                                            false),
                                    whichKind, callSiteSymbolTable);
                        while (callSiteAccessedIndices!=null) {
                            updateDataInTransit(callSiteData, callSiteAccessedIndices.head, calleeZoneData,
                                                null, passesAroundCall) ;
                            callSiteAccessedIndices = callSiteAccessedIndices.tail ;
                        }

                    }
                    break ;
                }
            }
          } else {
              // Accumulate no data, but still the resulting callSiteParamDataS[] must hold the
              // structure of the argument's zone tree. Otherwise setInfoPRZVTree() might not work properly.
              switch (zi.kind()) {
                  case SymbolTableConstants.RESULT: {
                      if (actualResultTree==null)
                          TapList.getSetFieldLocation(callSiteParamDataS[0], zi.accessTree, true);
                      break ;
                  }
                  case SymbolTableConstants.PARAMETER: {
                      int paramRk = zi.index ;
                      if (0<paramRk && paramRk<=nbParams
                          && (actualParamTreeS==null || actualParamTreeS[paramRk-1]==null)) {
                          if (callArrow.origin.isFortran() && calledUnit.isC()) {
                              zoneAccessTree = zi.accessTree ;
                          } else {
                              zoneAccessTree = zi.crossLanguageAccessTree(callArrow, toSkipZI, false) ;
                          }
                          TapList.getSetFieldLocation(callSiteParamDataS[paramRk], zoneAccessTree, true);
                      }
                      break ;
                  }
              }
          }
        }
        if (callSiteParamDataS!=null) {
            for (int i=1 ; i<callSiteParamDataS.length ; ++i) {
                // If info is not "naturally" carried and this is PASS-BY-VALUE, erase top-level of info:
                if (!valuesCarryInfo && callArrow.takesArgumentByValue(i)) {
                    eraseInfoOnTopLevel(callSiteParamDataS[i]);
                }
                // If F(T)->C(T*), remove the pointer level on top of info:
                if (callSiteParamDataS[i]!=null && callArrow.origin.isFortran() && calledUnit.isC()
                    && calleeExpectsAddress(calledUnit.formalArgumentType(i))) {
                    callSiteParamDataS[i] = callSiteParamDataS[i].tail ;
                }
            }
        }
        if (traceHere) {
            TapEnv.printlnOnTrace("   final callSiteData:") ;
            if (callSiteData==null) {
                TapEnv.printlnOnTrace("     null, not requested.") ;
            } else {
                TapEnv.dumpOnTrace(callSiteData) ;
            }
            if (callSiteParamDataS != null) {
                for (int i = 0; i < callSiteParamDataS.length; ++i) {
                    TapEnv.printlnOnTrace("   callSiteParamDataS["+i+(i==0?"=result":"")+"] = "+callSiteParamDataS[i]);
                }
            }
            TapEnv.printlnOnTrace("------------------- end translateCalleeDataToCallSite (BoolMatrix) -------------") ;
        }
        return callSiteParamDataS;
    }

    private static boolean calleeExpectsAddress(TypeSpec cType) {
        // We just check that C receiving type is a pointer, which we believe (?) implies
        // that the Fortran actual argument type is non-pointer and C type is a pointer to it.
        return TypeSpec.isA(cType, SymbolTableConstants.POINTERTYPE) ;
    }

    private static void updateDataInTransit(BoolVector dataInTransit, int index, boolean value,
                                     BoolVector passesThroughCall, BoolVector passesAroundCall) {
        boolean addInfoThroughCall = (passesThroughCall==null || passesThroughCall.get(index)) ;
        if (addInfoThroughCall) {
            boolean alsoInfoBeforeCall = (passesAroundCall==null || passesAroundCall.get(index)) ;
            if (alsoInfoBeforeCall) {
                if (value) dataInTransit.set(index, true) ;
            } else {
                dataInTransit.set(index, value) ;
            }
        }
    }

    private static void updateDataInTransit(BoolMatrix dataInTransit, int index, BoolVector rowThroughCall,
                                     BoolVector passesThroughCall, BoolVector passesAroundCall) {
        boolean addInfoThroughCall = (passesThroughCall==null || passesThroughCall.get(index)) ;
        if (addInfoThroughCall) {
            boolean alsoInfoBeforeCall = (passesAroundCall==null || passesAroundCall.get(index)) ;
            if (alsoInfoBeforeCall) {
                if (rowThroughCall!=null) {
                    if (dataInTransit.rows[index]==null) {
                        // make it explicit-identity
                        dataInTransit.rows[index] = new BoolVector(dataInTransit.nCols) ;
                        dataInTransit.rows[index].set(index, true) ;
                    }
                    dataInTransit.rows[index].cumulOr(rowThroughCall) ;
                }
            } else {
                dataInTransit.rows[index] = rowThroughCall ;
            }
        }
    }

    /**
     * Builds a BoolVector with 1 only for the Unit's zones of type "pointer"
     * which point to a type which is differentiated.
     */
    public static BoolVector buildUnitPointerZoneMask(Unit unit) {
        int length = unit.paramElemsNb();
        BoolVector result = new BoolVector(length);
        result.setFalse();
        WrapperTypeSpec pType;
        WrapperTypeSpec dType;
        for (int i = length - 1; i >= 0; i--) {
            pType = unit.paramElemZoneInfo(i).type;
            if (TypeSpec.isA(pType, SymbolTableConstants.POINTERTYPE)) {
                dType = ((PointerTypeSpec) pType.wrappedType).destinationType;
                if (dType == null || dType.wrappedType == null
                    || TypeSpec.isA(dType.wrappedType, SymbolTableConstants.METATYPE)
                    || TypeSpec.isA(dType.wrappedType, SymbolTableConstants.VOIDTYPE)
                    || dType.wrappedType.isDifferentiated(null)) {
                    result.set(i, true);
                }
            }
        }
        return result;
    }

    /**
     * Extends a given tree of zones with the destination of pointers.
     * <br><b>Globals required:</b> {@link #curInstruction},
     * {@link #curSymbolTable}, {@link #curCalledUnit}
     *
     * @param zonesTree    The tree of zones to be extended.
     * @param total        When passed a non-null ToBool, it is set to false if some pointed zone was included.
     * @param pointerDests The pointer dests matrix. It is unnecessary after PointerAnalyzer has run,
     *                     because the needed info is then stored at the level of the curBlock and curInstruction.
     * @param recursive    if true, recursively call across pointers.
     * @param isCalled     is true when we are on a function call and therefore
     *                     the zonesTree may contain zones of rank greater than the last declared
     *                     zone and these extra zones represent the formal parameters of the current call.
     *                     If isCalled is false, then the extra zones represent split variables.
     * @param upstream     when true, the pointer destination info is retrieved upstream
     *                     of the curInstruction, otherwise the info downstream is used.
     */
    public void includePointedElementsInTree(TapList zonesTree, ToBool total,
                                             BoolMatrix pointerDests, boolean recursive,
                                             boolean isCalled, boolean upstream) {
        int nbDestsZones = curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector topDestZones = new BoolVector(nbDestsZones);
        TapIntList trueRanks = ZoneInfo.listAllZones(zonesTree, false);
        topDestZones.set(trueRanks, true);
        includePointedElementsInTreeRec(zonesTree,
                new TapList<>(null, null), new TapList<>(null, null),
                new TapList<>(new TapPair<>(topDestZones, zonesTree), null),
                total, pointerDests, recursive, curSymbolTable,
                curInstruction, isCalled, upstream, nbDestsZones);
    }

    /**
     * Static version of includePointedElementsInTree().
     */
    public static void includePointedElementsInTree(TapList zonesTree, ToBool total,
                                                    BoolMatrix pointerDests, boolean recursive,
                                                    SymbolTable symbolTable, Instruction instruction,
                                                    boolean isCalled, boolean upstream) {
        int nbDestsZones = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND) ;
        BoolVector topDestZones = new BoolVector(nbDestsZones);
        TapIntList trueRanks = ZoneInfo.listAllZones(zonesTree, false) ;
        while (trueRanks!=null) {
            if (trueRanks.head<nbDestsZones) { // because of TmpZones introduced by BlockDifferentiator:
                topDestZones.set(trueRanks.head, true) ;
            }
            trueRanks = trueRanks.tail ;
        }
        includePointedElementsInTreeRec(zonesTree,
                new TapList<>(null, null), new TapList<>(null, null),
                new TapList<>(new TapPair<>(topDestZones, zonesTree), null),
                total, pointerDests, recursive, symbolTable,
                instruction, isCalled, upstream, nbDestsZones);
    }

    /**
     * Recursive implementation of includePointedElementsInTree() with "dejaVu" mechanism.
     * When reaching a zonesTree for a pointer, for which the location for destination has
     * not been made, e.g. when zonesTree == (&lt;z*&gt;), and the z* are pointer zones,
     * then we build the zones tree for the pointers' destinations "allDestinations",
     * and place it into "zonesTree" so that it becomes
     * zonesTree == (&lt;z*&gt; | allDestinations).
     */
    private static void includePointedElementsInTreeRec(TapList zonesTree,
                                                        TapList<TapList> dejaVu, TapList dejaVuPointerLists,
                                                        TapList<TapPair<BoolVector, TapList<?>>> dejaVuPointerDests,
                                                        ToBool total, BoolMatrix pointerDests, boolean recursive, SymbolTable symbolTable,
                                                        Instruction instruction, boolean isCalled, boolean upstream,
                                                        int nbDestsZones) {
        while (zonesTree != null) {
            if (TapList.contains(dejaVu.tail, zonesTree)) {
                zonesTree = null;
            } else {
                dejaVu.placdl(zonesTree);
                if (zonesTree.head instanceof TapList) {
                    //Record field case: go into it, then go right.
                    includePointedElementsInTreeRec((TapList) zonesTree.head, dejaVu, dejaVuPointerLists, dejaVuPointerDests,
                            total, pointerDests, recursive, symbolTable,
                            instruction, isCalled, upstream, nbDestsZones);
                    zonesTree = zonesTree.tail;
                } else if (!(zonesTree.head instanceof TapIntList)) {
                    //Wierd case: skip head, then go right.
                    zonesTree = zonesTree.tail;
                } else if (zonesTree.tail == null) {
                    //Leaf case, maybe pointer. Nothing included yet: include destinations, then go right if recursive
                    TapList<?> alreadyDone = pointerListAlreadyDone((TapIntList) zonesTree.head, dejaVuPointerLists.tail);
                    if (alreadyDone != null) {
                        // if this (set of) pointer(s) was already seen and done, we re-use its computed destinations:
                        // this will probably make the created tree of zones cyclic!
                        zonesTree.tail = alreadyDone.tail;
                        // then we must not recurse, to avoid infinite looping:
                        zonesTree = null;
                    } else {
                        BoolVector destZones = buildPointerDestinationZones((TapIntList) zonesTree.head, nbDestsZones,
                                pointerDests, symbolTable, instruction, isCalled, upstream);
                        TapPair<BoolVector, TapList<?>> alreadyDoneDests =
                                destZones == null ? null : pointerDestsAlreadyDone(destZones, dejaVuPointerDests, nbDestsZones);
                        if (alreadyDoneDests != null) {
                            // if the destinations of this (set of) pointer(s) were already built as a tree,
                            // re-use the built tree of zones. This will probably make the created tree of zones cyclic!
                            zonesTree.tail = alreadyDoneDests.second;
                            // then we must not recurse, to avoid infinite looping:
                            zonesTree = null;
                        } else {
                            zonesTree.tail = buildPointerDestinationTree((TapIntList) zonesTree.head,
                                    total, pointerDests, symbolTable, instruction,
                                    isCalled, upstream);
                            dejaVuPointerLists.placdl(zonesTree);
                            if (destZones != null) {
                                assert dejaVuPointerDests != null;
                                dejaVuPointerDests.placdl(new TapPair<>(destZones, zonesTree.tail));
                            }
                            zonesTree = recursive ? zonesTree.tail : null;
                        }
                    }
                } else {
                    //Leaf case, maybe pointer, but destinations already included: skip and don't go right.
                    zonesTree = null;
                }
            }
        }
    }

    /**
     * Build and return the BoolVector of all possible destinations at this location of any pointer in zonesList.
     * Special case: if there is no pointer in zonesList, returns null.
     */
    private static BoolVector buildPointerDestinationZones(TapIntList zonesList, int nbDestsZones,
                                                           BoolMatrix pointerDests, SymbolTable symbolTable, Instruction instruction,
                                                           boolean isCalled, boolean upstream) {
        Block block;
        if (instruction != null) {
            block = instruction.block;
        } else {
            block = symbolTable.declarationsBlock;
        }
        BoolVector result = new BoolVector(nbDestsZones);
        BoolVector newDests;
        int pointerRowIndex;
        boolean hasPointers = false;
        while (zonesList != null) {
            pointerRowIndex =
                    zoneRkToKindZoneRk(zonesList.head, SymbolTableConstants.PTRKIND, symbolTable);
            if (pointerRowIndex != -1) {
                hasPointers = true;
                newDests = symbolTable.getPointerDestinationVector(pointerRowIndex, instruction, block, pointerDests, upstream);
                if (newDests != null) {
                    result.cumulOr(newDests);
                }
            }
            zonesList = zonesList.tail;
        }
        return hasPointers ? result : null;
    }

    private static TapPair<BoolVector, TapList<?>> pointerDestsAlreadyDone(
            BoolVector destZones, TapList<TapPair<BoolVector, TapList<?>>> dejaVuPointerDests, int nbDestsZones) {
        TapPair<BoolVector, TapList<?>> found = null;
        while (dejaVuPointerDests != null && found == null) {
            if (destZones.equals(dejaVuPointerDests.head.first, nbDestsZones)) {
                found = dejaVuPointerDests.head;
            }
            dejaVuPointerDests = dejaVuPointerDests.tail;
        }
        return found;
    }

    /**
     * Build and return the TapList tree of all possible destinations at this location of any pointer in zonesList.
     */
    private static TapList buildPointerDestinationTree(TapIntList zonesList, ToBool total,
                                                       BoolMatrix pointerDests, SymbolTable symbolTable, Instruction instruction,
                                                       boolean isCalled, boolean upstream) {
        TapList allDestinations = null;
        ZoneInfo zoneInfo;
        int zoneRk;
        while (zonesList != null) {
            zoneRk = zonesList.head;
            zoneInfo = symbolTable.declaredZoneInfo(zoneRk, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null && zoneInfo.ptrZoneNb != -1) {
                TapList<?> newDestination =
                        symbolTable.getPointerDestinationZonesTreeHere(
                                new TapIntList(zoneRk, null),
                                instruction, null, pointerDests,
                                ((PointerTypeSpec) zoneInfo.type.wrappedType).destinationType,
                                null, upstream);
                if (newDestination != null) {
                    allDestinations =
                            TapList.cumulWithOper(allDestinations, newDestination, SymbolTableConstants.CUMUL_OR);
                }
            }
            zonesList = zonesList.tail;
        }
        if (total != null && TapList.oneNonSingleton(allDestinations)) {
            total.set(false);
        }
        return allDestinations;
    }

    private static TapList<?> pointerListAlreadyDone(TapIntList zonesList, TapList<?> dejaVuPointerLists) {
        TapList found = null;
        while (found == null && dejaVuPointerLists != null) {
            if (TapIntList.equalLists((TapIntList) ((TapList) dejaVuPointerLists.head).head, zonesList)) {
                found = (TapList) dejaVuPointerLists.head;
            }
            dejaVuPointerLists = dejaVuPointerLists.tail;
        }
        return found;
    }

    /**
     * For future dataflow analysis.
     */
    private static boolean isPointerZones(int paramRank, Tree[] actualParamTreeS, SymbolTable symbolTable,
                                          Instruction instruction, Unit calledUnit) {
        boolean result = false;
        TapList<?> zonesTree =
                symbolTable.treeOfZonesOfValue(actualParamTreeS[paramRank], new ToBool(), instruction, null);
        if (zonesTree != null && zonesTree.head instanceof TapIntList) {
            int zoneRk = ((TapIntList) zonesTree.head).head;
            result = zoneIsPointer(zoneRk, symbolTable, calledUnit);
        }
        return result;
    }

    /**
     * Removes the info that concern parts of an argument that are passed by value.
     */
    public static void eraseInfoOnTopLevel(TapList<?> zonesTree) {
        if (zonesTree != null) {
            if (zonesTree.head instanceof TapList) {
                // We are on a structure: recursive case.
                while (zonesTree != null) {
                    eraseInfoOnTopLevel((TapList) zonesTree.head);
                    zonesTree = zonesTree.tail;
                }
            } else {
                // We are above a leaf. This leaf must be removed because it is
                // passed by value (i.e. copied).
                // If the zoneTree has a cdr, then it is a pointer target,
                // and therefore it is passed by reference.
                zonesTree.head = null;
            }
        }
    }

    /**
     * Removes the info that concern parts of an argument that are passed by value,
     * and replaces the removed values with the given "replacementValue".
     */
    public static void eraseInfoOnTopLevel(TapList zonesTree, Object replacementValue) {
        if (zonesTree != null) {
            if (zonesTree.head instanceof TapList)
            // We are on a structure: recursive case.
            {
                while (zonesTree != null) {
                    eraseInfoOnTopLevel((TapList) zonesTree.head, replacementValue);
                    zonesTree = zonesTree.tail;
                }
            } else
            // We are above a leaf. This leaf must be removed because it is
            // passed by value (i.e. copied).
            // If the zoneTree has a cdr, then it is a pointer target,
            // and therefore it is passed by reference.
            {
                zonesTree.head = replacementValue;
            }
        }
    }

    /**
     * Sweeps through the given "boolVectorTree", which must be a tree of
     * BoolVector's. OR-accumulates all BoolVector leaves of this tree into BoolVector "result".
     *
     * @param followPointers when false, skips the parts of boolVectorTree that deal with pointers,
     *                       i.e. that can only be accessed through a pointer deref.
     */
    protected static void cumulOr(TapList<?> boolVectorTree, BoolVector result, boolean followPointers) {
        cumulOrRec(boolVectorTree, result, followPointers, new TapList<>(null, null));
    }

    private static void cumulOrRec(TapList<?> boolVectorTree, BoolVector result, boolean followPointers, TapList<TapList> dejaVu) {
        while (boolVectorTree != null) {
            if (TapList.contains(dejaVu.tail, boolVectorTree)) {
                boolVectorTree = null;
            } else {
                dejaVu.placdl(boolVectorTree);
                if (boolVectorTree.head instanceof TapList) {
                    cumulOrRec((TapList) boolVectorTree.head, result, followPointers, dejaVu);
                    boolVectorTree = boolVectorTree.tail;
                } else if (boolVectorTree.head instanceof BoolVector) {
                    result.cumulOr((BoolVector) boolVectorTree.head);
                    boolVectorTree = followPointers ? boolVectorTree.tail : null;
                } else {
                    boolVectorTree = boolVectorTree.tail;
                }
            }
        }
    }

    /**
     * OR-Accumulates the given BoolVector "info" into
     * each BoolVector leaf of "infoTree".
     */
    public static void orInfoPRZVTrees(BoolVector info, TapList<?> infoTree) {
        orInfoPRZVTreesRec(info, infoTree, new TapList<>(null, null));
    }

    private static void orInfoPRZVTreesRec(BoolVector info, TapList infoTree, TapList<TapList> dejaVu) {
        while (infoTree != null) {
            if (TapList.contains(dejaVu.tail, infoTree)) {
                infoTree = null;
            } else {
                dejaVu.placdl(infoTree);
                if (infoTree.head instanceof TapList) {
                    orInfoPRZVTreesRec(info, (TapList) infoTree.head, dejaVu);
                } else if (infoTree.head == null) {
                    infoTree.head = info.copy();
                } else {
                    ((BoolVector) infoTree.head).cumulOr(info);
                }
                infoTree = infoTree.tail;
            }
        }
    }

    /**
     * True if one may run the analysis in a special way that is able
     * to detect full access to arrays, instead of considering all properties
     * on arrays as undecidable.
     */
    public static boolean runSpecialCycleAnalysis(Block block) {
        return block instanceof HeaderBlock && block.isACleanDoLoop();
    }

    /**
     * @return the number of common declared zones of all destinations of the given "frontier".
     */
    public static int commonNDZofFrontierDestinations(TapList<FGArrow> frontier, int kind) {
        int result = -1;
        int newndz;
        FGArrow arrow;
        while (frontier != null) {
            arrow = frontier.head;
            newndz = arrow.destination.symbolTable.declaredZonesNb(kind);
            if (result == -1 || result > newndz) {
                result = newndz;
            }
            frontier = frontier.tail;
        }
        return Math.max(result, 0);
    }

    /**
     * @return the number of common declared zones of all origins of the given "frontier".
     */
    public static int commonNDZofFrontierOrigins(TapList<FGArrow> frontier, int kind) {
        int result = -1;
        int newndz;
        FGArrow arrow;
        while (frontier != null) {
            arrow = frontier.head;
            newndz = arrow.origin.symbolTable.declaredZonesNb(kind);
            if (result == -1 || result > newndz) {
                result = newndz;
            }
            frontier = frontier.tail;
        }
        return Math.max(result, 0);
    }

    /**
     * @return the number of zones that flow through all arrows of the given "frontier".
     * It is the min of the number of zones that can flow through each arrow.
     */
    public static int commonNDZofFrontierArrows(TapList<FGArrow> frontier, int kind) {
        int result = -1;
        int newndz;
        FGArrow arrow;
        while (frontier != null) {
            arrow = frontier.head;
            newndz = arrow.commonSymbolTable().declaredZonesNb(kind);
            if (result == -1 || result > newndz) {
                result = newndz;
            }
            frontier = frontier.tail;
        }
        return Math.max(result, 0);
    }

    /**
     * When this index "declaredIndex" in this block "block" has been detected
     * as a "unique-access" zone, i.e. a zone which is accessed always for the same
     * unique single cell during the enclosing loop, then the typical accessTree
     * must be the access tree for this cell only (cf F77:lh09).
     *
     * @return this new typical accessTree. Otherwise returns the old "accessTree".
     */
    public static Tree modifyAccessTreeForUniqueAccess(int declaredIndex, Tree accessTree,
                                                       Block block) {
        Tree typicalAccessTree = accessTree;
        LoopBlock enclosingLoop = block.enclosingLoop();
        if (enclosingLoop != null) {
            TapIntList uniqueAccessZones = enclosingLoop.header().uniqueAccessZones;
            TapList<Tree> uniqueAccessTrees = enclosingLoop.header().uniqueAccessTrees;
            while (uniqueAccessZones != null && uniqueAccessZones.head != declaredIndex) {
                uniqueAccessZones = uniqueAccessZones.tail;
                uniqueAccessTrees = uniqueAccessTrees.tail;
            }
            if (uniqueAccessZones != null) {
                typicalAccessTree = uniqueAccessTrees.head;
            }
        }
        return typicalAccessTree;
    }

    /**
     * @return a String with the names of all variables for which "selectedZones" is true.
     */
    public static String namesOfZones(BoolVector selectedZones, int whichKind, Unit unit) {
        ZoneInfo zoneInfo;
        String varName;
        String namesList = null;
        int ki ;
        for (int i=unit.paramElemsNb()-1 ; i>=0 ; --i) {
            zoneInfo = unit.paramElemZoneInfo(i);
            ki = (whichKind==SymbolTableConstants.ALLKIND ? i : zoneInfo.kindZoneNb(whichKind)) ;
            if (ki!=-1 && selectedZones.get(ki)) {
                varName = zoneInfo.accessTreePrint(unit.language());
                if (namesList == null) {
                    namesList = "";
                } else {
                    namesList = ", " + namesList;
                }
                namesList = varName + namesList;
            }
        }
        return namesList;
    }

    /**
     * @return a new (TapList) tree of Boolean's,
     * following the pattern of the given (TapList) tree of zones "zonesTree",
     * for which each TapIntList leaf becomes a Boolean which is true iff this TapIntList
     * of extended declared zones intersects the given "info"+"extraInfoZones".
     */
    public static TapList<?> buildInfoBoolTreeOfDeclaredZones(TapList<?> zonesTree,
                                                              BoolVector info, TapIntList extraInfoZones,
                                                              int whichKind, SymbolTable symbolTable) {
        return buildInfoBoolTreeOfDeclaredZonesRec(zonesTree, info, extraInfoZones,
                whichKind, symbolTable, new TapList<>(null, null));
    }

    private static TapList<?> buildInfoBoolTreeOfDeclaredZonesRec(TapList<?> zonesTree,
                                                                  BoolVector info, TapIntList extraInfoZones,
                                                                  int whichKind, SymbolTable symbolTable,
                                                                  TapList<TapPair<TapList, TapList>> dejaVu) {
        TapList hdBoolsTree = new TapList<>(null, null);
        TapList tlBoolsTree = hdBoolsTree;
        Object boolsSubTree;
        while (zonesTree != null) {
            TapList cycle = TapList.cassq(zonesTree, dejaVu.tail);
            if (cycle != null) {
                tlBoolsTree.tail = cycle.tail;
                zonesTree = null;
            } else {
                dejaVu.placdl(new TapPair<>(zonesTree, tlBoolsTree));
                if (zonesTree.head instanceof TapList) {
                    boolsSubTree = buildInfoBoolTreeOfDeclaredZonesRec((TapList) zonesTree.head,
                            info, extraInfoZones, whichKind, symbolTable, dejaVu);
                } else {
                    boolsSubTree = info != null &&
                            intersectsKindZoneRks(info, whichKind,
                                    (TapIntList)zonesTree.head,
                                    extraInfoZones, symbolTable);
                }
                tlBoolsTree = tlBoolsTree.placdl(boolsSubTree);
                zonesTree = zonesTree.tail;
            }
        }
        return hdBoolsTree.tail;
    }

    /**
     * Same as buildInfoBoolTreeOfDeclaredZones, but places a True only if ALL the list
     * of zones at this leaf are true for the given "info"+"extraInfoZones".
     */
    public static TapList<?> buildInfoBoolTreeOfDeclaredZonesAll(TapList<?> zonesTree,
                                                                 BoolVector info, TapIntList extraInfoZones,
                                                                 int whichKind, SymbolTable symbolTable) {
        return buildInfoBoolTreeOfDeclaredZonesAllRec(zonesTree, info, extraInfoZones,
                whichKind, symbolTable, new TapList<>(null, null));
    }

    private static TapList buildInfoBoolTreeOfDeclaredZonesAllRec(TapList zonesTree,
                                                                  BoolVector info, TapIntList extraInfoZones,
                                                                  int whichKind, SymbolTable symbolTable,
                                                                  TapList<TapPair<TapList, TapList>> dejaVu) {
        TapList hdBoolsTree = new TapList<>(null, null);
        TapList tlBoolsTree = hdBoolsTree;
        Object boolsSubTree;
        while (zonesTree != null) {
            TapList cycle = TapList.cassq(zonesTree, dejaVu.tail);
            if (cycle != null) {
                tlBoolsTree.tail = cycle.tail;
                zonesTree = null;
            } else {
                dejaVu.placdl(new TapPair<>(zonesTree, tlBoolsTree));
                if (zonesTree.head instanceof TapList) {
                    boolsSubTree = buildInfoBoolTreeOfDeclaredZonesAllRec((TapList) zonesTree.head,
                            info, extraInfoZones, whichKind, symbolTable, dejaVu);
                } else {
                    boolsSubTree = info != null &&
                            containsExtendedDeclared(info, whichKind,
                                    (TapIntList) zonesTree.head,
                                    extraInfoZones, symbolTable, null);
                }
                tlBoolsTree = tlBoolsTree.placdl(boolsSubTree);
                zonesTree = zonesTree.tail;
            }
        }
        return hdBoolsTree.tail;
    }

    /**
     * @return a new (TapList) tree of Boolean's,
     * following the pattern of the given (TapList) tree of zones "zonesTree",
     * in which a TapIntList leaf is true iff this zone is a pointer.
     */
    public static TapList buildInfoBoolTreeOfPointers(TapList zonesTree, SymbolTable symbolTable) {
        return buildInfoBoolTreeOfPointersRec(zonesTree, symbolTable, new TapList<>(null, null));
    }

    private static TapList buildInfoBoolTreeOfPointersRec(TapList zonesTree, SymbolTable symbolTable,
                                                          TapList<TapPair<TapList, TapList>> dejaVu) {
        TapList hdBoolsTree = new TapList<>(null, null);
        TapList tlBoolsTree = hdBoolsTree;
        Object boolsSubTree;
        while (zonesTree != null) {
            TapList cycle = TapList.cassq(zonesTree, dejaVu.tail);
            if (cycle != null) {
                tlBoolsTree.tail = cycle.tail;
                zonesTree = null;
            } else {
                dejaVu.placdl(new TapPair<>(zonesTree, tlBoolsTree));
                if (zonesTree.head instanceof TapList) {
                    boolsSubTree = buildInfoBoolTreeOfPointersRec((TapList) zonesTree.head, symbolTable, dejaVu);
                } else {
                    TapIntList ptrIndices =
                            mapZoneRkToKindZoneRk((TapIntList) zonesTree.head,
                                    SymbolTableConstants.PTRKIND, symbolTable);
                    boolsSubTree = ptrIndices != null;
                }
                tlBoolsTree = tlBoolsTree.placdl(boolsSubTree);
                zonesTree = zonesTree.tail;
            }
        }
        return hdBoolsTree.tail;
    }

    public final void setKindZoneRk(BoolVector info, int infoKind, int zoneRk, boolean value) {
        int kindZoneRk = zoneRkToKindZoneRk(zoneRk, infoKind) ;
        if (kindZoneRk>=0) info.set(kindZoneRk, value);
    }
    public static final void setKindZoneRk(BoolVector info, int infoKind, int zoneRk, boolean value,
                                           SymbolTable symbolTable) {
        info.set(zoneRkToKindZoneRk(zoneRk, infoKind, symbolTable), value); //INLINE IT!
    }

    public static void setZoneRks(BoolVector info, TapIntList zoneRks, boolean value) {
        info.set(zoneRks, value) ; //INLINE IT!!
    }
    public void setKindZoneRks(BoolVector info, int infoKind, TapIntList zoneRks, boolean value) {
        int kindZoneRk ;
        while (zoneRks!=null) {
            kindZoneRk = zoneRkToKindZoneRk(zoneRks.head, infoKind) ;
            if (kindZoneRk>=0) info.set(kindZoneRk, value);
            zoneRks = zoneRks.tail ;
        }
    }
    public static void setKindZoneRks(BoolVector info, int infoKind, TapIntList zoneRks,
                                      boolean value, SymbolTable symbolTable) {
        int kindZoneRk ;
        while (zoneRks!=null) {
            kindZoneRk = zoneRkToKindZoneRk(zoneRks.head, infoKind, symbolTable) ;
            if (kindZoneRk>=0) info.set(kindZoneRk, value);
            zoneRks = zoneRks.tail ;
        }
    }

    /**
     * @return true when one of the zones of the value of "expression" is either an extra "tmp" zone,
     * that appears in the given list of "extraInfoZones" that are assumed to have the property,
     * or it is of the required "kind" and there is a 1 in BoolVector "info" at the index
     * that corresponds to this zone. Fills "total" when provided non null.
     */
    public static boolean intersectsExtendedDeclared(BoolVector info, int kind,
                                                     Tree expression, ToBool total, Instruction instruction,
                                                     TapIntList extraInfoZones, SymbolTable symbolTable) {

        return intersectsKindZoneRks(info, kind,
                ZoneInfo.listAllZones(symbolTable.treeOfZonesOfValue(expression, total, instruction, null), true),
                extraInfoZones, symbolTable);
    }

    /**
     * @return true when "info" contains true for at least one zone in the given
     * TapIntList of extended declared zones or for an extra tmp zone that is in "extraInfoZones".
     */
    public static boolean intersectsExtendedDeclared(BoolVector info, int kind,
                                                     TapIntList extendedDeclaredZones,
                                                     TapIntList extraInfoZones, SymbolTable symbolTable) {
        boolean intersects = false;
        int zone;
        while (!intersects && extendedDeclaredZones != null) {
            zone = extendedDeclaredZones.head;
            if (zone != -1) {
                if (TapIntList.contains(extraInfoZones, zone)) {
                    intersects = true;
                } else if (info != null) {
                    int index = zoneRkToKindZoneRk(zone, kind, symbolTable);
                    if (index>=0) {
                        intersects = info.get(index);
                    }
                }
            }
            extendedDeclaredZones = extendedDeclaredZones.tail;
        }
        return intersects;
    }
    // tentative replacement code:
    public static boolean intersectsZoneRks(BoolVector info, TapIntList zoneRks,
                                            TapIntList extraInfoZones) {
        boolean intersects = false;
        int zoneRk;
        while (!intersects && zoneRks!=null) {
            zoneRk = zoneRks.head;
            if (zoneRk!=-1) {
                if (TapIntList.contains(extraInfoZones, zoneRk)) {
                    intersects = true;
                } else if (info!=null) {
                    intersects = info.get(zoneRk);
                }
            }
            zoneRks = zoneRks.tail;
        }
        return intersects;
    }
    public static boolean intersectsKindZoneRks(BoolVector info, int infoKind, TapIntList zoneRks,
                                                TapIntList extraInfoZones, SymbolTable symbolTable) {
        boolean intersects = false;
        int zoneRk;
        while (!intersects && zoneRks!=null) {
            zoneRk = zoneRks.head;
            if (zoneRk!=-1) {
                if (TapIntList.contains(extraInfoZones, zoneRk)) {
                    intersects = true;
                } else if (info!=null) {
                    zoneRk = zoneRkToKindZoneRk(zoneRk, infoKind, symbolTable);
                    intersects = zoneRk>=0 && info.get(zoneRk);
                }
            }
            zoneRks = zoneRks.tail;
        }
        return intersects;
    }

    /**
     * @return true when all zones in the given list of extended declared zones
     * is either true in "info" or is part of the given "extraInfoZones".
     */
    public static boolean containsExtendedDeclared(BoolVector info, int kind,
                                                   TapIntList extendedDeclaredZones,
                                                   TapIntList extraInfoZones, SymbolTable symbolTable, Unit calledUnit) {
        // replaces obsolete BoolVector.declaredContains() !
        boolean allTrue = true;
        int zone;
        int index;
        while (allTrue && extendedDeclaredZones != null) {
            zone = extendedDeclaredZones.head;
            if (zone != -1 && !TapIntList.contains(extraInfoZones, zone)) {
                index = zoneRkToKindZoneRk(zone, kind, symbolTable);
                if (index>=0 && !info.get(index)) {
                    allTrue = false;
                }
            }
            extendedDeclaredZones = extendedDeclaredZones.tail;
        }
        return allTrue;
    }

    public static boolean zoneIsPointer(int zoneRk,
                                        SymbolTable symbolTable, Unit calledUnit) {
        ZoneInfo zoneInfo = symbolTable.declaredZoneInfo(zoneRk, SymbolTableConstants.ALLKIND);
        return zoneInfo != null && zoneInfo.ptrZoneNb != -1;
    }

    //TODO: replace by calls to Unit.focusToKind or Unit.unfocusFromKind ?
    /**
     * Convert a BoolVector containing an info on zones of kind "oldKind".
     * into the same info (or a subset depending on oldKind/newKind) on zones of kind "newKind".
     * @return the converted info
     */
    public static BoolVector changeKind(BoolVector oldInfo, int oldLength, int oldKind,
                                        int newLength, int newKind, SymbolTable symbolTable) {
        BoolVector newInfo = new BoolVector(newLength);
        newInfo.setFalse();
        ZoneInfo zi;
        int newIndex;
        for (int index=oldLength-1 ; index>=0 ; --index) {
                if (oldInfo.get(index)) {
                    zi = symbolTable.declaredZoneInfo(index, oldKind);
                    if (zi != null) {
                        newIndex = zi.kindZoneNb(newKind);
                        if (newIndex >= 0) {
                            newInfo.set(newIndex, true);
                        }
                    }
                }
        }
        return newInfo;
    }

    /**
     * @return true when one of the possible destinations of the given
     * pointer "expression" may have been reallocated to a different memory address
     * between the forward and backward sweep of this expression's location.
     */
    public static boolean mayPointToRelocated(Tree expression, boolean onDiff, Instruction instruction, SymbolTable symbolTable, boolean inJointDiffCode) {
        Tree destinationTree = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(expression));
        TapIntList destinationZones = ZoneInfo.listAllZones(symbolTable.treeOfZonesOfValue(destinationTree, null, instruction, null), false);
        boolean mayPointToRelocated = false;
        int rk;
        ZoneInfo destinationZoneInfo;
        BoolVector constantZones = inJointDiffCode && instruction.block != null ? instruction.block.constantZones : null;
        while (!mayPointToRelocated && destinationZones != null) {
            rk = destinationZones.head;
            destinationZoneInfo = symbolTable.declaredZoneInfo(rk, SymbolTableConstants.ALLKIND);
            // This zone (with extendedDeclared rank rk) is relocated if it has a zoneInfo AND
            // (EITHER we are in a procedure differentiated in split mode
            //  OR the zone is modified till the subroutine exit) AND
            // (EITHER we don't want to be clever about detection of relocated memory chunks
            //  OR we found out that this zone may have been relocated)
            if (destinationZoneInfo != null && rk>=0 && (constantZones == null || !constantZones.get(rk))) {
                mayPointToRelocated = !TapEnv.get().refineADMM || (onDiff ? destinationZoneInfo.relocatedDiff : destinationZoneInfo.relocated);
            }
            destinationZones = destinationZones.tail;
        }
        return mayPointToRelocated;
    }

    /**
     * Runs the present analysis, possibly limited to the
     * part of the CallGraph which is under the given "rootUnits",
     * or on the whole CallGraph if "rootUnits" is null.
     * One may overload with one's own implementation.
     * <br>Default implementation: {@link #runBottomUpAnalysis(TapList)}
     * <br>Overloading implementation may be or may contain
     * {@link #runTopDownAnalysis(TapList)}
     * or {@link #runBottomUpAnalysis(TapList)},
     * in which case one must check their overloadable utilities.
     * <br><b>Globals provided:</b> {@link #curCallGraph}.
     *
     * @param rootUnits The list of all Units recursively under
     *                  the differentiation root, ordered top-down.
     */
    protected void run(TapList<Unit> rootUnits) {
        runBottomUpAnalysis(rootUnits);
    }

    protected final void runStatically() {
        TapList<Unit> units = curCallGraph.sortedComputationalUnits();
        while (units != null) {
            setCurUnitEtc(units.head);
            if (curUnit.hasSource()) {
                analyze();
            }
            units = units.tail;
        }
        setCurUnitEtc(null);
    }

    /**
     * Runs the present analysis, top-down on the current CallGraph,
     * starting at and including the root Units in "rootUnits",
     * or at the top Units of the  CallGraph if "rootUnits" is null.
     * <br>If this method is used, then one must consider overloading the methods: <ul>
     * <li> {@link #initializeCGForUnit()}           (<i>default nothing returns null</i>)</li>
     * <li> {@link #initializeCGForRootUnit()}       (<i>default nothing</i>)</li>
     * <li> {@link #analyze()}                       (<i>default {@link #analyzeForward(TapList, TapList, TapList)}</i>)</li>
     * <li> {@link #terminateCGForUnit()}             (<i>default nothing</i>)</li>
     * </ul>
     *
     * @param rootUnits The list of all Units recursively under
     *                  the differentiation root, ordered top-down.
     */
    protected final void runTopDownAnalysis(TapList<Unit> rootUnits) {
        Chrono totalAnalysisTimer = new Chrono();
        TapList<Unit> allCallees = null;
        if (rootUnits!=null) {
            allCallees = Unit.allCalleesMulti(rootUnits) ;
        } else {
            allCallees = curCallGraph.sortedComputationalUnits();
            rootUnits = allCallees ;
        }
        TapList<Unit> units = allCallees;
        topDownContexts = new UnitStorage(curCallGraph);
        unitAnalysisTimer.reset();
        while (units != null) {
            setCurUnitEtc(units.head);
            curUnit.isCGCycle = isCGCycleTail(curUnit);
            setTopDownContext(curUnit, initializeCGForUnit());
            curUnit.analysisIsOutOfDateDown = true;
            units = units.tail;
        }
        while (rootUnits != null) {
            setCurUnitEtc(rootUnits.head);
            if (curUnit.hasSource()) {
                initializeCGForRootUnit();
            }
            rootUnits = rootUnits.tail;
        }
        TapEnv.printlnOnTrace(15, "@@ " + curAnalysisName + " initialization (" + unitAnalysisTimer.elapsed() + " s)");
        // runAgainCG is true when we must do a new sweep on the Call Graph.
        // This happens when the program is recursive and the analysis result for some unit
        // has changed and influences a unit which is above in the Call Graph.
        boolean runAgainCG = true;
        while (runAgainCG) {
            runAgainCG = false;
            units = allCallees;
            while (!runAgainCG && units != null) {
                setCurUnitEtc(units.head);
                if (curUnit.analysisIsOutOfDateDown) {
                    curUnit.analysisIsOutOfDateDown = false;
                    if (curUnit.hasSource()) {
                        TapEnv.printOnTrace(15, "@@ " + curAnalysisName + " for unit " + curUnit.name() + " ");
                        sweeps = 0;
                        unitAnalysisTimer.reset();
                    }
                    // Run the present analysis on the current Unit:
                    analyze();
                    // New detection of call-graph cycles: sometimes, units already seen
                    // may become out of date even if there is no recursivity, cf DiffLiveness.
                    runAgainCG = calleeOutOfDateBefore(allCallees, curUnit);
                    if (curUnit.hasSource()) {
                        TapEnv.printlnOnTrace(15, "  " + sweeps + " sweeps, " + unitAnalysisTimer.elapsed() + " s");
                    }
                }
                units = units.tail;
            }
        }
        units = allCallees;
        while (units != null) {
            setCurUnitEtc(units.head);
            if (curUnit.hasSource()) {
                terminateCGForUnit();
            }
            units = units.tail;
        }
        topDownContexts = null ;
        TapEnv.printlnOnTrace(10, "@@ " + curAnalysisName + " total time " + totalAnalysisTimer.elapsed() + " s");
        setCurUnitEtc(null);
    }

    /**
     * Runs the present analysis, bottom-up on the current CallGraph,
     * up to and including the root Units in "rootUnits",
     * or up to the top of the CallGraph if "rootUnits" is null.
     * <br>If this method is used, then one must consider overloading the methods: <ul>
     * <li> {@link #initializeCGForUnit()}   (<i>default nothing returns null</i>)</li>
     * <li> {@link #analyze()}               (<i>default {@link #analyzeForward(TapList, TapList, TapList)}</i>)</li>
     * <li> {@link #terminateCGForUnit()}     (<i>default nothing</i>)</li>
     * </ul>
     *
     * @param rootUnits The list of all Units recursively under
     *                  the differentiation root, ordered top-down.
     */
    protected final void runBottomUpAnalysis(TapList<Unit> rootUnits) {
        Chrono totalAnalysisTimer = new Chrono();
        TapList allCalleesReverse =
                TapList.reverse(
                        rootUnits == null ? curCallGraph.sortedComputationalUnits() : Unit.allCalleesMulti(rootUnits));
        TapList units = allCalleesReverse;
        unitAnalysisTimer.reset();
        while (units != null) {
            setCurUnitEtc((Unit) units.head);
            curUnit.isCGCycle = isCGCycleHead(curUnit);
            initializeCGForUnit();
            curUnit.analysisIsOutOfDateUp = true;
            units = units.tail;
        }
        TapEnv.printlnOnTrace(15, "@@ " + curAnalysisName + " initialization (" + unitAnalysisTimer.elapsed() + " s)");
        // runAgainCG is true when we must do a new sweep on the Call Graph.
        // This happens when the program is recursive and the analysis result for some unit
        // has changed and influences a unit which is below in the Call Graph.
        boolean runAgainCG = true;
        while (runAgainCG) {
            runAgainCG = false;
            units = allCalleesReverse;
            while (!runAgainCG && units != null) {
                setCurUnitEtc((Unit) units.head);
                if (curUnit.analysisIsOutOfDateUp && curUnit.hasSource()) {
                    TapEnv.printOnTrace(15, "@@ " + curAnalysisName + " for unit " + curUnit.name() + " ");
                    curUnit.analysisIsOutOfDateUp = false;
                    sweeps = 0;
                    unitAnalysisTimer.reset();
                    // Run the present analysis on the current Unit.
                    // If the result of the analysis has changed since the previous time
                    // (or if this is the first time), "analyze()" must return true:
                    if (analyze()) {
                        markCallersOutOfDate(curUnit);
                        runAgainCG = curUnit.isCGCycle;
                        if (!runAgainCG) {
                            runAgainCG = mpiRecvOutOfDate();
                        }
                    }
                    TapEnv.printlnOnTrace(15, " : " + sweeps + " sweeps, " + unitAnalysisTimer.elapsed() + " s");
                }
                units = units.tail;
            }
        }
        units = allCalleesReverse;
        while (units != null) {
            setCurUnitEtc((Unit) units.head);
            if (curUnit.hasSource()) {
                terminateCGForUnit();
            }
            units = units.tail;
        }
        TapEnv.printlnOnTrace(10, "@@ " + curAnalysisName + " total time " + totalAnalysisTimer.elapsed() + " s");
        setCurUnitEtc(null);
    }

    /**
     * Runs the present analysis, mixing bottom-up and top-down. After each time a Unit is analyzed,
     * it may decide that the callers and/or the callees of this Unit must be (re-)analyzed.
     * Priority is given to re-analysis of callers, i.e. to the bottom-up sweep.
     */
    protected final void runBottomUpTopDownAnalysis(TapList<Unit> rootUnits) {
        Chrono totalAnalysisTimer = new Chrono();
        TapList<Unit> units;
        TapList<Unit> allCalleesDown = (rootUnits == null ? curCallGraph.sortedComputationalUnits() : Unit.allCalleesMulti(rootUnits));
        TapList<Unit> allCalleesUp = TapList.reverse(allCalleesDown);
        unitAnalysisTimer.reset();
        topDownContexts = new UnitStorage(curCallGraph);
        for (units = allCalleesDown; units != null; units = units.tail) {
            setCurUnitEtc(units.head);
            setTopDownContext(curUnit, initializeCGForUnit());
            curUnit.analysisIsOutOfDateUp = true;
            curUnit.analysisIsOutOfDateDown = false;
        }
        while (rootUnits != null) {
            setCurUnitEtc(rootUnits.head);
            initializeCGForRootUnit();
            rootUnits = rootUnits.tail;
            curUnit.analysisIsOutOfDateDown = true;
        }
        TapEnv.printlnOnTrace(15, "@@ " + curAnalysisName + " initialization (" + unitAnalysisTimer.elapsed() + " s)");
        // runAgainCG is true when we need to restart analysis on the complete set of Units
        // It is initialized to true, and reset to true when a recursive call requires a re-analysis.

boolean runAgainCG2 = true;
while (runAgainCG2) {
    runAgainCG2 = false ;

        boolean runAgainCG = true;
        while (runAgainCG) {
            runAgainCG = false;
            for (units = allCalleesUp; !runAgainCG && units != null; units = units.tail) {
                if (units.head.analysisIsOutOfDateUp && units.head.hasSource()) {
                    setCurUnitEtc(units.head);
                    TapEnv.printOnTrace(15, "@@ Bottom-up " + curAnalysisName + " for unit " + curUnit.name() + " ");
                    curUnit.analysisIsOutOfDateUp = false;
                    sweeps = 0;
                    unitAnalysisTimer.reset();
                    // Run the present analysis on the current Unit.
                    // analyzeInBottomUp() returns true if the up-result (i.e. the result used by callers)
                    // has changed since the previous time (or if this is the first time).
                    if (analyzeInBottomUp()) {
                        markCallersOutOfDate(curUnit);
                        runAgainCG = curUnit.isCGCycle;
                        if (!runAgainCG) {
                            runAgainCG = mpiRecvOutOfDate();
                        }
                    }
                    curUnit.analysisIsOutOfDateDown = true;
                    TapEnv.printlnOnTrace(15, "  " + sweeps + " sweeps, " + unitAnalysisTimer.elapsed() + " s");
                }
            }
        }
        runAgainCG = true;
        while (runAgainCG) {
            runAgainCG = false;
            for (units = allCalleesDown; !runAgainCG && units != null; units = units.tail) {
                if (units.head.analysisIsOutOfDateDown && units.head.hasSource()) {
                    setCurUnitEtc(units.head);
                    TapEnv.printOnTrace(15, "@@ Top-down " + curAnalysisName + " for unit " + curUnit.name() + " ");
                    curUnit.analysisIsOutOfDateDown = false;
                    sweeps = 0;
                    unitAnalysisTimer.reset();
                    // Run the present analysis on the current Unit.
                    // If this changes the up-result, make sure to restart analysis from the bottom-up phase
                    if (analyzeInTopDown()) {
                        markCallersOutOfDate(curUnit);
                        runAgainCG2 = true;
                    } else {
                        // New detection of call-graph cycles: sometimes, units already seen
                        // may become out of date even if there is no recursivity, cf DiffLiveness.
                        runAgainCG = calleeOutOfDateBefore(allCalleesDown, curUnit);
                    }
                    TapEnv.printlnOnTrace(15, "  " + sweeps + " sweeps, " + unitAnalysisTimer.elapsed() + " s");
                }
            }
        }
}
        for (units = allCalleesDown; units != null; units = units.tail) {
            if (units.head.hasSource()) {
                setCurUnitEtc(units.head);
                terminateCGForUnit();
            }
        }
        topDownContexts = null ;
        TapEnv.printlnOnTrace(10, "@@ " + curAnalysisName + " total time " + totalAnalysisTimer.elapsed() + " s");
        setCurUnitEtc(null);
    }

    protected boolean analyzeInBottomUp() {
        return false;
    }

    protected boolean analyzeInTopDown() {
        return false;
    }

    /**
     * Runs the present analysis on the curUnit.
     * One may overload with one's own implementation.
     * <br>Default implementation: {@link #analyzeForward(TapList, TapList, TapList)}
     * <br>Overloading implementation may be or may contain
     * {@link #analyzeForward(TapList, TapList, TapList)}
     * or {@link #analyzeBackward(TapList, TapList, TapList)},
     * in which case one must check their overloadable utilities.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}.
     *
     * @return For a bottom-up analysis, MUST return true IFF the
     * analysis result has changed since the previous time
     * (or if this is the first time).
     * For a top-down analysis, this result may be anything.
     */
    protected boolean analyze() {
        return analyzeForward(null, null, null);
    }

    /**
     * Runs the present data-flow independent analysis on the current Unit "curUnit".
     * More precisely, runs only on the contents of the blocks "insideBlocks".
     * This means that the analysis can be run as a single sweep through each Instruction
     * of "curUnit", without any notion of data-flow order. For instance this can be used
     * to propagate some data to each Unit recursively called by curUnit without modifying
     * the data because of interpretation of curUnit.
     *
     * @param initBlocks all the Flow-Graph blocks of curUnit for which a special
     *        initialization action must/will be made through a call to initializeFGForInitBlock()
     * @param insideBlocks all the Flow-Graph blocks of curUnit that must be swept.
     *        If given null, the analysis will sweep through all Blocks of curUnit.
     * @return true iff the analysis result on curUnit is modified since last analysis call.
     *        This return value is used only in the context of a bottom-up analysis (runBottomUpAnalysis())
     *        Otherwise this return value is ignored.
     */
    public final boolean analyzeStatically(TapList<Block> initBlocks,
                                           TapList<Block> insideBlocks) {
        boolean hasChanged = false;
        if (curUnit.hasSource()) {
            if (insideBlocks == null) {
                insideBlocks = curUnit.allBlocks;
            }
            initializeFGForUnit();
            TapList<Block> blockS = initBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                initializeFGForInitBlock();
                blockS = blockS.tail;
            }
            blockS = insideBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                blockS = blockS.tail;
                propagateValuesStaticallyThroughBlock();
            }
            ++sweeps;
            hasChanged = terminateFGForUnit();
            setCurBlockEtc(null);
        }
        return hasChanged;
    }

    /**
     * Runs the present analysis, forward on the current Unit "curUnit", or more precisely
     * on the fraction of "curUnit" that is between "entryArrows" and "exitArrows", which
     * consists of the blocks "insideBlocks". The three "entryArrows", "insideBlocks" and
     * "exitArrows" may be passed null, in which case the analysis is run on the full "curUnit".
     * As a result, this overwrites the results of this analysis on all Blocks in "insideBlock",
     * plus the info overwritten by initialization of the analysis on the origins of the "entryArrows"
     * and the info propagated as a result on the destinations of the "exitArrows".
     * <br><b>Globals required:</b> {@link #curUnit}.
     * <br>If this method is used, then one must consider overloading the methods: <ul>
     * <li> {@link #initializeFGForUnit()}                     (<i>default nothing</i>)</li>
     * <li> {@link #initializeFGForInitBlock()}                (<i>default nothing</i>)</li>
     * <li> {@link #setCurBlockEtc(Block)}                (<i>default provided</i>)</li>
     * <li> {@link #initializeFGForBlock()}               (<i>default nothing</i>)</li>
     * <li> {@link #accumulateValuesFromUpstream()}       (<i>default provided</i>)</li>
     * <li> {@link #compareUpstreamValues()}              (<i>default nothing returns false</i>)</li>
     * <li> {@link #propagateValuesForwardThroughBlock()} (<i>default provided</i>)</li>
     * <li> {@link #compareDownstreamValues()}            (<i>default nothing returns false</i>)</li>
     * <li> {@link #terminateFGForBlock()}                (<i>default nothing</i>)</li>
     * <li> {@link #terminateFGForTermBlock()}                 (<i>default nothing</i>)</li>
     * <li> {@link #terminateFGForUnit()}                      (<i>default nothing returns true</i>)</li>
     * </ul>
     *
     * @param entryArrows  the Flow-Graph arrows from which the analysis will start.
     * @param insideBlocks all the Flow-Graph blocks that are on the way from entryArrows to exitArrows.
     * @param exitArrows   the Flow-Graph arrows at which the analysis will stop.
     * @return true if the analysis result is modified since last analysis.
     */
    public final boolean analyzeForward(TapList<FGArrow> entryArrows,
                                        TapList<Block> insideBlocks, TapList<FGArrow> exitArrows) {
        //[llh] TODO: remove arg "insideBlocks" (can be computed locally from "entryArrows" and "exitArrows") ?
        if (curUnit.hasSource()) {
            Chrono unitInitializationTimer = new Chrono();
            boolean analyzeFullUnit = (entryArrows == null && insideBlocks == null && exitArrows == null);
            TapList<Block> initBlocks;
            TapList<Block> termBlocks;
            if (entryArrows == null) {
                initBlocks = new TapList<>(curUnit.entryBlock(), null);
            } else {
                initBlocks = mapOrigin(entryArrows);
            }
            if (insideBlocks == null) {
                insideBlocks = curUnit.allBlocks;
            }
            if (exitArrows == null) {
                termBlocks = new TapList<>(curUnit.exitBlock(), null);
            } else {
                termBlocks = mapDestination(exitArrows);
            }
            initializeFGForUnit();
            TapList<Block> blockS = initBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                initializeFGForInitBlock();
                blockS = blockS.tail;
            }
            blockS = insideBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                initializeFGForBlock();
                curBlock.analysisIsOutOfDate = true;
                blockS = blockS.tail;
            }
            TapEnv.printOnTrace(20, " (init " + unitInitializationTimer.elapsed() + " s) ");
            // True when we must do a new sweep on the Flow Graph.
            // This happens when the Unit has cycles and the result of a needed
            // Block was not computed yet or changed since previous sweep. */
            boolean runAgainFG = true;
            // Propagate the dependencies across the curUnit,
            // repeatedly until fixed point is reached
            // (i.e. until runAgainFG remains false) */
            while (runAgainFG) {
                TapEnv.printOnTrace(20, "|");
                runAgainFG = false;
                ++sweeps;
                blockS = insideBlocks;
                while (!runAgainFG && blockS != null) {
                    setCurBlockEtc(blockS.head);
                    blockS = blockS.tail;
                    if (curBlock.analysisIsOutOfDate) {
                        curBlock.analysisIsOutOfDate = false;
                        if (accumulateValuesFromUpstream()) {
                            if (compareUpstreamValues()) {
                                TapEnv.printOnTrace(20, "o");
                                if (propagateValuesForwardThroughBlock()) {
                                    if (compareDownstreamValues()) {
                                        markFlowOutOfDate(curBlock.flow(), exitArrows);
                                        if (curBlock.isFGCycleTail) {
                                            runAgainFG = true;
                                        }
                                    }
                                    if (compareDownstreamChannelValuesWithInitial(initBlocks)) {
                                        markFlowOutOfDate((entryArrows==null ? curUnit.entryBlock().flow() : entryArrows),
                                                          exitArrows);
                                        runAgainFG = true;
                                    }
                                }
                            } else {
                                TapEnv.printOnTrace(20, ".");
                            }
                        }
                    }
                }
            }
            TapEnv.printOnTrace(20, "| ");
            blockS = insideBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                terminateFGForBlock();
                blockS = blockS.tail;
            }
            blockS = termBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                // To avoid bugs due to exit arrows cycling back into the inside:
                if (!TapList.contains(insideBlocks, curBlock)) {
                    if (accumulateValuesFromUpstream()) {
                        compareUpstreamValues();
                    }
                    terminateFGForTermBlock();
                }
                blockS = blockS.tail;
            }
            setCurBlockEtc(null);
            if (analyzeFullUnit) {
                setCurBlockEtc(curUnit.exitBlock());
                boolean hasChanged = terminateFGForUnit();
                setCurBlockEtc(null);
                return hasChanged;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Runs the present analysis, backward on the current Unit "curUnit", or more precisely
     * on the fraction of "curUnit" that is between "entryArrows" and "exitArrows", which
     * consists of the blocks "insideBlocks". The three "entryArrows", "insideBlocks" and
     * "exitArrows" may be passed null, in which case the anlaysis is run on the full "curUnit".
     * As a result, this overwrites the results of this analysis on all Blocks in "insideBlock",
     * plus the info overwritten by initialization of the analysis on the destinations of the
     * "exitArrows" and the info propagated as a result on the origins of the "entryArrows".
     * <br><b>Globals required:</b> {@link #curUnit}.
     * <br>If this method is used, then one must consider overloading the methods: <ul>
     * <li> {@link #initializeFGForUnit()}                     (<i>default nothing</i>)</li>
     * <li> {@link #initializeFGForInitBlock()}                (<i>default nothing</i>)</li>
     * <li> {@link #setCurBlockEtc(Block)}                (<i>default provided</i>)</li>
     * <li> {@link #initializeFGForBlock()}               (<i>default nothing</i>)</li>
     * <li> {@link #accumulateValuesFromDownstream()}     (<i>default provided</i>)</li>
     * <li> {@link #compareDownstreamValues()}            (<i>default nothing returns false</i>)</li>
     * <li> {@link #propagateValuesBackwardThroughBlock()}(<i>default provided</i>)</li>
     * <li> {@link #compareUpstreamValues()}              (<i>default nothing returns false</i>)</li>
     * <li> {@link #terminateFGForBlock()}                (<i>default nothing</i>)</li>
     * <li> {@link #terminateFGForTermBlock()}                 (<i>default nothing</i>)</li>
     * <li> {@link #terminateFGForUnit()}                      (<i>default nothing returns true</i>)</li>
     * </ul>
     *
     * @param entryArrows  the Flow-Graph arrows at which the (backward) analysis will stop.
     * @param insideBlocks all the Flow-Graph blocks that are on the way from entryArrows to exitArrows.
     * @param exitArrows   the Flow-Graph arrows from which the (backward) analysis will start.
     * @return true iff the analysis result is modified since last analysis.
     */
    public final boolean analyzeBackward(TapList<FGArrow> entryArrows,
                                         TapList<Block> insideBlocks, TapList<FGArrow> exitArrows) {
        //[llh] TODO: remove arg "insideBlocks" (can be computed locally from "entryArrows" and "exitArrows") ?
        if (curUnit.hasSource()) {
            Chrono unitInitializationTimer = new Chrono();
            boolean analyzeFullUnit = entryArrows == null && insideBlocks == null && exitArrows == null;
            TapList<Block> initBlocks;
            TapList<Block> termBlocks;
            if (exitArrows == null) {
                initBlocks = new TapList<>(curUnit.exitBlock(), null);
            } else {
                initBlocks = mapDestination(exitArrows);
            }
            if (insideBlocks == null) {
                insideBlocks = curUnit.allBlocks;
            }
            if (entryArrows == null) {
                termBlocks = new TapList<>(curUnit.entryBlock(), null);
            } else {
                termBlocks = mapOrigin(entryArrows);
            }
            insideBlocks = TapList.reverse(insideBlocks);
            initializeFGForUnit();
            TapList<Block> blockS = insideBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                initializeFGForBlock();
                curBlock.analysisIsOutOfDate = true;
                blockS = blockS.tail;
            }
            blockS = initBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                initializeFGForInitBlock();
                blockS = blockS.tail;
            }
            TapEnv.printOnTrace(20, " (init " + unitInitializationTimer.elapsed() + " s) ");
            // True when we must do a new sweep on the Flow Graph.
            // This happens when the Unit has cycles and the result of a needed
            // Block was not computed yet or has changed since previous sweep. */
            boolean runAgainFG = true;
            //Propagate the dependencies across the curUnit,
            // repeatedly until fixed point is reached
            // (i.e. until runAgainFG remains false) */
            while (runAgainFG) {
                TapEnv.printOnTrace(20, "|");
                runAgainFG = false;
                ++sweeps;
                blockS = insideBlocks;
                while (!runAgainFG && blockS != null) {
                    setCurBlockEtc(blockS.head);
                    blockS = blockS.tail;
                    if (curBlock.analysisIsOutOfDate) {
                        curBlock.analysisIsOutOfDate = false;
                        if (accumulateValuesFromDownstream()) {
                            if (compareDownstreamValues()) {
                                TapEnv.printOnTrace(20, "o");
                                if (propagateValuesBackwardThroughBlock()) {
                                    if (compareUpstreamValues()) {
                                        markBackFlowOutOfDate(curBlock.backFlow(), entryArrows);
                                        if (curBlock.isFGCycleHead) {
                                            runAgainFG = true;
                                        }
                                    }
                                    if (compareUpstreamChannelValuesWithInitial(initBlocks)) {
                                        markBackFlowOutOfDate(exitArrows == null ? curUnit.exitBlock().backFlow() : exitArrows, entryArrows);
                                        runAgainFG = true;
                                    }
                                }
                            } else {
                                TapEnv.printOnTrace(20, ".");
                            }
                        }
                    }
                }
            }
            TapEnv.printOnTrace(20, "| ");
            blockS = insideBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                terminateFGForBlock();
                blockS = blockS.tail;
            }
            blockS = termBlocks;
            while (blockS != null) {
                setCurBlockEtc(blockS.head);
                // To avoid bugs due to entry arrows coming from cycles from the inside:
                if (!TapList.contains(insideBlocks, curBlock)) {
                    if (accumulateValuesFromDownstream()) {
                        compareDownstreamValues();
                    }
                    terminateFGForTermBlock();
                }
                blockS = blockS.tail;
            }
            setCurBlockEtc(null);
            if (analyzeFullUnit) {
                setCurBlockEtc(curUnit.entryBlock());
                boolean hasChanged = terminateFGForUnit();
                setCurBlockEtc(null);
                return hasChanged;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Set {@link #curUnit}.
     * Also set the relatedUnit in tapEnv (but [llh 8/8/14 TODO] doesn't reset it to null!! ).
     * One may overload with one's own choice, but it must call super.setCurUnitEtc(unit).
     * <br><b>Globals provided:</b> {@link #curCallGraph}.
     *
     * @param unit The new current Unit.
     */
    public void setCurUnitEtc(Unit unit) {
        curUnit = unit;
        //Should reset related Unit in all cases, but relatedUnit is not always handled as a stack :-(
        if (curUnit != null) {
            TapEnv.setRelatedUnit(curUnit);
        }
        TapEnv.setTraceCurAnalysis(tracedUnits != null
                                   && (tracedUnits.head == null
                                       || (curUnit != null && TapList.contains(tracedUnits, curUnit))));
        if (curUnit == null) {
            setCurBlockEtc(null);
        }
    }

    /**
     * Initializations before the sweeps on the Call Graph.
     * This must make all initializations related to "unit".
     * For a top-down analysis, this MUST return an Object,
     * which will be used as the initial value of the top-down context.
     * For a bottom-up analysis, this method must return something
     * but nobody cares for its value, so please return null!
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}.
     */
    protected Object initializeCGForUnit() {
        TapEnv.printlnOnTrace(15, curAnalysisName + " calling default empty initializeCGForUnit()");
        return null;
    }

    /**
     * If necessary, implement here whatever must be done
     * for initialization of the analysis' root Unit "rootUnit"
     * after initialization of all units but before the analysis starts.
     * This is needed only for a top-down analysis.
     * This default does nothing.
     * Overload with one's own choice.
     */
    protected void initializeCGForRootUnit() {
        TapEnv.printlnOnTrace(15, curAnalysisName + " calling default empty initializeCGForRootUnit()");
    }

    /**
     * Initializations before the sweeps on the Flow Graph of "unit".
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}.
     */
    protected void initializeFGForUnit() {
        TapEnv.printlnOnTrace(15, curAnalysisName + " calling default empty initializeFGForUnit()");
    }

    /**
     * Initializations on the "curBlock", which is
     * an initial entry block, before the sweeps on curUnit.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void initializeFGForInitBlock() {
        TapEnv.printlnOnTrace(15, curAnalysisName + " calling default empty initializeFGForInitBlock()");
    }

    /**
     * Set {@link #curBlock}, {@link #curSymbolTable}, {@link #nDZ}.
     * One may overload with one's own choice, but it must call super.setCurBlockEtc(block).
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}.
     *
     * @param block The new current Block.
     */
    protected void setCurBlockEtc(Block block) {
        // When block==null, we consider the curBlock is the curUnit's entryBlock
        // in particular to see its publicSymbolTable :
        if (block == null && curUnit != null && curUnit.publicSymbolTable() != null) {
            curBlock = curUnit.entryBlock();
        } else {
            curBlock = block;
        }
        if (curBlock == null) {
            curSymbolTable = (curUnit==null ? null : curUnit.publicSymbolTable()) ;
        } else {
            curSymbolTable = curBlock.symbolTable;
        }
        nDZ = (curSymbolTable==null ? 0 : curSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) ;
    }

    /**
     * If necessary, initializations for "curBlock" done just before
     * the analysis of the "curUnit" (which contains "curBlock").
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void initializeFGForBlock() {
    }

    /**
     * Collects the data-flow information flow-wise from the incoming flow arrows of
     * "curBlock".
     * Special behavior for loop headers,
     * distinguishing loop "entry", "cycle", and "exit".
     * One may overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     * <br>If on the other hand, one decides not to overload and use the
     * provided default, then one must consider overloading the methods: <ul>
     * <li> {@link #setEmptyCumulAndCycleValues()}        (<i>default nothing</i>)</li>
     * <li> {@link #getValueFlowingThrough()}             (<i>default nothing returns false</i>)</li>
     * <li> {@link #cumulValueWithAdditional(SymbolTable)}(<i>default nothing</i>)</li>
     * <li> {@link #cumulCycleValueWithAdditional(SymbolTable)}(<i>default nothing</i>)</li>
     * </ul>
     *
     * @return true when the collecting returned a "non-empty"
     * info, meaning that the control flow really reaches this point.
     */
    protected boolean accumulateValuesFromUpstream() {
        //[llh] TODO: improve behavior when one of the arrows comes across the frontier
        TapList<FGArrow> arrows = curBlock.backFlow();
        boolean runSpecialCycleAnalysis = runSpecialCycleAnalysis(curBlock);
        boolean somethingFlows = false;
        setEmptyCumulAndCycleValues();
        while (arrows != null) {
            curArrow = arrows.head;
            if (getValueFlowingThrough()) {
                somethingFlows = true;
                if (runSpecialCycleAnalysis && curArrow.inACycle) {
                    cumulCycleValueWithAdditional(curSymbolTable.getCommonRoot(curArrow.origin.symbolTable));
                } else {
                    cumulValueWithAdditional(curSymbolTable.getCommonRoot(curArrow.origin.symbolTable));
                }
            }
            arrows = arrows.tail;
        }
        return somethingFlows;
    }

    /**
     * Collects the data-flow information back from the downstream flow arrows
     * of "curBlock". @return true when the collecting returned a "non-empty"
     * info, meaning that the backwards control flow really reaches this point.
     * Special behavior for loop headers,
     * distinguishing loop "entry", "cycle", and "exit".
     * One may overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     * <br>If on the other hand, one decides not to overload and use the
     * provided default, then one must consider overloading the methods: <ul>
     * <li> {@link #setEmptyCumulAndCycleValues()}        (<i>default nothing</i>)</li>
     * <li> {@link #getValueFlowingBack()}                (<i>default nothing returns false</i>)</li>
     * <li> {@link #cumulValueWithAdditional(SymbolTable)}(<i>default nothing</i>)</li>
     * <li> {@link #cumulCycleValueWithAdditional(SymbolTable)}(<i>default nothing</i>)</li>
     * <li> {@link #initializeCumulValue()}               (<i>default nothing</i>)</li>
     * </ul>
     */
    protected boolean accumulateValuesFromDownstream() {
        //[llh] TODO: improve behavior when one of the arrows comes across the frontier !!!
        TapList<FGArrow> arrows = curBlock.flow();
        boolean runSpecialCycleAnalysis = runSpecialCycleAnalysis(curBlock);
        boolean somethingFlows = false;
        setEmptyCumulAndCycleValues();
        if (arrows != null) {
            while (arrows != null) {
                curArrow = arrows.head;
                if (getValueFlowingBack()) {
                    somethingFlows = true;
                    if (runSpecialCycleAnalysis && curArrow.containsCase(FGConstants.NEXT)) {
                        cumulCycleValueWithAdditional(curSymbolTable.getCommonRoot(curArrow.destination.symbolTable));
                    } else {
                        cumulValueWithAdditional(curSymbolTable.getCommonRoot(curArrow.destination.symbolTable));
                    }
                }
                arrows = arrows.tail;
            }
        } else {
            //Special case, at the tail of Blocks that go nowhere (e.g. after a stop)
            // we must create an empty info to propagate it upwards (cf f77:lha48)
            initializeCumulValue();
            somethingFlows = true;
        }
        return somethingFlows;
    }

    /**
     * Reinitialize the user-defined accumulators in which cumulValueWithAdditional() and
     * cumulCycleValueWithAdditional() will accumulate the data-flow values before
     * analyzing a Block.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void setEmptyCumulAndCycleValues() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty setEmptyCumulAndCycleValues()");
    }

    /**
     * Retrieve the data-flow value that flows forwards through "curArrow".
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}, {@link #curArrow},
     * {@link #curBlock}, {@link #curSymbolTable}.
     *
     * @return Must return true iff the control flow may reach curArrow
     */
    protected boolean getValueFlowingThrough() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty getValueFlowingThrough()");
        return false;
    }

    /**
     * Retrieve the data-flow value that flows backwards through "curArrow".
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}, {@link #curArrow},
     * {@link #curBlock}, {@link #curSymbolTable}.
     *
     * @return Must return true iff the control flow may reach curArrow
     */
    protected boolean getValueFlowingBack() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty getValueFlowingBack()");
        return false;
    }

    /**
     * Accumulate the retrieved data-flow value
     * (through the FGArrow curArrow) into the user-defined accumulator.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curArrow}, {@link #curBlock}, {@link #curSymbolTable}.
     *
     * @param commonSymbolTable The SymbolTable which is common to the origin
     *                          and the destination of curArrow.
     */
    protected void cumulValueWithAdditional(SymbolTable commonSymbolTable) {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty cumulValueWithAdditional()");
    }

    /**
     * Accumulate the retrieved cycling data-flow value
     * (through the cycling FGArrow curArrow) into the user-defined
     * accumulator for cycling data-flow values.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curArrow}, {@link #curBlock}, {@link #curSymbolTable}.
     *
     * @param commonSymbolTable The SymbolTable which is common to the origin
     *                          and the destination of curArrow.
     */
    protected void cumulCycleValueWithAdditional(SymbolTable commonSymbolTable) {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty cumulCycleValueWithAdditional()");
    }

    /**
     * Create an empty info to propagate it upwards.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void initializeCumulValue() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty initializeCumulValue()");
    }

    /**
     * Compares the current data-flow information with the info
     * stored here (upstream current "curBlock") upon previous sweep.
     *
     * @return true when something has changed and therefore
     * another Flow Graph sweep is  probably necessary.
     * Takes (should take) care of storing the new info when modified.
     * This default does nothing and returns false.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected boolean compareUpstreamValues() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty compareUpstreamValues()");
        return false;
    }

    /**
     * Compares the current computed info about message-Passing channels,
     * upstream the current Block "block" with the corresponding info upstream
     * each Block in "initBlocks" (remember this is a backwards analysis!).
     * If the current info is "larger", accumulates it into the info downstream the "initBlocks"
     * and returns true, meaning that the fixpoint iteration must restart. Otherwise returns false.
     */
    protected boolean compareUpstreamChannelValuesWithInitial(TapList<Block> initBlocks) {
        boolean initHasIncreased = false;
        TapIntList allMessagePassingChannelZones = curCallGraph.getAllMessagePassingChannelZones();
        TapIntList inAllChannelZones;
        Block initBlock;
        while (initBlocks != null) {
            initBlock = initBlocks.head;
            inAllChannelZones = allMessagePassingChannelZones;
            while (inAllChannelZones != null) {
                if (compareChannelZoneDataUpstream(inAllChannelZones.head, initBlock)) {
                    initHasIncreased = true;
                }
                inAllChannelZones = inAllChannelZones.tail;
            }
            initBlocks = initBlocks.tail;
        }
        return initHasIncreased;
    }

    /**
     * Compares the info for the Message-Passing channel zone "mpZone", upstream curBlock
     * compared with upstream "refBlock".
     * If curBlock's is larger, accumulates it into refBlock's and returns true.
     * This default does nothing, complains, and returns false.
     * Overload with one's own choice.
     */
    protected boolean compareChannelZoneDataUpstream(int mpZone, Block refBlock) {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty compareChannelZoneDataUpstream()");
        return false;
    }

    /**
     * Compares the current computed info about message-Passing channels, downstream the current
     * Block "block" with the corresponding info downstream each Block in "initBlocks".
     * If the current info is "larger", accumulates it into the info downstream the "initBlocks"
     * and returns true, meaning that the fixpoint iteration must restart. Otherwise returns false.
     */
    private boolean compareDownstreamChannelValuesWithInitial(TapList<Block> initBlocks) {
        boolean initHasIncreased = false;
        TapIntList allMessagePassingChannelZones = curCallGraph.getAllMessagePassingChannelZones();
        TapIntList inAllChannelZones;
        Block initBlock;
        while (initBlocks != null) {
            initBlock = initBlocks.head;
            inAllChannelZones = allMessagePassingChannelZones;
            while (inAllChannelZones != null) {
                if (compareChannelZoneDataDownstream(inAllChannelZones.head, initBlock)) {
                    initHasIncreased = true;
                }
                inAllChannelZones = inAllChannelZones.tail;
            }
            initBlocks = initBlocks.tail;
        }
        return initHasIncreased;
    }

    /**
     * Compares the info for the Message-Passing channel zone "mpZone", downstream curBlock
     * compared with downstream "refBlock".
     * If curBlock's is larger, accumulates it into refBlock's and returns true.
     * This default does nothing, complains, and returns false.
     * Overload with one's own choice.
     */
    protected boolean compareChannelZoneDataDownstream(int mpZone, Block refBlock) {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty compareChannelZoneDataDownstream()");
        return false;
    }

    /**
     * Compares the current data-flow information with the info
     * stored here (downstream current "curBlock") upon previous sweep.
     *
     * @return true when something has changed and therefore
     * another Flow Graph sweep is  probably necessary.
     * Takes (should take) care of storing the new info when modified.
     * This default does nothing and returns false.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected boolean compareDownstreamValues() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty compareDownstreamValues()");
        return false;
    }

    /**
     * Utility that may be used in implementation of compare(Up/Down)streamValues()
     * Compares BoolVector "newInfo" with the info stored for block in
     * the BlockStorage&lt;BoolVector&gt; "storedInfos".
     * If there is a modification, replace the stored by the new.
     * Does the same for the corresponding cycling infos.
     * returns true iff either was replaced.
     */
    protected boolean compareWithStorage(BoolVector newInfo, BoolVector newInfoCycle,
                                         int length,
                                         BlockStorage<BoolVector> storedInfos,
                                         BlockStorage<BoolVector> storedInfosCycle) {
        boolean modified = false;
        BoolVector oldInfo = storedInfos.retrieve(curBlock);
        if (newInfo != null && (oldInfo == null || !newInfo.equals(oldInfo, length))) {
            modified = true;
            storedInfos.store(curBlock, newInfo.copy());
        }
        if (runSpecialCycleAnalysis(curBlock)/*specialCycle*/) {
            oldInfo = storedInfosCycle.retrieve(curBlock);
            if (newInfoCycle != null
                    && (oldInfo == null || !newInfoCycle.equals(oldInfo, length))) {
                modified = true;
                storedInfosCycle.store(curBlock, newInfoCycle.copy());
            }
        }
        return modified;
    }

    /**
     * Special case of compareWithStorage() for pairs of BoolVector's, the first one for "normal" info,
     * the second one for the info on differentiated pointers (OnDiffPtr). This primitive makes sense
     * e.g. for DiffLiveness and TBR analyses.
     *
     * @return true if the new info is different from the previous one.
     * In that case, replaces the old info with the new.
     */
    protected boolean compareWithStorage(BoolVector newInfo, BoolVector newInfoOnDiffPtr,
                                         BoolVector newInfoCycle, BoolVector newInfoCycleOnDiffPtr,
                                         int length, int lengthOnDiffPtr,
                                         BlockStorage<TapPair<BoolVector, BoolVector>> storedInfos,
                                         BlockStorage<TapPair<BoolVector, BoolVector>> storedInfosCycle) {
        boolean modified = false;
        TapPair<BoolVector, BoolVector> oldInfo = storedInfos.retrieve(curBlock);
        if (newInfo != null || newInfoOnDiffPtr != null) {
            if (oldInfo == null) {
                oldInfo = new TapPair<>(null, null);
                storedInfos.store(curBlock, oldInfo);
            }
            if (newInfo != null && (oldInfo.first == null
                    || !newInfo.equals(oldInfo.first, length))) {
                modified = true;
                oldInfo.first = newInfo.copy();
            }
            if (newInfoOnDiffPtr != null && (oldInfo.second == null
                    || !newInfoOnDiffPtr.equals(oldInfo.second, lengthOnDiffPtr))) {
                modified = true;
                oldInfo.second = newInfoOnDiffPtr.copy();
            }
        }
        if (runSpecialCycleAnalysis(curBlock)/*specialCycle*/) {
            oldInfo = storedInfosCycle.retrieve(curBlock);
            if (newInfoCycle != null || newInfoCycleOnDiffPtr != null) {
                if (oldInfo == null) {
                    oldInfo = new TapPair<>(null, null);
                    storedInfosCycle.store(curBlock, oldInfo);
                }
                if (newInfoCycle != null && (oldInfo.first == null
                        || !newInfoCycle.equals(oldInfo.first, length))) {
                    modified = true;
                    oldInfo.first = newInfoCycle.copy();
                }
                if (newInfoCycleOnDiffPtr != null && (oldInfo.second == null
                        || !newInfoCycleOnDiffPtr.equals(oldInfo.second, lengthOnDiffPtr))) {
                    modified = true;
                    oldInfo.second = newInfoCycleOnDiffPtr.copy();
                }
            }
        }
        return modified;
    }

    /**
     * Propagates the data-flow information statically through "curBlock".
     */
    protected void propagateValuesStaticallyThroughBlock() {
        TapList<Instruction> instructions = curBlock.instructions;
        while (instructions != null) {
            curInstruction = instructions.head;
            propagateValuesStaticallyThroughExpression();
            instructions = instructions.tail;
        }
        curInstruction = null;
    }

    /**
     * Propagates the data-flow information forwards through "curBlock".
     * This default does propagation through each instruction.
     * One may overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     * <br>If on the other hand, one decides not to overload and use the
     * provided default, then one must consider overloading the method: <ul>
     * <li> {@link #propagateValuesForwardThroughExpression()}  (<i>default nothing returns true</i>)</li>
     * </ul>
     *
     * @return false when the control flow cannot go through (e.g. exit()).
     */
    protected boolean propagateValuesForwardThroughBlock() {
        TapList<Instruction> instructions = curBlock.instructions;
        boolean arrives = true;
        while (arrives && instructions != null) {
            curInstruction = instructions.head;
            arrives = propagateValuesForwardThroughExpression();
            instructions = instructions.tail;
        }
        curInstruction = null;
        return arrives;
    }

    /**
     * Propagates the data-flow information statically to the tree inside the curInstruction.
     * Default does nothing. Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}, {@link #curInstruction}.
     */
    protected void propagateValuesStaticallyThroughExpression() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty propagateValuesStaticallyThroughExpression() on " + curInstruction);
    }

    /**
     * Propagates the data-flow information forwards through the tree inside the curInstruction.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}, {@link #curInstruction}.
     *
     * @return false when the control flow cannot go through (e.g. exit()).
     */
    protected boolean propagateValuesForwardThroughExpression() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty propagateValuesForwardThroughExpression()");
        return true;
    }

    /**
     * Propagates the data-flow information backwards through "curBlock".
     * This default does propagation through each instruction.
     * One may overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     * <br>If on the other hand, one decides not to overload and use the
     * provided default, then one must consider overloading the method: <ul>
     * <li> {@link #propagateValuesBackwardThroughExpression()} (<i>default nothing returns true</i>)</li>
     * </ul>
     *
     * @return false when the control flow cannot go through (e.g. exit()).
     */
    protected boolean propagateValuesBackwardThroughBlock() {
        TapList<Instruction> instructions = TapList.reverse(curBlock.instructions);
        boolean arrives = true;
        while (arrives && instructions != null) {
            curInstruction = instructions.head;
            arrives = propagateValuesBackwardThroughExpression();
            instructions = instructions.tail;
        }
        curInstruction = null;
        return arrives;
    }

    /**
     * Propagates the data-flow information backwards through expression "tree".
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}, {@link #curInstruction}.
     *
     * @return false when the control flow cannot go through (e.g. exit()).
     */
    protected boolean propagateValuesBackwardThroughExpression() {
        TapEnv.printlnOnTrace(20, curAnalysisName + " calling default empty propagateValuesBackwardThroughExpression()");
        return true;
    }

    /**
     * If necessary, termination operations on an
     * inside block "curBlock" after the sweeps on a unit.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void terminateFGForBlock() {
    }

    /**
     * If necessary, termination operations on a
     * termination block "curBlock" after the sweeps on a unit.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit},
     * {@link #curBlock}, {@link #curSymbolTable}.
     */
    protected void terminateFGForTermBlock() {
    }

    /**
     * Termination operations at the end of the Flow Graph analysis of
     * the given Unit "curUnit". In the case of a bottom-up
     * analysis, must build the analysis result on the current "curUnit",
     * which is deduced from the values stored at the entry of the exit
     * Block, depending on the direction.
     * For a bottom-up analysis, this method MUST return
     * true IFF the analysis result has changed since
     * the previous time (or if this is the first time).
     * For a top-down analysis, this method must return a boolean value
     * but nobody cares for it.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}
     */
    protected boolean terminateFGForUnit() {
        TapEnv.printlnOnTrace(15, curAnalysisName + " calling default empty terminateFGForUnit()");
        return true;
    }

    /**
     * If necessary, runs some concluding analysis on Unit "unit"
     * after the present analysis is completed on the Call Graph.
     * This default does nothing.
     * Overload with one's own choice.
     * <br><b>Globals provided:</b> {@link #curCallGraph}, {@link #curUnit}.
     */
    protected void terminateCGForUnit() {
    }

    /**
     * For future dataflow analysis.
     */
    private boolean calleeOutOfDateAbove(Unit unit) {
        boolean result = false;
        Unit callee;
        TapList<CallArrow> arrows = unit.callees();
        while (!result && arrows != null) {
            callee = arrows.head.destination;
            result =
                    callee.rank() >= 0 &&
                            callee.rank() <= unit.rank() &&
                            callee.analysisIsOutOfDateDown;
            arrows = arrows.tail;
        }
        if (!result && curCallGraph.MPIRecvCalls != null) {
            result = mpiRecvOutOfDate();
        }
        return result;
    }

    /**
     * @return true when some unit is marked "OutOfDate" for the current
     * top-down analysis, somewhere in between the beginning of the list
     * of all analyzed units "allCallees" and the current analyzed "unit".
     */
    private boolean calleeOutOfDateBefore(TapList<Unit> allCallees, Unit unit) {
        boolean outOfDate = false;
        while (!outOfDate && allCallees != null && allCallees.head != unit) {
            outOfDate = allCallees.head.analysisIsOutOfDateDown;
            allCallees = allCallees.tail;
        }
        if (!outOfDate) {
            outOfDate = unit.analysisIsOutOfDateDown;
        }
        if (!outOfDate && curCallGraph.MPIRecvCalls != null) {
            outOfDate = mpiRecvOutOfDate();
        }
        return outOfDate;
    }

    private boolean mpiRecvOutOfDate() {
        boolean outOfDate = false;
        TapList<Unit> recvUnits = curCallGraph.MPIRecvCalls;
        while (!outOfDate && recvUnits != null) {
            outOfDate = recvUnits.head.analysisIsOutOfDateDown;
            recvUnits = recvUnits.tail;
        }
        return outOfDate;
    }

    /** Returns the deps as seen by the caller side,
     * i.e. by-value arguments are seen as unchanged. */
    public static BoolMatrix filterByValue(BoolMatrix publicDeps, int diffKind, Unit unit) {
        if (publicDeps!=null) {
            // Zones passed by value must not be considered modified:
            TapIntList byValueRows = null ;
            ZoneInfo zoneInfo ;
            int dki ;
            for (int i=unit.paramElemsNb()-1 ; i>=0 ; --i) {
                zoneInfo = unit.paramElemZoneInfo(i) ;
                dki = zoneInfo.kindZoneNb(diffKind) ;
                if (dki!=-1 && zoneInfo.passesByValue(unit, unit.language())) {
                    byValueRows = new TapIntList(dki, byValueRows) ;
                }
            }
            // Set the corresponding rows to implicit-identity:
            if (byValueRows!=null) {
                publicDeps = publicDeps.copy() ;
                while (byValueRows!=null) {
                    publicDeps.setRow(byValueRows.head, null) ;
                    byValueRows = byValueRows.tail;
                }
            }
        }
        return publicDeps ;
    }

    /**
     * @param totalRef  true when the Tree of the reference is actually total, i.e. is not an array index
     * @param zonesTree the tree of zones of the reference.
     *                  <br><b>Globals required:</b> {@link #curInstruction}, {@link #uniqueAccessZones}
     * @return true when the reference that has the given "totalRef" and the given "zonesList"
     * may be considered total in the current data-flow analysis. The answer depends of
     * the "curInstruction"'s where mask and of the current "uniqueAccessZones".
     */
    public boolean referenceIsTotal(boolean totalRef, TapList zonesTree) {
        TapIntList zonesList = ZoneInfo.listAllZones(zonesTree, true);
        int maxDeclZone = curInstruction.block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
        return curInstruction.getWhereMask() == null
                && !oneZoneIsMultiple(zonesList)
                && ((totalRef && allPartsAreUniqueScalar(zonesTree, new TapList<>(null, null)))
                    // Trick: inside loops that access always the same cell of an array
                    // for a same iteration, all accesses to this array are considered
                    // total. This emulates a simple killed-array mechanism:
                    || (zonesList != null && zonesList.head > 0 && zonesList.head < maxDeclZone && zonesList.tail == null &&
                        uniqueAccessZones != null && uniqueAccessZones.get(zonesList.head)));
    }

    /**
     * Checks that all leaves of the tree of zones zonesTree contain a unique zone
     * which is a scalar. In C, this is necessary in order to make sure that
     * an access is total: {@code *p} is total only if the variable pointed by p is a scalar.
     * In Fortran, we turn this test off, so that an access in array notation can be total.
     */
    private boolean allPartsAreUniqueScalar(TapList zonesTree, TapList<TapList> dejaVu) {
        if (curUnit.isFortran()) {
            return true;
        }
        boolean allOK = true;
        while (allOK && zonesTree != null) {
            if (TapList.contains(dejaVu.tail, zonesTree)) {
                zonesTree = null;
            } else {
                dejaVu.placdl(zonesTree);
                if (zonesTree.head instanceof TapList) {
                    allOK = allPartsAreUniqueScalar((TapList) zonesTree.head, dejaVu);
                } else if (zonesTree.head != null) {
                    TapIntList zonesList = (TapIntList) zonesTree.head;
                    // It's not OK to have two alternative zones
                    // and it's OK if the unique zone listed is a scalar
                    allOK = zonesList.tail == null && zoneIsScalar(zonesList.head);
                }
                zonesTree = zonesTree.tail;
            }
        }
        return allOK;
    }

    /**
     * @return true if one of the zones in "zonesList" (TapIntList list of extended declared ranks)
     * is actually labelled as multiple &rarr; in that case, an access to these zones can't be total.
     */
    public boolean oneZoneIsMultiple(TapIntList zonesList) {
        boolean found = false;
        ZoneInfo zoneInfo;
        while (!found && zonesList != null) {
            zoneInfo = curSymbolTable.declaredZoneInfo(zonesList.head, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null && zoneInfo.multiple) {
                found = true;
            }
            zonesList = zonesList.tail;
        }
        return found;
    }

    /**
     * @return true when formal param number "i" (from 1 to nbParams) of procedure "unit"
     * is only read and never overwritten inside "unit".
     */
    private static boolean argIsOnlyRead(Unit unit, int i) {
        TapIntList argiZones = ZoneInfo.listAllZones(unit.argsPublicRankTrees[i], true) ;
        return unit.unitInOutW() != null
            && !unit.unitInOutPossiblyW().intersects(argiZones)
            && (unit.unitInOutRW() == null
                || !unit.unitInOutRW().intersects(argiZones));
    }

    /**
     * Accumulates into "written" the zones (possibly and/or partly) written by "expr"
     */
    public static void collectZonesWrittenByExpression(Tree expr, int act, BoolVector written, Instruction instr) {
        switch (expr.opCode()) {
        case ILLang.op_call:
            //TODO
            break ;
        case ILLang.op_assign:
        case ILLang.op_plusAssign:
        case ILLang.op_minusAssign:
        case ILLang.op_timesAssign:
        case ILLang.op_divAssign:
        case ILLang.op_pointerAssign: {
            collectZonesWrittenByExpression(expr.down(1), WRITE, written, instr) ;
            collectZonesWrittenByExpression(expr.down(2), READ, written, instr) ;
            break ;
        }
        case ILLang.op_arrayAccess:
        case ILLang.op_fieldAccess:
        case ILLang.op_pointerAccess:
        case ILLang.op_ident: {
            if (act == WRITE) {
                SymbolTable symbolTable = instr.block.symbolTable ;
                TapIntList valueZones =
                    symbolTable.listOfZonesOfValue(expr, null, instr);
                written.set(valueZones, true) ;
            }
            break ;
        }
        case ILLang.op_loop: {
            if (expr.down(3).opCode()==ILLang.op_do) {
                collectZonesWrittenByExpression(expr.down(3).down(1), WRITE, written, instr) ;
            }
            break ;
        }
        }
    }

    /**
     * For future dataflow analysis.
     */
    private boolean isPointerZones(int paramRank, Tree[] actualParamTreeS) {
        boolean result = false;
        TapList zonesTree =
                curSymbolTable.treeOfZonesOfValue(actualParamTreeS[paramRank], new ToBool(), curInstruction, null);
        if (zonesTree != null && zonesTree.head instanceof TapIntList) {
            int zoneRk = ((TapIntList) zonesTree.head).head;
            result = zoneIsPointer(zoneRk);
        }
        return result;
    }

    /**
     * @param zonesTree The tree of extended declared zones for which the info is sought.
     * @param matrix    The matrix that contains the info.
     * @param whichKind The kind that is used to number the row indices in "matrix".
     * @return The resulting info as a tree of BoolVector's
     */
    public final TapList buildInfoPRZVTree(TapList zonesTree,
                                           BoolMatrix matrix, int whichKind) {
        return buildInfoPRZVTreeRec(zonesTree, matrix, whichKind,
                                    new TapList<>(null, null));
    }

    private TapList buildInfoPRZVTreeRec(TapList zonesTree,
                                         BoolMatrix matrix, int whichKind,
                                         TapList<TapPair<TapList, TapList>> dejaVu) {
        TapList hdPRZVsTree = new TapList<>(null, null);
        TapList tlPRZVsTree = hdPRZVsTree;
        Object przvsSubTree;
        while (zonesTree != null) {
            TapList cycle = TapList.cassq(zonesTree, dejaVu.tail);
            if (cycle != null) {
                tlPRZVsTree.tail = cycle.tail;
                zonesTree = null;
            } else {
                dejaVu.placdl(new TapPair<>(zonesTree, tlPRZVsTree));
                if (zonesTree.head instanceof TapList) {
                    przvsSubTree = buildInfoPRZVTreeRec((TapList) zonesTree.head,
                            matrix, whichKind, dejaVu);
                } else {
                    int nbColumns = matrix.nCols;
                    int rowIndex;
                    BoolVector result = new BoolVector(nbColumns);
                    BoolVector row;
                    TapIntList zones = (TapIntList) zonesTree.head;
                    while (zones != null) {
                        rowIndex = zoneRkToKindZoneRk(zones.head, whichKind);
                        if (rowIndex >= 0) {
                            row = matrix.getRow(rowIndex);
                            if (row == null) {
                                if (rowIndex < nbColumns) {
                                    result.set(rowIndex, true);
                                }
                            } else {
                                result.cumulOr(row);
                            }
                        }
                        zones = zones.tail;
                    }
                    przvsSubTree = result;
                }
                tlPRZVsTree = tlPRZVsTree.placdl(przvsSubTree);
                zonesTree = zonesTree.tail;
            }
        }
        return hdPRZVsTree.tail;
    }

    /**
     * Fills the global BoolVector "uniqueAccessZones" with the zones
     * that are accessed during the enclosing loop iteration as
     * unique cells: these accesses will be temporarily marked "total".
     */
    public void setUniqueAccessZones(Block block) {
        uniqueAccessZones = new BoolVector(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
        LoopBlock enclosingLoop = block.enclosingLoop();
        if (enclosingLoop != null) {
            uniqueAccessZones.set(enclosingLoop.header().uniqueAccessZones, true);
        }
    }

    /**
     * Sets the info to "value" for all "kind" zones of reference expression "refTree".
     * When followPointers is true, does it also for the zones pointed by the pointers in refTree.
     * <br><b>Globals required:</b> {@link #curInstruction}, {@link #curSymbolTable}
     */
    public void setRefValue(Tree refTree, int kind, BoolVector info,
                            boolean value, boolean followPointers) {
        TapList argZonesTree = curSymbolTable.treeOfZonesOfValue(refTree, null, curInstruction, null);
        if (followPointers) {
            argZonesTree = TapList.copyTree(argZonesTree);
            includePointedElementsInTree(argZonesTree, null, null, true, true, true);
        }
        TapIntList allZones = removeSpecialZones(ZoneInfo.listAllZones(argZonesTree, true)) ;
        if (kind==SymbolTableConstants.ALLKIND) {
            setZoneRks(info, allZones, value);
        } else {
            setKindZoneRks(info, kind, allZones, value);
        }
    }

    /** Removes the zones for NULL, Undef (so far, we keep allocate's) */
    private TapIntList removeSpecialZones(TapIntList zones) {
        TapIntList toZones = new TapIntList(-1, zones) ;
        TapIntList inZones = toZones;
        ZoneInfo zi ;
        while (inZones.tail!=null) {
            if (inZones.tail.head<2) {
                inZones.tail = inZones.tail.tail ;
            } else {
                inZones = inZones.tail ;
            }
        }
        return toZones.tail ;
    }

    /**
     * True if this loop is guaranteed to run at least once.
     */
    public final boolean loopRunsAtLeastOnce(HeaderBlock block) {
        Instruction instr = block.lastInstr();
        boolean result = false;
        if (instr == null || instr.tree.opCode() != ILLang.op_loop) {
            return false;
        }
        Tree loopControl = instr.tree.down(3);
        if (loopControl.opCode() == ILLang.op_do) {
            Tree loopFrom = loopControl.down(2);
            Tree loopTo = loopControl.down(3);
            Tree loopStride = loopControl.down(4);
            if (ILUtils.isNullOrNone(loopStride)) {
                loopStride = ILUtils.build(ILLang.op_intCst, 1);
            }
            if (ILUtils.evalsToGTZero(loopStride)) {
                result = ILUtils.evalsToGEZero(ILUtils.subTree(ILUtils.copy(loopTo), ILUtils.copy(loopFrom)));
            } else if (ILUtils.evalsToLTZero(loopStride)) {
                result = ILUtils.evalsToGEZero(ILUtils.subTree(ILUtils.copy(loopFrom), ILUtils.copy(loopTo)));
            }
        }
        return result;
    }

    /**
     * @return the mask of all zones whose info must be propagated
     * from (or to) the upstream non-cycling entry of this header
     * to (or from) the downstream non-cycling exit of this header.
     * This is true for all zones if the loop may cycle 0 times,
     * else this is true for array zones that
     * 1) have been treated as unique cells inside the loop,
     * 2) and will not be unique cells outside the loop,
     * 3) and are not totally accessed inside the loop.
     * (cf bug in F77:lh05 with this refined version of activity)
     */
    protected final BoolVector directEntryExitMask(HeaderBlock header, int vectorLength, int whichKind) {
        BoolVector mask = new BoolVector(vectorLength);
        if (loopRunsAtLeastOnce(header)) {
            TapIntList topUniqueAccessHere = header.topUniqueAccessZones;
            TapIntList loopCompleteZones = header.loopCompleteZones;
            int declaredZone;
            ZoneInfo zi;
            while (topUniqueAccessHere != null) {
                declaredZone = topUniqueAccessHere.head;
                if (!TapIntList.contains(loopCompleteZones, declaredZone)) {
                    zi = header.symbolTable.declaredZoneInfo(declaredZone, SymbolTableConstants.ALLKIND);
                    if (zi != null && zi.kindZoneNb(whichKind) != -1) {
                        mask.set(zi.kindZoneNb(whichKind), true);
                    }
                }
                topUniqueAccessHere = topUniqueAccessHere.tail;
            }
        } else {
            mask.setTrue();
        }
        return mask;
    }

    /**
     * @return the TapIntList of all declared zones of the "whichKind" kind
     * that cannot travel directly from the entry to the exit of the cycle
     * headed by this "header".
     * This is an alternative to using directEntryExitMask().
     */
    public final TapIntList declaredZonesNotFromEntryToExit(HeaderBlock header, int whichKind) {
        TapIntList maskedZones = null;
        if (loopRunsAtLeastOnce(header)) {
            TapIntList topUniqueAccessHere = header.topUniqueAccessZones;
            TapIntList loopCompleteZones = header.loopCompleteZones;
            int declaredZone;
            ZoneInfo zi;
            while (topUniqueAccessHere != null) {
                declaredZone = topUniqueAccessHere.head;
                if (!TapIntList.contains(loopCompleteZones, declaredZone)) {
                    zi = header.symbolTable.declaredZoneInfo(declaredZone, SymbolTableConstants.ALLKIND);
                    if (zi != null && zi.kindZoneNb(whichKind) != -1) {
                        maskedZones = new TapIntList(zi.kindZoneNb(whichKind), maskedZones);
                    }
                }
                topUniqueAccessHere = topUniqueAccessHere.tail;
            }
        }
        return maskedZones;
    }

    /**
     * Displays into the trace the list of the "nZ" zones of kind "kind" in "unit".
     */
    protected static final void traceDisplayPrivateZones(Unit unit, int nZ, int kind) {
        SymbolTable symbolTable = unit.bodySymbolTable();
        if (symbolTable == null) {
            symbolTable = unit.publicSymbolTable();
        }
        TapEnv.printOnTrace(" D:") ;
        for (int i=0 ; i<nZ ; ++i) {
            ZoneInfo zi = symbolTable.declaredZoneInfo(i, kind);
            TapEnv.printOnTrace(" [" + i + "]" + (zi == null ? "?" : zi.description));
        }
        TapEnv.printlnOnTrace();
    }

    /**
     * Adds into "matrix" that "zonesTree" receives info "przvsTree",
     * i.e. schematically that " zonesTree := przvsTree ; "
     * Assumes that zonesTree and przvsTree are two matching trees
     * (if they don't match, does some union-merge of subtrees, to make the
     * two trees match anyway)
     * For each pair of corresponding leaves of the two trees, one leaf is
     * a TapIntList of extended declared zones designating rows in the "matrix",
     * the other leaf is a BoolVector. Sets these rows in "matrix" to
     * this BoolVector, observing the whichKind.
     */
    public final void setInfoPRZVTree(BoolMatrix matrix, TapList zonesTree, TapList przvsTree,
                                      TapList mask, boolean totalSet, int whichKind) {
        setInfoPRZVTreeRec(matrix, zonesTree, przvsTree,
                mask, totalSet, whichKind, new TapList<>(null, null));
    }

    private void setInfoPRZVTreeRec(
            BoolMatrix matrix, TapList zonesTree, TapList przvsTree,
            TapList mask, boolean totalSet, int whichKind, TapList<TapList> dejaVu) {
        // When the zonesTree is a pointer followed by its
        // destinations, and the przvsTree concerns only the pointer,
        // drop the part about destinations, to avoid the fallback
        // strategy that sets the boolean to ALL zones in zonesTree.
        if (przvsTree != null && przvsTree.tail == null && przvsTree.head instanceof BoolVector
                && zonesTree != null && zonesTree.tail != null && zonesTree.head instanceof TapIntList) {
            zonesTree = new TapList<>(zonesTree.head, null);
        }
        if (TapList.lengthWithCycle(zonesTree) == TapList.length(przvsTree)) {
            while (zonesTree != null && przvsTree != null) {
                if (TapList.contains(dejaVu.tail, zonesTree)) {
                    zonesTree = null;
                } else {
                    dejaVu.placdl(zonesTree);
                    if (przvsTree.head instanceof TapList) {
                        if (zonesTree.head instanceof TapList) {
                            setInfoPRZVTreeRec(matrix,
                                    (TapList) zonesTree.head, (TapList) przvsTree.head,
                                    mask != null && mask.head instanceof TapList ? (TapList) mask.head : mask,
                                    totalSet, whichKind, dejaVu);
                        } else if (allowedByMaskHead(mask)) {
                            // Weird case where zonesTree and przvsTree do not match!
                            // Here "non-record" := "record"
                            BoolVector onePRZV = new BoolVector(matrix.nCols);
                            cumulOr((TapList) przvsTree.head, onePRZV, false);
                            TapIntList zones = (TapIntList) zonesTree.head;
                            zones = mapZoneRkToKindZoneRk(zones, whichKind);
                            matrix.overwriteDeps(zones, onePRZV, totalSet);
                        }
                    } else if (przvsTree.head != null) {
                        if (allowedByMaskHead(mask)) {
                            TapIntList zones;
                            if (zonesTree.head instanceof TapList)
                            //Weird case where zonesTree and przvsTree do not match!
                            // Here "record" := "non-record"
                            {
                                zones = ZoneInfo.listAllZones((TapList) zonesTree.head, true);
                            } else {
                                zones = (TapIntList) zonesTree.head;
                            }
                            // normal leaf case: flat list of zones receive a new BoolVector info:
                            zones = mapZoneRkToKindZoneRk(zones, whichKind);
                            matrix.overwriteDeps(zones, (BoolVector) przvsTree.head, totalSet);
                        }
                    }
                    zonesTree = zonesTree.tail;
                    przvsTree = przvsTree.tail;
                    mask = mask == null ? null : mask.tail;
                }
            }
        } else {
            // Weird case where  przvsTree and boolsTree do not match!
            // Summarize przvsTree as a single BoolVector, and propagate it into zonesTree, but not across pointers!
            BoolVector newVal = new BoolVector(matrix.nCols);
            cumulOr(przvsTree, newVal, false);
            TapIntList zones = mapZoneRkToKindZoneRk(ZoneInfo.listAllZones(zonesTree, false), whichKind);
            matrix.overwriteDeps(zones, newVal, totalSet);
        }
    }

    /**
     * Updates the BoolVector "infos", which holds some information on
     * private "whichKind" zones, by setting to "newVal" the info of the
     * zones in "zonesTree" that pass through the "mask".
     * When non-null, "mask" indicates the parts of zonesTree that must be considered.
     * At each leaf in "zonesTree" that passes through the "mask", the setting to "newVal"
     * is done according to the boolean "totalSet" and to the global "conservativeValue"
     * for the current data-flow analysis, as defined in method conservativeSetExtendedDeclared().
     * <br><b>Globals required:</b> {@link #curSymbolTable}
     */
    public final void setInfoBoolTreeToExtendedDeclaredZones(BoolVector infos, TapList zonesTree,
                                                             boolean newVal, TapList mask, boolean totalSet, int whichKind) {
        setInfoBoolTreeToExtendedDeclaredZonesRec(infos, zonesTree, newVal, mask, totalSet, whichKind,
                new TapList<>(null, null));
    }

    private void setInfoBoolTreeToExtendedDeclaredZonesRec(BoolVector infos, TapList zonesTree,
                                                           boolean newVal, TapList mask, boolean totalSet, int whichKind,
                                                           TapList<TapList> dejaVu) {
        while (zonesTree != null) {
            if (TapList.contains(dejaVu.tail, zonesTree)) {
                zonesTree = null;
            } else {
                dejaVu.placdl(zonesTree);
                if (zonesTree.head instanceof TapList) {
                    setInfoBoolTreeToExtendedDeclaredZonesRec(infos, (TapList) zonesTree.head, newVal,
                            mask != null && mask.head instanceof TapList ? (TapList) mask.head : mask,
                            totalSet, whichKind, dejaVu);
                } else {
                    if (allowedByMaskHead(mask)) {
                        conservativeSetExtendedDeclared(infos, (TapIntList) zonesTree.head, newVal,
                                conservativeValue, totalSet, whichKind, curSymbolTable);
                    }
                }
                zonesTree = zonesTree.tail;
                mask = mask == null ? null : mask.tail;
            }
        }
    }

    /**
     * Updates the BoolVector "infos", which holds some information on
     * private "whichKind" zones, with the additional tree of infos given in "boolsTree".
     * The tree "boolsTree" is the tree of infos for a given expression, for which the
     * corresponding tree of declared zones is the given "zonesTree".
     * When non-null, "mask" indicates the parts of zonesTree/boolsTree that must be considered.
     * This method recursively traverses "zonesTree", "mask", and "boolsTree" in parallel,
     * and when it reaches leaves and the mask allows for it, it sets into "infos" at the
     * current zones ("zonesTree") the boolean info found in "boolsTree".
     * This setting varies according to the boolean "totalSet" and to the global "conservativeValue"
     * for the current data-flow analysis, as defined in method conservativeSetExtendedDeclared().
     * <br><b>Globals required:</b> {@link #curSymbolTable}
     */
    public final void setInfoBoolTreeToExtendedDeclaredZones(BoolVector infos, TapList zonesTree,
                                                             TapList boolsTree, TapList mask, boolean totalSet, int whichKind) {
        setInfoBoolTreeToExtendedDeclaredZonesRec(infos, zonesTree, boolsTree, mask, totalSet, whichKind,
                new TapList<>(null, null));
    }

    private void setInfoBoolTreeToExtendedDeclaredZonesRec(BoolVector infos, TapList zonesTree,
                                                           TapList boolsTree, TapList mask, boolean totalSet, int whichKind,
                                                           TapList<TapList> dejaVu) {
        // When the zonesTree is a pointer followed by its
        // destinations, and the boolsTree concerns only the pointer,
        // drop the part about destinations, to avoid the fallback
        // strategy that sets the boolean to ALL zones in zonesTree.
        if (boolsTree != null && boolsTree.tail == null && boolsTree.head instanceof Boolean
                && zonesTree != null && zonesTree.tail != null && zonesTree.head instanceof TapIntList) {
            zonesTree = new TapList<>(zonesTree.head, null);
        }
// The following horrible case is removed (can't remember what it was for?) and it now caused a bug
// (lost information during activity propagation, by translateCalleeDataToCallSite(BoolVector,...) e.g. on set11/lh042).
// Our present convention is that the "Bool Trees" such as boolsTree may have a smaller length
//  (i.e. a smaller number of fields) than zonesTree in the case where the info for
// the ommitted last fields is "empty" or "false". 
//         if (TapList.lengthWithCycle(zonesTree) != TapList.lengthWithCycle(boolsTree)) {
//             // Weird case where  zonesTree and boolsTree do not match!
//             // Summarize boolsTree as a single boolean, and propagate it into zonesTree, but not across pointers!
//             boolean newVal = TapList.oneTrue(boolsTree);
//             TapIntList zonesList = ZoneInfo.listAllZones(zonesTree, false);
//             conservativeSetExtendedDeclared(infos, zonesList, newVal,
//                     conservativeValue, totalSet, whichKind, curSymbolTable);
//         } else {
            while (zonesTree != null && boolsTree != null) {
                if (TapList.contains(dejaVu.tail, zonesTree)) {
                    zonesTree = null;
                } else {
                    dejaVu.placdl(zonesTree);
                    if (boolsTree.head instanceof TapList) {
                        if (zonesTree.head instanceof TapList) {
                            setInfoBoolTreeToExtendedDeclaredZonesRec(infos,
                                    (TapList) zonesTree.head, (TapList) boolsTree.head,
                                    mask != null && mask.head instanceof TapList ? (TapList) mask.head : mask,
                                    totalSet, whichKind, dejaVu);
                        } else {
                            // Weird case where zonesTree and boolsTree do not match!
                            // Here "non-record" := "record"
                            if (allowedByMaskHead(mask)) {
                                conservativeSetExtendedDeclared(infos, (TapIntList) zonesTree.head,
                                        TapList.oneTrueInHead(boolsTree),
                                        conservativeValue, totalSet, whichKind, curSymbolTable);
                            }
                        }
                    } else if (boolsTree.head != null) {
                        if (allowedByMaskHead(mask)) {
                            TapIntList zones;
                            if (zonesTree.head instanceof TapList) {
                                // Weird case where zonesTree and boolsTree do not match!
                                // Here "record" := "non-record"
                                zones = ZoneInfo.listAllZones((TapList) zonesTree.head, true);
                            } else {
                                zones = (TapIntList) zonesTree.head;
                            }
                            conservativeSetExtendedDeclared(infos, zones, (Boolean) boolsTree.head,
                                    conservativeValue, totalSet, whichKind, curSymbolTable);
                        }
                    }
                    zonesTree = zonesTree.tail;
                    boolsTree = boolsTree.tail;
                    mask = mask == null ? null : mask.tail;
                }
            }
//         }
    }

    /**
     * @return true when the mask.head lets us look at the head of the present (TapList) trees.
     */
    private boolean allowedByMaskHead(TapList<?> mask) {
        if (mask == null) {
            return true;
        } else if (mask.head instanceof Boolean) {
            return (Boolean) mask.head;
        } else {
            return TapList.oneTrueInHead(mask);
        }
    }

    /**
     * Builds and returns the list of origins of the given
     * list of FGArrow's, in the same order.
     */
    private TapList<Block> mapOrigin(TapList<FGArrow> arrows) {
        TapList<Block> hdOrigins = new TapList<>(null, null);
        TapList<Block> tlOrigins = hdOrigins;
        while (arrows != null) {
            tlOrigins = tlOrigins.placdl(arrows.head.origin);
            arrows = arrows.tail;
        }
        return hdOrigins.tail;
    }

    /**
     * Builds and returns the list of destinations of the given
     * list of FGArrow's, in the same order.
     */
    private TapList<Block> mapDestination(TapList<FGArrow> arrows) {
        TapList<Block> hdDestinations = new TapList<>(null, null);
        TapList<Block> tlDestinations = hdDestinations;
        while (arrows != null) {
            tlDestinations = tlDestinations.placdl(arrows.head.destination);
            arrows = arrows.tail;
        }
        return hdDestinations.tail;
    }

    /**
     * In a given BoolVector, sets a given boolean value to all given declared zones.
     * For each declared ALLKIND zone in "zones", tries to set its "whichKind" value in "infos" to "newVal".
     * each declared ALLKIND zone is first converted into a "whichKind" zone number, as used in "infos".
     * Then for each "whichKind" zone, this setting is conservative in the following way:
     * if the existing value in "infos" is different from the desired new "newVal",
     * then "newVal" overrides the existing value only if this access is total
     * (i.e. "total" is true and the zone is not marked "multiple").
     * Otherwise if the access is not total, the value is set to the "conservativeVal",
     * which depends on the particular analysis/application.
     */
    private void conservativeSetExtendedDeclared(BoolVector infos,
                                                 TapIntList zones, boolean newVal, boolean conservativeVal,
                                                 boolean total, int whichKind, SymbolTable symbolTable) {
        ZoneInfo zoneInfo;
        int index;
        while (zones != null) {
            zoneInfo = symbolTable.declaredZoneInfo(zones.head, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null && zoneInfo.zoneNb>1) { // zoneNb is 0 for NULL and 1 for UnknownDest
                index = zoneRkToKindZoneRk(zones.head, whichKind);
                if (index >= 0) {
                    if (newVal != infos.get(index)) {
                        if (total && !zoneInfo.multiple) {
                            infos.set(index, newVal);
                        } else {
                            infos.set(index, conservativeVal);
                        }
                    }
                }
            }
            zones = zones.tail;
        }
    }

    /**
     * @param zoneRk the given extended declared ALLKIND zone rank.
     * @return true if "zoneRk" is for a scalar variable.
     */
    public final boolean zoneIsScalar(int zoneRk) {
        ZoneInfo zoneInfo = curSymbolTable.declaredZoneInfo(zoneRk, SymbolTableConstants.ALLKIND);
        return zoneInfo != null && zoneInfo.accessIndexes == null && zoneInfo.targetZoneOf == null;
    }

    /**
     * @param zoneRk the given extended declared ALLKIND zone rank.
     * @return true if "zoneRk" is for a pointer.
     */
    public final boolean zoneIsPointer(int zoneRk) {
        ZoneInfo zoneInfo = curSymbolTable.declaredZoneInfo(zoneRk, SymbolTableConstants.ALLKIND);
        return zoneInfo != null && zoneInfo.ptrZoneNb != -1;
    }

    /**
     * @return a String that shows the given data-flow info, given its length and kind,
     * and prefixing it with the key to the meaning of the zone numbers in the dump file.
     */
    public final String infoToString(BoolVector info, int infoLen, int kind) {
        return infoToString(info, infoLen, kind, curUnit, curSymbolTable);
    }

    /**
     * Static version of infoToString().
     */
    public static String infoToString(BoolVector info, int infoLen, int kind, Unit unit, SymbolTable symbolTable) {
        if (info == null) {
            return "null";
        }
        String kindLetter;
        switch (kind) {
            case SymbolTableConstants.ALLKIND:
                kindLetter = "a";
                break;
            case SymbolTableConstants.REALKIND:
                kindLetter = "r";
                break;
            case SymbolTableConstants.INTKIND:
                kindLetter = "i";
                break;
            case SymbolTableConstants.PTRKIND:
                kindLetter = "p";
                break;
            default:
                kindLetter = "?";
                break;
        }
        return "[s" + unit.rank() + ':' + kindLetter + "||d" + symbolTable.rank() + ':' + kindLetter + "] "
                + info.toString(infoLen);
    }

    /**
     * @return a string that shows the given data-flow info, which is made of
     * two parts, one on the original (ALLKIND) zones, the other on the future
     * derivatives of pointer (PTRKIND) zones.
     */
    protected final String infoToStringWithDiffPtr(BoolVector info, int infoLen, BoolVector infoOnDiffPtr, int infoLenOnDiffPtr) {
        String ptrString = "";
        if (infoOnDiffPtr != null) {
            ptrString = " Ptr:" + infoToString(infoOnDiffPtr, infoLenOnDiffPtr, SymbolTableConstants.PTRKIND);
        }
        return infoToString(info, infoLen, SymbolTableConstants.ALLKIND) + ptrString;
    }

    /**
     * Static version of infoToStringWithDiffPtr().
     */
    public static String infoToStringWithDiffPtr(BoolVector info, int infoLen, BoolVector infoOnDiffPtr, int infoLenOnDiffPtr, Unit unit, SymbolTable symbolTable) {
        String ptrString = "";
        if (infoOnDiffPtr != null) {
            ptrString = " Ptr:" + infoToString(infoOnDiffPtr, infoLenOnDiffPtr, SymbolTableConstants.PTRKIND, unit, symbolTable);
        }
        return infoToString(info, infoLen, SymbolTableConstants.ALLKIND, unit, symbolTable) + ptrString;
    }

    /**
     * @return a String that shows the given data-flow info, given its length and kind,
     * followed by the same "cycling" info i.e. the special, more accurate, info that
     * is propagated through the cycles of an enclosing clean DO-loop.
     * <p>
     * For future dataflow analysis.
     */
    private final String infoToStringWithCycle(BoolVector info, int infoLen, int kind, BoolVector infoCycle) {
        String cycleString = "";
        if (infoCycle != null) {
            cycleString = " (cycling:" + infoToString(infoCycle, infoLen, kind) + ')';
        }
        return infoToString(info, infoLen, kind) + cycleString;
    }

    /**
     * @return a string that shows the given data-flow info, which is made of
     * four parts: the first on the original (ALLKIND) zones, the second on the future
     * derivatives of pointer (PTRKIND) zones, and the third and fourth are the same for
     * the "cycling" info i.e. the special, more accurate, info that is propagated through
     * the cycles of an enclosing clean DO-loop.
     */
    protected final String infoToStringWithDiffPtrWithCycle(BoolVector info, int infoLen, BoolVector infoOnDiffPtr, int infoLenOnDiffPtr, BoolVector infoCycle, BoolVector infoCycleOnDiffPtr) {
        String cycleString = "";
        if (infoCycle != null || infoCycleOnDiffPtr != null) {
            cycleString =
                    " (cycling:" + infoToStringWithDiffPtr(infoCycle, infoLen, infoCycleOnDiffPtr, infoLenOnDiffPtr) + ')';
        }
        return infoToStringWithDiffPtr(info, infoLen, infoOnDiffPtr, infoLenOnDiffPtr) + cycleString;
    }

//-----------------------------------------------------------------------------------------
// CONVENTION on zones and their rank:
//  - zone rank 0 always represents the "destination" of NULL pointers
//  - zone rank 1 always represents the "destination" of undefined pointers
//  - next zone ranks represent IO streams (presently only rank 2)
//  - next zone ranks represent MPI channels (generally none or only one)
//  - next zone ranks represent the global variables of each of the application languages
//  - next zone ranks represent the global variables of each source file
//  - next zone ranks represent the variables of modules and other packages.
//  The above zone ranks are global: they always represent the same "variable/memory".
//  Then each procedure starts numbering of its own formal arguments
//   (and then its local variables) up from the highest global zone rank.
//-----------------------------------------------------------------------------------------

// CONVERSION UTILITIES BETWEEN:
//  -- zone rank of the general kind (ALLKIND) (from 0 up)
//  -- zone rank of a special kind (in {REALKIND, PTRKIND, ...} but possibly also ALLKIND)
//  -- zone ZoneInfo that contains useful info about one zone

    /**
     * Translates an extended declared zone rank into the corresponding BoolVector rank,
     * where the BoolVector is of kind "zoneKind".
     * <br><b>Globals required:</b> {@link #curCallGraph}, {@link #curSymbolTable}
     * @param extendedRk The given extended declared zone rank.
     * @param zoneKind   The kind (in {ALLKIND, REALKIND, PTRKIND}) used in the BoolVector.
     * @return The corresponding BoolVector rank.
     */
    public final int zoneRkToKindZoneRk(int extendedRk, int zoneKind) {
        if (extendedRk < 0 || extendedRk >= nDZ) return -1 ;
        // Make sure that this is not one of the root dummy zones (NULL, Undef, IO, channels), because
        // there cannot be any data-flow info attached to them (if there is one, it is wrong!)
        // This filtering should not be necessary, but sometimes it is...
        if (extendedRk<curCallGraph.numberOfDummyRootZones) return -1 ;
        if (zoneKind == SymbolTableConstants.ALLKIND) {
            //Translation is easier: no need to find the ZoneInfo:
            return extendedRk ;
        } else {
            //Need to find the ZoneInfo to get the "zoneKind" zone number:
            ZoneInfo zi = curSymbolTable.declaredZoneInfo(extendedRk, SymbolTableConstants.ALLKIND);
            int kZoneNb = (zi==null ? -1 : zi.kindZoneNb(zoneKind));
            return (kZoneNb==-1 ? -1 : kZoneNb);
        }
    }

    /**
     * Translates an extended declared zone rank into the corresponding BoolVector rank,
     * where the BoolVector is of kind "zoneKind". Static version.
     * @param extendedRk  The given extended declared zone rank.
     * @param zoneKind    The kind (in {ALLKIND, REALKIND, PTRKIND}) used in the BoolVector.
     * @param symbolTable The current symbolTable.
     * @return The corresponding BoolVector rank.
     */
    public static final int zoneRkToKindZoneRk(int extendedRk, int zoneKind, SymbolTable symbolTable) {
        if (extendedRk < 0 || extendedRk >= symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND)) return -1 ;
        // Make sure that this is not one of the root dummy zones (NULL, Undef, IO, channels), because
        // there cannot be any data-flow info attached to them (if there is one, it is wrong!)
        // This filtering should not be necessary, but sometimes it is...
        if (extendedRk<symbolTable.getCallGraph().numberOfDummyRootZones) return -1 ;
        if (zoneKind == SymbolTableConstants.ALLKIND) {
            //Translation is easier: no need to find the ZoneInfo:
            return extendedRk ;
        } else {
            //Need to find the ZoneInfo to get the "zoneKind" zone number:
            ZoneInfo zi = symbolTable.declaredZoneInfo(extendedRk, SymbolTableConstants.ALLKIND);
            int kZoneNb = (zi==null ? -1 : zi.kindZoneNb(zoneKind));
            return (kZoneNb==-1 ? -1 : kZoneNb);
        }
    }

    /**
     * Translates a list of extended declared zone ranks into a list of corresponding BoolVector ranks,
     * where the BoolVector is of kind "zoneKind". Preserves the order.
     * <br><b>Globals required:</b> {@link #curCallGraph}, {@link #curSymbolTable}
     * @param extendedRks The given list of extended declared zone ranks.
     * @param zoneKind   The kind (in {ALLKIND, REALKIND, PTRKIND}) used in the BoolVector.
     * @return The corresponding list of BoolVector ranks.
     */
    public final TapIntList mapZoneRkToKindZoneRk(TapIntList extendedRks, int zoneKind) {
        TapIntList hdResult = new TapIntList(-1, null);
        TapIntList tlResult = hdResult;
        int index;
        while (extendedRks != null) {
            index = zoneRkToKindZoneRk(extendedRks.head, zoneKind);
            if (index >= 0) {
                tlResult = tlResult.placdl(index);
            }
            extendedRks = extendedRks.tail;
        }
        return hdResult.tail;
    }

    /**
     * Translates a list of extended declared zone ranks into a list of corresponding BoolVector ranks,
     * where the BoolVector is of kind "zoneKind". Preserves the order. Static version.
     * @param extendedRks The given list of extended declared zone ranks.
     * @param zoneKind    The kind (in {ALLKIND, REALKIND, PTRKIND}) used in the BoolVector.
     * @param symbolTable The current symbolTable.
     * @return The corresponding list of BoolVector ranks.
     */
    public static final TapIntList mapZoneRkToKindZoneRk(TapIntList extendedRks, int zoneKind,
                                                         SymbolTable symbolTable) {
        TapIntList hdResult = new TapIntList(-1, null);
        TapIntList tlResult = hdResult;
        int index;
        while (extendedRks != null) {
            index = zoneRkToKindZoneRk(extendedRks.head, zoneKind, symbolTable);
            if (index >= 0) {
                tlResult = tlResult.placdl(index);
            }
            extendedRks = extendedRks.tail;
        }
        return hdResult.tail;
    }

    /**
     * Translates a list of extended declared zone ranks into a list of corresponding ZoneInfo's,
     * @param extendedRks the given list of extended declared zone ranks
     * @param symbolTable The current symbolTable.
     * @return the corresponding list of ZoneInfo's.
     */
    public static TapList<ZoneInfo> mapZoneRkToZoneInfo(TapIntList extendedRks, SymbolTable symbolTable) {
        TapList<ZoneInfo> hdResult = new TapList<>(null, null);
        TapList<ZoneInfo> tlResult = hdResult;
        ZoneInfo zoneInfo;
        while (extendedRks != null) {
            zoneInfo = symbolTable.declaredZoneInfo(extendedRks.head, SymbolTableConstants.ALLKIND);
            if (zoneInfo != null) {
                tlResult = tlResult.placdl(zoneInfo);
            }
            extendedRks = extendedRks.tail;
        }
        return hdResult.tail;
    }

    /**
     * Translates a zone rank "zoneRk" of kind "zoneKind", into an ALLKIND zone rank.
     * @param zoneRk    The given zone index.
     * @param zoneKind  The kind of the given zone, in {ALLKIND, REALKIND, PTRKIND}.
     * @param symbolTable The current symbolTable.
     * @return the corresponding extended declared zone rank.
     */
    public static final int kindZoneRkToZoneRk(int zoneRk, int zoneKind, SymbolTable symbolTable) {
        if (zoneRk>=0 && zoneKind!=SymbolTableConstants.ALLKIND) {
            //Need to go back to the ZoneInfo
            ZoneInfo zoneInfo = symbolTable.declaredZoneInfo(zoneRk, zoneKind);
            zoneRk = (zoneInfo==null ? -1 : zoneInfo.zoneNb) ;
        }
        return (zoneRk<0 ? -1 : zoneRk) ;
    }

}
