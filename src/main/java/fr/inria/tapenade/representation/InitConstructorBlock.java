/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Special Block at the entry of a Unit.
 */
public final class InitConstructorBlock extends Block {

    /**
     * Creation, with the given "symbolTable" attached.
     */
    protected InitConstructorBlock(SymbolTable symbolTable, TapList<Block> allBlocks) {
        super(symbolTable, null, allBlocks);
    }

    @Override
    public String toString() {
        return "I" + super.toString();
    }
}
