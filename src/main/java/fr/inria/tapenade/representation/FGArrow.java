/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * An arrow in the Flow Graph.
 */
public final class FGArrow {
    public Block origin;
    public Block destination;
    public int test;
    public TapIntList cases;
    public int rank;
    public Object carry;
    /**
     * The "iter" field represents the effect of this FGArrow
     * with respect to loops, i.e. the list of loops that this
     * arrow POPs from, followed by the loop it CYCLE's in or by
     * the list of loops it PUSH'es into.
     * It contains a list of TapPair's, whose
     * first element is the action in {POP, PUSH, CYCLE}, and the
     * second the loop.
     */
    public TapList<TapPair<Integer, LoopBlock>> iter;
    /**
     * Boolean which is true for arrows that go to the header of a SYNTACTIC loop,
     * i.e. a Do loop, back from syntactically INSIDE the loop,
     * without going through the outside of the loop.
     */
    public boolean inACycle;
    /**
     * Boolean which is true when this arrow is a jump from a switch case to
     * the next switch case.
     */
    public boolean isAJumpIntoNextCase;
    /**
     * Mark if the arrow is reachable during runtime. Can be set to false
     * when we can infer that a conditional will be always true or false for example.
     */
    public boolean reachable = true;

    /**
     * Creates a new FGArrow, and sets it from Block "origin"
     * to Block "destination", with "test" and "cas".
     * Adds this arrow to the corresponding flow and backFlow.
     */
    public FGArrow(Block origin, int test, int cas, Block destination) {
        super();
        this.origin = origin;
        this.test = test;
        this.cases = test == FGConstants.NO_TEST ? null : new TapIntList(cas, null);
        this.destination = destination;
        this.carry = null;
        if (origin != null) {
            origin.addFlow(this);
        }
        if (destination != null) {
            destination.addBackFlow(this);
        }
    }

    public FGArrow(Block origin, int test, TapIntList cases, Block destination) {
        super();
        this.origin = origin;
        this.test = test;
        this.cases = cases;
        this.destination = destination;
        this.carry = null;
        if (origin != null) {
            origin.addFlow(this);
        }
        if (destination != null) {
            destination.addBackFlow(this);
        }
    }

    public FGArrow(Block origin, int test, int cas, Block destination, boolean inCycle) {
        super();
        this.origin = origin;
        this.test = test;
        this.cases = test == FGConstants.NO_TEST ? null : new TapIntList(cas, null);
        this.destination = destination;
        this.carry = null;
        this.inACycle = inCycle;
        if (origin != null) {
            origin.addFlow(this);
        }
        if (destination != null) {
            destination.addBackFlow(this);
        }
    }

    public FGArrow(Block origin, int test, TapIntList cases, Block destination, boolean inCycle) {
        super();
        this.origin = origin;
        this.test = test;
        this.cases = cases;
        this.destination = destination;
        this.carry = null;
        this.inACycle = inCycle;
        if (origin != null) {
            origin.addFlow(this);
        }
        if (destination != null) {
            destination.addBackFlow(this);
        }
    }

    public FGArrow() {
        super();
    }

    public static String testAndCasesToString(int test, TapIntList cases) {
        StringBuilder result;
        switch (test) {
            case FGConstants.LOOP:
                result = new StringBuilder("LOOP:");
                break;
            case FGConstants.IF:
                result = new StringBuilder("IF:");
                break;
            case FGConstants.CASE:
                result = new StringBuilder("CASE:");
                break;
            case FGConstants.ENTRY:
                result = new StringBuilder("ENTRY:");
                break;
            case FGConstants.COMP_GOTO:
                result = new StringBuilder("COMP_GOTO:");
                break;
            case FGConstants.ASS_GOTO:
                result = new StringBuilder("ASS_GOTO:");
                break;
            case FGConstants.IO_GOTO:
                result = new StringBuilder("IO_GOTO:");
                break;
            case FGConstants.CALL:
                result = new StringBuilder("CALL:");
                break;
            case FGConstants.NO_TEST:
                result = new StringBuilder("====");
                break;
            default:
                result = new StringBuilder("??:");
                break;
        }
        TapIntList inCases = cases;
        int theCase;
        while (inCases != null) {
            theCase = inCases.head;
            switch (test) {
                case FGConstants.LOOP:
                    if (theCase == FGConstants.NEXT) {
                        result.append("NEXT");
                    } else if (theCase == FGConstants.EXIT) {
                        result.append("EXIT");
                    } else {
                        result.append("??");
                    }
                    break;
                case FGConstants.IF:
                    if (theCase == FGConstants.TRUE) {
                        result.append("TRUE");
                    } else if (theCase == FGConstants.FALSE) {
                        result.append("FALSE");
                    } else if (theCase == FGConstants.ELSEWHERE) {
                        result.append("ELSEWHERE");
                    } else {
                        result.append("??");
                    }
                    break;
                case FGConstants.CASE:
                    if (theCase == FGConstants.DEFAULT) {
                        result.append("DEFAULT");
                    } else {
                        result.append(theCase);
                    }
                    break;
                case FGConstants.ENTRY:
                    if (theCase == FGConstants.MAIN) {
                        result.append("MAIN");
                    } else {
                        result.append(theCase);
                    }
                    break;
                case FGConstants.COMP_GOTO:
                case FGConstants.ASS_GOTO:
                case FGConstants.IO_GOTO:
                case FGConstants.CALL:
                    result.append(theCase);
                    break;
                case FGConstants.NO_TEST:
                    break;
                default:
                    result.append("??");
                    break;
            }
            inCases = inCases.tail;
            if (inCases != null) {
                result.append('+');
            }
        }
        return result.toString();
    }

    /**
     * @return true if this arrow goes back to the header of a
     * loop, comes from inside this loop, and is not considered
     * going "out-then-back-in" this loop.
     */
    public boolean isCyclingArrow() {
        TapList<LoopBlock> destLoops = destination.enclosingLoops();
        return destLoops != null
                && destLoops.head.header() == destination
                && TapList.contains(origin.enclosingLoops(), destLoops.head)
                // An arrow that is apparently a cycle (i.e. from inside a loop to this
                //  loop header) really is cycling if in addition, either this loop
                //  is NOT a syntactic loop, or this arrow is marked as "inACycle":
                && (inACycle || !destination.isALoop());
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    /**
     * Deletes this FGArrow. Removes it from the flow from
     * its origin and from the backFlow to its destination.
     */
    public void delete() {
        redirectOrigin(null);
        redirectDestination(null);
    }

    /**
     * Redirects the origin of this FGArrow to "block".
     * If block==null, this disconnects this FGArrow from its origin.
     */
    public void redirectOrigin(Block block) {
        if (block != origin) {
            if (origin != null) {
                origin.setFlow(TapList.delete(this, origin.flow()));
            }
            origin = block;
            if (origin != null) {
                origin.setFlow(new TapList<>(this, origin.flow()));
            }
        }
    }

    /**
     * Redirects the destination of this FGArrow to "block".
     * If block==null, this disconnects this FGArrow from its destination.
     */
    public void redirectDestination(Block block) {
        if (block != destination) {
            if (destination != null) {
                destination.setBackFlow(TapList.delete(this, destination.backFlow()));
            }
            destination = block;
            if (destination != null) {
                destination.setBackFlow(new TapList<>(this, destination.backFlow()));
            }
        }
    }

    /**
     * Redirects the destination of this FGArrow to "block"
     * and may declare this FGArrow a cycle in the loop headed by "block".
     */
    public void redirectDestination(Block block, boolean cycle) {
        redirectDestination(block);
        inACycle = cycle;
    }

    /**
     * Inserts "block" between the current origin and current destination
     * of this FGArrow, so that this FGArrow now designates the arrow
     * that flows FROM the given "block".
     */
    public void insertBlockAtOrig(Block block, int test, int cas) {
        Block previousOrigin = origin;
        redirectOrigin(block);
        FGArrow newArrow =
                new FGArrow(previousOrigin, this.test, this.cases, block, false);
        //If we add some statements in the middle of a CASE-NOMATCH arrow,
        // we must turn this arrow into a CASE-DEFAULT (cf C:v105, F90:lh45) :
        newArrow.turnNomatchIntoDefault();
        this.test = test;
        this.cases = test == FGConstants.NO_TEST ? null : new TapIntList(cas, null);
    }

    /**
     * Inserts "block" between the current origin and current destination
     * of this FGArrow, so that this FGArrow now designates the arrow
     * that flows INTO the given "block".
     *
     * @return the newly created arrow that flows FROM the given "block".
     */
//[llh WARNING] on 9/9/2010 I changed the special case where
//  this FGArrow had no destination. It used to place no exiting flow
//  FGArrow after block; now it places an FGArrow, but with no destination !
    public FGArrow insertBlockAtDest(Block block, int test, int cas) {
        Block previousDestination = destination;
        redirectDestination(block);
        //If we add some statements in the middle of a CASE-NOMATCH arrow,
        // we must turn this arrow into a CASE-DEFAULT (cf C:v105, F90:lh45) :
        turnNomatchIntoDefault();
        FGArrow newArrow =
                new FGArrow(block, test, cas, previousDestination, this.inACycle);
        // If this arrow was a jump into next case of a switch, then
        // it's the new arrow flowing from "block" that inherits this property.
        // Same thing for the "inACycle" property.
        newArrow.isAJumpIntoNextCase = this.isAJumpIntoNextCase;
        this.inACycle = false;
        this.isAJumpIntoNextCase = false;
        return newArrow;
    }

    /**
     * Turns the unique final NOMATCH case after a switch into
     * a DEFAULT (which will also be unique and final).
     */
    public void turnNomatchIntoDefault() {
        if (test == FGConstants.CASE && TapIntList.contains(cases, FGConstants.NOMATCH)) {
            cases = TapIntList.copy(cases);
            TapIntList inCases = cases;
            while (inCases != null) {
                if (inCases.head == FGConstants.NOMATCH) {
                    inCases.head = FGConstants.DEFAULT;
                }
                inCases = inCases.tail;
            }
            Instruction switchInstr = origin.lastInstr();
            if (switchInstr != null) {
                Tree tree = switchInstr.tree;
                if (tree != null && tree.opCode() == ILLang.op_switch) {
                    tree.down(2).addChild(ILUtils.build(ILLang.op_switchCase,
                            ILUtils.build(ILLang.op_expressions),
                            ILUtils.build(ILLang.op_blockStatement)),
                            1 + tree.down(2).length());
                }
            }
        }
    }

    /**
     * Assume that this FGArrow and "newArrow" have same origin
     * and destination. This function tries to merge the two
     * FGArrows' test/cases into the test/case of this FGArrow.
     */
    public void mergeCases(FGArrow newArrow) {
        if (test != newArrow.test) {
            if (!(newArrow.cases != null && newArrow.cases.head == FGConstants.ELSEWHERE)) {
                TapEnv.toolWarning(-1, " FGArrow.mergeCases: Incompatible arrow tests " + test + " and " + newArrow.test);
            }
        } else if (test == FGConstants.IF) {
            int totalcases;
            totalcases = cases.head + newArrow.cases.head;
            if (totalcases < FGConstants.ALL_CASES) {
                cases = new TapIntList(totalcases, null);
            } else {
                test = FGConstants.NO_TEST;
                cases = null;
            }
        } else if (test == FGConstants.CASE) {
            cases = TapIntList.append(cases, newArrow.cases);
            /*// On range la liste cases par ordre croissant*/
            cases = TapIntList.sort(cases);
        } else {
            if
            (cases != null && TapIntList.contains(cases, FGConstants.DEFAULT)
                    || newArrow.cases != null
                    && TapIntList.contains(newArrow.cases, FGConstants.DEFAULT)) {
                cases = new TapIntList(FGConstants.DEFAULT, null);
            } else {
                cases = TapIntList.append(cases, newArrow.cases);
            }
        }
    }

    /**
     * @return True when this FGArrow exits from a loop and this loop has a loop local SymbolTable
     */
    public boolean exitsFromLoopAndLoopSymbolTable() {
        return containsCase(FGConstants.EXIT) && origin.symbolTable.declarationsBlock == origin;
    }

    public boolean containsCase(int oneCase) {
        if (test == FGConstants.IF) {
            return (cases.head & oneCase) != 0;
        } else {
            return TapIntList.contains(cases, oneCase);
        }
    }

    /**
     * Fills the field iter that summarizes the movement across nested loops structure.
     * Also sets all the references to this FGArrow into the entryBlocks,
     * exitBlocks, entryArrows, exitArrows, and cycleArrows of LoopBlocks.
     */
    public void coherence() {
        TapList<LoopBlock> origLoops = origin.enclosingLoops();
        TapList<LoopBlock> destLoops = destination.enclosingLoops();
        TapList<LoopBlock> tmpList = null;
        LoopBlock loop;
        boolean apparentlyCycling =
                destLoops != null && destination == destLoops.head.header();
        // We are really in a cycle if this arrow really goes from the end of one iteration
        // to the next iteration, remaining in the same instance of the enclosing loop.
        // This is true for all non-Do loops, and for the "cycle" arrows of DO-loops
        boolean reallyInACycle = apparentlyCycling
                && (inACycle || !destination.isALoop());
        // outThenBackIn is true when this is an arrow from inside a loop to this loop header,
        // but is really a jump out of the loop followed by a restart of the whole loop.
        // Example : ... 100 do 200 i=1,10 ... goto 100 ...  200 continue ...
        boolean outThenBackIn = apparentlyCycling
                && TapList.contains(origLoops, destLoops.head)
                && !reallyInACycle;
        if (reallyInACycle && TapList.contains(origLoops, destLoops.head)) {
            //this FGArrow terminates with a cycle inside deepest loop enclosing destination:
            loop = destLoops.head;
            iter = new TapList<>(new TapPair<>(FGConstants.CYCLE, loop), null);
            loop.cycleArrows = new TapList<>(this, loop.cycleArrows);
        } else {
            //this FGArrow terminates with a series of jumps into loops enclosing destination:
            iter = null;
        }
        while (destLoops != null
                && (!TapList.contains(origLoops, destLoops.head)
                || outThenBackIn)) {
            loop = destLoops.head;
            iter = new TapList<>(new TapPair<>(FGConstants.PUSH, loop), iter);
            if (!TapList.contains(loop.entryBlocks, destination)) {
                loop.entryBlocks =
                        new TapList<>(destination, loop.entryBlocks);
            }
            if (destination != loop.header()) {
                // If this is an alternate entry into the current loop:
                Tree pos = null;
                if (destination.instructions != null) {
                    pos = destination.headInstr().tree;
                }
                TapEnv.fileWarning(TapEnv.MSG_WARN, pos, "(CF01) Irreductible entry into loop");
            }
            loop.entryArrows = new TapList<>(this, loop.entryArrows);
            outThenBackIn = false;
            destLoops = destLoops.tail;
        }
        // Collect the LoopBlock's POPped from, an put them into
        // tmpList to get them in reverse order. Then add them into "iter".
        while (origLoops != null
                && (destLoops == null || origLoops.head != destLoops.head)) {
            tmpList = new TapList<>(origLoops.head, tmpList);
            origLoops = origLoops.tail;
        }
        while (tmpList != null) {
            loop = tmpList.head;
            iter = new TapList<>(new TapPair<>(FGConstants.POP, loop), iter);
            if (!TapList.contains(loop.exitBlocks, origin)) {
                loop.exitBlocks = new TapList<>(origin, loop.exitBlocks);
            }
            loop.exitArrows = new TapList<>(this, loop.exitArrows);
            tmpList = tmpList.tail;
        }
    }

    /**
     * @return the LoopBlock of the topmost loop level that "this" FGArrow pops from.
     * If this arrow does not POP, returns the plain origin Block of this arrow.
     */
    public Block topmostPop() {
        TapList<TapPair<Integer, LoopBlock>> inIter = iter;
        LoopBlock topmostPop = null;
        while (inIter != null && inIter.head.first.equals(FGConstants.POP)) {
            topmostPop = inIter.head.second;
            inIter = inIter.tail;
        }
        if (topmostPop != null) {
            return topmostPop;
        } else {
            return origin;
        }
    }

    /**
     * @return the LoopBlock of the loop level in which "this" FGArrow finally cycles.
     */
    public LoopBlock finalCycle() {
        TapList<TapPair<Integer, LoopBlock>> inIter = iter;
        if (inIter != null) {
            while (inIter.tail != null) {
                inIter = inIter.tail;
            }
        }
        if (inIter != null && inIter.head.first.equals(FGConstants.CYCLE)) {
            return inIter.head.second;
        } else {
            return null;
        }
    }

    public boolean goesInsideControl() {
        switch (test) {
            case FGConstants.LOOP:
                return TapIntList.contains(cases, FGConstants.NEXT);
            case FGConstants.IF:
            case FGConstants.ENTRY:
                return true;
            case FGConstants.CASE:
                return !TapIntList.contains(cases, FGConstants.NOMATCH);
            default:
                if (origin.isANameSpace()) {
                    SymbolTable nameSpaceBasisST = destination.symbolTable.basisSymbolTable();
                    // nameSpace "std" can be further defined in a unit, and is based on C++ root context ?
                    return nameSpaceBasisST == origin.symbolTable || nameSpaceBasisST == origin.symbolTable.getCallGraph().cPlusPlusRootSymbolTable();
                } else if (origin.isParallelController()) {
                    return destination.parallelControls != null && origin.lastInstr() == destination.parallelControls.head;
                } else {
                    return false;
                }
        }
    }

    public SymbolTable commonSymbolTable() {
        if (origin == null || origin.symbolTable == null ||
                destination == null || destination.symbolTable == null) {
            return null;
        }
        return origin.symbolTable.getCommonRoot(destination.symbolTable);
    }

    /**
     * used in treeGen.
     */
    public boolean mayBeNatural() {
        switch (test) {
            case FGConstants.ASS_GOTO:
                return false;
            case FGConstants.COMP_GOTO:
            case FGConstants.IO_GOTO:
            case FGConstants.CALL:
                return cases.head == FGConstants.DEFAULT;
            default:
                return true;
        }
    }

    /**
     * Prints in detail the contents of this FGArrow, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        if (inACycle) {
            TapEnv.print("in cycle arrow ");
        }
        if (origin == null) {
            TapEnv.print("null origin!");
        } else {
            TapEnv.print(origin.toString());
        }
        TapEnv.print(" ");
        dumpTestCaseCarry();
        TapEnv.print(" ");
        if (destination == null) {
            TapEnv.print("null destination!");
        } else {
            TapEnv.print(destination.toString());
        }
    }

    protected void dumpTestCaseCarry() throws java.io.IOException {
        TapEnv.print("==(" + rank + ")==");
        switch (test) {
            case FGConstants.LOOP:
                TapEnv.print("LOOP(");
                break;
            case FGConstants.IF:
                TapEnv.print("IF(");
                break;
            case FGConstants.ENTRY:
                TapEnv.print("ENTRY(");
                break;
            case FGConstants.COMP_GOTO:
                TapEnv.print("COMP_GOTO(");
                break;
            case FGConstants.ASS_GOTO:
                TapEnv.print("ASS_GOTO(");
                break;
            case FGConstants.IO_GOTO:
                TapEnv.print("IO_GOTO(");
                break;
            case FGConstants.CALL:
                TapEnv.print("CALL(");
                break;
            case FGConstants.CASE:
                TapEnv.print("CASE(");
                break;
            case FGConstants.NO_TEST:
                TapEnv.print("=====");
                break;
            default:
                TapEnv.print("??(");
                break;
        }
        dumpCase();
        if (test != FGConstants.NO_TEST) {
            TapEnv.print(")==");
        }
        if (carry != null) {
            TapEnv.print("&>");
        } else {
            TapEnv.print(">");
        }
    }

    private void dumpCase() throws java.io.IOException {
        TapIntList inCases = cases;
        int theCase;
        while (inCases != null) {
            theCase = inCases.head;
            switch (test) {
                case FGConstants.LOOP:
                    if (theCase == FGConstants.NEXT) {
                        TapEnv.print("NEXT");
                    } else if (theCase == FGConstants.EXIT) {
                        TapEnv.print("EXIT");
                    } else {
                        TapEnv.print("??");
                    }
                    break;
                case FGConstants.IF:
                    if (theCase == FGConstants.TRUE) {
                        TapEnv.print("TRUE");
                    } else if (theCase == FGConstants.FALSE) {
                        TapEnv.print("FALSE");
                    } else if (theCase == FGConstants.ELSEWHERE) {
                        TapEnv.print("ELSEWHERE");
                    } else {
                        TapEnv.print("??");
                    }
                    break;
                case FGConstants.CASE:
                    if (theCase == FGConstants.DEFAULT) {
                        TapEnv.print("DEFAULT");
                    } else {
                        TapEnv.print(String.valueOf(theCase));
                    }
                    break;
                case FGConstants.ENTRY:
                    if (theCase == FGConstants.MAIN) {
                        TapEnv.print("MAIN");
                    } else {
                        TapEnv.print(String.valueOf(theCase));
                    }
                    break;
                case FGConstants.COMP_GOTO:
                case FGConstants.ASS_GOTO:
                case FGConstants.IO_GOTO:
                case FGConstants.CALL:
                    TapEnv.print(String.valueOf(theCase));
                    break;
                default:
                    TapEnv.print("??");
                    break;
            }
            inCases = inCases.tail;
            if (inCases != null) {
                TapEnv.print(",");
            }
        }
    }

    @Override
    public String toString() {
        return "FGA"
//            +"#"+rank
                + "("
                + (origin == null ? "()" : origin)
                + "=="
                + testAndCasesToString(test, cases)
                + (inACycle ? "*" : "=")
                + "=>"
                + (destination == null ? "()" : destination)
                + ')'
//            +(inACycle?"(cycle)":"")
//            +(carry!=null?"*":"")
                ;
    }
}
