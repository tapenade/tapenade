/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * A primitive type.
 */
public final class PrimitiveTypeSpec extends TypeSpec {
    /**
     * The name of this primitive type.
     */
    private String name;

    /**
     * The name of this primitive type.
     */
    public String name() {
        return name;
    }

    /**
     * The size in bytes of this primitive type.
     */
    private int primitiveTypeSize = 1;

    /**
     * The size in bytes of this primitive type.
     */
    protected void setPrimitiveTypeSize(int primitiveTypeSize) {
        this.primitiveTypeSize = primitiveTypeSize;
    }

    /**
     * True when this type is numeric, but we still don't know exactly which one.
     */
    private boolean undefinedNumeric;

    /**
     * Creates a new primitive type with the given "name".
     */
    public PrimitiveTypeSpec(String name) {
        super(SymbolTableConstants.PRIMITIVETYPE);
        if ("real".equals(name)) {
            name = "float"; // Backward compatibility with past versions.
        }
        this.name = name;
    }

    /**
     * @return a Tree that is a good enough zero in the given environment.
     */
    public static Tree buildRealConstantZero() {
        int lang = TapEnv.relatedLanguage();
        if (lang == TapEnv.C || TapEnv.get().realSize == TapEnv.REAL_SIZE) {
            return ILUtils.build(ILLang.op_realCst, "0.0");
        } else if (lang == TapEnv.FORTRAN && TapEnv.get().realSize == 8) {
            return ILUtils.build(ILLang.op_realCst, "0.D0");
        } else {
            return ILUtils.build(ILLang.op_realCst, "0.0_" + TapEnv.get().realSize);
        }
    }

    @Override
    protected void setUndefinedNumeric(boolean value) {
        undefinedNumeric = value;
    }

    @Override
    protected boolean isUndefinedNumeric() {
        return undefinedNumeric;
    }

    /**
     * @return True if this type is "integer".
     */
    public boolean isInteger() {
        return !isUndefinedNumeric()
                && (name.equals("integer")
                ||
                (TapEnv.relatedLanguageIsC() || TapEnv.relatedLanguageIsCPLUSPLUS())
                        && name.equals("boolean"));
    }

    /**
     * @return True if this type is "float".
     */
    public boolean isReal() {
        return !isUndefinedNumeric() && name.equals("float");
    }

    /**
     * @return True if this type is either "float" or "complex".
     */
    public boolean isRealOrComplex() {
        return !isUndefinedNumeric() && (name.equals("float") || name.equals("complex"));
    }

    /**
     * @return True if this type, possibly yet undefined, can/will be either "float" or "complex".
     */
    public boolean isProbablyRealOrComplex() {
        return name.equals("float") || name.equals("complex");
    }

    /**
     * @return True if this type is "complex".
     */
    public boolean isComplex() {
        return !isUndefinedNumeric() && name.equals("complex");
    }

    /**
     * @return True if this type is the augmented type for overloading-based AD.
     */
    public boolean isAugmentedDouble() {
        return !isUndefinedNumeric() && name.equals("adouble");
    }

    /**
     * Composition rules for PrimitiveTypeSpec:
     * <pre>
     * We start from the following definitions:
     * {@code
     * ?INTEGER == INTEGER or REAL or COMPLEX
     * ?REAL    == REAL or COMPLEX
     * ?COMPLEX == COMPLEX (therefore ?COMPLEX is useless)
     * Composition rules are symmetric,
     * but for implementation, we make them assymetric
     * to keep/return the left type ("this") whenever possible.
     * ([llh] Not sure this is a good choice. Maybe the result
     * should share with neither left nor right type?)
     *
     * [code 0] is for other primitive types (Strings, Fortran Logicals, etc...)
     * [code 1]  INTEGER addWith  INTEGER =>  INTEGER
     * [code 1]  INTEGER addWith ?INTEGER => ?INTEGER
     * [code 1]  INTEGER addWith  REAL    =>  REAL
     * [code 1]  INTEGER addWith ?REAL    => ?REAL
     * [code 1]  INTEGER addWith  COMPLEX =>  COMPLEX
     * [code 2] ?INTEGER addWith ?INTEGER => ?INTEGER
     * [code 2] ?INTEGER addWith  REAL    => ?REAL
     * [code 2] ?INTEGER addWith ?REAL    => ?REAL
     * [code 2] ?INTEGER addWith  COMPLEX =>  COMPLEX
     * [code 3]  REAL    addWith  REAL    =>  REAL
     * [code 3]  REAL    addWith ?REAL    => ?REAL
     * [code 3]  REAL    addWith  COMPLEX =>  COMPLEX
     * [code 4] ?REAL    addWith ?REAL    => ?REAL
     * [code 4] ?REAL    addWith  COMPLEX =>  COMPLEX
     * [code 5]  COMPLEX addWith  COMPLEX =>  COMPLEX}
     *  </pre>
     */
    protected TypeSpec addWith(WrapperTypeSpec type) {
        TypeSpec result = this;
        if (type == null || type.wrappedType == null) {
            result = this;
        } else if (isA(type, SymbolTableConstants.ARRAYTYPE)) {
            WrapperTypeSpec elemType = type.wrappedType.elementType();
            TypeSpec resultElem = this.addWith(elemType);
            result = new ArrayTypeSpec(new WrapperTypeSpec(resultElem), ((ArrayTypeSpec) type.wrappedType).dimensions());
        } else if (isA(type, SymbolTableConstants.POINTERTYPE)) {
            WrapperTypeSpec elemType = ((PointerTypeSpec) type.wrappedType).destinationType;
            TypeSpec resultElem = this.addWith(elemType);
            result = new PointerTypeSpec(new WrapperTypeSpec(resultElem), ((PointerTypeSpec) type.wrappedType).offsetLength);
        } else if (isA(type, SymbolTableConstants.MODIFIEDTYPE)) {
            ModifiedTypeSpec modifiedType = (ModifiedTypeSpec) type.wrappedType;
            TypeSpec resultElem = this.addWith(modifiedType.elementType());
            result = new ModifiedTypeSpec(new WrapperTypeSpec(resultElem), modifiedType);
        } else if (isA(type, SymbolTableConstants.PRIMITIVETYPE)) {
            PrimitiveTypeSpec primType = (PrimitiveTypeSpec) type.wrappedType;
            int leftCode = 0;
            if (name.equals("complex")) {
                leftCode = 5;
            } else if (name.equals("float")) {
                leftCode = undefinedNumeric ? 4 : 3;
            } else if (name.equals("integer")
                    || TapEnv.relatedLanguageIsC() && name.equals("boolean")) {
                leftCode = undefinedNumeric ? 2 : 1;
            }
            int rightCode = 0;
            if (primType.name.equals("complex")) {
                rightCode = 5;
            } else if (primType.name.equals("float")) {
                rightCode = undefinedNumeric ? 4 : 3;
            } else if (primType.name.equals("integer")
                    || TapEnv.relatedLanguageIsC() && primType.name.equals("boolean")) {
                rightCode = undefinedNumeric ? 2 : 1;
            }
            if (leftCode == 3 && rightCode == 2) {
                result = new PrimitiveTypeSpec("float");
                ((PrimitiveTypeSpec) result).primitiveTypeSize = TapEnv.get().realSize;
                ((PrimitiveTypeSpec) result).undefinedNumeric = true;
            } else if (leftCode > 0 && rightCode > leftCode) {
                result = primType.copy();
            } else {
                // Fallback solution: return "this" whenever possible.
                result = this;
            }
        }
        return result;
    }

    @Override
    protected TypeSpec cloneAsUndefinedNumeric(boolean undefined) {
        PrimitiveTypeSpec result = new PrimitiveTypeSpec(name);
        result.undefinedNumeric = undefined;
        return result;
    }

    @Override
    protected String baseTypeName() {
        return name;
    }

    /**
     * double:8 float:4 long:8 int:4 short:2 byte:1 char:2 boolean:4 reference:4.
     */
    @Override
    protected int computeSize() {
        switch (name) {
            case "integer":
                return TapEnv.get().integerSize;
            case "float":
                return TapEnv.get().realSize;
            case "complex":
                return 2 * TapEnv.get().realSize;
            case "boolean":
                return TapEnv.get().integerSize;
            case "character":
                return TapEnv.get().charSize;
            default:
                return TapEnv.get().byteSize;
        }
    }

    @Override
    public boolean isCharacter() {
        return "character".equals(name);
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's:
        toOther = peelWrapperTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // If this is a "character", then we accept other being a "character" or a String.
        if ("character".equals(name)) {
            if (testTypesLitteral(comparison)) {
                return other instanceof PrimitiveTypeSpec && "character".equals(((PrimitiveTypeSpec) other).name);
            } else {
                if (other.isCharacter() || other.isString()) {
                    return true;
                }
            }
        }
        // peel size modifiers around other:
        Tree otherSizeModifierResolved = null;
        ToObject<ModifiedTypeSpec> toOtherModifiedTypeSpec = new ToObject<>(null);
        // If we want to compare "literally",
        // the other type can NOT be a ModifiedTypeSpec, even with empty modifiers, nor a MetaTypeSpec:
        if (!testTypesLitteral(comparison)) {
            ToObject<Tree> toOtherSizeModifierResolved = new ToObject<>(null);
            toOther = peelSizeModifiersTo(other, toOther, toOtherSizeModifierResolved, toOtherModifiedTypeSpec, false);
            if (toOther != null) {
                other = toOther.wrappedType();
            }
            otherSizeModifierResolved = toOtherSizeModifierResolved.obj();
            // if other is a MetaTypeSpec, turn it into a standard TypeSpec (hopefully a PrimitiveTypeSpec)
            if (other instanceof MetaTypeSpec) {
                other = ((MetaTypeSpec) other).makeLocalizedTypeSpec();
            }
        }
        // Special for C: an enum can be seen as an integer, if this is integer, it can compare with an enum:
        if (TapEnv.relatedLanguageIsC() &&
                isA(other, SymbolTableConstants.ENUMTYPE) && isInteger()) {
            return true;
        }
        // now other must be of matching kind:
        if (!(other instanceof PrimitiveTypeSpec)) {
            return false;
        }
        String otherName = ((PrimitiveTypeSpec) other).name;
        String thisName = name;
        // in C, Booleans are in fact integers:
        if (TapEnv.relatedLanguageIsC()) {
            if ("boolean".equals(otherName)) {
                otherName = "integer";
            }
            if ("boolean".equals(thisName)) {
                thisName = "integer";
            }
        }
        boolean comparesWell = true;
        if (testIsReceivesWithInference(comparison) && isUndefinedNumeric()) {
            // If we are looking at a RECEIVE and this receiving type is an undefinedNumeric, and we play type inference
            // then enlarge this receiving type to accomodate for the larger received type:
            if ("integer".equals(thisName) && "float".equals(otherName)) {
                this.name = "float";
            } else if ("complex".equals(otherName)) {
                this.name = "complex" ;
            }
            // leave comparesWell as true!
        } else if (testIsReceives(comparison)) {
            comparesWell = thisName == null || otherName != null &&
                    (thisName.equals(otherName)
                     || (isRealOrComplex() && otherName.equals("integer"))
                     || ("complex".equals(this.name) && "float".equals(otherName)));
        } else {
            comparesWell = thisName != null && thisName.equals(otherName);
        }

        // compare size modifiers:
        if (comparesWell) {
            boolean testTypesCompilDep = testTypesCompilDep(comparison);
            int unmodifiedSize = testTypesCompilDep ? size() : 0;
            int otherSizeModifierInteger = -1;
            if (otherSizeModifierResolved == null) {
                otherSizeModifierInteger = testTypesCompilDep ? unmodifiedSize : 0; //Convention for "no modifier at all"
            } else if (otherSizeModifierResolved.opCode() == ILLang.op_intCst) {
                otherSizeModifierInteger = otherSizeModifierResolved.intValue();
                if (testTypesCompilDep && otherSizeModifierInteger <= -2) {
                    otherSizeModifierInteger = otherSizeModifierInteger == -2 ? 2 * unmodifiedSize :
                            otherSizeModifierInteger == -4 ? 4 * unmodifiedSize : unmodifiedSize;
                }
            }
            int thisSizeModifierInteger = testTypesCompilDep ? unmodifiedSize : 0; //i.e. no modifier!
            // If we are looking at a RECEIVE and the receiving type (left type) is an undefinedNumeric,
            // and we play type inference then enlarge this left type to accomodate for the larger right type:
            if (testIsReceivesWithInference(comparison) && isUndefinedNumeric()) {
                if (ModifiedTypeSpec.sizeModifierIntegerSmaller(thisSizeModifierInteger, otherSizeModifierInteger)) {
                    ModifiedTypeSpec otherModifiedTypeSpec = toOtherModifiedTypeSpec.obj();
                    if (otherModifiedTypeSpec != null && toThis != null) {
                        toThis.setWrappedType(new ModifiedTypeSpec(new WrapperTypeSpec(this), otherModifiedTypeSpec));
                    }
                }
                // leave comparesWell as true!
            } else {
                comparesWell = ModifiedTypeSpec.compareModifiersWith(thisSizeModifierInteger, null,
                        otherSizeModifierInteger, otherSizeModifierResolved,
                        comparison);
            }
        }
        return comparesWell;
    }

    @Override
    protected TypeSpec weakenForInference(int comparison) {
        PrimitiveTypeSpec result = new PrimitiveTypeSpec(name);
        result.undefinedNumeric = true;
        return result;
    }

    /**
     * @return a copy of this type.
     */
    @Override
    public TypeSpec copy() {
        PrimitiveTypeSpec result = new PrimitiveTypeSpec(name);
        result.primitiveTypeSize = primitiveTypeSize;
        result.undefinedNumeric = undefinedNumeric;
        return result;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public int precisionSize() {
        if ("float".equals(name)) {
            return TapEnv.get().realSize;
        } else if ("complex".equals(name)) {
            return 2 * TapEnv.get().realSize;
        } else {
            return -1;
        }
    }

    @Override
    public Tree buildConstantZero() {
        switch (name) {
            case "integer":
                return ILUtils.build(ILLang.op_realCst, "0");
            case "float":
                return buildRealConstantZero();
            case "complex":
                return ILUtils.build(ILLang.op_complexConstructor,
                        buildRealConstantZero(),
                        buildRealConstantZero());
            default:
                return null;
        }
    }

    @Override
    public Tree buildConstantOne() {
        switch (name) {
            case "integer":
                return ILUtils.build(ILLang.op_realCst, "1");
            case "float":
                return ILUtils.build(ILLang.op_realCst, "1.0");
            case "complex":
                return ILUtils.build(ILLang.op_realCst, "1.0");
            default:
                return null;
        }
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        PrimitiveTypeSpec copiedResult = new PrimitiveTypeSpec(name);
        copiedResult.primitiveTypeSize = primitiveTypeSize;
        copiedResult.undefinedNumeric = undefinedNumeric;
        copiedResult.setOrAddTypeDeclName(typeDeclName());
        return copiedResult;
    }

    @Override
    public TypeSpec intToReal(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                              SymbolTable symbolTable) {
        if (name.equals("float") || name.equals("complex")) {
            return this;
        } else {
            return symbolTable.getTypeDecl("float").typeSpec.wrappedType;
        }
    }

    @Override
    protected TypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        return complexTypeSpec.wrappedType;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        if (TapEnv.get().complexStep && TapEnv.relatedLanguageIsFortran()) {
            if (name.equals("float")) {
                return symbolTable.getTypeDecl("complex").typeSpec ;
            } else if (name.equals("complex")) {
// [llh 30Mar22] TODO: because types are modified in-place, the following triggers wrongly on set12/cmplxstep02:
//                 TapEnv.fileError(null, "(ADxx) Source code uses COMPLEX: cannot apply the Complex-Step method") ;
                return null ;
            } else {
                return null ;
            }
        } else if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
            FieldDecl[] fields = new FieldDecl[2];
            fields[0] = new FieldDecl(TapEnv.assocAddressValueSuffix(), new WrapperTypeSpec(this));
            fields[1] = new FieldDecl(TapEnv.assocAddressDiffSuffix(), new WrapperTypeSpec(this));
            Tree modifiers = null;
            if (!TapEnv.relatedLanguageIsC()) {
                modifiers =
                        ILUtils.build(ILLang.op_modifiers, ILUtils.build(ILLang.op_ident, "sequence"));
            }
            diffTypeSpec = new WrapperTypeSpec(new CompositeTypeSpec(null, fields, SymbolTableConstants.RECORDTYPE, modifiers, null));
            this.createOrGetDiffTypeDeclSymbolHolder(
                    symbolTable,
                    diffTypeSpec, fSuffix);
            return diffTypeSpec;
        } else {
            if (multiDirMode && (name.equals("float") || name.equals("complex"))) {
                ArrayDim[] dimensions = new ArrayDim[1];
                dimensions[0] = multiDirDimensionMax;
                return new WrapperTypeSpec(new ArrayTypeSpec(new WrapperTypeSpec(this), dimensions));
            } else {
                return null;
            }
        }
    }

    @Override
    public boolean acceptsMultiDirDimension() {
        return true ;
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return TapEnv.associationByAddress() && ("float".equals(name) || "complex".equals(name)) ;
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return "float".equals(name) || "complex".equals(name);
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree result;
        if ("integer".equals(name)) {
            result = ILUtils.build(ILLang.op_integer);
        } else if ("float".equals(name)) {
            result = ILUtils.build(ILLang.op_float);
        } else if ("complex".equals(name)) {
            result = ILUtils.build(ILLang.op_complex);
        } else if ("boolean".equals(name)) {
            result = ILUtils.build(ILLang.op_boolean);
        } else if ("character".equals(name)) {
            result = ILUtils.build(ILLang.op_character);
        } else {
            result = ILUtils.build(ILLang.op_ident, name);
        }
        return result;
    }

    @Override
    public String showType() {
        int language = TapEnv.relatedUnit() != null ? TapEnv.relatedUnit().language() : -1;
        if ("integer".equals(name)) {
            return TapEnv.isFortran(language) ? "INTEGER" : "int";
        } else if ("float".equals(name)) {
            return TapEnv.isFortran(language) ? "REAL" : "float";
        } else if ("complex".equals(name)) {
            return TapEnv.isFortran(language) ? "COMPLEX" : "complex";
        } else if ("boolean".equals(name)) {
            return TapEnv.isFortran(language) ? "LOGICAL" : "_Bool";
        } else if ("character".equals(name)) {
            return TapEnv.isFortran(language) ? "CHARACTER" : "char";
        } else {
            return name;
        }
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + name.toUpperCase() //+"@"+Integer.toHexString(hashCode())
                + (undefinedNumeric ? "?" : "");
    }
}
