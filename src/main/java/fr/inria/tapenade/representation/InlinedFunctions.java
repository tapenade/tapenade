/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.TopDownTreeWalk;
import fr.inria.tapenade.utils.Tree;

/**
 * Inline all functions declared in Tapenade libraries.
 */
public final class InlinedFunctions {

    public transient TapList<Unit> libraryUnits = null ;
    /**
     * liste des functions a inliner dans le programme
     */
    private transient TapList<InFunction> foundFunctions;
    private boolean reRunSplit;
    private Tree valueOfInline;

    public InlinedFunctions() {
    }

    public void addLibraryUnit(Unit unit) {
        libraryUnits = new TapList<>(unit, libraryUnits) ;
    }

    /** Main routine that invokes inlining of all inlineable calls in "unit".
     * la fonction a appeler dans CallGraph
     * retourne la liste des Unit qui ont ete inlinees.
     */
    public void findInlinedFunctions(Unit unit) {
        if (TapEnv.traceInlineAnalysis()) {
            TapEnv.printlnOnTrace("==== INLINE CALLS IN UNIT "+unit.name()+" ====") ;
        }
        reRunSplit = true;
        while (reRunSplit) {
            findInlinedFunctionsRun(unit);
        }
    }

    private void findInlinedFunctionsRun(Unit unit) {
        foundFunctions = null;
        reRunSplit = false;
        Block currentBlock;
        TapList<Block> blocks = unit.allBlocks();
        while (blocks != null) {
            currentBlock = blocks.head;
            foundFunctions = findInlinedFunctions(currentBlock);
            if (foundFunctions != null && currentBlock.isALoop() &&
                    currentBlock.instructions.head.tree.down(3).operator().code == ILLang.op_while) {
                extractInlineFromTest(currentBlock);
                reRunSplit = true;
                foundFunctions = null;
            }
            if (foundFunctions != null) {
                splitBlock(currentBlock, unit.symbolTablesBottomUp(),
                        currentBlock.symbolTable, unit);
            }
            blocks = blocks.tail;
        }
        // pour renumeroter les nouveaux blocks
        // pb: fait des verifications en trop?
        unit.coherence();
    }

    /**
     * retourne une TapList contenant les InFunction trouvees.
     */
    private TapList<InFunction> findInlinedFunctions(Block block) {
        int rank = 0;
        TapList<InFunction> foundRank = null;
        TapList<Instruction> instructions = block.instructions;
        Instruction instruction;
        SymbolTable symbolTable = block.symbolTable;
        while (instructions != null) {
            instruction = instructions.head;
            InFunction found = null;
            if (instruction.tree != null
                && !(block.unit().isFortran() && instruction.isADeclaration())
                && instruction.tree.opCode() != ILLang.op_data) {
                found = findInlinedFunctions(instruction, symbolTable);
            }
            if (found != null) {
                found.callingInstruction = instruction;
                found.rank = rank;
                foundRank = TapList.append(foundRank, new TapList<>(found, null));
            }
            instructions = instructions.tail;
            rank = rank + 1;
        }
        return foundRank;
    }

    /**
     * on remplace while(test) par while(true)
     * on rajoute un premier block if(test) -&gt; fleche en dehors du while.
     */
    private void extractInlineFromTest(Block block) {
        Tree whileTree = block.instructions.head.tree.down(3);
        Tree testTree = ILUtils.copy(whileTree.down(1));
        Tree ifTree;
        String trueString = "true";
        if (block.unit().isFortran()) {
            trueString = ".true.";
        } else if (block.unit().isC()) {
            trueString = "1";
        }
        whileTree.setChild(ILUtils.build(ILLang.op_ident, trueString), 1);
        Block newTestBlock = new BasicBlock(block.symbolTable, block.parallelControls, null);
        ifTree = ILUtils.build(ILLang.op_if);
        ifTree.setChild(testTree, 1);
        newTestBlock.instructions = new TapList<>(new Instruction(ifTree, null), null);
        newTestBlock.setOrigLabel(block.origLabel());
        FGArrow nextArrow = block.getFGArrowTestCase(FGConstants.LOOP, FGConstants.NEXT);
        newTestBlock.setEnclosingLoop(block.enclosingLoop());
        nextArrow.insertBlockAtDest(newTestBlock, FGConstants.IF, FGConstants.TRUE);
        Block exitLoopBlock;
        FGArrow exitArrow = block.getFGArrowTestCase(FGConstants.LOOP, FGConstants.EXIT);
        exitLoopBlock = exitArrow.destination;
        new FGArrow(newTestBlock, FGConstants.IF, FGConstants.FALSE, exitLoopBlock);
        block.enclosingLoop().inside.tail = new TapList<>(newTestBlock,
                block.enclosingLoop().inside.tail);
    }

    /**
     * retourne la liste ordonnee de blocks eclates.
     */
    private void splitBlock(Block block,
                            TapList<SymbolTable> sortedSymbolTables, SymbolTable localST, Unit unit) {
        TapList<Block> resultBlocks = null;
        TapList<Block>[] newinBlocks;
        TapIntList instructionRank = null;
        TapList<InFunction> inFunc = foundFunctions;
        int r;
        int oldrank = -1;
        while (inFunc != null) {
            r = inFunc.head.rank;
            if (r != oldrank) {
                instructionRank =
                        TapIntList.append(instructionRank, new TapIntList(r, null));
                oldrank = r;
            } else {
                reRunSplit = true;
            }
            inFunc = inFunc.tail;
        }
        Block newBlock1;
        TapList<Instruction>[] listInstructions =
                TapList.split(block.instructions, instructionRank);
        Block[] newBlocks = new Block[listInstructions.length];
        Block firstNewBlock = null ;
        TapList<FGArrow> flow;
        for (int i = 0; i < listInstructions.length; i++) {
            if (listInstructions[i] != null) {
                newBlock1 = new BasicBlock(block.symbolTable, block.parallelControls, null);
                if (firstNewBlock==null) {
                    firstNewBlock = newBlock1 ;
                    // If block was the declarationBlock of its SymbolTable, the newFirstBlock must now play this role:
                    if (block == block.symbolTable.declarationsBlock) {
                        block.symbolTable.declarationsBlock = firstNewBlock ;
                    }
                }
                block.copyIntoWithoutArrows(newBlock1, new TapList<>(null, null));
                newBlock1.setOrigLabel(null);
                // pour, au debug, voir les rangs des blocks, sinon inutile
                newBlock1.rank = block.rank + i + 100;
                newBlock1.instructions = listInstructions[i];
                newBlocks[i] = newBlock1;
            }
        }
        for (int i = 0; i < listInstructions.length; i++) {
            // on relie les nouveaux blocks en sequence
            Block prev;
            Block next;
            if (0 < i) {
                if (newBlocks[i - 1] != null) {
                    prev = newBlocks[i - 1];
                } else if (1 < i) {
                    prev = newBlocks[i - 2];
                } else {
                    prev = null;
                }
                if (newBlocks[i] != null) {
                    next = newBlocks[i];
                } else {
                    next = null;
                }
                if (prev != null && next != null) {
                    new FGArrow(prev, FGConstants.NO_TEST, 0, next, false);
                }
            }
        }
        newinBlocks =
                inlineBlocks(newBlocks, sortedSymbolTables, localST);
        for (int i = 0; 2 * i + 1 < newBlocks.length; i++) {
            if (newBlocks[2 * i] != null) {
                resultBlocks =
                        TapList.append(
                                resultBlocks, new TapList<>(newBlocks[2 * i], null));
            }
            resultBlocks = TapList.append(resultBlocks, newinBlocks[i]);
        }
        if (newBlocks.length - newBlocks.length / 2 * 2 == 1) {
            if (newBlocks[newBlocks.length - 1] != null) {
                resultBlocks =
                        TapList.append(
                                resultBlocks,
                                new TapList<>(newBlocks[newBlocks.length - 1], null));
            }
        }
        Block newFirstBlock = null;
        Block newHeaderBlock = null;
        TapList<Block> newBlocksAfterHeader = null;
        Block newLastBlock;
        TapList<Block> toList;
        TapList<Block> tlList;
        Block curBlock = null;
        if (block instanceof HeaderBlock) {
            if (block.isALoop()) {
                /*le dernier block de resultBlocks doit devenir un Header */
                toList = new TapList<>(null, null);
                tlList = toList;
                while (resultBlocks != null) {
                    curBlock = resultBlocks.head;
                    if (resultBlocks.tail == null) {
                        newHeaderBlock =
                                new HeaderBlock(curBlock.symbolTable, curBlock.parallelControls, null);
                        newHeaderBlock.replaceInFlowGraph(curBlock);
                        curBlock = newHeaderBlock;
                    } else {
                        tlList = tlList.placdl(curBlock);
                    }
                    if (newFirstBlock == null) {
                        newFirstBlock = curBlock;
                    }
                    resultBlocks = resultBlocks.tail;
                }
                tlList.placdl(block.enclosingLoop());
                newLastBlock = curBlock;
                unit.updateNestsOfBlocks(block.enclosingLoop(), toList.tail);
            } else {
                /*le premier block de resultBlocks doit devenir un Header */
                assert resultBlocks != null;
                curBlock = resultBlocks.head;
                newFirstBlock = new HeaderBlock(curBlock.symbolTable, curBlock.parallelControls, null);
                newFirstBlock.replaceInFlowGraph(curBlock);
                newHeaderBlock = newFirstBlock;
                newHeaderBlock.setOrigLabel(block.origLabel());
                newBlocksAfterHeader = resultBlocks.tail;
                if (newBlocksAfterHeader != null) {
                    newLastBlock = TapList.last(newBlocksAfterHeader);
                } else {
                    newLastBlock = newHeaderBlock;
                }
            }
            newBlocksAfterHeader =
                    new TapList<>(newHeaderBlock, newBlocksAfterHeader);
        } else {
            assert resultBlocks != null;
            newFirstBlock = resultBlocks.head;
            newFirstBlock.setOrigLabel(block.origLabel());
            newBlocksAfterHeader = resultBlocks;
            newLastBlock = TapList.last(resultBlocks);
        }
        flow = block.backFlow();
        FGArrow arrow;
        while (flow != null) {
            arrow = flow.head;
            if (!arrow.inACycle) {
                arrow.redirectDestination(newFirstBlock);
            } else {
                arrow.redirectDestination(newHeaderBlock);
            }
            flow = flow.tail;
        }
        flow = block.flow();
        while (flow != null) {
            flow.head.redirectOrigin(newLastBlock);
            flow = flow.tail;
        }
        unit.updateNestsOfBlocks(block, newBlocksAfterHeader);
    }

    /**
     * Retourne une InFunction et positionne reRunSplit s'il y en a d'autres a inliner
     */
    private InFunction findInlinedFunctions(Instruction instruction, SymbolTable symbolTable) {
        InFunction found = null;
        InFunction inF;
        Tree currentTree;
        Tree resultTree = null;
        for (TopDownTreeWalk downTreeWalk = new TopDownTreeWalk(instruction.tree)
             ; !downTreeWalk.atEnd()
                ; downTreeWalk.advance()) {
            currentTree = downTreeWalk.get();
            if (currentTree.opCode() == ILLang.op_call
                    && currentTree.parent() != null) {
                String functionName = ILUtils.getCallFunctionNameString(currentTree);
                WrapperTypeSpec actualReturnType =
                        symbolTable.typeOf(currentTree.parent().opCode() == ILLang.op_assign
                                ? currentTree.parent().down(1)
                                : currentTree);
                Tree[] actualArgs = ILUtils.getArguments(currentTree).children();
                int nbActualArgs = actualArgs.length;
                WrapperTypeSpec[] actualArgsTypes = new WrapperTypeSpec[nbActualArgs];
                for (int i = 0; i < nbActualArgs; i++) {
                    actualArgsTypes[i] = (actualArgs[i].opCode() == ILLang.op_none
                            ? new WrapperTypeSpec(null)
                            : symbolTable.typeOf(actualArgs[i]));
                }
                Unit inlinedUnit = findInlinedUnit(functionName, actualArgsTypes, actualReturnType,
                                        currentTree, instruction);
                if (inlinedUnit != null) {
                    if (currentTree.parent().opCode()==ILLang.op_assign) {
                        resultTree = currentTree.parent().down(1);
                    }
                    inF = new InFunction(inlinedUnit, currentTree, instruction, resultTree, actualArgsTypes);
                    if (found == null) {
                        found = inF;
                    } else {
                        reRunSplit = true;
                    }
                }
            }
        }
        return found;
    }

    private TapList<Block>[] inlineBlocks(
            Block[] blocks,
            TapList<SymbolTable> sortedSymbolTables, SymbolTable localST) {
        @SuppressWarnings("unchecked")
        TapList<Block>[] newinBlocks = new TapList[blocks.length / 2 + 1];
        Tree instruction;
        // parcourt les blocks impairs:
        for (int i = 0; 2 * i + 1 < blocks.length; i++) {
            if (blocks[2 * i + 1].instructions != null
                    && blocks[2 * i + 1].instructions.head != null) {
                instruction = blocks[2 * i + 1].instructions.head.tree;
                if (instruction != null) {
                    newinBlocks[i] = inlineBlock(
                            instruction, blocks,
                            foundFunctions.head, 2 * i + 1,
                            sortedSymbolTables,
                            localST);
                }
            }
            foundFunctions = foundFunctions.tail;
        }
        return newinBlocks;
    }

    private Unit findInlinedUnit(String name, WrapperTypeSpec[] actualArgsTypes,
                                 WrapperTypeSpec actualReturnType,
                                 Tree tree, Instruction instruction) {
// Inlining is done to help AD: if not differentiable, we may not want to inline
//  to avoid temporary variables that may be hard to declare.
// Uncommenting this will avoid inlining calls on non-differentiable types:
//         boolean maybeActive =  actualReturnType!=null && TypeSpec.isDifferentiableType(actualReturnType) ;
//         for (int i=actualArgsTypes.length-1 ; maybeActive && i>=0 ; --i) {
//             if (!TypeSpec.isDifferentiableType(actualArgsTypes[i]))
//                 maybeActive = false ;
//         }
//         if (!maybeActive) return null;

        boolean trace = TapEnv.traceInlineAnalysis() ;
        if (trace) {
            TapEnv.printOnTrace("  Look for possible inlining of call "+name+"(") ;
            for (int i=0 ; i<actualArgsTypes.length ; ++i) {
                TapEnv.printOnTrace((i==0 ? "" : ", ")+actualArgsTypes[i]) ;
            }
            TapEnv.printlnOnTrace(") => "+actualReturnType) ;
            TapEnv.printlnOnTrace("    ...from "+tree) ;
            TapEnv.printlnOnTrace("       ...from instruction "+ILUtils.toString(instruction.tree)) ;
        }
        Unit found = null;
        Unit exactFound = null ;
        boolean foundNameInlineable = false;
        for (TapList<Unit> inList=libraryUnits ; inList != null ; inList = inList.tail) {
            Unit libraryUnit = inList.head;
            if (libraryUnit.name().equals(name)) {
                if (libraryUnit.isMadeForInline) foundNameInlineable = true ;
                FunctionTypeSpec funcTypeSpec =
                        (FunctionTypeSpec) libraryUnit.functionTypeSpec().localize(
                                new TapList<>(null, null), new ToBool(false));
                if (trace) {
                    TapEnv.printlnOnTrace("    Proposed: "+libraryUnit.name()+", of type: "+libraryUnit.functionTypeSpec()) ;
                    TapEnv.printlnOnTrace("      localized as: "+funcTypeSpec) ;
                }
                if (funcTypeSpec.matchesCall(actualReturnType, actualArgsTypes)
                    && !FunctionTypeSpec.complexAbsAmbiguous(name, funcTypeSpec.argumentsTypes, actualArgsTypes)
                    && actualReturnType.receivesVectorNeglectSizes(funcTypeSpec.returnType)) {
                    if (trace) {
                        TapEnv.printlnOnTrace("    --> matches!") ;
                    }
                    if (found==null) found = libraryUnit;
                    if (funcTypeSpec.matchesCallExactly(actualReturnType, actualArgsTypes)) {
                        if (trace) {
                            TapEnv.printlnOnTrace("    --> matches exactly!") ;
                        }
                        exactFound = libraryUnit ;
                    }
                } else {
                    if (trace) {
                        TapEnv.printlnOnTrace("    --> does not match") ;
                    }
                }
            }
        }
        if (exactFound!=null) found = exactFound ;
        if (found == null) {
            if (foundNameInlineable) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, tree, "Inline, bad number of arguments or types in call of " + name + " in " + ILUtils.toString(tree));
            }
        } else {
            if (trace) {
                TapEnv.printlnOnTrace("  Final choice:"+found+" inlineable:"+found.isMadeForInline) ;
            }
            if (!found.isMadeForInline) found = null ;
        }
        return found ;
    }

    private TapList<Block> inlineBlock(
            Tree instruction, Block[] blocks,
            InFunction foundinFunction, int rank,
            TapList<SymbolTable> sortedSymbolTables,
            SymbolTable localST) {
        Block currentBlock = blocks[rank];
        TapList<Block> inAllBlocks;
        SymbolTable futureDeclSymbolTable = localST ;
        // don't add declaration in a symbolTable of loop:
        while (futureDeclSymbolTable.declarationsBlock!=null
               && futureDeclSymbolTable.declarationsBlock.isALoop()
               && futureDeclSymbolTable.declarationsBlock instanceof HeaderBlock) {
            futureDeclSymbolTable = futureDeclSymbolTable.basisSymbolTable() ;
        }
        valueOfInline =
            foundinFunction.replaceAllVariables(currentBlock, localST, futureDeclSymbolTable);
        inAllBlocks = foundinFunction.allBlocks();
        inAllBlocks =
                TapList.append(inAllBlocks, new TapList<>(currentBlock, null));
        changeFlow(blocks[rank], inAllBlocks, foundinFunction);
        updateSymbolTableAndLastBlock(
                instruction, foundinFunction, currentBlock, inAllBlocks,
                sortedSymbolTables);
        Instruction firstInstruction = inAllBlocks.head.headInstr();
        Object message = instruction.getAnnotation("message");
        if (message != null) {
            firstInstruction.tree.setAnnotation("message", message);
        }

        return inAllBlocks;
    }

    /**
     * On raccroche backFlow et flow des blocks initiaux et finaux de la liste des nouveaux blocks.
     */
    private void changeFlow(Block block, TapList<Block> blocks, InFunction ffunction) {
        Block currentblock;
        while (blocks != null) {
            currentblock = blocks.head;
            if (!(currentblock instanceof HeaderBlock)) {
                entryArrows(block, currentblock, ffunction);
                exitArrows(block, currentblock, ffunction);
            }
            blocks = blocks.tail;
        }
    }

    private void updateSymbolTableAndLastBlock(
            Tree instTree, InFunction foundinFunction, Block block,
            TapList<Block> blocks, TapList<SymbolTable> sortedSymbolTables) {
        Block currentBlock;
        TapList<TapPair<SymbolTable, SymbolTable>> associationsST = null ;
        associationsST =
                new TapList<>(
                        new TapPair<>(
                                foundinFunction.copiedUnit.publicSymbolTable(),
                                block.symbolTable),
                        associationsST);
        if (block.unit().isFortran() && foundinFunction.copiedUnit.privateSymbolTable()!=null) {
            // sub-scopes are not permitted in Fortran, therefore skip diff of privateSymbolTable.
            associationsST = new TapList<>(
                                     new TapPair<>(
                                             foundinFunction.copiedUnit.privateSymbolTable(),
                                             block.symbolTable),
                                     associationsST) ;
        }
        SymbolTable newSymbolTable;
        while (blocks != null && blocks.tail != null) {
            currentBlock = blocks.head;
            newSymbolTable = currentBlock.symbolTable.smartCopy(associationsST, null, null, null, "Copy of ");
            newSymbolTable.unit = block.unit() ;
            currentBlock.symbolTable.cleanCopySymbolDecls();
            // We force the implicits table to the one of the calling unit,
            // because otherwise the newSymbolTable cannot be peeled off by isUseless()
            System.arraycopy(newSymbolTable.basisSymbolTable().implicits, 0,
                    newSymbolTable.implicits, 0, 26);
            if (newSymbolTable.isUseless() && newSymbolTable!=block.symbolTable) {
                newSymbolTable = newSymbolTable.basisSymbolTable();
            }
            currentBlock.symbolTable = newSymbolTable;
            // Re- typeCheck the inlined code in its new context:
            TapList<Instruction> newInstructions = currentBlock.instructions ;
            while (newInstructions!=null) {
                newSymbolTable.typeCheck(newInstructions.head.tree, false, false, false,
                                         newInstructions.head, new ToBool(false), block.unit()) ;
                newInstructions = newInstructions.tail ;
            }
            if (!TapList.contains(sortedSymbolTables, newSymbolTable)) {
                // Insert newSymbolTable as new first elem of sortedSymbolTables:
                TapList<SymbolTable> newTail = new TapList<>(sortedSymbolTables.head, sortedSymbolTables.tail);
                sortedSymbolTables.head = newSymbolTable;
                sortedSymbolTables.tail = newTail;
            }
            blocks = blocks.tail;
        }
        Tree currentTree;
        Tree assignLeftTree = null;
        boolean done = false;
        String infunctionName = foundinFunction.inlinedUnit.name();
        // on suppose que l'on change le premier appel de la fonction
        for (TopDownTreeWalk downTreeWalk = new TopDownTreeWalk(instTree);
             !downTreeWalk.atEnd();
             downTreeWalk.advance()) {
            currentTree = downTreeWalk.get();
            if (!done && currentTree.opCode() == ILLang.op_call && currentTree.parent() != null
                    && infunctionName.equals(ILUtils.getCalledNameString(currentTree))) {
                if (currentTree.parent().opCode() != ILLang.op_assign) {
                    currentTree.parent().setChild(
                            ILUtils.copy(valueOfInline), currentTree.rankInParent());
                } else {
                    assignLeftTree = currentTree.parent().down(1);
                    block.instructions = block.instructions.tail;
                }
                if (assignLeftTree != null) {
                    currentTree.parent().setChild(
                            ILUtils.copy(assignLeftTree), currentTree.rankInParent());
                }
                done = true;
            }
        }
    }

    /**
     * Toutes les backFlow viennent d'un EntryBlock.
     */
    private void entryArrows(Block block, Block curblock, InFunction ffunction) {
        EntryBlock entryBlock = ffunction.copiedUnit.entryBlock();
        TapList<FGArrow> backFlow = curblock.backFlow();
        FGArrow arrow;
        FGArrow entryArrow = null;
        /*while inutile backFlow ne contient qu'une arrow
         dans le cas simple des functions a inliner
         */
        while (backFlow != null) {
            arrow = backFlow.head;
            if (arrow.origin == entryBlock) {
                entryArrow = arrow;
                arrow.delete();
            }
            backFlow = backFlow.tail;
        }
        if (entryArrow != null) {
            backFlow = block.backFlow();
            while (backFlow != null) {
                backFlow.head.redirectDestination(curblock);
                backFlow = backFlow.tail;
            }
        }
    }

    /**
     * Toutes les flow vont sur un ExitBlock.
     */
    private void exitArrows(Block block, Block curblock, InFunction ffunction) {
        ExitBlock exitBlock = ffunction.copiedUnit.exitBlock();
        TapList<FGArrow> flow = curblock.flow();
        FGArrow arrow;
        while (flow != null) {
            arrow = flow.head;
            if (arrow.destination == exitBlock) {
                arrow.redirectDestination(block);
            }
            flow = flow.tail;
        }
    }
}
