/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * The specific SymbolTable entry for a class definition.
 */
public final class ClassDecl extends SymbolDecl {

    /**
     * The node of this class in the callgraph. It can be null in special case (nested class).
     **/
    public Unit unit;

    public ClassDecl(String name, Unit unit) {
        super(name, SymbolTableConstants.CLASS);
        this.unit = unit;
        this.unit.setClassTypeSpec(new ClassTypeSpec(this.unit));
    }

    @Override
    public String toString() {
        return "class " + symbol//+"@"+Integer.toHexString(hashCode())
                + "->" + unit;
    }
}
