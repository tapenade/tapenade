/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

/**
 * A "pointer" type, to some destination type.
 */
public final class PointerTypeSpec extends TypeSpec {

    /**
     * The pointer destination type.
     */
    public WrapperTypeSpec destinationType;

    /**
     * Access info on this destination e.g. "const" etc.
     */
    protected TapList<String> subAccess;

    /**
     * When this pointer type actually means a C array type, the dimension of this array.
     */
    public ArrayDim offsetLength;

    /**
     * Create a new pointer type to destination type, possibly with a C array dimension.
     */
    public PointerTypeSpec(WrapperTypeSpec destinationType, ArrayDim offsetLength) {
        super(SymbolTableConstants.POINTERTYPE);
        this.offsetLength = offsetLength;
        this.destinationType = destinationType;
    }

    /**
     * @return true if this PointerTypeSpec is a C array type.
     */
    public boolean isInFactArray() {
        return offsetLength != null;
    }

    /**
     * @return the ArrayTypeSpec if this PointerTypeSpec is a C array type.
     */
    protected ArrayTypeSpec pointerToArrayTypeSpec() {
        ArrayTypeSpec result = null;
        if (this.offsetLength != null) {
            ArrayDim[] newDimensions = new ArrayDim[1];
            newDimensions[0] = this.offsetLength;
            result = new ArrayTypeSpec(
                    this.destinationType.pointerToArrayTypeSpec(),
                    newDimensions);
        }
        return result;
    }

    @Override
    public TypeSpec wrappedType() {
        return destinationType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        destinationType = type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type);
    }

    @Override
    public TypeSpec nestedLevelType() {
        return destinationType;
    }

    @Override
    protected TypeSpec peelPointer() {
        return destinationType;
    }

    @Override
    protected String baseTypeName() {
        return "pointer";
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        return destinationType.baseTypeSpec(stopOnPointer);
    }

    @Override
    public WrapperTypeSpec modifiedBaseTypeSpec() {
        return destinationType.modifiedBaseTypeSpec();
    }

    @Override
    protected boolean isAnIOTypeSpec(SymbolTable symbolTable) {
        return destinationType != null && destinationType.isAnIOTypeSpec(symbolTable);
    }

    @Override
    public boolean containsAPointer() {
        return true;
    }

    @Override
    public boolean containsUnknownDimension() {
        boolean result = false;
        if (destinationType != null) {
            result = destinationType.containsUnknownDimension();
        }
        if (!result) {
            result = offsetLength == null || offsetLength.isUnknown();
        }
        return result;
    }

    @Override
    public void updateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (destinationType != null) {
            destinationType.updateAfterImports(symbolTable, dejaVu);
        }
        if (offsetLength != null) {
            offsetLength.updateImportedDim(symbolTable);
        }
    }

    @Override
    protected int computeSize() {
        return TapEnv.POINTER_SIZE;
    }

    @Override
    public boolean isPointer() {
        return true;
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a Pointer* make little sense, so we will peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // in C, pointers are actually integers, so they can receive integers:
        if (TapEnv.relatedLanguageIsC() && testIsReceives(comparison) &&
                other instanceof PrimitiveTypeSpec && ((PrimitiveTypeSpec) other).isInteger()) {
            return true;
        }
        // in C, a pointer to a function can RECEIVE a plain function (i.e. not a pointer to function):
        if (TapEnv.relatedLanguageIsC() && testIsReceives(comparison) && other.isFunction()) {
            other = new PointerTypeSpec(new WrapperTypeSpec(other), null);
        }
        // if other is a MetaTypeSpec, turn it into a standard TypeSpec (hopefully a PointerTypeSpec)
        if (other instanceof MetaTypeSpec && testAcceptsUnspecified(comparison)) {
            other = ((MetaTypeSpec) other).makeLocalizedTypeSpec();
        }
        // now other must be of matching kind:
        if (!(other instanceof PointerTypeSpec)) {
            return false;
        }
        PointerTypeSpec otherPointer = (PointerTypeSpec) other;
        // dejaVu case: stop recursion and return true:
        if (TapList.containsObjectPair(dejaVu, destinationType, otherPointer.destinationType)) {
            return true;
        }
        boolean comparesWell = true;
        // compare destinations:
        if (destinationType == null) {
            // if this's destinationType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                setWrappedType(otherPointer.destinationType == null ? null : otherPointer.destinationType.weakenForInference(comparison));
            } else {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (otherPointer.destinationType == null) {
            // if other's elementType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                otherPointer.setWrappedType(destinationType);
            } else {
                comparesWell = false;
            }
        } else if (testIsReceives(comparison) &&
                (isA(this.destinationType, SymbolTableConstants.VOIDTYPE) ||
                        this.destinationType != null && this.destinationType.isCharacter() ||
                        isA(otherPointer.destinationType, SymbolTableConstants.VOIDTYPE) ||
                        otherPointer.destinationType != null && otherPointer.destinationType.isCharacter())) {
            // In C, we can always assign a void* (and also a char*) to any pointer, and vice-versa
            //comparesWell = true ; //comparesWell is already true!
        } else {
            comparesWell =
                    destinationType.comparesWith(otherPointer.destinationType, comparison,
                            this, otherPointer,
                            new TapList<>(new TapPair<>(destinationType, otherPointer.destinationType), dejaVu));
        }
        // compare dimensions:
        if (comparesWell) {
            if (offsetLength == null) {
                // So far, no type inference on the pointer's dimension:
                comparesWell = true;
            } else if (otherPointer.offsetLength == null) {
                comparesWell = true;
            } else {
                comparesWell =
                        offsetLength.equalLength(otherPointer.offsetLength, comparison);
            }
        }
        return comparesWell;
    }

    @Override
    protected TypeSpec weakenForInference(int comparison) {
        if (destinationType == null) {
            return new PointerTypeSpec(new WrapperTypeSpec(null), null);
        } else {
            return new PointerTypeSpec((WrapperTypeSpec) destinationType.weakenForInference(comparison), null);
        }
    }

    @Override
    public PointerTypeSpec copy() {
        return new PointerTypeSpec((WrapperTypeSpec) destinationType.copy(), offsetLength);
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        TypeSpec copyDestination = destinationType == null ? null : destinationType.copyStopOnComposite(publishedUnit);
        return copyDestination == destinationType ? this : new PointerTypeSpec((WrapperTypeSpec) copyDestination, offsetLength);
    }

    @Override
    public Tree buildConstantZero() {
        if (offsetLength != null) {
            ArrayTypeSpec arrayTypeSpec = pointerToArrayTypeSpec();
            assert arrayTypeSpec != null;
            return arrayTypeSpec.buildConstantZero();
        } else {
            if (TapEnv.relatedLanguageIsC() || TapEnv.relatedUnit() == null) {
                return ILUtils.build(ILLang.op_cast,
                        ILUtils.build(ILLang.op_pointerType, ILUtils.build(ILLang.op_void)),
                        ILUtils.build(ILLang.op_intCst, 0));
            } else {
                return ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "NULL"),
                        ILUtils.build(ILLang.op_expressions));
            }
        }
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            return false;
        }
        return destinationType.containsMetaType(new TapList<>(this, dejaVu))
                || offsetLength == null || offsetLength.isUnknown();
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        PointerTypeSpec copiedResult = (PointerTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            copiedResult = new PointerTypeSpec(null, offsetLength);
            copiedResult.setOrAddTypeDeclName(typeDeclName());
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            if (destinationType != null) {
                copiedResult.destinationType = (WrapperTypeSpec) destinationType.localize(toAlreadyCopied, containsMeta);
            }
            if (containsMeta.get()) {
                // If this type contains a meta, write it in the memo of already localized types:
                alreadyRef.third = Boolean.TRUE;
            } else {
                // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
                // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
                alreadyRef.first = null;
            }
        }
        return copiedResult;
    }

    @Override
    public TypeSpec preciseDimensions(TypeSpec complementType,
                                      TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, SymbolTable symbolTable) {
        if (!isA(complementType, SymbolTableConstants.POINTERTYPE)) {
            return this;
        }
        PointerTypeSpec precisedType = (PointerTypeSpec) TapList.cassq(this, dejaVu);
        if (precisedType == null) {
            precisedType = new PointerTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, precisedType));
            WrapperTypeSpec precisedDestinationType =
                    WrapperTypeSpec.preciseDimensions(destinationType, ((PointerTypeSpec) complementType).destinationType,
                            dejaVu, symbolTable);
            ArrayDim precisedOffsetLength =
                    ArrayDim.preciseDimension(offsetLength, ((PointerTypeSpec) complementType).offsetLength,
                            symbolTable);
            if (precisedDestinationType == destinationType && precisedOffsetLength == offsetLength
                    || precisedDestinationType.wrappedType == null || precisedOffsetLength == null) {
                precisedType = this;
            } else {
                precisedType.destinationType = precisedDestinationType;
                precisedType.offsetLength = precisedOffsetLength;
            }
        }
        return precisedType;
    }

    @Override
    public TypeSpec intToReal(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                              SymbolTable symbolTable) {
        PointerTypeSpec convertedType = (PointerTypeSpec) TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            convertedType = new PointerTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedDestinationType =
                    WrapperTypeSpec.intToReal(destinationType, dejaVu, symbolTable);
            if (convertedDestinationType == destinationType
                    || convertedDestinationType.wrappedType == null) {
                convertedType = this;
            } else {
                convertedType.destinationType = convertedDestinationType;
                convertedType.offsetLength = offsetLength;
            }
        }
        return convertedType;
    }

    @Override
    protected TypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        PointerTypeSpec convertedType = (PointerTypeSpec) TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            convertedType = new PointerTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedDestinationType =
                    destinationType.realToComplex(dejaVu, complexTypeSpec);
            if (convertedDestinationType == destinationType
                    || convertedDestinationType.wrappedType == null) {
                convertedType = this;
            } else {
                convertedType.destinationType = convertedDestinationType;
                convertedType.offsetLength = offsetLength;
            }
        }
        return convertedType;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        WrapperTypeSpec result = null;
        ArrayDim diffOffsetLength = offsetLength;
        if (TapEnv.relatedLanguageIsFortran()) {
	    //lh/mv:2023-02-01: when declaring Fortran pointer/allocatable variables
	    //                  we *must not* have fixed dimensions (as e.g. 'nbmultidirmax')
	    //                  but must stick to the ':' notation
            multiDirDimensionMax =
                new ArrayDim(ILUtils.build(ILLang.op_dimColon,
                               ILUtils.build(ILLang.op_none), ILUtils.build(ILLang.op_none)),
                             null, null, null, -1, 0, 0);
        }
        WrapperTypeSpec activeDestinationType =
                destinationType.differentiateTypeSpecMemo(symbolTable, srcSymbolTable, diffUnitSort, fSuffix,
                        false, multiDirMode, multiDirDimensionMax,
                        "*"+hintArrayNameInText,
                        "Drf" + hintArrayNameInIdent, hintArrayNameTree, null);
        boolean isForAdjoint = diffUnitSort == 2 || diffUnitSort == 3; //Slightly dirty for constants ADJOINT and ADJOINT_FWD
        if (activeDestinationType == null && this.subAccess != null && isForAdjoint)
        // If this is for adjoints and we had subAccess info, the diff type loses it.
        // therefore the diff type is no longer the same, which was the meaning of activeDestinationType==null.
        {
            activeDestinationType = destinationType;
        }
        if (activeDestinationType != null) { // || addDimension) {
            PointerTypeSpec diffPointerType = new PointerTypeSpec(activeDestinationType, diffOffsetLength);
            if (!isForAdjoint) {
                diffPointerType.subAccess = this.subAccess;
            }
            result = new WrapperTypeSpec(diffPointerType);
        }
        if (TapEnv.associationByAddress()) {
            diffTypeSpec = result;
        }
        return result;
    }

    @Override
    protected void addDiffTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable) {
        destinationType.addRefDiffTypeSpec(symbolTable, srcSymbolTable);
    }

    @Override
    public boolean acceptsMultiDirDimension() {
        return (destinationType!=null && destinationType.acceptsMultiDirDimension()) ;
    }

    @Override
    protected void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
        if (diffInfos != null && destinationType != null) {
            destinationType.cumulActiveParts(diffInfos.tail, symbolTable);
            // Mark named types so that there is a differentiated type name:
            if (typeDeclName() != null && needsADiffType(null)) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeDeclName());
                if (typeDecl != null) {
                    typeDecl.setActive(true);
                }
            }
        }
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return destinationType != null && !TapList.contains(dejaVu, this)
                && destinationType.needsADiffType(new TapList<>(this, dejaVu));
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return destinationType != null && !TapList.contains(dejaVu, this)
                && destinationType.isDifferentiated(new TapList<>(this, dejaVu));
    }

    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        if (destinationType != null && destinationType.wrappedType == this) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) Illegal recursive type definition " + this.showType());
            return false;
        } else {
            assert destinationType != null;
            return destinationType.checkTypeSpecValidity(dejaVu);
        }
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree result;
        if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            result = ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            dejaVu = new TapList<>(this, dejaVu);
            if (offsetLength != null) {
                ArrayTypeSpec arrayTypeSpec = pointerToArrayTypeSpec();
                assert arrayTypeSpec != null;
                result = arrayTypeSpec.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
            } else {
                destinationType = destinationType.equalsDiffTypeSpecAndTypeSpec();
                result = destinationType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
                result = addTypeModifiers(result, ILUtils.buildListIdents(subAccess));
                result = ILUtils.build(ILLang.op_pointerType, result);
            }
        }
        return result;
    }

    @Override
    public String showType() {
        int language = TapEnv.relatedUnit() != null ? TapEnv.relatedUnit().language() : -1;
        return (destinationType == null ? "?" : destinationType.showType()) +
                (TapEnv.isFortran(language) ? " POINTER" : offsetLength == null ? "*" : "[" + offsetLength.showType() + "]");
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "*" //+"@"+Integer.toHexString(hashCode())
                + (offsetLength == null ? "" : "[" + offsetLength + "]")
                + (subAccess == null ? "" : subAccess) + (destinationType == null ? "?" : destinationType.toString());
    }
}
