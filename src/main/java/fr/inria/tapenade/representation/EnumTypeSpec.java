/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * An "enumeration" type.
 */
public final class EnumTypeSpec extends TypeSpec {

    /**
     * A name for this type.
     */
    private final String name;
    /**
     * The Tree of the list of possible values.
     */
    private final Tree enumTree;

    /**
     * Create a new enumeration type.
     */
    public EnumTypeSpec(String name, Tree tree) {
        super(SymbolTableConstants.ENUMTYPE);
        this.name = name;
        this.enumTree = tree;
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // Peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around an Enum* make little sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, we may want type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // Special for C: an enum can be seen as an integer, so it can compare with integers:
        if (TapEnv.relatedLanguageIsC() &&
                isA(other, SymbolTableConstants.PRIMITIVETYPE) && ((PrimitiveTypeSpec) other).isInteger()) {
            return true;
        }
        // else other must be of matching kind:
        if (!(other instanceof EnumTypeSpec)) {
            return false;
        }
        return enumTree.equalsTree(((EnumTypeSpec) other).enumTree);
    }

    @Override
    public TypeSpec copy() {
        return this;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        return ILUtils.build(ILLang.op_enumType,
                ILUtils.build(ILLang.op_ident, name),
                enumTree);
    }

    @Override
    public String showType() {
        String shorthandName = typeDeclName();
        if (shorthandName == null) {
            return "Enum " + name;
        } else {
            return shorthandName;
        }
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "enum" //+"@"+Integer.toHexString(hashCode())
                + " " + name + "(" + ILUtils.toString(enumTree) + ')';
    }
}
