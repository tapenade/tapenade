/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;
import fr.inria.tapenade.utils.ToObject;

/**
 * One dimension of an ArrayTypeSpec. Holds the original Tree,
 * and the computed lower and upper dimensions.
 */
public class ArrayDim {
    private Tree tree;

    /**
     * @return the array dimension tree.
     */
    public Tree tree() {
        return tree;
    }

    /** Special case: special dimension Tree to be used in declaration only */
    public Tree treeForDeclaration = null ;

    public void setSizeForDeclaration(Tree sizeTree) {
        treeForDeclaration = sizeTree ;
    }

    /**
     * Set the array dimension tree.
     *
     * @param tree the array dimension.
     */
    protected void setTree(Tree tree) {
        this.tree = tree;
    }

    /**
     * Array lower bound.
     */
    protected Integer lower;
    /**
     * Array lower bound.
     */
    protected Integer upper;

    /**
     * A "reference" array variable whose rankInRef'th dimension size determines this dimension size.
     */
    protected Tree refArrayTree;
    /**
     * The number of dimensions of the reference array.
     */
    protected int refNDims;
    /**
     * The rank of the reference dimension inside the reference array.
     * -1 for unknown, 0 for unique dimension, else 1 for 1st child, etc.
     */
    protected int rankInRef;
    /**
     * Prefer sizes that are given as an immediate integer, then as an integer variable,
     * then declared as ":" but certainly allocated, then as a last resort declared as allocatable...
     */
    protected int refQuality;
    /**
     * true when we cannot declare this dimension because its size is "dynamic", so that
     * actual refs to this dimension must use an explicit arrayTriplet instead of just ":"
     */
    public boolean unDeclarableLength = false ;
    /** String that may describe a possible overestimate of this dimension's length
     * (can be useful when unDeclarableLength is true) */
    public String overEstimate = null ;


    public ArrayDim(Tree dimensionTree, Integer lower, Integer upper, Tree refArrayTree, int rankInRef, int refNDims, int refQuality) {
        super();
        this.refArrayTree = refArrayTree;
        this.rankInRef = rankInRef;
        this.refNDims = refNDims;
        if (dimensionTree == null) {
            Tree lowerTree =
                    lower == null
                            ? ILUtils.build(ILLang.op_none)
                            : ILUtils.build(ILLang.op_intCst, lower);
            Tree upperTree =
                    upper == null
                            ? ILUtils.build(ILLang.op_none)
                            : ILUtils.build(ILLang.op_intCst, upper);
            this.tree = ILUtils.build(ILLang.op_dimColon, lowerTree, upperTree);
        } else {
            if (dimensionTree.opCode()!=ILLang.op_dimColon) {
                dimensionTree = ILUtils.build(ILLang.op_dimColon,
                                  ILUtils.build(ILLang.op_none),
                                  ILUtils.copy(dimensionTree)) ;
            }
            this.tree = dimensionTree;
            Tree lowerTree = dimensionTree.down(1) ;
            Tree upperTree = dimensionTree.down(2) ;
            if (lower==null) {
                if (ILUtils.isNullOrNone(lowerTree)) {
                    lower = 1 ;
                } else if (lowerTree.opCode()==ILLang.op_intCst) {
                    lower = lowerTree.intValue() ;
                }
            }
            if (upper==null) {
                if (ILUtils.isNullOrNone(upperTree)) {
                    upper = null ;
                } else if (upperTree.opCode()==ILLang.op_intCst) {
                    upper = upperTree.intValue() ;
                }
            }
        }
        this.refQuality = 0 ;
        if (refArrayTree!=null) this.refQuality = refQuality ;
        if (upper!=null) this.refQuality = 8 ;
        this.lower = lower;
        this.upper = upper;
    }

    private static boolean equalBound(Tree bLeft, Tree bRight, int comparison) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = TypeSpec.compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("Bound equal " + bLeft + " " + TypeSpec.showComparison(comparison) + " " + bRight);
            ++TypeSpec.compare_debug_indent;
        }
        boolean result;
        if (TypeSpec.testTypesLitteral(comparison)) {
            result = !ILUtils.isNullOrNone(bLeft) && !ILUtils.isNullOrNone(bRight) && bLeft.equalsTree(bRight);
        } else {
            result = ILUtils.isNullOrNone(bLeft) || ILUtils.isNullOrNone(bRight) ||
                    true // TODO should be some DIFFERENT!=eqOrDifferentValue(bLeft, bRight, instrLeft, instrRight), but this is not implemented yet
            ;
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            --TypeSpec.compare_debug_indent;
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = TypeSpec.compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("Bound equal " + bLeft + " " + TypeSpec.showComparison(comparison) + " " + bRight + " RETURNS:" + result);
        }
        return result;
    }

    protected static ArrayDim preciseDimension(ArrayDim mainDim, ArrayDim complementDim,
                                               SymbolTable symbolTable) {
        if (complementDim==null || mainDim==null || (mainDim.lower != null && mainDim.upper != null)) {
            return mainDim;
        }
        Integer complementLower = complementDim.lower;
        Integer complementUpper = complementDim.upper;
        if (complementDim.tree != null && complementDim.tree.opCode() != ILLang.op_arrayTriplet) {
            Tree lowerComplementDimTree;
            Tree upperComplementDimTree;
            if (complementDim.tree.opCode() == ILLang.op_dimColon) {
                lowerComplementDimTree = complementDim.tree.down(1);
                upperComplementDimTree = complementDim.tree.down(2);
            } else {
                lowerComplementDimTree = null;
                upperComplementDimTree = complementDim.tree;
            }
            if (complementLower == null) {
                if (lowerComplementDimTree == null
                        || lowerComplementDimTree.opCode() == ILLang.op_none) {
                    complementLower = 1;
                } else {
                    complementLower = symbolTable.computeIntConstant(lowerComplementDimTree);
                }
            }
            if (complementUpper == null) {
                if (upperComplementDimTree != null
                        && upperComplementDimTree.opCode() != ILLang.op_none) {
                    complementUpper = symbolTable.computeIntConstant(upperComplementDimTree);
                }
            }
            if (complementLower != null && complementUpper != null) {
                return new ArrayDim(complementDim.tree, complementLower, complementUpper,
                        mainDim.refArrayTree, mainDim.rankInRef, mainDim.refNDims, mainDim.refQuality);
            }
        }
        return mainDim;
    }

    /**
     * @return When dim1 and dim2 conform, the one which is best suited to provide
     * the size of this dimension. Otherwise return null.
     */
    protected static ArrayDim testConformingAndChoose(ArrayDim dim1, ArrayDim dim2) {
        int size1 = dim1 == null ? -1 : dim1.size();
        int size2 = dim2 == null ? -1 : dim2.size();
        if (size1 != size2 && size1 != -1 && size2 != -1) {
            return null;
        }
        if (dim2 == null || dim2.refQuality == 0 || size1 != -1) {
            return dim1;
        } else if (dim1 == null || dim1.refQuality == 0 || size2 != -1) {
            return dim2;
        } else if (dim1.betterThanCallSize()) {
            return dim1;
        } else if (dim2.betterThanCallSize()) {
            return dim2;
        } else if (dim1.refQuality > dim2.refQuality) {
            return dim1;
        } else if (dim1.refQuality == dim2.refQuality) {
            String name1 = ILUtils.baseName(dim1.refArrayTree);
            String name2 = ILUtils.baseName(dim2.refArrayTree);
            if (name1 != null && (name2 == null || name1.compareTo(name2) <= 0)) {
                return dim1;
            } else {
                return dim2;
            }
        } else {
            return dim2;
        }
    }

    /**
     * @return true if 1:expression or null:expression
     * and expression is not null, so that it is better to directly use "expression"
     * as the size in declarations, rather than to call SIZE(array).
     */
    private boolean betterThanCallSize() {
        Tree sizeTree = tree;
        if (sizeTree != null && sizeTree.opCode() == ILLang.op_dimColon) {
            if (ILUtils.isNullOrNone(sizeTree.down(1)) || ILUtils.isIntCst(sizeTree.down(1), 1)) {
                sizeTree = sizeTree.down(2);
            } else {
                sizeTree = null;
            }
        }
        return !ILUtils.isNullOrNone(sizeTree);
    }

    /**
     * Copy this ArrayDim.
     *
     * @return a copy, with a copy of the array tree dimension.
     */
    protected ArrayDim copy() {
        ArrayDim copy = new ArrayDim(tree.copy(), lower, upper, refArrayTree, rankInRef, refNDims, refQuality);
        copy.unDeclarableLength = this.unDeclarableLength ;
        return copy ;
    }

    /**
     * Erase "tree" if it uses local symbols of publishedUnit:
     *
     * @param publishedUnit Unit.
     */
    protected void erasePrivateInfo(Unit publishedUnit) {
        //[llh] TODO: presently, we accept dimension expressions that use variables from any outside scope, but
        // we should also reject expression if a variable comes from an outside scope invisible to the call site!
        if (publishedUnit != null) {
            TapList<SymbolDecl> toUsedSymbols = new TapList<>(null, null);
            SymbolTable unitPrivateSymbolTable = publishedUnit.privateSymbolTable();
            if (unitPrivateSymbolTable != null) {
                SymbolDecl.addUsedSymbolsInExpr(tree, toUsedSymbols, unitPrivateSymbolTable, null, true, true);
            }
            SymbolTable unitPublicSymbolTable = publishedUnit.publicSymbolTable();
            if (unitPublicSymbolTable != null) {
                SymbolDecl.addUsedSymbolsInExpr(tree, toUsedSymbols, unitPublicSymbolTable, null, true, true);
            }
            //Remove functionDecl's or typeDecl's from toUsedSymbols:
            TapList<SymbolDecl> inUsedSymbols = toUsedSymbols;
            while (inUsedSymbols.tail != null) {
                if (inUsedSymbols.tail.head == null || inUsedSymbols.tail.head instanceof FunctionDecl
                        || inUsedSymbols.tail.head instanceof TypeDecl || inUsedSymbols.tail.head instanceof ClassDecl) {
                    inUsedSymbols.tail = inUsedSymbols.tail.tail;
                } else {
                    inUsedSymbols = inUsedSymbols.tail;
                }
            }
            if (toUsedSymbols.tail != null) {
                tree = ILUtils.build(ILLang.op_dimColon,
                        ILUtils.build(ILLang.op_none),
                        ILUtils.build(ILLang.op_none));
            }
        }
        refArrayTree = null;
        rankInRef = -1;
        refNDims = 0;
        refQuality = 0;
    }

    protected boolean isSufficientlyDefined() {
        return lower != null && upper != null
                || !ILUtils.isNullOrNone(tree) && isDynamicallyExplicit(tree);
    }

    /**
     * @return true if dimTree, at run-time, gives full info about the size of this dimension.
     */
    private static boolean isDynamicallyExplicit(Tree dimTree) {
        if (dimTree.opCode() == ILLang.op_dimColon) {
            return !ILUtils.isNullOrNoneOrStar(dimTree.down(2));
        } else if (dimTree.opCode() == ILLang.op_arrayTriplet) {
            return !ILUtils.isNullOrNoneOrStar(dimTree.down(2));
        } else {
            return !ILUtils.isNullOrNoneOrStar(dimTree);
        }
    }

    /**
     * @return true if unknown dimension.
     */
    protected boolean isUnknown() {
        return (ILUtils.isNullOrNone(tree) ||
                tree.opCode() == ILLang.op_dimColon &&
                        ILUtils.isNullOrNone(tree.down(1)) &&
                        ILUtils.isNullOrNone(tree.down(2)))
                && upper == null
                && (lower == null || lower == 1);
    }

    /**
     * @return true if dimension is "none".
     */
    protected boolean isNone() {
        boolean result = false;
        Tree upperDimTree;
        if (tree != null) {
            if (tree.opCode() == ILLang.op_dimColon) {
                upperDimTree = tree.down(2);
            } else {
                upperDimTree = tree;
            }
            result = upperDimTree.opCode() == ILLang.op_none;
        }
        return result;
    }

    public  boolean isNullOrNoneOrStar() {
        if (tree == null) {
            return true ;
        } else {
            return ILUtils.isNullOrNoneOrStar(tree.opCode()==ILLang.op_dimColon ? tree.down(2) : tree) ;
        }
    }

    /**
     * @return intger array size using lower and upper bounds.
     */
    public int size() {
        if (lower != null && upper != null) {
            return upper + 1 - lower;
        } else {
            return -1;
        }
    }

    protected ArrayDim weakenForInference() {
        return new ArrayDim(null, null, null, null, -1, 0, 0);
    }

    protected boolean equalLength(ArrayDim otherDim, int comparison) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = TypeSpec.compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("Dimension equalLength " + this + " " + TypeSpec.showComparison(comparison) + " " + otherDim);
            ++TypeSpec.compare_debug_indent;
        }
        boolean areEqual = otherDim != null;
        if (areEqual) {
            if (this.lower != null && this.upper != null && otherDim.lower != null && otherDim.upper != null) {
                areEqual = this.upper - this.lower == otherDim.upper - otherDim.lower;
            } else if (this.refQuality > 0 && this.refQuality == otherDim.refQuality
                    && this.refArrayTree != null && otherDim.refArrayTree != null
                    && this.rankInRef == otherDim.rankInRef
                    && this.refArrayTree.equalsTree(otherDim.refArrayTree)) {
                // Special equality test for ArrayDim's that are used for temp vars dimensioned after SIZE() of another array:
                areEqual = true;
            } else if (tree != null && otherDim.tree != null) {
                areEqual = equalBound(tree.down(1), otherDim.tree.down(1), comparison)
                        && equalBound(tree.down(2), otherDim.tree.down(2), comparison);
            } else {
                areEqual = false;
            }
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            --TypeSpec.compare_debug_indent;
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = TypeSpec.compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("Dimension equalLength " + this + " " + TypeSpec.showComparison(comparison) + " " + otherDim + " RETURNS:" + areEqual);
        }
        return areEqual;
    }

    /**
     * @return a tree representing the array size.
     */
    public Tree getSize() {
        if (tree.down(1).opCode() == ILLang.op_none
                || ILUtils.evalsToOne(tree.down(1))) {
            return tree.down(2).copy();
        } else {
            return ILUtils.build(
                    ILLang.op_sub, tree.down(2).copy(),
                    ILUtils.buildSmartAddSub(
                            tree.down(1).copy(), 1,
                            ILUtils.build(ILLang.op_intCst, -1)));
        }
    }

    /**
     * Modifies the upper bound of this ArrayDim so that its total
     * size becomes equal to "sizeTree".
     */
    protected void setSize(Tree sizeTree) {
        Tree lowerTree;
        if (tree.down(1).opCode() == ILLang.op_none) {
            lowerTree = ILUtils.build(ILLang.op_intCst, 1);
        } else {
            lowerTree = tree.down(1).copy();
        }
        tree.setChild(
                ILUtils.buildSmartAddSub(
                        sizeTree, 1,
                        ILUtils.buildSmartAddSub(
                                lowerTree, 1, ILUtils.build(ILLang.op_intCst, -1))), 2);
    }

    protected Tree lowerTree() {
        if (lower != null) {
            return ILUtils.build(ILLang.op_intCst, lower);
        }
        if (tree.opCode() == ILLang.op_dimColon) {
            if (tree.down(1).opCode() == ILLang.op_none) {
                return ILUtils.build(ILLang.op_intCst, 1);
            } else {
                return tree.down(1);
            }
        } else {
            return ILUtils.build(ILLang.op_intCst, 1);
        }
    }

    protected Tree upperTree() {
        if (upper != null) {
            return ILUtils.build(ILLang.op_intCst, upper);
        }
        if (tree.opCode() == ILLang.op_dimColon) {
            return tree.down(2);
        } else {
            return tree;
        }
    }

    public boolean fullArrayTriplet(Tree tree) {
        if (tree.opCode() == ILLang.op_arrayTriplet) {
            Tree begin = tree.down(1);
            Tree end = tree.down(2);
            Tree stride = tree.down(3);
            return (ILUtils.isNullOrNone(begin) || begin.equalsTree(lowerTree())) &&
                    (ILUtils.isNullOrNone(end) || end.equalsTree(upperTree())) &&
                    (ILUtils.isNullOrNone(stride) || ILUtils.evalsToOne(stride));
        }
        return false;
    }

    protected void updateImportedDim(SymbolTable symbolTable) {
        Tree lowerDimTree;
        Tree upperDimTree;
        if (tree.opCode() == ILLang.op_dimColon) {
            lowerDimTree = tree.down(1);
            upperDimTree = tree.down(2);
        } else {
            lowerDimTree = null;
            upperDimTree = tree;
        }
        if (lower == null) {
            if (lowerDimTree != null
                    && lowerDimTree.opCode() != ILLang.op_none) {
                lower = symbolTable.computeIntConstant(lowerDimTree);
            }
        }
        if (upper == null) {
            if (upperDimTree != null
                    && upperDimTree.opCode() != ILLang.op_none) {
                upper = symbolTable.computeIntConstant(upperDimTree);
            }
        }
    }

    /** Mark this dimension (which evaluates well in "symbolTable") when it
     * cannot be evaluated correctly in the (publicST) SymbolTable
     * in which it must be used for an array declaration */
    public void detectUnDeclarableLength(SymbolTable symbolTable) {
        if (!unDeclarableLength && tree!=null) {
            SymbolTable publicST = (symbolTable.unit==null ? null : symbolTable.unit.publicSymbolTable()) ;
            if (publicST==null || !onlyPublicVars(tree.down(2), symbolTable, publicST)) {
                unDeclarableLength = true ;
            }
        }
    }

    /**
     * @return true iff all variables used to compute expr belong to
     *  the given public level publicST or to an enclosing level.
     * Therefore expr can be used as a size in an array declaration.
     */
    private static boolean onlyPublicVars(Tree expr, SymbolTable useSymbolTable, SymbolTable publicST) {
        if (expr==null) return true ;
        boolean ok = true ;
        switch (expr.opCode()) {
        case ILLang.op_ident: {
            ToObject<SymbolTable> toST = new ToObject<>(null) ;
            TapList<SymbolDecl> toFoundDecl = useSymbolTable.getToDecl(expr.stringValue(), SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, false, toST) ;
            SymbolDecl foundDecl = (toFoundDecl==null ? null : toFoundDecl.tail.head) ;
            ok = (foundDecl!=null &&
                  (// accept when variable in dimension is a formal argument:
                   publicST.nestedIn(toST.obj())
                   ||
                   // accept when variable in dimension is declared locally, but as a constant:
                   (foundDecl.isA(SymbolTableConstants.CONSTANT) && toST.obj().basisSymbolTable()==publicST)
                   )) ;
            break ;
        }
        case ILLang.op_call: {
            Tree args = ILUtils.getArguments(expr) ;
            ok = ("size".equals(ILUtils.getCalledNameString(expr))
                  && args!=null && args.length()>0
                  && onlyPublicVars(args.down(1), useSymbolTable, publicST)) ;
            break ;
        }
        case ILLang.op_add :
        case ILLang.op_sub :
        case ILLang.op_mul :
        case ILLang.op_div :
            ok = onlyPublicVars(expr.down(1), useSymbolTable, publicST)
                &&  onlyPublicVars(expr.down(2), useSymbolTable, publicST) ;
            break ;
        case ILLang.op_minus:
            ok = onlyPublicVars(expr.down(1), useSymbolTable, publicST) ;
            break ;
        case ILLang.op_intCst:
        case ILLang.op_none:
            break ;
        default:
            ok = false ;
            break ;
        }
        return ok ;
    }

    protected String showType() {
        int language = TapEnv.relatedUnit() != null ? TapEnv.relatedUnit().language() : -1;
        if (TapEnv.isFortran(language)) {
            return (lower == null || lower == 1 ? "" : lower + ":")
                    + (upper == null ? "*" : upper);
        } else {
            return lower != null && lower == 0 ?
                    upper == null ? "*" : Integer.toString(upper + 1) :
                    (lower == null ? "?" : lower) + ":" + (upper == null ? "*" : upper);
        }
    }

    /**
     * Prints in detail the contents of this ArrayDim, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
	//lh/mvo:2023-02-01: We show lower and upper when present, but also the tree because
	//                   in some cases (e.g. nbdirsmax) this is the meaningful data
	return (lower==null ? "_" : lower)+":"+(upper==null ? "*" : upper) //+" @"+Integer.toHexString(hashCode())
	    //+" ref:"+refArrayTree+" Q:"+refQuality +" unDeclarable:"+unDeclarableLength+" overEstimate:"+overEstimate
            +" [tree = "+tree+"]" ;
    }
}
