/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;
import fr.inria.tapenade.representation.TapList;

/**
 * A composite type, e.g. a record or a union.
 */
public class CompositeTypeSpec extends TypeSpec {

    /**
     * Kind of this composite type. Can be RECORDTYPE or UNIONTYPE.
     */
    private final int subKind;
    /**
     * The code unit that defines this type.
     */
    private final Unit definingUnit;
    /**
     * The fields of this composite type, in order.
     */
    public FieldDecl[] fields;
    /**
     * The modifiers of this type (e.g. sequence, private for F95 derived types).
     */
    public Tree modifiers;
    /**
     * When available, the name of this type.
     */
    public String name;

    /**
     * The list of activity Booleans for each field.
     * With a mechanism for sharing between copies of this same type.
     */
    private boolean[] activeFields;
    /**
     * Mechanism for sharing fields activity between copies:
     * List of the CompositeTypeSpec's copies that share this.activeFields.
     */
    private TapList<CompositeTypeSpec> copiesSharingActiveFields;

    /**
     * When true, indicates that this CompositeTypeSpec is an IO type (and therefore its zones must be special).
     */
    protected boolean isAnIOType;
    private NewSymbolHolder unknownTypeSymbolHolder;

    /** Flag to avoid solving an "extends" clause twice. */
    private boolean extendsDone = false ;

    /**
     * Creates a new composite type.
     */
    public CompositeTypeSpec(String name, FieldDecl[] fields, int subKind,
                             Tree modifiers, Unit definingUnit) {
        super(SymbolTableConstants.COMPOSITETYPE);
        this.name = name;
        this.fields = fields;
        this.copiesSharingActiveFields = new TapList<>(this, null);
        this.initActiveFields();
        this.subKind = subKind;
        this.modifiers = modifiers;
        if (modifiers == null && subKind == SymbolTableConstants.RECORDTYPE) {
            //[llh] why this only for RECORDTYPE ??
            this.modifiers = ILUtils.build(ILLang.op_modifiers);
        }
        this.definingUnit = definingUnit;
    }

    /**
     * @return true is this composite is a record type.
     */
    public boolean isRecordType() {
        return subKind == SymbolTableConstants.RECORDTYPE;
    }

    /**
     * @return true is this composite is a union type.
     */
    public boolean isUnionType() {
        return subKind == SymbolTableConstants.UNIONTYPE;
    }

    /**
     * @return the declaration of field "name" in this composite type.
     */
    public FieldDecl namedFieldDecl(String name) {
        FieldDecl found = null;
        if (fields != null) {
            for (int i = fields.length - 1; found == null && i >= 0; --i) {
                if (fields[i] != null && fields[i].symbol.equals(name)) {
                    found = fields[i];
                }
            }
        }
        return found;
    }

    /**
     * @return the type of field "name" in this composite type.
     */
    public WrapperTypeSpec namedFieldType(String name) {
        FieldDecl field = namedFieldDecl(name);
        if (field != null) {
            if (TapList.containsEquals(field.extraInfo(), "allocatable")) {
                if (!isA(field.type(), SymbolTableConstants.POINTERTYPE)) {
                    // cas ou on a fait peelPointerUnderAllocatable
                    // avant d'appeler typeOf
                    return new WrapperTypeSpec(new PointerTypeSpec(field.type(), null));
                }
            }
            return field.type();
        } else {
            return null;
        }
    }

    /**
     * @return the type of "field" in this composite type.
     */
    public WrapperTypeSpec getFieldType(Tree field) {
        int fieldRank = ILUtils.getFieldRank(field);
        if (fieldRank >= 0 && fieldRank < fields.length) {
            if (fields[fieldRank] != null) {
                return fields[fieldRank].type();
            } else {
                return null;
            }
        } else {
            return namedFieldType(ILUtils.getIdentString(field));
        }
    }

    /**
     * @return the rank of field "name" in this composite type.
     */
    public int namedFieldRank(String name) {
        int i = fields.length - 1;
        while (i >= 0 && !(fields[i] != null && fields[i].symbol.equals(name))) {
            i--;
        }
        if (i >= 0) {
            return i;
        } else {
            return -1;
        }
    }

    /**
     * @return the offset of field "name" in this composite type.
     */
    public int namedFieldOffset(String name) {
        int result = 0;
        int i = 0;
        while (i < fields.length && !fields[i].symbol.equals(name)) {
            result += fields[i].type().size();
            i++;
        }
        return result;
    }

    protected WrapperTypeSpec checkNamedFieldType(Tree instrTree,
                                                  SymbolTable symbolTable, WrapperTypeSpec typeSpec) {
        Tree fieldTree = instrTree.down(2);
        String fieldName = ILUtils.getIdentString(fieldTree);
        WrapperTypeSpec result = namedFieldType(fieldName);
        if (result != null) {
            // Then there exists a field named "name"
            ILUtils.setFieldRank(fieldTree, namedFieldRank(fieldName));
            return result;
        } else {
            // Otherwise we "recover", by adding a new field named "name"
            TapEnv.fileWarning(TapEnv.MSG_WARN, instrTree, "(TC45) No field named " + fieldName + " in " + this.showType());
            int fieldsLength = 0;
            if (fields != null) {
                fieldsLength = fields.length;
            }
            result = new WrapperTypeSpec(null);
            FieldDecl newFieldDecl = new FieldDecl(fieldTree, result);
            FieldDecl[] newFields = new FieldDecl[fieldsLength + 1];
            boolean[] newActiveFields = new boolean[newFields.length];
            for (int i = fieldsLength - 1; i >= 0; --i) {
                assert fields != null;
                newFields[i] = fields[i];
                newActiveFields[i] = activeFields[i];
            }
            newFields[newFields.length - 1] = newFieldDecl;
            fields = newFields;
            for (TapList<CompositeTypeSpec> sharing = this.copiesSharingActiveFields;
                 sharing != null;
                 sharing = sharing.tail) {
                sharing.head.activeFields = newActiveFields;
            }
            ILUtils.setFieldRank(fieldTree, newFields.length - 1);
            // FORTRAN90: s'il n'y a pas de TypeDecl associe, on le cree
            if (TapEnv.relatedUnit().isFortran()
                    && this.typeDeclName() == null) {
                unknownTypeSymbolHolder =
                        new NewSymbolHolder(TapEnv.disambigNewName("UnknownDerivedType"));
                Tree nameTree = unknownTypeSymbolHolder.makeNewRef(symbolTable);
                TypeDecl newTypeDecl = new TypeDecl(nameTree, typeSpec);
                unknownTypeSymbolHolder.setNewTypeDecl(newTypeDecl);
                unknownTypeSymbolHolder.addTypeDeclSymbolTable(symbolTable);
                unknownTypeSymbolHolder.makeNewRef(symbolTable);
                unknownTypeSymbolHolder.solvingLevelMustInclude(symbolTable);
            }
            return result;
        }
    }

    public void solveTypeExtends(SymbolTable symbolTable) {
        if (!extendsDone && !ILUtils.isNullOrNoneOrEmptyList(modifiers)) {
            String extName = null ;
            Tree[] modifTrees = modifiers.children() ;
            for (int i=modifTrees.length-1 ; i>=0 && extName==null ; --i) {
                if (modifTrees[i].opCode()==ILLang.op_extends) {
                    extName = ILUtils.getIdentString(modifTrees[i].down(1)) ;
                }
            }
            if (extName!=null) {
                WrapperTypeSpec extType = symbolTable.getTypeDecl(extName).typeSpec ;
                if (TypeSpec.isA(extType, SymbolTableConstants.COMPOSITETYPE)) {
                    // first, check that the exteded type is already extending another:
                    ((CompositeTypeSpec)extType.wrappedType).solveTypeExtends(symbolTable) ;
                    // second, copy fields of the extended type into this extending type:
                    FieldDecl[] extFields = ((CompositeTypeSpec)extType.wrappedType).fields ;
                    FieldDecl[] newFields = new FieldDecl[extFields.length + this.fields.length] ;
                    for (int i=0 ; i<extFields.length ; ++i) {
                        newFields[i] = extFields[i] ;
                    }
                    for (int i=0 ; i<this.fields.length ; ++i) {
                        newFields[extFields.length+i] = this.fields[i] ;
                    }
                    this.fields = newFields ;
                    extendsDone = true ;
                } else {
                    TapEnv.toolWarning(-1, "Don't know how to extend type "+extType+" to "+this) ;
                }
            }
        }
    }

    /**
     * @return true if all fields of this composite type are empty.
     */
    public boolean isEmpty() {
        boolean result = true;
        int i = 0;
        while (i < fields.length) {
            result = result && fields[i] == null;
            ++i;
        }
        return result;
    }

    /**
     * @return true if the field of rank "i" in this composite type is differentiated.
     */
    public boolean isDifferentiatedField(int i) {
        return fields[i] != null && activeFields != null && i<activeFields.length && activeFields[i];
    }

    public void initActiveFields() {
        this.activeFields = (fields == null ? null : new boolean[fields.length]);
    }

    /**
     * Make all CompositeTypeSpec's that share this.activeFields (as well as all CompositeTypeSpec's that share other.activeFields),
     * all now share other.activeFields, and make sure that further sharing is still possible.
     */
    public void shareActiveFields(CompositeTypeSpec other) {
        if (other.activeFields != null && activeFields != other.activeFields) {
            TapList<CompositeTypeSpec> futureCopiesSharing = TapList.append(this.copiesSharingActiveFields, other.copiesSharingActiveFields);
            TapList<CompositeTypeSpec> inCopies = futureCopiesSharing;
            while (inCopies != null) {
                inCopies.head.copiesSharingActiveFields = futureCopiesSharing;
                inCopies.head.activeFields = other.activeFields;
                inCopies = inCopies.tail;
            }
        }
    }


    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    protected boolean isAnIOTypeSpec(SymbolTable symbolTable) {
        return isAnIOType;
    }

    @Override
    public boolean containsAPointer() {
        boolean contains = false;
        for (int i = fields.length - 1; i >= 0 && !contains; --i) {
            if (fields[i] != null && fields[i].type() != null && fields[i].type().containsAPointer()) {
                contains = true;
            }
        }
        return contains;
    }

    @Override
    public void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        for (int i = fields.length - 1; i >= 0; --i) {
            if (fields[i] != null && fields[i].type() != null) {
                fields[i].type().updateAfterImports(symbolTable, dejaVu);
            }
        }
    }

    @Override
    protected int computeSize() {
        int result = 0;
        for (FieldDecl field : fields) {
            if (field != null) {
                if (isRecordType()) {
                    result += field.type().size();
                } else {
                    result = Math.max(result, field.type().size());
                }
            }
        }
        if (result == 0) {
            return 1;
        } else {
            return result;
        }
    }

    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (!TapList.contains(toDejaVu, this)) {
            toDejaVu.placdl(this);
            for (FieldDecl field : fields) {
                if (field != null) {
                    field.collectUsedTrees(toUsedTrees, toDejaVu);
                }
            }
        }
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around an Composite* make little sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind:
        if (!(other instanceof CompositeTypeSpec)) {
            return false;
        }
        CompositeTypeSpec otherCompositeType = (CompositeTypeSpec) other;
        String thisName = typeDeclName();
        if (thisName == null) {
            thisName = name;
        }
        String otherName = otherCompositeType.typeDeclName();
        if (otherName == null) {
            otherName = otherCompositeType.name;
        }
        // immediate success if the two composite types have the same name:
        if (thisName != null && thisName.equals(otherName)) {
            return true;
        }
        // ... else we must look into the detail of components:
        boolean comparesWell = true;
        if (fields == null) {
            // if this's fields are unspecified, we may try type inference:
            if (testHasInference(comparison)) {
                if (otherCompositeType.fields != null) {
                    fields = new FieldDecl[otherCompositeType.fields.length];
                    System.arraycopy(otherCompositeType.fields, 0, fields, 0, fields.length);
                }
            } else {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (otherCompositeType.fields == null) {
            // if the other's fields are unspecified, we may try type inference:
            if (testHasInference(comparison)) {
                otherCompositeType.fields = new FieldDecl[fields.length];
                System.arraycopy(fields, 0, otherCompositeType.fields, 0, fields.length);
            } else {
                comparesWell = false;
            }
        } else {
            comparesWell = otherCompositeType.fields.length == fields.length;
            // Test identity of each fields:
            for (int i = fields.length - 1; comparesWell && i >= 0; --i) {
                if (fields[i] == null)
                // SPECIAL CASE: empty fields may happen in differentiated records:
                //  => accept equality between 2 empty fields !
                {
                    comparesWell = otherCompositeType.fields[i] == null;
                } else if (otherCompositeType.fields[i] == null) {
                    comparesWell = false;
                } else {
                    comparesWell = fields[i].symbol != null
                            && fields[i].type() != null
                            && otherCompositeType.fields[i].type() != null
                            && fields[i].symbol.equals(otherCompositeType.fields[i].symbol)
                            && fields[i].type().comparesWith(otherCompositeType.fields[i].type(),
                            testSetEquals(testUnsetInference(comparison)),
                            null, null, dejaVu);
                }
            }
        }
        return comparesWell;
    }

    /**
     * @return a copy of this composite type.
     */
    @Override
    public TypeSpec copy() {
        FieldDecl[] fieldsCopy = new FieldDecl[fields.length];
        System.arraycopy(fields, 0, fieldsCopy, 0, fields.length);
        CompositeTypeSpec result = new CompositeTypeSpec(null, fieldsCopy, kind(), null, null);
        // Share active fields info:
        result.shareActiveFields(this);
        result.setOrAddTypeDeclName(typeDeclName());
        return result;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            return false;
        }
        dejaVu = new TapList<>(this, dejaVu);
        boolean contains = false;
        for (int i = 0; !contains && i < fields.length; ++i) {
            if (fields[i] != null && fields[i].type() != null) {
                contains = fields[i].type().containsMetaType(dejaVu);
            }
        }
        return contains;
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        CompositeTypeSpec copiedResult = (CompositeTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            copiedResult =
                    new CompositeTypeSpec(name, new FieldDecl[fields.length], subKind, modifiers, definingUnit);
            // Share active fields info:
            copiedResult.shareActiveFields(this);
            copiedResult.unknownTypeSymbolHolder = unknownTypeSymbolHolder;
            TapList<TapPair<Unit, String>> oldTypeDeclNames = this.typeDeclNames();
            copiedResult.setTypeDeclNames(oldTypeDeclNames);
            copiedResult.setOrAddTypeDeclName(typeDeclName());
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            FieldDecl fieldDecl;
            FieldDecl copiedFieldDecl;
            ToBool subTypeContainsMeta;
            for (int i = fields.length - 1; i >= 0; --i) {
                fieldDecl = fields[i];
                if (fieldDecl == null) {
                    copiedResult.fields[i] = null;
                } else {
                    copiedFieldDecl = new FieldDecl(fieldDecl.symbol, null);
                    copiedResult.fields[i] = copiedFieldDecl;
                    copiedFieldDecl.setInitializationTree(fieldDecl.getInitializationTree());
                    copiedFieldDecl.setBitfieldTree(fieldDecl.bitfieldTree());
                    subTypeContainsMeta = new ToBool(false);
                    if (fieldDecl.type() != null) {
                        copiedFieldDecl.setType((WrapperTypeSpec) fieldDecl.type().localize(toAlreadyCopied, subTypeContainsMeta));
                    }
                    if (subTypeContainsMeta.get()) {
                        containsMeta.set(true);
                    }
                }
            }
            if (containsMeta.get())
            // If this type contains a meta, write it in the memo of already localized types:
            {
                alreadyRef.third = Boolean.TRUE;
            } else
            // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
            // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
            {
                alreadyRef.first = null;
            }
        }
        return copiedResult;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable,
                                                 int diffUnitSort, String diffTypeSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax,
                                                 String hintArrayNameInText, String hintArrayNameInIdent,
                                                 Tree hintArrayNameTree, Tree nameTree) {
// System.out.println("DIFFERENTIATE COMPOSITETYPESPEC:"+this+" activeFields:"+activeFields+" dirmax:"+multiDirDimensionMax) ;
        if (multiDirMode && TapEnv.relatedLanguageIsFortran()
            && multiDirDimensionMax!=null && multiDirDimensionMax.isUnknown()) {
            //lh/mv:2023-02-01: If we replaced "nbdirsmax" by ":" because we are inside a pointer,
            //                  we must reset back to "nbdirsmax" for inside the fields of this record:
            multiDirDimensionMax = new ArrayDim(ILUtils.buildDimColon(1, ILUtils.copy(TapEnv.get().fixedNbdirsmaxTree)),
                                                null, null, null, -1, 0, 0);;
        }
        FieldDecl[] differentiatedFields = new FieldDecl[fields.length];
        String renamedTypeDeclName = null;
        TypeDecl typeDecl = null;
        TypeDecl renamedTypeDecl = null;
        for (int i = fields.length - 1; i >= 0; i--) {
            differentiatedFields[i] = null;
        }
        CompositeTypeSpec diffCompositeTypeSpec =
                new CompositeTypeSpec(null, differentiatedFields, subKind, modifiers, null);
        if (this.name != null) {
            diffCompositeTypeSpec.name = TapEnv.extendStringWithSuffix(this.name, diffTypeSuffix);
        }
        diffTypeSpec = new WrapperTypeSpec(diffCompositeTypeSpec);
        if (typeDeclName() != null) {
            typeDecl = symbolTable.getTypeDecl(typeDeclName());
            if (typeDecl == null) {
                renamedTypeDeclName =
                        TypeSpec.getRenamedTypeDeclName(typeDeclNames(), symbolTable.origUnit());
                if (renamedTypeDeclName != null) {
                    renamedTypeDecl = symbolTable.getTypeDecl(renamedTypeDeclName);
                    typeDecl = renamedTypeDecl;
                } else {
                    if (definingUnit != null) {
                        typeDecl = definingUnit.publicSymbolTable().getTopTypeDecl(typeDeclName());
                        if (typeDecl == null) {
                            typeDecl = definingUnit.privateSymbolTable().getTopTypeDecl(typeDeclName());
                        }
                    }
                }
            }
            TypeDecl activeTypeDecl = null;
            SymbolTable definitionST = null;
            if (typeDecl != null && !typeDecl.isATrueSymbolDecl) {
                typeDecl = null;
                diffTypeSpec = null;
            }
            if (typeDecl != null) {
                NewSymbolHolder newSymbolHolder = typeDecl.getDiffSymbolHolder(0, null, 0);
                if (newSymbolHolder == null) {
                    ToObject<SymbolTable> toST = new ToObject<>(null);
                    symbolTable.getDecl(typeDecl.symbol, SymbolTableConstants.TYPE, false, toST);
                    definitionST = toST.obj();

                    activeTypeDecl =
                        typeDecl.activeTypeDecl(definitionST, diffTypeSpec, diffTypeSuffix);
                    newSymbolHolder = typeDecl.getDiffSymbolHolder(0, null, 0);
                    // TODO: This whole business of differentiating types must be rewritten!
                    if (typeDecl.importedFrom!=null) {
                        definitionST = typeDecl.importedFrom.second ;
                        typeDecl = (TypeDecl)typeDecl.importedFrom.first ;
                    }
                }
                if (newSymbolHolder != null) {
                    String typeDeclName = renamedTypeDeclName == null ? typeDecl.symbol : renamedTypeDecl.symbol;
                    ToObject<SymbolTable> toST = new ToObject<>(null);
                    symbolTable.getDecl(typeDeclName, SymbolTableConstants.TYPE, false, toST);
                    SymbolTable importST = toST.obj();
                    if (importST != null && importST.isImports) {
                        newSymbolHolder.addTypeDeclSymbolTable(importST);
                    }
                    newSymbolHolder.addTypeDeclSymbolTable(definitionST);
                    diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder = newSymbolHolder;
                }
                if (activeTypeDecl != null) {
                    activeTypeDecl.dependsOn = typeDecl.dependsOn;
                }
                diffCompositeTypeSpec.diffFromTypeDecl = typeDecl;
                for (int i = 0; i < fields.length; i++) {
                    FieldDecl fieldDecl = fields[i];
                    if ((i<activeFields.length && activeFields[i])
                        || !TapEnv.get().stripDiffTypes
                        || (!TapEnv.doActivity()
                            && TypeSpec.isDifferentiableType(fieldDecl.type()))) {
                        WrapperTypeSpec diffTypeSpecField =
                                fieldDecl.type().differentiateTypeSpecMemo(symbolTable, srcSymbolTable,
                                        diffUnitSort, diffTypeSuffix, false, multiDirMode, multiDirDimensionMax,
                                        hintArrayNameInText + "%" + fieldDecl.symbol,
                                        hintArrayNameInIdent + "_" + fieldDecl.symbol, hintArrayNameTree, null);
                        FieldDecl actFieldDecl = fieldDecl;
                        if (diffTypeSpecField != null) {
                            actFieldDecl = new FieldDecl(fieldDecl.symbol, diffTypeSpecField);
                            if (fieldDecl.initializationTree() != null) {
                                actFieldDecl.setInitializationTree(ILUtils.copy(fieldDecl.getInitializationTree()));
                            }
                        }
                        actFieldDecl.setExtraInfo(fieldDecl.extraInfo());
                        diffCompositeTypeSpec.fields[i] = actFieldDecl;
                    } else if (TapEnv.associationByAddress()) {
                        diffCompositeTypeSpec.fields[i] = fieldDecl;
                    }
                }
                typeDecl.setActive();

            }
        }
        if (diffTypeSpec != null && diffCompositeTypeSpec.isEmpty()) {
            if (typeDecl != null) {
                typeDecl.setActive(false);
            }
            diffTypeSpec = null;
        }
        return diffTypeSpec;
    }

    @Override
    protected void addDiffTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable) {
        TypeDecl typeDecl = null;
        TypeDecl renamedTypeDecl = null;
        if (typeDeclName() != null) {
            typeDecl = symbolTable.getTypeDecl(typeDeclName());
        }
        if (typeDecl == null && TapEnv.relatedLanguageIsFortran()) {
            String renamedTypeDeclName =
                    TypeSpec.getRenamedTypeDeclName(typeDeclNames(), symbolTable.origUnit());
            if (renamedTypeDeclName != null) {
                renamedTypeDecl = symbolTable.getTypeDecl(renamedTypeDeclName);
                typeDecl = renamedTypeDecl;
                if (typeDecl.importedFrom!=null) {
                    typeDecl = (TypeDecl)typeDecl.importedFrom.first ;
                }
            } else if (typeDeclName() != null) {
                typeDecl = symbolTable.getTypeDecl(typeDeclName());
            }
//             if (typeDecl != null && renamedTypeDeclName != null) {
//                 ToObject<SymbolTable> toST = new ToObject<>(null);
//                 symbolTable.getDecl(typeDecl.symbol, SymbolTableConstants.TYPE, false, toST);
//                 SymbolTable definitionST = toST.obj();
//                 if (definitionST != null && definitionST.isImports) {
//                     //recuperer ca: definitionST = definitionST.usedModule.publicSymbolTable();
//                 }
//                 assert definitionST != null;
//                 typeDecl = definitionST.getTypeDecl(typeDeclName());
//             }
        }
        if (typeDecl != null) {
            NewSymbolHolder newSymbolHolder = typeDecl.getDiffSymbolHolder(0, null, 0);
            if (newSymbolHolder != null) {
                String typeDeclName = renamedTypeDecl == null ? typeDecl.symbol : renamedTypeDecl.symbol;
                ToObject<SymbolTable> toST = new ToObject<>(null);
                symbolTable.getDecl(typeDeclName, SymbolTableConstants.TYPE, false, toST);
                SymbolTable importST = toST.obj();
                if (importST != null && importST.isImports) {
                    newSymbolHolder.addTypeDeclSymbolTable(importST);
                }
            }
        }
    }

    @Override
    protected void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
        if (diffInfos != null) {
            for (int i = 0; i < fields.length && diffInfos != null; ++i) {
                if (fields[i] != null) {
                    TypeSpec fieldTypeSpec = fields[i].type();
                    TapList fieldDiffInfos = (diffInfos.head instanceof TapList ? (TapList) diffInfos.head : diffInfos) ;;
                    boolean fieldIsActive = TapList.oneTrue(fieldDiffInfos);
                    if (i<activeFields.length && fieldIsActive) {
                        activeFields[i] = true;
                        fieldTypeSpec.cumulActiveParts(fieldDiffInfos, symbolTable);
                    }
                }
                diffInfos = diffInfos.tail;
            }
            // Mark named types so that there is a differentiated type name:
            if (typeDeclName() != null && needsADiffType(null)) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeDeclName());
                if (typeDecl != null) {
                    typeDecl.setActive(true);
                }
            }
        }
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            return false;
        }
        boolean diffNeeded = false;
        TypeSpec fieldTypeSpec;
        boolean oneFieldDiff = false;
        boolean oneFieldNoDiff = false;
        dejaVu = new TapList<>(this, dejaVu);
        for (int i = fields.length - 1; !diffNeeded && i >= 0; --i) {
            fieldTypeSpec = (fields[i] == null ? null : fields[i].type());
            if (fieldTypeSpec != null && i<activeFields.length) {
                if (activeFields[i]) {
                    diffNeeded = fieldTypeSpec.needsADiffType(dejaVu);
                    oneFieldDiff = true;
                } else {
                    oneFieldNoDiff = true;
                }
            }
        }
        return diffNeeded || (oneFieldDiff && oneFieldNoDiff) ;
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
// System.out.print("?ISDIFFERENTIATED "+this);
// for(int i=0 ; i<fields.length ; ++i) System.out.print(" "+i+":"+activeFields[i]) ;
// System.out.println();
        boolean isDiff = false;
        for (int i = fields.length - 1; !isDiff && i >= 0; --i) {
            isDiff = (i<activeFields.length && activeFields[i]) ;
        }
        return isDiff;
    }

    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        for (int i = fields.length - 1; i >= 0; i--) {
            if (fields[i] != null && fields[i].type() != null) {
                WrapperTypeSpec elementType = fields[i].type();
                if (elementType != null && elementType.wrappedType == this) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) Illegal recursive type definition " + this.showType());
                    return false;
                } else if (!TapList.contains(dejaVu, elementType)) {
                    dejaVu = new TapList<>(elementType, dejaVu);
                    assert elementType != null;
                    if (!elementType.checkTypeSpecValidity(dejaVu)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree result;
        Tree nameTree;
        if (name != null) {
            nameTree = ILUtils.build(ILLang.op_ident, name);
        } else {
            nameTree = ILUtils.build(ILLang.op_none);
        }
        boolean generateTypeName;
        if (!TapEnv.relatedLanguageIsFortran()) {
            generateTypeName = useShortNames && //inVariableDecl &&
                    (this.diffTypeDeclSymbolHolder != null && this.diffTypeSpec == null
                            || name != null || typeDeclName() != null);
        } else {
            generateTypeName = useShortNames; //=inVariableDecl;
        }
        if (generateTypeName) {
            if (this.diffTypeDeclSymbolHolder != null
                    && this.diffTypeSpec == null) {
                nameTree = diffTypeDeclSymbolHolder.makeNewRef(symbolTable);
            } else if (unknownTypeSymbolHolder != null) {
                nameTree = unknownTypeSymbolHolder.makeNewRef(symbolTable);
            } else {
                NewSymbolHolder diffSH =
                    (this.diffFromTypeDecl == null ? null : this.diffFromTypeDecl.getDiffSymbolHolder(0, null, 0));
                if (diffSH != null) {
                    boolean fromOtherCompilationUnit =
                            (symbolTable != null && TapEnv.isC(symbolTable.language())
                                    && diffSH.solvingRootIsOutsideFile(symbolTable));
                    nameTree = diffSH.makeNewRef(fromOtherCompilationUnit ? null : symbolTable);
                } else if (name != null) {
                    nameTree = ILUtils.build(ILLang.op_ident, name);
                } else {
                    nameTree = ILUtils.build(ILLang.op_ident, typeDeclName());
                }
            }
            result = nameTree;
        } else if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            result = ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            dejaVu = new TapList<>(this, dejaVu);
            if (isRecordType()) {
                result = ILUtils.build(ILLang.op_recordType,
                        nameTree, ILUtils.copy(modifiers),
                        ILUtils.build(ILLang.op_declarations));
            } else {
                result = ILUtils.build(ILLang.op_unionType,
                        nameTree,
                        ILUtils.build(ILLang.op_varDeclarations));
            }
            Tree declaration;
            WrapperTypeSpec fieldTypeSpec;
            int nbSons = 0;
            for (FieldDecl field : fields) {
                if (field != null) {
                    // fieldTypeSpec may be incorrect in the case (diffTypeSpec == typeSpec)
                    fieldTypeSpec = field.type();
                    fieldTypeSpec = fieldTypeSpec.equalsDiffTypeSpecAndTypeSpec();
                    WrapperTypeSpec sharedTypeSpec = fieldTypeSpec.equalsDiffTypeSpecAndTypeSpec();
                    if (!TapEnv.relatedLanguageIsFortran()) {
                        while (isA(sharedTypeSpec, SymbolTableConstants.ARRAYTYPE) ||
                                isA(sharedTypeSpec, SymbolTableConstants.POINTERTYPE)
                                        && ((PointerTypeSpec) sharedTypeSpec.wrappedType).offsetLength != null) {
                            if (isA(sharedTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                sharedTypeSpec = sharedTypeSpec.wrappedType.elementType();
                            } else {
                                sharedTypeSpec = ((PointerTypeSpec) sharedTypeSpec.wrappedType).destinationType;
                            }
                        }
                    }
                    Tree declarator = ILUtils.build(ILLang.op_ident, field.symbol);
                    declarator = addDeclaratorModifiers(declarator, field.accessInfo);
                    declarator = generateDeclaratorTree(sharedTypeSpec, null, declarator, fieldTypeSpec);
                    if (field.initializationTree() != null) {
                        declarator = ILUtils.copy(field.generateInitializationTree()); //bizarre that we don't use declarator !
                    } else if (field.bitfieldTree() != null) {
                        declarator = ILUtils.build(ILLang.op_bitfieldDeclarator,
                                declarator, field.bitfieldTree());
                    }
                    Tree sharedTypeTree =
                            sharedTypeSpec.generateTree(symbolTable, field.dependsOn(), shortNames, true, dejaVu);
                    if (field.extraInfo() != null) {
                        sharedTypeTree = ILUtils.build(ILLang.op_modifiedType,
                                ILUtils.build(ILLang.op_modifiers,
                                        ILUtils.buildListIdents(field.extraInfo())),
                                sharedTypeTree);
                    }
                    declaration = ILUtils.build(ILLang.op_varDeclaration,
                            ILUtils.build(ILLang.op_modifiers),
                            sharedTypeTree,
                            ILUtils.build(ILLang.op_declarators, declarator));
                    result.down(result.length()).setChild(declaration, nbSons + 1);
                    nbSons = nbSons + 1;
                }
            }
        }
        result.setAnnotation("WrapperTypeSpec", new WrapperTypeSpec(this));
        return result;
    }

    @Override
    public String showType() {
        //[llh] TODO: the place where the name is stored varies with language: clean this up!
        String shorthandName = typeDeclName();
        if (shorthandName == null) {
            String nameStrg = name == null ? " unnamed" : " " + name;
            if (isRecordType()) {
                if (TapEnv.relatedLanguageIsFortran()) {
                    return "derived TYPE " + nameStrg.toUpperCase();
                } else {
                    return "Record " + nameStrg;
                }
            } else {
                return "Union " + nameStrg;
            }
        } else {
            if (TapEnv.relatedLanguageIsFortran()) {
                return "derived TYPE " + shorthandName.toUpperCase();
            } else {
                return shorthandName;
            }
        }
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        StringBuilder fieldsString;
        if (fields == null) {
            fieldsString = new StringBuilder("??");
        } else {
            fieldsString = new StringBuilder("(");
            for (int i = 0; i < fields.length; ++i) {
                if (i > 0) {
                    fieldsString.append(',');
                }
                fieldsString.append(fields[i]==null
                                    ? '?'
                                    : (fields[i].symbol
                                       + (isDifferentiatedField(i)?"@":"")
                                       + (fields[i].accessInfo == null ? "" : " access:" + fields[i].accessInfo)
                                       + (fields[i].extraInfo() ==null ? "" : " X:"+fields[i].extraInfo()))) ;
            }
            fieldsString.append(')');
        }
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "(generated as:" + genTypeDeclName() + ")\":")
                + (isRecordType() ? "record" : "union") //+"@"+Integer.toHexString(hashCode())
                //+ (ILUtils.isNullOrNoneOrEmptyList(modifiers) ? "" : " "+modifiers)
                + (name == null ? " unnamed" : " " + name) + " of " + fieldsString ;
    }
}
