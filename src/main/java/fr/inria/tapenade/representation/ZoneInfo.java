/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;
import fr.inria.tapenade.utils.ToBool;

/**
 * Memory zone information.
 */
public final class ZoneInfo {

    private int kind = SymbolTableConstants.LOCAL;

    /**
     * for PARAMETER: index of the parameter (from 1 up),
     * for RESULT: 0
     * for GLOBAL: index of the (declared) zone
     * Otherwise undefined.
     */
    public int index;
    /**
     * The zone number of this zoneInfo.
     */
    public int zoneNb = -1;
    /**
     * The reduced zone number of this real-type zoneInfo.
     */
    public int realZoneNb = -1;
    /**
     * The reduced zone number of this int-type zoneInfo.
     */
    public int intZoneNb = -1;
    /**
     * The reduced zone number of this pointer-type zoneInfo.
     */
    public int ptrZoneNb = -1;
    /**
     * If this zoneInfo is for a pointer, contains a tree of (extended declared) zone numbers
     * which will be used to represent the (initial) destination of this pointer.
     */
    public TapList targetZonesTree;
    /**
     * Reverse info of "targetZonesTree". If the current ZoneInfo represents a part of
     * the (initial) destination of a pointer, this holds the ZoneInfo of this pointer.
     */
    public ZoneInfo targetZoneOf;
    /**
     * When this ZoneInfo is a copy of an original ZoneInfo, this holds
     * the original ZoneInfo. This happens e.g. when a ZoneInfo is a copy
     * built while copying a SymbolTable.
     */
    public ZoneInfo from;
    /**
     * The type of this zone. This can be a (modified) primitive type or pointer type.
     */
    public WrapperTypeSpec type;
    /**
     * The size modifier that applies to the type.
     */
    public int typeSizeModifier = -1;
    /**
     * True iff this ZoneInfo is a pointer to a location built with allocate,
     * and therefore a pointer which cannot be redirected elsewhere
     * (except by a deallocate() followed by a new allocate()).
     */
    public boolean isAllocatable;
    /**
     * The typical expression that accesses this zone, starting from some
     * visible variable (or common, or global...) name.
     * Having an access tree does not mean the zone is accessible: it may still be Hidden!
     */
    public Tree accessTree = null ;

    /**
     * A textual description of this zone, used e.g. in messages, comments, and logs
     * and that should have an acceptable meaning even if the zone is Hidden.
     */
    public String description = null ;

    /** True when this ZoneInfo is about a memory location that cannot be accessed from
     * the current scope, i.e. the SymbolTable from where this ZoneInfo was searched for,
     * given its rank. This can be the case for zones stored at the root SymbolTables or
     * stored in the zone arrays of the importsSymbolTable's. */
    public boolean isHidden = false ;

    /** True when this is either an MPI channel or an IO stream zone */
    public boolean isChannelOrIO = false ;

    /**
     * The type of the root of the accessTree.
     */
    public WrapperTypeSpec rootAccessType;
    /**
     * When this zone contains arrays, contains the list of their dimensions expressions.
     */
    public TapList<TapPair<Tree, Tree>> accessIndexes;
    /**
     * When this zone is in a COMMON, its memory offset inside this COMMON.
     */
    public int startOffset;
    public int endOffset;
    public boolean infiniteEndOffset;
    /**
     * When in a COMMON, name of the common.
     */
    public String commonName;

    /** When true, this zone represents several zones, and therefore an access to it is never total */
    public boolean multiple;

    /** When true, a single variable name is not enough to designate this ZoneInfo,
     * and therefore the name must be shown with indices. */
    public boolean ambiguousWithoutIndices = false ;

    /**
     * One of the SymbolDecl's that owns this zone.
     */
    public SymbolDecl ownerSymbolDecl;
    public Unit declarationUnit;
    /**
     * Will become true if at some point in execution of the adjoint code, this zone
     * is deallocated or falls out of scope, and this execution point is not immediately
     * followed by its reverse sweep (i.e. deallocation is diff-live). In that case this
     * zone's memory chunk will be effectively deallocated then re-allocated later to
     * a different memory location, and therefore every TBR push/poppointer pair possibly
     * pointing to this location must be rebased by a call to ADMM_rebase,
     * and also (independently of push/pops) every allocation of this zone must be
     * ADMM_register'ed so that size information are available for the re-allocation.
     */
    public boolean relocated;
    /**
     * Same as relocated, but concerning the adjoint differentiated zone of this zone.
     */
    public boolean relocatedDiff;
    /**
     * Will become true as soon as there exists a pointer that may point to this zone
     * and that may be restored by a call to poppointer() while this zone has beed relocated,
     * therefore requiring a call to ADMM_rebase. In that case, then this zone must be
     * registered at the time when it is allocated/declared by a call to ADMM_register
     * and unregistered when it is deallocated/out-of-scope by a call to ADMM_unregister.
     */
    public boolean rebased;
    /**
     * Same as rebased, but concerning the adjoint differentiated zone of this zone.
     */
    public boolean rebasedDiff;
    /**
     * List of names of variables intersecting this zone.
     */
    private TapList<String> variableNames;

    /**
     * Wrapper around a boolean so that every proxy copy of this ZoneInfo
     * shares the same "is once active" status.
     */
    private ToBool toOnceActive = new ToBool(false);


    public ZoneInfo(int startOffset, int endOffset, boolean infiniteEndOffset) {
        super();
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        this.infiniteEndOffset = infiniteEndOffset;
    }

    public static boolean eqOrCopyOfEq(ZoneInfo z1, ZoneInfo z2) {
        if (z1 == null || z2 == null) {
            return false;
        }
        if (z1.from != null) {
            z1 = z1.from;
        }
        if (z2.from != null) {
            z2 = z2.from;
        }
        return z1 == z2;
    }

//     /** @return true if this ZoneInfo is a message-passing channel. */
//     public boolean isMessagePassingChannel() {
//         return variableNames!=null && CallGraph.isMessagePassingChannelName(variableNames.head) ;
//     }

    /** Lists all zones in zonesTree, possibly including zones 0 and 1 (NULL and Undef) if they are in zonesTree.
     * @param zonesTree       The Tree to be explored: a tree of
     *                        TapIntList of (extended) zones numbers.
     * @param throughPointers when true, exploration of "zoneTree" goes into pointer derefs.
     * @return the list of all zones from "zonesTree", with no duplicates.
     */
    public static TapIntList listAllZonesWithNU(TapList<?> zonesTree, boolean throughPointers) {
        TapIntList toCollector = new TapIntList(-1, null);
        listAllZonesWithNURec(zonesTree, throughPointers, toCollector, new TapList<>(null, null));
        return toCollector.tail;
    }

    /**
     * Recursive definition for listAllZonesWithNU().
     */
    private static void listAllZonesWithNURec(TapList<?> zonesTree, boolean throughPointers,
                                        TapIntList toCollector, TapList<TapList> dejaVu) {
        while (zonesTree != null && !TapList.contains(dejaVu.tail, zonesTree)) {
            dejaVu.placdl(zonesTree);
            if (zonesTree.head instanceof TapList) { //recursive case:
                listAllZonesWithNURec((TapList) zonesTree.head, throughPointers, toCollector, dejaVu);
            } else { //TapIntList leaf case (possibly a pointer):
                TapIntList newZones = (TapIntList) zonesTree.head;
                while (newZones != null) {
                    toCollector.appendSortedIfAbsent(newZones.head);
                    newZones = newZones.tail;
                }
                if (!throughPointers) {
                    zonesTree = null; // "don't go through pointers" case.
                }
            }
            if (zonesTree != null) {
                zonesTree = zonesTree.tail;
            }
        }
    }

    /** Lists all zones in zonesTree, excluding zones 0 and 1 (i.e. NULL and Undef pointer destinations) */
    public static TapIntList listAllZones(TapList<?> zonesTree, boolean throughPointers) {
        TapIntList toCollector = new TapIntList(-1, null);
        listAllZonesRec(zonesTree, throughPointers, toCollector, new TapList<>(null, null));
        return toCollector.tail;
    }

    /**
     * Recursive definition for listAllZones().
     */
    private static void listAllZonesRec(TapList<?> zonesTree, boolean throughPointers,
                                        TapIntList toCollector, TapList<TapList> dejaVu) {
        while (zonesTree != null && !TapList.contains(dejaVu.tail, zonesTree)) {
            dejaVu.placdl(zonesTree);
            if (zonesTree.head instanceof TapList) { //recursive case:
                listAllZonesRec((TapList) zonesTree.head, throughPointers, toCollector, dejaVu);
            } else { //TapIntList leaf case (possibly a pointer):
                TapIntList newZones = (TapIntList) zonesTree.head;
                while (newZones != null) {
                    if (newZones.head>=2) {
                        toCollector.appendSortedIfAbsent(newZones.head);
                    }
                    newZones = newZones.tail;
                }
                if (!throughPointers) {
                    zonesTree = null; // "don't go through pointers" case.
                }
            }
            if (zonesTree != null) {
                zonesTree = zonesTree.tail;
            }
        }
    }

    public ZoneInfo copy() {
        ZoneInfo copyZI =
                new ZoneInfo(startOffset, endOffset, infiniteEndOffset);
        copyZI.kind = kind;
        copyZI.index = index;
        copyZI.zoneNb = zoneNb;
        copyZI.realZoneNb = realZoneNb;
        copyZI.intZoneNb = intZoneNb;
        copyZI.ptrZoneNb = ptrZoneNb;
        copyZI.targetZonesTree = targetZonesTree;
        copyZI.from = from != null ? from : this;
        copyZI.type = type;
        copyZI.typeSizeModifier = typeSizeModifier;
        copyZI.accessTree = accessTree;
        copyZI.description = description;
        copyZI.isAllocatable = isAllocatable;
        copyZI.isChannelOrIO = isChannelOrIO ;
        copyZI.isHidden = isHidden ;
        copyZI.ambiguousWithoutIndices = ambiguousWithoutIndices ;
        copyZI.multiple = multiple ;
        copyZI.rootAccessType = rootAccessType;
        copyZI.accessIndexes = accessIndexes;
        copyZI.commonName = commonName;
        copyZI.variableNames = variableNames;
        copyZI.ownerSymbolDecl = ownerSymbolDecl;
        copyZI.declarationUnit = declarationUnit;
        return copyZI;
    }

    public boolean isCommon() {
        return (kind==SymbolTableConstants.GLOBAL && commonName!=null) ;
    }

    public boolean isParameter() {
        return kind == SymbolTableConstants.PARAMETER;
    }

    public boolean isResult() {
        return kind == SymbolTableConstants.RESULT;
    }

    public boolean isGlobal() {
        return kind == SymbolTableConstants.GLOBAL;
    }

    public boolean isControl() {
        return kind == SymbolTableConstants.CONTROL;
    }

    /**
     * @return true if this zone, although declared locally (i.e. not a PARAMETER, RESULT, nor an official GLOBAL),
     * is remanent i.e. SAVE, or DATA or initialized in declaration (which imply SAVE).
     */
    public boolean isRemanentLocal() {
        return commonName != null && commonName.startsWith("/Fortran_Save_");
    }

    public static boolean same(ZoneInfo zi1, ZoneInfo zi2) {
        // This is somewhat approximate, but probably sufficient...
        return (zi1.kind==zi2.kind
                && (zi1.accessTree==null ? zi2.accessTree==null : zi1.accessTree.equals(zi2.accessTree))
                && (zi1.description==null ? zi2.description==null : zi1.description.equals(zi2.description))
                && (zi1.commonName==null ? zi2.commonName==null : zi1.commonName.equals(zi2.commonName))
                && (zi1.startOffset == zi2.startOffset)) ;
    }

    /**
     * @return kind of ZoneInfo, in {PARAMETER, LOCAL, RESULT, GLOBAL (for language C)}.
     */
    public int kind() {
        return kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    /**
     * @param whichKind kind of ZoneInfo.
     * @return reduced zone number.
     */
    public int kindZoneNb(int whichKind) {
        switch (whichKind) {
            case SymbolTableConstants.REALKIND:
                return realZoneNb;
            case SymbolTableConstants.INTKIND:
                return intZoneNb;
            case SymbolTableConstants.PTRKIND:
                return ptrZoneNb;
            default:
                break;
        }
        return zoneNb;
    }

    /**
     * Declare this ZoneInfo as differentiated (once-active).
     */
    public void setOnceActive() {
        toOnceActive.set(true) ;
        // if this zone is the target of a pointer zone,
        // then if this zone is once-active,
        // the pointer zone is also once-active.
        if (targetZoneOf != null) {
            targetZoneOf.toOnceActive.set((true)) ;
        }
    }

    public void shareActivity(ZoneInfo model) {
        toOnceActive = model.toOnceActive ;
    }

    public boolean isOnceActive() {
        return toOnceActive.get();
    }

    public boolean isConstant() {
        return ownerSymbolDecl != null && ownerSymbolDecl.isCconst();
    }

    protected void addVariableName(String varName) {
        if (!TapList.containsEquals(variableNames, varName)) {
            variableNames = new TapList<>(varName, variableNames);
        }
    }

    protected void setVariableNames(TapList<String> varNames) {
        variableNames = varNames;
    }

    public TapList<String> variableNames() {
        return variableNames;
    }

    public String bestVarName() {
        return (accessTree==null ? null : ILUtils.baseName(accessTree)) ;
    }

    public String publicName() {
        String accessTreeString = null ;
        if (!ILUtils.isNullOrNone(accessTree) &&
            (accessTree.opCode() != ILLang.op_arrayAccess ||
             !ILUtils.isNullOrNone(accessTree.down(1)) ||
             !(ILUtils.isNullOrNone(accessTree.down(2)) ||
               allTreesNull(accessTree.down(2).children())))) {
            accessTreeString = ILUtils.toString(accessTree);
        }
        switch (kind) {
            case SymbolTableConstants.PARAMETER:
                return "Arg"+index + (accessTreeString!=null ? ":"+accessTreeString : "") ;
            case SymbolTableConstants.RESULT:
                return "Result" +(accessTreeString!=null ? ":"+accessTreeString : "") ;
            case SymbolTableConstants.GLOBAL:
                return (accessTreeString!=null ? accessTreeString : description) ;
            default:
                return "??";
        }
    }

    public boolean containsOffset(int testedOffset) {
        return startOffset <= testedOffset && (infiniteEndOffset || testedOffset < endOffset);
    }

    private boolean allTreesNull(Tree[] trees) {
        boolean result = true;
        int i = trees.length - 1;
        Tree tree;
        while (result && i >= 0) {
            tree = trees[i];
            result = ILUtils.isNullOrNone(tree) ||
                    tree.opCode() == ILLang.op_dimColon &&
                            ILUtils.isNullOrNone(tree.down(1)) &&
                            ILUtils.isNullOrNone(tree.down(2)) ||
                    tree.opCode() == ILLang.op_arrayTriplet &&
                            ILUtils.isNullOrNone(tree.down(1)) &&
                            ILUtils.isNullOrNone(tree.down(2)) &&
                            ILUtils.isNullOrNone(tree.down(3));
            i--;
        }
        return result;
    }

    public String accessTreePrint(int language) {
        return ILUtils.toString(accessTree, language, ambiguousWithoutIndices);
    }

    public void setMultiple() {
        multiple = true;
    }

    public void setAmbiguousWithoutIndices() {
        ambiguousWithoutIndices = true ;
    }

    /**
     * @return True when this is a zone created by an allocate (aka malloc) instruction.
     * Testing is done by checking that the access tree contains op_allocate.
     */
    public boolean comesFromAllocate() {
        return accessTree != null && comesFromAllocateRec(accessTree);
    }

    private boolean comesFromAllocateRec(Tree inAccessTree) {
        switch (inAccessTree.opCode()) {
            case ILLang.op_allocate:
                return true;
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
                return comesFromAllocateRec(inAccessTree.down(1));
            default:
                return false;
        }
    }

    /** Return the access Tree to this zone, possibly modified to take into account
     * the various address manipulations that are made implicitly by cross-language calls. */
    public Tree crossLanguageAccessTree(CallArrow callArrow, ToBool toSkip, boolean onlyWhenTotal) {
        Tree zoneAccessTree = this.accessTree ;
        toSkip.set(false) ;
        if (callArrow.origin.isC() && callArrow.destination.isFortran()
            && !this.passesByValue(callArrow.destination, TapEnv.C)) {
            if (onlyWhenTotal) {
                // Patch not quite clear (cf set10/mix16): when C calls F passing a pointer,
                // F will touch the deref of this pointer, but not the next elements if
                // this is indeed a C array. Therefore the touch must not be considered total.
                toSkip.set(true) ;
            } else {
                zoneAccessTree = ILUtils.addPointerAccessAtBase(ILUtils.copy(zoneAccessTree));
            }
        } else if (callArrow.origin.isFortran() && callArrow.destination.isC()) {
            if (TypeSpec.isA(this.type, SymbolTableConstants.POINTERTYPE) &&
                (ILUtils.isNullOrNone(zoneAccessTree) || zoneAccessTree.opCode() == ILLang.op_ident)) {
                // When F calls C, call foo(x) sends some "address of x" to foo(T* arg).
                // We claim no information passes through this "address of x" associated with arg.
                // Information passes through x identified with *arg.
                toSkip.set(true) ;
            } else if (ILUtils.startsWithPointerDeref(zoneAccessTree)) {
                // Don't modify zoneAccessTree when F call foo(x) corresponds to C foo(T arg).
                zoneAccessTree = ILUtils.addAddressOfAtBase(ILUtils.copy(zoneAccessTree));
            }
        }
        return zoneAccessTree ;
    }

// TODO: might be redundant with boolean isHidden, with current strategy that fills all zones of importsSymbolTable.
//     /**
//      * @return true when it is not possible to write a reference expression that accesses this zone
//      * from the scope of the given SymbolTable.
//      * This is equivalent to, and should be replaced by, symbolTable.buildZoneVisibleAccessTree(this)==null
//      */
//     public boolean isHiddenFrom(SymbolTable symbolTable) {
//         ZoneInfo zi = this;
//         while (zi.targetZoneOf != null) {
//             zi = zi.targetZoneOf;
//         }
//         boolean hidden = true;
//         VariableDecl varDecl;
//         TapList<String> varNames = zi.variableNames;
//         String varName;
//         if (varNames == null && !zi.isHidden && zi.accessTree!=null) {
//             varNames = new TapList<>(ILUtils.baseName(zi.accessTree), null);
//         }
//         while (hidden && varNames != null) {
//             varName = varNames.head;
//             if (varName != null) {
//                 //[llh] TODO: me might consider extending this to FunctionDecl's...
//                 varDecl = symbolTable.getVariableDecl(varName);
//                 if (varDecl != null
//                     && TapIntList.contains(listAllZones(varDecl.zones(), true), zi.zoneNb)) {
//                     hidden = false;
//                 }
//             }
//             varNames = varNames.tail;
//         }
//         return hidden;
//     }

    public WrapperTypeSpec pointerDestType() {
        if (ptrZoneNb == -1) {
            return null;
        } else {
            return ((PointerTypeSpec) type.wrappedType).destinationType;
        }
    }

    public boolean passesByValue(Unit inUnit, int callerLanguage) {
        boolean result = false;
        if (isParameter() && (accessTree==null || !ILUtils.isAccessedThroughPointer(accessTree))) {
            int language = inUnit.language();
            switch (language) {
                case TapEnv.FORTRAN:
                case TapEnv.FORTRAN90:
                case TapEnv.FORTRAN2003:
                    result = (ownerSymbolDecl instanceof VariableDecl && ownerSymbolDecl.hasModifier("value")) ;
                    break;
                case TapEnv.C:
                case TapEnv.CUDA:
                case TapEnv.CPLUSPLUS:
                    result = (TapEnv.isC(callerLanguage)
                              && (
                                  ownerSymbolDecl==null   // For external units.
                                  ||
                                  (ownerSymbolDecl instanceof VariableDecl && !ownerSymbolDecl.isReference()))) ;
                    break;
                default:
                    TapEnv.printlnOnTrace("(passesByValue) unknown language " + language);
                    break;
            }
        }
        return result;
    }

    /**
     * When it is possible to find it (statically).
     *
     * @return the integer number of bytes
     * occupied by this zone. Otherwise returns -1.
     */
    public int knownSize(SymbolTable symbolTable) {
        int size = type == null ? -1 : type.size();
        Tree accTree = accessTree;
        while (size > 0 && accTree != null) {
            switch (accTree.opCode()) {
                case ILLang.op_arrayAccess:
                    Tree[] dims = accTree.down(2).children();
                    for (int i = dims.length - 1; size > 0 && i >= 0; --i) {
                        int dimSize = knownSize(dims[i], symbolTable);
                        size = dimSize <= 0 ? -1 : size * dimSize;
                    }
                    accTree = accTree.down(1);
                    break;
                case ILLang.op_pointerAccess:
                    if (!ILUtils.isNullOrNone(accTree.down(2))) {
                        int dimSize = knownSize(accTree.down(2), symbolTable);
                        size = dimSize <= 0 ? -1 : size * dimSize;
                    }
                    accTree = accTree.down(1);
                    break;
                case ILLang.op_fieldAccess:
                    accTree = accTree.down(1);
                    break;
                case ILLang.op_ident:
                    accTree = null;
                    break;
                default:
                    accTree = null;
                    size = -1;
                    break;
            }
        }
        return size;
    }

    private int knownSize(Tree arrayIndex, SymbolTable symbolTable) {
        if (arrayIndex.opCode() == ILLang.op_arrayTriplet) {
            Integer computed;
            int firstIndex = 1;
            if (!ILUtils.isNullOrNone(arrayIndex.down(1))) {
                computed = symbolTable.computeIntConstant(arrayIndex.down(1));
                if (computed != null) {
                    firstIndex = computed;
                }
            }
            int lastIndex = -1;
            if (!ILUtils.isNullOrNone(arrayIndex.down(2))) {
                computed = symbolTable.computeIntConstant(arrayIndex.down(2));
                if (computed != null) {
                    lastIndex = computed;
                }
            }
            int stride = 1;
            if (!ILUtils.isNullOrNone(arrayIndex.down(3))) {
                computed = symbolTable.computeIntConstant(arrayIndex.down(3));
                if (computed != null) {
                    stride = computed;
                }
            }
            if (stride == 1 && firstIndex != -1 && lastIndex != -1) {
                return lastIndex - firstIndex + 1;
            } else {
                return -1;
            }
        } else {
            return 1;
        }
    }

    /**
     * Prints in detail the contents of this ZoneInfo, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        String kindStr ;
        switch (kind) {
            case SymbolTableConstants.PARAMETER:
                kindStr = "parameter #" + index + " ";
                break;
            case SymbolTableConstants.LOCAL:
                kindStr = "local var ";
                break;
            case SymbolTableConstants.RESULT:
                kindStr = "function result ";
                break;
            case SymbolTableConstants.GLOBAL:
                kindStr = "global ";
                if (commonName!=null)
                    kindStr = kindStr+"(from common " + commonName + " [" + startOffset + "," + endOffset + "[)  ";
                break;
            case SymbolTableConstants.CONTROL:
                kindStr = "local control ";
                break;
            default:
                kindStr = "unknown kind ";
                break;
        }
        boolean passedByValue = false ;
        if (isParameter() && declarationUnit!=null && (accessTree==null || !ILUtils.isAccessedThroughPointer(accessTree))) {
            passedByValue = passesByValue(declarationUnit, declarationUnit.language());
        }
        return (isHidden ? "[HIDDEN] " : "")
            + (accessTree!=null ? ILUtils.toString(accessTree)+" " : "")
            + (description!=null ? "\""+description+"\" " : "")
            //+ " @" + Integer.toHexString(hashCode())
            + (isOnceActive() ? " @z:":" z:")+zoneNb
            + (kindZoneNb(SymbolTableConstants.INTKIND)==-1 ? "" : "/i"+kindZoneNb(SymbolTableConstants.INTKIND))
            + (kindZoneNb(SymbolTableConstants.PTRKIND)==-1 ? "" : "/p"+kindZoneNb(SymbolTableConstants.PTRKIND))
            + (kindZoneNb(SymbolTableConstants.REALKIND)==-1 ? "" : "/r"+kindZoneNb(SymbolTableConstants.REALKIND))
            + (targetZoneOf==null ? "" : " "+targetZoneOf.zoneNb + "-i>")
            + (targetZonesTree==null ? "" : " -i>" + targetZonesTree)
            + " "+kindStr
            + (variableNames == null ? "()" : variableNames.toString())
            + ": " + (multiple?"multi-":"") + (type == null ? "null_type" : type)
            + (passedByValue ? ", passed by value" : "")
            ;
    }

}
