/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.Tree;

import java.util.Objects;

/**
 * An array type.
 */
public final class ArrayTypeSpec extends TypeSpec {

    private WrapperTypeSpec elementType;

    public void setElementType(WrapperTypeSpec elementType) {
        this.elementType = elementType;
    }

    @Override
    public WrapperTypeSpec elementType() {
        return elementType;
    }

    /**
     * The dimensions of the array, in the C-like order i.e. slow dimensions first.
     */
    private ArrayDim[] dimensions;

    /**
     * @return The dimensions of the array, in the C-like order i.e. slow dimensions first.
     */
    public ArrayDim[] dimensions() {
        return dimensions;
    }

    /**
     * Set the dimensions of the array.
     *
     * @param dimensions The dimensions of the array, in the C-like order i.e. slow dimensions first.
     */
    public void setDimensions(ArrayDim[] dimensions) {
        this.dimensions = dimensions;
    }

    /**
     * Create an array type on "elementType" with "dimensions".
     */
    public ArrayTypeSpec(WrapperTypeSpec elementType, ArrayDim[] dimensions) {
        super(SymbolTableConstants.ARRAYTYPE);
        this.elementType = elementType;
        this.dimensions = dimensions;
    }

    /**
     * @return an array of dimensions from "dimsList".
     */
    public static ArrayDim[] createDimensions(TapList<ArrayDim> dimsList) {
        ArrayDim[] dimsArray = new ArrayDim[TapList.length(dimsList)];
        for (int i = 0; i < dimsArray.length; ++i) {
            dimsArray[i] = dimsList.head;
            dimsList = dimsList.tail;
        }
        return dimsArray;
    }

    /**
     * @return -1 if length is statically unknown, e.g. {@code ([3],[2:*])} returns -1.
     */
    public static int totalArrayLength(TapList<ArrayDim> arrayDims) {
        int totalLength = 1;
        int dimLength;
        while (totalLength != -1 && arrayDims != null) {
            dimLength = arrayDims.head.size();
            totalLength = dimLength <= 0 ? -1 : totalLength * dimLength;
            arrayDims = arrayDims.tail;
        }
        return totalLength;
    }

    protected TypeSpec combineArrayWith(TypeSpec newActualTypeSpec) {
        boolean fortranPointer = TapEnv.relatedLanguageIsFortran() && isA(newActualTypeSpec, SymbolTableConstants.POINTERTYPE);
        if (fortranPointer) {
            TapEnv.toolWarning(-1, "It is clearer to specify dimensions on the type declaration statements");
            newActualTypeSpec = ((PointerTypeSpec) newActualTypeSpec).destinationType.wrappedType;
        }
        TypeSpec sumActualTypeSpec;
        if (fortranPointer) {
            sumActualTypeSpec = this.combineWith(newActualTypeSpec, null);
        } else if (elementType == null || elementType.wrappedType == null) {
            sumActualTypeSpec = newActualTypeSpec;
        } else {
            sumActualTypeSpec =
                    elementType.wrappedType.combineWith(newActualTypeSpec, null);
        }
        if (fortranPointer) {
            return new PointerTypeSpec(new WrapperTypeSpec(sumActualTypeSpec), null);
        } else if (sumActualTypeSpec == null) {
            return null;
        } else {
            return new ArrayTypeSpec(new WrapperTypeSpec(sumActualTypeSpec), dimensions);
        }
    }

    /**
     * Transforms array[n1:n2][p1:p2] into array[(n2-n1+1)*(p2-p1+1)].
     */
    protected ArrayDim linearizeAllDim() {
        ArrayDim result;
        TapList<ArrayDim> allDims = getAllDimensions();
        int totalLower;
        int totalUpper;
        boolean infiniteUpper;
        int dimWidth;
        assert allDims != null;
        ArrayDim curArrayDim = allDims.head;
        if (curArrayDim.lower == null) {
            totalLower = 1;
        } else {
            totalLower = curArrayDim.lower;
        }
        if (curArrayDim.upper == null) {
            totalUpper = 0;
            infiniteUpper = true;
        } else {
            totalUpper = curArrayDim.upper;
            infiniteUpper = false;
        }
        allDims = allDims.tail;
        while (allDims != null) {
            curArrayDim = allDims.head;
            dimWidth = curArrayDim.size() - 1;
            if (infiniteUpper || dimWidth < 0) {
                infiniteUpper = true;
            } else {
                totalUpper =
                        dimWidth * (totalUpper - totalLower + 1) + totalUpper;
            }
            allDims = allDims.tail;
        }
        result =
                new ArrayDim(null, totalLower, infiniteUpper ? null : totalUpper, null, -1, 0, 0);
        return result;
    }

    /** Check the given array of array-dimensions "origDimensions" for problematic cases.
     * When necessary, builds and returns a new array of array-dimensions that is better
     * for the symbols that use these dimensions and for their declaration instruction.
     * Also returns a new array of dimensions when there is a vector dimension appended.
     * Modification cases:<br>
     * -- orig dimension was "*" and we are targetting F90 so it must become ":" <br>
     * -- orig dimension was unspecified and we are building the type of a local var. <br>
     * -- orig dimension is known only at run-time so we can't use it for declaration. <br>
     * @return the new array of array-dimensions, or none if the given array is fine. */
    public ArrayDim[] checkNoneDimensions(ArrayDim[] origDimensions, boolean localDecl,
                                          boolean addMultiDirDim, ArrayDim multiDirDimensionMax,
                                          String hintArrayNameInText, String hintArrayNameInIdent,
                                          Tree hintTreeSize, Tree nameTree, SymbolTable targetSymbolTable,
                                          NewSymbolHolder symbolHolder) {
        ArrayDim[] result;
        ArrayDim dimI;
        boolean different = false;
        boolean multiDirDimMustBeNone = false;
        assert targetSymbolTable != null;
        Unit targetUnit = targetSymbolTable.unit;
        int nDims = origDimensions.length;
        boolean targetF9x = (targetUnit != null && targetUnit.isFortran9x());
        if (addMultiDirDim) {
            different = true;
            result = new ArrayDim[nDims + 1];
        } else {
            result = new ArrayDim[nDims];
        }
        for (int i = nDims - 1; i >= 0; --i) {
            dimI = origDimensions[i];
            result[i] = dimI;
            if (dimI.isNone()) {
                multiDirDimMustBeNone = true;
                if (localDecl) {
                    different = true;
                    // We must prepare an ISIZEnOF... variable:
                    TapList<Tree> dummyExprs = null;
                    for (int j = nDims; j > 0; --j) {
                        dummyExprs = new TapList<>(ILUtils.build(ILLang.op_none), dummyExprs);
                    }
                    Tree dummyArrayTree = ILUtils.build(ILLang.op_arrayAccess, null,
                            ILUtils.build(ILLang.op_expressions, dummyExprs));
                    IterDescriptor dimIterDescriptor;
                    if (dimI.refQuality > 0 && TapEnv.get().noisize && targetUnit != null && targetUnit.isFortran()) {
                        // If possible, try generate a SIZE() call for this dimension of the array:
                        dimIterDescriptor = new IterDescriptor(0, targetSymbolTable, null, dummyArrayTree,
                                ILUtils.toString(dimI.refArrayTree), nameTree, dimI.refArrayTree, dimI.rankInRef, dimI.refNDims);
                    } else if (dimI.refQuality > 0 && ILUtils.getIdentString(dimI.refArrayTree) != null) {
                        // Else if generating a ISIZEOF, then try refer to the reference array in dimI:
                        dimIterDescriptor = new IterDescriptor(0, targetSymbolTable, null, null, dummyArrayTree,
                                ILUtils.toString(dimI.refArrayTree), ILUtils.getIdentString(dimI.refArrayTree), dimI.refArrayTree, dimI.rankInRef, dimI.refNDims);
                    } else if (hintArrayNameInIdent != null) {
                        // Else just create an ISIZEOF referring to the given hintArrayName:
                        dimIterDescriptor = new IterDescriptor(0, targetSymbolTable, null, null, dummyArrayTree,
                                hintArrayNameInText, hintArrayNameInIdent, hintTreeSize, i + 1, nDims);
                    } else {
                        // Else just create an ISIZEOF referring to the future variable name:
                        dimIterDescriptor = new IterDescriptor(0, targetSymbolTable, null, dummyArrayTree,
                                hintArrayNameInText, nameTree, hintTreeSize, i + 1, nDims);
                    }
                    if (targetUnit != null) {
                        dimIterDescriptor.preciseDefaultLengthTree(targetUnit, targetSymbolTable);
                    }
                    Tree isizeIdent = dimIterDescriptor.getLengthTree();
                    result[i] = new ArrayDim(ILUtils.build(ILLang.op_dimColon, null, isizeIdent), null, null,
                            dimI.refArrayTree, dimI.rankInRef, dimI.refNDims, dimI.refQuality);
                }
	    }
            if (origDimensions[i].tree() != null) {
                checkHiddenSize(origDimensions[i].tree().down(1), targetSymbolTable, symbolHolder);
                checkHiddenSize(origDimensions[i].tree().down(2), targetSymbolTable, symbolHolder);
            }
        }
        if (addMultiDirDim) {
            ArrayDim multiDirDim = multiDirDimensionMax;
            if (multiDirDimMustBeNone && targetF9x) {
                multiDirDim = new ArrayDim(null, null, null, null, -1, 0, 0);
            }
            result[nDims] = multiDirDim;
        }
        return different ? result : null;
    }

    /**
     * Tries to improve dimension info contained in this ArrayTypeSpec by
     * incorporating info from the given arrayAccessTree.
     */
    public void checkDimensions(Tree arrayAccessTree, SymbolTable symbolTable) {
        WrapperTypeSpec typeSpec = symbolTable.typeOf(arrayAccessTree.down(1));
        ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
        for (int i = 0; i < this.dimensions.length; i++) {
            if (arrayAccessTree.down(2).down(i + 1).opCode() == ILLang.op_arrayTriplet) {
                Tree arrayTriplet = arrayAccessTree.down(2).down(i + 1);
                if (this.dimensions[i].lower != null
                        && this.dimensions[i].lower.equals(arrayTypeSpec.dimensions[i].lower)
                        && this.dimensions[i].upper == null) {
                    if (arrayTypeSpec.dimensions[i].upper != null) {
                        this.dimensions[i].upper = arrayTypeSpec.dimensions[i].upper;
                        this.dimensions[i].setTree(arrayTypeSpec.dimensions[i].tree());
                    } else if (arrayTriplet.down(2).opCode() != ILLang.op_none) {
                        Tree sizeTree = ILUtils.addTree(
                                ILUtils.subTree(arrayTriplet.down(2), arrayTriplet.down(1)),
                                ILUtils.build(ILLang.op_intCst, 1));
                        this.dimensions[i].setTree(ILUtils.build(ILLang.op_dimColon,
                                ILUtils.build(ILLang.op_intCst, 1),
                                sizeTree));
                    }
                }
            }
        }
    }

    /**
     * cf set07/v507.
     */
    private void checkHiddenSize(Tree arrayDimExpr, SymbolTable symbolTable,
                                 NewSymbolHolder symbolHolder) {
        TapList<SymbolDecl> toUsedSymbols = new TapList<>(null, null);
        TapList<String> undeclaredSymbols = new TapList<>(null, null);
        SymbolDecl.addUsedSymbolsInExpr(arrayDimExpr, toUsedSymbols, symbolTable, undeclaredSymbols, false, true);
        if (symbolHolder != null && undeclaredSymbols.tail != null) {
            undeclaredSymbols = TapList.deleteString("#NBDirsMax#", undeclaredSymbols);
            undeclaredSymbols = TapList.deleteString("NBDirsMax", undeclaredSymbols);
            undeclaredSymbols = TapList.deleteContainsString("ISIZE", undeclaredSymbols);
            if (undeclaredSymbols.tail != null) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, arrayDimExpr, "(TC56) Declaration of " + symbolHolder.probableName()
                        + " uses hidden variable " + undeclaredSymbols.tail);
            }
        }
        if (symbolHolder != null && toUsedSymbols.tail != null) {
            TapList<SymbolDecl> usedSymbols = toUsedSymbols.tail;
            TapList<SymbolDecl> dependsOn = symbolHolder.dependsOn();
            TapList<SymbolDecl> dependsOnRec = null;
            while (dependsOn != null) {
                if (dependsOn.head != null) {
                    dependsOnRec = TapList.append(dependsOn.head.dependsOn(), dependsOnRec);
                }
                dependsOn = dependsOn.tail;
            }
            boolean hiddenDeclaration;
            while (dependsOnRec != null && dependsOnRec.head == null) {
                dependsOnRec = dependsOnRec.tail;
            }
            while (usedSymbols != null) {
                if (!usedSymbols.head.isA(SymbolTableConstants.FUNCTION)) {
                    if (dependsOnRec == null) {
                        SymbolDecl seenDecl = symbolTable.getVariableOrConstantDecl(usedSymbols.head.symbol);
                        if (seenDecl == null) {
                            TapList<FunctionDecl> seenDecls =
                                    symbolTable.getFunctionDecl(usedSymbols.head.symbol, null, null, false);
                            seenDecl = seenDecls == null ? null : seenDecls.head;
                        }
                        hiddenDeclaration = (seenDecl!=usedSymbols.head) ;
                    } else {
                        hiddenDeclaration = !TapList.containsEquals(dependsOnRec, usedSymbols.head);
                    }
                    if (hiddenDeclaration) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, arrayDimExpr, "(TC56) Declaration of " + symbolHolder.probableName()
                                + " uses hidden variable (" + usedSymbols.head.symbol + ')');
                    }
                }
                usedSymbols = usedSymbols.tail;
            }
        }
    }

    /**
     * Adds "size reference" info into this array type's ArrayDim. For each dimension's ArrayDim,
     * inserts the info (if not already present with a better quality) that this dimension's size is the size of the
     * corresponding dimension of variable "referenceName".
     */
    public void preciseSizeReferences(Tree refArrayTree, int newRefQuality) {
        if (dimensions != null && refArrayTree != null) {
            for (int i = dimensions.length - 1; i >= 0; --i) {
                if (dimensions[i].refQuality < newRefQuality) {
                    dimensions[i].refArrayTree = refArrayTree;
                    dimensions[i].rankInRef = i + 1;
                    dimensions[i].refQuality = newRefQuality;
                }
            }
        }
    }

    @Override
    public TypeSpec wrappedType() {
        return elementType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        elementType = type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type);
    }

    @Override
    public TypeSpec nestedLevelType() {
        return elementType;
    }

    @Override
    protected String baseTypeName() {
        return elementType == null ? null : elementType.baseTypeName();
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        if (elementType == null) {
            return null;
        } else {
            return elementType.baseTypeSpec(stopOnPointer);
        }
    }

    @Override
    public WrapperTypeSpec modifiedBaseTypeSpec() {
        if (elementType == null) {
            return null;
        } else {
            return elementType.modifiedBaseTypeSpec();
        }
    }

    @Override
    public boolean containsAPointer() {
        return elementType != null && elementType.containsAPointer();
    }

    @Override
    public TapList<ArrayDim> getAllDimensions() {
        TapList<ArrayDim> result = elementType == null ? null : elementType.getAllDimensions();
        for (int i = dimensions.length - 1; i >= 0; --i) {
            result = new TapList<>(dimensions[i], result);
        }
        return result;
    }

    @Override
    public boolean containsUnknownDimension() {
        // empty or 0-length dimensions means Unknown dimensions
        boolean result = (dimensions==null || dimensions.length==0) ;
        if (!result && elementType != null) {
            result = elementType.containsUnknownDimension();
        }
        for (int i = dimensions.length - 1; i >= 0 && !result; --i) {
            result = dimensions[i] == null || dimensions[i].isUnknown();
        }
        return result;
    }

    @Override
    public void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (elementType != null) {
            elementType.updateAfterImports(symbolTable, dejaVu);
        }
        for (int i = dimensions.length - 1; i >= 0; i--) {
            dimensions[i].updateImportedDim(symbolTable);
        }
    }

    @Override
    protected int computeSize() {
        int size = elementType.size();
        for (int i = dimensions.length - 1; i >= 0; i--) {
            if (dimensions[i] != null) {
                size *= dimensions[i].size();
            } else {
                TapEnv.toolWarning(-1, "null dimension in " + this);
            }
        }
        return size;
    }

    @Override
    public boolean isCharacter() {
        // We accept to view a 1-length String as a Character:
        return elementType != null && elementType.isCharacter()
                && dimensions.length == 1 && Objects.equals(dimensions[0].lower, dimensions[0].upper);
    }

    @Override
    public boolean isString() {
        return elementType != null && elementType.isCharacter() && dimensions.length == 1;
    }

    @Override
    public boolean isTarget() {
        return elementType.isTarget();
    }

    @Override
    public boolean isArray() {
        return true;
    }

    @Override
    public void addUsedSymbolsInType(TapList<SymbolDecl> toDependsOn, SymbolTable symbolTable) {
        for (ArrayDim dimension : dimensions) {
            SymbolDecl.addUsedSymbolsInExpr(dimension.tree(), toDependsOn, symbolTable, null, false, true);
        }
    }

    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (!TapList.contains(toDejaVu, this)) {
            toDejaVu.placdl(this);
            for (ArrayDim dimension : dimensions) {
                toUsedTrees.placdl(dimension.tree());
            }
            if (elementType != null && elementType.wrappedType != null) {
                elementType.wrappedType.collectUsedTrees(toUsedTrees, toDejaVu);
            }
        }
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        if (other != null && other.containsArray()) {
            // (layers of ModifiedTypeSpec's *around an Array* make little sense, so we will peel them too)
            toOther = peelWrapperAndModifiedTo(other, toOther);
        } else {
            toOther = peelWrapperTo(other, toOther);
        }
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison) && testIsEquals(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        boolean onStrings = isString();
        // else other must be an array too, except if vector notation is in use,
        // except also that we accept <String> RECEIVES <Character>.
        if (!(other instanceof ArrayTypeSpec ||
                onStrings ||
                testAllowsVectorial(comparison) && (TapEnv.relatedUnit() == null || TapEnv.relatedUnit().isFortran9x()))) {
            return false;
        }
        // peel all dimensions in one go, skipping ModifiedTypeSpec's (which have little sense) and WrapperTypeSpec's:
        TapList<ArrayDim> toThisDimensions = new TapList<>(null, null);
        TapList<ArrayDim> toOtherDimensions = new TapList<>(null, null);
        TypeSpec toThisElement = peelDimensionsTo(this, toThis, toThisDimensions);
        TypeSpec thisElement = toThisElement.wrappedType();
        toThisDimensions = toThisDimensions.tail;
        TypeSpec toOtherElement = peelDimensionsTo(other, toOther, toOtherDimensions);
        TypeSpec otherElement = toOtherElement != null ? toOtherElement.wrappedType() : other;
        toOtherDimensions = toOtherDimensions.tail;
        boolean comparesWell = true;

        // dimensions must match (except that we accept <String> RECEIVES <Character>) :
        if (toThisDimensions == null) {
            if (toOtherDimensions != null) {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (toOtherDimensions == null) {
            comparesWell = (testAcceptsUnspecified(comparison) || testAllowsVectorial(comparison))
                    && !testTypesLitteral(comparison)
                    && (onStrings || TapEnv.relatedUnit() == null || TapEnv.relatedUnit().isFortran9x()
                    || testTypesWithoutSize(comparison));
        } else {
            if (testIsReceives(comparison)) {
                // If we are comparing two Strings, we don't bother to check matching lengthes:
                if (!onStrings) {
                    // Complain only when both TOTAL sizes are statically known and are different i.e.:
                    // [3,5]/[15] is ok, [*]/[2,3] is ok, [2,*]/10 is ok, BUT [10]/[11] not ok and [2,5]/[11] not ok.
                    int totalLength = totalArrayLength(toThisDimensions);
                    int totalLengthOther = totalArrayLength(toOtherDimensions);
                    comparesWell = totalLength == -1 || totalLengthOther == -1 || totalLengthOther == totalLength;
                }
            } else {
                // when test is EQUALS, we want strict equality of dimensions:
                comparesWell = TapList.length(toThisDimensions) == TapList.length(toOtherDimensions);
                while (comparesWell && toThisDimensions != null) {
                    if (toThisDimensions.head == null) {
                        comparesWell = false;
                    } else if (testLooksDimensionLength(comparison)) {
                        comparesWell = toThisDimensions.head.equalLength(toOtherDimensions.head, comparison);
                    }
                    toThisDimensions = toThisDimensions.tail;
                    toOtherDimensions = toOtherDimensions.tail;
                }
            }
        }

        // compare element types:
        if (comparesWell) {
            if (thisElement == null) {
                // if this's elementType is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    toThisElement.setWrappedType(otherElement == null ? null : otherElement.weakenForInference(comparison));
                } else {
                    comparesWell = testAcceptsUnspecified(comparison);
                }
            } else if (otherElement == null) {
                // if other's elementType is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    toOtherElement.setWrappedType(thisElement);
                } else {
                    comparesWell = false;
                }
            } else {
                comparesWell =
                        thisElement.comparesWith(otherElement, comparison,
                                toThisElement, toOtherElement, dejaVu);
            }
        }
        return comparesWell;
    }

    @Override
    protected TypeSpec weakenForInference(int comparison) {
        ArrayDim[] weakenedDimensions = new ArrayDim[dimensions.length];
        for (int i = dimensions.length - 1; i >= 0; --i) {
            weakenedDimensions[i] = dimensions[i] == null ? null : dimensions[i].weakenForInference();
        }
        if (elementType == null) {
            return new ArrayTypeSpec(null, weakenedDimensions);
        } else {
            return new ArrayTypeSpec((WrapperTypeSpec) elementType.weakenForInference(comparison), weakenedDimensions);
        }
    }

    @Override
    public TypeSpec copy() {
        return new ArrayTypeSpec((WrapperTypeSpec) elementType.copy(), dimensions);
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        ArrayDim[] copiedDimensions = null;
        if (dimensions != null) {
            copiedDimensions = new ArrayDim[dimensions.length];
            for (int i = dimensions.length - 1; i >= 0; --i) {
                if (dimensions[i] == null) {
                    copiedDimensions[i] = null;
                } else {
                    copiedDimensions[i] = dimensions[i].copy();
                    if (publishedUnit != null) {
                        copiedDimensions[i].erasePrivateInfo(publishedUnit);
                    }
                }
            }
        }
        return new ArrayTypeSpec((WrapperTypeSpec) elementType.copyStopOnComposite(publishedUnit), copiedDimensions);
    }

    @Override
    public int precisionSize() {
        return elementType == null ? -1 : elementType.precisionSize();
    }

    @Override
    public Tree buildConstantZero() {
        Tree result = elementType.wrappedType.buildConstantZero();
        if (TapEnv.relatedLanguageIsC()) {
            for (int i = dimensions.length - 1; i >= 0; i--) {
                result = ILUtils.build(ILLang.op_arrayConstructor, result);
            }
        }
        return result;
    }

    @Override
    public Tree buildConstantOne() {
        return elementType.wrappedType.buildConstantOne();
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        boolean contains = dimensions == null || elementType.containsMetaType(dejaVu);
        if (dimensions != null) {
            for (int i = dimensions.length - 1; !contains && i >= 0; --i) {
                contains = dimensions[i] == null || dimensions[i].isUnknown();
            }
        }
        return contains;
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        ArrayTypeSpec copiedResult = (ArrayTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            ArrayDim[] copiedDimensions = new ArrayDim[dimensions.length];
            for (int i = dimensions.length - 1; i >= 0; --i) {
                copiedDimensions[i] = dimensions[i] == null ? null : dimensions[i].copy();
            }
            copiedResult = new ArrayTypeSpec(null, copiedDimensions);
            copiedResult.setOrAddTypeDeclName(typeDeclName());
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            if (elementType != null) {
                copiedResult.elementType = (WrapperTypeSpec) elementType.localize(toAlreadyCopied, containsMeta);
            }
            if (containsMeta.get()) {
                // If this type contains a meta, write it in the memo of already localized types:
                alreadyRef.third = Boolean.TRUE;
            } else {
                // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
                // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
                alreadyRef.first = null;
            }
        }
        return copiedResult;
    }

    @Override
    public TypeSpec preciseDimensions(TypeSpec complementType,
                                      TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, SymbolTable symbolTable) {
        if (!isA(complementType, SymbolTableConstants.ARRAYTYPE)) {
            return this;
        }
        ArrayTypeSpec precisedType = (ArrayTypeSpec) TapList.cassq(this, dejaVu);
        if (precisedType == null) {
            precisedType = new ArrayTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, precisedType));
            WrapperTypeSpec precisedElementType =
                    WrapperTypeSpec.preciseDimensions(elementType, ((ArrayTypeSpec) complementType).elementType,
                            dejaVu, symbolTable);
            boolean oneDimensionPrecised = false;
            ArrayDim[] complementDimensions = ((ArrayTypeSpec) complementType).dimensions;
            ArrayDim[] precisedDimensions = null;
            ArrayDim precisedDimension;
            if (dimensions != null && complementDimensions != null && dimensions.length == complementDimensions.length) {
                precisedDimensions = new ArrayDim[dimensions.length];
                for (int i = dimensions.length - 1; i >= 0; --i) {
                    precisedDimension = ArrayDim.preciseDimension(dimensions[i], complementDimensions[i],
                            symbolTable);
                    if (precisedDimension == dimensions[i] || precisedDimension == null) {
                        precisedDimensions[i] = dimensions[i];
                    } else {
                        precisedDimensions[i] = precisedDimension;
                        oneDimensionPrecised = true;
                    }
                }
            }
            if (!oneDimensionPrecised) {
                precisedDimensions = dimensions;
            }
            if (precisedElementType == elementType && !oneDimensionPrecised
                    || precisedElementType.wrappedType == null || precisedDimensions == null) {
                precisedType = this;
            } else {
                precisedType.elementType = precisedElementType;
                precisedType.dimensions = precisedDimensions;
            }
        }
        return precisedType;
    }

    @Override
    public boolean containsUndefinedType() {
        return
                elementType == null || elementType.wrappedType == null
                        || elementType.wrappedType.containsUndefinedType();
    }

    @Override
    protected TypeSpec addMultiDirDimension(ArrayDim dimension) {
        ArrayDim[] newDimensions = new ArrayDim[dimensions.length + 1];
        boolean multiDirDimMustBeNone = false;
        for (int i = dimensions.length - 1; i >= 0; --i) {
            newDimensions[i] = dimensions[i];
            if (dimensions[i].isNone()) {
                multiDirDimMustBeNone = true;
            }
        }
        if (TapEnv.relatedLanguageIsFortran9x() && multiDirDimMustBeNone) {
            dimension = new ArrayDim(null, null, null, null, -1, 0, 0);
        }
        newDimensions[dimensions.length] = dimension;
        return new ArrayTypeSpec(elementType, newDimensions);
    }

    @Override
    public TypeSpec intToReal(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                              SymbolTable symbolTable) {
        ArrayTypeSpec convertedType = (ArrayTypeSpec) TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            convertedType = new ArrayTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedElementType =
                    WrapperTypeSpec.intToReal(elementType, dejaVu, symbolTable);
            if (convertedElementType == elementType || convertedElementType.wrappedType == null) {
                convertedType = this;
            } else {
                convertedType.elementType = convertedElementType;
                convertedType.dimensions = dimensions;
            }
        }
        return convertedType;
    }

    @Override
    protected TypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        ArrayTypeSpec convertedType = (ArrayTypeSpec) TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            convertedType = new ArrayTypeSpec(null, null);
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedElementType =
                    elementType.realToComplex(dejaVu, complexTypeSpec);
            if (convertedElementType == elementType || convertedElementType.wrappedType == null) {
                convertedType = this;
            } else {
                convertedType.elementType = convertedElementType;
                convertedType.dimensions = dimensions;
            }
        }
        return convertedType;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        boolean addMultiDirDimensionHere = false;
// System.out.println("DIFFERENTIATE ARRAY TYPE:"+this+" TOSCALAR:"+elementType.isScalar()) ;
        if (multiDirMode && elementType.isScalar()/* !isA(elementType, SymbolTableConstants.COMPOSITETYPE)*/) {
            addMultiDirDimensionHere = true;
            multiDirMode = false;
        }
        WrapperTypeSpec differentiatedElementType =
                elementType.differentiateTypeSpecMemo(symbolTable, srcSymbolTable, diffUnitSort, fSuffix, localDecl,
                        multiDirMode, multiDirDimensionMax,
                        hintArrayNameInText + "()", hintArrayNameInIdent, hintArrayNameTree, nameTree);
        ArrayDim currentMultiDirDim = multiDirDimensionMax;
        ArrayDim[] diffDimensions =
                checkNoneDimensions(dimensions, localDecl, addMultiDirDimensionHere, currentMultiDirDim,
                        hintArrayNameInText, hintArrayNameInIdent, hintArrayNameTree, hintArrayNameTree,
                        symbolTable, null);
        if (differentiatedElementType == null && diffDimensions == null) {
            // then the diff type is simply the original type:
            return null;
        } else {
            // else the diff type is different from original type. Build it and return it:
            WrapperTypeSpec result = new WrapperTypeSpec(
                    new ArrayTypeSpec(
                            differentiatedElementType == null ? elementType : differentiatedElementType,
                            diffDimensions == null ? dimensions : diffDimensions));
            if (TapEnv.associationByAddress()) {
                diffTypeSpec = result;
            }
            return result;
        }
    }

    @Override
    protected void addDiffTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable) {
        elementType.addRefDiffTypeSpec(symbolTable, srcSymbolTable);
    }

    @Override
    public boolean acceptsMultiDirDimension() {
        return (elementType!=null && elementType.acceptsMultiDirDimension()) ;
    }

    @Override
    protected void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
        if (diffInfos != null && elementType != null) {
            elementType.cumulActiveParts(diffInfos, symbolTable);
            // Mark named types so that there is a differentiated type name:
            if (typeDeclName() != null && needsADiffType(null)) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeDeclName());
                if (typeDecl != null) {
                    typeDecl.setActive(true);
                }
            }
        }
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return elementType != null && elementType.needsADiffType(dejaVu);
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return elementType != null && elementType.isDifferentiated(dejaVu);
    }

    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        if (elementType != null && elementType.wrappedType == this) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) Illegal recursive type definition " + this.showType());
            return false;
        } else if (elementType != null) {
            return elementType.checkTypeSpecValidity(dejaVu);
        } else {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) null array element type " + this.showType());
            return false;
        }
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            return ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            Tree typeTree;
            if (elementType != null) {
                dejaVu = new TapList<>(this, dejaVu);
                typeTree = elementType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
            } else {
                TapEnv.toolWarning(-1, "(ArrayTypeSpec regeneration) null wrapped type");
                typeTree = ILUtils.build(ILLang.op_ident, "UnknownType");
            }
            Tree[] dimensionTrees = new Tree[dimensions.length];
            for (int i = dimensions.length - 1; i >= 0; --i) {
                ArrayDim dimension = dimensions[i] ;
                Tree dimensionTree = null ;
                if (dimension!=null) {
                    dimensionTree = (dimension.treeForDeclaration!=null
                                     ? dimension.treeForDeclaration
                                     : dimension.tree()) ;
                }
                dimensionTrees[i] = ILUtils.copy(dimensionTree) ;
            }
            return ILUtils.build(ILLang.op_arrayType,
                    typeTree,
                    ILUtils.build(ILLang.op_dimColons, dimensionTrees));
        }
    }

    public String showDimensions() {
        boolean fortranStyle = TapEnv.relatedUnit() != null && TapEnv.isFortran(TapEnv.relatedUnit().language());
        StringBuilder dims = new StringBuilder(fortranStyle ? "(" : "[");
        if (fortranStyle) {
            // Translation from Fortran to IL has reversed the order of array indices.
            // When showing them, we must use their original "Fortran-style" order:
            for (int i = dimensions.length - 1; i >= 0; --i) {
                dims.append(dimensions[i] == null ? "?" : dimensions[i].showType());
                if (i > 0) {
                    dims.append(',');
                }
            }
        } else {
            for (int i = 0; i < dimensions.length; ++i) {
                if (i > 0) {
                    dims.append(',');
                }
                dims.append(dimensions[i] == null ? "?" : dimensions[i].showType());
            }
        }
        return dims + (fortranStyle ? ")" : "]");
    }

    @Override
    public String showType() {
        return (elementType == null ? "?" : elementType.showType())
                + (dimensions == null ? "??" : showDimensions());
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        String dimsString;
        if (dimensions == null) {
            dimsString = "??";
        } else {
            boolean fortranStyle = TapEnv.relatedUnit() != null && TapEnv.isFortran(TapEnv.relatedUnit().language());
            StringBuilder dims = new StringBuilder(fortranStyle ? "(" : "[");
            if (fortranStyle) {
                // Translation from Fortran to IL has reversed the order of array indices.
                // When showing them, we must use their original "Fortran-style" order:
                for (int i = dimensions.length - 1; i >= 0; --i) {
                    dims.append(dimensions[i] == null ? "?" : dimensions[i]);
                    if (i > 0) {
                        dims.append(',');
                    }
                }
            } else {
                for (int i = 0; i < dimensions.length; ++i) {
                    if (i > 0) {
                        dims.append(',');
                    }
                    dims.append(dimensions[i] == null ? "?" : dimensions[i]);
                }
            }
            dimsString = dims + (fortranStyle ? ")" : "]");
        }
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") //+ "array" +"@"+Integer.toHexString(hashCode())
                + dimsString + " of "
                + (elementType == null ? "?" : elementType.toString());
    }
}
