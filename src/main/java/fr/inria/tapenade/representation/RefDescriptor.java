/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

// If modifying code for this class, relevant nonRegression tests may be:
// set03/lh062 set04/lh108 set06/v322 set06/v324 set06/v325 set06/v350 set12/lh[10-12]
/**
 * Holds a description of all components of a given
 * expression Tree. Tree is most often a reference.
 * <p>
 * "components" means values contained or accessed through the tree.
 * The RefDescriptor finds and analyzes all the iterations contained
 * in the "tree", using the type information available.
 * <p>
 * The RefDescriptor chooses the way to access to all these components
 * for a given sort of "use", e.g. a PUSH-POP, an initialization, an I-O,...
 * (examples:
 * In order to initialize A(3,:), one can use vector notation for the ":".
 * In order to PUSH A(:)%x(:), the two ":" cannot use vector notation together.)
 * <p>
 * Then after the "use" is chosen, the RefDescriptor can generate PUSH-POP
 * or initialization or other standard operations working on this "tree".
 * Two RefDescriptor's can be merged, thus sharing the iterative loops
 * when performing some operations.
 * <p>
 * NOTE: when the given expression Tree contains an array index or a C
 * pointer-array index which is an op_dimColon instead of an op_arrayTriplet,
 * then we consider this is a full access to this particular dimension,
 * therefore sometimes simplifying the returned access instructions.
 */
public final class RefDescriptor {

    private static final int PUSHPOP = 1;
    private static final int CALL = 2;
    private static final int INIT = 3;
    private static final int ASSIGN = 4;
    /**
     * The type of "tree" seen as a whole.
     */
    public WrapperTypeSpec completeType;
    /**
     * The type of the elements of "tree".
     */
    public WrapperTypeSpec elementType;
    public Tree altRootIdent;
    /**
     * True when this RefDescriptor will access a pointer, i.e.
     * in the PUSH/POP case, it will call PUSH/POPPOINTERn().
     * This flag is set during createNestedLoopTree().
     */
    public boolean holdsAPointer;
    /**
     * If true, the generated code may contain IF (ALLOCATED/ASSOCIATED(...)) tests
     * to protect from access to unallocated memory.
     */
    public boolean mayProtectAccesses = true;
    /**
     * Because at one place, we need the variable used by the adjoint code
     * to store the POP'ped branch. This field must be shared with the
     * "toBranchVariable" of class FlowGraphDifferentiator.
     */
    public TapPair<NewSymbolHolder, WrapperTypeSpec> toBranchVariable;

    /**
     * When this RefDescriptor was built for a complete zone (instead of for a precise Tree),
     * holds the ZoneInfo of the zone.
     */
    public ZoneInfo zoneInfo;
    /**
     * The SymbolTable of the given tree.
     */
    private final SymbolTable defSymbolTable;
    /**
     * The SymbolTable where created trees will be used.
     */
    private final SymbolTable usageSymbolTable;
    /**
     * The SymbolTable where will be declared the necessary new indices (ii1,ii2...).
     */
    private final SymbolTable otherUsageSymbolTable;
    /**
     * The Unit which will contain the created trees.
     */
    private Unit targetUnit;
    /**
     * The "integer" type of "defSymbolTable".
     */
    private final WrapperTypeSpec integerTypeSpec;
    /**
     * A copy of the given "tree", annotated for easier
     * building of the required operations on it.
     */
    private Tree patternTree;
    /**
     * In the special case where the root name of the given "tree"
     * is not declared, we can put its WrapperTypeSpec here.
     */
    private WrapperTypeSpec knownRootTypeSpec;
    /**
     * When non-null, instructs the RefDescriptor to take this WrapperTypeSpec
     * as the type of the model Tree.
     */
    private WrapperTypeSpec forceRootTypeSpec;
    /**
     * Memo of all IterDescriptors used by this RefDescriptor.
     */
    private IterDescriptor[] itersMemo;
    private int itersMemoNextRk;
    /**
     * When non-null, holds the iterator for the multi-directional dimension.
     */
    private final IterDescriptor multiDirIterator;
    /**
     * True when the given tree is of (or must be turned into) differentiated type.
     * This is used to avoid creating references to non-active structure fields.
     */
    private final boolean isDiff;
    private Tree nonDiffRootIdent;
    private Tree diffRootIdent;
    /**
     * The tree of nested iterations, built after the choice
     * of the iteration sort for each IterDescriptor used,
     * i.e. after the choice of the usage of this RefDescriptor
     * (PUSH/POP, initialize, assign...).
     */
    private RefDescriptor.NestedLoopNode nestedLoopTree;

    /**
     * When greater than 2, indicates this RefDescriptor must create
     * calls to PUSH/POPCONTROL(n, nbControls-1).
     */
    private int nbControls;
    /**
     * When true, it is impossible to refer to this complete array
     * as a whole, in the manner that is usual in F77, i.e. with index "1".
     * Example: one can't write call FOO(A(i,1)) to pass A(i,1:100).
     */
    private boolean scalarIndices;

    /**
     * For the special case where this RefDescriptor is used together
     * with another one (e.g. for an increment, assign, or a storage/retrieval implemented
     * using a local storage variable, contains the other RefDescriptor.
     */
    private RefDescriptor companionVarDescriptor;
    /**
     * Number used to label matching PUSH and POPs.
     */
    private int ppLabel = -1;
    /**
     * Because at one place we need to know whether this will be
     * used to build a PUSH-POP or not.
     */
    private int futureUse = -1;
    /**
     * The Tree that must be used each time a variable must be created to
     * represent an unknown size.
     */
    private Tree hintRootTree;
    /**
     * The Tree that must be used each time an unknown size must be computed
     * with SIZE(hintArrayTreeForCallSize,).
     */
    private Tree hintArrayTreeForCallSize;

    /**
     * Creation of the RefDescriptor for the object given by "tree".
     * Extended primitive that copes with the weird case:
     * call TOTO(T(i)) with SUBROUTINE TOTO(array[1:N])
     * Also copes with new variables introduced by split:
     * they may have no type therefore we use the formal type instead.
     * When zoneInfo is passed non-null, it means that this RefDescriptor
     * designates exactly the full contents of this zone.
     */
    public RefDescriptor(Tree tree, ZoneInfo zoneInfo, WrapperTypeSpec type, WrapperTypeSpec expectedType, Tree hintRootTree,
                         SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                         SymbolTable otherUsageSymbolTable, Tree altRootIdent, boolean isDiff,
                         IterDescriptor multiDirIterator, Unit targetUnit) {
        super();
        this.zoneInfo = zoneInfo;
        this.defSymbolTable = defSymbolTable;
        this.usageSymbolTable = usageSymbolTable;
        this.otherUsageSymbolTable = otherUsageSymbolTable;
        this.integerTypeSpec = defSymbolTable.getTypeDecl("integer").typeSpec;
        this.targetUnit = targetUnit;
        this.multiDirIterator = multiDirIterator;
        this.isDiff = isDiff;
        this.altRootIdent = altRootIdent;
        if (type == null) {
            this.knownRootTypeSpec = expectedType;
        } else {
            tree = explicitWhenImplicitArray(tree, type, expectedType,
                    defSymbolTable);
        }
        this.hintRootTree = hintRootTree;
        initialize(tree, altRootIdent);
    }

    /**
     * Creation of the RefDescriptor for the object accessed by "tree",
     * giving explicitly the WrapperTypeSpec that must be taken for this tree.
     */
    public RefDescriptor(Tree tree, WrapperTypeSpec forcedType, Tree hintRootTree,
                         SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                         SymbolTable otherUsageSymbolTable, Tree altRootIdent, boolean isDiff,
                         IterDescriptor multiDirIterator, Unit targetUnit) {
        super();
        this.defSymbolTable = defSymbolTable;
        this.usageSymbolTable = usageSymbolTable;
        this.otherUsageSymbolTable = otherUsageSymbolTable;
        this.integerTypeSpec = defSymbolTable.getTypeDecl("integer").typeSpec;
        this.targetUnit = targetUnit;
        this.multiDirIterator = multiDirIterator;
        this.isDiff = isDiff;
        this.altRootIdent = altRootIdent;
        this.forceRootTypeSpec = forcedType;
        this.hintRootTree = hintRootTree;
        initialize(tree, altRootIdent);
    }

    /**
     * Creation of the RefDescriptor for the object accessed by "tree".
     */
    public RefDescriptor(Tree tree, Tree hintRootTree,
                         SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                         SymbolTable otherUsageSymbolTable, Tree altRootIdent, boolean isDiff,
                         IterDescriptor multiDirIterator, Unit targetUnit) {
        super();
        this.defSymbolTable = defSymbolTable;
        this.usageSymbolTable = usageSymbolTable;
        this.otherUsageSymbolTable = otherUsageSymbolTable;
        this.integerTypeSpec = defSymbolTable.getTypeDecl("integer").typeSpec;
        this.targetUnit = targetUnit;
        this.multiDirIterator = multiDirIterator;
        this.isDiff = isDiff;
        this.altRootIdent = altRootIdent;
        this.hintRootTree = hintRootTree;
        initialize(tree, altRootIdent);
    }

    /**
     * Creation of the RefDescriptor for the object given by "zoneInfo".
     */
    public RefDescriptor(ZoneInfo zoneInfo, Tree zoneInfoVisibleAccessTree,
                         SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                         SymbolTable otherUsageSymbolTable, Tree altRootIdent, boolean isDiff,
                         IterDescriptor multiDirIterator, Unit targetUnit) {
        super();
        this.zoneInfo = zoneInfo;
        this.defSymbolTable = defSymbolTable;
        this.usageSymbolTable = usageSymbolTable;
        this.otherUsageSymbolTable = otherUsageSymbolTable;
        this.integerTypeSpec = defSymbolTable.getTypeDecl("integer").typeSpec;
        this.targetUnit = targetUnit;
        this.multiDirIterator = multiDirIterator;
        this.isDiff = isDiff;
        this.altRootIdent = altRootIdent;
        this.hintArrayTreeForCallSize = zoneInfoVisibleAccessTree;
        initialize(zoneInfoVisibleAccessTree, altRootIdent);
    }

    /**
     * Replaces actual argument T(6) as T(6:) when the formal argument is declared with dimensions.
     */
    public static Tree explicitWhenImplicitArray(Tree tree, WrapperTypeSpec type,
                                                 WrapperTypeSpec expectedType, SymbolTable symbolTable) {
        if (tree.opCode() == ILLang.op_arrayAccess) {
            TapList<ArrayDim> dimensions = type.getAllDimensions();
            TapList<ArrayDim> expectedDimensions = expectedType.getAllDimensions();
            if (dimensions == null && expectedDimensions != null) {
// [llh] Can't remember why this was done, but it breaks set11/lh060 (and MIT-GCM!)
//                 WrapperTypeSpec arrayType = symbolTable.typeOf(tree.down(1));
//                 TapList<ArrayDim> arrayDimensions = arrayType.getAllDimensions();
//                 if (TapList.length(arrayDimensions) > TapList.length(expectedDimensions)) {
//                     expectedDimensions = arrayDimensions;
//                 }
                int size = 1;
                int dimSize;
                ArrayDim extraDimension;
                Tree index1;
                while (expectedDimensions != null && size != -1) {
                    extraDimension = expectedDimensions.head;
                    dimSize = extraDimension.size();
                    if (dimSize == -1) {
                        size = -1;
                    } else {
                        size = size * dimSize;
                    }
                    expectedDimensions = expectedDimensions.tail;
                }
                tree = ILUtils.copy(tree);
                int rank = 1;
                if (TapEnv.relatedLanguageIsFortran()) {
                    rank = tree.down(2).length();
                }
                index1 = tree.down(2).cutChild(rank);
                tree.down(2).setChild(ILUtils.build(ILLang.op_arrayTriplet,
                        index1,
                        size == -1 ?
                                ILUtils.build(ILLang.op_none) :
                                ILUtils.addTree(ILUtils.copy(index1),
                                        ILUtils.build(ILLang.op_intCst, size - 1)),
                        ILUtils.build(ILLang.op_none)),
                        rank);
                tree.down(2).down(rank).setAnnotation("subArrayAccess", Boolean.TRUE);
            }
        }
        return tree;
    }

    /**
     * Limitation: so far, we can handle
     * ONLY ONE expression in the list of exprs (1st son).
     */
    private static Tree turnIterVarRefIntoTriplets(Tree tree) {
        TapList<Tree> dos = null;
        while (tree.opCode() == ILLang.op_iterativeVariableRef) {
            dos = new TapList<>(tree.down(2), dos);
            tree = tree.down(1).down(1);
        }
        return replaceIndicesByTriplets(ILUtils.copy(tree), dos);
    }

    private static Tree replaceIndicesByTriplets(Tree tree, TapList<Tree> dos) {
        TapList<Tree> inDos = dos;
        Tree found = null;
        while (found == null && inDos != null) {
            if (dos.head.down(1).equalsTree(tree)) {
                found = dos.head;
            }
            inDos = inDos.tail;
        }
        if (found != null) {
            tree = ILUtils.build(ILLang.op_arrayTriplet,
                    ILUtils.copy(found.down(2)),
                    ILUtils.copy(found.down(3)),
                    ILUtils.copy(found.down(4)));
        } else if (!tree.isAtom()) {
            Tree[] sons = tree.children();
            Tree newSon;
            for (int i = sons.length - 1; i >= 0; i--) {
                newSon = replaceIndicesByTriplets(sons[i], dos);
                if (newSon != sons[i]) {
                    tree.setChild(newSon, i + 1);
                }
            }
        }
        return tree;
    }

    /**
     * Simplifies all sub-expressions of "expr" of the sort {@code *&exp} into {@code exp}.
     */
    private static Tree removeDerefOfAddress(Tree expr) {
        if (expr != null) {
            if (expr.opCode() == ILLang.op_pointerAccess
                    && ILUtils.isNullOrNone(expr.down(2))
                    && expr.down(1).opCode() == ILLang.op_address) {
                expr = removeDerefOfAddress(expr.down(1).cutChild(1));
            } else if (!expr.isAtom()) {
                Tree[] subTrees = expr.children();
                Tree newSubTree;
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    newSubTree = removeDerefOfAddress(subTrees[i]);
                    if (newSubTree != subTrees[i]) {
                        expr.setChild(newSubTree, i + 1);
                    }
                }
            }
        }
        return expr;
    }

    private static void synchronizeDoIterators(WrapperTypeSpec ts1, WrapperTypeSpec ts2) {
        //Peel off ModifiedTypeSpec's :
        while (ts1 != null && ts1.wrappedType != null &&
                ts1.wrappedType.kind() == SymbolTableConstants.MODIFIEDTYPE) {
            ts1 = ts1.wrappedType.elementType();
        }
        while (ts2 != null && ts2.wrappedType != null &&
                ts2.wrappedType.kind() == SymbolTableConstants.MODIFIEDTYPE) {
            ts2 = ts2.wrappedType.elementType();
        }
        assert ts1 != null;
        assert ts2 != null;
        switch (ts1.wrappedType.kind()) {
            case SymbolTableConstants.ARRAYTYPE: {
                if (ts2.wrappedType.kind() == SymbolTableConstants.ARRAYTYPE) {
                    //Synchronize the do-loops of matching dimensions
                    ArrayTypeSpec ats1 = (ArrayTypeSpec) ts1.wrappedType;
                    ArrayTypeSpec ats2 = (ArrayTypeSpec) ts2.wrappedType;
                    ArrayDim[] dl1 = ats1.dimensions();
                    ArrayDim[] dl2 = ats2.dimensions();
                    for (int i = dl1.length - 1; i >= 0; --i) {
                        if (dl1[i] != null && i < dl2.length && dl2[i] != null) {
                            ((ArrayDimPlusIter) dl1[i]).iter.setInitTree(((ArrayDimPlusIter) dl2[i]).iter.getInitTree());
                            ((ArrayDimPlusIter) dl1[i]).iter.indexHolder = ((ArrayDimPlusIter) dl2[i]).iter.indexHolder;//[llh 21/10/2010] new, to be tested ...
                            //[TODO] here synchronize the field iter.isArray to the most conservative of the two.
                        }
                    }
                    synchronizeDoIterators(ats1.elementType(), ats2.elementType());
                }
                break;
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                CompositeTypeSpec cts1 = (CompositeTypeSpec) ts1.wrappedType;
                CompositeTypeSpec cts2 = (CompositeTypeSpec) ts2.wrappedType;
                FieldDecl[] fl1 = cts1.fields;
                FieldDecl[] fl2 = cts2.fields;
                for (int i = fl1.length - 1; i >= 0; --i) {
                    if (fl1[i] != null && i < fl2.length && fl2[i] != null) {
                        synchronizeDoIterators(fl1[i].type(), fl2[i].type());
                    }
                }
                break;
            }
            case SymbolTableConstants.POINTERTYPE: {
                PointerTypeSpec pts1 = (PointerTypeSpec) ts1.wrappedType;
                PointerTypeSpec pts2 = (PointerTypeSpec) ts2.wrappedType;
                ArrayDim dim1 = pts1.offsetLength;
                ArrayDim dim2 = pts2.offsetLength;
                if (dim1 != null && dim2 != null) {
                    ((ArrayDimPlusIter) dim1).iter.setInitTree(((ArrayDimPlusIter) dim2).iter.getInitTree());
                    ((ArrayDimPlusIter) dim1).iter.indexHolder = ((ArrayDimPlusIter) dim2).iter.indexHolder;//[llh 21/10/2010] new, to be tested ...
                }
                //TODO: should we synchronize the two destination types ?
                break;
            }
            case SymbolTableConstants.PRIMITIVETYPE:
            default:
                break;
        }
    }

    private static boolean allDimensionsFull(ArrayDim[] dimensions) {
        boolean allFull = true;
        for (int i = dimensions.length - 1; i >= 0; i--) {
            if (!((ArrayDimPlusIter) dimensions[i]).iter.isImplicitCompleteDimension) {
                allFull = false;
            }
        }
        return allFull;
    }

    /**
     * true if this access, although it is an array access, can be
     * used without a do-loop in pure F77, using the classical Fortran
     * pseudo-pointer trick that calls F(T(i)) to mean F(T(i:)).
     */
    private static boolean subArrayAccess(ArrayDim[] dimensions) {
        return dimensions.length == 1 &&
                ((ArrayDimPlusIter) dimensions[0]).iter.allSubArrayAccesses;
    }

    /**
     * @return true when the given "typeSpec" is the same as the stored "elementTypeSpec".
     * "elementTypeSpec" is the type of the elements of the Tree handled by this RefDescriptor.
     */
    private static boolean isSameTSorActualTS(WrapperTypeSpec typeSpec, WrapperTypeSpec elemTypeSpec) {
        return typeSpec == elemTypeSpec ||
                typeSpec != null && elemTypeSpec != null && typeSpec.wrappedType != null &&
                        typeSpec.wrappedType == elemTypeSpec.wrappedType;
    }

    private static TapList<Tree> collectTreesRec(RefDescriptor.NestedLoopNode node, TapList<Tree> collected) {
        if (node.nodes != null) {
            TapList<RefDescriptor.NestedLoopNode> nodes = node.nodes;
            while (nodes != null) {
                collected = collectTreesRec(nodes.head, collected);
                nodes = nodes.tail;
            }
        } else {
            collected = new TapList<>(node.expression, collected);
        }
        return collected;
    }

    /**
     * Builds an instruction Tree that initializes reference expression "expr".
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     * When zoneInfo is passed non-null, it indicates that we want to initialize
     * the full corresponding zone, and then "expr" contains this zoneInfo's accessTree,
     * possibly slightly modified (see BlockDifferentiator.makeDiffInitialization()).
     */
    public static Tree makeInitialize(Unit targetUnit, ZoneInfo zoneInfo, Tree expr,
                                      WrapperTypeSpec actualArgType, WrapperTypeSpec formalArgType, Tree hintRootTree,
                                      SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                      SymbolTable otherUsageSymbolTable, Tree altRootIdent,
                                      boolean isDiff, IterDescriptor multiDirIterator,
                                      boolean protectAccesses, TapList ignoreStructure, boolean ignorePointed) {
        RefDescriptor refDescriptor =
            new RefDescriptor(expr, zoneInfo, actualArgType, formalArgType, hintRootTree,
                        defSymbolTable, usageSymbolTable, otherUsageSymbolTable,
                                  altRootIdent, isDiff, multiDirIterator, targetUnit);
        refDescriptor.setHintArrayTreeForCallSize(hintRootTree);
        refDescriptor.prepareForInitialize(targetUnit, ignoreStructure, ignorePointed);
        refDescriptor.mayProtectAccesses = protectAccesses;
        Tree result = refDescriptor.makeInitialize();
        return result;
    }

    public static Tree makeNormDiff(Unit targetUnit, Tree cumulTree, Tree newExpr, Tree oldExpr,
                                    SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                    Tree newAltRootIdent, Tree oldAltRootIdent,
                                    boolean isDiff, IterDescriptor multiDirIterator) {
        RefDescriptor oldRefDescriptor =
                new RefDescriptor(oldExpr, null, defSymbolTable, usageSymbolTable, null,
                        oldAltRootIdent, isDiff, multiDirIterator, targetUnit);
        RefDescriptor newRefDescriptor =
                new RefDescriptor(newExpr, null, defSymbolTable, usageSymbolTable, null,
                        newAltRootIdent, isDiff, multiDirIterator, targetUnit);
        oldRefDescriptor.setCompanionVarDescriptor(newRefDescriptor);
        oldRefDescriptor.prepareForAssignOrNormDiff(newRefDescriptor, targetUnit, null, true);

        return oldRefDescriptor.makeNormDiff(cumulTree, newRefDescriptor.nestedLoopTree, oldRefDescriptor.nestedLoopTree, newRefDescriptor, oldRefDescriptor);
    }

    /**
     * @return an instruction Tree that assigns one reference expression into another.
     * Namely: "lhsExpr" := "rhsExpr"
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     */
    public static Tree makeAssign(Unit targetUnit, Tree lhsExpr, Tree rhsExpr,
                                  SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                  Tree lhsAltRootIdent, Tree rhsAltRootIdent,
                                  boolean isDiff, IterDescriptor multiDirIterator) {
        RefDescriptor lhsRefDescriptor =
                new RefDescriptor(lhsExpr, null, defSymbolTable, usageSymbolTable, null,
                        lhsAltRootIdent, isDiff, multiDirIterator, targetUnit);
        RefDescriptor rhsRefDescriptor =
                new RefDescriptor(rhsExpr, null, defSymbolTable, usageSymbolTable, null,
                        rhsAltRootIdent, isDiff, multiDirIterator, targetUnit);
        //refDescriptor.setHintArrayTreeForCallSize(hintRootTree);
        lhsRefDescriptor.setCompanionVarDescriptor(rhsRefDescriptor);
        lhsRefDescriptor.prepareForAssignOrNormDiff(rhsRefDescriptor, targetUnit, null, true);
        return lhsRefDescriptor.makeAssign(rhsRefDescriptor);
    }

    /**
     * @return an instruction Tree that adds one reference expression into another.
     * Namely: "lhsExpr" := "lhsExpr"+"rhsExpr"
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     */
    public static Tree makeIncrement(Unit targetUnit, Tree lhsExpr, Tree rhsExpr,
                                     SymbolTable defSymbolTable, SymbolTable usageSymbolTable,
                                     Tree lhsAltRootIdent, Tree rhsAltRootIdent,
                                     boolean isDiff, IterDescriptor multiDirIterator) {
        RefDescriptor lhsRefDescriptor =
                new RefDescriptor(lhsExpr, null, defSymbolTable, usageSymbolTable, null,
                        lhsAltRootIdent, isDiff, multiDirIterator, targetUnit);
        RefDescriptor rhsRefDescriptor =
                new RefDescriptor(rhsExpr, null, defSymbolTable, usageSymbolTable, null,
                        rhsAltRootIdent, isDiff, multiDirIterator, targetUnit);
        lhsRefDescriptor.setCompanionVarDescriptor(rhsRefDescriptor);
        lhsRefDescriptor.prepareForAssignOrNormDiff(rhsRefDescriptor, targetUnit, null, true);
        return lhsRefDescriptor.makeIncrement(rhsRefDescriptor, false);
    }

    /**
     * @return an instruction Tree that PUSH'es expression "expr".
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     */
    public static Instruction makePush(Unit targetUnit, Tree expr,
                                       SymbolTable defSymbolTable, SymbolTable usageSymbolTable, int ppLabel) {
        RefDescriptor refDescriptor =
                new RefDescriptor(expr, null,
                        defSymbolTable, usageSymbolTable, null,
                        null, false, null, targetUnit);
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        return new Instruction(refDescriptor.makePush(), null, null);
    }

    /**
     * @return an instruction Tree that POP's expression "expr".
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     */
    public static Instruction makePop(Unit targetUnit, Tree expr,
                                      SymbolTable defSymbolTable, SymbolTable usageSymbolTable, int ppLabel,
                                      TapPair<NewSymbolHolder, WrapperTypeSpec> toBranchVariable) {
        RefDescriptor refDescriptor =
                new RefDescriptor(expr, null,
                        defSymbolTable, usageSymbolTable, null,
                        null, false, null, targetUnit);
        refDescriptor.toBranchVariable = toBranchVariable;
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        Tree result = refDescriptor.makePop();
        return new Instruction(result, null, null);
    }

    /**
     * Same as makePush(...), but replaces the root of "expr" with its derivative "diffRoot".
     */
    public static Instruction makePushDiff(Unit targetUnit, Tree expr, Tree diffRoot,
                                           SymbolTable defSymbolTable, SymbolTable usageSymbolTable, int ppLabel) {
        RefDescriptor refDescriptor =
                new RefDescriptor(expr, null,
                        defSymbolTable, usageSymbolTable, null,
                        diffRoot, true, null, targetUnit);
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        return new Instruction(refDescriptor.makePush(), null, null);
    }

    /**
     * Same as makePop(...), but replaces the root of "expr" with its derivative "diffRoot".
     */
    public static Instruction makePopDiff(Unit targetUnit, Tree expr, Tree diffRoot,
                                          SymbolTable defSymbolTable, SymbolTable usageSymbolTable, int ppLabel) {
        RefDescriptor refDescriptor =
                new RefDescriptor(expr, null,
                        defSymbolTable, usageSymbolTable, null,
                        diffRoot, true, null, targetUnit);
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        return new Instruction(refDescriptor.makePop(), null, null);
    }

    /**
     * Same as makePop, but will store a control integer using PUSH/POPCONTROL(case,nbCases).
     */
    public static Instruction makeFixedControlPop(Unit targetUnit, Tree expr,
                                                  int nbCases, SymbolTable defSymbolTable, SymbolTable usageSymbolTable, int ppLabel) {
        RefDescriptor refDescriptor =
                new RefDescriptor(expr, null, null,
                        defSymbolTable, usageSymbolTable, null,
                        null, false, null, targetUnit);
        refDescriptor.makeFixedControl(nbCases);
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        return new Instruction(refDescriptor.makePop(), null, null);
    }

    /**
     * Builds an instruction Tree that does the stack-like operation
     * "stackOp" on the memory described by "zoneInfo".
     * Quick call that does the whole chain of creation, preparation,
     * merging, and making Tree in one single call.
     * Does not allow for manipulation of individual RefDescriptors.
     */
    public static Tree makeStack(Unit targetUnit,
                                 String stackOp, boolean directDirection,
                                 ZoneInfo zoneInfo, Tree zoneInfoVisibleAccessTree,
                                 SymbolTable defSymbolTable,
                                 SymbolTable usageSymbolTable,
                                 boolean protectAccesses,
                                 int ppLabel) {
        RefDescriptor refDescriptor =
                new RefDescriptor(zoneInfo, zoneInfoVisibleAccessTree,
                        defSymbolTable, usageSymbolTable, null,
                        null, false, null, targetUnit);
        refDescriptor.prepareForStack(targetUnit, ppLabel, null, true);
        refDescriptor.mayProtectAccesses = protectAccesses;
        Tree result = refDescriptor.makeStack(refDescriptor.nestedLoopTree, stackOp, directDirection);
        refDescriptor.checkUnknownDimensionsForC(result, stackOp);
        return result;
    }

    public Tree refTree() {
        return patternTree;
    }

    public void setHintArrayTreeForCallSize(Tree tree) {
        hintArrayTreeForCallSize = tree;
    }

    /**
     * When altRootIdent is not null, must replace the root ident Tree
     * in all instructions created by this RefDescriptor.
     * For instance, this may be the differentiated variable...
     */
    private void initialize(Tree tree, Tree altRootIdent) {
        if (tree.opCode() == ILLang.op_deallocate
                || tree.opCode() == ILLang.op_allocate) {
            tree = tree.down(1);
        }
        this.patternTree = ILUtils.copy(tree);
        if (patternTree.opCode() == ILLang.op_iterativeVariableRef) {
            patternTree = turnIterVarRefIntoTriplets(patternTree);
        }
        if (altRootIdent != null) {
            patternTree = changeRootByAltRoot(patternTree, altRootIdent);
        }
        patternTree = removeDerefOfAddress(patternTree);
        // Sweep through patternTree to detect and analyse all iterators.
        // Creates IterDescriptors that are attached to the resulting WrapperTypeSpec.
        // ATTENTION: we suppose a max depth of nested iterations of 10 !!!
        itersMemo = new IterDescriptor[10];
        for (int i = 0; i < 10; i++) {
            itersMemo[i] = null;
        }
        itersMemoNextRk = 0;
        WrapperTypeSpec initTypeSpec = new WrapperTypeSpec(null);
        TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingTypeSpec = new TapPair<>(initTypeSpec, initTypeSpec);
        String nameInText = "";
        String nameInIdent = "";
        if (hintRootTree != null) {
            nameInText = ILUtils.buildNameInText(hintRootTree);
            nameInIdent = ILUtils.buildNameInIdent(hintRootTree);
        }
        if (hintArrayTreeForCallSize != null) {
            hintArrayTreeForCallSize = ILUtils.buildSizeArgument1(hintArrayTreeForCallSize, usageSymbolTable);
        }
        RefDescriptor.HintPlace hintPlace = new RefDescriptor.HintPlace(tree, nameInText, nameInIdent, hintArrayTreeForCallSize, 0, 1);
        WrapperTypeSpec elementTypeSpec =
                typesAndIterates(patternTree, null, true,
                        resultingTypeSpec, hintPlace);
        // Sweep through (and copy) the WrapperTypeSpec of the elements of the patternTree to
        // create and attach all remaining IterDescriptors:
        elementTypeSpec =
                createIterDescriptors(elementTypeSpec, hintPlace, null);
        itersMemo = null;
        this.elementType = elementTypeSpec;
        // Connect elementTypeSpec to the tail of resultingTypeSpec, to create the
        // completeTypeSpec WrapperTypeSpec of "tree":
        if (elementTypeSpec != null) {
            resultingTypeSpec.second.wrappedType =
                    elementTypeSpec.wrappedType;
        }
        // this.completeTypeSpec will be used to find, for each IterDescriptor,
        // whether it will be done with loops or with array notation.
        this.completeType = resultingTypeSpec.first;
    }

    /**
     * returns true for special fake variables for IOstreams.
     */
    public boolean isChannelOrIO() {
        return zoneInfo!=null && zoneInfo.isChannelOrIO ;
    }

    public void makeFixedControl(int nbControls) {
        // JUST COMMENT NEXT FRAGMENT TO COME BACK TO THE OLD MANAGEMENT OF CONTROL
        //  SWITCH PUSH/POP'S THAT USED PUSH/POP/INTEGER8() INSTEAD OF PUSH/POP/CONTROL()
        this.nbControls = nbControls;
        // END OF COMMENT FRAGMENT.
    }

    /**
     * Analyzes iteration indexes inside "tree",
     * which is a brand new copy of some program's expression AST.
     * Modifies "tree" by annotating analyzed iterators by special objects
     * (Objects of type IterDescriptorUse in annotation "iterDescriptorUse"
     * remark: could be storage "TreesIterDescriptorUses", but weird bugs due to annotations).
     * Also sets boolean global "scalarIndices" to true if there is a
     * scalar index after one of the vectorial indices: in this case,
     * it is impossible in F77 to manipulate the tree as a whole.
     * Uses the globals "itersMemo" and "itersMemoNextRk" to memorize the
     * IterDescriptors already created.
     *
     * @param tree              The expression tree to be analyzed.
     * @param contextDim        When non null, is the size of the present dimension
     *                          of the enclosing array: it is used to explicit notations like A(:).
     * @param elementalUse      When true, means that the tree will be used as an argument
     *                          of an elemental operation. Thus if the top of elementTypeSpec is an
     *                          ArrayTypeSpec, it is peeled away, and it is added into the dangling leaf
     *                          of resultingTypeSpec, so that the elemental usage sees the elements type.
     * @param resultingTypeSpec resulting description of the WrapperTypeSpec of
     *                          "tree" itself. It is a TapPair pair of (first) the root of the resulting
     *                          WrapperTypeSpec and (second) the dangling unknown WrapperTypeSpec inside it.
     * @param hintPlace         a bottom-up value containing "location" info to write the
     *                          messages and hints about required unknown dimensions.
     * @return The WrapperTypeSpec of the elements of the given "tree", i.e. the
     * type of "tree" if all iterators in it receive a scalar index value.
     */
    private WrapperTypeSpec typesAndIterates(Tree tree, ArrayDim contextDim,
                                             boolean elementalUse,
                                             TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingTypeSpec,
                                             RefDescriptor.HintPlace hintPlace) {
        /* TODO : this doesn't work well for weird trees such as:
         *   A(100:200)%x(50) (but the equivalent A(150)%x does work!)
         * because when the (50) is encountered, it's too late
         * to erase the ArrayTypeSpec level. This could be fixed
         * by retrieving the deepest ArrayTypeSpec level in
         * "resultingTypeSpec", and replacing/erasing it. */
        WrapperTypeSpec elementTypeSpec;
        WrapperTypeSpec danglingLeafType;
        switch (tree.opCode()) {
            case ILLang.op_ident: {
                IterDescriptor iterDescriptor = getIterDescriptorForTree(tree);
                if (iterDescriptor != null) {
                    // if this tree is the index of an enclosing do-loop:
                    RefDescriptor.IterDescriptorUse iterDescriptorUse =
                        new RefDescriptor.IterDescriptorUse(iterDescriptor, iterDescriptor.defaultUse, contextDim);
                    tree.setAnnotation("iterDescriptorUseAnnot", iterDescriptorUse);
                    ArrayDim[] dims1 = new ArrayDim[1];
                    int firstArrayIndex = targetUnit.isFortran() ? 1 : 0;
                    Tree lengthTree = iterDescriptor.getLengthTree();
                    if (lengthTree != null && lengthTree.opCode() == ILLang.op_intCst) {
                        contextDim = new ArrayDim(null, firstArrayIndex,
                                lengthTree.intValue() + firstArrayIndex - 1, null, -1, 0, 0);
                    }
                    dims1[0] = new ArrayDimPlusIter(contextDim, iterDescriptor);
                    iterDescriptorUse.contextDim = dims1[0];
                    danglingLeafType = new WrapperTypeSpec(null);
                    resultingTypeSpec.first = new WrapperTypeSpec(new ArrayTypeSpec(danglingLeafType, dims1));
                    resultingTypeSpec.second = danglingLeafType;
                    elementTypeSpec = integerTypeSpec;
                } else {
                    VariableDecl variableDecl;
                    NewSymbolHolder diffSymbolHolder = null;
                    String varSymbol = "";
                    WrapperTypeSpec typeSpec = null;
                    Tree nonDiffTree =
                        (diffRootIdent != null && tree.equalsTree(diffRootIdent) ? nonDiffRootIdent : tree) ;
                    if (nonDiffTree != null) {
                        diffSymbolHolder = NewSymbolHolder.getNewSymbolHolder(nonDiffTree);
                    }
                    if (diffSymbolHolder != null) {
                        if (diffSymbolHolder.derivationFrom == null) {
                            typeSpec = diffSymbolHolder.typeSpec();
                        } else if (diffSymbolHolder.derivationFrom instanceof VariableDecl) {
                            variableDecl = (VariableDecl) diffSymbolHolder.derivationFrom;
                            typeSpec = variableDecl.type();
                            varSymbol = variableDecl.symbol;
                        } else if (diffSymbolHolder.derivationFrom instanceof FunctionDecl) {
                            FunctionDecl funDecl = (FunctionDecl) diffSymbolHolder.derivationFrom;
                            typeSpec = funDecl.returnTypeSpec();
                            varSymbol = funDecl.symbol;
                        }
                    } else {
                        varSymbol = ILUtils.getIdentString(nonDiffTree);
                        variableDecl =
                                defSymbolTable.getVariableOrConstantDecl(varSymbol);
                        typeSpec = (variableDecl == null ? null : variableDecl.type());
                    }
                    elementTypeSpec = typeSpec != null ? typeSpec : knownRootTypeSpec;
                    if (forceRootTypeSpec != null) {
                        elementTypeSpec = forceRootTypeSpec;
                    }
                    if (hintPlace.arrayNameInText == null || hintPlace.arrayNameInText.isEmpty()) {
                        hintPlace.arrayNameInText = varSymbol;
                    }
                    if (hintPlace.arrayNameInIdent == null || hintPlace.arrayNameInIdent.isEmpty()) {
                        hintPlace.arrayNameInIdent = varSymbol;
                    }
                    if (hintPlace.arrayTreeForCallSize == null) {
                        hintPlace.arrayTreeForCallSize = nonDiffTree;
                    }
                }
                break;
            }
            case ILLang.op_dimColon:
            case ILLang.op_arrayTriplet: {
                IterDescriptor iterDescriptor = itersMemo[itersMemoNextRk];
                int firstArrayIndex = targetUnit.isFortran() ? 1 : 0;
                if (iterDescriptor == null) {
                    iterDescriptor = new IterDescriptor(itersMemoNextRk + 1, usageSymbolTable,
                            otherUsageSymbolTable,
                            ILUtils.build(ILLang.op_intCst, firstArrayIndex), hintPlace.refTree,
                            hintPlace.arrayNameInText, hintPlace.arrayNameInIdent, hintPlace.arrayTreeForCallSize, hintPlace.dim, hintPlace.ndims);
                    itersMemo[itersMemoNextRk] = iterDescriptor;
                }
                itersMemoNextRk++;
                RefDescriptor.IterDescriptorUse iterDescriptorUse =
                        new RefDescriptor.IterDescriptorUse(iterDescriptor, tree, contextDim);
                tree.setAnnotation("iterDescriptorUseAnnot", iterDescriptorUse);
                ArrayDim[] dims1 = new ArrayDim[1];
                // If this array section knows its length, use it right now. Otherwise keep context dimension size:
                Tree lengthTree = iterDescriptor.getLengthTree();
                if (lengthTree != null && lengthTree.opCode() == ILLang.op_intCst) {
                    contextDim = new ArrayDim(null, firstArrayIndex,
                            lengthTree.intValue() + firstArrayIndex - 1, null, -1, 0, 0);
                } else if (lengthTree != null && (contextDim == null || contextDim.tree() == null && contextDim.upper == null)) {
                    // else if contextDim still holds no info, use this "dynamic" lengthTree
                    // because most languages now allow dynamic sizing of declared arrays.
                    contextDim = new ArrayDim(ILUtils.build(ILLang.op_dimColon, ILUtils.build(ILLang.op_intCst, 1), ILUtils.copy(lengthTree)), null, null, null, -1, 0, 0);
                }
                // In cases contextDim is still null, create one:
                if (contextDim == null) {
                    contextDim = new ArrayDim(tree, null, null, null, -1, 0, 0);
                }
                dims1[0] = new ArrayDimPlusIter(contextDim, iterDescriptor);
                iterDescriptorUse.contextDim = dims1[0] ;
                danglingLeafType = new WrapperTypeSpec(null);
                resultingTypeSpec.first = new WrapperTypeSpec(new ArrayTypeSpec(danglingLeafType, dims1));
                resultingTypeSpec.second = danglingLeafType;
                elementTypeSpec = integerTypeSpec;
                break;
            }
            case ILLang.op_arrayAccess: {
                WrapperTypeSpec arrayType = typesAndIterates(tree.down(1), null, false,
                        resultingTypeSpec, hintPlace);

                hintPlace.arrayTreeForCallSize = ILUtils.copy(tree.down(1));

                if (TypeSpec.isA(arrayType, SymbolTableConstants.ARRAYTYPE)) {
                    ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) arrayType.wrappedType;
                    ArrayDim arrayDimType;
                    ArrayDim newArrayDimType;
                    Tree index;
                    TapList<ArrayDim> resultingDimensions = null;
                    Tree[] indexes = tree.down(2).children();
                    int indexlength = arrayTypeSpec.dimensions().length;
                    if (indexlength > indexes.length) {
                        indexlength = indexes.length;
                    }
                    TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingIndexTypeSpec;
                    WrapperTypeSpec initTypeSpec;
                    WrapperTypeSpec indexType;
                    for (int i = 0; i < indexlength; i++) {
                        arrayDimType = arrayTypeSpec.dimensions()[i];
                        index = indexes[i];
                        initTypeSpec = new WrapperTypeSpec(null);
                        resultingIndexTypeSpec = new TapPair<>(initTypeSpec, initTypeSpec);
                        indexType = typesAndIterates(index, arrayDimType, true,
                                resultingIndexTypeSpec,
                                hintPlace.deriveDim(i + 1, indexlength));
                        if (indexType != null) {
                            resultingIndexTypeSpec.second.wrappedType =
                                    indexType.wrappedType;
                        }
                        indexType = resultingIndexTypeSpec.first;
                        if (TypeSpec.isA(indexType, SymbolTableConstants.ARRAYTYPE)
                                && !indexType.isString()) {
                            newArrayDimType = ((ArrayTypeSpec) indexType.wrappedType).dimensions()[0];
                            resultingDimensions = new TapList<>(newArrayDimType,
                                    resultingDimensions);
                        } else {
                            scalarIndices = true;
                        }
                    }
                    if (resultingDimensions != null) {
                        ArrayDim[] newDimensions = new ArrayDim[TapList.length(resultingDimensions)];
                        for (int i = newDimensions.length - 1; i >= 0; i--) {
                            newDimensions[i] = resultingDimensions.head;
                            resultingDimensions = resultingDimensions.tail;
                        }
                        danglingLeafType = new WrapperTypeSpec(null);
                        resultingTypeSpec.second.wrappedType =
                                new ArrayTypeSpec(danglingLeafType, newDimensions);
                        resultingTypeSpec.second = danglingLeafType;
                    }
                    elementTypeSpec = arrayTypeSpec.elementType();
                } else {
                    elementTypeSpec = arrayType;
                }
                break;
            }
            case ILLang.op_fieldAccess:
                elementTypeSpec = typesAndIterates(tree.down(1), null, true,
                        resultingTypeSpec, hintPlace);
                if (TypeSpec.isA(elementTypeSpec, SymbolTableConstants.COMPOSITETYPE)) {
                    elementTypeSpec =
                            ((CompositeTypeSpec) elementTypeSpec.wrappedType).getFieldType(tree.down(2));
                }
                hintPlace.makeField(ILUtils.getIdentString(tree.down(2)));
                break;
            case ILLang.op_pointerAccess: {
                WrapperTypeSpec pointerType = typesAndIterates(tree.down(1), null, false,
                        resultingTypeSpec, hintPlace);
                if (TypeSpec.isA(pointerType, SymbolTableConstants.POINTERTYPE)) {
                    PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) pointerType.wrappedType;
                    if (!ILUtils.isNullOrNone(tree.down(2))) {
                        //This is a C-like array access:
                        ArrayDim arrayDimType = pointerTypeSpec.offsetLength;
                        Tree index = tree.down(2);
                        WrapperTypeSpec initTypeSpec = new WrapperTypeSpec(null);
                        TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingIndexTypeSpec =
                                new TapPair<>(initTypeSpec, initTypeSpec);
                        WrapperTypeSpec indexType = typesAndIterates(index, arrayDimType, true,
                                resultingIndexTypeSpec,
                                hintPlace.deriveDim(1, 1));
                        if (indexType != null) {
                            resultingIndexTypeSpec.second.wrappedType =
                                    indexType.wrappedType;
                        }
                        indexType = resultingIndexTypeSpec.first;
                        if (TypeSpec.isA(indexType, SymbolTableConstants.ARRAYTYPE) && !indexType.isString()) {
                            ArrayDim newArrayDimType = ((ArrayTypeSpec) indexType.wrappedType).dimensions()[0];
                            danglingLeafType = new WrapperTypeSpec(null);
                            resultingTypeSpec.second.wrappedType =
                                    new PointerTypeSpec(danglingLeafType, newArrayDimType);
                            resultingTypeSpec.second = danglingLeafType;
                        }
                    }
                    elementTypeSpec = pointerTypeSpec.destinationType;
                } else {
                    elementTypeSpec = pointerType;
                }
                hintPlace.makePointerDest();
                break;
            }
            case ILLang.op_complexConstructor:
                elementTypeSpec = defSymbolTable.getTypeDecl("complex").typeSpec;
                break;
            case ILLang.op_add:
            case ILLang.op_mul:
            case ILLang.op_sub:
            case ILLang.op_div:
            case ILLang.op_power: {
                WrapperTypeSpec son1TypeSpec;
                WrapperTypeSpec son2TypeSpec;
                int memoInitRk = itersMemoNextRk;
                son1TypeSpec =
                        typesAndIterates(tree.down(1), null, true,
                                resultingTypeSpec, hintPlace);
                int memoMaxRk = itersMemoNextRk;
                if (son1TypeSpec != null) {
                    resultingTypeSpec.second.wrappedType =
                            son1TypeSpec.wrappedType;
                }
                son1TypeSpec = resultingTypeSpec.first;
                WrapperTypeSpec initTypeSpec = new WrapperTypeSpec(null);
                TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingTypeSpecBis =
                        new TapPair<>(initTypeSpec, initTypeSpec);
                RefDescriptor.HintPlace hintPlace2 = new RefDescriptor.HintPlace(hintPlace.refTree, "", "", hintPlace.arrayTreeForCallSize, 0, 1);
                itersMemoNextRk = memoInitRk;
                son2TypeSpec =
                        typesAndIterates(tree.down(2), null, true,
                                resultingTypeSpecBis, hintPlace2);
                if (itersMemoNextRk < memoMaxRk) {
                    itersMemoNextRk = memoMaxRk;
                }
                if (son2TypeSpec != null) {
                    resultingTypeSpecBis.second.wrappedType =
                            son2TypeSpec.wrappedType;
                }
                son2TypeSpec = resultingTypeSpecBis.first;
                resultingTypeSpec.first = son1TypeSpec.addWith(son2TypeSpec);
                hintPlace.cumulWith(hintPlace2);
                resultingTypeSpec.second = new WrapperTypeSpec(null);
                elementTypeSpec = null;
                break;
            }
            case ILLang.op_minus:
                elementTypeSpec =
                        typesAndIterates(tree.down(1), null, true,
                                resultingTypeSpec, hintPlace);
                break;
            case ILLang.op_intCst:
                elementTypeSpec = integerTypeSpec;
                break;
            case ILLang.op_realCst:
                elementTypeSpec = defSymbolTable.getTypeDecl("float").typeSpec;
                break;
            case ILLang.op_stringCst: {
                elementTypeSpec = defSymbolTable.getTypeDecl("character").typeSpec;
                int length = tree.stringValue().length();
                int firstArrayIndex = targetUnit.isFortran() ? 1 : 0;
                if (length > 1) {
                    ArrayDim[] dims1 = new ArrayDim[1];
                    dims1[0] = new ArrayDim(null, firstArrayIndex, length, null, -1, 0, 0);
                    elementTypeSpec = new WrapperTypeSpec(new ArrayTypeSpec(elementTypeSpec, dims1));
                }
                break;
            }
            case ILLang.op_boolCst:
                elementTypeSpec = defSymbolTable.getTypeDecl("boolean").typeSpec;
                break;
            case ILLang.op_none:
                elementTypeSpec = new WrapperTypeSpec(new VoidTypeSpec());
                break;
            case ILLang.op_call:
                if ("sum".equals(ILUtils.getCalledNameString(tree).toLowerCase())) {
                    elementTypeSpec = defSymbolTable.getTypeDecl("float").typeSpec;
                } else {
                    //We assume this is a single-arg elemental function,
                    // which returns the type of its 1st argument:
                    elementTypeSpec =
                            typesAndIterates(ILUtils.getArguments(tree).down(1), null, true,
                                    resultingTypeSpec, hintPlace);
                }
                break;
            default:
//             TapEnv.toolWarning(-1, "(Typing RefDescriptor) Unexpected operator: "+tree.opName()+" in "+tree) ;
                elementTypeSpec = new WrapperTypeSpec(new VoidTypeSpec());
                hintPlace.arrayNameInText = "???";
                hintPlace.arrayNameInIdent = "???";
        }
        // Peel modifiers off elementTypeSpec,
        // putting them into the dangling tail of resultingTypeSpec :
        while (TypeSpec.isA(elementTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            ModifiedTypeSpec modelModifiedTypeSpec = (ModifiedTypeSpec) elementTypeSpec.wrappedType;
            elementTypeSpec = modelModifiedTypeSpec.elementType();
            danglingLeafType = new WrapperTypeSpec(null);
            resultingTypeSpec.second.wrappedType =
                    new ModifiedTypeSpec(danglingLeafType, modelModifiedTypeSpec);
            resultingTypeSpec.second = danglingLeafType;
        }
        // If tree will be used elementwise (i.e. by an elemental operation) and
        // if elementTypeSpec turns out to start as an ArrayTypeSpec,
        // then peel the array level off elementTypeSpec, into the dangling tail
        // of resultingTypeSpec. Also annotate "tree" to make all implicit array
        // dimensions explicit.
        if (elementalUse && targetUnit.isFortran() && TypeSpec.isA(elementTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
            ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) elementTypeSpec.wrappedType;
            ArrayDim[] dimensions = arrayTypeSpec.dimensions();
            ArrayDim[] dimensionsWithIter = new ArrayDim[dimensions.length];
            Tree[] explicitIndexes = new Tree[dimensions.length];
            TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingIndexTypeSpec;
            WrapperTypeSpec indexTypeSpec;
            for (int i = 0; i < dimensions.length; i++) {
                explicitIndexes[i] = ILUtils.build(ILLang.op_arrayTriplet);
                WrapperTypeSpec initTypeSpec = new WrapperTypeSpec(null);
                resultingIndexTypeSpec = new TapPair<>(initTypeSpec, initTypeSpec);
                typesAndIterates(explicitIndexes[i], dimensions[i], false,
                        resultingIndexTypeSpec, hintPlace.deriveDim(i + 1, dimensions.length));
                indexTypeSpec = resultingIndexTypeSpec.first;
                dimensionsWithIter[i] =
                        ((ArrayTypeSpec) indexTypeSpec.wrappedType).dimensions()[0];
            }
            tree.setAnnotation("explicitIndexes",
                    ILUtils.build(ILLang.op_expressions, explicitIndexes));
            danglingLeafType = new WrapperTypeSpec(null);
            resultingTypeSpec.second.wrappedType =
                    new ArrayTypeSpec(danglingLeafType, dimensionsWithIter);
            resultingTypeSpec.second = danglingLeafType;
            elementTypeSpec = arrayTypeSpec.elementType();
        } else if (elementalUse && targetUnit.isFortran() && TypeSpec.isA(elementTypeSpec, SymbolTableConstants.POINTERTYPE)
                && ((PointerTypeSpec) elementTypeSpec.wrappedType).offsetLength != null) {
            PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) elementTypeSpec.wrappedType;
            ArrayDim dimension = pointerTypeSpec.offsetLength;
            Tree explicitIndex = ILUtils.build(ILLang.op_arrayTriplet);
            WrapperTypeSpec initTypeSpec = new WrapperTypeSpec(null);
            TapPair<WrapperTypeSpec, WrapperTypeSpec> resultingIndexTypeSpec = new TapPair<>(initTypeSpec, initTypeSpec);
            typesAndIterates(explicitIndex, dimension, false,
                    resultingIndexTypeSpec, hintPlace.deriveDim(1, 1));
            WrapperTypeSpec indexTypeSpec = resultingIndexTypeSpec.first;
            ArrayDim dimensionWithIter =
                    ((ArrayTypeSpec) indexTypeSpec.wrappedType).dimensions()[0];
            tree.setAnnotation("explicitIndexes", explicitIndex);
            danglingLeafType = new WrapperTypeSpec(null);
            resultingTypeSpec.second.wrappedType =
                    new PointerTypeSpec(danglingLeafType, dimensionWithIter);
            resultingTypeSpec.second = danglingLeafType;
            elementTypeSpec = pointerTypeSpec.destinationType;
        }
        return elementTypeSpec;
    }

    /**
     * Creates a shallow copy of modelTypeSpec, where every ArrayDim is
     * replaced with a ArrayDimPlusIter, with a pointer to a
     * newly created IterDescriptor.
     */
    private WrapperTypeSpec createIterDescriptors(WrapperTypeSpec modelTypeSpec,
                                                  RefDescriptor.HintPlace hintPlace,
                                                  TapList<TypeSpec> dejaVu) {
        if (modelTypeSpec == null || modelTypeSpec.wrappedType == null) {
            return null;
        }
        if (TapList.contains(dejaVu, modelTypeSpec.wrappedType)) {
            TapEnv.toolWarning(-1, "Cannot create a reference to a recursive-type expression " + ILUtils.toString(patternTree));
            return null;
        }
        dejaVu = new TapList<>(modelTypeSpec.wrappedType, dejaVu);
        switch (modelTypeSpec.wrappedType.kind()) {
            case SymbolTableConstants.ARRAYTYPE: {
                ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) modelTypeSpec.wrappedType;
                ArrayDim[] dimensions = arrayTypeSpec.dimensions();
                ArrayDim[] copyDimensions = new ArrayDim[dimensions.length];
                ArrayDim dimension;
                int firstArrayIndex = targetUnit.isFortran() ? 1 : 0;
                for (int i = 0; i < dimensions.length; ++i) {
                    dimension = dimensions[i];
                    hintPlace.dim = i + 1;
                    IterDescriptor iterDescriptor =
                            new IterDescriptor(itersMemoNextRk + 1, usageSymbolTable,
                                    null,
                                    ILUtils.build(ILLang.op_intCst, firstArrayIndex), hintPlace.refTree,
                                    hintPlace.arrayNameInText, hintPlace.arrayNameInIdent, hintPlace.arrayTreeForCallSize, hintPlace.dim, dimensions.length);
                    itersMemoNextRk++;
                    iterDescriptor.setLengthTree(
                            ILUtils.buildSizeTree(dimension.tree().down(1),
                                    dimension.tree().down(2),
                                    ILUtils.build(ILLang.op_intCst, 1)));
                    copyDimensions[i] =
                            new ArrayDimPlusIter(dimension, iterDescriptor);
                }
                WrapperTypeSpec copyElementType =
                        createIterDescriptors(arrayTypeSpec.elementType(), hintPlace, dejaVu);
                return new WrapperTypeSpec(new ArrayTypeSpec(copyElementType, copyDimensions));
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec) modelTypeSpec.wrappedType;
                FieldDecl[] fields = compositeTypeSpec.fields;
                FieldDecl[] copyFields = new FieldDecl[fields.length];
                int memoInitRk = itersMemoNextRk;
                for (int i = fields.length - 1; i >= 0; --i) {
                    if (fields[i] != null) {
                        copyFields[i] =
                                new FieldDecl(fields[i].symbol,
                                        createIterDescriptors(fields[i].type(),
                                                hintPlace.deriveField(fields[i].symbol), dejaVu));
                        copyFields[i].setExtraInfo(fields[i].extraInfo()) ;
                    } else {
                        copyFields[i] = null;
                    }
                    itersMemoNextRk = memoInitRk;
                }
                CompositeTypeSpec copyCompositeType;
                if (compositeTypeSpec.isRecordType()) {
                    copyCompositeType = new CompositeTypeSpec(null, copyFields, SymbolTableConstants.RECORDTYPE, compositeTypeSpec.modifiers, null);
                } else {
                    copyCompositeType = new CompositeTypeSpec(null, copyFields, SymbolTableConstants.UNIONTYPE, null, null);
                }
                // Share active fields info:
                copyCompositeType.shareActiveFields(compositeTypeSpec);

                return new WrapperTypeSpec(copyCompositeType);
            }
            case SymbolTableConstants.MODIFIEDTYPE: {
                ModifiedTypeSpec modelModifiedTypeSpec = (ModifiedTypeSpec) modelTypeSpec.wrappedType;
                return new WrapperTypeSpec(
                        new ModifiedTypeSpec(createIterDescriptors(modelModifiedTypeSpec.elementType(), hintPlace, dejaVu),
                                modelModifiedTypeSpec));

//             ModifiedTypeSpec modifiedTypeSpec = (ModifiedTypeSpec)modelTypeSpec.wrappedType ;
//             return new WrapperTypeSpec(
//                       new ModifiedTypeSpec(
//                          createIterDescriptors(modifiedTypeSpec.elementType, hintPlace, dejaVu),
//                          modifiedTypeSpec.sizeModifier, modifiedTypeSpec.typeSign, null)) ;
            }
            case SymbolTableConstants.POINTERTYPE: {
                if (targetUnit.isFortran()) {
                    PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) modelTypeSpec.wrappedType;
                    ArrayDim dimension = pointerTypeSpec.offsetLength;
                    ArrayDimPlusIter copyDimension = null;
                    int firstArrayIndex = 1;
                    if (dimension != null) {
                        IterDescriptor iterDescriptor =
                                new IterDescriptor(itersMemoNextRk + 1, usageSymbolTable,
                                        null,
                                        ILUtils.build(ILLang.op_intCst, firstArrayIndex), hintPlace.refTree,
                                        hintPlace.arrayNameInText, hintPlace.arrayNameInIdent, hintPlace.arrayTreeForCallSize, hintPlace.dim, hintPlace.ndims);
                        itersMemoNextRk++;
                        iterDescriptor.setLengthTree(
                                ILUtils.buildSizeTree(dimension.tree().down(1),
                                        dimension.tree().down(2),
                                        ILUtils.build(ILLang.op_intCst, 1)));
                        copyDimension = new ArrayDimPlusIter(dimension, iterDescriptor);
                    }
                    hintPlace.makePointerDest();
                    return new WrapperTypeSpec(
                            new PointerTypeSpec(
                                    createIterDescriptors(pointerTypeSpec.destinationType, hintPlace, dejaVu),
                                    copyDimension));
                } else //In C, push/pop/init/... on a pointer means "on the pointer" and not "on its destination(s)"
                {
                    return modelTypeSpec;
                }
            }
            default:
                return modelTypeSpec;
        }
    }

    private Tree changeRootByAltRoot(Tree expr, Tree altRoot) {
        switch (expr.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return ILUtils.build(expr.opCode(),
                        changeRootByAltRoot(expr.down(1), altRoot),
                        ILUtils.copy(expr.down(2)));
            case ILLang.op_ident:
                nonDiffRootIdent = expr;
                diffRootIdent = altRoot;
                return ILUtils.copy(altRoot);
            default:
                return ILUtils.copy(expr);
        }
    }

    // mixed-language
    public void addDeref() {
        patternTree = ILUtils.build(ILLang.op_pointerAccess, ILUtils.copy(patternTree));
    }

    private IterDescriptor getIterDescriptorForTree(Tree searchedIndexTree) {
        int i = 0;
        IterDescriptor found = null;
        while (found == null && i < itersMemo.length) {
            if (itersMemo[i] != null && itersMemo[i].indexTree != null
                    && itersMemo[i].indexTree.equalsTree(searchedIndexTree)) {
                found = itersMemo[i];
            }
            ++i;
        }
        return found;
    }

    /**
     * Prepares this RefDescriptor to be used to build
     * PUSH/POP operations on the tree.
     * 1) Decides which iterations will be loops or arrays
     * 2) Builds the tree of nested loops
     * 3) If required, chooses a unique number for matching PUSH and POPs
     */
    public void prepareForStack(Unit targetUnit, int ppLabel, TapList ignoreStructure, boolean ignorePointed) {
        this.targetUnit = targetUnit;
        this.futureUse = PUSHPOP;
        boolean arrayNotationPermitted =
            decideArrayNotation(completeType, referencesIn(patternTree), PUSHPOP, ignorePointed, false);
        makeNestedLoopTree(ignoreStructure, ignorePointed, arrayNotationPermitted);
        if (TapEnv.get().numberPushPops) {
            this.ppLabel = ppLabel;
        }
    }

    /**
     * Prepares this RefDescriptor to be used to build
     * initialization operations on the tree.
     */
    public void prepareForInitialize(Unit targetUnit, TapList ignoreStructure, boolean ignorePointed) {
        this.targetUnit = targetUnit;
        this.futureUse = INIT;
        boolean arrayNotationPermitted =
            decideArrayNotation(completeType, referencesIn(patternTree), INIT, ignorePointed, false);
        makeNestedLoopTree(ignoreStructure, ignorePointed, arrayNotationPermitted);
    }

    /**
     * Jointly prepare this RefDescriptor and the "otherRefDescriptor" RefDescriptor
     * to be used to build an assignment or a computation of square norm operation between the two trees.
     */
    public void prepareForAssignOrNormDiff(RefDescriptor otherRefDescriptor, Unit targetUnit,
                                           TapList ignoreStructure, boolean ignorePointed) {
        this.targetUnit = targetUnit;
        this.futureUse = ASSIGN;
        otherRefDescriptor.targetUnit = targetUnit;
        boolean arrayNotationPermitted =
            decideArrayNotation(completeType, referencesIn(patternTree), ASSIGN, ignorePointed, false);
        otherRefDescriptor.decideArrayNotation(otherRefDescriptor.completeType,
                referencesIn(otherRefDescriptor.patternTree), ASSIGN, ignorePointed, false);
        synchronizeDoIterators(this.completeType, otherRefDescriptor.completeType);
        makeNestedLoopTree(ignoreStructure, ignorePointed, arrayNotationPermitted);
        otherRefDescriptor.makeNestedLoopTree(ignoreStructure, ignorePointed, arrayNotationPermitted);
    }

    /**
     * Collects the TapList of (roots of) copies of of all nontrivial (non simple ident ref)
     * variable references found in the given expression.
     * Example: on -A(:)+3*x*sin(T(:))+y+z%f2(i) returns (A T z)
     * @return the TapList of their root "baseTree()".
     */
    private TapList<Tree> referencesIn(Tree accessTree) {
        TapList<Tree> collection = new TapList<>(null, null);
        riRec(accessTree, collection);
        return collection.tail;
    }

    private void riRec(Tree accessTree, TapList<Tree> collection) {
        if (accessTree != null) {
            switch (accessTree.opCode()) {
                case ILLang.op_minus:
                    riRec(accessTree.down(1), collection);
                    break;
                case ILLang.op_call:
                    //We assume this is a single-arg elemental function,
                    // where array notation is directed by its 1st arg:
                    if (ILUtils.getArguments(accessTree).children().length > 0) {
                        riRec(ILUtils.getArguments(accessTree).down(1), collection);
                    }
                    break;
                case ILLang.op_add:
                case ILLang.op_mul:
                case ILLang.op_sub:
                case ILLang.op_div:
                case ILLang.op_power:
                case ILLang.op_complexConstructor:
                    riRec(accessTree.down(1), collection);
                    riRec(accessTree.down(2), collection);
                    break;
                case ILLang.op_arrayAccess:
                case ILLang.op_pointerAccess:
                case ILLang.op_fieldAccess:
                    collection.placdl(ILUtils.baseTree(ILUtils.copy(accessTree)));
                    break ;
                case ILLang.op_ident:
                    if (NewSymbolHolder.isANewSymbolRef(accessTree)) {
                        collection.placdl(ILUtils.copy(accessTree));
                    }
                    break;
                default:
            }
        }
    }

    private boolean oneIndexIsArray(Tree expr) {
        if (expr == null) {
            return false;
        } else if (expr.opCode() == ILLang.op_expressions) {
            Tree[] indices = expr.children();
            boolean oneArray = false;
            for (int i = indices.length - 1; i >= 0 && !oneArray; --i) {
                oneArray = oneIndexIsArray(indices[i]);
            }
            return oneArray;
        } else {
            return TypeSpec.isA(defSymbolTable.typeOf(expr), SymbolTableConstants.ARRAYTYPE);
        }
    }

    /**
     * @return a new TapList, in which each tree of "accessTrees" is
     * replaced by its closest ancestor that derefs the given "dimensions",
     * or just removed if there is no such ancestor. If on the way up, one
     * reaches a pointer deref (TODO: except on a F90 "ALLOCATABLE", which
     * is not really a pointer deref) and then the given "dimensions", then
     * set the toBool derefFound (if non-null) to true.
     * Note: we could be more stringent in checking that the accessTree
     * matches the given dimensions, but apparently it's not necessary!
     * If "dimensions" is null, go all the way up till the root.
     */
    private TapList<Tree> upTillDimensions(TapList<Tree> accessTrees,
                                           ArrayDim[] dimensions, ToBool derefFound) {
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        Tree accessTree;
        Tree origAccessTree;
        boolean hasDeref;
        while (accessTrees != null) {
            origAccessTree = accessTrees.head;
            if (origAccessTree != null) {
                accessTree = origAccessTree.parent();
                hasDeref = false;
                while (!(accessTree == null ||
                        accessTree.opCode() == ILLang.op_arrayAccess
                                && dimensions != null && oneIndexIsArray(accessTree.down(2)) ||
                        accessTree.opCode() == ILLang.op_pointerAccess
                                && dimensions != null && oneIndexIsArray(accessTree.down(2)))) {
                    if (accessTree.opCode() == ILLang.op_pointerAccess) {
                        hasDeref = true;
                    }
                    accessTree = accessTree.parent();
                }
                if (accessTree != null || dimensions == null) {
                    if (accessTree != null) {
                        tlResult = tlResult.placdl(accessTree);
                    }
                    if (hasDeref && derefFound != null) {
                        derefFound.set(true);
                    }
                }
            }
            accessTrees = accessTrees.tail;
        }
        return hdResult.tail;
    }

    /**
     * For each array dimension inside the type "typeSpec",
     * which is the type (completeTypeSpec) of objects we want to manipulate
     * (i.e. initialize, push, pop, assign, refer to, etc...)
     * decide whether this dimension can be treated with F90 array/vector notation,
     * or else must be handled by a DO-loop.
     * This decision is stored in the boolean isArray of each IterDescriptor.
     *
     * @return true when array notation is still permitted. i.e. when references to
     * the inside of objects of type typeSpec do not use array notation already,
     * and also do not deref a pointer (because we can't write A(:)%x when x is a pointer field)
     */
    private boolean decideArrayNotation(WrapperTypeSpec typeSpec, TapList<Tree> accessTrees,
                                        int mode, boolean ignorePointed, boolean insideElementTypeSpec) {
// System.out.println(" ENTER DECIDEARRAYNOTATION ON TYPE "+typeSpec+" elementType:"+elementType+" accessTrees:"+accessTrees) ;
        ToBool derefFound = new ToBool(false);
        // Find the moment when we go INSIDE the original expression:
        if (typeSpec == elementType ||
                typeSpec != null && elementType != null && typeSpec.wrappedType != null &&
                        typeSpec.wrappedType == elementType.wrappedType) {
            insideElementTypeSpec = true;
            accessTrees = upTillDimensions(accessTrees, null, derefFound);
        }
        boolean arrayNotationPermitted = true;
        if (typeSpec != null && typeSpec.wrappedType != null) {
            switch (typeSpec.wrappedType.kind()) {
                case SymbolTableConstants.ARRAYTYPE: {
                    ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
                    ArrayDim[] dimensions = arrayTypeSpec.dimensions();
                    if (!insideElementTypeSpec) {
                        accessTrees = upTillDimensions(accessTrees, dimensions, derefFound);
                    }
                    arrayNotationPermitted =
                            decideArrayNotation(arrayTypeSpec.elementType(), accessTrees,
                                    mode, ignorePointed, insideElementTypeSpec);
                    boolean arrayNotationUsedHere =
                        ((arrayNotationPermitted
                          // Special case when multidirectional diff: even if multiDir dimension has used
                          // array notation, we may still use array notation here because both apply to the
                          // same differentiated array.
                          || (multiDirIterator!=null
                              // check accessTree is already at the leaf access (i.e. on the primitive type element):
                              && (accessTrees==null || accessTrees.head.parent()==null)
                              && arrayTypeSpec.elementType()!=null
                              && arrayTypeSpec.elementType().isDifferentiablePlainType()))
                         && (targetUnit.hasArrayNotation()
                             || ((mode == PUSHPOP || mode == CALL)
                                 && (!scalarIndices || arrayTypeSpec.isString())
                                 && (allDimensionsFull(dimensions) || subArrayAccess(dimensions)))));
// System.out.println("  USED ARRAY NOTATION HERE FOR DIMS OF :"+typeSpec+" --> "+arrayNotationUsedHere) ;
                    for (int i = dimensions.length - 1; i >= 0; --i) {
                        ((ArrayDimPlusIter) dimensions[i]).iter.isArray = arrayNotationUsedHere;
                    }
                    // Array notation requires all ":" to be in the same array's dimensions.
                    // Therefore if we use array notation here, we can't use it in the
                    // enclosing types any more:
                    arrayNotationPermitted = arrayNotationPermitted && !arrayNotationUsedHere && !derefFound.get();
                    break;
                }
                case SymbolTableConstants.COMPOSITETYPE: {
                    // insideElementTypeSpec is certainly true. accessTrees is certainly empty.
                    CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec) typeSpec.wrappedType;
                    FieldDecl[] fields = compositeTypeSpec.fields;
                    arrayNotationPermitted = true;
                    boolean fieldNoDerefScalars;
                    for (int i = fields.length - 1; i >= 0; i--) {
                        if (fields[i] != null) {
                            fieldNoDerefScalars = decideArrayNotation(fields[i].type(), null,
                                    mode, ignorePointed, true);
                            arrayNotationPermitted = arrayNotationPermitted && fieldNoDerefScalars;
                        }
                    }
                    arrayNotationPermitted = arrayNotationPermitted && !derefFound.get();
                    break;
                }
                case SymbolTableConstants.MODIFIEDTYPE: {
                    ModifiedTypeSpec modifiedTypeSpec = (ModifiedTypeSpec) typeSpec.wrappedType;
                    arrayNotationPermitted = decideArrayNotation(modifiedTypeSpec.elementType(), accessTrees,
                            mode, ignorePointed, insideElementTypeSpec);
                    arrayNotationPermitted = arrayNotationPermitted && !derefFound.get();
                    break;
                }
                case SymbolTableConstants.POINTERTYPE: {
                    if (!ignorePointed || !insideElementTypeSpec) {
                        PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) typeSpec.wrappedType;
                        ArrayDim ptrDimension = pointerTypeSpec.offsetLength;
                        ArrayDimPlusIter dimension = null;
                        if (pointerTypeSpec.offsetLength instanceof ArrayDimPlusIter) {
                            dimension = (ArrayDimPlusIter) ptrDimension;
                        }
                        ArrayDim[] arrayOneDim = new ArrayDim[1];
                        arrayOneDim[0] = dimension;
                        if (!insideElementTypeSpec) {
                            accessTrees = upTillDimensions(accessTrees, arrayOneDim, null);
                        }
                        arrayNotationPermitted =
                                decideArrayNotation(pointerTypeSpec.destinationType, accessTrees,
                                        mode, ignorePointed, insideElementTypeSpec);
                        if (dimension != null) {
                            dimension.iter.isArray = arrayNotationPermitted &&
                                    (targetUnit.hasArrayNotation()
                                     || ((mode == PUSHPOP || mode == CALL)
                                         && dimension.iter.isImplicitCompleteDimension)) ;
                        }
                        /*"pointer is allocatable"*/// For F90 ALLOCATABLE "pointer" P only, it is
                        // allowed to write with array notation: [*P](:)
                        arrayNotationPermitted = false;
                    } else {
                        arrayNotationPermitted = false;
                        //  [llh] This is not logical: it should be allowed to write a(:)%p = &x since this
                        //  is an array assignment that does not really go through a pointer (no deref of p).
                        //  However, g95 and ifort compilers forbid that, so we have to comment out the following line:
                        //  arrayNotationPermitted = !derefFound.get() ;
                    }
                    break;
                }
                case SymbolTableConstants.PRIMITIVETYPE: {
                    // Special case when multidirectional diff: the multiDir index will use array notation,
                    boolean arrayNotationUsedHere =
                        multiDirIterator!=null && TypeSpec.isDifferentiableType(typeSpec) && targetUnit.hasArrayNotation() ;
// System.out.println("  USED ARRAY NOTATION HERE FOR MULTIDIR --> "+arrayNotationUsedHere+" MDI:"+multiDirIterator+" TS:"+typeSpec) ;
                    if (multiDirIterator!=null) multiDirIterator.isArray = arrayNotationUsedHere ;
                    // If using array notation for multidirectional diff, this forbids use of array notation in surrounding indices:
                    arrayNotationPermitted = !arrayNotationUsedHere && !derefFound.get();
                    break;
                }
                default:
                    arrayNotationPermitted = false;
            }
        }
// System.out.println(" >EXIT DECIDEARRAYNOTATION ON TYPE "+typeSpec+" array notation permitted:"+arrayNotationPermitted) ;
        return arrayNotationPermitted;
    }

    /**
     * Actual construction of the tree of nested iterations,
     * according to the choice that was just made of the iteration sort
     * for each IterDescriptor used ("do loop" or "array notation").
     *
     * @param ignoreStructure the tree of the components of the expressionTree that must be ignored.
     * @param ignorePointed   when true, the destination of pointer components or expressionTree must be ignored.
     */
    private void makeNestedLoopTree(TapList ignoreStructure, boolean ignorePointed, boolean arrayNotationPermitted) {
        TapList<TapPair<WrapperTypeSpec, Tree>> typeStack = buildReversedTypeStack(completeType, elementType);
        TapList<ArrayDim> dimensionsAlreadyInExpressionTree = new TapList<>(null, null) ;
        Tree expressionTree = solveIndexes(ILUtils.copy(patternTree), typeStack, arrayNotationPermitted, dimensionsAlreadyInExpressionTree);
        boolean canHaveDiff = TypeSpec.isDifferentiableType(completeType);
        nestedLoopTree = new RefDescriptor.NestedLoopNode(expressionTree, null, null, null, null, true, null, null, canHaveDiff);
        Tree rootTree = ILUtils.baseTree(nonDiffRootIdent == null ? expressionTree : nonDiffRootIdent);
        String rootIdent = ILUtils.baseName(nonDiffRootIdent == null ? expressionTree : nonDiffRootIdent);
        SymbolDecl rootDecl = rootIdent == null ? null : defSymbolTable.getVariableDecl(rootIdent);
        createNestedLoopTree(nestedLoopTree, expressionTree, rootDecl, completeType, null, null,
                ILUtils.build(ILLang.op_intCst, 1), false, true, canHaveDiff,
                ignoreStructure, ignorePointed, dimensionsAlreadyInExpressionTree,
                typeStack, ILUtils.copy(diffRootIdent != null ? diffRootIdent : rootTree));
    }

    /**
     * Build the reversed stack of array and pointer WrapperTypeSpec's that go from fromTypeSpec to elemTypeSpec,
     * which is included somewhere in fromTypeSpec. The stack is actually built of
     * TapPair's, used later to store the correspondence towards the corresponding Tree.
     */
    private TapList<TapPair<WrapperTypeSpec, Tree>> buildReversedTypeStack(WrapperTypeSpec fromTypeSpec,
                                                                           WrapperTypeSpec elemTypeSpec) {
        TapList<TapPair<WrapperTypeSpec, Tree>> result = null;
        boolean stop = false;
        while (!stop && fromTypeSpec != null && fromTypeSpec.wrappedType != null
                && !isSameTSorActualTS(fromTypeSpec, elemTypeSpec)) {
            switch (fromTypeSpec.wrappedType.kind()) {
                case SymbolTableConstants.ARRAYTYPE:
                    result = new TapList<>(new TapPair<>(fromTypeSpec, null), result);
                    fromTypeSpec = fromTypeSpec.wrappedType.elementType();
                    break;
                case SymbolTableConstants.POINTERTYPE:
                    result = new TapList<>(new TapPair<>(fromTypeSpec, null), result);
                    fromTypeSpec = ((PointerTypeSpec) fromTypeSpec.wrappedType).destinationType;
                    break;
                case SymbolTableConstants.MODIFIEDTYPE:
                    fromTypeSpec = fromTypeSpec.wrappedType.elementType();
                    break;
                default:
                    stop = true;
            }
        }
        return result;
    }

    /**
     * Modifies the given "expressionTree" (which is a copy of the pattern Tree)
     * by replacing all array expressions and array index expressions by
     * the actual index corresponding to the iteration choices made for
     * its associated IterDescriptor. For example, if the IterDescriptor chooses to
     * use a do-loop with do index "i", then the array index is "i". If it chooses
     * to use array notation, then the array index becomes an arrayTriplet.
     * Array triplet indexes that are sweeping the complete dimension are expressed
     * with the implicit ":". If all indexes are ":", then the list of indexes
     * is simply removed.
     * Also establishes correspondence from types to subTrees of expressionTree,
     * which is used to find the Tree that must be used in calls to SIZE().
     */
    private Tree solveIndexes(Tree expressionTree,
                              TapList<TapPair<WrapperTypeSpec, Tree>> typeStack, boolean arrayNotationPermitted,
                              TapList<ArrayDim> dimensionsAlreadyInExpressionTree) {
// System.out.println("  SOLVEINDEXES:"+expressionTree+" ARRAY NOTATION STILL PERMITTED:"+arrayNotationPermitted) ;
        if (expressionTree != null) {
            int opCode = expressionTree.opCode();
            if (opCode==ILLang.op_dimColon || opCode==ILLang.op_arrayTriplet) {
                RefDescriptor.IterDescriptorUse iterDescriptorUse =
                        expressionTree.getAnnotation("iterDescriptorUseAnnot");
                if (iterDescriptorUse != null) {
                    expressionTree = iterDescriptorUse.buildIndexTree(usageSymbolTable, targetUnit);
// System.out.println("     ITERDESCRIPTORUSE:"+iterDescriptorUse+" EXPRESSIONTREE:"+expressionTree);
                    dimensionsAlreadyInExpressionTree.placdl(iterDescriptorUse.contextDim) ;
                }
            }

            // When explicit indices are given through an annotation, turn expressionTree into standard form:
            Tree annotatedIndices = expressionTree.getAnnotation("explicitIndexes") ;
            if (annotatedIndices!=null) {
                expressionTree = ILUtils.copy(expressionTree) ;
                expressionTree.removeAnnotation("explicitIndexes") ;
                if (annotatedIndices.opCode() == ILLang.op_expressions) {
                    //Case of array indexes:
                    expressionTree = ILUtils.build(ILLang.op_arrayAccess, expressionTree, annotatedIndices);
                } else {
                    //Case of C pointer-array index:
                    expressionTree = ILUtils.build(ILLang.op_pointerAccess, expressionTree, annotatedIndices);
                }
            }

            if (expressionTree.opCode()==ILLang.op_arrayAccess) {
                TapList<TapPair<WrapperTypeSpec, Tree>> typeStackTail =
                    (typeStack==null ? null : typeStack.tail) ;
                Tree newIndices = solveIndexes(expressionTree.down(2), typeStackTail, arrayNotationPermitted, dimensionsAlreadyInExpressionTree);
                if (newIndices!=expressionTree.down(2)) {
                    expressionTree.setChild(newIndices, 2);
                }
                boolean allImplicitColons = true;  //not the same as allDimensionsFull !
                Tree[] indx = newIndices.children();
                for (int i = indx.length - 1; i >= 0; i--) {
                    if (indx[i].opCode()==ILLang.op_dimColon || indx[i].opCode()==ILLang.op_arrayTriplet) {
                        // This level is going to use array notation, therefore it is forbidden at enclosing levels:
                        arrayNotationPermitted = false ;
                    }
// System.out.println("ISIMPLICITCOMPLETEDIMENSION:"+indx[i]+" : "+ILUtils.isImplicitCompleteDimension(indx[i])) ;
                    if (!ILUtils.isImplicitCompleteDimension(indx[i])) {
                        allImplicitColons = false;
                    }
                }
                Tree newSubTree = solveIndexes(expressionTree.down(1), typeStackTail, arrayNotationPermitted, dimensionsAlreadyInExpressionTree);
                if (typeStack!=null) {
                    typeStack.head.second = newSubTree ;
                }
                if (allImplicitColons) {
                    // Then this level uses exclusively ":" notation, and thus we can remove arrayAccess:
                    expressionTree = newSubTree ;
                } else if (newSubTree!=expressionTree.down(1)) {
                    expressionTree.setChild(newSubTree, 1);
                }
            } else if (expressionTree.opCode()==ILLang.op_pointerAccess
                       // Exclude pointer accesses that are not C arrays:
                       && !ILUtils.isNullOrNone(expressionTree.down(2))) {
                TapList<TapPair<WrapperTypeSpec, Tree>> typeStackTail =
                    (typeStack==null ? null : typeStack.tail) ;
                Tree newIndex = solveIndexes(expressionTree.down(2), typeStackTail, arrayNotationPermitted, dimensionsAlreadyInExpressionTree);
                if (newIndex!=expressionTree.down(2)) {
                    expressionTree.setChild(newIndex, 2);
                }
                // Fortran array notation forbids array notation followed by deref i.e. *(A(:))
                Tree newSubTree = solveIndexes(expressionTree.down(1), typeStackTail, false, dimensionsAlreadyInExpressionTree);
                if (typeStack!=null) {
                    typeStack.head.second = newSubTree ;
                }
                if (newSubTree!=expressionTree.down(1)) {
                    expressionTree.setChild(newSubTree, 1);
                }
            } else if (!expressionTree.isAtom()) {
                Tree[] subTrees = expressionTree.children();
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    Tree newSubTree = solveIndexes(subTrees[i], typeStack, arrayNotationPermitted, dimensionsAlreadyInExpressionTree);
                    if (newSubTree != subTrees[i]) {
                        expressionTree.setChild(newSubTree, i + 1);
                    }
                }
            }
        }
// System.out.println(">>SOLVEINDEXES:"+expressionTree) ;
        return expressionTree;
    }

    /**
     * Build a tree of nested NestedLoopNode's, representing a structure of
     * nested do-loops with expressions at its leaves. This structure
     * corresponds to an object of type "typeSpec", and "expressionTree" is
     * a prefix to all the expressions at the leaves, corresponding to
     * the prefix of "typeSpec" that stops at "elementTypeSpec".
     * Boolean "insideElementTypeSpec" is false until we reach
     * a part of the "typeSpec" which is below this limit "elementTypeSpec",
     * in which case the corresponding expression must be built further.
     * The resulting structure of NestedLoopNode's is built by
     * refining its dangling leaves. At the end, the result is found
     * in the refined inNLT original argument.
     *
     * @param testTree the tree at this level, which can be used for testing e.g. ALLOCATED.
     */
    private void createNestedLoopTree(RefDescriptor.NestedLoopNode inNLT, Tree expressionTree, SymbolDecl rootDecl,
                                      WrapperTypeSpec typeSpec, TapList<String> extraInfo, Tree typeSizeModifier, Tree arraySize,
                                      boolean insideElementTypeSpec, boolean isScalar, boolean canHaveDiff,
                                      TapList ignoreStructure, boolean ignorePointed, TapList<ArrayDim> dimensionsAlreadyInExpressionTree,
                                      TapList<TapPair<WrapperTypeSpec, Tree>> typeStack, Tree testTree) {

        if (ignoreStructure != null && ignoreStructure.tail == null
                && ignoreStructure.head instanceof Boolean && (Boolean) ignoreStructure.head) {
            // Cases where the ignoreStructure tells us NOT to generate anything for this sub-nestedLoopTree :
            inNLT.expression = null; // To make sure no code is built for this leaf
            return;
        }

        // Find the moment when we go INSIDE the original expression:
        if (!insideElementTypeSpec && isSameTSorActualTS(typeSpec, elementType)) {
            insideElementTypeSpec = true;
        }

        int typeKind = SymbolTableConstants.UNKNOWNTYPE;
        if (typeSpec != null && typeSpec.wrappedType != null) {
            typeKind = typeSpec.wrappedType.kind();
        }

        if (typeKind == SymbolTableConstants.PRIMITIVETYPE && TapEnv.modeIsOverloading()) {
            assert typeSpec != null;
            TypeDecl tmp = targetUnit.callGraph().topBlock.symbolTable.getTypeDecl(typeSpec.baseTypeName());
            if (tmp != null) {
                typeSpec = tmp.typeSpec;
            }
            typeKind = (typeSpec==null || typeSpec.wrappedType==null
                        ? SymbolTableConstants.UNKNOWNTYPE
                        : typeSpec.wrappedType.kind()) ;
        }

// System.out.println("IN CREATENESTEDLOOPTREE ON TYPE "+typeSpec+" ELEMENTTYPE:"+elementType) ;
// System.out.println("   EXPRESSIONTREE:"+expressionTree) ;
// System.out.println("  inNLT NESTEDLOOPTREE NOW:"+(inNLT==null ? " null" : "")) ;
// if (inNLT!=null) {inNLT.dumpNestedLoopTree(2) ; System.out.println() ;}
        switch (typeKind) {
            case SymbolTableConstants.ARRAYTYPE: {
                assert typeSpec != null;
                ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
                ArrayDim[] dimensions = arrayTypeSpec.dimensions();
                ArrayDimPlusIter dimension;
                TapList<IterDescriptor> loopIterators = null;
                TapList<Tree> indexes = null;
                Tree indexTree;
                Tree indexesTree;
                boolean allImplicitColons = true;
                boolean noArrayAccess = (expressionTree.opCode() != ILLang.op_arrayAccess);
                for (ArrayDim arrayDim : dimensions) {
                    dimension = (ArrayDimPlusIter) arrayDim;
                    Tree hintTreeFromStack = TapList.cassq(typeSpec, typeStack);
                    if (hintTreeFromStack == null) {
                        dimension.iter.setHintArrayTreeForCallSize(expressionTree);
                    } else {
                        dimension.iter.setHintArrayTreeForCallSize(hintTreeFromStack);
                    }
                    if (dimension.iter.isArray) {
                        if (futureUse == PUSHPOP) {
                            dimension.iter.preciseDefaultLengthTree(targetUnit, usageSymbolTable);
                            arraySize = ILUtils.mulTree(dimension.iter.getLengthTree(), arraySize);
                        }
                        isScalar = false;
                    } else {
                        loopIterators = new TapList<>(dimension.iter, loopIterators);
                    }
// System.out.println("       ARRAYDIM:"+arrayDim+" ALREADY:"+dimensionsAlreadyInExpressionTree) ;
                    if (!TapList.contains(dimensionsAlreadyInExpressionTree, arrayDim)) {
                        if (insideElementTypeSpec || (multiDirIterator!=null && noArrayAccess)) {
                            indexTree = dimension.iter.buildIndexTree(usageSymbolTable, targetUnit);
// System.out.println("      ADDING INDEXTREE:"+indexTree+" FROM ITER:"+dimension.iter+" FROM DIM:"+dimension) ;
                            // If dimension.iter has chosen array notation, then since we are
                            // "insideElementTypeSpec", the index spans the entiere dimension
                            // and is therefore an implicit complete dimension
                            if (!dimension.iter.isArray) {
                                allImplicitColons = false;
                            }
                            indexes = new TapList<>(indexTree, indexes);
                        }
                    }
                }
                indexesTree = null ;
                if (indexes!=null) {
                    indexes = TapList.nreverse(indexes);
                    // indexes are now ordered from the slowest dim to the quickest.
                    // loopIterators are now ordered from the quickest dim to the slowest.
                    indexesTree = ILUtils.build(ILLang.op_expressions, indexes);
                }
                if ((insideElementTypeSpec && !allImplicitColons) || (multiDirIterator!=null && noArrayAccess)) {
                    if (indexesTree!=null) {
                        expressionTree =
                            ILUtils.build(ILLang.op_arrayAccess,
                                    ILUtils.copy(expressionTree),
                                    ILUtils.copy(indexesTree));
// System.out.println("   ARRAY BUILDS EXPRESSIONTREE:"+expressionTree) ;
                    }
                } else if (expressionTree.opCode() == ILLang.op_arrayAccess) {
                    // Else, it seems that the expressionTree already had its array indices at the current NLT level.
                    // Since we are going to create one deeper level with the same expressionTree,
                    // we here remove the arrayAccess layer around the current NLT level's expression:
                    inNLT.expression = ILUtils.copy(inNLT.expression.down(1));
                }
                if (targetUnit.isFortran() && rootDecl != null && TypeSpec.isA(rootDecl.type(), SymbolTableConstants.POINTERTYPE)) {
                    // Since we are going to access array elements, test that the array is allocated:
                    inNLT.protectAllocatedAssociated =
                            ILUtils.buildCall(
                                    ILUtils.build(ILLang.op_ident, rootDecl.hasModifier("allocatable") ? "ALLOCATED" : "ASSOCIATED"),
                                    ILUtils.build(ILLang.op_expressions, ILUtils.copy(testTree)));
                    rootDecl = null; //means that the test on testTree is already made.
                }
                if (indexesTree!=null) {
                    testTree = ILUtils.build(ILLang.op_arrayAccess,
                                 ILUtils.copy(testTree),
                                 indexesTree);
                }
                if (loopIterators != null) {
                    inNLT.nodes =
                        new TapList<>(new RefDescriptor.NestedLoopNode(expressionTree, typeSpec, extraInfo, null, null,
                                    isScalar, loopIterators, null, canHaveDiff),
                                    inNLT.nodes);
                    inNLT = inNLT.nodes.head;
                }
                createNestedLoopTree(inNLT, expressionTree, rootDecl,
                        arrayTypeSpec.elementType(), null,
                        typeSizeModifier, arraySize, insideElementTypeSpec, isScalar, canHaveDiff,
                        ignoreStructure, ignorePointed, dimensionsAlreadyInExpressionTree, typeStack, testTree);
                break;
            }
            case SymbolTableConstants.POINTERTYPE: {
                assert typeSpec != null;
                PointerTypeSpec pointerTypeSpec = (PointerTypeSpec) typeSpec.wrappedType;
                ArrayDim rawDimension = pointerTypeSpec.offsetLength;
                ArrayDimPlusIter dimension = null;
                if (rawDimension instanceof ArrayDimPlusIter) {
                    dimension = (ArrayDimPlusIter) rawDimension;
                }
                // In case of C, when we want to describe a pointer and not its destination,
                //  rawDimension exists but is just an ArrayDim. Then dimension is null and
                //  this will rightfully prevent this algo to go inside the pointer destination.
                if (!ignorePointed || dimension != null) {
                    // If offsetLength is not null, it means this is in fact not a simple F90 pointer,
                    // but rather a C pointer that represents (a section of) a C array.
                    // In this case, it is this array that we want to access.

                    if (dimension != null && dimension.iter != null) {
                        if (insideElementTypeSpec || isSameTSorActualTS(pointerTypeSpec.destinationType, elementType)) {
                            dimension.iter.setHintArrayTreeForCallSize(expressionTree);
                        } else {
                            dimension.iter.setHintArrayTreeForCallSize(TapList.cassq(typeSpec, typeStack));
                        }
                    }
                    if (ignoreStructure != null && ignoreStructure.head instanceof Boolean)
                    // We are going to build code for the destination: peel off the ignoreStructure for the pointer itself:
                    {
                        ignoreStructure = ignoreStructure.tail;
                    }
                    if (dimension != null && dimension.iter.isArray && futureUse == PUSHPOP) {
                        // if this is complete access to a C array-like pointer, when the final use is PUSH/POP,
                        // make sure we use a pusharray/poparray() :
                        dimension.iter.preciseDefaultLengthTree(targetUnit, usageSymbolTable);
                        arraySize = ILUtils.mulTree(dimension.iter.getLengthTree(), arraySize);
                        if (expressionTree.opCode() == ILLang.op_pointerAccess) {
                            expressionTree = ILUtils.copy(expressionTree.down(1));
                        }
                        createNestedLoopTree(inNLT, expressionTree, rootDecl, pointerTypeSpec.destinationType, null,
                                typeSizeModifier, arraySize, insideElementTypeSpec, false, canHaveDiff,
                                ignoreStructure, ignorePointed, dimensionsAlreadyInExpressionTree, typeStack, testTree);
                    } else {
                        TapList<IterDescriptor> loopIterators = null;
                        if (dimension != null) {
                            if (dimension.iter.isArray) {
                                isScalar = false;
                            } else {
                                loopIterators = new TapList<>(dimension.iter, null);
                            }
                        }
                        Tree indexTree = null;
                        if (dimension != null) {
                            indexTree = dimension.iter.buildIndexTree(usageSymbolTable, targetUnit);
                        }
                        if (insideElementTypeSpec) {
                            expressionTree = ILUtils.build(ILLang.op_pointerAccess,
                                    ILUtils.copy(expressionTree),
                                    ILUtils.copy(indexTree));
                        }
                        inNLT.nodes =
                            new TapList<>(new RefDescriptor.NestedLoopNode(expressionTree, typeSpec, extraInfo, null, null,
                                        isScalar, loopIterators, null, canHaveDiff),
                                        inNLT.nodes);
                        inNLT = inNLT.nodes.head;
                        if (rootDecl != null) {
                            // Since we are going to deref the pointer,
                            //  test that the pointer is either allocated or associated:
                            if (targetUnit.isFortran()) {
                                inNLT.protectAllocatedAssociated =
                                        ILUtils.buildCall(
                                                ILUtils.build(ILLang.op_ident,
                                                        rootDecl.hasModifier("allocatable") ? "ALLOCATED" : "ASSOCIATED"),
                                                ILUtils.build(ILLang.op_expressions, ILUtils.copy(testTree)));
                                rootDecl = null; //means that the test on testTree is already made.
                            } else if (targetUnit.isC() && rawDimension == null
                                    && rootDecl.kind == SymbolTableConstants.VARIABLE
                                    && ((VariableDecl) rootDecl).formalArgRank == SymbolTableConstants.NOT_A_FORMAL_ARG) {
                                inNLT.protectAllocatedAssociated = ILUtils.copy(testTree);
                                rootDecl = null; //means that the test on testTree is already made.
                            }
                        }
                        testTree = ILUtils.build(ILLang.op_pointerAccess,
                                ILUtils.copy(testTree),
                                indexTree);
                        createNestedLoopTree(inNLT, expressionTree, rootDecl, pointerTypeSpec.destinationType, null,
                                typeSizeModifier, arraySize, insideElementTypeSpec, isScalar, canHaveDiff,
                                ignoreStructure, ignorePointed, dimensionsAlreadyInExpressionTree, typeStack, testTree);
                    }
                } else if (ignoreStructure != null
                        && ignoreStructure.head instanceof Boolean
                        && (Boolean) ignoreStructure.head) {
                    // Case where we must ignore this part of the nestedLoopTree
                    inNLT.expression = null; // To make sure no code is built for this leaf
                } else {
                    // otherwise, we access only the pointer's address value and don't go inside.
                    // TODO: this problem is caused by identification of C arrays and pointers.
                    inNLT.nodes =
                            new TapList<>(new RefDescriptor.NestedLoopNode(expressionTree, typeSpec, extraInfo, typeSizeModifier, arraySize,
                                    isScalar, null, null, canHaveDiff),
                                    inNLT.nodes);
                    this.holdsAPointer = true;
                }
                break;
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                assert typeSpec != null;
                CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec) typeSpec.wrappedType;
                FieldDecl[] fields = compositeTypeSpec.fields;
                TapList subIgnoreStructs = TapList.reverse(ignoreStructure);
                TapList subIgnoreStruct;
                boolean fieldCanHaveDiff;
                Tree fieldNameTree;
                for (int i = fields.length - 1; i >= 0; i--) {
                    fieldCanHaveDiff = canHaveDiff &&
                            (compositeTypeSpec.isDifferentiatedField(i)
                                    || !TapEnv.doActivity() && TypeSpec.isDifferentiableType(fields[i].type()));
                    if (subIgnoreStructs != null && subIgnoreStructs.head instanceof TapList) {
                        subIgnoreStruct = (TapList) subIgnoreStructs.head;
                        subIgnoreStructs = subIgnoreStructs.tail;
                    } else {
                        subIgnoreStruct = subIgnoreStructs;
                    }
                    if (fields[i] != null && (!isDiff || fieldCanHaveDiff)) {
                        fieldNameTree = ILUtils.build(ILLang.op_ident, fields[i].symbol);
                        ILUtils.setFieldRank(fieldNameTree, i);
                        createNestedLoopTree(inNLT,
                                ILUtils.build(ILLang.op_fieldAccess,
                                        ILUtils.copy(expressionTree),
                                        ILUtils.copy(fieldNameTree)),
                                fields[i],
                                fields[i].type(), fields[i].extraInfo(), typeSizeModifier,
                                ILUtils.copy(arraySize),
                                insideElementTypeSpec, isScalar, fieldCanHaveDiff,
                                subIgnoreStruct, ignorePointed, dimensionsAlreadyInExpressionTree, typeStack,
                                ILUtils.build(ILLang.op_fieldAccess,
                                        ILUtils.copy(testTree),
                                        ILUtils.copy(fieldNameTree)));
                    }
                }
                break;
            }
            case SymbolTableConstants.MODIFIEDTYPE: {
                assert typeSpec != null;
                ModifiedTypeSpec modifiedTypeSpec = (ModifiedTypeSpec) typeSpec.wrappedType;
                Tree sizeModifier;
                int sizeModifierInt = modifiedTypeSpec.sizeModifierValue();
                if (sizeModifierInt != -1) {
                    sizeModifier = ILUtils.build(ILLang.op_intCst, sizeModifierInt);
                } else {
                    sizeModifier = ILUtils.copy(modifiedTypeSpec.sizeModifier);
                }
                createNestedLoopTree(inNLT, expressionTree, rootDecl, modifiedTypeSpec.elementType(), null,
                        sizeModifier == null ? typeSizeModifier : sizeModifier,
                        arraySize, insideElementTypeSpec, isScalar, canHaveDiff,
                        ignoreStructure, ignorePointed, dimensionsAlreadyInExpressionTree, typeStack, testTree);
                // Cosmetic: attach the  modifiedtype to the primitivetype's NestedLoopTree,
                //  so that we may initialize with 0.D0 instead of 0.0:
                if (inNLT.nodes!=null) {
                    inNLT.nodes.head.modifiedTypeAbove = modifiedTypeSpec ;
                }
                break;
            }
            case SymbolTableConstants.PRIMITIVETYPE:
            default: {
                TapList<IterDescriptor> loopIterators = null;
                // When multiDirIterator!=null, this implies in particular
                // that we are referring to the differentiated variable ! (if it exists, of course).
                if (multiDirIterator != null) {
                    if (!multiDirIterator.justAnIndex()) {
                        if (multiDirIterator.isArray) {
                            isScalar = false;
                        } else {
                            loopIterators = new TapList<>(multiDirIterator, null);
                        }
                    }
                    Tree multiDirIndexTree = multiDirIterator.buildIndexTree(usageSymbolTable, targetUnit);
                    expressionTree = ILUtils.copy(expressionTree);
                    if (expressionTree.opCode() == ILLang.op_arrayAccess) {
                        expressionTree.down(2).addChild(multiDirIndexTree, -1);
                    } else if (!(multiDirIterator.isArray && multiDirIterator.isImplicitCompleteDimension)) {
                        expressionTree = ILUtils.build(ILLang.op_arrayAccess,
                                           expressionTree,
                                           ILUtils.build(ILLang.op_expressions, multiDirIndexTree));
                    }
                }

                if (expressionTree.opCode() == ILLang.op_pointerAccess
                        && expressionTree.down(2).opCode() == ILLang.op_none
                        && rootDecl != null
                        && inNLT.protectAllocatedAssociated == null) {
                    if (targetUnit.isC()
                            && rootDecl.kind == SymbolTableConstants.VARIABLE
                            && ((VariableDecl) rootDecl).formalArgRank == SymbolTableConstants.NOT_A_FORMAL_ARG
                            || TapEnv.inputLanguage() == TapEnv.MIXED) {
                        // TODO seulement si c'est un pointer avec malloc?
                        inNLT.protectAllocatedAssociated = ILUtils.copy(expressionTree.down(1));
                    }
                }
                if (multiDirIterator != null && !multiDirIterator.justAnIndex()) {
                    inNLT.nodes =
                        new TapList<>(new RefDescriptor.NestedLoopNode(expressionTree, typeSpec, extraInfo, null, null,
                                    isScalar, loopIterators, null, canHaveDiff),
                                    inNLT.nodes);
                    inNLT = inNLT.nodes.head;
                }
                inNLT.nodes =
                        new TapList<>(new RefDescriptor.NestedLoopNode(expressionTree, typeSpec, extraInfo, typeSizeModifier, arraySize,
                                isScalar, null, null, canHaveDiff),
                                inNLT.nodes);
                break;
            }
        }
// System.out.println("DONE CREATENESTEDLOOPTREE ON TYPE "+typeSpec) ;
// System.out.println("  inNLT NESTEDLOOPTREE NOW:"+(inNLT==null ? " null" : "")) ;
// if (inNLT!=null) {inNLT.dumpNestedLoopTree(2) ; System.out.println() ;}
    }

    /**
     * Collect into "collected" all the trees found at the leaves
     * of the nestedLoopTree tree of this RefDescriptor.
     */
    public TapList<Tree> collectTrees(TapList<Tree> collected) {
        return collectTreesRec(nestedLoopTree, collected);
    }

    public void forceStaticSave(RefDescriptor saveVarVarDescriptor) {
        companionVarDescriptor = saveVarVarDescriptor;
    }

    public void setCompanionVarDescriptor(RefDescriptor otherVarDescriptor) {
        companionVarDescriptor = otherVarDescriptor;
    }

    public boolean usesStaticSave() {
        return companionVarDescriptor != null;
    }

    public Tree makePush() {
        Tree resultTree;
        if (companionVarDescriptor != null) {
            mayProtectAccesses = false; //TODO: try protecting accesses ?
            resultTree = companionVarDescriptor.makeAssign(this);
            checkUnknownDimensionsForC(resultTree, "assign");
        } else {
            resultTree = makeStack(nestedLoopTree, "push", true);
            //TODO CHECK MAKEPUSH OF "+patternTree+" ZI:"+zoneInfo
            checkUnknownDimensionsForC(resultTree, "push");
        }
        return resultTree;
    }

    public Tree makePop() {
        Tree resultTree;
        if (companionVarDescriptor != null) {
            mayProtectAccesses = false; //TODO: try protecting accesses ?
            resultTree = makeAssign(companionVarDescriptor);
            checkUnknownDimensionsForC(resultTree, "assign");
        } else {
            resultTree = makeStack(nestedLoopTree, "pop", false);
            //TODO CHECK MAKEPOP  OF "+patternTree+" ZI:"+zoneInfo
            checkUnknownDimensionsForC(resultTree, "pop");
        }
        return resultTree;
    }

    /**
     * Creates the instruction that rebases all pointers and/or their diff pointers in the
     * expression represented by this RefDescriptor. We assume the nestedLoopTree of this
     * RefDescriptor has been prepared by this.prepareForStack().
     * Ignores the primal pointers for the True leaves of "ignorePrimal".
     * Ignores the diff   pointers for the True leaves of "ignoreDiff".
     *
     * @param ignorePrimal the tree of Booleans indicating the leaves for which no rebase is needed (for the primal)
     * @param ignoreDiff   the tree of Booleans indicating the leaves for which no rebase is needed (for the diff)
     * @param diffBaseVar  the differentiated root variable, used when some diff pointer must be rebased.
     * @param numRebase    the "unique" number used to build a label to identify this rebase for debugAD.
     * @return an instruction that does the requested rebase of POPped pointers.
     */
    public Tree makeRebase(TapList ignorePrimal, TapList ignoreDiff, Tree diffBaseVar, int numRebase) {
        Tree resultTree = makeRebase(nestedLoopTree, ignorePrimal, ignoreDiff, diffBaseVar, numRebase);
        checkUnknownDimensionsForC(resultTree, "rebase");
        return resultTree;
    }

    private Tree makeStack(RefDescriptor.NestedLoopNode node, String oper, boolean directOrder) {
        Tree result;
        if (node.nodes != null) {
            // then this node creates a do-loop
            // around the code created by node.nodes:
            TapList<IterDescriptor> loopsOutside = node.loopIterators;
            TapList<NestedLoopNode> nodes = node.nodes;
            TapList<Tree> codeInside = null;
            while (nodes != null) {
                Tree subTree = makeStack(nodes.head, oper, directOrder) ;
                if (subTree!=null) codeInside = new TapList<>(subTree, codeInside);
                nodes = nodes.tail;
            }
            if (directOrder) {
                codeInside = TapList.nreverse(codeInside);
            }
            if (codeInside != null && codeInside.tail == null) {
                result = codeInside.head;
            } else {
                result = ILUtils.build(ILLang.op_blockStatement, codeInside);
            }
            while (loopsOutside != null) {
                Tree loopHeader =
                        loopsOutside.head
                                .buildDoHeaderTree(usageSymbolTable, targetUnit, directOrder);
                result = ILUtils.build(ILLang.op_loop, null, null,
                        loopHeader, result);
                loopsOutside = loopsOutside.tail;
            }
        } else if (node.branchIsEmpty()) {
            // this is the case of branches or leaves in the nestedLoopTree that are empty because
            // the ignore* filtering system has removed all code for them:
            result = null;
        } else {
            // else this node creates a Stack call
            // on the expression contained in this leaf node:
            Tree expr = node.expression;
            WrapperTypeSpec type = node.type;
            Tree sizeModifier = ILUtils.copy(node.typeSizeModifier);
            if (sizeModifier != null && sizeModifier.opCode() == ILLang.op_ident) {
                if (sizeModifier.stringValue().equals(TapEnv.UNDEFINEDSIZE)
                        || sizeModifier.stringValue().equals("signed")
                        || sizeModifier.stringValue().equals("unsigned")) {
                    sizeModifier = null;
                } else if (sizeModifier.stringValue().equals("short")
                        || sizeModifier.stringValue().equals("long")) {
                    sizeModifier = ILUtils.build(ILLang.op_ident, "SIZE_OF_" +
                            sizeModifier.stringValue());
                }
            } else if (sizeModifier != null && sizeModifier.opCode() == ILLang.op_nameEq) {
                sizeModifier = sizeModifier.down(2);
            }
            Tree arraySize = ILUtils.copy(node.arraySize);
            String typeName = (type==null ? "Undefined" : type.baseTypeName());
            String sizedType;
            Tree lengthMultiplier = null;
            result = null;
            //TODO: [llh] should call WrapperTypeSpec.buildTypeNameForProcName(), but it's not so easy :-(
            if (nbControls >= 2) {
                int nBits = 1;
                int curCtr = nbControls - 1;
                while (curCtr >= 2) {
                    curCtr = curCtr / 2;
                    nBits++;
                }
                sizedType = "Control" + nBits + "b";
            } else if (typeName.equals("character")) {
                sizedType = "Character";
                if (sizeModifier != null && !(sizeModifier.opCode() == ILLang.op_intCst &&
                        sizeModifier.intValue() == -2)) {
                    lengthMultiplier = sizeModifier;
                }
            } else if (typeName.equals("integer") || typeName.equals("float") || typeName.equals("complex")) {
                sizedType = (typeName.equals("integer") ? "Integer"
                        : (typeName.equals("complex") ? "Complex"
                        : "Real"));
                int baseSize;
                if (sizeModifier == null) {
                    baseSize = (typeName.equals("integer") ? TapEnv.get().integerSize
                            : (typeName.equals("complex") ? 2 * TapEnv.get().realSize
                            : TapEnv.get().realSize));
                } else if (sizeModifier.opCode() == ILLang.op_intCst) {
                    if (sizeModifier.intValue() == -2) {
                        baseSize = (typeName.equals("integer") ? 2 * TapEnv.get().integerSize
                                : (typeName.equals("complex") ? 2 * TapEnv.get().doubleRealSize
                                : TapEnv.get().doubleRealSize));
                    } else if (sizeModifier.intValue() == -3) { // Convention: -3 means "short". short float seem ridiculous?
                        baseSize = (typeName.equals("integer") ? TapEnv.get().integerSize / 2
                                : (typeName.equals("complex") ? 2 * TapEnv.get().realSize
                                : TapEnv.get().realSize));
                    } else if (sizeModifier.intValue() == -4) {
                        baseSize = (typeName.equals("integer") ? 4 * TapEnv.get().integerSize
                                : (typeName.equals("complex") ? 2 * TapEnv.get().doubleDoubleRealSize
                                : TapEnv.get().doubleDoubleRealSize));
                    } else {
                        baseSize = sizeModifier.intValue();
                    }
                } else {
                    baseSize = typeName.equals("integer") ? TapEnv.get().integerSize : TapEnv.get().realSize;
                    lengthMultiplier = ILUtils.buildSmartMulDiv(sizeModifier, -1,
                            ILUtils.build(ILLang.op_intCst, baseSize));
                }
                sizedType = sizedType + baseSize;
            } else if (typeName.equals("pointer")) {
                sizedType = "Pointer" + TapEnv.get().pointerSize;
            } else if (typeName.equals("boolean")) {
                sizedType = "Boolean";
            } else if (typeName.equals("Undefined")) {
                sizedType = "UNKNOWNTYPE";
            } else {
                sizedType = typeName;
            }
            if (lengthMultiplier != null) {
                arraySize = ILUtils.mulTree(lengthMultiplier, arraySize);
            }
            TapList<Tree> finalArgs = null;
            String funcName = oper + sizedType;
            boolean argIsArray = arraySize != null &&
                    !(node.isScalar && ILUtils.evalsToOne(arraySize));
            boolean isAllocatableArray = false;
            WrapperTypeSpec typeSpec = defSymbolTable.typeOf(expr);
            if (typeSpec != null) {
                allocationCheckingNeeded(expr);
                if (typeName.equals("pointer")) {
                    isAllocatableArray = defSymbolTable.varHasModifier(expr, "allocatable");
                    if (isAllocatableArray) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, expr, "(AD07) Data-Flow recovery needs to restore allocatable pointer "+ILUtils.toString(expr) + ", not implemented yet") ;
                    }
                }
            }

            if (argIsArray) {
                funcName = funcName + "Array";
                finalArgs = new TapList<>(arraySize, null);
            }

            expr = ILUtils.copy(expr);
            if (!argIsArray && MixedLanguageInfos.passesByValue(targetUnit.language(), targetUnit.language()) //I'd prefer C as target language...
                    && "pop".equals(oper)) {
                if (expr.opCode() == ILLang.op_pointerAccess &&
                        ILUtils.isNullOrNone(expr.down(2))) {
                    expr = expr.cutChild(1);
                } else {
                    expr = ILUtils.build(ILLang.op_address, expr);
                }
            }
            if (!argIsArray && targetUnit.isC() && typeName.equals("pointer")
                    && "pop".equals(oper)) {
                expr = ILUtils.build(ILLang.op_cast,
                        ILUtils.build(ILLang.op_pointerType,
                                ILUtils.build(ILLang.op_pointerType,
                                        ILUtils.build(ILLang.op_void))),
                        expr);
            }
            if (targetUnit.isFortran9x() && typeName.equals("pointer")
                    && (oper.equals("push") || oper.equals("pop"))) {
                targetUnit.usesISO_C_BINDING = true;
                if (!isAllocatableArray) {
                    if (oper.equals("push")) {
                        expr = ILUtils.buildCLoc(expr, "C_LOC");
                    } else {
                        TapPair<NewSymbolHolder, Tree> buildPair = NewSymbolHolder.buildCptrTree(expr, targetUnit.privateSymbolTable(),
                                false, "cptr", targetUnit, typeSpec, false);
                        NewSymbolHolder cptrSH = buildPair.first;
                        result = buildPair.second;
                        expr = cptrSH.makeNewRef(targetUnit.privateSymbolTable());
                    }
                }
            }

            finalArgs = new TapList<>(expr, finalArgs);

            if (targetUnit.isFortran()) {
                funcName = funcName.toUpperCase();
            }

            Tree callFunc = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, funcName),
                    ILUtils.build(ILLang.op_expressions, finalArgs));
            if (result == null) {
                result = callFunc;
            } else {
                result.addChild(callFunc, 1);
            }
            if (TapEnv.get().numberPushPops && ppLabel >= 0) {
                result.setAnnotation("endLineComment", ILUtils.build(ILLang.op_stringCst, "pp:" + ppLabel));
            }
        }
        // Mechanism to protect PUSH/POP against unallocated arrays:
        if (result != null && mayProtectAccesses && node.expression != null && node.protectAllocatedAssociated != null) {
            if (oper.equals("push")) {
                RefDescriptor branchVarRefDescriptor =
                        new RefDescriptor(ILUtils.build(ILLang.op_intCst, 0), null,
                                usageSymbolTable, usageSymbolTable, null,
                                null, false, null, targetUnit);
                branchVarRefDescriptor.makeFixedControl(2);
                branchVarRefDescriptor.prepareForStack(targetUnit, -1, null, true);
                Tree pushZero = branchVarRefDescriptor.makePush();
                branchVarRefDescriptor =
                        new RefDescriptor(ILUtils.build(ILLang.op_intCst, 1), null,
                                usageSymbolTable, usageSymbolTable, null,
                                null, false, null, targetUnit);
                branchVarRefDescriptor.makeFixedControl(2);
                branchVarRefDescriptor.prepareForStack(targetUnit, -1, null, true);
                Tree pushOne = branchVarRefDescriptor.makePush();
                result = ILUtils.build(ILLang.op_if,
                        ILUtils.copy(node.protectAllocatedAssociated),
                        ILUtils.build(ILLang.op_blockStatement,
                                result,
                                pushOne),
                        pushZero);
            } else if ("pop".equals(oper)) {
                if (toBranchVariable.first == null) {
                    toBranchVariable.first = new NewSymbolHolder("branch", targetUnit, toBranchVariable.second, -1) ;
                }
                toBranchVariable.first.declarationLevelMustInclude(targetUnit.privateSymbolTable());
                Tree expr = toBranchVariable.first.makeNewRef(usageSymbolTable);
                RefDescriptor refDescriptor =
                        new RefDescriptor(expr, null,
                                usageSymbolTable, usageSymbolTable, null,
                                null, false, null, targetUnit);
                refDescriptor.makeFixedControl(2);
                refDescriptor.prepareForStack(targetUnit, -1, null, true);
                Tree popZeroOrOne = refDescriptor.makePop();
                result = ILUtils.build(ILLang.op_blockStatement,
                        popZeroOrOne,
                        ILUtils.build(ILLang.op_if,
                                ILUtils.build(ILLang.op_eq,
                                        ILUtils.copy(expr),
                                        ILUtils.build(ILLang.op_intCst, 1)),
                                result));
            } else {
                result = ILUtils.build(ILLang.op_if, ILUtils.copy(node.protectAllocatedAssociated), result);
            }
        }
        return result;
    }

    /**
     * Creates the instruction that rebases all pointers and/or their diff pointers in the expression represented by "node".
     * Ignores the primal pointers for the True leaves of "ignorePrimal".
     * Ignores the diff   pointers for the True leaves of "ignoreDiff".
     *
     * @param node         the NestedLoopTree subtree for the current sub-expression to be rebased
     * @param ignorePrimal the tree of Booleans indicating the leaves for which no rebase is needed (for the primal)
     * @param ignoreDiff   the tree of Booleans indicating the leaves for which no rebase is needed (for the diff)
     * @param diffBaseVar  the differentiated root variable, used when some diff pointer must be rebased.
     * @param numRebase    the "unique" number used to build a label to identify this rebase for debugAD.
     * @return an instruction that does the requested rebase of POPped pointers.
     */
    private Tree makeRebase(RefDescriptor.NestedLoopNode node, TapList ignorePrimal, TapList ignoreDiff, Tree diffBaseVar, int numRebase) {
        Tree result;
        if (node.nodes != null) {
            // then this node creates a do-loop
            // around the code created by node.nodes:
            TapList<IterDescriptor> loopsOutside = node.loopIterators;
            TapList<NestedLoopNode> nodes = node.nodes;
            TapList<Tree> codeInside = null;
            RefDescriptor.NestedLoopNode subNode;
            TapList subIgnorePrimal;
            TapList subIgnoreDiff;
            int rank;
            Tree subTree;
            while (nodes != null) {
                subNode = nodes.head;
                subIgnorePrimal = ignorePrimal;
                subIgnoreDiff = ignoreDiff;
                if (subNode.expression.opCode() == ILLang.op_fieldAccess) {
                    rank = ILUtils.getFieldRank(subNode.expression.down(2));
                    if (ignorePrimal != null && ignorePrimal.head instanceof TapList) {
                        subIgnorePrimal = (TapList) TapList.nth(ignorePrimal, rank);
                    }
                    if (ignoreDiff != null && ignoreDiff.head instanceof TapList) {
                        subIgnoreDiff = (TapList) TapList.nth(ignoreDiff, rank);
                    }
                }
                subTree = makeRebase(nodes.head,
                        subIgnorePrimal,
                        node.canHaveDiff ? subIgnoreDiff : new TapList<>(Boolean.TRUE, null),
                        node.canHaveDiff ? diffBaseVar : null, numRebase);
                if (subTree != null) {
                    codeInside = new TapList<>(subTree, codeInside);
                }
                nodes = nodes.tail;
            }
            if (codeInside == null) {
                result = null;
            } else {
                if (codeInside.tail == null) {
                    result = codeInside.head;
                } else {
                    result = ILUtils.build(ILLang.op_blockStatement, codeInside);
                }
                while (loopsOutside != null) {
                    Tree loopHeader =
                            loopsOutside.head
                                    .buildDoHeaderTree(usageSymbolTable, targetUnit, false);
                    result = ILUtils.build(ILLang.op_loop, null, null,
                            loopHeader, result);
                    loopsOutside = loopsOutside.tail;
                }
            }
        } else if (node.branchIsEmpty() || TapList.trueForAll(ignorePrimal) && (!node.canHaveDiff || diffBaseVar == null || TapList.trueForAll(ignoreDiff))) {
            result = null;
        } else {
            // else this node creates a Rebase call
            // on the expression contained in this leaf node:
            Tree expr = node.expression;
            WrapperTypeSpec type = node.type;
            Tree sizeModifier = ILUtils.copy(node.typeSizeModifier);
            result = null;
            if (sizeModifier != null && sizeModifier.opCode() == ILLang.op_ident) {
                if (sizeModifier.stringValue().equals(TapEnv.UNDEFINEDSIZE)
                        || sizeModifier.stringValue().equals("signed")
                        || sizeModifier.stringValue().equals("unsigned")) {
                    sizeModifier = null;
                } else if (sizeModifier.stringValue().equals("short")
                        || sizeModifier.stringValue().equals("long")) {
                    sizeModifier = ILUtils.build(ILLang.op_ident, "SIZE_OF_" +
                            sizeModifier.stringValue());
                }
            } else if (sizeModifier != null && sizeModifier.opCode() == ILLang.op_nameEq) {
                sizeModifier = sizeModifier.down(2);
            }
            Tree arraySize = ILUtils.copy(node.arraySize);
            boolean argIsArray = arraySize != null &&
                    !(node.isScalar && ILUtils.evalsToOne(arraySize));
            TapList<Tree> finalArgs = null;
            if (numRebase != -1) {
                finalArgs = new TapList<>(ILUtils.build(ILLang.op_stringCst, "Reb" + numRebase), null);
            }
            String funcName = !node.canHaveDiff || diffBaseVar == null || TapList.trueForAll(ignoreDiff) ? "ADMM_rebase" : "ADMM_rebaseShadowed";
            if (argIsArray) {
                funcName = funcName + "Array";
                finalArgs = new TapList<>(arraySize, finalArgs);
            }
            expr = ILUtils.copy(expr);
            if (!argIsArray && MixedLanguageInfos.passesByValue(targetUnit.language(), targetUnit.language())) { //I'd prefer C as target language...
                if (expr.opCode() == ILLang.op_pointerAccess &&
                        ILUtils.isNullOrNone(expr.down(2))) {
                    expr = expr.cutChild(1);
                } else {
                    expr = ILUtils.build(ILLang.op_address, expr);
                }
            }
            if (targetUnit.isC() && !argIsArray) {
                expr = ILUtils.build(ILLang.op_cast,
                        ILUtils.build(ILLang.op_pointerType,
                                ILUtils.build(ILLang.op_pointerType,
                                        ILUtils.build(ILLang.op_void))),
                        expr);
            }
            Tree exprFortran = null;
            Tree primalExpr = null;
            if (targetUnit.isFortran9x() && !usageSymbolTable.varHasModifier(expr, "allocatable")) {
                TapPair<NewSymbolHolder, Tree> buildPair =
                        NewSymbolHolder.buildCptrTree(expr, targetUnit.privateSymbolTable(), TapList.trueForAll(ignorePrimal), "cptr", targetUnit, type, true);
                NewSymbolHolder cptrSH = buildPair.first;
                result = buildPair.second;
                exprFortran = cptrSH.makeNewRef(usageSymbolTable);
                if (TapList.trueForAll(ignorePrimal)) {
                    primalExpr = exprFortran;
                }
            }
            if (node.canHaveDiff && diffBaseVar != null && !TapList.trueForAll(ignoreDiff)) {
                Tree diffExpr;
                if (targetUnit.isFortran9x() && !usageSymbolTable.varHasModifier(expr, "allocatable")) {
                    Tree tmpDiffExpr = ILUtils.replaceIdentStringOccurences(ILUtils.copy(expr), ILUtils.baseName(node.expression), diffBaseVar);
                    TapPair<NewSymbolHolder, Tree> buildPair = NewSymbolHolder.buildCptrTree(tmpDiffExpr, targetUnit.privateSymbolTable(),
                            false, "cptrb", targetUnit, type, true);
                    NewSymbolHolder cptrSH = buildPair.first;
                    Tree resultb = buildPair.second;
                    result.addChild(ILUtils.copy(resultb.down(1)), 2);
                    result.addChild(ILUtils.copy(resultb.down(2)), 4);
                    diffExpr = cptrSH.makeNewRef(usageSymbolTable);
                } else {
                    diffExpr = ILUtils.replaceIdentStringOccurences(ILUtils.copy(expr), ILUtils.baseName(node.expression), diffBaseVar);
                }
                finalArgs = new TapList<>(diffExpr, finalArgs);
            }
            if (exprFortran != null) {
                expr = exprFortran;
            }

            if (!TapList.trueForAll(ignorePrimal)) {
                primalExpr = expr;
            } else {
                if (primalExpr == null) {
                    primalExpr = ILUtils.build(ILLang.op_intCst, 0);
                }
                if (targetUnit.isC()) {
                    primalExpr = ILUtils.build(ILLang.op_cast,
                            ILUtils.build(ILLang.op_pointerType,
                                    ILUtils.build(ILLang.op_void)),
                            primalExpr);
                }
            }
            finalArgs = new TapList<>(primalExpr, finalArgs);
            if (targetUnit.isFortran()) {
                funcName = funcName.toUpperCase();
            }
            Tree callFunc = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, funcName),
                    ILUtils.build(ILLang.op_expressions, finalArgs));
            if (result == null) {
                result = callFunc;
            } else {
                result.addChild(callFunc, result.length() / 2 + 1);
            }
        }
        return result;
    }

    private void checkUnknownDimensionsForC(Tree resultTree, String action) {
        // We believe that the "Not sure" problem can happen only when manipulating complete zones, 
        // which happens for global reinitializations etc, or for the push/pops of snapshots before calls :
        if (resultTree != null && targetUnit.isC() && zoneInfo != null) {
            WrapperTypeSpec type1 = completeType;
            WrapperTypeSpec type2 = knownRootTypeSpec;
            if (type1 == null) {
                type1 = usageSymbolTable.typeOf(patternTree);
            }
            if (ILUtils.containsUnknownDimension(patternTree)) {
                TapEnv.fileWarning(TapEnv.MSG_ALWAYS, null, "(AD15) Not sure how to "
                        + action + (isDiff ? " the derivative " : " ") + ILUtils.toString(patternTree)
                        + " of type:" + type1.showType() + (type2 == null ? "" : " (formal:" + type2.showType() + ')')
                        + " with instruction:" + ILUtils.toString(resultTree));
            }
        }
    }

    public Tree makeInitialize() {
        Tree resultTree = makeInitialize(nestedLoopTree);
        checkUnknownDimensionsForC(resultTree, "initialize");
        return resultTree;
    }

    private Tree makeInitialize(RefDescriptor.NestedLoopNode node) {
        Tree result;
        if (node.nodes != null) {
            // then this node creates a do-loop
            // around the code created by node.nodes :
            TapList<IterDescriptor> loopsOutside = node.loopIterators;
            TapList<NestedLoopNode> nodes = node.nodes;
            TapList<Tree> codeInside = null;
            while (nodes != null) {
                Tree subTree = makeInitialize(nodes.head) ;
                if (subTree!=null) codeInside = new TapList<>(subTree, codeInside);
                nodes = nodes.tail;
            }
            codeInside = TapList.nreverse(codeInside);
            if (codeInside == null) {
                result = null;
            } else if (codeInside.tail == null) {
                result = codeInside.head;
            } else {
                result = ILUtils.build(ILLang.op_blockStatement, codeInside);
            }
            while (result!=null && loopsOutside!=null) {
                Tree loopHeader =
                        loopsOutside.head
                                .buildDoHeaderTree(usageSymbolTable, targetUnit, true);
                result = ILUtils.build(ILLang.op_loop, null, null,
                        loopHeader, result);
                loopsOutside = loopsOutside.tail;
            }
        } else if (node.branchIsEmpty()) {
            // Case of branches or leaves in the nestedLoopTree that are empty because
            // the ignore* filtering system has removed all code for them:
            result = null;
        } else if (node.isAllocatable()) {
            // Case of a F90 ALLOCATABLE variable, which for us is a pointer,
            // but it is illegal to initialize it to NULL().
            result = null;
        } else {
            //else this node creates an atomic initialization statement
            // on the expression contained in this leaf node :
            Tree expr = node.expression;
            WrapperTypeSpec type = node.type;
            Tree sizeModifier = ILUtils.copy(node.typeSizeModifier);
            if (sizeModifier != null && sizeModifier.opCode() == ILLang.op_nameEq) {
                sizeModifier = sizeModifier.down(2);
            }
            Tree zeroTree = null;
            if (TypeSpec.isA(type, SymbolTableConstants.POINTERTYPE)) {
                //[TODO] for C, we'd better generate a simple ident "NULL":
                if (TapEnv.relatedLanguageIsC()) {
                    zeroTree = ILUtils.build(ILLang.op_ident, "NULL");
                } else {
                    zeroTree = ILUtils.buildCall(
                            ILUtils.build(ILLang.op_ident, "null"),
                            ILUtils.build(ILLang.op_expressions));
                }
            } else {
                String typeName;
                if (type == null) {
                    typeName = "NULL_TYPE_NAME";
                    TapEnv.fileWarning(TapEnv.MSG_WARN, expr, "(AD14) Differentiated procedure of " + TapEnv.relatedUnit().name() + " needs to initialize to 0 an undefined variable " + expr);
                    TapEnv.toolWarning(-1, "Warning: Wrong initialization value for " + ILUtils.toString(expr) + " of type null");
                } else {
                    typeName = type.baseTypeName();
                }
                if (typeName.equals("character")) {
                    zeroTree = ILUtils.build(ILLang.op_stringCst, "");
                } else if (typeName.equals("integer")) {
                    zeroTree = ILUtils.build(ILLang.op_intCst, 0);
                } else if (typeName.equals("float") || typeName.equals("complex")) {
                    if (sizeModifier == null || TapEnv.relatedLanguageIsC()) {
                        zeroTree = PrimitiveTypeSpec.buildRealConstantZero();
                    } else if (sizeModifier.opCode() == ILLang.op_intCst) {
                        if (sizeModifier.intValue() == -2) {
                            zeroTree = ILUtils.build(ILLang.op_realCst, "0.D0");
                        } else {
                            int sizeModifierValue = sizeModifier.intValue();
                            assert type != null;
                            if (TapEnv.relatedLanguageIsFortran77()
                                    || type.hasUndefinedSize()) {
                                if (sizeModifierValue == 8) {
                                    zeroTree = ILUtils.build(ILLang.op_realCst, "0.D0");
                                } else {
                                    zeroTree = PrimitiveTypeSpec.buildRealConstantZero();
                                }
                            } else {
                                if (typeName.equals("complex")) {
                                    sizeModifierValue = sizeModifierValue / 2;
                                }
                                zeroTree = ILUtils.build(ILLang.op_realCst, "0.0_" + sizeModifierValue);
                            }
                        }
                    } else if (sizeModifier.opCode() == ILLang.op_stringCst) {
                        zeroTree = ILUtils.build(ILLang.op_realCst, "0.0_" + ILUtils.getIdentString(sizeModifier));
                    } else {
                        zeroTree = PrimitiveTypeSpec.buildRealConstantZero();
                    }
                    if (typeName.equals("complex")) {
                        zeroTree = ILUtils.build(ILLang.op_complexConstructor,
                                zeroTree,
                                ILUtils.copy(zeroTree));
                    }
                } else if (typeName.equals("Undefined")) {
                    if (!CallGraph.isMessagePassingChannelName(ILUtils.toString(expr))) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(AD14) Differentiated procedure of " + TapEnv.relatedUnit().name() + " needs to initialize to 0 an undefined-type variable " + ILUtils.toString(expr));
new Throwable().printStackTrace() ;
                    }
                } else if (typeName.equals("NULL_TYPE_NAME")) {
                    zeroTree = ILUtils.build(ILLang.op_realCst, "0.0_NULLTYPE");
                } else {
                    zeroTree = PrimitiveTypeSpec.buildRealConstantZero();
                }
            }
            if (zeroTree == null) {
                TapEnv.toolWarning(-1, "Warning: Cannot find an initialization value for " + ILUtils.toString(expr) + " of type " + type);
                zeroTree = ILUtils.build(ILLang.op_intCst, 0);
            }
            if (TapEnv.associationByAddress()) {
                result = ILUtils.build(ILLang.op_assign,
                           ILUtils.build(ILLang.op_fieldAccess,
                             ILUtils.copy(expr),
                             ILUtils.build(ILLang.op_ident, TapEnv.assocAddressDiffSuffix())),
                           zeroTree);
            } else {
                result = ILUtils.build(ILLang.op_assign, ILUtils.copy(expr), zeroTree);
            }
        }

        if (result != null && mayProtectAccesses && node.expression != null && node.protectAllocatedAssociated != null) {
            result = ILUtils.build(ILLang.op_if, ILUtils.copy(node.protectAllocatedAssociated), result);
        }
        return result;
    }

    public Tree makeAssign(RefDescriptor rhsd) {
        Tree resultTree = makeAssign(nestedLoopTree, rhsd.nestedLoopTree, false, false);
        checkUnknownDimensionsForC(resultTree, "assign");
        return resultTree;
    }

    public Tree makeIncrement(RefDescriptor incrd, boolean needAtomic) {
        Tree resultTree = makeAssign(nestedLoopTree, incrd.nestedLoopTree, true, needAtomic);
        checkUnknownDimensionsForC(resultTree, "increment");
        return resultTree;
    }

    /**
     * increment is "true" to build an increment operation instead of an assignment.
     */
    private Tree makeAssign(RefDescriptor.NestedLoopNode node, RefDescriptor.NestedLoopNode rhsNode,
                            boolean increment, boolean needAtomic) {
// System.out.println("CALLING MAKEASSIGN OF "+(node==null ? "null" : "")) ;
// if (node!=null) {node.dumpNestedLoopTree(2) ;}
// System.out.println("RECEIVING "+(rhsNode==null ? "null" : "")) ;
// if (rhsNode!=null) {rhsNode.dumpNestedLoopTree(2) ;} ;
        Tree result;
        if (node.nodes != null) {
            // then this node is not a leaf.
            // For instance, it may create a do-loop or a list of statements.
            TapList<IterDescriptor> loopsOutside = node.loopIterators;
            TapList<NestedLoopNode> nodes = node.nodes;
            TapList<NestedLoopNode> rhsNodes = rhsNode.nodes;
            if (loopsOutside!=null && rhsNodes==null) {
                //case of [FOR iteration on i]x(i) receiving scalar y:
                rhsNodes = new TapList<>(rhsNode, null) ;
            }
            TapList<Tree> codeInside = null;
            while (nodes != null && rhsNodes != null) {
                Tree subTree = makeAssign(nodes.head, rhsNodes.head, increment, needAtomic) ;
                if (subTree!=null) codeInside = new TapList<>(subTree, codeInside);
                nodes = nodes.tail;
                rhsNodes = rhsNodes.tail;
            }
            if (nodes != null || rhsNodes != null) {
                TapEnv.toolWarning(-1, "(RefDescriptor makeAssign) Different shapes in lhs ans rhs");
            }
            codeInside = TapList.nreverse(codeInside);
            if (codeInside != null && codeInside.tail == null) {
                result = codeInside.head;
            } else {
                result = ILUtils.build(ILLang.op_blockStatement, codeInside);
            }
            while (loopsOutside != null) {
                Tree loopHeader =
                        loopsOutside.head
                                .buildDoHeaderTree(usageSymbolTable, targetUnit, true);
                result = ILUtils.build(ILLang.op_loop, null, null,
                        loopHeader, result);
                loopsOutside = loopsOutside.tail;
            }
        } else if (node.branchIsEmpty()) {
            // this is the case of branches or leaves in the nestedLoopTree that are empty because
            // the ignore* filtering system has removed all code for them:
            result = null;
        } else {
            // else this node creates an (atomic) assignment statement
            // on the expression contained in this leaf node.
            Tree newLhs = ILUtils.copy(node.expression);
            Tree newRhs = ILUtils.copy(rhsNode.expression);
            if (increment) {
                result = ILUtils.incrementByProtectedExpr(TapEnv.relatedLanguage(), newLhs, newRhs, needAtomic);
            } else {
                result = ILUtils.setToProtectedExpr(TapEnv.relatedLanguage(), newLhs, newRhs,
                                     (node.modifiedTypeAbove==null ? node.type : node.modifiedTypeAbove)) ;
            }
        }

        if (result != null && mayProtectAccesses && node.expression != null && node.protectAllocatedAssociated != null) {
            result = ILUtils.build(ILLang.op_if, ILUtils.copy(node.protectAllocatedAssociated), result);
        }
// System.out.println("MAKEASSIGN RETURNS: "+result) ;
        return result;
    }

    private Tree makeNormDiff(Tree cumulTree, RefDescriptor.NestedLoopNode newNode, RefDescriptor.NestedLoopNode oldNode, RefDescriptor newRefDescriptor, RefDescriptor oldRefDescriptor) {
        Tree result;
        if (oldNode.nodes != null) {
            // then this node is not a leaf.
            // For instance, it may create a do-loop or a list of statements.
            TapList<IterDescriptor> loopsOutside = oldNode.loopIterators;
            TapList<NestedLoopNode> oldNodes = oldNode.nodes;
            TapList<NestedLoopNode> newNodes = newNode.nodes;
            TapList<Tree> codeInside = null;
            while (oldNodes != null && newNodes != null) {
                Tree subTree = makeNormDiff(cumulTree, newNodes.head, oldNodes.head, newRefDescriptor, oldRefDescriptor) ;
                if (subTree!=null) codeInside = new TapList<>(subTree, codeInside);
                oldNodes = oldNodes.tail;
                newNodes = newNodes.tail;
            }
            if (oldNodes != null || newNodes != null) {
                TapEnv.toolWarning(-1, "(RefDescriptor makeNormDiff) different shapes in old and new");
            }
            codeInside = TapList.nreverse(codeInside);
            if (codeInside != null && codeInside.tail == null) {
                result = codeInside.head;
            } else {
                result = ILUtils.build(ILLang.op_blockStatement, codeInside);
            }
            while (loopsOutside != null) {
                Tree loopHeader =
                        loopsOutside.head
                                .buildDoHeaderTree(usageSymbolTable, targetUnit, true);
                result = ILUtils.build(ILLang.op_loop, null, null,
                        loopHeader, result);
                loopsOutside = loopsOutside.tail;
            }
        } else if (newNode.branchIsEmpty() || oldNode.branchIsEmpty()) {
            // this is the case of branches or leaves in the nestedLoopTree that are empty because
            // the ignore* filtering system has removed all code for them:
            result = null;
        } else {
            // else this node creates an atomic assignment statement
            // on the expression contained in this leaf node.
            // Take care of the special case when one leaf is "aDouble"
            Tree oldExpr = ILUtils.copy(oldNode.expression);
            Tree newExpr = ILUtils.copy(newNode.expression);

            if (newNode.type != null && newNode.type.isAugmentedDoubleBase()) {
                newExpr = ILUtils.build(ILLang.op_call, ILUtils.build(ILLang.op_none),
                        ILUtils.build(ILLang.op_ident, "getDiff"),
                        ILUtils.build(ILLang.op_expressions));
            }

            if (oldNode.type != null && oldNode.type.isAugmentedDoubleBase()) {
                oldExpr = ILUtils.build(ILLang.op_call, ILUtils.build(ILLang.op_none),
                        ILUtils.build(ILLang.op_ident, "getDiff"),
                        ILUtils.build(ILLang.op_expressions));
            }


            TapList<ArrayDim> dimensions = oldRefDescriptor.completeType.getAllDimensions();
            boolean usesArrayNotation = false;
            while (!usesArrayNotation && dimensions != null) {
                if (dimensions.head instanceof ArrayDimPlusIter) {
                    usesArrayNotation = ((ArrayDimPlusIter) dimensions.head).iter.isArray;
                }
                dimensions = dimensions.tail;
            }
            result = ILUtils.build(ILLang.op_power,
                    ILUtils.build(ILLang.op_sub, newExpr, oldExpr),
                    ILUtils.build(ILLang.op_intCst, 2));
            if (usesArrayNotation) {
                result = ILUtils.reducSumProtectedExpr(result, null);
            }
            result =
                    ILUtils.build(ILLang.op_assign,
                            ILUtils.copy(cumulTree),
                            ILUtils.build(ILLang.op_add,
                                    ILUtils.copy(cumulTree), result));
        }
        return result;
    }

    /**
     * @return an expression tree that refers to the contents of
     * the current RefDescriptor.
     * Unlike makeStack or makeAssign, this is sometimes not posible,
     * e.g what is a single expression referring to A%x and A%z ???
     */
    public Tree makeRef() {
        Tree resultTree = makeRef(nestedLoopTree);
        checkUnknownDimensionsForC(resultTree, "build a reference to");
        return resultTree;
    }

    private Tree makeRef(RefDescriptor.NestedLoopNode node) {
        Tree result;
        if (node == null) {
            // TODO verifier ce code!!!! cf F77 lha18
            return ILUtils.copy(patternTree);
        }
        if (node.nodes != null) {
            /* then this node creates an iterative variable ref
             * around the list of expressions created by node.nodes */
            TapList<NestedLoopNode> nodes = node.nodes;
            TapList<Tree> codeInside = null;
            while (nodes != null) {
                Tree subTree = makeRef(nodes.head) ;
                if (subTree!=null) codeInside = new TapList<>(subTree, codeInside);
                nodes = nodes.tail;
            }
            codeInside = TapList.nreverse(codeInside);
            result = ILUtils.build(ILLang.op_expressions, codeInside);
            TapList<IterDescriptor> loopsOutside = node.loopIterators;
            while (loopsOutside != null) {
                Tree loopHeader =
                        loopsOutside.head
                                .buildDoHeaderTree(usageSymbolTable, targetUnit, true);
                result = ILUtils.build(ILLang.op_iterativeVariableRef,
                        result, loopHeader);
                loopsOutside = loopsOutside.tail;
            }
        } else if (node.branchIsEmpty()) {
            // this is the case of branches or leaves in the nestedLoopTree that are empty because
            // the ignore* filtering system has removed all code for them:
            result = null;
        } else
            /* else this node creates an atomic reference expression */ {
            result = ILUtils.copy(node.expression);
        }
        return result;
    }

    /**
     * @return the root tree that should be allocated to "validate" tree.
     */
    private Tree allocationCheckingNeeded(Tree tree) {
        Tree result = null;
        WrapperTypeSpec typeSpec;
        int operator = tree.opCode();
        switch (operator) {
            case ILLang.op_arrayAccess: {
                result = allocationCheckingNeeded(tree.down(1));
                break;
            }
            case ILLang.op_pointerAccess: {
                result = tree.down(1);
                break;
            }
            case ILLang.op_fieldAccess: {
                typeSpec = defSymbolTable.typeOf(tree.down(1));
                if (TypeSpec.isA(typeSpec.wrappedType, SymbolTableConstants.POINTERTYPE)) {
                    result = tree.down(1);
                } else {
                    result = allocationCheckingNeeded(tree.down(1));
                }
                break;
            }
            case ILLang.op_ident: {
                VariableDecl varDecl = defSymbolTable.getVariableDecl(tree.stringValue());
                if (varDecl != null) {
                    if (TypeSpec.isA(varDecl.type().wrappedType, SymbolTableConstants.POINTERTYPE)) {
                        result = tree;
                    }
                }
                break;
            }
            default:
                result = null;
                break;
        }
        return result;
    }

    @Override
    public String toString() {
        return "REFDESCRIPTOR: " + patternTree + " (" + completeType + ") " + nestedLoopTree;
    }

    /**
     * This class holds a textual representation of where we are inside the
     * analyzed tree. This info is used to build the error variable names and
     * the hint messages when some dimensions are unknown and must be requested.
     */
    private static final class HintPlace {
        private Tree refTree;
        private String arrayNameInText;
        private String arrayNameInIdent;
        private Tree arrayTreeForCallSize; //[llh 21/6/12]: supprimer a terme.
        /**
         * The rank (starting at 1) of the current dimension in its enclosing list of dimensions.
         */
        private int dim;
        /**
         * The number of dims in the enclosing list of dimensions.
         */
        private int ndims;

        private HintPlace(Tree refTree, String arrayNameInText,
                          String arrayNameInIdent, Tree arrayTreeSize, int dim, int ndims) {
            super();
            this.refTree = refTree;
            this.arrayNameInText = arrayNameInText;
            this.arrayNameInIdent = arrayNameInIdent;
            this.arrayTreeForCallSize = arrayTreeSize;
            this.dim = dim;
            this.ndims = ndims;
        }

        private HintPlace deriveDim(int ndim, int ndims) {
            return new RefDescriptor.HintPlace(refTree, arrayNameInText/*+"()"*/, arrayNameInIdent, arrayTreeForCallSize, ndim, ndims);
        }

        private HintPlace deriveField(String fieldName) {
            return new RefDescriptor.HintPlace(refTree, arrayNameInText + "%" + fieldName,
                    arrayNameInIdent + "_" + fieldName,
                    arrayTreeForCallSize,
                    0, 1);
        }

        private void makeField(String fieldName) {
            arrayNameInText = arrayNameInText + "%" + fieldName;
            arrayNameInIdent = arrayNameInIdent + "_" + fieldName;
        }

        private void makePointerDest() {
            arrayNameInText = "*" + arrayNameInText;
            arrayNameInIdent = "Drf" + arrayNameInIdent;
        }

        private void cumulWith(RefDescriptor.HintPlace hintPlace2) {
            if ((arrayNameInText == null || arrayNameInText.isEmpty())
                    && !(hintPlace2.arrayNameInText == null || hintPlace2.arrayNameInText.isEmpty())) {
                arrayNameInText = hintPlace2.arrayNameInText;
                arrayNameInIdent = hintPlace2.arrayNameInIdent;
                dim = hintPlace2.dim;
            }
        }

        @Override
        public String toString() {
            return "HintPlace(" + refTree + " " + arrayNameInText + " " + arrayNameInIdent
                    + " sz:" + arrayTreeForCallSize + " " + dim + "/" + ndims + ')';
        }
    }

    /**
     * Nodes and leaves of the tree of the do-loops-nest
     * around individual expressions.
     */
    private static final class NestedLoopNode {
        /**
         * When on a leaf, the expression Tree, otherwise null.
         * This field is used as the criterion to decide leaf or node.
         */
        private Tree expression;
        /**
         * When non-null, the test expression that must wrap this level's code.
         */
        private Tree protectAllocatedAssociated;
        /**
         * When on a leaf, the expression WrapperTypeSpec, otherwise null.
         */
        private WrapperTypeSpec type;
        /**
         * When on a leaf, the size modifier of the expression WrapperTypeSpec, otherwise -1.
         */
        private Tree typeSizeModifier;
        /**
         * When on a leaf, and only if the leaf PrimitiveTypeSpec is inside a ModifiedTypeSpec,
         * then contains this ModifiedTypeSpec.
         */
        private ModifiedTypeSpec modifiedTypeAbove = null ;
        /**
         * When on a leaf, the array size of the expression Tree, which
         * can be null when the tree is a scalar.
         */
        private Tree arraySize;
        /**
         * True when the expression Tree is a scalar.
         */
        private boolean isScalar;
        /**
         * True when the expression is a F90 allocatable.
         */
        private boolean isAllocatable = false ;
        /**
         * True when there may be a derivative part for this (sub-)expression tree.
         */
        private boolean canHaveDiff;
        /**
         * When not on a leaf, IterDescriptor's of the do loops around inside NestedLoopNode's.
         */
        private TapList<IterDescriptor> loopIterators;
        /**
         * When not on a leaf, the inside NestedLoopNode's.
         */
        private TapList<RefDescriptor.NestedLoopNode> nodes;

        /**
         * Plain creation.
         */
        private NestedLoopNode(Tree expression, WrapperTypeSpec type, TapList<String> extraInfo, Tree typeSizeModifier,
                               Tree arraySize, boolean isScalar,
                               TapList<IterDescriptor> loopIterators, TapList<NestedLoopNode> nodes, boolean canHaveDiff) {
            super();
            this.expression = expression;
            this.type = type;
            this.typeSizeModifier = typeSizeModifier;
            this.arraySize = arraySize;
            this.isScalar = isScalar;
            this.isAllocatable = TapList.containsEquals(extraInfo, "allocatable") ;
            this.canHaveDiff = canHaveDiff;
            this.loopIterators = loopIterators;
            this.nodes = nodes;
        }

        private boolean branchIsEmpty() {
            if (nodes != null) {
                boolean isEmpty = true;
                TapList<RefDescriptor.NestedLoopNode> inNodes = nodes;
                while (isEmpty && inNodes != null) {
                    isEmpty = inNodes.head.branchIsEmpty();
                    inNodes = inNodes.tail;
                }
                return isEmpty;
            } else {
                return expression == null;
            }
        }

        private boolean isAllocatable() {
            return isAllocatable ;
        }

        public void dumpNestedLoopTree(int indent) {
            for (int i=indent ; i>0 ; --i) System.out.print("  ") ;
            if (nodes==null) {
                System.out.println("LEAF:"+expression+" OF TYPE:"+type+"_"+typeSizeModifier+"["+arraySize+"]") ;
            } else {
                if (loopIterators==null) {
                    System.out.print("SEQUENCE") ;
                } else {
                    System.out.print("LOOP") ;
                    TapList<IterDescriptor> inLoops = loopIterators;
                    while (inLoops != null) {
                        System.out.print(" OVER "+inLoops.head) ;
                        inLoops = inLoops.tail;
                    }
                }
                System.out.println() ;
                TapList<RefDescriptor.NestedLoopNode> inNodes = nodes;
                while (inNodes != null) {
                    inNodes.head.dumpNestedLoopTree(indent+1) ;
                    inNodes = inNodes.tail;
                }
            }
        }

        @Override
        public String toString() {
            if (nodes != null) {
                StringBuilder result = new StringBuilder("REFERENCE TO ");
                result.append(expression);
                result.append(" OF TYPE:"+type);
                if (loopIterators==null) {
                    result.append(" SEQUENCE OF") ;
                } else {
                    TapList<IterDescriptor> inLoops = loopIterators;
                    result.append(" LOOP") ;
                    while (inLoops != null) {
                        result.append(" OVER ").append(inLoops.head);
                        inLoops = inLoops.tail;
                    }
                }
                result.append(" {");
                TapList<RefDescriptor.NestedLoopNode> inNodes = nodes;
                while (inNodes != null) {
                    result.append("\n");
                    result.append(inNodes.head);
                    inNodes = inNodes.tail;
                }
                return result+"}";
            } else {
                return "LEAF:"+expression+" type:"+type+"_"+typeSizeModifier+"["+arraySize+"]";
            }
        }
    }

    /**
     * Private local class that holds one usage of an IterDescriptor
     * (iterDescriptor) in one particular place (e.g. an index of an array).
     * In addition to the IterDescriptor (that holds its length, array-or-not, index...),
     * this class holds info which is local to this usage, i.e. the actual index,
     * how it is related to the generic index of the IterDescriptor,
     * and whether the index can be replaced by the implicit ":"
     * in case the IterDescriptor is of the "array" sort.
     */
    private final class IterDescriptorUse {
        /**
         * true iff this iteration ranges completely over a dimension
         * and can therefore be replaced by an implicit ":"
         */
        private boolean isImplicitCompleteDimension = false ;
        /**
         * The explicit op_dimColon or op_arrayTriplet for this use,
         * With no holes in it, i.e. it can't be just ":" nor "1:X", but must
         * be e.g. "-10:100:1" or "1:X:1" or "3:100:2".
         */
        private Tree explicitTree;
        /**
         * The underlying IterDescriptor.
         */
        private IterDescriptor iterDescriptor;

        /** The array dim that this IterDescriptor sweeps */
        public ArrayDim contextDim ;

        /**
         * Creation. When tree in not complete, explicit the missing parts
         * using the dimension "contextDim" of the array that uses this iterator.
         * E.g. if T is of dim 2:100, T(10:) means T(10:100)
         * Sets the boolean "isImplicitCompleteDimension" if the index
         * can be freely replaced by ":".
         * Initializes the size of  the underlying IterDescriptor.
         */
        private IterDescriptorUse(IterDescriptor iterDescriptor, Tree tree, ArrayDim contextDim) {
            super();
            this.iterDescriptor = iterDescriptor;
            int treeOper = tree.opCode();
            if (treeOper == ILLang.op_dimColon || treeOper == ILLang.op_arrayTriplet) {
                explicitTree = ILUtils.copy(tree);
                Tree contextLower = contextDim == null ? null : ILUtils.copy(contextDim.lowerTree());
                Tree contextUpper = contextDim == null ? null : ILUtils.copy(contextDim.upperTree());
                if (ILUtils.isNullOrNone(explicitTree.down(1)) && contextLower != null) {
                    explicitTree.setChild(contextLower, 1);
                }
                if (ILUtils.isNullOrNone(explicitTree.down(2)) && contextUpper != null) {
                    explicitTree.setChild(contextUpper, 2);
                }
                Tree explicitStride;
                if (tree.getAnnotation("subArrayAccess") != null) {
                    iterDescriptor.allSubArrayAccesses = true;
                }
                if (treeOper == ILLang.op_arrayTriplet) {
                    if (ILUtils.isNullOrNone(explicitTree.down(3))) {
                        explicitStride = ILUtils.build(ILLang.op_intCst, 1);
                        explicitTree.setChild(explicitStride, 3);
                    } else {
                        explicitStride = explicitTree.down(3);
                    }
                } else {
                    explicitStride = ILUtils.build(ILLang.op_intCst, 1);
                }
                if (iterDescriptor==multiDirIterator) {
                    // because we don't want to sweep on 1:nbdirsmax instead of 1:nbdirs (or maybe we do?)
                    isImplicitCompleteDimension = false;
                } else if (ILUtils.evalsToOne(explicitStride)
                           // unDeclarableLength implies that array access can never be considered on full dimension.
                           && (contextDim==null || !contextDim.unDeclarableLength)
                           && (contextLower == null || defSymbolTable.equalValues(explicitTree.down(1), contextLower))
                           && (contextUpper == null || defSymbolTable.equalValues(explicitTree.down(2), contextUpper)
                               // Attempt to fix set11/mvo09: when we access a zone, and zone is not "split", then don't explicit dim size:
                               || (zoneInfo!=null && sweepsZoneExactly(zoneInfo.accessTree, defSymbolTable, zoneInfo))
// Following is a questionable attempt to fix set11/mvo09. It "works" but it looks too dirty so we revoked it:
//                          || comesFromHiddenScope(explicitTree.down(2), defSymbolTable)
                        )) {
                    isImplicitCompleteDimension = true;
                } else {
                    isImplicitCompleteDimension = false;
                    iterDescriptor.isImplicitCompleteDimension = false;
                }
                if (!ILUtils.isNullOrNone(explicitTree.down(1))) {
                    iterDescriptor.setInitTree(ILUtils.copy(explicitTree.down(1)));
                }
                iterDescriptor.preciseLengthTree(
                            ILUtils.buildSizeTree(iterDescriptor.getInitTree(),
                                                  explicitTree.down(2),
                                                  explicitStride));
            } else if (treeOper == ILLang.op_do) {
                explicitTree = ILUtils.copy(tree);
                Tree explicitStride = explicitTree.down(4);
                if (ILUtils.isNullOrNone(explicitStride)) {
                    explicitStride = ILUtils.build(ILLang.op_intCst, 1);
                    explicitTree.setChild(explicitStride, 4);
                }
                isImplicitCompleteDimension = false;
                iterDescriptor.isImplicitCompleteDimension = false;
                iterDescriptor.setInitTree(ILUtils.copy(explicitTree.down(2)));
                iterDescriptor.preciseLengthTree(ILUtils.buildSizeTree(explicitTree.down(2),
                        explicitTree.down(3),
                        explicitStride));
            }
        }

        /**
         * @return true when
         */
        private boolean sweepsZoneExactly(Tree expr, SymbolTable symbolTable, ZoneInfo zoneInfo) {
            TapIntList zonesOfExpr = symbolTable.listOfZonesOfValue(expr, null, null) ;
            return (zonesOfExpr!=null && zonesOfExpr.head==zoneInfo.zoneNb && zonesOfExpr.tail==null) ;
        }

        /** Returns true iff we suspect that the given sizeExpr, which appears in the size expression
         * of some array dimension, probably comes from the place where the array was declared
         * and some variables used in this size expressions are no longer valid here in symbolTable.
         * In that case we will force *not* using this size expression here, replacing it with an
         * access to the full extent of the dimension (e.g ":" in Fortran). This is obviously an
         * approximate patch. It more or less fixes a bug shown by set11/mvo09... */
        private boolean comesFromHiddenScope(Tree sizeExpr, SymbolTable symbolTable) {
            boolean hidden = false ;
            if (sizeExpr!=null) {
                switch (sizeExpr.opCode()) {
                    case ILLang.op_ident:
                        hidden = (symbolTable.getDecl(sizeExpr.stringValue(), SymbolTableConstants.SYMBOL, false) == null) ;
                        break ;
                    case ILLang.op_fieldAccess:
                        hidden = comesFromHiddenScope(sizeExpr.down(1), symbolTable) ;
                        break ;
                    default: {
                        Tree[] exps = sizeExpr.children();
                        for (int i=exps.length-1 ; i>=0 && !hidden ; --i) {
                            hidden = comesFromHiddenScope(exps[i], symbolTable) ;
                        }
                    }
                }
            }
            return hidden ;
        }

        /**
         * @return a new Tree which is an index for this dimension.
         * If the dimension is not chosen to use array notation,
         * it will be a scalar expression with a do-loop index in it.
         * If it uses array notation, then it will be an arrayTriplet,
         * and in particular an implicit ":" if possible.
         */
        private Tree buildIndexTree(SymbolTable usageSymbolTable, Unit targetUnit) {
            if (iterDescriptor.isArray) {
                if (iterDescriptor.allSubArrayAccesses) {
                    return ILUtils.copy(explicitTree.down(1));
                } else if (isImplicitCompleteDimension || explicitTree.opCode() == ILLang.op_dimColon) {
                    return ILUtils.build(ILLang.op_arrayTriplet);
                } else { // explicitTree is certainly an arrayTriplet:
                    Tree arrayTriplet = ILUtils.copy(explicitTree);
                    if (ILUtils.evalsToOne(arrayTriplet.down(3))) {
                        arrayTriplet.setChild(ILUtils.build(ILLang.op_none), 3);
                    }
                    return arrayTriplet;
                }
            } else {
                Tree index = iterDescriptor.buildIndexTree(usageSymbolTable, targetUnit);
                Tree from = explicitTree.down(1);
                Tree stride = null;
                if (explicitTree.opCode() == ILLang.op_arrayTriplet) {
                    stride = explicitTree.down(3);
                }
                Tree initIndex = iterDescriptor.getInitTree();
                if (!ILUtils.evalsToZero(initIndex)) {
                    index = ILUtils.subTree(index, initIndex);
                }
                return ILUtils.addTree(ILUtils.copy(from),
                         (stride == null
                          ? index
                          : ILUtils.mulTree(ILUtils.copy(stride), index)));
            }
        }

        public String toString() {
            return "IDUse("+iterDescriptor+" implicitCompleteDim:"+isImplicitCompleteDimension+" explicitTree:"+explicitTree+")" ;
        }

    }
}
