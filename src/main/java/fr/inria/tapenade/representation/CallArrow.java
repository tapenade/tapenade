/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;

/**
 * One arrow in the CallGraph.
 */
public final class CallArrow {

    /**
     * Origin Unit of this CallArrow.
     */
    public Unit origin;
    /**
     * destination Unit of this CallArrow.
     */
    public Unit destination;

    /** Elementary formal arguments that are at a leaf of their complete formal arg structure.
     * Related to the question of mismatchnig types of formal and actual arg. */
    public BoolVector coversFurther = null ;

    /**
     * True if this arrow is a CALL.
     */
    private boolean isCall;

    /**
     * @return True if this arrow is a CALL.
     */
    public boolean isCall() {
        return isCall;
    }

    /**
     * True if this arrow is an IMPORT.
     */
    private boolean isImport;

    /**
     * @return True if this arrow is an IMPORT.
     */
    public boolean isImport() {
        return isImport;
    }

    public void setImport(boolean anImport) {
        isImport = anImport;
    }

    /**
     * True if this arrow is a CONTAINS.
     */
    private boolean isContain;

    /**
     * @return True if this arrow is a CONTAINS.
     */
    public boolean isContain() {
        return isContain;
    }

    public void setContain(boolean contain) {
        isContain = contain;
    }

    /**
     * Number of times the caller calls the callee (textual occurrences of call).
     */
    private int times;

    /**
     * @return Number of times the caller calls the callee (textual occurrences of call).
     */
    protected int times() {
        return times;
    }

    protected void setTimes(int times) {
        this.times = times;
    }

    /**
     * Original name used in the statement that calls this CallArrow,
     * if different from destination name.
     * Used for display in the HTML call graph.
     */
    private String srcCallName;

    /**
     * Creates a new CallArrow from origin Unit to destination Unit.
     */
    protected CallArrow(Unit origin, Unit destination) {
        super();
        this.origin = origin;
        this.times = 0;
        this.destination = destination;
        if (origin != null) {
            origin.callees = new TapList<>(this, origin.callees);
        }
        if (destination != null) {
            destination.callers = new TapList<>(this, destination.callers);
        }
    }

    /**
     * @return the origin Unit of this CallArrow.
     */
    public Unit origin() {
        return origin;
    }

    /**
     * @return the destination Unit of this CallArrow.
     */
    public Unit destination() {
        return destination;
    }

    public String srcCallName() {
        return srcCallName;
    }

    /**
     * Sets the original name used in the statement that calls this CallArrow.
     */
    protected void setSrcCallName(String calledName) {
        if (calledName != null && destination != null && !calledName.equals(destination.name())) {
            if (origin().isFortran()) {
                calledName = calledName.toUpperCase();
            }
            srcCallName = calledName;
        }
    }

    protected void addKind(int newKind) {
        switch (newKind) {
            case SymbolTableConstants.CALLS:
                isCall = true;
                break;
            case SymbolTableConstants.IMPORTS:
                isImport = true;
                break;
            case SymbolTableConstants.CONTAINS:
                isContain = true;
                break;
            default:
                break;
        }
    }

    protected void delKind(int k) {
        switch (k) {
            case SymbolTableConstants.CALLS:
                isCall = false;
                break;
            case SymbolTableConstants.IMPORTS:
                isImport = false;
                break;
            case SymbolTableConstants.CONTAINS:
                isContain = false;
                break;
            default:
                break;
        }
    }

    protected boolean followsDirections(int callsDir, int importsDir, int containsDir,
                                        TapList<Unit> forwardDeclarationUnits) {
        return (isCall && (callsDir == 1)
                // If this arrow calls a Unit that is forward-declared, forget it:
                && !TapList.contains(forwardDeclarationUnits, destination)
                //[llh] TRICK: if importsDir is opposite of callsDir, then there
                // are cycles in the ordering. To avoid these cycles, only
                // consider "call" arrows that are inside the same upperLevelUnit:
                && ((importsDir != -1) ||
                (origin.upperLevelUnit() == destination.upperLevelUnit()))) ||
                (isImport && (importsDir == 1)) ||
                (isContain && (containsDir == 1));
    }

    /**
     * @param parameterRank from 1 to nbArgs.
     * @return true if this CallArrow passes arguments by value.
     */
    public boolean takesArgumentByValue(int parameterRank) {
        return destination.takesArgumentByValue(parameterRank, origin.language());
    }

    /**
     * Deletes this CallArrow.
     */
    protected void delete() {
        redirectOrigin(null);
        redirectDestination(null);
    }

    /**
     * Redirects the origin of this CallArrow to "unit". If unit==null, this
     * disconnects this CallArrow from its origin.
     */
    protected void redirectOrigin(Unit unit) {
        if (origin != null) {
            origin.callees = TapList.delete(this, origin.callees);
        }
        origin = unit;
        if (origin != null) {
            origin.callees = TapList.addUnlessPresentEquals(origin.callees, this);
        }
    }

    /**
     * Redirects the destination of this CallArrow to "unit".
     * If unit==null, this disconnects this CallArrow from its destination.
     */
    protected void redirectDestination(Unit unit) {
        if (destination != null) {
            destination.callers = TapList.delete(this, destination.callers);
        }
        destination = unit;
        if (destination != null) {
            destination.callers = new TapList<>(this, destination.callers);
        }
    }

    /**
     * Pre-detects whether formal and actual types don't match, so that there may be blurring.
     */
    protected void computeCoversFurther() {
        if (destination.hasParamElemsInfo()
            // Doing the following in all cases would make sense, but cycling structures seem to cause bugs (set04/lh114)
            && (destination.isIntrinsic() || destination.isExternal())) {
            int nbParams = 0;
            int destShapeLength = destination.paramElemsNb();
            ZoneInfo zi;
            TapList toLocation;
            for (int i = 0; i < destShapeLength; ++i) {
                zi = destination.paramElemZoneInfo(i);
                if (zi.isParameter() && zi.index > nbParams) {
                    nbParams = zi.index;
                }
            }
            // See set10/ampi10, for utility of the "coversFurther" mechanism.
            coversFurther = new BoolVector(destShapeLength) ;
            // Detect the elementary formal arguments that are at a leaf of their complete formal arg structure.
            // This is about blurring when formal and actual types don't match.
            TapList[] paramCoverage = new TapList[nbParams];
            for (int i = nbParams - 1; i >= 0; --i) {
                paramCoverage[i] = new TapList<>(null, null);
            }
            // first pass: accumulate the structure covered by the formal arguments.
            for (int i = 0; i < destShapeLength; ++i) {
                zi = destination.paramElemZoneInfo(i);
                if (zi.isParameter()) {
                    TapList.getSetFieldLocation(paramCoverage[zi.index - 1], zi.accessTree, true);
                }
            }
            // second pass: detect the formal params that are at the leaf of the covered structure.
            for (int i = 0; i < destShapeLength; ++i) {
                zi = destination.paramElemZoneInfo(i);
                if (zi.isParameter()) {
                    toLocation = TapList.getSetFieldLocation(paramCoverage[zi.index - 1], zi.accessTree, false);
                    if (toLocation.tail == null && toLocation.head == null) {
                        coversFurther.set(i, true) ;
                    }
                }
            }
        }
    }

    @Override
    public String toString() {
        return "CallArrow from " + (origin == null ? null : origin.name())
                + " to " + (destination == null ? null : destination.name());
    }
}
