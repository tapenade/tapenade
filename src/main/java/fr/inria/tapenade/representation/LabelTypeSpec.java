/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * A type for special procedure arguments that are indeed labels
 * e.g. addresses after return from the procedure.
 */
final class LabelTypeSpec extends TypeSpec {

    /**
     * Create a new label type.
     */
    public LabelTypeSpec() {
        super(SymbolTableConstants.LABELTYPE);
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

//     // [llh] Written from scratch on 2017sept5, eventually to clean the mess of canReceive/sameTypes/etc...
//     public boolean isLargerThan(TypeSpec other) {
//         // Peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
//         // (layers of ModifiedTypeSpec's *around a LabelType* make no sense, so we may peel them)
//         other = peelWrapperAndModified(other) ;
//         // Easy strict == case:
//         if (this==other) return true ;
//         // other must be specified and must be of matching kind: 
//         return (other!=null && other instanceof LabelTypeSpec) ;
//     }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a LabelType* make no sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind:
        return other instanceof LabelTypeSpec;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        return ILUtils.build(ILLang.op_star);
    }

    @Override
    public String showType() {
        return "Label";
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "label"//+"@"+Integer.toHexString(hashCode())
                ;
    }
}
