/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * A type for C++ name space.
 */
public class NameSpaceDecl extends SymbolDecl {

    protected SymbolTable symbolTable;

    public NameSpaceDecl(String name, SymbolTable nameSpaceSymbolTable) {
        super(name, SymbolTableConstants.NAMESPACE);
        this.symbolTable = nameSpaceSymbolTable;
    }

    @Override
    public String toString() {
        return symbol    //+ " @" + Integer.toHexString(hashCode())
            + ": nameSpace. SymbolTable: " + symbolTable
            ;
    }
}
