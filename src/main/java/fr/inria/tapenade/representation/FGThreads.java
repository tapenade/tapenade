/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.ILLang;

/**
 * Class used only by class FlowGraphBuilder.
 * Holds a collection of control flow arrows that reach a same point.
 */
final class FGThreads {

    /**
     * Top of the chain of ThreadListCells.
     * One ThreadListCell contains one thread.
     */
    private ThreadListCell topCell;

    /**
     * Assumes this FGThread is single and simple.
     *
     * @return origin Block of this single thread.
     */
    protected Block block() {
        return topCell.block;
    }

    /**
     * Adds one thread to this FGThreads.
     * Adds the thread coming from "block", when the "test" goes to case "cas".
     */
    protected void add(Block block, int test, int cas) {
        topCell = new ThreadListCell(block, test, cas, topCell);
    }

    /**
     * Concatenates "newThreads" in front of this FGThreads:
     * Warning: reuses ThreadListCell's of "newThreads", which will be broken.
     */
    protected void concat(FGThreads newThreads) {
        if (topCell != null) {
            ThreadListCell lastCell = topCell;
            while (lastCell.next != null) {
                lastCell = lastCell.next;
            }
            lastCell.next = newThreads.topCell;
        } else {
            topCell = newThreads.topCell;
        }
    }

    /**
     * Makes all threads flow to the "destinationBlock":
     * Does not modify the contents of the current threads.
     */
    protected void convergeToBlock(Block destinationBlock, boolean inCycle, boolean traceOn) {
        if (traceOn) {
            TapEnv.printlnOnTrace(" Converge(ToBlock) thread(s): " + (topCell == null ? "empty!" : detail(topCell)));
            TapEnv.printlnOnTrace("  to " + showBlockTail(destinationBlock));
        }
        FGArrow newArrow;
        //When one of these arrows is a switch CASE (except the final NOMATCH case),
        // then the other arrows that are NOT switch CASEs
        // must be labelled "isAJumpIntoNextCase".
        ThreadListCell currentCell = topCell;
        boolean isSwitchCase = false;
        while (!isSwitchCase && currentCell != null) {
            isSwitchCase = currentCell.test == FGConstants.CASE && currentCell.cas != FGConstants.NOMATCH;
            currentCell = currentCell.next;
        }
        // Now build the FGArrows:
        currentCell = topCell;
        while (currentCell != null) {
            newArrow = new FGArrow(currentCell.block, currentCell.test,
                    currentCell.cas, destinationBlock, inCycle);
            if (isSwitchCase && currentCell.test != FGConstants.CASE) {
                newArrow.isAJumpIntoNextCase = true;
            }
            currentCell = currentCell.next;
        }
    }

    /**
     * Makes all threads flow to a new instruction built for "tree":
     * Builds a new Block only when necessary (2 incoming threads...)
     * Modifies the contents of the current threads: at the
     * end, current FGThreads is just at the end of the Block that received "tree".
     */
    protected void convergeToInstr(Instruction instruction, TapList<Block> allBlocks, Unit unit,
                                   SymbolTable symbolTable, TapList<Instruction> parallelControls, boolean traceOn) {
        if (traceOn) {
            TapEnv.printlnOnTrace(" Converge(ToInstr) thread(s): " + (topCell == null ? "empty!" : detail(topCell)));
            TapEnv.printlnOnTrace("  to new instruction tree:" + instruction.tree + " of SymbolTable "
                    + symbolTable.addressChain() + " with parallelControls:" + parallelControls);
        }
        assert topCell != null;
        if ((unit.isProcedureWithCode() || unit.isOutsideTBD() || unit.isInterface() || unit.isTranslationUnit())
                // ^ For the time being, we allow no control flow in Classes and Modules => never create new Block
                &&
                (topCell.next != null
                        // ^ If there is more than one arriving thread, or...
                        || topCell.test != FGConstants.NO_TEST
                        // ^ if this thread is associated to a nontrivial case, or...
                        || topCell.block.flow() != null
                        // ^ if the preceding block has many exit flow arrows, or...
                        || symbolTable != topCell.block.symbolTable
                        //^ if the two blocks are not in the same symbol context, or...
                        || parallelControls != topCell.block.parallelControls
                        //^ if the two blocks are not in the same parallel control context, or...
                        || ILUtils.isParallelController(instruction.tree)
                        //^ if the new instruction is the opening pragma of a parallel region or loop
                        || Directive.containDirectives(instruction.preComments)
                        //^ if the new instruction has an AD directive in its preComments, or...
                        || Directive.containDirectives(instruction.preCommentsBlock)
                        //^ if the new instruction has an AD directive in its preCommentsBlock, or...
                )) {
            // ...THEN new Instruction must not be fused into previous block:
            BasicBlock block = new BasicBlock(symbolTable, parallelControls, allBlocks);
            // If the current SymbolTable has still no declarationsBlock,
            // then give it this new block as its declarationsBlock:
            if (symbolTable.declarationsBlock == null) {
                symbolTable.declarationsBlock = block;
            }
            block.addInstrTl(instruction);
            convergeToBlock(block, false, traceOn);
            topCell = new ThreadListCell(block, FGConstants.NO_TEST, FGConstants.NO_TEST, null);
        } else if ((symbolTable.unit == null || symbolTable.unit.isFortran())
                && instruction.isADeclaration()
                && instruction.tree.opCode() != ILLang.op_include
                && instruction.isUnitDefinitionStub() == null) {
            // Fuse new declaration Instruction into previous block, BUT trying to place it:
            //  -- BEFORE the computations statements if this is a declaration (except an include),
            //  -- AND BEFORE any other declaration or computation instruction that uses it.
            int movedUp = topCell.block.addInstrDeclTlBeforeUse(instruction,
                    topCell.block.symbolDeclDeclared(instruction),
                    null, topCell.block.symbolTable, false);
            if (movedUp != 0) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, instruction.tree, "(DD31) Declaration statement " + ILUtils.toString(instruction.tree, symbolTable.language()) + " moved " + movedUp + " declarations up");
            }
        } else {
            // Fuse new Instruction at the tail of previous block:
            topCell.block.addInstrTl(instruction);
        }
        if (traceOn) {
            TapEnv.printlnOnTrace(" Yielding new thread(s): " + detail(topCell));
        }
    }

    /**
     * Assumes this FGThread is single and simple.
     * Re-types it with the given newTest and newCas.
     */
    protected void reTypeThread(int newTest, int newCas) {
        topCell.test = newTest;
        topCell.cas = newCas;
    }

    /**
     * Removes all threads in current FGThreads.
     */
    protected void empty() {
        topCell = null;
    }

    protected boolean isNotEmpty() {
        return topCell != null;
    }

    // Should be merged with toString...
    private String showBlockTail(Block block) {
        TapList<Instruction> instrs = block.instructions;
        if (instrs == null) {
            return "Block@" + Integer.toHexString(block.hashCode()) + "[]";
        } else if (instrs.tail == null) {
            return "Block@" + Integer.toHexString(block.hashCode()) + "[" + instrs.head.tree + "]";
        } else if (instrs.tail.tail == null) {
            return "Block@" + Integer.toHexString(block.hashCode()) + "[" + instrs.head.tree + " ; " + instrs.tail.head.tree + "]";
        } else {
            TapList<Instruction> revInstrs = TapList.reverse(instrs);
            return "Block@" + Integer.toHexString(block.hashCode()) + "[... " + revInstrs.tail.head.tree + " ; " + revInstrs.head.tree + "]";
        }
    }

    private String detail(ThreadListCell inThread) {
        String result = "";
        while (inThread != null) {
            result = result + showBlockTail(inThread.block) + "==" + FGArrow.testAndCasesToString(inThread.test, new TapIntList(inThread.cas, null)) + "=>";
            inThread = inThread.next;
            if (inThread != null) {
                result = result + System.lineSeparator() + "                     ";
            }
        }
        return result;
    }

    @Override
    public String toString() {
        ThreadListCell inThread = topCell;
        int length = 0;
        while (inThread != null) {
            inThread = inThread.next;
            length++;
        }
        return "FGThreads[holds:" + length + "]";
    }

    /**
     * Local private class of FGThreads.
     * Holds one cell of the list of threads.
     */
    private static final class ThreadListCell {
        private final Block block;
        private int test;
        private int cas;
        private ThreadListCell next;

        private ThreadListCell(Block block, int test, int cas, ThreadListCell next) {
            super();
            this.block = block;
            this.test = test;
            this.cas = cas;
            this.next = next;
        }

        @Override
        public String toString() {
            return block + "//" + test + "/" + cas + "||" + next;
        }
    }
}
