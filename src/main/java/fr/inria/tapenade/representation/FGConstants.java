/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Constants used by the FlowGraph.
 */
public final class FGConstants {
    public static final int ENTRY = -1;
    public static final int INIT_CONSTRUCTOR = 22;
    public static final int NO_TEST = 0;
    public static final int COMP_GOTO = 10;
    public static final int ASS_GOTO = 11;
    public static final int IO_GOTO = 12;
    public static final int IO_ERR = 13;
    public static final int IO_END = 14;
    public static final int IO_EOR = 15;
    public static final int CALL = 15;
    public static final int MAIN = 1;
    public static final int IF = 20;
    public static final int CASE = 21;
    public static final int LOOP = 30;
    public static final int NEXT = 1;
    public static final int EXIT = 0;
    public static final int DEFAULT = -1;
    public static final int NOMATCH = -2;
    /**
     * The following values are chosen such that LT + EQ = LE, etc...<br>
     * They are the value of a 3-bit object such that<br>
     * bit 1 (*4) represents "less than"<br>
     * bit 2 (*2) represents "equals" (or "true")<br>
     * bit 3 (*1) represens "greater than"<br>
     * Therefore 7 (=  "111") represents all cases.
     */
    public static final int EQ = 2;
    public static final int TRUE = 2;
    public static final int NE = 5;
    public static final int FALSE = 5;
    public static final int LT = 4;
    public static final int LE = 6;
    public static final int GT = 1;
    public static final int GE = 3;
    public static final int ALL_CASES = 7;
    public static final int ELSEWHERE = 9;
    /**
     * The following for the "iter" field in FGArrow's.
     */
    protected static final Integer POP = -1;
    protected static final Integer PUSH = 1;
    protected static final Integer CYCLE = 0;

    private FGConstants() {
    }
}
