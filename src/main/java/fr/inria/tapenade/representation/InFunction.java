/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;

/**
 * Inline a function declared in Tapenade libraries.
 */
final class InFunction {
    protected final Unit inlinedUnit;
    protected final Unit copiedUnit;
    private final Tree callingTree;
    protected Instruction callingInstruction;
    protected int rank = -1;
    private Tree resultTree;
    private FunctionTypeSpec functionTypeSpec;

    /**
     * Prepares a sort of copy of the body of an inlininable function,
     * used to actually replace the inlinable call in the calling Unit.
     *
     * @param inF             the definition unit of the inlinable function
     * @param callTree        the inlinable call in the calling Unit (may be an assignment)
     * @param callInstruction the Instruction that contains the inlinable call
     * @param lhs             when callTree is an assignment, its LHS, otherwise null.
     * @param actualArgsTypes used to precise the functionTypeSpec using the actual call types
     */
    protected InFunction(Unit inF, Tree callTree, Instruction callInstruction, Tree lhs,
                         WrapperTypeSpec[] actualArgsTypes) {
        super();
        inlinedUnit = inF;
        insertMetaVar();
        callingTree = callTree;
        if (lhs != null) {
            resultTree = ILUtils.copy(lhs);
        }
        copiedUnit = new Unit(null, -1, null, inlinedUnit.language());
        Unit.copyUnitBody(inlinedUnit, copiedUnit, callInstruction);
        if (inlinedUnit.allBlocks!=null) {
            copiedUnit.setPrivateSymbolTable(inlinedUnit.allBlocks.head.symbolTable) ;
        }
        copiedUnit.orig2copiedBlocks = null;
        copiedUnit.origPublicSTDeclarationBlock = null;
        copiedUnit.copyPublicSTDeclarationBlock = null;
        copiedUnit.messages = inlinedUnit.messages;
        if (inlinedUnit.hasArrayNotation()) {
            copiedUnit.setHasArrayNotation();
        }
        Block copyBlock;
        for (TapList<Block> copiedBlocks = copiedUnit.allBlocks; copiedBlocks != null; copiedBlocks = copiedBlocks.tail) {
            copyBlock = copiedBlocks.head;
            // To avoid clash with ranks in calling Unit.
            copyBlock.rank += 1000 ;
            copyBlock.parallelControls = callInstruction.block.parallelControls;
        }
        functionTypeSpec = callTree.getAnnotation("functionTypeSpec");
        // Various not-so-clean tricks to obtain the most accurate types.
        if (functionTypeSpec == null) {
            functionTypeSpec =
                    (FunctionTypeSpec)
                            inlinedUnit.functionTypeSpec().localize(new TapList<>(null, null), new ToBool(false));
            for (int i=functionTypeSpec.argumentsTypes.length-1 ; i>=0 ; --i) {
                if (actualArgsTypes[i]!=null)
                    functionTypeSpec.argumentsTypes[i] = actualArgsTypes[i] ;
            }
            Tree funcCallTree = (callTree.opCode()==ILLang.op_call ? callTree : callTree.down(2)) ;
            WrapperTypeSpec actualReturnType = callInstruction.block.symbolTable.typeOf(funcCallTree) ;
            if (actualReturnType!=null)
                functionTypeSpec.returnType = actualReturnType ;

            callTree.setAnnotation("functionTypeSpec", functionTypeSpec);
        }
    }

    /**
     * remplace toutes les occurences de la variable identOld par
     * le sous-arbre identNew
     * dans allBlocks
     * a faire aussi dans les tables des symboles.
     */
    private static void replaceVariable(Tree treeOld, Tree treeNew, TapList<Block> blocks) {
        Block currentBlock;
        while (blocks != null) {
            currentBlock = blocks.head;
            TapList<Instruction> instructions = currentBlock.instructions;
            Instruction instruction;
            while (instructions != null) {
                instruction = instructions.head;
                if (treeNew.isAtom()) {
                    if (instruction.whereMask() != null) {
                        InFunction.replaceAllIdent(
                                instruction.whereMask().getControlTree(), treeOld, treeNew);
                    }
                    InFunction.replaceAllIdent(
                            instruction.tree, treeOld, treeNew);
                } else {
                    if (instruction.whereMask() != null) {
                        InFunction.replaceAll(instruction.whereMask().getControlTree(), treeOld, treeNew);
                    }
                    InFunction.replaceAll(instruction.tree, treeOld, treeNew);
                }
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }
    }

    /**
     * oldTree est un arbre atomique
     * newTree est un arbre quelconque.
     */
    private static void replaceAll(Tree tree, Tree oldTree, Tree newTree) {
        Tree currentTree;
        TapList<Tree> trees = getAll(tree, oldTree);
if (TapEnv.traceInlineAnalysis()) {
  TapEnv.printlnOnTrace("   Replace all "+trees+" with "+newTree+" in "+tree) ;
}
        while (trees != null) {
            currentTree = trees.head;
if (TapEnv.traceInlineAnalysis()) {
  TapEnv.printlnOnTrace("     old:"+currentTree+" parent:"+currentTree.parent()) ;
}
            currentTree.parent().setChild(ILUtils.copy(newTree), currentTree.rankInParent());
            trees = trees.tail;
        }
    }

    /**
     * oldTree est un arbre quelconque.
     */
    private static TapList<Tree> getAll(Tree tree, Tree oldTree) {
        TapList<Tree> result = null;
        Tree currentTree;
        for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd(); i.advance()) {
            currentTree = i.get();
            if (currentTree.equalsTree(oldTree)) {
                result = new TapList<>(currentTree, result);
            }
        }
        return result;
    }

    /**
     * oldTree et newTree sont des arbres atomiques.
     */
    public static void replaceAllIdent(Tree tree, Tree oldTree, Tree newTree) {
        if (newTree.isStringAtom() && newTree.opCode() == oldTree.opCode()) {
            String newTreeValue = newTree.stringValue();
            Tree currentTree;
            for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd(); i.advance()) {
                currentTree = i.get();
                if (currentTree.equalsTree(oldTree)) {
                    currentTree.setValue(newTreeValue);
                }
            }
        } else {
            replaceAll(tree, oldTree, newTree);
        }
    }

    /**
     * Retourne une string non declaree dans futureUsageSymbolTable.
     */
    private static String createNewVarName(String name, SymbolTable futureUsageSymbolTable) {
        int i = 1;
        while (futureUsageSymbolTable.getVariableDecl(name + i)
                != null) {
            i = i + 1;
        }
        return name + i;
    }

    protected TapList<Block> allBlocks() {
        return copiedUnit.allBlocks;
    }

    private void insertMetaVar() {
        Tree callTree = inlinedUnit.entryBlock().headTree();
        assert callTree != null;
        Tree unitName = ILUtils.getCalledName(callTree);
        Tree[] args = ILUtils.getArguments(callTree).children();
        replaceVariable(
                unitName,
                ILUtils.build(ILLang.op_metavar, unitName.stringValue()),
                new TapList<>(inlinedUnit.entryBlock(), inlinedUnit.allBlocks));
        for (Tree arg : args) {
            replaceVariable(
                    arg,
                    ILUtils.build(ILLang.op_metavar, arg.stringValue()),
                    new TapList<>(inlinedUnit.entryBlock(), inlinedUnit.allBlocks));
        }
    }

    protected Tree replaceAllVariables(Block block, SymbolTable futureUsageSymbolTable,
                                       SymbolTable futureDeclSymbolTable) {
        Tree[] newTrees = ILUtils.getArguments(callingTree).children();
        assert inlinedUnit.entryBlock().headTree() != null;
        Tree[] oldTrees = ILUtils.getArguments(inlinedUnit.entryBlock().headTree()).children();
        Tree functionName = ILUtils.getCalledName(inlinedUnit.entryBlock().headTree());
        for (int i = 0; i < oldTrees.length; i++) {
            if (!ILUtils.instrHasSideEffect(newTrees[i]) && cost(newTrees[i]) < 100) {
                replaceVariable(oldTrees[i], newTrees[i], allBlocks());
            } else {
                insertLocalVariable(oldTrees[i], newTrees[i],
                        futureUsageSymbolTable, futureDeclSymbolTable, i);
            }
        }
        Tree tempVar = null;
        Tree functionNameIdent =
                ILUtils.build(
                        ILLang.op_metavar, functionName.stringValue());
        if (resultTree != null) {
            replaceAssignLHS(functionNameIdent, resultTree, null,
                             futureUsageSymbolTable);
        } else {
            ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
            tempVar = block.buildTemporaryVariable(functionName.stringValue(), callingTree,
                               futureUsageSymbolTable, toNSH, false, null, true, functionTypeSpec, 0, functionName) ;
            // un peu trop restrictif sur la structure des fonctions a inliner
            replaceAssignLHS(functionNameIdent, tempVar, toNSH.obj(), futureUsageSymbolTable);
        }
        return tempVar;
    }

    /**
     * Replace lhs "treeOld" with "treeNew" in all assignment instructions.
     * When the modified assignment gives an opportunity to guess the dimension
     * of the "treeNew", update the declaration of treeNew which is
     * kept in its NewSymbolHolder "tempVarHolder".
     */
    private void replaceAssignLHS(Tree treeOld, Tree treeNew,
                                  NewSymbolHolder tempVarHolder,
                                  SymbolTable futureUsageSymbolTable) {
        WrapperTypeSpec tempVarType = null ;
        if (tempVarHolder!=null && tempVarHolder.newVariableDecl()!=null) {
            tempVarType = tempVarHolder.newVariableDecl().type() ;
        }
        Block currentBlock;
        TapList<Block> blocks = allBlocks();
        while (blocks != null) {
            currentBlock = blocks.head;
            TapList<Instruction> instructions = currentBlock.instructions;
            Instruction instruction;
            Tree tree1;
            while (instructions != null) {
                instruction = instructions.head;
                if (instruction.tree.opCode() == ILLang.op_assign) {
                    tree1 = instruction.tree.down(1);
                    if (tree1.equalsTree(treeOld)) {
                        instruction.tree.setChild(ILUtils.copy(treeNew), 1);
                    } else if (tree1.opCode() == ILLang.op_pointerAccess
                            && tree1.down(1).equalsTree(treeOld)) {
                        tree1.setChild(ILUtils.copy(treeNew), 1);
                    }
                    WrapperTypeSpec curTypeSpec = futureUsageSymbolTable.typeOf(instruction.tree.down(2));
                    if (tempVarType!=null && curTypeSpec!=null && tempVarType!=curTypeSpec) {
                        tempVarType.receives(curTypeSpec, null, null);
                        if (TypeSpec.isA(tempVarType, SymbolTableConstants.ARRAYTYPE)) {
                            TapList<ArrayDim> dims = tempVarType.getAllDimensions();
                            if (dims == null) {
                                if (TypeSpec.isA(curTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                                    ((ArrayTypeSpec) tempVarType.wrappedType).setDimensions(((ArrayTypeSpec) curTypeSpec.wrappedType).dimensions());
                                    // Because we have improved tempVarType, we must update its declaration instruction:
                                    if (tempVarHolder.instruction()!=null) {
                                        Tree typeTree = tempVarType.wrappedType.generateTree(null, null, null, true, null);
                                        tempVarHolder.instruction().tree.setChild(typeTree ,2) ;
                                    }
                                }
                            }
                        }
                    }
                }
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }
    }

    /**
     * Utilise quand un parametre d'une fonction a inliner est une expression non atomique
     * TODO verifier que la nouvelle variable n'existe pas dans TOUTES les symbolTables.
     */
    private void insertLocalVariable(Tree formalArg, Tree actualArg, SymbolTable futureUsageSymbolTable,
                                     SymbolTable futureDeclSymbolTable, int rank) {
        // On suppose allBlocks triee:
        Block firstBlock = allBlocks().head;
        TapList<Instruction> instructions = firstBlock.instructions;
        Tree leftTree = ILUtils.copy(formalArg);
        assert leftTree != null;
        String newVarName =
                createNewVarName(leftTree.stringValue(), futureUsageSymbolTable);
        leftTree.setValue(newVarName);
        Tree tree = ILUtils.build(ILLang.op_assign, leftTree, ILUtils.copy(actualArg));
        Instruction newInstr;
        WrapperTypeSpec newVarType;
        if (functionTypeSpec.argumentsTypes != null) {
            newVarType = functionTypeSpec.argumentsTypes[rank];
        } else {
            newVarType = new WrapperTypeSpec(null);
        }
        VariableDecl varDecl = new VariableDecl(leftTree, newVarType);
        futureDeclSymbolTable.addSymbolDecl(varDecl);
        Tree varDeclaration = varDecl.buildVarDeclaration(futureDeclSymbolTable);
        replaceVariable(formalArg, leftTree, allBlocks());
        TapList<Instruction> lastinstruction = null;
        while (instructions != null) {
            lastinstruction = instructions;
            instructions = instructions.tail;
        }
        assert lastinstruction != null;
        newInstr = new Instruction(varDeclaration, lastinstruction.head.syntaxController,
                lastinstruction.head.block);
        futureDeclSymbolTable.declarationsBlock.addInstrHdAfterDecls(newInstr);
        newInstr =
                new Instruction(
                        tree, lastinstruction.head.syntaxController,
                        lastinstruction.head.block);
        newInstr.setWhereMask(lastinstruction.head.getWhereMask());
        // inserts newInstr as new first elem of lastinstruction :
        TapList<Instruction> newTail = new TapList<>(lastinstruction.head, lastinstruction.tail);
        lastinstruction.head = newInstr;
        lastinstruction.tail = newTail;
        replaceVariable(
                leftTree,
                ILUtils.build(ILLang.op_ident, leftTree.stringValue()),
                allBlocks());
    }

    private int cost(Tree tree) {
        int result;
        switch (tree.opCode()) {
            case ILLang.op_intCst:
            case ILLang.op_realCst:
                result = 2;
                break;
            case ILLang.op_arrayAccess:
                result = 2 + cost(tree.down(2).children());
                break;
            case ILLang.op_add:
                result = 5 + cost(tree.down(1)) + cost(tree.down(2));
                break;
            case ILLang.op_mul:
                result = 20 + cost(tree.down(1)) + cost(tree.down(2));
                break;
            case ILLang.op_div:
                result = 30 + cost(tree.down(1)) + cost(tree.down(2));
                break;
            case ILLang.op_call:
                result = 100 + cost(ILUtils.getArguments(tree).children());
                break;
            default:
                result = 10;
                break;
        }
        return result;
    }

    private int cost(Tree[] tree) {
        int result = 0;
        for (Tree value : tree) {
            result = result + cost(value);
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("InFunction ");
        result.append(inlinedUnit);
        result.append(" rank ");
        result.append(rank);
        result.append(System.lineSeparator());
        if (callingInstruction != null) {
            result.append(callingInstruction).append(System.lineSeparator());
        }
        if (callingTree != null) {
            result.append(callingTree).append(System.lineSeparator());
        }
        TapList<Block> allBlocks = copiedUnit.allBlocks;
        while (allBlocks != null) {
            result.append("    Block ").append(allBlocks.head).append(" instructions:").
                    append(allBlocks.head.instructions).append(System.lineSeparator());
            allBlocks = allBlocks.tail;
        }
        return result.toString();
    }
}
