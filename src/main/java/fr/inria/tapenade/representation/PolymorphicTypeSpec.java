/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2024 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * The polymorphic type of all possible types derived from a root type
 */
public class PolymorphicTypeSpec extends TypeSpec {

    /** The root of this polymorphic type */
    public TypeSpec rootType;

    public PolymorphicTypeSpec(TypeSpec rootType) {
        super(SymbolTableConstants.POLYMORPHICTYPE);
        this.rootType = rootType ;
    }

    @Override
    public TypeSpec wrappedType() {
        return rootType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        rootType = (type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type));
    }

    @Override
    public TypeSpec nestedLevelType() {
        return rootType;
    }

    @Override
    protected String baseTypeName() {
        return rootType == null ? null : rootType.baseTypeName();
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        if (rootType == null) {
            return null;
        } else {
            return rootType.baseTypeSpec(stopOnPointer);
        }
    }

    @Override
    public WrapperTypeSpec modifiedBaseTypeSpec() {
        if (rootType == null) {
            return null;
        } else {
            return rootType.modifiedBaseTypeSpec();
        }
    }

    @Override
    public boolean containsAPointer() {
        return rootType!=null && rootType.containsAPointer();
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return new PolymorphicTypeSpec(rootType.copyStopOnComposite(publishedUnit)) ;
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a Polymorphic type* make little sense, so we will peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // now other must be of matching kind:
        if (!(other instanceof PolymorphicTypeSpec)) {
            return false;
        }
        PolymorphicTypeSpec otherPolymorphic = (PolymorphicTypeSpec) other ;
        // dejaVu case: stop recursion and return true:
        if (TapList.containsObjectPair(dejaVu, rootType, otherPolymorphic.rootType)) {
            return true;
        }
        boolean comparesWell = true;
        // compare root types:
        if (rootType == null) {
            // if this's rootType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                setWrappedType(otherPolymorphic.rootType == null ? null : otherPolymorphic.rootType.weakenForInference(comparison));
            } else {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (otherPolymorphic.rootType == null) {
            // if other's rootType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                otherPolymorphic.setWrappedType(rootType);
            } else {
                comparesWell = false;
            }
        } else if (testIsReceives(comparison) &&
                   (isA(this.rootType, SymbolTableConstants.VOIDTYPE) ||
                    isA(otherPolymorphic.rootType, SymbolTableConstants.VOIDTYPE))) {
            //comparesWell = true ; //comparesWell is already true!
        } else {
            comparesWell =
                    rootType.comparesWith(otherPolymorphic.rootType, comparison,
                            this, otherPolymorphic,
                            new TapList<>(new TapPair<>(rootType, otherPolymorphic.rootType), dejaVu));
        }
        return comparesWell;
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":")
            +"Polymorphic" //+"@"+Integer.toHexString(hashCode())
            +" from " + (rootType == null ? "?" : rootType.toString());
    }
}
