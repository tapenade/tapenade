/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.DepsAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.InOutAnalyzer;
import fr.inria.tapenade.analysis.MultithreadAnalyzer;
import fr.inria.tapenade.analysis.PointerAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.ir2tree.ControlStruct;
import fr.inria.tapenade.utils.BoolMatrix;
import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

import java.io.File;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Environment containing parameters useful during Tapenade analysis.
 */
public final class TapEnv {

    public static final String UNDEFINEDSIZE = "undefinedSize";
    /**
     * The "undefined" file language.
     */
    public static final int UNDEFINED = -1;
    /**
     * The "language" of files written in FORTRAN (77 or 90 or up).
     */
    public static final int FORTRAN = 1;
    /**
     * The "language" of files written in FORTRAN90 or up.
     */
    public static final int FORTRAN90 = 2;
    /**
     * The "language" of files written in FORTRAN2003 or up.
     */
    public static final int FORTRAN2003 = 3;
    /**
     * The "language" of files written in C.
     */
    public static final int C = 4;
    /**
     * The "language" of files written in C++.
     */
    public static final int CPLUSPLUS = 5;
    /**
     * The "language" of files written in C Cuda
     */
    public static final int CUDA = 6;
    /**
     * The "language" of files written in julia
     */
    public static final int JULIA = 7;
    /**
     * A "language" indicating that the file is an include file.
     */
    public static final int INCLUDE = 98;
    /**
     * A "language" indicating that a file is a JAR.
     */
    public static final int JAR = 99;
    /**
     * A "language" for mixtures of different languages.
     */
    public static final int MIXED = 100;

    public static final int FORTRAN_FAMILY = 0;
    public static final int C_FAMILY = 1;
    public static final int CPLUSPLUS_FAMILY = 2;

    /**
     * The default, standard cpp preprocessor.
     */
    public static final String STD_CPP_COMMAND = "/usr/bin/cpp";
    /**
     * The default, standard cpp preprocessor for MacOS.
     */
    public static final String MAC_CPP_COMMAND = "/usr/bin/cpp";

    public static final int CPLUSPLUS_PARSER_NB_ARGS = 6;
    public static final int NO_POSITION = -1;
    public static final int NEXT_TREE = -2;

    /**
     * Message level.
     */
    public static final int MSG_ALWAYS = -1;
    public static final int MSG_ERROR = 0;
    public static final int MSG_SEVERE = 5;
    public static final int MSG_DEFAULT = 10;
    public static final int MSG_WARN = 15;
    public static final int MSG_TRACE = 30;

    /**
     * Default size of a byte.
     */
    protected static final int BYTE_SIZE = 1;
    /**
     * Default size of a char.
     */
    protected static final int CHAR_SIZE = 1;
    /**
     * Default size of a pointer.
     */
    protected static final int POINTER_SIZE = 8;
    /**
     * Default size of a float.
     */
    protected static final int REAL_SIZE = 4;
    protected static final int TANGENT_MODE = 1;
    /**
     * Because during analyses, it is sometimes useful
     * to know which differentiation mode will be performed later.
     */
    private static final int PREPROCESS_MODE = 0;
    private static final int ADJOINT_MODE = -1;
    private static final int OVERLOAD_MODE = 2;

    /** The max number of differemtiation "sorts",
     * Currently 5 is enough, for e.g. functions F_FWD, F_BWD, F_B, F_D,
     * or for variables vd, vb, (alternative)vb0... */
    public static final int MAX_DIFF_SORTS = 5 ;

    /**
     * No debug of differentiated program.
     */
    public static final int NO_DEBUG = 0;
    /**
     * Tangent debug mode of differentiated program.
     */
    public static final int TGT_DEBUG = 1;
    /**
     * Adjoint debug mode of differentiated program.
     */
    public static final int ADJ_DEBUG = -1;

    /**
     * tapenade -html style sheet.
     */
    public static final String tapenadeCss = "<link type=\"text/CSS\" rel=\"stylesheet\" href=\"tapenade.css\">";

    /**
     * tapenade -html with https://getbootstrap.com/ style sheet.
     */
    public static final String bootstrapCss = "<link rel=\"stylesheet\" href=\"https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css\" integrity=\"sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2\" crossorigin=\"anonymous\">";
    /**
     * A static character used to show active subtrees in ILUtils.toString().
     */
    private static String activeMark = "";
    /**
     * A static character used to show pointer-active subtrees in ILUtils.toString().
     */
    private static String pointerActiveMark = "";

    /**
     * @return character used to show active subtrees in ILUtils.toString().
     */
    public static String activeMark() {
        return activeMark;
    }

    /**
     * Set character used to show active subtrees in ILUtils.toString().
     */
    public static void setActiveMark(String activeMark) {
        TapEnv.activeMark = activeMark;
    }

    /**
     * @return character used to show reqexplicit subtrees in ILUtils.toString().
     */
    public static String pointerActiveMark() {
        return pointerActiveMark;
    }

    /**
     * Set character used to show reqexplicit subtrees in ILUtils.toString().
     */
    public static void setPointerActiveMark(String pointerActiveMark) {
        TapEnv.pointerActiveMark = pointerActiveMark;
    }

    /**
     * The $TAPENADE_HOME directory.
     */
    private static String tapenadeHome = System.getProperty("tapenade_home");

    /**
     * @return The $TAPENADE_HOME directory.
     */
    public static String tapenadeHome() {
        return tapenadeHome;
    }

    /**
     * Set tapenadeHome.
     *
     * @param tapenadeDirectory $TAPENADE_HOME or System.getProperty("user.dir").
     */
    public static void setTapenadeHome(String tapenadeDirectory) {
        tapenadeHome = tapenadeDirectory;
    }

    /**
     * A marker to distinguish "servlet" Tapenade from standard "command-line" Tapenade.
     */
    private static boolean isServlet;

    /**
     * A marker to distinguish "servlet" Tapenade from standard "command-line" Tapenade.
     *
     * @return true if Tapenade is used as a web server, else false.
     */
    public static boolean isServlet() {
        return isServlet;
    }

    /**
     * Call during servlet initialization.
     */
    public static void setIsServlet() {
        isServlet = true;
    }

    /**
     * Empty constructor.
     */
    private TapEnv() {
    }

    /**
     * Builds a specific TapEnvForThread for each thread, so that TapEnv environment values
     * for one thread do not interfere with other threads.
     * The specific TapEnvForThread for the current thread is obtained by TapEnv.get().
     * (Compatibily java1.8: {@code new ThreadLocal<TapEnvForThread>())}
     */
    private static final ThreadLocal<TapEnvForThread> tapEnvForThread = new ThreadLocal<TapEnvForThread>() {
        @Override
        protected TapEnvForThread initialValue() {
            return new TapEnvForThread();
        }
    };

    public static TapEnvForThread get() {
        return tapEnvForThread.get();
    }

    /**
     * True if this was compiled to be distributed.
     * Some internal debug mechanisms and traces are then deactivated.
     */
    private static boolean isDistribVersion;

    /**
     * @return true when this is a distribution version.
     */
    public static final boolean isDistribVersion() {
        return isDistribVersion;
    }

    /**
     * Initialize isDistribVersion.
     *
     * @param value true for distribution.
     */
    public static void setDistribVersion(boolean value) {
        isDistribVersion = value;
    }

    /**
     * @param lang the language
     * @return the language extension string.
     */
    public static final String outputExtension(int lang) {
        switch (lang) {
            case FORTRAN:
                return ".f";
            case FORTRAN90:
                return ".f90";
            case FORTRAN2003:
                return ".f03";
            case C:
            case MIXED:
                return ".c";
            case CPLUSPLUS:
                return ".c++";
            case CUDA:
                return ".cu";
            case JULIA:
                return ".jl";
            default:
                return "";
        }
    }

    /**
     * Get suffix of file.
     *
     * @param filename full file name.
     * @return suffix of filename
     */
    public static final String getExtension(String filename) {
        return filename.substring(1+filename.lastIndexOf('.')).toLowerCase();
    }

    public static final void switchOnCombineDeclMessages(boolean onOff) {
        get().combineDeclMessagesSwitch = onOff;
    }

    /**
     * @param language integer value.
     * @return true when the given language is Fortran.
     */
    public static final boolean isFortran(int language) {
        return language == FORTRAN || language == FORTRAN90 || language == FORTRAN2003;
    }

    /**
     * @param language integer value.
     * @return true when the given language is Fortran 90 or higher.
     */
    public static final boolean isFortran9x(int language) {
        return language == FORTRAN90 || language == FORTRAN2003;
    }

    /**
     * @param language integer value.
     * @return true when the given language is Fortran 2003 or higher.
     */
    public static final boolean isFortran2003(int language) {
        return language == FORTRAN2003;
    }

    /**
     * @param language integer value.
     * @return true when the given language family is C (C, Cuda, C++...).
     */
    public static final boolean isC(int language) {
        return language == C || language == CUDA || language == CPLUSPLUS;
    }

    public static final boolean isCexactly(int language) {
        return language == C ;
    }

    /**
     * @param language integer value.
     * @return true when the given language C++.
     */
    public static final boolean isCPlusPlus(int language) {
        return language == CPLUSPLUS;
    }

    /**
     * @param language integer value.
     * @return true when the given language is Julia.
     */
    public static final boolean isJulia(int language) {
        return language == JULIA;
    }


    /**
     * @param lang1 integer.
     * @param lang2 integer.
     * @return true when lang1 and lang2 are Fortran, or
     * when lang1 and lang2 are C (or C++).
     */
    public static final boolean sameLanguageFamily(int lang1, int lang2) {
        return isFortran(lang1) && isFortran(lang2) || isC(lang1) && isC(lang2);
    }

    public static final CallArrow relatedArrow() {
        return get().relatedArrow;
    }

    public static final void setRelatedArrow(CallArrow arrow) {
        get().relatedArrow = arrow;
    }

    /**
     * @return this TapEnv's "current" Unit, used to attach all messages emitted, and also to
     * know the particular programming language, sometimes incurring limitations.
     */
    public static final Unit relatedUnit() {
        return get().relatedUnit;
    }

    /**
     * Set this TapEnv's "current" Unit, used to attach all messages emitted, and also to
     * know the particular programming language, sometimes incurring limitations.
     *
     * @param unit current Unit.
     */
    public static final void setRelatedUnit(Unit unit) {
        get().relatedUnit = unit;
        if (unit == null) {
            setRelatedLanguage(null);
        } else {
            setRelatedLanguage(unit.publicSymbolTable());
        }
        if (get().tracedTypeCheckUnits == null) {
            get().traceTypeCheckAnalysis =
                    get().traceTypeCheck != null && get().traceTypeCheck.head.equals("%all%");
        } else {
            get().traceTypeCheckAnalysis =
                    get().tracedTypeCheckUnits.head == null
                            || unit != null && TapList.contains(get().tracedTypeCheckUnits, unit);
        }
        if (get().tracedInlineUnits == null) {
            get().traceInlineAnalysis =
                    get().traceInline != null && get().traceInline.head.equals("%all%");
        } else {
            get().traceInlineAnalysis =
                    get().tracedInlineUnits.head == null
                            || unit != null && TapList.contains(get().tracedInlineUnits, unit);
        }
    }

    public static final void pushRelatedUnit(Unit unit) {
        get().relatedUnits = new TapList<>(get().relatedUnit, get().relatedUnits);
        get().relatedUnit = unit;
        if (unit == null) {
            setRelatedLanguage(null);
        } else {
            setRelatedLanguage(unit.publicSymbolTable());
        }
    }

    public static final void popRelatedUnit() {
        if (get().relatedUnits != null) {
            get().relatedUnit = get().relatedUnits.head;
            get().relatedUnits = get().relatedUnits.tail;
        } else {
            get().relatedUnit = null;
        }
        if (get().relatedUnit == null) {
            setRelatedLanguage(null);
        } else {
            setRelatedLanguage(get().relatedUnit.publicSymbolTable());
        }
    }

    /**
     * Reset this TapEnv's "current" Unit to null.
     */
    public static final void resetRelatedUnit() {
        get().relatedUnit = null;
        get().relatedUnits = new TapList<>(null, null);
        setRelatedLanguage(null);
    }

    public static final void setUseSharpInclude(boolean useIt) {
        get().useSharpInclude = useIt ;
    }

    public static final boolean useSharpInclude() {
        return get().useSharpInclude ;
    }

    /**
     * Set current differentiation mode to "PREPROCESS_NODE".
     */
    public static final void setModeNoDiff() {
        get().diffMode = PREPROCESS_MODE;
    }

    public static final void setModeTangent() {
        get().diffMode = TANGENT_MODE;
    }

    public static final void setModeAdjoint() {
        get().diffMode = ADJOINT_MODE;
    }

    public static final void setModeOverloading() {
        get().diffMode = OVERLOAD_MODE;
    }

    public static final boolean modeIsNoDiff() {
        return get().diffMode == PREPROCESS_MODE;
    }

    public static final boolean modeIsTangent() {
        return get().diffMode == TANGENT_MODE;
    }

    public static final boolean modeIsAdjoint() {
        return get().diffMode == ADJOINT_MODE;
    }

    public static final boolean modeIsOverloading() {
        return get().diffMode == OVERLOAD_MODE;
    }

    /**
     * reset to zero the next number to identify matching PUSH/POP's.
     */
    public static final void resetPushPopNumber() {
        get().pushPopNumber = 0;
    }

    /**
     * @return a new free number to identify matching PUSH/POP's.
     */
    public static final int getNewPushPopNumber() {
        if (get().numberPushPops) {
            get().pushPopNumber++;
        }
        return get().pushPopNumber - 1;
    }

    public static final boolean associationByAddress() {
        return get().associationByAddress;
    }

    public static final void setAssociationByAddress(boolean value) {
        get().associationByAddress = value;
    }

    public static final String assocAddressValueSuffix() {
        return get().assocAddressValueSuffix;
    }

    public static final String assocAddressDiffSuffix() {
        return get().assocAddressDiffSuffix;
    }

    public static final boolean valid() {
        return get().valid;
    }

    public static final void setValid(boolean value) {
        get().valid = value;
    }

    /**
     * True when we want to trace the current analysis on-the-fly.
     */    
    public static final boolean traceCurAnalysis() {
        return get().traceCurAnalysis;
    }

    public static final void setTraceCurAnalysis(boolean value) {
        get().traceCurAnalysis = value ;
    }

    public static final boolean traceCalledUnit(Unit calledUnit) {
        return (get().traceCalledUnitName != null
                && ("%any%".equals(get().traceCalledUnitName)
                    || calledUnit.name().equals(get().traceCalledUnitName)));
    }

    public static final boolean traceTypeCheckAnalysis() {
        return get().traceTypeCheckAnalysis;
    }

    public static final void setTraceTypeCheckAnalysis(boolean value) {
        get().traceTypeCheckAnalysis = value;
    }

    public static final boolean traceInlineAnalysis() {
        return get().traceInlineAnalysis;
    }

    public static final void setTraceInlineAnalysis(boolean value) {
        get().traceInlineAnalysis = value;
    }

    public static final boolean traceInputIL() {
        return get().traceInputIL;
    }

    public static final void setTraceInputIL(boolean value) {
        get().traceInputIL = value;
    }

    public static final TapList<String> tracePointer() {
        return get().tracePointer;
    }

    public static final void setTracePointer(TapList<String> unitNames) {
        get().tracePointer = unitNames;
    }

    public static final TapList<String> traceFlowGraphBuild() {
        return get().traceFlowGraphBuild;
    }

    public static final void setTraceFlowGraphBuild(TapList<String> unitNames) {
        get().traceFlowGraphBuild = unitNames;
    }

    public static final TapList<String> traceInOut() {
        return get().traceInOut;
    }

    public static final void setTraceInOut(TapList<String> unitNames) {
        get().traceInOut = unitNames;
    }

    public static final TapList<String> traceDeps() {
        return get().traceDeps;
    }

    public static final void setTraceDeps(TapList<String> unitNames) {
        get().traceDeps = unitNames;
    }

    public static final TapList<String> traceADDeps() {
        return get().traceADDeps;
    }

    public static final void setTraceADDeps(TapList<String> unitNames) {
        get().traceADDeps = unitNames;
    }

    public static final TapList<String> traceActivity() {
        return get().traceActivity;
    }

    public static final void setTraceActivity(TapList<String> unitNames) {
        get().traceActivity = unitNames;
    }

    public static final TapList<String> traceReqExplicit() {
        return get().traceReqExplicit;
    }

    public static final void setTraceReqExplicit(TapList<String> unitNames) {
        get().traceReqExplicit = unitNames;
    }

    public static final TapList<String> traceDiffLiveness() {
        return get().traceDiffLiveness;
    }

    public static final void setTraceDiffLiveness(TapList<String> unitNames) {
        get().traceDiffLiveness = unitNames;
    }

    public static final TapList<String> traceTBR() {
        return get().traceTBR;
    }

    public static final void setTraceTBR(TapList<String> unitNames) {
        get().traceTBR = unitNames;
    }

    public static final TapList<String> traceMultithread() {
        return get().traceMultithread;
    }

    public static final void setTraceMultithread(TapList<String> unitNames) {
        get().traceMultithread = unitNames;
    }

    public static final TapList<String> duplicableUnitNames() {
        return get().duplicableUnitNames;
    }

    public static final void setDuplicableUnitNames(TapList<String> unitNames) {
        get().duplicableUnitNames = unitNames;
    }

    /**
     * Debug mode of differentiated programs.
     *
     * @return debug mode.
     */
    public static final int debugAdMode() {
        return get().debugAdMode;
    }

    /**
     * Set debug mode of differentiated program.
     *
     * @param value debug mode.
     */
    public static final void setDebugAdMode(int value) {
        get().debugAdMode = value;
    }

    public static final boolean debugADMM() {
        return get().debugADMM;
    }

    public static final void setDebugADMM(boolean value) {
        get().debugADMM = value;
    }

    public static final int diffKind() {
        return get().diffKind;
    }

    public static final void setDiffKind(int kind) {
        get().diffKind = kind;
    }

    public static final boolean doActivity() {
        return get().doActivity;
    }

    public static final void setDoActivity(boolean value) {
        get().doActivity = value;
    }

    public static final boolean doUsefulness() {
        return get().doUsefulness;
    }

    public static final void setDoUsefulness(boolean value) {
        get().doUsefulness = value;
    }

    public static final boolean debugActivity() {
        return get().debugActivity;
    }

    public static final void setDebugActivity(boolean value) {
        get().debugActivity = value;
    }

    public static final boolean doTBR() {
        return get().doTBR;
    }

    public static final void setDoTBR(boolean value) {
        get().doTBR = value;
    }

    public static final boolean doRecompute() {
        return get().doRecompute;
    }

    public static final void setDoRecompute(boolean value) {
        get().doRecompute = value;
    }

    public static final boolean doMPI() {
        return get().doMPI;
    }

    public static final void setDoMPI(boolean value) {
        get().doMPI = value;
    }

    public static final boolean doOpenMP() {
        return get().doOpenMP;
    }

    public static final void setDoOpenMP(boolean value) {
        get().doOpenMP = value;
    }

    public static final boolean doOpenMPZ3() {
        return get().doOpenMPZ3;
    }

    public static final void setDoOpenMPZ3(boolean value) {
        get().doOpenMPZ3 = value;
    }

    public static final DepsAnalyzer depsAnalyzer() {
        return get().depsAnalyzer;
    }

    public static final void setDepsAnalyzer(DepsAnalyzer analyzer) {
        get().depsAnalyzer = analyzer;
    }

    public static final InOutAnalyzer inOutAnalyzer() {
        return get().inOutAnalyzer;
    }

    public static final void setInOutAnalyzer(InOutAnalyzer analyzer) {
        get().inOutAnalyzer = analyzer;
    }

    public static final MultithreadAnalyzer multithreadAnalyzer() {
        return get().multithreadAnalyzer;
    }

    public static final void setMultithreadAnalyzer(MultithreadAnalyzer analyzer) {
        get().multithreadAnalyzer = analyzer;
    }

    public static final ADActivityAnalyzer adActivityAnalyzer() {
        return get().adActivityAnalyzer;
    }

    public static final void setADActivityAnalyzer(ADActivityAnalyzer analyzer) {
        get().adActivityAnalyzer = analyzer;
    }

    public static final ADTBRAnalyzer adTbrAnalyzer() {
        return get().adTbrAnalyzer;
    }

    public static final void setADTBRAnalyzer(ADTBRAnalyzer analyzer) {
        get().adTbrAnalyzer = analyzer;
    }

    public static final DiffLivenessAnalyzer diffLivenessAnalyzer() {
        return get().diffLivenessAnalyzer;
    }

    public static final void setDiffLivenessAnalyzer(DiffLivenessAnalyzer analyzer) {
        get().diffLivenessAnalyzer = analyzer;
    }

    public static final PointerAnalyzer pointerAnalyzer() {
        return get().pointerAnalyzer;
    }

    public static final void setPointerAnalyzer(PointerAnalyzer analyzer) {
        get().pointerAnalyzer = analyzer;
    }

    public static final ReqExplicit reqExplicitAnalyzer() {
        return get().reqExplicitAnalyzer;
    }

    public static final void setReqExplicitAnalyzer(ReqExplicit analyzer) {
        get().reqExplicitAnalyzer = analyzer;
    }

    public static final String optionsString() {
        return get().optionsString;
    }

    public static final void addOptionsString(String options) {
        get().optionsString = get().optionsString + options;
    }

    public static final boolean mustTangent() {
        return get().mustTangent;
    }

    public static final void setMustTangent(boolean value) {
        get().mustTangent = value;
    }

    public static final boolean mustAdjoint() {
        return get().mustAdjoint;
    }

    public static final void setMustAdjoint(boolean value) {
        get().mustAdjoint = value;
    }

    public static final boolean mustContext() {
        return get().mustContext;
    }

    public static final void setMustContext(boolean value) {
        get().mustContext = value;
    }

    public static final int diffReplica() {
        return get().diffReplica ;
    }

    public static final boolean profile() {
        return get().profile;
    }

    public static final Unit assocAddressDiffTypesUnit(int language) {
        return get().assocAddressDiffTypesUnits[getAssocAddressIndex(language)];
    }

    public static final void setAssocAddressDiffTypesUnit(Unit unit) {
        get().assocAddressDiffTypesUnits[getAssocAddressIndex(unit.language())] = unit;
    }

    public static final int getAssocAddressIndex(int language) {
        int result = FORTRAN_FAMILY;
        if (language == C || language==CUDA) {
            result = C_FAMILY;
        } else if (language == CPLUSPLUS) {
            result = CPLUSPLUS_FAMILY;
        }
        return result;
    }

    public static final boolean removeDeadPrimal() {
        return get().removeDeadPrimal;
    }

    public static final void setRemoveDeadPrimal(boolean value) {
        get().removeDeadPrimal = value;
    }

    public static final boolean removeDeadControl() {
        return get().removeDeadControl;
    }

    public static final void setRemoveDeadControl(boolean value) {
        get().removeDeadControl = value;
    }

    public static final boolean spareDiffReinitializations() {
        return get().spareDiffReinitializations;
    }

    public static final void setSpareDiffReinitializations(boolean value) {
        get().spareDiffReinitializations = value;
    }

    public static final Instruction currentIncludeInstruction() {
        return get().currentIncludeInstruction;
    }

    public static final void setCurrentIncludeInstruction(Instruction instruction) {
        get().currentIncludeInstruction = instruction;
    }

    /**
     * Add a unique counter to a new variable "name",
     * so as to be able to recognize it, e.g. with sed.
     */
    public static final String disambigNewName(String name) {
        if (get().newSHdisambiguator < 0) {
            return name;
        } else {
            get().newSHdisambiguator++;
            return name + "x" + get().newSHdisambiguator;
        }
    }

    /**
     * @return The separator used in file names between directories and files e.g. "/"
     */
    public static final String parserFileSeparator() {
        return get().parserFileSeparator;
    }

    /**
     * The separator used in file names between directories and files e.g. "/"
     */
    public static final void setParserFileSeparator(String separator) {
        get().parserFileSeparator = separator;
    }

    /**
     * @return the string that starts line comments in the given language.
     */
    public static final String commentStart(int language) {
        String result = "";
        switch (language) {
            case FORTRAN:
                result = "C";
                break;
            case FORTRAN90:
            case FORTRAN2003:
                result = "!";
                break;
            case C:
                result = "//";
                break;
            default:
                break;
        }
        return result;
    }

    /**
     * @return the name of the programming language lang.
     */
    public static final String languageName(int lang) {
        String langName = "UNKNOWN";
        switch (lang) {
            case TapEnv.FORTRAN:
                langName = "FORTRAN";
                break;
            case TapEnv.FORTRAN90:
                langName = "FORTRAN90";
                break;
            case TapEnv.FORTRAN2003:
                langName = "FORTRAN2003";
                break;
            case TapEnv.C:
                langName = "C";
                break;
            case TapEnv.CUDA:
                langName = "CUDA";
                break;
            case TapEnv.CPLUSPLUS:
                langName = "CPP";
                break;
            case TapEnv.MIXED:
                langName = "MIXED";
                break;
            default:
                break;
        }
        return langName;
    }

    /**
     * @return the given input language of the current source code.
     */
    public static final int inputLanguage() {
        return get().inputLanguage;
    }

    public static final void setInputLanguage(int lang) {
        get().inputLanguage = lang;
    }

    /**
     * @return the wanted output language of the resulting code.
     */
    public static final int outputLanguage() {
        return get().outputLanguage;
    }

    public static final void setOutputLanguage(int lang) {
        get().outputLanguage = lang;
    }

    public static final int relatedLanguage() {
        return get().relatedLanguage;
    }

    /**
     * @return True if the language of the related Unit or SymbolTable is C.
     */
    public static final boolean relatedLanguageIsC() {
        return get().relatedLanguage == C || get().relatedLanguage == CUDA;
    }

    /**
     * @return True if the language of the related Unit or SymbolTable is C++.
     */
    public static final boolean relatedLanguageIsCPLUSPLUS() {
        return get().relatedLanguage == CPLUSPLUS;
    }

    /**
     * @return True if the language of the related Unit or SymbolTable is Fortran.
     */
    public static final boolean relatedLanguageIsFortran() {
        int language = get().relatedLanguage;
        return language == FORTRAN || language == FORTRAN90 || language == FORTRAN2003;
    }

    /**
     * @return True if the language of the related Unit or SymbolTable is Fortran77.
     */
    public static final boolean relatedLanguageIsFortran77() {
        return get().relatedLanguage == TapEnv.FORTRAN;
    }

    /**
     * @return True if the related language is Fortran90 or higher.
     */
    public static final boolean relatedLanguageIsFortran9x() {
        int language = get().relatedLanguage;
        return language == FORTRAN90 || language == FORTRAN2003;
    }

    public static final void setRelatedLanguage(int lang) {
        get().relatedLanguage = lang;
    }

    public static final void setRelatedLanguage(SymbolTable symbolTable) {
        get().relatedLanguage = symbolTable != null ? symbolTable.language() : get().inputLanguage;
    }

    /**
     * @return a TapList (Language1 Language2 byReference_or_byResult bind_info_list).
     */
    public static final TapList<MixedLanguageInfos> mixedLanguageInfos() {
        return get().mixedLangInfos;
    }

    public static final void setMixedLanguageInfos(TapList<MixedLanguageInfos> infos) {
        get().mixedLangInfos = infos;
    }

    public static final void addMixedLanguageInfos(MixedLanguageInfos infos) {
        get().mixedLangInfos = new TapList<>(infos, get().mixedLangInfos);
    }

    public static final void setLanguages(int rank, int lang) {
        get().languages[rank] = lang;
    }

    public static final int languages(int i) {
        return get().languages[i];
    }

    public static final void warningFortran77WithFortran90Features(Unit unit) {
        if (unit.language() == FORTRAN) {
            unit.setLanguageAndUp(FORTRAN90);
            unit.setHasArrayNotationAndUp() ;
            fileWarning(TapEnv.MSG_WARN, null, "(DD27) " + unit.name() + " contains FORTRAN90 feature");
        }
    }

    /**
     * Set the current directory, used to look for include files
     * set the rootDirectoryPath.
     */
    public static final void setCurrentDirectory(String dir) {
        Path curPath = null;
        if (dir != null && !dir.isEmpty()) {
            curPath = Paths.get(dir).toAbsolutePath().normalize();
        }
        get().currentDirectory = dir;
        if (get().rootDirectoryPath == null) {
            get().rootDirectoryPath = curPath;
        } else if (curPath != null && !get().currentDirectory.isEmpty() && !get().rootDirectoryPath.equals(curPath)) {
            int nbRoot = get().rootDirectoryPath.getNameCount();
            int nbCur = curPath.getNameCount();
            int i = 0;
            int nbEquals = 0;
            while (i < Math.min(nbRoot, nbCur)) {
                if (get().rootDirectoryPath.getName(i).equals(curPath.getName(i))) {
                    nbEquals = i;
                }
                i = i + 1;
            }
            get().rootDirectoryPath = get().rootDirectoryPath.subpath(0, nbEquals + 1);
            get().rootDirectoryPath = curPath.getRoot().resolve(get().rootDirectoryPath);
        }
    }

    public static final void setCurrentParsedFile(String filename) {
        try {
            Path curPath = Paths.get(filename).toAbsolutePath().normalize();
            get().currentParsedFile = curPath.getFileName().toString();
        } catch (java.io.IOError | SecurityException | java.nio.file.InvalidPathException e) {
            String name = filename.substring(filename.lastIndexOf(File.separator) + 1);
            systemWarning(-1, "Malformed input or input contains unmappable characters: " + name);
            get().currentParsedFile = name;
        }
    }

    public static final String currentParsedFile() {
        return get().currentParsedFile;
    }

    public static final void setCurrentTranslationUnitSymbolTable(SymbolTable symbolTable) {
        get().currentTranslationUnitSymbolTable = symbolTable;
    }

    public static final SymbolTable currentTranslationUnitSymbolTable() {
        return get().currentTranslationUnitSymbolTable;
    }

    public static final void setIncludeDirs(TapList<String> includeList) {
        get().includeDirs = includeList;
    }

    public static final TapList<String> includeDirs() {
        return get().includeDirs ;
    }

    public static final void pushIncludeFile(String includeFileName) {
        get().includeFileStack = new TapList<>(includeFileName, get().includeFileStack);
    }

    public static final String popIncludeFile() {
        // Error case where the current include file is forced inlined:
        if (get().includeFileStack==null) return null ;

        String popped = get().includeFileStack.head ;
        get().includeFileStack = get().includeFileStack.tail ;
        return popped ;
    }

    public static final TapList<String> includeFileStack() {
        return get().includeFileStack;
    }

    public static final boolean exitingBackToInclude(String includeFileName) {
        return get().includeFileStack!=null && get().includeFileStack.tail!=null
            && includeFileName.equals(get().includeFileStack.tail.head) ;
    }

    public static final boolean inIncludeFile() {
        return get().inIncludeFile;
    }

    /**
     * Say that we are in an include file, triggered by the given "includeTree".
     *
     * @return the includeTree, rewritten if necessary to be an op_include.
     */
    public static final Tree setInIncludeFile(Tree includeTree) {
        Tree includeCommandAsOpInclude = getIncludeTree(includeTree);
        checkAndUpdateTapEnvInclude(includeCommandAsOpInclude);
        return includeCommandAsOpInclude;
    }

    public static final Tree getIncludeTree(Tree includeTree) {
        Tree result = null;
        if (ILUtils.stopInclude(includeTree, "tapenade end #include")) {
            result = null;
        } else if (ILUtils.inInclude(includeTree, "tapenade begin #include")) {
            if (includeTree.opCode() == ILLang.op_include) {
                result = includeTree;
            } else {
                String include = includeTree.down(1).stringValue()
                        .substring("tapenade begin #include ".length());
                include = filterIncludeFileName(include, true, true);
                result = ILUtils.build(ILLang.op_include, include);
            }
        } else if (includeTree.opCode() == ILLang.op_include) {
            result = includeTree;
        }
        return result;
    }

    /**
     * Checks that "includeTree", which must be an "end-of-include" annotation tree,
     * actually matches the next enclosing include in the current "includeFiles"
     * (or there is no next enclosing include).
     */
    public static final boolean enclosingIncludeIsEmptyOr(Tree includeTree) {
        if (get().includeFiles.tail == null) {
            return true;
        }
        if (includeTree.opCode() != ILLang.op_include) {
            includeTree = includeTree.down(1);
        }
        String nextIncludeFile = includeTree.stringValue();
        if (nextIncludeFile.startsWith("tapenade end #include")) {
            nextIncludeFile = nextIncludeFile.substring("tapenade end #include ".length());
            nextIncludeFile = filterIncludeFileName(nextIncludeFile, true, true);
        }
        return get().includeFiles.tail.head.equals(nextIncludeFile);
    }

    public static final String filterIncludeFileName(String includeFileName, boolean withCapsule, boolean mustExist) {
        // pb seulement pour unix
        String usrStr = "/usr/";
        String optStr = "/opt/";
        String includeStr = "include/";
        if ((includeFileName.startsWith(usrStr) || includeFileName.startsWith(optStr))
                && includeFileName.indexOf(includeStr) > 0) {
            int index = includeFileName.lastIndexOf(includeStr);
            includeFileName = includeFileName.substring(index + includeStr.length());
            if (withCapsule) includeFileName = "<" + includeFileName + ">";
        } else {
            String includeName = includeFileName;
            if (!isStdIncludeName(includeName)) {
                if (includeName.startsWith("\"")) {
                    includeName = includeName.substring(1, includeName.length() - 1);
                }
                boolean includeFileExists = new File(includeName).exists();
                if (!includeFileExists && !get().currentDirectory.isEmpty()) {
                    includeName = get().rootDirectoryPath.toString() + File.separator + includeName;
                    includeFileExists = new File(includeName).exists();
                }
                if (!includeFileExists && mustExist) {
                    parserError("Fatal error: " + includeName + ": No such include file");
                    tapenadeExit(1);
                }
            }
            if (!get().currentDirectory.isEmpty() && includeFileName.startsWith(get().currentDirectory)) {
                Path pathInclude = Paths.get(includeFileName);
                Path absInclude = pathInclude.toAbsolutePath();
                absInclude = absInclude.normalize();
                includeFileName = get().rootDirectoryPath.relativize(absInclude).toString();
            } else {
                TapList<String> includeDirectories = get().includeDirs;
                String includeDir;
                int index = -1;
                while (includeDirectories != null && index == -1) {
                    includeDir = includeDirectories.head;
                    if (includeFileName.startsWith(includeDir)) {
                        index = includeFileName.indexOf(includeDir) + includeDir.length();
                        includeFileName = includeFileName.substring(index);
                    }
                    includeDirectories = includeDirectories.tail;
                }
            }
            includeFileName = ILUtils.normalizePath(includeFileName) ;
            if (withCapsule) includeFileName = "\"" + includeFileName + "\"";
        }
        return includeFileName;
    }

    /**
     * Say that we are no longer in an include file.
     */
    public static final void resetIncludeManager() {
        get().inIncludeFile = false;
        get().includeFiles = null;
        get().includeFileStack = null ;
        get().currentIncludeInstruction = null;
    }

    /**
     * @return True if we are inside a C standard include file.
     */
    public static final boolean inStdCIncludeFile() {
        return get().inIncludeFile && isStdCInclude(get().currentIncludeInstruction);
    }

    public static final boolean isStdIncludeName(String includeName) {
        return includeName.startsWith("<") && includeName.endsWith(">")
                ||
                includeName.endsWith("mpi.h\"")
                        &&
                        //PATCH TEMPORAIRE pour code SEISM de M.Morlighem:
                        !includeName.endsWith("seismmpi.h\"")
                || includeName.endsWith("mpio.h\"")
                || includeName.endsWith("mpif.h")
                || isCPPIncludeBug(includeName);
    }

    private static final boolean isCPPIncludeBug(String includeName) {
        return includeName.equals("\"<built-in>\"")
                || includeName.equals("\"<command line>\"");
    }

    public static final boolean isStdCInclude(Instruction curInclude) {
        boolean result = false;
        if (curInclude.tree != null
                && curInclude.tree.opCode() == ILLang.op_include) {
            String includeName = curInclude.tree.stringValue();
            result = relatedLanguageIsC() && isStdIncludeName(includeName);
            if (!result && curInclude.fromInclude() != null) {
                result = isStdCInclude(curInclude.fromInclude());
            }
        }
        return result;
    }

    // returns true si on n'a pas deja vu l'include (cf pb avec cpp -dI qui duplique le meme #include)
    // et si ce n'est pas un include dans un include standard (cf pb avec stdio.h)
    public static final boolean checkAndUpdateTapEnvInclude(Tree tree) {
        String treeValue = (tree == null ? null : tree.stringValue());
        boolean duplicatedInclude = (tree != null
                && get().currentIncludeInstruction != null
                && get().currentIncludeInstruction.tree != null
                && treeValue.equals(get().currentIncludeInstruction.tree.stringValue()));
        if (!duplicatedInclude) {
            updateTapEnvInclude(tree);
        }
        return tree == null || !(duplicatedInclude || isCPPIncludeBug(treeValue));
    }

    public static final void updateTapEnvInclude(Tree tree) {
        if (tree != null) {
            get().inIncludeFile = true;
            get().includeFiles = new TapList<>(tree.stringValue(), get().includeFiles);
            Instruction oldIncludeInstruction = get().currentIncludeInstruction;
            get().currentIncludeInstruction = new Instruction(tree);
            get().currentIncludeInstruction.setFromInclude(oldIncludeInstruction);
        } else {
            if (get().includeFiles != null) {
                get().includeFiles = get().includeFiles.tail;
                get().currentIncludeInstruction = get().currentIncludeInstruction.fromInclude();
            }
            if (get().includeFiles == null) {
                get().inIncludeFile = false;
            }
        }
    }

    public static final boolean isDiffIncludeFileName(String includeFileName) {
        TapList<TapTriplet<String, String, Tree>> diffIncludeInfos = get().differentiatedIncludeNames.tail;
        boolean isPrimalInclude = false;
        boolean isDiffInclude = false;
        while (diffIncludeInfos != null) {
            TapTriplet<String, String, Tree> diffIncludeInfo = diffIncludeInfos.head;
            if (!isPrimalInclude) {
                isPrimalInclude = includeFileName.equals(diffIncludeInfo.first);
            }
            if (!isDiffInclude) {
                isDiffInclude = includeFileName.equals(diffIncludeInfo.second);
            }
            diffIncludeInfos = diffIncludeInfos.tail;
        }
        return !isPrimalInclude && isDiffInclude;
    }

    public static final boolean alreadyDiffInclude(String includeFileName) {
        TapList<TapTriplet<String, String, Tree>> diffIncludeInfos = get().differentiatedIncludeNames.tail;
        boolean isDiffInclude = false;
        while (diffIncludeInfos != null && !isDiffInclude) {
            TapTriplet<String, String, Tree> diffIncludeInfo = diffIncludeInfos.head;
            isDiffInclude = includeFileName.equals(diffIncludeInfo.second);
            diffIncludeInfos = diffIncludeInfos.tail;
        }
        return isDiffInclude;
    }

    // Dirty patch because of the transformation of "include foo" into "include foo_p" in case of implicit typing (cf set01/B04)
    public static final boolean sameFinalIncludeFile(String include1, String include2) {
        if (include1.equals(include2)) {
            return true;
        }
        String diffIncludeName = getDiffIncludeName(include1);
        if (diffIncludeName != null && diffIncludeName.equals(include2)) {
            return true;
        }
        if (include2 != null) {
            diffIncludeName = getDiffIncludeName(include2);
            return include1.equals(diffIncludeName);
        }
        return false;
    }

    public static final TapTriplet<String, String, Tree> getSetDiffIncludeInfo(String includeFileName) {
        TapTriplet<String, String, Tree> diffIncludeInfo =
                TapList.assqStringInTriplet(includeFileName, get().differentiatedIncludeNames.tail);
        if (diffIncludeInfo == null) {
            diffIncludeInfo = new TapTriplet<>(includeFileName, null, null);
            get().differentiatedIncludeNames.placdl(diffIncludeInfo);
        }
        return diffIncludeInfo;
    }

    public static final String getDiffIncludeName(String includeFileName) {
        TapTriplet<String, String, Tree> diffIncludeInfo =
                TapList.assqStringInTriplet(includeFileName, get().differentiatedIncludeNames.tail);
        return diffIncludeInfo == null ? null : diffIncludeInfo.second;
    }

    public static final String differentiateIncludeName(String includeName, String suffix) {
        String result;
        if (!isStdIncludeName(includeName)) {
            if (includeName.startsWith("\"")) {
                includeName = "\"" + stripPath(includeName.substring(1));
            } else {
                includeName = stripPath(includeName);
            }
            int index = includeName.lastIndexOf('.');
            String name = includeName;
            if (index >= 0) {
                name = includeName.substring(0, index);
                result = extendStringWithSuffix(name, suffix) + "." + getExtension(includeName);
            } else {
                if (name.endsWith("\"")) {
                    name = name.substring(0, name.length() - 1);
                }
                result = extendStringWithSuffix(name, suffix);
            }
        } else {
            // Something probably wrong somewhere: differentiation requested to diff something from standard include file!
            toolWarning(-1, "(Differentiation of include file) Don't want to differentiate a standard include: " + includeName);
            result = includeName;
        }
        return result;
    }

    /**
     * Place a source-code correspondence between "source" and "code" trees
     * for future display tools (e.g. html).
     */
    public static final void setSourceCodeCorrespondence(Tree source, Tree code,
                                                         boolean forward, boolean backward) {
        TapPair<String, String> sourceTags = source.getAnnotation("toOtherTags");
        TapPair<String, String> codeTags = code.getAnnotation("toOtherTags");
        if (sourceTags == null) {
            sourceTags = new TapPair<>(null, null);
            source.setAnnotation("toOtherTags", sourceTags);
        }
        if (codeTags == null) {
            codeTags = new TapPair<>(null, null);
            code.setAnnotation("toOtherTags", codeTags);
            //Each Tree is annotated with an TapPair.
            // The field "first" is the label that identifies the current place in the current Tree.
            // The field "second" is the label that identifies the corresponding place in the other Tree.
        }
        if (forward) {
            if (sourceTags.second == null) {
                if (codeTags.first == null) {
                    codeTags.first = "i" + get().nextCodeTag;
                    get().nextCodeTag++;
                }
                sourceTags.second = codeTags.first;
            }
        }
        if (backward) {
            if (codeTags.second == null) {
                if (sourceTags.first == null) {
                    sourceTags.first = "i" + get().nextSourceTag;
                    get().nextSourceTag++;
                }
                codeTags.second = sourceTags.first;
            }
        }
    }

    /**
     * Reset counters for source-message correspondence.
     */
    public static final void initializeSourceMessageCorrespondence() {
        get().nextSourceMsgTag = 0;
        get().nextMessageTag = 0;
    }

    /**
     * Extends a string with a suffix.
     * If Suffix contains "%", this "%" stands for the original string.
     */
    public static final String extendStringWithSuffix(String str, String suffix) {
        // Not sure where to put this very basic utility function
        int center = suffix.indexOf('%');
        if (center < 0) {
            center = -1;
        }
        return (center <= 0 ? "" : suffix.substring(0, center)) + str + suffix.substring(center + 1);
    }

    /**
     * Extends the given String with something to distinguish its replicas.
     * Replicas are introduced by option "-diffReplica N"
     */
    public static final String extendReplica(String str, int replicaRank) {
        if (TapEnv.diffReplica()<2) {
            return str ;
        } else if (TapEnv.diffReplica()==2) { // cf Onera's thinking of complex numbers...
            return str+(replicaRank==0 ? "r" : "i") ;
        } else {
            return str+(replicaRank+1) ;
        }
    }

    /**
     * Trace detail level.
     */
    public static int traceLevel = 0 ;

    /**
     * Maximum trace detail level.
     * Traces and Milestones with a detail level above "traceLevel" are not shown.
     */
    public static final void setTraceLevel(int level) {
        traceLevel = level;
    }

    /**
     * Maximum inverse severity of shown messages.
     * Messages with inverse severity above "msgLevel" are not shown.
     */
    private static int msgLevel = TapEnv.MSG_DEFAULT;

    /**
     * Maximum inverse severity of shown messages.
     * Messages with inverse severity above "msgLevel" are not shown.
     */
    public static final void setMsgLevel(int level) {
        msgLevel = level;
    }

    /**
     * @return severity of shown messages.
     */
    public static final int msgLevel() {
        return msgLevel;
    }

    public static final OutputStream curOutputStream() {
        return get().curOutputStream;
    }

    public static final void setCurOutputStream(OutputStream outStream) {
        get().curOutputStream = outStream;
    }

    /**
     * Push the given "newOutputStream" as the new current OutputStream for all Strings
     * printed and all messages emitted through this TapEnv.
     */
    public static final void pushOutputStream(OutputStream newOutputStream) {
        get().flushOutputStream();
        get().outputStreamStack = new TapList<>(newOutputStream, get().outputStreamStack);
        get().curOutputStream = newOutputStream;
    }

    /**
     * Pop back to the previous current OutputStream for all Strings
     * printed and all messages emitted through this TapEnv.
     */
    public static final void popOutputStream() {
        get().flushOutputStream();
        if (get().outputStreamStack.tail != null) {
            get().outputStreamStack = get().outputStreamStack.tail;
        }
        get().curOutputStream = get().outputStreamStack.head;
    }

    /**
     * Close the current OutputStream that was used for all Strings
     * printed and all messages emitted through this TapEnv.
     */
    public static final void closeOutputStream() {
        if (get().curOutputStream != System.out) {
            try {
                get().curOutputStream.close();
            } catch (java.io.IOException e) {
                System.out.println("OutputStream broken "+e);
            }
        }
    }

    /**
     * When the "level" of the given message "str" is under trace threshold (traceLevel),
     * prints "str" then newline, on the traceOutputStream.
     * @param level this message level e.g. 5:topmost, 10:one phase, 15:one phase on one Unit...
     * @param str the message to print (must not be null)
     */
    public static final void printOnTrace(int level, String str) {
        if (level <= traceLevel) {
            printOnTrace(str);
        }
    }

    /**
     * When the "level" of the given message "str" is under trace threshold (traceLevel),
     * prints "str" on the traceOutputStream.
     * @param level this message level e.g. 5:topmost, 10:one phase, 15:one phase on one Unit...
     * @param str the message to print (must not be null)
     */
    public static final void printlnOnTrace(int level, String str) {
        if (level <= traceLevel) {
            printlnOnTrace(str);
        }
    }

    public static final void printlnOnTrace(int level) {
        if (level <= traceLevel) {
            printlnOnTrace();
        }
    }

    /**
     * Same as printlnOnTrace(), but indents first.
     */
    public static final void indentprintlnOnTrace(String str) {
        indentOnTrace();
        printlnOnTrace(str);
    }

    /**
     * Same as printlnOnTrace(), but indents first.
     */
    public static final void indentprintlnOnTrace() {
        indentOnTrace();
        printlnOnTrace();
    }

    /**
     * Same as printOnTrace(), but indents first.
     */
    public static final void indentprintOnTrace(String str) {
        indentOnTrace();
        printOnTrace(str);
    }

    /**
     * Prints "str" then newline on the traceOutputStream.
     */
    public static final void printlnOnTrace(String str) {
        printOnTrace(str + System.lineSeparator());
        get().traceLineStart = true ;
    }

    public static final void printlnOnTrace() {
        get().traceLineStart = false ; // To avoid printing white space before this newLine.
        printOnTrace(System.lineSeparator());
        get().traceLineStart = true ;
    }

    /**
     * Prints "str" (non-null please) on the traceOutputStream.
     */
    public static final void printOnTrace(String str) {
        pushOutputStream(get().traceOutputStream);
        try {
            if (get().traceLineStart) {indentOnTrace(); get().traceLineStart = false;}
            print(str);
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for trace broken "+e);
        }
        popOutputStream();
    }

    /**
     * Prints "str" (non-null please) on the dumpOutputStream.
     */
    public static final void printOnDump(String str) {
        pushOutputStream(get().dumpOutputStream);
        try {
            print(str);
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for dump broken "+e);
        }
        popOutputStream();
    }

    /** Convention: argInfo[0] is about the result */
    public static final void dumpArgInfoOnTrace(Object[] argInfo) {
        pushOutputStream(get().traceOutputStream);
        try {
            print("[") ;
            for (int i=1 ; i<argInfo.length ; ++i) {
                if (i!=1) print(", ") ;
                dumpObject(argInfo[i]) ;
            }
            print(" -> ") ;
            dumpObject(argInfo[0]) ;
            print("]") ;
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for trace broken "+e);
        }
        popOutputStream();
    }

    /**
     * Prints a dump "obj" on the traceOutputStream.
     * This method takes care of avoiding infinite looping in circular references.
     */
    public static final void dumpOnTrace(Object obj) {
        pushOutputStream(get().traceOutputStream);
        try {
            if (get().traceLineStart) {indentOnTrace(); get().traceLineStart = false;}
            dumpObject(obj);
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for trace broken "+e);
        }
        popOutputStream();
    }

    /**
     * Dumps a BoolMatrix on the traceOutputStream, knowing its row and column Maps
     */
    public static final void dumpBoolMatrixOnTrace(BoolMatrix boolMatrix) {
        pushOutputStream(get().traceOutputStream);
        try {
            if (boolMatrix == null) {
                printlnOnTrace("null");
            } else {
                boolMatrix.dump(new int[]{0, boolMatrix.nRows}, new int[]{0, boolMatrix.nCols});
            }
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for dump broken "+e);
        }
        popOutputStream();
    }

    /**
     * Dumps a BoolMatrix on the traceOutputStream, knowing its row and column Maps,
     * and displaying an additional split at and after splitRow and splitCol.
     */
    public static final void dumpBoolMatrixOnTrace(BoolMatrix boolMatrix, int splitRow, int splitCol) {
        pushOutputStream(get().traceOutputStream);
        try {
            if (boolMatrix == null) {
                printlnOnTrace("null");
            } else {
                boolMatrix.dump(new int[]{0, splitRow, boolMatrix.nRows}, new int[]{0, splitCol, boolMatrix.nCols});
            }
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for dump broken "+e);
        }
        popOutputStream();
    }

    /**
     * Message that indicates a problem with the system e.g. "file not found".
     */
    public static final void systemError(String message) {
        get().emitMessage(NO_POSITION, null, "System: " + message, TapEnv.MSG_ERROR);
    }

    /**
     * Message that indicates a problem with the system e.g. "file not found".
     */
    public static final void systemWarning(int inverseSeverity, String message) {
        get().emitMessage(NO_POSITION, null, "System: " + message, inverseSeverity);
    }

    /**
     * Message that indicates a serious error in Tapenade implementation.
     */
    public static final void toolError(String message) {
        get().emitMessage(NO_POSITION, null, "Tool: " + message, TapEnv.MSG_ERROR);
    }

    /**
     * Message that indicates a problem with Tapenade implementation,
     * but Tapenade will try to go on. e.g. "Unexpected operator" or "not yet implemented".
     *
     * @param inverseSeverity Indicates problem severity,
     *                        from 0 (very serious) up (less and less serious).
     *                        May also be -1, in which case the message is always shown.
     */
    public static final void toolWarning(int inverseSeverity, String message) {
        get().emitMessage(NO_POSITION, null, "Tool: " + message, inverseSeverity);
    }

    /**
     * Message that indicates an inconsistency in the command sent to Tapenade.
     */
    public static final void commandWarning(int inverseSeverity, String message) {
        get().emitMessage(NO_POSITION, null, "Command: " + message, inverseSeverity);
    }

    /**
     * Message that indicates a parser error.
     */
    public static final void parserError(String message) {
        get().hasParsingError = true;
        get().emitMessage(NO_POSITION, null, "File: " + message, TapEnv.MSG_ERROR);
    }

    /**
     * Message that indicates a defect in the files (e.g. type-checking, aliasing, irreducible loops, etc...).
     */
    public static final void fileError(Tree tree, String message) {
        Unit relUnit = get().relatedUnit;
        if (tree == null && relUnit != null) {
            if (relUnit.entryBlock() != null && relUnit.entryBlock().headInstr() != null) {
                tree = relUnit.entryBlock().headInstr().tree;
            } else if (relUnit.isModule()) {
                tree = relUnit.parametersOrModuleNameTree;
            }
        }
        int position = tree == null ? NO_POSITION : ILUtils.position(tree);
	get().emitMessage(position, tree, message, TapEnv.MSG_ERROR);
    }

    /**
     * Message that indicates a defect in the files (e.g. type-checking, aliasing, irreducible loops, etc...).
     */
    public static final void fileWarning(int inverseSeverity, int position, String message) {
        get().emitMessage(position, null, message, inverseSeverity);
    }

    /**
     * Message that indicates a defect in the files (e.g. type-checking, aliasing, irreducible loops, etc...).
     */
    public static final void fileWarning(int inverseSeverity, Tree tree, String message) {
        Unit relUnit = get().relatedUnit;
        if (tree == null && relUnit != null) {
            if (relUnit.entryBlock() != null && relUnit.entryBlock().headInstr() != null) {
                tree = relUnit.entryBlock().headInstr().tree;
            } else if (relUnit.isModule()) {
                tree = relUnit.parametersOrModuleNameTree;
            }
        }
        int position = tree == null ? NO_POSITION : ILUtils.position(tree);
        get().emitMessage(position, tree, message, inverseSeverity);
    }

    public static final void addMessageAnnotation(Tree tree, String displayMessage) {
        if (tree != null) {
            TapList<String> msgTty = tree.getAnnotation("message");
            if (msgTty == null) {
                msgTty = new TapList<>(displayMessage, null);
                tree.setAnnotation("message", msgTty);
            } else {
                TapList<String> tailMsg = msgTty;
                while (msgTty != null) {
                    tailMsg = msgTty;
                    msgTty = msgTty.tail;
                }
                tailMsg.placdl(displayMessage);
            }
        }
    }

    /**
     * @return the list of all messages (of type PositionAndMessage) that have been emitted
     * about the given "origCallGraph" and also possibly about its differentiated "diffCallGraph".
     */
    public static final TapList<PositionAndMessage> getAllMessages(CallGraph origCallGraph, CallGraph diffCallGraph,
                                                                   TapList<Unit> transformedUnits, boolean showMessagesOfAllUnits) {
        TapList<PositionAndMessage> hdAllMessages = new TapList<>(null, null);
        TapList<PositionAndMessage> tlAllMessages = hdAllMessages;
        TapList<PositionAndMessage> messagesList = get().danglingMessages.tail;
        while (messagesList != null) {
            tlAllMessages = tlAllMessages.placdl(messagesList.head);
            messagesList = messagesList.tail;
        }
        if (origCallGraph != null) {
            tlAllMessages =
                    getMessages(tlAllMessages, origCallGraph, transformedUnits, showMessagesOfAllUnits);
        }
        if (diffCallGraph != null) {
            tlAllMessages =
                    getMessages(tlAllMessages, diffCallGraph, transformedUnits, true);
        }
        return hdAllMessages.tail;
    }

    private static final TapList<PositionAndMessage> getMessages(TapList<PositionAndMessage> tlAllMessages, CallGraph callGraph,
                                                                 TapList<Unit> transformedUnits, boolean showMessagesOfAllUnits) {
        if (callGraph != null) {
            TapList<Unit> allUnits = callGraph.sortedUnits();
            if (allUnits == null) {
                allUnits = callGraph.units();
            }
            // on rajoute les intrinsicUnits qui peuvent avoir des messages TC
            allUnits = TapList.append(allUnits, callGraph.intrinsicUnits());
            // on rajoute les interfaceUnits qui peuvent avoir des messages (signatures differ)
            allUnits = TapList.append(allUnits, callGraph.interfaceUnits());
            // also consider dummy Units that have been added for procedure calls with unknown procedure name:
            allUnits = TapList.append(allUnits, callGraph.dummyUnits) ;
            Unit unit;
            while (allUnits != null) {
                unit = allUnits.head;
                // Show errors of an original Unit only when this original Unit is
                // transformed somehow by the differentiation process, i.e. when there
                // exists a diff Unit or even a transformed "copied" Unit in the diffCallGraph.
                // Show them also for interfaces or when we are debugging (traceLevel>default zero)
                boolean showMsgOfUnit = showMessagesOfAllUnits ||
                        TapList.contains(transformedUnits, unit) ||
                        unit.isInterface() ||
                        traceLevel > 0 ;
                if (showMsgOfUnit) {
                    tlAllMessages = getOneMessage(callGraph, unit, tlAllMessages);
                    // Except when we are debugging, avoid repeating the same message list:
                    if (traceLevel <= 0 && !showMessagesOfAllUnits) {
                        unit.messages.tail = null;
                    }
                }
                allUnits = allUnits.tail;
            }
        }
        return tlAllMessages;
    }

    private static final TapList<PositionAndMessage> getOneMessage(CallGraph callGraph, Unit unit,
                                                                   TapList<PositionAndMessage> tlAllMessages) {
        PositionAndMessage pm;
        TapList<PositionAndMessage> messagesList = unit.messages.tail;
        String curUnitName;
        while (messagesList != null) {
            pm = messagesList.head;
            // Sometimes the same pm gets attached to 2 different Units:
            if (pm.unit() != null && !pm.unit().name().equals(unit.name())) {
                pm = new PositionAndMessage(pm);
            }
            if (!unit.isExternal()) {
                Unit curUnit = unit;
                if (curUnit.origUnit != null) {
                    curUnit = curUnit.origUnit; // sinon on affiche diffUnit dans la partie html pour le source
                }
                while (!curUnit.isTopInFile() && curUnit.upperLevelUnit() != null) {
                    curUnit = curUnit.upperLevelUnit();
                }
                pm.setUnit(unit);
                curUnitName = curUnit.name();
                if (curUnit.translationUnitSymbolTable() != null) {
                    curUnitName = curUnit.translationUnitSymbolTable().containerFileName;
                }
                if (callGraph == get().origCallGraph() || unit.origUnit != null) {
                    pm.fileName = curUnitName + get().preprocessFileSuffix;
                } else {
                    pm.fileName = curUnitName;
                }
            }
            tlAllMessages = tlAllMessages.placdl(pm);
            messagesList = messagesList.tail;
        }
        return tlAllMessages;
    }

// Inline and remove when we are happy with this new style!
protected final static boolean BETTERMESSAGESTYLE = false ;

    /**
     * Save all messages as a text file named "msgFileName".
     */
    public static final void saveAllMessagesAsText(TapList<PositionAndMessage> msgs, String msgFileName) {
        File file = new File(msgFileName);
        if (file.exists()) {
            if (!file.renameTo(new File(msgFileName + '~'))) {
                TapEnv.printlnOnTrace("Could not open output file " + msgFileName + '~');
            }
        }
        try {
            OutputStream textFile = java.nio.file.Files.newOutputStream(
                    Paths.get(msgFileName));
            pushOutputStream(textFile);
            int nbMsg = 1;
            PositionAndMessage pm;
            String message ;
            while (msgs != null) {
                pm = msgs.head;
                message = pm.message() ;
if (!BETTERMESSAGESTYLE) {
                String unitName = (pm.unit() == null ? "" : pm.unit().name()) ;
                if (unitName != null && !unitName.isEmpty()) {
                    message = unitName+": "+message ;
                }
}
                println(nbMsg + " " + message) ;
                msgs = msgs.tail;
                ++nbMsg;
            }
            textFile.close();
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for messages in text file broken "+e);
        }
        popOutputStream();
    }

    /**
     * Save all messages as a HTML file named "msgFileName".
     */
    public static final void saveAllMessagesAsHTML(TapList<PositionAndMessage> msgs, String msgFileName,
                                                   String cssDir, String htmlDirectory,
                                                   String outputFileName, boolean hasGlobalDeclarations,
                                                   String preprocessSuffix) {
        String name = "";
        Unit unit;
        String unitName = "";
        String thisMsgTarget;
        int nbMsg = 1;
        try {
            OutputStream htmlFile = java.nio.file.Files.newOutputStream(
                    Paths.get(msgFileName));
            pushOutputStream(htmlFile);
            println("<!DOCTYPE html>");
            println("<title>Generated by TAPENADE</title>");
            println(TapEnv.bootstrapCss);
            println("<link type=\"text/CSS\" rel=\"styles" +
                    "heet\" href=\"" + cssDir + "tapenade.css\">");
            println("<body>");
            println();
            println("<pre>");
            while (msgs != null) {
                if (msgs.head.tags.second != null) {
                    print("<a name=\"" + msgs.head.tags.second + "\"></a>");
                }
                unit = msgs.head.unit();
                thisMsgTarget = TapEnv.stripPath(msgs.head.fileName);
                if (unit != null) {
                    unitName = unit.shortName() + ": ";
                }
                if (outputFileName != null && !outputFileName.isEmpty()) {
                    name = outputFileName + preprocessSuffix + getLangSuffix(unit);
                } else if (!thisMsgTarget.isEmpty()) {
                    name = thisMsgTarget + getLangSuffix(unit);
                }
                if (hasGlobalDeclarations && name.isEmpty()) {
                    name = "GlobalDeclarations_p";
                }
                String message = msgs.head.message();
                if (!name.isEmpty()) {
                    print(nbMsg + " " + unitName);
                    print(faqLink(message));
                    String targetAnchor = msgs.head.tags.first;
                    println("<a href=\"" + htmlDirectory + name + ".html" + (targetAnchor == null ? "" : "#" + targetAnchor)
                            + "\" target=\"origFile\">" + norefman(message) + "</a>");
                } else {
                    print(nbMsg + " " + faqLink(message));
                    println(unitName + norefman(message));
                }
                msgs = msgs.tail;
                ++nbMsg;
            }
            println("</pre>");
            println("</body>");
            htmlFile.close();
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for messages in HTML file broken "+e);
        }
        popOutputStream();
    }

    private static final String getLangSuffix(Unit unit) {
        String langSuffix;
        if (unit == null) {
            Unit translationUnit = null;
            boolean ok = false;
            TapList<SymbolTable> tuSTs = get().origCallGraph().getTranslationUnitSymbolTables();
            while (!ok && tuSTs != null) {
                translationUnit = tuSTs.head.unit;
                ok = !translationUnit.isPredefinedModuleOrTranslationUnit();
                tuSTs = tuSTs.tail;
            }
            assert translationUnit != null;
            langSuffix = outputExtension(translationUnit.language());
        } else {
            langSuffix = outputExtension(unit.language());
        }
        return langSuffix;
    }

    /**
     * Tapenade FAQ link to the message description.
     *
     * @param message Tapenade error or warning.
     * @return href to the corresponding faq.html entry.
     */
    private static final String faqLink(String message) {
        String result = "";
        if (message.matches(".*\\([A-Z][A-Z]\\d\\d\\).*")) {
            int first;
            int last;
            first = message.indexOf('(');
            last = message.indexOf(')', first);
            String errorStr = message.substring(first + 1, last);
            result = "<a href=\"http://www-sop.inria.fr/ecuador/tapenade/userdoc/build/html/tapenade/faq.html#"
                    + errorStr + "\" target= \"_blank\">(" + errorStr + ")</a> ";
        }
        return result;
    }

    private static final String norefman(String message) {
        String result = message;
        if (message.matches(".*\\([A-Z][A-Z]\\d\\d\\).*")) {
            result = message.substring(message.indexOf(')') + 2);
        }
        return result;
    }

    /**
     * Prints String "str" onto this TapEnv's current OutputStream,
     * i.e. either the top of the stack of pushed OutputStream's, or System.out.
     * Used in printOnTrace, printOnDump, dump methods.
     *
     * @param str String to print.
     * @throws java.io.IOException if an output error is detected.
     */
    public static final void print(String str) throws java.io.IOException {
        get().curOutputStream.write(str.getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    /**
     * println to avoid system dependent "string\\n" or
     * string concatenation "string" + "\\n".
     *
     * @throws java.io.IOException if an output error is detected.
     */
    public static final void println(String str) throws java.io.IOException {
        get().curOutputStream.write((str + System.lineSeparator()).getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    public static final void println() throws java.io.IOException {
        get().curOutputStream.write(System.lineSeparator().getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    public static final int traceIndent() {
        return get().traceIndent;
    }

    public static final void setTraceIndent(int amount) {
        get().traceIndent = amount;
    }

    public static final void incrTraceIndent(int delta) {
        get().traceIndent += delta;
    }

    public static final void decrTraceIndent(int delta) {
        get().traceIndent -= delta;
    }

    /**
     * Prints indentation on the traceOutputStream.
     */
    public static final void indentOnTrace(int amount) {
        pushOutputStream(get().traceOutputStream);
        try {
            indent(amount);
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for trace broken "+e);
        }
        popOutputStream();
    }

    public static final void indentOnTrace() {
        pushOutputStream(get().traceOutputStream);
        try {
            indent(get().traceIndent);
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for trace broken "+e);
        }
        popOutputStream();
    }

    /**
     * Prints "indent" white spaces onto this TapEnv's current OutputStream.
     */
    public static final void indent(int indent) throws java.io.IOException {
        StringBuilder whites = new StringBuilder();
        for (int i = indent; i > 0; --i) {
            whites.append(' ');
        }
        get().curOutputStream.write(whites.toString().getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    /**
     * Prints "indent" white spaces, then String "str", onto this TapEnv's current OutputStream.
     */
    public static final void indentPrint(int indent, String str) throws java.io.IOException {
        indent(indent);
        get().curOutputStream.write(str.getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    /**
     * Prints "indent" white spaces, then String "str", then newline, onto this TapEnv's current OutputStream.
     */
    public static final void indentPrintln(int indent, String str) throws java.io.IOException {
        indent(indent);
        get().curOutputStream.write((str + System.lineSeparator()).getBytes(java.nio.charset.StandardCharsets.UTF_8));
    }

    /**
     * Prints in detail the contents of data-flow info "boolVector" onto this.curOutputStream,
     * prefixed with the given description "text" and a description "mapName" of its map "map".
     */
    public static final void printlnMappedBoolVector(String text, BoolVector boolVector,
                                                     int len, String mapName)
            throws java.io.IOException {
        print(text);
        dumpBoolVector(boolVector, len, mapName);
        println();
    }

    /**
     * Prints in detail the contents of data-flow info "boolVectorPair" onto this.curOutputStream,
     * prefixed with the given description "text". We assume boolVectorPair is a pair of data-flow info,
     * first part on standard zones following "map", second part on pointer zones following ptrMap.
     * The second part is needed e.g in diffLiveness or TBR analyses, and stands for differentiated pointers.
     */
    public static final void printlnMappedBoolVectorWithDiffPtr(String text, TapPair<BoolVector, BoolVector> boolVectorPair,
                                                                int len, String mapName, int ptrLen, String ptrMapName)
            throws java.io.IOException {
        printlnMappedBoolVectorWithDiffPtr(text, (boolVectorPair == null ? null : boolVectorPair.first), len, mapName,
                (boolVectorPair == null ? null : boolVectorPair.second), ptrLen, ptrMapName);
    }

    /**
     * Same as printlnMappedBoolVectorWithDiffPtr(TapPair,...), but the two BoolVectors are passed individually
     */
    public static final void printlnMappedBoolVectorWithDiffPtr(String text, BoolVector boolVector, int len, String mapName,
                                                                BoolVector ptrBoolVector, int ptrLen, String ptrMapName)
            throws java.io.IOException {
        print(text);
        dumpBoolVector(boolVector, len, mapName);
        print(" Ptr:");
        dumpBoolVector(ptrBoolVector, ptrLen, ptrMapName);
        println();
    }

    /**
     * Prints in detail the contents of "boolVector", of given length,
     * onto this.curOutputStream.
     */
    public static final void dumpBoolVector(BoolVector boolVector, int length, String mapName)
            throws java.io.IOException {
        if (boolVector == null) {
            print("null");
        } else {
            print(mapName + " ");
            if (boolVector.isFalse(length)) {
                print("empty");
            } else {
                boolVector.dump(length);
            }
        }
    }

    public static final void dumpBoolMatrix(BoolMatrix boolMatrix, String rowMapName, String colMapName)
        throws java.io.IOException {
        if (boolMatrix==null) {
            print("null");
        } else if (boolMatrix.nRows==0 || boolMatrix.nCols==0) {
            print(boolMatrix.nRows+"x"+boolMatrix.nCols+" matrix") ;
        } else {
            for (int i=0 ; i<boolMatrix.nRows ; ++i) {
                if (boolMatrix.rows[i]==null) {
                    println("null") ;
                } else {
                    boolMatrix.rows[i].dump(boolMatrix.nCols) ;
                }
            }
        }
    }

    /**
     * Prints in detail the contents of "object",
     * which must belong to this package, onto this.curOutputStream.
     */
    public static final void dumpObject(Object object) throws java.io.IOException {
        if (object == null) {
            print("null");
        } else if (object instanceof Object[]) {
            Object[] array = (Object[]) object;
            print("{");
            for (int i = 0; i < array.length; i++) {
                dumpObject(array[i]);
                if (i < array.length - 1) {
                    print(" ; ");
                }
            }
            print("}");
        } else if (object instanceof Boolean || object instanceof Integer) {
            print(object.toString());
        } else if (object instanceof String) {
            print((String) object);
        } else if (object instanceof BoolVector) {
            print(((BoolVector) object).toString());
        } else if (object instanceof BoolMatrix) {
            ((BoolMatrix) object).dump();
        } else if (object instanceof Tree) {
            ILUtils.dump((Tree) object, 0);
        } else if (object instanceof CallGraph) {
            ((CallGraph) object).dump(0);
        } else if (object instanceof Unit) {
            ((Unit) object).dump(0);
        } else if (object instanceof Block) {
            ((Block) object).dump(0);
        } else if (object instanceof BlockStorage) {
            ((BlockStorage) object).dump();
        } else if (object instanceof FGArrow) {
            ((FGArrow) object).dump();
        } else if (object instanceof Instruction) {
            ((Instruction) object).dump();
        } else if (object instanceof SymbolTable) {
            ((SymbolTable) object).dump(0);
        } else if (object instanceof SymbolDecl) {
            ((SymbolDecl) object).dump(0);
        } else if (object instanceof ArrayDim) {
            ((ArrayDim) object).dump();
        } else if (object instanceof WrapperTypeSpec) {
            ((WrapperTypeSpec) object).dump();
        } else if (object instanceof TypeSpec) {
            ((TypeSpec) object).dump();
        } else if (object instanceof ZoneInfo) {
            ((ZoneInfo) object).dump();
        } else if (object instanceof ControlStruct) {
            ((ControlStruct) object).dump(0);
        } else if (object instanceof TapList) {
            ((TapList) object).dump();
        } else if (object instanceof TapIntList) {
            ((TapIntList) object).dump();
        } else if (object instanceof InstructionMask) {
            ((InstructionMask) object).dump();
        } else {
            println("Don't know how to dump objects of this type: " + object);
        }
    }

    /**
     * Resets the mechanism that ensures that a SymbolTable is printed only once.
     */
    public static final void resetSeenSymbolTables() {
        get().seenSymbolTables = null;
    }

    /**
     * Retrieves the short name of a SymbolTable that has already been printed.
     *
     * @return null if not already printed.
     */
    public static final String getSeenSymbolTableShortName(SymbolTable symbolTable) {
        return TapList.cassq(symbolTable, get().seenSymbolTables);
    }

    protected static final void dumpSymbolTableIfNew(SymbolTable symbolTable, int indent) throws java.io.IOException {
        if (symbolTable == null) {
            println("NULL SymbolTable");
        } else if (getSeenSymbolTableShortName(symbolTable) != null) {
            println("already seen " + symbolTable.shortName() + " (" + symbolTable.addressChain() + ')');
        } else {
            println();
            symbolTable.dump(indent + 4);
            println();
            get().seenSymbolTables = new TapList<>(new TapPair<>(symbolTable, "seen"), get().seenSymbolTables);
        }
    }

    public static final String stripPath(String completeFileName) {
        try {
            return new File(completeFileName).getName();
        } catch (NullPointerException e) {
            TapEnv.printlnOnTrace(e.toString());
            return "xxx?";
        }
    }

    public static final String stripLanguageExtension(String fileName) {
        int dotPos = fileName.lastIndexOf('.');
        if (dotPos >= 0) {
            fileName = fileName.substring(0, dotPos);
        }
        return fileName;
    }

    public static final boolean stringContainedIn(String str, String[] strings) {
        boolean found = false;
        for (int i = strings.length - 1; !found && i > 0; --i) {
            found = strings[i].equals(str);
        }
        return found;
    }

    /**
     * Displays an integer on 3 characters, padding with white space if needed.
     */
    public static final String str3(int i) {
        if (i > 99) {
            return "" + i;
        } else if (i > 9) {
            return " " + i;
        } else {
            return "  " + i;
        }
    }

    public static final void tapenadeExit(int exitCode) {
        if (!isServlet) {
            if (exitCode != 0) {
                TapEnv.printlnOnTrace("tapenade: Differentiation failed");
            }
            System.exit(exitCode);
        }
    }
}
