/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ActivityPattern;

/**
 * A symbol declaration object specialized for variables.
 */
public class VariableDecl extends SymbolDecl {
    private WrapperTypeSpec type;

    public void setType(WrapperTypeSpec typeSpec) {
        this.type = typeSpec;
    }

    public Integer constantValue;
    /**
     * for a formal parameter, its rank in the parameters list. From 1 up (special 0 for result).
     */
    public int formalArgRank = SymbolTableConstants.NOT_A_FORMAL_ARG;
    /**
     * for a VariableDecl in generated code (e.g. in the differentiated CallGraph),
     * the formalArgRank of its counterpart in the source (i.e. in the original CallGraph).
     */
    public int formalArgRankInOrigUnit = SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK;
    public boolean isReturnVar;
    public FunctionDecl isReturnVarFromFunction;
    /**
     * True to remember that this is a SUBROUTINE's name.
     */
    public boolean isFunctionName;
    /**
     * True to remember that this is certainly a variable
     * (i.e. certainly not a SUBROUTINE's name).
     */
    public boolean isVariableName;
    /**
     * When this is a "pointer-to-function" variable, holding a pointer to a function,
     * holds the function it is given upon initialization.
     */
    public FunctionDecl[] initFunctionDecls;
    public TapList activityInfo;
    /**
     * The tree of zones of this variable (with a "hat").
     */
    private TapList toZones = new TapList<>(null, null);
    /**
     * for a constant, its value
     * for a variable, its initialization with op_assign
     * Contient l'arbre complet car la partie droite peut etre
     * modifiee au typeCheck (arrayAccess &rarr; call).
     */
    private Tree initializationTree;

    public VariableDecl(Tree identTree, WrapperTypeSpec typeSpec) {
        super(identTree, SymbolTableConstants.VARIABLE);
        this.type = typeSpec;
    }

    public VariableDecl(Tree identTree, WrapperTypeSpec typeSpec, boolean isATrueSymbolDecl) {
        super(identTree, SymbolTableConstants.VARIABLE);
        this.type = typeSpec;
        this.isATrueSymbolDecl = isATrueSymbolDecl;
    }

    public VariableDecl() {
        super();
    }

    public void setReturnVar() {
        formalArgRank = 0 ;
        isReturnVar = true;
    }

    public void setReturnVar(FunctionDecl funcDecl, boolean isFortran) {
        if (!isFortran) {
            //In Fortran, don't set formalArgRank to 0, because return var is not function name.
            formalArgRank = 0 ;
        }
        isReturnVar = true;
        isReturnVarFromFunction = funcDecl;
    }

    public Tree initializationTree() {
        if (initializationTree == null || initializationTree.isAtom()) {
            return initializationTree;
        } else {
            return initializationTree.down(2);
        }
    }

    public Tree getInitializationTree() {
        return initializationTree;
    }

    public void setInitializationTree(Tree tree) {
        initializationTree = tree;
    }

    public Tree generateInitializationTree() {
        if (WrapperTypeSpec.isPointerOrArrayOfPointer(type)
                || initializationTree.opCode() == ILLang.op_assign
                && initializationTree.down(2).opCode() == ILLang.op_call
                && initializationTree.down(2).down(1).opCode() == ILLang.op_ident
                && initializationTree.down(2).down(1).stringValue().toLowerCase().equals("null")) {
            Tree resultTree = ILUtils.build(ILLang.op_pointerAssign,
                    initializationTree.cutChild(1), initializationTree.cutChild(2));
            resultTree.copyAnnotations(initializationTree);
            initializationTree = resultTree;
        }
        if (initializationTree.isAtom()) {
            return ILUtils.build(ILLang.op_assign,
                    ILUtils.build(ILLang.op_ident, symbol),
                    ILUtils.copy(initializationTree));
        } else if (initializationTree.opCode() == ILLang.op_assign ||
                initializationTree.opCode() == ILLang.op_pointerAssign) {
            Tree result = ILUtils.copy(initializationTree);
            result.setChild(ILUtils.build(ILLang.op_ident, symbol), 1);
            return result;
        } else {
            return ILUtils.copy(initializationTree);
        }
    }

    @Override
    public boolean isTarget(WrapperTypeSpec modelTypeSpec) {
        if (type == null || type.wrappedType == null
                || !type.wrappedType.isTarget()) {
            return false;
        }
        if (modelTypeSpec == null || modelTypeSpec.wrappedType == null) {
            return true;
        }
        return modelTypeSpec.canMatchPointerTo(type);
    }

    @Override
    public WrapperTypeSpec type() {
        return type;
    }

    @Override
    public TapList zones() {
        return toZones.tail;
    }

    @Override
    public void setZones(TapList value) {
        toZones.tail = value;
    }

    @Override
    public void accumulateZones(TapList intTree) {
        if (intTree != null) {
            toZones.tail = TapList.cumulWithOper(toZones.tail, intTree, SymbolTableConstants.CUMUL_OR);
        }
    }

    @Override
    public int formalArgRankOrResultZero() {
	return (isReturnVar ? 0 : formalArgRank) ;
    }

    public void adoptZones(VariableDecl model) {
        toZones = model.toZones;
    }

    @Override
    protected void recomputeDependsOn(SymbolTable symbolTable) {
        TapList<SymbolDecl> toDependsOn = new TapList<>(null, dependsOn);
        if (initializationTree != null) {
            addUsedSymbolsInExpr(initializationTree(), toDependsOn, symbolTable, null, false, true);
        }
        if (type != null && type.wrappedType != null) {
            type.wrappedType.addUsedSymbolsInType(toDependsOn, symbolTable);
        }
        dependsOn = toDependsOn.tail;
    }

    /**
     * Collects into toUsedTrees all expressions that are needed for the
     * declaration of this SymbolDecl.
     *
     * @param toUsedTrees TapList of used Tree
     * @param toDejaVu    TapList of TypeSpec
     */
    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (initializationTree != null) {
            toUsedTrees.tail = new TapList<>(initializationTree(), toUsedTrees.tail);
        }
        if (type != null && type.wrappedType != null) {
            type.wrappedType.collectUsedTrees(toUsedTrees, toDejaVu);
        }
    }

    /**
     * Set a variable declaration as a CONSTANT.
     */
    public void setConstant() {
        kind = SymbolTableConstants.CONSTANT;
    }

    public boolean equalsOther(VariableDecl varDecl) {
        boolean result = this.symbol.equals(varDecl.symbol);
        if (result) {
            result = this.kind == varDecl.kind
                    && this.type.equalsCompilDep(varDecl.type);
        }
        return result;
    }

    public void turnTypeIntoPointer() {
        type = new WrapperTypeSpec(new PointerTypeSpec(type, null));
    }

    public Tree buildVarDeclaration(SymbolTable symbolTable) {
        type = type.checkNoneDimensionsOfNewSH(this.symbol, this.symbol,
                ILUtils.build(ILLang.op_ident, this.symbol), symbolTable, null);
        return ILUtils.build(ILLang.op_varDeclaration,
                ILUtils.build(ILLang.op_modifiers),
                type.wrappedType.generateTree(null, null, null, true, null),
                ILUtils.build(ILLang.op_declarators,
                        ILUtils.build(ILLang.op_ident, this.symbol)));
    }

    public boolean cumulActiveField(Tree tree) {
        boolean modified;
        if (this.activityInfo == null) {
            this.activityInfo = new TapList<>(null, null);
        }
        TapList toLeaf = TapList.getSetFieldLocation(this.activityInfo, tree, true);
        if (toLeaf.head instanceof Boolean && (Boolean) toLeaf.head) {
            modified = false;
        } else {
            if (toLeaf.head instanceof TapList) {
                ((TapList) toLeaf.head).setAll(true);
            } else {
                toLeaf.head = Boolean.TRUE;
            }
            modified = true;
        }
        return modified;
    }

    public boolean passesByValue(Unit refUnit) {
        boolean result = false;
        if (refUnit != null) {
            if (refUnit.isFortran()) {
                result = this.hasModifier("value");
            } else if (refUnit.isC()) {
                result = !this.isReference() ;
            }
        }
        return result;
    }

    /** The NewSymbolHolders for the various diff versions of this VariableDecl, one set per diffSort.
     * In general, there is only one per set, but there can be many if using TapEnvForThread.diffReplica. */
    private NewSymbolHolder[][] varDiffSymbolHolderRs = null ;

    /**
     * Puts the NewSymbolHolder "nSH" as the differentiated symbol of
     * this VariableDecl, for the given diffSort and for the given replica number.
     * "diffSort" is here because there may be different coexisting
     * derivative symbols for a given original symbol,
     * for example vd, vb, vb0... for a given variable v.
     * "replica" (unless negative) is the number of the replicated diff variable.
     * "pattern" is not used, it is only for FunctionDecl's.
     */
    @Override
    public void setDiffSymbolHolder(int diffSort, ActivityPattern pattern, int iReplic, NewSymbolHolder nSH) {
        if (TapEnv.diffReplica()>=0) {
            if (varDiffSymbolHolderRs==null) {
                varDiffSymbolHolderRs = new NewSymbolHolder[TapEnv.MAX_DIFF_SORTS][] ;
                for (int j=TapEnv.MAX_DIFF_SORTS-1 ; j>=0 ; --j)
                    varDiffSymbolHolderRs[j] = new NewSymbolHolder[TapEnv.diffReplica()] ;
            }
            varDiffSymbolHolderRs[diffSort][iReplic] = nSH ;
        } else {
            super.setDiffSymbolHolder(diffSort, null, 0, nSH) ;
        }
    }

    /**
     * @return the NewSymbolHolder that has been stored by setDiffSymbolHolder()
     * as the differentiated symbol of this VariableDecl,
     * for the given diffSort and for the given replica number
     */
    @Override
    public NewSymbolHolder getDiffSymbolHolder(int diffSort, ActivityPattern pattern, int iReplic) {
        if (TapEnv.diffReplica()>0) {
            return (varDiffSymbolHolderRs==null ? null : varDiffSymbolHolderRs[diffSort][iReplic]) ;
        } else {
            return super.getDiffSymbolHolder(diffSort, null, 0) ;
        }
    }

    @Override
    public boolean hasDiffSymbolHolders() {
        return varDiffSymbolHolderRs!=null || super.hasDiffSymbolHolders() ;
    }

    /**
     * For every NewSymbolHolder that is stored in this VariableDecl (in provision of
     * the moment when this Symboldecl receives its finalname), replace it with newNSH if it was oldNSH.
     * This is used when oldNSH is absorbed by newNSH.
     */
    @Override
    protected void replaceDiffSymbolHolder(NewSymbolHolder oldNSH, NewSymbolHolder newNSH) {
        if (varDiffSymbolHolderRs!=null) {
            for (int i=varDiffSymbolHolderRs.length-1 ; i>=0 ; --i) {
                for (int iReplic=TapEnv.diffReplica()-1 ; iReplic>=0 ; --iReplic) {
                    if (varDiffSymbolHolderRs[i][iReplic]==oldNSH)
                        varDiffSymbolHolderRs[i][iReplic] = newNSH ;
                }
            }
        }
        super.replaceDiffSymbolHolder(oldNSH, newNSH) ;
    }

    @Override
    protected void eraseDiffSymbolHolder(int diffSort) {
        if (varDiffSymbolHolderRs!=null) {
            // not sure we want to do something here?...
        }
        super.eraseDiffSymbolHolder(diffSort);
    }

    @Override
    public String toString() {
        return symbol  //+" @"+Integer.toHexString(hashCode())
            + ": " + (type==null ? "null_type" : type)
            + (kind() == SymbolTableConstants.CONSTANT
               ? " constant (value:"+constantValue+")."
               : (kind() == SymbolTableConstants.FUNCNAME
                  ? " return variable."
                  : " variable."))
            + " zones:" + zones()
            + (importedFrom==null ? "" : " (=>" + importedFrom.first+" from @"+Integer.toHexString(importedFrom.second.hashCode()) + ")")
            + (!isReference() ? "" : " reference")
            + (!isActiveSymbolDecl() ? "" : " active")  //+" toActive@"+Integer.toHexString(toIsActive.hashCode())
            + (initializationTree==null ? "" : " Init:"+initializationTree)
            + (extraInfo() == null ? "" : " X"+extraInfo())
            + (accessInfo == null ? "" : " A"+accessInfo)
            //+" ArgRkS:"+formalArgRank+":"+formalArgRankInOrigUnit
            //+" depends on:"+ dependsOn()
            //+ " "+hasxx()
            ;

    }

}
