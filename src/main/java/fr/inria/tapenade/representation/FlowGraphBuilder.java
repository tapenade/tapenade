/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.frontend.TreeProtocol;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Operator;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToInt;
import fr.inria.tapenade.utils.Tree;

import java.io.IOException;

/**
 * Machinery to build of a FlowGraph and its SymbolTable's for a Unit
 * that will be read though a tree protocol.
 */
public final class FlowGraphBuilder {

    /**
     * Only for temporary C++ debug. Remove later.
     */
    private static int fgbDepth = -1;

    /**
     * Current built unit.
     */
    private Unit unit;
    /**
     * The FlowGraph's entry block.
     */
    private EntryBlock entryBlock;
    /**
     * The FlowGraph's exit block.
     */
    private ExitBlock exitBlock;
    /**
     * The list of all Blocks already built for the Flow Graph currently being built.
     * This list will be used by later propagations, e.g. to detect dead Blocks that are
     * not accessible from the entry block, and to build the final set of Blocks.
     */
    private final TapList<Block> allBlocks = new TapList<>(null, null);
    /**
     * Stack of the nested controls around the current place. Used to manage the
     * break's, exit's, and cycle's.
     */
    private TapList<FlowGraphBuilder.ControlJumpInfo> nestedControlStack;
    /**
     * Association list between all labels and the place in the FlowGraph that
     * they are designating. Includes a marker to detect double declaration of
     * such a label.
     */
    private final LabelHeap labelHeap = new LabelHeap();
    /**
     * Where the arriving Tree comes from...
     */
    private TreeProtocol inputTreeStream;
    /**
     * The CallGraph that will receive the Unit built.
     */
    private CallGraph callGraph;
    /**
     * Holds the SymbolTable for the place of the FlowGraph that is currently being built.
     */
    private SymbolTable curSymbolTable;
    /**
     * Holds the parallelControls that control the place of the FlowGraph that is currently being built.
     */
    private TapList<Instruction> curParallelControls;
    /**
     * Holds the list of all names of variable GOTO labels (also called "assigned goto"'s).
     */
    private TapList<String> labelVars;
    /**
     * Syntactic Controller Instruction, around the place of the FlowGraph that
     * is currently being built.
     */
    private Instruction curSCInstruction;
    /**
     * List of pending comments. They will be pre-attached to the next Instruction built.
     */
    private final TapPair<TapList<Tree>, TapList<Tree>> pendingComments2 = new TapPair<>(null, null);
    private Tree labelAbovePostComments;
    /**
     * Holds the list of all names of variable FORMAT labels.
     */
    private TapList<String> formatLabels;
    private InstructionMask whereMask;
    /**
     * The original language of the arriving trees.
     */
    private int language;
    private boolean traceOn;

    /**
     * Include mixing declarations and statements are inlined.
     */
    private boolean includeContainsOneDeclaration;

    /**
     * Given an inputTreeStream, which is ready to output a piece of code, as a well-formed
     * IL tree starting with op_function, op_program, op_constructor, op_class, op_module or op_nameSpace,
     * fill the given empty unit with the internal representation of the incoming piece of code.
     *
     * @param inputTreeStream    The input stream, which is ready to output the piece of code
     * @param unit               The unit that will contain the internal representation of the piece code
     * @param contextSymbolTable The symbol table of the level that will contain the piece of code
     */
    public static Unit build(TreeProtocol inputTreeStream, SymbolTable contextSymbolTable,
                             CallGraph callGraph, int language, Unit unit, boolean fromLib) throws IOException {
        TapEnv.pushRelatedUnit(unit) ;
        FlowGraphBuilder thisBuilder = new FlowGraphBuilder();
        thisBuilder.callGraph = callGraph;
        thisBuilder.traceOn = false;
        thisBuilder.unit = unit;
        thisBuilder.language = language;
        thisBuilder.inputTreeStream = inputTreeStream;
        try {
            thisBuilder.buildUnit(contextSymbolTable, fromLib);
        } catch (IOException exception) {
            // cleanup, and repropagate till the "file" level of nested Units.
            TapEnv.systemError(exception.getMessage() + " in " + unit.shortName());
            TapEnv.resetIncludeManager();
            if (unit.isTranslationUnit()) {
                // In case of recovery, the last FGArrow's to the exit may be missing:
                TapList<Block> danglingBlocks = thisBuilder.allBlocks.tail;
                if (danglingBlocks == null) {
                    new FGArrow(thisBuilder.entryBlock, FGConstants.ENTRY, FGConstants.MAIN, thisBuilder.exitBlock);
                } else {
                    while (danglingBlocks != null) {
                        if (danglingBlocks.head.flow() == null) {
                            new FGArrow(danglingBlocks.head, FGConstants.NO_TEST, 0, thisBuilder.exitBlock);
                        }
                        danglingBlocks = danglingBlocks.tail;
                    }
                }
                unit.makeFlowGraph(thisBuilder.entryBlock, thisBuilder.allBlocks.tail, thisBuilder.exitBlock, false);
            } else {
                throw exception;
            }
        }
        TapEnv.popRelatedUnit() ;
        return thisBuilder.unit;
    }

    /**
     * @return the SymbolTable (at or around) enclosingSymbolTable, that will host the SymbolDecl of
     * a symbol, given the privacy and modifiers of this symbol.
     */
    public static SymbolTable findHostingSymbolTable(SymbolTable enclosingSymbolTable,
                                                     String privacy, Tree modifiers) {
        if (privacy != null && privacy.contains("protected")) {
            enclosingSymbolTable = enclosingSymbolTable.basisSymbolTable();
        } else if (privacy != null && privacy.contains("public")) {
            enclosingSymbolTable = enclosingSymbolTable.basisSymbolTable().basisSymbolTable();
        } else if (enclosingSymbolTable.isTranslationUnitSymbolTable()
                && ILUtils.contains(modifiers, ILLang.op_ident, "private") == null) {
            enclosingSymbolTable = enclosingSymbolTable.basisSymbolTable();
        }
        return enclosingSymbolTable;
    }

    // TODO: CLARIFY: always call with instruction!=null and tree==null (=> store op_format's in Instruction's as well)
    // then simplify!
    // TODO: CLARIFY: unify comments and commentsBlock => remove the 2's in preComments2, postComments2, or pendingComments2
    // TODO: CLARIFY: flatten pending comments and other things into one op_comments containing an ordered collection of
    //  op_stringCst, op_blockComments, op_pppLine, or maybe other (directives?)

    private static void attachComments(Instruction instruction, Tree tree,
                                      TapPair<Tree, Tree> postComments2,
                                      TapPair<TapList<Tree>, TapList<Tree>> pendingComments2) {
        if (instruction == null && tree == null) {
            //If there's nowhere to attach the comments, append the "post" to the tail of the "pending", and keep them pending:
            if (postComments2 != null && postComments2.first != null) {
                pendingComments2.first = TapList.append(pendingComments2.first, postComments2.first.childrenList());
                postComments2.first = null;
            }
            if (postComments2 != null && postComments2.second != null) {
                pendingComments2.second = TapList.append(pendingComments2.second, postComments2.second.childrenList());
                postComments2.second = null;
            }
        } else {
            if (pendingComments2.first != null) {
                Tree preComments =
                        ILUtils.build(ILLang.op_comments, ILUtils.collectCommentStrings(pendingComments2.first));
                if (instruction != null) {
                    instruction.preComments = preComments;
                } else {
                    tree.setAnnotation("preComments", preComments);
                }
                pendingComments2.first = null;
            }
            if (pendingComments2.second != null) {
                Tree preCommentsBlock =
                        ILUtils.build(ILLang.op_commentsBlock, ILUtils.collectCommentStrings(pendingComments2.second));
                if (instruction != null) {
                    instruction.preCommentsBlock = preCommentsBlock;
                } else {
                    tree.setAnnotation("preCommentsBlock", preCommentsBlock);
                }
                pendingComments2.second = null;
            }
            if (postComments2 != null && postComments2.first != null) {
                Tree postComments =
                        ILUtils.build(ILLang.op_comments, ILUtils.collectCommentStrings(postComments2.first));
                if (instruction != null) {
                    instruction.postComments = postComments;
                } else {
                    tree.setAnnotation("postComments", postComments);
                }
                postComments2.first = null;
            }
            if (postComments2 != null && postComments2.second != null) {
                Tree postCommentsBlock =
                        ILUtils.build(ILLang.op_commentsBlock, ILUtils.collectCommentStrings(postComments2.second));
                if (instruction != null) {
                    instruction.postCommentsBlock = postCommentsBlock;
                } else {
                    tree.setAnnotation("postCommentsBlock", postCommentsBlock);
                }
                postComments2.second = null;
            }
        }
    }

    private void buildUnit(SymbolTable contextSymbolTable, boolean fromLib) throws IOException {
        SymbolTable unitSymbolTable = new SymbolTable(contextSymbolTable);
        unitSymbolTable.setLanguage(language);
        unitSymbolTable.isPublic = true ;
        unitSymbolTable.unit = unit;
        unitSymbolTable.setCaseDependent(!TapEnv.isFortran(language));
        SymbolTable bodySymbolTable = unitSymbolTable;
        this.curSymbolTable = unitSymbolTable;
        unit.addDerivedSymbolTable(unitSymbolTable);
        unit.setPublicSymbolTable(unitSymbolTable);
        if (unit.isFortran()) {
            unit.fortranStuff().refSymbolTable = unitSymbolTable ;
        }
        entryBlock = new EntryBlock(unitSymbolTable);
        exitBlock = new ExitBlock(unitSymbolTable);
        TapEnv.pushRelatedUnit(unit);
        unit.setEntryBlock(entryBlock);
        unit.setExitBlock(exitBlock);
        unit.position = inputTreeStream.position() + 1;
        boolean keepUnit = true;
        Block centralBlock = null;
        WrapperTypeSpec returnTypeSpec = new WrapperTypeSpec(new VoidTypeSpec());
        boolean variableArgList = false;
        int i;

        int unitCode = inputTreeStream.seeOperator().code;
        switch (unitCode) {
            case ILLang.op_file:
            case ILLang.op_declarations:
            case ILLang.op_blockStatement: { // These ops mean we start reading a file (i.e. a "Translation Unit"):
                // Do NOT pop the next operator when this is a file, because this next blockStatement/declarations
                // will be popped and read during the next treeStream2FlowGraph().
                traceOn = traceOn(unit.name());
                unitSymbolTable.setIsTranslationUnit();
                unitSymbolTable.setShortName("SymbolTable of Translation Unit (i.e. file) " + unit.name());
                unit.setTranslationUnitSymbolTable(unitSymbolTable);
                unitSymbolTable.containerFileName = TapEnv.stripLanguageExtension(unit.name());
                if (!unit.name().startsWith("iso_c_binding")) {
                    callGraph.addTranslationUnitSymbolTable(unitSymbolTable);
                }
                TapEnv.setCurrentTranslationUnitSymbolTable(unitSymbolTable);
                break;
            }
            case ILLang.op_module: { // A module definition is arriving:
                // Eat up this module operator:
                inputTreeStream.readOperator();
                unit.setModule();
                Tree moduleNameTree = inputTreeStream.readTree();
                boolean isBlockData = moduleNameTree.opCode() == ILLang.op_none ||
                        moduleNameTree.opCode() == ILLang.op_ident &&
                                ILUtils.getIdentString(moduleNameTree).toLowerCase().startsWith("_blockdata_");
                if (TapEnv.traceInputIL()) {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("--> [" + fgbDepth + "] module " + moduleNameTree);
                }
                if (moduleNameTree.opCode() == ILLang.op_none) {
                    unit.setName("");
                    moduleNameTree = ILUtils.build(ILLang.op_ident, "");
                    unitSymbolTable.setShortName("SymbolTable of block data");
                    TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth() + "] Building block data");
                } else {
                    Unit oldUnit = null;
                    TapList<FunctionDecl> modDecls =
                            unitSymbolTable.getFunctionDecl(ILUtils.getIdentString(moduleNameTree), null, null, false);
                    // We're actually looking for an (undefined) module. Skip until we find one:
                    while (modDecls != null &&
                            !(modDecls.head.unit() != null
                                    && (modDecls.head.unit().isUndefined() || modDecls.head.unit().isModule()))) {
                        modDecls = modDecls.tail;
                    }
                    FunctionDecl modDecl = modDecls == null ? null : modDecls.head;
                    if (modDecl != null) {
                        oldUnit = modDecl.unit();
                    }
                    if (oldUnit == null) {
                        oldUnit = callGraph.getUnit(ILUtils.getIdentString(moduleNameTree));
                    }
                    if (oldUnit != null && oldUnit.isUndefined()) {
                        unit.absorb(oldUnit);
                        callGraph.deleteUnit(oldUnit);
                    }
                    unit.setName(ILUtils.getIdentString(moduleNameTree));
                    SymbolTable unitImportsSymbolTable = new SymbolTable(contextSymbolTable);
                    unitImportsSymbolTable.setLanguage(language);
                    unitImportsSymbolTable.unit = unit;
                    unitImportsSymbolTable.setCaseDependent(!TapEnv.isFortran(language));
                    unitImportsSymbolTable.setShortName("Imports SymbolTable of module " + unit.name());
		    unitImportsSymbolTable.isImports = true ;
//                     unitImportsSymbolTable.initWaitingZoneInfos() ;
                    unit.setImportsSymbolTable(unitImportsSymbolTable);
                    unitSymbolTable.setBasisSymbolTable(unitImportsSymbolTable) ;
                    unitSymbolTable.setShortName("Public SymbolTable of module " + unit.name());
                    TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth()
                            + "] Building module: " + unit.name() + " @" + Integer.toHexString(unit.hashCode())
                            + " provisional rank:" + unit.rank());
                }
                traceOn = traceOn(unit.name());
                if (!isBlockData) {
                    TapEnv.warningFortran77WithFortran90Features(unit);
                }
                TapList<FunctionDecl> prevDecls;
                FunctionDecl prevDecl = null;
                // hostingSymbolTable is the SymbolTable that must contain this module's FunctionDecl:
                // Modules exist only in Fortran, where files are not packages => this is the fortranRootSymbolTable.
                SymbolTable hostingSymbolTable = callGraph.fortranRootSymbolTable();
                if (!unit.name().isEmpty()) {
                    prevDecls = hostingSymbolTable.getFunctionDecl(
                            ILUtils.getIdentString(moduleNameTree), null, null, false);
                    prevDecl = prevDecls == null ? null : prevDecls.head;
                }
                if (prevDecl != null && prevDecl.unit() != null) {
                    if (prevDecl.unit().isUndefined()) {
                        hostingSymbolTable.removeDecl(prevDecl.symbol, SymbolTableConstants.FUNCTION, false);
                        prevDecl = null;
                    } else if (!prevDecl.isIntrinsic()) {
                        TapEnv.fileWarning(TapEnv.MSG_SEVERE, moduleNameTree, "(DD16) Double declaration of " + prevDecl.unit().name() + " (ignored new)");
                        keepUnit = false;
                    } else {
                        prevDecl = null;
                    }
                }
                if (prevDecl == null) {
                    FunctionDecl tmpFunctionDecl = new FunctionDecl(moduleNameTree, unit, hostingSymbolTable);
                    hostingSymbolTable.addSymbolDecl(tmpFunctionDecl);
                    keepUnit = true;
                }
                // Build this Module's private SymbolTable:
                SymbolTable privST = unit.privateSymbolTable();
                if (privST == null) {
                    privST = new SymbolTable(unit.publicSymbolTable());
                    privST.unit = unit;
                    privST.setShortName("Private SymbolTable of module " + unit.name());
                    unit.addDerivedSymbolTable(privST);
                    unit.setPrivateSymbolTable(privST);
                }
                // Build this Module's unique central Block (Block 0):
                centralBlock = new BasicBlock(privST, null, allBlocks);
                unit.publicSymbolTable().declarationsBlock = centralBlock;
                privST.declarationsBlock = centralBlock;
                bodySymbolTable = privST;
                break;
            }
            case ILLang.op_class: {
                // A class definition is arriving
                // Eat up this class operator:
                inputTreeStream.readOperator();
                unit.setClass();
                unit.modifiers = inputTreeStream.readTree();
                ToInt toLineNo = new ToInt(-1) ;
                Tree classNameTree = inputTreeStream.readTree(toLineNo);
                Tree peeledClassNameTree = classNameTree;
                if (peeledClassNameTree.opCode() == ILLang.op_scopeAccess) {
                    peeledClassNameTree = peeledClassNameTree.down(2);
                }
                String className = ILUtils.getIdentString(peeledClassNameTree);
                if (TapEnv.traceInputIL()) {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("--> [" + fgbDepth + "] class " + className);
                }
                unit.setName(className);
                traceOn = traceOn(className);
                ClassDecl existingClassDecl = contextSymbolTable.getClassDecl(className);
                Unit existingClassUnit = existingClassDecl == null ? null : existingClassDecl.unit;
                // if class was already "seen" before it's defined, we must merge with the temporary Unit created then:
                if (existingClassUnit != null) {
                    unit.absorbAndDeleteUnit(existingClassUnit);
                    unitSymbolTable.removeDecl(className, SymbolTableConstants.CLASS, false);
                    TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth()
                            + "] Continuing class: " + unit.name()
                            + " @" + Integer.toHexString(existingClassUnit.hashCode())
                            + "==>@" + Integer.toHexString(unit.hashCode()) + " new provisional rank:" + unit.rank());
                } else {
                    TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth()
                            + "] Building class: " + unit.name() + " @" + Integer.toHexString(unit.hashCode())
                            + " provisional rank:" + unit.rank());
                }
                unitSymbolTable.setShortName("Public SymbolTable of class " + unit.name());
                contextSymbolTable.addNewSymbolDecl(new ClassDecl(className, unit));
                VariableDecl thisDecl = new VariableDecl(ILUtils.build(ILLang.op_ident, "this"),
                        new WrapperTypeSpec(new PointerTypeSpec(
                                new WrapperTypeSpec(unit.classTypeSpec()), null)));
                unitSymbolTable.addSymbolDecl(thisDecl);
                Tree parentClassesTree = inputTreeStream.readTree();
                if (!ILUtils.isNullOrNoneOrEmptyList(parentClassesTree)) {
                    Tree[] parentClasses = parentClassesTree.children();
                    for (int k = parentClasses.length - 1; k >= 0; --k) {
                        //TODO: getIdentString must be replaced to work with namespaces:
                        String parentClassName = ILUtils.getIdentString(parentClasses[k].down(2));
                        if (parentClassName == null) {
                            TapEnv.printlnOnTrace("Failed to lookup class name " + parentClasses[k].down(2));
                        } else {
                            ClassDecl parentClassDecl = parentClassName == null ? null : unitSymbolTable.getClassDecl(parentClassName);
                            Unit parentClass = parentClassDecl == null ? null : parentClassDecl.unit;
                            if (parentClass == null) {
                                parentClass = callGraph.createNewUnit(unit.upperLevelUnit(), language);
                                parentClass.setExternalSymbolTable(callGraph.languageRootSymbolTable(language));
                                TapEnv.toolWarning(-1, " NOTE: parent class " + parentClass + " created as undefined");
                                parentClass.setUndefined();
                                parentClass.setName(parentClassName);
                                ClassDecl globalClassDecl = new ClassDecl(parentClassName, parentClass);
                                callGraph.languageRootSymbolTable(language).addSymbolDecl(globalClassDecl);
                            }
                            CallGraph.addCallArrow(unit, SymbolTableConstants.IMPORTS, parentClass);
                            Tree[] modifiersImport = parentClasses[k].down(1).children();
                            boolean isVirtual = false;
                            boolean isPublic = false;
                            for (int j = modifiersImport.length - 1; j >= 0; --j) {
                                isPublic = ILUtils.getIdentString(modifiersImport[j]).contains("public");
                                isVirtual = ILUtils.getIdentString(modifiersImport[j]).contains("virtual");
                            }
                            unit.addParentClass(parentClass, isVirtual, isPublic);
                            parentClass.addChildrenClass(unit);
                        }
                    }
                }
                // Build this Class's protected SymbolTable:
                SymbolTable protST = unit.protectedSymbolTable();
                if (protST == null) {
                    protST = new SymbolTable(unit.publicSymbolTable());
                    protST.unit = unit;
                    protST.setShortName("Protected SymbolTable of class " + unit.name());
                    unit.addDerivedSymbolTable(protST);
                    unit.setProtectedSymbolTable(protST);
                }
                // Build this Class's private SymbolTable:
                SymbolTable privST = unit.privateSymbolTable();
                if (privST == null) {
                    privST = new SymbolTable(unit.protectedSymbolTable());
                    privST.unit = unit;
                    privST.setShortName("Private SymbolTable of class " + unit.name());
                    unit.addDerivedSymbolTable(privST);
                    unit.setPrivateSymbolTable(privST);
                }
                // Add the header instruction into this Class's entryBlock:
                Tree classHeaderTree = ILUtils.build(ILLang.op_class,
                        unit.modifiers,
                        classNameTree,
                        parentClassesTree,
                        null);
                entryBlock.addInstrTl(new Instruction(classHeaderTree, null, toLineNo.get()));
                // Build this Class's unique central Block (Block 0):
                centralBlock = new BasicBlock(privST, null, allBlocks);
                unit.publicSymbolTable().declarationsBlock = centralBlock;
                protST.declarationsBlock = centralBlock;
                privST.declarationsBlock = centralBlock;
                bodySymbolTable = privST;
                keepUnit = true;
                break;
            }
            case ILLang.op_constructor:
            case ILLang.op_destructor: {
                boolean isConstructor = unitCode == ILLang.op_constructor;
                // Eat up this destructor/constructor operator:
                inputTreeStream.readOperator();
                if (isConstructor) {
                    unit.setConstructor();
                } else {
                    unit.setDestructor();
                }
                unit.modifiers = inputTreeStream.readTree();
                ToInt toLineNo = new ToInt(-1) ;
                Tree xxstructorNameTree = inputTreeStream.readTree(toLineNo);
                Tree xxstructorNameIdentTree = xxstructorNameTree;
                Tree scopeTree = null;
                if (xxstructorNameTree.opCode() == ILLang.op_scopeAccess) {
                    scopeTree = xxstructorNameTree.down(1);
                    xxstructorNameIdentTree = xxstructorNameTree.down(2);
                }
                String xxstructorNameString = ILUtils.getIdentString(xxstructorNameIdentTree);
                String classNameString = isConstructor ? xxstructorNameString : xxstructorNameString.substring(1);// remove ~
                Unit classUnit = unit.upperLevelUnit();
                // If cons/destructor definition is "normally" inside its class, we prefer to use the name of this class
                // rather than the classname given as 2nd son of the cons/destructor, because the latter may be not specialized.
                if (classUnit != null && classUnit.isClass()
                        && ILUtils.stripSpecialization(classUnit.name()).equals(classNameString)) {
                    classNameString = classUnit.name();
                    xxstructorNameString = isConstructor ? classNameString : "~" + classNameString;
                    xxstructorNameIdentTree.setValue(xxstructorNameString);
                } else {
                    // else we must look for the class that this cons/destructor belongs to,
                    // and re-attach this cons/destructor's Unit into this class.
                    ClassDecl classDecl = scopeTree == null ? null : contextSymbolTable.getClassDecl(scopeTree);
                    if (classDecl != null) {
                        classDecl = classDecl.unit.privateSymbolTable().getClassDecl(classNameString);
                    } else {
                        classDecl = contextSymbolTable.getClassDecl(classNameString);
                    }
                    if (classDecl == null) {
                        TapEnv.toolWarning(-1, "Parent class of " + (isConstructor ? "constructor " : "destructor ") + xxstructorNameTree + " not found: marked as undefined");
                        unit.setUndefined();
                        classUnit = null;
                    } else {
                        classUnit = classDecl.unit;
                        callGraph.detachFromUpperUnit(unit);
                        callGraph.attachToUpperUnit(unit, classUnit);
                    }
                }
                if (TapEnv.traceInputIL()) {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("--> [" + fgbDepth + "] "
                            + (isConstructor ? "constructor " : "destructor ")
                            + xxstructorNameString);
                }
                unit.setName(xxstructorNameString);
                traceOn = traceOn(xxstructorNameString);
                TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth() + "] Building " + (isConstructor ? "constructor" : "destructor") + " of class: " + classNameString + " @" + Integer.toHexString(unit.hashCode()) + " provisional rank:" + unit.rank());
                unitSymbolTable.declareFormalParamsLevel();
                unitSymbolTable.setShortName("Formal params SymbolTable of " + (isConstructor ? "constructor" : "destructor") + " of class " + unit.name());
                returnTypeSpec = new WrapperTypeSpec(new VoidTypeSpec());
                int position = inputTreeStream.position() + 1;
                Tree callTree = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, xxstructorNameString),
                        ILUtils.build(ILLang.op_expressions));
                ILUtils.setPosition(callTree, position);
                WrapperTypeSpec[] argumentsTypeSpecs;
                if (isConstructor) {
                    // Here come the constructor's params
                    Tree paramsTree = inputTreeStream.readTree();
                    Tree[] paramsTrees = paramsTree.children();
                    TapList<WrapperTypeSpec> paramTypeSpecList = null;
                    int nbArg = 1;
                    for (i = 0; i < paramsTrees.length; i++) {
                        TapList<WrapperTypeSpec> varTypeSpecs =
                                unitSymbolTable.addVarDeclaration(paramsTrees[i], null, true, null);
                        // assigns the correct parameter rank:
                        Tree[] paramIdents = paramsTrees[i].down(3).children();
                        for (Tree paramIdent : paramIdents) {
                            if (paramIdent.opCode() != ILLang.op_star) {
                                String varName;
                                if (paramIdent.opCode() == ILLang.op_ident
                                        || paramIdent.opCode() == ILLang.op_metavar) {
                                    varName = ILUtils.getIdentString(paramIdent);
                                } else {
                                    varName = ILUtils.baseName(paramIdent);
                                }
                                VariableDecl varDecl = unitSymbolTable.getVariableDecl(varName);
                                if (varDecl != null) {
                                    varDecl.formalArgRank = nbArg;
                                }
                            }
                            ILUtils.getArguments(callTree).addChild(ILUtils.copy(paramIdent), nbArg);
                            nbArg++;
                        }
                        paramTypeSpecList = TapList.append(paramTypeSpecList, varTypeSpecs);
                    }
                    argumentsTypeSpecs = new WrapperTypeSpec[TapList.length(paramTypeSpecList)];
                    i = 0;
                    while (paramTypeSpecList != null) {
                        argumentsTypeSpecs[i] = paramTypeSpecList.head;
                        paramTypeSpecList = paramTypeSpecList.tail;
                        i++;
                    }
                } else {
                    argumentsTypeSpecs = new WrapperTypeSpec[0];
                }
                unit.setFunctionTypeSpec(new FunctionTypeSpec(returnTypeSpec, argumentsTypeSpecs, false));

                if (classUnit != null) {
                    TapList<FunctionDecl> sameFuncs =
                            classUnit.privateSymbolTable().getTopFunctionDecl(xxstructorNameString, returnTypeSpec,
                                    argumentsTypeSpecs, true);
                    if (sameFuncs == null) {
                        sameFuncs =
                                classUnit.protectedSymbolTable().getTopFunctionDecl(xxstructorNameString, returnTypeSpec,
                                        argumentsTypeSpecs, true);
                    }
                    if (sameFuncs == null) {
                        sameFuncs =
                                classUnit.publicSymbolTable().getTopFunctionDecl(xxstructorNameString, returnTypeSpec,
                                        argumentsTypeSpecs, true);
                    }
                    if (sameFuncs != null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, xxstructorNameTree, "A declaration of " + (isConstructor ? "constructor" : "destructor") + " in class " + classNameString + " already exists.");
                    }
                }

                // Add the header instruction into the entryBlock:
                entryBlock.addInstrTl(new Instruction(callTree, curSCInstruction, toLineNo.get()));

                // Create the functionDecl and add it into the symbolTable
                SymbolTable hostingSymbolTable =
                        findHostingSymbolTable(contextSymbolTable, null, unit.modifiers);
                FunctionDecl tmpFunctionDecl =
                        new FunctionDecl(xxstructorNameIdentTree, unit, contextSymbolTable, classNameString);
                if (!unit.isTopInFile() && !unit.upperLevelUnit().privacy.isEmpty() /*&& methodDecl == null*/) {
                    tmpFunctionDecl.addExtraInfo(new TapList<>(unit.upperLevelUnit().privacy, null));
                }
                hostingSymbolTable.addSymbolDecl(tmpFunctionDecl);
                bodySymbolTable = new SymbolTable(unitSymbolTable);
                bodySymbolTable.setShortName("Local variables SymbolTable of " + (isConstructor ? "constructor " : "destructor ") + " of class " + unit.name());
                bodySymbolTable.unit = unit;
                unit.addDerivedSymbolTable(bodySymbolTable);
                unit.setPrivateSymbolTable(bodySymbolTable);
                break;
            }
            case ILLang.op_function:
            case ILLang.op_program: {
                // Eat up this op_function or op_program operator:
                inputTreeStream.readOperator();
//             unit.setStandard() ;
                // Function modifiers :
                Tree modifiers = inputTreeStream.readTree();
                unit.modifiers = modifiers;
                Tree returnTypeTree;
                Tree classNameTree = null;
                String classNameString = null;

                if (unitCode == ILLang.op_function) {
                    // Dans la declaration d'une methode/fonction, on a:
                    // * des modifiers (public,  private, protected)
                    // * son type de retour (final int... par ex)
                    // * optionnellement le nom de sa classe (si la methode n'est pas statique, elle l'a)
                    // * un nom
                    // * des args
                    // * un body (qui peut etre vide)
                    ToBool isPointer = new ToBool(false);
                    returnTypeTree = inputTreeStream.readTree();
                    returnTypeSpec =
                            TypeSpec.build(returnTypeTree, unitSymbolTable, new Instruction(returnTypeTree),
                                    new TapList<>(null, null), new TapList<>(null, null),
                                    new TapList<>(null, null), isPointer, null);
                    if (isPointer.get()) {
                        returnTypeSpec = new WrapperTypeSpec(new PointerTypeSpec(returnTypeSpec, null));
                    }
                    classNameTree = inputTreeStream.readTree();
                }

                // Next comes the function or method name:
                ToInt toLineNo = new ToInt(-1) ;
                Tree funcNameTree = inputTreeStream.readTree(toLineNo);
                String funcNameString = ILUtils.getIdentString(funcNameTree);
                if (funcNameTree.opCode() == ILLang.op_none) {
                    funcNameString = "_main_";
                    funcNameTree = ILUtils.build(ILLang.op_ident, funcNameString);
                }
                unit.setName(funcNameString);
                if (TapEnv.traceInputIL()) {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("--> [" + fgbDepth + "] function "
                            + funcNameString);
                }
                traceOn = traceOn(unit.name());
                if (TapEnv.isFortran(language)) {
                    // In present implementation, only Fortran units have an importsSymbolTable:
                    SymbolTable unitImportsSymbolTable = new SymbolTable(contextSymbolTable);
                    unitImportsSymbolTable.setLanguage(language);
                    unitImportsSymbolTable.unit = unit;
                    unitImportsSymbolTable.setCaseDependent(!TapEnv.isFortran(language));
                    unitImportsSymbolTable.setShortName("Imports SymbolTable of procedure " + unit.name());
                    unitImportsSymbolTable.isImports = true ;
                    unit.setImportsSymbolTable(unitImportsSymbolTable);
                    unitSymbolTable.setBasisSymbolTable(unitImportsSymbolTable) ;
                }
                TapEnv.printlnOnTrace(15, "@@ [" + unit.nestingDepth() + "] Building " + (unit.isInterface() ? "interface" : "flow-graph for unit") + ": " + unit.name() + " @" + Integer.toHexString(unit.hashCode()) + " provisional rank:" + unit.rank());
                VariableDecl returnVarDecl;
                VariableDecl otherReturnVarDecl = null;
                unitSymbolTable.declareFormalParamsLevel();
                unitSymbolTable.setShortName("Formal params SymbolTable of procedure " + unit.name());
                Unit standardUnit = unit;
                if (returnTypeSpec.wrappedType == null
                        && TapEnv.isC(language)
                        && unit.name().equals("main")) {
                    returnTypeSpec.wrappedType = new VoidTypeSpec();
                }

                // [llh 2017/08/01] For no good reason, our current Fortran (parser+f2il)
                // makes an ugly mess out of "bind" and "explicitReturnVar" info.
                // Sometimes the bind tree (operator accessDecl) is placed into
                // annotation "explicitReturnVar". TODO: This must be fixed !
                // Meanwhile we sort things out here:
                Tree annotExplicitReturn = funcNameTree.getAnnotation("explicitReturnVar");
                if (annotExplicitReturn != null && annotExplicitReturn.opCode() == ILLang.op_accessDecl) {
                    // This should have been a "bind" !
                    funcNameTree.removeAnnotation("explicitReturnVar");
                    funcNameTree.setAnnotation("bind", annotExplicitReturn);
                }

                Tree annot = funcNameTree.getAnnotation("bind");
                if (annot != null && annot.opCode() == ILLang.op_accessDecl) {
                    unit.modifiers = annot;    //[llh] probably wrong: looses previous modifiers!
                    unit.setLanguageAndUp(TapEnv.FORTRAN2003);
                }

                if (!TypeSpec.isA(returnTypeSpec, SymbolTableConstants.VOIDTYPE)) {
                    // If function has a return type:
                    annotExplicitReturn = funcNameTree.getAnnotation("explicitReturnVar");
                    if (annotExplicitReturn != null
                            && annotExplicitReturn.opCode() == ILLang.op_ident) {
                        otherReturnVarDecl =
                                new VariableDecl(annotExplicitReturn, returnTypeSpec);
                        unitSymbolTable.addSymbolDecl(otherReturnVarDecl);
                        otherReturnVarDecl.setNoneInstruction();
                    } else {
                        // !!! comportement different pour fortran et C/C++
                        if (TapEnv.isC(language) && !fromLib) {
                            NewSymbolHolder otherFuncNameSH = new NewSymbolHolder(funcNameString);
                            otherFuncNameSH.setAsVariable(returnTypeSpec, null);
                            otherFuncNameSH.makeNewRef(unit.publicSymbolTable());
                            otherFuncNameSH.solvingLevelMustInclude(unit.privateSymbolTable());
                            otherFuncNameSH.declarationLevelMustInclude(unit.publicSymbolTable());
                            otherReturnVarDecl = otherFuncNameSH.newVariableDecl();
                            otherReturnVarDecl.setNoneInstruction();
                        } else {
                            returnVarDecl =
                                    new VariableDecl(funcNameTree, returnTypeSpec);
                            returnVarDecl.setReturnVar();
                            unitSymbolTable.addSymbolDecl(returnVarDecl);
                            returnVarDecl.setNoneInstruction();
                        }
                    }
                }
                // Then come formal parameters: insert them into the symbolTable (scope (b))
                int position = inputTreeStream.position() + 1;
                Tree paramsTree = inputTreeStream.readTree();
                Tree[] paramsTrees = paramsTree.children();
                TapList<WrapperTypeSpec> paramTypeSpecList = null;
                Tree callTree = ILUtils.buildCall(
                        ILUtils.copy(funcNameTree),
                        ILUtils.build(ILLang.op_expressions));
                ILUtils.setPosition(callTree, position);
                int nbArg = 1;
                TapList<WrapperTypeSpec> varTypeSpecs;
                for (i = 0; i < paramsTrees.length; i++) {
                    if (paramsTrees[i].opCode() == ILLang.op_variableArgList) {
                        TapEnv.toolWarning(-1, "(DD99) Warning: feature of C not supported: variable argument list");
                        variableArgList = true;
                    } else if (!ILUtils.isNullOrNone(paramsTrees[i])
                            && paramsTrees[i].opCode() == ILLang.op_varDeclaration) {
                        varTypeSpecs =
                                unitSymbolTable.addVarDeclaration(paramsTrees[i], null, true, null);
                        // assigns the correct parameter rank:
                        Tree[] paramIdents = paramsTrees[i].down(3).children();
                        // special case of a varDeclaration that specifies only the type -> implicitly 1 variable:
                        if (paramIdents.length == 0) {
                            paramIdents = new Tree[]{ILUtils.build(ILLang.op_ident, "_")};
                        }
                        for (Tree paramIdent : paramIdents) {
                            if (paramIdent.opCode() != ILLang.op_star) {
                                String varName;
                                if (paramIdent.opCode() == ILLang.op_ident
                                        || paramIdent.opCode() == ILLang.op_metavar) {
                                    varName = ILUtils.getIdentString(paramIdent);
                                } else {
                                    varName = ILUtils.baseName(paramIdent);
                                }
                                if (varName != null) {
                                    VariableDecl varDecl = unitSymbolTable.getVariableDecl(varName);
                                    if (varDecl != null) {
                                        varDecl.formalArgRank = nbArg;
                                    }
                                }
                            }
                            ILUtils.getArguments(callTree).addChild(ILUtils.copy(paramIdent), nbArg);
                            nbArg++;
                        }
                        paramTypeSpecList =
                                TapList.append(paramTypeSpecList, varTypeSpecs);
                    }
                }

                // Add an instruction containing the formal params into entryBlock:
                Instruction headInstruction = new Instruction(callTree, curSCInstruction, toLineNo.get()) ;
                entryBlock.addInstrTl(headInstruction);

                // For Cuda, declare the global blockIdx, blockDim, and threadIdx, plus a few global things:
                if (ILUtils.contains(modifiers, "__global__", true) || ILUtils.contains(modifiers, "__device__", true)) {
                    callGraph.hasCuda = true ;
                    WrapperTypeSpec integerTypeSpec = unitSymbolTable.getTypeDecl("integer").typeSpec ;
                    FieldDecl[] uint3Fields = new FieldDecl[4] ;
                    uint3Fields[0] = new FieldDecl("x", integerTypeSpec) ;
                    uint3Fields[1] = new FieldDecl("y", integerTypeSpec) ;
                    uint3Fields[2] = new FieldDecl("z", integerTypeSpec) ;
                    uint3Fields[3] = new FieldDecl("w", integerTypeSpec) ;
                    WrapperTypeSpec uint3 = new WrapperTypeSpec(
                                                new CompositeTypeSpec("uint3", uint3Fields,
                                                    SymbolTableConstants.RECORDTYPE, null, null)) ;
                    VariableDecl parallelGlobalVarDecl ;
                    parallelGlobalVarDecl = new VariableDecl(ILUtils.build(ILLang.op_ident, "blockIdx"), uint3) ;
                    parallelGlobalVarDecl.setInstruction(headInstruction) ;
                    unitSymbolTable.addSymbolDecl(parallelGlobalVarDecl) ;
                    parallelGlobalVarDecl = new VariableDecl(ILUtils.build(ILLang.op_ident, "blockDim"), uint3) ;
                    parallelGlobalVarDecl.setInstruction(headInstruction) ;
                    unitSymbolTable.addSymbolDecl(parallelGlobalVarDecl) ;
                    parallelGlobalVarDecl = new VariableDecl(ILUtils.build(ILLang.op_ident, "threadIdx"), uint3) ;
                    parallelGlobalVarDecl.setInstruction(headInstruction) ;
                    unitSymbolTable.addSymbolDecl(parallelGlobalVarDecl) ;
                }

                WrapperTypeSpec[] argumentsTypes = new WrapperTypeSpec[TapList.length(paramTypeSpecList)];
                i = 0;
                while (paramTypeSpecList != null) {
                    argumentsTypes[i] = paramTypeSpecList.head;
                    paramTypeSpecList = paramTypeSpecList.tail;
                    i++;
                }

                if (language == TapEnv.CPLUSPLUS) {
                    // Forget about the scope prefix in front of the given classNameTree,
                    // because we suppose it is redundant
                    if (classNameTree.opCode() == ILLang.op_scopeAccess) {
                        classNameTree = classNameTree.down(2);
                    }
                    classNameString = ILUtils.getIdentString(classNameTree);
                    if (classNameString != null) {
                        Unit classUnit;
                        if (classNameTree.opCode() == ILLang.op_ident && unit.upperLevelUnit() != null &&
                                unit.upperLevelUnit().name().equals(classNameString)) {
                            // then if the className is the same as the surrounding class definition, it's easy:
                            classUnit = unit.upperLevelUnit();
                        } else {
                            TapEnv.printlnOnTrace("DANGER: function Unit out of classUnit: " + unit.name()
                                    + " i.e. " + funcNameString);
// System.out.println("  classNameTree:"+classNameTree+" upperLevelUnit:"+unit.upperLevelUnit()+" upperName:"+(unit.upperLevelUnit()==null?"?":ILUtils.stripSpecialization(unit.upperLevelUnit().name))) ;
                            // else look for the class with the name given in classNameTree
                            classUnit = null; //TODO: find classUnit
                            unit.setUpperLevelUnit(classUnit);
                        }
                        if (classUnit != null) {
                            TapList<FunctionDecl> sameFuncs =
                                    classUnit.privateSymbolTable().getTopFunctionDecl(funcNameString,
                                            returnTypeSpec, argumentsTypes, true);
                            if (sameFuncs == null) {
                                sameFuncs = classUnit.protectedSymbolTable().getTopFunctionDecl(funcNameString,
                                        returnTypeSpec, argumentsTypes, true);
                            }
                            if (sameFuncs == null) {
                                sameFuncs = classUnit.publicSymbolTable().getTopFunctionDecl(funcNameString,
                                        returnTypeSpec, argumentsTypes, true);
                            }
                            if (sameFuncs != null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree, "A declaration of function " + funcNameString + " in class " + classUnit.name() + " already exists (ignoring new decl)" + " HERE:" + new FunctionTypeSpec(returnTypeSpec, argumentsTypes) + " EXISTING:" + sameFuncs.head + " FTYPE:" + sameFuncs.head.functionTypeSpec());
                            }
                        } else {
                            // si l'on n'a pas encore cree la classe, on declare
                            // notre methode de type undefined et
                            // on attend que la classe vienne nous chercher.
                            TapEnv.toolWarning(-1, " NOTE: function " + unit + " will be marked as undefined");
                            unit.setUndefined();
                        }
                    } else {
                        TapList<FunctionDecl> sameFuncs =
                                callGraph.cPlusPlusRootSymbolTable().getTopFunctionDecl(funcNameString,
                                        returnTypeSpec, argumentsTypes, true);
                        if (sameFuncs != null) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree, "A declaration of function " + funcNameString + " already exists (ignoring new decl)" + " HERE:" + new FunctionTypeSpec(returnTypeSpec, argumentsTypes) + " EXISTING:" + sameFuncs.head + " FTYPE:" + sameFuncs.head.functionTypeSpec());
                        }
                    }
                }

                boolean isStatic = ILUtils.contains(unit.modifiers, ILLang.op_ident, "private") != null;

// System.out.println("  LOOKING FOR PREVIOUS DEFINITIONS OF "+unit);
                // Search for already existing decl of this procedure in the current decl hosting SymbolTable.
                // In C++, we must look for existing decl with exactly the same types, because overloading is permitted.
                // otherwise, we look for any existing decl with the same number of arguments.
                boolean allowsOverloading = language == TapEnv.CPLUSPLUS;
                TapList<FunctionDecl> prevDecls =
                        contextSymbolTable.getTopFunctionDecl(funcNameString, returnTypeSpec, argumentsTypes,
                                classNameString, allowsOverloading);
// System.out.println("a PREVDECLS:"+prevDecls) ;
// System.out.println("CG INTERF U:"+ callGraph.interfaceUnits()) ;
                if (prevDecls == null) {
                    TapList<Unit> interfaceUnits = callGraph.interfaceUnits();
                    Unit existingInterface ;
                    while (interfaceUnits != null) {
                        existingInterface = interfaceUnits.head ;
                        if (existingInterface.name().equals(unit.name())

                            // cf set03/lh051 vs set11/mvo02: interface decls in MODULE are not seen out of MODULE unless it is USE'd ?
                            && !existingInterface.enclosingUnitOfInterface.isModule()
                            && existingInterface.enclosingUnitOfInterface.upperLevelUnit()!=null
                            && existingInterface.enclosingUnitOfInterface.upperLevelUnit().encloses(unit)
                            ) {
// System.out.println("   !! NEW "+unit+" DETECTED AS SOLVING INTERFACE "+existingInterface) ;
                            SymbolDecl prevInterfaceDecl =
                                    existingInterface.enclosingUnitOfInterface.publicSymbolTable().getDecl(unit.name(), SymbolTableConstants.FUNCTION, true);
                            if (prevInterfaceDecl == null) {
                                prevInterfaceDecl =
                                        existingInterface.enclosingUnitOfInterface.privateSymbolTable().getDecl(unit.name(), SymbolTableConstants.FUNCTION, true);
                            }
                            if (prevInterfaceDecl != null) {
                                prevDecls = new TapList<>((FunctionDecl) prevInterfaceDecl, prevDecls);
                            }
                        }
                        interfaceUnits = interfaceUnits.tail;
                    }
                }
// System.out.println("b PREVDECLS:"+prevDecls) ;
                // If not found, search as a function formal parameter of an enclosing procedure:
                if (prevDecls == null && language == TapEnv.FORTRAN
                        && contextSymbolTable.basisSymbolTable().isFormalParamsLevel()) {
                    prevDecls = contextSymbolTable.basisSymbolTable().getTopFunctionDecl(
                            funcNameString, returnTypeSpec, argumentsTypes, false);
                }
                // If not found, search as an extern procedure in the current language's root SymbolTable:
                if (prevDecls == null && !isStatic) {
                    prevDecls = callGraph.languageRootSymbolTable(language).getTopFunctionDecl(
                            funcNameString, returnTypeSpec, argumentsTypes, false);
                }
                // If not found, search as a procedure in the "other" language (MIXED case)
                if (prevDecls == null && TapEnv.inputLanguage() == TapEnv.MIXED) {
                    prevDecls = callGraph.languageRootSymbolTable(language == TapEnv.C ? TapEnv.FORTRAN : TapEnv.C).getFunctionDecl(
                            funcNameString, returnTypeSpec, argumentsTypes, false);
                }
// System.out.println("c PREVDECLS:"+prevDecls) ;
                // we assume there is only 1 existing function that matches with the current added function
                // and several interfaceDecls that match the current added function cf set11/vpf06
                unit.setFunctionTypeSpec(
                        new FunctionTypeSpec(returnTypeSpec, argumentsTypes, variableArgList));
                FunctionDecl prevDecl;
                while (prevDecls != null) {
                    prevDecl = prevDecls.head;
                    if (prevDecl != null && prevDecl.unit() != null) {
                        if (prevDecl.isStandard()) {
                            if (unit.isInterface()) {
                                if (prevDecl.functionTypeSpec().returnType.wrappedType != null
                                        && unit.functionTypeSpec().returnType.wrappedType != null
                                        && !prevDecl.functionTypeSpec().returnType.equalsCompilDep(unit.functionTypeSpec().returnType)) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree, "(TC90) Type mismatch for result of function " + unit.name() + " declared in interface " + unit.functionTypeSpec().returnType.showType() + ",previously given " + prevDecl.functionTypeSpec().returnType.showType() + " (ignored new)");
                                }
                                keepUnit = false;
                                standardUnit = prevDecl.unit();
                            } else if (unit.isIntrinsic() || fromLib) {
                                keepUnit = true;
                            } else if (prevDecl.unit().upperLevelUnit() != unit.upperLevelUnit()
                                    || prevDecl.isInterface() || unit.isInterface()) {
                                keepUnit = true;
                                if (TapEnv.sameLanguageFamily(prevDecl.unit().language(), language)
                                        && prevDecl.unit().upperLevelUnit().isTranslationUnit()
                                        && unit.upperLevelUnit().isTranslationUnit()) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree,
                                            "(DD35) Multiple definition of procedure " + unit.name());
                                    keepUnit = false;
                                }
                            } else if (!TapEnv.sameLanguageFamily(prevDecl.unit().language(), language)) {
                                keepUnit = true;
                            } else {
                                if (!unit.isInterface() && language != TapEnv.CPLUSPLUS) {
                                    // No worry for an "interface": it is normal *not* to build a flow graph then.
                                    // Don't complain for duplicate func decls in C++, because we still don't know
                                    // how to compare types well. So we just assume the program is correct!
                                    TapEnv.toolWarning(-1, "Could not build the flow graph of unit " + unit.name());
                                }
                                keepUnit = false;
                            }
                        } else if (prevDecl.isInterface()) {
                            if (prevDecl.unit().enclosingUnitOfInterface == unit.upperLevelUnit()) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree,
                                        "(DD21) Procedure " + prevDecl.unit().name()
                                                + " defined in interface body clashes with internal procedure defined in "
                                                + unit.upperLevelUnit().name());
                            }
                            if (prevDecl.functionTypeSpec().returnType.wrappedType != null
                                    && unit.functionTypeSpec().returnType.wrappedType != null
                                    && !prevDecl.functionTypeSpec().returnType.
                                    equalsCompilDep(unit.functionTypeSpec().returnType)) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree, "(TC91) Type mismatch for result of function " + unit.name() + " declared " + unit.functionTypeSpec().returnType.showType() + ",previously given in interface " + prevDecl.functionTypeSpec().returnType.showType());
                            }
                            // detruire la oldUnit seulement si l'analyse de la nouvelle unit termine correctement:
                            callGraph.setPreviousFunctionDeclAndNewUnit(prevDecl, unit);
                            keepUnit = true;
                        } else if (prevDecl.isExternal()) {
                            if (fromLib && prevDecl.unit().isC() && unit.isC()) {
                                // en C on garde l'external et on jette la unit definie dans une lib cf C v69
                            } else if (TapEnv.sameLanguageFamily(prevDecl.unit().language(), language)) {
// [llh] not sure the following makes sense. To be discussed Vs. fix in TypeSpec.combineWith().
//                                 // le returnType doit etre exactement le meme:
//                                 if (unit.functionTypeSpec().returnType.wrappedType == null) {
//                                     // TODO a verifier, cf F90 v453:
//                                     unit.functionTypeSpec().returnType.wrappedType = prevDecl.functionTypeSpec().returnType.wrappedType;
//                                 }
                                boolean protoMatches =
                                        unit.functionTypeSpec().receives(prevDecl.functionTypeSpec(), null, null);
                                if (!protoMatches && unit.isC()) {
                                    FunctionTypeSpec protoType = prevDecl.functionTypeSpec();
                                    // In C, the prototype can have zero arguments.
                                    protoMatches = protoType != null
                                            && (protoType.argumentsTypes == null || protoType.argumentsTypes.length == 0)
                                            && unit.functionTypeSpec().returnType != null
                                            && unit.functionTypeSpec().returnType.equalsCompilDep(protoType.returnType);
                                }
                                if (protoMatches) {
                                    Unit oldUnit = prevDecl.unit();
                                    if (oldUnit.getForwardDeclaration() != null) {
                                        // on garde l'instruction forward d'origine
                                        unit.setForwardDeclaration(oldUnit.getForwardDeclaration());
                                    }
                                    // detruire la oldUnit seulement si l'analyse de la nouvelle unit termine correctement:
                                    callGraph.setPreviousFunctionDeclAndNewUnit(prevDecl, unit);
                                } else {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, funcNameTree, "(TC92) Incompatible declaration of function prototype or interface " + unit.name());
                                }
                            }
                            keepUnit = true;
                        }
                    }
                    prevDecls = prevDecls.tail;
                }
                // Create the functionDecl and add it into the symbolTable:
                FunctionDecl tmpFunctionDecl =
                        new FunctionDecl(funcNameTree, standardUnit, contextSymbolTable, classNameString);
                // Accumulate possible modifiers from previous other declaration (?) :
                SymbolDecl prevSymbolDecl =
                        contextSymbolTable.getSymbolDecl(ILUtils.getIdentString(funcNameTree));
                if (prevSymbolDecl != null) {
                    tmpFunctionDecl.addExtraInfo(prevSymbolDecl.extraInfo());
                }
                // Accumulate other modifiers:
                if (!unit.isTopInFile()
                        && unit.upperLevelUnit().fortranStuff() != null
                        && unit.upperLevelUnit().fortranStuff().publicPrivateDefault != null) {
                    String defaultPublicPrivate =
                            unit.upperLevelUnit().fortranStuff().publicPrivateDefault;
                    tmpFunctionDecl.addExtraInfo(new TapList<>(defaultPublicPrivate, null));
                } else if (!unit.isTopInFile() && !unit.upperLevelUnit().privacy.isEmpty() /*&& methodDecl == null*/) {
                    tmpFunctionDecl.addExtraInfo(new TapList<>(unit.upperLevelUnit().privacy, null));
                }

                TapTriplet<FunctionDecl, Unit, TapPair<FunctionDecl, SymbolTable>> previous =
                        callGraph.getPreviousFunctionDeclAndNewUnit(funcNameString);
                if (previous != null && !previous.first.unit().isInterface()) {
                    previous.third = new TapPair<>(tmpFunctionDecl, contextSymbolTable) ; //was: callGraph.fillPreviousFunctionDeclAndNewUnit(tmpFunctionDecl, contextSymbolTable);
                } else {
                    // unitSymbolTable is the (public/formal parameters) ST of the currently built Unit.
                    // We set hostingSymbolTable to the SymbolTable that must contain this unit's FunctionDecl:
                    SymbolTable hostingSymbolTable =
                            findHostingSymbolTable(contextSymbolTable, null, modifiers);
//                 if (methodDecl != null) {
//                     tmpFunctionDecl.addExtraInfo(methodDecl.extraInfo());
//                 }
                    if (unit.isInterface() || unit.isExternal()) {
                        Unit upperUnit = unit.enclosingUnitOfInterface;
                        if (upperUnit != null && upperUnit.isStandard()) {
                            SymbolDecl declAsFormalParam =
                                    upperUnit.publicSymbolTable().getTopSymbolDecl(unit.name());
                            if (declAsFormalParam != null) {
                                hostingSymbolTable = upperUnit.publicSymbolTable();
                            }
                        }
                    }
                    // If this is actually defining the name of a function type, do not enter it as the name of a function:
                    if (!unit.isAbstractFunctionType) {
                        hostingSymbolTable.addSymbolDecl(tmpFunctionDecl);
                    }
                }
                if (otherReturnVarDecl != null && tmpFunctionDecl.unit() != null) {
                    tmpFunctionDecl.unit().setOtherReturnVar(otherReturnVarDecl);
                    otherReturnVarDecl.setReturnVar(tmpFunctionDecl, TapEnv.isFortran(language));
                }
                if (unit.modifiers != null && unit.modifiers.opCode() == ILLang.op_accessDecl) {
                    String cName = unit.name();
                    if (unit.modifiers.down(2).length() == 2
                            && unit.modifiers.down(2).down(2).opCode() == ILLang.op_nameEq) {
                        cName = unit.modifiers.down(2).down(2).down(2).stringValue();
                    }
                    callGraph.addBindFortranCFunction(unit.name(), cName, tmpFunctionDecl);
                }
                bodySymbolTable = new SymbolTable(unitSymbolTable);
                bodySymbolTable.setShortName("Local variables SymbolTable of procedure " + unit.name());
                bodySymbolTable.unit = unit;
                unit.addDerivedSymbolTable(bodySymbolTable);
                unit.setPrivateSymbolTable(bodySymbolTable);
                break ;
            }
            default: {
                TapEnv.toolWarning(-1, "(Flow Graph builder) Unexpected unit operator: " + unitCode);
            }
        }

        if (!keepUnit) {
            pushUnitWillBeDeleted();
        }

        FGThreads threads = new FGThreads();
        curParallelControls = null;
        if (unit.isModule() || unit.isClass()) {
            int nbParts = entryBlock.maxEntryRank();
            threads.add(entryBlock, FGConstants.ENTRY, nbParts + 1);
            threads.convergeToBlock(centralBlock, false, traceOn);
            threads.empty();
            threads.add(centralBlock, FGConstants.NO_TEST, FGConstants.NO_TEST);
        } else if (unit.isConstructor()) {
            threads.add(entryBlock, FGConstants.ENTRY, FGConstants.MAIN);
            InitConstructorBlock initConstructorBlock = new InitConstructorBlock(unitSymbolTable, allBlocks);
            unit.setInitConstructorBlock(initConstructorBlock);
            threads.convergeToBlock(initConstructorBlock, false, traceOn);
            threads.empty();
            threads.add(initConstructorBlock, FGConstants.NO_TEST, FGConstants.NO_TEST);
            treeStream2FlowGraph(threads, unitSymbolTable);
        } else {
            threads.add(entryBlock, FGConstants.ENTRY, FGConstants.MAIN);
        }

        treeStream2FlowGraph(threads, bodySymbolTable);

        if (!keepUnit) {
            popUnitWillBeDeleted();
        }

        // The header is processed. Now process the body:
        if (!keepUnit) {
            if (!unit.isInterface() && language != TapEnv.CPLUSPLUS) {
                // No worry for an "interface": it is normal *not* to build a flow graph then.
                // Don't complain for duplicate func decls in C++, because we still don't know 
                // how to compare types well. So we just assume the program is correct!
                TapEnv.toolWarning(-1, "Could not build the flow graph of unit " + unit.name());
            }
            callGraph.deleteUnit(unit);
            unit = null;
        } else {
            // Connect the final threads to the exitBlock:
            threads.convergeToBlock(exitBlock, false, traceOn);

            // Put the remaining pending comments in the catch-all place for this Unit.
            if (pendingComments2.first != null) {
                unit.setLostComments(TapList.append(pendingComments2.first, unit.lostComments()));
            }
            if (pendingComments2.second != null) {
                unit.setLostComments(TapList.append(pendingComments2.second, unit.lostComments()));
            }

            // Propagation to set the "assigned-goto" flow arrows:
            analyzeAssGoto();
            // Error messages about undefined labels:
            labelHeap.undefinedLabels(exitBlock, unitSymbolTable);

            // Final polishing of the unit's flow graph, detects loops, cycling arrows,
            //  empty blocks or blocks to merge, etc...:
            unit.makeFlowGraph(entryBlock, allBlocks.tail, exitBlock, false);

            callGraph.absorbAndDeleteAfterCreateNewUnit();

// System.out.println(" UNIT TYPE BEFORE EXPORT:"+unit+" --> "+unit.functionTypeSpec()) ;
            unit.exportFunctionTypeSpec();
// System.out.println(" UNIT TYPE  AFTER EXPORT:"+unit+" --> "+unit.functionTypeSpec()) ;
            callGraph.resetPreviousFunctionDeclAndNewUnit();
        }

        if (TapEnv.traceInputIL()) {
            switch (unitCode) {
                case ILLang.op_file:
                case ILLang.op_declarations:
                case ILLang.op_blockStatement:
                    break;
                case ILLang.op_module: {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] module " + unit);
                    break;
                }
                case ILLang.op_class: {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] class " + unit);
                    break;
                }
                case ILLang.op_destructor: {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] destructor " + unit);
                    break;
                }
                case ILLang.op_constructor: {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] constructor " + unit);
                    break;
                }
                default: {// We assume we have just seen an op_function or op_program.
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] function " + unit);
                    break;
                }
            }
        }
        TapEnv.popRelatedUnit();
    }

    private boolean traceOn(String unitName) {
        return (TapEnv.traceFlowGraphBuild() != null
                && (TapEnv.traceFlowGraphBuild().head.equals("%all%")
                || TapList.containsEquals(TapEnv.traceFlowGraphBuild(), unitName)));
    }

    /**
     * As the tree operators arrive through the tree protocol, this builds the
     * Blocks and chains them with control flow arrows. Recursive function. At
     * each call, takes a FGThreads for the incoming control flow, and modifies
     * it to hold the control flow flowing out of the sub-tree.
     * Uses global curParallelControls, the ordered chain of the
     * parallel ccontrol instructions that affect the coming tree
     *
     * @param readySymbolTable when non-null, an already prepared SymbolTable for the coming Tree,
     *                         which is probably a blockStatement or a declarations Tree.
     */
    private void treeStream2FlowGraph(FGThreads threads, SymbolTable readySymbolTable) throws IOException {
        Instruction newInstr;
        // TODO: preComments2 is useless. One should directly fill pendingComments2! [FEWER NONREGRESSION]
        TapPair<Tree, Tree> preComments2 = new TapPair<>(null, null);
        TapPair<Tree, Tree> postComments2 = new TapPair<>(null, null);
        Tree instructionTree;
        int position;
        ToInt toLineNo = new ToInt(-1);
        String annotationName;
        Tree annotationTree;
        while (inputTreeStream.seeOperator().code == ILLang.op_annotation) {
            inputTreeStream.readOperator();
            annotationName = inputTreeStream.readString();
            annotationTree = inputTreeStream.readTree();
            if ((!TapEnv.inStdCIncludeFile() || annotationName.equals("include")) &&
                    // cochonnerie rajoutee par cpp sur mac os x a ignorer:
                    !(TapEnv.isC(language) && !annotationTree.isAtom() && annotationTree.down(1).stringValue().startsWith("#pragma GCC set_debug_pwd"))) {
                if (annotationName.equals("preComments")) {
                    // [FEWER NONREGRESSION] SHOULD BE: preComments2.first = ILUtils.appendComments(preComments2.first, annotationTree);
                    if (preComments2.first == null) {
                        preComments2.first = annotationTree;
                    } else {
                        ILUtils.appendComments(preComments2.first, annotationTree);
                    }
                } else if (annotationName.equals("postComments")) {
                    // [FEWER NONREGRESSION] SHOULD BE: postComments2.first = ILUtils.appendComments(postComments2.first, annotationTree);
                    if (postComments2.first == null) {
                        postComments2.first = annotationTree;
                    } else {
                        ILUtils.appendComments(postComments2.first, annotationTree);
                    }
                } else if (annotationName.equals("preCommentsBlock")) {
                    // [FEWER NONREGRESSION] SHOULD BE: preComments2.second = ILUtils.appendCommentsBlock(preComments2.second, annotationTree);
                    if (preComments2.second == null) {
                        preComments2.second = annotationTree;
                    } else {
                        ILUtils.appendCommentsBlock(preComments2.second, annotationTree);
                    }
                } else if (annotationName.equals("postCommentsBlock")) {
                    // [FEWER NONREGRESSION] SHOULD BE: postComments2.second = ILUtils.appendCommentsBlock(postComments2.second, annotationTree);
                    if (postComments2.second == null) {
                        postComments2.second = annotationTree;
                    } else {
                        ILUtils.appendCommentsBlock(postComments2.second, annotationTree);
                    }
                } else if (annotationName.equals("include")) {
                    // This mechanism is triggered only in C, that inserts these "include" annotations in the input IL
                    // This mechanism should be removed and replaced by the more general mechanism now developed
                    // for fortran+fpp with the new op_pppLine operator placed in comments. Cf addInstructionTreeAndPppLines()
                    // [VMP] on expanse tous les includes a l'interieur d'une Unit
                    // -> [llh] sauf les includes C "standard", qu'on ne veut pas expanser mais garder en #include
                    Tree includeCommand = TapEnv.getIncludeTree(annotationTree);
                    boolean doInclude =
                        (includeCommand == null
                         ? TapEnv.get().includeFiles != null && TapEnv.enclosingIncludeIsEmptyOr(annotationTree)
                         : (TapEnv.checkAndUpdateTapEnvInclude(includeCommand)
                            && (!TapEnv.get().expandAllIncludeFile
                                || TapEnv.isStdIncludeName(includeCommand.stringValue())))) ;
                    if (doInclude && includeCommand != null
                            && TapList.containsEquals(TapEnv.includeFileStack(), includeCommand.stringValue())) {
                        if (!unit.isTranslationUnit()) {
                            doInclude = false;
                            TapEnv.checkAndUpdateTapEnvInclude(null);
                        }
                    }
                    if (doInclude) {
                        if (preComments2.first != null) {
                            pendingComments2.first = TapList.append(pendingComments2.first, preComments2.first.childrenList());
                            preComments2.first = null;
                        }
                        if (preComments2.second != null) {
                            pendingComments2.second = TapList.append(pendingComments2.second, preComments2.second.childrenList());
                            preComments2.second = null;
                        }
                        if (includeCommand != null) {
                            String includeName = includeCommand.stringValue();
                            if (!TapEnv.isStdIncludeName(includeName)) {
                                TapEnv.pushIncludeFile(includeName);
                            }
// System.out.println(" ---> ADDINSTRUCTIONTREE:"+includeCommand) ;
                            newInstr =
                                    addInstructionTree(includeCommand, null, postComments2, pendingComments2, threads, true);
                            if (includeCommand.equalsTree(TapEnv.currentIncludeInstruction().tree)) {
                                TapEnv.currentIncludeInstruction().preComments = newInstr.preComments;
                                TapEnv.currentIncludeInstruction().preCommentsBlock = newInstr.preCommentsBlock;
                                TapEnv.currentIncludeInstruction().postComments = newInstr.postComments;
                                TapEnv.currentIncludeInstruction().postCommentsBlock = newInstr.postCommentsBlock;
                            }
                            newInstr.setFromIncludeCopy(TapEnv.currentIncludeInstruction(), false);
                        } else {
                            // exiting from the current include:
                            TapEnv.updateTapEnvInclude(null);
                        }
                        // wipe out any awaiting comment, so that they don't appear out of this include.
                        pendingComments2.first = null;
                        pendingComments2.second = null;
                        preComments2.first = null;
                        preComments2.second = null;
                        postComments2.first = null;
                        postComments2.second = null;
                    }
                }
            }
        }
        if (preComments2.first != null) {
            pendingComments2.first = TapList.append(pendingComments2.first, preComments2.first.childrenList());
            preComments2.first = null;
        }
        if (preComments2.second != null) {
            pendingComments2.second = TapList.append(pendingComments2.second, preComments2.second.childrenList());
            preComments2.second = null;
        }
        if (labelAbovePostComments != null) {
            postComments2.first = ILUtils.appendComments(postComments2.first, labelAbovePostComments);
            labelAbovePostComments = null;
        }
        TapList<Tree> endOfRegionDirectives = extractEndDirectivesFromPostComments(postComments2) ;
// if (endOfRegionDirectives!=null) System.out.println("EXTRACTED "+endOfRegionDirectives+" FROM POSTCOMMENTS:"+postComments2) ;
        switch (inputTreeStream.seeOperator().code) {
            case ILLang.op_include: {
                // This deals with include commands from Fortran:
                Tree includeTree = inputTreeStream.readTree(toLineNo);
                includeTree.setValue(ILUtils.normalizePath(includeTree.stringValue()));
// System.out.println("SEEN TREE INCLUDE:"+includeTree) ;
                Tree includeCommandAsOpInclude = TapEnv.setInIncludeFile(includeTree);
// System.out.println("TAPENV.currentIncludeInstruction is NOW:"+TapEnv.currentIncludeInstruction()) ;
                if (includeCommandAsOpInclude != null) {
                    // This is an entry into an include:
                    addInstructionTree(includeCommandAsOpInclude, toLineNo, postComments2, pendingComments2, threads, false);
                }
                if (TapEnv.isC(language)) {
                    // We arrive here in C only when a #include was not found.
                    // The sequel code must not be seen as part of the include:
                    TapEnv.resetIncludeManager() ;
                }
                break;
            }
            case ILLang.op_module:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
            case ILLang.op_program:
            case ILLang.op_function: {
                // Definition of a sub-Unit inside the current Unit.
                // => build a special, almost empty, declaration Instruction as a placeholder:
                if (unitWillBeDeleted()) {
                    inputTreeStream.readTree(); //Don't analyze below.
                } else {
                    int opCode = inputTreeStream.seeOperator().code;
                    Unit subUnit = callGraph.createNewUnit(unit, language);
                    TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPrePppLines = new TapList<>(null, null) ;
                    TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPostPppLines = new TapList<>(null, null) ;
                    TapTriplet<TapList<Tree>, Tree, Tree> pppLineAndComments ;
                    TapPair<Tree, Tree> tmpPost2 = new TapPair<>(null, null) ;
                    TapPair<TapList<Tree>, TapList<Tree>> tmpPending2 = new TapPair<>(null, null) ;
                    Tree tapIncludeTree ;
                    if (!TapEnv.inStdCIncludeFile()) {
                        extractPppLines(pendingComments2, postComments2, toPrePppLines, toPostPppLines) ;
                        toPrePppLines = toPrePppLines.tail ;
                        while (toPrePppLines!=null) {
                            pppLineAndComments = toPrePppLines.head ;
                            tmpPending2.first = pppLineAndComments.first ;
                            tmpPost2.first = pppLineAndComments.third ;
                            tapIncludeTree = manageIncludeLine(pppLineAndComments.second, toLineNo,
                                                               tmpPending2, threads, true) ;
                            addInstructionTree(tapIncludeTree, toLineNo, tmpPost2,
                                               new TapPair<TapList<Tree>, TapList<Tree>>(null, null), threads, true) ;
                            toPrePppLines = toPrePppLines.tail ;
                        }
                    }
                    try {
                        ++fgbDepth;
                        TapEnv.get().saveAndResetIncludeManager() ;
                        subUnit = FlowGraphBuilder.build(inputTreeStream, curSymbolTable,
                                callGraph, language, subUnit, false);
                        TapEnv.get().restoreIncludeManager() ;
                        --fgbDepth;
                    } catch (IOException exception) { // If buildUnitFlowGraph crashed badly: catch; clear subUnit; re-throw
                        --fgbDepth;
                        callGraph.deleteUnit(subUnit);
                        callGraph.deleteUnitFromSymbolTable(subUnit);
                        throw exception;
                    }

                    if (subUnit != null) {
                        subUnit.preComments = pendingComments2.first == null ? null :
                                ILUtils.build(ILLang.op_comments, ILUtils.collectCommentStrings(pendingComments2.first));
                        pendingComments2.first = null;
                        subUnit.preCommentsBlock = pendingComments2.second == null ? null :
                                ILUtils.build(ILLang.op_commentsBlock, ILUtils.collectCommentStrings(pendingComments2.second));
                        pendingComments2.second = null;
                        subUnit.postComments = postComments2.first == null ? null :
                                ILUtils.build(ILLang.op_comments, ILUtils.collectCommentStrings(postComments2.first));
                        postComments2.first = null;
                        subUnit.postCommentsBlock = postComments2.second == null ? null :
                                ILUtils.build(ILLang.op_commentsBlock, ILUtils.collectCommentStrings(postComments2.second));
                        postComments2.second = null;
                        subUnit.activityPatterns = null;
                        subUnit.setTranslationUnitSymbolTable(TapEnv.currentTranslationUnitSymbolTable());
                        // Management of the "main program":
                        if (opCode == ILLang.op_program ||
                                opCode == ILLang.op_function && TapEnv.isC(language) && subUnit.name().equals("main")) {
                            if (callGraph.getMainUnit() != null) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(DD10) Double declaration of the main program: " + callGraph.getMainUnit().name() + " and " + subUnit.name() + " (overwritten previous)");
                            }
                            callGraph.setMainUnit(subUnit);
                        }
                        if (subUnit.isModule() || subUnit.isClass()) {
                            // Move public symbols to their correct SymbolTable:
                            TapList<SymbolDecl> publicSymbolDecls = subUnit.privateSymbolTable().findPublicSymbolDecls();
                            if (publicSymbolDecls != null) {
                                TapList<SymbolDecl> inSymbolDecls = publicSymbolDecls;
                                while (inSymbolDecls != null) {
                                    if (inSymbolDecls.head.isA(SymbolTableConstants.FUNCTION)) {
                                        ((FunctionDecl) inSymbolDecls.head).definitionSymbolTable = subUnit.publicSymbolTable();
                                    } else if (inSymbolDecls.head.isA(SymbolTableConstants.INTERFACE)) {
                                        ((InterfaceDecl) inSymbolDecls.head).definitionSymbolTable = subUnit.publicSymbolTable();
                                    }
                                    inSymbolDecls = inSymbolDecls.tail;
                                }
                                subUnit.privateSymbolTable().moveSymbolDeclListUpRec(publicSymbolDecls, subUnit.publicSymbolTable());
                            }
                            // Move protected symbols to their correct SymbolTable:
                            TapList<SymbolDecl> protectedSymbolDecls = subUnit.privateSymbolTable().findProtectedSymbolDecls();
                            if (protectedSymbolDecls != null) {
                                TapList<SymbolDecl> inSymbolDecls = protectedSymbolDecls;
                                while (inSymbolDecls != null) {
                                    if (inSymbolDecls.head.isA(SymbolTableConstants.FUNCTION)) {
                                        ((FunctionDecl) inSymbolDecls.head).definitionSymbolTable = subUnit.protectedSymbolTable();
                                    } else if (inSymbolDecls.head.isA(SymbolTableConstants.INTERFACE)) {
                                        ((InterfaceDecl) inSymbolDecls.head).definitionSymbolTable = subUnit.protectedSymbolTable();
                                    }
                                    inSymbolDecls = inSymbolDecls.tail;
                                }
                                subUnit.privateSymbolTable().moveSymbolDeclListUpRec(protectedSymbolDecls, subUnit.protectedSymbolTable());
                            }
                        }
                        if (!TapEnv.inStdCIncludeFile()) {
                            instructionTree = Instruction.createUnitDefinitionStubTree(subUnit);
                            if (subUnit.isClass()) {
                                // If current language allows re-opening of a Unit definition, identify which bit of the
                                // defined Unit is defined by this part of its definition, and remember this:
                                instructionTree.setAnnotation("UnitPart", subUnit.entryBlock().maxEntryRank());
                            }
                            int subUnitHeaderLineNo = 0 ;
                            if (subUnit.entryBlock!=null && subUnit.entryBlock.instructions!=null) {
                                subUnitHeaderLineNo = subUnit.entryBlock.instructions.head.lineNo ;
                            }
                            toLineNo.set(subUnitHeaderLineNo) ;
                            addInstructionTree(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                            toPostPppLines = toPostPppLines.tail ;
                            while (toPostPppLines!=null) {
                                pppLineAndComments = toPostPppLines.head ;
                                tmpPending2.first = pppLineAndComments.first ;
                                tmpPost2.first = pppLineAndComments.third ;
                                tapIncludeTree = manageIncludeLine(pppLineAndComments.second, toLineNo,
                                                                   tmpPending2, threads, true) ;
                                addInstructionTree(tapIncludeTree, toLineNo, tmpPost2,
                                                   new TapPair<TapList<Tree>, TapList<Tree>>(null, null), threads, true) ;
                                toPostPppLines = toPostPppLines.tail ;
                            }
                        }
                    }
                }
                break;
            }
            case ILLang.op_goto: {
                inputTreeStream.readOperator();
                position = inputTreeStream.position();
                Tree labelTree = inputTreeStream.readTree() ;
                // Get or set(+register) the destination BasicBlock,
                // and converge threads to it:
                threads.convergeToBlock(labelHeap.getSetLabelled(labelTree.stringValue(), allBlocks, false,
                        /* vmp: position-> tree, mais lequel??? */ null),
                        false, traceOn);
                // Lastly, empty the current threads.
                threads.empty();
                break;
            }
            case ILLang.op_gotoLabelVar:
                // Build the incoming assigned goto instruction:
                instructionTree = inputTreeStream.readTree(toLineNo);
                // Declare the present goto variable:
                newLabelVar(ILUtils.gotoLabelVar(instructionTree));
                // Converge threads to here:
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                // No threads go out of here: empty the current threads.
                // FGArrows from here will be set later by assigned goto's
                // analysis.*/
                threads.empty();
                break;
            case ILLang.op_assignLabelVar:
                // Build the incoming goto variable assignment:
                instructionTree = inputTreeStream.readTree(toLineNo);
                // Declare the present goto variable:
                newLabelVar(ILUtils.assignedLabelVar(instructionTree));
                // Converge threads to here:
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                break;
            case ILLang.op_labelStatement: {
                inputTreeStream.readOperator();
                position = inputTreeStream.position();
                /* First comes the (list of) label(s): */
                Tree labelsTree = inputTreeStream.readTree();
                /* Extract the label(s): */
                TapList<String> labelsList;
                if (labelsTree.opCode() == ILLang.op_labels) {
                    labelsList = null;
                    Tree[] labels = labelsTree.children();
                    for (Tree label : labels) {
                        labelsList = new TapList<>(label.stringValue(), labelsList);
                    }
                } else {
                    labelsList = new TapList<>(labelsTree.stringValue(), null);
                }
                /* Get or set (and register) the destination basicBlock: */
                Block block = null;
                while (labelsList != null) {
                    if (block == null) {
                        block = labelHeap.getSetLabelled(labelsList.head, allBlocks, true, labelsTree);
                    } else {
                        labelHeap.fuseLabelled(labelsList.head, block, labelsTree);
                    }
                    labelsList = labelsList.tail;
                }
                /* In case of double declaration of labels, "block" may be null. */
                if (block != null) {
                    block.symbolTable = curSymbolTable;
                    /* converge threads to it: */
                    threads.convergeToBlock(block, false, traceOn);
                    /* and resume building the FG from the labelled statement */
                    threads.empty();
                    threads.add(block, FGConstants.NO_TEST, FGConstants.NO_TEST);
                }
                labelAbovePostComments = postComments2.first;
                postComments2.first = null;
                treeStream2FlowGraph(threads, null);
                break;
            }
            case ILLang.op_compGoto:
                position = inputTreeStream.position() + 1;
                instructionTree = inputTreeStream.readTree(toLineNo);
                ILUtils.setPosition(instructionTree, position);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                if (newInstr != null) {
                    // Analyze the list of labels, or complain...
                    if (instructionTree.down(1).opCode() == ILLang.op_labels) {
                        Tree[] labels = instructionTree.down(1).children();
                        position++;
                        for (int i = 0; i < labels.length; i++) {
                            /* Set a flow arrow to each label: */
                            threads.reTypeThread(FGConstants.COMP_GOTO, i + 1);
                            threads.convergeToBlock(labelHeap.getSetLabelled(labels[i].stringValue(), allBlocks,
                                    false, instructionTree), false, traceOn);
                        }
                    } else {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, instructionTree, "(CF02) Computed-GOTO without legal destinations list");
                    }
                    threads.reTypeThread(FGConstants.COMP_GOTO, FGConstants.DEFAULT);
                }
                break;
            case ILLang.op_if: {
                FGThreads thenThreads = new FGThreads();
                FGThreads elseThreads = new FGThreads();
                Tree testExpression;
                Tree ifTestInstruction;
                inputTreeStream.readOperator();
                position = inputTreeStream.position();
                // Then comes the test expression. Converge threads to it:
                testExpression = inputTreeStream.readTree(toLineNo);
                ifTestInstruction = ILUtils.build(ILLang.op_if, testExpression);
                ILUtils.setPosition(ifTestInstruction, position);
                newInstr = addInstructionTreeAndPppLines(ifTestInstruction, toLineNo, null, pendingComments2, threads, false);
                // Store the previous SCInstruction:
                Instruction oldSCInstruction = curSCInstruction;
                curSCInstruction = newInstr;
                boolean notEmpty = threads.isNotEmpty();
                if (notEmpty)
                    /* Build the flow-graph for the "then" part: */ {
                    thenThreads.add(threads.block(), FGConstants.IF, FGConstants.TRUE);
                }
                treeStream2FlowGraph(thenThreads, null);
                if (notEmpty)
                    /* Build the flow-graph for the "else" part: */ {
                    elseThreads.add(threads.block(), FGConstants.IF, FGConstants.FALSE);
                }
                treeStream2FlowGraph(elseThreads, null);
                /*
                 * Build the final threads, concatenation of then and else threads:
                 */
                threads.empty();
                threads.concat(thenThreads);
                threads.concat(elseThreads);
                /* Restore the previous SCInstruction: */
                curSCInstruction = oldSCInstruction;
                break;
            }
            case ILLang.op_where: {
                inputTreeStream.readOperator();
                Tree localTest = inputTreeStream.readTree(toLineNo);
                instructionTree = ILUtils.build(ILLang.op_where, localTest);
                InstructionMask oldWhereMask = whereMask;
                whereMask = new InstructionMask(localTest, whereMask);
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                treeStream2FlowGraph(threads, null);
                whereMask = whereMask.createElseMask();
                treeStream2FlowGraph(threads, null);
                Tree namedWhere = inputTreeStream.readTree();
                if (namedWhere.opCode() != ILLang.op_none) {
                    localTest.setAnnotation("namedWhere", namedWhere);
                }
                whereMask = oldWhereMask;
                break;
            }
            case ILLang.op_switch:
            case ILLang.op_switchType: {
                Tree exprTree;
                int caseNumber = -1;
                boolean caseIsDefault;
                TapList<Tree> hdListTreeCase = new TapList<>(null, null);
                TapList<Tree> tlListTreeCase = hdListTreeCase;
                Tree defaultTreeCase = null;
                Block switchBlock = null;
                Operator switchOperator = inputTreeStream.readOperator();
                position = inputTreeStream.position();
                Tree switchTestInstruction = ILUtils.build(switchOperator.code, inputTreeStream.readTree(toLineNo));
                ILUtils.setPosition(switchTestInstruction, position);
                newInstr = addInstructionTreeAndPppLines(switchTestInstruction, toLineNo, postComments2, pendingComments2, threads, false);
                Instruction oldSCInstruction = curSCInstruction;
                curSCInstruction = newInstr;
                // Skip possible annotations (TODO: maybe we'd better analyse them?)
                while (inputTreeStream.seeOperator().code == ILLang.op_annotation) {
                    inputTreeStream.readOperator();
                    annotationName = inputTreeStream.readString();
                    annotationTree = inputTreeStream.readTree();
                }
                // We assume the next operator must be an op_switchCases:
                inputTreeStream.readOperator();
                boolean notEmpty = threads.isNotEmpty();
                if (notEmpty) {
                    switchBlock = threads.block();
                }
                threads.empty();
                boolean foundDefault = false;
                FlowGraphBuilder.ControlJumpInfo switchJumpInfo = new FlowGraphBuilder.ControlJumpInfo(ILLang.op_switch, null, null);
                nestedControlStack = new TapList<>(switchJumpInfo, nestedControlStack);
                while (inputTreeStream.seeOperator().code != ILLang.op_endOfList) {
                    // Eat out all possible comments:
                    while (inputTreeStream.seeOperator().code == ILLang.op_annotation) {
                        inputTreeStream.readOperator();
                        annotationName = inputTreeStream.readString();
                        annotationTree = inputTreeStream.readTree();
                        if (annotationName.equals("preComments")) {
                            // [FEWER NONREGRESSION] SHOULD BE: preComments2.first = ILUtils.appendComments(preComments2.first, annotationTree);
                            if (preComments2.first == null) {
                                preComments2.first = annotationTree;
                            } else {
                                ILUtils.appendComments(preComments2.first, annotationTree);
                            }
                        } else if (annotationName.equals("postComments")) {
                            // [FEWER NONREGRESSION] SHOULD BE: postComments2.first = ILUtils.appendComments(postComments2.first, annotationTree);
                            if (postComments2.first == null) {
                                postComments2.first = annotationTree;
                            } else {
                                ILUtils.appendComments(postComments2.first, annotationTree);
                            }
                        } else if (annotationName.equals("preCommentsBlock")) {
                            // [FEWER NONREGRESSION] SHOULD BE: preComments2.second = ILUtils.appendComments(preComments2.second, annotationTree);
                            if (preComments2.second == null) {
                                preComments2.second = annotationTree;
                            } else {
                                ILUtils.appendCommentsBlock(preComments2.second, annotationTree);
                            }
                        } else if (annotationName.equals("postCommentsBlock")) {
                            // [FEWER NONREGRESSION] SHOULD BE: postComments2.second = ILUtils.appendComments(postComments2.second, annotationTree);
                            if (postComments2.second == null) {
                                postComments2.second = annotationTree;
                            } else {
                                ILUtils.appendCommentsBlock(postComments2.second, annotationTree);
                            }
                        }
                    }
                    if (preComments2.first != null) {
                        pendingComments2.first = TapList.append(pendingComments2.first, preComments2.first.childrenList());
                        preComments2.first = null;
                    }
                    if (preComments2.second != null) {
                        pendingComments2.second = TapList.append(pendingComments2.second, preComments2.second.childrenList());
                        preComments2.second = null;
                    }
                    // We assume the next operator must be an op_switchCase or op_typeCase:
                    Operator switchCaseOperator = inputTreeStream.readOperator();
                    exprTree = inputTreeStream.readTree();
                    caseIsDefault = (exprTree.children().length == 0);
                    exprTree = ILUtils.build(switchCaseOperator.code, exprTree, ILUtils.build(ILLang.op_blockStatement));
                    if (caseIsDefault) {
                        defaultTreeCase = exprTree;
                    } else {
                        tlListTreeCase = tlListTreeCase.placdl(exprTree);
                    }
                    // Build the flow-graph for the case i
                    if (notEmpty) {
                        if (caseIsDefault) {
                            foundDefault = true;
                            threads.add(switchBlock, FGConstants.CASE, FGConstants.DEFAULT);
                        } else {
                            caseNumber++;
                            threads.add(switchBlock, FGConstants.CASE, caseNumber);
                        }
                    }
                    treeStream2FlowGraph(threads, null);
                }
                // Skip the current op_endOfList operator:
                inputTreeStream.readOperator();
                if (defaultTreeCase != null) {
                    tlListTreeCase = tlListTreeCase.placdl(defaultTreeCase);
                }
                switchTestInstruction.setChild(ILUtils.build(ILLang.op_switchCases, hdListTreeCase.tail), 2);
                // Add all the waiting threads that have come through
                // op_break statements into the final threads :
                threads.concat(switchJumpInfo.threadsToAfter);
                if (!foundDefault) {
                    threads.add(switchBlock, FGConstants.CASE, FGConstants.NOMATCH);
                }
                nestedControlStack = nestedControlStack.tail;
                // Restore the previous SCInstruction:
                curSCInstruction = oldSCInstruction;
                break;
            }
            case ILLang.op_parallelLoop:
            case ILLang.op_parallelRegion: {
                Operator parallelOperator = inputTreeStream.readOperator();
                position = inputTreeStream.position();
                Tree dialectNameTree = inputTreeStream.readTree();
                if (ILUtils.isIdent(dialectNameTree, "CUDA", true)) {
                    callGraph.hasCuda = true ;
                }
                Tree parallelInfoTree = inputTreeStream.readTree(toLineNo);
                Tree regionHeader = ILUtils.build(parallelOperator.code, dialectNameTree, parallelInfoTree);
                Instruction regionHeaderInstruction =
                    addInstructionTreeAndPppLines(regionHeader, toLineNo, postComments2, pendingComments2, threads, false);
                ILUtils.setPosition(regionHeader, position);
                Block regionHeaderBlock = threads.block();
                threads.empty();
                threads.add(regionHeaderBlock, FGConstants.NO_TEST, FGConstants.NO_TEST);
                // Store the previous SCInstruction:
                Instruction oldSCInstruction = curSCInstruction;
                curSCInstruction = regionHeaderInstruction;
                // Push the new parallel control:
                curParallelControls = new TapList<>(regionHeaderInstruction, curParallelControls);
                treeStream2FlowGraph(threads, null);
                // Pop  the new parallel control:
                curParallelControls = curParallelControls.tail;
                // Restore the previous SCInstruction:
                curSCInstruction = oldSCInstruction;
                break;
            }
            case ILLang.op_loop: {
                FGThreads bodyThreads = new FGThreads();
                Tree loopNameTree;
                String loopName;
                String loopLabel;
                Tree loopExpression;
                Tree loopInstruction;
                inputTreeStream.readOperator();
                position = inputTreeStream.position();
                // OPT_NAME comes first. Keep it if present:
                loopNameTree = inputTreeStream.readTree();
                loopName = ILUtils.getIdentString(loopNameTree);
                // Then comes OPT_LABEL. Keep it if present:
                loopLabel = ILUtils.getIdentString(inputTreeStream.readTree());
                // Then comes the loop control.
                // Build a new headerBlock, and converge threads to it:
                if (allBlocks.tail == null) {
                    instructionTree = ILUtils.build(ILLang.op_none);
                    addInstructionTreeAndPppLines(instructionTree, null, postComments2, pendingComments2, threads, true);
                }
                // If this loop header is marked II-loop, make sure there is a plain block
                // before it, containing e.g. a No-Op with a II-loop directive.
                // This is needed to solve problems in data-flow analysis of nested
                // code fragments.
                if (threads.isNotEmpty() && (containsIIloop(pendingComments2.first) || containsIIloop(pendingComments2.second))) {
                    threads.convergeToInstr(new Instruction(ILUtils.build(ILLang.op_none), curSCInstruction, 0),
                            allBlocks, unit, curSymbolTable, curParallelControls, traceOn);
                }
                HeaderBlock headerBlock = new HeaderBlock(null, curParallelControls, allBlocks);
                loopExpression = inputTreeStream.readTree(toLineNo);
                loopInstruction = ILUtils.build(ILLang.op_loop, loopNameTree, null, loopExpression, null);
                ILUtils.setPosition(loopInstruction, position);
                newInstr = new Instruction(loopInstruction, curSCInstruction, toLineNo.get());
                attachComments(newInstr, loopInstruction, null, pendingComments2);
                headerBlock.addInstrTl(newInstr);
                SymbolTable prevSymbolTable = curSymbolTable ;
                if (loopExpression.opCode() == ILLang.op_for
                        && loopExpression.down(1).opCode() == ILLang.op_varDeclaration) {
                    headerBlock.declaresItsIterator = true;
                    curSymbolTable = new SymbolTable(curSymbolTable);
                    curSymbolTable.setShortName("Local forvar SymbolTable on top of " + prevSymbolTable.shortName());
                    curSymbolTable.unit = unit;
                    unit.addDerivedSymbolTable(curSymbolTable);
                    newInstr = new Instruction(loopExpression.down(1));
                    curSymbolTable.addVarDeclaration(loopExpression.down(1), newInstr, true, null);
                    curSymbolTable.declarationsBlock = headerBlock;
                }
                headerBlock.symbolTable = curSymbolTable;
                threads.convergeToBlock(headerBlock, false, traceOn);
                // Store the previous SCInstruction:
                Instruction oldSCInstruction = curSCInstruction;
                curSCInstruction = newInstr;
                // Open scope of current loop body (for exit's and cycle's):
                FlowGraphBuilder.ControlJumpInfo loopJumpInfo = new FlowGraphBuilder.ControlJumpInfo(ILLang.op_loop, loopLabel, loopName);
                nestedControlStack = new TapList<>(loopJumpInfo, nestedControlStack);
                // Build the flow-graph for the "body" part:
                bodyThreads.add(headerBlock, FGConstants.LOOP, FGConstants.NEXT);
                treeStream2FlowGraph(bodyThreads, null);
                // Threads after the end of body, loop back to headerBlock:
                bodyThreads.convergeToBlock(headerBlock, true, traceOn);
                // Threads going to a "cycle" loop back to headerBlock:
                loopJumpInfo.threadsToCycle.convergeToBlock(headerBlock, true, traceOn);
                // Build the final threads after the loop:
                threads.empty();
                threads.concat(loopJumpInfo.threadsToAfter);
                if (loopExpression.opCode() != ILLang.op_none) {
                    threads.add(headerBlock, FGConstants.LOOP, FGConstants.EXIT);
                }
                // Close the scope of the current loop body:
                nestedControlStack = nestedControlStack.tail;
                // Restore the previous SCInstruction:
                curSCInstruction = oldSCInstruction;
                curSymbolTable = prevSymbolTable;
                break;
            }
            case ILLang.op_exit:
                inputTreeStream.readOperator();
                // First and only child is the loop reference.
                // Find the loop, and declare this new exit for it:
                declareLoopBreak(inputTreeStream.readTree(), threads);
                // Empty the current threads:
                threads.empty();
                break;
            case ILLang.op_cycle:
                inputTreeStream.readOperator();
                // First and only child is the loop reference.
                // Find the loop, and declare this new cycle for it:
                // [vmp] : TODO inCycle sur les fleches
                declareLoopCycle(inputTreeStream.readTree(), threads);
                // Empty the current threads:
                threads.empty();
                break;
            case ILLang.op_break:
                // [llh 15/12/08: don't keep the break instruction in the CFG!
                inputTreeStream.readOperator();
                // Find the enclosing control; declare this exits from it:
                declareControlBreak(threads);
                // Empty the current threads:
                threads.empty();
                break;
            case ILLang.op_expressions:
                // Split these "expressions" (legal here but strange) as separate instructions:
                inputTreeStream.readOperator();
                while (inputTreeStream.seeOperator().code != ILLang.op_endOfList) {
                    treeStream2FlowGraph(threads, null);
                }
                // Skip the op_endOfList operator:
                inputTreeStream.readOperator();
                break;
            case ILLang.op_directive:
            case ILLang.op_constructorCall:
            case ILLang.op_assign:
            case ILLang.op_address:
            case ILLang.op_arrayTriplet:
            case ILLang.op_substringAccess:
            case ILLang.op_stringCst:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_ident:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_scopeAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_unary:
            case ILLang.op_bitAndAssign:
            case ILLang.op_divAssign:
            case ILLang.op_bitOrAssign:
            case ILLang.op_leftShiftAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_modAssign:
            case ILLang.op_plusAssign:
            case ILLang.op_rightShiftAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_bitXorAssign:
            case ILLang.op_ifExpression:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                instructionTree = inputTreeStream.readTree(toLineNo);
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                break;
            case ILLang.op_call:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                if (newInstr != null) {
                    checkAlternativeReturn(threads, ILUtils.getArguments(instructionTree).children());
                }
                break;
            case ILLang.op_ioCall:
                instructionTree = inputTreeStream.readTree(toLineNo);
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                checkNameEq(threads, instructionTree.down(2).children());
                break;
            case ILLang.op_format: {
                position = inputTreeStream.position() + 1;
                instructionTree = inputTreeStream.readTree();
                // Warning: not perfect: AD CHECKPOINT-START/END directives may go unseen !
                attachComments(null, instructionTree, postComments2, pendingComments2);
                String formatLabel = instructionTree.down(1).stringValue();
                labelHeap.checkFormatLabel(formatLabel, instructionTree);
                if (!TapList.containsEquals(formatLabels, formatLabel)) {
                    formatLabels = new TapList<>(formatLabel, formatLabels);
                }
                unit.addFormatTree(instructionTree);
                break;
            }
            case ILLang.op_continue:
                instructionTree = inputTreeStream.readTree();
                // If some directives are waiting, keep the continue and attach the comments to it.
                // Moreover, put the resulting Instruction in a new separate Block.
                // Otherwise, just ignore the CONTINUE statement and let FGThreads unchanged.
                if (Directive.containDirectives(pendingComments2.first)
                        || Directive.containDirectives(pendingComments2.second)) {
                    addInstructionTreeAndPppLines(instructionTree, null, postComments2, pendingComments2, threads, false);
                }
                break;
            case ILLang.op_return:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                if (newInstr != null) {
                    threads.convergeToBlock(exitBlock, false, traceOn);
                    // Empty the current threads:
                    threads.empty();
                }
                break;
            case ILLang.op_stop:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                if (newInstr != null) {
                    // After a stop, the flow of control goes to nowhere:
                    threads.empty();
                }
                break;
            case ILLang.op_file:
            case ILLang.op_declarations:
            case ILLang.op_blockStatement: {
                // Save the previous symbolTable:
                SymbolTable prevSymbolTable = curSymbolTable;
                inputTreeStream.readOperator();
                if (readySymbolTable != null) {
                    curSymbolTable = readySymbolTable;
                } else {
                    curSymbolTable = new SymbolTable(curSymbolTable);
                    curSymbolTable.setShortName("Local variables SymbolTable on top of " + prevSymbolTable.shortName());
                    curSymbolTable.unit = unit;
                    unit.addDerivedSymbolTable(curSymbolTable);
                    if (prevSymbolTable == unit.publicSymbolTable()) {
                        unit.setPrivateSymbolTable(curSymbolTable);
                    }
                }
                // Analyze all instructions and declarations in order,
                // until op_endOfList is reached:
                while (inputTreeStream.seeOperator().code != ILLang.op_endOfList) {
                    treeStream2FlowGraph(threads, null);
                }
                // Skip this op_endOfList operator:
                inputTreeStream.readOperator();
                // Restore the previous symbolTable:
                curSymbolTable = prevSymbolTable;
                break;
            }
            case ILLang.op_nameSpace: {
                // Save the previous symbolTable:
                SymbolTable prevSymbolTable = curSymbolTable;
                inputTreeStream.readOperator();
                position = inputTreeStream.position();
                Tree nameSpaceTree = inputTreeStream.readTree(toLineNo);
                Tree nameSpaceInstruction = ILUtils.build(ILLang.op_nameSpace, nameSpaceTree);
                ILUtils.setPosition(nameSpaceInstruction, position);
                addInstructionTreeAndPppLines(nameSpaceInstruction, toLineNo, null, pendingComments2, threads, true);

                if (TapEnv.traceInputIL()) {
                    ++fgbDepth;
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("--> [" + fgbDepth + "] nameSpace " + nameSpaceTree);
                }

                curSymbolTable = curSymbolTable.getSetNameSpaceDecl(nameSpaceTree).symbolTable;

                // Analyze nameSpace body:
                treeStream2FlowGraph(threads, curSymbolTable);
                // Restore the previous symbolTable:
                curSymbolTable = prevSymbolTable;
                if (TapEnv.traceInputIL()) {
                    for (int jj = 0; jj < fgbDepth; ++jj) {
                        TapEnv.printOnTrace(" ");
                    }
                    TapEnv.printlnOnTrace("<-- [" + fgbDepth + "] nameSpace " + nameSpaceTree);
                    --fgbDepth;
                }
                break;
            }
            case ILLang.op_varDeclaration:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_recordType:
            case ILLang.op_unionType:
            case ILLang.op_enumType: {
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                if (newInstr.isInStdCInclude()) {
                    TapEnv.switchOnCombineDeclMessages(false);
                }
                SymbolTable hostSymbolTable = curSymbolTable;
                if (hostSymbolTable.unit == null && TapEnv.currentTranslationUnitSymbolTable() != null && newInstr.isInStdCInclude()) {
                // Special case : if this declaration is contained in a standard C include, we may want to put it directly
                // at the language root level (to save space?) although this might be incorrect if some variables are static?
                    hostSymbolTable = callGraph.languageRootSymbolTable(language);
                }
                ToBool addInstruction = new ToBool(true);
                switch (instructionTree.opCode()) {
                    case ILLang.op_varDeclaration:
                        hostSymbolTable.addVarDeclaration(instructionTree, newInstr, true, addInstruction);
                        break;
                    case ILLang.op_varDimDeclaration:
                        hostSymbolTable.addVarDimDeclaration(instructionTree, newInstr);
                        break;
                    case ILLang.op_typeDeclaration:
                        hostSymbolTable.addTypeDeclaration(instructionTree.down(1), instructionTree.down(2), newInstr, null);
                        break;
                    case ILLang.op_constDeclaration:
                        hostSymbolTable.addConstDeclaration(instructionTree, newInstr);
                        break;
                    case ILLang.op_external:
                        hostSymbolTable.addExternalOrForwardDeclaration(instructionTree, newInstr, false);
                        break;
                    case ILLang.op_recordType:
                    case ILLang.op_unionType:
                    case ILLang.op_enumType:
                        // e.g. in C, one can declare these types alone (not inside a varDecl nor a typeDecl)
                        // and this is legal because these types come with their own name (e.g. "struct ttt")
                        hostSymbolTable.addTypeDeclaration(null, instructionTree, newInstr, null);
                        break;
                    default:
                        break;
                }
                if (!addInstruction.get() || TapEnv.inStdCIncludeFile()) {
                    threads.block().removeInstr(newInstr);
                }
                TapEnv.switchOnCombineDeclMessages(true);
                break;
            }
            case ILLang.op_implicit: {
                Tree implicitTree = inputTreeStream.readTree();
                curSymbolTable.basisSymbolTable().addImplicit(implicitTree, curSymbolTable);
                curSymbolTable.addImplicit(implicitTree, curSymbolTable);
                if (TapEnv.inIncludeFile()) {
                    // sinon Multiple implicit statements
                    TapEnv.fileWarning(TapEnv.MSG_WARN, implicitTree, "(DD24) "
                            + ILUtils.toString(TapEnv.currentIncludeInstruction().tree)
                            + " contains an implicit declaration");
                    TapEnv.currentIncludeInstruction().tree = null;
                }
                break;
            }
            case ILLang.op_equivalence:
            case ILLang.op_common:
                unit.fortranStuff().activateArrayAccessOrCall(false);
                instructionTree = inputTreeStream.readTree(toLineNo);
                unit.fortranStuff().activateArrayAccessOrCall(true);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                unit.fortranStuff().collectFortranCEDSForLater(newInstr, curSymbolTable);
                if (instructionTree.opCode() == ILLang.op_common) {
                    String commonName = (instructionTree.down(1).opCode()==ILLang.op_none ? "//" : instructionTree.down(1).stringValue()) ;
                    if (!TapList.containsEquals(callGraph.usedCommonNames, commonName)) {
                        callGraph.usedCommonNames = new TapList<>(commonName, callGraph.usedCommonNames) ;
                    }
                }
                break;
            case ILLang.op_data:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                curSymbolTable.addData(newInstr);
                unit.fortranStuff().collectFortranCEDSForLater(newInstr, curSymbolTable);
                break;
            case ILLang.op_save:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                curSymbolTable.addSave(newInstr);
                unit.fortranStuff().collectFortranCEDSForLater(newInstr, curSymbolTable);
                break;
            case ILLang.op_nameList:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                curSymbolTable.addNameList(newInstr);
                break;
            case ILLang.op_intrinsic:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                curSymbolTable.addIntrinsicDeclaration(instructionTree, newInstr);
                break;
            case ILLang.op_useDecl:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                unit.collectImportForLater(newInstr, curSymbolTable);
                break;
            case ILLang.op_interfaceDecl: {
                // Start machinery to read(i.e. build) the coming tree in parallel with
                // its analysis that adds the declarations into the SymbolTable:
                inputTreeStream.pushTreeReader();
                TapList<Unit> units = unit.addInterfaceDecl(inputTreeStream, callGraph, curSymbolTable);
                // il faut garder la signature des interfaces
                if (units != null) {
                    callGraph.setInterfaceUnits(TapList.append(callGraph.interfaceUnits(), units));
                }
                instructionTree = inputTreeStream.popTreeReader();
                newInstr = addInstructionTreeAndPppLines(instructionTree, null, postComments2, pendingComments2, threads, true);
                newInstr.setInterfaceUnits(units);
                break;
            }
            case ILLang.op_friendsDeclaration:
                instructionTree = inputTreeStream.readTree(toLineNo);
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                // TODO: we may want to take into account this "friendsDeclaration" somehow
                break;
            case ILLang.op_accessDecl:
                instructionTree = inputTreeStream.readTree(toLineNo);
                // on garde tous les accessDecl
                newInstr = addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, true);
                TapList<String> modifiers = null;
                Tree[] modifIdents;
                if (instructionTree.down(1).isList()) {
                    modifIdents = instructionTree.down(1).children();
                } else {
                    modifIdents = new Tree[]{instructionTree.down(1)};
                }
                for (int i = modifIdents.length - 1; i >= 0; --i) {
                    String modifierStr = ILUtils.getIdentString(modifIdents[i]);
                    if (modifierStr != null) {
                        modifiers = new TapList<>(modifierStr, modifiers);
                    }
                }

                // IL syntax is dirty around here: the 2nd child of accessDecl should be an (a list of) identifier
                // of already declared symbols, for which this accessDecl is simply a modifier. However it seems people
                // have sometimes placed as 2nd child a full declaration e.g. a type. We must cope with this here!
                if (instructionTree.down(2).opCode() == ILLang.op_typeDeclaration) {
                    curSymbolTable.addTypeDeclaration(
                            instructionTree.down(2).down(1), instructionTree.down(2).down(2), newInstr, modifiers);
                } else if (instructionTree.down(2).opCode() == ILLang.op_varDeclaration) {
                    curSymbolTable.addVarDeclaration(instructionTree.down(2), newInstr, true, null);
                }

                if (instructionTree.down(1).opCode() == ILLang.op_accessDecl
                    && ILUtils.isIdent(instructionTree.down(1).down(1), "bind", false)) {
                    if (instructionTree.down(2).opCode() == ILLang.op_typeDeclaration) {
                        unit.fortranStuff().addBindCDecl(curSymbolTable, instructionTree);
                    } else {
                        unit.fortranStuff().collectFortranCEDSForLater(newInstr, curSymbolTable);
                    }
                }
                if (instructionTree.down(2).children().length == 0) {
                    if (unit.fortranStuff() != null) {
                        unit.fortranStuff().addPublicPrivateDecl(instructionTree.down(1).stringValue().toUpperCase(), instructionTree);
                    }
                    unit.privacy = instructionTree.down(1).stringValue().toLowerCase();
                } else {
                    boolean delayedDeclaration =
                            TapList.containsEquals(modifiers, "private") || TapList.containsEquals(modifiers, "public");
                    curSymbolTable.declareAllSymbolsIn(instructionTree.down(2), instructionTree.down(2),
                            callGraph.usedCommonNames, instructionTree.down(1), SymbolTableConstants.SYMBOL,
                            delayedDeclaration, false, newInstr);
                }
                if (instructionTree.down(1).opCode() == ILLang.op_ident
                        && instructionTree.down(1).stringValue().equals("allocatable")) {
                    curSymbolTable.addAllocatableDeclaration(instructionTree, newInstr);
                }
                break;
            case ILLang.op_none:
                inputTreeStream.readOperator();
                addInstructionTreeAndPppLines(null, toLineNo, postComments2, pendingComments2, threads, false);
                break;
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_pointerAssign:
            case ILLang.op_nullify:
                instructionTree = inputTreeStream.readTree(toLineNo);
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                TapEnv.warningFortran77WithFortran90Features(unit);
                break;
            default:
                TapEnv.toolWarning(-1, "(FlowGraphBuilder.treeStream2FlowGraph) Unexpected operator: "
                        + inputTreeStream.seeOperator().name);
                instructionTree = inputTreeStream.readTree(toLineNo);
                addInstructionTreeAndPppLines(instructionTree, toLineNo, postComments2, pendingComments2, threads, false);
                break;
        }
        if (endOfRegionDirectives!=null) {
            instructionTree = ILUtils.build(ILLang.op_none) ;
            addInstructionTree(instructionTree, toLineNo, postComments2, new TapPair<>(endOfRegionDirectives, null), threads, false);
        }
        //If remaining postComments could not be attached, then append them to the tail of the "pending", and keep them waiting:
        if (postComments2.first != null) {
            pendingComments2.first = TapList.append(pendingComments2.first, postComments2.first.childrenList());
            postComments2.first = null;
        }
        if (postComments2.second != null) {
            pendingComments2.second = TapList.append(pendingComments2.second, postComments2.second.childrenList());
            postComments2.second = null;
        }
    }

    private TapList<Tree> extractEndDirectivesFromPostComments(TapPair<Tree, Tree> postComments2) {
        TapList<Tree> toContents, inContents ;
        boolean changed ;
        TapList<Tree> extracted = null ;
        if (postComments2.first!=null) {
            toContents = new TapList<>(null, postComments2.first.childrenList()) ;
            inContents = toContents ;
            changed = false ;
            while (inContents.tail!=null) {
                if (Directive.isEndOfRegionDirective(inContents.tail.head)) {
                    extracted = new TapList<>(inContents.tail.head, extracted) ;
                    changed = true ;
                    inContents.tail = inContents.tail.tail;
                } else {
                    inContents = inContents.tail ;
                }
            }
            if (changed) {
                postComments2.first =
                    (toContents.tail==null ? null
                     : ILUtils.build(postComments2.first.opCode(), toContents.tail)) ;
            }
        }
        if (postComments2.second!=null) {
            toContents = new TapList<>(null, postComments2.second.childrenList()) ;
            inContents = toContents ;
            changed = false ;
            while (inContents.tail!=null) {
                if (Directive.isEndOfRegionDirective(inContents.tail.head)) {
                    extracted = new TapList<>(inContents.tail.head, extracted) ;
                    changed = true ;
                    inContents.tail = inContents.tail.tail;
                } else {
                    inContents = inContents.tail ;
                }
            }
            if (changed) {
                postComments2.second =
                    (toContents.tail==null ? null
                     : ILUtils.build(postComments2.second.opCode(), toContents.tail)) ;
            }
        }
        return TapList.nreverse(extracted) ;
    }

    private void extractPppLines(TapPair<TapList<Tree>, TapList<Tree>> pendingComments2, TapPair<Tree, Tree> postComments2,
                                 TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPrePppLines,
                                 TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPostPppLines) {
        TapList<TapTriplet<TapList<Tree>, Tree, Tree>> tlPrePppLines = toPrePppLines ;
        TapList<Tree> hdTrueComments = new TapList<>(null, null) ;
        TapList<Tree> tlTrueComments = hdTrueComments ;
        TapList<Tree> pendingComments = pendingComments2.first ;
        while (pendingComments!=null) {
            if (pendingComments.head.opCode()==ILLang.op_pppLine && isAPostIncludeLine(pendingComments.head)) {
                tlPrePppLines =
                    tlPrePppLines.placdl(
                        new TapTriplet<TapList<Tree>, Tree, Tree>(hdTrueComments.tail, pendingComments.head, null)) ;
                hdTrueComments.tail = null ;
                tlTrueComments = hdTrueComments ;
            } else {
                tlTrueComments = tlTrueComments.placdl(pendingComments.head);
            }
            pendingComments = pendingComments.tail ;
        }
        pendingComments2.first = hdTrueComments.tail ;
        if (postComments2!=null && postComments2.first!=null) {
            TapList<TapTriplet<TapList<Tree>, Tree, Tree>> tlPostPppLines = toPostPppLines ;
            TapTriplet<TapList<Tree>, Tree, Tree> lastTriplet = null ;
            hdTrueComments.tail = null ;
            tlTrueComments = hdTrueComments ;
            pendingComments = ILUtils.collectCommentStrings(postComments2.first) ;
            while (pendingComments!=null) {
                if (pendingComments.head.opCode()==ILLang.op_pppLine && isAPostIncludeLine(pendingComments.head)) {
                    lastTriplet = new TapTriplet<>(hdTrueComments.tail, pendingComments.head, null) ;
                    tlPostPppLines = tlPostPppLines.placdl(lastTriplet) ;
                    hdTrueComments.tail = null ;
                    tlTrueComments = hdTrueComments ;
                } else {
                    tlTrueComments = tlTrueComments.placdl(pendingComments.head);
                }
                pendingComments = pendingComments.tail ;
            }
            Tree newLastPost = (hdTrueComments.tail!=null ? ILUtils.build(ILLang.op_comments, hdTrueComments.tail) : null) ;
            if (lastTriplet!=null) {
                postComments2.first = null ;
                lastTriplet.third = newLastPost ;
            } else {
                postComments2.first = newLastPost ;
            }
        }
    }

    private boolean isAPostIncludeLine(Tree pppLine) {
        return (pppLine.length()>=2
                && pppLine.down(1).opCode()==ILLang.op_intCst
                && pppLine.down(2).opCode()==ILLang.op_stringCst) ;
    }

    private Tree manageIncludeLine(Tree pppLine, ToInt toLineNo,
                                   TapPair<TapList<Tree>, TapList<Tree>> preComments2,
                                   FGThreads threads, boolean isDeclaration) {
// System.out.println("MANAGE "+pppLine+" INCLUDESTACK: "+TapEnv.includeFileStack()) ;
        Tree markerTree = null ;
        Tree[] args = pppLine.children() ;
        int lineNo = (args.length>0 ? args[0].intValue() : -1) ;
        String fileName = (args.length>1 ? TapEnv.filterIncludeFileName(args[1].stringValue(), false, false) : null) ;
        boolean entering = false ;
        boolean returning = false ;
        boolean systemHeader = false ;
        for (int rk=2 ; rk<args.length ; ++rk) {
            int argValue = args[rk].intValue() ;
            if (argValue==1)
                entering = true ;
            else if (argValue==2)
                returning = true ;
            else if (argValue==3)
                systemHeader = true ;
        }
        if (fileName!=null) {
            if (fileName.endsWith("<command-line>") || fileName.endsWith("<built-in>")
                || fileName.endsWith("stdc-predef.h")) {
                // A number of cases to ignore, e.g. stuff added by cpp...
// System.out.println(" PPPLINE IGNORED SPECIAL:"+fileName+" "+entering+returning) ;
            } else if (entering) {
                // we are entering into a new nested include level, and
                //  "fileName" is the name of this new include we are entering into.
                // Before going into the include, possibly emit an empty instruction
                // to hold preComments, because they do not belong to the include:
                addInstructionTree(null, toLineNo, new TapPair<>(null, null), preComments2, threads, isDeclaration) ;
                // then emit the "entry-into-include" instruction:
                TapEnv.pushIncludeFile(fileName) ;
                markerTree = ILUtils.build(ILLang.op_include, fileName) ;
                TapEnv.updateTapEnvInclude(markerTree) ;
// System.out.println(" UNDERSTOOD AS MARKERTREE: "+markerTree) ;
            } else if (returning) {
                // we are exiting the current include file, and "fileName" is
                //  the name of the enclosing file we are returning into.
                // Before returning from include, possibly emit an empty instruction
                // to hold preComments, because they belong to the include:
                addInstructionTree(null, toLineNo, new TapPair<>(null, null), preComments2, threads, isDeclaration) ;
                // then do the "return-from-include":
                TapEnv.popIncludeFile() ;
                TapEnv.updateTapEnvInclude(null) ;
// System.out.println(" UNDERSTOOD AS MARKERTREE: "+markerTree) ;
            } else {
                // all other cases must be ignored.
// System.out.println(" PPPLINE IGNORED:"+fileName+" "+entering+returning) ;
            }
        }
        return markerTree ;
    }

    /** Split the pending- and post- comments that are scheduled to be given/attached to
     * instructionTree, extracting the pppLines (i.e. # lines that are added into the
     * [fc]pp preprocessed file) that may be in these comments.
     * Each pppLine becomes a new op_include Instruction,
     * which is added to the Flow Graph (together with its possible comments) before or after
     * the "main" Instruction which is added for instructionTree. This main Instruction for
     * instructionTree is given the remaining comments that were not given to one pppLine Instruction. */
    private Instruction addInstructionTreeAndPppLines(Tree instructionTree, ToInt toLineNo,
                                                      TapPair<Tree, Tree> postComments2,
                                                      TapPair<TapList<Tree>, TapList<Tree>> pendingComments2,
                                                      FGThreads threads, boolean isDeclaration) {
        // Triplet meaning: (for a given (pppLine) Tree) (in the order they should appear in code, i.e.:)
        //  1st: the pendingComments for it ; 2nd: the pppLine Tree itself; 3rd: the postComments for it.
        TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPrePppLines = new TapList<>(null, null) ;
        TapList<TapTriplet<TapList<Tree>, Tree, Tree>> toPostPppLines = new TapList<>(null, null) ;
// System.out.println("EXTRACT PPPLINES AROUND:"+instructionTree) ;
// System.out.println("  PRECEDED     BY "+(pendingComments2==null?"nullpair":pendingComments2.first)) ;
// System.out.println("  AND FOLLOWED BY "+(postComments2==null?"nullpair":postComments2.first)) ;
        extractPppLines(pendingComments2, postComments2, toPrePppLines, toPostPppLines) ;
// System.out.println("  FINDS PRE:"+toPrePppLines.tail) ;
// System.out.println("   AND POST:"+toPostPppLines.tail) ;
        TapTriplet<TapList<Tree>, Tree, Tree> pppLineAndComments ;
        TapPair<Tree, Tree> tmpPost2 = new TapPair<>(null, null) ;
        TapPair<TapList<Tree>, TapList<Tree>> tmpPending2 = new TapPair<>(null, null) ;
        Tree tapIncludeTree ;
        toPrePppLines = toPrePppLines.tail ;
        while (toPrePppLines!=null) {
            pppLineAndComments = toPrePppLines.head ;
            tmpPending2.first = pppLineAndComments.first ;
            tmpPost2.first = pppLineAndComments.third ;
            tapIncludeTree = manageIncludeLine(pppLineAndComments.second, toLineNo, tmpPending2, threads, isDeclaration) ;
            addInstructionTree(tapIncludeTree, toLineNo, tmpPost2, new TapPair<TapList<Tree>, TapList<Tree>>(null, null), threads, isDeclaration) ;
            toPrePppLines = toPrePppLines.tail ;
        }
        Instruction mainInstr =
            addInstructionTree(instructionTree, toLineNo, postComments2, pendingComments2, threads, isDeclaration);
        toPostPppLines = toPostPppLines.tail ;
        while (toPostPppLines!=null) {
            pppLineAndComments = toPostPppLines.head ;
            tmpPending2.first = pppLineAndComments.first ;
            tmpPost2.first = pppLineAndComments.third ;
            tapIncludeTree = manageIncludeLine(pppLineAndComments.second, toLineNo, tmpPending2, threads, isDeclaration) ;
            addInstructionTree(tapIncludeTree, toLineNo, tmpPost2, new TapPair<TapList<Tree>, TapList<Tree>>(null, null), threads, isDeclaration) ;
            toPostPppLines = toPostPppLines.tail ;
        }
        return mainInstr ;
    }

    /**
     * Add instruction "instructionTree" at the sequel (denoted by "threads") of the Flow Graph being built.
     *
     * @param isDeclaration true iff the new instruction is a declaration (as opposed to an executable statement)
     * @return new Instruction built to contain "instructionTree".
     */
    private Instruction addInstructionTree(Tree instructionTree, ToInt toLineNo,
                                           TapPair<Tree, Tree> postComments2,
                                           TapPair<TapList<Tree>, TapList<Tree>> pendingComments2,
                                           FGThreads threads, boolean isDeclaration) {

        // Don't keep "statement function" declaration instructions:
        if (instructionTree!=null && instructionTree.opCode()==ILLang.op_statementFunctionDeclaration)
            instructionTree = null ;

        // If instructionTree is null, but we have comments to attach, make it an op_none:
        if (instructionTree==null
            && (pendingComments2.first!=null || pendingComments2.second!=null
                || postComments2.first!=null || postComments2.second!=null))
            instructionTree = ILUtils.build(ILLang.op_none) ;

        Instruction newInstr = null ;
        if (instructionTree!=null) {
// System.out.println("ADD INSTRUCTION "+instructionTree+" PRECEDED WITH PENDING COMMENTS:"+pendingComments2+", FOLLOWED BY POST:"+postComments2+(TapEnv.inIncludeFile()?", AND IN INCLUDE CONTEXT:"+TapEnv.currentIncludeInstruction():", IN NO INCLUDE")) ;

            newInstr = new Instruction(instructionTree, curSCInstruction,
                                       ((toLineNo==null || toLineNo.get()==-1) ? 0 : toLineNo.get()));
// if (postComments2!=null && (postComments2.first!=null || postComments2.second!=null)) {
//  System.out.println("GOING TO ATTACH POSTCOMMENTS "+postComments2+" AND PENDING PRECOMMENTS "+pendingComments2) ;
//  System.out.println(" TO INSTRUCTION "+newInstr+" (TREE:"+instructionTree+")") ;
// }
            attachComments(newInstr, instructionTree, postComments2, pendingComments2);
            if (whereMask != null) {
                newInstr.setWhereMask(whereMask);
            }
            if (threads.isNotEmpty()) {
                threads.convergeToInstr(newInstr, allBlocks, unit, curSymbolTable, curParallelControls, traceOn);
            } else if (!isDeclaration) {
                unit.addDeadTree(instructionTree);
            } else {
                // [llh 10Aug2018] apparently this is still useful, e.g. for units CONTAIN'ed in a module.
                // but this is an old mechanism that should be fused with modules now having a Flow Graph!
                unit.addNonFlowInstruction(newInstr);
            }
            if (!isDeclaration && instructionTree.opCode() != ILLang.op_include && instructionTree.opCode() != ILLang.op_none && (unit.isModule() || unit.isClass())) {
                TapEnv.fileWarning(15, instructionTree, "(DD32) A purely declarative unit should not contain an instruction");
            }
            if (TapEnv.inIncludeFile()) {
                newInstr.setFromInclude(TapEnv.currentIncludeInstruction());
                if (isDeclaration) {
                    includeContainsOneDeclaration = true;
                }
                if (!newInstr.isInStdInclude() && includeContainsOneDeclaration
                        && !(isDeclaration
                             || instructionTree.opCode()==ILLang.op_include
                             || instructionTree.opCode()==ILLang.op_none
                             || (unit.isFortran() && FortranStuff.isStatementFunctionDecl(curSymbolTable, instructionTree)))) {
                    // We currently refuse that a "user" include file contains both declarations and instructions.
                    // If this happens, this include file is inlined in-place:
                    if (TapEnv.currentIncludeInstruction().tree != null) {
                        TapEnv.fileWarning(15, instructionTree, "(DD34) Include file was inlined: "
                                + TapEnv.currentIncludeInstruction().tree.stringValue());
                        TapEnv.currentIncludeInstruction().tree = null;
                    }
                    TapEnv.resetIncludeManager();
                }
            } else {
                includeContainsOneDeclaration = false;
            }

        }
        return newInstr;
    }

    private boolean containsIIloop(TapList<Tree> comments) {
        boolean found = false;
        while (!found && comments != null) {
            found = Directive.isIILoopDirective(comments.head);
            comments = comments.tail;
        }
        return found;
    }

    /**
     * Adds "threads" to the list of the threads that must break out of the
     * closest enclosing control structure.
     */
    private void declareControlBreak(FGThreads threads) {
        if (nestedControlStack != null) {
            nestedControlStack.head.threadsToAfter.concat(threads);
        }
    }

    /**
     * Adds "threads" to the list of the threads that must exit from the loop
     * labelled by "labelOrName".
     */
    private void declareLoopBreak(Tree labelOrName, FGThreads threads) {
        FlowGraphBuilder.ControlJumpInfo loopJumpInfo = findLoopJumpInfo(ILUtils.getIdentString(labelOrName));
        if (loopJumpInfo != null) {
            loopJumpInfo.threadsToAfter.concat(threads);
        }
    }

    /**
     * Adds "threads" to the list of the threads that must cycle in the loop
     * labelled by "labelOrName".
     */
    private void declareLoopCycle(Tree labelOrName, FGThreads threads) {
        FlowGraphBuilder.ControlJumpInfo loopJumpInfo = findLoopJumpInfo(ILUtils.getIdentString(labelOrName));
        if (loopJumpInfo != null) {
            loopJumpInfo.threadsToCycle.concat(threads);
        }
    }

    /**
     * @return the ControlJumpInfo of the enclosing loop with the given
     * "labelOrName". If no loop matches, or if "labelOrName" is null, returns
     * the top of the stack, i.e. the deepest enclosing loop.
     */
    private FlowGraphBuilder.ControlJumpInfo findLoopJumpInfo(String labelOrName) {
        TapList<FlowGraphBuilder.ControlJumpInfo> stack = nestedControlStack;
        FlowGraphBuilder.ControlJumpInfo found = null;
        while (found == null && stack != null) {
            if (stack.head.destinationOper == ILLang.op_loop && (labelOrName == null
                    || labelOrName.equals(stack.head.label) || labelOrName.equals(stack.head.name))) {
                found = stack.head;
            }
            stack = stack.tail;
        }
        if (found == null && nestedControlStack != null) {
            found = nestedControlStack.head;
        }
        return found;
    }

    /**
     * Declares a new name that is used in the subroutine as a variable GOTO
     * label. Does nothing if already declared.
     */
    private void newLabelVar(String labelVar) {
        if (!TapList.containsEquals(labelVars, labelVar)) {
            labelVars = new TapList<>(labelVar, labelVars);
        }
    }

    /**
     * Does the propagation of assigned GOTO's. The goal of this propagation is
     * to set the minimal set of CFArrows from computed GOTO's to their possible
     * destinations. Before this propagation, we assume the initial FlowGraph
     * has no arrows leaving computed GOTO's. This analysis computes the
     * possible values of these variable GOTO labels, and sets the FGArrows
     * accordingly. This assumes that all variable GOTO labels have been
     * previously declared (by "newLabelVar").
     */
    private void analyzeAssGoto() {
        Block block;
        Block targetBlock;
        Tree instrTree;
        int numberLabelVars;
        int pos;
        int finalGotoRank;
        Block[] effectAssGoto;
        TapList<Instruction> instructions;
        TapList<Block> inAllBlocks = allBlocks.tail;
        if (labelVars != null) {
            numberLabelVars = TapList.length(labelVars);
            entryBlock.setLabelVariablesEffect(null);
            entryBlock.setFinalGotoRank(-1);
            /*
             * Initialize before propagation: for each built Block "block", in
             * whatever order,
             */
            while (inAllBlocks != null) {
                block = inAllBlocks.head;
                inAllBlocks = inAllBlocks.tail;
                /*
                 * clear the place where already propagated values will be
                 * stored:
                 */
                block.setAssGotoInfoDone(null);
                effectAssGoto = null;
                finalGotoRank = -1;
                /* for each instruction in "block", in flow order: */
                instructions = block.instructions;
                while (instructions != null) {
                    instrTree = instructions.head.tree;
                    switch (instrTree.opCode()) {
                        case ILLang.op_assignLabelVar:
                            /*
                             * when it is an assignment to a Label Variable, add its
                             * effect into "effectAssGoto", i.e. the effect of
                             * "block" on label variables.
                             */
                            if (!TapList.containsEquals(formatLabels, ILUtils.assignedLabel(instrTree))) {
                                targetBlock = labelHeap.getLabelled(ILUtils.assignedLabel(instrTree));
                                if (targetBlock == null) {
                                    TapEnv.fileWarning(TapEnv.MSG_WARN, instrTree,
                                            "(TC50) Label " + ILUtils.assignedLabel(instrTree) + " is not defined");
                                } else {
                                    if (effectAssGoto == null) {
                                        effectAssGoto = mkNewAssGotoInfo();
                                    }
                                    pos = labelVars.rankString(ILUtils.assignedLabelVar(instrTree));
                                    effectAssGoto[pos] = targetBlock;
                                }
                            }
                            break;
                        case ILLang.op_assign:
                            // check the assignment does not touch a
                            // "label variable". Otherwise emit an error message.
                            if (labelVars.rankString(ILUtils.assignedName(instrTree)) != -1) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrTree, "(TC51) Label variable " + ILUtils.assignedName(instrTree)
                                        + " must be written by an ASSIGN-TO construct");
                            }
                            break;
                        case ILLang.op_gotoLabelVar:
                            //We assume this is the "gotoLabelVar" that terminates
                            // the current block. Compute and store the
                            // corresponding rank in future labelVarInfo's.
                            pos = labelVars.rankString(ILUtils.gotoLabelVar(instrTree));
                            if (pos == -1) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, instrTree, "(TC52) Variable " + ILUtils.gotoLabelVar(instrTree)
                                        + " used in assigned-GOTO is not a label variable");
                            }
                            finalGotoRank = pos;
                            break;
                        default:
                            break;
                    }
                    instructions = instructions.tail;
                }
                block.setLabelVariablesEffect(effectAssGoto);
                block.setFinalGotoRank(finalGotoRank);
            }
            exitBlock.setLabelVariablesEffect(null);
            exitBlock.setFinalGotoRank(-1);
            // End of initialization. Propagate from the first block:
            propagate(entryBlock, mkNewAssGotoInfo());
            // Clean after propagation:
            inAllBlocks = allBlocks.tail;
            while (inAllBlocks != null) {
                block = inAllBlocks.head;
                inAllBlocks = inAllBlocks.tail;
                block.setAssGotoInfoDone(null);
                block.setLabelVariablesEffect(null);
                pos = block.getFinalGotoRank();
                if (pos != -1) {
                    if (block.flow() == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, block.lastInstr().tree, "(CF03) This assigned-GOTO to "
                                + TapList.nth(labelVars, pos) + " goes nowhere");
                    } else if (block.flow().tail == null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, block.lastInstr().tree,
                                "(CF04) This assigned-GOTO to " + TapList.nth(labelVars, pos)
                                        + " only goes to label " + block.flow().head.destination.origLabel());
                        block.removeInstr(block.lastInstr());
                        block.flow().head.test = FGConstants.NO_TEST;
                        block.flow().head.cases = null;
                    } else {
                        cleanupLabelList(block.flow(), block.lastInstr());
                    }
                    block.setFinalGotoRank(-1);
                }
            }
        }
    }

    /**
     * Removes unused labels from the (optional) list of labels given with an assigned-goto statement.
     */
    private void cleanupLabelList(TapList<FGArrow> flow, Instruction assGoto) {
        if (!ILUtils.isNullOrNoneOrEmptyList(assGoto.tree.down(2))) {
            TapIntList usedLabels = null;
            while (flow != null) {
                usedLabels = TapIntList.append(usedLabels, flow.head.cases);
                flow = flow.tail;
            }
            Tree[] labels = assGoto.tree.down(2).children();
            TapList<Tree> keptLabels = null;
            for (int i = labels.length - 1; i >= 0; --i) {
                String labelString = labels[i].stringValue();
                int labelInt = Integer.parseInt(labelString);
                if (TapIntList.contains(usedLabels, labelInt)) {
                    keptLabels = new TapList<>(labels[i], keptLabels);
                }
            }
            assGoto.tree.setChild(ILUtils.build(ILLang.op_labels, keptLabels), 2);
        }
    }

    /**
     * Creates a new AssGotoInfo, which is an array of destination Blocks, one
     * for each declared variable GOTO label, following the order of the labels
     * in labelVars.
     */
    private Block[] mkNewAssGotoInfo() {
        Block[] info;
        int i;
        int numberLabelVars;
        numberLabelVars = TapList.length(labelVars);
        info = new Block[numberLabelVars];
        for (i = numberLabelVars - 1; i >= 0; i--) {
            info[i] = null;
        }
        return info;
    }

    /*
     * @return a new AssGotoInfo, that represents the "then" combination of
     * "assGotoInfo", followed by "combineAssGotoInfo". Does not modify its
     * arguments: builds a new "copy" object.
     */
    private Block[] newCombineAssGotoInfo(Block[] assGotoInfo, Block[] combineAssGotoInfo) {
        Block[] info;
        int i;
        int numberLabelVars;
        numberLabelVars = TapList.length(labelVars);
        info = new Block[numberLabelVars];
        for (i = numberLabelVars - 1; i >= 0; i--) {
            if (combineAssGotoInfo[i] != null) {
                info[i] = combineAssGotoInfo[i];
            } else {
                info[i] = assGotoInfo[i];
            }
        }
        return info;
    }

    /*
     * @return "true" when the two AssGotoInfo's "info1" and "info2" are
     * equivalent.
     */
    private boolean eqAssGotoInfo(Block[] info1, Block[] info2) {
        if (info1 == info2) {
            return true;
        } else {
            int i = TapList.length(labelVars);
            boolean isEqual = true;
            while (--i >= 0 && isEqual) {
                isEqual = info1[i] == info2[i];
            }
            return isEqual;
        }
    }

    /*
     * Propagates the current "assGotoInfo" across "block", adds new FGArrows
     * when necessary, and then recursively propagates from successor Block's.
     */
    private void propagate(Block block, Block[] assGotoInfo) {
        Block[] blockEffect = block.getLabelVariablesEffect();
        TapList<Block[]> assGotoInfoDone = block.getAssGotoInfoDone();
        int finalGotoRank = block.getFinalGotoRank();
        Block[] newAssGotoInfo;
        Block nextBlock;
        TapList<FGArrow> arrows;
        /*
         * search in "assGotoInfoDone" to find if "assGotoInfo" has already been
         * propagated across the current "block".
         */
        while (assGotoInfoDone != null && !eqAssGotoInfo(assGotoInfo, assGotoInfoDone.head)) {
            assGotoInfoDone = assGotoInfoDone.tail;
        }
        if (assGotoInfoDone == null) {
            // then we are sure this "assGotoInfo" hasn't been
            // propagated yet. Add it into the "propagated" infos:
            block.setAssGotoInfoDone(new TapList<>(assGotoInfo, block.getAssGotoInfoDone()));
            // then actually propagate it through "block":
            if (blockEffect == null) {
                newAssGotoInfo = assGotoInfo;
            } else {
                newAssGotoInfo = newCombineAssGotoInfo(assGotoInfo, blockEffect);
            }
            // when "block" ends with a assigned GOTO,
            // add the new exiting FGArrow's :
            if (finalGotoRank != -1) {
                nextBlock = newAssGotoInfo[finalGotoRank];
                if (nextBlock == null) {
                    TapEnv.fileWarning(TapEnv.MSG_WARN, block.lastInstr().tree, "(CF05) This assigned-GOTO to "
                            + TapList.nth(labelVars, finalGotoRank) + " is possibly undefined");
                } else if (block.getFGArrowTo(nextBlock) == null) {
                    new FGArrow(block, FGConstants.ASS_GOTO, Integer.parseInt(nextBlock.origLabel()), nextBlock, false);
                }
                /*
                 * then propagate the result across exiting flow arrows. Don't
                 * propagate an info across an arrow from an op_gotoLabelVar
                 * when this info says to follow another direction!
                 */
            }
            arrows = block.flow();
            while (arrows != null) {
                nextBlock = arrows.head.destination;
                if (finalGotoRank == -1 || nextBlock == newAssGotoInfo[finalGotoRank]) {
                    propagate(nextBlock, newAssGotoInfo);
                }
                arrows = arrows.tail;
            }
        }
    }

    private void checkAlternativeReturn(FGThreads threads, Tree[] trees) {
        Tree expression;
        int arrowCase = 0;
        for (Tree tree : trees) {
            expression = tree;
            if (expression.opCode() == ILLang.op_label) {
                String label = expression.stringValue();
                // Get or set(+register) the destination BasicBlock,
                // and converge threads to it:
                arrowCase++;
                threads.reTypeThread(FGConstants.CALL, arrowCase);
                threads.convergeToBlock(labelHeap.getSetLabelled(label, allBlocks, false, expression),
                        false, traceOn);
            }
        }
        if (arrowCase > 0) {
            threads.reTypeThread(FGConstants.CALL, FGConstants.DEFAULT);
        }
    }

    private void checkNameEq(FGThreads threads, Tree[] trees) {
        String label = null;
        Tree expression;
        String nameEq1;
        int arrowCase = FGConstants.DEFAULT;
        boolean foundOneLabel = false;
        for (Tree tree : trees) {
            expression = tree;
            if (expression.opCode() == ILLang.op_nameEq) {
                nameEq1 = expression.down(1).stringValue();
                if (nameEq1.equals("err") || nameEq1.equals("end") || nameEq1.equals("eor")) {
                    label = expression.down(2).stringValue();
                    if (nameEq1.equals("err")) {
                        arrowCase = FGConstants.IO_ERR;
                    } else if (nameEq1.equals("end")) {
                        arrowCase = FGConstants.IO_END;
                    } else {
                        arrowCase = FGConstants.IO_EOR;
                    }
                    foundOneLabel = true;
                }
            }
            if (label != null) {
                // Get or set(+register) the destination BasicBlock,
                // and converge threads to it:
                threads.reTypeThread(FGConstants.IO_GOTO, arrowCase);
                threads.convergeToBlock(labelHeap.getSetLabelled(label, allBlocks, false, expression),
                        false, traceOn);
            }
            label = null;
        }
        if (foundOneLabel) {
            threads.reTypeThread(FGConstants.IO_GOTO, FGConstants.DEFAULT);
        }
    }

    /**
     * Marker to tell treeStream2FlowGraph *not* to build sub-Units,
     * because even the current Unit will be erased in the end,
     * and so far we don't know how to erase the sub-Units cleanly.
     */
    private int willBeDeletedMark;

    private void pushUnitWillBeDeleted() {
        ++willBeDeletedMark;
    }

    private void popUnitWillBeDeleted() {
        --willBeDeletedMark;
    }

    private boolean unitWillBeDeleted() {
        return willBeDeletedMark > 0;
    }

    /**
     * Holds the info needed to manage control flow jumps such as goto's,
     * exit's, break's, cycle's,... with respect to one particular control
     * structure.
     */
    private static final class ControlJumpInfo {
        private final int destinationOper;
        private final String name;
        private final String label;
        private final FGThreads threadsToCycle;
        private final FGThreads threadsToAfter;

        private ControlJumpInfo(int operator, String label, String name) {
            super();
            this.destinationOper = operator;
            this.label = label;
            this.name = name;
            this.threadsToCycle = new FGThreads();
            this.threadsToAfter = new FGThreads();
        }
    }
}
