/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;

/**
 * One boundary in an alignment Map.
 * Contains the current offset, and the list of variables
 * that start and stop at that offset.
 * Also contains information regarding the memory fragment that follows
 * this boundary and stops at next boundary. This information
 * consists of the WrapperTypeSpec in this memory fragment, and holds a place
 * for the zones that will be later defined for this fragment.
 */
public final class AlignmentBoundary {
    /**
     * The offset of the current boundary
     * wrt the enclosing alignmentMap.
     */
    public int offset;
    public boolean infiniteOffset;
    /**
     * The list of the VariableDecl's of the variables that terminate
     * here (their last cell was at the previous offset).
     */
    public TapList<VariableDecl> endVars;
    /**
     * (RESTRICTED) Reference to the variables in the next memory fragment.
     * Used to build the IL instructions that reset, save,
     * push, pop,... these variables.
     */
    public Tree accessTree;
    /**
     * (RESTRICTED) The type of whatever starts here and extends till the next boundary.
     */
    protected WrapperTypeSpec nextType = null ;
    /**
     * The WrapperTypeSpec of objects in the next memory fragment (can be array or elements type, unclear...).
     */
    public WrapperTypeSpec type;
    /**
     * The zones defined for the next memory fragment.
     */
    protected TapList zones;

    /**
     * The list of the TapTriplet VariableDecl, SymbolTable, Tree of the variables
     * that start here.
     */
    public TapList<ZoneInfoAccessElements> startVars;

    /**
     * (RESTRICTED) If this boundary in the alignment Map defines the next
     * variable in a common, then this field holds its variableDecl.
     */
    private VariableDecl commonVariableDecl;

    /**
     * True when the zone starting at this boundary is active.
     * This field is used only for the degenerate case of MemMaps created
     * during differentiation, and that only remember activity.
     */
    protected boolean active = false ;

    /**
     * Creation of a boundary at the position "offset".
     */
    protected AlignmentBoundary(int offset) {
        super();
        this.offset = offset;
    }

    /**
     * Creation of a boundary at the position "offset".
     */
    public AlignmentBoundary(int offset, boolean infiniteOffset) {
        super();
        this.offset = offset;
        this.infiniteOffset = infiniteOffset;
    }

    /** @return true iff this AlignmentBoundary has location defined by "offset2" and "infiniteOffset2" */
    public boolean isEqual(int offset2, boolean infiniteOffset2) {
        return (infiniteOffset2 ? this.infiniteOffset
                : (!this.infiniteOffset && this.offset==offset2)) ;
    }

    /** @return true iff this AlignmentBoundary defines the same location as the given "otherBoundary" */
    public boolean isEqual(AlignmentBoundary otherBoundary) {
        return (otherBoundary.infiniteOffset ? this.infiniteOffset
                : (!this.infiniteOffset && this.offset==otherBoundary.offset)) ;
    }

    /** @return true iff this AlignmentBoundary must appear strictly before
     * location defined by "offset2" and "infiniteOffset2" */
    public boolean isBefore(int offset2, boolean infiniteOffset2) {
        return !this.infiniteOffset && (infiniteOffset2 || this.offset<offset2) ;
    }

    /** @return true iff this AlignmentBoundary must appear strictly before
     * the given "otherBoundary" */
    public boolean isBefore(AlignmentBoundary otherBoundary) {
        return !this.infiniteOffset && (otherBoundary.infiniteOffset || this.offset<otherBoundary.offset) ;
    }

    /**
     * Comparison between 2 AlignmentBoundary's.
     *
     * @return true iff offset1 is strictly smaller than offset2,
     * i.e. must appear before offset2 in the list of AlignmentBoundaries.
     */
    protected static boolean lt(int offset1, boolean infiniteOffset1,
                                int offset2, boolean infiniteOffset2) {
        return !infiniteOffset1 && (infiniteOffset2 || offset1 < offset2);
    }

    /**
     * Comparison between 2 AlignmentBoundary's.
     *
     * @return true iff offset1 is smaller than or equal to offset2,
     * i.e. must appear before or with offset2 in the list of AlignmentBoundaries.
     */
    protected static boolean le(int offset1, boolean infiniteOffset1,
                                int offset2, boolean infiniteOffset2) {
        if (infiniteOffset1) {
            return infiniteOffset2;
        } else {
            return infiniteOffset2 || offset1 <= offset2;
        }
    }

    public TapList<VariableDecl> startVarDecls() {
        TapList<VariableDecl> result = null;
        TapList<ZoneInfoAccessElements> stV = startVars;
        while (stV != null) {
            result = TapList.addLast(result, (VariableDecl)stV.head.symbolDecl);
            stV = stV.tail;
        }
        return result;
    }

    public void setCommonVariableDecl(VariableDecl variableDecl) {
        commonVariableDecl = variableDecl;
    }

    public VariableDecl commonVariableDecl() {
        return commonVariableDecl;
    }

    /**
     * Add a variable name that starts here.
     */
    public void addStartVar(VariableDecl variableDecl, SymbolTable fromSymbolTable, Tree accessTree) {
        ZoneInfoAccessElements newAccess =
            new ZoneInfoAccessElements(variableDecl, accessTree, fromSymbolTable) ;
        startVars = new TapList<>(newAccess, startVars);
    }

    /**
     * Add a variable name that ends here.
     */
    public void addEndVar(VariableDecl variableDecl, SymbolTable fromSymbolTable) {
        endVars = new TapList<>(variableDecl, endVars);
    }

    /** Remove the given variableDecl from the variables that start here */
    public void removeStartVar(VariableDecl variableDecl) {
        TapList<ZoneInfoAccessElements> toStartVars = new TapList<>(null, startVars) ;
        TapList<ZoneInfoAccessElements> inStartVars = toStartVars ;
        boolean found = false ;
        while (!found && inStartVars.tail!=null) {
            if (inStartVars.tail.head.symbolDecl==variableDecl) {
                found = true ;
                inStartVars.tail = inStartVars.tail.tail ;
            } else {
                inStartVars = inStartVars.tail ;
            }
        }
        startVars = toStartVars.tail ;
    }

    /** Remove the given variableDecl from the variables that end here */
    public void removeEndVar(VariableDecl variableDecl) {
        endVars = TapList.delete(variableDecl, endVars) ;
    }

    public WrapperTypeSpec buildNextType(int nbBytes) {
        if (type==null) return null ;
        int totalSize = type.size() ;
        if (nbBytes == -1 || totalSize==nbBytes) {
            nextType = type ;
        } else {
            TapPair<WrapperTypeSpec, ArrayDim[]> baseTypeAndDims = TypeSpec.peelArrayDimsAroundComposite(type) ;
            int baseSize = baseTypeAndDims.first.size() ;
            ArrayDim[] dimensions = {new ArrayDim(null, 1, nbBytes/baseSize, null, -1, 0, 0)} ;
            nextType = new WrapperTypeSpec(new ArrayTypeSpec(baseTypeAndDims.first, dimensions)) ;
        }
        return nextType ;
    }

    @Override
    public String toString() {
        return "("+(infiniteOffset ? "+inf" : offset)
            +"-"+endVars+"+"+startVars+(active ? " active " : " ")+type+")";
    }
}
