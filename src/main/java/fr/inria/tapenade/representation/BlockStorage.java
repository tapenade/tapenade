/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Storage object to store some piece of information
 * for each Block of a given FlowGraph.
 */
public final class BlockStorage<T> {

    /**
     * The array where the storage is made.
     */
    public final Object[] storage;

    /**
     * Creates a new BlockStorage for the Block's of the given "flowGraph".
     * Each cell is initialized to null.
     */
    public BlockStorage(Unit unit) {
        super();
        storage = new Object[unit.nbBlocks + 2]; // +2 is for the entry/exit blocks
        for (int i = storage.length - 1; i >= 0; --i) {
            storage[i] = null;
        }
    }

    /**
     * Stores "value" into the info attached to "block".
     */
    public void store(Block block, T value) {
        storage[block.rank + 1] = value;
    }

    /**
     * Retrieves the value of the info attached to "block".
     */
    public T retrieve(Block block) {
        @SuppressWarnings("unchecked")
        T result = (T) storage[block.rank + 1];
        return result;
    }

    /**
     * Prints in detail the contents of this BlockStorage, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        for (int i = 0; i < storage.length; ++i) {
            TapEnv.print((i - 1) + ">");
            TapEnv.dumpObject(storage[i]);
            TapEnv.println();
        }
    }
}
