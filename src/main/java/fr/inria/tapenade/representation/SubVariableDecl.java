
package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * A temporary pseudo-VariableDecl that declares one separate field of a true VariableDecl.
 * This is used during allocation of zones when pieces of the true variable can not be
 * allocated all together (e.g BIND(C) declaration binding a C struct and a Fortran COMMON */
public final class SubVariableDecl extends VariableDecl {
    public VariableDecl trueVariableDecl ;
    public TapIntList toField ;
    public Tree accessTree ;

    public SubVariableDecl(Tree identTree, WrapperTypeSpec typeSpec, VariableDecl trueVariableDecl, TapIntList toField, Tree accessTree) {
        super(identTree, typeSpec) ;
        this.trueVariableDecl = trueVariableDecl ;
        this.toField = toField ;
        this.accessTree = accessTree ;
    }

    /** Accumulates the given tree of zones "intTree" into the field "toField" of the "trueVariableDecl".
     * Refine structure of the existing tree of zones as needed. */
    public void accumulateSubZones(TapList intTree) {
        TapList toTrueZones = new TapList(trueVariableDecl.zones(), null) ;
        TapList inTrueZones = toTrueZones ;
        TapIntList itinerary = toField ;
        int rank ;
        while (itinerary!=null) {
            rank = itinerary.head ;
            if (inTrueZones.head==null) {inTrueZones.head = new TapList(null, null) ;}
            inTrueZones = (TapList)inTrueZones.head ;
            while (rank-- > 0) {
                if (inTrueZones.tail==null) {inTrueZones.tail = new TapList(null, null) ;}
                inTrueZones = inTrueZones.tail ;
            }
            itinerary = itinerary.tail ;
        }
        inTrueZones.head = TapList.cumulWithOper((TapList)inTrueZones.head, intTree, SymbolTableConstants.CUMUL_OR);
        trueVariableDecl.setZones((TapList)toTrueZones.head) ;
    }

    @Override
    public String toString() {
        return "SubVariableDecl "+toField+" OF "+trueVariableDecl ;
    }
}
