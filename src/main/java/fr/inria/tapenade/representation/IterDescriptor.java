/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * Represents an iteration of a given "length",
 * i.e. from "init" to "init+length-1".
 * Holds the decision whether to implement this iteration
 * with a do-loop or an array notation.
 */
public final class IterDescriptor {

    /**
     * True if this iteration will not be implemented with a DO loop.
     */
    public boolean isArray;
    /**
     * true iff this iteration ranges completely over a dimension
     * and can therefore be replaced by an implicit ":".
     */
    public boolean isImplicitCompleteDimension = true;
    /**
     * True if all uses of this iteration are in subArray mode,
     * i.e. start from an array element and continue with stride one.
     * In this case, we can use the classical Fortran
     * pseudo-pointer trick that calls F(T(i))
     * for a function declared F(T(:)).
     */
    public boolean allSubArrayAccesses;
    /**
     * If the iteration is implemented with a do-loop,
     * NewSymbolHolder of the loop index.
     */
    public NewSymbolHolder indexHolder;
    public Tree indexTree;
    public Tree defaultUse;
    /**
     * Initial value of the iteration. Convention null=0.
     */
    private Tree initTree;
    /**
     * length of the iteration, i.e. number of successive values.
     */
    private Tree lengthTree;
    /**
     * True if some other part of the tool will take care of placing a DO loop around,
     * and this IterDescriptor just represents the loop index scalar variable.
     */
    private boolean justAnIndex;
    /**
     * depth inside nested IterDescriptor loops.
     * Used to build the index name.
     */
    private int depth;
    /**
     * Info used to build error and hint messages for unknown dimensions.
     */
    private Tree hintRefTree;
    private String hintArrayNameInText = "";
    private Tree hintArrayNameInIdentTree;
    private Tree hintArrayTreeForCallSize;
    private String hintArrayNameInIdent = "";
    private int hintDim;

    /**
     * Plain creation. Creates the NewSymbolHolder for the loop index,
     * and register it at the enclosing local SymbolTable level.
     */
    public IterDescriptor(int depth, SymbolTable symbolTable, SymbolTable fwdSymbolTable,
                          Tree initIndex, Tree hintRefTree,
                          String hintArrayNameInText, String hintArrayNameInIdent,
                          Tree hintTreeSize, int hintDim, int hintNDims) {
        super();
        setDepth(depth, symbolTable, fwdSymbolTable);
        this.initTree = initIndex;
        this.hintRefTree = hintRefTree;
        this.hintArrayNameInText = hintArrayNameInText;
        this.hintArrayNameInIdent = hintArrayNameInIdent;
        this.hintArrayTreeForCallSize = hintTreeSize;
        if (symbolTable.unit != null && symbolTable.unit.isFortran()) {
            //In Fortran, dimensions are in the reverse of IL order !
            hintDim = hintNDims + 1 - hintDim;
        }
        this.hintDim = hintDim;
    }

    /**
     * Plain creation. Creates the NewSymbolHolder for the loop index,
     * and register it at the enclosing local SymbolTable level.
     */
    public IterDescriptor(int depth, SymbolTable symbolTable,
                          Tree initIndex, Tree hintRefTree,
                          String hintArrayNameInText, Tree hintArrayNameInIdentTree,
                          Tree hintTreeSize, int hintDim, int hintNDims) {
        super();
        setDepth(depth, symbolTable, null);
        this.initTree = initIndex;
        this.hintRefTree = hintRefTree;
        this.hintArrayNameInText = hintArrayNameInText;
        this.hintArrayTreeForCallSize = hintTreeSize;
        this.hintArrayNameInIdentTree = hintArrayNameInIdentTree;
        if ((hintRefTree.opCode() == ILLang.op_arrayAccess ||
                hintRefTree.opCode() == ILLang.op_arrayDeclarator) &&
                symbolTable.unit != null && symbolTable.unit.isFortran()) {
            //In Fortran, dimensions are in the reverse of IL order !
            hintDim = hintNDims + 1 - hintDim;
        }
        this.hintDim = hintDim;
    }

    /**
     * Alternative creation. Creates the IterDescriptor for an enclosing "do" iterator.
     * This implies that the loop index name is given and need not be created.
     */
    public IterDescriptor(Tree doTree) {
        super();
        this.indexTree = ILUtils.copy(doTree.down(1));
        this.defaultUse = doTree;
    }

    /**
     * Alternative creation. Creates the IterDescriptor for the "multiDir" dimension.
     */
    public IterDescriptor(NewSymbolHolder dirIndexSymbolHolder) {
        super();
        this.indexHolder = dirIndexSymbolHolder;
    }

    public void setJustAnIndex(boolean value) {
        justAnIndex = value;
    }

    public boolean justAnIndex() {
        return justAnIndex;
    }

    public void setHintArrayTreeForCallSize(Tree tree) {
        hintArrayTreeForCallSize = tree;
    }

    private void setDepth(int d, SymbolTable symbolTable, SymbolTable fwdSymbolTable) {
        TapList<NewSymbolHolder> inNestedIntIndexSymbolHolders = new TapList<>(null, null);
        depth = d;
        /* look for the SymbolTable that will declare the nested loop indexes: */
        SymbolTable localSymbolTable = symbolTable;
        while (localSymbolTable != null && localSymbolTable.nestedIntIndexSymbolHolders == null) {
            localSymbolTable = localSymbolTable.basisSymbolTable();
        }
        if (localSymbolTable == null) {
            localSymbolTable = symbolTable;
        }
        /* get/set the NewSymbolHolder of this IterDescriptor inside localSymbolTable: */
        assert localSymbolTable != null;
        if (localSymbolTable.nestedIntIndexSymbolHolders != null) {
            inNestedIntIndexSymbolHolders = localSymbolTable.nestedIntIndexSymbolHolders;
        }
        while (d > 1) {
            if (inNestedIntIndexSymbolHolders.tail == null) {
                inNestedIntIndexSymbolHolders.tail = new TapList<>(null, null);
            }
            d--;
            inNestedIntIndexSymbolHolders = inNestedIntIndexSymbolHolders.tail;
        }
        if (inNestedIntIndexSymbolHolders.head == null) {
            indexHolder = new NewSymbolHolder("ii" + depth);
            if (symbolTable != fwdSymbolTable) {
                indexHolder.setOtherDefSymbolTable(fwdSymbolTable);
            }
            indexHolder.setAsVariable(symbolTable.getTypeDecl("integer").typeSpec, null);
            inNestedIntIndexSymbolHolders.head = indexHolder;
        } else {
            indexHolder = inNestedIntIndexSymbolHolders.head;
        }
        indexHolder.declarationLevelMustInclude(symbolTable);
    }

    public Tree getInitTree() {
        return ILUtils.copy(initTree);
    }

    public void setInitTree(Tree initTree) {
        this.initTree = initTree;
    }

    public Tree getLengthTree() {
        return ILUtils.copy(lengthTree);
    }

    public void setLengthTree(Tree lengthTree) {
        this.lengthTree = lengthTree;
    }

    /**
     * Tries to keep (TODO!) the simplest possible lengthTree, between
     * the given "lengthTree" and the previously kept "this.lengthTree"
     */
    public void preciseLengthTree(Tree lengthTree) {
        if (lengthTree != null
                && (this.lengthTree == null ||
                lengthTree.opCode() == ILLang.op_intCst)) {
            this.lengthTree = lengthTree;
        }
    }

    public void preciseDefaultLengthTree(Unit targetUnit, SymbolTable symbolTable) {
        if (ILUtils.isNullOrNoneOrStar(lengthTree)) {
            if (TapEnv.get().noisize && targetUnit.isFortran() && hintArrayTreeForCallSize != null
                    && ILUtils.isAVarRef(hintArrayTreeForCallSize, symbolTable)) {
                lengthTree = ILUtils.buildCall(
                        ILUtils.build(ILLang.op_ident, "size"),
                        ILUtils.build(ILLang.op_expressions,
                                ILUtils.copy(hintArrayTreeForCallSize),
                                ILUtils.build(ILLang.op_intCst, hintDim)));
            } else {
                if (hintArrayNameInText.startsWith("#")
                        && hintArrayNameInText.endsWith("#")) {
                    hintArrayNameInText = hintArrayNameInText.substring(1, hintArrayNameInText.length() - 1);
                }
                if (hintArrayNameInIdent.isEmpty()) {
                    hintArrayNameInIdent = ILUtils.toString(hintArrayTreeForCallSize);
                    if (hintArrayNameInIdent.startsWith("#")
                            && hintArrayNameInIdent.endsWith("#")) {
                        hintArrayNameInIdent = hintArrayNameInIdent.substring(1, hintArrayNameInIdent.length() - 1);
                    }
                }
                TapEnv.fileWarning(TapEnv.MSG_WARN, hintRefTree, "(AD12) User help requested: unknown dimension " + hintDim + " of array " + hintArrayNameInText);
                String sizeName = "ISIZE" + hintDim + "OF" + hintArrayNameInIdent;
                String sizeHint = "the size of dimension " + hintDim +
                        " of array " + hintArrayNameInText;
                if (hintArrayNameInIdentTree != null) {
                    lengthTree = hintArrayNameInIdentTree;
                    lengthTree = ILUtils.build(ILLang.op_concatIdent,
                            ILUtils.build(ILLang.op_ident, "ISIZE" + hintDim + "OF"),
                            lengthTree);
                } else {
                    lengthTree = ILUtils.build(ILLang.op_ident, sizeName);
                }
                if (TapList.rassqString(sizeName, targetUnit.userHelp.tail) == null) {
                    targetUnit.userHelp.placdl(new TapPair<>(sizeHint, sizeName));
                }
            }
            // Substract offset "initTree" for F90 expressions such as A(offset:)
            if (initTree != null && targetUnit.isFortran()) {
                lengthTree = ILUtils.buildSmartAddSub(
                        ILUtils.buildSmartAddSub(
                                lengthTree,
                                -1,
                                ILUtils.copy(initTree)),
                        1,
                        ILUtils.build(ILLang.op_intCst, 1));
            }
        }
    }

    /**
     * @return an index tree that will range over this iteration space.
     * Will be an index identifier or an arrayTriplet according
     * to the choice or array notation made in "isArray".
     */
    public Tree buildIndexTree(SymbolTable usageSymboltable, Unit targetUnit) {
        if (justAnIndex) {
            return indexHolder.makeNewRef(usageSymboltable);
        } else if (isArray && targetUnit.hasArrayNotation()) {
            if (isImplicitCompleteDimension) {
                return ILUtils.build(ILLang.op_arrayTriplet);
            } else {
                preciseDefaultLengthTree(targetUnit, usageSymboltable);
                Tree lastTree =
                        ILUtils.buildSmartAddSub(
                                ILUtils.buildSmartAddSub(ILUtils.copy(initTree), 1,
                                        ILUtils.copy(lengthTree)), -1,
                                ILUtils.build(ILLang.op_intCst, 1));
                return ILUtils.build(ILLang.op_arrayTriplet,
                        ILUtils.copy(initTree),
                        lastTree,
                        ILUtils.build(ILLang.op_none));
            }
        } else if (isArray && allSubArrayAccesses) {
            return ILUtils.copy(initTree);
        } else {
            return indexHolder.makeNewRef(usageSymboltable);
        }
    }

    public Tree buildDoHeaderTree(SymbolTable usageSymboltable, Unit targetUnit, boolean directOrder) {
        if (defaultUse != null) {
            return ILUtils.copy(defaultUse);
        }
        preciseDefaultLengthTree(targetUnit, usageSymboltable);
        Tree lastTree =
                ILUtils.buildSmartAddSub(
                  ILUtils.buildSmartAddSub(ILUtils.copy(initTree),
                                           1,
                                           ILUtils.copy(lengthTree)),
                  -1,
                  ILUtils.build(ILLang.op_intCst, 1));
        if (lastTree == null) {
            lastTree = ILUtils.build(ILLang.op_intCst, 0);
        }
        if (directOrder) {
            return ILUtils.build(ILLang.op_do,
                    indexHolder.makeNewRef(usageSymboltable),
                    ILUtils.copy(initTree),
                    lastTree);
        } else {
            return ILUtils.build(ILLang.op_do,
                    indexHolder.makeNewRef(usageSymboltable),
                    lastTree,
                    ILUtils.copy(initTree),
                    ILUtils.build(ILLang.op_intCst, -1));
        }
    }

    @Override
    public String toString() {
        return "IterDescriptor "
                //+ (justAnIndex ? "justanindex " : "notjustanindex ")
                //+"@"+Integer.toHexString(hashCode())
                + indexHolder + "Notation:"+(isArray ? "array " : "index ") + initTree
                + " +>> " + lengthTree + " hintSize " + ILUtils.toString(hintArrayTreeForCallSize);
    }
}
