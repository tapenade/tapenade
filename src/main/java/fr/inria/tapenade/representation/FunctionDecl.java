/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ActivityPattern;

/**
 * The specific SymbolTable entry for a function definition.
 */
public final class FunctionDecl extends SymbolDecl {

    /**
     * for a formal parameter, its rank in the parameters list. From 1 up (special 0 for result).
     */
    public int formalArgRank = SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK;
    /**
     * for a function passed as formal parameter, its zone.
     */
    private TapList zones;
    /**
     * symbolTable containing the principal FunctionDecl of this function.
     * null corresponds to the rootSymbolTable.
     */
    public SymbolTable definitionSymbolTable;
    /**
     * C external declaration.
     */
    public Tree declarator;
    /**
     * When this is a "pointer-to-function" variable, holding a pointer to a function,
     * holds the function it is given upon initialization.
     */
    public FunctionDecl[] initFunctionDecls;
    /**
     * The function's Unit, with a hat to allow sharing by several FunctionDecl's.
     */
    private ToObject<Unit> toUnit = new ToObject<>(null);

    public FunctionDecl() {
        super();
    }

    public FunctionDecl(Tree identTree, Unit unit) {
        super(identTree, SymbolTableConstants.FUNCTION);
        toUnit.setObj(unit);
    }

    public FunctionDecl(Tree identTree, Unit unit, SymbolTable symbolTable) {
        super(identTree, SymbolTableConstants.FUNCTION);
        toUnit.setObj(unit);
        this.definitionSymbolTable = symbolTable;
    }

    public FunctionDecl(Tree identTree, Unit unit, SymbolTable symbolTable, String className) {
        super(identTree, SymbolTableConstants.FUNCTION);
        this.className = className;
        toUnit.setObj(unit);
        this.definitionSymbolTable = symbolTable;
    }

    /**
     * @return the Unit declared by this FunctionDecl.
     */
    public Unit unit() {
        return toUnit.obj();
    }

    /**
     * Set the Unit declared by this FunctionDecl.
     */
    public void setUnit(Unit unit) {
        toUnit.setObj(unit);
    }

    /**
     * Really share the hat-to-Unit that comes from otherFunctionDecl.
     */
    public void shareUnitFrom(FunctionDecl otherFunctionDecl) {
        toUnit = otherFunctionDecl.toUnit;
    }

    public void deleteUnitIfDifferent(Unit otherUnit, CallGraph callGraph) {
        // [llh] Can't figure out why we must not delete interfaces but if we do, tests crash.
        if (toUnit.obj() != null && toUnit.obj() != otherUnit && !toUnit.obj().isInterface()) {
            callGraph.deleteUnit(toUnit.obj());
            toUnit.setObj(null);
        }
    }

    @Override
    public boolean isTarget(WrapperTypeSpec modelTypeSpec) {
        if (type() == null) {
            return false;
        }
        if (modelTypeSpec == null || modelTypeSpec.wrappedType == null) {
            return true;
        }
        return modelTypeSpec.canMatchPointerTo(type());
    }

    @Override
    public WrapperTypeSpec type() {
        if (toUnit.obj()==null) return null ;
        FunctionTypeSpec functionType = toUnit.obj().functionTypeSpec() ;
        return (functionType==null ? null : new WrapperTypeSpec(functionType));
    }

    @Override
    public TapList zones() {
        return zones;
    }

    @Override
    public void setZones(TapList intTree) {
        zones = intTree;
    }

    public FunctionTypeSpec functionTypeSpec() {
        return toUnit.obj() == null ? null : toUnit.obj().functionTypeSpec();
    }

    @Override
    public void accumulateZones(TapList intTree) {
	//No accumulation: plainly set zones!
        zones = intTree;
    }

    @Override
    public int formalArgRankOrResultZero() {
	return formalArgRank ;
    }

    public WrapperTypeSpec[] argumentsTypesSpec() {
        FunctionTypeSpec functionType = functionTypeSpec() ;
        return (functionType==null ? null : functionType.argumentsTypes) ;
    }

    public WrapperTypeSpec returnTypeSpec() {
        FunctionTypeSpec functionType = functionTypeSpec() ;
        return (functionType==null ? null : functionType.returnType) ;
    }

    public boolean isStandard() {
        return toUnit.obj() != null && toUnit.obj().isStandard();
    }

    @Override
    public boolean isExternal() {
        return toUnit.obj() != null && toUnit.obj().isExternal();
    }

    public boolean isIntrinsic() {
        return toUnit.obj() != null && toUnit.obj().isIntrinsic();
    }

    public boolean isVarFunction() {
        return toUnit.obj() != null && toUnit.obj().isVarFunction();
    }

    public boolean isInterface() {
        return toUnit.obj() != null && toUnit.obj().isInterface();
    }

    public boolean isRenamed() {
        return toUnit.obj() != null && toUnit.obj().isRenamed();
    }

    public boolean isElemental() {
        return toUnit.obj() != null && toUnit.obj().isElemental();
    }

    public boolean isIntrinsicNotElemental() {
        return toUnit.obj() != null && toUnit.obj().isIntrinsicNotElemental();
    }

    public boolean isModule() {
        return toUnit.obj() != null && toUnit.obj().isModule();
    }

    /**
     * @return true if this declaration of a function matches (i.e. can be called with)
     * the types of the given actual arguments callArgTypes (and optionally matches
     * the expected type of the result callResultType, although we are
     * not sure it is reasonable to type-check on the result type?).
     */
    public boolean declarationMatchesCall(TypeSpec callResultType, TypeSpec[] callArgTypes) {
        if (toUnit.obj() != null && toUnit.obj().functionTypeSpec() != null) {
            FunctionTypeSpec funcTypeSpec = toUnit.obj().functionTypeSpec();
            funcTypeSpec = (FunctionTypeSpec) funcTypeSpec.localize(new TapList<>(null, null), new ToBool(false));
            return (funcTypeSpec.matchesCall(callResultType, callArgTypes)
                    && !FunctionTypeSpec.complexAbsAmbiguous(toUnit.obj().name(), funcTypeSpec.argumentsTypes, callArgTypes)) ;
        } else {
            return false;
        }
    }

    public boolean sameTypes(WrapperTypeSpec otherReturnType, WrapperTypeSpec[] otherArgumentsTypes) {
        if (toUnit.obj() != null && toUnit.obj().functionTypeSpec() != null) {
            return toUnit.obj().functionTypeSpec().sameTypes(otherReturnType, otherArgumentsTypes);
        } else {
            return false;
        }
    }

    /** The NewSymbolHolders for the various diff versions of this FunctionDecl */
    private TapList<TapPair<ActivityPattern, NewSymbolHolder>>[] funcDiffSymbolHolders;
    /** The NewSymbolHolder for the copy version of this FunctionDecl in the diff code */
    private NewSymbolHolder funcCopySymbolHolder = null ;

    /**
     * Puts the NewSymbolHolder "nSH" as the differentiated symbol of
     * this FunctionDecl, for the given diffSort and for the given ActivityPattern.
     * "diffSort" is here because there may be different coexisting
     * derivative symbols for a given original symbol,
     * for example F_FWD, F_BWD, F_B for a given function F.
     * "pattern" is used here only for functions, that may have
     * several different coexisting derivatives, one per calling ActivityPattern.
     * "replica" is not used, it is only for VariableDecl's.
     */
    @Override
    public void setDiffSymbolHolder(int diffSort, ActivityPattern pattern, int replica, NewSymbolHolder nSH) {
        // indices in funcDiffSymbolHolders are shift by one, because "copy" version is stored elsewhere.
        if (!this.isModule()) {
            if (funcDiffSymbolHolders == null) {
                funcDiffSymbolHolders = new TapList[TapEnv.MAX_DIFF_SORTS-1];
            }
            TapList<TapPair<ActivityPattern, NewSymbolHolder>> newSymbolHolders;
            if (diffSort == 0) { //ORIGCOPY case: don't look at pattern!
                if (funcCopySymbolHolder == null) {
                    funcCopySymbolHolder = nSH ;
                }
            } else {
                newSymbolHolders = funcDiffSymbolHolders[diffSort-1];
                TapPair<ActivityPattern, NewSymbolHolder> forThisPattern =
                        TapList.assq(pattern, newSymbolHolders);
                if (forThisPattern == null) {
                    forThisPattern = new TapPair<>(pattern, null);
                    funcDiffSymbolHolders[diffSort-1] = new TapList<>(forThisPattern, newSymbolHolders);
                }
                forThisPattern.second = nSH;
            }
        } else {
            super.setDiffSymbolHolder(diffSort, null, 0, nSH) ;
        }
    }

    /**
     * @return the NewSymbolHolder that has been stored by setDiffSymbolHolder()
     * as the differentiated symbol of this SymbolDecl,
     * for the given diffSort and for the given pattern.
     */
    @Override
    public NewSymbolHolder getDiffSymbolHolder(int diffSort, ActivityPattern pattern, int replica) {
        if (!this.isModule()) {
            if (diffSort == 0) { //ORIGCOPY case: don't look at pattern!
                return funcCopySymbolHolder ;
            } else {
                if (funcDiffSymbolHolders == null || funcDiffSymbolHolders.length < diffSort) {
                    return null;
                } else {
                    TapList<TapPair<ActivityPattern, NewSymbolHolder>> newSymbolHolders = funcDiffSymbolHolders[diffSort-1];
                    if (pattern == null) {
                        return newSymbolHolders == null ? null : newSymbolHolders.head.second;
                    } else {
                        return TapList.cassq(pattern, newSymbolHolders);
                    }
                }
            }
        } else {
            return super.getDiffSymbolHolder(diffSort, pattern, replica) ;
        }
    }

    @Override
    public boolean hasDiffSymbolHolders() {
        return funcDiffSymbolHolders != null || funcCopySymbolHolder != null || super.hasDiffSymbolHolders() ;
    }

    /**
     * For every NewSymbolHolder that is stored in this SymbolDecl (in provision of
     * the moment when this Symboldecl will receive its finalname), replace it with newNSH if it was oldNSH.
     * This is used when oldNSH is absorbed by newNSH.
     */
    @Override
    protected void replaceDiffSymbolHolder(NewSymbolHolder oldNSH, NewSymbolHolder newNSH) {
        if (!this.isModule()) {
            if (funcDiffSymbolHolders != null) {
                for (int i = funcDiffSymbolHolders.length - 1; i >= 0; --i) {
                    TapList<TapPair<ActivityPattern, NewSymbolHolder>> newSymbolHolders = funcDiffSymbolHolders[i];
                    while (newSymbolHolders != null) {
                        TapPair<ActivityPattern, NewSymbolHolder> activityToHolder = newSymbolHolders.head;
                        if (activityToHolder.second == oldNSH) {
                            activityToHolder.second = newNSH;
                        }
                        newSymbolHolders = newSymbolHolders.tail;
                    }
                }
            }
            if (funcCopySymbolHolder == oldNSH) {
                funcCopySymbolHolder = newNSH;
            }
        } else {
            super.replaceDiffSymbolHolder(oldNSH, newNSH) ;
        }
    }

    @Override
    protected void eraseDiffSymbolHolder(int diffSort) {
        if (!this.isModule()) {
//             if (funcDiffSymbolHolders != null && funcDiffSymbolHolders.length > diffSort - 1) {
//                 TapList<TapPair<ActivityPattern, NewSymbolHolder>> newSymbolHolders = funcDiffSymbolHolders[diffSort - 1];
//                 while (newSymbolHolders != null) {
//                     TapPair<ActivityPattern, NewSymbolHolder> toThisNSH = newSymbolHolders.head;
//                     if (toThisNSH.second == this) {
//                         toThisNSH.second = null;
//                     }
//                     newSymbolHolders = newSymbolHolders.tail;
//                 }
//             }
            if (diffSort == 0 && funcCopySymbolHolder != null) {
                funcCopySymbolHolder = null;
            }
        } else {
            super.eraseDiffSymbolHolder(diffSort) ;
        }
    }

    @Override
    public String toString() {
        return symbol  //+" @"+Integer.toHexString(hashCode())
            + ": "
            + (toUnit.obj() == null
               ? "unspecified function."
               : (toUnit.obj().isModule()
                  ? "module " + toUnit.obj()
                  : ((toUnit.obj().isIntrinsic() ? "intrinsic " : "")
                     + (toUnit.obj().isInterface() ? "interfaceinterface " : "")
                     + (toUnit.obj().isVarFunction() ? "varfunction " : "")
                     + (toUnit.obj().isExternal() ? "external " : "")
                     + (toUnit.obj().isElemental() ? "elemental " : "")
                     + (toUnit.obj().functionTypeSpec()==null ? "null_type" : toUnit.obj().functionTypeSpec())
                     + " function"))
               + toUnit.obj()
                    //+"@"+Integer.toHexString(toUnit.obj().hashCode())
                    +".")
            + (importedFrom==null ? "" : " (=>" + " @"+Integer.toHexString(importedFrom.first.hashCode())+" from @"+Integer.toHexString(importedFrom.second.hashCode()) + ")")
            + (!isActiveSymbolDecl() ? "" : " Active")
            + (extraInfo() == null ? "" : " X"+extraInfo())
            + ((zones != null) ? " zones:" + zones : "")
            +" depends on:"+ dependsOn()
            ;
    }
}
