/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * A simple wrapper around another type. Used to build a type (possibly empty)
 * that can later be created or modified by anyone sharing this wrapper,
 * so that everyone sharing the wrapper will see the modification.
 * In principle all wrappers may be removed when all type choices
 * are done, i.e. after type-checking.
 */
public final class WrapperTypeSpec extends TypeSpec {

    /**
     * The type inside the wrapper.
     */
    public TypeSpec wrappedType;

    /**
     * Create a new wrapper type around "wrappedType".
     */
    public WrapperTypeSpec(TypeSpec wrappedType) {
        super(SymbolTableConstants.WRAPPERTYPE);
        this.wrappedType = wrappedType;
    }

    // Make non-static? and Move to parent class !.
    public static boolean isFunctionOrPointerToFunction(WrapperTypeSpec typeSpec) {
        return isA(typeSpec, SymbolTableConstants.FUNCTIONTYPE) ||
                isA(typeSpec, SymbolTableConstants.POINTERTYPE)
                        && isA(((PointerTypeSpec) typeSpec.wrappedType).destinationType, SymbolTableConstants.FUNCTIONTYPE);
    }

    // Make non-static? and Move to parent class !.
    public static boolean isPointerOrArrayOfPointer(WrapperTypeSpec typeSpec) {
        return isA(typeSpec, SymbolTableConstants.POINTERTYPE) ||
                isA(typeSpec, SymbolTableConstants.ARRAYTYPE)
                        && isA(typeSpec.wrappedType.elementType(), SymbolTableConstants.POINTERTYPE);
    }

    /**
     * @return true if this type is deep/simple enough to
     * receive the extra vectorial dimension.
     * Example: REAL(:,:), REAL &rarr; true <br>
     * RECORD, REAL*, REAL(:,:)(:) &rarr; false.
     */
    public static boolean canReceiveVectorialDim(WrapperTypeSpec typeSpec, boolean passedArray) {
        if (typeSpec == null || typeSpec.wrappedType == null) {
            return false;
        }
        if (isA(typeSpec, SymbolTableConstants.PRIMITIVETYPE)) {
            return true;
        } else if (isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
            if (passedArray) {
                return false;
            } else {
                return canReceiveVectorialDim(typeSpec.wrappedType.elementType(),
                        true);
            }
        } else if (isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            return canReceiveVectorialDim(typeSpec.wrappedType.elementType(),
                    passedArray);
        } else {
            return false;
        }
    }

    /**
     * @return true if contains a Pointer level before reaching an array or record type.
     */
    public static boolean hasPointerOnTop(WrapperTypeSpec typeSpec) {
        if (typeSpec == null || typeSpec.wrappedType == null) {
            return false;
        }
        if (isA(typeSpec, SymbolTableConstants.POINTERTYPE)) {
            return true;
        } else if (isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            return hasPointerOnTop(typeSpec.wrappedType.elementType());
        } else {
            return false;
        }
    }

    // Move to parent class !.
    protected static boolean hasAnArrayDimension(WrapperTypeSpec typeSpec) {
        if (isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
            return true;
        } else if (isA(typeSpec, SymbolTableConstants.POINTERTYPE)
                && ((PointerTypeSpec) typeSpec.wrappedType).destinationType != null) {
            return hasAnArrayDimension(((PointerTypeSpec) typeSpec.wrappedType).destinationType);
        } else {
            return false;
        }
    }

    public static boolean isFree(WrapperTypeSpec typeSpec) {
        return typeSpec == null || typeSpec.wrappedType == null;
    }

    // Move to parent class !.
    public static boolean isNullOrVoid(WrapperTypeSpec typeSpec) {
        return typeSpec == null
                || typeSpec.wrappedType == null
                || typeSpec.wrappedType.kind() == SymbolTableConstants.VOIDTYPE;
    }

    /**
     * @return a WrapperTypeSpec built from "mainType" by replacing wherever possible
     * imprecise dimensions by fixed dimensions found in the matching complementType.
     * We assume that mainType and complementType have similar shapes except possibly for dimensions.
     * The returned result may share parts with mainType, or may even be strictly eq to it.
     */
    public static WrapperTypeSpec preciseDimensions(WrapperTypeSpec mainType, WrapperTypeSpec complementType,
                                                    TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, SymbolTable symbolTable) {
        if (mainType == null || complementType == null ||
                mainType.wrappedType == null || complementType.wrappedType == null) {
            return mainType;
        }
        TypeSpec precisedActualType =
                mainType.wrappedType.preciseDimensions(complementType.wrappedType,
                        dejaVu, symbolTable);
        if (precisedActualType == mainType.wrappedType || precisedActualType == null) {
            return mainType;
        } else {
            return new WrapperTypeSpec(precisedActualType);
        }
    }

    /**
     * When the given origType is non-differentiable (e.g. integer), returns a copied
     * WrapperTypeSpec with integers replaced with reals. Otherwise returns the same origType.
     */
    public static WrapperTypeSpec intToReal(WrapperTypeSpec origType, TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                                            SymbolTable symbolTable) {
        if (origType == null || origType.wrappedType == null) {
            return origType;
        }
        TypeSpec convertedActualType =
                origType.wrappedType.intToReal(dejaVu, symbolTable);
        if (convertedActualType == origType.wrappedType || convertedActualType == null) {
            return origType;
        } else {
            return new WrapperTypeSpec(convertedActualType);
        }
    }

    /**
     * Seeks into this type the deepest array element type.
     */
    public WrapperTypeSpec scalarTypeSpec(boolean stopOnPointer, SymbolTable symbolTable) {
        if (isA(this, SymbolTableConstants.ARRAYTYPE) && this.wrappedType.elementType() != null) {
            return this.wrappedType.elementType().scalarTypeSpec(stopOnPointer, symbolTable);
        } else if (isA(this, SymbolTableConstants.MODIFIEDTYPE)
                && ((ModifiedTypeSpec) this.wrappedType).sizeModifierValue() == -1) {
            return this.wrappedType.elementType().scalarTypeSpec(stopOnPointer, symbolTable);
        } else if (!stopOnPointer && isA(this, SymbolTableConstants.POINTERTYPE)) {
            return ((PointerTypeSpec) this.wrappedType).destinationType.scalarTypeSpec(stopOnPointer, symbolTable);
        } else {
            return this;
        }
    }

    /**
     * @return true if this type is a wrapper around boolean type.
     * If this wrapper is free, fills it with boolean type.
     */
    protected boolean checkBoolean() {
        WrapperTypeSpec baseType = baseTypeSpec(true);
        if (baseType.wrappedType == null) {
            baseType.wrappedType = new PrimitiveTypeSpec("boolean");
            return true;
        } else {
            return
                    isA(baseType, SymbolTableConstants.PRIMITIVETYPE)
                            && (baseType.wrappedType.baseTypeName(
                    ).equals("boolean")
                            || TapEnv.inputLanguage() == TapEnv.C
                            && baseType.wrappedType.baseTypeName(
                    ).equals("integer"))
                            || TapEnv.relatedLanguage() == TapEnv.C && isA(baseType, SymbolTableConstants.ENUMTYPE);
        }
    }

    /**
     * @return true if this type is a wrapper around some numeric type.
     * If this wrapper is free, fills it with a "generic" numeric type.
     */
    protected boolean checkNumeric() {
        // TODO: think of a way to specify that a type is "numeric",
        // not knowing whether it will be integer or float in the end.
        // In the meantime, "checkNumeric" does not force any type into an unspecified type...
        WrapperTypeSpec baseType = baseTypeSpec(true);
        if (baseType != null && baseType.wrappedType == null) {
            baseType.wrappedType = new PrimitiveTypeSpec("float");
            baseType.wrappedType.setUndefinedNumeric(true);
            return true;
        } else if ((TapEnv.relatedLanguageIsC() || TapEnv.relatedLanguageIsCPLUSPLUS())
                && (isA(wrappedType, SymbolTableConstants.POINTERTYPE) || isA(wrappedType, SymbolTableConstants.ENUMTYPE))) {
            return true;
        }
        return baseType != null && baseType.isNumericBase();
    }

    /**
     * @return true if this type is a wrapper around integer type.
     * If this wrapper is free, fills it with integer type.
     */
    protected boolean checkNumericInt() {
        WrapperTypeSpec baseType = baseTypeSpec(true);
        if (baseType.wrappedType == null) {
            baseType.wrappedType = new PrimitiveTypeSpec("integer");
            return true;
        } else {
            return isA(baseType, SymbolTableConstants.PRIMITIVETYPE) && ((PrimitiveTypeSpec) baseType.wrappedType).isInteger()
                    ||
                    isA(baseType, SymbolTableConstants.ENUMTYPE) && TapEnv.relatedLanguageIsC();
        }
    }

    /**
     * @return true if this type is a wrapper around a composite type.
     * If this wrapper is free, fills it with a composite RECORD type.
     */
    protected boolean checkRecord() {
        if (wrappedType == null) {
            FieldDecl[] fieldDecls = null;
            wrappedType = new CompositeTypeSpec(null, fieldDecls, SymbolTableConstants.RECORDTYPE, null, null);
            return true;
        } else {
            return isA(wrappedType, SymbolTableConstants.COMPOSITETYPE) || isA(wrappedType, SymbolTableConstants.CLASSTYPE);
        }
    }

    /**
     * @return true if this type is a wrapper around a pointer type.
     * If this wrapper is free, fills it with a pointer to free type.
     */
    protected boolean checkPointer() {
        if (wrappedType == null) {
            wrappedType = new PointerTypeSpec(new WrapperTypeSpec(null), null);
            return true;
        } else {
            return isA(wrappedType, SymbolTableConstants.POINTERTYPE);
        }
    }

    /**
     * @return true if this type is a wrapper around an array type.
     * If this wrapper is free, fills it with an array type of free element type.
     */
    protected boolean checkArray() {
        if (wrappedType == null) {
            wrappedType = new ArrayTypeSpec(new WrapperTypeSpec(null), null);
            return true;
        } else {
            return isA(wrappedType, SymbolTableConstants.ARRAYTYPE);
        }
    }

    // Move to parent class !.
    public boolean isPrimitiveTypeCharacter() {
        return baseTypeName() != null
                && baseTypeName().equals("character")
                && isA(this, SymbolTableConstants.PRIMITIVETYPE);
    }

    protected boolean isPrimitiveTypeFloat() {
        return baseTypeName() != null
                && baseTypeName().equals("float")
                && isA(this, SymbolTableConstants.PRIMITIVETYPE);
    }

    protected boolean isPrimitiveTypeInteger() {
        return baseTypeName() != null
                && baseTypeName().equals("integer")
                && isA(this, SymbolTableConstants.PRIMITIVETYPE);
    }

    // Move to parent class !.
    protected boolean isMetaType() {
        boolean result = isA(this, SymbolTableConstants.METATYPE);
        if (!result && isA(this, SymbolTableConstants.ARRAYTYPE)) {
            result = isA(this.wrappedType.elementType(), SymbolTableConstants.METATYPE);
        }
        return result;
    }

    // Move to parent class !
    @Override
    public boolean isScalar() {
        WrapperTypeSpec typeSpec = this;
        while (isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            typeSpec = typeSpec.wrappedType.elementType();
        }
        return isA(typeSpec, SymbolTableConstants.PRIMITIVETYPE);
    }

    // Move to parent class !

    /**
     * Expand this type into an array type that makes it conform with array types type1 (and type2 if non-null).
     */
    protected WrapperTypeSpec conformingTypeSpec(WrapperTypeSpec type1, WrapperTypeSpec type2) {
        if ((type1 == null || type1.isCharacter() || type1.isString()) &&
                (type2 == null || type2.isCharacter() || type2.isString())) {
            return this;
        }
        if (type1 != null && type2 != null
                && !isA(type1, SymbolTableConstants.METATYPE) && !isA(type2, SymbolTableConstants.METATYPE)
                && this.baseTypeName().equals(type1.baseTypeName())
                && type1.equalsLiterally(type2)) {
            return type1;
        }
        WrapperTypeSpec resultType;
        WrapperTypeSpec elemType1 = null;
        WrapperTypeSpec elemType2 = null;
        TapList<ArrayDim> dims12;
        TapList<ArrayDim> dims1 = null;
        TapList<ArrayDim> dims2 = null;
        if (type1 != null) {
            dims1 = type1.getAllDimensions();
        }
        if (type2 != null) {
            dims2 = type2.getAllDimensions();
        }
        if (dims1 == null) {
            dims12 = dims2;
            elemType1 = type1;
        } else if (dims2 == null) {
            dims12 = dims1;
            elemType2 = type2;
        } else {
            dims12 = new TapList<>(null, null);
            TapList<ArrayDim> tlDims12 = dims12;
            ArrayDim newDim;
            boolean conforms = true;
            while (conforms && dims1 != null && dims2 != null) {
                newDim = ArrayDim.testConformingAndChoose(dims1.head, dims2.head);
                if (newDim == null) {
                    conforms = false;
                } else {
                    tlDims12 = tlDims12.placdl(newDim);
                }
                dims1 = dims1.tail;
                dims2 = dims2.tail;
            }
            if (!conforms || dims1 != null || dims2 != null) {
                resultType = null;
                return resultType;
            }
            dims12 = dims12.tail;
        }
        if (dims12 == null) {
            if (isA(this, SymbolTableConstants.PRIMITIVETYPE)
                    && (this.wrappedType.baseTypeName().
                    equals("float")
                    || this.wrappedType.baseTypeName().
                    equals("complex"))) {
                if (isA(type1, SymbolTableConstants.MODIFIEDTYPE) && isA(type2, SymbolTableConstants.MODIFIEDTYPE)) {
                    resultType = this;
                    if (type1.equalsCompilDep(type2)) {
                        resultType = type1;
                    } else {
                        int size1 =
                                ((ModifiedTypeSpec) type1.wrappedType).sizeModifierValue();
                        int size2 =
                                ((ModifiedTypeSpec) type2.wrappedType).sizeModifierValue();
                        if (size1 > 0 && size1 > size2) {
                            resultType = new WrapperTypeSpec(new ModifiedTypeSpec(this, (ModifiedTypeSpec)type1.wrappedType)) ;
                        } else if (size2 > 0) {
                            resultType = new WrapperTypeSpec(new ModifiedTypeSpec(this, (ModifiedTypeSpec)type2.wrappedType)) ;
                        }
                    }
                } else {
                    if (isA(type1, SymbolTableConstants.MODIFIEDTYPE)) {
                        // TODO c'est faux???
                        if (type1.hasUndefinedSize()) {
                            type2.receives(type1, null, null);
                        }
                        resultType = new WrapperTypeSpec(new ModifiedTypeSpec(this, (ModifiedTypeSpec)type1.wrappedType)) ;
                    } else {
                        if (isA(type2, SymbolTableConstants.MODIFIEDTYPE)) {
                            // TODO ici aussi c'est faux, cf todoF90 v508
                            if (type2.hasUndefinedSize()) {
                                type1.receives(type2, null, null);
                            }
                            resultType = new WrapperTypeSpec(new ModifiedTypeSpec(this, (ModifiedTypeSpec)type2.wrappedType)) ;
                        } else {
                            resultType = this;
                        }
                    }
                }
            } else {
                resultType = this;
            }
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.indentOnTrace(TapEnv.traceIndent());
                TapEnv.printlnOnTrace("ConformingTS of " + this + " :: " + type1 + " + " + type2 + " --> " + resultType);
            }
        } else {
            ArrayDim[] dimensions = new ArrayDim[TapList.length(dims12)];
            int i = 0;
            while (dims12 != null) {
                dimensions[i] = dims12.head;
                i++;
                dims12 = dims12.tail;
            }
            WrapperTypeSpec elementType;
            if (isA(type1, SymbolTableConstants.ARRAYTYPE)) {
                elemType1 = type1.wrappedType.elementType();
            }
            if (isA(type2, SymbolTableConstants.ARRAYTYPE)) {
                elemType2 = type2.wrappedType.elementType();
            }
            elementType = this.conformingTypeSpec(elemType1, elemType2);
            resultType = new WrapperTypeSpec(new ArrayTypeSpec(elementType, dimensions));
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.indentOnTrace(TapEnv.traceIndent());
                TapEnv.printlnOnTrace("ConformingTS of " + this + " :: " + type1 + " + " + type2 + " --> " + resultType);
            }
        }
        return resultType;
    }

    // Move to parent class !
    public WrapperTypeSpec combineWith(WrapperTypeSpec newTypeSpec, SymbolTable symbolTable) {
        if (wrappedType == null) {
            return newTypeSpec;
        } else if (newTypeSpec.wrappedType != null) {
            TypeSpec
                    sumActualTypeSpec =
                    wrappedType.combineWith(newTypeSpec.wrappedType, symbolTable);
            if (sumActualTypeSpec == null) {
                return null;
            } else {
                return new WrapperTypeSpec(sumActualTypeSpec);
            }
        } else {
            return this;
        }
    }

    // Move to parent class !
    public WrapperTypeSpec addWith(WrapperTypeSpec type) {
        WrapperTypeSpec result = this;
        if (this.wrappedType == null || isA(this, SymbolTableConstants.VOIDTYPE)) {
            result = type;
        } else if (type == null || type.wrappedType == null || isA(type, SymbolTableConstants.VOIDTYPE)) {
            result = this;
        } else {
            ToObject<ModifiedTypeSpec> toModified = new ToObject<>(null);
            WrapperTypeSpec thisElemType = this.elementType();
            WrapperTypeSpec thisBaseType =
                    TypeSpec.peelSizeModifier(thisElemType, toModified);
            Tree thisModifier = null;
            int thisElemTypeSize;
            if (toModified.obj() != null) {
                thisModifier = toModified.obj().sizeModifier;
                thisElemTypeSize = toModified.obj().sizeModifierValue();
                if (thisElemTypeSize != -1) {
                    thisElemTypeSize = toModified.obj().computeSize();
                }
            } else {
                thisElemTypeSize = thisBaseType.computeSize();
            }

            WrapperTypeSpec typeElemType = type.elementType();
            WrapperTypeSpec typeBaseType =
                    TypeSpec.peelSizeModifier(typeElemType, toModified);
            Tree typeModifier = null;
            int typeElemTypeSize;
            if (toModified.obj() != null) {
                typeModifier = toModified.obj().sizeModifier;
                typeElemTypeSize = toModified.obj().sizeModifierValue();
                if (typeElemTypeSize != -1) {
                    typeElemTypeSize = toModified.obj().computeSize();
                }
            } else {
                typeElemTypeSize = typeBaseType.computeSize();
            }

            if (isA(thisBaseType, SymbolTableConstants.POINTERTYPE)) {
                result = thisBaseType.conformingTypeSpec(this, type);
            } else if (isA(typeBaseType, SymbolTableConstants.POINTERTYPE)) {
                result = typeBaseType.conformingTypeSpec(this, type);
            } else if (isA(thisBaseType, SymbolTableConstants.ENUMTYPE)) {
                result = typeBaseType.conformingTypeSpec(this, type);
            } else if (isA(typeBaseType, SymbolTableConstants.ENUMTYPE)) {
                result = thisBaseType.conformingTypeSpec(this, type);
            } else if (isA(thisBaseType, SymbolTableConstants.PRIMITIVETYPE)) {
                result = new WrapperTypeSpec(((PrimitiveTypeSpec) thisBaseType.wrappedType).addWith(typeBaseType));
                String resultBaseTypeName = result.baseTypeName();
                if (resultBaseTypeName.equals(thisBaseType.baseTypeName())) {
                    if (resultBaseTypeName.equals(typeBaseType.baseTypeName())) {
                        // choices below are heuristic:
                        if (thisElemTypeSize == -1) {
                            result = thisElemType;
                        } else if (typeElemTypeSize == -1) {
                            result = typeElemType;
                        } else if (thisElemTypeSize >= typeElemTypeSize) {
                            result = thisElemType;
                        } else {
                            result = typeElemType;
                        }
                    } else {
                        result = thisElemType;
                    }
                } else {
                    if (resultBaseTypeName.equals(typeBaseType.baseTypeName())) {
                        result = typeElemType;
                    }
                }
                result = result.conformingTypeSpec(this, type);
            }
        }
        return result;
    }

    // Move to parent class !
    public WrapperTypeSpec checkNoneDimensionsOfNewSH(
            String hintNameInText, String hintNameInIdent, Tree hintTreeInCallSize,
            SymbolTable symbolTable, NewSymbolHolder symbolHolder) {
        WrapperTypeSpec type = this;
        if (isA(this, SymbolTableConstants.ARRAYTYPE)) {
            ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) this.wrappedType;
            ArrayDim[] checkedDimensions = arrayTypeSpec.checkNoneDimensions(arrayTypeSpec.dimensions(),
                    !symbolTable.isFormalParamsLevel(),
                    false, null, hintNameInText,
                    hintNameInIdent, hintTreeInCallSize,
                    (symbolHolder==null ? null : symbolHolder.hintRootTree),
                    symbolTable, symbolHolder);
            if (checkedDimensions != null) {
                type = new WrapperTypeSpec(new ArrayTypeSpec(
                        arrayTypeSpec.elementType(),
                        checkedDimensions));
            }
        }
        return type;
    }

    /**
     * @return true if a declaration of a variable of this WrapperTypeSpec is cheap and easy
     * Example: a primitive, a pointer, a void
     * Counter example: an array of dynamic size (&rArr; requiring a decl with ISIZEOF).
     */
    public boolean easilyDeclared() {
        if (wrappedType == null) {
            return false;
        }
        switch (wrappedType.kind()) {
            case SymbolTableConstants.ARRAYTYPE: {
                WrapperTypeSpec basisTypeSpec = wrappedType.elementType();
                boolean easy = basisTypeSpec != null && basisTypeSpec.easilyDeclared();
                ArrayDim[] dimensions = ((ArrayTypeSpec) wrappedType).dimensions();
                int i = dimensions.length - 1;
                while (easy && i >= 0) {
                    if (dimensions[i].lower == null || dimensions[i].upper == null) {
                        easy = false;
                    }
                    --i;
                }
                return easy;
            }
            case SymbolTableConstants.MODIFIEDTYPE: {
                WrapperTypeSpec basisTypeSpec = wrappedType.elementType();
                return basisTypeSpec != null && basisTypeSpec.easilyDeclared();
            }
            case SymbolTableConstants.POINTERTYPE:
            case SymbolTableConstants.PRIMITIVETYPE:
            case SymbolTableConstants.VOIDTYPE:
                return true;
            default:
                return false;
        }
    }

    // Move to parent class !
    public WrapperTypeSpec differentiateTypeSpecMemo(SymbolTable symbolTable, SymbolTable srcSymbolTable,
                                             int diffUnitSort, String fSuffix,
                                             boolean localDecl, boolean multiDirMode, ArrayDim multiDirDimensionMax,
                                             String hintArrayNameInText,
                                             String hintArrayNameInIdent, Tree hintArraySize, Tree nameTree) {
        if (wrappedType == null || symbolTable == null) {
            return null;
        }
        WrapperTypeSpec result = wrappedType.diffTypeSpec;
        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
            if (result == null) {
                WrapperTypeSpec equalsTypeSpec;
                if (TapEnv.relatedLanguageIsFortran()) {
                    equalsTypeSpec = TapList.findTypeSpec(TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable().associationByAddressTypes,
                            this);
                } else {
                    equalsTypeSpec = TapList.findTypeSpec(symbolTable.getCallGraph().globalRootSymbolTable().associationByAddressTypes,
                            this);
                }
                if (equalsTypeSpec != null && equalsTypeSpec != this) {
                    result = equalsTypeSpec.differentiateTypeSpecMemo(symbolTable, srcSymbolTable, diffUnitSort,
                            fSuffix, localDecl, multiDirMode, multiDirDimensionMax, hintArrayNameInText,
                            hintArrayNameInIdent, hintArraySize, nameTree);
                    wrappedType.diffTypeSpec = equalsTypeSpec.wrappedType.diffTypeSpec;
                } else {
                    result = wrappedType.differentiateTypeSpec(symbolTable, srcSymbolTable, diffUnitSort, fSuffix,
                            localDecl, multiDirMode, multiDirDimensionMax,
                            hintArrayNameInText, hintArrayNameInIdent, hintArraySize, nameTree);
                    wrappedType.diffTypeSpec = result;
                    if (this.isScalar()) {
                        if (TapEnv.relatedLanguageIsFortran()) {
                            TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable().associationByAddressTypes =
                                    new TapList<>(this,
                                            TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable().associationByAddressTypes);
                        } else {
                            symbolTable.getCallGraph().globalRootSymbolTable().associationByAddressTypes =
                                    new TapList<>(this,
                                            symbolTable.getCallGraph().globalRootSymbolTable().associationByAddressTypes);
                        }
                    }
                }
            } else {
                this.addRefDiffTypeSpec(symbolTable, srcSymbolTable);
            }
        } else {
            if (result == null) {
                result = wrappedType.differentiateTypeSpec(symbolTable, srcSymbolTable, diffUnitSort, fSuffix,
                        localDecl, multiDirMode, multiDirDimensionMax,
                        hintArrayNameInText, hintArrayNameInIdent, hintArraySize, nameTree);
                if (isA(this, SymbolTableConstants.COMPOSITETYPE) || TapEnv.get().complexStep) {
                    wrappedType.diffTypeSpec = result;
                }
            } else {
                this.addRefDiffTypeSpec(symbolTable, srcSymbolTable);
            }
            if (wrappedType.diffTypeSpec != null
                    && wrappedType.diffTypeSpec.equalsLiterally(this)) {
                wrappedType.diffTypeSpec = null;
                result = null;
            }
        }
        return result;
    }

    @Override
    public boolean acceptsMultiDirDimension() {
        return (wrappedType!=null && wrappedType.acceptsMultiDirDimension()) ;
    }

    // Move to parent class !
    public WrapperTypeSpec equalsDiffTypeSpecAndTypeSpec() {
        if (this.wrappedType != null
                && this.wrappedType.diffFromTypeDecl != null
                && (this.wrappedType.diffFromTypeDecl.typeSpec.wrappedType.diffTypeSpec
                == null
                || isA(this, SymbolTableConstants.COMPOSITETYPE)
                && ((CompositeTypeSpec) this.wrappedType.diffFromTypeDecl.typeSpec.
                wrappedType.diffTypeSpec.wrappedType).isEmpty()
                || this.equalsCompilDep(this.wrappedType.diffFromTypeDecl.typeSpec))) {
            return this.wrappedType.diffFromTypeDecl.typeSpec;
        } else {
            return this;
        }
    }

    /**
     * @return true if the 2 types are similar enough
     * so that a pointer to "this" may point to a "destTypeSpec".
     */
    public boolean canMatchPointerTo(WrapperTypeSpec destTypeSpec) {
        if (this == destTypeSpec) {
            return true;
        }
        TapList<ArrayDim> dims = getAllDimensions();
        TapList<ArrayDim> destdims = destTypeSpec.getAllDimensions();
        if ((dims == null) == (destdims != null)) {
            return false;
        }
        WrapperTypeSpec base = baseTypeSpec(true);
        WrapperTypeSpec destbase = destTypeSpec.baseTypeSpec(true);
        if (base == null || base.wrappedType == null) {
            // Accept that a void* may point on anything.
            return true;
        } else if (destbase == null || destbase.wrappedType == null) {
            return false;
        } else {
            TypeSpec actualbase = base.wrappedType;
            TypeSpec destactualbase = destbase.wrappedType;
            if (isA(actualbase, SymbolTableConstants.PRIMITIVETYPE)) {
                if (isA(destactualbase, SymbolTableConstants.PRIMITIVETYPE)) {
                    return ((PrimitiveTypeSpec) actualbase).name().equals(
                            ((PrimitiveTypeSpec) destactualbase).name());
                } else {
                    return false;
                }
            } else if (isA(actualbase, SymbolTableConstants.FUNCTIONTYPE)) {
                return isA(destactualbase, SymbolTableConstants.FUNCTIONTYPE);
            } else if (isA(actualbase, SymbolTableConstants.COMPOSITETYPE)) {
                return actualbase == destactualbase;
            } else {
                // Accept that a void* may point on anything.
                return isA(actualbase, SymbolTableConstants.VOIDTYPE);
            }
        }
    }

    /**
     * @return a String that represents the present WrapperTypeSpec,
     * to be used in a procedure name that is specialized for this type.
     * <p>
     * Examples: INTEGER4, INTEGER8, REAL4, REAL8, REAL8ARRAY,...
     * "toArraySize" must contain upon input a Tree which is the size of this
     * WrapperTypeSpec if it is an array, null otherwise.
     * Upon output this size is modified when this
     * typeSpec is a string: it is multiplied by the number of characters.
     */
    public String buildTypeNameForProcName(TapList<Tree> toArraySize, SymbolTable symbolTable) {
        ToObject<ModifiedTypeSpec> toModifiedType = new ToObject<>(null);
        WrapperTypeSpec bTypeSpec = peelSizeModifier(this, toModifiedType);
        int modifier = toModifiedType.obj() == null ? -1 : toModifiedType.obj().sizeModifierValue();
        String typeName = bTypeSpec.baseTypeName();
        String tName;
        if (typeName.equals("character")) {
            tName = "character";
            if (modifier != -1 && modifier != 1) {
                if (toArraySize.head == null) {
                    toArraySize.head = ILUtils.build(ILLang.op_intCst, modifier);
                } else {
                    toArraySize.head = ILUtils.mulTree(
                            ILUtils.build(ILLang.op_intCst, modifier),
                            toArraySize.head);
                }
            }
        } else if (typeName.equals("integer")) {
            if (modifier > 0) {
                tName = "integer" + modifier;
            } else if (modifier == -2) {
                tName = "integer" + 2 * TapEnv.get().integerSize;
            } else {
                tName = "integer" + TapEnv.get().integerSize;
            }
        } else if (typeName.equals("float")) {
            if (modifier > 0) {
                tName = "real" + modifier;
            } else if (modifier == -2) {
                tName = "real" + TapEnv.get().doubleRealSize;
            } else {
                tName = "real" + TapEnv.get().realSize;
            }
        } else if (typeName.equals("complex")) {
            if (modifier > 0) {
                tName = "real" + 2 * modifier;
            } else if (modifier == -2) {
                tName = "real" + 4 * TapEnv.get().doubleRealSize;
            } else {
                tName = "real" + 4 * TapEnv.get().realSize;
            }
        } else if (typeName.equals("Undefined") || typeName.isEmpty()) {
            tName = "UNKNOWNTYPE";
        } else {
            tName = typeName;
        }
        if (toArraySize.head != null) {
            tName = tName + "ARRAY";
        }
        return tName;
    }

    protected WrapperTypeSpec pointerToArrayTypeSpec() {
        WrapperTypeSpec result = this;
        if (isA(wrappedType, SymbolTableConstants.POINTERTYPE)
                && ((PointerTypeSpec) wrappedType).offsetLength != null) {
            result = new WrapperTypeSpec(
                    ((PointerTypeSpec) wrappedType).pointerToArrayTypeSpec());
        }
        return result;
    }

    @Override
    public TypeSpec wrappedType() {
        return wrappedType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        wrappedType = type instanceof WrapperTypeSpec ? ((WrapperTypeSpec) type).wrappedType : type;
    }

    @Override
    public boolean isAnIOTypeSpec(SymbolTable symbolTable) {
        return wrappedType != null && wrappedType.isAnIOTypeSpec(symbolTable);
    }

    @Override
    protected TypeSpec cloneAsUndefinedNumeric(boolean undefined) {
        return new WrapperTypeSpec(wrappedType.cloneAsUndefinedNumeric(undefined));
    }

    @Override
    public TypeSpec nestedLevelType() {
        return wrappedType;
    }

    @Override
    public TypeSpec peelPointer() {
        return wrappedType == null ? null : wrappedType.peelPointer();
    }

    @Override
    public String baseTypeName() {
        return wrappedType == null ? "Undefined" : wrappedType.baseTypeName();
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        if (wrappedType != null
                && (isA(wrappedType, SymbolTableConstants.ARRAYTYPE)
                || isA(wrappedType, SymbolTableConstants.FUNCTIONTYPE)
                || isA(wrappedType, SymbolTableConstants.MODIFIEDTYPE)
                || isA(wrappedType, SymbolTableConstants.POINTERTYPE) && !stopOnPointer)) {
            return wrappedType.baseTypeSpec(stopOnPointer);
        }
        //TODO: too many repetitive calls to baseType....(). Should be factored...
        else {
            return this;
        }
    }

    @Override
    public WrapperTypeSpec modifiedBaseTypeSpec() {
        if (isA(wrappedType, SymbolTableConstants.ARRAYTYPE)
                || isA(wrappedType, SymbolTableConstants.POINTERTYPE)) {
            return wrappedType.modifiedBaseTypeSpec();
        } else {
            return this;
        }
    }

    @Override
    public boolean containsAPointer() {
        return wrappedType != null && wrappedType.containsAPointer();
    }

    @Override
    public TapList<ArrayDim> getAllDimensions() {
        return wrappedType == null ? null : wrappedType.getAllDimensions();
    }

    @Override
    public boolean containsUnknownDimension() {
        return wrappedType != null && wrappedType.containsUnknownDimension();
    }

    @Override
    public WrapperTypeSpec elementType() {
        if (isA(wrappedType, SymbolTableConstants.ARRAYTYPE)) {
            return wrappedType.elementType();
        } else {
            return this;
        }
    }

    @Override
    public void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (wrappedType != null) {
            wrappedType.updateAfterImports(symbolTable, dejaVu);
        }
    }

    @Override
    protected int computeSize() {
        if (wrappedType == null)
        //sinon division par 0 dans MemoryMap !
        {
            return 1;
        } else {
            return wrappedType.size();
        }
    }

    @Override
    public boolean isFunction() {
        return wrappedType != null && wrappedType.isFunction();
    }

    @Override
    public boolean isCharacter() {
        return wrappedType != null && wrappedType.isCharacter();
    }

    @Override
    public boolean isString() {
        return wrappedType != null && wrappedType.isString();
    }

    @Override
    public boolean isPointer() {
        return wrappedType != null && wrappedType.isPointer();
    }

    @Override
    public boolean isTarget() {
        return wrappedType != null && wrappedType.isTarget();
    }

    @Override
    public boolean isArray() {
        return wrappedType != null && wrappedType.isArray();
    }

    @Override
    public boolean isNamedType() {
        return wrappedType != null && wrappedType.isNamedType();
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // Easy strict == case:
        if (this == other) {
            return true;
        }
        if (wrappedType == null) {
            // if this type is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                if (other != null) {
                    this.setWrappedType(other.weakenForInference(comparison));
                }
                return true;
            } else {
                return !testTypesLitteral(comparison) && testAcceptsUnspecified(comparison);
            }
        } else {
            // recurse into this WrapperTypeSpec layer:
            return wrappedType.comparesWith(other, comparison, this,
                    toOther, dejaVu);
        }
    }

    @Override
    protected TypeSpec weakenForInference(int comparison) {
        if (wrappedType == null) {
            return this;
        } else {
            return new WrapperTypeSpec(wrappedType.weakenForInference(comparison));
        }
    }

    @Override
    public TypeSpec copy() {
        return new WrapperTypeSpec(wrappedType==null ? null : wrappedType.copy()) ;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        TypeSpec copyWrapped = (wrappedType == null ? null : wrappedType.copyStopOnComposite(publishedUnit)) ;
        return (copyWrapped == wrappedType ? this : new WrapperTypeSpec(copyWrapped));
    }

    @Override
    public int precisionSize() {
        return wrappedType == null ? -1 : wrappedType.precisionSize();
    }

    @Override
    public Tree buildConstantZero() {
        return wrappedType == null ? null : wrappedType.buildConstantZero();
    }

    @Override
    public Tree buildConstantOne() {
        return wrappedType == null ? null : wrappedType.buildConstantOne();
    }

    // Merge with parent class !

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        return wrappedType != null && wrappedType.containsMetaType(dejaVu);
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        WrapperTypeSpec copiedResult = (WrapperTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            copiedResult = new WrapperTypeSpec(null);
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            TypeSpec copiedActualTypeSpec =
                    wrappedType == null ? null : wrappedType.localize(toAlreadyCopied, containsMeta);
            if (copiedActualTypeSpec instanceof WrapperTypeSpec) {
                // This means that solving the inside wrappedType has returned a wrapper Type.
                // this may happen only if the inside is a Meta type, which are solved as wrappers.
                copiedResult = (WrapperTypeSpec) copiedActualTypeSpec;
                alreadyRef.second = copiedResult;
            } else {
                copiedResult.wrappedType = copiedActualTypeSpec;
            }
            if (containsMeta.get()) {
                // If this type contains a meta, write it in the memo of already localized types:
                alreadyRef.third = Boolean.TRUE;
            } else {
                // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
                // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
                alreadyRef.first = null;
            }
        }
        return copiedResult;
    }

    @Override
    protected boolean hasUndefinedSize() {
        if (wrappedType == null) {
            return false;
        } else {
            return wrappedType.hasUndefinedSize();
        }
    }

    @Override
    protected void setHasUndefinedSize(boolean val) {
        if (wrappedType != null) {
            wrappedType.setHasUndefinedSize(val);
        }
    }

    // Merge with parent class !
    @Override
    protected WrapperTypeSpec addMultiDirDimension(ArrayDim dimension) {
        if (wrappedType != null) {
            return new WrapperTypeSpec(wrappedType.addMultiDirDimension(dimension));
        } else {
            TapEnv.toolWarning(-1, "(Add one dimension) Not expected on unspecified type " + this);
            ArrayDim[] dimensions = new ArrayDim[1];
            dimensions[0] = dimension;
            return new WrapperTypeSpec(new ArrayTypeSpec(this, dimensions));
        }
    }

    // Merge with parent class !
    @Override
    protected WrapperTypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        TypeSpec convertedActualType =
                this.wrappedType.realToComplex(dejaVu, complexTypeSpec);
        return new WrapperTypeSpec(convertedActualType);
    }

    // Merge with parent class addDiffTypeSpec() !
    protected void addRefDiffTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable) {
        if (TapEnv.associationByAddress() && wrappedType.diffTypeSpec == null && !TapEnv.get().complexStep) {
            SymbolTable diffTypesSymbolTable;
            if (TapEnv.relatedLanguageIsFortran()) {
                diffTypesSymbolTable = TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable();
            } else {
                diffTypesSymbolTable = symbolTable.getCallGraph().globalRootSymbolTable();
            }
            WrapperTypeSpec equalsTypeSpec =
                    TapList.findTypeSpec(diffTypesSymbolTable.associationByAddressTypes,
                            this);
            if (equalsTypeSpec != null) {
                wrappedType.diffTypeSpec = equalsTypeSpec.wrappedType.diffTypeSpec;
            }
        }
        wrappedType.addDiffTypeSpec(symbolTable, srcSymbolTable);
    }

    @Override
    public void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
        if (diffInfos != null && wrappedType != null) {
            wrappedType.cumulActiveParts(diffInfos, symbolTable);
            // Mark named types so that there is a differentiated type name:
            if (typeDeclName() != null && needsADiffType(null)) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeDeclName());
                if (typeDecl != null) {
                    typeDecl.setActive(true);
                }
            }
        }
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return wrappedType != null && wrappedType.needsADiffType(dejaVu);
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return wrappedType != null && wrappedType.isDifferentiated(dejaVu);
    }

    // Merge with parent class .
    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        return wrappedType == null || wrappedType.checkTypeSpecValidity(dejaVu);
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree typeTree;
        if (wrappedType == null) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) null wrapped type");
            typeTree = ILUtils.build(ILLang.op_ident, "UnknownType");
        } else if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            typeTree = ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            dejaVu = new TapList<>(this, dejaVu);
            String shortName = useShortNames ? wrappedType.findShortName(dependsOn, shortNames) : null;
            if (shortName != null) {
                if (isA(this, SymbolTableConstants.PRIMITIVETYPE)) {
                    typeTree = wrappedType.generateTree(symbolTable, dependsOn, shortNames, false, dejaVu);
                } else {
                    typeTree = ILUtils.build(ILLang.op_ident, shortName);
                }
            } else {
                // [llh] following code to be refactored:
                if (TapEnv.relatedLanguageIsC()) {
                    NewSymbolHolder diffSH = wrappedType.diffTypeDeclSymbolHolder;
                    if (diffSH != null) {
                        // Design problem: C procedures are in the global C SymbolTable,
                        // and the types they contain are from the compilation unit of the
                        // procedure definition. Usage of this procedure from a different
                        // compilation unit may see these types from the definition
                        // compilation unit, and therefore the solving level of the diff
                        // type name goes up to the C SymbolTable, causing name conflicts.
                        boolean fromOtherCompilationUnit =
                                (symbolTable != null && TapEnv.isC(symbolTable.language())
                                        && diffSH.solvingRootIsOutsideFile(symbolTable));
                        typeTree = diffSH.makeNewRef(fromOtherCompilationUnit ? null : symbolTable);
                    } else if (!isA(this, SymbolTableConstants.PRIMITIVETYPE) && wrappedType.typeDeclName() != null) {
                        typeTree = ILUtils.build(ILLang.op_ident, wrappedType.genTypeDeclName());
                    } else if (isA(this, SymbolTableConstants.COMPOSITETYPE) && ((CompositeTypeSpec) wrappedType).name != null) {
                        typeTree = ILUtils.build(ILLang.op_ident, ((CompositeTypeSpec) wrappedType).name);
                    } else {
                        typeTree = wrappedType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
                    }
                } else {
                    typeTree = wrappedType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
                }
            }
        }
        return typeTree;
    }

    @Override
    public String showType() {
        return wrappedType == null ? "UNKNOWN" : wrappedType.showType();
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "{" //+"@"+Integer.toHexString(hashCode())+" "
                + (wrappedType == null ? "?" : wrappedType.toString()) + "}";
    }
}
