/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.ir2tree.ControlStruct;
import fr.inria.tapenade.utils.*;

/**
 * One Block of a Flow Graph.
 * Blocks contain sequences of elementary instructions that
 * are executed always in sequence from the first to the last.
 * The last instruction may be a test which decides which Flow
 * Arrow (FGArrow) is taken after the end of the Block's execution.
 * If the last instruction is not a test, then there may be only
 * one flow arrow leaving from the Block.
 */
public class Block {

    private TapList<FGArrow> flow;
    private TapList<FGArrow> backFlow;

    /**
     * The ordered list of all instructions in this Block.
     */
    public TapList<Instruction> instructions;
    /**
     * The SymbolTable of symbols visible from this Block.
     */
    public SymbolTable symbolTable;
    /**
     * The controlling parallel instructions for this Block.
     */
    public TapList<Instruction> parallelControls;
    /**
     * CLEAR, VISITING or VISITED.
     */
    protected int visit;
    /**
     * A unique rank of this Block. -1 is for the EntryBlock.
     * Other Blocks are numbered from 0 up, in DFST order.
     */
    public int rank;
    /**
     * May be used to give a symbolic name to this Block.
     * This name will be used when toString() shows this Block.
     */
    public String symbolicRk;
    /**
     * True if this Block has an exiting flow FGArrow that goes
     * to a Block earlier or equal (i.e. with a lower or equal rank)
     * in the DFST order.
     */
    public boolean isFGCycleTail;
    /**
     * True if this Block has an incoming backFlow FGArrow that comes
     * from a Block later or equal (i.e. with a greater or equal rank)
     * in the DFST order.
     */
    public boolean isFGCycleHead;
    /**
     * The zones that are constant from the beginning of this
     * Block till the exit of the containing Unit.
     * This info is filled by InOutAnalyzer.
     */
    public BoolVector constantZones;
    /**
     * The zones that are unused from the beginning of this
     * Block till the exit of the containing Unit.
     * This info is filled by InOutAnalyzer.
     */
    public BoolVector unusedZones;
    /**
     * Mark useful in any Data Flow analysis to write
     * whether this Block must be analyzed again at next analysis sweep.
     */
    public boolean analysisIsOutOfDate = true;
    /**
     * When this Blocks end with a test, the associated test zone.
     */
    public int testZone = -1;
    /**
     * The list of blocks controlling this block, from the most deeply nested up.
     */
    public TapList<Block> controllers;
    /**
     * The list of the control zones controlling this block.
     */
    public TapIntList controllersZone = null ;
    /**
     * Vector of the ranks of the Blocks that are possibly executed after this one.
     */
    public BoolVector successor;
    /**
     * Vector of the ranks of the Blocks that are possibly executed before this one.
     */
    public BoolVector predecessor;
    /**
     * Vector of the ranks of the Blocks that are certainly executed after this one.
     */
    public BoolVector postDominator;
    /**
     * Vector of the ranks of the Blocks that are certainly executed before this one.
     */
    public BoolVector dominator;
    /**
     * The closest Block certainly executed after this one.
     */
    protected Block immediatePostDominator;
    /**
     * The closest Block certainly executed before this one.
     */
    protected Block immediateDominator;
    /**
     * True if this Block is reachable from the Unit's entry.
     * [llh 9Nov2023]: this info is apparently not used and is different
     * from the info stored in "reachUp" and "reachDown".
     * Maybe we could use the methods from Unit.computeReachability()
     * to refine our "reachUp" and "reachDown", then remove computeReachability().
     */
    protected boolean reachable = true;
    /**
     * The matrix of pointer destinations at the begining of the block.
     */
    public BoolMatrix pointerInfosIn;
    /**
     * A label that may be attached to the first instruction of this Block.
     */
    private String origLabel;
    /**
     * The container Block in the hierarchy of nested loop Blocks.
     */
    private Block enclosing;

    /** True iff the entry of this Block can be reached from entry */
    public boolean reachUp = false ;

    /** True iff the exit of this Block can be reached from entry */
    public boolean reachDown = false ;

    /** True iff the entry of this Block can reach exit */
    public boolean backReachUp = false ;

    /** True iff the exit of this Block can reach exit */
    public boolean backReachDown = false ;

    /**
     * Temporary storage.
     */
    private Object tmp1;
    /**
     * Temporary storage.
     */
    private int tmp2;
    /**
     * Temporary storage.
     */
    private Object tmp3;
    /**
     * Temporary storage (used in PointerAnalyzer)
     */
    public boolean tmp4 ;

    /**
     * Create an empty Block, set its SymbolTable, and register it into the list
     * of created Blocks "allBlocks" (if nonempty).
     */
    public Block(SymbolTable symbolTable, TapList<Instruction> parallelControls,
                 TapList<Block> allBlocks) {
        super();
        this.symbolTable = symbolTable;
        this.parallelControls = parallelControls;
        if (allBlocks != null) {
            allBlocks.tail = new TapList<>(this, allBlocks.tail);
        }
    }

    /**
     * Adds trees "decls" after the declarations in the list trees.
     */
    public static TapList<Tree> addTreeDeclTl(TapList<Tree> trees, TapList<Tree> decls) {
        // same as addInstrDeclTl, but working on lists of Tree instead of lists of Instruction.
        if (trees != null) {
            TapList<Tree> curInstructionListCell = trees;
            boolean ok = false;
            Tree nextTree = curInstructionListCell.head;
            if (nextTree == null || !ILUtils.isADeclaration(nextTree)) {
                trees = TapList.append(decls, trees);
            } else {
                while (curInstructionListCell.tail != null && !ok) {
                    nextTree = curInstructionListCell.tail.head;
                    if (nextTree == null || !(ILUtils.isADeclaration(nextTree) || nextTree.opCode()==ILLang.op_none)) {
                        curInstructionListCell.tail =
                                TapList.append(decls, curInstructionListCell.tail);
                        ok = true;
                    }
                    curInstructionListCell = curInstructionListCell.tail;
                }
                if (!ok) {
                    curInstructionListCell.tail =
                            TapList.append(decls, curInstructionListCell.tail);
                }
            }
        } else {
            trees = decls;
        }
        return trees;
    }

    private static boolean isDuplicable(Tree expr, SymbolTable symbolTable) {
        switch (expr.opCode()) {
            case ILLang.op_call: {
                Unit calledFunctionUnit = DataFlowAnalyzer.getCalledUnit(expr, symbolTable);
                if (calledFunctionUnit == null
                    || !(calledFunctionUnit.isIntrinsic()
                         ||
                         // Called unit was declared duplicable with option -duplicable:
                         TapList.containsString(TapEnv.duplicableUnitNames(), calledFunctionUnit.name(),
                                !TapEnv.isFortran(symbolTable.language())))) {
                    return false;
                }
                return isDuplicable(ILUtils.getArguments(expr), symbolTable);
            }
            case ILLang.op_assign:
                return false;
            default:
                if (!expr.isAtom()) {
                    Tree[] args = expr.children();
                    boolean duplicable = true;
                    int i = args.length - 1;
                    while (duplicable && i >= 0) {
                        duplicable = isDuplicable(args[i], symbolTable);
                        i--;
                    }
                    return duplicable;
                } else {
                    return true;
                }
        }
    }

    /** @return true when this block flows to a Block that cannot see scope SymbolTable */
    public boolean flowsOutOfScope(SymbolTable symbolTable) {
        TapList<FGArrow> inFlow = flow ;
        Block dest ;
        boolean flowsOut = false ;
        while (inFlow!=null && !flowsOut) {
            dest = inFlow.head.destination ;
            flowsOut = dest!=null && !dest.symbolTable.nestedIn(symbolTable) ;
            inFlow = inFlow.tail ;
        }
        return flowsOut ;
    }

    /**
     * Utility for "isCheapDuplicable".
     */
    private static int duplicableCost(Tree tree, SymbolTable symbolTable) {
        switch (tree.opCode()) {
            case ILLang.op_nameEq:
                return duplicableCost(tree.down(2), symbolTable);
            case ILLang.op_fieldAccess:
            case ILLang.op_minus:
            case ILLang.op_not:
                return duplicableCost(tree.down(1), symbolTable);
            case ILLang.op_arrayAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
                return duplicableCost(tree.down(1), symbolTable) + 1;
            case ILLang.op_ident:
                return 1;
            case ILLang.op_mul:
            case ILLang.op_div:
                return duplicableCost(tree.down(1), symbolTable)
                        + duplicableCost(tree.down(2), symbolTable) + 2;
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_complexConstructor:
                return duplicableCost(tree.down(1), symbolTable)
                        + duplicableCost(tree.down(2), symbolTable) + 1;
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
                return duplicableCost(tree.down(1), symbolTable)
                        + duplicableCost(tree.down(2), symbolTable);
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_bitCst:
            case ILLang.op_label:
            case ILLang.op_star:
            case ILLang.op_none:
                return 0;
            /* Default: a priori not duplicable at all: return a high cost! */
            default:
                return 1000;
        }
    }

    /**
     * @return the list of all Flow Arrows leaving from this Block.
     * For a LoopBlock, returns all FGArrows that leave some Block
     * inside the LoopBlock to some Block outside the LoopBlock.
     */
    public TapList<FGArrow> flow() {
        return flow;
    }

    /**
     * Set the list of all Flow Arrows leaving from this Block.
     *
     * @param flow TapList of FGArrow.
     */
    protected final void setFlow(TapList<FGArrow> flow) {
        this.flow = flow;
    }

    /**
     * Adds the given arrow into the flow from this Block.
     */
    protected void addFlow(FGArrow arrow) {
        flow = new TapList<>(arrow, flow);
    }

    /**
     * @return true when there is a FGArrow from this Block to dest
     */
    public boolean flowsTo(Block dest) {
        boolean found = false ;
        TapList<FGArrow> inFlow = flow ;
        while (inFlow!=null && !found) {
            found = (inFlow.head.destination==dest) ;
            inFlow = inFlow.tail ;
        }
        return found ;
    }

    /**
     * @return the list of all Flow Arrows arriving to this Block.
     * For a LoopBlock, returns all FGArrows that leave some Block
     * outside the LoopBlock to some Block inside the LoopBlock.
     */
    public TapList<FGArrow> backFlow() {
        return backFlow;
    }

    /**
     * Set the list of all Flow Arrows arriving to this Block.
     *
     * @param backFlow TapList of FGArrow.
     */
    protected final void setBackFlow(TapList<FGArrow> backFlow) {
        this.backFlow = backFlow;
    }

    /**
     * Adds the given arrow into the flow to this Block.
     */
    protected void addBackFlow(FGArrow arrow) {
        backFlow = new TapList<>(arrow, backFlow);
    }

    /**
     * @return the first instruction in this Block.
     */
    public Instruction headInstr() {
        return instructions == null ? null : instructions.head;
    }

    /**
     * @return the last instruction in this Block.
     */
    public Instruction lastInstr() {
        if (instructions == null) {
            return null;
        }
        TapList<Instruction> inInstructions = instructions;
        while (inInstructions.tail != null) {
            inInstructions = inInstructions.tail;
        }
        return inInstructions.head;
    }

    /**
     * @return the label that may be attached to the first instruction of this Block.
     */
    public String origLabel() {
        return origLabel;
    }

    /**
     * Set the label attached to the first instruction of this Block.
     */
    public void setOrigLabel(String label) {
        origLabel = label;
    }

    /**
     * @return the container Block in the hierarchy of nested loop Blocks.
     */
    public LoopBlock enclosingLoop() {
        return (LoopBlock) enclosing;
    }

    /**
     * Sets the container Block in the hierarchy of nested loop Blocks.
     */
    public void setEnclosingLoop(LoopBlock enclosingLoop) {
        this.enclosing = enclosingLoop;
    }

    /**
     * @return the effect of this Block with respect to AssGotoInfo's.
     */
    public Block[] getLabelVariablesEffect() {
        return (Block[]) tmp1;
    }

    /**
     * Sets the effect of this Block with respect to AssGotoInfo's.
     */
    public void setLabelVariablesEffect(Block[] effectAssGoto) {
        tmp1 = effectAssGoto;
    }

    /**
     * Gets this Block's Tree regeneration ControlStruct (using field "tmp1").
     */
    public ControlStruct getControlStruct() {
        return (ControlStruct) tmp1;
    }

    /**
     * Sets this Block's Tree regeneration ControlStruct (using field "tmp1").
     */
    public void setControlStruct(ControlStruct controlStruct) {
        tmp1 = controlStruct;
    }

    /**
     * Gets the rank of the variable in the computed GOTO that
     * terminates this block (using field "tmp2"). Must be -1 if no GOTO.
     */
    public int getFinalGotoRank() {
        return tmp2;
    }

    /**
     * Sets the rank of the variable in the computed GOTO that
     * terminates this block (using field "tmp2"). Must be -1 if no GOTO.
     */
    public void setFinalGotoRank(int rank) {
        tmp2 = rank;
    }

    /**
     * Gets the list of AssGotoInfo's that have already been propagated through this Block.
     */
    public TapList<Block[]> getAssGotoInfoDone() {
        return (TapList<Block[]>) tmp3;
    }

    /**
     * Sets the list of AssGotoInfo's that have already been propagated through this Block.
     */
    public void setAssGotoInfoDone(TapList<Block[]> assGotoInfoDone) {
        tmp3 = assGotoInfoDone;
    }

    /**
     * Gets this Block's BoolVector of dominating FGArrow's (using field "tmp3").
     */
    public BoolVector getDominatorArrows() {
        return (BoolVector) tmp3;
    }

    /**
     * Sets this Block's BoolVector of dominating FGArrow's (using field "tmp3").
     */
    protected void setDominatorArrows(BoolVector dominators) {
        tmp3 = dominators;
    }

    /**
     * Replaces oldBlock with this Block and updates all arrows.
     *
     * @param oldBlock Block to replace.
     */
    protected final void replaceInFlowGraph(Block oldBlock) {
        instructions = oldBlock.instructions;
        origLabel = oldBlock.origLabel;
        if (this instanceof HeaderBlock && oldBlock instanceof HeaderBlock) {
            ((HeaderBlock) this).declaresItsIterator =
                    ((HeaderBlock) oldBlock).declaresItsIterator;
        }

        TapList<FGArrow> oldFlow = oldBlock.flow;
        while (oldFlow != null) {
            oldFlow.head.origin = this;
            oldFlow = oldFlow.tail;
        }
        flow = oldBlock.flow;
        oldBlock.flow = null;

        TapList<FGArrow> oldBackFlow = oldBlock.backFlow;
        while (oldBackFlow != null) {
            oldBackFlow.head.destination = this;
            oldBackFlow = oldBackFlow.tail;
        }
        backFlow = oldBlock.backFlow;
        oldBlock.backFlow = null;
    }

    /**
     * @return the enclosingOrigUnit of this Block.
     * May return null if Flow Graph is not finished building.
     */
    public Unit unit() {
        return symbolTable == null ? null : symbolTable.unit;
    }

    /**
     * When necessary, makes this current Block the new declarationsBlock
     * of its SymbolTable. When "modelBlock" was the declarationsBlock of
     * its own symbolTable, and assuming that "this" new Block is in fact
     * a copy of "modelBlock", then fills the "declarationsBlock" field
     * of their symbolTable so that it points to "this" new block instead
     * of to the old "modelBlock".
     */
    protected void newDeclarationsBlock(Block modelBlock) {
        if (modelBlock.symbolTable != null &&
                modelBlock.symbolTable.declarationsBlock == modelBlock) {
            modelBlock.symbolTable.declarationsBlock = this;
        }
    }

    /**
     * Places most elements of this Block into "newBlock".
     * Copies instructions, labels, rank, and where masks
     * Doesn't copy Flow Graph arrows.
     * Uses "alreadyCopiedMasks" to make sure that copied whereMask's keep equal (==) tests.
     */
    protected void copyIntoWithoutArrows(Block newBlock, TapList<TapPair<Instruction, Instruction>> alreadyCopiedMasks) {
        // for HeaderBlock's
        newBlock.enclosing = enclosing;
        newBlock.flow = null;
        newBlock.backFlow = null;
        newBlock.origLabel = origLabel;
        newBlock.rank = rank;
        TapList<Instruction> instrs = instructions;
        Instruction instr;
        TapList<Instruction> hdCopiedInstrs = new TapList<>(null, null);
        TapList<Instruction> tlCopiedInstrs = hdCopiedInstrs;
        Instruction copiedInstr;
        while (instrs != null) {
            instr = instrs.head;
            copiedInstr = instr.copy(alreadyCopiedMasks);
            // If original instruction was split, remove the "sourcetree" annotation
            // so that code decompilation doesn't reuse the original instruction tree:
            if (copiedInstr.tree!=null && copiedInstr.tree.getAnnotation("hasBeenSplit")==Boolean.TRUE
                // Exception: don't do this on overloaded assignments.
                && !ILUtils.isOverloadedAssign(copiedInstr.tree)) {
                copiedInstr.tree.removeAnnotation("sourcetree");
                ILUtils.putBackNamedArguments(copiedInstr.tree) ;
            }

            copiedInstr.block = newBlock;
            tlCopiedInstrs = tlCopiedInstrs.placdl(copiedInstr);
            instrs = instrs.tail;
        }
        newBlock.instructions = hdCopiedInstrs.tail;
    }

    /**
     * Fills this Block with a copy of the modelBlock instructions.
     * Instruction's are copied too. Tree's are copied.
     */
    public void copyInstructions(Block modelBlock,
                                 TapList<TapPair<Instruction, Instruction>> copiedIncludes) {
        TapList<Instruction> hdCopiedInstrs = new TapList<>(null, null);
        TapList<Instruction> tlCopiedInstrs = hdCopiedInstrs;
        TapList<Instruction> origInstrs = modelBlock.instructions;
        Instruction origInstr;
        Instruction copiedInstr;
        while (origInstrs != null) {
            origInstr = origInstrs.head;
            copiedInstr = new Instruction(ILUtils.copy(origInstr.tree), null, this);
            copiedInstr.setWhereMask(origInstr.whereMask());
            if (origInstr.fromInclude() != null) {
                copiedInstr.setFromInclude(getSetCopiedInclude(origInstr.fromInclude(), copiedIncludes));
            }
            copiedInstr.preComments = origInstr.preComments;
            copiedInstr.preCommentsBlock = origInstr.preCommentsBlock;
            copiedInstr.postComments = origInstr.postComments;
            copiedInstr.postCommentsBlock = origInstr.postCommentsBlock;
            // [vmp] est-ce utile de copier les directives?
            //copiedInstr.directives = origInstr.directives;
            tlCopiedInstrs = tlCopiedInstrs.placdl(copiedInstr);
            origInstrs = origInstrs.tail;
        }
        instructions = hdCopiedInstrs.tail;
    }

    private Instruction getSetCopiedInclude(Instruction origInclude,
                                            TapList<TapPair<Instruction, Instruction>> copiedIncludes) {
        TapPair<Instruction, Instruction> found = TapList.assq(origInclude, copiedIncludes.tail);
        if (found == null) {
            found = new TapPair<>(origInclude, null);
            copiedIncludes.placdl(found);
        }
        if (found.second == null) {
            Instruction copiedInclude = new Instruction(ILUtils.copy(origInclude.tree), null, this);
            found.second = copiedInclude;
            if (origInclude.fromInclude() != null) {
                copiedInclude.setFromInclude(getSetCopiedInclude(origInclude.fromInclude(), copiedIncludes));
            }
            copiedInclude.preComments = origInclude.preComments;
            copiedInclude.preCommentsBlock = origInclude.preCommentsBlock;
            copiedInclude.postComments = origInclude.postComments;
            copiedInclude.postCommentsBlock = origInclude.postCommentsBlock;
        }
        return found.second;
    }

    /**
     * Adds a new use statement Instruction at the head of this Block.
     *
     * @param instr fortran use statement.
     */
    public void addInstrHdIfNotPresent(Instruction instr) {
        boolean found = false;
        if (instructions != null) {
            TapList<Instruction> curInstructionListCell = instructions;
            Instruction curInstr;
            while (curInstructionListCell != null && !found) {
                curInstr = curInstructionListCell.head;
                found = instr.tree.equalsTree(curInstr.tree);
                curInstructionListCell = curInstructionListCell.tail;
            }
        }
        if (!found) {
            addInstrHd(instr);
        }
    }

    /**
     * Adds a new Instruction with "tree" at the head of this Block.
     */
    public void addInstrHd(Tree tree) {
        addInstrHd(new Instruction(tree, null, this));
    }

    /**
     * Adds instruction "instr" at the head of this Block.
     */
    public void addInstrHd(Instruction instr) {
        instructions = new TapList<>(instr, instructions);
    }

    /**
     * Adds use declaration.
     */
    public void addUseDecl(Tree tree) {
        TapList<Instruction> inInstructions = this.instructions;
        int place = 0;
        while (inInstructions != null
                && (inInstructions.head.tree.opCode() == ILLang.op_implicit
                || inInstructions.head.tree.opCode() == ILLang.op_useDecl)) {
            ++place;
            inInstructions = inInstructions.tail;
        }
        addInstructionAt(tree, place);
    }

    /**
     * @return the first instruction of this Block which is not a declaration.
     */
    public Instruction firstInstrAfterDecls() {
        TapList<Instruction> inInstructions = instructions;
        Instruction found = null;
        while (inInstructions != null && found == null) {
            if (!(inInstructions.head.isADeclaration() || inInstructions.head.isANoOp())) {
                found = inInstructions.head;
            }
            inInstructions = inInstructions.tail;
        }
        return found;
    }

    /**
     * Adds a new Instruction with "tree" at the head of this Block
     * BUT after the declarations.
     *
     * @return the new Instruction created.
     */
    public Instruction addInstrHdAfterDecls(Tree tree) {
        Instruction newInstr = new Instruction(tree, null, this);
        addInstrHdAfterDecls(newInstr);
        return newInstr;
    }

    /**
     * Adds instruction "instr" at the head of this Block
     * BUT after the declarations.
     */
    public void addInstrHdAfterDecls(Instruction instr) {
        TapList<Instruction> toInstructions = new TapList<>(null, instructions);
        TapList<Instruction> inInstructions = toInstructions;
        while (inInstructions.tail != null
               && (inInstructions.tail.head.isADeclaration()
                   || inInstructions.tail.head.isANoOp())) {
            inInstructions = inInstructions.tail;
        }
        inInstructions.tail = new TapList<>(instr, inInstructions.tail);
        instr.block = this;
        instructions = toInstructions.tail;
    }

    /**
     * Adds instruction tree "instr" at the end of this Block.
     */
    public void addInstrTl(Tree instr) {
        addInstrTl(new Instruction(instr, null, this));
    }

    /**
     * Adds instruction "instr" at the end of this Block.
     */
    public void addInstrTl(Instruction instr) {
        if (instructions == null) {
            instructions = new TapList<>(instr, null);
        } else {
            TapList<Instruction> inInstructions = instructions;
            while (inInstructions.tail != null) {
                inInstructions = inInstructions.tail;
            }
            inInstructions.tail = new TapList<>(instr, null);
        }
        if (instr.block == null) {
            instr.block = this;
        }
    }

    /**
     * @return true if instr is already in this Block
     * and is not in an include.
     */
    public boolean alreadyPresent(Instruction instr) {
        boolean found = false;
        if (instr.fromInclude() == null) {
            TapList<Instruction> instrs = instructions;
            Instruction curInstr;
            while (!found && instrs != null) {
                curInstr = instrs.head;
                found = instr.tree.equalsTree(curInstr.tree);
                instrs = instrs.tail;
            }
        }
        return found;
    }

    /**
     * Adds instruction "instr" after the declarations in this Block
     * BUT before the declarations that depend on this declaration.
     *
     * @param symbolDecls the symbols that the given "instr" declares or defines.
     * @param declST      The SymbolTable where the new "symbolDecls" will be.
     * @param strict      when true, vars in a COMMON must be declared before the COMMON.
     * @return when the instruction was moved some lines before its original
     * location, to be before using declarations or computation instructions,
     * returns the number of locations moved up. Otherwise returns 0.
     */
    public int addInstrDeclTlBeforeUse(Instruction instr, TapList<SymbolDecl> symbolDecls,
                                       TapList<Instruction> usageInstructions,
                                       SymbolTable declST, boolean strict) {
        int movedUp = 0;
        if (instructions != null) {
            if (declST == null) {
                declST = this.symbolTable;
            }
            TapList<Instruction> hdInstructions = new TapList<>(null, instructions);
            TapList<Instruction> inInstructions = hdInstructions;
            Instruction nextInstr;
            boolean placeItHere = false;
            // Place the new declaration statement "instr", at a location which is
            // immediately after the last declaration that is before any usage:
            while (!placeItHere && inInstructions.tail != null) {
                nextInstr = inInstructions.tail.head;
                // Place it here if this is the end of the declaration section:
                placeItHere = 
                    // if next instruction is not part of the declaration section:
                    !(nextInstr.isADeclaration()
                      || nextInstr.isANoOp()
                      || (symbolTable!=null && unit().isFortran() && FortranStuff.isStatementFunctionDecl(symbolTable, nextInstr.tree)))
                    ||  // or if, in Fortran, we are reaching the "CONTAINS" section:
                    TapEnv.isFortran(declST.language()) && nextInstr.isAProcedure()
                    ||  // or if next instruction is signalled as a usage:
                    usageInstructions != null && usageInstructions.tail != null && TapList.contains(usageInstructions, nextInstr)
                    ||  // or if next instruction uses this symbolDecl:
                    symbolDecls != null && dependsOn(nextInstr.tree, symbolDecls, instr, declST, strict);
                if (!placeItHere) {
                    inInstructions = inInstructions.tail;
                }
            }
            movedUp = TapList.length(inInstructions.tail);
            inInstructions.tail = new TapList<>(instr, inInstructions.tail);
            instructions = hdInstructions.tail;
        } else {
            instructions = new TapList<>(instr, null);
        }
        if (instr.block == null) {
            instr.block = this;
        }
        return movedUp;
    }

    /**
     * Adds instructions "decls" after the declarations in this Block.
     */
    public void addInstrDeclTl(TapList<Instruction> decls) {
        if (instructions != null) {
            TapList<Instruction> curInstructionListCell = instructions;
            boolean ok = false;
            Instruction nextInstr = curInstructionListCell.head;
            if (!(nextInstr.isADeclaration() || nextInstr.isANoOp() || (symbolTable!=null && unit().isFortran() && FortranStuff.isStatementFunctionDecl(symbolTable, nextInstr.tree)))) {
                instructions = TapList.append(decls, instructions);
            } else {
                while (curInstructionListCell.tail != null && !ok) {
                    nextInstr = curInstructionListCell.tail.head;
                    if (!(nextInstr.isADeclaration() || nextInstr.isANoOp() || (symbolTable!=null && unit().isFortran() && FortranStuff.isStatementFunctionDecl(symbolTable, nextInstr.tree)))) {
                        curInstructionListCell.tail =
                                TapList.append(decls, curInstructionListCell.tail);
                        ok = true;
                    }
                    curInstructionListCell = curInstructionListCell.tail;
                }
                if (!ok) {
                    curInstructionListCell.tail =
                            TapList.append(decls, curInstructionListCell.tail);
                }
            }
        } else {
            instructions = decls;
        }
    }

    /**
     * Add inside this Block a new Instruction containing the given tree,
     * at a location given by "place".
     * If place is positive or 0, location is counted from the head down,
     * and 0 means before the 1st instruction. Too large place inserts at the tail.
     * If place is negative, location is counted from the tail up,
     * and -1 means after the last instruction. Too large (-)place inserts at head.
     */
    public void addInstructionAt(Tree tree, int place) {
        TapList<Instruction> hdInstructions = new TapList<>(null, this.instructions);
        TapList<Instruction> inInstructions = hdInstructions;
        if (place < 0) {
            int length = TapList.length(this.instructions);
            place = place + length < 0 ? 0 : place + length + 1;
        }
        while (place > 0 && inInstructions.tail != null) {
            inInstructions = inInstructions.tail;
            --place;
        }
        inInstructions.placdl(new Instruction(tree, null, this));
        this.instructions = hdInstructions.tail;
    }

    /**
     * @return rank to insert the declaration Tree "tree" after the declarations in this fileTree
     * BUT before the declarations that depends on this declaration.
     * [llh] TODO: I'm not sure this code is right, e.g. dependsOn() test wrongly ordered?
     */
    public int findRankToInsert(Tree fileTree, Tree tree) {
        Instruction instr = new Instruction(tree, null, this);
        Tree[] sons = fileTree.children();
        if (sons.length == 0) {
            // This special call to dependsOn() mostly manages forward declarations:
            dependsOn(tree, null, instr, this.symbolTable, true);
            return 1;
        }
        Tree nextTree;
        int i = sons.length - 1;
        int nbIncludes = 0;
        boolean found = false;
        while (!found && i >= 0) {
            nextTree = sons[i];
            if (nextTree != null && nextTree.opCode() != ILLang.op_include) {
                if (dependsOn(tree, symbolDeclDeclared(nextTree, null), instr, this.symbolTable, true)
                    || tree.opCode() == ILLang.op_function && nextTree.opCode() == ILLang.op_typeDeclaration
                    || !(ILUtils.isADeclaration(nextTree) || nextTree.opCode()==ILLang.op_none)) {
                    found = true;
                }
            } else {
                ++nbIncludes;
            }
            --i;
        }
        if (nbIncludes != 0) {
            ++nbIncludes;
        }
        return found ? i + 3 : nbIncludes;
    }

    /**
     * @return the list of all SymbolDecl's defined in Instruction.
     */
    public TapList<SymbolDecl> symbolDeclDeclared(Instruction instruction) {
        return symbolDeclDeclared(instruction.tree, null);
    }

    /**
     * @return the list of all SymbolDecl's defined in Tree.
     */
    public TapList<SymbolDecl> symbolDeclDeclared(Tree tree, SymbolTable inSymbolTable) {
        TapList<SymbolDecl> toResult = new TapList<>(null, null);
        TapList<SymbolDecl> symbolDecls = toResult;
        int op = tree.opCode();
        SymbolDecl symbolDecl;
        String name;
        SymbolTable symbolTable;
        if (inSymbolTable != null) {
            symbolTable = inSymbolTable;
        } else if (this.unit() != null) {
            if (this.unit().isModule()) {
                symbolTable = this.unit().privateSymbolTable();
            } else {
                symbolTable = this.symbolTable;
            }
        } else {
            if (TapEnv.relatedUnit() != null) {
                symbolTable = TapEnv.relatedUnit().privateSymbolTable();
            } else {
                symbolTable = this.symbolTable;
            }
        }
        switch (op) {
            case ILLang.op_varDeclaration:
                Tree declarators = tree.down(3);
                if (declarators.opCode() == ILLang.op_declarators) {
                    for (int i = 1; i <= declarators.length(); i++) {
                        name = ILUtils.baseName(declarators.down(i));
                        if (name != null) {
                            if (symbolTable != null) {
                                symbolDecl = symbolTable.getSymbolDecl(name);
                                if (symbolDecl != null) {
                                    symbolDecls.placdl(symbolDecl);
                                }
                            }
                        }
                    }
                }
                break;
            case ILLang.op_varDimDeclaration:
                for (int i = 1; i <= tree.length(); i++) {
                    name = ILUtils.baseName(tree.down(i));
                    if (name != null) {
                        if (symbolTable != null) {
                            symbolDecl = symbolTable.getSymbolDecl(name);
                            if (symbolDecl != null) {
                                symbolDecls.placdl(symbolDecl);
                            }
                        }
                    }
                }
                break;
            case ILLang.op_constDeclaration:
                break;
            case ILLang.op_typeDeclaration:
                name = ILUtils.baseName(tree.down(1));
                if (name != null) {
                    if (symbolTable != null) {
                        symbolDecl = symbolTable.getSymbolDecl(name);
                        if (symbolDecl != null) {
                            symbolDecls.placdl(symbolDecl);
                        }
                    }
                }
                break;
            case ILLang.op_functionDeclarator:
                break;
            case ILLang.op_accessDecl:
                if (tree.down(2).opCode() == ILLang.op_typeDeclaration) {
                    name = tree.down(2).down(1).stringValue();
                    if (name != null && symbolTable != null) {
                        symbolDecl = symbolTable.getSymbolDecl(name);
                        if (symbolDecl != null) {
                            symbolDecls.placdl(symbolDecl);
                        }
                    }
// [llh] we comment to enforce that an accessDecl merely modifies but doesn't really "declare"
//                 } else {
//                     for (int i = 1; i <= tree.down(2).length(); i++) {
//                         name = ILUtils.baseName(tree.down(2).down(i));
//                         if (name != null && symbolTable != null) {
//                             symbolDecl = symbolTable.getSymbolDecl(name);
//                             if (symbolDecl != null) {
//                                 symbolDecls.placdl(symbolDecl);
//                             }
//                         }
//                     }
                }
                break;
            case ILLang.op_external:
                for (int i = 1; i <= tree.length(); i++) {
                    name = ILUtils.baseName(tree.down(i));
                    if (name != null) {
                        if (symbolTable != null) {
                            symbolDecl = symbolTable.getSymbolDecl(name);
                            if (symbolDecl != null) {
                                symbolDecls.placdl(symbolDecl);
                            }
                        }
                    }
                }
                break;
            case ILLang.op_interfaceDecl:
                if (tree.down(1).opCode() == ILLang.op_ident) {
                    name = tree.down(1).stringValue();
                    if (name != null && symbolTable != null) {
                        symbolDecl = symbolTable.getSymbolDecl(name);
                        if (symbolDecl != null) {
                            symbolDecls.placdl(symbolDecl);
                        }
                    }
                }
                for (int i = 1; i <= tree.down(2).length(); i++) {
                    if (tree.down(2).down(i).opCode() == ILLang.op_interface) {
                        name = ILUtils.getUnitName(tree.down(2).down(i).down(1));
                        if (name != null && symbolTable != null) {
                            symbolDecl = symbolTable.getSymbolDecl(name);
                            if (symbolDecl != null) {
                                symbolDecls.placdl(symbolDecl);
                            }
                        }
                    }
                }
                break;
            case ILLang.op_intrinsic:
                break;
            case ILLang.op_save:
                break;
            case ILLang.op_function:
            case ILLang.op_program:
            case ILLang.op_module:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
            case ILLang.op_nameSpace: {
                Tree unitName = tree.down(op == ILLang.op_function ? 4 :
                        op == ILLang.op_module || op == ILLang.op_nameSpace ? 1 : 2);
                // Neglect the Unit place-holder instructions:
                if (!ILUtils.isNullOrNone(unitName) && tree.getAnnotation("Unit") == null && symbolTable != null) {
                    name = unitName.stringValue();
                    if (name != null) {
                        symbolDecl = symbolTable.getSymbolDecl(name);
                        if (symbolDecl != null) {
                            symbolDecls.placdl(symbolDecl);
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
        return toResult.tail;
    }

    /**
     * @param strict when true, we consider that a COMMON "tree" dependsOn the declaration of its contained variables.
     * @return true if tree depends on one of the symbolDecl in symbolDecls (that are defined by defInstr).
     */
    private boolean dependsOn(Tree tree, TapList<SymbolDecl> symbolDecls, Instruction defInstr,
                              SymbolTable declST, boolean strict) {
        boolean result = false;
        // We say that the new instruction "defInstr" is not blocked on its way down to the last declaration location by an
        // instruction (in "tree") which is a public/private/external/intrinsic declaration.
        // In other words, the public/etc declaration "tree" may appear first, before "defInstr", in the list of declarations.
        if (tree != null && !ILUtils.isADeclMayAppearFirst(tree)) {
            SymbolDecl symbolDecl;
            SymbolTable symbolTable = this.symbolTable;
            if (unit() != null && (symbolTable == null || unit().isModule())) {
                symbolTable = unit().privateSymbolTable();
            }
            TapList<SymbolDecl> toUsedSymbolsInTree = new TapList<>(null, null);
            SymbolDecl.addUsedSymbolsInExpr(tree, toUsedSymbolsInTree, declST, null, false, strict);
            TapList<SymbolDecl> inSymbolDecls = symbolDecls;
            while (inSymbolDecls != null && !result) {
                symbolDecl = inSymbolDecls.head;
                result = dependsOnOne(tree, symbolDecl, defInstr, symbolTable);
                if (!result) {
                    result = TapList.containsSameNameSymbolDecl(toUsedSymbolsInTree.tail, symbolDecl);
                }
                inSymbolDecls = inSymbolDecls.tail;
            }
        }
        return result;
    }

    /**
     * @param defInstr defines symbolDecl.
     * @return true if tree depends on symbolDecl.
     */
    private boolean dependsOnOne(Tree tree, SymbolDecl symbolDecl, Instruction defInstr,
                                 SymbolTable symbolTable) {
        boolean depends = false;
        int op = tree.opCode();
        SymbolDecl symbDecl = null;
        switch (op) {
            case ILLang.op_varDeclaration:
            case ILLang.op_constDeclaration: {
                String baseName = ILUtils.baseName(tree.down(op == ILLang.op_constDeclaration ? 1 : 2));
                if (baseName != null) {
                    symbDecl = symbolTable.getSymbolDecl(baseName);
                }
                if (symbolDecl == null) {
                    depends = false;
                } else {
                    depends = symbDecl == symbolDecl && !symbDecl.isA(SymbolTableConstants.FUNCTION)
                            || symbDecl != null &&
                            TapList.contains(symbDecl.dependsOn, symbolDecl)
                            || symbDecl != null && symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                            TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl);
                }
                Tree[] declarators = tree.down(op == ILLang.op_constDeclaration ? 2 : 3).children();
                for (int i = declarators.length - 1; !depends && i >= 0; --i) {
                    symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(declarators[i]));
                    if (symbolDecl == null) {
                        depends = false;
                    } else {
                        depends = symbDecl == symbolDecl && !symbDecl.isA(SymbolTableConstants.FUNCTION)
                                || symbDecl != null &&
                                TapList.contains(symbDecl.dependsOn, symbolDecl)
                                || symbDecl != null && symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                                TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl);
                    }
                    if ((defInstr.tree.opCode() == ILLang.op_varDimDeclaration
                            || defInstr.tree.opCode() == ILLang.op_accessDecl)
                            && symbDecl == symbolDecl) {
                        depends = false;
                    }
                }

                break;
            }
            case ILLang.op_varDimDeclaration: {
                int i = tree.length();
                while (!depends && i > 0) {
                    symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(tree.down(i)));
                    depends = symbDecl != null
                            && (symbDecl == symbolDecl
                            || TapList.contains(symbDecl.dependsOn, symbolDecl)
                            || symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                            TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl));
                    i = i - 1;
                }
                break;
            }
            case ILLang.op_common: {
                int i = tree.down(2).length();
                while (!depends && i > 0) {
                    symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(tree.down(2).down(i)));
                    depends = (symbDecl != null &&
                            (TapList.contains(symbDecl.dependsOn, symbolDecl)
                                    || (symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                                    TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl))));
                    i = i - 1;
                }
                break;
            }
            case ILLang.op_equivalence: {
                int i = tree.length();
                int j;
                while (!depends && i > 0) {
                    j = tree.down(i).length();
                    while (!depends && j > 0) {
                        symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(tree.down(i).down(j)));
                        depends = (symbDecl != null &&
                                (symbDecl == symbolDecl
                                        || TapList.contains(symbDecl.dependsOn, symbolDecl)
                                        || (symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                                        TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl))));
                        j = j - 1;
                    }
                    i = i - 1;
                }
                break;
            }
            case ILLang.op_typeDeclaration:
                if (tree.down(1).opCode() == ILLang.op_ident) {
                    symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(tree.down(1)));
                    depends = symbDecl != null && symbolDecl != null &&
                            TapList.containsEquals(symbDecl.dependsOn, symbolDecl);
                }
                break;
            case ILLang.op_accessDecl:
                if (!ILUtils.isIdent(tree.down(1), "bind", false)) {
                    tree = tree.down(2);
                    int i = tree.length();
                    if (tree.opCode() != ILLang.op_typeDeclaration) {
                        while (!depends && i > 0) {
                            symbDecl = symbolTable.getSymbolDecl(ILUtils.baseName(tree.down(i)));
                            depends = symbDecl != null
                                    && (symbDecl == symbolDecl
                                    || TapList.contains(symbDecl.dependsOn, symbolDecl)
                                    || symbDecl != null && symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                                    TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl));
                            i = i - 1;
                        }
                    }
                }
                break;
            case ILLang.op_function: {
                String funcName = ILUtils.baseName(tree.down(4));
                if (funcName != null) {
                    symbDecl = symbolTable.getSymbolDecl(funcName);
                    depends = symbDecl != null &&
                            TapList.contains(symbDecl.dependsOn, symbolDecl)
                            || symbDecl != null && symbolDecl.isA(SymbolTableConstants.VARIABLE) &&
                            TapList.containsVariableDecl(symbDecl.dependsOn, (VariableDecl) symbolDecl);
                }
                break;
            }
            case ILLang.op_intrinsic:
            case ILLang.op_save:
            case ILLang.op_functionDeclarator:
            case ILLang.op_external:
            case ILLang.op_interfaceDecl:
            case ILLang.op_useDecl:
                break;
            default:
                break;
        }
        return depends;
    }

    /**
     * Removes "instr" from this Block.
     */
    public void removeInstr(Instruction instr) {
        TapList<Instruction> toInstructions = new TapList<>(null, instructions);
        TapList<Instruction> inInstructions = toInstructions;
        while (inInstructions.tail != null && inInstructions.tail.head != instr) {
            inInstructions = inInstructions.tail;
        }
        if (inInstructions.tail != null) {
            inInstructions.tail = inInstructions.tail.tail;
        }
        instructions = toInstructions.tail;
    }

    /**
     * Removes external symbolDecl's declaration-instruction from this Block.
     */
    protected void removeExternalInstr(SymbolDecl symbolDecl) {
        TapList<Instruction> toInstructions = new TapList<>(null, instructions);
        TapList<Instruction> inInstructions = toInstructions;
        while (inInstructions.tail != null
                && !(inInstructions.tail.head.tree.opCode() == ILLang.op_external
                && inInstructions.tail.head.tree.down(1).stringValue().equals(symbolDecl.symbol))) {
            inInstructions = inInstructions.tail;
        }
        if (inInstructions.tail != null) {
            inInstructions.tail = inInstructions.tail.tail;
        }
        instructions = toInstructions.tail;
    }

    /**
     * @param operator == op_varDeclaration or op_typeDeclaration or op_interfaceDecl.
     * @return the varDeclaration or typeDeclaration or interfaceDecl Instruction
     * that declares this symbolDecl in this Block.
     */
    public Instruction getOperatorDeclarationInstruction(
            SymbolDecl symbolDecl, int operator, SymbolTable symbolTable) {
        Instruction result = null;
        TapList<Instruction> instrs = instructions;
        Instruction curInstr;
        while (instrs != null && result == null) {
            curInstr = instrs.head;
            if (curInstr.tree != null
                    && curInstr.tree.opCode() == operator) {
                TapList<SymbolDecl> symbolDecls = symbolDeclDeclared(curInstr.tree, symbolTable);
                if (TapList.containsEquals(symbolDecls, symbolDecl)) {
                    result = curInstr;
                }
            }
            instrs = instrs.tail;
        }
        return result;
    }

    /**
     * Inserts the given "block" before this Block in the Flow Graph.
     * This requires that the given "block" does not end with a test.
     */
    public void insertBlockBefore(Block block) {
        TapList<FGArrow> arrows = backFlow;
        block.backFlow = arrows;
        backFlow = null;
        while (arrows != null) {
            arrows.head.destination = block;
            arrows = arrows.tail;
        }
        new FGArrow(block, FGConstants.NO_TEST, null, this);
    }

    /**
     * Removes this Block from its containing Flow Graph, redirecting all
     * arrows flowing to it towards its unique flow destination.
     * This requires that this Block has only one exiting flow arrow.
     */
    public void removeAndShunt() {
        if (flow == null || flow.tail != null) {
            TapEnv.toolWarning(-1, "Removing a Block with more than one flow arrow");
        } else {
            FGArrow flowArrow = flow.head;
            Block futureDest = flowArrow.destination;
            FGArrow backFlowArrow;
            flowArrow.redirectDestination(null);
            TapList<FGArrow> inBackFlow = backFlow;
            while (inBackFlow != null) {
                backFlowArrow = inBackFlow.head;
                backFlowArrow.redirectDestination(futureDest);
                backFlowArrow.inACycle = flowArrow.inACycle;
                backFlowArrow.isAJumpIntoNextCase = flowArrow.isAJumpIntoNextCase;
                inBackFlow = inBackFlow.tail;
            }
        }
    }

    /**
     * @return true if this Block has a unique instruction which is an op_loop,
     * used to control other Blocks. In particular returns false if it is an
     * op_loop that already contains a body, which means it is a closed instruction
     * that is equivalent in this Flow Graph to an atomic, simple instruction.
     */
    public boolean isALoop() {
        return instructions != null && instructions.tail == null
                && instructions.head.tree != null
                && instructions.head.tree.opCode() == ILLang.op_loop
                && instructions.head.tree.down(4).opCode() == ILLang.op_none;
    }

    /**
     * @return true if this block "isALoop" and is the Header of a DO-loop.
     */
    public boolean isADoLoop() {
        return enclosing != null
                && isALoop()
                && instructions.head.tree.down(3).opCode() == ILLang.op_do
                && ((LoopBlock) enclosing).header() == this;
    }

    /**
     * @return true if this is a standard DO loop HeaderBlock, marked as loopIndexUnusedAfterLoop.
     * Do not test on the "enclosing" field, as this method is called on differentiated Blocks,
     * for which the "enclosing" field has not been conputed
     */
    public boolean isDoWithIndexUnusedAfterLoop() {
        return this instanceof HeaderBlock
                && ((HeaderBlock) this).loopIndexUnusedAfterLoop
                && isALoop()
                && instructions.head.tree.down(3).opCode() == ILLang.op_do;
    }

    /**
     * @return true if this block "isALoop" and is the Header of a TIMES-loop.
     */
    public boolean isATimesLoop() {
        return enclosing != null
                && isALoop()
                && instructions.head.tree.down(3).opCode() == ILLang.op_times
                && ((LoopBlock) enclosing).header() == this;
    }

    /**
     * @return true if this Block "isALoop" and is the HeaderBlock of a normal DO-loop,
     * which has no dirty jumps in or out of the loop.
     */
    public boolean isACleanDoLoop() {
        return enclosing != null
                && isALoop()
                && instructions.head.tree.down(3).opCode() == ILLang.op_do
                && ((LoopBlock) enclosing).entryBlocks != null
                && ((LoopBlock) enclosing).entryBlocks.tail == null
                && ((LoopBlock) enclosing).entryBlocks.head == this
                && ((LoopBlock) enclosing).exitArrows != null
                && ((LoopBlock) enclosing).exitArrows.tail == null
                && ((LoopBlock) enclosing).exitArrows.head.origin == this;
    }

    /**
     * @return true if this is a control block, i.e a block
     * terminated by a control flow instruction.
     * When this Block is a header, it is a control even if it has no LOOP:exit arrow.
     */
    public boolean isControl() {
        return (flow != null && flow.tail != null) || this instanceof HeaderBlock || this.isParallelController();
    }

    /**
     * @return true if this Block is the controller of a parallel region or loop e.g. $OMP PARALLEL.
     */
    public boolean isParallelController() {
        return instructions != null && ILUtils.isParallelController(instructions.head.tree);
    }

    /**
     * @return the op_code of the tail test of this Block
     * If the last instruction of this block is a test, returns its
     * op_code, otherwise returns -1.
     */
    public int lastTest() {
        Instruction lastInstruction = lastInstr();
        int code = -1;
        if (lastInstruction != null && lastInstruction.tree != null) {
            code = lastInstruction.tree.opCode();
            if (code != ILLang.op_if &&
                    code != ILLang.op_loop &&
                    code != ILLang.op_where &&
                    code != ILLang.op_call &&
                    code != ILLang.op_ioCall &&
                    code != ILLang.op_switch &&
                    code != ILLang.op_switchType &&
                    code != ILLang.op_compGoto &&
                    code != ILLang.op_gotoLabelVar &&
                    code != ILLang.op_return) {
                code = -1;
            }
        }
        return code;
    }

    /**
     * @return true if the block terminates with an if test, which is a real
     * "if" that controls the next choice in the control flow.
     */
    public boolean isAnIf() {
        Instruction lastInstr = lastInstr();
        if (lastInstr != null && lastInstr.tree != null
                && lastInstr.tree.opCode() == ILLang.op_if) {
            Tree b1 = lastInstr.tree.down(2);
            Tree b2 = lastInstr.tree.down(3);
            // flow peut etre null en mode inverse si
            // (Tree regeneration) wrong flow graph structure after block HeaderBlock#N
            return (b1 == null || b1.opCode() == ILLang.op_none)
                    && (b2 == null || b2.opCode() == ILLang.op_none)
                    && flow != null && flow.head != null && flow.head.test == FGConstants.IF;
        } else {
            return false;
        }
    }

    /**
     * @return true if the block terminates with a nameSpace, which is a "true"
     * analyzed nameSpace, meaning that its contents are in the next Block(s).
     */
    public boolean isANameSpace() {
        Instruction lastInstr = lastInstr();
        return lastInstr != null && lastInstr.tree != null
                && lastInstr.tree.opCode() == ILLang.op_nameSpace
                && ILUtils.isNullOrNone(lastInstr.tree.down(2));
    }

    /** Returns true if this block contains no meaningful code, and therefore can be skipped
     * See for instance in FlowGraphDifferentiator.allExitsTurn() */
    public boolean containsNoCode() {
        boolean noCode = true ;
        TapList<Instruction> inInstructions = instructions;
        while (inInstructions != null) {
            if (!inInstructions.head.isANoOp()) {
                noCode = false ;
            }
            inInstructions = inInstructions.tail;
        }
        return noCode && flow!=null && flow.tail==null ;
    }

    /**
     * @return the FGArrow from this Block to the given "nextBlock".
     */
    public FGArrow getFGArrowTo(Block nextBlock) {
        TapList<FGArrow> inFlow = flow;
        while (inFlow != null && inFlow.head.destination != nextBlock) {
            inFlow = inFlow.tail;
        }
        if (inFlow != null) {
            return inFlow.head;
        } else {
            return null;
        }
    }

    /**
     * @return the FGArrow from this Block and with the given test and case.
     */
    public FGArrow getFGArrowTestCase(int test, int oneCase) {
        FGArrow currentArrow;
        FGArrow thisCaseArrow = null;
        FGArrow defaultCaseArrow = null;
        TapList<FGArrow> arrows = flow;
        while (arrows != null && thisCaseArrow == null) {
            currentArrow = arrows.head;
            if (currentArrow.test == test) {
                if (currentArrow.test == FGConstants.NO_TEST || currentArrow.containsCase(oneCase)) {
                    thisCaseArrow = currentArrow;
                } else if (TapIntList.contains(currentArrow.cases, FGConstants.DEFAULT)) {
                    defaultCaseArrow = currentArrow;
                }
            }
            arrows = arrows.tail;
        }
        return thisCaseArrow != null ? thisCaseArrow : defaultCaseArrow;
    }

    /**
     * Very temporarily during FlowGraph building, enclosing may be only a Block.
     */
    protected Block enclosingBlock() {
        return enclosing;
    }

    /**
     * Very temporarily during FlowGraph building, enclosing may be only a Block.
     */
    protected void setEnclosingBlock(Block enclosingBlock) {
        enclosing = enclosingBlock;
    }

    /**
     * Sets "enclosingBlock" as the header
     * of the loop directly enclosing this Block.
     * "enclosingBlock" may be a HeaderBlock or a BasicBlock.<br>
     * Protection: this setting is not done if the
     * Block already has an enclosing loop and the new enclosing
     * is not contained in the old enclosing.
     */
    protected void setEnclosingBlockProtected(Block enclosingBlock) {
        if (enclosing == null) {
            enclosing = enclosingBlock;
        } else {
            Block aboveLoop = enclosingBlock.enclosing;
            while (aboveLoop != enclosing && aboveLoop != null) {
                aboveLoop = aboveLoop.enclosing;
            }
            if (aboveLoop != null) {
                enclosing = enclosingBlock;
            }
        }
    }

    /**
     * @return the list of all enclosing LoopBlock's,
     * from the inside out.
     */
    public TapList<LoopBlock> enclosingLoops() {
        if (enclosing == null) {
            return null;
        } else {
            return new TapList<>((LoopBlock) enclosing, enclosingLoop().enclosingLoopBlocks());
        }
    }

    public boolean enclosedIn(LoopBlock loopBlock) {
        if (enclosing==loopBlock) return true ;
        if (enclosing==null) return false ;
        return enclosing.enclosedIn(loopBlock) ;
    }

    /**
     * Merges current Block with successor "nextBlock".
     * The result is in the modified current Block.
     */
    protected void mergeWithSuccessor(Block nextBlock) {
        TapList<FGArrow> arrows;
        /*First append the two lists of instructions:*/
        if (instructions == null) {
            if (origLabel == null) {
                origLabel = nextBlock.origLabel;
            }
            instructions = nextBlock.instructions;
        } else {
            TapList<Instruction> inInstructions = instructions;
            while (inInstructions.tail != null) {
                inInstructions = inInstructions.tail;
            }
            inInstructions.tail = nextBlock.instructions;
        }
        nextBlock.instructions = null;
        /*then current Block receives the exit flow of "nextBlock" :*/
        arrows = nextBlock.flow;
        flow = arrows;
        while (arrows != null) {
            /*redirect the origin of these FGArrows:*/
            arrows.head.origin = this;
            arrows = arrows.tail;
        }
        nextBlock.flow = null;
        nextBlock.backFlow = null;
        // If a SymbolTable sees nextBlock as its declaration Block,
        // now it must see this block instead:
        if (nextBlock.symbolTable != null && nextBlock.symbolTable.declarationsBlock == nextBlock) {
            nextBlock.symbolTable.declarationsBlock = this;
        }
    }

    /**
     * Removes current Block, because we assume it is empty.
     * All flow arrows jump over it.
     */
    protected void skipEmpty() {
        if (flow == null) {
            while (backFlow != null) {
                backFlow.head.delete();
            }
        } else {
            FGArrow arrow = flow.head;
            boolean inCycle = arrow.inACycle;
            Block nextBlock = arrow.destination;
            arrow.delete();
            while (backFlow != null) {
                backFlow.head.destination = nextBlock;
                /*vmp: faut-il mettre inCycle sur toutes les fleches?
                 */
                if (inCycle) {
                    backFlow.head.inACycle = inCycle;
                }
                nextBlock.backFlow =
                        new TapList<>(backFlow.head, nextBlock.backFlow);
                backFlow = backFlow.tail;
            }
            if (inCycle && nextBlock instanceof HeaderBlock
                    && ((HeaderBlock) nextBlock).origCycleLabel() == null) {
                ((HeaderBlock) nextBlock).setOrigCycleLabel(origLabel);
            } else if (nextBlock.origLabel == null) {
                nextBlock.origLabel = origLabel;
            }
            //Redirect the link from the current SymbolTable to its declarations Block:
            if (symbolTable != null && symbolTable.declarationsBlock == this) {
                symbolTable.declarationsBlock = nextBlock;
            }
        }
        flow = null;
        backFlow = null;
    }

    /**
     * Exchanges the arrows that flow from current Block to Blocks
     * "dest1" and "dest2". This only affects operations that sweep
     * all destinations from a block, that would now encounter the
     * two destinations in the other order.
     */
    protected void exchangeFlowArrowsTo(Block dest1, Block dest2) {
        TapList<FGArrow> toDest1 = null;
        TapList<FGArrow> toDest2 = null;
        TapList<FGArrow> inFlow = flow;
        FGArrow tmpArrow;
        while (inFlow != null) {
            if (inFlow.head.destination == dest1) {
                toDest1 = inFlow;
            } else if (inFlow.head.destination == dest2) {
                toDest2 = inFlow;
            }
            inFlow = inFlow.tail;
        }
        if (toDest1 != null && toDest2 != null) {
            tmpArrow = toDest1.head;
            toDest1.head = toDest2.head;
            toDest2.head = tmpArrow;
        }
    }

    protected void coherence() {
        TapList<Instruction> inInstructions;
        TapList<FGArrow> inArrows;
        inInstructions = instructions;
        while (inInstructions != null) {
            inInstructions.head.block = this;
            inInstructions = inInstructions.tail;
        }
        inArrows = flow;
        while (inArrows != null) {
            inArrows.head.origin = this;
            inArrows = inArrows.tail;
        }
        inArrows = backFlow;
        while (inArrows != null) {
            inArrows.head.destination = this;
            inArrows = inArrows.tail;
        }
    }

    /**
     * Split instructions of this Block, e.g. to isolate subroutine
     * and function calls.
     */
    protected void splitInstructions(TapList<Instruction> beforeEntry, TapList<Instruction> beforeCycle) {
        TapList<Instruction> topNewInstructionsReverse = new TapList<>(null, null);
        TapList<Instruction> topNewDeclarations = new TapList<>(null, null);
        Instruction instruction;
        Tree split;
        SymbolTable declSymbolTable = symbolTable;
        ToBool hasBeenSplit = new ToBool(false);
        // don't add declaration in a symbolTable of loop:
        while (declSymbolTable.declarationsBlock == null
                || declSymbolTable.declarationsBlock.isALoop()) {
            declSymbolTable = declSymbolTable.basisSymbolTable();
        }
        while (instructions != null) {
            instruction = instructions.head;
            TapEnv.get().resultRK = 1;
            TapEnv.get().argRK = 1;
            TapEnv.get().expRK = 1;
            splitMaskInWhereInstructionRec(instruction, instruction.whereMask(),
                    topNewInstructionsReverse);
            hasBeenSplit.set(false);
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace("Looking for splitting tree:"+instruction.tree);
            }
            split = splitTree(instruction.tree,
                              topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                              symbolTable, declSymbolTable, instruction,
                              false, false, false, true, hasBeenSplit);
            if (split != null) {
                instruction.setTree(split) ;
                if (TapEnv.relatedUnit().isFortran()
                    && !ILUtils.isAUnitPlaceHolder(split) // sub-Units must remain in place.
                    && (ILUtils.isADeclaration(split) || split.opCode()==ILLang.op_none)
                    // Restriction: Consider an include or a noOp as a declaration only if we have not yet met
                    // a non-declaration instruction (cf set02/v053) :
                    && !((split.opCode()==ILLang.op_include || split.opCode()==ILLang.op_none)
                         && topNewInstructionsReverse.tail!=null)) {
                    topNewDeclarations.placdl(instruction);
                } else {
                    topNewInstructionsReverse.placdl(instruction);
                }
            }
            if (hasBeenSplit.get()) {
                instruction.tree.setAnnotation("hasBeenSplit", Boolean.TRUE);
            }
            instructions = instructions.tail;
        }
        topNewInstructionsReverse = topNewInstructionsReverse.tail;
        while (topNewInstructionsReverse != null) {
            instructions =
                    new TapList<>(topNewInstructionsReverse.head, instructions);
            topNewInstructionsReverse = topNewInstructionsReverse.tail;
        }
        topNewDeclarations = topNewDeclarations.tail;
        while (topNewDeclarations != null) {
            instructions = new TapList<>(topNewDeclarations.head, instructions);
            topNewDeclarations = topNewDeclarations.tail;
        }
        //[llh] TODO: maybe in C the declarations of these temporary variables
        // should be made local to this Block's symbolTable.declarationsBlock.
    }

    private void splitMaskInWhereInstructionRec(
            Instruction instruction, InstructionMask origInstructionMask,
            TapList<Instruction> topNewInstructionsReverse) {
        if (origInstructionMask != null) {
            splitMaskInWhereInstructionRec(instruction,
                    origInstructionMask.enclosingMask,
                    topNewInstructionsReverse);
            Tree testTree = origInstructionMask.getControlTree();
            if (whereTestMustBeSplit(testTree)) {
		ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
		Tree newRef = buildTemporaryVariable("mask", testTree, symbolTable,
                                                     toNSH, false, null, true, null, -1, null) ;
                if (TapEnv.traceTypeCheckAnalysis()) {
                    TapEnv.printlnOnTrace("  split expression "+testTree+" into "+newRef) ;
                }
                Tree maskSetTree =
                        ILUtils.build(ILLang.op_assign, newRef, ILUtils.copy(testTree));
                Instruction splitInstruction =
                        makeSplitInstruction(maskSetTree, instruction, true);
                splitInstruction.declareIsMaskDefinition();
                toNSH.obj().declareUsageInstruction(splitInstruction);
                splitInstruction.setWhereMask(origInstructionMask.enclosingMask);
                topNewInstructionsReverse.placdl(splitInstruction);
                newRef = ILUtils.copy(newRef);
                origInstructionMask.setControlTree(newRef);
                if (instruction.tree.opCode() == ILLang.op_where) {
                    instruction.tree.setChild(newRef, 1);
                }
            }
        }
    }

    private boolean whereTestMustBeSplit(Tree testTree) {
        return TapEnv.modeIsAdjoint() &&
                !ILUtils.isAVarRef(testTree, symbolTable);
    }

    /**
     * Splits an expression Tree according to the following rules:<br>
     * -- assignments inside an expression must be split out<br>
     * -- a call argument must be split in the cases explained in
     * method funcArgMustBeSplit<br>
     * -- a call result must be split in the cases explained in
     * method funcResultMustBeSplit<br>
     * -- a call to a routine S must not be split from its args and/or its result
     * when S is in {concat, format, index, open}. vmp: ou est-ce fait?<br>
     * NOTE : We are still unsure of where this split must be done: here
     * before zones are made, or during differentiation when activity is better known.
     *
     * @param beforeEntry if given non-null and if this Block is a HeaderBlock ,
     *        accumulates the code split out of "tree" that must be placed before the
     *        entry into this loop instead of inside the header Block of this loop.
     * @param beforeCycle if given non-null and if this Block is a HeaderBlock ,
     *        accumulates the code split out of "tree" that must be placed before the
     *        cycling in this loop instead of inside the header Block of this loop.
     * @param declSymbolTable the symbolTable in which newSymbolHolders must be declared.
     *                        If symbolTable is a loop, declSymbolTable must be above.
     * @return split tree.
     */
    private Tree splitTree(Tree tree, TapList<Instruction> topNewInstructionsReverse,
                           TapList<Instruction> topNewDeclarations,
                           TapList<Instruction> beforeEntry, TapList<Instruction> beforeCycle,
                           SymbolTable symbolTable, SymbolTable declSymbolTable, Instruction oldInstruction,
                           boolean inExpr, boolean assigned, boolean nonLinear, boolean withMask, ToBool hasBeenSplit) {
        switch (tree.opCode()) {
            case ILLang.op_assign:
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign: {
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                Tree split =
                        splitTree(tree.down(2), topNewInstructionsReverse,
                                topNewDeclarations, beforeEntry, beforeCycle, symbolTable, declSymbolTable, oldInstruction,
                                true, true, nonLinear, withMask, hasBeenSplit);
                if (tree.down(2) != split) {
                    tree.setChild(split, 2);
                }
                if (inExpr) {
                    Instruction newInstr = makeSplitInstruction(tree, oldInstruction, withMask) ;
                    emitNewSplitInstr(newInstr, topNewInstructionsReverse, beforeEntry, beforeCycle) ;
                    tree = ILUtils.copy(tree.down(1));
                }
                tree = addSizeAndTypeInAllocate(tree);
                return tree;
            }
            case ILLang.op_pointerAssign:
                return addSizeAndTypeInAllocate(tree);
            case ILLang.op_call: {
                Unit calledFunctionUnit = DataFlowAnalyzer.getCalledUnit(tree, symbolTable);
                FunctionTypeSpec functionTypeSpec =
                        tree.getAnnotation("functionTypeSpec");
                if (functionTypeSpec == null) {
                    if (calledFunctionUnit != null) {
                        functionTypeSpec = calledFunctionUnit.functionTypeSpec();
                    }
                }
                Tree[] args = ILUtils.getArguments(tree).children();
                Tree oldArg;
                Tree newArg;
                WrapperTypeSpec argTypeSpec;
                WrapperTypeSpec resultTypeSpec;
		Tree newRef ;
                Instruction newDecl;
                boolean isElementalNoChangeDim =
                        calledFunctionUnit != null && calledFunctionUnit.isElemental() &&
                                !TapEnv.stringContainedIn(calledFunctionUnit.name().toLowerCase(),
                                        new String[]{"sum", "spread", "any"});
                boolean functionMayBeActive = calledFunctionUnit == null
                        || TypeSpec.isDifferentiableType(functionTypeSpec);
                boolean inNameEq;
                for (int i = 0; i < args.length; i++) {
                    oldArg = args[i];
                    inNameEq = (oldArg.opCode() == ILLang.op_nameEq) ;
                    if (inNameEq) {
                        oldArg = oldArg.down(2);
                    }
                    newArg = splitTree(oldArg,
                            topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                            symbolTable, declSymbolTable, oldInstruction,
                            true, false, true, withMask, hasBeenSplit);
                    ToBool maskInSum = null;
                    if (i == 1) {
                        maskInSum = new ToBool(false);
                    }
                    if (funcArgMustBeSplit(newArg, tree, functionMayBeActive,
                            calledFunctionUnit, nonLinear, symbolTable,
                            maskInSum)) {
                        // if the call argument must be split from the call itself:
			ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
			newRef = buildTemporaryVariable("arg"+TapEnv.get().argRK, oldArg, symbolTable,
							  toNSH, true, null, true, functionTypeSpec, i+1, null) ;
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.printlnOnTrace("  split expression "+newArg+" into "+newRef) ;
                        }
                        TapEnv.get().argRK++;
                        if (newArg.parent() != null) {
                            newArg = ILUtils.copy(newArg);
                        }
                        hasBeenSplit.set(true);
                        // pour les instructions vectorielles
			argTypeSpec = toNSH.obj().typeSpec() ;
                        Instruction newInstr =
			    makeSplitInstruction(ILUtils.build(ILLang.op_assign, newRef, newArg),
						 oldInstruction, isElementalNoChangeDim && argTypeSpec.isArray()) ;
                        emitNewSplitInstr(newInstr, topNewInstructionsReverse, beforeEntry, beforeCycle) ;
                        toNSH.obj().declareUsageInstruction(newInstr);
                        if (maskInSum != null && maskInSum.get()) {
                            newInstr.setWhereMask(null);
                        }
                        newArg = newRef ;
                    }
                    if (oldArg != newArg) {
                        newArg = ILUtils.copy(newArg);
                        if (inNameEq) {
                            ILUtils.getArguments(tree).down(i + 1).setChild(newArg, 2);
                        } else {
                            ILUtils.getArguments(tree).setChild(newArg, i + 1);
                        }
                    }
                }
                if (funcResultMustBeSplit(tree, inExpr, assigned,
                        functionMayBeActive, calledFunctionUnit, nonLinear,
                        symbolTable)) {
                    // if the call itself must be split from its enclosing expression:
		    ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
		    newRef = buildTemporaryVariable("result"+TapEnv.get().resultRK,
					     tree, symbolTable, toNSH, true, null, true, functionTypeSpec, 0, null) ;
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.printlnOnTrace("  split expression "+tree+" into "+newRef) ;
                    }
                    TapEnv.get().resultRK++;
                    hasBeenSplit.set(true);
                    tree = ILUtils.build(ILLang.op_assign, newRef, ILUtils.copy(tree));
                    Instruction newInstr = makeSplitInstruction(tree, oldInstruction, toNSH.obj().typeSpec().isArray());
                    emitNewSplitInstr(newInstr, topNewInstructionsReverse, beforeEntry, beforeCycle) ;
                    toNSH.obj().declareUsageInstruction(newInstr);
                    tree = ILUtils.copy(newRef) ;
                }
                return tree;
            }
            case ILLang.op_constructorCall: {
                Tree[] expressions = tree.down(3).children();
                Tree split;
                for (int i = 0; i < expressions.length; i++) {
                    split = splitTree(expressions[i],
                            topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                            symbolTable, declSymbolTable, oldInstruction,
                            true, false, nonLinear, withMask, hasBeenSplit);
                    if (expressions[i] != split) {
                        tree.down(3).setChild(split, i + 1);
                    }
                }
                return tree;
            }
            case ILLang.op_ioCall: {
                Tree[] expressions = tree.down(3).children();
                Tree split;
                for (int i = 0; i < expressions.length; i++) {
                    split = splitTree(expressions[i],
                            topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                            symbolTable, declSymbolTable, oldInstruction,
                            true, false, nonLinear, withMask, hasBeenSplit);
                    if (expressions[i] != split) {
                        tree.down(3).setChild(split, i + 1);
                    }
                }
                return tree;
            }
            case ILLang.op_arrayAccess: {
                Tree[] indexes = tree.down(2).children();
                Tree split;
                for (int i = 0; i < indexes.length; i++) {
                    split = splitTree(indexes[i], topNewInstructionsReverse,
                            topNewDeclarations, beforeEntry, beforeCycle, symbolTable, declSymbolTable, oldInstruction,
                            true, false, true, withMask, hasBeenSplit);
                    if (indexes[i] != split) {
                        tree.down(2).setChild(split, i + 1);
                    }
                }
                return tree;
            }
            case ILLang.op_pointerAccess: {
                Tree split = splitTree(tree.down(2), topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle, symbolTable, declSymbolTable,
                        oldInstruction, true, false, true, withMask, hasBeenSplit);
                if (tree.down(2) != split) {
                    tree.setChild(split, 2);
                }
                return tree;
            }
            case ILLang.op_forallRanges:
            case ILLang.op_expressions:
            case ILLang.op_arrayConstructor: {
                Tree[] expressions = tree.children();
                Tree split;
                for (int i = 0; i < expressions.length; i++) {
                    split =
                            splitTree(expressions[i], topNewInstructionsReverse,
                                    topNewDeclarations, beforeEntry, beforeCycle, symbolTable, declSymbolTable, oldInstruction,
                                    true, false, nonLinear, withMask, hasBeenSplit);
                    if (expressions[i] != split) {
                        tree.setChild(split, i + 1);
                    }
                }
                return tree;
            }
            case ILLang.op_power:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_concat:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mod:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_ge:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_lt:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_complexConstructor:
            case ILLang.op_switch:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_mul:
            case ILLang.op_div:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_if:
            case ILLang.op_where:
            case ILLang.op_switchCase: {
                Tree split = splitTree(tree.down(1),
                        topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, declSymbolTable, oldInstruction,
                        true, false, nonLinear, withMask, hasBeenSplit);
                if (split == null) {
                    return tree;  //Temporary, for BLN to "pass"
                }
                if (tree.down(1) != split) {
                    tree.setChild(split, 1);
                }
                return tree;
            }
            case ILLang.op_address: {
                Tree split = splitTree(tree.down(1),
                        topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, declSymbolTable, oldInstruction,
                        true, true, nonLinear, withMask, hasBeenSplit);
                if (tree.down(1) != split) {
                    tree.setChild(split, 1);
                }
                return tree;
            }
            case ILLang.op_ifExpression:
                // Possible bug: code split out of the true or false case of an ifExpression
                //  will be executed regardless of the test, possibly changing semantics.
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 3, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, true, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_fieldAccess:
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_switchCases:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_parallelRegion:
            case ILLang.op_parallelLoop:
                return tree;
            case ILLang.op_loop:
                splitTreeDown(
                        tree, 3, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_do:
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, null,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 3, topNewInstructionsReverse, topNewDeclarations, beforeEntry, null,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 4, topNewInstructionsReverse, topNewDeclarations, beforeEntry, null,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_while:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_until:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, null, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_times:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, null,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_for:
                // TODO: split expressions in a "for"
                return tree;
            case ILLang.op_arrayTriplet:
            case ILLang.op_substringAccess:
                splitTreeDown(
                        tree, 1, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 2, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(
                        tree, 3, topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_forall:
                splitTreeDown(tree, 1, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(tree, 2, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_forallRangeSet:
                splitTreeDown(tree, 2, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_forallRangeTriplet:
                splitTreeDown(tree, 2, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(tree, 3, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                splitTreeDown(tree, 4, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_nameEq:
            case ILLang.op_unary:
            case ILLang.op_multiCast:
            case ILLang.op_cast:
                splitTreeDown(tree, 2, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            case ILLang.op_return: {
                Tree split = splitTree(tree.down(1),
                        topNewInstructionsReverse, topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, declSymbolTable, oldInstruction,
                        true, false, nonLinear, withMask, hasBeenSplit);
                //Debug mode requires that the unsplit return value be split anyway (unless it is a var reference):
                if (tree.down(1) != null && tree.down(1) == split
                        && TapEnv.debugAdMode() != TapEnv.NO_DEBUG && !TapEnv.modeIsAdjoint()
                        && !ILUtils.isAVarRefOrConstant(split)) {

		    ToObject<NewSymbolHolder> toNSH = new ToObject<>(null) ;
		    Tree newRef = buildTemporaryVariable("result"+TapEnv.get().resultRK,
						   tree.down(1), symbolTable, toNSH, true, null, true, null, -1, null) ;
                    if (TapEnv.traceTypeCheckAnalysis()) {
                        TapEnv.printlnOnTrace("  split expression "+tree+" into "+newRef) ;
                    }
                    TapEnv.get().resultRK++;
                    hasBeenSplit.set(true);
                    Tree assignTree = ILUtils.build(ILLang.op_assign, newRef, ILUtils.copy(split));
                    Instruction newInstr = makeSplitInstruction(assignTree, oldInstruction, false);
                    emitNewSplitInstr(newInstr, topNewInstructionsReverse, beforeEntry, beforeCycle) ;
                    toNSH.obj().declareUsageInstruction(newInstr);
                    split = ILUtils.copy(newRef) ;
                }
                if (tree.down(1) != split) {
                    tree.setChild(split, 1);
                }
                splitTreeDown(tree, 2, topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, oldInstruction, nonLinear, withMask, hasBeenSplit);
                return tree;
            }
            case ILLang.op_ident:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_bitCst:
            case ILLang.op_compGoto:
            case ILLang.op_label:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_break:
            case ILLang.op_continue:
            case ILLang.op_throw:
            case ILLang.op_stop:
            case ILLang.op_star:
            case ILLang.op_statementFunctionDeclaration:
            case ILLang.op_strings:
            case ILLang.op_none:
            case ILLang.op_formatElem:
            case ILLang.op_directive:
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_binary:
                return tree;
            case ILLang.op_varDeclaration:
                if (TapEnv.relatedUnit().isFortran()
                        && ILUtils.contains(tree, ILLang.op_assign, null) != null
                        && ILUtils.contains(tree, ILLang.op_modifiers, "constant") == null
                        && ILUtils.contains(tree, ILLang.op_save, null) == null
                        ||
                        !TapEnv.relatedUnit().isFortran()
                                //&& (!TapEnv.modeIsTangent() || TapEnv.get().multiDiffMode)
                                && ILUtils.contains(tree, ILLang.op_assign, null) != null
                                && ILUtils.contains(tree, ILLang.op_modifiers, "constant") == null
                                && ILUtils.contains(tree, ILLang.op_modifiers, "const") == null
                        || (TapEnv.associationByAddress() || TapEnv.relatedUnit().isFortran() && !TapEnv.doTBR())
                        && ILUtils.contains(tree, ILLang.op_modifiers, "constant") == null) {
                    Tree[] declarators = tree.down(3).children();
                    Tree assign = null;
                    Tree newDecl = null;
                    boolean splitInsideInit;
                    boolean splitDecls = false;
                    boolean splitNextDecls = false;
                    // il faut mettre la op_varDeclaration avant les nouveaux assign
                    if (TapEnv.relatedUnit().isFortran()) {
                        topNewDeclarations.placdl(
                                makeSplitInstruction(tree, oldInstruction, false));
                    } else {
                        topNewInstructionsReverse.placdl(
                                makeSplitInstruction(tree, oldInstruction, false));
                    }
                    TapIntList deleteRanks = null;
                    Tree declarator;
                    Tree saveTypeTree;
                    for (int i = 0; i < declarators.length; i++) {
                        declarator = declarators[i];
                        if (declarator.opCode() == ILLang.op_assign
                                && ILUtils.isOptPointersIdent(declarator.down(1))) {
                            // This Tree remembers that an op_allocate inside a declarator has been replaced with a copy.
                            // We must remember that because the original op_allocate is stored to be annotated later with
                            // its allocated zones, and this annotation should now go to the new, copied declarator:
                            Tree copiedAllocateTree = null;
                            splitInsideInit = !TapEnv.relatedUnit().isFortran() && !TapEnv.doTBR();
                            if (i > 0 || TapEnv.get().multiDirDiffMode && !unit().hasArrayNotation()) {
                                splitDecls = true;
                            }
                            Tree declaredIdent = declarator.down(1);
                            while (declaredIdent.opCode() == ILLang.op_pointerDeclarator) {
                                declaredIdent = declaredIdent.down(1);
                            }
                            Tree splitInit = null;
                            if (!TapEnv.relatedUnit().isModule()) {
                                assign = ILUtils.build(ILLang.op_assign,
                                        ILUtils.copy(declaredIdent),
                                        ILUtils.copy(declarator.down(2)));
                                ILUtils.incorporateAnnotations(assign, declarator);
                                if (declarator.down(2).opCode() == ILLang.op_allocate) {
                                    copiedAllocateTree = assign.down(2);
                                }
                                splitInit = splitTree(assign.down(2), topNewInstructionsReverse,
                                        topNewDeclarations, beforeEntry, beforeCycle,
                                        symbolTable, declSymbolTable, oldInstruction,
                                        true, true, nonLinear, withMask, hasBeenSplit);
                            }
                            if (splitInit != null
                                && (!declarator.down(2).equalsTree(assign.down(2))
                                    //A call or an allocate (i.e. a C malloc) as a declaration initializer
                                    // must be split if differentiation is on,
                                    // but not in Fortran as the initialized variable is "SAVE" :
                                    || (!TapEnv.relatedUnit().isFortran()
                                        && !TapEnv.modeIsNoDiff()
                                        && (splitInit.opCode() == ILLang.op_call ||
                                            splitInit.opCode() == ILLang.op_allocate))
                                    || TapEnv.get().multiDirDiffMode && !unit().hasArrayNotation()
                                    || TapEnv.associationByAddress())) {
                                assign.setChild(ILUtils.copy(splitInit), 2);
                                if (declarator.down(2).opCode() == ILLang.op_allocate) {
                                    copiedAllocateTree = assign.down(2);
                                }
                                splitInsideInit = true;
                            }
                            if (splitDecls) {
                                saveTypeTree = addSaveModifier(tree.down(2), symbolTable,
                                        declarators, i);
                                newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                        ILUtils.build(ILLang.op_modifiers),
                                        ILUtils.copy(saveTypeTree),
                                        ILUtils.build(ILLang.op_declarators,
                                                ILUtils.copy(declarator)));
                                deleteRanks = new TapIntList(i + 1, deleteRanks);
                                if (TapEnv.relatedUnit().isFortran()) {
                                    topNewDeclarations.placdl(
                                            makeSplitInstruction(newDecl, oldInstruction, false));
                                } else {
                                    topNewInstructionsReverse.placdl(
                                            makeSplitInstruction(newDecl, oldInstruction, false));
                                }
                                splitNextDecls = true;
                            }
                            if (splitInsideInit) {
                                tree.down(3).setChild(ILUtils.copy(declarator.down(1)), i + 1);
                                if (newDecl != null) {
                                    newDecl.down(3).setChild(ILUtils.copy(declarator.down(1)), 1);
                                }
                                topNewInstructionsReverse.placdl(
                                        makeSplitInstruction(assign, oldInstruction, false));
                            if (copiedAllocateTree != null && copiedAllocateTree != declarator.down(2)) {
                                symbolTable.unit.callGraph().replaceInAllocationPoints(declarator.down(2),
                                        copiedAllocateTree);
                            }
                            }
                            if (i == 0) {
                                if (symbolTable.unit != null
                                        && symbolTable.unit.isFortran9x()
                                        && ILUtils.contains(tree.down(2), ILLang.op_ident, "save") == null) {
                                    saveTypeTree = addSaveModifier(tree.down(2), symbolTable,
                                            declarators, 0);
                                    tree.setChild(ILUtils.copy(saveTypeTree), 2);
                                }
                            }
                        } else if (splitNextDecls) {
                            newDecl = ILUtils.build(ILLang.op_varDeclaration,
                                    ILUtils.build(ILLang.op_modifiers),
                                    ILUtils.copy(tree.down(2)),
                                    ILUtils.build(ILLang.op_declarators,
                                            ILUtils.copy(declarator)));
                            deleteRanks = new TapIntList(i + 1, deleteRanks);
                            if (TapEnv.relatedUnit().isFortran()) {
                                topNewDeclarations.placdl(
                                        makeSplitInstruction(newDecl, oldInstruction, false));
                            } else {
                                topNewInstructionsReverse.placdl(
                                        makeSplitInstruction(newDecl, oldInstruction, false));
                            }
                        }
                    }
                    while (deleteRanks != null) {
                        tree.down(3).removeChild(deleteRanks.head);
                        deleteRanks = deleteRanks.tail;
                    }
                    return null;
                } else {
                    return tree;
                }
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_interfaceDecl:
            case ILLang.op_useDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_data:
            case ILLang.op_save:
            case ILLang.op_common:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_sizeof:
            case ILLang.op_pointerType:
            case ILLang.op_nameList:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_friendsDeclaration:
            case ILLang.op_enumType:
            case ILLang.op_recordType:
            case ILLang.op_unionType:
                return tree;
            case ILLang.op_function:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
                // ^ These are for the empty trees used as placeholders for sub-Units definitions:
                return tree;
            default:
                TapEnv.toolWarning(-1, "(Split tree) Unexpected operator: " + tree.opName());
                return tree;
        }
    }

    private void emitNewSplitInstr(Instruction newInstr, TapList<Instruction> toNewInstructions,
                                   TapList<Instruction> beforeEntry, TapList<Instruction> beforeCycle) {
        if (beforeEntry==null && beforeCycle==null) {
            toNewInstructions.placdl(newInstr);
        } else {
            Instruction newInstrBis = newInstr ;
            // if both beforeEntry and beforeCycle are given non-null, newInstr must be copied:
            if (beforeEntry!=null && beforeCycle!=null) {
                newInstrBis = new Instruction(newInstr.tree, newInstr.syntaxController, null) ;
                newInstrBis.preComments = ILUtils.copy(newInstr.preComments) ;
                newInstrBis.preCommentsBlock = ILUtils.copy(newInstr.preCommentsBlock) ;
                newInstrBis.setWhereMask(newInstr.whereMask());
                newInstrBis.setFromInclude(newInstr.fromInclude());
                newInstrBis.lineNo = newInstr.lineNo ;
            }
            if (beforeEntry!=null)
                beforeEntry.placdl(newInstr);
            if (beforeCycle!=null)
                beforeCycle.placdl(newInstrBis);
        }
    }

    /** Returns a temporary variable usable to split expression "expr" from this Instruction.
     * This variable should be named after the given "name" (possibly extended as usual
     * if conflicts with some existing variable). It is meant to contain the result of "expr".
     * This either reuses an existing temporary variable if available,
     * or creates a new one and takes care of its declaration.
     * @param name The suggested name for the returned temporary variable.
     * @param expr The expression that should be precomputed and assigned to the temporary variable.
     * @param targetSymbolTable The SymbolTable where the future temporary variable will live.
     * @param toNSH When passed non-null, a container that will contain
     *   upon return the NewSymbolHolder of the temporary variable.
     * @param reuse If passed true, may return an already created temporary variable of same name and type.
     * @param excludedReuse when reuse==true, a list of NewSymbolHolder's that must not be reused here.
     * @param declare If passed false, assume the variable is not yet used and
     *   needs no declaration. In doubt, pass true.
     * @param functionTypeSpec In the case where "expr" is the actual argument or result
     *   of some procedure call, the type of this procedure. Else null.
     * @param argRank When "expr" is the actual argument or result
     *   of some procedure call, the rank of this argument (from 1 up) or 0 when result. Else -1.
     * @param refForHint When given non-null, some ref-expression whose dimensions are the same as "expr",
     *   that can be useful to get better hints about the values of ISIZEnOF... dimension sizes.
     * @return The ref-expression Tree of the temporary variable. */
    public Tree buildTemporaryVariable(String name, Tree expr, SymbolTable targetSymbolTable,
                                       ToObject<NewSymbolHolder> toNSH,
                                       boolean reuse, TapList<NewSymbolHolder> excludedReuse, boolean declare,
                                       FunctionTypeSpec functionTypeSpec, int argRank, Tree refForHint) {
	// don't add declaration in a SymbolTable of a loop:
        while (targetSymbolTable.declarationsBlock == null
                || targetSymbolTable.declarationsBlock.isALoop()) {
            targetSymbolTable = targetSymbolTable.basisSymbolTable();
        }
	WrapperTypeSpec exprType = symbolTable.typeOf(expr);
        // Take advantage of possibly better type info from the function's type:
        if (functionTypeSpec!=null && argRank!=-1) {
            // NOTE: when argRank==0, one could also use callTree.getAnnotation("arrayReturnTypeSpec").
            WrapperTypeSpec typeFromFunctionType =
                (argRank==0 ? functionTypeSpec.returnType : functionTypeSpec.argumentsTypes[argRank-1]) ;
            // If we are in the "dummyzerodiff" case (dirty test declare==false), if the call on-the-fly converts
            // from actual INT to formal REAL, force this conversion for the new temporary variable
            // (cf set09/v109, set09/v110, but "not" set12/lh06)
            if (!declare && argRank>0 && exprType.isIntegerBase() && typeFromFunctionType.isProbablyRealOrComplexBase()) {
                 exprType = WrapperTypeSpec.intToReal(exprType, new TapList<>(null, null), symbolTable);
            }
            exprType = WrapperTypeSpec.preciseDimensions(exprType, typeFromFunctionType,
                                                         new TapList<>(null, null), symbolTable);
        }

	if (exprType == null
	    || exprType.wrappedType == null
	    || exprType.wrappedType.containsUndefinedType()) {
	    String thisExprDescription ;
	    if (functionTypeSpec==null) {
		thisExprDescription = "this expression" ;
	    } else if (argRank==0) {
		thisExprDescription = "result of function "+ILUtils.toString(ILUtils.getCalledName(expr)) ;
	    } else {
		thisExprDescription = "argument "+argRank+" of procedure "+ILUtils.toString(ILUtils.getCalledName(expr)) ;
	    }
	    TapEnv.fileWarning(TapEnv.MSG_WARN, expr,
			       "(TC43) Could not find or guess type of "+thisExprDescription+", set to temporary variable "+name+". Please check");
	}

	// (?) Normalize the dimensions in exprType, using possible array triplet in expr:
        // TODO: Not sure the following is well implemented, even if it proves useful on a few cases.
        // In any case, present implementation is wrong if exprType has more than 1 dimension.
        Tree tripletTree = null;
	if (expr!=null && TypeSpec.isA(exprType, SymbolTableConstants.ARRAYTYPE)) {
	    ArrayTypeSpec arrayExprType = (ArrayTypeSpec)exprType.wrappedType ;
	    if (arrayExprType.dimensions().length==1) {
		ArrayDim presentDim = arrayExprType.dimensions()[0] ;
		tripletTree = ILUtils.findArrayTriplet(expr) ;
		if (tripletTree!=null) {
                    WrapperTypeSpec typeOfDim = symbolTable.typeOf(tripletTree) ;
                    if (TypeSpec.isA(typeOfDim, SymbolTableConstants.ARRAYTYPE)) {
                        ArrayTypeSpec tripletType = (ArrayTypeSpec)typeOfDim.wrappedType ;
                        tripletType.checkDimensions(tripletTree, symbolTable);
                        ArrayDim tripletDim = tripletType.dimensions()[0] ;
                        if (tripletDim.refQuality > presentDim.refQuality) {
                            arrayExprType.setDimensions(tripletType.dimensions());
                        } else {
                            tripletTree = null ;
                        }
                    }
                }
	    }
	}

	name = TapEnv.disambigNewName(name) ;
	NewSymbolHolder symbolHolder = null ;
	if (reuse) {
	    symbolHolder = targetSymbolTable.reuseNewSymbolHolder(name, exprType, excludedReuse) ;
	}
	if (symbolHolder==null) {
	    symbolHolder = new NewSymbolHolder(name);

            // Compute the SymbolDecl's that the new newSymbolHolder depends on:
            TapList<SymbolDecl> toDependsOn = new TapList<>(null, null);
            SymbolDecl.addUsedSymbolsInExpr(expr, toDependsOn, targetSymbolTable, null, false, true);
            symbolHolder.setDependsOn(toDependsOn);

	    // just in case symbolHolder needs ISIZEnOF... dimensions sizes:
	    symbolHolder.setHintRootTree(refForHint);
	    symbolHolder.setHintArrayTreeForCallSize(refForHint);

            // build the various strings that are used e.g. in messages that hint about declaration size:
            String hintNameInIdent = name;
	    String hintNameInText = name;
	    Tree hintTreeInSize = null;
	    if (symbolHolder.hintRootTree != null) {
		hintNameInIdent = ILUtils.buildNameInIdent(symbolHolder.hintRootTree);
		hintNameInText = ILUtils.buildNameInText(symbolHolder.hintRootTree);
	    }
	    if (symbolHolder.hintArrayTreeForCallSize != null) {
		hintTreeInSize = ILUtils.buildSizeArgument1(symbolHolder.hintArrayTreeForCallSize, symbolTable);
	    }

            exprType = symbolHolder.checkVariableDimension(exprType,
                             hintNameInIdent, hintNameInText, symbolHolder.hintArrayTreeForCallSize,
                             targetSymbolTable.unit, symbolTable.unit, symbolTable) ;

	    exprType = exprType.checkNoneDimensionsOfNewSH(hintNameInText, hintNameInIdent,
							   hintTreeInSize, targetSymbolTable, symbolHolder);

	    symbolHolder.setAsVariable(exprType, null);
	    // prepare the declaration Instruction for the new symbol:
	    Tree typeTree;
	    if (exprType.wrappedType == null) {
                TapEnv.toolWarning(-1, "(Building intermediate variable) empty type for " + name);
                typeTree = ILUtils.build(ILLang.op_ident, "UnknownType");
            } else {
                typeTree = exprType.wrappedType.generateTree(null, null, null, true, null);
            }
	    Instruction newDeclaration = new Instruction() ;
	    newDeclaration.block = targetSymbolTable.declarationsBlock ;
	    newDeclaration.setTree(
                    ILUtils.build(ILLang.op_varDeclaration,
                      ILUtils.build(ILLang.op_modifiers),
                      typeTree,
                      ILUtils.build(ILLang.op_declarators,
                        symbolHolder.makeNewRef(targetSymbolTable)))) ;
	    symbolHolder.setInstruction(newDeclaration);
	    symbolHolder.newVariableDecl().setInstruction(newDeclaration);
	}

        if (declare) {
            symbolHolder.declarationLevelMustInclude(targetSymbolTable);
            if (TapEnv.doOpenMP() && this.parallelControls != null) {
                symbolHolder.preparePrivateClause(this, symbolTable.unit.privateSymbolTable(), true, false);
                symbolHolder.preparePrivateClause(this, symbolTable.unit.privateSymbolTable(), false, false);
            }
        }
	if (toNSH!=null) toNSH.setObj(symbolHolder) ;
	Tree newRef = symbolHolder.makeNewRef(targetSymbolTable);

        if (TypeSpec.isA(exprType, SymbolTableConstants.ARRAYTYPE)) {
            ArrayDim[] dimensions = ((ArrayTypeSpec)exprType.wrappedType).dimensions() ;
            boolean oneDimExplicit = false ;
            TapList<Tree> indices = null ;
            Tree index ;
            for (int i=dimensions.length-1 ; i>=0 ; --i) {
                if (dimensions[i].unDeclarableLength && dimensions[i].tree().opCode()==ILLang.op_dimColon) {
                    // translate the op_dimColon into op_arrayTriplet:
                    index = ILUtils.build(ILLang.op_arrayTriplet,
                              ILUtils.copy(dimensions[i].tree().down(1)),
                              ILUtils.copy(dimensions[i].tree().down(2)),
                              ILUtils.build(ILLang.op_none)) ;
                    oneDimExplicit = true ;
                } else {
                    // access the full dimension:
                    index = ILUtils.build(ILLang.op_arrayTriplet) ;
                }
                indices = new TapList<>(index, indices) ;
            }
            if (oneDimExplicit) {
                newRef = ILUtils.build(ILLang.op_arrayAccess, newRef,
                           ILUtils.build(ILLang.op_expressions, indices)) ;
            }
        }
        return newRef ;
    }

    private Tree addSizeAndTypeInAllocate(Tree instrOrExpr) {
        if (TapEnv.relatedLanguageIsFortran()
                && (instrOrExpr.opCode() == ILLang.op_pointerAssign || instrOrExpr.opCode() == ILLang.op_assign)
                && !ILUtils.isNullOrNone(instrOrExpr.down(2))
                && instrOrExpr.down(2).opCode() == ILLang.op_allocate) {
            WrapperTypeSpec resultType = null;
            if (!ILUtils.isNullOrNone(instrOrExpr.down(1))) {
                resultType = symbolTable.typeOf(instrOrExpr.down(1));
            }
            // Normally, resultType must be a pointer. Peel off this pointer level:
            if (TypeSpec.isA(resultType, SymbolTableConstants.POINTERTYPE)) {
                assert resultType != null;
                resultType = ((PointerTypeSpec) resultType.wrappedType).destinationType;
            }
            // Then peel off every array type:
            if (TypeSpec.isA(resultType, SymbolTableConstants.ARRAYTYPE)) {
                assert resultType != null;
                resultType = resultType.wrappedType.elementType();
            }
            int typeSize = 0;
            if (resultType != null) {
                typeSize = resultType.size();
            }
            Tree allocTree = instrOrExpr.down(2);
            if (ILUtils.isNullOrNone(allocTree.down(1))) {
                Tree nbElems = ILUtils.build(ILLang.op_intCst, typeSize);
                Tree oneDim;
                Tree[] dims = allocTree.down(2).children();
                for (int i = dims.length - 1; i >= 0; --i) {
                    oneDim = dims[i];
                    if (oneDim.opCode() == ILLang.op_dimColon) {
                        oneDim = ILUtils.buildSizeTree(oneDim.down(1), oneDim.down(2), null);
                    } else if (oneDim.opCode() == ILLang.op_arrayTriplet) {
                        oneDim = ILUtils.buildSizeTree(oneDim.down(1), oneDim.down(2), oneDim.down(3));
                    }
                    nbElems = ILUtils.buildSmartMulDiv(nbElems, 1, oneDim);
                }
                allocTree.setChild(nbElems, 1);
            }
            if (ILUtils.isNullOrNone(allocTree.down(3)) && resultType != null && resultType.wrappedType != null) {
                allocTree.setChild(
                        resultType.wrappedType.generateTree(this.symbolTable, null, null, false, null),
                        3);
            }
        }
        return instrOrExpr;
    }

    private Tree addSaveModifier(Tree tree, SymbolTable symbolTable, Tree[] declarators, int i) {
        Tree typeTree;
        typeTree = ILUtils.copy(tree);
        if (symbolTable.unit != null
                && symbolTable.unit.isFortran9x()
                && ILUtils.contains(typeTree, ILLang.op_ident, "save") == null) {
            // si le varDecl n'a pas de declaration save separee:
            String name = ILUtils.baseName(declarators[i].isAtom() ? declarators[i] : declarators[i].down(1));
            VariableDecl varDecl = symbolTable.getVariableDecl(name);
            if (varDecl!=null && !varDecl.hasInstructionWithRootOperator(ILLang.op_save, null)) {
                if (typeTree.opCode() == ILLang.op_modifiedType) {
                    typeTree.down(1).addChild(ILUtils.build(ILLang.op_ident, "save"), typeTree.down(1).length() + 1);
                } else {
                    typeTree = ILUtils.build(ILLang.op_modifiedType,
                            ILUtils.build(ILLang.op_modifiers,
                                    ILUtils.build(ILLang.op_ident, "save")),
                            typeTree);
                }
            }
        }
        return typeTree;
    }

    private void splitTreeDown(
            Tree tree, int rank,
            TapList<Instruction> topNewInstructionsReverse, TapList<Instruction> topNewDeclarations,
            TapList<Instruction> beforeEntry, TapList<Instruction> beforeCycle,
            SymbolTable symbolTable, Instruction oldInstruction, boolean nonLinear, boolean withMask, ToBool hasBeenSplit) {
        SymbolTable declSymbolTable = symbolTable;
        // don't add declaration in a symbolTable of loop:
        while (declSymbolTable.declarationsBlock == null
                || declSymbolTable.declarationsBlock.isALoop()) {
            declSymbolTable = declSymbolTable.basisSymbolTable();
        }
        Tree split =
                splitTree(tree.down(rank), topNewInstructionsReverse,
                        topNewDeclarations, beforeEntry, beforeCycle,
                        symbolTable, declSymbolTable, oldInstruction,
                        true, false, nonLinear, withMask, hasBeenSplit);
        if (tree.down(rank) != split) {
            tree.setChild(split, rank);
        } else {
        }
    }

    private Instruction makeSplitInstruction(Tree newTree, Instruction oldInstruction, boolean withMask) {
        Instruction newInstruction =
                new Instruction(newTree, oldInstruction.syntaxController, oldInstruction.block);
        int language = TapEnv.relatedLanguage();
        if (oldInstruction.preComments != null) {
            Tree preCommentsForNewTree = ILUtils.build(ILLang.op_comments);
            oldInstruction.preComments =
                    distributeDirectiveComments(newTree, oldInstruction.preComments, preCommentsForNewTree, language);
            newInstruction.preComments = preCommentsForNewTree.length() == 0 ? null : preCommentsForNewTree;
        }
        if (oldInstruction.preCommentsBlock != null) {
            Tree preCommentsForNewTree = ILUtils.build(ILLang.op_comments);
            oldInstruction.preCommentsBlock =
                    distributeDirectiveComments(newTree, oldInstruction.preCommentsBlock, preCommentsForNewTree, language);
            newInstruction.preCommentsBlock = preCommentsForNewTree.length() == 0 ? null : preCommentsForNewTree;
        }
        if (withMask) {
            newInstruction.setWhereMask(oldInstruction.whereMask());
        }
        newInstruction.setFromInclude(oldInstruction.fromInclude());
        newInstruction.lineNo = oldInstruction.lineNo ;
        return newInstruction;
    }

    /**
     * Distribute the comments given as the children of "preComments" in two.
     * Some comments must stay attached to their original tree and Instruction
     * (e.g. because they rather deal with it than with the newTree).
     * These comments are returned as a new Tree, that will probably replace "preComments".
     * The other comments are placed in the new op_comments tree "preCommentsForNewTree",
     * These comments will probably be attached to the new Instruction containing the newTree.
     */
    private Tree distributeDirectiveComments(Tree newTree, Tree preComments, Tree preCommentsForNewTree, int language) {
        if (ILUtils.isNullOrNoneOrEmptyList(preComments)) {
            return preComments;
        }
        Tree[] comments = preComments.children();
        TapList<Tree> commentsStaying = null;
        for (int i = comments.length - 1; i >= 0; --i) {
            if (commentMustStay(comments[i], newTree, language)) {
                commentsStaying = new TapList<>(comments[i], commentsStaying);
            } else {
                preCommentsForNewTree.addChild(comments[i], 1);
            }
        }
        if (preCommentsForNewTree.length() == 0) {
            return preComments;
        } else {
            return ILUtils.build(ILLang.op_comments, commentsStaying);
        }
    }

    /**
     * Decision to attach a comment to the 1st or the 2nd half of an instruction split.
     * This code is quite ad-hoc, depending on the nature of the comment and on the split tree.
     */
    private boolean commentMustStay(Tree comment, Tree newTree, int language) {
        String directiveText = Directive.getDirectiveText(comment);
        if (directiveText == null) {
            // Normal preComments must move with the first instruction resulting from split:
            return false;
        } else {
            while (directiveText.startsWith(" ")) {
                directiveText = directiveText.substring(1);
            }
            String directiveTextUC = directiveText.toUpperCase();
            if (directiveTextUC.startsWith("CHANNEL ")) {
                Tree callTree = newTree;
                if (callTree.opCode() == ILLang.op_assign) {
                    callTree = callTree.down(2);
                }
                return callTree.opCode() != ILLang.op_call ||
                        !MPIcallInfo.isMessagePassingFunction(ILUtils.getCallFunctionNameString(callTree), language);
            } else {
                // Default: directive will move!
                return false;
            }
        }
    }

    /**
     * @return true when the SUM must split its arguments because
     * either the SUM has a where mask that must be split,
     * or the SUMmed array is an expression with too many arrays [llh: why?].
     */
    private boolean splitSum(Tree tree, SymbolTable symbolTable,
                             ToBool maskInSum, int maxArray) {
        if (maskInSum != null
                && ILUtils.getArguments(tree).down(2) != null
                && symbolTable.typeOf(ILUtils.getArguments(tree).down(2)).isBooleanBase()
                && whereTestMustBeSplit(ILUtils.getArguments(tree).down(2))) {
            maskInSum.set(true);
            return true;
        } else {
            int nb = 0;
            for (TopDownTreeWalk i = new TopDownTreeWalk(ILUtils.getArguments(tree).down(1)); !i.atEnd(); i.advance()) {
                if (i.get().opCode() == ILLang.op_ident) {
                    VariableDecl varDecl =
                            symbolTable.getVariableDecl(i.get().stringValue());
                    if (varDecl != null && TypeSpec.isA(varDecl.type(), SymbolTableConstants.ARRAYTYPE)) {
                        ++nb;
                    }
                }
            }
            return nb > maxArray;
        }
    }

    protected String citeFlowShort() {
        StringBuilder result = new StringBuilder();
        TapList<FGArrow> blocks = backFlow;
        while (blocks != null) {
            result.append(':').append(blocks.head.origin.rank);
            blocks = blocks.tail;
        }
        result.append(">_>");
        blocks = flow;
        while (blocks != null) {
            result.append(blocks.head.destination.rank).append(':');
            blocks = blocks.tail;
        }
        return result.toString();
    }

    /**
     * For future use.
     *
     * @return true if the control context of this block
     * allows for a static saving of the tape, i.e. this
     * block is either in the main stream, of inside a nest of
     * loops that all have a static iteration space.
     */
    public boolean hasStaticIterationSpace() {
        return enclosing == null
                || ((LoopBlock) enclosing).staticIterationLength() != null
                && enclosing.hasStaticIterationSpace();
    }

    /**
     * Same as Block.hasStaticIterationSpace().
     * <br>
     * For future use.
     *
     * @return the TapList of all the enclosing
     * iteration lengths, which are all assumed static.
     */
    public TapList<Integer> getStaticIterationSpace() {
        if (enclosing == null) {
            return null;
        } else {
            return new TapList<>(
                    ((LoopBlock) enclosing).staticIterationLength(),
                    enclosing.getStaticIterationSpace());
        }
    }

    /**
     * Same as Block.hasStaticIterationSpace().
     * <br>
     * For future use.
     *
     * @return the TapList of all the loop indexes of the
     * surrounding loops, which are all assumed of static length.
     */
    public TapList<Tree> getStaticIterationIndices() {
        if (enclosing == null) {
            return null;
        } else {
            return new TapList<>(
                    ((LoopBlock) enclosing).staticNormalizedIndex(),
                    enclosing.getStaticIterationIndices());
        }
    }

    /**
     * @return the position of this Block in its textual file.
     */
    protected int getPosition() {
        int pos = -1;
        TapList<Instruction> blockInstructions = instructions;
        Instruction currentInstruction;
        while (pos == -1 && blockInstructions != null) {
            currentInstruction = blockInstructions.head;
            if (currentInstruction != null) {
                pos = currentInstruction.getPosition();
            }
            blockInstructions = blockInstructions.tail;
        }
        return pos;
    }

    /**
     * @return true when this call argument must be split from the call.
     * This decision is made only from the call point of view: It is
     * possible that a split is required from the arg point of view
     * if this arg is itself a call whose result must be split.<br>
     * <pre>
     * {@code
     * Naming F the called function and ARG the argument, we MUST split if:
     * -- AND F is active
     * -- AND diff mode is not PREPROCESS_MODE (NoDiff)
     * -- AND (IF (F is an intrinsic with a separate diff function)
     * IF (diff mode is TANGENT_MODE)
     *    (ARG cannot be duplicated because it has side effects
     *     or its cost*occurrence number is too large)
     * ELSE  !! we know diff mode is ADJOINT_MODE
     *    (ARG is not a VarRefOrConstant and
     *     Either this intrinsic has no predefined derivatives
     *     or we are in a SQRT or buggy F90 SUM
     *       (because ADReverse has a very clever treatment of SQRT
     *        and F90 SUM is still buggy))
     * ELSE  !! we know we are on a user or external procedure
     *    ((diff mode is ADJOINT_MODE OR MULTIDIR mode)
     *     AND (ARG is not a VarRefOrConstant)
     *     AND (ARG is not a string manipulation cf F90:lha24)))
     *     -- AND mixed language call}
     * </pre>
     * Attention: we force split of arguments of SQRT as soon as they are not
     * simple var references (although this is not very clean).
     */
    private boolean funcArgMustBeSplit(Tree arg, Tree callTree,
                                       boolean functionIsActive, Unit calledUnit, boolean nonLinear,
                                       SymbolTable symbolTable, ToBool maskInSum) {
        if (!functionIsActive || TapEnv.modeIsNoDiff()) return false ;
        // Rare F77 construct for argument passing by value:
        if (arg.opCode()==ILLang.op_call && "%val".equals(ILUtils.getCalledNameString(arg))) return false ;
        String functionName = ILUtils.getCallFunctionNameString(callTree);
        boolean functionIsIntrinsic = (calledUnit != null && calledUnit.isIntrinsic()) ;
        boolean hasPredefinedDiff = (calledUnit != null && calledUnit.diffInfo != null
                                     // The following is only because of bugs in reverse diff of SUM:
                                     && !("sum".equals(functionName)
                                          && TapEnv.relatedUnit().isFortran9x()
                                          && splitSum(callTree, symbolTable, maskInSum, 5))) ;
        // Estimate how many times the arg appears in the diff code:
        int timesUsed = (nonLinear ? 1 : 0)
                + (argIsActive(arg, symbolTable) ? 1 : 0);
        if (TapEnv.get().multiDirDiffMode) {
            timesUsed = 3 * timesUsed;
        }
        if (isSqrt(callTree) || isArcSinOrCos(callTree)) {
            timesUsed = timesUsed + 1;
        }
        timesUsed = timesUsed + 1;
        int argCost =
            (isDuplicable(arg, symbolTable) ? duplicableCost(arg, symbolTable) : 9999) ;
        boolean duplicationIsExpensive = (timesUsed * argCost > timesUsed + argCost + 7) ;
        boolean argIsSumMask = false ;
        if ("sum".equals(functionName) && TapEnv.relatedUnit().isFortran9x()) {
            // We force splitting the "mask" argumemt of a sum in adjoint mode, not really
            // because of its cost, but rather because duplicating the test is not well
            // protected against changes in the tested arrays. cf z1() vs arg11() in set10/lh215.
            Tree argNumber2 = ILUtils.getArguments(callTree).down(2) ;
            if (argNumber2!=null && argNumber2.opCode()==ILLang.op_nameEq) argNumber2 = argNumber2.down(2) ;
            argIsSumMask = (arg==argNumber2) ;
        }
        boolean shouldSplitInTangent = duplicationIsExpensive ;
        boolean shouldSplitInAdjoint = (!ILUtils.isAVarRefOrConstant(arg)
                                        && !hasPredefinedDiff
                                        && (duplicationIsExpensive || argIsSumMask)) ;
        return ((TapEnv.associationByAddress() && !ILUtils.isAVarRefOrConstant(arg))
                ||
                (functionIsIntrinsic
                 ? (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
                    // In debug mode, we must make the same choices in tangent and adjoint
                    ? (shouldSplitInTangent || shouldSplitInAdjoint)
                    : (TapEnv.modeIsTangent() ? shouldSplitInTangent : shouldSplitInAdjoint))
                 : ((TapEnv.get().multiDirDiffMode || TapEnv.modeIsAdjoint() || TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG)
                    && !ILUtils.isAVarRefOrConstant(arg)
                    && !ILUtils.isAStringManipulation(arg)))
                || (calledUnit != null
                    && !functionIsIntrinsic
                    && calledUnit.language() != TapEnv.relatedLanguage()
                    && !ILUtils.isAVarRefOrConstant(arg)));
    }

    /**
     * @return true when the present tree, which is a function call,
     * must be split from its expression.
     * <pre>
     * {@code
     * Naming F the called function, the rules used here are that we MUST split if:
     * -- AND diff mode is not PREPROCESS_MODE (NoDiff)
     * -- AND F has a result and this result is not already directly assigned
     * -- AND F is not a call to a very special intrinsic (e.g. allocated)
     * -- AND EITHER (this F() is used non-linearly (and thus would be used twice in the diff code)
     *                and cannot be duplicated)
     *        OR (IF (diff mode is TANGENT_MODE) THEN
     *                (EITHER F is differentiated as a user or external procedure OR F is sqrt)
     *            ELSE     !! we know diff mode is ADJOINT_MODE
     *            (F is differentiated AND
     *             (OR F is not intrinsic
     *              OR F is intrinsic but has no predefined derivatives
     *              OR F is a buggy F90 SUM (if reverse diff of F90 SUM is still buggy))))}
     *      </pre>
     * Notice: this is not very clean, but we also want to split SQRT in tangent mode!
     */
    private boolean funcResultMustBeSplit(Tree callTree, boolean inExpr,
                                          boolean assigned, boolean functionIsActive, Unit calledUnit,
                                          boolean nonLinear, SymbolTable symbolTable) {
        String functionName = ILUtils.getCallFunctionNameString(callTree);
        boolean functionIsIntrinsicWithSeparableDiff =
                calledUnit != null && calledUnit.isIntrinsic() && calledUnit.diffInfo != null;
        return !TapEnv.modeIsNoDiff() &&
                !assigned && inExpr && (functionName!=null) &&
                !(functionName.equals("allocated") || functionName.equals("associated")
                        || functionName.equals("shape")
                        || functionName.equals("size")
                        || functionName.equals("trim")
                        || functionName.equals("lbound")
                        || functionName.equals("ubound")) &&
                ((nonLinear && !isDuplicable(callTree, symbolTable)) ||
                        (TapEnv.debugAdMode() == TapEnv.ADJ_DEBUG
                                // In debug mode, we must make the same choices in tgt and adjoint
                                ? functionIsActive && !functionIsIntrinsicWithSeparableDiff
                                || isSqrt(callTree) || isArcSinOrCos(callTree)
                                || functionIsActive &&
                                (!functionIsIntrinsicWithSeparableDiff ||
                                        // The following is only because of bugs in reverse diff of SUM:
                                        functionName.equals("sum") &&
                                                TapEnv.relatedUnit().isFortran9x() &&
                                                splitSum(callTree, symbolTable, null, 2))
                                : TapEnv.modeIsTangent()
                                ?
                                functionIsActive && !functionIsIntrinsicWithSeparableDiff
                                        || isSqrt(callTree) || isArcSinOrCos(callTree)
                                : //Then we are in ADJOINT_MODE
                                functionIsActive &&
                                        (!functionIsIntrinsicWithSeparableDiff ||
                                                // The following is only because of bugs in reverse diff of SUM:
                                                functionName.equals("sum") &&
                                                        TapEnv.relatedUnit().isFortran9x() &&
                                                        splitSum(callTree, symbolTable, null, 2))));
    }

    private boolean isSqrt(Tree callTree) {
        String functionName = ILUtils.getCallFunctionNameString(callTree);
        return (functionName!=null) && (functionName.equals("sqrt") || functionName.equals("dsqrt") || functionName.equals("csqrt"));
    }

    private boolean isArcSinOrCos(Tree callTree) {
        String functionName = ILUtils.getCallFunctionNameString(callTree);
        return (functionName!=null) && (functionName.equals("asin") || functionName.equals("dasin") ||
                functionName.equals("acos") || functionName.equals("dacos"));
    }

    private boolean argIsActive(Tree exp, SymbolTable symbolTable) {
        WrapperTypeSpec expType = symbolTable.typeOf(exp);
        return expType == null ||
                TypeSpec.isDifferentiableType(expType.wrappedType);
    }

    // TODO: this code is similar to a part of splitInstructions() => refactor?
    public void sortDeclarationsAndOperations() {
        TapList<Instruction> tlOps = new TapList<>(null, null);
        TapList<Instruction> tlDecls = new TapList<>(null, tlOps);
        TapList<Instruction> hdDeclsOps = tlDecls;
        TapList<Instruction> inInstructions = instructions;
        Instruction instr;
        Tree tree ;
        while (inInstructions != null) {
            instr = inInstructions.head;
            tree = instr.tree ;
            if (tree!=null
                && !ILUtils.isAUnitPlaceHolder(tree) // sub-Units must remain in place.
                && (ILUtils.isADeclaration(tree) || tree.opCode()==ILLang.op_data || tree.opCode()==ILLang.op_none)
                // Restriction: Consider an include or a noOp as a declaration only if we have not yet met
                // a non-declaration instruction (cf set02/v053) :
                && !((tree.opCode()==ILLang.op_include || tree.opCode()==ILLang.op_none)
                     && tlDecls.tail!=tlOps)) {
                tlDecls = tlDecls.placdl(instr);
            } else {
                tlOps = tlOps.placdl(instr);
            }
            inInstructions = inInstructions.tail;
        }
        tlDecls.tail = tlDecls.tail.tail;
        instructions = hdDeclsOps.tail;
    }

    public boolean isVarDeclaredInBlock(SymbolDecl symbolDecl) {
        return symbolDecl.isVarDeclared(instructions);
    }

    /**
     * Remove instructions containing declaration of unused NewSymbolHolder.
     *
     * @param unit current Unit.
     */
    public void checkUnusedDeclaration(Unit unit) {
        TapList<Instruction> decls = instructions;
        Instruction instr;
        TapList<SymbolDecl> symbolDecls;
        TapList<Instruction> removedDecls = null;
        while (decls != null) {
            instr = decls.head;
            if (!instr.isDifferentiated && ILUtils.isAVariableOnlyDeclaration(instr.tree)) {
                symbolDecls = instr.declaredSymbolDecls(
                        unit.publicSymbolTable(), unit.privateSymbolTable());
                if (!unit.isCPlusPlus() && //So far in C++, we have no symbolDecls, but we want to keep the declarations!
                        symbolDecls == null) {
                    // si c'est une globale mixed-language
                    symbolDecls = instr.declaredSymbolDecls(unit.callGraph().globalRootSymbolTable(), null);
                    if (symbolDecls == null) {
                        removedDecls = new TapList<>(instr, removedDecls);
                    }
                }
            }
            decls = decls.tail;
        }
        instructions = TapList.deleteAll(removedDecls, instructions);
    }

    /**
     * TypeChecks this Block.
     *
     * @param definitionSymbolTable SymbolTable used to typecheck this Block.
     */
    protected void typeCheck(SymbolTable definitionSymbolTable) {
        TapList<Instruction> inInstructions = instructions;
        Tree instrOrExpr;
        ToBool isBeginning = new ToBool(true);
        Instruction instr;
        while (inInstructions != null) {
            instr = inInstructions.head;
            if (!instr.isInStdCInclude()) {
                instrOrExpr = inInstructions.head.tree;
                TapEnv.setTraceIndent(0);
                definitionSymbolTable.typeCheck(instrOrExpr, false, false, false,
                        inInstructions.head, isBeginning, null);
            }
            inInstructions = inInstructions.tail;
        }
    }

    /**
     * @return true if this block that contains sub-units definitions is outside of the notion of control flow.
     */
    public boolean isOutsideFlow() {
        boolean containsUnits = false;
        for (TapList<Instruction> inInstructions = instructions
             ; inInstructions != null && !containsUnits
                ; inInstructions = inInstructions.tail) {
            if (inInstructions.head.isUnitDefinitionStub() != null) {
                containsUnits = true;
            }
        }
        return containsUnits;
    }

    /**
     * @return an ordered list of all statements in this Block.
     */
    public TapList<Tree> listOfStatements() {
        TapList<Instruction> inInstructions = instructions;
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        while (inInstructions != null) {
            if (inInstructions.head != null && inInstructions.head.tree != null) {
                tlResult = tlResult.placdl(inInstructions.head.tree);
            }
            inInstructions = inInstructions.tail;
        }
        return hdResult.tail;
    }

    /** True if this Block already contains an Instruction with the given tree */
    public boolean containsInstructionTree(Tree tree) {
        boolean contains = false ;
        TapList<Instruction> inInstructions = instructions;
        while (!contains && inInstructions!=null) {
            if (inInstructions.head != null && inInstructions.head.tree != null
                && inInstructions.head.tree.equalsTree(tree)) {
                contains = true ;
            }
            inInstructions = inInstructions.tail;
        }
        return contains ;
    }

    /** If this Block contains declarations and contains inside the declarations part
     * a comment starting with startString, then returns the remaining of the comment.
     * Otherwise returns null. */
    public String hasDeclarationCommentStartingWith(String startString) {
        boolean inDeclarations=false ;
        String someText = null ;
        Instruction instruction ;
        TapList<Instruction> inInstructions = instructions;
        while (inInstructions!=null && !inDeclarations) {
            instruction = inInstructions.head ;
            if (someText==null) someText = instruction.hasPreCommentStartingWith(startString) ;
            if (someText!=null) {if (instruction.isADeclaration()) inDeclarations = true ;}
            if (someText==null) someText = instruction.hasPostCommentStartingWith(startString) ;
            inInstructions = inInstructions.tail ;
        }
        return (inDeclarations ? someText : null) ;
    }

    /** Returns true if this Block contains a comment with the given "someText" */
    public boolean hasCommentWith(String someText) {
        boolean found = false ;
        Instruction instruction ;
        TapList<Instruction> inInstructions = instructions;
        while (inInstructions!=null && !found) {
            instruction = inInstructions.head ;
            if (instruction.hasCommentWith(someText)) {
                found = true ;
            }
            inInstructions = inInstructions.tail ;
        }
        return found ;
    }

    /**
     * For future use.
     *
     * @param varName
     * @return
     */
    public boolean seesAsShared(String varName) {
        if (parallelControls == null) {
            return false;
        }
        boolean seenAsShared = true;
        TapList<Instruction> inParallelControls = parallelControls;
        while (inParallelControls != null && seenAsShared) {
            if (ILUtils.isParallelController(inParallelControls.head.tree)) {
                Tree clauses = inParallelControls.head.tree.down(2);
                if (!ILUtils.isNullOrNoneOrEmptyList(clauses)) {
                    Tree[] allClauses = clauses.children();
                    for (Tree allClause : allClauses) {
                        if (!ILUtils.isNullOrNoneOrEmptyList(allClause)) {
                            switch (allClause.opCode()) {
                                case ILLang.op_privateVars:
                                case ILLang.op_firstPrivateVars:
                                case ILLang.op_lastPrivateVars: {
                                    if (ILUtils.contains(allClause.down(1), varName, false)) {
                                        seenAsShared = false;
                                    }
                                    break;
                                }
                                case ILLang.op_reductionVars: {
                                    if (ILUtils.contains(allClause.down(2), varName, false)) {
                                        seenAsShared = false;
                                    }
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            inParallelControls = inParallelControls.tail;
        }
        return seenAsShared;
    }

    /**
     * Prints in detail the contents of this Block, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing.
     * @throws java.io.IOException if an output error is detected.
     */
    public void dump(int indent) throws java.io.IOException {
        // First display the head line for the block.
        if (this instanceof BasicBlock) {
            TapEnv.indentPrint(indent, "BasicBlock number " + rank + ':');
        } else if (this instanceof HeaderBlock) {
            TapEnv.indentPrint(indent, "HeaderBlock number " + rank + ':');
        } else if (this instanceof EntryBlock) {
            TapEnv.indentPrint(indent, "EntryBlock:");
        } else if (this instanceof InitConstructorBlock) {
            TapEnv.indentPrint(indent, "InitConstructorBlock:");
        } else if (this instanceof ExitBlock) {
            TapEnv.indentPrint(indent, "ExitBlock:");
        } else if (this instanceof LoopBlock) {
            TapEnv.indentPrint(indent, "LoopBlock:");
        } else {
            TapEnv.indentPrint(indent, "untyped Block number " + rank + ':');
        }
        if (origLabel() != null) {
            TapEnv.print("  (orig label " + origLabel() + ')');
        }
        if (this instanceof HeaderBlock && ((HeaderBlock) this).origCycleLabel() != null) {
            TapEnv.print("  (orig cycle label " + ((HeaderBlock) this).origCycleLabel() + ')');
        }
        TapEnv.println();
        TapEnv.indentPrintln(indent, "=====================");
        // then display the zone number
        if (testZone != -1) {
            TapEnv.indentPrintln(indent + 2, "Zone of Control: " + testZone);
        }
        if (controllers != null) {
            TapEnv.indentPrint(indent + 4, "Controlled by: ");
            TapList<Block> controllersList = controllers;
            while (controllersList != null) {
                if (controllersList.head != null) {
                    controllersList.head.cite();
                }
                controllersList = controllersList.tail;
            }
            TapEnv.println();
        }
        // Display Control-Flow information:
        if (unit() != null) {
            TapEnv.indentPrintln(indent + 4, "Successor: " + unit().blocksFromBitVector(successor));
            TapEnv.indentPrintln(indent + 4, "Predecessor: " + unit().blocksFromBitVector(predecessor));
            TapEnv.indentPrintln(indent + 4, "Dominator: " + unit().blocksFromBitVector(dominator));
            TapEnv.indentPrintln(indent + 4, "Immediate Dominator: " + immediateDominator);
            TapEnv.indentPrintln(indent + 4, "Post dominator: " + unit().blocksFromBitVector(postDominator));
            TapEnv.indentPrintln(indent + 4, "Immediate PostDominator: " + immediatePostDominator);
            TapEnv.indentPrintln(indent + 4, "Reachable(static inference): " + reachable+" reachUp/Down:"+reachUp+"/"+reachDown+" backReachUp/Down:"+backReachUp+"/"+backReachDown);
        }
        // then display the symbol table:
        TapEnv.indentPrint(indent + 2, "SymbolTable: ");
        if (symbolTable == null) {
            TapEnv.println("NULL");
        } else {
            TapEnv.println(symbolTable.shortName() + " " + symbolTable.addressChain());
        }
        if (parallelControls != null) {
            TapEnv.indentPrintln(indent + 2, "Parallel Controls:" + parallelControls);
        }
        // then display the incoming control flow:
        TapEnv.indentPrintln(indent + 2, "Incoming flow FGArrow's:");
        TapList<FGArrow> objects = backFlow;
        FGArrow arrow;
        while (objects != null) {
            TapEnv.indent(indent + 4);
            arrow = objects.head;
            if (arrow == null) {
                TapEnv.println("null Arrow!");
            } else {
                if (arrow.inACycle) {
                    TapEnv.print("in cycle arrow ");
                }
                if (arrow.origin == null) {
                    TapEnv.print("null origin!");
                } else {
                    TapEnv.print(arrow.origin.toString());
                }
                TapEnv.print(" ");
                arrow.dumpTestCaseCarry();
            }
            TapEnv.println();
            objects = objects.tail;
        }
        // then display the instructions inside:
        dumpInstructions(indent + 2);
        // then display the exiting control flow:
        TapEnv.indentPrintln(indent + 2, "Exiting flow FGArrow's:");
        objects = flow;
        while (objects != null) {
            TapEnv.indent(indent + 4);
            arrow = objects.head;
            if (arrow == null) {
                TapEnv.println("null Arrow!");
            } else {
                if (arrow.inACycle) {
                    TapEnv.print("in cycle arrow ");
                }
                arrow.dumpTestCaseCarry();
                TapEnv.print(" ");
                if (arrow.destination == null) {
                    TapEnv.print("null destination!");
                } else {
                    TapEnv.print(arrow.destination.toString());
                }
            }
            TapEnv.println();
            objects = objects.tail;
        }
    }

    /**
     * Dumps instructions of this Block.
     *
     * @param indent indentation.
     * @throws java.io.IOException if an output error is detected.
     */
    protected final void dumpInstructions(int indent) throws java.io.IOException {
        if (instructions == null) {
            TapEnv.indentPrintln(indent, "Instructions: empty");
        } else {
            TapEnv.indentPrintln(indent, "Instructions:");
            for (TapList<Instruction> instrs = instructions; instrs != null; instrs = instrs.tail) {
                TapEnv.indent(indent + 2);
                instrs.head.dump();
                TapEnv.println();
            }
        }
    }

    /**
     * Prints a short reference to this Block onto TapEnv.curOutputStream().
     *
     * @throws java.io.IOException if an output error is detected.
     */
    public void cite() throws java.io.IOException {
        TapEnv.print("?B(" + rank + ')');
    }

    @Override
    public String toString() {
        return "B" + (symbolicRk != null ? "\"" + symbolicRk + "\"" : "#" + rank)
                //+"@"+Integer.toHexString(hashCode())
                ;
    }
}
