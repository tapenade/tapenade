/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.Tree;

/**
 * Holds the heap of all known labels ("goto" labels).
 */
public final class LabelHeap {

    private LabelHeap.LabelHeapCell topCell;

    /**
     * @return the unique Block designated by the "label".
     * Creates this Block if necessary.
     * "defines" is a flag indicating that this label
     * is here defined (instead of used)
     * In case of a redefinition of a label, the second
     * definition is ignored, and this function returns null.
     */
    public Block getSetLabelled(
            String label, TapList<Block> allBlocks,
            boolean defines, Tree position) {
        LabelHeap.LabelHeapCell curCell = topCell;
        while (curCell != null && !curCell.label.equals(label)) {
            curCell = curCell.next;
        }
        if (curCell == null) {
            BasicBlock block = new BasicBlock(null, null, allBlocks);
            block.setOrigLabel(label);
            topCell = new LabelHeap.LabelHeapCell(label, block, topCell);
            curCell = topCell;
        }
        if (defines) {
            if (curCell.definitionPosition != null) {
                TapEnv.fileWarning(TapEnv.MSG_SEVERE, position, "(DD13) Double definition of label " + label + " (ignored new)");
                return null;
            } else {
                curCell.definitionPosition = position;
            }
        } else {
            curCell.usagePositions =
                    new TapList<>(position, curCell.usagePositions);
        }
        return curCell.block;
    }

    /**
     * Look for Block labelled "label".
     * If it exists, either complain for double definition or fuse it into "fuseBlock".
     * Otherwise, let it be "fuseBlock".
     */
    public void fuseLabelled(
            String label, Block fuseBlock, Tree position) {
        LabelHeap.LabelHeapCell curCell = topCell;
        while (curCell != null && !curCell.label.equals(label)) {
            curCell = curCell.next;
        }
        if (curCell == null) {
            topCell = new LabelHeap.LabelHeapCell(label, fuseBlock, topCell);
            topCell.definitionPosition = position;
        } else if (curCell.definitionPosition != null) {
            TapEnv.fileWarning(TapEnv.MSG_SEVERE, position, "(DD13) Double definition of label " + label + " (ignored new)");
        } else {
            Block oldBlock = curCell.block;
            curCell.block = fuseBlock;
            curCell.definitionPosition = position;
            /*All arrows going to oldBlock must now go to curBlock: */
            TapList<FGArrow> arrows = oldBlock.backFlow();
            while (arrows != null) {
                arrows.head.redirectDestination(fuseBlock);
                arrows = arrows.tail;
            }
        }
    }

    public void checkFormatLabel(
            String label, Tree position) {
        LabelHeap.LabelHeapCell curCell = topCell;
        while (curCell != null && !curCell.label.equals(label)) {
            curCell = curCell.next;
        }
        if (curCell == null) {
            topCell = new LabelHeap.LabelHeapCell(label, null, topCell);
            topCell.definitionPosition = position;
        } else {
            TapEnv.fileWarning(TapEnv.MSG_SEVERE, position, "(DD13) Double definition of label " + label + " (ignored new)");
        }
    }

    public Block getLabelled(String label) {
        LabelHeap.LabelHeapCell curCell = topCell;
        while (curCell != null && !curCell.label.equals(label)) {
            curCell = curCell.next;
        }
        if (curCell != null) {
            return curCell.block;
        } else {
            return null;
        }
    }

    public void undefinedLabels(
            Block exitBlock, SymbolTable symbolTable) {
        LabelHeap.LabelHeapCell curCell = topCell;
        TapList<Tree> usages;
        while (curCell != null) {
            if (curCell.definitionPosition == null) {
                usages = curCell.usagePositions;
                while (usages != null) {
                    TapEnv.fileWarning(TapEnv.MSG_SEVERE, usages.head, "(TC50) Label " + curCell.label + " is not defined");
                    curCell.block.symbolTable = symbolTable;
                    usages = usages.tail;
                }
                new FGArrow(curCell.block, FGConstants.NO_TEST, null, exitBlock);
            }
            curCell = curCell.next;
        }
    }

    /**
     * Local private class of LabelHeap.
     * Holds one cell of the heap.
     */
    private static final class LabelHeapCell {
        private final String label;
        private final LabelHeap.LabelHeapCell next;
        private Block block;
        private Tree definitionPosition;
        private TapList<Tree> usagePositions;

        private LabelHeapCell(String label, Block block, LabelHeap.LabelHeapCell next) {
            super();
            this.label = label;
            this.block = block;
            this.next = next;
            this.definitionPosition = null;
            this.usagePositions = null;
        }
    }
}
