/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.TapPair;

/**
 * Special control-flow Block that is created temporarily after the
 * FlowGraph is built and therefore has no rank and information
 * attached via the usual BlockStorages.
 */
public final class TemporaryBlock extends Block {

    /**
     * Local storage for this Block's activity info.
     */
    public TapList<BoolVector> blockActivities;

    /**
     * Local storage for this Block's usefulness info.
     */
    public TapList<BoolVector> blockUsefulnesses;

    /**
     * Local storage for this Block's ReqExplicit info.
     */
    public TapList<BoolVector> blockReqXs;

    /**
     * Local storage for this Block's Available Memory info.
     */
    public TapList<BoolVector> blockAvlXs;

    /**
     * Local storage for this Block's TBR info.
     */
    public TapList<TapPair<BoolVector, BoolVector>> blockTBRs;

    /**
     * Local storage for this Block's TBR Recomputation info.
     */
    public TapList<TapPair<TapList<Instruction>, TapList<Instruction>>> blockRecomputations;

    /**
     * Creation, sharing SymbolTable with "block".
     */
    public TemporaryBlock(Block refBlock) {
        super(refBlock.symbolTable, refBlock.parallelControls, null);
        rank = -99;
    }

    @Override
    public String toString() {
        return "TempB"
                //+"@"+Integer.toHexString(hashCode())
                ;
    }
}
