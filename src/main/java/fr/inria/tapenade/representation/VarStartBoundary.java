/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Registration object, that goes from a VariableDecl
 * to its starting position in a MemMap
 */
public final class VarStartBoundary {
    public VariableDecl variableDecl ;
    public MemMap map ;
    public AlignmentBoundary boundary ;

    public VarStartBoundary(VariableDecl variableDecl, MemMap map, AlignmentBoundary boundary) {
        this.variableDecl = variableDecl ;
        this.map = map ;
        this.boundary = boundary ;
    }
}
