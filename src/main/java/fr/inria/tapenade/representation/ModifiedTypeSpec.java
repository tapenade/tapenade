/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * A type with additional modifications, e.g. size.
 */
public final class ModifiedTypeSpec extends TypeSpec {

    private WrapperTypeSpec elementType;

    /**
     * The tree of the expression that modifies the size of this type (if any).
     */
    public Tree sizeModifier;

    /**
     * The size modifier, resolved as much as possible (if any and computable).
     */
    protected Tree sizeModifierResolved;

    /**
     * SymbolTableConstants.SIGNED for "signed",
     * SymbolTableConstants.UNSIGNED for "unsigned",
     * SymbolTableConstants.UNSPECIFIED_SIGNED if unspecified.
     */
    protected int typeSign;
    /**
     * The integer size of this modified type.
     * Should be completely computed at creation time, using symbolTable etc...
     * If it can't be fully computed, it may be -2 for double, -4 for long double,
     * else -1 if it really cannot be computed.
     */
    private int sizeModifierInteger = -1;
    private boolean undefinedSize = false ;

    /**
     * Create a modified type around the given elementType, with given modifiers.
     */
    public ModifiedTypeSpec(WrapperTypeSpec elementType, Tree sizeModifier,
                            SymbolTable symbolTable) {
        super(SymbolTableConstants.MODIFIEDTYPE);
        this.elementType = elementType;
        this.sizeModifier = sizeModifier;
        resolveSizeModifier(symbolTable);
    }

    /**
     * Create a modified type around the given elementType, following the given model modified type.
     */
    public ModifiedTypeSpec(WrapperTypeSpec elementType, ModifiedTypeSpec model) {
        super(SymbolTableConstants.MODIFIEDTYPE);
        this.elementType = elementType;
        this.sizeModifier = model.sizeModifier;
        this.sizeModifierResolved = model.sizeModifierResolved;
        this.sizeModifierInteger = model.sizeModifierInteger;
        this.typeSign = model.typeSign;
    }

    protected static boolean sizeModifierIntegerSmaller(int smallSize, int largeSize) {
        return smallSize > 0 && largeSize > smallSize
                ||
                smallSize == -1 && largeSize != -1
                ||
                smallSize == -2 && largeSize > 8;
    }

    /**
     * <pre>
     * {@code
     * Convention: integer size modifiers may be:
     * n>0 for a known size in bytes,
     *     -> in this case the "Tree" modifier is the size expression, that could be evaluated to n
     * 0 for unmodified base type size (but not explicited because compiler_dependent)
     *     -> in this case the "Tree" modifier is null, since there was no modifier!
     * -1 for unknown size value (specified by Tree T)
     *     -> in this case the "Tree" modifier is the size expression, which could not be evaluated
     * -2 for double base type size
     *     -> in this case the "Tree" modifier is op_intCst:-2 meaning "double" or "long"
     * -3 for short,
     *     -> in this case the "Tree" modifier is op_intCst:-3 meaning "short"
     * -4 for double double base type size.
     *     -> in this case the "Tree" modifier is op_intCst:-4 meaning "long double" or "long long"
     * In the tables below, leftmodifier is on the left, rightmodifier on the top,
     * tt means ok, ff means not-ok, * means acceptable but possible accuracy problem}
     * </pre>
     *
     * @param leftModifierInteger
     * @param leftModifierTree
     * @param rightModifierInteger
     * @param rightModifierTree
     * @param comparison
     * @return
     */
    protected static boolean compareModifiersWith(int leftModifierInteger, Tree leftModifierTree,
                                                  int rightModifierInteger, Tree rightModifierTree,
                                                  int comparison) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace(" +Comparing size modifiers " + leftModifierInteger + ":"
                    + leftModifierTree + " vs " + rightModifierInteger + ":" + rightModifierTree);
        }
        if (testTypesWithoutSize(comparison)) {
            return true;
        }
        if (testIsReceives(comparison)) {
            //          n2    0   -1   -2
            //     ------------------------
            // n1  |  n1>=n2  *    *    *
            //  0  |    *    tt    *    *
            // -1  |    *     *  T1==T2 *
            // -2  |    *    tt    *   tt
            if (leftModifierInteger > 0 && rightModifierInteger > 0) {
                return leftModifierInteger >= rightModifierInteger;
            } else if (leftModifierInteger <= -2 && (rightModifierInteger == 0 || rightModifierInteger <= -2)) {
                return true;
            } else if (leftModifierInteger == 0 && rightModifierInteger == 0) {
                return true;
            } else if (leftModifierInteger == -1 && rightModifierInteger == -1
                       && (leftModifierTree==null ? rightModifierTree==null : leftModifierTree.equalsTree(rightModifierTree))) {
                return true;
            } else
            //if (accuracyFlag!=null) accuracyFlag.set(true) ;
            {
                return true;
            }
        } else {
            //          n2    0   -1   -2
            //     ------------------------
            // n1  |  n1==n2  *    *    *
            //  0  |    *    tt    *   ff
            // -1  |    *     *  T1==T2 *
            // -2  |    *    ff    *   tt
            if (leftModifierInteger == rightModifierInteger && leftModifierInteger != -1) {
                return true;
            } else if (leftModifierInteger > 0 && rightModifierInteger > 0 && leftModifierInteger != rightModifierInteger) {
                return false;
            } else if ((leftModifierInteger == 0 || leftModifierInteger <= -2) && (rightModifierInteger == 0 || rightModifierInteger <= -2)) {
                return false;
            } else { //if (accuracyFlag!=null) accuracyFlag.set(true) ;
                return leftModifierInteger == -1 && rightModifierInteger == -1
                    && (leftModifierTree==null ? rightModifierTree==null : leftModifierTree.equalsTree(rightModifierTree)) ;
            }
        }
    }

    /**
     * Set the modifier of this modified type, following the given model modified type.
     */
    protected void getModifierFrom(ModifiedTypeSpec model) {
        this.sizeModifier = model.sizeModifier;
        this.sizeModifierResolved = model.sizeModifierResolved;
        this.sizeModifierInteger = model.sizeModifierInteger;
    }

    /**
     * Tries to evaluate the sizeModifier expression tree that is used to declare this modified type.<br>
     * If sizeModifier is null (e.g. modifier is only an "unsigned") then evaluation returns null.<br>
     * Else if the expression is "double", "long", "long long", etc then the evaluation returns intCst -2 or -4.<br>
     * Else if the expression evaluates to an integer value, then the evaluation returns this value in an intCst.<br>
     * Else if the expression is a constant with an initial value, returns the evaluation of this initial value.<br>
     * Else returns the sizeModifier itself.
     */
    protected void resolveSizeModifier(SymbolTable symbolTable) {
        if (sizeModifier == null) {
            sizeModifierResolved = null;
            sizeModifierInteger = -1;
        } else {
            Tree sizeModifierPeeled = (sizeModifier.opCode() == ILLang.op_nameEq ? sizeModifier.down(2) : sizeModifier) ;
            Tree result = null;
            boolean doubleBytesIfComplex = false ;
            if (sizeModifierPeeled.opCode() == ILLang.op_ident) {
                String modString = sizeModifierPeeled.stringValue();
                if ("short".equals(modString)) {
                    result = ILUtils.build(ILLang.op_intCst, -3); //NOT DONE YET!
                } else if ("double".equals(modString) || "long".equals(modString)) {
                    result = ILUtils.build(ILLang.op_intCst, -2);
                } else if ("long double".equals(modString) || "long long".equals(modString)) {
                    result = ILUtils.build(ILLang.op_intCst, -4);
                } else {
                    doubleBytesIfComplex = true ; // Case of COMPLEX(wp)
                }
            } else if (sizeModifierPeeled.opCode() == ILLang.op_intCst) {
                result = sizeModifierPeeled;
            }
            if (result == null) {
                result = resolveSizeModifierRec(sizeModifierPeeled, symbolTable);
            }
            if (doubleBytesIfComplex && isComplexBase() && result!=null && result.opCode()==ILLang.op_intCst) {
                result = ILUtils.build(ILLang.op_intCst, 2*result.intValue()) ;
            }
            // Place the new result only if it is better than the previously stored sizeModifierResolved:
            if (result != null &&
                    (sizeModifierResolved == null ||
                            result.opCode() == ILLang.op_intCst && sizeModifierResolved.opCode() != ILLang.op_intCst)) {
                sizeModifierResolved = result;
                sizeModifierInteger = result.opCode() == ILLang.op_intCst ? result.intValue() : -1;
            }
        }
    }

    private Tree resolveSizeModifierRec(Tree sizeExpression, SymbolTable symbolTable) {
        Integer ival = symbolTable.computeIntConstant(sizeExpression);
        if (ival != null) {
            return ILUtils.build(ILLang.op_intCst, ival);
        } else if (sizeExpression.opCode() == ILLang.op_ident) {
            VariableDecl cstDecl = symbolTable.getConstantDecl(sizeExpression.stringValue());
            Tree cstInit = (cstDecl == null ? null : cstDecl.initializationTree()) ;
            if (cstInit != null) {
                return resolveSizeModifierRec(cstInit, symbolTable);
            } else {
                return sizeExpression;
            }
        } else {
            return sizeExpression;
        }
    }

    /**
     * @return the size of this modified type, if statically known, else returns -1.
     * If size is a dynamic variable, returns -1. If the size is defined relatively (e.g. "double"), then
     * doesn't compute the final, compiler-dependent value, and instead returns -2 or -4 for "long double".
     */
    public int sizeModifierValue() {
        return sizeModifierInteger;
    }

    /** Given the size modifier of this ModifiedTypeSpec, which is assumedly applied to a complex type,
     * tries to build a size modifier Tree that would apply to either the real or imaginary part. */
    public Tree complexHalfModifier() {
        int modifierInt = sizeModifierValue() ;
        if (modifierInt>0) {
            return ILUtils.build(ILLang.op_intCst, modifierInt/2) ;
        } else {
            return sizeModifier ;
        }
    }

    /** Given the size modifier of this ModifiedTypeSpec, which is assumedly applied to a non-complex type,
     * tries to build a size modifier Tree that would apply to a complex that has this type of real/imaginary parts. */
    public Tree complexDuplModifier() {
        int modifierInt = sizeModifierValue() ;
        if (modifierInt>0) {
            return ILUtils.build(ILLang.op_intCst, modifierInt*2) ;
        } else {
            return sizeModifier ;
        }
    }

    public boolean hasKindEqual() {
        return sizeModifier != null && sizeModifier.opCode() == ILLang.op_nameEq
                && ILUtils.isIdent(sizeModifier.down(1), "kind", false);
    }

    public boolean hasCbindingModifier() {
        boolean result = sizeModifier.opCode() == ILLang.op_ident;
        if (result) {
            String modifier = sizeModifier.stringValue();
            result = modifier.startsWith("c_int")
                    || modifier.startsWith("c_short")
                    || modifier.startsWith("c_long")
                    || modifier.startsWith("c_signed")
                    || modifier.startsWith("c_size")
                    || modifier.startsWith("c_float")
                    || modifier.startsWith("c_double")
                    || modifier.startsWith("c_bool")
                    || modifier.startsWith("c_char");
        }
        return result;
    }

    public boolean hasCbindingModifier(String cModifier) {
        boolean result = sizeModifier.opCode() == ILLang.op_ident;
        if (result) {
            String modifier = sizeModifier.stringValue();
            result = modifier.startsWith(cModifier);
        }
        return result;
    }

    /**
     * The unmodified type inside.
     */
    @Override
    public WrapperTypeSpec elementType() {
        return elementType;
    }

    @Override
    protected void setHasUndefinedSize(boolean val) {
        undefinedSize = val;
    }

    @Override
    protected boolean hasUndefinedSize() {
        return undefinedSize;
    }

    @Override
    protected boolean isUndefinedNumeric() {
        TypeSpec ats = elementType.wrappedType;
        return ats != null && ats.isUndefinedNumeric();
    }

    @Override
    protected void setUndefinedNumeric(boolean value) {
        elementType.wrappedType.setUndefinedNumeric(value);
    }

    @Override
    public TypeSpec wrappedType() {
        return elementType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        elementType = type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type);
    }

    @Override
    protected TypeSpec cloneAsUndefinedNumeric(boolean undefined) {
        return new ModifiedTypeSpec((WrapperTypeSpec) elementType.cloneAsUndefinedNumeric(undefined), this);
    }

    @Override
    public TypeSpec nestedLevelType() {
        return elementType;
    }

    @Override
    protected TypeSpec peelPointer() {
        return elementType == null ? null : elementType.peelPointer();
    }

    @Override
    protected String baseTypeName() {
        return elementType.baseTypeName();
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        return elementType.baseTypeSpec(stopOnPointer);
    }

    @Override
    public boolean containsAPointer() {
        return elementType != null && elementType.containsAPointer();
    }

    @Override
    public boolean containsUnknownDimension() {
        return elementType != null && elementType.containsUnknownDimension();
    }

    @Override
    public void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (elementType != null) {
            elementType.updateAfterImports(symbolTable, dejaVu);
        }
        resolveSizeModifier(symbolTable);
    }

    @Override
    protected int computeSize() {
        int size = elementType.size();
        int sizeValue = sizeModifierInteger;
        if (sizeValue > 0) {
            if (baseTypeName().equals("character")) {
                return size * sizeValue;
            } else {
                return sizeValue;
            }
        } else if (sizeValue == -2) {
            if (baseTypeName().equals("float")) {
                return TapEnv.get().doubleRealSize;
            } else if (baseTypeName().equals("complex")) {
                return TapEnv.get().doubleRealSize * 2;
            } else {
                return size * 2;
            }
        } else if (sizeValue == -4) {
            if (baseTypeName().equals("float")) {
                return TapEnv.get().doubleRealSize * 2;
            } else if (baseTypeName().equals("complex")) {
                return TapEnv.get().doubleRealSize * 4;
            } else {
                return size * 4;
            }
        } else {
            return size;
        }
    }

    @Override
    public boolean isCharacter() {
        return elementType != null && elementType.isCharacter();
    }

    @Override
    public boolean isString() {
        return elementType != null && elementType.isString();
    }

    @Override
    public boolean isTarget() {
        return elementType.isTarget();
    }

    @Override
    public boolean isArray() {
        return elementType != null && elementType.isArray();
    }

    @Override
    public void addUsedSymbolsInType(TapList<SymbolDecl> toDependsOn, SymbolTable symbolTable) {
        if (sizeModifier != null) {
            SymbolDecl.addUsedSymbolsInExpr(sizeModifier, toDependsOn, symbolTable, null, false, true);
        }
    }

    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (!TapList.contains(toDejaVu, this)) {
            toDejaVu.placdl(this);
            if (sizeModifier != null) {
                toUsedTrees.placdl(sizeModifier);
            }
            if (elementType != null && elementType.wrappedType != null) {
                elementType.wrappedType.collectUsedTrees(toUsedTrees, toDejaVu);
            }
        }
    }

    @Override
    public boolean isPointer() {
        return elementType != null && elementType.isPointer();
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's:
        while (other instanceof WrapperTypeSpec) {
            toOther = other;
            other = ((WrapperTypeSpec) other).wrappedType;
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // If we want to compare "literally",
        // the other type MUST be a ModifiedTypeSpec, even if this has empty modifiers.
        if (testTypesLitteral(comparison)) {
            if (!(other instanceof ModifiedTypeSpec)) {
                return false;
            }
        }
        // peel size modifiers around this and around other:
        ToObject<Tree> toThisSizeModifierResolved = new ToObject<>(null);
        // Special case: if we want to compare "literally",
        // we must compare the raw size modifiers and not the "resolved" ones,
        // so peelSizeModifiersTo will retrieve the non-resolved modifier Tree.
        TypeSpec toThisUnmodified = peelSizeModifiersTo(this, toThis, toThisSizeModifierResolved, null, testTypesLitteral(comparison));
        TypeSpec thisUnmodified = toThisUnmodified.wrappedType();
        Tree thisSizeModifierResolved = toThisSizeModifierResolved.obj();
        ToObject<Tree> toOtherSizeModifierResolved = new ToObject<>(null);
        ToObject<ModifiedTypeSpec> toOtherModifiedTypeSpec = new ToObject<>(null);
        // here also, retrieve the non-resolved modifier Tree if we want to compare "literally".
        TypeSpec toOtherUnmodified = peelSizeModifiersTo(other, toOther, toOtherSizeModifierResolved, toOtherModifiedTypeSpec, testTypesLitteral(comparison));
        TypeSpec otherUnmodified = toOtherUnmodified == null ? other : toOtherUnmodified.wrappedType();
        Tree otherSizeModifierResolved = toOtherSizeModifierResolved.obj();
        boolean comparesWell = true;

        // compare element types:
        if (comparesWell) {
            if (thisUnmodified == null) {
                // if this's unmodified type is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    thisUnmodified = otherUnmodified == null ? null : otherUnmodified.weakenForInference(comparison);
                    toThisUnmodified.setWrappedType(thisUnmodified);
                } else {
                    comparesWell = testAcceptsUnspecified(comparison);
                }
            } else if (otherUnmodified == null) {
                // if other's unmodified type is unspecified, it all depends on type inference:
                if (testHasInference(comparison)) {
                    if (toOtherUnmodified != null) {
                        toOtherUnmodified.setWrappedType(thisUnmodified);
                    }
                } else {
                    comparesWell = false;
                }
            } else {
                comparesWell =
                        thisUnmodified.comparesWith(otherUnmodified, comparison,
                                toThisUnmodified, toOtherUnmodified, dejaVu);
            }
        }

        // compare size modifiers:
        if (comparesWell && thisUnmodified != null) {
            int thisSizeModifierInteger = -1;
            int otherSizeModifierInteger = -1;
            boolean testTypesCompilDep = testTypesCompilDep(comparison);
            int unmodifiedSize = testTypesCompilDep ? thisUnmodified.size() : 0;
            if (thisSizeModifierResolved == null) {
                thisSizeModifierInteger = testTypesCompilDep ? unmodifiedSize : 0; //Convention for "no modifier at all"
            } else if (thisSizeModifierResolved.opCode() == ILLang.op_intCst) {
                thisSizeModifierInteger = thisSizeModifierResolved.intValue();
                if (testTypesCompilDep && thisSizeModifierInteger <= -2) {
                    thisSizeModifierInteger = thisSizeModifierInteger == -2 ? 2 * unmodifiedSize :
                            thisSizeModifierInteger == -4 ? 4 * unmodifiedSize : unmodifiedSize;
                }
            }
            if (otherSizeModifierResolved == null) {
                otherSizeModifierInteger = testTypesCompilDep ? unmodifiedSize : 0; //Convention for "no modifier at all"
            } else if (otherSizeModifierResolved.opCode() == ILLang.op_intCst) {
                otherSizeModifierInteger = otherSizeModifierResolved.intValue();
                if (testTypesCompilDep && otherSizeModifierInteger <= -2) {
                    otherSizeModifierInteger = otherSizeModifierInteger == -2 ? 2 * unmodifiedSize :
                            otherSizeModifierInteger == -4 ? 4 * unmodifiedSize : unmodifiedSize;
                }
            }
            // If we are looking at a RECEIVE and the receiving type (left type) is an undefinedNumeric,
            // and we play type inference then enlarge this left type to accomodate for the larger right type:
            if (testIsReceivesWithInference(comparison) && isUndefinedNumeric()) {
                if (sizeModifierIntegerSmaller(thisSizeModifierInteger, otherSizeModifierInteger)) {
                    ModifiedTypeSpec otherModifiedTypeSpec = toOtherModifiedTypeSpec.obj();
                    if (otherModifiedTypeSpec != null) {
                        this.getModifierFrom(otherModifiedTypeSpec);
                    }
                }
                // leave comparesWell as true!
            } else {
                // If compiler dependent comparison, then -2, -3, and -4 have been replaced with actual sizes, strictly positive.
                // Now both thisSizeModifierInteger and otherSizeModifierResolved belong to {-4, -3, -2, -1, 0, or n>0}
                comparesWell = compareModifiersWith(thisSizeModifierInteger, thisSizeModifierResolved,
                        otherSizeModifierInteger, otherSizeModifierResolved,
                        comparison);
            }
        }
        return comparesWell;
    }

    @Override
    protected TypeSpec weakenForInference(int comparison) {
        return new ModifiedTypeSpec((WrapperTypeSpec) elementType.weakenForInference(comparison), this);
    }

    @Override
    public TypeSpec copy() {
        return new ModifiedTypeSpec((WrapperTypeSpec) elementType.copy(), this);
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        TypeSpec copyElement = elementType == null ? null : elementType.copyStopOnComposite(publishedUnit);
        return copyElement == elementType ? this : new ModifiedTypeSpec((WrapperTypeSpec) copyElement, this);
    }

    /**
     * @return the size in bytes of this numeric type,
     * or -1 for unknown, or -2, -3, or -4 for double,short,or quadruple.
     */
    @Override
    public int precisionSize() {
        return sizeModifierInteger;
    }

    @Override
    public Tree buildConstantZero() {
        String baseTypeName = baseTypeName();
        if (baseTypeName.equals("integer")) {
            return ILUtils.build(ILLang.op_intCst, 0);
        } else if (baseTypeName.equals("float")) {
            SymbolTable symbolTable;
            if (TapEnv.relatedUnit() != null) {
                symbolTable = TapEnv.relatedUnit().privateSymbolTable();
                if (symbolTable == null) {
                    symbolTable = TapEnv.relatedUnit().publicSymbolTable();
                }
            }
            int sizeModifier = sizeModifierInteger;
            if (sizeModifier > 0 && TapEnv.relatedLanguageIsFortran9x()
                    && !this.hasUndefinedSize()) {
                return ILUtils.build(ILLang.op_realCst, "0.0_" + sizeModifier);
            } else if (sizeModifier == -2 || sizeModifier == 8) {
                if (TapEnv.relatedLanguageIsC()) {
                    return ILUtils.build(ILLang.op_realCst, "0.0");
                } else {
                    return ILUtils.build(ILLang.op_realCst, "0.D0");
                }
            } else {
                return PrimitiveTypeSpec.buildRealConstantZero();
            }
        } else if (baseTypeName.equals("complex")) {
            return PrimitiveTypeSpec.buildRealConstantZero();
        } else {
            return null;
        }
    }

    @Override
    public Tree buildConstantOne() {
        String baseTypeName = baseTypeName();
        if (baseTypeName.equals("integer")) {
            return ILUtils.build(ILLang.op_intCst, 1);
        } else if (baseTypeName.equals("float") || baseTypeName.equals("complex")) {
            SymbolTable symbolTable;
            if (TapEnv.relatedUnit() != null) {
                symbolTable = TapEnv.relatedUnit().privateSymbolTable();
                if (symbolTable == null) {
                    symbolTable = TapEnv.relatedUnit().publicSymbolTable();
                }
            }
            int sizeModifier = sizeModifierInteger;
            if (sizeModifier > 0 && TapEnv.relatedLanguageIsFortran9x()) {
                return ILUtils.build(ILLang.op_realCst, "1.0_" + sizeModifier);
            } else if (sizeModifier == -2 || sizeModifier == 8) {
                if (TapEnv.relatedLanguageIsC()) {
                    return ILUtils.build(ILLang.op_realCst, "1.0");
                } else {
                    return ILUtils.build(ILLang.op_realCst, "1.D0");
                }
            } else {
                return ILUtils.build(ILLang.op_realCst, "1.0");
            }
        } else {
            return null;
        }
    }

    @Override
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        return elementType.containsMetaType(dejaVu);
    }

    @Override
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        ModifiedTypeSpec copiedResult = (ModifiedTypeSpec) findAlreadyCopiedType(toAlreadyCopied, containsMeta);
        if (copiedResult == null) {
            // Insert right away an unfinished copy into "toAlreadyCopied", to avoid infinite looping:
            copiedResult = new ModifiedTypeSpec(null, this);
            copiedResult.undefinedSize = undefinedSize;
            copiedResult.setOrAddTypeDeclName(typeDeclName());
            TapTriplet<TypeSpec, TypeSpec, Boolean> alreadyRef = new TapTriplet<>(this, copiedResult, Boolean.FALSE);
            toAlreadyCopied.placdl(alreadyRef);
            if (elementType != null) {
                copiedResult.elementType = (WrapperTypeSpec) elementType.localize(toAlreadyCopied, containsMeta);
            }
            if (containsMeta.get())
            // If this type contains a meta, write it in the memo of already localized types:
            {
                alreadyRef.third = Boolean.TRUE;
            } else
            // Now that we have avoided infinite looping in this type, if this type contains no Meta type,
            // we remove it from toAlreadyCopied to make sure that only copies of types with Meta are shared.
            {
                alreadyRef.first = null;
            }

        }
        return copiedResult;
    }

    @Override
    public TypeSpec preciseDimensions(TypeSpec complementType,
                                      TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, SymbolTable symbolTable) {
        TypeSpec precisedType = TapList.cassq(this, dejaVu);
        if (precisedType == null) {
            if (!isA(complementType, SymbolTableConstants.MODIFIEDTYPE)) {
                // When the complement type is not modified,
                // we precise this.elementtype. We also take away possible "undefinedSize".
                TypeSpec precisedElementActualType = null;
                assert elementType != null;
                if (elementType.wrappedType != null) {
                    precisedElementActualType =
                            elementType.wrappedType.preciseDimensions(complementType, dejaVu, symbolTable);
                }
                if (!undefinedSize && precisedElementActualType == elementType.wrappedType) {
                    precisedType = this;
                } else if (undefinedSize) {
                    precisedType = precisedElementActualType;
                } else {
                    precisedType = new ModifiedTypeSpec(new WrapperTypeSpec(precisedElementActualType), this);
                }
            } else {
                precisedType = new ModifiedTypeSpec(null, this);
                dejaVu.placdl(new TapPair<>(this, precisedType));
                WrapperTypeSpec precisedElementType =
                        WrapperTypeSpec.preciseDimensions(elementType, ((ModifiedTypeSpec) complementType).elementType,
                                dejaVu, symbolTable);
                ModifiedTypeSpec complementModifiedType = (ModifiedTypeSpec) complementType;
                Tree complementModValue = complementModifiedType.sizeModifierResolved;
                boolean preciseSize = undefinedSize
                        || (sizeModifierResolved == null || sizeModifierResolved.opCode() != ILLang.op_intCst)
                        && complementModValue != null && complementModValue.opCode() == ILLang.op_intCst;
                if (precisedElementType == elementType && !preciseSize
                        || precisedElementType.wrappedType == null) {
                    precisedType = this;
                } else {
                    ((ModifiedTypeSpec) precisedType).elementType = precisedElementType;
                    ((ModifiedTypeSpec) precisedType).sizeModifier = complementModifiedType.sizeModifier;
                    ((ModifiedTypeSpec) precisedType).sizeModifierResolved = complementModValue;
                    ((ModifiedTypeSpec) precisedType).sizeModifierInteger = complementModifiedType.sizeModifierInteger;
                }
            }
        }
        return precisedType;
    }

    @Override
    protected TypeSpec addMultiDirDimension(ArrayDim dimension) {
        if (isA(elementType, SymbolTableConstants.ARRAYTYPE)) {
            return new ModifiedTypeSpec(elementType.addMultiDirDimension(dimension), this);
        } else {
            ArrayDim[] dimensions = new ArrayDim[1];
            dimensions[0] = dimension;
            return new ArrayTypeSpec(new WrapperTypeSpec(this), dimensions);
        }
    }

    @Override
    public TypeSpec intToReal(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                              SymbolTable symbolTable) {
        TypeSpec convertedType = TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            convertedType = new ModifiedTypeSpec(null, this);
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedElementType =
                    WrapperTypeSpec.intToReal(elementType, dejaVu, symbolTable);
            if (convertedElementType == elementType
                    || convertedElementType.wrappedType == null) {
                convertedType = this;
            } else {
                ((ModifiedTypeSpec) convertedType).elementType = convertedElementType;
            }
        }
        return convertedType;
    }

    @Override
    protected TypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        TypeSpec convertedType = TapList.cassq(this, dejaVu);
        if (convertedType == null) {
            ModifiedTypeSpec convertedModifiedType = new ModifiedTypeSpec(null, this);
            convertedModifiedType.sizeModifier = convertedModifiedType.complexDuplModifier() ;
            if (convertedModifiedType.sizeModifierInteger>0) {
                convertedModifiedType.sizeModifierInteger = 2*convertedModifiedType.sizeModifierInteger ;
                convertedModifiedType.sizeModifierResolved =
                    ILUtils.build(ILLang.op_intCst, convertedModifiedType.sizeModifierInteger) ;
            }
            convertedType = convertedModifiedType ;
            dejaVu.placdl(new TapPair<>(this, convertedType));
            WrapperTypeSpec convertedElementType = elementType.realToComplex(dejaVu, complexTypeSpec);
            if (convertedElementType == elementType
                    || convertedElementType.wrappedType == null) {
                convertedType = this;
            } else {
                ((ModifiedTypeSpec) convertedType).elementType = convertedElementType;
            }
        }
        return convertedType;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        WrapperTypeSpec result;
        boolean addMultiDirDimensionHere = false;
// System.out.println("DIFFERENTIATE MODIFIED TYPE:"+this+" TOSCALAR:"+elementType.isScalar()) ;
        if (multiDirMode && elementType.isScalar()/* !isA(elementType, SymbolTableConstants.COMPOSITETYPE)*/) {
            addMultiDirDimensionHere = true;
            multiDirMode = false;
        }
        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep) {
            FieldDecl[] fields = new FieldDecl[2];
            fields[0] = new FieldDecl(TapEnv.assocAddressValueSuffix(), new WrapperTypeSpec(this));
            fields[1] = new FieldDecl(TapEnv.assocAddressDiffSuffix(), new WrapperTypeSpec(this));
            Tree modifiers = null;
            if (!TapEnv.relatedLanguageIsC()) {
                modifiers =
                        ILUtils.build(ILLang.op_modifiers, ILUtils.build(ILLang.op_ident, "sequence"));
            }
            diffTypeSpec = new WrapperTypeSpec(new CompositeTypeSpec(null, fields, SymbolTableConstants.RECORDTYPE, modifiers, null));
            this.createOrGetDiffTypeDeclSymbolHolder(
                    symbolTable,
                    diffTypeSpec, fSuffix);
            result = diffTypeSpec;
        } else {
            WrapperTypeSpec diffElementType =
                    elementType.differentiateTypeSpecMemo(symbolTable, srcSymbolTable, diffUnitSort, fSuffix,
                            localDecl, multiDirMode, multiDirDimensionMax,
                            hintArrayNameInText, hintArrayNameInIdent, hintArrayNameTree, null);
            if (diffElementType == null && !addMultiDirDimensionHere) {
                result = null;
            } else {
                ModifiedTypeSpec resultModifiedTypeSpec =
                        new ModifiedTypeSpec(diffElementType == null ? elementType : diffElementType, this);
                if (TapEnv.get().complexStep && elementType.isRealBase() && diffElementType.isComplexBase()) {
                    resultModifiedTypeSpec.sizeModifier = this.complexDuplModifier() ;
                }
                TypeSpec resultActualTypeSpec = resultModifiedTypeSpec ;
                if (addMultiDirDimensionHere) {
                    resultActualTypeSpec = resultActualTypeSpec.addMultiDirDimension(multiDirDimensionMax);
                }
                result = new WrapperTypeSpec(resultActualTypeSpec);
            }
        }
        return result;
    }

    @Override
    public boolean acceptsMultiDirDimension() {
        return (elementType!=null && elementType.acceptsMultiDirDimension()) ;
    }

    @Override
    protected void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
        if (diffInfos != null && elementType != null) {
            elementType.cumulActiveParts(diffInfos, symbolTable);
            // Mark named types so that there is a differentiated type name:
            if (typeDeclName() != null && needsADiffType(null)) {
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeDeclName());
                if (typeDecl != null) {
                    typeDecl.setActive(true);
                }
            }
        }
    }

    @Override
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return elementType != null && elementType.needsADiffType(dejaVu);
    }

    @Override
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return elementType != null && elementType.isDifferentiated(dejaVu);
    }

    @Override
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        if (elementType != null && elementType.wrappedType == this) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC41) Illegal recursive type definition " + this.showType());
            return false;
        } else {
            assert elementType != null;
            return elementType.checkTypeSpecValidity(dejaVu);
        }
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        Tree result;
        if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            result = ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            dejaVu = new TapList<>(this, dejaVu);
            result = elementType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
            TapList<Tree> modifierExprs = null;
            if (!hasUndefinedSize()) {
                if (sizeModifier != null) {
                    modifierExprs = new TapList<>(ILUtils.copy(sizeModifier), modifierExprs);
                }
                if (typeSign == SymbolTableConstants.SIGNED) {
                    modifierExprs = new TapList<>(ILUtils.build(ILLang.op_ident, "signed"), modifierExprs);
                } else if (typeSign == SymbolTableConstants.UNSIGNED) {
                    modifierExprs = new TapList<>(ILUtils.build(ILLang.op_ident, "unsigned"), modifierExprs);
                }
            }
            result = addTypeModifiers(result, modifierExprs);
        }
        return result;
    }

    @Override
    public String showType() {
        int language = TapEnv.relatedUnit() != null ? TapEnv.relatedUnit().language() : -1;
        String signStr = typeSign ==
                SymbolTableConstants.UNSPECIFIED_SIGNED ? "" : typeSign == SymbolTableConstants.SIGNED ? "signed " : "unsigned ";
        if (ILUtils.isIntCst(sizeModifierResolved, -4)) {
            if (elementType.isPrimitiveTypeFloat()) {
                return signStr + "long double";
            } else {
                return "long long " + elementType.showType();
            }
        } else if (ILUtils.isIntCst(sizeModifierResolved, -2)) {
            if (elementType.isPrimitiveTypeFloat()) {
                return signStr + (TapEnv.isFortran(language) ? "DOUBLE PRECISION" : "double");
            } else if (elementType.isPrimitiveTypeInteger()) {
                return signStr + "long int";
            } else {
                return signStr + (TapEnv.isFortran(language) ? "DOUBLE PRECISION " : "long ") + elementType.showType();
            }
        } else if (ILUtils.isIdent(sizeModifierResolved, TapEnv.UNDEFINEDSIZE, true)) {
            return signStr + elementType.showType();
        } else {
            return signStr + elementType.showType() + (TapEnv.isFortran(language) ? "*" : "_") + ILUtils.toString(sizeModifierResolved);
        }
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "Modified" //+"@"+Integer.toHexString(hashCode())
                + '(' + ILUtils.toString(sizeModifier) + "->"
                + ILUtils.toString(sizeModifierResolved) + (hasUndefinedSize() ? " UndefSize! " : "")
                + '|' +
                (typeSign == SymbolTableConstants.UNSPECIFIED_SIGNED ? ""
                        : typeSign == SymbolTableConstants.SIGNED ? "signed" : "unsigned") + ") "
                + (elementType == null ? "?" : elementType.toString());
    }
}
