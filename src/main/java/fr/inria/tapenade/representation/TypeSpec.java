/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject;
import fr.inria.tapenade.utils.Tree;

/**
 * Abstract class, parent of all classes that actually specify a type.
 * All type classes derive from TypeSpec.
 */
public abstract class TypeSpec {

    public static final String TARGET = "target";
    private static final int BEINGSIZED = -2;
    private static final int UNKNOWNSIZE = -1;
    private static final int ILLEGALTYPESIZE = 1;

    private static final int INFERENCE = 8;
    private static final int VECTORIAL = 16;
    private static final int LOOKDIMLENGTH = 32;
    private static final int RECEIVE = 64;
    /**
     * 64+32+16+8+3
     */
    private static final int RECVVECTINFRCPDP = 123;
    /**
     * 64+32   +8+4
     */
    private static final int RECVooooINFRNOSZ = 108;
    /**
     * 64+32   +8+3
     */
    private static final int RECVooooINFRCPDP = 107;
    /**
     * 64+32     +3
     */
    private static final int RECVooooooooCPDP = 99;
    /**
     * 64   +16  +4
     */
    private static final int RECVVECTNOLNNOSZ = 84;
    /**
     * 32     +4
     */
    private static final int ooooooooooooNOSZ = 36;
    /**
     * 16  +4
     */
    private static final int ooooNOLNooooNOSZ = 20;
    /**
     * 32     +3
     */
    private static final int ooooooooooooCPDP = 35;
    /**
     * 16  +3
     */
    private static final int ooooNOLNooooCPDP = 19;
    /**
     * 32     +2
     */
    private static final int ooooooooooooCPID = 34;
    /**
     * 32     +1
     */
    private static final int ooooooooooooEQEQ = 33;
    /**
     * Current indentation in debug display of the progress of comparesWith() comparison.
     */
    protected static int compare_debug_indent;
    /**
     * The position of the tree that defines this TypeSpec.
     */
    private final Tree position = null;
    /**
     * When this TypeSpec is in fact a differentiated type, this field
     * holds the NewSymbolHolder of the name of this differentiated type.
     */
    public NewSymbolHolder diffTypeDeclSymbolHolder;
    public WrapperTypeSpec diffTypeSpec;
    public TypeDecl diffFromTypeDecl;
    /**
     * kind of this type: {VOIDTYPE, LABELTYPE, PRIMITIVETYPE, MODIFIEDTYPE, ENUMTYPE,
     * POINTERTYPE, ARRAYTYPE, COMPOSITETYPE, FUNCTIONTYPE}.
     */
    private final int kind;
    private int storedSize = UNKNOWNSIZE;
    /**
     * List of TapPair&lt;unit, typeDeclName&gt;.
     */
    private TapList<TapPair<Unit, String>> typeDeclNames;
    /**
     * Type name (before rename).
     */
    private String typeDeclName;

    /**
     * Creation.
     *
     * @param kind The kind of this type, may be in strange cases UNKNOWNTYPE, ACTUALTYPE, METATYPE, WRAPPERTYPE,
     *             also still bizarre NAMEDTYPE but this may disappear, otherwise belonging to
     *             {VOIDTYPE, LABELTYPE, PRIMITIVETYPE, MODIFIEDTYPE, ENUMTYPE,
     *             POINTERTYPE, ARRAYTYPE, COMPOSITETYPE, FUNCTIONTYPE}.
     */
    protected TypeSpec(int kind) {
        super();
        this.kind = kind;
    }

    /**
     * True if the given type is possibly differentiable i.e. contains some continuous component such as a float.
     * This is weaker than isDifferentiated(), which checks that at least one variable of this type has been found active.
     *
     * @param actualTypeSpec the given type
     * @return true if the given "actualTypeSpec" is possibly differentiable.
     */
    public static boolean isDifferentiableType(TypeSpec actualTypeSpec) {
        return isDifferentiableTypeRec(actualTypeSpec, null);
    }

    private static boolean isDifferentiableTypeRec(TypeSpec actualTypeSpec, TapList<TypeSpec> dejaVu) {
        // Case of infinite recursion in types:
        if (TapList.contains(dejaVu, actualTypeSpec)) {
            return true;
        }
        //If type (still) unknown, say yes :
        if (actualTypeSpec == null) {
            return true;
        }
        dejaVu = new TapList<>(actualTypeSpec, dejaVu);
        switch (actualTypeSpec.kind()) {
            case SymbolTableConstants.FUNCTIONTYPE: {
                WrapperTypeSpec resultType = ((FunctionTypeSpec) actualTypeSpec).returnType;
                WrapperTypeSpec[] argTypes = ((FunctionTypeSpec) actualTypeSpec).argumentsTypes;
                int i = argTypes == null ? -1 : argTypes.length - 1;
                boolean oneDifferentiableArg = false;
                if (resultType != null && !isA(resultType, SymbolTableConstants.VOIDTYPE)) {
                    oneDifferentiableArg = isDifferentiableTypeRec(
                            resultType.wrappedType, dejaVu);
                }
                while (!oneDifferentiableArg && i >= 0) {
                    assert argTypes != null;
                    oneDifferentiableArg = argTypes[i] == null
                            || isDifferentiableTypeRec(
                            argTypes[i].wrappedType, dejaVu);
                    i--;
                }
                return oneDifferentiableArg;
            }
            case SymbolTableConstants.ARRAYTYPE: {
                WrapperTypeSpec elementType = actualTypeSpec.elementType();
                return elementType == null
                        || isDifferentiableTypeRec(elementType.wrappedType,
                        dejaVu);
            }
            case SymbolTableConstants.MODIFIEDTYPE: {
                WrapperTypeSpec elementType = actualTypeSpec.elementType();
                return elementType == null
                        || isDifferentiableTypeRec(elementType.wrappedType,
                        dejaVu);
            }
            case SymbolTableConstants.WRAPPERTYPE: {
                TypeSpec wrappedType = ((WrapperTypeSpec) actualTypeSpec).wrappedType;
                return wrappedType == null || isDifferentiableTypeRec(wrappedType, dejaVu);
            }
            case SymbolTableConstants.COMPOSITETYPE: {
                CompositeTypeSpec compositeTypeSpec = (CompositeTypeSpec) actualTypeSpec;
                boolean isDifferentiable = false;
                int i = compositeTypeSpec.fields.length - 1;
                while (!isDifferentiable && i >= 0) {
                    FieldDecl fieldDecl = compositeTypeSpec.fields[i];
                    isDifferentiable = fieldDecl == null             //This case for erroneous programs
                            || fieldDecl.type() == null // If type unknown, say yes. (not sure?)
                            || isDifferentiableTypeRec(fieldDecl.type().wrappedType, dejaVu);
                    i--;
                }
                return isDifferentiable;
            }
            case SymbolTableConstants.POINTERTYPE: {
                WrapperTypeSpec destinationType = ((PointerTypeSpec) actualTypeSpec).destinationType;
                return destinationType == null ||
                        isDifferentiableTypeRec(destinationType.wrappedType, dejaVu);
            }
            case SymbolTableConstants.PRIMITIVETYPE: {
                String typeName = ((PrimitiveTypeSpec) actualTypeSpec).name();
                // case "-differentiateintegers"
                return "float".equals(typeName) ||
                        "complex".equals(typeName) ||
                        TapEnv.diffKind() == SymbolTableConstants.ALLKIND && "integer".equals(typeName);
            }
            // In C, a void* can very well cast into a double* :
            case SymbolTableConstants.VOIDTYPE:
            case SymbolTableConstants.METATYPE:
                return true;
            default:
                return false;
        }
    }

    protected static boolean ignoreTypeDeclName(String typeDeclName) {
        return typeDeclName == null
                || typeDeclName.startsWith("struct ")
                || typeDeclName.startsWith("enum ")
                || typeDeclName.startsWith("union ")
                // pb FILE dans <stdio.h>:
                || typeDeclName.startsWith("__FILE")
                || typeDeclName.startsWith("_IO_FILE")
                || typeDeclName.startsWith("_G");
    }

    /**
     * Build a TypeSpec from a tree.
     *
     * @param typeTree             tree representing a type.
     * @param symbolTable          current symbolTable.
     * @param instruction          Instruction containing the type definition.
     * @param toTypeUsedSymbols    types used in the definition.
     * @param toSymbolDeclInfos    type modifiers.
     * @param toSymbolDeclAccess   type access modifiers.
     * @param isPointer            pointer or not pointer.
     * @param currentBuiltTypeSpec for recursive type definition.
     * @return a typeSpec.
     */
    public static WrapperTypeSpec build(Tree typeTree, SymbolTable symbolTable,
                                        Instruction instruction, TapList<SymbolDecl> toTypeUsedSymbols,
                                        TapList<String> toSymbolDeclInfos, TapList<String> toSymbolDeclAccess,
                                        ToBool isPointer, WrapperTypeSpec currentBuiltTypeSpec) {
        switch (typeTree.opCode()) {
            case ILLang.op_none:
                // typeTree == none() => return WrapperTypeSpec := WrapperTypeSpec(null)
                return new WrapperTypeSpec(null);
            case ILLang.op_void:
                // typeTree == void() => return WrapperTypeSpec := WrapperTypeSpec(void())
                return new WrapperTypeSpec(new VoidTypeSpec());
            case ILLang.op_variableArgList:
                return new WrapperTypeSpec(null);
            case ILLang.op_integer: {
                return symbolTable.getCallGraph().globalRootSymbolTable().getTypeDecl("integer").typeSpec;
            }
            case ILLang.op_float: {
                return symbolTable.getCallGraph().globalRootSymbolTable().getTypeDecl("float").typeSpec;
            }
            case ILLang.op_complex: {
                return symbolTable.getCallGraph().globalRootSymbolTable().getTypeDecl("complex").typeSpec;
            }
            case ILLang.op_boolean: {
                return symbolTable.getCallGraph().globalRootSymbolTable().getTypeDecl("boolean").typeSpec;
            }
            case ILLang.op_character: {
                return symbolTable.getCallGraph().globalRootSymbolTable().getTypeDecl("character").typeSpec;
            }
            case ILLang.op_scopeAccess: {
                SymbolTable scopeST = symbolTable.getPrefixedNamedScope(typeTree.down(1));
                if (scopeST == null) {
                    return new WrapperTypeSpec(null);
                } else {
                    return build(typeTree.down(2), scopeST, instruction, toTypeUsedSymbols,
                            toSymbolDeclInfos, toSymbolDeclAccess, isPointer, currentBuiltTypeSpec);
                }
            }
            case ILLang.op_classType: {
                WrapperTypeSpec resultType =
                    build(typeTree.down(1), symbolTable, instruction, toTypeUsedSymbols,
                          toSymbolDeclInfos, toSymbolDeclAccess, isPointer, currentBuiltTypeSpec);
                return new WrapperTypeSpec(new PolymorphicTypeSpec(resultType));
            }
            case ILLang.op_procedureInterfaceName:
                if (ILUtils.isNullOrNone(typeTree.down(1))) {
                    return new WrapperTypeSpec(new FunctionTypeSpec(null, null, false)) ;
                } else {
                    // The (F2003) constructor "procedureInterfaceName" is ambiguous:
                    // "PROCEDURE(name), POINTER :: P" may mean that "name" is
                    // either the name of the function that P points to (i.e. its INTERFACE),
                    // or the name of the *type* of that function (i.e. its ABSTRACT INTERFACE).
                    String name = ILUtils.getIdentString(typeTree.down(1));
                    TapList<FunctionDecl> functionOrInterfaceDecls =
                        symbolTable.getFunctionDecl(name, null, null, false) ;
                    if (functionOrInterfaceDecls!=null) {
                        return functionOrInterfaceDecls.head.type() ;
                    } else {
                        TypeDecl typeDecl = symbolTable.getTypeDecl(name);
                        if (typeDecl == null) {
                            // default choice: build a TypeDecl. But this may be invalidated later
                            // during some later Unit.addInterfaceDecl():
                            typeDecl = new TypeDecl(name, new WrapperTypeSpec(null));
                            symbolTable.addSymbolDecl(typeDecl);
                        }
                        return typeDecl.typeSpec;
                    }
                }
            case ILLang.op_ident: {
                String typeName = ILUtils.getIdentString(typeTree);
                TypeDecl typeDecl = symbolTable.getTypeDecl(typeName);
                ClassDecl classDecl;
                if (typeDecl == null) { // maybe a classDecl ?
                    classDecl = symbolTable.getClassDecl(typeName);
                    if (classDecl != null) {
                        ClassDecl topClassDecl = symbolTable.getTopClassDecl(typeName);
                        if (topClassDecl != null) {
                            toTypeUsedSymbols.placdl(topClassDecl);
                        }
                        return new WrapperTypeSpec(classDecl.unit.classTypeSpec());
                    }
                }
                if (typeDecl == null) {
                    typeDecl =
                            new TypeDecl(typeName, new WrapperTypeSpec(new NamedTypeSpec(typeName)));
                    symbolTable.addSymbolDecl(typeDecl);
                }
                return typeDecl.typeSpec;
            }
            case ILLang.op_metavar: {
                String typeName = ILUtils.getIdentString(typeTree);
                return new WrapperTypeSpec(new MetaTypeSpec(typeName));
            }
            case ILLang.op_unionType:
            case ILLang.op_recordType: {
                WrapperTypeSpec result;
                int i;
                int j;
                Tree fieldDecl;
                Tree[] declarators;
                WrapperTypeSpec fieldTypeSpec;
                FieldDecl fieldSymbolDecl;
                TapList<FieldDecl> fieldList = null;
                boolean isRecord = typeTree.opCode() == ILLang.op_recordType;
                // E.g. F95 modifiers such as "sequence", "private"... :
                Tree modifiersTree = isRecord ? typeTree.down(2) : null;
                Tree fieldsTree = typeTree.down(isRecord ? 3 : 2);
                if (ILUtils.isNullOrNone(fieldsTree)) {
                    //case of  "struct structName var ;" with type "structName" being defined around or defined elsewhere:
                    String typeName = ILUtils.buildTypeName(typeTree);
                    TypeDecl typeDecl = symbolTable.getTypeDecl(typeName);
                    if (typeDecl == null) {
                        typeDecl = symbolTable.getTypeDecl((typeTree.opCode() == ILLang.op_recordType ? "struct " : "union ") + typeName);
                    }
                    if (typeDecl != null) {
                        result = typeDecl.typeSpec;
                    } else {
                        result = symbolTable.findCompositeTypeSpec(typeName);
                    }
                    if (result == null) {
                        if (isA(currentBuiltTypeSpec, SymbolTableConstants.COMPOSITETYPE) &&
                                typeName.equals(((CompositeTypeSpec) currentBuiltTypeSpec.wrappedType).name)) {
                            result = currentBuiltTypeSpec;
                        } else {
                            result = new WrapperTypeSpec(
                                    new CompositeTypeSpec(
                                            typeName, new FieldDecl[0],
                                            isRecord ? SymbolTableConstants.RECORDTYPE : SymbolTableConstants.UNIONTYPE,
                                            null, TapEnv.relatedUnit()));
                            symbolTable.danglingCircularTypes = new TapList<WrapperTypeSpec>(result, symbolTable.danglingCircularTypes);
                        }
                    }
                } else {
                    Tree[] fieldDecls = fieldsTree.children();
                    FieldDecl[] fieldArray = new FieldDecl[0];
                    String compositeTypeName = ILUtils.getIdentString(typeTree.down(1));
                    result = new WrapperTypeSpec(new CompositeTypeSpec(compositeTypeName, fieldArray,
                                                           (isRecord ? SymbolTableConstants.RECORDTYPE : SymbolTableConstants.UNIONTYPE),
                                                           modifiersTree, TapEnv.relatedUnit()));
                    for (i = 0; i < fieldDecls.length; ++i) {
                        TapList<String> toFieldInfos = new TapList<>(null, null);
                        TapList<String> toFieldAccess = new TapList<>(null, null);
                        ToBool fieldIsPointer = new ToBool(false);
                        fieldDecl = fieldDecls[i];
                        if (fieldDecl.opCode() == ILLang.op_varDeclaration) {
                            fieldTypeSpec = build(fieldDecl.down(2), symbolTable, instruction,
                                    toTypeUsedSymbols, toFieldInfos, toFieldAccess, fieldIsPointer, result);
                            declarators = fieldDecl.down(3).children();
                            for (j = 0; j < declarators.length; j++) {
                                if (declarators[j].opCode() != ILLang.op_functionDeclarator) {
                                    // TODO: Don't know exactly yet what to do about function declarators in a record...
                                    fieldSymbolDecl = (FieldDecl) SymbolDecl.build(fieldTypeSpec,
                                            toFieldAccess.tail, fieldIsPointer.get(), declarators[j], SymbolTableConstants.FIELD,
                                            symbolTable, instruction, toTypeUsedSymbols.tail);
                                    if (TapList.containsEquals(toFieldInfos.tail, "private")) {
                                        TapEnv.fileWarning(TapEnv.MSG_WARN, fieldDecl, "(DD18) Tapenade happily ignores private modifier in a composite type");
                                    }
                                    assert fieldSymbolDecl != null;
                                    fieldSymbolDecl.addExtraInfo(toFieldInfos.tail);
                                    fieldList = new TapList<>(fieldSymbolDecl, fieldList);
                                    toTypeUsedSymbols.tail =
                                            TapList.prependNoDups(fieldSymbolDecl.dependsOn(), toTypeUsedSymbols.tail);
                                }
                            }
                        } else if (fieldDecl.opCode() == ILLang.op_unionType ||
                                fieldDecl.opCode() == ILLang.op_recordType) {
                            // Bizarre cases of fields with no name...
                            build(fieldDecl, symbolTable, instruction,
                                    toTypeUsedSymbols, toFieldInfos, toFieldAccess, fieldIsPointer, result);
                        }
                    }
                    // We build an array of all field declarations:
                    fieldArray = new FieldDecl[TapList.length(fieldList)];
                    for (i = TapList.length(fieldList) - 1; i >= 0; --i) {
                        assert fieldList != null;
                        fieldArray[i] = fieldList.head;
                        fieldList = fieldList.tail;
                    }
                    ((CompositeTypeSpec) result.wrappedType).fields = fieldArray;
                    ((CompositeTypeSpec) result.wrappedType).initActiveFields();
                }
                typeTree.setAnnotation("WrapperTypeSpec", result);
                return result;
            }
            case ILLang.op_pointerType: {
                ToBool subIsPointer = new ToBool(false);
                TapList<String> toSubAccess = new TapList<>(null, null);
                WrapperTypeSpec resultType =
                        build(typeTree.down(1), symbolTable, instruction,
                                toTypeUsedSymbols, toSymbolDeclInfos, toSubAccess, subIsPointer, null);
                //Should never happen, logically:
                if (subIsPointer.get()) {
                    resultType = new WrapperTypeSpec(new PointerTypeSpec(resultType, null));
                }

                PointerTypeSpec pointerType = new PointerTypeSpec(resultType, null);
                pointerType.subAccess = toSubAccess.tail;
                return new WrapperTypeSpec(pointerType);
            }
            case ILLang.op_referenceType: {
                TapList<String> toSubAccess = new TapList<>(null, null);
                WrapperTypeSpec resultType =
                        build(typeTree.down(1), symbolTable, instruction,
                                toTypeUsedSymbols, toSymbolDeclInfos, toSubAccess, new ToBool(false), null);
                ReferenceTypeSpec referenceType = new ReferenceTypeSpec(resultType);
                referenceType.subAccess = toSubAccess.tail;
                return new WrapperTypeSpec(referenceType);
            }
            case ILLang.op_modifiedType: {
                WrapperTypeSpec resultType =
                        build(typeTree.down(2), symbolTable, instruction,
                                toTypeUsedSymbols, toSymbolDeclInfos, toSymbolDeclAccess, isPointer, null);
                Tree[] modifiersTrees = typeTree.down(1).children();
                //[llh 4Sept2017] Patch because a bug in version 6119 has created "float real" C types,
                // and they must be understood as a plain "float"s.
                if (modifiersTrees.length == 1 && ILUtils.isIdent(modifiersTrees[0], "float", true)
                        && !resultType.baseTypeName().equals("complex")) {
                    typeTree.down(1).removeChild(1);
                    modifiersTrees = typeTree.down(1).children();
                }

                TapPair<Tree, ArrayDim[]> sizeAndDims = new TapPair<>(null, null);
                int typeSign = dispatchModifiers(modifiersTrees, null, sizeAndDims,
                        isPointer, toSymbolDeclInfos, toSymbolDeclAccess, toTypeUsedSymbols,
                        typeTree, instruction, symbolTable);
                // sizeAndDims.first contains the sizeTypeModifier Tree, and .second contains the ArrayDim[] dimensions.
                if (sizeAndDims.first != null || typeSign != 0) {
                    ModifiedTypeSpec modifiedType = new ModifiedTypeSpec(resultType, sizeAndDims.first, symbolTable);
                    modifiedType.typeSign = typeSign;
                    resultType = new WrapperTypeSpec(modifiedType);
                }
                if (sizeAndDims.second != null) {
                    resultType = new WrapperTypeSpec(new ArrayTypeSpec(resultType, sizeAndDims.second));
                }
                return resultType;
            }
            case ILLang.op_arrayType: {
                WrapperTypeSpec resultType = build(typeTree.down(1), symbolTable, instruction,
                        toTypeUsedSymbols, toSymbolDeclInfos, toSymbolDeclAccess, isPointer, null);
                ArrayDim[] dimensions =
                        buildDimsSpec(typeTree.down(2), symbolTable, toTypeUsedSymbols, null);
                if (TapEnv.inputLanguage() == TapEnv.C) {
                    return new WrapperTypeSpec(new PointerTypeSpec(resultType,
                            dimensions.length > 0 ? dimensions[0] : null));
                } else {
                    return new WrapperTypeSpec(new ArrayTypeSpec(resultType, dimensions));
                }
            }
            case ILLang.op_enumType: {
                WrapperTypeSpec result;
                String name = "";
                if (typeTree.down(1).opCode() != ILLang.op_none) {
                    name = typeTree.down(1).stringValue();
                }
                Tree previousIndexEnum = ILUtils.build(ILLang.op_intCst, -1);
                result = new WrapperTypeSpec(new EnumTypeSpec(name, typeTree.down(2)));
                if (typeTree.down(2).opCode() != ILLang.op_none) {
                    for (int i = 1; i <= typeTree.down(2).children().length; i++) {
                        Tree decl;
                        Tree oneDeclarator;
                        String declName;
                        if (typeTree.down(2).down(i).opCode() == ILLang.op_assign) {
                            declName = ILUtils.baseName(typeTree.down(2).down(i).down(1));
                            previousIndexEnum = ILUtils.copy(typeTree.down(2).down(i).down(2));
                            oneDeclarator = ILUtils.copy(typeTree.down(2).down(i));
                        } else {
                            declName = ILUtils.baseName(typeTree.down(2).down(i));
                            previousIndexEnum = ILUtils.addTree(ILUtils.build(ILLang.op_intCst, 1),
                                    ILUtils.copy(previousIndexEnum));
                            oneDeclarator = ILUtils.build(ILLang.op_assign,
                                    ILUtils.copy(typeTree.down(2).down(i)),
                                    previousIndexEnum);
                        }
                        decl = ILUtils.build(ILLang.op_varDeclaration,
                                ILUtils.build(ILLang.op_modifiers,
                                        // make sure the enum elements remain local to the translation unit:
                                        ILUtils.build(ILLang.op_ident, "privateEnumElement")),
                                ILUtils.build(ILLang.op_none),
                                ILUtils.build(ILLang.op_declarators, oneDeclarator));
                        symbolTable.addVarDeclaration(decl, null, false, null);
                        VariableDecl varDecl = symbolTable.getTopVariableDecl(declName);
                        if (varDecl != null) {
                            varDecl.setType(result);
                            varDecl.setConstant();
                            varDecl.isATrueSymbolDecl = !TapEnv.inStdCIncludeFile();
                            varDecl.setInstruction(instruction);
                        }
                    }
                } else {
                    if (!name.isEmpty()) {
                        TypeDecl typeDecl = symbolTable.getTypeDecl(name);
                        if (typeDecl == null) {
                            typeDecl = symbolTable.getTypeDecl("enum " + name);
                        }
                        if (typeDecl != null) {
                            result = typeDecl.typeSpec;
                        }
                    }
                }
                return result;
            }
            case ILLang.op_functionType: {
                WrapperTypeSpec returnTypeSpec =
                        build(typeTree.down(1), symbolTable, instruction,
                                toTypeUsedSymbols, toSymbolDeclInfos, new TapList<>(null, null), isPointer, null);
                ToBool isVariableArgList = new ToBool(false);
                TapList<WrapperTypeSpec> argList = SymbolDecl.buildTypeSpecFromArgs(symbolTable, instruction, toTypeUsedSymbols,
                        typeTree.down(2).children(), isVariableArgList);
                WrapperTypeSpec[] argsTypeSpecs = new WrapperTypeSpec[TapList.length(argList)];
                for (int i = TapList.length(argList) - 1; i >= 0; --i) {
                    argsTypeSpecs[i] = argList.head;
                    argList = argList.tail;
                }
                return new WrapperTypeSpec(new FunctionTypeSpec(returnTypeSpec, argsTypeSpecs, isVariableArgList.get()));
            }
            case ILLang.op_declarators: {
                return new WrapperTypeSpec(new MetaTypeSpec("Error"));
            }
            default:
                TapEnv.toolWarning(-1, "(TypeSpec.build) Unexpected operator: " + typeTree.opName() + " in " + typeTree);
                return new WrapperTypeSpec(null);
        }
    }

    /**
     * Dispatch modifiers:<br>
     * -- "type size" modifiers are combined into one "sizeTypeModifier", that will
     * be stored eventually in the returned ModifiedTypeSpec<br>
     * -- "signed" and "unsigned" modifiers are collected into int "typeSign" (default 0)
     * which will be stored eventually in the returned ModifiedTypeSpec (similarly to "type size" modifiers)<br>
     * -- "dimension" modifiers are collected into "dimensions" and used eventually to
     * build an ArrayTypeSpec layer in the returned ModifiedTypeSpec<br>
     * -- "pointer" modifiers sets boolean "isPointer", that will be used eventually to
     * add a pointer layer at the appropriate location in the SymbolDecl's type.<br>
     * -- "accessDecl(bind,...)" modifiers are directly attached to the Instruction.tree<br>
     * -- "restrict", "const"... modifiers are collected into "toSymbolDeclAccess" and will
     * be stored eventually into the "subAccessInfo" of the enclosing TypeSpec or of the SymbolDecl<br>
     * -- all other modifiers are collected into "toSymbolDeclInfos", that will
     * be stored eventually into the "extraInfo" of the SymbolDecl
     * (but maybe they should all be treated like "const" etc ?)
     */
    protected static int dispatchModifiers(Tree[] modifiersTrees, Tree modifiedTree, TapPair<Tree, ArrayDim[]> sizeAndDims,
                                           ToBool isPointer, TapList<String> toSymbolDeclInfos, TapList<String> toSymbolDeclAccess,
                                           TapList<SymbolDecl> toTypeUsedSymbols,
                                           Tree typeTree, Instruction instruction, SymbolTable symbolTable) {
        ArrayDim[] dimensions = null;
        Tree modifier;
        Tree sizeTypeModifier = null;
        int typeSign = 0;
        for (int i = modifiersTrees.length - 1; i >= 0; --i) {
            modifier = modifiersTrees[i];
            if (modifier != null && modifier.opCode() == ILLang.op_arrayAccess) {
                // We are before preTypeCheck(), so Fortran calls are still mistaken as arrays:
                if (symbolTable.unit != null && symbolTable.unit.fortranStuff() != null) {
                    Tree newModifier = symbolTable.unit.fortranStuff().checkArrayAccessOrCall(symbolTable, modifier);
                    if (newModifier != null && newModifier != modifier) {
                        if (TapEnv.traceTypeCheckAnalysis()) {
                            TapEnv.indentOnTrace(TapEnv.traceIndent());
                            TapEnv.printlnOnTrace("Understand modifier " + modifier + " as function call " + newModifier);
                        }
                        instruction.replaceTree(modifier, newModifier);
                        modifier = newModifier;
                    }
                }
            }
            assert modifier != null;
            switch (modifier.opCode()) {
                case ILLang.op_dimColons:
                    dimensions = buildDimsSpec(modifier, symbolTable, toTypeUsedSymbols, modifiedTree);
                    break;
                case ILLang.op_ident: {
                    String modifierName = modifier.stringValue();
                    if ("pointer".equals(modifierName)) {
                        // pour fortran90 remonter pointer
                        isPointer.set(true);
                    } else if ("allocatable".equals(modifierName)) {
                        isPointer.set(true);
                        toSymbolDeclInfos.placdl(modifierName);
                    } else if ("short".equals(modifierName)) {
                        sizeTypeModifier = modifier;
                    } else if ("double".equals(modifierName)) {
                        if (sizeTypeModifier == null) {
                            sizeTypeModifier = modifier;
                        } else if (ILUtils.isIdent(sizeTypeModifier, "long", true)) {
                            sizeTypeModifier = ILUtils.build(ILLang.op_ident, "long double");
                        }
                    } else if ("long".equals(modifierName)) {
                        if (sizeTypeModifier == null) {
                            sizeTypeModifier = modifier;
                        } else if (ILUtils.isIdent(sizeTypeModifier, "long", true)) {
                            sizeTypeModifier = ILUtils.build(ILLang.op_ident, "long long");
                        } else if (ILUtils.isIdent(sizeTypeModifier, "double", true)) {
                            sizeTypeModifier = ILUtils.build(ILLang.op_ident, "long double");
                        }
                    } else if ("signed".equals(modifierName)) {
                        typeSign = 1;
                    } else if ("unsigned".equals(modifierName)) {
                        typeSign = 2;
                    } else if ("const".equals(modifierName) ||
                            "__const".equals(modifierName) ||
                            "__const__".equals(modifierName) ||
                            "restrict".equals(modifierName) ||
                            "__restrict".equals(modifierName) ||
                            "__restrict__".equals(modifierName)) {
                        // These modifiers apply to the "current" sub-type:
                        //  => they must not be attached to the varDecl but rather to the (correct sub-) TypeSpec
                        toSymbolDeclAccess.placdl(modifierName);
                    } else if ("constant".equals(modifierName) ||
                            "__constant__".equals(modifierName) ||
                            "__shared__".equals(modifierName) ||
                            "__device__".equals(modifierName) ||
                            "__managed__".equals(modifierName) ||
                            "volatile".equals(modifierName) ||
                            "__volatile".equals(modifierName) ||
                            "__volatile__".equals(modifierName) ||
                            "save".equals(modifierName) ||
                            "auto".equals(modifierName) ||
                            "register".equals(modifierName) ||
                            "data".equals(modifierName) ||
                            "in".equals(modifierName) ||
                            "out".equals(modifierName) ||
                            "inout".equals(modifierName) ||
                            "external".equals(modifierName) ||
                            "extern".equals(modifierName) ||
                            "intrinsic".equals(modifierName) ||
                            "optional".equals(modifierName) ||
                            "deferred".equals(modifierName) ||
                            "nopass".equals(modifierName) ||
                            "pass".equals(modifierName) ||
                            "target".equals(modifierName) ||
                            "value".equals(modifierName) ||
                            "static".equals(modifierName) ||
                            "inline".equals(modifierName) ||
                            "__inline__".equals(modifierName) ||
                            "__noinline__".equals(modifierName) ||
                            "private".equals(modifierName) ||
                            "public".equals(modifierName) ||
                            "sequence".equals(modifierName) ||
                            "contiguous".equals(modifierName)) {
                        toSymbolDeclInfos.placdl(modifierName);
                    } else {
                        SymbolDecl.addUsedSymbolsInExpr(modifier, toTypeUsedSymbols, symbolTable, null, false, true);
                        if (sizeTypeModifier == null) {
                            sizeTypeModifier = modifier;
                        } else {
                            TapEnv.toolWarning(-1, "** Type modifier analysis: Can't combine type size modifier " + modifierName + " with " + ILUtils.toString(sizeTypeModifier));
                        }
                    }
                    break;
                }
                case ILLang.op_nameEq: {
                    String name = ILUtils.getIdentString(modifier.down(1));
                    if ("kind".equals(name)) {
                        sizeTypeModifier = modifier;
                    } else {
                        TapEnv.toolWarning(-1, "** Type modifier analysis: Unexpected named modifier: " + modifier + " in " + typeTree);
                    }
                    break;
                }
                case ILLang.op_accessDecl:
                    // Special case of BIND(C) specifications inside this TypeSpec:
                    if (ILUtils.isIdent(modifier.down(1), "bind", true)
                        && symbolTable.unit!=null && symbolTable.unit.fortranStuff()!=null) {
                        //TODO: It is a little sad to rebuild a temporary Tree because this looses links to the user code.
                        Tree rebuiltBindDecl = ILUtils.build(ILLang.op_accessDecl,
                                                 ILUtils.copy(modifier),
                                                 ILUtils.copy(instruction.tree.down(3))) ;
                        symbolTable.unit.fortranStuff().collectFortranCEDSForLater(new Instruction(rebuiltBindDecl), symbolTable);
                        //[llh] TODO the line below should disappear, subsumed by the lines above.
                        symbolTable.unit.fortranStuff().addBindCDecl(symbolTable, instruction.tree);
                    }
                    break;
                default:
                    // All other modifiers (expressions, intCst, call...) are considered expressions that constrain the size:
                    sizeTypeModifier = modifier;
            }
        }
        sizeAndDims.first = sizeTypeModifier;
        sizeAndDims.second = dimensions;
        return typeSign;
    }

    private static ArrayDim[] buildDimsSpec(Tree dimsTree,
                                            SymbolTable symbolTable, TapList<SymbolDecl> toTypeUsedSymbols, Tree typedTree) {
        Tree[] dimTrees = dimsTree.children();
        Tree dimTree;
        Tree lowerDimTree;
        Tree upperDimTree;
        Integer lower = null;
        Integer upper = null;
        ArrayDim[] dimensions = new ArrayDim[dimTrees.length];
        String identNext = ILUtils.getIdentString(typedTree);
        for (int i = 0; i < dimTrees.length; i++) {
            dimTree = dimTrees[i];
            if (dimTree.opCode() == ILLang.op_dimColon) {
                lowerDimTree = dimTree.down(1);
                upperDimTree = dimTree.down(2);
            } else {
                lowerDimTree = null;
                upperDimTree = dimTree;
            }
            if (lowerDimTree != null
                    && lowerDimTree.opCode() != ILLang.op_none) {
                lower = symbolTable.computeIntConstant(lowerDimTree);
                SymbolDecl.addUsedSymbolsInExpr(lowerDimTree, toTypeUsedSymbols, symbolTable, null, false, true);
            }
            if (upperDimTree != null
                    && upperDimTree.opCode() != ILLang.op_none) {
                upper = symbolTable.computeIntConstant(upperDimTree);
                SymbolDecl.addUsedSymbolsInExpr(upperDimTree, toTypeUsedSymbols, symbolTable, null, false, true);
            }
            dimensions[i] = new ArrayDim(dimTree, lower, upper, identNext == null ? null : typedTree, i + 1, dimTrees.length, 1);
        }
        return dimensions;
    }

    // Write it as virtual!
    public static WrapperTypeSpec peelSizeModifier(WrapperTypeSpec typeSpec, ToObject<ModifiedTypeSpec> toModifiedType) {
        if (typeSpec == null || typeSpec.wrappedType == null) {
            return typeSpec;
        } else if (isA(typeSpec.wrappedType, SymbolTableConstants.ARRAYTYPE)) {
            return
                    peelSizeModifier(
                            typeSpec.wrappedType.elementType(), toModifiedType);
        } else if (isA(typeSpec.wrappedType, SymbolTableConstants.MODIFIEDTYPE)) {
            ModifiedTypeSpec modifiedType = (ModifiedTypeSpec) typeSpec.wrappedType;
            if (toModifiedType!=null && modifiedType.sizeModifierValue() != -1) {
                toModifiedType.setObj(modifiedType);
            }
            return peelSizeModifier(modifiedType.elementType(), toModifiedType);
        } else {
            return typeSpec;
        }
    }

    /**
     * Peels all array dimensions around the given type, until it reaches a composite
     * type or a leaf type.
     *
     * @return a TapPair of (1) the bottom composite or leaf
     * type found and of (2) the array of ArrayDim objects that were found on the way.
     */
    protected static TapPair<WrapperTypeSpec, ArrayDim[]> peelArrayDimsAroundComposite(WrapperTypeSpec typeSpec) {
        if (TapEnv.inputLanguage() == TapEnv.C || TapEnv.inputLanguage() == TapEnv.CPLUSPLUS) {
            // No vectorial/elemental fieldAccess allowed in C/C++:
            return new TapPair<>(typeSpec, null);
        } else {
            TapList<ArrayDim> collectedDims = null;
            boolean stopHere = false;
            while (!stopHere && typeSpec != null && typeSpec.wrappedType != null) {
                switch (typeSpec.wrappedType.kind()) {
                    case SymbolTableConstants.MODIFIEDTYPE:
                        typeSpec = typeSpec.wrappedType.elementType();
                        break;
                    case SymbolTableConstants.POINTERTYPE:
                        typeSpec = ((PointerTypeSpec) typeSpec.wrappedType).destinationType;
                        break;
                    case SymbolTableConstants.ARRAYTYPE: {
                        ArrayDim[] oDims = ((ArrayTypeSpec) typeSpec.wrappedType).dimensions();
                        for (int i = oDims.length - 1; i >= 0; --i) {
                            collectedDims = new TapList<>(oDims[i], collectedDims);
                        }
                        typeSpec = typeSpec.wrappedType.elementType();
                        break;
                    }
                    default:
                        stopHere = true;
                }
            }
            ArrayDim[] dims = null;
            if (collectedDims != null) {
                int nbDims = TapList.length(collectedDims);
                dims = new ArrayDim[nbDims];
                for (int i = 0; i < nbDims; ++i) {
                    dims[i] = collectedDims.head;
                    collectedDims = collectedDims.tail;
                }
            }
            return new TapPair<>(typeSpec, dims);
        }
    }

    protected static FunctionTypeSpec findFunctionTypeSpecUnderTypeSpec(WrapperTypeSpec typeSpec) {
        FunctionTypeSpec funcTypeSpec = null;
        if (typeSpec != null && typeSpec.wrappedType != null) {
            switch (typeSpec.wrappedType.kind()) {
                case SymbolTableConstants.MODIFIEDTYPE: {
                    funcTypeSpec = findFunctionTypeSpecUnderTypeSpec(
                            typeSpec.wrappedType.elementType());
                    break;
                }
                case SymbolTableConstants.POINTERTYPE: {
                    funcTypeSpec = findFunctionTypeSpecUnderTypeSpec(
                            ((PointerTypeSpec) typeSpec.wrappedType).destinationType);
                    break;
                }
                case SymbolTableConstants.ARRAYTYPE:
                    funcTypeSpec = findFunctionTypeSpecUnderTypeSpec(
                            typeSpec.wrappedType.elementType());
                    break;
                case SymbolTableConstants.FUNCTIONTYPE:
                    funcTypeSpec = (FunctionTypeSpec) typeSpec.wrappedType;
                    break;
                case SymbolTableConstants.LABELTYPE:
                case SymbolTableConstants.PRIMITIVETYPE:
                case SymbolTableConstants.VOIDTYPE:
                case SymbolTableConstants.METATYPE:
                case SymbolTableConstants.NAMEDTYPE:
                case SymbolTableConstants.COMPOSITETYPE:
                case SymbolTableConstants.ENUMTYPE:
                default:
                    break;
            }
        }
        return funcTypeSpec;
    }

    protected static boolean canCompare(WrapperTypeSpec type1, WrapperTypeSpec type2) {

        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.printOnTrace("          ");
            for (int i = compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("TS CANCOMPARE? " + type1 + " WITH " + type2);
            ++compare_debug_indent;
        }

        //TODO: also take care of array dimensions !!
        WrapperTypeSpec baseTypeLeft = type1.baseTypeSpec(true);
        WrapperTypeSpec baseTypeRight = type2.baseTypeSpec(true);
        boolean result = false;
        boolean noReturn = false;
        if (baseTypeLeft.wrappedType == null) {
            if (baseTypeRight.wrappedType != null) {
                if (baseTypeRight.isNumericBase()) {
                    baseTypeLeft.wrappedType =
                            baseTypeRight.wrappedType.
                                    cloneAsUndefinedNumeric(true);
                } else {
                    baseTypeLeft.wrappedType = baseTypeRight.wrappedType;
                }
            }
            result = true;
        } else if (isA(baseTypeLeft, SymbolTableConstants.PRIMITIVETYPE)
                && baseTypeLeft.wrappedType
                .isUndefinedNumeric()) {
            if (baseTypeRight.wrappedType != null
                    && !baseTypeRight.isNumericBase()) {
                result = false;
            } else {
                noReturn = true;
            }
        } else {
            noReturn = true;
        }
        if (noReturn) {
            if (baseTypeRight.wrappedType == null) {
                if (baseTypeLeft.wrappedType != null) {
                    if (baseTypeLeft.isNumericBase()) {
                        baseTypeRight.wrappedType =
                                baseTypeLeft.wrappedType.
                                        cloneAsUndefinedNumeric(true);
                    } else {
                        baseTypeRight.wrappedType =
                                baseTypeLeft.wrappedType;
                    }
                }
                result = true;
            } else if (isA(baseTypeRight, SymbolTableConstants.PRIMITIVETYPE)
                    && baseTypeRight.wrappedType
                    .isUndefinedNumeric()) {
                if (baseTypeLeft.wrappedType != null
                        && !baseTypeLeft.isNumericBase()) {
                    result = false;
                } else {
                    noReturn = true;
                }
            } else {
                noReturn = true;
            }
        }
        if (noReturn) {
            assert baseTypeLeft.wrappedType != null;
            result =
                    baseTypeLeft.wrappedType.equalsCompilDep(baseTypeRight.wrappedType)
                            || baseTypeLeft.isNumericBase() && baseTypeRight.isNumericBase();
        }

        if (TapEnv.traceTypeCheckAnalysis()) {
            --compare_debug_indent;
            TapEnv.printOnTrace("          ");
            for (int i = compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace(">TS CANCOMPARE " + type1 + " WITH " + type2
                    + " RETURNS:" + result);
        }

        return result;
    }

    public static String getRenamedTypeDeclName(TapList<TapPair<Unit, String>> typeDeclNames, Unit inUnit) {
        String name = null;
        boolean found = false;
        while (typeDeclNames != null && !found) {
            found = typeDeclNames.head.first == inUnit;
            if (found) {
                name = typeDeclNames.head.second;
            }
            typeDeclNames = typeDeclNames.tail;
        }
        return name;
    }

    /**
     * @param kind may be ACTUALTYPE, ARRAYTYPE, FUNCTIONTYPE, LABELTYPE
     *             MOFIFIEDTYPE, PRIMITIVETYPE, COMPOSITETYPE, POINTERTYPE, VOIDTYPE, METATYPE
     *             (defined in SymbolTableConstants).
     * @return true when this TypeSpec is of the given kind "testKind"
     * Skips over layers of WrapperTypeSpec's.
     */
    public static boolean isA(TypeSpec type, int kind) {
        while (type instanceof WrapperTypeSpec) {
            type = ((WrapperTypeSpec) type).wrappedType;
        }
        return type != null && type.kind == kind;
    }

    protected static boolean testIsEquals(int comparison) {
        return (comparison & RECEIVE) == 0;
    }

    protected static boolean testIsReceives(int comparison) {
        return (comparison & RECEIVE) != 0;
    }

    protected static boolean testLooksDimensionLength(int comparison) {
        return (comparison & LOOKDIMLENGTH) != 0;
    }

    protected static boolean testAllowsVectorial(int comparison) {
        return (comparison & VECTORIAL) != 0;
    }

    protected static boolean testHasInference(int comparison) {
        return (comparison & INFERENCE) != 0;
    }

    protected static boolean testIsReceivesWithInference(int comparison) {
        return (comparison & 72) == 72;
    } //64+8

    protected static boolean testTypesWithoutSize(int comparison) {
        return comparison % INFERENCE == 4;
    }

    protected static boolean testTypesCompilDep(int comparison) {
        return comparison % INFERENCE == 3;
    }

    private static boolean testTypesCompilIndep(int comparison) {
        return comparison % INFERENCE == 2;
    }

    protected static boolean testTypesLitteral(int comparison) {
        return comparison % INFERENCE == 1;
    }

    protected static boolean testAcceptsUnspecified(int comparison) {
        return testIsReceives(comparison);
    }

    protected static int testSetEquals(int comparison) {
        return comparison & ~RECEIVE;
    }

    protected static int testUnsetInference(int comparison) {
        return comparison & ~INFERENCE;
    }

    public static String showComparison(int comparison) {
        return (testIsEquals(comparison) ? "EQ" : "RECV")
                + (testAllowsVectorial(comparison) ? "+VECTORIAL" : "")
                + (testLooksDimensionLength(comparison) ? "" : "+NODIMLENGTH")
                + (testHasInference(comparison) ? "+INFERENCE" : "")
                + " ["
                + (testTypesWithoutSize(comparison) ? "neglect-sizes" : testTypesCompilDep(comparison) ? "compil-dep" : testTypesCompilIndep(comparison) ? "compil-INdep" : "litt-eq")
                + "]";
    }

    /**
     * @return a declarator tree built by wrapping declarator layers around the given
     * declarator tree "declarator", each added declarator layer corresponding to the successive
     * nested type layers in "typeSpec", but stopping when reaching the type layer "stoppingTypeSpec".
     */
    public static Tree generateDeclaratorTree(WrapperTypeSpec stoppingTypeSpec,
                                              TapList<SymbolDecl> declaredTypes, Tree declarator, WrapperTypeSpec typeSpec) {
        //TODO : rewrite in object manner, into the various subclasses of TypeSpec ?
        TypeSpec actualTypeSpec;
        while (typeSpec != null &&
                !typeSpec.equalsLiterally(stoppingTypeSpec)) {
            actualTypeSpec = typeSpec.wrappedType;
            if (isA(actualTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                if (TapEnv.relatedLanguageIsFortran() && typeSpec.isString()) {
                    /*de taille 1 car string
                     cas a traiter sinon on perd le * dans
                     character truc*(10)
                     */
                    ArrayDim[] dimensions =
                            ((ArrayTypeSpec) actualTypeSpec).dimensions();
                    Tree size;
                    if (dimensions[0] != null
                            && dimensions[0].tree().down(2).opCode()
                            != ILLang.op_none) {
                        size = dimensions[0].tree().down(2).copy();
                    } else {
                        size = ILUtils.build(ILLang.op_star);
                    }
                    declarator =
                            ILUtils.build(ILLang.op_sizedDeclarator, declarator, size);
                } else {
                    declarator =
                            ILUtils.build(
                                    ILLang.op_arrayDeclarator, declarator,
                                    ILUtils.build(ILLang.op_dimColons));
                    ArrayDim[] dimensions =
                            ((ArrayTypeSpec) actualTypeSpec).dimensions();
                    for (int i = 0; i < dimensions.length; i++) {
                        if (dimensions[i] != null) {
                            declarator.down(2).setChild(
                                    dimensions[i].tree().copy(), i + 1);
                        } else {
                            declarator.down(2).setChild(
                                    ILUtils.build(
                                            ILLang.op_dimColon,
                                            ILUtils.build(ILLang.op_none),
                                            ILUtils.build(ILLang.op_none)), i + 1);
                        }
                    }
                }
                typeSpec = actualTypeSpec.elementType();
            } else if (isA(actualTypeSpec, SymbolTableConstants.POINTERTYPE)) {
                if (((PointerTypeSpec) actualTypeSpec).offsetLength != null) {
                    //For C, transform a "pointer to a fixed array zone" into a "array of...",
                    // i.e. float *x into float x[5]  :
                    typeSpec = typeSpec.pointerToArrayTypeSpec();
                } else {
                    declarator = ILUtils.build(ILLang.op_pointerDeclarator, declarator);
                    typeSpec = ((PointerTypeSpec) actualTypeSpec).destinationType;
                }
                declarator = addDeclaratorModifiers(declarator, ((PointerTypeSpec) actualTypeSpec).subAccess);
            } else if (isA(actualTypeSpec, SymbolTableConstants.FUNCTIONTYPE)) {
                declarator =
                        ILUtils.build(
                                ILLang.op_functionDeclarator, declarator,
                                ILUtils.build(ILLang.op_argDeclarations));
                WrapperTypeSpec[]
                        argumentsTypes =
                        ((FunctionTypeSpec) actualTypeSpec).argumentsTypes;
                for (int i = 0; i < argumentsTypes.length; i++) {
                    declarator.down(2).setChild(
                            argumentsTypes[i].generateTree(null, null, declaredTypes, true, null),
                            i + 1);
                }
                TapEnv.toolWarning(-1, "(Regenerate a declarator) Declarator for function not yet implemented");
                typeSpec = ((FunctionTypeSpec) actualTypeSpec).returnType;
            } else if (isA(actualTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                if (TapEnv.relatedLanguageIsFortran()) {
                    typeSpec = null;
                } else {
                    typeSpec = actualTypeSpec.elementType();
                }
            } else {
                typeSpec = null;
            }
        }
        return declarator;
    }

    /**
     * Utility for generateTree(): modifies and/or returns declaratorTree to add the given modifiers.
     */
    public static Tree addDeclaratorModifiers(Tree declaratorTree, TapList<String> modifiers) {
        if (modifiers != null) {
            Tree[] modifiersTrees;
            if (declaratorTree != null && declaratorTree.opCode() == ILLang.op_modifiedDeclarator) {
                modifiersTrees = declaratorTree.down(1).children();
                declaratorTree = declaratorTree.cutChild(2);
            } else {
                modifiersTrees = new Tree[0];
            }
            Tree[] newModifiersTrees = new Tree[modifiersTrees.length + TapList.length(modifiers)];
            int index = 0;
            while (modifiers != null) {
                newModifiersTrees[index] = ILUtils.build(ILLang.op_ident, modifiers.head);
                ++index;
                modifiers = modifiers.tail;
            }
            for (Tree modifiersTree : modifiersTrees) {
                newModifiersTrees[index] = ILUtils.copy(modifiersTree);
                ++index;
            }
            declaratorTree = ILUtils.build(ILLang.op_modifiedDeclarator,
                    ILUtils.build(ILLang.op_modifiers, newModifiersTrees),
                    declaratorTree);
        }
        return declaratorTree;
    }

    /**
     * Utility for generateTree(): modifies and/or returns typeTree to add the given modifiers.
     */
    public static Tree addTypeModifiers(Tree typeTree, TapList<Tree> modifiers) {
        if (modifiers != null) {
            Tree[] modifiersTrees;
            if (typeTree != null && typeTree.opCode() == ILLang.op_modifiedType) {
                modifiersTrees = typeTree.down(1).children();
                typeTree = typeTree.cutChild(2);
            } else {
                modifiersTrees = new Tree[0];
            }
            Tree[] newModifiersTrees = new Tree[modifiersTrees.length + TapList.length(modifiers)];
            int index = 0;
            while (modifiers != null) {
                newModifiersTrees[index] = modifiers.head;
                ++index;
                modifiers = modifiers.tail;
            }
            for (Tree modifiersTree : modifiersTrees) {
                newModifiersTrees[index] = ILUtils.copy(modifiersTree);
                ++index;
            }
            typeTree = ILUtils.build(ILLang.op_modifiedType,
                    ILUtils.build(ILLang.op_modifiers, newModifiersTrees),
                    typeTree);
        }
        return typeTree;
    }

    public TapList<TapPair<Unit, String>> typeDeclNames() {
        return typeDeclNames;
    }

    public void setTypeDeclNames(TapList<TapPair<Unit, String>> tDeclNames) {
        typeDeclNames = tDeclNames;
    }

    public void addInTypeDeclNames(Unit unit, String name) {
        typeDeclNames = new TapList<>(new TapPair<>(unit, name), typeDeclNames);
    }

    protected String genTypeDeclName() {
        String result = null;
        TapList<TapPair<Unit, String>> otherNames = typeDeclNames;
        TapPair<Unit, String> otherName;
        if (!ignoreTypeDeclName(typeDeclName)) {
            result = typeDeclName;
        }
        while (otherNames != null && result == null) {
            otherName = otherNames.head;
            if (otherName.first == null) {
                if (!ignoreTypeDeclName(otherName.second)) {
                    result = otherName.second;
                }
            }
            otherNames = otherNames.tail;
        }
        if (result == null) {
            result = typeDeclName;
        }
        return result;
    }

    public String typeDeclName() {
        return genTypeDeclName();
    }

    protected void setTypeDeclName(String name) {
        typeDeclName = name;
    }

    protected void setOrAddTypeDeclName(String name) {
        if (typeDeclName == null) {
            typeDeclName = name;
        }
        if (!TapList.containsEqualsObjectPair(typeDeclNames, null, name)) {
            typeDeclNames = new TapList<>(new TapPair<>(null, name), typeDeclNames);
        }
    }

    /**
     * The particular type sub-kind of this TypeSpec.
     */
    public int kind() {
        return kind;
    }

    protected TypeSpec findAlreadyCopiedType(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied,
                                             ToBool containsMeta) {
        toAlreadyCopied = toAlreadyCopied.tail;
        while (toAlreadyCopied != null &&
                (toAlreadyCopied.head.first == null ||
                        !(this.kind == toAlreadyCopied.head.first.kind // <-- This test is needed if we want to preserve the levels of WrapperTypeSpec (otherwise, the following equalsLiterally will skip over these wrapper levels)
                                && this.equalsLiterally(toAlreadyCopied.head.first)))) {
            toAlreadyCopied = toAlreadyCopied.tail;
        }
        if (toAlreadyCopied != null && Boolean.TRUE.equals(toAlreadyCopied.head.third)) {
            containsMeta.set(true);
        }
        return toAlreadyCopied == null ? null : toAlreadyCopied.head.second;
    }

    /**
     * Combination of two TypeSpec's.
     * Combines, when possible, this current TypeSpec with a new one "newActualTypeSpec".
     * If the combination succeeds, returns the combined TypeSpec. Otherwise returns null.
     * Warning, both this and newActualTypeSpec may be modified.
     * Warning: source language dependent!!!!
     */
    protected TypeSpec combineWith(TypeSpec newActualTypeSpec, SymbolTable symbolTable) {
        TypeSpec result = null;
        if (isA(this, SymbolTableConstants.VOIDTYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.VOIDTYPE)) {
                result = this;
            }
        } else if (isA(this, SymbolTableConstants.ARRAYTYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                if (this.isIncompleteType()) {
                    /*vmp:  ce cas se produit si
                     declaration incomplete de tableau dans une equivalence
                     -> pb pas de msg d'erreur en cas de double declaration
                     de dimension avec l'instruction dimension t(n)
                     */
                    ArrayDim[] dimThis = ((ArrayTypeSpec) this).dimensions();
                    ArrayDim[] dimNew = ((ArrayTypeSpec) newActualTypeSpec).dimensions();
                    /*if old declaration is incomplete, complete it with the new one. */
                    if (dimThis.length == dimNew.length) {
                        for (int i = dimThis.length - 1; i >= 0; i--) {
                            if (dimThis[i].tree() == null
                                    || dimThis[i].tree().down(1).opCode()
                                    == ILLang.op_none) {
                                dimThis[i].setTree(dimNew[i].tree());
                                dimThis[i].lower = dimNew[i].lower;
                                dimThis[i].upper = dimNew[i].upper;
                            }
                        }
                        result = this;
                    }
                } else if (TapEnv.inputLanguage() != TapEnv.C) {
                    if (baseTypeName().equals("character")) {
                        // character*10 t followed by dimension t[200]:
                        if (isA(this.elementType().wrappedType,
                                SymbolTableConstants.ARRAYTYPE)
                                || isA(newActualTypeSpec.elementType().
                                wrappedType, SymbolTableConstants.ARRAYTYPE)) {
                            // If old type has already 2 nested array dims: forget new type.
                            TapEnv.fileWarning(TapEnv.MSG_WARN, TapEnv.NEXT_TREE, "(DD02) Cannot combine successive declarations of character array: " + this.showType() + " and " + newActualTypeSpec.showType() + " (ignored new)");
                            result = null;
                        } else {
                            result = ((ArrayTypeSpec) newActualTypeSpec).combineArrayWith(
                                    this);
                        }
                    } else if (newActualTypeSpec.baseTypeName().equals("character")) {
                        // dimension t[200] followed by character*10 t:
                        if (isA(this.elementType().wrappedType,
                                SymbolTableConstants.ARRAYTYPE)
                                || isA(newActualTypeSpec.elementType().
                                wrappedType, SymbolTableConstants.ARRAYTYPE)) {
                            // If old type has already 2 nested array dims: forget new type.
                            TapEnv.fileWarning(TapEnv.MSG_WARN, TapEnv.NEXT_TREE, "(DD02) Cannot combine successive declarations of character array: " + this.showType() + " and " + newActualTypeSpec.showType() + " (ignored new)");
                            result = null;
                        } else {
                            result = ((ArrayTypeSpec) this).combineArrayWith(
                                    newActualTypeSpec);
                        }
                    } else {
                        result = this;
                    }
                } else {
                    /*en fortran, on n'a pas de tableau de tableau
                     on arrive dans ce cas si
                     declaration de tableau suivi d'equivalence
                     ou bien si double declaration de dimension
                     pb: on ne sait pas differencier les 2 cas
                     TODO: pour les autres langages qui l'autorisent
                     */
                    result = this;
                }
            } else {
                result = ((ArrayTypeSpec) this).combineArrayWith(
                        newActualTypeSpec);
            }
        } else if (isA(newActualTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
            result = ((ArrayTypeSpec) newActualTypeSpec).combineArrayWith(
                    this);
        } else if (isA(this, SymbolTableConstants.PRIMITIVETYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                    && newActualTypeSpec.equalsCompilIndep(this)) {
                // Combination is also able to relax "UndefinedNumeric" status. Debugs F90:v453
                if (isUndefinedNumeric() && !newActualTypeSpec.isUndefinedNumeric()) {
                    result = newActualTypeSpec;
                } else {
                    result = this;
                }
            }
        } else if (isA(this, SymbolTableConstants.MODIFIEDTYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                ModifiedTypeSpec thisModified = (ModifiedTypeSpec) this;
                ModifiedTypeSpec newModified = (ModifiedTypeSpec) newActualTypeSpec;
                if (thisModified.sizeModifierValue() == newModified.sizeModifierValue()) {
                    WrapperTypeSpec newElementType =
                            thisModified.elementType().combineWith(newModified.elementType(), symbolTable);
                    if (newElementType != null) {
                        result = new ModifiedTypeSpec(newElementType, thisModified);
                    }
                }
            }
        } else if (isA(this, SymbolTableConstants.COMPOSITETYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.COMPOSITETYPE)
                    && newActualTypeSpec.equalsCompilIndep(this)) {
                result = this;
            }
        } else if (isA(this, SymbolTableConstants.POINTERTYPE)) {
            if (isA(newActualTypeSpec, SymbolTableConstants.POINTERTYPE) && isA(((PointerTypeSpec) newActualTypeSpec).destinationType, SymbolTableConstants.ARRAYTYPE)) {
                WrapperTypeSpec thisDestTypeSpec = ((PointerTypeSpec) this).destinationType;
                WrapperTypeSpec arrayTypeSpec = ((PointerTypeSpec) newActualTypeSpec).destinationType;
                ((ArrayTypeSpec) arrayTypeSpec.wrappedType).setElementType(thisDestTypeSpec);
                ((PointerTypeSpec) this).destinationType = ((PointerTypeSpec) newActualTypeSpec).destinationType;
                result = this;
            } else {
                if (((PointerTypeSpec) this).destinationType.wrappedType == null) {
                    ((PointerTypeSpec) this).destinationType.wrappedType = newActualTypeSpec;
                }
                result = this;
            }
        }
        return result;
    }

    private boolean isIncompleteType() {
        boolean result = false;
        if (isA(this, SymbolTableConstants.ARRAYTYPE)) {
            ArrayDim[] dimThis = ((ArrayTypeSpec) this).dimensions();
            int i = 0;
            while (!result && i < dimThis.length) {
                result = dimThis[i].tree() == null
                        || dimThis[i].tree().down(1).opCode()
                        == ILLang.op_none;
                i = i + 1;
            }
        }
        return result;
    }

    protected boolean isUndefinedNumeric() {
        return false;
    }

    protected void setUndefinedNumeric(boolean value) {
    }

    private TapList<SymbolDecl> getDependsOnSymbolDeclAndModulesDef(TapList<Tree> usedTrees, SymbolTable symbolTable,
                                                                    TapList<Unit> definedInModule) {
        TapList<SymbolDecl> result = new TapList<>(null, null);
        TapList<SymbolDecl> toResult = result;
        Tree tree;
        String name;
        SymbolDecl symbolDecl;
        SymbolTable curST;
        Unit module;
        TapList<Unit> toDefinedInModule = definedInModule;
        while (usedTrees != null) {
            tree = usedTrees.head;
            // TODO traiter toutes les expressions qui apparaissent dans la definition d'un type
            // et pas seulement les ident
            name = ILUtils.baseName(tree);
            curST = symbolTable;
            symbolDecl = null;
            module = null;
            while (symbolDecl == null && curST != null) {
                if (name != null) {
                    symbolDecl = curST.getTopDecl(name, SymbolTableConstants.SYMBOL);
                    if (symbolDecl != null && curST.isImports && symbolDecl.importedFrom!=null) {
                        module = symbolDecl.importedFrom.second.unit ;
                    }
                }
                curST = curST.basisSymbolTable();
            }
            if (symbolDecl != null) {
                if (module != null) {
                    toDefinedInModule = toDefinedInModule.placdl(module);
                }
                toResult = toResult.placdl(symbolDecl);
            }
            usedTrees = usedTrees.tail;
        }
        return result.tail;
    }

    /**
     * only for association By Address.
     */
    protected void createOrGetDiffTypeDeclSymbolHolder(SymbolTable diffSymbolTable,
                                                       WrapperTypeSpec diffTypeSpec, String fSuffix) {
        NewSymbolHolder diffSH = diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder;
        SymbolTable basisSymbolTable = diffSymbolTable.findAssocAddresTypeDeclSymbolTable();
        TapList<Unit> modules = new TapList<>(null, null);
        if (diffSH == null) {
            String origTypeName = baseTypeName();
            if ("float".equals(origTypeName) && TapEnv.relatedLanguageIsFortran()) {
                origTypeName = "real";
            }
            int size = size();
            origTypeName = origTypeName + size;
            diffSH = new NewSymbolHolder(
                    TapEnv.extendStringWithSuffix(origTypeName, fSuffix));
            TapList<SymbolDecl> dependsOn;
            TapList<Tree> toUsedTrees = new TapList<>(null, null);
            TapList<TypeSpec> dejaVu = new TapList<>(null, null);
            diffTypeSpec.wrappedType.collectUsedTrees(toUsedTrees, dejaVu);
            dependsOn = diffTypeSpec.wrappedType.getDependsOnSymbolDeclAndModulesDef(toUsedTrees.tail,
                    diffSymbolTable, modules);
            diffSH.setAsType(diffTypeSpec, dependsOn);
        }
        referencesDiffSHType(basisSymbolTable, diffTypeSpec, diffSH,
                basisSymbolTable, modules.tail, fSuffix);
    }

    private void referencesDiffSHType(SymbolTable diffSymbolTable,
                                      WrapperTypeSpec diffTypeSpec, NewSymbolHolder diffSH,
                                      SymbolTable basisSymbolTable, TapList<Unit> usedModules, String fSuffix) {
        int lang = TapEnv.C;
        if (diffSymbolTable != null && diffSymbolTable.unit != null) {
            lang = diffSymbolTable.unit.language();
        }
        if (TapEnv.assocAddressDiffTypesUnit(lang) != null) {
            diffSH.addTypeDeclSymbolTable(TapEnv.assocAddressDiffTypesUnit(lang).publicSymbolTable());
            SymbolTable basisDiffSymbolTable = diffSymbolTable;
            while (basisDiffSymbolTable != null && !basisDiffSymbolTable.isFormalParamsLevel()) {
                basisDiffSymbolTable = basisDiffSymbolTable.basisSymbolTable();
            }
            while (basisDiffSymbolTable != null
                    && basisDiffSymbolTable.origUnit() != null
                    && !basisDiffSymbolTable.origUnit().isTopInFile()) {
                basisDiffSymbolTable = basisDiffSymbolTable.basisSymbolTable();
                while (basisDiffSymbolTable.basisSymbolTable() != null && !basisDiffSymbolTable.isFormalParamsLevel()) {
                    basisDiffSymbolTable = basisDiffSymbolTable.basisSymbolTable();
                }
            }
            Unit module;
            while (usedModules != null) {
                // TapEnv.assocAddressDiffTypesModule() utilise tous les modules utilise's
                // pour definir les types differenties
                module = usedModules.head;
                assert module.publicSymbolTable().origUnit() != null;
                Tree useDeclTree = ILUtils.build(ILLang.op_useDecl,
                        ILUtils.build(ILLang.op_ident,
                                module.publicSymbolTable().origUnit().name() + (TapEnv.get().stripPrimalModules ? "" : fSuffix)),
                        ILUtils.build(ILLang.op_renamedVisibles));
                Instruction useInstr = new Instruction(useDeclTree);
                useInstr.isDifferentiated = true;
                TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003).publicSymbolTable().
                        declarationsBlock.addInstrHdIfNotPresent(useInstr);
                CallGraph.addCallArrow(TapEnv.assocAddressDiffTypesUnit(TapEnv.FORTRAN2003), SymbolTableConstants.IMPORTS, module);
                usedModules = usedModules.tail;
            }
        }
        if (basisSymbolTable != null && diffSH != null) {
            diffSH.addTypeDeclSymbolTable(basisSymbolTable);
            diffSH.makeNewRef(basisSymbolTable);
            diffSH.solvingLevelMustInclude(basisSymbolTable);
            diffSH.declarationLevelMustInclude(basisSymbolTable);
        }
        diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder = diffSH;
    }

    public String getRenamedTypeDeclName(Unit inUnit) {
        return getRenamedTypeDeclName(typeDeclNames, inUnit);
    }


    public boolean isScalar() {
        TypeSpec typeSpec = this;
        while (isA(typeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
            typeSpec = typeSpec.elementType();
        }
        return isA(typeSpec, SymbolTableConstants.PRIMITIVETYPE);
    }

    /**
     * @return true if the base of this type is "boolean".
     */
    public boolean isBooleanBase() {
        return baseTypeName().equals("boolean");
    }

    /**
     * @return true if the base of this type is numeric,
     * which means either integer or real or complex (or boolean or char in C).
     */
    public boolean isNumericBase() {
        String baseName = baseTypeName();
        return baseName != null
                && ("integer".equals(baseName)
                || "float".equals(baseName)
                || "complex".equals(baseName)
                // If current language is C, elements of enum are integers:
                || TapEnv.relatedLanguageIsC() && isA(this, SymbolTableConstants.ENUMTYPE)
                || TapEnv.relatedLanguageIsC() && "char".equals(baseName)
                || TapEnv.relatedLanguageIsC() && "boolean".equals(baseName));
    }

    /**
     * @return true if the base of this type is "integer".
     */
    public boolean isIntegerBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return actualTypeSpec != null
                && (isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isInteger()
                // If current language is C, elements of enum are integers:
                || isA(actualTypeSpec, SymbolTableConstants.ENUMTYPE) && TapEnv.relatedLanguageIsC());
    }

    /**
     * @return true if the base of this type is "float".
     */
    public boolean isRealBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isReal();
    }

    /**
     * @return true if the base of this type is "complex".
     */
    public boolean isComplexBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isComplex();
    }

    /**
     * @return true if the base of this type is either "float" or "complex",
     * which means a type that may be differentiated.
     */
    public boolean isRealOrComplexBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isRealOrComplex();
    }

    /**
     * @return true if the base of this type is either "float" or "complex" or
     * a yet undefined type which can/will be only "float" or "complex".
     */
    boolean isProbablyRealOrComplexBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isProbablyRealOrComplex();
    }

    /**
     * @return true if the base of this type is the augmented type for overloading-based AD.
     */
    public boolean isAugmentedDoubleBase() {
        WrapperTypeSpec baseTypeSpec = baseTypeSpec(true);
        TypeSpec actualTypeSpec = baseTypeSpec == null ? null : baseTypeSpec.wrappedType;
        return isA(actualTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                && ((PrimitiveTypeSpec) actualTypeSpec).isAugmentedDouble();
    }

    /**
     * @return true if this type is a PRIMITIVETYPE, possibly wrapped in MODIFIEDTYPE's,
     * and its primitive type is differentiable.
     */
    public boolean isDifferentiablePlainType() {
        TypeSpec peeledType = peelWrapperAndModified(this);
        return isA(peeledType, SymbolTableConstants.PRIMITIVETYPE) && peeledType.isDifferentiated(null);
    }

    protected TypeSpec peelWrapperAndModified(TypeSpec typeSpec) {
        boolean peeling = true;
        while (peeling) {
            if (typeSpec == null) {
                peeling = false;
            } else if (typeSpec instanceof WrapperTypeSpec) {
                typeSpec = ((WrapperTypeSpec) typeSpec).wrappedType;
                peeling = true;
            } else if (typeSpec instanceof ModifiedTypeSpec) {
                typeSpec = typeSpec.elementType();
                peeling = true;
            } else {
                peeling = false;
            }
        }
        return typeSpec;
    }

    protected TypeSpec peelWrapperAndModifiedTo(TypeSpec type, TypeSpec toType) {
        boolean peeling = true;
        while (peeling) {
            if (type == null) {
                peeling = false;
            } else if (type instanceof WrapperTypeSpec) {
                toType = type;
                type = ((WrapperTypeSpec) type).wrappedType;
                peeling = true;
            } else if (type instanceof ModifiedTypeSpec) {
                toType = type;
                type = type.elementType();
                peeling = true;
            } else {
                peeling = false;
            }
        }
        return toType;
    }

    protected TypeSpec peelWrapperTo(TypeSpec type, TypeSpec toType) {
        while (type instanceof WrapperTypeSpec) {
            toType = type;
            type = ((WrapperTypeSpec) type).wrappedType;
        }
        return toType;
    }

    public TypeSpec peelDimensionsTo(TypeSpec type, TypeSpec toType, TapList<ArrayDim> toDimensions) {
        boolean peeling = true;
        while (peeling) {
            if (type == null) {
                peeling = false;
            } else if (type instanceof ArrayTypeSpec) {
                ArrayDim[] dims = ((ArrayTypeSpec) type).dimensions();
                for (ArrayDim dim : dims) {
                    toDimensions = toDimensions.placdl(dim);
                }
                toType = type;
                type = type.elementType();
                peeling = true;
            } else if (type instanceof WrapperTypeSpec) {
                toType = type;
                type = ((WrapperTypeSpec) type).wrappedType;
                peeling = true;
            } else if (type instanceof ModifiedTypeSpec && type.containsArray()) {
                toType = type;
                type = type.elementType();
                peeling = true;
            } else {
                peeling = false;
            }
        }
        return toType;
    }

    // ==================== SECTION ON VARIOUS TYPE COMPARISONS =======================
    // J'ai remplace tous les canReceive(), isLargerThan(), isAlmostEqualForInterface(), equalType()
    // Il faut encore  remplacer tous les sameTypes(), canCompare(), canCombine(), etc...

    public boolean containsArray() {
        if (this instanceof ArrayTypeSpec) {
            return true;
        } else if (this instanceof PointerTypeSpec
                || this instanceof ReferenceTypeSpec
                || this instanceof FunctionTypeSpec) {
            return false;
        } else {
            TypeSpec inside = wrappedType();
            return inside != null && inside.containsArray();
        }
    }

    /**
     * @param toModifiedTypeSpec when given non-null, contains upon exit the deepest ModifiedTypeSpec found.
     */
    protected TypeSpec peelSizeModifiersTo(TypeSpec type, TypeSpec toType, ToObject<Tree> toModifierTree,
                                           ToObject<ModifiedTypeSpec> toModifiedTypeSpec, boolean remainLitteral) {
        boolean peeling = true;
        while (peeling) {
            if (type == null) {
                peeling = false;
            } else if (type instanceof WrapperTypeSpec) {
                toType = type;
                type = ((WrapperTypeSpec) type).wrappedType;
                peeling = true;
            } else if (type instanceof ModifiedTypeSpec) {
                if (toModifiedTypeSpec != null) {
                    toModifiedTypeSpec.setObj((ModifiedTypeSpec) type);
                }
                if (!type.hasUndefinedSize()) {
                    Tree sizeModifierTree =
                            remainLitteral ? ((ModifiedTypeSpec) type).sizeModifier : ((ModifiedTypeSpec) type).sizeModifierResolved;
                    if (sizeModifierTree != null) {
                        toModifierTree.setObj(sizeModifierTree);
                    }
                }
                toType = type;
                type = type.elementType();
                peeling = true;
            } else {
                peeling = false;
            }
        }
        return toType;
    }

    /**
     * @return wrappedType or elementType or destinationType
     * or returnType.
     */
    public TypeSpec wrappedType() {
        return null;
    }

    public void setWrappedType(TypeSpec type) {
    }

    /**
     * @return true when this type is the C predefined type FILE or IO_FILE.
     */
    protected boolean isAnIOTypeSpec(SymbolTable symbolTable) {
        // Default return false.
        return false;
    }

    /**
     * @return a copy of this type with "undefinedNumeric" in its
     * numeric leaf type set to the given boolean "undefined".
     */
    protected TypeSpec cloneAsUndefinedNumeric(boolean undefined) {
        // Default return null, Print error message ? or go abstract ?
        return null;
    }

    /**
     * @return the nested type, or null when there is no meaningful nested type.
     */
    public TypeSpec nestedLevelType() {
        // Default return null, meaning that there is no type nested inside
        return null;
    }

    /**
     * Assuming that this type is a pointer type,
     *
     * @return the inside pointed type.
     */
    protected TypeSpec peelPointer() {
        return this;
    }

    /**
     * @return a name for the base type of this type, when this makes sense.
     */
    abstract protected String baseTypeName();

    /**
     * @param stopOnPointer when false, will also go down into pointers destination type
     * @return the base type found.
     */
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        // Default return null. Print error message ? or go abstract ?
        return null;
    }

    /**
     * @return the ModifiedTypeSpec around the base of this type.
     * If no ModifiedTypeSpec around, returns the same as baseTypeSpec(false).
     */
    public WrapperTypeSpec modifiedBaseTypeSpec() {
        // Default return null. Print error message ? or go abstract ?
        return null;
    }

    /**
     * @return true if this type is a pointer or at least contains a pointer field.
     */
    public boolean containsAPointer() {
        // Default return false.
        return false;
    }

    /**
     * @return the list of all dimensions of this type and its nested types,
     * in the C-like order i.e. outside/slow dimensions first.
     * Doesn't go into pointers nor into function or composite types.
     */
    public TapList<ArrayDim> getAllDimensions() {
        // Default return null.
        return null;
    }

    /**
     * @return true when this type has at least one dimension which is not known explicitly.
     */
    public boolean containsUnknownDimension() {
        // Default return false.
        return false;
    }

    /**
     * @return the type of the elements of the deepest array type in this type.
     */
    public WrapperTypeSpec elementType() {
        return null;
    }

    /**
     * Updates this type with the full information available from the given "symbolTable"
     * and from the other symbol tables that it imports. Example: a type may use a variable
     * name (for size, dimension...) that was not known before all symbols from USE'd Modules
     * have been imported.
     */
    public void updateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        storedSize = UNKNOWNSIZE;
        if (!TapList.contains(dejaVu, this)) {
            doUpdateAfterImports(symbolTable, new TapList<>(this, dejaVu));
        }
    }

    /**
     * Utility for updateAfterImports(). Don't use otherwise!
     */
    protected void doUpdateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        // Default do nothing. Print error message ? or go abstract ?
    }

    /**
     * @return (and stores for later reference) the memory size in bytes occupied by objects of this type.
     */
    public int size() {
        if (storedSize == BEINGSIZED) {
            TapEnv.fileWarning(TapEnv.MSG_WARN, position, "(TC46) Illegal recursive type definition");
            storedSize = ILLEGALTYPESIZE;
        } else if (storedSize == UNKNOWNSIZE) {
            storedSize = BEINGSIZED;
            storedSize = this.computeSize();
        }
        return storedSize;
    }

    /**
     * Utility for size(). Use size() instead.
     */
    protected int computeSize() {
        // Default return 1. Print error message ? or go abstract ?
        return 1;
    }

    /**
     * @return true if this type is a function type.
     */
    public boolean isFunction() {
        return false;
    }

    /**
     * @return true if this type is a character type.
     */
    public boolean isCharacter() {
        return false;
    }

    /**
     * @return true if this type is a string type, i.e. a one-dimensional array of characters.
     */
    public boolean isString() {
        return false;
    }

    /**
     * @return true if this type is a pointer type.
     */
    public boolean isPointer() {
        return false;
    }

    public boolean isPointerToFunction() {
        if (isPointer()) {
            TypeSpec pointedType = peelPointer() ;
            if (pointedType!=null && pointedType.isFunction()) {
                return true ;
            }
        }
        return false ;
    }

    /**
     * @return true if this type is declared as a pointer "target".
     */
    public boolean isTarget() {
        return false;
    }

    /**
     * @return true if this is an array type.
     */
    public boolean isArray() {
        return false;
    }

    /**
     * @return true if this is a plain named type, i.e. a placeholder for
     * a type that should be defined elsewhere, later during building the representation.
     */
    public boolean isNamedType() {
        return false;
    }

    /**
     * Augments the given (hatted) list dependsOn with the SymbolDecl's of all symbols used in this type.
     */
    public void addUsedSymbolsInType(TapList<SymbolDecl> dependsOn, SymbolTable symbolTable) {
        // Default does nothing. Print error message ? or go abstract ?
    }

    /**
     * Collects into toUsedTrees all the symbol Trees used in this type.
     */
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        // Default collects nothing. Print error message ? or go abstract ?
    }

    /**
     * Test that this type can receive a value of the other type, performing type inference if needed and possible,
     * and allowing array operations in the style of F90.
     */
    public boolean receives(TypeSpec other, TypeSpec toThis, TypeSpec toOther) {
        return comparesWith(other, RECVVECTINFRCPDP, toThis, toOther, null);
    }

    /**
     * @return true if this type can receive a value of the other type, without trying type inference,
     * neglecting mismatches of array length and allowing array operations in the style of F90.
     */
    public boolean receivesVectorNeglectSizes(TypeSpec other) {
        return comparesWith(other, RECVVECTNOLNNOSZ, null, null, null);
    }

    /**
     * @return true it this type can receive a value of the other type, performing type inference if needed and possible,
     * but without allowing array operations in the style of F90.
     */
    public boolean receivesNoVector(TypeSpec other, TypeSpec toThis, TypeSpec toOther) {
        return comparesWith(other, RECVooooINFRCPDP, toThis, toOther, null);
    }

    /**
     * @return true it this type can receive a value of the other type, performing type inference if needed and possible,
     * and not caring about different type precision at the leaf primitive types,
     * but without allowing array operations in the style of F90.
     */
    public boolean receivesNoVectorNeglectSizes(TypeSpec other, TypeSpec toThis, TypeSpec toOther) {
        return comparesWith(other, RECVooooINFRNOSZ, toThis, toOther, null);
    }

    /**
     * @return true if this type can receive a value of the other type, but without allowing type inference
     * and without allowing F90-style array notation.
     */
    public boolean receivesNoInferenceNoVector(TypeSpec other) {
        return comparesWith(other, RECVooooooooCPDP, null, null, null);
    }

    /**
     * @return true if this type is equal to the other,
     * in a laxist way that compares the number of dimensions but not their lengthes
     * and additionally accepts vectorial notation (i.e. array:this vs scalar:other),
     * and not caring about different type precision at the leaf primitive types.
     */
    public boolean equalsNeglectSizesNeglectLengthes(TypeSpec other) {
        return comparesWith(other, ooooNOLNooooNOSZ, null, null, null);
    }

    /**
     * @return true if this type is equal to the other,
     * in a laxist way that compares the number of dimensions but not their lengthes,
     * and additionally accepts vectorial notation (i.e. array:this vs scalar:other),
     * and in the "compile-dep" way regarding primitive type sizes.
     */
    public boolean equalsCompilDepNeglectLengthes(TypeSpec other) {
        return comparesWith(other, ooooNOLNooooCPDP, null, null, null);
    }

    /**
     * @return true if this type is equal to the other, in a very laxist way since
     * we don't care about different type precision at the leaf primitive types,
     * and additionally accept vectorial notation (i.e. array:this vs scalar:other).
     */
    public boolean equalsNeglectSizes(TypeSpec other) {
        return comparesWith(other, ooooooooooooNOSZ, null, null, null);
    }

    /**
     * @return true if this type is equal to the other, in a rather laxist way since
     * this uses knowledge about the specific compiler/platform default choices.
     */
    public boolean equalsCompilDep(TypeSpec other) {
        return comparesWith(other, ooooooooooooCPDP, null, null, null);
    }

    /**
     * @return true if this type is equal to the other, in a rather strict way since
     * equality must remain whatever the specific compiler/platform default choices.
     */
    public boolean equalsCompilIndep(TypeSpec other) {
        return comparesWith(other, ooooooooooooCPID, null, null, null);
    }

    /**
     * @return true if this type is litterally equal to the other.
     * When dimensions are given after a dimension of some reference array (ArrayDim.refArrayTree),
     * equality is true iff the reference array and dimension are the same.
     */
    public boolean equalsLiterally(TypeSpec other) {
        return comparesWith(other, ooooooooooooEQEQ, null, null, null);
    }

    /**
     * Perform "comparison" of this type with an "other" type, with possible type inference by side-effect.
     *
     * @param other      The other type, with respect to which comparison is made.
     * @param comparison the comparison performed. See possible comparisons in the comments around showComparison()
     *                   If comparison allows for type inference, then the types wrapped immediately around this and other should be provided in toThis and toOther,
     *                   otherwise type inference will not be done at the top level of types.
     * @param toThis     the TypeSpec immediately containing this type. Useful only if type inference is requested, otherwise may be null.
     * @param toOther    the TypeSpec immediately containing the other type. Useful only if type inference is requested, otherwise may be null.
     * @param dejaVu     the list of type pairs already being compared as "this" vs. "other", and being the destination of some pointer type.
     *                   This classically avoids infinite loops in cycles of types.
     * @return true if this type compares well according to "comparison" with the "other" type.
     */
    public boolean comparesWith(TypeSpec other, int comparison,
                                TypeSpec toThis, TypeSpec toOther,
                                TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace("Comparing types " + this + " " + showComparison(comparison) + " " + other);
            ++compare_debug_indent;
        }
        boolean result = testComparesWith(other, comparison, toThis, toOther, dejaVu);
        if (TapEnv.traceTypeCheckAnalysis()) {
            --compare_debug_indent;
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            for (int i = compare_debug_indent; i > 0; --i) {
                TapEnv.printOnTrace("| ");
            }
            TapEnv.printlnOnTrace(">Compared types " + this + " " + showComparison(comparison) + " " + other + " RETURNS:" + result);
        }
        return result;
    }

    /**
     * Really does the work of comparison at the current type level of "this".
     */
    protected abstract boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                                TapList<TapPair<TypeSpec, TypeSpec>> dejaVu);

    /**
     * @return a copy of this type where some details are left unspecified because
     * type inference is unable to determine them for sure (for this sort of comparison).
     */
    protected TypeSpec weakenForInference(int comparison) {
        // Default returns this type unmodified.
        return this;
    }

    /**
     * Make sure that this type shares the activity information of the given reference type.
     * This is needed e.g when actual type and formal type of a procedure call are two a priori
     * different types (of course such that formalType can "receives" actualType), and they both
     * contain structs: the actual type must share the field activity information of the formal.
     */
    protected void shareActiveFields(TypeSpec referenceType) {
        WrapperTypeSpec referenceTypeBasis = referenceType.baseTypeSpec(false);
        WrapperTypeSpec thisTypeBasis = this.baseTypeSpec(false);
        if (isA(referenceTypeBasis, SymbolTableConstants.COMPOSITETYPE)
                && isA(thisTypeBasis, SymbolTableConstants.COMPOSITETYPE))
        // Share active fields info:
        {
            ((CompositeTypeSpec) thisTypeBasis.wrappedType).shareActiveFields(
                    (CompositeTypeSpec) referenceTypeBasis.wrappedType);
        }
    }

    /**
     * @return a copy of this type, copying recursively inside the nested types tree structure,
     * but stopping copy (i.e. returning the same objects) for the fields of a CompositeTypeSpec,
     * the sub-types of a FunctionTypeSpec, or for MetaTypeSpec, EnumTypeSpec, NamedTypeSpec, VoidTypeSpec.
     */
    public TypeSpec copy() {
        // Default return null, Print error message ? or go abstract ?
        return null;
    }

    /**
     * @param publishedUnit when non-null, remove from this type all information that relates to
     *                      local data of publishedUnit, i.e. that is not visible from outside publishedUnit
     * @return a copy of this type, copying recursively inside the nested types tree structure,
     * but stopping copy (i.e. returning the same objects) for (WrapperTypeSpec's on) CompositeTypeSpec,
     * the sub-types of a FunctionTypeSpec, or for MetaTypeSpec, EnumTypeSpec, NamedTypeSpec, VoidTypeSpec
     */
    public abstract TypeSpec copyStopOnComposite(Unit publishedUnit);

    /**
     * @return the size in bytes of this numeric type,
     * or -1 for unknown, or -2, -3, or -4 for double,short,or quadruple.
     */
    public int precisionSize() {
        // Default return -1
        return -1;
    }

    /**
     * @return a Tree that is a "zero" of this type, whenever it is defined.
     * For pointer type, this is the null pointer.
     */
    public Tree buildConstantZero() {
        // Default return null. Print error message ? or go abstract ?
        return null;
    }

    /**
     * @return a Tree that is a "one" of this type, whenever it is defined.
     */
    public Tree buildConstantOne() {
        // Default return null. Print error message ? or go abstract ?
        return null;
    }

    /**
     * @param dejaVu List of dejaVu sub-types, to avoid looping. Pass null at call.
     * @return true when some component of this type is an unsolved TypeSpec, because
     * it contains either a MetaTypeSpec or an unknown array (or pointer) dimension.
     */
    protected boolean containsMetaType(TapList<TypeSpec> dejaVu) {
        // Default return false. Print error message ?
        return false;
    }

    /**
     * @param toAlreadyCopied the hatted A-list from each original sub-type to its copy
     *                        plus a boolean which is true iff the original sub-type contains a meta Type.
     * @param containsMeta    contains true upon return iff this type contains a Meta sub-type.
     *                        We assume containsMeta is initialized upon false upon call.
     * @return the copied type.
     */
    protected TypeSpec localize(TapList<TapTriplet<TypeSpec, TypeSpec, Boolean>> toAlreadyCopied, ToBool containsMeta) {
        return this;
    }

    /**
     * Try and refine the dimension information inside this type, by matching it
     * with the given complementType, which has the same structure.
     *
     * @param dejaVu List of dejaVu sub-types, to avoid looping. Pass null at call.
     */
    protected TypeSpec preciseDimensions(TypeSpec complementType,
                                         TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, SymbolTable symbolTable) {
        return this;
    }

    protected boolean hasUndefinedSize() {
        return false;
    }

    protected void setHasUndefinedSize(boolean val) {
    }

    public boolean containsUndefinedType() {
        return false;
    }

    protected TypeSpec addMultiDirDimension(ArrayDim dimension) {
        ArrayDim[] dimensions = new ArrayDim[1];
        dimensions[0] = dimension;
        return new ArrayTypeSpec(new WrapperTypeSpec(this), dimensions);
    }

    /**
     * When this type is non-differentiable (e.g. integer), returns a copied
     * type with integers replaced with reals. Otherwise returns the original type.
     *
     * @param dejaVu List of dejaVu sub-types, to avoid looping. Pass null at call.
     */
    public TypeSpec intToReal(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu,
                              SymbolTable symbolTable) {
        // Default return this. Print error message ? or go abstract ?
        return this;
    }

    protected TypeSpec realToComplex(TapList<TapPair<TypeSpec, TypeSpec>> dejaVu, WrapperTypeSpec complexTypeSpec) {
        return this;
    }

    /**
     * @return the differentiated counterpart of this type, which is assumed differentiable.
     * Returning null means that the differentiated counterpart is exactly the same as this type.
     * The returned type is different from "this" either if
     * the differentiated element type is different, or if
     * dimensions must change, e.g. vector mode, or F90 using
     * dimension "*" which must be replaced by ":" in the diff type.
     */
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        // Default returns null. Print error message ?
        return null;
    }

    /**
     * Only for association by address.
     */
    protected void addDiffTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable) {
        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep
                && symbolTable != null
                && this.isScalar()) {
            NewSymbolHolder diffSH = diffTypeSpec.wrappedType.diffTypeDeclSymbolHolder;
            SymbolTable basisSymbolTable = symbolTable.findAssocAddresTypeDeclSymbolTable();
            referencesDiffSHType(symbolTable, diffTypeSpec, diffSH, basisSymbolTable, null, null);
        }
    }

    /**
     * @return true when the new dimension added by multiDirMode can apply to this typeSpec,
     * instead of at some deeper level e.g. included in composite or pointer types.
     */
    public boolean acceptsMultiDirDimension() {
        return false ;
//         TypeSpec baseTypeSpec = (typeSpec==null ? null : typeSpec.baseTypeSpec(false)) ;
//         // When baseTypeSpec is a primitive type then multiDirMode adds a new dimension to this diffVarRef.
//         // Otherwise, the new dimensions occur inside the various fields of diffVarRef.
//         return baseTypeSpec!=null && baseTypeSpec.isDifferentiablePlainType();
    }

    /**
     * Accumulates into this type and into its sub-types, the given information about which types parts
     * (e.g. named types, record fields..) are used as active, which implies that they must have a
     * differentiated counterpart in the differentiate type of this type.
     */
    protected void cumulActiveParts(TapList diffInfos, SymbolTable symbolTable) {
    }

    /**
     * @param dejaVu TapList of TypeSpec
     * @return true iff there must be a differentiated type for this type, different from this type.
     * For instance a record with all fields passive need no diff type, and a record with all fields
     * completely active, do not need a diff type either because the original type also works as well.
     * Also note that in modes associationByAddress, float types and also records with all fields
     * completely active DO need a diff type, because float type is replaced by
     * a special type aka "aDouble".
     */
    public boolean needsADiffType(TapList<TypeSpec> dejaVu) {
        return false;
    }

    /**
     * @param dejaVu List of dejaVu sub-types, to avoid looping. Pass null at call.
     * @return true if at least one variable with this type has at least one active part.
     * This is stronger than isDifferentiableType()
     * i.e. this.isDifferentiated() implies isDifferentiableType(this).
     */
    public boolean isDifferentiated(TapList<TypeSpec> dejaVu) {
        return false;
    }

    /**
     * Checks that this type is valid, i.e. contains no gaping hole and is not circular.
     */
    protected boolean checkTypeSpecValidity(TapList<TypeSpec> dejaVu) {
        return true;
    }

    /**
     * Utility for generateTree(): finds a short name for this type, if it exists.
     */
    protected String findShortName(TapList<SymbolDecl> dependsOn, TapList<SymbolDecl> shortNames) {
        TypeDecl dependsOnTypeDecl = TypeDecl.findTypeDecl(dependsOn);
        String dependsOnTypeName = dependsOnTypeDecl == null ? null : dependsOnTypeDecl.symbol;
        TapList<String> foundShortNames = null;
        while (shortNames != null) {
            TypeDecl typeDecl = (TypeDecl) shortNames.head;
            if (typeDecl.isATrueSymbolDecl
                    && typeDecl.typeSpec.wrappedType == this
                    && (dependsOnTypeName == null || typeDecl.symbol.equals(dependsOnTypeName)
                    || dependsOnTypeDecl.typeSpec.wrappedType.diffTypeSpec == this)) {
                foundShortNames = new TapList<>(typeDecl.symbol, foundShortNames);
            }
            shortNames = shortNames.tail;
        }
        String shortName = null;
        String notSoShortShortName = null;
        while (foundShortNames != null && shortName == null) {
            shortName = foundShortNames.head;
            if (shortName.startsWith("struct ")
                    || shortName.startsWith("union ")
                    || shortName.startsWith("enum ")) {
                notSoShortShortName = shortName;
                shortName = null;
            }
            foundShortNames = foundShortNames.tail;
        }
        return shortName != null ? shortName : notSoShortShortName;
    }

    /**
     * Generate a Tree for this type.
     *
     * @param symbolTable   The SymbolTable context where the generated Tree will appear.
     * @param dependsOn     ?used during findShortName()?
     * @param shortNames    The list of types that have a short name.
     * @param useShortNames When true, if this type has a short name in "shortNames",
     *                      the generated Tree will simply use that name.
     *                      Set it to false to avoid silly "typedef truc=truc ;".
     * @return A Tree that stands for this type.
     */
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) unspecified type !");
        return ILUtils.build(ILLang.op_ident, "UnknownType");
    }

    /**
     * @return a String that shows this type, with the usual syntax of the current language.
     */
    public String showType() {
        return toString();
    }

    /**
     * Prints in detail the contents of this type, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return "Unspecified WrapperTypeSpec";
    }
}
