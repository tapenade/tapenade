/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * A "void" type, i.e. the type of "no expression".
 */
public final class VoidTypeSpec extends TypeSpec {

    /**
     * Create a new void type.
     */
    public VoidTypeSpec() {
        super(SymbolTableConstants.VOIDTYPE);
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a Void* make little sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testIsReceivesWithInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                // Not sure of the semantics for TYPE_RECEIVES test: conservative answer is true!
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind:
        return other instanceof VoidTypeSpec;
    }

    @Override
    public VoidTypeSpec copy() {
        return this;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public WrapperTypeSpec differentiateTypeSpec(SymbolTable symbolTable, SymbolTable srcSymbolTable, int diffUnitSort, String fSuffix,
                                                 boolean localDecl, boolean multiDirMode,
                                                 ArrayDim multiDirDimensionMax, String hintArrayNameInText,
                                                 String hintArrayNameInIdent, Tree hintArrayNameTree, Tree nameTree) {
        return TapEnv.associationByAddress() ? new WrapperTypeSpec(this) : null;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        return ILUtils.build(ILLang.op_void);
    }

    @Override
    public String showType() {
        return "void";
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return "void";
    }
}
