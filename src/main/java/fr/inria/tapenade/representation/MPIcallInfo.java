/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * The collection of all info about a particular Message-Passing call,
 * needed to analyze it and to differentiate it more easily later.
 */
public final class MPIcallInfo {

    private final String funcName;

    /**
     * The full name of this message-passing call, capitalized the way it is used.
     */
    public String funcName() {
        return funcName;
    }

    private boolean onDifferentiableType;

    /**
     * When false, indicates that this message-passing call deals with values of non-differentiable type,
     * and therefore must not be differentiated.
     */
    public boolean isOnDifferentiableType() {
        return onDifferentiableType;
    }

    /**
     * The complete call tree of this message-passing call.
     */
    private final Tree callTree;

    /**
     * The block that contains this message-passing call.
     */
    private final Block block;

    // maybe this should rather be set at creation time, directly from the funcName.
    /**
     * The channel attached to this point-to-point message-passing call,
     * found in an annotation, probably given by an AD directive.
     * The annotation is: $AD CHANNEL "channel name".
     */
    private Tree channel;

    /**
     * The argument that contains the origin or origin-root process (when applicable).
     */
    private final Tree orig;

    /**
     * The argument that contains the destination or destination-root process (when applicable).
     */
    private final Tree dest;

    /**
     * The argument that contains the tag of this message-passing call (when applicable).
     */
    private final Tree tag;

    /**
     * The argument that contains the communicator of this message-passing call (when applicable).
     */
    private final Tree comm;

    /**
     * The ranks of the arguments that contain a buffer (i.e. a communicated value) which is read (when applicable).
     */
    private final TapIntList readBufferRks;
    /**
     * The ranks of the arguments that contain a buffer (i.e. a communicated value) which is written (when applicable).
     */
    private final TapIntList writtenBufferRks;
    /**
     * The ranks of the arguments that contain the MPI type of a buffer, written or read.
     */
    private final TapIntList bufferTypeRks;
    /**
     * The rank of the argument that contains the reduction operation (when applicable).
     */
    private final int reduceOpRk;

    /**
     * Creates an Message-Passing-Call info, and precomputes whether it runs on values of differentiable type or not.
     */
    private MPIcallInfo(String funcName, Tree callTree, Tree channel, Block block,
                        Tree orig, Tree dest, Tree tag, Tree comm,
                        TapIntList readBufferRks, TapIntList writtenBufferRks, TapIntList bufferTypeRks, int reduceOpRk) {
        super();
        this.funcName = funcName;
        this.callTree = callTree;
        this.block = block;
        this.channel = channel;
        this.orig = orig;
        this.dest = dest;
        this.tag = tag;
        this.comm = comm;
        this.readBufferRks = readBufferRks;
        this.writtenBufferRks = writtenBufferRks;
        this.bufferTypeRks = bufferTypeRks;
        this.reduceOpRk = reduceOpRk;
        this.onDifferentiableType = true;

        TapIntList bufferRks = TapIntList.quickUnion(readBufferRks, writtenBufferRks);
        Tree bufferArg;
        Tree bufferTypeArg;
        String typeName;
        TypeSpec argType;
        while (bufferRks != null && onDifferentiableType) {
            bufferArg = ILUtils.getArguments(callTree).down(bufferRks.head);
            argType = block.symbolTable.typeOf(bufferArg);
            if (argType != null && !TypeSpec.isDifferentiableType(argType)) {
                onDifferentiableType = false;
            }
            bufferRks = bufferRks.tail;
        }
        while (bufferTypeRks != null && onDifferentiableType) {
            bufferTypeArg = ILUtils.getArguments(callTree).down(bufferTypeRks.head);
            if (bufferTypeArg.opCode() == ILLang.op_ident) {
                typeName = bufferTypeArg.stringValue().toLowerCase();
                if (typeName.startsWith("mpi_char")
                        || typeName.equals("mpi_byte")
                        || typeName.equals("mpi_logical")
                        || typeName.equals("mpi_short")
                        || typeName.startsWith("mpi_int")
                        || typeName.equals("mpi_long")
                        || typeName.equals("mpi_long_long_int")
                        || typeName.startsWith("mpi_unsigned")
                ) {
                    onDifferentiableType = false;
                }
            }
            bufferTypeRks = bufferTypeRks.tail;
        }
    }

    /**
     * @return the Message-Passing-Call info according to the MPI primitive called.
     */
    private static MPIcallInfo buildMessagePassingMPIcallInfo(String funcName, Tree callTree, Tree channel,
                                                              Block block) {
        String lcFuncName = funcName.toLowerCase();
        Tree args = ILUtils.getArguments(callTree);
        if (lcFuncName.equals("mpi_gather") || lcFuncName.equals("ampi_gather")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, args.down(7), null, args.down(8),
                    new TapIntList(1, null), new TapIntList(4, null), new TapIntList(3, new TapIntList(6, null)), -1);
        } else if (lcFuncName.equals("mpi_scatter") || lcFuncName.equals("ampi_scatter")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    args.down(7), null, null, args.down(8),
                    new TapIntList(1, null), new TapIntList(4, null), new TapIntList(3, new TapIntList(6, null)), -1);
        } else if (lcFuncName.equals("mpi_allgather") || lcFuncName.equals("ampi_allgather")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, null, null, args.down(7),
                    new TapIntList(1, null), new TapIntList(4, null), new TapIntList(3, new TapIntList(6, null)), -1);
        } else if (lcFuncName.equals("mpi_gatherv") || lcFuncName.equals("ampi_gatherv")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, args.down(8), null, args.down(9),
                    new TapIntList(1, null), new TapIntList(4, null), new TapIntList(3, new TapIntList(7, null)), -1);
        } else if (lcFuncName.equals("mpi_scatterv") || lcFuncName.equals("ampi_scatterv")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    args.down(8), null, null, args.down(9),
                    new TapIntList(1, null), new TapIntList(5, null), new TapIntList(4, new TapIntList(7, null)), -1);
        } else if (lcFuncName.equals("mpi_allgatherv") || lcFuncName.equals("ampi_allgatherv")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, null, null, args.down(8),
                    new TapIntList(1, null), new TapIntList(4, null), new TapIntList(3, new TapIntList(7, null)), -1);
        } else if (lcFuncName.equals("mpi_bcast") || lcFuncName.equals("ampi_bcast")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    args.down(4), null, null, args.down(5),
                    new TapIntList(1, null), new TapIntList(1, null), new TapIntList(3, null), -1);
        } else if (lcFuncName.equals("mpi_reduce") || lcFuncName.equals("ampi_reduce")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, args.down(6), null, args.down(7),
                    new TapIntList(1, null), new TapIntList(2, null), new TapIntList(4, null), 5);
        } else if (lcFuncName.equals("mpi_allreduce") || lcFuncName.equals("ampi_allreduce")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, null, null, args.down(6),
                    new TapIntList(1, null), new TapIntList(2, null), new TapIntList(4, null), 5);
        } else if (lcFuncName.equals("mpi_wait") || lcFuncName.equals("ampi_wait")) {
            return new MPIcallInfo(funcName, callTree, ILUtils.build(ILLang.op_none), block,
                    null, null, null, null, null, null, null, -1);
        } else if (lcFuncName.equals("mpi_send") || lcFuncName.equals("mpi_isend")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, args.down(4), args.down(5), args.down(6),
                    new TapIntList(1, null), null, new TapIntList(3, null), -1);
        } else if (lcFuncName.equals("ampi_send") || lcFuncName.equals("ampi_isend")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, args.down(4), args.down(5), args.down(7),
                    new TapIntList(1, null), null, new TapIntList(3, null), -1);
        } else if (lcFuncName.equals("mpi_recv") || lcFuncName.equals("mpi_irecv")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    args.down(4), null, args.down(5), args.down(6),
                    null, new TapIntList(1, null), new TapIntList(3, null), -1);
        } else if (lcFuncName.equals("ampi_recv") || lcFuncName.equals("ampi_irecv")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    args.down(4), null, args.down(5), args.down(7),
                    null, new TapIntList(1, null), new TapIntList(3, null), -1);
        } else if (lcFuncName.equals("mpi_comm_dup") || lcFuncName.equals("ampi_comm_dup")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, null, null, args.down(1), null, null, null, -1);
        } else if (lcFuncName.equals("mpi_comm_split") || lcFuncName.equals("ampi_comm_split")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, null, null, args.down(1), null, null, null, -1);
        } else if (lcFuncName.equals("mpi_comm_create") || lcFuncName.equals("ampi_comm_create")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, null, null, args.down(1), null, null, null, -1);
        } else if (lcFuncName.equals("mpi_comm_free") || lcFuncName.equals("ampi_comm_free")) {
            return new MPIcallInfo(funcName, callTree, channel, block,
                    null, null, null, args.down(1), null, null, null, -1);
        } else if (lcFuncName.equals("mpi_init") || lcFuncName.equals("ampi_init_nt") ||
                lcFuncName.equals("mpi_comm_group") || lcFuncName.equals("ampi_comm_group") ||
                lcFuncName.equals("mpi_comm_spawn") || lcFuncName.equals("ampi_comm_spawn") ||
                lcFuncName.equals("mpi_comm_rank") || lcFuncName.equals("ampi_comm_rank") ||
                lcFuncName.equals("mpi_comm_size") || lcFuncName.equals("ampi_comm_size") ||
                lcFuncName.equals("mpi_finalize") || lcFuncName.equals("ampi_finalize_nt") ||
                lcFuncName.equals("mpi_barrier") || lcFuncName.equals("mpi_waitall")) {
            return null;
        } else {
            TapEnv.toolError("(Build MP call info) Unexpected MPI primitive " + funcName + " in " + callTree);
            return null;
        }
    }

    /**
     * Get (or create) the Message-Passing info attached to the particular call "callTree".
     *
     * @return null when callTree is not and MPI call
     * or is an MPI call irrelevant to AD (e.g. mpi_init, mpi_finalize,...).
     */
    public static MPIcallInfo getMessagePassingMPIcallInfo(String funcName, Tree callTree,
                                                           int language, Block block) {
        MPIcallInfo result = callTree.getAnnotation("MPIcallInfo");
        Tree channelTree = callTree.getAnnotation("channel");
        if (result == null && isMessagePassingFunction(funcName, language)) {
            result = buildMessagePassingMPIcallInfo(funcName, callTree, channelTree, block);
            if (result != null) {
                callTree.setAnnotation("MPIcallInfo", result);
            }
        }
        // L'analyse des directives est faite apres le typeCheck, donc on complete
        // apres creation le champ "channel" qui est donne par la directive:
        // (Si on inverse l'ordre (directive puis typeCheck) il faudra adapter ce code)
        if (result != null && result.channel == null && channelTree != null) {
            result.channel = channelTree;
        }
        return result;
    }

    /**
     * @param funcName MPI or AMPI function name.
     * @param language current language.
     * @return true for a Message Passing call.
     */
    public static boolean isMessagePassingFunction(String funcName, int language) {
        boolean caseIndep = TapEnv.isFortran(language) || language == TapEnv.UNDEFINED;
        if (caseIndep) {
            funcName = funcName.toLowerCase();
            return funcName.startsWith("mpi_") || funcName.startsWith("ampi_");
        } else {
            return funcName.startsWith("MPI_") || funcName.startsWith("AMPI_");
        }
    }

    protected static void checkMessagePassingCalls(CallGraph callGraph) {
        TapList<TapPair<MPIcallInfo, TapIntList>> allMPChannels = callGraph.getAllMessagePassingChannels();
        MPIcallInfo channelInfo;
        while (allMPChannels != null) {
            channelInfo = allMPChannels.head.first;
            channelInfo.channel = channelInfo.callTree.getAnnotation("channel");
            allMPChannels = allMPChannels.tail;
        }
    }

    protected static int checkNumberOfChannels(CallGraph callGraph) {
        int result = 0;
        String channelName;
        TapList<String> channels = null;
        Tree directive;
        TapList<TapPair<MPIcallInfo, TapIntList>> allChannels = callGraph.getAllMessagePassingChannels();
        TapList<TapPair<MPIcallInfo, TapIntList>> hdCleanedChannels = new TapList<>(null, null);
        TapList<TapPair<MPIcallInfo, TapIntList>> toCleanedChannels = hdCleanedChannels;
        while (allChannels != null) {
            directive = allChannels.head.first.channel;
            boolean keep = true;
            if (directive != null) {
                channelName = directive.stringValue();
                if (!TapList.containsEquals(channels, channelName)) {
                    channels = new TapList<>(channelName, channels);
                } else {
                    keep = false;
                }
            }
            if (keep) {
                toCleanedChannels = toCleanedChannels.placdl(allChannels.head);
                result = result + 1;
            }
            allChannels = allChannels.tail;
        }
        callGraph.setAllMessagePassingChannels(hdCleanedChannels.tail);
        return result;
    }

    /**
     * NOT_YET_IMPLEMENTED !!! When possible, replace a Tree which has been returned by the C
     * preprocessor, e.g. (MPI_Comm)0x44000000, with a reference to the MPI
     * defined name it had before, e.g. MPI_COMM_WORLD.
     */
    public static Tree replaceMPIconstantsIfPossible(Tree preprocessedTree) {
        // Not yet implemented, and honestly I'm not sure how this should be done...
        // It's done now in TreeGen.java, in a somewhat ad-hoc way...
        return preprocessedTree;
    }

    /**
     * @return True when this is a send or recv i.e. one end of a point-to-point communication.
     */
    public boolean isPointToPoint() {
        return funcName != null && (funcName.toLowerCase().endsWith("send") ||
                funcName.toLowerCase().endsWith("recv"));
    }

    public boolean argumentIsRead(int argumentRank) {
        return TapIntList.contains(readBufferRks, argumentRank);
    }

    public Tree sentExprToChannel() {
        return readBufferRks != null && isPointToPoint() ? ILUtils.getArguments(callTree).down(readBufferRks.head) : null;
    }

    public Tree receivedExprFromChannel() {
        return writtenBufferRks != null && isPointToPoint() ? ILUtils.getArguments(callTree).down(writtenBufferRks.head) : null;
    }

    public boolean argumentIsABuffer(int argumentRank) {
        return TapIntList.contains(readBufferRks, argumentRank) || TapIntList.contains(writtenBufferRks, argumentRank);
    }

    public boolean argumentIsAType(int argumentRank) {
        return TapIntList.contains(bufferTypeRks, argumentRank);
    }

    public boolean argumentIsAReduceOp(int argumentRank) {
        return reduceOpRk == argumentRank;
    }

    public boolean isReduceCall() {
        return reduceOpRk != -1;
    }

    public boolean isNonBlocking() {
        if (funcName == null) {
            return false;
        }
        String lcFuncName = funcName.toLowerCase();
        return lcFuncName.endsWith("mpi_isend") ||
                lcFuncName.endsWith("mpi_irecv") ||
                lcFuncName.endsWith("mpi_wait");
    }

    public boolean isCommFree() {
        return funcName != null && funcName.toLowerCase().endsWith("mpi_comm_free");
    }

    public boolean isCommCreation() {
        if (funcName == null) {
            return false;
        }
        String lcFuncName = funcName.toLowerCase();
        return lcFuncName.endsWith("mpi_comm_dup") ||
                lcFuncName.endsWith("mpi_comm_split") ||
                lcFuncName.endsWith("mpi_comm_create");
    }

    public void registerMessagePassingChannel(CallGraph callGraph) {
        TapPair<MPIcallInfo, TapIntList> curChannel = this.findMessagePassingChannel(callGraph);
        if (curChannel == null && this.isPointToPoint()) {
            curChannel = new TapPair<>(this, null);
            callGraph.setAllMessagePassingChannels(new TapList<>
                    (curChannel, callGraph.getAllMessagePassingChannels()));
        }
    }

    public TapPair<MPIcallInfo, TapIntList> findMessagePassingChannel(CallGraph callGraph) {
        TapList<TapPair<MPIcallInfo, TapIntList>> inAllChannels = callGraph.getAllMessagePassingChannels();
        TapPair<MPIcallInfo, TapIntList> result = null;
        while (result == null && inAllChannels != null) {
            if (this.equalMessagePassingChannel(inAllChannels.head.first)) {
                result = inAllChannels.head;
            }
            inAllChannels = inAllChannels.tail;
        }
        return result;
    }

    public TapIntList findMessagePassingChannelZones(CallGraph callGraph) {
        TapIntList result = null;
        if (isPointToPoint()) {
            TapPair<MPIcallInfo, TapIntList> channelTapPair = findMessagePassingChannel(callGraph);
            if (channelTapPair != null) {
                result = channelTapPair.second;
            }
        }
        return result;
    }

    // equalMessagePassingChannel est false si les directives sont toutes les deux non null et sont differentes
    // ou si les tags sont toutes les deux des constantes entieres et sont differentes
    // dans tous les autres cas, on considere que c'est le meme channel
    // TODO traiter MPI_ANY_TAG
    private boolean equalMessagePassingChannel(MPIcallInfo channelInfo) {
        boolean result = true;
        if (this.comm != null && channelInfo.comm != null) {
            result = equalDirectiveOrCommunicatorOrTag(this.comm, channelInfo.comm);
        }
        if (result) {
            boolean equalDirective = true;
            boolean equalConstanteTag = true;
            if (this.channel != null && channelInfo.channel != null) {
                equalDirective = equalDirectiveOrCommunicatorOrTag(this.channel, channelInfo.channel);
            }
            if (this.block.symbolTable == channelInfo.block.symbolTable) {
                VariableDecl varTag1;
                VariableDecl varTag2;
                Integer tag1 = null;
                Integer tag2 = null;
                //[llh 21/01/2013] TODO pour comparer this.tag et channelInfo.tag(), il vaudrait
                // mieux appeler une bonne implementation (a ecrire) de ILUtils.equalValue() :
                if (this.tag != null) {
                    if (this.tag.opCode() == ILLang.op_intCst) {
                        tag1 = this.tag.intValue();
                    } else if (ILUtils.baseName(this.tag) != null) {
                        varTag1 = this.block.symbolTable.getConstantDecl(ILUtils.baseName(this.tag));
                        if (varTag1 != null) {
                            tag1 = varTag1.constantValue;
                        }
                    } else {
                        TapEnv.toolWarning(-1, "Analysis cannot finely compare tag " + this.tag);
                    }
                }
                if (channelInfo.tag != null) {
                    if (channelInfo.tag.opCode() == ILLang.op_intCst) {
                        tag2 = channelInfo.tag.intValue();
                    } else if (ILUtils.baseName(channelInfo.tag) != null) {
                        varTag2 = this.block.symbolTable.getConstantDecl(ILUtils.baseName(channelInfo.tag));
                        if (varTag2 != null) {
                            tag2 = varTag2.constantValue;
                        }
                    } else {
                        TapEnv.toolWarning(-1, "Analysis cannot finely compare tag " + channelInfo.tag);
                    }
                }
                if (tag1 != null && tag2 != null) {
                    equalConstanteTag = tag1.intValue() == tag2.intValue();
                    // les channel sont null lors du typeCheck, elles sont mises a jour avant InOut
                    if (this.channel != null && channelInfo.channel != null
                            && equalDirective != equalConstanteTag) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, channelInfo.callTree, "(MPI01) channel directives " + ILUtils.toString(this.channel) + " " + ILUtils.toString(channelInfo.channel) + " incompatible with tags " + ILUtils.toString(this.tag) + " " + ILUtils.toString(channelInfo.tag));
                    }
                }
            }
            result = equalDirective && equalConstanteTag;
        }
        return result;
    }

    // en fortran ident(mpi_comm_world)
    // en c cast(ident MPI_Comm,bitCst:0x44000000)
    // return true, false ou peut-etre
    private boolean equalDirectiveOrCommunicatorOrTag(Tree tagOrComm1, Tree tagOrComm2) {
        // todo tester meme zone si non constante
        return tagOrComm1.equalsTree(tagOrComm2);
    }

    public static boolean isNonBlockingMPI(Tree callTree, Unit calledUnit, Block block) {
        MPIcallInfo messagePassingInfo =
                MPIcallInfo.getMessagePassingMPIcallInfo(calledUnit.name(), callTree, block.symbolTable.language(), block);
        return messagePassingInfo != null && messagePassingInfo.isNonBlocking();
    }

    @Override
    public String toString() {
        String lcFuncName = funcName.toLowerCase();
        String centralString;
        if (lcFuncName.endsWith("send")) {
            centralString = "sends " + readBufferRks + " to " + dest + " through channel " + channel;
        } else if (lcFuncName.endsWith("recv")) {
            centralString = "receives " + writtenBufferRks + " from " + orig + " through channel " + channel;
        } else if (lcFuncName.startsWith("mpi_gather") || lcFuncName.startsWith("ampi_gather") ||
                lcFuncName.startsWith("mpi_allgather") || lcFuncName.startsWith("ampi_allgather") ||
                lcFuncName.startsWith("mpi_scatter") || lcFuncName.startsWith("ampi_scatter") ||
                lcFuncName.endsWith("bcast") || lcFuncName.endsWith("reduce")) {
            centralString = "global, reads " + readBufferRks + " of " + (orig == null ? "all procs" : "proc " + orig)
                    + ", writes " + writtenBufferRks + " of " + (dest == null ? "all procs" : "proc " + dest);
        } else {
            centralString = "ancillary";
        }
        return "<MPIcallInfo " + funcName + ": " + centralString + (onDifferentiableType ? ">" : " (non-diff Type)>");
    }
}
