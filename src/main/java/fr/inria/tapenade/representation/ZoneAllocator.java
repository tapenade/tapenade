/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Allocates memory zone information.
 */
final class ZoneAllocator {
    public int nextZone;
    public TapList<ZoneInfo> zoneInfos;
    public int nextRealZone;
    public TapList<ZoneInfo> realZoneInfos;
    public int nextIntZone;
    public TapList<ZoneInfo> intZoneInfos;
    public int nextPtrZone;
    public TapList<ZoneInfo> ptrZoneInfos;
    public String commonName;
    public int startOffset;
    public int endOffset;
    public boolean infiniteEndOffset;
    private boolean possiblyMultiple = false ;
    private boolean needsAccessTree = false ;
    private boolean allocateZonesForPointed = false ;
//     public boolean allocateGlobals = false ;

    /** The SymbolTable that will host the table of all ZoneInfo's created by this ZoneAllocator */
    public SymbolTable targetSymbolTable ;

    public ZoneAllocator(SymbolTable targetSymbolTable) {
        super();
        this.targetSymbolTable = targetSymbolTable ;
        if (targetSymbolTable != null) {
            nextZone = targetSymbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND);
            nextRealZone = targetSymbolTable.freeDeclaredZone(SymbolTableConstants.REALKIND);
            nextIntZone = targetSymbolTable.freeDeclaredZone(SymbolTableConstants.INTKIND);
            nextPtrZone = targetSymbolTable.freeDeclaredZone(SymbolTableConstants.PTRKIND);
        } else {
            nextZone = 0;
            nextRealZone = 0;
            nextIntZone = 0;
            nextPtrZone = 0;
        }
        zoneInfos = null;
        realZoneInfos = null;
        intZoneInfos = null;
        ptrZoneInfos = null;
    }

    public void setZoneNumber4(int[] zoneNumber4) {
        nextZone = zoneNumber4[0];
        nextRealZone = zoneNumber4[1];
        nextIntZone = zoneNumber4[2];
        nextPtrZone = zoneNumber4[3];
    }

    public void setCommonName(String commonName) {
        this.commonName = commonName;
    }

    public void setOffsets(int startOffset, int endOffset, boolean infiniteEndOffset) {
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        this.infiniteEndOffset = infiniteEndOffset;
    }

    public void setPossiblyMultiple(boolean possiblyMultiple) {
        this.possiblyMultiple = possiblyMultiple;
    }

    public void setNeedsAccessTree(boolean needsAccessTree) {
        this.needsAccessTree = needsAccessTree;
    }

    /** When set to true, allocation of a zone z1 for pointer p triggers
     * allocation of a zone z2 for *p, so that z1 initial default destination is z2. */
    public void setAllocateZonesForPointed(boolean allocateZonesForPointed) {
        this.allocateZonesForPointed = allocateZonesForPointed ;
    }

    /** When true, a single variable name is not enough to designate one zone, and therefore the name must be shown with indices. */
    public boolean ambiguousWithoutIndices = false ;

    private boolean containsSomeCIOSymbol(TapList<ZoneInfoAccessElements> accesses) {
        boolean contains = false ;
        Tree accessTree ;
        while (accesses!=null && !contains) {
            accessTree = accesses.head.accessTree ;
            contains = (accessTree!=null && ILUtils.isACIOSymbol(accessTree)) ;
            accesses = accesses.tail ;
        }
        return contains ;
    }

    private boolean thisIsIO(WrapperTypeSpec accessType, TapList<ZoneInfoAccessElements> accesses, SymbolTable declSymbolTable) {
        if (declSymbolTable==null && accesses!=null) {
            declSymbolTable = accesses.head.symbolTable ;
        }
        return (declSymbolTable!=null
                && TapEnv.isC(declSymbolTable.language())
                && (accessType.isAnIOTypeSpec(declSymbolTable) || containsSomeCIOSymbol(accesses))) ;
    }

    /**
     * Allocates a new set of zones for all objects accessed by the given "accesses",
     * which must all be of the same type "type+typeModifier+declExtraInfo".
     * In general, "accesses" is a singleton, containing only one declared symbol.
     * However they may be several in the case of COMMON, BIND(C), EQUIVALENCE...
     * @param rootType the root type, i.e. the type of the root symbol(s) declared
     * @param accessArgRankOrResultZero when the (singleton) accesses is a formal argument, its rank, and zero for function result.
     * @param isReal true to force the allocated zones below to have the real kind.
     * @param isInt true to force the allocated zones below to have the integer kind.
     * @param isPtr true to force the allocated zones below to have the pointer kind.
     * @param targetZoneOf when non-null, all allocated zones below will be marked as initial destinations of this pointer zone.
     * @param declSymbolTable the SymbolTable of the main declaration location. Only used for a dubious
     *  last attempt to fix size modifiers, probably too late anyway as COMMON alignements are already fixed.
     * @param dejaVu mechanism to avoid infinite looping in recursive types.
     * @return the tree of all zone ranks allocated for the given "accesses".
     */
    public TapList allocateZones(WrapperTypeSpec type, int typeModifier, TapList<String> declExtraInfo,
                                 TapList<ZoneInfoAccessElements> accesses,
                                 WrapperTypeSpec rootType, int accessArgRankOrResultZero, boolean isInt, boolean isReal, boolean isPtr,
                                 ZoneInfo targetZoneOf,
                                 SymbolTable declSymbolTable,
                                 TapList<TapPair<CompositeTypeSpec, TapList>> dejaVu) {
        TapList zones;
        if (type == null || type.wrappedType == null) {
            // Recovery case. Should not happen.
            zones = allocateOneZone(new WrapperTypeSpec(null), typeModifier, accesses,
				    rootType, accessArgRankOrResultZero, isInt, isReal, isPtr,
				    false, targetZoneOf);
        } else if (thisIsIO(type, accesses, declSymbolTable)) {
            // If current language is C and this is some root SymbolTable, or that of a non-standard Unit,
            //  and the type or the variable name indicates that this expression is IO,
            //  then zone(s) are the Tapenade predefined zone for IO: zoneNbOfAllIOStreams.
            zones = new TapList<>(new TapIntList(TapEnv.get().origCallGraph().zoneNbOfAllIOStreams, null), null) ;
        } else {
            switch (type.wrappedType.kind()) {
                case SymbolTableConstants.ARRAYTYPE: {
                    ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) type.wrappedType;
                    TapList<ZoneInfoAccessElements> hdIndexedAccesses = new TapList<>(null, null) ;
                    TapList<ZoneInfoAccessElements> tlIndexedAccesses = hdIndexedAccesses ;
                    while (accesses!=null) {
                        tlIndexedAccesses = tlIndexedAccesses.placdl(accesses.head.buildIndex(arrayTypeSpec)) ;
                        accesses = accesses.tail ;
                    }
                    zones = allocateZones(arrayTypeSpec.elementType(), typeModifier, declExtraInfo,
                                          hdIndexedAccesses.tail,
                                          rootType, accessArgRankOrResultZero, isInt, isReal, isPtr,
                                          targetZoneOf, declSymbolTable, dejaVu) ;
                    break ;
                }
                case SymbolTableConstants.COMPOSITETYPE: {
                    TapList zoneInfosBefore = zoneInfos;
                    CompositeTypeSpec recordTypeSpec = (CompositeTypeSpec) type.wrappedType;
                    zones = TapList.cassq(recordTypeSpec, dejaVu);
                    if (zones != null) {
                        // Set a mark that means this zone tree is recursive:
                        zones.head = Boolean.TRUE;
                    } else {
                        int listLength = recordTypeSpec.fields.length + 1;
                        while (listLength > 0) {
                            zones = new TapList<>(null, zones);
                            listLength--;
                        }
                        dejaVu = new TapList<>(new TapPair<>(recordTypeSpec, zones), dejaVu);
                        TapList inZones = zones.tail;
                        FieldDecl field;
                        for (int i = 0; i < recordTypeSpec.fields.length; i++) {
                            field = recordTypeSpec.fields[i];
                            if (field != null) {
                                TapList<ZoneInfoAccessElements> hdFieldedAccesses = new TapList<>(null, null) ;
                                TapList<ZoneInfoAccessElements> tlFieldedAccesses = hdFieldedAccesses ;
                                TapList<ZoneInfoAccessElements> inAccesses = accesses ;
                                while (inAccesses!=null) {
                                    tlFieldedAccesses = tlFieldedAccesses.placdl(inAccesses.head.buildField(field, i)) ;
                                    inAccesses = inAccesses.tail ;
                                }
                                inZones.head = allocateZones(field.type(), typeModifier, field.extraInfo(),
                                                             hdFieldedAccesses.tail,
                                                             rootType, accessArgRankOrResultZero, isInt, isReal, isPtr,
                                                             targetZoneOf, declSymbolTable, dejaVu) ;
                            }
                            inZones = inZones.tail;
                        }
                        if (zones.head != null) {
                            // If this zone tree was found recursive, mark all zoneInfos as "multiple".
                            TapList zoneInfosNow = zoneInfos;
                            while (zoneInfosNow != zoneInfosBefore) {
                                ((ZoneInfo) zoneInfosNow.head).setMultiple();
                                zoneInfosNow = zoneInfosNow.tail;
                            }
                        }
                    }
                    zones = zones.tail;
                    break ;
                }
                case SymbolTableConstants.MODIFIEDTYPE: {
                    ModifiedTypeSpec modifiedTypeSpec = (ModifiedTypeSpec) type.wrappedType;
                    // Only now are all imported symbol tables ready,
                    // so this is the right time to try and (re-)compute the values of the size modifiers:
                    if (declSymbolTable != null) {
                        modifiedTypeSpec.resolveSizeModifier(declSymbolTable);
                    }
                    int sizeModifier = modifiedTypeSpec.sizeModifierValue();
                    zones = allocateZones(modifiedTypeSpec.elementType(),
                                          (sizeModifier == -1 ? typeModifier : sizeModifier),
                                          declExtraInfo, 
                                          accesses, //warning: accessTree was copied by ILUtils.copy() ! useful ?
                                          rootType, accessArgRankOrResultZero, isInt, isReal, isPtr,
                                          targetZoneOf, declSymbolTable, dejaVu) ;
                    break ;
                }
                case SymbolTableConstants.POINTERTYPE: {
                    zones = allocateOneZone(type, typeModifier, accesses,
                                            rootType, accessArgRankOrResultZero, isInt, isReal, true,
                                            TapList.containsEquals(declExtraInfo, "allocatable"),
                                            targetZoneOf);
// This "allocateZonesForPointed" is a (failed) attempt to *not* declare "initial destination" zones for
// destinations of allocated pointers. It fails because most calls to malloc or allocate are in fact "multiple"
// so that the initial destination has a meaning, which is the destination of the other instances of this malloc
// It is a pity, because in general, it would be much cleaner to say, after a malloc(p), that p simply points to Undef.
// Also, this would reduce the number of zones...
//                     if (allocateZonesForPointed) {
                        // Now allocate zones that will be used by data-flow analyses to represent
                        // the destination of this pointer at any (conventional) initial time:
                        ZoneInfo pointerZone = lastAllocated() ;
                        PointerTypeSpec pointerType = (PointerTypeSpec) type.wrappedType;
                        TapList<ZoneInfoAccessElements> hdDerefedAccesses = new TapList<>(null, null) ;
                        TapList<ZoneInfoAccessElements> tlDerefedAccesses = hdDerefedAccesses ;
                        while (accesses!=null) {
                            tlDerefedAccesses = tlDerefedAccesses.placdl(accesses.head.buildDeref(pointerType)) ;
                            accesses = accesses.tail ;
                        }
                        pointerZone.targetZonesTree = allocateZones(pointerType.destinationType,
                                                                -1, null, hdDerefedAccesses.tail,
                                                                rootType, accessArgRankOrResultZero, false, false, false,
                                                                pointerZone, declSymbolTable, dejaVu) ;
                        //TODO: share targetZonesTree with the proxyZoneInfo's of pointerZone !!
//                     }
                    break ;
                }
                case SymbolTableConstants.VOIDTYPE:
                    zones = allocateOneZone(type, typeModifier, accesses,
                                        rootType, accessArgRankOrResultZero, isInt, true, isPtr,
                                        TapList.containsEquals(declExtraInfo, "allocatable"),
					targetZoneOf);
                    break;
                case SymbolTableConstants.PRIMITIVETYPE:
                case SymbolTableConstants.METATYPE:
                    zones = allocateOneZone(type, typeModifier, accesses,
                                        rootType, accessArgRankOrResultZero, isInt, isReal, isPtr,
                                        TapList.containsEquals(declExtraInfo, "allocatable"),
					targetZoneOf);
                    break;
                case SymbolTableConstants.FUNCTIONTYPE:
                    zones = allocateOneZone(type, typeModifier, accesses,
                                        rootType, accessArgRankOrResultZero, false, false, false,
                                        false, targetZoneOf);
                    break;
                default:
                    zones = null;
            }
        }
        return zones;
    }

    public TapList allocateOneZone(WrapperTypeSpec type, int accessTypeModifier, TapList<ZoneInfoAccessElements> accesses,
                                   WrapperTypeSpec rootType, int accessArgRankOrResultZero,
                                   boolean isInt, boolean isReal, boolean isPtr, 
                                   boolean isAllocatable, ZoneInfo targetZoneOf) {
        if (TypeSpec.isA(type, SymbolTableConstants.METATYPE)) {
            isReal = true;
            isInt = true;
        }
        if (TypeSpec.isA(type, SymbolTableConstants.PRIMITIVETYPE)) {
            String typeName = ((PrimitiveTypeSpec) type.wrappedType).name();
            if (typeName.equals("integer")) {
                isInt = true;
            }
            if (typeName.equals("float") || typeName.equals("complex")) {
                isReal = true;
            }
        }
        if (TypeSpec.isA(type, SymbolTableConstants.POINTERTYPE)) {
            isPtr = true;
        }
        ZoneInfo zoneInfo;
        if (targetZoneOf != null) {
            zoneInfo = new ZoneInfo(-1, -1, false);
        } else {
            zoneInfo = new ZoneInfo(startOffset, endOffset, infiniteEndOffset);
        }
        if (possiblyMultiple) {
            zoneInfo.setMultiple() ;
        }
        if (ambiguousWithoutIndices) {
            zoneInfo.setAmbiguousWithoutIndices() ;
        }
//         if (commonName == null) {
//             zoneInfo.commonName = null;
//         } else {
//             zoneInfo.setKind(SymbolTableConstants.COMMON);
            zoneInfo.commonName = commonName;
//         }
        if (targetSymbolTable.isGlobalSymbolTable() || targetSymbolTable.isTranslationUnitSymbolTable()) {
            zoneInfo.setKind(SymbolTableConstants.GLOBAL);
            zoneInfo.index = nextZone;
        }
        // By default, "system" zones are hidden (i.e. IO, channels, allocation points, COMMON...)
        if (targetSymbolTable.isGlobalSymbolTable()) {
            zoneInfo.isHidden = true ;
        }
        zoneInfo.type = type;
        zoneInfo.rootAccessType = rootType;
        zoneInfo.typeSizeModifier = accessTypeModifier;
        zoneInfo.isAllocatable = isAllocatable;
        if (accessArgRankOrResultZero == 0) {
            zoneInfo.setKind(SymbolTableConstants.RESULT);
            zoneInfo.index = 0 ;
            zoneInfo.isHidden = false ;
        } else if (accessArgRankOrResultZero > 0) {
            zoneInfo.setKind(SymbolTableConstants.PARAMETER);
            zoneInfo.index = accessArgRankOrResultZero;
            zoneInfo.isHidden = false ;
        }
        if (isInt) {
            zoneInfo.intZoneNb = nextIntZone;
            nextIntZone++;
            intZoneInfos = new TapList<>(zoneInfo, intZoneInfos);
        } else {
            zoneInfo.intZoneNb = -1;
        }
        if (isReal) {
            zoneInfo.realZoneNb = nextRealZone;
            nextRealZone++;
            realZoneInfos = new TapList<>(zoneInfo, realZoneInfos);
        } else {
            zoneInfo.realZoneNb = -1;
        }
        if (isPtr) {
            zoneInfo.ptrZoneNb = nextPtrZone;
            nextPtrZone++;
            ptrZoneInfos = new TapList<>(zoneInfo, ptrZoneInfos);
        } else {
            zoneInfo.ptrZoneNb = -1;
        }
        zoneInfo.targetZoneOf = targetZoneOf;
        zoneInfo.zoneNb = nextZone;
        nextZone++;
        // Heuristic to make sure the zone is named after a formal parameter if possible...
        boolean hasUsedFormalParamName = false ;
        while (accesses!=null) {
            ZoneInfoAccessElements oneAccess = accesses.head ;
            SymbolDecl rootSymbol = oneAccess.symbolDecl ;
            // When rootSymbol is a special SubVariableDecl built for BIND(C) with a struct, take the true VariableDecl inside:
            if (rootSymbol instanceof SubVariableDecl) {
                rootSymbol = ((SubVariableDecl)rootSymbol).trueVariableDecl ;
            }
            boolean accessThroughParameter =
                (rootSymbol instanceof VariableDecl && ((VariableDecl)rootSymbol).formalArgRank>=0) ;
            if (targetSymbolTable.isGlobalSymbolTable() && oneAccess.symbolTable!=null && oneAccess.symbolTable.unit!=null) {
                // This is the case of a shared variable access at one of the locations that really uses or imports the shared variable.
                // if targetSymbolTable is a global SymbolTable, then we must prepare for
                // creation of a proxy ZoneInfo entry for each Unit that can see this global:
                ZoneInfo proxyZoneInfo = zoneInfo.copy() ;
//                 if (commonName==null) { // Only if this is not about a COMMON:
//                     // NOTE: we choose here *not* to share activity for a variable in COMMON,
//                     // to preserve the possibility of differentiating the variable locally
//                     // and not inside the COMMON. cf set11/lh019 or set10/lh202.
// // but this causes bugs e.g. set06/v308 set11/lh043 set11/v02 -> TODO?
                    proxyZoneInfo.shareActivity(zoneInfo);
//                 }
                proxyZoneInfo.from = null ; // clear "from"
                proxyZoneInfo.declarationUnit = oneAccess.symbolTable.unit ;
                proxyZoneInfo.isHidden = oneAccess.symbolTable.isGlobalSymbolTable() ;
                proxyZoneInfo.accessTree = oneAccess.accessTree;
                proxyZoneInfo.description = ILUtils.toString(proxyZoneInfo.accessTree, Unit.language(proxyZoneInfo.declarationUnit), ambiguousWithoutIndices) ;
                proxyZoneInfo.accessIndexes = oneAccess.accessIndexes;
                if (rootSymbol!=null) {
                    if (rootSymbol.symbol!=null) proxyZoneInfo.addVariableName(rootSymbol.symbol) ;
                    if (proxyZoneInfo.ownerSymbolDecl==null) proxyZoneInfo.ownerSymbolDecl = rootSymbol ;
                }

                // When possible, update the rootAccessType of this new proxy ZoneInfo,
                //  from the root of its accessTree:
                if (oneAccess.accessTree!=null) {
                    Tree proxyBaseTree = ILUtils.baseTree(oneAccess.accessTree) ;
                    if (proxyBaseTree!=null)
                        proxyZoneInfo.rootAccessType = oneAccess.symbolTable.typeOf(proxyBaseTree) ;
                }

                SymbolTable importsSymbolTable ;
                if (TapEnv.isC(oneAccess.symbolTable.language())) {
                    importsSymbolTable = oneAccess.symbolTable ;
                } else {
                    importsSymbolTable = oneAccess.symbolTable.unit.importsSymbolTable() ;
                }
                if (zoneInfo.targetZoneOf!=null) {
                    proxyZoneInfo.targetZoneOf = importsSymbolTable.retrieveWaitingPtrZoneInfo(zoneInfo.targetZoneOf.ptrZoneNb) ;
                }
                if (importsSymbolTable!=null) {
                    importsSymbolTable.addWaitingZoneInfo(proxyZoneInfo) ;
                }
                if (zoneInfo.accessTree==null || accessThroughParameter) {
                    // This is a heuristic to try and prefer an accessTree that
                    //  refers to a formal param rather than an equivalenced name:
                    zoneInfo.accessTree = oneAccess.accessTree ;
                    zoneInfo.declarationUnit = oneAccess.symbolTable.unit ;
                    zoneInfo.description = ILUtils.toString(zoneInfo.accessTree, Unit.language(zoneInfo.declarationUnit), ambiguousWithoutIndices) ;
                }
                if (commonName!=null) {
                    if (zoneInfo.isRemanentLocal()) {
                        zoneInfo.declarationUnit = proxyZoneInfo.declarationUnit ;
                        zoneInfo.description =
                            ILUtils.toString(zoneInfo.accessTree, Unit.language(zoneInfo.declarationUnit), false)
                            +"[save in "+zoneInfo.declarationUnit.shortName()+"]";
                    } else {
                        zoneInfo.declarationUnit = proxyZoneInfo.declarationUnit ;
                        zoneInfo.description = commonName+"["+startOffset+","+(infiniteEndOffset?"+inf":endOffset)+"[" ;
                    }
                } else if (zoneInfo.accessTree!=null) {
                    if (zoneInfo.comesFromAllocate()) {
                        zoneInfo.declarationUnit = proxyZoneInfo.declarationUnit ;
                        zoneInfo.description =
                            ILUtils.toString(zoneInfo.accessTree, Unit.language(zoneInfo.declarationUnit), false) ;
//                             ILUtils.toString(ILUtils.skipAllocate(zoneInfo.accessTree), Unit.language(zoneInfo.declarationUnit), false)
//                             +"[alloc in "+zoneInfo.declarationUnit.shortName()+"]";
                        // The ZoneInfo of an allocated piece of memory is Hidden, even from the routine that calls alloc:
                        proxyZoneInfo.isHidden = true ;
                        proxyZoneInfo.description = zoneInfo.description ;
                    } else {
                        zoneInfo.declarationUnit = proxyZoneInfo.declarationUnit ;
                        zoneInfo.description = ILUtils.toString(zoneInfo.accessTree, Unit.language(zoneInfo.declarationUnit), ambiguousWithoutIndices) ;
                    }
                }
                if (zoneInfo.ownerSymbolDecl==null) {
                    // When the owner SymbolDecl comes from the "access", copy it on the reference zoneInfo:
                    zoneInfo.ownerSymbolDecl = rootSymbol ;
                }
            } else {
                // This is the case of a basic variable access, or of the "root" definition of the variable,
                // which may be inaccessible except through another access that imports it.
                zoneInfo.declarationUnit = targetSymbolTable.unit;
//                 zoneInfo.isHidden = false ;
                if (zoneInfo.accessTree==null || accessThroughParameter) {
                    // This is a heuristic to try and prefer an accessTree that
                    //  refers to a formal param rather than an equivalenced name:
                    zoneInfo.accessTree = oneAccess.accessTree;
                    zoneInfo.description = ILUtils.toString(zoneInfo.accessTree, Unit.language(zoneInfo.declarationUnit), ambiguousWithoutIndices) ;
                    zoneInfo.accessIndexes = oneAccess.accessIndexes;
                }
                if (rootSymbol!=null) {
                    if (rootSymbol.symbol!=null) zoneInfo.addVariableName(rootSymbol.symbol) ;
                    if (zoneInfo.ownerSymbolDecl==null) zoneInfo.ownerSymbolDecl = rootSymbol ;
                }
                // In C, variables can be declared at the file level.
                // The following makes sure they are not declared hidden:
                SymbolTable importsSymbolTable = null ;

                importsSymbolTable = targetSymbolTable ;
                if (importsSymbolTable.isTranslationUnitSymbolTable()) {
                    importsSymbolTable.addWaitingZoneInfo(zoneInfo) ;
                }
//                 if (TapEnv.isC(oneAccess.symbolTable.language())) {
//                     importsSymbolTable = oneAccess.symbolTable ;
//                 } else if (oneAccess.symbolTable.unit!=null) {
//                     importsSymbolTable = oneAccess.symbolTable.unit.importsSymbolTable() ;
//                 }
// System.out.println(" ONEACCESS:"+oneAccess+" TARGETSYMBOLTABLE:"+targetSymbolTable+" IMPORTSSYMBOLTABLE:"+importsSymbolTable);
//                 if (importsSymbolTable!=null /*was targetSymbolTable.isTranslationUnitSymbolTable()*/) {
// System.out.println("ADD WAITING 2 "+zoneInfo+" INTO "+importsSymbolTable) ;
//                     importsSymbolTable.addWaitingZoneInfo(zoneInfo) ;
//                 }

            }
            accesses = accesses.tail ;
        }
        zoneInfos = new TapList<>(zoneInfo, zoneInfos);
        return new TapList<>(new TapIntList(zoneInfo.zoneNb, null), null);
    }

    public ZoneInfo lastAllocated() {
        if (zoneInfos == null) {
            return null;
        }
        return zoneInfos.head;
    }

    public void addVarDecl(TapList zonesTree, SymbolDecl symbolDecl) {
        TapIntList zones = ZoneInfo.listAllZones(zonesTree, true);
        ZoneInfo zoneInfo;
        while (zones != null) {
            zoneInfo = (ZoneInfo) TapList.nth(zoneInfos, nextZone - zones.head - 1);
            if (zoneInfo != null) {
                zoneInfo.ownerSymbolDecl = symbolDecl;
            }
            zones = zones.tail;
        }
    }

    public void addArgRank(TapList zonesTree, int argRank) {
        TapIntList zones = ZoneInfo.listAllZones(zonesTree, true);
        ZoneInfo zoneInfo;
        while (zones != null) {
            zoneInfo = (ZoneInfo) TapList.nth(zoneInfos, nextZone - zones.head - 1);
            if (zoneInfo != null) {
                if (argRank == 0) {
                    zoneInfo.setKind(SymbolTableConstants.RESULT);
                } else if (argRank > 0) {
                    zoneInfo.setKind(SymbolTableConstants.PARAMETER);
                }
                zoneInfo.index = argRank;
            }
            zones = zones.tail;
        }
    }

    public void extractAllZoneInfosIntoTargetSymbolTable(TapList<ZoneInfo> toAllocatedZones) {
        ZoneInfo[] newZoneInfos = extractZoneInfos() ;
        targetSymbolTable.setDeclaredZoneInfos(newZoneInfos, SymbolTableConstants.ALLKIND);
        targetSymbolTable.setFreeDeclaredZone(nextZone, SymbolTableConstants.ALLKIND);
        if (toAllocatedZones!=null) {
            for (int i=0 ; i<newZoneInfos.length ; ++i) {
                toAllocatedZones.placdl(newZoneInfos[i]) ;
            }
        }
        targetSymbolTable.setDeclaredZoneInfos(extractRealZoneInfos(), SymbolTableConstants.REALKIND);
        targetSymbolTable.setFreeDeclaredZone(nextRealZone, SymbolTableConstants.REALKIND);
        targetSymbolTable.setDeclaredZoneInfos(extractIntZoneInfos(), SymbolTableConstants.INTKIND);
        targetSymbolTable.setFreeDeclaredZone(nextIntZone, SymbolTableConstants.INTKIND);
        targetSymbolTable.setDeclaredZoneInfos(extractPtrZoneInfos(), SymbolTableConstants.PTRKIND);
        targetSymbolTable.setFreeDeclaredZone(nextPtrZone, SymbolTableConstants.PTRKIND);
    }

    public ZoneInfo[] extractAllZoneInfosAsExternalShape() {
        return extractZoneInfos() ;
    }

    private ZoneInfo[] extractZoneInfos() {
        int nbz = TapList.length(zoneInfos);
        ZoneInfo[] result = new ZoneInfo[nbz];
        for (int i = nbz - 1; i >= 0; i--) {
            result[i] = zoneInfos.head;
            zoneInfos = zoneInfos.tail;
        }
        return result;
    }

    private ZoneInfo[] extractIntZoneInfos() {
        int nbz = TapList.length(intZoneInfos);
        ZoneInfo[] result = new ZoneInfo[nbz];
        for (int i = nbz - 1; i >= 0; i--) {
            result[i] = intZoneInfos.head;
            intZoneInfos = intZoneInfos.tail;
        }
        return result;
    }

    private ZoneInfo[] extractRealZoneInfos() {
        int nbz = TapList.length(realZoneInfos);
        ZoneInfo[] result = new ZoneInfo[nbz];
        for (int i = nbz - 1; i >= 0; i--) {
            result[i] = realZoneInfos.head;
            realZoneInfos = realZoneInfos.tail;
        }
        return result;
    }

    private ZoneInfo[] extractPtrZoneInfos() {
        int nbz = TapList.length(ptrZoneInfos);
        ZoneInfo[] result = new ZoneInfo[nbz];
        for (int i = nbz - 1; i >= 0; i--) {
            result[i] = ptrZoneInfos.head;
            ptrZoneInfos = ptrZoneInfos.tail;
        }
        return result;
    }
}
