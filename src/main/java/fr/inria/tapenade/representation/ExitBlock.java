/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.Tree;

/**
 * Special Block at the exit of a Unit.
 */
public final class ExitBlock extends Block {

    /**
     * Used to store temporarily the comments that indicate pointer dests.
     */
    private TapList<Tree> pointerComments;

    /**
     * @return the comments that indicate pointer dests.
     */
    public TapList<Tree> pointerComments() {
        return pointerComments;
    }

    /**
     * Store temporarily the comments that indicate pointer dests.
     */
    public void setPointerComments(TapList<Tree> pointerComments) {
        this.pointerComments = pointerComments;
    }

    /**
     * Creation, with the given "symbolTable" attached.
     */
    public ExitBlock(SymbolTable symbolTable) {
        super(symbolTable, null, null);
    }

    /**
     * Prints a short reference to this EntryBlock onto TapEnv.curOutputStream().
     */
    @Override
    public void cite() throws java.io.IOException {
        TapEnv.print("Exit");
    }

    @Override
    public String toString() {
        return "X" + super.toString();
    }
}
