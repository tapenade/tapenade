/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */
package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * A type with a name. This class should be replaced soon.
 */
public final class NamedTypeSpec extends TypeSpec {

    /**
     * The given name.
     */
    private final String name;
    /**
     * The Tree that contains the given name.
     */
    private final Tree nameTree;

    /**
     * Create a new named type.
     */
    public NamedTypeSpec(String name) {
        super(SymbolTableConstants.NAMEDTYPE);
        this.name = name;
        this.nameTree = ILUtils.build(ILLang.op_ident, name);
    }

    /**
     * @return the name given to this named type.
     */
    public String name() {
        return nameTree.stringValue();
    }

    @Override
    protected String baseTypeName() {
        return "";
    }

    @Override
    public boolean isNamedType() {
        return true ;
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // Peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a NamedType* make little sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, we may want type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind:
        if (!(other instanceof NamedTypeSpec)) {
            return false;
        }
        return name != null && name.equals(((NamedTypeSpec) other).name);
    }

    @Override
    public NamedTypeSpec copy() {
        return this;
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        return ILUtils.copy(nameTree);
    }

    @Override
    public String showType() {
        return name;
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "NamedTypeSpec" //+"@"+Integer.toHexString(hashCode())
                + ':' + name();
    }
}
