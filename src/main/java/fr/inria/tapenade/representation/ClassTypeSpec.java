/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * A type for classes.
 */
public class ClassTypeSpec extends TypeSpec {
    /**
     * Unit of this class.
     */
    public Unit unit;
    private String name;

    public ClassTypeSpec(Unit unit) {
        super(SymbolTableConstants.CLASSTYPE);
        this.unit = unit;
        this.name = unit.name();
        if (name != null && name.length() > 6 && name.startsWith("class ")) {
            name = name.substring(6);
        }
    }

    @Override
    protected String baseTypeName() {
        return this.name;
    }

//     // [llh] Written from scratch on 2017sept5, eventually to clean the mess of canReceive/sameTypes/etc...
//     public boolean isLargerThan(TypeSpec other) {
//         // Peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
//         // (layers of ModifiedTypeSpec's *around a Class* make no sense, so we may peel them)
//         other = peelWrapperAndModified(other) ;
//         // Easy strict == case:
//         if (this==other) return true ;
//         // other must be specified and must be of matching kind: 
//         if (other==null || !(other instanceof ClassTypeSpec)) return false ;
//         ClassTypeSpec otherClassType = (ClassTypeSpec)other ;
//         // Test class derivation:
//         return (unit==null ||
//                 (otherClassType.unit!=null && otherClassType.unit.subClassOf(unit))) ;
//     }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a Class* make no sense, so we may peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind: 
        if (!(other instanceof ClassTypeSpec)) {
            return false;
        }
        ClassTypeSpec otherClassType = (ClassTypeSpec) other;
        if (testIsEquals(comparison)) {
            return unit != null && otherClassType.unit == unit;
        } else {
            return unit == null ||
                    otherClassType.unit != null && otherClassType.unit.subClassOf(unit);
        }
    }

//     public boolean equalType(TypeSpec otherTypeSpec,
//             SymbolTable otherSymbolTable, int eqLevel,
//             TapList<TapPair> dejaVu, SymbolTable symbolTable) {
//         boolean result = false ;
//         if (otherTypeSpec != null) {
//             if (this == otherTypeSpec)
//                 result = true;
//             else if (TypeSpec.isA(otherTypeSpec, CLASSTYPE) && otherTypeSpec instanceof ClassTypeSpec)
//                 result = (name != null && name.equals(((ClassTypeSpec)otherTypeSpec).name)) ;

//         }
//         return result;
//     }

    @Override
    public TypeSpec copy() {
        return this; // elaborate?
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        return this;
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        return ILUtils.build(ILLang.op_ident, name);
    }

    @Override
    public String showType() {
        return name;
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "Class" //+"@"+Integer.toHexString(hashCode())
                + " " + name;
    }
}
