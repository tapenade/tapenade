/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

/**
 * A field declaration in a CompositeTypeSpec.
 */
public final class FieldDecl extends SymbolDecl {
    private WrapperTypeSpec type;

    @Override
    public WrapperTypeSpec type() {
        return type;
    }

    protected void setType(WrapperTypeSpec typeSpec) {
        this.type = typeSpec;
    }

    private Tree bitfieldTree;

    protected Tree bitfieldTree() {
        return bitfieldTree;
    }

    protected void setBitfieldTree(Tree bitfieldTree) {
        this.bitfieldTree = bitfieldTree;
    }

    /**
     * contains the initialization parent op_assign or op_pointerAssign because
     * the assigneent rhs may be modified later, after type-checking (e.g from arrayAccess to call)
     */
    private Tree initializationTree;

    protected FieldDecl(Tree identTree, WrapperTypeSpec typeSpec) {
        super(identTree, SymbolTableConstants.FIELD);

        this.type = typeSpec;
    }

    protected FieldDecl(String name, WrapperTypeSpec typeSpec) {
        super(name, SymbolTableConstants.FIELD);

        this.type = typeSpec;
    }

    protected Tree initializationTree() {
        if (initializationTree == null || initializationTree.isAtom()) {
            return initializationTree;
        } else {
            return initializationTree.down(2);
        }
    }

    protected Tree generateInitializationTree() {
        if (initializationTree == null) {
            return null;
        } else if (initializationTree.isAtom()) {
            return ILUtils.build(ILLang.op_assign,
                    ILUtils.build(ILLang.op_ident, symbol),
                    initializationTree);
        } else {
            return initializationTree;
        }
    }

    protected Tree getInitializationTree() {
        return initializationTree;
    }

    protected void setInitializationTree(Tree tree) {
        initializationTree = tree;
    }

    /**
     * Collects into toUsedTrees all expressions that are needed for the
     * declaration of this SymbolDecl.
     *
     * @param toUsedTrees TapList of used Tree
     * @param toDejaVu    TapList of TypeSpec
     */
    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (type != null && type.wrappedType != null) {
            type.wrappedType.collectUsedTrees(toUsedTrees, toDejaVu);
        }
    }

    @Override
    public String toString() {
        return symbol     //+ " @" + Integer.toHexString(hashCode())
            + ": " + (type == null ? "null_type" : type) + " field."
            + (!isActiveSymbolDecl() ? "" : " Active")
            + (initializationTree==null ? "" : " Init:"+initializationTree)
            + (extraInfo() == null ? "" : " X"+extraInfo())
            + (bitfieldTree==null ? "" : " B:" + ILUtils.toString(bitfieldTree))
            ;
    }
}

