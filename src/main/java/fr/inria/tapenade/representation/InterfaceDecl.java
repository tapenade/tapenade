/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ADActivityAnalyzer;

//TODO: CLARIFY THE DUPLICATE REPRESENTATIONS, EITHER InterfaceDecl OR A FunctionDecl such that isInterface() is true !
/**
 * 3 different interfaces declarations exist in Fortran90:
 * <p>
 * 1/ Simple interface block to provide an explicit
 * interface to an external of dummy procedure, this
 * interface has no associated name.
 * <pre>
 * {@code interface
 * function f(t) ...  end function f
 * end interface}
 * </pre>
 * cf nonRegression/set06/v222.
 * <p>
 * 2/ Interface used for overloading with several
 * interface bodies, with a generic name.
 * The associated InterfaceDecl contains a list of FunctionDecl,
 * its symbol is the generic name.
 * <pre>
 * {@code
 * interface name
 * function f1(t) ...  end function f1
 * ...
 * function fn(t) ...  end function fn
 * end interface}
 * </pre>
 * cf nonRegressions/set04/v032.
 * <p>
 * 3/ Interface used for overloading with several
 * procedure names, with a generic name.
 * <pre>
 * {@code
 * interface g
 * module procedure sg, dg
 * end interface}
 * </pre>
 * The associated InterfaceDecl contains a list of String.
 * cf nonRegressions/set04/v046.
 * <p>
 * The declaration of the interface is contained in an Instruction.
 * The root Tree operator is interfaceDecl:
 * <pre>
 * {@code
 * interfaceDecl  -> OptExpression Interfaces OptExpression;
 * Interfaces  ::= interfaces;
 * interfaces  -> Interface * ...;
 * Interface  ::= interface moduleProcedure;
 * }</pre>
 * <p>
 * OptExpression.1 contains the name of the interface (2/ and 3/)
 * or none (1/).
 * interface operator contains the header of a function as described
 * in 1/ and 2/
 * moduleProcedure operator contains a list of strings as described in 3/
 * <p>
 * In 1/, the InterfaceDecl symbol is the name of the contained procedure.
 * In 2/ and 3/, the InterfaceDecl symbol is the name of the interface.
 */
public final class InterfaceDecl extends SymbolDecl {
    protected boolean usefulName = true;
    public TapList<Tree> contents;
    /**
     * The FunctionDecl's grouped by this InterfaceDecl.
     */
    public TapList<FunctionDecl> functionDecls;
    /**
     * List of function names
     */
    public TapList<String> functionNames;
    /**
     * The unit that defined this Interface.
     */
    protected Unit containerUnit;
    /**
     * symbolTable containing the definition of this interface
     * null corresponds to the rootSymbolTable.
     */
    protected SymbolTable definitionSymbolTable;
    /**
     * for a function passed as formal parameter, its rank in the parameters list. From 1 up (special 0 for result).
     */
    public int formalArgRank = SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK;
    /**
     * True iff this InterfaceDecl results from differentiation of another one.
     */
    private boolean isADifferentiatedInterface;
    /**
     * The list of the Units of kind "interfaceunits" that were
     * created when parsing this interface declaration.
     */
    private TapList<Unit> interfaceUnits;

    protected InterfaceDecl() {
        super();
        this.kind = SymbolTableConstants.INTERFACE;
        this.defPositions = new TapList<>(null, null);
        this.nameTree = null;
        this.functionDecls = null;
    }

    public InterfaceDecl(Tree identTree) {
        super(identTree, SymbolTableConstants.INTERFACE);
        this.nameTree = identTree;
        this.functionDecls = null;
    }

    protected InterfaceDecl(Tree identTree, FunctionDecl functionDecl,
                            SymbolTable symbolTable, Unit containerUnit) {
        super(identTree, SymbolTableConstants.INTERFACE);
        this.containerUnit = containerUnit;

        this.nameTree = identTree;
        this.definitionSymbolTable = symbolTable;
        this.functionDecls = null;
        if (functionDecl != null) {
            this.functionDecls = new TapList<>(functionDecl, null);
            functionDecl.unit().enclosingUnitOfInterface = containerUnit;
        }
    }

    public TapList<Unit> interfaceUnits() {
        return interfaceUnits;
    }

    public void setInterfaceUnits(TapList<Unit> units) {
        interfaceUnits = units;
    }

    protected void addInInterfaceDecl(Tree tree) {
        this.contents = TapList.append(this.contents, new TapList<>(tree, null));
        if (tree.opCode() == ILLang.op_moduleProcedure) {
            Tree[] sons = tree.children();
            for (Tree son : sons) {
                if (son.opCode() == ILLang.op_ident) {
                    this.functionNames =
                            new TapList<>(
                                    son.stringValue(), this.functionNames);
                }
            }
        }
    }

    protected void addInInterfaceDecl(FunctionDecl functionDecl, Unit enclosingUnit) {
        functionDecl.unit().enclosingUnitOfInterface = enclosingUnit;
        this.functionDecls =
                TapList.append(
                        this.functionDecls, new TapList<>(functionDecl, null));
        if (nameTree == null) {
            nameTree = ILUtils.build(ILLang.op_none);
            symbol = functionDecl.symbol;
            usefulName = false;
        }
    }

    /**
     * [llh] future implementation.
     */
    private FunctionDecl findCalledFunction(FunctionTypeSpec actualCallTypeSpec,
                                            Tree actualCallTree) {
        TapList<FunctionDecl> funcDecls = functionDecls;
        TapList<String> funcNames = functionNames;
        FunctionDecl funcDecl;
        TapList<FunctionDecl> sameFuncDecls;
        String funcName;
        int thisMatchRk;
        int bestMatchRkSoFar = 999;
        // The list of exact matches:
        TapList<FunctionDecl> exactMatches = null;
        // The best approximate match so far:
        FunctionDecl bestMatchSoFar = null;
        // The 2 SymbolTables in which the "interfaced" procedures can be:
        SymbolTable publicSymbolTable = definitionSymbolTable;
        SymbolTable privateSymbolTable = null;
        if (definitionSymbolTable != null) {
            // HACK to make it work with the new copy of
            // symboltable that copy functionDecl too.
            Unit unit = definitionSymbolTable.unit;
            if (unit != null) {
                publicSymbolTable = unit.publicSymbolTable();
                privateSymbolTable = unit.privateSymbolTable();
            }
        }
        // Explore the possible funcDecl's of the interface definition:
        while (funcDecls != null) {
            funcDecl = funcDecls.head;
            thisMatchRk = measureMatching(funcDecl);
            if (thisMatchRk == 0) {
                exactMatches = new TapList<>(funcDecl, exactMatches);
                bestMatchRkSoFar = 0;
            } else if (thisMatchRk < bestMatchRkSoFar) {
                bestMatchSoFar = funcDecl;
                bestMatchRkSoFar = thisMatchRk;
            }
            funcDecls = funcDecls.tail;
        }
        // Explore the possible procedure names of the interface definition:
        while (funcNames != null) {
            funcName = funcNames.head;
            funcDecl = null;
            if (privateSymbolTable != null) {
                sameFuncDecls = privateSymbolTable.getFunctionDecl(funcName, null, null, false);
                funcDecl = sameFuncDecls == null ? null : sameFuncDecls.head;
            }
            if (funcDecl == null && publicSymbolTable != null) {
                sameFuncDecls = publicSymbolTable.getFunctionDecl(funcName, null, null, false);
                funcDecl = sameFuncDecls == null ? null : sameFuncDecls.head;
            }
            if (funcDecl != null) {
                thisMatchRk = measureMatching(funcDecl);
                if (thisMatchRk == 0) {
                    exactMatches = new TapList<>(funcDecl, exactMatches);
                    bestMatchRkSoFar = 0;
                } else if (thisMatchRk < bestMatchRkSoFar) {
                    bestMatchSoFar = funcDecl;
                    bestMatchRkSoFar = thisMatchRk;
                }
            }
            funcNames = funcNames.tail;
        }
        // return the best match found. Complain if problems.
        if (exactMatches != null) {
            // return the procedure(s) matching exactly, if any:
            if (exactMatches.tail != null) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, actualCallTree, "(DD23) More than one procedure in interface " + symbol + " exactly match this call");
            }
            return exactMatches.head;
        } else if (bestMatchSoFar != null) {
            // ... else return the best approximately matching procedure:
            return bestMatchSoFar;
        } else {
            // ... else complain and return null:
            TapEnv.fileWarning(TapEnv.MSG_WARN, actualCallTree, "(DD23) No procedure in interface " + symbol + " can match this call");
            return null;
        }
    }

    private int measureMatching(FunctionDecl funcDecl) {
        if (funcDecl != null && funcDecl.functionTypeSpec() != null) {
            return 4;
        } else {
            return 1000;
        }
    }
    // end future implementation [llh]

    public FunctionDecl findFunctionDecl(WrapperTypeSpec[] types, WrapperTypeSpec resultType, Tree callTree) {
        //TODO: Peut-etre tenir compte du resultType quand il est non null !!
        FunctionDecl functionDecl = findFunctionDeclEqualsOrElementType(types, true, true, false, callTree);
        if (functionDecl == null) {
            // vmp: array-notation?
            functionDecl = findFunctionDeclEqualsOrElementType(types, true, false, false, callTree);
        }
        if (functionDecl == null) {
            functionDecl = findFunctionDeclEqualsOrElementType(types, false, true, false, callTree);
        }
        if (functionDecl == null) {
            // vmp: array-notation + type compatible?
            functionDecl = findFunctionDeclEqualsOrElementType(types, false, false, false, callTree);
        }
        return functionDecl;
    }

    protected FunctionDecl findOneFunctionDecl(WrapperTypeSpec[] types, Tree callTree) {
        return findFunctionDeclEqualsOrElementType(types, false, false, true, callTree);
    }

    private FunctionDecl findFunctionDeclEqualsOrElementType(
            WrapperTypeSpec[] types,
            boolean strictComparisonForType, boolean strictComparisonForArray,
            boolean onlyParamsNumber,
            Tree callTree) {
        // symbolTableOfInterfaceDef == public or privateSymbolTable
        SymbolTable symbolTableOfInterfaceDef = this.definitionSymbolTable;
        // HACK to make it work with the new copy of symboltable that copy functionDecl too :
        Unit unit = symbolTableOfInterfaceDef == null ? null : symbolTableOfInterfaceDef.unit;
        SymbolTable publicSymbolTable = unit == null ? symbolTableOfInterfaceDef : unit.publicSymbolTable();
        SymbolTable privateSymbolTable = unit == null ? symbolTableOfInterfaceDef : unit.privateSymbolTable();
        FunctionDecl foundFunc = null;
        FunctionDecl func;
        TapList<FunctionDecl> sameFuncs;
        // Search in functionDecls, NOT considering optional arguments:
        TapList<FunctionDecl> funcs = functionDecls;
        while (foundFunc == null && funcs != null) {
            func = funcs.head;
            if (func.functionTypeSpec() != null &&
                    isCalledFunctionFromInterface(
                            func.unit(), types,
                            strictComparisonForType, strictComparisonForArray, onlyParamsNumber, false,
                            callTree)) {
                foundFunc = func;
            }
            funcs = funcs.tail;
        }
        // Search in functionDecls, considering optional arguments:
        funcs = callTree.opCode() == ILLang.op_call ? functionDecls : null;
        while (foundFunc == null && funcs != null) {
            func = funcs.head;
            if (func.functionTypeSpec() != null &&
                    isCalledFunctionFromInterface(
                            func.unit(), types,
                            strictComparisonForType, strictComparisonForArray, onlyParamsNumber, true,
                            callTree)) {
                foundFunc = func;
            }
            funcs = funcs.tail;
        }
        // Search in functionNames, NOT considering optional arguments:
        TapList<String> funcNames = functionNames;
        while (foundFunc == null && funcNames != null) {
            sameFuncs = privateSymbolTable.getFunctionDecl(funcNames.head, null, null, false);
            func = sameFuncs == null ? null : sameFuncs.head;
            if (func != null && func.functionTypeSpec() != null &&
                    isCalledFunctionFromInterface(
                            func.unit(), types,
                            strictComparisonForType, strictComparisonForArray, onlyParamsNumber, false,
                            callTree)) {
                foundFunc = func;
            }
            funcNames = funcNames.tail;
        }
        // Search in functionNames, considering optional arguments:
        funcNames = callTree.opCode() == ILLang.op_call ? functionNames : null;
        while (foundFunc == null && funcNames != null) {
            sameFuncs = privateSymbolTable.getFunctionDecl(funcNames.head, null, null, false);
            func = sameFuncs == null ? null : sameFuncs.head;
            if (func != null && func.functionTypeSpec() != null &&
                    isCalledFunctionFromInterface(
                            func.unit(), types,
                            strictComparisonForType, strictComparisonForArray, onlyParamsNumber, true,
                            callTree)) {
                foundFunc = func;
            }
            funcNames = funcNames.tail;
        }
        // Fallback searches: if only one functionDecls or one functionNames.
        // [llh] not sure this is not redundant with the searches above?
        if (foundFunc == null) {
            func = null;
            if (functionDecls != null && functionDecls.tail == null) {
                func = functionDecls.head;
            }
            if (func == null && functionNames != null && functionNames.tail == null) {
                sameFuncs =
                        privateSymbolTable.getTopFunctionDecl(
                                functionNames.head, null, null, false);
                if (sameFuncs == null) {
                    sameFuncs =
                            publicSymbolTable.getTopFunctionDecl(
                                    functionNames.head, null, null, false);
                }
                func = sameFuncs == null ? null : sameFuncs.head;
            }
            if (func != null) {
                //ATTENTION: contrived code (cf F90:v06 vs v139).
                // We want to return this unique func as a fallback match
                // either if it has a different number of arguments (for v139) or if it has
                // the same number but only with correct types (for v06 to avoid circularity).
                FunctionTypeSpec funcTypeSpec = func.functionTypeSpec();
                assert funcTypeSpec != null;
                if (types.length != funcTypeSpec.argumentsTypes.length) {
                    foundFunc = func;
                } else {
                    boolean matches = true;
                    for (int index = types.length - 1; matches && index >= 0; --index) {
                        matches =
                                types[index] != null
                                        && types[index].equalsCompilDep(funcTypeSpec.argumentsTypes[index]);
                    }
                    if (matches) {
                        foundFunc = func;
                    }
                }
            }
        }
        return foundFunc;
    }

    private boolean isCalledFunctionFromInterface(
            Unit calledUnit,
            WrapperTypeSpec[] types,
            boolean strictComparisonForType, boolean strictComparisonForArray,
            boolean onlyParamsNumber, boolean considerOptionalArguments,
            Tree callTree) {
        FunctionTypeSpec funcTypeSpec = calledUnit.functionTypeSpec();
        int nbFormalArgs = funcTypeSpec.argumentsTypes.length;
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printOnTrace("Checking if call " + ILUtils.toString(callTree) + " actual types:");
            TapEnv.dumpOnTrace(types);
            TapEnv.printOnTrace(" is matched by available interface " + calledUnit.name() + ':' + funcTypeSpec);
            if (onlyParamsNumber) {
                TapEnv.printOnTrace(", watching only params length");
            } else {
                TapEnv.printOnTrace(", " + (strictComparisonForType ? "strict" : "lax") + " on types, " + (strictComparisonForArray ? "strict" : "lax") + " on arrays");
            }
            TapEnv.setTraceIndent(TapEnv.traceIndent() + 2);
        }
        boolean matches;
        if (considerOptionalArguments) {
            Tree newCallTree = ILUtils.copy(callTree);
            Tree optionalTree = ILUtils.copy(callTree);
            Tree[] actualArgs = ILUtils.getArguments(callTree).children();
            int nbActualArgs = actualArgs.length;
            types =
                    calledUnit.typeCheckNameEqOptionalArguments(null, actualArgs, types,
                            new WrapperTypeSpec[nbActualArgs], nbActualArgs,
                            newCallTree, optionalTree, new ToBool(false));
            calledUnit.typeCheckOptionalArguments(
                    types, newCallTree, optionalTree, new ToBool(false));
            int optNb = calledUnit.getOptionalParameterDeclsNumber();
            int nbNeededArgs = nbFormalArgs - optNb;
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace(", with " + nbNeededArgs + " args needed and " + optNb + " optional");
            }
            matches = nbNeededArgs <= nbActualArgs && nbActualArgs <= nbFormalArgs;
        } else {
            if (TapEnv.traceTypeCheckAnalysis()) {
                TapEnv.printlnOnTrace();
            }
            matches = types.length == nbFormalArgs;
        }
        if (matches && !onlyParamsNumber) {
            WrapperTypeSpec actualType;
            WrapperTypeSpec formalType;
            for (int index = Math.min(types.length, nbFormalArgs) - 1
                 ; matches && index >= 0
                    ; --index) {
                actualType = types[index];
                formalType = funcTypeSpec.argumentsTypes[index];
                // Specific FORTRAN90: we accept to call F(REAL) with argument of type "pointer to REAL"
                if (actualType != null && actualType.isPointer() && !formalType.isPointer()) {
                    actualType = (WrapperTypeSpec) actualType.peelPointer();
                }
                if (actualType != null && !WrapperTypeSpec.isFree(actualType)) {
                    boolean eqType;
                    if (strictComparisonForType) {
                        if (strictComparisonForArray) {
                            // Test for COMPIL_DEP equality (no use to try stricter COMPIL_INDEP
                            // because searching in multiple-interface is definitly COMPIL_DEP)
                            eqType = actualType.equalsCompilDep(formalType);
                        } else {
                            eqType = actualType.equalsCompilDepNeglectLengthes(formalType);
                        }
                    } else {
                        if (strictComparisonForArray) {
                            eqType = actualType.equalsNeglectSizes(formalType);
                        } else {
                            eqType = actualType.equalsNeglectSizesNeglectLengthes(formalType);
                        }
                    }
                    matches = eqType;
                }
            }
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.setTraceIndent(TapEnv.traceIndent() - 2);
            TapEnv.indentOnTrace(TapEnv.traceIndent());
            TapEnv.printlnOnTrace("--> Matching result:" + matches);
        }
        return matches;
    }

    protected TapList<Unit> units() {
        TapList<Unit> result = null;
        if (functionNames == null) {
            TapList<FunctionDecl> funcDecl = functionDecls;
            while (funcDecl != null) {
                result = new TapList<>(funcDecl.head.unit(), result);
                funcDecl = funcDecl.tail;
            }
        }
        return result;
    }

    protected FunctionDecl functionDecl() {
        FunctionDecl result = null;
        if (functionDecls != null) {
            result = functionDecls.head;
        }
        return result;
    }

    public boolean mustHaveADiff(ADActivityAnalyzer activityAnalyzer) {
        boolean active = false;
        FunctionDecl oneUnitDecl;
        TapList<FunctionDecl> oneUnitDecls;
        TapList<FunctionDecl> inFunctionDecls = functionDecls;
        while (!active && inFunctionDecls != null) {
            oneUnitDecl = inFunctionDecls.head;
            active = activityAnalyzer.isActiveUnit(oneUnitDecl.unit().origUnit);
            inFunctionDecls = inFunctionDecls.tail;
        }
        SymbolTable visibleSymbolTable =
                containerUnit == null ? definitionSymbolTable : containerUnit.privateSymbolTable();
        TapList<String> inFunctionNames = functionNames;
        while (!active && inFunctionNames != null) {
            oneUnitDecls = visibleSymbolTable.getFunctionDecl(inFunctionNames.head, null, null, false);
            oneUnitDecl = oneUnitDecls == null ? null : oneUnitDecls.head;
            if (oneUnitDecl != null) {
                Unit oneUnit = oneUnitDecl.unit();
                Unit origUnit = oneUnit.origUnit != null ? oneUnit.origUnit : oneUnit;
                active = activityAnalyzer.isActiveUnit(origUnit);
            }
            inFunctionNames = inFunctionNames.tail;
        }
        return active;
    }

    @Override
    public int formalArgRankOrResultZero() {
	return formalArgRank ;
    }

    @Override
    public String toString() {
        return symbol  //+" @"+Integer.toHexString(hashCode())
            + ": " + "interface."
            + " nameTree:" + nameTree
            + " interfaceUnits:" + interfaceUnits
            + " functionDecls:" + functionDecls
            + " functionNames:" + functionNames
            + " contents:" + contents
            + (extraInfo() == null ? "" : " X"+extraInfo())
            ;
    }

}
