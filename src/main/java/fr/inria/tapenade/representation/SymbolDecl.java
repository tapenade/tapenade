/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ActivityPattern;

/**
 * A symbol declaration, i.e. and entry in a SymbolTable.
 */
public class SymbolDecl {
    public String symbol;
    public Tree nameTree;
    public TapList<Tree> defPositions;
    /**
     * Useful to know what are the different notations leading to the same copy (a tree of this list)
     */
    protected InheritanceTree inheritanceTree;

    /**
     * Temporary: link to the copy of this SymbolDecl.
     */
    protected SymbolDecl copySymbolDecl;

    /** When this SymbolDecl was IMPORTed from another SymbolTable,
     * keeps the link to the original SymbolDecl and SymbolTable */
    public TapPair<SymbolDecl, SymbolTable> importedFrom ;

    public int kind;
    /**
     * The list of symbols defined in the same SymbolTable level, that are
     * used by this symbol declaration, and must therefore be declared
     * before this symbol is declared.
     */
    public TapList<SymbolDecl> dependsOn;
    public boolean isATrueSymbolDecl = true;
    protected String className;
    /**
     * Access info on this symbol e.g. "const" etc.
     */
    public TapList<String> accessInfo;
    /* Pour un symbolDecl, booleans indiquant s'il y a
     * une declaration "principale":
     *   une declaration op_varDeclaration ou
     *   op_typeDeclaration
     *   op_none est utilisee pour les parametres formels
     * une declaration supplementaire:
     *   une declaration op_varDimDeclaration
     *   une declaration op_common contenant un arrayDeclarator
     *     donnant la taille du tableau
     *   une declaration op_external pour un FunctionDecl
     *   une declaration op_intrinsic pour un FunctionDecl
     * les autres declarations (op_equivalence, op_constDeclaration,
     * ... ) n'ont pas besoin d'etre connues du symbolDecl.
     */
    private boolean hasMainInstruction;
    private boolean hasNoneInstruction;
    private boolean hasVarDimInstruction;
    private boolean hasCommonInstruction;
    private boolean hasCommonInstructionWithArrayDeclarator;
    private boolean hasExternalInstruction;
    private boolean hasIntrinsicInstruction;
    private boolean hasSaveInstruction;
    private boolean hasOptionalInstruction;
    private boolean hasPointerInstruction;

// public String hasxx() {
//  return
//   (hasMainInstruction() ? "1":"0")+
//   (hasOtherInstruction() ? "1":"0")+" "+
//   (hasMainInstruction ? "1":"0")+
//   (hasNoneInstruction ? "1":"0")+
//   (hasVarDimInstruction ? "1":"0")+
//   (hasCommonInstruction ? "1":"0")+
//   (hasCommonInstructionWithArrayDeclarator ? "1":"0")+" "+
//   (hasExternalInstruction ? "1":"0")+
//   (hasIntrinsicInstruction ? "1":"0")+
//   (hasSaveInstruction ? "1":"0")+
//   (hasOptionalInstruction ? "1":"0")+
//   (hasPointerInstruction ? "1":"0") ;
// }

    /**
     * Sometimes a SymbolDecl must be replaced with another, but the old SymbolDecl must remain for a while
     * because someone will look for it (see all the "update" functions that replace FOO with FOO_NODIFF).
     * Instead of actually removing the SymbolDecl in a later sweep, we may just
     * mark it as "isConsideredRemoved" to ensure TreeGen will not regenerate a declaration for it.
     */
    public boolean isConsideredRemoved = false;

    //[llh] TODO: fusionner ces (is|set)Active avec les (is|set)Active de Karim.
    public boolean hasAllocatableInstruction;
    private boolean hasAllocatableInstructionWithArrayDeclarator;
    private TapList<String> hasAccessDeclModifier;
    private boolean hasInterfaceInstruction;
    /**
     * pour les fixImplicit de symbolDecl declare's dans un include.
     */
    private Instruction fromInclude;
    /**
     * When the type comes from a Fortran IMPLICIT declaration, this implicit type.
     */
    private TypeSpec fromImplicitType = null;
    /**
     * Wrapper around a boolean so that every copy of the same original symbolDecl
     * shares the same differentiation status.
     */
    protected ToBool toIsActive = new ToBool(false);
    /**
     * True when this SymbolDecl is repeated at an upper SymbolTable level (the "sharingSymbolTable")
     * to implement the fact that it is not local to the current file.
     */
    private boolean isShared;
    /** True for variables declared as reference (e.g with the C++ ampersand notation) */
    private boolean isReference;
    /**
     * Extra information on this symbol, list of strings belonging to
     * {constant, save, data, in, out, inout, allocatable, external,
     * intrinsic, optional, pointer, target, private, public, ...}.
     */
    private TapList<String> extraInfo;

    /** True for C symbol names ending in "_" that represent a Fortran symbol, thus
     * using the old/default convention that does not require the BIND declaration. */
    public boolean underscoreToFortran = false ;

    protected SymbolDecl() {
        this.defPositions = new TapList<>(null, null);
    }

    protected SymbolDecl(String name, int kind) {
        this.symbol = name;
        this.kind = kind;
        this.defPositions = new TapList<>(null, null);
    }

    protected SymbolDecl(Tree identTree, int kind) {
        super();
        this.symbol = (identTree == null ? null : ILUtils.getIdentString(identTree)) ;
        this.kind = kind;
        this.defPositions = new TapList<>(identTree, null);
        this.nameTree = identTree;
    }

    public final void setFromImplicitType(TypeSpec implicitType) {
        fromImplicitType = implicitType;
    }

    public final TypeSpec fromImplicitType() {
        return fromImplicitType;
    }

    public static TapList<String> addInfosInList(TapList<String> extraInfo,
                                                 TapList<String> newInfos, SymbolDecl symbolDecl) {
        if (newInfos != null) {
            if (extraInfo == null) {
                if (TapList.containsEquals(newInfos, "pointer") && symbolDecl instanceof VariableDecl) {
                    ((VariableDecl)symbolDecl).turnTypeIntoPointer();
                    extraInfo = TapList.deleteString("pointer", newInfos);
                } else {
                    extraInfo = newInfos;
                }
            } else {
                String str = null;
                TapList<String> nInfos = newInfos;
                while (newInfos != null) {
                    str = newInfos.head;
                    if (str != null) {
                        if (str.equals("public") || str.equals("private")
                                || str.equals("PUBLIC") || str.equals("PRIVATE")) {
                            if (!(str.equals("public")
                                    && TapList.containsEquals(
                                    extraInfo, "private")
                                    || str.equals("private")
                                    && TapList.containsEquals(
                                    extraInfo, "public"))) {
                                if (!TapList.containsEquals(extraInfo, str)) {
                                    if
                                    (!(str.equals("PUBLIC")
                                            || str.equals("PRIVATE"))
                                            && (TapList.containsEquals(
                                            extraInfo, "public")
                                            || TapList.containsEquals(
                                            extraInfo, "private"))) {
                                        extraInfo = new TapList<>(str, extraInfo);
                                    } else {
                                        if
                                        (str.equals("public")
                                                || str.equals("private")) {
                                            extraInfo =
                                                    TapList.deleteString(
                                                            "PUBLIC", extraInfo);
                                            extraInfo =
                                                    TapList.deleteString(
                                                            "PRIVATE", extraInfo);
                                            extraInfo =
                                                    new TapList<>(str, extraInfo);
                                        }
                                    }
                                }
                            }
                        } else {
                            if (str.equals("pointer") && symbolDecl instanceof VariableDecl) {
                                ((VariableDecl)symbolDecl).turnTypeIntoPointer();
                            } else {
                                if (!TapList.containsEquals(extraInfo, str)) {
                                    extraInfo = new TapList<>(str, extraInfo);
                                }
                            }
                        }
                    }
                    newInfos = newInfos.tail;
                }
                /*dans ce cas, nInfos est une liste contenant
                 * un seul TapPair Unit, Tree
                 * tree == useDecl.down(2)
                 */
                if (str == null) {
                    extraInfo = TapList.append(extraInfo, nInfos);
                }
            }
        }
        return extraInfo;
    }

    protected static SymbolDecl copy(SymbolDecl symbol2copy) {
        return symbol2copy;
    }

    public static SymbolDecl build(WrapperTypeSpec typeSpec, TapList<String> accessInfo,
                                   boolean isPointer,
                                   Tree declarator, int flag, SymbolTable symbolTable,
                                   Instruction instruction, TapList<SymbolDecl> typeUsedSymbols) {
        switch (declarator.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_metavar: {
                SymbolDecl result;
                switch (flag) {
                    case SymbolTableConstants.FUNCTION: {
                        Unit functionUnit;
                        if (symbolTable.language() == TapEnv.FORTRAN) {
                            functionUnit = Unit.makeVarFunction(ILUtils.getIdentString(declarator), symbolTable.unit);
                        } else {
                            functionUnit = symbolTable.buildExternalUnit(ILUtils.getIdentString(declarator), false);
                        }
                        if (TapEnv.inIncludeFile() && TapEnv.isStdCInclude(TapEnv.currentIncludeInstruction())) {
                            functionUnit.setInStdCIncludeFile();
                        }
                        functionUnit.setFunctionTypeSpec((FunctionTypeSpec) typeSpec.wrappedType);
                        result = new FunctionDecl(declarator, functionUnit);
                        break;
                    }
                    case SymbolTableConstants.CONSTANT: {
                        VariableDecl variableDecl = symbolTable.getVariableDecl(
                                declarator.stringValue());
                        if (variableDecl != null) {
                            result = variableDecl;
                        } else {
                            result = new VariableDecl(declarator, typeSpec);
                        }
                        ((VariableDecl) result).setConstant();
                        break;
                    }
                    case SymbolTableConstants.FIELD:
                        if (isPointer) {
                            typeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
                        }
                        result = new FieldDecl(declarator, typeSpec);
                        break;
                    case SymbolTableConstants.VARIABLE:
                    default:
                        if (isPointer) {
                            typeSpec = new WrapperTypeSpec(new PointerTypeSpec(typeSpec, null));
                        }
                        if (TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
                            arrayTypeSpec.preciseSizeReferences(declarator, 8);
                        }
                        result = new VariableDecl(declarator, typeSpec);
                        break;
                }
                result.dependsOn = typeUsedSymbols;
                result.accessInfo = accessInfo;
                if (instruction != null && instruction.fromInclude() != null) {
                    result.setFromInclude(instruction.fromInclude());
                }
                return result;
            }
            case ILLang.op_arrayDeclarator: {
                Tree[] dimTrees = declarator.down(2).children();
                Tree dimTree;
                Tree lowerDimTree;
                Tree upperDimTree;
                Integer lower = TapEnv.relatedLanguageIsFortran() ? 1 : 0;
                Integer upper = null;
                ArrayDim[] dimensions = new ArrayDim[dimTrees.length];
                String identNext = ILUtils.getIdentString(declarator.down(1));
                for (int i = 0; i < dimTrees.length; i++) {
                    dimTree = dimTrees[i];
                    if (dimTree.opCode() == ILLang.op_dimColon) {
                        TapList<SymbolDecl> newSymbolDecls =
                                symbolTable.declareAllSymbolsIn(dimTree, dimTree,
                                        null, null, SymbolTableConstants.VARIABLE, true, true, instruction);
                        typeUsedSymbols = TapList.prependNoDups(newSymbolDecls, typeUsedSymbols);
                        lowerDimTree = dimTree.down(1);
                        upperDimTree = dimTree.down(2);
                    } else {
                        lowerDimTree = null;
                        upperDimTree = dimTree;
                    }
                    TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, typeUsedSymbols);
                    if (!ILUtils.isNullOrNone(lowerDimTree)) {
                        assert lowerDimTree != null;
                        lower = symbolTable.computeIntConstant(lowerDimTree);
                        addUsedSymbolsInExpr(lowerDimTree, toTypeUsedSymbols, symbolTable, null, false, true);
                    }
                    if (!ILUtils.isNullOrNone(upperDimTree)) {
                        assert upperDimTree != null;
                        upper = symbolTable.computeIntConstant(upperDimTree);
                        addUsedSymbolsInExpr(upperDimTree, toTypeUsedSymbols, symbolTable, null, false, true);
                    }
                    typeUsedSymbols = toTypeUsedSymbols.tail;
                    dimensions[i] = new ArrayDim(dimTree, lower, upper, identNext == null ? null : declarator.down(1), i + 1, dimTrees.length, 1);
                }
                if (TapEnv.relatedUnit() != null
                        && TapEnv.relatedUnit().isFortran()
                        && TypeSpec.isA(typeSpec, SymbolTableConstants.ARRAYTYPE)
                        && !typeSpec.baseTypeName().equals("character")) {
                    ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) typeSpec.wrappedType;
                    if (arrayTypeSpec.dimensions() != null) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, declarator, "(DD22) Ignored array dimensions " + arrayTypeSpec.showDimensions() + " in declaration of " + ILUtils.toString(declarator.down(1)));
                    }
                    typeSpec = arrayTypeSpec.elementType();
                }
                TypeSpec arrayType =
                    (TapEnv.relatedLanguageIsC() ?
                     new PointerTypeSpec(typeSpec, (dimensions.length > 0 ? dimensions[0] : null)) :
                     new ArrayTypeSpec(typeSpec, dimensions)) ;
                return build(new WrapperTypeSpec(arrayType), accessInfo, isPointer,
                             declarator.down(1), flag, symbolTable, instruction, typeUsedSymbols);
            }
            // Special: this happens when processing EQUIVALENCES: ref to T(10) implies
            //  declaration of T with a still unknown dimension.
            case ILLang.op_arrayAccess: {
                int dim = declarator.down(2).children().length;
                ArrayDim[] dimensions = new ArrayDim[dim];
                for (int i = 0; i < dim; i++) {
                    dimensions[i] =
                            new ArrayDim(
                                    ILUtils.build(
                                            ILLang.op_dimColon,
                                            ILUtils.build(ILLang.op_none),
                                            ILUtils.build(ILLang.op_none)),
                                    1, null, null, -1, 0, 0);
                }
                return build(
                        new WrapperTypeSpec(new ArrayTypeSpec(new WrapperTypeSpec(null), dimensions)),
                        accessInfo, isPointer, declarator.down(1), flag, symbolTable, instruction, typeUsedSymbols);
            }
            case ILLang.op_substringAccess:
                // Special: this happens when processing EQUIVALENCES:
                return build(new WrapperTypeSpec(null), accessInfo, isPointer,
                        declarator.down(1), flag, symbolTable, instruction, typeUsedSymbols);
            case ILLang.op_sizedDeclarator: {
                Tree sizeModifier = declarator.down(2);
                TypeSpec newSizedType;
                if (typeSpec.isCharacter()) {
                    Integer length = null;
                    if (sizeModifier.opCode() == ILLang.op_intCst) {
                        length = sizeModifier.intValue();
                    }
                    String identNext = ILUtils.getIdentString(declarator.down(1));
                    ArrayDim[] dims = new ArrayDim[1];
                    dims[0] = new ArrayDim(null, 1, length, identNext == null ? null : declarator.down(1), 1, 1, 1);
                    newSizedType = new ArrayTypeSpec(typeSpec, dims);
                } else {
                    newSizedType = new ModifiedTypeSpec(typeSpec, sizeModifier, symbolTable);
                }
                return build(new WrapperTypeSpec(newSizedType), accessInfo, isPointer, declarator.down(1),
                        flag, symbolTable, instruction, typeUsedSymbols);
            }
            case ILLang.op_pointerDeclarator:
                PointerTypeSpec pointerType = new PointerTypeSpec(typeSpec, null);
                pointerType.subAccess = accessInfo;
                return build(new WrapperTypeSpec(pointerType), null, isPointer,
                        declarator.down(1), flag, symbolTable, instruction, typeUsedSymbols);
            case ILLang.op_referenceDeclarator: {
                SymbolDecl result =
                    build(typeSpec, null, isPointer,
                          declarator.down(1), flag, symbolTable, instruction, typeUsedSymbols);
                result.setReference() ;
                return result ;
            }
            case ILLang.op_pointerAssign:
            case ILLang.op_assign: {
                Tree initialTree = declarator.down(2);
                Integer initialValue = null;
                if (initialTree.opCode() != ILLang.op_none
                        && (typeSpec.isIntegerBase() || ILUtils.seemsInteger(initialTree))) {
                    initialValue = symbolTable.computeIntConstant(initialTree);
                }
                TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, typeUsedSymbols);
                addUsedSymbolsInExpr(initialTree, toTypeUsedSymbols, symbolTable, null, false, true);
                SymbolDecl resultVariableDecl =
                        build(typeSpec, accessInfo, isPointer, declarator.down(1), flag,
                                symbolTable, instruction, toTypeUsedSymbols.tail);
                if (resultVariableDecl instanceof VariableDecl) {
                    ((VariableDecl) resultVariableDecl).setInitializationTree(declarator);
                    ((VariableDecl) resultVariableDecl).constantValue = initialValue;
                } else {
                    if (resultVariableDecl instanceof FieldDecl) {
                        ((FieldDecl) resultVariableDecl).setInitializationTree(declarator);
                    }
                }
                return resultVariableDecl;
            }
            case ILLang.op_bitfieldDeclarator: {
                Tree bitfieldTree = declarator.down(2);
                TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, typeUsedSymbols);
                addUsedSymbolsInExpr(bitfieldTree, toTypeUsedSymbols, symbolTable, null, false, true);
                SymbolDecl resultVariableDecl =
                        build(typeSpec, accessInfo, isPointer, declarator.down(1), flag,
                                symbolTable, instruction, toTypeUsedSymbols.tail);
                if (resultVariableDecl instanceof FieldDecl) {
                    ((FieldDecl) resultVariableDecl).setBitfieldTree(bitfieldTree);
                }
                return resultVariableDecl;
            }
            case ILLang.op_functionDeclarator: {
                TapList<SymbolDecl> toTypeUsedSymbols = new TapList<>(null, typeUsedSymbols);
                ToBool isVariableArgList = new ToBool(false);
                TapList<WrapperTypeSpec> argList = buildTypeSpecFromArgs(symbolTable, instruction, toTypeUsedSymbols,
                        declarator.down(2).children(), isVariableArgList);
                WrapperTypeSpec[] argsTypeSpecs = new WrapperTypeSpec[TapList.length(argList)];
                for (int i = TapList.length(argList) - 1; i >= 0; --i) {
                    argsTypeSpecs[i] = argList.head;
                    argList = argList.tail;
                }
                FunctionTypeSpec functionTypeSpec = new FunctionTypeSpec(typeSpec, argsTypeSpecs, isVariableArgList.get());
                boolean isPlainFuncDecl = declarator.down(1).opCode() == ILLang.op_ident;
                return build(new WrapperTypeSpec(functionTypeSpec), accessInfo, isPointer, declarator.down(1),
                        (symbolTable.unit == null || symbolTable.unit.isC()) && !isPlainFuncDecl ? flag : SymbolTableConstants.FUNCTION,
                        symbolTable, instruction, toTypeUsedSymbols.tail);
            }
            case ILLang.op_modifiedDeclarator: {
                Tree[] modifiers = declarator.down(1).children();
                TapList<String> toInfos = new TapList<>(null, null);
                for (int i = modifiers.length - 1; i >= 0; i--) {
                    if (modifiers[i].opCode() == ILLang.op_ident) {
                        String modifierName = ILUtils.getIdentString(modifiers[i]);
                        if (modifierName.equals("const") ||
                                modifierName.equals("__const") ||
                                modifierName.equals("__const__") ||
                                modifierName.equals("restrict") ||
                                modifierName.equals("__restrict")) {
                            accessInfo = new TapList<>(modifierName, accessInfo);
                        } else {
                            toInfos.placdl(modifierName);
                        }
                    } else {
                        TapEnv.toolWarning(-1, " ** Unexpected modifier operator: " + modifiers[i].opName() + " in " + ILUtils.toString(declarator));
                    }
                }
                SymbolDecl result = build(typeSpec, accessInfo, isPointer, declarator.down(2), flag,
                        symbolTable, instruction, typeUsedSymbols);
                result.addExtraInfo(toInfos.tail);
                return result;
            }
            case ILLang.op_pointerAccess:
                return null;
            default:
                TapEnv.toolWarning(-1, "(SymbolDecl build) Unexpected declarator operator: " + declarator.opName());
                return null;
        }
    }

    protected static TapList<WrapperTypeSpec> buildTypeSpecFromArgs(SymbolTable symbolTable,
                                                                    Instruction instruction,
                                                                    TapList<SymbolDecl> toTypeUsedSymbols,
                                                                    Tree[] argDecls, ToBool isVariableArgList) {
        TapList<WrapperTypeSpec> argList = null;
        Tree argDecl;
        Tree[] declarators;
        WrapperTypeSpec argTypeSpec;
        VariableDecl argSymbolDecl;
        isVariableArgList.set(false);
        for (Tree decl : argDecls) {
            argDecl = decl;
            TapList<String> toAccess = new TapList<>(null, null);
            ToBool argIsPointer = new ToBool(false);
            switch (argDecl.opCode()) {
                case ILLang.op_varDeclaration:
                    argTypeSpec =
                            TypeSpec.build(
                                    argDecl.down(2), symbolTable, instruction,
                                    toTypeUsedSymbols, new TapList<>(null, null), toAccess,
                                    argIsPointer, null);
                    declarators = argDecl.down(3).children();
                    if (declarators.length > 0) {
                        for (Tree declarator : declarators) {
                            argSymbolDecl = (VariableDecl) build(argTypeSpec, toAccess.tail,
                                    argIsPointer.get(),
                                    declarator, SymbolTableConstants.VARIABLE, symbolTable, instruction,
                                    null);
                            argList = new TapList<>(argSymbolDecl.type(), argList);
                        }
                    } else {
                        argList = new TapList<>(argTypeSpec, argList);
                    }
                    break;
                case ILLang.op_variableArgList:
                    isVariableArgList.set(true);
                    break;
                default:
                    argTypeSpec =
                            TypeSpec.build(
                                    argDecl, symbolTable, instruction, toTypeUsedSymbols,
                                    new TapList<>(null, null), new TapList<>(null, null),
                                    argIsPointer, null);
                    if (argIsPointer.get()) {
                        argTypeSpec = new WrapperTypeSpec(new PointerTypeSpec(argTypeSpec, null));
                    }
                    argList = new TapList<>(argTypeSpec, argList);
            }
        }
        return argList;
    }

    /**
     * Finds all symbols used in "expression", that must be declared before "expression", and
     * that are defined in (the current top level of) "symbolTable".
     * When "top" is true, accumulates only symbols found in the top of "symbolTable".
     * Accumulates them in the tail of "toUsedSymbols".
     *
     * @param toUsedSymbols a hatted list (TapList starting with a null) that accumulates the used symbols
     * @param top           when true, searches VariableDecl's only in the top of "symbolTable".
     * @param strict        true, variable in Common are added to its "UsedSymbols"
     */
    public static void addUsedSymbolsInExpr(Tree expression,
                                               TapList<SymbolDecl> toUsedSymbols, SymbolTable symbolTable,
                                               TapList<String> undeclaredSymbols, boolean top, boolean strict) {
        switch (expression.opCode()) {
            case ILLang.op_ident: {
                String symbol = expression.stringValue();
                TapList usedSymbols = null;
                SymbolDecl symbolDecl;
                if (symbol != null) {
                    symbolDecl = symbolTable.getDecl(symbol, SymbolTableConstants.VARIABLE, SymbolTableConstants.CONSTANT, top, null);
                    if (symbolDecl != null) {
                        usedSymbols = new TapList<>(symbolDecl, null);
                    }
                    if (usedSymbols == null) {
                        // TODO: we should use the arguments types to restrict the functionDecls found.
                        usedSymbols = symbolTable.getFunctionDecl(symbol, null, null, false);
                    }
                    if (usedSymbols == null) {
                        symbolDecl = symbolTable.getTypeDecl(symbol);
                        if (symbolDecl != null) {
                            usedSymbols = new TapList<>(symbolDecl, null);
                        }
                    }
                    if (usedSymbols == null) {
                        symbolDecl = symbolTable.getClassDecl(symbol);
                        if (symbolDecl != null) {
                            usedSymbols = new TapList<>(symbolDecl, null);
                        }
                    }
                    if (usedSymbols != null) {
                        while (usedSymbols != null) {
                            symbolDecl = (SymbolDecl) usedSymbols.head;
                            if (!TapList.contains(toUsedSymbols, symbolDecl)) {
                                toUsedSymbols.placdl(symbolDecl);
                            }
                            usedSymbols = usedSymbols.tail;
                        }
                    } else {
                        if (undeclaredSymbols != null) {
                            undeclaredSymbols.tail = new TapList<>(expression.stringValue(), undeclaredSymbols.tail);
                        }
                    }
                }
                break;
            }
            case ILLang.op_scopeAccess: {
                // Add symbols used in the given searching scope or nameSpace:
                addUsedSymbolsInExpr(expression.down(1), toUsedSymbols,
                        symbolTable, undeclaredSymbols, top, strict);
                SymbolTable scopeST = symbolTable.getPrefixedNamedScope(expression.down(1));
                // Add symbols used in expression.down(2) only if their searching scope is equal to the current symbolTable:
                if (scopeST == symbolTable) {
                    addUsedSymbolsInExpr(
                            expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_power:
            case ILLang.op_dimColon:
            case ILLang.op_concat:
            case ILLang.op_functionType:
            case ILLang.op_lt:
            case ILLang.op_gt:
            case ILLang.op_le:
            case ILLang.op_ge:
            case ILLang.op_eq:
            case ILLang.op_neq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
            case ILLang.op_modifiedType:
                addUsedSymbolsInExpr(
                        expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_loop:
                addUsedSymbolsInExpr(
                        expression.down(3), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_if:
            case ILLang.op_switch:
            case ILLang.op_minus:
            case ILLang.op_deallocate:
            case ILLang.op_referenceType:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_sizeof:
            case ILLang.op_not:
            case ILLang.op_pointerType:
            case ILLang.op_fieldAccess:
                addUsedSymbolsInExpr(
                        expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_boolCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
            case ILLang.op_strings:
            case ILLang.op_realCst:
            case ILLang.op_bitCst:
            case ILLang.op_letter:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_metavar:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_declarators:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
                break;
            case ILLang.op_nameEq:
                if (ILUtils.isIdentOf(expression.down(1), new String[]{"len", "kind"}, false)) {
                    addUsedSymbolsInExpr(expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            case ILLang.op_do:
            case ILLang.op_arrayConstructor:
            case ILLang.op_dimColons:
            case ILLang.op_equivalence:
            case ILLang.op_varDeclarations:
            case ILLang.op_modifiers: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    addUsedSymbolsInExpr(sons[i], toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_call:
                addUsedSymbolsInExpr(ILUtils.getArguments(expression), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(ILUtils.getCalledName(expression), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_arrayType:
            case ILLang.op_pointerAccess:
            case ILLang.op_concatIdent:
                addUsedSymbolsInExpr(expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_constructorCall:
                addUsedSymbolsInExpr(expression.down(3), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_complexConstructor:
            case ILLang.op_recordType:
                // For now, we don't go inside a composite type, but when we do, don't forget
                // to *not* consider as used the reference to other types if they are named as "struct SomeType"
            case ILLang.op_unionType:
            case ILLang.op_enumType:
            case ILLang.op_variableArgList:
            case ILLang.op_address:
            case ILLang.op_include:
            case ILLang.op_stop:
                break;
            case ILLang.op_unary:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_module:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_varDeclaration: {
                addUsedSymbolsInExpr(
                        expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                Tree[] declarators = expression.down(3).children();
                for (Tree declarator : declarators) {
                    if (declarator.opCode() == ILLang.op_assign ||
                            declarator.opCode() == ILLang.op_pointerAssign) {
                        addUsedSymbolsInExpr(declarator.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                    }
                }
                break;
            }
            case ILLang.op_typeDeclaration: {
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            }
            case ILLang.op_constDeclaration:
                addUsedSymbolsInExpr(
                        expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                //WARNING: this case continues into next case:
            case ILLang.op_accessDecl:
            case ILLang.op_nameList: {
                Tree[] declarators = expression.down(2).children();
                for (Tree declarator : declarators) {
                    addUsedSymbolsInExpr(
                            declarator, toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_common: {
                // Warning: for a COMMON, and only when "strict" is false, we say that the variables appearing in the list
                // need *not* be declared *before* the common (Fortran compilers accept that!)
                // Only variables inside those top-level variables (e.g. sizes) must be declared beforehand.
                // When "strict" is true, then all variables in the COMMON are in its "usedSymbols".
                Tree[] declarators = expression.down(2).children();
                for (Tree declarator : declarators) {
                    if (declarator.opCode() == ILLang.op_arrayDeclarator) {
                        if (strict) {
                            addUsedSymbolsInExpr(declarator.down(1), toUsedSymbols,
                                    symbolTable, undeclaredSymbols, top, strict);
                        }
                        addUsedSymbolsInExpr(declarator.down(2), toUsedSymbols,
                                symbolTable, undeclaredSymbols, top, strict);
                    } else {
                        if (strict) {
                            addUsedSymbolsInExpr(declarator, toUsedSymbols,
                                    symbolTable, undeclaredSymbols, top, strict);
                        }
                    }
                }
                break;
            }
            case ILLang.op_varDimDeclaration:
            case ILLang.op_intrinsic:
            case ILLang.op_external: {
                Tree[] declarators = expression.children();
                for (Tree declarator : declarators) {
                    addUsedSymbolsInExpr(
                            declarator, toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_arrayTriplet:
            case ILLang.op_ifExpression:
            case ILLang.op_allocate:
                addUsedSymbolsInExpr(
                        expression.down(1), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(3), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_ioCall:
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(3), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            case ILLang.op_data: {
                Tree[] sons = expression.down(1).children();
                for (int i = sons.length - 1; i >= 0; --i) {
                    addUsedSymbolsInExpr(sons[i], toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_save:
            case ILLang.op_argDeclarations:
            case ILLang.op_declarations:
            case ILLang.op_expressions: {
                Tree[] sons = expression.children();
                for (int i = sons.length - 1; i >= 0; --i) {
                    addUsedSymbolsInExpr(sons[i], toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                }
                break;
            }
            case ILLang.op_function:
                // trouver les types, constantes:
                addUsedSymbolsInExpr(
                        expression.down(2), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(3), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                addUsedSymbolsInExpr(
                        expression.down(5), toUsedSymbols, symbolTable, undeclaredSymbols, top, strict);
                break;
            default:
                TapEnv.toolWarning(-1, "(Get used symbols) Unexpected operator: " + expression.opName());
                break;
        }
    }

    public boolean isDifferentiableSymbolDecl(int diffKind, boolean diffConstants) {
        return ((diffConstants
                || (!isA(SymbolTableConstants.CONSTANT)
                && !hasModifier("constant")
                //[llh] "const" doesn't mean passive !!
                // const modifier for a formal parameter means "in" parameter (read only)
                // else const modifier means constant
                && !(isCconst()
                && isA(SymbolTableConstants.VARIABLE)
                && ((VariableDecl) this).formalArgRank == SymbolTableConstants.NOT_A_FORMAL_ARG)))
                &&
                (type() == null
                        || type().wrappedType == null
                        || TypeSpec.isDifferentiableType(type().wrappedType)));
    }

    public boolean isActiveSymbolDecl() {
        boolean doActivity = TapEnv.doActivity();
        if (doActivity) {
            return isActive();
        } else {
            int diffKind = TapEnv.diffKind();
            return this.isDifferentiableSymbolDecl(diffKind, true);
        }
    }

    protected Instruction fromInclude() {
        return fromInclude;
    }

    protected void setFromInclude(Instruction fromInc) {
        fromInclude = fromInc;
    }

    /**
     * isActive true means: for this SymbolDecl, there exists a derivative,
     * i.e. another symbolDecl which contains the derivative of this one.
     */
    public boolean isActive() {
        return toIsActive.get();
    }

    public void setActive(boolean act) {
        toIsActive.set(act);
    }

    public void setActive() {
        toIsActive.set(true);
        if (this instanceof VariableDecl
                && this.importedFrom != null
                && !this.importedFrom.first.isActive()) {
            this.importedFrom.first.setActive();
        }
    }

    public static boolean sameOriginDecl(SymbolDecl decl1, SymbolDecl decl2) {
        while (decl1.importedFrom!=null && decl1.importedFrom.first!=null) decl1 = decl1.importedFrom.first ;
        while (decl2.importedFrom!=null && decl2.importedFrom.first!=null) decl2 = decl2.importedFrom.first ;
        return decl1==decl2 ;
    }

    public void shareActivity(SymbolDecl model) {
// if ("p".equals(symbol)) {
//   System.out.println("SHAREACTIVITY "+model+" INTO "+this) ;
// }
        toIsActive = model.toIsActive;
    }

    public void shareActivity(ToBool sharedActivity) {
// if ("p".equals(symbol)) {
// System.out.println("SHAREACTIVITY INTO "+this) ;
// new Throwable().printStackTrace() ;
// }
        toIsActive = sharedActivity;
    }

    public int kind() {
        return kind;
    }

    public TapList<SymbolDecl> dependsOn() {
        return dependsOn;
    }

    public void setConstantKind() {
        kind = SymbolTableConstants.CONSTANT;
    }

    /**
     * @return true when this SymbolDecl is a target
     * and its type matches the given "modelTypeSpec".
     */
    public boolean isTarget(WrapperTypeSpec modelTypeSpec) {
        return false;
    }

    public boolean isReference() {
        return isReference ;
    }

    public void setReference() {
        isReference = true ;
    }

    public boolean isCconst() {
        return TapList.containsEquals(accessInfo, "const");
    }

    public boolean isShared() {
        return isShared;
    }

    public void setShared() {
        isShared = true;
    }

    public TapList<String> extraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(TapList<String> info) {
        extraInfo = info;
    }

    public void addExtraInfo(TapList<String> info) {
        this.extraInfo = addInfosInList(this.extraInfo, info, this);
    }

    public void setSystemPredefined() {
        addExtraInfo(new TapList<>("tapenade_system_predefined", null));
    }

    public boolean isSystemPredefined() {
        return TapList.containsEquals(extraInfo, "tapenade_system_predefined");
    }

    public boolean isOptional() {
        return TapList.containsEquals(extraInfo, "optional");
    }

    public boolean isExternal() {
        return TapList.containsEquals(extraInfo, "external");
    }

    public boolean isExtern() {
        return TapList.containsEquals(extraInfo, "extern");
    }

    public boolean isPrivate() {
        return TapList.containsEquals(extraInfo, "private");
    }

    public boolean isPublic() {
        return TapList.containsEquals(extraInfo, "public");
    }

    public boolean isProtected() {
        return TapList.containsEquals(extraInfo, "protected");
    }

    public boolean hasModifier(String modifier) {
        return TapList.containsEquals(extraInfo, modifier);
    }

    /**
     * Collects into toUsedTrees all expressions that are needed for the
     * declaration of this SymbolDecl.
     *
     * @param toUsedTrees TapList of used Tree
     * @param toDejaVu    TapList of TypeSpec
     */
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
    }

    /**
     * Build a rather shallow copy of this SymbolDecl, suitable for SymbolTable.copy()
     * TypeDecl's are not copied. Remember the copy in this
     * symbolDecl's "copySymbolDecl" field, so as to build a single copy.
     */
    protected SymbolDecl copy(TapList<TapPair<SymbolTable, SymbolTable>> associationsST,
                              UnitStorage<Unit> associationsUnits) {
        if (copySymbolDecl != null) {
            return copySymbolDecl;
        }
        SymbolDecl copyDecl;
        Unit origUnit;
        Unit copiedUnit;
        switch (kind) {
            case SymbolTableConstants.VARIABLE: {
                VariableDecl copy = new VariableDecl();
                VariableDecl model = (VariableDecl) this;
                copy.setType(model.type());
                copy.setInitializationTree(model.getInitializationTree());
                copy.constantValue = model.constantValue;
                copy.formalArgRank = model.formalArgRank;
                copy.adoptZones(model);
                copy.isReturnVarFromFunction = model.isReturnVarFromFunction;
                copy.isATrueSymbolDecl = model.isATrueSymbolDecl;
                // attention, il faudrait plutot recuperer la copie de l'Instruction
                copy.setInstruction(model);
                copy.setNullInstructionIfNone();
                if (fromInclude != null) {
                    Instruction copyFromInclude = Instruction.fromIncludeCopy(fromInclude, true);
                    copy.setFromInclude(fromInclude);
                }
                copyDecl = copy;
                copyDecl.shareActivity(this);
                break;
            }
            case SymbolTableConstants.FUNCTION: {
                FunctionDecl copy = new FunctionDecl();
                FunctionDecl model = (FunctionDecl) this;
                origUnit = model.unit();
                copiedUnit = origUnit;
                if (associationsUnits != null) {
                    copiedUnit = associationsUnits.retrieve(origUnit);
                    if (copiedUnit == null) {
                        copiedUnit = origUnit;
                    }
                }
                copy.setUnit(copiedUnit);
                copy.formalArgRank = model.formalArgRank;
                copy.setZones(model.zones());
                copy.isATrueSymbolDecl = model.isATrueSymbolDecl;
                // attention, il faut recuperer la copie de l'Instruction
                copy.setInstruction(model);
                copy.setNullInstructionIfNone();
                if (associationsST != null) {
                    copy.definitionSymbolTable = TapList.cassq(model.definitionSymbolTable, associationsST);
                } else {
                    copy.definitionSymbolTable = model.definitionSymbolTable;
                }
                copyDecl = copy;
                copyDecl.shareActivity(this);
                break;
            }
            case SymbolTableConstants.INTERFACE: {
                InterfaceDecl model = (InterfaceDecl) this;
                InterfaceDecl copy = new InterfaceDecl();
                copy.nameTree = model.nameTree;
                copy.symbol = model.symbol;
                copy.usefulName = model.usefulName;
                copy.contents = model.contents;
                copy.containerUnit = model.containerUnit;
                TapList<FunctionDecl> inFunctionDecls = model.functionDecls;
                TapList<FunctionDecl> hdFunctionDecls = new TapList<>(null, null);
                TapList<FunctionDecl> tlFunctionDecls = hdFunctionDecls;
                while (inFunctionDecls != null) {
                    origUnit = inFunctionDecls.head.unit();
                    copiedUnit = origUnit;
                    if (associationsUnits != null) {
                        copiedUnit = associationsUnits.retrieve(origUnit);
                        if (copiedUnit == null) {
                            copiedUnit = origUnit;
                        }
                    }
                    tlFunctionDecls = tlFunctionDecls.placdl(
                            new FunctionDecl(ILUtils.build(ILLang.op_ident, origUnit.name()), copiedUnit));
                    inFunctionDecls = inFunctionDecls.tail;
                }
                copy.functionDecls = hdFunctionDecls.tail;
                TapList<Unit> inInterfaceUnits = model.interfaceUnits();
                TapList<Unit> hdInterfaceUnits = new TapList<>(null, null);
                TapList<Unit> tlInterfaceUnits = hdInterfaceUnits;
                while (inInterfaceUnits != null) {
                    origUnit = inInterfaceUnits.head;
                    copiedUnit = origUnit;
                    if (associationsUnits != null) {
                        copiedUnit = associationsUnits.retrieve(origUnit);
                        if (copiedUnit == null) {
                            copiedUnit = origUnit;
                        }
                    }
                    if (copiedUnit.rank() != -1) {
                        tlInterfaceUnits = tlInterfaceUnits.placdl(copiedUnit);
                    }
                    inInterfaceUnits = inInterfaceUnits.tail;
                }
                copy.setInterfaceUnits(hdInterfaceUnits.tail);
                copy.functionNames = TapList.append(model.functionNames, null);
                if (associationsST != null) {
                    SymbolTable assocOfDefinitionSymbolTable =
                        TapList.cassq(model.definitionSymbolTable, associationsST);
                    if (assocOfDefinitionSymbolTable!=null) {
                        copy.definitionSymbolTable = assocOfDefinitionSymbolTable ;
                    } else {
                        // this is a dirty recovery: it may happen that we have been given a limited
                        // associationsST that does not know about assoc of the original definitionSymbolTable
                        // In that case we keep the original definitionSymbolTable, better than nothing.
                        copy.definitionSymbolTable = model.definitionSymbolTable;
                    }
                } else {
                    copy.definitionSymbolTable = model.definitionSymbolTable;
                }
                copy.setInstruction(model);
                copy.setNullInstructionIfNone();
                copyDecl = copy;
                copyDecl.shareActivity(this);
                break;
            }
            case SymbolTableConstants.TYPE:
            default:
                // Default: do not copy.
                copyDecl = this;
        }
        if (copyDecl != this) {
            copyDecl.symbol = symbol;
            copyDecl.kind = kind;
            copyDecl.accessInfo = accessInfo;
            copyDecl.fromImplicitType = fromImplicitType;
            copyDecl.isShared = isShared;
// The following could (?) be done right now, but at present it is done
//  during a later step, see calls to SymbolTable.updateImportedFroms().
//             if (associationsST==null || importedFrom==null) {
//                 copyDecl.importedFrom = importedFrom ;
//             } else {
// System.out.println("COPYING SYMBOLDECL "+this+" IMPORTEDFROM "+importedFrom) ;
// System.out.println("  WITH ASSOCIATIONST:");
// SymbolTable.traceShowAssociationsST(associationsST) ;
// System.out.println("  FINDING:"+importedFrom.second.getExistingCopy(associationsST)) ;
//                 copyDecl.importedFrom =
//                     new TapPair<>(importedFrom.first.copySymbolDecl,
//                                   importedFrom.second.getExistingCopy(associationsST)) ;
//             }
	    copyDecl.importedFrom = importedFrom; // See later call to SymbolTable.updateImportedFroms().
            if (this.isReference()) copyDecl.setReference() ;
            if (extraInfo != null) {
                TapList<String> origExtraInfo = extraInfo;
                TapList<String> copyExtraInfo = new TapList<>(null, null);
                TapList<String> tlCopyExtraInfo = copyExtraInfo;
                while (origExtraInfo != null) {
                    String head = origExtraInfo.head;
                    tlCopyExtraInfo = tlCopyExtraInfo.placdl(head);
                    origExtraInfo = origExtraInfo.tail;
                }
                copyDecl.extraInfo = copyExtraInfo.tail;
            }
            copySymbolDecl = copyDecl;
        }
        return copyDecl;
    }

    protected void copyLinksIntoCopy(SymbolDecl copyDecl,
                                     TapList<TapPair<SymbolTable, SymbolTable>> associationsST) {
        if (copyDecl != this) {
                TapList<SymbolDecl> oldDependsOn = dependsOn;
                TapList<SymbolDecl> newDependsOn = new TapList<>(null, null);
                TapList<SymbolDecl> tlNewDependsOn = newDependsOn;
                SymbolDecl newDepend;
                while (oldDependsOn != null) {
                    newDepend = oldDependsOn.head;
                    if (newDepend != null && newDepend.copySymbolDecl != null) {
                        newDepend = newDepend.copySymbolDecl;
                    }
                    tlNewDependsOn = tlNewDependsOn.placdl(newDepend);
                    oldDependsOn = oldDependsOn.tail;
                }
                copyDecl.dependsOn = newDependsOn.tail;
        }
    }

    public WrapperTypeSpec type() {
        return null;
    }

    public TapList zones() {
        return null;
    }

    public void setZones(TapList intTree) {
    }

    public void accumulateZones(TapList intTree) {
    }

    public int formalArgRankOrResultZero() {
	return -1 ;
    }

    /** Assuming that this SymbolDecl has associated memory, allocate the zones for this memory,
     * and create the corresponding ZoneInfo's. When these zones are already allocated/created,
     * it means this SymbolDecl is a global, and we must tell the enclosing importsSymbolTable to
     * later build a proxy ZoneInfo for it. */
    public void allocateZones(ZoneAllocator zoneAllocator, SymbolTable symbolTable, Unit declarationUnit) {
	int argRank = formalArgRankOrResultZero() ;
	if (zones() == null) {
            ZoneInfoAccessElements access =
                new ZoneInfoAccessElements(this, ILUtils.build(ILLang.op_ident, symbol), symbolTable) ;
	    TapList regionZones =
		zoneAllocator.allocateZones(type(), -1, extraInfo(), new TapList<>(access, null),
                                            type(), argRank, false, false, false, null, symbolTable, null) ;
	    accumulateZones(regionZones);
	} else {
	    if (argRank != -1) {
		// If this is a formal arg, and its zone is
		// already allocated, we must write the formal
		// argument rank into this zone's ZoneInfo
		zoneAllocator.addArgRank(zones(), argRank);
	    }
	}
    }

    protected void recomputeDependsOn(SymbolTable symbolTable) {
    }

    /**
     * @return true when this SymbolDecl is of the given kind "testKind"
     * The kind may be SYMBOL, VARIABLE, FIELD, FUNCTION, TYPE, CONSTANT, FUNCNAME, INTERFACE, CLASS, NAMESPACE.
     */
    public boolean isA(int testKind) {
        return testKind == SymbolTableConstants.SYMBOL || testKind == kind;
    }

    protected boolean hasPrivateModifier(TapList<SymbolDecl> addSymbolDecls) {
        boolean result = false;
        SymbolDecl addSymbolDecl = TapList.findNamedSymbolDecl(addSymbolDecls, symbol);
        if (addSymbolDecl != null) {
            result = addSymbolDecl.isPrivate();
        }
        return result;
    }

    protected boolean hasPublicModifier(TapList<SymbolDecl> addSymbolDecls) {
        boolean result = false;
        SymbolDecl addSymbolDecl = TapList.findNamedSymbolDecl(addSymbolDecls, symbol);
        if (addSymbolDecl != null) {
            result = addSymbolDecl.isPublic();
        }
        return result;
    }

    protected boolean hasProtectedModifier(TapList<SymbolDecl> addSymbolDecls) {
        boolean result = false;
        SymbolDecl addSymbolDecl = TapList.findNamedSymbolDecl(addSymbolDecls, symbol);
        if (addSymbolDecl != null) {
            result = addSymbolDecl.isProtected();
        }
        return result;
    }

    /** The NewSymbolHolder's for the various diff versions of this SymbolDecl, one per diffSort. */
    private NewSymbolHolder[] diffSymbolHolders = null ;

    /**
     * Puts the NewSymbolHolder "nSH" as the differentiated symbol of this SymbolDecl,
     * for the given diffSort and for the given ActivityPattern and for the given replica number.
     * This is the default implementation, that uses only "diffSort", not "pattern" not "replica".
     * "diffSort" is here because there may be different coexisting derivative symbols for
     * a given original symbol, for example F_FWD, F_BWD, F_B for a given function F.
     * "pattern" is used only for FunctionDecl's.
     * "replica" is used only for VariableDecl's.
     */
    public void setDiffSymbolHolder(int diffSort, ActivityPattern pattern, int replica, NewSymbolHolder nSH) {
        //TODO?: Split into setOrigCopySymbolHolder and setDiffSymbolHolder
        if (diffSymbolHolders == null) {
            diffSymbolHolders = new NewSymbolHolder[TapEnv.MAX_DIFF_SORTS];
        }
        diffSymbolHolders[diffSort] = nSH;
    }

    /**
     * @return the NewSymbolHolder that has been stored by setDiffSymbolHolder()
     * as the differentiated symbol of this SymbolDecl for the given diffSort.
     */
    public NewSymbolHolder getDiffSymbolHolder(int diffSort, ActivityPattern pattern, int replica) {
        //TODO?: Split into getOrigCopySymbolHolder and  getDiffSymbolHolder
        return (diffSymbolHolders==null ? null : diffSymbolHolders[diffSort]) ;
    }

    public boolean hasDiffSymbolHolders() {
        return diffSymbolHolders != null;
    }

    /**
     * For every NewSymbolHolder that is stored in this SymbolDecl (in provision of
     * the moment when this Symboldecl will receive its finalname), replace it with newNSH if it was oldNSH.
     * This is used when oldNSH is absorbed by newNSH.
     */
    protected void replaceDiffSymbolHolder(NewSymbolHolder oldNSH, NewSymbolHolder newNSH) {
        if (diffSymbolHolders != null) {
            for (int i = diffSymbolHolders.length - 1; i >= 0; --i) {
                if (diffSymbolHolders[i] == oldNSH) {
                    diffSymbolHolders[i] = newNSH;
                }
            }
        }
    }

    protected void eraseDiffSymbolHolder(int diffSort) {
        if (diffSymbolHolders != null) {
            diffSymbolHolders[diffSort] = null;
        }
    }

    /**
     * Declare that this SymbolDecl already has an existing declaration,
     * made by Instruction "instr" that is located in the given "code".
     *
     * @param instr The Instruction that contains the existing declaration of SymbolDecl.
     */
    public void setInstruction(Instruction instr) {
        if (instr != null && instr.tree != null) {
            Tree instrTree = instr.tree;
            //[llh 14/05/2013] strange special case for accessDecl(, typeDeclaration()) :
            // --> Because IL syntax sometimes places a op_typeDeclaration as 2nd child
            //     of a op_accessDecl (This is ugly and MUST be changed!)
            if (instrTree.opCode() == ILLang.op_accessDecl && instrTree.down(2).opCode() == ILLang.op_typeDeclaration) {
                instrTree = instrTree.down(2);
            }
            switch (instrTree.opCode()) {
                case ILLang.op_varDeclaration:
                case ILLang.op_typeDeclaration:
                case ILLang.op_parallelRegion:
                    hasMainInstruction = true;
                    break;
                case ILLang.op_none:
                    hasMainInstruction = true;
                    hasNoneInstruction = true;
                    break;
                case ILLang.op_varDimDeclaration:
                    hasVarDimInstruction = true;
                    break;
                case ILLang.op_common:
                    hasCommonInstruction = true;
                    hasCommonInstructionWithArrayDeclarator =
                            ILUtils.containsArrayDeclarator(this, instrTree)
                                    || hasCommonInstructionWithArrayDeclarator;
                    break;
                case ILLang.op_external:
                    hasExternalInstruction = true;
                    break;
                case ILLang.op_intrinsic:
                    hasIntrinsicInstruction = true;
                    break;
                case ILLang.op_save:
                    hasSaveInstruction = true;
                    break;
                case ILLang.op_interfaceDecl:
                    hasMainInstruction = true;
                    hasInterfaceInstruction = true;
                    break;
                case ILLang.op_accessDecl:
                    if (instrTree.down(1).opCode() == ILLang.op_ident) {
                        switch (instrTree.down(1).stringValue()) {
                            case "optional":
                                hasOptionalInstruction = true;
                                break;
                            case "allocatable":
                            case "target":
                                hasAllocatableInstruction = true;
                                hasAllocatableInstructionWithArrayDeclarator =
                                        ILUtils.containsArrayDeclarator(this, instrTree)
                                                || hasAllocatableInstructionWithArrayDeclarator;
                                break;
                            case "pointer":
                                hasPointerInstruction = true;
                                break;
                            default:
                                hasAccessDeclModifier = new TapList<>(instrTree.down(1).stringValue(), hasAccessDeclModifier);
                                break;
                        }
                    }
                    break;
                case ILLang.op_call: {
                    hasMainInstruction = true;
                    break;
                }
                default:
                    break;
            }
        }
    }

    public void setInstruction(SymbolDecl model) {
        hasMainInstruction = model.hasMainInstruction || hasMainInstruction;
        hasNoneInstruction = model.hasNoneInstruction || hasNoneInstruction;
        hasVarDimInstruction = model.hasVarDimInstruction || hasVarDimInstruction;
        hasCommonInstruction = model.hasCommonInstruction || hasCommonInstruction;
        hasCommonInstructionWithArrayDeclarator = model.hasCommonInstructionWithArrayDeclarator
                || hasCommonInstructionWithArrayDeclarator;
        hasExternalInstruction = model.hasExternalInstruction || hasExternalInstruction;
        hasIntrinsicInstruction = model.hasIntrinsicInstruction || hasIntrinsicInstruction;
        hasSaveInstruction = model.hasSaveInstruction || hasSaveInstruction;
        hasOptionalInstruction = model.hasOptionalInstruction || hasOptionalInstruction;
        hasPointerInstruction = model.hasPointerInstruction || hasPointerInstruction;
        hasAllocatableInstruction = model.hasAllocatableInstruction || hasAllocatableInstruction;
        hasAllocatableInstructionWithArrayDeclarator = model.hasAllocatableInstructionWithArrayDeclarator
                || hasAllocatableInstructionWithArrayDeclarator;
        hasAccessDeclModifier = model.hasAccessDeclModifier;
        hasInterfaceInstruction = model.hasInterfaceInstruction;
    }

    public void setMainDeclInstr(boolean val) {
        hasMainInstruction = val;
    }

    public void setExternalInstr(boolean val) {
        hasExternalInstruction = val;
    }

    public void setInterfaceInstr(boolean val) {
        hasInterfaceInstruction = val;
        hasMainInstruction = val;
    }

    public void setCommonInstr(boolean val) {
        hasCommonInstruction = val;
    }

    public void setCommonWithArrayDeclaratorInstr(boolean val) {
        hasCommonInstructionWithArrayDeclarator = val;
    }

    protected void setNullInstructionIfNone() {
        if (hasNoneInstruction) {
            hasMainInstruction = false;
            hasNoneInstruction = false;
        }
    }

    public void setNullInstruction() {
        hasMainInstruction = false;
        hasNoneInstruction = false;
        hasVarDimInstruction = false;
        hasCommonInstruction = false;
        hasCommonInstructionWithArrayDeclarator = false;
        hasExternalInstruction = false;
        hasIntrinsicInstruction = false;
        hasSaveInstruction = false;
        hasOptionalInstruction = false;
        hasPointerInstruction = false;
        hasAllocatableInstruction = false;
        hasAllocatableInstructionWithArrayDeclarator = false;
        hasAccessDeclModifier = null;
        hasInterfaceInstruction = false;
    }

    public void setNoneInstruction() {
        hasMainInstruction = true;
        hasNoneInstruction = true;
    }

    public boolean hasMainInstruction() {
        return hasMainInstruction || hasNoneInstruction || hasInterfaceInstruction;
    }

    public boolean hasOtherInstruction() {
        return hasVarDimInstruction
                || hasCommonInstruction
                || hasCommonInstructionWithArrayDeclarator
                || hasExternalInstruction
                || hasIntrinsicInstruction
                || hasSaveInstruction
                || hasOptionalInstruction
                || hasPointerInstruction
                || hasAllocatableInstruction
                || hasAllocatableInstructionWithArrayDeclarator
                || hasAccessDeclModifier != null;
    }

    public boolean hasOtherCombinableDeclarationPart() {
        return hasExternalInstruction
                || hasIntrinsicInstruction
                || hasOptionalInstruction
                || hasPointerInstruction
                || hasAllocatableInstruction
                || hasAccessDeclModifier != null;
    }

    public boolean hasInterfaceInstruction() {
        return hasInterfaceInstruction;
    }

    public boolean hasInstructionWithRootOperator(int operator, String value) {
        switch (operator) {
            case ILLang.op_varDeclaration:
            case ILLang.op_typeDeclaration:
                return hasMainInstruction;
            case ILLang.op_none:
                return hasNoneInstruction;
            case ILLang.op_varDimDeclaration:
                return hasVarDimInstruction;
            case ILLang.op_common:
                return hasCommonInstruction;
            case ILLang.op_external:
                return hasExternalInstruction;
            case ILLang.op_intrinsic:
                return hasIntrinsicInstruction;
            case ILLang.op_save:
                return hasSaveInstruction;
            case ILLang.op_interfaceDecl:
                return hasInterfaceInstruction;
            case ILLang.op_accessDecl:
                if (value != null) {
                    switch (value) {
                        case "optional":
                            return hasOptionalInstruction;
                        case "allocatable":
                        case "target":
                            return hasAllocatableInstruction;
                        case "pointer":
                            return hasPointerInstruction;
                        default:
                            return TapList.containsEquals(hasAccessDeclModifier, value);
                    }
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    public boolean hasInstructionWithRootOperatorAndArrayDeclarator(int operator) {
        if (operator == ILLang.op_common) {
            return hasCommonInstructionWithArrayDeclarator;
        } else {
            if (operator == ILLang.op_accessDecl) {
                return hasAllocatableInstructionWithArrayDeclarator;
            } else {
                return false;
            }
        }
    }

    public boolean isVarDeclared(TapList<Instruction> instructions) {
        boolean result = false;
        TapList<Instruction> insts = instructions;
        Instruction inst;
        while (!result && insts != null) {
            inst = insts.head;
            if (inst.tree != null
                    && inst.tree.opCode() == ILLang.op_varDeclaration) {
                Tree declarators = inst.tree.down(3);
                int i = declarators.length();
                int k = 1;
                while (!result && k <= i) {
                    String baseName = ILUtils.baseName(declarators.down(k));
                    if (baseName != null) {
                        result = ILUtils.baseName(declarators.down(k)).equals(symbol);
                    }
                    k = k + 1;
                }
            }
            insts = insts.tail;
        }
        return result;
    }

    public boolean isExternalDeclared(TapList<Instruction> instructions) {
        if (MPIcallInfo.isMessagePassingFunction(symbol, TapEnv.UNDEFINED)) {
            return true;
        }
        boolean result = false;
        while (instructions != null && !result) {
            result = isExternalDeclared(instructions.head);
            instructions = instructions.tail;
        }
        return result;
    }

    public boolean isExternalDeclared(Instruction instruction) {
        if (instruction == null || instruction.tree == null) {
            return hasExternalInstruction;
        }
        Tree tree = instruction.tree;
        boolean result = false;
        if (tree.opCode() == ILLang.op_external) {
            result = ILUtils.contains(tree, symbol, false);
        } else {
            if (tree.opCode() == ILLang.op_varDeclaration
                    && tree.down(2).opCode() == ILLang.op_modifiedType) {
                Tree modifiers = tree.down(2).down(1);
                result = modifiers.opCode() == ILLang.op_modifiers
                        && (ILUtils.contains(modifiers, "external", false)
                        || ILUtils.contains(modifiers, "extern", true))
                        && (ILUtils.contains(tree.down(3), symbol, true)
                        || ILUtils.containsFunctionDeclarator(tree.down(3), symbol));
            }
        }
        return result;
    }

    public boolean isIntrinsicDeclared(TapList<Instruction> instructions) {
        boolean result = false;
        while (instructions != null && !result) {
            result = isIntrinsicDeclared(instructions.head);
            instructions = instructions.tail;
        }
        return result;
    }

    public boolean isIntrinsicDeclared(Instruction instruction) {
        if (instruction == null || instruction.tree == null) {
            return hasIntrinsicInstruction;
        }
        Tree tree = instruction.tree;
        boolean result = false;
        if (tree.opCode() == ILLang.op_intrinsic) {
            result = ILUtils.contains(tree, symbol, true);
        } else {
            if (tree.opCode() == ILLang.op_varDeclaration
                    && tree.down(2).opCode() == ILLang.op_modifiedType) {
                Tree modifiers = tree.down(2).down(1);
                result = modifiers.opCode() == ILLang.op_modifiers
                        && ILUtils.contains(modifiers, "intrinsic", false)
                        && ILUtils.contains(tree.down(3), symbol, true);
            }
        }
        return result;
    }

    public boolean isContainedFunctionDeclDeclared(Unit inUnit) {
        boolean result = kind == SymbolTableConstants.FUNCTION;
        if (result) {
            Unit unit = ((FunctionDecl) this).unit();
            result = unit.upperLevelUnit() == inUnit;
        }
        return result;
    }

    /** collects into siblingDecls (must be a hatted list)
     * for each SymbolDecl's of which the given "symbolDecl" is a same-name copy built by an import (aka USE),
     * the pair of the original SymbolDecl and the SymbolTable it belongs to. */
    public static void collectImportedSiblingsDecls(SymbolDecl symbolDecl,
                                                   TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls) {
        while (symbolDecl!=null && symbolDecl.importedFrom != null) {
            SymbolDecl origSymbolDecl = symbolDecl.importedFrom.first ;
            // if renamed, stop exploration:
            if (origSymbolDecl!=null && !origSymbolDecl.symbol.equals(symbolDecl.symbol)) {
                origSymbolDecl = null ;
            }
            if (origSymbolDecl!=null) {
                collectSiblingDecl(siblingDecls, origSymbolDecl, symbolDecl.importedFrom.second) ;
            }
            symbolDecl = origSymbolDecl ;
        }
    }

    /**
     * Collects into the collection "siblingDecls" the pair of "symbolDecl"
     * and the SymbolTable "symbolDecl" belongs to.
     * @param siblingDecls  the current list of "sibling" declarations collected.
     *    It is a hatted list, ans all declarations must concern the same symbol with same name.
     *    All found declarations are grouped per owner SymbolTable, rootmost last.
     * @param symbolDecl The symbolDecl we want to collect into "siblingDecls"
     * @param symbolTable The SymbolTable that owns "symbolDecl".
     */
    public static void collectSiblingDecl(TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> siblingDecls,
                                     SymbolDecl symbolDecl, SymbolTable symbolTable) {
        TapList<TapPair<SymbolTable, TapList<SymbolDecl>>> inSiblingDecls = siblingDecls;
        TapPair<SymbolTable, TapList<SymbolDecl>> place = null;
        while (place == null && inSiblingDecls.tail != null) {
            if (inSiblingDecls.tail.head.first == symbolTable) {
                place = inSiblingDecls.tail.head;
            } else {
                inSiblingDecls = inSiblingDecls.tail;
            }
        }
        if (place == null) {
            place = new TapPair<>(symbolTable, null);
            inSiblingDecls.tail = new TapList<>(place, null);
        }
        place.second = new TapList<>(symbolDecl, place.second);
    }

    /**
     * Prints in detail the contents of this SymbolDecl, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing (not used at present).
     */
    public void dump(int indent) throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return "SymbolDecl"    //+" @"+Integer.toHexString(hashCode())
            +(isReference?" &":" ")+symbol+" of unexpected kind "+kind ;
    }

}
