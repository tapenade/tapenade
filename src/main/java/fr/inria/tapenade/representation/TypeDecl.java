/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;

/**
 * A symbol declaration object specialized for types.
 */
public final class TypeDecl extends SymbolDecl {
    public WrapperTypeSpec typeSpec;

    public TypeDecl(Tree identTree, WrapperTypeSpec typeSpec) {
        super(identTree, SymbolTableConstants.TYPE);

        this.typeSpec = typeSpec;
        setTypeDeclName(this.symbol);
    }

    public TypeDecl(String name, WrapperTypeSpec typeSpec) {
        super(name, SymbolTableConstants.TYPE);

        this.typeSpec = typeSpec;
        setTypeDeclName(this.symbol);
    }

    public TypeDecl(Tree identTree, WrapperTypeSpec typeSpec, Unit inUnit) {
        super(identTree, SymbolTableConstants.TYPE);

        this.typeSpec = typeSpec;
        setTypeDeclName(this.symbol, inUnit);
    }

    public static TypeDecl findTypeDecl(TapList<SymbolDecl> dependsOn) {
        TypeDecl result = null;
        SymbolDecl symbolDecl;
        while (dependsOn != null && result == null) {
            symbolDecl = dependsOn.head;
            if (symbolDecl != null && symbolDecl.kind == SymbolTableConstants.TYPE) {
                result = (TypeDecl) symbolDecl;
            }
            dependsOn = dependsOn.tail;
        }
        return result;
    }

    public void setTypeDeclName(String name, Unit inUnit) {
        if (typeSpec.wrappedType != null) {
            typeSpec.wrappedType.addInTypeDeclNames(inUnit, name);
        }
    }

    public String getTypeDeclName() {
        if (typeSpec.wrappedType != null) {
            return typeSpec.wrappedType.typeDeclName();
        } else {
            return null;
        }
    }

    public void setTypeDeclName(String name) {
        if (typeSpec.wrappedType != null) {
            String oldName = typeSpec.wrappedType.typeDeclName();
            if (TypeSpec.ignoreTypeDeclName(oldName)) {
                typeSpec.wrappedType.setTypeDeclName(name);
            }
        }
    }

    public TypeDecl activeTypeDecl(SymbolTable diffSymbolTable, WrapperTypeSpec activeTypeSpec, String fSuffix) {
        // TODO: this code should be vastly simplified when diff of type names is handled by VarRefDifferentiator in the standard way.
        TypeDecl diffTypeDecl = null;
        String origTypeName = this.symbol;
        if (TypeSpec.isA(this.typeSpec, SymbolTableConstants.COMPOSITETYPE)
            || TapEnv.associationByAddress()) {
            NewSymbolHolder diffSymbolHolder = this.getDiffSymbolHolder(0, null, 0) ;
            if (diffSymbolHolder!=null) {
                diffTypeDecl = diffSymbolHolder.newTypeDecl();
            } else if (activeTypeSpec!=null) {
                if (this.importedFrom!=null) {
                    diffTypeDecl =
                        ((TypeDecl)this.importedFrom.first).activeTypeDecl(diffSymbolTable, activeTypeSpec, fSuffix) ;
                }
                if (this.importedFrom!=null && origTypeName.equals(importedFrom.first.symbol)) {
                    diffSymbolHolder = ((TypeDecl)this.importedFrom.first).getDiffSymbolHolder(0, null, 0) ;
                } else {
                    // not sure about the diffKind of diffSymbolHolder
                    diffSymbolHolder =
                        new NewSymbolHolder(TapEnv.extendStringWithSuffix(origTypeName, fSuffix));
                    diffSymbolHolder.setAsType(activeTypeSpec, null);
                    diffSymbolHolder.isDerivationFrom(this, fSuffix);
                    diffTypeDecl = diffSymbolHolder.newTypeDecl();
                }
                SymbolTable basisSymbolTable = diffSymbolTable;
                if (TapEnv.associationByAddress() && diffSymbolTable!=null && this.typeSpec.isScalar()) {
                    basisSymbolTable = diffSymbolTable.findAssocAddresTypeDeclSymbolTable();
                }
                if (basisSymbolTable != null) {
                        diffSymbolHolder.addTypeDeclSymbolTable(basisSymbolTable);
                        diffSymbolHolder.makeNewRef(basisSymbolTable);
                        diffSymbolHolder.makeNewRef(diffSymbolTable);
                        diffSymbolHolder.solvingLevelMustInclude(basisSymbolTable);
                        if (TapEnv.associationByAddress() && !TapEnv.get().complexStep
                                && basisSymbolTable.basisSymbolTable() != null) {
                            int basisLanguage = basisSymbolTable.language() ;
                            if (basisLanguage==TapEnv.UNDEFINED && basisSymbolTable.unit!=null)
                                basisLanguage = basisSymbolTable.unit.language() ;
                            diffSymbolHolder.declarationLevelMustInclude(
                                    TapEnv.assocAddressDiffTypesUnit(basisLanguage).publicSymbolTable());
                        }
                }
                this.setDiffSymbolHolder(0, null, 0, diffSymbolHolder);
            }
        } else {
            WrapperTypeSpec baseTypeSpec = this.typeSpec.baseTypeSpec(true);
            TapEnv.toolWarning(-1, "(Diff of a Type): Don't know how to differentiate a " + baseTypeSpec);
        }
        return diffTypeDecl;
    }

    /**
     * Collects into toUsedTrees all expressions that are needed for the
     * declaration of this SymbolDecl.
     *
     * @param toUsedTrees TapList of used Tree
     * @param toDejaVu    TapList of TypeSpec
     */
    @Override
    public void collectUsedTrees(TapList<Tree> toUsedTrees, TapList<TypeSpec> toDejaVu) {
        if (typeSpec != null && typeSpec.wrappedType != null) {
            typeSpec.wrappedType.collectUsedTrees(toUsedTrees, toDejaVu);
        }
    }

    @Override
    public WrapperTypeSpec type() {
        return typeSpec;
    }

    @Override
    public String toString() {
        return symbol    //+" @"+Integer.toHexString(hashCode())
            + ": type."
	    + (importedFrom==null
               ? "" 
               : " (=>" + importedFrom.first+" from "
                 +(importedFrom.second==null
                   ? ("@"+Integer.toHexString(importedFrom.second.hashCode()))
                   : "null")
                 +")")
            + " stands for " + (typeSpec==null ? "null_type" : typeSpec)
            + (!isActiveSymbolDecl() ? "" : " active")
            +" depends on:"+ dependsOn()
            ;
    }

}
