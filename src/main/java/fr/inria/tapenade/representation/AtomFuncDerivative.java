/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TopDownTreeWalk;
import fr.inria.tapenade.utils.Tree;

/**
 * Object that manages the derivatives of a black-box routine,
 * intrinsic or external or other. For each routine, one such
 * object is built from a specification which is an IL Tree,
 * as described in function incorporateNewAtomFunc(Tree, SymbolTable).
 */
public final class AtomFuncDerivative {
    public Tree origPattern;
    public TapList<TapPair<Tree, Tree>> origKeyTreeList;
    public Tree factor;
    public TapList<TapPair<Tree, Tree>> factorKeyTreeList;
    public TapPair<Tree, Tree>[] paramVars;
    public Tree[] derivs;
    public TapList<TapPair<Tree, Tree>>[] derivsKeyTreeList;

    public AtomFuncDerivative(
            Tree origPattern, TapList<TapPair<Tree, Tree>> origKeyTreeList, Tree factor,
            TapList<TapPair<Tree, Tree>> factorKeyTreeList, TapPair<Tree, Tree>[] paramVars, Tree[] derivs,
            TapList<TapPair<Tree, Tree>>[] derivsKeyTreeList) {
        super();
        this.origPattern = origPattern;
        this.origKeyTreeList = origKeyTreeList;
        this.factor = factor;
        this.factorKeyTreeList = factorKeyTreeList;
        this.paramVars = paramVars;
        this.derivs = derivs;
        this.derivsKeyTreeList = derivsKeyTreeList;
    }

    /**
     * @return a copy of "tree", replacing all of its subtrees that are keys in
     * the A-list "keyTreeList" by their corresponding associated
     * values in A-list "modelKeyTreeList".
     */
    private static Tree copyReplacingKeyTrees(
            Tree tree, TapList<TapPair<Tree, Tree>> keyTreeList, TapList<TapPair<Tree, Tree>> modelKeyTreeList) {
        TapList<TapPair<Tree, Tree>> inKeyTreeList = keyTreeList;
        TapPair<Tree, Tree> keyTree;
        TapPair<Tree, Tree> foundPair;
        Tree found = null;
        while (inKeyTreeList != null) {
            keyTree = inKeyTreeList.head;
            foundPair = TapList.assqTree(keyTree.first, modelKeyTreeList);
            if (foundPair != null) {
                found = foundPair.second;
            }
            if (found == null) {
                keyTree.second = ILUtils.build(ILLang.op_none);
            } else {
                keyTree.second = ILUtils.copy(found);
            }
            inKeyTreeList = inKeyTreeList.tail;
        }
        return ILUtils.copyWatchingSubtrees(tree, keyTreeList).first;
    }

    /**
     * Incorporates the definition of derivatives of an atomic function into
     * the properties attached to that atomic function in the "symbolTable".
     * Assumes that defTree is an IL Tree of the shape:
     * binary(call(none(), ident "atomicFunctionName",
     * expressions(containing &lt;e1&gt;,&lt;e2&gt;,...))
     * &lt;factor&gt;,
     * expressions(binary(&lt;e1&gt;,none(),&lt;e'1&gt;),
     * binary(&lt;e2&gt;,none(),&lt;e'2&gt;),
     * ...))
     * Where the &lt;ei&gt; are metavariables (op_metavar) that get instantiated
     * during the pattern-match between the origPattern and the actual call,
     * and the &lt;e'i&gt; are the corresponding partial derivatives wrt &lt;ei&gt;.
     * Of course, each &lt;e'i&gt; may use some or all of the &lt;ej&gt;.
     */
    public static void incorporateNewAtomFunc(Tree defTree, Unit functionUnit) {
        if (functionUnit.isFortran()) {
            // beware of case-independence:
            Tree funcNameTree = ILUtils.getCalledName(defTree.down(1));
            funcNameTree.setValue(funcNameTree.stringValue().toLowerCase());
        }
        Tree origPattern = defTree.down(1);
        Tree factor = defTree.down(2);
        TapList<TapPair<Tree, Tree>> factorVars = null;
        Tree[] derivatives = defTree.down(3).children();
        if (factor != null && factor.opCode() == ILLang.op_none) {
            factor = null;
        }
        if (factor != null) {
            factorVars = getMetavarsFromPattern(factor, null);
        }
        TapList<TapPair<Tree, Tree>> origKeyTreeList = getMetavarsFromPattern(origPattern, null);
        origKeyTreeList = new TapList<>(new TapPair<>(ILUtils.build(
                ILLang.op_metavar, "TOP_EXPRESSION"), null), origKeyTreeList);
        int nbVars = derivatives.length;
        TapPair<Tree, Tree>[] paramVars = new TapPair[nbVars];
        Tree[] derivs = new Tree[nbVars];
        TapList<TapPair<Tree, Tree>>[] derivsVars = new TapList[nbVars];
        Tree varTree;
        for (int i = 0; i < nbVars; i++) {
            varTree = derivatives[i].down(1);
            paramVars[i] = TapList.assqTree(varTree, origKeyTreeList);
            derivs[i] = derivatives[i].down(3);
            derivsVars[i] = getMetavarsFromPattern(derivs[i], null);
        }
        functionUnit.diffInfo =
                new AtomFuncDerivative(
                        origPattern, origKeyTreeList, factor, factorVars, paramVars, derivs,
                        derivsVars);
    }

    public static TapList<TapPair<Tree, Tree>> getMetavarsFromPattern(
            Tree tree, TapList<TapPair<Tree, Tree>> keyTreeList) {
        if (tree.opCode() == ILLang.op_metavar) {
            return new TapList<>(new TapPair<>(tree, null), keyTreeList);
        } else {
            Tree[] subTrees = tree.children();
            if (subTrees != null) {
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    keyTreeList =
                            getMetavarsFromPattern(subTrees[i], keyTreeList);
                }
            }
            return keyTreeList;
        }
    }

    public void matches(Tree tree) {
        origKeyTreeList.head.second = tree;
        ILUtils.matches(tree, origPattern, origKeyTreeList);
    }

    public Tree[] getParamExprs() {
        Tree[] paramExprs = new Tree[paramVars.length];
        for (int i = paramVars.length - 1; i >= 0; i--) {
            paramExprs[i] = paramVars[i].second;
        }
        return paramExprs;
    }

    public Tree buildPartialDerivative(int i) {
        Tree result = buildPartialWithoutFactor(i);
        if (factor != null) {
            result =
                    ILUtils.buildSmartMulDiv(
                            buildDerivativeFactor(), 1, result);
        }
        return result;
    }

    private Tree buildDerivativeFactor() {
        return copyReplacingKeyTrees(factor, factorKeyTreeList, origKeyTreeList);
    }

    private Tree buildPartialWithoutFactor(int i) {
        return copyReplacingKeyTrees(
                derivs[i], derivsKeyTreeList[i], origKeyTreeList);
    }

    public TapList<Tree> getDiffCallNames() {
        TapList<Tree> result = null;
        Tree tree;
        Tree currentTree;
        String functionName = ILUtils.getCalledNameString(origPattern);
        for (Tree deriv : derivs) {
            tree = deriv;
            for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd(); i.advance()) {
                currentTree = i.get();
                if (currentTree.opCode() == ILLang.op_call) {
                    currentTree = ILUtils.getCalledName(currentTree);
                    if (currentTree.opCode() == ILLang.op_ident
                            && !currentTree.stringValue().equals(functionName)) {
                        result = new TapList<>(ILUtils.copy(currentTree), result);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String strDerivs = "";
        for (int i = derivs.length - 1; i >= 0; --i) {
            strDerivs = ILUtils.toString(derivs[i]) + strDerivs;
            if (i != 0) {
                strDerivs = " ; " + strDerivs;
            }
        }
        String strParamVars = "";
        for (int i = paramVars.length - 1; i >= 0; --i) {
            strParamVars = paramVars[i] + strParamVars;
            if (i != 0) {
                strParamVars = " ; " + strParamVars;
            }
        }
        return "Jac of " + ILUtils.toString(origPattern) + "->" + factor + " factor [" + strDerivs + "] [" + strParamVars + ']';
    }
}
