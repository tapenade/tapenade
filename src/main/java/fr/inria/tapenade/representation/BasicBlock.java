/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Standard plain Block ("basic block") of a Flow Graph,
 * not a loop header, nor a Unit's entry or exit block.
 */
public final class BasicBlock extends Block {

    /**
     * Creates a new BasicBlock, with the given "symbolTable",
     * and registering it into the list of created Blocks "allBlocks".
     */
    public BasicBlock(SymbolTable symbolTable, TapList<Instruction> parallelControls,
                      TapList<Block> allBlocks) {
        super(symbolTable, parallelControls, allBlocks);
    }

    /**
     * Prints a short reference to this BasicBlock onto TapEnv.curOutputStream().
     */
    @Override
    public void cite() throws java.io.IOException {
        TapEnv.print("BB(" + rank + ')');
    }

    @Override
    public String toString() {
        return "B" + super.toString(); //+" @"+Integer.toHexString(hashCode());
    }

}
