Reading-in the input IL ASTs - <span style="color:green">**<Step 0>**</span>
============================================================================

* Files:

`FlowGraphBuilder FGThreads FlowGraphNormalizer LabelHeap`

```
       Tapenade.loadGeneralLibraries()
       CallGraph.readILstream()
       --> FlowGraphBuilder.build()
           --> FlowGraphBuilder.treeStream2FlowGraph()
           --> unit.makeFlowGraph()                    
```

At the very beginning of this step, Tapenade reads special
(user-modifiable) files that provide information about standard
procedures (Intrinsics, Externals\...) Done by
`Tapenade.loadGeneralLibraries()`, calling class `GeneralLibReader`.
Information is about number and data-types of arguments (params,
side-effect, or globals), and about the effect of the procedure on its
arguments (InOut, differentiable dependencies\...) Information can also
provide expressions for the partial derivatives of the procedure.
Information can also provide code to expand each procedure call in-line
(see below)

Then the AST's of the actual source code are read
(`CallGraph.readILstream()\rightarrowFlowGraphBuilder.build()`) The
input AST's arrive through a `BufferedReader` stream. Tree operators are
seen before tree children trees. Atomic tree operators are seen before
the atomic tree's value. Just after the tree of the last child of a
List-arity tree, the Stream spits an endOfList \"operator\".
Progressively, as the AST's are read from the Streams, actions are done
that build the IR. This is not done through a callback nor a visitor
mechanism: instead, a driver routine explicitly pops data from the
current Stream, and accordingly progressively builds the IR. This driver
handles its context, i.e.

-   The current nesting of files/classes/methods/functions around the
    current point in the AST Stream.

-   The current nesting of namespaces

-   The `SymbolTable` corresponding to the current point

When the driver reaches the body of a procedure definition, the IR
building is subcontracted to `FlowGraphBuilder.treeStream2FlowGraph()`,
that manages an additional context about the threads of control that
reach the current point in the AST stream. Holds in particular:

-   When in a procedure, the current nest of control operators
    controlling the current point.

-   When in a procedure, the current list of Control-Flow arriving to
    this point (argument FGThreads threads)

Each time the driver has completed eating a complete procedure, it runs
a normalization step (`unit.makeFlowGraph()`) that solves computed
GOTOs, removes empty `Block`'s, groups successive `Block`'s, groups
redundant Flow Arrows, etc.

