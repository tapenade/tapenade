/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/**
 * Container for an atomic instruction in a Block in a Flow Graph.
 */
public final class Instruction {

    /**
     * The instruction's Abstract Syntax Tree.
     */
    public Tree tree;

    /**
     * The "line" comments before this instruction,
     * e.g. comments starting with // and ending with newline.
     */
    public Tree preComments;

    /**
     * The "line" comments after this instruction,
     * e.g. comments starting with // and ending with newline.
     */
    public Tree postComments;

    /**
     * The "block" comments before this instruction, which are
     * comments starting with a special code and ending with another special code.
     */
    public Tree preCommentsBlock;

    /**
     * The "block" comments after this instruction, which are
     * comments starting with a special code and ending with another special code.
     */
    public Tree postCommentsBlock;

    /**
     * The basic block that contains this instruction.
     */
    public Block block;

    /**
     * The list of Tapenade Directive's attached to this instruction, if any.
     */
    public TapList<Directive> directives;

    /**
     * The deepest instruction syntactically around this instruction, if any.
     * This is the control instruction of the deepest syntactic construct that
     * contains this instruction.
     */
    public Instruction syntaxController;

    /** When not lost by parsing, the line number in the source file. 0 otherwise. */
    public int lineNo = 0 ;

    /**
     * Set "isPhantom" to true to ensure this Instruction will not be shown.
     */
    public boolean isPhantom;
    /**
     * List of all the modifications brought by this instruction
     * on the current pointers destinations info. List of TapPair's:<ul>
     * <li> first : singleton TapIntList of pointer row index </li>
     * <li> second: BoolVector of the new possible destinations </li>
     * </ul>
     */
    public TapList<TapPair<TapIntList, BoolVector>> pointerDestsChanges;
    /**
     * Used during differentiation to mark that this instruction
     * has been differentiated (Specific to AD).
     */
    public boolean isDifferentiated;
    /**
     * The vectorial mask that guards this instruction, if any.
     */
    private InstructionMask whereMask;
    /**
     * When true, this means that this instruction results from the split of
     * a masked WHERE statement, and this instruction is the one that sets the mask.
     */
    private boolean isMaskDefinition;
    private Instruction fromInclude;
    /**
     * For an interfaceDecl, its associated InterfaceUnit.
     */
    private TapList<Unit> interfaceUnits;

    /**
     * Create an empty Instruction.
     */
    public Instruction() {
        super();
    }

    /**
     * Create an Instruction containing the given tree.
     */
    public Instruction(Tree tree) {
        super();
        setTree(tree) ;
    }

    /**
     * Create an Instruction containing the given tree,
     * also giving its syntactic controller.
     */
    public Instruction(Tree tree, Instruction syntaxController) {
        super();
        setTree(tree) ;
        this.syntaxController = syntaxController;
    }

    /**
     * Create an Instruction containing the given tree,
     * also giving its syntactic controller and source code line number.
     */
    public Instruction(Tree tree, Instruction syntaxController, int lineNo) {
        super();
        setTree(tree) ;
        this.syntaxController = syntaxController;
        this.lineNo = lineNo ;
    }

    /**
     * Create an Instruction containing the given tree,
     * also giving its syntactic controller and containing Block.
     */
    public Instruction(Tree tree, Instruction syntaxController, Block block) {
        super();
        setTree(tree) ;
        this.syntaxController = syntaxController;
        this.block = block;
    }

    /** Places newTree into this Instruction, and accordingly set annotation from newTree to Instruction */
    public void setTree(Tree newTree) {
        tree = newTree ;
        if (newTree!=null) {
            newTree.setAnnotation("instruction", this);
        }
    }

    /**
     * @return a new "unit definition stub" Instruction, used to mark the location of this Unit's definition in the code.
     */
    public static Instruction createUnitDefinitionStub(Unit definedUnit, Block containerBlock) {
        return new Instruction(createUnitDefinitionStubTree(definedUnit), null, containerBlock);
    }

    /**
     * @return a new "unit definition stub" Tree, used to mark the location of the given Unit's definition in the code.
     */
    public static Tree createUnitDefinitionStubTree(Unit definedUnit) {
        //[llh] completer op_constructor, op_destructor, (et peut-etre op_interface)
        int stubOp = definedUnit.isClass() ? ILLang.op_class : definedUnit.isModule() ? ILLang.op_module : ILLang.op_function;
        int nameRank = definedUnit.isClass() ? 2 : definedUnit.isModule() ? 1 : 4;
        Tree defTree = ILUtils.build(stubOp);
        defTree.setAnnotation("Unit", definedUnit);
        defTree.setChild(ILUtils.build(ILLang.op_ident, definedUnit.name()), nameRank);
        return defTree;
    }

    /**
     * @return a copy of this.fromInclude.
     */
    public static Instruction fromIncludeCopy(Instruction fromIncludeInstr, boolean copy) {
        Instruction result;
        if (fromIncludeInstr == null) {
            result = null;
        } else {
            result = new Instruction(copy ? ILUtils.copy(fromIncludeInstr.tree) : fromIncludeInstr.tree);
            Instruction copiedOneFromInclude = result;
            Instruction fromIncl = fromIncludeInstr.fromInclude;
            while (fromIncl != null) {
                copiedOneFromInclude.fromInclude = new Instruction(copy ? ILUtils.copy(fromIncl.tree) : fromIncl.tree);
                fromIncl = fromIncl.fromInclude;
                copiedOneFromInclude = copiedOneFromInclude.fromInclude;
            }
        }
        return result;
    }

    /**
     * Returns in general a copy of origWhereMask, with equal (==) controlInstruction
     * for equal (==) in origWhereMask.
     * Special case: when alreadyCopiedMasks holds a predefined value as the copy of a given mask,
     * then this predefined value is returned, even if it is not strictly speaking a copy of origWhereMask.
     */
    private static Instruction smartCopyWhereMask(Instruction origWhereMask,
                                                  TapList<TapPair<Instruction, Instruction>> alreadyCopiedMasks) {
        TapPair<Instruction, Instruction> place = TapList.assq(origWhereMask, alreadyCopiedMasks);
        Instruction resultInstruction;
        Tree origTree = origWhereMask.tree;
        if (place == null || place.second == null) {
            resultInstruction = new Instruction(ILUtils.copy(origTree));
            alreadyCopiedMasks.placdl(new TapPair<>(origWhereMask, resultInstruction));
        } else {
            resultInstruction = place.second;
        }

        resultInstruction.fromInclude = origWhereMask.fromInclude;
        return resultInstruction;
    }

    /**
     * Add the comment with text "content" in front of "oldComment".
     */
    private static Tree addComment(Tree oldComment, String content) {
        Tree newComment = ILUtils.build(ILLang.op_stringCst, content);
        if (oldComment != null) {
            oldComment.addChild(newComment, 1);
            return oldComment;
        } else {
            return ILUtils.build(ILLang.op_comments, newComment);
        }
    }

    /**
     * @param wait   When true, use the existing diff name if exists, but do not create it if it doesn't exist.
     * @param suffix The suffix used for differentiation. May be passed null when wait==true.
     */
    public static Instruction differentiateChainedIncludes(Instruction includeInstr, String suffix, boolean wait) {
// System.out.println("differentiateChainedIncludes "+includeInstr+" suffix:"+suffix+" wait to diff include file name:"+wait) ;
        Instruction diffIncludeInstr;

        String originalIncludeString = includeInstr.tree.stringValue();
        TapTriplet<String, String, Tree> diffIncludeInfo = TapEnv.getSetDiffIncludeInfo(originalIncludeString);
        if (diffIncludeInfo.second == null) {                  //This include doesn't need a diff include *so far*
            if (wait) {                                      //This instruction doesn't need a diff
                if (diffIncludeInfo.third == null)             //No tree has been built yet for the diff code for this include (diff or not)
                {
                    diffIncludeInfo.third = ILUtils.copy(includeInstr.tree);
                }
            } else {                                         //This instruction needs a diff => This include needs a diff
                String diffIncludeFileName = TapEnv.differentiateIncludeName(originalIncludeString, suffix);
                diffIncludeInfo.second = diffIncludeFileName;
                if (diffIncludeInfo.third == null)             //No tree has been built yet for the diff code for this include (diff or not)
                {
                    diffIncludeInfo.third = ILUtils.copy(includeInstr.tree);
                }
                diffIncludeInfo.third.setValue(diffIncludeFileName);
            }
        }
        diffIncludeInstr = new Instruction(diffIncludeInfo.third); // So that all include's in the diff code share the same Tree
        if (includeInstr.fromInclude != null && !ILUtils.isNullOrEmptyAtom(includeInstr.fromInclude.tree)) {
            diffIncludeInstr.fromInclude =
                    differentiateChainedIncludes(includeInstr.fromInclude, suffix, wait);
        }
        return diffIncludeInstr;
    }

    public static TapList<Unit> usedUnits(Tree tree, TapList<Unit> collected, SymbolTable symbolTable) {
        if (tree == null) {
            return collected;
        }
        switch (tree.opCode()) {
            case ILLang.op_constDeclaration:
            case ILLang.op_assign:
                collected = usedUnits(tree.down(2), collected, symbolTable);
                break;
            case ILLang.op_varDeclaration:
                collected = usedUnits(tree.down(3), collected, symbolTable);
                break;
            case ILLang.op_arrayConstructor:
            case ILLang.op_declarators: {
                Tree[] decls = tree.children();
                for (int i = decls.length - 1; i >= 0; --i) {
                    collected = usedUnits(decls[i], collected, symbolTable);
                }
                break;
            }
            case ILLang.op_call:
                collected = usedUnits(ILUtils.getCalledName(tree), collected, symbolTable);
                break;
            case ILLang.op_ident: {
                String funcName = ILUtils.getIdentString(tree);
                FunctionDecl funcDecl = funcName == null ? null : symbolTable.getFunctionNotInterfaceDecl(funcName);
                Unit found = funcDecl == null ? null : funcDecl.unit();
                if (found != null) {
                    collected = new TapList<>(found, collected);
                }
                break;
            }
            case ILLang.op_minus:
            case ILLang.op_address: {
                collected = usedUnits(tree.down(1), collected, symbolTable);
                break;
            }
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_rightShift:
            case ILLang.op_leftShift:
            case ILLang.op_bitOr: {
                collected = usedUnits(tree.down(1), collected, symbolTable);
                collected = usedUnits(tree.down(2), collected, symbolTable);
                break;
            }
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_include:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
            case ILLang.op_none:
            case ILLang.op_letter:
            case ILLang.op_allocate:
                break;
            case ILLang.op_function:
            case ILLang.op_program:
            case ILLang.op_module:
            case ILLang.op_class:
            case ILLang.op_constructor:
            case ILLang.op_destructor:
            case ILLang.op_nameSpace:
                // Unit place-holders!
                break;
            case ILLang.op_enumType:
            case ILLang.op_recordType:
            case ILLang.op_unionType:
            case ILLang.op_using:
            case ILLang.op_scopeAccess:
            case ILLang.op_constructorCall:
            case ILLang.op_destructorCall:
                // TODO: might be units to collect there!
                break;
            default:
                TapEnv.toolError("(usedUnits) unexpected operator " + tree.opName());
        }
        return collected;
    }

    /** @return true if this instruction is a call that must be checkpointed (aka "joint mode") */
    public boolean thisCallIsCheckpointed(Unit calledUnit) {
        return (hasDirective(Directive.CHECKPOINT)!=null
                || (hasDirective(Directive.NOCHECKPOINT)==null
                    && (calledUnit.hasDirective(Directive.CHECKPOINT)!=null
                        || (calledUnit.hasDirective(Directive.NOCHECKPOINT)==null
                            && TapEnv.get().defaultCheckpointCalls)
                        )
                    )
                ) ;
    }

    /**
     * @return the vectorial mask of this instruction, if any.
     */
    public InstructionMask whereMask() {
        return whereMask;
    }

    /**
     * @return true when this instruction is the one that sets the mask of a WHERE.
     */
    public boolean isMaskDefinition() {
        return isMaskDefinition;
    }

    /**
     * States that this instruction is the one that sets the mask of a WHERE.
     */
    protected void declareIsMaskDefinition() {
        isMaskDefinition = true;
    }

    /**
     * When this instruction comes from an INCLUDE file.
     *
     * @return the instruction that does the include.
     */
    public Instruction fromInclude() {
        return fromInclude;
    }

    private Instruction fromIncludeRoot() {
        Instruction result = fromInclude;
        while (result.fromInclude != null) {
            result = result.fromInclude;
        }
        return result;
    }

    public String fromIncludeRootName() {
        if (fromInclude == null) {
            return null;
        }
        Instruction result = fromIncludeRoot();
        if (result.tree != null && result.tree.opCode() == ILLang.op_include) {
            return result.tree.stringValue();
        } else {
            return null;
        }
    }

    public void setFromInclude(Instruction incl) {
        fromInclude = incl;
    }

    /**
     * Connects enclosingWhereMask as an additional enclosing where mask
     * around this Instruction (except if it has already been connected)
     */
    public void encloseInWhereMask(InstructionMask enclosingWhereMask) {
        if (whereMask == null) {
            whereMask = enclosingWhereMask;
        } else if (whereMask != enclosingWhereMask) {
            InstructionMask lastWhereMask = whereMask;
            while (lastWhereMask.enclosingMask != null
                    && lastWhereMask.enclosingMask != enclosingWhereMask) {
                lastWhereMask = lastWhereMask.enclosingMask;
            }
            lastWhereMask.enclosingMask = enclosingWhereMask;
        }
    }

    /**
     * Update the given Instruction (which is a unit definition stub) so that it becomes a stub for this definedUnit.
     */
    public void updateUnitDefinitionStub(Unit definedUnit) {
        int nameRank = definedUnit.isClass() ? 2 : definedUnit.isModule() ? 1 : 4;
        tree.setAnnotation("Unit", definedUnit);
        tree.setChild(ILUtils.build(ILLang.op_ident, definedUnit.name()), nameRank);
    }

    public Unit isUnitDefinitionStub() {
        return tree == null ? null : tree.getAnnotation("Unit");
    }

    /**
     * @return true if this instruction syntactically controls "controlled".
     */
    public boolean syntaxControls(Instruction controlled) {
        while (controlled != null && controlled != this) {
            controlled = controlled.syntaxController;
        }
        return controlled != null;
    }

    /**
     * @return true if this instruction is loop control.
     */
    public boolean isALoop() {
        return tree != null && tree.opCode() == ILLang.op_loop;
    }

    /**
     * @return the "position" of this instruction in the file it comes from.
     */
    public int getPosition() {
        return tree == null ? -1 : ILUtils.position(tree);
    }

    /**
     * Sets the "position" of this instruction to the same as of refInstr.
     */
    public void setPosition(Instruction refInstr) {
        int refPos = refInstr == null ? -1 : refInstr.getPosition();
        if (tree != null) {
            ILUtils.setPosition(tree, refPos);
        }
    }

    /**
     * @return a copy of this instruction, making sure that the
     * copy's where mask is a copy of the instruction's vectorial mask.
     */
    public Instruction copy(TapList<TapPair<Instruction, Instruction>> alreadyCopiedMasks) {
        Instruction copyInstr = new Instruction(tree.copy());
        copyInstr.preComments = preComments;
        copyInstr.postComments = postComments;
        copyInstr.preCommentsBlock = preCommentsBlock;
        copyInstr.postCommentsBlock = postCommentsBlock;
        copyInstr.directives = directives;
        // dans le cas ("where" tree~Tree)
        // vmp a faire si on inline des fonctions plus compliquees que abs
        // Ne marche pas avec des where imbriques
        copyInstr.whereMask = null;
        if (whereMask != null) {
            Instruction localTest = whereMask.controlInstruction;
            Instruction result = smartCopyWhereMask(localTest, alreadyCopiedMasks);
            copyInstr.whereMask = whereMask.copy();
            copyInstr.whereMask.controlInstruction = result;
        }
        copyInstr.isMaskDefinition = isMaskDefinition;
        copyInstr.fromInclude = fromIncludeCopy(fromInclude, true);
        copyInstr.isPhantom = isPhantom;
        copyInstr.interfaceUnits = interfaceUnits;
        return copyInstr;
    }

    /**
     * @return a copy (ILUtils.copy()) of this instruction's Tree,
     * with all comments present on this instruction
     * copied and attached to the root of the copy tree.
     */
    public Tree copyTreePlusComments() {
        if (tree == null) {
            return null;
        } else {
            Tree copyTree = ILUtils.copy(tree);
            copyCommentsToTree(copyTree) ;
            return copyTree;
        }
    }

    /** Copy the comments that were stored in this Instruction, back as "*Comments*" annotations on the given tree */
    public void copyCommentsToTree(Tree tree) {
        Tree postCommentsLocation = tree;
        //Special case: postComments of a loop must be placed on the do-header:
        if (tree.opCode() == ILLang.op_loop && tree.down(3) != null) {
            postCommentsLocation = tree.down(3);
        }
        if (preComments != null) {
            tree.setAnnotation("preComments", ILUtils.copy(preComments));
        }
        if (preCommentsBlock != null) {
            tree.setAnnotation("preCommentsBlock", ILUtils.copy(preCommentsBlock));
        }
        if (postComments != null) {
            postCommentsLocation.setAnnotation("postComments", ILUtils.copy(postComments));
        }
        if (postCommentsBlock != null) {
            postCommentsLocation.setAnnotation("postCommentsBlock", ILUtils.copy(postCommentsBlock));
        }
    }

    /**
     * Looks for a directive of the given kind attached to this instruction.
     *
     * @param kind The searched kind of directive
     * @return The directive of the given kind, or null if not found.
     */
    public Directive hasDirective(int kind) {
        return Directive.hasDirective(directives, kind);
    }

    /**
     * @return true if this instruction is a where control instruction.
     */
    public boolean isAWhereControl() {
        return tree.opCode() == ILLang.op_where
                && tree.down(2).opCode() == ILLang.op_none
                && tree.down(3).opCode() == ILLang.op_none
                && tree.getAnnotation("notemptywhere") == null;
    }

    /**
     * @return the vectorial mask of this instruction.
     * When the instruction is a where test, the returned vectorial mask
     * does not include this where control.
     */
    public InstructionMask getWhereMask() {
        if (tree == null) {
            return null;
        }
        if (whereMask != null && isAWhereControl()) {
            return whereMask.enclosingMask;
        } else {
            return whereMask;
        }
    }

    /**
     * Sets a vectorial mask on this instruction.
     */
    public void setWhereMask(InstructionMask whereMask) {
        this.whereMask = whereMask;
    }

    /**
     * @return the nesting depth of this instruction's vectorial masks.
     */
    public int whereMaskDepth() {
        return whereMask == null ? 0 : whereMask.depth();
    }

    public String hasPreCommentStartingWith(String startString) {
        String suffix = hasCommentStartingWith(preComments, startString) ;
        if (suffix==null) {
            suffix = hasCommentStartingWith(preCommentsBlock, startString) ;
        }
        return suffix;
    }

    public String hasPostCommentStartingWith(String startString) {
        String suffix = hasCommentStartingWith(postComments, startString) ;
        if (suffix==null) {
            suffix = hasCommentStartingWith(postCommentsBlock, startString) ;
        }
        return suffix;
    }

    public static String hasCommentStartingWith(Tree comment, String startString) {
        if (comment==null) return null ;
        String suffix = null ;
        switch (comment.opCode()) {
            case ILLang.op_comments: {
                Tree[] inside = comment.children() ;
                for (int i=0 ; i<inside.length && suffix==null ; ++i) {
                    suffix = hasCommentStartingWith(inside[i], startString) ;
                }
                break ;
            }
            case ILLang.op_stringCst: {
                if (comment.stringValue().startsWith(startString)) {
                    suffix = comment.stringValue().substring(startString.length()) ;
                }
                break ;
            }
        }
        return suffix ;
    }

    public boolean hasCommentWith(String text) {
        return hasCommentWith(preComments, text) || hasCommentWith(preCommentsBlock, text)
            || hasCommentWith(postComments, text) || hasCommentWith(postCommentsBlock, text) ;
    }

    public static boolean hasCommentWith(Tree comment, String text) {
        if (comment==null) return false ;
        boolean found = false ;
        switch (comment.opCode()) {
            case ILLang.op_comments: {
                Tree[] inside = comment.children() ;
                for (int i=0 ; i<inside.length && !found ; ++i) {
                    found = hasCommentWith(inside[i], text) ;
                }
                break ;
            }
            case ILLang.op_stringCst: {
                if (comment.stringValue().equals(text)) {
                    found = true ;
                }
                break ;
            }
        }
        return found ;
    }

    public String containsPreComment(String searched) {
        if (preComments == null) {
            return null;
        }
        Tree[] preCommentStringTrees = preComments.children();
        String found = null;
        for (int i = preCommentStringTrees.length - 1; i >= 0 && found == null; --i) {
            if (preCommentStringTrees[i].opCode() == ILLang.op_stringCst
                    && preCommentStringTrees[i].stringValue().toLowerCase().contains(searched)) {
                found = preCommentStringTrees[i].stringValue();
            }
        }
        return found;
    }

    /**
     * Add the comment with text "newComment" in front of this instruction's preComments.
     */
    public void addPreComments(String newComment) {
        this.preComments = addComment(this.preComments, newComment);
    }

    /**
     * Add the given newComments after this instruction's preComments.
     */
    public void appendPreComments(TapList<Tree> newComments) {
        if (newComments != null) {
            this.preComments = ILUtils.appendComments(this.preComments, newComments, ILLang.op_comments);
        }
    }

    /**
     * Add the given newComments after this instruction's postComments.
     */
    public void appendPostComments(TapList<Tree> newComments) {
        if (newComments != null) {
            this.postComments = ILUtils.appendComments(this.postComments, newComments, ILLang.op_comments);
        }
    }

    public boolean isAProcedure() {
        return tree.opCode() == ILLang.op_function || tree.opCode() == ILLang.op_program;
    }

    /**
     * @return true if this instruction is a declaration statement.
     */
    public boolean isADeclaration() {
        return tree != null && ILUtils.isADeclaration(tree);
    }

    /**
     * @return true if this instruction is a no_op, i.e. a op_none, created only e.g. to hold some comments.
     */
    public boolean isANoOp() {
        return tree!=null && tree.opCode()==ILLang.op_none ;
    }

    /**
     * @return the list of all SymbolDecl's declared by this Instruction.
     */
    public TapList<SymbolDecl> declaredSymbolDecls(SymbolTable publicSymbolTable, SymbolTable privateSymbolTable) {
        TapList<SymbolDecl> result = null;
        switch (tree.opCode()) {
            case ILLang.op_varDeclaration: {
                result = getSymbolDeclsFrom(tree.down(3), publicSymbolTable, privateSymbolTable);
                break;
            }
            case ILLang.op_constDeclaration: {
                result = getSymbolDeclsFrom(tree.down(2), publicSymbolTable, privateSymbolTable);
                break;
            }
            case ILLang.op_varDimDeclaration: {
                result = getSymbolDeclsFrom(tree, publicSymbolTable, privateSymbolTable);
                break;
            }
            case ILLang.op_typeDeclaration:
                if (tree.down(1).opCode() == ILLang.op_ident) {
                    SymbolDecl symbDecl = null;
                    if (privateSymbolTable != null) {
                        symbDecl = privateSymbolTable.getTopSymbolDecl(ILUtils.baseName(tree.down(1)));
                    }
                    if (symbDecl == null) {
                        symbDecl = publicSymbolTable.getTopSymbolDecl(ILUtils.baseName(tree.down(1)));
                    }
                    if (symbDecl != null) {
                        result = new TapList<>(symbDecl, null);
                    }
                }
                break;
            case ILLang.op_accessDecl:
                if (tree.down(2).opCode() == ILLang.op_expressions) {
                    result = getSymbolDeclsFrom(tree.down(2), publicSymbolTable, privateSymbolTable);
                }
                break;
            case ILLang.op_external:
            case ILLang.op_intrinsic:
            case ILLang.op_save:
                result = getSymbolDeclsFrom(tree, publicSymbolTable, privateSymbolTable);
                break;
            case ILLang.op_common:
                result = getSymbolDeclsFrom(tree.down(2), publicSymbolTable, privateSymbolTable);
                break;
            case ILLang.op_functionDeclarator:
            case ILLang.op_equivalence:
            default:
                break;
        }
        return result;
    }

    private TapList<SymbolDecl> getSymbolDeclsFrom(Tree declarators,
                                                   SymbolTable publicSymbolTable, SymbolTable privateSymbolTable) {
        TapList<SymbolDecl> result = null;
        int i = declarators.length();
        SymbolDecl symbDecl = null;
        while (i > 0) {
            if (privateSymbolTable != null) {
                symbDecl = privateSymbolTable.getTopSymbolDecl(ILUtils.baseName(declarators.down(i)));
            }
            if (symbDecl == null && publicSymbolTable != null) {
                symbDecl = publicSymbolTable.getTopSymbolDecl(ILUtils.baseName(declarators.down(i)));
            }
            if (symbDecl != null) {
                result = new TapList<>(symbDecl, result);
            }
            i = i - 1;
        }
        return result;
    }

    /**
     * Declare that this Instruction belongs to the include file "origInclude".
     */
    public void setFromIncludeCopy(Instruction origInclude, boolean copy) {
        Instruction copiedInclude = fromIncludeCopy(origInclude, copy);
        this.setFromInclude(copiedInclude);
    }

    public boolean isInStdCInclude() {
        return (TapEnv.relatedLanguageIsC() || TapEnv.relatedLanguage() == TapEnv.MIXED) && isInStdInclude();
    }

    public boolean isInStdInclude() {
        boolean inStd = false;
        Instruction include = fromInclude;
        if (include != null && tree.opCode() == ILLang.op_include &&
                tree.stringValue().equals(include.tree.stringValue())) {
            // Because of silly choice that Instr:include "foo.h" is {from Instr:include "foo.h"}:
            include = include.fromInclude();
        }
        while (include != null && !inStd) {
            inStd = (include.tree != null && TapEnv.isStdIncludeName(include.tree.stringValue()));
            include = include.fromInclude;
        }
        return inStd;
    }

    /**
     * Decorates the given primalInstruction and each of diffInstructionS,
     * with the "fromInclude" info built by differentiation of this Instruction
     * ("this" Instruction is the "fromInclude" of the original non-differentiated instruction).
     * Keeps track of the correspondence between non-diff and diff include file names
     * through TapEnv.get().differentiatedIncludeNames.
     *
     * @param primalInstruction the primal copy of the original source instruction
     * @param primalIsRewritten true when the primal copy is created specifically
     *                          for the diff code (e.g. foo_nodiff), instead of being just a reference to the source (e.g. foo)
     * @param diffInstructionSR  the (array of (replicas)) list of new differentiated instructions.
     * @param suffix            The suffix used for differentiation. May be passed null when
     *                          this is called not for differentiation but during fixImplicits preliminary phase.
     */
    public void setIncludeOfDiffInstructions(Instruction primalInstruction, boolean primalIsRewritten,
                                             TapList<Instruction>[] diffInstructionSR, String suffix) {
        if (!TapEnv.get().expandDiffIncludeFile && this.tree != null && this.tree.stringValue() != null && !this.tree.stringValue().isEmpty()) {
            Instruction diffInclude = this;
            if (!TapEnv.alreadyDiffInclude(this.tree.stringValue())) {
                diffInclude = differentiateChainedIncludes(this, suffix,
                                      (!primalIsRewritten
                                       && (diffInstructionSR == null || diffInstructionSR.length==0 || diffInstructionSR[0]==null)));
            }
            TapList<Instruction> diffInstructionS;
            if (diffInstructionSR!=null) {
                for (int iReplic=diffInstructionSR.length-1 ; iReplic>=0 ; --iReplic) {
                    diffInstructionS = diffInstructionSR[iReplic] ;
                    while (diffInstructionS != null) {
                        diffInstructionS.head.fromInclude = diffInclude;
                        diffInstructionS = diffInstructionS.tail;
                    }
                }
            }
            if (primalInstruction != null) {
                primalInstruction.fromInclude = diffInclude;
                if (primalInstruction.tree != null
                        && primalInstruction.tree.opCode() == ILLang.op_include
                        && primalInstruction.tree.stringValue().equals(tree.stringValue()))
                // If this is the diff of the include instruction itself (i.e. Instr:include "foo.h" {from Instr:include "foo.h"})
                {
                    primalInstruction.tree = diffInclude.tree;
                }
            }
        }
    }

    /**
     * Modify TapEnv.differentiatedIncludeNames so that this include name will be differentiated in the generated code
     */
    public void forceDiffInclude(String suffix) {
        String originalIncludeString = tree.stringValue();
        TapTriplet<String, String, Tree> diffIncludeInfo = TapEnv.getSetDiffIncludeInfo(originalIncludeString);
        if (diffIncludeInfo.second == null) {
            diffIncludeInfo.second = TapEnv.differentiateIncludeName(originalIncludeString, suffix);
        }
    }

    public TapList<Unit> declaresUnits(SymbolTable symbolTable) {
        TapList<Unit> found = null;
        if (tree != null) {
            if (tree.opCode() == ILLang.op_function) {
                String funcName = ILUtils.getIdentString(tree.down(4));
                //TODO: regarder les types et appeler symbolTable.getFunctionDecl()
                FunctionDecl funcDecl = funcName == null ? null : symbolTable.getFunctionNotInterfaceDecl(funcName);
                if (funcDecl != null) {
                    found = new TapList<>(funcDecl.unit(), null);
                }
            } else if (tree.opCode() == ILLang.op_varDeclaration) {
                Tree[] declarators = tree.down(3).children();
                for (int i = declarators.length - 1; i >= 0; --i) {
                    if (declarators[i].opCode() == ILLang.op_functionDeclarator) {
                        String funcName = ILUtils.getIdentString(declarators[i].down(1));
                        //TODO: regarder les types et appeler symbolTable.getFunctionDecl()
                        FunctionDecl funcDecl = funcName == null ? null : symbolTable.getFunctionNotInterfaceDecl(funcName);
                        if (funcDecl != null) {
                            found = new TapList<>(funcDecl.unit(), found);
                        }
                    }
                }
            }
        }
        return found;
    }


    /**
     * Replaces the given oldTree by the given newTree in-place
     * inside the tree of this Instruction.
     * Does nothing when newTree is null or equals oldTree.
     * When necessary, the tree of this Instruction is itself modified.
     *
     * @return the newTree.
     */
    public Tree replaceTree(Tree oldTree, Tree newTree) {
        if (newTree == null || newTree == oldTree) {
            return oldTree;
        }
        if (tree == oldTree) {
            setTree(newTree);
        } else {
            oldTree.parent().setChild(newTree, oldTree.rankInParent());
        }
        return newTree;
    }

    /**
     * Sets the interface Units associated to this interface declaration Instruction.
     */
    public void setInterfaceUnits(TapList<Unit> units) {
        interfaceUnits = units;
        while (units != null) {
            units.head.setInterfaceDeclaration(this);
            units = units.tail;
        }
    }

    /**
     * Prints in detail the contents of this Instruction, onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        InstructionMask mask = whereMask;
        if (mask != null) {
            TapEnv.print("whereMask:");
            mask.dump();
        }
        TapEnv.print(" ");
        if (isPhantom) {
            TapEnv.print("phantom: ");
        }
        if (tree != null) {
            if (tree.getAnnotation("Unit") != null) {
                TapEnv.print("<Definition of " + tree.getAnnotation("Unit") + ">");
            } else {
                TapEnv.print(ILUtils.toString(tree));
            }
            TapEnv.print(" +line:" + lineNo) ;
            int pos = ILUtils.getPosition(tree);
            if (pos != 0) {
                TapEnv.print(" +position:" + pos);
            }
        } else {
            TapEnv.print("null tree");
        }
        if (preComments != null) {
            TapEnv.print(" +preComments:");
            if (ILUtils.isNullOrNoneOrEmptyList(preComments))
                TapEnv.print("empty") ;
            else
                ILUtils.dump(preComments, 0);
        }
        if (postComments != null) {
            TapEnv.print(" +postComments:");
            if (ILUtils.isNullOrNoneOrEmptyList(postComments))
                TapEnv.print("empty") ;
            else
                ILUtils.dump(postComments, 0);
        }
        if (preCommentsBlock != null) {
            TapEnv.print(" +preCommentsBlock:");
            if (ILUtils.isNullOrNoneOrEmptyList(preCommentsBlock))
                TapEnv.print("empty") ;
            else
                ILUtils.dump(preCommentsBlock, 0);
        }
        if (postCommentsBlock != null) {
            TapEnv.print(" +postCommentsBlock:");
            if (ILUtils.isNullOrNoneOrEmptyList(postCommentsBlock))
                TapEnv.print("empty") ;
            else
                ILUtils.dump(postCommentsBlock, 0);
        }
        if (directives != null) {
            TapEnv.print(" +directives:");
            TapEnv.print(directives.toString());
        }
        if (fromInclude != null) {
            TapEnv.print(" from ");
            fromInclude.dump();
        }
    }

    @Override
    public String toString() {
        String result = "Instr:"; //+" @"+Integer.toHexString(hashCode()) ;
        if (isPhantom) {
            result = "(phantom)" + result;
        }
        if (whereMask != null) {
            result = result + whereMask;
        }
        if (tree == null) {
            result = result + "null_tree";
        } else if (tree.getAnnotation("Unit") != null) {
            result = result + "<Definition of " + tree.getAnnotation("Unit") + ">";
        } else {
            result = result + tree;
        }

        String filename = null ;
        if (block!=null && block.unit()!=null) {
            Unit fileUnit = block.unit().translationUnit() ;
            if (fileUnit!=null) {
                filename = fileUnit.name() ;
            }
        }
        result = result+" (at line "+lineNo+" in file "+filename+")" ;

        if (fromInclude != null) {
            result = result 
                    + " {from " + fromInclude //+" @"+Integer.toHexString(fromInclude.hashCode())
                    //+ ":@" + (fromInclude.tree == null ? "null" : Integer.toHexString(fromInclude.tree.hashCode()))
                    + '}';
        }
        return result;
    }
}
