/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * An ArrayDim Object plus its IterDescriptor information.
 */
public final class ArrayDimPlusIter extends ArrayDim {
    protected final IterDescriptor iter;

    protected ArrayDimPlusIter(ArrayDim model, IterDescriptor iter) {
        super(model.tree(), model.lower, model.upper,
                model.refArrayTree, model.rankInRef, model.refNDims, model.refQuality);
        this.iter = iter;
    }
}
