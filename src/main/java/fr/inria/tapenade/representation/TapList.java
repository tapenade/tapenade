/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.BoolVector;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Int2ZoneInfo;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToObject ;
import fr.inria.tapenade.utils.Tree;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * List of Objects, simply chained forward.
 */
public final class TapList<T>
        implements Iterable<T> {

    /**
     * The head Object in this list.
     */
    public T head;

    /**
     * The tail of this list.
     */
    public TapList<T> tail;

    /**
     * Creates a new list cell and initializes it to null.
     */
    public TapList() {
        super();
    }

    /**
     * Creates a new list cell with given "head" and "tail".
     */
    public TapList(T head, TapList<T> tail) {
        super();
        this.head = head;
        this.tail = tail;
    }

    /**
     * @return the length of given list. Returns 0 if list is null.
     */
    public static <T> int length(TapList<T> list) {
        int result = 0;
        while (list != null) {
            ++result;
            list = list.tail;
        }
        return result;
    }

    /**
     * Same as length, but stops on a cycle.
     *
     * @return the length of given list.
     */
    public static <T> int lengthWithCycle(TapList<T> list) {
        TapList<TapList<T>> dejaVu = null;
        int result = 0;
        while (list != null) {
            if (TapList.contains(dejaVu, list)) {
                list = null;
            } else {
                dejaVu = new TapList<>(list, dejaVu);
                ++result;
                list = list.tail;
            }
        }
        return result;
    }

    public static <T> TapList<T> addUnlessPresent(TapList<T> list, T obj) {
        if (obj == null) {
            return list;
        }
        boolean found = TapList.contains(list, obj);
        return found ? list : new TapList<>(obj, list);
    }

    protected static <T> TapList<T> addUnlessPresentEquals(TapList<T> list, T obj) {
        if (obj == null) {
            return list;
        }
        TapList<T> inList = list;
        boolean found = false;
        while (!found && inList != null) {
            found = inList.head.equals(obj);
            inList = inList.tail;
        }
        return found ? list : new TapList<>(obj, list);
    }

    /**
     * @return true if "obj" is found in the given list
     * Comparison is done with ==.
     */
    public static <T> boolean contains(TapList<T> list, T obj) {
        boolean found = false;
        while (!found && list != null) {
            found = list.head == obj;
            list = list.tail;
        }
        return found;
    }

    /**
     * @return true if "obj" is found in the given list
     * Comparison is done with equals().
     */
    public static <T> boolean containsEquals(TapList<T> list, T obj) {
        boolean found = false;
        while (!found && list != null) {
            found = obj.equals(list.head);
            list = list.tail;
        }
        return found;
    }

    /**
     * @param caseSensitive when true, the string equality is case sensitive.
     * @return true if the string "str" is found in "list".
     */
    public static boolean containsString(TapList<String> list, String str, boolean caseSensitive) {
        boolean found = false;
        while (!found && list != null) {
            found = caseSensitive ?
                    list.head.equals(str) :
                    list.head.equalsIgnoreCase(str);
            list = list.tail;
        }
        return found;
    }

    /**
     * @param caseSensitive when true, the string equality is case sensitive.
     * @return true if "list" contains one String that is in "searched".
     */
    public static boolean containsOneOfStrings(TapList<String> list, String[] searched, boolean caseSensitive) {
        boolean found = false;
        for (int i = searched.length - 1; i >= 0 && !found; --i) {
            found = containsString(list, searched[i], caseSensitive);
        }
        return found;
    }

    /**
     * @return true if the given list contains the given "tree".
     * Comparison is done with ILUtils.eqOrDisjointRef and not Tree.equals().
     * TODO: does not work well with pointers !
     */
    public static boolean containsTree(TapList<Tree> list, Tree tree) {
        boolean found = false;
        while (!found && list != null) {
            found = ILUtils.eqOrDisjointRef(list.head, tree, null, null) == 1;
            list = list.tail;
        }
        return found;
    }

    /**
     * @return true if the given "tree" is found in the given list.
     * Comparison is done with t1.equalsTree(t2).
     */
    public static boolean containsTreeEquals(TapList<Tree> list, Tree tree) {
        boolean found = false;
        while (!found && list != null) {
            found = tree.equalsTree(list.head);
            list = list.tail;
        }
        return found;
    }

    public static boolean intersectTreeEquals(TapList<Tree> list1, TapList<Tree> list2) {
        boolean intersects = false;
        while (!intersects && list1 != null) {
            intersects = containsTreeEquals(list2, list1.head);
            list1 = list1.tail;
        }
        return intersects;
    }

    /**
     * @return true if an TapPair containing the given first and second
     * is found in the given list.
     */
    protected static <T, U> boolean containsObjectPair(TapList<TapPair<T, U>> list, T first, U second) {
        boolean found = false;
        TapPair<T, U> pair;
        while (!found && list != null) {
            pair = list.head;
            found = pair.first == first && pair.second == second;
            list = list.tail;
        }
        return found;
    }

    public static boolean containsEqualsObjectPair(TapList<TapPair<Unit, String>> list,
                                                   Unit first, String second) {
        boolean found = false;
        TapPair<Unit, String> pair;
        while (!found && list != null) {
            pair = list.head;
            found = pair.first == first && pair.second.equals(second);
            list = list.tail;
        }
        return found;
    }

    /**
     * @return true if the given "varDecl" is found in the given list.
     * Comparison is done with VariableDecl.equalsOther().
     */
    public static boolean containsVariableDecl(TapList<SymbolDecl> list, VariableDecl varDecl) {
        boolean found = false;
        while (!found && list != null) {
            found = list.head != null
                    && list.head.isA(SymbolTableConstants.VARIABLE)
                    && varDecl.equalsOther((VariableDecl) list.head);
            list = list.tail;
        }
        return found;
    }

    /**
     * @return true if the given list contains a SymbolDecl with the same name as for the given "symbolDecl".
     */
    public static boolean containsSameNameSymbolDecl(TapList<SymbolDecl> list, SymbolDecl symbolDecl) {
        boolean found = false;
        while (!found && list != null) {
            found = list.head != null
                    && symbolDecl.symbol.equals(list.head.symbol);
            list = list.tail;
        }
        return found;
    }

    /**
     * @return null if no element is found.
     * Comparison is done with WrapperTypeSpec.equalLiterally.
     */
    public static WrapperTypeSpec findTypeSpec(TapList<TypeSpec> list, WrapperTypeSpec typeSpec) {
        WrapperTypeSpec found = null;
        while (found == null && list != null) {
            if (list.head != null
                    && typeSpec.equalsLiterally(list.head)) {
                found = (WrapperTypeSpec) list.head;
            }
            list = list.tail;
        }
        return found;
    }

    /**
     * @return null if no element is found.
     * Comparison is done with String.equals().
     */
    public static SymbolDecl findNamedSymbolDecl(TapList<SymbolDecl> list, String name) {
        SymbolDecl found = null;
        while (found == null && list != null) {
            if (list.head != null && name.equals(list.head.symbol)) {
                found = list.head;
            }
            list = list.tail;
        }
        return found;
    }

    /**
     * Replaces all occurences of "from" into "to" in "list".
     */
    public static <T> void replace(TapList<T> list, T from, T to) {
        while (list != null) {
            if (list.head == from) {
                list.head = to;
            }
            list = list.tail;
        }
    }

//     /** Adds the new TapTriplet "entry" into the given "toList".
//      * If "toList" is null, it cannot be modified to contain the given "entry",
//      * therefore it is advised that "toList" is built with and always starts with a null entry */
//     public static <KEYTYPE, VALTYPE1, VALTYPE2> void
//         addEntry(TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2> entry,
//                  TapList<TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2>> toList) {
//         toList.tail = new TapList<>(entry, toList.tail) ;
//     }

//     /** Return the first TapTriplet entry in "list" whose first field equals "key" */
//     public static <KEYTYPE, VALTYPE1, VALTYPE2> TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2>
//         getEntry(KEYTYPE key, TapList<TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2>> list) {
//         TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2> entry = null ;
//         while (entry==null && list!=null) {
//             if (list.head!=null && list.head.first==key) entry = list.head ;
//             list = list.tail ;
//         }
//         return entry ;
//     }

//     /** Returns and removes the first TapTriplet entry in "toList" whose first field equals "key".
//      * The first entry in the given "toList" cannot be removed,
//      * therefore it is advised that "toList" is built with and always starts with a null entry */
//     public static <KEYTYPE, VALTYPE1, VALTYPE2> TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2>
//         delEntry(KEYTYPE key, TapList<TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2>> toList) {
//         TapTriplet<KEYTYPE, VALTYPE1, VALTYPE2> entry = null ;
//         while (entry==null && toList.tail!=null) {
//             if (toList.tail.head!=null && toList.tail.head.first==key) {
//                 entry = toList.tail.head ;
//                 toList.tail = toList.tail.tail ;
//             } else {
//                 toList = toList.tail ;
//             }
//         }
//         return entry ;
//     }

    /**
     * Return the element matching "key" in the association list "list".
     * Comparison is done with == .
     *
     * @param list The association list, assumed to be a list of TapPair.
     * @return null if no matching association is found.
     */
    public static <KEYTYPE, VALUETYPE> TapPair<KEYTYPE, VALUETYPE>
    assq(KEYTYPE key, TapList<TapPair<KEYTYPE, VALUETYPE>> list) {
        TapPair<KEYTYPE, VALUETYPE> found = null;
        while (found == null && list != null) {
            if (list.head != null && list.head.first == key) {
                found = list.head;
            }
            list = list.tail;
        }
        return found;
    }

    /**
     * Return the value in the element matching "key" in the association list "list".
     * Comparison is done with == .
     *
     * @param list The association list, assumed to be a list of TapPair
     * @return null if no matching association is found.
     */
    public static <KEYTYPE, VALUETYPE> VALUETYPE
    cassq(KEYTYPE key, TapList<TapPair<KEYTYPE, VALUETYPE>> list) {
        TapPair<KEYTYPE, VALUETYPE> found = TapList.assq(key, list);
        return found == null ? null : found.second;
    }

    /**
     * Return the first element containing "value" in the association list "list".
     * Comparison is done with == .
     *
     * @param list The association list, assumed to be a list of TapPair
     * @return null if no matching association is found.
     */
    public static <KEYTYPE, VALUETYPE> TapPair<KEYTYPE, VALUETYPE>
    rassq(VALUETYPE value, TapList<TapPair<KEYTYPE, VALUETYPE>> list) {
        TapPair<KEYTYPE, VALUETYPE> found = null;
        while (found == null && list != null) {
            if (list.head != null && list.head.second == value) {
                found = list.head;
            }
            list = list.tail;
        }
        return found;
    }

    /**
     * @param list The association list, assumed to be a list of TapPair,
     *             with first elements being TapIntList
     * @return null if no matching association is found.
     * Comparison is done with == .
     */
    public static <T> TapPair<TapIntList, T> assqOne(int key, TapList<TapPair<TapIntList, T>> list) {
        TapPair<TapIntList, T> found = null;
        while (found == null && list != null) {
            if (list.head != null && list.head.first.head == key) {
                found = list.head;
            }
            list = list.tail;
        }
        return found;
    }

    public static TapPair<Tree, Tree> assqTree(Tree tree, TapList<TapPair<Tree, Tree>> list) {
        while (list != null &&
                !list.head.first.equalsTree(tree)) {
            list = list.tail;
        }
        return list == null ? null : list.head;
    }

    /**
     * Same as assq, but looks for "string" with String.equals().
     */
    public static <T> TapPair<String, T> assqString(String string, TapList<TapPair<String, T>> list) {
        while (list != null && !list.head.first.equals(string)) {
            list = list.tail;
        }
        return list == null ? null : list.head;
    }

    /**
     * Same as cassq, but looks for "string" with String.equals().
     */
    public static <T> T cassqString(String string, TapList<TapPair<String, T>> list) {
        TapPair<String, T> found = TapList.assqString(string, list);
        return found == null ? null : found.second;
    }

    /**
     * Same as rassq, but looks for "string" with String.equals().
     */
    public static TapPair<String, String> rassqString(String string, TapList<TapPair<String, String>> list) {
        while (list != null && !list.head.second.equals(string)) {
            list = list.tail;
        }
        return list == null ? null : list.head;
    }

    /**
     * Same as assqString, but the association list is made of TapTriplet.
     */
    public static <T, U> TapTriplet<String, T, U> assqStringInTriplet(
            String string, TapList<TapTriplet<String, T, U>> list) {
        while (list != null && !list.head.first.equals(string)) {
            list = list.tail;
        }
        return list == null ? null : list.head;
    }

    /**
     * @param list The association list, assumed to be a list of Int2ZoneInfo.
     * @return null if no matching association is found.
     * Comparison is done with == .
     */
    public static Int2ZoneInfo assq(int key, TapList<Int2ZoneInfo> list) {
        Int2ZoneInfo found = null;
        while (found == null && list != null) {
            if (list.head != null && list.head.getZoneNumber() == key) {
                found = list.head;
            }
            list = list.tail;
        }
        return found;
    }

    /**
     * @param list The association list, assumed to be a list of Int2ZoneInfo.
     * @return null if no matching association is found.
     * Comparison is done with == .
     */
    public static ZoneInfo cassq(int key, TapList<Int2ZoneInfo> list) {
        Int2ZoneInfo found = TapList.assq(key, list);
        return found == null ? null : found.getZoneInfo();
    }

    /**
     * @return the rank'th element in "list".
     * When rank=0, this returns the first element.
     */
    public static Object nth(TapList<?> list, int rank) {
        while (list != null && rank != 0) {
            --rank;
            list = list.tail;
        }
        return list == null ? null : list.head;
    }

    /**
     * @return a TapList containing all elements
     * of "list" that are not in "delList".
     * Caution: the result shares structure with "list".
     */
    public static <T> TapList<T> deleteAll(TapList<T> delList, TapList<T> list) {
        TapList<T> toResult = new TapList<>(null, list);
        TapList<T> inResult = toResult;
        while (inResult.tail != null) {
            if (contains(delList, inResult.tail.head)) {
                inResult.tail = inResult.tail.tail;
            } else {
                inResult = inResult.tail;
            }
        }
        return toResult.tail;
    }

    /**
     * @return a copy of "list" with the n-th element deleted
     * Elements are counted from 0 up.
     * Caution: the result shares structure with "list".
     */
    public static <T> TapList<T> deleteNth(TapList<T> list, int n) {
        TapList<T> hdResult = new TapList<>(null, null);
        TapList<T> tlResult = hdResult;
        while (list != null) {
            if (n <= 0) {
                tlResult.tail = list.tail;
                list = null;
            } else {
                tlResult = tlResult.placdl(list.head);
                --n;
                list = list.tail;
            }
        }
        return hdResult.tail;
    }

    /**
     * @return a TapList containing all elements
     * of "list" that are not == to "delElem".
     * Caution: the result shares structure with "list".
     */
    public static <T> TapList<T> delete(T delElem, TapList<T> list) {
        TapList<T> toResult = new TapList<>(null, list);
        TapList<T> inResult = toResult;
        while (inResult.tail != null) {
            if (inResult.tail.head == delElem) {
                inResult.tail = inResult.tail.tail;
            } else {
                inResult = inResult.tail;
            }
        }
        return toResult.tail;
    }

    /** Variant of delete() that does not modify the given list.
     * Still, the resulting list shares a part with "list".
     */
    public static <T> TapList<T> safeDelete(T delElem, TapList<T> list) {
        if (list==null)
            return null ;
        else if (list.head==delElem)
            return list.tail ;
        else
            return new TapList<>(list.head, safeDelete(delElem, list.tail)) ;
    }

    /**
     * @return a TapList containing all elements
     * of "list" that are not String.equals() to "delElem".
     * Caution: the result shares structure with "list".
     */
    public static TapList<String> deleteString(String delElem, TapList<String> list) {
        TapList<String> toResult = new TapList<>(null, list);
        TapList<String> inResult = toResult;
        while (inResult.tail != null) {
            if (inResult.tail.head != null && inResult.tail.head.equals(delElem)) {
                inResult.tail = inResult.tail.tail;
            } else {
                inResult = inResult.tail;
            }
        }
        return toResult.tail;
    }

    /**
     * @return a TapList containing all elements
     * of "list" that are a String containing "delElem".
     * Caution: the result shares structure with "list".
     */
    public static TapList<String> deleteContainsString(String delElem, TapList<String> list) {
        TapList<String> toResult = new TapList<>(null, list);
        TapList<String> inResult = toResult;
        while (inResult.tail != null) {
            if (inResult.tail.head != null &&
                    inResult.tail.head.startsWith(delElem)) {
                inResult.tail = inResult.tail.tail;
            } else {
                inResult = inResult.tail;
            }
        }
        return toResult.tail;
    }

    /**
     * @return a new TapList, concatenation of "list1" followed by "list2".
     * Caution: the result shares structure with "list2".
     */
    public static <T> TapList<T> append(TapList<T> list1, TapList<T> list2) {
        if (list1 == null) {
            return list2;
        } else {
            TapList<T> res = new TapList<>(null, list2);
            TapList<T> inRes = res;
            while (list1 != null) {
                inRes = inRes.placdl(list1.head);
                list1 = list1.tail;
            }
            return res.tail;
        }
    }

    /**
     * Prepends the elements of "newElems" in front of "oldList",
     * discarding duplicates. @return the resulting list.
     * So far, we don't try to preserve the order of newElems.
     */
    public static <T> TapList<T> prependNoDups(TapList<T> newElems, TapList<T> oldList) {
        while (newElems != null) {
            if (!contains(oldList, newElems.head)) {
                oldList = new TapList<>(newElems.head, oldList);
            }
            newElems = newElems.tail;
        }
        return oldList;
    }

    /**
     * @return a new TapList containing all elements of "list1" or "list2".
     */
    public static <T> TapList<T> addAll(TapList<T> list1, TapList<T> list2) {
        TapList<T> result = append(list2, null);
        while (list1 != null) {
            if (!contains(result, list1.head)) {
                result = new TapList<>(list1.head, result);
            }
            list1 = list1.tail;
        }
        return result;
    }

    /**
     * @return the set union of "list1" and "list2".
     * "list1" is assumed to be already a set (no duplicates).
     * Apart for removed duplicate elements, the result preserves
     * the order of "list1" followed by "list2".
     */
    public static <T> TapList<T> union(TapList<T> list1, TapList<T> list2) {
        TapList<T> toResult = new TapList<>(null, list1);
        TapList<T> inResult;
        while (list2 != null) {
            inResult = toResult;
            while (inResult != null && inResult.tail != null) {
                if (inResult.tail.head == list2.head) {
                    inResult = null;
                } else {
                    inResult = inResult.tail;
                }
            }
            if (inResult != null) {
                inResult.placdl(list2.head);
            }
            list2 = list2.tail;
        }
        return toResult.tail;
    }

    /**
     * Same as "union()", but equality test is on Strings.
     */
    public static TapList<String> unionString(TapList<String> list1, TapList<String> list2) {
        TapList<String> toResult = new TapList<>(null, list1);
        TapList<String> inResult;
        while (list2 != null) {
            inResult = toResult;
            while (inResult != null && inResult.tail != null) {
                if (inResult.tail.head.equals(list2.head)) {
                    inResult = null;
                } else {
                    inResult = inResult.tail;
                }
            }
            if (inResult != null) {
                inResult.placdl(list2.head);
            }
            list2 = list2.tail;
        }
        return toResult.tail;
    }

    /**
     * @return the last element of the given list.
     */
    public static <T> T last(TapList<T> list) {
        if (list == null) {
            return null;
        }
        while (list.tail != null) {
            list = list.tail;
        }
        return list.head;
    }

    /**
     * @return the last cell of the given list, i.e. the one that contains its last element.
     */
    public static <T> TapList<T> toLast(TapList<T> list) {
        if (list == null) {
            return null;
        }
        while (list.tail != null) {
            list = list.tail;
        }
        return list;
    }

    /**
     * Removes the last element of the given "list".
     */
    public static <T> TapList<T> removeLast(TapList<T> list) {
        if (list == null || list.tail == null) {
            return null;
        }
        TapList<T> toTail = list;
        while (toTail.tail.tail != null) {
            toTail = toTail.tail;
        }
        toTail.tail = null;
        return list;
    }

    /**
     * @return the list made of the given "list" plus the "object" at the end.
     * Modifies the original "list" when non-null.
     */
    public static <T> TapList<T> addLast(TapList<T> list, T object) {
        if (list == null) {
            list = new TapList<>(object, null);
        } else {
            TapList.toLast(list).tail = new TapList<>(object, null);
        }
        return list;
    }

    /**
     * coupe la liste en 2n+1 sous-listes
     * n longueur de la liste des positions de coupure
     * suppose que listRank est une liste ordonnee croissante des positions
     * retourne un tableau de 2n+1 Object
     * les indices impairs du tableau correspondent aux contenus reperes par listRank.
     */
    public static TapList<Instruction>[] split(TapList<Instruction> list, TapIntList listRank) {
        int len = TapIntList.length(listRank);
        TapList<Instruction> result1;
        @SuppressWarnings("unchecked")
        TapList<Instruction>[] resultList = new TapList[2 * len + 1];
        int i = 0;
        int rank;
        int position;
        position = 0;
        while (listRank != null && list != null) {
            result1 = null;
            rank = listRank.head;
            while (list != null && i < rank) {
                result1 = TapList.append(result1,
                        new TapList<>(list.head, null));
                list = list.tail;
                i = i + 1;
            }
            resultList[position] = result1;
            assert list != null;
            resultList[position + 1] = new TapList<>(list.head, null);
            listRank = listRank.tail;
            position = position + 2;
            list = list.tail;
            i = i + 1;
        }
        resultList[position] = list;
        return resultList;
    }

    /**
     * @return a copy of the given list.
     */
    public static <T> TapList<T> copy(TapList<T> list) {
        TapList<T> result = new TapList<>(null, null);
        TapList<T> tlResult = result;
        while (list != null) {
            tlResult = tlResult.placdl(list.head);
            list = list.tail;
        }
        return result.tail;
    }

    /**
     * @return a reversed copy of the given "list".
     */
    public static <T> TapList<T> reverse(TapList<T> list) {
        TapList<T> result = null;
        while (list != null) {
            result = new TapList<>(list.head, result);
            list = list.tail;
        }
        return result;
    }

    /**
     * Physically reverses the given "list" and returns it.
     */
    public static <T> TapList<T> nreverse(TapList<T> list) {
        TapList<T> tmp;
        TapList<T> result = null;
        while (list != null) {
            tmp = list.tail;
            list.tail = result;
            result = list;
            list = tmp;
        }
        return result;
    }

    /**
     * @return a copy of "list" with duplicated elements removed.
     */
    public static <T> TapList<T> unique(TapList<T> list) {
        TapList<T> result = new TapList<>(null, null);
        TapList<T> tail = result;
        while (list != null) {
            if (!contains(result, list.head)) {
                tail = tail.placdl(list.head);
            }
            list = list.tail;
        }
        return result.tail;
    }

    /**
     * Convert an array of Object to a TapList.
     */
    public static <T> TapList<T> fromObjectArray(T[] array) {
        TapList<T> list = null;
        if (array != null) {
            for (int i = array.length - 1; i >= 0; i--) {
                list = new TapList<>(array[i], list);
            }
        }
        return list;
    }

    /**
     * Removes the given "lcValue" from the given list of "extraInfo".
     * To be made cleaner and less specific.
     */
    public static TapList<String> cleanExtraInfoValue(TapList<String> extraInfo, String lcValue) {
        TapList<String> toExtraInfo = new TapList<>(null, extraInfo);
        TapList<String> inExtraInfo = toExtraInfo;
        String infoString;
        while (inExtraInfo.tail != null) {
            infoString = inExtraInfo.tail.head;
            if (infoString != null && infoString.equalsIgnoreCase(lcValue)) {
                inExtraInfo.tail = inExtraInfo.tail.tail;
            } else {
                inExtraInfo = inExtraInfo.tail;
            }
        }
        return toExtraInfo.tail;
    }

    public static void replaceExtraInfoValue(TapList<String> extraInfo,
                                             String lcValue, String lcValue2) {
        while (extraInfo != null) {
            String infoString = extraInfo.head;
            if (infoString != null && infoString.equalsIgnoreCase(lcValue)) {
                extraInfo.head = lcValue2;
            }
            extraInfo = extraInfo.tail;
        }
    }

    protected static boolean equalsImportedModules(TapList<Instruction> l1, TapList<Instruction> l2) {
        boolean result = TapList.length(l1) == TapList.length(l2);
        if (result) {
            while (l1 != null && l2 != null && result) {
                result = l1.head.tree.equalsTree(l2.head.tree);
                l1 = l1.tail;
                l2 = l2.tail;
            }
        }
        return result;
    }

    /**
     * Parses an identifier String of the form {@code toto.x.z} or {@code toto%x%z} into
     * a list of Strings {@code ("toto" "x" "y")}. To handle pointers, parsing of {@code toto**.y}
     * returns {@code ("toto" "*" "*" "y")}, representing (C-style) {@code (*toto)->y}.
     */
    public static TapList<String> parseIdentifier(String ident) {
        StringTokenizer identTokenizer = new StringTokenizer(ident, ".%");
        TapList<String> hdParsed = new TapList<>(null, null);
        TapList<String> tlParsed = hdParsed;
        String varNameOptStar;
        int derefLevel;
        while (identTokenizer.hasMoreElements()) {
            varNameOptStar = identTokenizer.nextToken();
            derefLevel = 0;
            while (!varNameOptStar.isEmpty() && varNameOptStar.charAt(varNameOptStar.length() - 1) == '*') {
                ++derefLevel;
                varNameOptStar = varNameOptStar.substring(0, varNameOptStar.length() - 1);
            }
            tlParsed = tlParsed.placdl(varNameOptStar);
            while (derefLevel != 0) {
                tlParsed = tlParsed.placdl("*");
                --derefLevel;
            }
        }
        return hdParsed.tail;
    }

// ********** Special part for TapList TREES *********

    /**
     * @return true if the two TapList-trees are the same, even if not ==.
     */
    public static boolean sameTree(TapList<?> tree1, TapList<?> tree2) {
        return sameTreeRec(tree1, tree2, new TapList<>(null, null)) ;
    }

    /**
     * Recursive utility for sameTree().
     */
    private static boolean sameTreeRec(TapList<?> tree1, TapList<?> tree2, TapList<TapPair<TapList<?>, TapList<?>>> dejaVu) {
        boolean mayBeSame = true ;
        while (mayBeSame && tree1!=tree2 && !inDejaVu(tree1, tree2, dejaVu.tail)) {
            mayBeSame = tree1!=null && tree2!=null ;
            if (mayBeSame) {
                dejaVu.placdl(new TapPair<>(tree1, tree2));
                if (tree1.head instanceof TapList) {
                    mayBeSame = (tree2.head instanceof TapList && sameTreeRec((TapList)tree1.head, (TapList)tree2.head, dejaVu)) ;
                } else if (tree1.head instanceof TapIntList) {
                    mayBeSame = (tree2.head instanceof TapIntList && TapIntList.equalLists((TapIntList)tree1.head, (TapIntList)tree2.head)) ;
                } else if (tree1.head instanceof BoolVector) {
                    // Not finished: needs BoolVector's true "length" !!
                    mayBeSame = (tree2.head instanceof BoolVector
                                 && ((BoolVector)tree1.head).grossLength()==((BoolVector)tree2.head).grossLength()
                                 && ((BoolVector)tree1.head).equals((BoolVector)tree2.head, ((BoolVector)tree2.head).grossLength())) ;
                } else { //Default case includes Boolean
                    mayBeSame = (tree1.head==tree2.head) ;
                }
                tree1 = tree1.tail;
                tree2 = tree2.tail;
            }
        }
        return mayBeSame ;
    }

    // Question : isn't this the same as containsObjectPair() ?
    private static boolean inDejaVu(TapList<?> tree1, TapList<?> tree2, TapList<TapPair<TapList<?>, TapList<?>>> dejaVu) {
        boolean found = false ;
        while (!found && dejaVu!=null) {
            found = (dejaVu.head.first==tree1 && dejaVu.head.second==tree2) ;
            dejaVu = dejaVu.tail ;
        }
        return found ;
    }

    /**
     * @return a deep copy of this TapList, which must be a tree of
     * TapIntList's, Boolean's, or ToBool's .
     * Takes care of infinite trees that come from variables of recursive type.
     */
    public static TapList<?> copyTree(TapList<?> model) {
        return copyTreeRec(model, new TapList<>(null, null));
    }

    /**
     * Recursive utility for copyTree().
     */
    private static TapList<?> copyTreeRec(TapList<?> model, TapList<TapPair<TapList<?>, TapList<?>>> dejaVu) {
        TapList toResult = new TapList<>(null, null);
        TapList inResult = toResult;
        TapPair<TapList<?>, TapList<?>> foundDejaVu;
        while (model != null) {
            if ((foundDejaVu = TapList.assq(model, dejaVu)) != null) {
                inResult.tail = foundDejaVu.second;
                model = null;
            } else {
                inResult.tail = new TapList<>(null, null);
                inResult = inResult.tail;
                dejaVu.placdl(new TapPair<>(model, inResult));
                if (model.head != null) {
                    if (model.head instanceof TapList) {
                        inResult.head = copyTreeRec((TapList) model.head, dejaVu);
                    } else if (model.head instanceof TapIntList) {
                        inResult.head = TapIntList.copy((TapIntList) model.head);
                    } else if (model.head instanceof ToBool) {
                        inResult.head = new ToBool(((ToBool) model.head).get());
                    } else {//Default case includes Boolean
                        inResult.head = model.head;
                    }
                }
                model = model.tail;
            }
        }
        return toResult.tail;
    }

    /**
     * @return a copy of the given "leaf", which is a leaf of a TapList-tree,
     * i.e. a  TapIntList, BoolVector, ToBool, or Boolean.
     */
    private static Object copyLeaf(Object leaf) {
        if (leaf instanceof TapIntList) {
            return TapIntList.copy((TapIntList) leaf);
        } else if (leaf instanceof ToBool) {
            return new ToBool(((ToBool) leaf).get());
        } else if (leaf instanceof BoolVector) {
            return ((BoolVector) leaf).copy();
        } else {
            return leaf;
        }
    }

    /**
     * @return a deep copy of this TapList, which must be a tree of
     * TapIntList's, Boolean's, or ToBool's .
     * Shares (i.e. does not copy) the leaves of the tree.
     * Does not copy pointer destination part.
     * Therefore there is no risk of infinite looping.
     */
    public static TapList<?> copyTreeNoPointed(TapList<?> model) {
        if (model == null) {
            return null;
        }
        if (model.head instanceof TapList) {
            TapList hdCopy = new TapList<>(null, null);
            TapList tlCopy = hdCopy;
            while (model != null) {
                tlCopy = tlCopy.placdl(copyTreeNoPointed((TapList) model.head));
                model = model.tail;
            }
            return hdCopy.tail;
        } else {
            return new TapList<>(model.head, null);
        }
    }

    /** OR-accumulates all leaves of tree, which are BoolVector's,
     * into a single BoolVector and returns it.
     * Does not go inside pointers, therefore there is no risk of infinite looping. */
    public static BoolVector cumulOr(TapList<?> tree) {
        ToObject<BoolVector> toCumul = new ToObject<>(null) ;
        coRec(tree, toCumul) ;
        return toCumul.obj() ;
    }
    private static void coRec(TapList<?> tree, ToObject<BoolVector> toCumul) {
        if (tree!=null) {
            if (tree.head instanceof TapList) {
                while (tree!=null) {
                    coRec((TapList)tree.head, toCumul) ;
                    tree = tree.tail ;
                }
            } else if (tree.head instanceof BoolVector) {
                if (toCumul.obj()==null) {
                    toCumul.setObj(((BoolVector)tree.head).copy()) ;
                } else {
                    toCumul.obj().cumulOr((BoolVector)tree.head) ;
                }
            } // one may add new leaf types here to extend this method e.g. to trees of Booleans...
        }
    }


    /** Remove zones Null, UnknownDests and IO (generally 0, 1, and 2) in all TapIntLists in tree */
    public static void removeNonWritableZones(TapList tree) {
        rnwzRec(tree, new TapList(null, null)) ;
    }

    /** Recursive utility for removeNonWritableZones */
    private static void rnwzRec(TapList tree, TapList<TapList> dejaVu) {
        while (tree!=null) {
            if (TapList.contains(dejaVu, tree)) {
                tree = null ;
            } else {
                dejaVu.placdl(tree);
                if (tree.head instanceof TapList) {
                    rnwzRec((TapList)tree.head, dejaVu) ;
                } else if (tree.head instanceof TapIntList) {
                    tree.head = (Object)rnwzLeaf((TapIntList)tree.head) ;
                }
            }
            tree = tree.tail ;
        }
    }

    private static TapIntList rnwzLeaf(TapIntList zones) {
        TapIntList hdNewZones = new TapIntList(-1, null) ;
        TapIntList inNewZones = hdNewZones ;
        while (zones!=null) {
            if (zones.head!=0 && zones.head!=1 && zones.head!=2)
                inNewZones = inNewZones.placdl(zones.head) ;
            zones = zones.tail ;
        }
        return hdNewZones.tail ;
    }

    /**
     * @return (or creates and returns) the sub-tree of the given tree "infoTree"
     * which is accessed through the given "accessTree" (e.g. %x[3]%toto).
     * When "createIfAbsent" is true, this function never returns null !
     * When "createIfAbsent" is false, doesn't modify "infoTree" and may return null if nothing found.
     * This function assumes that it is given a non-null "infoTree" !
     * CONVENTION [Jan 2008] on the placement of pointer destination info:
     * REMAIN IN SYNC with CONVENTION in DataFlowAnalyzer.includePointedElementsInTreeRec().
     */
    public static TapList<?> getSetFieldLocation(TapList infoTree, Tree accessTree, boolean createIfAbsent) {
        if (accessTree == null) {
            return infoTree;
        } else {
            switch (accessTree.opCode()) {
                case ILLang.op_arrayAccess:
                case ILLang.op_expressions:
                    return getSetFieldLocation(infoTree, accessTree.down(1), createIfAbsent);
                case ILLang.op_address:
                    infoTree = getSetFieldLocation(infoTree, accessTree.down(1), createIfAbsent);
                    infoTree = new TapList(null, infoTree) ;
                    return infoTree ;
                case ILLang.op_fieldAccess: {
                    infoTree = getSetFieldLocation(infoTree, accessTree.down(1), createIfAbsent);
                    int fieldRank = ILUtils.getFieldRank(accessTree.down(2));
                    Object resObj = TapList.nth(infoTree, fieldRank);
                    if (resObj instanceof TapList) {
                        return (TapList) resObj;
                    } else if (createIfAbsent && infoTree != null) {
                        Boolean previousBool = null;
                        if (infoTree.head instanceof Boolean) {
                            previousBool = (Boolean) infoTree.head;
                            infoTree.head = new TapList<>(previousBool, null);
                        } else if (infoTree.head == null) {
                            infoTree.head = new TapList<>(null, null);
                        }
                        while (fieldRank > 0) {
                            if (infoTree.tail == null) {
                                infoTree.tail = new TapList<>(new TapList<>(previousBool, null), null);
                            }
                            infoTree = infoTree.tail;
                            fieldRank--;
                        }
                        if (!(infoTree.head instanceof TapList)) {
                            infoTree.head = new TapList<>(null, null);
                        }
                        return (TapList) infoTree.head;
                    } else {
                        return infoTree;
                    }
                }
                case ILLang.op_pointerAccess:
                    infoTree = getSetFieldLocation(infoTree, accessTree.down(1), createIfAbsent);
                    if (infoTree == null) {
                        return null;
                    } else {
                        if (infoTree.tail == null && createIfAbsent) {
                            infoTree.tail = new TapList<>(null, null);
                        }
                        return infoTree.tail;
                    }
                case ILLang.op_allocate:
                case ILLang.op_ident:
                case ILLang.op_none:
                    return infoTree;
                default:
                    TapEnv.printlnOnTrace("Tool: (getSetFieldLocation) Unexpected operator: "
                            + accessTree.opName());
                    return infoTree;
            }
        }
    }

    /** Modifies inside the given tree, which is a TapList tree of Booleans,
     * replacing all Boolean leaves with their negation. */
    public static void negateBooleans(TapList tree) {
        negateBooleansRec(tree, new TapList<>(null, null)) ;
    }
    private static void negateBooleansRec(TapList tree, TapList<TapList<?>> dejaVu) {
        while (tree!=null) {
            if (TapList.contains(dejaVu.tail, tree)) {
                tree = null ;
            } else {
                dejaVu.placdl(tree);
                if (tree.head instanceof TapList) {
                    negateBooleansRec((TapList)tree.head, dejaVu) ;
                } else if (tree.head instanceof Boolean) {
                    tree.head = (tree.head==Boolean.TRUE ? Boolean.FALSE : Boolean.TRUE) ;
                }
            }
            tree = tree.tail ;
        }
    }

    /**
     * @return true when all the (TapIntList) leaves are singletons.
     * Takes care of infinite cycles in the tree.
     */
    public static boolean allSingletonLeaves(TapList<?> tree) {
        return allSingletonLeavesRec(tree, new TapList<>(null, null));
    }

    private static boolean allSingletonLeavesRec(TapList<?> tree, TapList<TapList<?>> dejaVu) {
        boolean result = true;
        while (result && tree != null) {
            if (TapList.contains(dejaVu.tail, tree)) {
                tree = null;
            } else {
                dejaVu.placdl(tree);
                if (tree.head != null) {
                    if (tree.head instanceof TapList) {
                        result = allSingletonLeavesRec((TapList) tree.head, dejaVu);
                    } else if (((TapIntList) tree.head).tail != null) {
                        result = false;
                    }
                }
                tree = tree.tail;
            }
        }
        return result;
    }

    /**
     * @return true if at least one leaf of the given "boolsTree" is
     * a TapIntList with more than one element.
     * Takes care of infinite cycles in the tree.
     * boolsTree must be a TapList tree of TapIntList's.
     */
    public static boolean oneNonSingleton(TapList<?> boolsTree) {
        return oneNonSingletonRec(boolsTree, new TapList<>(null, null));
    }

    private static boolean oneNonSingletonRec(TapList<?> boolsTree, TapList<TapList<?>> dejaVu) {
        boolean found = false;
        while (!found && boolsTree != null) {
            if (TapList.contains(dejaVu.tail, boolsTree)) {
                boolsTree = null;
            } else {
                dejaVu.placdl(boolsTree);
                if (boolsTree.head instanceof TapList) {
                    found = oneNonSingletonRec((TapList) boolsTree.head, dejaVu);
                } else if (boolsTree.head == null || boolsTree.head instanceof TapIntList) {
                    found = boolsTree.head == null || ((TapIntList) boolsTree.head).tail != null;
                }
                boolsTree = boolsTree.tail;
            }
        }
        return found;
    }

    /**
     * Utility for RefDescriptor ignoreXXX mechanism. Not very general yet.
     */
    public static boolean trueForAll(TapList<?> boolsTree) {
        return boolsTree != null && boolsTree.tail == null && boolsTree.head instanceof Boolean && (Boolean) boolsTree.head;
    }

    /**
     * @return true if at least one leaf in the HEAD of the
     * given TapList of Boolean's "boolsTree" is true.
     */
    public static boolean oneTrueInHead(TapList<?> boolsTree) {
        return boolsTree != null && oneTrue(new TapList<>(null, new TapList<>(boolsTree.head, null)));
    }

    /**
     * @return true if at least one leaf of the given "boolsTree" is true.
     * Takes care of infinite cycles in the tree.
     * boolsTree must be a TapList tree of Boolean's.
     */
    public static boolean oneTrue(TapList<?> boolsTree) {
        return oneTrueRec(boolsTree, new TapList<>(null, null));
    }

    /**
     * @return true if there is a Boolean.TRUE leaf inside the TapList-tree of Booleans boolsTree.
     */
    private static boolean oneTrueRec(TapList<?> boolsTree, TapList<TapList<?>> dejaVu) {
        boolean found = false;
        while (!found && boolsTree != null) {
            if (TapList.contains(dejaVu.tail, boolsTree)) {
                boolsTree = null;
            } else {
                dejaVu.placdl(boolsTree);
                if (boolsTree.head instanceof TapList) {
                    found = oneTrueRec((TapList) boolsTree.head, dejaVu);
                } else if (boolsTree.head instanceof Boolean) {
                    found = (Boolean) boolsTree.head;
                }
                boolsTree = boolsTree.tail;
            }
        }
        return found;
    }

    public static boolean oneFalse(TapList<?> boolsTree) {
        return oneFalseRec(boolsTree, new TapList<>(null, null));
    }

    /**
     * @return true if there is a Boolean.FALSE or a null leaf inside the TapList-tree of Booleans boolsTree.
     */
    private static boolean oneFalseRec(TapList<?> boolsTree, TapList<TapList<?>> dejaVu) {
        boolean found = false;
        while (!found && boolsTree != null) {
            if (TapList.contains(dejaVu.tail, boolsTree)) {
                boolsTree = null;
            } else {
                dejaVu.placdl(boolsTree);
                if (boolsTree.head instanceof TapList) {
                    found = oneFalseRec((TapList) boolsTree.head, dejaVu);
                } else if (boolsTree.head instanceof Boolean) {
                    found = !(Boolean) boolsTree.head;
                } else {// boolsTree.head == null
                    found = true;
                }
                boolsTree = boolsTree.tail;
            }
        }
        return found;
    }

    /**
     * Recursively accumulates tree "added" into tree "target", using operation "oper".
     *
     * @return the resulting tree, which in general is the new contents of "target".
     * Takes care of cycles in the trees.
     * The leaves of the tree may be all Boolean's or all BoolVector's or all TapIntList's.
     * The result will maybe share structure with the original "target",
     * but will never share structure with "added". In other words, the parts of "added"
     * which appear in the result will be deep copies.
     */
    public static TapList<?> cumulWithOper(TapList<?> target, Object added, int oper) {
        return cumulWithOperRec(target, added, oper, null, new TapList<>(null, null));
    }

    private static TapList<?> cumulWithOperRec(TapList<?> target, Object added, int oper,
                                               TapList<TapList> dejaVuTarget,
                                               TapList<TapPair<Object, TapList>> dejaVuAdded) {
        if (target != null) {
            if (contains(dejaVuTarget, target)) {
                return target;
            }
            dejaVuTarget = new TapList<>(target, dejaVuTarget);
        }
        TapList hdTarget = new TapList<>(null, target);
        TapList toTarget = hdTarget;
        while (added != null) {
            TapPair foundDejaVuAdded = TapList.assq(added, dejaVuAdded.tail);
            if (foundDejaVuAdded != null) {
                toTarget.tail = (TapList) foundDejaVuAdded.second;
                added = null;
            } else if (added instanceof TapList) {
                // Case 1 : target @= (hd.tl)
                TapList addedL = (TapList) added;
                if (toTarget.tail == null) {
                    toTarget.tail = new TapList<>(null, null);
                }
                dejaVuAdded.placdl(new TapPair<>(added, toTarget.tail));
                // Copy-expand the head of target (resp. added) if
                //  it doesn't match the head of added (resp. target):
                TapList addedHeadL = null;
                if (addedL.head instanceof TapList
                        && !(toTarget.tail.head instanceof TapList)) {
                    // target.head is less detailled than added.head => copy-expand target.head:
                    addedHeadL = (TapList) addedL.head;
                    int i = TapList.length(addedHeadL);
                    Object leafToExpand = toTarget.tail.head;
                    TapList expandedTarget = null;
                    while (i > 0) {
                        expandedTarget = new TapList<>(copyLeaf(leafToExpand), expandedTarget);
                        --i;
                    }
                    toTarget.tail.head = expandedTarget;
                    // Re-use the leafToExpand for accumulating with the rest of "added"
                    if (toTarget.tail.tail == null && addedL.tail != null) {
                        toTarget.tail.tail = new TapList<>(copyLeaf(leafToExpand), null);
                    }
                } else if (!(addedL.head instanceof TapList)
                        && toTarget.tail.head instanceof TapList) {
                    // added.head is less detailled than target.head => copy-expand added.head:
                    addedHeadL = null;
                    int i = TapList.length((TapList) toTarget.tail.head);
                    while (i > 0) {
                        addedHeadL = new TapList<>(copyLeaf(addedL.head), addedHeadL);
                        --i;
                    }
                } else if (addedL.head instanceof TapList) {
                    addedHeadL = (TapList) addedL.head;
                }
                if (toTarget.tail.head instanceof TapList) {
                    toTarget.tail.head =
                            cumulWithOperRec((TapList<?>) toTarget.tail.head, addedHeadL, oper,
                                    dejaVuTarget, dejaVuAdded);
                } else {
                    toTarget.tail.head =
                            cumulLeavesWithOper(toTarget.tail.head, addedL.head, oper);
                }
                toTarget = toTarget.tail;
                added = addedL.tail;
            } else {
                // Case 2 : target @= Val
                if (toTarget.tail == null) {
                    added = null;
                } else if (toTarget.tail.head instanceof TapList) {
                    toTarget.tail.head =
                            cumulWithOperRec((TapList<?>) toTarget.tail.head, added, oper,
                                    dejaVuTarget, dejaVuAdded);
                } else {
                    toTarget.tail.head =
                            cumulLeavesWithOper(toTarget.tail.head, added, oper);
                }
                toTarget = toTarget.tail;
            }
        }
        return hdTarget.tail;
    }

    private static Object cumulLeavesWithOper(Object target, Object added, int oper) {
        if (added == null) {
            return target;
        }
        if (added instanceof TapIntList) {
            if (target == null) {
                if (oper != SymbolTableConstants.CUMUL_MINUS) {
                    target = TapIntList.copy((TapIntList) added);
                }
            } else {
                if (oper == SymbolTableConstants.CUMUL_OR) {
                    target = TapIntList.quickUnion((TapIntList) target, (TapIntList) added);
                } else if (oper == SymbolTableConstants.CUMUL_AND) {
                    target = TapIntList.intersection((TapIntList) target, (TapIntList) added);
                } else if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    target = TapIntList.minus((TapIntList) target, (TapIntList) added);
                }
            }
        } else if (added instanceof ToBool) {
            if (target == null) {
                if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    target = new ToBool(!((ToBool) added).get());
                } else {
                    target = new ToBool(((ToBool) added).get());
                }
            } else {
                if (oper == SymbolTableConstants.CUMUL_OR) {
                    if (((ToBool) added).get()) {
                        target = new ToBool(true);
                    }
                } else if (oper == SymbolTableConstants.CUMUL_AND) {
                    if (!((ToBool) added).get()) {
                        target = new ToBool(false);
                    }
                } else if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    if (((ToBool) added).get()) {
                        target = new ToBool(false);
                    }
                }
            }
        } else if (added instanceof Boolean) {
            if (target == null) {
                if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    target = (Boolean) added ? Boolean.FALSE : Boolean.TRUE;
                } else {
                    target = added;
                }
            } else {
                if (oper == SymbolTableConstants.CUMUL_OR) {
                    if ((Boolean) added) {
                        target = Boolean.TRUE;
                    }
                } else if (oper == SymbolTableConstants.CUMUL_AND) {
                    if (!(Boolean) added) {
                        target = Boolean.FALSE;
                    }
                } else if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    if ((Boolean) added) {
                        target = Boolean.FALSE;
                    }
                }
            }
        } else if (added instanceof BoolVector) {
            if (target == null) {
                if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    target = ((BoolVector) added).not();
                } else {
                    target = ((BoolVector) added).copy();
                }
            } else {
                if (oper == SymbolTableConstants.CUMUL_OR) {
                    ((BoolVector) target).cumulOr((BoolVector) added);
                } else if (oper == SymbolTableConstants.CUMUL_AND) {
                    ((BoolVector) target).cumulAnd((BoolVector) added);
                } else if (oper == SymbolTableConstants.CUMUL_MINUS) {
                    ((BoolVector) target).cumulMinus((BoolVector) added);
                }
            }
        } else {
            TapEnv.printlnOnTrace("Tool: (Cumul Info trees) class not implemented:" + added);
        }
        return target;
    }

    /**
     * Sweeps through the given (TapList tree of TapIntList) "zonesTree", replacing
     * each integer by the value at this rank (plus offset) in the given array "values".
     */
    public static void replaceIntsWithValueInts(TapList zonesTree, int[] values, int offset) {
        while (zonesTree != null) {
            if (zonesTree.head instanceof TapList) {
                replaceIntsWithValueInts((TapList) zonesTree.head, values, offset);
            } else {
                TapIntList zonesList = (TapIntList) zonesTree.head;
                while (zonesList != null) {
                    zonesList.head = values[zonesList.head + offset];
                    zonesList = zonesList.tail;
                }
            }
            zonesTree = zonesTree.tail;
        }
    }

    /**
     * Adds all integers in TapIntList "newInts" into each TapIntList leaf of the TapList tree "zonesTree".
     */
    public static void addIntoLeaves(TapList zonesTree, TapIntList newInts) {
        while (zonesTree != null) {
            if (zonesTree.head instanceof TapList) {
                addIntoLeaves((TapList) zonesTree.head, newInts);
            } else {
                zonesTree.head = TapIntList.quickUnion((TapIntList) zonesTree.head, newInts);
            }
            zonesTree = zonesTree.tail;
        }
    }

    /**
     * Replaces the tail of this list by a new list cell, whose head
     * is "obj", and whose tail is the previous tail of this list.
     *
     * @return this new list cell.
     */
    public TapList<T> placdl(T obj) {
        return tail = new TapList<>(obj, tail);
    }

    /**
     * @return the rank of the Object "obj" in the current list.
     * Comparison is done with == .
     * First is 0, and "not found" is -1.
     */
    public int rank(Object obj) {
        int rank = 0;
        TapList inList = this;
        while (inList != null && inList.head != obj) {
            ++rank;
            inList = inList.tail;
        }
        return inList == null ? -1 : rank;
    }

    /**
     * @return the rank of the string "str" in the current list.
     * Comparison is done with String.equals().
     * First is 0, and "not found" is -1.
     */
    public int rankString(String str) {
        int rank = 0;
        TapList inList = this;
        while (inList != null && !inList.head.equals(str)) {
            ++rank;
            inList = inList.tail;
        }
        return inList == null ? -1 : rank;
    }

    /**
     * Adds element "object" at the end of this list.
     */
    public void newR(T object) {
        TapList<T> inList = this;
        while (inList.tail != null) {
            inList = inList.tail;
        }
        inList.tail = new TapList<>(object, null);
    }

    public TapList<TapPair<Unit, Tree>> findExtraInfos(Unit unit) {
        TapList<TapPair<Unit, Tree>> result = null;
        TapList<TapPair<Unit, Tree>> tmpInfo = findExtraInfo(unit);
        if (tmpInfo != null) {
            result = new TapList<>(tmpInfo.head, null);
            while (tmpInfo != null) {
                tmpInfo = tmpInfo.tail;
                if (tmpInfo != null) {
                    tmpInfo = tmpInfo.findExtraInfo(unit);
                    if (tmpInfo != null) {
                        result = TapList.append(result, new TapList<>(tmpInfo.head, null));
                    }
                }
            }
        }
        return result;
    }

    /**
     * on a une TapList de TapPair (unit, tree)
     * returns une TapList dont le head est
     * la premiere TapPair trouve'e avec first == unit.
     */
    private TapList<TapPair<Unit, Tree>> findExtraInfo(Unit unit) {
        @SuppressWarnings("unchecked")
        TapList<TapPair<Unit, Tree>> tmpInfo = (TapList<TapPair<Unit, Tree>>) this;
        boolean found = tmpInfo.head.first == unit;
        while (tmpInfo.tail != null && !found) {
            found = tmpInfo.tail.head.first == unit;
            tmpInfo = tmpInfo.tail;
        }
        if (!found) {
            tmpInfo = null;
        }
        return tmpInfo;
    }

    /**
     * TapList implements Iterable.
     */
    @Override
    public java.util.Iterator<T> iterator() {
        TapListIterator<T> iter = new TapListIterator<>();
        iter.current = this;
        return iter;
    }

    /**
     * Prints the contents of this TapList onto TapEnv.curOutputStream().
     */
    public void dump() throws java.io.IOException {
        TapEnv.print(this.toStringRec(null, null, new TapList<>(null, null)));
    }

    @Override
    public String toString() {
        int dagSize = dagSize(new TapList<>(null, null));
        if (dagSize > 150) {
            return "enormousDag:" + dagSize;
        } else {
            return toStringRec(null, null, new TapList<>(null, null));
        }
    }

    public String toString(Object info) {
        return toStringRec(info, null, new TapList<>(null, null));
    }

    /**
     * Utility for toString. Accepts cycling objects and dags. Shows cycles and dags.
     */
    private String toStringRec(Object info, TapList<TapList> dejaVuSeeing, TapList<TapList> dejaVuSeen) {
        int dagRank;
        int cycleRank;
        if ((cycleRank = dejaVuSeeing == null ? -1 : dejaVuSeeing.rank(this)) != -1) {
            // +cycleLabel;
            return "CYCLE-" + (cycleRank + 1);
        } else if ((dagRank = dejaVuSeen.rank(this)) != -1) {
            return "DAG-" + dagRank;
        } else {
            TapList listTail = this;
            String result = null;
            cycleRank = -1;
            dagRank = -1;
            while (listTail != null
                    && (cycleRank = dejaVuSeeing == null ? -1 : dejaVuSeeing.rank(listTail)) == -1
                    && (dagRank = dejaVuSeen.rank(listTail)) == -1) {
                result = result == null ? "(" : result + " ";
                dejaVuSeeing = new TapList<>(listTail, dejaVuSeeing);
                if (listTail.head instanceof TapList) {
                    result = result + ((TapList) listTail.head).toStringRec(info, dejaVuSeeing, dejaVuSeen);
                } else {
                    if (listTail.head instanceof Tree) {
                        result = result + ILUtils.toString((Tree) listTail.head);
                    } else if (listTail.head instanceof BoolVector) {
                        if (info == null) {
                            result = result + listTail.head;
                        } else {
                            result = result + ((BoolVector) listTail.head).toString((int[]) info);
                        }
                    } else {
                        result = result + listTail.head;
                    }
                }
                listTail = listTail.tail;
            }
            if (listTail != null) {
                if (cycleRank != -1) {
                    result = result + " . " + "CYCLE-" + (cycleRank + 1);
                } else if (dagRank != -1) {
                    result = result + " . " + "DAG-" + dagRank;
                } else {
                    result = result + " . " + listTail;
                }
            }
            result = result + ')';
            dejaVuSeen.newR(this);
            if (this.head instanceof TapList && this.tail != null) {
                dejaVuSeen.newR(this.tail);
            }
            return result;
        }
    }

    public int dagSize(TapList<TapList> dejaVu) {
        TapList listTail = this;
        int result = 0;
        while (listTail != null && !TapList.contains(dejaVu.tail, listTail)) {
            dejaVu.placdl(listTail);
            if (listTail.head instanceof TapList) {
                result = result + ((TapList) listTail.head).dagSize(dejaVu);
            } else {
                result = result + 1;
            }
            listTail = listTail.tail;
        }
        if (listTail != null) {
            ++result;
        }
        return result;
    }

    /**
     * Sets all Boolean leaves to the given value.
     * Assumes this is a TapList tree of Boolean
     * Skips pointer derefs.
     */
    public void setAll(boolean value) {
        TapList objTree = this;
        while (objTree != null) {
            if (objTree.head instanceof TapList) {
                ((TapList) objTree.head).setAll(value);
                objTree = objTree.tail;
            } else {
                objTree.head = value;
                objTree = null;
            }
        }
    }

// ********** End of special part for TapList TREES *********

    /**
     * Suppport for the Iterable interface.
     */
    private static class TapListIterator<T> implements java.util.Iterator<T> {
        private TapList<T> current;

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public T next() {
            if (current == null) {
                throw new NoSuchElementException("TapListIterator");
            }
            T object = current.head;
            current = current.tail;
            return object;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
