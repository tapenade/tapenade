/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

/**
 * Tapenade internal representation.
 * Translates the incoming parsed abstract syntax trees
 * into the internal representation.
 *
 * @author Inria - Ecuador team, tapenade@inria.fr
 */
package fr.inria.tapenade.representation;
