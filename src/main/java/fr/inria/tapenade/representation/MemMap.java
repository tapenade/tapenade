/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.* ;

/** A map for a set of variables that share a common location in memory, e.g.
 * through a Fortran COMMON or EQUIVALENCE declaration, a BIND(C), ... */
public final class MemMap {

    /** A name to identify the shared location, e.g. the COMMON name */
    public String name = null ;

    /** The (hatted) sequence of boundaries that partition this shared location */
    public TapList<AlignmentBoundary> boundaries = new TapList<>(null, null) ;

    public MemMap(String commonName) {
        name = commonName ;
    }

    /** Finds the Boundary for "offset" in the tail of "inBoundaries".
     * Advances in "inBoundaries" until the place where "offset" should be.
     * If this AlignmentBoundary is not present, creates and inserts it.
     * @return the sub-list of  "inBoundaries" whose tail.head is the AlignmentBoundary found/created. */
    private TapList<AlignmentBoundary> gotoGetSetBoundary(TapList<AlignmentBoundary> inBoundaries,
                                                          int offset, boolean infiniteEndOffset, WrapperTypeSpec type) {
        while (inBoundaries.tail!=null && inBoundaries.tail.head.isBefore(offset, infiniteEndOffset)) {
            inBoundaries = inBoundaries.tail ;
        }
        if (inBoundaries.tail==null || !inBoundaries.tail.head.isEqual(offset, infiniteEndOffset)) {
            inBoundaries.placdl(new AlignmentBoundary(offset, infiniteEndOffset)) ;
        }
        if (type!=null) inBoundaries.tail.head.type = type ;
        return inBoundaries ;
    }

    /** Insert "variableDecl", of size "varSize", at the given starting "offset" into this MemMap.
     * Update the mapAccesses accordingly. */
    public void insertVariableAt(int offset, int varSize, VariableDecl variableDecl,
                                 SymbolTable declSymbolTable, MemoryMaps thisMaps) {
        TapList<AlignmentBoundary> inBoundaries = gotoGetSetBoundary(boundaries, offset, false, variableDecl.type()) ;
        inBoundaries.tail.head.addStartVar(variableDecl, declSymbolTable, null) ;
        // Update the mapAccesses info, since the current VariableDecl now belongs to this MemMap:
        thisMaps.addRegistration(new VarStartBoundary(variableDecl, this, inBoundaries.tail.head)) ;
        inBoundaries = gotoGetSetBoundary(inBoundaries, offset+varSize, varSize==0, null) ;
        inBoundaries.tail.head.addEndVar(variableDecl, declSymbolTable) ;
    }

    /** Return the offset of the end of this MemMap.
     * Sets true in "toInfiniteLastOffset" if the end is at some indefinite offset. */
    public int getLastOffset(ToBool toInfiniteLastOffset) {
        AlignmentBoundary lastBoundary = TapList.last(boundaries) ;
        if (lastBoundary==null) { // if this map is still empty
            toInfiniteLastOffset.set(false) ;
            return 0 ;
        } else {
            toInfiniteLastOffset.set(lastBoundary.infiniteOffset) ;
            return lastBoundary.offset ;
        }
    }

    /** Set boundaries in this MemMap to represent region from startOffset to endOffset/infiniteEndOffset,
     * if these boundaries are not already present. Sets to true the "active" field of all boundaries
     * from startOffset to endOffset(excluded). */
    public void setActiveRegion(int startOffset, int endOffset, boolean infiniteEndOffset) {
        TapList<AlignmentBoundary> inBoundaries = gotoGetSetBoundary(boundaries, startOffset, false, null) ;
        inBoundaries.tail.head.active = true ;
        inBoundaries = inBoundaries.tail ;
        while (inBoundaries.tail!=null && inBoundaries.tail.head.isBefore(endOffset, infiniteEndOffset)) {
            inBoundaries.tail.head.active = true ;
            inBoundaries = inBoundaries.tail ;
        }
        if (inBoundaries.tail==null || !inBoundaries.tail.head.isEqual(endOffset, infiniteEndOffset)) {
            inBoundaries.placdl(new AlignmentBoundary(endOffset, infiniteEndOffset)) ;
        }
    }

    /** @return true if at least a part of the region from startOffset to
     * endOffset/infiniteEndOffset(excluded) is marked as active */
    public boolean isActiveRegion(int startOffset, int endOffset, boolean infiniteEndOffset) {
        TapList<AlignmentBoundary> inBoundaries = gotoGetSetBoundary(boundaries, startOffset, false, null) ;
        boolean isActive = inBoundaries.tail.head.active ;
        inBoundaries = inBoundaries.tail ;
        while (!isActive && inBoundaries.tail!=null && inBoundaries.tail.head.isBefore(endOffset, infiniteEndOffset)) {
            if (inBoundaries.tail.head.active) isActive = true ;
            inBoundaries = inBoundaries.tail ;
        }
        return isActive ;
    }

    /** Merge "otherMap" into this MemMap.
     * Update the mapAccesses accordingly. */
    public void absorb(MemMap otherMap, int shift, MemoryMaps otherMaps, MemoryMaps thisMaps) {
        AlignmentBoundary otherBoundary ;
        TapList<AlignmentBoundary> toBoundaries = boundaries ;
        TapList<AlignmentBoundary> otherBoundaries = otherMap.boundaries.tail ;
        // Transfer the given "otherMap" into this MemMap.
        while (otherBoundaries!=null) {
            otherBoundary = otherBoundaries.head ;
            // Shift otherBoundary to align it with this.boundaries:
            otherBoundary.offset -= shift ;
            while (toBoundaries.tail!=null && toBoundaries.tail.head.isBefore(otherBoundary)) {
                toBoundaries = toBoundaries.tail ;
            }
            if (toBoundaries.tail==null || !toBoundaries.tail.head.isEqual(otherBoundary)) {
                toBoundaries = toBoundaries.placdl(otherBoundary) ;
            } else {  //toBoundaries.tail.head and otherBoundary define the same location:
                AlignmentBoundary thisBoundary = toBoundaries.tail.head ;
                TapList<VariableDecl> otherEndings = otherBoundary.endVars ;
                while (otherEndings!=null) {
                    thisBoundary.endVars =
                        new TapList<>(otherEndings.head, thisBoundary.endVars) ;
                    otherEndings = otherEndings.tail ;
                }
                TapList<ZoneInfoAccessElements> otherStartings = otherBoundary.startVars ;
                while (otherStartings!=null) {
                    thisBoundary.startVars =
                        new TapList<>(otherStartings.head, thisBoundary.startVars) ;
                    // Update the mapAccesses info, since the current VariableDecl now belongs to this MemMap:
                    VarStartBoundary thisDeclAccess = 
                        otherMaps.delRegistration(otherStartings.head.symbolDecl) ;
                    thisDeclAccess.map = this ;
                    thisDeclAccess.boundary = thisBoundary ;
                    thisMaps.addRegistration(thisDeclAccess) ;
                    otherStartings = otherStartings.tail ;
                }
            }
            otherBoundaries = otherBoundaries.tail ;
        }
        // Remove the given "otherMap" from where it was:
        TapList<MemMap> toOther = otherMaps.maps ;
        while (toOther!=null && toOther.tail!=null) {
            if (toOther.tail.head==otherMap) {
                toOther.tail = toOther.tail.tail ;
                toOther = null ;
            } else {
                toOther = toOther.tail ;
            }
        }
    }

    /** Remove the given VariableDecl from this MemMap, possibly leaving a hole with no variable. */
    public void removeVariableFrom(VariableDecl varDecl, MemoryMaps thisMaps) {
        VarStartBoundary startPlace = thisMaps.delRegistration(varDecl) ;
        MemMap oldMap = startPlace.map ;
        AlignmentBoundary oldBoundary = startPlace.boundary ;
        oldBoundary.removeStartVar(varDecl) ;
        TapList<AlignmentBoundary> inBoundaries = oldMap.boundaries.tail ;
        while (inBoundaries!=null && inBoundaries.head!=oldBoundary) {
            inBoundaries = inBoundaries.tail ;
        }
        while (inBoundaries!=null && !TapList.contains(inBoundaries.head.endVars, varDecl)) {
            inBoundaries = inBoundaries.tail ;
        }
        inBoundaries.head.removeEndVar(varDecl) ;
     }

    /** Use the given zoneAllocator to allocate zones for all variables in this MemMap.
     * TODO: Also accumulate into callGraph, for each Unit, the waiting lists of ZoneInfo of this MemMap
     * that the Unit may see (through its ImportsSymbolTable) */
    public void allocateZones(ZoneAllocator zoneAllocator, Unit declarationUnit) {
        AlignmentBoundary boundary, nextBoundary ;
        int nextOffset ;
        boolean infiniteNextOffset ;
        int jump = 0 ;
        TapList<ZoneInfoAccessElements> curAccesses = null, inAccesses ;
        TapList<AlignmentBoundary> inBoundaries = boundaries.tail ;
        TapList<Int2ZoneInfo> toCopiedZoneInfos = new TapList<>(null, null);
        VariableDecl varDecl ;
        boolean isInt, isReal, isPtr ;
        TapList<String> cumulExtraInfo ;
        TapList regionZones;
        WrapperTypeSpec accessType ;
        zoneAllocator.setCommonName(name) ;
        boolean needAdditionalAllocateZones = (declarationUnit != null && false /*was needAdditionalZoneInfo(boundaries, declarationUnit) [for BIND(C)??]*/) ;
        ToBool currentSharedSymbolActivity = new ToBool(false);

        while (inBoundaries!=null) {
            boundary = inBoundaries.head ;
            nextBoundary = (inBoundaries.tail==null ? null : inBoundaries.tail.head) ;
            infiniteNextOffset = (nextBoundary==null || nextBoundary.infiniteOffset) ;
            nextOffset = (infiniteNextOffset ? -1 : nextBoundary.offset) ;
            // This is the offset jump for next iteration:
            jump = (infiniteNextOffset ? -1 : nextOffset-boundary.offset) ;
            accessType = boundary.buildNextType(jump) ;
            curAccesses = removeEndingVars(curAccesses, boundary.endVars, jump) ;
            if (curAccesses==null) currentSharedSymbolActivity = new ToBool(false);
            zoneAllocator.ambiguousWithoutIndices = (curAccesses!=null) ;
            curAccesses = addStartingVars(curAccesses, boundary.startVars, jump);
            zoneAllocator.ambiguousWithoutIndices =
                zoneAllocator.ambiguousWithoutIndices || !allVariablesEndAt(curAccesses, nextBoundary) ;
            if ((curAccesses != null || boundary.type != null) && !needAdditionalAllocateZones) {
                zoneAllocator.setOffsets(boundary.offset, nextOffset, infiniteNextOffset);
                isInt = false;
                isReal = false;
                isPtr = false;
                cumulExtraInfo = null;
                for (inAccesses=curAccesses ; inAccesses!=null ; inAccesses=inAccesses.tail) {
                    varDecl = (VariableDecl)inAccesses.head.symbolDecl ;
//                     if (name==null) { // Only if this is not about a COMMON:
//                         // NOTE: we might choose here *not* to share activity for a variable in COMMON,
//                         // to preserve the possibility of differentiating the variable locally
//                         // and not inside the COMMON. cf set11/lh019 or set10/lh202.
// // but this causes bugs e.g. set06/v308 set11/lh043 set11/v02 -> TODO?
                        varDecl.shareActivity(currentSharedSymbolActivity) ;
//                     }
                    if (varDecl instanceof SubVariableDecl) {
                        ((SubVariableDecl)varDecl).trueVariableDecl.shareActivity(currentSharedSymbolActivity) ;
                    }
                    WrapperTypeSpec type = varDecl.type();
                    if (accessType==null) accessType = type ;
                    if (type.isRealOrComplexBase()) isReal = true;
                    if (type.isIntegerBase()) isInt = true;
                    if (type.isPointer()) isPtr = true;
                    cumulExtraInfo = TapList.unionString(cumulExtraInfo, varDecl.extraInfo());
                }
// System.out.println("GOING TO ALLOCATE ZONES FOR VARIABLEDECLS:");
// for (inAccesses=curAccesses ; inAccesses!=null ; inAccesses=inAccesses.tail) {
//     System.out.println("   ---VD:"+inAccesses.head.symbolDecl) ;
//     System.out.println("   ---ST:"+inAccesses.head.symbolTable) ;
//     System.out.println("   ---TR:"+inAccesses.head.accessTree) ;
// }
// System.out.println("   WITH COMMON ACCESSTYPE:"+accessType) ;
                regionZones =
                    zoneAllocator.allocateZones(accessType, -1, cumulExtraInfo, curAccesses,
                                                accessType, -1, isInt, isReal, isPtr, null,
                                                (declarationUnit==null ? null : declarationUnit.privateSymbolTable()),
                                                null) ;
// System.out.println("--> RETURNED ZONES: "+regionZones) ;

//                     zoneAllocator.allocateZones(boundary.accessTree, null, boundary.accessType, boundary.accessType,
//                                                 -1, cumulExtraInfo, -1, isInt, isReal, isPtr, null, null,
//                                                 (declarationUnit==null ? null : declarationUnit.privateSymbolTable()),
//                                                 declarationUnit) ;
// System.out.println(" ==> LAST ALLOCATED ZONE A:"+zoneAllocator.lastAllocated()) ;
                boundary.zones = regionZones;
                for (inAccesses=curAccesses ;
                     inAccesses!=null ;
                     inAccesses=inAccesses.tail) {
                  varDecl = (VariableDecl)inAccesses.head.symbolDecl ;
                  if (varDecl instanceof SubVariableDecl) {
                    ((SubVariableDecl)varDecl).accumulateSubZones(regionZones) ;
                  } else {
                    varDecl.accumulateZones(regionZones);
                    if (varDecl.nameTree!=null) {
                        ZoneInfo libReaderZoneInfo = (ZoneInfo)varDecl.nameTree.getAnnotation("zoneInfo") ;
                        // Ad-hoc mechanism to retrieve the zoneInfo of a lib shape element
                        // so that this zoneInfo receives its final zone numbers computed by zoneAllocator.
                        // NOTE: this mechanism is very limited anyway (only for COMMON shape elements in "lib" files)
                        // and should be rewritten more in accordance with the general method.
                        if (libReaderZoneInfo!=null) {
                            ZoneInfo finalZoneInfo = zoneAllocator.lastAllocated() ;
// System.out.println("EXCHANGING "+libReaderZoneInfo+" AND NEW "+finalZoneInfo) ;
                            libReaderZoneInfo.index = finalZoneInfo.index ;
                            libReaderZoneInfo.intZoneNb = finalZoneInfo.intZoneNb ;
                            libReaderZoneInfo.realZoneNb = finalZoneInfo.realZoneNb ;
                            libReaderZoneInfo.ptrZoneNb = finalZoneInfo.ptrZoneNb ;
                            finalZoneInfo.accessTree = null;
                            finalZoneInfo.setVariableNames(null) ;
                            finalZoneInfo.description = libReaderZoneInfo.description ;
                            libReaderZoneInfo.accessTree = null ;
// System.out.println("EXCHANGED  "+libReaderZoneInfo+" AND NEW "+finalZoneInfo) ;
                        }
                    }
                  }
// Now done in allocateOneZone():
//                     zoneAllocator.addVarName(regionZones, varDecl.symbol) ;
                }
// Now done in allocateOneZone():
//                 if (curAccesses != null) {
//                     zoneAllocator.addVarDecl(regionZones, curAccesses.head.symbolDecl);
//                 }
// System.out.println(" ==> LAST ALLOCATED ZONE B:"+zoneAllocator.lastAllocated()) ;

//             } else if (declarationUnit != null) {
//                 copiedZoneInfos = copyAdditionalDeclaredZoneInfos(boundary.startVars, toCopiedZoneInfos,
//                                                     commonName, boundary.offset, nextOffset, infiniteNextOffset);
//                 for (int kind = 0; kind < 4; kind++) {
//                     allCopiedZoneInfos[kind] = TapList.append(allCopiedZoneInfos[kind], copiedZoneInfos[kind]);
//                 }
            }
            inBoundaries = inBoundaries.tail ;
        }
        zoneAllocator.ambiguousWithoutIndices = false ;
        zoneAllocator.setOffsets(-1, -1, false) ;
        zoneAllocator.setCommonName(null) ;
    }

    /** Returns true if all current variables "curAccesses" will end at the next "boundary", i.e.
     * if the next call to removeEndingVars() will return an empty list */
    private static boolean allVariablesEndAt(TapList<ZoneInfoAccessElements> curAccesses, AlignmentBoundary boundary) {
        if (boundary==null) return true ;
        boolean allEnd = true ;
        while (allEnd && curAccesses!=null) {
            if (!TapList.contains(boundary.endVars, (VariableDecl)curAccesses.head.symbolDecl)) {
                allEnd = false ;
            }
            curAccesses = curAccesses.tail ;
        }
        return allEnd ;
    }

    /** Remove from the list of current variables those which end at this offset.
     * @return the given "list", modified removing any triplet whose first field
     * (a VariableDecl) is in the given "removedVars".
     */
    private static TapList<ZoneInfoAccessElements> removeEndingVars(
            TapList<ZoneInfoAccessElements> curAccesses,
            TapList<VariableDecl> removedVars,
            int jump) {
        TapList<ZoneInfoAccessElements> toResult = new TapList<>(null, curAccesses);
        TapList<ZoneInfoAccessElements> inResult = toResult;
        ZoneInfoAccessElements access ;
        while (inResult.tail != null) {
            if (TapList.contains(removedVars, (VariableDecl)inResult.tail.head.symbolDecl)) {
                inResult.tail = inResult.tail.tail;
            } else {
                access = inResult.tail.head ;
                access.startOffsetInArray += jump ;
                if (access.accessTree.opCode() == ILLang.op_arrayAccess) {
                    access.accessTree = ILUtils.build(ILLang.op_arrayAccess,
                                          ILUtils.copy(access.accessTree.down(1)),
                                            ILUtils.build(ILLang.op_expressions,
                                              ILUtils.build(ILLang.op_arrayTriplet,
                                                ILUtils.build(ILLang.op_intCst, access.accessTree.down(2).down(1).down(2).intValue()+1),
                                                ILUtils.build(ILLang.op_intCst, access.startOffsetInArray/access.baseSize)))) ;
                }
                inResult = inResult.tail;
            }
        }
        return toResult.tail;
    }

    /** Add into the list of current variables those which start at this offset.
     */
    private static TapList<ZoneInfoAccessElements> addStartingVars(
            TapList<ZoneInfoAccessElements> curAccesses,
            TapList<ZoneInfoAccessElements> addedVars,
            int jump) {
        ZoneInfoAccessElements origAccess, newAccess ;
        VariableDecl varDecl ;
        WrapperTypeSpec varType ;
        Tree accessTree ;
        while (addedVars!=null) {
            origAccess = addedVars.head ;
            varDecl = (VariableDecl)origAccess.symbolDecl ;
            varType = varDecl.type();
            if (varDecl instanceof SubVariableDecl) {
                //Special case of a temporary dummy VariableDecl built to represent a part of a struct used by BIND(C):
                accessTree = ILUtils.copy(((SubVariableDecl)varDecl).accessTree) ;
            } else {
                accessTree = ILUtils.build(ILLang.op_ident, varDecl.symbol) ;
            }
            int baseSize = -1 ;
            if (varType.isArray()) {
                int arraySize = varType.size() ;
                TapPair<WrapperTypeSpec, ArrayDim[]> baseTypeAndDims = TypeSpec.peelArrayDimsAroundComposite(varType) ;
                baseSize = baseTypeAndDims.first.size() ;
                if (arraySize!=jump && jump!=-1 && baseSize!=-1) {
                    accessTree = ILUtils.build(ILLang.op_arrayAccess,
                                   accessTree,
                                   ILUtils.build(ILLang.op_expressions,
                                     ILUtils.build(ILLang.op_arrayTriplet,
                                       ILUtils.build(ILLang.op_intCst, 0),
                                       ILUtils.build(ILLang.op_intCst, jump/baseSize - 1)))) ;
                }
            }
            newAccess = new ZoneInfoAccessElements(origAccess.symbolDecl, accessTree, origAccess.symbolTable) ;
            newAccess.startOffsetInArray = 0 ;
            newAccess.baseSize = baseSize ;
            curAccesses = new TapList<>(newAccess, curAccesses) ;
            addedVars = addedVars.tail ;
        }
        return curAccesses ;
    }

    /** @return true iff this MemMap concerns at least one variable which is declared by the scope "targetSymbolTable" */
    protected boolean touchesSymbolTable(SymbolTable targetSymbolTable) {
// System.out.println("touchesSymbolTable "+targetSymbolTable) ;
        TapList<AlignmentBoundary> inBoundaries = boundaries.tail ;
        TapList<ZoneInfoAccessElements> inStartVars ;
        boolean touches = false ;
        while (!touches && inBoundaries!=null) {
            inStartVars = inBoundaries.head.startVars ;
            while (!touches && inStartVars!=null) {
                VariableDecl varDecl = (VariableDecl)inStartVars.head.symbolDecl ;
// System.out.println("   stv:"+varDecl) ;
                if (targetSymbolTable.getTopVariableDecl(varDecl.symbol)==varDecl) touches = true ;
                inStartVars = inStartVars.tail ;
            }
            inBoundaries = inBoundaries.tail ;
        }
// System.out.println("  replies:"+touches) ;
        return touches ;
    }

    @Override
    public String toString() {
        String contents = "" ;
        TapList<AlignmentBoundary> inBoundaries = boundaries ;
        while (inBoundaries!=null) {
            contents = contents+inBoundaries.head ;
            inBoundaries = inBoundaries.tail ;
        }
        return "MemMap("+name+"):"+contents ;
    }
    
}
