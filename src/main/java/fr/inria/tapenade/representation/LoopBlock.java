/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.Tree;

/**
 * Special control-flow block for loops. One LoopBlock represents
 * the whole loop, and not only its header. LoopBlock's are
 * found in the "topBlocks" field of the FlowGraph, and in the
 * "inside" field of LoopBlock's. These LoopBlocks
 * are linked by the FGArrows between their "inside" Block's, and
 * using the originInLevel and destinationInLevel of these FGArrows,
 * one stays inside the same loop level.
 */
public final class LoopBlock extends Block {

    /**
     * The list of BasicBlock's, LoopBlock's and HeaderBlock's
     * immediately enclosed in this loop level.
     */
    public TapList<Block> inside;

    private TapList<LoopBlock> enclosingLoopBlocks;

    /**
     * The list of the LoopBlock's of all enclosing
     * loops, from the innermost loops to the outermost.
     */
    protected TapList<LoopBlock> enclosingLoopBlocks() {
        return enclosingLoopBlocks;
    }

    protected void setEnclosingLoopBlocks(TapList<LoopBlock> enclosingLoopBlocks) {
        this.enclosingLoopBlocks = enclosingLoopBlocks;
    }

    /**
     * All loop-entry blocks of this loop.
     */
    public TapList<Block> entryBlocks;
    /**
     * All loop-exit blocks of this loop.
     */
    public TapList<Block> exitBlocks;
    /**
     * The list of all FGArrows that leave some Block
     * inside the LoopBlock to some Block outside the LoopBlock.
     */
    public TapList<FGArrow> exitArrows;
    /**
     * All FGArrow's that cycle in this loop, to its HeaderBlock.
     */
    public TapList<FGArrow> cycleArrows;
    /**
     * The list of all FGArrows that leave some Block
     * outside the LoopBlock to some Block inside the LoopBlock.
     */
    protected TapList<FGArrow> entryArrows;

    /**
     * Creation, with the list of the immediate inside Blocks.
     */
    public LoopBlock(TapList<Block> inside) {
        super(null, null, null);
        this.inside = inside;
    }

    /**
     * @return the list of all FGArrows that leave some Block
     * outside the LoopBlock to some Block inside the LoopBlock.
     */
    @Override
    public TapList<FGArrow> backFlow() {
        return entryArrows;
    }

    /**
     * @return the list of all FGArrows that leave some Block
     * inside the LoopBlock to some Block outside the LoopBlock.
     */
    @Override
    public TapList<FGArrow> flow() {
        return exitArrows;
    }

    /**
     * @return the header of this loop.
     */
    public HeaderBlock header() {
        return (HeaderBlock) inside.head;
    }

    /** Marker set to true during differentiation, to indicate this is a fixed-point loop. */
    public boolean isFixedPoint = false ;

    /**
     * if this loop has a number of iterations
     * that is a static value or bounded by a static value,
     * and therefore can easily use static tape instead of stack tape,
     *
     * @return an Integer with this static value,
     * otherwise returns null.
     */
    public Integer staticIterationLength() {
        TapList<Instruction> instructions = header().instructions;
        if (instructions == null || instructions.tail != null) {
            return null;
        } else {
            Instruction instruction = instructions.head;
            Tree tree = instruction.tree;
            if (tree == null || tree.opCode() != ILLang.op_loop) {
                return null;
            }
            tree = tree.down(3);
            if (tree == null || tree.opCode() != ILLang.op_do) {
                return null;
            }
            SymbolTable symbolTable = header().symbolTable;
            Integer fromInt;
            Integer toInt;
            Integer strideInt;
            fromInt =
                    symbolTable.computeIntConstant(tree.down(2));
            toInt =
                    symbolTable.computeIntConstant(tree.down(3));
            if (ILUtils.isNullOrNone(tree.down(4))) {
                strideInt = 1;
            } else {
                strideInt =
                        symbolTable.computeIntConstant(tree.down(4));
            }
            if (fromInt != null && toInt != null && strideInt != null) {
                int from = fromInt;
                int to = toInt;
                int stride = strideInt;
                if (stride == 1) {
                    return to - from + 1;
                } else if (stride == -1) {
                    return from - to + 1;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    public Tree staticNormalizedIndex() {
        TapList<Instruction> instructions = header().instructions;
        if (instructions == null || instructions.tail != null) {
            return null;
        } else {
            Instruction instruction = instructions.head;
            Tree tree = instruction.tree;
            if (tree == null || tree.opCode() != ILLang.op_loop) {
                return null;
            }
            tree = tree.down(3);
            if (tree == null || tree.opCode() != ILLang.op_do) {
                return null;
            }
            SymbolTable symbolTable = header().symbolTable;
            Integer fromInt;
            Integer toInt;
            Integer strideInt;
            fromInt =
                    symbolTable.computeIntConstant(tree.down(2));
            toInt =
                    symbolTable.computeIntConstant(tree.down(3));
            if (ILUtils.isNullOrNone(tree.down(4))) {
                strideInt = 1;
            } else {
                strideInt =
                        symbolTable.computeIntConstant(tree.down(4));
            }
            if (fromInt != null && toInt != null && strideInt != null) {
                int from = fromInt;
                int to = toInt;
                int stride = strideInt;
                Tree index = ILUtils.copy(tree.down(1));
                if (stride == 1) {
                    if (from != 1) {
                        return ILUtils.build(ILLang.op_sub,
                                index,
                                ILUtils.build(ILLang.op_intCst, from - 1));
                    } else {
                        return index;
                    }
                } else if (stride == -1) {
                    if (to != 1) {
                        return ILUtils.build(ILLang.op_sub,
                                index,
                                ILUtils.build(ILLang.op_intCst, to - 1));
                    } else {
                        return index;
                    }
                } else {
                    return null;
                }
            } else {
                return null;
            }
        }
    }

    /**
     * Prints a short reference to this LoopBlock onto TapEnv.curOutputStream().
     */
    @Override
    public void cite() throws java.io.IOException {
        TapEnv.print("LB(" + rank + ')');
    }

    @Override
    public String toString() {
        return "L" + super.toString();
    }
}
