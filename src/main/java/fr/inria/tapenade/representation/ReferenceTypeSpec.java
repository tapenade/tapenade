/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * A "reference" type to some other type.
 */
public final class ReferenceTypeSpec extends TypeSpec {

    /**
     * The reference destination type.
     */
    public TypeSpec destinationType;

    /**
     * Access info on this destination e.g. "const" etc.
     */
    public TapList<String> subAccess;

    /**
     * Create a new ref type to destination type, possibly with a C++ array dimension.
     */
    public ReferenceTypeSpec(TypeSpec destinationType) {
        super(SymbolTableConstants.REFERENCETYPE);
        this.destinationType = destinationType;
    }

    @Override
    public TypeSpec wrappedType() {
        return destinationType;
    }

    @Override
    public void setWrappedType(TypeSpec type) {
        destinationType = type instanceof WrapperTypeSpec ? (WrapperTypeSpec) type : new WrapperTypeSpec(type);
    }

    @Override
    public TypeSpec nestedLevelType() {
        return destinationType;
    }

    @Override
    protected String baseTypeName() {
        return "Reference";
    }

    @Override
    public WrapperTypeSpec baseTypeSpec(boolean stopOnPointer) {
        return destinationType == null ? null : destinationType.baseTypeSpec(stopOnPointer);
    }

    @Override
    protected boolean testComparesWith(TypeSpec other, int comparison, TypeSpec toThis, TypeSpec toOther,
                                       TapList<TapPair<TypeSpec, TypeSpec>> dejaVu) {
        // peel off layers of WrapperTypeSpec's and ModifiedTypeSpec's:
        // (layers of ModifiedTypeSpec's *around a Reference* make little sense, so we will peel them)
        toOther = peelWrapperAndModifiedTo(other, toOther);
        if (toOther != null) {
            other = toOther.wrappedType();
        }
        // easy strict == case:
        if (this == other) {
            return true;
        }
        // if other is unspecified, it all depends on type inference:
        if (other == null) {
            if (testHasInference(comparison)) {
                if (toOther != null) {
                    toOther.setWrappedType(this);
                }
                return true;
            } else {
                return false;
            }
        }
        // else other must be of matching kind:
        if (!(other instanceof ReferenceTypeSpec)) {
            return false;
        }
        ReferenceTypeSpec otherReference = (ReferenceTypeSpec) other;
        // dejaVu case: stop recursion and return true:
        if (TapList.containsObjectPair(dejaVu, destinationType, otherReference.destinationType)) {
            return true;
        }
        boolean comparesWell = true;
        // compare destinations:
        if (destinationType == null) {
            // if this's destinationType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                setWrappedType(otherReference.destinationType == null ? null : otherReference.destinationType.weakenForInference(comparison));
            } else {
                comparesWell = testAcceptsUnspecified(comparison);
            }
        } else if (otherReference.destinationType == null) {
            // if other's elementType is unspecified, it all depends on type inference:
            if (testHasInference(comparison)) {
                otherReference.setWrappedType(destinationType);
            } else {
                comparesWell = false;
            }
        } else {
            comparesWell =
                    destinationType.comparesWith(otherReference.destinationType, comparison,
                            this, otherReference,
                            new TapList<>(new TapPair<>(destinationType, otherReference.destinationType), dejaVu));
        }
        return comparesWell;
    }

    @Override
    public void updateAfterImports(SymbolTable symbolTable, TapList<TypeSpec> dejaVu) {
        if (destinationType != null) {
            destinationType.updateAfterImports(symbolTable, dejaVu);
        }
    }

    @Override
    public TypeSpec copyStopOnComposite(Unit publishedUnit) {
        TypeSpec copyDestination = destinationType == null ? null : destinationType.copyStopOnComposite(publishedUnit);
        return copyDestination == destinationType ? this : new ReferenceTypeSpec(copyDestination);
    }

    @Override
    public Tree generateTree(SymbolTable symbolTable, TapList<SymbolDecl> dependsOn,
                             TapList<SymbolDecl> shortNames, boolean useShortNames, TapList<TypeSpec> dejaVu) {
        if (TapList.contains(dejaVu, this)) {
            TapEnv.toolWarning(-1, "(TypeSpec tree regeneration) circular type");
            return ILUtils.build(ILLang.op_ident, "CircularType");
        } else {
            destinationType = ((WrapperTypeSpec) destinationType).equalsDiffTypeSpecAndTypeSpec();
            dejaVu = new TapList<>(this, dejaVu);
            Tree result = destinationType.generateTree(symbolTable, dependsOn, shortNames, true, dejaVu);
            result = addTypeModifiers(result, ILUtils.buildListIdents(subAccess));
            return ILUtils.build(ILLang.op_referenceType, result);
        }
    }

    @Override
    public String showType() {
        int language = TapEnv.relatedUnit() != null ? TapEnv.relatedUnit().language() : -1;
        return (destinationType == null ? "?" : destinationType.showType()) +
                (TapEnv.isFortran(language) ? " REFERENCE" : "&");
    }

    @Override
    public void dump() throws java.io.IOException {
        TapEnv.print(toString());
    }

    @Override
    public String toString() {
        return (typeDeclName() == null ? "" : "\"" + typeDeclName() + "\":") + "ref" //+"@"+Integer.toHexString(hashCode())
                + " to " + (subAccess == null ? "" : " " + subAccess) + (destinationType == null ? "?" : destinationType.toString());
    }
}
