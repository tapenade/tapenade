/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.utils.ILLang;
import fr.inria.tapenade.utils.ILLangOps;
import fr.inria.tapenade.utils.Operator;
import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.ToBool;
import fr.inria.tapenade.utils.ToInt;
import fr.inria.tapenade.utils.TopDownTreeWalk;
import fr.inria.tapenade.utils.Tree;

import fr.inria.tapenade.differentiation.ExpressionDifferentiator;

import java.util.Enumeration;

/**
 * IL Trees basic utility methods.
 */
public final class ILUtils {

    private static final int DIFFERENT = -1;
    private static final int DOUBT = 0;
    private static final int SAME = 1;
    private static final int MAX_LIST_LENGTH = 78;

    /**
     * Gets an IL operator from its name.
     *
     * @param name The name of the operator.
     * @return The operator if it exists otherwise null.
     */
    public static Operator operator(String name) {
        int m;
        int g = 1;
        int d = ILLangOps.opsLength() - 1;
        String opName;
        while (g <= d) {
            m = (g + d) / 2;
            opName = ILLangOps.ops(m).name;
            if (name.equals(opName)) {
                return ILLangOps.ops(m);
            } else if (name.compareTo(opName) < 0) {
                d = m - 1;
            } else {
                g = m + 1;
            }
        }
        return null;
    }

    /**
     * if "tree" is an op_ident, returns its string.
     * @return the String, otherwise returns null.
     */
    public static String getIdentString(Tree tree) {
        if (tree != null && (tree.opCode() == ILLang.op_ident || tree.opCode() == ILLang.op_metavar)) {
            return tree.stringValue();
        } else {
            return null;
        }
    }

    /**
     * @param caseSensitive when true, the string equality is case sensitive.
     * @return True iff "tree" is op_ident:"name".
     */
    public static boolean isIdent(Tree tree, String name, boolean caseSensitive) {
        return tree != null && name != null
                && tree.opCode() == ILLang.op_ident
                && (caseSensitive ?
                name.equals(tree.stringValue()) :
                name.equalsIgnoreCase(tree.stringValue()));
    }

    public static boolean isACIOSymbol(Tree tree) {
        String varName = baseName(tree);
        return "stdin".equals(varName) || "stdout".equals(varName) || "stderr".equals(varName);
    }

    /**
     * @return true if "tree" is the controller of an OpenMP parallel region or loop.
     */
    public static boolean isParallelController(Tree tree) {
        return tree != null &&
                (tree.opCode() == ILLang.op_parallelRegion || tree.opCode() == ILLang.op_parallelLoop);
    }
    
    /**
     * @return true if "tree" is the controller of a CUDA call.
     */
    public static boolean isCudaController(Tree tree) {
        return (tree != null
                && tree.opCode() == ILLang.op_parallelRegion
                && ILUtils.isIdent(tree.down(1), "CUDA", true)) ;
    }

    /**
     * @param caseSensitive when true, the string equality is case sensitive.
     * @return True iff "tree" is op_ident:"name" with "name" belonging to the given array "names".
     */
    public static boolean isIdentOf(Tree tree, String[] names, boolean caseSensitive) {
        if (tree == null || tree.opCode() != ILLang.op_ident) {
            return false;
        } else {
            String name = tree.stringValue();
            boolean found = false;
            for (int i = names.length - 1; i >= 0 && !found; --i) {
                found = caseSensitive ? names[i].equals(name) : names[i].equalsIgnoreCase(name);
            }
            return found;
        }
    }

    /**
     * @return True iff String "name" belongs to the given array "names".
     */
    public static boolean isStringOf(String name, String[] names) {
        boolean found = false;
        for (int i = names.length - 1; i >= 0 && !found; --i) {
            found = names[i].equals(name);
        }
        return found;
    }

    /**
     * @return True iff "tree" is op_intCst:"value".
     */
    public static boolean isIntCst(Tree tree, int value) {
        return tree != null
                && tree.opCode() == ILLang.op_intCst
                && tree.intValue() == value;
    }

    /**
     * @return True iff this tree is op_ident nested in 0 or more op_pointerDeclarator.
     */
    public static boolean isOptPointersIdent(Tree tree) {
        return tree != null &&
                (tree.opCode() == ILLang.op_ident
                        ||
                        tree.opCode() == ILLang.op_pointerDeclarator
                                && isOptPointersIdent(tree.down(1)));
    }

    public static boolean isAllocate(Tree tree) {
        if (tree==null) return false ;
        while (tree.opCode() == ILLang.op_cast) {
            tree = tree.down(2);
        }
        return tree.opCode() == ILLang.op_allocate ;
    }

    /** Copy "tree" skipping its op_allocate level */
    public static Tree skipAllocate(Tree tree) {
        if (tree==null) {
            return null ;
        } else {
            switch (tree.opCode()) {
                case ILLang.op_allocate:
                    return ILUtils.copy(tree.down(1)) ;
                case ILLang.op_fieldAccess:
                case ILLang.op_arrayAccess:
                case ILLang.op_pointerAccess:
                    return ILUtils.build(tree.opCode(),
                             skipAllocate(tree.down(1)),
                             ILUtils.copy(tree.down(2))) ;
                default:
                    return ILUtils.copy(tree) ;
            }
        }
    }

    /**
     * @return True if this tree is a direct access to a visible variable name,
     * followed by array or field references, without going through a pointer.
     */
    public static boolean isAccessibleDirectlyFromVariable(Tree tree) {
        if (tree == null) {
            return true;
        }
        switch (tree.opCode()) {
            case ILLang.op_allocate:
            case ILLang.op_pointerAccess:
                return false;
            case ILLang.op_address:
                //It's ok to go through an op_address, for the case of an allocatable
                // field or variable "v", which is considered as "&v".
            default:
                if (!tree.isAtom() && tree.down(1) != null) {
                    return isAccessibleDirectlyFromVariable(tree.down(1));
                } else {
                    return true;
                }
        }
    }

    /**
     * @return the name of the called function
     * when "callTree" is a procedure, function, method  or
     * constructor call.
     */
    public static String getCallFunctionNameString(Tree callTree) {
        if (callTree.opCode() == ILLang.op_ioCall) {
            callTree = callTree.down(1);
        } else if (callTree.opCode() == ILLang.op_call) {
            callTree = getCalledName(callTree);
        }
        while (callTree != null && callTree.opCode() != ILLang.op_ident) {
            /*if (callTree.opCode() == ILLang.op_pointerAccess || callTree.opCode() == ILLang.op_arrayAccess) {
                callTree = callTree.down(1);
            } else  */
            if (callTree.opCode() == ILLang.op_scopeAccess /*|| callTree.opCode() == ILLang.op_fieldAccess*/) {
                callTree = callTree.down(2);
            } else {
                callTree = null;
            }
        }
        return callTree != null ? getIdentString(callTree) : null;
    }

    public static Tree buildCall(Tree object, Tree funcName, Tree arguments) {
        return build(ILLang.op_call, object, funcName, arguments);
    }

    public static Tree buildCall(Tree funcName, Tree arguments) {
        return build(ILLang.op_call,
                build(ILLang.op_none),
                funcName,
                arguments);
    }

    /**
     * @return the object on which this method is called.
     */
    public static Tree getObject(Tree callTree) {
        return callTree.down(1);
    }

    /**
     * @return the name Tree of the called method or function.
     */
    public static Tree getCalledName(Tree callTree) {
        return callTree.down(2);
    }

    /**
     * @return the name String of the called method or function.
     */
    public static String getCalledNameString(Tree callTree) {
        Tree calledName = getCalledName(callTree);
        while (calledName != null && calledName.opCode() != ILLang.op_ident) {
            if (calledName.opCode() == ILLang.op_pointerAccess || calledName.opCode() == ILLang.op_arrayAccess) {
                calledName = calledName.down(1);
            } else if (calledName.opCode() == ILLang.op_scopeAccess || calledName.opCode() == ILLang.op_fieldAccess) {
                calledName = calledName.down(2);
            } else {
                calledName = null;
            }
        }
        return getIdentString(calledName);
    }

    /**
     * @return the actual arguments of this method or function call.
     */
    public static Tree getArguments(Tree callTree) {
        return callTree.down(3);
    }

    public static void setArguments(Tree callTree, Tree arguments) {
        callTree.setChild(arguments, 3);
    }

    /**
     * @param caseSensitive when true, the String equality is case sensitive.
     * @return true when "callTree" is a call of a function called "calledName".
     */
    public static boolean isCallingString(Tree callTree, String calledName, boolean caseSensitive) {
        if (callTree != null && callTree.opCode() == ILLang.op_call) {
            String calledString = getCalledNameString(callTree);
            return caseSensitive ?
                    calledName.equals(calledString) :
                    calledName.equalsIgnoreCase(calledString);
        } else {
            return false;
        }
    }

    public static String getClassNameString(Tree constructorCallTree) {
        String classNameType = null;
        if (constructorCallTree.opCode() == ILLang.op_pointerAccess
                || constructorCallTree.opCode() == ILLang.op_arrayAccess) {
            classNameType = baseName(constructorCallTree.down(1));
        } else if (constructorCallTree.opCode() == ILLang.op_ident) {
            classNameType = constructorCallTree.stringValue();
        }
        // in theory: classNameType = "class *" where * is the class name.
        if (classNameType != null && classNameType.length() >= 6 && classNameType.startsWith("class ")) {
            return classNameType.substring(6); // from index 6 to the end
        }

        return classNameType;
    }

    /**
     * @return true if expression "expr" dereferences at least one pointer.
     */
    public static boolean isAccessedThroughPointer(Tree expr) {
        switch (expr.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_iterativeVariableRef:
                return isAccessedThroughPointer(expr.down(1));
            case ILLang.op_expressions: {
                Tree[] sons = expr.children();
                boolean isatp = false;
                for (int i = sons.length - 1; !isatp && i >= 0; --i) {
                    isatp = isAccessedThroughPointer(sons[i]);
                }
                return isatp;
            }
            case ILLang.op_pointerAccess:
                return true;
            default:
                // TODO passesByValue ...
                return false;
        }
    }

    public static boolean startsWithPointerDeref(Tree expr) {
        switch (expr.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_iterativeVariableRef:
                return startsWithPointerDeref(expr.down(1));
            case ILLang.op_pointerAccess:
                if (ILUtils.isNullOrNone(expr.down(1)) || expr.down(1).opCode()==ILLang.op_ident)
                    return true;
                else
                    return startsWithPointerDeref(expr.down(1));
            default:
                return false;
        }
    }

    /** Detects that some index in indices is itself an array of arbitrary indices */
    public static boolean hasTwoLevelIndex(Tree indices, SymbolTable symbolTable) {
        Tree[] indexList = indices.children();
        boolean hasTwoLevels = false ;
        for (int i=indexList.length-1 ; !hasTwoLevels && i>=0 ; --i) {
            hasTwoLevels = !ILUtils.isNullOrNone(indexList[i])
                && indexList[i].opCode()!=ILLang.op_arrayTriplet
                && symbolTable.typeOf(indexList[i]).isArray() ;
        }
        return hasTwoLevels ;
    }

    /**
     * Look if a modifier named "value" appears in this declaration Tree.
     *
     * @param value The searched modifier. Belongs to {in, out, inout, constant, const, optional}
     * @return true if the modifier is found.
     */
    public static boolean containsModifier(String value, Tree tree, boolean stopOnPointer) {
        boolean result = false;
        // Dirty code: we assume there is only one declarator!
        Tree declarator = tree.down(3).down(1);
        if (stopOnPointer && declarator != null && containsPointerDeclarator(declarator)) {
            return false;
        }
        if (tree.opCode() == ILLang.op_varDeclaration
                && tree.down(2).opCode() == ILLang.op_modifiedType) {
            result = containsModifierRec(value, tree.down(2));
        }
        return result;
    }

    /**
     * Returns true if there is a pointer level on the way from this declarator tree to its leaf
     */
    private static boolean containsPointerDeclarator(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_ident:
                return false;
            case ILLang.op_pointerDeclarator:
                return true;
            case ILLang.op_assign:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_sizedDeclarator:
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
                return containsPointerDeclarator(tree.down(1));
            default:
                return false;
        }
    }

    private static boolean containsModifierRec(String value, Tree subTree) {
        boolean result;
        Tree modifiers = subTree.down(1);
        boolean found = false;
        int nb = modifiers.length();
        int i = 1;
        while (!found && i <= nb) {
            found = modifiers.down(i).opCode() == ILLang.op_ident
                    && modifiers.down(i).stringValue().equals(value);
            i = i + 1;
        }
        result = found;
        if (!result && subTree.down(2).opCode() == ILLang.op_modifiedType) {
            result = containsModifierRec(value, subTree.down(2));
        }
        return result;
    }

    /**
     * @return true if expression "expr" refers to an array without specifying its dimension.
     * This means we don't have enough information to e.g. traverse it or initialize it etc...
     */
    public static boolean containsUnknownDimension(Tree expr) {
        boolean unknown = false;
        switch (expr.opCode()) {
            case ILLang.op_fieldAccess:
            case ILLang.op_iterativeVariableRef:
                unknown = containsUnknownDimension(expr.down(1));
                break;
            case ILLang.op_expressions:
                Tree[] sons = expr.children();
                for (int i = sons.length - 1; !unknown && i >= 0; --i) {
                    unknown = containsUnknownDimension(sons[i]);
                }
                break;
            case ILLang.op_arrayAccess:
                unknown = containsUnknownDimension(expr.down(1));
                if (!unknown) {
                    Tree[] dims = expr.down(2).children();
                    for (int i = dims.length - 1; i >= 0 && !unknown; --i) {
                        if (isUnknownDim(dims[i])) {
                            unknown = true;
                        }
                    }
                }
                break;
            case ILLang.op_pointerAccess:
                unknown = containsUnknownDimension(expr.down(1));
                if (!unknown) {
                    unknown = isUnknownDim(expr.down(2));
                }
                break;
            default:
                break;
        }
        return unknown;
    }

    private static boolean isUnknownDim(Tree dim) {
        if (isNullOrNone(dim)) {
            return true;
        } else {
            Tree lastIndex = dim;
            if (dim.opCode() == ILLang.op_dimColon) {
                lastIndex = dim.down(2);
            } else if (dim.opCode() == ILLang.op_arrayTriplet) {
                lastIndex = dim.down(2);
            }
            return isNullOrNoneOrStar(lastIndex);
        }
    }

    /**
     * @return true if the "tree" is a call to an IO that reads data into
     * its arguments.
     */
    public static boolean isIORead(Tree tree) {
        if (tree.opCode() != ILLang.op_ioCall) {
            return false;
        }
        String ioAction = getIdentString(tree.down(1));
        assert ioAction != null;
        return ioAction.equals("read") || ioAction.equals("accept")
                || ioAction.equals("decode");
    }

    /**
     * @return true if the "tree" is a call to an IO that writes data from
     * its arguments.
     */
    public static boolean isIOWrite(Tree tree) {
        if (tree.opCode() != ILLang.op_ioCall) {
            return false;
        }
        String ioAction = getIdentString(tree.down(1));
        assert ioAction != null;
        return ioAction.equals("write") || ioAction.equals("rewrite")
                || ioAction.equals("print") || ioAction.equals("encode");
    }

    /**
     * Suppose that expressions exp1 and exp2 start from the same root variable.
     *
     * @return true if the access path of exp1 and the access path of exp2
     * certainly lead to different memory locations.
     */
    public static boolean disjointPath(Tree exp1, Tree exp2) {
        if (isNullOrNone(exp1)) {
            return !isNullOrNone(exp2);
        }
        if (isNullOrNone(exp2)) {
            return true;
        }
        switch (exp1.opCode()) {
            case ILLang.op_ident:
                return false;
            case ILLang.op_fieldAccess:
                if (exp2.opCode() == ILLang.op_ident) {
                    return false;
                } else {
                    return exp2.opCode() != ILLang.op_fieldAccess
                            || !getIdentString(exp1.down(2)).equals(getIdentString(exp2.down(2)))
                            || disjointPath(exp1.down(1), exp2.down(1));
                }
            case ILLang.op_arrayAccess:
                if (exp2.opCode() == ILLang.op_ident) {
                    return false;
                } else {
                    return exp2.opCode() != exp1.opCode()
                            || disjointPath(exp1.down(1), exp2.down(1));
                }
            default:
                TapEnv.toolError("(disjointPath) Unexpected operator: " + exp1.opName());
                return false;
        }
    }

    /**
     * @return the String of the label variable that is used
     * if "tree" is a "gotoLabelVar" Tree.
     */
    public static String gotoLabelVar(Tree tree) {
        return tree.down(1).stringValue();
    }

    /**
     * @return the String of the label variable
     * that is being assigned
     * if "tree" is a "assignLabelVar" Tree.
     */
    public static String assignedLabelVar(Tree tree) {
        return tree.down(2).stringValue();
    }

    /**
     * @return the label value assigned to the label var
     * if "tree" is a "assignLabelVar" Tree.
     */
    public static String assignedLabel(Tree tree) {
        return tree.down(1).stringValue();
    }

    /**
     * @return the name
     * of the variable that is being written
     * if "tree" is an ILLang.op_assign Tree.
     */
    public static String assignedName(Tree tree) {
        return baseName(tree.down(1));
    }

    /**
     * Assuming that "field" is the ident tree that
     * designates a record field,
     *
     * @return the
     * conventional rank of this field, that has been
     * stored here during type-checking. This rank
     * starts at 0 for the first field.
     * In case this rank is not defined, returns -1.
     */
    public static int getFieldRank(Tree field) {
        Object fieldRankObject = field.getAnnotation("fieldRank");
        if (fieldRankObject == null) {
            return -1;
        } else {
            return (Integer) fieldRankObject;
        }
    }

    public static void setFieldRank(Tree field, int fieldRank) {
        field.setAnnotation("fieldRank", fieldRank);
    }

    public static boolean isImplicitCompleteDimension(Tree indexTree) {
        return indexTree.opCode() == ILLang.op_dimColon
                && isNullOrNone(indexTree.down(1))
                && isNullOrNone(indexTree.down(2))
                ||
                indexTree.opCode() == ILLang.op_arrayTriplet
                        && isNullOrNone(indexTree.down(1))
                        && isNullOrNone(indexTree.down(2))
                        && isNullOrNone(indexTree.down(3));
    }

    public static TapList<Tree> collectIndexes (Tree tree, TapList<Tree> indexes) {
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess: {
                Tree[] indices = tree.down(2).children() ;
                for (int i=indices.length-1 ; i>=0 ; --i)
                    indexes = new TapList<>(indices[i], indexes) ;
                collectIndexes(tree.down(1), indexes) ;
                break ;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
                collectIndexes(tree.down(1), indexes) ;
                break;
            case ILLang.op_pointerAccess:
                indexes = new TapList<>(tree.down(2), indexes) ;
                collectIndexes(tree.down(1), indexes) ;
                break;
            case ILLang.op_ident:
                break ;
            default:
                TapEnv.toolError("(Collect indexes) Unexpected operator: " + tree.opName());
        }
        return indexes ;
    }

    /**
     * @return the op_ident Tree which is the root of expression "tree".
     */
    public static Tree baseTree(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_arrayDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_sizedDeclarator:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_deallocate:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
            case ILLang.op_assign:
            case ILLang.op_referenceDeclarator:
                return baseTree(tree.down(1));
            case ILLang.op_call:
                return baseTree(getCalledName(tree));
            case ILLang.op_modifiers:
            case ILLang.op_allocate:
            case ILLang.op_star:
            case ILLang.op_none:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_stringCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_modifiedType:
            case ILLang.op_declarators:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_minus:
            case ILLang.op_concat:
                return null;
            case ILLang.op_expressions:
            case ILLang.op_ident:
                return tree;
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_nameEq:
            case ILLang.op_cast:
            case ILLang.op_scopeAccess:
                return baseTree(tree.down(2));
            case ILLang.op_ifExpression:
            case ILLang.op_if: {
                // During Differentiation, we build conditional expressions.
                // The base tree is therefore the base of either of the two expressions.
                Tree base = baseTree(tree.down(2));
                if (base == null) {
                    base = baseTree(tree.down(3));
                }
                return base;
            }
            case ILLang.op_iterativeVariableRef:
                return baseTree(tree.down(1).down(1));
            default:
                TapEnv.toolError("(Get base Tree) Unexpected operator: " + tree.opName());
                return tree;
        }
    }

    /**
     * If "tree" is an expression ultimately referencing
     * a variable name.
     *
     * @return this name.
     */
    public static String baseName(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_metavar:
                return getIdentString(tree);
            case ILLang.op_arrayDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_sizedDeclarator:
            case ILLang.op_pointerAssign:
            case ILLang.op_assign:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_deallocate:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
            case ILLang.op_recordType:
            case ILLang.op_interfaceDecl:
            case ILLang.op_referenceDeclarator:
                return baseName(tree.down(1));
            case ILLang.op_call:
                return baseName(getCalledName(tree));
            case ILLang.op_expressions:
                if (tree.length() > 0) {
                    return baseName(tree.down(1));
                } else {
                    return null;
                }
            case ILLang.op_allocate:
            case ILLang.op_star:
            case ILLang.op_none:
            case ILLang.op_mul:
            case ILLang.op_div:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_minus:
            case ILLang.op_ifExpression:
            case ILLang.op_if:
            case ILLang.op_modifiers:
            case ILLang.op_modifiedType:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_referenceType:
            case ILLang.op_unionType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_declarators:
            case ILLang.op_stringCst:
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_bitCst:
                return null;
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_nameEq:
            case ILLang.op_cast:
            case ILLang.op_scopeAccess:
            case ILLang.op_accessDecl:
            case ILLang.op_binary:
                // overloading fortran:
                // bind(c) fortran:
                // makes little sense, but...
                return baseName(tree.down(2));
            case ILLang.op_iterativeVariableRef:
                return baseName(tree.down(1).down(1));
            default:
                if (!tree.isAtom() && tree.down(1) != null && tree.down(1).opCode() == ILLang.op_none) {
                    // overloading fortran:
                    return tree.opName();
                } else {
                    TapEnv.toolError("(Get base name) Unexpected operator: " + tree.opName());
                    return null;
                }
        }
    }

    public static Tree baseTypeTree(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
            case ILLang.op_ident:
                return tree;
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_functionType:
            case ILLang.op_recordType:
            case ILLang.op_enumType:
                return baseTypeTree(tree.down(1));
            case ILLang.op_modifiedType:
                return baseTypeTree(tree.down(2));
            default:
                return null;
        }
    }

    /** Places an op_pointerAccess around "tree", which must be a pointer expression.
     * Simplifies op_pointerAccess(op_address(tree), null). */
    public static Tree addPointerAccess(Tree tree) {
        if (tree.opCode() == ILLang.op_address) {
            return tree.cutChild(1);
        } else {
            return build(ILLang.op_pointerAccess, tree, build(ILLang.op_none));
        }
    }

    /**
     * Inserts an op_pointerAccess around the base op_ident of "tree",
     * which must be a reference expression.
     */
    public static Tree addPointerAccessAtBase(Tree tree) {
        if (tree==null) return build(ILLang.op_pointerAccess) ;
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
                tree.setChild(addPointerAccessAtBase(tree.cutChild(1)), 1);
                break;
            case ILLang.op_address:
                if (tree.down(1).opCode() == ILLang.op_ident) {
                    tree = tree.cutChild(1);
                } else {
                    tree.setChild(addPointerAccessAtBase(tree.cutChild(1)), 1);
                }
                break;
            case ILLang.op_ident:
                tree = build(ILLang.op_pointerAccess, tree, build(ILLang.op_none));
                break;
            case ILLang.op_none:
                tree = build(ILLang.op_pointerAccess) ;
                break ;
            default:
                TapEnv.toolError("(Add pointerAccess at base) Unexpected operator: " + tree.opName());
        }
        return tree;
    }

    /**
     * Places an op_address around "tree", which must be a reference expression.
     * Simplifies op_address(op_pointerAccess(tree, null)).
     */
    public static Tree addAddressOf(Tree tree) {
        if (tree.opCode() == ILLang.op_pointerAccess && isNullOrNone(tree.down(2))) {
            return tree.cutChild(1);
        } else {
            return build(ILLang.op_address, tree);
        }
    }

    /**
     * Inserts an op_address around the base op_ident of "tree",
     * which must be a reference expression.
     */
    public static Tree addAddressOfAtBase(Tree tree) {
        if (tree==null) return build(ILLang.op_address) ;
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_address:
                tree.setChild(addAddressOfAtBase(tree.cutChild(1)), 1);
                break;
            case ILLang.op_pointerAccess:
                // Question: is the following test correct? or should it be
                // tree.down(1).opCode() == ILLang.op_ident && isNullOrNone(tree.down(2))
                if (isNullOrNone(tree.down(1)) || tree.down(1).opCode() == ILLang.op_ident) {
                    tree = tree.cutChild(1);
                } else {
                    tree.setChild(addAddressOfAtBase(tree.cutChild(1)), 1);
                }
                break;
            case ILLang.op_ident:
                tree = build(ILLang.op_address, tree);
                break;
            case ILLang.op_none:
                tree = build(ILLang.op_address) ;
                break ;
            default:
                TapEnv.toolError("(Add addressOf at base) Unexpected operator: " + tree.opName());
        }
        return tree;
    }

    public static String getUnitName(Tree tree) {
        if (tree.opCode() == ILLang.op_function) {
            return getIdentString(tree.down(4));
        }
        return null;
    }

    public static Tree contains(Tree tree, int operator, String value) {
        // [llh] This method is unclear.
        Tree result = null;
        if (tree == null) {
            return null;
        }
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
                result = contains(tree.down(1), operator, value);
                break;
            case ILLang.op_fieldAccess:
                if (operator == ILLang.op_fieldAccess) {
                    result = tree;
                }
                break;
            case ILLang.op_varDeclaration:
                if (operator == ILLang.op_assign
                        || operator == ILLang.op_pointerAssign) {
                    result = contains(tree.down(3), operator, value);
                } else if (operator == ILLang.op_modifiers) {
                    result = contains(tree.down(2), ILLang.op_modifiers, value);
                }
                break;
            case ILLang.op_declarators: {
                int i = 1;
                while (result == null && i <= tree.length()) {
                    result = contains(tree.down(i), operator, value);
                    i = i + 1;
                }
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_pointerAssign: {
                result = tree;
                break;
            }
            case ILLang.op_modifiedType:
                result = contains(tree.down(1), operator, value);
                if (result == null) {
                    result = contains(tree.down(2), operator, value);
                }
                break;
            case ILLang.op_modifiers: {
                int i = 1;
                while (result == null && i <= tree.length()) {
                    if (isIdent(tree.down(i), value, true)) {
                        result = tree.down(i);
                    }
                    i = i + 1;
                }
                break;
            }
            default:
                result = null;
                break;
        }
        return result;
    }

    public static boolean isAVariableOnlyDeclaration(Tree tree) {
        if (tree.opCode() != ILLang.op_varDeclaration) {
            return false;
        }
        Tree[] declarators = tree.down(3).children();
        boolean onlyVarDeclarators = true;
        for (int i = declarators.length - 1; i >= 0 && onlyVarDeclarators; --i) {
            Tree declarator = declarators[i];
            while (declarator.opCode() == ILLang.op_modifiedDeclarator) {
                declarator = declarator.down(2);
            }
            if (declarator.opCode() == ILLang.op_functionDeclarator ||
                    declarator.opCode() == ILLang.op_constructorDeclarator ||
                    declarator.opCode() == ILLang.op_destructorDeclarator) {
                onlyVarDeclarators = false;
            }
        }
        return onlyVarDeclarators;
    }

    /**
     * @return true if given "tree" is a varDeclaration that declares variable "varName"
     */
    public static boolean declaresVariable(Tree tree, String varName) {
        if (varName == null || tree.opCode() != ILLang.op_varDeclaration) {
            return false;
        }
        Tree[] declarators = tree.down(3).children();
        boolean found = false;
        Tree declarator;
        for (int i = declarators.length - 1; i >= 0 && !found; --i) {
            declarator = declarators[i];
            while (declarator != null && declarator.opCode() != ILLang.op_ident) {
                switch (declarator.opCode()) {
                    case ILLang.op_arrayDeclarator:
                    case ILLang.op_pointerDeclarator:
                    case ILLang.op_sizedDeclarator:
                    case ILLang.op_referenceDeclarator:
                    case ILLang.op_assign:
                        declarator = declarator.down(1);
                        break;
                    case ILLang.op_modifiedDeclarator:
                        declarator = declarator.down(2);
                        break;
                    default:
                        declarator = null;
                }
            }
            if (varName.equals(getIdentString(declarator))) {
                found = true;
            }
        }
        return found;
    }

    public static boolean isADeclaration(Tree tree) {
        return (tree.opCode() == ILLang.op_varDeclaration
                || tree.opCode() == ILLang.op_varDimDeclaration
                || tree.opCode() == ILLang.op_constDeclaration
                || tree.opCode() == ILLang.op_typeDeclaration
                || tree.opCode() == ILLang.op_functionDeclarator //[llh] I think functionDeclarator should not be here!
                || tree.opCode() == ILLang.op_function
                || tree.opCode() == ILLang.op_program
                || tree.opCode() == ILLang.op_module
                || tree.opCode() == ILLang.op_class
                || tree.opCode() == ILLang.op_constructor
                || tree.opCode() == ILLang.op_destructor
                || tree.opCode() == ILLang.op_interfaceDecl
                || tree.opCode() == ILLang.op_accessDecl
                || tree.opCode() == ILLang.op_external
                || tree.opCode() == ILLang.op_intrinsic
                || tree.opCode() == ILLang.op_save
                || tree.opCode() == ILLang.op_nameList
                || tree.opCode() == ILLang.op_common
                || tree.opCode() == ILLang.op_equivalence
                || tree.opCode() == ILLang.op_useDecl
                || tree.opCode() == ILLang.op_include);
    }

    public static boolean isAUnitPlaceHolder(Tree tree) {
        return tree != null
                && (tree.opCode() == ILLang.op_function
                || tree.opCode() == ILLang.op_program
                || tree.opCode() == ILLang.op_module
                || tree.opCode() == ILLang.op_class
                || tree.opCode() == ILLang.op_constructor
                || tree.opCode() == ILLang.op_destructor
                || tree.opCode() == ILLang.op_nameSpace)
                && tree.getAnnotation("Unit") != null;
    }

    public static boolean isADeclMayAppearFirst(Tree tree) {
        return tree.opCode() == ILLang.op_external
                || tree.opCode() == ILLang.op_intrinsic
                || tree.opCode() == ILLang.op_accessDecl
                && !isNullOrNoneOrEmptyList(tree.down(2))
                && !isIdentOf(tree.down(1),
                // Put here the list of access modifiers that should *follow* their variable's main type decl:
                new String[]{"allocatable", "target", "pointer"},  // ? in,out,inout,optional...
                false);
    }

    /**
     * @return true when "tree" is a reference to a variable writable at the present location.
     * This means that "tree" can be overwritten, modified,
     * passed as a "write" argument of a function
     * or put on the left side of an assignment.
     * WARNING: differs from isAWritableVarRef(): if this variable was declared as a "const"
     * argument of a C procedure, this method considers this tree is *not* writeable.
     */
    public static boolean isAVarRef(Tree tree, SymbolTable symbolTable) {
        switch (tree.opCode()) {
            case ILLang.op_ident: {
                SymbolDecl symbolDecl =
                        symbolTable == null ? null : symbolTable.getSymbolDecl(getIdentString(tree));
                if (symbolDecl != null) {
                    return symbolDecl.isA(SymbolTableConstants.VARIABLE) && !symbolDecl.hasModifier("constant")
                            && !symbolDecl.isCconst();
                } else {
                    return NewSymbolHolder.getAttachedVariableDecl(tree) != null;
                }
            }
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
                return isAVarRef(tree.down(1), symbolTable);
            default:
                return false;
        }
    }

    /**
     * @return true when "tree" is a reference to a variable intrinsically writable.
     * This means that "tree" can be overwritten, modified,
     * passed as a "write" argument of a function
     * or put on the left side of an assignment.
     * WARNING: this is not exactly the same as isAVarRef(): a FORTRAN CONSTANT
     * is considered *not* writable whereas a C "const" argument is considered
     * writable because "const" only means that this procedure promises it will
     * not write in the variable (but it would be possible). This is important
     * because the diff of tree will be writeable because we will not make it "const".
     */
    public static boolean isAWritableVarRef(Tree tree, SymbolTable symbolTable) {
        switch (tree.opCode()) {
            case ILLang.op_ident: {
                SymbolDecl symbolDecl = symbolTable.getSymbolDecl(getIdentString(tree));
                if (symbolDecl != null) {
                    return symbolDecl.isA(SymbolTableConstants.VARIABLE) && !symbolDecl.hasModifier("constant");
                } else {
                    return NewSymbolHolder.getAttachedVariableDecl(tree) != null;
                }
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
                return isAWritableVarRef(tree.down(1), symbolTable);
            default:
                return false;
        }
    }

    /**
     * Same as isAWritableVarRef() but rejects op_fieldAccess and op_substringAccess.
     */
    public static boolean isAWritableIdentVarRef(Tree tree, SymbolTable symbolTable) {
        switch (tree.opCode()) {
            case ILLang.op_ident: {
                SymbolDecl symbolDecl = symbolTable.getSymbolDecl(getIdentString(tree));
                if (symbolDecl != null) {
                    return symbolDecl.isA(SymbolTableConstants.VARIABLE) && !symbolDecl.hasModifier("constant");
                } else {
                    return NewSymbolHolder.getAttachedVariableDecl(tree) != null;
                }
            }
            case ILLang.op_arrayAccess:
            case ILLang.op_address:
            case ILLang.op_pointerAccess: {
                return isAWritableIdentVarRef(tree.down(1), symbolTable);
            }
            default:
                return false;
        }
    }

    /**
     * @return true when "tree" either is a reference to a variable or
     * is a constant immediate value, i.e. in other words when "tree"
     * does not need a computation to obtain its value.
     */
    public static boolean isAVarRefOrConstant(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
            case ILLang.op_address:
            case ILLang.op_minus:
            case ILLang.op_arrayConstructor:
                return isAVarRefOrConstant(tree.down(1));
            case ILLang.op_multiCast:
            case ILLang.op_cast:
                return isAVarRefOrConstant(tree.down(2));
            case ILLang.op_ident:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_stringCst:
            case ILLang.op_bitCst:
            case ILLang.op_label:
            case ILLang.op_star:
            case ILLang.op_none:
                return true;
            default:
                return false;
        }
    }

    public static boolean isAStringManipulation(Tree tree) {
        if (TapEnv.relatedUnit().isFortran()
                && tree.opCode() == ILLang.op_call) {
            String calledFuncName = getCalledNameString(tree);
            return calledFuncName != null &&
                    (calledFuncName.equals("trim") ||
                            calledFuncName.equals("adjustl") ||
                            calledFuncName.equals("adjustr"));
        } else {
            return false;
        }
    }

    public static boolean isAccessDeclValue(Tree tree, String value) {
        boolean result = tree.opCode() == ILLang.op_accessDecl
                && tree.down(1).opCode() == ILLang.op_ident;
        if (result) {
            result = tree.down(1).stringValue().equals(value);
        }
        return result;
    }

    /**
     * @param tree is an accessDecl Tree
     * @param rank of declared variable
     * @return FORTRAN_NAME if tree== accessDecl(accessDecl(ident:bind, expressions[ident:c]), expressions[ident:FORTRAN_NAME])
     * or return C_NAME if tree ==
     * accessDecl(accessDecl(ident:bind, expressions[ident:c, nameEq(ident:name, C_NAME)]), expressions[ident:FORTRAN_NAME]).
     * [llh 12Dec2022] TODO: make this method cleaner and more general, or remove if not used any more.
     */
    public static String getBindCValue(Tree tree, int rank) {
        String result = null;
        if (tree.opCode() == ILLang.op_accessDecl) {
            if (tree.down(1).down(2).length() == 1) {
                result = tree.down(2).down(1).stringValue();
            } else {
                result = tree.down(1).down(2).down(2).down(2).stringValue();
            }
        } else if (tree.opCode() == ILLang.op_varDeclaration) {
            Tree accessDeclTree = findAccessDecl(tree);
            if (accessDeclTree != null) {
                if (accessDeclTree.down(2).length() == 1) {
                    result = baseName(tree.down(3).down(rank));
                } else {
                    result = accessDeclTree.down(2).down(2).down(2).stringValue();
                }
            }
        }
        return result;
    }

    /** If the given listOfElements contains a child of the form nameEq("name","value"),
     * returns this "value". Otherwise returns null. */
    public static Tree getNamedElement(Tree listOfElements, String name) {
        Tree found = null ;
        Tree[] children = listOfElements.children() ;
        for (int i=0 ; i<children.length && found==null ; ++i) {
            if (children[i].opCode()==ILLang.op_nameEq && ILUtils.isIdent(children[i].down(1), name, true))
                found = children[i].down(2) ;
        }
        return found ;
    }

    private static Tree findAccessDecl(Tree tree) {
        Tree result = null;
        Tree currentTree;
        for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd() && result == null; i.advance()) {
            currentTree = i.get();
            if (currentTree.opCode() == ILLang.op_accessDecl) {
                result = currentTree;
            }
        }
        return result;
    }

    public static boolean instrHasSideEffect(Tree expr) {
        Tree[] exprSons;
        switch (expr.opCode()) {
            case ILLang.op_call:
            case ILLang.op_assign:
            case ILLang.op_ioCall:
                return true;
            default:
                exprSons = expr.children();
                boolean hasSideEffect = false;
                if (exprSons != null) {
                    int i = exprSons.length - 1;
                    while (!hasSideEffect && i >= 0) {
                        hasSideEffect = instrHasSideEffect(exprSons[i]);
                        --i;
                    }
                }
                return hasSideEffect;
        }
    }

    public static Tree findSizeofCall(Tree expression) {
        if (expression.opCode() == ILLang.op_sizeof) {
            return expression;
        } else {
            Tree[] exprSons = expression.children();
            Tree found = null;
            if (exprSons != null) {
                for (int i = exprSons.length - 1; i >= 0 && found == null; --i) {
                    found = findSizeofCall(exprSons[i]);
                }
            }
            return found;
        }
    }

    public static boolean isNullOrEmptyAtom(Tree tree) {
        if (tree == null) {
            return true;
        }
        if (!tree.isAtom()) {
            return false;
        }
        String atVal = tree.stringValue();
        return atVal == null || atVal.isEmpty();
    }

    public static boolean isNullOrNone(Tree tree) {
        return tree == null || tree.opCode() == ILLang.op_none;
    }

    public static boolean isNullOrNoneOrStar(Tree tree) {
        return tree == null || tree.opCode() == ILLang.op_none
                || tree.opCode() == ILLang.op_star;
    }

    public static boolean isNullOrNoneOrVoid(Tree tree) {
        return tree == null || tree.opCode() == ILLang.op_none
                || tree.opCode() == ILLang.op_void;
    }

    public static boolean isStar(Tree tree) {
        return tree != null && tree.opCode() == ILLang.op_star;
    }

    public static boolean isCall(Tree tree) {
        return tree.opCode() == ILLang.op_call
                || tree.opCode() == ILLang.op_assign && tree.down(2).opCode() == ILLang.op_call;
    }

    /**
     * @return tree if tree is a call,
     * tree.down(2) if tree is a call to a function with a return value assignment,
     * and null otherwise.
     */
    public static Tree getCall(Tree tree) {
        if (tree.opCode() == ILLang.op_call) {
            return tree;
        }
        if (tree.opCode() == ILLang.op_assign && tree.down(2).opCode() == ILLang.op_call) {
            return tree.down(2);
        }
        return null;
    }

    /**
     * When possible, extracts a list of ordered atomic statements from tree.
     * Reciprocal of buildSingleStatement().
     * Should be refined if necessary.
     */
    public static TapList<Tree> getListOfStatements(Tree tree) {
        if (tree.opCode() == ILLang.op_blockStatement) {
            return tree.childrenList();
        } else {
            return new TapList<>(tree, null);
        }
    }

    /**
     * When possible, turns the given ordered list of statements into a single
     * statement. Reciprocal of getListOfStatements().
     * Should be refined if necessary.
     */
    public static Tree buildSingleStatement(TapList<Tree> statements) {
        if (statements == null) {
            return build(ILLang.op_none);
        } else if (statements.tail == null) {
            return statements.head;
        } else {
            return build(ILLang.op_blockStatement, statements);
        }
    }

    /**
     * @return a list of op_ident Tree's, with the given identNames, and keeping the order.
     */
    public static TapList<Tree> buildListIdents(TapList<String> identNames) {
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        while (identNames != null) {
            tlResult = tlResult.placdl(build(ILLang.op_ident, identNames.head));
            identNames = identNames.tail;
        }
        return hdResult.tail;
    }

    /**
     * @return the number of successive individual atomic values in the list of expressions "tree".
     * If this length cannot be statically determined, return -1.
     */
    public static int buildIterativeLength(Tree tree, SymbolTable symbolTable) {
        int length = 1;
        switch (tree.opCode()) {
            case ILLang.op_expressions:
            case ILLang.op_varDeclarators: {
                int sublength;
                Tree[] exps = tree.children();
                length = 0;
                for (int i = exps.length - 1; i >= 0 && length != -1; --i) {
                    sublength = buildIterativeLength(exps[i], symbolTable);
                    length = sublength == -1 ? -1 : length + sublength;
                }
                break;
            }
            case ILLang.op_iterativeVariableRef: {
                length = buildIterativeLength(tree.down(1), symbolTable);
                if (length != -1) {
                    Tree sizeTree = buildSizeTree(tree.down(2).down(2), tree.down(2).down(3), tree.down(2).down(4));
                    Integer iters = symbolTable.computeIntConstant(sizeTree);
                    length = iters == null ? -1 : length * iters;
                }
                break;
            }
            default:
                WrapperTypeSpec type = symbolTable.typeOf(tree);
                TapList<ArrayDim> typeDims = type.getAllDimensions();
                int sublength;
                while (typeDims != null && length != -1) {
                    sublength = typeDims.head.size();
                    length = sublength == -1 ? -1 : length * sublength;
                    typeDims = typeDims.tail;
                }
                break;
        }
        return length;
    }

    public static boolean isNullOrEmptyList(Tree tree) {
        return tree == null ||
                tree.isList() && tree.children().length == 0;
    }

    public static boolean isNullOrNoneOrEmptyList(Tree tree) {
        return tree == null ||
                tree.opCode() == ILLang.op_none ||
                tree.isList() && tree.children().length == 0;
    }

    public static boolean isNotNoneNorEmpty(Tree tree) {
        int opCode = tree.opCode();
        if (opCode == ILLang.op_none) {
            return false;
        } else if (tree.isList()) {
            return tree.children().length != 0;
        } else {
            return true;
        }
    }

    public static boolean isNotNone(Tree tree) {
        return tree.opCode() != ILLang.op_none;
    }

    public static boolean isNotVoid(Tree tree) {
        return tree.opCode() != ILLang.op_void;
    }

    public static boolean isNotNoneNorVoid(Tree tree) {
        int opCode = tree.opCode();
        return opCode != ILLang.op_none && opCode != ILLang.op_void;
    }

    public static boolean isZero(Tree tree) {
        return tree.opCode() == ILLang.op_intCst && tree.intValue() == 0
                ||
                tree.opCode() == ILLang.op_realCst && ("0.0".equals(tree.stringValue())
                        || "0.D0".equals(tree.stringValue())
                        || tree.stringValue().startsWith("0.0_"));
    }

    public static TapList<Tree> commentsToListReversed(Tree commentsTree) {
        if (commentsTree == null) {
            return null;
        }
        TapList<Tree> result = null;
        Tree[] commentsSons = commentsTree.children();
        for (Tree commentsSon : commentsSons) {
            result = new TapList<>(commentsSon, result);
        }
        return result;
    }

    public static Tree listReversedToComments(TapList<Tree> commentsListReversed) {
        if (commentsListReversed == null) {
            return null;
        }
        TapList<Tree> commentsList = TapList.nreverse(commentsListReversed);
        return build(ILLang.op_comments, commentsList);
    }

    public static void adoptOrAppendComments(Tree commentsTree, String commentsString,
                                             Tree commentedTree) {
        if (commentsTree != null) {
            Tree existingAnnot = commentedTree.getAnnotation(commentsString);
            if (existingAnnot != null) {
                commentsTree = appendComments(existingAnnot, commentsTree);
            }
            commentedTree.setAnnotation(commentsString, commentsTree);
        }
    }

    public static TapList<Tree> copiedSonsList(Tree tree) {
        TapList<Tree> list = null;
        int nbSons = tree.length();
        Tree[] sons = tree.children();
        for (int index = nbSons - 1; index >= 0; --index) {
            list = new TapList<>(sons[index].copy(), list);
        }
        return list;
    }

    /**
     * Because all the "appendComment*" methods are erratic on the tree nesting...,
     * this method does a final flattening to make comments canonic.
     */
    public static TapList<Tree> collectCommentStrings(Tree commentsTree) {
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        ccsRec(commentsTree, tlResult);
        return hdResult.tail;
    }

    public static TapList<Tree> collectCommentStrings(TapList<Tree> commentsTrees) {
        TapList<Tree> hdResult = new TapList<>(null, null);
        TapList<Tree> tlResult = hdResult;
        while (commentsTrees != null) {
            tlResult = ccsRec(commentsTrees.head, tlResult);
            commentsTrees = commentsTrees.tail;
        }
        return hdResult.tail;
    }

    private static TapList<Tree> ccsRec(Tree commentsTree, TapList<Tree> tlResult) {
        if (commentsTree.opCode() == ILLang.op_stringCst) {
            tlResult = tlResult.placdl(build(ILLang.op_stringCst, commentsTree.stringValue()));
        } else if (commentsTree.opCode() == ILLang.op_pppLine) {
            tlResult = tlResult.placdl(ILUtils.copy(commentsTree)) ;
        } else if (commentsTree.isList()) {
            Tree[] children = commentsTree.children();
            for (Tree child : children) {
                tlResult = ccsRec(child, tlResult);
            }
        }
        return tlResult;
    }

    public static Tree appendComments(Tree comments1, Tree comments2) {
        return appendComments1(comments1, comments2, ILLang.op_comments);
    }

    public static void appendCommentsBlock(Tree comments1, Tree comments2) {
        appendComments1(comments1, comments2, ILLang.op_commentsBlock);
    }

    private static Tree appendComments1(Tree comments1, Tree comments2, int commentsOperator) {
        // comments1 est un op_comments ou op_commentsBlock
        // comments2 est un op_comments ou op_commentsBlock ou un op_stringCst
        if (comments2 != null) {
            if (comments2.opCode() == ILLang.op_stringCst) {
                if (comments1 == null) {
                    comments2 = build(commentsOperator, copy(comments2));
                    return comments2;
                } else {
                    comments1.addChild(comments2, comments1.length() + 1);
                    return comments1;
                }
            } else {
                TapList<Tree> sons = copiedSonsList(comments2);
                return appendComments(comments1, sons, ILLang.op_comments);
            }
        } else {
            return comments1;
        }
    }

    public static Tree appendComments(Tree oldComments, TapList<Tree> newComments, int operatorCode) {
        if (oldComments == null) {
            oldComments = build(operatorCode, newComments);
        } else {
            int rank = oldComments.length() + 1;
            while (newComments != null) {
                oldComments.addChild(newComments.head, rank);
                rank = rank + 1;
                newComments = newComments.tail;
            }
        }
        return oldComments;
    }

    public static void setOrigTree(Tree tree, Tree origTree) {
        tree.setAnnotation("origTree", origTree);
    }

    public static void setPosition(Tree tree, int position) {
        tree.setAnnotation("position", position);
    }

    public static int getPosition(Tree tree) {
        Object positionObject = tree.getAnnotation("position");
        if (positionObject == null) {
            return 0;
        } else {
            return (Integer) positionObject;
        }
    }

    public static Tree removeSourceCodeAnnot(Tree tree) {
        tree.removeAnnotation("toOtherTags");
        return tree;
    }

    /**
     * @return an expression Tree that evaluates to the size
     * of this dimension (i.e. (to - from + stride)/stride
     * or equivalently (to - from)/stride + 1  ).
     */
    public static Tree buildSizeTree(Tree from, Tree to, Tree stride) {
        Tree result;
        if (to.opCode() == ILLang.op_none || to.opCode() == ILLang.op_star) {
            result = null;
        } else {
            int intFrom = 1;
            int intTo = 1;
            int intStride = 1;
            boolean knownIntFrom = true;
            boolean knownIntTo = true;
            boolean knownIntStride = true;
            if (from.opCode() == ILLang.op_none) {
                intFrom = 1;
            } else if (from.opCode() == ILLang.op_intCst) {
                intFrom = from.intValue();
            } else {
                knownIntFrom = false;
            }
            if (to.opCode() == ILLang.op_intCst) {
                intTo = to.intValue();
            } else {
                knownIntTo = false;
            }
            if (stride == null || stride.opCode() == ILLang.op_none) {
                intStride = 1;
            } else if (stride.opCode() == ILLang.op_intCst) {
                intStride = stride.intValue();
            } else if (evalsToOne(stride)) {
		intStride = 1 ;
            } else if (evalsToMinusOne(stride)) {
		intStride = -1 ;
	    } else {
                knownIntStride = false;
            }
            if (knownIntFrom) {
                if (knownIntTo) {
                    if (knownIntStride) {
                        result = build(ILLang.op_intCst,
                                (intTo - intFrom) / intStride + 1);
                    } else {
                        result = build(ILLang.op_add,
                                build(ILLang.op_div,
                                        build(ILLang.op_intCst,
                                                intTo - intFrom),
                                        stride.copy()),
                                build(ILLang.op_intCst, 1));
                    }
                } else {
                    if (knownIntStride && intStride == 1) {
                        if (intFrom == 1) {
                            result = to.copy();
                        } else {
                            result = subTree(to.copy(),
                                    build(ILLang.op_intCst, intFrom - 1));
                        }
                    } else if (knownIntStride && intStride == -1) {
                        if (intFrom == -1) {
                            result = minusTree(to.copy());
                        } else {
                            result = subTree(build(ILLang.op_intCst, intFrom + 1),
                                    to.copy());
                        }
                    } else {
                        if (intFrom == 0) {
                            result = to.copy();
                        } else {
                            result = subTree(to.copy(),
                                    build(ILLang.op_intCst, intFrom));
                        }
                        assert stride != null;
                        result = build(ILLang.op_add,
                                build(ILLang.op_div,
                                        result, stride.copy()),
                                build(ILLang.op_intCst, 1));
                    }
                }
            } else {
                if (knownIntTo) {
                    if (knownIntStride && intStride == 1) {
                        if (intTo == -1) {
                            result = minusTree(from.copy());
                        } else {
                            result = subTree(build(ILLang.op_intCst, intTo + 1),
                                    from.copy());
                        }
                    } else if (knownIntStride && intStride == -1) {
                        if (intTo == 1) {
                            result = from.copy();
                        } else {
                            result = subTree(from.copy(),
                                    build(ILLang.op_intCst, intTo - 1));
                        }
                    } else {
                        assert stride != null;
                        if (intTo == 0) {
                            result = subTree(build(ILLang.op_intCst, 1),
                                    build(ILLang.op_div,
                                            from.copy(), stride.copy()));
                        } else {
                            result = build(ILLang.op_add,
                                    build(ILLang.op_div,
                                            build(ILLang.op_sub,
                                                    build(ILLang.op_intCst, intTo),
                                                    from.copy()),
                                            stride.copy()),
                                    build(ILLang.op_intCst, 1));
                        }
                    }
                } else {
                    if (knownIntStride && intStride == 1) {
                        result = addTree(subTree(to.copy(), from.copy()),
                                build(ILLang.op_intCst, 1));
                    } else if (knownIntStride && intStride == -1) {
                        result = addTree(subTree(from.copy(), to.copy()),
                                build(ILLang.op_intCst, 1));
                    } else {
                        assert stride != null;
                        result = build(ILLang.op_add,
                                build(ILLang.op_div,
                                        subTree(to.copy(),
                                                from.copy()),
                                        stride.copy()),
                                build(ILLang.op_intCst, 1));
                    }
                }
            }
        }
        return result;
    }

    /** Returns a copy of ref-expression "expr" in which array accesses are removed.
     * Example: *(A(2,:).field(j+1)).field2 gives *(A.field).field2 */
    public static Tree stripArrayAccesses(Tree expr) {
        if (expr==null) return null ;
        switch (expr.opCode()) {
            case ILLang.op_arrayAccess:
                return stripArrayAccesses(expr.down(1)) ;
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return ILUtils.build(expr.opCode(),
                         stripArrayAccesses(expr.down(1)),
                         ILUtils.copy(expr.down(2))) ;
            case ILLang.op_ident:
            default :
                return ILUtils.copy(expr) ;
        }
    }

    public static Tree modifyRootName(Tree model, String oldName, String newName) {
        Tree result = null;
        switch (model.opCode()) {
            case ILLang.op_ident:
                if (model.stringValue().equals(oldName)) {
                    result = build(ILLang.op_ident, newName);
                } else {
                    result = copy(model);
                }
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                result = model.operator().tree();
                result.setChild(modifyRootName(model.down(1), oldName, newName), 1);
                result.setChild(copy(model.down(2)), 2);
                break;
            default:
                TapEnv.toolError("(modifyRootName) Unexpected operator: " + model.opName());
                break;
        }
        assert result != null;
        result.copyAnnotations(model);
        return result;
    }

    /**
     * Replaces all occurrences of op_ident(identString) with "newTree" in the given "tree".
     * Modifies "tree" in place.
     *
     * @return the modified tree.
     */
    public static Tree replaceIdentStringOccurences(Tree tree,
                                                    String identString, Tree newTree) {
        if (tree.opCode() == ILLang.op_ident && tree.stringValue().equals(identString)) {
            return copy(newTree);
        } else if (tree.isAtom()) {
            return tree;
        } else {
            for (int i = tree.length() - 1; i >= 0; --i) {
                tree.setChild(replaceIdentStringOccurences(tree.down(i + 1), identString, newTree), i + 1);
            }
            return tree;
        }
    }

    /** If the given tree contains call arguments, and those have been "normalized"
     * by inserting none() arguments and removing the nameEq's, puts the argument list
     * back in the fortran-style form with no none() and named arguments when needed. */
    public static void putBackNamedArguments(Tree tree) {
        if (!tree.isAtom()) {
            for (int i=tree.length() ; i>0 ; --i) {
                putBackNamedArguments(tree.down(i)) ;
            }
        }
        if (tree.opCode()==ILLang.op_call) {
            Tree[] args = ILUtils.getArguments(tree).children() ;
            int noneFirstRk = -1 ;
            for (int i=0 ; noneFirstRk==-1 && i<args.length ; ++i) {
                if (args[i].opCode()==ILLang.op_none)
                    noneFirstRk = i ;
            }
            if (noneFirstRk!=-1) {
                TapList<Tree> newArgs = null ;
                for (int i=args.length-1 ; i>=0 ; --i) {
                    if (args[i].opCode()!=ILLang.op_none) {
                        Tree newArg ;
                        newArg = ILUtils.copy(args[i]) ;
                        Tree sourceNameEq = args[i].getAnnotation("sourcetree") ;
//                         if (i>=noneFirstRk) {  //Comment out to put back every nameEq
                            if (sourceNameEq!=null && sourceNameEq.opCode()==ILLang.op_nameEq) {
                                newArg.removeAnnotation("sourcetree") ;
                                newArg = ILUtils.build(ILLang.op_nameEq,
                                           ILUtils.copy(sourceNameEq.down(1)),
                                           newArg) ;
                            }
//                         }
                        newArgs = new TapList<>(newArg, newArgs) ;
                    }
                }
                ILUtils.setArguments(tree, ILUtils.build(ILLang.op_expressions, newArgs)) ;
            }
        }
    }

    /**
     * @return true iff "tree" matches "patternTree". As a side-effect,
     * places inside "keyTreeList",  in front of each metavariable Tree
     * from "patternTree", the matched Tree from "tree".
     * However, even if finally there is no matching, tree traversal goes on and
     * keeps associating trees when reasonably possible.
     */
    public static boolean matches(Tree tree, Tree patternTree, TapList<TapPair<Tree, Tree>> keyTreeList) {
        TapPair<Tree, Tree> place = TapList.assqTree(patternTree, keyTreeList);
        if (place != null) {
            place.second = tree;
            return true;
        } else {
            boolean matches = true;
            if (tree.operator() != patternTree.operator()) {
                matches = false;
            } else if (tree.isAtom()) {
                matches = tree.equalsTree(patternTree);
            } else {
                Tree[] subTrees = tree.children();
                Tree[] subPatternTrees = patternTree.children();
                int len = subTrees.length;
                if (len < subPatternTrees.length) {
                    matches = false;
                } else if (len > subPatternTrees.length) {
                    len = subPatternTrees.length;
                    matches = false;
                }
                for (int i = 0; i < len; ++i) {
                    if (!matches(subTrees[i], subPatternTrees[i], keyTreeList)) {
                        matches = false;
                    }
                }
            }
            return matches;
        }
    }

    /**
     * Builds a recursive copy of origTree, observing separately copied
     * sub-trees given by "subTreeCopies".
     * The TapList "subTreeCopies" is a key-list of TapPair's (Tree . Tree).
     * For each sub-tree of origTree that appears as
     * a key in subTreeCopies, this sub-tree is not copied, and instead the
     * corresponding "value" tree is inserted into the copied tree.
     * In the special case where the "value" Tree is null, a copy of the
     * key-tree is inserted into the copied tree.
     * This function builds also a new key-list, where the keys are the
     * copies of the original key Trees. These copies are those who appear
     * in the copied Tree. The values in this new key-list are null.
     *
     * @return the TapPair of the copied Tree and the new key-list.
     */
    public static TapPair<Tree, TapList<TapPair<Tree, Tree>>> copyWatchingSubtrees(
            Tree origTree, TapList<TapPair<Tree, Tree>> subTreeCopies) {
        TapList<TapPair<Tree, Tree>> toNewKeyList = new TapList<>(null, null);
        Tree copiedTree = cwsr(origTree, subTreeCopies, toNewKeyList);
        return new TapPair<>(copiedTree, toNewKeyList.tail);
    }

    private static Tree cwsr(Tree origTree, TapList<TapPair<Tree, Tree>> subTreeCopies,
                             TapList<TapPair<Tree, Tree>> toNewKeyList) {
        TapPair<Tree, Tree> place = TapList.assq(origTree, subTreeCopies);
        Tree resultTree;
        if (place == null) {
            Operator operator = origTree.operator();
            if (!origTree.isAtom()) {
                resultTree = operator.tree();
                Tree[] subTrees = origTree.children();
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    resultTree.setChild(
                            cwsr(subTrees[i], subTreeCopies, toNewKeyList), i + 1);
                }
                if (origTree.allAnnotations() != null) {
                    resultTree.copyAnnotations(origTree);
                }
            } else {
                resultTree = copy(origTree);
            }
        } else {
            if (place.second == null) {
                resultTree = copy(origTree);
            } else {
                resultTree = place.second;
            }
            toNewKeyList.placdl(new TapPair<>(resultTree, null));
        }
        return resultTree;
    }

    /**
     * Same as copyWatchingSubtrees, but tree equality tests are made with ".equals".
     *
     * @return the copied tree.
     */
    public static Tree copyWatchingEqualsSubtrees(Tree origTree, TapList<TapPair<Tree, Tree>> subTreeCopies) {
        TapPair<Tree, Tree> place = TapList.assqTree(origTree, subTreeCopies);
        Tree resultTree;
        if (place == null) {
            Operator operator = origTree.operator();
            if (!origTree.isAtom()) {
                resultTree = operator.tree();
                Tree[] subTrees = origTree.children();
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    resultTree.setChild(
                            copyWatchingEqualsSubtrees(subTrees[i], subTreeCopies), i + 1);
                }
                if (origTree.allAnnotations() != null) {
                    resultTree.copyAnnotations(origTree);
                }
            } else {
                resultTree = copy(origTree);
            }
        } else {
            if (place.second == null) {
                resultTree = copy(origTree);
            } else {
                resultTree = copy(place.second);
            }
        }
        return resultTree;
    }

    /**
     * @return the "position" of "tree".
     * To do that, go up until a tree annotated with
     * its "position" is found, then depth-first sweep this
     * above tree, incrementing the position, until "tree"
     * is found again.
     */
    public static int position(Tree tree) {
        Tree aboveTree = tree;
        while (aboveTree != null && getPosition(aboveTree) == 0) {
            aboveTree = aboveTree.parent();
        }
        ToInt foundPosition = new ToInt(-1);
        if (aboveTree != null) {
            sweepPosition(aboveTree, tree, getPosition(aboveTree), foundPosition);
        }
        return foundPosition.get();
    }

    /**
     * Utility for position(). Sweeps through inTree, in pre-order, incrementing position,
     * until finding searchTree, whose position is the result we are looking for
     */
    private static int sweepPosition(Tree inTree, Tree searchTree, int position, ToInt foundPosition) {
        if (foundPosition.get() == -1) {
            if (inTree == searchTree)
            // then we have found the searchTree: this is the position we are looking for.
            {
                foundPosition.set(position);
            } else {
                // else go on searching inside inTree:
                Tree[] subTrees = inTree.children();
                // count 1 for the head operator:
                ++position;
                if (subTrees != null) {
                    for (Tree subTree : subTrees) {
                        position = sweepPosition(subTree, searchTree, position, foundPosition);
                    }
                }
                // count 1 for the endOfList "operator":
                if (inTree.isList()) {
                    ++position;
                }
            }
        }
        return position;
    }

    /**
     * @return a copy of the given "tree".
     * The original Tree and the copied Tree share their set of annotations.
     * Each subtree annotated with a "NewSymbolHolder", (i.e. the subtree might change later)
     * is copied into a new subtree with the same "NewSymbolHolder", and registered by
     * this "NewSymbolHolder".
     * This way, when the "NewSymbolHolder" eventually decides on the final form
     * of the subtree, the copied subtree will receive the final form too.
     */
    public static Tree copy(Tree tree) {
        return tree == null ? null : tree.copy();
    }

    /**
     * Reads the annotations at the root of origTree and tries to
     * merge them with the annotations at the root of destTree.
     * This depends on the "meaning" of the annotations created by Tapenade.
     * You may develop this method further if other annotations get lost...
     */
    public static void incorporateAnnotations(Tree destTree, Tree origTree) {
        if (origTree != null && destTree != null) {
            TapList<String> origMessages = origTree.getAnnotation("message");
            TapList<String> destMessages = destTree.getAnnotation("message");
            if (origMessages != destMessages) { // to avoid creating cycling lists!
                if (destMessages == null) {
                    destTree.setAnnotation("message", origMessages);
                } else {
                    TapList<String> tlDestMessages = destMessages;
                    while (tlDestMessages.tail != null) {
                        tlDestMessages = tlDestMessages.tail;
                    }
                    tlDestMessages.tail = origMessages;
                }
            }
        }
    }

    /**
     * Given "returnTree" (a C-style "return &lt;expression&gt;" tree), creates "assignTree"
     * (a tree of shape "returnVarName := &lt;expression&gt;") that shares the &lt;expression&gt; tree
     * instead of copying it. This damages the original returnTree.
     * WARNING: IMMEDIATELY after usage of the created assignTree, the returnTree
     * MUST be repaired into its original form with resetReturnFromAssign(returnTree, assignTree).
     */
    public static Tree turnReturnIntoAssign(Tree returnTree, String returnVarName) {
        Tree expression = returnTree.cutChild(1);
        return build(ILLang.op_assign, build(ILLang.op_ident, returnVarName), expression);
    }

    /**
     * Restoration. cf turnReturnIntoAssign().
     */
    public static void resetReturnFromAssign(Tree returnTree, Tree assignTree) {
        Tree expression = assignTree.cutChild(2);
        returnTree.setChild(expression, 1);
    }

    /**
     * Modifies assigmentTree to represent the corresponding "true" assignment.
     * For instance, given {@code (*x) = y+1} which is the 2nd child of {@code float *x = y+1},
     * turns it temporarily into {@code x = y+1}. This damages the original assignment.
     * WARNING: IMMEDIATELY after usage of the modified assignment tree, it MUST
     * be repaired into its original form with resetAssignFromInitDecl(returnedTree).
     *
     * @param assignmentTree Tree present in a declaration with initialization.
     */
    public static void turnAssignFromInitDecl(Tree assignmentTree) {
        if (assignmentTree.getAnnotation("danglingDeclChain") != null) {
            TapEnv.toolError("Danger: forbidden nested call of turnAssignFromInitDecl()");
        } else {
            Tree parentTreeCut = assignmentTree;
            int parentRankCut = 1;
            Tree declDeclarator = assignmentTree.down(1);
            //Peel off all type decoration around the declared identifier:
            while (declDeclarator != null
                    && declDeclarator.opCode() != ILLang.op_ident
                    && declDeclarator.opCode() != ILLang.op_arrayDeclarator) {
                switch (declDeclarator.opCode()) {
                    case ILLang.op_pointerDeclarator:
                    case ILLang.op_functionDeclarator:
                    case ILLang.op_bitfieldDeclarator:
                        parentTreeCut = declDeclarator;
                        parentRankCut = 1;
                        declDeclarator = declDeclarator.down(1);
                        break;
                    case ILLang.op_modifiedDeclarator:
                        parentTreeCut = declDeclarator;
                        parentRankCut = 2;
                        declDeclarator = declDeclarator.down(2);
                        break;
                    default:
                        TapEnv.toolWarning(-1, "(Peeling declarator) Unexpected operator: " + declDeclarator.opName());
                        declDeclarator = null;
                        break;
                }
            }
            if (parentTreeCut != assignmentTree && declDeclarator != null) {
                declDeclarator = parentTreeCut.cutChild(parentRankCut);
                Tree danglingDeclChain = assignmentTree.cutChild(1);
                assignmentTree.setAnnotation("danglingDeclChain", danglingDeclChain);
                assignmentTree.setChild(declDeclarator, 1);
            }
        }
    }

    /**
     * Restoration. cf turnAssignFromInitDecl().
     */
    public static void resetAssignFromInitDecl(Tree assignmentTree) {
        Tree danglingDeclChain = assignmentTree.getAnnotation("danglingDeclChain");
        if (danglingDeclChain != null) {
            assignmentTree.removeAnnotation("danglingDeclChain");
            Tree declDeclarator = danglingDeclChain;
            Tree parentTreeCut = null;
            int parentRankCut = -1;
            while (!isNullOrNone(declDeclarator)) {
                switch (declDeclarator.opCode()) {
                    case ILLang.op_pointerDeclarator:
                    case ILLang.op_functionDeclarator:
                    case ILLang.op_bitfieldDeclarator:
                        parentTreeCut = declDeclarator;
                        parentRankCut = 1;
                        declDeclarator = declDeclarator.down(1);
                        break;
                    case ILLang.op_modifiedDeclarator:
                        parentTreeCut = declDeclarator;
                        parentRankCut = 2;
                        declDeclarator = declDeclarator.down(2);
                        break;
                    default:
                        TapEnv.toolWarning(-1, "(un-Peeling declarator) Unexpected operator: " + declDeclarator.opName());
                        break;
                }
            }
            Tree leaf = assignmentTree.cutChild(1);
            assignmentTree.setChild(danglingDeclChain, 1);
            assert parentTreeCut != null;
            parentTreeCut.setChild(leaf, parentRankCut);
        }
    }

    //TODO: fix incoherence with turnAssignFromInitDecl():
    //  this splitDeclInit() peels of ILLang.op_arrayDeclarator,
    //  turnAssignFromInitDecl() does not !
    public static TapPair<Tree, Tree> splitDeclInit(Tree tree, boolean addSaveModifier, Unit curUnit) {
        Tree type = tree.opCode() == ILLang.op_varDeclaration ? copy(tree.down(2)) : copy(tree.down(1));
        Tree[] declarators = tree.opCode() == ILLang.op_varDeclaration ? tree.down(3).children() : tree.down(2).children();
        TapList<Tree> newAssignments = null;
        TapList<Tree> newDeclarators = null;
        Tree newVarDecl = null;
        // ne pas splitter real :: two=2.0 en fortran, le split ne donne pas le meme resultat!
        for (int i = declarators.length - 1; i >= 0; i--) {
            if (declarators[i].opCode() == ILLang.op_assign) {
                if (addSaveModifier
                        && contains(type, ILLang.op_ident, "save") == null) {
                    type = build(ILLang.op_modifiedType,
                            build(ILLang.op_modifiers,
                                    build(ILLang.op_ident, "save")),
                            type);
                }
                newAssignments = new TapList<>(
                        build(ILLang.op_assign,
                                copy(peelDeclarators(declarators[i].down(1))),
                                copy(declarators[i].down(2))),
                        newAssignments);
                if (curUnit.isC()) {
                    newDeclarators = new TapList<>(
                            copy(declarators[i]),
                            newDeclarators);
                } else {
                    newDeclarators = new TapList<>(
                            copy(declarators[i].down(1)),
                            newDeclarators);
                }
            } else if (declarators[i].opCode() == ILLang.op_pointerAssign) {
                // TODO modifier save?
                newAssignments = new TapList<>(
                        build(ILLang.op_pointerAssign,
                                copy(peelDeclarators(declarators[i].down(1))),
                                copy(declarators[i].down(2))),
                        newAssignments);
                newDeclarators = new TapList<>(
                        copy(declarators[i].down(1)),
                        newDeclarators);
            } else {
                newDeclarators = new TapList<>(
                        copy(declarators[i]),
                        newDeclarators);
            }
        }
        if (newAssignments != null && newAssignments.tail != null) {
            newAssignments = new TapList<>(
                    build(ILLang.op_blockStatement,
                            newAssignments),
                    null);
        }
        if (tree.opCode() == ILLang.op_varDeclaration) {
            newVarDecl = build(tree.opCode(),
                    build(ILLang.op_modifiers),
                    type,
                    build(ILLang.op_declarators,
                            newDeclarators));
        }
        return new TapPair<>(newVarDecl == null
                ? build(tree.opCode(), type, build(ILLang.op_declarators, newDeclarators))
                : newVarDecl,
                newAssignments == null ? null : newAssignments.head);
    }

    private static Tree peelDeclarators(Tree declarator) {
        switch (declarator.opCode()) {
            case ILLang.op_pointerDeclarator:
            case ILLang.op_sizedDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_arrayDeclarator: //warning: C arrays are sligtly different from pointers
                return peelDeclarators(declarator.down(1));
            case ILLang.op_modifiedDeclarator:
                return peelDeclarators(declarator.down(2));
            default:
                return declarator;
        }
    }

    /**
     * Given do header, build assignment.
     *
     * @param doTree do header {@code i=a,b,c"}
     * @return assignment statement {@code i:=a"}.
     */
    public static Tree buildInitAssign(Tree doTree) {
        return build(ILLang.op_assign, copy(doTree.down(1)), copy(doTree.down(2)));
    }

    /**
     * @return the list of Trees "refsR", augmented with all the expressions
     * recursively needed to evaluate expressions in "treeOfExps".
     * New expressions are added only if they are not already present.
     * If "keepTop" is false, top-level expressions in "treeOfExps"
     * are not returned.
     */
    public static TapList<Tree> usedVarsInTreeOfExps(TapList<Tree> treeOfExps,
                                                     TapList<Tree> refsR, boolean keepTop) {
        Tree expression;
        while (treeOfExps != null) {
            expression = treeOfExps.head;
            refsR = usedVarsInExp(expression, refsR, keepTop);
            treeOfExps = treeOfExps.tail;
        }
        return refsR;
    }

    /**
     * Augment the list of Trees "refsR" with all the expressions
     * recursively needed to evaluate "expression".
     * New expressions are added only if they are not already present.
     * @param keepTop if false, the top "expression" itself is not added.
     * @return the augmented "refsR" list.
     */
    public static TapList<Tree> usedVarsInExp(
            Tree expression, TapList<Tree> refsR, boolean keepTop) {
        if (expression == null) {
            return refsR;
        }
        switch (expression.opCode()) {
            // The most meaningful cases first:
            case ILLang.op_address:
                return usedVarsInExp(expression.down(1), refsR, false);
            case ILLang.op_fieldAccess:
                if (keepTop) refsR = addTreeInList(expression, refsR);
                return usedVarsInExp(expression.down(1), refsR, false);
            case ILLang.op_pointerAccess:
                if (keepTop) refsR = addTreeInList(expression, refsR);
                refsR = usedVarsInExp(expression.down(1), refsR, true);
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                return refsR;
            case ILLang.op_arrayAccess:
                if (keepTop) refsR = addTreeInList(expression, refsR);
                refsR = usedVarsInExp(expression.down(1), refsR, false);
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                return refsR;
            case ILLang.op_ident:
                if (keepTop) refsR = addTreeInList(expression, refsR);
                return refsR;
            case ILLang.op_call:
                refsR = usedVarsInExp(getObject(expression), refsR, true);
                return usedVarsInExp(getArguments(expression), refsR, true);
            // Then, moderately important cases:
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
                refsR = usedVarsInExp(expression.down(1), refsR, false);
                return usedVarsInExp(expression.down(2), refsR, true);
            case ILLang.op_strings:
            case ILLang.op_expressions:
            case ILLang.op_arrayConstructor: {
                Tree[] exps = expression.children();
                for (Tree exp : exps) {
                    refsR = usedVarsInExp(exp, refsR, keepTop);
                }
                return refsR;
            }
            case ILLang.op_blockStatement:
            case ILLang.op_dimColons:
            case ILLang.op_declarators: {
                Tree[] exps = expression.children();
                for (Tree exp : exps) {
                    refsR = usedVarsInExp(exp, refsR, true);
                }
                return refsR;
            }
            case ILLang.op_constructorCall:
            case ILLang.op_varDeclaration:
                return usedVarsInExp(expression.down(3), refsR, true);
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_sizeof:
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_where:
            case ILLang.op_switch:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_sizedDeclarator:
                return usedVarsInExp(expression.down(1), refsR, true);
            case ILLang.op_arrayDeclarator:
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                if (keepTop) refsR = addTreeInList(expression, refsR);
                return refsR;
            case ILLang.op_binary:
                refsR = usedVarsInExp(expression.down(1), refsR, true);
                return usedVarsInExp(expression.down(3), refsR, true);
            case ILLang.op_arrayTriplet:
            case ILLang.op_if:
            case ILLang.op_ifExpression:
                // In IL, an expression may be a "if".
                // Moreover, the "protected expression" mechanism creates
                // if-expressions even for Fortran.
                refsR = usedVarsInExp(expression.down(1), refsR, true);
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                return usedVarsInExp(expression.down(3), refsR, true);
            case ILLang.op_iterativeVariableRef:
                return usedVarsInExp(expression.down(1), refsR, keepTop);
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                return usedVarsInExp(expression.down(2), refsR, true);
            case ILLang.op_loop:
                return usedVarsInExp(expression.down(4), refsR, true);
            case ILLang.op_do:
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                refsR = usedVarsInExp(expression.down(3), refsR, true);
                return usedVarsInExp(expression.down(4), refsR, true);
            // Last, trivial cases:
            case ILLang.op_dimColon:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_neq:
            case ILLang.op_eq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_ge:
            case ILLang.op_le:
            case ILLang.op_gt:
            case ILLang.op_lt:
            case ILLang.op_concat:
            case ILLang.op_concatIdent:
            case ILLang.op_nameEq:
            case ILLang.op_complexConstructor:
            case ILLang.op_return:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
                refsR = usedVarsInExp(expression.down(1), refsR, true);
                return usedVarsInExp(expression.down(2), refsR, true);
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_ioCall:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_label:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_compGoto:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_continue:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_common:
            case ILLang.op_nameList:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_save:
            case ILLang.op_data:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return refsR;
            default:
                TapEnv.toolWarning(-1, "(Variables used in expression) Unexpected operator: " + expression.opName());
                return refsR;
        }
    }

    /**
     * Augment the list of Trees "refsR" with all the expressions whose primal
     * is recursively needed to evaluate the diff of "expression".
     * Augment the (hatted) list of Trees "toDiffRefsR" with all the expressions whose diff
     * is recursively needed to evaluate the diff of "expression".
     * New expressions are added only if they are not already present.
     * Example: for *(p[i+j]), i , j go to refsR, and p, p[i], (and if keepTop *(p[i])) go to toDiffRefsR
     * @param toDiffRefsR the hatted list that accumulates the expressions whose diff is used. 
     * @param keepTop if false, the top "expression" itself is not added.
     * @return the augmented "refsR" list.
     */
    public static TapList<Tree> usedVarsInDiffExp(Tree expression, TapList<Tree> refsR,
                                                  TapList<Tree> toDiffRefsR, boolean keepTop) {
        if (expression == null) {
            return refsR;
        }
        switch (expression.opCode()) {
            // The most meaningful cases first:
            case ILLang.op_address:
                return usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, false);
            case ILLang.op_fieldAccess:
                if (keepTop) toDiffRefsR.tail = addTreeInList(expression, toDiffRefsR.tail) ;
                return usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, false);
            case ILLang.op_pointerAccess:
                if (keepTop) toDiffRefsR.tail = addTreeInList(expression, toDiffRefsR.tail) ;
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, true);
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                return refsR;
            case ILLang.op_arrayAccess:
                if (keepTop) toDiffRefsR.tail = addTreeInList(expression, toDiffRefsR.tail) ;
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, false);
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                return refsR;
            case ILLang.op_ident:
                if (keepTop) toDiffRefsR.tail = addTreeInList(expression, toDiffRefsR.tail) ;
                return refsR;
            case ILLang.op_call:
                refsR = usedVarsInDiffExp(getObject(expression), refsR, toDiffRefsR, true);
                return usedVarsInDiffExp(getArguments(expression), refsR, toDiffRefsR, true);
            // Then, moderately important cases:
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, false);
                return usedVarsInDiffExp(expression.down(2), refsR, toDiffRefsR, true);
            case ILLang.op_strings:
            case ILLang.op_expressions:
            case ILLang.op_arrayConstructor: {
                Tree[] exps = expression.children();
                for (Tree exp : exps) {
                    refsR = usedVarsInDiffExp(exp, refsR, toDiffRefsR, keepTop);
                }
                return refsR;
            }
            case ILLang.op_blockStatement:
            case ILLang.op_dimColons:
            case ILLang.op_declarators: {
                Tree[] exps = expression.children();
                for (Tree exp : exps) {
                    refsR = usedVarsInDiffExp(exp, refsR, toDiffRefsR, true);
                }
                return refsR;
            }
            case ILLang.op_constructorCall:
            case ILLang.op_varDeclaration:
                return usedVarsInDiffExp(expression.down(3), refsR, toDiffRefsR, true);
            case ILLang.op_allocate:
            case ILLang.op_deallocate:
            case ILLang.op_nullify:
            case ILLang.op_sizeof:
            case ILLang.op_minus:
            case ILLang.op_not:
            case ILLang.op_where:
            case ILLang.op_switch:
            case ILLang.op_pointerDeclarator:
            case ILLang.op_functionDeclarator:
            case ILLang.op_sizedDeclarator:
                return usedVarsInExp(expression.down(1), refsR, true);
            case ILLang.op_arrayDeclarator:
                refsR = usedVarsInExp(expression.down(2), refsR, true);
                if (keepTop) toDiffRefsR.tail = addTreeInList(expression, toDiffRefsR.tail) ;
                return refsR;
            case ILLang.op_binary:
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, true);
                return usedVarsInDiffExp(expression.down(3), refsR, toDiffRefsR, true);
            case ILLang.op_arrayTriplet:
            case ILLang.op_if:
            case ILLang.op_ifExpression:
                /* In IL, an expression may be a "if".
                 * Moreover, the "protected expression" mechanism creates
                 * if-expressions even for Fortran. */
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, true);
                refsR = usedVarsInDiffExp(expression.down(2), refsR, toDiffRefsR, true);
                return usedVarsInDiffExp(expression.down(3), refsR, toDiffRefsR, true);
            case ILLang.op_iterativeVariableRef:
                return usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, keepTop);
            case ILLang.op_unary:
            case ILLang.op_cast:
            case ILLang.op_multiCast:
                return usedVarsInDiffExp(expression.down(2), refsR, toDiffRefsR, true);
            case ILLang.op_loop:
                return usedVarsInDiffExp(expression.down(4), refsR, toDiffRefsR, true);
            case ILLang.op_do:
                refsR = usedVarsInDiffExp(expression.down(2), refsR, toDiffRefsR, true);
                refsR = usedVarsInDiffExp(expression.down(3), refsR, toDiffRefsR, true);
                return usedVarsInDiffExp(expression.down(4), refsR, toDiffRefsR, true);
            // Last, trivial cases:
            case ILLang.op_dimColon:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_mod:
            case ILLang.op_power:
            case ILLang.op_div:
            case ILLang.op_neq:
            case ILLang.op_eq:
            case ILLang.op_and:
            case ILLang.op_or:
            case ILLang.op_xor:
            case ILLang.op_bitAnd:
            case ILLang.op_bitOr:
            case ILLang.op_bitXor:
            case ILLang.op_ge:
            case ILLang.op_le:
            case ILLang.op_gt:
            case ILLang.op_lt:
            case ILLang.op_concat:
            case ILLang.op_concatIdent:
            case ILLang.op_nameEq:
            case ILLang.op_complexConstructor:
            case ILLang.op_return:
            case ILLang.op_leftShift:
            case ILLang.op_rightShift:
                refsR = usedVarsInDiffExp(expression.down(1), refsR, toDiffRefsR, true);
                return usedVarsInDiffExp(expression.down(2), refsR, toDiffRefsR, true);
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
            case ILLang.op_ioCall:
            case ILLang.op_stringCst:
            case ILLang.op_letter:
            case ILLang.op_label:
            case ILLang.op_stop:
            case ILLang.op_break:
            case ILLang.op_compGoto:
            case ILLang.op_assignLabelVar:
            case ILLang.op_gotoLabelVar:
            case ILLang.op_none:
            case ILLang.op_star:
            case ILLang.op_continue:
            case ILLang.op_varDimDeclaration:
            case ILLang.op_typeDeclaration:
            case ILLang.op_constDeclaration:
            case ILLang.op_external:
            case ILLang.op_useDecl:
            case ILLang.op_interfaceDecl:
            case ILLang.op_intrinsic:
            case ILLang.op_accessDecl:
            case ILLang.op_common:
            case ILLang.op_nameList:
            case ILLang.op_equivalence:
            case ILLang.op_include:
            case ILLang.op_save:
            case ILLang.op_data:
            case ILLang.op_arrayType:
            case ILLang.op_pointerType:
            case ILLang.op_enumType:
            case ILLang.op_unionType:
            case ILLang.op_recordType:
            case ILLang.op_functionType:
            case ILLang.op_modifiedType:
            case ILLang.op_void:
            case ILLang.op_integer:
            case ILLang.op_float:
            case ILLang.op_complex:
            case ILLang.op_boolean:
            case ILLang.op_character:
                return refsR;
            default:
                TapEnv.toolWarning(-1, "(Variables used in diff expression) Unexpected operator: " + expression.opName());
                return refsR;
        }
    }

    public static boolean equalValues(Tree tree1, Tree tree2,
                                      Instruction instr1, Instruction instr2) {
        switch (tree1.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return eqOrDisjointRef(tree1, tree2, instr1, instr2) == 1;
            default:
                boolean equals = false;
                if (tree1.operator() == tree2.operator()) {
                    if (!tree1.isAtom()) {
                        Tree[] subTrees1 = tree1.children();
                        Tree[] subTrees2 = tree2.children();
                        equals = tree1.length() == tree2.length();
                        if (equals) {
                            for (int i = subTrees1.length - 1; i >= 0; i--) {
                                if (!equalValues(subTrees1[i], subTrees2[i], instr1, instr2)) {
                                    equals = false;
                                }
                            }
                        }
                    } else {
                        equals = tree1.equalsTree(tree2);
                    }
                }
                return equals;
        }
    }

    /**
     * @return DIFFERENT = -1 if the two trees certainly always refer to different memory cells,
     * SAME =  1 if the two trees certainly always refer to the same memory cell,
     * DOUBT = 0 otherwise, i.e. in case of doubt.
     */
    public static int eqOrDisjointRef(Tree tree1, Tree tree2,
                                      Instruction instr1, Instruction instr2) {
        switch (tree1.opCode()) {
            /*TODO: refine using zone, baseName, refOffset...*/
            case ILLang.op_ident:
                if (tree2.opCode() == ILLang.op_ident
                        && getIdentString(tree1).equals(getIdentString(tree2))) {
                    if (NewSymbolHolder.getNewSymbolHolder(tree1)
                            == NewSymbolHolder.getNewSymbolHolder(tree2)) {
                        return SAME;
                    } else {
                        return DIFFERENT;
                    }
                } else if (tree2.opCode() == ILLang.op_arrayAccess) {
                    return eqOrDisjointRefIdentArray(tree1, tree2, instr1, instr2);
                } else {
                    return DOUBT;
                }
            case ILLang.op_arrayAccess: {
                if (tree2.opCode() == ILLang.op_ident) {
                    return eqOrDisjointRefIdentArray(tree2, tree1, instr1, instr2);
                } else if (tree2.opCode() != ILLang.op_arrayAccess
                        || tree1.down(2).children().length != tree2.down(2).children().length) {
                    return DOUBT;
                }
                int eqOrDisjointBase =
                        eqOrDisjointRef(tree1.down(1), tree2.down(1), instr1, instr2);
                if (eqOrDisjointBase == SAME) {
                    Tree[] indices1 = tree1.down(2).children();
                    Tree[] indices2 = tree2.down(2).children();
                    int totalIndexComparison = SAME;
                    int i = indices1.length - 1;
                    while (i >= 0 && totalIndexComparison != DIFFERENT) {
                        int eqOrDisjointIndex = eqOrDisjointExprs(indices1[i], indices2[i]);
                        if (eqOrDisjointIndex == DIFFERENT) {
                            totalIndexComparison = DIFFERENT;
                        } else if (eqOrDisjointIndex == DOUBT) {
                            totalIndexComparison = DOUBT;
                        }
                        i--;
                    }
                    return totalIndexComparison;
                } else if (eqOrDisjointBase == DIFFERENT) {
                    return DIFFERENT;
                } else {
                    return DOUBT;
                }
            }
            case ILLang.op_fieldAccess: {
                if (tree2.opCode() != ILLang.op_fieldAccess) {
                    return DOUBT;
                }
                int eqOrDisjointBase = eqOrDisjointRef(tree1.down(1),
                        tree2.down(1), instr1, instr2);
                if (eqOrDisjointBase == SAME) {
                    if (getIdentString(tree1.down(2)).equals(
                            getIdentString(tree2.down(2)))) {
                        return SAME;
                    } else {
                        return DIFFERENT;
                    }
                } else if (eqOrDisjointBase == DIFFERENT) {
                    return DIFFERENT;
                } else {
                    /*Attention: this is maybe a too strong approximation:
                     * we assume that if  the field name is different, then the memory cell is different! */
                    if (getIdentString(tree1.down(2)).equals(
                            getIdentString(tree2.down(2)))) {
                        return DOUBT;
                    } else {
                        return DIFFERENT;
                    }
                }
            }
            case ILLang.op_pointerAccess:
                if (tree2.opCode() != ILLang.op_pointerAccess) {
                    return DOUBT;
                } else if (instr1 == instr2 &&
                        eqOrDisjointRef(tree1.down(1), tree2.down(1), instr1, instr2) == SAME) {
                    return eqOrDisjointExprs(tree1.down(2), tree2.down(2));
                } else {
                    return DOUBT;
                }
            default:
                return DOUBT;
        }
    }

    private static int eqOrDisjointExprs(Tree expr1, Tree expr2) {
        //For pointers in Fortran: the offset expressions are op_none => they are equal!
        if (expr1 != null && expr1.opCode() == ILLang.op_none &&
                expr2 != null && expr2.opCode() == ILLang.op_none) {
            return SAME;
        }

        Tree diffExpr = subTree(expr1, expr2);
        //TODO: buildSmartAddSub should use instr1 and instr2, because of pointers !
        if (evalsToZero(diffExpr)) {
            return SAME;
        } else if (diffExpr.opCode() == ILLang.op_intCst) {
            return DIFFERENT;
        } else {
            return DOUBT;
        }
    }

    // eqOrDisjointRef of a and a(???)
    private static int eqOrDisjointRefIdentArray(Tree identtree1, Tree arraytree2,
                                                 Instruction instr1, Instruction instr2) {
        int result = DOUBT;
        if (instr1 != null && instr2 != null) {
            result = eqOrDisjointRef(identtree1, arraytree2.down(1),
                    instr1, instr2);
            if (result == SAME) {
                if (!instr1.block.symbolTable.typeOf(identtree1).equalsCompilDep(instr2.block.symbolTable.typeOf(arraytree2))) {
                    result = DOUBT;
                }
            }
        }
        return result;
    }

    public static TapList<Tree> addTreeInList(Tree tree, TapList<Tree> list) {
        if (tree == null || TapList.containsTree(list, tree)) {
            return list;
        } else {
            return new TapList<>(tree, list);
        }
    }

    public static TapList<Tree> addListTreeInList(TapList<Tree> listTree, TapList<Tree> list) {
        while (listTree != null) {
            list = addTreeInList(listTree.head, list);
            listTree = listTree.tail;
        }
        return list;
    }

    /**
     * @return a dimColon given its size and index of first element.
     */
    public static Tree buildDimColon(int minIndex, Tree dimSize) {
        if (minIndex != 1) {
            if (minIndex <= 0) {
                dimSize = subTree(dimSize, build(ILLang.op_intCst, 1 - minIndex));
            } else {
                dimSize = addTree(dimSize, build(ILLang.op_intCst, minIndex - 1));
            }
        }
        return build(ILLang.op_dimColon,
                build(ILLang.op_intCst, minIndex), dimSize);
    }

    /**
     * @param tree is a list Tree
     * @return the reversed list Tree
     */
    public static Tree reverseArrayComponentOrder(Tree tree) {
        Tree result = tree.operator().tree();
        for (int i = 0; i < tree.length(); i++) {
            result.addChild(copy(tree.down(i + 1)), 1);
        }
        return result;
    }

    private static void changeOrAdoptSon(Tree father, Tree son, int rank, int sonsNb) {
        if (son == null) {
            son = ILLangOps.ops(ILLang.op_none).tree();
        }
        if (rank <= sonsNb) {
            father.setChild(son, rank);
        } else {
            father.addChild(son, rank);
        }
    }

    public static TapList<Tree> arrayTripletsInside(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_arrayAccess: {
                TapList<Tree> result = null;
                Tree[] sons = tree.down(2).children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    result = TapList.append(arrayTripletsInside(sons[i]), result);
                }
                return result;
            }
            case ILLang.op_arrayTriplet:
                return new TapList<>(tree, null);
            default:
                return null;
        }
    }

    /** When the given expression "expr" is of array type and this type comes from
     * that of an arrayTriplet inside "expr", returns this arrayTriplet access (normalized?)
     * Otherwise returns null */
    public static Tree findArrayTriplet(Tree expr) {
	switch (expr.opCode()) {
	  case ILLang.op_arrayAccess:
	    return (oneIsArrayTriplet(expr.down(2)) ? tryNormalize(expr) : null) ;
	  case ILLang.op_minus:
	  case ILLang.op_fieldAccess:
	    return findArrayTriplet(expr.down(1)) ;
	  case ILLang.op_unary:
          case ILLang.op_cast:
	    return findArrayTriplet(expr.down(2)) ;
	  case ILLang.op_add:
	  case ILLang.op_sub:
	  case ILLang.op_mul:
	  case ILLang.op_div:
	  case ILLang.op_mod:
	  case ILLang.op_power:
	  case ILLang.op_neq:
          case ILLang.op_eq:
          case ILLang.op_and:
          case ILLang.op_or:
          case ILLang.op_xor:
          case ILLang.op_bitAnd:
          case ILLang.op_bitOr:
          case ILLang.op_bitXor:
          case ILLang.op_ge:
          case ILLang.op_le:
          case ILLang.op_gt:
          case ILLang.op_lt: {
	    Tree triplet = findArrayTriplet(expr.down(1)) ;
	    if (triplet==null) triplet = findArrayTriplet(expr.down(2)) ;
	    return triplet ;
	  }
	  case ILLang.op_call: {
	    // TODO: here, we should search recursively on simple intrinsics e.g. sin, log, etc.
	    return null ;
	  }
	  default:
	    return null ;
	}
    }

    public static boolean oneIsArrayTriplet(Tree indices) {
        Tree[] indexArray = indices.children();
        boolean foundTriplet = false;
        for (int i = indexArray.length - 1; i >= 0 && !foundTriplet; --i) {
            foundTriplet = indexArray[i].opCode() == ILLang.op_arrayTriplet;
        }
        return foundTriplet;
    }

    private static Tree tryNormalize(Tree tree) {
        assert tree != null;
        Tree result = copy(tree);
        Tree sons = result.down(2);
        for (int i = 1; i <= sons.length(); i++) {
            if (sons.down(i).opCode() == ILLang.op_arrayTriplet
                    && sons.down(i).down(3).opCode() == ILLang.op_none) {
                Tree lower = sons.down(i).down(1);
                if (!(lower.opCode() == ILLang.op_intCst
                        && lower.intValue() == 1)
                        && lower.opCode() != ILLang.op_none) {
                    sons.down(i).setChild(addTree(
                            subTree(sons.down(i).down(2), sons.down(i).down(1)),
                            build(ILLang.op_intCst, 1)), 2);
                    sons.down(i).setChild(
                            build(ILLang.op_intCst, 1), 1);
                }
            }
        }
        return result;
    }

    public static Tree keepArrayTriplet(Tree tree) {
        assert tree != null;
        Tree result = copy(tree);
        for (int i = tree.length(); i > 0; i--) {
            if (tree.down(i).opCode() != ILLang.op_arrayTriplet) {
                result.removeChild(i);
            }
        }
        return result;
    }

    /**
     * @param tree is a varDeclaration Tree.
     */
    public static Tree findDimColons(Tree tree) {
        Tree result = null;
        if (tree.opCode() == ILLang.op_varDeclaration) {
            tree = tree.down(2);
        }
        int op = tree.opCode();
        switch (op) {
            case ILLang.op_modifiedType:
                tree = tree.down(1);
                boolean found = false;
                int i = 1;
                while (!found && i <= tree.down(1).length()) {
                    found = tree.down(1).down(i).opCode() == ILLang.op_dimColons;
                    if (found) {
                        result = tree.down(1).down(i);
                    }
                    i = i + 1;
                }
                break;
            case ILLang.op_arrayType:
                result = tree.down(1).isAtom() ? null : tree.down(1).down(2);
                break;
            case ILLang.op_arrayDeclarator:
                result = tree.down(2);
                break;
            default:
                break;
        }
        return result;
    }

    public static Tree buildArrayDeclarator(int language, Tree declTree, Tree typeTree) {
        assert declTree != null;
        Tree result = declTree;
        if (declTree.opCode() == ILLang.op_arrayDeclarator) {
            if (language == TapEnv.C) {
                result = build(ILLang.op_arrayDeclarator, declTree,
                        typeTree);
            } else {
                Tree dimColons = copy(typeTree);
                assert dimColons != null;
                dimColons.addChildren(copiedSonsList(declTree.down(2)), -1);
                declTree.setChild(dimColons, 2);
            }
        } else {
            result = build(ILLang.op_arrayDeclarator, declTree,
                    typeTree);
        }
        return result;
    }

    /**
     * @param tree is a declarators Tree or a varDimDeclaration.
     */
    public static int findRankOfDeclarator(Tree tree, String symbol) {
        int result = 1;
        boolean found = false;
        int op;
        while (result < tree.length() && !found) {
            op = tree.down(result).opCode();
            switch (op) {
                case ILLang.op_ident:
                    found = tree.down(result).stringValue().equals(symbol);
                    break;
                case ILLang.op_arrayDeclarator:
                    found = tree.down(result).down(1).stringValue().equals(symbol);
                    break;
                default:
                    break;
            }
            if (!found) {
                result = result + 1;
            }
        }
        return result;
    }

    // si callTree = p(r,__,ec=r-e,x=r*e) et resultTree = p(r,_,arg1,arg2)
    //  resultTree devient  p(r,__,ec=arg1,x=arg2)
    public static void reBuildNameEqArgs(Tree callTree, Tree resultTree) {
        int nbArgsResultTree = getArguments(resultTree).children().length;
        int nbArgsCallTree = getArguments(callTree).children().length;
        int nbArgs = Math.max(nbArgsResultTree, nbArgsCallTree);
        Tree callArg;
        Tree resultArg;
        Tree tmpTree;
        Tree callArguments = getArguments(callTree);
        Tree resultArguments = getArguments(resultTree);
        for (int j = 0; j < nbArgs; j++) {
            resultArg = resultArguments.down(j + 1);
            callArg = callArguments.down(j + 1);
            if (resultArg != null && callArg != null) {
                if (resultArg.opCode() == ILLang.op_nameEq
                        && callArg.opCode() != ILLang.op_none
                        && callArg.opCode() != ILLang.op_nameEq) {
                    tmpTree = copy(resultArg);
                    tmpTree.setChild(copy(callArg), 2);
                    resultArguments.setChild(tmpTree, j + 1);
                } else if (resultArg.opCode() != ILLang.op_none
                        && callArg.opCode() != ILLang.op_none) {
                    resultArguments.setChild(copy(callArg), j + 1);
                }
            }
        }
    }

    /**
     * {@code *(*(*p.f1).f2).f3.f4  ==>  *_.f3.f4}
     */
    public static Tree pathFromPointerAccess(Tree expr) {
        switch (expr.opCode()) {
            case ILLang.op_pointerAccess:
                return build(ILLang.op_pointerAccess, build(ILLang.op_none), build(ILLang.op_none));
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
                return build(expr.opCode(),
                        pathFromPointerAccess(expr.down(1)),
                        copy(expr.down(2)));
            default:
                return copy(expr);
        }
    }

    /**
     * @return the reversed list of all nested levels in "expr".
     * For example {@code A[2,1:n]%x[:]} produces {@code {A, A[2,1:n], A[2,1:n]%x, A[2,1:n]%x[:]}}.
     */
    public static TapList<Tree> reversedExprAccess(Tree expr) {
        TapList<Tree> cumulTrees = null;
        int opcode;
        while (expr != null) {
            cumulTrees = new TapList<>(expr, cumulTrees);
            opcode = expr.opCode();
            if (opcode == ILLang.op_fieldAccess
                    || opcode == ILLang.op_arrayAccess
                    || opcode == ILLang.op_pointerAccess) {
                expr = expr.down(1);
            } else {
                expr = null;
            }
        }
        return cumulTrees;
    }

    public static Tree getAllocatedRef(Tree accessTree) {
        if (accessTree == null) {
            return null;
        }
        switch (accessTree.opCode()) {
            case ILLang.op_allocate:
                return accessTree.down(1);
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return getAllocatedRef(accessTree.down(1));
            default:
                return null;
        }
    }

    public static TapList<Tree> reversedExprAccessFromPointerAccess(Tree expr, boolean peelAllocate) {
        TapList<Tree> cumulTrees = null;
        int opcode;
        while (expr != null) {
            // Peel off possible "op_allocate":
            if (peelAllocate) {
                while (expr != null && expr.opCode() == ILLang.op_allocate) {
                    expr = expr.down(1);
                }
            }
            cumulTrees = new TapList<>(expr, cumulTrees);
            assert expr != null;
            opcode = expr.opCode();
            if (opcode == ILLang.op_fieldAccess
                    || opcode == ILLang.op_arrayAccess) {
                expr = expr.down(1);
            } else {
                expr = null;
            }
        }
        return cumulTrees;
    }

    /**
     * @param listTree      is a list-arity Tree of atomic subtrees.
     * @param caseSensitive when true, the string equality is case sensitive.
     */
    public static boolean contains(Tree listTree, String value, boolean caseSensitive) {
        boolean found = false;
        Tree[] children = listTree.children();
        for (int i = children.length - 1; i >= 0 && !found; --i) {
            if (children[i].isStringAtom()) {
                found = caseSensitive
                        ? value.equals(children[i].stringValue())
                        : value.equalsIgnoreCase(children[i].stringValue());
            }
        }
        return found;
    }

    /**
     * @param trees is a tree-list of functionDeclarator subtrees.
     */
    public static boolean containsFunctionDeclarator(Tree trees, String value) {
        boolean result = false;
        int i = 1;
        while (!result && i <= trees.length()) {
            result = trees.down(i).opCode() == ILLang.op_functionDeclarator
                    && trees.down(i).down(1).isAtom()
                    && trees.down(i).down(1).stringValue().equals(value);
            i = i + 1;
        }
        return result;
    }

    /**
     * If tree is a varDeclaration with too many declared variables,
     * splits it into a list of varDeclaration, each of acceptable length. Otherwise returns null
     */
    public static TapList<Tree> splitLongDeclaration(Tree tree) {
        int nbDecls = tree.down(3).length();
        if (nbDecls <= MAX_LIST_LENGTH || tree.down(2).opCode() == ILLang.op_none) {
            return null;
        } else {
            TapList<Tree> hdResult = new TapList<>(null, null);
            TapList<Tree> tlResult = hdResult;
            Tree[] origDecls = tree.down(3).children();
            int index = 0;
            Tree[] copiedDecls;
            while (index < nbDecls) {
                int stopAt = Math.min(nbDecls, index + MAX_LIST_LENGTH);
                copiedDecls = new Tree[stopAt - index];
                for (int i = 0; i < copiedDecls.length; ++i) {
                    copiedDecls[i] = copy(origDecls[index + i]);
                }
                tlResult = tlResult.placdl(build(ILLang.op_varDeclaration,
                                             copy(tree.down(1)),
                                             copy(tree.down(2)),
                                             build(ILLang.op_declarators, copiedDecls)));
                index = stopAt;
            }
            return hdResult.tail;
        }
    }

    public static String typeName(Tree tree) {
        String result = "_";
        if (tree.opCode() == ILLang.op_recordType
                || tree.opCode() == ILLang.op_enumType
                || tree.opCode() == ILLang.op_unionType) {
            result = getIdentString(tree.down(1));
        } else {
            if (tree.opCode() == ILLang.op_typeDeclaration
                    && tree.down(1).opCode() == ILLang.op_none) {
                result = typeName(tree.down(2));
            }
        }
        return result;
    }

    public static String buildTypeName(Tree tree) {
        String result = null;
        if (tree.opCode() == ILLang.op_recordType
                || tree.opCode() == ILLang.op_enumType
                || tree.opCode() == ILLang.op_unionType) {
            String identString = getIdentString(tree.down(1));
            if (identString != null) {
                String opName = "";
                if (tree.opCode() == ILLang.op_recordType) {
                    opName = "struct ";
                } else if (tree.opCode() == ILLang.op_unionType) {
                    opName = "union ";
                } else if (tree.opCode() == ILLang.op_enumType) {
                    opName = "enum ";
                }
                result = opName + identString;
            } else {
                result = null;
            }
        } else {
            if (tree.opCode() == ILLang.op_typeDeclaration
                    && tree.down(1).opCode() == ILLang.op_none) {
                result = buildTypeName(tree.down(2));
            }
        }
        return result;
    }

    /**
     * @return the line number or 0 if there is no line number accessible.
     */
    public static int getLineNumber(Tree tree) {
        Object annotValue = null;
        if (tree.opCode() == ILLang.op_call) {
            annotValue = getCalledName(tree).getAnnotation("line");
        } else if (tree.opCode() == ILLang.op_assign) {
            annotValue = tree.down(1).getAnnotation("line");
        }
        return annotValue == null ? 0 : intConstantValue((Tree) annotValue);
    }

    public static boolean inInclude(Tree tree, String beginOrEnd) {
        boolean result = false;
        String line;
        if (tree.opCode() == ILLang.op_include) {
            line = tree.stringValue();
            if (line.equals(beginOrEnd)) {
                result = true;
            } else if (beginOrEnd.equals("tapenade begin #include")
                    && !line.equals("tapenade end #include")) {
                result = true;
            }
        } else {
            line = tree.down(1).stringValue();
            result = line.startsWith(beginOrEnd);
        }
        return result;
    }

    public static boolean stopInclude(Tree tree, String stopValue) {
        Tree keyTree = (tree.opCode()==ILLang.op_include ? tree : tree.down(1)) ;
        return keyTree.stringValue().startsWith(stopValue) ;
    }

    public static Tree peelInOutConstValueModifier(Tree type) {
        if (type.opCode() != ILLang.op_modifiedType) {
            return type;
        }
        int toRemove = -1;
        Tree[] modifiers = type.down(1).children();
        for (int i = modifiers.length - 1; i >= 0; i--) {
            if (modifiers[i].opCode() == ILLang.op_ident) {
                String value = modifiers[i].stringValue();
                if (value.equals("in") || value.equals("out")// || value.equals("inout")
                        || value.equals("value")
                        || value.equals("const")
                        || value.equals("constant")) {
                    toRemove = i;
                }
            }
        }
        if (toRemove != -1) {
            type.down(1).removeChild(toRemove + 1);
        }
        if (type.down(1).length() == 0) {
            return type.cutChild(2);
        } else {
            return type;
        }
    }

    public static Tree peelConstModifier(Tree type) {
        if (type.opCode() != ILLang.op_modifiedType) {
            return type;
        }
        int toRemove = -1;
        Tree[] modifiers = type.down(1).children();
        for (int i = modifiers.length - 1; i >= 0; i--) {
            if (modifiers[i].opCode() == ILLang.op_ident) {
                String value = modifiers[i].stringValue();
                if (value.equals("const")
                        || value.equals("constant")) {
                    toRemove = i;
                }
            }
        }
        if (toRemove != -1) {
            type.down(1).removeChild(toRemove + 1);
        }
        if (type.down(1).length() == 0) {
            return type.cutChild(2);
        } else {
            return type;
        }
    }

    public static void peelModifier(Tree tree, String modifier) {
        // We assume tree is a op_varDeclaration!
        Tree typeSpec = tree.down(2); // maybe some day the modifiers will be in tree.down(1)
        if (typeSpec.opCode() == ILLang.op_modifiedType) {
            peelModifierRec(typeSpec, modifier);
        }
    }

    private static void peelModifierRec(Tree typeSpec, String modifier) {
        int toRemove = -1;
        Tree[] modifiers = typeSpec.down(1).children();
        for (int i = modifiers.length - 1; i >= 0; i--) {
            if (modifiers[i].opCode() == ILLang.op_ident) {
                String value = modifiers[i].stringValue();
                if (value.equals(modifier)) {
                    toRemove = i;
                }
            }
        }
        if (toRemove != -1) {
            typeSpec.down(1).removeChild(toRemove + 1);
        } else if (typeSpec.down(2).opCode() == ILLang.op_modifiedType) {
            peelModifierRec(typeSpec.down(2), modifier);
        }
    }

    public static void checkFunctionName(
            FunctionDecl functionDecl, SymbolTable symbolTable) {
        String functionName = functionDecl.symbol;
        Instruction instr =
                symbolTable.declarationsBlock.getOperatorDeclarationInstruction(functionDecl, ILLang.op_varDeclaration, null);
        if (instr != null && instr.tree.opCode() == ILLang.op_varDeclaration) {
            Tree declarators = instr.tree.down(3);
            boolean found = false;
            int i = 1;
            Tree decl;
            while (!found && i <= declarators.length()) {
                decl = declarators.down(i);
                if (decl.opCode() == ILLang.op_ident) {
                    found = decl.stringValue().equals(functionName);
                }
                if (found) {
                    decl.setAnnotation("isFunctionName", Boolean.TRUE);
                }
                i = i + 1;
            }
        }
    }

    public static boolean equalsInclude(String include, Tree tree) {
        boolean result = false;
        if (tree != null && tree.opCode() == ILLang.op_include) {
            result = include.equals(tree.stringValue());
        }
        return result;
    }

    public static Tree variableDeclaratorToVariableRef(Tree varDecl) {
        Tree result;
        switch (varDecl.opCode()) {
            case ILLang.op_arrayDeclarator:
                result = build(ILLang.op_arrayAccess,
                        variableDeclaratorToVariableRef(varDecl.down(1)),
                        build(ILLang.op_expressions));
                break;
            case ILLang.op_pointerDeclarator:
                result = build(ILLang.op_pointerAccess,
                        variableDeclaratorToVariableRef(varDecl.down(1)));
                break;
            default:
                result = copy(varDecl);
                break;
        }
        return result;
    }

    /**
     * @return in general a new Tree array[from:to], *except* when array is itself an array section, in which case
     * returns a copy of array, modified/combined with from:to at the right place.
     */
    public static Tree buildArrayTripletAccess(Tree array, Tree from, Tree to, int startIndex) {
        // Detect the case where array is enclosed in an op_minus:
        boolean inMinus = false;
        if (array.opCode() == ILLang.op_minus) {
            inMinus = true;
            array = array.cutChild(1);
        }
        ToInt toRank = new ToInt(-1);
        Tree foundTriplet = getToArrayTriplet(array, toRank);
        if (foundTriplet == null) {
            array = build(ILLang.op_arrayAccess,
                    array,
                    build(ILLang.op_expressions,
                            to == null ? from : build(ILLang.op_arrayTriplet, from, to)));
        } else {
            array = copy(array);
            Tree parent = getToArrayTriplet(array, toRank);
            assert parent != null;
            Tree oldSection = parent.down(toRank.get() + 1);
            Tree newSection = combineSections(oldSection, from, to, startIndex);
            parent.setChild(newSection, toRank.get() + 1);
        }
        if (inMinus) {
            array = build(ILLang.op_minus, array);
        }
        return array;
    }

    /**
     * @return (access to) the *unique* arrayTriplet found in "ref". Otherwise returns null.
     */
    private static Tree getToArrayTriplet(Tree ref, ToInt toRank) {
        if (ref == null) {
            return null;
        }
        switch (ref.opCode()) {
            case ILLang.op_arrayAccess: {
                Tree[] indices = ref.down(2).children();
                Tree found = null;
                for (int i = indices.length - 1; i >= 0; --i) {
                    if (indices[i] != null && indices[i].opCode() == ILLang.op_arrayTriplet) {
                        if (found == null) {
                            found = ref.down(2);
                            toRank.set(i);
                        } else { //return null if more than one arrayTriplet:
                            found = null;
                            i = -1;
                        }
                    }
                }
                return found;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return getToArrayTriplet(ref.down(1), toRank);
            case ILLang.op_ident:
            default:
                return null;
        }
    }

    /**
     * Combines section==[f1:t1:s1] with [from:to], yielding more or less [f1+from*s1:f1+to*s1:s1]
     * The output is different depending on startIndex==0 or 1.
     */
    private static Tree combineSections(Tree section, Tree from, Tree to, int startIndex) {
        Tree stride1 = section.down(3);
        if (startIndex == 1) {
            from = subTree(from, build(ILLang.op_intCst, 1));
        }
        if (!isNullOrNone(stride1)) {
            from = mulTree(from, copy(stride1));
        }
        if (to != null) {
            if (startIndex == 1) {
                to = subTree(to, build(ILLang.op_intCst, 1));
            }
            if (!isNullOrNone(stride1)) {
                to = mulTree(to, copy(stride1));
            }
        }
        Tree from1 = section.down(1);
        if (isNullOrNone(from1)) {
            from1 = build(ILLang.op_intCst, startIndex);
        }
        Tree result = addTree(from1, from);
        if (to != null) {
            result = build(ILLang.op_arrayTriplet,
                    result,
                    addTree(copy(from1), to),
                    copy(stride1));
        }
        return result;
    }

    public static void removeTreeDeclOf(String symbol, Tree varDeclTree) {
        int foundRank = -1;
        Tree[] varDecls = varDeclTree.down(3).children();
        for (int i = varDecls.length - 1; i >= 0 && foundRank < 0; --i) {
            if (symbol.equals(baseName(varDecls[i]))) {
                foundRank = i + 1;
            }
        }
        if (foundRank > 0) {
            varDeclTree.down(3).removeChild(foundRank);
        }
    }

    /**
     * @return true if this for loop may be transformed into a do loop
     */
    public static boolean mayBeTransformed2Do(Tree tree) {
        boolean result = false;
        String indexName = null;
        Tree strideTree = null;
        if (tree.down(1).opCode() == ILLang.op_assign) {
            result = tree.down(1).down(1).opCode() == ILLang.op_ident;
            if (result) {
                indexName = tree.down(1).down(1).stringValue();
            }
        } else if (tree.down(1).opCode() == ILLang.op_varDeclaration) {
            result = tree.down(1).down(3).down(1).opCode() == ILLang.op_assign
                    && tree.down(1).down(3).down(1).down(1).opCode() == ILLang.op_ident;
            if (result) {
                indexName = tree.down(1).down(3).down(1).down(1).stringValue();
            }
        }
        if (result) {
            int unaryStride = isUnaryPrefixOrPostfixIncrOrDecr(tree.down(3));
            if (unaryStride != 0
                    && isIdent(tree.down(3).down(2), indexName, true)) {
                strideTree = build(ILLang.op_intCst, unaryStride == -1 ? -1 : 1);
            } else if (tree.down(3).opCode() == ILLang.op_assign
                    && isIncr(tree.down(3), indexName)) {
                strideTree = getIncr(tree.down(3), indexName);
            } else {
                result = false;
            }
        }
        result = result
                && (tree.down(2).opCode() == ILLang.op_lt
                || tree.down(2).opCode() == ILLang.op_le
                || tree.down(2).opCode() == ILLang.op_gt
                || tree.down(2).opCode() == ILLang.op_ge);
        if (result) {
            Tree leftOfTest = tree.down(2).down(1);
            Tree rightOfTest = tree.down(2).down(2);
            if (leftOfTest.opCode() == ILLang.op_mul &&
                    rightOfTest.opCode() == ILLang.op_mul &&
                    leftOfTest.down(2).equalsTree(strideTree) &&
                    rightOfTest.down(2).equalsTree(strideTree)) {
                leftOfTest = leftOfTest.down(1);
                rightOfTest = rightOfTest.down(1);
            }
            result = isIdent(leftOfTest, indexName, true)
                    || isIdent(rightOfTest, indexName, true);
        }
        if (tree.down(2).opCode() == ILLang.op_neq) {
            result = isUnaryPrefixOrPostfixIncrOrDecr(tree.down(3)) != 0;
        }
        return result;
    }

    private static int isUnaryPrefixOrPostfixIncrOrDecr(Tree tree) {
        if (tree.opCode() == ILLang.op_unary) {
            if (tree.down(1).stringValue().equals("++postfix")
                    || tree.down(1).stringValue().equals("++prefix")) {
                return 1;
            } else if (tree.down(1).stringValue().equals("--postfix")
                    || tree.down(1).stringValue().equals("--prefix")) {
                return -1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    private static boolean isIncr(Tree tree, String indexName) {
        return isIdent(tree.down(1), indexName, true)
                && (tree.down(2).opCode() == ILLang.op_add
                || tree.down(2).opCode() == ILLang.op_sub)
                && (isIdent(tree.down(2).down(1), indexName, true)
                || isIdent(tree.down(2).down(2), indexName, true));
    }

    private static Tree getIncr(Tree tree, String indexName) {
        Tree result;
        if (tree.down(2).down(1).opCode() == ILLang.op_ident
                && tree.down(2).down(1).stringValue().equals(indexName)) {
            result = tree.down(2).down(2);
        } else {
            result = tree.down(2).down(1);
        }
        return result;
    }

    /**
     * Transforms a for-loop into a do-loop.
     *
     * @return a do-loop.
     */
    public static Tree transformFor2Do(Tree tree) {
        Tree result = build(ILLang.op_do);
        boolean incr = true;
        if (tree.down(1).opCode() == ILLang.op_assign) {
            result.setChild(copy(tree.down(1).down(1)), 1);
            result.setChild(copy(tree.down(1).down(2)), 2);
        } else if (tree.down(1).opCode() == ILLang.op_varDeclaration) {
            Tree initDecl = tree.down(1).down(3).down(1);
            result.setChild(copy(initDecl.down(1)), 1);
            result.setChild(copy(initDecl.down(2)), 2);
        }
        String indexName = result.down(1).stringValue();
        Tree strideTree = null;
        if (tree.down(3).opCode() == ILLang.op_unary) {
            if (tree.down(3).down(1).stringValue().equals("++postfix")
                    || tree.down(3).down(1).stringValue().equals("++prefix")) {
                strideTree = build(ILLang.op_intCst, 1);
                result.setChild(build(ILLang.op_none), 4);
            } else if (tree.down(3).down(1).stringValue().equals("--postfix")
                    || tree.down(3).down(1).stringValue().equals("--prefix")) {
                strideTree = build(ILLang.op_intCst, -1);
                result.setChild(strideTree, 4);
                incr = false;
            }
        } else if (tree.down(3).opCode() == ILLang.op_assign) {
            strideTree = copy(getIncr(tree.down(3), indexName));
            result.setChild(strideTree, 4);
        }
        Tree leftOfTest = tree.down(2).down(1);
        Tree rightOfTest = tree.down(2).down(2);
        if (tree.down(2).opCode() == ILLang.op_lt
                || tree.down(2).opCode() == ILLang.op_gt) {
            if (leftOfTest.opCode() == ILLang.op_mul &&
                    rightOfTest.opCode() == ILLang.op_mul &&
                    strideTree != null &&
                    leftOfTest.down(2).equalsTree(strideTree) &&
                    rightOfTest.down(2).equalsTree(strideTree)) {
                leftOfTest = leftOfTest.down(1);
                rightOfTest = rightOfTest.down(1);
            }
            if (leftOfTest.opCode() == ILLang.op_ident
                    && leftOfTest.stringValue().equals(indexName)) {
                if (incr) {
                    result.setChild(build(ILLang.op_sub,
                            copy(rightOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                } else {
                    result.setChild(build(ILLang.op_add,
                            copy(rightOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                }
            } else {
                if (incr) {
                    result.setChild(build(ILLang.op_sub,
                            copy(leftOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                } else {
                    result.setChild(build(ILLang.op_add,
                            copy(leftOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                }
            }
        } else if (tree.down(2).opCode() == ILLang.op_le
                || tree.down(2).opCode() == ILLang.op_ge) {
            if (leftOfTest.opCode() == ILLang.op_mul &&
                    rightOfTest.opCode() == ILLang.op_mul &&
                    strideTree != null &&
                    leftOfTest.down(2).equalsTree(strideTree) &&
                    rightOfTest.down(2).equalsTree(strideTree)) {
                leftOfTest = leftOfTest.down(1);
                rightOfTest = rightOfTest.down(1);
            }
            if (leftOfTest.opCode() == ILLang.op_ident
                    && leftOfTest.stringValue().equals(indexName)) {
                result.setChild(copy(rightOfTest), 3);
            } else {
                result.setChild(copy(leftOfTest), 3);
            }
        } else if (tree.down(2).opCode() == ILLang.op_neq) {
            if (leftOfTest.opCode() == ILLang.op_ident
                    && leftOfTest.stringValue().equals(indexName)) {
                if (incr) {
                    result.setChild(build(ILLang.op_sub,
                            copy(rightOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                } else {
                    result.setChild(build(ILLang.op_add,
                            copy(rightOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                }
            } else {
                if (incr) {
                    result.setChild(build(ILLang.op_sub,
                            copy(leftOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                } else {
                    result.setChild(build(ILLang.op_add,
                            copy(leftOfTest),
                            build(ILLang.op_intCst, 1)), 3);
                }
            }

        }
        return result;
    }

    /** Builds a new Tree, copy of given ref-expression "ref", but in which
     * root ref-expression "oldRoot" is replaced with "newRoot" */
    public static Tree replaceRootWithNewRoot(Tree ref, Tree oldRoot, Tree newRoot) {
        Tree result = null ;
        if (ref==null) {
            result = null ;
        } else if (ref.equalsTree(oldRoot)) {
            result = newRoot ;
        } else {
            switch(ref.opCode()) {
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
            case ILLang.op_pointerAccess:
                result = ILUtils.build(ref.opCode(),
                           replaceRootWithNewRoot(ref.down(1), oldRoot, newRoot),
                           ILUtils.copy(ref.down(2))) ;
                break ;
            case ILLang.op_allocate:
                result = ILUtils.build(ILLang.op_allocate,
                           replaceRootWithNewRoot(ref.down(1), oldRoot, newRoot),
                           ILUtils.copy(ref.down(2)),
                           ILUtils.copy(ref.down(3)),
                           ILUtils.copy(ref.down(4)),
                           ILUtils.copy(ref.down(5))) ;
                break ;
            case ILLang.op_ident:
                result = ILUtils.copy(ref) ;
                break ;
            default:
                TapEnv.toolError("(Replace root reference): Unexpected operator: " + ref.opName()) ;
                result = ILUtils.copy(ref) ;
            }
        }
        return result ;
    }

    public static void replaceAllCallName(Tree tree, String oldUnitName, String newUnitName, Tree newUnitNameTree, SymbolTable symbolTable) {
        switch (tree.opCode()) {
            case ILLang.op_call: {
                Tree calledName = getCalledName(tree);
                if (calledName.opCode() == ILLang.op_ident
                    && (TapEnv.associationByAddress()
                        ? getIdentString(calledName).equals(oldUnitName)
                        : symbolTable.isFunctionNamed(getIdentString(calledName), oldUnitName))) {
                    if (newUnitNameTree != null) {
                        tree.setChild(copy(newUnitNameTree), 2);
                    } else {
                        calledName.setValue(newUnitName);
                    }
                    Object annotValue;
                    Tree annot = null;
                    Enumeration<?> e = tree.allAnnotations();
                    if (e != null) {
                        while (e.hasMoreElements()) {
                            annotValue = ((TapPair) e.nextElement()).second;
                            if (annotValue instanceof Tree) {
                                annot = (Tree) annotValue;
                                if (annot.opCode() == ILLang.op_call
                                        && isIdent(getCalledName(annot), oldUnitName, true)) {
                                    getCalledName(annot).setValue(newUnitName);
                                }
                            }
                        }
                    }
                }
                Tree arguments = getArguments(tree);
                replaceAllCallName(arguments, oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                break;
            }
            case ILLang.op_plusAssign:
            case ILLang.op_minusAssign:
            case ILLang.op_timesAssign:
            case ILLang.op_divAssign:
            case ILLang.op_assign:
                replaceAllCallName(tree.down(1), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                replaceAllCallName(tree.down(2), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                break;
            case ILLang.op_fieldAccess:
                if (newUnitNameTree != null) {
                    replaceAllCallName(tree.down(1), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                }
                break;
            case ILLang.op_arrayAccess:
            case ILLang.op_substringAccess:
            case ILLang.op_pointerAccess:
                if (newUnitNameTree != null) {
                    replaceAllCallName(tree.down(1), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                }
                replaceAllCallName(tree.down(2), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                break;
            case ILLang.op_function:
                // This is a function decl in an interface decl: only the function name may need update:
                replaceAllCallName(tree.down(4), oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                break;
            case ILLang.op_ident: {
                String idName = getIdentString(tree);
                if (idName!=null
                    && (TapEnv.associationByAddress()
                        ? idName.equals(oldUnitName)
                        : symbolTable.isFunctionNamed(idName, oldUnitName))) {
                    if (newUnitNameTree != null) {
                        Tree newFuncName = copy(newUnitNameTree);
                        newFuncName.setAnnotation("isFunctionName", Boolean.TRUE);
                        tree.parent().setChild(newFuncName, tree.rankInParent());
                    } else {
                        tree.setValue(newUnitName);
                        tree.setAnnotation("isFunctionName", Boolean.TRUE);
                    }
                }
                break;
            }
//             case ILLang.op_varDeclaration:
//                 break;
            default: {
                Tree[] subTrees = tree.children();
                if (subTrees != null) {
                    for (int i = subTrees.length - 1; i >= 0; i--) {
                        replaceAllCallName(subTrees[i], oldUnitName, newUnitName, newUnitNameTree, symbolTable);
                    }
                }
                break;
            }
        }
    }

    public static void replaceAllIdentsNamed(Tree tree, String oldName, String newName) {
        Tree currentTree;
        for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd(); i.advance()) {
            currentTree = i.get();
            if (currentTree.opCode() == ILLang.op_ident
                    && currentTree.stringValue().equals(oldName)) {
                currentTree.setValue(newName);
            }
        }
    }

    public static void replaceUsedModule(Tree tree, String oldUnitName, Tree newUnitNameTree) {
        Tree currentTree;
        for (TopDownTreeWalk i = new TopDownTreeWalk(tree); !i.atEnd(); i.advance()) {
            currentTree = i.get();
            if (currentTree.opCode() == ILLang.op_useDecl
                    && currentTree.down(1).stringValue().equals(oldUnitName)) {
                currentTree.setChild(copy(newUnitNameTree), 1);
            }
        }
    }

    /**
     * @return a TapList of module names used by the op_interfaceDecl Tree.
     */
    public static TapList<String> getModulesFromInterfaceDecl(Tree tree) {
        TapList<String> result = null;
        int nbInterfaces = tree.down(2).length();
        Tree blockStatementTree;
        String moduleName;
        for (int i = 1; i <= nbInterfaces; i++) {
            if (tree.down(2).down(i).opCode() == ILLang.op_interface) {
                blockStatementTree = tree.down(2).down(i).down(1).down(6);
                for (int j = 1; j <= blockStatementTree.length(); j++) {
                    if (blockStatementTree.down(j).opCode() == ILLang.op_useDecl) {
                        moduleName = getIdentString(blockStatementTree.down(j).down(1));
                        if (!moduleName.startsWith("#") && !moduleName.equals("iso_c_binding")) {
                            result = new TapList<>(moduleName, result);
                        }
                    }
                }
            }
        }
        return result;
    }

    public static void removeUsedModuleFromInterfaceDecl(Tree tree, String moduleName) {
        int nbInterfaces = tree.down(2).length();
        Tree blockStatementTree;
        for (int i = 1; i <= nbInterfaces; i++) {
            if (tree.down(2).down(i).opCode() == ILLang.op_interface) {
                blockStatementTree = tree.down(2).down(i).down(1).down(6);
                for (int j = blockStatementTree.length(); j >= 1; j--) {
                    if (blockStatementTree.down(j).opCode() == ILLang.op_useDecl) {
                        if (getIdentString(blockStatementTree.down(j).down(1)).equals(moduleName)) {
                            blockStatementTree.removeChild(j);
                        }
                    }
                }
            }
        }
    }

    /** Combines two successive use-restriction trees,
     * that are of form either "onlyVisibles" or "renamesVisibles".
     * These restriction trees apply in order, i.e. this method
     * returns the use-restriction tree that results from applying
     * "newRestrict" over "oldRestrict".
     * <pre>
     * [any] + renamedVisibles[] &rarr; [any]
     * onlyVisibles[ident1] + onlyVisibles[ident2] &rarr; onlyVisibles[ident1]
     * renamedVisibles[r1] + renamedVisibles[r2] &rarr; renamedVisibles[r1 + r2] (TODO)
     * onlyVisibles[ident1] + renamedVisibles[r2] &rarr; onlyVisibles[ident1, renamedVisibles[r2]] (TODO)
     * </pre>
     */
    public static Tree mergeUseDeclTrees(Tree newRestrict, Tree oldRestrict) {
        Tree result = newRestrict;
        if (newRestrict.opCode() == ILLang.op_renamedVisibles
            && newRestrict.length() == 0) {
            result = oldRestrict;
        } else if (oldRestrict.opCode() == ILLang.op_renamedVisibles
                   && oldRestrict.length() == 0) {
            result = newRestrict;
        } else if (newRestrict.opCode() == ILLang.op_onlyVisibles
                   && oldRestrict.opCode() == ILLang.op_onlyVisibles) {
            TapList<Tree> newList = newRestrict.childrenList() ;
            TapList<Tree> toOldList = new TapList<>(null, oldRestrict.childrenList()) ;
            TapList<Tree> inOldList ;
            Tree oldElem, newElem ;
            String newName ;
            TapList<Tree> cumul = null ;
            while (newList!=null) {
                newElem = newList.head ;
                newName = (newElem.opCode()==ILLang.op_renamed ? newElem.down(2).stringValue() : newElem.stringValue()) ;
                inOldList = toOldList ;
                boolean found = false ;
                while (!found && inOldList.tail!=null) {
                    oldElem = inOldList.tail.head ;
                    if (oldElem.opCode()==ILLang.op_ident && oldElem.stringValue().equals(newName)) {
                        cumul = new TapList<>(ILUtils.copy(newElem), cumul) ;
                        found = true ;
                        inOldList.tail = inOldList.tail.tail ;
                    } else if (oldElem.opCode()==ILLang.op_renamed && oldElem.down(1).stringValue().equals(newName)) {
                        if (newElem.opCode()==ILLang.op_renamed) {
                            cumul = new TapList<>(ILUtils.build(ILLang.op_renamed,
                                                    ILUtils.copy(newElem.down(1)),
                                                    ILUtils.copy(oldElem.down(2))),
                                                  cumul) ;
                        } else { //newElem.opCode()==ILLang.op_ident
                            cumul = new TapList<>(ILUtils.copy(oldElem), cumul) ;
                        }
                        found = true ;
                        inOldList.tail = inOldList.tail.tail ;
                    } else {
                        inOldList = inOldList.tail ;
                    }
                }
                newList = newList.tail ;
            }
            result = ILUtils.build(ILLang.op_onlyVisibles, TapList.nreverse(cumul)) ;

        } else if (newRestrict.opCode() == ILLang.op_renamedVisibles
                   && oldRestrict.opCode() == ILLang.op_renamedVisibles
                   && newRestrict.down(1).opCode() == ILLang.op_renamed
                   && oldRestrict.down(1).opCode() == ILLang.op_renamed) {
            // TODO: the following seems partial, and should be rewritten:
            if (newRestrict.down(1).down(2).stringValue().equals(oldRestrict.down(1).down(1).stringValue())) {
                result = copy(result);
                result.down(1).setChild(copy(oldRestrict.down(1).down(2)), 2);
            } else {
                TapEnv.toolWarning(-1, "(Merge use declarations) Renamed declarations probably ignored: (" + toString(newRestrict) + ") and (" + toString(oldRestrict) + ')');
            }
        } else {
            TapEnv.toolWarning(-1, "(Merge use declarations) Renamed declarations probably ignored: (" + toString(newRestrict) + ") and (" + toString(oldRestrict) + ')');
        }
        return result;
    }

    /**
     * Transforms a varDeclaration with arrayType into
     * a varDeclaration with declarators with arrayDeclarator (fortran77 style).
     *
     * @return the modified varDeclaration.
     */
    public static Tree arrayTypeToArrayDeclarator(Tree tree) {
        assert tree != null;
        Tree result = copy(tree); // pour garder les annotations sur tree
        Tree dimColons = tree.down(2).down(2);
        result.setChild(copy(result.down(2).down(1)), 2);
        Tree[] declarators = tree.down(3).children();
        Tree decl;
        for (int i = 0; i < declarators.length; i++) {
            decl = build(ILLang.op_arrayDeclarator,
                    copy(declarators[i]),
                    copy(dimColons));
            result.down(3).setChild(decl, i + 1);
        }
        return result;
    }

    public static boolean containsArrayDeclarator(SymbolDecl varDecl,
                                                  Tree tree) {
        String varName = varDecl.symbol;
        Tree[] varDecls = tree.down(2).children();
        boolean found = false;
        int i = 0;
        while (!found && i < varDecls.length) {
            if (varDecls[i].opCode() == ILLang.op_arrayDeclarator) {
                String varDeclName = varDecls[i].down(1).stringValue();
                found = varDeclName.equals(varName);
            }
            i = i + 1;
        }
        return found;
    }

    public static boolean hasInitializations(Tree tree) {
        boolean found = false;
        switch (tree.opCode()) {
            case ILLang.op_declarators: {
                Tree[] sons = tree.children();
                for (int i = sons.length - 1; !found && i >= 0; --i) {
                    found = hasInitializations(sons[i]);
                }
                break;
            }
            case ILLang.op_assign:
            case ILLang.op_pointerAssign:
                found = true;
                break;
            default:
                break;
        }
        return found;
    }

    public static boolean hasFortran90Modifiers(Tree tree) {
        boolean found = false;
        if (tree.opCode() == ILLang.op_modifiedType) {
            if (tree.down(1).opCode() == ILLang.op_modifiers) {
                int length = tree.down(1).length();
                int i = 1;
                while (!found && i <= length) {
                    found = isFortran90Modifier(tree.down(1).down(i));
                    i = i + 1;
                }
            }
        }
        return found;
    }

    private static boolean isFortran90Modifier(Tree tree) {
        boolean result;
        if (tree.opCode() == ILLang.op_ident) {
            String value = tree.stringValue();
            result = value.equals("in") || value.equals("out") || value.equals("inout");
        } else {
            result = tree.opCode() == ILLang.op_dimColons;
        }
        return result;
    }

    /**
     * @return a printable string to represent the given reference Tree.
     */
    public static String buildNameInText(Tree tree) {
        if (tree == null) {
            return "";
        }
        switch (tree.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_metavar:
                return getIdentString(tree);
            case ILLang.op_arrayAccess:
            case ILLang.op_arrayDeclarator:
                return buildNameInText(tree.down(1))/*+"()"*/;
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_nameEq:
                return buildNameInText(tree.down(2));
            case ILLang.op_pointerAccess:
            case ILLang.op_pointerDeclarator:
                return "*" + buildNameInText(tree.down(1));
            case ILLang.op_fieldAccess:
                return buildNameInText(tree.down(1)) + "%" + getIdentString(tree.down(2));
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
                return "ConstantValue";
            case ILLang.op_minus:
            case ILLang.op_referenceDeclarator:
                return buildNameInText(tree.down(1));
            case ILLang.op_address:
                return "Adr" + buildNameInText(tree.down(1));
            case ILLang.op_none:
                return "None" ;
            default:
                TapEnv.toolError("(Building hint string): Unexpected operator: " + tree.opName() + " in " + tree);
                return toString(tree);
        }
    }

    /**
     * @return a string that can be used in an identifier to represent the given reference Tree.
     */
    public static String buildNameInIdent(Tree tree) {
        if (tree == null) {
            return "";
        }
        switch (tree.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_metavar:
                return getIdentString(tree);
            case ILLang.op_arrayAccess:
            case ILLang.op_arrayDeclarator:
            case ILLang.op_minus:
            case ILLang.op_referenceDeclarator:
                return buildNameInIdent(tree.down(1));
            case ILLang.op_modifiedDeclarator:
            case ILLang.op_nameEq:
                return buildNameInIdent(tree.down(2));
            case ILLang.op_pointerAccess:
            case ILLang.op_pointerDeclarator:
                return "Drf" + buildNameInIdent(tree.down(1));
            case ILLang.op_fieldAccess:
                return buildNameInIdent(tree.down(1)) + "_" + getIdentString(tree.down(2));
            case ILLang.op_realCst:
            case ILLang.op_intCst:
            case ILLang.op_stringCst:
                return "ConstantValue";
            case ILLang.op_address:
                return "Adr" + buildNameInIdent(tree.down(1));
            case ILLang.op_none:
                return "None" ;
            default:
                TapEnv.toolError("(Building hint string): Unexpected operator: " + tree.opName() + " in " + tree);
                return toString(tree);
        }
    }

    /**
     * used in size(...).
     */
    public static Tree buildSizeArgument1(Tree tree, SymbolTable symbolTable) {
        if (isAVarRef(tree, symbolTable)) {
            switch (tree.opCode()) {
                case ILLang.op_arrayAccess:
                case ILLang.op_arrayDeclarator:
                case ILLang.op_pointerAccess:
                case ILLang.op_minus:
                case ILLang.op_address:
                    return buildSizeArgument1(tree.down(1), symbolTable);
                default:
                    return tree;
            }
        } else {
            return null;
        }
    }

    public static boolean hasATypeOperator(Tree tree) {
        boolean result;
        int operator = tree.opCode();
        result = operator == ILLang.op_pointerType
                || operator == ILLang.op_arrayType;
        return result;
    }

    public static boolean isAFortranOverloadedOperator(Tree tree) {
        return !tree.isAtom()
                && tree.opCode() != ILLang.op_binary
                && tree.down(1)!=null
                && tree.down(1).opCode() == ILLang.op_none;
    }

    /**
     * Removes the specialization suffix added to C++ specialized templated objects.
     */
    public static String stripSpecialization(String name) {
        int index = name.indexOf("_SPEC");
        return index <= 0 ? name : name.substring(0, index);
    }

    public static Tree concatWithNull(Tree strTree) {
        return build(ILLang.op_concat, strTree,
                buildCall(
                        build(ILLang.op_ident, "char"),
                        build(ILLang.op_expressions,
                                build(ILLang.op_intCst, 0))));
    }

    public static Tree buildStringCst(String str, boolean fromFortran) {
        Tree result = build(ILLang.op_stringCst, str);
        if (fromFortran) {
            result = ILUtils.concatWithNull(result);
        }
        return result;
    }

// begin all about OpenMP

    /**
     * If "ompPragma" contains a "schedule" clause.
     *
     * @return the "schedule" clause and removes it from "ompPragma".
     */
    public static Tree getRemoveSchedule(Tree ompPragma) {
        Tree schedule = null;
        int rank = -1;
        if (!isNullOrNone(ompPragma.down(2))) {
            Tree[] clauses = ompPragma.down(2).children();
            for (int i = clauses.length - 1; rank == -1 && i >= 0; --i) {
                if (clauses[i] != null && clauses[i].opCode() == ILLang.op_schedule) {
                    rank = i;
                }
            }
            if (rank != -1) {
                schedule = clauses[rank];
                ompPragma.down(2).removeChild(rank + 1);
            }
        }
        return schedule;
    }

    /**
     * @return True if the given "schedule" clause actually specifies "static".
     */
    public static boolean isStaticSchedule(Tree schedule) {
        if (schedule == null) {
            return false;
        }
        Tree[] exps = schedule.down(1).children();
        boolean hasStatic = false;
        for (int i = exps.length - 1; !hasStatic && i >= 0; --i) {
            if (isIdent(exps[i], "static", false)) {
                hasStatic = true;
            }
        }
        return hasStatic;
    }

    /**
     * Adds the given "schedule" clause to the (head of the) clauses of the given "ompPragma".
     */
    public static void addSchedule(Tree schedule, Tree ompPragma) {
        if (!isNullOrNone(schedule)) {
            if (isNullOrNone(ompPragma.down(2))) {
                ompPragma.setChild(build(ILLang.op_expressions), 2);
            }
            ompPragma.down(2).addChild(schedule, 1);
        }
    }

// end all about OpenMP

// begin all about numerical operations on trees:

    public static Tree tapenadeUtilityFunctionName(Unit diffUnit, String funcName) {
        return build(ILLang.op_ident, (diffUnit.isFortran() ? funcName.toUpperCase() : funcName));
    }

    public static Tree cleanBoolCopy(Tree boolTree, Unit diffUnit) {
        if (boolTree == null) {
            return null;
        }
        String boolStr = getIdentString(boolTree);
        if (boolStr == null || boolStr.isEmpty()) {
            return null;
        }
        if (boolStr.equalsIgnoreCase("true") ||
                boolStr.equalsIgnoreCase(".true.")) {
            return build(ILLang.op_boolCst, diffUnit.isC() ? "1" : "TRUE");
        } else if (boolStr.equalsIgnoreCase("false") ||
                boolStr.equalsIgnoreCase(".false.")) {
            return build(ILLang.op_boolCst, diffUnit.isC() ? "0" : "FALSE");
        } else {
            return copy(boolTree);
        }
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to 0.
     * ATTENTION CONVENTION: null Tree evaluates to zero !.
     */
    public static boolean evalsToZero(Tree tree) {
        if (tree == null) {
            return true;
        }
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() == 0;
            case ILLang.op_realCst: {
                TapIntList toIntVal = new TapIntList(0, null);
                return realIsInt(tree.stringValue(), toIntVal)
                        && toIntVal.head == 0;
            }
            case ILLang.op_minus:
                return evalsToZero(tree.down(1));
            default:
                return false;
        }
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to 1.
     */
    public static boolean evalsToOne(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() == 1;
            case ILLang.op_realCst: {
                TapIntList toIntVal = new TapIntList(0, null);
                return realIsInt(tree.stringValue(), toIntVal)
                        && toIntVal.head == 1;
            }
            case ILLang.op_minus:
                return evalsToMinusOne(tree.down(1));
            default:
                return false;
        }
    }

    /**
     * @return the optional "dim" argument of a F90 SUM. Returns null when no dim is present.
     */
    public static Tree getOptionalDim(Tree[] srcParamExprs, SymbolTable symbolTable) {
        Tree dimTree = null;
        for (int i = srcParamExprs.length - 1; i >= 1 && dimTree == null; --i) {
            if (srcParamExprs[i].opCode() == ILLang.op_nameEq
                    && srcParamExprs[i].down(1).stringValue().equals("dim")) {
                dimTree = srcParamExprs[i].down(2);
            }
        }
        if (dimTree == null && srcParamExprs.length >= 2) {
            WrapperTypeSpec arg1type = symbolTable.typeOf(srcParamExprs[1]);
            if (!TypeSpec.isA(arg1type, SymbolTableConstants.ARRAYTYPE) && srcParamExprs[1].opCode() != ILLang.op_nameEq) {
                dimTree = srcParamExprs[1];
            }
        }
        return dimTree;
    }

    /**
     * @return the optional "mask" argument of a F90 SUM. Returns null when no mask is present.
     */
    public static Tree getOptionalMask(Tree[] srcParamExprs, SymbolTable symbolTable) {
        Tree maskTree = null;
        for (int i = srcParamExprs.length - 1; i >= 1 && maskTree == null; --i) {
            if (srcParamExprs[i].opCode() == ILLang.op_nameEq
                    && srcParamExprs[i].down(1).stringValue().equals("mask")) {
                maskTree = srcParamExprs[i].down(2);
            }
        }
        if (maskTree == null && srcParamExprs.length >= 2) {
            WrapperTypeSpec arg1type = symbolTable.typeOf(srcParamExprs[1]);
            if (TypeSpec.isA(arg1type, SymbolTableConstants.ARRAYTYPE) && srcParamExprs[1].opCode() != ILLang.op_nameEq) {
                maskTree = srcParamExprs[1];
            } else if (srcParamExprs.length >= 3 && srcParamExprs[2].opCode() != ILLang.op_nameEq) {
                maskTree = srcParamExprs[2];
            }
        }
        return maskTree;
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to -1.
     */
    public static boolean evalsToMinusOne(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() == -1;
            case ILLang.op_realCst: {
                TapIntList toIntVal = new TapIntList(0, null);
                return realIsInt(tree.stringValue(), toIntVal)
                        && toIntVal.head == -1;
            }
            case ILLang.op_minus:
                return evalsToOne(tree.down(1));
            default:
                return false;
        }
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to a value strictly positive.
     */
    public static boolean evalsToGTZero(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() > 0;
            case ILLang.op_realCst: {
                String value = tree.stringValue();
                ToBool isDouble = new ToBool(false);
                int[] digits = new int[40];
                parseNumericalString(value, digits, isDouble, false);
                return digits[0] > 0;
            }
            default:
                return false;
        }
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to a value strictly positive.
     */
    public static boolean evalsToGEZero(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() >= 0;
            case ILLang.op_realCst: {
                String value = tree.stringValue();
                ToBool isDouble = new ToBool(false);
                int[] digits = new int[40];
                parseNumericalString(value, digits, isDouble, false);
                return digits[0] >= 0;
            }
            default:
                return false;
        }
    }

    /**
     * @return true if "tree" is an expression with no variables,
     * that evaluates (statically) to a value strictly negative.
     */
    public static boolean evalsToLTZero(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue() < 0;
            case ILLang.op_realCst: {
                String value = tree.stringValue();
                ToBool isDouble = new ToBool(false);
                int[] digits = new int[40];
                parseNumericalString(value, digits, isDouble, false);
                return digits[0] < 0;
            }
            default:
                return false;
        }
    }

    /**
     * Tests that the current String (found in a op_realCst)
     * actually represents an integer.
     * If so, returns true, and if "toIntVal" is non-null, also sets its
     * head to the corresponding integer value if in [-maxint;+maxint].
     */
    protected static boolean realIsInt(String str, TapIntList toIntVal) {
        if (str!=null && (str.endsWith("iF") || str.endsWith("jF"))) {
            // this is a C complex constant.
            return false ;
        }
        int[] digits = new int[40];
        int power = parseNumericalString(str, digits, new ToBool(false), false);
        if (digits[0] == 0) {
            toIntVal.head = 0;
            return true;
        }
        if (power < 0) {
            return false;
        }
        for (int i = power + 1; i < 40; ++i) {
            if (digits[i] != 0) {
                return false;
            }
        }
        if (toIntVal != null) {
            int val = 0;
            int d;
            boolean negative = false;
            for (int i = 0; i <= power; ++i) {
                d = digits[i];
                if (d < 0) {
                    negative = true;
                    d = -d;
                }
                if (i == 39 || val + 1 >= Integer.MAX_VALUE / 10) {
                    i = power;
                    val = Integer.MAX_VALUE;
                } else {
                    val = 10 * val + d;
                }
            }
            if (negative) {
                val = -val;
            }
            toIntVal.head = val;
        }
        return true;
    }

    /**
     * @return true when expression is constant or made with constants.
     */
    public static boolean isExpressionConstant(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
            case ILLang.op_realCst:
                return true;
            case ILLang.op_minus:
                return isExpressionConstant(tree.down(1));
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_mul:
            case ILLang.op_div:
                if (isExpressionConstant(tree.down(1))) {
                    return isExpressionConstant(tree.down(2));
                } else {
                    return false;
                }
            default:
                return false;
        }
    }

    /**
     * @return true when it is a numerical constant.
     */
    public static boolean isExpressionNumConstant(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
            case ILLang.op_realCst:
                return true;
            case ILLang.op_minus:
                return isExpressionNumConstant(tree.down(1));
            default:
                return false;
        }
    }

    /**
     * @return true when it is an integer constant.
     */
    public static boolean isExpressionIntConstant(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return true;
            case ILLang.op_minus:
                return isExpressionIntConstant(tree.down(1));
            default:
                return false;
        }
    }

    /**
     * @return value when it is an integer constant.
     */
    public static int intConstantValue(Tree tree) {
        switch (tree.opCode()) {
            case ILLang.op_intCst:
                return tree.intValue();
            case ILLang.op_minus:
                return -intConstantValue(tree.down(1));
            default:
                TapEnv.toolError("(Value of int constant) Unexpected operator: " + tree.opName());
                return 0;
        }
    }

    /**
     * @return the constant Tree sum of the two given constant Trees.
     */
    public static Tree addExpressionsNumConstants(Tree left, Tree right) {
        int leftOp = left.opCode();
        int rightOp = right.opCode();
        boolean leftIsOpposed = false;
        boolean rightIsOpposed = false;
        int leftSign = 1;
        int rightSign = 1;
        while (leftOp == ILLang.op_minus) {
            left = left.down(1);
            leftSign = -leftSign;
            leftIsOpposed = !leftIsOpposed;
            leftOp = left.opCode();
        }
        while (rightOp == ILLang.op_minus) {
            right = right.down(1);
            rightSign = -rightSign;
            rightIsOpposed = !rightIsOpposed;
            rightOp = right.opCode();
        }
        if (leftOp == ILLang.op_intCst && rightOp == ILLang.op_intCst) {
            return build(ILLang.op_intCst,
                    leftSign * left.intValue()
                            + rightSign * right.intValue());
        } else {
            String leftString = left.stringValue();
            String rightString = right.stringValue();
            return build(ILLang.op_realCst,
                    addNumericalStrings(
                            leftString, leftIsOpposed, rightString, rightIsOpposed));
        }
    }

    protected static String addNumericalStrings(
            String str1, boolean isStr1Opposed, String str2,
            boolean isStr2Opposed) {
        ToBool isDouble = new ToBool(false);
        int[] digits1 = new int[40];
        int power1 =
                parseNumericalString(str1, digits1, isDouble, isStr1Opposed);
        int[] digits2 = new int[40];
        int power2 =
                parseNumericalString(str2, digits2, isDouble, isStr2Opposed);
        boolean positiveSum = positiveSum(digits1, power1, digits2, power2);
        if (power1 <= power2) {
            shiftRight(digits1, power2 - power1 + 1, str1, str2);
            shiftRight(digits2, 1, str1, str2);
            power2++;
            power1 = power2;
        } else {
            shiftRight(digits1, 1, str1, str2);
            shiftRight(digits2, power1 - power2 + 1, str1, str2);
            power1++;
            power2 = power1;
        }
        int carry = 0;
        for (int i = 40 - 1; i >= 0; i--) {
            digits2[i] = digits1[i] + digits2[i] + carry;
            carry = 0;
            if (positiveSum) {
                if (digits2[i] >= 10) {
                    digits2[i] -= 10;
                    carry++;
                } else if (digits2[i] < 0) {
                    digits2[i] += 10;
                    carry--;
                }
            } else {
                if (digits2[i] > 0) {
                    digits2[i] -= 10;
                    carry++;
                } else if (digits2[i] <= -10) {
                    digits2[i] += 10;
                    carry--;
                }
            }
        }
        String result = "";
        int lasti = -1;
        if (!positiveSum) {
            result = "-";
            for (int i = 0; i < 40; i++) {
                digits2[i] = -digits2[i];
            }
        }
        for (int i = 0; i < 40; i++) {
            if (digits2[i] != 0) {
                lasti = i;
            }
        }
        if (power2 >= 0 && power2 <= 20) {
            /*then don't use "e" notation: */
            if (lasti < power2 + 1) {
                lasti = power2 + 1;
            }
            boolean initial = true;
            for (int i = 0; i <= lasti; i++) {
                if (!initial || power2 <= 0 || digits2[i] != 0) {
                    result = result + digits2[i];
                    initial = false;
                }
                if (i == power2) {
                    if (initial) {
                        result = result + '0';
                    }
                    result = result + '.';
                }
            }
            if (isDouble.get()) {
                result = result + "D0";
            }
        } else {
            /*use "e" notation: */
            result = result + digits2[0] + '.';
            for (int i = 1; i <= lasti; i++) {
                result = result + digits2[i];
            }
            if (isDouble.get()) {
                result = result + 'D' + power2;
            } else {
                result = result + 'E' + power2;
            }
        }
        return result;
    }

    /**
     * Parses the String "str" considered as a number, regardless integer or floating point.
     * when "takeOpposite" is true,  returns the opposite number of what "str" gives.
     *
     * @return 3 things:
     * <pre>
     * 1) in the result: the power of 10 applied to the mantissa
     * 2) in digits[], the mantissa
     * 3) in isDouble, true when the number is DOUBLE PRECISION.
     * </pre>
     * Warning: negative numbers are represented with negative digits !!
     * The returned number should be understood as &lt;digits[0]&gt;.&lt;digits[1:39]&gt;*10^power.
     */
    private static int parseNumericalString(String str,
                                            int[] digits, ToBool isDouble, boolean takeOpposite) {
        int strl = str.length();
        char c;
        boolean isNegativePower = false;
        boolean inMantissa = true;
        boolean beforeDot = true;
        int power = 0;
        int curPower = 0;
        int epower = 0;
        int i;
        int d;
        for (d = 0; d < 40; d++) {
            digits[d] = 0;
        }
        d = 0;
        for (i = 0; i < strl; i++) {
            c = str.charAt(i);
            if (c >= '0' && c <= '9') {
                if (beforeDot) {
                    power++;
                } else {
                    curPower--;
                }
                if (inMantissa) {
                    if (!(d == 0 && c == '0')) {
                        digits[d] = c - '0';
                        if (d == 0) {
                            if (beforeDot) {
                                power = 0;
                            } else {
                                power = curPower;
                            }
                        }
                        d++;
                    }
                } else {
                    epower = epower * 10 + c - '0';
                }
            } else if (c == '.') {
                curPower = 0;
                beforeDot = false;
            } else if (c == '-') {
                if (inMantissa) {
                    takeOpposite = !takeOpposite;
                } else {
                    isNegativePower = true;
                }
            } else if (c == 'd' || c == 'D') {
                isDouble.set(true);
                inMantissa = false;
                beforeDot = false;
            } else if (c == 'e' || c == 'E') {
                inMantissa = false;
                beforeDot = false;
            } else if (c == '_') {
                beforeDot = false;
                i = strl; //=> don't look after the "_"
            }
        }
        if (takeOpposite) {
            for (d = 0; d < 40; d++) {
                digits[d] = -digits[d];
            }
        }
        return isNegativePower ? power - epower : power + epower;
    }

    private static boolean positiveSum(
            int[] digits1, int power1, int[] digits2, int power2) {
        if (power1 > power2) {
            return digits1[0] > 0;
        } else if (power2 > power1) {
            return digits2[0] > 0;
        } else {
            int d = 0;
            boolean found = false;
            while (!found && d < 40) {
                if (digits1[d] + digits2[d] != 0) {
                    found = true;
                } else {
                    d++;
                }
            }
            return d >= 40 || digits1[d] + digits2[d] > 0;
        }
    }

    private static void shiftRight(int[] digits, int offset, String str1, String str2) {
        boolean loosePrecision = false;
        int i;
        if (offset > 40) {
            offset = 40;
        }
        for (i = 40 - offset; i < 40; i++) {
            if (digits[i] != 0) {
                loosePrecision = true;
            }
        }
        if (loosePrecision) {
            TapEnv.toolError("Lost accuracy in adding " + str1 + " and " + str2);
        }
        for (i = 40 - offset - 1; i >= 0; i--) {
            digits[i + offset] = digits[i];
        }
        for (i = 0; i < offset; i++) {
            digits[i] = 0;
        }
    }

    /**
     * @return true if "tree" is an expression that starts by a "-".
     */
    public static boolean isNegativeExpression(Tree tree) {
        int oper = tree.opCode();
        if (oper == ILLang.op_intCst) {
            int value = tree.intValue();
            return value < 0;
        } else if (oper == ILLang.op_realCst) {
            String value = tree.stringValue();
            return value.charAt(0) == '-';
        } else {
            return oper == ILLang.op_minus;
        }
    }

    /**
     * @return the Tree opposite of a numerical expression.
     */
    public static Tree buildExpressionOpposite(Tree tree) {
        Tree result = buildNumericalOpposite(tree);
        if (result == null) {
            return build(ILLang.op_minus, tree);
        } else {
            return result;
        }
    }

    /**
     * @return the Tree opposite of a numerical expression if it is easy,
     * e.g. in particular when isNegativeExpression(tree) is true, null otherwise.
     */
    public static Tree buildNumericalOpposite(Tree tree) {
        int oper = tree.opCode();
        if (oper == ILLang.op_intCst) {
            int value = tree.intValue();
            return build(ILLang.op_intCst, -value);
        } else if (oper == ILLang.op_realCst) {
            StringBuilder valueBuf = new StringBuilder(tree.stringValue());
            if (valueBuf.charAt(0) == '-') {
                valueBuf.delete(0, 1);
            } else {
                valueBuf.insert(0, '-');
            }
            return build(ILLang.op_realCst, valueBuf.toString());
        } else if (oper == ILLang.op_minus) {
            return tree.cutChild(1);
        } else {
            return null;
        }
    }

    /**
     * @return true when the given Tree seems to be an expression with
     * a numerical INTEGER value, that can be computed.
     */
    public static boolean seemsInteger(Tree exp) {
        switch (exp.opCode()) {
            case ILLang.op_div:
            case ILLang.op_mul:
            case ILLang.op_add:
            case ILLang.op_sub:
            case ILLang.op_power:
                return seemsInteger(exp.down(1)) && seemsInteger(exp.down(2));
            case ILLang.op_minus:
                return seemsInteger(exp.down(1));
            case ILLang.op_intCst:
            case ILLang.op_ident:
                return true;
            case ILLang.op_realCst:
            default:
                return false;
        }
    }

// Management of protected expressions, i.e. expressions that
// differ according to a test (think of (test?exp1:exp2) in C).

    /**
     * @return the expression tree that computes the last (if "exit" is false)
     * or the exit value (if "exit" is true)
     * of the index of a do-loop. @return null when this expression
     * contains side-effets and therefore cannot be executed
     * twice without risk.
     */
    public static Tree hasDuplicableLastIndex(Tree doLoopControl, boolean exit) {
        Tree resultTree = null;
        Tree firstTree = doLoopControl.down(2);
        Tree lastTree = doLoopControl.down(3);
        Tree strideTree = doLoopControl.down(4);
        if (isNullOrNone(strideTree)) {
            strideTree = build(ILLang.op_intCst, 1);
        }
        if (!(instrHasSideEffect(firstTree)
                || instrHasSideEffect(lastTree)
                || instrHasSideEffect(strideTree))) {
            /* Exit index is:
             * (lastTree+strideTree) - MOD((lastTree-firstTree), strideTree) */
            resultTree =
                    subTree(exit ?
                                    addTree(copy(lastTree), copy(strideTree))
                                    : copy(lastTree),
                            buildSmartMod(
                                    subTree(copy(lastTree), copy(firstTree)),
                                    copy(strideTree)));
        }
        return resultTree;
    }

    /**
     * Multiplies two factors, taking care of the case where
     * any of them is protected. In this case, builds a new protected
     * factor, where the test is the OR of both tests
     * and the factor is the mulTree of the two factors.
     * In either branch "then" and "else", a null or a none "()" means zero.
     * Works only in the following cases (symmetric cases work just as well):
     * <pre>
     * ()           * ()               &rarr; ()
     * Exp1         * Exp2             &rarr; Exp1*Exp2
     * Exp1         * (T2?SExp2:Exp2)  &rarr; (T2?Exp1*SExp2:Exp1*Exp2)
     * (T1?():Exp1) * (T2?():Exp2)     &rarr; (T1orT2?():Exp1*Exp2)
     * </pre>
     */
    public static Tree mulProtectedExprs(Tree factor1, TypeSpec type1, Tree factor2, TypeSpec type2) {
        if (factor1 == null || factor2 == null) {
            return null;
        }
        Tree test1 = isIfExpression(factor1) ? factor1.cutChild(1) : null;
        Tree test2 = isIfExpression(factor2) ? factor2.cutChild(1) : null;
        Tree result;
        if (test1 == null) {
            if (test2 == null) {
                result = mulTreeWatchComplex(factor1, type1, factor2, type2);
            } else {
                Tree fact2 = factor2.cutChild(3);
                Tree sfact2 = factor2.cutChild(2);
                Tree sfact;
                if (!isNullOrNone(sfact2)) {
                    sfact = mulProtectedExprs(copy(factor1), type1, sfact2, type2);
                } else {
                    sfact = null;
                }
                result = build(ILLang.op_ifExpression,
                               test2, sfact, mulProtectedExprs(factor1, type1, fact2, type2));
            }
        } else {
            Tree fact1 = factor1.cutChild(3);
            if (test2 == null) {
                Tree sfact1 = factor1.cutChild(2);
                Tree sfact;
                if (!isNullOrNone(sfact1)) {
                    sfact = mulProtectedExprs(sfact1, type1, copy(factor2), type2);
                } else {
                    sfact = null;
                }
                result = build(ILLang.op_ifExpression,
                               test1, sfact, mulProtectedExprs(fact1, type1, factor2, type2));
            } else {
                Tree fact2 = factor2.cutChild(3);
                result = build(ILLang.op_ifExpression,
                               build(ILLang.op_or, test1, test2), null,
                               mulProtectedExprs(fact1, type1, fact2, type2));
            }
        }
        return result;
    }

    /**
     * Multiplies two factors, taking care of the case where
     * one of them is a complex number constructor with one zero component.
     */
    private static Tree mulTreeWatchComplex(Tree factor1, TypeSpec type1, Tree factor2, TypeSpec type2) {
        if (factor1 == null || factor2 == null) {
            return null;
        }
        boolean isCmplx1 = isCallingString(factor1, "cmplx", false) || isCallingString(factor1, "complex", false);
        boolean zeroReal1 = false;
        boolean zeroImag1 = false;
        if (isCmplx1) {
            zeroReal1 = isZero(getArguments(factor1).down(1));
            zeroImag1 = isZero(getArguments(factor1).down(2));
        }
        boolean isCmplx2 = isCallingString(factor2, "cmplx", false) || isCallingString(factor2, "complex", false);
        boolean zeroReal2 = false;
        boolean zeroImag2 = false;
        if (isCmplx2) {
            zeroReal2 = isZero(getArguments(factor2).down(1));
            zeroImag2 = isZero(getArguments(factor2).down(2));
        }
        Tree result = null;
        if (isCmplx1 && (zeroReal1 || zeroImag1)) {
            if (!isCmplx2 && !(type2 != null && type2.isComplexBase())) {
                if (zeroReal1) {
                    result = buildCall(
                            build(ILLang.op_ident, "cmplx"),
                            build(ILLang.op_expressions,
                                    copy(getArguments(factor1).down(1)),
                                    mulTree(copy(getArguments(factor1).down(2)),
                                            factor2)));
                } else { //zeroImag1
                    result = buildCall(
                            build(ILLang.op_ident, "cmplx"),
                            build(ILLang.op_expressions,
                                    mulTree(copy(getArguments(factor1).down(1)),
                                            factor2),
                                    copy(getArguments(factor1).down(2))));
                }
                copyExtraChildren(getArguments(factor1), 2, getArguments(result), 2);
            } else if (zeroImag1) { // if factor1 is in fact a real.
                result = mulTree(copy(getArguments(factor1).down(1)), factor2);
            }
        } else if (!isCmplx1 && !(type1 != null && type1.isComplexBase())) {
            if (isCmplx2 && (zeroReal2 || zeroImag2)) {
                if (zeroReal2) {
                    result = buildCall(
                            build(ILLang.op_ident, "cmplx"),
                            build(ILLang.op_expressions,
                                    copy(getArguments(factor2).down(1)),
                                    mulTree(factor1,
                                            copy(getArguments(factor2).down(2)))));
                } else { //zeroImag2
                    result = buildCall(
                            build(ILLang.op_ident, "cmplx"),
                            build(ILLang.op_expressions,
                                    mulTree(factor1,
                                            copy(getArguments(factor2).down(1))),
                                    copy(getArguments(factor2).down(2))));
                }
                copyExtraChildren(getArguments(factor2), 2, getArguments(result), 2);
            }
        } else if (zeroImag2) { // if factor2 is in fact a real.
            result = mulTree(factor1, copy(getArguments(factor2).down(1)));
        }
        // Fallback case:
        if (result == null) {
            result = mulTree(factor1, factor2);
        }
        return result;
    }

    /**
     * Same as mulProtectedExprs, but makes a division.
     */
    public static Tree divProtectedExprs(Tree factor1, Tree factor2) {
        if (factor1 == null || factor2 == null) {
            return null;
        }
        Tree test1 = isIfExpression(factor1) ? factor1.cutChild(1) : null;
        Tree test2 = isIfExpression(factor2) ? factor2.cutChild(1) : null;
        Tree result;
        if (test1 == null) {
            if (test2 == null) {
                result = divTree(factor1, factor2);
            } else {
                Tree fact2 = factor2.cutChild(3);
                Tree sfact2 = factor2.cutChild(2);
                Tree sfact;
                if (!isNullOrNone(sfact2)) {
                    sfact = divProtectedExprs(copy(factor1), sfact2);
                } else {
                    sfact = null;
                }
                result = build(ILLang.op_ifExpression, test2, sfact, divProtectedExprs(factor1, fact2));
            }
        } else {
            Tree fact1 = factor1.cutChild(3);
            if (test2 == null) {
                Tree sfact1 = factor1.cutChild(2);
                Tree sfact;
                if (!isNullOrNone(sfact1)) {
                    sfact = divProtectedExprs(sfact1, copy(factor2));
                } else {
                    sfact = null;
                }
                result = build(ILLang.op_ifExpression, test1, sfact, divProtectedExprs(fact1, factor2));
            } else {
                Tree fact2 = factor2.cutChild(3);
                result = build(ILLang.op_ifExpression, build(ILLang.op_or, test1,
                        test2), null, divProtectedExprs(fact1, fact2));
            }
        }
        return result;
    }

    /**
     * Same as mulProtectedExprs, but just takes the opposite.
     */
    public static Tree minusProtectedExpr(Tree factor) {
        if (isIfExpression(factor)) {
            Tree fact = factor.cutChild(2);
            if (!isNullOrNone(fact)) {
                fact = minusProtectedExpr(fact);
            }
            factor.setChild(fact, 2);
            fact = factor.cutChild(3);
            if (!isNullOrNone(fact)) {
                fact = minusProtectedExpr(fact);
            }
            factor.setChild(fact, 3);
            return factor;
        } else {
            return minusTree(factor);
        }
    }

    public static Tree reducSumProtectedExpr(Tree factor, Tree maskTree) {
        boolean hasIf = isIfExpression(factor);
        Tree fact1, fact2 = null;
        Tree sumArgs;
        if (hasIf) {
            fact1 = factor.cutChild(2);
            fact2 = factor.cutChild(3);
        } else {
            fact1 = factor;
        }
        if (!isNullOrNone(fact1)) {
            if (maskTree == null) {
                sumArgs = build(ILLang.op_expressions, fact1);
            } else {
                sumArgs = build(ILLang.op_expressions, fact1,
                        build(ILLang.op_nameEq,
                                build(ILLang.op_ident, "MASK"),
                                copy(maskTree)));
            }
            fact1 = build(ILLang.op_call, build(ILLang.op_none), build(ILLang.op_ident, "sum"), sumArgs);
        }
        if (!isNullOrNone(fact2)) {
            if (maskTree == null) {
                sumArgs = build(ILLang.op_expressions, fact2);
            } else {
                sumArgs = build(ILLang.op_expressions, fact2,
                        build(ILLang.op_nameEq,
                                build(ILLang.op_ident, "MASK"),
                                copy(maskTree)));
            }
            fact2 = build(ILLang.op_call, build(ILLang.op_none), build(ILLang.op_ident, "sum"), sumArgs);
        }
        if (hasIf) {
            factor.setChild(fact1, 2);
            factor.setChild(fact2, 3);
        } else {
            factor = fact1;
        }
        return factor;
    }

    public static boolean isIfExpression(Tree expr) {
        return expr != null && (expr.opCode() == ILLang.op_if || expr.opCode() == ILLang.op_ifExpression);
    }

    /**
     * Adds two factors, taking care of the case where
     * any of them is protected (i.e. an ifExpression).
     * A few examples:
     * <pre>
     * Exp1 + Exp2                     &rarr; Exp1+Exp2
     * Exp1 + (T2?():Exp2)             &rarr; (T2?Exp1:Exp1+Exp2)
     * Exp1 + (T2?SExp2:Exp2)          &rarr; (T2?Exp1+SExp2:Exp1+Exp2)
     * </pre>
     */
    public static Tree addProtectedExprs(Tree factor1, Tree factor2) {
        // TODO: does not work when the inside of factor(1 or 2) contains a "if" at a deeper level
        if (factor1 == null) {
            return factor2;
        }
        if (factor2 == null) {
            return factor1;
        }
        Tree test1, then1, else1;
        if (isIfExpression(factor1)) {
            test1 = factor1.cutChild(1);
            then1 = factor1.cutChild(2);
            else1 = factor1.cutChild(3);
        } else {
            test1 = null;
            then1 = factor1;
            else1 = null;
        }
        Tree test2, then2, else2;
        if (isNullOrNone(then1)) {
            then1 = null;
        }
        if (isNullOrNone(else1)) {
            else1 = null;
        }
        if (isIfExpression(factor2)) {
            test2 = factor2.cutChild(1);
            then2 = factor2.cutChild(2);
            else2 = factor2.cutChild(3);
        } else {
            test2 = null;
            then2 = factor2;
            else2 = null;
        }
        if (isNullOrNone(then2)) {
            then2 = null;
        }
        if (isNullOrNone(else2)) {
            else2 = null;
        }
        Tree result;
        // Special cases when either factor1 or factor2 are not protected:
        if (test1 == null) {
            if (test2 == null) {
                result = addTreeWatchComplex(then1, then2);
            } else {
                Tree copyThen1 = copy(then1);
                result = build(ILLang.op_ifExpression,
                           copy(test2),
                           addProtectedExprs(then1, then2),
                           addProtectedExprs(copyThen1, else2));
            }
        } else {
            if (test2 == null) {
                Tree copyThen2 = copy(then2);
                result = build(ILLang.op_ifExpression,
                           copy(test1),
                           addProtectedExprs(then1, then2),
                           addProtectedExprs(else1, copyThen2));
            } else {
                // More complex case when both factor1 and factor2 are protected:
                Tree newThen;
                if (testImpliesTest(test1, test2)) {
                    newThen = addProtectedExprs(then1, then2);
                } else if (testImpliesNotTest(test1, test2)) {
                    newThen = addProtectedExprs(copy(then1), copy(else2));
                } else {
                    newThen = build(ILLang.op_ifExpression,
                                copy(test2),
                                addProtectedExprs(copy(then1), copy(then2)),
                                addProtectedExprs(copy(then1), copy(else2)));
                }
                Tree newElse;
                if (notTestImpliesTest(test1, test2)) {
                    newElse = addProtectedExprs(copy(else1), copy(then2));
                } else if (notTestImpliesNotTest(test1, test2)) {
                    newElse = addProtectedExprs(copy(else1), copy(else2));
                } else {
                    newElse = build(ILLang.op_ifExpression,
                                copy(test2),
                                addProtectedExprs(copy(else1), copy(then2)),
                                addProtectedExprs(copy(else1), copy(else2)));
                }
                result = build(ILLang.op_ifExpression,
                           copy(test1), newThen, newElse);
            }
        }
        return result;
    }

    /** Looks at argument argRank of the given callTree.
     * If this argument is "protected", i.e. it is an ifExpression,
     * and we are targetting Fortran, then returns a new tree which has
     * the protection outside and the call inside.
     * Otherwise returns the unmodified callTree. */
    public static Tree pullProtectedArg(Tree callTree, int argRank) {
        Tree arg = ILUtils.getArguments(callTree).down(argRank) ;
        if (arg!=null && arg.opCode()==ILLang.op_ifExpression
            && TapEnv.relatedUnit().isFortran()) {
            arg = ILUtils.getArguments(callTree).cutChild(argRank) ;
            Tree test = arg.cutChild(1) ;
            Tree argTrue = arg.cutChild(2) ;
            Tree argFalse = arg.cutChild(3) ;
            if (!ILUtils.isNullOrNone(argTrue)) {
                Tree finalArgTrue = ILUtils.copy(callTree) ;
                ILUtils.getArguments(finalArgTrue).setChild(argTrue, argRank) ;
                argTrue = finalArgTrue ;
            }
            if (!ILUtils.isNullOrNone(argFalse)) {
                Tree finalArgFalse = ILUtils.copy(callTree) ;
                ILUtils.getArguments(finalArgFalse).setChild(argFalse, argRank) ;
                argFalse = finalArgFalse ;
            }
            return ILUtils.build(ILLang.op_ifExpression,
                                 test, argTrue, argFalse);
        } else {
            return callTree ;
        }
    }

    /**
     * Adds two factors, taking care of the case where they are complex number constructors.
     */
    private static Tree addTreeWatchComplex(Tree factor1, Tree factor2) {
        if (factor1 == null) {
            return factor2;
        }
        if (factor2 == null) {
            return factor1;
        }
        Tree result;
        boolean isCmplx1 = isCallingString(factor1, "cmplx", false);
        boolean isCmplx2 = isCallingString(factor2, "cmplx", false);
        // Fuse two Complex constructors only if they have the same optional arguments:
        boolean bothComplexConstructors = false;
        if (isCmplx1 && isCmplx2) {
            bothComplexConstructors = true;
            Tree[] args1 = getArguments(factor1).children();
            Tree[] args2 = getArguments(factor2).children();
            int i = 2;
            while (bothComplexConstructors && i < args1.length && i < args2.length) {
                bothComplexConstructors = args1[i].equalsTree(args2[i]);
                ++i;
            }
            if (i < args1.length || i < args2.length) {
                bothComplexConstructors = false;
            }
        }
        if (bothComplexConstructors) {
            result = buildCall(
                    build(ILLang.op_ident, "cmplx"),
                    build(ILLang.op_expressions,
                            addTree(getArguments(factor1).cutChild(1),
                                    getArguments(factor2).cutChild(1)),
                            addTree(getArguments(factor1).cutChild(2),
                                    getArguments(factor2).cutChild(2))));
            copyExtraChildren(getArguments(factor1), 2, getArguments(result), 2);
        } else {
            result = addTree(factor1, factor2);
        }
        return result;
    }

    /**
     * Copies all children Tree's of "srcTree", up from rank "srcIndex",
     * adding them to the children of (ListTree) "destListTree", starting at rank "destIndex".
     * For both Trees, first child correspond to index 0.
     */
    private static void copyExtraChildren(Tree srcTree, int srcIndex, Tree destListTree, int destIndex) {
        Tree[] srcTreeArray = srcTree.children();
        for (int i = srcTreeArray.length - 1; i >= srcIndex; --i) {
            destListTree.addChild(srcTreeArray[i], destIndex + 1);
        }
    }

    private static boolean testImpliesTest(Tree test1, Tree test2) {
        if (test2 == null || test2.equalsTree(test1)) {
            return true;
        } else if (test1 == null) {
            return false;
        } else if (test1.opCode() == ILLang.op_and) {
            return testImpliesTest(test1.down(1), test2)
                    || testImpliesTest(test1.down(2), test2);
        } else if (test1.opCode() == ILLang.op_not) {
            return notTestImpliesTest(test1.down(1), test2);
        } else if (test2.opCode() == ILLang.op_or) {
            return testImpliesTest(test1, test2.down(1))
                    || testImpliesTest(test1, test2.down(2));
        } else if (test2.opCode() == ILLang.op_not) {
            return notTestImpliesTest(test2.down(1), test1);
        } else {
            return false;
        }
    }

    private static boolean notTestImpliesTest(Tree test1, Tree test2) {
        if (test1 == null) {
            return true;
        } else if (test2 == null || test1.equalsTree(test2)) {
            return false;
        } else if (test1.opCode() == ILLang.op_or) {
            return notTestImpliesTest(test1.down(1), test2)
                    || notTestImpliesTest(test1.down(2), test2);
        } else if (test1.opCode() == ILLang.op_not) {
            return testImpliesTest(test1.down(1), test2);
        } else if (test2.opCode() == ILLang.op_or) {
            return notTestImpliesTest(test1, test2.down(1))
                    || notTestImpliesTest(test1, test2.down(2));
        } else if (test2.opCode() == ILLang.op_not) {
            return testImpliesTest(test2.down(1), test1);
        } else {
            return false;
        }
    }

    private static boolean testImpliesNotTest(Tree test1, Tree test2) {
        return notTestImpliesTest(test2, test1);
    }

    private static boolean notTestImpliesNotTest(Tree test1, Tree test2) {
        return testImpliesTest(test2, test1);
    }

    public static Tree incrementByProtectedExpr(int language, Tree lhs, Tree factor, boolean needAtomic) {
        if (isIfExpression(factor)) {
            Tree expr1 = factor.cutChild(2);
            if (isNullOrNone(expr1) || isZero(expr1)) {
                expr1 = null;
            }
            Tree expr2 = factor.cutChild(3);

            if (expr1 == null) {
                return build(ILLang.op_if,
                        build(ILLang.op_not, factor.cutChild(1)),
                        incrementByProtectedExpr(language, lhs, expr2, needAtomic),
                        build(ILLang.op_none));
            } else {
                if (isNullOrNone(expr2) || isZero(expr2)) {
                    expr2 = null;
                }
                return build(ILLang.op_if,
                        factor.cutChild(1),
                        incrementByProtectedExpr(language, copy(lhs), expr1, needAtomic),
                        expr2 == null
                                ? build(ILLang.op_none)
                                : incrementByProtectedExpr(language, lhs, expr2, needAtomic));
            }
        } else {
            if (needAtomic && TapEnv.doOpenMP()) {
                return build(ILLang.op_atomicStatement,
                         build(ILLang.op_ident, "OMP"),
                         build(ILLang.op_expressions,
                           build(ILLang.op_ident, "update")),
                         build(ILLang.op_assign,
                           lhs,
                           buildSmartAddSub(copy(lhs), 1, factor)));
            } else if (needAtomic && TapEnv.relatedUnit()!=null && TapEnv.relatedUnit().callGraph().hasCuda) {
                Tree assignment = build(ILLang.op_assign, lhs, buildSmartAddSub(copy(lhs), 1, factor));
                assignment.setAnnotation("preComments",
                                         build(ILLang.op_stringCst, "Must be atomic increment")) ;
                return assignment ;
            } else {
                return build(ILLang.op_assign, lhs, buildSmartAddSub(copy(lhs), 1, factor));
            }
        }
    }

    public static Tree setToProtectedExpr(int language, Tree lhs, Tree rhs, TypeSpec assignedType) {
        if (isIfExpression(rhs)) {
            Tree thenExpr = rhs.cutChild(2);
            Tree elseExpr = rhs.cutChild(3);
            Tree zeroTree;
            if (assignedType != null && (assignedType.isRealOrComplexBase() || assignedType.isIntegerBase())) {
                zeroTree = assignedType.buildConstantZero();
            } else {
                zeroTree = build(ILLang.op_realCst, "0.0");
            }
            if (isNullOrNone(thenExpr)) {
                thenExpr = copy(zeroTree);
            }
            if (isNullOrNone(elseExpr)) {
                elseExpr = copy(zeroTree);
            }
            if (TapEnv.isC(language)) {
                return build(ILLang.op_assign,
                        lhs,
                        build(ILLang.op_ifExpression, rhs.cutChild(1),
                                thenExpr, elseExpr));
            } else {
                return build(ILLang.op_if, rhs.cutChild(1),
                        setToProtectedExpr(language, lhs, thenExpr, assignedType),
                        setToProtectedExpr(language, copy(lhs), elseExpr, assignedType));
            }
        } else {
            return build(ILLang.op_assign, lhs, rhs);
        }
    }

    public static Tree minusTree(Tree tree) {
        if (tree == null) {
            return build(ILLang.op_intCst, -1);
        } else if (tree.opCode() == ILLang.op_minus) {
            return tree.cutChild(1);
        } else if (tree.opCode() == ILLang.op_intCst) {
            return build(ILLang.op_intCst,
                    -tree.intValue());
        } else {
            return build(ILLang.op_minus, tree);
        }
    }

    public static Tree addTree(Tree tree1, Tree tree2) {
        Tree result = buildSmartAddSub(tree1, 1, tree2);
        if (result == null) {
            return build(ILLang.op_intCst, 0);
        }
        return result;
    }

    public static Tree subTree(Tree tree1, Tree tree2) {
        Tree result = buildSmartAddSub(tree1, -1, tree2);
        if (result == null) {
            return build(ILLang.op_intCst, 0);
        }
        return result;
    }

    public static Tree mulTree(Tree tree1, Tree tree2) {
        if (tree1 == null) {
            return tree2;
        }
        return buildSmartMulDiv(tree1, 1, tree2);
    }

    private static Tree divTree(Tree tree1, Tree tree2) {
        if (tree2 == null) {
            return tree1;
        }
        if (tree1 == null) {
            tree1 = build(ILLang.op_realCst, "1.D0");
        }
        return buildSmartMulDiv(tree1, -1, tree2);
    }

    public static Tree powerTree(Tree tree1, Tree tree2) {
        return build(ILLang.op_power, tree1, tree2);
    }

// STUFF TO IMPLEMENT AUTO-FACTORIZATION OF THE DIFFERENTIATED VARIABLE.
// for instance turn exp1*Vb +/- exp2*Vb into (exp1+/-exp2)*Vb.
// Methods: getLastDiffFactor(), findSameDiffFactor(), factorizeDiffVar()

    /**
     * @return leftTree.mod(rightTree), with simplifications.
     */
    private static Tree buildSmartMod(Tree leftTree, Tree rightTree) {
        Tree result = null;
        boolean isMoreSimple = false;
        if (isExpressionNumConstant(rightTree)) {
            int rightSign = 1;
            if (rightTree.opCode() == ILLang.op_minus) {
                rightTree = rightTree.down(1);
                rightSign = -1;
            }
            if (evalsToOne(rightTree) || evalsToMinusOne(rightTree)) {
                switch (rightTree.opCode()) {
                    case ILLang.op_realCst: {
                        result = PrimitiveTypeSpec.buildRealConstantZero();
                        isMoreSimple = true;
                        break;
                    }
                    case ILLang.op_intCst: {
                        result = build(ILLang.op_intCst, 0);
                        isMoreSimple = true;
                        break;
                    }
                    default:
                        break;
                }
            } else if (isExpressionNumConstant(leftTree)) {
                /*TODO : plus malin */
                int leftSign = 1;
                if (leftTree.opCode() == ILLang.op_minus) {
                    leftTree = leftTree.down(1);
                    leftSign = -1;
                }
                if
                (rightTree.opCode() == ILLang.op_intCst
                        && leftTree.opCode() == ILLang.op_intCst) {
                    result =
                            build(
                                    ILLang.op_intCst,
                                    leftSign * leftTree.intValue()
                                            % (rightSign * rightTree.intValue()));
                    isMoreSimple = true;
                }
            }
        }
        if (!isMoreSimple) {
            result = build(ILLang.op_mod, leftTree, rightTree);
        }
        return result;
    }

    /**
     * @return a Tree that adds or substracts the two operands, with simplifications.
     */
    public static Tree buildSmartAddSub(Tree leftTree, int factor, Tree rightTree) {
        TapList<Tree> toTerms = new TapList<>(null, null);
        TapIntList toFactors = new TapIntList(1, null);
        if (leftTree != null) {
            addTermsFrom(leftTree, 1, toTerms, toFactors);
        }
        if (rightTree != null) {
            addTermsFrom(rightTree, factor, toTerms, toFactors);
        }
        // Put the constant at the end, if any:
        if (toTerms.head != null && !evalsToZero(toTerms.head)) {
            toTerms.newR(toTerms.head);
            toFactors.newR(1);
        }
        toTerms = toTerms.tail;
        toFactors = toFactors.tail;
        if (toTerms == null) {
            return null;
        }
        // if possible, make sure there is a positive term first:
        if (toFactors.head <= 0) {
            TapList<Tree> inTerms = toTerms.tail;
            TapIntList inFactors = toFactors.tail;
            while (inFactors != null && inFactors.head <= 0) {
                inTerms = inTerms.tail;
                inFactors = inFactors.tail;
            }
            if (inFactors != null) {
                Tree positiveTerm = inTerms.head;
                int positiveFactor = inFactors.head;
                inTerms.head = toTerms.head;
                inFactors.head = toFactors.head;
                toTerms.head = positiveTerm;
                toFactors.head = positiveFactor;
            }
        }
        // build the result Tree:
        Tree result = null;
        Tree term;
        int repetition;
        while (toTerms != null) {
            term = toTerms.head;
            factor = toFactors.head;
            if (isNegativeExpression(term)) {
                factor = -factor;
                term = buildNumericalOpposite(term);
            }
            Tree lastDiffRef = getLastDiffFactor(term);
            repetition = 1;
//             // This replaces multiplications by integers by repeated additions:
//             if (factor<-1)
//                 {repetition=-factor; factor = -1;}
//             else if (factor>1)
//                 {repetition=factor; factor = 1;}
//             // end repeated additions
            for (int i = repetition; i > 0; i--) {
                if (term.parent() != null) {
                    term = copy(term);
                }
                if (factor != 0) {
                    if (result == null) {
                        if (factor == -1) {
                            result = buildExpressionOpposite(term);
                        } else if (factor == 1) {
                            result = term;
                        } else {
                            result = mulLeftmost(build(ILLang.op_intCst, factor), term);
                        }
                    } else {
                        ToInt childRk = new ToInt(0);
                        Tree parentOfMatch = findSameDiffFactor(lastDiffRef, result, childRk, true);
                        if (parentOfMatch != null) {
                            // Factorize exp1*Vb +/- exp2*Vb into (exp1+/-exp2)*Vb
                            result = factorizeDiffVar(result, parentOfMatch, childRk.get(),
                                    term, factor);
                        } else {
                            // Fallback case: just add new factor*term
                            if (factor < -1) {
                                result = build(ILLang.op_sub,
                                        result,
                                        mulLeftmost(build(ILLang.op_intCst, -factor), term));
                            } else if (factor == -1) {
                                result =
                                        build(ILLang.op_sub, result, term);
                            } else if (factor == 1) {
                                result =
                                        build(ILLang.op_add, result, term);
                            } else if (term.opCode() == ILLang.op_realCst && "1.0".equals(term.stringValue()))
                            // Special 2*1.0(introduced by factorizeDiffVar) -> 2
                            {
                                result = build(ILLang.op_add,
                                        result,
                                        build(ILLang.op_intCst, factor));
                            } else {
                                result = build(ILLang.op_add,
                                        result,
                                        mulLeftmost(build(ILLang.op_intCst, factor), term));
                            }
                        }
                    }
                }
            }
            toTerms = toTerms.tail;
            toFactors = toFactors.tail;
        }
        return result;
    }

    // assume term is a "left comb", so the last factor is simply the 2nd child.
    private static Tree getLastDiffFactor(Tree term) {
        term = lastFactor(term);
        if (isNullOrNone(term)
                || !isAVarRef(term, null)
                || !isADiffRef(term)) // optional, to restrict factorization to diff variables.
        {
            term = null;
        }
        return term;
    }

    /**
     * @return null if no sub multiplicative-term matches.
     */
    private static Tree findSameDiffFactor(Tree searched, Tree expr, ToInt childRk, boolean top) {
        if (searched == null || expr == null) {
            return null;
        }
        if (top && sameDiffRef(searched, lastFactor(expr))) {
            childRk.set(0);
            return expr;
        }
        if (expr.opCode() == ILLang.op_add || expr.opCode() == ILLang.op_sub) {
            if (sameDiffRef(searched, lastFactor(expr.down(2)))) {
                childRk.set(2);
                return expr;
            }
        }
        if (expr.opCode() == ILLang.op_add || expr.opCode() == ILLang.op_sub || expr.opCode() == ILLang.op_minus) {
            if (sameDiffRef(searched, lastFactor(expr.down(1)))) {
                childRk.set(1);
                return expr;
            }
            return findSameDiffFactor(searched, expr.down(1), childRk, false);
        }
        return null;
    }

    private static Tree lastFactor(Tree term) {
        if (term.opCode() == ILLang.op_div) {
            term = term.down(1);
        }
        return term.opCode() == ILLang.op_mul ? term.down(2) : term;
    }

    private static boolean isADiffRef(Tree ref) {
        switch (ref.opCode()) {
            case ILLang.op_ident:
                return ref.getAnnotation("NewSymbolHolder") != null;
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return isADiffRef(ref.down(1));
            default:
                return false;
        }
    }

// END OF STUFF TO IMPLEMENT AUTO-FACTORIZATION.

    private static boolean sameDiffRef(Tree ref1, Tree ref2) {
        switch (ref1.opCode()) {
            case ILLang.op_ident:
                return ref1.equalsTree(ref2) &&
                        ref1.getAnnotation("NewSymbolHolder") == ref2.getAnnotation("NewSymbolHolder");
            case ILLang.op_arrayAccess:
            case ILLang.op_fieldAccess:
            case ILLang.op_pointerAccess:
                return ref2.opCode() == ref1.opCode() &&
                        ref1.down(2).equalsTree(ref2.down(2)) &&
                        sameDiffRef(ref1.down(1), ref2.down(1));
            default:
                return false;
        }
    }

    // rankInParent==0 implies parentOfMatch==fullExpr,
    // therefore the returned Tree shall be the new full Expression.
    private static Tree factorizeDiffVar(Tree fullExpr, Tree parentOfMatch, int rankInParent,
                                         Tree term, int factor) {
        Tree subExpr = parentOfMatch;
        if (parentOfMatch.opCode() == ILLang.op_add) {
            subExpr = parentOfMatch.down(rankInParent);
        } else if (parentOfMatch.opCode() == ILLang.op_sub) {
            subExpr = parentOfMatch.down(rankInParent);
            if (rankInParent == 2) {
                factor = -factor;
            }
        } else if (parentOfMatch.opCode() == ILLang.op_minus) {
            subExpr = parentOfMatch.down(1);
            factor = -factor;
        }
        // Now add factor*term into subExpr, yielding modified subExpr:
        Tree divisor = null;
        boolean subExprWasDiv = false;
        if (subExpr.opCode() == ILLang.op_div) {
            subExprWasDiv = true;
            divisor = subExpr.cutChild(2);
            subExpr = subExpr.cutChild(1);
        }
        Tree origCoeff = subExpr.opCode() == ILLang.op_mul ? subExpr.cutChild(1) : build(ILLang.op_realCst, "1.0");
        if (divisor != null) {
            origCoeff = build(ILLang.op_div, origCoeff, divisor);
        }
        divisor = null;
        if (term.opCode() == ILLang.op_div) {
            divisor = term.cutChild(2);
            term = term.cutChild(1);
        }
        Tree addedCoeff = term.opCode() == ILLang.op_mul ? term.cutChild(1) : build(ILLang.op_realCst, "1.0");
        if (divisor != null) {
            addedCoeff = build(ILLang.op_div, addedCoeff, divisor);
        }
        Tree newCoeff = buildSmartAddSub(origCoeff, factor, addedCoeff);
        if (subExpr.opCode() == ILLang.op_mul && newCoeff != null && !subExprWasDiv) {
            subExpr.setChild(newCoeff, 1);
            return fullExpr;
        } else {
            Tree newSubExpr = null;
            if (newCoeff != null) {
                newSubExpr = build(ILLang.op_mul, newCoeff, subExpr.opCode() == ILLang.op_mul ? subExpr.cutChild(2) : subExpr);
            }
            if (rankInParent != 0) {
                parentOfMatch.setChild(newSubExpr, rankInParent);
                return fullExpr;
            } else {
                return newSubExpr;
            }
        }
    }

    /**
     * @return a Tree for "factor*term", built to avoid parentheses
     * around term, by adding factor as the lowest leftmost leaf of term.
     * This method may modify the "term" and may return parts of it.
     */
    private static Tree mulLeftmost(Tree factor, Tree term) {
        switch (term.opCode()) {
            case ILLang.op_mul:
            case ILLang.op_div:
                term.setChild(mulLeftmost(factor, term.cutChild(1)), 1);
                return term;
            default:
                return build(ILLang.op_mul, factor, term);
        }
    }

    private static void addTermsFrom(Tree tree, int factor,
                                     TapList<Tree> toTerms, TapIntList toFactors) {
        switch (tree.opCode()) {
            case ILLang.op_add:
                addTermsFrom(tree.down(1), factor, toTerms, toFactors);
                addTermsFrom(tree.down(2), factor, toTerms, toFactors);
                break;
            case ILLang.op_sub:
                addTermsFrom(tree.down(1), factor, toTerms, toFactors);
                addTermsFrom(tree.down(2), -factor, toTerms, toFactors);
                break;
            case ILLang.op_minus:
                addTermsFrom(tree.down(1), -factor, toTerms, toFactors);
                break;
            case ILLang.op_mul:
                if (isExpressionIntConstant(tree.down(1))) {
                    addTermsFrom(
                            tree.down(2), factor * intConstantValue(tree.down(1)),
                            toTerms, toFactors);
                } else if (isExpressionIntConstant(tree.down(2))) {
                    addTermsFrom(
                            tree.down(1), factor * intConstantValue(tree.down(2)),
                            toTerms, toFactors);
                } else {
                    addOneTerm(tree, factor, toTerms, toFactors);
                }
                break;
            case ILLang.op_intCst:
            case ILLang.op_realCst:
                if (isNegativeExpression(tree)) {
                    factor = -factor;
                    tree = buildNumericalOpposite(tree);
                }
                assert tree != null;
                if ((factor < -1 || factor > 1)
                        && tree.opCode() == ILLang.op_realCst) {
                    // So far, we don't know how to multiply real constants:
                    addOneTerm(tree, factor, toTerms, toFactors);
                } else {
                    if (factor == -1
                            && tree.opCode() == ILLang.op_realCst) {
                        tree = buildNumericalOpposite(tree);
                    } else if (factor != 1
                            && tree.opCode() == ILLang.op_intCst) {
                        tree = build(ILLang.op_intCst, factor * tree.intValue());
                    }
                    factor = 1;
                    if (toTerms.head == null) {
                        toTerms.head = tree;
                    } else {
                        toTerms.head = addExpressionsNumConstants(toTerms.head, tree);
                    }
                }
                break;
            default:
                addOneTerm(tree, factor, toTerms, toFactors);
                break;
        }
    }

    /** Adds term "factor"*"tree" into the list of terms "toFactors"*"toTerms". */
    private static void addOneTerm(Tree tree, int factor,
                                   TapList<Tree> toTerms, TapIntList toFactors) {
        if (!evalsToZero(tree)) {
            while (toTerms.tail != null
                    && !equalValues(toTerms.tail.head, tree, null, null)) {
                toTerms = toTerms.tail;
                toFactors = toFactors.tail;
            }
            if (toTerms.tail == null) {
                toTerms.tail = new TapList<>(tree, null);
                toFactors.tail = new TapIntList(0, null);
            }
            toFactors.tail.head = toFactors.tail.head + factor;
        }
    }

    /**
     * @return a Tree that multiplies (power==1) or divides (power==-1)
     * the two operands, with simplifications.
     */
    public static Tree buildSmartMulDiv(Tree leftTree, int power,
                                        Tree rightTree) {
        TapList<Tree> toTerms = new TapList<>(null, null);
        TapIntList toPowers = new TapIntList(0, null);
        ToBool positive = new ToBool(true);
        Tree nextTerm;
        Tree numerator;
        Tree denominator;
        Tree result;
        int nextPower;
        if (power == -1 && evalsToZero(rightTree)) {
            TapEnv.toolError("Building a division by zero!");
            return build(ILLang.op_div, leftTree,
                    PrimitiveTypeSpec.buildRealConstantZero());
        }
        if (evalsToZero(leftTree) || evalsToZero(rightTree)) {
            return PrimitiveTypeSpec.buildRealConstantZero();
        }
        mulTermsFrom(leftTree, 1, toTerms, toPowers, positive);
        mulTermsFrom(rightTree, power, toTerms, toPowers, positive);
        toTerms = toTerms.tail;
        toPowers = toPowers.tail;
        numerator = null;
        denominator = null;
        int repetition;
        Tree newTree;
        Tree newPowerTree;
        int positivePower;
        while (toTerms != null) {
            nextTerm = toTerms.head;
            nextPower = toPowers.head;
            repetition = 1;
//              // This replaces exponentiation by integers by repeated multiplications:
//              if (nextPower<-1)
//              {repetition=-nextPower; nextPower = -1;}
//              else if (nextPower>1)
//              {repetition=nextPower; nextPower = 1;}
//              // end repeated multiplications.
            for (int i = repetition; i > 0; --i) {
                if (nextPower != 0) {
                    if (nextTerm.parent() != null) {
                        nextTerm = copy(nextTerm);
                    }
                    newTree = nextPower > 0 ? numerator : denominator;
                    positivePower = nextPower > 0 ? nextPower : -nextPower;
                    if (positivePower == 1) {
                        newPowerTree = nextTerm;
                    } else {
                        newPowerTree = build(ILLang.op_power, nextTerm,
                                build(ILLang.op_intCst, positivePower));
                    }
                    if (newTree == null) {
                        newTree = newPowerTree;
                    } else {
                        if (newPowerTree.parent() != null) {
                            newPowerTree = copy(newPowerTree);
                        }
                        newTree = build(ILLang.op_mul, newTree, newPowerTree);
                    }
                    if (nextPower > 0) {
                        numerator = newTree;
                    } else {
                        denominator = newTree;
                    }
                }
            }
            toTerms = toTerms.tail;
            toPowers = toPowers.tail;
        }
        if (numerator == null) {
            if (leftTree.opCode() == ILLang.op_realCst
                    || rightTree.opCode() == ILLang.op_realCst) {
                numerator = build(ILLang.op_realCst, "1.0");
            } else {
                numerator = build(ILLang.op_intCst, 1);
            }
        }
        if (denominator == null) {
            result = numerator;
        } else {
            result = build(ILLang.op_div, numerator, denominator);
        }
        if (!positive.get()) {
            result = build(ILLang.op_minus, result);
        }
        return result;
    }

//   end all about numerical operations on trees.
// begin all about building an IL Tree of given operator and sons:

    private static void mulTermsFrom(Tree tree, int power, TapList<Tree> toTerms,
                                     TapIntList toPowers, ToBool positive) {
        switch (tree.opCode()) {
            case ILLang.op_mul:
                mulTermsFrom(tree.down(1), power, toTerms, toPowers, positive);
                mulTermsFrom(tree.down(2), power, toTerms, toPowers, positive);
                break;
            case ILLang.op_div:
                mulTermsFrom(tree.down(1), power, toTerms, toPowers, positive);
                mulTermsFrom(tree.down(2), -power, toTerms, toPowers, positive);
                break;
            case ILLang.op_minus:
                if (power % 2 == 1 || power < 0 && -power % 2 == 1) {
                    positive.not();
                }
                mulTermsFrom(tree.down(1), power, toTerms, toPowers, positive);
                break;
            case ILLang.op_power:
                if (isExpressionIntConstant(tree.down(2))) {
                    mulTermsFrom(
                            tree.down(1), power * intConstantValue(tree.down(2)),
                            toTerms, toPowers, positive);
                } else {
                    mulOneTerm(tree, power, toTerms, toPowers);
                }
                break;
            case ILLang.op_intCst:
            case ILLang.op_realCst:
                if (isNegativeExpression(tree)) {
                    positive.not();
                    tree = buildNumericalOpposite(tree);
                }
                mulOneTerm(tree, power, toTerms, toPowers);
                break;
            default:
                mulOneTerm(tree, power, toTerms, toPowers);
                break;
        }
    }

    private static void mulOneTerm(Tree tree, int power,
                                   TapList<Tree> toTerms, TapIntList toPowers) {
        if (!evalsToOne(tree)) {
            while (toTerms.tail != null
                    && !equalValues(toTerms.tail.head, tree, null, null)) {
                toTerms = toTerms.tail;
                toPowers = toPowers.tail;
            }
            if (toTerms.tail == null) {
                toTerms.tail = new TapList<>(tree, null);
                toPowers.tail = new TapIntList(0, null);
            }
            toPowers.tail.head = toPowers.tail.head + power;
        }
    }

    public static Tree build(int opRank, int value) {
        Tree result = ILLangOps.ops(opRank).tree();
        result.setValue(value);
        return result;
    }

    public static Tree build(int opRank, String value) {
        Tree result = ILLangOps.ops(opRank).tree();
        result.setValue(value);
        return result;
    }

    public static Tree build(int opRank) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        int sonRk = 0;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        int sonRk = 1;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        int sonRk = 2;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2, Tree son3) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        int sonRk = 3;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2,
                             Tree son3, Tree son4) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        changeOrAdoptSon(result, son4, 4, sonsNb);
        int sonRk = 4;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2,
                             Tree son3, Tree son4, Tree son5) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        changeOrAdoptSon(result, son4, 4, sonsNb);
        changeOrAdoptSon(result, son5, 5, sonsNb);
        int sonRk = 5;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2,
                             Tree son3, Tree son4, Tree son5,
                             Tree son6) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        changeOrAdoptSon(result, son4, 4, sonsNb);
        changeOrAdoptSon(result, son5, 5, sonsNb);
        changeOrAdoptSon(result, son6, 6, sonsNb);
        int sonRk = 6;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2,
                             Tree son3, Tree son4, Tree son5,
                             Tree son6, Tree son7) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        changeOrAdoptSon(result, son4, 4, sonsNb);
        changeOrAdoptSon(result, son5, 5, sonsNb);
        changeOrAdoptSon(result, son6, 6, sonsNb);
        changeOrAdoptSon(result, son7, 7, sonsNb);
        int sonRk = 7;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree son1, Tree son2,
                             Tree son3, Tree son4, Tree son5,
                             Tree son6, Tree son7, Tree son8) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        changeOrAdoptSon(result, son1, 1, sonsNb);
        changeOrAdoptSon(result, son2, 2, sonsNb);
        changeOrAdoptSon(result, son3, 3, sonsNb);
        changeOrAdoptSon(result, son4, 4, sonsNb);
        changeOrAdoptSon(result, son5, 5, sonsNb);
        changeOrAdoptSon(result, son6, 6, sonsNb);
        changeOrAdoptSon(result, son7, 7, sonsNb);
        changeOrAdoptSon(result, son8, 8, sonsNb);
        int sonRk = 8;
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

//   end all about building an IL Tree of given operator and sons.
// begin all about displaying an IL Tree, including toString():

    public static Tree build(int opRank, TapList<Tree> trees) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        int sonRk = 0;
        Tree tree;
        while (trees != null) {
            tree = trees.head;
            if (tree.parent() != null) {
                tree = copy(tree);
            }
            changeOrAdoptSon(result, tree, ++sonRk, sonsNb);
            trees = trees.tail;
        }
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    public static Tree build(int opRank, Tree[] trees) {
        Tree result = ILLangOps.ops(opRank).tree();
        int sonsNb = result.children().length;
        int sonRk = 0;
        for (Tree tree : trees) {
            changeOrAdoptSon(result, tree, ++sonRk, sonsNb);
        }
        while (++sonRk <= sonsNb) {
            result.setChild(
                    ILLangOps.ops(ILLang.op_none).tree(), sonRk);
        }
        return result;
    }

    /**
     * Prints in detail the contents of the given IL tree, onto TapEnv.curOutputStream().
     * This printing may go on several lines, with indentation.
     * This printing calls toStringRec() for single-line pieces of the tree.
     *
     * @param indent the amount of indentation to be used for this printing.
     */
    public static void dump(Tree tree, int indent) throws java.io.IOException {
        dumpRec(tree, indent, "");
    }

    /**
     * Recursive method called by dump().
     *
     * @param indent        the amount of indentation to be used for this printing.
     * @param commentPrefix the prefix string that will indicate a comment.
     */
    private static void dumpRec(Tree tree, int indent, String commentPrefix) throws java.io.IOException {
        Tree comments = tree.getAnnotation("preCommentsBlock");
        if (comments != null) {
            dumpRec(comments, indent, "//[preB on " + tree.opName() + "] ");
            TapEnv.println();
            TapEnv.indent(indent);
        }
        comments = tree.getAnnotation("preComments");
        if (comments != null) {
            dumpRec(comments, indent, "//[pre on " + tree.opName() + "] ");
            TapEnv.println();
            TapEnv.indent(indent);
        }
        switch (tree.opCode()) {
            case ILLang.op_file: // op_file obsolescent...
                for (int i = 1; i <= tree.length(); i++) {
                    dumpRec(tree.down(i), indent, commentPrefix);
                    TapEnv.println();
                    TapEnv.println();
                    TapEnv.indent(indent);
                }
                break;
            case ILLang.op_class:
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(" class ");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print(" parents:");
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print(" ");
                if (isNullOrNoneOrEmptyList(tree.down(4))) {
                    TapEnv.print("{}");
                } else {
                    dumpRec(tree.down(4), indent + 2, commentPrefix);
                }
                break;
            case ILLang.op_declarations:
            case ILLang.op_blockStatement:
                TapEnv.print("{");
                TapEnv.println();
                for (int i = 1; i <= tree.length(); i++) {
                    TapEnv.indent(indent + 2);
                    dumpRec(tree.down(i), indent + 2, commentPrefix);
                    TapEnv.print(";");
                    TapEnv.println();
                }
                TapEnv.indent(indent);
                TapEnv.print("}");
                break;
            case ILLang.op_function:
                dumpRec(tree.down(1), indent, commentPrefix);
                dumpRec(tree.down(2), indent, commentPrefix);
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print(" function ");
                dumpRec(tree.down(4), indent, commentPrefix);
                TapEnv.print("(");
                dumpRec(tree.down(5), indent, commentPrefix);
                TapEnv.print(") ");
                if (tree.down(4).getAnnotation("explicitReturnVar") != null) {
                    TapEnv.print(" explicitReturnVar (" + toString(tree.down(4).getAnnotation("explicitReturnVar")) + ") ");
                }
                if (tree.down(4).getAnnotation("bind") != null) {
                    TapEnv.print(" bind (" + toString(tree.down(4).getAnnotation("bind")) + ") ");
                }
                dumpRec(tree.down(6), indent, commentPrefix);
                break;
            case ILLang.op_program:
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print("program ");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print("(");
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print(") ");
                dumpRec(tree.down(4), indent, commentPrefix);
                break;
            case ILLang.op_loop:
                TapEnv.print("loop [");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print("][");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print("] ");
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print(" ");
                dumpRec(tree.down(4), indent, commentPrefix);
                break;
            case ILLang.op_parallelRegion:
                TapEnv.print("parallel region [");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print("] ");
                dumpRec(tree.down(2), indent, commentPrefix);
                dumpRec(tree.down(3), indent, commentPrefix);
                break;
            case ILLang.op_parallelLoop:
                TapEnv.print("parallel loop [");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print("] ");
                dumpRec(tree.down(2), indent, commentPrefix);
                dumpRec(tree.down(3), indent, commentPrefix);
                break;
            case ILLang.op_if:
                TapEnv.print("if (");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(") then ");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print(" else ");
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print(" endif");
                break;
            case ILLang.op_switch:
            case ILLang.op_switchType: {
                TapEnv.print("switch on "+(tree.opCode()==ILLang.op_switchType ? "type of " : "")+"(");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(") ") ;
                dumpRec(tree.down(2), indent, commentPrefix);
                break ;
            }
            case ILLang.op_switchCases: {
                TapEnv.print("{");
                TapEnv.println();
                for (int i = 1; i <= tree.length(); i++) {
                    TapEnv.indent(indent+2);
                    dumpRec(tree.down(i), indent+2, commentPrefix);
                    TapEnv.println();
                }
                TapEnv.indent(indent);
                TapEnv.print("}");
                break ;
            }
            case ILLang.op_switchCase: {
                TapEnv.print("case (") ;
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(") ") ;
                dumpRec(tree.down(2), indent, commentPrefix);
                break ;
            }
            case ILLang.op_typeCase: {
                TapEnv.print("case type is ") ;
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(" ") ;
                dumpRec(tree.down(2), indent, commentPrefix);
                break ;
            }
            case ILLang.op_recordType:
                TapEnv.print("record");
                if (tree.down(1).opCode() != ILLang.op_none) {
                    TapEnv.print("<");
                    dumpRec(tree.down(1), indent, commentPrefix);
                    TapEnv.print(">");
                }
                if (tree.down(2).opCode() != ILLang.op_none) {
                    TapEnv.print("[");
                    dumpRec(tree.down(2), indent, commentPrefix);
                    TapEnv.print("[");
                }
                TapEnv.print("{");
                dumpRec(tree.down(3), indent, commentPrefix);
                TapEnv.print("}");
                break;
            case ILLang.op_unionType:
                TapEnv.print("union");
                if (tree.down(1).opCode() != ILLang.op_none) {
                    TapEnv.print("<");
                    dumpRec(tree.down(1), indent, commentPrefix);
                    TapEnv.print(">");
                }
                TapEnv.print("{");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print("}");
                break;
            case ILLang.op_comments:
            case ILLang.op_commentsBlock: {
                String blockPrefix = tree.opCode() == ILLang.op_commentsBlock ? "[Block]" : "[]";
                for (int i = 1; i <= tree.length(); ++i) {
                    TapEnv.print(commentPrefix + blockPrefix + toStringRec(tree.down(i), true, false, 0, TapEnv.C, null));
                    if (i < tree.length()) {
                        TapEnv.println();
                        TapEnv.indent(indent);
                    }
                }
                break;
            }
// [llh 27/08/2014] TODO The following cases should also have a corresponding in toStringRec() :

            case ILLang.op_common:
                TapEnv.print("common ");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(" ");
                dumpRec(tree.down(2), indent, commentPrefix);
                break;
            case ILLang.op_equivalence:
                TapEnv.print("equivalence ");
                for (int i = 1; i <= tree.length(); i++) {
                    TapEnv.print("(");
                    dumpRec(tree.down(i), indent, commentPrefix);
                    TapEnv.print(") ");
                }
                break;
            case ILLang.op_modifiedType:
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print("*");
                dumpRec(tree.down(1), indent, commentPrefix);
                break;
            case ILLang.op_typeDeclaration:
                TapEnv.print("typedef ");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(":");
                dumpRec(tree.down(2), indent, commentPrefix);
                break;
            case ILLang.op_constDeclaration:
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(" constant ");
                dumpRec(tree.down(2), indent, commentPrefix);
                break;
            case ILLang.op_arrayType:
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print("[");
                dumpRec(tree.down(2), indent, commentPrefix);
                TapEnv.print("]");
                break;
            case ILLang.op_nameSpace:
                TapEnv.print("namespace ");
                dumpRec(tree.down(1), indent, commentPrefix);
                TapEnv.print(":");
                dumpRec(tree.down(2), indent, commentPrefix);
                break;
            default:
                TapEnv.print(toStringRec(tree, true, false, 0, TapEnv.C, null));
        }
        comments = tree.getAnnotation("postComments");
        if (comments != null) {
            TapEnv.println();
            TapEnv.indent(indent);
            dumpRec(comments, indent, "//[post on " + tree.opName() + "] ");
        }
        comments = tree.getAnnotation("postCommentsBlock");
        if (comments != null) {
            TapEnv.println();
            TapEnv.indent(indent);
            dumpRec(comments, indent, "//[postB on " + tree.opName() + "] ");
        }
    }

    public static String toString(Tree tree) {
        return toStringRec(tree, true, true, 0, TapEnv.C, null);
    }

    public static String toString(Tree tree, int language) {
        return toStringRec(tree, true, true, 0, language, null);
    }

    public static String toString(Tree tree, int language, boolean withIndices) {
        return toStringRec(tree, withIndices, true, 0, language, null);
    }

    public static String toString(Tree tree, int language, ActivityPattern pattern) {
        return toStringRec(tree, true, true, 0, language, pattern);
    }

    public static String toString(Tree tree, ActivityPattern pattern) {
        return toStringRec(tree, true, true, 0, TapEnv.C, pattern);
    }

    /**
     * @param withTopComments when true, also print the comments attached to the top of the tree
     * @return a String that displays the given tree. The given tree is assumed to
     * remain on only one line, which means that the given tree may not be a multi-line
     * tree such as a list of statements, a procedure, a module, etc.
     * To display the tree on several lines, use dump() instead.
     * The returned tree String is also decorated with comments and annotations about
     * type and vectorial dimensions. It may also be decorated with the activeMark
     * put <b>after</b> each tree element which is active (i.e. has a derivative).
     * This activity decoration is triggered by setting the activeMark to a nonempty String.
     */
    private static String toStringRec(Tree tree, boolean withIndices, boolean withTopComments,
                                      int requiredLevel, int language, ActivityPattern pattern) {
        String result;
        int thisLevel = 100;
        if (tree == null) {
            return "null";
        }
        // String of all sorts of additional info that must be printed about the given "tree".
        // Will be printed at location "most representative" of the tree, e.g. "X{moreInfo}+{moreInfo}Y{moreInfo}"
        String moreInfo = "";
        if (pattern != null && ADActivityAnalyzer.isAnnotatedActive(pattern, tree, null)) {
            moreInfo = moreInfo + TapEnv.activeMark();
        }
        if (pattern != null && ReqExplicit.isAnnotatedPointerActive(pattern, tree)) {
            moreInfo = moreInfo + TapEnv.pointerActiveMark();
        }
        // uncomment when debugging cutCosts in ExpressionDifferentiator:
//         moreInfo = moreInfo+ExpressionDifferentiator.cutCostsString(tree) ;

        switch (tree.opCode()) {
            case ILLang.op_ident:
            case ILLang.op_intCst:
            case ILLang.op_realCst:
            case ILLang.op_boolCst:
            case ILLang.op_bitCst:
                result = tree.stringValue() + moreInfo;
                break;
            case ILLang.op_stringCst:
                result = "\"" + tree.stringValue() + "\"" + moreInfo;
                break;
            case ILLang.op_letter:
                result = "'" + tree.stringValue() + "'" + moreInfo;
                break;
            case ILLang.op_include:
                result = "include " + tree.stringValue() + moreInfo;
                break;
            case ILLang.op_metavar:
                result = "meta " + tree.stringValue() + moreInfo;
                break;
            case ILLang.op_labelStatement:
                result = toStringRec(tree.down(1), withIndices, true, 2, language, pattern) +
                        "      " + toStringRec(tree.down(2), withIndices, true, 2, language, pattern);
                break;
            case ILLang.op_nameEq:
            case ILLang.op_assign:
                thisLevel = 1;
                result =
                        toStringRec(tree.down(1), withIndices, true, 2, language, pattern) + "=" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 1, language, pattern);
                break;
            case ILLang.op_return:
                result = "return " + toStringRec(tree.down(1), withIndices, true, 2, language, pattern);
                if (!isNullOrNone(tree.down(2))) {
                    result = result + " (gto:" + toStringRec(tree.down(2), withIndices, true, 2, language, pattern) + ')';
                }
                break;
            case ILLang.op_pointerAssign:
                thisLevel = 1;
                result =
                        toStringRec(tree.down(1), withIndices, true, 2, language, pattern) + "=>" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 1, language, pattern);
                break;
            case ILLang.op_minus:
                thisLevel = 2;
                result = "-" + moreInfo + toStringRec(tree.down(1), withIndices, true, 3, language, pattern);
                break;
            case ILLang.op_sub:
                thisLevel = 2;
                result =
                        toStringRec(tree.down(1), withIndices, true, 2, language, pattern) + "-" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 3, language, pattern);
                break;
            case ILLang.op_add:
                thisLevel = 2;
                result =
                        toStringRec(tree.down(1), withIndices, true, 2, language, pattern) + "+" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 3, language, pattern);
                break;
            case ILLang.op_mul:
                thisLevel = 3;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + "*" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_mod:
                thisLevel = 3;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + "%" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_div:
                thisLevel = 3;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + "/" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_power:
                thisLevel = 4;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + "^" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_leftShift:
                thisLevel = 4;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + "<<" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_rightShift:
                thisLevel = 4;
                result =
                        toStringRec(tree.down(1), withIndices, true, 3, language, pattern) + ">>" + moreInfo
                                + toStringRec(tree.down(2), withIndices, true, 4, language, pattern);
                break;
            case ILLang.op_fieldAccess:
                thisLevel = 5;
                result =
                        toStringRec(tree.down(1), withIndices, true, 4, language, pattern) + "."
                                + toStringRec(tree.down(2), withIndices, true, 5, language, pattern) + moreInfo;
                break;
            case ILLang.op_scopeAccess:
                thisLevel = 5;
                result = toStringRec(tree.down(1), withIndices, true, 4, language, pattern) + "::"
                        + toStringRec(tree.down(2), withIndices, true, 5, language, pattern);
                break;
            case ILLang.op_allocate:
                if (tree.down(2)!=null && tree.down(2).opCode()==ILLang.op_stringCst) {
                    // Case of the special dummy tree built to represent the allocated chunk:
                    result = "[alloc"+toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                        +" in "+tree.down(2).stringValue()+"]" ;
                } else {
                    // Case of the allocation command itself:
                    result = "alloc(" + toStringRec(tree.down(1), withIndices, true, 0, language, pattern) +
                        (isNullOrNone(tree.down(2)) ? "" :
                                " ; " + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) +
                                        " ; " + toStringRec(tree.down(3), withIndices, true, 0, language, pattern) +
                                        " ; " + toStringRec(tree.down(4), withIndices, true, 0, language, pattern)
                        )
                        + ')';
                }
                break;
            case ILLang.op_pointerAccess:
                if (isNullOrNone(tree.down(2))) {
                    result = "*" + moreInfo + toStringRec(tree.down(1), withIndices, true, 6, language, pattern);
                } else {
                    result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "["
                            + toStringRec(tree.down(2), withIndices, true, 0, language, pattern)
                            + ']' + moreInfo;
                }
                break;
            case ILLang.op_pointerDeclarator:
                result = '*' + toStringRec(tree.down(1), withIndices, true, 6, language, pattern) + moreInfo;
                break;
            case ILLang.op_address:
                result = '&' + toStringRec(tree.down(1), withIndices, true, 6, language, pattern) + moreInfo;
                break;
            case ILLang.op_destructorCall:
                result = '~' + toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "()" + moreInfo;
                break;
	    case ILLang.op_parallelSpread: {
		result = toStringRec(tree.down(2), withIndices, true, 0, language, pattern) ;
		String sizes = "" ;
		Tree[] fanSizes = tree.down(3).children() ;
		for (int i=0 ; i<fanSizes.length ; ++i) {
		    if (i!=0) sizes = sizes+"," ;
		    sizes = sizes+toStringRec(fanSizes[i], withIndices, true, 0, language, pattern) ;
		}
		result = result+"<<<"+sizes+">>>"+moreInfo ;
		break ;
	    }
            case ILLang.op_call:
            case ILLang.op_constructorCall: {
                boolean isCall = tree.opCode() == ILLang.op_call;
                Tree obj = isCall ? getObject(tree) : tree.down(1);
                Tree func = isCall ? getCalledName(tree) : tree.down(2);
                Tree[] args = (isCall ? getArguments(tree) : tree.down(3)).children();
                result = "";
                // then recursively display the args:
                for (int i = args.length - 1; i >= 0; --i) {
                    result = toStringRec(args[i], withIndices, true, 0, language, pattern) + result;
                    if (i > 0) {
                        result = "," + result;
                    }
                }
                boolean markAsActive =
                        pattern != null && ADActivityAnalyzer.isAnnotatedActive(pattern, func, null);
                boolean markAsPointerActive =
                        pattern != null && ReqExplicit.isAnnotatedPointerActive(pattern, func);
                Tree explicitReturnVar = func.getAnnotation("explicitReturnVar");
                Tree bind = func.getAnnotation("bind");
                result = (isNullOrNone(obj) ? "" : toStringRec(obj, withIndices, true, 0, language, pattern) + ".")
                        + toStringRec(func, withIndices, true, 0, language, pattern)
                        + (markAsActive ? TapEnv.activeMark() : "") + (markAsPointerActive ? TapEnv.pointerActiveMark() : "") // +" @"+Integer.toHexString(tree.hashCode())
                        + "(" + result + ")" + moreInfo
                        + (isNullOrNone(explicitReturnVar)
                        ? ""
                        : " [explicitReturnVar:" + toStringRec(explicitReturnVar, withIndices, true, 0, language, pattern) + "] ")
                        + (isNullOrNoneOrEmptyList(bind)
                        ? ""
                        : " [bind:" + toStringRec(bind, withIndices, true, 0, language, pattern) + "] ");
                break;
            }
            case ILLang.op_arrayAccess:
            case ILLang.op_arrayDeclarator:
                result = "";
                // fortran on inverse
                if (withIndices) {
                    Tree[] subTrees = tree.down(2).children();
                    // then recursively display the subTrees:
                    if (TapEnv.isFortran(language)) {
                        for (int i = 0; i < subTrees.length; i++) {
                            result = toStringRec(subTrees[i], true, true, 0, language, pattern) + result;
                            if (i < subTrees.length - 1) {
                                result = "," + result;
                            }
                        }
                    } else {
                        for (int i = subTrees.length - 1; i >= 0; i--) {
                            result = toStringRec(subTrees[i], true, true, 0, language, pattern) + result;
                            if (i > 0) {
                                result = "," + result;
                            }
                        }
                    }
                    result = "[" + result + "]";
                }
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + result + moreInfo;
                break;
            case ILLang.op_comments: {
                result = "";
                int i;
                Tree[] subTrees = tree.children();
                for (i = subTrees.length - 1; i >= 0; i--) {
                    result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + result;
                    if (i != 0) {
                        result = "|" + result;
                    }
                }
                break;
            }
            case ILLang.op_none:
                result = "_";
                break;
            case ILLang.op_star:
                result = "*";
                break;
            case ILLang.op_arrayTriplet:
                result =
                        toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + ':'
                                + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                if (tree.down(3).opCode() != ILLang.op_none) {
                    result = result + ':' + toStringRec(tree.down(3), withIndices, true, 0, language, pattern);
                }
                break;
            case ILLang.op_iterativeVariableRef:
            case ILLang.op_forall:
                result =
                        "(" + toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + ","
                                + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ')';
                break;
            case ILLang.op_modifiers:
            case ILLang.op_expressions:
            case ILLang.op_forallRanges:
            case ILLang.op_argDeclarations:
            case ILLang.op_declarators: {
                Tree[] subTrees = tree.children();
                result = "";
                for (int i = subTrees.length - 1 ; i >= 0 ; --i) {
                    if (i!=subTrees.length-1) result = " "+result;
                    result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern)+result;
                }
                break;
            }
            case ILLang.op_varDeclarations:
            case ILLang.op_declarations:
            case ILLang.op_varDeclarators:
            case ILLang.op_dimColons: {
                Tree[] subTrees = tree.children();
                if (subTrees.length == 0) {
                    result = "";
                } else {
                    result = toStringRec(subTrees[subTrees.length - 1], withIndices, true, 0, language, pattern);
                    for (int i = subTrees.length - 2; i >= 0; --i) {
                        result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + "," + result;
                    }
                }
                break;
            }
            case ILLang.op_strings: {
                Tree[] subTrees = tree.children();
                result = "";
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    String stringi = toStringRec(subTrees[i], withIndices, true, 0, language, pattern);
                    if (i != subTrees.length - 1) {
                        result = stringi + " " + result;
                    } else {
                        result = stringi;
                    }
                }
                break;
            }
            case ILLang.op_do:
                if (tree.down(4).opCode() == ILLang.op_none) {
                    result = "";
                } else {
                    result = "," + toStringRec(tree.down(4), withIndices, true, 0, language, pattern);
                }
                result =
                        toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "="
                                + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ","
                                + toStringRec(tree.down(3), withIndices, true, 0, language, pattern) + result;
                break;
            case ILLang.op_forallRangeTriplet:
                if (tree.down(4).opCode() == ILLang.op_none) {
                    result = "";
                } else {
                    result = ":" + toStringRec(tree.down(4), withIndices, true, 0, language, pattern);
                }
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "="
                        + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ':'
                        + toStringRec(tree.down(3), withIndices, true, 0, language, pattern) + result;
                break;
            case ILLang.op_forallRangeSet:
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + " in "
                        + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_dimColon:
                result =
                        toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + ':'
                                + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_renamed:
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + " => "
                        + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_renamedVisibles: {
                Tree[] subTrees = tree.children();
                result = "";
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + " " + result;
                }
                break;
            }
            case ILLang.op_onlyVisibles: {
                Tree[] subTrees = tree.children();
                result = "";
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + " " + result;
                }
                result = "only: " + result;
                break;
            }
            case ILLang.op_blockStatement: {
                int i;
                Tree[] subTrees = tree.children();
                if (subTrees == null) {
                    result = ":" + tree.stringValue();
                } else {
                    result = "}";
                    // then recursively display the subTrees:
                    for (i = subTrees.length - 1; i >= 0; i--) {
                        result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + result;
                        if (i > 0) {
                            result = "," + result;
                        }
                    }
                    result = "{" + result;
                }
                break;
            }
            case ILLang.op_multiCast: {
                result = "(M "+toStringRec(tree.down(1), withIndices, true, 0, language, pattern)+")"
                    +toStringRec(tree.down(2), withIndices, true, 0, language, pattern) ;
                break;
            }
            case ILLang.op_arrayConstructor: {
                Tree[] subTrees = tree.children();
                result = "";
                for (int i = subTrees.length - 1; i >= 0; i--) {
                    result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + " " + result;
                }
                result = "/" + result + "/" + moreInfo;
                break;
            }
            case ILLang.op_functionDeclarator:
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "("
                        + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ')';
                break;
            case ILLang.op_varDeclaration:
                result =
                        (isNullOrNoneOrEmptyList(tree.down(1)) ? "" :
                                toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + ' ')
                                + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ' '
                                + toStringRec(tree.down(3), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_if:
                result = "if " + toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                        + " then " + toStringRec(tree.down(2), withIndices, true, 0, language, pattern)
                        + " else " + toStringRec(tree.down(3), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_ifExpression:
                result = "ifexp " + toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                        + " then " + toStringRec(tree.down(2), withIndices, true, 0, language, pattern)
                        + " else " + toStringRec(tree.down(3), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_accessDecl:
                result = toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                        + " " + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_function: {
                Tree explicitReturnVar = tree.down(4).getAnnotation("explicitReturnVar");
                Tree bind = tree.down(4).getAnnotation("bind");
                result =
                        "function(" + toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                                + "," + toStringRec(tree.down(2), withIndices, true, 0, language, pattern)
                                + "," + toStringRec(tree.down(3), withIndices, true, 0, language, pattern)
                                + "," + toStringRec(tree.down(4), withIndices, true, 0, language, pattern)
                                + "," + toStringRec(tree.down(5), withIndices, true, 0, language, pattern)
                                + " ["
                                + (isNullOrNone(explicitReturnVar)
                                ? ""
                                : "explicitReturnVar:"
                                + toStringRec(explicitReturnVar, withIndices, true, 0, language, pattern))
                                + (isNullOrNoneOrEmptyList(bind)
                                ? ""
                                : " bind:"
                                + toStringRec(bind, withIndices, true, 0, language, pattern))
                                + "] "
                                + "," + toStringRec(tree.down(6), withIndices, true, 0, language, pattern)
                                + ')';
                break;
            }
            case ILLang.op_void:
                result = "void";
                break;
            case ILLang.op_integer:
                result = TapEnv.isFortran(language) ? "INTEGER" : "int";
                break;
            case ILLang.op_float:
                result = TapEnv.isFortran(language) ? "REAL" : "float";
                break;
            case ILLang.op_complex:
                result = TapEnv.isFortran(language) ? "COMPLEX" : "complex";
                break;
            case ILLang.op_boolean:
                result = TapEnv.isFortran(language) ? "LOGICAL" : "_Bool";
                break;
            case ILLang.op_character:
                result = TapEnv.isFortran(language) ? "CHARACTER" : "char";
                break;
            case ILLang.op_modifiedType:
                result = "[" + toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + "]"
                        + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_typeDeclaration:
                result =
                        "type " + toStringRec(tree.down(1), withIndices, true, 0, language, pattern)
                                + ':' + toStringRec(tree.down(2), withIndices, true, 0, language, pattern);
                break;
            case ILLang.op_recordType: {
                result = "record{" + toStringRec(tree.down(3), withIndices, true, 0, language, pattern) + "}";
                break;
            }
            case ILLang.op_nameSpace: {
                result = "namespace " + toStringRec(tree.down(1), withIndices, true, 0, language, pattern) + ":(" + toStringRec(tree.down(2), withIndices, true, 0, language, pattern) + ')';
                break;
            }
            default: {
                int i;
                Tree[] subTrees = tree.children();
                if (subTrees == null) {
                    result = ":" + tree.stringValue();
                } else {
                    result = ")";
                    // then recursively display the subTrees:
                    for (i = subTrees.length - 1; i >= 0; i--) {
                        result = toStringRec(subTrees[i], withIndices, true, 0, language, pattern) + result;
                        if (i > 0) {
                            result = "," + result;
                        }
                    }
                    result = "(" + result;
                }
                result = tree.opName() + moreInfo + result;
                break;
            }
        }
        if (withTopComments) {
            Tree comments = tree.getAnnotation("preComments");
            if (comments != null) {
                result = "preC:[" + toStringRec(comments, withIndices, false, 0, language, pattern) + "]" + result;
            }
            comments = tree.getAnnotation("postComments");
            if (comments != null) {
                result = result + "postC:[" + toStringRec(comments, withIndices, false, 0, language, pattern) + "]";
            }
        }
        if (thisLevel < requiredLevel) {
            result = "(" + result + ')';
        }
        return result;
    }

    public static Tree[] removeSizeModifiers(Tree[] modifiers) {
        if (modifiers == null) {
            return null;
        }
        TapList<Tree> keptModifiers = null;
        Tree modifier;
        for (int i = modifiers.length - 1; i >= 0; --i) {
            modifier = modifiers[i];
            switch (modifier.opCode()) {
                case ILLang.op_dimColons:
                case ILLang.op_accessDecl:
                    keptModifiers = new TapList<>(modifier, keptModifiers);
                    break;
                case ILLang.op_ident: {
                    if (isIdentOf(modifier, new String[]{"signed", "unsigned", "const", "__const",
                            "__const__", "restrict", "__restrict", "volatile", "__volatile",
                            "__volatile__", "auto", "register", "extern", "static", "inline"}, true)
                            ||
                            isIdentOf(modifier, new String[]{"pointer", "allocatable",
                                    "constant", "save", "data", "in", "out", "inout", "external",
                                    "intrinsic", "optional", "target", "value", "private", "public", "sequence"}, false)
                    ) {
                        keptModifiers = new TapList<>(modifier, keptModifiers);
                    }
                    break;
                }
                case ILLang.op_nameEq: {
                    if (!isIdent(modifier.down(1), "kind", false)) {
                        keptModifiers = new TapList<>(modifier, keptModifiers);
                    }
                    break;
                }
                default:
                    // All other modifiers (expressions, intCst, call...) are considered expressions that constrain the size:
                    break;
            }
        }
        if (keptModifiers == null) {
            return null;
        }
        Tree[] result = new Tree[TapList.length(keptModifiers)];
        for (int i = 0; i < result.length; ++i) {
            result[i] = keptModifiers.head;
            keptModifiers = keptModifiers.tail;
        }
        return result;
    }

    public static Tree buildCLoc(Tree expr, String cLoc) {
        return build(ILLang.op_call,
                build(ILLang.op_none),
                build(ILLang.op_ident, cLoc),
                build(ILLang.op_expressions, expr));
    }

    /**
     * @return True when given "decl" is modifiedDeclarator(modifiers[ident:const], "subDecl").
     */
    public static boolean isAConstModified(Tree decl) {
        return decl != null && decl.opCode() == ILLang.op_modifiedDeclarator &&
                decl.down(1).length() == 1 && isIdent(decl.down(1).down(1), "const", true);
    }

    /** @return true when the given Tree is an overloaded assignment, i.e. a call to "assign"
     * with a sourcetree annotation which is an op_binary "assign" */
    public static boolean isOverloadedAssign(Tree callAssign) {
        Tree sourceTree = callAssign.getAnnotation("sourcetree") ;
        return (sourceTree!=null &&
                sourceTree.opCode()==ILLang.op_binary &&
                isIdent(sourceTree.down(2), "assign", true)) ;
    }

    /**
     * Assumes pragma is an OpenMP pragma. Adds privateNSH into
     * the list of variables that must be added as "private".
     *
     * @param privateNSH the symbol holder of the future variable to declare private.
     * @param pragma     the OpenMP pragma source tree.
     * @param inFwd      when true, the private clause goes to the fwd differentiated pragma
     * @param diff       when true, the private clause will be about the variable's derivative
     */
    public static void addVarIntoFuturePrivates(NewSymbolHolder privateNSH, Tree pragma, boolean inFwd, boolean diff) {
        String annotationName = (inFwd
                ? "futurePrivatesFwd"
                : (diff
                ? "futurePrivatesBwdDiff"
                : "futurePrivatesBwd"));
        TapList<NewSymbolHolder> privateNSHs =
                pragma.getAnnotation(annotationName);
        if (!TapList.contains(privateNSHs, privateNSH)) {
            pragma.setAnnotation(annotationName,
                    new TapList<>(privateNSH, privateNSHs));
        }
    }

    /** Same as addVarIntoFuturePrivates, but will create "omp shared" clauses */
    public static void addVarIntoFutureShareds(NewSymbolHolder sharedNSH, Tree pragma, boolean inFwd, boolean diff) {
        String annotationName = (inFwd
                ? "futureSharedsFwd"
                : (diff
                ? "futureSharedsBwdDiff"
                : "futureSharedsBwd"));
        TapList<NewSymbolHolder> sharedNSHs =
                pragma.getAnnotation(annotationName);
        if (!TapList.contains(sharedNSHs, sharedNSH)) {
            pragma.setAnnotation(annotationName,
                    new TapList<>(sharedNSH, sharedNSHs));
        }
    }

    /**
     * Same as addVarIntoFuturePrivates, but will cause a REDUCTION(+:...)
     */
    public static void addVarIntoFutureSumReductions(NewSymbolHolder sumReductionNSH, Tree pragma) {
        String annotationName = "futureSumReductionsBwdDiff";
        TapList<NewSymbolHolder> sumReductionNSHs =
                pragma.getAnnotation(annotationName);
        if (!TapList.contains(sumReductionNSHs, sumReductionNSH)) {
            pragma.setAnnotation(annotationName,
                    new TapList<>(sumReductionNSH, sumReductionNSHs));
        }
    }

    /**
     * When stringAtomTree is an atomic Tree with a String value,
     * assuming this string is a file path, normalizes this path by
     * removing "dirname/.." sequences.
     */
    public static String normalizePath(String path) {
        ToInt toStart = new ToInt(-1);
        ToInt toEnd = new ToInt(-1);
        while (simplifiableDotDot(path, toStart, toEnd)) {
            path = path.substring(0, toStart.get()) + path.substring(toEnd.get());
        }
        return path;
    }

    /**
     * Returns true when one normalization is possible
     */
    private static boolean simplifiableDotDot(String path, ToInt toStart, ToInt toEnd) {
        int end = path.indexOf("/../");
        if (end > 0) {
            int start = path.lastIndexOf("/", end - 1);
            toStart.set(start + 1);
            toEnd.set(end + 4);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Assumes pragma is an OpenMP pragma.
     *
     * @param pragma  the OpenMP pragma source tree.
     * @param usageST the scope of the place where the pragma is.
     * @param inFwd   when true, returns the trees for the fwd differentiated pragma
     * @param diff    when true, returns the trees that are meant to be differentiated
     * @return the list of Tree's
     * coming from the variables stored for private declaration.
     * The stored list of variables stored to be private is then cleared.
     * Since this list has been accumulated with the latest first,
     * we can reverse it again upon rebuild.
     */
    public static TapList<Tree> extractPrivateTrees(Tree pragma, SymbolTable usageST, boolean inFwd, boolean diff) {
        String annotationName = (inFwd
                ? "futurePrivatesFwd"
                : (diff
                ? "futurePrivatesBwdDiff"
                : "futurePrivatesBwd"));
        TapList<NewSymbolHolder> privateNSHs =
                pragma.getRemoveAnnotation(annotationName);
        TapList<Tree> result = null;
        while (privateNSHs != null) {
            result = new TapList<>(privateNSHs.head.makeNewRef(usageST), result);
            privateNSHs = privateNSHs.tail;
        }
        return result;
    }

    /** Same as extractPrivateTrees, but extracts the future SHARED variables */
    public static TapList<Tree> extractSharedTrees(Tree pragma, SymbolTable usageST, boolean inFwd, boolean diff) {
        String annotationName = (inFwd
                ? "futureSharedsFwd"
                : (diff
                ? "futureSharedsBwdDiff"
                : "futureSharedsBwd"));
        TapList<NewSymbolHolder> sharedNSHs =
                pragma.getRemoveAnnotation(annotationName);
        TapList<Tree> result = null;
        while (sharedNSHs != null) {
            result = new TapList<>(sharedNSHs.head.makeNewRef(usageST), result);
            sharedNSHs = sharedNSHs.tail;
        }
        return result;
    }

    /**
     * Same as extractPrivateTrees, but extracts the future REDUCTION(+:...) diff variables
     */
    public static TapList<Tree> extractSumReductionTrees(Tree pragma, SymbolTable usageST) {
        String annotationName = "futureSumReductionsBwdDiff";
        TapList<NewSymbolHolder> sumReductionNSHs =
                pragma.getRemoveAnnotation(annotationName);
        TapList<Tree> result = null;
        while (sumReductionNSHs != null) {
            result = new TapList<>(sumReductionNSHs.head.makeNewRef(usageST), result);
            sumReductionNSHs = sumReductionNSHs.tail;
        }
        return result;
    }

    /** Same as TypeSpec.acceptMultiDirDimension(), but on the Tree that designates the type. */
    public static boolean acceptsMultiDirDimension(Tree typeTree) {
        if (typeTree==null) return false ;
        switch (typeTree.opCode()) {
            case ILLang.op_pointerType:
            case ILLang.op_arrayType:
                return acceptsMultiDirDimension(typeTree.down(1));
            case ILLang.op_modifiedType:
                return acceptsMultiDirDimension(typeTree.down(2)) ;
            case ILLang.op_float:
            case ILLang.op_complex:
                return true ;
        // integers, characters, booleans, are leaf types that might accept a dimension, but not for AD!
            case ILLang.op_integer:
            case ILLang.op_character:
            case ILLang.op_boolean:
            default:
                return false ;
        }
    }

    private ILUtils() {
    }
}
