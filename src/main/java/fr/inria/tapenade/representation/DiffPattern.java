/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapIntList;

/**
 * Class that holds one differentiation pattern.
 * The unit name and each var name are "identifier"'s.
 * Each "identifier" is in fact a TapList of String's that
 * describes the path to the object starting from the root namespace.
 */
public final class DiffPattern {
    private TapList<TapList<String>> indepsPathNames;
    private TapList<TapList<String>> depsPathNames;
    private TapList<TapIntList> indepsPathZones;
    private TapList<TapIntList> depsPathZones;
    private TapIntList indepsZones;
    private TapIntList depsZones;
    private String casN;

    public DiffPattern(String casN,
                       TapList<TapList<String>> indepsPathNames,
                       TapList<TapList<String>> depsPathNames) {
        super();
        this.casN = casN;
        this.indepsPathNames = indepsPathNames;
        this.depsPathNames = depsPathNames;
    }

    public TapList<TapList<String>> indepsPathNames() {
        return indepsPathNames;
    }

    public void setIndepsPathNames(TapList<TapList<String>> indepsPathNames) {
        this.indepsPathNames = indepsPathNames;
    }

    public TapList<TapList<String>> depsPathNames() {
        return depsPathNames;
    }

    public void setDepsPathNames(TapList<TapList<String>> depsPathNames) {
        this.depsPathNames = depsPathNames;
    }

    public TapList<TapIntList> indepsPathZones() {
        return indepsPathZones;
    }

    public void setIndepsPathZones(TapList<TapIntList> indepsPathZones) {
        this.indepsPathZones = indepsPathZones;
    }

    public TapList<TapIntList> depsPathZones() {
        return depsPathZones;
    }

    public void setDepsPathZones(TapList<TapIntList> depsPathZones) {
        this.depsPathZones = depsPathZones;
    }

    public TapIntList indepsZones() {
        return indepsZones;
    }

    public void setIndepsZones(TapIntList indepsZones) {
        this.indepsZones = indepsZones;
    }

    public TapIntList depsZones() {
        return depsZones;
    }

    public void setDepsZones(TapIntList depsZones) {
        this.depsZones = depsZones;
    }

    public String casN() {
        return casN;
    }

    @Override
    public String toString() {
        return "[" + casN + "]:" + indepsPathNames + "=>" + depsPathNames;
    }
}
