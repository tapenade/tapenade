/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;


/**
 * Storage object to store some piece of information
 * for each Unit of a given CallGraph.
 */
public final class UnitStorage<T> {

    /**
     * The array where the storage is made.
     */
    private Object[] storage;

    /**
     * Creates a new UnitStorage for all Unit's of the given "callGraph".
     * Each cell is initialized to null.
     */
    public UnitStorage(CallGraph callGraph) {
        super();
        storage = new Object[callGraph.nbUnits()];
        for (int i = callGraph.nbUnits() - 1; i >= 0; i--) {
            storage[i] = null;
        }
    }

    /**
     * Stores "value" into the info attached to "unit".
     */
    public void store(Unit unit, T value) {
        int rank = unit.rank();
        if (rank >= 0) {
            if (rank >= storage.length) {
                resizeStorage(rank);
            }
            storage[rank] = value;
        }
    }

    /**
     * Retrieves the value of the info attached to "unit".
     */
    public T retrieve(Unit unit) {
        int rank = unit.rank();
        if (rank >= 0) {
            if (rank >= storage.length) {
                resizeStorage(rank);
            }
            return (T) storage[rank];
        } else {
            return null;
        }
    }

    /**
     * Extends the storage size, by chunks of at least 20, so that index "rank" exists.
     */
    private void resizeStorage(int rank) {
        Object[] newStorage = new Object[rank + 20 + 1];
        System.arraycopy(storage, 0, newStorage, 0, storage.length);
        for (int i = storage.length; i < rank + 20; i++) {
            newStorage[i] = null;
        }
        storage = newStorage;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = storage.length - 1; i >= 0; --i) {
            result.insert(0, (i == 0 ? "" : " ") + i + ':' + storage[i]);
        }
        return "[" + result + "]";
    }

}
