/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

/**
 * Class that holds one differentiation root point.
 * The unit name and each var name are "identifier"'s.
 * Each "identifier" is in fact a TapList of String's that
 * describes the path to the object starting from the root namespace.
 */
public final class DiffRoot {
    private final TapList<String> unitPathName;
    private TapList<DiffPattern> diffPatterns;
    private Unit unit;

    public TapList<String> unitPathName() {
        return unitPathName;
    }

    public TapList<DiffPattern> diffPatterns() {
        return diffPatterns;
    }

    /**
     * @return the differentiation root unit.
     */
    public Unit unit() {
        return unit;
    }

    /**
     * @param unit the differentiation root unit.
     */
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public DiffRoot(Unit unit, TapList<String> unitPathName,
                    TapList<DiffPattern> diffPatterns) {
        super();
        this.unitPathName = unitPathName;
        this.unit = unit;
        this.diffPatterns = diffPatterns;
    }

    /**
     * @return the lists of all Units in the given list of DiffRoot's.
     */
    public static TapList<Unit> collectUnits(TapList<DiffRoot> diffRoots) {
        TapList<Unit> hdResult = new TapList<>(null, null);
        TapList<Unit> tlResult = hdResult;
        while (diffRoots != null) {
            if (!TapList.contains(hdResult.tail, diffRoots.head.unit)) {
                tlResult = tlResult.placdl(diffRoots.head.unit);
            }
            diffRoots = diffRoots.tail;
        }
        return hdResult.tail;
    }

    /**
     * @return the DiffRoot in the given list "diffRoots" that
     * deals with the Unit "unit" or null if not found.
     */
    public static DiffRoot findUnit(TapList<DiffRoot> diffRoots, Unit unit) {
        DiffRoot found = null;
        while (found == null && diffRoots != null) {
            if (diffRoots.head.unit == unit) {
                found = diffRoots.head;
            }
            diffRoots = diffRoots.tail;
        }
        return found;
    }

    public static String rebuildIdentifier(TapList<String> identifierElements) {
        StringBuilder result = null;
        while (identifierElements != null) {
            if (result == null) {
                result = new StringBuilder(identifierElements.head);
            } else {
                result.append('.').append(identifierElements.head);
            }
            identifierElements = identifierElements.tail;
        }
        assert result != null;
        return result.toString();
    }

    public void appendDiffPattern(DiffPattern pattern) {
        boolean foundDuplicate = false;
        TapList<DiffPattern> inDiffPatterns = diffPatterns;
        while (!foundDuplicate && inDiffPatterns != null) {
            foundDuplicate = inDiffPatterns.head.equals(pattern);
            inDiffPatterns = inDiffPatterns.tail;
        }
        if (!foundDuplicate) {
            diffPatterns = new TapList<>(pattern, diffPatterns);
        }
    }

    @Override
    public String toString() {
        return unitPathName + "(" + unit + "):" + diffPatterns;
    }
}
