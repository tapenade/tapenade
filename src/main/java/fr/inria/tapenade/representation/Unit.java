
/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.*;
import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.ActivityPattern;
import fr.inria.tapenade.analysis.DataFlowAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.MultithreadAnalyzer;
import fr.inria.tapenade.frontend.TreeProtocol;
import fr.inria.tapenade.ir2tree.DeclStruct;
import fr.inria.tapenade.ir2tree.TreeGen;
import fr.inria.tapenade.ir2tree.VariableDeclStruct;
import fr.inria.tapenade.toplevel.Tapenade;

import java.io.File;
import java.io.IOException;

/**
 * One node in the call graph. Represents one particular procedure
 * or interface or external or module or package or file... of the current code.
 * Holds links to callers, callees, containing CallGraph, SymbolTable's,
 * <p>
 * For procedures, holds its Flow Graph in the form of an EntryBlock, an
 * ExitBlock, and a graph of Block's in between.
 */
public final class Unit {

    private static final int UNDEFINED = -1;
    private static final int STANDARD = 0;
    private static final int INTRINSIC = 1;
    private static final int EXTERNAL = 2;
    private static final int VARFUNCTION = 3;
    private static final int MODULE = 4;
    private static final int INTERFACE = 5;
    private static final int RENAMED = 6;
    /**
     * OUTSIDE_"To_Be_Differentiated" code
     */
    private static final int OUTSIDE_TBD = 7;
    private static final int CLASS = 8;
    private static final int CONSTRUCTOR = 9;
    private static final int DESTRUCTOR = 10;
    private static final int FILE = 11;
    private static final int DEADBLOCK = -2;
    private static final int DUMMYPROCEDURE = -3;

    /**
     * Experimental: when the number of zones is very high, a sparse representation of BoolVector
     * and even more of BoolMatrix may be more compact, at the cost of longer operations.
     * This constant sets the number of zones above which sparse representation may be preferred
     * At present this mechanism is deactivated, because the run-time penalty appears very high.
     * If number of zones is above this limit, use sparse BoolMatrix and BoolVector's
     **/
    private static final int TOOMANYZONES = 128;

    public final boolean hasTooManyZones() {
        // At present this mechanism is deactivated, because the run-time penalty appears very high:
        // => still experimental, more testing needed on large real codes.
        // return (externalShape == null ? false : (externalShape.length >= Unit.TOOMANYZONES)) ;
        return false;
    }

    /**
     * Some given name for this Unit.
     */
    private String name = "";

    /**
     * @return name of this Unit. For a translation Unit,
     * it is the full path name of the corresponding file.
     */
    public String name() {
        if (isDummy() && dummyAccessedAs!=null) {
            return "accessed as "+ILUtils.toString(ILUtils.getCalledName(dummyAccessedAs), language) ;
        } else {
            return name;
        }
    }

    /**
     * Set unit's name.
     *
     * @param name unit name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /** True for F2003 "ABSTRACT INTERFACE" declarations, that do not define a function, but a function *type* */
    protected boolean isAbstractFunctionType = false ;

    /**
     * tapenade web server does not show full path name.
     *
     * @return name of this Unit without directory.
     */
    protected String shortName() {
        if (TapEnv.isServlet()) {
            return name.substring(name.lastIndexOf(File.separator) + 1);
        } else {
            return name;
        }
    }

    /**
     * List of all CallArrow's arriving on this Unit.
     */
    public TapList<CallArrow> callers;
    /**
     * List of all CallArrow's leaving from this Unit.
     */
    public TapList<CallArrow> callees;
    /**
     * List of all classes that inherit from this class (unit).
     **/
    public TapList<Unit> childrenClasses;
    /**
     * List of the Units of all the subroutines defined locally, inside this Unit.
     */
    public TapList<Unit> lowerLevelUnits;
    /**
     * Indication of the position of this Unit in its file.
     * If -1, then getPosition will recompute it from the positions
     * of the instructions inside.
     */
    public int position = -1;
    /**
     * When this is an interface Unit, its enclosing Unit.
     */
    public Unit enclosingUnitOfInterface;
    private FortranStuff fortranStuff;

    /**
     * Special for treatment of Fortran specific info.
     */
    public FortranStuff fortranStuff() {
        return fortranStuff;
    }

    /** The memory maps built for non-global (non-COMMON) EQUIVALENCEd variables (Fortran-specific) */
    public MemoryMaps localMaps = null ;

    /**
     * For C++ and oriented-object in general, indicate the privacy of this unit: public, private or protected.
     **/
    public String privacy = "";

    /**
     * error and warning messages.
     */
    public TapList<PositionAndMessage> messages = new TapList<>(null, null);
    /**
     * List of all implicitly imported modules.
     */
    public TapList<Unit> otherImportedModules;
    /**
     * Fortran module must use ISO_C_BINDING module.
     */
    public boolean usesISO_C_BINDING;
    /**
     * If entryBlock is null, contains parameters or module name Tree.
     */
    public Tree parametersOrModuleNameTree = ILUtils.build(ILLang.op_expressions);

    private VariableDecl otherReturnVar;

    /**
     * The alternate return variable name, if different from the function's name.
     */
    public VariableDecl otherReturnVar() {
        return otherReturnVar;
    }

    public void setOtherReturnVar(VariableDecl otherReturnVar) {
        this.otherReturnVar = otherReturnVar;
    }

    /**
     * Modifiers for this function, method or subroutine.
     */
    public Tree modifiers;
    /**
     * The entry Block.
     */
    public EntryBlock entryBlock;
    /**
     * The exit Block.
     */
    public ExitBlock exitBlock;
    /**
     * DFST-ordered list of all Blocks, excluding EntryBlock and ExitBlock.
     */
    public TapList<Block> allBlocks;
    /**
     * Total number of Blocks, not including EntryBlock nor ExitBlock.
     */
    public int nbBlocks;
    /**
     * DFST-ordered list of all Flow Graph arrows.
     */
    public TapList<FGArrow> allArrows;
    /**
     * Total number of FGArrows, including those from/to EntryBlock and ExitBlock.
     */
    public int nbArrows;
    /**
     * The list of format Trees of this Unit.
     */
    public TapList<Tree> formats;
    /**
     * The list of instructions that do not depend on the flow (e.g. sub-Units definitions),
     * and that could not be placed into normal Flow Graph Block's because they are unreachable
     * by the control flow (e.g. because they are after an op_goto, op_exit, op_return...).
     */
    public TapList<Instruction> nonFlowInstructions;
    /**
     * The tree of comments pre-attached to this Unit.
     */
    public Tree preComments;
    /**
     * The tree of comments post-attached to this Unit.
     */
    public Tree postComments;
    /**
     * The tree of block-comments pre-attached to this Unit.
     */
    public Tree preCommentsBlock;

    /**
     * The tree of block-comments post-attached to this Unit.
     */
    public Tree postCommentsBlock;
    /**
     * List of things (e.g. variables) requested from the user, for the current Unit.
     */
    public TapList<TapPair<String, String>> userHelp = new TapList<>(null, null);
    /**
     * (Hack) The "original" Unit.
     * (e.g. in AD, the unit from which this one is copied or differentiated).
     */
    public Unit origUnit;
    /**
     * When this Unit is copied from origUnit, correspondence from
     * the Block's of origUnit to the Block's of this Unit. Wiped out when used.
     */
    public BlockStorage<Block> orig2copiedBlocks;
    public Block origPublicSTDeclarationBlock;
    public Block copyPublicSTDeclarationBlock;
    /**
     * Info for AD on this Unit.
     * List of all possible ActivityPattern's for the calls to this Unit.
     * Each ActivityPattern contains information about which variables being active, useful, etc,
     * at all necessary locations inside this Unit (Specific to AD).
     */
    public TapList<ActivityPattern> activityPatterns;
    /**
     * Differentiable dependencies for this Unit. Used so far, mostly to store
     * the AD deps matrix of intrinsics found from the user-given library files.
     */
    public BoolMatrix unitADDependencies;
    /**
     * The NewSymbolHolder of new variable "NBDirsMax".
     */
    public NewSymbolHolder dirNumberMaxSymbolHolder;
    /**
     * When non-null, the Tree to use as the max number of vector-diff directions (in general ident:"NBDirsMax")
     */
    public Tree dirNumberMaxTree ;
    /**
     * An ArrayDim: [arrayDimMin:NBDirsMax].
     */
    public ArrayDim multiDirDimensionMax;
    /**
     * Special (TEMPORARY) field to hold info about differentiation
     * For instance, this holds the predefined differentiation for mathematical intrinsics.
     */
    public Object diffInfo;

    /**
     * List of AD directives attached to this Unit.
     */
    public TapList<Directive> directives;
    /**
     * Flags for no Checkpoint strategy.
     */
    public boolean maybeCheckpointed = false ;
    public boolean maybeNoCheckpointed = false ;
    /**
     * The list of unit names called by this unit and that are active.
     */
    public TapList<String> activeCalledNames;
    /**
     * Utility temporary info built during static analyses.
     * True iff this Unit is head (if the analysis is bottom-up)
     * or tail (if the analysis is top-down) of a cycle in this Call Graph.
     * This cycle detection is based on the rank's of the Units.
     */
    public boolean isCGCycle;

    /**
     * Mark useful in any bottom-up static Data Flow analysis to write
     * whether this Unit must be analyzed again at next analysis sweep.
     */
    public boolean analysisIsOutOfDateUp = true;

    /**
     * Mark useful in any top-down static Data Flow analysis to write
     * whether this Unit must be analyzed again at next analysis sweep.
     */
    public boolean analysisIsOutOfDateDown = true;

    /**
     * True for procedures that F90 can use as elemental,
     * i.e. on an array of arguments of the expected type.
     */
    public boolean usedAsElementalResult;
    /**
     * True if calls to this Unit are supposed to be inlined.
     */
    public boolean isMadeForInline;
    /**
     * Default first index of arrays. Fortran = 1, C = 0.
     */
    public int arrayDimMin = -1;

    /** The number of global zones in the externalShape of this Unit.
     * It is also the rank (plus 1) of the last zone whose data-flow info
     * can be plainly copied between this Unit and any of its caller Units. */
    public int[] globalZonesNumber4 = null ;
    public int globalZonesNumber(int whichKind) {return globalZonesNumber4[whichKind] ;}

    /** The number of public zones of this Unit i.e. the globals plus the arguments */
    public int[] publicZonesNumber4 = null ;
    public int publicZonesNumber(int whichKind) {return publicZonesNumber4[whichKind] ;}

    /**
     * Array of ZoneInfo holding information about the elementary parameters of this Unit.
     */
    public ZoneInfo[] externalShape;

    /** BoolVector, one bit per zone (i.e. per index in the externalShape),
     * with value 1 if this zone can be accessed by this Unit or by
     * some Unit recursively called. This info is needed to compensate for
     * the introduction of irrelevant (HIDDEN) zones due to the
     * removal of the old "side-effect" mechanism.
     * This info may be used to compress the data-flow info on this Unit, since it is
     * guaranteed (?) that the info on non-relevant zones is necessarily neutral. */
    public BoolVector relevantZones = null ;
    /**
     * For each formal argument, indexed by its rank (0 for result),
     * contains a tree (for each elementary part of this  argument, e.g for records)
     * of the corresponding public ranks, i.e. the rank in the external shape.
     */
    public TapList[] argsPublicRankTrees;
    /**
     * Pointer destinations info, upon unit exit, wrt destinations upon unit entry.
     */
    public BoolMatrix pointerEffect;
    /**
     * In-Out data-flow info: vector of zones possibly/partly not accessed.
     */
    public BoolVector unitInOutN;
    /**
     * In-Out data-flow info: vector of zones possibly/partly read and not written.
     */
    public BoolVector unitInOutR;
    /**
     * In-Out data-flow info: vector of zones possibly/partly not read and only written.
     */
    public BoolVector unitInOutW;
    /**
     * In-Out data-flow info: vector of zones possibly/partly read then written.
     */
    public BoolVector unitInOutRW;
    /**
     * TBR info: vector of zones possibly becoming TBR when going through this Unit,
     * in other words zones used non-linearly and therefore needed in derivatives.
     */
    public BoolVector unitTBR = null ;

    /** Summary of zones and arguments that the diff of this Unit must handle. */
    public BoolVector mustHandleTBR = null ;

    /**
     * Plain dependencies data-flow info: dependency of each output wrt each input.
     */
    public BoolMatrix plainDependencies ;
//     public BoolMatrix unitDependencies; // temporary; remove when nopubliczones method works!

    public NewSymbolHolder tagMessagePassingSymbolHolder;
    /**
     * The vector of all the zones of all the variables that are
     * used in the declarations part, e.g. as dimension declarator.
     */
    public BoolVector zonesUsedInDeclarations;
    /**
     * A temporary rank used only during sorting.
     */
    protected int sortRk = -1;
    /**
     * For an intrinsic, a special unique generic arrow that stands for
     * any call to this intrinsic. Returned by getCallArrow on an intrinsic.
     */
    protected CallArrow genericArrow;
    /**
     * True for functions whose arguments and result types are fully specified.
     * If not, pieces of the type must be inferred.
     */
    protected boolean isTotallyDeclared = true;
    /**
     * The CallGraph to which this Unit belongs.
     */
    private CallGraph callGraph;
    /**
     * List of all classes (unit) inherited by this class (unit).
     **/
    private TapList<TapTriplet<Unit, Boolean, Boolean>> parentClasses;
    /**
     * If this Unit was defined as a local unit of an enclosing Unit,
     * contains the enclosing Unit. Otherwise contains null.
     */
    public Unit upperLevelUnit;
    /**
     * The SymbolTable of this Unit's top private symbols.
     */
    private SymbolTable privateSymbolTable;
    /**
     * The SymbolTable of this Unit's formal parameters or protected symbolDecls.
     */
    private SymbolTable protectedSymbolTable;
    /**
     * The SymbolTable of this Unit's formal parameters or public symbolDecls.
     */
    private SymbolTable publicSymbolTable;
    /**
     * The SymbolTable of this Unit's imported context.
     */
    private SymbolTable importsSymbolTable = null ;
    /**
     * The enclosing SymbolTable of this Unit.
     */
    private SymbolTable externalSymbolTable;
    private SymbolTable translationUnitSymbolTable;
    /**
     * List of all the SymbolTable's belonging to this Unit.
     * Contains all SymbolTable's created for this Unit or for
     * one of its Blocks, ordered with the deepest SymbolTable's first.
     */
    private TapList<SymbolTable> symbolTablesBottomUp;
    /**
     * Programming language.
     */
    private int language;
    /**
     * The kind of this Unit.
     */
    private int kind = STANDARD;

    /**
     * To ensure we don't delete a Unit twice.
     */
    private boolean wasDeleted;

    public boolean wasDeleted() {
        return wasDeleted;
    }

    public void setDeleted() {
        wasDeleted = true;
    }

    private TapList<Instruction> interfaceDeclarations;
    /**
     * When this Unit is defined in an include file, link to the #include Instruction.
     */
    private Instruction fromInclude;
    private int unitRank;
    /**
     * True if this is the MPI or ISO_C_BINDING module.
     */
    private boolean isPredefinedModuleOrTranslationUnit;
    private boolean isCalledFromOtherLanguage;
    /**
     * List of all directly imported modules use declarations.
     */
    private TapList<Instruction> importedModulesUseDecl;
    /**
     * List of all directly imported modules.
     */
    private TapList<Unit> importedModules;
    private TapList<TapTriplet<Unit, Tree, Instruction>> importedModulesAndUseInfo;
    /**
     * The function type of this Unit.
     */
    private FunctionTypeSpec functionTypeSpec;
    private ClassTypeSpec classTypeSpec;
    /**
     * The constructor Block.
     */
    private InitConstructorBlock initConstructorBlock;
    /**
     * DFST-ordered list of all top-level Blocks.
     */
    private TapList<Block> topBlocks;
    /**
     * Total number of loops.
     */
    private int nbLoopBlocks;
    /**
     * List of all Tree's proved dead because unreachable.
     */
    private TapList<Tree> deadTrees;
    /**
     * List of all comments lost while building this Unit.
     */
    private TapList<Tree> lostComments;

    // End part: about the different possible kinds of a Unit.
    private boolean isDiffPackage;
    private Unit otherName;
    /**
     * For C, for an EXTERNAL Unit, the C-style forward declaration instruction.
     */
    private Instruction forwardDeclaration;
    /**
     * True if this Unit comes from a C include file.
     */
    private boolean inStdCIncludeFile;
    /**
     * True if we allow for array notation ("vector" notation) in this Unit.
     */
    private boolean hasArrayNotation = false ;

    /**
     * For each formal argument, indexed by its rank (0 for result),
     * contains a tree of indexes for each elementary part of this
     * argument (e.g. for records). For each elementary part, the index
     * stored here is the index of this elemArg.
     */
    private TapList[] argsZoneTrees;
    /**
     * Reverse info of "argsZoneTrees". For each rank as a elementary argument,
     * gives the rank of this elementary argument in the externalShape.
     * From this rank, the externalShape[] will return the ZoneInfo.
     * We call "elementary argument" an atomic piece, e.g. a component of a
     * record, of an actual argument.
     */
    private int[] elemArgPublicRanks;
    /**
     * For each parameter zones numbers [contained into the external shape],
     * contains its corresponding elementary argument number.
     */
    private int[] zoneRkToElemArgRk;

    private boolean containsAllocDealloc;
    /**
     * Rank of the last test zone allocated here, plus one.
     * Equivalently total number of test zones created for this Unit.
     * Test zones are zones created to hold this Unit's conditional tests' results.
     */
    private int testZonesNb;

    /** For each control zone rank (from 0 up), the rank of the corresponding ZoneInfo. */
    public int[] testZoneInfos;

    /** When this block is a controller (e.g. ends with a control statement),
     * returns the zone attached to this control, otherwise returns -1 */
    public int testZoneOfControllerBlock(Block block) {
        return (block.testZone==-1 ? -1 : testZoneInfos[block.testZone]) ;
    }

    /** This instr is assumed to be a where, and therefore is a control.
     * Returns the zone attached to this control */
    public int testZoneOfControllerWhere(Instruction instr) {
        int whereDepth = instr.whereMaskDepth() ;
        // Because of poor design (a where is controlled by itself!)
        --whereDepth ;
        int controlIndex = testZonesNb-whereDepth-1 ;
        return testZoneInfos[controlIndex] ;
    }

    /** Builds a new BoolVector containing true for all control zones that control
     *  the given Instruction (which is in the given Block), or null if empty */
    public BoolVector buildControlDepsOf(Instruction instr, Block block) {
        TapIntList controlTests = block.controllersZone ;
        int whereDepth = instr.whereMaskDepth();
        // Because of poor design (a where is controlled by itself!)
        if (instr.tree!=null && instr.tree.opCode()==ILLang.op_where) --whereDepth ;
        if (controlTests!=null || whereDepth>0) {
            BoolVector controlDeps =
                new BoolVector(block.symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND));
            while (controlTests!=null) {
                controlDeps.set(testZoneInfos[controlTests.head], true) ;
                controlTests = controlTests.tail;
            }
            int lastControlZone = testZonesNb;
            for (int i=whereDepth ; i>0 ; --i) {
                controlDeps.set(testZoneInfos[lastControlZone - i], true);
            }
            return controlDeps ;
        } else {
            return null ;
        }
    }

    /**
     * Used to rebuild correct "targetZonesTree" in side-effect ZoneInfo's.
     */
    private TapList<TapPair<ZoneInfo, TapList>> targetZoneInfos;

    /** Used to memorize In-Out info of II-LOOPs */
    private TapList<TapPair<Block, TapPair<BoolVector, BoolVector>>> fragmentInOutCkp = null ;;

    /** Used to memorize needed additional include's in differentiated C, e.g. adBinomial.h, adFixedPoint.h ... */
    public TapList<String> newDiffCIncludes = null ;

    private Unit() {} ;

    /**
     * Creates a new Unit and links it to its CallGraph.
     */
    public Unit(CallGraph callGraph, int rank, Unit upperLevelUnit, int language) {
        super();
        this.language = language;
        this.setRank(rank);
        this.callGraph = callGraph;
        this.upperLevelUnit = upperLevelUnit;
        // a faire seulement pour fortran
        if (this.isFortran()) {
            fortranStuff = new FortranStuff();
            fortranStuff.unit = this ;
            fortranStuff.callGraph = callGraph ;
        }
        if (callGraph != null) {
            localMaps = new MemoryMaps() ;
            if (fortranStuff != null && upperLevelUnit != null && !upperLevelUnit.isTranslationUnit()) {
                fortranStuff.statementFunctions =
                        upperLevelUnit.fortranStuff.statementFunctions;
            }
        }
    }

    public static TapList<Unit> removePhantomPackages(TapList<Unit> diffUnits) {
        TapList<Unit> hdResult = new TapList<>(null, null);
        TapList<Unit> tlResult = hdResult;
        while (diffUnits != null) {
            if (diffUnits.head != null && diffUnits.head.isDiffPackage) {
                tlResult = tlResult.placdl(diffUnits.head);
            }
            diffUnits = diffUnits.tail;
        }
        return hdResult.tail;
    }

    /**
     * Takes the ranks of the first following blocks from block, and
     * set the corresponding cell in array to value.
     *
     * @param array an array of boolean
     * @param block a block
     * @param value a boolean value
     * @return true if some blocks in the flow of block have a smaller rank.
     */
    private static boolean markDownStreamBlock(boolean[] array, Block block, boolean value) {
        boolean result = false;
        for (TapList<FGArrow> iter = block.flow(); iter != null; iter = iter.tail) {
            mark(array, iter.head.destination, value);
            result |= iter.head.destination.rank <= block.rank;
        }
        return result;
    }

    /**
     * Associate information 'value with upstream block of 'block in boolean 'array.
     */
    private static boolean markUpStreamBlock(boolean[] array, Block block, boolean value) {
        boolean result = false;
        for (TapList<FGArrow> iter = block.backFlow(); iter != null; iter = iter.tail) {
            mark(array, iter.head.origin, value);
            result |= iter.head.destination.rank >= block.rank;
        }
        return result;
    }

    /**
     * Get information about block in the boolean vector array.
     */
    private static boolean marked(boolean[] array, Block block) {
        return array[block.rank + 1];
    }

    /**
     * Put boolean information `value about `block in boolean vector `array.
     */
    private static void mark(boolean[] array, Block block, boolean value) {
        array[block.rank + 1] = value;
    }

    /**
     * Find in the list of outgoing arrows of block, all the arrows with
     * a test field equals to test.
     */
    private static TapList<FGArrow> findFlowArrow(Block block, int test) {
        return findFGArrow(block.flow(), test);
    }

    /**
     * Find in a list of FGArrow, all the arrows with a test field equal to test.
     */
    private static TapList<FGArrow> findFGArrow(TapList<FGArrow> flow, int test) {
        TapList<FGArrow> hd = new TapList<>(null, null);
        TapList<FGArrow> tl = hd;
        for (; flow != null; flow = flow.tail) {
            if (TapIntList.contains(flow.head.cases, test)) {
                tl = tl.placdl(flow.head);
            }
        }
        return tl;
    }

    /**
     * @return the boolean value associated to block.
     */
    private static boolean getBitForBlock(BoolVector bv, Block block) {
        return bv.get(block.rank + 1);
    }

    // FOLLOWING COMPONENTS ARE MEANINGFUL ONLY FOR PROCEDURAL Unit's :

    // Begin part: fields and small methods dealing with Flow Graph representation:

    /**
     * Constructor for Units that are still dubious between External and Intrinsic.
     */
    public static Unit makeExternalOrIntrinsic(String funcName, CallGraph callGraph, int language) {
        Unit unit = callGraph.createNewUnit(null, language);
        unit.setExternalSymbolTable(callGraph.languageRootSymbolTable(language));
        unit.kind = OUTSIDE_TBD;
        unit.name = funcName;
        unit.activityPatterns = null;
        return unit;
    }

    /**
     * Constructor for external Units.
     */
    public static Unit makeExternal(String funcName, CallGraph callGraph, int language, boolean isPrivate) {
        Unit unit = callGraph.createNewUnit(null, language);
        if (isPrivate) {
            unit.setTranslationUnitSymbolTable(TapEnv.currentTranslationUnitSymbolTable());
        } else {
            unit.setExternalSymbolTable(callGraph.languageRootSymbolTable(language));
        }
        unit.kind = EXTERNAL;
        unit.name = funcName;
        unit.activityPatterns = null;
        return unit;
    }

    /**
     * Constructor for a dummy Unit, standing for some Unit that is unknown
     * because e.g. we don't know its name (example: call struc.func[i](1.5,2.5))
     * Ideally, there should be no dummy Unit used/referred to in the code,
     * once pointer analysis has been run?
     */
    public static Unit makeDummyUnit(String funcName, CallGraph callGraph, int language) {
        Unit dummyUnit = new Unit() ;
        callGraph.dummyUnits = new TapList<>(dummyUnit, callGraph.dummyUnits) ;
        dummyUnit.setKind(DUMMYPROCEDURE) ;
        dummyUnit.setRank(-1) ;
        dummyUnit.setName(funcName) ;
        dummyUnit.language = language;
        dummyUnit.callGraph = callGraph;
        dummyUnit.setExternalSymbolTable(callGraph.languageRootSymbolTable(language));
        dummyUnit.activityPatterns = null;
        return dummyUnit;
    }

    public boolean isDummy() {return kind==DUMMYPROCEDURE;}

    /** Used only for DUMMYPROCEDURE's: holds the place where it is accessed */
    public Tree dummyAccessedAs = null ;

    /**
     * @return an empty unit designed to represent a function which
     * is only a variable of type "function" inside another unit.
     */
    public static Unit makeVarFunction(String funcName, Unit callingUnit) {
        Unit unit = callingUnit.callGraph.createNewUnit(null, callingUnit.language);
        // If procedure F declares a varfunction G, then G sees the same globals as F.
        unit.setExternalSymbolTable(callingUnit.externalSymbolTable());
        /*unit.genericArrow = new CallArrow(null, unit);*/
        if (callingUnit.isFortran()) {
            unit.kind = VARFUNCTION;
        } else {
            unit.kind = EXTERNAL;
        }
        unit.name = funcName;
        return unit;
    }

    /**
     * Similar to allCallers(), but starts from several initialUnits.
     */
    public static TapList<Unit> allCallersMulti(TapList<Unit> initialUnits) {
        TapList<Unit> toDejaVu = new TapList<>(null, null);
        TapList<Unit> callersList = null;
        while (initialUnits != null) {
            callersList = allCallersRec(initialUnits.head, callersList, toDejaVu);
            initialUnits = initialUnits.tail;
        }
        return callersList;
    }

    /**
     * Recursive search for callers.
     */
    private static TapList<Unit> allCallersRec(
            Unit unit, TapList<Unit> callersList, TapList<Unit> toDejaVu) {
        if (TapList.contains(toDejaVu, unit) || unit.rank() < 0) {
            return callersList;
        } else {
            TapList<CallArrow> callArrows = unit.callers;
            Unit origin;
            toDejaVu.tail = new TapList<>(unit, toDejaVu.tail);
            while (callArrows != null) {
                origin = callArrows.head.origin;
                callersList = allCallersRec(origin, callersList, toDejaVu);
                callArrows = callArrows.tail;
            }
            return new TapList<>(unit, callersList);
        }
    }

    /**
     * Similar to allCallees(), but starts from several initialUnits.
     */
    public static TapList<Unit> allCalleesMulti(TapList<Unit> initialUnits) {
        TapList<Unit> toDejaVu = new TapList<>(null, null);
        TapList<Unit> calleesList = null;
        while (initialUnits != null) {
            calleesList = allCalleesRec(initialUnits.head, calleesList, toDejaVu);
            initialUnits = initialUnits.tail;
        }
        return calleesList;
    }

    /**
     * Similar to allCalleesMulti, but avoids going through "avoidedUnits".
     */
    public static TapList<Unit> allCalleesMultiAvoiding(TapList<Unit> initialUnits, TapList<Unit> avoidedUnits) {
        TapList<Unit> toDejaVu = new TapList<>(null, avoidedUnits);
        TapList<Unit> calleesList = null;
        while (initialUnits != null) {
            calleesList = allCalleesRec(initialUnits.head, calleesList, toDejaVu);
            initialUnits = initialUnits.tail;
        }
        return calleesList;
    }

    /**
     * Recursive search for callees.
     */
    private static TapList<Unit> allCalleesRec(Unit unit,
                                               TapList<Unit> calleesList, TapList<Unit> toDejaVu) {
        if (TapList.contains(toDejaVu, unit) || unit.rank() < 0) {
            return calleesList;
        } else {
            TapList<CallArrow> callArrows = unit.callees;
            Unit destination;
            toDejaVu.tail = new TapList<>(unit, toDejaVu.tail);
            while (callArrows != null) {
                destination = callArrows.head.destination;
                if (callArrows.head.isCall()) {
                    calleesList = allCalleesRec(destination, calleesList, toDejaVu);
                }
                callArrows = callArrows.tail;
            }
            return new TapList<>(unit, calleesList);
        }
    }

    /**
     * Copies the contents of origUnit into copyUnit.
     *
     * @param context the context in which the copyUnit will appear. E.g. when copyUnit is used to inline a call.
     */
    protected static TapList<FGArrow> copyUnitBody(Unit origUnit, Unit copyUnit, Instruction context) {
        TapList<Block> hdNewBlocks = new TapList<>(null, null);
        TapList<Block> tlNewBlocks = hdNewBlocks;
        Block origBlock;
        Block copyBlock;
        Block copyDest;
        TapList<FGArrow> hdNewArrows = new TapList<>(null, null);
        TapList<FGArrow> tlNewArrows = hdNewArrows;
        FGArrow origArrow;
        FGArrow copyArrow;
        TapList<Block> origBlocks = origUnit.allBlocks;
        TapList<FGArrow> flow;
        // first, copy Block's :
        copyUnit.orig2copiedBlocks = new BlockStorage<>(origUnit);
        TapList<TapPair<Instruction, Instruction>> alreadyCopiedMasks = new TapList<>(null, null);
        // entryBlock may be null for MODULEs or VARFUNCTIONs
        if (origUnit.entryBlock != null) {
            // For some unknown reason, for inline in C++, it happens that origUnit.publicSymbolTable() is wrong...
            copyUnit.setPublicSymbolTable(origUnit.entryBlock.symbolTable);
            copyUnit.entryBlock = new EntryBlock(origUnit.entryBlock.symbolTable);
            origUnit.entryBlock.copyIntoWithoutArrows(copyUnit.entryBlock, alreadyCopiedMasks);
            copyUnit.orig2copiedBlocks.store(origUnit.entryBlock, copyUnit.entryBlock);
        }
        if (origUnit.exitBlock != null) {
            copyUnit.exitBlock = new ExitBlock(origUnit.exitBlock.symbolTable);
            origUnit.exitBlock.copyIntoWithoutArrows(copyUnit.exitBlock, alreadyCopiedMasks);
            copyUnit.orig2copiedBlocks.store(origUnit.exitBlock, copyUnit.exitBlock);
        }
        while (origBlocks != null) {
            origBlock = origBlocks.head;
            if (origBlock instanceof HeaderBlock) {
                copyBlock = new HeaderBlock(origBlock.symbolTable, origBlock.parallelControls, null);
                ((HeaderBlock) copyBlock).declaresItsIterator = ((HeaderBlock) origBlock).declaresItsIterator;
                ((HeaderBlock) copyBlock).setOrigCycleLabel(((HeaderBlock) origBlock).origCycleLabel());
            } else {
                copyBlock = new BasicBlock(origBlock.symbolTable, origBlock.parallelControls, null);
            }
            origBlock.copyIntoWithoutArrows(copyBlock, alreadyCopiedMasks);
            // If the copy unit is meant to be placed in a given context (enclosing include, or
            // enclosing WHERE control...), then adapt the copy Instructions accordingly.
            if (context != null) {
                Instruction copyInstruction;
                Instruction enclosingInclude = context.fromInclude();
                InstructionMask enclosingMask = context.getWhereMask();
                for (TapList<Instruction> copyInstructions = copyBlock.instructions;
                     copyInstructions != null; copyInstructions = copyInstructions.tail) {
                    copyInstruction = copyInstructions.head;
                    if (enclosingInclude != null) {
                        copyInstruction.setFromInclude(enclosingInclude);
                    }
                    if (enclosingMask != null) {
                        copyInstruction.encloseInWhereMask(enclosingMask);
                    }
                }
            }
            copyUnit.orig2copiedBlocks.store(origBlock, copyBlock);
            tlNewBlocks = tlNewBlocks.placdl(copyBlock);
            origBlocks = origBlocks.tail;
        }
        copyUnit.allBlocks = hdNewBlocks.tail;
        if (origUnit.publicSymbolTable() != null) {
            Block origPublicSTDB = origUnit.publicSymbolTable().declarationsBlock;
            copyUnit.origPublicSTDeclarationBlock = origPublicSTDB;
            if (origPublicSTDB != null && origPublicSTDB.rank >= -1) {
                copyUnit.copyPublicSTDeclarationBlock =
                        copyUnit.orig2copiedBlocks.retrieve(origPublicSTDB);
            }
        }
        // then, copy FGArrow's :
        if (origUnit.entryBlock != null) {
            origBlocks = new TapList<>(origUnit.entryBlock, origUnit.allBlocks);
        }
        while (origBlocks != null) {
            flow = origBlocks.head.flow();
            copyBlock = copyUnit.orig2copiedBlocks.retrieve(origBlocks.head);
            while (flow != null) {
                origArrow = flow.head;
                copyDest = copyUnit.orig2copiedBlocks.retrieve(origArrow.destination);
                if (copyDest != null) {
                    copyArrow = new FGArrow(copyBlock, origArrow.test, origArrow.cases,
                            copyDest, origArrow.inACycle);
                    tlNewArrows = tlNewArrows.placdl(copyArrow);
                    copyArrow.rank = origArrow.rank;
                }
                flow = flow.tail;
            }
            origBlocks = origBlocks.tail;
        }
        // Also compute the new parallelControls
        BlockStorage<TapList<Instruction>> parallelControlsCorrepondence = new BlockStorage<>(origUnit);
        origBlocks = origUnit.allBlocks;
        while (origBlocks != null) {
            origBlock = origBlocks.head;
            copyBlock = copyUnit.orig2copiedBlocks.retrieve(origBlock);
            copyBlock.parallelControls =
                    getSetNewParallelControls(origBlock.parallelControls,
                            copyUnit.orig2copiedBlocks, parallelControlsCorrepondence);
            origBlocks = origBlocks.tail;
        }
        return hdNewArrows.tail;
    }

    /**
     * Transposes a given list of parallel controls which applies to an existing Unit
     * into a new "mirror" list of parallel controls that applies to a new unit deduced from the existing one,
     * following the correspondence "blockCorrespondence" from existing Unit's Blocks to new Unit's Blocks,
     * and the already built new lists of parallel controls "parallelControlsCorrepondence".
     */
    public static TapList<Instruction> getSetNewParallelControls(TapList<Instruction> oldParallelControls,
                                                                 BlockStorage<Block> blockCorrespondence,
                                                                 BlockStorage<TapList<Instruction>> parallelControlsCorrepondence) {
        if (oldParallelControls == null) {
            return null;
        }
        Block oldControlBlock = oldParallelControls.head.block;
        TapList<Instruction> newControls = parallelControlsCorrepondence.retrieve(oldControlBlock);
        if (newControls == null) {
            TapList<Instruction> newControlsAround =
                    getSetNewParallelControls(oldParallelControls.tail,
                            blockCorrespondence, parallelControlsCorrepondence);
            TapList<Instruction> newControlBlockInstrs =
                    blockCorrespondence.retrieve(oldControlBlock).instructions;
            if (newControlBlockInstrs != null) {
                newControls = new TapList<>(newControlBlockInstrs.head, newControlsAround);
            } else {
                newControls = newControlsAround;
            }
            parallelControlsCorrepondence.store(oldControlBlock, newControls);
        }
        return newControls;
    }

    private static void dumpDeclaredKindZones(SymbolTable symbolTable,
                                              int kind, int indent) throws IOException {
        if (symbolTable != null) {
            dumpDeclaredKindZones(symbolTable.basisSymbolTable(), kind, indent);
            dumpArrayOfZones(symbolTable.getDeclaredZoneInfos(kind),
                    symbolTable.firstDeclaredZone(kind), indent);
            if (symbolTable.getAdditionalDuplicatedDeclaredZoneInfos() != null) {
                dumpTapListOfZones(symbolTable.getAdditionalDuplicatedDeclaredZoneInfos()[kind], indent);
            }
        }
    }

    private static void dumpArrayOfZones(ZoneInfo[] zoneInfos,
                                         int offset, int indent) throws IOException {
        if (zoneInfos != null) {
            for (int i = 0; i < zoneInfos.length; ++i) {
                TapEnv.indentPrint(indent, TapEnv.str3(i + offset) + "> ");
                if (zoneInfos[i] != null) {
                    zoneInfos[i].dump();
                } else {
                    TapEnv.print(" null, meaning hidden");
                }
                TapEnv.println();
            }
        }
    }

    private static void dumpTapListOfZones(TapList<Int2ZoneInfo> zoneInfos,
                                           int indent) throws IOException {
        Int2ZoneInfo iZoneInfo;
        while (zoneInfos != null) {
            iZoneInfo = zoneInfos.head;
            TapEnv.indentPrint(indent, TapEnv.str3(iZoneInfo.getZoneNumber()) + "> ");
            iZoneInfo.getZoneInfo().dump();
            TapEnv.println();
            zoneInfos = zoneInfos.tail;
        }
    }

    private static void dumpBlocksNesting(TapList<Block> blocks) throws IOException {
        Block block;
        TapEnv.print("(");
        while (blocks != null) {
            block = blocks.head;
            if (block instanceof LoopBlock) {
                TapEnv.print("L" + block.rank + ':');
                dumpBlocksNesting(((LoopBlock) block).inside);
            } else {
                TapEnv.print("B" + block.rank);
            }
            blocks = blocks.tail;
            if (blocks != null) {
                TapEnv.print(" ");
            }
        }
        TapEnv.print(")");
    }

    /**
     * @return the CallGraph to which this Unit belongs.
     */
    public CallGraph callGraph() {
        return callGraph;
    }

    /**
     * @return the list of all CallArrow's arriving on this Unit.
     */
    public TapList<CallArrow> callers() {
        return callers;
    }

    /**
     * @return the list of all CallArrow's corresponding to a op_call of this Unit.
     */
    public TapList<CallArrow> callersThatCall() {
        TapList<CallArrow> result = null;
        TapList<CallArrow> inCallerArrows = callers;
        CallArrow arrow;
        while (inCallerArrows != null) {
            arrow = inCallerArrows.head;
            if (arrow.origin != null && arrow.isCall()) {
                result = new TapList<>(arrow, result);
            }
            inCallerArrows = inCallerArrows.tail;
        }
        return result;
    }

    /**
     * Same as callersThatCall.
     *
     * @return the list of name Strings of the callers.
     */
    public TapList<String> callerUnitNamesThatCall() {
        TapList<String> result = null;
        TapList<CallArrow> inCallerArrows = callers;
        CallArrow arrow;
        String callerName;
        while (inCallerArrows != null) {
            arrow = inCallerArrows.head;
            if (arrow.origin != null && arrow.isCall()) {
                callerName = arrow.origin.name;
                if (callerName != null && !TapList.containsEquals(result, callerName)) {
                    result = new TapList<>(callerName, result);
                }
            }
            inCallerArrows = inCallerArrows.tail;
        }
        return result;
    }

    /**
     * @return the list of all CallArrow's leaving from this Unit.
     */
    public TapList<CallArrow> callees() {
        return callees;
    }

    public void addParentClass(Unit unit, boolean isVirtualInheritance, boolean isPublicInheritance) {
        parentClasses = new TapList<>(new TapTriplet<>(unit, isVirtualInheritance, isPublicInheritance), parentClasses);
    }

    public void addChildrenClass(Unit unit) {
        childrenClasses = new TapList<>(unit, childrenClasses);
    }

    /**
     * Declares "unit" as defined locally, inside this Unit.
     */
    public void addLowerLevelUnit(Unit unit) {
        lowerLevelUnits = new TapList<>(unit, lowerLevelUnits);
    }

    /**
     * @return the Unit enclosing this one, if exists. null otherwise.
     */
    public Unit upperLevelUnit() {
        return upperLevelUnit;
    }

    public void setUpperLevelUnit(Unit upUnit) {
        upperLevelUnit = upUnit;
    }

    /**
     * @return 0 for the topmost Unit's
     */
    public int nestingDepth() {
        int depth = 0;
        Unit curUnit = upperLevelUnit;
        while (curUnit != null) {
            ++depth;
            curUnit = curUnit.upperLevelUnit;
        }
        return depth;
    }

    /**
     * @return the list of all Units enclosing this one, from the inside out.
     */
    public TapList<Unit> enclosingUnits() {
        if (upperLevelUnit == null) {
            return null;
        } else {
            return new TapList<>(upperLevelUnit, upperLevelUnit.enclosingUnits());
        }
    }

    /**
     * @return true if this Unit contains the "enclosed" Unit.
     * A Unit does not enclose itself.
     */
    public boolean encloses(Unit enclosed) {
        boolean encloses = false;
        while (!encloses && enclosed != null) {
            enclosed = enclosed.upperLevelUnit;
            encloses = enclosed == this;
        }
        return encloses;
    }

    /**
     * @return the SymbolTable of this Unit's top private symbols.
     */
    public SymbolTable privateSymbolTable() {
        return privateSymbolTable;
    }

    /**
     * @return the SymbolTable of this Unit's top protected symbols.
     */
    public SymbolTable protectedSymbolTable() {
        return protectedSymbolTable;
    }

    /**
     * @return the SymbolTable of this Unit's formal parameters or public symbolDecls.
     */
    public SymbolTable publicSymbolTable() {
        return publicSymbolTable;
    }

    /**
     * @return the imported context SymbolTable of this Unit.
     */
    public SymbolTable importsSymbolTable() {
        return importsSymbolTable;
    }

    /**
     * Sets the SymbolTable of this Unit's top private symbols.
     */
    public void setPrivateSymbolTable(SymbolTable symbolTable) {
        privateSymbolTable = symbolTable;
    }

    /**
     * Sets the SymbolTable of this Unit's top protected symbols.
     */
    public void setProtectedSymbolTable(SymbolTable symbolTable) {
        protectedSymbolTable = symbolTable;
    }

    /**
     * Sets the SymbolTable of this Unit's formal parameters or public symbolDecls.
     */
    public void setPublicSymbolTable(SymbolTable symbolTable) {
        publicSymbolTable = symbolTable;
    }

    /**
     * Sets the imported context SymbolTable of this Unit.
     */
    public void setImportsSymbolTable(SymbolTable symbolTable) {
        importsSymbolTable = symbolTable;
    }

    /**
     * Sets the enclosing SymbolTable of this Unit.
     */
    public void setExternalSymbolTable(SymbolTable symbolTable) {
        externalSymbolTable = symbolTable;
    }

    /**
     * @return the enclosing SymbolTable of this Unit.
     */
    public SymbolTable externalSymbolTable() {
        SymbolTable resultST;
        if (externalSymbolTable != null) {
            resultST = externalSymbolTable;
        } else if (publicSymbolTable != null) {
            resultST = publicSymbolTable.basisSymbolTable();
        } else if (privateSymbolTable != null && privateSymbolTable.basisSymbolTable() != null) {
            resultST = privateSymbolTable.basisSymbolTable().basisSymbolTable();
        } else {
            resultST = callGraph.languageRootSymbolTable(language);
        }
        return resultST;
    }

    public void setTranslationUnitSymbolTable(SymbolTable symbolTable) {
        translationUnitSymbolTable = symbolTable;
    }

    public SymbolTable translationUnitSymbolTable() {
        return translationUnitSymbolTable;
    }

    public SymbolTable bodySymbolTable() {
        if (privateSymbolTable != null) {
            return privateSymbolTable;
        } else if (protectedSymbolTable != null && isClass()) {
            return protectedSymbolTable;
        } else if (publicSymbolTable != null) {
            return publicSymbolTable;
        } else {
            return translationUnitSymbolTable;
        }
    }

    /** Returns the SymbolTable that contains the main FunctionDecl for this (function) Unit. */
    public SymbolTable functionDeclHostSymbolTable() {
        SymbolTable result = null ;
        if (upperLevelUnit.isTranslationUnit()) {
            result = upperLevelUnit.externalSymbolTable();
        } else if (upperLevelUnit.isModule()) {
            if (upperLevelUnit.isPrivate()) {
                result = upperLevelUnit.privateSymbolTable() ;
            } else {
                result = upperLevelUnit.publicSymbolTable();
            }
        }
        return result ;
    }

    /**
     * Adds a SymbolTable into the list of all SymbolTables belonging to this Unit.
     * This routine must be called for the upmost SymbolTable's first, so that
     * these SymbolTables are ordered bottom-up.
     */
    public void addDerivedSymbolTable(SymbolTable symbolTable) {
        symbolTablesBottomUp = new TapList<>(symbolTable, symbolTablesBottomUp);
    }

    private void removeDerivedSymbolTable(SymbolTable symbolTable) {
        TapList<SymbolTable> toList = new TapList<>(null, symbolTablesBottomUp);
        TapList<SymbolTable> inList = toList;
        while (inList.tail != null) {
            if (inList.tail.head == symbolTable) {
                inList.tail = inList.tail.tail;
            } else {
                inList = inList.tail;
            }
        }
        symbolTablesBottomUp = toList.tail;
    }

    /**
     * @return the list of all the SymbolTable's belonging to this Unit.
     * The list contains all SymbolTable's created for this Unit or for
     * one of its Blocks, ordered with the deepest SymbolTable's first.
     */
    public TapList<SymbolTable> symbolTablesBottomUp() {
        return symbolTablesBottomUp;
    }

    /**
     * @return the programming language.
     */
    public int language() {
        return language;
    }

    public static int language(Unit unit) {
        return (unit==null ? TapEnv.UNDEFINED : unit.language) ;
    }

    /**
     * Sets the programming language.
     */
    public void setLanguage(int lang) {
        language = lang;
    }

    /**
     * Sets the programming language, and recursively up on each enclosing Unit.
     */
    public void setLanguageAndUp(int lang) {
        language = lang;
        if (upperLevelUnit != null) {
            upperLevelUnit.setLanguageAndUp(lang);
        }
    }

    /**
     * Allow for array notation ("vector" notation) in this Unit.
     */
    public void setHasArrayNotation() {
        hasArrayNotation = true;
    }

    /**
     * Allow for array notation ("vector" notation) in this Unit and recursively up.
     */
    public void setHasArrayNotationAndUp() {
        hasArrayNotation = true;
        if (upperLevelUnit != null) {
            upperLevelUnit.setHasArrayNotationAndUp() ;
        }
    }

    /**
     * @return True if the programming language is any FORTRAN.
     */
    public boolean isFortran() {
        return TapEnv.isFortran(language);
    }

    /**
     * @return True if the programming language is FORTRAN90 or upper.
     */
    public boolean isFortran9x() {
        return TapEnv.isFortran9x(language);
    }

    /**
     * @return True if the programming language is FORTRAN2003 or upper.
     */
    public boolean isFortran2003() {
        return TapEnv.isFortran2003(language);
    }

    /**
     * @return True if the programming language is C.
     */
    public boolean isCexactly() {
        return TapEnv.isCexactly(language);
    }

    public boolean isCPlusPlus() {
        return TapEnv.isCPlusPlus(language);
    }

    public boolean isC() {
        return TapEnv.isC(language);
    }

    // End part: fields and small methods dealing with Flow Graph representation.
    // Begin part: fields dedicated to AD:
    // IT WOULD BE NICE NOT TO HAVE THESE FIELDS HERE, BUT...

    public boolean sameLanguage(int lang) {
        if (lang == TapEnv.C) {
            return language == TapEnv.C ;
        } else if (lang == TapEnv.CUDA) {
            return language == TapEnv.CUDA ;
        } else if (lang == TapEnv.CPLUSPLUS) {
            return isCPlusPlus();
        } else if (lang != TapEnv.MIXED) {
            return isFortran();
        } else {
            return false;
        }
    }

    /**
     * Sets the kind of this Unit.
     */
    public void setKind(Unit otherUnit) {
        kind = otherUnit.kind;
    }

    public void setKind(int kind) {
        this.kind = kind;
    }

    public void setUndefined() {
        kind = UNDEFINED;
    }

    /**
     * The sort of differentiated unit this is
     */
    public int sortOfDiffUnit = -1; // is DiffConstants.UNDEFSORT

    /**
     * @return True if this is an undefined module.
     */
    public boolean isUndefined() {
        return kind == UNDEFINED;
    }

    /**
     * @return True if this is a standard Unit.
     */
    public boolean isStandard() {
        return kind == STANDARD;
    }

    /**
     * Declare this is an intrinsic Unit.
     */
    public void setIntrinsic() {
        kind = INTRINSIC;
    }

    /**
     * @return True if this is an intrinsic Unit.
     */
    public boolean isIntrinsic() {
        return kind == INTRINSIC;
    }

    /**
     * Declare this is an external Unit.
     */
    public void setExternal() {
        kind = EXTERNAL;
    }

    /**
     * @return True if this is an external Unit.
     */
    public boolean isExternal() {
        return kind == EXTERNAL;
    }

    /**
     * @return True if this is a procedure Unit.
     */
    public boolean isProcedure() {
        return kind == STANDARD
                || kind == INTRINSIC
                || kind == CONSTRUCTOR
                || kind == DESTRUCTOR
                || kind == EXTERNAL
                || kind == UNDEFINED;
    }

    /**
     * @return True if this is a procedure Unit with its code exposed.
     */
    public boolean isProcedureWithCode() {
        return kind == STANDARD || kind == CONSTRUCTOR || kind == DESTRUCTOR
                || kind == INTRINSIC && hasSource() /*cas des inline ! */;
    }

    /**
     * @return True if this is a package of sub-Units.
     */
    public boolean isPackage() {
        return kind == FILE || kind == MODULE || kind == CLASS;
    }

    /**
     * Declare this is a Unit for a variable that holds a function.
     */
    public void setVarFunction() {
        kind = VARFUNCTION;
    }

    /**
     * @return True if this is a Unit for a variable that holds a function.
     */
    public boolean isVarFunction() {
        return kind == VARFUNCTION;
    }

    /**
     * Declare this is a module.
     */
    public void setModule() {
        kind = MODULE;
    }

    /**
     * @return True if this is a module.
     */
    public boolean isModule() {
        return kind == MODULE;
    }

    /**
     * Declare this is a class.
     */
    public void setClass() {
        kind = CLASS;
    }

    /**
     * @return True if this is a class.
     */
    public boolean isClass() {
        return kind == CLASS;
    }

    /**
     * Declare this is an constructor Unit.
     */
    public void setConstructor() {
        kind = CONSTRUCTOR;
    }

    /**
     * @return True if this is an constructor Unit.
     */
    public boolean isConstructor() {
        return kind == CONSTRUCTOR;
    }

    /**
     * Declare this is an destructor Unit.
     */
    public void setDestructor() {
        kind = DESTRUCTOR;
    }

    /**
     * Declare this is an interface Unit.
     */
    public void setInterface() {
        kind = INTERFACE;
    }

    public void setInterfaceDeclaration(Instruction instr) {
        interfaceDeclarations = new TapList<>(instr, interfaceDeclarations);
    }

    /**
     * @return True if this is an (unsolved) interface Unit.
     */
    public boolean isInterface() {
        return kind == INTERFACE;
    }

    /**
     * @return True if this is a renamed Unit.
     */
    public boolean isRenamed() {
        return kind == RENAMED;
    }

    /**
     * @return True if this is an outside Unit, maybe external, maybe intrinsic.
     */
    public boolean isOutsideTBD() {
        return kind == OUTSIDE_TBD;
    }

    /**
     * @return True if this is an external, or an intrinsic, or an outside to be determined.
     */
    public boolean isOutside() {
        return kind == OUTSIDE_TBD || kind == EXTERNAL || kind == INTRINSIC;
    }

    /**
     * @return True if this is a "Translation Unit" (i.e. the Unit for a file).
     */
    public boolean isTranslationUnit() {
        return kind == FILE;
    }

    /**
     * Declare this is a "Translation Unit" (i.e. the Unit for a file).
     */
    public void setTranslationUnit(String fileName) {
        kind = FILE;
        name = fileName;
    }

    /**
     * @return True if this is a top-level unit, i.e. immediately inside a file (aka Translation Unit).
     */
    public boolean isTopInFile() {
        return (upperLevelUnit==null ? !isTranslationUnit() : upperLevelUnit.isTranslationUnit()) ;
    }

    /** Returns the enclosing file Unit (aka Translation Unit). */
    public Unit translationUnit() {
        if (isTranslationUnit()) {
            return this ;
        } else if (upperLevelUnit==null) {
            return null ;
        } else {
            return upperLevelUnit.translationUnit() ;
        }
    }

    public Instruction fromInclude() {
        return fromInclude;
    }

    public void setFromInclude(Instruction fromInc) {
        fromInclude = fromInc;
    }

    /**
     * @return The rank of this Unit, for access in UnitStorage's,
     * Positive number.
     */
    public int rank() {
        return unitRank;
    }

    public void setRank(int rank) {
        this.unitRank = rank;
    }

    /**
     * @return true if this is the MPI module.
     */
    public boolean isPredefinedModuleOrTranslationUnit() {
        return isPredefinedModuleOrTranslationUnit;
    }

    /**
     * Declares that this is the MPI module.
     */
    public void setPredefinedModuleOrTranslationUnit() {
        isPredefinedModuleOrTranslationUnit = true;
        if (upperLevelUnit != null) {
            upperLevelUnit.setPredefinedModuleOrTranslationUnit();
        }
    }

    public void setCalledFromOtherLanguage() {
        isCalledFromOtherLanguage = true;
    }

    public boolean isCalledFromOtherLanguage() {
        return isCalledFromOtherLanguage;
    }

    /**
     * @return the list of all directly imported modules.
     */
    public TapList<Unit> importedModules() {
        return importedModules;
    }

    /**
     * @return true if this Unit knows its source code, i.e. Flow Graph, etc...
     */
    public boolean hasSource() {
        return !isInterface() && !isVarFunction() && !isMadeForInline && entryBlock != null;
    }

    // End part: various minor boolean indicators.
    // Begin part: bookkeeping of memory zones:

    public ClassTypeSpec classTypeSpec() {
        return classTypeSpec;
    }

    public void setClassTypeSpec(ClassTypeSpec type) {
        classTypeSpec = type;
    }

    /**
     * @return the function type of this Unit.
     */
    public FunctionTypeSpec functionTypeSpec() {
        return functionTypeSpec;
    }

    /**
     * Sets the function type of this Unit.
     */
    public void setFunctionTypeSpec(FunctionTypeSpec functionTypeSpec) {
        this.functionTypeSpec = functionTypeSpec;
    }

    /**
     * @return the number of formal arguments of the function
     * associated to this Unit, as declared in the SymbolTable.
     */
    public int formalArgumentsNb() {
        if (functionTypeSpec == null || functionTypeSpec.argumentsTypes == null) {
            return 0;
        } else {
            return functionTypeSpec.argumentsTypes.length;
        }
    }

    public TypeSpec formalArgumentType(int rank) {
        if (functionTypeSpec == null || functionTypeSpec.argumentsTypes == null) {
            return null ;
        } else {
            return functionTypeSpec.argumentsTypes[rank-1] ;
        }
    }

    /**
     * @return the header of the current procedure Unit,
     * in the form of a tree with top operator "call".
     */
    public Tree headTree() {
        Tree result;
        if (entryBlock != null) {
            result = entryBlock.headTree();
        } else {
            result = ILUtils.buildCall(
                    ILUtils.build(ILLang.op_ident, name),
                    parametersOrModuleNameTree);
        }
        return result;
    }

    /**
     * @return the entry block.
     */
    public EntryBlock entryBlock() {
        return entryBlock;
    }

    /**
     * Sets the entry block.
     */
    public void setEntryBlock(EntryBlock eb) {
        entryBlock = eb;
    }

    /**
     * Sets the entry block.
     */
    public void setInitConstructorBlock(InitConstructorBlock ib) {
        initConstructorBlock = ib;
    }

    /**
     * @return the exit Block.
     */
    public ExitBlock exitBlock() {
        return exitBlock;
    }

    /**
     * Sets the exit Block.
     */
    public void setExitBlock(ExitBlock eb) {
        exitBlock = eb;
    }

    /**
     * @return the DFST-ordered list of all top-level Blocks.
     * i.e. the Blocks that are not enclosed in any loop.
     * This list does not contain the EntryBlock nor the ExitBlock.
     */
    public TapList<Block> topBlocks() {
        return topBlocks;
    }

    /**
     * Add a block to allBlock. This will break Flow Graph numbering,
     * therefore it will have to be re-made later.
     */
    public void addBlock(Block block) {
        allBlocks = new TapList<>(block, allBlocks);
    }

    /**
     * @return the DFST-ordered list of all Blocks.
     * This list does not contain the EntryBlock nor the ExitBlock.
     */
    public TapList<Block> allBlocks() {
        return allBlocks;
    }

    /**
     * @return the Flow-Graph's arrow of given "rank".
     */
    public FGArrow getArrow(int rank) {
        return (FGArrow) TapList.nth(allArrows, rank);
    }

    /**
     * Adds "tree" to the list of all Tree's proved dead because unreachable.
     */
    public void addDeadTree(Tree tree) {
        deadTrees = new TapList<>(tree, deadTrees);
    }

    /**
     * @return the list of all Tree's proved dead because unreachable.
     */
    public TapList<Tree> deadTrees() {
        return deadTrees;
    }

    /**
     * Adds formatTree to the list of format Trees of this Unit.
     */
    public void addFormatTree(Tree formatTree) {
        formats = new TapList<>(formatTree, formats);
    }

    public void addNonFlowInstruction(Instruction instr) {
        nonFlowInstructions = TapList.addLast(nonFlowInstructions, instr);
    }

    // End part: bookkeeping of memory zones.
    // Begin part: standard control-flow info on this Unit:

    /**
     * @return the list of format Trees of this Unit.
     */
    public TapList<Tree> formats() {
        return formats;
    }

    /**
     * Adds "tree" to the list of all comments lost while building this Unit.
     */
    public void addLostComment(Tree tree) {
        lostComments = TapList.addLast(lostComments, tree);
    }

    /**
     * @return the list of all comments lost while building this Unit.
     */
    public TapList<Tree> lostComments() {
        return lostComments;
    }

    /**
     * Sets the list of all comments lost while building this Unit.
     */
    public void setLostComments(TapList<Tree> lostComments) {
        this.lostComments = lostComments;
    }

    public void appendPostComments(TapList<Tree> newComments) {
        if (newComments != null) {
            postComments = ILUtils.appendComments(postComments, newComments, ILLang.op_comments);
        }
    }

    /**
     * @return Boolean(true) if this Unit is a package (of the source CallGraph) for which
     * there will be a differentiated package Unit. Also returns true if this Unit is this
     * corresponding differentiated package Unit (of the differentiated Call Graph).
     * This is set to true when the new module contains something new in addition
     * to the contents of the primal, such as the declaration of some differentiated objects.
     * Otherwise, this is also set to true when TapEnv.get().stripPrimalModules is false.
     */
    public Boolean isDiffPackage() {
        return isDiffPackage;
    }

    public void setIsDiffPackage(boolean isDiff) {
        isDiffPackage = isDiff;
        if (origUnit != null) {
            origUnit.isDiffPackage = isDiff;
        }
    }

    /**
     * (Hack) Name of another unit (if any) that "corresponds" to this Unit.
     * Used for two-way source-code correspondence e.g. for HTML display.
     */
    public Unit otherName() {
        return otherName;
    }

    public void setOtherName(Unit otherUnit) {
        otherName = otherUnit;
    }

    /**
     * @return true when this Unit is (in particular) differentiated as a context to call diff code.
     */
    public boolean isAContext() {
        TapList<ActivityPattern> inActivityPatterns = activityPatterns;
        boolean isContext = false;
        while (!isContext && inActivityPatterns != null) {
            isContext = inActivityPatterns.head.isContext();
            inActivityPatterns = inActivityPatterns.tail;
        }
        return isContext;
    }

    /**
     * @return one Directive of kind "kind" in the list of AD directives attached to this Unit.
     */
    public Directive hasDirective(int kind) {
        return Directive.hasDirective(directives, kind);
    }

    public boolean mustDifferentiateJoint() {
        return maybeCheckpointed;
    }

    public boolean mustDifferentiateSplit() {
        return maybeNoCheckpointed;
    }

    /**
     * @return true if this unit has predefined derivatives,
     * e.g. from an external specification library.
     */
    public boolean hasPredefinedDerivatives() {
        return (isIntrinsic() || isExternal() || isOutsideTBD()) && diffInfo != null;
    }

    /**
     * @return For C, for an EXTERNAL Unit, the C-style forward declaration instruction.
     */
    public Instruction getForwardDeclaration() {
        return forwardDeclaration;
    }

    /**
     * For C, for an EXTERNAL Unit, set the C-style forward declaration instruction.
     */
    public void setForwardDeclaration(Instruction instr) {
        forwardDeclaration = instr;
    }

    /**
     * @return true for procedures that F90 can use as elemental,
     * i.e. on an array of arguments of the expected type.
     */
    public boolean isElemental() {
        //[llh] TODO clarify difference with field "usedAsElementalResult" ?
        boolean elemental = false;
        if (isFortran()) {
            elemental = isIntrinsic() && !isIntrinsicNotElemental();
        }
        if (isFortran9x() && !elemental) {
            if (modifiers != null) {
                Tree subTree;
                int i = 1;
                while (!elemental && i <= modifiers.length()) {
                    subTree = modifiers.down(i);
                    if (subTree.opCode() == ILLang.op_ident) {
                        elemental = subTree.stringValue().equals("elemental");
                    }
                    i = i + 1;
                }
            }
        }
        return elemental;
    }

    public boolean isIntrinsicNotElemental() {
        boolean result = isFortran() && isIntrinsic();
        if (result) {
            result = name.equals("all") || name.equals("any")
                    || name.equals("count") || name.equals("maxval")
                    || name.equals("minloc")
                    || name.equals("minval") || name.equals("present")
                    || name.equals("product") || name.equals("size")
                    || name.equals("sum")
                    || name.equals("associated")
                    || name.equals("lbound") || name.equals("ubound")
                    || name.equals("reshape")
                    || name.equals("dot_product");
        }
        return result;
    }

    /**
     * @return true if this Unit comes from a C include file.
     */
    public boolean inStdCIncludeFile() {
        return inStdCIncludeFile;
    }

    /**
     * Declares that this Unit comes from a C include file.
     */
    public void setInStdCIncludeFile() {
        inStdCIncludeFile = true;
    }

    /**
     * @param parameterRank rank of this parameter, from 1 to nbArgs
     * @return True if this unit, when called by another unit of language "callerLanguage",
     * gets the parameter of rank parameterRank by value.
     */
    public boolean takesArgumentByValue(int parameterRank, int callerLanguage) {
        if (parameterRank <=0 || parameterRank > formalArgumentsNb()) {
            return false ;
        } else if (TapEnv.isFortran(callerLanguage) && this.isC()) { // Unsatisfactory patch!
            return true ;
        } else {
            VariableDecl paramVarDecl = null ;
            // Try various means of finding this parameter's VariableDecl. First using zones:
            TapIntList zones = (argsPublicRankTrees==null ? null
                                : ZoneInfo.listAllZones(argsPublicRankTrees[parameterRank], true)) ;
            while (zones!=null && paramVarDecl==null) {
                if (paramElemZoneInfo(zones.head).ownerSymbolDecl instanceof VariableDecl)
                    paramVarDecl = (VariableDecl)paramElemZoneInfo(zones.head).ownerSymbolDecl ;
                zones = zones.tail;
            }
            // If paramVarDecl not found, try using parameter name:
            if (paramVarDecl==null) {
                Tree formalArg = ILUtils.getArguments(headTree()).down(parameterRank) ;
                String formalName = (formalArg==null ? null : ILUtils.baseName(formalArg)) ;
                if (formalName!=null) {
                    paramVarDecl = publicSymbolTable.getTopVariableDecl(formalName) ;
                    if (paramVarDecl==null) {
                        NewSymbolHolder newSH = NewSymbolHolder.getNewSymbolHolder(formalArg);
                        if (newSH != null) {
                            paramVarDecl = newSH.newVariableDecl;
                        }
                    }
                }
            }
            if (paramVarDecl!=null) {
                return paramVarDecl.passesByValue(this);
            } else {
                // If everything else fails, just look at languages:
                return MixedLanguageInfos.passesByValue(callerLanguage, language) ;
            }
        }
    }

    /**
     * @return true if we allow for array notation ("vector" notation) in this Unit.
     */
    public boolean hasArrayNotation() {
        return isFortran9x() || hasArrayNotation;
    }

    /**
     * @return true if this Unit is a function, i.e. return type is not 'void'.
     */
    public boolean isAFunction() {
        if (functionTypeSpec != null) {
            return functionTypeSpec.isAFunction();
        } else {
            TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "Undefined functionTypeSpec for procedure " + this);
            return false;
        }
    }

    /**
     * @return true when this Unit contains info on its elementary parameters.
     */
    public boolean hasParamElemsInfo() {
        return externalShape != null;
    }

    /**
     * @return the number of elementary parameters of this Unit.
     */
    public int paramElemsNb() {
        return (externalShape==null ? 0 : externalShape.length);
    }

    /**
     * @return the ZoneInfo of the i-th elementary parameter of this Unit.
     */
    public ZoneInfo paramElemZoneInfo(int i) {
        return externalShape[i];
    }

    // End part: standard control-flow info on this Unit.
    // Begin part: standard data-flow info on this Unit:

    /** Returns the "possibly read" and "possibly written" info about one given checkpointed code fragment,
     * here represented by its "main" Block "ckpBlock" */
    public TapPair<BoolVector, BoolVector> fragmentInOutCkp(Block ckpBlock) {
        return TapList.cassq(ckpBlock, fragmentInOutCkp) ;
    }

    public void setFragmentInOutCkp(Block ckpBlock, TapPair<BoolVector, BoolVector> info) {
        TapPair<Block, TapPair<BoolVector, BoolVector>> found =
            TapList.assq(ckpBlock, fragmentInOutCkp) ;
        if (found==null) {
            found = new TapPair<>(ckpBlock, null) ;
            fragmentInOutCkp = new TapList<>(found, fragmentInOutCkp) ;
        }
        found.second = info ;
    }

    public void setAllocDealloc() {
        containsAllocDealloc = true;
    }

    /**
     * Fill this.testZonesNb, Block.testZone
     */
    public void computeTestZones() {
        BlockStorage<BoolVector> fsStorage;
        BlockStorage<BoolVector> storage;
        BoolVector tempVector;
        TapList<Block> inBlocks;
        boolean[] fgVector;
        Block block;
        if (!hasSource()) {
            return;
        }
        // Initialization:
        tempVector = buildBitVectorOnBlocks();
        fsStorage = forcedSuccessorAnalysis();
        storage = new BlockStorage<>(this);
        for (int i = 0; i < nbBlocks + 2; i++) {
            storage.storage[i] = buildBitVectorOnBlocks();
        }
        fgVector = allocateFlowGraphVector();
        // Flowgraph traversal:
        fixpoint:
        for (; ; ) {
            for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
                block = inBlocks.head;
                if (marked(fgVector, block)) {
                    mark(fgVector, block, false);
                    if (computeControllersForBlock(block, tempVector, fsStorage, storage) &&
                            markDownStreamBlock(fgVector, block, true)) {
                        continue fixpoint;
                    }
                }
            }
            break;
        }
        // Now allocate the test zones:
        allocateTestZones(storage);
        TapList<Block> inBlocks2;
        Block controlBlock;
        Block aBlock;
        BoolVector controlVector;

        // Now fill the fields controllers and controllingTestZones:
        for (inBlocks = allBlocks(); inBlocks != null; inBlocks = inBlocks.tail) {
            aBlock = inBlocks.head;
            controlVector = storage.retrieve(aBlock);
            if (!controlVector.isFalse(nbBlocks + 2)) {
                for (inBlocks2 = allBlocks(); inBlocks2 != null; inBlocks2 = inBlocks2.tail) {
                    controlBlock = inBlocks2.head;
                    if (getBitForBlock(controlVector, controlBlock)) {
                        aBlock.controllers = new TapList<>(controlBlock, aBlock.controllers);
                        aBlock.controllersZone = new TapIntList(controlBlock.testZone, aBlock.controllersZone);
                    }
                }
            }
        }
    }

    /**
     * Initialize the forced successor data for test blocks.
     */
    private BlockStorage<BoolVector> forcedSuccessorAnalysis() {
        BlockStorage<BoolVector> storage;
        BlockStorage<BoolVector> result;
        TapList<Block> inBlocks;
        TapList<Block> reverseBlocks;
        BoolVector bVectorForExitBlock;
        BoolVector resultBv;
        BoolVector fs;
        BoolVector tempVector;
        Block b;
        boolean[] fgVector;

        if (!hasSource()) {
            return null;
        }
        reverseBlocks = TapList.reverse(allBlocks);

        // Init
        result = new BlockStorage<>(this);
        storage = new BlockStorage<>(this);

        // For exit block
        bVectorForExitBlock = buildBitVectorOnBlocks();
        setBitForBlock(bVectorForExitBlock, exitBlock, true);
        storage.store(exitBlock(), bVectorForExitBlock);

        // For all other blocks
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            tempVector = buildBitVectorOnBlocks();
            tempVector.setTrue();
            storage.store(inBlocks.head, tempVector);
        }
        tempVector = buildBitVectorOnBlocks();
        tempVector.setTrue();
        // Fixpoint iteration of flowgraph
        fgVector = allocateFlowGraphVector();
        fixpoint:
        for (; ; ) {
            for (inBlocks = reverseBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
                b = inBlocks.head;
                if (marked(fgVector, b)) {
                    mark(fgVector, b, false);
                    if (fsThroughBlock(inBlocks.head, storage, tempVector, exitBlock) &&
                            markUpStreamBlock(fgVector, b, true)) {
                        continue fixpoint;
                    }
                }
            }
            break;
        }
        // Initialize result
        for (int i = 1; i < nbBlocks + 2; i++) {
            result.storage[i] = new BoolVector(nbBlocks + 2);
        }
        // Summarize the data computed
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            b = inBlocks.head;
            if (b.isControl()) {

                fs = storage.retrieve(b);
                for (int i = 1; i < nbBlocks + 2; i++) {
                    if (fs.get(i) && i - 1 != b.rank) {
                        resultBv = (BoolVector) result.storage[i];
                        setBitForBlock(resultBv, b, true);
                    }
                }

            }
        }
        return result;
    }

    /**
     * This subroutine knowing the forced successor of each direct successor of block,
     * compute the forced successor of block.
     *
     * @param block the block concerned
     * @param store where to find the forced successor of other block
     * @param temp  a temporary
     * @return true if the information has changed
     */
    private boolean fsThroughBlock(Block block, BlockStorage<BoolVector> store, BoolVector temp, Block exitBlock) {
        TapList<FGArrow> inArrows;
        TapList<FGArrow> flowingArrows;
        FGArrow arrow;
        BoolVector oldValue;
        BoolVector newValue;
        BoolVector inputValue;


        oldValue = store.retrieve(block);
        flowingArrows = block.flow();
        // We want to do as if a "stop" was connected to the exitBlock.
        // This way, "I1" in "if(Test) then I1 else stop endif" will
        // be controlled by the Test !
        if (flowingArrows == null) {
            FGArrow dummyArrow = new FGArrow();
            dummyArrow.destination = exitBlock;
            flowingArrows = new TapList<>(dummyArrow, null);
        }
        newValue = temp;
        newValue.setTrue();
        for (inArrows = flowingArrows; inArrows != null; inArrows = inArrows.tail) {
            arrow = inArrows.head;
            inputValue = store.retrieve(arrow.destination);
            newValue.cumulAnd(inputValue);
        }
        setBitForBlock(newValue, block, true);
        if (!oldValue.equals(newValue, nbBlocks + 2)) {
            oldValue.setCopy(newValue);
            return true;
        }
        return false;
    }

    /**
     * For each block, accumulate the control vector coming from imcoming
     * arrows, add control by an incoming arrow if its origin block is a control,
     * then cut off the blocks of whom the current block is a forced successor.
     * @return true iff block's control vector changed, and therefore fixed-point is not reached.
     */
    private boolean computeControllersForBlock(Block block, BoolVector newValue,
                                               BlockStorage<BoolVector> fsStorage,
                                               BlockStorage<BoolVector> storage) {
        BoolVector forcedSuccessor;
        BoolVector oldValue;
        TapList<FGArrow> inArrows;
        Block upBlock;

        oldValue = storage.retrieve(block);
        forcedSuccessor = fsStorage.retrieve(block);
        newValue.setFalse();
        for (inArrows = block.backFlow();
             inArrows != null;
             inArrows = inArrows.tail) {
            upBlock = inArrows.head.origin;
            // Set control if the upBlock is a control.
            if (upBlock.isControl()) {
                setBitForBlock(newValue, upBlock, true);
            }
            // Accumulate incoming dependencies
            newValue.cumulOr(storage.retrieve(upBlock), nbBlocks + 2);
        }

        // Cut of the FS Vector
        if (forcedSuccessor != null) {
            newValue.cumulMinus(forcedSuccessor);
        }

        // If block is syntactically inside a loop nest, then we force these loop headers to control it:
        LoopBlock container = block.enclosingLoop();
        HeaderBlock containerHeader;
        while (container != null) {
            containerHeader = container.header();
            if (containerHeader != null) {
                setBitForBlock(newValue, containerHeader, true);
            }
            container = container.enclosingLoop();
        }
        // Similarly, force control by parallelControllerBlock's
        if (block.parallelControls != null) {
            Block parallelControllerBlock = block.parallelControls.head.block ;
            setBitForBlock(newValue, parallelControllerBlock, true);
        }

        // Save the result if it has changed since the last iteration
        if (!newValue.equals(oldValue, nbBlocks + 2)) {
            oldValue.setCopy(newValue);
            return true;
        }
        return false;
    }

    /**
     * Very long function trying to allocate a minimal number of testZones.
     */
    private void allocateTestZones(BlockStorage<BoolVector> storage) {
        // First calculate the number of block controlled by each block,
        // then for each controlling block the list of blocks that happens
        // to be controlling the same block.
        TapIntList[] coControlles = new TapIntList[nbBlocks];
        int[] nbOfControlledBlocks = new int[nbBlocks];
        TapList<Block> blocks;
        blocks = allBlocks;
        while (blocks != null) {
            Block block = blocks.head;
            BoolVector bv = storage.retrieve(block);
            for (int i = 1; i < nbBlocks + 1; i++) {
                if (bv.get(i)) {
                    nbOfControlledBlocks[i - 1]++;
                    for (int j = i + 1; j < nbBlocks + 1; j++) {
                        if (bv.get(j)) {
                            if (coControlles[i - 1] == null ||
                                    !TapIntList.contains(coControlles[i - 1], j - 1)) {
                                coControlles[i - 1] = new TapIntList(j - 1, coControlles[i - 1]);
                            }
                            if (coControlles[j - 1] == null ||
                                    !TapIntList.contains(coControlles[j - 1], i - 1)) {
                                coControlles[j - 1] = new TapIntList(i - 1, coControlles[j - 1]);
                            }
                        }
                    }
                }
            }
            blocks = blocks.tail;
        }
        // Now order the control blocks by their number of controlling blocks
        TapList<Block> controllingBlocks = null;
        blocks = allBlocks;
        while (blocks != null) {
            Block b = blocks.head;
            if (b.isControl()) {
                // Find the place in the list:
                if (controllingBlocks == null) {
                    controllingBlocks = new TapList<>(b, null);
                } else {
                    TapList<Block> appendTo = controllingBlocks;
                    TapList<Block> last = null;
                    while (nbOfControlledBlocks[appendTo.head.rank] >=
                            nbOfControlledBlocks[b.rank] && appendTo.tail != null) {
                        last = appendTo;
                        appendTo = appendTo.tail;
                    }
                    if (nbOfControlledBlocks[appendTo.head.rank] <
                            nbOfControlledBlocks[b.rank]) {
                        if (last != null) {
                            last.tail = new TapList<>(b, appendTo);
                        } else {
                            controllingBlocks = new TapList<>(b, appendTo);
                        }
                    } else {
                        appendTo.tail = new TapList<>(b, appendTo.tail);
                    }
                }
            }
            blocks = blocks.tail;
        }
        // Now compute the maximum number of nested op_where's
        blocks = allBlocks;
        TapList<Instruction> instructions;
        Instruction instruction;
        int maxDepth = 0;
        int depth;
        while (blocks != null) {
            Block b = blocks.head;
            instructions = b.instructions;
            while (instructions != null) {
                instruction = instructions.head;
                if (instruction.whereMask() != null) {
                    depth = instruction.whereMask().depth();
                    if (depth > maxDepth) {
                        maxDepth = depth;
                    }
                }
                instructions = instructions.tail;
            }
            blocks = blocks.tail;
        }

        // Now allocate the testZones
        int[] testZones = new int[nbBlocks + 2];
        testZonesNb = -1;
        for (int i = testZones.length - 1; i >= 0; --i) {
            testZones[i] = -1;
        }
        while (controllingBlocks != null) {
            Block b = controllingBlocks.head;
            TapIntList list = coControlles[b.rank];
            int max = -1;
            while (list != null) {
                int co = list.head;
                if (testZones[co + 1] > max) {
                    max = testZones[co + 1];
                }
                list = list.tail;
            }
            testZones[b.rank + 1] = max + 1;
            if (max + 1 > testZonesNb) {
                testZonesNb = max + 1;
            }
            controllingBlocks = controllingBlocks.tail;
        }
        testZonesNb = testZonesNb + 1 + maxDepth;

        // Place the data in the blocks
        blocks = allBlocks();
        while (blocks != null) {
            Block b = blocks.head;
            b.testZone = testZones[b.rank + 1];
            blocks = blocks.tail;
        }
    }

    /**
     * @return a boolean vector big enough for storing info on blocks of unit.
     */
    private boolean[] allocateFlowGraphVector() {
        boolean[] fgVector = new boolean[nbBlocks + 2];
        for (int i = fgVector.length - 1; i >= 0; --i) {
            fgVector[i] = true;
        }
        return fgVector;
    }

    /**
     * Compute usual informations on flowgraphs.
     * Called in CallGraph.terminateTypesAndZones().
     */
    protected void computeAllDominatorlikeInfo() {
        computePredecessors();
        computeSucessors();
        computeDominator();
        computePostDominator();
        //computeDominatorArrows();
        computeReachability();
    }

    /**
     * Fills the block.predecessor field of each block in unit,
     * with a boolvector representing the list of block which are
     * predecessor of the block.
     * A successor is a block present on a path from the block to an exit
     * of the subroutine.
     */
    private void computePredecessors() {
        boolean[] fgVector = allocateFlowGraphVector();
        TapList<Block> inBlocks;
        BoolVector tempVector;
        // Init
        tempVector = buildBitVectorOnBlocks();
        entryBlock.predecessor = buildBitVectorOnBlocks();
        exitBlock.predecessor = buildBitVectorOnBlocks();
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            inBlocks.head.predecessor = buildBitVectorOnBlocks();
        }
        // Flowgraph traversal
        fixpoint:
        for (; ; ) {
            for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
                if (marked(fgVector, inBlocks.head)) {
                    mark(fgVector, inBlocks.head, false);
                    tempVector.setFalse();
                    TapList<FGArrow> backFlow = inBlocks.head.backFlow();
                    for (; backFlow != null; backFlow = backFlow.tail) {
                        tempVector.cumulOr(backFlow.head.origin.predecessor);
                        setBitForBlock(tempVector, backFlow.head.origin, true);
                    }
                    if (!equalsBoolVector(inBlocks.head.predecessor, tempVector)) {
                        inBlocks.head.predecessor.setCopy(tempVector);
                        if (markDownStreamBlock(fgVector, inBlocks.head, true)) {
                            continue fixpoint;
                        }
                    }
                }
            }
            break;
        }
        // ExitBlock accumulation
        BoolVector exitBlockPredecessor = exitBlock.predecessor;
        TapList<FGArrow> backFlow = exitBlock.backFlow();
        for (; backFlow != null; backFlow = backFlow.tail) {
            exitBlockPredecessor.cumulOr(backFlow.head.origin.predecessor);
            setBitForBlock(exitBlockPredecessor, backFlow.head.origin, true);
        }
    }

    /**
     * Fills the block.successor field of each block in unit,
     * with a boolvector representing the list of block which
     * are successors  of the block.
     * A successor is a block present on a path from the block
     * to an exit of the subroutine.
     */
    private void computeSucessors() {
        boolean[] fgVector = allocateFlowGraphVector();
        TapList<Block> inBlocks;
        BoolVector tempVector;
        // Init
        tempVector = buildBitVectorOnBlocks();
        entryBlock.successor = buildBitVectorOnBlocks();
        exitBlock.successor = buildBitVectorOnBlocks();
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            inBlocks.head.successor = buildBitVectorOnBlocks();
        }
        // Mark all exitBlocks
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            if (inBlocks.head.flow() == null) {
                markUpStreamBlock(fgVector, inBlocks.head, true);
            }
        }
        // Flowgraph traversal
        fixpoint:
        for (; ; ) {
            for (inBlocks = TapList.reverse(allBlocks);
                 inBlocks != null;
                 inBlocks = inBlocks.tail) {
                if (marked(fgVector, inBlocks.head)) {
                    mark(fgVector, inBlocks.head, false);
                    tempVector.setFalse();
                    TapList<FGArrow> flow = inBlocks.head.flow();
                    for (; flow != null; flow = flow.tail) {
                        tempVector.cumulOr(flow.head.destination.successor);
                        setBitForBlock(tempVector, flow.head.destination, true);
                    }
                    if (!equalsBoolVector(inBlocks.head.successor, tempVector)) {
                        inBlocks.head.successor.setCopy(tempVector);
                        if (markUpStreamBlock(fgVector, inBlocks.head, true)) {
                            continue fixpoint;
                        }
                    }
                }
            }
            break;
        }
        // Entry block accumulation
        BoolVector entryBlockSuccessor = entryBlock.successor;
        if (entryBlock.flow() != null) {
            entryBlockSuccessor.setCopy(entryBlock.flow().head.destination.successor);
            setBitForBlock(entryBlockSuccessor, entryBlock.flow().head.destination, true);
        }
    }

    /**
     * Fills the block.postDominator field of each block in unit,
     * with a boolvector representing the list of block which are
     * postDominators (or forced successors) of the block.
     * Post dominators are blocks which are forced successors of a block,
     * i.e block present in every path from the block to the
     * exit of the subroutine (neglecting exceptional exits).
     */
    private void computePostDominator() {
        boolean[] fgVector = allocateFlowGraphVector();
        TapList<Block> inBlocks;
        BoolVector tempVector;
        tempVector = buildBitVectorOnBlocks();
        // Init the root
        exitBlock.postDominator = buildBitVectorOnBlocks();
        setBitForBlock(exitBlock.postDominator, exitBlock, true);
        // Init the rest
        entryBlock.postDominator = buildBitVectorOnBlocks();
        entryBlock.postDominator.setTrue();
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            inBlocks.head.postDominator = buildBitVectorOnBlocks();
            inBlocks.head.postDominator.setTrue();
        }
        // Flowgraph traversal
        fixpoint:
        for (; ; ) {
            for (inBlocks = TapList.reverse(allBlocks);
                 inBlocks != null;
                 inBlocks = inBlocks.tail) {
                if (marked(fgVector, inBlocks.head)) {
                    mark(fgVector, inBlocks.head, false);
                    TapList<FGArrow> flow = inBlocks.head.flow();
                    BoolVector workVector = inBlocks.head.postDominator;
                    tempVector.setCopy(workVector);
                    // PostDom(u) = \inter_(v \in succ(u)) PostDom(v) U { u }
                    for (; flow != null; flow = flow.tail) {
                        workVector.cumulAnd(flow.head.destination.postDominator);
                    }
                    setBitForBlock(workVector, inBlocks.head, true);
                    // If changed, iterate again on upstream:
                    if (!equalsBoolVector(workVector, tempVector)) {
                        if (markUpStreamBlock(fgVector, inBlocks.head, true)) {
                            continue fixpoint;
                        }
                    }
                }
            }
            break;
        }
        // Acumulate for entryBlocks
        BoolVector workVector = entryBlock.postDominator;
        if (entryBlock.flow() != null) {
            workVector.setCopy(entryBlock.flow().head.destination.postDominator);
            setBitForBlock(workVector, entryBlock.flow().head.destination, true);
        }
        // Find immediate postdominator
        if (allBlocks != null) {
            Block[] blocks = toObjectArray(allBlocks());
            for (inBlocks = allBlocks;
                 inBlocks != null;
                 inBlocks = inBlocks.tail) {
                tempVector.setCopy(inBlocks.head.postDominator);
                setBitForBlock(tempVector, inBlocks.head, false);
                for (int i = 1; i < nbBlocks + 2; ++i) {
                    Block block;
                    if (i == nbBlocks + 1) {
                        block = exitBlock;
                    } else {
                        block = blocks[i - 1];
                    }
                    if (tempVector.get(i) && equalsBoolVector(block.postDominator, tempVector)) {
                        inBlocks.head.immediatePostDominator = block;
                        break;
                    }
                }
            }
        }
        if (entryBlock.flow() != null) {
            entryBlock.immediatePostDominator = entryBlock.flow().head.destination;
        }
    }

    /**
     * Fills the block.dominator field of each block in unit,
     * with a boolvector representing the list of block which are
     * dominators (or forced predecessor) of the block.
     * Dominators are blocks which are forced predecessor of a block,
     * i.e block present in every path from the entryBlock to this block.
     */
    private void computeDominator() {
        boolean[] fgVector = allocateFlowGraphVector();
        TapList<Block> inBlocks;
        BoolVector tempVector;
        tempVector = buildBitVectorOnBlocks();
        // Init the root
        entryBlock.dominator = buildBitVectorOnBlocks();
        setBitForBlock(entryBlock.dominator, entryBlock, true);
        // Init other blocks
        exitBlock.dominator = buildBitVectorOnBlocks();
        exitBlock.dominator.setTrue();
        for (inBlocks = allBlocks; inBlocks != null; inBlocks = inBlocks.tail) {
            inBlocks.head.dominator = buildBitVectorOnBlocks();
            inBlocks.head.dominator.setTrue();
        }
        for (int i = fgVector.length - 1; i >= 0; --i) {
            fgVector[i] = true;
        }
        // Flowgraph traversal
        fixpoint:
        for (; ; ) {
            for (inBlocks = allBlocks;
                 inBlocks != null;
                 inBlocks = inBlocks.tail) {
                if (marked(fgVector, inBlocks.head)) {
                    mark(fgVector, inBlocks.head, false);
                    TapList<FGArrow> backFlow = inBlocks.head.backFlow();
                    BoolVector workVector = inBlocks.head.dominator;
                    tempVector.setCopy(workVector);
                    // Dom(u) = \inter_(v \in pred(u)) Dom(v) U { u }
                    for (; backFlow != null; backFlow = backFlow.tail) {
                        workVector.cumulAnd(backFlow.head.origin.dominator);
                    }
                    setBitForBlock(workVector, inBlocks.head, true);
                    // si un seul successeur, c'est un successeur oblige:
                    if (!equalsBoolVector(workVector, tempVector)) {
                        if (markDownStreamBlock(fgVector, inBlocks.head, true)) {
                            continue fixpoint;
                        }
                    }
                }
            }
            break;
        }
        // Acumulate for exitBlock
        BoolVector workVector = exitBlock.dominator;
        TapList<FGArrow> backFlow = exitBlock.backFlow();
        // accumule l'intersection de tous les successeurs oblige de ce noeuds */
        for (; backFlow != null; backFlow = backFlow.tail) {
            workVector.cumulAnd(backFlow.head.origin.dominator);
        }
        setBitForBlock(workVector, exitBlock, true);
        // Find immediate dominator
        if (allBlocks() != null) {
            Block[] blocks = toObjectArray(allBlocks());
            for (inBlocks = allBlocks;
                 inBlocks != null;
                 inBlocks = inBlocks.tail) {
                tempVector.setCopy(inBlocks.head.dominator);
                if (inBlocks.head.rank == 0) {
                    inBlocks.head.immediateDominator = entryBlock;
                    // iDom = entryBlock
                } else {
                    setBitForBlock(tempVector, inBlocks.head, false);
                    for (int i = 1; i < nbBlocks + 1; ++i) {
                        if (tempVector.get(i)
                                && equalsBoolVector(blocks[i - 1].dominator, tempVector)) {
                            inBlocks.head.immediateDominator = blocks[i - 1];
                            break;
                        }
                    }
                }
            }
            tempVector.setCopy(exitBlock.dominator);
            setBitForBlock(tempVector, exitBlock, false);
            for (int i = 1; i < nbBlocks + 1; ++i) {
                if (tempVector.get(i) && equalsBoolVector(blocks[i - 1].dominator, tempVector)) {
                    exitBlock.immediateDominator = blocks[i - 1];
                    break;
                }
            }
        } else {
            exitBlock.immediateDominator = entryBlock;
        }
    }

    // End part: standard data-flow info on this Unit.

    public void computeDominatorArrows() {
        TapList<Block> inAllBlocks = allBlocks;
        Block block;
        int nbOfArrows = nbArrows;
        BoolVector boolVect;
        // Initialize the dominators:
        entryBlock.setDominatorArrows(new BoolVector(nbOfArrows));
        entryBlock.setControlStruct(null);
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            boolVect = new BoolVector(nbOfArrows);
            boolVect.setTrue();
            block.setDominatorArrows(boolVect);
            block.setControlStruct(null);
            inAllBlocks = inAllBlocks.tail;
        }
        exitBlock.setDominatorArrows(new BoolVector(nbOfArrows));
        exitBlock.setControlStruct(null);
        // Run the dominator analysis:
        BoolVector domIn;
        boolean runAgain = true;
        while (runAgain) {
            runAgain = false;
            inAllBlocks = allBlocks;
            while (inAllBlocks != null) {
                block = inAllBlocks.head;
                domIn = accumulateDomIn(block, nbOfArrows);
                if (!block.getDominatorArrows().equals(domIn, nbOfArrows)) {
                    block.setDominatorArrows(domIn);
                    runAgain = true;
                }
                inAllBlocks = inAllBlocks.tail;
            }
        }
        if (!isFortran()) {
            // If language allows for local scopes, make sure that each scope
            // that starts in a control structure is actually fully included in this structure.
            SymbolTable blockScope ;
            TapList<TapPair<SymbolTable, BoolVector>> dominatorsOfScopes = null ;
            inAllBlocks = allBlocks;
            while (inAllBlocks != null) {
                block = inAllBlocks.head;
                blockScope = block.symbolTable ;
                if (blockScope != null && blockScope.declarationsBlock==block)
                    dominatorsOfScopes =
                        new TapList<>(new TapPair(blockScope, block.getDominatorArrows()),
                                      dominatorsOfScopes) ;
                inAllBlocks = inAllBlocks.tail;
            }
            inAllBlocks = allBlocks;
            BoolVector scopeDominatorArrows ;
            while (inAllBlocks != null) {
                block = inAllBlocks.head;
                blockScope = block.symbolTable ;
                while (blockScope!=null && blockScope!=bodySymbolTable()) {
                    scopeDominatorArrows = TapList.cassq(blockScope, dominatorsOfScopes) ;
                    if (scopeDominatorArrows!=null) {
                        block.getDominatorArrows().cumulOr(scopeDominatorArrows) ;
                    }
                    blockScope = blockScope.basisSymbolTable() ;
                }
                inAllBlocks = inAllBlocks.tail;
            }
        }
// if ("foo_bwd".equals(name)) {
// TapList<Block> debugBlocks = allBlocks;
// while (debugBlocks != null) {
//   Block debugBlock = debugBlocks.head;
//   System.out.println("DOMINATORS OF BLOCK "+(debugBlock.rank<10 ? " " : "")+debugBlock+" : "+debugBlock.getDominatorArrows()) ;
//   debugBlocks = debugBlocks.tail;
// }
// }
    }

    /**
     * @return the arrow dominators at the beginning of "block",
     * by making the intersection of all dominators that might
     * arrive through each incoming control-flow arrow (FGArrow).
     */
    private BoolVector accumulateDomIn(Block block, int nbOfArrows) {
        Block origin;
        BoolVector domIn;
        BoolVector domIncoming;
        domIn = new BoolVector(nbOfArrows);
        domIn.setTrue();
        TapList<TapPair<Integer, LoopBlock>> inIter;
        LoopBlock loopBlock;
        TapList<FGArrow> arrows = block.backFlow();
        FGArrow fgArrow;
        FGArrow loopArrow;
        // AND-accumulate the dominators incoming through
        // each FGArrow into domIn:
        while (arrows != null) {
            fgArrow = arrows.head;
            origin = fgArrow.origin;
            domIncoming = origin.getDominatorArrows().copy();
            // The following 4 lines are modified to cope with namespaces:
            if (fgArrow.goesInsideControl()) {
                domIncoming.set(fgArrow.rank, true);
            } else {
                removeExitedNamespacesAndParallelControls(fgArrow, domIncoming);
            }
            // Look at the loops traversed by this FGArrow:
            inIter = fgArrow.iter;
            loopBlock = null;
            // When this arrow is jumping out of a nest of loops,
            //  the domIncoming is the one arriving at the topmost loop's entry :
            while (inIter != null
                    && inIter.head.first.equals(FGConstants.POP)) {
                if (inIter.head.second
                        .header() != block) {
                    loopBlock = inIter.head.second;
                }
                inIter = inIter.tail;
            }
            if (loopBlock != null) {
                domIncoming = accumulateDomIn(loopBlock.header(), nbOfArrows);
            }
            // Make sure we are not dominated by structured control which are finished:
            removeExitedNamespacesAndParallelControls(fgArrow, domIncoming);
            // When this arrow is jumping into a nest of loops
            // in an abnormal way (doesn't jump to a do-loop header),
            // then force it to be controlled by the loop entry:
            //  this will keep the original syntax :
            while (inIter != null) {
                if (inIter.head.first.equals(FGConstants.PUSH)) {
                    loopBlock = inIter.head.second;
                    if (block != loopBlock.header()) {
                        loopArrow = loopBlock.header().getFGArrowTestCase(
                                FGConstants.LOOP, FGConstants.NEXT);
                        if (loopArrow != null) {
                            domIncoming.set(loopArrow.rank, true);
                        }
                    }
                }
                inIter = inIter.tail;
            }
            // When this arrow is a "jumpIntoNextCase" in a switch,
            // then don't intersect with its dominators, because
            // this would make the current block exterior to the switch.
            if (fgArrow.isAJumpIntoNextCase) {
                domIncoming = null;
            }
            if (domIncoming != null) {
                domIn.cumulAnd(domIncoming);
            }
            arrows = arrows.tail;
        }
        return domIn;
    }

    // For namespaces, parallel controls, and in general single-entry-single-exit structured constructs:
    // Sweeps from the given arrow rank down to arrow rank 0, and when the given arrow exits from
    // a structured construct, removes domination by this structured construct's entry arrow.
    private void removeExitedNamespacesAndParallelControls(FGArrow arrow, BoolVector domVector) {
        SymbolTable commonRoot = arrow.origin.symbolTable.getCommonRoot(arrow.destination.symbolTable);
        FGArrow oneArrow;
        for (int arrowIndex = arrow.rank - 1; arrowIndex >= 0; --arrowIndex) {
            if (domVector.get(arrowIndex)) {
                oneArrow = getArrow(arrowIndex);
                if (oneArrow.origin.isANameSpace() &&
                        !commonRoot.nestedIn(oneArrow.destination.symbolTable)
                        ||
                        oneArrow.origin.isParallelController() &&
                                !TapList.contains(arrow.destination.parallelControls, oneArrow.origin.lastInstr())) {
                    domVector.set(arrowIndex, false);
                }
            }
        }
    }

    /**
     * Implement this algorithm:
     * <p>
     * -  For each arrow depending on a test,
     * compute if the test has a constant
     * result.
     * <p>
     * - If a constant result if found, mark
     * the non reachable arrow as such.
     * <p>
     * - For each block with only non-reachable arrows,
     * mark all blocks dominated by this one as
     * non reachable.
     */
    private void computeReachability() {
        TapList<Block> inBlocks;
        // Compute reachability for arrows:
        for (inBlocks = allBlocks;
             inBlocks != null;
             inBlocks = inBlocks.tail) {
            Block block = inBlocks.head;
            if (block.lastInstr() != null && block.lastInstr().tree != null &&
                    block.lastInstr().tree.opCode() == ILLang.op_if) {
                Object value = block.symbolTable.staticValue(block.lastInstr().tree.down(1));
                if (value instanceof Boolean) {
                    Boolean v = (Boolean) value;
                    int test = v ? FGConstants.FALSE : FGConstants.TRUE;
                    TapList<FGArrow> nonReachableArrow = findFlowArrow(block, test);
                    for (; nonReachableArrow != null; nonReachableArrow = nonReachableArrow.tail) {
                        nonReachableArrow.head.reachable = false;
                    }
                }
            }
        }
        // Now find Block without a reachable incoming arrow:
        for (inBlocks = allBlocks;
             inBlocks != null;
             inBlocks = inBlocks.tail) {
            Block block = inBlocks.head;
            if (allIncomingArrowsNonReachable(block)) {
                setForcedSuccessorsAsNonReachable(block);
            }
        }
    }

    private boolean allIncomingArrowsNonReachable(Block origin) {
        TapList<FGArrow> backFlow = origin.backFlow();
        for (; backFlow != null; backFlow = backFlow.tail) {
            if (backFlow.head.reachable) {
                return false;
            }
        }
        return true;
    }

    private void setForcedSuccessorsAsNonReachable(Block block) {
        TapList<Block> inBlocks;
        block.reachable = false;
        for (inBlocks = allBlocks;
             inBlocks != null;
             inBlocks = inBlocks.tail) {
            if (getBitForBlock(inBlocks.head.dominator, block)) {
                inBlocks.head.reachable = false;
            }
        }
    }

    /**
     * Convert a TapList&lt;Block&gt; to an array of Block,
     * used if you access the list by the ranks.
     */
    private Block[] toObjectArray(TapList<Block> blocks) {
        int s = TapList.length(blocks);
        Block[] array = new Block[s];
        int u = 0;
        while (blocks != null) {
            array[u] = blocks.head;
            blocks = blocks.tail;
            u++;
        }
        return array;
    }

    private BoolVector buildBitVectorOnBlocks() {
        return new BoolVector(nbBlocks + 2);
    }

    /**
     * Mark the block block as a forced successor in bVector.
     */
    private void setBitForBlock(BoolVector bVector, Block block, boolean value) {
        bVector.set(block.rank + 1, value);
    }

    /**
     * @return true if b1 and b2 are equal in the context of block.
     */
    private boolean equalsBoolVector(BoolVector b1, BoolVector b2) {
        return b1.equals(b2, nbBlocks + 2);
    }

    /**
     * @return the ordered list of the Blocks that are
     * designated by the given BoolVector "bv".
     * There is no verification that bv corresponds to a bitvector for
     * blocks in this Unit.
     */
    public TapList<Block> blocksFromBitVector(BoolVector bv) {
        TapList<Block> result = null;
        if (bv != null) {
            if (getBitForBlock(bv, exitBlock)) {
                result = new TapList<>(exitBlock, result);
            }
            TapList<Block> reverseBlocks = TapList.reverse(allBlocks);
            while (reverseBlocks != null) {
                if (getBitForBlock(bv, reverseBlocks.head)) {
                    result = new TapList<>(reverseBlocks.head, result);
                }
                reverseBlocks = reverseBlocks.tail;
            }
            if (getBitForBlock(bv, entryBlock)) {
                result = new TapList<>(entryBlock, result);
            }
        }
        return result;
    }

    /**
     * In-Out data-flow info: vector of zones possibly/partly not accessed.
     */
    public BoolVector unitInOutN() {
        return unitInOutN;
    }

    /**
     * In-Out data-flow info: vector of zones possibly/partly read and not written.
     */
    public BoolVector unitInOutR() {
        return unitInOutR;
    }

    /**
     * In-Out data-flow info: vector of zones possibly/partly not read only written.
     */
    public BoolVector unitInOutW() {
        return unitInOutW;
    }

    /**
     * In-Out data-flow info: vector of zones possibly/partly read then written.
     */
    public BoolVector unitInOutRW() {
        return unitInOutRW;
    }

    /**
     * TBR info: vector of zones possibly becoming TBR when going through this Unit,
     * in other words zones used non-linearly and therefore needed in derivatives.
     */
    public BoolVector unitTBR() {
        return unitTBR;
    }

    /**
     * @return the vector of zones that are certainly completely read by this Unit.
     */
    public BoolVector unitInOutCertainlyR() {
        return unitInOutN.not().minus(unitInOutW);
    }

    /**
     * @return the vector of zones that are certainly completely written by this Unit.
     */
    public BoolVector unitInOutCertainlyW() {
        if (unitInOutN == null) {
            return null;
        } else {
            return unitInOutN.not().minus(unitInOutR);
        }
    }

    /**
     * @return the vector of zones that are possibly read by this Unit.
     */
    public BoolVector unitInOutPossiblyR() {
        if (unitInOutR == null) {
            return unitInOutRW;
        } else {
            return unitInOutR.or(unitInOutRW);
        }
    }

    /**
     * @return the vector of zones that are possibly written by this Unit.
     */
    public BoolVector unitInOutPossiblyW() {
        if (unitInOutW == null) {
            return unitInOutRW;
        } else {
            return unitInOutW.or(unitInOutRW);
        }
    }

    /**
     * @return the vector of zones that are possibly not written by this Unit.
     */
    public BoolVector unitInOutPossiblyNotW() {
        if (unitInOutN == null) {
            return unitInOutR;
        } else {
            return unitInOutN.or(unitInOutR);
        }
    }

    /**
     * @return the vector of zones that are possibly read or written by this Unit.
     */
    public BoolVector unitInOutPossiblyRorW() {
        return unitInOutR.or(unitInOutW).or(unitInOutRW);
    }

    /**
     * Prints a human-readable description of all the data-flow info found on this Unit.
     */
    public void dumpDataFlow(
            boolean dumpPointers, boolean dumpInOut,
            boolean dumpDeps, boolean dumpInside) throws IOException {
        TapList<Block> blocks;
        Block block;
        Instruction instruction;
        TapList<Instruction> instructions;
        BlockStorage<TapList<BoolVector>> activities;
        BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> livenesses;
        BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> livenessesCkp;
        BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> overwrites;
        BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> overwritesCkp;
        BlockStorage<TapList<TapPair<BoolVector, BoolVector>>> tbrs;
        BlockStorage<TapList<TapPair<TapList<Instruction>, TapList<Instruction>>>> recomps;
        BlockStorage<TapList<BoolVector>> reqXs;
        BlockStorage<TapList<BoolVector>> avlXs;
        TapList<BoolVector> instrsActivities;
        TapList<TapPair<TapList<Instruction>, TapList<Instruction>>> instrsRecomps;
        TapList<BoolVector> instrsReqXs;
        TapList<BoolVector> instrsAvlXs;
        TapList<TapPair<BoolVector, BoolVector>> instrsLivenesses;
        TapList<TapPair<BoolVector, BoolVector>> instrsLivenessesCkp;
        TapList<TapPair<BoolVector, BoolVector>> instrsOverwrites;
        TapList<TapPair<BoolVector, BoolVector>> instrsOverwritesCkp;
        TapList<TapPair<BoolVector, BoolVector>> instrsTBRs;
        BlockStorage<TapList<BoolVector>> usefulnesses;
        TapList<BoolVector> instrsUsefulnesses;
        SymbolTable symbolTable;
        MultithreadAnalyzer multithreadAnalyzer = TapEnv.multithreadAnalyzer() ;

        int unitAllLen, unitDiffLen, unitPtrLen ;
        if (publicZonesNumber4!=null) { //TODO: this should become the only case!
            unitAllLen  = publicZonesNumber(SymbolTableConstants.ALLKIND);
            unitDiffLen = publicZonesNumber(TapEnv.diffKind());
            unitPtrLen  = publicZonesNumber(SymbolTableConstants.PTRKIND);
        } else {
            unitAllLen  = publicSymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND);
            unitDiffLen = publicSymbolTable().declaredZonesNb(TapEnv.diffKind());
            unitPtrLen  = publicSymbolTable().declaredZonesNb(SymbolTableConstants.PTRKIND);
        }
        String unitAllMapName  = "[x" + this.rank() + ":a]";
        String unitDiffMapName = "[x" + this.rank() + ":r]";
        String unitPtrMapName  = "[x" + this.rank() + ":p]";
        if (this.publicSymbolTable()!=null) {
            unitAllMapName = "[s" + this.rank() + ":a||d" + this.publicSymbolTable().rank() + ":a]";
            unitDiffMapName = "[s" + this.rank() + ":r||d" + this.publicSymbolTable().rank() + ":r]";
            unitPtrMapName = "[s" + this.rank() + ":p||d" + this.publicSymbolTable().rank() + ":p]";
        }
        // *********** EXTERNAL INFO ON UNIT's ENTRY : ***********
        TapEnv.println(" External data-dep info on entry of unit " + this + ":");
        // In-Out:
        if (dumpInOut && !isIntrinsic()) { // Intrinsics have their InOut info in "external" form:
            TapEnv.printlnMappedBoolVector("  === In-Out __ (global): ", unitInOutN, unitAllLen, unitAllMapName);
            TapEnv.printlnMappedBoolVector("  === In-Out R_ (global): ", unitInOutR, unitAllLen, unitAllMapName);
            TapEnv.printlnMappedBoolVector("  === In-Out _W (global): ", unitInOutW, unitAllLen, unitAllMapName);
            TapEnv.printlnMappedBoolVector("  === In-Out RW (global): ", unitInOutRW, unitAllLen, unitAllMapName);
        }
        if (dumpDeps) {
            // Plain Depencencies:
            TapEnv.print("  === Plain Dependencies matrix:"+unitAllMapName+"*"+unitAllMapName);
            if (plainDependencies != null) {
                TapEnv.println();
                TapEnv.dumpObject(plainDependencies);
            } else {
                TapEnv.println(" null");
            }
            // AD Depencencies:
            ADActivityAnalyzer adActivityAnalyzer = TapEnv.adActivityAnalyzer();
            TapEnv.print("  === Differentiable Dependencies matrix:" + unitDiffMapName + "*" + unitDiffMapName);
            if (adActivityAnalyzer != null && adActivityAnalyzer.getDiffDeps(this) != null) {
                TapEnv.println();
                TapEnv.dumpObject(adActivityAnalyzer.getDiffDeps(this));
            } else {
                TapEnv.println(" null");
            }
        }
        // Multithread info:
        if ((TapEnv.doOpenMP() || callGraph.hasCuda) && !TapEnv.modeIsNoDiff()) {
            BoolVector sharedZones = multithreadAnalyzer.getSharedZonesOfUnit(this) ;
            TapEnv.printlnMappedBoolVector("  === Multithread shared: ", sharedZones, unitAllLen, unitAllMapName);
        }
        // Activities and usefulnesses:
        TapList<ActivityPattern> inActivityPatterns = activityPatterns;
        if (inActivityPatterns == null) {
            // if this Unit is not active, or if there was no activity analysis done,
            // we still want to see the other info about in-out, pointers...
            ActivityPattern notActive = new ActivityPattern(this, false, false, TapEnv.diffKind());
            notActive.setCallActivity(null);
            notActive.setExitActivity(null);
            inActivityPatterns = new TapList<>(notActive, null);
        }
        int activityNumber = 0;
        ActivityPattern curActivity;
        while (inActivityPatterns != null) {
            ++activityNumber;
            curActivity = inActivityPatterns.head;
            TapEnv.println("  ### Activity Context number " + activityNumber + ": @" + Integer.toHexString(curActivity.hashCode()) + " ###");
            if (curActivity.callActivity() != null) {
                if (!TapEnv.doActivity()) {
                    TapEnv.println(" NO OPTIM activity!");
                }
                TapEnv.printlnMappedBoolVector("  === Activity    /entry: ", curActivity.callActivity(), unitDiffLen, unitDiffMapName);
                if (curActivity.callUsefulness() != null) {
                    TapEnv.printlnMappedBoolVector("  === Usefulness  /entry: ", curActivity.callUsefulness(), unitDiffLen, unitDiffMapName);
                }
                activities = curActivity.activities();
                usefulnesses = curActivity.usefulnesses();
                if (TapEnv.doActivity()) {
                    TapTriplet<Unit, BoolVector, BoolVector>[] varFunctionInfos = curActivity.varFunctionADActivities();
                    if (varFunctionInfos != null) {
                        String startline = "  === varFunction actvty: ";
                        for (int i = 0; i < varFunctionInfos.length; i++) {
                            if (varFunctionInfos[i] != null) {
                                TapEnv.print(startline + "Arg" + (i + 1) + ':');
                                startline = " ; ";
                                TapEnv.dumpObject(varFunctionInfos[i].second);
                                TapEnv.print("=>");
                                TapEnv.dumpObject(varFunctionInfos[i].third);
                            }
                        }
                        if (startline.equals(" ; ")) {
                            TapEnv.println();
                        }
                    }
                }
            } else {
                activities = null;
                usefulnesses = null;
            }
            reqXs = curActivity.reqXs();
            avlXs = curActivity.avlXs();
            livenesses = curActivity.diffLivenesses();
            livenessesCkp = curActivity.diffLivenessesCkp();
            overwrites = curActivity.diffOverwrites();
            overwritesCkp = curActivity.diffOverwritesCkp();
            tbrs = curActivity.tbrs();
            recomps = curActivity.recomps();
            if (!TapEnv.debugActivity()) {
                // ReqExplicit:
                TapEnv.printlnMappedBoolVector("  === ReqX        /entry: ", curActivity.entryReqX(), unitAllLen, unitAllMapName);
                TapEnv.printlnMappedBoolVector("  === AvlX        /entry: ", curActivity.entryAvlX(), unitAllLen, unitAllMapName);
                // Diff Liveness:
                TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Loc DiffLive/entry: ",
                        curActivity.entryDiffLivenessesCkp(), unitAllLen, unitAllMapName,
                        curActivity.entryDiffLivenessesCkpOnDiffPtr(), unitPtrLen, unitPtrMapName);
                TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Loc DiffOwrt/entry: ",
                        curActivity.entryDiffOverwritesCkp(), unitAllLen, unitAllMapName,
                        curActivity.entryDiffOverwritesCkpOnDiffPtr(), unitPtrLen, unitPtrMapName);
                // TBR:
                if( hasSource() ) {
                    TapEnv.printlnMappedBoolVector("  === TBR reqrd on entry: ", curActivity.adTBRTopDownContext(), unitAllLen, unitAllMapName);
                }
                else {
                    TapEnv.printOnDump("  === no TBR is reqrd on entry");
                }
            }
            if (hasSource() && dumpInside) {
                // *********** LOCAL INFO ON ENTRY BLOCK : ***********
                int blockAllLen;
                int blockDiffLen;
                int blockPointerRowLen;
                int blockPointerColLen;
                int blockDiffPtrLen;
                String blockAllMapName;
                String blockDiffMapName;
                String blockPointerRowMapName;
                String blockPointerColMapName;
                String blockDiffPtrMapName;
                TapEnv.println("  -----------------------------------------------------");
                TapEnv.print("  Control zones:");
                dumpControlZones(1);
                TapEnv.println(" Local data-dep info: Entry Block:     " + entryBlock.citeFlowShort());
                // In-Out:
                if (dumpInOut) {
                    if (!isIntrinsic()) { // Intrinsics have their InOut info in "external" form:
                        TapEnv.printlnMappedBoolVector("  === In-Out __ (global): ", unitInOutN, unitAllLen, unitAllMapName);
                        TapEnv.printlnMappedBoolVector("  === In-Out R_ (global): ", unitInOutR, unitAllLen, unitAllMapName);
                        TapEnv.printlnMappedBoolVector("  === In-Out _W (global): ", unitInOutW, unitAllLen, unitAllMapName);
                        TapEnv.printlnMappedBoolVector("  === In-Out RW (global): ", unitInOutRW, unitAllLen, unitAllMapName);
                    }
                    TapEnv.printlnMappedBoolVector("  === Constant till end : ", entryBlock.constantZones, unitAllLen, unitAllMapName);
                    TapEnv.printlnMappedBoolVector("  === Dead at this point: ", entryBlock.unusedZones, unitAllLen, unitAllMapName);
                }
                // Multithread info:
                if ((TapEnv.doOpenMP() || callGraph.hasCuda) && !TapEnv.modeIsNoDiff()) {
                    BoolVector sharedZones = multithreadAnalyzer.getSharedZonesOfBlock(entryBlock) ;
                    TapEnv.printlnMappedBoolVector("  === Multithread shared: ", sharedZones, unitAllLen, unitAllMapName);
                }
                //  Activities:
                if (activities != null) {
                    TapEnv.printlnMappedBoolVector("  === Activity          : ",
                            activities.retrieve(entryBlock).head, unitDiffLen, unitDiffMapName);
                }
                // Usefulnesses:
                if (usefulnesses != null) {
                    TapEnv.printlnMappedBoolVector("  === Usefulness        : ",
                            usefulnesses.retrieve(entryBlock).head, unitDiffLen, unitDiffMapName);
                }
                // ReqExplicit
                if (reqXs != null && !TapEnv.debugActivity()) {
                    TapList<BoolVector> reqXsList = reqXs.retrieve(entryBlock);
                    if (reqXsList != null) {
                        TapEnv.printlnMappedBoolVector("  === ReqX upon entry   : ",
                                reqXsList.head, unitAllLen, unitAllMapName);
                    }
                }
                if (avlXs != null && !TapEnv.debugActivity()) {
                    TapList<BoolVector> avlXsList = avlXs.retrieve(entryBlock);
                    if (avlXsList != null) {
                        TapEnv.printlnMappedBoolVector("  === AvlX upon entry   : ",
                                avlXsList.head, unitAllLen, unitAllMapName);
                    }
                }
                // Diff   Liveness:
                if (livenesses != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> livenessList = livenesses.retrieve(entryBlock);
                    if (livenessList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Liveness     : ",
                                livenessList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (livenessesCkp != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> livenessList = livenessesCkp.retrieve(entryBlock);
                    if (livenessList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Liveness Ckp : ",
                                livenessList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (overwrites != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> overwriteList = overwrites.retrieve(entryBlock);
                    if (overwriteList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Overwrite    : ",
                                overwriteList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (overwritesCkp != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> overwriteList = overwritesCkp.retrieve(entryBlock);
                    if (overwriteList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Overwrite Ckp: ",
                                overwriteList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                // TBR
                if (tbrs != null && !TapEnv.debugActivity()) {
                    TapEnv.printlnMappedBoolVectorWithDiffPtr("  === TBR upon entry    : ",
                            tbrs.retrieve(entryBlock).head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                }
                // *********** LOCAL INFO ON EACH BLOCK : ***********
                blocks = allBlocks();
                while (blocks != null) {
                    block = blocks.head;
                    symbolTable = block.symbolTable;
                    blockAllLen = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                    blockAllMapName = "[s" + this.rank() + ":a||d" + symbolTable.rank() + ":a]";
                    blockDiffLen = symbolTable.declaredZonesNb(TapEnv.diffKind());
                    blockDiffMapName = "[s" + this.rank() + ":r||d" + symbolTable.rank() + ":r]";
                    blockPointerRowLen = symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
                    blockPointerRowMapName = "[d" + symbolTable.rank() + ":p]";
                    blockPointerColLen = symbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
                    blockPointerColMapName = "[d" + symbolTable.rank() + ":a]";
                    blockDiffPtrLen = symbolTable.declaredZonesNb(SymbolTableConstants.PTRKIND);
                    blockDiffPtrMapName = "[s" + this.rank() + ":p||d" + symbolTable.rank() + ":p]";
                    TapEnv.println(" Local data-dep info: " + block + ":     " + block.citeFlowShort());
                    if (block instanceof HeaderBlock && !TapEnv.debugActivity()) {
                        TapEnv.println("  === Localized:"
                                + ((HeaderBlock) block).localizedZones
                                + " UniqueAccess:"
                                + ((HeaderBlock) block).uniqueAccessZones
                                + " TopUniqueAccess:"
                                + ((HeaderBlock) block).topUniqueAccessZones
                                + " LoopComplete:"
                                + ((HeaderBlock) block).loopCompleteZones);
                    }
                    instructions = new TapList<>(null, block.instructions);
                    // Pointers:
                    if (dumpPointers) {
                        TapEnv.print("  === Pointers destinations matrix: ");
                        if (block.pointerInfosIn != null) {
                            TapEnv.println(blockPointerRowMapName + "*" + blockPointerColMapName);
                            TapEnv.dumpBoolMatrix(block.pointerInfosIn, blockPointerRowMapName, blockPointerColMapName) ;
                            TapEnv.println();
                        } else {
                            TapEnv.println(" null");
                        }
                    }
                    // In-Out:
                    if (dumpInOut && !TapEnv.debugActivity()) {
                        TapEnv.printlnMappedBoolVector("  === Constant till end : ", block.constantZones, blockAllLen, blockAllMapName);
                    }
                    if (dumpInOut && !TapEnv.debugActivity()) {
                        TapEnv.printlnMappedBoolVector("  === Dead at this point: ", block.unusedZones, blockAllLen, blockAllMapName);
                    }
                    // Multithread info:
                    if ((TapEnv.doOpenMP() || callGraph.hasCuda) && !TapEnv.modeIsNoDiff()) {
                        BoolVector sharedZones = multithreadAnalyzer.getSharedZonesOfBlock(block) ;
                        TapEnv.printlnMappedBoolVector("  === Multithread shared: ", sharedZones, blockAllLen, blockAllMapName);
                    }
                    // Activities:
                    if (activities != null) {
                        instrsActivities = activities.retrieve(block);
                    } else {
                        instrsActivities = null;
                    }
                    // Usefulnesses:
                    if (usefulnesses != null) {
                        instrsUsefulnesses = usefulnesses.retrieve(block);
                    } else {
                        instrsUsefulnesses = null;
                    }
                    // ReqExplicit
                    if (reqXs != null) {
                        instrsReqXs = reqXs.retrieve(block);
                    } else {
                        instrsReqXs = null;
                    }
                    if (avlXs != null) {
                        instrsAvlXs = avlXs.retrieve(block);
                    } else {
                        instrsAvlXs = null;
                    }
                    // Diff   Liveness:
                    if (livenesses != null) {
                        instrsLivenesses = livenesses.retrieve(block);
                    } else {
                        instrsLivenesses = null;
                    }
                    if (livenessesCkp != null) {
                        instrsLivenessesCkp = livenessesCkp.retrieve(block);
                    } else {
                        instrsLivenessesCkp = null;
                    }
                    if (overwrites != null) {
                        instrsOverwrites = overwrites.retrieve(block);
                    } else {
                        instrsOverwrites = null;
                    }
                    if (overwritesCkp != null) {
                        instrsOverwritesCkp = overwritesCkp.retrieve(block);
                    } else {
                        instrsOverwritesCkp = null;
                    }
                    // TBR
                    if (tbrs != null) {
                        instrsTBRs = tbrs.retrieve(block);
                    } else {
                        instrsTBRs = null;
                    }
                    // Recomputations
                    if (recomps != null) {
                        instrsRecomps = recomps.retrieve(block);
                    } else {
                        instrsRecomps = null;
                    }
                    if (instrsRecomps != null && instrsRecomps.head != null && !TapEnv.debugActivity()) {
                        TapEnv.println("      === Adjoint bwd recomputations: " + instrsRecomps.head.first 
                                       +(instrsRecomps.head.second==null ? "" : " +Ptr: "+instrsRecomps.head.second)) ;
                        instrsRecomps = instrsRecomps.tail;
                    }
                    // *********** LOCAL INFO ON EACH INSTRUCTION : ***********
                    while (instructions != null) {
                        instruction = instructions.head;
                        if (instruction != null) {
                            TapEnv.print("      " + ILUtils.toString(instruction.tree, curActivity));
                            if (instrsActivities != null && !TapEnv.debugActivity()) {
                                boolean dead1 =
                                        !DiffLivenessAnalyzer.isTreeOrSubtreeRequiredInDiff(
                                                curActivity, instruction.tree, true);
                                boolean dead2 =
                                        !DiffLivenessAnalyzer.isTreeOrSubtreeRequiredInDiff(
                                                curActivity, instruction.tree, false);
                                if (dead1 || dead2) {
                                    TapEnv.print("    DIFF-DEAD" + (dead1 ? ", in adjoint-ckp" : "") + (dead2 ? ", in tangent or adjoint-split" : ""));
                                }
                            }
                            if (instruction.tree != null && instruction.tree.opCode() == ILLang.op_assign && !TapEnv.debugActivity()) {
                                boolean isTBR =
                                        ADTBRAnalyzer.isTBR(curActivity, instruction.tree.down(1));
                                if (isTBR) {
                                    TapEnv.print("    TBR!");
                                }
                            }
                            TapEnv.println();
                            if (dumpPointers) {
                                TapList<TapPair<TapIntList, BoolVector>> destsChange = instruction.pointerDestsChanges;
                                TapIntList ptrRks;
                                BoolVector ptrRow;
                                if (destsChange != null) {
                                    TapEnv.println("      === Pointers destinations update: " + blockPointerRowMapName);
                                    while (destsChange != null) {
                                        ptrRks = destsChange.head.first;
                                        ptrRow = destsChange.head.second;
                                        TapEnv.printlnMappedBoolVector("        " + TapEnv.str3(ptrRks.head) + "> ",
                                                                       ptrRow, blockPointerColLen, blockPointerColMapName);
                                        destsChange = destsChange.tail;
                                    }
                                }
                            }
                        }
                        instructions = instructions.tail;
                        // Activities:
                        if (instrsActivities != null) {
                            TapEnv.printlnMappedBoolVector("      === Activity          : ",
                                    instrsActivities.head, blockDiffLen, blockDiffMapName);
                            instrsActivities = instrsActivities.tail;
                        }
                        // Usefulnesses:
                        if (instrsUsefulnesses != null) {
                            TapEnv.printlnMappedBoolVector("      === Usefulness        : ",
                                    instrsUsefulnesses.head, blockDiffLen, blockDiffMapName);
                            instrsUsefulnesses = instrsUsefulnesses.tail;
                        }
                        // ReqExplicit
                        if (instrsReqXs != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVector("      === ReqX              : ",
                                    instrsReqXs.head, blockAllLen, blockAllMapName);
                            instrsReqXs = instrsReqXs.tail;
                        }
                        if (instrsAvlXs != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVector("      === AvlX              : ",
                                    instrsAvlXs.head, blockAllLen, blockAllMapName);
                            instrsAvlXs = instrsAvlXs.tail;
                        }
                        // Diff   Liveness:
                        if (instrsLivenesses != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVectorWithDiffPtr("      === Diff Liveness     : ",
                                    instrsLivenesses.head, blockAllLen, blockAllMapName, blockDiffPtrLen, blockDiffPtrMapName);
                            instrsLivenesses = instrsLivenesses.tail;
                        }
                        if (instrsLivenessesCkp != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVectorWithDiffPtr("      === Diff Liveness Ckp : ",
                                    instrsLivenessesCkp.head, blockAllLen, blockAllMapName, blockDiffPtrLen, blockDiffPtrMapName);
                            instrsLivenessesCkp = instrsLivenessesCkp.tail;
                        }
                        if (instrsOverwrites != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVectorWithDiffPtr("      === Diff Overwrite    : ",
                                    instrsOverwrites.head, blockAllLen, blockAllMapName, blockDiffPtrLen, blockDiffPtrMapName);
                            instrsOverwrites = instrsOverwrites.tail;
                        }
                        if (instrsOverwritesCkp != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVectorWithDiffPtr("      === Diff Overwrite Ckp: ",
                                    instrsOverwritesCkp.head, blockAllLen, blockAllMapName, blockDiffPtrLen, blockDiffPtrMapName);
                            instrsOverwritesCkp = instrsOverwritesCkp.tail;
                        }
                        // TBR
                        if (instrsTBRs != null && !TapEnv.debugActivity()) {
                            TapEnv.printlnMappedBoolVectorWithDiffPtr("      === TBR               : ",
                                    instrsTBRs.head, blockAllLen, blockAllMapName, blockDiffPtrLen, blockDiffPtrMapName);
                            instrsTBRs = instrsTBRs.tail;
                        }
                        if (instrsRecomps != null && instrsRecomps.head != null && !TapEnv.debugActivity()) {
                            TapEnv.println("      === Adjoint bwd recomputations: " + instrsRecomps.head.first
                                           +(instrsRecomps.head.second==null ? "" : " +Ptr: "+instrsRecomps.head.second));
                            instrsRecomps = instrsRecomps.tail;
                        }
                    }
                    blocks = blocks.tail;
                }
                // *********** LOCAL INFO ON EXIT BLOCK : ***********
                TapEnv.println(" Local data-dep info: Exit Block:     " + exitBlock.citeFlowShort());
                // Activities:
                if (activities != null) {
                    TapEnv.printlnMappedBoolVector("  === Activity          : ",
                            activities.retrieve(exitBlock).head, unitDiffLen, unitDiffMapName);
                }
                // Usefulnesses:
                if (usefulnesses != null) {
                    TapEnv.printlnMappedBoolVector("  === Usefulness        : ",
                            usefulnesses.retrieve(exitBlock).head, unitDiffLen, unitDiffMapName);
                }
                // ReqExplicit
                if (reqXs != null && !TapEnv.debugActivity()) {
                    TapList<BoolVector> reqXsList = reqXs.retrieve(exitBlock);
                    if (reqXsList != null) {
                        TapEnv.printlnMappedBoolVector("  === ReqX upon exit    : ",
                                reqXsList.head, unitAllLen, unitAllMapName);
                    }
                }
                if (avlXs != null && !TapEnv.debugActivity()) {
                    TapList<BoolVector> avlXsList = avlXs.retrieve(exitBlock);
                    if (avlXsList != null) {
                        TapEnv.printlnMappedBoolVector("  === AvlX upon exit    : ",
                                avlXsList.head, unitAllLen, unitAllMapName);
                    }
                }
                // Diff   Liveness:
                if (livenesses != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> livenessList = livenesses.retrieve(exitBlock);
                    if (livenessList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Liveness     : ",
                                livenessList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (livenessesCkp != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> livenessList = livenessesCkp.retrieve(exitBlock);
                    if (livenessList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Liveness Ckp : ",
                                livenessList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (overwrites != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> overwriteList = overwrites.retrieve(exitBlock);
                    if (overwriteList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Overwrite    : ",
                                overwriteList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                if (overwritesCkp != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> overwriteList = overwritesCkp.retrieve(exitBlock);
                    if (overwriteList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === Diff Overwrite Ckp: ",
                                overwriteList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
                // TBR
                if (tbrs != null && !TapEnv.debugActivity()) {
                    TapList<TapPair<BoolVector, BoolVector>> tbrList = tbrs.retrieve(exitBlock);
                    if (tbrList != null) {
                        TapEnv.printlnMappedBoolVectorWithDiffPtr("  === TBR upon exit     : ",
                                tbrList.head, unitAllLen, unitAllMapName, unitPtrLen, unitPtrMapName);
                    }
                }
            }
            TapEnv.println("  -----------------------------------------------------");
            // *********** EXTERNAL INFO ON UNIT's EXIT : ***********
            TapEnv.println(" External data-dep info on exit from unit " + this + ":");
            if (dumpPointers) {
                TapEnv.print("  === Pointers destinations: ");
                if (pointerEffect != null) {
                    TapEnv.println(unitPtrMapName + "*" + unitAllMapName);
                    pointerEffect.dump(new int[]{0, unitPtrLen}, new int[]{0, unitAllLen});
                    TapEnv.println();
                } else {
                    TapEnv.println(" null");
                }
            }
            // Activities:
            if (!TapEnv.doActivity()) {
                TapEnv.println(" NO OPTIM activity!");
            }
            if (curActivity.exitActivity() != null) {
                TapEnv.printlnMappedBoolVector("  === Activity     /exit: ",
                        curActivity.exitActivity(), unitDiffLen, unitDiffMapName);
            }
            if (curActivity.exitUsefulness() != null) {
                TapEnv.printlnMappedBoolVector("  === Usefulness   /exit: ",
                        curActivity.exitUsefulness(), unitDiffLen, unitDiffMapName);
            }
            if (!TapEnv.debugActivity()) {
                // ReqExplicit
                if (curActivity.exitReqX() != null) {
                    TapEnv.printlnMappedBoolVector("  === ReqX         /exit: ",
                            curActivity.exitReqX(), unitAllLen, unitAllMapName);
                }
                if (curActivity.exitAvlX() != null) {
                    TapEnv.printlnMappedBoolVector("  === AvlX         /exit: ",
                            curActivity.exitAvlX(), unitAllLen, unitAllMapName);
                }
            }
            TapEnv.println("  ### End of Activity Context number " + activityNumber + ": @" + Integer.toHexString(curActivity.hashCode()) + " ###");
            inActivityPatterns = inActivityPatterns.tail;
        }
        TapEnv.println();
    }

    /**
     * Turns a Unit which was created as a OUTSIDE_TBD or STANDARD, into an INTRINSIC.
     */
    public void turnIntrinsic() {
        if (kind != INTRINSIC) {
            callGraph.deleteUnit(this);
            setRank(-1);
            genericArrow = new CallArrow(null, this);
            kind = INTRINSIC;
            callGraph.addIntrinsicUnit(this);
        }
    }

    /**
     * Turns a Unit which was created as a OUTSIDE_TBD, into an EXTERNAL.
     */
    public void turnExternal() {
        if (kind != EXTERNAL) {
            kind = EXTERNAL;
        }
    }

    /**
     * Turns a Unit which was created as an INTERFACE, into a VARFUNCTION.
     */
    private void turnVarFunction(Unit callingUnit) {
        setExternalSymbolTable(callingUnit.externalSymbolTable());
        kind = VARFUNCTION;
    }

    /**
     * Try to absorb "absorbedUnit" into this Unit, then delete "absorbedUnit".
     */
    public void absorbAndDeleteUnit(Unit absorbedUnit) {
        if (language == -1) {
            language = absorbedUnit.language;
        }
        if (position == -1) {
            position = absorbedUnit.position;
        }
        if (kind == -1) {
            kind = absorbedUnit.kind;
        }
        if (callGraph == null) {
            callGraph = absorbedUnit.callGraph;
        }
        if (localMaps == null) localMaps = absorbedUnit.localMaps ;

        if (modifiers == null) {
            modifiers = absorbedUnit.modifiers;
        }
        if (functionTypeSpec == null) {
            functionTypeSpec = absorbedUnit.functionTypeSpec;
        }
        if (unitADDependencies == null) {
            unitADDependencies = absorbedUnit.unitADDependencies;
        }
        if (diffInfo == null) {
            diffInfo = absorbedUnit.diffInfo;
        }

        // Symbol Tables (at least the ones at the root):
        if (/*privateSymbolTable==null && */absorbedUnit.privateSymbolTable != null) {
            if (privateSymbolTable != null && privateSymbolTable != absorbedUnit.privateSymbolTable) {
                removeDerivedSymbolTable(privateSymbolTable);
            }
            privateSymbolTable = absorbedUnit.privateSymbolTable;
            addDerivedSymbolTable(privateSymbolTable);
        }
        if (/*protectedSymbolTable==null && */absorbedUnit.protectedSymbolTable != null) {
            if (protectedSymbolTable != null && protectedSymbolTable != absorbedUnit.protectedSymbolTable) {
                removeDerivedSymbolTable(protectedSymbolTable);
            }
            protectedSymbolTable = absorbedUnit.protectedSymbolTable;
            addDerivedSymbolTable(protectedSymbolTable);
        }
        if (absorbedUnit.publicSymbolTable != null) {
            if (publicSymbolTable != null && publicSymbolTable != absorbedUnit.publicSymbolTable) {
                removeDerivedSymbolTable(publicSymbolTable);
            }
            publicSymbolTable = absorbedUnit.publicSymbolTable;
            addDerivedSymbolTable(publicSymbolTable);
        }

        // Blocks:
//         if (entryBlock==null) {
        entryBlock = absorbedUnit.entryBlock;
        entryBlock.symbolTable = publicSymbolTable;
//         }
//         if (exitBlock==null) {
        exitBlock = absorbedUnit.exitBlock;
        exitBlock.symbolTable = publicSymbolTable;
//         }
        if (allBlocks == null) {
            allBlocks = absorbedUnit.allBlocks;
        }
        if (topBlocks == null) {
            topBlocks = absorbedUnit.topBlocks;
        }

        // Deal with the container Unit:
        if (upperLevelUnit != absorbedUnit.upperLevelUnit) {
            TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(Unit.absorb) incompatible container units for " + this + " (in " + upperLevelUnit + ") absorbing " + absorbedUnit + " (in" + absorbedUnit.upperLevelUnit + ')');
        } else if (upperLevelUnit != null) {
            CallGraph.getCallArrow(upperLevelUnit, absorbedUnit).delete();
        }

        // Deal with lowerLevelUnits:
        TapList<Unit> absorbedLowerUnits = absorbedUnit.lowerLevelUnits;
        absorbedUnit.lowerLevelUnits = null;
        TapList<Unit> toCurrentLowerUnits = new TapList<>(null, this.lowerLevelUnits);
        TapList<Unit> toTlCurrentLowerUnits = toCurrentLowerUnits;
        while (toTlCurrentLowerUnits.tail != null) {
            toTlCurrentLowerUnits = toTlCurrentLowerUnits.tail;
        }
        while (absorbedLowerUnits != null) {
            absorbedLowerUnits.head.upperLevelUnit = this;
            toTlCurrentLowerUnits = toTlCurrentLowerUnits.placdl(absorbedLowerUnits.head);
            absorbedLowerUnits = absorbedLowerUnits.tail;
        }
        this.lowerLevelUnits = toCurrentLowerUnits.tail;

        this.absorb(absorbedUnit) ;
//         TapList<CallArrow> callArrows;
//         CallArrow callArrow;
//         callArrows = absorbedUnit.callees;
//         while (callArrows != null) {
//             callArrow = callArrows.head;
//             callArrow.redirectOrigin(this);
//             callArrows = callArrows.tail;
//         }
//         callArrows = absorbedUnit.callers;
//         while (callArrows != null) {
//             callArrow = callArrows.head;
//             callArrow.redirectDestination(this);
//             callArrows = callArrows.tail;
//         }

//         if (absorbedUnit.importedModulesUseDecl != null) {
//             if (this.importedModulesUseDecl != null) {
//                 if (!TapList.equalsImportedModules(this.importedModulesUseDecl, absorbedUnit.importedModulesUseDecl)) {
//                     String unitMsg = null;
//                     if (absorbedUnit.isInterface()) {
//                         unitMsg = absorbedUnit.name;
//                     } else if (this.isInterface()) {
//                         unitMsg = this.name;
//                     }
//                     if (unitMsg == null) {
//                         TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(Unit.absorb) Not implemented " + this
//                                 + " imports modules: " + importedModulesUseDecl + " and " + absorbedUnit
//                                 + " imports modules: " + absorbedUnit.importedModulesUseDecl);
//                     } else {
//                         TapEnv.fileWarning(TapEnv.MSG_WARN, position, "(DD29) interface and procedure "
//                                 + unitMsg + " use statements differ");
//                     }
//                 }
//             } else {
//                 this.importedModulesUseDecl = absorbedUnit.importedModulesUseDecl;
//             }
//         }
        absorbedUnit.callGraph.deleteUnit(absorbedUnit);
    }

    /**
     * Combines the callers and callees of "absorbedUnit" into this Unit.
     */
    protected void absorb(Unit absorbedUnit) {
        // Deal with callers and callees:
        TapList<CallArrow> callArrows;
        CallArrow callArrow;
        callArrows = absorbedUnit.callees;
        while (callArrows != null) {
            callArrow = callArrows.head;
            callArrow.redirectOrigin(this);
            callArrows = callArrows.tail;
        }
        callArrows = absorbedUnit.callers;
        while (callArrows != null) {
            callArrow = callArrows.head;
            callArrow.redirectDestination(this);
            callArrows = callArrows.tail;
        }
        // Deal with imports. TODO Not really well done!
        if (absorbedUnit.importedModulesUseDecl != null) {
            if (this.importedModulesUseDecl != null) {
                if (!TapList.equalsImportedModules(this.importedModulesUseDecl, absorbedUnit.importedModulesUseDecl)) {
                    String unitMsg = null;
                    if (absorbedUnit.isInterface()) {
                        unitMsg = absorbedUnit.name;
                    } else if (this.isInterface()) {
                        unitMsg = this.name;
                    }
                    if (unitMsg == null) {
                        TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(Unit.absorb) Not implemented "
                                + this + " imports modules: " + importedModulesUseDecl + " and " + absorbedUnit
                                + " imports modules: " + absorbedUnit.importedModulesUseDecl);
                    } else {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, position, "(DD29) interface and procedure "
                                + unitMsg + " use statements differ");
                    }
                }
            } else {
                this.importedModulesUseDecl = absorbedUnit.importedModulesUseDecl;
            }
        }
    }

    protected void collectImportForLater(Instruction useInstr, SymbolTable symbolTable) {
        TapEnv.warningFortran77WithFortran90Features(this);
        String moduleName = ILUtils.getIdentString(useInstr.tree.down(1));
        Unit usedModule = symbolTable.getModule(moduleName);
        if (usedModule == null) {
            usedModule = callGraph.createNewUnit(null, language);
            usedModule.setExternalSymbolTable(callGraph.languageRootSymbolTable(language));
            usedModule.kind = UNDEFINED;
            usedModule.name = moduleName;
            FunctionDecl globalModDecl = new FunctionDecl(useInstr.tree.down(1), usedModule);
            callGraph.languageRootSymbolTable(language).addSymbolDecl(globalModDecl);
        }
        importedModulesUseDecl = TapList.append(importedModulesUseDecl, new TapList<>(useInstr, null));
        CallGraph.addCallArrow(this, SymbolTableConstants.IMPORTS, usedModule);
    }

    /**
     * @return the original standard Unit of which this Unit is an interface.
     */
    public Unit getOrigUnitOfInterface(CallGraph callGraph) {
        Unit origUnitOfInterface = null;
        if (kind == INTERFACE) {
            Unit oUnit = callGraph.getUnit(name, functionTypeSpec());
            if (oUnit != null && oUnit != this && oUnit.kind == STANDARD) {
                origUnitOfInterface = oUnit;
            }
        }
        return origUnitOfInterface;
    }

    /**
     * @return the STANDARD Unit (when it exists) corresponding to this INTERFACE Unit.
     * This code is not very satisfying, because the getFunctionDecl should be
     * made more specific by using the INTERFACE's functionTypeSpec().
     */
    public Unit getUnitOfInterface() {
        TapList<FunctionDecl> standardDecls = externalSymbolTable().getFunctionDecl(name, functionTypeSpec().returnType, functionTypeSpec().argumentsTypes, true);
        // we assume there's only 1 function that matches with this signature.
        FunctionDecl standardDecl = standardDecls == null ? null : standardDecls.head;
        if (standardDecl == null) {
            standardDecls = publicSymbolTable.getTopFunctionDecl(name, null, null, false);
            standardDecl = standardDecls == null ? null : standardDecls.head;
            if (standardDecl == null) {
                standardDecls = privateSymbolTable.getTopFunctionDecl(name, null, null, false);
                standardDecl = standardDecls == null ? null : standardDecls.head;
            }
        }
        return standardDecl == null ? null : standardDecl.unit();
    }

    /**
     * @return the closest upper level class unit of this unit.
     **/
    public Unit getUpperLevelClassUnit() {
        Unit result = this;
        while (result != null && !result.isClass()) {
            result = result.upperLevelUnit;
        }
        return result;
    }

    /**
     * @return the argument that deals with type size
     * in the given array of arguments of a called intrinsic.
     */
    protected Tree getElementalIntrinsicTypeConverter(Tree[] args) {
        Tree result = null;
        if (isFortran()) {
            if (args.length == 2
                    && (name.equals("aint")
                    || name.equals("anint")
                    || name.equals("ceiling")
                    || name.equals("floor")
                    || name.equals("int")
                    || name.equals("nint")
                    || name.equals("real"))) {
                result = args[1];
            } else if (name.equals("cmplx")) {
                if (args.length == 3) {
                    result = args[2];
                } else if (args.length == 2 && args[1].opCode() == ILLang.op_nameEq) {
                    result = args[1];
                }
            }
            if (result != null && result.opCode() == ILLang.op_nameEq) {
                result = result.down(2);
            }
        }
        return result;
    }

    /**
     * @return the position of this Unit in its textual file.
     */
    public int getPosition() {
        if (position == -1) {
            if (entryBlock != null) {
                position = entryBlock.getPosition();
            }
            if (position == -1) {
                TapList<Block> blocks = allBlocks;
                while (position == -1 && blocks != null) {
                    position = blocks.head.getPosition();
                    blocks = blocks.tail;
                }
            }
            if (position == -1 && publicSymbolTable != null && publicSymbolTable.declarationsBlock != null) {
                position = publicSymbolTable.declarationsBlock.getPosition();
            }
            if (position == -1 && privateSymbolTable != null && privateSymbolTable.declarationsBlock != null) {
                position = privateSymbolTable.declarationsBlock.getPosition();
            }
            if (position == -1 && translationUnitSymbolTable != null && translationUnitSymbolTable.declarationsBlock != null) {
                position = translationUnitSymbolTable.declarationsBlock.getPosition();
            }
        }
        return position;
    }

    /**
     * @return the list of Units for which there exists a path in the
     * CallGraph from that Unit to 'this' Unit. @return an ordered TapList
     * of Unit's, so that each caller appears after all its callees.
     * The given Unit is put in first position in the result.
     */
    public TapList<Unit> allCallers() {
        return allCallersRec(this, null, new TapList<>(null, null));
    }

    /**
     * @return the TapList of all computation Unit's recursively called
     * by "this" Unit. The returned list is ordered, so that each
     * caller appears before its callees. In case of recursivity
     * loops, no Unit appears twice. The first Unit in the list is
     * "this" unit. The result doesn't contain non-computation Units
     * such as modules, varFunctions, interfaces...
     */
    public TapList<Unit> allCallees() {
        return allCalleesRec(this, null, new TapList<>(null, null));
    }

    /**
     * Sets the Flow Graph contents of this Unit.
     * Takes a raw description of a Flow Graph, i.e. entryBlock, allBlocks, and exitBlock,
     * condenses, simplifies, and normalizes it, and then puts the result
     * as the new Flow Graph contents of this Unit.
     * Assumes that all given Blocks are linked by the correct FGArrow's.
     * Normalization includes Block fusion when possible, loop nesting choice,
     * detection of unreachable Blocks, and coherence enforcement.
     * When this function completes, the Unit has its "allBlocks" and "topBlocks"
     * computed, plus all the various fields related to the loop nesting in
     * contained BasicBlock's, LoopBlock's, HeaderBlock's, FGArrow's, etc.
     *
     * @param entryBlock     The EntryBlock of the given raw Flow Graph
     * @param givenAllBlocks All the inside blocks of the given raw Flow Graph,
     *                       unordered, EntryBlock and ExitBlock excluded
     * @param exitBlock      The ExitBlock of the given raw Flow Graph
     * @param finalGraph     true when this graph is not going to be analyzed any more,
     *                       and is rather the final result of AD. In this case condensation will be
     *                       a little more aggressive.
     */
    public void makeFlowGraph(EntryBlock entryBlock,
                              TapList<Block> givenAllBlocks, ExitBlock exitBlock,
                              boolean finalGraph) {
        TapList<Block> inAllBlocks;
        TapList<Block> deadBlocks;
        SymbolTable curSymbolTable;
        SymbolTable newSymbolTable;
        Block curBlock;
        boolean hasUselessLevels;
        // First store the elements of the raw flow graph ...
        this.entryBlock = entryBlock;
        this.exitBlock = exitBlock;
        if (isClass() || isModule() /* || isTranslationUnit()*/) {
            // Don't play normalization: there's no real control flow in these Units.
            allBlocks = givenAllBlocks;
            topBlocks = givenAllBlocks;
        } else {
            // ... then start normalization of the new FlowGraph.
            // (condensation, loop nesting, unreachable blocks)
            FlowGraphNormalizer flowGraphNormalizer =
                    new FlowGraphNormalizer(entryBlock, givenAllBlocks, exitBlock);
            flowGraphNormalizer.condenseFG(finalGraph);
            // Constructors have a special Block 0 for initializations :
            if (!isConstructor()) {
                flowGraphNormalizer.makeSurePrivateDeclarationsBlock(this);
            }
            flowGraphNormalizer.sortFlowArrows();
            topBlocks = flowGraphNormalizer.findOptimalLoops();
            deadBlocks = flowGraphNormalizer.getDeadBlocks();
            while (deadBlocks != null) {
                declareDeadBlock(deadBlocks.head);
                deadBlocks = deadBlocks.tail;
            }
        }

        // then fill all fields that can be deduced from the
        // "topBlocks" and "inside" fields. Therefore, all
        // information stored in the FlowGraph will become coherent.
        coherence();
        // then remove useless levels in the tree of SymbolTable's:
        // A SymbolTable is "useless" if it defines
        // nothing new wrt its basisSymbolTable.
        inAllBlocks = this.allBlocks;
        SymbolTable referenceSymbolTable = bodySymbolTable();
        if (!finalGraph) {
          while (inAllBlocks != null) {
            curBlock = inAllBlocks.head;
            curSymbolTable = curBlock.symbolTable;
            if (curSymbolTable != null && curSymbolTable != referenceSymbolTable) {
                hasUselessLevels = false;
                // if the current SymbolTable is based on a useless SymbolTable,
                //  re-basis it to the next SymbolTable that is not useless.
                newSymbolTable = curSymbolTable.basisSymbolTable();
                // For XAIF, may happen that curSymbolTable is the root SymbolTable:
                if (newSymbolTable != null) {
                    while (newSymbolTable != referenceSymbolTable && newSymbolTable.isUseless()) {
                        hasUselessLevels = true;
                        removeDerivedSymbolTable(newSymbolTable);
                        newSymbolTable = newSymbolTable.basisSymbolTable();
                    }
                    if (hasUselessLevels) {
                        curSymbolTable.setBasisSymbolTable(newSymbolTable);
                    }
                    // if the current SymbolTable is itself useless,
                    // redirect the current Block's SymbolTable to
                    // the next SymbolTable that is not useless.
                    if (curSymbolTable.isUseless()) {
                        removeDerivedSymbolTable(curSymbolTable);
                        curBlock.symbolTable = newSymbolTable;
                    }
                }
            }
            inAllBlocks = inAllBlocks.tail;
          }
        }
        if (privateSymbolTable != null && privateSymbolTable.declarationsBlock == null && this.allBlocks != null) {
            Block blockForExtraDecls = this.allBlocks.head;
            // Very special case (for Fortran): if the first of allBlocks (which in general holds the declarations)
            // has a "$AD LABEL" inside its declarations part, then the declarations part may extend
            // after this first block, till a Block that contains the corresponding "$AD END-LABEL".
            // In that case it is this second Block that should receive the possible extra declarations.
            String labelInDeclarations = blockForExtraDecls.hasDeclarationCommentStartingWith("$AD LABEL ") ;
            if (labelInDeclarations!=null) {
                blockForExtraDecls = null ;
                String searchedLabel = "$AD END-LABEL "+labelInDeclarations ;
                for (TapList<Block> blocks = this.allBlocks ;
                     blocks!=null && blockForExtraDecls==null ;
                     blocks = blocks.tail) {
                    Block block = blocks.head;
                    if (block.hasCommentWith(searchedLabel)) {
                        blockForExtraDecls = block ;
                    }
                }
            }
            privateSymbolTable.declarationsBlock = blockForExtraDecls ;
        }
        if (publicSymbolTable != null && publicSymbolTable.declarationsBlock == null) {
            publicSymbolTable.declarationsBlock = new BasicBlock(publicSymbolTable, null, null);
            publicSymbolTable.declarationsBlock.rank = -37;
        }
    }

    /**
     * For each variable declared, but incompletely
     * (e.g. there was a dimension T(100), but nothing else on T),
     * uses the implicit type mechanism to terminate the
     * typing of the variable in the SymbolTable.
     */
    public void fixImplicits() {
        TapList<SymbolTable> inSymbolTables = symbolTablesBottomUp;
        while (inSymbolTables != null) {
            inSymbolTables.head.fixImplicits();
            inSymbolTables = inSymbolTables.tail;
        }
    }

    /**
     * Refine the FunctionTypeSpec of this Unit, using the types
     * found in the function's definition, but removing from these types
     * the "Hint" information, and any other bit of info that refers
     * to the inside of this Unit and should not be used outside.
     */
    public void exportFunctionTypeSpec() {
        if (!isPackage()) {
            TypeSpec[] argumentsTypes = functionTypeSpec.argumentsTypes;
            Tree callTree;
            Tree[] callArgsTrees;
            if (entryBlock != null) {
                callTree = entryBlock.headTree();
                assert callTree != null;
                callArgsTrees = ILUtils.getArguments(callTree).children();
            } else {
                callTree = parametersOrModuleNameTree;
                callArgsTrees = parametersOrModuleNameTree.children();
            }
            SymbolTable argsSymbolTable = publicSymbolTable();
            for (int i = callArgsTrees.length - 1; i >= 0; --i) {
                if (callArgsTrees[i].opCode() != ILLang.op_star) {
                    String varName;
                    if (callArgsTrees[i].opCode() == ILLang.op_ident
                            || callArgsTrees[i].opCode() == ILLang.op_metavar) {
                        varName = ILUtils.getIdentString(callArgsTrees[i]);
                    } else {
                        varName = ILUtils.baseName(callArgsTrees[i]);
                    }
                    SymbolDecl symbolDecl = argsSymbolTable.getSymbolDecl(varName);
                    if (symbolDecl != null && argumentsTypes != null && i < argumentsTypes.length) {
                        if (symbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                            WrapperTypeSpec declaredType = symbolDecl.type();
                            if (!TypeSpec.canCompare(declaredType, (WrapperTypeSpec) argumentsTypes[i]
                            )) {
                                TapEnv.fileWarning(TapEnv.MSG_WARN, callArgsTrees[i], "(TC31) Type mismatch in argument "
                                        + (i + 1) + " of function " + name + ", was " + declaredType.showType() + ", is here "
                                        + argumentsTypes[i].showType());
                            }
                            argumentsTypes[i] = declaredType;
                        } else if (symbolDecl.isA(SymbolTableConstants.FUNCTION)) {
                            argumentsTypes[i] =
                                    new WrapperTypeSpec(
                                            ((FunctionDecl) symbolDecl).unit().functionTypeSpec());
                        }
                        argumentsTypes[i] = argumentsTypes[i].copyStopOnComposite(this); //erase hints
                    }
                }
            }
            // seulement pour fortran ou on peut declarer une fonction en plusieurs morceaux
            if (isFortran()) {
                WrapperTypeSpec returnTypeSpec = functionTypeSpec().returnType;
                SymbolDecl symbolDecl = argsSymbolTable.getSymbolDecl(this.name);
                if (symbolDecl != null) {
                    if (symbolDecl.isA(SymbolTableConstants.VARIABLE)) {
                        WrapperTypeSpec declaredType = symbolDecl.type();
                        if (!TypeSpec.canCompare(declaredType, returnTypeSpec)) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, callTree, "(TC31) Type mismatch in result of function "
                                    + name + ", was " + declaredType.showType() + ", is here " + returnTypeSpec.showType());
                        }
                        functionTypeSpec.returnType = declaredType;
                    }
                }
            }
            functionTypeSpec.returnType = (WrapperTypeSpec) functionTypeSpec.returnType.copyStopOnComposite(this); //erase hints
        }
    }

    /**
     * Same as exportFunctionTypeSpec(), but only for the argument named "argName", with the given "typeSpec".
     */
    protected void exportOneArgumentType(String argName, WrapperTypeSpec typeSpec) {
        if (functionTypeSpec == null) {
            // TODO cas des pointeurs sur function passe's en parametres
            // cf v103
        } else if (!isModule()) {
            WrapperTypeSpec[] argumentsTypes = functionTypeSpec.argumentsTypes;
            assert entryBlock.headTree() != null;
            Tree[] callArgsTrees = ILUtils.getArguments(entryBlock.headTree()).children();
            for (int i = callArgsTrees.length - 1; i >= 0; i--) {
                if (callArgsTrees[i].opCode() == ILLang.op_ident
                        && ILUtils.getIdentString(callArgsTrees[i]).equals(argName)) {
                    argumentsTypes[i] = typeSpec;
                }
            }
        }
    }

    /**
     * Generates the ordered list of Tree's for the parameters of this Unit.
     *
     * @param locationSymbolTable the SymbolTable of the future location of the generated parameter list.
     */
    public TapList<Tree> generateHeaderParamsList(SymbolTable locationSymbolTable) {
        Tree[] callParams;
        if (entryBlock != null && entryBlock.headTree() != null) {
            callParams = ILUtils.getArguments(entryBlock.headTree()).children();
        } else {
            callParams = parametersOrModuleNameTree.children();
        }
        TapList<Tree> paramsTypedTreeList = null;
        if (callParams.length != 0) {
            Tree generatedParam;
            String paramName;
            TapList<Tree> topParams = new TapList<>(null, null);
            TapList<Tree> tailParams = topParams;
            if (this.sameLanguage(TapEnv.relatedLanguage())) {
                if (this.isC()) {
// System.out.println("  ?? GENERATE FROM:");
// for (int i=0 ; i<callParams.length ; ++i) System.out.print("  "+callParams[i]) ;
// System.out.println("   WITH LOCATIONSYMBOLTABLE:"+locationSymbolTable+" NOTES: functionTypeSpec: "+functionTypeSpec+" publicSymbolTable: "+publicSymbolTable) ;
                    paramsTypedTreeList = getCTypedParamsList(callParams, locationSymbolTable);
// System.out.println("  ================> "+paramsTypedTreeList);
                } else {
                    for (Tree callParam : callParams) {
                        generatedParam = callParam.copy();
                        paramName = ILUtils.baseName(generatedParam);
                        if (paramName != null // we don't need all functions that matches
                                && (publicSymbolTable.getTopFunctionDecl(paramName, null, null, false) != null
                                || publicSymbolTable.getTopInterfaceDecl(paramName) != null)) {
                            generatedParam.setAnnotation("isFunctionName", Boolean.TRUE);
                        }
                        tailParams = tailParams.placdl(generatedParam);
                    }
                    paramsTypedTreeList = new TapList<>(
                            ILUtils.build(ILLang.op_varDeclaration,
                                    ILUtils.build(ILLang.op_modifiers),
                                    ILUtils.build(ILLang.op_none),
                                    ILUtils.build(ILLang.op_declarators, topParams.tail)),
                            null);
                }
            } else if (this.isFortran()) {
                // on genere un prototype C d'une procedure Fortran, il faut rajouter les pointerTypeSpec autour des types
                // et enlever les modifiers c_float, c_*
                Tree generatedParamType;
                for (int i = 0; i < callParams.length; ++i) {
                    if (!takesArgumentByValue(i + 1, TapEnv.languages(0))
                            && !(TapEnv.get().multiDirDiffMode && i == callParams.length - 1)) {
                        WrapperTypeSpec baseTypeSpec = functionTypeSpec().argumentsTypes[i];
                        if (TypeSpec.isA(baseTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                            if (((ModifiedTypeSpec) baseTypeSpec.wrappedType).hasCbindingModifier()) {
                                if (((ModifiedTypeSpec) baseTypeSpec.wrappedType).hasCbindingModifier("c_double")) {
                                    TypeSpec elementTypeSpec = baseTypeSpec.wrappedType.elementType();
                                    baseTypeSpec = new WrapperTypeSpec(new ModifiedTypeSpec(new WrapperTypeSpec(elementTypeSpec),
                                            ILUtils.build(ILLang.op_ident, "double"),
                                            null));
                                } else {
                                    baseTypeSpec = baseTypeSpec.wrappedType.elementType();
                                }
                            }
                        }
                        if (TypeSpec.isA(baseTypeSpec, SymbolTableConstants.PRIMITIVETYPE)
                                || TypeSpec.isA(baseTypeSpec, SymbolTableConstants.MODIFIEDTYPE)) {
                            generatedParamType =
                                    baseTypeSpec.generateTree(publicSymbolTable, null, null, true, null);
                            generatedParamType = ILUtils.build(ILLang.op_pointerType, generatedParamType);
                        } else if (baseTypeSpec.baseTypeSpec(true).isCharacter()) {
                            generatedParamType = ILUtils.build(ILLang.op_pointerType,
                                    ILUtils.build(ILLang.op_ident, "char"));
                        } else if (TypeSpec.isA(baseTypeSpec, SymbolTableConstants.ARRAYTYPE)) {
                            generatedParamType =
                                    baseTypeSpec.generateTree(publicSymbolTable, null, null, true, null);
                            ArrayTypeSpec arrayTypeSpec = (ArrayTypeSpec) baseTypeSpec.wrappedType;
                            ArrayDim[] dimensions = arrayTypeSpec.dimensions();
                            if (dimensions.length == 1) {
                                generatedParamType.down(2).down(1).setChild(ILUtils.build(ILLang.op_none), 2);
                            } else {
                                // si 2 dimensions == -1, alors pointer et pas array:
                                int nbUndefinedSize = 0;
                                for (int j = dimensions.length - 1; j >= 0; --j) {
                                    if (dimensions[j].size() == -1 || nbUndefinedSize != 0) {
                                        generatedParamType.down(2).down(j + 1).setChild(ILUtils.build(ILLang.op_none), 2);
                                        nbUndefinedSize = nbUndefinedSize + 1;
                                    }
                                }
                                if (nbUndefinedSize > 1) {
                                    baseTypeSpec = baseTypeSpec.modifiedBaseTypeSpec();
                                    generatedParamType =
                                            baseTypeSpec.generateTree(publicSymbolTable, null, null, true, null);
                                    generatedParamType = ILUtils.build(ILLang.op_pointerType, generatedParamType);
                                }
                            }
                        } else if (TypeSpec.isA(baseTypeSpec, SymbolTableConstants.COMPOSITETYPE)) {
                            String typeDeclName = baseTypeSpec.wrappedType.typeDeclName();
                            TypeDecl typeDecl = TapEnv.relatedUnit().translationUnitSymbolTable.
                                    getTypeDecl(typeDeclName);
                            if (typeDecl == null) {
                                typeDecl = TapEnv.relatedUnit().translationUnitSymbolTable.
                                        getTypeDecl("struct " + typeDeclName);
                            }
                            if (typeDecl != null) {
                                typeDeclName = typeDecl.symbol;
                                generatedParamType = ILUtils.build(ILLang.op_ident, typeDeclName);
                                generatedParamType = ILUtils.build(ILLang.op_pointerType, generatedParamType);
                            } else {
                                typeDeclName = "UnknownTypeName";
                                TypeDecl otherTypeDecl = callGraph.getOtherBindType(baseTypeSpec);
                                if (otherTypeDecl != null) {
                                    generatedParamType = otherTypeDecl.typeSpec.wrappedType.generateTree(
                                            publicSymbolTable, null, null, true, null);
                                } else {
                                    generatedParamType = ILUtils.build(ILLang.op_ident, typeDeclName);
                                    generatedParamType = ILUtils.build(ILLang.op_pointerType, generatedParamType);
                                }
                            }
                        } else {
                            generatedParamType =
                                    baseTypeSpec.generateTree(publicSymbolTable, null, null, true, null);
                        }
                    } else {
                        generatedParamType =
                                functionTypeSpec.argumentsTypes[i].generateTree(publicSymbolTable, null, null, true, null);
                    }
                    generatedParam = ILUtils.build(ILLang.op_varDeclaration,
                            ILUtils.build(ILLang.op_modifiers),
                            generatedParamType,
                            ILUtils.build(ILLang.op_declarators));
                    tailParams = tailParams.placdl(generatedParam);
                }
                paramsTypedTreeList = topParams.tail;
            } else {
                for (Tree callParam : callParams) {
                    generatedParam = callParam.copy();
                    paramName = ILUtils.baseName(generatedParam);
                    if (paramName != null // we don't need all functions that matches
                            && (publicSymbolTable.getTopFunctionDecl(paramName, null, null, false) != null
                            || publicSymbolTable.getTopInterfaceDecl(paramName) != null)) {
                        generatedParam.setAnnotation("isFunctionName", Boolean.TRUE);
                    }
                    tailParams = tailParams.placdl(generatedParam);
                }
                paramsTypedTreeList = new TapList<>(
                        ILUtils.build(ILLang.op_varDeclaration,
                                ILUtils.build(ILLang.op_modifiers),
                                ILUtils.build(ILLang.op_none),
                                ILUtils.build(ILLang.op_declarators, topParams.tail)),
                        null);
            }
        }

        if (functionTypeSpec() != null && functionTypeSpec().variableArgList) {
            paramsTypedTreeList =
                    TapList.addLast(paramsTypedTreeList,
                            ILUtils.build(ILLang.op_variableArgList));
        }
        return paramsTypedTreeList;
    }

    private TapList<Tree> getCTypedParamsList(Tree[] callParams, SymbolTable locationSymbolTable) {
        Tree generatedParam;
        String paramName;
        VariableDecl paramDecl;
        FunctionDecl functionParamDecl;
        TapList<DeclStruct> declStructs;
        Tree declTree;
        TapList<Tree> paramDecls = null;
        TapList<SymbolDecl> externalTypes = locationSymbolTable.getAllTypeDecls();
        for (int i = callParams.length - 1; i >= 0; --i) {
            generatedParam = callParams[i].copy();
            paramName = ILUtils.baseName(generatedParam);
            if (generatedParam.opCode() == ILLang.op_star) {
                declTree = ILUtils.copy(generatedParam);
                paramDecls = new TapList<>(declTree, paramDecls);
            } else if (functionTypeSpec != null
                    && functionTypeSpec.argumentsTypes != null
                    && "_".equals(paramName)
                    && functionTypeSpec.argumentsTypes[i] != null) {
                declTree = functionTypeSpec.argumentsTypes[i].generateTree(locationSymbolTable, null, externalTypes, true, null);
                paramDecls = new TapList<>(declTree, paramDecls);
            } else {
                Tree paramNameTree = null;
                paramDecl = locationSymbolTable.getTopVariableDecl(paramName);
                if (paramDecl == null) {
                    NewSymbolHolder newSH = NewSymbolHolder.getNewSymbolHolder(ILUtils.baseTree(generatedParam));
                    if (newSH != null) {
                        paramDecl = newSH.newVariableDecl();
                        paramNameTree = newSH.makeNewRef(locationSymbolTable);
                    }
                }
                if (paramNameTree == null) {
                    paramNameTree = ILUtils.build(ILLang.op_ident, paramName);
                }
                if (paramDecl == null) {
                    TapList<FunctionDecl> functionsParamDecl = locationSymbolTable.getTopFunctionDecl(paramName, null, null, false);
                    // We have only one function that matches (C lang)
                    functionParamDecl = functionsParamDecl == null ? null : functionsParamDecl.head;
                    if (functionParamDecl != null
                            && functionParamDecl.unit() != null) {
                        paramDecl = new VariableDecl(paramNameTree,
                                new WrapperTypeSpec(
                                        new PointerTypeSpec(
                                                new WrapperTypeSpec(functionParamDecl.unit().functionTypeSpec()),
                                                null))); //TODO a verifier si offsetLength!=null
                    }
                }
                //[llh 6Jan2021] TODO: we should not use here primitives from ir2tree (TreeGen, VariableDeclStruct...)
                // it is heavy, neither necessary nor logic. Better write specific code here.
                declStructs = TreeGen.addSymbolDeclStructs(paramDecl, null,
                        locationSymbolTable, null, paramNameTree);
                if (declStructs != null) {
                    declTree =
                            ((VariableDeclStruct) declStructs.head).generateTree(externalTypes, privateSymbolTable());
                    paramDecls = new TapList<>(declTree, paramDecls);
                    // cette instruction est perdue pour un parametre formel
                    // decompile' deux fois s'il y a une declaration forward
                    assert paramDecl != null;
                    paramDecl.setInstruction(new Instruction(declTree));
                }
            }
        }
        return paramDecls;
    }

    /**
     * Builds the tree of the return type of this Unit.
     */
    public Tree buildReturnTypeTree(Block privateDeclBlock,
                                    TapList<SymbolDecl> externalTypes, ToObject<Tree> toNewDeclTree,
                                    TapList<Instruction> publicDeclarationsInstructions) {
        Tree returnTypeTree;
        if (isAFunction()) {
            SymbolDecl varOrFuncDecl = publicSymbolTable().getTopVariableDecl(name);
            WrapperTypeSpec localReturnType = functionTypeSpec().returnType;
            boolean returnsArrayTypeInFortran = isFortran()
                    && TypeSpec.isA(localReturnType, SymbolTableConstants.ARRAYTYPE)
                    && !localReturnType.isCharacter()
                    && !localReturnType.isString();
            if (varOrFuncDecl == null) {
                // We can have only one function/method called "name" in the method's symbolTable in question !
                TapList<FunctionDecl> funcDecls = publicSymbolTable().getTopFunctionDecl(name, localReturnType, functionTypeSpec().argumentsTypes, true);
                varOrFuncDecl = funcDecls == null ? null : funcDecls.head;
            }

            if ((varOrFuncDecl == null
                    || ((privateDeclBlock == null || !varOrFuncDecl.isVarDeclared(privateDeclBlock.instructions))
                    && !varOrFuncDecl.isVarDeclared(publicDeclarationsInstructions)))
                    && (otherReturnVar == null
                    || ((privateDeclBlock == null || !otherReturnVar.isVarDeclared(privateDeclBlock.instructions))
                    && !otherReturnVar.isVarDeclared(publicDeclarationsInstructions)))
                    && functionTypeSpec() != null) {
                if (varOrFuncDecl != null) {
                    varOrFuncDecl.setNoneInstruction();
                }
                if (returnsArrayTypeInFortran) {
                    returnTypeTree = null;
                    if (varOrFuncDecl == null) {
                        varOrFuncDecl =
                                new VariableDecl(ILUtils.build(ILLang.op_ident, name), localReturnType);
                    }
                    if (varOrFuncDecl instanceof VariableDecl) {
                        Tree newDecl = ((VariableDecl) varOrFuncDecl).buildVarDeclaration(publicSymbolTable());
                        toNewDeclTree.setObj(newDecl);
                    }
                } else {
                    returnTypeTree =
                            localReturnType.generateTree(null, null, externalTypes, true, null);
                }
            } else if (this.isC()) {
                returnTypeTree =
                        localReturnType.generateTree(null, null, externalTypes, true, null);
            } else {
                returnTypeTree = ILUtils.build(ILLang.op_none);
            }
        } else {
            returnTypeTree = ILUtils.build(ILLang.op_void);
        }
        return returnTypeTree;
    }

    protected void importAllSymbolTables() {
        TapList<TapTriplet<Unit, Tree, Instruction>> otherMods = null;
        if (importedModulesUseDecl != null) {
            TapList<TapTriplet<Unit, Tree, Instruction>> newImportedModules = null;
            TapList<Instruction> useDecls = importedModulesUseDecl;
            // TapList of TapPair (implicitlyImportedModule, renamedOnlyTree)
            TapList<TapTriplet<Unit, Tree, Instruction>> allImportedModules = null;
            if (importedModulesAndUseInfo == null) {
                while (useDecls != null) {
                    Tree useDecl = useDecls.head.tree;
                    String moduleName = ILUtils.getIdentString(useDecl.down(1));
                    Unit module = externalSymbolTable().getModule(moduleName);
                    if (module != null && !module.isUndefined()) {
                        Tree onlyRenamed = ILUtils.copy(useDecl.down(2));
                        allImportedModules = TapList.append(
                                new TapList<>(new TapTriplet<>(module, onlyRenamed, useDecls.head), null),
                                allImportedModules);
                        TapTriplet<Unit, Tree, Instruction> initialImport =
                            new TapTriplet<>(module, onlyRenamed, null) ;
                        otherMods = TapList.append(buildOtherModsRec(module, onlyRenamed,
                                                         new TapList<>(module, null),
                                                         new TapList<>(initialImport, null)),
                                                   otherMods);
                        newImportedModules = TapList.append(
                                newImportedModules,
                                new TapList<>(new TapTriplet<>(module, ILUtils.copy(useDecl.down(2)), useDecls.head), null));
                    } else if (moduleName.equals("mpi") || moduleName.equals("ampi")
                            || moduleName.equals("iso_c_binding")) {
                        // il faut laisser le use meme si on n'a pas le module
                        module = buildPredefinedModule(moduleName);
                        newImportedModules = TapList.append(
                                newImportedModules,
                                new TapList<>(new TapTriplet<>(module, ILUtils.copy(useDecl.down(2)), useDecls.head), null));
                    } else {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, useDecl, "(DD15) Module " + moduleName + " is not defined");
                    }
                    useDecls = useDecls.tail ;
                }
                importedModulesAndUseInfo = newImportedModules;
            } else {
                importedModulesAndUseInfo = null;  // [llh] this is bizarre and possibly to be removed...
            }
            TapList<TapTriplet<Unit, Tree, Instruction>> deeperImportedModules = otherMods;
            importedModulesAndUseInfo = newImportedModules;
            importedModules = buildImportedModules(importedModulesAndUseInfo);
            TapList<TapTriplet<Unit, Tree, Instruction>> deeperUseDecls = deeperImportedModules;
            while (deeperUseDecls != null) {
                importsSymbolTable.importModuleSymbolTable(deeperUseDecls.head, false);
                deeperUseDecls = deeperUseDecls.tail;
            }
            // il ne faut pas supprimer les useDecl multiples cf v349
            // on ne peut pas les merger correctement dans un useDecl
            TapList<TapTriplet<Unit, Tree, Instruction>> useDeclsInfo = importedModulesAndUseInfo;
            while (useDeclsInfo != null) {
                importsSymbolTable.importModuleSymbolTable(useDeclsInfo.head, true);
                useDeclsInfo = useDeclsInfo.tail;
            }
            fixImportedDeclarations();
        }
    }

    protected void importInheritedClassesSymbolTables() {
        if (parentClasses != null && this.isClass()) {
            // On ne traite pour le moment que l'heritage simple
            if (TapList.length(parentClasses) > 1) {
                TapEnv.fileWarning(TapEnv.MSG_SEVERE, null, "(TC70) Multiple inheritance is not well managed yet (class " + this.name + ')');
            }
            TapList<TapTriplet<Unit, Boolean, Boolean>> tmpParentClasses = parentClasses;
            Unit parentClass;
            boolean isVirtualInheritance;
            boolean isPublicInheritance;
            SymbolTable targetSymbolTable;
            while (tmpParentClasses != null) {
                parentClass = tmpParentClasses.head.first;
                isVirtualInheritance = tmpParentClasses.head.second;
                isPublicInheritance = tmpParentClasses.head.third;
                if (parentClass.publicSymbolTable() != null) {
                    targetSymbolTable = isPublicInheritance ? publicSymbolTable() : privateSymbolTable();
                    targetSymbolTable.importAllClassSymbolTable(
                            parentClass.publicSymbolTable(), this, parentClass, isVirtualInheritance);
                }
                if (parentClass.protectedSymbolTable() != null) {
                    targetSymbolTable = isPublicInheritance ? protectedSymbolTable() : privateSymbolTable();
                    targetSymbolTable.importAllClassSymbolTable(
                            parentClass.protectedSymbolTable(), this, parentClass, isVirtualInheritance);
                }
                tmpParentClasses = tmpParentClasses.tail;
            }
        }
    }

    /** Builds the list of triplets (imported-module, import-restrictions, use-instruction) for all modules
     * imported (directly or recursively) by "module", given (as "moduleRestrictions") the
     * specific import restrictions on "module" itself in this particular context.
     * @return the list of imported pairs, shallow imports first.
     * @param dejaVu avoids infinite recursion on cycles of USE declarations.
     * @param dejaVu2 a stronger "deja-vu" argument, hopefully useless and even perhaps wrong,
     * to work around the cycling bug found by Jean. It accumulates deja-vu modules from
     * all visited branches rather than only from the current branch.
     */
    private TapList<TapTriplet<Unit, Tree, Instruction>>
        buildOtherModsRec(Unit module, Tree moduleRestrictions,
                          TapList<Unit> dejaVu,
                          TapList<TapTriplet<Unit, Tree, Instruction>> dejaVu2) {
        TapList<TapTriplet<Unit, Tree, Instruction>> deeperImportedModules = null;
        if (!module.isPrivate()) {
            deeperImportedModules = renameRenamings(module.importedModulesAndUseInfo, moduleRestrictions) ;
            TapList<TapTriplet<Unit, Tree, Instruction>> otherModsLevel1 = deeperImportedModules;
            Unit importedModule;
            Tree importRestrictions;
            while (otherModsLevel1 != null) {
                importedModule = otherModsLevel1.head.first;
                importRestrictions = otherModsLevel1.head.second;
                if (TapList.contains(dejaVu, importedModule)) {
                    String recurChain = importedModule.name ;
                    TapList<Unit> inDejaVu = dejaVu ;
                    while (inDejaVu!=null) {
                        recurChain = inDejaVu.head.name+" USEs "+recurChain ;
                        inDejaVu = inDejaVu.tail ;
                    }
                    // USE recursivity a priori forbidden, but just in case (cf set07/v401)
                    TapEnv.fileWarning(TapEnv.MSG_SEVERE, null, "(TC42) Module USE recursivity: " + recurChain);
                } else if (alreadyImportedThisWay(dejaVu2, importedModule, importRestrictions)) {
                    // [llh 27/06/12] If we already encountered this "importedModule", we prefer not to
                    // look at it twice. Otherwise complexity sometimes explodes...
                    //TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "Problematic repeated USE modules. New "+importedModule+" in "+dejaVu2) ;
                } else {
                    dejaVu2.placdl(otherModsLevel1.head);
                    TapList<TapTriplet<Unit, Tree, Instruction>> otherMods1 =
                        buildOtherModsRec(importedModule, importRestrictions,
                                          new TapList<>(importedModule, dejaVu), dejaVu2);
                    deeperImportedModules = TapList.append(deeperImportedModules, otherMods1);
                }
                otherModsLevel1 = otherModsLevel1.tail;
            }
        }
        return deeperImportedModules;
    }

    private boolean alreadyImportedThisWay(TapList<TapTriplet<Unit, Tree, Instruction>> dejaVu2,
                                           Unit module, Tree restrictions) {
        boolean found = false ;
        while (!found && dejaVu2!=null) {
            if (dejaVu2.head.first == module &&
                (ILUtils.isNullOrNone(dejaVu2.head.second)
                 || dejaVu2.head.second.equalsTree(restrictions)))
                found = true ;
            dejaVu2 = dejaVu2.tail ;
        }
        return found ;
    }

    protected boolean isPrivate() {
        boolean result;
        if (isModule()) {
            result = fortranStuff != null &&
                    fortranStuff.publicPrivateDefault != null &&
                    fortranStuff.publicPrivateDefault.equals("PRIVATE");
        } else {
            result = modifiers != null && modifiers.opCode() == ILLang.op_modifiers
                    && modifiers.length() != 0
                    && modifiers.down(1).opCode() == ILLang.op_ident
                    && modifiers.down(1).stringValue().equals("private");
        }
        return result;
    }

    private Unit buildPredefinedModule(String moduleName) {
        Unit result;
        result = callGraph.getUnit(moduleName);
        if (result == null || result.isUndefined()) {
            String resourcesDirectory = TapEnv.tapenadeHome() + File.separator + "resources" + File.separator;
            String inputFileName = resourcesDirectory + TapEnv.get().stdLibraryDirectory + moduleName + "f.f90";
            try {
                TapEnv.printlnOnTrace(15, "@@ Loading " + moduleName + " information from library " + inputFileName);
                Tapenade.parseFiles(false, "." + File.separator,
                                    new TapList<>(new TapPair<>(inputFileName, TapEnv.FORTRAN90), null), callGraph, null, 1, false);
                Unit predefinedUnit = callGraph.getUnit(moduleName);
                predefinedUnit.name = moduleName;
                predefinedUnit.isPredefinedModuleOrTranslationUnit = true;
                result = predefinedUnit;
                predefinedUnit.typeCheck();
                TapList<Unit> predefLowerLevelUnits = predefinedUnit.lowerLevelUnits;
                while (predefLowerLevelUnits != null) {
                    predefLowerLevelUnits.head.typeCheck();
                    predefLowerLevelUnits = predefLowerLevelUnits.tail;
                }
            } catch (IOException e) {
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "Definition of " + moduleName + " module not found in " + TapEnv.get().stdLibraryDirectory);
            }
        }
        return result;
    }

    private TapList<TapTriplet<Unit, Tree, Instruction>>
        renameRenamings(TapList<TapTriplet<Unit, Tree, Instruction>> importedModules, Tree renamedOnly) {
        TapList<TapTriplet<Unit, Tree, Instruction>> result = null;
        TapTriplet<Unit, Tree, Instruction> op;
        while (importedModules != null) {
            Unit importedModule = importedModules.head.first;
            Tree renamed = importedModules.head.second;
            op = new TapTriplet<>(importedModule,
                                  ILUtils.mergeUseDeclTrees(renamedOnly, renamed),
                                  importedModules.head.third);
            result = new TapList<>(op, result);
            importedModules = importedModules.tail;
        }
        return result;
    }

    private TapList<Unit> buildImportedModules(TapList<TapTriplet<Unit, Tree, Instruction>> impModulesAndUse) {
        TapList<Unit> result = null;
        while (impModulesAndUse != null) {
            result = new TapList<>(impModulesAndUse.head.first, result);
            impModulesAndUse = impModulesAndUse.tail;
        }
        return result;
    }

    private void fixImportedDeclarations() {
        TapList<SymbolTable> inSymbolTables = symbolTablesBottomUp;
        while (inSymbolTables != null) {
            inSymbolTables.head.fixImportedDeclarations();
            inSymbolTables = inSymbolTables.tail;
        }
        TapList<Unit> lowerUnits = lowerLevelUnits;
        while (lowerUnits != null) {
            lowerUnits.head.fixImportedDeclarations();
            lowerUnits = lowerUnits.tail;
        }
    }

    protected boolean subClassOf(Unit parentUnit) {
        return parentUnit == this || subClassOfRec(parentUnit);
    }

    private boolean subClassOfRec(Unit parentUnit) {
        boolean isSubClass = false;
        TapList<TapTriplet<Unit, Boolean, Boolean>> parents = this.parentClasses;
        while (!isSubClass && parents != null) {
            isSubClass = parents.head.first.subClassOf(parentUnit);
            parents = parents.tail;
        }
        return isSubClass;
    }

    /**
     * TypeChecks this Unit. TypeChecks each Instruction
     * of this Unit with respect to its enclosing Block's SymbolTable.
     * Supposes all declarations are already in the SymbolTable, even
     * when they were found later in the code, because all the declarations
     * are inserted into the SymbolTable during a previous phase
     * (cf FlowGraphBuilder). Adds implicit or error recovery declarations
     * for undeclared variables, and creates Unit's for undeclared
     * routines called.
     */
    public void typeCheck() {
        TapList<Block> inAllBlocks = allBlocks;
        Block block;
        TapList<Instruction> inInstructions;
        SymbolTable symbolTable;
        Instruction instruction;
        Tree instrOrExpr;
        // utilise' pour les statementFunctionDecl qui apparaissent au debut
        ToBool isBeginning = new ToBool(true);

        TapEnv.printlnOnTrace(15, "@@ Checking types for unit: " + this.name);
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            inInstructions = block.instructions;
            symbolTable = block.symbolTable;
            if ((this.isModule() || this.isClass()) && privateSymbolTable() != null) {
                // An instruction in a module or class can use private symbols of this module or class:
                symbolTable = privateSymbolTable();
            }
            while (inInstructions != null) {
                instruction = inInstructions.head;
                instrOrExpr = instruction.tree;
                if (instrOrExpr != null) {
                    symbolTable.typeCheck(instrOrExpr, false, false, false,
                            instruction, isBeginning, this);
                }
                inInstructions = inInstructions.tail;
            }
            isBeginning.set(isBeginning.get()
                    && (block.instructions == null
                    || block.lastInstr().isADeclaration()
                    || block.lastInstr().tree.opCode() == ILLang.op_data));
            inAllBlocks = inAllBlocks.tail;
        }
        // il faut faire computeSave apres le typeCheck des call
        // pour ne pas mettre des FunctionDecl en save
        if (fortranStuff != null) {
            if (fortranStuff.getSaveAll()) {
                fortranStuff.computeSaveVarDecls(this.publicSymbolTable(),
                    this.publicSymbolTable().getAllTopVariableDecls());
                fortranStuff.computeSaveVarDecls(this.privateSymbolTable(),
                    this.privateSymbolTable().getAllTopVariableDecls());
            }
            // Fill the MemoryMaps with all collected COMMON, EQUIVALENCE, SAVE, DATA, BIND,... declarations.
            while (fortranStuff.listFortranCEDS!=null) {
                TapPair<Tree, SymbolTable> layoutDecl = fortranStuff.listFortranCEDS.head ;
                MemoryMaps.placeCEDSIntoMemoryMaps(layoutDecl.first, layoutDecl.second, this, localMaps, callGraph) ;
                fortranStuff.listFortranCEDS = fortranStuff.listFortranCEDS.tail ;
            }
            //fortranStuff.commonEquivsIntoMemoryMap();
            fortranStuff.declareBindCInMemoryMap();

            fortranStuff.implicitDeclarations = null;
        }
// [Unique FunctionDecl mode:]
//        TapList<???> inSymbolTables = symbolTablesBottomUp;
//        symbolTable = (SymbolTable)inSymbolTables.head ;
//        while (inSymbolTables!=null) {
//            // Remove duplicate FunctionDecls, because they are useless after Type-Checking.
//            if (!symbolTable.isFormalParamsLevel() && symbolTable.basisSymbolTable()!=null)
//                symbolTable.removeDuplicateFunctionDecls() ;
//            inSymbolTables = inSymbolTables.tail;
//        }
// [End FunctionDecl mode.]
        // Add annotations on function names (needed only on declaration instructions ?) :
        if (publicSymbolTable != null) {
            inAllBlocks = TapList.addUnlessPresent(allBlocks, publicSymbolTable.declarationsBlock);
        }
        if (privateSymbolTable != null) {
            inAllBlocks = TapList.addUnlessPresent(inAllBlocks, privateSymbolTable.declarationsBlock);
        }
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            symbolTable = block.symbolTable;
            inInstructions = block.instructions;
            while (inInstructions != null) {
                instruction = inInstructions.head;
                if (instruction.tree != null && (instruction.isADeclaration() || instruction.tree.opCode() == ILLang.op_data)) {
                    symbolTable.terminateFunctionDeclarationInstructions(instruction.tree);
                }
                symbolTable.collectPointedFunctionNames(instruction.tree) ;
                inInstructions = inInstructions.tail;
            }
            inAllBlocks = inAllBlocks.tail;
        }
        if (TapEnv.traceTypeCheckAnalysis()) {
            TapEnv.printlnOnTrace();
        }
    }

    /**
     * TypeChecks this interface. Checks coherence
     * between interface and complete declaration if found.
     */
    protected void typeCheckInterface() {
        TapEnv.printlnOnTrace(15, "@@ Checking types for interface: " + this.name);
        //Type-Check the declaration part of this interface, which is in Block 0 :
        TapList<Block> inAllBlocks = allBlocks;
        while (inAllBlocks != null) {
            Block declBlock = inAllBlocks.head;
            TapList<Instruction> inInstructions = declBlock.instructions;
            SymbolTable symbolTable = declBlock.symbolTable;
            ToBool isBeginning = new ToBool(true);
            Instruction curInstr;
            while (inInstructions != null) {
                curInstr = inInstructions.head;
                symbolTable.typeCheck(curInstr.tree, false, false, false,
                        curInstr, isBeginning, null);
                inInstructions = inInstructions.tail;
            }
            inAllBlocks = inAllBlocks.tail;
        }

        TapList<FunctionDecl> actualFunctionDecls = externalSymbolTable().getFunctionDecl(name, functionTypeSpec().returnType, functionTypeSpec().argumentsTypes, true);
        if (actualFunctionDecls == null) // [llh 12Sept2017] Recovery if inconsistency!
        {
            actualFunctionDecls = externalSymbolTable().getFunctionDecl(name, functionTypeSpec().returnType, null, true);
        }
        if (actualFunctionDecls == null) {
            actualFunctionDecls = publicSymbolTable.getTopFunctionDecl(name, null, null, false);
        }
        if (actualFunctionDecls == null) {
            actualFunctionDecls = privateSymbolTable.getTopFunctionDecl(name, null, null, false);
        }

        // We assume there is only one actualUnit corresponding to this interface:
        FunctionDecl actualFunctionDecl = actualFunctionDecls == null ? null : actualFunctionDecls.head;
        Unit unit = actualFunctionDecl == null ? null : actualFunctionDecl.unit();

        if (unit != null && !unit.headTree().equalsTree(this.headTree())
                // Don't modify the interface Unit after the true Unit in the cross-language case:
                && unit.language == this.language) {
            SymbolDecl[] interfaceParameterDecls = getParameterDecls();
            SymbolDecl[] unitParameterDecls = unit.getParameterDecls();
            if (interfaceParameterDecls.length != unitParameterDecls.length) {
                TapEnv.fileWarning(TapEnv.MSG_WARN, headTree(), "Interface and procedure headers differ: " + ILUtils.toString(unit.headTree()) + " and " + ILUtils.toString(this.headTree()));
            } else {
                TapList<Tree> interfaceDecls = findInterfaceDeclations();
                TapList<Tree> intfDecls;
                for (int i = 0; i < interfaceParameterDecls.length; i++) {
                    // on renomme les parametres dans l'interface:
                    if (!interfaceParameterDecls[i].symbol.equals(unitParameterDecls[i].symbol)) {
                        intfDecls = interfaceDecls;
                        while (intfDecls != null) {
                            ILUtils.replaceAllIdentsNamed(intfDecls.head, interfaceParameterDecls[i].symbol, "paramXXX" + i);
                            intfDecls = intfDecls.tail;
                        }
                    }
                }
                for (int i = 0; i < interfaceParameterDecls.length; i++) {
                    // on renomme les parametres dans l'interface:
                    if (!interfaceParameterDecls[i].symbol.equals(unitParameterDecls[i].symbol)) {
                        intfDecls = interfaceDecls;
                        while (intfDecls != null) {
                            ILUtils.replaceAllIdentsNamed(intfDecls.head, "paramXXX" + i, unitParameterDecls[i].symbol);
                            intfDecls = intfDecls.tail;
                        }
                    }
                }
                entryBlock.instructions.head.tree = ILUtils.copy(unit.headTree());
            }
        }

        if (unit != null
                && !unit.functionTypeSpec().equalsCompilDep(functionTypeSpec())) {
            WrapperTypeSpec returnType = functionTypeSpec().returnType;
            WrapperTypeSpec[] argumentsTypes = functionTypeSpec().argumentsTypes;
            WrapperTypeSpec otherReturnType =
                    unit.functionTypeSpec().returnType;
            WrapperTypeSpec[] otherArgumentsTypes =
                    unit.functionTypeSpec().argumentsTypes;
            boolean mismatch = false;
            if (unit.sameLanguage(this.language)) {
                mismatch = otherReturnType.wrappedType != null
                        && !otherReturnType.equalsCompilDep(returnType);
                if (!mismatch && otherArgumentsTypes != null && argumentsTypes != null) {
                    mismatch = otherArgumentsTypes.length != argumentsTypes.length;
                    for (int i = 0; !mismatch && i < argumentsTypes.length; ++i) {
                        mismatch = otherArgumentsTypes[i].wrappedType == null
                                || !otherArgumentsTypes[i].equalsCompilDep(argumentsTypes[i]);
                    }
                }
            } else {
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(DD999) Warning: type-checking of mixed language interface not yet implemented: " + name);
            }
            if (mismatch) {
                TapEnv.setRelatedUnit(unit);
                TapEnv.fileWarning(TapEnv.MSG_WARN, null, "(TC93) Type mismatch in interface " + name + ", " + functionTypeSpec().showType() + ", declared: " + unit.functionTypeSpec().showType());
            }
        }

        // Fill the MemoryMaps with all collected COMMON, EQUIVALENCE, SAVE, DATA, BIND,... declarations.
        if (fortranStuff != null) {
            while (fortranStuff.listFortranCEDS!=null) {
                TapPair<Tree, SymbolTable> layoutDecl = fortranStuff.listFortranCEDS.head ;
                MemoryMaps.placeCEDSIntoMemoryMaps(layoutDecl.first, layoutDecl.second, this, localMaps, callGraph) ;
                fortranStuff.listFortranCEDS = fortranStuff.listFortranCEDS.tail ;
            }
            //fortranStuff.commonEquivsIntoMemoryMap();
        }
    }

    protected WrapperTypeSpec[] typeCheckNameEqOptionalArguments(
            SymbolTable symbolTable, Tree[] actualArgs, WrapperTypeSpec[] actualTypes,
            WrapperTypeSpec[] nameEqTypes, int rankOfFirstNameEq, Tree newCallTree,
            Tree optionalTree, ToBool callUsesNameEq) {
        int nbFormalArgs = functionTypeSpec().argumentsTypes.length;
        WrapperTypeSpec[] result = new WrapperTypeSpec[nbFormalArgs];
        SymbolDecl[] parameterDecls = getParameterDecls();
        SymbolDecl parameter;
        String argName;
        int maxi = parameterDecls == null ? 0 : parameterDecls.length;
        if (maxi > nbFormalArgs) {
            maxi = nbFormalArgs;
        }
        for (int i = 0; i < maxi; ++i) {
            assert parameterDecls != null;
            if (parameterDecls[i] != null) {
                ILUtils.getArguments(newCallTree).setChild(
                        ILUtils.build(ILLang.op_nameEq,
                                ILUtils.build(ILLang.op_ident, parameterDecls[i].symbol),
                                ILUtils.build(ILLang.op_none)),
                        i + 1);
            }
        }
        if (actualArgs != null) {
            for (int i = 0; i < actualArgs.length; i++) {
                if (actualArgs[i].opCode() == ILLang.op_nameEq) {
                    callUsesNameEq.set(true);
                    argName = ILUtils.getIdentString(actualArgs[i].down(1));
                    ToInt formalArgRank = new ToInt(SymbolTableConstants.UNDEFINED_FORMAL_ARG_RANK);
                    parameter = findParameter(parameterDecls, argName, formalArgRank);
                    WrapperTypeSpec typeSpec = nameEqTypes[i];
                    if (parameter != null) {
                        result[formalArgRank.get() - 1] = typeSpec;
                        ILUtils.getArguments(newCallTree).setChild(ILUtils.copy(actualArgs[i].down(2)),
                                formalArgRank.get());
                        ILUtils.getArguments(newCallTree).down(formalArgRank.get()).
                                setAnnotation("sourcetree", actualArgs[i]);
                        ILUtils.getArguments(optionalTree).setChild(ILUtils.copy(actualArgs[i]),
                                formalArgRank.get());
                    } else {
                        if (!this.isIntrinsic()) {
                            TapEnv.fileWarning(TapEnv.MSG_WARN, actualArgs[i], "(TC53) Incorrect keyword argument " + argName + " in call to procedure " + ILUtils.getCalledNameString(newCallTree));
                            ILUtils.getArguments(optionalTree).setChild(ILUtils.copy(actualArgs[i]),
                                    i + 1);
                        } else if (TapEnv.relatedUnit().isFortran9x()
                                && this.name.equals("sum")) {
                            if (argName.equals("mask")) {
                                //mask must have the same shape as the first argument of sum
                                symbolTable.setConformingDimension(
                                        symbolTable.getTypeDecl("boolean").typeSpec,
                                        actualTypes[0], typeSpec, newCallTree);
                            }
                        }
                        ILUtils.getArguments(newCallTree).setChild(ILUtils.copy(actualArgs[i]),
                                i + 1);
                        if (i < nbFormalArgs) {
                            result[i] = typeSpec;
                        }
                    }
                } else {
                    ILUtils.getArguments(newCallTree).setChild(ILUtils.copy(actualArgs[i]), i + 1);
                    if (i < nbFormalArgs) {
                        result[i] = actualTypes[i];
                    }
                    if (i > rankOfFirstNameEq) {
                        TapEnv.fileWarning(TapEnv.MSG_WARN, actualArgs[i], "(TC54) Missing keyword argument in procedure " + ILUtils.getCalledNameString(newCallTree) + " call");
                    }
                    ILUtils.getArguments(optionalTree).setChild(ILUtils.copy(actualArgs[i]), i + 1);
                }
            }
        }
        return result;
    }

    private TapList<Tree> findInterfaceDeclations() {
        TapList<Tree> result = null;
        TapList<Instruction> interfaceDecls = interfaceDeclarations;
        TapList<Tree> interfaces;
        while (interfaceDecls != null) {
            interfaces = interfaceDecls.head.tree.down(2).childrenList();
            while (interfaces != null) {
                if (interfaces.head.opCode() == ILLang.op_interface
                        && interfaces.head.down(1).opCode() == ILLang.op_function
                        && interfaces.head.down(1).down(4).stringValue().equals(name)) {
                    result = new TapList<>(interfaces.head.down(1), result);
                }
                interfaces = interfaces.tail;
            }
            interfaceDecls = interfaceDecls.tail;
        }
        return result;
    }

    private SymbolDecl findParameter(SymbolDecl[] parameterDecls, String param, ToInt rank) {
        SymbolDecl result = null;
        if (parameterDecls != null) {
            int i = 0;
            while (result == null && i < parameterDecls.length) {
                if (parameterDecls[i] != null && parameterDecls[i].symbol.equals(param)) {
                    result = parameterDecls[i];
                    rank.set(i + 1);
                }
                i++;
            }
        }
        return result;
    }

    protected void typeCheckOptionalArguments(WrapperTypeSpec[] actualTypes, Tree newCallTree,
                                              Tree optionalTree, ToBool callUsesNameEq) {
        SymbolDecl[] parameterDecls = getParameterDecls();
        int nbArgs = ILUtils.getArguments(newCallTree).length();
        int nbParams = functionTypeSpec().argumentsTypes.length;
        int neededArgs = nbParams - nbArgs; //useful in Fortran
        WrapperTypeSpec paramTypeSpec;

        if (parameterDecls != null) {
            for (int i = 0; i < parameterDecls.length; i++) {
                if (parameterDecls[i] == null || parameterDecls[i].isOptional() && neededArgs != 0) {
                    if (!callUsesNameEq.get()) {
                        ILUtils.getArguments(optionalTree).addChild(ILUtils.build(ILLang.op_none), i + 1);
                    }
                    if (parameterDecls[i] != null) {
                        paramTypeSpec = parameterDecls[i].type();
                    } else {
                        paramTypeSpec = new WrapperTypeSpec(null);
                    }
                    insertTypeSpec(actualTypes, paramTypeSpec, i);
                    neededArgs = neededArgs - 1;
                } else if (parameterDecls[i] != null && actualTypes != null
                        && actualTypes.length > i && (actualTypes[i] == null || actualTypes[i].wrappedType == null)) {
                    actualTypes[i] = parameterDecls[i].type();
                }
            }
        }
    }

    private void insertTypeSpec(WrapperTypeSpec[] actualTypes, WrapperTypeSpec typeSpec, int pos) {
        if (pos < actualTypes.length) {
            if (actualTypes.length - 1 - pos >= 0) {
                System.arraycopy(actualTypes, pos, actualTypes, pos + 1, actualTypes.length - 1 - pos);
            }
            actualTypes[pos] = typeSpec;
        }
    }

    protected void preprocess() {
        if (hasSource()) {
            if (TapEnv.get().traceTypeCheck != null && TapEnv.get().traceTypeCheck.head.equals("%all%")) {
                TapEnv.setTraceTypeCheckAnalysis(true);
            }
            TapEnv.get().inlinedFunctions.findInlinedFunctions(this);
            //Because Block#0 may have been changed by inline:
            if (privateSymbolTable() != null) {
                if (allBlocks != null) {
                    privateSymbolTable().declarationsBlock = allBlocks.head;
                }
                splitInstructions();
                privateSymbolTable().declarationsBlock = null;
            }
            distributeOMPclauses();
            makeFlowGraph(entryBlock, allBlocks, exitBlock, false);
            TapEnv.setTraceTypeCheckAnalysis(false);
        }
    }

    /**
     * Move "schedule" clauses from each OMP PARALLEL to its contained OMP DO's.
     */
    private void distributeOMPclauses() {
        TapList<Block> inAllBlocks = allBlocks;
        TapList<TapPair<Instruction, Tree>> collectedSchedules = null;
        Block block;
        Tree ompPragma;
        Tree schedule;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            if (block.isParallelController()) {
                ompPragma = block.instructions.head.tree;
                if (ompPragma.opCode() == ILLang.op_parallelRegion) {
                    schedule = ILUtils.getRemoveSchedule(ompPragma);
                    if (schedule != null) {
                        collectedSchedules =
                                new TapList<>(new TapPair<>(block.instructions.head, schedule), collectedSchedules);
                    }
                }
            }
            inAllBlocks = inAllBlocks.tail;
        }
        inAllBlocks = allBlocks;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            if (block.isParallelController()) {
                ompPragma = block.instructions.head.tree;
                if (ompPragma.opCode() == ILLang.op_parallelLoop) {
                    TapList<Instruction> enclosings = block.parallelControls;
                    while (enclosings != null) {
                        schedule = TapList.cassq(enclosings.head, collectedSchedules);
                        if (schedule != null) {
                            ILUtils.addSchedule(ILUtils.copy(schedule), ompPragma);
                        }
                        enclosings = enclosings.tail;
                    }
                }
            }
            inAllBlocks = inAllBlocks.tail;
        }
    }

    private void splitInstructions() {
        TapList<Block> inAllBlocks = allBlocks;
        Block block;
        boolean isADoLoopHeader;
        TapList<Instruction> beforeEntry, beforeCycle ;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            // new instructions split out of a loop header must be moved to before loop entry and/or loop cycle:
            isADoLoopHeader = block.isALoop() && block instanceof HeaderBlock;
            beforeEntry = (isADoLoopHeader ? new TapList<>(null, null) : null) ;
            beforeCycle = (isADoLoopHeader ? new TapList<>(null, null) : null) ;
            block.splitInstructions(beforeEntry, beforeCycle);
            if (isADoLoopHeader) {
                // if the do header was split, we must put split code into one or two new Blocks
                beforeEntry = TapList.nreverse(beforeEntry.tail) ;
                if (beforeEntry!=null) {
                    Block blockBeforeEntry = new BasicBlock(block.symbolTable, block.parallelControls, null);
                    blockBeforeEntry.setOrigLabel(block.origLabel());
                    block.setOrigLabel(null);
                    blockBeforeEntry.instructions = beforeEntry ;
                    ((HeaderBlock) block).insertBlockBeforeLoopEntry(blockBeforeEntry) ;
                    updateNestsOfBlocks(block.enclosingLoop(),
                            new TapList<>(blockBeforeEntry, new TapList<>(block.enclosingLoop(), null))) ;
                }
                beforeCycle = TapList.nreverse(beforeCycle.tail) ;
                if (beforeCycle!=null) {
                    Block blockBeforeCycle = new BasicBlock(block.symbolTable, block.parallelControls, null);
                    blockBeforeCycle.instructions = beforeCycle ;
                    ((HeaderBlock) block).insertBlockBeforeLoopCycle(blockBeforeCycle) ;
                    updateNestsOfBlocks(block,
                            new TapList<>(block, new TapList<>(blockBeforeCycle, null))) ;
                }
            }
            inAllBlocks = inAllBlocks.tail;
        }
        TapList<SymbolTable> symbolTables = symbolTablesBottomUp();
        while (symbolTables != null) {
            symbolTables.head.solveNewSymbolHolders(false);
            symbolTables = symbolTables.tail;
        }
    }

    /**
     * Inside the list of Blocks that holds this level of the nest of loops,
     * i.e. the nested level that contains "block", replaces "block"
     * by "blocks".
     */
    protected void updateNestsOfBlocks(Block block, TapList<Block> blocks) {
        TapList<Block> toLevelAbove;
        TapList<Block> inLevelAbove;
        if (block.enclosingLoop() == null) {
            toLevelAbove = new TapList<>(null, topBlocks());
        } else {
            toLevelAbove = new TapList<>(null, block.enclosingLoop().inside);
        }
        inLevelAbove = toLevelAbove;
        while (inLevelAbove != null && inLevelAbove.tail != null) {
            if (inLevelAbove.tail.head == block) {
                inLevelAbove.tail =
                        TapList.append(blocks, inLevelAbove.tail.tail);
                inLevelAbove = null;
            } else {
                inLevelAbove = inLevelAbove.tail;
            }
        }
        if (block.enclosingLoop() == null) {
            topBlocks = toLevelAbove.tail;
        } else {
            block.enclosingLoop().inside = toLevelAbove.tail;
        }
    }

    protected void checkMessagePassingCalls(CallGraph callGraph) {
        MPIcallInfo.checkMessagePassingCalls(callGraph);
    }

    private TapList explicitTargetZoneInfos(TapList targetZoneTree,
                                            SymbolTable origSymbolTable, Unit origUnit) {
        TapList hdResult = new TapList<>(null, null);
        TapList tlResult = hdResult;
        while (targetZoneTree != null) {
            if (targetZoneTree.head instanceof TapList) {
                tlResult =
                        tlResult.placdl(explicitTargetZoneInfos((TapList) targetZoneTree.head,
                                origSymbolTable, origUnit));
            } else if (targetZoneTree.head instanceof TapIntList) {
                tlResult =
                        tlResult.placdl(explicitTargetZoneInfos((TapIntList) targetZoneTree.head,
                                origSymbolTable, origUnit));
            } else {
                tlResult = tlResult.placdl(null);
            }
            targetZoneTree = targetZoneTree.tail;
        }
        return hdResult.tail;
    }

    private TapList explicitTargetZoneInfos(TapIntList targetZoneList,
                                            SymbolTable origSymbolTable, Unit origUnit) {
        TapList hdResult = new TapList<>(null, null);
        TapList tlResult = hdResult;
        ZoneInfo explicitZI;
        while (targetZoneList != null) {
            explicitZI = origSymbolTable.declaredZoneInfo(targetZoneList.head, SymbolTableConstants.ALLKIND) ;
            tlResult = tlResult.placdl(explicitZI);
            targetZoneList = targetZoneList.tail;
        }
        return hdResult.tail;
    }

    /**
     * Allocates the Zone numbers for the declared variables in this Unit,
     * then (except if on a package) recursively for the sub-units declared locally.
     * @param firstZones the next available zone numbers, for (anyType, int, real, ptr)
     * @param staticMemory when true, allocate only the "static" zones of this Unit,
     *  i.e. those of the MODULE variables, the CLASS static variables, the various sort of remanent variables...
     * @param toAllocatedZones collects the ordered list of ZoneInfo created (in decreasing rank order).
     *  This argument is used only when staticMemory==true. Otherwise pass null.
     */
    protected void allocateDeclaredZones(int[] firstZones, boolean staticMemory, TapList<ZoneInfo> toAllocatedZones) {
        if (publicSymbolTable == null) {
            return;
        }
        SymbolTable basisNumberingST = (isTopInFile() ? callGraph.languageRootSymbolTable(language) : upperLevelUnit.privateSymbolTable()) ;
        publicSymbolTable.computeFirstDeclaredZone(firstZones, basisNumberingST);
        ZoneAllocator zoneAllocator = new ZoneAllocator(publicSymbolTable);
        if (hasSource()) {
            // Ask for creation of zones for the "initial" destination(s) of POINTER formal args:
            zoneAllocator.setAllocateZonesForPointed(true) ;
        }
        localMaps.allocateZones(zoneAllocator, this) ; //was declaredMemoryMap.allocateZones(zoneAllocator, this);
        publicSymbolTable.allocateZones(zoneAllocator, this);
        zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedZones);
        TapList<SymbolTable> privateSymbolTables = TapList.reverse(symbolTablesBottomUp).tail;
        // Work around a bug due to somebody putting
        // the same SymbolTable twice during inlining => TODO: Fix that bug!
        privateSymbolTables = TapList.unique(privateSymbolTables);
        SymbolTable symbolTable ;
        while (privateSymbolTables != null) {
            symbolTable = privateSymbolTables.head;
            symbolTable.computeFirstDeclaredZone(firstZones, symbolTable.basisSymbolTable());
            zoneAllocator = new ZoneAllocator(symbolTable);
            if (!isStandard()) {
                zoneAllocator.setAllocateZonesForPointed(true) ;
            }
            if (symbolTable==privateSymbolTable()) {
                if (isStandard()) {
                    // Prepare the special zones for control-flow decisions:
                    computeTestZones() ;
                    WrapperTypeSpec testType = new WrapperTypeSpec(null) ;
                    testZoneInfos = new int[testZonesNb] ;
                    for (int i=0 ; i<testZonesNb ; ++i) {
                        Tree testDummy = ILUtils.build(ILLang.op_ident, "%test_"+i+"%") ;
                        ZoneInfoAccessElements access =
                            new ZoneInfoAccessElements(new VariableDecl(testDummy, testType), null, symbolTable) ;
                        testZoneInfos[i] =
                            ((TapIntList)zoneAllocator.allocateOneZone(testType, -1, new TapList<>(access, null),
                                              null, -1, false, false, false, false, null).head).head ;
                        ZoneInfo lastZoneInfo = zoneAllocator.lastAllocated() ;
                        lastZoneInfo.description = "test_"+i ;
                        lastZoneInfo.setKind(SymbolTableConstants.CONTROL) ;
                    }
                }
                localMaps.allocateZones(zoneAllocator, this) ;
            } else {
                // Only if some day IL allows EQUIVALENCEs in local scopes:
                // symbolTable.localMaps.allocateZones(zoneAllocator, this) ;
            }
            symbolTable.allocateZones(zoneAllocator, this);
            zoneAllocator.extractAllZoneInfosIntoTargetSymbolTable(toAllocatedZones);
            privateSymbolTables = privateSymbolTables.tail;
        }
        //localMaps = null; // usage of this MemoryMaps finishes here.
        for (int i=0 ; i<4 ; ++i) { // i.e. for i in {ALLKIND, REALKIND, INTKIND, PTRKIND}
            firstZones[i] = bodySymbolTable().freeDeclaredZone(i);
        }
	Unit subUnit ;
        for (TapList<Unit> subUnits=lowerLevelUnits ; subUnits!=null ; subUnits=subUnits.tail) {
	    subUnit = subUnits.head ;
	    if (staticMemory) { // Allocate static, global zones:
		if (subUnit.isPackage()) {
		    if (TapEnv.traceTypeCheckAnalysis()) {
			TapEnv.printlnOnTrace("  allocate static memory zones for CONTAINed " + subUnit.name()+" starting at z="+firstZones[0]) ;
		    }
		    subUnit.allocateDeclaredZones(firstZones, true, toAllocatedZones) ;
		}
	    }
	}
    }

    protected void computeZoneInterfaces() {
        if (isProcedureWithCode() || kind == MODULE || kind == INTERFACE) {
            int declNb = publicSymbolTable.declaredZonesNb(SymbolTableConstants.ALLKIND);
            int[] globNb4 = publicSymbolTable.firstDeclaredZone4();
            int z;
            int rz;
            int iz;
            int pz;
            int nbElemArgs = 0;
            // Accumulates elements of the future externalShape:
            TapList<ZoneInfo> shapeElements = null;
            for (z = 0; z < declNb; ++z) {
                ZoneInfo zi = publicSymbolTable.declaredZoneInfo(z, SymbolTableConstants.ALLKIND);
                // pour les contained subroutines, les variables de la unit
                // englobante sont des GLOBAL (pas des parameter ou result)
                // [vmp]: les varfunctions ne changent pas !
                if (z < globNb4[0] && zi != null && zi.variableNames() != null) {
                    TapList<FunctionDecl> funcDecls1 = publicSymbolTable
                            .getTopFunctionDecl(zi.variableNames().head, null, null, false);
                    FunctionDecl funcDecl1 = funcDecls1 == null ? null : funcDecls1.head; // we assume to have only 1 function that matches with the name.
                    if (!(funcDecl1 != null && funcDecl1.isVarFunction())
                            && (zi.kind() == SymbolTableConstants.PARAMETER || zi.kind() == SymbolTableConstants.RESULT || zi.kind() == SymbolTableConstants.LOCAL)) {
                        ZoneInfo newZoneInfo = zi.copy();
                        newZoneInfo.shareActivity(zi) ;
                        newZoneInfo.setKind(SymbolTableConstants.GLOBAL);
                        newZoneInfo.index = z;
                        zi = newZoneInfo ;
                    }
                }
                if (zi != null && (kind == INTRINSIC || kind == EXTERNAL)
                        && zi.comesFromAllocate()) {
                    // The global zones for the "allocate" commands must not be
                    // accessible by an external or intrinsic procedure ([llh] not sure about the others?):
                    zi = null;
                }
                if (zi != null) {
                    shapeElements = new TapList<>(zi, shapeElements);
                    if (zi.kind() == SymbolTableConstants.PARAMETER || zi.kind() == SymbolTableConstants.RESULT) {
                        nbElemArgs++;
                    }
                    rz = zi.kindZoneNb(SymbolTableConstants.REALKIND);
                    iz = zi.kindZoneNb(SymbolTableConstants.INTKIND);
                    pz = zi.kindZoneNb(SymbolTableConstants.PTRKIND);
                }
            }
// This may be a design bug (cf set05/v060): interface Units may have a hole
// between last global and first parameter zone. Not sure this "hole" is justified...
if (kind==INTERFACE) globNb4 = importsSymbolTable().freeDeclaredZone4();
            this.globalZonesNumber4 = globNb4 ;
            publicZonesNumber4 = publicSymbolTable.freeDeclaredZone4();
            int shapeLength = TapList.length(shapeElements);
            externalShape = new ZoneInfo[shapeLength];
            for (int i = shapeLength - 1; i >= 0; --i) {
                externalShape[i] = shapeElements.head;
                shapeElements = shapeElements.tail;
            }
            relevantZones = new BoolVector(shapeLength) ;
            for (int i=0 ; i<shapeLength ; ++i) {
                if (!externalShape[i].isHidden) {
                    relevantZones.set(i, true) ;
                }
            }
            computeElemArgInfo(nbElemArgs);
            // Build the TapIntList of declared zones which are local,
            // but initialized, e.g. zones of vars in DATA declaration
            if (entryBlock != null) {
                TapIntList collectedZones = null;
                SymbolTable dataSymbolTable = publicSymbolTable();
                if (dataSymbolTable != null) {
                    collectedZones = collectZonesOfData(
                            dataSymbolTable.dataList, null,
                            dataSymbolTable);
                    collectedZones =
                            collectZonesOfInitializedVariables(dataSymbolTable, collectedZones);
                }
                dataSymbolTable = privateSymbolTable();
                if (dataSymbolTable != null) {
                    collectedZones = collectZonesOfData(
                            dataSymbolTable.dataList, collectedZones,
                            dataSymbolTable);
                    collectedZones =
                            collectZonesOfInitializedVariables(dataSymbolTable, collectedZones);
                }
                // Redundant with previous sweep. Should replace previous sweep asap.
                // Sweep on ZoneInfos, to find those that have an initial value.
                collectedZones =
                        collectZonesOfInitializedVarDecl(bodySymbolTable(), collectedZones);
                entryBlock.setInitializedLocalZones(collectedZones);
            }
        } else if (isOutside() || kind == VARFUNCTION || isDummy()) {
            //TODO: read externalShape and in-out info from an external database.
            setDefaultOrUpdateExternalShape();
            relevantZones = null ; //Maybe we should put something here?
            if (elemArgPublicRanks == null) {
                computeElemArgInfo(externalShape.length);
            }
            if (kind==INTRINSIC && genericArrow.coversFurther==null) {
                genericArrow.computeCoversFurther();
            }
        }
    }

    private TapIntList collectZonesOfData(TapList<Tree> trees, TapIntList collected,
                                          SymbolTable symbolTable) {
        while (trees != null) {
            collected = collectZonesOfData(trees.head, collected, symbolTable);
            trees = trees.tail;
        }
        return collected;
    }

    private TapIntList collectZonesOfData(Tree dataTree, TapIntList collected,
                                          SymbolTable symbolTable) {
        switch (dataTree.opCode()) {
            case ILLang.op_data:
                collected = collectZonesOfData(dataTree.down(1), collected, symbolTable);
                break;
            case ILLang.op_expressions: {
                Tree[] sons = dataTree.children();
                for (int i = sons.length - 1; i >= 0; i--) {
                    collected = collectZonesOfData(sons[i], collected, symbolTable);
                }
                break;
            }
            case ILLang.op_fieldAccess:
            case ILLang.op_arrayAccess:
                collected = collectZonesOfData(dataTree.down(1), collected, symbolTable);
                break;
            case ILLang.op_ident: {
                String varName = ILUtils.getIdentString(dataTree);
                VariableDecl varDecl = symbolTable.getVariableOrConstantOrFuncNameDecl(varName);
                TapIntList zones = ZoneInfo.listAllZones(varDecl.zones(), true);
                collected = TapIntList.append(collected, zones);
                break;
            }
            case ILLang.op_iterativeVariableRef: {
                collected = collectZonesOfData(ILUtils.baseTree(dataTree), collected, symbolTable);
                break;
            }
            default:
                TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(Data zones) Unexpected operator: " + dataTree.opName());
                break;
        }
        return collected;
    }

    /**
     * Collects into "collected" the list of all local zones of the
     * given "symbolTable" that have an initial value given by their declaration.
     */
    private TapIntList collectZonesOfInitializedVarDecl(SymbolTable symbolTable,
                                                        TapIntList collected) {
        ZoneInfo zoneInfo;
        SymbolDecl deepSymbolDecl;
        for (int zz = symbolTable.firstDeclaredZone(SymbolTableConstants.ALLKIND);
             zz < symbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND);
             ++zz) {
            zoneInfo = symbolTable.declaredZoneInfo(zz, SymbolTableConstants.ALLKIND);
            deepSymbolDecl = symbolTable.symbolDeclOf(zoneInfo.accessTree);
            if (deepSymbolDecl instanceof VariableDecl &&
                    ((VariableDecl) deepSymbolDecl).initializationTree() != null
                    ||
                    deepSymbolDecl instanceof FieldDecl &&
                            ((FieldDecl) deepSymbolDecl).initializationTree() != null) {
                collected = new TapIntList(zz, collected);
            }
        }
        return collected;
    }

    private TapIntList collectZonesOfInitializedVariables(SymbolTable symbolTable, TapIntList collected) {
        TapList<SymbolDecl> listOfVars = symbolTable.getAllTopVariableDecls();
        VariableDecl variableDecl;
        TapIntList zones;
        while (listOfVars != null) {
            variableDecl = (VariableDecl) listOfVars.head;
            if (variableDecl.initializationTree() != null) {
                zones = ZoneInfo.listAllZones(variableDecl.zones(), true);
                collected = TapIntList.append(collected, zones);
            }
            listOfVars = listOfVars.tail;
        }
        return collected;
    }

    /** Returns a translation of the given "vectorAllKind", which is based on the ALLKIND zones numbering,
     * into its subset dealing only with "kind" zones, and using the "kind" zones numbering. */
    public BoolVector focusToKind(BoolVector vectorAllKind, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return vectorAllKind ;
        int kindLength = publicZonesNumber(kind) ;
        BoolVector result = new BoolVector(kindLength) ;
        for (int i=externalShape.length-1 ; i>=0 ; --i) {
            ZoneInfo zi = externalShape[i] ;
            if (zi!=null) {
                int ki = zi.kindZoneNb(kind) ;
                if (ki>=0 && vectorAllKind.get(i)) {
                    result.set(ki, true) ;
                }
            }
        }
        return result ;
    }

    /** Reciprocal of focusToKind(). Fills the other zones with false */
    public BoolVector unfocusFromKind(BoolVector vectorKind, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return vectorKind ;
        BoolVector result = new BoolVector(externalShape.length) ;
        for (int i=externalShape.length-1 ; i>=0 ; --i) {
            ZoneInfo zi = externalShape[i] ;
            if (zi!=null) {
                int ki = zi.kindZoneNb(kind) ;
                if (ki>=0 && vectorKind.get(ki)) {
                    result.set(i, true) ;
                }
            }
        }
        return result ;
    }

    /** Same as focusToKind(BoolVector), but on a BoolMatrix */
    public BoolMatrix focusToKind(BoolMatrix matrixAllKind, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return matrixAllKind;
        int kindLength = publicZonesNumber(kind) ;
        BoolMatrix result = new BoolMatrix(kindLength, kindLength) ;
        if (!matrixAllKind.isImplicitZero()) {
            result.rows = new BoolVector[kindLength];
            for (int i=externalShape.length-1 ; i>=0 ; --i) {
                ZoneInfo zi = externalShape[i] ;
                if (zi!=null) {
                    int ki = zi.kindZoneNb(kind) ;
                    if (ki>=0) {
                        if (matrixAllKind.rows[i]==null) {
                            result.rows[ki] = null ;
                        } else {
                            result.rows[ki] = focusToKind(matrixAllKind.rows[i], kind) ;
                        }
                    }
                }
            }
        }
        return result ;
    }

    /** Same as focusToKind(BoolVector), but on a TapIntList of allKind zones.
     * @return those in the given ALLKIND zones that are actually of the required "kind",
     * as a new list of "kind" zones. */
    public TapIntList focusToKind(TapIntList allKindZones, int kind) {
        if (kind==SymbolTableConstants.ALLKIND) return allKindZones ;
        TapIntList hdResult = new TapIntList(-1, null) ;
        TapIntList tlResult = hdResult ;
        int ki ;
        while (allKindZones!=null) {
            ki = externalShape[allKindZones.head].kindZoneNb(kind) ;
            if (ki!=-1) {
                tlResult = tlResult.placdl(ki) ;
            }
            allKindZones = allKindZones.tail ;
        }
        return hdResult.tail ;
    }

    /**
     * @return the list of the "kind" zones of the result of this Unit
     */
    public TapIntList returnIndices(int kind) {
        ZoneInfo zoneInfo ;
        int ki ;
        TapIntList indices = null ;
        for (int i=externalShape.length-1 ; i>=0 ; --i) {
            zoneInfo = externalShape[i] ;
            if (zoneInfo.isResult()) {
                ki = zoneInfo.kindZoneNb(kind) ;
                if (ki!=-1) {
                    indices = new TapIntList(ki, indices) ;
                }
            }
        }
        return indices ;
    }

    protected void computeArrowZoneInterfaces() {
        TapList<CallArrow> arrows = callees;
        CallArrow arrow;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.destination != null && !arrow.destination.isPackage()) {
                arrow.computeCoversFurther();
            }
            arrows = arrows.tail;
        }
    }

    /**
     * If externalShape exists, checks its zone numbers with the types,
     * because the types may have been specified further since creation.
     * Otherwise, creates a default externalShape.
     */
    private void setDefaultOrUpdateExternalShape() {
        if (externalShape == null) {
            ZoneAllocator zoneAllocator = new ZoneAllocator(externalSymbolTable());
            zoneAllocator.setAllocateZonesForPointed(true) ;
            // We want the external shape of this unit to include the zoneinfos of
            // all global variables visible in the enclosing scope. So we pre-fill
            // this zoneAllocator with these already-built global zoneInfos:
            // (old comment??) TODO: take care of "additional" (used for mixed-language analysis)
            zoneAllocator.zoneInfos = callGraph.allGlobalsForExternals ;
            zoneAllocator.setZoneNumber4(callGraph.globalsForExternalsNumber4) ;
            this.globalZonesNumber4 = callGraph.globalsForExternalsNumber4 ;

            WrapperTypeSpec[] argumentsTypes = functionTypeSpec().argumentsTypes;
            ZoneInfoAccessElements dummyAccess = new ZoneInfoAccessElements(null, null, publicSymbolTable()) ;
            if (argumentsTypes != null) {
                for (int i = 0; i < argumentsTypes.length; i++) {
                    zoneAllocator.allocateZones(argumentsTypes[i], -1, null, new TapList<>(dummyAccess, null),
                                                argumentsTypes[i], i+1, false, false, false, null, publicSymbolTable(), null) ;
                }
            }
            WrapperTypeSpec returnType = functionTypeSpec().returnType;
            boolean hasReturn =
                returnType == null || returnType.wrappedType == null
                        || !TypeSpec.isA(returnType, SymbolTableConstants.VOIDTYPE);
            if (hasReturn) {
                zoneAllocator.allocateZones(returnType, -1, null, new TapList<>(dummyAccess, null),
                                            returnType, 0, false, false, false, null, publicSymbolTable(), null) ;
            }

            //Special: here we don't "extractAllZoneInfosInto()" into the targetSymbolTable of zoneAllocator.
            // We just extract the "ALLKIND" table of ZoneInfo's and use it as this Unit's externalShape.
            externalShape = zoneAllocator.extractAllZoneInfosAsExternalShape();
            publicZonesNumber4 =
                new int[]{zoneAllocator.nextZone,
                          zoneAllocator.nextRealZone,
                          zoneAllocator.nextIntZone,
                          zoneAllocator.nextPtrZone} ;
        } else {
            // The following is for external shapes of Units declared in Lib files
            // We must base their external shape on top of the globals that they see
            // through their externalSymbolTable(). This offsets all zone indices.
            // In addition, when the lib info refers to a global, we must identify it
            // with the same global which is now present in the externalSymbolTable().
            TapList<SymbolTable> allGlobalSymbolTables =
                new TapList<>(callGraph.globalRootSymbolTable(),
                    new TapList<>(callGraph.fortranRootSymbolTable(),
                        new TapList<>(callGraph.cRootSymbolTable(),
                            new TapList<>(callGraph.cudaRootSymbolTable(),
                                new TapList<>(callGraph.cPlusPlusRootSymbolTable(),
                                              null))))) ;
            int[] newIndices = new int[externalShape.length] ;
            int newIndex ;
            int remainParams = 0 ;
            ZoneInfo searchedZI ;
            ZoneInfo[] globalZIs ;
            for (int i=0 ; i<externalShape.length ; ++i) {
                newIndex = -1 ;
                searchedZI = externalShape[i] ;
                if (searchedZI.isGlobal() || searchedZI.isCommon()) {
                    // For every zone declared global by the "lib" declaration of this unit, find the
                    // corresponding global zoneinfo that is held by this unit's externalSymbolTable():
                    TapList<SymbolTable> inGlobalSymbolTables = allGlobalSymbolTables ;
                    while (inGlobalSymbolTables!=null && newIndex==-1) {
                        globalZIs =
                            inGlobalSymbolTables.head.getDeclaredZoneInfos(SymbolTableConstants.ALLKIND);
                        for (int j=globalZIs.length-1 ; j>=0 && newIndex==-1 ; --j) {
                            if (ZoneInfo.same(searchedZI, globalZIs[j])) {
                                newIndex = globalZIs[j].zoneNb ;
                            }
                        }
                        inGlobalSymbolTables = inGlobalSymbolTables.tail ;
                    }
                } else if (searchedZI.isParameter() || searchedZI.isResult()) {
                    ++remainParams ;
                }
                newIndices[i] = newIndex ;
            }
            // We consider that the global zones seen by an external or intrinsic from a lib file are the
            // global zones of each language used, but not the globals found in the given files, packages or modules:
            this.globalZonesNumber4 = callGraph.globalsForExternalsNumber4 ;
            int globalsNb = this.globalZonesNumber4[0] ;
            ZoneInfo[] futureExternalShape = new ZoneInfo[globalsNb+remainParams] ;
            ZoneInfo globalZI ;
            for (SymbolTable globalST : allGlobalSymbolTables) {
                importGlobalZIs(futureExternalShape, globalST) ;
            }
            ToObject<ModifiedTypeSpec> toModifiedType ;
            WrapperTypeSpec leafZoneType ;
            int lastZ = this.globalZonesNumber4[0] ;
            int lastRealZ = this.globalZonesNumber4[1] ;
            int lastIntZ  = this.globalZonesNumber4[2] ;
            int lastPtrZ  = this.globalZonesNumber4[3] ;
            int j = globalsNb ;
            ZoneInfo localZI ;
            for (int i=0 ; i<externalShape.length ; ++i) {
                // if this ZoneInfo has not been found as a global, it must
                // remain at the "same" place in the external shape,
                // only with new indices because globals have been moved.
                if (newIndices[i]==-1) {
                    localZI = externalShape[i] ;
                    localZI.zoneNb = lastZ ;
                    ++lastZ ;
                    toModifiedType = new ToObject<>(null);
                    leafZoneType = TypeSpec.peelSizeModifier(localZI.type, toModifiedType);
                    if (toModifiedType.obj() != null) {
                        localZI.typeSizeModifier = toModifiedType.obj().sizeModifierValue();
                    }
                    boolean isReal = false;
                    boolean isInt = false;
                    boolean isPtr = false;
                    if (leafZoneType==null || leafZoneType.wrappedType == null) {
                        isReal = true;
                        isInt = true;
                    } else if (TypeSpec.isA(leafZoneType, SymbolTableConstants.POINTERTYPE)) {
                        isPtr = true ;
                    } else if (TypeSpec.isA(leafZoneType, SymbolTableConstants.PRIMITIVETYPE)) {
                        String leafZoneTypeName = ((PrimitiveTypeSpec) leafZoneType.wrappedType).name();
                        if (leafZoneTypeName.equals("integer")) {
                            isReal = false;
                            isInt = true;
                        } else if (leafZoneTypeName.equals("float") || leafZoneTypeName.equals("complex")) {
                            isReal = true;
                            isInt = false;
                        }
                    } else if (TypeSpec.isA(leafZoneType, SymbolTableConstants.METATYPE)) {
                        String baseTypeName = ((MetaTypeSpec) leafZoneType.wrappedType).name;
                        if (baseTypeName.startsWith("float") || baseTypeName.startsWith("complex")) {
                            isReal = true;
                            isInt = false;
                        } else {
                            isReal = true;
                            isInt = true;
                        }
                    }
                    if (isReal) {
                        localZI.realZoneNb = lastRealZ;
                        lastRealZ++;
                    } else {
                        localZI.realZoneNb = -1 ;
                    }
                    if (isInt) {
                        localZI.intZoneNb = lastIntZ;
                        lastIntZ++;
                    } else {
                        localZI.intZoneNb = -1 ;
                    }
                    if (isPtr) {
                        localZI.ptrZoneNb = lastPtrZ;
                        lastPtrZ++;
                    } else {
                        localZI.ptrZoneNb = -1 ;
                    }
                    futureExternalShape[j] = localZI ;
                    ++j ;
                } else {
                    newIndex = newIndices[i] ;
                    // replace the global zoneInfo with a not-HIDDEN copy:
                    futureExternalShape[newIndex] = futureExternalShape[newIndex].copy() ;
                    futureExternalShape[newIndex].isHidden = false ;
                }
            }
            publicZonesNumber4 =
                new int[]{lastZ, lastRealZ, lastIntZ, lastPtrZ} ;
            externalShape = futureExternalShape ;
            // apply the offsets to the various bitsets of data-flow info:
            unitADDependencies = applyOffsets(unitADDependencies, newIndices, globalsNb, lastZ) ;
            unitInOutN = applyOffsets(unitInOutN.not(), newIndices, globalsNb, lastZ).not() ;
            unitInOutR = applyOffsets(unitInOutR, newIndices, globalsNb, lastZ) ;
            unitInOutW = applyOffsets(unitInOutW, newIndices, globalsNb, lastZ) ;
            unitInOutRW = applyOffsets(unitInOutRW, newIndices, globalsNb, lastZ) ;
        }

    }

    private void importGlobalZIs(ZoneInfo[] futureExternalShape, SymbolTable globalSymbolTable) {
        ZoneInfo[] globalZIs =
            globalSymbolTable.getDeclaredZoneInfos(SymbolTableConstants.ALLKIND);
        for (int j=globalZIs.length-1 ; j>=0 ; --j) {
            ZoneInfo globalZI = globalZIs[j] ;
            futureExternalShape[globalZI.zoneNb] = globalZI ;
        }
    }

    /** Example: given origVect=[abcd], newindices[2] goes to 1, globalsNb=3, length=3+4-1=6
     * builds [.c.abd] */
    private BoolVector applyOffsets(BoolVector origVect, int[] newIndices, int globalsNb, int length) {
        if (origVect==null) return null ;
        BoolVector newVect = new BoolVector(length) ;
        newVect.setFalse() ;
        int indexInNewLocals = globalsNb ;
        for (int i=0 ; i<newIndices.length ; ++i) {
            if (newIndices[i]<0) {
                if (origVect.get(i)) { //newVect is initialized with 0's anyway
                    newVect.set(indexInNewLocals, true) ;
                }
                ++indexInNewLocals ;
            } else {
                if (origVect.get(i)) { //newVect is initialized with 0's anyway
                    newVect.set(newIndices[i], true) ;
                }
            }
        }
        return newVect ;
    }

    private BoolMatrix applyOffsets(BoolMatrix origMat, int[] newIndices, int globalsNb, int length) {
        if (origMat==null) return null;
        BoolMatrix newMat = new BoolMatrix(length, length) ;
        BoolVector row ;
        newMat.setIdentity() ;
        int indexInNewLocals = globalsNb ;
        for (int i=0 ; i<newIndices.length ; ++i) {
            row = origMat.getRow(i) ;
            if (newIndices[i]<0) {
                if (row!=null) { //newMat is initialized with Id rows anyway
                    newMat.setRow(indexInNewLocals, applyOffsets(row, newIndices, globalsNb, length)) ;
                }
                ++indexInNewLocals ;
            } else {
                if (row!=null) { //newMat is initialized with Id rows anyway
                    newMat.setRow(newIndices[i], applyOffsets(row, newIndices, globalsNb, length)) ;
                }
            }
        }
        return newMat ;
    }

    /**
     * Computes the ranks of the elementary arguments, and creates and fills the
     * three arrays "argsZoneTrees", "argsPublicRankTrees" and "elemArgPublicRanks"
     * that store correspondence between arguments and elementary arguments.
     */
    private void computeElemArgInfo(int nbElemArgs) {
        elemArgPublicRanks = new int[nbElemArgs];
        ZoneInfo zoneInfo;
        TapList location;
        int elemArgRank = 0;
        TapList tree;
        Tree accessTree;
        int formalArgumentsNumber = formalArgumentsNb();
        int maxZoneInfoNb = -1;
        for (ZoneInfo info : externalShape) {
            zoneInfo = info;
            if (zoneInfo != null && zoneInfo.kind() == SymbolTableConstants.PARAMETER) {
                if (zoneInfo.index > formalArgumentsNumber) {
                    formalArgumentsNumber = zoneInfo.index;
                }
            }
            if (zoneInfo != null && zoneInfo.zoneNb > maxZoneInfoNb) {
                maxZoneInfoNb = zoneInfo.zoneNb;
            }
        }
        argsZoneTrees = new TapList[formalArgumentsNumber + 1];
        for (int i = argsZoneTrees.length - 1; i >= 0; i--) {
            argsZoneTrees[i] = new TapList<>(null, null);
        }
        zoneRkToElemArgRk = new int[maxZoneInfoNb + 1];
        for (int i = 0; i < externalShape.length; i++) {
            zoneInfo = externalShape[i];
            if (zoneInfo != null) {
                int paramNumber = -1;
                if (zoneInfo.kind() == SymbolTableConstants.PARAMETER) {
                    paramNumber = zoneInfo.index;
                } else if (zoneInfo.kind() == SymbolTableConstants.RESULT) {
                    paramNumber = 0;
                }
                if (paramNumber >= 0) {
                    accessTree = zoneInfo.accessTree;
                    if (ILUtils.isAccessibleDirectlyFromVariable(accessTree)) {
                        tree = argsZoneTrees[paramNumber];
                        location = TapList.getSetFieldLocation(tree, accessTree, true);
                        location.head = new TapIntList(elemArgRank, null);
                    }
                    zoneRkToElemArgRk[zoneInfo.zoneNb] = elemArgRank;
                    elemArgPublicRanks[elemArgRank] = i;
                    elemArgRank++;
                }
            }
        }
        argsPublicRankTrees = new TapList[formalArgumentsNumber + 1];
        for (int i = argsZoneTrees.length - 1; i >= 0; i--) {
            argsPublicRankTrees[i] = TapList.copyTree(argsZoneTrees[i]);
            TapList.replaceIntsWithValueInts(argsPublicRankTrees[i], elemArgPublicRanks, 0);
        }
    }

    /**
     * @return true when this Unit has global arguments that are
     * neither the returned value nor in the list of formal arguments,
     * and that may be used (R or W) by this Unit.
     */
    protected boolean hasGlobalArgs() {
        boolean globalFound = false;
        if (externalShape != null) {
            BoolVector unitInOutUse = (unitInOutN==null ? null : unitInOutR.or(unitInOutW).or(unitInOutRW)) ;
            ZoneInfo zi;
            for (int i=externalShape.length-1 ; !globalFound && i>0 ; --i) {
                zi = externalShape[i];
                if (zi!=null && (zi.isCommon() || zi.isGlobal()) && (unitInOutN==null || unitInOutUse.get(i))) {
                    globalFound = true;
                }
            }
        }
        return globalFound;
    }

    /**
     * Declares that "deadBlock" is dead code.
     * Disconnects deadBlock from the FlowGraph.
     */
    private void declareDeadBlock(Block deadBlock) {
        deadBlock.rank = DEADBLOCK;
        TapList<FGArrow> arrows = deadBlock.flow();
        while (arrows != null) {
            arrows.head.redirectDestination(null);
            arrows = arrows.tail;
        }
        arrows = deadBlock.backFlow();
        while (arrows != null) {
            arrows.head.redirectOrigin(null);
            arrows = arrows.tail;
        }
        TapList<Instruction> instructions = deadBlock.instructions;
        while (instructions != null) {
            addDeadTree(instructions.head.tree);
            instructions = instructions.tail;
        }
    }

    /**
     * Computes and sets a number of utility information,
     * most of them derived from the choice about loops
     * done during the "findOptimalLoops" step.
     * <p>
     * Precisely, sets the following:<br>
     * - for the FlowGraph: allBlocks, nbBlocks<br>
     * - for each Block: rank, enclosingLoop, flowGraph, (TODO: successors, predecessors)
     * boolean fields "isFGCycleHead" and "isFGCycleTail".<br>
     * - for each LoopBlock: enclosingLoops, depth, entryBlocks, exitBlocks,
     * entryArrows, exitArrows, and cycleArrows<br>
     * - for each Instruction: block<br>
     * - for each FGArrow: rank, iter, originInLevel, destinationInLevel<br>
     * This function requires that the following info is available:<br>
     * -- topBlocks, list of top-level blocks<br>
     * -- entryBlock and exitBlock<br>
     * -- for each LoopBlock, the list of its immediately enclosed Blocks
     * (field "inside").
     */
    protected void coherence() {
        TapList<Block> inAllBlocks;
        TapList<FGArrow> arrows;
        int curBlockRank;
        int curArrowRank;
        int curLoopRank;
        Block block;
        FGArrow arrow;
        /*Recompute the good, ordered, allBlocks list.
         * On the fly, create the LoopBlock's and
         * set the enclosingLoop and enclosingLoops fields.*/
        allBlocks = new TapList<>(null, null);
        allArrows = new TapList<>(null, null);
        getAllBlocks(topBlocks, allBlocks, null, null);
        allBlocks = allBlocks.tail;
        /*then for each Block in allBlocks, reset flow, BackFlow, instructions,*/
        entryBlock.rank = -1;
        entryBlock.coherence();
        inAllBlocks = allBlocks;
        curBlockRank = 0;
        curLoopRank = 0;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            block.rank = curBlockRank++;
            block.symbolicRk = null;
            block.isFGCycleTail = false;
            block.isFGCycleHead = false;
            block.coherence();
            if (block instanceof HeaderBlock) {
                LoopBlock enclosing = block.enclosingLoop();
                if (enclosing != null) {
                    enclosing.rank = curLoopRank++;
                }
            }
            inAllBlocks = inAllBlocks.tail;
        }
        exitBlock.rank = curBlockRank;
        exitBlock.coherence();
        nbBlocks = curBlockRank;
        nbLoopBlocks = curLoopRank;
        curArrowRank = 0;
        inAllBlocks = new TapList<>(entryBlock, allBlocks);
        while (inAllBlocks != null) {
            arrows = inAllBlocks.head.flow();
            while (arrows != null) {
                arrow = arrows.head;
                arrow.coherence();
                arrow.setRank(curArrowRank);
                allArrows.newR(arrow);
                curArrowRank++;
                if (arrow.origin.rank >= arrow.destination.rank) {
                    arrow.origin.isFGCycleTail = true;
                    arrow.destination.isFGCycleHead = true;
                }
                arrows = arrows.tail;
            }
            inAllBlocks = inAllBlocks.tail;
        }
        allArrows = allArrows.tail;
        nbArrows = curArrowRank;
    }

    /**
     * Fills the tail of "toBlocks" list with the
     * flattened set of all BasicBlock's and HeaderBlock's
     * recursively contained in the "fromBlocks" list.
     *
     * @return the new tail of "toBlocks".
     * On the fly, sets the enclosingLoops of LoopBlock's, and the
     * enclosingLoop of all Block's.
     * Also, for each LoopBlock, clean all fields entryBlocks, exitBlocks,
     * entryArrows, exitArrows, and cycleArrows.
     */
    private TapList<Block> getAllBlocks(TapList<Block> fromBlocks, TapList<Block> toBlocks,
                                        LoopBlock enclosing, TapList<LoopBlock> enclosings) {
        while (fromBlocks != null) {
            fromBlocks.head.setEnclosingLoop(enclosing);
            if (fromBlocks.head instanceof LoopBlock) {
                LoopBlock loopBlock = (LoopBlock) fromBlocks.head;
                loopBlock.entryBlocks = null;
                loopBlock.exitBlocks = null;
                loopBlock.entryArrows = null;
                loopBlock.exitArrows = null;
                loopBlock.cycleArrows = null;
                loopBlock.setEnclosingLoopBlocks(enclosings);
                toBlocks =
                        getAllBlocks(
                                loopBlock.inside, toBlocks, loopBlock,
                                new TapList<>(loopBlock, enclosings));
            } else {
                toBlocks = toBlocks.placdl(fromBlocks.head);
            }
            fromBlocks = fromBlocks.tail;
        }
        return toBlocks;
    }

    /**
     * Translate the published zones from the given TapIntList "publicZones",
     * which are expressed with respect to "this" Unit, into the corresponding
     * published zones expressed with respect to Unit "origUnit".
     * If one such zone is simply unknown in origUnit, then discard it.
     */
    public TapIntList translateToCallerZones(TapIntList publicZones, Unit origUnit) {
        TapIntList callerZones = null;
        while (publicZones != null) {
            ZoneInfo zi = this.externalShape[publicZones.head] ;
            if (zi!=null
                && zi.kind()==SymbolTableConstants.GLOBAL
                && zi.zoneNb < origUnit.publicZonesNumber4[SymbolTableConstants.ALLKIND]) {
                callerZones = TapIntList.addIntoSorted(callerZones, zi.zoneNb) ;
            }
            publicZones = publicZones.tail;
        }
        return callerZones;
    }

    /** Computes the list of zones of the result of this (function) Unit.
     * @return the zones of the result of this (function) Unit, null if no result.
     */
    public TapIntList zonesOfResult() {
        TapIntList result = null;
        for (int i = externalShape.length - 1; i >= 0; --i) {
            if (externalShape[i] != null
                    && externalShape[i].kind() == SymbolTableConstants.RESULT) {
                result = new TapIntList(i, result);
            }
        }
        return result;
    }

    /**
     * Initializes "zonesUsedInDeclarations", the vector of all the zones of all the variables that are
     * used in the declarations part, e.g. as dimension declarator.
     */
    protected void computeZonesUsedInDeclarations() {
        zonesUsedInDeclarations = new BoolVector(bodySymbolTable().declaredZonesNb(SymbolTableConstants.ALLKIND));
        TapList<Block> bl = allBlocks;
        //instr0 is an "exemplary" instruction, at the beginning of the Unit:
        Instruction instr0 = null;
        while (instr0 == null && bl != null) {
            if (bl.head.instructions != null) {
                instr0 = bl.head.instructions.head;
            }
            bl = bl.tail;
        }
        collectZonesUsedInDeclarations(publicSymbolTable().getAllTopSymbolDecls(), instr0);
        collectZonesUsedInDeclarations(bodySymbolTable().getAllTopSymbolDecls(), instr0);
    }

    /**
     * Utility for the above zonesUsedInDeclarations().
     */
    private void collectZonesUsedInDeclarations(TapList<SymbolDecl> listOfSymbolDecls,
                                                Instruction instr0) {
        SymbolDecl symbolDecl;
        TapList<Tree> toUsedTrees = new TapList<>(null, null);
        TapList<TypeSpec> toDejaVu = new TapList<>(null, null);
        BoolVector zonesUsedByDecl;
        while (listOfSymbolDecls != null) {
            symbolDecl = listOfSymbolDecls.head;
            symbolDecl.collectUsedTrees(toUsedTrees, toDejaVu);
            while (toUsedTrees.tail != null) {
                zonesUsedByDecl = privateSymbolTable.zonesUsedByExp(toUsedTrees.tail.head, instr0);
                zonesUsedInDeclarations.cumulOr(zonesUsedByDecl);
                toUsedTrees.tail = toUsedTrees.tail.tail;
            }
            listOfSymbolDecls = listOfSymbolDecls.tail;
        }
    }

    protected TapList<Unit> addInterfaceDecl(
            TreeProtocol inputStream, CallGraph callGraph,
            SymbolTable symbolTable) throws IOException {
        Tree currentTree;
        Tree preCommentsAnnot = null;
        Tree postCommentsAnnot = null;
        TapList<Unit> result = null;
        inputStream.readOperator();
        while (inputStream.seeOperator().code == ILLang.op_annotation) {
            String annotationName;
            Tree annotationTree;
            inputStream.readOperator();
            annotationName = inputStream.readString();
            annotationTree = inputStream.readTree();
            if (annotationName.equals("preComments")) {
                preCommentsAnnot = annotationTree;
            } else if (annotationName.equals("postComments")) {
                postCommentsAnnot = annotationTree;
            }
        }
        boolean isAbstract = false ;
        currentTree = inputStream.readTree();
        if (currentTree.opCode()==ILLang.op_abstract) {
            isAbstract = true ;
            currentTree = null ;
        }
        inputStream.readOperator();
        SymbolTable enclosingPrivateSymbolTable;
        if (symbolTable.unit != null) {
            if (symbolTable.unit.isModule()) {
                enclosingPrivateSymbolTable =
                        symbolTable.unit.publicSymbolTable();
            } else {
                enclosingPrivateSymbolTable =
                        symbolTable.unit.privateSymbolTable();
            }
        } else {
            enclosingPrivateSymbolTable = callGraph.languageRootSymbolTable(language);
        }
        // enclosingPrivateSymbolTable now is the closest enclosing private
        // SymbolTable, in which the present InterfaceDecl will be added.
        // When no enclosing Unit, it is the current language's root SymbolTable.
        InterfaceDecl interfaceDecl = null;
        if (!ILUtils.isNullOrNone(currentTree)) {
            interfaceDecl =
                    symbolTable.addInterfaceDecl(
                            currentTree, this);
            interfaceDecl.setInterfaceInstr(true);
        }
        while (inputStream.seeOperator().code != ILLang.op_endOfList) {
            switch (inputStream.seeOperator().code) {
                case ILLang.op_interface:
                    inputStream.readOperator();
                    if (inputStream.seeOperator().code == ILLang.op_function) {
                        Unit unit = callGraph.createNewUnit(this, language);
                        unit.enclosingUnitOfInterface = this;
                        unit.setInterface();
                        if (isAbstract) unit.isAbstractFunctionType = true ;
                        SymbolDecl declAsFormalParam = null;
                        FlowGraphBuilder.build(inputStream, symbolTable, callGraph, language, unit, false);
                        unit.preComments = preCommentsAnnot;
                        unit.postComments = postCommentsAnnot;
                        unit.exportFunctionTypeSpec();
                        Tree interfaceHead = unit.entryBlock.headTree() ;
                        Tree interfaceName = ILUtils.getCalledName(interfaceHead) ;
                        unit.parametersOrModuleNameTree = ILUtils.getArguments(interfaceHead);
                        if (this.isStandard()) {
                            declAsFormalParam =
                                    this.publicSymbolTable().getTopSymbolDecl(unit.name);
                        }
                        if (isAbstract) {
                            // Declare this "interface" as a type-name declaration standing for a function type:
                            TypeDecl typeDecl = new TypeDecl(interfaceName, new WrapperTypeSpec(unit.functionTypeSpec())) ;
                            callGraph.deleteUnit(unit) ;
                            typeDecl.setInterfaceInstr(true);
                            symbolTable.unit.privateSymbolTable().addSymbolDecl(typeDecl);
                        } else {
                            FunctionDecl functionDecl;
                            TypeDecl ambiguousDecl = symbolTable.unit.privateSymbolTable().getTypeDecl(unit.name);
                            if (ambiguousDecl!=null) {
                                WrapperTypeSpec ambiguousType = ambiguousDecl.typeSpec ;
                                if (ambiguousType!=null && ambiguousType.wrappedType==null) {
                                    ambiguousType.wrappedType = unit.functionTypeSpec();
                                }
                                symbolTable.unit.privateSymbolTable().removeDecl(unit.name, SymbolTableConstants.TYPE, false) ;
                            }
                            if (declAsFormalParam != null && declAsFormalParam.isA(SymbolTableConstants.VARIABLE)) {
                                callGraph.languageRootSymbolTable(language).removeDecl(unit.name, SymbolTableConstants.FUNCTION, true);
                                unit.turnVarFunction(this);
                                functionDecl = new FunctionDecl(interfaceName, unit);
                                functionDecl.setInterfaceInstr(true);
                                SymbolDecl oldVarDecl = enclosingPrivateSymbolTable.basisSymbolTable().
                                    getDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                                TapList<SymbolDecl> dependsOn;
                                if (oldVarDecl != null) {
                                    dependsOn = oldVarDecl.dependsOn();
                                    enclosingPrivateSymbolTable.basisSymbolTable().
                                        removeDecl(functionDecl.symbol, SymbolTableConstants.VARIABLE, true);
                                    functionDecl.dependsOn = dependsOn;
                                }
                                enclosingPrivateSymbolTable.basisSymbolTable().addSymbolDecl(functionDecl);
                            } else {
                                TapList<FunctionDecl> functionDecls =
                                    symbolTable.getFunctionDecl(unit.name, unit.functionTypeSpec().returnType,
                                                            unit.functionTypeSpec().argumentsTypes, true);
                                if (functionDecls == null) { // [llh 12Sept2017] Recovery when inconsistent types in func and interface.
                                    functionDecls = symbolTable.getFunctionDecl(unit.name, unit.functionTypeSpec().returnType, null, true);
                                }
                                functionDecl = functionDecls.head;// we assume that only one function is called "unit.name"
                            }
                            functionDecl.setInterfaceInstr(true);
                            if (interfaceDecl != null) {
                                interfaceDecl.addInInterfaceDecl(functionDecl, this);
                                interfaceDecl.setInterfaceUnits(
                                    TapList.append(interfaceDecl.interfaceUnits(),
                                            new TapList<>(unit, null)));
                            }
                            // Ne pas le faire si on choisit de VIRER les interfaceUnit resolues !! : (cf f90:v37)
                            // <vmp> 2013.05.07: on vire les interfaceUnit resolues
                            //CallGraph.addCallArrow(this, CONTAINS, unit) ;
                            //CallGraph.addCallArrow(this, CALLS, unit) ;
                            // la unit importe les imports de l'interface, sinon pb d'ordre dans le callGraph
                            TapList<Instruction> importedModulesInstrs = unit.importedModulesUseDecl;
                            String usedModuleName;
                            Unit usedModule;
                            while (importedModulesInstrs != null) {
                                if (importedModulesInstrs.head.tree.opCode() == ILLang.op_useDecl) {
                                    usedModuleName = importedModulesInstrs.head.tree.down(1).stringValue();
                                    usedModule = symbolTable.getModule(usedModuleName);
                                    if (usedModule != null) {
                                        CallGraph.addCallArrow(this, SymbolTableConstants.IMPORTS, usedModule);
                                    }
                                }
                                importedModulesInstrs = importedModulesInstrs.tail;
                            }
                            result = new TapList<>(unit, result);
                        }
                    }
                    break;
                case ILLang.op_moduleProcedure:
                    currentTree = inputStream.readTree();
                    interfaceDecl.addInInterfaceDecl(currentTree);
                    break;
                case ILLang.op_annotation:
                    String annotationName;
                    Tree annotationTree;
                    inputStream.readOperator();
                    annotationName = inputStream.readString();
                    annotationTree = inputStream.readTree();
                    if (annotationName.equals("preComments")) {
                        preCommentsAnnot = annotationTree;
                    } else if (annotationName.equals("postComments")) {
                        postCommentsAnnot = annotationTree;
                    }
                    break;
                default:
                    TapEnv.toolWarning(TapEnv.MSG_ALWAYS, "(Reading interface declaration) Unexpected tree: " + inputStream.readTree());
                    break;
            }
        }
        inputStream.readOperator();
        inputStream.readTree();
        return result;
    }

    protected void declareAllSymbolDecls() {
        TapList<SymbolTable> inSymbolTables = TapList.reverse(symbolTablesBottomUp);
        // vmp ordre TopDown (cf F90 v106)
        while (inSymbolTables != null) {
            inSymbolTables.head.declareAllSymbolDecls();
            inSymbolTables = inSymbolTables.tail;
        }
    }

    protected void solveTypeExtends() {
        TapList<SymbolTable> inSymbolTables = TapList.reverse(symbolTablesBottomUp);
        while (inSymbolTables != null) {
            inSymbolTables.head.solveTypeExtends() ;
            inSymbolTables = inSymbolTables.tail;
        }
    }

    /**
     * @return null if no declaration is found.
     */
    public SymbolDecl getImportedDecl(String name, int kind) {
        Unit currentUnit = this;
        SymbolTable importedSymbolTable;
        SymbolDecl result = null;
        while (result == null && currentUnit != null) {
            importedSymbolTable =
                    currentUnit.publicSymbolTable().basisSymbolTable();
            if (importedSymbolTable != null && importedSymbolTable.isImports) {
                result = importedSymbolTable.getTopDecl(name, kind);
            }
            currentUnit = currentUnit.upperLevelUnit;
        }
        return result;
    }

    /**
     * @return true si intrinsicDecl est declare'e dans un des modules importe'
     * pb: on ne peut pas passer par les importedSymbolTable car les intrinsic
     * n'y sont pas: elles sont rajoutees au typeCheck des declarations du module
     * qui est fait apres la copie des symbolTable importees
     * TODO dans ce qui suit on ne tient pas compte des restrictions faites
     * par les only ou renamed
     * il faudrait affiner ...
     */
    public boolean isUseAssociatedIntrinsic(FunctionDecl intrinsicDecl) {
        boolean result = false;
        TapList<Unit> importedMods = importedModules;
        while (!result && importedMods != null) {
            result = importedMods.head.publicSymbolTable().getTopFunctionDecl(intrinsicDecl.symbol, null, null, false)
                    != null;
            importedMods = importedMods.tail;
        }
        importedMods = otherImportedModules;
        while (!result && importedMods != null) {
            result = importedMods.head.publicSymbolTable().getTopFunctionDecl(intrinsicDecl.symbol, null, null, false)
                    != null;
            importedMods = importedMods.tail;
        }
        return result;
    }

    private SymbolDecl[] getParameterDecls() {
        SymbolDecl[] result = null;
        Tree[] callArgsTrees;
        if (entryBlock != null && entryBlock.headTree() != null) {
            callArgsTrees = ILUtils.getArguments(entryBlock.headTree()).children();
            result = new SymbolDecl[callArgsTrees.length];
            SymbolTable argsSymbolTable = publicSymbolTable();
            for (int i = 0; i < callArgsTrees.length; i++) {
                Tree argTree = callArgsTrees[i];
                if (argTree.opCode() != ILLang.op_star) {
                    result[i] =
                            argsSymbolTable.getVariableDecl(ILUtils.baseName(argTree));
                    if (result[i] == null) {
                        TapList<FunctionDecl> tmp = argsSymbolTable.getFunctionDecl(ILUtils.baseName(argTree), null, null, false);
                        result[i] = tmp == null ? null : tmp.head;
                        // we assume there's only 1 function/method with this signature
                    }
                    if (result[i] == null) {
                        TapList<InterfaceDecl> interfaceDecls =
                                argsSymbolTable.getAllInterfaceDecls(ILUtils.baseName(argTree));
                        if (interfaceDecls != null) {
                            result[i] = interfaceDecls.head;
                        }
                    }
                } else {
                    result[i] = null;
                }
            }
        }
        return result;
    }

    protected int getOptionalParameterDeclsNumber() {
        int result = 0;
        Tree[] callArgsTrees;
        if (entryBlock != null) {
            callArgsTrees = ILUtils.getArguments(entryBlock.headTree()).children();
            SymbolTable argsSymbolTable = publicSymbolTable();
            VariableDecl params;
            for (Tree argTree : callArgsTrees) {
                if (argTree.opCode() != ILLang.op_star) {
                    params =
                            argsSymbolTable.getVariableDecl(ILUtils.baseName(argTree));
                    if (params == null || params.isOptional()) {
                        result = result + 1;
                    }
                }
            }
        }
        return result;
    }

    /**
     * @return null if no suitable middle block found.
     */
    public Block middleNormalBlock() {
        Block middleBlock = null;
        if (allBlocks != null) {
            int ll = TapList.length(allBlocks);
            int n1 = ll / 2;
            int n2 = n1 + 1;
            while (middleBlock == null && n1 >= 0 && n2 < ll) {
                middleBlock = (Block) TapList.nth(allBlocks, n1);
                if (middleBlock.isALoop()) {
                    middleBlock = null;
                }
                if (middleBlock == null) {
                    middleBlock = (Block) TapList.nth(allBlocks, n2);
                    if (middleBlock.isALoop()) {
                        middleBlock = null;
                    }
                }
                n1 = n1 - 1;
                n2 = n2 + 1;
            }
        }
        return middleBlock;
    }

    /**
     * Creates a PARTIAL copy of "this" Unit.
     * Used by differentiation to create copied, non-differentiated Units.
     * Copies the Flow Graph and Flow-Graph-related info..
     * For every allocation of an active variable, insert allocation
     * of the differentiated variable.
     * Does NOT create the copied SymbolTable's.
     */
    public Unit copyUnitExceptSymbolTables(CallGraph callGraph,
                                           Unit upperLevelUnit, Unit otherEnclosingUnitOfInterface) {
        Unit copyUnit = callGraph.createNewUnit(upperLevelUnit, language);
        copyUnit.name = this.name;
        copyUnit.kind = this.kind;
        copyUnit.language = language;
        copyUnit.position = this.position;
        copyUnit.allArrows = copyUnitBody(this, copyUnit, null);
        copyUnit.topBlocks = null;
        copyUnit.nbBlocks = this.nbBlocks;
        copyUnit.nbArrows = this.nbArrows;
        copyUnit.nbLoopBlocks = this.nbLoopBlocks;
        copyUnit.functionTypeSpec = this.functionTypeSpec;
        copyUnit.formats = this.formats;
        copyUnit.preComments = this.preComments;
        copyUnit.postComments = this.postComments;
        copyUnit.preCommentsBlock = this.preCommentsBlock;
        copyUnit.postCommentsBlock = this.postCommentsBlock;
        copyUnit.lostComments = this.lostComments;
        copyUnit.modifiers = this.modifiers;
//         copyUnit.declaredMemoryMap = this.declaredMemoryMap;
        copyUnit.otherReturnVar = this.otherReturnVar;
        copyUnit.hasArrayNotation = this.hasArrayNotation;
        copyUnit.enclosingUnitOfInterface = otherEnclosingUnitOfInterface;
        copyUnit.containsAllocDealloc = containsAllocDealloc;
        if (userHelp.tail != null) {
            copyUnit.userHelp = new TapList<>(null, userHelp.tail);
        }
        return copyUnit;
    }

    /**
     * @return an unordered list of all Units recursively contained in this Unit.
     */
    public TapList<Unit> collectContainedUnits(TapList<Unit> collector) {
        TapList<Unit> contained = lowerLevelUnits;
        while (contained != null) {
            collector = contained.head.collectContainedUnits(new TapList<>(contained.head, collector));
            contained = contained.tail;
        }
        return collector;
    }

    /**
     * @param regionBlock Block that ends with the parallel region pragma
     */
    public TapTriplet<TapList<FGArrow>, TapList<Block>, TapList<FGArrow>>
    getFrontierOfParallelRegion(Block regionBlock) {
        Instruction parallelControl = regionBlock.lastInstr();
        TapList<Block> parallelRegionBody = null;
        TapList<FGArrow> exitArrows = null;
        for (TapList<Block> inAllBlocks = allBlocks; inAllBlocks != null; inAllBlocks = inAllBlocks.tail) {
            Block block = inAllBlocks.head;
            if (TapList.contains(block.parallelControls, parallelControl)) {
                parallelRegionBody = new TapList<>(block, parallelRegionBody);
                for (TapList<FGArrow> inExitArrows = block.flow(); inExitArrows != null; inExitArrows = inExitArrows.tail) {
                    if (!TapList.contains(inExitArrows.head.destination.parallelControls, parallelControl)) {
                        exitArrows = new TapList<>(inExitArrows.head, exitArrows);
                    }
                }
            }
        }
        return new TapTriplet<>(regionBlock.flow(), TapList.nreverse(parallelRegionBody), exitArrows);
    }

    /**
     * Prints in detail the contents of this Unit, onto TapEnv.curOutputStream().
     *
     * @param indent the amount of indentation to be used for this printing.
     */
    public void dump(int indent) throws IOException {
        TapEnv.indentPrint(indent, "Call Node number " + rank() + ": " + this + " contained in " + upperLevelUnit());
        TapEnv.println();
        TapEnv.pushRelatedUnit(this);
        if (fromInclude != null) {
            TapEnv.indentPrint(indent + 6, "From Include:" + fromInclude);
            TapEnv.println();
        }
        TapEnv.indentPrint(indent + 6, "LowerLevelUnits " + lowerLevelUnits);
        TapEnv.println();
        TapEnv.indentPrint(indent + 6, "Error messages");
        TapEnv.println();
        TapList<PositionAndMessage> msg = messages.tail;
        while (msg != null) {
            TapEnv.indentPrint(indent + 8, msg.head.toString());
            TapEnv.println();
            msg = msg.tail;
        }
        TapEnv.indentPrint(indent + 6, "SymbolTables:" + bodySymbolTable() + " till external:" + externalSymbolTable());
        TapEnv.println();
        TapEnv.indentPrint(indent + 6, "tuST:" + translationUnitSymbolTable);
        TapEnv.println();
        CallArrow arrow;
        TapList<CallArrow> callArrows = callers();
        TapEnv.indentPrint(indent + 6, "CALL GRAPH PREDECESSORS: ");
        while (callArrows != null) {
            arrow = callArrows.head;
            if (arrow.origin != null) {
                arrow.origin.cite();
            }
            if (arrow.isCall()) {
                TapEnv.print(" calls");
            }
            if (arrow.isImport()) {
                TapEnv.print(" imports");
            }
            if (arrow.isContain()) {
                TapEnv.print(" contains");
            }
            TapEnv.print(" it, ");
            callArrows = callArrows.tail;
        }
        TapEnv.println();
        callArrows = callees();
        TapEnv.indentPrint(indent + 6, "IMPORTS AND CONTAINS:");
        if (callArrows==null) TapEnv.print(" none") ;
        while (callArrows != null) {
            arrow = callArrows.head;
            if (arrow.isImport() || arrow.isContain()) {
                TapEnv.print(arrow.isImport() ? " imports " : " contains ");
                if (arrow.destination != null) {
                    arrow.destination.cite();
                }
                TapEnv.print(",");
            }
            callArrows = callArrows.tail;
        }
        TapEnv.println();
        callArrows = callees();
        while (callArrows != null) {
            arrow = callArrows.head;
            if (arrow.isCall()) {
                TapEnv.indentPrint(indent + 6, "CALLS: " + arrow.destination) ;
            }
            callArrows = callArrows.tail;
        }
        TapEnv.indentPrint(indent + 6, "unit type:" + functionTypeSpec());
        TapEnv.println();
        TapEnv.indentPrint(indent + 6, "unit external shape: " + (externalShape == null ? "null external shape" : "see x" + this.rank()));
        TapEnv.indentPrint(indent + 6, "unit relevant zones: "+relevantZones) ;
        if (globalZonesNumber4!=null && publicZonesNumber4!=null) {
            TapEnv.indentPrintln(indent + 6, "unit zones global->public: a:"+globalZonesNumber4[0]+"->"+publicZonesNumber4[0]+" r:"+globalZonesNumber4[1]+"->"+publicZonesNumber4[1]+" i:"+globalZonesNumber4[2]+"->"+publicZonesNumber4[2]+" p:"+globalZonesNumber4[3]+"->"+publicZonesNumber4[3]) ;
        }
        TapEnv.println();
        if (argsZoneTrees != null) {
            TapEnv.indentPrint(indent + 6, "unit args zone-trees:");
            TapEnv.println();
            for (int i = 0; i < argsZoneTrees.length; ++i) {
                TapEnv.indentPrint(indent + 8, "arg#" + i + " zones tree:" + argsZoneTrees[i]);
                TapEnv.println();
            }
            TapEnv.indentPrint(indent + 6, "elem arg ranks ");
            for (int i = 0; i < elemArgPublicRanks.length; ++i) {
                TapEnv.print(i + ":" + elemArgPublicRanks[i] + " ");
            }
            TapEnv.println();
        }
        TapEnv.indentPrint(indent + 6, "Local Memory Map of unit:");
        if (localMaps == null) {
            TapEnv.print(" null");
            TapEnv.println();
        } else if (localMaps.maps.tail == null) {
            TapEnv.print(" EMPTY");
            TapEnv.println();
        } else {
            TapEnv.println();
            for (MemMap oneMap : localMaps.maps.tail) {
                TapEnv.indentPrintln(indent+10, oneMap.toString()) ;
            }
        }
        TapEnv.indentPrint(indent + 2, "FLOW GRAPH:");
        TapEnv.println();
        if (entryBlock == null) {
            TapEnv.indentPrint(indent + 4, "null FlowGraph!");
            TapEnv.println();
        } else {
            TapEnv.indentPrint(indent + 4, "Blocks nesting: ");
            dumpBlocksNesting(topBlocks());
            TapEnv.println();
            entryBlock.dump(indent + 4);
            TapEnv.println();
            TapList<Block> inAllBlocks = allBlocks;
            while (inAllBlocks != null) {
                inAllBlocks.head.dump(indent + 6);
                TapEnv.println();
                inAllBlocks = inAllBlocks.tail;
            }
            exitBlock.dump(indent + 4);
            TapEnv.println();
            TapEnv.indentPrint(indent + 4, "Non-flow instructions:");
            TapList<Instruction> inNonFlowInstructions = nonFlowInstructions;
            while (inNonFlowInstructions != null) {
                inNonFlowInstructions.head.dump();
                TapEnv.println();
                inNonFlowInstructions = inNonFlowInstructions.tail;
            }
            TapEnv.println();
            TapEnv.indentPrintln(indent + 4, "Dead code:");
            TapList<Tree> inDeadTrees = deadTrees();
            while (inDeadTrees != null) {
                ILUtils.dump(inDeadTrees.head, indent + 6);
                TapEnv.println();
                inDeadTrees = inDeadTrees.tail;
            }
        }
        TapList<Tree> inFormats = formats;
        if (inFormats != null) {
            TapEnv.indentPrint(indent + 4, "Format declarations:");
            TapEnv.println();
            while (inFormats != null) {
                ILUtils.dump(inFormats.head, indent + 5);
                TapEnv.println();
                inFormats = inFormats.tail;
            }
        }
        TapList<Tree> inLostComments = lostComments;
        if (inLostComments != null) {
            TapEnv.indentPrint(indent + 4, "Lost comments:");
            TapEnv.println();
            while (inLostComments != null) {
                ILUtils.dump(inLostComments.head, indent + 5);
                TapEnv.println();
                inLostComments = inLostComments.tail;
            }
        }
        TapList<Directive> dirs = directives ;
        if (dirs!=null) {
            TapEnv.indentPrintln(indent + 4, "Unit directives:");
            while (dirs!=null) {
                TapEnv.indentPrintln(indent + 5, dirs.head.toString()) ;
                dirs = dirs.tail ;
            }
        }
        Tree comments ;
        comments = preComments;
        if (comments != null) {
            TapEnv.indentPrintln(indent + 4, "Unit preComments:");
            ILUtils.dump(comments, indent + 5);
            TapEnv.println();
        }
        comments = preCommentsBlock;
        if (comments != null) {
            TapEnv.indentPrintln(indent + 4, "Unit preCommentsBlock:");
            ILUtils.dump(comments, indent + 5);
            TapEnv.println();
        }
        comments = postComments;
        if (comments != null) {
            TapEnv.indentPrintln(indent + 4, "Unit postComments:");
            ILUtils.dump(comments, indent + 5);
            TapEnv.println();
        }
        comments = postCommentsBlock;
        if (comments != null) {
            TapEnv.indentPrintln(indent + 4, "Unit postCommentsBlock:");
            ILUtils.dump(comments, indent + 5);
            TapEnv.println();
        }
        TapEnv.popRelatedUnit();
    }

    public void dumpExternalZones() throws IOException {
        ZoneInfo zoneInfo;
        TapEnv.indentPrint(2, "External zones of == public zones of == external shape of unit " + this + ":");
        TapEnv.println();
        if (externalShape != null && externalShape.length != 0) {
            for (int i = 0; i < externalShape.length; ++i) {
                TapEnv.indentPrintln(6, "x" + this.rank() + ":a" + i + " : "+externalShape[i]);
            }
        } else {
            TapEnv.indentPrint(6, "empty");
            TapEnv.println();
        }
    }

    private void dumpControlZones(int indent) throws IOException {
        if (testZonesNb == 0) {
            TapEnv.print(" none.");
            TapEnv.println();
        } else {
            String[] controllers = new String[testZonesNb];
            for (int i = controllers.length - 1; i >= 0; --i) {
                controllers[i] = "";
            }
            TapList<Block> blocks = allBlocks();
            Block block;
            while (blocks != null) {
                block = blocks.head;
                if (block.testZone >= 0) {
                    controllers[block.testZone] =
                            controllers[block.testZone] + " or " + block;
                }
                blocks = blocks.tail;
            }
            TapEnv.println();
            for (int i = 0; i < controllers.length; ++i) {
                TapEnv.indentPrintln(indent, TapEnv.str3(i) + "> " + controllers[i]);
            }
        }
    }

    public String getNameFromBindC() {
        String result = null;
        if (this.modifiers != null) {
            Tree accessDecl = this.modifiers;
            if (accessDecl.opCode() == ILLang.op_accessDecl) {
                if (accessDecl.down(2).length() != 1) {
                    result = accessDecl.down(2).down(2).down(2).stringValue();
                } else {
                    result = name;
                }
            }
        }
        return result;
    }

    public void printSizeStatistics() {
        Block block;
        int nbBlocks = TapList.length(allBlocks) ;
        int nbLoops = 0 ;
        int nbInstrs = 0 ;
        int minNbZ  = 0 ;
        int minNbPZ  = 0 ;
        int minNbRZ  = 0 ;
        SymbolTable pubST = publicSymbolTable() ;
        if (pubST!=null) {
            minNbZ = pubST.freeDeclaredZone(SymbolTableConstants.ALLKIND) ;
            minNbPZ = pubST.freeDeclaredZone(SymbolTableConstants.PTRKIND) ;
            minNbRZ = pubST.freeDeclaredZone(SymbolTableConstants.REALKIND) ;
        }
        int maxNbZ  = minNbZ ;
        int maxNbPZ = minNbPZ ;
        int maxNbRZ = minNbRZ ;
        TapList<Block> inAllBlocks = allBlocks ;
        while (inAllBlocks != null) {
            block = inAllBlocks.head;
            if (block instanceof HeaderBlock) ++nbLoops ;
            if (block.symbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND)>maxNbZ) {
                maxNbZ = block.symbolTable.freeDeclaredZone(SymbolTableConstants.ALLKIND) ;
            }
            if (block.symbolTable.freeDeclaredZone(SymbolTableConstants.PTRKIND)>maxNbPZ) {
                maxNbPZ = block.symbolTable.freeDeclaredZone(SymbolTableConstants.PTRKIND) ;
            }
            if (block.symbolTable.freeDeclaredZone(SymbolTableConstants.REALKIND)>maxNbRZ) {
                maxNbRZ = block.symbolTable.freeDeclaredZone(SymbolTableConstants.REALKIND) ;
            }
            TapList<Instruction> inInstructions = block.instructions ;
            while (inInstructions!=null) {
                if (!inInstructions.head.isADeclaration()) ++nbInstrs ;
                inInstructions = inInstructions.tail ;
            }
            inAllBlocks = inAllBlocks.tail ;
        }
        System.out.println("SIZES OF "+name()+": "+nbBlocks+" blocks, "+nbInstrs+" instructions, "+nbLoops+" loops, zones:["+minNbZ+","+maxNbZ+"] pointers:["+minNbPZ+","+maxNbPZ+"] floats:["+minNbRZ+","+maxNbRZ+"]") ;
    }

    /**
     * Prints a short reference to this Unit onto TapEnv.curOutputStream().
     */
    public void cite() throws IOException {
        TapEnv.print(name);
        if (language == TapEnv.FORTRAN2003) {
            String nameFromBindC = getNameFromBindC();
            if (nameFromBindC != null) {
                TapEnv.print("(<=" + getNameFromBindC() + ")");
            }
        }
        TapEnv.print("(" + TapEnv.languageName(language) + ")");
    }

    @Override
    public String toString() {
        String kindString;
        switch (kind) {
            case STANDARD:
                kindString = "Unit";
                break;
            case INTRINSIC:
                kindString = "IntrinsicUnit";
                break;
            case EXTERNAL:
                kindString = "ExternalUnit";
                break;
            case VARFUNCTION:
                kindString = "VarFunctionUnit";
                break;
            case UNDEFINED:
                kindString = "UndefModuleOrClassUnit";
                break;
            case MODULE:
                kindString = "ModuleUnit";
                break;
            case INTERFACE:
                kindString = "InterfaceUnit";
                break;
            case OUTSIDE_TBD:
                kindString = "OutsideUnit_TBD";
                break;
            case CONSTRUCTOR:
                kindString = "ConstructorUnit";
                break;
            case DESTRUCTOR:
                kindString = "DestructorUnit";
                break;
            case CLASS:
                kindString = "ClassUnit";
                break;
            case FILE:
                kindString = "TranslationUnit";
                break;
            default:
                kindString = "?Unit" + kind + "?";
        }
        String nameFromBindC = "";
        if (language == TapEnv.FORTRAN2003) {
            nameFromBindC = getNameFromBindC();
            if (nameFromBindC != null) {
                nameFromBindC = "(<=" + nameFromBindC + ')';
            } else {
                nameFromBindC = "";
            }
        }
        return kindString //+"@"+Integer.toHexString(hashCode())
            + (wasDeleted?"!DELETED!":"")
                + "<rk " + rank() + ':' + name + "(" + TapEnv.languageName(language) + ')' + nameFromBindC + ">";
    }
}
