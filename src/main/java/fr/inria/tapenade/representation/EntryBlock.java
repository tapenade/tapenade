/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.Tree;

/**
 * Special Block at the entry of a Unit.
 */
public final class EntryBlock extends Block {

    /**
     * The list of zones that are considered initialized upon the Unit's entry.
     */
    private TapIntList initializedLocalZones;

    /**
     * Creation, with the given "symbolTable" attached.
     */
    public EntryBlock(SymbolTable symbolTable) {
        super(symbolTable, null, null);
    }

    /**
     * @return The list of zones that are considered initialized upon the Unit's entry.
     */
    public TapIntList initializedLocalZones() {
        return initializedLocalZones;
    }

    /**
     * The list of zones that are considered initialized upon the Unit's entry.
     */
    public void setInitializedLocalZones(TapIntList initializedLocalZones) {
        this.initializedLocalZones = initializedLocalZones;
    }

    /**
     * @return the head Tree, which is the Unit's header tree.
     */
    public Tree headTree() {
        if (instructions == null) {
            return null;
        } else {
            return instructions.head.tree;
        }
    }

    /**
     * Prints a short reference to this EntryBlock onto TapEnv.curOutputStream().
     */
    @Override
    public void cite() throws java.io.IOException {
        TapEnv.print("Entry");
    }

    /**
     * @return the maximum rank of the entries for this EntryBlock.
     * This is probably the same as the number of present entry arrows.
     * This is useful for Units with many entries e.g. namespaces defined by parts.
     */
    public int maxEntryRank() {
        int max = 0;
        TapIntList cases;
        for (TapList<FGArrow> entries = flow(); entries != null; entries = entries.tail) {
            cases = entries.head.cases;
            while (cases != null) {
                if (cases.head > max) {
                    max = cases.head;
                }
                cases = cases.tail;
            }
        }
        return max;
    }

    @Override
    public String toString() {
        return "E" + super.toString();
    }
}
