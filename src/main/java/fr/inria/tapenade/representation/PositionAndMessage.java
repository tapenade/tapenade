/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.Tree;

/**
 * Wrapper around a message that Tapenade emits
 * and its related position in the source.
 */
public final class PositionAndMessage {
    /**
     * Related unit.
     */
    private Unit unit;
    /**
     * Related file name.
     */
    protected String fileName = "";
    /**
     * Related integer position.
     */
    private int position;
    /**
     * Related tree.
     */
    protected Tree tree;
    private final String message;
    /**
     * For correspondence between "source" tree and "message"
     * for html display tools.
     */
    protected TapPair<String, String> tags = new TapPair<>(null, null);

    /**
     * Related unit of this message.
     *
     * @return related Unit.
     */
    protected Unit unit() {
        return unit;
    }

    /**
     * Set related unit of this message.
     *
     * @param unit related Unit.
     */
    protected void setUnit(Unit unit) {
        this.unit = unit;
    }

    /**
     * String message.
     *
     * @return String.
     */
    public String message() {
        return message;
    }

    /**
     * @return integer position.
     */
    public int position() {
        return position;
    }

    /**
     * Set position of the message.
     *
     * @param position integer.
     */
    protected void setPosition(int position) {
        this.position = position;
    }

    /**
     * PositionAndMessage creation.
     *
     * @param i   integer position in the tree.
     * @param msg message.
     * @param tr  associated tree.
     */
    protected PositionAndMessage(int i, String msg, Tree tr) {
        super();
        position = i;
        message = msg;
        tree = tr;
    }

    /**
     * PositionAndMessage creation from an orig PositionAndMessage.
     *
     * @param orig PositionAndMessage
     */
    public PositionAndMessage(PositionAndMessage orig) {
        super();
        assert orig != null;
        unit = orig.unit;
        fileName = orig.fileName;
        position = orig.position;
        tree = orig.tree;
        message = orig.message;
        tags = new TapPair<>(null, null);
    }

    @Override
    public String toString() {
        return "{" + unit + " " + position + " " + message + " " + tags + " " + ILUtils.toString(tree) + "}";
    }
}
