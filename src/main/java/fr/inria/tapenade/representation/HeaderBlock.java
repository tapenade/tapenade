/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.utils.TapIntList;
import fr.inria.tapenade.utils.Tree;

/**
 * Special Block for loop headers in a Flow Graph.
 */
public final class HeaderBlock extends Block {

    /**
     * true if we discovered that the loop index
     * is unused after exit from this loop.
     */
    public boolean loopIndexUnusedAfterLoop;
    /**
     * true if we discovered that the loop index
     * may be overwritten during the loop (dirty !).
     */
    public boolean dirtyOverwrittenIndex;
    /**
     * List of array zones which are accessed by the loop as
     * "localized", i.e. no loop-carried deps between array elements.
     * Example for: Do i; A(i+1)=B(i); B(i-1)=A(i+1); EndDo
     * A is localized, B is not.
     */
    public TapIntList localizedZones;
    /**
     * List of array zones which are accessed by the loop iteration as
     * unique cells: these accesses will be temporarily marked "total".
     */
    public TapIntList uniqueAccessZones;
    /**
     * List of the Trees that access to the unique memory cell
     * of the array zones listed in uniqueAccessZones. Same order.
     */
    public TapList<Tree> uniqueAccessTrees;
    /**
     * List of array zones which are in "uniqueAccessZones" but will not
     * be any more at the above loop level.
     */
    public TapIntList topUniqueAccessZones;
    /**
     * List of array zones which are in "topUniqueAccessZones" and
     * which are "completely" accessed, i.e. all the iterations will
     * end up to have swept all cells of the array.
     * This is used to get a simple killed-array mechanism.
     */
    public TapIntList loopCompleteZones;
    /**
     * True iff this HeaderBlock contains a C-style for-loop that
     * declares the type of its iterator.
     */
    public boolean declaresItsIterator;
    /**
     * The label of the loop.
     */
    private String origCycleLabel;

    public HeaderBlock(SymbolTable symbolTable, TapList<Instruction> parallelControls,
                       TapList<Block> allBlocks) {
        super(symbolTable, parallelControls, allBlocks);
    }

    /**
     * @return the label of the loop.
     */
    public String origCycleLabel() {
        return origCycleLabel;
    }

    /**
     * Sets the label of the loop.
     */
    public void setOrigCycleLabel(String label) {
        origCycleLabel = label;
    }

    /**
     * Inserts the given "block" before the loop entry into this HeaderBlock in the Flow Graph.
     * This requires that the given "block" does not end with a test.
     */
    public void insertBlockBeforeLoopEntry(Block block) {
        FGArrow arrow;
        TapList<FGArrow> toArrows = new TapList<>(null, backFlow());
        TapList<FGArrow> inArrows = toArrows;
        while (inArrows.tail != null) {
            arrow = inArrows.tail.head;
            if (arrow.inACycle) {
                inArrows = inArrows.tail;
            } else {
                arrow.destination = block;
                block.setBackFlow(new TapList<>(arrow, block.backFlow()));
                inArrows.tail = inArrows.tail.tail;
            }
        }
        setBackFlow(toArrows.tail);
        new FGArrow(block, FGConstants.NO_TEST, null, this);
    }

    /**
     * Inserts the given "block" before loop cycling in this HeaderBlock in the Flow Graph.
     * This requires that the given "block" does not end with a test.
     */
    public void insertBlockBeforeLoopCycle(Block block) {
        FGArrow arrow;
        TapList<FGArrow> toArrows = new TapList<>(null, backFlow());
        TapList<FGArrow> inArrows = toArrows;
        while (inArrows.tail != null) {
            arrow = inArrows.tail.head;
            if (arrow.inACycle) {
                arrow.destination = block;
                arrow.inACycle = false ;
                block.setBackFlow(new TapList<>(arrow, block.backFlow()));
                inArrows.tail = inArrows.tail.tail;
            } else {
                inArrows = inArrows.tail;
            }
        }
        setBackFlow(toArrows.tail);
        new FGArrow(block, FGConstants.NO_TEST, null, this, true);
    }

    /**
     * @return the list of arrows entering the loop level whose header is this HeaderBlock.
     */
    public TapList<FGArrow> enteringArrows() {
        TapList<FGArrow> arrows = backFlow();
        FGArrow arrow;
        TapList<FGArrow> hdResult = new TapList<>(null, null);
        TapList<FGArrow> tlResult = hdResult;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.finalCycle() != arrow.destination.enclosingLoop()) {
                tlResult = tlResult.placdl(arrow);
            }
            arrows = arrows.tail;
        }
        return hdResult.tail;
    }

    /**
     * @return the list of arrows cycling inside the loop level whose header is this HeaderBlock.
     */
    public TapList<FGArrow> cyclingArrows() {
        TapList<FGArrow> arrows = backFlow();
        FGArrow arrow;
        TapList<FGArrow> hdResult = new TapList<>(null, null);
        TapList<FGArrow> tlResult = hdResult;
        while (arrows != null) {
            arrow = arrows.head;
            if (arrow.finalCycle() == arrow.destination.enclosingLoop()) {
                tlResult = tlResult.placdl(arrow);
            }
            arrows = arrows.tail;
        }
        return hdResult.tail;
    }

    /**
     * Prints a short reference to this HeaderBlock onto TapEnv.curOutputStream().
     */
    @Override
    public void cite() throws java.io.IOException {
        TapEnv.print("HB(" + rank + ')');
    }

    @Override
    public String toString() {
        return "H" + super.toString();
    }
}
