/*
 * TAPENADE Automatic Differentiation Engine
 * Copyright (C) 1999-2021 Inria
 * See the LICENSE.md file in the project root for more information.
 *
 */

package fr.inria.tapenade.representation;

import fr.inria.tapenade.analysis.ADActivityAnalyzer;
import fr.inria.tapenade.analysis.ADTBRAnalyzer;
import fr.inria.tapenade.analysis.DepsAnalyzer;
import fr.inria.tapenade.analysis.DiffLivenessAnalyzer;
import fr.inria.tapenade.analysis.InOutAnalyzer;
import fr.inria.tapenade.analysis.MultithreadAnalyzer;
import fr.inria.tapenade.analysis.PointerAnalyzer;
import fr.inria.tapenade.analysis.ReqExplicit;
import fr.inria.tapenade.utils.TapPair;
import fr.inria.tapenade.utils.TapTriplet;
import fr.inria.tapenade.utils.Tree;

import java.io.OutputStream;
import java.nio.file.Path;

/**
 * Contains all settings needed by Tapenade at some point,
 * (except those specific to AD that go into adEnv),
 * and that are specific for each individual thread.
 */
public final class TapEnvForThread {

    /**
     * Association from original include-file name to differentiated include-file name,
     * plus collection of waiting include Trees that should receive the differentiated
     * include-file name if it is created.
     */
    public final TapList<TapTriplet<String, String, Tree>> differentiatedIncludeNames =
            new TapList<>(null, null);
    /**
     * The OutputStream for traces (milestones, differentiation debug traces...).
     */
    public final OutputStream traceOutputStream = System.out;
    /**
     * [FortranDialect, C, CPP].
     */
    protected final int[] languages = new int[3];
    public String parserFileSeparator;
    protected TapList<MixedLanguageInfos> mixedLangInfos;

    /** When true, prefer generate #include instead of Fortran include */
    public boolean useSharpInclude = false ;

    /**
     * Current differentiation mode.
     */
    protected int diffMode = TapEnv.TANGENT_MODE;
    /**
     * Size of an integer for this system.
     */
    public int integerSize = 4;
    /**
     * Size of a byte for this system.
     */
    public int byteSize = TapEnv.BYTE_SIZE;
    /**
     * Size of a char for this system.
     */
    public int charSize = TapEnv.CHAR_SIZE;
    /**
     * Size of a pointer for this system.
     */
    public int pointerSize = TapEnv.POINTER_SIZE;
    /**
     * Size of a float for this system.
     */
    public int realSize = TapEnv.REAL_SIZE;
    /**
     * Size of a double precision float for this system.
     */
    public int doubleRealSize = 2 * TapEnv.REAL_SIZE;
    /**
     * Size of a long double precision float for this system.
     */
    public int doubleDoubleRealSize = 4 * TapEnv.REAL_SIZE;
    /**
     * True if the coming differentiation will feature some tangent diff.
     */
    public boolean mustTangent;
    /**
     * True if the coming differentiation will feature some adjoint diff.
     */
    public boolean mustAdjoint;
    /**
     * Number of replicates of diff variables, as requested by "-replicateDiff" option.
     */
    public int diffReplica = 1 ;
    /**
     * True if the coming differentiation will be multi-directional.
     */
    public boolean multiDirDiffMode;
    /**
     * When non-null, the expression to use as the max number of vector-diff directions.
     */
    public String fixedNbdirsmaxString;
    /**
     * When non-null, the Tree to use as the max number of vector-diff directions.
     * This is used only for CompositeTypeSpec.differentiateTypeSpec()
     * Otherwise, take the current diff Unit cdu and call
     * cdu.dirNumberMaxSymbolHolder.makeNewRef(cdu.publicSymbolTable())
     */
    public Tree fixedNbdirsmaxTree;
    /**
     * When non-null, the expression to use as the number of vector-diff directions.
     */
    public String fixedNbdirsString;
    /**
     * When non-null, the Tree to use as the number of vector-diff directions.
     */
    public Tree fixedNbdirsTree;
    /**
     * True if the coming tangent diff will include estimation of the differentiability interval.
     */
    protected boolean valid;
    /**
     * When true, activity analysis is activated.
     */
    protected boolean doActivity = true;
    /**
     * When false, usefulness analysis is de-activated.
     */
    protected boolean doUsefulness = true;
    /**
     * When true, non-active procedure arguments are not differentiated.
     */
    public boolean spareNoDiffArgs = true;
    /**
     * When true, reinitialization of derivative of passive variables is postponed.
     */
    protected boolean spareDiffReinitializations = true;
    /**
     * When true, diff expressions are split to eliminate common subexpressions.
     */
    public boolean splitDiffExpressions = true;
    /**
     * When true, diff instructions are merged when profitable.
     */
    public boolean mergeDiffInstructions = true;
    /**
     * When true, only used variables are pushed on the adjoint mode stack.
     */
    public boolean dontSaveUnused = true;
    /**
     * When true, TBR analysis is run to further reduce the adjoint mode stack.
     */
    public boolean doTBR = true;
    /**
     * When true, cheap instructions may be repeated to further reduce the adjoint mode stack.
     */
    public boolean doRecompute = true;
    /**
     * When true, primal code that is eventually not needed for derivatives is sliced away.
     */
    public boolean removeDeadPrimal = true;
    /**
     * When true, original control instructions that are eventually\
     * not needed for derivatives are sliced away.
     */
    public boolean removeDeadControl = true;
    /**
     * When true, code outside the differentiation root is also regenerated,
     * to prepare the context to call the diff code.
     */
    public boolean mustContext;
    /**
     * TEMPORARY: Option to run the old context system (before option "-context" existed),
     * that adds memory allocation and pointer computations of the adjoint variables
     * into the copied "_CD" and "_CB" procedures.
     */
    public boolean oldContext;
    /**
     * When true, try to use local static storage when possible instead of PUSH/POP.
     */
    public boolean staticTape;
    /**
     * When true, differentiated types are simplified by stripping passive components.
     */
    public boolean stripDiffTypes = true;
    /**
     * When true, useless primal code (procedures...) is stripped from differentiated files.
     */
    public boolean stripPrimalCode = true;
    /**
     * (Requested by one user, but dangerous)
     * When true, the fact that a unit IMPORTS an active module is not a sufficient reason to
     * keep its primal version in the diff code (as a _NODIFF unit).
     */
    public boolean stripPrimalEvenIfImportsActive;
    /**
     * When false, all modules are differentiated (previous default, for compatibility and test)
     * when true, only active modules are differentiated.
     */
    public boolean stripPrimalModules = true;
    /**
     * When true, instrument the diff code with profiling instructions.
     */
    public boolean profile;
    /**
     * When true, place differentiated values using association-by-address instead of association-by-name.
     */
    protected boolean associationByAddress;

    /**
     * Suffix for orig values with association-by-address.
     */
    public String assocAddressValueSuffix = "v";

    /**
     * Suffix for diff values with association-by-address.
     */
    public String assocAddressDiffSuffix = "d";

    /**
     * The FORTRAN module, C unit, C++ unit that defines the special types for association-by-address.
     */
    protected Unit[] assocAddressDiffTypesUnits = new Unit[3];
    /**
     * Debug mode of differentiated programs.
     * NO_DEBUG:no debug ; TGT_DEBUG:debug tangent ; ADJ_DEBUG:debug adjoint.
     */
    protected int debugAdMode;
    /**
     * label all calls to ADMM primitives with a unique label for debugging
     */
    protected boolean debugADMM = false;
    /**
     * When true, in debug tangent mode, tries also to test that the passives are not varied.
     */
    public boolean debugPassives;
    /**
     * When true, forbids modifications of the dependent and independent of root Units.
     */
    public boolean fixInterface;
    /**
     * When true, fortran REAL's are turned into COMPLEX'es to enable the complex-step method
     */
    public boolean complexStep = false ;

    /**
     * When true, place a common numeric mark on matching Push/POP's.
     */
    public boolean numberPushPops;
    public int pushPopNumber;
    public int inputLanguage = TapEnv.UNDEFINED;
    public int outputLanguage = TapEnv.UNDEFINED;
    /**
     * The directory where to find the Library specification files.
     */
    public String stdLibraryDirectory;
    /**
     * The directory where to put all generated files.
     */
    public String outputDirectory = "."+java.io.File.separator;
    /**
     * Summarizes all extra options selected for this Tapenade run.
     */
    public String optionsString = "";
    /**
     * The suffix used to build the names of differentiated files.
     */
    public String diffFileSuffix = "";
    /**
     * The suffix used to build the names of preprocessed/non-differentiated files.
     */
    public String preprocessFileSuffix = "_p";
    /**
     * The additional suffix for "multi-directional" diff modes.
     */
    public String multiFuncSuffix = "v";
    /**
     * .f90, .f95 or .f03.
     */
    public String suffixF90 = ".f90??";
    /**
     * True when dump mode is on.
     */
    public boolean dumps;

    /**
     * If an error during parsing occurs, exit(1).
     */
    public boolean hasParsingError;
    /**
     * Used for source-code correspondence.
     */
    protected int nextCodeTag;
    protected int nextSourceTag;
    /**
     * Used for source-message correspondence.
     */
    protected int nextSourceMsgTag;
    protected int nextMessageTag;
    /**
     * True when -p and no analysis.
     */
    public boolean createStub;
    /**
     * List of names of Unit's to be dumped.
     */
    public TapList<String> dumpUnitNames;
    /**
     * True if user wants a compact dump of the source Call Graph.
     */
    public boolean dumpCallGraph;
    /**
     * True if user wants a dump of the source Symbol Tables.
     */
    public boolean dumpSymbolTables;
    /**
     * True if user wants a dump of the source Flow-Graphs.
     */
    public boolean dumpFlowGraphs;
    /**
     * True if user wants a dump of the zones numbers used in the dump.
     */
    public boolean dumpZones;
    /**
     * True if user wants a dump of the Data Flow results on the source.
     */
    public boolean dumpDataFlow;
    /**
     * True if user wants a dump of the differentiated code's Symbol Tables.
     */
    public boolean dumpDiffSymbolTables;
    /**
     * True if user wants a dump of the differentiated code's Flow-Graphs.
     */
    public boolean dumpDiffFlowGraphs;
    /**
     * True if user wants a dump of pointer info in the data-flow of the Call Graph.
     */
    public boolean dumpPointers;
    /**
     * True if user wants a dump of In-Out info in the data-flow of the Call Graph.
     */
    public boolean dumpInOut;
    /**
     * True if user wants a dump of dependency info matrices in the data-flow of the Call Graph.
     */
    public boolean dumpDeps;
    /**
     * When true, Tapenade prints the system command(s) used to parse the input files into IL.
     */
    public boolean traceParser;
    /**
     * When true, the tree operators coming from the input IL will be shown.
     */
    public boolean traceInputIL;
    /**
     * When true, we trace analysis on-the-fly on the curUnit.
     */
    public boolean traceCurAnalysis;
    /**
     * Names of Unit's for which we require detailed trace messages during FlowGraph building.
     */
    public TapList<String> traceFlowGraphBuild;
    /**
     * Names of Unit's for which we require detailed trace messages during typecheck.
     */
    public TapList<String> traceTypeCheck;
    /**
     * Names of Unit's for which we require detailed trace messages during inlining.
     */
    public TapList<String> traceInline;
    /**
     * List of names of Unit's requiring detailed trace messages during Pointer analysis.
     */
    public TapList<String> tracePointer;
    /**
     * List of names of Unit's requiring detailed trace messages during In-Out analysis.
     */
    public TapList<String> traceInOut;
    /**
     * List of names of Unit's requiring detailed trace messages during Deps analysis.
     */
    public TapList<String> traceDeps;
    /**
     * List of names of Unit's requiring detailed trace messages during ADDeps analysis.
     */
    public TapList<String> traceADDeps;
    /**
     * List of names of Unit's requiring detailed trace messages during Activity analysis.
     */
    public TapList<String> traceActivity;
    /**
     * List of names of Unit's requiring detailed trace messages during ReqExplicit analysis.
     */
    public TapList<String> traceReqExplicit;
    /**
     * List of names of Unit's requiring detailed trace messages during DiffLiveness analysis.
     */
    public TapList<String> traceDiffLiveness;
    /**
     * List of names of Unit's requiring detailed trace messages during TBR analysis.
     */
    public TapList<String> traceTBR;
    /**
     * List of names of Unit's requiring detailed trace messages during Multithread analysis.
     */
    public TapList<String> traceMultithread;
    /**
     * List of names of function Unit's declared by the user as "duplicable", i.e. cheap and without side-effects
     */
    public TapList<String> duplicableUnitNames;
    /**
     * When true, run special analyses on MPI calls.
     */
    protected boolean doMPI;
    /**
     * When true, run analyses for OpenMP clauses.
     */
    protected boolean doOpenMP;
    /**
     * When true, run special analyses for OpenMP clauses using Z3 model checker.
     */
    protected boolean doOpenMPZ3;
    /**
     * The data-dependency analyzer, finding dependencies of outputs wrt inputs of procedures.
     */
    protected DepsAnalyzer depsAnalyzer;
    /**
     * The In-Out analyzer, finding variables read and/or written by procedures.
     */
    protected InOutAnalyzer inOutAnalyzer;
    /**
     * The OpenMP clauses analyzer, finding the OpenMP status of variables (shared, private...)
     */
    protected MultithreadAnalyzer multithreadAnalyzer;
    /**
     * The activity analyzer, finding variables that must have a derivative.
     */
    protected ADActivityAnalyzer adActivityAnalyzer;
    /**
     * The TBR analyzer, finding variables that must be stored and then retrieved
     * during an adjoint differentiation.
     */
    protected ADTBRAnalyzer adTbrAnalyzer;
    /**
     * The differentiable liveness analyzer, finding original code that is
     * eventually useful or not for the derivative computations.
     */
    protected DiffLivenessAnalyzer diffLivenessAnalyzer;
    /**
     * The pointer destination anlayzer (points-to analysis).
     */
    protected PointerAnalyzer pointerAnalyzer;
    /**
     * The pointer activity analyzer, finds pointers for which a derivative
     * must be defined. Also finds allocations, de-allocations, and pointer operations
     * that must be transposed for the derivative counterpart.
     */
    protected ReqExplicit reqExplicitAnalyzer;
    /**
     * The default checkpointing decision on call sites.
     */
    public boolean defaultCheckpointCalls = true ;
    /**
     * This integer will be incremented each time we create a new temporary
     * symbol (tmp, arg, result, dumm, dummyarraydiff...)
     * and then attached to the new symbol name to make it unique
     * so that it can be recognized, e.g. with sed.
     */
    public int newSHdisambiguator = -1;
    /**
     * In the differentiated code, do not create "ISIZEOF" constants that the user must
     * then define, but rather use the Fortran90 dynamic SIZE() function.
     */
    public boolean noisize;
    /**
     * Like noisize, but use the dynamic SIZE() function even when not in Fortran90.
     */
    public boolean noisize77;
    /**
     * When true, the differentiated code incorporates enough of the primal code to be standalone.
     */
    public boolean standaloneDiff = false;
    /**
     * List of names of Unit's of Units requiring detailed trace messages during differentiation.
     */
    public TapList<String> traceDifferentiationUnitNames;
    /**
     * List of names of Unit's of Units requiring detailed trace messages during tree regeneration.
     */
    public TapList<String> traceTreeGenUnitNames;
    /**
     * List of the source Units requiring detailed trace messages during tree regeneration.
     */
    public TapList<Unit> srcUnitsToTraceInTreeGen;
    /**
     * Rank of one Block in the selected Unit, on which traces should focus.
     */
    public int traceBlockRk = -99;
    /**
     * Name of the destination Unit of CallArrow's on which traces should focus.
     */
    public String traceCalledUnitName = null;

    /**
     * When differentiating, holds the CallGraph of the original, non differentiated code.
     */
    public CallGraph origCallGraph() {
        return origCallGraph;
    }

    public void setOrigCallGraph(CallGraph cg) {
        origCallGraph = cg;
    }

    private CallGraph origCallGraph;

    /**
     * About handling of comments by the preprocessors (e.g. cpp, fpp...)
     * Default false implies preprocessor called with "-C", thus keeping comments.
     */
    public boolean noComment;

    /**
     * A-List of SymbolTables already shown on dump.
     * A-List of TapPair's (SymbolTable,String to refer to it).
     */
    public TapList<TapPair<SymbolTable, String>> seenSymbolTables;
    /**
     * The OutputStream for traces (milestones, differentiation debug traces...).
     */
    public OutputStream dumpOutputStream;
    /**
     * The current output stream used by print().
     */
    public OutputStream curOutputStream = System.out;
    public TapList<OutputStream> outputStreamStack = new TapList<>(System.out, null);
    /**
     * The "current" Unit, used to attach all messages emitted, and also to
     * know the particular programming language, sometimes incurring limitations.
     */
    protected Unit relatedUnit;
    protected TapList<Unit> relatedUnits = new TapList<>(null, null);
    protected int relatedLanguage = TapEnv.UNDEFINED;
    /**
     * -I include path.
     */
    protected TapList<String> includeDirs;
    /**
     * The root directory of the parsed files.
     */
    protected Path rootDirectoryPath;
    /**
     * The current directory, used to look for include files.
     */
    protected String currentDirectory;
    protected String currentParsedFile;
    /**
     * The max line length for our Fortran90 parser.
     */
    public String fortran90LineLength = "132";
    public int fortran90OutputLineLength = 72;
    /**
     * The command to run the/a preprocessor before AD of a fortran file.
     */
    public String fppCommand;
    /**
     * The options for the preprocessor before AD of a fortran file.
     */
    public String fppOptions;
    /**
     * The command to run the/a preprocessor before AD of a C file.
     */
    public String cppCommand;
    /**
     * The options for the preprocessor before AD of a C file.
     */
    public String cppOptions;
    /**
     * When true, refine application of the ADMM mechanism i.e. Register/Unregister/Rebase
     * by restricting it to those that really need it (according to a static data-flow analysis).
     * <p>
     * a) Only Rebase pointers that may point to chunks that may be reallocated
     * <p>
     * b) Only Register/Unregister chunks that may be pointed to by TBR pointers.
     */
    public boolean refineADMM = true;
    /**
     * When true, expand all includes in the generated files.
     */
    public boolean expandAllIncludeFile;
    /**
     *
     */
    public boolean expandDiffIncludeFile;
    /**
     * The include Instruction that contained the current Instruction.
     */
    protected Instruction currentIncludeInstruction;
    //TODO: Check includeFiles possibly redundant with includeFileStack ??
    public TapList<String> includeFiles;
    protected boolean inIncludeFile;
    /**
     * Stack of the currenly opened include files,
     * i.e. the names of the include files we are currently in.
     */
    protected TapList<String> includeFileStack;

    /** 4 stacks working synchronously, to allow for saving and restoring "include managers" */
    private TapList<Boolean> bbStack = null ;
    private TapList<TapList<String>> f1Stack = null ;
    private TapList<TapList<String>> f2Stack = null ;
    private TapList<Instruction> iiStack = null ;

    public void saveAndResetIncludeManager() {
        bbStack = new TapList<>((inIncludeFile ? Boolean.TRUE : Boolean.FALSE), bbStack) ;
        f1Stack = new TapList<>(includeFiles, f1Stack) ;
        f2Stack = new TapList<>(includeFileStack, f2Stack) ;
        iiStack = new TapList<>(currentIncludeInstruction, iiStack) ;
        inIncludeFile = false ;
        includeFiles = null ;
        includeFileStack = null ;
        currentIncludeInstruction = null ;
    }

    public void restoreIncludeManager() {
        inIncludeFile = (bbStack.head==Boolean.TRUE) ;
        bbStack = bbStack.tail ;
        includeFiles = f1Stack.head ;
        f1Stack = f1Stack.tail;
        includeFileStack = f2Stack.head ;
        f2Stack = f2Stack.tail;
        currentIncludeInstruction = iiStack.head ;
        iiStack = iiStack.tail;
    }

    /**
     * For dumpUnit.
     */
    protected CallArrow relatedArrow;
    /**
     * List of inlinedFunctions (cf libraries/GeneralLibReader).
     */
    public InlinedFunctions inlinedFunctions = new InlinedFunctions();
    /**
     * For a unit, list of all labels.
     */
    public TapList<String> usedLabels;
    /**
     * Used during instruction splitting: Static ints to increment
     * the number at the end of the intermediate var names
     * such as "result", "arg".
     */
    protected int resultRK = 1;
    protected int argRK = 1;
    protected int expRK = 1;
    /**
     * Used in treeGen.
     */
    public int nextNewLabel = 100;
    /**
     * The kind of zones that are currently differentiated. Usually, it is
     * REALKIND, but in a very special mode, it can be ALLKIND.
     */
    protected int diffKind = SymbolTableConstants.REALKIND;
    /**
     * When true, deactivates filterVariedUsefulWithDeps() and
     * mergeUsefulnessIntoVariedness() so that when the normal
     * mode eventually finds no activity at all, this mode
     * still runs top-down activity analysis through the code.
     * This may help find where the activity chain is broken.
     */
    protected boolean debugActivity;
    /**
     * The list of all messages that could not be related to such or such Unit.
     */
    protected TapList<PositionAndMessage> danglingMessages = new TapList<>(null, null);
    protected SymbolTable currentTranslationUnitSymbolTable;
    /**
     * The current amount of indentation.
     */
    protected int traceIndent;
    /**
     * True when trace is at the start of a new line.
     */
    protected boolean traceLineStart = true ;
    /**
     * When false, messages DD05 (Cannot combine successive declarations...) are inhibited.
     * Because repeated inclusion of C standard include files cause spurious DD05 messages.
     */
    protected boolean combineDeclMessagesSwitch = true;
    /**
     * Set to true to test Andreas' ABS Linear Forms ("ALF").
     */
    public boolean absLinearForm;
    /**
     * Unit's for which we require detailed trace messages during typecheck.
     */
    protected TapList<Unit> tracedTypeCheckUnits;
    /**
     * True when we want to trace typeCheck analysis on-the-fly on the current Unit.
     */
    protected boolean traceTypeCheckAnalysis;
    /**
     * Unit's for which we require detailed trace messages during inline.
     */
    public TapList<Unit> tracedInlineUnits;
    /**
     * True when we want to trace inline analysis on-the-fly on the current Unit.
     */
    protected boolean traceInlineAnalysis;
    /**
     * The list of all messages with position == NEXT_TREE.
     */
    private TapList<PositionAndMessage> nextTreeMessages = new TapList<>(null, null);

    public final void parserError(String message) {
        hasParsingError = true;
        emitMessage(TapEnv.NO_POSITION, null, "File: " + message, 0);
    }

    public final synchronized void emitMessage(int position, Tree tree, String message, int inverseSeverity) {
        //synchronized: emitMessage est appelee par des exceptions et par du code normal.
        // => il ne faut pas appeler emitMessage dans 2 threads en meme temps
        //    sinon danglingMessages perd des messages

// prepend file name, line number, unit name to message
if (TapEnv.BETTERMESSAGESTYLE) message = addLocationInfo(message, tree) ;

        TapList<PositionAndMessage> toMessages = (relatedUnit == null ? danglingMessages : relatedUnit.messages) ;
        PositionAndMessage pm = new PositionAndMessage(position, message, tree);
        PositionAndMessage curpm;
        setSourceMessageCorrespondence(tree, pm);
        while (toMessages.tail != null && position > toMessages.tail.head.position()) {
            toMessages = toMessages.tail;
        }
        boolean messageDejaVu = false;
        while (!messageDejaVu && toMessages.tail != null) {
            curpm = toMessages.tail.head;
            messageDejaVu =
                    position == curpm.position() && tree == curpm.tree && message.equals(curpm.message());
            toMessages = toMessages.tail;
        }

        if (!messageDejaVu) {
            // Send message to .msg file. This is also used to keep track to avoid duplicates:
            toMessages.placdl(pm) ; //was toMessages.tail = new TapList<>(pm, toMessages.tail);

            // print on standard out. Print only messages above min severity :
            if (inverseSeverity <= TapEnv.msgLevel()) {
                TapEnv.printlnOnTrace(message);
            }
            // if there is a dump file, print on it. Print all messages whatever min severity:
            if (curOutputStream != System.out) {
                try {
                    curOutputStream.write((message + System.lineSeparator())
                            .getBytes(java.nio.charset.StandardCharsets.UTF_8));
                } catch (java.io.IOException e) {
                    System.out.println("OutputStream for messages broken "+e);
                }
            }
            // store in the list of messages for the present tree, to be written
            // for "-msginfile" mode or for source-message correspondence.
            // Print all messages whatever min severity:
            TapEnv.addMessageAnnotation(tree, message);
            if (position == TapEnv.NEXT_TREE) {
                nextTreeMessages = new TapList<>(pm, nextTreeMessages);
            } else if (position >= 0) {
                PositionAndMessage pm1;
                while ((pm1 = nextTreeMessages.head) != null) {
                    pm1.setPosition(position);
                    setSourceMessageCorrespondence(tree, pm1);
                    TapEnv.addMessageAnnotation(tree, pm1.message()); //[llh] redondant?
                    nextTreeMessages = nextTreeMessages.tail;
                }
            }
        }
    }

    /** Add to the message some locationinfo, e.g. line number and source file name */
    private String addLocationInfo(String message, Tree relTree) {
        if (relatedUnit!=null) {
            message = relatedUnit.name()+": "+message ;
        }
        while( relTree!=null && relTree.parent()!=null ) {
            relTree = relTree.parent();
        }
        if (relTree==null) {
            message = "unknown related tree: "+message ;
        } else {
            Instruction instr = relTree.getAnnotation("instruction") ;
            if (instr==null) {
                message = "unknown related instr: "+message ;
            } else {
                message = "line "+instr.lineNo+": "+message ;
                Unit relUnit = (relatedUnit!=null ? relatedUnit : (instr.block!=null ? instr.block.unit() : null)) ;
                if (relUnit==null) {
                    message = "unknown related Unit, "+message ;
                } else {
                    Unit relFile = relUnit ;
                    while (relFile!=null && !relFile.isTranslationUnit()) {
                        relFile = relFile.upperLevelUnit() ;
                    }
                    if (relFile==null) {
                        message = "unknown related file, "+message ;
                    } else {
                        message = "File "+relFile.name()+", "+message ;
                    }
                }
            }
        }
        return message ;
    }

    /**
     * Place a source-code correspondence between "source" tree and "message"
     * for html display tools.
     */
    private void setSourceMessageCorrespondence(Tree source, PositionAndMessage message) {
        if (source != null) {
            TapPair<String, String> sourceTags =
                    source.getAnnotation("toMessageTags");
            TapPair<String, String> messageTags = message.tags;
            if (sourceTags == null) {
                sourceTags = new TapPair<>(null, null);
                source.setAnnotation("toMessageTags", sourceTags);
            }
            if (messageTags == null) {
                messageTags = new TapPair<>(null, null);
                message.tags = messageTags;
            }
            if (sourceTags.second == null) {
                if (messageTags.first == null) {
                    messageTags.first = "p" + nextMessageTag;
                    nextMessageTag++;
                }
                sourceTags.second = messageTags.first;
            } else {
                messageTags.first = sourceTags.second;
            }
            if (messageTags.second == null) {
                if (sourceTags.first == null) {
                    sourceTags.first = "p" + nextSourceMsgTag;
                    nextSourceMsgTag++;
                }
                messageTags.second = sourceTags.first;
            }
        }
    }

    /**
     * Flush the current OutputStream for all Strings
     * printed and all messages emitted through this TapEnv.
     */
    public final synchronized void flushOutputStream() {
        try {
            curOutputStream.flush();
        } catch (java.io.IOException e) {
            System.out.println("OutputStream for messages broken "+e);
        }
    }

    protected TapEnvForThread() {
    }
}
